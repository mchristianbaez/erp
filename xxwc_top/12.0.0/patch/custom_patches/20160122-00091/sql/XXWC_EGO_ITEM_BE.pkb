CREATE OR REPLACE PACKAGE BODY APPS.XXWC_EGO_ITEM_BE
AS
   /***************************************************************************************************************************************
   -- File Name: XXWC_EGO_ITEM_BE.pkb
   --
   -- PROGRAM TYPE: PL/SQL Script   <API>
   --
   -- PURPOSE:
   -- HISTORY
   -- ====================================================================================================================================
   -- ====================================================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- ---------------------------------------------------------------------------------------
   -- 1.0     01-MAR-2015   Shankar Vanga   TMS#20150126-00044  - Create Single Item Workflow Tool
   -- 2.0     26-May-2015   P.vamshidhar    TMS#20150519-00091 - Single Item UI Enhancements
                                            1. Added to function to create link in Workflow monitor
                                            if function return 0 user not eligible to access link else yes.
                                            2. Adding Global_Attributre6 to store Core status  in apply_changes
                                            procedure.
                                            3. Cas_number new column added to process_items procedure
                                            4  Country of origin storing in C_EXT_ATTR2.It was added in UPdated_UDA_Attribute proc.
   -- 3.0     17-Jun-2015  P.vamshidhar     1. Commented Purging data from temp table.  The will be taken care in
                                               'XXWC Rejected Items Purging' program. TMS#20150609-00025

   -- 4.0     22-Jun-2015   P.Vamshidhar    TMS#20150608-00031  - Single Item UI Enhancements.
                                            Added code to populate below UDA attributes.
                                            Fullsize Image    - XXWC_A_F_1_ATTR   - C_EXT_ATTR15
                                            Thumbnail Image   - XXWC_A_T_1_ATTR   - C_EXT_ATTR20

   -- 5.0     13-Aug-2015   P.Vamshidhar    TMS#20150708-00061  - PCAM enhancements part 3
                                            Added code to process data related cyber tax code
                                            Cyber Source Tax code  -  C_EXT_ATTR11
                                            Fullfillment           -  C_EXT_ATTR7

   -- 6.0     24-Sep-2015   P.Vamshidhar    TMS#20150901-00146 - PCAM add delete functionality for Cross References
                                            and TMS#20150917-00149 -- Cross reference Length issue.
                                            and TMS#20150807-00170 -- PDH - Add UDA in PDH for Last Verified Date
                                            TMS#20141006-00021 - Description and Shelf Life changes not consistently
                                            propagating to lower level Orgs from Master Organization
                                            added LBS to if WEIGHT_UOM_CODE is null
   -- 7.0     16-Nov-2015   P.Vamshidhar    TMS#20151109-00106 - Issue while updating WEIGHT_UOM_CODE.  Hence moved code into
                                            XXWC_PIM_CHG_ITEM_MGMT

   -- 8.0     04-Dec-2015   P.Vamshidhar    TMS#20151202-00192 - PCAM issue with UN Number and Hazard Class

   -- 9.0     06-Oct-2016   P.Vamshidhar    TMS#20160603-00026 - PCAM catclass value not updating in Oracle
   --10.0     30-Nov-2016   P.Vamshidhar    TMS#20160122-00091  - GL Code Enhancements to leverage GL API

   ***********************************************************************************************************************************/

   g_err_callfrom   VARCHAR2 (100) := 'XXWC_EGO_ITEM_BE';
   g_distro_list    VARCHAR2 (100) := 'HDSOracleDevelopers@hdsupply.com';



   PROCEDURE DEBUG_LOG (P_MSG_TEXT IN VARCHAR2, p_COMMIT IN BOOLEAN := FALSE)
   IS
   /**********************************************************************************************************************************
            PROCEDURE : DEBUG_LOG

              REVISIONS:
              Ver        Date        Author                     Description
              ---------  ----------  ---------------    ------------------------------------------------------------------------------
              1.0        01-MAR-2015   Shankar Vanga     TMS#20150126-00044  - Create Single Item Workflow Tool
                                                         Initial Version

   ***********************************************************************************************************************************/


   BEGIN
      --dbms_output.put_line(P_MSG_TEXT);
      --insert into xxwc.xxwc_temp_bus_eve(MESG) values ( P_MSG_TEXT);
      NULL;
   --commit;

   END DEBUG_LOG;

   ------------------------------------------------------


   PROCEDURE purge_temp_data (i_change_id           IN NUMBER,
                              i_item_id                NUMBER,
                              i_change_order_type      VARCHAR2)
   IS
      /*******************************************************************************************************************************
               PROCEDURE : purge_temp_data

                 REVISIONS:
                 Ver        Date        Author                     Description
                 ---------  ----------  ---------------    ----------------------------------------------------------------------------
                 1.0        01-MAR-2015  Shankar Vanga     TMS#20150126-00044  - Create Single Item Workflow Tool
                                                           Initial Version
      *********************************************************************************************************************************/


      l_delete_flag   VARCHAR2 (1) := 'Y';
      l_error_code    VARCHAR2 (1) := 'S';
      l_err_msg       VARCHAR2 (2000);

      l_procedure     VARCHAR2 (100) := 'PURGE_TEMP_DATA';
   BEGIN
      ---not to call this..changed by Ram for PK values

      fnd_attached_documents2_pkg.delete_attachments (
         X_entity_name                => 'XXWC_MTL_SY_ITEMS_CHG_B',
         X_pk1_value                  => i_item_id,
         X_pk2_value                  => '222',
         X_pk3_value                  => i_change_id,
         X_pk4_value                  => NULL,
         X_pk5_value                  => NULL,
         X_delete_document_flag       => l_delete_flag,
         X_automatically_added_flag   => NULL);

      --update the tables with reject status no delete

      DELETE FROM xxwc_mtl_sy_items_chg_b
            WHERE oracle_change_id = i_change_id;

      DELETE FROM xxwc.xxwc_chg_mtl_cross_ref
            WHERE oracle_change_id = i_change_id;

      DELETE FROM xxwc.xxwc_chg_ego_item_attrs
            WHERE change_id = i_change_id;
   --Need to call Item delete from base tables...later..
   EXCEPTION
      WHEN OTHERS
      THEN
         l_err_msg :=
               l_err_msg
            || ' ...Error_Stack...'
            || DBMS_UTILITY.format_error_stack ()
            || ' Error_Backtrace...'
            || DBMS_UTILITY.format_error_backtrace ();

         -- Calling ERROR API
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || l_procedure,
            p_ora_error_msg       => SUBSTR (l_err_msg, 1, 2000),
            p_error_desc          => SUBSTR (l_err_msg, 1, 240),
            p_distribution_list   => g_distro_list,
            p_module              => 'INV');

         l_error_code := 'E';
         ROLLBACK;
         l_err_msg :=
            'Error in deleting from fnd_attachments and delete tables for Change Order:';
   END;

   ---------------------------------------------------------------------------------------
   PROCEDURE print_uda_data (P_ATTRS_ROW_TABLE     EGO_USER_ATTR_ROW_TABLE,
                             P_ATTRS_DATA_TABLE    EGO_USER_ATTR_DATA_TABLE)
   IS
   /*******************************************************************************************************************************
            PROCEDURE : print_uda_data

              REVISIONS:
              Ver        Date        Author                     Description
              ---------  ----------  ---------------    ----------------------------------------------------------------------------------
              1.0        01-MAR-2015   Shankar Vanga     TMS#20150126-00044  - Create Single Item Workflow Tool
                                                         Initial Version

   *********************************************************************************************************************************/


   BEGIN
      FOR ag_row IN 1 .. P_ATTRS_ROW_TABLE.COUNT
      LOOP
         FOR at_data IN 1 .. P_ATTRS_DATA_TABLE.COUNT
         LOOP
            IF (P_ATTRS_ROW_TABLE (AG_ROW).ROW_IDENTIFIER =
                   P_ATTRS_DATA_TABLE (at_data).ROW_IDENTIFIER)
            THEN
               DBMS_OUTPUT.put_line (
                     P_ATTRS_ROW_TABLE (AG_ROW).ROW_IDENTIFIER
                  || ':'
                  || P_ATTRS_ROW_TABLE (AG_ROW).attr_group_name
                  || '.'
                  || P_ATTRS_DATA_TABLE (at_data).ATTR_NAME
                  || ' = '
                  || P_ATTRS_DATA_TABLE (at_data).ATTR_VALUE_STR
                  || P_ATTRS_DATA_TABLE (at_data).ATTR_VALUE_NUM
                  || P_ATTRS_DATA_TABLE (at_data).ATTR_VALUE_DATE);
            END IF;
         END LOOP;
      END LOOP;
   END;

   ------------------------------------------------------------------------------------------


   PROCEDURE update_uda_attributes (i_change_order    IN            VARCHAR2,
                                    i_change_type     IN            VARCHAR2, -- Added by Vamshi in rev 6.0 @ TMS#20150807-00170
                                    i_change_id       IN            NUMBER,
                                    i_inv_item_id     IN            NUMBER,
                                    i_org_id          IN            NUMBER,
                                    o_error_code         OUT NOCOPY VARCHAR2,
                                    o_error_message      OUT NOCOPY VARCHAR2)
   IS
      /*******************************************************************************************************************************
               PROCEDURE : update_uda_attributes

                 REVISIONS:
                 Ver        Date        Author                     Description
                 ---------  ----------  ---------------    ----------------------------------------------------------------------------------
                 1.0        01-MAR-2015   Shankar Vanga     TMS#20150126-00044  - Create Single Item Workflow Tool
                                                            Initial Version

                 2.0        26-May-2015   P.vamshidhar      TMS#20150519-00091 - Single Item UI Enhancements
                                                            Country of origin storing in C_EXT_ATTR2.

                 4.0         22-Jun-2015   P.Vamshidhar     TMS#20150608-00031  - Single Item UI Enhancements.
                                                            Added code to populate below UDA attributes.
                                                            Fullsize Image    - XXWC_A_F_1_ATTR   - C_EXT_ATTR15
                                                            Thumbnail Image   - XXWC_A_T_1_ATTR   - C_EXT_ATTR20

                 5.0         13-Aug-2015   P.Vamshidhar     TMS#20150708-00061  - PCAM enhancements part 3
                                                            Added code to process data related cyber tax code
                                                            Cyber Source Tax code  -  C_EXT_ATTR11
                                                            Fullfillment           -  C_EXT_ATTR7

                 6.0         24-Sep-2015   P.Vamshidhar     TMS#20150807-00170 -- PDH - Add UDA in PDH for Last Verified Date

      *********************************************************************************************************************************/

      l_procedure                VARCHAR2 (100) := 'UPDATE_UDA_ATTRIBUTES';

      CURSOR cur_uda_attr (
         c_change_id   IN NUMBER,
         c_inv_id      IN NUMBER,
         c_org_id      IN NUMBER)
      IS
           SELECT EXTENSION_ID,
                  ORGANIZATION_ID,
                  INVENTORY_ITEM_ID,
                  ITEM_CATALOG_GROUP_ID,
                  ATTR_GROUP_ID,
                  DATA_LEVEL_ID,
                  C_EXT_ATTR1,
                  C_EXT_ATTR2,   -- Added by Vamshi in V2.0 TMS#20150519-00091
                  C_EXT_ATTR3,
                  C_EXT_ATTR6,
                  C_EXT_ATTR7,
                  C_EXT_ATTR9,
                  C_EXT_ATTR10,
                  C_EXT_ATTR11,
                  C_EXT_ATTR14,
                  C_EXT_ATTR15,
                  C_EXT_ATTR20,  -- Added by Vamshi in V4.0 TMS#20150608-00031
                  TRUNC (SYSDATE) D_EXT_ATTR1,  -- Added by Vamshi in Rev. 6.0
                  CHANGE_ID,
                  TL_EXT_ATTR2
             FROM xxwc.xxwc_chg_ego_item_attrs
            WHERE     CHANGE_ID = c_change_id
                  AND INVENTORY_ITEM_ID = c_inv_id
                  AND ORGANIZATION_ID = c_org_id
         ORDER BY attr_group_id;

      l_error_message_list       Error_handler.error_tbl_type;

      x_failed_row_id_list       VARCHAR2 (1000);
      x_return_status            VARCHAR2 (100);
      x_msg_count                NUMBER;
      x_msg_data                 VARCHAR2 (10000);
      x_error_code               NUMBER;

      l_attr_row_table           EGO_USER_ATTR_ROW_TABLE;
      l_attr_data_table          EGO_USER_ATTR_DATA_TABLE;
      l_row_counter              NUMBER;
      l_error_code               VARCHAR2 (1) := 'S';
      l_err_msg                  VARCHAR2 (2000);
      l_err_message              VARCHAR2 (4000);
      l_transaction_type         VARCHAR2 (50) := 'UPDATE';
      x_errorcode                VARCHAR2 (4000);
      l_err_message1             VARCHAR2 (4000);
      l_attr_grp1_name           VARCHAR2 (80) := 'XXWC_INTERNAL_ATTR_AG';
      l_attr_grp2_name           VARCHAR2 (80) := 'XXWC_WHITE_CAP_MST_AG';
      l_attr_group_id            ego_fnd_dsc_flx_ctx_ext.attr_group_id%TYPE;
      l_appl_id                  FND_APPLICATION.APPLICATION_ID%TYPE := 431;
      l_attr_group_name          VARCHAR2 (500);
      ---------------------------------------------------
      -- UDA
      ---------------------------------------------------
      l_row_identifier           NUMBER;
      l_attr_row_index           NUMBER := 1;
      l_attr_data_index          NUMBER := 1;

      TYPE item_uda_rec_type IS RECORD
      (
         attr_grp_name     VARCHAR2 (30),
         attr_name         VARCHAR2 (30),
         attr_value_str    VARCHAR2 (155),
         attr_value_num    NUMBER,
         attr_value_date   DATE
      );

      TYPE item_uda_tbl_type IS TABLE OF item_uda_rec_type
         INDEX BY PLS_INTEGER;

      l_attr_grp_tbl             item_uda_tbl_type;

      TYPE attr_group_tbl IS TABLE OF apps.EGO_ITM_USR_ATTR_INTRFC%ROWTYPE
         INDEX BY PLS_INTEGER;

      l_attrs_tbl                attr_group_tbl;
      i                          NUMBER := 0;
      l_item_id                  NUMBER;
      l_org_id                   NUMBER;
      l_conflict_material_flag   VARCHAR2 (10);
      lvc_prior_country_origin   VARCHAR2 (100); -- Added by Vamshi in V6.0 TMS#20150807-00170
   BEGIN
      l_attr_row_index := 0;
      l_attr_data_index := 0;
      l_attr_data_table := EGO_USER_ATTR_DATA_TABLE ();
      l_attr_row_table := EGO_USER_ATTR_ROW_TABLE ();

      FOR cur_uda_attr_data
         IN cur_uda_attr (c_change_id   => i_change_id,
                          c_inv_id      => i_inv_item_id,
                          c_org_id      => i_org_id)
      LOOP
         Error_Handler.Initialize;


         -- Website Attributes
         SELECT UPPER (attr_group_name), attr_group_id, application_id
           INTO l_attr_group_name, l_attr_group_id, l_appl_id
           FROM EGO_ATTR_GROUPS_V
          WHERE attr_group_id = cur_uda_attr_data.ATTR_GROUP_ID;

         IF l_attr_group_name = l_attr_grp1_name
         THEN
            l_attr_row_table.EXTEND;
            l_attr_row_index := l_attr_row_table.COUNT;
            l_row_identifier := l_attr_row_index;
            l_attr_row_table (l_attr_row_index) :=
               ego_user_attr_row_obj (l_row_identifier,
                                      l_attr_group_id,
                                      l_appl_id,
                                      'EGO_ITEMMGMT_GROUP',
                                      l_attr_group_name,
                                      'ITEM_LEVEL',
                                      NULL,
                                      NULL,
                                      NULL,
                                      NULL,
                                      NULL,
                                      'SYNC');


            l_attr_data_table.EXTEND;
            l_attr_data_index := l_attr_data_table.COUNT;
            l_attr_data_table (l_attr_data_index) :=
               ego_user_attr_data_obj (l_row_identifier,
                                       'XXWC_VENDOR_NUMBER_ATTR',
                                       cur_uda_attr_data.C_EXT_ATTR1,
                                       NULL,
                                       NULL,
                                       NULL,
                                       NULL,
                                       l_row_identifier);

            -- Added Below code by Vamshi in V2.0  TMS#20150519-00091 -- Start

            IF cur_uda_attr_data.C_EXT_ATTR2 IS NOT NULL
            THEN
               l_attr_data_table.EXTEND;
               l_attr_data_index := l_attr_data_table.COUNT;
               l_attr_data_table (l_attr_data_index) :=
                  ego_user_attr_data_obj (
                     l_row_identifier,
                     'XXWC_COUNTRY_ORIGIN_ATTR',
                     SUBSTR (cur_uda_attr_data.C_EXT_ATTR2, 1, 2),
                     NULL,
                     NULL,
                     NULL,
                     NULL,
                     l_row_identifier);

               -- Added below code by Vamshi in V6.0 TMS#20150807-00170

               IF i_change_type = 'CHANGE_ORDER'
               THEN
                  BEGIN
                     SELECT XXWC_COUNTRY_ORIGIN_ATTR_DISP
                       INTO lvc_prior_country_origin
                       FROM XXWC_EGO_INTRNAL_AT_ATTR_AGV
                      WHERE     INVENTORY_ITEM_ID = i_inv_item_id
                            AND ORGANIZATION_ID = 222
                            AND ROWNUM < 2;
                  EXCEPTION
                     WHEN NO_DATA_FOUND
                     THEN
                        lvc_prior_country_origin := NULL;
                  END;


                  IF NVL (SUBSTR (lvc_prior_country_origin, 1, 2), 'A1') <>
                        NVL (SUBSTR (cur_uda_attr_data.C_EXT_ATTR2, 1, 2),
                             'A1')
                  THEN
                     l_attr_data_table.EXTEND;
                     l_attr_data_index := l_attr_data_table.COUNT;
                     l_attr_data_table (l_attr_data_index) :=
                        ego_user_attr_data_obj (
                           l_row_identifier,
                           'XXWC_COO_LAST_VERIFIED_DT_ATTR',
                           NULL,
                           NULL,
                           cur_uda_attr_data.D_EXT_ATTR1,
                           NULL,
                           NULL,
                           l_row_identifier);
                  END IF;
               END IF;
            -- Added above code by Vamshi in V6.0 TMS#20150807-00170
            END IF;

            -- Added above code by Vamshi in V2.0  TMS#20150519-00091 -- End.



            l_attr_data_table.EXTEND;
            l_attr_data_index := l_attr_data_table.COUNT;
            l_attr_data_table (l_attr_data_index) :=
               ego_user_attr_data_obj (l_row_identifier,
                                       'XXWC_SUGGESTED_RETAIL_PRICE',
                                       cur_uda_attr_data.C_EXT_ATTR9,
                                       NULL,
                                       NULL,
                                       NULL,
                                       NULL,
                                       l_row_identifier);

            l_attr_data_table.EXTEND;
            l_attr_data_index := l_attr_data_table.COUNT;
            l_attr_data_table (l_attr_data_index) :=
               ego_user_attr_data_obj (l_row_identifier,
                                       'XXWC_RETAIL_UOM_ATTR',
                                       cur_uda_attr_data.C_EXT_ATTR10,
                                       NULL,
                                       NULL,
                                       NULL,
                                       NULL,
                                       l_row_identifier);

            IF cur_uda_attr_data.C_EXT_ATTR11 = 'N'
            THEN
               l_conflict_material_flag := 'No';
            ELSE
               l_conflict_material_flag := 'Yes';
            END IF;

            l_attr_data_table.EXTEND;
            l_attr_data_index := l_attr_data_table.COUNT;
            l_attr_data_table (l_attr_data_index) :=
               ego_user_attr_data_obj (l_row_identifier,
                                       'XXWC_CONFLICT_MINERALS_ATTR',
                                       l_conflict_material_flag,
                                       NULL,
                                       NULL,
                                       NULL,
                                       NULL,
                                       l_row_identifier);

            l_attr_data_table.EXTEND;
            l_attr_data_index := l_attr_data_table.COUNT;
            l_attr_data_table (l_attr_data_index) :=
               ego_user_attr_data_obj (l_row_identifier,
                                       'XXWC_ITEM_LEVEL_ATTR',
                                       cur_uda_attr_data.C_EXT_ATTR6,
                                       NULL,
                                       NULL,
                                       NULL,
                                       NULL,
                                       l_row_identifier);


            -- Added below code by Vamshi in TMS#20150708-00061 -  FULFILLMENT -- Start

            l_attr_data_table.EXTEND;
            l_attr_data_index := l_attr_data_table.COUNT;
            l_attr_data_table (l_attr_data_index) :=
               ego_user_attr_data_obj (l_row_identifier,
                                       'XXWC_FULFILLMENT_ATTR',
                                       cur_uda_attr_data.C_EXT_ATTR7,
                                       NULL,
                                       NULL,
                                       NULL,
                                       NULL,
                                       l_row_identifier);
         -- Added above code by Vamshi in TMS#20150708-00061 -  FULFILLMENT -- End



         END IF;                --IF l_attr_group_name = l_attr_grp1_name THEN

         -- White Cap Internal Attributes
         IF     (l_attr_group_name = l_attr_grp2_name)
            AND (   cur_uda_attr_data.C_EXT_ATTR14 IS NOT NULL
                 OR cur_uda_attr_data.C_EXT_ATTR6 IS NOT NULL
                 OR cur_uda_attr_data.C_EXT_ATTR7 IS NOT NULL
                 OR cur_uda_attr_data.TL_EXT_ATTR2 IS NOT NULL
                 OR cur_uda_attr_data.C_EXT_ATTR3 IS NOT NULL
                 OR cur_uda_attr_data.C_EXT_ATTR15 IS NOT NULL
                 OR cur_uda_attr_data.C_EXT_ATTR20 IS NOT NULL -- Added by Vamshi in V4.0 - TMS#20150608-00031
                 OR cur_uda_attr_data.C_EXT_ATTR1 IS NOT NULL
                 OR cur_uda_attr_data.C_EXT_ATTR11 IS NOT NULL) -- Added by Vamshi in V6.0 - TMS#20150708-00061 )
         THEN
            l_attr_row_table.EXTEND;
            l_attr_row_index := l_attr_row_table.COUNT;
            l_row_identifier := l_attr_row_index;
            l_attr_row_table (l_attr_row_index) :=
               ego_user_attr_row_obj (l_row_identifier,
                                      l_attr_group_id,
                                      l_appl_id,
                                      'EGO_ITEMMGMT_GROUP',
                                      l_attr_group_name,
                                      'ITEM_LEVEL',
                                      NULL,
                                      NULL,
                                      NULL,
                                      NULL,
                                      NULL,
                                      'SYNC');

            l_attr_data_table.EXTEND;
            l_attr_data_index := l_attr_data_table.COUNT;
            l_attr_data_table (l_attr_data_index) :=
               ego_user_attr_data_obj (l_row_identifier,
                                       'XXWC_W_S_D_ATTR',
                                       cur_uda_attr_data.C_EXT_ATTR14,
                                       NULL,
                                       NULL,
                                       NULL,
                                       NULL,
                                       l_row_identifier);

            l_attr_data_table.EXTEND;
            l_attr_data_index := l_attr_data_table.COUNT;
            l_attr_data_table (l_attr_data_index) :=
               ego_user_attr_data_obj (l_row_identifier,
                                       'XXWC_M_P_N_ATTR',
                                       cur_uda_attr_data.C_EXT_ATTR6,
                                       NULL,
                                       NULL,
                                       NULL,
                                       NULL,
                                       l_row_identifier);

            l_attr_data_table.EXTEND;
            l_attr_data_index := l_attr_data_table.COUNT;
            l_attr_data_table (l_attr_data_index) :=
               ego_user_attr_data_obj (l_row_identifier,
                                       'XXWC_PRODUCT_LINE_ATTR',
                                       cur_uda_attr_data.C_EXT_ATTR7,
                                       NULL,
                                       NULL,
                                       NULL,
                                       NULL,
                                       l_row_identifier);

            l_attr_data_table.EXTEND;
            l_attr_data_index := l_attr_data_table.COUNT;
            l_attr_data_table (l_attr_data_index) :=
               ego_user_attr_data_obj (l_row_identifier,
                                       'XXWC_WEB_LONG_DESCRIPTION_ATTR',
                                       cur_uda_attr_data.TL_EXT_ATTR2,
                                       NULL,
                                       NULL,
                                       NULL,
                                       NULL,
                                       l_row_identifier);

            l_attr_data_table.EXTEND;
            l_attr_data_index := l_attr_data_table.COUNT;
            l_attr_data_table (l_attr_data_index) :=
               ego_user_attr_data_obj (l_row_identifier,
                                       'XXWC_BULKY_ITEM_ATTR',
                                       cur_uda_attr_data.C_EXT_ATTR3,
                                       NULL,
                                       NULL,
                                       NULL,
                                       NULL,
                                       l_row_identifier);

            l_attr_data_table.EXTEND;
            l_attr_data_index := l_attr_data_table.COUNT;
            l_attr_data_table (l_attr_data_index) :=
               ego_user_attr_data_obj (l_row_identifier,
                                       'XXWC_A_F_1_ATTR',
                                       cur_uda_attr_data.C_EXT_ATTR15,
                                       NULL,
                                       NULL,
                                       NULL,
                                       NULL,
                                       l_row_identifier);

            -- Added below code in v4.0 TMS#20150608-00031
            l_attr_data_table.EXTEND;
            l_attr_data_index := l_attr_data_table.COUNT;
            l_attr_data_table (l_attr_data_index) :=
               ego_user_attr_data_obj (l_row_identifier,
                                       'XXWC_A_T_1_ATTR',
                                       cur_uda_attr_data.C_EXT_ATTR20,
                                       NULL,
                                       NULL,
                                       NULL,
                                       NULL,
                                       l_row_identifier);

            l_attr_data_table.EXTEND;
            l_attr_data_index := l_attr_data_table.COUNT;
            l_attr_data_table (l_attr_data_index) :=
               ego_user_attr_data_obj (l_row_identifier,
                                       'XXWC_BRAND_ATTR',
                                       cur_uda_attr_data.C_EXT_ATTR1,
                                       NULL,
                                       NULL,
                                       NULL,
                                       NULL,
                                       l_row_identifier);

            -- Added below code by Vamshi in TMS#20150708-00061 - Cyber Source Tax code -- Start


            l_attr_data_table.EXTEND;
            l_attr_data_index := l_attr_data_table.COUNT;
            l_attr_data_table (l_attr_data_index) :=
               ego_user_attr_data_obj (l_row_identifier,
                                       'XXWC_TAX_CODE_ATTR',
                                       cur_uda_attr_data.C_EXT_ATTR11,
                                       NULL,
                                       NULL,
                                       NULL,
                                       NULL,
                                       l_row_identifier);
         -- Added above code by Vamshi in TMS#20150708-00061 -- End


         END IF;                --IF l_attr_group_name = l_attr_grp2_name THEN

         l_item_id := cur_uda_attr_data.INVENTORY_ITEM_ID;
         l_org_id := cur_uda_attr_data.ORGANIZATION_ID;
      END LOOP;                                        --FOR cur_uda_attr_data

      --print_uda_data(l_attr_row_table,l_attr_data_table);
      EGO_ITEM_PUB.PROCESS_USER_ATTRS_FOR_ITEM (
         P_API_VERSION             => '1.0',
         P_INVENTORY_ITEM_ID       => l_item_id,
         P_ORGANIZATION_ID         => l_org_id,
         P_ATTRIBUTES_ROW_TABLE    => l_attr_row_table,
         P_ATTRIBUTES_DATA_TABLE   => l_attr_data_table,
         X_FAILED_ROW_ID_LIST      => x_failed_row_id_list,
         X_RETURN_STATUS           => x_return_status,
         X_ERRORCODE               => x_error_code,
         X_MSG_COUNT               => x_msg_count,
         X_MSG_DATA                => x_msg_data);

      DBMS_OUTPUT.put_line (
         'x_failed_row_id_list = ' || x_failed_row_id_list);

      IF x_return_status <> 'S'
      THEN
         Error_Handler.Get_message_list (l_error_message_list);
         l_error_code := 'E';

         FOR i IN 1 .. l_error_message_list.COUNT
         LOOP
            l_err_msg := NULL;
            l_err_msg := l_error_message_list (i).MESSAGE_TEXT;
            l_err_message := l_err_message || l_err_msg;
         END LOOP;

         l_err_message1 := l_err_message1 || l_err_message;
      END IF;                                 --IF x_return_status <> 'S' THEN


      o_error_code := l_error_code;
      o_error_message := l_err_message1;
   EXCEPTION
      WHEN OTHERS
      THEN
         l_err_msg :=
               l_err_msg
            || ' ...Error_Stack...'
            || DBMS_UTILITY.format_error_stack ()
            || ' Error_Backtrace...'
            || DBMS_UTILITY.format_error_backtrace ();

         -- Calling ERROR API
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || l_procedure,
            p_ora_error_msg       => SUBSTR (l_err_msg, 1, 2000),
            p_error_desc          => SUBSTR (l_err_msg, 1, 240),
            p_distribution_list   => g_distro_list,
            p_module              => 'INV');

         o_error_code := 'E';
         o_error_message := 'Error in update_uda_attributes Procedure';
   END update_uda_attributes;

   --------------------------------------------------------------------------------


   PROCEDURE update_attachments (i_change_order    IN            VARCHAR2,
                                 i_change_id       IN            NUMBER,
                                 i_inv_item_id     IN            NUMBER,
                                 i_org_id          IN            NUMBER,
                                 o_error_code         OUT NOCOPY VARCHAR2,
                                 o_error_message      OUT NOCOPY VARCHAR2)
   IS
      /*******************************************************************************************************************************
               PROCEDURE : update_attachments

                 REVISIONS:
                 Ver        Date        Author                     Description
                 ---------  ----------  ---------------    ----------------------------------------------------------------------------------
                 1.0        01-MAR-2015   Shankar Vanga     TMS#20150126-00044  - Create Single Item Workflow Tool
                                                            Initial Version

      *********************************************************************************************************************************/


      l_error_code   VARCHAR2 (1) := 'S';
      l_err_msg      VARCHAR2 (2000);

      l_procedure    VARCHAR2 (100) := 'UPDATE_ATTACHMENTS';
   BEGIN
      ---PK Values changed by Ram, PK values are 1= item, 2= org..
      BEGIN            -- What if you have more attachments for the same item.
         UPDATE FND_ATTACHED_DOCUMENTS
            SET ENTITY_NAME = 'MTL_SYSTEM_ITEMS', pk3_value = NULL
          WHERE     1 = 1
                AND pk1_value = i_org_id
                AND pk2_value = i_inv_item_id
                AND pk3_value = i_change_id
                AND entity_name = 'XXWC_MTL_SY_ITEMS_CHG_B';

         ---Code changes required here for on implement, no delete update an attribute for complete \ implement \ release

         -- Ram reverted back.. else OAF will break..
         DELETE FROM xxwc_mtl_sy_items_chg_b
               WHERE 1 = 1 AND oracle_change_id = i_change_id;

         DELETE FROM xxwc.xxwc_chg_mtl_cross_ref
               WHERE 1 = 1 AND oracle_change_id = i_change_id;

         DELETE FROM xxwc.xxwc_chg_ego_item_attrs
               WHERE 1 = 1 AND change_id = i_change_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_error_code := 'E';
            l_err_msg :=
                  'Error Update-delete in procedure update_attachments for change order'
               || i_change_order;
      END;

      o_error_code := l_error_code;
      o_error_message := l_err_msg;
   EXCEPTION
      WHEN OTHERS
      THEN
         l_err_msg :=
               l_err_msg
            || ' ...Error_Stack...'
            || DBMS_UTILITY.format_error_stack ()
            || ' Error_Backtrace...'
            || DBMS_UTILITY.format_error_backtrace ();

         -- Calling ERROR API
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || l_procedure,
            p_ora_error_msg       => SUBSTR (l_err_msg, 1, 2000),
            p_error_desc          => SUBSTR (l_err_msg, 1, 240),
            p_distribution_list   => g_distro_list,
            p_module              => 'INV');


         o_error_code := 'E';
         o_error_message := 'Error in update_attachments Procedure';
   END update_attachments;

   PROCEDURE Update_Category (i_category_name     IN            VARCHAR2,
                              i_category_values   IN            VARCHAR2,
                              i_inv_item_id       IN            NUMBER,
                              i_org_id            IN            NUMBER,
                              o_error_code           OUT NOCOPY VARCHAR2,
                              o_error_message        OUT NOCOPY VARCHAR2)
   IS
      /*******************************************************************************************************************************
               PROCEDURE : Update_Category

                 REVISIONS:
                 Ver        Date        Author                     Description
                 ---------  ----------  ---------------    ----------------------------------------------------------------------------------
                 1.0        01-MAR-2015   Shankar Vanga     TMS#20150126-00044  - Create Single Item Workflow Tool
                                                            Initial Version
                 9.0        06-Oct-2016   P.Vamshidhar      TMS#20160603-00026 - PCAM catclass value not updating in Oracle

      *********************************************************************************************************************************/

      ln_api_version     NUMBER := 1.0;
      lc_return_status   VARCHAR2 (10);
      ln_msg_count       NUMBER;
      lc_msg_data        VARCHAR2 (500) := NULL;
      ln_error_code      NUMBER;
      l_cat_set_id       NUMBER;
      l_structure_id     NUMBER;
      l_old_cat_id       NUMBER;
      l_cat_id           NUMBER;
      l_error_code       VARCHAR2 (10);
      l_err_msg          VARCHAR2 (2000);
      l_error_mesg       VARCHAR2 (4000);
      l_err_message      VARCHAR2 (4000);
      l_procedure        VARCHAR2 (100) := 'UPDATE_CATEGORY';
   BEGIN
      lc_msg_data := NULL;
      l_err_msg := NULL;
      l_error_code := 'S';
      l_err_message := NULL;
      l_old_cat_id := NULL;
      l_cat_id := NULL;

      ----------------------------
      --Derive the Category Set ID
      ----------------------------
      BEGIN
         SELECT category_set_id, structure_id
           INTO l_cat_set_id, l_structure_id
           FROM mtl_category_sets
          WHERE category_set_name = i_category_name;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_error_code := 'E';
            l_err_msg :=
               'Error in Category Set ID Derivation' || i_category_name;
      END;

      DEBUG_LOG ('Get Category Set ID : ' || l_err_msg);

      l_err_message := l_err_message || ':' || l_err_msg;

      -----------------------------
      --Derive the Old Category ID
      -----------------------------
      BEGIN
         SELECT category_id
           INTO l_old_cat_id
           FROM mtl_item_categories
          WHERE     inventory_item_id = i_inv_item_id
                AND category_set_id = l_cat_set_id
                AND organization_id=222; -- Added in 9.0 Rev by Vamshi.
      EXCEPTION
         WHEN OTHERS
         THEN
            L_OLD_CAT_ID := NULL;
      END;

      l_err_message := NULL;

      ------------------------
      --Derive the Category ID
      ------------------------

      BEGIN
         SELECT category_id
           INTO l_cat_id
           FROM mtl_categories_kfv
          WHERE     concatenated_segments = i_category_values
                AND structure_id = l_structure_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_error_code := 'E';
            l_err_msg :=
               'Error in new Category ID Derivation' || i_category_name;
      END;

      IF l_old_cat_id IS NOT NULL
      THEN
         inv_item_category_pub.update_Category_Assignment (
            p_api_version         => ln_api_version,
            p_init_msg_list       => FND_API.G_TRUE,
            p_commit              => FND_API.G_TRUE,
            p_category_id         => l_cat_id,
            p_old_category_id     => l_old_cat_id,
            p_category_set_id     => l_cat_set_id,
            p_inventory_item_id   => i_inv_item_id,
            p_organization_id     => i_org_id,
            x_return_status       => lc_return_status,
            x_errorcode           => ln_error_code,
            x_msg_count           => ln_msg_count,
            x_msg_data            => lc_msg_data);

         IF lc_return_status != 'S'
         THEN
            l_err_msg := lc_msg_data;
            l_error_code := 'E';
         END IF;

         o_error_code := l_error_code;
         o_error_message := l_err_msg;
      ELSE
         inv_item_category_pub.create_Category_Assignment (
            p_api_version         => ln_api_version,
            p_init_msg_list       => FND_API.G_TRUE,
            p_commit              => FND_API.G_TRUE,
            p_category_id         => l_cat_id,
            p_category_set_id     => l_cat_set_id,
            p_inventory_item_id   => i_inv_item_id,
            p_organization_id     => i_org_id,
            x_return_status       => lc_return_status,
            x_errorcode           => ln_error_code,
            x_msg_count           => ln_msg_count,
            x_msg_data            => lc_msg_data);

         IF lc_return_status != 'S'
         THEN
            l_err_msg := lc_msg_data;
            l_error_code := 'E';
         END IF;

         o_error_code := l_error_code;
         o_error_message := l_err_msg;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         l_err_msg :=
               l_err_msg
            || ' ...Error_Stack...'
            || DBMS_UTILITY.format_error_stack ()
            || ' Error_Backtrace...'
            || DBMS_UTILITY.format_error_backtrace ();

         -- Calling ERROR API
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || l_procedure,
            p_ora_error_msg       => SUBSTR (l_err_msg, 1, 2000),
            p_error_desc          => SUBSTR (l_err_msg, 1, 240),
            p_distribution_list   => g_distro_list,
            p_module              => 'INV');


         o_error_code := 'E';
         O_ERROR_MESSAGE := 'Error in Update_Category Procedure';
         O_ERROR_MESSAGE := SQLCODE || ':' || SQLERRM;
   END Update_Category;

   ---------------------------------------------------------

   PROCEDURE Process_Cross_Ref (i_change_order    IN            VARCHAR2,
                                o_error_code         OUT NOCOPY VARCHAR2,
                                o_error_message      OUT NOCOPY VARCHAR2)
   IS
      /*******************************************************************************************************************************
               PROCEDURE : Process_Cross_Ref

                 REVISIONS:
                 Ver        Date        Author                     Description
                 ---------  ----------  ---------------    ---------------------------------------------------------------------------
                 1.0        01-MAR-2015   Shankar Vanga     TMS#20150126-00044  - Create Single Item Workflow Tool
                                                            Initial Version

                 6.0        24-Sep-2015   P.Vamshidhar      TMS#20150901-00146 - PCAM add delete functionality for Cross References
                                                            and TMS#20150917-00149 -- Cross reference Length issue.
      *********************************************************************************************************************************/


      l_api_version      NUMBER := 1.0;
      l_init_msg_list    VARCHAR2 (2) := FND_API.G_TRUE;
      l_commit           VARCHAR2 (2) := FND_API.G_TRUE;
      l_XRef_tbl         MTL_CROSS_REFERENCES_PUB.XRef_Tbl_Type;
      x_message_list     Error_Handler.Error_Tbl_Type;
      x_return_status    VARCHAR2 (2);
      x_msg_count        NUMBER := 0;
      l_cross_ref_id     NUMBER;
      l_error_code       VARCHAR2 (10) := 'S';
      l_err_msg          VARCHAR2 (2000);
      l_error_message    VARCHAR2 (4000);
      l_error_message1   VARCHAR2 (4000);
      l_cross_exists     NUMBER;
      l_procedure        VARCHAR2 (100) := 'PROCESS_CROSS_REF';

      -- Added below variables in TMS#20150901-00146
      ln_change_id       eng_engineering_changes.change_id%TYPE;
      lvc_change_type    eng_engineering_changes.change_mgmt_type_code%TYPE;

      CURSOR cur_item_cross_ref (
         c_change_id   IN NUMBER)    -- Added in TMS#20150901-00146  - Rev.6.0
      --c_change_order   IN VARCHAR2) -- Commented in TMS#20150901-00146  - Rev.6.0
      IS
         SELECT xref.change_id,
                chg.change_Notice change_order,
                xref.inventory_item_id,
                msi.segment1 item_number,
                cross_Reference_type,
                SUBSTR (cross_Reference, 1, 25) cross_Reference, -- Added Substr in TMS#20150917-00149
                xref.attribute1 vendor,
                xref.cross_reference_id,
                NULL mfg_delete_flag, -- Added in TMS#20150901-00146  - Rev.6.0
                NULL bcode_delete_flag -- Added in TMS#20150901-00146  - Rev.6.0
           FROM xxwc.XXWC_CHG_MTL_CROSS_REF xref,
                apps.mtl_system_items msi,
                eng_engineering_changes chg
          WHERE     xref.change_id = chg.change_id
                AND msi.inventory_item_id = xref.inventory_item_id
                AND msi.organization_id = xref.organization_id
                --AND chg.change_notice = c_change_order;   -- Commented in TMS#20150901-00146  - Rev.6.0
                AND chg.change_id = c_change_id -- Added in TMS#20150901-00146  - Rev.6.0
         -- Added below query in Rev 6.0
         UNION
         SELECT xpcd.change_id,
                xpcd.change_notice change_order,
                xref.inventory_item_id,
                xref.item_number,
                xpcd.cross_ref_type cross_Reference_type,
                SUBSTR (xpcd.cross_ref, 1, 25) cross_Reference,
                xpcd.vendor,
                NULL cross_reference_id,
                xpcd.mfg_delete_flag mfg_delete_flag,
                xpcd.bcode_delete_flag
           FROM xxwc.xxwc_pcam_creff_data_purge xpcd,
                apps.eng_engineering_changes egc,
                (SELECT a.inventory_item_id, segment1 item_number
                   FROM xxwc.xxwc_chg_mtl_cross_ref a,
                        apps.mtl_system_items_b b
                  WHERE     a.inventory_item_id = b.inventory_item_id
                        AND b.organization_id = 222
                        AND change_id = c_change_id
                        AND ROWNUM < 2) xref
          WHERE     NVL (xpcd.change_id, 0) = egc.change_id
                AND egc.CHANGE_MGMT_TYPE_CODE = 'CHANGE_ORDER'
                AND egc.change_id = c_change_id;
   BEGIN
      BEGIN
         SELECT COUNT (1)
           INTO l_cross_exists
           FROM XXWC_MTL_SY_ITEMS_CHG_B item, eng_engineering_changes chg
          WHERE     item.oracle_change_id = chg.change_id
                AND chg.change_notice = i_change_order;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_error_code := 'E';
            l_err_msg :=
                  'Cross Reference derivation Failed for Change Order:'
               || i_change_order;
      END;


      -- Added Below code by Vamshi in TMS#20150901-00146 @ 6.0 Rev. -- Start
      BEGIN
         SELECT change_id, change_mgmt_type_code
           INTO ln_change_id, lvc_change_type
           FROM eng_engineering_changes chg
          WHERE chg.change_notice = i_change_order;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_error_code := 'E';
            l_err_msg :=
                  'Cross Reference derivation Failed for Change Order Id:'
               || i_change_order;
      END;

      -- Added Above code by Vamshi in TMS#20150901-00146 @ 6.0 Rev. -- End


      IF l_cross_exists > 0 AND ln_change_id IS NOT NULL -- Added by Vamshi in TMS#20150901-00146 @ 6.0 Rev.
      THEN
         L_ERROR_MESSAGE := NULL;
         l_error_code := NULL;

         FOR cur_item_cross_ref_data --IN cur_item_cross_ref (c_change_order => i_change_order) -- Commented by Vamshi in 6.0
            IN cur_item_cross_ref (c_change_id => ln_change_id) -- Added by Vamshi in 6.0
         LOOP
            --------------------------------
            --Check if that Cross Ref exists
            --------------------------------
            l_error_code := NULL;
            l_err_msg := NULL;
            l_cross_ref_id := NULL; -- Added by Vamshi TMS#20150608-00031 -- V4.0

            BEGIN
               SELECT cross_reference_id
                 INTO l_cross_ref_id
                 FROM mtl_cross_references
                WHERE     inventory_item_id =
                             cur_item_cross_ref_data.inventory_item_id
                      AND cross_reference_type =
                             cur_item_cross_ref_data.cross_Reference_type
                      AND cross_reference =
                             cur_item_cross_ref_data.cross_Reference;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_error_code := 'E';
                  l_err_msg :=
                        'Cross Reference ID Derivation failed for Cross Reference :'
                     || cur_item_cross_ref_data.cross_Reference_type
                     || cur_item_cross_ref_data.cross_Reference;
            END;

            IF l_cross_ref_id IS NOT NULL
            THEN
               IF    (    cur_item_cross_ref_data.cross_Reference_type =
                             'VENDOR'
                      AND cur_item_cross_ref_data.mfg_delete_flag IS NOT NULL)
                  OR                 -- Added in TMS#20150901-00146  - Rev.6.0
                     (    cur_item_cross_ref_data.cross_Reference_type <>
                             'VENDOR'
                      AND cur_item_cross_ref_data.bcode_delete_flag
                             IS NOT NULL)
               THEN                  -- Added in TMS#20150901-00146  - Rev.6.0
                  l_XRef_tbl (1).Transaction_Type := 'DELETE'; -- Added in TMS#20150901-00146  - Rev.6.0
               ELSE                  -- Added in TMS#20150901-00146  - Rev.6.0
                  l_XRef_tbl (1).Transaction_Type := 'UPDATE';
               END IF;               -- Added in TMS#20150901-00146  - Rev.6.0
            ELSE
               l_XRef_tbl (1).Transaction_Type := 'CREATE';
            END IF;

            l_XRef_tbl (1).Cross_Reference :=
               cur_item_cross_ref_data.cross_Reference;
            l_XRef_tbl (1).Cross_Reference_id :=
               cur_item_cross_ref_data.cross_reference_id;
            l_XRef_tbl (1).Cross_Reference_Type :=
               cur_item_cross_ref_data.cross_Reference_type;
            l_XRef_tbl (1).Inventory_Item_Id :=
               cur_item_cross_ref_data.inventory_item_id;
            l_XRef_tbl (1).Org_Independent_Flag := 'Y';
            l_XRef_tbl (1).attribute1 := cur_item_cross_ref_data.vendor;

            -- Added in TMS#20150901-00146  - Rev.6.0 -- Start
            IF     (   l_XRef_tbl (1).Transaction_Type = 'DELETE'
                    OR l_XRef_tbl (1).Transaction_Type = 'UPDATE')
               AND lvc_change_type = 'CHANGE_ORDER'
            THEN
               l_XRef_tbl (1).Cross_Reference_id := l_cross_ref_id;
            END IF;

            -- Added in TMS#20150901-00146  - Rev.6.0 -- End

            MTL_CROSS_REFERENCES_PUB.Process_XRef (
               p_api_version     => l_api_version,
               p_init_msg_list   => l_init_msg_list,
               p_commit          => l_commit,
               p_XRef_Tbl        => l_XRef_tbl,
               x_return_status   => x_return_status,
               x_msg_count       => x_msg_count,
               x_message_list    => x_message_list);

            L_ERROR_CODE := X_RETURN_STATUS;
            L_ERROR_MESSAGE := NULL;

            IF X_RETURN_STATUS != 'S'
            THEN
               Error_Handler.GET_MESSAGE_LIST (
                  x_message_list   => x_message_list);

               FOR i IN 1 .. x_message_list.COUNT
               LOOP
                  L_ERROR_MESSAGE :=
                        L_ERROR_MESSAGE
                     || '|'
                     || X_MESSAGE_LIST (I).MESSAGE_TEXT;
               END LOOP;
            -- Added in TMS#20150901-00146  - Rev.6.0 -- Start
            ELSIF X_RETURN_STATUS = 'S'
            THEN
               IF    cur_item_cross_ref_data.mfg_delete_flag IS NOT NULL
                  OR cur_item_cross_ref_data.bcode_delete_flag IS NOT NULL
               THEN
                  DELETE FROM XXWC.XXWC_PCAM_CREFF_DATA_PURGE
                        WHERE     CHANGE_ID = ln_change_id
                              AND CROSS_REF =
                                     cur_item_cross_ref_data.cross_Reference;
               END IF;
            -- Added in TMS#20150901-00146  - Rev.6.0 -- End
            END IF;
         END LOOP;                              --FOR  cur_item_cross_ref_data
      END IF;                                     --IF l_cross_exists > 0 THEN

      o_error_code := l_error_code;
      o_error_message := l_error_message1;
   EXCEPTION
      WHEN OTHERS
      THEN
         l_err_msg :=
               l_err_msg
            || ' ...Error_Stack...'
            || DBMS_UTILITY.format_error_stack ()
            || ' Error_Backtrace...'
            || DBMS_UTILITY.format_error_backtrace ();

         -- Calling ERROR API
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || l_procedure,
            p_ora_error_msg       => SUBSTR (l_err_msg, 1, 2000),
            p_error_desc          => SUBSTR (l_err_msg, 1, 240),
            p_distribution_list   => g_distro_list,
            p_module              => 'INV');

         o_error_code := 'E';
         o_error_message := 'Procedure Process_Cross_Ref Failed';
   END PROCESS_CROSS_REF;

   ------------------------------------------------------------


   PROCEDURE Process_item (i_change_order    IN            VARCHAR2,
                           o_error_code         OUT NOCOPY VARCHAR2,
                           o_error_message      OUT NOCOPY VARCHAR2)
   IS
      /*******************************************************************************************************************************
               PROCEDURE : Process_item

                 REVISIONS:
                 Ver        Date        Author                     Description
                 ---------  ----------  ---------------    ----------------------------------------------------------------------------------
                 1.0        01-MAR-2015   Shankar Vanga     TMS#20150126-00044  - Create Single Item Workflow Tool
                                                            Initial Version
                 2.0        26-May-2015   P.vamshidhar      TMS#20150519-00091 - Single Item UI Enhancements
                                                            Cas_number new column added to process_items procedure

                 6.0        11-Aug-2015   P.Vamshidhar      TMS#20141006-00021 - Description and Shelf Life changes not consistently
                                                            propagating to lower level Orgs from Item Master
                                                            added LBS to if WEIGHT_UOM_CODE is null
                 7.0        16-Nov-2015   P.Vamshidhar      TMS#20151109-00106 - Issue while updating WEIGHT_UOM_CODE.  Hence moved code into
                                                            XXWC_PIM_CHG_ITEM_MGMT

                 8.0        04-Dec-2015   P.Vamshidhar      TMS#20151202-00192 - PCAM issue with UN Number and Hazard Class

      *********************************************************************************************************************************/


      l_INVENTORY_ITEM_ID    mtl_system_items_b.inventory_item_id%TYPE;
      l_ORG_ID               mtl_system_items_b.organization_id%TYPE;
      l_error_message_list   Error_handler.error_tbl_type;
      x_return_status        VARCHAR2 (1000);
      x_msg_count            NUMBER;
      l_item_table           EGO_Item_PUB.Item_Tbl_Type;
      x_item_table           EGO_Item_PUB.Item_Tbl_Type;
      l_err_msg              VARCHAR2 (2000);
      l_err_message          VARCHAR2 (4000);
      l_err_message1         VARCHAR2 (4000);
      l_error_code           VARCHAR2 (10) := 'S';
      l_item_exists          NUMBER;

      l_procedure            VARCHAR2 (100) := 'PROCESS_ITEM';

      CURSOR cur_item_update (
         c_change_order    VARCHAR2)
      IS
         SELECT item.oracle_change_id,
                item.inventory_item_id,
                item.organization_id,
                chg.change_notice change_order,
                item.segment1 item_number,
                item.description,
                item.item_type                                    -- Item Type
                              ,
                DECODE (global_attribute4,
                        'CI', 'Active',
                        item.inventory_item_status_code)
                   inventory_item_status_code                   -- Item Status
                                             ,
                item.primary_uom_code                                    --UOM
                                     ,
                item.shelf_life_days,
                item.hazardous_material_flag                     --HazMat Item
                                            ,
                item.unit_weight                                     -- Weight
                                ,
                item.list_price_per_unit                             --Po Cost
                                        ,
                item.long_description,
                NVL (item.weight_uom_code, 'LBS') weight_uom_code --Shipping Weight UOM    -- Added nvl by Vamshi in TMS#20141006-00021
                                                                 ,
                item.last_update_date,
                item.last_updated_by,
                item.last_update_login,
                item.attribute1                                    --CA Prop65
                               ,
                item.attribute2,
                item.attribute3                            --CA Pesticide Flag
                               ,
                item.attribute4                                      --VOC G/L
                               ,
                item.attribute5                              --Pesticide State
                               ,
                item.attribute6                                 --VOC Category
                               ,
                item.attribute7                             --VOC Sub Category
                               ,
                item.attribute8                                        --MSDS#
                               ,
                item.attribute9                              --Packaging Group
                               ,
                item.attribute10,
                item.attribute11                                   --ORMD Flag
                                ,
                item.attribute12,
                item.attribute13,
                item.attribute14,
                item.attribute15,
                item.attribute16,
                item.attribute17                          --Hazmat Description
                                ,
                item.attribute18                             --Contractor Type
                                ,
                item.attribute19,
                item.attribute20,
                item.attribute21,
                item.attribute22                               -- Taxware Code
                                ,
                item.attribute23,
                item.attribute24,
                item.attribute25,
                item.attribute26,
                item.attribute27,
                item.attribute28,
                item.attribute29,
                ITEM.ATTRIBUTE30,
                NVL (ITEM.ATTRIBUTE_CATEGORY, 'WC') ATTRIBUTE_CATEGORY,
                ITEM.ITEM_CATALOG_GROUP_ID,
                GLOBAL_ATTRIBUTE4,
                item.CAS_NUMBER, -- Added by Vamshi in TMS#20150519-00091 V2.0
                HAZARD_CLASS_ID, -- Added by Vamshi in TMS#20151202-00192 V8.0
                UN_NUMBER_ID     -- Added by Vamshi in TMS#20151202-00192 V8.0
           FROM XXWC_MTL_SY_ITEMS_CHG_B item, eng_engineering_changes chg
          WHERE     item.oracle_change_id = chg.change_id
                AND chg.change_notice = c_change_order;
   BEGIN
      Error_Handler.Initialize;

      mo_global.init ('INV');

      BEGIN
         SELECT COUNT (1)
           INTO l_item_exists
           FROM XXWC_MTL_SY_ITEMS_CHG_B item, eng_engineering_changes chg
          WHERE     item.oracle_change_id = chg.change_id
                AND chg.change_notice = i_change_order;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_error_code := 'E';
            l_err_msg :=
               'Item Derivation Failed for Change Order:' || i_change_order;
      END;

      IF l_item_exists > 0
      THEN
         FOR cur_item_update_data
            IN cur_item_update (c_change_order => i_change_order)
         LOOP
            l_item_table (1).Transaction_Type := 'UPDATE';
            L_ITEM_TABLE (1).INVENTORY_ITEM_ID :=
               CUR_ITEM_UPDATE_DATA.INVENTORY_ITEM_ID;
            L_ITEM_TABLE (1).ORGANIZATION_ID :=
               CUR_ITEM_UPDATE_DATA.ORGANIZATION_ID;
            L_ITEM_TABLE (1).ITEM_CATALOG_GROUP_ID :=
               CUR_ITEM_UPDATE_DATA.ITEM_CATALOG_GROUP_ID;

            -- Added by Vamshi in TMS#20141006-00021
            --Commented below code in Rev 7.0 by Vamshi.
            -- XXWC_WEIGHT_UOM_CODE_UPDATE (
            -- i_change_order,
            -- CUR_ITEM_UPDATE_DATA.INVENTORY_ITEM_ID);

            IF cur_item_update_data.Description IS NOT NULL
            THEN
               l_item_table (1).Description :=
                  cur_item_update_data.description;
            END IF;

            IF cur_item_update_data.long_description IS NOT NULL
            THEN
               l_item_table (1).Long_Description :=
                  cur_item_update_data.long_description;
            END IF;

            IF cur_item_update_data.attribute1 IS NOT NULL
            THEN
               l_item_table (1).attribute1 := cur_item_update_data.attribute1;
            END IF;

            IF cur_item_update_data.attribute2 IS NOT NULL
            THEN
               l_item_table (1).attribute2 := cur_item_update_data.attribute2;
            END IF;

            IF cur_item_update_data.attribute3 IS NOT NULL
            THEN
               l_item_table (1).attribute3 := cur_item_update_data.attribute3;
            END IF;

            IF cur_item_update_data.attribute4 IS NOT NULL
            THEN
               l_item_table (1).attribute4 := cur_item_update_data.attribute4;
            END IF;

            IF cur_item_update_data.attribute5 IS NOT NULL
            THEN
               l_item_table (1).attribute5 := cur_item_update_data.attribute5;
            END IF;

            IF cur_item_update_data.attribute6 IS NOT NULL
            THEN
               l_item_table (1).attribute6 := cur_item_update_data.attribute6;
            END IF;

            IF cur_item_update_data.attribute7 IS NOT NULL
            THEN
               l_item_table (1).attribute7 := cur_item_update_data.attribute7;
            END IF;

            IF cur_item_update_data.attribute8 IS NOT NULL
            THEN
               l_item_table (1).attribute8 := cur_item_update_data.attribute8;
            END IF;

            IF cur_item_update_data.attribute9 IS NOT NULL
            THEN
               l_item_table (1).attribute9 := cur_item_update_data.attribute9;
            END IF;

            IF cur_item_update_data.attribute10 IS NOT NULL
            THEN
               l_item_table (1).attribute10 :=
                  cur_item_update_data.attribute10;
            END IF;

            IF cur_item_update_data.attribute11 IS NOT NULL
            THEN
               l_item_table (1).attribute11 :=
                  cur_item_update_data.attribute11;
            END IF;

            IF cur_item_update_data.attribute12 IS NOT NULL
            THEN
               l_item_table (1).attribute12 :=
                  cur_item_update_data.attribute12;
            END IF;

            IF cur_item_update_data.attribute13 IS NOT NULL
            THEN
               l_item_table (1).attribute13 :=
                  cur_item_update_data.attribute13;
            END IF;

            IF cur_item_update_data.attribute14 IS NOT NULL
            THEN
               l_item_table (1).attribute14 :=
                  cur_item_update_data.attribute14;
            END IF;

            IF cur_item_update_data.attribute15 IS NOT NULL
            THEN
               l_item_table (1).attribute15 :=
                  cur_item_update_data.attribute15;
            END IF;

            IF cur_item_update_data.attribute16 IS NOT NULL
            THEN
               l_item_table (1).attribute16 :=
                  cur_item_update_data.attribute16;
            END IF;

            IF cur_item_update_data.attribute17 IS NOT NULL
            THEN
               l_item_table (1).attribute17 :=
                  cur_item_update_data.attribute17;
            END IF;

            IF cur_item_update_data.attribute18 IS NOT NULL
            THEN
               l_item_table (1).attribute18 :=
                  cur_item_update_data.attribute18;
            END IF;

            IF cur_item_update_data.attribute19 IS NOT NULL
            THEN
               l_item_table (1).attribute19 :=
                  cur_item_update_data.attribute19;
            END IF;

            IF cur_item_update_data.attribute20 IS NOT NULL
            THEN
               l_item_table (1).attribute20 :=
                  cur_item_update_data.attribute20;
            END IF;

            IF cur_item_update_data.attribute21 IS NOT NULL
            THEN
               l_item_table (1).attribute21 :=
                  cur_item_update_data.attribute21;
            END IF;

            IF cur_item_update_data.attribute22 IS NOT NULL
            THEN
               l_item_table (1).attribute22 :=
                  cur_item_update_data.attribute22;
            END IF;

            IF cur_item_update_data.attribute23 IS NOT NULL
            THEN
               l_item_table (1).attribute23 :=
                  cur_item_update_data.attribute23;
            END IF;

            IF cur_item_update_data.attribute24 IS NOT NULL
            THEN
               l_item_table (1).attribute24 :=
                  cur_item_update_data.attribute24;
            END IF;

            IF cur_item_update_data.attribute25 IS NOT NULL
            THEN
               l_item_table (1).attribute25 :=
                  cur_item_update_data.attribute25;
            END IF;

            IF cur_item_update_data.attribute26 IS NOT NULL
            THEN
               l_item_table (1).attribute26 :=
                  cur_item_update_data.attribute26;
            END IF;

            IF cur_item_update_data.attribute27 IS NOT NULL
            THEN
               l_item_table (1).attribute27 :=
                  cur_item_update_data.attribute27;
            END IF;

            IF cur_item_update_data.attribute28 IS NOT NULL
            THEN
               l_item_table (1).attribute28 :=
                  cur_item_update_data.attribute28;
            END IF;

            IF cur_item_update_data.attribute29 IS NOT NULL
            THEN
               l_item_table (1).attribute29 :=
                  cur_item_update_data.attribute29;
            END IF;

            IF cur_item_update_data.attribute30 IS NOT NULL
            THEN
               l_item_table (1).attribute30 :=
                  cur_item_update_data.attribute30;
            END IF;

            IF cur_item_update_data.attribute_category IS NOT NULL
            THEN
               l_item_table (1).attribute_category :=
                  cur_item_update_data.attribute_category;
            END IF;

            IF CUR_ITEM_UPDATE_DATA.ITEM_TYPE IS NOT NULL
            THEN
               L_ITEM_TABLE (1).ITEM_TYPE := CUR_ITEM_UPDATE_DATA.ITEM_TYPE;

               IF CUR_ITEM_UPDATE_DATA.GLOBAL_ATTRIBUTE4 = 'CI'
               THEN
                  IF CUR_ITEM_UPDATE_DATA.ITEM_TYPE = 'NON-STOCK'
                  THEN
                     L_ITEM_TABLE (1).TEMPLATE_NAME := 'New Item - Non-Stock';
                  ELSIF CUR_ITEM_UPDATE_DATA.ITEM_TYPE = 'RENTAL'
                  THEN
                     L_ITEM_TABLE (1).TEMPLATE_NAME := 'New Item - Rental';
                  ELSIF CUR_ITEM_UPDATE_DATA.ITEM_TYPE = 'REPAIR'
                  THEN
                     L_ITEM_TABLE (1).TEMPLATE_NAME := 'New Item - Repair';
                  END IF;
               END IF;
            END IF;

            IF cur_item_update_data.inventory_item_status_code IS NOT NULL
            THEN
               l_item_table (1).inventory_item_status_code :=
                  cur_item_update_data.inventory_item_status_code;
            END IF;

            IF cur_item_update_data.primary_uom_code IS NOT NULL
            THEN
               l_item_table (1).primary_uom_code :=
                  cur_item_update_data.primary_uom_code;
            END IF;

            IF cur_item_update_data.shelf_life_days IS NOT NULL
            THEN
               l_item_table (1).shelf_life_days :=
                  cur_item_update_data.shelf_life_days;
            END IF;

            IF cur_item_update_data.hazardous_material_flag IS NOT NULL
            THEN
               l_item_table (1).hazardous_material_flag :=
                  cur_item_update_data.hazardous_material_flag;
            END IF;

            IF cur_item_update_data.unit_weight IS NOT NULL
            THEN
               l_item_table (1).unit_weight :=
                  cur_item_update_data.unit_weight;
            END IF;

            IF cur_item_update_data.list_price_per_unit IS NOT NULL
            THEN
               l_item_table (1).list_price_per_unit :=
                  cur_item_update_data.list_price_per_unit;
            END IF;

            IF cur_item_update_data.weight_uom_code IS NOT NULL
            THEN
               l_item_table (1).weight_uom_code :=
                  cur_item_update_data.weight_uom_code;
            END IF;

            -- Added Below code by Vamshi at TMS#20150519-00091 in V2.0 -- Start
            IF cur_item_update_data.cas_number IS NOT NULL
            THEN
               l_item_table (1).cas_number := cur_item_update_data.cas_number;
            END IF;

            -- Added above code by Vamshi at TMS#20150519-00091 in V2.0 -- End

            -- Added below code by Vamshi in TMS#20151202-00192 in V8.0 -- Begin

            IF cur_item_update_data.un_number_id IS NOT NULL
            THEN
               l_item_table (1).un_number_id :=
                  cur_item_update_data.un_number_id;
            END IF;

            IF cur_item_update_data.hazard_class_id IS NOT NULL
            THEN
               l_item_table (1).hazard_class_id :=
                  cur_item_update_data.hazard_class_id;
            END IF;

            -- Added below code by Vamshi in TMS#20151202-00192 in V8.0 -- End

            x_return_status := NULL;
            l_error_code := 'N';
            l_err_msg := NULL;
            x_msg_count := 0;
            EGO_ITEM_PUB.Process_Items (
               p_api_version      => 1.0,
               p_init_msg_list    => FND_API.g_TRUE,
               p_commit           => FND_API.g_TRUE,
               p_Item_Tbl         => l_item_table,
               x_Item_Tbl         => x_item_table,
               P_ROLE_GRANT_TBL   => EGO_ITEM_PUB.G_MISS_ROLE_GRANT_TBL,
               x_return_status    => x_return_status,
               x_msg_count        => x_msg_count);

            IF (x_return_status = FND_API.G_RET_STS_SUCCESS)
            THEN
               l_error_code := 'S';
               l_err_msg := NULL;
            ELSE
               l_error_code := 'E';
               Error_Handler.get_message_list (
                  x_message_list   => l_error_message_list);

               FOR i IN 1 .. l_error_message_list.COUNT
               LOOP
                  l_err_msg := NULL;
                  l_err_msg := l_error_message_list (i).MESSAGE_TEXT;
                  l_err_message := l_err_message || l_err_msg;
               END LOOP;
            END IF;

            l_err_message1 := l_err_message || l_err_message;
         END LOOP;
      END IF;                                      --IF l_item_exists > 0 THEN

      o_error_code := l_error_code;
      o_error_message := l_err_message1;
   EXCEPTION
      WHEN OTHERS
      THEN
         l_err_msg :=
               l_err_msg
            || ' ...Error_Stack...'
            || DBMS_UTILITY.format_error_stack ()
            || ' Error_Backtrace...'
            || DBMS_UTILITY.format_error_backtrace ();

         -- Calling ERROR API
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || l_procedure,
            p_ora_error_msg       => SUBSTR (l_err_msg, 1, 2000),
            p_error_desc          => SUBSTR (l_err_msg, 1, 240),
            p_distribution_list   => g_distro_list,
            p_module              => 'INV');


         o_error_code := 'E';
         o_error_message :=
               'Error in Process_item Prcedure'
            || SQLCODE
            || '-'
            || SUBSTR (SQLERRM, 1, 250);
   END Process_item;

   --+=======================================================================*/
   --| Purpose : This is the main procedure and this will be used to call    |
   --|           different procedures as appropriate                         |
   --+=======================================================================+
   --| MODIFICATION HISTORY                                                  |
   --| Person        Date             Version       Comments                 |
   --+ =========     =============   ========     ===========================+
   --| Ram          08-Mar-2015     1.0          Created                    |
   --+=======================================================================*/
   PROCEDURE Apply_Changes (i_change_id IN NUMBER)
   IS
      /*******************************************************************************************************************************
               PROCEDURE : Apply_Changes

                 REVISIONS:
                 Ver        Date        Author                     Description
                 ---------  ----------  ---------------    ----------------------------------------------------------------------------------
                 1.0        01-MAR-2015   Shankar Vanga     TMS#20150126-00044  - Create Single Item Workflow Tool
                                                            Initial Version
                 2.0        26-May-2015   P.vamshidhar      TMS#20150519-00091 - Single Item UI Enhancements
                                                            Added core status category

                 6.0        20-Oct-2015   P.Vamshidhar      TMS#20150807-00170 -- PDH - Add UDA in PDH for Last Verified Date
               10.0         30-Nov-2016   P.Vamshidhar      TMS#20160122-00091  - GL Code Enhancements to leverage GL API
      *********************************************************************************************************************************/


      CURSOR cur_item_update (
         c_change_id    NUMBER)
      IS
         SELECT item.oracle_change_id,
                item.inventory_item_id,
                item.organization_id,
                chg.change_notice change_order,
                chg.change_mgmt_type_code, -- Added by Vamshi in Rev 6.0 @ TMS#20150807-00170
                item.segment1 item_number,
                item.description,
                item.shelf_life_days,
                item.hazardous_material_flag,
                item.unit_weight,
                item.list_price_per_unit,
                item.long_description,
                item.weight_uom_code,
                global_attribute1 Item_category_1                    --inv Cat
                                                 ,
                global_attribute2 Item_category_2                   --Web hier
                                                 ,
                global_attribute3 Item_category_3             --web item y/n/c
                                                 ,
                global_attribute5 Item_category_4            ---Time Sensitive
                                                 ,
                NVL (global_attribute6, 'Non-Core') Item_category_5 --- Core status  -- Added by Vamshi in TMS#20150519-00091 V2.0
           ---Added cat3 web item add call categroy, loop3
           FROM XXWC_MTL_SY_ITEMS_CHG_B item, eng_engineering_changes chg
          WHERE     item.oracle_change_id = chg.change_id
                AND chg.change_id = c_change_id;

      l_reprocess_flag        VARCHAR2 (1);
      l_error_code            VARCHAR2 (240);
      l_process_flag          VARCHAR2 (1);
      l_error_message         VARCHAR2 (240);
      x_error_code            VARCHAR2 (1000);
      x_error_message         VARCHAR2 (4000);
      l_rd_rec_count          NUMBER;
      l_failed_count          NUMBER;
      l_processed_count       NUMBER;

      TYPE varchar_80_tbl IS TABLE OF VARCHAR2 (80)
         INDEX BY PLS_INTEGER;

      l_category_names        varchar_80_tbl;
      l_category_values       varchar_80_tbl;
      l_category_name_1       VARCHAR2 (50) := 'Inventory Category';
      l_category_name_2       VARCHAR2 (50) := 'WC Web Hierarchy';
      l_category_name_3       VARCHAR2 (50) := 'Website Item';
      l_category_name_4       VARCHAR2 (50) := 'Time Sensitive';
      l_category_name_5       VARCHAR2 (50) := 'Core Status'; -- Added by Vamshi @ TMS#20150519-00091 26-May-2015
      l_action_type           VARCHAR2 (20);
      L_ITEM_PROCESS_STATUS   VARCHAR2 (1);
      l_index                 PLS_INTEGER;

      l_procedure             VARCHAR2 (100) := 'APPLY_CHANGES';
      l_err_msg               VARCHAR2 (2000);
      ln_item_id              MTL_SYSTEM_ITEMS_B.INVENTORY_ITEM_ID%TYPE;
      lb_acct_update          BOOLEAN;
      
      cursor c_item_orgs (p_change_id1 NUMBER) is
        SELECT B.organization_id
          FROM xxwc_mtl_sy_items_chg_b a, apps.mtl_system_items_b b
         WHERE     a.change_id = p_change_id1
               AND A.INVENTORY_ITEM_ID = B.INVENTORY_ITEM_ID and b.inventory_item_status_code<>'Inactive';      
      
   BEGIN
      ----------------------------------------------
      UPDATE xxwc_mtl_sy_items_chg_b
         SET weight_uom_code = 'LBS'
       WHERE CHANGE_ID = i_change_id AND weight_uom_code IS NULL;

      -----------------------------------------------------
      l_processed_count := 0;
      l_failed_count := 0;

      IF i_change_id IS NULL
      THEN
         RETURN;
      END IF;
      
      -----
      BEGIN
       SELECT INVENTORY_ITEM_ID INTO ln_item_id FROM xxwc_mtl_sy_items_chg_b WHERE CHANGE_ID=i_change_id;
      EXCEPTION
      WHEN NO_DATA_FOUND THEN 
       ln_item_id := NULL;
      END;  
      -----
      DEBUG_LOG (
            'i_change_id: '
         || i_change_id
         || ' l_rd_rec_count: '
         || l_rd_rec_count);

      ------------------
      --Get Record Count
      ------------------
      BEGIN
         SELECT COUNT (1)
           INTO l_rd_rec_count
           FROM XXWC_MTL_SY_ITEMS_CHG_B item, eng_engineering_changes chg
          WHERE     item.oracle_change_id = chg.change_id
                AND chg.change_id = i_change_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            NULL;
      END;

      DEBUG_LOG (
            'i_change_id: '
         || i_change_id
         || ' l_rd_rec_count: '
         || l_rd_rec_count);

      IF l_rd_rec_count < 1
      THEN
         RETURN;
      END IF;

      FOR cur_item_update_data IN cur_item_update (c_change_id => i_change_id)
      LOOP
         -----------------------------
         --Initialization of Variables
         -----------------------------
         x_error_code := NULL;
         x_error_message := NULL;
         l_process_flag := NULL;
         l_error_message := NULL;

         --------------------------------
         --Process Item
         --------------------------------

         Process_item (i_change_order    => cur_item_update_data.change_order,
                       o_error_code      => l_process_flag,
                       o_error_message   => l_error_message);
         l_item_process_status := l_process_flag;

         IF l_item_process_status != 'S'
         THEN
            RETURN;
         END IF;

         l_category_values.delete ();
         L_CATEGORY_NAMES.DELETE ();
         l_index := 0;

         IF CUR_ITEM_UPDATE_DATA.ITEM_CATEGORY_1 IS NOT NULL
         THEN
            l_index := l_index + 1;
            l_category_names (l_index) := l_category_name_1;
            l_category_values (l_index) :=
               cur_item_update_data.Item_category_1;
         END IF;

         IF CUR_ITEM_UPDATE_DATA.ITEM_CATEGORY_2 IS NOT NULL
         THEN
            l_index := l_index + 1;
            l_category_names (l_index) := l_category_name_2;
            l_category_values (l_index) :=
               cur_item_update_data.Item_category_2;
         END IF;

         IF CUR_ITEM_UPDATE_DATA.ITEM_CATEGORY_3 IS NOT NULL
         THEN
            l_index := l_index + 1;
            l_category_names (l_index) := l_category_name_3;
            l_category_values (l_index) :=
               cur_item_update_data.Item_category_3;
         END IF;

         IF CUR_ITEM_UPDATE_DATA.ITEM_CATEGORY_4 IS NOT NULL
         THEN
            l_index := l_index + 1;
            l_category_names (l_index) := l_category_name_4;
            l_category_values (l_index) :=
               cur_item_update_data.Item_category_4;
         END IF;

         -- Added below code by Vamshi @ TMS#20150519-00091 26-May-2015  -- start

         IF CUR_ITEM_UPDATE_DATA.ITEM_CATEGORY_5 IS NOT NULL
         THEN
            l_index := l_index + 1;
            l_category_names (l_index) := l_category_name_5;
            l_category_values (l_index) :=
               cur_item_update_data.Item_category_5;
         END IF;

         -- Added above code by Vamshi @ TMS#20150519-00091 26-May-2015  -- end.

         ------------------------------------------------------------
         -- Item Categories
         -------------------------------------------------------------


         FOR I IN 1 .. L_CATEGORY_NAMES.COUNT
         LOOP
            l_process_flag := NULL;
            l_error_message := NULL;
            update_category (
               i_category_name     => l_category_names (i),
               i_category_values   => l_category_values (i),
               i_inv_item_id       => cur_item_update_data.inventory_item_id,
               i_org_id            => cur_item_update_data.organization_id,
               o_error_code        => l_process_flag,
               o_error_message     => l_error_message);
            DEBUG_LOG (
                  'Category '
               || l_category_names (i)
               || ': '
               || l_category_values (i)
               || ':'
               || i_change_id
               || ' change_order: '
               || cur_item_update_data.change_order
               || ' l_process_flag: '
               || l_process_flag
               || ' l_error_message: '
               || l_error_message);
         END LOOP;

         ------------------------------------------------------------
         -- Item UDAs
         -------------------------------------------------------------
         update_uda_attributes (
            i_change_order    => cur_item_update_data.change_order,
            i_change_type     => cur_item_update_data.change_mgmt_type_code, -- Added by Vamshi in Rev 6.0 @ TMS#20150807-00170
            i_change_id       => cur_item_update_data.oracle_change_id,
            i_inv_item_id     => cur_item_update_data.inventory_item_id,
            i_org_id          => cur_item_update_data.organization_id,
            o_error_code      => l_process_flag,
            o_error_message   => l_error_message);
         DEBUG_LOG (
               'UDA: '
            || i_change_id
            || ' change_order: '
            || cur_item_update_data.change_order
            || ' l_process_flag: '
            || l_process_flag
            || ' l_error_message: '
            || l_error_message);

         -----------------------------------------------------
         --Item Cross References
         -----------------------------------------------------
         Process_Cross_Ref (
            i_change_order    => cur_item_update_data.change_order,
            o_error_code      => l_process_flag,
            o_error_message   => l_error_message);
         DEBUG_LOG (
               'Cross reference 1: '
            || i_change_id
            || ' change_order: '
            || cur_item_update_data.change_order
            || ' l_process_flag: '
            || l_process_flag
            || ' l_error_message: '
            || l_error_message);

         --------------------------------------------------------------
         --Item Attachments
         --------------------------------------------------------------
         update_attachments (
            i_change_order    => cur_item_update_data.change_order,
            i_change_id       => cur_item_update_data.oracle_change_id,
            i_inv_item_id     => cur_item_update_data.inventory_item_id,
            i_org_id          => cur_item_update_data.organization_id,
            o_error_code      => l_process_flag,
            o_error_message   => l_error_message);
         DEBUG_LOG (
               'Attachment: '
            || i_change_id
            || ' change_order: '
            || cur_item_update_data.change_order
            || ' l_process_flag: '
            || l_process_flag
            || ' l_error_message: '
            || l_error_message);
      END LOOP;

      COMMIT;
      
      -- Added below code in Rev 10.0
      
      FOR r_item_orgs IN c_item_orgs (i_change_id) LOOP
            
      IF ln_item_id IS NOT NULL THEN
      lb_acct_update:=XXWC_INV_ITEM_ACCT_UPDATE_PKG.accts_update(r_item_orgs.organization_id,ln_item_id);
      END IF;
      
      END LOOP;
     
   EXCEPTION
      WHEN OTHERS
      THEN
         l_err_msg :=
               l_err_msg
            || ' ...Error_Stack...'
            || DBMS_UTILITY.format_error_stack ()
            || ' Error_Backtrace...'
            || DBMS_UTILITY.format_error_backtrace ();

         -- Calling ERROR API
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || l_procedure,
            p_ora_error_msg       => SUBSTR (l_err_msg, 1, 2000),
            p_error_desc          => SUBSTR (l_err_msg, 1, 240),
            p_distribution_list   => g_distro_list,
            p_module              => 'INV');
   END Apply_Changes;

   -----------------------------------------------------------------


   FUNCTION handle_CO_Status_change (
      P_SUBSCRIPTION_GUID   IN            RAW,
      P_EVENT               IN OUT NOCOPY WF_EVENT_T)
      RETURN VARCHAR2
   IS
      /*******************************************************************************************************************************
               FUNCTION : handle_CO_Status_change

                 REVISIONS:
                 Ver        Date        Author                     Description
                 ---------  ----------  ---------------   ----------------------------------------------------------------------------------
                 1.0        01-MAR-2015 Shankar Vanga     TMS#20150126-00044  - Create Single Item Workflow Tool
                                                            Initial Version
              -- 3.0        17-Jun-2015 P.vamshidhar      1. Commented Purging data from temp table.  The will be taken care in
                                                           'XXWC Rejected Items Purging' program.  TMS#20150609-00025

      *********************************************************************************************************************************/


      i_change_id             NUMBER;
      l_item_id               NUMBER;
      l_error                 VARCHAR2 (1000);

      l_procedure             VARCHAR2 (100) := 'HANDLE_CO_STATUS_CHANGE';
      l_err_msg               VARCHAR2 (2000);


      l_param_name            VARCHAR2 (240);
      l_param_value           VARCHAR2 (2000);
      l_event_name            VARCHAR2 (2000);
      l_event_key             VARCHAR2 (2000);
      l_err_text              VARCHAR2 (3000);
      l_param_list            WF_PARAMETER_LIST_T;

      CURSOR get_change_order_details (
         cp_change_id    NUMBER)
      IS
         SELECT ctypes.change_order_type,
                ec.change_notice,
                ec.APPROVAL_STATUS_TYPE,
                ec.change_id,
                ec.status_code,
                evi.revised_item_id item_id,
                ecs.status_name,
                ecs.status_name change_order_status,
                lkp.meaning approval_status,
                usr.user_name co_created_by
           FROM eng_revised_items evi,
                apps.eng_change_order_types_vl ctypes,
                eng_engineering_changes ec,
                eng_change_statuses_vl ecs,
                mfg_lookups lkp,
                fnd_user usr
          WHERE     ecs.status_code = ec.status_code
                AND usr.user_id = ec.created_by
                AND ec.change_id = evi.change_id
                AND ctypes.change_order_type_id = ec.change_order_type_id
                AND ec.change_id = cp_change_id
                AND ec.APPROVAL_STATUS_TYPE = lkp.lookup_code(+)
                AND lkp.lookup_type(+) = 'ENG_ECN_APPROVAL_STATUS'
         UNION ALL
         SELECT ctypes.change_order_type,
                ec.change_notice,
                ec.APPROVAL_STATUS_TYPE,
                ec.change_id,
                ec.status_code,
                TO_NUMBER (cs.pk1_value) item_id,
                ecs.status_name,
                ecs.status_name change_order_status,
                lkp.meaning approval_status,
                usr.user_name co_created_by
           FROM eng_change_subjects cs,
                apps.eng_change_order_types_vl ctypes,
                eng_engineering_changes ec,
                eng_change_statuses_vl ecs,
                mfg_lookups lkp,
                fnd_user usr
          WHERE     ecs.status_code = ec.status_code
                AND usr.user_id = ec.created_by
                AND ec.change_id = cs.change_id
                AND cs.change_line_id IS NULL
                AND ctypes.change_order_type_id = ec.change_order_type_id
                AND ec.change_id = cp_change_id
                AND ec.APPROVAL_STATUS_TYPE = lkp.lookup_code(+)
                AND lkp.lookup_type(+) = 'ENG_ECN_APPROVAL_STATUS';


      l_approval_status       VARCHAR2 (30);

      l_msg                   VARCHAR2 (4000);
      l_change_order_status   VARCHAR2 (40);
      l_change_order_type     VARCHAR2 (80);
   BEGIN
      l_event_name := p_event.geteventname ();
      L_PARAM_LIST := P_EVENT.GETPARAMETERLIST;
      L_MSG := L_MSG || l_event_name || ':';

      FOR i IN l_param_list.FIRST .. l_param_list.LAST
      LOOP
         l_param_name := l_param_list (i).getname;
         L_PARAM_VALUE := L_PARAM_LIST (I).GETVALUE;
         L_MSG :=
               L_MSG
            || l_param_name
            || ' = '
            || L_PARAM_VALUE
            || ','
            || CHR (10);

         IF (l_param_name = 'ChangeId')
         THEN
            i_change_id := l_param_value;
         END IF;

         FOR rec IN get_change_order_details (i_change_id)
         LOOP
            l_approval_status := rec.approval_status;
            l_item_id := rec.item_id;
            l_change_order_status := rec.change_order_status;
            l_change_order_type := rec.change_order_type;
         END LOOP;

         --DEBUG_LOG(l_msg);
         IF l_change_order_type NOT IN ('XXWC L1-L2 Create Item Request',
                                        'XXWC L3 Item Creation',
                                        'XXWC L1 Item Update Request',
                                        'XXWC L2 Item Update Request',
                                        'XXWC L3 Item Update Request')
         THEN
            RETURN 'SUCCESS';
         END IF;

         IF (l_approval_status = 'Rejected')
         THEN
            --Commented below code in  TMS#20150609-00025     by Vamshi in V3.0
            NULL;
         --purge_temp_data (i_change_id, l_item_id, l_change_order_type);

         END IF;

         DEBUG_LOG (
               'i_change_id: '
            || i_change_id
            || ' l_change_order_status: '
            || l_change_order_status);

         IF (l_change_order_status IN ('Implemented', 'Released'))
         THEN
            DEBUG_LOG (
                  i_change_id
               || ' Status is: '
               || l_change_order_status
               || ', Applying changes to base tables...');
            Apply_Changes (i_change_id);
         END IF;
      END LOOP;

      RETURN 'SUCCESS';
   EXCEPTION
      WHEN OTHERS
      THEN
         l_err_msg :=
               l_err_msg
            || ' ...Error_Stack...'
            || DBMS_UTILITY.format_error_stack ()
            || ' Error_Backtrace...'
            || DBMS_UTILITY.format_error_backtrace ();

         -- Calling ERROR API
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || l_procedure,
            p_ora_error_msg       => SUBSTR (l_err_msg, 1, 2000),
            p_error_desc          => SUBSTR (l_err_msg, 1, 240),
            p_distribution_list   => g_distro_list,
            p_module              => 'INV');


         l_error := SQLERRM;
         --l_err_text := 'ERROR : ' || TO_CHAR (SQLCODE) || '---' || l_error;
         RETURN l_error;
   END handle_CO_Status_change;

   /*******************************************************************************************************************************
           FUNCTION : XXWC_EGOGROUP_USR_VALID_FUNC

              REVISIONS:
              Ver        Date        Author                     Description
              ---------  ----------  ---------------    ----------------------------------------------------------------------------
           -- 2.0       26-May-2015  P.vamshidhar       TMS#20150519-00091 - Single Item UI Enhancements
                                                        Added to function to create link in Workflow monitor
                                                        if function return 0 user not eligible to access link else yes.

   *********************************************************************************************************************************/

   FUNCTION XXWC_EGOGROUP_USR_VALID_FUNC (P_USER_NAME    IN VARCHAR2,
                                          P_GROUP_NAME   IN VARCHAR2)
      RETURN NUMBER
   IS
      ln_count   NUMBER;
   BEGIN
      SELECT COUNT (1)
        INTO ln_count
        FROM ego_group_members_v
       WHERE     UPPER (MEMBER_USER_NAME) = UPPER (P_USER_NAME)
             AND UPPER (GROUP_NAME) = UPPER (P_GROUP_NAME);

      IF ln_count > 0
      THEN
         RETURN 1;
      ELSE
         RETURN 0;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 0;
   END;


   /*******************************************************************************************************************************
    FUNCTION : XXWC_WEIGHT_UOM_CODE_UPDATE

    REVISIONS:
    Ver        Date           Author                     Description
    ---------  ----------    ---------------    ----------------------------------------------------------------------------
    6.0        11-Aug-2015   P.Vamshidhar       TMS#20141006-00021 - Description and Shelf Life changes not consistently
                                                propagating to lower level Orgs from Item Master


   *********************************************************************************************************************************/

   PROCEDURE XXWC_WEIGHT_UOM_CODE_UPDATE (p_change_notice       IN VARCHAR2,
                                          p_inventory_item_id   IN NUMBER)
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
      l_weight_uom_code     MTL_SYSTEM_ITEMS_B.WEIGHT_UOM_CODE%TYPE;
      l_inventory_item_id   MTL_SYSTEM_ITEMS_B.inventory_item_id%TYPE;
      l_mast_org_id         MTL_SYSTEM_ITEMS_B.organization_id%TYPE
                               := FND_PROFILE.VALUE ('XXWC_ITEM_MASTER_ORG');
      l_procedure           VARCHAR2 (1000) := 'XXWC_WEIGHT_UOM_CODE_UPDATE';
      l_err_msg             VARCHAR2 (2000);
   BEGIN
      BEGIN
         SELECT NVL (WEIGHT_UOM_CODE, 'LBS')
           INTO l_weight_uom_code
           FROM XXWC.XXWC_MTL_SY_ITEMS_CHG_B A,
                APPS.ENG_ENGINEERING_CHANGES B
          WHERE     b.change_notice = p_change_notice
                AND a.change_id = b.change_id
                AND ROWNUM < 2;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_weight_uom_code := 'LBS';
      END;

      UPDATE MTL_SYSTEM_ITEMS_B
         SET WEIGHT_UOM_CODE = l_weight_uom_code
       WHERE     INVENTORY_ITEM_ID = p_inventory_item_id
             AND ORGANIZATION_ID = NVL (l_mast_org_id, '222');

      IF SQL%ROWCOUNT > 0
      THEN
         COMMIT;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         l_err_msg :=
               l_err_msg
            || ' ...Error_Stack...'
            || DBMS_UTILITY.format_error_stack ()
            || ' Error_Backtrace...'
            || DBMS_UTILITY.format_error_backtrace ();

         -- Calling ERROR API
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || l_procedure,
            p_ora_error_msg       => SUBSTR (l_err_msg, 1, 2000),
            p_error_desc          => SUBSTR (l_err_msg, 1, 240),
            p_distribution_list   => g_distro_list,
            p_module              => 'INV');
   END;
END XXWC_EGO_ITEM_BE;
/