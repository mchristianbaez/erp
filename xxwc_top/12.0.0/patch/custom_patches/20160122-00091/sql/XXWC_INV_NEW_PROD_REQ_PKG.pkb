CREATE OR REPLACE PACKAGE BODY APPS.XXWC_INV_NEW_PROD_REQ_PKG
AS
   /*************************************************************************************************************************
   *  Copyright (c) 2011 Lucidity Consulting Group
   *  All rights reserved.
   *************************************************************************************************************************
   *   $Header XXWC_INV_NEW_PROD_REQ_PKG.pkb $
   *   Module Name: XXWC_INV_NEW_PROD_REQ_PKG.pkb
   *
   *   PURPOSE:   This package is used by the XXWC New product request workflow
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        12/01/2011  Shankar Hariharan        Initial Version
   *   1.1       05/22/2012   Shankar Hariharan        Added Code to assign Sales velocity category
   *                                                   and item cost
   *   1.2       06/06/2012 Shankar Hariharan       Added Inv Category assignment
   *   1.3      06/10/2012  Shankar Hariharan         Added functionality to derive COGS and Revenue Account
   *   1.4      06/26/2012  Shankar Hariharan         Changed the cross-reference to Vendor
   *   1.5      04/01/2013 Shankar Hariharan       TMS Task 20130401-00537
   *   1.6      06/20/2013 Shankar Hariharan       TMS Task 20130620-00850
   *   1.7      09/23/2013  Ram Talluri            TMS 20130913-00647 and 20130912-00860
   *   1.8      12/04/2013 Shankar Hariharan       TMS 20131121-00084
   *   1.9      04/28/2014  Manjula Chellappan     TMS # 20140407-00238 - New procedure POPULATE_PRODUCT_UDA
   *   2.0      06/03/2014  Praveen Pawar          TMS # 20140519-00390 - NPBI Attribute Update Requirement
   *   2.1      11/11/2014  Raghavendra S          TMS# 20141001-00253 - Canada Multi org Changes
   *   2.2      09/25/2015  P.Vamshidar            TMS#20150918-00018  - Correct cloning issue with XXWC_Clone_Parts form
   *   2.3      11/30/2016  P.Vamshidhar           TMS#20160122-00091 - GL Code Enhancements to leverage GL API 
   *************************************************************************************************************************/

   g_default_email   fnd_user.email_address%TYPE
                        := 'HDSOracleDevelopers@hdsupply.com';

   -- Function added 10-Jun-2012 to generate cogs and sales account by shankar
   FUNCTION get_gl_account (i_inventory_item_id   IN NUMBER,
                            i_organization_id     IN NUMBER,
                            i_acc_type            IN VARCHAR2)
      RETURN NUMBER
   IS
      l_mst_org_id     NUMBER := fnd_profile.VALUE ('XXWC_ITEM_MASTER_ORG');
      l_cogs_acc       VARCHAR2 (10);
      l_sales_acc      VARCHAR2 (10);
      l_expn_acc       VARCHAR2 (10); -- Added in Rev 2.3
      l_org_cogs_id    NUMBER;
      l_org_sales_id   NUMBER;
      l_org_expn_id    NUMBER;   -- Added in Rev 2.3
      lvc_gcc_cogs  VARCHAR2(4000);
      lvc_gcc_sales VARCHAR2(4000);
      lvc_gcc_expn  VARCHAR2(4000); -- Added in Rev 2.3
      l_gen_acc_id  GL_CODE_COMBINATIONS.CODE_COMBINATION_ID%TYPE;
   BEGIN
      -- Derive COGS and Revenue based on Item Category
      -- Added by Shankar on 10-Jun-2012
      SELECT c.attribute1, c.attribute2, c.attribute8  -- Added attribute8 in Rev 2.3
        INTO l_cogs_acc, l_sales_acc, l_expn_acc
        FROM mtl_item_categories a,
             mtl_default_category_sets b,
             mtl_categories c
       WHERE     a.category_set_id = b.category_set_id
             AND a.organization_id = l_mst_org_id
             AND b.functional_area_id = 1
             AND a.inventory_item_id = i_inventory_item_id
             AND a.category_id = c.category_id;

        -- Added expense account in Rev 2.3 
      SELECT cost_of_sales_account, sales_account, expense_account
        INTO l_org_cogs_id, l_org_sales_id, l_org_expn_id  
        FROM mtl_parameters  
       WHERE organization_id = i_organization_id;

      IF i_acc_type = 'C'
      THEN
         IF l_cogs_acc IS NULL
         THEN
            RETURN l_org_cogs_id;
         ELSE
		 -- Below changes made in Rev 2.3 by Vamshi - Begin 
          /*  BEGIN
               SELECT c.code_combination_id
                 INTO l_gen_acc_id
                 FROM gl_code_combinations b, gl_code_combinations c
                WHERE     b.code_combination_id = l_org_cogs_id
                      AND b.segment1 = c.segment1
                      AND b.segment2 = c.segment2
                      AND b.segment3 = c.segment3
                      AND b.segment5 = c.segment5
                      AND b.segment6 = c.segment6
                      AND b.segment7 = c.segment7
                      AND c.segment4 = l_cogs_acc;

               RETURN (l_gen_acc_id);
            EXCEPTION
               WHEN OTHERS
               THEN
                  RETURN (l_org_cogs_id);
            END;*/	 
		 
		 
            BEGIN
               SELECT    a.segment1
                      || '.'
                      || a.segment2
                      || '.'
                      || a.segment3
                      || '.'
                      || l_cogs_acc
                      || '.'
                      || a.segment5
                      || '.'
                      || a.segment6
                      || '.'
                      || a.segment7
                 INTO lvc_gcc_cogs
                 FROM APPS.GL_CODE_COMBINATIONS a
                WHERE a.CODE_COMBINATION_ID = l_org_cogs_id;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  RETURN l_org_cogs_id;
            END;

            BEGIN
				SELECT a.code_combination_id
				  INTO l_gen_acc_id
				  FROM gl_code_combinations a
				 WHERE    a.segment1
					   || '.'
					   || a.segment2
					   || '.'
					   || a.segment3
					   || '.'
					   || a.segment4
					   || '.'
					   || a.segment5
					   || '.'
					   || a.segment6
					   || '.'
					   || a.segment7 = lvc_gcc_cogs;

               RETURN l_gen_acc_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_gen_acc_id := 0;
            END;

            IF l_gen_acc_id = 0
            THEN
               l_gen_acc_id :=
                  xxwc_inv_item_acct_update_pkg.create_ccid (lvc_gcc_cogs);
            END IF;

            RETURN l_gen_acc_id;
		  -- Above changes made in Rev 2.3 by Vamshi - End. 			
         END IF;
      ELSIF i_acc_type = 'S'
      THEN
         IF l_sales_acc IS NULL
         THEN
            RETURN l_org_sales_id;
         ELSE
		    -- Below changes done in Rev 2.3 by Vamshi - Begin
		    /* 
            BEGIN
               SELECT c.code_combination_id
                 INTO l_gen_acc_id
                 FROM gl_code_combinations b, gl_code_combinations c
                WHERE     b.code_combination_id = l_org_sales_id
                      AND b.segment1 = c.segment1
                      AND b.segment2 = c.segment2
                      AND b.segment3 = c.segment3
                      AND b.segment5 = c.segment5
                      AND b.segment6 = c.segment6
                      AND b.segment7 = c.segment7
                      AND c.segment4 = l_sales_acc;

               RETURN (l_gen_acc_id);
            EXCEPTION
               WHEN OTHERS
               THEN
                  RETURN (l_org_sales_id);
            END;  */		 
		 
            BEGIN
               SELECT    a.segment1
                      || '.'
                      || a.segment2
                      || '.'
                      || a.segment3
                      || '.'
                      || l_sales_acc
                      || '.'
                      || a.segment5
                      || '.'
                      || a.segment6
                      || '.'
                      || a.segment7
                 INTO lvc_gcc_sales
                 FROM APPS.GL_CODE_COMBINATIONS a
                WHERE a.CODE_COMBINATION_ID = l_org_sales_id;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  RETURN l_org_sales_id;
            END;

            BEGIN
				SELECT a.code_combination_id
				  INTO l_gen_acc_id
				  FROM gl_code_combinations a
				 WHERE    a.segment1
					   || '.'
					   || a.segment2
					   || '.'
					   || a.segment3
					   || '.'
					   || a.segment4
					   || '.'
					   || a.segment5
					   || '.'
					   || a.segment6
					   || '.'
					   || a.segment7 = lvc_gcc_sales;

               RETURN l_gen_acc_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_gen_acc_id := 0;
            END;

            IF l_gen_acc_id = 0
            THEN
               l_gen_acc_id :=
                  xxwc_inv_item_acct_update_pkg.create_ccid (lvc_gcc_sales);
            END IF;

            RETURN l_gen_acc_id;

         END IF;
      ELSIF i_acc_type = 'E'
      THEN
         IF l_expn_acc IS NULL
         THEN
            RETURN l_org_expn_id;
         ELSE
            BEGIN
               SELECT    a.segment1
                      || '.'
                      || a.segment2
                      || '.'
                      || a.segment3
                      || '.'
                      || l_expn_acc
                      || '.'
                      || a.segment5
                      || '.'
                      || a.segment6
                      || '.'
                      || a.segment7
                 INTO lvc_gcc_expn
                 FROM APPS.GL_CODE_COMBINATIONS a
                WHERE a.CODE_COMBINATION_ID = l_org_expn_id;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  RETURN l_org_expn_id;
            END;

            BEGIN
				SELECT a.code_combination_id
				  INTO l_gen_acc_id
				  FROM gl_code_combinations a
				 WHERE    a.segment1
					   || '.'
					   || a.segment2
					   || '.'
					   || a.segment3
					   || '.'
					   || a.segment4
					   || '.'
					   || a.segment5
					   || '.'
					   || a.segment6
					   || '.'
					   || a.segment7 = lvc_gcc_expn;

               RETURN l_gen_acc_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_gen_acc_id := 0;
            END;

            IF l_gen_acc_id = 0
            THEN
               l_gen_acc_id :=
                  xxwc_inv_item_acct_update_pkg.create_ccid (lvc_gcc_expn);
            END IF;

            RETURN l_gen_acc_id;
		    -- Above changes done in Rev 2.3 by Vamshi - End				  			
         END IF;
      END IF;
   END get_gl_account;

   PROCEDURE CREATE_CROSS_REF (p_segment1               IN VARCHAR2,
                               p_cross_reference_type   IN VARCHAR2,
                               p_cross_reference        IN VARCHAR2,
                               p_description            IN VARCHAR2)
   IS
      l_cross_reference_id   NUMBER;
      l_inventory_item_id    NUMBER;
      l_API_NAME             VARCHAR2 (30) := 'CREATE_CROSS_REF';
      l_Module_Name          VARCHAR2 (120);
   BEGIN
      -- Begining of API
      l_Module_Name := G_MODULE_NAME || '.' || l_API_NAME;
      XXWC_GEN_ROUTINES_PKG.LOG_MSG (p_debug_level   => G_LEVEL_PROCEDURE,
                                     p_mod_name      => l_Module_Name,
                                     P_DEBUG_MSG     => 'Beging of API');

      SELECT inventory_item_id
        INTO l_inventory_item_id
        FROM mtl_system_items_b msib
       WHERE msib.segment1 = p_segment1 AND ROWNUM = 1;

      mtl_cross_references_pkg.insert_row (
         p_source_system_id         => NULL,
         p_start_date_active        => NULL,
         p_end_date_active          => NULL,
         p_object_version_number    => NULL,
         p_uom_code                 => NULL,
         p_revision_id              => NULL,
         p_epc_gtin_serial          => NULL,
         p_inventory_item_id        => l_inventory_item_id,
         p_organization_id          => NULL,
         p_cross_reference_type     => p_cross_reference_type,
         p_cross_reference          => p_cross_reference,
         p_org_independent_flag     => 'Y',
         p_request_id               => NULL,
         p_attribute1               => NULL,
         p_attribute2               => NULL,
         p_attribute3               => NULL,
         p_attribute4               => NULL,
         p_attribute5               => NULL,
         p_attribute6               => NULL,
         p_attribute7               => NULL,
         p_attribute8               => NULL,
         p_attribute9               => NULL,
         p_attribute10              => NULL,
         p_attribute11              => NULL,
         p_attribute12              => NULL,
         p_attribute13              => NULL,
         p_attribute14              => NULL,
         p_attribute15              => NULL,
         p_attribute_category       => NULL,
         p_description              => p_description,
         p_creation_date            => NULL,
         p_created_by               => NULL,
         p_last_update_date         => NULL,
         p_last_updated_by          => NULL,
         p_last_update_login        => NULL,
         p_program_application_id   => NULL,
         p_program_id               => NULL,
         p_program_update_date      => NULL,
         x_cross_reference_id       => l_cross_reference_id);

      COMMIT;
      XXWC_GEN_ROUTINES_PKG.LOG_MSG (p_debug_level   => G_LEVEL_PROCEDURE,
                                     p_mod_name      => l_Module_Name,
                                     P_DEBUG_MSG     => 'End Of API');
   END CREATE_CROSS_REF;



   -- *********************************************************************************
   -- PROCEDURE CREATE_ITEM_COST: API to  create Item Cost : But API is more used in the function
   --
   -- *********************************************************************************

   PROCEDURE CREATE_ITEM_COST (p_Inventory_Item_ID   IN            NUMBER,
                               p_Organization_ID     IN            NUMBER,
                               p_Item_Cost           IN            NUMBER,
                               p_uom_code            IN            VARCHAR2,
                               x_Return_Status          OUT NOCOPY VARCHAR2,
                               x_Msg_Data               OUT NOCOPY VARCHAR2,
                               x_Msg_Count              OUT NOCOPY NUMBER)
   IS
      l_API_NAME                         VARCHAR2 (30) := 'CREATE_ITEM_COST';
      l_Module_Name                      VARCHAR2 (120);
      l_transaction_type_id              NUMBER;
      l_transaction_interface_id         NUMBER;
      l_mat_account                      NUMBER;
      C_COST_TYPE_AVG           CONSTANT NUMBER := 2;
      C_COST_ELEMENT_MATERIAL   CONSTANT NUMBER := 1;
      C_PROCESS_FLAG            CONSTANT NUMBER := 1;
   BEGIN
      -- Begining of API
      l_Module_Name := G_MODULE_NAME || '.' || l_API_NAME;
      -- Initialize Return Status Variable
      X_RETURN_STATUS := Fnd_API.G_RET_STS_SUCCESS;

      SELECT transaction_type_id
        INTO l_transaction_type_id
        FROM mtl_transaction_types
       WHERE description = 'Update average cost information';

      SELECT mtl_material_transactions_s.NEXTVAL
        INTO l_transaction_interface_id
        FROM DUAL;

      SELECT material_account, default_cost_group_id
        INTO l_mat_account, g_cost_group_id
        FROM mtl_parameters
       WHERE organization_id = p_organization_id;

      INSERT INTO inv.MTL_TRANSACTIONS_INTERFACE (transaction_interface_id,
                                                  transaction_header_id,
                                                  source_code,
                                                  source_line_id,
                                                  source_header_id,
                                                  process_flag,
                                                  transaction_mode,
                                                  last_update_date,
                                                  last_updated_by,
                                                  creation_date,
                                                  created_by,
                                                  inventory_item_id,
                                                  organization_id,
                                                  transaction_date,
                                                  transaction_source_id,
                                                  transaction_action_id,
                                                  transaction_source_type_id,
                                                  transaction_type_id,
                                                  material_account,
                                                  material_overhead_account,
                                                  resource_account,
                                                  outside_processing_account,
                                                  overhead_account,
                                                  new_average_cost,
                                                  transaction_quantity,
                                                  transaction_uom,
                                                  cost_group_id)
           VALUES (l_transaction_interface_id,
                   22222,
                   'ACU',
                   '1',
                   '1',
                   '1',
                   '3',
                   SYSDATE,                                         --Run date
                   fnd_global.user_id,                               --user_id
                   SYSDATE,                                         --Run date
                   fnd_global.user_id,                               --user_id
                   p_inventory_item_id,                    --Inventory Item Id
                   p_organization_id,                        --Organization Id
                   SYSDATE,                                   --batch_run_date
                   24,                                 --transaction action id
                   13,                            --transaction source type id
                   80,                                      --transaction type
                   l_transaction_type_id,
                   l_mat_account,                           --material_account
                   NULL,                          --,material_overhead_account
                   NULL,                                   --,resource_account
                   NULL,                         --,outside_processing_account
                   NULL,                                   --,overhead_account
                   p_item_cost,                                 --new avg cost
                   0,                                                   --Zero
                   p_uom_code,
                   g_cost_group_id);

      COMMIT;
      x_return_status := 'S';
   EXCEPTION
      WHEN OTHERS
      THEN
         x_Return_Status := 'U';
         x_Msg_Data := 'When Others Error : ' || SQLERRM;
         x_Msg_Count := 1;
         XXWC_GEN_ROUTINES_PKG.LOG_MSG (p_debug_level   => G_LEVEL_ERROR,
                                        p_mod_name      => l_Module_Name,
                                        P_DEBUG_MSG     => x_Msg_Data);
   END CREATE_ITEM_COST;


   PROCEDURE CREATE_ITEM_REL (p_item_id IN NUMBER, p_rel_item_id IN NUMBER)
   IS
      l_inventory_item_id   NUMBER;
      l_API_NAME            VARCHAR2 (30) := 'CREATE_ITEM_REL';
      l_Module_Name         VARCHAR2 (120);
      l_row_id              VARCHAR2 (120);
   BEGIN
      -- Begining of API
      l_Module_Name := G_MODULE_NAME || '.' || l_API_NAME;
      XXWC_GEN_ROUTINES_PKG.LOG_MSG (p_debug_level   => G_LEVEL_PROCEDURE,
                                     p_mod_name      => l_Module_Name,
                                     P_DEBUG_MSG     => 'Beging of API');

      mtl_related_items_pkg.Insert_Row (
         X_ROWID                   => l_row_id,
         X_Inventory_Item_Id       => p_item_id,
         X_Organization_Id         => g_organization_id,
         X_Related_Item_Id         => p_rel_item_id,
         --X_Relationship_Type_Id    => 2, -- Commented as per version 2.0 --
         X_Relationship_Type_Id    => 8, -- Added as per version 2.0 to change the relationship type to 'Superseded' --
         X_Reciprocal_Flag         => 'Y',
         X_Planning_Enabled_Flag   => 'N',
         X_Start_Date              => NULL,
         X_End_Date                => NULL,
         X_Attr_Context            => NULL,
         X_Attr_Char1              => NULL,
         X_Attr_Char2              => NULL,
         X_Attr_Char3              => NULL,
         X_Attr_Char4              => NULL,
         X_Attr_Char5              => NULL,
         X_Attr_Char6              => NULL,
         X_Attr_Char7              => NULL,
         X_Attr_Char8              => NULL,
         X_Attr_Char9              => NULL,
         X_Attr_Char10             => NULL,
         X_Attr_Num1               => NULL,
         X_Attr_Num2               => NULL,
         X_Attr_Num3               => NULL,
         X_Attr_Num4               => NULL,
         X_Attr_Num5               => NULL,
         X_Attr_Num6               => NULL,
         X_Attr_Num7               => NULL,
         X_Attr_Num8               => NULL,
         X_Attr_Num9               => NULL,
         X_Attr_Num10              => NULL,
         X_Attr_Date1              => NULL,
         X_Attr_Date2              => NULL,
         X_Attr_Date3              => NULL,
         X_Attr_Date4              => NULL,
         X_Attr_Date5              => NULL,
         X_Attr_Date6              => NULL,
         X_Attr_Date7              => NULL,
         X_Attr_Date8              => NULL,
         X_Attr_Date9              => NULL,
         X_Attr_Date10             => NULL,
         X_Last_Update_Date        => SYSDATE,
         X_Last_Updated_By         => fnd_global.user_id,
         X_Creation_Date           => SYSDATE,
         X_Created_By              => fnd_global.user_id,
         X_Last_Update_Login       => fnd_global.login_id,
         X_Object_Version_Number   => 1);
      COMMIT;
      XXWC_GEN_ROUTINES_PKG.LOG_MSG (p_debug_level   => G_LEVEL_PROCEDURE,
                                     p_mod_name      => l_Module_Name,
                                     P_DEBUG_MSG     => 'End Of API');
   END CREATE_ITEM_REL;


   PROCEDURE ASSIGN_INV_CATEGORY (
      p_category_id         IN            NUMBER,
      p_organization_id     IN            NUMBER,
      p_Inventory_Item_ID   IN            NUMBER,
      x_Return_Status          OUT NOCOPY VARCHAR2,
      x_Msg_Data               OUT NOCOPY VARCHAR2,
      x_Msg_Count              OUT NOCOPY NUMBER)
   IS
      x_message_list                Error_Handler.Error_Tbl_Type;

      x_errorcode                   VARCHAR2 (2000);
      l_category_id                 NUMBER;
      l_Category_Set_ID             NUMBER;
      l_MSg_Count                   NUMBER;
      l_API_NAME                    VARCHAR2 (30) := 'ASSIGN_CATEGORY';
      l_Module_Name                 VARCHAR2 (120);
      l_MULT_ITEM_CAT_ASSIGN_FLAG   VARCHAR2 (1);
   BEGIN
      -- Begining of API
      l_Module_Name := G_MODULE_NAME || '.' || l_API_NAME;
      -- Initialize Return Status Variable
      X_RETURN_STATUS := Fnd_API.G_RET_STS_SUCCESS;
      XXWC_GEN_ROUTINES_PKG.LOG_MSG (p_debug_level   => G_LEVEL_STATEMENT,
                                     p_mod_name      => l_Module_Name,
                                     P_DEBUG_MSG     => 'At begining of API');

      BEGIN
         SELECT mic.category_id, mic.Category_Set_ID
           INTO l_category_id, l_Category_Set_ID
           FROM mtl_item_categories mic, MTL_CATEGORY_SETS_V ms
          WHERE     mic.inventory_item_id = p_Inventory_Item_ID
                AND mic.organization_id = p_organization_id
                AND Mic.Category_Set_ID = ms.category_set_id
                AND ms.category_set_name = 'Inventory Category'
                AND ROWNUM = 1;

         IF l_category_id <> p_Category_ID
         THEN
            -- If User Choosen Category is different from Default Category then Update Item to User Choosen Category
            INV_ITEM_CATEGORY_PUB.Update_Category_Assignment (
               p_api_version         => 1.0,
               p_init_msg_list       => 'T',
               p_commit              => 'T',
               p_category_id         => p_category_id,
               p_old_category_id     => l_category_id,
               p_category_set_id     => l_category_set_id,
               p_inventory_item_id   => p_Inventory_Item_ID,
               p_organization_id     => p_organization_id,
               x_return_status       => x_return_status,
               x_errorcode           => x_errorcode,
               x_msg_count           => x_msg_count,
               x_msg_data            => x_msg_data);

            IF X_Return_Status <> 'S'
            THEN
               FOR K IN 1 .. X_MSG_COUNT
               LOOP
                  X_MSG_DATA :=
                     FND_MSG_PUB.GET (P_MSG_INDEX => K, P_ENCODED => 'F');
               END LOOP;

               XXWC_GEN_ROUTINES_PKG.LOG_MSG (
                  p_debug_level   => G_LEVEL_ERROR,
                  p_mod_name      => l_Module_Name,
                  P_DEBUG_MSG     =>    'Error while Updating Item Category : '
                                     || x_Msg_Data);
            ELSE
               X_Msg_Data := 'Updating Item to Item Category was successful ';
               XXWC_GEN_ROUTINES_PKG.LOG_MSG (
                  p_debug_level   => G_LEVEL_ERROR,
                  p_mod_name      => l_Module_Name,
                  P_DEBUG_MSG     => x_Msg_Data);
            END IF;
         END IF;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            BEGIN
               SELECT category_set_id
                 INTO l_Category_Set_ID
                 FROM MTL_CATEGORY_SETS_V
                WHERE category_set_name = 'Inventory Category';

               INV_ITEM_CATEGORY_PUB.Create_Category_Assignment (
                  p_api_version         => 1.0,
                  p_init_msg_list       => 'T',
                  p_commit              => 'T',
                  p_category_id         => p_category_id,
                  p_category_set_id     => l_category_set_id,
                  p_inventory_item_id   => p_Inventory_Item_ID,
                  p_organization_id     => p_organization_id,
                  x_return_status       => x_return_status,
                  x_errorcode           => x_errorcode,
                  x_msg_count           => x_msg_count,
                  x_msg_data            => x_msg_data);

               IF X_Return_Status <> 'S'
               THEN
                  FOR K IN 1 .. X_MSG_COUNT
                  LOOP
                     X_MSG_DATA :=
                        FND_MSG_PUB.GET (P_MSG_INDEX => K, P_ENCODED => 'F');
                  END LOOP;

                  XXWC_GEN_ROUTINES_PKG.LOG_MSG (
                     p_debug_level   => G_LEVEL_ERROR,
                     p_mod_name      => l_Module_Name,
                     P_DEBUG_MSG     =>    'Error while assigning item to Item Category : '
                                        || x_Msg_Data);
               ELSE
                  X_Msg_Data := 'Assinging Item to Category was successful ';
                  XXWC_GEN_ROUTINES_PKG.LOG_MSG (
                     p_debug_level   => G_LEVEL_ERROR,
                     p_mod_name      => l_Module_Name,
                     P_DEBUG_MSG     => x_Msg_Data);
               END IF;
            EXCEPTION
               WHEN OTHERS
               THEN
                  x_Return_Status := Fnd_API.G_RET_STS_UNEXP_ERROR;
                  XXWC_GEN_ROUTINES_PKG.LOG_MSG (
                     p_debug_level   => G_LEVEL_ERROR,
                     p_mod_name      => l_Module_Name,
                     P_DEBUG_MSG     =>    'When Others Error while creating Category Assignement : '
                                        || SQLERRM);
            END;
      END;

      COMMIT;
   END ASSIGN_INV_CATEGORY;


   PROCEDURE ASSIGN_CATEGORY (p_category_id         IN            NUMBER,
                              p_organization_id     IN            NUMBER,
                              p_Inventory_Item_ID   IN            NUMBER,
                              x_Return_Status          OUT NOCOPY VARCHAR2,
                              x_Msg_Data               OUT NOCOPY VARCHAR2,
                              x_Msg_Count              OUT NOCOPY NUMBER)
   IS
      x_message_list                Error_Handler.Error_Tbl_Type;

      x_errorcode                   VARCHAR2 (2000);
      l_category_id                 NUMBER;
      l_Category_Set_ID             NUMBER;
      l_structure_id                NUMBER;
      l_MSg_Count                   NUMBER;
      l_API_NAME                    VARCHAR2 (30) := 'ASSIGN_CATEGORY';
      l_Module_Name                 VARCHAR2 (120);
      l_MULT_ITEM_CAT_ASSIGN_FLAG   VARCHAR2 (1);
   BEGIN
      -- Begining of API
      l_Module_Name := G_MODULE_NAME || '.' || l_API_NAME;
      -- Initialize Return Status Variable
      X_RETURN_STATUS := Fnd_API.G_RET_STS_SUCCESS;
      XXWC_GEN_ROUTINES_PKG.LOG_MSG (p_debug_level   => G_LEVEL_STATEMENT,
                                     p_mod_name      => l_Module_Name,
                                     P_DEBUG_MSG     => 'At begining of API');

      SELECT Category_Set_ID, structure_id
        INTO l_Category_Set_ID, l_structure_id
        FROM MTL_CATEGORY_SETS_V ms
       WHERE ms.category_set_name = 'Sales Velocity';

      SELECT category_id
        INTO l_category_id
        FROM MTL_CATEGORIES_V
       WHERE structure_id = l_structure_id AND segment1 = 'N';

      INV_ITEM_CATEGORY_PUB.Create_Category_Assignment (
         p_api_version         => 1.0,
         p_init_msg_list       => 'T',
         p_commit              => 'T',
         p_category_id         => l_category_id,
         p_category_set_id     => l_category_set_id,
         p_inventory_item_id   => p_Inventory_Item_ID,
         p_organization_id     => p_organization_id,
         x_return_status       => x_return_status,
         x_errorcode           => x_errorcode,
         x_msg_count           => x_msg_count,
         x_msg_data            => x_msg_data);

      IF X_Return_Status <> 'S'
      THEN
         FOR K IN 1 .. X_MSG_COUNT
         LOOP
            X_MSG_DATA := FND_MSG_PUB.GET (P_MSG_INDEX => K, P_ENCODED => 'F');
         END LOOP;

         XXWC_GEN_ROUTINES_PKG.LOG_MSG (
            p_debug_level   => G_LEVEL_ERROR,
            p_mod_name      => l_Module_Name,
            P_DEBUG_MSG     =>    'Error while assigning item to Item Category : '
                               || x_Msg_Data);
      ELSE
         X_Msg_Data := 'Assinging Item to Category was successful ';
         XXWC_GEN_ROUTINES_PKG.LOG_MSG (p_debug_level   => G_LEVEL_ERROR,
                                        p_mod_name      => l_Module_Name,
                                        P_DEBUG_MSG     => x_Msg_Data);
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         x_Return_Status := Fnd_API.G_RET_STS_UNEXP_ERROR;
         XXWC_GEN_ROUTINES_PKG.LOG_MSG (
            p_debug_level   => G_LEVEL_ERROR,
            p_mod_name      => l_Module_Name,
            P_DEBUG_MSG     =>    'When Others Error while creating Category Assignement : '
                               || SQLERRM);
   END ASSIGN_CATEGORY;

  /*************************************************************************************************************************
   *   Procedure : CREATE_ITEM
   *
   *   PURPOSE:   Procedure use to create item 
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        12/01/2011  Shankar Hariharan        Initial Version
   *   2.3        11/30/2016  P.Vamshidhar             TMS#20160122-00091 - GL Code Enhancements to leverage GL API 
   *************************************************************************************************************************/
   
   PROCEDURE CREATE_ITEM (
      p_Parts_On_Fly_Rec   IN OUT NOCOPY Parts_On_Fly_Rec,
      x_Return_Status         OUT NOCOPY VARCHAR2,
      x_Msg_Data              OUT NOCOPY VARCHAR2,
      x_Msg_Count             OUT NOCOPY NUMBER)
   IS
      l_Item_Rec                        Inv_Item_Grp.Item_Rec_Type;
      l_Item_Rec_in                     Inv_Item_Grp.Item_Rec_Type;
      l_Item_rec_out                    Inv_Item_Grp.Item_rec_Type;
      l_revision_rec                    INV_ITEM_GRP.Item_Revision_Rec_Type;
      l_rev_index_failure               BOOLEAN := FALSE;
      lx_item_Rec                       Inv_Item_Grp.Item_rec_Type;
      l_error_TBL                       Inv_Item_Grp.Error_Tbl_Type;
      lx_error_TBL                      Inv_Item_Grp.Error_Tbl_Type;
      lx_Message_List                   Error_Handler.Error_Tbl_Type;
      lx_Return_Status                  VARCHAR2 (1);
      lx_Msg_Data                       VARCHAR2 (2000);
      lx_msg_Count                      NUMBER;
      l_result                          BOOLEAN;
      l_phase                           VARCHAR2 (500) := NULL;
      l_status                          VARCHAR2 (500) := NULL;
      l_devphase                        VARCHAR2 (500) := 'PENDING';
      l_devstatus                       VARCHAR2 (500) := NULL;
      l_message                         VARCHAR2 (500) := NULL;
      l_API_NAME                        VARCHAR2 (30) := 'CREATE_ITEM';
      l_Request_ID                      NUMBER;
      l_Module_Name                     VARCHAR2 (120);
      l_Import_Item_Cost_Only           NUMBER := 1;  --Import item costs only
      l_Cost_RUN_Option_Create          NUMBER := 1; -- Insert new cost information only
      l_Group_RUn_Option_Specific       NUMBER := 1; -- Run for Specific group ID
      l_Cost_TYpe_Avg_Rate              VARCHAR2 (30) := 'Avg Rates';
      l_Delete_Rows_Yes                 NUMBER := 1; -- Delete Rows after successful Import
      l_Profile_Status                  BOOLEAN;
      C_CROSS_REFERENCE_TYPE   CONSTANT VARCHAR2 (30) := 'VENDOR';
      l_organization_id                 NUMBER := g_organization_id;
      l_cogs_acc_id                     NUMBER;
      l_sales_acc_id                    NUMBER;
   BEGIN
      -- Begining of API
      l_Module_Name := G_MODULE_NAME || '.' || l_API_NAME;
      -- Initialize Return Status Variable
      X_RETURN_STATUS := Fnd_API.G_RET_STS_SUCCESS;

      XXWC_GEN_ROUTINES_PKG.LOG_MSG (p_debug_level   => G_LEVEL_STATEMENT,
                                     p_mod_name      => l_Module_Name,
                                     P_DEBUG_MSG     => 'At begining of API');

      -- Assign Parts_ON_Fly Record Structure values to Item_REC .

      l_item_rec.segment1 := p_Parts_On_Fly_Rec.ITEM_NUMBER;
      l_item_rec.organization_id := p_Parts_On_Fly_Rec.MST_ORGANIZATION_ID;
      l_item_rec.description := p_Parts_On_Fly_Rec.ITEM_DESCRIPTION;
      l_item_rec.long_description := p_Parts_On_Fly_Rec.long_DESCRIPTION;
      l_item_rec.primary_uom_code := p_Parts_On_Fly_Rec.PRIMARY_UOM_CODE;
      --l_item_rec.list_price_per_unit := p_Parts_On_Fly_Rec.LIST_PRICE;
      l_item_rec.Weight_UOM_CODE := 'LBS'; --p_Parts_On_Fly_Rec.Weight_UOM_CODE ;
      l_Item_Rec.Unit_Weight := P_Parts_ON_Fly_Rec.UNIT_WEIGHT;
      l_Item_Rec.Buyer_ID := P_Parts_ON_Fly_Rec.default_Buyer;
      --l_Item_Rec.template_id         := P_Parts_ON_Fly_Rec.item_template_id;
      l_Item_Rec.shelf_life_days := P_Parts_ON_Fly_Rec.shelf_life_days;
      l_Item_Rec.HAZARDOUS_MATERIAL_FLAG :=
         P_Parts_On_Fly_Rec.HAZARDOUS_MATERIAL_FLAG;
      l_Item_Rec.attribute_category := 'WC';
      l_Item_Rec.attribute11 := P_Parts_On_Fly_Rec.HAZARDOUS_MATERIAL_FLAG;
      l_Item_Rec.attribute9 := P_Parts_On_Fly_Rec.packaging_group;
      l_Item_Rec.attribute18 := P_Parts_On_Fly_Rec.container_Type;
      l_Item_Rec.attribute8 := P_Parts_On_Fly_Rec.msds_sheets;
      l_Item_Rec.attribute1 := P_Parts_On_Fly_Rec.ca_prop65_value;
      l_Item_Rec.attribute3 := P_Parts_On_Fly_Rec.pesticide_flag;
      l_Item_Rec.attribute5 := P_Parts_On_Fly_Rec.pesticide_flag_state;
      l_Item_Rec.attribute4 := P_Parts_On_Fly_Rec.voc_value;
      l_Item_Rec.attribute6 := P_Parts_On_Fly_Rec.voc_category;
      l_Item_Rec.attribute7 := P_Parts_On_Fly_Rec.voc_sub_category;
      l_Item_Rec.attribute10 := P_Parts_On_Fly_Rec.country_of_origin;
      l_Item_Rec.fixed_lot_multiplier :=
         P_Parts_On_Fly_Rec.fixed_lot_multiplier;
      l_Item_Rec.LIST_PRICE_PER_UNIT := P_Parts_On_Fly_Rec.po_cost;
      l_Item_Rec.full_lead_time := P_Parts_On_Fly_Rec.lead_time;

      SELECT DECODE (P_Parts_On_Fly_Rec.sourced_from,
                     'Branch Direct', 2,
                     'DC', 1,
                     NULL)
        INTO l_Item_Rec.source_type
        FROM DUAL;

      -- Added by Shankar 10-Jun-2012 per request from Lee Spitzer
      BEGIN
         SELECT item_catalog_group_id
           INTO l_Item_Rec.item_catalog_group_id
           FROM mtl_item_catalog_Groups_b
          WHERE segment1 = 'WHITE CAP';
      EXCEPTION
         WHEN OTHERS
         THEN
            NULL;
      END;

      IF NVL (P_Parts_ON_Fly_Rec.shelf_life_days, 0) > 0
      THEN
         l_Item_Rec.shelf_life_code := 1;
      ELSE
         l_Item_Rec.shelf_life_code := 4;
      END IF;

      IF P_Parts_ON_Fly_Rec.hazard_class IS NOT NULL
      THEN
         SELECT hazard_class_id
           INTO l_Item_Rec.hazard_Class_id
           FROM po_hazard_classes
          WHERE UPPER (hazard_class) =
                   UPPER (P_Parts_ON_Fly_Rec.hazard_class);
      END IF;

      --l_Item_Rec.tax_code := P_Parts_ON_Fly_Rec.tax_code;
      l_Item_Rec.attribute22 := P_Parts_ON_Fly_Rec.tax_code;    -- 14-Feb-2012


      IF P_Parts_ON_Fly_Rec.un_number IS NOT NULL
      THEN
         SELECT un_number_id
           INTO l_Item_Rec.un_number_id
           FROM po_un_numbers
          WHERE UPPER (un_number) = UPPER (P_Parts_ON_Fly_Rec.un_number);
      END IF;

      l_item_rec.inventory_item_status_code := 'New-EHS';
      l_item_rec.Allow_Item_Desc_Update_Flag := 'Y';

      -- Call Create_Item API from INV_ITEM_GRP API

      INV_ITEM_GRP.Create_Item (
         P_Commit             => fnd_api.g_true,
         P_Validation_Level   => fnd_api.g_valid_level_full,
         P_Item_Rec           => l_Item_Rec,
         p_template_id        => p_Parts_On_Fly_Rec.ITEM_TEMPLATE_ID,
         X_Item_Rec           => lx_item_Rec,
         X_Return_Status      => lx_Return_Status,
         X_Error_TBL          => lx_error_tbl);


      IF (lx_return_status = 'S')
      THEN
         -- Commit Item Creation API
         COMMIT;
         XXWC_GEN_ROUTINES_PKG.LOG_MSG (
            p_debug_level   => G_LEVEL_PROCEDURE,
            p_mod_name      => l_Module_Name,
            P_DEBUG_MSG     => 'Procedure INV_ITEM_GRP.Create_Item processed successfully');
      ELSE
         --x_Return_Status := 'E' ;

         lx_msg_Data := 'Error creating item...' || CHR (13) || CHR (10);

         FOR i IN 1 .. lx_error_tbl.COUNT
         LOOP
            lx_Msg_Data :=
                  x_msg_Data
               || lx_error_tbl (i).message_name
               || ' - '
               || lx_error_tbl (i).MESSAGE_TEXT
               || CHR (13)
               || CHR (10);
         END LOOP;

         XXWC_GEN_ROUTINES_PKG.LOG_MSG (p_debug_level   => G_LEVEL_ERROR,
                                        p_mod_name      => l_Module_Name,
                                        P_DEBUG_MSG     => lx_Msg_Data);
         -- Exit the Process
         x_Return_Status := 'E';
         x_Msg_Data := lx_Msg_Data;
         RETURN;
      END IF;

      -- Assign Item To Item Category
      IF p_Parts_On_Fly_Rec.Category_ID IS NOT NULL
      THEN
         XXWC_INV_NEW_PROD_REQ_PKG.ASSIGN_INV_CATEGORY (
            p_category_id         => p_Parts_On_Fly_Rec.Category_ID,
            p_organization_id     => p_Parts_On_Fly_Rec.Mst_Organization_ID,
            p_Inventory_Item_ID   => lx_item_Rec.INVENTORY_ITEM_ID,
            x_Return_Status       => lx_Return_Status,
            x_Msg_Data            => lx_Msg_Data,
            x_Msg_Count           => lx_Msg_Count);

         IF lx_Return_status = Fnd_API.G_RET_STS_SUCCESS
         THEN
            XXWC_GEN_ROUTINES_PKG.LOG_MSG (
               p_debug_level   => G_LEVEL_PROCEDURE,
               p_mod_name      => l_Module_Name,
               P_DEBUG_MSG     => 'Assign Inv category API call is successful');
         ELSE
            XXWC_GEN_ROUTINES_PKG.LOG_MSG (
               p_debug_level   => G_LEVEL_ERROR,
               p_mod_name      => l_Module_Name,
               P_DEBUG_MSG     =>    'Assign Inv Category errored out :'
                                  || lx_MSg_Data);
            x_Return_Status := 'E';
            x_Msg_Data := x_Msg_Data || lx_Msg_Data;
         END IF;
      ELSE
         XXWC_GEN_ROUTINES_PKG.LOG_MSG (
            p_debug_level   => G_LEVEL_STATEMENT,
            p_mod_name      => l_Module_Name,
            P_DEBUG_MSG     => 'Item Category Infomration is not provided');
      END IF;

      -- Assign Item to Assignment ORganization
      -- Call This API only When ASSGN_ORGANIZATION_ID has a Value
      FOR c1_rec IN (SELECT organization_id
                       FROM xxwc_inv_product_org
                      WHERE request_id = P_Parts_ON_Fly_Rec.request_id)
      LOOP
         /*
           EGO_ITEM_PUB.Assign_Item_To_Org (
              p_api_version         => 1.0
            , p_init_msg_list       => FND_API.G_TRUE
            , p_commit              => FND_API.G_TRUE
            , p_Inventory_Item_Id   => lx_item_Rec.INVENTORY_ITEM_ID
            , p_Item_Number         => NULL
            , p_Organization_Id     => c1_rec.ORGANIZATION_ID
            , p_Organization_Code   => NULL
            , p_Primary_Uom_Code    => P_Parts_ON_Fly_Rec.PRIMARY_UOM_CODE
            , x_return_status       => lx_return_status
            , x_msg_count           => lx_msg_count);
          */
         -- using a different procedure so item cogs and revenue account can be passed
         l_Error_tbl.DELETE;

         l_Item_rec_in.INVENTORY_ITEM_ID := lx_item_Rec.INVENTORY_ITEM_ID;
         l_Item_rec_in.ORGANIZATION_ID := c1_rec.organization_id;
         l_Item_rec_in.PRIMARY_UOM_CODE := P_Parts_ON_Fly_Rec.PRIMARY_UOM_CODE;
         --l_item_rec_in.list_price_per_unit    :=  l_item_cost;
         -- ADded by Shankar to derive COGS and Sales account
         l_item_rec_in.cost_of_sales_account :=
            get_gl_account (lx_item_Rec.INVENTORY_ITEM_ID,
                            c1_rec.organization_id,
                            'C');
         l_item_rec_in.sales_account :=
            get_gl_account (lx_item_Rec.INVENTORY_ITEM_ID,
                            c1_rec.organization_id,
                            'S');
         -- Below code added in Rev 2.3							   
         l_item_rec_in.expense_account :=
            get_gl_account (lx_item_Rec.INVENTORY_ITEM_ID,
                            c1_rec.organization_id,
                            'E');


         --Start : Added revision record processing

         INV_ITEM_GRP.Create_Item (p_commit          => 'T',
                                   p_Item_rec        => l_Item_rec_in,
                                   p_Revision_rec    => l_revision_rec,
                                   p_Template_Id     => NULL,
                                   p_Template_Name   => NULL,
                                   x_Item_rec        => l_Item_rec_out,
                                   x_return_status   => lx_return_status,
                                   x_Error_tbl       => l_Error_tbl);

         IF (lx_return_status = 'S')
         THEN
            -- Commit Item Creation API
            COMMIT;
            XXWC_GEN_ROUTINES_PKG.LOG_MSG (
               p_debug_level   => G_LEVEL_PROCEDURE,
               p_mod_name      => l_Module_Name,
               P_DEBUG_MSG     => 'Procedure EGO_ITEM_PUB.Assign_Item_To_Org processed successfully');
         ELSE
            FOR errno IN 1 .. NVL (l_Error_tbl.LAST, 0)
            LOOP
               IF (l_Error_tbl (errno).MESSAGE_TEXT IS NOT NULL)
               THEN
                  lx_msg_data :=
                     l_Error_tbl (errno).MESSAGE_TEXT || '::' || lx_msg_data;
               ELSE
                  lx_msg_data :=
                     l_Error_tbl (errno).MESSAGE_NAME || '::' || lx_msg_data;
               END IF;
            END LOOP;                                           -- l_Error_tbl

            lX_Msg_Data :=
                  'Error Assigning item to assignment organization'
               || CHR (13)
               || CHR (10);
            XXWC_GEN_ROUTINES_PKG.LOG_MSG (p_debug_level   => G_LEVEL_ERROR,
                                           p_mod_name      => l_Module_Name,
                                           P_DEBUG_MSG     => lx_Msg_Data);
            x_Return_Status := 'E';
            x_Msg_Data := x_Msg_Data || lx_Msg_Data;
            RETURN;
         END IF;
      END LOOP;

      -- Create Vendor Part Number
      -- Manufracturer Part Number is nOt provided then Do not Call Create Cross Reference API
      IF p_Parts_On_Fly_Rec.Mfg_Part_Number IS NOT NULL
      THEN
         XXWC_INV_NEW_PROD_REQ_PKG.CREATE_CROSS_REF (
            p_segment1               => p_Parts_On_Fly_Rec.Item_Number,
            p_cross_reference_type   => 'VENDOR',
            p_cross_reference        => p_Parts_On_Fly_Rec.Mfg_Part_Number,
            p_description            => p_Parts_On_Fly_Rec.Item_Description);

         XXWC_GEN_ROUTINES_PKG.LOG_MSG (
            p_debug_level   => G_LEVEL_PROCEDURE,
            p_mod_name      => l_Module_Name,
            P_DEBUG_MSG     => 'After Call to API CREATE_CROSS_REF');
      END IF;

      IF p_Parts_On_Fly_Rec.Upc_Number IS NOT NULL
      THEN
         XXWC_INV_NEW_PROD_REQ_PKG.CREATE_CROSS_REF (
            p_segment1               => p_Parts_On_Fly_Rec.Item_Number,
            p_cross_reference_type   => 'UPC',
            p_cross_reference        => p_Parts_On_Fly_Rec.UPC_Number,
            p_description            => p_Parts_On_Fly_Rec.Item_Description);

         XXWC_GEN_ROUTINES_PKG.LOG_MSG (
            p_debug_level   => G_LEVEL_PROCEDURE,
            p_mod_name      => l_Module_Name,
            P_DEBUG_MSG     => 'After Call to API CREATE_CROSS_REF');
      END IF;


      IF p_Parts_On_Fly_Rec.replaced_item_id IS NOT NULL
      THEN
         XXWC_INV_NEW_PROD_REQ_PKG.CREATE_ITEM_REL (
            p_item_id       => lx_item_Rec.INVENTORY_ITEM_ID,
            p_rel_item_id   => p_Parts_On_Fly_Rec.replaced_item_id);

         XXWC_GEN_ROUTINES_PKG.LOG_MSG (
            p_debug_level   => G_LEVEL_PROCEDURE,
            p_mod_name      => l_Module_Name,
            P_DEBUG_MSG     => 'After Call to API CREATE_CROSS_REF');
      END IF;

      p_Parts_On_Fly_Rec.INVENTORY_ITEM_ID := lx_item_Rec.INVENTORY_ITEM_ID;
   EXCEPTION
      WHEN OTHERS
      THEN
         X_Return_Status := Fnd_API.G_RET_STS_ERROR;
         X_Msg_Data := 'When Others Error :' || SQLERRM;
         XXWC_GEN_ROUTINES_PKG.LOG_MSG (p_debug_level   => G_LEVEL_PROCEDURE,
                                        p_mod_name      => l_Module_Name,
                                        P_DEBUG_MSG     => lx_Msg_Data);
   END CREATE_ITEM;



   PROCEDURE Launch_CreateItem_WF (itemtype    IN            VARCHAR2,
                                   itemkey     IN            VARCHAR2,
                                   actid       IN            NUMBER,
                                   funcmode    IN            VARCHAR2,
                                   resultout      OUT NOCOPY VARCHAR2)
   IS
      x_progress        VARCHAR2 (100);
      l_part_type_rec   PARTS_ON_FLY_REC;
      l_request_id      NUMBER;
      l_Return_Status   VARCHAR2 (30);
      l_Msg_Data        VARCHAR2 (2000);
      l_Msg_Count       NUMBER;
      l_error_msg       VARCHAR2 (2000);
   BEGIN
      -- Do nothing in cancel or timeout mode
      --

      IF (funcmode <> wf_engine.eng_run)
      THEN
         resultout := wf_engine.eng_null;
         RETURN;
      END IF;

      l_request_id :=
         wf_engine.GetItemAttrNumber (itemtype   => itemtype,
                                      itemkey    => itemkey,
                                      aname      => 'REQUEST_ID');

      SELECT REQUEST_ID,
             PART_TYPE_ID,
             ITEM_NUMBER,                                    --SP_ITEM_NUMBER,
             MFG_PART_NUMBER,
             PART_DESCRIPTION,
             PRIMARY_UOM,
             LONG_DESCRIPTION,
             SHELF_LIFE_DAYS,
             UNIT_WEIGHT,
             TAX_CODE,
             HAZARDOUS_ITEM_FLAG,
             HAZARD_CLASS,
             PACKAGING_GROUP,
             UN_NUMBER,
             CONTAINER_TYPE,
             PACKAGE_LABELS,
             MSDS_SHEETS,
             CA_PROP65_VALUE,
             PESTICIDE_FLAG,
             PESTICIDE_FLAG_STATE,
             VOC_VALUE,
             VOC_CATEGORY,
             VOC_SUB_CATEGORY,
             PO_COST,
             FIXED_LOT_MULTIPLIER,
             DEFAULT_BUYER,
             COUNTRY_OF_ORIGIN,
             REPLACED_ITEM_ID,
             LEAD_TIME,
             CATEGORY_ID,
             UPC_NUMBER,
             VENDOR_ID,
             VENDOR_SITE_ID,
             SOURCED_FROM,
             SUGG_LIST_PRICE
        INTO l_part_type_rec.REQUEST_ID,
             l_part_type_rec.ITEM_TEMPLATE_ID,
             l_part_type_rec.ITEM_NUMBER,    --l_part_type_rec.SP_ITEM_NUMBER,
             l_part_type_rec.MFG_PART_NUMBER,
             l_part_type_rec.ITEM_DESCRIPTION,
             l_part_type_rec.PRIMARY_UOM_CODE,
             l_part_type_rec.LONG_DESCRIPTION,
             l_part_type_rec.SHELF_LIFE_DAYS,
             l_part_type_rec.UNIT_WEIGHT,
             l_part_type_rec.TAX_CODE,
             l_part_type_rec.HAZARDOUS_MATERIAL_FLAG,
             l_part_type_rec.HAZARD_CLASS,
             l_part_type_rec.PACKAGING_GROUP,
             l_part_type_rec.UN_NUMBER,
             l_part_type_rec.CONTAINER_TYPE,
             l_part_type_rec.PACKAGE_LABELS,
             l_part_type_rec.MSDS_SHEETS,
             l_part_type_rec.CA_PROP65_VALUE,
             l_part_type_rec.PESTICIDE_FLAG,
             l_part_type_rec.PESTICIDE_FLAG_STATE,
             l_part_type_rec.VOC_VALUE,
             l_part_type_rec.VOC_CATEGORY,
             l_part_type_rec.VOC_SUB_CATEGORY,
             l_part_type_rec.PO_COST,
             l_part_type_rec.FIXED_LOT_MULTIPLIER,
             l_part_type_rec.DEFAULT_BUYER,
             l_part_type_rec.COUNTRY_OF_ORIGIN,
             l_part_type_rec.REPLACED_ITEM_ID,
             l_part_type_rec.LEAD_TIME,
             l_part_type_rec.CATEGORY_ID,
             l_part_type_rec.UPC_NUMBER,
             l_part_type_rec.VENDOR_ID,
             l_part_type_rec.VENDOR_SITE_ID,
             l_part_type_rec.SOURCED_FROM,
             l_part_type_rec.SUGG_LIST_PRICE
        FROM xxwc_inv_product_request
       WHERE request_id = l_request_id;

      l_part_type_rec.mst_organization_id := g_organization_id;
      CREATE_ITEM (p_Parts_On_Fly_Rec   => l_part_type_rec,
                   x_Return_Status      => l_return_status,
                   x_Msg_Data           => l_msg_data,
                   x_Msg_Count          => l_msg_count);

      IF l_return_status = 'S'
      THEN
         -- Revision 1.9 Added by Manjula on 29-Apr-14 for TMS 20140407-00238  Begin

         POPULATE_PRODUCT_UDA (l_part_type_rec.inventory_item_id,
                               l_error_msg);

         IF l_error_msg IS NOT NULL
         THEN
            l_msg_data := l_msg_data || l_error_msg;
         END IF;

         --Revision 1.9 Added by Manjula on 29-Apr-14 for TMS 20140407-00238  End

         UPDATE xxwc_inv_product_request
            SET inventory_item_id = l_part_type_rec.inventory_item_id,
                wf_status_code = 'New-EHS',
                error_msg = SUBSTR (l_error_msg, 1, 2000)
          -- Above line Revision 1.9 Added by Manjula on 29-Apr-14 for TMS 20140407-00238
          WHERE request_id = l_part_type_rec.request_id;

         resultout := wf_engine.eng_completed || ':' || 'Y';
      ELSE
         UPDATE xxwc_inv_product_request
            SET wf_status_code = 'Error',
                error_msg = SUBSTR (l_msg_data, 1, 2000)
          WHERE request_id = l_part_type_rec.request_id;

         resultout := wf_engine.eng_completed || ':' || 'N';
      END IF;
   END Launch_CreateItem_WF;


   PROCEDURE data_Steward_reject (itemtype    IN            VARCHAR2,
                                  itemkey     IN            VARCHAR2,
                                  actid       IN            NUMBER,
                                  funcmode    IN            VARCHAR2,
                                  resultout      OUT NOCOPY VARCHAR2)
   IS
      l_request_id   NUMBER;
   BEGIN
      -- Do nothing in cancel or timeout mode
      --
      IF (funcmode <> wf_engine.eng_run)
      THEN
         resultout := wf_engine.eng_null;
         RETURN;
      END IF;

      l_request_id :=
         wf_engine.GetItemAttrNumber (itemtype   => itemtype,
                                      itemkey    => itemkey,
                                      aname      => 'REQUEST_ID');

      UPDATE xxwc_inv_product_request
         SET wf_status_code = 'REJECT',
             error_msg = 'Rejected by Data Steward'
       WHERE request_id = l_request_id;

      resultout := wf_engine.eng_completed || ':' || 'ACTIVITY_PERFORMED';
   END data_Steward_reject;


   PROCEDURE ehs_review (itemtype    IN            VARCHAR2,
                         itemkey     IN            VARCHAR2,
                         actid       IN            NUMBER,
                         funcmode    IN            VARCHAR2,
                         resultout      OUT NOCOPY VARCHAR2)
   IS
      l_request_id       NUMBER;
      l_Item_Rec         Inv_Item_Grp.Item_Rec_Type;
      l_Item_Rec1        Inv_Item_Grp.Item_Rec_Type;
      lx_item_Rec        Inv_Item_Grp.Item_rec_Type;
      lx_error_TBL       Inv_Item_Grp.Error_Tbl_Type;
      lx_Message_List    Error_Handler.Error_Tbl_Type;
      lx_Return_Status   VARCHAR2 (1);
      lx_Msg_Data        VARCHAR2 (2000);
      lx_msg_Count       NUMBER;
      l_item_id          NUMBER;
      l_message          VARCHAR2 (500) := NULL;
      l_API_NAME         VARCHAR2 (30) := 'UPDATE_ITEM';
      l_Module_Name      VARCHAR2 (120);
   BEGIN
      -- Do nothing in cancel or timeout mode
      --
      IF (funcmode <> wf_engine.eng_run)
      THEN
         resultout := wf_engine.eng_null;
         RETURN;
      END IF;

      l_request_id :=
         wf_engine.GetItemAttrNumber (itemtype   => itemtype,
                                      itemkey    => itemkey,
                                      aname      => 'REQUEST_ID');

      SELECT inventory_item_id
        INTO l_item_id
        FROM xxwc_inv_product_request
       WHERE request_id = l_request_id;

      FOR c1_Rec IN (SELECT g_organization_id organization_id FROM DUAL
                     UNION
                     SELECT organization_id
                       FROM xxwc_inv_product_org
                      WHERE request_id = l_request_id)
      LOOP
         l_item_rec := l_item_rec1;
         lx_item_rec := l_item_rec1;
         l_item_rec.inventory_item_id := l_item_id;
         l_item_rec.inventory_item_status_code := 'New-Purc';
         l_item_rec.organization_id := c1_rec.organization_id;
         inv_item_grp.update_item (
            p_commit             => fnd_api.g_true,
            p_lock_rows          => fnd_api.g_true,
            p_validation_level   => fnd_api.g_valid_level_full,
            p_item_rec           => l_item_rec,
            x_item_rec           => lx_item_rec,
            x_return_status      => lx_return_status,
            x_error_tbl          => lx_error_tbl);
      END LOOP;

      IF (lx_return_status = 'S')
      THEN
         -- Commit Item Creation API
         UPDATE xxwc_inv_product_request
            SET wf_status_code = 'New-Purc'
          WHERE request_id = l_request_id;

         COMMIT;
         XXWC_GEN_ROUTINES_PKG.LOG_MSG (
            p_debug_level   => G_LEVEL_PROCEDURE,
            p_mod_name      => l_Module_Name,
            P_DEBUG_MSG     => 'Procedure INV_ITEM_GRP.Update_Item processed successfully');
         resultout := wf_engine.eng_completed || ':' || 'ACTIVITY_PERFORMED';
      ELSE
         --x_Return_Status := 'E' ;

         lx_msg_Data := 'Error updating item...' || CHR (13) || CHR (10);

         FOR i IN 1 .. lx_error_tbl.COUNT
         LOOP
            lx_Msg_Data :=
                  lx_msg_Data
               || lx_error_tbl (i).message_name
               || ' - '
               || lx_error_tbl (i).MESSAGE_TEXT
               || CHR (13)
               || CHR (10);
         END LOOP;

         XXWC_GEN_ROUTINES_PKG.LOG_MSG (p_debug_level   => G_LEVEL_ERROR,
                                        p_mod_name      => l_Module_Name,
                                        P_DEBUG_MSG     => lx_Msg_Data);

         -- Exit the Process
         --x_Return_Status := 'E';
         --x_Msg_Data := lx_Msg_Data;
         UPDATE xxwc_inv_product_request
            SET wf_status_code = 'Error',
                error_msg =
                      'Error EHS review of item--'
                   || l_item_rec.organization_id
                   || '-'
                   || l_item_rec.inventory_item_id
                   || '--'
                   || lx_msg_data
          WHERE request_id = l_request_id;

         RETURN;
      END IF;

      /*
            UPDATE xxwc_inv_product_request
               SET wf_status_code = 'New-Purch'
             WHERE request_id = l_request_id;
      */
      resultout := wf_engine.eng_completed || ':' || 'ACTIVITY_PERFORMED';
   END ehs_review;

   PROCEDURE po_review (itemtype    IN            VARCHAR2,
                        itemkey     IN            VARCHAR2,
                        actid       IN            NUMBER,
                        funcmode    IN            VARCHAR2,
                        resultout      OUT NOCOPY VARCHAR2)
   IS
      l_request_id       NUMBER;
      l_Item_Rec         Inv_Item_Grp.Item_Rec_Type;
      l_Item_Rec1        Inv_Item_Grp.Item_Rec_Type;
      lx_item_Rec        Inv_Item_Grp.Item_rec_Type;
      lx_error_TBL       Inv_Item_Grp.Error_Tbl_Type;
      lx_Message_List    Error_Handler.Error_Tbl_Type;
      lx_Return_Status   VARCHAR2 (1);
      lx_Msg_Data        VARCHAR2 (2000);
      lx_msg_Count       NUMBER;
      l_item_id          NUMBER;
      l_message          VARCHAR2 (500) := NULL;
      l_API_NAME         VARCHAR2 (30) := 'UPDATE_ITEM';
      l_Module_Name      VARCHAR2 (120);
   BEGIN
      -- Do nothing in cancel or timeout mode
      --
      IF (funcmode <> wf_engine.eng_run)
      THEN
         resultout := wf_engine.eng_null;
         RETURN;
      END IF;

      l_request_id :=
         wf_engine.GetItemAttrNumber (itemtype   => itemtype,
                                      itemkey    => itemkey,
                                      aname      => 'REQUEST_ID');

      SELECT inventory_item_id
        INTO l_item_id
        FROM xxwc_inv_product_request
       WHERE request_id = l_request_id;

      FOR c1_Rec IN (SELECT g_organization_id organization_id FROM DUAL
                     UNION
                     SELECT organization_id
                       FROM xxwc_inv_product_org
                      WHERE request_id = l_request_id)
      LOOP
         l_item_rec := l_item_rec1;
         lx_item_rec := l_item_rec1;
         l_item_rec.inventory_item_id := l_item_id;
         l_item_rec.inventory_item_status_code := 'New-Price';
         l_item_rec.organization_id := c1_rec.organization_id;
         inv_item_grp.update_item (
            p_commit             => fnd_api.g_true,
            p_lock_rows          => fnd_api.g_true,
            p_validation_level   => fnd_api.g_valid_level_full,
            p_item_rec           => l_item_rec,
            x_item_rec           => lx_item_rec,
            x_return_status      => lx_return_status,
            x_error_tbl          => lx_error_tbl);
      END LOOP;

      IF (lx_return_status = 'S')
      THEN
         -- Commit Item Creation API
         UPDATE xxwc_inv_product_request
            SET wf_status_code = 'New-Price'
          WHERE request_id = l_request_id;

         COMMIT;
         XXWC_GEN_ROUTINES_PKG.LOG_MSG (
            p_debug_level   => G_LEVEL_PROCEDURE,
            p_mod_name      => l_Module_Name,
            P_DEBUG_MSG     => 'Procedure INV_ITEM_GRP.Update_Item processed successfully');
         resultout := wf_engine.eng_completed || ':' || 'ACTIVITY_PERFORMED';
      ELSE
         --x_Return_Status := 'E' ;

         lx_msg_Data := 'Error updating item...' || CHR (13) || CHR (10);

         FOR i IN 1 .. lx_error_tbl.COUNT
         LOOP
            lx_Msg_Data :=
                  lx_msg_Data
               || lx_error_tbl (i).message_name
               || ' - '
               || lx_error_tbl (i).MESSAGE_TEXT
               || CHR (13)
               || CHR (10);
         END LOOP;

         XXWC_GEN_ROUTINES_PKG.LOG_MSG (p_debug_level   => G_LEVEL_ERROR,
                                        p_mod_name      => l_Module_Name,
                                        P_DEBUG_MSG     => lx_Msg_Data);

         -- Exit the Process
         --x_Return_Status := 'E';
         --x_Msg_Data := lx_Msg_Data;
         UPDATE xxwc_inv_product_request
            SET wf_status_code = 'Error',
                error_msg = 'Error PO review of item--' || lx_msg_data
          WHERE request_id = l_request_id;

         RETURN;
      END IF;


      /*
            UPDATE xxwc_inv_product_request
               SET wf_status_code = 'New-Pricing'
             WHERE request_id = l_request_id;
      */
      resultout := wf_engine.eng_completed || ':' || 'ACTIVITY_PERFORMED';
   END po_review;


   PROCEDURE pricing_review (itemtype    IN            VARCHAR2,
                             itemkey     IN            VARCHAR2,
                             actid       IN            NUMBER,
                             funcmode    IN            VARCHAR2,
                             resultout      OUT NOCOPY VARCHAR2)
   IS
      l_request_id       NUMBER;
      l_Item_Rec         Inv_Item_Grp.Item_Rec_Type;
      l_Item_Rec1        Inv_Item_Grp.Item_Rec_Type;
      lx_item_Rec        Inv_Item_Grp.Item_rec_Type;
      lx_error_TBL       Inv_Item_Grp.Error_Tbl_Type;
      lx_Message_List    Error_Handler.Error_Tbl_Type;
      lx_Return_Status   VARCHAR2 (1);
      lx_Msg_Data        VARCHAR2 (2000);
      lx_msg_Count       NUMBER;
      l_item_id          NUMBER;
      l_message          VARCHAR2 (500) := NULL;
      l_API_NAME         VARCHAR2 (30) := 'UPDATE_ITEM';
      l_Module_Name      VARCHAR2 (120);
   BEGIN
      -- Do nothing in cancel or timeout mode
      --
      IF (funcmode <> wf_engine.eng_run)
      THEN
         resultout := wf_engine.eng_null;
         RETURN;
      END IF;

      l_request_id :=
         wf_engine.GetItemAttrNumber (itemtype   => itemtype,
                                      itemkey    => itemkey,
                                      aname      => 'REQUEST_ID');

      SELECT inventory_item_id
        INTO l_item_id
        FROM xxwc_inv_product_request
       WHERE request_id = l_request_id;

      FOR c1_Rec IN (SELECT g_organization_id organization_id FROM DUAL
                     UNION
                     SELECT organization_id
                       FROM xxwc_inv_product_org
                      WHERE request_id = l_request_id)
      LOOP
         l_item_rec := l_item_rec1;
         lx_item_rec := l_item_rec1;
         l_item_rec.inventory_item_id := l_item_id;
         l_item_rec.inventory_item_status_code := 'New-DC';
         l_item_rec.organization_id := c1_rec.organization_id;
         inv_item_grp.update_item (
            p_commit             => fnd_api.g_true,
            p_lock_rows          => fnd_api.g_true,
            p_validation_level   => fnd_api.g_valid_level_full,
            p_item_rec           => l_item_rec,
            x_item_rec           => lx_item_rec,
            x_return_status      => lx_return_status,
            x_error_tbl          => lx_error_tbl);
      END LOOP;

      IF (lx_return_status = 'S')
      THEN
         -- Commit Item Creation API
         UPDATE xxwc_inv_product_request
            SET wf_status_code = 'New-DC'
          WHERE request_id = l_request_id;

         COMMIT;
         XXWC_GEN_ROUTINES_PKG.LOG_MSG (
            p_debug_level   => G_LEVEL_PROCEDURE,
            p_mod_name      => l_Module_Name,
            P_DEBUG_MSG     => 'Procedure INV_ITEM_GRP.Update_Item processed successfully');
         resultout := wf_engine.eng_completed || ':' || 'ACTIVITY_PERFORMED';
      ELSE
         --x_Return_Status := 'E' ;

         lx_msg_Data := 'Error updating item...' || CHR (13) || CHR (10);

         FOR i IN 1 .. lx_error_tbl.COUNT
         LOOP
            lx_Msg_Data :=
                  lx_msg_Data
               || lx_error_tbl (i).message_name
               || ' - '
               || lx_error_tbl (i).MESSAGE_TEXT
               || CHR (13)
               || CHR (10);
         END LOOP;

         XXWC_GEN_ROUTINES_PKG.LOG_MSG (p_debug_level   => G_LEVEL_ERROR,
                                        p_mod_name      => l_Module_Name,
                                        P_DEBUG_MSG     => lx_Msg_Data);

         -- Exit the Process
         --x_Return_Status := 'E';
         --x_Msg_Data := lx_Msg_Data;
         UPDATE xxwc_inv_product_request
            SET wf_status_code = 'Error',
                error_msg = 'Error Pricing review of item--' || lx_msg_data
          WHERE request_id = l_request_id;

         RETURN;
      END IF;

      /*
            UPDATE xxwc_inv_product_request
               SET wf_status_code = 'New-DC'
             WHERE request_id = l_request_id;
      */
      resultout := wf_engine.eng_completed || ':' || 'ACTIVITY_PERFORMED';
   END pricing_review;

   PROCEDURE activate_item (itemtype    IN            VARCHAR2,
                            itemkey     IN            VARCHAR2,
                            actid       IN            NUMBER,
                            funcmode    IN            VARCHAR2,
                            resultout      OUT NOCOPY VARCHAR2)
   IS
      l_request_id       NUMBER;
      l_Item_Rec         Inv_Item_Grp.Item_Rec_Type;
      l_Item_Rec1        Inv_Item_Grp.Item_Rec_Type;
      lx_item_Rec        Inv_Item_Grp.Item_rec_Type;
      lx_error_TBL       Inv_Item_Grp.Error_Tbl_Type;
      lx_Message_List    Error_Handler.Error_Tbl_Type;
      lx_Return_Status   VARCHAR2 (1);
      lx_Msg_Data        VARCHAR2 (2000);
      lx_msg_Count       NUMBER;
      l_item_id          NUMBER;
      l_message          VARCHAR2 (500) := NULL;
      l_API_NAME         VARCHAR2 (30) := 'UPDATE_ITEM';
      l_Module_Name      VARCHAR2 (120);
   BEGIN
      -- Do nothing in cancel or timeout mode
      --
      IF (funcmode <> wf_engine.eng_run)
      THEN
         resultout := wf_engine.eng_null;
         RETURN;
      END IF;

      l_request_id :=
         wf_engine.GetItemAttrNumber (itemtype   => itemtype,
                                      itemkey    => itemkey,
                                      aname      => 'REQUEST_ID');

      SELECT inventory_item_id
        INTO l_item_id
        FROM xxwc_inv_product_request
       WHERE request_id = l_request_id;

      l_item_rec.inventory_item_id := l_item_id;
      l_item_rec.inventory_item_status_code := 'Active';

      --      l_item_rec.organization_id :=181;
      /*
          inv_item_grp.update_item (
               p_commit             => fnd_api.g_true
             , p_lock_rows          => fnd_api.g_true
             , p_validation_level   => fnd_api.g_valid_level_full
             , p_item_rec           => l_item_rec
             , x_item_rec           => lx_item_rec
             , x_return_status      => lx_return_status
             , x_error_tbl          => lx_error_tbl);
            */

      FOR c1_Rec IN (SELECT g_organization_id organization_id FROM DUAL
                     UNION
                     SELECT organization_id
                       FROM xxwc_inv_product_org
                      WHERE request_id = l_request_id)
      LOOP
         l_item_rec := l_item_rec1;
         lx_item_rec := l_item_rec1;
         l_item_rec.inventory_item_id := l_item_id;
         l_item_rec.inventory_item_status_code := 'Active';
         l_item_rec.organization_id := c1_rec.organization_id;
         inv_item_grp.update_item (
            p_commit             => fnd_api.g_true,
            p_lock_rows          => fnd_api.g_true,
            p_validation_level   => fnd_api.g_valid_level_full,
            p_item_rec           => l_item_rec,
            x_item_rec           => lx_item_rec,
            x_return_status      => lx_return_status,
            x_error_tbl          => lx_error_tbl);
      END LOOP;

      IF (lx_return_status = 'S')
      THEN
         -- Commit Item Creation API
         UPDATE xxwc_inv_product_request
            SET wf_status_code = 'Active'
          WHERE request_id = l_request_id;

         COMMIT;
         XXWC_GEN_ROUTINES_PKG.LOG_MSG (
            p_debug_level   => G_LEVEL_PROCEDURE,
            p_mod_name      => l_Module_Name,
            P_DEBUG_MSG     => 'Procedure INV_ITEM_GRP.Update_Item processed successfully');
         resultout := wf_engine.eng_completed || ':' || 'ACTIVITY_PERFORMED';
      ELSE
         --x_Return_Status := 'E' ;

         lx_msg_Data := 'Error updating item...' || CHR (13) || CHR (10);

         FOR i IN 1 .. lx_error_tbl.COUNT
         LOOP
            lx_Msg_Data :=
                  lx_msg_Data
               || lx_error_tbl (i).message_name
               || ' - '
               || lx_error_tbl (i).MESSAGE_TEXT
               || CHR (13)
               || CHR (10);
         END LOOP;

         XXWC_GEN_ROUTINES_PKG.LOG_MSG (p_debug_level   => G_LEVEL_ERROR,
                                        p_mod_name      => l_Module_Name,
                                        P_DEBUG_MSG     => lx_Msg_Data);

         -- Exit the Process
         --x_Return_Status := 'E';
         --x_Msg_Data := lx_Msg_Data;
         UPDATE xxwc_inv_product_request
            SET wf_status_code = 'Error',
                error_msg = 'Error Activating item--' || lx_msg_data
          WHERE request_id = l_request_id;

         RETURN;
      END IF;

      resultout := wf_engine.eng_completed || ':' || 'ACTIVITY_PERFORMED';
   END activate_item;


   PROCEDURE SET_ITEM_NUMBER_NULL (P_TRANSACTION_ID IN NUMBER)
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
      l_Profile_Status   BOOLEAN;
   BEGIN
      DELETE FROM XXWC_SHARE_VARIABLES
            WHERE     Transaction_Type = G_CLONE_PARTS_SO
                  AND Transaction_ID = P_TRANSACTION_ID
                  AND Created_By = Fnd_Global.USer_ID;

      COMMIT;
   END SET_ITEM_NUMBER_NULL;

   PROCEDURE SET_ITEM_NUMBER (P_INVENTORY_ITEM_ID   IN NUMBER,
                              P_ITEM_NUMBER         IN VARCHAR2,
                              P_TRANSACTION_TYPE    IN VARCHAR2,
                              P_ORGANIZATION_ID     IN NUMBER,
                              P_ORGANIZATION_CODE   IN VARCHAR2,
                              P_TRANSACTION_ID      IN NUMBER)
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;

      -- Define a Cursor to update Existing Record
      CURSOR Item_Cur
      IS
         SELECT *
           FROM XXWC_SHARE_VARIABLES
          WHERE     Transaction_TYpe = p_Transaction_TYpe
                AND Inventory_Item_ID = P_INVENTORY_ITEM_ID
                AND TRANSACTION_ID = P_TRANSACTION_ID
                AND Created_By = Fnd_Global.User_ID;

      --And   Organization_ID   = P_Organization_ID
      --And   Item_Number       = p_Item_Number
      l_Record_Found   VARCHAR2 (1) := 'N';
   BEGIN
      FOR Item_Rec IN Item_Cur
      LOOP
         l_Record_Found := 'Y';

         UPDATE XXWC_SHARE_VARIABLES
            SET TRANSACTION_TYPE = p_Transaction_TYpe,
                TRANSACTION_ID = P_TRANSACTION_ID,
                INVENTORY_ITEM_ID = P_INVENTORY_ITEM_ID,
                ITEM_NUMBER = p_Item_Number,
                CREATED_BY = Fnd_Global.User_ID,
                ORGANIZATION_ID = P_Organization_ID
          WHERE     TRANSACTION_ID = p_TRANSACTION_ID
                AND TRANSACTION_TYPE = p_Transaction_TYpe
                AND CREATED_BY = Fnd_Global.User_ID
                AND Inventory_Item_ID = P_INVENTORY_ITEM_ID;
      END LOOP;

      IF L_Record_Found = 'N'
      THEN
         INSERT INTO XXWC_SHARE_VARIABLES (TRANSACTION_TYPE,
                                           TRANSACTION_ID,
                                           INVENTORY_ITEM_ID,
                                           ITEM_NUMBER,
                                           ORGANIZATION_ID,
                                           ORGANIZATION_CODE,
                                           CREATED_BY,
                                           CREATION_DATE)
              VALUES (P_TRANSACTION_TYPE,
                      P_TRANSACTION_ID,
                      P_INVENTORY_ITEM_ID,
                      P_ITEM_NUMBER,
                      P_ORGANIZATION_ID,
                      P_ORGANIZATION_CODE,
                      Fnd_Global.User_ID,
                      SYSDATE);
      END IF;

      COMMIT;
   END;

   FUNCTION GET_ITEM_NUMBER (P_TRANSACTION_ID     IN NUMBER,
                             P_TRANSACTION_TYPE   IN VARCHAR2)
      RETURN VARCHAR2
   IS
      l_Item_Number   VARCHAR2 (40);
   BEGIN
        SELECT Item_Number
          INTO l_ITem_Number
          FROM XXWC_SHARE_VARIABLES
         WHERE     Transaction_ID = P_TRANSACTION_ID
               AND Transaction_Type = p_Transaction_Type
               AND Created_By = FNd_Global.User_ID
               AND ROWNUM = 1
      ORDER BY creation_date ASC;

      RETURN l_ITEM_NUMBER;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END GET_ITEM_NUMBER;

   FUNCTION GET_INVENTORY_ITEM_ID (P_TRANSACTION_ID     IN NUMBER,
                                   P_TRANSACTION_TYPE   IN VARCHAR2)
      RETURN NUMBER
   IS
      l_Inventory_Item_ID   NUMBER;
   BEGIN
        SELECT Inventory_Item_ID
          INTO l_Inventory_Item_ID
          FROM XXWC_SHARE_VARIABLES
         WHERE     Transaction_ID = P_TRANSACTION_ID
               AND Transaction_Type = p_Transaction_Type
               AND Created_By = FNd_Global.User_ID
               AND ROWNUM = 1
      ORDER BY creation_date ASC;

      RETURN l_Inventory_Item_ID;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END GET_INVENTORY_ITEM_ID;


   PROCEDURE process_item_org_assignments (l_trx_id        IN     NUMBER,
                                           l_inv_item_id   IN     NUMBER,
                                           l_org_id        IN     NUMBER,
                                           l_uom_code      IN     VARCHAR2,
                                           l_item_cost     IN     NUMBER,
                                           l_from_org_id   IN     NUMBER,
                                           o_ret_status       OUT VARCHAR2,
                                           o_ret_msg          OUT VARCHAR2)
   IS
      /******************************************************************************************************************************************
       *   PROCEDURE PROCESS_ITEM_ORG_ASSIGNMENTS                                                                                               *
       *                                                                                                                                        *
       *   PURPOSE:   This Procedure is used to clonning inventory Item to Org                                                                  *
       *                                                                                                                                        *
       *                                                                                                                                        *
       *   REVISIONS:                                                                                                                           *
       *   Ver        Date        Author                     Description                                                                        *
       *   ---------  ----------  ---------------         -------------------------                                                             *
       *   2.2        25-SEP-2015  P.Vamshidhar           TMS#20150918-00018  - Correct cloning issue with XXWC_Clone_Parts form                *
       *   2.3        30-NOV-2016  P.Vamshidhar           TMS#20160122-00091 - GL Code Enhancements to leverage GL API                          *       
       *                                                                                                                                        *
       *****************************************************************************************************************************************/
      l_api_name             CONSTANT VARCHAR2 (30) := 'Process_Item_Org_Assignments';
      l_return_status                 VARCHAR2 (1);
      l_item_number                   VARCHAR2 (40);
      l_msg_count                     NUMBER := 0;
      l_Item_Org_Assignment_Rec       EGO_Item_PUB.Item_Org_Assignment_Rec_Type;
      l_Item_rec_in                   INV_ITEM_GRP.Item_Rec_Type;
      l_revision_rec                  INV_ITEM_GRP.Item_Revision_Rec_Type;
      l_rev_index_failure             BOOLEAN := FALSE;
      l_Item_rec_out                  INV_ITEM_GRP.Item_Rec_Type;
      l_Error_tbl                     INV_ITEM_GRP.Error_Tbl_Type;
      l_master_org                    INV.MTL_SYSTEM_ITEMS_B.ORGANIZATION_ID%TYPE;
      l_event_return_status           VARCHAR2 (1); --business event enhancement
      l_msg_data                      VARCHAR2 (2000);
      l_process_control               VARCHAR2 (2000)
         := INV_EGO_REVISION_VALIDATE.Get_Process_Control;      --Bug: 4881908
      l_Item_Org_Assignment_Tbl       EGO_Item_PUB.Item_Org_Assignment_Tbl_Type;
      indx                            BINARY_INTEGER := 1;
      l_request_id                    NUMBER;
      l_Import_Item_Cost_Only         NUMBER := 1;    --Import item costs only
      l_Cost_RUN_Option_Create        NUMBER := 1; -- Insert new cost information only
      l_Group_RUn_Option_Specific     NUMBER := 1; -- Run for Specific group ID
      l_Cost_TYpe_Avg_Rate            VARCHAR2 (30) := 'Avg Rates';
      l_Delete_Rows_Yes               NUMBER := 1; -- Delete Rows after successful Import
      --Added by Shankar 04-Dec-2013 20131121-00084
      l_template_name                 VARCHAR2 (50);

      call_status                     BOOLEAN;
      l_cp_phase                      VARCHAR2 (100);
      l_cp_status                     VARCHAR2 (100);
      l_dev_phase                     VARCHAR2 (100);
      l_dev_status                    VARCHAR2 (100);
      l_req_message                   VARCHAR2 (240);

      l_cogs_acc_id                   NUMBER;
      l_sales_acc_id                  NUMBER;
      l_expn_acc_id                   NUMBER;  -- Added in Rev 2.3
      l_item_type                     VARCHAR2 (50);

      -- Added by Shankar Hariharan 01-Apr-2013
      --TMS 20130401-00537
      l_assign_return_status          VARCHAR2 (1);
      l_assign_msg_count              NUMBER := 0;
      l_assign_msg_data               VARCHAR2 (1000);
      l_assignment_set_rec            mrp_src_assignment_pub.assignment_set_rec_type;
      l_assignment_set_val_rec        mrp_src_assignment_pub.assignment_set_val_rec_type;
      l_assignment_tbl                mrp_src_assignment_pub.assignment_tbl_type;
      l_assignment_val_tbl            mrp_src_assignment_pub.assignment_val_tbl_type;
      o_assignment_set_rec            mrp_src_assignment_pub.assignment_set_rec_type;
      o_assignment_set_val_rec        mrp_src_assignment_pub.assignment_set_val_rec_type;
      o_assignment_tbl                mrp_src_assignment_pub.assignment_tbl_type;
      o_assignment_val_tbl            mrp_src_assignment_pub.assignment_val_tbl_type;
      lv_inventory_item_status_code   mtl_system_items_b.inventory_item_status_code%TYPE; -- Added by Vamshi in Rev. 2.2 @ TMS#20150918-00018
   BEGIN
      -- Clear the Item GRP API message table before processing an item
      l_Error_tbl.DELETE;

      l_Item_rec_in.INVENTORY_ITEM_ID := l_INV_ITEM_ID;
      l_Item_rec_in.ORGANIZATION_ID := l_ORG_ID;
      l_Item_rec_in.PRIMARY_UOM_CODE := l_UOM_CODE;
      l_item_rec_in.list_price_per_unit := l_item_cost;
      --Added by Shankar 04-Dec-2013 20131121-00084
      l_item_rec_in.attribute20 := NULL;

      -- Added below code by Vamshi in Rev. 2.2 @ TMS#20150918-00018 -- Start
      BEGIN
         SELECT inventory_item_status_code
           INTO lv_inventory_item_status_code
           FROM apps.mtl_system_items_b
          WHERE     inventory_item_id = l_inv_item_id
                AND organization_id = l_from_org_id;

         l_Item_rec_in.inventory_item_status_code :=
            lv_inventory_item_status_code;
      EXCEPTION
         WHEN OTHERS
         THEN
            NULL;
      END;

      -- Added above code by Vamshi in Rev. 2.2 @ TMS#20150918-00018 -- End

      SELECT item_type
        INTO l_item_type
        FROM mtl_system_items
       WHERE organization_id = 222 AND inventory_item_id = l_inv_item_id;

      IF l_item_type = 'INTANGIBLE'
      THEN
         l_template_name := 'Intangible';
      ELSIF l_item_type = 'RENTAL'
      THEN
         l_template_name := 'Rental - Std';
      ELSIF l_item_type = 'REPAIR'
      THEN
         l_template_name := 'Repair';
      ELSIF l_item_type = 'RE_RENT'
      THEN
         l_template_name := 'Re-Rental';
      ELSE
         l_template_name := 'Non-Stock';
      END IF;

      --End Shankar Hariharan 04-Dec-2013 20131121-00084

      -- ADded by Shankar to derive COGS and Sales account
      l_item_rec_in.cost_of_sales_account :=
         get_gl_account (l_inv_item_id, l_org_id, 'C');
      l_item_rec_in.sales_account :=
         get_gl_account (l_inv_item_id, l_org_id, 'S');
      -- Added below code in Rev 2.3   
      l_item_rec_in.expense_account :=
         get_gl_account (l_inv_item_id, l_org_id, 'E');
         


      --Start : Added revision record processing

      INV_ITEM_GRP.Create_Item (p_commit          => 'T',
                                p_Item_rec        => l_Item_rec_in,
                                p_Revision_rec    => l_revision_rec,
                                p_Template_Id     => NULL,
                                p_Template_Name   => l_template_name --'Non-Stock'
                                                                    ,
                                x_Item_rec        => l_Item_rec_out,
                                x_return_status   => l_return_status,
                                x_Error_tbl       => l_Error_tbl);

      IF (l_return_status = FND_API.g_RET_STS_SUCCESS)
      THEN
         SELECT MASTER_ORGANIZATION_ID
           INTO l_master_org
           FROM mtl_parameters
          WHERE ORGANIZATION_ID = l_Item_rec_in.ORGANIZATION_ID;

         UPDATE MTL_SYSTEM_ITEMS_TL ASSIGNEE
            SET (ASSIGNEE.DESCRIPTION,
                 ASSIGNEE.LONG_DESCRIPTION,
                 ASSIGNEE.SOURCE_LANG) =
                   (SELECT DESCRIPTION, LONG_DESCRIPTION, SOURCE_LANG
                      FROM MTL_SYSTEM_ITEMS_TL MASTER
                     WHERE     INVENTORY_ITEM_ID =
                                  l_Item_rec_in.INVENTORY_ITEM_ID
                           AND ORGANIZATION_ID = L_MASTER_ORG
                           AND MASTER."LANGUAGE" = ASSIGNEE."LANGUAGE")
          WHERE     INVENTORY_ITEM_ID = l_Item_rec_in.INVENTORY_ITEM_ID
                AND ORGANIZATION_ID = l_Item_rec_in.ORGANIZATION_ID;

         -- Added by Shankar 22-May-2012
         -- Call assign category to assign sales velocity category
         ASSIGN_CATEGORY (p_category_id         => NULL,
                          p_organization_id     => l_org_id,
                          p_Inventory_Item_ID   => l_inv_item_id,
                          x_Return_Status       => l_return_status,
                          x_Msg_Data            => l_msg_data,
                          x_Msg_Count           => l_msg_count);


         -- Added by Shankar 20-Jun-2013 Task 20130620-00850
         -- Added Default Subinventory
         SELECT segment1, item_type
           INTO l_item_number, l_item_type
           FROM mtl_system_items
          WHERE     inventory_item_id = l_inv_item_id
                AND organization_id = l_org_id;


         INSERT INTO mtl_item_sub_defaults (inventory_item_id,
                                            organization_id,
                                            subinventory_code,
                                            default_type,
                                            last_update_date,
                                            last_updated_by,
                                            creation_date,
                                            created_by,
                                            last_update_login)
                 VALUES (
                           l_inv_item_id,
                           l_org_id,
                           DECODE (l_item_type,
                                   'RENTAL', 'Rental',
                                   'RE_RENT', 'Rental',
                                   'General'),
                           --decode(substr(l_item_number,1,1),'R','Rental','General'),
                           2,                                      --receiving
                           SYSDATE,
                           fnd_global.user_id,
                           SYSDATE,
                           fnd_global.user_id,
                           -1);

         --Added by Shankar 20-Jun-2013 Task 20130620-00850 END


         IF l_item_cost IS NOT NULL
         THEN
            CREATE_ITEM_COST (p_Inventory_Item_ID   => l_inv_item_id,
                              p_Organization_ID     => l_org_id,
                              p_Item_Cost           => l_item_cost,
                              p_uom_code            => l_uom_code,
                              x_Return_Status       => l_return_status,
                              x_Msg_Data            => l_msg_data,
                              x_Msg_Count           => l_msg_count);
         /* commented out based on new design on 24-May-2012 by Shankar Hariharan
   l_Request_ID := fnd_request.submit_request (application    => 'INV',
                                      program        => 'INCTCM');
       IF l_request_id <> 0 then
         commit;
           call_status := fnd_concurrent.wait_for_request(l_request_id, 5, 1800,
                          l_cp_phase, l_cp_status, l_dev_phase, l_dev_status, l_req_message);
           IF call_status = TRUE THEN
               IF l_dev_phase!= 'Completed' OR
                l_dev_status IN ('Cancelled','Error','Terminated') THEN
               l_msg_data := l_msg_data||'::'||'Error running cost import';
               END IF;
           ELSE
               l_msg_data := l_msg_data||'::'||'Error running cost import';
           END IF;
       ELSE
         l_msg_data := l_msg_data||'::'||'Error submitting cost import';
       END IF; */
         END IF;                                      -- item cost is not null

         -- Added by Shankar Hariharan 01-Apr-2013
         --TMS 20130401-00537
         BEGIN
            SELECT assignment_set_id,
                   assignment_type,
                   sourcing_rule_id,
                   sourcing_rule_type
              INTO l_assignment_tbl (1).assignment_set_id,
                   l_assignment_tbl (1).assignment_type,
                   l_assignment_tbl (1).sourcing_rule_id,
                   l_assignment_tbl (1).sourcing_rule_type
              FROM mrp_sr_assignments
             WHERE     inventory_item_id = l_inv_item_id
                   AND organization_id = l_from_org_id
                   AND ROWNUM = 1;

            l_assignment_tbl (1).operation := 'CREATE';
            l_assignment_tbl (1).organization_id := l_org_id;
            l_assignment_tbl (1).inventory_item_id := l_inv_item_id;
            --------who columns
            l_assignment_tbl (1).creation_date := SYSDATE;
            l_assignment_tbl (1).last_update_date := SYSDATE;
            l_assignment_tbl (1).last_updated_by := fnd_global.user_id;
            l_assignment_tbl (1).created_by := fnd_global.user_id;

            mrp_src_assignment_pub.process_assignment (
               p_api_version_number       => 1.0,
               p_init_msg_list            => fnd_api.g_false,
               p_return_values            => fnd_api.g_false,
               p_commit                   => fnd_api.g_true,
               x_return_status            => l_assign_return_status,
               x_msg_count                => l_assign_msg_count,
               x_msg_data                 => l_assign_msg_data,
               p_assignment_set_rec       => l_assignment_set_rec,
               p_assignment_set_val_rec   => l_assignment_set_val_rec,
               p_assignment_tbl           => l_assignment_tbl,
               p_assignment_val_tbl       => l_assignment_val_tbl,
               x_assignment_set_rec       => o_assignment_set_rec,
               x_assignment_set_val_rec   => o_assignment_set_val_rec,
               x_assignment_tbl           => o_assignment_tbl,
               x_assignment_val_tbl       => o_assignment_val_tbl);
         EXCEPTION
            WHEN OTHERS
            THEN
               l_assign_msg_data := 'Sourcing Rule not available.';
         END;
      END IF;

      l_msg_count := l_msg_count + NVL (l_Error_tbl.COUNT, 0);

      FOR errno IN 1 .. NVL (l_Error_tbl.LAST, 0)
      LOOP
         IF (l_Error_tbl (errno).MESSAGE_TEXT IS NOT NULL)
         THEN
            --EGO_Item_Msg.Add_Error_Text (1, l_Error_tbl(errno).MESSAGE_TEXT);
            --dbms_output.put_line(l_Error_tbl(errno).MESSAGE_TEXT);
            l_msg_data :=
               l_Error_tbl (errno).MESSAGE_TEXT || '::' || l_msg_data;
         ELSE
            --EGO_Item_Msg.Add_Error_Message (1, 'INV', l_Error_tbl(errno).MESSAGE_NAME);
            --dbms_output.put_line(l_Error_tbl(errno).MESSAGE_NAME);
            l_msg_data :=
               l_Error_tbl (errno).MESSAGE_NAME || '::' || l_msg_data;
         END IF;
      END LOOP;                                                 -- l_Error_tbl

      IF l_return_status = FND_API.g_RET_STS_SUCCESS
      THEN
         SELECT segment1
           INTO l_item_number
           FROM mtl_system_items
          WHERE     inventory_item_id = l_inv_item_id
                AND organization_id = l_org_id;

         XXWC_INV_NEW_PROD_REQ_PKG.SET_ITEM_NUMBER (
            P_INVENTORY_ITEM_ID   => l_inv_item_id,
            P_ITEM_NUMBER         => l_item_number,
            P_TRANSACTION_TYPE    => G_CLONE_PARTS_SO,
            P_ORGANIZATION_ID     => l_org_id,
            P_ORGANIZATION_CODE   => NULL,
            P_TRANSACTION_ID      => l_trx_id);
         COMMIT;
      END IF;

      o_ret_Status := l_return_status;
      o_ret_msg := l_assign_msg_data || l_msg_data;
   EXCEPTION
      WHEN OTHERS
      THEN
         --x_return_status  :=  G_RET_STS_UNEXP_ERROR;
         l_msg_data := SQLERRM;
         o_ret_Status := 'E';
         o_ret_msg := l_msg_data;
   END process_item_org_assignments;


   /*
   -- Procedure to update unit cost on sales orders for cloned parts
   -- created by Shankar Hariharan 25-May-2012
   --Modified by Ram Talluri on 9/23/2013 for TMS 20130913-00647 and 20130912-00860

   */

   PROCEDURE update_unit_cost_on_so (errbuf    OUT VARCHAR2,
                                     retcode   OUT VARCHAR2)
   IS
      l_item_cost   NUMBER;

      CURSOR c1
      IS
         /*--Commented by  Ram Talluri on 9/23/2013 for TMS 20130913-00647 and 20130912-00860
          SELECT a.order_number
               , a.ordered_date
               , b.line_number
               , c.segment1
               , d.organization_code
               , d.organization_name
               , c.list_price_per_unit
               , c.item_type
               , b.line_id
               , b.ship_from_org_id
               , b.inventory_item_id
            FROM oe_order_headers_all a
               , oe_order_lines_all b
               , mtl_system_items_b c
               , org_organization_definitions d
           WHERE     a.header_id = b.header_id
                 AND b.inventory_item_id = c.inventory_item_id
                 AND b.ship_from_org_id = c.organization_id
                 AND c.organization_id = d.organization_id
                 AND unit_cost IS NULL
                 AND c.item_type <> 'INTANGIBLE'
                 AND c.segment1 NOT LIKE 'R%';*/
         --added by by  Ram Talluri on 9/23/2013 for TMS 20130913-00647 and 20130912-00860
         SELECT a.order_number,
                a.ordered_date,
                b.line_number,
                c.segment1,
                d.organization_code,
                c.list_price_per_unit,
                c.item_type,
                b.line_id,
                b.ship_from_org_id,
                b.inventory_item_id
           FROM apps.oe_order_headers a,                        -- version 2.1
                apps.oe_order_lines b,                          -- version 2.1
                mtl_system_items_b c,
                mtl_parameters d
          WHERE     a.header_id = b.header_id
                AND b.inventory_item_id = c.inventory_item_id
                AND b.ship_from_org_id = c.organization_id
                AND c.organization_id = d.organization_id
                AND unit_cost IS NULL
                AND (    c.item_type NOT IN ('INTANGIBLE',
                                             'FRT',
                                             'RENTAL',
                                             'REPAIR',
                                             'RE_RENT',
                                             'SERVICE ITEM')
                     AND c.INVENTORY_ITEM_STATUS_CODE NOT IN ('Repair',
                                                              'Intangible'))
                AND c.segment1 NOT LIKE 'R%'
                AND b.creation_date >= SYSDATE - 7;
   BEGIN
      fnd_file.put_line (
         fnd_file.output,
         '               ORDER LINES UPDATED WITH ITEM COST                      ');
      fnd_file.put_line (fnd_file.output,
                         '               ----------------------------------');
      fnd_file.put_line (fnd_file.output, ' ');
      fnd_file.put_line (
         fnd_file.output,
         'Order#          Line#      Item#                                   Cost');
      fnd_file.put_line (
         fnd_file.output,
         '-----------------------------------------------------------------------');

      FOR c1_rec IN c1
      LOOP
         BEGIN
            SELECT NVL (item_cost, 0)
              INTO l_item_cost
              FROM cst_item_costs
             WHERE     inventory_item_id = c1_rec.inventory_item_id
                   AND organization_id = c1_rec.ship_from_org_id
                   AND cost_type_id = 2;

            IF l_item_cost = 0
            THEN
               l_item_cost := c1_rec.list_price_per_unit;
            END IF;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_item_cost := c1_rec.list_price_per_unit;
         END;

         IF l_item_cost IS NOT NULL
         THEN
            UPDATE oe_order_lines_all
               SET unit_cost = l_item_cost
             WHERE line_id = c1_rec.line_id;

            fnd_file.put_line (
               fnd_file.output,
                  c1_rec.order_number
               || '           '
               || c1_rec.line_number
               || '       '
               || RPAD (c1_rec.segment1, 40, ' ')
               || l_item_cost);
         END IF;
      END LOOP;
   END update_unit_cost_on_so;


   --Added by Manjula on 29-Apr-14 for TMS 20140407-00238  Begin

   PROCEDURE POPULATE_PRODUCT_UDA (p_inv_item_id   IN     NUMBER,
                                   p_error_msg        OUT VARCHAR2)
   IS
      TYPE trec_uda_list IS RECORD
      (
         attr_group_name     VARCHAR2 (200),
         attr_group_id       NUMBER,
         attr_name           VARCHAR2 (200),
         attr_display_name   VARCHAR2 (200),
         attr_value_var      VARCHAR2 (400),
         attr_value_num      NUMBER,
         attr_value_dt       DATE,
         attr_data_type      VARCHAR2 (5),
         exception_flag      VARCHAR2 (1)
      );


      -- List of the attributes to be updated is maintained in the table--

      TYPE trec_uda_list_tbl IS TABLE OF trec_uda_list
         INDEX BY BINARY_INTEGER;

      t_uda_list_tbl            trec_uda_list_tbl;

      --API specific declaration --

      l_attributes_row_table    ego_user_attr_row_table
                                   := ego_user_attr_row_table ();
      l_attributes_data_table   ego_user_attr_data_table
                                   := ego_user_attr_data_table ();

      -- Variables declaration --

      l_tbl_rec_cnt             NUMBER := 0;
      l_grp_identifier          NUMBER := 0;
      l_attr_identifier         NUMBER := 0;
      l_internal_attr_grp_id    NUMBER := 0;
      l_attr_grp_id             NUMBER := 0;
      l_data_level              VARCHAR2 (40);
      l_org_id                  NUMBER := 0;
      l_resp_id                 NUMBER := 0;
      l_attr_grp_app_id         NUMBER := 0;
      l_failed_row_id_list      VARCHAR2 (2000) := NULL;
      l_return_status           VARCHAR2 (1) := NULL;
      l_err_msg                 VARCHAR2 (4000) := NULL;
      l_msg_count               NUMBER := 0;
      l_msg_data                VARCHAR2 (4000) := NULL;
      l_errorcode               NUMBER := 0;
      l_user_id                 NUMBER := fnd_global.user_id;
      l_inv_item_id             NUMBER := p_inv_item_id;
      l_part_number             VARCHAR2 (100);
      l_coo_desc                VARCHAR2 (150);
      l_coo                     VARCHAR2 (5);
      l_vendor_id               NUMBER;
      l_vendor_number           VARCHAR2 (150);
      l_msg_index_out           NUMBER;
      l_sec                     VARCHAR2 (150);

      -- constants declaration --

      c_attr_coo                VARCHAR2 (40) := 'XXWC_COUNTRY_ORIGIN_ATTR';
      c_attr_vendor             VARCHAR2 (40) := 'XXWC_VENDOR_NUMBER_ATTR';
      c_ego_itemmgmt_group      VARCHAR2 (60) := 'EGO_ITEMMGMT_GROUP';
      c_internal_attr_ag        VARCHAR2 (60) := 'XXWC_INTERNAL_ATTR_AG';
      c_procedure_name          VARCHAR2 (150)
         := 'XXWC_INV_NEW_PROD_REQ_PKG.POPULATE_PRODUCT_UDA';
   BEGIN
      BEGIN
         l_org_id := fnd_profile.VALUE ('XXWC_ITEM_MASTER_ORG');

         DBMS_OUTPUT.put_line ('Org_id ' || l_org_id);


         BEGIN
            SELECT attr_group_id
              INTO l_internal_attr_grp_id
              FROM ego_attr_groups_v
             WHERE     attr_group_name = c_internal_attr_ag
                   AND attr_group_type = c_ego_itemmgmt_group;

            DBMS_OUTPUT.put_line (
               'l_internal_attr_grp_id ' || l_internal_attr_grp_id);
         EXCEPTION
            WHEN OTHERS
            THEN
               l_internal_attr_grp_id := NULL;
               p_error_msg := 'Error 1 : l_internal_attr_grp_id ' || SQLERRM;
         END;


         BEGIN
            SELECT responsibility_id, application_id
              INTO l_resp_id, l_attr_grp_app_id
              FROM fnd_responsibility
             WHERE responsibility_key = 'EGO_PIM_DATA_LIBRARIAN';

            DBMS_OUTPUT.put_line ('l_resp_id ' || l_resp_id);
            DBMS_OUTPUT.put_line ('l_attr_grp_app_id ' || l_attr_grp_app_id);
         EXCEPTION
            WHEN OTHERS
            THEN
               l_resp_id := NULL;
               l_attr_grp_app_id := NULL;
               p_error_msg :=
                  p_error_msg || ' Error 2 : l_resp_id ' || SQLERRM;
         END;

         ---------------------------------------------------------------------------
         -- Get the Country of Origin and Master vendor ID forthe new product --
         ---------------------------------------------------------------------------

         BEGIN
            SELECT segment1
              INTO l_part_number
              FROM apps.mtl_system_items_b
             WHERE     inventory_item_id = l_inv_item_id
                   AND organization_id = l_org_id;


            BEGIN
               SELECT NVL (country_of_origin, 'ZZ - UNDETERMINED'), vendor_id -- Modified as per version 2.0 to default country of origin --
                 INTO l_coo_desc, l_vendor_id
                 FROM xxwc.xxwc_inv_product_request
                WHERE item_number = l_part_number AND ROWNUM = 1;

               BEGIN
                  SELECT lookup_code
                    INTO l_coo
                    FROM apps.fnd_lookup_values
                   WHERE     lookup_type = 'XXWC_TERRITORIES'
                         AND meaning = l_coo_desc
                         AND NVL (end_date_active, SYSDATE) >= SYSDATE
                         AND enabled_flag = 'Y';
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_coo := NULL;

                     p_error_msg :=
                           p_error_msg
                        || ' Error 3 : Error in Getting Country of Origin '
                        || SQLERRM;
               END;


               BEGIN
                  SELECT segment1
                    INTO l_vendor_number
                    FROM apps.ap_suppliers
                   WHERE vendor_id = l_vendor_id;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_vendor_number := NULL;
                     p_error_msg :=
                           p_error_msg
                        || ' Error 6 : Error in getting Vendor Number '
                        || SQLERRM;
               END;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_coo_desc := NULL;
                  l_vendor_id := NULL;
                  p_error_msg :=
                        p_error_msg
                     || ' Error 4 : Error in getting country of origin and Vendor Number from New Product Data '
                     || SQLERRM;
            END;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_part_number := NULL;
               p_error_msg :=
                     p_error_msg
                  || ' Error 5 : Error in getting part number '
                  || SQLERRM;
         END;



         ---------------------------------------------------------------------------
         -- Initialize the table for UDA List with Country or origin and Vendor Number--
         ---------------------------------------------------------------------------


         IF l_internal_attr_grp_id IS NOT NULL
         THEN
            IF l_coo IS NOT NULL
            THEN
               l_tbl_rec_cnt := l_tbl_rec_cnt + 1;
               t_uda_list_tbl (l_tbl_rec_cnt).attr_group_id :=
                  l_internal_attr_grp_id;
               t_uda_list_tbl (l_tbl_rec_cnt).attr_name := c_attr_coo;
               t_uda_list_tbl (l_tbl_rec_cnt).attr_value_var := l_coo;
               t_uda_list_tbl (l_tbl_rec_cnt).attr_value_num := NULL;
               t_uda_list_tbl (l_tbl_rec_cnt).attr_value_dt := NULL;
               t_uda_list_tbl (l_tbl_rec_cnt).exception_flag := 'N';
            END IF;

            IF l_vendor_id IS NOT NULL
            THEN
               l_tbl_rec_cnt := l_tbl_rec_cnt + 1;
               t_uda_list_tbl (l_tbl_rec_cnt).attr_group_id :=
                  l_internal_attr_grp_id;
               t_uda_list_tbl (l_tbl_rec_cnt).attr_name := c_attr_vendor;
               t_uda_list_tbl (l_tbl_rec_cnt).attr_value_var :=
                  l_vendor_number;
               t_uda_list_tbl (l_tbl_rec_cnt).attr_value_num := NULL;
               t_uda_list_tbl (l_tbl_rec_cnt).attr_value_dt := NULL;
               t_uda_list_tbl (l_tbl_rec_cnt).exception_flag := 'N';
            END IF;

            DBMS_OUTPUT.put_line (
                  'ego_user_attrs_data_pvt.g_sync_mode '
               || ego_user_attrs_data_pvt.g_sync_mode);

            ---------------------------------------------------------------------------
            -- Build the table for UDA List with Country or origin and Vendor Number--
            ---------------------------------------------------------------------------

            FOR ln_rec_num IN t_uda_list_tbl.FIRST .. t_uda_list_tbl.LAST
            LOOP
               l_attr_grp_id := t_uda_list_tbl (ln_rec_num).attr_group_id;
               l_grp_identifier := l_grp_identifier + 1;

               l_data_level := 'ITEM_LEVEL';
               l_attributes_row_table.EXTEND ();
               l_attributes_row_table (l_grp_identifier) :=
                  ego_user_attrs_data_pub.build_attr_group_row_object (
                     p_row_identifier      => l_grp_identifier,
                     p_attr_group_id       => l_attr_grp_id,
                     p_attr_group_app_id   => l_attr_grp_app_id,
                     p_attr_group_type     => c_ego_itemmgmt_group,
                     p_attr_group_name     => NULL,
                     p_data_level          => l_data_level,
                     p_data_level_1        => NULL,
                     p_data_level_2        => NULL,
                     p_data_level_3        => NULL,
                     p_data_level_4        => NULL,
                     p_data_level_5        => NULL,
                     p_transaction_type    => ego_user_attrs_data_pvt.g_sync_mode);
               ---------------------------------------------------------------------------
               -- Build the data for UDA List with Country or origin and Vendor Number--
               ---------------------------------------------------------------------------

               l_attr_identifier := l_attr_identifier + 1;
               l_attributes_data_table.EXTEND ();
               l_attributes_data_table (l_attr_identifier) :=
                  ego_user_attr_data_obj (
                     l_grp_identifier,
                     t_uda_list_tbl (ln_rec_num).attr_name,
                     t_uda_list_tbl (ln_rec_num).attr_value_var,
                     t_uda_list_tbl (ln_rec_num).attr_value_num,
                     t_uda_list_tbl (ln_rec_num).attr_value_dt,
                     NULL,
                     NULL,
                     NULL);

               ---------------------------------------------------------------------------
               -- Invoke the API for populating UDA with Country or origin and Vendor Number--
               ---------------------------------------------------------------------------

               ego_item_pub.process_user_attrs_for_item (
                  p_api_version               => 1.0,
                  p_inventory_item_id         => l_inv_item_id,
                  p_organization_id           => l_org_id,
                  p_attributes_row_table      => l_attributes_row_table,
                  p_attributes_data_table     => l_attributes_data_table,
                  p_entity_id                 => NULL,
                  p_entity_index              => NULL,
                  p_entity_code               => NULL,
                  p_debug_level               => 0,
                  p_init_error_handler        => fnd_api.g_true,
                  p_write_to_concurrent_log   => fnd_api.g_false,
                  p_init_fnd_msg_list         => fnd_api.g_false,
                  p_log_errors                => fnd_api.g_true,
                  p_add_errors_to_fnd_stack   => fnd_api.g_false,
                  p_commit                    => fnd_api.g_false,
                  x_failed_row_id_list        => l_failed_row_id_list,
                  x_return_status             => l_return_status,
                  x_errorcode                 => l_errorcode,
                  x_msg_count                 => l_msg_count,
                  x_msg_data                  => l_msg_data);

               DBMS_OUTPUT.put_line ('l_return_status ' || l_return_status);
               DBMS_OUTPUT.put_line ('l_errorcode ' || l_errorcode);
               DBMS_OUTPUT.put_line ('l_msg_count ' || l_msg_count);
               DBMS_OUTPUT.put_line ('l_msg_data ' || l_msg_data);

               IF l_return_status = 'E'
               THEN
                  p_error_msg :=
                     p_error_msg || ' Error 7 : Can not populate PDH UDA ';

                  FOR i IN 1 .. l_msg_count
                  LOOP
                     fnd_Msg_Pub.get (p_msg_index       => i,
                                      p_encoded         => Fnd_Api.G_FALSE,
                                      p_data            => l_msg_data,
                                      p_msg_index_out   => l_msg_index_out);
                     p_error_msg :=
                        p_error_msg || ' : ' || SUBSTR (l_msg_data, 1, 100);
                  END LOOP;
               END IF;
            END LOOP;
         END IF;
      END;
   EXCEPTION
      WHEN OTHERS
      THEN
         p_error_msg :=
               p_error_msg
            || ' Procedure Exception : '
            || SUBSTR (SQLERRM, 1, 100);


         l_sec := DBMS_UTILITY.FORMAT_ERROR_BACKTRACE;

         IF p_error_msg IS NOT NULL
         THEN
            XXCUS_error_pkg.XXCUS_error_main_api (
               p_called_from         => c_procedure_name,
               p_calling             => l_sec,
               p_request_id          => NULL,
               p_ora_error_msg       => p_error_msg,
               p_error_desc          => 'Error updating PDH UDA for Country of Origin and Master Vendor Number',
               p_distribution_list   => g_default_email,
               p_module              => 'XXWC');
         END IF;
   END POPULATE_PRODUCT_UDA;
--Added by Manjula on 29-Apr-14 for TMS 20140407-00238  End

END XXWC_INV_NEW_PROD_REQ_PKG;
/