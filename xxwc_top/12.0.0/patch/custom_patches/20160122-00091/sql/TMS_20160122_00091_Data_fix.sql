/*************************************************************************
  $Header TMS_20160122_00091_Data_fix.sql $
  Module Name: TMS#20160122_00091_Data_fix  Data Fix script 

  PURPOSE: Data Fix script for TMS#20160122_00091

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        21-OCT-2016  P.Vamshidhar          TMS#20160122_00091
**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000

BEGIN
   DBMS_OUTPUT.put_line ('TMS:20160122_00091 - Before Update');

   UPDATE APPS.MTL_CATEGORIES_B MCB
      SET MCB.ATTRIBUTE8 = MCB.ATTRIBUTE1
    WHERE     MCB.ATTRIBUTE1 IS NOT NULL
          AND MCB.ATTRIBUTE8 IS NULL
          AND NVL (MCB.DISABLE_DATE, SYSDATE + 1) >= SYSDATE
          AND MCB.ENABLED_FLAG = 'Y';

   DBMS_OUTPUT.put_line ('TMS:20160122_00091: ' || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20160122_00091 - End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20160122_00091 , Errors : ' || SQLERRM);
END;
/