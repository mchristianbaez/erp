CREATE OR REPLACE package body APPS.xxwc_post_cat_assign_sub_pkg
as
   /***************************************************************************************************************
    File Name:XXWC_POST_CAT_ASSIGN_SUB_PKG
    PROGRAM TYPE: PL/SQL Package spec and body
    PURPOSE:
    HISTORY
    -- Description   : Called from the business event
    --                 oracle.apps.ego.item.postCatalogAssignmentChange
    --
    -- Dependencies Tables        :
    -- Dependencies Views         : None
    -- Dependencies Sequences     :
    -- Dependencies Procedures    :
    -- Dependencies Functions     : None
    -- Dependencies Packages      : None
    -- Dependencies Types         : None
    -- Dependencies Database Links: None
    ===============================================================================================================
           Last Update Date : 01/08/2014
    ===============================================================================================================
    ===============================================================================================================
    VERSION DATE          AUTHOR(S)       DESCRIPTION
    ------- -----------   --------------- -------------------------------------------------------------------------
    1.0     08-Jan-2014   Praveen Pawar    Initial creation of the package
    2.0     18-Mar-2014   Praveen Pawar    Modified to handle oracle.apps.ego.batch.postbatchprocess
                                           Business Event
                                           TMS# 20140319-00190
    2.1     25-Jul-2014   Praveen Pawar    Modifying PROCESS_EVENT function to handle processing of
                                           Inventory Category catalog and added private function process_cogs_sales
                                           and error handling TMS #?20140507-00218
    2.2     06-Oct-2015   Pattabhi Avula   TMS#20150929-00148 -- Error Alerts for categories missing Sales and COGS
                                           exception skipped with count values count
    2.3     02-Dec-2015   Kishorebabu V    TMS# 20151105-00158   Added additional error handling steps
    2.4     17-Oct-2016   Pattabhi Avula   TMS#20160926-00258 - Improve Error Handling in XXWC_POST_CAT_ASSIGN_SUB_PKG
                                           - Commented the Pragma Autonomous transaction
            28-Oct-2016   P.Vamshidhar     TMS#20160122-00091 - GL Code Enhancements to leverage GL API
   **************************************************************************/

   ---------------------------------------------------------------------------------------------------------------
   -- Added private function process_cogs_sales w.r.t. ver 2.1 --
   ---------------------------------------------------------------------------------------------------------------
   /**************************************************************************************************************
   *
   * FUNCTION
   *  process_cogs_sales
   *
   * DESCRIPTION
   *  This function will sync the cogs and sales account based on new inventory catalog category value
   *
   * PARAMETERS
   * ==========
   * NAME                          TYPE     DESCRIPTION
   * -----------------             -------- ---------------------------------------------------------------------
   * p_inventory_item_id   IN       NUMBER    inventory_item_id
   * p_organization_id     IN       NUMBER    organization_id
   * p_new_cogs_segment    IN       VARCHAR2  COGS
   * p_new_sales_segment   IN       VARCHAR2  New Sales Segment
   * p_item_number         OUT      VARCHAR2  Item Number
   * p_org_code            OUT      VARCHAR2  Org Code
   * p_old_cogs_gl_code    OUT      VARCHAR2  Old COGS GL code
   * p_new_cogs_gl_code    OUT      VARCHAR2  New COGS GL Code
   * p_old_sales_gl_code   OUT      VARCHAR2  Old Sales GL COde
   * p_new_sales_gl_code   OUT      VARCHAR2  New Sales GL Code
   RETURN
   BOOLEAN
    =============================================================================================================
       VERSION DATE          AUTHOR(S)       DESCRIPTION
       ------- -----------   --------------- --------------------------------------------------------------------
       2.1     25-Jul-2014   Praveen Pawar    Modifying PROCESS_EVENT function to handle processing of
                                              Inventory Category catalog and error handling TMS #?20140507-00218
       2.3     02-Dec-2015   Kishorebabu V    TMS# 20151105-00158   Added additional error handling steps
       2.4     17-Oct-2016   Pattabhi Avula   TMS#20160926-00258 - Improve Error Handling in XXWC_POST_CAT_ASSIGN_SUB_PKG
                                              - Commented the Pragma Autonomous transaction
               28-Oct-2016   P.Vamshidhar     TMS#20160122-00091 - GL Code Enhancements to leverage GL API

   **************************************************************************************************************/
   function process_cogs_sales (p_inventory_item_id   in     number,
                                p_organization_id     in     number,
                                p_new_cogs_segment    in     varchar2,
                                p_new_sales_segment   in     varchar2,
                                p_new_expn_segment    in     varchar2,
                                p_item_number            out varchar2,
                                p_org_code               out varchar2,
                                p_old_cogs_gl_code       out varchar2,
                                p_new_cogs_gl_code       out varchar2,
                                p_old_sales_gl_code      out varchar2,
                                p_new_sales_gl_code      out varchar2,
                                p_old_expn_gl_code       out varchar2,
                                p_new_expn_gl_code       out varchar2)
      return boolean
   is
      -- PRAGMA AUTONOMOUS_TRANSACTION; -- Commented in 2.4V
      cv_dist_list            varchar2 (100) := 'HDSOracleDevelopers@hdsupply.com';
      l_missing_gl_dl         varchar2 (400)
         := apps.fnd_profile.value ('XXWC_EGO_MISSING_GL_DL'); --Added functional DL as per Manny's comment on 19-Aug-14
      cv_inventory_category   mtl_category_sets_tl.category_set_name%type
                                 := 'Inventory Category';
      l_num_msg_cnt           number := null;
      lv_new_cogs_ccid        number := null;
      lv_new_sales_ccid       number := null;
      lv_new_expn_ccid        number := null;
      lv_new_cogs             varchar2 (181);
      lv_new_sales            varchar2 (181);
      lv_new_expn             varchar2 (181);
      lv_error_message        varchar2 (4000);
      l_chr_return_status     varchar2 (100) := null;
      x_message_list          error_handler.error_tbl_type;
      x_tbl_item_table        ego_item_pub.item_tbl_type;
      l_tbl_item_table        ego_item_pub.item_tbl_type;
      l_calling               varchar2 (4000) := null;
      l_error_desc            varchar2 (4000) := null; --Ver 2.3 Added by Kishorebabu V as per TMS# 20151105-00158
      l_parameters            varchar2 (1000) := null; --Ver 2.3 Added by Kishorebabu V as per TMS# 20151105-00158
      ln_count                number := 0;

      cursor cur_item_account_details (
         p_inventory_item_id   in number,
         p_new_cogs_segment    in varchar2,
         p_new_sales_segment   in varchar2,
         p_new_expn_segment    in varchar2)
      is
         select item_number,
                organization_id,
                organization_code,
                cogs_account_id,
                existing_cogs,
                cogs_segment,
                sales_account_id,
                existing_sales,
                sales_segment,
                expn_account_id,
                existing_expn,
                expn_segment,
                created_by
           from (select msi.segment1 item_number,
                        msi.organization_id,
                        mtp.organization_code,
                        msi.cost_of_sales_account cogs_account_id,
                        gck1.concatenated_segments existing_cogs,
                        gcc1.segment4 cogs_segment,
                        msi.sales_account sales_account_id,
                        gck2.concatenated_segments existing_sales,
                        gcc2.segment4 sales_segment,
                        msi.expense_account expn_account_id,
                        gck3.concatenated_segments existing_expn,
                        gck3.segment4 expn_segment,
                        msi.created_by
                   from mtl_system_items_b msi,
                        gl_code_combinations gcc1,
                        gl_code_combinations gcc2,
                        gl_code_combinations gcc3,
                        gl_code_combinations_kfv gck1,
                        gl_code_combinations_kfv gck2,
                        gl_code_combinations_kfv gck3,
                        mtl_parameters mtp
                  where     msi.inventory_item_id = p_inventory_item_id
                        and msi.organization_id = p_organization_id
                        and msi.organization_id = mtp.organization_id
                        and gck1.code_combination_id =
                               gcc1.code_combination_id
                        and gck2.code_combination_id =
                               gcc2.code_combination_id
                        and gck3.code_combination_id =
                               gcc3.code_combination_id
                        and (   gcc1.segment4 <>
                                   nvl (p_new_cogs_segment, gcc1.segment4)
                             or gcc2.segment4 <>
                                   nvl (p_new_sales_segment, gcc2.segment4)
                             or gcc3.segment4 <>
                                   nvl (p_new_expn_segment, gcc3.segment4))
                        and gcc1.code_combination_id =
                               msi.cost_of_sales_account
                        and gcc2.code_combination_id = msi.sales_account
                        and gcc3.code_combination_id = msi.expense_account);
   begin
      for rec_item_account_details
         in cur_item_account_details (p_inventory_item_id,
                                      p_new_cogs_segment,
                                      p_new_sales_segment,
                                      p_new_expn_segment)
      loop
         l_calling := null;                                -- Added in Rev 2.3

         if p_new_cogs_segment is not null
         then
            lv_new_cogs := null;
            lv_new_cogs_ccid := null;
            --Ver 2.3 Added by Kishorebabu V as per TMS# 20151105-00158  /*Start*/
            l_calling := 'While Fetching lv_new_cogs';
            l_error_desc :=
                  'cogs_account_id: '
               || rec_item_account_details.cogs_account_id
               || 'p_new_cogs_segment :'
               || p_new_cogs_segment;

            --Ver 2.3 Added by Kishorebabu V as per TMS# 20151105-00158  /*End*/
            select    segment1
                   || '.'
                   || segment2
                   || '.'
                   || segment3
                   || '.'
                   || p_new_cogs_segment
                   || '.'
                   || segment5
                   || '.'
                   || segment6
                   || '.'
                   || segment7
                      new_cogs_concat_segments
              into lv_new_cogs
              from gl_code_combinations_kfv
             where code_combination_id =
                      rec_item_account_details.cogs_account_id;

            begin
               l_calling := 'While Fetching lv_new_cogs_ccid'; --Ver 2.3 Added by Kishorebabu V as per TMS# 20151105-00158
               l_error_desc := 'lv_new_cogs: ' || lv_new_cogs; --Ver 2.3 Added by Kishorebabu V as per TMS# 20151105-00158

               -- Added below code in Rev 2.4 by Vamshi -  Begin
               --               SELECT code_combination_id
               --                 INTO lv_new_cogs_ccid
               --                 FROM gl_code_combinations_kfv
               --                WHERE concatenated_segments = lv_new_cogs;

               lv_new_cogs_ccid :=
                  xxwc_inv_item_acct_update_pkg.create_ccid (lv_new_cogs);
            exception
               when no_data_found
               then
                  p_item_number := rec_item_account_details.item_number;
                  p_org_code := rec_item_account_details.organization_code;
                  p_old_cogs_gl_code := rec_item_account_details.existing_cogs;
                  p_new_cogs_gl_code := lv_new_cogs;
            end;
         end if;

         if p_new_sales_segment is not null
         then
            lv_new_sales := null;
            lv_new_sales_ccid := null;
            l_calling := 'While Fetching lv_new_sales'; --Ver 2.3 Added by Kishorebabu V as per TMS# 20151105-00158
            l_error_desc :=
                  'sales_account_id: '
               || rec_item_account_details.sales_account_id
               || 'p_new_sales_segment :'
               || p_new_sales_segment; --Ver 2.3 Added by Kishorebabu V as per TMS# 20151105-00158

            select    segment1
                   || '.'
                   || segment2
                   || '.'
                   || segment3
                   || '.'
                   || p_new_sales_segment
                   || '.'
                   || segment5
                   || '.'
                   || segment6
                   || '.'
                   || segment7
                      new_sales_concat_segments
              into lv_new_sales
              from gl_code_combinations_kfv
             where code_combination_id =
                      rec_item_account_details.sales_account_id;

            begin
               l_calling := 'While Fetching lv_new_sales_ccid';
               l_error_desc := 'lv_new_sales: ' || lv_new_sales;

               -- Added below code in Rev 2.4 by Vamshi -  Begin
               --               SELECT code_combination_id
               --                 INTO lv_new_sales_ccid
               --                 FROM gl_code_combinations_kfv
               --                WHERE concatenated_segments = lv_new_sales;

               lv_new_sales_ccid :=
                  xxwc_inv_item_acct_update_pkg.create_ccid (lv_new_sales);
            exception
               when no_data_found
               then
                  p_item_number := rec_item_account_details.item_number;
                  p_org_code := rec_item_account_details.organization_code;
                  p_old_sales_gl_code :=
                     rec_item_account_details.existing_sales;
                  p_new_sales_gl_code := lv_new_sales;
            end;
         end if;

         -- Added below code in Rev 2.4 by Vamshi -  Begin

         if p_new_expn_segment is not null
         then
            lv_new_expn := null;
            lv_new_expn_ccid := null;
            l_calling := 'While Fetching lv_new_expn'; 
            l_error_desc :=
                  'Expn_account_id: '
               || rec_item_account_details.expn_account_id
               || 'p_new_expn_segment :'
               || p_new_expn_segment; 

            select    segment1
                   || '.'
                   || segment2
                   || '.'
                   || segment3
                   || '.'
                   || p_new_expn_segment
                   || '.'
                   || segment5
                   || '.'
                   || segment6
                   || '.'
                   || segment7
                      new_expn_concat_segments
              into lv_new_expn
              from gl_code_combinations_kfv
             where code_combination_id =
                      rec_item_account_details.expn_account_id;

            begin
               l_calling := 'While Fetching lv_new_expn_ccid';
               l_error_desc := 'lv_new_expn: ' || lv_new_expn;

               lv_new_expn_ccid :=
                  xxwc_inv_item_acct_update_pkg.create_ccid (lv_new_expn);
            exception
               when no_data_found
               then
                  p_item_number := rec_item_account_details.item_number;
                  p_org_code := rec_item_account_details.organization_code;
                  p_old_expn_gl_code :=
                     rec_item_account_details.existing_expn;
                  p_new_expn_gl_code := lv_new_expn;
            end;
         end if;

         -- Added above code in Rev 2.4 by Vamshi -  End

         if lv_new_expn_ccid is not null or lv_new_sales_ccid is not null or lv_new_cogs_ccid is not null 
         then
            l_calling := 'Apps Initialize';                -- Added in Rev 2.3
            fnd_global.apps_initialize (rec_item_account_details.created_by,
                                        fnd_global.resp_id,
                                        fnd_global.resp_appl_id);
            x_message_list.delete;
            error_handler.initialize;
            l_tbl_item_table (1).transaction_type := 'UPDATE';
            l_tbl_item_table (1).inventory_item_id := p_inventory_item_id;
            l_tbl_item_table (1).organization_id :=
               rec_item_account_details.organization_id;

            if lv_new_expn_ccid is not null
            then
               l_tbl_item_table (1).expense_account := lv_new_expn_ccid;
            end if;

            if lv_new_sales_ccid is not null
            then
               l_tbl_item_table (1).sales_account := lv_new_sales_ccid;
            end if;
            
            if lv_new_cogs_ccid is not null
            then
               l_tbl_item_table (1).cost_of_sales_account := lv_new_cogs_ccid;
            end if;
           

            l_calling := 'Ego Item Process';               -- Added in Rev 2.3
            ego_item_pub.process_items (
               p_api_version     => 1.0,
               p_init_msg_list   => fnd_api.g_true,
               p_commit          => fnd_api.g_true,
               p_item_tbl        => l_tbl_item_table,
               x_item_tbl        => x_tbl_item_table,
               x_return_status   => l_chr_return_status,
               x_msg_count       => l_num_msg_cnt);
            commit;

            if (l_chr_return_status <> fnd_api.g_ret_sts_success)
            then
               error_handler.get_message_list (
                  x_message_list   => x_message_list);
               lv_error_message :=
                     'Error while updating COGS and Sales account for Item Id: '
                  || p_inventory_item_id
                  || ' and organization Id: '
                  || rec_item_account_details.organization_id
                  || '. Error Message: ';
               l_calling :=
                  'Updating Item COGS and Sales account information using EGO_ITEM_PUB.PROCESS_ITEMS API...';

               for i in 1 .. x_message_list.count
               loop
                  lv_error_message :=
                        lv_error_message
                     || i
                     || ')'
                     || x_message_list (i).message_text
                     || ' ';
               end loop;

               xxcus_error_pkg.xxcus_error_main_api (
                  p_called_from         => 'XXWC_POST_CAT_ASSIGN_SUB_PKG.PROCESS_EVENT',
                  p_calling             => l_calling,
                  p_request_id          => null,
                  p_ora_error_msg       => lv_error_message,
                  p_error_desc          => 'Standard API Error',
                  p_distribution_list   => l_missing_gl_dl,
                  p_module              => 'XXWC');
            -- changed the distribution list to Functional list based on Manny's review comment and after discussion with Perry, Manjulla on 19-Aug-14
            -- COMMIT;  --commented after code review with Manjulla on 12-Aug-14
            end if;    --IF (l_chr_return_status <> FND_API.G_RET_STS_SUCCESS)
         end if;
      --IF lv_new_cogs_ccid  IS NOT NULL OR lv_new_sales_ccid IS NOT NULL
      end loop;

      if p_new_cogs_gl_code is not null or p_new_sales_gl_code is not null or p_new_expn_gl_code is not null 
      then
         return false;
      else
         return true;
      end if;
   exception
      when others
      then
         l_parameters :=
               'p_organization_id:- '
            || p_organization_id
            || 'p_inventory_item_id:- '
            || p_inventory_item_id
            || 'p_new_cogs_segment:- '
            || p_new_cogs_segment
            || 'p_new_sales_segment:- '
            || p_new_sales_segment
            || 'p_new_expn_segment:- '
            || p_new_expn_segment
            ; 
         lv_error_message :=
            'Parameters: ' || l_parameters || ' Error Message: ' || sqlerrm; --Ver 2.3 Modified by Kishorebabu V as per TMS# 20151105-00158
         l_calling := 'Processing COGS and SALES and EXPENSE Codes...' || l_calling;
         rollback;
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_POST_CAT_ASSIGN_SUB_PKG.PROCESS_COGS_SALES',
            p_calling             => l_calling,
            p_request_id          => null,
            p_ora_error_msg       => lv_error_message,
            p_error_desc          => 'User defined exception: ' || l_error_desc, --Ver 2.3 Modified by Kishorebabu V as per TMS# 20151105-00158
            p_distribution_list   => cv_dist_list,
            p_module              => 'XXWC');
         -- COMMIT;  --commented after code review with Manjulla on 12-Aug-14
         return null;
   end process_cogs_sales;

   ------------------------------------------
   -- End of additonal code w.r.t. ver 2.1 --
   ------------------------------------------

   /**************************************************************************
   *
   * FUNCTION
   *  process_event
   *
   * DESCRIPTION
   *  This function will get executed when catelog category assignment or ego post batch process Business event is triggered
   *
   * PARAMETERS
   * ==========
   * NAME                          TYPE     DESCRIPTION
   * -----------------             -------- ---------------------------------------------
   * p_subscription_guid   IN          RAW           Subscription GUI ID
   * p_event               IN OUT      wf_event_t    Event details
   RETURN
   VARCHAR2
    ================================================================
       VERSION DATE          AUTHOR(S)       DESCRIPTION
       ------- -----------   --------------- ---------------------------------
       1.0     08-Jan-2014   Praveen Pawar    Initial creation of the package
       2.2     06-Oct-2015   Pattabhi Avula   TMS#20150929-00148 -- Error Alerts
                                              for categories missing Sales and COGS
                                              exception skipped with count values count
   *************************************************************************/

   function process_event (p_subscription_guid   in            raw,
                           p_event               in out nocopy wf_event_t)
      return varchar2
   is
      l_param_name            varchar2 (240);
      l_param_value           varchar2 (2000);
      l_event_name            varchar2 (2000);
      l_err_text              varchar2 (3000);
      l_param_list            wf_parameter_list_t;
      l_org_id                number;
      l_item_id               number;
      l_catalog_id            number;
      ln_request_id           number;
      ln_master_org           number;
      l_category_id           number;
      ln_category_set_id      number;
      ln_cat_intf_rec_cnt     number;
      --------------------------
      -- Added w.r.t. ver 2.1 --
      --------------------------
      ln_inv_cat_set_id       number;
      ln_website_cat_set_id   number;
      ln_inv_cat_rec_cnt      number;
      lv_result               boolean;
      lb_send_email           boolean;
      lv_notif_hdr            varchar2 (2000);
      lv_subject_line         varchar2 (2000);
      l_del                   varchar2 (1) := '|';
      l_file                  utl_file.file_type;
      l_string                varchar2 (3000) := null;
      l_file_name             varchar2 (200) := null;
      l_instance_name         varchar2 (200) := null;
      l_missing_gl_dl         varchar2 (400)
         := apps.fnd_profile.value ('XXWC_EGO_MISSING_GL_DL');
      max_line_length         binary_integer := 32767;
      l_result                varchar2 (2000) := null;
      l_result_msg            varchar2 (4000) := null;
      lv_host                 varchar2 (256) := 'mailoutrelay.hdsupply.net';
      lv_hostport             varchar2 (20) := '25';
      lv_old_cogs_gl_code     gl_code_combinations_kfv.concatenated_segments%type;
      lv_new_cogs_gl_code     gl_code_combinations_kfv.concatenated_segments%type;
      lv_old_sales_gl_code    gl_code_combinations_kfv.concatenated_segments%type;
      lv_new_sales_gl_code    gl_code_combinations_kfv.concatenated_segments%type;
      lv_old_expn_gl_code     gl_code_combinations_kfv.concatenated_segments%type;
      lv_new_expn_gl_code     gl_code_combinations_kfv.concatenated_segments%type;

      ---------------------------------------------
      -- End of code modification w.r.t. ver 2.1 --
      ---------------------------------------------
      lv_error_message        varchar2 (4000);
      cv_dist_list            varchar2 (100)
                                 := 'HDSOracleDevelopers@hdsupply.com';
      l_dml_type              varchar2 (40);
      l_error                 varchar2 (4000);
      lv_org_code             mtl_parameters.organization_code%type := null;
      lv_item_number          mtl_system_items_b.segment1%type := null;
      lv_category             mtl_categories_b.segment1%type := null;
      cv_website_item         mtl_category_sets_tl.category_set_name%type
                                 := 'Website Item';
      no_event_exception      exception;
      cv_inventory_category   mtl_category_sets_tl.category_set_name%type
                                 := 'Inventory Category';
      -- Added w.r.t version 2.1 --
      l_num_msg_cnt           number := null;
      lv_new_cogs_ccid        gl_code_combinations.code_combination_id%type
                                 := null;
      lv_new_sales_ccid       gl_code_combinations.code_combination_id%type
                                 := null;
      lv_new_expn_ccid        gl_code_combinations.code_combination_id%type
                                 := null;
      lv_new_cogs_segment     varchar2 (25) := null;
      lv_new_sales_segment    varchar2 (25) := null;
      lv_new_expn_segment     varchar2 (25) := null;
      l_chr_return_status     varchar2 (100) := null;
      x_message_list          error_handler.error_tbl_type;
      x_tbl_item_table        ego_item_pub.item_tbl_type;
      l_tbl_item_table        ego_item_pub.item_tbl_type;
      l_calling               varchar2 (4000) := null;

      -- Added w.r.t version 2.2 --
      l_count                 number := null;

      --------------------------------------------
      -- Added w.r.t. ver 2.1 to declare cursor --
      -- Cursor to fetch item and organizations --
      -- from base table based on Item ID       --
      --------------------------------------------
      cursor cur_item_details (
         pn_item_id   in number)
      is
           select msi.inventory_item_id, msi.organization_id
             from mtl_system_items_b msi, mtl_parameters mtp
            where     msi.inventory_item_id = pn_item_id
                  and msi.organization_id = mtp.organization_id
         order by mtp.organization_code;

      --------------------------------------------
      -- Added w.r.t. ver 2.1 to declare cursor --
      -- Cursor to fetch all the successfully   --
      -- processed records for Inventory Category-
      -- catalog from interface table           --
      --------------------------------------------
      cursor cur_batch_item_details (
         pn_request_id   in number,
         pn_cat_set_id   in number,
         pn_org_id       in number)
      is
         select mic.inventory_item_id,
                mcb.attribute1 cogs_segment,
                mcb.attribute2 sales_segment,
                mcb.attribute8 expn_segment
           from apps.mtl_item_categories_interface mic,
                apps.mtl_category_sets_b mcs,
                apps.mtl_categories_b mcb
          where     mic.request_id = pn_request_id
                and mic.category_set_id = pn_cat_set_id
                and mic.process_flag = 7
                and mic.transaction_type in ('CREATE', 'UPDATE')
                and mic.organization_id = pn_org_id
                and mcs.category_set_id = mic.category_set_id
                and mcs.structure_id = mcb.structure_id
                and mcb.category_id = mic.category_id;
   begin
      if (p_event is null)
      then
         raise no_event_exception;
      end if;

      l_event_name := p_event.geteventname ();
      l_param_list := p_event.getparameterlist;
      l_calling := 'Checking event name and fetching parameter value...';

      if     l_param_list is not null
         and l_event_name =
                'oracle.apps.ego.item.postCatalogAssignmentChange'
      then
         ln_master_org :=
            to_number (fnd_profile.value ('XXWC_ITEM_MASTER_ORG'));

         for i in l_param_list.first .. l_param_list.last
         loop
            l_param_name := l_param_list (i).getname;
            l_param_value := l_param_list (i).getvalue;

            if (l_param_name = 'DML_TYPE')
            then
               l_dml_type := l_param_value;
            elsif (l_param_name = 'INVENTORY_ITEM_ID')
            then
               l_item_id := l_param_value;
            elsif (l_param_name = 'ORGANIZATION_ID')
            then
               l_org_id := l_param_value;
            elsif (l_param_name = 'CATALOG_ID')
            then
               l_catalog_id := l_param_value;
            elsif (l_param_name = 'CATEGORY_ID')
            then
               l_category_id := l_param_value;
            end if;
         end loop;

         commit;

         if ln_master_org = l_org_id
         then
            ln_category_set_id := null;
            l_calling := 'Fetching category set ID for Website item...';

            begin
               select category_set_id
                 into ln_category_set_id
                 from mtl_category_sets_tl
                where category_set_name = cv_website_item;
            exception
               when others
               then
                  ln_category_set_id := null;
            end;

            if ln_category_set_id = l_catalog_id
            then
               l_calling := 'Fetching category value...';

               begin
                  select mcb.segment1
                    into lv_category
                    from mtl_category_sets_b mcs, mtl_categories_b mcb
                   where     mcs.category_set_id = l_catalog_id
                         and mcs.structure_id = mcb.structure_id
                         and mcb.category_id = l_category_id;
               exception
                  when others
                  then
                     lv_category := null;
               end;

               if    (l_dml_type = 'UPDATE' and lv_category = 'N')
                  or (l_dml_type = 'DELETE')
               then
                  l_calling := 'Fetching item number and organization code...';

                  select msi.segment1, mtp.organization_code
                    into lv_item_number, lv_org_code
                    from mtl_system_items_b msi, mtl_parameters mtp
                   where     msi.organization_id = mtp.organization_id
                         and msi.inventory_item_id = l_item_id
                         and mtp.organization_id = l_org_id;

                  insert into xxwc.xxwc_ego_catalog_category_log
                       values (lv_item_number,
                               lv_org_code,
                               l_item_id,
                               l_org_id,
                               cv_website_item,
                               l_catalog_id,
                               lv_category,
                               l_category_id,
                               l_dml_type,
                               fnd_global.user_id,
                               sysdate);

                  commit;
               end if;
            -------------------------------------------------------------------------
            -- Added w.r.t ver 2.1 to handle processing Inventory Category catalog --
            -------------------------------------------------------------------------
            else
               l_calling := 'Fetching instance name...';

               begin
                  select upper (instance_name)
                    into l_instance_name
                    from v$instance;
               exception
                  when others
                  then
                     null;
               end;

               lv_subject_line :=
                     'Error: Missing SALES or COGS or EXPENSE GL Code - See Attached for details. Instance - '
                  || l_instance_name;
               lv_notif_hdr :=
                  'The Inventory Category classification (cat class) assignment for an Item has changed in the MST organization, but a corresponding GL account does not exist at one or more other organizations.  Please contact the finance department to create the missing GL account for the organization(s) listed in the attachment.';
               l_file_name :=
                     'Missing_GL_Code_'
                  || to_char (sysdate, 'DD-MON-YY HH.MI AM')
                  || '.csv';
               l_file :=
                  utl_file.fopen ('XXWC_PDH_MISSING_GL_CODE',
                                  l_file_name,
                                  'W',
                                  max_line_length);
               l_string :=
                  'Item Number|Organization Code|Existing COGS GL Code Combination|New COGS GL Code Combination|Existing SALES GL Code Combination|New SALES GL Code Combination|Existing EXPENSE GL Code Combination|New EXPENSE GL Code Combination';
               utl_file.put_line (l_file, l_string);
               ln_category_set_id := null;
               l_calling :=
                  'Fetching category set ID for Inventory category...';

               begin
                  select category_set_id
                    into ln_category_set_id
                    from apps.mtl_category_sets_tl
                   where category_set_name = cv_inventory_category;
               exception
                  when others
                  then
                     ln_category_set_id := null;
               end;

               if     ln_category_set_id = l_catalog_id
                  and l_dml_type <> 'DELETE'
               then
                  lv_new_cogs_segment := null;
                  lv_new_sales_segment := null;
                  lv_new_expn_segment := null;
                  l_calling := 'Fetching COGS, EXPENSE and Sales segment...';

                  begin
                     l_calling :=
                           'Fetching COGS, Sales and Expense account segment values for Inventory Category catalog. Category Id: '
                        || l_category_id;

                     select mcb.attribute1, mcb.attribute2, mcb.attribute8
                       into lv_new_cogs_segment,
                            lv_new_sales_segment,
                            lv_new_expn_segment
                       from apps.mtl_category_sets_b mcs,
                            apps.mtl_categories_b mcb
                      where     mcs.category_set_id = l_catalog_id
                            and mcs.structure_id = mcb.structure_id
                            and mcb.category_id = l_category_id;
                  exception
                     when others
                     then
                        lv_error_message := 'Error Message: ' || sqlerrm;
                        rollback;
                        xxcus_error_pkg.xxcus_error_main_api (
                           p_called_from         => 'XXWC_POST_CAT_ASSIGN_SUB_PKG.PROCESS_EVENT',
                           p_calling             => l_calling,
                           p_request_id          => null,
                           p_ora_error_msg       => lv_error_message,
                           p_error_desc          => 'User defined exception',
                           p_distribution_list   => l_missing_gl_dl,
                           p_module              => 'XXWC');
                        -- changed the distribution list to Functional list based on Manny's review comment and after discussion with Perry, Manjulla on 19-Aug-14
                        --   COMMIT; --commented after review with Manjulla on 12-Aug-14
                        return l_error;
                  end;

                  if    lv_new_cogs_segment is not null
                     or lv_new_sales_segment is not null
                  then
                     for rec_item_details in cur_item_details (l_item_id)
                     loop
                        lv_result := null;
                        lv_org_code := null;
                        lv_item_number := null;
                        lv_old_cogs_gl_code := null;
                        lv_new_cogs_gl_code := null;
                        lv_old_sales_gl_code := null;
                        lv_new_sales_gl_code := null;
                        lv_old_expn_gl_code := null;
                        lv_new_expn_gl_code := null;
                        lv_result :=
                           process_cogs_sales (
                              rec_item_details.inventory_item_id,
                              rec_item_details.organization_id,
                              lv_new_cogs_segment,
                              lv_new_sales_segment,
                              lv_new_expn_segment,
                              lv_item_number,
                              lv_org_code,
                              lv_old_cogs_gl_code,
                              lv_new_cogs_gl_code,
                              lv_old_sales_gl_code,
                              lv_new_sales_gl_code,
                              lv_old_expn_gl_code,
                              lv_new_expn_gl_code);

                        if not lv_result
                        then
                           lb_send_email := true;

                           if    lv_old_cogs_gl_code is not null
                              or lv_old_sales_gl_code is not null
                              or lv_old_expn_gl_code is null
                           then
                              select    lv_item_number
                                     || l_del
                                     || lv_org_code
                                     || l_del
                                     || decode (lv_old_cogs_gl_code,
                                                null, '',
                                                lv_old_cogs_gl_code)
                                     || l_del
                                     || decode (lv_old_cogs_gl_code,
                                                null, '',
                                                lv_new_cogs_gl_code)
                                     || l_del
                                     || decode (lv_old_sales_gl_code,
                                                null, '',
                                                lv_old_sales_gl_code)
                                     || l_del
                                     || decode (lv_old_sales_gl_code,
                                                null, '',
                                                lv_new_sales_gl_code)
                                     || l_del
                                     || decode (lv_old_expn_gl_code,
                                                null, '',
                                                lv_old_expn_gl_code)
                                     || l_del
                                     || decode (lv_old_expn_gl_code,
                                                null, '',
                                                lv_new_expn_gl_code)
                                into l_string
                                from dual;
                           end if;

                           /*

                              IF     lv_old_cogs_gl_code IS NOT NULL
                                 AND lv_old_sales_gl_code IS NOT NULL
                              THEN
                                 l_string :=
                                       lv_item_number
                                    || l_del
                                    || lv_org_code
                                    || l_del
                                    || lv_old_cogs_gl_code
                                    || l_del
                                    || lv_new_cogs_gl_code
                                    || l_del
                                    || lv_old_sales_gl_code
                                    || l_del
                                    || lv_new_sales_gl_code;
                              ELSIF     lv_old_cogs_gl_code IS NOT NULL
                                    AND lv_old_sales_gl_code IS NULL
                              THEN
                                 l_string :=
                                       lv_item_number
                                    || l_del
                                    || lv_org_code
                                    || l_del
                                    || lv_old_cogs_gl_code
                                    || l_del
                                    || lv_new_cogs_gl_code
                                    || l_del
                                    || ''
                                    || l_del
                                    || '';
                              ELSIF     lv_old_cogs_gl_code IS NULL
                                    AND lv_old_sales_gl_code IS NOT NULL
                              THEN
                                 l_string :=
                                       lv_item_number
                                    || l_del
                                    || lv_org_code
                                    || l_del
                                    || ''
                                    || l_del
                                    || ''
                                    || l_del
                                    || lv_old_sales_gl_code
                                    || l_del
                                    || lv_new_sales_gl_code;
                              END IF;
                            */

                           utl_file.put_line (l_file, l_string);
                        end if;
                     end loop;
                  else
                     --<Starts> Added for Ver#2.2 on 06-OCT-2015 for TMS#20150929-00148
                     begin
                        select count (*)
                          into l_count
                          from fnd_flex_value_sets ffvs,
                               fnd_flex_values_vl ffvl
                         where     ffvs.flex_value_set_id =
                                      ffvl.flex_value_set_id
                               and ffvs.flex_value_set_name =
                                      'XXWC_INVENTORY_CATEGORY_VS'
                               and ffvl.enabled_flag = 'Y'
                               and ffvl.flex_value = l_category_id;
                     exception
                        when others
                        then
                           l_count := 0;
                     end;

                     if l_count = 0
                     then
                        xxcus_error_pkg.xxcus_error_main_api (
                           p_called_from         => 'XXWC_POST_CAT_ASSIGN_SUB_PKG.PROCESS_EVENT',
                           p_calling             => l_calling,
                           p_request_id          => null,
                           p_ora_error_msg       =>    'COGS and Sales DFFs are not configured for Inventory Category ID: '
                                                    || l_category_id,
                           p_error_desc          => 'User defined exception',
                           p_distribution_list   => l_missing_gl_dl,
                           p_module              => 'XXWC');
                     end if;

                     --<Ends> Added for Ver#2.2 on 06-OCT-2015 for TMS#20150929-00148
                     -- changed the distribution list to Functional list based on Manny's review comment and after discussion with Perry, Manjulla on 19-Aug-14
                     commit;
                  end if;

                  --lv_new_cogs_segment  IS NOT NULL OR lv_new_sales_segment IS NOT NULL
                  if lb_send_email
                  then
                     utl_file.fclose (l_file);
                     xxcus_misc_pkg.send_email_attachment (
                        p_sender        => 'no-reply@whitecap.net',
                        p_recipients    => l_missing_gl_dl,
                        p_subject       => lv_subject_line,
                        p_message       => lv_notif_hdr,
                        p_attachments   => l_file_name,
                        x_result        => l_result,
                        x_result_msg    => l_result_msg);
                  end if;

                  commit;
               end if;                  --IF ln_category_set_id = l_catalog_id
            end if;
         end if;
      ----------------------------------------------------------------------------------
      -- Added as per version # 2.0 on 18-Mar-2014                                    --
      -- Capture oracle.apps.ego.batch.postbatchprocess business event                --
      -- And check if it got triggered for processing Website Item Catalog Categories --
      ----------------------------------------------------------------------------------
      elsif     l_param_list is not null
            and l_event_name = 'oracle.apps.ego.batch.postbatchprocess'
      then
         ln_master_org :=
            to_number (fnd_profile.value ('XXWC_ITEM_MASTER_ORG'));

         for i in l_param_list.first .. l_param_list.last
         loop
            l_param_name := l_param_list (i).getname;
            l_param_value := l_param_list (i).getvalue;

            if (l_param_name = 'REQUEST_ID')
            then
               ln_request_id := l_param_value;
            end if;
         end loop;

         --------------------------
         -- Added w.r.t. ver 2.1 --
         --------------------------
         l_calling := 'Fetching category set ID for website catalog...';

         begin
            select category_set_id
              into ln_website_cat_set_id
              from mtl_category_sets_tl
             where category_set_name = cv_website_item;
         exception
            when others
            then
               ln_category_set_id := null;
         end;

         ---------------------------------------------
         -- End of code modification w.r.t. ver 2.1 --
         ---------------------------------------------
         ln_cat_intf_rec_cnt := 0;

         select count (1)
           into ln_cat_intf_rec_cnt
           from mtl_item_categories_interface
          where     request_id = ln_request_id
                and category_set_id = ln_website_cat_set_id;

         -- Added w.r.t. ver 2.1 --
         if ln_cat_intf_rec_cnt > 0
         then
            begin
               insert into xxwc.xxwc_ego_catalog_category_log
                  (select item_number,
                          organization_code,
                          inventory_item_id,
                          organization_id,
                          category_set_name,
                          category_set_id,
                          category_name,
                          category_id,
                          transaction_type,
                          created_by,
                          last_update_date
                     from inv.mtl_item_categories_interface
                    where     request_id = ln_request_id
                          and process_flag = 7
                          and category_set_name = cv_website_item
                          and organization_id = ln_master_org
                          and (       transaction_type = 'UPDATE'
                                  and category_name = 'N'
                               or transaction_type = 'DELETE'));

               commit;
            exception
               when others
               then
                  lv_error_message := 'Error Message: ' || sqlerrm;
                  l_calling :=
                     'Capture SKUs which underwent change in Website Item Catalog Category value.';
                  rollback;
                  xxcus_error_pkg.xxcus_error_main_api (
                     p_called_from         => 'XXWC_POST_CAT_ASSIGN_SUB_PKG.PROCESS_EVENT',
                     p_calling             => l_calling,
                     p_request_id          => ln_request_id,
                     p_ora_error_msg       => lv_error_message,
                     p_error_desc          => 'User defined exception',
                     p_distribution_list   => cv_dist_list,
                     p_module              => 'XXWC');
            --   COMMIT; --commented after review with Manjulla on 12-Aug-14
            end;
         end if;

         -- End of code modification w.r.t version # 2.0 --

         -------------------------
         -- Added w.r.t ver 2.1 --
         -------------------------
         begin
            select upper (instance_name) into l_instance_name from v$instance;
         exception
            when others
            then
               null;
         end;

         lv_subject_line :=
               'Error: Missing SALES or COGS GL Code - See Attached for details. Instance - '
            || l_instance_name;
         lv_notif_hdr :=
            'The Inventory Category classification (cat class) assignment for an Item has changed in the MST organization, but a corresponding GL account does not exist at one or more other organizations.  Please contact the finance department to create the missing GL account for the organization(s) listed in the attachment.';
         l_file_name :=
               'Missing_GL_Code_'
            || to_char (sysdate, 'DD-MON-YY HH.MI AM')
            || '.csv';
         l_file :=
            utl_file.fopen ('XXWC_PDH_MISSING_GL_CODE',
                            l_file_name,
                            'W',
                            max_line_length);
         l_string :=
            'Item Number|Organization Code|Existing COGS GL Code Combination|New COGS GL Code Combination|Existing SALES GL Code Combination|New SALES GL Code Combination|Existing EXPENSE GL Code Combination|New EXPENSE GL Code Combination';
         utl_file.put_line (l_file, l_string);
         ln_inv_cat_set_id := null;

         begin
            select category_set_id
              into ln_inv_cat_set_id
              from apps.mtl_category_sets_tl
             where category_set_name = cv_inventory_category;
         exception
            when others
            then
               ln_category_set_id := null;
         end;

         select count (1)
           into ln_inv_cat_rec_cnt
           from mtl_item_categories_interface
          where     request_id = ln_request_id
                and category_set_id = ln_inv_cat_set_id;

         if ln_inv_cat_rec_cnt > 0
         then
            for rec_batch_item_details
               in cur_batch_item_details (ln_request_id,
                                          ln_inv_cat_set_id,
                                          ln_master_org)
            loop
               if    rec_batch_item_details.cogs_segment is not null
                  or rec_batch_item_details.sales_segment is not null
               then
                  for rec_item_details
                     in cur_item_details (
                           rec_batch_item_details.inventory_item_id)
                  loop
                     lv_result := null;
                     lv_org_code := null;
                     lv_item_number := null;
                     lv_old_cogs_gl_code := null;
                     lv_new_cogs_gl_code := null;
                     lv_old_sales_gl_code := null;
                     lv_new_sales_gl_code := null;
                     lv_old_expn_gl_code := null;
                     lv_new_expn_gl_code := null;

                     lv_result :=
                        process_cogs_sales (
                           rec_item_details.inventory_item_id,
                           rec_item_details.organization_id,
                           rec_batch_item_details.cogs_segment,
                           rec_batch_item_details.sales_segment,
                           rec_batch_item_details.expn_segment,
                           lv_item_number,
                           lv_org_code,
                           lv_old_cogs_gl_code,
                           lv_new_cogs_gl_code,
                           lv_old_sales_gl_code,
                           lv_new_sales_gl_code,
                           lv_old_expn_gl_code,
                           lv_new_expn_gl_code);

                     if not lv_result
                     then
                        lb_send_email := true;

                        if    lv_old_cogs_gl_code is not null
                           or lv_old_sales_gl_code is not null
                           or lv_old_expn_gl_code is not null
                        then
                           select    lv_item_number
                                  || l_del
                                  || lv_org_code
                                  || l_del
                                  || decode (lv_old_cogs_gl_code,
                                             null, null,
                                             lv_old_cogs_gl_code)
                                  || l_del
                                  || decode (lv_old_cogs_gl_code,
                                             null, null,
                                             lv_new_cogs_gl_code)
                                  || l_del
                                  || decode (lv_old_sales_gl_code,
                                             null, null,
                                             lv_old_sales_gl_code)
                                  || l_del
                                  || decode (lv_old_sales_gl_code,
                                             null, null,
                                             lv_new_sales_gl_code)
                                  || l_del
                                  || decode (lv_old_expn_gl_code,
                                             null, null,
                                             lv_old_expn_gl_code)
                                  || l_del
                                  || decode (lv_old_expn_gl_code,
                                             null, null,
                                             lv_new_expn_gl_code)
                             into l_string
                             from dual;
                        /*

                         IF     lv_old_cogs_gl_code IS NOT NULL
                            AND lv_old_sales_gl_code IS NOT NULL
                         THEN
                            l_string :=
                                  lv_item_number
                               || l_del
                               || lv_org_code
                               || l_del
                               || lv_old_cogs_gl_code
                               || l_del
                               || lv_new_cogs_gl_code
                               || l_del
                               || lv_old_sales_gl_code
                               || l_del
                               || lv_new_sales_gl_code
                               ;
                         ELSIF     lv_old_cogs_gl_code IS NOT NULL
                               AND lv_old_sales_gl_code IS NULL
                         THEN
                            l_string :=
                                  lv_item_number
                               || l_del
                               || lv_org_code
                               || l_del
                               || lv_old_cogs_gl_code
                               || l_del
                               || lv_new_cogs_gl_code
                               || l_del
                               || ''
                               || l_del
                               || '';
                         ELSIF     lv_old_cogs_gl_code IS NULL
                               AND lv_old_sales_gl_code IS NOT NULL
                         THEN
                            l_string :=
                                  lv_item_number
                               || l_del
                               || lv_org_code
                               || l_del
                               || ''
                               || l_del
                               || ''
                               || l_del
                               || lv_old_sales_gl_code
                               || l_del
                               || lv_new_sales_gl_code;
                         END IF;
 */
                        end if;

                        utl_file.put_line (l_file, l_string);
                     end if;
                  end loop;
               end if;
            end loop;

            if lb_send_email
            then
               utl_file.fclose (l_file);
               xxcus_misc_pkg.send_email_attachment (
                  p_sender        => 'no-reply@whitecap.net',
                  p_recipients    => l_missing_gl_dl,
                  p_subject       => lv_subject_line,
                  p_message       => lv_notif_hdr,
                  p_attachments   => l_file_name,
                  x_result        => l_result,
                  x_result_msg    => l_result_msg);
            end if;

            commit;
         end if;
      end if;

      return 'SUCCESS';
   exception
      when no_event_exception
      then
         return 'ERROR: WF_EVENT_MSG IS NULL';
      when others
      then
         l_error := sqlerrm;
         l_err_text := 'Error : ' || to_char (sqlcode) || '---' || l_error;
         rollback;
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_POST_CAT_ASSIGN_SUB_PKG.PROCESS_EVENT',
            --p_calling                => 'Main function...',
            p_calling             => l_calling,
            p_request_id          => null,
            --p_ora_error_msg          => l_err_text,
            p_ora_error_msg       => substr (
                                          'Error_Stack...'
                                       || dbms_utility.format_error_stack ()
                                       || 'Error_Backtrace...'
                                       || dbms_utility.format_error_backtrace (),
                                       1,
                                       2000),
            --p_error_desc             => 'User defined exception',
            p_error_desc          => substr (l_err_text, 1, 240),
            p_distribution_list   => cv_dist_list,
            p_module              => 'XXWC');
         -- COMMIT; --commented after review with Manjulla on 12-Aug-14
         return l_error;
   end process_event;
end xxwc_post_cat_assign_sub_pkg;
/