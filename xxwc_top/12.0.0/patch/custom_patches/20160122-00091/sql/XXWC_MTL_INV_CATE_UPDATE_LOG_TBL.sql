/******************************************************************************************************
-- File Name: XXWC_MTL_INV_CATE_UPDATE_LOG.sql
--
-- PROGRAM TYPE: Table script
--
-- PURPOSE: Table use to track account details on MTL_CATEGORIES table.
-- HISTORY
-- ==========================================================================================================
-- ==========================================================================================================
-- VERSION DATE          AUTHOR(S)       DESCRIPTION
-- ------- -----------   --------------- --------------------------------------------------------------------
-- 1.0     21-OCT-2016   P.Vamshidhar    TMS#20160122-00091  GL Code Enhancements to leverage GL API
--                                       Initial version

************************************************************************************************************/
CREATE TABLE XXWC.XXWC_MTL_INV_CATE_UPDATE_LOG
(
  CATEGORY_ID       NUMBER,
  COGS_ACCT         VARCHAR2(150 BYTE),
  SALES_ACCT        VARCHAR2(150 BYTE),
  EXPN_ACCT         VARCHAR2(150 BYTE),
  PROCESS_STATUS    VARCHAR2(10 BYTE),
  REQUEST_ID        NUMBER,
  CREATION_DATE     DATE,
  LAST_UPDATE_DATE  DATE
)
/