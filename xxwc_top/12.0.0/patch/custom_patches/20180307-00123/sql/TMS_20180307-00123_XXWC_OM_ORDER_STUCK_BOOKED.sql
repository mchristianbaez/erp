/*************************************************************************
  $Header TMS_20180307-00123_XXWC_OM_ORDER_STUCK_BOOKED.sql $
  Module Name: TMS_XXWC_OM_ORDER_STUCK_BOOKED.sql

  PURPOSE:   Created to process the order headers stucked in booked status
             and lines are in CLOSED or CANCELLED status.

  REVISIONS:
  Ver        Date        Author               Description
  ---------  ----------  ---------------      -------------------------
  1.0        03/19/2018  Pattabhi Avula       TMS#20180307-00123 
                                              Initial Version
**************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000;
DECLARE

CURSOR cur_ords
  IS   
   SELECT rowid hdr_rowid, oh.header_id
     FROM apps.oe_order_headers_all oh 
    WHERE oh.flow_status_code = 'BOOKED' 
	  AND oh.order_type_id IN (1001,1011,1004)
	  AND TRUNC(oh.last_update_date) <= TRUNC(SYSDATE-7) 
      AND EXISTS (SELECT 1
                    FROM apps.wf_items wi
                   WHERE wi.item_key(+) = oh.header_id
                     AND wi.item_type = 'OEOH'
                     AND  wi.end_date IS NULL)
      AND NOT EXISTS(SELECT 1
                       FROM apps.oe_order_lines_all ol   
                      WHERE ol.header_id=oh.header_id        
                        AND ol.flow_status_code NOT IN ('CLOSED','CANCELLED'))
   UNION
   SELECT rowid hdr_rowid, oh.header_id 
     FROM apps.oe_order_headers_all oh
    WHERE oh.flow_status_code = 'BOOKED'
	  AND oh.order_type_id IN (1001,1011,1004)
      AND TRUNC(oh.last_update_date) <= TRUNC(SYSDATE-7)	
      AND NOT EXISTS (SELECT 1
                        FROM APPS.wf_items wi
                       WHERE WI.item_key(+) = oh.header_id
                         AND wi.item_type = 'OEOH')
      AND NOT EXISTS(SELECT 1
                       FROM  apps.oe_order_lines_all ol   
                      WHERE ol.header_id=oh.header_id        
                        AND ol.flow_status_code NOT IN ('CLOSED','CANCELLED'));
						
-- Local variables
 l_script_name                VARCHAR2 (256) := 'XXWC_OM_ORDER_STUCK_BOOKED datafix';
 l_updated_rowcount           NUMBER (20);
 l_sec                        VARCHAR2 (256);
 l_request_id                 NUMBER(38):= fnd_global.conc_request_id;
 l_locked_line                VARCHAR2(2);
 l_curr_date                  DATE DEFAULT SYSDATE-7;
 l_count                      NUMBER(10):=0;
 l_max_date                   DATE;
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20180307-00123   , Before Update');
   
   FOR I IN cur_ords 
     LOOP
	 
	 -- Inserting into custom table to track the orders.
	     l_locked_line := xxwc_om_force_ship_pkg.is_row_locked (I.hdr_rowid, 'OE_ORDER_HEADERS_ALL');
      IF l_locked_line = 'N' THEN
		       l_sec := 'Insert record for '||l_script_name;
         xxwc_om_orders_datafix_pkg.insert_record_log( 
	                              p_order_header_id   => I.header_id
			                     ,p_order_line_id     => NULL
			                     ,p_request_id        => l_request_id
			                     ,p_script_name       => l_script_name
			                    );    
	  -- Fetching the line level max last update date.
	  BEGIN
	   SELECT MAX(TRUNC(LAST_UPDATE_DATE))
	     INTO l_max_date
	     FROM apps.oe_order_lines_all 
	    WHERE header_id = i.header_id;
	  EXCEPTION
	   WHEN OTHERS THEN
	     l_max_date:=SYSDATE;
	  END;
	  
	  -- checking the max last update date with sysdate-7 value
	  
	   IF l_max_date <= TRUNC(l_curr_date) THEN 
	    
		  -- Updating the headers table
		  UPDATE oe_order_headers_all
		     SET flow_status_code = 'CLOSED'
                 ,open_flag = 'N'
		   WHERE header_id = I.header_id;
	   END IF;	  
	  l_count:=l_count+1;
	  
	  END IF;  -- Lock checking end if
	  
	 END LOOP;
	 DBMS_OUTPUT.put_line (
         'TMS: 20180307-00123  Num of Sales orders  updated are: '||l_count);
	 COMMIT;
	  DBMS_OUTPUT.put_line ('TMS: 20180307-00123  , End Update');
EXCEPTION
 WHEN OTHERS THEN
 DBMS_OUTPUT.put_line ('TMS: 20180307-00123, Errors : ' || SQLERRM);
END;
/