CREATE VIEW XXCUS.XXCUS_OPN_CURRENT_PROPERTY_V AS
/* 
-- *****************************************************************************************
-- Scope: This view will be the source for the APEX COMMON systems and the Web Application
--               Location Finder Plus tool.
-- Grants:  Oracle EBIZPRD database user XXCUS has granted "SELECT" access to user INTERFACE_DSTAGE
-- $Header XXCUS_OPN_CURRENT_PROPERTY_V.sql $
-- Module Name: HDS Property Manager
-- REVISIONS:
-- Ver        Date                 Author                            Description
-- ---------  ----------          ----------                           ----------------
-- 1.0        10/14/2015  Bala Seshadri                Created.                                      
-- ***************************************************************************************** 
*/
SELECT *
  FROM XXCUS.XXCUS_OPN_PROPERTIES_ALL
 WHERE 1 = 1 
       AND OPN_TYPE = 'PROPERTY' 
       AND HDS_CURRENT_FLAG = 'Y';
--
COMMENT ON TABLE XXCUS.XXCUS_OPN_CURRENT_PROPERTY_V IS 'ESMS: 305018  OR TMS: 20151014-00163';
--
GRANT SELECT ON XXCUS.XXCUS_OPN_CURRENT_PROPERTY_V TO INTERFACE_DSTAGE;
--