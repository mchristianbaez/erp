#!/usr/bin/perl
use IPC::Open2;

sub buildSqlCommand($);
sub processLdtFiles($);
sub directoryExists($$);
sub getLctFileName($$);
sub processJltFiles($);
sub printAntBuildFile();
sub checkXmlFileType($$);
sub checkFileType($$);
sub processXMLPubFiles($);
sub initsystemVariables($);
sub generateBuildCommands();
sub runBuildCommands();
sub executeCommand($);
sub processSQLFiles($);
sub processFormsFiles($);
sub processPLDFiles($);
sub processHTMLFiles($);
sub addCommandToStack($);
sub executeUserHook($);
sub printDebug($$$);
sub printRecompileObjectsSql($);
sub processWFTFiles($);
sub processWFXFiles($);
sub verifyParams($);
sub printUsage();
sub processImageFiles($);
sub processProgFiles($);
sub processOAServerFiles($$);
sub getFileList ($);
sub processOAFiles($);
sub processJpxFiles($$);
sub processOAWebuiFiles($$);
sub processOAServerFiles($$);

verifyParams(\@ARGV);

my $INSTALL_DIR = $ARGV[1];
my $log_dir=$INSTALL_DIR;


my $debugOn=0;
my $debugParam=$ARGV[2];
if(defined($debugParam))
{
  if($debugParam=~ /debug=true/)
  {
    $debugOn=1;
  }
}

#<STDIN>;
#$INSTALL_DIR ="/home/am11icr3/code/code";

######################################################################################
#Get values of environment variables
######################################################################################
my $AU_TOP = $ENV{"AU_TOP"};
printDebug($l_progress,$debugMod," $AU_TOP  :". $AU_TOP );
my $JAVA_TOP = $ENV{"JAVA_TOP"};
printDebug($l_progress,$debugMod," $JAVA_TOP  :". $JAVA_TOP );
my $CUSTOM_TOP = $ENV{"CUSTOM_TOP"};
printDebug($l_progress,$debugMod," $CUSTOM_TOP  :". $CUSTOM_TOP );
my $COMMON_TOP = $ENV{"COMMON_TOP"};
printDebug($l_progress,$debugMod," $COMMON_TOP  :". $COMMON_TOP );
my $INSTALL_FILE=$ENV{"CONFIG_FILE"};
printDebug($l_progress,$debugMod," $INSTALL_FILE :". $INSTALL_FILE);

######################################################################################
#Variable declaration to track the debug module being executed. 
######################################################################################

my $debugMod="";

######################################################################################
#Variable declarations for init parameters. 
#These are all supplied in install.txt
######################################################################################

my $apps_pwd=$ARGV[0];
my $apps_user="apps";
my $db_server;
my $db_port;
my $db_sid;
my $pre_process_file="pre_process.txt";
my $post_process_file="post_process.txt";

## HDS Customization
# These are used by the SQL procedure for the case where the file is an EIS script.
my $eis_user="xxeis";
my $eis_pwd=$ARGV[3];

my $currentdir;
my $prevdir;
my $startdir;

my @commandsToExecute=();


######################################################################################
##Initialise the #system variables like jdbc connection and password: This is needed 
##to ensure that  all the wfload/fndload/xdoloader commands have the right information 
##for installation.
######################################################################################

printDebug($l_progress,$debugMod,$INSTALL_DIR);
initsystemVariables($INSTALL_FILE);
# HDS Customized to use the Java top custom folder
#my $JAVA_DIR=$CUSTOM_TOP."/java";
my $JAVA_DIR=$JAVA_TOP;
######################################################################################
#open the log file in the right directory for installation
######################################################################################

open(LOG_FILE,"+>$log_dir/log.txt");
open(DEBUG_FILE,"+>$log_dir/debug.txt");
printDebug($l_progress,$debugMod,"ls -1 $INSTALL_DIR");

######################################################################################
#Generate the list of subdirectories that need to be processed.
######################################################################################

my @sub_directories=`ls -1 $INSTALL_DIR`;

######################################################################################
#Generate all the build commands for files in the installation script
######################################################################################

generateBuildCommands();

######################################################################################
#Run all the build commands
######################################################################################

runBuildCommands();

######################################################################################
#Finish and close the log files Execute the post_process user hook if it is defined
######################################################################################

close LOG_FILE;
close DEBUG_FILE;

######################################################################################
# runBuildCommands : takes the the array commands_to_execute 
# as an input and individually calls out the code to execute
# each command and capture the output in log files.
######################################################################################

sub runBuildCommands()
{
  my $l_progress="000";
  my $debugMod="runBuildCommands";
  my $count=0;
  $l_progress="010";
  foreach $cmd (@commandsToExecute) {
    $l_progress="020";
#    printDebug($l_progress,$debugMod, $cmd);
    $l_progress="030";
#    <STDIN>;
    executeCommand($cmd);
    $l_progress="040";
    $count++;
    $l_progress="050";
# The code below is only for testing and not to be used as such.
#    if($count==5)
#    {
#      exit;
#    }
  }
}
######################################################################################
# executeCommand : takes the actual command 
# as an input. The output is read from the out
# buffer and is redirected the log file.
######################################################################################
sub executeCommand($)
{
  my $l_progress="000";
  my $debugMod="executeCommand";
  my $cmd=$_[0];
  printDebug($l_progress,$debugMod," $cmd :". $cmd);
  my $pid = open2(*READER, *WRITER, "$cmd" );
  printDebug($l_progress,$debugMod," $pid  :". $pid );
  $l_progress="020";
  my $line="";
  close(WRITER);
  $l_progress="030";
  while(<READER>)
  {
    $l_progress="040";
    $line=$_;
    print LOG_FILE $line;
    #printDebug($l_progress,$debugMod,$line);
    $l_progress="050";
  }
  $l_progress="060";
  close(READER);
  $l_progress="070";
}
######################################################################################
# generateBuildCommands : looks at each subdirectory
# and looks at the fiels in each of them and does the
# processing required to generate the command to install
# the file.
######################################################################################

sub generateBuildCommands()
{
  my $debugMod="generateBuildCommands";
  my $l_progress="000";
  printDebug($l_progress,$debugMod,"Start");
  if(defined(@sub_directories))
  {
    $l_progress="010";
    printDebug($l_progress,$debugMod,"Sub directories defined");
    $l_progress="020";
    $startdir=`pwd`;
    $l_progress="030";
    $currentdir=$startdir;
    $l_progress="040";
    chdir($INSTALL_DIR);
    $l_progress="050";
    ######################################################################################
    #Execute the pre_process user hook if it is defined in install.txt
    ######################################################################################

    executeUserHook("pre_process");

    if(directoryExists(\@sub_directories,"sql")==1)
    {
      $l_progress="060";
      processSQLFiles("sql");
    }
    if(directoryExists(\@sub_directories,"pld"))
    {
      $l_progress="070";
      processPLDFiles("pld");
    }
    if(directoryExists(\@sub_directories,"forms"))
    {
      $l_progress="080";
      processFormsFiles("forms");
    }
    if(directoryExists(\@sub_directories,"jsp"))
    {
      $l_progress="090";
      processHTMLFiles("jsp");
    }
	# HDS is disabling JLT deployments with this script
    # if(directoryExists(\@sub_directories,"jlt"))
    # {
      # $l_progress="100";
      # processJltFiles("jlt");
    # }  
    if(directoryExists(\@sub_directories,"ldt"))
    {
      $l_progress="110";
      processLdtFiles("ldt");
    }
    if(directoryExists(\@sub_directories,"xmlpub"))
    {
      $l_progress="120";
      processXMLPubFiles("xmlpub");
    }
    if(directoryExists(\@sub_directories,"reports"))
    {
      $l_progress="130";
      processReportFiles("reports");
    }
    if(directoryExists(\@sub_directories,"java"))
    {
      $l_progress="140";
      processJavaFiles("java");
    }
    if(directoryExists(\@sub_directories,"wft"))
    {
      $l_progress="145";
      processWFTFiles("wft");
    }
    if(directoryExists(\@sub_directories,"images"))
    {
      $l_progress ="147";
      processImageFiles("images");
    }
    if(directoryExists(\@sub_directories,"wfx"))
    {
      $l_progress ="187";
      processWFXFiles("wfx");
    }
    if(directoryExists(\@sub_directories,"prog"))
    {
      $l_progress ="188";
      processProgFiles("prog");
    }

    #Execute the post_process user hook if it is defined
    $l_progress="150";
    executeUserHook("post_process");
    $l_progress="160";
    $,="\n";
    $l_progress="170";
    printDebug($l_progress,$debugMod,"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
    printDebug($l_progress,$debugMod,"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
    printDebug($l_progress,$debugMod,"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
    $l_progress="180";
    $,="\n";
    open(SCRIPTS,"+>scripts.txt");
	## HDS Customization - Strip out passwords from the scripts file
	my $commandLineForScriptsFile = "";
	foreach (@commandsToExecute) {
		$commandLineForScriptsFile = $_;
		$commandLineForScriptsFile =~ s/($apps_pwd|$eis_pwd)/****/;
		print SCRIPTS $commandLineForScriptsFile;
	}
    close SCRIPTS;
    $l_progress="190";
    printDebug($l_progress,$debugMod,"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
    printDebug($l_progress,$debugMod,"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
    printDebug($l_progress,$debugMod,"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
    $l_progress="200";
    #chdir($startdir);
    $l_progress="210";
  }
  printDebug($l_progress,$debugMod,"End");
}
######################################################################################
# processProgFiles: copy the prog files to $XXJW_TOP/bin directory and create soft links
######################################################################################

sub processProgFiles($)
{
  my $l_progress="000";
  my $debugMod="processProgFiles";
  printDebug($l_progress,$debugMod,"Start");

  my $dir=$_[0];
  printDebug($l_progress,$debugMod," $dir :". $dir);
  $l_progress="010";
  my @progFiles=`ls $dir`;
  $,="\n";
  $l_progress="020";
  foreach $progFile (@progFiles) {
    $l_progress="030";
    chomp($progFile);
    $l_progress="040";
    printDebug($l_progress,$debugMod,"$dir/$progFile"."\n");
    if(-f "$dir/$progFile")
    {
      $l_progress="050";
      printDebug($l_progress,$debugMod,"cp $dir/$progFile $CUSTOM_TOP/bin\n");
      $l_progress="060";
      addCommandToStack("cp $dir/$progFile $CUSTOM_TOP/bin\n");
      printDebug($l_progress,$debugMod,"progFile : $progFile\n");
      $progFile =~ /^(.*)\.prog/;
      my $progfileWithoutExt=$1;
      printDebug($l_progress,$debugMod,"progfileWithoutExt ".$progfileWithoutExt."\n");
      printDebug($l_progress,$debugMod,"ln -sf $ENV{FND_TOP}/bin/fndcpesr $CUSTOM_TOP/bin/$progfileWithoutExt\n");
      addCommandToStack("ln -sf $ENV{FND_TOP}/bin/fndcpesr $CUSTOM_TOP/bin/$progfileWithoutExt\n");
      $l_progress="070";
    }
  }
}
######################################################################################
# processJavaFiles: print out the build.xml file to be passed onto ant for building.
######################################################################################

sub processJavaFiles($)
{
  my $l_progress="000";
  my $debugMod="processJavaFiles";
  printDebug($l_progress,$debugMod,"Start");

  my $dir=$_[0];
  printDebug($l_progress,$debugMod," $dir :". $dir);
  $l_progress="010";
  printAntBuildFile();

  $l_progress="020";
  printDebug($l_progress,$debugMod,"ant");
  $l_progress="030";
  addCommandToStack("ant\n");
  
  #HDS Customization, set group permissions to write for classes folder
  $l_progress="031";
  addCommandToStack("chmod -R g+w classes/ \n");
  
  $l_progress="050";
  addCommandToStack("cp -Rv classes/* $JAVA_DIR \n");
  $l_progress="060";
  processOAFiles($dir);
  $l_progress="070";
  printDebug($l_progress,$debugMod,"End");
}
######################################################################################
# processImageFiles: copies all files within to the directory to $OA_MEDIA directory
######################################################################################

sub processImageFiles($)
{
  my $l_progress="000";
  my $debugMod="processImageFiles";
  printDebug($l_progress,$debugMod,"Start");

  my $dir=$_[0];
  printDebug($l_progress,$debugMod," $dir :". $dir);
  $l_progress="010";

  my @imagefiles=`ls $dir`;
  $l_progress="020";

  foreach $imagefile (@imagefiles) {
    $l_progress="030";
    chomp($imagefile);
    $l_progress="040";
    if(-f "$dir/$imagefile")
    {
      $l_progress="050";
      printDebug($l_progress,$debugMod,"cp $dir/$imagefile $ENV{OA_MEDIA}");
      $l_progress="060";
      addCommandToStack("cp $dir/$imagefile $ENV{OA_MEDIA}\n");
      $l_progress="070";
    }
  }
  $l_progress="080";
  printDebug($l_progress,$debugMod,"End");
}

######################################################################################
# processHTMLFiles: copies all files within to the OA_HTML directory
######################################################################################

sub processHTMLFiles($)
{
  my $l_progress="000";
  my $debugMod="processHTMLFiles";
  printDebug($l_progress,$debugMod,"Start");

  my $dir=$_[0];
  printDebug($l_progress,$debugMod," $dir :". $dir);
  $l_progress="010";

  my @htmlfiles=`ls $dir`;
  $l_progress="020";
  
  foreach $htmlfile (@htmlfiles) {
    $l_progress="030";
    chomp($htmlfile);
    $l_progress="040";
    if(-f "$dir/$htmlfile")
    {
      $l_progress="050";
      printDebug($l_progress,$debugMod,"cp $dir/$htmlfile $COMMON_TOP/html");
      $l_progress="060";
      addCommandToStack("cp $dir/$htmlfile $COMMON_TOP/html\n");
      $l_progress="070";
    }
  }
  $l_progress="080";
  printDebug($l_progress,$debugMod,"End");
}
######################################################################################
# processWFXFiles: copies the wft file by uploading to the db
######################################################################################
sub processWFXFiles($)
{
  my $l_progress="000";
  my $debugMod="processWFXFiles";
  printDebug($l_progress,$debugMod,"Start");
  my $dir=$_[0];

  printDebug($l_progress,$debugMod," $dir :". $dir);
  $l_progress="010";
  my @wfxFiles =`ls $dir/*.wfx`;
  $l_progress="020";
  my @subscriptionFiles=();
  foreach $wfxfile (@wfxFiles) {

    $l_progress="030";
    chomp($wfxfile);
    my $fileType=checkXmlFileType($wfxfile,"wfx");
    printDebug($l_progress,$debugMod," fileType : $fileType\n");
    $l_progress="050";
    if($fileType eq "WF_EVENTS")
    {
      addCommandToStack("adjava oracle.apps.fnd.wf.WFXLoad -u apps $apps_pwd $db_server:$db_port:$db_sid thin US  $wfxfile\n");
      $l_progress="060";
      printDebug($l_progress,$debugMod,"adjava oracle.apps.fnd.wf.WFXLoad -u apps $apps_pwd $db_server:$db_port:$db_sid thin US  $wfxfile\n");
    }
    else
    {
      $l_progress="065";
      @subscriptionFiles=(@subscriptionFiles,$wfxfile);
      printDebug($l_progress,$debugMod,"Add event subscription file to stack for processign later $wfxfile\n");
    }
    $l_progress="070";
  }

  foreach $wfxfile (@subscriptionFiles) {
    $l_progress="080";
    chomp($wfxfile);
    addCommandToStack("adjava oracle.apps.fnd.wf.WFXLoad -u apps $apps_pwd $db_server:$db_port:$db_sid thin US  $wfxfile\n");
    $l_progress="090";
    printDebug($l_progress,$debugMod,"adjava oracle.apps.fnd.wf.WFXLoad -u apps $apps_pwd $db_server:$db_port:$db_sid thin US  $wfxfile\n");
    $l_progress="100";
  }

  printDebug($l_progress,$debugMod,"End");
}

######################################################################################
# processWFTFiles: copies the wft file by uploading to the db
######################################################################################
sub processWFTFiles($)
{
  my $l_progress="000";
  my $debugMod="processWFTFiles";
  printDebug($l_progress,$debugMod,"Start");
  my $dir=$_[0];

  printDebug($l_progress,$debugMod," $dir :". $dir);
  $l_progress="010";
  my @wftFiles =`ls $dir/*.wft`;
  $l_progress="020";

  foreach $wftfile (@wftFiles) {
    $l_progress="030";
    chomp($wftfile);
    $l_progress="050";
    addCommandToStack("WFLOAD apps/$apps_pwd 0 Y UPLOAD $wftfile\n");

    $l_progress="060";
    printDebug($l_progress,$debugMod,"WFLOAD 0 Y UPLOAD apps/**** $wftfile\n");
    $l_progress="070";
  }
  printDebug($l_progress,$debugMod,"End");
}
######################################################################################
# processFormsFiles: copies the form file to the custom top and builds the file from there
######################################################################################
sub processFormsFiles($)
{
  my $l_progress="000";
  my $debugMod="processFormsFiles";
  printDebug($l_progress,$debugMod,"Start");
  my $dir=$_[0];
  printDebug($l_progress,$debugMod," $dir :". $dir);
  $l_progress="010";
  my @formfiles =`ls $dir/*.fmb`;

  $l_progress="020";
  
  foreach $formfile (@formfiles) {
	# set environment vars the first time
	if ($l_progress eq "020") 
	{
		printDebug($l_progress,$debugMod,'export FORM_PATH=$AU_TOP/forms/US:$FORM_PATH:$AU_TOP/resource/US');
		addCommandToStack('export FORM_PATH=$AU_TOP/forms/US:$FORM_PATH:$AU_TOP/resource/US' . "\n");
		
		printDebug($l_progress,$debugMod,'export FORMS_PATH=$FORMS_PATH:$AU_TOP/forms/US:$AU_TOP/resource/US:S:$AU_TOP/resource');
		addCommandToStack('export FORMS_PATH=$FORMS_PATH:$AU_TOP/forms/US:$AU_TOP/resource/US:S:$AU_TOP/resource' . "\n");
	}
  
    $l_progress="030";
    chomp($formfile);
    $l_progress="040";
    printDebug($l_progress,$debugMod,"cp $formfile $CUSTOM_TOP/forms/US");
    $l_progress="050";
    addCommandToStack("cp $formfile $CUSTOM_TOP/forms/US\n");

    $l_progress="060";
    $formfile=~/\/(.*\.fmb)/;
    my $fileName=$1;
	
	$formfile=~/\/(.*)\.fmb/;
	my $fileBaseName=$1;

    printDebug($l_progress,$debugMod,"frmcmp_batch userid=$apps_user/**** module=$CUSTOM_TOP/forms/US/$fileName output_file=$CUSTOM_TOP/FORMS/US/$fileBaseName module_type=form compile_all=special");
    $l_progress="070";
    addCommandToStack("frmcmp_batch userid=$apps_user/$apps_pwd module=$CUSTOM_TOP/forms/US/$fileName output_file=$CUSTOM_TOP/forms/US/$fileBaseName.fmx module_type=form compile_all=special \n");
    $l_progress="080";

  }
  printDebug($l_progress,$debugMod,"End");
}

######################################################################################
# processPLDFiles: processes all pld files. It compiles the file and put its 
# into $AU_TOP/resource
######################################################################################

sub processPLDFiles($)
{
  my $l_progress="000";
  my $debugMod="processPLDFiles";
  printDebug($l_progress,$debugMod,"Start");
  my $dir=$_[0];
  printDebug($l_progress,$debugMod," $dir :". $dir);
  $l_progress="010";
  my @pldfiles =`ls $dir`;
  $l_progress="020";
  foreach $pldfile (@pldfiles) {
    $l_progress="030";
    chomp($pldfile);
    $l_progress="040";
    #system("cp pld/$pldfile $AU_TOP/resource\n");
    printDebug($l_progress,$debugMod,"cp $dir/$pldfile $AU_TOP/resource");
    $l_progress="050";
    addCommandToStack("cp $dir/$pldfile $AU_TOP/resource\n");
    $l_progress="060";
    #system("f60gen module=$AU_TOP/resource/$pldfile userid=$apps_user/$apps_pwd module_type=LIBRARY\n" );
	
	# Get PLL Base Name
	$pldfile=~/(.*)\.pll/;
	my $fileBaseName=$1;
	
	## Upgraded to R12 version of forms compile
    printDebug($l_progress,$debugMod,"frmcmp_batch module=$AU_TOP/resource/$pldfile userid=$apps_user/**** output_file=$AU_TOP/resource/$fileBaseName.plx module_type=LIBRARY batch=yes compile_all=special" );
    $l_progress="070";
    # old 11i command
	# addCommandToStack("f60gen module=$AU_TOP/resource/$pldfile userid=$apps_user/$apps_pwd module_type=LIBRARY\n");
	addCommandToStack("frmcmp_batch module=$AU_TOP/resource/$pldfile userid=$apps_user/$apps_pwd output_file=$AU_TOP/resource/$fileBaseName.plx module_type=LIBRARY batch=yes compile_all=special \n");
	
    $l_progress="080";
  }
  $l_progress="090";
  printDebug($l_progress,$debugMod,"End");
}

######################################################################################
# processSQLFiles: processes all sql files. Compiles them into the db directly.
######################################################################################

sub processSQLFiles($)
{
  my $l_progress="000";
  my $debugMod="processSQLFiles";
  printDebug($l_progress,$debugMod,"Start");
  my $dir=$_[0];
  printDebug($l_progress,$debugMod," $dir :". $dir);
  $l_progress="010";
  my @sqlfiles=`ls $dir`;
  $l_progress="020";
  foreach $sqlfile (@sqlfiles) {
    $l_progress="030";
    printDebug($l_progress,$debugMod,$sqlfile);
    $l_progress="040";
    chomp($sqlfile);
    if(-f "$dir/$sqlfile")
    {
      $l_progress="050";
      if("$dir/$sqlfile" !~ /recompile_objects\.sql/ && "$dir/$sqlfile" !~ /recompile\.sql/)
      {
        $l_progress="060";
        buildSqlCommand("$dir/$sqlfile");
        $l_progress="065";
      }
    }
  }
  $l_progress="070";
  printRecompileObjectsSql("$dir/recompile_objects.sql");
  $l_progress="080";
 
  buildSqlCommand("$dir/recompile_objects.sql");
  $l_progress="090";
  printDebug($l_progress,$debugMod,"End");
}

######################################################################################
# printAntBuildFile : builds the build.xml file
######################################################################################

sub printAntBuildFile()
{
  my $l_progress="000";
  my $debugMod="printAntBuildFile";
  printDebug($l_progress,$debugMod,"Start");
  open(OUTFILE,"+>build.xml") or die('File build.xml not opened $!');
  $l_progress="010";
  print OUTFILE  "<project name=\"compile\" default=\"compile\" basedir=\".\">\n";
  $l_progress="020";
  print OUTFILE  "    <description>\n";
  $l_progress="030";
  print OUTFILE  "         build file for compiling java code\n";
  $l_progress="040";
  print OUTFILE  "    </description>\n";
  $l_progress="050";
  print OUTFILE  "  <property name=\"src\" location=\"java\"/>\n";
  $l_progress="060";
  print OUTFILE  "  <property name=\"build\" location=\"classes\"/>\n";
  $l_progress="070";
  print OUTFILE  "  <target name=\"init\">\n";
  $l_progress="080";
  print OUTFILE  "    <tstamp/>\n";
  $l_progress="090";
  print OUTFILE  "    <delete dir=\"\${build}\"/>\n";
  $l_progress="100";
  print OUTFILE  "    <mkdir dir=\"\${build}\"/>\n";
  $l_progress="110";
  print OUTFILE  "  </target>\n";
  $l_progress="120";
  print OUTFILE  "  <target name=\"compile\" depends=\"init\"\n";
  $l_progress="130";
  print OUTFILE  "        description=\"compile the source \" >\n";
  $l_progress="140";
  print OUTFILE  "    <javac srcdir=\"\${src}\" destdir=\"\${build}\" listfiles=\"yes\" />\n";
  $l_progress="150";
  print OUTFILE  "  </target>\n";
  $l_progress="160";
  print OUTFILE  "</project>\n";
  $l_progress="170";
  close OUTFILE;
  $l_progress="180";
  printDebug($l_progress,$debugMod,"End");
}

######################################################################################
# buildSqlCommand : builds the sql command for each sql file
######################################################################################

sub buildSqlCommand($)
{
  my $l_progress="000";
  my $debugMod="buildSqlCommand";
  printDebug($l_progress,$debugMod,"Start");
  my $file=$_[0];
  $l_progress="010";
  #system("sqlplus $apps_user/$apps_pwd \@$file\n");

  
  ## HDS Customization
  # If the file contains the text "Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator."
  # it is an EIS installation file and must be run as the XXEIS user. The EIS password is the 4th parameter to the program.

  # Default these variables to be the APPS user and password
  my $sql_user = $apps_user;
  my $sql_pwd = $apps_pwd;
	
  my $eischeckline;
  my $eislinecount = 0;
  printDebug($l_progress,$debugMod,"Scanning $file to check if it is an EIS script.");
  # Open the file
  if (open(my $sqlfile, '<', $file)){
	# Read each line while there are lines left
	while ($eischeckline = <$sqlfile>) {
		# If we are past the first 15 lines, give up. This is for performance.
		if ($eislinecount >= 15) {
			last;
		}
		$eislinecount++;
		# See if the line has this phrase
		if ($eischeckline =~ m/Run this file in XXEIS user/) {
			# This is an EIS file!
			printDebug($l_progress,$debugMod,"File $file has been identified as an EIS deployment script.");
			# Since this is an EIS file we should use the EIS user and password.
			$sql_user = $eis_user;
			$sql_pwd = $eis_pwd;
			
			## We need to remove all carriage returns from EIS files before running them
			# Step one replaces all carriage returns (\r) with newlines. This will create some blank lines
			# in places where there was \r\n but it will fix cases where there was a single \r.
			printDebug($l_progress,$debugMod,"sed -i.bak 's/\\r/\\n/g' $file");
			$l_progress="015";
			addCommandToStack("sed -i 's/\\r/\\n/' $file\n");
			
			# Step two replaces all blank lines (\n\n) by using a global delete on any blank lines.
			printDebug($l_progress,$debugMod,'sed -i.bak \'/^$/d\' ' . $file);
			$l_progress="016";
			addCommandToStack('sed -i \'/^$/d\' ' . $file . "\n");
		
			last;	# Exit the while loop
		} 
	}
	close $sqlfile;		# Don't forget to close the file!
  } else {
	printDebug($l_progress,$debugMod,"Could not open file $file $!");
  }
  
  # HDS Customized to always add "set SQLBLANKLINES on" to every SQL file
  printDebug($l_progress,$debugMod,"sed -i '1s;^;set SQLBLANKLINES on;' $file");
  addCommandToStack("sed -i '1s;^;set SQLBLANKLINES on \\n;' $file\n");
  
  # HDS Customized to use the new $sql_user and $sql_pwd to support XXEIS deployments.
  printDebug($l_progress,$debugMod,"sqlplus $sql_user/**** \@$file");
  $l_progress="020";
  addCommandToStack("sqlplus $sql_user/$sql_pwd \@$file\n");
  $l_progress="030";
  printDebug($l_progress,$debugMod,"End");
}

######################################################################################
# processReportFiles: processes all reports files. 
# Basically copies them to the reports/US directory.
######################################################################################

sub processReportFiles($)
{
  my $l_progress="000";
  my $debugMod="processReportFiles";
  printDebug($l_progress,$debugMod,"Start");
  my $dir=$_[0];
  printDebug($l_progress,$debugMod," $dir :". $dir);
  $l_progress="010";
  my @reportFiles=`ls $dir/*`;
  $l_progress="020";
  foreach $reportFile (@reportFiles) {
    $l_progress="030";
    chomp($reportFile);
    $l_progress="040";
    #system("FNDLOAD $apps_user/$apps_pwd 0 Y UPLOAD $lctfilename $dir/$ldtfile\n");
#    system("cp $reportFile $CUSTOM_TOP/reports/US\n");
    printDebug($l_progress,$debugMod,"cp $reportFile $CUSTOM_TOP/reports/US");
    $l_progress="050";
    addCommandToStack("cp $reportFile $CUSTOM_TOP/reports/US\n");
    $l_progress="060";
    printDebug($l_progress,$debugMod,"\n");
    $l_progress="070";
  }
  $l_progress="080";
  printDebug($l_progress,$debugMod,"End");
}

######################################################################################
# processLdtFiles: processes ldt files. Looks 
# at the ldt and finds the corresponding
# lct file and generates the build command for it.
######################################################################################

sub processLdtFiles($)
{
  my $l_progress="000";
  my $debugMod="processLdtFiles";
  printDebug($l_progress,$debugMod,"Start");
  my $dir=$_[0];
  printDebug($l_progress,$debugMod," $dir :". $dir);
  $l_progress="010";
  my @ldtfiles=`ls $dir`;
  $l_progress="020";
  foreach $ldtfile (@ldtfiles) {
    $l_progress="030";
    chomp($ldtfile);
    $l_progress="040";
    my $lctfilename=getLctFileName($dir,$ldtfile);
    printDebug($l_progress,$debugMod," $lctfilename :". $lctfilename);
    $l_progress="050";
	
	## HDS Customization to check whether LDT is for Concurrent Programs and requires
	## CUSTOM_MODE = Force
	my $forceString = "";
	if ($lctfilename =~ /afcpprog/) {
		$forceString = "UPLOAD_MODE=REPLACE CUSTOM_MODE=FORCE";
	}
    #system("FNDLOAD $apps_user/$apps_pwd 0 Y UPLOAD $lctfilename $dir/$ldtfile\n");
    printDebug($l_progress,$debugMod,"FNDLOAD $apps_user/**** 0 Y UPLOAD $forceString $lctfilename $dir/$ldtfile");
    $l_progress="060";
    addCommandToStack("FNDLOAD $apps_user/$apps_pwd 0 Y UPLOAD $lctfilename $dir/$ldtfile $forceString \n");
    $l_progress="070";
    printDebug($l_progress,$debugMod,"\n");
    $l_progress="080";
  }
  $l_progress="090";
  printDebug($l_progress,$debugMod,"End");
}

######################################################################################
# processJltFiles: builds the command to install jlt files
######################################################################################

sub processJltFiles($)
{
  my $l_progress="000";
  my $debugMod="processJltFiles";
  printDebug($l_progress,$debugMod,"Start");
  my $dir=$_[0];
  printDebug($l_progress,$debugMod," $dir :". $dir);
  $l_progress="010";
  my @jltfiles=`ls $dir`;
  foreach $jltfile (@jltfiles) {
    $l_progress="020";
    chomp($jltfile);
    $l_progress="030";
    #system("java oracle.apps.ak.akload $apps_user $apps_pwd THIN \"(DESCRIPTION=(ADDRESS=(PROTOCOL=tcp)(HOST=$db_server) (PORT=$db_port))(CONNECT_DATA=(SID=$db_sid)))\" UPLOAD  $dir/$jltfile UPDATE AMERICAN_AMERICA.WE8ISO8859P1\n");
    printDebug($l_progress,$debugMod,"java oracle.apps.ak.akload $apps_user **** THIN \"(DESCRIPTION=(ADDRESS=(PROTOCOL=tcp)(HOST=$db_server) (PORT=$db_port))(CONNECT_DATA=(SID=$db_sid)))\" UPLOAD  $dir/$jltfile UPDATE AMERICAN_AMERICA.WE8ISO8859P1");
    $l_progress="040";
    addCommandToStack("java oracle.apps.ak.akload $apps_user $apps_pwd THIN \"(DESCRIPTION=(ADDRESS=(PROTOCOL=tcp)(HOST=$db_server) (PORT=$db_port))(CONNECT_DATA=(SID=$db_sid)))\" UPLOAD  $dir/$jltfile UPDATE AMERICAN_AMERICA.WE8ISO8859P1\n");
    $l_progress="050";
  }
  $l_progress="060";
  printDebug($l_progress,$debugMod,"End");
}

######################################################################################
# getLctFileName: Looks at the ldt file to 
# find the lct file name. This is used to build
# the command for the ldt file.
######################################################################################
sub getLctFileName($$)
{
  my $l_progress="000";
  my $debugMod="getLctFileName";
  printDebug($l_progress,$debugMod,"Start");
  $l_progress="010";
  my $directory=$_[0];
  $l_progress="020";
  my $ldtfile=$_[1];
  printDebug($l_progress,$debugMod," $ldtfile :". $ldtfile);
  $l_progress="030";
  open(LCTFILE,"$directory/$ldtfile")or (warn($!)&&die("Exiting"));
  $l_progress="040";
  while(<LCTFILE>)
  {
    $l_progress="050";
    $line=$_;
    $l_progress="060";
    if($line  =~  /\# dbdrv.*(\@.*\.lct).*$/i)
    {
      $l_progress="060";
      printDebug($l_progress,$debugMod,"$1");
      $l_progress="070";
      close LCTFILE;
      return $1;
    }
    $l_progress="080";
  }
  close LCTFILE;
  $l_progress="090";
  return "";
  printDebug($l_progress,$debugMod,"End");
}

######################################################################################
# directoryExists: Checks if a particular 
# installation directory exists in the folder
# if it does then the files are processed
######################################################################################

sub directoryExists($$)
{
  my $l_progress="000";
  my $debugMod="directoryExists";
  printDebug($l_progress,$debugMod,"Start");
  $l_progress="010";
  my $directorylist=$_[0];
  printDebug($l_progress,$debugMod," $directorylist :". $directorylist);
  my @directories=@$directorylist;
#  print @directories."\n";
  my $dirname =$_[1];
  printDebug($l_progress,$debugMod," $dirname  :". $dirname );
  $l_progress="020";
  foreach $dir (@directories) {
    $l_progress="030";
    chomp($dir);
    $l_progress="040";
    if(-d $dir && $dir eq $dirname)
    {
      $l_progress="050";
      printDebug($l_progress,$debugMod,"##########################################");
      printDebug($l_progress,$debugMod,"##############MATCHED $dir  ############## \n");
      printDebug($l_progress,$debugMod,"##########################################\n");
      $l_progress="060";
      return 1;
    }
  }
  $l_progress="070";
  printDebug($l_progress,$debugMod,"End");
  return 0;
}

######################################################################################
# checkXmlFileType: If the file type is not obvious
# from the naming convention open the xml file to
# figure out the file type.
######################################################################################

sub checkXmlFileType($$)
{
  my $l_progress="000";
  my $debugMod="checkXmlFileType";
  printDebug($l_progress,$debugMod,"Start");
  my $bifile=$_[0];
  printDebug($l_progress,$debugMod," $bifile :". $bifile);
  my $dir=$_[1];
  printDebug($l_progress,$debugMod," $dir :". $dir);
  my $l_progress="000";
  $l_progress="010";
  open(INFILE,"$bifile") or (warn($!)&&die("Exiting"));
  $l_progress="020";
  while(<INFILE>)
  {
    $l_progress="030";
    my $line=$_;
    printDebug($l_progress,$debugMod," $line :". $line);
    $l_progress="040";
#    print $line;
    if($line =~ /\<xapi:delivery\>/)
    {
      $l_progress="050";
      close INFILE;
      $l_progress="060";
      return "BURSTING_FILE";
    }
    elsif($line =~ /\<dataTemplate\s+name.*=\"/)
    {
      $l_progress="070";
      close INFILE;
      $l_progress="080";
      return "DATA_TEMPLATE";
    }
    elsif($line =~ /\<WF_EVENT_SUBSCRIPTIONS\>/)
    {
      $l_progress="090";
      close INFILE;
      $l_progress="100";
      return "WF_EVENT_SUBSCRIPTIONS";
    }
    elsif($line =~ /\<WF_EVENTS\>/)
    {
      $l_progress="110";
      close INFILE;
      $l_progress="120";
      return "WF_EVENTS";
    }
    $l_progress="090";
  }
  $l_progress="100";
  printDebug($l_progress,$debugMod,"End");
}
  ;
######################################################################################
# checkFileType: Look at the xml publisher file
# to figure out if it is an RTF template/bursting file
# etc.
######################################################################################

sub checkFileType($$)
{
  my $l_progress="000";
  my $debugMod="checkFileType";
  printDebug($l_progress,$debugMod,"Start");
  my $bifile=$_[0];
  printDebug($l_progress,$debugMod," $bifile :". $bifile);
  $l_progress="010";
  my @file_details=();
  $l_progress="020";
  my $dir=$_[1];
  printDebug($l_progress,$debugMod," $dir :". $dir);
  $l_progress="030";
  if($bifile =~ /BURSTING_FILE_([A-Z]+)?_(.*)\.xml/)
  {
    $l_progress="040";
    @file_details=($1,$2,"BURSTING_FILE");
    $l_progress="050";
  }
  elsif($bifile =~ /DATA_TEMPLATE_([A-Z]+)?_(.*)\.xml/)
  {
    $l_progress="060";
    @file_details=($1,$2,"DATA_TEMPLATE");
    $l_progress="070";
  }
  elsif($bifile =~ /\.xml/)
  {
    $l_progress="080";
    printDebug($l_progress,$debugMod,$bifile."Does not conform to naming standards");
    $l_progress="090";
    $lob_type=checkXmlFileType($bifile,$dir);
    $l_progress="100";
    return $lob_type;
  }
  elsif($bifile =~ /TEMPLATE_SOURCE_([A-Z]+)?_(.*)_([a-z][a-z]).*\.rtf/)
  {
    $l_progress="110";
    $lob_type="TEMPLATE_SOURCE";
    $l_progress="120";
    @file_details=($1,$2,"TEMPLATE_SOURCE");
    $l_progress="130";
  }
  $l_progress="150";
  printDebug($l_progress,$debugMod,"End");
  return \@file_details;
};

######################################################################################
# processXMLPubFiles: Code to process xml publisher files.
# Find the file type file lob type lob code etc and build the
# command.
######################################################################################


sub processXMLPubFiles($)
{
  my $l_progress="000";
  my $debugMod="processXMLPubFiles";
  printDebug($l_progress,$debugMod,"Start");
  my $cmd="";
  my $dir=$_[0];
  printDebug($l_progress,$debugMod," $dir :". $dir);
  $l_progress="010";
  my @files=<$dir/*>;
  $l_progress="020";
  my $lob_type="";
  my $lob_code="";
  my %filetypes=("BURSTING_FILE"=>"XML-BURSTING-FILE","DATA_TEMPLATE"=>"XML-DATA-TEMPLATE","TEMPLATE_SOURCE"=>"RTF");
  $l_progress="030";
  my $i=0;

  foreach $bifile (@files) {
    $l_progress="040";
    printDebug($l_progress,$debugMod,"XMLPUBLISHER FILE FOUND ".$bifile);
    $l_progress="050";
    chomp($bifile);
    $l_progress="060";
    my $file_details_ref=checkFileType($bifile,$dir);
    printDebug($l_progress,$debugMod," $file_details_ref :". $file_details_ref);
    $l_progress="070";
    my @file_details=@$file_details_ref;
    $l_progress="080";
    my $file_type=$file_details[2];
    printDebug($l_progress,$debugMod," $file_type :". $file_type);
    $l_progress="090";
    my $lob_code=$file_details[1];
    printDebug($l_progress,$debugMod," $lob_code :". $lob_code);
    $l_progress="100";
    my $app_short_code=$file_details[0];
    printDebug($l_progress,$debugMod," $app_short_code :". $app_short_code);
    $l_progress="110";
    my $xdo_file_type=$filetypes{$file_type};
    printDebug($l_progress,$debugMod," $xdo_file_type :". $xdo_file_type);
    $l_progress="120";
    printDebug($l_progress,$debugMod,"############## file_type is $file_type" ."\n");
    printDebug($l_progress,$debugMod,"############## xdo_file_type is $xdo_file_type" ."\n");
    printDebug($l_progress,$debugMod,"############## lob_code is $lob_code" ."\n");
    printDebug($l_progress,$debugMod,"############## app_short_code is $app_short_code" ."\n");
    printDebug($l_progress,$debugMod,"\n");
    $l_progress="130";
    if(defined($xdo_file_type))
    {
      $l_progress="140";
      $cmd= "java oracle.apps.xdo.oa.util.XDOLoader UPLOAD \\  -DB_USERNAME $apps_user \\  -DB_PASSWORD $apps_pwd \\  -JDBC_CONNECTION $db_server:$db_port:$db_sid \\  -LOB_TYPE $file_type \\  -APPS_SHORT_NAME $app_short_code \\  -LOB_CODE $lob_code \\  -LANGUAGE en \\  -TERRITORY 00 \\  -NLS_LANG American_America\.WE8ISO8859P1 \\  -XDO_FILE_TYPE $xdo_file_type \\  -FILE_CONTENT_TYPE \'text\/html\' \\  -CUSTOM_MODE FORCE \\  -FILE_NAME \.\/$bifile";
      my $debugcmd= "java oracle.apps.xdo.oa.util.XDOLoader UPLOAD \\  -DB_USERNAME $apps_user \\  -DB_PASSWORD **** \\  -JDBC_CONNECTION $db_server:$db_port:$db_sid \\  -LOB_TYPE $file_type \\  -APPS_SHORT_NAME $app_short_code \\  -LOB_CODE $lob_code \\  -LANGUAGE en \\  -TERRITORY 00 \\  -NLS_LANG American_America\.WE8ISO8859P1 \\  -XDO_FILE_TYPE $xdo_file_type \\  -FILE_CONTENT_TYPE \'text\/html\' \\  -CUSTOM_MODE FORCE \\  -FILE_NAME \.\/$bifile";
      $l_progress="150";
      #system($cmd);
      printDebug($l_progress,$debugMod,$debugcmd."\n");
      $l_progress="160";
      #@commandsToExecute=(@commandsToExecute,$cmd."\n");
      addCommandToStack($cmd);
      $l_progress="170";
    }
    $l_progress="180";
    $i=$i+1;
  }
  $l_progress="190";
  printDebug($l_progress,$debugMod,"End");
}
######################################################################################
# initsystemVariables: Look at the file install.txt
# to initialize the variables / parameters that are 
# important for the installation process.
######################################################################################

sub initsystemVariables($)
{
  my $l_progress="000";
  my $debugMod="initsystemVariables";
  printDebug($l_progress,$debugMod,"Start");
  my $install_file=$_[0];
  printDebug($l_progress,$debugMod," $install_file :". $install_file);
  if(!defined($install_file))
  {
    $install_file="$INSTALL_DIR/install.txt";
  }
#  print $install_file;
  open(INSTALLPARAMS,$install_file)||warn("Unable to open install file $install_file\n");
  $l_progress="010";
  my $line;
  printDebug($l_progress,$debugMod,"#################################################################################################");
  printDebug($l_progress,$debugMod,"INSTALL PARAMETERS DEFINED  in $INSTALL_DIR/install.txt");
  printDebug($l_progress,$debugMod,"#################################################################################################");
  $l_progress="020";
  while(<INSTALLPARAMS>)
  {
    $l_progress="030";
    $line=$_;
    $l_progress="040";
    chomp($line);
    $l_progress="050";
    if($line =~ /db_sid.*=(.*)$/)
    {
      $l_progress="120";
      $db_sid=$1;
      printDebug($l_progress,$debugMod,$db_sid);
      $l_progress="130";
    }
    if($line =~ /db_port.*=(.*)$/)
    {
      $l_progress="140";
      $db_port=$1;
      printDebug($l_progress,$debugMod,$db_port);
      $l_progress="150";
    }
    if($line =~ /db_server.*=(.*)$/)
    {
      $l_progress="160";
      $db_server=$1;
      printDebug($l_progress,$debugMod,$db_server);
      $l_progress="170";
    }
   if($line =~ /custom_top.*=(.*)$/)
    {
      $l_progress="180";
      $CUSTOM_TOP=$1;
      printDebug($l_progress,$debugMod, $CUSTOM_TOP);
      $l_progress="190";
    }
   }
  $l_progress="260";
  printDebug ($l_progress,$debugMod,"#################################################################################################\n");
  $l_progress="270";
  close INSTALLPARAMS;
  $l_progress="280";
  chomp($apps_pwd);
  chomp($apps_user);
  chomp($db_server);
  chomp($db_port);
  chomp($db_sid);
  $l_progress="290";
  printDebug($l_progress,$debugMod,"End");
}

######################################################################################
# addCommandToStack: Adds a command to the stack that holds all the commands.
######################################################################################

sub addCommandToStack($)
{
  my $l_progress="000";
  my $debugMod="addCommandToStack";
  printDebug($l_progress,$debugMod,"Start");
  my $cmd=$_[0];
  $l_progress="010";
  @commandsToExecute=(@commandsToExecute,$cmd);
  $l_progress="030";
  printDebug($l_progress,$debugMod,"End");
}

######################################################################################
# executeUserHook: Executes the commands in the user hook file. This can be done as
# pre-processing and post-processing.
######################################################################################

sub executeUserHook($)
{
  my $l_progress="000";
  my $debugMod="executeUserHook";
  printDebug($l_progress,$debugMod,"Start");
  my $stage=$_[0];
  $l_progress="010";
  my $fileToRead="";
  if($stage eq "pre_process")
  {
    $l_progress="020";
    $fileToRead=$pre_process_file;
    $l_progress="030";
  }
  else
  {
    $l_progress="040";
    $fileToRead=$post_process_file;
    $l_progress="050";
  }
  $l_progress="060";
  printDebug($l_progress,$debugMod,"Executing user hook ".$fileToRead);
  $l_progress="060";
  open (HOOKFILE,$fileToRead)||(warn("$stage user hook not defined: $fileToRead $!") );
  $l_progress="070";
  while(<HOOKFILE>)
  {
    $l_progress="080";
    my $cmd=$_;
    printDebug($l_progress,$debugMod," $cmd :". $cmd);
    $l_progress="090";
    chomp($cmd);
    $l_progress="095";
    addCommandToStack($cmd."\n");
    $l_progress="100";
  }
  $l_progress="110";
  close HOOKFILE;
  $l_progress="120";
  printDebug($l_progress,$debugMod,"End");
}

sub printDebug($$$)
{
  if($debugOn==1)
  {
    my $debugStmt=$_[2];
    my $debugMod=$_[1];
    my $debugProgress=$_[0];
    print DEBUG_FILE $debugMod." : ".$debugProgress." : ".$debugStmt."\n";
  }
}

sub printRecompileObjectsSql($)
{
  my $l_progress="000";
  my $debugMod="printRecompileObjectsSql";
  printDebug($l_progress,$debugMod,"Start");
  my $filename=$_[0];
  printDebug($l_progress,$debugMod," $filename :". $filename);
  $l_progress="010";
  printDebug($l_progress,$debugMod,`pwd`);
  open(RECOMPILEOBJECTS,"+>$filename")or die("Unable to open file $! $filename");
  
  ## HDS Customizations to exclude specific kinds of files which will never compile
  print RECOMPILEOBJECTS "SET HEAD OFF;\n";
  print RECOMPILEOBJECTS "SET FEED OFF;\n";
  print RECOMPILEOBJECTS "SET PAGES 9999;\n";
  print RECOMPILEOBJECTS "SPOOL sql/recompile.sql\n\n";
  print RECOMPILEOBJECTS "SELECT \'ALTER PACKAGE \' || OWNER || \'.\' || OBJECT_NAME || \' COMPILE;\'\n";
  print RECOMPILEOBJECTS "FROM   ALL_OBJECTS\n";
  print RECOMPILEOBJECTS "WHERE  STATUS = \'INVALID\'\n";
  print RECOMPILEOBJECTS "AND  OBJECT_NAME NOT LIKE \'AP_%\'\n";
  print RECOMPILEOBJECTS "AND  OBJECT_NAME NOT LIKE \'CSD_%\'\n";
  print RECOMPILEOBJECTS "AND  OBJECT_NAME NOT LIKE \'FV_%\'\n";
  print RECOMPILEOBJECTS "AND  OBJECT_NAME NOT LIKE \'MSC_%\'\n";
  print RECOMPILEOBJECTS "AND  OBJECT_NAME NOT LIKE \'XLA_%\'\n";
  print RECOMPILEOBJECTS "and owner in (\'APPS\', \'APPS_MRC\', \'XXWC\', \'XXCUS\', '\XXEIS\')\n";
  print RECOMPILEOBJECTS "AND    OBJECT_TYPE = \'PACKAGE\';\n\n";
  
  print RECOMPILEOBJECTS "SELECT \'ALTER PACKAGE \' || OWNER || \'.\' || OBJECT_NAME || \' COMPILE BODY;\'\n";
  print RECOMPILEOBJECTS "FROM   ALL_OBJECTS\n";
  print RECOMPILEOBJECTS "WHERE  STATUS = \'INVALID\'\n";
  print RECOMPILEOBJECTS "AND  OBJECT_NAME NOT LIKE \'AP_%\'\n";
  print RECOMPILEOBJECTS "AND  OBJECT_NAME NOT LIKE \'CSD_%\'\n";
  print RECOMPILEOBJECTS "AND  OBJECT_NAME NOT LIKE \'FV_%\'\n";
  print RECOMPILEOBJECTS "AND  OBJECT_NAME NOT LIKE \'MSC_%\'\n";
  print RECOMPILEOBJECTS "AND  OBJECT_NAME NOT LIKE \'XLA_%\'\n";
  print RECOMPILEOBJECTS "and owner in (\'APPS\', \'APPS_MRC\', \'XXWC\', \'XXCUS\', '\XXEIS\')\n";
  print RECOMPILEOBJECTS "AND    OBJECT_TYPE = \'PACKAGE BODY\';\n\n";
  
  print RECOMPILEOBJECTS "SELECT \'ALTER VIEW \' || OWNER || \'.\' || OBJECT_NAME || \' COMPILE;\'\n";
  print RECOMPILEOBJECTS "FROM   ALL_OBJECTS\n";
  print RECOMPILEOBJECTS "WHERE  STATUS = \'INVALID\'\n";
  print RECOMPILEOBJECTS "AND  OBJECT_NAME NOT LIKE \'AP_%\'\n";
  print RECOMPILEOBJECTS "AND  OBJECT_NAME NOT LIKE \'CSD_%\'\n";
  print RECOMPILEOBJECTS "AND  OBJECT_NAME NOT LIKE \'FV_%\'\n";
  print RECOMPILEOBJECTS "AND  OBJECT_NAME NOT LIKE \'MSC_%\'\n";
  print RECOMPILEOBJECTS "AND  OBJECT_NAME NOT LIKE \'XLA_%\'\n";
  print RECOMPILEOBJECTS "and owner in (\'APPS\', \'APPS_MRC\', \'XXWC\', \'XXCUS\', '\XXEIS\')\n";
  print RECOMPILEOBJECTS "AND    OBJECT_TYPE = \'VIEW\';\n\n";
  print RECOMPILEOBJECTS "SPOOL OFF\n";
  print RECOMPILEOBJECTS "SET FEED ON;\n";
  print RECOMPILEOBJECTS "/*\n";
  print RECOMPILEOBJECTS "** Run the sql statements you just generated\n";
  print RECOMPILEOBJECTS "*/\n\n";
  print RECOMPILEOBJECTS "\@sql/recompile.sql\n\n";
  print RECOMPILEOBJECTS "SELECT OBJECT_NAME, OBJECT_TYPE FROM USER_OBJECTS \n";
  print RECOMPILEOBJECTS "WHERE STATUS = \'INVALID\' ;\n";
  print RECOMPILEOBJECTS "--AND OBJECT_NAME LIKE \'RCV%\' ;\n";
  print RECOMPILEOBJECTS "EXIT;\n";
  close RECOMPILEOBJECTS;
  $l_progress="020";
  printDebug($l_progress,$debugMod,"End");
}
sub verifyParams($)
{
  $,="\n";
  my $paramsString=$_[0];
  my @params=@$paramsString;
  my $paramsLength=@params;
  if($paramsLength<2)
  {
    printUsage();
    exit;
  }
  return 1;
}
sub printUsage()
{
  print "Usage: perl Install.pl <apps_pwd> <install_dir> <debug=[true|false]>\n";
}
######################################################################################
# processOAFiles: Starting point for processing all OA Files. i.e VO/EO.xml / PG/RN.xml
# jpx files etc.
######################################################################################

sub processOAFiles($)
{
  my $l_progress="000";
  my $debugMod="processOAFiles";
  printDebug($l_progress,$debugMod,"Start");
  my $cmd="";
  my $dir=$_[0];
  printDebug($l_progress,$debugMod," $dir :". $dir);
  $l_progress="010";
  my $pathLength=length($dir);
  my $varLength=0;
  my @files=getFileList($dir);
  $l_progress="020";
  my @serverSideFiles=();
  my @javaFiles=();
  my @clientSideFiles=();
  my @jpxFiles=();
  foreach $file (@files) {
    $varLength=length($file);
    $l_progress="025";
	printDebug($l_progress,$debugMod,"file : $file");
    printDebug($l_progress,$debugMod,"varLength : $varLength");
    my $str=substr($file,$pathLength,$varLength);
    $str =~ /^(.*)\/.*$/;
	printDebug($l_progress,$debugMod,"str : $str");
    my $baseDir=$1;
	printDebug($l_progress,$debugMod,"baseDir : $baseDir");

    if(! -d "$JAVA_DIR$baseDir")
    {
      addCommandToStack("mkdir -p $JAVA_DIR$baseDir \n");
    }
    $l_progress="030";
    if($file =~ /^.*server.*\.xml/ && $file !~/^.*server\.xml/)
    {
      $l_progress="040";
      @serverSideFiles=(@serverSideFiles,$file);
      #print "Server Side ".$file."\n";
      $l_progress="045";
    }
    elsif ($file =~ /^.*webui.*\/.*\.xml/ && $file !~/^.*webui\.xml/)
    {
      $l_progress="050";
      @clientSideFiles=(@clientSideFiles,$file);
      #print "Client Side ".$file."\n";
      $l_progress="055";
    }
    elsif ($file =~ /^.*\.java/)
    {
      $l_progress="060";
      @javaFiles=(@javaFiles,$file);
      #print "Java ".$file."\n";
      $l_progress="065";
    }
    elsif ($file =~ /^.*\.jpx/)
    {
      $l_progress="070";
      @jpxFiles=(@jpxFiles,$file);
      #print "Jpx ".$file."\n";
      $l_progress="075";
    }

    $l_progress="080";
  }
  processOAServerFiles(\@serverSideFiles,$dir);
  processOAWebuiFiles(\@clientSideFiles,$dir);
  processJpxFiles(\@jpxFiles,$dir);
  $l_progress="090";
  printDebug($l_progress,$debugMod,"End");
}
# Accepts one argument: the full path to a directory.
# Returns: A list of files that reside in that path.
######################################################################################
# getFileList: Gets the list of files under a given directory.
######################################################################################

sub getFileList ($)
{
    my $l_progress="000";
    my $debugMod="getFileList";
    printDebug($l_progress,$debugMod,"Start");

    my $path = shift;
    $l_progress="010";
    printDebug($l_progress,$debugMod,"path : $path");
    #print $path,"\n";
  #<STDIN>;
    opendir (DIR, $path)
        or die "Unable to open $path: $!";
    # We are just chaining the grep and map from
    # the previous example.
    # You'll see this often, so pay attention ;)
    # This is the same as:
    # LIST = map(EXP, grep(EXP, readdir()))
    $l_progress="020";
    printDebug($l_progress,$debugMod,$l_progress);

    my @files =
        # Third: Prepend the full path
        map { $path . '/' . $_ }
        # Second: take out '.' and '..'
        grep { !/^\.{1,2}$/ }
        # First: get all files
        readdir (DIR);
    $l_progress="030";
    printDebug($l_progress,$debugMod,$l_progress);
    my @files2 =();
    closedir (DIR);
    $,="\n";
    #print @files;

    for (@files) {
      $l_progress="040";
      printDebug($l_progress,$debugMod,$l_progress);

      if (-d $_) {
        if($_ =~ /((\.)|(\.\.))/)
        {
          $l_progress="050";
          printDebug($l_progress,$debugMod,$l_progress);
          # Add all of the new files from this directory
          # (and its subdirectories, and so on... if any)
        }
        else
        {
          $l_progress="060";
          printDebug($l_progress,$debugMod,$l_progress);
          #print "%%%%%%%%%%%%%%%%% $_ %%%%%%%%%%%%%%%%\n";
          push @files2, getFileList ($_);
        }
        $l_progress="070";
        printDebug($l_progress,$debugMod,$l_progress);
      } else {
        # Do whatever you want here =) .. if anything.
        $l_progress="080";
        printDebug($l_progress,$debugMod,$l_progress);
      }
      $l_progress="090";
      printDebug($l_progress,$debugMod,$l_progress);
    }
    # NOTE: we're returning the list of files
  #print "\nexit for $path \n";
  $l_progress="100";
  printDebug($l_progress,$debugMod,$l_progress);

  @files=(@files,@files2);
  $l_progress="110";
  printDebug($l_progress,$debugMod,$l_progress);

  return @files;
}

######################################################################################
# processOAServerFiles: Deals with all .xml files under the server directory
######################################################################################

sub processOAServerFiles($$)
{
  my $l_progress="000";
  my $debugMod="processOAServerFiles";
  printDebug($l_progress,$debugMod,"Start");
  my $pathLength=length($_[1]);
  $l_progress="010";
  printDebug($l_progress,$debugMod,"pathLength : $pathLength");
  my $fileListPtr=$_[0];
  my @fileList=@$fileListPtr;
  foreach $file (@fileList) 
  {
    $l_progress="020";
    printDebug($l_progress,$debugMod,"l_progress : $l_progress");
    $varLength=length($file);
    $l_progress="030";
    printDebug($l_progress,$debugMod,"varLength : $varLength");

    my $str=substr($file,$pathLength,$varLength);
    $l_progress="040";
    
    printDebug($l_progress,$debugMod,"str : $str");
    printDebug($l_progress,$debugMod,"file : $file");

    printDebug($l_progress,$debugMod,"cp -v $file $JAVA_DIR$str");
    addCommandToStack("cp -v $file $JAVA_DIR$str \n");
    $l_progress="050";
  }
  $l_progress="060";
  printDebug($l_progress,$debugMod,"End");
}
######################################################################################
# processOAWebuiFiles: Deals with all .xml files under the webui directory. i.e all PG/RN.xml's
######################################################################################


sub processOAWebuiFiles($$)
{
  my $l_progress="000";
  my $debugMod="processOAWebuiFiles";
  printDebug($l_progress,$debugMod,"Start");
  my $dir=$_[1];
  my $pathLength=length($dir);
  $l_progress="010";
  printDebug($l_progress,$debugMod,"pathLength : $pathLength");

  my $fileListPtr=$_[0];
  my @fileList=@$fileListPtr;
  foreach $file (@fileList) 
  {
    $l_progress="020";
    printDebug($l_progress,$debugMod,"l_progress : $l_progress");

    $varLength=length($file);
    $l_progress="030";
    printDebug($l_progress,$debugMod,"varLength : $varLength");

    my $str=substr($file,$pathLength,$varLength);
    $l_progress="040";
    printDebug($l_progress,$debugMod,"str : $str");
    printDebug($l_progress,$debugMod,"file : $file");
    printDebug($l_progress,$debugMod,"cp -v $file $JAVA_DIR$str");
    addCommandToStack("cp -v $file $JAVA_DIR$str \n");
    printDebug($l_progress,$debugMod,"java oracle.jrad.tools.xml.importer.XMLImporter $file -rootdir $dir -username $apps_user -password **** -dbconnection \"(DESCRIPTION =(ADDRESS_LIST =(ADDRESS = (PROTOCOL = TCP)(HOST = $db_server )(PORT =$db_port)))(CONNECT_DATA =(SERVICE_NAME = $db_sid)))\"");
    $l_progress="050";
    addCommandToStack("java oracle.jrad.tools.xml.importer.XMLImporter $file -rootdir $dir  -username $apps_user -password $apps_pwd -dbconnection \"(DESCRIPTION =(ADDRESS_LIST =(ADDRESS = (PROTOCOL = TCP)(HOST = $db_server )(PORT =$db_port)))(CONNECT_DATA =(SERVICE_NAME = $db_sid)))\" \n");
  }
  $l_progress="060";
  printDebug($l_progress,$debugMod,"End");
}
######################################################################################
# processJpxFiles: Deals with uploading all jpx files.
######################################################################################

sub processJpxFiles($$)
{
  my $l_progress="000";
  my $debugMod="processJpxFiles";
  printDebug($l_progress,$debugMod,"Start");

  my $pathLength=length($_[1]);
  $l_progress="010";
  printDebug($l_progress,$debugMod,"pathLength : $pathLength");

  my $fileListPtr=$_[0];
  my @fileList=@$fileListPtr;
  foreach $file (@fileList) 
  {
    $l_progress="020";
    printDebug($l_progress,$debugMod,"l_progress : $l_progress");

    $varLength=length($file);
    $l_progress="030";
    printDebug($l_progress,$debugMod,"varLength : $varLength");

    my $str=substr($file,$pathLength,$varLength);
    $l_progress="040";
    printDebug($l_progress,$debugMod,"str : $str");
    printDebug($l_progress,$debugMod,"file : $file");

    printDebug($l_progress,$debugMod,"java oracle.jrad.tools.xml.importer.JPXImporter $file  -username $apps_user -password $apps_pwd -dbconnection \"(DESCRIPTION =(ADDRESS_LIST =(ADDRESS = (PROTOCOL = TCP)(HOST = $db_server )(PORT =$db_port)))(CONNECT_DATA =(SERVICE_NAME = $db_sid)))\"");
    $l_progress="050";
    addCommandToStack("java oracle.jrad.tools.xml.importer.JPXImporter $file  -username $apps_user -password $apps_pwd -dbconnection \"(DESCRIPTION =(ADDRESS_LIST =(ADDRESS = (PROTOCOL = TCP)(HOST = $db_server )(PORT =$db_port)))(CONNECT_DATA =(SERVICE_NAME = $db_sid)))\" \n");
  }
  $l_progress="060";
  printDebug($l_progress,$debugMod,"End");
}