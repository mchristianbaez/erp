CREATE OR REPLACE PACKAGE BODY apps.xxwc_sales_order_acknowlg_file
IS
/**********************************************************************************************************
    $Header xxwc_sales_order_acknowlg_file.pkb $
    Module Name: xxwc_sales_order_acknowlg_file
    PURPOSE: TO send PO ack
    REVISIONS:
    Ver    Date         Author                Description
    ------ -----------  ------------------    ----------------
	1.0    13-Mar-2013                        Generated 3/13/2013 5:04:55 PM from APPS@EBIZPRD
    2.0    29-Nov-2016  Rakesh Patel          TMS# 20161122-00021 - MEP PO acknowledgment (Replace ampersand sign with AND in the file name)
***********************************************************************************************************/
   
    l_req_id         NUMBER := fnd_global.conc_request_id;
    l_run_from_uc4   VARCHAR2 (1) := 'N';

    FUNCTION check_type (p_in_string VARCHAR2, p_type VARCHAR2)
        RETURN BOOLEAN;

    PROCEDURE create_file (p_errbuf                       OUT NOCOPY VARCHAR2
                          ,p_retcode                      OUT NOCOPY VARCHAR2
                          ,p_customer_account_number   IN            VARCHAR2
                          ,pv_start_date               IN            VARCHAR2
                          ,pv_end_date                 IN            VARCHAR2
                          ,p_order_number              IN            VARCHAR2
                          ,p_send_file_by_email        IN            VARCHAR2
                          ,p_recipient_email           IN            VARCHAR2
                          ,p_run_from_uc4              IN            VARCHAR2 := 'N')
    IS
        v_file_name                  VARCHAR2 (124);
        n_loop_counter               NUMBER := 0;
        v_run_id                     NUMBER := xxwc.xxwc_sales_order_mmsi_line_s.NEXTVAL;

        v_sender_address             VARCHAR2 (128);
        v_subject                    VARCHAR2 (1024);
        v_recipient_email            VARCHAR2 (128);
        v_error_message              CLOB;
        v_err_code                   VARCHAR2 (12) := '0';

        p_start_date                 DATE := fnd_date.canonical_to_date (pv_start_date);
        p_end_date                   DATE := fnd_date.canonical_to_date (pv_end_date);
        vr_directory_path            VARCHAR2 (228);
        vr_arch_directory_path       VARCHAR2 (228);
        v_mmsi_directory_not_found   EXCEPTION;
        e_order_number_not_number    EXCEPTION;
    BEGIN
        l_run_from_uc4 := p_run_from_uc4;

        FOR nm IN (SELECT directory_path
                     FROM all_directories
                    WHERE directory_name = 'XXWC_MMSI')
        LOOP
            vr_directory_path := nm.directory_path;
        END LOOP;

        IF vr_directory_path IS NULL
        THEN
            RAISE v_mmsi_directory_not_found;
        END IF;

        FOR nm IN (SELECT directory_path
                     FROM all_directories
                    WHERE directory_name = 'XXWC_MMSI_ARCH')
        LOOP
            vr_arch_directory_path := nm.directory_path;
        END LOOP;

        IF vr_directory_path IS NULL
        THEN
            RAISE v_mmsi_directory_not_found;
        END IF;

        SELECT NVL (TO_DATE (pv_start_date, 'YYYY-MM-DD HH24:MI:SS'), TRUNC (SYSDATE - 1)) INTO p_start_date FROM DUAL;

        SELECT NVL (TRUNC (TO_DATE (pv_end_date, 'YYYY-MM-DD HH24:MI:SS')), TRUNC (SYSDATE)) INTO p_end_date FROM DUAL;

        EXECUTE IMMEDIATE 'truncate table xxwc.email_attachment_clobs';

        xxwc_log_debug ('p_customer_account_number=' || p_customer_account_number);
        xxwc_log_debug ('p_start_date=' || p_start_date);
        xxwc_log_debug ('p_end_date =' || p_end_date);
        xxwc_log_debug ('pv_start_date=' || pv_start_date);
        xxwc_log_debug ('pv_end_date =' || pv_end_date);
        xxwc_log_debug ('p_order_number  =' || p_order_number);
        xxwc_log_debug ('p_send_file_by_email =' || p_send_file_by_email);
        xxwc_log_debug ('p_recipient_email =' || p_recipient_email);

        p_errbuf := v_error_message;
        p_retcode := v_err_code;

        IF p_order_number IS NULL
        THEN
            ---**************************************************************
            --populate file based on customer account
            ---**************************************************************
            xxwc_log_debug ('p_customer_account_number=' || p_customer_account_number);

            FOR r0
                IN (SELECT a.party_name
                          ,a.account_number
                          ,a.party_number
                          ,a.status
                          ,a.org_id
                          ,a.cust_account_id
                      FROM xxwc_customer_acct_mnt a
                     WHERE     a.account_number IN
                                   (SELECT flex_value
                                      -- ,flex_value_meaning
                                      -- ,description
                                      -- ,enabled_flag
                                      --,start_date_active
                                      -- ,end_date_active

                                      FROM fnd_flex_values_vl
                                     WHERE     (   ('' IS NULL)
                                                OR (structured_hierarchy_level IN
                                                        (SELECT hierarchy_id
                                                           FROM fnd_flex_hierarchies_vl h
                                                          WHERE     h.flex_value_set_id =
                                                                        (SELECT flex_value_set_id
                                                                           FROM fnd_flex_value_sets
                                                                          WHERE flex_value_set_name =
                                                                                    'XXWC_MMSI_accounts')
                                                                AND h.hierarchy_name LIKE '')))
                                           AND flex_value_set_id = (SELECT flex_value_set_id
                                                                      FROM fnd_flex_value_sets
                                                                     WHERE flex_value_set_name = 'XXWC_MMSI_accounts')
                                           AND enabled_flag = 'Y'
                                           AND NVL (end_date_active, SYSDATE + 1 / 24) > SYSDATE)
                           AND a.account_number = NVL (p_customer_account_number, a.account_number))
            LOOP
                create_file_for_account (r0.account_number
                                        ,vr_directory_path
                                        ,p_start_date
                                        ,p_end_date
                                        ,v_run_id
                                        ,p_send_file_by_email
                                        ,NULL);
            END LOOP;
        ELSIF p_order_number IS NOT NULL
        THEN
            IF NOT check_type (p_order_number, 'NUMBER')
            THEN
                xxwc_log_debug ('Order number should be number: ' || p_order_number);
                RAISE e_order_number_not_number;
            END IF;

            create_file_for_account (NULL
                                    ,vr_directory_path
                                    ,p_start_date
                                    ,p_end_date
                                    ,v_run_id
                                    ,p_send_file_by_email
                                    ,p_order_number);
        END IF;

        -- send file by email

        IF p_send_file_by_email = 'Y'
        THEN
            FOR lr IN (SELECT DISTINCT file_name
                         FROM xxwc.email_attachment_clobs
                        WHERE clob_less_32k IS NOT NULL)
            LOOP
                BEGIN
                    SELECT email_address
                      INTO v_sender_address
                      FROM fnd_user
                     WHERE user_id = fnd_profile.VALUE ('USER_ID');
                EXCEPTION
                    WHEN OTHERS
                    THEN
                        v_sender_address := 'donotreply@hdsupply.com';
                END;

                v_recipient_email := NVL (p_recipient_email, v_sender_address);

                SELECT 'MMSI File: ' || lr.file_name INTO v_subject FROM DUAL;

                FOR r4 IN (SELECT instance_name FROM v$instance)
                LOOP
                    v_subject := v_subject || ' from ' || r4.instance_name;
                END LOOP;

                send_mail_with_attachement (v_sender_address
                                           ,v_recipient_email
                                           ,lr.file_name
                                           ,v_subject);
            END LOOP;
        END IF;

        EXECUTE IMMEDIATE 'truncate table xxwc.email_attachment_clobs';

        COMMIT;
    EXCEPTION
        WHEN v_mmsi_directory_not_found
        THEN
            v_error_message := 'Please verify that XXWC_MMSI_ARCH and XXWC_MMSI are created and granted for Public';
            v_err_code := '2';
            p_errbuf := v_error_message;
            p_retcode := v_err_code;
            xxwc_log (v_error_message, 'create_file');
        WHEN e_order_number_not_number
        THEN
            v_error_message := 'Error : Order number should be number: ' || p_order_number;
            v_err_code := '2';
            p_errbuf := v_error_message;
            p_retcode := v_err_code;
            xxwc_log (v_error_message, 'create_file');
        WHEN OTHERS
        THEN
            v_error_message :=
                   'Error_Stack...'
                || DBMS_UTILITY.format_error_stack ()
                || ' Error_Backtrace...'
                || DBMS_UTILITY.format_error_backtrace ();

            v_err_code := '2';
            p_errbuf := v_error_message;
            p_retcode := v_err_code;
            xxwc_log (v_error_message, 'create_file');
    END;

    --************************************************************************************
    --************************************************************************************
    --************************************************************************************
    --************************************************************************************
    --************************************************************************************
    --************************************************************************************

    PROCEDURE populate_data (p_customer_account_number   IN VARCHAR2
                            ,p_start_date                IN DATE
                            ,p_end_date                  IN DATE
                            ,p_file_name                 IN VARCHAR2
                            ,p_run_id                    IN NUMBER)
    IS
        v_del          VARCHAR2 (1) := '~';

        v_trunc_date   DATE := TRUNC (SYSDATE, 'MI');
    BEGIN
        IF (p_customer_account_number IS NOT NULL)
        THEN
            xxwc_log_debug (' Fetching Data for account=' || p_customer_account_number);

            FOR r
                IN (  SELECT    x1.format_name
                             || v_del
                             || x1.customer_name
                             || v_del
                             || x1.customer_mm_id
                             || v_del
                             || x1.customer_po_number
                             || v_del
                             || x1.order_date
                             || v_del
                             || x1.required_date                                                   ---intentionally null
                             || v_del
                             || x1.distributor_name
                             || v_del
                             || x1.distributor_mmd_id
                             || v_del
                             || x1.distributor_location_id
                             || v_del
                             || x1.job_name
                             || v_del
                             || x1.address_id
                             || v_del
                             || x1.ship_to_name                                -- that is what the view translates it to
                             || v_del
                             || x1.ship_to_address_1
                             || v_del
                             || x1.ship_to_address_2
                             || v_del
                             || x1.ship_to_city
                             || v_del
                             || x1.ship_to_state
                             || v_del
                             || x1.ship_to_zip
                             || v_del
                             || x1.order_number
                             || v_del
                             || x1.line_number
                             || v_del
                             || x1.description
                             || v_del
                             || SUM (x1.quantity)                                                         -- AS quantity
                             || v_del
                             || x1.item_required_date
                             --, ol.pricing_quantity_uom AS price_unit_size
                             || v_del
                             || x1.price_unit_of_measure
                             || v_del
                             || x1.price_each
                             || v_del
                             || x1.unit_price
                             || v_del
                             || x1.upc
                             || v_del
                             || x1.distributor_item_id
                             || v_del
                             || x1.customer_item_number                                             --intentionally null
                             || v_del
                             || SUM (x1.extended_price)                                             -- AS extended_price
                             || v_del
                             || SUM (x1.qty_shipping)                                                 -- AS qty_shipping
                             || v_del
                             || SUM (x1.qty_backord)                                                   -- AS qty_backord
                             || v_del
                             || x1.cost_code                                                        --intentionally null
                             || v_del
                             || x1.phase_code                                                       --intentionally null
                             || v_del
                             || x1.cost_type_code                                                   --intentionally null
                             || v_del
                             || x1.general_ledger_code                                              --intentionally null
                             || v_del
                             || x1.release_number                                                   --intentionally null
                             || v_del
                             || x1.buyer                                               --intentionally null at this time
                             || v_del
                             || x1.item_mfg_name                                       --intentionally null at this time
                             || v_del
                             || x1.item_mfg_cat_num                                    --intentionally null at this time
                             || v_del
                             || x1.orig_sales_order_num                                --intentionally null at this time
                             || v_del
                             || x1.cust_job_number
                                 line_to_file
                            ,x1.customer_name
                            ,x1.customer_po_number
                            ,x1.order_date
                            ,x1.order_number
                            ,x1.line_number
                            ,x1.description
                            ,x1.booked_date
                            ,x1.header_id
                            ,MIN (x1.line_id) line_id
                            ,x1.account_number
                        FROM xxwc_sales_order_mmsi_v x1
                       WHERE     x1.account_number = p_customer_account_number
                             AND x1.booked_date BETWEEN p_start_date AND p_end_date
                             AND x1.booked_date < TRUNC (SYSDATE)
                             --  and( x1.booked_date - TRUNC (SYSDATE))>=23/24

                             AND x1.line_id NOT IN (SELECT line_id
                                                      FROM xxwc.xxwc_sales_order_mmsi_line
                                                     WHERE send_flag = 'S' AND l_run_from_uc4 = 'Y')
                    -- base on flag - change it
                    GROUP BY x1.format_name
                            ,x1.customer_name
                            ,x1.customer_mm_id
                            ,x1.customer_po_number
                            ,x1.order_date
                            ,x1.required_date                                                      ---intentionally null
                            ,x1.distributor_name
                            ,x1.distributor_mmd_id
                            ,x1.distributor_location_id
                            ,x1.job_name
                            ,x1.address_id
                            ,x1.ship_to_name                                   -- that is what the view translates it to
                            ,x1.ship_to_address_1
                            ,x1.ship_to_address_2
                            ,x1.ship_to_city
                            ,x1.ship_to_state
                            ,x1.ship_to_zip
                            ,x1.order_number
                            ,x1.line_number
                            ,x1.description
                            ,x1.item_required_date
                            ,x1.price_unit_of_measure
                            ,x1.price_each
                            ,x1.unit_price
                            ,x1.upc
                            ,x1.distributor_item_id
                            ,x1.customer_item_number                                                --intentionally null
                            ,x1.cost_code                                                           --intentionally null
                            ,x1.phase_code                                                          --intentionally null
                            ,x1.cost_type_code                                                      --intentionally null
                            ,x1.general_ledger_code                                                 --intentionally null
                            ,x1.release_number                                                      --intentionally null
                            ,x1.buyer                                                  --intentionally null at this time
                            ,x1.item_mfg_name                                          --intentionally null at this time
                            ,x1.item_mfg_cat_num                                       --intentionally null at this time
                            ,x1.orig_sales_order_num                                   --intentionally null at this time
                            ,x1.cust_job_number
                            ,x1.header_id
                            -- ,x1.line_id
                            ,x1.booked_date
                            ,x1.account_number
                    ORDER BY x1.order_number, x1.line_number)
            LOOP
                --DELETE previous run lines for non uc4 run
                FOR rline IN (SELECT line_id, ROWID vtrowid
                                FROM xxwc.xxwc_sales_order_mmsi_line
                               WHERE send_flag = 'V' AND line_id = r.line_id)
                LOOP
                    DELETE FROM xxwc.xxwc_sales_order_mmsi_line
                          WHERE ROWID = rline.vtrowid;
                END LOOP;

                INSERT INTO xxwc.xxwc_sales_order_mmsi_line (line_to_file
                                                            ,customer_name
                                                            ,customer_po_number
                                                            ,order_date
                                                            ,order_number
                                                            ,line_number
                                                            ,description
                                                            ,booked_date
                                                            ,file_name
                                                            ,file_sent_date
                                                            ,header_id
                                                            ,line_id
                                                            ,send_flag
                                                            ,account_number
                                                            ,run_id)
                     VALUES (r.line_to_file
                            ,r.customer_name
                            ,r.customer_po_number
                            ,r.order_date
                            ,r.order_number
                            ,r.line_number
                            ,r.description
                            ,r.booked_date
                            ,p_file_name
                            ,v_trunc_date
                            ,r.header_id
                            ,r.line_id
                            ,DECODE (l_run_from_uc4,  'N', 'T',  'Y', 'I')
                            ,r.account_number
                            ,p_run_id);
            END LOOP;

            COMMIT;
        END IF;
    END;

    --************************************************************************************
    --************************************************************************************
    --************************************************************************************
    --************************************************************************************
    --************************************************************************************
    --************************************************************************************
    --POPULATE FOR SPECIAL ORDER NUMBER

    PROCEDURE populate_data_order (p_file_name IN VARCHAR2, p_order_number IN VARCHAR2, p_run_id IN NUMBER)
    IS
        v_del          VARCHAR2 (1) := '~';

        v_trunc_date   DATE := TRUNC (SYSDATE, 'MI');
    BEGIN
        xxwc_log_debug ('populate_data_order start');

        FOR r
            IN (  SELECT    x1.format_name
                         || v_del
                         || x1.customer_name
                         || v_del
                         || x1.customer_mm_id
                         || v_del
                         || x1.customer_po_number
                         || v_del
                         || x1.order_date
                         || v_del
                         || x1.required_date                                                       ---intentionally null
                         || v_del
                         || x1.distributor_name
                         || v_del
                         || x1.distributor_mmd_id
                         || v_del
                         || x1.distributor_location_id
                         || v_del
                         || x1.job_name
                         || v_del
                         || x1.address_id
                         || v_del
                         || x1.ship_to_name                                    -- that is what the view translates it to
                         || v_del
                         || x1.ship_to_address_1
                         || v_del
                         || x1.ship_to_address_2
                         || v_del
                         || x1.ship_to_city
                         || v_del
                         || x1.ship_to_state
                         || v_del
                         || x1.ship_to_zip
                         || v_del
                         || x1.order_number
                         || v_del
                         || x1.line_number
                         || v_del
                         || x1.description
                         || v_del
                         || SUM (x1.quantity)                                                             -- AS quantity
                         || v_del
                         || x1.item_required_date
                         --, ol.pricing_quantity_uom AS price_unit_size
                         || v_del
                         || x1.price_unit_of_measure
                         || v_del
                         || x1.price_each
                         || v_del
                         || x1.unit_price
                         || v_del
                         || x1.upc
                         || v_del
                         || x1.distributor_item_id
                         || v_del
                         || x1.customer_item_number                                                 --intentionally null
                         || v_del
                         || SUM (x1.extended_price)                                                 -- AS extended_price
                         || v_del
                         || SUM (x1.qty_shipping)                                                     -- AS qty_shipping
                         || v_del
                         || SUM (x1.qty_backord)                                                       -- AS qty_backord
                         || v_del
                         || x1.cost_code                                                            --intentionally null
                         || v_del
                         || x1.phase_code                                                           --intentionally null
                         || v_del
                         || x1.cost_type_code                                                       --intentionally null
                         || v_del
                         || x1.general_ledger_code                                                  --intentionally null
                         || v_del
                         || x1.release_number                                                       --intentionally null
                         || v_del
                         || x1.buyer                                                   --intentionally null at this time
                         || v_del
                         || x1.item_mfg_name                                           --intentionally null at this time
                         || v_del
                         || x1.item_mfg_cat_num                                        --intentionally null at this time
                         || v_del
                         || x1.orig_sales_order_num                                    --intentionally null at this time
                         || v_del
                         || x1.cust_job_number
                             line_to_file
                        ,x1.customer_name
                        ,x1.customer_po_number
                        ,x1.order_date
                        ,x1.order_number
                        ,x1.line_number
                        ,x1.description
                        ,x1.booked_date
                        ,x1.header_id
                        ,MIN (x1.line_id) line_id
                        ,x1.account_number
                    FROM xxwc_sales_order_mmsi_v x1
                   WHERE     x1.order_number = p_order_number
                         AND x1.booked_date <= TRUNC (SYSDATE)
                         AND x1.account_number IN
                                 (SELECT flex_value
                                    -- ,flex_value_meaning
                                    -- ,description
                                    -- ,enabled_flag
                                    --,start_date_active
                                    -- ,end_date_active

                                    FROM fnd_flex_values_vl
                                   WHERE     (   ('' IS NULL)
                                              OR (structured_hierarchy_level IN
                                                      (SELECT hierarchy_id
                                                         FROM fnd_flex_hierarchies_vl h
                                                        WHERE     h.flex_value_set_id =
                                                                      (SELECT flex_value_set_id
                                                                         FROM fnd_flex_value_sets
                                                                        WHERE flex_value_set_name = 'XXWC_MMSI_accounts')
                                                              AND h.hierarchy_name LIKE '')))
                                         AND flex_value_set_id = (SELECT flex_value_set_id
                                                                    FROM fnd_flex_value_sets
                                                                   WHERE flex_value_set_name = 'XXWC_MMSI_accounts')
                                         AND enabled_flag = 'Y'
                                         AND NVL (end_date_active, SYSDATE + 1 / 24) > SYSDATE)
                --  and( x1.booked_date - TRUNC (SYSDATE))>=23/24
                GROUP BY x1.format_name
                        ,x1.customer_name
                        ,x1.customer_mm_id
                        ,x1.customer_po_number
                        ,x1.order_date
                        ,x1.required_date                                                          ---intentionally null
                        ,x1.distributor_name
                        ,x1.distributor_mmd_id
                        ,x1.distributor_location_id
                        ,x1.job_name
                        ,x1.address_id
                        ,x1.ship_to_name                                       -- that is what the view translates it to
                        ,x1.ship_to_address_1
                        ,x1.ship_to_address_2
                        ,x1.ship_to_city
                        ,x1.ship_to_state
                        ,x1.ship_to_zip
                        ,x1.order_number
                        ,x1.line_number
                        ,x1.description
                        ,x1.item_required_date
                        ,x1.price_unit_of_measure
                        ,x1.price_each
                        ,x1.unit_price
                        ,x1.upc
                        ,x1.distributor_item_id
                        ,x1.customer_item_number                                                    --intentionally null
                        ,x1.cost_code                                                               --intentionally null
                        ,x1.phase_code                                                              --intentionally null
                        ,x1.cost_type_code                                                          --intentionally null
                        ,x1.general_ledger_code                                                     --intentionally null
                        ,x1.release_number                                                          --intentionally null
                        ,x1.buyer                                                      --intentionally null at this time
                        ,x1.item_mfg_name                                              --intentionally null at this time
                        ,x1.item_mfg_cat_num                                           --intentionally null at this time
                        ,x1.orig_sales_order_num                                       --intentionally null at this time
                        ,x1.cust_job_number
                        ,x1.header_id
                        --,x1.line_id
                        ,x1.booked_date
                        ,x1.account_number
                ORDER BY x1.order_number, x1.line_number)
        LOOP
            xxwc_log_debug ('Fetching Data for order:' || p_order_number);

            INSERT INTO xxwc.xxwc_sales_order_mmsi_line (line_to_file
                                                        ,customer_name
                                                        ,customer_po_number
                                                        ,order_date
                                                        ,order_number
                                                        ,line_number
                                                        ,description
                                                        ,booked_date
                                                        ,file_name
                                                        ,file_sent_date
                                                        ,header_id
                                                        ,line_id
                                                        ,send_flag
                                                        ,account_number
                                                        ,run_id)
                 VALUES (r.line_to_file
                        ,r.customer_name
                        ,r.customer_po_number
                        ,r.order_date
                        ,r.order_number
                        ,r.line_number
                        ,r.description
                        ,r.booked_date
                        ,p_file_name
                        ,v_trunc_date
                        ,r.header_id
                        ,r.line_id
                        ,'O'
                        ,r.account_number
                        ,p_run_id);
        END LOOP;

        COMMIT;
    END;

    --************************************************************************************
    --************************************************************************************
    --************************************************************************************
    --************************************************************************************
    --************************************************************************************
    --************************************************************************************
    -- will log messages based on from where

    PROCEDURE xxwc_log_debug (p_message VARCHAR2)
    IS
    BEGIN
        IF l_req_id > 0
        THEN
            fnd_file.put_line (fnd_file.LOG, p_message);
        ELSE
            DBMS_OUTPUT.put_line (p_message);
        END IF;
    -- DBMS_OUTPUT.put_line (p_message);

    END;

    PROCEDURE xxwc_log (p_message VARCHAR2, p_procedure_name VARCHAR2)
    IS
        v_distro_list   VARCHAR2 (75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
    BEGIN
        IF l_req_id > 0
        THEN
            fnd_file.put_line (fnd_file.LOG, '*Message from ' || p_procedure_name || ' ' || p_message);
        ELSE
            DBMS_OUTPUT.put_line ('*Message from ' || p_procedure_name || ' ' || p_message);
        END IF;

        xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'xxwc_sales_order_acknowlg_file, by Rasikha'
           ,                                                                                          -- l_package_name,
            p_calling             => p_procedure_name
           ,p_request_id          => l_req_id
           ,p_ora_error_msg       => SUBSTR (p_message, 1, 2000)
           ,p_error_desc          =>    'Error running '
                                     || 'xxwc_sales_order_acknowlg_file, by Rasikha'
                                     || '.'
                                     || p_procedure_name
           ,p_distribution_list   => v_distro_list
           ,p_module              => 'EDI MMSI');
    END;

    --************************************************************************************
    --************************************************************************************
    --************************************************************************************
    --************************************************************************************
    --************************************************************************************
    --************************************************************************************

    PROCEDURE send_mail_with_attachement (p_from        IN VARCHAR2
                                         ,p_to          IN VARCHAR2
                                         ,p_file_name   IN VARCHAR2
                                         ,p_subject     IN VARCHAR2)
    IS
        v_connection               UTL_SMTP.connection;
        -- mime blocks (the sections of the email body that can become attachments)
        -- must be delimited by a string, this particular string is just an example
        c_mime_boundary   CONSTANT VARCHAR2 (256) := '-----AABCDEFBBCCC0123456789DE';

        p_smtp_hostname            VARCHAR2 (256) := 'mailoutrelay.hdsupply.net';
        p_smtp_portnum             VARCHAR2 (20) := '25';
        v_valid_email              NUMBER := 0;
        e_invalid_email            EXCEPTION;
        v_message                  VARCHAR2 (254);
    BEGIN
        -- Build the contents before connecting to the mail server
        -- that way you can begin pumping the data immediately
        -- and not risk an SMTP timeout

        v_valid_email := xxwc_sales_order_acknowlg_file.valid_email (p_from);

        IF v_valid_email = 0
        THEN
            v_message := 'Invalid email addess :' || p_from;
            RAISE e_invalid_email;
        END IF;

        v_valid_email := xxwc_sales_order_acknowlg_file.valid_email (p_to);

        IF v_valid_email = 0
        THEN
            v_message := 'Invalid email addess :' || p_to;
            RAISE e_invalid_email;
        END IF;

        v_connection := UTL_SMTP.open_connection (p_smtp_hostname, p_smtp_portnum);
        UTL_SMTP.helo (v_connection, p_smtp_hostname);
        UTL_SMTP.mail (v_connection, p_from);
        UTL_SMTP.rcpt (v_connection, p_to);
        UTL_SMTP.open_data (v_connection);

        UTL_SMTP.write_data (v_connection, 'From: ' || p_from || UTL_TCP.crlf);
        UTL_SMTP.write_data (v_connection, 'To: ' || p_to || UTL_TCP.crlf);
        UTL_SMTP.write_data (v_connection, 'Subject: ' || p_subject || UTL_TCP.crlf);
        UTL_SMTP.write_data (v_connection, 'MIME-Version: 1.0' || UTL_TCP.crlf);

        UTL_SMTP.write_data (v_connection
                            ,'Content-Type: multipart/mixed; boundary="' || c_mime_boundary || '"' || UTL_TCP.crlf);
        UTL_SMTP.write_data (v_connection, UTL_TCP.crlf);
        UTL_SMTP.write_data (v_connection, 'This is a multi-part message in MIME format.' || UTL_TCP.crlf);

        UTL_SMTP.write_data (v_connection, '--' || c_mime_boundary || UTL_TCP.crlf);
        UTL_SMTP.write_data (v_connection, 'Content-Type: text/plain' || UTL_TCP.crlf);

        -- Set up attachment header
        UTL_SMTP.write_data (v_connection
                            ,'Content-Disposition: attachment; filename="' || p_file_name || '"' || UTL_TCP.crlf);
        UTL_SMTP.write_data (v_connection, UTL_TCP.crlf);

        -- Write attachment contents
        FOR r IN (  SELECT *
                      FROM xxwc.email_attachment_clobs
                     WHERE file_name = p_file_name
                  ORDER BY insert_id)
        LOOP
            UTL_SMTP.write_data (v_connection, r.clob_less_32k);
        END LOOP;

        --
        -- End attachment
        UTL_SMTP.write_data (v_connection, UTL_TCP.crlf);
        UTL_SMTP.write_data (v_connection, '--' || c_mime_boundary || '--' || UTL_TCP.crlf);

        UTL_SMTP.close_data (v_connection);
        UTL_SMTP.quit (v_connection);
    EXCEPTION
        WHEN e_invalid_email
        THEN
            xxwc_log_debug ('Can not send email ' || v_message);
        WHEN OTHERS
        THEN
            xxwc_log_debug (DBMS_UTILITY.format_error_stack);
    END;

    --************************************************************************************
    --************************************************************************************
    --************************************************************************************
    --************************************************************************************
    --************************************************************************************
    --************************************************************************************

    PROCEDURE create_file_for_account (p_account_number       IN VARCHAR2
                                      ,p_filedir              IN VARCHAR2
                                      ,p_start_date           IN DATE
                                      ,p_end_date             IN DATE
                                      ,p_run_id               IN NUMBER
                                      ,p_send_file_by_email   IN VARCHAR2
                                      ,p_order_number         IN VARCHAR2)
    IS
        v_ftp_file            VARCHAR2 (244);
        v_file_name           VARCHAR2 (244);
        v_file_dir            VARCHAR2 (244) := 'XXWC_MMSI';
        v_file_handle         UTL_FILE.file_type;
        v_os_error            CLOB;
        n_loop_counter        NUMBER := 0;
        v_file_data           CLOB := NULL;
        v_dat_length          NUMBER := 0;
        v_exception           EXCEPTION;

        v_exception_message   VARCHAR2 (244);
        v_run_text            VARCHAR2 (124);
        v_filedir_archive     VARCHAR2 (244);
    BEGIN
        xxwc_log_debug ('create_file_for_account for account_number=' || p_account_number);

        IF p_account_number IS NOT NULL AND check_type (p_account_number, 'NUMBER')
        THEN
            FOR rt
                IN (SELECT    'WCC_'
                           || REPLACE( REPLACE (account_name, ' ', '_'), '&', 'AND') --TMS#20161122-00021
                           || '_'
                           || TO_CHAR (SYSDATE, 'YYYYDDMM')
                           || '_'
                           || p_run_id
                           || DECODE (l_run_from_uc4,  'Y', '',  'N', 'M')
                           || '.txt'
                               file_name
                          ,'WCC_' || REPLACE( REPLACE (account_name, ' ', '_'), '&', 'AND') || '.txt' ftp_file --TMS#20161122-00021
                      FROM hz_cust_accounts cust
                     WHERE account_number = p_account_number)
            LOOP
                v_file_name := rt.file_name;
                v_ftp_file := rt.ftp_file;
            END LOOP;

            SELECT 'WCC_' || REPLACE( REPLACE (account_name, ' ', '_'), '&', 'AND') || '.txt' --TMS#20161122-00021
              INTO v_ftp_file
              FROM hz_cust_accounts cust
             WHERE account_number = p_account_number;

            v_run_text := 'Account Number =' || p_account_number || ' ';
        ELSIF p_order_number IS NOT NULL
        THEN
            FOR rt
                IN (SELECT    'WCC_'
                           || REPLACE( REPLACE (customer_name, ' ', '_'), '&', 'AND') --TMS#20161122-00021
                           || '_order_'
                           || p_order_number
                           || '_'
                           || TO_CHAR (SYSDATE, 'YYYYDDMM')
                           || '_'
                           || p_run_id
                           || '.txt'
                               file_name
                      FROM xxwc_sales_order_mmsi_v
                     WHERE order_number = p_order_number --  and   account_number IN ('1076000', '2586000', '154984000', '118830000')
                                                        AND ROWNUM = 1)
            LOOP
                v_file_name := rt.file_name;
                v_ftp_file := NULL;
            END LOOP;

            v_run_text := 'Order Number =' || p_order_number || ' ';
        END IF;

        IF v_file_name IS NULL
        THEN
            v_exception_message := v_run_text || ' does not exist in Oracle';
            xxwc_log_debug (v_exception_message);
            RAISE v_exception;
        END IF;

        --delete prevously sent file and populate data into staging table
        IF v_ftp_file IS NOT NULL AND p_account_number IS NOT NULL
        THEN
            IF l_run_from_uc4 = 'Y'
            THEN
                SELECT xxwc_edi_iface_pkg.xxwc_oscommand_run ('rm ' || p_filedir || '/' || v_ftp_file)
                  INTO v_os_error
                  FROM DUAL;
            END IF;

            populate_data (p_account_number
                          ,p_start_date
                          ,p_end_date
                          ,v_file_name
                          ,p_run_id);
        ELSIF p_order_number IS NOT NULL
        THEN
            populate_data_order (v_file_name, p_order_number, p_run_id);
        END IF;

        --   xxwc_log_debug ('v_file_name=' || v_file_name);

        v_file_dir := 'XXWC_MMSI';

        IF UTL_FILE.is_open (v_file_handle)
        THEN
            UTL_FILE.fclose (v_file_handle);
        -- DBMS_OUTPUT.put_line ('Closed All');
        END IF;

        IF p_account_number IS NOT NULL
        THEN
            FOR r
                IN (  SELECT ROWID lk
                            ,line_to_file
                            ,customer_name
                            ,customer_po_number
                            ,order_date
                            ,order_number
                            ,line_number
                            ,description
                            ,booked_date
                            ,file_name
                            ,file_sent_date
                            ,header_id
                            ,line_id
                            ,send_flag
                        FROM xxwc.xxwc_sales_order_mmsi_line k
                       WHERE     k.account_number = p_account_number
                             AND k.booked_date BETWEEN p_start_date AND p_end_date -- AND k.booked_date < TRUNC (SYSDATE)                     --orders from previous business day
                             AND k.send_flag = DECODE (l_run_from_uc4,  'Y', 'I',  'N', 'T')
                             AND run_id = p_run_id
                    ORDER BY header_id, line_id)
            LOOP
                n_loop_counter := n_loop_counter + 1;

                IF n_loop_counter = 1
                THEN
                    v_file_handle := UTL_FILE.fopen (v_file_dir, v_file_name, 'w');
                END IF;

                UTL_FILE.put_line (v_file_handle, r.line_to_file);

                IF p_send_file_by_email = 'Y'
                THEN
                    v_file_data := v_file_data || r.line_to_file || CHR (10);
                    v_dat_length := v_dat_length + LENGTH (v_file_data);

                    IF v_dat_length >= '25000'
                    THEN
                        INSERT INTO xxwc.email_attachment_clobs (file_name, clob_less_32k, insert_id)
                             VALUES (v_file_name, v_file_data, n_loop_counter);

                        v_file_data := NULL;
                        v_dat_length := 0;
                    END IF;
                END IF;

                UPDATE xxwc.xxwc_sales_order_mmsi_line
                   SET send_flag = DECODE (l_run_from_uc4,  'Y', 'S',  'N', 'V')
                 WHERE ROWID = r.lk;
            END LOOP;

            --insert leftover chunk to the table
            IF p_send_file_by_email = 'Y'
            THEN
                INSERT INTO xxwc.email_attachment_clobs (file_name, clob_less_32k, insert_id)
                     VALUES (v_file_name, v_file_data, n_loop_counter + 1);
            END IF;

            IF UTL_FILE.is_open (v_file_handle)
            THEN
                UTL_FILE.fclose (v_file_handle);
            END IF;

            --find archive directory path
            FOR nm IN (SELECT directory_path
                         FROM all_directories
                        WHERE directory_name = 'XXWC_MMSI_ARCH')
            LOOP
                v_filedir_archive := nm.directory_path;
            END LOOP;

            --copy file to ftp file
            IF n_loop_counter > 0
            THEN
                IF l_run_from_uc4 = 'Y'
                THEN
                    SELECT xxwc_edi_iface_pkg.xxwc_oscommand_run (
                               'cp ' || p_filedir || '/' || v_file_name || ' ' || p_filedir || '/' || v_ftp_file)
                      INTO v_os_error
                      FROM DUAL;

                    xxwc_log_debug (
                        'oscommand output =' || NVL (v_os_error, v_file_name || ' was copied to file ' || v_ftp_file));

                    SELECT xxwc_edi_iface_pkg.xxwc_oscommand_run ('chmod 777 ' || p_filedir || '/' || v_ftp_file)
                      INTO v_os_error
                      FROM DUAL;

                    xxwc_log_debug (
                           'oscommand output ='
                        || NVL (v_os_error, v_file_name || 'chmod 777 ' || p_filedir || '/' || v_ftp_file));
                END IF;

                SELECT xxwc_edi_iface_pkg.xxwc_oscommand_run (
                           'mv ' || p_filedir || '/' || v_file_name || ' ' || v_filedir_archive || '/' || v_file_name)
                  INTO v_os_error
                  FROM DUAL;

                xxwc_log_debug ('oscommand output =' || NVL (v_os_error, v_file_name || ' was archived'));
            END IF;
        ELSIF p_order_number IS NOT NULL
        THEN
            IF UTL_FILE.is_open (v_file_handle)
            THEN
                UTL_FILE.fclose (v_file_handle);
            END IF;

            FOR r IN (  SELECT ROWID lk
                              ,line_to_file
                              ,customer_name
                              ,customer_po_number
                              ,order_date
                              ,order_number
                              ,line_number
                              ,description
                              ,booked_date
                              ,file_name
                              ,file_sent_date
                              ,header_id
                              ,line_id
                              ,send_flag
                          FROM xxwc.xxwc_sales_order_mmsi_line k
                         WHERE k.order_number = p_order_number -- AND k.booked_date < TRUNC (SYSDATE)                     --orders from previous business day
                                                              AND k.send_flag = 'O' AND run_id = p_run_id
                      ORDER BY header_id, line_id)
            LOOP
                n_loop_counter := n_loop_counter + 1;

                IF n_loop_counter = 1
                THEN
                    v_file_handle := UTL_FILE.fopen (v_file_dir, v_file_name, 'w');
                END IF;

                UTL_FILE.put_line (v_file_handle, r.line_to_file);

                --  xxwc_log_debug ('p_recipient_email:=' || p_recipient_email);

                IF p_send_file_by_email = 'Y'
                THEN
                    v_file_data := v_file_data || r.line_to_file || CHR (10);

                    IF v_dat_length >= '25000'
                    THEN
                        INSERT INTO xxwc.email_attachment_clobs (file_name, clob_less_32k, insert_id)
                             VALUES (v_file_name, v_file_data, n_loop_counter);

                        v_file_data := NULL;
                        v_dat_length := 0;
                    END IF;
                END IF;

                UPDATE xxwc.xxwc_sales_order_mmsi_line
                   SET send_flag = 'D'
                 WHERE ROWID = r.lk;
            END LOOP;

            INSERT INTO xxwc.email_attachment_clobs (file_name, clob_less_32k, insert_id)
                 VALUES (v_file_name, v_file_data, n_loop_counter + 1);
        END IF;

        IF n_loop_counter > 0
        THEN
            IF UTL_FILE.is_open (v_file_handle)
            THEN
                UTL_FILE.fclose (v_file_handle);
            END IF;
        ELSIF n_loop_counter = 0
        THEN
            xxwc_log_debug (' No data found for  ' || v_run_text);
        END IF;
    EXCEPTION
        WHEN v_exception
        THEN
            RAISE;
    END;

    --************************************************************************************
    --************************************************************************************
    --************************************************************************************
    --***************************************************************
    --***************************************************************

    FUNCTION check_type (p_in_string VARCHAR2, p_type VARCHAR2)
        RETURN BOOLEAN
    IS
        v_return_flag   BOOLEAN := FALSE;
        v_number        NUMBER;
    BEGIN
        IF UPPER (p_type) = 'NUMBER'
        THEN
            BEGIN
                SELECT TO_NUMBER (p_in_string) INTO v_number FROM DUAL;

                v_return_flag := TRUE;
            EXCEPTION
                WHEN OTHERS
                THEN
                    v_return_flag := FALSE;
            END;
        END IF;

        RETURN v_return_flag;
    END;

    --******************************************************************************
    --************************************************************************************
    --************************************************************************************
    --************************************************************************************

    FUNCTION valid_email (p_email IN VARCHAR2)
        RETURN NUMBER
    IS
        cemailregexp   CONSTANT VARCHAR2 (1000)
            := '^[a-z0-9!#$%&''*+/=?^_`{|}~-]+(\.[a-z0-9!#$%&''*+/=?^_`{|}~-]+)*@([a-z0-9]([a-z0-9-]*[a-z0-9])?\.)+([A-Z]{2}|arpa|biz|com|info|intww|name|net|org|pro|aero|asia|cat|coop|edu|gov|jobs|mil|mobi|museum|pro|tel|travel|post)$' ;
    BEGIN
        IF REGEXP_LIKE (p_email, cemailregexp, 'i')
        THEN
            RETURN 1;
        ELSE
            RETURN 0;
        END IF;
    EXCEPTION
        WHEN OTHERS
        THEN
            xxwc_log_debug ('email address ' || p_email || ' is not right format;');
            RETURN 0;
    END;
END;
/

-- Grants for Package Body
GRANT EXECUTE ON apps.xxwc_sales_order_acknowlg_file TO interface_xxcus
/

-- End of DDL Script for Package Body APPS.XXWC_SALES_ORDER_ACKNOWLG_FILE
