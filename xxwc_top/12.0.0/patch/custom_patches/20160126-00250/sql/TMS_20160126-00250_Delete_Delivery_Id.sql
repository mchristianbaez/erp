/*
 TMS: 20160126-00250  
 Date: 01/26/2016
 Notes: O#18968238 will not closeout. Customer paid cash for the item, delivery docs printed. Cust left with the item, but will not close. Please help.
*/

SET SERVEROUTPUT ON SIZE 100000;


delete from XXWC.XXWC_WSH_SHIPPING_STG where header_id = 36700023 
and delivery_id=4193469;

commit
/