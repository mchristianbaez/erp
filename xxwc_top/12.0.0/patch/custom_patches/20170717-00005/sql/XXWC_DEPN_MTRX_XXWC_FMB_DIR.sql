/*************************************************************************
  $Header XXWC_DEPN_MTRX_XXWC_FMB_DIR.sql $
  Module Name: XXWC_DEPN_MTRX_XXWC_FMB_DIR

  PURPOSE: Table to maintain forms and reports modified history.

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    ---------------------------------------------------------------------------------------
  1.1        15-Jun-2017  P.vamshidhar          TMS#20170707-00224 - Custom Dependency Matrix
*******************************************************************************************************************************************/ 
DECLARE
   lvc_environment   VARCHAR2 (100);
   lvc_sql           VARCHAR2 (4000);
BEGIN
   SELECT '/xx_iface/' || LOWER (NAME) || '/inbound/xx_depn_mtrx/nondb'
     INTO lvc_environment
     FROM v$database;

   lvc_sql :=
         'CREATE OR REPLACE DIRECTORY XXWC_DEPN_MTRX_XXWC_FMB_DIR AS '
      || ''''
      || lvc_environment
      || '''';

   EXECUTE IMMEDIATE lvc_sql;

   lvc_sql :=
         'CREATE OR REPLACE DIRECTORY XXWC_CUST_DEPN_MTRX_NONDB_DIR AS '
      || ''''
      || lvc_environment
      || '''';   
   EXECUTE IMMEDIATE lvc_sql;	  
END;
/