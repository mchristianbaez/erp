/*************************************************************************
  $Header XXWC_CUSTOM_RDF_DEP_MTRX_TBL.sql $
  Module Name: XXWC_CUSTOM_RDF_DEP_MTRX_TBL

  PURPOSE: Table to maintain forms and reports modified history.

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    ---------------------------------------------------------------------------------------
  1.1        15-Jun-2017  P.vamshidhar          TMS#20170707-00224 - Custom Dependency Matrix
*******************************************************************************************************************************************/ 
CREATE TABLE XXWC.XXWC_CUST_DEPN_MTRX_EXT_TBL
(
  FILE_PERMISSIONS  VARCHAR2(1000 BYTE),
  FILE_TYPE         VARCHAR2(64 BYTE),
  FILE_OWNER        VARCHAR2(64 BYTE),
  FILE_GROUP        VARCHAR2(64 BYTE),
  FILE_SIZE         VARCHAR2(64 BYTE),
  FILE_MONTH        VARCHAR2(64 BYTE),
  FILE_DAY          VARCHAR2(64 BYTE),
  FILE_TIME         VARCHAR2(64 BYTE),
  FILE_NAME         VARCHAR2(612 BYTE)
)
ORGANIZATION EXTERNAL
  (  TYPE ORACLE_LOADER
     DEFAULT DIRECTORY XXWC_DEPN_MTRX_XXWC_FMB_DIR
     ACCESS PARAMETERS 
       ( RECORDS DELIMITED BY NEWLINE
    PREPROCESSOR XXWC_DEPN_MTRX_XXWC_FMB_DIR: 'list_files.sh'
    NOBADFILE
    NODISCARDFILE
    NOLOGFILE
    FIELDS TERMINATED BY WHITESPACE
                   )
     LOCATION (XXWC_DEPN_MTRX_XXWC_FMB_DIR:'list_files_dummy_source.txt')
  )
REJECT LIMIT UNLIMITED
NOPARALLEL
NOMONITORING;
/