CREATE OR REPLACE PACKAGE APPS.XXWC_CUSTOM_DEPEN_MTRX_PKG
IS
   /********************************************************************************
   FILE NAME: XXWC_CUSTOM_DEPEN_MTRX_PKG.pks

   PROGRAM TYPE: PL/SQL Package

   PURPOSE: To load dependency objects

   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     07-JUL-2017   P.Vamshidhar   TMS#20170707-00224 - Custom Dependency Matrix

   ********************************************************************************/

   TYPE tab_xxwc_custom_dep_mtrx_tbl
      IS TABLE OF XXWC.XXWC_CUSTOM_DEP_MTRX_TBL_TMP%ROWTYPE
      INDEX BY BINARY_INTEGER;

   obj_xxwc_custom_dep_mtrx_tbl   tab_xxwc_custom_dep_mtrx_tbl;

   PROCEDURE main (x_errbuf         OUT VARCHAR2,
                   x_retcode        OUT NUMBER,
                   p_from_date   IN     VARCHAR2,
                   p_source      IN     VARCHAR2);

   PROCEDURE delete_data (x_errbuf         OUT VARCHAR2,
                          x_retcode        OUT NUMBER,
                          p_from_date   IN     DATE,
                          p_source      IN     VARCHAR2);

   PROCEDURE load_db_objects (x_errbuf         OUT VARCHAR2,
                              x_retcode        OUT VARCHAR2,
                              p_from_date   IN     DATE);

   PROCEDURE load_db_source (x_errbuf         OUT VARCHAR2,
                             x_retcode        OUT VARCHAR2,
                             p_from_date   IN     DATE);

   PROCEDURE load_oracle_forms (x_errbuf         OUT VARCHAR2,
                                x_retcode        OUT VARCHAR2,
                                p_from_date   IN     DATE);

   PROCEDURE load_oracle_plls (x_errbuf         OUT VARCHAR2,
                               x_retcode        OUT VARCHAR2,
                               p_from_date   IN     DATE);


   PROCEDURE load_oracle_reports (x_errbuf         OUT VARCHAR2,
                                  x_retcode        OUT VARCHAR2,
                                  p_from_date   IN     DATE);

   PROCEDURE load_host_objects (x_errbuf         OUT VARCHAR2,
                                x_retcode        OUT VARCHAR2,
                                p_from_date   IN     DATE);



   PROCEDURE load_wf_objects (x_errbuf         OUT VARCHAR2,
                              x_retcode        OUT VARCHAR2,
                              p_from_date   IN     DATE);


   PROCEDURE text_process (p_object_name IN VARCHAR2, p_text IN VARCHAR2);

   PROCEDURE insert_data (p_owner               IN VARCHAR2,
                          p_object_name         IN VARCHAR2,
                          p_object_type         IN VARCHAR2,
                          pd_creation_date      IN DATE,
                          pd_last_update_date   IN DATE,
                          p_obj_type            IN VARCHAR2);

   PROCEDURE data_update;
END;
/