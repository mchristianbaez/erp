/*************************************************************************
  $Header XXWC_CUSTOM_DEP_MTRX_TBL.sql $
  Module Name: XXWC_CUSTOM_DEP_MTRX_TBL

  PURPOSE: Table to maintain forms and reports modified history.

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    ---------------------------------------------------------------------------------------
  1.1        15-Jun-2017  P.vamshiddhar         TMS#20170707-00224 - Custom Dependency Matrix
*******************************************************************************************************************************************/ 
CREATE TABLE XXWC.XXWC_CUSTOM_DEP_MTRX_TBL
(
  OWNER                 VARCHAR2(30 BYTE)       NOT NULL,
  NAME                  VARCHAR2(30 BYTE)       NOT NULL,
  TYPE                  VARCHAR2(18 BYTE),
  REFERENCED_OWNER      VARCHAR2(30 BYTE),
  REFERENCED_NAME       VARCHAR2(64 BYTE),
  REFERENCED_TYPE       VARCHAR2(18 BYTE),
  REFERENCED_LINK_NAME  VARCHAR2(128 BYTE),
  DEPENDENCY_TYPE       VARCHAR2(4 BYTE),
  CREATED_BY            VARCHAR2(40 BYTE)       DEFAULT 'APPS',
  LAST_UPDATED_BY       VARCHAR2(40 BYTE)       DEFAULT 'APPS',
  LAST_UPDATE_DATE      DATE,
  OBJ_TYPE              VARCHAR2(100 BYTE),
  CREATION_DATE         DATE
);
/
CREATE SYNONYM APPS.XXWC_RO4D_MAIN_TBL FOR XXWC.XXWC_CUSTOM_DEP_MTRX_TBL
/
GRANT ALL ON APPS.XXWC_RO4D_MAIN_TBL TO EA_APEX
/