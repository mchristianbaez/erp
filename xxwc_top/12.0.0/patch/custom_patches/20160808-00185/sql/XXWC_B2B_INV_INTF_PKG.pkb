CREATE OR REPLACE PACKAGE BODY APPS.XXWC_B2B_INV_INTF_PKG
AS        
   /*************************************************************************
    Copyright (c) 2012 Lucidity Consulting Group
    All rights reserved.
   **************************************************************************
     $Header XXWC_B2B_INV_INTF_PKG $
     Module Name: XXWC_B2B_INV_INTF_PKG.pks

     PURPOSE:   XXWC B2B Open Invoice Outbound Interface

     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        05/01/2014  Gopi Damuluri           Initial Version TMS# 20140219-00148
     1.1        06/09/2014  Gopi Damuluri           TMS# 20140609-00256
                                                    Update AR Invoice DFF "B2B Invoice Print Date" after sending Invoice file to Liaison
     1.2        08/12/2014  Gopi Damuluri           TMS# 20140812-00199
                                                    Remove File Header and End Of File records from B2B Invoice Extract
     1.3        09/30/2014  Gopi Damuluri           TMS# 20140930-00321
                                                    Invoice Extract should have PO Line#s from Staging but not Order Line#s in Oracle.
     1.4        05/16/2016  Gopi Damuluri           TMS# 20160516-00148 - Issue with B2B Stats showing Tax Amounts
	 1.5        07/07/2016  Niraj K Ranjan          TMS# 20160705-00271   B2B Resolve UOM issue for NPL Invoices\Development
	 1.6		08/29/2016	Pattabhi Avula			TMS#20160808-00185 -- Removed Commas in currency formats
**************************************************************************/

   --Email Defaults
   g_dflt_email              fnd_user.email_address%TYPE := 'HDSOracleDevelopers@hdsupply.com';

   /*************************************************************************
     Procedure : Write_Log

     PURPOSE:   This procedure logs debug message in Concurrent Log file
     Parameter:
            IN
                p_debug_msg      -- Debug Message
   ************************************************************************/
   PROCEDURE write_log (p_debug_msg IN VARCHAR2)
   IS
   BEGIN
      fnd_file.put_line (fnd_file.LOG, p_debug_msg);
   END write_log;

   /*************************************************************************
      Procedure : Write_Error

     PURPOSE:   This procedure logs error message
     Parameter:
            IN
                p_debug_msg      -- Debug Message
   ************************************************************************/

   --add message to concurrent output file
   PROCEDURE write_error (p_debug_msg IN VARCHAR2)
   IS
      l_req_id          NUMBER        := fnd_global.conc_request_id;
      l_err_callfrom    VARCHAR2 (75) DEFAULT 'XXWC_AR_BILL_TRUST_INT_PKG';
      l_err_callpoint   VARCHAR2 (75) DEFAULT 'START';
      l_distro_list     VARCHAR2 (75)
                                   DEFAULT 'HDSOracleDevelopers@hdsupply.com';
   BEGIN
      xxcus_error_pkg.xxcus_error_main_api
         (p_called_from            => l_err_callfrom,
          p_calling                => l_err_callpoint,
          p_request_id             => l_req_id,
          p_ora_error_msg          => SUBSTR (p_debug_msg, 1, 2000),
          p_error_desc             => 'Error running xxwc_billtrust_intf_pkg with PROGRAM ERROR',
          p_distribution_list      => l_distro_list,
          p_module                 => 'AR'
         );
   END write_error;

   /*************************************************************************
     Procedure : gen_open_invoice_file

     PURPOSE:   This procedure creates file for the open invoices for
                B2B Liason
     Parameter:
     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.1        06/09/2014  Gopi Damuluri           TMS# 20140609-00256
                                                    Update AR Invoice DFF "B2B Invoice Print Date" after sending Invoice file to Liaison
     1.2        08/12/2014  Gopi Damuluri           TMS# 20140812-00199
                                                    Remove File Header and End Of File records from B2B Invoice Extract
     1.3        09/30/2014  Gopi Damuluri           TMS# 20140930-00321
                                                    Invoice Extract should have PO Line#s from Staging but not Order Line#s in Oracle.
     1.5        07/07/2016  Niraj K Ranjan          TMS# 20160705-00271   B2B Resolve UOM issue for NPL Invoices\Development
	 1.6		08/29/2016	Pattabhi Avula			TMS#20160808-00185 -- Removed Commas in currency formats
   ************************************************************************/
   PROCEDURE gen_open_invoice_file (
      errbuf             OUT      VARCHAR2,
      retcode            OUT      VARCHAR2,
      p_directory_name   IN OUT   VARCHAR2,
      p_from_date        IN       VARCHAR2,
      p_to_date          IN       VARCHAR2,
      p_tp_name          IN       VARCHAR2
   )
   IS

      CURSOR cur_trading_partner (c_from_date DATE, c_to_date DATE)  -- TMS# 20140219-00148
      IS   SELECT distinct stg.TP_NAME
             FROM xxwc_bill_trust_inv_headers_vw x1
                , xxwc.xxwc_b2b_cust_info_tbl    stg   -- TMS# 20140219-00148
            WHERE x1.org_id = 162
              AND x1.party_id = stg.party_id
              AND SYSDATE BETWEEN NVL(stg.start_date_active, SYSDATE - 10) AND NVL(stg.end_date_active, SYSDATE + 10)
              AND NVL(stg.deliver_invoice,'N') = 'Y' -- TMS# 20140219-00148
              AND x1.order_type  = 'STANDARD ORDER'  -- TMS# 20140219-00148
              AND x1.transaction_type = 'INV'        -- TMS# 20140219-00148
              AND (stg.tp_name = p_tp_name OR p_tp_name IS NULL)    -- TMS# 20140219-00148
              AND (   (    c_from_date IS NOT NULL
                       AND c_to_date IS NOT NULL
                       AND TRUNC (x1.invoice_date) BETWEEN TRUNC (c_from_date)
                                                       AND TRUNC (c_to_date)
                      )
                   OR (    c_from_date IS NULL
                       AND c_to_date IS NULL
                       AND x1.bt_send_date IS NULL
                      )
                  )
              AND ( 
                    ( UPPER (NVL (x1.batch_source, 'ZZZ')) NOT IN ('REBILL', 'REBILL-CM', 'LATE CHARGES SOURCE') )
                    OR
                    (
                      UPPER (NVL (x1.batch_source, 'ZZZ')) IN ('ORDER MANAGEMENT', 'REPAIR OM SOURCE', 'STANDARD OM SOURCE')
                      AND x1.transaction_type = 'CRM'
                    )
                  )
              AND (NVL (x1.terms, 'ZZZ') NOT IN ('COD', 'PRFRDCASH')
                    OR
                   NVL (x1.acct_payment_term, 'ZZZ') NOT IN ('COD', 'PRFRDCASH')
                  );



      CURSOR cur_operating_units
      IS SELECT   DISTINCT (ou.organization_id) org_id, ou.organization_id,
                     ou.NAME org_name, ou.location_id, hl.address_line_1,
                     hl.address_line_2, hl.town_or_city, hl.region_2,
                     hl.postal_code, hl.region_1, hl.country
                FROM hr_all_organization_units ou,
                     hr_locations hl
               WHERE ou.organization_id = 162 -- Need to set to profile
                 AND ou.location_id = hl.location_id
            ORDER BY ou.organization_id;

      CURSOR cur_inv_headers (c_from_date DATE, c_to_date DATE, c_org_id NUMBER, c_tp_name VARCHAR2) -- TMS# 20140219-00148
      IS
         SELECT   x1.*
             FROM xxwc_bill_trust_inv_headers_vw x1
                , xxwc.xxwc_b2b_cust_info_tbl    stg   -- TMS# 20140219-00148
            WHERE x1.org_id = c_org_id
              AND x1.party_id = stg.party_id
              AND SYSDATE BETWEEN NVL(stg.start_date_active, SYSDATE - 10) AND NVL(stg.end_date_active, SYSDATE + 10)
              AND stg.tp_name = c_tp_name                       -- TMS# 20140219-00148
              AND NVL(stg.deliver_invoice,'N') = 'Y'            -- TMS# 20140219-00148
              AND x1.order_type  = 'STANDARD ORDER'             -- TMS# 20140219-00148
              AND x1.transaction_type = 'INV'                   -- TMS# 20140219-00148
              AND EXISTS (SELECT '1'
                            FROM oe_order_headers_all           ooh
                               , oe_order_sources               oos
                               , ra_customer_trx_all            rcta
                           WHERE 1 = 1
                             AND rcta.customer_trx_id = x1.invoice_id
                             AND to_char(ooh.order_number) = rcta.interface_header_attribute1  -- TMS# 20140219-00148
                             AND ooh.order_source_id  = oos.order_source_id     -- TMS# 20140219-00148
                             AND oos.name             = 'B2B LIASON'            -- TMS# 20140219-00148
                             AND rcta.attribute10     IS NULL
                         )
              AND TRUNC (x1.invoice_date) BETWEEN TRUNC (c_from_date) AND TRUNC (c_to_date)
              AND (NVL (x1.terms, 'ZZZ') NOT IN ('COD', 'PRFRDCASH')
                    OR
                   NVL (x1.acct_payment_term, 'ZZZ') NOT IN ('COD', 'PRFRDCASH')
                  )
              AND UPPER (NVL (x1.batch_source, 'ZZZ')) IN ('ORDER MANAGEMENT', 'STANDARD OM SOURCE')
--              AND x1.transaction_type = 'CRM'
/*
              AND (   (    c_from_date IS NOT NULL
                       AND c_to_date IS NOT NULL
                       AND TRUNC (x1.invoice_date) BETWEEN TRUNC (c_from_date)
                                                       AND TRUNC (c_to_date)
                      )
                   OR (    c_from_date IS NULL
                       AND c_to_date IS NULL
                       AND x1.bt_send_date IS NULL
                      )
                  )
              AND ( 
                    ( UPPER (NVL (x1.batch_source, 'ZZZ')) NOT IN ('REBILL', 'REBILL-CM', 'LATE CHARGES SOURCE') )
                    OR
                    (
                      UPPER (NVL (x1.batch_source, 'ZZZ')) IN ('ORDER MANAGEMENT', 'REPAIR OM SOURCE', 'STANDARD OM SOURCE')
                      AND x1.transaction_type = 'CRM'
                    )
                  )
*/
         ORDER BY x1.invoice_number;

      CURSOR cur_inv_lines (c_invoice_id NUMBER)
      IS
         SELECT   x1.*
             FROM xxwc_bill_trust_inv_lines_vw x1
            WHERE x1.invoice_id = c_invoice_id
         -- 02/18/2013 CGonzalez: Changed to sort with the new line number (OM LIne number and Prism Line Number)
         ORDER BY x1.new_line_number;

      -- 04/01/2012 CG To Pull Prism Line Notes
      -- 06/08/2012 CG Modified the source of the data for the line notes
      -- from table xxwc.xxwc_ar_prism_inv_notes_tbl to
      -- xxwc.XXWC_AR_PRISM_INV_LN_NOTES_STG
      CURSOR cur_prism_ln_note (c_trx_number VARCHAR2, c_line_number VARCHAR2)
      IS
         SELECT DISTINCT (trx_number), line_no, line_order, sequencenumber,
                         description
                    FROM xxwc.xxwc_ar_prism_inv_ln_notes_stg
                   WHERE trx_number = c_trx_number
                     AND line_no = c_line_number
                     AND line_order != 1
                     AND PRINT = 'Y'
                ORDER BY line_order, sequencenumber;

      
      -- 06/11/2012 CG: added to breakout shipping/packing instructions
      -- from OM that have CRs
      -- 06/18/2012 CG: Modified to pull directly from OM Lines
      -- 09/06/2012 CG: Added block to pull serial numbers from
      -- WDD Attribute1 as notes for BT. Set in LVL1, moved Ship
      -- notes and packing notes to LVL2 and LVL3 
      -- 01/09/13 CG: Changing code for shipping and packing instruction
      -- pull to improve performance     
      CURSOR cur_oracle_ln_note (c_oe_line_id NUMBER)
      IS
         -- 01/09/13 CG: Added for performance
         WITH    cntr1     AS
                (
                    SELECT  LEVEL AS n
                    FROM    dual
                    CONNECT BY  LEVEL <= ( SELECT   MAX ( LENGTH (oel1.shipping_instructions) - LENGTH (REPLACE (oel1.shipping_instructions, CHR (10))) ) + 1
                                           FROM     oe_order_lines_all oel1
                                           WHERE    oel1.line_id = c_oe_line_id AND oel1.shipping_instructions IS NOT NULL
                                         )
                )
/*  -- Version# 17.0 Commented Start >    , cntr2     AS
                (
                    SELECT  LEVEL AS n
                    FROM    dual
                    CONNECT BY  LEVEL <= ( SELECT   MAX ( LENGTH (oel1.packing_instructions) - LENGTH (REPLACE (oel1.packing_instructions, CHR (10))) ) + 1
                                           FROM     oe_order_lines_all oel1
                                           WHERE    oel1.line_id = c_oe_line_id AND oel1.packing_instructions IS NOT NULL
                                         )
                )    */ -- Version# 17.0 Commented End <
         -- 01/09/13 CG: Added for performance
-- Version# 18.0 > Start
         SELECT line_id, 
                1 gsa_lvl, 
                'COUNTRY_CODE'                                                                                                 country_code,
                XXWC_OM_CFD_PKG.GET_COUNTRY_OF_ORIGIN(ool.invoice_to_org_id, ool.inventory_item_id)     om_note,
                rownum                                                                                                         rid
           FROM oe_order_lines_all                                                                                             ool
          WHERE 1 = 1
            AND ool.line_id                  = c_oe_line_id
         UNION
-- Version# 18.0 < End
         select oola.line_id
              , 2 SN_LVL
--              , wdd.attribute1 serial_number_list
--              , ('SN: '||wdd.attribute1) om_note
              , oola.attribute7 serial_number_list
              , ('Serial Number: '||oola.attribute7) om_note
              , 1 rid                                         
         from   oe_order_lines_all oola
         where  oola.line_id = c_oe_line_id
         and    oola.attribute7 is not null
         UNION
         -- 01/09/13 CG: Changed for performance
         select oel2.line_id
              , 3 ship_lvl
              , oel2.shipping_instructions
              , REGEXP_SUBSTR (shipping_instructions
                                , '[^' || CHR (10) || ']+'
                                , 1
                                , n) om_note
              , n rid
           FROM oe_order_lines_all oel2
           JOIN cntr1 ON cntr1.n <= LENGTH (oel2.shipping_instructions) - LENGTH( REPLACE(oel2.shipping_instructions, CHR(10) ) ) + 1
         AND   oel2.line_id = c_oe_line_id AND oel2.shipping_instructions IS NOT NULL  
/*       UNION  -- Version# 17.0 -- Commented > Start
         -- 01/09/13 CG: Changed for performance
         select oel2.line_id
                , 4 pck_lvl
                , oel2.packing_instructions
                , REGEXP_SUBSTR (packing_instructions
                                  , '[^' || CHR (10) || ']+'
                                  , 1
                                  , n) om_note
                , n rid
         FROM   oe_order_lines_all oel2
         JOIN   cntr2 ON cntr2.n <= LENGTH (oel2.packing_instructions) - LENGTH( REPLACE(oel2.packing_instructions, CHR(10) ) ) + 1
         AND    oel2.line_id = c_oe_line_id AND oel2.packing_instructions IS NOT NULL 
         -- Version# 17.0 -- Commented < End
*/         UNION
         -- 12/21/12 CG: Added for CP# 2001/ TMS 20121217-00744
             SELECT line_id, 
                    5 gsa_lvl, 
                    attribute14 gsa_note,
                    attribute14 om_note,
                    /*REGEXP_SUBSTR (attribute14,
                                   '[^' || CHR (10) || ']+',
                                   1,
                                   LEVEL
                                  ) om_note,*/
                    -- LEVEL rid
                    rownum rid
               FROM oe_order_lines_all
              WHERE attribute14 IS NOT NULL
                AND line_id = c_oe_line_id
         /*CONNECT BY LEVEL <=
                         LENGTH (attribute14)
                       - LENGTH (REPLACE (attribute14, CHR (10)))
                       + 1
                AND line_id = PRIOR line_id
                AND PRIOR DBMS_RANDOM.VALUE IS NOT NULL*/
           ORDER BY 2, 5;

      -- 10/11/2012 CG: Added for CP #923 to add repair tool information
      -- 10/11/2012 CG: Added another parameter for the invoice_id 
      -- to pull the CM applications
      CURSOR cur_repair_info (c_oe_line_id NUMBER, c_invoice_id NUMBER, c_invoice_line_id NUMBER) is -- Version# 16.0
      -- 10/11/2012 CG: Added
         select app.customer_trx_id
                , 1 CM_APP_LVL
                , ('Inv '||rcta.trx_number||' amt '||TO_CHAR(app.amount_applied,'FM9999999.90')) App_Data
                , ('Applied to '||rcta.trx_number||' $'||TO_CHAR(app.amount_applied,'FM9999999.90')) om_note
                , rownum rid
             from ar_receivable_applications_all app
                , ra_customer_trx_all            rcta
         where  app.application_type = 'CM'
         and    app.display = 'Y'
         and    app.customer_trx_id = c_invoice_id
         and    app.applied_customer_trx_id = rcta.customer_trx_id
         UNION -- Version# 16.0 > Start
         select rctl_r.customer_trx_id
              , 1 CM_APP_LVL
              ,  ('Inv '||rctl_r.customer_trx_id) App_Data
              , ('Reference Invoice# '||rcta.trx_number) om_note
              , rownum rid
         from oe_order_lines_all        ool
            , ra_customer_trx_lines_all rctl
            , ra_customer_trx_lines_all rctl_r
            , ra_customer_trx_all       rcta
        where 1 = 1
          and ool.line_type_id               = 1008                                 -- CREDIT ONLY
          and rctl_r.customer_trx_id         = c_invoice_id
          and rctl_r.customer_trx_line_id    = c_invoice_line_id
          and to_char(ool.LINE_ID)           = rctl_r.interface_line_attribute6
          and to_char(ool.REFERENCE_LINE_ID) = rctl.interface_line_attribute6
          and rcta.customer_trx_id           = rctl.customer_trx_id
          and NOT EXISTS (SELECT '1' FROM ar_receivable_applications_all app
                 WHERE 1 = 1
                   AND app.application_type = 'CM'
                   AND app.display          = 'Y'
                   AND app.customer_trx_id  = rctl_r.customer_trx_id
                          )
          -- Version# 16.0 < End
         UNION
         SELECT oel.line_id
               , 2
               , msi.concatenated_segments repair_item
               , (csl1.meaning||' billing for part '||msi.concatenated_segments||' on SR '||cia.incident_number||' and Repair '||cr.repair_number) om_note       
               , cral.repair_actual_line_id rid       
          FROM   csd_repairs cr
               , cs_incidents_all_b cia
               , cs_estimate_details ced
               , csd_repair_actuals cra
               , csd_repair_actual_lines cral
               , oe_order_headers_all oeh
               , oe_order_lines_all oel
               , mtl_system_items_vl msi
               , cs_transaction_types_vl stt
               , cs_txn_billing_types sbt
               , cs_lookups csl
               , cs_billing_type_categories cbtc
               , cs_lookups csl1
               , mtl_system_items_vl msi1
         WHERE         cr.incident_id = cia.incident_id
                 AND cr.repair_line_id = cra.repair_line_id
                 AND cra.repair_actual_id = cral.repair_actual_id
                 AND cral.estimate_detail_id = ced.estimate_detail_id
                 AND ced.original_source_code = 'DR'
                 AND cr.inventory_item_id = msi.inventory_item_id
                 AND msi.organization_id = cs_std.get_item_valdn_orgzn_id
                 AND sbt.txn_billing_type_id = ced.txn_billing_type_id
                 AND sbt.transaction_type_id = stt.transaction_type_id
                 AND ced.order_header_id = oeh.header_id(+)
                 AND ced.order_line_id = oel.line_id(+)
                 AND csl.lookup_type = 'CS_BILLING_CATEGORY'
                 AND csl.lookup_code = cbtc.billing_category
                 AND csl1.lookup_type = 'MTL_SERVICE_BILLABLE_FLAG'
                 AND csl1.lookup_code = cbtc.billing_type
                 AND sbt.billing_type = cbtc.billing_type
                 AND csl.enabled_flag = 'Y'
                 AND TRUNC (SYSDATE) BETWEEN TRUNC ( NVL (csl.start_date_active, SYSDATE)  )
                                         AND  TRUNC ( NVL (csl.end_date_active, SYSDATE) )
                 AND cral.replaced_item_id = msi1.inventory_item_id(+)
                 AND msi1.organization_id(+) = cs_std.get_item_valdn_orgzn_id
                 AND oel.line_id = c_oe_line_id  
        Order by 2,5;

-- Version# 15 > Start
      CURSOR cur_oracle_rent_note (c_oe_num NUMBER, c_trx_id NUMBER) -- Version# 18.0
      IS
         WITH    cntr1     AS
                (
                    SELECT  LEVEL AS n
                    FROM    dual
                    CONNECT BY  LEVEL <= ( SELECT   MAX ( LENGTH (oeh1.shipping_instructions) - LENGTH (REPLACE (oeh1.shipping_instructions, CHR (10))) ) + 1
                                           FROM     oe_order_headers_all oeh1
                                           WHERE    oeh1.order_number = c_oe_num AND oeh1.shipping_instructions IS NOT NULL
                                         )
                )
               , cntr2     AS
                (
                    SELECT  LEVEL AS n
                    FROM    dual
                    CONNECT BY  LEVEL <= ( SELECT   MAX ( LENGTH (cr.problem_description) - LENGTH (REPLACE (cr.problem_description, CHR (10))) ) + 1
                                             FROM cs_estimate_details  ced
                                                , csd_repairs          cr
                                                , oe_order_headers_all ooh
                                            WHERE   1 = 1
                                              AND ooh.order_number         = c_oe_num
                                              AND ced.order_header_id      = ooh.header_id 
                                              AND ced.original_source_id   = cr.repair_line_id
                                              AND ced.original_source_code = 'DR'
                                              AND cr.problem_description IS NOT NULL
                                              AND rownum = 1
                                         )
                ) 
         select oeh2.header_id
                , oeh2.shipping_instructions
                , DECODE(rownum,1,'Shipping Notes: ','')||REGEXP_SUBSTR (shipping_instructions
                                  , '[^' || CHR (10) || ']+'
                                  , 1
                                  , n) om_note
                , rownum rid
         FROM   oe_order_headers_all oeh2
         JOIN   cntr1 ON cntr1.n <= LENGTH (oeh2.shipping_instructions) - LENGTH( REPLACE(oeh2.shipping_instructions, CHR(10) ) ) + 1
         AND    oeh2.order_number = c_oe_num AND oeh2.shipping_instructions IS NOT NULL  
         UNION
--         (SELECT 1 header_id
--              , cr.problem_description
--              , REGEXP_SUBSTR (problem_description
--                                , '[^' || CHR (10) || ']+'
--                                , 1
--                                , n) om_note
--              , n rid
--           FROM csd_repairs cr
--           JOIN cntr2 ON cntr2.n <= LENGTH (cr.problem_description) - LENGTH( REPLACE(cr.problem_description, CHR(10) ) ) + 1
--            AND cr.problem_description IS NOT NULL
--            AND rownum = 1)
         (SELECT cr2.header_id
              , cr2.problem_description
              , DECODE(rownum,1,'Repair Notes: ','')||REGEXP_SUBSTR (cr2.problem_description
                                , '[^' || CHR (10) || ']+'
                                , 1
                                , n) om_note
              , rownum rid
           FROM (SELECT ooh.header_id
                      , cr.problem_description 
                   FROM cs_estimate_details ced
                      , csd_repairs cr
                      , oe_order_headers_all ooh
                  WHERE 1 = 1
                    AND ooh.order_number         = c_oe_num
                    AND ced.order_header_id      = ooh.header_id 
                    AND ced.original_source_id   = cr.repair_line_id
                    AND ced.original_source_code = 'DR'
                    AND cr.problem_description IS NOT NULL
                    AND rownum = 1) cr2
           JOIN cntr2 ON cntr2.n <= LENGTH (cr2.problem_description) - LENGTH( REPLACE(cr2.problem_description, CHR(10) ) ) + 1)
-- Version# 18.0 > Start
           UNION
               ( SELECT c_trx_id
                      , 'Delivery_Id' Delivery_Id
                      , get_delivery_id(c_trx_id) om_note
                      , 1 rid
                   FROM dual)
-- Version# 18.0 < End
      ORDER BY 1, 4;
      
-- Version# 15 < End

      /*CURSOR cur_oracle_ln_note (
         c_invoice_line_id NUMBER)
      IS
             SELECT invoice_line_id
                   ,1 ship_lvl
                   ,shipping_instructions
                   ,REGEXP_SUBSTR (shipping_instructions
                                  ,'[^' || CHR (10) || ']+'
                                  ,1
                                  ,LEVEL)
                       om_note
                   ,LEVEL rid
               FROM xxwc_bill_trust_inv_lines_vw
              WHERE     shipping_instructions IS NOT NULL
                    AND invoice_line_id = c_invoice_line_id
         CONNECT BY     LEVEL <=
                             LENGTH (shipping_instructions)
                           - LENGTH (
                                REPLACE (shipping_instructions, CHR (10)))
                           + 1
                    AND invoice_line_id = PRIOR invoice_line_id
                    AND PRIOR DBMS_RANDOM.VALUE IS NOT NULL
         UNION
             SELECT invoice_line_id
                   ,2 pck_lvl
                   ,packing_instructions
                   ,REGEXP_SUBSTR (packing_instructions
                                  ,'[^' || CHR (10) || ']+'
                                  ,1
                                  ,LEVEL)
                       om_note
                   ,LEVEL rid
               FROM xxwc_bill_trust_inv_lines_vw
              WHERE     packing_instructions IS NOT NULL
                    AND invoice_line_id = c_invoice_line_id
         CONNECT BY     LEVEL <=
                             LENGTH (packing_instructions)
                           - LENGTH (
                                REPLACE (packing_instructions, CHR (10)))
                           + 1
                    AND invoice_line_id = PRIOR invoice_line_id
                    AND PRIOR DBMS_RANDOM.VALUE IS NOT NULL
         ORDER BY 1, 2, 5;*/

      /*  -- 06/08/2012: Changed to pull from a different notes table  (line Level)
        select  distinct(trx_number)
                , line_number
                , sequence_number
                , description prism_line_note
        from    xxwc.xxwc_ar_prism_inv_notes_tbl x1
        where   x1.trx_number = c_trx_number
        and     to_char(x1.line_number) = c_line_number
        and     x1.description is not null
        and     nvl(x1.print_flag, 'N') = 'Y'
        order by x1.sequence_number;*/

      -- Cursor to pull messages? notes located elsewheree
      v_file                UTL_FILE.file_type;
      l_filename            VARCHAR2 (240);
      l_from_date           DATE;
      l_to_date             DATE;
      l_cur_date            VARCHAR2 (240);
      l_count_hdrs          NUMBER;
      l_count_lns           NUMBER;
      l_prism_notes         VARCHAR2 (2000);
      l_prism_note_ln       NUMBER;
      -- 04/12/2012 Added Rental Data
      l_rental_start_date   DATE;
      l_rental_end_date     DATE;
      l_rental_type         VARCHAR2 (240);
      l_line_number         VARCHAR2 (15);
      l_order_qty           NUMBER;
      l_shipped_qty         NUMBER;
      l_unit_price          VARCHAR2 (240);
      
      -- 12/03/2012 CG: Added for CP #872 Modify credit memo document to show invoices applied through the auto-application process, with a remaining balance of $0
      l_total_amt_applied   NUMBER;
      l_amount_remaining    NUMBER;
      
      -- 12/21/2012 CG: Added for CP#2030/TMS 20121217-00640 Modify tax process to charge a 1% surcharge on commodity items for CA orders
      l_line_tax_amt        NUMBER;
      l_line_tax_rate       NUMBER;
      l_total_tax_amt       NUMBER;
      l_max_line_number     NUMBER;
      l_hdr_tax_amt         NUMBER;
      l_hdr_tax_rate        NUMBER;
      l_item_tax_code       VARCHAR2(240);
      l_rent_note_exists    NUMBER; -- Version# 15
      l_sec                 VARCHAR2(100);
      l_po_line_num         NUMBER; -- Version# 1.3
	  l_po_stg_uom          XXWC.XXWC_B2B_SO_LINES_STG_TBL.UNIT_OF_MEASURE%TYPE; --Ver 1.5
      
   BEGIN
      write_log ('Begining EBS To Bill Trust Open Invoice File Generation ');
      write_log ('========================================================');
      write_log ('  ');

      --Initialize the Out Parameter
      errbuf := '';
      retcode := '0';
      l_from_date := TO_DATE (p_from_date, 'YYYY/MM/DD HH24:MI:SS');
      l_to_date   := TO_DATE (p_to_date, 'YYYY/MM/DD HH24:MI:SS');

      -- Setting current date for file header
      l_cur_date := NULL;

      SELECT TO_CHAR (SYSDATE, 'MM/DD/YYYY')
        INTO l_cur_date
        FROM DUAL;

      -- Displaying parameters passed in the concurrent program log file
      l_sec := 'Displaying parameters passed in the concurrent program log file';
      write_log ('Parameters');
      write_log (RPAD ('File Drop Directory: ', 25, ' ') || p_directory_name);

      write_log (RPAD ('From Date: ', 25, ' ')|| TO_CHAR (l_from_date, 'MM/DD/YYYY'));
      write_log (RPAD ('To Date: ', 25, ' ')|| TO_CHAR (l_to_date, 'MM/DD/YYYY'));

      FOR rec_tp IN cur_trading_partner(l_from_date, l_to_date) LOOP
        EXIT WHEN cur_trading_partner%NOTFOUND;

        SELECT 'WC_'||rec_tp.tp_name||'_'||TO_CHAR (SYSDATE, 'MMDDYYYY_HHMMSS')||'.txt'
          INTO l_filename
          FROM DUAL;

        --Open the file handler
        v_file := UTL_FILE.fopen (location       => p_directory_name,
                                  filename       => l_filename,
                                  open_mode      => 'w');

        write_log (' Writing to the file ... Opening Header Loop - '||rec_tp.tp_name);
        l_count_hdrs := 0;
        l_count_lns := 0;

      l_sec := 'Before c0 cursor';
      FOR c0 IN cur_operating_units
      LOOP
         EXIT WHEN cur_operating_units%NOTFOUND;

-- Version# 1.2 > Start
/*
         -- Writing File Header
         l_sec := 'Writing File Header';
         UTL_FILE.put_line (v_file,
                            UPPER (   'FH'
                                   || '|'
                                   || l_cur_date
                                   || '|'
                                   || 'WCI'
                                   || '|'
                                   || c0.org_name
                                   || '|'
                                   || c0.address_line_1
                                   || '|'
                                   || c0.town_or_city
                                   || '|'
                                   || c0.region_2
                                   || '|'
                                   || c0.postal_code
                                   || '|'
                                   || c0.region_1
                                   || '|'
                                   || '|'
                                  --02/20/12 CG Commented per UAT1 Issue #50
                                  --c0.country||'|'
                                  )
                           );
*/ 
-- Version# 1.2 < End

         FOR c1 IN cur_inv_headers (l_from_date, l_to_date, c0.org_id, rec_tp.tp_name)
         LOOP
            l_sec := 'Inside c1 - '||c1.invoice_id;
            EXIT WHEN cur_inv_headers%NOTFOUND;
            l_count_hdrs := l_count_hdrs + 1;
            -- 04/12/2012 CGonzalez: Added rental data
            l_rental_start_date := NULL;
            l_rental_end_date := NULL;
            l_rental_type := NULL;

            IF    UPPER (c1.order_type) LIKE '%RENTAL%'
               OR UPPER (c1.rental_type) LIKE '%RENTAL%'
            THEN
               BEGIN
                  SELECT x1.rental_ship_date, x1.rental_return_date
                    INTO l_rental_start_date, l_rental_end_date
                    FROM xxwc_bill_trust_inv_lines_vw x1
                   WHERE x1.invoice_id = c1.invoice_id
                     AND x1.rental_ship_date IS NOT NULL
                     AND ROWNUM = 1;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_rental_start_date := NULL;
                     l_rental_end_date := NULL;
               END;

               IF c1.batch_source NOT LIKE 'PRISM%'
               THEN
                  l_rental_type := c1.order_type;
               ELSE
                  l_rental_type := c1.rental_type;
               END IF;
            END IF;

            -- 04/12/2012 CGonzalez: Added rental data

            -- Writting Header Record
            l_sec := 'Writting Header Record';
            UTL_FILE.put_line
               (v_file,
                UPPER
                   (   c1.header_record_type
                    || '|'
                    || c1.invoice_id
                    || '|'
                    || c1.customer_account_number
                    || '|'
                    || c1.customer_name
                    || '|'
                    || c1.remit_to_address_code
                    || '|'
                    || c1.cust_ship_to_number
                    || '|'
                    || NULL
                    || '|'
                    ||  -- c1.cust_ship_to_name||'|'|| -- 03/12/2012 Blank out
                       c1.cust_ship_to_location
                    || '|'
                    || c1.cust_ship_to_address1
                    || '|'
                    || (   c1.cust_ship_to_address2
                        || ' '
                        || c1.cust_ship_to_address3
                       )
                    || '|'
                    || c1.cust_ship_to_city
                    || '|'
                    || c1.cust_ship_to_state_prov
                    || '|'
                    || c1.cust_ship_to_zip_code
                    || '|'
                    ||              --02/20/12 CG Commented per UAT1 Issue #50
                       NULL
                    || '|'
                    ||                        --c1.cust_ship_to_country||'|'||
                       c1.cust_bill_to_number
                    || '|'
                    || NULL
                    || '|'
                    || -- 03/12/2012 Blank out and move to bill to location c1.cust_bill_to_name||'|'||
                       c1.cust_bill_to_name
                    || '|'
                    ||           -- 03/12/2012 c1.cust_bill_to_location||'|'||
                       c1.cust_bill_to_address1
                    || '|'
                    || c1.cust_bill_to_address2
                    || '|'
                    || c1.cust_bill_to_city
                    || '|'
                    || c1.cust_bill_to_state_prov
                    || '|'
                    || c1.cust_bill_to_zip_code
                    || '|'
                    ||              --02/20/12 CG Commented per UAT1 Issue #50
                       NULL
                    || '|'
                    ||                        --c1.cust_bill_to_country||'|'||
                       c1.invoice_number
                    || '|'
                    || TO_CHAR (c1.invoice_date, 'MM/DD/YYYY')
                    || '|'
                    || regexp_replace(c1.po_number,  '[[:cntrl:]]', ' ')  --  Version# 15
                    || '|'
                    || c1.order_number
                    || '|'
                    || c1.order_date
                    || '|'
                    || c1.terms
                    || '|'
                    || c1.ship_via
                    || '|'
                    || c1.ordered_by
                    || '|'
                    || c1.account_manager
                    || '|'
                    || c1.taken_by
                    || '|'
                    || c1.acct_job_no
                    || '|'
                    || c1.customer_job_no
                    || '|'
                    || regexp_replace(c1.received_by,  '[[:cntrl:]]', ' ')     ----added regexp replace by harsha for removing CHRF TMS#20140203-00308 
                    || '|'
                    || c1.transaction_type
                    || '|'
                    || c1.branch_code
                    || '|'
                    || c1.branch_address1
                    || '|'
                    || c1.branch_address2
                    || '|'
                    || c1.branch_city
                    || '|'
                    || c1.branch_state
                    || '|'
                    || c1.branch_zip_code
                    || '|'
                    || c1.branch_region_1
                    || '|'
                    ||              --02/20/12 CG Commented per UAT1 Issue #50
                       NULL
                    || '|'
                    ||                              --c1.branch_country||'|'||
                       c1.branch_phone_number
                    || '|'
                    || '|'                                           /*||'|'||
                                     c1.territory_code*/
                    || c1.whs_branch_name
                    || '|'
                    || l_rental_type
                    || '|'
                    || TO_CHAR (l_rental_start_date, 'MM/DD/YYYY')
                    || '|'
                    || TO_CHAR (l_rental_end_date, 'MM/DD/YYYY')
                   )
               );


/* Version# 17.0 -- Add Header Shipping Notes for all Order Types
            -- Rental Order Header Shipping Notes >> Version# 15 Start
            IF    UPPER (c1.order_type) LIKE '%RENTAL%'
               OR UPPER (c1.rental_type) LIKE '%RENTAL%'
            THEN
*/

            l_rent_note_exists := NULL;
            FOR c1A IN cur_oracle_rent_note (c1.order_number, c1.invoice_id)  -- Version# 18.0
            LOOP
             l_sec := 'Rental Note Cursor - '||c1.invoice_id;
             -- IF c1A.rid = 1 THEN
             
             IF l_rent_note_exists IS NULL AND TRIM(c1A.om_note) IS NOT NULL THEN -- Version# 18.0
               l_rent_note_exists := 1;
             
               UTL_FILE.put_line
                   (v_file,
                    UPPER (   'D'
                           || '|'
                           || c1.invoice_id
                           || '|'
                           || '0'
                           || '|'
                           || '0'
                           || '|'
                           || 'HDRDESC'
                           || '|'
                           || NULL
                           || '|'
                           || NULL
                           || '|'
                           || NULL -- uom_code
                           || '|'
                           || 1 --l_order_qty
                           || '|'
                           || 1 -- l_shipped_qty
                           || '|'
                           || 0 -- c2.qty_backordered
                           || '|'
                           || 0 -- l_unit_price
                           || '|'
                           || 0 -- extended_price
                           || '|'
                           || NULL
                           || '|'
                           || NULL
                           || '|'
                           || NULL
                           || '|'
                           || NULL
                           || '|'
                           || NULL
                           || '|'
                           || NULL
                           || '|'
                           || NULL
                           || '|'
                           || NULL
                           || '|'
                           || 0
                           || '|'
                           || NULL
                           || '|'
                           || NULL
                           || '|'
                           || NULL
                          )
                   );

              UTL_FILE.put_line (v_file,
                              UPPER (   'N'
                                     || '|'
                                     || c1.invoice_id
                                     || '|'
                                     || '0'
                                     || '|'
                                     || '********************************************'
                                     || '|'
                                     || NULL
                                     || '|'
                                    )
                             );
               END IF;

              UTL_FILE.put_line (v_file,
                              UPPER (   'N'
                                     || '|'
                                     || c1.invoice_id
                                     || '|'
                                     || '0'
                                     || '|'
                                     || TRIM (regexp_replace(c1A.om_note,  '[[:cntrl:]]', ' ') ) -- Version# 15
                                     || '|'
                                     || NULL
                                     || '|'
                                    )
                             );

            END LOOP;

             IF l_rent_note_exists = 1 THEN
              l_rent_note_exists := NULL;
              UTL_FILE.put_line (v_file,
                              UPPER (   'N'
                                     || '|'
                                     || c1.invoice_id
                                     || '|'
                                     || '0'
                                     || '|'
                                     || '********************************************'
                                     || '|'
                                     || NULL
                                     || '|'
                                    )
                             );
            END IF;  -- IF c1A.n = 1 THEN

--            END IF; -- IF    UPPER (c1.order_type) LIKE '%RENTAL%'  -- Version# 17.0 -- Add Header Shipping Notes for all Order Types

            -- Rental Order Header Shipping Notes << Version# 15 End

            -- Opening Loop to Pull Invoice Lines
            -- 12/21/2012 CG: Added for CP#2030/TMS 20121217-00640 Modify tax process to charge a 1% surcharge on commodity items for CA orders
            write_log ('Invoice Number: '||c1.invoice_number);
            l_total_tax_amt       := 0;
            l_sec := 'Opening Loop to Pull Invoice Lines ';
            FOR c2 IN cur_inv_lines (c1.invoice_id)
            LOOP
               EXIT WHEN cur_inv_lines%NOTFOUND;
               
               write_log ('c2.line_number ' || c2.line_number);
               write_log ('c2.new_line_number ' || c2.new_line_number);
               
               -- Version# 1.3 > Start
               ------------------------------------------------------------------
               -- Derive PO Line# from B2B PO Staging Table
               ------------------------------------------------------------------
               l_po_line_num := NULL;
               BEGIN
                 SELECT line_number
				       ,unit_of_measure --Ver 1.5
                   INTO l_po_line_num   
				       ,l_po_stg_uom    --Ver 1.5
                   FROM xxwc.xxwc_b2b_so_lines_stg_tbl
                  WHERE 1 = 1
                    AND customer_po_number = c1.po_number
                    AND inventory_item     = c2.part_number;
               EXCEPTION
               WHEN OTHERS THEN
                 l_po_line_num := c2.new_line_number;
				 l_po_stg_uom  := c2.uom_code; --Ver 1.5
               END;
               -- Version# 1.3 < End

               l_count_lns := l_count_lns + 1;
               l_order_qty := NULL;
               l_shipped_qty := NULL;
               l_line_number := NULL;

               IF UPPER (c2.part_number) = 'HDRSHIP'
               THEN
                  l_order_qty := 1;
                  l_shipped_qty := 1;
               ELSE
                  l_order_qty := c2.qty_ord;
                  l_shipped_qty := c2.qty_shipped;
               END IF;

               -- 08/15/2012 CG: Changed to accomodate request for dynamic precision
               -- of unit price based on data, with a minimum of 2 dec places and
               -- a max of 5, without trailling 0s.
               l_unit_price := NULL;

               BEGIN
                  SELECT
                 --TRIM (TO_CHAR (price, '999,999,990.0000')) unit_price_now,
                         (CASE
                             WHEN (LENGTH (SUBSTR (TO_CHAR (c2.price),
                                                     INSTR (TO_CHAR (c2.price),
                                                            '.'
                                                           )
                                                   + 1
                                                  )
                                          )
                                  ) <= 2
                                THEN TRIM (TO_CHAR (c2.price,
                                                    -- '999,999,990.00')  -- Ver#1.6
												   '999999990.00')  -- Ver#1.6
                                          )
                             WHEN (LENGTH (SUBSTR (TO_CHAR (c2.price),
                                                     INSTR (TO_CHAR (c2.price),
                                                            '.'
                                                           )
                                                   + 1
                                                  )
                                          )
                                  ) = 3
                                THEN TRIM (TO_CHAR (c2.price,
                                                    -- '999,999,990.000'  -- Ver#1.6
													'999999990.000' -- Ver#1.6
                                                   )
                                          )
                             WHEN (LENGTH (SUBSTR (TO_CHAR (c2.price),
                                                     INSTR (TO_CHAR (c2.price),
                                                            '.'
                                                           )
                                                   + 1
                                                  )
                                          )
                                  ) = 4
                                THEN TRIM (TO_CHAR (c2.price,
                                                   --   '999,999,990.0000'  -- Ver#1.6
													'999999990.0000'  -- Ver#1.6
                                                   )
                                          )
                             ELSE TRIM (TO_CHAR (c2.price,
                                                 -- '999,999,990.00000')  -- Ver#1.6
												 '999999990.00000')  -- Ver#1.6
                                       )
                          END
                         ) unit_price_going_fwd
                    INTO l_unit_price
                    FROM DUAL;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_unit_price :=
                               --   TRIM (TO_CHAR (c2.price, '999,999,990.00000')); -- Ver#1.6
							  TRIM (TO_CHAR (c2.price, '999999990.00000')); -- Ver#1.6
               END;

               -- 12/21/2012 CG: Added for CP#2030/TMS 20121217-00640 Modify tax process to charge a 1% surcharge on commodity items for CA orders
               l_line_tax_amt        := 0;
               l_line_tax_rate       := 0;
               IF c1.trx_ship_state = 'CA' or c1.pr_ship_state = 'CA' 
               THEN
                   write_log ('Tax Test - State: '||c1.trx_ship_state ||' - '||c1.pr_ship_state);
                   
                   l_item_tax_code := null;
                   begin
                    select  msib.attribute22
                    into    l_item_tax_code
                    from    mtl_system_items_b msib
                            , mtl_parameters mp
                    where   msib.segment1 = c2.part_number
                    and     msib.organization_id = mp.master_organization_id -- mp.organization_id 
                    and     mp.organization_code = c2.branch_code
                    and     rownum = 1;
                   exception
                   when others then
                    l_item_tax_code := null;
                   end;
                   
                   write_log ('Item - tax code: '||c2.part_number||' - '||l_item_tax_code);
                   
                   IF nvl(l_item_tax_code,'UNKNOWN') = '76431' 
                   THEN
                       l_line_tax_amt        := 0;
                       l_line_tax_rate       := 0;
                       begin               
                        select  tax.taxamt
                                , tax.taxrate*100
                        into    l_line_tax_amt
                                , l_line_tax_rate                    
                        from    ra_customer_trx_lines_all trx_ln
                                , ra_customer_trx_lines_all tx_ln
                                , taxware.taxaudit_header hdr
                                , taxware.taxaudit_tax tax
                        where   trx_ln.customer_trx_line_id = c2.invoice_line_id
                        and     trx_ln.customer_trx_line_id = tx_ln.link_to_cust_trx_line_id
                        and     tx_ln.line_type = 'TAX'
                        and     tx_ln.customer_trx_id = hdr.oracleid
                        and     hdr.headerno = tax.headerno
                        and     tx_ln.line_number = tax.detailno
                        and     trx_ln.extended_amount = tax.baseamt
                        and     tax.taxlevel = 'SS';
                       exception
                       when others then
                        l_line_tax_amt        := 0;
                        l_line_tax_rate       := 0;
                       end;
                        
                       IF NVL(l_line_tax_amt,0) > 0 then
                        l_total_tax_amt := l_total_tax_amt + l_line_tax_amt;               
                       END IF;
                   END IF;   
               END IF;
               l_max_line_number := c2.line_number;
               
               write_log ('Line Tax - Total Tax (CA 1): '||l_line_tax_amt||' - '||l_total_tax_amt);
               -- 12/21/2012 CG: Added for CP#2030/TMS 20121217-00640
               
               -- 06/08/2012 CG Modified number format to contain commas
               l_sec := 'Writing Invoice Line details ';
               UTL_FILE.put_line
                   (v_file,
                    UPPER (   c2.line_record_type
                           || '|'
                           || c2.invoice_id
                           || '|'
                           || c2.invoice_line_id
                           || '|'
                           -- 02/18/2013 CGonzalez: Changed to sort with the new line number (OM LIne number and Prism Line Number)
                           -- || c2.line_number
                           -- || c2.new_line_number --Version# 1.3
                           || l_po_line_num -- Version# 1.3
                           || '|'
                           || c2.part_number
                           || '|'
                           || c2.part_description
                           || '|'
                           || c2.branch
                           || '|'
                           || l_po_stg_uom --c2.uom_code  --Ver 1.5
                           || '|'
                           ||       -- 06/08/2012 CG Modified to accom freight
                                    -- c2.qty_ord||'|'||
                                    -- c2.qty_shipped||'|'||
                              l_order_qty
                           || '|'
                           || l_shipped_qty
                           || '|'
                           || c2.qty_backordered
                           || '|'
                           -- 08/15/2012 CG: Changed to accomodate request for dynamic precision
                           -- of unit price based on data, with a minimum of 2 dec places and
                           -- a max of 5, without trailling 0s.
                           -- || TRIM (TO_CHAR (c2.price, '999,999,990.0000'))
                           || l_unit_price
                           || '|'
                           || TRIM (TO_CHAR (c2.extended_price,
                                             --  '999,999,990.00'  -- Ver#1.6
										    '999999990.00'  -- Ver#1.6
                                            )
                                   )
                           || '|'
                           || c2.branch_address1
                           || '|'
                           || c2.branch_address2
                           || '|'
                           || c2.branch_city
                           || '|'
                           || c2.branch_state
                           || '|'
                           || c2.branch_zip_code
                           || '|'
                           || c2.branch_region_1
                           || '|'
                           ||       --02/20/12 CG Commented per UAT1 Issue #50
                              NULL
                           || '|'
                           ||                       --c2.branch_country||'|'||
                              c1.branch_phone_number
                           || '|'
                           || TRIM (TO_CHAR (c2.discount_amount,
                                              --  '999,999,990.00'  -- Ver#1.6
											 '999999990.00'-- Ver#1.6
                                            )
                                   )
                           || '|'
                           || TO_CHAR (c2.discount_date, 'MM/DD/YYYY')
                           || '|'
                           -- 12/21/2012 CG: Added for CP#2030/TMS 20121217-00640: Modified to remove the 1%
                           || trim (to_char( (c2.tax_rate - nvl(l_line_tax_rate,0)) , '990.00') )
                           || '|'
                           ||    -- 02/20/12 CG Added per UAT Issues #
                                 -- 02/23/12 CG changed taxable for tax amount
                                 -- c2.taxable
                              -- 12/21/2012 CG: Added for CP#2030/TMS 20121217-00640: Modified to remove the 1%
                               -- TRIM (TO_CHAR ( (c2.tax_amount - nvl(l_line_tax_amt,0)), '999,999,990.00'))   -- Ver#1.6
							 TRIM (TO_CHAR ( (c2.tax_amount - nvl(l_line_tax_amt,0)), '999999990.00'))  -- Ver#1.6
                          )
                   );

                -- 10/11/2012 CG: Added for CP #923 to add repair tool information   
                -- and CP #872 Modify credit memo document to show invoices applied through the auto-application process, with a remaining balance of $0   
                -- CP# 872/ TMS 20121217-00785: to show CM Application
                -- CP# 923/ TMS 20121217-00710: for repair order tool detail  
                IF c1.batch_source NOT LIKE 'PRISM%' then
                    write_log (' Repair Line Info');
                    l_sec := 'Repair Line Info ';
                    for c5 in cur_repair_info (c2.oe_order_line_id, c2.invoice_id, c2.invoice_line_id)  -- Version# 16.0
                    loop                    
                        exit when cur_repair_info%notfound;
                        
                        UTL_FILE.put_line (v_file,
                                        UPPER (   c2.note_record_type
                                               || '|'
                                               || c2.invoice_id
                                               || '|'
                                               -- 02/18/2013 CGonzalez: Changed to sort with the new line number (OM LIne number and Prism Line Number)
                                               -- || c2.line_number
                                               --|| c2.new_line_number -- Version# 1.3
                                               || l_po_line_num -- Version# 1.3
                                               || '|'
                                               || TRIM (regexp_replace(c5.om_note,  '[[:cntrl:]]', ' ') ) -- Version# 15
                                               || '|'
                                               || NULL
                                               || '|'
                                              )
                                       );
                        
                    end loop;
                
                END IF;
                -- 10/11/2012 CG: Added for CP #923

               -- 01/13/2012 Adding line level comments from Order Line
               -- Looking at the known seeded comments type fields
               -- Adding Line Level
               -- 12/21/12 CG: Added for CP# 2001/ TMS 20121217-00744. Removed 
               -- condition to check only for Shipping and Packing Instructions
               /*IF     (   c2.shipping_instructions IS NOT NULL
                       OR c2.packing_instructions IS NOT NULL
                      )
                  -- 04/01/2012 CG: Excluded for PRISM Source
                  AND c1.batch_source NOT LIKE 'PRISM%'
               --or c2.service_txn_comments is not null
               --or c2.revrec_comments is not null)*/
               IF c1.batch_source NOT LIKE 'PRISM%'
               THEN
                  write_log (' Non Prism Note');
                  -- 06/11/2012 CG: Changed to use cursor and pull data
                  -- parsed from the view if they have CRs
                  /*UTL_FILE.PUT_LINE (v_file,UPPER(
                                      c2.note_record_type||'|'||
                                      c2.invoice_id||'|'||
                                      c2.line_number||'|'||
                                      c2.shipping_instructions||'|'||
                                      c2.packing_instructions||'|'
                                      --c2.service_txn_comments||'|'||
                                      --c2.revrec_comments||'|'
                                      )
                                    );*/
                  l_prism_note_ln := 0;

                  -- 08/17/2012 CG: Corrected to pass the order line ID to the expected cursor
                  -- FOR c4 IN cur_oracle_ln_note (c2.invoice_line_id)           
                  l_sec := 'Looping into CUR_ORACLE_LN_NOTE ';
                  FOR c4 IN cur_oracle_ln_note (c2.oe_order_line_id)
                  LOOP
                     EXIT WHEN cur_oracle_ln_note%NOTFOUND;
                     
                     IF c4.gsa_lvl = 1 THEN
                        IF c4.om_note IS NOT NULL THEN
                           UTL_FILE.put_line (v_file,
                                              UPPER (   c2.note_record_type
                                                     || '|'
                                                     || c2.invoice_id
                                                     || '|'
                                                     -- 02/18/2013 CGonzalez: Changed to sort with the new line number (OM LIne number and Prism Line Number)
                                                     -- || c2.line_number
                                                     --|| c2.new_line_number -- Version# 1.3
                                                     || l_po_line_num -- Version# 1.3
                                                     || '|'
                                                     || 'Country of Origin: '||SUBSTR(TRIM (regexp_replace(c4.om_note,  '[[:cntrl:]]', ' ') ),1,1000) -- Version# 15
                                                     || '|'
                                                     || NULL
                                                     || '|'
                                                    --c2.service_txn_comments||'|'||
                                                    --c2.revrec_comments||'|'
                                                    )
                                             );
                        END IF;
                     ELSE
                        UTL_FILE.put_line (v_file,
                                           UPPER (   c2.note_record_type
                                                  || '|'
                                                  || c2.invoice_id
                                                  || '|'
                                                  -- 02/18/2013 CGonzalez: Changed to sort with the new line number (OM LIne number and Prism Line Number)
                                                  -- || c2.line_number
                                                  -- || c2.new_line_number -- Version# 1.3
                                                  || l_po_line_num -- Version# 1.3
                                                  || '|'
                                                  || SUBSTR(TRIM (regexp_replace(c4.om_note,  '[[:cntrl:]]', ' ') ),1,1000) -- Version# 15
                                                  || '|'
                                                  || NULL
                                                  || '|'
                                                 --c2.service_txn_comments||'|'||
                                                 --c2.revrec_comments||'|'
                                                 )
                                          );
                     END IF;
                     l_prism_note_ln := l_prism_note_ln + 1;
                  END LOOP;

                  write_log (   ' Non Prism Note Exit. Printed '
                             || l_prism_note_ln
                             || ' notes'
                            );
               -- 06/11/2012 CG
               ELSIF c1.batch_source LIKE 'PRISM%'
               THEN
                  -- 04/01/2012 pull prism invoice notes from new staging table
                  l_prism_notes := NULL;
                  l_prism_note_ln := 0;
                  write_log (' Prism Note Entry');
                  l_sec := 'Prism Note Entry ';
                  FOR c3 IN cur_prism_ln_note (c1.invoice_number,
                                               c2.prism_line_number
                                              )
                  LOOP
                     EXIT WHEN cur_prism_ln_note%NOTFOUND;
                     -- 06/08/2012 CG: Commenting and moving into the loop
                     --                want to display individual prism notes
                     /*l_prism_notes := l_prism_notes||c3.prism_line_note||' ';
                     l_prism_note_ln := length (l_prism_notes);

                     if l_prism_note_ln >= 2000
                     then
                         l_prism_notes := substr(l_prism_notes, 1, 2000);
                         exit;
                     end if;*/
                     UTL_FILE.put_line (v_file,
                                        UPPER (   c2.note_record_type
                                               || '|'
                                               || c2.invoice_id
                                               || '|'
                                               -- 02/18/2013 CGonzalez: Changed to sort with the new line number (OM LIne number and Prism Line Number)
                                               -- || c2.line_number
                                               -- || c2.new_line_number -- Version# 1.3
                                               || l_po_line_num -- Version# 1.3
                                               || '|'
                                               ||
                                                  -- 06/08/2012 CG Modified to change data source
                                                  --c3.prism_line_note||'|'||
                                                  regexp_replace(c3.description,  '[[:cntrl:]]', ' ')  -- Version# 15
                                               || '|'
                                               || NULL
                                               || '|'
                                              )
                                       );
                     l_prism_note_ln := l_prism_note_ln + 1;
                  END LOOP;

                  write_log (   ' Prism Note Exit. Printed '
                             || l_prism_note_ln
                             || ' notes'
                            );
                   -- 06/08/2012 CG: Commenting and moving into the loop
                   --                want to display individual prism notes
                   /*if l_prism_notes is not null then

                       Write_Log (' Prism Note Print' );
                       UTL_FILE.PUT_LINE (v_file,UPPER(
                                       c2.note_record_type||'|'||
                                       c2.invoice_id||'|'||
                                       c2.line_number||'|'||
                                       l_prism_notes||'|'||
                                       NULL||'|'
                                       )
                                     );

                   end if;*/
               ELSE
                  NULL;                                               -- noop
               END IF;
            
               write_log ('END c2.new_line_number ' || c2.new_line_number);
               
            END LOOP;


            -- 12/03/2012 CG: added for CP #872 Modify credit memo document to show invoices applied through the auto-application process, with a remaining balance of $0
            -- To determine the total amounts applied
            l_sec := 'To determine the total amounts applied ';
            IF c1.batch_source in ('REPAIR OM SOURCE','STANDARD OM SOURCE','ORDER MANAGEMENT')
                AND c1.transaction_type = 'CRM'
            THEN
                l_total_amt_applied := 0;
                l_amount_remaining  := 0;
                 
                begin
                    select sum(app.amount_applied)
                    into   l_total_amt_applied
                    from   ar_receivable_applications_all app
                           , ra_customer_trx_all rcta
                    where  app.application_type = 'CM'
                    and    app.display = 'Y'
                    and    app.customer_trx_id = c1.invoice_id
                    and    app.applied_customer_trx_id = rcta.customer_trx_id;
                exception
                when others then
                    l_total_amt_applied := c1.total_shipping_and_handling;
                end;
                
                l_amount_remaining := c1.amount_due_remaining;
                
            ELSE
                l_total_amt_applied := 0;
                l_amount_remaining  := 0;
                l_total_amt_applied := c1.total_shipping_and_handling;
                l_amount_remaining  := c1.total_invoice;
            END IF;
            -- 12/03/2012 CG

            -- 12/21/2012 CG: Added for CP#2030/TMS 20121217-00640: Modified to remove the 1%
            IF (c1.trx_ship_state = 'CA' or c1.pr_ship_state = 'CA') AND NVL(l_total_tax_amt,0) > 0 THEN
            
                write_log ('New tax line being added');
                l_sec := 'New tax line being added';
                UTL_FILE.put_line
                   (v_file,
                    UPPER (   'D'
                           || '|'
                           || c1.invoice_id
                           || '|'
                           || 99999
                           || '|'
                           || to_char(nvl(l_max_line_number,0)+1)
                           || '|'
                           || '1% ASSESSMENT'
                           || '|'
                           || 'CA STATE 1% LUMBER ASSESSMENT'
                           || '|'
                           || c1.branch_code
                           || '|'
                           || 'EA'
                           || '|'
                           || 1
                           || '|'
                           || 1
                           || '|'
                           || 0
                           || '|'
                            --  || TRIM (TO_CHAR (NVL(l_total_tax_amt,0), '999,999,990.00' )) -- NVL(l_total_tax_amt,0)  -- Ver#1.6
						   || TRIM (TO_CHAR (NVL(l_total_tax_amt,0), '999999990.00' )) -- NVL(l_total_tax_amt,0)  -- Ver#1.6
                           || '|'
                          -- || TRIM (TO_CHAR (NVL(l_total_tax_amt,0), '999,999,990.00' ))  -- Ver#1.6
						   || TRIM (TO_CHAR (NVL(l_total_tax_amt,0), '999999990.00' ))  -- Ver#1.6
                           || '|'
                           || c1.branch_address1
                           || '|'
                           || c1.branch_address2
                           || '|'
                           || c1.branch_city
                           || '|'
                           || c1.branch_state
                           || '|'
                           || c1.branch_zip_code
                           || '|'
                           || c1.branch_region_1
                           || '|'
                           || NULL
                           || '|'
                           || c1.branch_phone_number
                           || '|'
                           || '0.00' --TRIM (TO_CHAR (0, '999,999,990.00' ) )
                           || '|'
                           || TO_DATE ('01/01/1952', 'MM/DD/YYYY')
                           || '|'
                           -- 12/21/2012 CG: Added for CP#2030/TMS 20121217-00640: Modified to remove the 1%
                           || 1
                           || '|'
                           || '0.00' --TRIM (TO_CHAR ( 0, '999,999,990.00'))
                          ));
                  
                l_hdr_tax_amt   := c1.total_tax - l_total_tax_amt;
                -- 03/01/2013 CG: TMS 20130201-01380: Changed to recalc tax rate based on subtotal and tax amount
                -- l_hdr_tax_rate  := c1.tax_rate - 1;
                if nvl(l_hdr_tax_amt, 0) > 0 then
                    begin                    
                        l_hdr_tax_rate := round((l_hdr_tax_amt/c1.total_gross)*100, 2);  
                    exception
                    when others then
                        l_hdr_tax_rate := 0;
                    end;
                else
                    l_hdr_tax_rate := 0;
                end if;
            ELSE
                l_hdr_tax_amt   := c1.total_tax;
                -- 03/01/2013 CG: TMS 20130201-01380: Changed to recalc tax rate based on subtotal and tax amount
                -- l_hdr_tax_rate  := c1.tax_rate;
                if nvl(l_hdr_tax_amt, 0) > 0 then
                    begin                    
                        l_hdr_tax_rate := round((l_hdr_tax_amt/c1.total_gross)*100, 2);  
                    exception
                    when others then
                        l_hdr_tax_rate := 0;
                    end;
                else
                    l_hdr_tax_rate := 0;
                end if;  
            END IF;
            
            -- 12/21/2012 CG: Added for CP#2030/TMS 20121217-00640: Modified to remove the 1%

            write_log (' Total Print');
            -- Writing Total Records
            l_sec := 'Writing Total Records';
            UTL_FILE.put_line
                      (v_file,
                       UPPER (   c1.total_record_type
                              || '|'
                              || c1.invoice_id
                              || '|'
                              || TRIM (TO_CHAR ( (nvl(l_total_tax_amt,0)+c1.total_gross), -- Total Gross
                                                --  '999,999,990.00'  -- Ver#1.6
											   '999999990.00'   -- Ver#1.6
                                               )
                                      )
                              || '|'
                              -- -- 12/21/2012 CG: Added for CP#2030/TMS 20121217-00640: Modified to remove the 1%
                              || TRIM (TO_CHAR ( nvl(l_hdr_tax_amt,0), -- c1.total_tax, -- Total Tax
                                               -- '999,999,990.00')  -- Ver#1.6
												'999999990.00')   -- Ver#1.6
                                      )
                              || '|'
                              -- 12/03/2012 CG: added for CP #872 (TMS 20121217-00785) Changed to l_total_amt_applied from c1.total_shipping_and_handling
                              /*|| TRIM
                                     (TO_CHAR (c1.total_shipping_and_handling,
                                               '999,999,990.00'
                                              )
                                     )*/
                              || TRIM
                                     (TO_CHAR (l_total_amt_applied,         -- Total Shipping -n- Handling Invoice - Amount Applied for CM
                                                -- '999,999,990.00'   -- Ver#1.6
											    '999999990.00' -- Ver#1.6
                                              )
                                     )
                              || '|'
                              -- 02/15/2013 CG: Changed to pull the amount due remaining for CMs and Invoice total for all others
                              /*|| TRIM (TO_CHAR (c1.total_invoice,
                                                '999,999,990.00'
                                               )
                                      )*/
                              || TRIM (TO_CHAR (l_amount_remaining,
                                                -- '999,999,990.00'   -- Ver#1.6
												'999999990.00'  -- Ver#1.6
                                               )
                                      )
                              || '|'
                              -- 12/03/2012 CG: added for CP #872 Changed to l_total_amt_applied from c1.total_shipping_and_handling
                              || l_hdr_tax_rate -- c1.tax_rate
                             )
                      );
            -- Opening Loop to Pull Invoice Messages/Comments at Invoice Header Level
            -- Per Email 01/12/2012 Notes will be at the Order Line Level in a seeded field
            /*UTL_FILE.PUT_LINE (v_file,
                               'N'||'|'||
                               c1.invoice_id||'|'||
                               'NO MESSAGE CURSOR FOR INVOICE HEADER!!!'
                              );*/

            -- 03/23/2012 CGonzalez: Added portion to update send date for BT Transactions
            write_log (' Trx Update');

            -- Version# 1.1 > Start
            l_sec := 'Updating Invoice DFF with the Invoice sent date';
            BEGIN
               UPDATE ra_customer_trx_all
                  SET attribute10 = TO_CHAR (SYSDATE, 'MM/DD/YYYY')
                      , attribute_category = c1.org_id
                WHERE customer_trx_id = c1.invoice_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  write_log
                     (   '  Could not update B2B Invoice Print Date for Invoice '
                      || c1.invoice_number
                     );
            END;
            -- Version# 1.1 < End

         END LOOP;
-- Version# 1.2 > Start
/*
         -- Writing End of File per Operating Unit
         l_sec := 'Writing End of File per Operating Unit';
         UTL_FILE.put_line (v_file,
                               'FE'
                            || '|'
                            || 'END OF COMPANY DATA FILE'
                            || '|'
                            || 'WCI'
                            || '|'
                           );
*/                           
-- Version# 1.2 < End

      END LOOP; -- cur_operating_units

      -- Control numbers in log file for concurrent program
      write_log ('  ');
      write_log (   ' Exported '
                 || l_count_hdrs
                 || ' invoices and '
                 || l_count_lns
                 || ' invoice lines.'
                );
      write_log (' Closing the the file ...');
      --Closing the file handler
      UTL_FILE.fclose (v_file);

      END LOOP; -- cur_trading_partner

      COMMIT;
   EXCEPTION
      WHEN UTL_FILE.invalid_path
      THEN
         UTL_FILE.fclose (v_file);
         errbuf := 'File Path is invalid.';
         retcode := '2';
         write_error (errbuf);
         write_log (errbuf);
      WHEN UTL_FILE.invalid_mode
      THEN
         UTL_FILE.fclose (v_file);
         errbuf := 'The open_mode parameter in FOPEN is invalid.';
         retcode := '2';
         write_error (errbuf);
         write_log (errbuf);
      WHEN UTL_FILE.invalid_filehandle
      THEN
         UTL_FILE.fclose (v_file);
         errbuf := 'File handle is invalid..';
         retcode := '2';
         write_error (errbuf);
         write_log (errbuf);
      WHEN UTL_FILE.invalid_operation
      THEN
         UTL_FILE.fclose (v_file);
         errbuf := 'File could not be opened or operated on as requested';
         retcode := '2';
         write_error (errbuf);
         write_log (errbuf);
      WHEN UTL_FILE.write_error
      THEN
         UTL_FILE.fclose (v_file);
         errbuf :=
                 'Operating system error occurred during the write operation';
         retcode := '2';
         write_error (errbuf);
         write_log (errbuf);
      WHEN UTL_FILE.internal_error
      THEN
         UTL_FILE.fclose (v_file);
         write_log ('Unspecified PL/SQL error.');
      WHEN UTL_FILE.file_open
      THEN
         UTL_FILE.fclose (v_file);
         errbuf := 'The requested operation failed because the file is open.';
         retcode := '2';
         write_error (errbuf);
         write_log (errbuf);
      WHEN UTL_FILE.invalid_filename
      THEN
         UTL_FILE.fclose (v_file);
         errbuf := 'The filename parameter is invalid.';
         retcode := '2';
         write_error (errbuf);
         write_log (errbuf);
      WHEN UTL_FILE.access_denied
      THEN
         UTL_FILE.fclose (v_file);
         errbuf := 'Permission to access to the file location is denied.';
         retcode := '2';
         write_error (errbuf);
         write_log (errbuf);
      WHEN OTHERS
      THEN
         UTL_FILE.fclose (v_file);
         errbuf := 'Error Msg :' || SQLERRM;
         retcode := '2';
         write_error (errbuf);
         write_log (errbuf);

      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => 'XXWC_B2B_INV_INTF_PKG.GEN_OPEN_INVOICE_FILE'
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(SQLERRM, 1, 2000)
                                          ,p_error_desc        => 'Error in B2B Invoice GEN_OPEN_INVOICE_FILE procedure, When Others Exception'
                                          ,p_distribution_list => g_dflt_email
                                          ,p_module            => 'XXWC');
   END gen_open_invoice_file;

-- Version# 18.0 > Start
   -- Function to derive DeliveryId detals
   FUNCTION get_delivery_id (p_cust_trx_id IN NUMBER) return VARCHAR2
   IS
      l_del_id    VARCHAR2 (200);
      l_row_cnt   NUMBER := 1;
   BEGIN
     FOR rec_del IN (SELECT distinct stg.delivery_id  
                       FROM ra_Customer_Trx_lines_all rcta
                          , xxwc_wsh_shipping_stg     stg
                      WHERE interface_line_attribute2 = 'STANDARD ORDER'
                        AND INTERFACE_LINE_ATTRIBUTE6 = stg.line_id
                        AND rcta.customer_trx_id      = p_cust_trx_id) 
     LOOP
       
       IF l_row_cnt = 1 THEN
         l_del_id  := 'Delivery Tag#: '||rec_del.delivery_id;
         l_row_cnt := l_row_cnt + 1;
       ELSE
         l_del_id  := l_del_id||' , '||rec_del.delivery_id;
       END IF;
     END LOOP;   
  
     RETURN l_del_id;
   END get_delivery_id;
-- Version# 18.0 < End

-- Version# 1.4 > Start
   /*************************************************************************
     Function: get_invoice_count

     PURPOSE:   To derive Invoice Counts for B2B Stats Page

     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.4        05/16/2016  Gopi Damuluri           TMS# 20160516-00148 - Issue with B2B Stats showing Tax Amounts
   ************************************************************************/
   FUNCTION get_invoice_count (p_cust_acount_id IN NUMBER
                             , p_start_Date     IN DATE
                             , p_end_date       IN DATE
                              ) return NUMBER
   IS
      l_inv_cnt   NUMBER := 0;
   BEGIN
     SELECT COUNT(1)
       INTO l_inv_cnt
       FROM ra_customer_trx_all                rcta
          , ar_payment_schedules_all           apsa
          , oe_order_headers_all               ooha
      WHERE 1 = 1
        AND ooha.sold_to_org_id              = p_cust_acount_id
        AND ooha.order_source_id             = 1021
        AND rcta.interface_header_attribute1 = TO_CHAR (ooha.order_number)
        AND TRUNC(rcta.trx_date)       BETWEEN p_start_Date AND p_end_date
        AND rcta.org_id                      = 162
        AND apsa.customer_trx_id             = rcta.customer_trx_id;

     RETURN l_inv_cnt;
   EXCEPTION
   WHEN OTHERS THEN
     RETURN l_inv_cnt;
   END get_invoice_count;
   
   /*************************************************************************
     Function: get_invoice_amt

     PURPOSE:   To derive Invoice Amounts for B2B Stats Page

     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.4        05/16/2016  Gopi Damuluri           TMS# 20160516-00148 - Issue with B2B Stats showing Tax Amounts
   ************************************************************************/
   FUNCTION get_invoice_amt (p_cust_acount_id IN NUMBER
                           , p_start_Date     IN DATE
                           , p_end_date       IN DATE
                            ) return NUMBER
   IS
      l_inv_amt   NUMBER := 0;
   BEGIN
     SELECT SUM(amount_line_items_original)
       INTO l_inv_amt
       FROM ra_customer_trx_all                rcta
          , ar_payment_schedules_all           apsa
          , oe_order_headers_all               ooha
      WHERE 1 = 1
        AND ooha.sold_to_org_id              = p_cust_acount_id
        AND ooha.order_source_id             = 1021
        AND rcta.interface_header_attribute1 = TO_CHAR (ooha.order_number)
        AND TRUNC(rcta.trx_date)       BETWEEN p_start_Date AND p_end_date
        AND rcta.org_id                      = 162
        AND apsa.customer_trx_id             = rcta.customer_trx_id;

     RETURN l_inv_amt;
   EXCEPTION
   WHEN OTHERS THEN
     RETURN l_inv_amt;
   END get_invoice_amt;
-- Version# 1.4 < End

END XXWC_B2B_INV_INTF_PKG;
/
