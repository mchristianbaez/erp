/*
 TMS: 20151001-00191
 Date: 10/06/2015
*/
set serveroutput on size 1000000;
declare
 --
 cursor hds_alerts is
 select 
     job_name
    ,remove_user
    ,error_email
    ,warning_email 
    ,rowid alert_rowid
 from xxcus.xxcus_fix_jobs_alert_tmp a
 where 1 =1
 and status ='NEW'; 
 --
 cursor job_info (p_job_name in varchar2) is
      select a.concurrent_program_id, a.application_id, 'SINGLE_REQ', b.user_id, b.owned_by 
      from   apps.fnd_concurrent_programs_tl a
                  ,xxcus.xxcus_monitor_cp_b               b
      where  1 =1
           and  b.request_set_id is null --What this means is we are looking for single request only
           and  a.user_concurrent_program_name =p_job_name
           and  a.concurrent_program_id =b.concurrent_program_id
           and  a.application_id =b.application_id
  union all
      select a.request_set_id, a.application_id, 'REQ_SET', b.user_id, b.owned_by      
      from   apps.fnd_request_sets_tl     a
                  ,xxcus.xxcus_monitor_cp_b  b
      where  1 =1
           and  b.concurrent_program_id is null --What this means is we are looking for request set only
           and  a.user_request_set_name =p_job_name
           and  a.request_set_id =b.request_set_id
           and  a.application_id =b.application_id;
 --
 l_program varchar2(150) :=Null; 
 l_job_type varchar2(20) :=Null;
 l_job_id  number :=0;
 l_owned_by  varchar2(150) :=Null; 
 --
 l_curr_user_id number :=0; 
 l_app_id number :=0;
 l_proceed boolean :=Null;
 l_count number :=0;
 --
 l_sc_userid number :=15985; --Supply chain generic user XXWC_INT_SUPPLYCHAIN
 l_om_userid number :=15986; --Order managenent generic user XXWC_INT_SALESFULFILLMENT
 l_fin_userid number :=1290; --WC Finance generic user XXWC_INT_FINANCE
 --
 l_hdshr_intf_user_id number :=1270; --HDSHRINTERFACE
 l_gl_intf_user_id number :=1229; --GLINTERFACE
 l_hds_ap_intf_user_id number :=1271; --HDSAPINTERFACE
 --
     procedure print_log(p_message in varchar2) is
     begin
      if apps.fnd_global.conc_request_id >0 then
       apps.fnd_file.put_line(fnd_file.log, p_message);
      else
       dbms_output.put_line(p_message);
      end if;
     end;
 --
begin
     --
     print_log('');     
     print_log('Begin update of alert details for 24 * 7 monitoring...');
     print_log('');
     --
     begin
      --
      select count(1)
      into    l_count
      from   all_objects
      where 1=1
      and owner ='XXCUS'
      and object_name ='XXCUS_MONITOR_CP_BKUP_TMP'      
      and object_type ='TABLE';
       --
        if l_count >0 then
         --
          execute immediate 'drop table xxcus.xxcus_monitor_cp_bkup_tmp';           
          --
          execute immediate 'create table xxcus.xxcus_monitor_cp_bkup_tmp as select * from xxcus.xxcus_monitor_cp_b';
          --  
       else
          --
          execute immediate 'create table  xxcus.xxcus_monitor_cp_bkup_tmp as select * from xxcus.xxcus_monitor_cp_b';
          --        
       end if;
       --
     exception
      when others then
       print_log('Fatal error in drop off existing back up table xxcus.xxcus_fix_jobs_alert_tmp'||sqlerrm);
       raise program_error;
     end;
     --
     begin       
       --
       savepoint start_here;
       --
       delete xxcus.xxcus_monitor_cp_b
       where  1 =1
         and concurrent_program_id =48126
         and application_id =602         
         and user_id =1271
         and owned_by ='FINANCE';
       --
       if (sql%rowcount >0) then
         --
         print_log('Removed record with Concurrent_Program_ID: 48126 [Create Accounting] for User_ID: 1271 [HDSAPINTERFACE] and Application_ID: 602 [XLA], rowcount :'||sql%rowcount);
         --       
        commit;
       end if;
       --          
     exception                
      when others then
       print_log('Delete program Create Accounting, error: when others, msg :  '||sqlerrm);
       rollback to start_here;                  
     end;
    --   
     begin       
       --
       savepoint start_here;
       --
       delete xxcus.xxcus_monitor_cp_b
       where  1 =1
         and concurrent_program_id =58387
         and application_id =200         
         and user_id =1271
         and owned_by ='FINANCE';
       --
       if (sql%rowcount >0) then
         --
         print_log('Removed record with Concurrent_Program_ID: 58387 [HDS Credit Card Outstanding Transactions Mgmt (Aging)] for User_ID: 1271 [HDSAPINTERFACE] and Application_ID: 200 [SQLAP], rowcount :'||sql%rowcount);
         --       
        commit;
       end if;
       --          
     exception                
      when others then
       print_log('Delete program HDS Credit Card Outstanding Transactions Mgmt (Aging), error: when others, msg :  '||sqlerrm);
       rollback to start_here;                  
     end;
    --    
     begin    
       --
       savepoint start_here;
       --
       delete xxcus.xxcus_monitor_cp_b
       where  1 =1
         and concurrent_program_id =46403
         and application_id =200         
         and user_id =1271
         and owned_by ='FINANCE';
       --
       if (sql%rowcount >0) then
         --
         print_log('Removed record with Concurrent_Program_ID: 46403 [Visa VCF 4 Transaction Loader and Validation Program] for User_ID: 1271 [HDSAPINTERFACE] and Application_ID: 200 [SQLAP], rowcount :'||sql%rowcount);
         --       
        commit;
       end if;
       --          
     exception                
      when others then
       print_log('Delete program Visa VCF 4 Transaction Loader and Validation Program, error: when others, msg :  '||sqlerrm);
       rollback to start_here;                  
     end;
    --
    for rec in hds_alerts loop
     --     
     l_job_id :=Null;
     l_app_id :=Null;
     l_job_type :=Null; 
     --         
     begin 
          open job_info (rec.job_name);
          fetch job_info into l_job_id, l_app_id, l_job_type, l_curr_user_id, l_owned_by;
          close job_info; 
       --
       if (l_job_id is not null) then
         l_proceed :=TRUE;
       else
         l_proceed :=FALSE;       
       end if;
       --            
     exception
      when no_data_found then
       print_log('Invalid job name : '||rec.job_name);
       l_proceed :=FALSE;
      when too_many_rows then      
       print_log('@ program = '||l_program||', error: when-too many rows');
       l_proceed :=FALSE;                 
      when others then
       print_log('@ program = '||l_program||', error: when others, msg :  '||sqlerrm);
       l_proceed :=FALSE;                  
     end;
        --
        if (l_proceed) then
         --
         if l_job_type ='SINGLE_REQ' then
             --
             begin 
              --
              savepoint start_update;
              --
                         update xxcus.xxcus_monitor_cp_b
                          set user_id =case
                                                    when rec.remove_user ='Y' then Null
                                                    else l_curr_user_id
                                                   end
                                ,error_email_address =rec.error_email
                                ,warning_email_address =rec.warning_email
                          where  1 =1
                                and concurrent_program_id =l_job_id
                                and application_id =l_app_id
                                and owned_by =l_owned_by;
                      --
                        if (sql%rowcount >0) then
                         update xxcus.xxcus_fix_jobs_alert_tmp set status ='PROCESSED' where rowid =rec.alert_rowid;
                        else
                         update xxcus.xxcus_fix_jobs_alert_tmp set status ='FAIL' where rowid =rec.alert_rowid;                
                        end if;            
                      --
             exception
              when others then
               update xxcus.xxcus_fix_jobs_alert_tmp set status ='ERROR' where rowid =rec.alert_rowid;              
               rollback to start_update;
             end;
             --
         elsif l_job_type ='REQ_SET' then
             --
             begin
              --
              savepoint start_update;
              --
                 update xxcus.xxcus_monitor_cp_b
                  set user_id =case
                                            when rec.remove_user ='Y' then Null
                                            else l_curr_user_id
                                           end
                        ,error_email_address =rec.error_email
                        ,warning_email_address =rec.warning_email
                  where  1 =1
                        and request_set_id =l_job_id
                        and application_id =l_app_id
                        and owned_by =l_owned_by;
              --
                if (sql%rowcount >0) then
                 update xxcus.xxcus_fix_jobs_alert_tmp set status ='PROCESSED' where rowid =rec.alert_rowid;
                else
                 update xxcus.xxcus_fix_jobs_alert_tmp set status ='FAIL' where rowid =rec.alert_rowid;                
                end if;            
              --
             exception
              when others then
               update xxcus.xxcus_fix_jobs_alert_tmp set status ='ERROR' where rowid =rec.alert_rowid;              
               rollback to start_update;
             end;
             --       
         else  --l_job_type is other than REQ_SET OR SINGLE_REQ
          -- 
           Null;
          --
         end if;
         --
        else
           update xxcus.xxcus_fix_jobs_alert_tmp set status ='JOB NOT IN ALERT TABLE' where rowid =rec.alert_rowid;        
        end if;
        --    
    end loop;  
    --
     commit;
     --    
     print_log('');     
     print_log('End update of alert details for 24 * 7 monitoring...');
     print_log('');
     --    
exception
 when others then
  print_log('Outer block, message ='||sqlerrm);
end;
/