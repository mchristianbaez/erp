  /*  
  Module: HDS Real Estate [OPN -Oracle Property Manager]
  Author: Balaguru Seshadri
  Date: 01/12/2016
  Ticket: TMS 20160112-00123 / ESMS 313314
  Description:   Add new columns HDS_Feed_to_Extended , HDS_FRU_Loc, HDS_Feed_to_External and fax_number 
               to xxcus.xxcus_opn_fru_loc_all and hds_prop_current_flag field in xxcus.xxcus_opn_properties_all tables 
  */
ALTER TABLE XXCUS.XXCUS_OPN_FRU_LOC_ALL ADD HDS_FRU_LOC VARCHAR2(50);
ALTER TABLE XXCUS.XXCUS_OPN_FRU_LOC_ALL ADD HDS_FEED_TO_EXTENDED VARCHAR2(1);
ALTER TABLE XXCUS.XXCUS_OPN_FRU_LOC_ALL ADD HDS_FEED_TO_EXTERNAL VARCHAR2(1);
ALTER TABLE XXCUS.XXCUS_OPN_FRU_LOC_ALL ADD FAX_NUMBER VARCHAR2(20);
--
COMMENT ON TABLE XXCUS.XXCUS_OPN_FRU_LOC_ALL IS 'TMS 20160112-00123 / ESMS 313314';
--