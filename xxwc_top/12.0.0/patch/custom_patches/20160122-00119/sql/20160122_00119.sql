/********************************************************************************
   $Header 20160122_00119.SQL $
   Module Name: 20160122_00119

   PURPOSE: Update PO#s : 15304, 15354 and 15351 to be not processed.

   REVISIONS:
   Ver        Date        Author                     Description
   ---------  ----------  ---------------         -------------------------
   1.0        01/22/2015  Gopi Damuluri           TMS# 20160122-00119
********************************************************************************/

UPDATE xxwc.xxwc_b2b_so_hdrs_stg_tbl 
set process_flag  = 'N'
  , error_message = NULL
WHERE customer_po_number IN ('15304', '15354', '15351'); 

COMMIT;
/