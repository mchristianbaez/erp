  /*******************************************************************************
  Table: XXWC.XXWC_B2B_POD_CUST_EMAILS_TBL
  Description: XXWC.XXWC_B2B_POD_CUST_EMAILS_TBL
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     18-June-2017        Pahwa Nancy   TMS 20170601-00292 Create table
  ********************************************************************************/
-- Create table
create table XXWC.XXWC_B2B_POD_CUST_EMAILS_TBL
(
  email VARCHAR2(4000),
  flag  VARCHAR2(1) default 'N'
)
tablespace XXWC_DATA
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
/