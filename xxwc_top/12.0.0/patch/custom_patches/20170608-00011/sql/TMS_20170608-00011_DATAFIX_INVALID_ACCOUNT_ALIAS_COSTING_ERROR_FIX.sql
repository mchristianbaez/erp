/******************************************************************************
  $Header TMS_20170608-00011_DATAFIX_INVALID_ACCOUNT_ALIAS_COSTING_ERROR_FIX.sql $
  Module Name:Data Fix script for 20170608-00011

  PURPOSE: Data fix script for 20170608-00011 Invalid/No Account alias for 
           transaction causing costing error.

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    -------------------------------
  1.0        08-JUN-2017  P.Vamshidhar          TMS#20170608-00011

*******************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000


BEGIN
   DBMS_OUTPUT.put_line ('Before Update');

   UPDATE MTL_MATERIAL_TRANSACTIONS
      SET TRANSACTION_SOURCE_ID = 3296,
          costed_flag = 'N',
          transaction_group_id = NULL,
          ERROR_CODE = NULL,
          error_explanation = NULL
    WHERE transaction_id = 585280179 AND organization_id = 240;
	
	 UPDATE MTL_MATERIAL_TRANSACTIONS
      SET TRANSACTION_SOURCE_ID = 7856,
          costed_flag = 'N',
          transaction_group_id = NULL,
          ERROR_CODE = NULL,
          error_explanation = NULL
    WHERE transaction_id = 585207166 AND organization_id = 291;


   DBMS_OUTPUT.put_line ('Records update-' || SQL%ROWCOUNT);
   COMMIT;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Unable to update record ' || SQLERRM);
END;
/