CREATE OR REPLACE PACKAGE BODY apps.xxwc_sf_routines_pkg
AS
   /*************************************************************************
     $Header xxwc_sf_routines_pkg $
     Module Name: xxwc_sf_routines_pkg.pks

     PURPOSE:   This package holds common routines used by Sales and Fulfilment jobs.

     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        07/23/2015  Gopi Damuluri           Initial Version
                                                    TMS# 20150722-00086
     1.1        12/15/2015  Gopi Damuluri           TMS# 20151215-00156
                                                    Pending Pre-Bill issue for sales order lines
   **************************************************************************/

   g_distro_list              VARCHAR2 (75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';


  /********************************************************************************
  -- PROCEDURE: update_ofd_delivery_run
  --
  -- PURPOSE: Used to process Order Lines stuck in PICKED status
  --
  -- HISTORY
  -- ===========================================================================
  -- ===========================================================================
  -- VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- -------------------------------------
  -- 1.0     07/23/2015    Gopi Damuluri   Initial Version
  --                                       TMS# 20150722-00086
  -- 1.1     12/15/2015    Gopi Damuluri   TMS# 20151215-00156
  --                                       Pending Pre-Bill issue for sales order lines
  *******************************************************************************/
   PROCEDURE process_picked_so_lines (p_errbuf      OUT VARCHAR2
                                    , p_retcode     OUT NUMBER
                                    , p_order_num    IN VARCHAR2)
   IS

    l_request_id     NUMBER;
    l_sec            VARCHAR2(100);
    l_error_message  VARCHAR2(240);

    -- API Output Variables
    o_return_status  VARCHAR2(200);
    o_msg_count      NUMBER;
    o_msg_data       VARCHAR2(200);


    ----------------------------------------------------------------
    -- Cursor to extract Deliveries stuck in PICKED status
    ----------------------------------------------------------------
      CURSOR cur_picked
          IS
      SELECT DISTINCT wtp.stop_id          pick_stop_id 
           , wtp.trip_id                   pick_trip_id
           , wtp.stop_location_id          pick_stop_location_id
           , wtp.planned_departure_date    pick_planned_departure_date
           , wtd.stop_location_id          drop_stop_location_id
           , wtd.stop_id                   drop_stop_id
           , wtd.trip_id                   drop_trip_id
           , wtd.planned_departure_date    drop_planned_departure_date
           , ooh.order_number
           , wda.delivery_id
        FROM wsh_trips                     wt
           , wsh_trip_stops                wtp
           , wsh_trip_stops                wtd
           , wsh_delivery_legs             wdl
           , wsh_delivery_assignments      wda
           , wsh_delivery_details          wdd
           , oe_order_lines_all            ool
           , oe_order_headers_all          ooh
       WHERE wt.trip_id                  = wtp.trip_id
         AND wt.trip_id                  = wtd.trip_id
         AND wtp.stop_id                 = wdl.pick_up_stop_id
         AND wtd.stop_id                 = wdl.drop_off_stop_id
         AND wdl.delivery_id             = wda.delivery_id
         AND wdd.source_line_id          = ool.line_id
         AND ooh.header_id               = ool.header_id
         AND wdd.delivery_detail_Id      = wda.delivery_detail_id
         AND ool.flow_status_code        NOT IN ( 'CLOSED', 'CANCELLED', 'ENTERED') 
         AND wdd.released_Status         = 'Y'
         AND (p_order_num IS NULL OR ooh.order_number = p_order_num)
    ORDER BY ooh.order_number,  wda.delivery_id;

BEGIN

        FOR rec IN cur_picked LOOP

             fnd_file.put_line(fnd_file.log, 'Order Number - '||rec.order_number||'         Delivery Id - '||''||rec.delivery_id);

             ---------------------------------------------------------------
             -- Closing Pick-Up Trips
             ---------------------------------------------------------------
             o_return_status := NULL;
             o_msg_count     := NULL;
             o_msg_data      := NULL;

             l_sec := 'Closing Pick-Up Trips';
             WSH_TRIP_STOPS_PUB.Stop_Action
               ( p_api_version_number     => 1.0,
                 p_init_msg_list          => FND_API.G_TRUE,
                 x_return_status          => o_return_status,
                 x_msg_count              => o_msg_count,
                 x_msg_data               => o_msg_data,
                 p_action_code            => 'CLOSE',
                 p_stop_id                => rec.pick_stop_id,
                 p_trip_id                => rec.pick_trip_id,
                 p_stop_location_id       => rec.pick_stop_location_id,
                 p_planned_dep_date       => rec.pick_planned_departure_date,
                 p_actual_date            => SYSDATE,
                 p_defer_interface_flag   => 'Y');

             COMMIT;

             fnd_file.put_line(fnd_file.log, 'x_return_status - '||o_return_status);
             fnd_file.put_line(fnd_file.log, 'x_msg_count - '||o_msg_count);
             fnd_file.put_line(fnd_file.log, 'x_msg_data - '||o_msg_data);

             ---------------------------------------------------------------
             -- Closing Drop-Off Trips
             ---------------------------------------------------------------
             o_return_status := NULL;
             o_msg_count     := NULL;
             o_msg_data      := NULL;
             
             l_sec := 'Closing Drop-Off Trips';
             WSH_TRIP_STOPS_PUB.Stop_Action
               ( p_api_version_number     => 1.0,
                 p_init_msg_list          => FND_API.G_TRUE,
                 x_return_status          => o_return_status,
                 x_msg_count              => o_msg_count,
                 x_msg_data               => o_msg_data,
                 p_action_code            => 'CLOSE',
                 p_stop_id                => rec.drop_stop_id,
                 p_trip_id                => rec.drop_trip_id,
                 p_stop_location_id       => rec.drop_stop_location_id,
                 p_planned_dep_date       => rec.drop_planned_departure_date,
                 p_actual_date            => SYSDATE,
                 p_defer_interface_flag   => 'Y');

             COMMIT;

             fnd_file.put_line(fnd_file.log, 'x_return_status - '||o_return_status);
             fnd_file.put_line(fnd_file.log, 'x_msg_count - '||o_msg_count);
             fnd_file.put_line(fnd_file.log, 'x_msg_data - '||o_msg_data);

             ---------------------------------------------------------------
             -- Execute "Interface Trip Stop - SRS" Program
             ---------------------------------------------------------------
             l_sec := 'Execute "Interface Trip Stop - SRS" Program';
             l_request_id := fnd_request.submit_request ('WSH',                      -- application
                                                          'WSHINTERFACE',            -- program short name--XXWC_OM_PROGRESS_SO_LINES
                                                          '',                        -- description
                                                          '',                        -- start time
                                                          FALSE,                     -- sub request
                                                          'ALL',                     -- argument1
                                                          NULL,                      -- argument2
                                                          rec.delivery_id,
                                                          '0',
                                                          NULL,
                                                          NULL,
                                                          NULL,
                                                          '1',
                                                          '1',
                                                           CHR (0)                   -- represents end of arguments
                                                                 );
        COMMIT;

        END LOOP;

      -- Version# 1.1 > Start
      ---------------------------------------------------------------
      -- Insert records into XXWC.XXWC_OM_FULFILL_ACCEPTANCE table
      ---------------------------------------------------------------
      l_sec := 'Insert records into XXWC.XXWC_OM_FULFILL_ACCEPTANCE table';

      INSERT INTO xxwc.xxwc_om_fulfill_acceptance
                          (header_id
                          ,accepted_by
                          ,accepted_date
                          ,accepted_signature
                          ,acceptance_comments
                          ,process_flag
                          ,error_msg
                          ,creation_date
                          ,created_by
                          ,last_update_date
                          ,last_updated_by
                          ,last_update_login
                          ,line_id)
      (SELECT shp.id       -- Header_id
           ,shp.created_by      -- Accepted_by
           ,shp.creation_date   -- Acceptance Date
           ,shp.signature_name  -- Accepted Signature
           ,NULL                -- Acceptance Comments
           ,'N'                 -- Process Flag
           ,NULL                -- Error Message
           ,SYSDATE             -- Creation Date
           ,shp.created_by      -- created by
           ,SYSDATE             -- last_update_date
           ,shp.created_by      -- last_updated_by
           ,NULL                -- last_update_login
           ,ool.line_id          -- line_id
        FROM xxwc.xxwc_signature_capture_tbl shp
           , xxwc.xxwc_wsh_shipping_stg stg
           , oe_order_lines_all ool
       WHERE 1 = 1
         AND signature_name      != '***CANCEL***'
         AND ship_confirm_status  = 'COMPLETED'
         AND stg.delivery_id      = shp.delivery_id
         AND ool.line_id          =  stg.line_id
         AND ool.flow_status_code = 'PRE-BILLING_ACCEPTANCE'
         AND shp.creation_date like sysdate
         AND NOT EXISTS (SELECT '1' 
                              FROM xxwc.xxwc_om_fulfill_acceptance fa
                             WHERE fa.line_id = stg.line_id)
        UNION
      SELECT shp.id       -- Header_id
           ,shp.created_by      -- Accepted_by
           ,shp.creation_date   -- Acceptance Date
           ,shp.signature_name  -- Accepted Signature
           ,NULL                -- Acceptance Comments
           ,'N'                 -- Process Flag
           ,NULL                -- Error Message
           ,SYSDATE             -- Creation Date
           ,shp.created_by      -- created by
           ,SYSDATE             -- last_update_date
           ,shp.created_by      -- last_updated_by
           ,NULL                -- last_update_login
           ,ool.line_id          -- line_id
        FROM xxwc.xxwc_signature_capture_arc_tbl shp
           , xxwc.xxwc_wsh_shipping_stg stg
           , oe_order_lines_all ool
       WHERE 1 = 1
         AND signature_name      != '***CANCEL***'
         AND ship_confirm_status  = 'COMPLETED'
         AND stg.delivery_id      = shp.delivery_id
         AND ool.line_id          =  stg.line_id
         AND ool.flow_status_code = 'PRE-BILLING_ACCEPTANCE'
         AND shp.creation_date > sysdate - 5
         AND NOT EXISTS (SELECT '1' 
                              FROM xxwc.xxwc_om_fulfill_acceptance fa
                             WHERE fa.line_id = stg.line_id))
        ;
     -- Version# 1.1 < End

EXCEPTION
WHEN OTHERS THEN

    l_error_message := 'Error processing Stuck Picked SO Lines';
    xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => 'XXWC_SF_ROUTINES_PKG.PROCESS_PICKED_SO_LINES',
         p_calling             => l_sec,
         p_request_id          => fnd_global.conc_request_id,
         p_ora_error_msg       => SUBSTR('SQLERRM' ,1 ,2000),
         p_error_desc          => l_error_message,
         p_distribution_list   => g_distro_list,
         p_module              => 'OM');
    p_retcode := 2;

END process_picked_so_lines;

END xxwc_sf_routines_pkg;
/