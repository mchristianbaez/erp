create or replace PACKAGE BODY      APPS.XXWC_MWA_ROUTINES_PKG AS

   /*****************************************************************************************************************************************
   *   $Header XXWC_MWA_ROUTINES_PKG $                                                                                                      *
   *   Module Name: XXWC_MWA_ROUTINES_PKG                                                                                                   *
   *                                                                                                                                        *
   *   PURPOSE:   This package is used by the extensions in MSCA                                                                            *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        16-OCT-2014  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
   *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
   *   1.1        20-JUN-2015  Lee Spitzer               TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions                   *
   *              20-JUN-2015  Lee Spitzer               TMS Ticket Number 20150622-00004 RF - UPC Update                                   *
   *                                                                                                                                        *
   *   1.2        08-JUL-2015  Lee Spitzer               TMS Ticket 20150714-00142 RF - Implement fixes to include with release 2           *
   *   1.3        09-Mar-2017  Ashwin Sridhar            Added Debug messages and logic for TMS#20170126-00027 in the procedure WC_LOCATOR  *
   *****************************************************************************************************************************************/

  
  
  PROCEDURE DEBUG_LOG (P_MESSAGE   VARCHAR2)
  IS
   
    
       PRAGMA AUTONOMOUS_TRANSACTION;
       
       BEGIN
    
        IF g_debug = 'Y' THEN
      
          FND_LOG.STRING (G_LOG_LEVEL, upper(G_PACKAGE||'.'||G_CALL_FROM) ||' '|| G_CALL_POINT, P_MESSAGE);
         
          COMMIT;
        
        END IF;
  
  EXCEPTION
  
    WHEN others THEN
        
        g_sqlcode := SQLCODE;
        g_sqlerrm := SQLERRM;
        g_message := 'Error in generating debug log ';
    
            xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                 , p_calling             => g_call_point
                                                 , p_ora_error_msg       => SQLERRM
                                                 , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                 , p_distribution_list   => g_distro_list
                                                 , p_module              => g_module);

   
  END DEBUG_LOG;


  /*****************************************************************************************************************************************
   *   FUNCTION GET_OPEN_RCV_QTY                                                                                                            *
   *                                                                                                                                        *
   *   PURPOSE:   This function is used to get the default open quantity for receiving mobile forms                                         *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        16-OCT-2014  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
   *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
   *****************************************************************************************************************************************/

    FUNCTION GET_OPEN_RCV_QTY  ( P_ORGANIZATION_ID  IN VARCHAR2        --2
                               , P_TRANSACTION_TYPE IN VARCHAR2        --3
                               , P_PO_HEADER_ID IN VARCHAR2            --4
                               , P_PO_LINE_ID IN VARCHAR2              --5
                               , P_INVENTORY_ITEM_ID IN VARCHAR2       --6
                               , P_UOM_CODE IN VARCHAR2                --7
                               , P_REQUISITION_HEADER_ID IN VARCHAR2   --8
                               , P_SHIPMENT_HEADER_ID IN VARCHAR2      --9
                               , P_ORDER_HEADER_ID IN VARCHAR2)         --0
                               
              RETURN VARCHAR2 IS
        
        l_organization_id NUMBER;
        l_po_header_id NUMBER;
        l_po_line_id NUMBER;
        l_inventory_item_id NUMBER;
        l_requisition_header_id NUMBER;
        l_shipment_header_id NUMBER;
        l_order_header_id NUMBER;
        
        l_item_uom VARCHAR2(25);
        l_item_uom_code varchar2(3);
        l_order_qty NUMBER;
        l_order_uom VARCHAR2(25);
        l_order_uom_code VARCHAR2(3);
        l_rcv_qty   NUMBER;
        l_rcv_uom   VARCHAR2(25);
        l_qty NUMBER DEFAULT 2;
        
        
    BEGIN
    
      g_call_from := 'GET_OPEN_RCV_QTY';
      g_call_point := 'Start';
      
        
        debug_log('p_organization_id ' || p_organization_id);
        debug_log('p_transaction_type ' || p_transaction_type);
        debug_log('p_po_header_id ' || p_po_header_id);
        debug_log('p_po_line_id ' || p_po_line_id);
        debug_log('p_inventory_item_id ' || p_inventory_item_id);
        debug_log('p_uom_code ' || p_uom_code);
        debug_log('p_requisition_header_id ' || p_requisition_header_id);
        debug_log('p_shipment_header_id ' || p_shipment_header_id);
        debug_log('p_order_header_id ' || p_order_header_id);
      
      g_call_point := 'Converting parameters from varchar2 to numbers';
      
      
      BEGIN
        g_call_point := 'Converting Organization Id numbers';
        SELECT to_number(p_organization_id)
        INTO   l_organization_id 
        from   dual;
      EXCEPTION
        WHEN others THEN
          l_po_header_id := NULL;
          g_sqlcode := SQLCODE;
          g_sqlerrm := SQLERRM;
          g_message := 'When others getting l_organization_id ' || g_call_point || g_sqlcode || g_sqlerrm;
          debug_log(g_message);
          RAISE g_exception;
      END;
      
      
      BEGIN
        g_call_point := 'Converting PO Header to numbers';
        SELECT to_number(p_po_header_id)
        INTO   l_po_header_id 
        FROM   dual;
      EXCEPTION
        WHEN others THEN
          l_po_header_id := NULL;
          g_sqlcode := SQLCODE;
          g_sqlerrm := SQLERRM;
          g_message := 'When others getting l_po_header_id ' || g_call_point || g_sqlcode || g_sqlerrm;
          debug_log(g_message);
          RAISE g_exception;
      END;
      
      
      BEGIN
        g_call_point := 'Converting Item to numbers';
        SELECT to_number(p_inventory_item_id)
        INTO   l_inventory_item_id
        from   dual;
      EXCEPTION
        WHEN others THEN
          l_po_header_id := NULL;
          g_sqlcode := SQLCODE;
          g_sqlerrm := SQLERRM;
          g_message := 'When others getting l_inventory_item_id ' || g_call_point || g_sqlcode || g_sqlerrm;
          debug_log(g_message);
          RAISE g_exception;
      END;
      
      BEGIN
        g_call_point := 'Converting PO Line to numbers';
        SELECT to_number(p_po_line_id)
        INTO   l_po_line_id 
        from   dual;
      EXCEPTION
        WHEN others THEN
          l_po_header_id := NULL;
          g_sqlcode := SQLCODE;
          g_sqlerrm := SQLERRM;
          g_message := 'When others getting l_po_line_id ' || g_call_point || g_sqlcode || g_sqlerrm;
          debug_log(g_message);
          RAISE g_exception;
      END;
      
      BEGIN
        g_call_point := 'Converting Requisition Header to numbers';
        SELECT to_number(p_requisition_header_id)
        INTO   l_requisition_header_id 
        from   dual;
      EXCEPTION
        WHEN others THEN
          l_po_header_id := NULL;
          g_sqlcode := SQLCODE;
          g_sqlerrm := SQLERRM;
          g_message := 'When others getting l_requisition_header_id ' || g_call_point || g_sqlcode || g_sqlerrm;
          debug_log(g_message);
          RAISE g_exception;
      END;
      
      
      BEGIN
        g_call_point := 'Converting Shipment to numbers';
        SELECT to_number(p_shipment_header_id)
        INTO   l_shipment_header_id 
        FROM   dual;
      EXCEPTION
        WHEN others THEN
          l_po_header_id := NULL;
          g_sqlcode := SQLCODE;
          g_sqlerrm := SQLERRM;
          g_message := 'When others getting l_shipment_header_id ' || g_call_point || g_sqlcode || g_sqlerrm;
          debug_log(g_message);
          RAISE g_exception;
      END;
      
      BEGIN
        g_call_point := 'Converting Order Header to numbers';
        SELECT to_number(p_order_header_id)
        INTO   l_order_header_id 
        FROM   dual;
      EXCEPTION
        WHEN others THEN
          l_po_header_id := NULL;
          g_sqlcode := SQLCODE;
          g_sqlerrm := SQLERRM;
          g_message := 'When others getting l_order_header_id ' || g_call_point || g_sqlcode || g_sqlerrm;
          debug_log(g_message);
          RAISE g_exception;
      END;
      
        debug_log('l_organization_id ' || l_organization_id);
        debug_log('l_po_header_id ' || l_po_header_id);
        debug_log('l_po_line_id ' || l_po_line_id);
        debug_log('l_inventory_item_id ' || l_inventory_item_id);
        debug_log('l_requisition_header_id ' || l_requisition_header_id);
        debug_log('l_shipment_header_id ' || l_shipment_header_id);
        debug_log('l_order_header_id ' || l_order_header_id);
 
 
      IF l_inventory_item_id IS NOT NULL THEN
      
        BEGIN
            SELECT msib.primary_unit_of_measure, msib.primary_uom_code
            INTO   l_item_uom, l_item_uom_code
            FROM   mtl_system_items_b msib
            WHERE  inventory_item_id = l_inventory_item_id
            AND    organization_id = l_organization_id;
        EXCEPTION
          WHEN others THEN
              l_item_uom := '';
              l_item_uom_code := '';
        END;
        
      
      END IF;
      
      IF p_transaction_type = 'RECEIVE' THEN
      
        --PO Receive transaction
        IF l_po_header_id IS NOT NULL AND l_inventory_item_id IS NOT NULL THEN
        
          BEGIN
              SELECT ms.to_org_primary_uom, sum(ms.to_org_primary_quantity)
              INTO   l_order_uom, l_order_qty
              FROM   mtl_supply ms
              WHERE  ms.to_organization_id = l_organization_id
              AND    ms.po_line_id = nvl(l_po_line_id,ms.po_line_id)
              AND    ms.item_id = nvl(l_inventory_item_id,ms.item_id)
              AND    ms.po_header_id = nvl(l_po_header_id,ms.po_header_id)
              AND    ms.supply_type_code = 'PO'
            GROUP BY ms.to_org_primary_uom;
           EXCEPTION
              WHEN OTHERS THEN
                    l_order_qty := 0;
                    l_order_uom := l_item_uom;
          END;
          
          
        END IF;
        
        
         --Internal Shipment Transaction
        IF l_shipment_header_id IS NOT NULL AND l_inventory_item_id IS NOT NULL THEN
        
          BEGIN
              SELECT ms.to_org_primary_uom, sum(ms.to_org_primary_quantity)
              INTO   l_order_uom, l_order_qty
              FROM   mtl_supply ms
              WHERE  ms.to_organization_id = l_organization_id
              AND    ms.item_id = nvl(l_inventory_item_id,ms.item_id)
              AND    ms.shipment_header_id = nvl(l_shipment_header_id,ms.shipment_header_id)
              AND    ms.supply_type_code = 'SHIPMENT'
            GROUP BY ms.to_org_primary_uom;
           EXCEPTION
              WHEN OTHERS THEN
                    l_order_qty := 0;
                    l_order_uom := l_item_uom;
          END;
          
          
        END IF;
         
        --Requisition Transaction
        IF l_requisition_header_id IS NOT NULL AND l_inventory_item_id IS NOT NULL THEN
        
          BEGIN
              SELECT ms.to_org_primary_uom, sum(ms.to_org_primary_quantity)
              INTO   l_order_uom, l_order_qty
              FROM   mtl_supply ms
              WHERE  ms.to_organization_id = l_organization_id
              AND    ms.item_id = nvl(l_inventory_item_id,ms.item_id)
              AND    ms.req_header_id = nvl(l_requisition_header_id,ms.req_header_id)
              AND    ms.supply_type_code = 'REQUISITION'
            GROUP BY ms.to_org_primary_uom;
           EXCEPTION
              WHEN OTHERS THEN
                    l_order_qty := 0;
                    l_order_uom := l_item_uom;
          END;
          
          
        END IF;
        
        --RMA Transaction
        IF l_order_header_id IS NOT NULL AND l_inventory_item_id IS NOT NULL THEN
        
          BEGIN
            SELECT oola.order_quantity_uom,  sum(nvl(ordered_quantity,0)) - sum(nvl(cancelled_quantity, 0)) - sum(nvl(shipped_quantity,0)) open_quantity
            INTO   l_order_uom, l_order_qty
            FROM   oe_order_lines_all oola
            WHERE  oola.header_id = nvl(l_order_header_id, oola.header_id)
            AND    oola.ship_from_org_id = l_organization_id
            AND    oola.line_category_code = 'RETURN'
            GROUP BY oola.order_quantity_uom;
            
          EXCEPTION
              WHEN OTHERS THEN
                    l_order_qty := 0;
                    l_order_uom := l_item_uom;
          END;
        
        END IF;
          
      END IF;
      
      IF p_transaction_type = 'DELIVER' THEN
      
        --PO Deliver transaction
        IF l_po_header_id IS NOT NULL AND l_inventory_item_id IS NOT NULL THEN
        
          BEGIN
              SELECT ms.to_org_primary_uom, sum(ms.to_org_primary_quantity)
              INTO   l_order_uom, l_order_qty
              FROM   mtl_supply ms
              WHERE  ms.to_organization_id = l_organization_id
              AND    ms.po_line_id = nvl(l_po_line_id,ms.po_line_id)
              AND    ms.item_id = nvl(l_inventory_item_id,ms.item_id)
              AND    ms.po_header_id = nvl(l_po_header_id,ms.po_header_id)
              AND    ms.supply_type_code = 'RECEIVING'
            GROUP BY ms.to_org_primary_uom;
           EXCEPTION
              WHEN OTHERS THEN
                    l_order_qty := 0;
                    l_order_uom := l_item_uom;
          END;
        
        
         END IF;
         
        --Receipt Transaction
        IF l_shipment_header_id IS NOT NULL AND l_inventory_item_id IS NOT NULL THEN
        
          BEGIN
                SELECT ms.to_org_primary_uom, sum(ms.to_org_primary_quantity)
                INTO   l_order_uom, l_order_qty
                FROM   mtl_supply ms
                WHERE  ms.to_organization_id = l_organization_id
                AND    ms.shipment_header_id = nvl(l_shipment_header_id,ms.shipment_header_id)
                AND    ms.item_id = nvl(l_inventory_item_id,ms.item_id)
                AND    ms.supply_type_code = 'RECEIVING'
              GROUP BY ms.to_org_primary_uom;
             EXCEPTION
                WHEN OTHERS THEN
                      l_order_qty := 0;
                      l_order_uom := l_item_uom;
            END;
                  
        END IF;
        
        --RMA Transaction
        IF l_order_header_id IS NOT NULL AND l_inventory_item_id IS NOT NULL THEN
        
          BEGIN
            SELECT rsl.primary_unit_of_measure, sum(nvl(rsl.quantity_received,0)) - sum(nvl(rt.primary_quantity,0))
            into   l_order_uom, l_order_qty
            FROM   rcv_shipment_lines rsl,
                   rcv_transactions rt
            WHERE  rsl.item_id = nvl(l_inventory_item_id, rsl.item_id)
            AND    rsl.to_organization_id = l_organization_id
            AND    rsl.source_document_code = 'RMA'
            AND    rsl.destination_type_code = 'RECEIVING'
            AND    rsl.oe_order_header_id = nvl(l_order_header_id,rsl.oe_order_header_id)
            AND    rsl.shipment_line_id = rt.shipment_line_id(+)
            AND    rt.transaction_type(+) = 'DELIVER'
            GROUP BY rsl.primary_unit_of_measure;
          EXCEPTION
              WHEN OTHERS THEN
                    l_order_qty := 0;
                    l_order_uom := l_item_uom;
          END;
          
        END IF;
        
      END IF;
      
        l_qty := nvl(l_order_qty,0);
    
      RETURN l_qty;
        
    EXCEPTION
      WHEN g_exception THEN 
      
              l_qty := -1;
              return l_qty;

      WHEN others THEN
              
              l_qty := -99999;
              return l_qty;
      
             g_sqlcode := SQLCODE;
             g_sqlerrm := SQLERRM;
             g_message := 'when others error ';
             debug_log(g_message || g_sqlcode || g_sqlerrm);
    
            xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                 , p_calling             => g_call_point
                                                 , p_ora_error_msg       => SQLERRM
                                                 , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                 , p_distribution_list   => g_distro_list
                                                 , p_module              => g_module);


          
    END GET_OPEN_RCV_QTY;
                         
    /*****************************************************************************************************************************************
   *   PROCEDURE GET_LOC_LOV_RCV                                                                                                            *
   *                                                                                                                                        *
   *   PURPOSE:   This function is used to get the default open quantity for receiving mobile forms                                         *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
   *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
   *****************************************************************************************************************************************/
  
    PROCEDURE GET_LOC_LOV_RCV ( x_locators               OUT    NOCOPY t_genref --0
                              , p_organization_id        IN     NUMBER    --1
                              , p_subinventory_code      IN     VARCHAR2  --2
                              , p_restrict_locators_code IN     NUMBER    --3
                              , p_inventory_item_id      IN     NUMBER    --4
                              , p_concatenated_segments  IN     VARCHAR2  --5
                              , p_transaction_type_id    IN     NUMBER    --6
                              , p_wms_installed          IN     VARCHAR2  --7
                              , p_project_id             IN     NUMBER    --8
                              , p_task_id                IN     NUMBER    --9
                              , p_locator_alias          IN     VARCHAR2) --10
    IS
                              
      l_default_type NUMBER := 2; --1 = Shipping, Receiving = Receiving, 3 = Move Order
      l_return_type  VARCHAR2(1) := 'H'; --H - QOH, T - ATT, R-ATR
    
    BEGIN

      g_call_from := 'GET_LOC_LOV_RCV';
      g_call_point := 'Start';
      
      
        debug_log('p_organization_id ' || p_organization_id);
        debug_log('p_subinventory_code ' || p_subinventory_code);
        debug_log('p_restrict_locators_code  ' || p_restrict_locators_code);
        debug_log('p_inventory_item_id ' || p_inventory_item_id);
        debug_log('p_concatenated_segments ' || p_concatenated_segments);
        debug_log('p_transaction_type_id ' || p_transaction_type_id);
        debug_log('p_wms_installed ' || p_wms_installed);
        debug_log('p_project_id ' || p_project_id);
        debug_log('p_task_id ' || p_task_id);
        debug_log('p_locator_alias ' || p_locator_alias);
    
        
          --Added 5/12/2015 to prevent open cursors
        if x_locators%isOPEN then
           debug_log('closing x_locators cursor');
            close x_locators;
        
        end if;

    
          OPEN x_locators FOR
          --Primary Bin Query
          SELECT   loc.inventory_location_id
                 , loc.concatenated_segments
                 , loc.description
                 , loc.bin_assignment
                 , loc.on_hand
                , loc.max_qty
          from  (SELECT   wil.inventory_location_id
                        , wil.locator_segments concatenated_segments
                        , wil.description
                        , 'PRIMARY' bin_assignment
                        , 1 bin_sequence
                        , xxwc_mwa_routines_pkg.get_onhand(p_inventory_item_id => p_inventory_item_id
                                                  , p_organization_id   => p_organization_id
                                                  , p_subinventory      => p_subinventory_code
                                                  , p_locator_id        => wil.inventory_location_id
                                                  , p_return_type       => l_return_type
                                                  , p_lot_number        => NULL
                                                  ) on_hand
                        , max_minmax_quantity max_qty
                 FROM wms_item_locations_kfv wil,
                      mtl_item_sub_inventories misi
                 WHERE wil.organization_id = p_organization_id
                 AND NVL(wil.disable_date, TRUNC(SYSDATE + 1)) > TRUNC(SYSDATE)
                 AND wil.subinventory_code = p_subinventory_code
                 AND wil.PROJECT_ID IS NULL
                 AND wil.task_id IS NULL
                 ---
                 AND misi.inventory_item_id(+) = p_inventory_item_id
                 AND wil.organization_id = misi.organization_id(+)
                 AND wil.subinventory_code = misi.secondary_inventory(+)
                 --Item Transaction Defaults
                 AND EXISTS (SELECT *
                             FROM   mtl_item_loc_defaults mild
                             WHERE wil.organization_id = mild.organization_id
                             and wil.subinventory_code = mild.subinventory_code
                             AND wil.inventory_location_id = mild.locator_id
                             AND mild.inventory_item_id = p_inventory_item_id
                             AND mild.default_type = l_default_type
                             )
                 UNION
                 SELECT   wil.inventory_location_id
                        , wil.locator_segments concatenated_segments
                        , wil.description
                        , 'ALTERNATE' bin_assignment
                        , 2 bin_sequence
                        , xxwc_mwa_routines_pkg.get_onhand(p_inventory_item_id => p_inventory_item_id
                                                  , p_organization_id   => p_organization_id
                                                  , p_subinventory      => p_subinventory_code
                                                  , p_locator_id        => wil.inventory_location_id
                                                  , p_return_type       => l_return_type
                                                  , p_lot_number        => NULL
                                                  ) on_hand
                        , NULL max_qty
                 FROM wms_item_locations_kfv wil
                 WHERE wil.organization_id = p_organization_id
                 AND NVL(wil.disable_date, TRUNC(SYSDATE + 1)) > TRUNC(SYSDATE)
                 AND wil.subinventory_code = p_subinventory_code
                 AND wil.PROJECT_ID IS NULL
                 AND wil.task_id IS NULL
                 --Secondary Locators
                 AND EXISTS (SELECT *
                             FROM   mtl_secondary_locators msl
                             WHERE wil.organization_id = msl.organization_id
                             AND wil.subinventory_code = msl.subinventory_code
                             AND wil.inventory_location_id = msl.secondary_locator
                             AND msl.inventory_item_id = p_inventory_item_id
                             --Item Transaction Defaults
                             AND NOT EXISTS (SELECT *
                                             FROM   mtl_item_loc_defaults mild
                                             WHERE msl.organization_id = mild.organization_id
                                             AND msl.subinventory_code = mild.subinventory_code
                                             AND msl.secondary_locator = mild.locator_id
                                             AND msl.inventory_item_id = mild.inventory_item_id
                                             AND mild.default_type = l_default_type
                                             )
                              )
                 UNION
                 SELECT   wil.inventory_location_id
                        , wil.locator_segments concatenated_segments
                        , wil.description
                        , 'ON-HAND' bin_assignment
                        , 3 bin_sequence
                        , xxwc_mwa_routines_pkg.get_onhand(p_inventory_item_id => p_inventory_item_id
                                                  , p_organization_id   => p_organization_id
                                                  , p_subinventory      => p_subinventory_code
                                                  , p_locator_id        => wil.inventory_location_id
                                                  , p_return_type       => l_return_type
                                                  , p_lot_number        => NULL
                                                  ) on_hand
                        , null max_qty
                 FROM wms_item_locations_kfv wil                  
                 WHERE wil.organization_id = p_organization_id
                 AND NVL(wil.disable_date, TRUNC(SYSDATE + 1)) > TRUNC(SYSDATE)
                 AND wil.subinventory_code = p_subinventory_code
                 AND wil.PROJECT_ID IS NULL
                 AND wil.task_id IS NULL
                 AND EXISTS (SELECT *
                             FROM   mtl_onhand_quantities_detail moqd
                             WHERE  wil.organization_id = moqd.organization_id
                             AND wil.subinventory_code = moqd.subinventory_code
                             AND wil.inventory_location_id = moqd.locator_id
                             AND moqd.inventory_item_id = p_inventory_item_id
                             AND NOT EXISTS (SELECT *
                                             FROM   mtl_item_loc_defaults mild
                                             WHERE moqd.organization_id = mild.organization_id
                                             AND moqd.subinventory_code = mild.subinventory_code
                                             AND moqd.locator_id = mild.locator_id
                                             AND moqd.inventory_item_id = mild.inventory_item_id
                                             )
                            AND NOT EXISTS (SELECT *
                                           FROM   mtl_secondary_locators msl
                                           WHERE moqd.organization_id = msl.organization_id
                                           AND moqd.subinventory_code = msl.subinventory_code
                                           AND moqd.locator_id = msl.secondary_locator
                                           AND moqd.inventory_item_id = msl.inventory_item_id
                                           )
                            )
              /*UNION
              SELECT   wil.inventory_location_id
                        , wil.locator_segments concatenated_segments
                        , wil.description
                        , 'OTHER' bin_assignment
                        , 4 bin_sequence
                        , 0
                        , null max_qty
                 FROM wms_item_locations_kfv wil                  
                 WHERE wil.organization_id = p_organization_id
                 AND NVL(wil.disable_date, TRUNC(SYSDATE + 1)) > TRUNC(SYSDATE)
                 AND wil.subinventory_code = p_subinventory_code
                 AND wil.PROJECT_ID IS NULL
                 AND wil.task_id IS NULL
                 AND NOT EXISTS (SELECT *
                                FROM   mtl_item_loc_defaults mild
                                WHERE wil.organization_id = mild.organization_id
                                AND wil.subinventory_code = mild.subinventory_code
                                AND wil.inventory_location_id = mild.locator_id
                                AND mild.inventory_item_id = p_inventory_item_id
                               )
                AND NOT EXISTS (SELECT *
                                FROM   mtl_secondary_locators msl
                                WHERE wil.organization_id = msl.organization_id
                                AND wil.subinventory_code = msl.subinventory_code
                                AND wil.inventory_location_id = msl.secondary_locator
                                AND msl.inventory_item_id = p_inventory_item_id
                                )
                AND NOT EXISTS (SELECT *
                                 FROM   mtl_onhand_quantities_detail moqd
                                 WHERE  wil.organization_id = moqd.organization_id
                                 AND wil.subinventory_code = moqd.subinventory_code
                                 AND wil.inventory_location_id = moqd.locator_id
                                 AND moqd.inventory_item_id = p_inventory_item_id
                                )
                */
                )
                loc                
              order by loc.bin_sequence, loc.on_hand, loc.concatenated_segments;
                 
    
    EXCEPTION
    
      WHEN others THEN
             g_sqlcode := SQLCODE;
             g_sqlerrm := SQLERRM;
             g_message := 'when others error ';
             debug_log(g_message || g_sqlcode || g_sqlerrm);
    
               --Added 5/12/2015 to prevent open cursors
              if x_locators%isOPEN then
                 debug_log('closing x_locators cursor');
                  close x_locators;
              
              end if;

              
            xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                 , p_calling             => g_call_point
                                                 , p_ora_error_msg       => SQLERRM
                                                 , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                 , p_distribution_list   => g_distro_list
                                                 , p_module              => g_module);


    
    END  GET_LOC_LOV_RCV;
   
   
    /*****************************************************************************************************************************************
   *   FUNCTION GET_ONHAND                                                                                                                 *
   *                                                                                                                                        *
   *   PURPOSE:   This function is used to get return the current on-hand, available to transact and available to reserve quantity          *
   *                                                                                                                                        *
   *    Return :  NUMBER
   *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
   *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
   *****************************************************************************************************************************************/

                          
    FUNCTION GET_ONHAND ( p_inventory_item_id   IN   NUMBER
                        , p_organization_id     IN   NUMBER
                        , p_subinventory        IN   VARCHAR2
                        , p_locator_id          IN   NUMBER
                        , p_return_type         IN   VARCHAR2
                        , p_lot_number          IN   VARCHAR2 DEFAULT NULL
                        )
              RETURN NUMBER
    IS
      qoh                NUMBER;
      rqoh               NUMBER;
      qr                 NUMBER;
      qs                 NUMBER;
      att                NUMBER;
      atr                NUMBER;
      ls                 VARCHAR2 (1);
      mc                 NUMBER;
      md                 VARCHAR2 (1000);
      l_lot_control      BOOLEAN;
      l_serial_control   BOOLEAN;
      l_lc_code          NUMBER;
      l_sn_code          NUMBER;
      l_lot_number       VARCHAR2 (30);
   BEGIN
    
     g_call_from := 'GET_ONHAND';
     g_call_point := 'Start';
    
       debug_log('p_inventory_item_id ' || p_inventory_item_id);
       debug_log('p_organization_id ' || p_organization_id);
       debug_log('p_subinventory ' || p_subinventory);
       debug_log('p_locator_id ' || p_locator_id);
       debug_log('p_return_type ' || p_return_type);
       debug_log('p_lot_number ' || p_lot_number);
             
    
      IF p_lot_number IS NOT NULL
      THEN
         l_lot_control := TRUE;
         l_lot_number := p_lot_number;
      ELSE
         l_lot_control := FALSE;
         l_lot_number := NULL;
      END IF;

      g_call_point := 'clearing quantity cache';
      
      inv_quantity_tree_pub.clear_quantity_cache;
      
      g_call_point := 'calling inv_quantity_tree_pub.query_quantities';
      
      inv_quantity_tree_pub.query_quantities
                                  (p_api_version_number       => 1.0,
                                   p_init_msg_lst             => 'T',
                                   x_return_status            => ls,
                                   x_msg_count                => mc,
                                   x_msg_data                 => md,
                                   p_organization_id          => p_organization_id,
                                   p_inventory_item_id        => p_inventory_item_id,
                                   p_tree_mode                => 1
                                                                  --'Reservation Mode'
      ,
                                   p_is_revision_control      => FALSE,
                                   p_is_lot_control           => l_lot_control
                                                                       --FALSE
                                                                              --l_lot_control
      ,
                                   p_is_serial_control        => FALSE
                                                                      --l_serial_control
      ,
                                   p_lot_expiration_date      => SYSDATE,
                                   p_revision                 => NULL,
                                   p_lot_number               => l_lot_number,
                                   p_subinventory_code        => p_subinventory,
                                   p_locator_id               => p_locator_id,
                                   x_qoh                      => qoh,
                                   x_rqoh                     => rqoh,
                                   x_qr                       => qr,
                                   x_qs                       => qs,
                                   x_att                      => att,
                                   x_atr                      => atr
                                  );

      IF p_return_type = 'H'
      THEN
         RETURN (qoh);
         debug_log('qoh ' || qoh);
      ELSIF p_return_type = 'R'
      THEN
         RETURN (atr);
         debug_log('atr ' || atr);
      ELSIF p_return_type = 'T'
      THEN
         RETURN (att);
         debug_log('att ' || att);
      ---
      ELSIF p_return_type = 'QOH'
      THEN
         RETURN (qoh);
         debug_log('qoh ' || qoh);
      ELSIF p_return_type = 'RQOH' 
      THEN
        RETURN (rqoh);
        debug_log('rqoh ' || rqoh);
      ELSIF p_return_type = 'QR'
      THEN
        RETURN (qr);
        debug_log('qr ' || qr);
      ELSIF p_return_type = 'QS'
      THEN
        RETURN (qs);
        debug_log('qs ' || qs);
      ELSIF p_return_type = 'ATR'
      THEN
         RETURN (atr);
         debug_log('atr ' || atr);
      ELSIF p_return_type = 'ATT'
      THEN
         RETURN (att);
         debug_log('att ' || att);
      ELSE
         RETURN 0;
         debug_log('0 ' || 0);
      END IF;
      
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN (0);
         debug_log('0 ' || 0);
         
         g_sqlcode := SQLCODE;
         g_sqlerrm := SQLERRM;
         g_message := 'when others error ';
         debug_log(g_message || g_sqlcode || g_sqlerrm);
    
         xxcus_error_pkg.xxcus_error_main_api (  p_called_from         => g_package||'.'||g_call_from
                                               , p_calling             => g_call_point
                                               , p_ora_error_msg       => SQLERRM
                                               , p_error_desc          => 'Error in call ' || g_call_from || g_call_point
                                               , p_distribution_list   => g_distro_list
                                               , p_module              => g_module);

   END GET_ONHAND;
   
   
    /*****************************************************************************************************************************************
     *   FUNCTION GET_DEFAULT_LOCATOR_ID                                                                                                      *
     *                                                                                                                                        *
     *   PURPOSE:   This function is used to get return the primary bin assignment of an item aka primary bin                                 *
     *  
     *    Parameters: p_organization_id  IN NUMBER - organization_id
     *                p_inventory_item_id IN NUMBER - inventory_item_id
     *                p_subinventory_code IN VARCHAR2 - subinventory_code
     *                p_default_type  IN  VARCHAR2 - 1 = Shipping, 2 = Receiving, 3 = Move Order Requisition, lookup type from MFG_LOOKUPS of 'MTL_DEFAULT_LOCATORS'
     *                
     *    Return :  NUMBER - locator_id from mtl_item_loc_defaults                                                                                                                   *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
     *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
     *****************************************************************************************************************************************/


      FUNCTION GET_DEFAULT_LOCATOR_ID ( p_organization_id    IN NUMBER
                                      , p_inventory_item_id  IN NUMBER
                                      , p_subinventory_code  IN VARCHAR2
                                      , p_default_type       IN NUMBER)
               RETURN NUMBER IS
          
          l_locator_id NUMBER;
          l_default_type NUMBER := 2; --Default Receiving if p_default is null;
          l_subinventory_code VARCHAR2(10);
                
        BEGIN
        
        g_call_from := 'GET_DEFAULT_LOCATOR_ID';
        g_call_point := 'Start';
    
        debug_log('p_organization_id ' || p_organization_id);
        debug_log('p_inventory_item_id ' || p_inventory_item_id);
        debug_log('p_subinventory_code ' || p_subinventory_code);
        debug_log('p_default_type ' || p_default_type);
        
        g_call_point := 'setting default type';
    
        IF p_default_type IS NOT NULL AND p_default_type NOT IN (1,2,3) THEN
              --setting default_type to l_default_type
              l_default_type := p_default_type;
        ELSE
              l_default_type := l_default_type;
        END IF;
        
        debug_log('l_default_type ' || l_default_type);
        
        if p_subinventory_code is null then
        
          g_call_point := 'finding subinventory_code';
          
            BEGIN
              SELECT misd.subinventory_code
              INTO   l_subinventory_code
              FROM   mtl_item_sub_defaults misd
              WHERE  misd.inventory_item_id = p_inventory_item_id
              AND    misd.organization_id = p_organization_id
              AND    misd.default_type = l_default_type;
            EXCEPTION
              WHEN others THEN
                l_subinventory_code := NULL;
                g_sqlcode := SQLCODE;
                g_sqlerrm := SQLERRM;
                g_message := 'Could not find subinventory';
                raise g_exception;
            END;
      
      
        else 
        
          l_subinventory_code := p_subinventory_code;
            
        END IF;
        
        g_call_point := 'finding default_locator';
        
        debug_log('l_default_type ' || l_default_type);
        debug_log('l_subinventory_code ' || l_subinventory_code);
        
          BEGIN
            SELECT mild.locator_id
            INTO   l_locator_id
            FROM   mtl_item_loc_defaults mild
            WHERE  mild.inventory_item_id = p_inventory_item_id
            AND    mild.organization_id = p_organization_id
            AND    mild.default_type = l_default_type
            AND    mild.subinventory_code = l_subinventory_code;
          EXCEPTION
            WHEN others THEN
                g_sqlcode := sqlcode;
                g_sqlerrm := sqlerrm;
                g_message := 'Error getting default locator id';
                --Setting locator_id to -1
                l_locator_id := -1;
          END;
        
          debug_log('l_locator_id ' || l_locator_id);
        
          RETURN l_locator_id;
          
          
      EXCEPTION
        WHEN g_exception THEN
        
            RETURN -1;
            
            debug_log('g_exception ' || g_message || g_sqlcode || g_sqlerrm);
            debug_log('return -1');
      
        WHEN OTHERS THEN
           RETURN -1;
             debug_log('g_exception ' || g_message || g_sqlcode || g_sqlerrm);
             debug_log('return -1');
       
         g_sqlcode := SQLCODE;
         g_sqlerrm := SQLERRM;
         g_message := 'when others error ';
         debug_log(g_message || g_sqlcode || g_sqlerrm);
    
         xxcus_error_pkg.xxcus_error_main_api (  p_called_from         => g_package||'.'||g_call_from
                                               , p_calling             => g_call_point
                                               , p_ora_error_msg       => SQLERRM
                                               , p_error_desc          => 'Error in call ' || g_call_from || g_call_point
                                               , p_distribution_list   => g_distro_list
                                               , p_module              => g_module);

        END GET_DEFAULT_LOCATOR_ID;
        
        
      /*****************************************************************************************************************************************
     *   FUNCTION GET_LOCATOR_NAME                                                                                                            *
     *                                                                                                                                        *
     *   PURPOSE:   This function is used to return the locator concatenated segments                                                         *
     *                                                                                                                                        *
     *    Parameters: p_organization_id  IN NUMBER - organization_id                                                                          *
     *                p_locator_id IN NUMBER - locator_id                                                                                     *
     *                                                                                                                                        *
     *    Return :  VARCHAR2 - concateated_segments from wms_item_locations_kfv                                                               *                                                   *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
     *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
     *****************************************************************************************************************************************/
    
      FUNCTION GET_LOCATOR_NAME  ( p_organization_id IN NUMBER
                                 , p_locator_id IN NUMBER)
               RETURN VARCHAR2 IS
          
          l_locator VARCHAR2(40);
          
            
       BEGIN
       
          g_call_from := 'GET_LOCATOR_NAME';
          g_call_point := 'Start';
      
            debug_log('p_organization_id ' || p_organization_id);
            debug_log('p_locator_id ' || p_locator_id);
         
          BEGIN
              SELECT concatenated_segments
              INTO   l_locator
              FROM   wms_item_locations_kfv
              WHERE  organization_id = p_organization_id
              AND    inventory_location_id = p_locator_id;
          
          EXCEPTION
              WHEN others THEN
                g_sqlcode := sqlcode;
                g_sqlerrm := sqlerrm;
                g_message := 'Error in finding concatated_segments from wms_item_locations_kfv';
                debug_log(g_message || g_sqlcode || g_sqlerrm);
                l_locator := '';
          END;
          
          debug_log('l_locator ' || l_locator);
          
          return l_locator;
        
          
        EXCEPTION
      
          WHEN OTHERS THEN
             RETURN '';
             
             debug_log('g_exception ' || g_message || g_sqlcode || g_sqlerrm);
             debug_log('return -1');
       
           g_sqlcode := SQLCODE;
           g_sqlerrm := SQLERRM;
           g_message := 'when others error ';
           debug_log(g_message || g_sqlcode || g_sqlerrm);
      
           xxcus_error_pkg.xxcus_error_main_api (  p_called_from         => g_package||'.'||g_call_from
                                                 , p_calling             => g_call_point
                                                 , p_ora_error_msg       => SQLERRM
                                                 , p_error_desc          => 'Error in call ' || g_call_from || g_call_point
                                                 , p_distribution_list   => g_distro_list
                                                 , p_module              => g_module);

        
        END GET_LOCATOR_NAME;
        
        
     /*****************************************************************************************************************************************
     *   FUNCTION GET_ITEM_SUB_MAX_QTY                                                                                                        *
     *                                                                                                                                        *
     *   PURPOSE:   This function is used to return the max qty from subinventory of the item subinventories                                  *
     *                                                                                                                                        *
     *    Parameters: p_organization_id  IN NUMBER - organization_id                                                                          *
     *                p_inventory_item_id IN NUMBER -- inventory_item_id                                                                      *
     *                p_subinventory_code IN VARCHAR2 - subinventory_code                                                                     *
     *                                                                                                                                        *
     *    Return :  NUMBER - MAX_MINMAX_QTY from MTL_ITEM_SUBINVENTORIES                                                                      *                                                   *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
     *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
     *****************************************************************************************************************************************/

        FUNCTION GET_ITEM_SUB_MAX_QTY   ( p_organization_id    IN NUMBER
                                        , p_inventory_item_id  IN NUMBER
                                        , p_subinventory_code  IN VARCHAR2)
                 RETURN NUMBER IS

          l_max_minmax_quantity NUMBER;
                 
        BEGIN
        
          g_call_from := 'GET_ITEM_SUB_MAX_QTY';
          g_call_point := 'Start';
      
            debug_log('p_organization_id ' || p_organization_id);
            debug_log('p_inventory_item_id ' || p_inventory_item_id);
            debug_log('p_subinventory_code ' || p_subinventory_code);
            
            BEGIN
              
              SELECT max_minmax_quantity
              INTO   l_max_minmax_quantity
              FROM   mtl_item_sub_inventories
              WHERE  organization_id = p_organization_id
              AND    inventory_item_id = p_inventory_item_id
              and    secondary_inventory = p_subinventory_code;
              
            EXCEPTION
              WHEN OTHERS THEN
                  g_sqlcode := SQLCODE;
                  g_sqlerrm := SQLERRM;
                  g_message := 'Error getting max qty for item subinventory';
                  l_max_minmax_quantity := 0;
            
            END;
            
            return l_max_minmax_quantity;
        
        EXCEPTION
        
          WHEN OTHERS THEN
               RETURN 0;

               debug_log('g_exception ' || g_message || g_sqlcode || g_sqlerrm);
               debug_log('return 0');
         
             g_sqlcode := SQLCODE;
             g_sqlerrm := SQLERRM;
             g_message := 'when others error ';
             debug_log(g_message || g_sqlcode || g_sqlerrm);
        
             xxcus_error_pkg.xxcus_error_main_api (  p_called_from         => g_package||'.'||g_call_from
                                                   , p_calling             => g_call_point
                                                   , p_ora_error_msg       => SQLERRM
                                                   , p_error_desc          => 'Error in call ' || g_call_from || g_call_point
                                                   , p_distribution_list   => g_distro_list
                                                   , p_module              => g_module);

        
        END GET_ITEM_SUB_MAX_QTY;
        
        
     /*****************************************************************************************************************************************
     *   PROCEDURE PROCESS_ITEM_DEFAULT_LOCATOR                                                                                               *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure is used to insert or update MTL_ITEM_SUB_DEFAULTS AND MTL_ITEM_LOC_DEFAULTS                                *
     *                                                                                                                                        *
     *    Parameters: p_organization_id  IN NUMBER - organization_id                                                                          *
     *                p_inventory_item_id IN NUMBER -- inventory_item_id                                                                      *
     *                p_subinventory_code IN VARCHAR2 - subinventory_code                                                                     *
     *                p_locator_id IN NUMBER --LOCATOR_ID                                                                                     *
     *                p_default_type  IN  NUMBER - 1 = Shipping, 2 = Receiving, 3 = Move Order Requisition, lookup type from MFG_LOOKUPS of 'MTL_DEFAULT_LOCATORS'
     *                                                                                                                                        *
     *    Out :  X_RETURN = 0 = Success, 1 = Error                                                                                            *
     *           X_MESSAGE = Return message                                                                                                   *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
     *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
     *****************************************************************************************************************************************/

        PROCEDURE PROCESS_ITEM_DEFAULT_LOCATOR   ( p_organization_id    IN NUMBER
                                                 , p_inventory_item_id  IN NUMBER
                                                 , p_subinventory_code  IN VARCHAR2
                                                 , p_locator_id IN NUMBER
                                                 , p_default_type IN NUMBER
                                                 , x_return  OUT NUMBER
                                                 , x_message OUT VARCHAR2)
                  IS
         
         l_sub_default_count NUMBER DEFAULT 0;
         l_loc_default_count NUMBER DEFAULT 0;
         
         l_organization_id NUMBER;
         l_inventory_item_id NUMBER;
         l_subinventory_code VARCHAR2(10);
         l_default_type NUMBER;
         l_locator_id NUMBER;
                  
      BEGIN

          g_call_from := 'PROCESS_ITEM_DEFAULT_LOCATOR';
          g_call_point := 'Start';
      
          debug_log('p_organization_id ' || p_organization_id);
          debug_log('p_inventory_item_id ' || p_inventory_item_id);
          debug_log('p_subinventory_code ' || p_subinventory_code);
          debug_log('p_locator_id ' || p_locator_id);
          debug_log('p_default_type ' || p_default_type);
          
          --Check Organization ID
          g_call_point := 'Getting Organization Id';
          
          BEGIN
            SELECT organization_id
            into   l_organization_id
            FROM   mtl_parameters
            where  organization_id = p_organization_id;
          EXCEPTION
            WHEN others THEN
                g_sqlcode := SQLCODE;
                g_sqlerrm := SQLERRM;
                g_message := 'Error getting organization_id ' || g_sqlcode || g_sqlerrm;
                raise g_exception;
          END;
          
          g_call_point := 'Getting Inventory Item Id';
          
          BEGIN
            SELECT inventory_item_id
            into   l_inventory_item_id
            FROM   mtl_system_items_b
            WHERE  organization_id = l_organization_id
            AND    inventory_item_id = p_inventory_item_id;
          EXCEPTION
            WHEN others THEN
                g_sqlcode := SQLCODE;
                g_sqlerrm := SQLERRM;
                g_message := 'Error getting inventory_item_id ' || g_sqlcode || g_sqlerrm;
                raise g_exception;
          END;
          
          g_call_point := 'Getting Subinventory Code';
          
          BEGIN
            SELECT secondary_inventory_name
            into   l_subinventory_code
            FROM   mtl_secondary_inventories
            WHERE  organization_id = l_organization_id
            and    secondary_inventory_name = p_subinventory_code;
          EXCEPTION
            WHEN others THEN
                g_sqlcode := SQLCODE;
                g_sqlerrm := SQLERRM;
                g_message := 'Error getting subinventory_code ' || g_sqlcode || g_sqlerrm;
                raise g_exception;
          END;
          
          g_call_point := 'Getting Locator Id';
          
          BEGIN
            SELECT inventory_location_id
            INTO   l_locator_id
            FROM   wms_item_locations_kfv
            WHERE  organization_id = l_organization_id
            AND    subinventory_code = l_subinventory_code
            and    inventory_location_id = p_locator_id;
          EXCEPTION
            WHEN others THEN
                g_sqlcode := SQLCODE;
                g_sqlerrm := SQLERRM;
                g_message := 'Error getting locator_id ' || g_sqlcode || g_sqlerrm;
                raise g_exception;
          END;
          
          g_call_point := 'Getting Default Type';
          
          BEGIN
            SELECT lookup_code
            into   l_default_type
            FROM   mfg_lookups
            WHERE  lookup_type = 'MTL_DEFAULT_LOCATORS'
            and    lookup_code = p_default_type;
          EXCEPTION
            WHEN others THEN
                g_sqlcode := SQLCODE;
                g_sqlerrm := SQLERRM;
                g_message := 'Error getting default type ' || g_sqlcode || g_sqlerrm;
                raise g_exception;
          END;
          
          
          g_call_point := 'Getting Subinventory Default Count';
         
           BEGIN
            SELECT count(inventory_item_id)
            INTO   l_sub_default_count
            FROM   mtl_item_sub_defaults
            WHERE  organization_id = l_organization_id
            AND    inventory_item_id = l_inventory_item_id
            and    default_type = l_default_type;
          EXCEPTION
            WHEN others THEN
                g_sqlcode := SQLCODE;
                g_sqlerrm := SQLERRM;
                g_message := 'Error getting Subinventory Default Count ' || g_sqlcode || g_sqlerrm;
                raise g_exception;
          END;
          
          
          --if count is 0 then insert, else update mtl_item_sub_defaults
          IF l_sub_default_count = 0 THEN
            
                g_call_point := 'Insert into MTL_ITEM_SUB_DEFAULTS';
              
                BEGIN
                   
                    INSERT INTO MTL_ITEM_SUB_DEFAULTS
                                (organization_id
                                ,inventory_item_id
                                ,subinventory_code
                                ,default_type
                                ,creation_date
                                ,created_by
                                ,last_update_date
                                ,last_updated_by
                                )
                                VALUES
                                (l_organization_id
                                ,l_inventory_item_id
                                ,l_subinventory_code
                                ,l_default_type
                                ,SYSDATE
                                ,FND_GLOBAL.USER_ID
                                ,SYSDATE
                                ,FND_GLOBAL.USER_ID
                                );
                                
                
                EXCEPTION
                    WHEN OTHERS THEN  
                        g_sqlcode := SQLCODE;
                        g_sqlerrm := SQLERRM;
                        g_message := 'Error inserting into MTL_ITEM_SUB_DEFAULTS';
                        raise g_exception;
                END;
          
          ELSE
          
                g_call_point := 'Updating MTL_ITEM_SUB_DEFAULTS';
                
                BEGIN
                    UPDATE MTL_ITEM_SUB_DEFAULTS
                    SET     subinventory_code = l_subinventory_code
                           ,last_update_date = SYSDATE
                           ,last_updated_by = FND_GLOBAL.USER_ID
                    WHERE  organization_id = l_organization_id
                    AND    inventory_item_id = l_inventory_item_id
                    AND    default_type = l_default_type;
                                
                
                EXCEPTION
                    WHEN OTHERS THEN  
                        g_sqlcode := SQLCODE;
                        g_sqlerrm := SQLERRM;
                        g_message := 'Error updating into MTL_ITEM_SUB_DEFAULTS';
                        raise g_exception;
                END;
            
            END IF;
            
          g_call_point := 'Getting Locator Default Count';
         
           BEGIN
            SELECT count(inventory_item_id)
            INTO   l_loc_default_count
            FROM   mtl_item_loc_defaults
            WHERE  organization_id = l_organization_id
            AND    inventory_item_id = l_inventory_item_id
            AND    default_type = l_default_type;
          EXCEPTION
            WHEN others THEN
                g_sqlcode := SQLCODE;
                g_sqlerrm := SQLERRM;
                g_message := 'Error getting Locator Default Count ' || g_sqlcode || g_sqlerrm;
                raise g_exception;
          END;
          
          
          --if count is 0 then insert, else update mtl_item_sub_defaults
          IF l_loc_default_count = 0 THEN
            
                g_call_point := 'Insert into MTL_ITEM_LOC_DEFAULTS';
              
                BEGIN
                   
                    INSERT INTO MTL_ITEM_LOC_DEFAULTS
                                (organization_id
                                ,inventory_item_id
                                ,subinventory_code
                                ,locator_id
                                ,default_type
                                ,creation_date
                                ,created_by
                                ,last_update_date
                                ,last_updated_by
                                )
                                VALUES
                                (l_organization_id
                                ,l_inventory_item_id
                                ,l_subinventory_code
                                ,l_locator_id
                                ,l_default_type
                                ,SYSDATE
                                ,FND_GLOBAL.USER_ID
                                ,SYSDATE
                                ,FND_GLOBAL.USER_ID
                                );
                                
                
                EXCEPTION
                    WHEN OTHERS THEN  
                        g_sqlcode := SQLCODE;
                        g_sqlerrm := SQLERRM;
                        g_message := 'Error inserting into MTL_ITEM_LOC_DEFAULTS';
                        raise g_exception;
                END;
          
          ELSE
          
                g_call_point := 'Updating MTL_ITEM_LOC_DEFAULTS';
                
                BEGIN
                    UPDATE MTL_ITEM_LOC_DEFAULTS
                    SET     subinventory_code = l_subinventory_code
                           ,locator_id = l_locator_id
                           ,last_update_date = SYSDATE
                           ,last_updated_by = FND_GLOBAL.USER_ID
                    WHERE  organization_id = l_organization_id
                    AND    inventory_item_id = l_inventory_item_id
                    AND    default_type = l_default_type;
                                
                
                EXCEPTION
                    WHEN OTHERS THEN  
                        g_sqlcode := SQLCODE;
                        g_sqlerrm := SQLERRM;
                        g_message := 'Error updating into MTL_ITEM_LOC_DEFAULTS';
                        raise g_exception;
                END;
            
            END IF;
          
          
            COMMIT;
            
          x_return := 0;
          x_message := 'Success';
          
      
      EXCEPTION
      
        WHEN g_exception THEN
            
            ROLLBACK;
            
            x_return := 1;
            x_message := g_message;
            
            debug_log('g_exception '|| g_message || g_sqlcode || g_sqlerrm);
        
          WHEN OTHERS THEN
            
            x_return := 1;
            x_message := g_message;
          
             
             g_sqlcode := SQLCODE;
             g_sqlerrm := SQLERRM;
             g_message := 'when others error ';
             debug_log(g_message || g_sqlcode || g_sqlerrm);
        
             xxcus_error_pkg.xxcus_error_main_api (  p_called_from         => g_package||'.'||g_call_from
                                                   , p_calling             => g_call_point
                                                   , p_ora_error_msg       => SQLERRM
                                                   , p_error_desc          => 'Error in call ' || g_call_from || g_call_point
                                                   , p_distribution_list   => g_distro_list
                                                   , p_module              => g_module);

      
      END PROCESS_ITEM_DEFAULT_LOCATOR;
      
      /*****************************************************************************************************************************************
     *   PROCEDURE PROCESS_ITEM_SUB_MAX                                                                                                       *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure is used to insert or update MTL_ITEM_SUB_INVENTORIES                                                       *
     *                                                                                                                                        *
     *    Parameters: p_organization_id  IN NUMBER - organization_id                                                                          *
     *                p_inventory_item_id IN NUMBER -- inventory_item_id                                                                      *
     *                p_subinventory_code IN VARCHAR2 - subinventory_code                                                                     *
     *                p_max_qty  IN  NUMBER                                                                                                   *
     *                                                                                                                                        *
     *    Out :  X_RETURN = 0 = Success, 1 = Error                                                                                            *
     *           X_MESSAGE = Return message                                                                                                   *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
     *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
     *****************************************************************************************************************************************/

        PROCEDURE PROCESS_ITEM_SUB_MAX           ( p_organization_id    IN NUMBER
                                                 , p_inventory_item_id  IN NUMBER
                                                 , p_subinventory_code  IN VARCHAR2
                                                 , p_max_qty IN NUMBER
                                                 , x_return  OUT NUMBER
                                                 , x_message OUT VARCHAR2)
                  IS
                  
           l_item_sub_count NUMBER DEFAULT 0;
           l_organization_id NUMBER;
           l_inventory_item_id NUMBER;
           l_subinventory_code VARCHAR2(10);
           l_max_qty NUMBER;
           l_inventory_planning_code NUMBER := 6;
        
        BEGIN
        
          g_call_from := 'PROCESS_ITEM_SUB_MAX';
          g_call_point := 'Start';
      
          debug_log('p_organization_id ' || p_organization_id);
          debug_log('p_inventory_item_id ' || p_inventory_item_id);
          debug_log('p_subinventory_code ' || p_subinventory_code);
          debug_log('p_max_qty ' || p_max_qty);
          
          
          --Check Organization ID
          g_call_point := 'Getting Organization Id';
          
          BEGIN
            SELECT organization_id
            into   l_organization_id
            FROM   mtl_parameters
            where  organization_id = p_organization_id;
          EXCEPTION
            WHEN others THEN
                g_sqlcode := SQLCODE;
                g_sqlerrm := SQLERRM;
                g_message := 'Error getting organization_id ' || g_sqlcode || g_sqlerrm;
                raise g_exception;
          END;
          
          g_call_point := 'Getting Inventory Item Id';
          
          BEGIN
            SELECT inventory_item_id
            into   l_inventory_item_id
            FROM   mtl_system_items_b
            WHERE  organization_id = l_organization_id
            AND    inventory_item_id = p_inventory_item_id;
          EXCEPTION
            WHEN others THEN
                g_sqlcode := SQLCODE;
                g_sqlerrm := SQLERRM;
                g_message := 'Error getting inventory_item_id ' || g_sqlcode || g_sqlerrm;
                raise g_exception;
          END;
          
          g_call_point := 'Getting Subinventory Code';
          
          BEGIN
            SELECT secondary_inventory_name
            into   l_subinventory_code
            FROM   mtl_secondary_inventories
            WHERE  organization_id = l_organization_id
            and    secondary_inventory_name = p_subinventory_code;
          EXCEPTION
            WHEN others THEN
                g_sqlcode := SQLCODE;
                g_sqlerrm := SQLERRM;
                g_message := 'Error getting subinventory_code ' || g_sqlcode || g_sqlerrm;
                raise g_exception;
          END;
          
          
          g_call_from := 'Getting count on MTL_ITEM_SUB_INVENTORIES';
          
            BEGIN
              SELECT count(inventory_item_id)
              INTO   l_item_sub_count
              FROM   mtl_item_sub_inventories
              WHERE  organization_id = p_organization_id
              AND    inventory_item_id = p_inventory_item_id
              AND    secondary_inventory = p_subinventory_code;
            EXCEPTION
              WHEN others THEN
                    g_sqlcode := SQLCODE;
                    g_sqlerrm := SQLERRM;
                    g_message := 'Error getting count MTL_ITEM_SUB_INVENTORIES';
                    l_item_sub_count := 0;
            END;
          
          --if count is - then insert, else update
          if l_item_sub_count = 0 then
            
              BEGIN
                    INSERT INTO  mtl_item_sub_inventories
                                (
                                 organization_id
                                ,inventory_item_id
                                ,secondary_inventory
                                ,last_update_date
                                ,last_updated_by
                                ,creation_date
                                ,created_by
                                ,inventory_planning_code
                                ,max_minmax_quantity
                                )
                                VALUES
                                (
                                 l_organization_id
                                ,l_inventory_item_id
                                ,l_subinventory_code
                                ,SYSDATE
                                ,FND_GLOBAL.USER_ID
                                ,SYSDATE
                                ,FND_GLOBAL.USER_ID
                                ,l_inventory_planning_code
                                ,p_max_qty
                                );
                EXCEPTION
                  WHEN others THEN
                      g_sqlcode := SQLCODE;
                      g_sqlerrm := SQLERRM;
                      g_message := 'Error inserting MTL_ITEM_SUB_INVENTORIES';
                      raise g_exception;
                
                END;
                                
          
          ELSE
          
              BEGIN
                   UPDATE mtl_item_sub_inventories
                   SET    max_minmax_quantity = p_max_qty
                         ,last_update_date = SYSDATE
                         ,last_updated_by = FND_GLOBAL.USER_ID
                   where  organization_id = l_organization_id
                   AND    inventory_item_id = l_inventory_item_id
                   AND    secondary_inventory = l_subinventory_code;
              
              EXCEPTION
                  
                    WHEN others THEN
                      g_sqlcode := SQLCODE;
                      g_sqlerrm := SQLERRM;
                      g_message := 'Error updating MTL_ITEM_SUB_INVENTORIES';
                      raise g_exception;
                      
              END;
              
          END IF;
          
          
          COMMIT;
        
          x_return := 0;
          x_message := 'Success';
          
        EXCEPTION
      
        WHEN g_exception THEN
            
            ROLLBACK;
            
            x_return := 1;
            x_message := g_message;
            
            debug_log('g_exception '|| g_message || g_sqlcode || g_sqlerrm);
        
          WHEN OTHERS THEN
            
            x_return := 1;
            x_message := g_message;
          
             
             g_sqlcode := SQLCODE;
             g_sqlerrm := SQLERRM;
             g_message := 'when others error ';
             debug_log(g_message || g_sqlcode || g_sqlerrm);
        
             xxcus_error_pkg.xxcus_error_main_api (  p_called_from         => g_package||'.'||g_call_from
                                                   , p_calling             => g_call_point
                                                   , p_ora_error_msg       => SQLERRM
                                                   , p_error_desc          => 'Error in call ' || g_call_from || g_call_point
                                                   , p_distribution_list   => g_distro_list
                                                   , p_module              => g_module);

      
       END PROCESS_ITEM_SUB_MAX;

    /*****************************************************************************************************************************************
     *   PROCEDURE PROCESS_ITEM_LOCATOR_TIE                                                                                                    *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure is used to update or insert records into the MTL_SECONDARY_LOCATORS                                        *
     *                                                                                                                                        *
     *    Parameters: p_organization_id  IN NUMBER - organization_id                                                                          *
     *                p_inventory_item_id IN NUMBER -- inventory_item_id                                                                      *
     *                p_subinventory_code IN VARCHAR2 - subinventory_code                                                                     *
     *                p_locator_id IN NUMBER --LOCATOR_ID                                                                                     *
     *                                                                                                                                        *
     *    Out :  X_RETURN = 0 = Success, 1 = Error                                                                                            *
     *           X_MESSAGE = Return message                                                                                                   *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
     *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
     *   1.1        08-JUL-2015  Lee Spitzer               TMS Ticket 20150714-00142 RF - Implement fixes to include with release 2           *
     *****************************************************************************************************************************************/

        PROCEDURE PROCESS_ITEM_LOCATOR_TIE       ( p_organization_id    IN NUMBER
                                                 , p_inventory_item_id  IN NUMBER
                                                 , p_subinventory_code  IN VARCHAR2
                                                 , p_locator_id IN NUMBER
                                                 , x_return  OUT NUMBER
                                                 , x_message OUT VARCHAR2)
                  IS
                  
           l_organization_id NUMBER;
           l_inventory_item_id NUMBER;
           l_subinventory_code VARCHAR2(10);
           l_locator_id NUMBER;
           l_item_locator_count NUMBER;
           l_status_id NUMBER DEFAULT 1;
           l_return_status                      VARCHAR2(1) := FND_API.G_Ret_Sts_Success;
           l_msg_count                          NUMBER;
           l_msg_data                           VARCHAR2(4000);
           l_msg_index_out                      NUMBER;
           l_reason_id                          NUMBER; --Added 08-JUL-2015 TMS Ticket 20150714-00142 RF - Implement fixes to include with release 2

                  
       BEGIN
       
          g_call_from := 'PROCESS_ITEM_LOCATOR_TIE';
          g_call_point := 'Start';
      
          debug_log('p_organization_id ' || p_organization_id);
          debug_log('p_inventory_item_id ' || p_inventory_item_id);
          debug_log('p_subinventory_code ' || p_subinventory_code);
          debug_log('p_locator_id ' || p_locator_id);
          
          
          --Check Organization ID
          g_call_point := 'Getting Organization Id';
          
          BEGIN
            SELECT organization_id
            into   l_organization_id
            FROM   mtl_parameters
            where  organization_id = p_organization_id;
          EXCEPTION
            WHEN others THEN
                g_sqlcode := SQLCODE;
                g_sqlerrm := SQLERRM;
                g_message := 'Error getting organization_id ' || g_sqlcode || g_sqlerrm;
                raise g_exception;
          END;
          
          g_call_point := 'Getting Inventory Item Id';
          
          BEGIN
            SELECT inventory_item_id
            into   l_inventory_item_id
            FROM   mtl_system_items_b
            WHERE  organization_id = l_organization_id
            AND    inventory_item_id = p_inventory_item_id;
          EXCEPTION
            WHEN others THEN
                g_sqlcode := SQLCODE;
                g_sqlerrm := SQLERRM;
                g_message := 'Error getting inventory_item_id ' || g_sqlcode || g_sqlerrm;
                raise g_exception;
          END;
          
          g_call_point := 'Getting Subinventory Code';
          
          BEGIN
            SELECT secondary_inventory_name
            into   l_subinventory_code
            FROM   mtl_secondary_inventories
            WHERE  organization_id = l_organization_id
            and    secondary_inventory_name = p_subinventory_code;
          EXCEPTION
            WHEN others THEN
                g_sqlcode := SQLCODE;
                g_sqlerrm := SQLERRM;
                g_message := 'Error getting subinventory_code ' || g_sqlcode || g_sqlerrm;
                raise g_exception;
          END;
        
        
        g_call_point := 'Getting Locator Id Code';
          
          BEGIN
            SELECT inventory_location_id
            INTO   l_locator_id
            FROM   wms_item_locations_kfv
            WHERE  organization_id = l_organization_id
            AND    subinventory_code = l_subinventory_code
            and    inventory_location_id = p_locator_id;
          EXCEPTION
            WHEN others THEN
                g_sqlcode := SQLCODE;
                g_sqlerrm := SQLERRM;
                g_message := 'Error getting locator_id ' || g_sqlcode || g_sqlerrm;
                raise g_exception;
          END;
        
          g_call_point := 'Getting Count Item Locators';
        
          BEGIN
              SELECT count(inventory_item_id)
              INTO   l_item_locator_count
              FROM   mtl_secondary_locators
              WHERE  organization_id = l_organization_id
              AND    subinventory_code = l_subinventory_code
              AND    secondary_locator = l_locator_id
              AND    inventory_item_id = l_inventory_item_id;
          
          EXCEPTION
            WHEN others THEN
                g_sqlcode := SQLCODE;
                g_sqlerrm := SQLERRM;
                g_message := 'Error getting count item locators  ' || g_sqlcode || g_sqlerrm;
                raise g_exception;
              
          END;
        
                            
          
          --if no tie exists then execute item locator tie api
          IF l_item_locator_count = 0 THEN
          
              --Call WMS API for Locator Tie
              BEGIN
                  INV_LOC_WMS_PUB.Create_Loc_Item_Tie     ( x_return_status              => l_return_status
                                                          , x_msg_count                  => l_msg_count
                                                          , x_msg_data                   => l_msg_data
                                                          , p_inventory_item_id          => l_inventory_item_id
                                                          , p_item                       => NULL
                                                          , p_organization_id            => l_organization_id
                                                          , p_organization_code          => NULL
                                                          , p_subinventory_code          => l_subinventory_code
                                                          , p_inventory_location_id      => l_locator_id
                                                          , p_locator                    => NULL
                                                          , p_status_id                  => l_status_id
                                                          , p_par_level                  => NULL
                                                           );
    
                        -- if problems, display messages and terminate processing
                        IF l_return_status != 'S' THEN
                            g_message := 'error processing item / location assignment';
                            For ln_index IN 1 .. l_msg_count Loop
                                FND_MSG_PUB.Get(p_msg_index       => ln_index,
                                                p_encoded         => FND_API.G_False,
                                                p_data            => l_msg_data,
                                                p_msg_index_out   => l_msg_index_out);
                                FND_FILE.Put_Line(FND_FILE.Log, l_msg_data);
                            END Loop;
                            RAISE g_exception;
                        End If;

              
              --Added 08-JUL-2015 TMS Ticket 20150714-00142 RF - Implement fixes to include with release 2
              g_call_point := 'Getting Reason Id';  
              
              BEGIN
                select reason_id
                into   l_reason_id
                from   mtl_transaction_reasons
                where  reason_name = '15 - RF';
              EXCEPTION
                when others then
                      l_reason_id := null;
              END;


              g_call_point := 'Setting Reason Id to program_id';
              
              BEGIN
              
                update mtl_secondary_locators
                    set  program_id = l_reason_id
                where  inventory_item_id = l_inventory_item_id
                and    subinventory_code = l_subinventory_code
                and    secondary_locator = l_locator_id
                and    organization_id = l_organization_id
                and    status_id = l_status_id;
              
              EXCEPTION
                when others then
                      g_sqlcode := SQLCODE;
                      g_sqlerrm := SQLERRM;
                      g_message := 'Error updating mtl_secondary_locators with reason_id';
                
              END;
              
              --End Added 08-JUL-2015 TMS Ticket 20150714-00142 RF - Implement fixes to include with release 2

            EXCEPTION
              WHEN others THEN
                  g_sqlcode := SQLCODE;
                  g_sqlerrm := SQLERRM;
                  g_message := 'Error calling INV_LOC_WMS_PUB.Create_Loc_Item_Tie ';
                  raise g_exception;
             END;
             
             COMMIT;
             
             g_message := 'Added';
           
          else
            
            g_message := 'Already Exists';
            
          END IF;          
          
        x_return := 0;
        x_message := g_message;
 
       EXCEPTION
      
        WHEN g_exception THEN
            
            ROLLBACK;
            
            x_return := 1;
            x_message := g_message;
            
            debug_log('g_exception '|| g_message || g_sqlcode || g_sqlerrm);
        
          WHEN OTHERS THEN
            
            x_return := 1;
            x_message := g_message;
          
             
             g_sqlcode := SQLCODE;
             g_sqlerrm := SQLERRM;
             g_message := 'when others error ';
             debug_log(g_message || g_sqlcode || g_sqlerrm);
        
             xxcus_error_pkg.xxcus_error_main_api (  p_called_from         => g_package||'.'||g_call_from
                                                   , p_calling             => g_call_point
                                                   , p_ora_error_msg       => SQLERRM
                                                   , p_error_desc          => 'Error in call ' || g_call_from || g_call_point
                                                   , p_distribution_list   => g_distro_list
                                                   , p_module              => g_module);

      
 
       END PROCESS_ITEM_LOCATOR_TIE;

     /*****************************************************************************************************************************************
     *   PROCEDURE PROCESS_ITEM_LOCATOR_TIE_CCR                                                                                               *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure is the concurrent program to called by the trigger XXWC_MMT_ITEM_LOC_TIE_AIU used to update or insert records into the MTL_SECONDARY_LOCATORS                                        *
     *                                                                                                                                        *
     *    Parameters: p_organization_id  IN NUMBER - organization_id                                                                          *
     *                p_inventory_item_id IN NUMBER -- inventory_item_id                                                                      *
     *                p_subinventory_code IN VARCHAR2 - subinventory_code                                                                     *
     *                p_locator_id IN NUMBER --LOCATOR_ID                                                                                     *
     *                                                                                                                                        *
     *    Out :  errbuf = standard errbug for concurrent programs                                                                             *
     *           retcode = standard retcode for concurrent programs                                                                           *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
     *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
     *****************************************************************************************************************************************/
     

         PROCEDURE PROCESS_ITEM_LOCATOR_TIE_CCR
                                                 ( errbuf               OUT VARCHAR2
                                                 , retcode              OUT VARCHAR2
                                                 , p_organization_id    IN NUMBER
                                                 , p_inventory_item_id  IN NUMBER
                                                 , p_subinventory_code  IN VARCHAR2
                                                 , p_locator_id IN NUMBER)
                    IS
            
            x_return NUMBER;
            x_message VARCHAR2(2000);
            l_retcode NUMBER;
            
          BEGIN
          
            XXWC_MWA_ROUTINES_PKG.PROCESS_ITEM_LOCATOR_TIE 
                  ( p_organization_id => p_organization_id
                  , p_inventory_item_id => p_inventory_item_id
                  , p_subinventory_code => p_subinventory_code
                  , p_locator_id => p_locator_id
                  , x_return => x_return
                  , x_message => x_message);
          
          
            if x_return != 0 then
            
                l_retcode := 1;
                
            else
            
                l_retcode := 0;
                
            end if;
            
            retcode := l_retcode;
          
          END PROCESS_ITEM_LOCATOR_TIE_CCR;
          
     /*****************************************************************************************************************************************
     *   PROCEDURE material_label                                                                                                             *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure used to print material labels
     *                                                                                                                                        *
     *    Parameters: p_organization_id  IN NUMBER - organization_id                                                                          *
     *                p_inventory_item_id IN NUMBER -- inventory_item_id                                                                      *
     *                p_lot_number IN VARCHAR2 - subinventory_code                                                                            *
     *                p_quantity IN NUMBER                                                                                                    *
     *                p_copies  IN NUMBER                                                                                                     *
     *                p_user_id IN NUMBER --LOCATOR_ID                                                                                     *
     *                                                                                                                                        *
     *    Out :  errbuf = standard errbug for concurrent programs                                                                             *
     *           retcode = standard retcode for concurrent programs                                                                           *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
     *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
     *****************************************************************************************************************************************/
              
          PROCEDURE material_label
              (   p_organization_id                 IN          VARCHAR2   DEFAULT NULL
              ,   p_inventory_item_id               IN          VARCHAR2   DEFAULT NULL
              ,   p_lot_number                      IN          VARCHAR2   DEFAULT NULL
              ,   p_quantity                        IN          VARCHAR2   DEFAULT NULL
              ,   p_printer_name                    IN          VARCHAR2   DEFAULT NULL
              ,   p_copies                          IN          VARCHAR2   DEFAULT 0
              ,   p_user_id                         IN          VARCHAR2   DEFAULT NULL
              ,   x_return                          OUT         NUMBER
              ,   x_message                         OUT         VARCHAR2
              ) is
              
    x_return_status                   VARCHAR2(1);
    x_msg_count                       NUMBER;
    x_msg_data                        VARCHAR2(2000);
    x_label_status                    VARCHAR2(1);
    l_business_flow_code              NUMBER := NULL;
    l_label_type                      NUMBER := 1; --1 = Material
    l_organization_id                 NUMBER;
    l_inventory_item_id               NUMBER;
    l_revision                        VARCHAR2(10) := NULL;
    --l_lot_number                      VARCHAR2(30) := NULL;
    l_fm_serial_number                VARCHAR2(30) := NULL;
    l_to_serial_number                VARCHAR2(30) := NULL;
    l_lpn_id                          NUMBER := NULL;
    l_subinventory_code               VARCHAR2(10) := NULL;
    l_locator_id                      NUMBER := NULL;
    l_delivery_id                     NUMBER := NULL;
    --l_quantity                        NUMBER := NULL;
    l_uom                             VARCHAR2(3) := NULL;
    l_wip_entity_id                   NUMBER := NULL;
    l_fm_schedule_number              VARCHAR2(30) := NULL;
    l_to_schedule_number              VARCHAR2(30) := NULL;
    l_format_id                       NUMBER  := -1; --Standard Material
    --l_printer_name                    VARCHAR2(30);
    l_responsibility_id               NUMBER;
    l_resp_appl_id                    NUMBER;
    --l_format_name                     VARCHAR2(40) := 'Koch';
    l_copies                    NUMBER := 1;
    l_user_id                   NUMBER;
    l_quantity                  NUMBER;
    l_printer                   VARCHAR2(30);

  BEGIN
        
      g_call_from := 'MATERIAL_LABEL';
      g_call_point := 'Start';
       
      if g_debug = 'Y' THEN
      
          debug_log('p_organization_id ' || p_organization_id);
          debug_log('p_inventory_item_id ' || p_inventory_item_id);
          debug_log('p_lot_number ' || p_lot_number);
          debug_log('p_quantity ' || p_quantity);
          debug_log('p_copies ' || p_copies);
          debug_log('p_printer_name  ' || p_printer_name);    
          debug_log('p_user_id  ' || p_user_id);         
          
      END IF;
      
      
       BEGIN
        g_call_point := 'Converting Organization Id numbers';
        SELECT to_number(p_organization_id)
        INTO   l_organization_id 
        from   dual;
      EXCEPTION
        WHEN others THEN
          l_organization_id := NULL;
          g_sqlcode := SQLCODE;
          g_sqlerrm := SQLERRM;
          g_message := 'When others getting l_organization_id ' || g_call_point || g_sqlcode || g_sqlerrm;
          debug_log(g_message);
          RAISE g_exception;
      END;
      
      
      
      BEGIN
        g_call_point := 'Converting Item to numbers';
        SELECT to_number(p_inventory_item_id)
        INTO   l_inventory_item_id
        from   dual;
      EXCEPTION
        WHEN others THEN
          l_inventory_item_id := NULL;
          g_sqlcode := SQLCODE;
          g_sqlerrm := SQLERRM;
          g_message := 'When others getting l_inventory_item_id ' || g_call_point || g_sqlcode || g_sqlerrm;
          debug_log(g_message);
          RAISE g_exception;
      END;
      
      BEGIN
        g_call_point := 'Converting Quantity to Numbers';
        SELECT to_number(p_quantity)
        INTO   l_quantity
        FROM   dual;
      EXCEPTION
        WHEN others THEN
          l_quantity := NULL;
          g_sqlcode := SQLCODE;
          g_sqlerrm := SQLERRM;
          g_message := 'When others getting l_quantity ' || g_call_point || g_sqlcode || g_sqlerrm;
          debug_log(g_message);
          RAISE g_exception;
      END;
      
      
      g_call_point := 'Converting Copies to numbers';
      BEGIN
        SELECT to_number(p_copies)
        INTO   l_copies
        FROM   dual;
      EXCEPTION
        WHEN others THEN
          l_copies := NULL;
          g_sqlcode := SQLCODE;
          g_sqlerrm := SQLERRM;
          g_message := 'When others getting l_copies ' || g_call_point || g_sqlcode || g_sqlerrm;
          debug_log(g_message);
          RAISE g_exception;
      END;
      
      g_call_point := 'Converting User Id to numbers';
      BEGIN
        SELECT to_number(p_user_id)
        INTO   l_user_id
        FROM   dual;
      EXCEPTION
        WHEN others THEN
          l_user_id := NULL;
          g_sqlcode := SQLCODE;
          g_sqlerrm := SQLERRM;
          g_message := 'When others getting l_user_id ' || g_call_point || g_sqlcode || g_sqlerrm;
          debug_log(g_message);
          RAISE g_exception;
      END;
      
      g_call_point := 'Setting responsibility id';
      begin
        select responsibility_id, application_id
        into   l_responsibility_id, l_resp_appl_id
        from   fnd_responsibility_tl
        where  language = g_language
        and    responsibility_name = 'Inventory';
      exception
          when others then
              g_sqlcode := SQLCODE;
              g_sqlerrm := SQLERRM;
              g_message := 'Could not find responsibility' || G_SQLCODE || G_SQLERRM;
              raise g_exception;
      end;

      g_call_point := 'Getting label format id';
      
      BEGIN
          select label_format_id
          into   l_format_id
          FROM   wms_label_formats
          where  label_format_name = 'Item'
          and    document_id = l_label_type;
      EXCEPTION
          when others then
              g_sqlcode := SQLCODE;
              g_sqlerrm := SQLERRM;
              g_message := 'Could not find label format id' || G_SQLCODE || G_SQLERRM;
      END;
      
      IF p_printer_name IS NULL THEN
      
        l_printer := xxwc_mwa_routines_pkg.get_default_label_printer;
        
      ELSE
      
        l_printer := p_printer_name;
        
      END IF;
   
        fnd_global.apps_initialize(p_user_id,l_responsibility_id,l_resp_appl_id);
        
        INV_LABEL.PRINT_LABEL_MANUAL_WRAP
        (   x_return_status                   => x_return_status
        ,   x_msg_count                       => x_msg_count
        ,   x_msg_data                        => x_msg_data
        ,   x_label_status                    => x_label_status
        ,   p_business_flow_code              => l_business_flow_code
        ,   p_label_type                      => l_label_type
        ,   p_organization_id                 => p_organization_id
        ,   p_inventory_item_id               => p_inventory_item_id
        ,   p_revision                        => l_revision
        ,   p_lot_number                      => p_lot_number
        ,   p_fm_serial_number                => l_fm_serial_number
        ,   p_to_serial_number                => l_to_serial_number
        ,   p_lpn_id                          => l_lpn_id
        ,   p_subinventory_code               => l_subinventory_code
        ,   p_locator_id                      => l_locator_id
        ,   p_delivery_id                     => l_delivery_id
        ,   p_quantity                        => p_quantity
        ,   p_uom                             => l_uom
        ,   p_wip_entity_id                   => l_wip_entity_id
        ,   p_no_of_copies                    => l_copies
        ,   p_fm_schedule_number              => l_fm_schedule_number
        ,   p_to_schedule_number              => l_to_schedule_number
        ,   p_format_id                       => l_format_id
        ,   p_printer_name                    => l_printer
        );
        
    COMMIT;
    
    IF g_debug = 'Y' then
        debug_log('x_return_status ' || x_return_status);
        debug_log('x_msg_count '|| x_msg_count);
        debug_log('x_msg_data ' || x_msg_data);
        debug_log('x_label_status ' || x_label_status);
    END IF;
    
    
    if x_return_status = 'S' then
        
          x_return := 0;
          x_message := 'Label Print Success to ' || l_printer;
          
    else
      
          x_return := 1;
          x_message := 'Return status ' || x_return_status || ' Msg Count ' || x_msg_count || ' ' || x_msg_data || ' label status ' || x_label_status;
    
    end if;
    
    EXCEPTION
        WHEN g_exception THEN
            g_sqlcode := SQLCODE;
            g_sqlerrm := SQLERRM;
            g_message := 'When others ' || g_sqlcode || g_sqlerrm;
            
        WHEN OTHERS THEN
             xxcus_error_pkg.xxcus_error_main_api (  p_called_from         => g_package||'.'||g_call_from
                                                   , p_calling             => g_call_point
                                                   , p_ora_error_msg       => SQLERRM
                                                   , p_error_desc          => 'Error in call ' || g_call_from || g_call_point
                                                   , p_distribution_list   => g_distro_list
                                                   , p_module              => g_module);
    
    END MATERIAL_LABEL;
    
     /*****************************************************************************************************************************************
     *  FUNCTION get_default_label_printer                                                                                                             *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure used get default label printer profile
     *                                                                                                                                        *
     *    Parameters: None                                                                              *
     *    Return : XXWC_LABEL_PRINTER profile                                                                                                                                        *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
     *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
     *****************************************************************************************************************************************/
              

    FUNCTION get_default_label_printer 
      RETURN VARCHAR2 IS
    
   l_printer VARCHAR2(30); 

  
  BEGIN
  
    G_CALL_FROM  := 'get_default_label_printer';
        
    
    G_CALL_POINT := 'getting XXWC_LABEL_PRINTER profile ';
    
      l_printer := FND_PROFILE.VALUE('XXWC_LABEL_PRINTER');
    
      if g_debug = 'Y' THEN
      
         debug_log('l_printer ' || l_printer);
      
      end if;
      
      
      if l_printer = NULL then
     
        g_call_point := 'getting noprint printer';
        
          BEGIN
            SELECT printer_name
            INTO   l_printer
            FROM   fnd_printer
            WHERE  printer_name = 'noprint' --noprint is the default printer for EBS
            ;
          EXCEPTION
            WHEN others THEN
                l_printer := NULL;
          END;
              
      END IF;
                    
      RETURN l_printer;
  
    EXCEPTION
      WHEN others THEN
                 xxcus_error_pkg.xxcus_error_main_api (  p_called_from         => g_package||'.'||g_call_from
                                                       , p_calling             => g_call_point
                                                       , p_ora_error_msg       => SQLERRM
                                                       , p_error_desc          => 'Error in call ' || g_call_from || g_call_point
                                                       , p_distribution_list   => g_distro_list
                                                       , p_module              => g_module);
    
  END get_default_label_printer;

     /*****************************************************************************************************************************************
     *  PROCEDURE GET_RTV_LOV                                                                                                                 *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure used for list of values for internal rtv on the mobile device, C or H                                      *
     *                                                                                                                                        *
     *    Parameters: None                                                                                                                    *
     *    Return : XXWC_LABEL_PRINTER profile                                                                                                 *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
     *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
     *****************************************************************************************************************************************/

  PROCEDURE GET_RTV_LOV ( x_flexvalues             OUT    NOCOPY t_genref --0
                        , p_flex_value             IN     VARCHAR2
                        ) 
            IS
  
  BEGIN               

      g_call_from := 'GET_RTV_LOV';
      g_call_point := 'Start';
 
    
        IF g_debug = 'Y' THEN
              debug_log('p_flex_value ' || p_flex_value);
      
        END IF;
    
    
      --Added 5/12/2015 to prevent open cursors
        if x_flexvalues%isOPEN then
           debug_log('closing x_flexvalues cursor');
            close x_flexvalues;
        
        end if;
        
        
        OPEN x_flexvalues FOR
        SELECT   ffvv.flex_value,
                 ffvv.flex_value_meaning,
                 ffvv.description
          FROM   FND_FLEX_VALUE_SETS ffvs,
                 FND_FLEX_VALUES_VL ffvv
          WHERE  ffvs.flex_value_set_name = 'XXWC_HOLD_OR_CLOSE'
          AND    ffvs.flex_value_set_id = ffvv.flex_value_set_id
          AND    ffvv.enabled_flag = 'Y'
          AND    SYSDATE BETWEEN nvl(ffvv.start_date_active, SYSDATE-1) AND nvl(ffvv.end_date_active,SYSDATE + 1)
          AND    ffvv.flex_value like nvl(p_flex_value,ffvv.flex_value)
          ;
          
          
          

    EXCEPTION
        WHEN others THEN
          g_sqlcode := SQLCODE;
          g_sqlerrm := SQLERRM;
          g_message := 'When others list of values for XXWC_HOLD_OR_CLOSE valueset ' || g_call_point || g_sqlcode || g_sqlerrm;
          debug_log(g_message);
           --Added 5/12/2015 to prevent open cursors
          if x_flexvalues%isOPEN then
             debug_log('closing x_flexvalues cursor');
              close x_flexvalues;
          
          end if;
        
   
  END GET_RTV_LOV;
  
  /*****************************************************************************************************************************************
     *  PROCEDURE PROCESS_UPDATE_RTV_DFF                                                                                                      *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure used to update the shipment_line_id list of values for internal rtv on the mobile device, C or H                                      *
     *                                                                                                                                        *
     *    Parameters: None                                                                                                                    *
     *    Return : XXWC_LABEL_PRINTER profile                                                                                                 *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
     *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
     *****************************************************************************************************************************************/

    PROCEDURE PROCESS_UPDATE_RTV_DFF ( p_shipment_header_id    IN VARCHAR2
                                     , p_inventory_item_id      IN VARCHAR2
                                     , p_organization_id       IN VARCHAR2
                                     , p_value                 IN VARCHAR2
                                     , p_user_id               IN VARCHAR2
                                     , x_return                OUT NUMBER
                                     , x_message               OUT VARCHAR2)
              IS

    l_organization_id NUMBER;
    l_shipment_header_id NUMBER;
    l_inventory_item_id NUMBER;
    l_user_id  NUMBER;
    
    BEGIN
    
     g_call_from := 'PROCESS_UPDATE_RTV_DFF';
     g_call_point := 'Start';
    
     IF g_debug = 'Y' THEN
              debug_log('p_shipment_header_id ' || p_shipment_header_id);
              debug_log('p_inventory_item_id ' || p_inventory_item_id);
              debug_log('p_organization_id ' || p_organization_id);
              debug_log('p_value ' || p_value);
              debug_log('p_user_id ' || p_user_id);
     END IF;
     
       BEGIN
        g_call_point := 'Converting Organization Id numbers';
        SELECT to_number(p_organization_id)
        INTO   l_organization_id 
        from   dual;
      EXCEPTION
        WHEN others THEN
          l_organization_id := NULL;
          g_sqlcode := SQLCODE;
          g_sqlerrm := SQLERRM;
          g_message := 'When others getting l_organization_id ' || g_call_point || g_sqlcode || g_sqlerrm;
          debug_log(g_message);
          RAISE g_exception;
      END;
      
      
      
      BEGIN
        g_call_point := 'Converting Item to numbers';
        SELECT to_number(p_inventory_item_id)
        INTO   l_inventory_item_id
        from   dual;
      EXCEPTION
        WHEN others THEN
          l_inventory_item_id := NULL;
          g_sqlcode := SQLCODE;
          g_sqlerrm := SQLERRM;
          g_message := 'When others getting l_inventory_item_id ' || g_call_point || g_sqlcode || g_sqlerrm;
          debug_log(g_message);
          RAISE g_exception;
      END;
      
      BEGIN
        g_call_point := 'Converting shipment_header id to number';
        SELECT to_number(p_shipment_header_id)
        INTO   l_shipment_header_id
        from   dual;
      EXCEPTION
        WHEN others THEN
          l_inventory_item_id := NULL;
          g_sqlcode := SQLCODE;
          g_sqlerrm := SQLERRM;
          g_message := 'When others getting l_shipment_header_id ' || g_call_point || g_sqlcode || g_sqlerrm;
          debug_log(g_message);
          RAISE g_exception;
      END;
     
      BEGIN
        g_call_point := 'Converting User Id numbers';
        SELECT to_number(p_user_id)
        INTO   l_user_id 
        from   dual;
      EXCEPTION
        WHEN others THEN
          l_user_id := NULL;
          g_sqlcode := SQLCODE;
          g_sqlerrm := SQLERRM;
          g_message := 'When others getting l_user_id ' || g_call_point || g_sqlcode || g_sqlerrm;
          debug_log(g_message);
          RAISE g_exception;
      END;
      
     
     IF p_value = 'C' OR p_value = 'H' OR p_value IS NULL OR p_value = '' THEN
     
       BEGIN
         UPDATE rcv_shipment_lines  
            SET   attribute1 = p_value,
                  last_update_date = SYSDATE,
                  last_updated_by = l_user_id
         WHERE  shipment_header_id = l_shipment_header_id
         AND    item_id = l_inventory_item_id
         AND    to_organization_id = l_organization_id;
         
       EXCEPTION
        WHEN others THEN
              g_sqlcode := SQLCODE;
              g_sqlerrm := SQLERRM;
              g_message := 'Error updating rcv_shipment_lines ' || g_sqlcode || g_sqlerrm;
              debug_log(g_message);
              ROLLBACK;
              raise g_exception;
       END;
        

      COMMIT;

      x_return := 0;
      x_message := 'Success ';

   ELSE
    
      x_return := 1;
      x_message := 'Value is not C, H, or blank.  Unable to update';

   END IF;

   EXCEPTION
    when g_exception then
    
      x_return := 1;
      x_message := 'Error ' || g_message;
      
               xxcus_error_pkg.xxcus_error_main_api (  p_called_from         => g_package||'.'||g_call_from
                                                       , p_calling             => g_call_point
                                                       , p_ora_error_msg       => SQLERRM
                                                       , p_error_desc          => 'Error in call ' || g_call_from || g_call_point
                                                       , p_distribution_list   => g_distro_list
                                                       , p_module              => g_module);
      
    when others then
      x_return := 1;
      x_message := 'Error ' || g_message;
      
           xxcus_error_pkg.xxcus_error_main_api (  p_called_from         => g_package||'.'||g_call_from
                                                       , p_calling             => g_call_point
                                                       , p_ora_error_msg       => SQLERRM
                                                       , p_error_desc          => 'Error in call ' || g_call_from || g_call_point
                                                       , p_distribution_list   => g_distro_list
                                                       , p_module              => g_module);
        
    
    END PROCESS_UPDATE_RTV_DFF;

     /*****************************************************************************************************************************************
     *  PROCEDURE PROCESS_UBD_CCR                                                                                                             *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure used to submit the XXWC UBD Label Report from a RF Device                                                  *
     *                                                                                                                                        *
     *    Parameters: p_date    IN VARCHAR2 --Use by date                                                                                     *                                        *
     *                p_label_copies    IN VARCHAR2 --Number of Copies                                                                        *
     *                p_status_flag     IN VARCHAR2 --F or S -- parameters to determine label size                                            *
     *    Out : x_return -- 0 = Success, 1 = Warning, 2 = Error                                                                               *
     *          x_message --Message out                                                                                                       *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        06-FEB-201  Lee Spitzer               TMS Ticket 20141001-00198                                                           *
     *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
     *****************************************************************************************************************************************/

     PROCEDURE PROCESS_UBD_CCR
                                                 ( p_date               IN  VARCHAR2
                                                 , p_label_copies       IN  VARCHAR2
                                                 , p_status_flag        IN VARCHAR2
                                                 , x_return             OUT NUMBER
                                                 , x_message            OUT VARCHAR2
                                                 )
      IS
      
      l_date                VARCHAR2(50); --convert to report date format;
      l_label_copies        NUMBER;
      l_status_flag         VARCHAR2(1);
      l_printer             VARCHAR2 (30);
      l_set_print_options   BOOLEAN;
      l_set_mode            BOOLEAN;
      xml_layout            BOOLEAN;
      x_request_id          NUMBER;
      
    BEGIN
    
       g_call_from := 'PROCESS_UBD_CCR';
       g_call_point := 'Start';
      
         debug_log('p_date ' || p_date);
         debug_log('p_label_copies ' || p_label_copies);
         debug_log('p_status_flag ' || p_status_flag);
       
      g_call_point := 'get date format';
      
      BEGIN
        SELECT to_char(to_date(p_date), 'YYYY/MM/DD HH:MI:SS') 
        into   l_date
        from dual;
      EXCEPTION
        WHEN others THEN
              l_date := NULL;
              g_sqlcode := SQLCODE;
              g_sqlerrm := SQLERRM;
              g_message := 'Error convertng date format ' || g_sqlcode || g_sqlerrm;
              debug_log(g_message);
              raise g_exception;
      END;
      
        debug_log('l_date ' || l_date);
        
      g_call_point := 'get label copies';
      
      BEGIN
        select p_label_copies
        into   l_label_copies
        FROM   dual;
      exception
        when others then
              l_label_copies := 0;
              g_sqlcode := SQLCODE;
              g_sqlerrm := SQLERRM;
              g_message := 'Error getting label copies ' || g_sqlcode || g_sqlerrm;
              debug_log(g_message);
              raise g_exception;
      end;
      
        debug_log('l_label_copies ' || l_label_copies);
        
      IF l_label_copies < 0 THEN 
        g_message := 'Number of copies has to be greater or equal than 0';
        raise g_exception;
      end if;
      
      
      g_call_point := 'get flex value';
      
      BEGIN
        SELECT ffvv.flex_value
        into   l_status_flag
        FROM   FND_FLEX_VALUE_SETS ffvs,
                 FND_FLEX_VALUES_VL ffvv
        WHERE  ffvs.flex_value_set_name = 'XXWC_INV_LOT_FLAG'
        AND    ffvs.flex_value_set_id = ffvv.flex_value_set_id
        AND    ffvv.enabled_flag = 'Y'
        AND    SYSDATE BETWEEN nvl(ffvv.start_date_active, SYSDATE-1) AND nvl(ffvv.end_date_active,SYSDATE + 1)
        AND    ffvv.flex_value = p_status_flag;
       exception
        WHEN others THEN
              l_status_flag := 'F';
              g_sqlcode := SQLCODE;
              g_sqlerrm := SQLERRM;
              g_message := 'Error l_status_flag ' || g_sqlcode || g_sqlerrm;
              debug_log(g_message);
              raise g_exception;
      END;
      
        debug_log('l_status_flag ' || l_status_flag);
        
      g_call_point := 'get printer value';
      
      BEGIN
        l_printer := fnd_profile.VALUE ('PRINTER');
      EXCEPTION
        WHEN others THEN
             l_printer := 'noprint';
              g_sqlcode := SQLCODE;
              g_sqlerrm := SQLERRM;
              g_message := 'Error l_printer ' || g_sqlcode || g_sqlerrm;
              debug_log(g_message);
              raise g_exception;
      END;
      
         l_set_print_options :=
            fnd_request.set_print_options (printer               => l_printer,
                                           style                 => NULL,
                                           copies                => l_label_copies,
                                           save_output           => TRUE,
                                           print_together        => 'N',
                                           validate_printer      => 'RESOLVE'
                                          );
         xml_layout :=
            fnd_request.add_layout ('XXWC',
                                    'XXWC_UBD_LABELS',
                                    'en',
                                    'US',
                                    'PDF'
                                   );
         x_request_id :=
            fnd_request.submit_request
                                   (application      => 'XXWC',
                                    program          => 'XXWC_UBD_LABELS',
                                    description      => 'XXWC UBD Label Report',
                                    sub_request      => FALSE,
                                    argument1        => l_date,
                                    argument2        => l_label_copies,
                                    argument3        => l_status_flag
                                   );
      
      
        COMMIT;
        
        
        x_return := 0;
        x_message := 'Success Request ' || x_request_id || ' to printer ' || l_printer;

    EXCEPTION
    
    when g_exception then
    
      x_return := 2;
      x_message := 'Error ' || g_message;
      
           xxcus_error_pkg.xxcus_error_main_api (  p_called_from        => g_package||'.'||g_call_from
                                                , p_calling             => g_call_point
                                                , p_ora_error_msg       => SQLERRM
                                                , p_error_desc          => 'Error in call ' || g_call_from || g_call_point
                                                , p_distribution_list   => g_distro_list
                                                , p_module              => g_module);

    
    WHEN others THEN
      x_return := 2;
      x_message := 'Error ' || g_message;
      
           xxcus_error_pkg.xxcus_error_main_api (  p_called_from        => g_package||'.'||g_call_from
                                                , p_calling             => g_call_point
                                                , p_ora_error_msg       => SQLERRM
                                                , p_error_desc          => 'Error in call ' || g_call_from || g_call_point
                                                , p_distribution_list   => g_distro_list
                                                , p_module              => g_module);

      
    END PROCESS_UBD_CCR;

     /*****************************************************************************************************************************************
     *  FUNCTION CHECK_ITEM_IS_UBD                                                                                                            *
     *                                                                                                                                        *
     *   PURPOSE:   This function is to check if the item is assigned to the time sensitive category and if yes then return Y, otherwise N    *
     *                                                                                                                                        *
     *    Parameters: p_organization_id IN  VARCHAR2                                                                                          *
     *                p_inventory_item_id IN VARCHAR2                                                                                         *
     *    Return : Y or N                                                                                                                     *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
     *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
     *   1.1        01-JUN-2015  Lee Spitzer               TMS Ticket 20150604-00154 Bundle 1                                                                        *
     *                                                      Change UBD label print logic to be based on shelf life days                       *
     *                                                       ( print if shelf life days <1000 and >0)                                         *
     *****************************************************************************************************************************************/


      FUNCTION CHECK_ITEM_IS_TS ( p_organization_id    IN VARCHAR2
                                , p_item     IN VARCHAR2)
              RETURN VARCHAR2
      IS
      
        l_category_set_name     VARCHAR2(30) := 'Time Sensitive';
        l_yes_no                VARCHAR2(1) DEFAULT 'N';
        l_category_value        VARCHAR2(3) DEFAULT 'No';
        l_inventory_item_id     NUMBER;
        l_organization_Id       NUMBER;
        l_shelf_life_days       NUMBER; --Added TMS Ticket 20150604-00154 - 01-JUN-2015
      
      BEGIN
      
        g_call_from := 'CHECK_ITEM_IS_TS';
        g_call_point := 'Start';
      
          debug_log('p_organization_id ' || p_organization_id);
          debug_log('p_item ' || p_item);
       
        
        g_call_point := 'Convert organization id to number';
        
        BEGIN
          SELECT to_number(p_organization_id)
          INTO   l_organization_Id
          from   dual;
        END;
        
         debug_log('l_organization_id ' || l_organization_id);
       
      
        BEGIN
          --SELECT inventory_item_id -- Removed TMS Ticket 20150604-00154 01-JUN-2015
          --into   l_inventory_item_id --Removed TMS Ticket 20150604-00154 01-JUN-2015
          SELECT inventory_item_id, shelf_life_days --Added 20150604-00154 TMS Ticket 01-JUN-2015
          into   l_inventory_item_id, l_shelf_life_days --Added 20150604-00154 TMS Ticket 01-JUN-2015
          FROM   mtl_system_items_kfv
          WHERE  concatenated_segments = p_item
          and    organization_id = l_organization_id;
        exception
          WHEN others THEN
              l_inventory_item_id := NULL;
              l_shelf_life_days := 0; --Added TMS Ticket 20150604-00154 01-JUN-2015
        end;
        
        
        debug_log('l_inventory_item_id ' || l_inventory_item_id); --Added TMS Ticket 20150604-00154 01-JUN-2015
        debug_log('l_shelf_life_days ' || l_shelf_life_days); --Added TMS Ticket 20150604-00154 01-JUN-2015
        
        g_call_point := 'Get category_value';
      
        BEGIN
        
            SELECT mck.concatenated_segments
            into   l_category_value
            FROM   mtl_category_sets mcs,
                   mtl_item_categories mic,
                   mtl_categories_kfv mck
            WHERE  mcs.category_set_name = l_category_set_name
            AND    mcs.category_set_id = mic.category_set_id
            AND    mic.category_id = mck.category_id
            AND    mic.inventory_item_id = l_inventory_item_id
            AND    mic.organization_id = l_organization_Id;
        EXCEPTION
          WHEN others THEN
              g_sqlcode := sqlcode;
              g_sqlerrm := sqlerrm;
              g_message := 'Error getting l_category_value ' || g_sqlcode || g_sqlerrm;
              debug_log(g_message);
              l_category_value := 'No';
        END;
        
        debug_log('l_category_value ' || l_category_value);
        
        IF upper(l_category_value) = 'YES' THEN
        
            l_yes_no := 'Y';
            
        ELSE
        
            l_yes_no := 'N';
        
        END IF;
        
        debug_log('l_yes_no ' || l_yes_no);
        
        
        --Added TMS Ticket 20150604-00154 01-JUN-2015 if logic
        g_call_point := 'Validate Shelf Life Days';
        
        if l_yes_no =  'N' THEN --If the category code is set to N but shelf life days is greater than 0 and less than or equal to 1000, that should be flagged as Y
        
          if l_shelf_life_days > 0 and l_shelf_life_days  <=1000 then
          
              l_yes_no := 'Y';
        
          else
          
              l_yes_no := 'N';
        
          end if;
        
        --Added TMS Ticket 01-JUN-2015 20150604-00154 - If the value was already set to Y then keep it set to Y
        else
        
          l_yes_no := 'Y';
          
        end if;
        
        debug_log('l_yes_no after shelf life validating ' || l_yes_no); 
        
        --End Added TMS Ticket 20150604-00154 01-JUN-2015
        
        
        return l_yes_no;
        
     EXCEPTION   
     
     WHEN others THEN
     
      RETURN l_yes_no;
      
      g_message := 'Error ' || g_message;
      
           xxcus_error_pkg.xxcus_error_main_api (  p_called_from        => g_package||'.'||g_call_from
                                                , p_calling             => g_call_point
                                                , p_ora_error_msg       => SQLERRM
                                                , p_error_desc          => 'Error in call ' || g_call_from || g_call_point
                                                , p_distribution_list   => g_distro_list
                                                , p_module              => g_module);

      
      END CHECK_ITEM_IS_TS;
      
     /*****************************************************************************************************************************************
     *  FUNCTION DEFAULT_ITEM_UBD                                                                                                             *
     *                                                                                                                                        *
     *   PURPOSE:   This function is to calculate the UBD is assigned to the time sensitive category and if yes then return Y, otherwise N    *
     *                                                                                                                                        *
     *    Parameters: p_organization_id IN  VARCHAR2                                                                                          *
     *                p_inventory_item_id IN VARCHAR2                                                                                         *
     *    Return : Y or N                                                                                                                     *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
     *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
     *****************************************************************************************************************************************/


      FUNCTION DEFAULT_ITEM_UBD ( p_organization_id     IN VARCHAR2
                                , p_item                IN VARCHAR2)
              RETURN VARCHAR2
      IS
      
        l_category_set_name     VARCHAR2(30) := 'Time Sensitive';
        l_yes_no                VARCHAR2(1) DEFAULT 'N';
        l_category_value        VARCHAR2(3) DEFAULT 'No';
        l_inventory_item_id     NUMBER;
        l_organization_Id       NUMBER;
        l_ubd                   VARCHAR2(25);
        
      
      BEGIN
      
        g_call_from := 'DEFAULT_ITEM_UBD';
        g_call_point := 'Start';
      
          debug_log('p_organization_id ' || p_organization_id);
          debug_log('p_item ' || p_item);
       
        
        g_call_point := 'Convert organization id to number';
        
        BEGIN
          SELECT to_number(p_organization_id)
          INTO   l_organization_Id
          from   dual;
        END;
        
         debug_log('l_organization_id ' || l_organization_id);
       
      
        BEGIN
          SELECT inventory_item_id, trunc(SYSDATE) + shelf_life_days
          into   l_inventory_item_id, l_ubd
          FROM   mtl_system_items_kfv
          WHERE  concatenated_segments = p_item
          and    organization_id = l_organization_id;
        exception
          WHEN others THEN
              l_inventory_item_id := NULL;
              l_ubd := trunc(SYSDATE);
        end;
      
        debug_log('l_ubd ' || l_ubd);
        debug_log('l_inventory_item_id ' || l_inventory_item_id);
        
        
        RETURN l_ubd;
        
     EXCEPTION   
     
     WHEN others THEN
     
      RETURN l_ubd;
      
      g_message := 'Error ' || g_message;
      
           xxcus_error_pkg.xxcus_error_main_api (  p_called_from        => g_package||'.'||g_call_from
                                                , p_calling             => g_call_point
                                                , p_ora_error_msg       => SQLERRM
                                                , p_error_desc          => 'Error in call ' || g_call_from || g_call_point
                                                , p_distribution_list   => g_distro_list
                                                , p_module              => g_module);

    END DEFAULT_ITEM_UBD;
    
     /*****************************************************************************************************************************************
     *  PROCEDURE GET_TS_LABEL_LOV                                                                                                            *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure used for list of values for time sensitive label list of valus on the mobile device, C or H                *
     *                                                                                                                                        *
     *    Parameters: None                                                                                                                    *
     *    Return : x_flexvalus                                                                                                                *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        06-FEB-2015  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
     *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
     *****************************************************************************************************************************************/

  PROCEDURE GET_TS_LABEL_LOV ( x_flexvalues        OUT    NOCOPY t_genref --0
                        , p_flex_value             IN     VARCHAR2
                        ) 
            IS
  
  BEGIN               

      g_call_from := 'GET_TS_LABEL_LOV';
      g_call_point := 'Start';
 
    
        IF g_debug = 'Y' THEN
              debug_log('p_flex_value ' || p_flex_value);
      
        END IF;
    
     --Added 5/12/2015 to prevent open cursors
        if x_flexvalues%isOPEN then
           debug_log('closing x_flexvalues cursor');
            close x_flexvalues;
        
        end if;
        
        
        OPEN x_flexvalues FOR
        SELECT   ffvv.flex_value,
                 ffvv.flex_value_meaning,
                 ffvv.description
          FROM   FND_FLEX_VALUE_SETS ffvs,
                 FND_FLEX_VALUES_VL ffvv
          WHERE  ffvs.flex_value_set_name = 'XXWC_INV_LOT_FLAG'
          AND    ffvs.flex_value_set_id = ffvv.flex_value_set_id
          AND    ffvv.enabled_flag = 'Y'
          AND    SYSDATE BETWEEN nvl(ffvv.start_date_active, SYSDATE-1) AND nvl(ffvv.end_date_active,SYSDATE + 1)
          AND    ffvv.flex_value like nvl(p_flex_value, ffvv.flex_value)
          ;
          
          
          

    EXCEPTION
        WHEN others THEN
          g_sqlcode := SQLCODE;
          g_sqlerrm := SQLERRM;
          g_message := 'When others list of values for XXWC_INV_LOT_FLAG valueset ' || g_call_point || g_sqlcode || g_sqlerrm;
          debug_log(g_message);
            
             --Added 5/12/2015 to prevent open cursors
          if x_flexvalues%isOPEN then
             debug_log('closing x_flexvalues cursor');
              close x_flexvalues;
          
          end if;
        
   
  END GET_TS_LABEL_LOV;
  
  
       /*****************************************************************************************************************************************
     *  FUNCTION WC_BRANCH_LOCATOR_CONTROL                                                                                                    *
     *                                                                                                                                        *
     *   PURPOSE:   This function is to return if the subinventory has locators assigned to it                                                *
     *                                                                                                                                        *
     *    Parameters: p_organization_id IN  VARCHAR2                                                                                          *
     *                p_subinventory_code IN VARCHAR2                                                                                         *
     *    Return : Y or N                                                                                                                     *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        09-Feb-2014  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
     *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
     *****************************************************************************************************************************************/


      FUNCTION WC_BRANCH_LOCATOR_CONTROL ( p_organization_id      IN NUMBER
                                         , p_subinventory_code   IN VARCHAR2)
              RETURN NUMBER
      IS
      
        l_count NUMBER DEFAULT 0; 
        
      BEGIN
        
        g_call_from := 'WC_BRANCH_LOCATOR_CONTROL';
        g_call_point := 'Start';
 
    
        IF g_debug = 'Y' THEN
              debug_log('p_organization_id ' || p_organization_id);
              debug_log('p_subinventory_code ' || p_subinventory_code);
        END IF;
    
        
        g_call_point := 'Get count on WMS_ITEM_LOCATIONS_KFV';
        
        BEGIN
          SELECT count(*)
          INTO   l_count
          FROM   wms_item_locations_kfv
          WHERE  organization_id = p_organization_id
          AND    subinventory_code = p_subinventory_code;
        exception
          WHEN others THEN
              g_sqlcode := SQLCODE;
              g_sqlerrm := SQLERRM;
              g_message := 'Error getting count for organization_id ' || p_organization_id || ' and subinventory_code ' || p_subinventory_code;
              raise g_exception;
        end;
              
          debug_log('l_count ' || l_count);
          
        return l_count;
        
      EXCEPTION
        
        WHEN g_exception THEN
        
          RETURN l_count;
          
            debug_log('l_count ' || l_count);
          
            g_message := g_message || ' ' || g_call_point || g_sqlcode || g_sqlerrm;
            xxcus_error_pkg.xxcus_error_main_api (  p_called_from        => g_package||'.'||g_call_from
                                                , p_calling             => g_call_point
                                                , p_ora_error_msg       => SQLERRM
                                                , p_error_desc          => 'Error in call ' || g_call_from || g_call_point
                                                , p_distribution_list   => g_distro_list
                                                , p_module              => g_module);

        
        WHEN others THEN
         
         RETURN l_count;
          
            debug_log('l_count ' || l_count);
          
            g_message := 'Error ' || g_message;
      
           xxcus_error_pkg.xxcus_error_main_api (  p_called_from        => g_package||'.'||g_call_from
                                                , p_calling             => g_call_point
                                                , p_ora_error_msg       => SQLERRM
                                                , p_error_desc          => 'Error in call ' || g_call_from || g_call_point
                                                , p_distribution_list   => g_distro_list
                                                , p_module              => g_module);

      
      END WC_BRANCH_LOCATOR_CONTROL;

     /*****************************************************************************************************************************************
     *   PROCEDURE DELETE_ITEM_LOCATOR_TIE                                                                                                    *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure is used to remove records into the MTL_SECONDARY_LOCATORS                                                  *
     *                                                                                                                                        *
     *    Parameters: p_organization_id  IN NUMBER - organization_id                                                                          *
     *                p_inventory_item_id IN NUMBER -- inventory_item_id                                                                      *
     *                p_subinventory_code IN VARCHAR2 - subinventory_code                                                                     *
     *                p_locator_id IN NUMBER --LOCATOR_ID                                                                                     *
     *                                                                                                                                        *
     *    Out :  X_RETURN = 0 = Success, 1 = Error                                                                                            *
     *           X_MESSAGE = Return message                                                                                                   *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
     *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
     *********y********************************************************************************************************************************/

        PROCEDURE DELETE_ITEM_LOCATOR_TIE        ( p_organization_id    IN NUMBER
                                                 , p_inventory_item_id  IN NUMBER
                                                 , p_subinventory_code  IN VARCHAR2
                                                 , p_locator_id IN NUMBER
                                                 , x_return  OUT NUMBER
                                                 , x_message OUT VARCHAR2)
              IS
        
        BEGIN
        
        
        g_call_from := 'DELETE_ITEM_LOCATOR_TIE';
        g_call_point := 'Start';
 
    
        IF g_debug = 'Y' THEN
              debug_log('p_organization_id ' || p_organization_id);
              debug_log('p_inventory_item_id ' || p_inventory_item_id);
              debug_log('p_subinventory_code ' || p_subinventory_code);
              debug_log('p_locator_id ' || p_locator_id);
        END IF;
    
        
        g_call_point := 'Delete Secondary Locator Record';
        
            BEGIN
              DELETE FROM MTL_SECONDARY_LOCATORS
                 WHERE organization_id = p_organization_id
                   AND secondary_locator = p_locator_id
                   AND inventory_item_id = p_inventory_item_id
                   AND subinventory_code = nvl(p_subinventory_code,subinventory_code);
            Exception
                WHEN OTHERS THEN 
                   g_sqlcode := SQLCODE;
                   g_sqlerrm := SQLERRM;
                   g_message := 'Error deleting MTL_SECONDARY_LOCATORS ' || g_sqlcode || g_sqlerrm;
                   raise g_exception;
             END;
             
            COMMIT;
            
        x_return := 0;
        x_message := 'Deleted';
        
      EXCEPTION
        
        WHEN g_exception THEN
        
            g_message := g_message || ' ' || g_call_point || g_sqlcode || g_sqlerrm;
          
            debug_log(g_message);
          
            ROLLBACK;
            
            x_return := 1;
            x_message := 'Error ' || g_message;
       
          
            xxcus_error_pkg.xxcus_error_main_api (  p_called_from        => g_package||'.'||g_call_from
                                                , p_calling             => g_call_point
                                                , p_ora_error_msg       => SQLERRM
                                                , p_error_desc          => 'Error in call ' || g_call_from || g_call_point
                                                , p_distribution_list   => g_distro_list
                                                , p_module              => g_module);

        
        WHEN others THEN
          
            g_message := g_message || ' ' || g_call_point || g_sqlcode || g_sqlerrm;
          
            debug_log(g_message);
            
            x_return := 1;
            x_message := 'When others error ' || g_message;

            ROLLBACK;       
         
           xxcus_error_pkg.xxcus_error_main_api (  p_called_from        => g_package||'.'||g_call_from
                                                , p_calling             => g_call_point
                                                , p_ora_error_msg       => SQLERRM
                                                , p_error_desc          => 'Error in call ' || g_call_from || g_call_point
                                                , p_distribution_list   => g_distro_list
                                                , p_module              => g_module);

      
      END;
      
      /*****************************************************************************************************************************************
     *  FUNCTION WC_LOCATOR                                                                                                                   *
     *                                                                                                                                        *
     *   PURPOSE:   This function is to return if the locator exists                                                                          *
     *                                                                                                                                        *
     *    Parameters: p_organization_id IN  VARCHAR2                                                                                          *
     *                p_subinventory_code IN VARCHAR2                                                                                         *
     *    Return : Count of locators tied to subinventory for the organization                                                                *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        09-Feb-2014  Lee Spitzer            TMS Ticket 20141001-00198                                                          *
     *                                                  WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
     *   1.1        09-Mar-2017  Ashwin Sridhar         Added Debug messages and logic for TMS#20170126-00027
     *****************************************************************************************************************************************/


      FUNCTION WC_LOCATOR ( p_locator              IN VARCHAR2)
              RETURN VARCHAR2 IS
              
          l_locator VARCHAR2(40) DEFAULT p_locator;
          --l_organization_id NUMBER; --Commented by Ashwin.s on 09-Mar-2017 for TMS#20170126-00027
          ln_user_id NUMBER;          --Added by Ashwin.s on 09-Mar-2017 for TMS#20170126-00027
          lv_user_name VARCHAR2(100); --Added by Ashwin.s on 09-Mar-2017 for TMS#20170126-00027
          
      BEGIN
      
      g_call_from := 'WC_LOCATOR';
      g_call_point := 'Start';
      ln_user_id:=fnd_profile.value('USER_ID'); --Added by Ashwin.s on 09-Mar-2017 for TMS#20170126-00027
      
      
        debug_log('p_locator ' || p_locator);

      --Added the IF condition by Ashwin.s on 09-Mar-2017 for TMS#20170126-00027
      IF ln_user_id IS NOT NULL THEN
  
        BEGIN
  
          SELECT user_name
          INTO   lv_user_name
          FROM   fnd_user
          WHERE user_id=ln_user_id;

        EXCEPTION
        WHEN others THEN

          lv_user_name:=null;

        END;
  
      END IF;        
        
        BEGIN
          SELECT substr(p_locator,instr(p_locator,'-',1,1)+1, length(p_locator) -  instr(p_locator,'-',1,1)) 
          INTO  l_locator
          FROM  dual;
        EXCEPTION
          WHEN others THEN
            g_sqlcode := sqlcode;
            g_sqlerrm := sqlerrm;
            g_message := 'Error substr locator ' || p_locator;
            raise g_exception;
         END;
        
        --Added this debug for locator by Ashwin.S on 09-Mar-2017 for TMS#20170126-00027
        debug_log('l_locator ' || l_locator);
                                                                
        return l_locator;
        
    EXCEPTION
    
        WHEN g_exception THEN
            
            --Added this debug for locator by Ashwin.S on 09-Mar-2017 for TMS#20170126-00027
            g_message := g_message||' Error in WC_LOCATOR ' ||' for user '||lv_user_name||' '||g_sqlcode || g_sqlerrm;
            
                xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                     , p_calling             => g_call_point
                                                     , p_ora_error_msg       => SQLERRM
                                                     , p_error_desc          => g_message
                                                     , p_distribution_list   => g_distro_list
                                                     , p_module              => g_module);

          return l_locator;

          WHEN OTHERS THEN
            
            g_sqlcode := SQLCODE;
            g_sqlerrm := SQLERRM;
            --Added this debug for locator by Ashwin.S on 09-Mar-2017 for TMS#20170126-00027
            g_message := g_message||' Error in WC_LOCATOR ' ||'-'||' for parameter Locator '||p_locator||' for user '||lv_user_name||' '||g_sqlcode || g_sqlerrm;
            
                xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                     , p_calling             => g_call_point
                                                     , p_ora_error_msg       => SQLERRM
                                                     , p_error_desc          => g_message
                                                     , p_distribution_list   => g_distro_list
                                                     , p_module              => g_module);

       return l_locator;
      
      END WC_LOCATOR;
      
    /*****************************************************************************************************************************************
     *  FUNCTION WC_LOCATOR_PREFIX                                                                                                            *
     *                                                                                                                                        *
     *   PURPOSE:   This function is to return if the locator exists                                                                          *
     *                                                                                                                                        *
     *    Parameters: p_organization_id IN  VARCHAR2                                                                                          *
     *                p_subinventory_code IN VARCHAR2                                                                                         *
     *    Return : Count of locators tied to subinventory for the organization                                                                *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        09-Feb-2014  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
     *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
     *****************************************************************************************************************************************/


      FUNCTION WC_LOCATOR_PREFIX ( p_locator              IN VARCHAR2)
              RETURN VARCHAR2 IS
              
          l_prefix VARCHAR2(40) DEFAULT '';
          
      BEGIN
      
      g_call_from := 'WC_LOCATOR_PREFIX';
      g_call_point := 'Start';
      
      
        debug_log('p_locator ' || p_locator);
        
        BEGIN
          SELECT substr(p_locator, 1, instr(p_locator,'-',1,1)-1)
          INTO   l_prefix
          from dual;
        EXCEPTION
          WHEN others THEN
              g_sqlcode := SQLCODE;
              g_sqlerrm := SQLERRM;
              g_message := 'Error getting locator prefix';
              raise g_exception;
        END;
        
        return l_prefix;
        
    EXCEPTION
    
        WHEN g_exception THEN
            
            g_message := 'Error in WC_LOCATOR ' || g_sqlcode || g_sqlerrm;
            return l_prefix;
                xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                     , p_calling             => g_call_point
                                                     , p_ora_error_msg       => SQLERRM
                                                     , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                     , p_distribution_list   => g_distro_list
                                                     , p_module              => g_module);
          WHEN OTHERS THEN
            
            g_sqlcode := SQLCODE;
            g_sqlerrm := SQLERRM;
            g_message := 'Error in WC_LOCATOR ' || g_sqlcode || g_sqlerrm;
            return l_prefix;
                xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                     , p_calling             => g_call_point
                                                     , p_ora_error_msg       => SQLERRM
                                                     , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                     , p_distribution_list   => g_distro_list
                                                     , p_module              => g_module);
      
      END WC_LOCATOR_PREFIX;
      
     /*****************************************************************************************************************************************
     *  PROCEDURE CREATE_WC_LOCATOR                                                                                                           *
     *                                                                                                                                        *
     *   PURPOSE:   This function is to return if the locator exists                                                                          *
     *                                                                                                                                        *
     *    Parameters: p_organization_id IN  VARCHAR2                                                                                          *
     *                p_subinventory_code IN VARCHAR2                                                                                         *
     *    Return : Count of locators tied to subinventory for the organization                                                                *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        09-Feb-2014  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
     *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
     *   1.1        08-JUL-2015  Lee Spitzer               TMS Ticket 20150714-00142 RF - Implement fixes to include with release 2           *
     *****************************************************************************************************************************************/


      PROCEDURE CREATE_WC_LOCATOR ( p_organization_id IN NUMBER
                                  , p_subinventory_code IN VARCHAR2
                                  , p_locator IN VARCHAR2
                                  , x_return  OUT NUMBER
                                  , x_message OUT VARCHAR2) IS
      
      
        CURSOR c_wc_locator IS
          SELECT  '0-'||p_locator||'.' wc_locator
          FROM   dual
          UNION
          SELECT  '1-'||p_locator||'.' wc_locator
          FROM   dual
          UNION
          select  '2-'||p_locator||'.' wc_locator
          FROM   dual
          UNION
          SELECT '3-'||p_locator||'.' wc_locator
         FROM   dual;
      
          l_exists NUMBER;
          l_return_status           VARCHAR2 (1) := fnd_api.g_ret_sts_success;
          l_msg_count               NUMBER;
          l_msg_data                VARCHAR2 (4000);
          l_inventory_location_id   NUMBER;
          l_locator_exists          VARCHAR2 (1);
          l_error_msg               VARCHAR2 (2000) := NULL;
          l_locator_id              NUMBER;
          l_count                   NUMBER DEFAULT 0;
          l_new_locators            VARCHAR2(10000) DEFAULT '';
          l_reason_id               NUMBER; --Added 01-JUN-2015 TMS Ticket 20150714-00142 RF - Implement fixes to include with release 2 

      
      BEGIN
      
       g_call_from := 'CREATE_WC_LOCATOR';
       g_call_point := 'Start';
      
        debug_log('p_organization_id ' || p_organization_id);
        debug_log('p_subinventory_code ' || p_subinventory_code);
        debug_log('p_locator ' || p_locator);
        
        
       g_call_point := 'Entering loop to check locators exists';
       
            FOR r_wc_locator in c_wc_locator
              LOOP
                
                g_call_point := 'Check if locators exists in org';
                debug_log(g_call_point);
        
                
                BEGIN
                  SELECT count(*)
                  INTO   l_exists
                  FROM   wms_item_locations_kfv wil
                  WHERE  wil.organization_id = p_organization_id
                  and    wil.concatenated_segments = r_wc_locator.wc_locator;
              
                EXCEPTION
                  WHEN others THEN
                      g_sqlcode := SQLCODE;
                      g_sqlerrm := SQLERRM;
                      g_message := 'Error getting count';
                      raise g_exception;
                END;
              
                debug_log('l_exists ' || l_exists || ' for locator ' || r_wc_locator.wc_locator);
                
                --if the locator doesn't exists in the org
                if l_exists = 0 then
                
                 --Added 01-JUN-2015 TMS Ticket 20150714-00142 RF - Implement fixes to include with release 2 
                    BEGIN
          
                      SELECT reason_id
                      into   l_reason_id
                      FROM   MTL_TRANSACTION_REASONS
                      where  reason_name = '15 - RF';
              
                    EXCEPTION
                        WHEN others THEN
                              l_reason_id := NULL;
                    END;
                    --End Added 01-JUN-2015 TMS Ticket 20150714-00142 RF - Implement fixes to include with release 2 
                    
                    
                    --Create the locator
                             --mo_global.init ('INV');  
      
                            l_error_msg := NULL;
                            l_inventory_location_id := NULL;
                            l_locator_exists := NULL;
                            l_msg_data := NULL;
                            l_msg_count := NULL;
                            l_return_status := NULL;

                    inv_loc_wms_pub.create_locator (
                          x_return_status              => l_return_status,
                          x_msg_count                  => l_msg_count,
                          x_msg_data                   => l_msg_data,
                          x_inventory_location_id      => l_inventory_location_id,
                          x_locator_exists             => l_locator_exists,
                          p_organization_id            => p_organization_id,
                          p_organization_code          => NULL,
                          p_concatenated_segments      => r_wc_locator.wc_locator, 
                          p_description                => '', 
                          p_inventory_location_type    => NULL,                     --3,
                          p_picking_order              => NULL,
                          p_location_maximum_units     => NULL,
                          p_subinventory_code          => p_subinventory_code,
                          p_location_weight_uom_code   => NULL,
                          p_max_weight                 => NULL,
                          p_volume_uom_code            => NULL,
                          p_max_cubic_area             => NULL,
                          p_x_coordinate               => NULL,
                          p_y_coordinate               => NULL,
                          p_z_coordinate               => NULL,
                          p_physical_location_id       => NULL,
                          p_pick_uom_code              => NULL,
                          p_dimension_uom_code         => NULL,
                          p_length                     => NULL,
                          p_width                      => NULL,
                          p_height                     => NULL,
                          p_status_id                  => 1,
                          p_dropping_order             => NULL,
                          p_attribute_category         => '',
                          --p_attribute1                 => '', --Removed 01-JUN-2015 - TMS Ticket 20150714-00142 RF - Implement fixes to include with release 2 
                          p_attribute1                 => l_reason_id, --Added 01-JUN-2015 - TMS Ticket 20150714-00142 RF - Implement fixes to include with release 2 
                          p_attribute2                 => '',
                          p_attribute3                 => '',
                          p_attribute4                 => '',
                          p_attribute5                 => '',
                          p_attribute6                 => '',
                          p_attribute7                 => '',
                          p_attribute8                 => '',
                          p_attribute9                 => '',
                          p_attribute10                => '',
                          p_attribute11                => '',
                          p_attribute12                => '',
                          p_attribute13                => '',
                          p_attribute14                => '',
                          p_attribute15                => '',
                          p_alias                      => '');
                  
                      debug_log('l_return_status ' || l_return_status);
                  
                      IF (l_return_status <> 'S')
                      THEN
                          l_count := l_count + 1;
                          FOR l_index IN 1 .. l_msg_count
                          LOOP
                              l_msg_data := fnd_msg_pub.get (l_index, 'F');
                              --l_error_msg :=                         l_error_msg || '-' || SUBSTR (l_msg_data, 1, 50);
                          END LOOP;
                          l_error_msg := l_error_msg || '-' || SUBSTR (l_msg_data, 1, 100);
                          debug_log(l_error_msg);
                          
                      END IF;
                      
                      if (l_return_status = 'S') THEN
                      
                          if l_new_locators = '' then
                          
                            l_new_locators := r_wc_locator.wc_locator;
                              
                          ELSE
                          
                            l_new_locators := l_new_locators || ' '|| r_wc_locator.wc_locator;
                          
                          end if;
                          
                      end if;
                
                END IF;
              
              END LOOP;
        
          COMMIT;
          
        x_return := l_count;
        
        
        IF l_count > 0 THEN
            x_message := 'Error creating locators ' || l_error_msg;
        ELSE
            x_message := 'Success ' || l_new_locators;
        END IF;
        
      EXCEPTION
    
        WHEN g_exception THEN
            
            g_message := 'Error in CREATE_WC_LOCATOR ' || g_sqlcode || g_sqlerrm;
            x_return := 1;
            x_message := g_message;
                xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                     , p_calling             => g_call_point
                                                     , p_ora_error_msg       => SQLERRM
                                                     , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                     , p_distribution_list   => g_distro_list
                                                     , p_module              => g_module);
          WHEN OTHERS THEN
            
            g_sqlcode := SQLCODE;
            g_sqlerrm := SQLERRM;
            g_message := 'Error in CREATE_WC_LOCATOR ' || g_sqlcode || g_sqlerrm;
            x_return := 1;
            x_message := g_message;
                xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                     , p_calling             => g_call_point
                                                     , p_ora_error_msg       => SQLERRM
                                                     , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                     , p_distribution_list   => g_distro_list
                                                     , p_module              => g_module);
      
    
      
      END CREATE_WC_LOCATOR;
      
     /*****************************************************************************************************************************************
     *  PROCEDURE GET_WC_ITEM_LOV                                                                                                             *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure used for items and upc code                                                                                *
     *                                                                                                                                        *
     *    Parameters: None                                                                                                                    *
     *    Return : XXWC_LABEL_PRINTER profile                                                                                                 *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
     *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
     *****************************************************************************************************************************************/


    PROCEDURE GET_WC_ITEM_LOV ( x_item             OUT    NOCOPY t_genref --0
                              , p_organization_id  IN     VARCHAR2
                              , p_item             IN     VARCHAR2
                             )
      IS
      
      l_organization_id NUMBER;
   
   BEGIN               

      g_call_from := 'GET_WC_ITEM_LOV';
      g_call_point := 'Start';
 
    
        IF g_debug = 'Y' THEN
              debug_log('p_item ' || p_item);
      
        END IF;
    
      g_call_point := 'Convert organization_id to number';
      
      BEGIN
        SELECT to_number(p_organization_id)
        INTO   l_organization_id
        FROM   dual;
      EXCEPTION
        WHEN others THEN
              g_sqlcode := SQLCODE;
              g_sqlerrm := SQLERRM;
              g_message := 'Error converting organization_id';
              debug_log(g_message);
              raise g_exception;
      END;
    
         --Added 5/12/2015 to prevent open cursors
        if x_item%isOPEN then
           debug_log('closing x_item cursor');
            close x_item;
        
        end if;
        
        
        OPEN x_item FOR
          SELECT  msi.inventory_item_id
                , msi.concatenated_segments
                , msi.primary_uom_code
                , msi.description
                , msi.lot_control_code
                , msi.serial_number_control_code
                , msi.shelf_life_days
                , 0
          FROM   mtl_system_items_kfv msi
          WHERE  msi.organization_id = l_organization_id
          --AND    msi.concatenated_segments like nvl(p_item , msi.concatenated_segments) --removed 4/20/2015
          AND    msi.concatenated_segments = upper(p_item) --added 4/20/2015
          UNION
          SELECT  msi.inventory_item_id
                , msi.concatenated_segments
                , msi.primary_uom_code
                , msi.description
                , msi.lot_control_code
                , msi.serial_number_control_code
                , msi.shelf_life_days
                , 0
          FROM   mtl_system_items_kfv msi
          WHERE  msi.organization_id = l_organization_id
          AND    EXISTS (SELECT mcr.inventory_item_id
                         FROM   mtl_cross_references mcr
                         WHERE  1=1--msi.organization_id = fnd_profile.VALUE('XXWC_ITEM_MASTER_ORG')
                         --AND mcr.cross_reference_type = fnd_profile.VALUE('INV:GTIN_CROSS_REFERENCE_TYPE') removed 5/12/2015
                         AND mcr.organization_id IS NULL                                        
                         AND mcr.inventory_item_id = msi.inventory_item_id
                         --AND mcr.cross_reference like nvl(p_item , mcr.cross_reference)) --removed 4/20/2015
                         AND mcr.cross_reference = upper(p_item)
                         AND  exists (select mcrt.cross_reference_type --added exists cross reference type 5/12/2015 to include multiple cross reference types
                                      from   mtl_cross_reference_types mcrt
                                      where  nvl(mcrt.attribute1,'N') = 'Y'
                                      AND    mcrt.cross_reference_type = mcr.cross_reference_type)
                          ) --added 4/20/2015
          ;
          
          
          

    EXCEPTION
        WHEN g_exception THEN

            --Added 5/12/2015 to prevent open cursors
            if x_item%isOPEN then
               debug_log('closing x_item cursor');
                close x_item;
            
            end if;

            
                xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                     , p_calling             => g_call_point
                                                     , p_ora_error_msg       => SQLERRM
                                                     , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                     , p_distribution_list   => g_distro_list
                                                     , p_module              => g_module);
          
          
          
          WHEN OTHERS THEN
            
            g_sqlcode := SQLCODE;
            g_sqlerrm := SQLERRM;
            g_message := 'When others ' ||g_sqlcode || g_sqlerrm;
            
            
             --Added 5/12/2015 to prevent open cursors
              if x_item%isOPEN then
                 debug_log('closing x_item cursor');
                  close x_item;
              
              end if;
            
                xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                     , p_calling             => g_call_point
                                                     , p_ora_error_msg       => SQLERRM
                                                     , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                     , p_distribution_list   => g_distro_list
                                                     , p_module              => g_module);
      
                
     
    END GET_WC_ITEM_LOV;
 

     /*****************************************************************************************************************************************
     *  FUNCTION GET_PROFILE_VALUE                                                                                                            *
     *                                                                                                                                        *
     *   PURPOSE:   This function used to return the profile value from profile option                                                        *
     *                                                                                                                                        *
     *    Parameters: None                                                                                                                    *
     *    Return : XXWC_LABEL_PRINTER profile                                                                                                 *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
     *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
     *****************************************************************************************************************************************/


    FUNCTION GET_PROFILE_VALUE (p_profile_option_name IN VARCHAR2)
      RETURN VARCHAR2 IS
    
      l_profile_value VARCHAR2(240);
      
    BEGIN
    
      g_call_from := 'GET_PROFILE_VALUE';
      g_call_point := 'Start';
 
      debug_log('p_profile_option_name ' || p_profile_option_name);
      
      
      BEGIN
        SELECT fnd_profile.VALUE(p_profile_option_name)
        into   l_profile_value
        FROM   dual;
      EXCEPTION
        WHEN others THEN
             l_profile_value := '';
      END;
      
        debug_log('l_profile_value ' || l_profile_value);
      
      return l_profile_value;
      
    EXCEPTION
    
    WHEN OTHERS THEN
            
            g_sqlcode := SQLCODE;
            g_sqlerrm := SQLERRM;
            g_message := 'When others ' ||g_sqlcode || g_sqlerrm;
            
                xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                     , p_calling             => g_call_point
                                                     , p_ora_error_msg       => SQLERRM
                                                     , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                     , p_distribution_list   => g_distro_list
                                                     , p_module              => g_module);

    END GET_PROFILE_VALUE;
      
     /*****************************************************************************************************************************************
     *  PROCEDURE GET_WC_SUBLOV                                                                                                               *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure used for items and upc code                                                                                *
     *                                                                                                                                        *
     *    Parameters: None                                                                                                                    *
     *    Return : XXWC_LABEL_PRINTER profile                                                                                                 *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
     *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
     *   1.1        20-JUN-2015  Lee Spitzer               TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions                   *
     *****************************************************************************************************************************************/

    PROCEDURE GET_WC_SUB_LOV ( x_subinventory           OUT    NOCOPY t_genref --0
                              , p_organization_id       IN     VARCHAR2
                              , p_subinventory_code     IN     VARCHAR2
                              , p_transfer_from_sub     IN     VARCHAR2
                              )
   
    IS
    
         l_organization_id NUMBER;
         l_transfer_locator_type NUMBER; --Added 06/20/2015 TMS Ticket Number RF - Miscellaneous Transactions
   
   BEGIN               

      g_call_from := 'GET_WC_SUB_LOV';
      g_call_point := 'Start';
 
        debug_log('p_organization_id ' || p_organization_id);
        debug_log('p_subinventory_code ' || p_subinventory_code);
        debug_log('p_transfer_from_sub ' || p_transfer_from_sub);
        
    
      g_call_point := 'Convert organization_id to number';
      
      
      BEGIN
        SELECT to_number(p_organization_id)
        INTO   l_organization_id
        FROM   dual;
      EXCEPTION
        WHEN others THEN
              g_sqlcode := SQLCODE;
              g_sqlerrm := SQLERRM;
              g_message := 'Error converting organization_id';
              debug_log(g_message);
              raise g_exception;
      END;
      
      
      g_call_point := 'Get subinventory code';
      
      
            --Added 5/12/2015 to prevent open cursors
            if x_subinventory%isOPEN then
               debug_log('closing x_subinventory cursor');
                close x_subinventory;
            
            end if;



      if p_transfer_from_sub is null or p_transfer_from_sub = '' then
      
            BEGIN    
              OPEN x_subinventory FOR
                  SELECT secondary_inventory_name,
                         description,
                         locator_type,
                         asset_inventory
                  FROM   mtl_secondary_inventories
                  WHERE  organization_id = l_organization_id
                  --AND    secondary_inventory_name like nvl(p_subinventory_code, secondary_inventory_name)
                  AND    upper(secondary_inventory_name) like upper(nvl(p_subinventory_code, secondary_inventory_name)) --Added 07/08/2015 - 
                  AND    sysdate < nvl(disable_date,sysdate+1) --Added 06/20/2015 TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
                  ORDER by secondary_inventory_name;
             EXCEPTION
                WHEN others then
                    g_sqlcode := SQLCODE;
                    g_sqlerrm := SQLERRM;
                    g_message := 'When others getting subinventory code ';
                    raise g_exception;
              END;
             
      else
      
        --Added 06/20/2015 TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions; We need to know if the transfer subinventory is locator controlled or not
        BEGIN
            select locator_type
            into   l_transfer_locator_type
            from   mtl_secondary_inventories
            where  organization_id = l_organization_id
            --and    secondary_inventory_name = p_transfer_from_sub
            and    upper(secondary_inventory_name) = upper(p_transfer_from_sub) --Added 06/20/2015 TMS Ticket Number RF - Miscellaneous Transactions
            and    sysdate < nvl(disable_date,sysdate+1); --Added 06/20/2015 TMS Ticket Number RF - Miscellaneous Transactions
        EXCEPTION
            when others then
                  l_transfer_locator_type := 1;
        END;
      
        --Added 06/20/2015 TMS Ticket Number RF 20150622-00003 - Miscellaneous Transactions - If Locator Type is 1 = non locator control, then the transfer and subinventory can't be the same
        if l_transfer_locator_type = 1 then 
          BEGIN    
                OPEN x_subinventory FOR
                    SELECT secondary_inventory_name,
                           description,
                           locator_type,
                           asset_inventory
                    FROM   mtl_secondary_inventories
                    WHERE  organization_id = l_organization_id
                    --AND    secondary_inventory_name != p_transfer_from_sub
                    --AND    secondary_inventory_name like nvl(p_subinventory_code, secondary_inventory_name)
                    AND    upper(secondary_inventory_name) != upper(p_transfer_from_sub) -- Added 06/20/2015 TMS Ticket Number RF - Miscellaneous Transactions
                    AND    upper(secondary_inventory_name) like upper(nvl(p_subinventory_code, secondary_inventory_name)) --Added 06/20/2015 TMS Ticket Number RF - Miscellaneous Transactions
                    AND    sysdate < nvl(disable_date,sysdate+1)
                    ORDER by secondary_inventory_name;
               EXCEPTION
                  WHEN others then
                      g_sqlcode := SQLCODE;
                      g_sqlerrm := SQLERRM;
                      g_message := 'When others getting subinventory code ';
                      raise g_exception;
                END;

        --Added 06/20/2015 TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions - If Locator Type is greater than 1, locator control, then the transfer and subinventory can be the same
        else
        
        BEGIN    
                OPEN x_subinventory FOR
                    SELECT secondary_inventory_name,
                           description,
                           locator_type,
                           asset_inventory
                    FROM   mtl_secondary_inventories
                    WHERE  organization_id = l_organization_id
                    --AND    secondary_inventory_name like nvl(p_subinventory_code, secondary_inventory_name) -- Added 06/20/2015 TMS Ticket Number RF - Miscellaneous Transactions
                    AND    upper(secondary_inventory_name) like upper(nvl(p_subinventory_code, secondary_inventory_name)) -- Added 06/20/2015 TMS Ticket Number RF - Miscellaneous Transactions
                    and    sysdate < nvl(disable_date,sysdate+1)
                    ORDER by secondary_inventory_name;
               EXCEPTION
                  WHEN others then
                      g_sqlcode := SQLCODE;
                      g_sqlerrm := SQLERRM;
                      g_message := 'When others getting subinventory code ';
                      raise g_exception;
                END;

        
        end if;
    
    END IF;

    EXCEPTION
        WHEN g_exception THEN

            --Added 5/12/2015 to prevent open cursors
            if x_subinventory%isOPEN then
               debug_log('closing x_subinventory cursor');
                close x_subinventory;
            
            end if;


                xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                     , p_calling             => g_call_point
                                                     , p_ora_error_msg       => SQLERRM
                                                     , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                     , p_distribution_list   => g_distro_list
                                                     , p_module              => g_module);
          WHEN OTHERS THEN
            
            g_sqlcode := SQLCODE;
            g_sqlerrm := SQLERRM;
            g_message := 'When others ' ||g_sqlcode || g_sqlerrm;
            
            --Added 5/12/2015 to prevent open cursors
            if x_subinventory%isOPEN then
               debug_log('closing x_subinventory cursor');
                close x_subinventory;
            
            end if;

                xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                     , p_calling             => g_call_point
                                                     , p_ora_error_msg       => SQLERRM
                                                     , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                     , p_distribution_list   => g_distro_list
                                                     , p_module              => g_module);
      
    
    
      
    END GET_WC_SUB_LOV;

    
   /*****************************************************************************************************************************************
   *   PROCEDURE GET_OPEN_SALES_ORDERS                                                                                                      *
   *                                                                                                                                        *
   *   PURPOSE:   This procedure is used to get open_sales_orders                                                                           *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
   *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
   *****************************************************************************************************************************************/
      
    
   FUNCTION GET_OPEN_SALES_ORDERS ( p_organization_id     IN NUMBER
                                  , p_inventory_item_id   IN NUMBER
                                  , p_uom                 IN VARCHAR2
                                  )
      RETURN NUMBER IS  
      
      l_open_so NUMBER;
    
    BEGIN
    
    
        g_call_from := 'GET_OPEN_SALES_ORDERS';
        g_call_point := 'Start';
 
        debug_log('p_organization_id ' || p_organization_id);
        debug_log('p_inventory_item_id ' || p_inventory_item_id);
        debug_log('p_uom ' || p_uom);
      

         BEGIN
             SELECT NVL (SUM (l.ordered_quantity * po_uom_s.po_uom_convert (um.unit_of_measure,
                                                                            p_uom,
                                                                            l.inventory_item_id)),
                         0)-- Shankar TMS 20130124-00974 23-Jan-2013
             INTO   l_open_so
             FROM   oe_order_lines_all l, 
                    oe_order_headers_all h,
                    mtl_units_of_measure_vl um
             WHERE  l.header_id = h.header_id
             AND    h.flow_status_code NOT IN ('ENTERED', 'CLOSED', 'CANCELLED')
             AND    ((l.flow_status_code = 'AWAITING_SHIPPING'
             AND NOT EXISTS (SELECT 'Out For Delivery'
                             FROM    xxwc.xxwc_wsh_shipping_stg x1
                             WHERE   x1.header_id = l.header_id
                             AND     x1.line_id = l.line_id
                             AND      x1.ship_from_org_id = l.ship_from_org_id
                             AND     x1.status IN 'OUT_FOR_DELIVERY'))
                  OR (l.flow_status_code = 'BOOKED' AND l.line_type_id = 1005)) -- Shankar TMS 20130107-00851 17-Jan-2013
              AND l.ship_from_org_id = p_organization_id
              AND l.inventory_item_id = p_inventory_item_id
              AND l.order_quantity_uom = um.uom_code;
      EXCEPTION
        WHEN others THEN
                g_sqlcode := SQLCODE;
                g_sqlerrm := SQLERRM;
                raise g_exception;
      
      END;
      
                
      RETURN  l_open_so;
    
    EXCEPTION
    
      WHEN g_exception THEN
          
          l_open_so := 0;
          
          return l_open_so;
          
               g_message := g_sqlcode || g_sqlerrm;
            
                xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                     , p_calling             => g_call_point
                                                     , p_ora_error_msg       => SQLERRM
                                                     , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                     , p_distribution_list   => g_distro_list
                                                     , p_module              => g_module);



      WHEN others THEN
            
            l_open_so := 0;
            
            return l_open_so;
                      
            g_sqlcode := SQLCODE;
            g_sqlerrm := SQLERRM;
            g_message := 'When others ' ||g_sqlcode || g_sqlerrm;
            
                xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                     , p_calling             => g_call_point
                                                     , p_ora_error_msg       => SQLERRM
                                                     , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                     , p_distribution_list   => g_distro_list
                                                     , p_module              => g_module);
      

    
    END GET_OPEN_SALES_ORDERS;

    
   /*****************************************************************************************************************************************
   *   PROCEDURE GET_OPEN_COUNTER_ORDERS                                                                                                    *
   *                                                                                                                                        *
   *   PURPOSE:   This procedure is used to get open_counter_orders                                                                         *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
   *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
   *****************************************************************************************************************************************/
  
   
    FUNCTION GET_OPEN_COUNTER_ORDERS ( p_organization_id     IN NUMBER
                                      , p_inventory_item_id   IN NUMBER
                                      , p_uom                 IN VARCHAR2
                                      )
      RETURN NUMBER IS  
      
      l_open_so NUMBER;
    
    BEGIN
    
        g_call_from := 'GET_OPEN_COUNTER_ORDERS';
        g_call_point := 'Start';
 
        debug_log('p_organization_id ' || p_organization_id);
        debug_log('p_inventory_item_id ' || p_inventory_item_id);
        debug_log('p_uom ' || p_uom);
      

         BEGIN
             SELECT NVL (SUM (l.ordered_quantity * po_uom_s.po_uom_convert (um.unit_of_measure,
                                                                            p_uom,
                                                                            l.inventory_item_id)),
                         0)-- Shankar TMS 20130124-00974 23-Jan-2013
             INTO   l_open_so
             FROM   oe_order_lines_all l, 
                    oe_order_headers_all h,
                    mtl_units_of_measure_vl um
             WHERE  l.header_id = h.header_id
             AND h.flow_status_code NOT IN ('ENTERED', 'CLOSED', 'CANCELLED')
             AND l.line_type_id = 1005
             AND l.ship_from_org_id = p_organization_id
             AND l.inventory_item_id = p_inventory_item_id
             AND l.order_quantity_uom = um.uom_code
             ;
        EXCEPTION
          WHEN others THEN
                g_sqlcode := SQLCODE;
                g_sqlerrm := SQLERRM;
                raise g_exception;
                
        END;
          
      RETURN  l_open_so;
    
    exception
    
          WHEN g_exception THEN
          
          l_open_so := 0;
          
          return l_open_so;
          
               g_message := g_sqlcode || g_sqlerrm;
            
                xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                     , p_calling             => g_call_point
                                                     , p_ora_error_msg       => SQLERRM
                                                     , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                     , p_distribution_list   => g_distro_list
                                                     , p_module              => g_module);

      
      WHEN others THEN
            
            l_open_so := 0;
            
            return l_open_so;

            g_sqlcode := SQLCODE;
            g_sqlerrm := SQLERRM;
            g_message := 'When others ' ||g_sqlcode || g_sqlerrm;
            
                xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                     , p_calling             => g_call_point
                                                     , p_ora_error_msg       => SQLERRM
                                                     , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                     , p_distribution_list   => g_distro_list
                                                     , p_module              => g_module);
      

    END GET_OPEN_COUNTER_ORDERS;
    
   /*****************************************************************************************************************************************
   *   PROCEDURE GET_WC_OH                                                                                                                  *
   *                                                                                                                                        *
   *   PURPOSE:   This procedure is used to get the on-hand inventroy                                                                       *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
   *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
   *****************************************************************************************************************************************/
  
    PROCEDURE GET_WC_OH             ( x_oh                     OUT    NOCOPY t_genref --0
                                    , p_organization_id        IN     NUMBER    --1
                                    , p_inventory_item_id      IN     VARCHAR2  --2
                                    , p_subinventory_code      IN     VARCHAR2  --3
                                    , p_locator_id             IN     VARCHAR2  --4
                                    , p_lot_number             IN     VARCHAR2  --5
                                    , p_project_id             IN     NUMBER    --6
                                    , p_task_id                IN     NUMBER    --7
                                    ) IS
                                    
    BEGIN
    
        g_call_from := 'GET_WC_OH';
        g_call_point := 'Start';
 
        debug_log('p_organization_id ' || p_organization_id);
        debug_log('p_inventory_item_id ' || p_inventory_item_id);
        debug_log('p_subinventory_code ' || p_subinventory_code);
        debug_log('p_locator_id ' || p_locator_id);
        debug_log('p_lot_number ' || p_lot_number);
        debug_log('p_project_id ' || p_project_id);
        debug_log('p_task_id ' || p_task_id);
    
          BEGIN
          
          --Added 5/12/2015 to prevent open cursors
            if x_oh%isOPEN then
               debug_log('closing x_oh cursor');
                close x_oh;
            
            end if;

            OPEN x_oh FOR
                      SELECT moq.subinventory_code,
                             moq.locator_id,
                             wil.concatenated_segments loc,
                             moq.lot_number,
                             (xxwc_mwa_routines_pkg.get_onhand(p_inventory_item_id => msib.inventory_item_id
                                                                        , p_organization_id   => msib.organization_id
                                                                        , p_subinventory      => moq.subinventory_code
                                                                        , p_locator_id        => moq.locator_id
                                                                        , p_return_type       => 'RQOH'
                                                                        , p_lot_number        => NULL
                                                                        )  - 
                             xxwc_mwa_routines_pkg.GET_OPEN_SALES_ORDERS( p_organization_id => msib.organization_id
                                                                        , p_inventory_item_id => msib.inventory_item_id
                                                                        , p_uom => msib.primary_unit_of_measure)) available,
                            (xxwc_mwa_routines_pkg.get_onhand(p_inventory_item_id => msib.inventory_item_id
                                                                        , p_organization_id   => msib.organization_id
                                                                        , p_subinventory      => moq.subinventory_code
                                                                        , p_locator_id        => moq.locator_id
                                                                        , p_return_type       => 'RQOH'
                                                                        , p_lot_number        => NULL
                                                                        )  - 
                             xxwc_mwa_routines_pkg.GET_OPEN_COUNTER_ORDERS( p_organization_id => msib.organization_id
                                                                        , p_inventory_item_id => msib.inventory_item_id
                                                                        , p_uom => msib.primary_unit_of_measure)) on_hand,
                             nvl(xxwc_inv_ais_pkg.get_onorder_qty (msib.organization_id, msib.inventory_item_id),0) on_order
                        FROM   mtl_system_items_kfv msib,
                               mtl_onhand_quantities_detail moq,
                               wms_item_locations_kfv wil
                        WHERE  msib.inventory_item_Id = moq.inventory_item_id
                        AND    msib.organization_Id = moq.organization_id
                        AND    moq.subinventory_code = wil.subinventory_code(+)
                        AND    moq.locator_id = wil.inventory_location_id(+)
                        AND    moq.subinventory_code = moq.subinventory_code
                        ---   Parameters
                        AND    msib.organization_id = p_organization_id
                        AND    msib.inventory_item_id = p_inventory_item_id
                        AND    moq.subinventory_code = nvl(p_subinventory_code, moq.subinventory_code)
                        AND    nvl(moq.locator_id,-1) = nvl(p_locator_id,nvl(moq.locator_id,-1))
                        AND    nvl(moq.lot_number,'-99999') = nvl(p_lot_number, nvl(moq.lot_number, '-99999'))
                        AND    nvl(moq.project_id,-1) = nvl(p_project_id, nvl(moq.project_id,-1))
                        AND    nvl(moq.task_id,-1) = nvl(p_task_id, nvl(moq.task_id,-1))
                        group by msib.inventory_item_id, 
                               msib.organization_id, 
                               msib.segment1, 
                               msib.description,
                               msib.primary_uom_code,
                               moq.subinventory_code,
                               moq.locator_id,
                               wil.concatenated_segments,
                               moq.lot_number,
                               msib.primary_unit_of_measure
                        ORDER BY
                                msib.organization_id,
                                msib.segment1,
                                decode(moq.subinventory_code,
                                       'General','-1',
                                       moq.subinventory_code, moq.subinventory_code);


          EXCEPTION
              WHEN others THEN
                  g_sqlcode := SQLCODE;
                  g_sqlerrm := SQLERRM;
                  raise g_exception;
          END;
          

    EXCEPTION

        WHEN g_exception THEN
            
            --Added 5/12/2015 to prevent open cursors
            if x_oh%isOPEN then
               debug_log('closing x_oh cursor');
                close x_oh;
            
            end if;


                xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                     , p_calling             => g_call_point
                                                     , p_ora_error_msg       => SQLERRM
                                                     , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                     , p_distribution_list   => g_distro_list
                                                     , p_module              => g_module);
          WHEN OTHERS THEN
            
            g_sqlcode := SQLCODE;
            g_sqlerrm := SQLERRM;
            g_message := 'When others ' ||g_sqlcode || g_sqlerrm;
            
            --Added 5/12/2015 to prevent open cursors
            if x_oh%isOPEN then
               debug_log('closing x_oh cursor');
                close x_oh;
            
            end if;

                xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                     , p_calling             => g_call_point
                                                     , p_ora_error_msg       => SQLERRM
                                                     , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                     , p_distribution_list   => g_distro_list
                                                     , p_module              => g_module);
      
    
    END GET_WC_OH;
    
    
     /*****************************************************************************************************************************************
     *  PROCEDURE GET_ITEM_ONLY_LOV                                                                                                             *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure used for items and upc code                                                                                *
     *                                                                                                                                        *
     *    Parameters: None                                                                                                                    *
     *    Return : XXWC_LABEL_PRINTER profile                                                                                                 *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
     *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
     *****************************************************************************************************************************************/


    PROCEDURE GET_ITEM_ONLY_LOV ( x_item             OUT    NOCOPY t_genref --0
                                , p_organization_id  IN     VARCHAR2
                                , p_item             IN     VARCHAR2
                                )
      IS
      
      l_organization_id NUMBER;
   
   BEGIN               

      g_call_from := 'GET_ITEM_ONLY_LOV';
      g_call_point := 'Start';
 
    
        IF g_debug = 'Y' THEN
              debug_log('p_item ' || p_item);
      
        END IF;
    
      g_call_point := 'Convert organization_id to number';
      
      BEGIN
        SELECT to_number(p_organization_id)
        INTO   l_organization_id
        FROM   dual;
      EXCEPTION
        WHEN others THEN
              g_sqlcode := SQLCODE;
              g_sqlerrm := SQLERRM;
              g_message := 'Error converting organization_id';
              debug_log(g_message);
              raise g_exception;
      END;
    
      --Added 5/12/2015 to prevent open cursors
            if x_item%isOPEN then
               debug_log('closing x_item cursor');
                close x_item;
            end if;


        OPEN x_item FOR
          SELECT  msi.inventory_item_id
                , msi.concatenated_segments
                , msi.primary_uom_code
                , msi.description
                , msi.lot_control_code
                , msi.serial_number_control_code
                , msi.shelf_life_days
                , 0
          FROM   mtl_system_items_kfv msi
          WHERE  msi.organization_id = l_organization_id
          --AND    msi.concatenated_segments like nvl(p_item , msi.concatenated_segments) --removed 4/20/2015
          AND    msi.concatenated_segments = upper(p_item) --added 4/20/2015
          ;
          
          
          

    EXCEPTION
        WHEN g_exception THEN
            
                  --Added 5/12/2015 to prevent open cursors
            if x_item%isOPEN then
               debug_log('closing x_item cursor');
                close x_item;
            end if;

                xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                     , p_calling             => g_call_point
                                                     , p_ora_error_msg       => SQLERRM
                                                     , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                     , p_distribution_list   => g_distro_list
                                                     , p_module              => g_module);
          WHEN OTHERS THEN
            
            g_sqlcode := SQLCODE;
            g_sqlerrm := SQLERRM;
            g_message := 'When others ' ||g_sqlcode || g_sqlerrm;
            
                  --Added 5/12/2015 to prevent open cursors
            if x_item%isOPEN then
               debug_log('closing x_item cursor');
                close x_item;
            end if;

                xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                     , p_calling             => g_call_point
                                                     , p_ora_error_msg       => SQLERRM
                                                     , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                     , p_distribution_list   => g_distro_list
                                                     , p_module              => g_module);
      
    
     
    END GET_ITEM_ONLY_LOV;          

     /*****************************************************************************************************************************************
     *  PROCEDURE GET_UPC_ONLY_LOV                                                                                                             *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure used for items and upc code                                                                                *
     *                                                                                                                                        *
     *    Parameters: None                                                                                                                    *
     *    Return : XXWC_LABEL_PRINTER profile                                                                                                 *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
     *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
     *****************************************************************************************************************************************/


    PROCEDURE GET_UPC_ONLY_LOV ( x_item             OUT    NOCOPY t_genref --0
                                , p_organization_id  IN     VARCHAR2
                                , p_item             IN     VARCHAR2
                                )
      IS
      
      l_organization_id NUMBER;
   
   BEGIN               

      g_call_from := 'GET_UPC_ONLY_LOV';
      g_call_point := 'Start';
 
    
        IF g_debug = 'Y' THEN
              debug_log('p_item ' || p_item);
      
        END IF;
    
      g_call_point := 'Convert organization_id to number';
      
      BEGIN
        SELECT to_number(p_organization_id)
        INTO   l_organization_id
        FROM   dual;
      EXCEPTION
        WHEN others THEN
              g_sqlcode := SQLCODE;
              g_sqlerrm := SQLERRM;
              g_message := 'Error converting organization_id';
              debug_log(g_message);
              raise g_exception;
      END;
    
        --Added 5/12/2015 to prevent open cursors
        if x_item%isOPEN then
           debug_log('closing x_item cursor');
            close x_item;
        
        end if;
    
        OPEN x_item FOR
          SELECT  msi.inventory_item_id
                , msi.concatenated_segments
                , msi.primary_uom_code
                , msi.description
                , msi.lot_control_code
                , msi.serial_number_control_code
                , msi.shelf_life_days
                , 0
          FROM   mtl_system_items_kfv msi
          WHERE  msi.organization_id = l_organization_id
          AND    EXISTS (SELECT mcr.inventory_item_id
                         FROM   mtl_cross_references mcr
                         WHERE  1=1--msi.organization_id = fnd_profile.VALUE('XXWC_ITEM_MASTER_ORG')
                         --AND mcr.cross_reference_type = fnd_profile.VALUE('INV:GTIN_CROSS_REFERENCE_TYPE') removed 5/12/2015
                         AND mcr.organization_id IS NULL                                        
                         AND mcr.inventory_item_id = msi.inventory_item_id
                         --AND mcr.cross_reference like nvl(p_item , mcr.cross_reference)) --removed 4/20/2015
                         AND mcr.cross_reference = upper(p_item)
                         AND  exists (select mcrt.cross_reference_type --added exists cross reference type 5/12/2015 to include multiple cross reference types
                                      from   mtl_cross_reference_types mcrt
                                      where  nvl(mcrt.attribute1,'N') = 'Y'
                                      AND    mcrt.cross_reference_type = mcr.cross_reference_type)
                          ) --added 4/20/2015
          ;
          
          
          

    EXCEPTION
        WHEN g_exception THEN
              
              --Added 5/12/2015 to close cursor
              if x_item%isOPEN then
                 debug_log('closing x_item cursor');
                  close x_item;
              end if;
            
                xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                     , p_calling             => g_call_point
                                                     , p_ora_error_msg       => SQLERRM
                                                     , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                     , p_distribution_list   => g_distro_list
                                                     , p_module              => g_module);
          WHEN OTHERS THEN
            
            g_sqlcode := SQLCODE;
            g_sqlerrm := SQLERRM;
            g_message := 'When others ' ||g_sqlcode || g_sqlerrm;
            
            
               --Added 5/12/2015 to close cursor
              if x_item%isOPEN then
                 debug_log('closing x_item cursor');
                  close x_item;
              end if;
              
              
                xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                     , p_calling             => g_call_point
                                                     , p_ora_error_msg       => SQLERRM
                                                     , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                     , p_distribution_list   => g_distro_list
                                                     , p_module              => g_module);
      
    
     
    END GET_UPC_ONLY_LOV;

     /*****************************************************************************************************************************************
     *  PROCEDURE PROCESS_SUB_XFER                                                                                                            *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure used to process sub xfer                                                                                   *
     *                                                                                                                                        *
     *    Parameters: None                                                                                                                    *
     *    Return : 0 = Sucess Otherwise errror                                                                                                *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        20-JUN-2015  Lee Spitzer               TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions                   *
     *****************************************************************************************************************************************/
    
    
  PROCEDURE PROCESS_SUB_XFER   ( p_organization_id       IN NUMBER       --1
                               , p_inventory_item_id     IN NUMBER       --2
                               , p_subinventory          IN VARCHAR2     --3
                               , p_locator_id            IN NUMBER       --4
                               , p_transfer_subinventory IN VARCHAR2     --5
                               , p_transfer_locator      IN NUMBER       --6
                               , p_lot_number            IN VARCHAR2     --7
                               , p_uom                   IN VARCHAR2     --8
                               , p_qty                   IN NUMBER       --9
                               , p_user_id               IN NUMBER       --10
                               , x_return                OUT NUMBER      --11
                               , x_message               OUT VARCHAR2)  --12
    IS


        l_transaction_type_id number;
        l_transaction_source_type_id number;
        lv_source_header_id number;
        lv_source_line_id number;
        lv_transaction_interface_id number;
        lv_transaction_header_id number;
        l_msg_cnt      number;
        l_msg_data     varchar2(2000);
        l_trans_count  number;
        p_retval      number;
        l_return_status  number;
        l_return_status2 varchar2(1);
        l_exception exception;
        l_acct_period_id number;
        l_cost_group_id number;
        l_txfer_cost_group_id number;
        l_reason_id NUMBER;
    
    BEGIN
    
      g_call_from := 'PROCESS_SUB_XFER';
      g_call_point := 'Start';
      
      
      debug_log('p_organization_id ' || p_organization_id);
      debug_log('p_inventory_item_id ' || p_inventory_item_id);
      debug_log('p_subinventory ' || p_subinventory);
      debug_log('p_locator_id ' || p_locator_id);
      debug_log('p_transfer_subinventory ' || p_transfer_subinventory);
      debug_log('p_transfer_locator ' || p_transfer_locator);
      debug_log('p_lot_number ' || p_lot_number);
      debug_log('p_uom ' || p_uom);
      debug_log('p_qty ' || p_qty);
      debug_log('p_user_id ' || p_user_id);


      g_call_point := 'getting l_transacion_type_id';
      
      begin
      
          select transaction_type_id
          into   l_transaction_type_id
          from   mtl_transaction_types
          where  transaction_type_name = 'Subinventory Transfer';
      exception
          when others then
               g_sqlcode := SQLCODE;
               g_sqlerrm := SQLERRM;
               g_message := 'Error getting l_transaction_type_id';
      end;
      
      debug_log('l_transaction_type_id ' || l_transaction_type_id);
      
      g_call_point := 'getting l_transaction_source_type_id';
      
      begin
          select transaction_source_type_id
          into  l_transaction_source_type_id
          from MTL_TXN_SOURCE_TYPES
          where  transaction_source_type_name = 'Inventory';
      exception
          when others then
               g_sqlcode := SQLCODE;
               g_sqlerrm := SQLERRM;
               g_message := 'Error getting l_transaction_source_type_id';
      end;
      
      debug_log('l_transaction_source_type_id ' || l_transaction_source_type_id);
      
      
      g_call_point := 'getting lv_source_header_id';
      
      begin
    
          SELECT mtl_material_transactions_s.NEXTVAL
          INTO lv_source_header_id
          FROM DUAL;

      exception
          when others then
               g_sqlcode := SQLCODE;
               g_sqlerrm := SQLERRM;
               g_message := 'Error getting lv_source_header_id';
      end;
      
      
      debug_log('lv_source_header_id ' || lv_source_header_id);
      
      lv_source_line_id := lv_source_header_id;
      lv_transaction_interface_id := lv_source_header_id;
      lv_transaction_header_id := lv_source_header_id;
      
      debug_log('lv_source_line_id ' || lv_source_line_id);
      debug_log('lv_transaction_interface_id ' || lv_transaction_interface_id);
      debug_log('lv_transaction_header_id ' || lv_transaction_header_id);
      

      g_call_point := 'getting l_acct_period_id';
      
      begin
        select acct_period_id
        into   l_acct_period_id
        from  org_acct_periods
        where  sysdate between period_start_date and schedule_close_date
        and    organization_id = p_organization_id;
            
      exception
        when others then
          g_sqlcode := SQLCODE;
          g_sqlerrm := SQLERRM;
          g_message := 'Error getting l_acct_period_id';
                 
      end;

      debug_log('l_acct_period_id ' || l_acct_period_id);
      
      g_call_point := 'getting l_cost_group_id';
      
      begin
        select default_cost_group_id
        into   l_cost_group_id
        from   mtl_secondary_inventories
        where  organization_id = p_organization_id
        and    secondary_inventory_name = p_subinventory;
                
      exception
        when others then
          g_sqlcode := SQLCODE;
          g_sqlerrm := SQLERRM;
          g_message := 'Error getting l_cost_group_id';
      end;
      
      
      debug_log('l_cost_group_id ' || l_cost_group_id);
      
      g_call_point := 'getting l_txfer_cost_group_id';
      
      begin
        select default_cost_group_id
        into   l_txfer_cost_group_id
        from   mtl_secondary_inventories
        where  organization_id = p_organization_id
        and    secondary_inventory_name = p_transfer_subinventory;
      exception
         when others then
          g_sqlcode := SQLCODE;
          g_sqlerrm := SQLERRM;
          g_message := 'Error getting l_txfer_cost_group_id';
      end;
      
      debug_log('l_txfer_cost_group_id ' || l_txfer_cost_group_id);
      
      g_call_point := 'Clear Quantity Cache';
      
      BEGIN
      inv_quantity_tree_pub.clear_quantity_cache;
        --FND_GLOBAL.APPS_INITIALIZE(USER_ID=>p_user_id,RESP_ID=>50649,RESP_APPL_ID=>385);
      exception
         when others then
          g_sqlcode := SQLCODE;
          g_sqlerrm := SQLERRM;
          g_message := 'Error clear quantity cache';
      end;
      
      
      g_call_point := 'Getting reason id';
      
      BEGIN
          
         SELECT reason_id
         into   l_reason_id
         FROM   MTL_TRANSACTION_REASONS
         where  reason_name = '15 - RF';
    
      EXCEPTION
              WHEN others THEN
                l_reason_id := NULL;
      END;
          

      g_call_point := 'Insert into mtl_material_transactions_temp';
      
          BEGIN
          
            insert into mtl_material_transactions_temp
                                 (TRANSACTION_TEMP_ID,
                                  TRANSACTION_HEADER_ID,
                                  LAST_UPDATE_DATE,
                                  LAST_UPDATED_BY,
                                  CREATION_DATE,
                                  CREATED_BY,
                                  INVENTORY_ITEM_ID,
                                  ORGANIZATION_ID,
                                  SUBINVENTORY_CODE,
                                  LOCATOR_ID,
                                  TRANSACTION_QUANTITY,
                                  PRIMARY_QUANTITY,
                                  TRANSACTION_UOM,
                                  TRANSACTION_TYPE_ID,
                                  TRANSACTION_ACTION_ID,
                                  TRANSACTION_SOURCE_TYPE_ID,
                                  TRANSACTION_DATE,
                                  ACCT_PERIOD_ID,
                                  TRANSFER_SUBINVENTORY,
                                  TRANSFER_TO_LOCATION,
                                  POSTING_FLAG,
                                  PROCESS_FLAG,
                                  COST_GROUP_ID,
                                  TRANSFER_COST_GROUP_ID,
                                  REASON_ID)
              values             (lv_transaction_interface_id, --TRANSACTION_INTERFACE_ID,
                                  lv_transaction_interface_id,
                                  sysdate, --last_update_date,
                                  p_user_id, --last_updated_by,
                                  sysdate, --creation_date,
                                  p_user_id, --created_by                             
                                  p_inventory_item_id, --INVENTORY_ITEM_ID
                                  p_organization_id, --ORGANIZATION_ID,
                                  p_subinventory, --SUBINVENTORY_CODE,
                                  p_locator_id,--LOCATOR_ID,
                                  p_qty, --primary_QTY
                                  p_qty, --transaction_qty
                                  p_uom,--TRANSACTION_UOM
                                  l_transaction_type_id, --TRANSACTION_TYPE_ID,
                                  l_transaction_type_id, --TRANSACTION_ACTION_ID,
                                  l_transaction_source_type_id, --TRANSACTION_SOURCE_TYPE_ID,
                                  sysdate, --transaction_date
                                  l_acct_period_id, --acct_period_id
                                  p_transfer_subinventory, --TRANSFER_SUBINVENTORY,
                                  p_transfer_locator, --TRANSFER_LOCATOR,
                                  'Y', --Posting_FLAG
                                  'Y', --PROCESS_FLAG
                                  l_cost_group_id, --COST_GROUP_ID,
                                  l_txfer_cost_group_id, --TRANSFER_COST_GROUP_ID
                                  l_reason_id);--Reason Id

        exception
           when others then
            g_sqlcode := SQLCODE;
            g_sqlerrm := SQLERRM;
            g_message := 'Error insert into mtl_material_transactions_temp';
        end;                                  
                                
          
          if p_lot_number is not null or p_lot_number != '' then
              
              g_call_point := 'Insert mtl_transaction_lots_temp';
        
              
              BEGIN  
                  INSERT INTO mtl_transaction_lots_temp
                                    (transaction_temp_id,
                                     last_update_date,
                                     last_updated_by,
                                     creation_date,
                                     created_by,
                                     transaction_quantity,
                                     primary_quantity,
                                     lot_number,
                                     lot_expiration_date,
                                     secondary_quantity
                                    )
                             VALUES (lv_transaction_interface_id,
                                     SYSDATE,
                                     p_user_id,
                                     SYSDATE,
                                     p_user_id,
                                     p_qty,
                                     p_qty,
                                     p_lot_number,
                                     NULL,
                                     0);
                               
         exception
           when others then
            g_sqlcode := SQLCODE;
            g_sqlerrm := SQLERRM;
            g_message := 'Error insert into mtl_transaction_lots_temp';
        end;  
        
        END IF;
        
        commit;
        
        g_call_point := 'clear quantity cache 2';
            

        BEGIN
          inv_quantity_tree_pub.clear_quantity_cache;
        EXCEPTION
        when others then
            g_sqlcode := SQLCODE;
            g_sqlerrm := SQLERRM;
            g_message := 'Error clear_quantity_cache 2';
        END;
              
        
        g_call_point := 'call inv_txn_manager_pub.process_transactions ';
        
        
        BEGIN
                p_retval :=
                      inv_txn_manager_pub.process_transactions
                            (p_api_version           => 1,
                             p_init_msg_list         => fnd_api.g_false,
                             p_commit                => fnd_api.g_false,
                             p_validation_level      => fnd_api.g_valid_level_full,
                             x_return_status         => l_return_status2,
                             x_msg_count             => l_msg_cnt,
                             x_msg_data              => l_msg_data,
                             x_trans_count           => l_trans_count,
                             p_table                 => 2,
                             p_header_id             => lv_transaction_interface_id
                            );
    
        EXCEPTION
          when others then
              g_sqlcode := SQLCODE;
              g_sqlerrm := SQLERRM;
              g_message := 'Error inv_txn_manager_pub.process_transactions';
          END;
          
        
        commit;
            
        
       g_call_point := 'clear quantity cache 3 ';
            

        BEGIN
          inv_quantity_tree_pub.clear_quantity_cache;
        EXCEPTION
        when others then
            g_sqlcode := SQLCODE;
            g_sqlerrm := SQLERRM;
            g_message := 'Error clear_quantity_cache 3';
        END;
        
        
          debug_log('p_retval ' || p_retval);
          debug_log('l_return_status2 ' || l_return_status2);
          debug_log('l_msg_data ' || l_msg_data);
            
           if p_retval = 0 then
               g_message := 'Success';
           else
               g_message := 'Error' || l_msg_data;
           end if;
      
        
        
        x_return := p_retval;
        x_message :=  g_message;  
        
        debug_log('x_return ' || x_return);
        debug_log('x_message ' || x_message);
          
       
       EXCEPTION
        
        WHEN g_exception THEN
              
        x_return := 1;
        x_message :=  g_message;  
       
        debug_log('x_return ' || x_return);
        debug_log('x_message ' || x_message);
        
                xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                     , p_calling             => g_call_point
                                                     , p_ora_error_msg       => SQLERRM
                                                     , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                     , p_distribution_list   => g_distro_list
                                                     , p_module              => g_module);
          WHEN OTHERS THEN
            
            g_sqlcode := SQLCODE;
            g_sqlerrm := SQLERRM;
            g_message := 'When others ' ||g_sqlcode || g_sqlerrm;
            
            x_return := 1;
            x_message :=  g_message;  
        
            debug_log('x_return ' || x_return);
            debug_log('x_message ' || x_message);
              
                xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                     , p_calling             => g_call_point
                                                     , p_ora_error_msg       => SQLERRM
                                                     , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                     , p_distribution_list   => g_distro_list
                                                     , p_module              => g_module);
      
    
    END PROCESS_SUB_XFER;
    
     /*****************************************************************************************************************************************
     *  PROCEDURE GET_ACCOUNT_ALIAS_LOV                                                                                                       *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure used for account alias                                                                                     *
     *                                                                                                                                        *
     *    Parameters: None                                                                                                                    *
     *    Return : Cursor for Account Alias LOV                                                                                               *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        20-JUN-2015  Lee Spitzer               TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions                   *
     *****************************************************************************************************************************************/


    PROCEDURE GET_ACCOUNT_ALIAS_LOV ( x_alias            OUT    NOCOPY t_genref --0
                                    , p_organization_id  IN     VARCHAR2
                                    , p_alias            IN     VARCHAR2
                                    )
    IS
    
      l_organization_id NUMBER;
    
    BEGIN
                  

      g_call_from := 'GET_ACCOUNT_ALIAS_LOV';
      g_call_point := 'Start';
 
    
        IF g_debug = 'Y' THEN
              debug_log('p_organization_id ' || p_organization_id);
              debug_log('p_alias ' || p_alias);
      
        END IF;
    
      g_call_point := 'Convert organization_id to number';
      
      BEGIN
        SELECT to_number(p_organization_id)
        INTO   l_organization_id
        FROM   dual;
      EXCEPTION
        WHEN others THEN
              g_sqlcode := SQLCODE;
              g_sqlerrm := SQLERRM;
              g_message := 'Error converting organization_id';
              debug_log(g_message);
              raise g_exception;
      END;
    
        if x_alias%isOPEN then
           debug_log('closing x_alias cursor');
            close x_alias;
        
        end if;
    
        OPEN x_alias FOR
          SELECT disposition_id, concatenated_segments, description
          FROM   mtl_generic_dispositions_kfv
          WHERE  organization_id = l_organization_id
          AND    sysdate between nvl(effective_date,sysdate-1) and nvl(disable_date,sysdate+1)
          AND    upper(concatenated_segments) like upper(nvl(p_alias, concatenated_segments))
          order by concatenated_segments
          ;

    EXCEPTION
        WHEN g_exception THEN
              
              if x_alias%isOPEN then
                 debug_log('closing x_alias cursor');
                  close x_alias;
              end if;
            
                xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                     , p_calling             => g_call_point
                                                     , p_ora_error_msg       => SQLERRM
                                                     , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                     , p_distribution_list   => g_distro_list
                                                     , p_module              => g_module);
          WHEN OTHERS THEN
            
            g_sqlcode := SQLCODE;
            g_sqlerrm := SQLERRM;
            g_message := 'When others ' ||g_sqlcode || g_sqlerrm;
            
            
              if x_alias%isOPEN then
                 debug_log('closing x_alias cursor');
                  close x_alias;
              end if;
              
              
                xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                     , p_calling             => g_call_point
                                                     , p_ora_error_msg       => SQLERRM
                                                     , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                     , p_distribution_list   => g_distro_list
                                                     , p_module              => g_module);
    END GET_ACCOUNT_ALIAS_LOV;
    

     /*****************************************************************************************************************************************
     *  PROCEDURE GET_TRANSACTION_TYPE_LOV                                                                                                    *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure used for tranasction type                                                                                  *
     *                                                                                                                                        *
     *    Parameters: None                                                                                                                    *
     *    Return : Cursor for Transaction Type LOV                                                                                            *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        20-JUN-2015  Lee Spitzer               TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions                   *
     *****************************************************************************************************************************************/


    PROCEDURE GET_TRANSACTION_TYPE_LOV ( x_transaction            OUT    NOCOPY t_genref --0
                                        , p_transaction_type      IN     VARCHAR2
                                       )                                

    IS
    
    
    BEGIN
                  

      g_call_from := 'GET_TRANSACTION_TYPE_LOV';
      g_call_point := 'Start';
 
    
        IF g_debug = 'Y' THEN
              debug_log('p_transaction_type ' || p_transaction_type);
        END IF;
    
      g_call_point := 'Convert organization_id to number';
    
        if x_transaction%isOPEN then
           debug_log('closing x_transaction cursor');
            close x_transaction;
        
        end if;
    
        OPEN x_transaction FOR
          select mtt.transaction_type_id, 
                 mtt.transaction_type_name, 
                 mtt.description, 
                 mtt.transaction_action_id, 
                 mtt.transaction_source_type_id, 
                 mtt.user_defined_flag, 
                 mtt.status_control_flag, 
                 mtt.location_required_flag
          from   mtl_transaction_types mtt
          where  nvl(mtt.disable_date,sysdate+1) > sysdate
          AND    upper(mtt.transaction_type_name) like upper(nvl(p_transaction_type, mtt.transaction_type_name))
          AND    mtt.user_defined_flag = 'N' --Seeded Transaction Types
          AND    exists (select mtst.transaction_source_type_id
                         from   MTL_TXN_SOURCE_TYPES mtst
                         where  mtst.transaction_source_type_name = 'Account alias'
                         and    mtst.transaction_source_type_id = mtt.transaction_source_type_id)
          order by mtt.transaction_type_name
          ;

    EXCEPTION
        WHEN g_exception THEN
              
              if x_transaction%isOPEN then
                 debug_log('closing x_transaction cursor');
                  close x_transaction;
              end if;
            
                xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                     , p_calling             => g_call_point
                                                     , p_ora_error_msg       => SQLERRM
                                                     , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                     , p_distribution_list   => g_distro_list
                                                     , p_module              => g_module);
          WHEN OTHERS THEN
            
            g_sqlcode := SQLCODE;
            g_sqlerrm := SQLERRM;
            g_message := 'When others ' ||g_sqlcode || g_sqlerrm;
            
            
              if x_transaction%isOPEN then
                 debug_log('closing x_transaction cursor');
                  close x_transaction;
              end if;
              
              
                xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                     , p_calling             => g_call_point
                                                     , p_ora_error_msg       => SQLERRM
                                                     , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                     , p_distribution_list   => g_distro_list
                                                     , p_module              => g_module);
    END GET_TRANSACTION_TYPE_LOV;

     /*****************************************************************************************************************************************
     *  PROCEDURE PROCESS_ALIAS_TXN                                                                                                           *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure used to process account alias                                                                              *
     *                                                                                                                                        *
     *    Parameters: None                                                                                                                    *
     *    Return : 0 = Sucess Otherwise errror                                                                                                *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        20-JUN-2015  Lee Spitzer               TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions                   *
     *****************************************************************************************************************************************/


    PROCEDURE PROCESS_ALIAS_TXN  ( p_organization_id       IN NUMBER       --1
                                 , p_inventory_item_id     IN NUMBER       --2
                                 , p_subinventory          IN VARCHAR2     --3
                                 , p_locator_id            IN NUMBER       --4
                                 , p_lot_number            IN VARCHAR2     --5
                                 , p_uom                   IN VARCHAR2     --6
                                 , p_qty                   IN NUMBER       --7
                                 , p_disposition_id        IN NUMBER       --8
                                 , p_transaction_type_id   IN NUMBER       --9
                                 , p_transaction_action_id IN NUMBER       --10
                                 , p_transaction_source_type_id IN NUMBER  --11
                                 , p_user_id               IN NUMBER       --12
                                 , x_return                OUT NUMBER      --13
                                 , x_message               OUT VARCHAR2)   --14                                  
    IS
    
        l_transaction_type_id number;
        l_transaction_source_type_id number;
        lv_source_header_id number;
        lv_source_line_id number;
        lv_transaction_interface_id number;
        lv_transaction_header_id number;
        l_msg_cnt      number;
        l_msg_data     varchar2(2000);
        l_trans_count  number;
        p_retval      number;
        l_return_status  number;
        l_return_status2 varchar2(1);
        l_exception exception;
        l_acct_period_id number;
        l_cost_group_id number;
        l_txfer_cost_group_id number;
        l_reason_id NUMBER;
    
    BEGIN
    
      g_call_from := 'PROCESS_ALIAS_TXN';
      g_call_point := 'Start';
      
      
      debug_log('p_organization_id ' || p_organization_id);
      debug_log('p_inventory_item_id ' || p_inventory_item_id);
      debug_log('p_subinventory ' || p_subinventory);
      debug_log('p_locator_id ' || p_locator_id);
      debug_log('p_lot_number ' || p_lot_number);
      debug_log('p_uom ' || p_uom);
      debug_log('p_qty ' || p_qty);
      debug_log('p_disposition_id ' || p_disposition_id);
      debug_log('p_transaction_type_id ' || p_transaction_type_id);
      debug_log('p_transaction_action_id ' || p_transaction_action_id);
      debug_log('p_transaction_source_type_id ' || p_transaction_source_type_id);
      debug_log('p_user_id ' || p_user_id);

      g_call_point := 'getting lv_source_header_id';
      
      begin
    
          SELECT mtl_material_transactions_s.NEXTVAL
          INTO lv_source_header_id
          FROM DUAL;

      exception
          when others then
               g_sqlcode := SQLCODE;
               g_sqlerrm := SQLERRM;
               g_message := 'Error getting lv_source_header_id';
      end;
      
      
      debug_log('lv_source_header_id ' || lv_source_header_id);
      
      lv_source_line_id := lv_source_header_id;
      lv_transaction_interface_id := lv_source_header_id;
      lv_transaction_header_id := lv_source_header_id;
      
      debug_log('lv_source_line_id ' || lv_source_line_id);
      debug_log('lv_transaction_interface_id ' || lv_transaction_interface_id);
      debug_log('lv_transaction_header_id ' || lv_transaction_header_id);
      

      g_call_point := 'getting l_acct_period_id';
      
      begin
        select acct_period_id
        into   l_acct_period_id
        from  org_acct_periods
        where  sysdate between period_start_date and schedule_close_date
        and    organization_id = p_organization_id;
            
      exception
        when others then
          g_sqlcode := SQLCODE;
          g_sqlerrm := SQLERRM;
          g_message := 'Error getting l_acct_period_id';
                 
      end;

      debug_log('l_acct_period_id ' || l_acct_period_id);
      
      g_call_point := 'getting l_cost_group_id';
      
      begin
        select default_cost_group_id
        into   l_cost_group_id
        from   mtl_secondary_inventories
        where  organization_id = p_organization_id
        and    secondary_inventory_name = p_subinventory;
                
      exception
        when others then
          g_sqlcode := SQLCODE;
          g_sqlerrm := SQLERRM;
          g_message := 'Error getting l_cost_group_id';
      end;
      
      
      debug_log('l_cost_group_id ' || l_cost_group_id);
      
      g_call_point := 'Clear Quantity Cache';
      
      BEGIN
      inv_quantity_tree_pub.clear_quantity_cache;
        --FND_GLOBAL.APPS_INITIALIZE(USER_ID=>p_user_id,RESP_ID=>50649,RESP_APPL_ID=>385);
      exception
         when others then
          g_sqlcode := SQLCODE;
          g_sqlerrm := SQLERRM;
          g_message := 'Error clear quantity cache';
      end;
      
      
      g_call_point := 'Getting reason id';
      
      BEGIN
          
         SELECT reason_id
         into   l_reason_id
         FROM   MTL_TRANSACTION_REASONS
         where  reason_name = '15 - RF';
    
      EXCEPTION
              WHEN others THEN
                l_reason_id := NULL;
      END;
          

      g_call_point := 'Insert into mtl_material_transactions_temp';
      
          BEGIN
          
            insert into mtl_material_transactions_temp
                                 (TRANSACTION_TEMP_ID,
                                  TRANSACTION_HEADER_ID,
                                  LAST_UPDATE_DATE,
                                  LAST_UPDATED_BY,
                                  CREATION_DATE,
                                  CREATED_BY,
                                  INVENTORY_ITEM_ID,
                                  ORGANIZATION_ID,
                                  SUBINVENTORY_CODE,
                                  LOCATOR_ID,
                                  TRANSACTION_QUANTITY,
                                  PRIMARY_QUANTITY,
                                  TRANSACTION_UOM,
                                  TRANSACTION_TYPE_ID,
                                  TRANSACTION_ACTION_ID,
                                  TRANSACTION_SOURCE_TYPE_ID,
                                  TRANSACTION_DATE,
                                  ACCT_PERIOD_ID,
                                  --DISTRIBUTION_ACCOUNT_ID,
                                  TRANSACTION_SOURCE_ID,
                                  --TRANSFER_SUBINVENTORY,
                                  --TRANSFER_TO_LOCATION,
                                  POSTING_FLAG,
                                  PROCESS_FLAG,
                                  COST_GROUP_ID,
                                  --TRANSFER_COST_GROUP_ID,
                                  REASON_ID)
              values             (lv_transaction_interface_id, --TRANSACTION_INTERFACE_ID,
                                  lv_transaction_interface_id,
                                  sysdate, --last_update_date,
                                  p_user_id, --last_updated_by,
                                  sysdate, --creation_date,
                                  p_user_id, --created_by                             
                                  p_inventory_item_id, --INVENTORY_ITEM_ID
                                  p_organization_id, --ORGANIZATION_ID,
                                  p_subinventory, --SUBINVENTORY_CODE,
                                  p_locator_id,--LOCATOR_ID,
                                  p_qty, --primary_QTY
                                  p_qty, --transaction_qty
                                  p_uom,--TRANSACTION_UOM
                                  p_transaction_type_id, --TRANSACTION_TYPE_ID,
                                  p_transaction_action_id, --TRANSACTION_ACTION_ID,
                                  p_transaction_source_type_id, --TRANSACTION_SOURCE_TYPE_ID,
                                  sysdate, --transaction_date
                                  l_acct_period_id, --acct_period_id
                                  --p_disposition_id, --DISTRIBUTION_ACCOUNT_ID
                                  p_disposition_id, --TRANSACTION_SOURCE_ID
                                  --p_transfer_subinventory, --TRANSFER_SUBINVENTORY,
                                  --p_transfer_locator, --TRANSFER_LOCATOR,
                                  'Y', --Posting_FLAG
                                  'Y', --PROCESS_FLAG
                                  l_cost_group_id, --COST_GROUP_ID,
                                  --l_txfer_cost_group_id, --TRANSFER_COST_GROUP_ID
                                  l_reason_id);--Reason Id

        exception
           when others then
            g_sqlcode := SQLCODE;
            g_sqlerrm := SQLERRM;
            g_message := 'Error insert into mtl_material_transactions_temp';
        end;                                  
                                
          
          if p_lot_number is not null or p_lot_number != '' then
              
              g_call_point := 'Insert mtl_transaction_lots_temp';
        
              
              BEGIN  
                  INSERT INTO mtl_transaction_lots_temp
                                    (transaction_temp_id,
                                     last_update_date,
                                     last_updated_by,
                                     creation_date,
                                     created_by,
                                     transaction_quantity,
                                     primary_quantity,
                                     lot_number,
                                     lot_expiration_date,
                                     secondary_quantity
                                    )
                             VALUES (lv_transaction_interface_id,
                                     SYSDATE,
                                     p_user_id,
                                     SYSDATE,
                                     p_user_id,
                                     p_qty,
                                     p_qty,
                                     p_lot_number,
                                     NULL,
                                     0);
                               
         exception
           when others then
            g_sqlcode := SQLCODE;
            g_sqlerrm := SQLERRM;
            g_message := 'Error insert into mtl_transaction_lots_temp';
        end;  
        
        END IF;
        
        commit;
        
        g_call_point := 'clear quantity cache 2';
            

        BEGIN
          inv_quantity_tree_pub.clear_quantity_cache;
        EXCEPTION
        when others then
            g_sqlcode := SQLCODE;
            g_sqlerrm := SQLERRM;
            g_message := 'Error clear_quantity_cache 2';
        END;
              
        
        g_call_point := 'call inv_txn_manager_pub.process_transactions ';
        
        
        BEGIN
                p_retval :=
                      inv_txn_manager_pub.process_transactions
                            (p_api_version           => 1,
                             p_init_msg_list         => fnd_api.g_false,
                             p_commit                => fnd_api.g_false,
                             p_validation_level      => fnd_api.g_valid_level_full,
                             x_return_status         => l_return_status2,
                             x_msg_count             => l_msg_cnt,
                             x_msg_data              => l_msg_data,
                             x_trans_count           => l_trans_count,
                             p_table                 => 2,
                             p_header_id             => lv_transaction_interface_id
                            );
    
        EXCEPTION
          when others then
              g_sqlcode := SQLCODE;
              g_sqlerrm := SQLERRM;
              g_message := 'Error inv_txn_manager_pub.process_transactions';
          END;
          
        
        commit;
            
        
       g_call_point := 'clear quantity cache 3 ';
            

        BEGIN
          inv_quantity_tree_pub.clear_quantity_cache;
        EXCEPTION
        when others then
            g_sqlcode := SQLCODE;
            g_sqlerrm := SQLERRM;
            g_message := 'Error clear_quantity_cache 3';
        END;
        
        
          debug_log('p_retval ' || p_retval);
          debug_log('l_return_status2 ' || l_return_status2);
          debug_log('l_msg_data ' || l_msg_data);
            
           if p_retval = 0 then
               g_message := 'Success';
           else
               g_message := 'Error' || l_msg_data;
           end if;
      
        
        
        x_return := p_retval;
        x_message :=  g_message;  
        
        debug_log('x_return ' || x_return);
        debug_log('x_message ' || x_message);
          
       
       EXCEPTION
        
        WHEN g_exception THEN
              
        x_return := 1;
        x_message :=  g_message;  
       
        debug_log('x_return ' || x_return);
        debug_log('x_message ' || x_message);
        
                xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                     , p_calling             => g_call_point
                                                     , p_ora_error_msg       => SQLERRM
                                                     , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                     , p_distribution_list   => g_distro_list
                                                     , p_module              => g_module);
          WHEN OTHERS THEN
            
            g_sqlcode := SQLCODE;
            g_sqlerrm := SQLERRM;
            g_message := 'When others ' ||g_sqlcode || g_sqlerrm;
            
            x_return := 1;
            x_message :=  g_message;  
        
            debug_log('x_return ' || x_return);
            debug_log('x_message ' || x_message);
              
                xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                     , p_calling             => g_call_point
                                                     , p_ora_error_msg       => SQLERRM
                                                     , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                     , p_distribution_list   => g_distro_list
                                                     , p_module              => g_module);
    
    END PROCESS_ALIAS_TXN;
    
    
     /*****************************************************************************************************************************************
     *  PROCEDURE GET_VENDOR_LOV                                                                                                              *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure used for vendors                                                                                           *
     *                                                                                                                                        *
     *    Parameters: None                                                                                                                    *
     *    Return : Cursor for Vendor LOV                                                                                                      *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        20-JUN-2015  Lee Spitzer               TMS Ticket Number 20150622-00004 RF - UPC Update                                   *
     *****************************************************************************************************************************************/


    PROCEDURE GET_VENDOR_LOV ( x_vendor            OUT    NOCOPY t_genref --0
                              , p_vendor_name      IN     VARCHAR2
                              ) 
    IS
    
    BEGIN
      
      g_call_from := 'GET_VENDOR_LOV';
      g_call_point := 'Start';
 
    
        IF g_debug = 'Y' THEN
              debug_log('p_vendor_name ' || p_vendor_name);
        END IF;
    
      
        if x_vendor%isOPEN then
           debug_log('closing x_vendor cursor');
            close x_vendor;
        
        end if;
    
        
        OPEN x_vendor FOR
          select  ass.vendor_id, 
                  ass.vendor_name, 
                  ass.segment1 vendor_number
          from   ap_suppliers ass
          where  sysdate between nvl(ass.start_date_active, sysdate-1) and nvl(ass.end_date_active, sysdate+1)
          and    ass.vendor_name like nvl(upper(p_vendor_name)||'%', ass.vendor_name) --added upper case per feedback from initial testing on 21-JUN-2015
          and    exists (select 1
                         from   ap_supplier_sites assa
                         where  ass.vendor_id = assa.vendor_id
                         and    sysdate  < nvl(assa.inactive_date,sysdate+1)
                         and    assa.purchasing_site_flag = 'Y')
          order by ass.vendor_name;
            

    EXCEPTION
        WHEN g_exception THEN
              
              if x_vendor%isOPEN then
                 debug_log('closing x_vendor cursor');
                  close x_vendor;
              end if;
            
                xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                     , p_calling             => g_call_point
                                                     , p_ora_error_msg       => SQLERRM
                                                     , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                     , p_distribution_list   => g_distro_list
                                                     , p_module              => g_module);
          WHEN OTHERS THEN
            
            g_sqlcode := SQLCODE;
            g_sqlerrm := SQLERRM;
            g_message := 'When others ' ||g_sqlcode || g_sqlerrm;
            
            
              if x_vendor%isOPEN then
                 debug_log('closing x_vendor cursor');
                  close x_vendor;
              end if;
              
              
                xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                     , p_calling             => g_call_point
                                                     , p_ora_error_msg       => SQLERRM
                                                     , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                     , p_distribution_list   => g_distro_list
                                                     , p_module              => g_module);

    
    END GET_VENDOR_LOV;
                             


     /*****************************************************************************************************************************************
     *  PROCEDURE PROCESS_XREF                                                                                                                *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure used for process cross reference                                                                           *
     *                                                                                                                                        *
     *    Parameters: None                                                                                                                    *
     *    Return : 0 = Sucess Otherwise errror                                                                                                *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        20-JUN-2015  Lee Spitzer               TMS Ticket Number 20150622-00004 RF - UPC Update                                   *
     *****************************************************************************************************************************************/
                             

    PROCEDURE PROCESS_XREF( p_organization_id       IN VARCHAR2 --1
                          , p_inventory_item_id     IN VARCHAR2 --2
                          , p_cross_reference       IN VARCHAR2 --3
                          , p_description           IN VARCHAR2 --4
                          , p_user_id               IN VARCHAR2 --5
                          , x_return                OUT NUMBER --6
                          , x_message               OUT VARCHAR2) --7
    
    IS
                          
      l_cross_reference_type VARCHAR2(25) := 'PENDING';
      l_count NUMBER;
      l_xref_tbl                           MTL_CROSS_REFERENCES_PUB.XRef_Tbl_Type;
      l_xref_id                            NUMBER;
      l_indx                               NUMBER := 0;
      l_cnt                                NUMBER := 0;

      l_return_status                      VARCHAR2(1);
      l_msg_count                          NUMBER;
      l_message_list                       Error_Handler.Error_Tbl_Type;

    BEGIN
    
    
      g_call_from := 'PROCESS_XREF';
      g_call_point := 'Start';
      
      debug_log('p_organization_id ' || p_organization_id);
      debug_log('p_inventory_item_id ' || p_inventory_item_id);
      debug_log('p_cross_reference ' || p_cross_reference);
      debug_log('p_description ' || p_description);
      debug_log('p_user_id ' || p_user_id);
      
      debug_log('l_cross_reference_type ' || l_cross_reference_type);
      
      g_call_point := 'Find out if cross reference exists';
      
      Begin
        Select nvl(count(cross_reference_id),0)
        INTO   l_count
        From MTL_CROSS_REFERENCES_B
        Where cross_reference_type = l_cross_reference_type
        And inventory_item_id = p_inventory_item_id
        And Decode(organization_id, NULL, org_independent_flag, organization_id) = Decode(p_organization_id, NULL, 'Y', p_organization_id)
        And cross_reference = p_cross_reference;
      EXCEPTION
        when others then
            l_count := 0;
            g_sqlcode := SQLCODE;
            g_sqlerrm := SQLERRM;
            g_message := 'Error getting existing cross reference type ' ;
            raise g_exception;
      END;
      
      debug_log('l_count ' || l_count);
      
      g_call_point := 'Before old cross-reference check';
      
      IF l_count > 0 THEN 
        
          g_message := 'Cross reference ' || p_cross_reference || ' already exists for this item.';
          raise g_exception;
        
      ELSE 
        
        l_xref_tbl(l_indx).transaction_type := 'CREATE';   -- CREATE, UPDATE, DELETE
        l_xref_tbl(l_indx).cross_reference_id := NULL;
     
        -- UPC codes can be generic for item across orgs
        g_call_point := 'Before Org independent flag logic';
        
        If p_organization_id IS NULL Then
          l_xref_tbl(l_indx).organization_id := NULL;
          l_xref_tbl(l_indx).org_independent_flag := 'Y';
        Else
          l_xref_tbl(l_indx).organization_id := p_organization_id;
          l_xref_tbl(l_indx).org_independent_flag := 'N';
        End If;
  
        g_call_point := 'Before populating API variables';
        l_xref_tbl(l_indx).inventory_item_id := p_inventory_item_id;
        l_xref_tbl(l_indx).cross_reference_type := l_cross_reference_type;
        l_xref_tbl(l_indx).cross_reference := p_cross_reference;
        --l_xref_tbl(l_indx).description := p_description; --removed 06/29/2015 per client testing feedback
        if p_description is not null then 
          l_xref_tbl(l_indx).attribute1 := p_description; --added 06/29/2015 per client testing feedback
          l_xref_tbl(l_indx).attribute_category := FND_PROFILE.VALUE('ORG_ID'); --added 06/29/2015 per client testing feedback
        end if;
        
        l_xref_tbl(l_indx).last_update_date := SYSDATE;
        l_xref_tbl(l_indx).last_updated_by := p_user_id;
        l_xref_tbl(l_indx).creation_date := SYSDATE;
        l_xref_tbl(l_indx).created_by := p_user_id;
        l_xref_tbl(l_indx).last_update_login := FND_GLOBAL.login_id;
        l_xref_tbl(l_indx).request_id := FND_GLOBAL.conc_request_id;
        l_xref_tbl(l_indx).program_application_id := FND_GLOBAL.prog_appl_id;
        l_xref_tbl(l_indx).program_id :=FND_GLOBAL.conc_program_id;
        l_xref_tbl(l_indx).program_update_date := SYSDATE;

      
        g_call_point := 'Before call to MTL_CROSS_REFERENCES_PUB.Process_XRef';
        
          MTL_CROSS_REFERENCES_PUB.Process_XRef(p_api_version        => 1.0
                                               ,p_init_msg_list      => FND_API.G_TRUE
                                               ,p_commit             => FND_API.G_FALSE
                                               ,p_xref_tbl           => l_xref_tbl
                                               ,x_return_status      => l_return_status
                                               ,x_msg_count          => l_msg_count
                                               ,x_message_list       => l_message_list
                                               );
  
  
          debug_log('l_return_status ' || l_return_status);
      
          If l_return_status <> 'S' Then
            debug_log('l_msg_count ' || l_msg_count);
            g_message := '';
            For ndx In 1..l_message_list.COUNT Loop
              g_message :=  l_message_list(ndx).message_text;
              debug_log(g_message);
            End Loop;
            RAISE g_exception;
            
          End If;
      
        COMMIT;
        
      END IF;
      
        x_return := 0;
        x_message := 'Success';
    
    
    EXCEPTION
        WHEN g_exception THEN
            
          x_return := 1;
          x_message := g_message;
              
                xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                     , p_calling             => g_call_point
                                                     , p_ora_error_msg       => SQLERRM
                                                     , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                     , p_distribution_list   => g_distro_list
                                                     , p_module              => g_module);
          WHEN OTHERS THEN
            
            g_sqlcode := SQLCODE;
            g_sqlerrm := SQLERRM;
            g_message := 'When others ' ||g_sqlcode || g_sqlerrm;
            
            x_return := 1;
            x_message := g_message;
            
                xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                     , p_calling             => g_call_point
                                                     , p_ora_error_msg       => SQLERRM
                                                     , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                     , p_distribution_list   => g_distro_list
                                                     , p_module              => g_module);


   
    END PROCESS_XREF;
    
     /*****************************************************************************************************************************************
     *  PROCEDURE GET_SALES_ORDER_LOV                                                                                                         *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure used for STANDARD and INTERNAL Orders                                                                      *
     *                                                                                                                                        *
     *    Parameters: None                                                                                                                    *
     *    Return : Cursor for Sales Order                                                                                                     *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        20-JUN-2015  Lee Spitzer               TMS Ticket Number 20150622-00006 RF - Load Check                                   *
     *****************************************************************************************************************************************/


    PROCEDURE GET_SALES_ORDER_LOV ( x_sales_order       OUT    NOCOPY t_genref --0
                                  , p_organization_id   IN     VARCHAR2
                                  , p_order_number      IN     VARCHAR2
                              )
  
      IS                            
  
      l_organization_id NUMBER;
  
     BEGIN
      
      g_call_from := 'GET_SALES_ORDER_LOV';
      g_call_point := 'Start';
 
    
        IF g_debug = 'Y' THEN
              debug_log('p_organization_id ' || p_organization_id);
              debug_log('p_order_number ' || p_order_number);
        END IF;
    
      
        if x_sales_order%isOPEN then
           debug_log('closing x_sales_order cursor');
            close x_sales_order;
        
        end if;
        
        MO_GLOBAL.INIT('ONT');
      
      
      BEGIN
        SELECT to_number(p_organization_id) INTO l_organization_id FROM dual;
      EXCEPTION
        WHEN others THEN
          g_sqlcode := SQLCODE;
          g_sqlerrm := SQLERRM;
          g_message := 'Could not convert organization id to number ';
          raise g_exception;
      END;

      debug_log('l_organization_id ' || l_organization_id);
      
    
     g_call_point := 'Getting list of order numbers';

      OPEN x_sales_order FOR
          SELECT   DISTINCT
                             ooha.header_id, 
                             ooha.order_number,
                             to_char(trunc(ooha.booked_date),'MM/DD/YYYY'),
                             hca.cust_account_id,
                             hp.party_id,
                             hca.account_number,
                             hp.party_name customer_name,
                             ottt.name order_type
                      FROM   oe_order_headers ooha,
                             hz_cust_accounts hca,
                             hz_parties hp,
                             oe_transaction_types_tl ottt,
                             oe_transaction_types otta
                      WHERE  ooha.sold_to_org_id = hca.cust_account_id
                      AND    hca.party_id = hp.party_id
                      AND    ooha.order_number = p_order_number 
                      AND    nvl(ooha.cancelled_flag, 'N') = 'N'
                      AND    nvl(ooha.booked_flag,'N') = 'Y'
                      AND    nvl(ooha.open_flag,'N') = 'Y'
                      AND    ooha.flow_status_code not in ('CLOSED','CANCELLED')
                      AND    ottt.transaction_type_id = ooha.order_type_id
                      AND    ottt.NAME in ('STANDARD ORDER','INTERNAL ORDER')
                      AND    ottt.SOURCE_LANG = g_language
                      AND    otta.transaction_type_id = ottt.transaction_type_id
                      AND    otta.transaction_type_code = 'ORDER'
                      AND    otta.order_category_code = ooha.order_category_code
                      AND    EXISTS   (SELECT *
                                      FROM   oe_order_lines oola
                                      WHERE  oola.header_id = ooha.header_id
                                      AND    oola.ship_from_org_id = l_organization_id
                                      AND    nvl(oola.cancelled_flag,'N') = 'N'
                                      AND    nvl(oola.booked_flag,'N') = 'Y'
                                      AND    nvl(oola.open_flag,'N') = 'Y'
                                      AND    oola.flow_status_code not in('CLOSED','CANCELLED')
                                      AND    EXISTS (SELECT *
                                                     FROM   oe_transaction_types_tl ottt2,
                                                            oe_transaction_types otta2
                                                     WHERE  ottt2.transaction_type_id = oola.line_type_id
                                                     AND    ottt2.NAME in ('STANDARD LINE', 'INTERNAL LINE')
                                                     AND    ottt2.SOURCE_LANG = g_language
                                                     AND    otta2.transaction_type_id = ottt2.transaction_type_id
                                                     AND    otta2.transaction_type_code = 'LINE'
                                                     AND    otta2.order_category_code = oola.line_category_code
                                                     )
                                        );            

    EXCEPTION
        WHEN g_exception THEN
              
              if x_sales_order%isOPEN then
                 debug_log('closing x_sales_order cursor');
                  close x_sales_order;
              end if;
            
                xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                     , p_calling             => g_call_point
                                                     , p_ora_error_msg       => SQLERRM
                                                     , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                     , p_distribution_list   => g_distro_list
                                                     , p_module              => g_module);
          WHEN OTHERS THEN
            
            g_sqlcode := SQLCODE;
            g_sqlerrm := SQLERRM;
            g_message := 'When others ' ||g_sqlcode || g_sqlerrm;
            
            
              if x_sales_order%isOPEN then
                 debug_log('closing x_sales_order cursor');
                  close x_sales_order;
              end if;
              
              
                xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                     , p_calling             => g_call_point
                                                     , p_ora_error_msg       => SQLERRM
                                                     , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                     , p_distribution_list   => g_distro_list
                                                     , p_module              => g_module);
      END GET_SALES_ORDER_LOV;
                                

    
END XXWC_MWA_ROUTINES_PKG;
/