/*
 TMS: 20151125-00074 Unable to Cancel internal Order # 18788866 - Line # 1.2 - Order Line id - 59273608
 Req line id 25352126
 Unable To Save The Changes As The Corresponding Internal Requisition Cannot Be Updated (Doc ID 1990805.1)
 Date: 11/25/2015
 */
SET SERVEROUTPUT ON SIZE 1000000

DECLARE
   l_create_req_supply   BOOLEAN;
BEGIN
   l_create_req_supply := PO_SUPPLY.create_req (25352126, 'REQ LINE');
   COMMIT;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Unable to create Supply record ' || SQLERRM);
END;
/