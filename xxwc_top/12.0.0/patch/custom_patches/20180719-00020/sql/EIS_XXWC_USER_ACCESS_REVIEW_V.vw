-------------------------------------------------------------------------------------------
/*****************************************************************************************
  $Header XXEIS.EIS_XXWC_USER_ACCESS_REVIEW_V.vw $
  Module Name: EiS eXpress Administrator
  PURPOSE: Oracle User Access Review - WC
  REVISIONS:
  Ver          Date        Author                Description
  ---------  ----------  ------------------    ----------------
  1.0		 7/19/2018      Siva			   TMS#20180719-00020 
******************************************************************************************/
---------------------------------------------------------------------------------------------
CREATE OR REPLACE VIEW XXEIS.EIS_XXWC_USER_ACCESS_REVIEW_V (USER_NAME, EMPLOYEE_NUMBER, FULL_NAME, EMAIL_ADDRESS, SUPER_CODE, SUPER_NAME, ASSIGNMENT_REASON, LAST_ASSIGNED_BY, LAST_UPDATE_DATE, EFFECTIVE_START_DATE, EFFECTIVE_END_DATE, SUPERVISOR_EMAIL, SUPERVISOR_NAME, ROLE_ORIG_SYSTEM, BUYER_EMPLOYEE_ID, IS_BUYER, BUYER_POSITION, BUYER_JOB, GL_SEGMENT, SCOPE, JOB_DESCRIPTION, FRU, FRU_DESCRIPTION, BRANCH, REGION, LOB, REVIEWER, REVIEWER_EMAIL_ADDRESS, JOB_FAMILY_DESCR)
AS
  SELECT DISTINCT a.user_name ,
    NVL (p1.employee_number, '-') employee_number ,
    NVL (u.description, '-') full_name ,
    u.email_address email_address ,
    a.assigning_role super_code ,
    rsuper.display_name super_name ,
    a.assignment_reason ,
    uupdated.description last_assigned_by ,
    a.last_update_date ,
    a.effective_start_date ,
    a.effective_end_date ,
    p2.email_address supervisor_email ,
    p2.full_name supervisor_name ,
    a.role_orig_system ,
    buyers.agent_id buyer_employee_id,
    CASE
      WHEN buyers.agent_id IS NULL
      THEN 'N'
      ELSE 'Y'
    END is_buyer ,
    pos.name buyer_position ,
    pos.job_name buyer_job ,
    NVL (gc.segment1, '-') gl_segment ,
    CASE
      WHEN scope.user_name IS NULL
      THEN 'GSC'
      ELSE 'WC'
    END scope ,
    ps.job_descr job_description ,
    ps.attribute1 fru ,
    lc.fru_descr fru_description ,
    lc.lob_branch branch,
    mp.attribute9 region ,
    lc.business_unit lob,
    CASE
      WHEN upper(ps.job_family_descr) LIKE 'EXECUTIVE'
      THEN 'Manual Review by Compliance Manager'
      ELSE NVL(xacmv.manager_name,p2.full_name)
    END reviewer,
    NVL(xacmv.spvr_email_addr,p2.email_address) reviewer_email_address,
    ps.job_family_descr
  FROM applsys.wf_user_role_assignments a ,
    applsys.fnd_user u,
    applsys.fnd_user uupdated ,
    applsys.wf_local_roles rsuper,
    (SELECT DISTINCT user_name
    FROM
      (SELECT DISTINCT u.user_name
      FROM apps.per_people_f per,
        apps.per_all_assignments_f ass,
        apps.gl_code_combinations gc,
        apps.fnd_user u
      WHERE per.person_id                           = ass.person_id
      AND ass.default_code_comb_id                  = gc.code_combination_id
      AND per.person_id                             = u.employee_id
      AND gc.segment1                               = '0W'
      AND NVL (per.effective_end_date, sysdate + 1) > sysdate
      AND NVL (u.end_date, sysdate             + 1) > sysdate
      UNION
      SELECT DISTINCT a.user_name
      FROM applsys.wf_user_role_assignments a
      WHERE a.role_name LIKE '%XXWC%'
      AND a.effective_start_date <= sysdate
      AND a.effective_end_date   >= sysdate
      )
    ) scope,
    apps.per_people_f p1,
    apps.per_all_assignments_f pa1,
    apps.gl_code_combinations gc,
    apps.per_people_f p2,
    po_agents buyers,
    hr_employees hre,
    apps.per_positions_v pos,
    xxcus.xxcushr_ps_emp_all_tbl ps,
    xxcus.xxcus_location_code_tbl lc,
    apps.mtl_parameters mp,
    (SELECT (manager_last_name
      ||', '
      ||manager_first_name) manager_name,
      spvr_email_addr,
      upper(contractors_ntid) user_name
    FROM apps.xxwc_actdir_contractor_mngr_v
    )xacmv
  WHERE a.user_name     = u.user_name
  AND (sysdate         >= u.start_date
  AND sysdate           < NVL (u.end_date, sysdate + 1))
  AND a.last_updated_by = uupdated.user_id
  AND a.assigning_role  = rsuper.name
  AND (a.assigning_role LIKE 'UMX%'
  OR a.assigning_role LIKE 'FND_RESP|%')
  AND a.effective_start_date <= sysdate
  AND a.effective_end_date   >= sysdate
  AND u.user_name             = scope.user_name(+)
  AND u.employee_id           = p1.person_id(+)
  AND TRUNC(sysdate) BETWEEN p1.effective_start_date(+) AND p1.effective_end_date(+)
  AND p1.person_id = pa1.person_id(+)
  AND TRUNC(sysdate) BETWEEN pa1.effective_start_date(+) AND pa1.effective_end_date(+)
  AND pa1.default_code_comb_id = gc.code_combination_id(+)
  AND pa1.supervisor_id        = p2.person_id(+)
  AND TRUNC(sysdate) BETWEEN p2.effective_start_date(+) AND p2.effective_end_date(+)
  AND p1.person_id            = hre.employee_id(+)
  AND hre.employee_id         = buyers.agent_id(+)
  AND (sysdate               >= NVL (buyers.start_date_active, sysdate - 1)
  AND sysdate                 < NVL (buyers.end_date_active, sysdate   + 1))
  AND pa1.position_id         = pos.position_id(+)
  AND p1.employee_number      = ps.employee_number(+)
  AND ps.attribute1           = lc.fru(+)
  AND lc.fru                  = mp.attribute10(+)
  AND lc.lob_branch           = mp.attribute11(+)
  AND u.user_name             = xacmv.user_name(+)
  AND a.role_name             = a.assigning_role
  AND a.effective_end_date(+) = '01-JAN-9999'
  AND rsuper.display_name LIKE '%Role%'
  AND NOT EXISTS
    (SELECT 1
    FROM
      (SELECT user_name FROM fnd_user WHERE user_name LIKE 'XX%'
      UNION
      SELECT user_name
      FROM fnd_user
      WHERE user_name IN ('DLSMANAGER','DLSUSER','GLINTERFACE','INTERFACE_DESCARTES','IRC_EMP_GUEST','INTERFACE_EQUIFAX','IRC_EXT_GUEST')
      )b
    WHERE b.user_name=a.user_name
    )
  AND RSUPER.DISPLAY_NAME NOT IN ('HDS Internet Expenses User','HDS Internet Expenses User - CAD','WC Role - Associate')
/
 