/*
TMS:20180222-00063 - FW: Please read: Duplicate cat-class assignment for item #121AMF99 in branch #853
Author: P.Vamshidhar
Delete duplicate record from ISR
 */
SET SERVEROUTPUT ON SIZE 1000000

BEGIN
   DBMS_OUTPUT.put_line ('Before delete1');

   DELETE FROM xxwc.xxwc_isr_details_all
         WHERE     inventory_item_id = 11784979
               AND organization_id = 369
               AND INV_CAT_SEG1 = 'QQ';

   DBMS_OUTPUT.put_line ('Records deleted1-' || SQL%ROWCOUNT);
   COMMIT;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Unable to delete ' || SQLERRM);
END;
/