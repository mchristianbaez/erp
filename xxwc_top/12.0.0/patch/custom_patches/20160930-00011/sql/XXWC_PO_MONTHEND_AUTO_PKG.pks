CREATE OR REPLACE PACKAGE APPS.XXWC_PO_MONTHEND_AUTO_PKG
IS
   /*************************************************************************
   *  Copyright (c) 2016 HD Supply Inc. 
   *  All rights reserved.
   **************************************************************************
   *   $Header XXWC_PO_MONTHEND_AUTO_PKG.pks $
   *   Module Name: XXWC_PO_MONTHEND_AUTO_PKG.pks
   *
   *   PURPOSE:    This package is used to Automate PO month end process.
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        11/15/2016  Neha Saini               Initial Version TMS# 20160930-00011 
   * ***************************************************************************/

   /**************************************************************************
   *   procedure Name: open_next_period
   *  *   Parameter:
   *          IN   p_period_name
   *   PURPOSE:  This procedure is used to open next period for PO
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        11/15/2016  Neha Saini             Initial Version TMS# 20160930-00011 
   * ***************************************************************************/
   PROCEDURE open_next_period ( p_period_name varchar2);
     /**************************************************************************
   *   procedure Name: close_curr_period
   *  *   Parameter:
   *          IN    p_period_name,p_next_period 
   *   PURPOSE:  This procedure is used to close current period for PO
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        11/15/2016  Neha Saini             Initial Version TMS# 20160930-00011
   * ***************************************************************************/
   PROCEDURE close_curr_period ( p_period_name varchar2,p_next_period VARCHAR2); 
   /*************************************************************************
   *   Procedure : main
   *
   *   PURPOSE:   This procedure is for concurrent program -> XXWC PO Period Open/Close.
   *   Parameter:
   *          IN  p_operating_unit,p_action,p_next_period,p_curr_period
   *          OUT   ERRBUF,RETCODE
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        11/15/2016  Neha Saini               Initial Version TMS# 20160930-00011 
   * ************************************************************************/

   PROCEDURE main(ERRBUF              OUT VARCHAR2,
                  RETCODE             OUT NUMBER,
                  p_operating_unit    IN  NUMBER,
                  p_action            IN  VARCHAR2,
                  p_next_period       IN  VARCHAR2,
                  p_curr_period       IN  VARCHAR2  );
  
END XXWC_PO_MONTHEND_AUTO_PKG;
/
