CREATE OR REPLACE PACKAGE BODY APPS.XXWC_PO_MONTHEND_AUTO_PKG
AS
   /*************************************************************************
   *  Copyright (c) 2016 HD Supply Inc.
   *  All rights reserved.
   **************************************************************************
   *   $Header XXWC_PO_MONTHEND_AUTO_PKG.pkb $
   *   Module Name: XXWC_PO_MONTHEND_AUTO_PKG.pkb
   *
   *   PURPOSE:    This package is used to Automate PO month end process.
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        11/15/2016  Neha Saini               Initial Version TMS# 20160930-00011
   * ***************************************************************************/
   /*****************************
 -- GLOBAL VARIABLES
 ******************************/

   g_error_message     VARCHAR2 (32000);
   g_location          VARCHAR2 (10000);
   g_ledger_id         NUMBER;
   g_ap_app_id         NUMBER;
   g_po_app_id         NUMBER;
   G_EXCEPTION         EXCEPTION;
   g_user_id           NUMBER;
   g_org_id            NUMBER;
   --Global variable to store the org id and conc request id
   g_conc_request_id   NUMBER := FND_GLOBAL.CONC_REQUEST_ID;


   /*************************************************************************
   *  Procedure : Write_Error
   *
   * PURPOSE:   This procedure logs error message in Custom Error table
   * Parameter:
   *        IN
   *            p_debug_msg      -- Debug Message
   * REVISIONS:
   * Ver        Date        Author                     Description
   *  ---------  ----------  ---------------         -------------------------
   *  1.0        11/15/2016  Neha Saini               Initial Version TMS# 20160930-00011
  ************************************************************************/
   --add message to Error table
   PROCEDURE write_error (p_debug_msg IN VARCHAR2)
   IS
      l_err_callfrom    VARCHAR2 (100) DEFAULT 'XXWC_PO_MONTHEND_AUTO_PKG';
      l_err_callpoint   VARCHAR2 (100) DEFAULT 'START';
      l_distro_list     VARCHAR2 (100)
                           DEFAULT 'HDSOracleDevelopers@hdsupply.com';
   BEGIN
      xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => l_err_callfrom,
         p_calling             => l_err_callpoint,
         p_request_id          => g_conc_request_id,
         p_ora_error_msg       => SUBSTR (p_debug_msg, 1, 2000),
         p_error_desc          => 'Error running XXWC_PO_MONTHEND_AUTO_PKG with PROGRAM ERROR ',
         p_distribution_list   => l_distro_list,
         p_module              => 'PO');
   END write_error;

   /*************************************************************************
   *   Procedure : Write_output
   *
   *   PURPOSE:   This procedure is used for adding message to concurrent output file
   *   Parameter:
   *          IN
   *              p_debug_msg      -- Debug Message
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *  ---------  ----------  ---------------         -------------------------
   *   1.0        11/15/2016  Neha Saini               Initial Version TMS# 20160930-00011
   * ************************************************************************/

   PROCEDURE Write_output (p_debug_msg IN VARCHAR2)
   IS
   BEGIN
      IF NVL (g_conc_request_id, 0) <= 0
      THEN
         DBMS_OUTPUT.PUT_LINE (p_debug_msg);
      ELSE
         fnd_file.put_line (fnd_file.output, p_debug_msg);
         fnd_file.put_line (fnd_file.LOG, p_debug_msg);
      END IF;
   END Write_output;

   /**************************************************************************
   *   procedure Name: open_next_period
   *  *   Parameter:
   *          IN   p_period_name
   *          OUT  None
   *   PURPOSE:  This procedure is used to open next period for PO
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        11/15/2016  Neha Saini             Initial Version TMS# 20160930-00011
   * ***************************************************************************/
   PROCEDURE open_next_period (p_period_name VARCHAR2)
   IS
   BEGIN
      g_error_message := NULL;
      g_location := 'opening next period now';
      write_output (g_location);

      UPDATE gl_period_statuses
         SET closing_status = 'O',
             last_updated_by = fnd_global.user_id,
             last_update_date = SYSDATE,
             last_update_login = fnd_global.login_id
       WHERE     set_of_books_id = g_ledger_id
             AND period_name = p_period_name
             AND application_id = g_po_app_id
             AND closing_status <> 'O';

     

      IF SQL%ROWCOUNT > 0
      THEN
         g_location := 'Next period ' || p_period_name || ' Open Successfully';
         write_output (g_location);
      ELSE
         g_location :=
            'Next period ' || p_period_name || ' is not Opened yet .';
         write_output (g_location);
      END IF;
      
      COMMIT;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         g_error_message :=
               'Error at open next period for '
            || p_period_name
            || '-'
            || 'Period is either already open or does not exist. please check gl_perid_statuses table.'
            || ' - '
            || SQLERRM;
      WHEN TOO_MANY_ROWS
      THEN
         g_error_message :=
               'Error at open next period for '
            || p_period_name
            || '-'
            || ' too many rowas returned for this period .please check gl_perid_statuses table.'
            || ' - '
            || SQLERRM;
      WHEN OTHERS
      THEN
         g_error_message :=
               'Error at open next period for '
            || p_period_name
            || ' - '
            || SQLERRM;
   END open_next_period;

   /**************************************************************************
 *   procedure Name: close_curr_period
 *  *   Parameter:
 *          IN    p_period_name,,p_next_period
 *          OUT   None
 *   PURPOSE:  This procedure is used to close current period for PO
 *   REVISIONS:
 *   Ver        Date        Author                     Description
 *   ---------  ----------  ---------------         -------------------------
 *   1.0        11/15/2016  Neha Saini             Initial Version TMS# 20160930-00011
 * ***************************************************************************/
   PROCEDURE close_curr_period (p_period_name    VARCHAR2,
                                p_next_period    VARCHAR2)
   IS
      v_app_id              NUMBER;
      v_resp_id             NUMBER;
      v_resp_name           VARCHAR2 (100);
      v_request_id          NUMBER;
      v_app_name            VARCHAR2 (20);
      v_req_phase           VARCHAR2 (100);
      v_req_status          VARCHAR2 (100);
      v_req_dev_phase       VARCHAR2 (100);
      v_req_dev_status      VARCHAR2 (100);
      v_req_message         VARCHAR2 (100);
      v_req_return_status   BOOLEAN;
   BEGIN
      g_error_message := NULL;
      g_location := 'Start " close current period "  now';
      write_output (g_location);


      BEGIN
         g_error_message := NULL;
         g_location := 'Closing Current PO period now';
         write_output (g_location);

         UPDATE gl_period_statuses
            SET closing_status = 'C',
                last_updated_by = fnd_global.user_id,
                last_update_date = SYSDATE,
                last_update_login = fnd_global.login_id
          WHERE     set_of_books_id = g_ledger_id
                AND period_name = p_period_name
                AND application_id = g_po_app_id
                AND closing_status = 'O';

         

         IF SQL%ROWCOUNT > 0
         THEN
            g_location :=
               'Current period ' || p_period_name || ' Close Successfully';
            write_output (g_location);
         ELSE
            g_location :=
               'Next period ' || p_period_name || ' is not Closed yet.';
            write_output (g_location);
         END IF;
         
         COMMIT;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            g_error_message :=
                  'Error at open next period for '
               || p_period_name
               || '-'
               || 'Period is either already open or does not exist. please check gl_perid_statuses table.'
               || ' - '
               || SQLERRM;
            RAISE g_exception;
         WHEN TOO_MANY_ROWS
         THEN
            g_error_message :=
                  'Error at open next period for '
               || p_period_name
               || '-'
               || ' too many rowas returned for this period .please check gl_perid_statuses table.'
               || ' - '
               || SQLERRM;
            RAISE g_exception;
         WHEN OTHERS
         THEN
            g_error_message :=
                  'Error at open next period for '
               || p_period_name
               || ' - '
               || SQLERRM;
            RAISE g_exception;
      END;


      --  IF the update successful then submit program "Reset Period End Accrual Flags" using FND_REQUEST.submit_request API

      --submit �Reset Period End Accrual Flags"

      g_user_id := FND_GLOBAL.USER_ID;


      g_location := 'get user id ' || g_user_id;
      write_output (g_location);

      BEGIN
         mo_global.init ('PO');
         mo_global.set_policy_context ('S', 162);

         SELECT fa.application_id,
                fr.responsibility_id,
                fr.responsibility_name,
                fa.application_short_name
           INTO v_app_id,
                v_resp_id,
                v_resp_name,
                v_app_name
           FROM fnd_responsibility_vl fr, fnd_application fa
          WHERE     responsibility_name LIKE 'HDS Purchasing Super User'
                AND fr.application_id = fa.application_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            v_app_id := NULL;
            v_resp_id := NULL;
            v_resp_name := NULL;
            v_app_name := NULL;
      END;

      -- Initializing apps
      fnd_global.apps_initialize (user_id        => g_user_id,
                                  resp_id        => v_resp_id,
                                  resp_appl_id   => v_app_id);

      g_location :=
         'Now submitting request for " Reset Period End Accrual Flags " ';
      write_output (g_location);
      --Environment Set up
      FND_REQUEST.SET_ORG_ID (162);

      --submitted "Reset Period End Accrual Flags"
      v_request_id :=
         fnd_request.submit_request (application   => v_app_name, /* Short name of the application under which the program is registered in this it will be ONT since the Reserve Order program is registered under order management*/
                                     program       => 'POXRAF', -- Short name of the concurrent program needs to submitted
                                     description   => NULL,
                                     start_time    => NULL,
                                     sub_request   => NULL);



      COMMIT;

      g_location := 'Concurrent Program Request ID' || v_request_id;
      write_output (g_location);


      -- Concurrent Program Status
      IF v_request_id <> 0 AND v_request_id IS NOT NULL
      THEN
         LOOP
            v_req_return_status :=
               fnd_concurrent.wait_for_request (v_request_id, -- request ID for which results need to be known
                                                60, --interval parameter  time between checks default is 60 Seconds
                                                0, -- Max wait default is 0 , this is the maximum amount of time a program should wait for it to get completed
                                                v_req_phase,
                                                v_req_status,
                                                v_req_dev_phase,
                                                v_req_dev_status,
                                                v_req_message);
            /*Others are output parameters to get the phase and status of the submitted concurrent program*/
            EXIT WHEN    UPPER (v_req_phase) = 'COMPLETED'
                      OR UPPER (v_req_status) IN ('CANCELLED',
                                                  'ERROR',
                                                  'TERMINATED');
         END LOOP;
      END IF;

      g_location := 'Concurrent Program Status' || v_req_status;
      write_output (g_location);

      --check if "Reset Period End Accrual Flags"  completed successfully if not end program with warning
      IF v_req_status IN ('CANCELLED', 'ERROR', 'TERMINATED')
      THEN
         g_error_message :=
            'Program " Reset Period End Accrual Flags "  did not completed Successfully';
         RAISE g_exception;
      END IF;

      --End of procedure
      g_location := 'Completed Successfully close_curr_period';
      write_output (g_location);
   EXCEPTION
      WHEN g_exception
      THEN
         g_error_message :=
               'Error at close current period '
            || g_error_message
            || '-'
            || SUBSTR (SQLERRM, 1, 1000);
      WHEN OTHERS
      THEN
         g_error_message :=
               'Error at close current period '
            || g_error_message
            || '-'
            || SQLERRM;
   END close_curr_period;

   /*************************************************************************
   *   Procedure : main
   *
   *   PURPOSE:   This procedure is for concurrent program -> XXWC PO Period Open/Close.
   *               Short Name: XXWC_PO_PERIOD_OPN_CLS
   *               Executable: XXWC_PO_PERIOD_OPN_CLS
   *
   *   Parameter:
   *          IN  p_operating_unit,p_action,p_next_period,p_curr_period
   *          OUT   ERRBUF,RETCODE
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        11/15/2016  Neha Saini               Initial Version TMS# 20160930-00011
   * ************************************************************************/

   PROCEDURE main (ERRBUF                OUT VARCHAR2,
                   RETCODE               OUT NUMBER,
                   p_operating_unit   IN     NUMBER,
                   p_action           IN     VARCHAR2,
                   p_next_period      IN     VARCHAR2,
                   p_curr_period      IN     VARCHAR2)
   IS
      l_next_ap_status   APPS.gl_period_statuses.closing_status%TYPE;
      l_curr_ap_status   APPS.gl_period_statuses.closing_status%TYPE;
      l_next_po_status   APPS.gl_period_statuses.closing_status%TYPE;
      l_count            NUMBER;
   BEGIN
      --initializing variables
      g_error_message := NULL;
      ERRBUF := NULL;
      RETCODE := 0;
      --printing IN parameters
      g_location := 'Start main for XXWC PO Period Open/Close Program';
      write_output (g_location);
      g_location := 'Now printing IN parameters ';
      write_output (g_location);
      g_location := 'p_operating_unit ' || p_operating_unit;
      write_output (g_location);
      g_location := 'p_action ' || p_action;
      write_output (g_location);
      g_location := 'p_next_period ' || p_next_period;
      write_output (g_location);
      g_location := 'p_curr_period ' || p_curr_period;
      write_output (g_location);

      -- get ledger id
      SELECT set_of_books_id
        INTO g_ledger_id
        FROM apps.hr_operating_units
       WHERE organization_id = p_operating_unit;

      g_location := 'g_ledger_id Ledger Id ' || g_ledger_id;
      write_output (g_location);

      -- get AP application id
      SELECT application_id
        INTO g_ap_app_id
        FROM apps.fnd_application_vl
       WHERE application_name LIKE 'Payables';

      g_location := 'g_ap_app_id  AP Application ID ' || g_ap_app_id;
      write_output (g_location);

      -- get PO application id
      SELECT application_id
        INTO g_po_app_id
        FROM apps.fnd_application_vl
       WHERE application_name LIKE 'Purchasing';

      g_location := 'g_po_app_id PO Application ID ' || g_po_app_id;
      write_output (g_location);
      g_org_id := NVL (FND_PROFILE.VALUE ('ORG_ID'), 162);

      g_location := 'g_org_id ORG ID ' || g_org_id;
      write_output (g_location);

      -- Actual logic started here

      --get next AP period Status
      BEGIN
         SELECT closing_status
           INTO l_next_ap_status
           FROM gl_period_statuses gps
          WHERE     gps.period_name = p_next_period
                AND gps.application_id = g_ap_app_id
                AND gps.set_of_books_id = g_ledger_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            g_error_message :=
                  'Error fetching status of AP next period '
               || SUBSTR (SQLERRM, 1, 1000);
            RAISE G_EXCEPTION;
      END;

      g_location :=
         'l_next_ap_status  for  next AP period status ' || l_next_ap_status;
      write_output (g_location);

      --get current AP period status
      BEGIN
         SELECT closing_status
           INTO l_curr_ap_status
           FROM gl_period_statuses gps
          WHERE     gps.period_name = p_curr_period
                AND gps.application_id = g_ap_app_id
                AND gps.set_of_books_id = g_ledger_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            g_error_message :=
                  'Error fetching status of AP currrent period '
               || SUBSTR (SQLERRM, 1, 1000);
            RAISE G_EXCEPTION;
      END;

      g_location :=
            'l_curr_ap_status  for  current AP period status '
         || l_curr_ap_status;
      write_output (g_location);

      --get status for PO Next Period
      BEGIN
         SELECT closing_status
           INTO l_next_po_status
           FROM gl_period_statuses gps
          WHERE     gps.period_name = p_next_period
                AND gps.application_id = g_po_app_id
                AND gps.set_of_books_id = g_ledger_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            g_error_message :=
                  'Error fetching status of PO next period '
               || SUBSTR (SQLERRM, 1, 1000);
            RAISE G_EXCEPTION;
      END;

      g_location :=
         'l_next_po_status  for next PO period status ' || l_next_po_status;
      write_output (g_location);

      IF p_action = 'Close'
      THEN
         --Current PO period should only be allowed to close if:
         --Next AP period = Open
         --Current AP period = Closed
         --Next PO period = Open
         --check if Next AP period is OPen
         IF l_next_ap_status = 'O'
         THEN
            --check if currentr Ap period is Closed
            IF l_curr_ap_status = 'C'
            THEN
               --check if Next PO period is OPen
               IF l_next_po_status = 'O'
               THEN
                  g_location := ' Trying to close Current period Now';
                  write_output (g_location);
                  --close current period
                  close_curr_period (p_curr_period, p_next_period);

                  IF g_error_message IS NOT NULL
                  THEN
                     RAISE G_EXCEPTION;
                  END IF;
               ELSE
                  g_error_message := '  NEXT PO period is not OPEN ';
                  RAISE G_EXCEPTION;
               END IF;
            ELSE
               g_error_message := '  Current AP period is not CLOSE ';
               RAISE G_EXCEPTION;
            END IF;
         ELSE
            g_error_message := '  Next AP period is not OPEN ';
            RAISE G_EXCEPTION;
         END IF;
      ELSIF p_action = 'Open'
      THEN
         --Next PO period should only be allowed to open if:
         --Next AP Period = Open
         --Current AP Period (both CI and GSC) = Closed

         --check if period is already not opened
         BEGIN
            SELECT COUNT (1)
              INTO l_count
              FROM gl_period_statuses
             WHERE     set_of_books_id = g_ledger_id
                   AND period_name = p_next_period
                   AND application_id = g_po_app_id
                   AND closing_status = 'O';

            IF l_count > 0
            THEN
               g_location :=
                  ' Period ' || p_next_period || ' is already open for PO';
               write_output (g_location);
            END IF;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_count := 0;
         END;

         --check if Next AP period is OPEN AND  --check if Currenr AP period is CLOSE
         IF l_count = 0
         THEN
            IF l_next_ap_status = 'O' -- check for both C and I and GSC need to confirm from RAM
            THEN
               IF l_curr_ap_status = 'C'
               THEN
                  g_location := ' Trying to open next period Now';
                  write_output (g_location);
                  --open next period
                  open_next_period (p_next_period);

                  IF g_error_message IS NOT NULL
                  THEN
                     RAISE G_EXCEPTION;
                  END IF;
               ELSE
                  g_error_message := '  Current AP period is not CLOSE ';
                  RAISE G_EXCEPTION;
               END IF;
            ELSE
               g_error_message := '  Next AP period is not OPEN ';
               RAISE G_EXCEPTION;
            END IF;
         END IF;
      END IF;

      -- Logic ends here

      g_location :=
         'Successful End of main for XXWC PO Period Open/Close Program';
      write_output (g_location);
   EXCEPTION
      WHEN G_EXCEPTION
      THEN
         ERRBUF := NULL;
         RETCODE := 1;
         g_location :=
               'Error at g_exception in main  '
            || g_error_message
            || ' - '
            || SUBSTR (SQLERRM, 1, 1000);
         write_output (g_location);
      WHEN OTHERS
      THEN
         ERRBUF := NULL;
         RETCODE := 2;
         g_location :=
               'Error at when others in main  '
            || g_error_message
            || ' -  '
            || SUBSTR (SQLERRM, 1, 1000);
         write_output (g_location);
   END;
END XXWC_PO_MONTHEND_AUTO_PKG;
/