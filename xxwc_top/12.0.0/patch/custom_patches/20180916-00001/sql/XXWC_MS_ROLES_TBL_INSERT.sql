/*************************************************************************
 Copyright (c) 2012 HD Supply
 All rights reserved.
**************************************************************************
   $Header XXWC_SCAF_MAINT_SEQ $
  Module Name: XXWC_SCAF_MAINT_SEQ

  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)        DESCRIPTION
  -- ------- -----------   ------------     -----------------------------------------------
  -- 1.0     23-AUG-2018   Rakesh Patel     TMS#20180916-00001-Sales Support Form Enhancement - SCAF
  *************************************************************************/
SET DEFINE OFF;

Insert into WC_APPS.XXWC_MS_ROLES_TBL
   (ROLE_ID, ROLE_NAME, EMAIL, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY)
 Values
   (40, 'SCAF-CAF', NULL , SYSDATE, 'WC_APPS', SYSDATE, 'WC_APPS');

insert into WC_APPS.XXWC_MS_ROLES_TBL
   (ROLE_ID, ROLE_NAME, EMAIL, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY)
Values
   (41, 'SCAF-HR-VP', NULL, SYSDATE, 'WC_APPS', SYSDATE, 'WC_APPS');
	
COMMIT;

insert into WC_APPS.XXWC_MS_ROLES_TBL
   (ROLE_ID, ROLE_NAME, EMAIL, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY)
 Values
   (42, 'SCAF-RHR', NULL, SYSDATE, 'WC_APPS', SYSDATE, 'WC_APPS');
	
COMMIT;

insert into WC_APPS.XXWC_MS_ROLES_TBL
   (ROLE_ID, ROLE_NAME, EMAIL, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY)
 Values
   (43, 'SCAF-SSG', NULL, SYSDATE, 'WC_APPS', SYSDATE, 'WC_APPS');
	
COMMIT;
