/*************************************************************************
  $Header TMS_20170221-00191_Branch_015_ISO.sql $
  Module Name: TMS_20170221-00191   Branch 015 ISO

  PURPOSE: Data fix to update the printer details

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        23-FEB-2017  Pattabhi Avula         TMS#20170221-00191 

**************************************************************************/ 

SET SERVEROUTPUT ON SIZE 100000;
SET VERIFY OFF;

DECLARE
BEGIN

DBMS_OUTPUT.put_line ('TMS: 20170221-00191    , Before Update');

    UPDATE apps.wsh_report_printers
       SET DEFAULT_PRINTER_FLAG=NULL
     WHERE PRINTER_NAME='P141';

DBMS_OUTPUT.put_line (
         'TMS: 20170221-00191 - number of records updated: '
      || SQL%ROWCOUNT);
COMMIT;
   DBMS_OUTPUT.put_line ('TMS: 20170221-00191   , End Update');
   
END; 
/