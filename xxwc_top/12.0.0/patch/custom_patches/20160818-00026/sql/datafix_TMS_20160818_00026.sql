/*************************************************************************
  $Header datafix_TMS_20160818_00026.sql $
  Module Name: TMS_20160818_00026


  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
   1.0       30-AUG-2016  Niraj K Ranjan        TMS#20160818-00026   ORder# 21487515 - stuck in pending pre-bill
**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20160818_00026   , Before Update');

   update apps.oe_order_lines_all
   set INVOICE_INTERFACE_STATUS_CODE='YES'
      ,REVREC_SIGNATURE_DATE=sysdate
      ,open_flag='N'
      ,flow_status_code='CLOSED'
      ,INVOICED_QUANTITY=144
   where line_id=77598873
   and headeR_id=46702976;


   DBMS_OUTPUT.put_line (
         'TMS: 20160818_00026  Sales order lines updated (Expected:1): '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20160818_00026    , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20160818_00026 , Errors : ' || SQLERRM);
END;
/
