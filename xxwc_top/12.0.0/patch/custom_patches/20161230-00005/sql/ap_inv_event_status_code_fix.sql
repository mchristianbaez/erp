REM $Header: ap_inv_event_status_code_fix.sql 120.0.12010000.7 2014/09/29 22:37:19 vinaik noship $
REM +=======================================================================+
REM |    Copyright (c) 2001, 2014 Oracle Corporation, Redwood Shores, CA, USA     |
REM |                         All rights reserved.                          |
REM +=======================================================================+
REM | FILENAME                                                              |
REM |     ap_inv_event_status_code_fix.sql                                  |
REM |                                                                       |
REM | DESCRIPTION                                                           |
REM |     This script will correct all records in xla_events that have      |
REM |     invalid event_status_code                                         |
REM | HISTORY Created by zrehman on 22-April-2008                           |
REM +=======================================================================+
REM dbdrv:none

SET SERVEROUTPUT ON;
SET VERIFY OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK;
WHENEVER OSERROR EXIT FAILURE ROLLBACK;

/*PROMPT ________________________________________________________
PROMPT
PROMPT Following are the parameters that would be asked for
PROMPT by the script, along with their meanings:
PROMPT
PROMPT Email_id:
PROMPT ========
PROMPT A valid email id to get log output files in email.
PROMPT This is an optional features, to make use of this feature please 
PROMPT Follow the Metalink Note 1582550.1 and pass valid email id. 
PROMPT
PROMPT This feature can be ignored by not passing email id 
PROMPT (just by pressing enter key).
PROMPT
PROMPT ________________________________________________________*/

DECLARE

  l_file_location             v$parameter.value%TYPE;
  l_message                   VARCHAR2(500);
  l_count                     NUMBER;
  l_count1                    NUMBER;
  l_bug_no                    NUMBER := 6992111;  
  l_instance_name             VARCHAR2(100);
  l_host_name                 VARCHAR2(100);
  l_bkp_tab                   ALL_TABLES.TABLE_NAME%TYPE;
  l_driver_tab                ALL_TABLES.TABLE_NAME%TYPE;
  l_select_list               LONG;
  l_table_name                ALL_TABLES.TABLE_NAME%TYPE;
  l_where_clause              LONG;
  l_col_list                  LONG;
  l_sql_stmt                  LONG;
  l_debug_info                LONG;
  l_calling_sequence          LONG;
  l_email_flag                BOOLEAN DEFAULT FALSE;
  l_email_id                  VARCHAR2(255) := 'ramabujjayya.talluri@hdsupply.com';
  l_date                      VARCHAR2(100);

BEGIN

  l_calling_sequence := 'ap_inv_event_status_code_fix';

  ---------------------------------------------------------------------
  -- Step 1: Set the Log and Output files
  ---------------------------------------------------------------------
  l_message := 'Set log and out files';
  AP_Acctg_Data_Fix_PKG.Open_Log_Out_Files(6992111||'-fix',l_file_location);
  AP_Acctg_Data_Fix_PKG.Print('<html><body>');


  -----------------------------------------------------------------------------
  --               Printing instance details
  -----------------------------------------------------------------------------

  BEGIN
   l_date := to_char(sysdate,'DD-MON-YYYY HH24:MI:SS');
    
    SELECT instance_name, host_name
      INTO l_instance_name, l_host_name
      FROM v$instance;
    AP_Acctg_Data_Fix_PKG.Print('Fix script executed on instance '||
       l_instance_name||' host_name '||l_host_name||' on '|| l_date);
  EXCEPTION
  WHEN OTHERS THEN
    AP_Acctg_Data_Fix_PKG.Print('While querying instance name');
    l_message := 'Exception :: '||SQLERRM||'';
    AP_Acctg_Data_Fix_PKG.Print(l_message);                                                   
    
  END;


  ---------------------------------------------------------------------
  -- Step 2: Backup tables
  ---------------------------------------------------------------------
  
  l_debug_info := 'Calling bkp_acctg for backing Accounting Journals for Invoice ';
  
AP_ACCTG_DATA_FIX_PKG.back_up_acctg
(p_bug_number => l_bug_no,
p_driver_table => 'AP_TEMP_DATA_DRIVER_'||l_bug_no,
p_calling_sequence => l_calling_sequence);
   
  -----------------------------------------------------------------------------
  -- Step 3: Correct the transaction Records.
  ---------------------------------------------------------------------
  AP_Acctg_Data_Fix_PKG.Print('_______________________________________'||
                            '_______________________________________');
  l_message :=  '<html><body>Start of main process to correct' ||
               ' the Data Corruption  <p> ';
  AP_Acctg_Data_Fix_PKG.Print(l_message);

  UPDATE ap_header_driver_6992111 ahd
     SET process_flag = 'N'
   WHERE ahd.event_id in (SELECT event_id
         FROM ap_temp_data_driver_6992111
	WHERE process_flag = 'N');

      COMMIT;
      
 SELECT count(*)
    INTO l_count
    FROM ap_temp_data_driver_6992111
   WHERE process_flag = 'Y';

  SELECT count(*)
    INTO l_count1
    FROM ap_header_driver_6992111
   WHERE process_flag = 'Y';

  IF (l_count > 0 OR l_count1 > 0) THEN

    /*****************************************************************************/
    /*   CASE 1: CORRECT EVENT_STATUS_CODE IN XLA_EVENTS
    /*****************************************************************************/
    BEGIN
 
      l_debug_info := 'Updating the Accounting Events from an Incomplete status '||
                      'to an unprocessed status';

      UPDATE XLA_EVENTS XE
         SET XE.event_status_code = 'U',
             XE.process_status_code = 'I'
       WHERE XE.event_id IN
             (SELECT event_id
                FROM ap_temp_data_driver_6992111 tmp
               WHERE tmp.process_flag = 'Y'
             )
         AND XE.process_status_code <> 'P'; /*Bug 14195137*/
  
      l_message := '======== Updated '||SQL%ROWCOUNT || ' records in '||
                   'XLA_EVENTS with event status code U ======== <p>';
      AP_Acctg_Data_Fix_PKG.Print(l_message);
  
      UPDATE ap_temp_data_driver_6992111 dr
         SET process_flag = 'E'
       WHERE process_flag = 'Y'
         AND EXISTS 
             (SELECT 'accounted event'
                FROM xla_events xe		
               WHERE xe.application_id = 200
	         AND xe.event_id = dr.event_id
		 AND process_status_code = 'P'
             );

      l_message := '======== Updated '||SQL%ROWCOUNT || ' records in '||
                   'ap_temp_data_driver_6992111 to E for accounted events ======== <p>';
      AP_Acctg_Data_Fix_PKG.Print(l_message);
      
      UPDATE ap_temp_data_driver_6992111
         SET process_flag = 'D'
       WHERE process_flag = 'Y';

      l_message := '======== Updated '||SQL%ROWCOUNT || ' records in '||
                   'ap_temp_data_driver_6992111 to D ======== <p>';
      AP_Acctg_Data_Fix_PKG.Print(l_message);


  
      COMMIT;

    EXCEPTION
      WHEN OTHERS THEN
        ROLLBACK;
        l_message := 'Encountered an Exception '||SQLERRM||
	             ' at '||l_calling_sequence||' while performing '||l_calling_sequence;
        AP_Acctg_Data_Fix_PKG.Print(l_message);

        UPDATE ap_temp_data_driver_6992111
           SET process_flag = 'E'
         WHERE process_flag NOT IN ('N','E');
  
        l_message := '======== Updated '||SQL%ROWCOUNT || ' records in '||
                     'ap_temp_data_driver_6992111 to E ======== <p>';
        AP_Acctg_Data_Fix_PKG.Print(l_message);

	COMMIT;   
    END;

    /*****************************************************************************/
    /*   CASE 2: CORRECT ACCOUNTING_ENTRY_STATUS_CODE IN XLA_AE_HEADERS
    /*****************************************************************************/
    BEGIN

      l_debug_info := 'Updating the Accounting Events from an Incomplete status '||
                      'to an unprocessed status';
      UPDATE xla_ae_headers XAH
         SET XAH.accounting_entry_status_code = 'I'
       WHERE XAH.application_id = 200
         AND XAH.accounting_entry_status_code = 'F'
	 AND NVL(XAH.gl_transfer_status_code, 'N') <> 'Y' /*Bug 14195137*/
         AND (XAH.ae_header_id, XAH.event_id) IN
              (SELECT ae_header_id, event_id
                 FROM ap_header_driver_6992111 tmp
                WHERE tmp.process_flag = 'Y'
              );
  
      l_message := '======== Updated '||SQL%ROWCOUNT || ' records in '||
                   'XLA_AE_HEADERS with accounting entry status code I ======== <p>';
      AP_Acctg_Data_Fix_PKG.Print(l_message);

      UPDATE ap_header_driver_6992111
         SET process_flag = 'D'
       WHERE process_flag = 'Y';

      l_message := '======== Updated '||SQL%ROWCOUNT || ' records in '||
                   'ap_header_driver_6992111 to D ======== <p>';
      AP_Acctg_Data_Fix_PKG.Print(l_message);

      COMMIT;

    EXCEPTION
      WHEN OTHERS THEN
        ROLLBACK;
        l_message := 'Encountered an Exception '||SQLERRM||
	             ' at '||l_calling_sequence||' while performing '||l_calling_sequence;
        AP_Acctg_Data_Fix_PKG.Print(l_message);

        UPDATE ap_header_driver_6992111
           SET process_flag = 'E'
         WHERE process_flag NOT IN ('N','E');
  
        l_message := '======== Updated '||SQL%ROWCOUNT || ' records in '||
                     'ap_header_driver_6992111 to E ======== <p>';
        AP_Acctg_Data_Fix_PKG.Print(l_message);

	COMMIT;   

    END;

  END IF;

  ---------------------------------------------------------------------
  -- Step 4: Inform user about Backup table names
  ---------------------------------------------------------------------
  AP_Acctg_Data_Fix_PKG.Print('_______________________________________'||
                              '_______________________________________');
  l_message := 'EVENTS_6992111 for affected columns of '||
               'xla_events';
  AP_Acctg_Data_Fix_PKG.Print(l_message);

  l_message := 'HEADERS_6992111 for affected columns of '||
               'xla_ae_headers';
  AP_Acctg_Data_Fix_PKG.Print(l_message);

l_message := 'LINES_6992111 for affected columns of '||
               'xla_ae_headers';
  AP_Acctg_Data_Fix_PKG.Print(l_message);
  
  l_message := 'DISTRIB_LINKS_6992111 for affected columns of '||
               'xla_ae_headers';
  AP_Acctg_Data_Fix_PKG.Print(l_message);
    
  l_message :=  '</body></html>';
  AP_Acctg_Data_Fix_PKG.Print(l_message);
  AP_Acctg_Data_Fix_PKG.Close_Log_Out_Files;
  
    IF (l_email_id IS NOT NULL) THEN
        l_email_flag := AP_Acctg_Data_Fix_PKG.send_mail_clob_impl(l_email_id,l_file_location);
    END IF;
    IF (l_email_flag) THEN
       dbms_output.put_line ('Email Sent');
    ELSE
       dbms_output.put_line ('Email Sending Failed');  
    END IF;

  dbms_output.put_line('--------------------------------------------------'||
                       '-----------------------------');
  dbms_output.put_line(l_file_location||' is the log file created');
  dbms_output.put_line('--------------------------------------------------'||
                        '-----------------------------');
EXCEPTION

  WHEN OTHERS THEN
    l_message := 'Exception :: '||SQLERRM;
    AP_Acctg_Data_Fix_PKG.Print(l_message);
    AP_Acctg_Data_Fix_PKG.Print('</body></html>');
    AP_ACCTG_DATA_FIX_PKG.Close_Log_Out_Files;
    
    IF (l_email_id IS NOT NULL) THEN
        l_email_flag := AP_Acctg_Data_Fix_PKG.send_mail_clob_impl(l_email_id,l_file_location);
    END IF;
    IF (l_email_flag) THEN
       dbms_output.put_line ('Email Sent');
    ELSE
       dbms_output.put_line ('Email Sending Failed');  
    END IF;
    DBMS_OUTPUT.put_line(l_file_location||' is the log file created');

END;
/
COMMIT;
EXIT;
