SET SERVEROUTPUT ON SIZE 1000000;

BEGIN
   DBMS_OUTPUT.put_line ('Before update');

   UPDATE wsh_delivery_details
      SET released_status = 'D',
          src_requested_quantity = 0,
          requested_quantity = 0,
          shipped_quantity = 0,
          cycle_count_quantity = 0,
          cancelled_quantity =
             DECODE (requested_quantity,
                     0, cancelled_quantity,
                     requested_quantity),
          subinventory = NULL,
          serial_number = NULL,
          locator_id = NULL,
          lot_number = NULL,
          revision = NULL,
          inv_interfaced_flag = 'X',
          oe_interfaced_flag = 'X'
    WHERE delivery_detail_id IN (10508934);

   DBMS_OUTPUT.put_line ('Records updated1-' || SQL%ROWCOUNT);

   UPDATE wsh_delivery_assignments
      SET delivery_id = NULL
    WHERE delivery_detail_id IN (10508934);

   DBMS_OUTPUT.put_line ('Records updated2-' || SQL%ROWCOUNT);
   COMMIT;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Unable to update ' || SQLERRM);
END;
/
