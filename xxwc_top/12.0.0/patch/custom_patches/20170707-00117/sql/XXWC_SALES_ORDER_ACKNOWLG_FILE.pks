/* Formatted on 3/5/2013 1:21:55 PM (QP5 v5.206) */
-- Start of DDL Script for Package APPS.XXWC_SALES_ORDER_ACKNOWLG_FILE
-- Generated 3/5/2013 1:21:55 PM from APPS@EBIZDEV

CREATE OR REPLACE PACKAGE apps.xxwc_sales_order_acknowlg_file
IS
 /******************************************************************************************************
   -- File Name: xxwc_pim_chg_item_mgmt.pkb
   --
   -- PROGRAM TYPE: Package Body
   --
   -- PURPOSE:   To Process Change Management data
   -- HISTORY
   -- ================================================================================================================================
   -- ================================================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- ------------------------------------------------------------------------------------------
   -- 1.0     2/21/2013   Rasikha Galimova  TMS#20130218-00861  - Create Single Item Workflow Tool
   -- 2.1     07/11/2017  P.Vamshidhar      TMS#20170707-00117 -  Berg/ Material Management/White Cap System
    ***************************************************************************************************************************************/


    --TMS 
    --Provide Order acknowledgement to 3 new customers using Material Management Inc.
    --by Rasikha Galimova
    --2/21/2013
    --
    PROCEDURE create_file (p_errbuf                       OUT NOCOPY VARCHAR2
                          ,p_retcode                      OUT NOCOPY VARCHAR2
                          ,p_customer_account_number   IN            VARCHAR2
                          ,pv_start_date               IN            VARCHAR2
                          ,pv_end_date                 IN            VARCHAR2
                          ,p_order_number              IN            VARCHAR2
                          ,p_send_file_by_email        IN            VARCHAR2
                          ,p_recipient_email           IN            VARCHAR2
                          ,p_run_from_uc4              IN            VARCHAR2 := 'N');

    PROCEDURE populate_data (p_customer_account_number   IN VARCHAR2
                            ,p_start_date                   DATE
                            ,p_end_date                     DATE
                            ,p_file_name                    VARCHAR2
                            ,p_run_id                       NUMBER);

    PROCEDURE populate_data_order (p_file_name VARCHAR2, p_order_number IN VARCHAR2, p_run_id NUMBER);

    PROCEDURE xxwc_log (p_message VARCHAR2, p_procedure_name VARCHAR2);

    PROCEDURE xxwc_log_debug (p_message VARCHAR2);

    PROCEDURE send_mail_with_attachement (p_from         VARCHAR2
                                         ,p_to           VARCHAR2
                                         ,p_file_name    VARCHAR2
                                         ,p_subject      VARCHAR2);

    PROCEDURE create_file_for_account (p_account_number       IN VARCHAR2
                                      ,p_filedir              IN VARCHAR2
                                      ,p_start_date           IN DATE
                                      ,p_end_date             IN DATE
                                      ,p_run_id               IN NUMBER
                                      ,p_send_file_by_email   IN VARCHAR2
                                      ,p_order_number         IN VARCHAR2);

    FUNCTION valid_email (p_email IN VARCHAR2)
        RETURN NUMBER;

   -- Added below function in Rev 2.1 to call function
   FUNCTION DERIVE_MM_CUSTOMER_ID (P_ACCOUNT_NUMBER   IN VARCHAR2,
                                   P_SHIP_TO_CITY     IN VARCHAR2,
                                   P_SHIP_TO_STATE    IN VARCHAR2)
      RETURN VARCHAR2;
END;                                                                                                     -- Package spec
/

-- End of DDL Script for Package APPS.XXWC_SALES_ORDER_ACKNOWLG_FILE
