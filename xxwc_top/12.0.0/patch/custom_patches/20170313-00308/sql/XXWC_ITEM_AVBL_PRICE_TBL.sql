  /********************************************************************************
  FILE NAME: XXWC_ITEM_AVBL_PRICE_TBL.sql
  
  PROGRAM TYPE: Global table to store item number and warehouse number 
  
  PURPOSE: Global table to store item number and warehouse number 
  
  HISTORY
  ==========================================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- ----------------------------------------------------
  1.0     03/19/2017    Rakesh P.       TMS#20170313-00308 -Validate the user for resp access in order create api, enable debug message
  *******************************************************************************************/
CREATE GLOBAL TEMPORARY TABLE XXWC.XXWC_ITEM_AVBL_PRICE_TBL
(
  sequence             NUMBER,
  organization_code	   VARCHAR2 (3 Byte),
  organization_name	   VARCHAR2 (240 Byte),
  organization_id      NUMBER,
  segment1	           VARCHAR2 (40 Byte),
  inventory_item_id    NUMBER,
  primary_uom_code     VARCHAR2(3 BYTE),
  selling_price        NUMBER  
)
ON COMMIT DELETE ROWS
NOCACHE;

