  /********************************************************************************
  FILE NAME: XXWC_OM_FND_USER_DROP.sql
  
  PROGRAM TYPE: Drop XXWC_OM_FND_USER table
  
  PURPOSE: Drop XXWC_OM_FND_USER table
  
  HISTORY
  ==========================================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- ----------------------------------------------------
  1.0     03/15/2017    Rakesh P.       TMS#20170313-00308 -Validate the user for resp access in order create api, enable debug message
  *******************************************************************************************/
DROP TABLE XXWC.XXWC_OM_FND_USER;        