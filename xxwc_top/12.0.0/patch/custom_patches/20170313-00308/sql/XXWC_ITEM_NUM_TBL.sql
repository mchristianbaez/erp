  /********************************************************************************
  FILE NAME: XXWC_ITEM_NUM_TBL.sql
  
  PROGRAM TYPE: Create table record type to store item number 
  
  PURPOSE: Create table record type to store item number 
  
  HISTORY
  ==========================================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- ----------------------------------------------------
  1.0     03/15/2017    Rakesh P.       TMS#20170313-00308 -Validate the user for resp access in order create api, enable debug message
  *******************************************************************************************/
SET serveroutput ON;

WHENEVER SQLERROR CONTINUE

CREATE OR REPLACE TYPE xxwc.xxwc_item_num_tbl IS TABLE OF VARCHAR2(40);
/
    
CREATE OR REPLACE PUBLIC SYNONYM xxwc_item_num_tbl FOR xxwc.xxwc_item_num_tbl;
/

GRANT ALL ON xxwc.xxwc_item_num_tbl TO INTERFACE_OSO;
/


