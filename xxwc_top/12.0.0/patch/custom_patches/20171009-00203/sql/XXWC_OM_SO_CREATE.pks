CREATE OR REPLACE PACKAGE APPS.XXWC_OM_SO_CREATE IS

 /*************************************************************************
     $Header XXWC_OM_SO_CREATE $
     Module Name: XXWC_OM_SO_CREATE.pks

     PURPOSE:   Handwrite Project	
    
	Input Parameters:  a. p_order_header_rec - p_order_header_rec is a record type 
                                       be used for order header information.
									   
                       b. p_order_lines_tbl - p_order_lines_tbl is a table type       
                                         will be used to store line level information.
                       
	Output Parameters: o_order_out_rec is a record type and will have following column
	                   o_error_code - 'E' - Error or 'S'- Success
                       o_error_msg  - Error message, reason for the failure.
					   
     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        11/09/2016  Rakesh Patel            Handwrite Project	
	 2.0        3/15/2017   Rakesh Patel            Validate the user for resp access in order create api, enable debug message
	 2.2        4/17/2017   Rakesh Patel            TMS#20170417-00208-Mobile Quote Form 0.2 Release - PLSQL api changes  
	 2.3        4/20/2017   Christian Baez          TMS#20170417-00208-Mobile Quote Form 0.2 Release - added get district by ntid function
	 2.4        05/01/2017  Rakesh Patel            TMS#20170501-00147-Mobile pricing app 0.2 UC4 job for XXWC_OM_ORD_LN_MV mv refresh   
	 2.5        05/22/2017  Rakesh Patel            TMS#20170518-00094-Mobile pricing app version 3.0 plsql api changes
	 2.7        06/07/2017  Rakesh P./Christian B.  TMS#20170607-00031-Add access to DMRSMRVPEtc
	 2.9        08/22/2017  Rakesh P./Christian B.  TMS#20170817-00115-CheckIT 1.1.0 - Customer Prospect
	 3.0        08/25/2017  Rakesh P./Christian B.  TMS#20170825-00127-CheckIT 1.1.0 Customer Prospecting Fast Follow
	 4.0        08/28/2017  Rakesh P.               TMS#20170822-00145-Mobile Quote PLSQL api changes
	 4.2        09/29/2017  Rakesh P.               TMS#20170929-00034-Mobile Quote app: Populate the actual (Market Price) UNIT_LIST_PRICE on Quote Lines using API
	 4.3        10/09/2017  Rakesh P.               TMS#20171009-00203-upon successful completion of the quote creation procedure; additionally return the QUOTE_TOTAL                        
**************************************************************************/
    --TMS#20170313-00308 - Validate the user for resp access in order create api, enable debug message start
    g_sqlcode                   NUMBER;   
    g_sqlerrm                   VARCHAR2(2000);
    g_message                   VARCHAR2(2000);
    g_package                   VARCHAR2(200) :=   'XXWC_OM_SO_CREATE';
    g_call_from                 VARCHAR2(200);
    g_distro_list               VARCHAR2 (80) := 'hdsoracledevelopers@hdsupply.com';
    g_module                    VARCHAR2 (80) := 'OM';
    --TMS#20170313-00308 - Validate the user for resp access in order create api, enable debug message end
    
   FUNCTION get_onhand (
       p_inventory_item_id   IN NUMBER
      ,p_organization_id     IN NUMBER
      ,p_base_uom            IN VARCHAR2
      ,p_ord_uom             IN VARCHAR2
      ,p_return_type         IN VARCHAR2 
    ) RETURN NUMBER;

   PROCEDURE CREATE_SALES_ORDER
   (
     p_order_header_rec     IN  xxwc.xxwc_order_header_rec
    ,p_order_lines_tbl      IN  xxwc.xxwc_order_lines_tbl
    ,o_status               OUT VARCHAR2
    ,o_order_number         OUT NUMBER
    ,o_message              OUT VARCHAR2    
	,p_debug_mode           IN  VARCHAR2  DEFAULT NULL   --E/T/NULL --TMS#20170313-00308 - Validate the user for resp access in order create api, enable debug message start
   );
   
   /*************************************************************************
     PROCEDURE GET_CUSTOMER_INFO
    
	Input Parameters:  a. p_cust_search_rec - customer search data.
									   
                       b. o_cust_data_tbl_out_rec - customer data
     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        11/09/2016  Rakesh Patel            Handwrite Project	
     2.2        4/17/2017   Rakesh Patel            TMS#20170417-00208-Mobile Quote Form 0.2 Release - PLSQL api changes  
     2.5        05/22/2017  Rakesh Patel            TMS#20170518-00094-Mobile pricing app version 3.0 plsql api changes
**************************************************************************/
   
   FUNCTION GET_CUSTOMER_INFO_RS(p_cust_account_code IN VARCHAR2
                                ,p_customer_name     IN VARCHAR2
                                ,p_user_nt_id        IN VARCHAR2  DEFAULT NULL  --Added for Rev 2.2
                                ,p_filter            IN VARCHAR2  DEFAULT NULL  --Added for Rev 2.5
                                ) return sys_refcursor;
   
   FUNCTION GET_BRANCH_INFO ( p_user_nt_id IN VARCHAR2 ) return sys_refcursor;
   
   FUNCTION GET_BRANCH_INFO(  p_user_nt_id IN VARCHAR2
                             ,p_email_id IN VARCHAR2 ) return sys_refcursor;
							 
   FUNCTION GET_BRANCH_DETAILS(  p_organization_id IN VARCHAR2 DEFAULT NULL ) return sys_refcursor;      
   
   FUNCTION GET_CUSTOMER_CONTACTS(p_cust_account_id IN NUMBER) return sys_refcursor;
   
   FUNCTION GET_AUTH_BUYER(p_cust_account_id IN NUMBER) return sys_refcursor;
   
   PROCEDURE GET_PRODUCT_INFO( p_branch_id   IN NUMBER
                             ,p_customer_id IN NUMBER
                             ,p_jobsiteid   IN NUMBER
                             ,p_item_search IN VARCHAR2
                             ,p_filter      IN VARCHAR2
                             ,p_Sort        IN VARCHAR2     
                             ,p_warhouse_num_tbl IN  xxwc.xxwc_warhouse_num_tbl DEFAULT NULL --Rev 2.2
                             ,p_filter_stock IN VARCHAR2 DEFAULT NULL --Rev 2.5
                             ,o_cur_output  OUT SYS_REFCURSOR
                             );
   
   PROCEDURE GET_PRODUCT_INFO( p_branch_id   IN NUMBER
                             ,p_customer_id IN NUMBER
                             ,p_jobsiteid   IN NUMBER
                             ,p_item_search IN VARCHAR2
                             ,p_filter      IN VARCHAR2
                             ,p_Sort        IN VARCHAR2    
                             );                                                  

   PROCEDURE line_pricing_data ( p_organization_id NUMBER
                              , p_cust_account_id NUMBER
                              , p_site_use_id NUMBER
                             );
                             
   FUNCTION get_list_price(   i_organization_id   in number
                             , i_inventory_item_id in NUMBER
                             ) return NUMBER;

   FUNCTION GET_ORDER_INFO(  p_order_number     IN NUMBER
                            ,p_order_type_id    IN NUMBER   
                            ,p_created_by       IN NUMBER  
                            ,p_customer_number  IN VARCHAR2
                            ,p_order_from_date  IN VARCHAR2
                            ,p_order_to_date    IN VARCHAR2  
                            ,p_Warehouse_id     IN NUMBER
                            ,p_status           IN VARCHAR2
                           ) return sys_refcursor;
  
   --Rev 2.2 < Start
   PROCEDURE GET_PRODUCT_INFO_AIS( p_branch_id   IN NUMBER
                                  ,p_customer_id IN NUMBER
                                  ,p_jobsiteid   IN NUMBER
                                  ,p_item_search IN VARCHAR2
                                  ,p_filter      IN VARCHAR2
                                  ,p_Sort        IN VARCHAR2
                                  ,p_call_pr_api IN VARCHAR2 DEFAULT NULL 
                                 );
                                 
  PROCEDURE line_pricing_data ( p_cust_account_id NUMBER
                              , p_site_use_id NUMBER
                             );

  PROCEDURE refresh_xxwc_om_ord_ln_mv (errbuf  OUT VARCHAR2,
                                       retcode OUT VARCHAR2);
                              
  PROCEDURE GET_CUSTOMER_PURCH_INFO( p_customer_id  IN NUMBER
                                  , p_jobsiteid     IN NUMBER
                                  , p_item_num_tbl  IN  xxwc.xxwc_item_num_tbl  --Rev 2.4
                                  , o_cur_output    OUT SYS_REFCURSOR -- return sys_refcursor
                                  );
  
 --Rev 2.2 > End                               
  
  --Rev 2.4 < Start
  PROCEDURE GET_CUSTOMER_PURCH_PRODUCTS ( p_customer_id  IN NUMBER
                                  , p_jobsiteid     IN NUMBER
                                  , p_item_search   IN VARCHAR2 
                                  , o_cur_output    OUT SYS_REFCURSOR -- return sys_refcursor
                                  );
  --Rev 2.4 > End 
  
  FUNCTION GET_LINE_INFO(  p_header_id     IN NUMBER
                           ) return sys_refcursor;
                                                    
  PROCEDURE CALCUALTE_TAX
    (
     p_order_header_rec     IN  OE_Order_PUB.Header_Rec_Type
    ,p_order_line_rec       IN  OE_Order_PUB.Line_Rec_Type
    ,o_x_tax_value          OUT NUMBER 
   );
   
   PROCEDURE create_contact (p_customer_id       IN     NUMBER,
                             p_first_name        IN     VARCHAR2,
                             p_last_name         IN     VARCHAR2,
                             p_contact_num       IN     VARCHAR2,
                             p_email_id          IN     VARCHAR2,
                             p_fax_num           IN     VARCHAR2,
                             p_cust_contact_id      OUT NUMBER,
                             p_error_message        OUT VARCHAR2);
   
   PROCEDURE CREATE_JOB_SITE
   (
     p_xxwc_address_rec     IN  xxwc.xxwc_address_rec
    ,o_status               OUT VARCHAR2
    ,o_ship_to_site_use_id      OUT NUMBER
    ,o_bill_to_site_use_id      OUT NUMBER
    ,o_message              OUT VARCHAR2    
   );
             
    PROCEDURE CREATE_ITEM (
      p_Parts_On_Fly_Rec   IN OUT NOCOPY xxwc_Parts_On_Fly_Rec,
      x_Return_Status         OUT NOCOPY VARCHAR2,
      x_Msg_Data              OUT NOCOPY VARCHAR2,
      x_Msg_Count             OUT NOCOPY NUMBER,
      x_part_number           OUT NOCOPY VARCHAR2,
      x_inventory_item_id     OUT NOCOPY NUMBER,
      x_list_price            OUT NOCOPY NUMBER,
      x_primary_uom_code      OUT NOCOPY VARCHAR2
    );                                                  
    
    FUNCTION get_sales_rep_id (p_cust_account_id IN NUMBER) RETURN NUMBER;
    
    FUNCTION get_payment_term_id (p_cust_account_id IN NUMBER) RETURN NUMBER;
    
    FUNCTION get_payment_term_nm (p_cust_account_id IN NUMBER) RETURN VARCHAR2;
    
    FUNCTION payment_term (p_term_id IN NUMBER)  RETURN VARCHAR2;
    
    --TMS#20170313-00308 - Validate the user for resp access in order create api, enable debug message
    PROCEDURE get_item_avbl_price
     (
       p_item_num_tbl     IN  xxwc.xxwc_item_num_tbl
      ,p_warhouse_num_tbl IN  xxwc.xxwc_warhouse_num_tbl
      ,p_cust_account_id  IN  NUMBER
      ,p_site_use_id      IN  NUMBER
      ,o_cur_output      OUT SYS_REFCURSOR
    );
    
    --get all the branches in a given district by ntid
    FUNCTION GET_DISTRICT_INFO_BY_NTID(p_sr_ntid IN VARCHAR2)return sys_refcursor; 
    
    --Rev 2.5 < Start
    FUNCTION IS_COMMODITY_ITEMS (p_inventory_item_id IN NUMBER) RETURN VARCHAR2;
    
                                    
    FUNCTION GET_CUSTOMER_INFO_ACTIVE ( p_cust_account_code IN VARCHAR2
                                    ,p_customer_name     IN VARCHAR2
                                    ,p_user_nt_id        IN VARCHAR2
                                   ) return sys_refcursor;   
    --Rev 2.5 > End
    
    --Rev 2.7 < Start
    FUNCTION GET_BRANCH_INFO_BY_BR_CODE(p_br_code IN VARCHAR2)return sys_refcursor;--return sys_refcursor
    
    FUNCTION GET_EEID_NUMBER( NTID_IN VARCHAR2) RETURN NUMBER;
    --Rev 2.7 > End
	
    --Rev 2.9 < Start
    FUNCTION GET_CUSTOMER_PROSPECT_INFO ( p_customer_name  IN VARCHAR2) return sys_refcursor;
	FUNCTION GET_SALESREP_PHONE (p_cust_account_id   IN NUMBER) RETURN VARCHAR2;
    --Rev 2.9 > End
    
    --Rev 3.0 < Start
    FUNCTION GET_SALES_REP_STATES_ACTIVE (p_customer_name IN VARCHAR2) RETURN VARCHAR2;

    FUNCTION get_master_acct_sales_rep (p_cust_account_id   IN NUMBER) RETURN VARCHAR2;
    --Rev 3.0 > End
	
	--Rev 4.0 < Start
	PROCEDURE CREATE_QUOTE
    (
     p_order_header_rec     IN  xxwc.xxwc_order_header_rec
    ,p_order_lines_tbl      IN  xxwc.xxwc_order_lines_tbl
    ,o_status               OUT VARCHAR2
    ,o_quote_number         OUT NUMBER
    ,o_quote_total          OUT NUMBER  -- Rev 4.3
    ,o_message              OUT VARCHAR2    
	,p_debug_mode           IN  VARCHAR2  DEFAULT NULL   --E/T/NULL 
    );
   
	FUNCTION GET_QUOTE_INFO(  p_ntid         IN VARCHAR2
                            ,p_quote_number     IN NUMBER DEFAULT NULL
                          ) return sys_refcursor;
   
   FUNCTION GET_QUOTE_LINE_INFO(  p_header_id     IN NUMBER
                           ) return sys_refcursor; 
						   
	FUNCTION GET_DOC_HEADER  (p_header_id   IN NUMBER) return VARCHAR2;
    
    FUNCTION GET_UN_NUMBER ( p_inventory_item_id   IN NUMBER
                        ,p_ship_from_org_id   IN NUMBER) return VARCHAR2;
                        
    FUNCTION GET_LUMBER_TAX_RATE RETURN NUMBER;                    
    
    FUNCTION GET_UNITWGT_TOTAL (p_header_id IN NUMBER) RETURN NUMBER;
    
    FUNCTION  get_job_site_contact (p_ship_to_contact_id NUMBER ) RETURN VARCHAR2; --Rev 4.2
    
    FUNCTION Get_Sold_to_contact_phone(p_sold_to_contact_id NUMBER) return VARCHAR2;
    
    FUNCTION get_auth_buyer(p_auth_buyer_flag IN VARCHAR2, p_auth_buyer_id IN NUMBER) RETURN VARCHAR2; 
    --Rev 4.0 > End
	
END XXWC_OM_SO_CREATE;
/