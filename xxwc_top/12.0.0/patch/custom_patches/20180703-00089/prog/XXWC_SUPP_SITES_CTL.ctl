--*****************************************************************
--*File Name: XXWC_SUPP_SITES_CTL.ctl                           *
--*Author: Thiruvengadam Rajaiah.N                                *
--*Creation Date: 15-Sep-2011                                     *
--*Last Updated Date: 21-May-2018 - Vamshi                        *
--*Data Source:                                                   *
--*Description: This is the control file. SQL Loader will         *
--*           use to load the Suppliers  information.             *
--* TMS#20180703-00089 - AH Harries Suppliers Conversion          * 
--****************************************************************
OPTIONS (SKIP=0)
LOAD DATA
APPEND INTO TABLE XXWC_AP_SUPPLIER_SITES_INT
FIELDS TERMINATED BY '|' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(   VENDOR_NUMBER                    "TRIM(:VENDOR_NUMBER)"
 , ORG_ID                           "TRIM(:ORG_ID)"
 , PURCHASING_SITE_FLAG             "SUBSTR(TRIM(:PURCHASING_SITE_FLAG),1,1)"
 , PAY_SITE_FLAG                    "SUBSTR(TRIM(:PAY_SITE_FLAG),1,1)"
 , VENDOR_SITE_CODE                 "TRIM(:VENDOR_SITE_CODE)"
 , ADDRESS_LINE1                    "TRIM(:ADDRESS_LINE1)"
 , ADDRESS_LINE2                    "TRIM(:ADDRESS_LINE2)"
 , ADDRESS_LINE3                    "TRIM(:ADDRESS_LINE3)"
 , CITY                             "TRIM(:CITY)"
 , STATE                            "TRIM(:STATE)"
 , ZIP                              "TRIM(:ZIP)"
 , COUNTY                           "TRIM(:COUNTY)"
 , COUNTRY                          "TRIM(:COUNTRY)"
 , PHONE                            "TRIM(:PHONE)"
 , FAX                              "TRIM(:FAX)" 
 , ACCTS_PAY_CODE_COMBINATION_ID    "TRIM(:ACCTS_PAY_CODE_COMBINATION_ID)"
 , PREPAY_CODE_COMBINATION_ID       "TRIM(:PREPAY_CODE_COMBINATION_ID)"
 , PAYMENT_METHOD_LOOKUP_CODE       "TRIM(:PAYMENT_METHOD_LOOKUP_CODE)"
 , EXCLUSIVE_PAYMENT_FLAG           "TRIM(:EXCLUSIVE_PAYMENT_FLAG)"
 , TERMS_ID                         "TRIM(:TERMS_ID)" 
 , PAY_GROUP_LOOKUP_CODE            "TRIM(:PAY_GROUP_LOOKUP_CODE)"
 , MATCH_OPTION                     "TRIM(:MATCH_OPTION)"
 , DISTRIBUTION_SET_NAME            "TRIM(:DISTRIBUTION_SET_NAME)"
 , HOLD_UNMATCHED_INVOICES_FLAG     "TRIM(:HOLD_UNMATCHED_INVOICES_FLAG)"
 , TAX_REPORTING_SITE_FLAG          "TRIM(:TAX_REPORTING_SITE_FLAG)"
 , STATUS                           "TRIM(:STATUS)"
 , REJECT_CODE                      "TRIM(:REJECT_CODE)")