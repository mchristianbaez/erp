--*****************************************************************
--*File Name: XXWC_AP_SUPPLIER_INT.ctl                            *
--*Author: Thiruvengadam Rajaiah.N                                *
--*Creation Date: 15-Sep-2011                                     *
--*Last Updated Date: 21-May-2018 - Vamshi                        *
--*Data Source: AHH                                               *
--*Description: This is the control file. SQL Loader will         *
--*           use to load the Suppliers  information.             *
--* TMS#20180703-00089 - AH Harries Suppliers Conversion          *
--*                                Date format changed to MM/DD/RR*
--*****************************************************************
OPTIONS (SKIP=0)
LOAD DATA
REPLACE INTO TABLE XXWC_AP_SUPPLIER_INT
FIELDS TERMINATED BY '|' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(  VENDOR_NAME                      "REGEXP_REPLACE(LTRIM(RTRIM(:VENDOR_NAME)), '([^[:print:]])','')"
 , ATTRIBUTE1                       "REGEXP_REPLACE(LTRIM(RTRIM(:ATTRIBUTE1)), '([^[:print:]])','')"
 , VENDOR_NAME_ALT                  "REGEXP_REPLACE(LTRIM(RTRIM(:VENDOR_NAME_ALT)), '([^[:print:]])','')"
 , NUM_1099                         "REGEXP_REPLACE(LTRIM(RTRIM(:NUM_1099)), '([^[:print:]])','')"
 , TYPE_1099                        "REGEXP_REPLACE(LTRIM(RTRIM(:TYPE_1099)), '([^[:print:]])','')"
 , FEDERAL_REPORTABLE_FLAG          "REGEXP_REPLACE(LTRIM(RTRIM(:FEDERAL_REPORTABLE_FLAG)), '([^[:print:]])','')"
 , PAYMENT_PRIORITY                 "REGEXP_REPLACE(LTRIM(RTRIM(:PAYMENT_PRIORITY)), '([^[:print:]])','')"
 , VENDOR_TYPE_LOOKUP_CODE          "REGEXP_REPLACE(LTRIM(RTRIM(:VENDOR_TYPE_LOOKUP_CODE)), '([^[:print:]])','')"
 , DAYS_EARLY_RECEIPT_ALLOWED       "REGEXP_REPLACE(LTRIM(RTRIM(:DAYS_EARLY_RECEIPT_ALLOWED)), '([^[:print:]])','')"
 , DAYS_LATE_RECEIPT_ALLOWED        "REGEXP_REPLACE(LTRIM(RTRIM(:DAYS_LATE_RECEIPT_ALLOWED)), '([^[:print:]])','')"
 , RECEIPT_DAYS_EXCEPTION_CODE      "REGEXP_REPLACE(LTRIM(RTRIM(:RECEIPT_DAYS_EXCEPTION_CODE)), '([^[:print:]])','')"
 , SEGMENT1                         "REGEXP_REPLACE(LTRIM(RTRIM(:SEGMENT1)), '([^[:print:]])','')"
 , INVOICE_CURRENCY_CODE            "REGEXP_REPLACE(LTRIM(RTRIM(:INVOICE_CURRENCY_CODE)), '([^[:print:]])','')"
 , PAYMENT_CURRENCY_CODE            "REGEXP_REPLACE(LTRIM(RTRIM(:PAYMENT_CURRENCY_CODE)), '([^[:print:]])','')"
 , SHIP_TO_LOCATION_CODE            "REGEXP_REPLACE(LTRIM(RTRIM(:SHIP_TO_LOCATION_CODE)), '([^[:print:]])','')"
 , BILL_TO_LOCATION_CODE            "REGEXP_REPLACE(LTRIM(RTRIM(:BILL_TO_LOCATION_CODE)), '([^[:print:]])','')"
 , ENABLED_FLAG                     "REGEXP_REPLACE(LTRIM(RTRIM(:ENABLED_FLAG)), '([^[:print:]])','')"
 , EMPLOYEE_ID                      "REGEXP_REPLACE(LTRIM(RTRIM(:EMPLOYEE_ID)), '([^[:print:]])','')"
 , PAY_GROUP_LOOKUP_CODE            "REGEXP_REPLACE(LTRIM(RTRIM(:PAY_GROUP_LOOKUP_CODE)), '([^[:print:]])','')"
 , ATTRIBUTE2                       "REGEXP_REPLACE(LTRIM(RTRIM(:ATTRIBUTE2)), '([^[:print:]])','')"
 , ATTRIBUTE3                       "REGEXP_REPLACE(LTRIM(RTRIM(:ATTRIBUTE3)), '([^[:print:]])','')"
 , ATTRIBUTE4                       "REGEXP_REPLACE(LTRIM(RTRIM(:ATTRIBUTE4)), '([^[:print:]])','')"
 , ATTRIBUTE5                       "REGEXP_REPLACE(LTRIM(RTRIM(:ATTRIBUTE5)), '([^[:print:]])','')"
 , ATTRIBUTE6                       "REGEXP_REPLACE(LTRIM(RTRIM(:ATTRIBUTE6)), '([^[:print:]])','')"
 , ATTRIBUTE7                       "REGEXP_REPLACE(LTRIM(RTRIM(:ATTRIBUTE7)), '([^[:print:]])','')"
 , ATTRIBUTE8                       "REGEXP_REPLACE(LTRIM(RTRIM(:ATTRIBUTE8)), '([^[:print:]])','')"
 , ATTRIBUTE9                       "REGEXP_REPLACE(LTRIM(RTRIM(:ATTRIBUTE9)), '([^[:print:]])','')"
 , ATTRIBUTE10                      "REGEXP_REPLACE(LTRIM(RTRIM(:ATTRIBUTE10)), '([^[:print:]])','')"
 , ATTRIBUTE11                      "REGEXP_REPLACE(LTRIM(RTRIM(:ATTRIBUTE11)), '([^[:print:]])','')"
 , ATTRIBUTE12                      "REGEXP_REPLACE(LTRIM(RTRIM(:ATTRIBUTE12)), '([^[:print:]])','')"
 , ATTRIBUTE13                      "REGEXP_REPLACE(LTRIM(RTRIM(:ATTRIBUTE13)), '([^[:print:]])','')"
 , ATTRIBUTE14                      "REGEXP_REPLACE(LTRIM(RTRIM(:ATTRIBUTE14)), '([^[:print:]])','')"
 , STATUS                           "REGEXP_REPLACE(LTRIM(RTRIM(:STATUS)), '([^[:print:]])','')"
 , START_DATE_ACTIVE                "TO_DATE(REGEXP_REPLACE(LTRIM(RTRIM(:START_DATE_ACTIVE)), '([^[:print:]])',''),'MM/DD/YY')"
 , END_DATE_ACTIVE                 "TO_DATE(REGEXP_REPLACE(LTRIM(RTRIM(:END_DATE_ACTIVE)), '([^[:print:]])',''),'MM/DD/YY')"
 , SET_OF_BOOKS_ID                  "REGEXP_REPLACE(LTRIM(RTRIM(:SET_OF_BOOKS_ID)), '([^[:print:]])','')"
 , TERMS_NAME                       "REGEXP_REPLACE(LTRIM(RTRIM(:TERMS_NAME)), '([^[:print:]])','')"
 , TAX_TYPE                         "REGEXP_REPLACE(LTRIM(RTRIM(:TAX_TYPE)), '([^[:print:]])','')"
 , ATTRIBUTE15                      "REGEXP_REPLACE(LTRIM(RTRIM(:ATTRIBUTE15)), '([^[:print:]])','')"
 , REJECT_CODE                      "REGEXP_REPLACE(LTRIM(RTRIM(:REJECT_CODE)), '([^[:print:]])','')")
