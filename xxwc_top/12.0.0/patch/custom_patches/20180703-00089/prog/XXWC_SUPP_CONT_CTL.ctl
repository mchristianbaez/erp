--*********************************************************************
--*File Name: XXWC_SUPP_CONT_CTL.ctl                                  *
--*Author: Thiruvengadam Rajaiah.N                                    *
--*Creation Date: 15-Sep-2011                                         *
--*Last Updated Date: 21-May-2018 - Vamshi                            *
--*Data Source:                                                       *
--*Description: This is the control file. SQL Loader will             *
--*           use to load the Suppliers Site Contacts  information.   *
--* TMS#20180703-00089 - AH Harries Suppliers Conversion              *
--*                                                                   *
--*********************************************************************
OPTIONS (SKIP=0)
LOAD DATA
REPLACE INTO TABLE XXWC_AP_SUP_SITE_CONTACT_INT
FIELDS TERMINATED BY '|' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
( VENDOR_NUMBER                    "TRIM(:VENDOR_NUMBER)"
 , VENDOR_SITE_CODE                 "TRIM(:VENDOR_SITE_CODE)"
 , FIRST_NAME                       "TRIM(:FIRST_NAME)"
 , LAST_NAME                        "TRIM(:LAST_NAME)"
 , AREA_CODE                        "TRIM(:AREA_CODE)"
 , PHONE                            "TRIM(:PHONE)"
 , FAX                              "TRIM(:FAX)"
 , EMAIL_ADDRESS                    "TRIM(:EMAIL_ADDRESS)"
 , STATUS                           "TRIM(:STATUS)"
 , REJECT_CODE                      "TRIM(:REJECT_CODE)")