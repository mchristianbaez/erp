CREATE OR REPLACE PACKAGE APPS.XXWC_AP_SUPPLIERS_CONV_PKG
AS
   /*****************************************************************************************************************
*    script name: XXWC_AP_SUPPLIERS_CONV_PKG.pks
*
*    interface / conversion name: Suppliers conversion.
      *
      *    functional purpose: convert suppliers/banks using Open interface
                               and Bank API's
      *
      *    history:
      *
      *    version    date              author             description
      ***************************************************************************************************************
      *    1.0        04-sep-2011   T.Rajaiah     initial development.
      *    1.2        23-May-2018   P.Vamshidhar  TMS#20180703-00089 - AH Harries Suppliers Conversion
      *                                           Added procedures to execute CVR and creating GL code combinations
      **************************************************************************************************************/
   l_verify_flag               VARCHAR2 (1);
   l_error_message             VARCHAR2 (240);
   g_federal_reportable_flag   VARCHAR2 (1);
   g_type_1099                 VARCHAR2 (15);
   g_vendor_site_code          VARCHAR2 (15);
   g_validate_only             VARCHAR2 (1);
   g_submit                    VARCHAR2 (1);
   v_add                       NUMBER (15) := 0;
   g_import_only               VARCHAR2 (1);
   g_vendor_type               VARCHAR2 (40);
   g_terms_name                VARCHAR2 (50);
   g_term_id                   NUMBER (15);
   g_vendor_id                 NUMBER;
   g_existing                  VARCHAR2 (3);

   PROCEDURE xxwc_ap_supplier_valid (p_vendor_name    VARCHAR2,
                                     p_type_1099      VARCHAR2,
                                     p_vendor_type    VARCHAR2,
                                     p_terms_name     VARCHAR2,
                                     p_pay_group      VARCHAR2);

   PROCEDURE xxwc_ap_supp_sites_valid (p_site_code     VARCHAR2,
                                       p_country       VARCHAR2,
                                       p_state         VARCHAR2,
                                       p_terms_name    VARCHAR2);

   PROCEDURE xxwc_supp_import_stand;

   PROCEDURE xxwc_stage_to_interface;

   PROCEDURE main (errbuf               OUT VARCHAR2,
                   retcode              OUT NUMBER,
                   p_validate_only   IN     VARCHAR2,
                   p_submit          IN     VARCHAR2);

   PROCEDURE xxwc_supp_bank_branch;

   PROCEDURE xxwc_supp_acct_assignment;

   PROCEDURE xxwc_main (errbuf OUT VARCHAR2, retcode OUT VARCHAR2);

   FUNCTION derive_ccid (p_concat_segs IN VARCHAR2, x_ret_msg OUT VARCHAR2)
      RETURN NUMBER;

End Xxwc_Ap_Suppliers_Conv_Pkg;
/