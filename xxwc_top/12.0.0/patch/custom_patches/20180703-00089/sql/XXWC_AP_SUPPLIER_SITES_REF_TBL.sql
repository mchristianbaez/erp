 /********************************************************************************
  FILE NAME: XXWC.XXWC_AP_SUPPLIER_SITES_REF_TBL.sql

  PROGRAM TYPE: Table script

  PURPOSE: Conversion purpose for AH Harries

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)          DESCRIPTION
  ------- -----------   ---------------    --------------------------------------
  1.0     05/016/2018   Pattabhi Avula     TMS#20180308-00301   -- Initial Version
  ********************************************************************************/  
  CREATE TABLE XXWC.XXWC_AP_SUPPLIER_SITES_REF_TBL 
   (AHH_SUPPLIER_NUMBER 	VARCHAR2(50), 
	ORACLE_VENDOR_SITE_ID 	NUMBER
   );
/