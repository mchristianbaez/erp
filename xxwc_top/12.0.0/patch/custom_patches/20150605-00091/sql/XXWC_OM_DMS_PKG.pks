CREATE OR REPLACE PACKAGE apps.xxwc_om_dms_pkg IS

  /********************************************************************************
  -- File Name: XXWC_OM_MYLOGISTICSS_PKG
  --
  -- PROGRAM TYPE: PL/SQL Script   <API>
  --
  -- PURPOSE: Package for 
  --
  -- HISTORY
  -- ===========================================================================
  -- ===========================================================================
  -- VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- -------------------------------------
  -- 1.0     20-May-2014   Kathy Poling    Created this package.  TMS 20140606-00082
  -- 1.2     20-Nov-2014   Kathy Poling    Added procedure ship_confirm_sig_capt and
                                           function line_status.
  -- 1.3     31-Mar-2015   Gopi Damuluri   TMS 20150227-00035 
  --                                       Added new procedure - UPDATE_OFD_DELIVERY_RUN
  -- 1.4     04-Dec-2015   Pattabhi Avula  TMS 20150605-00091 -- Added p_order_type_id
                                           for ship_confirm procedure
  *******************************************************************************/
  PROCEDURE gen_file(p_errbuf      OUT VARCHAR2
                    ,p_retcode     OUT VARCHAR2
                    ,p_order_type  IN VARCHAR2
                    ,p_dummy       IN VARCHAR2
                    ,p_header_id   IN NUMBER
                    ,p_delivery_id IN NUMBER);

  PROCEDURE ship_confirm(p_errbuf              OUT VARCHAR2
                        ,p_retcode             OUT VARCHAR2
                        ,p_user_name           IN VARCHAR2
                        ,p_responsibility_name IN VARCHAR2
						,p_order_type_id       IN NUMBER  -- Added for Vern 1.4
						,p_run_zone            IN VARCHAR2);  -- Added for Vern 1.4

  FUNCTION dms_org(p_ship_from_org_id IN NUMBER) RETURN VARCHAR2;

  PROCEDURE uc4_submit(p_errbuf         OUT VARCHAR2
                      ,p_retcode        OUT VARCHAR2
                      ,p_conc_prog_name IN VARCHAR2
                      ,p_user_name      IN VARCHAR2
                      ,p_resp_name      IN VARCHAR2
					  ,p_order_type_id  IN NUMBER  -- Added for Vern 1.4
					  ,p_run_zone       IN VARCHAR2);  -- Added for Vern 1.4);

  PROCEDURE email_errors(p_header_id   IN NUMBER
                        ,p_delivery_id IN NUMBER
                        ,p_error_msg   VARCHAR2);

  PROCEDURE ship_confirm_sig_capt(p_errbuf              OUT VARCHAR2
                                 ,p_retcode             OUT VARCHAR2
                                 ,p_user_name           IN VARCHAR2
                                 ,p_responsibility_name IN VARCHAR2
                                 ,p_run_zone            IN VARCHAR2
                                 ,p_date                IN VARCHAR2);
  FUNCTION line_status(p_header_id IN NUMBER, p_delivery_id IN NUMBER)
    RETURN NUMBER;

-- Version# 1.3 > Start
  PROCEDURE update_ofd_delivery_run (p_header_id NUMBER);
-- Version# 1.3 < End

END xxwc_om_dms_pkg;
/