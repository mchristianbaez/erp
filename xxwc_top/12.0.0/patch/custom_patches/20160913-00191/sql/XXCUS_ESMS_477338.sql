/*
 -- Author: Balaguru Seshadri
 -- Parameters: None
 -- Modification History
 -- ESMS                TMS                          Date                    Version     Comments
 -- =========== ===============  ===========    =======    ========================
 -- 477338          20160913-00191  13-SEP-2016     1.0            Created.
 */ 
SET SERVEROUTPUT ON SIZE 1000000
declare
  --
  --
 procedure print_log(p_message in varchar2) is
 begin
      dbms_output.put_line(p_message);
 end print_log;
 --         
begin
  --
  print_log('Start: Main');
  --
  begin 
     --
    savepoint square1;
    --
        delete /*+ parallel (a 8) */ from xxcus.xxcus_rebate_recpt_history_tbl a
        where 1 =1
        and a.status_flag = 'P'
        and a.fiscal_per_id <=201403;
    --
    print_log('Deleted records from table xxcus_rebate_recpt_history_tbl with status flag "P" and fiscal period on or before 201407, record count ='||sql%rowcount);
    --
    commit;
    --      
  exception
   when others then
    print_log('@Failed to delete record from table xxcus_rebate_recpt_history_tbl with status flag "P" and fiscal period on or before 201407, msg ='||sqlerrm);
    rollback to square1;
  end;   
  -- 
 print_log('End: Main');
 -- 
exception
 when others then
  print_log('Outer block of SQL script, message ='||sqlerrm);
end;
/