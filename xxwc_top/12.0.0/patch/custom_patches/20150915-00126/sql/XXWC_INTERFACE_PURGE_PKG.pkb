CREATE OR REPLACE PACKAGE BODY APPS.xxwc_interface_purge_pkg
AS
   g_err_callfrom   VARCHAR2 (75) DEFAULT 'XXWC_INTERFACE_PURGE_PKG';
   g_distro_list    VARCHAR2 (75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';

   /********************************************************************************************************************************
   -- Package XXWC_INTERFACE_PURGE_PKG
   -- ******************************************************************************************************************************
   --
   -- PURPOSE: Package is to purge processed records in tables listed
   --          in 'XXWC_INTF_PURGE_TABLES' value set.
   -- HISTORY
   -- ==============================================================================================================================
   -- ==============================================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- ----------------------------------------------------------------------------------------
   -- 1.0     05-Feb-2015   P.Vamshidhar    Created this package.   TMS#20141120-00077
                                            White Cap C&I Concurrent Program for
                                            Purging processed records from SCM interface tables.

                                           Table Name                     Column Name       Column values
                                           MTL_SYSTEM_ITEMS_INTERFACE     PROCESS_FLAG         7
                                           MTL_ITEM_REVISIONS_INTERFACE   PROCESS_FLAG         7
                                           EGO_ITM_USR_ATTR_INTRFC        PROCESS_STATUS       4
                                           EGO_BULKLOAD_INTF              PROCESS_STATUS       7, 99
                                           XXWC_PO_LINE_ADDTIONAL_ATTR_T  Creation_date<=sysdate-760  --> added in 1.2 Rev.

   -- 1.1     03-Mar-2015   P.Vamshidhar   Removed validation to archive table while deleting
                                           from interface table and commented l_sec individual and initialized to
                                           'ALL' when it is all -- -  TMS#20150303-00158

   -- 1.2     16-Oct-2015   P.Vamshidhar    TMS#20150915-00126
                                            Added new Procedure to Purge from custom table
                                            XXWC.XXWC_PO_LINE_ADDTIONAL_ATTR_T
   ************************************************************************************************************************************/



   PROCEDURE Int_Data_cleanup (p_errbuf             OUT VARCHAR2,
                               p_retcode            OUT VARCHAR2,
                               p_tab_selection   IN     VARCHAR2)
   IS
      /********************************************************************************************************************************
      -- PROCEDURE: Int_Data_cleanup
      --
      -- PURPOSE: Procedure is main procedure to purge processed records in SCM interface tables listed
      --          in 'XXWC_INTF_PURGE_TABLES' value set.  Program will be called from
      --          'XXWC SCM Purging Interface Tables Data' Program.
      -- HISTORY
      -- =============================================================================================================================
      -- =============================================================================================================================
      -- VERSION DATE          AUTHOR(S)       DESCRIPTION
      -- ------- -----------   --------------- ---------------------------------------------------------------------------------------
      -- 1.0     05-Feb-2015   P.Vamshidhar    Created this Procedure. - TMS#20141120-00077
      -- 1.1     03-Mar-2015   P.Vamshidhar    Commented l_sec individual and initialized to
                                               'ALL' when it is all.   - TMS#20150303-00158
      -- 1.2     16-Oct-2015   P.Vamshidhar    TMS#20150915-00126
                                               Added new Procedure to Purge from custom table
                                               XXWC.XXWC_PO_LINE_ADDTIONAL_ATTR_T

      *******************************************************************************************************************************/

      l_err_msg            CLOB;
      l_sec                VARCHAR2 (110) DEFAULT 'START';
      l_procedure          VARCHAR2 (50) := 'INT_DATA_CLEANUP';
      l_application_name   VARCHAR2 (30) := 'XXWC';

      l_item_errbuf        VARCHAR2 (2000);
      l_rev_errbuf         VARCHAR2 (2000);
      l_attr_errbuf        VARCHAR2 (2000);
      l_bulk_errbuf        VARCHAR2 (2000);
      l_po_line_errbuf     VARCHAR2 (2000);      -- Added by Vamshi in Rev 1.2
      l_item_retcode       VARCHAR2 (1);
      l_rev_retcode        VARCHAR2 (1);
      l_attr_retcode       VARCHAR2 (1);
      l_bulk_retcode       VARCHAR2 (1);
      l_po_line_retcode    VARCHAR2 (1);         -- Added by Vamshi in Rev 1.2


      CURSOR CUR_ERROR
      IS
         SELECT '1. MTL_SYSTEM_ITEMS_INTERFACE' TABLE_NAME,
                COUNT (1) ERROR_RECORDS
           FROM MTL_SYSTEM_ITEMS_INTERFACE
          WHERE PROCESS_FLAG IN (3, 4)
         UNION
         SELECT '2. MTL_ITEM_REVISIONS_INTERFACE' TABLE_NAME,
                COUNT (1) ERROR_RECORDS
           FROM MTL_ITEM_REVISIONS_INTERFACE
          WHERE PROCESS_FLAG IN (3, 4)
         UNION
         SELECT '3. EGO_ITM_USR_ATTR_INTRFC' TABLE_NAME,
                COUNT (1) ERROR_RECORDS
           FROM EGO_ITM_USR_ATTR_INTRFC
          WHERE PROCESS_STATUS IN (3)
         UNION
         SELECT '4. EGO_BULKLOAD_INTF' TABLE_NAME, COUNT (1) ERROR_RECORDS
           FROM EGO_BULKLOAD_INTF
          WHERE PROCESS_STATUS NOT IN (1, 7, 99)
         -- Added below by Vamshi in Rev 1.2  -  Start
         UNION
         SELECT '5. XXWC_PO_LINE_ADDTIONAL_ATTR_T' TABLE_NAME,
                COUNT (1) ERROR_RECORDS
           FROM XXWC.XXWC_PO_LINE_ADDTIONAL_ATTR_T XPLA
          WHERE XPLA.CREATION_DATE <= SYSDATE - 720;
   -- Added above by Vamshi in Rev 1.2  -  End
   BEGIN
      fnd_file.put_line (
         fnd_file.LOG,
         'Beginning of interfaced processed data backup & Purging from tables listed in XXWC_INTF_PURGE_TABLES value set');
      fnd_file.put_line (
         fnd_file.LOG,
         '==============================================================================================================');
      fnd_file.put_line (fnd_file.LOG, '  ');


      fnd_file.put_line (
         fnd_file.output,
         '============================================================================================');
      fnd_file.put_line (
         fnd_file.output,
         '      Table Name                      Num. of Records Archived      Num.of Records Deleted  ');
      fnd_file.put_line (
         fnd_file.output,
         '============================================================================================');
      fnd_file.put_line (fnd_file.output, '     ');

      IF p_tab_selection = 'ALL'
      THEN
         -- MTL_SYSTEM_ITEMS_INTERFACE TABLE PROCESS
         l_sec := 'ALL';                         -- added --TMS#20150303-00158
         --l_sec := 'MTL_SYSTEM_ITEMS_INTERFACE'; --TMS#20150303-00158
         MTL_SYSTEM_ITEMS_INTF_PRC (l_item_errbuf, l_item_retcode);

         -- MTL_ITEM_REVISIONS_INTERFACE TABLE PROCESS
         --l_sec := 'MTL_ITEM_REVISIONS_INTERFACE';  --TMS#20150303-00158
         MTL_ITEM_REV_INTF_PRC (l_rev_errbuf, l_rev_retcode);

         --EGO_BULKLOAD_INTF TABLE PROCESS
         --l_sec := 'EGO_BULKLOAD_INTF';  --TMS#20150303-00158
         EGO_BULKLOAD_INTF_PRC (l_bulk_errbuf, l_bulk_retcode);

         --EGO_ITM_USR_ATTR_INTRFC TABLE PROCESS
         --l_sec := 'EGO_ITM_USR_ATTR_INTRFC';  --TMS#20150303-00158
         EGO_ITM_ATTR_INTF_PRC (l_attr_errbuf, l_attr_retcode);

         --XXWC_PO_LINE_ADDTIONAL_ATTR_T TABLE PROCESS
         -- Added by Vamshi in Rev 1.2
         XXWC_PO_LINE_ATTRIBUTE_PRC (l_po_line_errbuf, l_po_line_retcode);
      ELSIF p_tab_selection = 'MTL_SYSTEM_ITEMS_INTERFACE'
      THEN
         l_sec := p_tab_selection;
         MTL_SYSTEM_ITEMS_INTF_PRC (l_item_errbuf, l_item_retcode);
      ELSIF p_tab_selection = 'MTL_ITEM_REVISIONS_INTERFACE'
      THEN
         l_sec := p_tab_selection;
         MTL_ITEM_REV_INTF_PRC (l_rev_errbuf, l_rev_retcode);
      ELSIF p_tab_selection = 'EGO_BULKLOAD_INTF'
      THEN
         l_sec := p_tab_selection;
         EGO_BULKLOAD_INTF_PRC (l_bulk_errbuf, l_bulk_retcode);
      ELSIF p_tab_selection = 'EGO_ITM_USR_ATTR_INTRFC'
      THEN
         l_sec := p_tab_selection;
         EGO_ITM_ATTR_INTF_PRC (l_attr_errbuf, l_attr_retcode);
      -- Added below by Vamshi in Rev 1.2  -  Start
      ELSIF p_tab_selection = 'XXWC_PO_LINE_ADDTIONAL_ATTR_T'
      THEN
         l_sec := p_tab_selection;
         XXWC_PO_LINE_ATTRIBUTE_PRC (l_po_line_errbuf, l_po_line_retcode);
      -- Added above by Vamshi in Rev 1.2  -  End
      END IF;

      fnd_file.put_line (fnd_file.output, '     ');

      fnd_file.put_line (
         fnd_file.output,
         '============================================================================================');

      fnd_file.put_line (fnd_file.output, '     ');

      fnd_file.put_line (
         fnd_file.output,
         '============================================================================================');

      fnd_file.put_line (
         fnd_file.output,
         'Table Name                                      Num. Of Error Records/Purge Records ');  -- Changed in Rev 1.2

      fnd_file.put_line (
         fnd_file.output,
         '============================================================================================');

      FOR REC_ERROR IN CUR_ERROR
      LOOP
         fnd_file.put_line (
            fnd_file.output,
               ' '
            || RPAD (REC_ERROR.TABLE_NAME, 30, ' ')
            || '                  '
            || LPAD (REC_ERROR.ERROR_RECORDS, 10, ' '));
      END LOOP;

      fnd_file.put_line (
         fnd_file.output,
         '============================================================================================');


      IF (   l_item_retcode = 2
          OR l_rev_retcode = 2
          OR l_bulk_retcode = 2
          OR l_attr_retcode = 2)
      THEN
         l_err_msg :=
            SUBSTR (
               TRIM (
                     l_item_errbuf
                  || ' '
                  || l_rev_errbuf
                  || ' '
                  || l_attr_errbuf
                  || ' '
                  || l_bulk_errbuf),
               1,
               1900);
         RAISE PROGRAM_ERROR;
      END IF;

      p_retcode := 0;
      p_errbuf := NULL;
   EXCEPTION
      WHEN PROGRAM_ERROR
      THEN
         l_err_msg :=
               l_err_msg
            || l_sec
            || ' ...Error_Stack...'
            || DBMS_UTILITY.format_error_stack ()
            || ' Error_Backtrace...'
            || DBMS_UTILITY.format_error_backtrace ();
         fnd_file.put_line (fnd_file.LOG, l_err_msg);
         -- Calling ERROR API
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || l_procedure,
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (l_err_msg, 1, 2000),
            p_error_desc          => SUBSTR (l_err_msg, 1, 240),
            p_distribution_list   => g_distro_list,
            p_module              => 'INV');

         p_retcode := 2;
         p_errbuf := l_err_msg;
      WHEN OTHERS
      THEN
         l_err_msg :=
               l_err_msg
            || l_sec
            || ' ...Error_Stack...'
            || DBMS_UTILITY.format_error_stack ()
            || ' Error_Backtrace...'
            || DBMS_UTILITY.format_error_backtrace ();
         fnd_file.put_line (fnd_file.LOG, l_err_msg);
         -- Calling ERROR API
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || l_procedure,
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (l_err_msg, 1, 2000),
            p_error_desc          => SUBSTR (l_err_msg, 1, 240),
            p_distribution_list   => g_distro_list,
            p_module              => 'INV');

         p_retcode := 2;
         p_errbuf := l_err_msg;
   END;


   PROCEDURE MTL_SYSTEM_ITEMS_INTF_PRC (p_errbuf    OUT VARCHAR2,
                                        p_retcode   OUT VARCHAR2)
   IS
      /**********************************************************************************************
      -- PROCEDURE: MTL_SYSTEM_ITEMS_INTF_PRC
      --
      -- PURPOSE: Procedure created to purge processed records from table MTL_SYSTEM_ITEMS_INTERFACE.
      --          Table needs to be registered in 'XXWC_INTF_PURGE_TABLES' value set.
      -- HISTORY
      -- ============================================================================================
      -- ============================================================================================
      -- VERSION DATE          AUTHOR(S)       DESCRIPTION
      -- ------- -----------   --------------- ------------------------------------------------------
      -- 1.0     05-Feb-2015   P.Vamshidhar    Created this Procedure. - TMS#20141120-00077
      -- 1.1     03-Mar-2015   P.Vamshidhar    Removed validation to archive table while deleting
                                               from interface table TMS#20150303-00158
      ***********************************************************************************************/

      CURSOR CUR_MTL_SYSTEM_ITEMS
      IS
         SELECT MSII.*, SYSDATE INSERT_DATE
           FROM MTL_SYSTEM_ITEMS_INTERFACE MSII
          WHERE     MSII.PROCESS_FLAG IN (7)
                AND MSII.LAST_UPDATE_DATE <= SYSDATE - 1;

      TYPE l_mtl_system_items_intf IS TABLE OF CUR_MTL_SYSTEM_ITEMS%ROWTYPE;

      l_mtl_system_items_intf_obj   l_mtl_system_items_intf;
      l_record_count                NUMBER;

      l_table_name                  VARCHAR2 (30)
                                       := 'MTL_SYSTEM_ITEMS_INTERFACE';
      l_status                      VARCHAR2 (1);
      l_total_record_count          NUMBER := 0;
      l_record_delete_count         NUMBER := 0;
   BEGIN
      fnd_file.put_line (
         fnd_file.LOG,
         'Beginning MTL_SYSTEM_ITEMS_INTERFACE table processed data cleanup');
      fnd_file.put_line (
         fnd_file.LOG,
         '=================================================================');

      -- Checking MTL_SYSTEM_ITEMS_INTERFACE registered in 'XXWC_INTF_PURGE_TABLES' value set
      l_status := table_check (l_table_name);

      fnd_file.put_line (fnd_file.LOG,
                         l_table_name || ' Table Check validation completed');

      IF l_status = 'Y'
      THEN
         -- Deleting history data from XXWC.XXWC_MTL_SYS_ITEM_INTF_ARC_TBL table.
         DELETE FROM XXWC.XXWC_MTL_SYS_ITEM_INTF_ARC_TBL
               WHERE TRUNC (INSERT_DATE) <= TRUNC (SYSDATE) - 7;

         fnd_file.put_line (
            fnd_file.LOG,
            'XXWC.XXWC_MTL_SYS_ITEM_INTF_ARC_TBL backup table data cleared');

         -- Inserting data into backup table from interface table.
         OPEN CUR_MTL_SYSTEM_ITEMS;

         LOOP
            FETCH CUR_MTL_SYSTEM_ITEMS
               BULK COLLECT INTO l_mtl_system_items_intf_obj
               LIMIT 10000;

            FORALL l_record_count IN 1 .. l_mtl_system_items_intf_obj.COUNT
               INSERT INTO XXWC.XXWC_MTL_SYS_ITEM_INTF_ARC_TBL
                    VALUES l_mtl_system_items_intf_obj (l_record_count);

            l_total_record_count :=
               l_total_record_count + l_mtl_system_items_intf_obj.COUNT;

            l_mtl_system_items_intf_obj.DELETE;
            EXIT WHEN CUR_MTL_SYSTEM_ITEMS%NOTFOUND;
         END LOOP;

         CLOSE CUR_MTL_SYSTEM_ITEMS;

         fnd_file.put_line (
            fnd_file.LOG,
            'Data copied into XXWC.XXWC_MTL_SYS_ITEM_INTF_ARC_TBL backup table');

         --since backup completed, deleting data from interface table.

         DELETE FROM MTL_SYSTEM_ITEMS_INTERFACE MSII
               WHERE      --    Commented below code TMS#20150303-00158 -- 1.1
                     --       EXISTS
                     --        (SELECT 1
                     --           FROM XXWC.XXWC_MTL_SYS_ITEM_INTF_ARC_TBL XMSI
                     --          WHERE XMSI.INVENTORY_ITEM_ID =
                     --                   MSII.INVENTORY_ITEM_ID) AND
                     PROCESS_FLAG IN (7) AND LAST_UPDATE_DATE <= SYSDATE - 1;

         l_record_delete_count := SQL%ROWCOUNT;

         fnd_file.put_line (
            fnd_file.LOG,
            'Data deleted from interface table ' || l_table_name);

         IF l_record_delete_count = l_total_record_count
         THEN
            --Printing information in output
            fnd_file.put_line (
               fnd_file.output,
                  RPAD (l_table_name, 30, ' ')
               || '                   '
               || LPAD (l_total_record_count, 10, ' ')
               || '                   '
               || LPAD (l_record_delete_count, 10, ' '));
            COMMIT;
            fnd_file.put_line (
               fnd_file.LOG,
               l_table_name || ' table cleanup was completed successfully');
            p_retcode := 0;
         ELSE
            ROLLBACK;
            fnd_file.put_line (
               fnd_file.LOG,
                  l_table_name
               || ' table cleanup was not completed successfully');
            p_retcode := 2;
            -- Added errbuf -- TMS#20150303-00158.
            p_errbuf := l_table_name || ' delete count is not equal';
         END IF;
      ELSE
         FND_FILE.PUT_LINE (FND_FILE.LOG,
                            'Since Table check failed error out');
         p_retcode := 2;
      END IF;

      fnd_file.put_line (fnd_file.LOG, '   ');
   EXCEPTION
      WHEN OTHERS
      THEN
         CLOSE CUR_MTL_SYSTEM_ITEMS;

         ROLLBACK;
         p_retcode := 2;
         -- Added table name to get which table got exception -- TMS#20150303-00158.
         p_errbuf := SUBSTR ( (l_table_name || SQLERRM), 1, 2000);
   END;



   PROCEDURE MTL_ITEM_REV_INTF_PRC (p_errbuf    OUT VARCHAR2,
                                    p_retcode   OUT VARCHAR2)
   IS
      /**********************************************************************************************
      -- PROCEDURE: MTL_ITEM_REV_INTF_PRC
      --
      -- PURPOSE: Procedure created to purge processed records from table MTL_ITEM_REVISIONS_INTERFACE.
      --          Table needs to be registered in 'XXWC_INTF_PURGE_TABLES' value set.
      -- HISTORY
      -- ============================================================================================
      -- ============================================================================================
      -- VERSION DATE          AUTHOR(S)       DESCRIPTION
      -- ------- -----------   --------------- ------------------------------------------------------
      -- 1.0     05-Feb-2015   P.Vamshidhar    Created this Procedure. - TMS#20141120-00077
      -- 1.1     03-Mar-2015   P.Vamshidhar    Removed validation to archive table while deleting
                                               from interface table TMS#20150303-00158
      ***********************************************************************************************/

      CURSOR CUR_MTL_ITEM_REV_INTF
      IS
         SELECT MIRI.*, SYSDATE INSERT_DATE
           FROM MTL_ITEM_REVISIONS_INTERFACE MIRI
          WHERE     MIRI.PROCESS_FLAG IN (7)
                AND MIRI.LAST_UPDATE_DATE <= SYSDATE - 1;

      TYPE l_mtl_item_rev_intf IS TABLE OF CUR_MTL_ITEM_REV_INTF%ROWTYPE;

      l_mtl_item_rev_intf_obj   l_mtl_item_rev_intf;
      l_record_count            NUMBER;

      l_table_name              VARCHAR2 (30)
                                   := 'MTL_ITEM_REVISIONS_INTERFACE';
      l_status                  VARCHAR2 (1);
      l_total_record_count      NUMBER := 0;
      l_record_delete_count     NUMBER := 0;
   BEGIN
      fnd_file.put_line (
         fnd_file.LOG,
         'Beginning MTL_ITEM_REVISIONS_INTERFACE table processed data cleanup');
      fnd_file.put_line (
         fnd_file.LOG,
         '===================================================================');

      -- Checking MTL_ITEM_REVISIONS_INTERFACE registered in 'XXWC_INTF_PURGE_TABLES' value set
      l_status := table_check (l_table_name);

      fnd_file.put_line (fnd_file.LOG,
                         l_table_name || ' Table Check validation completed');

      IF l_status = 'Y'
      THEN
         -- Deleting history data from XXWC.XXWC_MTL_ITEM_REV_INTF_ARC_TBL table.
         DELETE FROM XXWC.XXWC_MTL_ITEM_REV_INTF_ARC_TBL
               WHERE TRUNC (INSERT_DATE) <= TRUNC (SYSDATE) - 7;

         fnd_file.put_line (
            fnd_file.LOG,
            'XXWC.XXWC_MTL_ITEM_REV_INTF_ARC_TBL backup table data cleared');

         -- Inserting data into backup table from interface table.
         OPEN CUR_MTL_ITEM_REV_INTF;

         LOOP
            FETCH CUR_MTL_ITEM_REV_INTF
               BULK COLLECT INTO l_mtl_item_rev_intf_obj
               LIMIT 10000;

            FORALL l_record_count IN 1 .. l_mtl_item_rev_intf_obj.COUNT
               INSERT INTO XXWC.XXWC_MTL_ITEM_REV_INTF_ARC_TBL
                    VALUES l_mtl_item_rev_intf_obj (l_record_count);

            l_total_record_count :=
               l_total_record_count + l_mtl_item_rev_intf_obj.COUNT;

            l_mtl_item_rev_intf_obj.DELETE;
            EXIT WHEN CUR_MTL_ITEM_REV_INTF%NOTFOUND;
         END LOOP;

         CLOSE CUR_MTL_ITEM_REV_INTF;

         fnd_file.put_line (
            fnd_file.LOG,
            'Data copied into XXWC.XXWC_MTL_ITEM_REV_INTF_ARC_TBL backup table');

         --since backup completed, deleting data from interface table.

         DELETE FROM MTL_ITEM_REVISIONS_INTERFACE MSII
               WHERE      --    Commented below code TMS#20150303-00158 -- 1.1
                     --  EXISTS
                     --        (SELECT 1
                     --           FROM XXWC.XXWC_MTL_ITEM_REV_INTF_ARC_TBL XMSI
                     --          WHERE XMSI.INVENTORY_ITEM_ID =
                     --                   MSII.INVENTORY_ITEM_ID) AND
                     PROCESS_FLAG IN (7) AND LAST_UPDATE_DATE <= SYSDATE - 1;

         l_record_delete_count := SQL%ROWCOUNT;

         fnd_file.put_line (
            fnd_file.LOG,
            'Data deleted from interface table ' || l_table_name);

         IF l_record_delete_count = l_total_record_count
         THEN
            --Printing information in output
            fnd_file.put_line (
               fnd_file.output,
                  RPAD (l_table_name, 30, ' ')
               || '                   '
               || LPAD (l_total_record_count, 10, ' ')
               || '                   '
               || LPAD (l_record_delete_count, 10, ' '));
            COMMIT;
            fnd_file.put_line (
               fnd_file.LOG,
               l_table_name || ' table cleanup was completed successfully');
            p_retcode := 0;
         ELSE
            ROLLBACK;
            fnd_file.put_line (
               fnd_file.LOG,
                  l_table_name
               || ' table cleanup was not completed successfully');
            p_retcode := 2;
            -- Added errbuf -- TMS#20150303-00158.
            p_errbuf := l_table_name || ' delete count is not equal';
         END IF;
      ELSE
         FND_FILE.PUT_LINE (FND_FILE.LOG,
                            'Since Table check failed error out');
         p_retcode := 2;
      END IF;

      fnd_file.put_line (fnd_file.LOG, '   ');
   EXCEPTION
      WHEN OTHERS
      THEN
         CLOSE CUR_MTL_ITEM_REV_INTF;

         ROLLBACK;
         p_retcode := 2;
         -- Added table name to get which table got exception -- TMS#20150303-00158.
         p_errbuf := SUBSTR ( (l_table_name || SQLERRM), 1, 2000);
   END;



   PROCEDURE EGO_ITM_ATTR_INTF_PRC (p_errbuf    OUT VARCHAR2,
                                    p_retcode   OUT VARCHAR2)
   IS
      /**********************************************************************************************
      -- PROCEDURE: EGO_ITM_ATTR_INTF_PRC
      --
      -- PURPOSE: Procedure created to purge processed records from table EGO_ITM_USR_ATTR_INTRFC .
      --          Table needs to be registered in 'XXWC_INTF_PURGE_TABLES' value set.
      -- HISTORY
      -- ============================================================================================
      -- ============================================================================================
      -- VERSION DATE          AUTHOR(S)       DESCRIPTION
      -- ------- -----------   --------------- ------------------------------------------------------
      -- 1.0     05-Feb-2015   P.Vamshidhar    Created this Procedure. - TMS#20141120-00077
      -- 1.1     03-Mar-2015   P.Vamshidhar    Removed validation to archive table while deleting
                                               from interface table TMS#20150303-00158
      ***********************************************************************************************/


      CURSOR CUR_EGO_ITM_ATTR_INTF
      IS
         SELECT EIUI.*, SYSDATE INSERT_DATE
           FROM EGO_ITM_USR_ATTR_INTRFC EIUI
          WHERE     EIUI.PROCESS_STATUS IN (4, 7)
                AND EIUI.LAST_UPDATE_DATE <= SYSDATE - 1;

      TYPE l_ego_itm_attr_intf IS TABLE OF CUR_EGO_ITM_ATTR_INTF%ROWTYPE;

      l_ego_itm_attr_intf_obj   l_ego_itm_attr_intf;
      l_record_count            NUMBER;

      l_table_name              VARCHAR2 (30) := 'EGO_ITM_USR_ATTR_INTRFC';
      l_status                  VARCHAR2 (1);
      l_total_record_count      NUMBER := 0;
      l_record_delete_count     NUMBER := 0;
   BEGIN
      fnd_file.put_line (
         fnd_file.LOG,
         'Beginning EGO_ITM_USR_ATTR_INTRFC table processed data cleanup');
      fnd_file.put_line (
         fnd_file.LOG,
         '=================================================================');

      -- Checking EGO_ITM_USR_ATTR_INTRFC registered in 'XXWC_INTF_PURGE_TABLES' value set
      l_status := table_check (l_table_name);

      fnd_file.put_line (fnd_file.LOG,
                         l_table_name || ' Table Check validation completed');

      IF l_status = 'Y'
      THEN
         -- Deleting history data from XXWC.XXWC_EGO_ITM_USRAT_INF_ARC_TBL table.
         DELETE FROM XXWC.XXWC_EGO_ITM_USRAT_INF_ARC_TBL
               WHERE TRUNC (INSERT_DATE) <= TRUNC (SYSDATE) - 7;

         fnd_file.put_line (
            fnd_file.LOG,
            'XXWC.XXWC_EGO_ITM_USRAT_INF_ARC_TBL backup table data cleared');

         -- Inserting data into backup table from interface table.
         OPEN CUR_EGO_ITM_ATTR_INTF;

         LOOP
            FETCH CUR_EGO_ITM_ATTR_INTF
               BULK COLLECT INTO l_ego_itm_attr_intf_obj
               LIMIT 10000;

            FORALL l_record_count IN 1 .. l_ego_itm_attr_intf_obj.COUNT
               INSERT INTO XXWC.XXWC_EGO_ITM_USRAT_INF_ARC_TBL
                    VALUES l_ego_itm_attr_intf_obj (l_record_count);

            l_total_record_count :=
               l_total_record_count + l_ego_itm_attr_intf_obj.COUNT;

            l_ego_itm_attr_intf_obj.DELETE;
            EXIT WHEN CUR_EGO_ITM_ATTR_INTF%NOTFOUND;
         END LOOP;

         CLOSE CUR_EGO_ITM_ATTR_INTF;

         fnd_file.put_line (
            fnd_file.LOG,
            'Data copied into XXWC.XXWC_EGO_ITM_USRAT_INF_ARC_TBL backup table');

         --since backup completed, deleting data from interface table.

         DELETE FROM EGO_ITM_USR_ATTR_INTRFC MSII
               WHERE      --    Commented below code TMS#20150303-00158 -- 1.1
                         --    EXISTS
                         --        (SELECT 1
                         --           FROM XXWC.XXWC_EGO_ITM_USRAT_INF_ARC_TBL XMSI
                         --          WHERE XMSI.INVENTORY_ITEM_ID =
                         --                   MSII.INVENTORY_ITEM_ID) AND
                         PROCESS_STATUS IN (4, 7)
                     AND LAST_UPDATE_DATE <= SYSDATE - 1;

         l_record_delete_count := SQL%ROWCOUNT;

         fnd_file.put_line (
            fnd_file.LOG,
            'Data deleted from interface table ' || l_table_name);

         IF l_record_delete_count = l_total_record_count
         THEN
            --Printing information in output
            fnd_file.put_line (
               fnd_file.output,
                  RPAD (l_table_name, 30, ' ')
               || '                   '
               || LPAD (l_total_record_count, 10, ' ')
               || '                   '
               || LPAD (l_record_delete_count, 10, ' '));
            COMMIT;
            fnd_file.put_line (
               fnd_file.LOG,
               l_table_name || ' table cleanup was completed successfully');
            p_retcode := 0;
         ELSE
            ROLLBACK;
            fnd_file.put_line (
               fnd_file.LOG,
                  l_table_name
               || ' table cleanup was not completed successfully');
            p_retcode := 2;
            -- Added errbuf -- TMS#20150303-00158.
            p_errbuf := l_table_name || ' delete count is not equal';
         END IF;
      ELSE
         FND_FILE.PUT_LINE (FND_FILE.LOG,
                            'Since Table check failed error out');
         p_retcode := 2;
      END IF;

      fnd_file.put_line (fnd_file.LOG, '   ');
   EXCEPTION
      WHEN OTHERS
      THEN
         CLOSE CUR_EGO_ITM_ATTR_INTF;

         ROLLBACK;
         p_retcode := 2;
         -- Added table name to get which table got exception -- TMS#20150303-00158.
         p_errbuf := SUBSTR ( (l_table_name || SQLERRM), 1, 2000);
   END;



   PROCEDURE EGO_BULKLOAD_INTF_PRC (p_errbuf    OUT VARCHAR2,
                                    p_retcode   OUT VARCHAR2)
   IS
      /**********************************************************************************************
      -- PROCEDURE: EGO_BULKLOAD_INTF_PRC
      --
      -- PURPOSE: Procedure created to purge processed records from table EGO_BULKLOAD_INTF.
      --          Table needs to be registered in 'XXWC_INTF_PURGE_TABLES' value set.
      -- HISTORY
      -- ============================================================================================
      -- ============================================================================================
      -- VERSION DATE          AUTHOR(S)       DESCRIPTION
      -- ------- -----------   --------------- ------------------------------------------------------
      -- 1.0     05-Feb-2015   P.Vamshidhar    Created this Procedure. - TMS#20141120-00077
      -- 1.1     03-Mar-2015   P.Vamshidhar    Removed validation to archive table while deleting
                                               from interface table TMS#20150303-00158
      ***********************************************************************************************/


      CURSOR CUR_EGO_BULKLOAD_INTF
      IS
         SELECT EBI.*, SYSDATE INSERT_DATE
           FROM EGO_BULKLOAD_INTF EBI
          WHERE     EBI.PROCESS_STATUS IN (7, 99)
                AND EBI.LAST_UPDATE_DATE <= SYSDATE - 1;

      TYPE l_ego_bulkload_intf_intf IS TABLE OF CUR_EGO_BULKLOAD_INTF%ROWTYPE;

      l_ego_bulkload_intf_obj   l_ego_bulkload_intf_intf;
      l_record_count            NUMBER;

      l_table_name              VARCHAR2 (30) := 'EGO_BULKLOAD_INTF';
      l_status                  VARCHAR2 (1);
      l_total_record_count      NUMBER := 0;
      l_record_delete_count     NUMBER := 0;
   BEGIN
      fnd_file.put_line (
         fnd_file.LOG,
         'Beginning EGO_BULKLOAD_INTF table processed data cleanup');
      fnd_file.put_line (
         fnd_file.LOG,
         '=================================================================');

      -- Checking EGO_BULKLOAD_INTF registered in 'XXWC_INTF_PURGE_TABLES' value set
      l_status := table_check (l_table_name);

      fnd_file.put_line (fnd_file.LOG,
                         l_table_name || ' Table Check validation completed');

      IF l_status = 'Y'
      THEN
         -- Deleting history data from XXWC.XXWC_EGO_BULKLOAD_INTF_ARC_TBL table.
         DELETE FROM XXWC.XXWC_EGO_BULKLOAD_INTF_ARC_TBL
               WHERE TRUNC (INSERT_DATE) <= TRUNC (SYSDATE) - 7;

         fnd_file.put_line (
            fnd_file.LOG,
            'XXWC.XXWC_EGO_BULKLOAD_INTF_ARC_TBL backup table data cleared');

         -- Inserting data into backup table from interface table.
         OPEN CUR_EGO_BULKLOAD_INTF;

         LOOP
            FETCH CUR_EGO_BULKLOAD_INTF
               BULK COLLECT INTO l_ego_bulkload_intf_obj
               LIMIT 10000;

            FORALL l_record_count IN 1 .. l_ego_bulkload_intf_obj.COUNT
               INSERT INTO XXWC.XXWC_EGO_BULKLOAD_INTF_ARC_TBL
                    VALUES l_ego_bulkload_intf_obj (l_record_count);

            l_total_record_count :=
               l_total_record_count + l_ego_bulkload_intf_obj.COUNT;

            l_ego_bulkload_intf_obj.DELETE;
            EXIT WHEN CUR_EGO_BULKLOAD_INTF%NOTFOUND;
         END LOOP;

         CLOSE CUR_EGO_BULKLOAD_INTF;

         fnd_file.put_line (
            fnd_file.LOG,
            'Data copied into XXWC.XXWC_EGO_BULKLOAD_INTF_ARC_TBL backup table');

         --since backup completed, deleting data from interface table.

         DELETE FROM EGO_BULKLOAD_INTF MSII
               WHERE      --    Commented below code TMS#20150303-00158 -- 1.1
                         --       EXISTS
                         --        (SELECT 1
                         --           FROM XXWC.XXWC_EGO_BULKLOAD_INTF_ARC_TBL XMSI
                         --          WHERE XMSI.TRANSACTION_ID = MSII.TRANSACTION_ID) AND
                         PROCESS_STATUS IN (7, 99)
                     AND LAST_UPDATE_DATE <= SYSDATE - 1;

         l_record_delete_count := SQL%ROWCOUNT;

         fnd_file.put_line (
            fnd_file.LOG,
            'Data deleted from interface table ' || l_table_name);

         IF l_record_delete_count = l_total_record_count
         THEN
            --Printing information in output
            fnd_file.put_line (
               fnd_file.output,
                  RPAD (l_table_name, 30, ' ')
               || '                   '
               || LPAD (l_total_record_count, 10, ' ')
               || '                   '
               || LPAD (l_record_delete_count, 10, ' '));
            COMMIT;
            fnd_file.put_line (
               fnd_file.LOG,
               l_table_name || ' table cleanup was completed successfully');
            p_retcode := 0;
         ELSE
            ROLLBACK;
            fnd_file.put_line (
               fnd_file.LOG,
                  l_table_name
               || ' table cleanup was not completed successfully');
            p_retcode := 2;
            -- Added errbuf -- TMS#20150303-00158.
            p_errbuf := l_table_name || ' delete count is not equal';
         END IF;
      ELSE
         FND_FILE.PUT_LINE (FND_FILE.LOG,
                            'Since Table check failed error out');
         p_retcode := 2;
      END IF;

      fnd_file.put_line (fnd_file.LOG, '   ');
   EXCEPTION
      WHEN OTHERS
      THEN
         CLOSE CUR_EGO_BULKLOAD_INTF;

         ROLLBACK;
         p_retcode := 2;
         -- Added table name to get which table got exception -- TMS#20150303-00158.
         p_errbuf := SUBSTR ( (l_table_name || SQLERRM), 1, 2000);
   END;

   FUNCTION table_check (p_table_name IN VARCHAR2)
      RETURN VARCHAR2
   IS
      /**********************************************************************************************
      -- Function: table_check
      --
      -- PURPOSE: Function created to check input table registered in
      --          'XXWC_INTF_PURGE_TABLES' value set.
      -- HISTORY
      -- ============================================================================================
      -- ============================================================================================
      -- VERSION DATE          AUTHOR(S)       DESCRIPTION
      -- ------- -----------   --------------- ------------------------------------------------------
      -- 1.0     05-Feb-2015   P.Vamshidhar      Created this function. - TMS#20141120-00077
      ***********************************************************************************************/


      l_status            VARCHAR2 (1) := 'N';
      l_purge_value_set   VARCHAR2 (30) := 'XXWC_INTF_PURGE_TABLES';
   BEGIN
      SELECT 'Y'
        INTO l_status
        FROM APPS.FND_FLEX_VALUE_SETS FFVS, APPS.FND_FLEX_VALUES FFV
       WHERE     FFVS.FLEX_VALUE_SET_ID = FFV.FLEX_VALUE_SET_ID
             AND FFVS.FLEX_VALUE_SET_NAME = l_purge_value_set
             AND FFV.FLEX_VALUE = p_table_name
             AND ENABLED_FLAG = 'Y';

      RETURN (NVL (l_status, 'N'));
   EXCEPTION
      WHEN OTHERS
      THEN
         fnd_file.put_line (
            fnd_file.LOG,
               p_table_name
            || ' Error occured @ Valueset check '
            || SUBSTR (SQLERRM, 1, 2000));
         RETURN ('N');
   END;

   PROCEDURE XXWC_PO_LINE_ATTRIBUTE_PRC (p_errbuf    OUT VARCHAR2,
                                         p_retcode   OUT VARCHAR2)
   IS
      /***********************************************************************************************************************
      -- PROCEDURE: XXWC_PO_LINE_ATTRIBUTE_PRC
      --
      -- PURPOSE: Procedure created to delete records created 2 years before from table XXWC.XXWC_PO_LINE_ADDTIONAL_ATTR_T
      --          Table needs to be registered in 'XXWC_INTF_PURGE_TABLES' value set.
      -- HISTORY
      -- =====================================================================================================================
      -- =====================================================================================================================
      -- VERSION DATE          AUTHOR(S)       DESCRIPTION
      -- ------- -----------   --------------- -------------------------------------------------------------------------------
      -- 1.2     16-Oct-2015   P.Vamshidhar    TMS#20150915-00126 - Created procedure to capture data from table
                                               XXWC.XXWC_PO_LINE_ADDTIONAL_ATTR_T before archive.
      ***********************************************************************************************************************/
      CURSOR CUR_XXWC_PO_LINE_BULKLOAD
      IS
         SELECT XPLA.*, SYSDATE INSERT_DATE
           FROM XXWC.XXWC_PO_LINE_ADDTIONAL_ATTR_T XPLA
          WHERE NVL (XPLA.CREATION_DATE, SYSDATE) <= SYSDATE - 720;

      TYPE l_xxwc_po_line_addtional_t
         IS TABLE OF CUR_XXWC_PO_LINE_BULKLOAD%ROWTYPE;

      l_xxwc_po_line_addt_attr_obj   l_xxwc_po_line_addtional_t;
      l_record_count                 NUMBER;

      l_table_name                   VARCHAR2 (30)
                                        := 'XXWC_PO_LINE_ADDTIONAL_ATTR_T';
      l_status                       VARCHAR2 (1);
      l_total_record_count           NUMBER := 0;
      l_record_delete_count          NUMBER := 0;
   BEGIN
      fnd_file.put_line (
         fnd_file.LOG,
         'Beginning XXWC.XXWC_PO_LINE_ADDTIONAL_ATTR_T table processed data cleanup');
      fnd_file.put_line (
         fnd_file.LOG,
         '=================================================================');

      -- Checking XXWC_PO_LINE_ADDTIONAL_ATTR_T registered in 'XXWC_INTF_PURGE_TABLES' value set
      l_status := table_check (l_table_name);

      fnd_file.put_line (fnd_file.LOG,
                         l_table_name || ' Table Check validation completed');

      IF l_status = 'Y'
      THEN
         -- Deleting history data from XXWC.XXWC_PO_LINE_ADDT_ATTR_ARCH_T table.
         DELETE FROM XXWC.XXWC_PO_LINE_ADDT_ATTR_ARCH_T
               WHERE TRUNC (INSERT_DATE) <= TRUNC (SYSDATE) - 7;

         fnd_file.put_line (
            fnd_file.LOG,
            'XXWC.XXWC_PO_LINE_ADDT_ATTR_ARCH_T backup table data cleared');

         -- Inserting data into backup table from interface table.
         OPEN CUR_XXWC_PO_LINE_BULKLOAD;

         LOOP
            FETCH CUR_XXWC_PO_LINE_BULKLOAD
               BULK COLLECT INTO l_xxwc_po_line_addt_attr_obj
               LIMIT 10000;

            FORALL l_record_count IN 1 .. l_xxwc_po_line_addt_attr_obj.COUNT
               INSERT INTO XXWC.XXWC_PO_LINE_ADDT_ATTR_ARCH_T
                    VALUES l_xxwc_po_line_addt_attr_obj (l_record_count);

            l_total_record_count :=
               l_total_record_count + l_xxwc_po_line_addt_attr_obj.COUNT;

            l_xxwc_po_line_addt_attr_obj.DELETE;
            EXIT WHEN CUR_XXWC_PO_LINE_BULKLOAD%NOTFOUND;
         END LOOP;

         CLOSE CUR_XXWC_PO_LINE_BULKLOAD;

         fnd_file.put_line (
            fnd_file.LOG,
            'Data copied into XXWC.XXWC_PO_LINE_ADDT_ATTR_ARCH_T backup table');

         --since backup completed, deleting data from interface table.

         DELETE FROM XXWC.XXWC_PO_LINE_ADDTIONAL_ATTR_T XPLA
               WHERE NVL (XPLA.CREATION_DATE, SYSDATE) <= SYSDATE - 720;

         l_record_delete_count := SQL%ROWCOUNT;

         fnd_file.put_line (
            fnd_file.LOG,
            'Data deleted from interface table ' || l_table_name);

         IF l_record_delete_count = l_total_record_count
         THEN
            --Printing information in output
            fnd_file.put_line (
               fnd_file.output,
                  RPAD (l_table_name, 30, ' ')
               || '                   '
               || LPAD (l_total_record_count, 10, ' ')
               || '                   '
               || LPAD (l_record_delete_count, 10, ' '));
            COMMIT;
            fnd_file.put_line (
               fnd_file.LOG,
               l_table_name || ' table cleanup was completed successfully');
            p_retcode := 0;
         ELSE
            ROLLBACK;
            fnd_file.put_line (
               fnd_file.LOG,
                  l_table_name
               || ' table cleanup was not completed successfully');
            p_retcode := 2;
            p_errbuf := l_table_name || ' delete count is not equal';
         END IF;
      ELSE
         FND_FILE.PUT_LINE (FND_FILE.LOG,
                            'Since Table check failed error out');
         p_retcode := 2;
      END IF;

      fnd_file.put_line (fnd_file.LOG, '   ');
   EXCEPTION
      WHEN OTHERS
      THEN
         CLOSE CUR_XXWC_PO_LINE_BULKLOAD;

         ROLLBACK;
         p_retcode := 2;
         p_errbuf := SUBSTR ( (l_table_name || SQLERRM), 1, 2000);
   END;
END xxwc_interface_purge_pkg;
/
