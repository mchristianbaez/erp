   /********************************************************************************
   -- File Name: XXWC_PO_LINE_ADDT_ATTR_ARCH_T.sql
   --
   -- PROGRAM TYPE: SQL Script   <API>
   --
   -- PURPOSE: Table to capture archive data to XXWC.XXWC_PO_LINE_ADDTIONAL_ATTR_T table
   --
   -- HISTORY
   -- ======================================================================================
   -- ======================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- ------------------------------------------------
   -- 1.0     16-Oct-2015   P.Vamshidhar    TMS#20150915-00126
                                            Added new Procedure to Purge from custom table
                                            XXWC.XXWC_PO_LINE_ADDTIONAL_ATTR_T
   *****************************************************************************************/

CREATE TABLE XXWC.XXWC_PO_LINE_ADDT_ATTR_ARCH_T
(
  PO_HEADER_ID        NUMBER,
  PO_LINE_ID          NUMBER,
  LINE_LOCATION_ID    NUMBER,
  ORGANIZATION_ID     NUMBER,
  ITEM_ID             NUMBER,
  STOCK_STATUS        VARCHAR2(10 BYTE),
  SYSTEM_DELIV_PRICE  NUMBER,
  BPA_NUMBER          VARCHAR2(20 BYTE),
  BPA_PRICE           NUMBER,
  LIST_PRICE          NUMBER,
  CREATED_BY          NUMBER,
  CREATION_DATE       DATE,
  LAST_UPDATED_BY     NUMBER,
  LAST_UPDATED_DATE   DATE,
  LAST_UPDATED_LOGIN  NUMBER,
  ORG_ID              NUMBER,
  INSERT_DATE         DATE
);
/
GRANT ALL ON XXWC.XXWC_PO_LINE_ADDT_ATTR_ARCH_T TO APPS;
/