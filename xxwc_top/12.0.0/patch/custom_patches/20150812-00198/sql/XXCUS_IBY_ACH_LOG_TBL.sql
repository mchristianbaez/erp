--
-- TMS 20150812-00198
-- XXCUS.XXCUS_IBY_ACH_LOG  (Table) 
-- Purpose: Store any errors during the rename of the Oracle Payments created temp file to the one searched by UC4.
-- 
CREATE TABLE XXCUS.XXCUS_IBY_ACH_LOG
(
  request_id number
,command   varchar2(4000)
,error_msg  varchar2(3000)
);
--
COMMENT ON TABLE XXCUS.XXCUS_IBY_ACH_LOG IS 'TMS 20150812-00198';
--
