 /********************************************************************************
  FILE NAME: XXWC.XXWC_INVITEM_XREF_REF
  PROGRAM TYPE: Table script

  PURPOSE: Conversion purpose for AH Harries

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     05/16/2018    Nancy Pahwa  TMS#20180308-00301  -- Initial Version
  ********************************************************************************/
CREATE TABLE "XXWC"."XXWC_INVITEM_XREF_REF" 
   (	"PARTNUMBER" VARCHAR2(30), 
	"XREF_PART" VARCHAR2(200), 
	"XREF_TYPE" VARCHAR2(200), 
	"ERROR_MESSAGE" VARCHAR2(4000), 
	"STATUS" VARCHAR2(1), 
	"CROSS_REF_STATUS" VARCHAR2(1), 
	"CROSS_REF_MESSAGE" VARCHAR2(2000)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "XXWC_DATA" ;