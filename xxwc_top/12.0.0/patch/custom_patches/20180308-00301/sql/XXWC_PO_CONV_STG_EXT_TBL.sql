 /********************************************************************************
  FILE NAME: "XXWC"."XXWC_PO_CONV_STG_EXT_TBL" 

  PROGRAM TYPE: External Table script

  PURPOSE: Conversion purpose for AH Harries

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     05/16/2018    Nancy Pahwa  TMS#20180308-00301  -- Initial Version
  ********************************************************************************/
CREATE TABLE "XXWC"."XXWC_PO_CONV_STG_EXT_TBL" 
   (  "ORGANIZATION_CODE" VARCHAR2(3), 
  "VENDOR_NAME" VARCHAR2(50), 
  "VENDOR_NUMBER" VARCHAR2(50), 
  "VENDOR_SITE_CODE" VARCHAR2(50), 
  "PARTNUMBER" VARCHAR2(50), 
  "ONHAND_QTY" NUMBER, 
  "AVG_COST" NUMBER, 
  "STATUS" VARCHAR2(1)
   ) 
   ORGANIZATION EXTERNAL 
    ( TYPE ORACLE_LOADER
      DEFAULT DIRECTORY "XXWC_AR_AHH_CASH_RCPT_CONV_DIR"
      ACCESS PARAMETERS
      ( RECORDS DELIMITED BY NEWLINE CHARACTERSET WE8MSWIN1252
      SKIP 1
    BADFILE 'XXWC_PO_CONV_STG_CONV.bad'
    DISCARDFILE 'XXWC_PO_CONV_STG_CONV.dsc'
    FIELDS TERMINATED BY '|'
    OPTIONALLY ENCLOSED BY '"' AND '"'
    REJECT ROWS WITH ALL NULL FIELDS
                )
      LOCATION
       ( "XXWC_AR_AHH_CASH_RCPT_CONV_DIR":'XXWC_PO_CONV_STG_CONV.csv'
       )
    )
   REJECT LIMIT UNLIMITED ;