CREATE OR REPLACE PACKAGE BODY apps.xxwc_ahh_conversion_pkg AS
  /*************************************************************************
  *   $Header xxwc_ahh_conversion_pkg.pkb $
  *   Module Name: xxwc_ahh_conversion_pkg
  *
  *   PURPOSE:   Used in receipt mass write off
  *
  *   REVISIONS:
  *   Ver        Date        Author                     Description
  *   ---------  ----------  ---------------         -------------------------
  *  1.0        04/24/2018   Nancy Pahwa    TMS#20180308-00301      Initial Version
  * ***************************************************************************/

  PROCEDURE xxwc_ahh_conv_upload_prc(p_filename     IN VARCHAR2,
                                     p_directory    IN VARCHAR2,
                                     p_location     IN VARCHAR2 DEFAULT NULL,
                                     p_new_filename OUT VARCHAR2) AS
    /*************************************************************************
      *   PROCEDURE Name: xxwc_ahh_conv_upload_prc
      *
      *   PURPOSE:   update status and error message in table xxwc_ahh_conv_upload_prc
    
    HISTORY
      ===============================================================================
      VERSION DATE          AUTHOR(S)              DESCRIPTION
      ------- -----------   --------------- -----------------------------------------
      1.0     04/24/2018    Nancy Pahwa   TMS#20180308-00301  AHH Upload process
     *****************************************************************************/
    l_file           UTL_FILE.file_type;
    l_blob_len       NUMBER;
    l_pos            INTEGER := 1;
    l_amount         BINARY_INTEGER := 32767;
    l_buffer         RAW(32767);
    l_directory      VARCHAR2(500);
    v_new_filename   VARCHAR2(500);
    v_new_filename1  VARCHAR2(500);
    l_count          NUMBER;
    l_bfilename      VARCHAR2(500);
    v_bfile          BFILE;
    l_year           NUMBER;
    l_timestamp      NUMBER;
    l_directory_path VARCHAR2(200);
    l_sql            VARCHAR2(200);
  BEGIN
    -- Checking if the Directory exists. Creating a directory if it doesn't exist.
    l_directory     := p_directory;
    v_new_filename  := SUBSTR(p_filename,
                              INSTR(p_filename, '/') + 1,
                              LENGTH(p_filename));
    v_new_filename1 := upper(NVL(SUBSTR(v_new_filename,
                                        0,
                                        INSTR(v_new_filename, '.') - 1),
                                 v_new_filename)) || '.csv';
    --Dbms_Output.Put_Line(l_directory||'  '||v_new_filename);
    v_bfile := BFILENAME(p_directory, v_new_filename1);
    l_file  := UTL_FILE.fopen(p_directory, v_new_filename1, 'WB', 32760);
    --      IF DBMS_LOB.FILEEXISTS (v_bfile) = 1
    --      THEN
    FOR rec IN (SELECT blob_content lblob
                  FROM apex_application_temp_files
                 WHERE name = p_filename
                   AND id = id) LOOP
      l_blob_len := DBMS_LOB.getlength(rec.lblob);
      WHILE l_pos < l_blob_len LOOP
        DBMS_LOB.read(rec.lblob, l_amount, l_pos, l_buffer);
        UTL_FILE.put_raw(l_file, l_buffer, FALSE);
        l_pos := l_pos + l_amount;
      END LOOP;
    END LOOP;
    UTL_FILE.fclose(l_file);
    --  END IF;
  EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      -- Close the file if something goes wrong.
      IF UTL_FILE.is_open(l_file) THEN
        UTL_FILE.fclose(l_file);
      END IF;
    
      RAISE;
  END;
END;
/