/*************************************************************************
  $Header XXWC_AR_AHH_CASH_RCPT_CONV_DIR.sql $
  Module Name: XXWC_AR_AHH_CASH_RCPT_CONV_DIR
  PURPOSE: To access Conversion files

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    ---------------------------------------------------------------------------------------
  1.0        19-Jun-2018  P.vamshidhar          TMS#20180308-00301 - Conversions
*******************************************************************************************************************************************/ 
DECLARE
   lvc_environment   VARCHAR2 (100);
   lvc_sql           VARCHAR2 (4000);
BEGIN
   SELECT '/xx_iface/' || LOWER (NAME) || '/inbound/conversions/artrans'
     INTO lvc_environment
     FROM v$database;

   lvc_sql :=
         'CREATE OR REPLACE DIRECTORY XXWC_AR_AHH_CASH_RCPT_CONV_DIR AS '
      || ''''
      || lvc_environment
      || '''';

   EXECUTE IMMEDIATE lvc_sql;
END;
/