 /********************************************************************************
  FILE NAME: XXWC.XXWC_AR_INVOICES_CONV_EXT_TBL.sql

  PROGRAM TYPE: External Table script

  PURPOSE: Conversion purpose for AH Harries

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     05/16/2018    Pattabhi Avula  TMS#20180308-00301  -- Initial Version
  ********************************************************************************/
  CREATE TABLE "XXWC"."XXWC_AR_INVOICES_CONV_EXT_TBL" 
   (	"INTERFACE_LINE_ATTRIBUTE1" VARCHAR2(150), 
	"ATTRIBUTE2" VARCHAR2(150), 
	"ATTRIBUTE3" VARCHAR2(150), 
	"ACCTD_AMOUNT" VARCHAR2(30), 
	"PAYING_CUSTOMER_ID" VARCHAR2(30), 
	"AMOUNT" VARCHAR2(30), 
	"TERM_NAME" VARCHAR2(50), 
	"ORIG_SYSTEM_BILL_CUSTOMER_REF" VARCHAR2(240), 
	"ORIG_SYSTEM_SOLD_CUSTOMER_REF" VARCHAR2(240), 
	"TRX_DATE" VARCHAR2(30), 
	"TRX_NUMBER" VARCHAR2(30), 
	"CUST_TRX_TYPE_NAME" VARCHAR2(30), 
	"UNIT_SELLING_PRICE" VARCHAR2(30), 
	"PRIMARY_SALESREP_NUMBER" VARCHAR2(30), 
	"TRX_NUMBER_1" VARCHAR2(100), 
	"SALES_ORDER" VARCHAR2(100), 
	"PURCHASE_ORDER" VARCHAR2(100), 
	"SHIP_TO_PARTY_SITE_NUMBER" VARCHAR2(50), 
	"COMMENTS" VARCHAR2(240)
   ) 
   ORGANIZATION EXTERNAL 
    ( TYPE ORACLE_LOADER
      DEFAULT DIRECTORY "XXWC_AR_AHH_CASH_RCPT_CONV_DIR"
      ACCESS PARAMETERS
      ( RECORDS DELIMITED BY NEWLINE CHARACTERSET WE8MSWIN1252
      SKIP 1
    BADFILE 'XXWC_AR_INVOICES_CONV.bad'
    DISCARDFILE 'XXWC_AR_INVOICES_CONV.dsc'
    FIELDS TERMINATED BY '|'
    OPTIONALLY ENCLOSED BY '"' AND '"'
    REJECT ROWS WITH ALL NULL FIELDS
                    )
      LOCATION
       ( "XXWC_AR_AHH_CASH_RCPT_CONV_DIR":'XXWC_AR_INVOICES_CONV.csv'
       )
    )
   REJECT LIMIT UNLIMITED ;