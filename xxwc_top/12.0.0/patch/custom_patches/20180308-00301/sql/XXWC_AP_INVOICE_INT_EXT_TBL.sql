 /********************************************************************************
  FILE NAME: XXWC.XXWC_AP_INVOICE_INT_EXT_TBL.sql

  PROGRAM TYPE: External Table script

  PURPOSE: Conversion purpose for AH Harries

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     05/16/2018    Pattabhi Avula  TMS#20180308-00301  -- Initial Version
  ********************************************************************************/
CREATE TABLE XXWC.XXWC_AP_INVOICE_INT_EXT_TBL
(
  INVOICE_ID                VARCHAR2(30 BYTE),
  INVOICE_NUM               VARCHAR2(50 BYTE),
  INVOICE_TYPE_LOOKUP_CODE  VARCHAR2(50 BYTE),
  INVOICE_DATE              VARCHAR2(20 BYTE),
  VENDOR_NUM                VARCHAR2(40 BYTE),
  VENDOR_NAME               VARCHAR2(240 BYTE),
  VENDOR_SITE_ID            VARCHAR2(30 BYTE),
  VENDOR_SITE_CODE          VARCHAR2(100 BYTE),
  INVOICE_AMOUNT            VARCHAR2(30 BYTE),
  INVOICE_CURRENCY_CODE     VARCHAR2(50 BYTE),
  TERMS_ID                  VARCHAR2(100 BYTE),
  DESCRIPTION               VARCHAR2(240 BYTE),
  AWT_GROUP_ID              VARCHAR2(30 BYTE),
  AWT_GROUP_NAME            VARCHAR2(25 BYTE),
  ATTRIBUTE_CATEGORY        VARCHAR2(140 BYTE),
  ATTRIBUTE1                VARCHAR2(140 BYTE),
  REJECT_CODE               VARCHAR2(500 BYTE),
  STATUS                    VARCHAR2(15 BYTE),
  TERMS_NAME                VARCHAR2(100 BYTE)
)
ORGANIZATION EXTERNAL
  (  TYPE ORACLE_LOADER
     DEFAULT DIRECTORY XXWC_AR_AHH_CASH_RCPT_CONV_DIR
     ACCESS PARAMETERS 
       ( RECORDS DELIMITED BY NEWLINE CHARACTERSET WE8MSWIN1252
      SKIP 1
    BADFILE 'XXWC_AP_OPEN_INV_LOAD.bad'
    DISCARDFILE 'XXWC_AP_OPEN_INV_LOAD.dsc'
    FIELDS TERMINATED BY '|'
    OPTIONALLY ENCLOSED BY '"' AND '"'
    REJECT ROWS WITH ALL NULL FIELDS
                      )
     LOCATION (XXWC_AR_AHH_CASH_RCPT_CONV_DIR:'XXWC_AP_OPEN_INV_LOAD.csv')
  )
REJECT LIMIT UNLIMITED;

GRANT SELECT ON XXWC.XXWC_AP_INVOICE_INT_EXT_TBL TO EA_APEX;