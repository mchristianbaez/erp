 /********************************************************************************
  FILE NAME: XXWC.XXWC_AP_SUPPLIER_SITES_CONV_T.sql

  PROGRAM TYPE: Table script

  PURPOSE: Conversion purpose for AH Harries

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)          DESCRIPTION
  ------- -----------   ---------------    --------------------------------------
  1.0     05/016/2018   Pattabhi Avula     TMS#20180308-00301   -- Initial Version
  ********************************************************************************/ 
  CREATE TABLE XXWC.XXWC_AP_SUPPLIER_SITES_CONV_T 
   (VENDOR_NUMBER 					VARCHAR2(30), 
	ORG_ID 							NUMBER, 
	PURCHASING_SITE_FLAG 			VARCHAR2(1), 
	PAY_SITE_FLAG    				VARCHAR2(1), 
	VENDOR_SITE_CODE 				VARCHAR2(100), 
	ADDRESS_LINE1 					VARCHAR2(240), 
	ADDRESS_LINE2 					VARCHAR2(240), 
	ADDRESS_LINE3 					VARCHAR2(240), 
	CITY  							VARCHAR2(60), 
	STATE 							VARCHAR2(150), 
	ZIP     						VARCHAR2(60), 
	COUNTY  						VARCHAR2(150), 
	COUNTRY 						VARCHAR2(60), 
	PHONE   						VARCHAR2(15), 
	FAX     						VARCHAR2(15), 
	ACCTS_PAY_CODE_COMBINATION_ID 	VARCHAR2(20), 
	PREPAY_CODE_COMBINATION_ID 		VARCHAR2(20), 
	PAYMENT_METHOD_LOOKUP_CODE 		VARCHAR2(20), 
	EXCLUSIVE_PAYMENT_FLAG 			VARCHAR2(1), 
	TERMS_NAME 						VARCHAR2(30), 
	PAY_GROUP_LOOKUP_CODE 			VARCHAR2(30), 
	MATCH_OPTION 					VARCHAR2(25), 
	DISTRIBUTION_SET_NAME 			VARCHAR2(50), 
	HOLD_UNMATCHED_INVOICES_FLAG 	VARCHAR2(1), 
	TAX_REPORTING_SITE_FLAG 		VARCHAR2(1), 
	STATUS 							VARCHAR2(20), 
	REJECT_CODE 					VARCHAR2(500)
   );
   /