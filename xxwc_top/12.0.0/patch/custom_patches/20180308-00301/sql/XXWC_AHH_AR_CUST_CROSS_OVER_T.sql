 /********************************************************************************
  FILE NAME: XXWC.XXWC_AHH_AR_CUST_CROSS_OVER_T
  PROGRAM TYPE: Table script

  PURPOSE: Conversion purpose for AH Harries

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     05/16/2018    Nancy Pahwa  TMS#20180308-00301  -- Initial Version
  ********************************************************************************/
CREATE TABLE "XXWC"."XXWC_AHH_AR_CUST_CROSS_OVER_T" 
   (	"CUST_NUM" VARCHAR2(20), 
	"CUST_SITE" VARCHAR2(50), 
	"CUST_NAME" VARCHAR2(240), 
	"CUST_ADD_LINE1" VARCHAR2(240), 
	"ORACLE_CUST_NUM" VARCHAR2(20)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "XXWC_DATA" ;