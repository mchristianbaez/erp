 /********************************************************************************
  FILE NAME: XXWC.XXWC_AP_TRANSACT_TYPE_STG_TBL.sql

  PROGRAM TYPE: Table script

  PURPOSE: Conversion purpose for AH Harries

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)          DESCRIPTION
  ------- -----------   ---------------    --------------------------------------
  1.0     05/016/2018   Pattabhi Avula     TMS#20180308-00301   -- Initial Version
  ********************************************************************************/  
  CREATE TABLE XXWC.XXWC_AP_TRANSACT_TYPE_STG_TBL 
   (AHH_TRANSACTION_TYPE 		VARCHAR2(50), 
	ORACLE_TRANSACTION_TYPE 	VARCHAR2(25), 
	CREATION_DATE 				DATE, 
	CREATED_BY 					NUMBER, 
	LAST_UPDATED_BY 			NUMBER, 
	LAST_UPDATE_DATE 			DATE, 
	LAST_UPDATE_LOGIN 			NUMBER
   );
   /