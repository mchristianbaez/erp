 /********************************************************************************
  FILE NAME: xxwc.XXWC_ONHAND_BAL_CNV
  PROGRAM TYPE: Alter Table script

  PURPOSE: Conversion purpose for AH Harries

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     05/16/2018    Nancy Pahwa  TMS#20180308-00301  -- Initial Version
  ********************************************************************************/
-- Add/modify columns
truncate table xxwc.XXWC_ONHAND_BAL_CNV;
alter table xxwc.XXWC_ONHAND_BAL_CNV modify transaction_quantity varchar2(40);
alter table xxwc.XXWC_ONHAND_BAL_CNV modify transaction_cost varchar2(20);
