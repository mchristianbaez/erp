 /********************************************************************************
  FILE NAME: XXWC.XXWC_AP_PAID_INVOICE_STG.sql

  PROGRAM TYPE: Table script

  PURPOSE: Conversion purpose for AH Harries

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)          DESCRIPTION
  ------- -----------   ---------------    --------------------------------------
  1.0     05/016/2018   Pattabhi Avula     TMS#20180308-00301   -- Initial Version
  ********************************************************************************/
  CREATE TABLE XXWC.XXWC_AP_PAID_INVOICE_STG 
   (INVOICE_NUM 		VARCHAR2(30), 
	VENDOR_NUM 			VARCHAR2(30), 
	INVOICE_AMOUNT 		NUMBER(38,0), 
	INVOICE_DATE 		DATE, 
	VENDOR_SITE_CODE 	VARCHAR2(100), 
	STATUS 				VARCHAR2(30), 
	REJECT_CODE 		VARCHAR2(500), 
	ORG_ID 				VARCHAR2(10) DEFAULT NVL(TO_NUMBER(DECODE(SUBSTRB(USERENV('CLIENT_INFO'),1,1), ' ', NULL,SUBSTRB(USERENV('CLIENT_INFO'),1,10))),162)
   );
   /