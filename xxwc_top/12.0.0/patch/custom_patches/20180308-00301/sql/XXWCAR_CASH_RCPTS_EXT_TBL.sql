 /********************************************************************************
  FILE NAME: "XXWC"."XXWCAR_CASH_RCPTS_EXT_TBL" 

  PROGRAM TYPE: External Table script

  PURPOSE: Conversion purpose for AH Harries

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     05/16/2018    Nancy Pahwa  TMS#20180308-00301  -- Initial Version
  ********************************************************************************/
CREATE TABLE "XXWC"."XXWCAR_CASH_RCPTS_EXT_TBL" 
   (  "DEPOSIT_DATE" VARCHAR2(100), 
  "OPERATOR_CODE" VARCHAR2(15), 
  "CHECK_NBR" VARCHAR2(20), 
  "CHECK_AMT" VARCHAR2(100), 
  "URL_LINK" VARCHAR2(200), 
  "CUSTOMER_NBR" VARCHAR2(30), 
  "INVOICE_NBR" VARCHAR2(30), 
  "FILL" VARCHAR2(15), 
  "INVOICE_AMT" VARCHAR2(100), 
  "DISC_AMT" VARCHAR2(100), 
  "GL_ACCT_NBR" VARCHAR2(10), 
  "SHORT_PAY_CODE" VARCHAR2(30), 
  "BILL_TO_LOCATION" VARCHAR2(100), 
  "COMMENTS" VARCHAR2(100), 
  "SHORT_CODE_INTERFACED" VARCHAR2(1), 
  "SHORT_CODE_INT_DATE" VARCHAR2(100), 
  "RECEIPT_TYPE" VARCHAR2(15), 
  "ORACLE_ACCOUNT_NBR" VARCHAR2(164), 
  "CUST_ACCOUNT_ID" VARCHAR2(100), 
  "ORACLE_FLAG" VARCHAR2(128), 
  "RECEIPT_ID" VARCHAR2(100), 
  "ABA_NUMBER" VARCHAR2(30), 
  "BANK_ACCOUNT_NUM" VARCHAR2(100), 
  "ORG_ID" VARCHAR2(100)
   ) 
   ORGANIZATION EXTERNAL 
    ( TYPE ORACLE_LOADER
      DEFAULT DIRECTORY "XXWC_AR_AHH_CASH_RCPT_CONV_DIR"
      ACCESS PARAMETERS
      ( RECORDS DELIMITED BY NEWLINE CHARACTERSET WE8MSWIN1252
      SKIP 1
    BADFILE 'XXWCAR_CASH_RCPTS_TBL.bad'
    DISCARDFILE 'XXWCAR_CASH_RCPTS_TBL.dsc'
    FIELDS TERMINATED BY '|'
    OPTIONALLY ENCLOSED BY '"' AND '"'
    REJECT ROWS WITH ALL NULL FIELDS
                                )
      LOCATION
       ( "XXWC_AR_AHH_CASH_RCPT_CONV_DIR":'XXWCAR_CASH_RCPTS_TBL.csv'
       )
    )
   REJECT LIMIT UNLIMITED ;