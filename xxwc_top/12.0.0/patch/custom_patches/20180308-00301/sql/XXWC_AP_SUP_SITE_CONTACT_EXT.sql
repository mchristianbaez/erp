 /********************************************************************************
  FILE NAME: XXWC.XXWC_AP_SUP_SITE_CONTACT_EXT.sql

  PROGRAM TYPE: External Table script

  PURPOSE: Conversion purpose for AH Harries

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     05/16/2018    Pattabhi Avula  TMS#20180308-00301  -- Initial Version
  ********************************************************************************/
  CREATE TABLE XXWC.XXWC_AP_SUP_SITE_CONTACT_EXT
   (VENDOR_NUMBER		 VARCHAR2(20), 
	VENDOR_SITE_CODE	 VARCHAR2(100), 
	FIRST_NAME			 VARCHAR2(50), 
	LAST_NAME			 VARCHAR2(100), 
	AREA_CODE			 VARCHAR2(30), 
	PHONE				 VARCHAR2(40), 
	FAX					 VARCHAR2(40), 
	EMAIL_ADDRESS		 VARCHAR2(2000), 
	STATUS				 VARCHAR2(30), 
	REJECT_CODE			 VARCHAR2(500)
   ) 
   ORGANIZATION EXTERNAL 
    ( TYPE ORACLE_LOADER
      DEFAULT DIRECTORY "XXWC_AR_AHH_CASH_RCPT_CONV_DIR"
      ACCESS PARAMETERS
      ( RECORDS DELIMITED BY NEWLINE CHARACTERSET WE8MSWIN1252
      SKIP 1
    BADFILE 'XXWC_AP_SUP_SITES_CONTACT_LOAD.bad'
    DISCARDFILE 'XXWC_AP_SUP_SITES_CONTACT_LOAD.dsc'
    FIELDS TERMINATED BY '|'
    OPTIONALLY ENCLOSED BY '"' AND '"'
    REJECT ROWS WITH ALL NULL FIELDS
          )
      LOCATION
       ( "XXWC_AR_AHH_CASH_RCPT_CONV_DIR":'XXWC_AP_SUP_SITES_CONTACT_LOAD.csv'
       )
    )
   REJECT LIMIT UNLIMITED;
   /