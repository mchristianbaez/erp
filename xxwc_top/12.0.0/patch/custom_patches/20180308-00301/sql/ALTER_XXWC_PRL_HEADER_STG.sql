 /********************************************************************************
  FILE NAME: xxwc.XXWC_PRL_HEADER_STG
  PROGRAM TYPE: Alter Table script

  PURPOSE: Conversion purpose for AH Harries

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     05/16/2018    Nancy Pahwa  TMS#20180308-00301  -- Initial Version
  ********************************************************************************/
-- Add/modify columns
alter table xxwc.XXWC_PRL_HEADER_STG add REQUEST_ID NUMBER;
