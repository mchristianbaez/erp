 /********************************************************************************
  FILE NAME: XXWC.XXWC_AHH_AP_CONV_GRANTS_TO_APEX_USER.sql

  PROGRAM TYPE: Grant scripts

  PURPOSE: AP Conversion purpose in APEX application

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     05/09/2018    Pattabhi Avula  TMS#20180308-00301  -- Initial Version
  ********************************************************************************/
  GRANT READ, WRITE ON DIRECTORY XXWC_AR_AHH_CASH_RCPT_CONV_DIR TO XXWC
  /
  GRANT EXECUTE, READ, WRITE ON DIRECTORY XXWC_AR_AHH_CASH_RCPT_CONV_DIR TO EA_APEX 
  /
  GRANT SELECT,INSERT,UPDATE,DELETE ON XXWC.XXWC_AP_BRANCH_LIST_STG_TBL TO EA_APEX
  /
  GRANT SELECT ON XXWC.XXWC_AP_BRANCH_LIST_EXT_TBL TO EA_APEX
  /
  GRANT SELECT,INSERT,UPDATE,DELETE ON XXWC.XXWC_AP_GL_MAP_STG_TBL TO EA_APEX
  /
  GRANT SELECT ON XXWC.XXWC_AP_GL_MAP_EXT_TBL TO EA_APEX
  /
  GRANT SELECT ON XXWC.XXWC_AP_INVOICE_INT_EXT_TBL TO EA_APEX
  /
  GRANT SELECT,INSERT,UPDATE,DELETE ON XXWC.XXWC_AP_PAID_INVOICE_STG TO EA_APEX
  /
  GRANT SELECT ON XXWC.XXWC_AP_PAID_INVOICE_EXT_TBL TO EA_APEX
  /
  GRANT SELECT,INSERT,UPDATE,DELETE ON XXWC.XXWC_AP_PMT_TERMS_STG_T TO EA_APEX
  /
  GRANT SELECT ON XXWC.XXWC_AP_PMT_TERMS_EXT_TBL TO EA_APEX
  /
  GRANT SELECT,INSERT,UPDATE,DELETE ON XXWC.XXWC_AP_SUP_SITE_CONT_CONV_T TO EA_APEX
  /
  GRANT SELECT ON XXWC.XXWC_AP_SUP_SITE_CONTACT_EXT TO EA_APEX
  /
  GRANT ALTER ON XXWC.XXWC_AP_SUP_SITES_XREF_EXT_TBL TO EA_APEX
  /
  GRANT SELECT ON XXWC.XXWC_AP_SUP_SITES_XREF_EXT_TBL TO EA_APEX
  /
  GRANT SELECT ON XXWC.XXWC_AP_SUP_XREF_EXT_TBL TO EA_APEX
  /
  GRANT ALTER ON XXWC.XXWC_AP_SUP_XREF_EXT_TBL TO EA_APEX
  /
  GRANT SELECT ON XXWC.XXWC_AP_SUPPLIER_EXT_TBL TO EA_APEX
  /
  GRANT SELECT ON XXWC.XXWC_AP_SUPPLIER_INT_EXT_TBL TO EA_APEX
  /
  GRANT SELECT,INSERT,UPDATE,DELETE ON XXWC.XXWC_AP_SUPPLIER_REF_TBL TO EA_APEX
  /
  GRANT SELECT,INSERT,UPDATE,DELETE ON XXWC.XXWC_AP_SUPPLIER_SITES_CONV_T TO EA_APEX
  /
  GRANT SELECT ON XXWC.XXWC_AP_SUPPLIER_SITES_EXT_TBL TO EA_APEX
  /
  GRANT SELECT,INSERT,UPDATE,DELETE ON XXWC.XXWC_AP_SUPPLIER_SITES_REF_TBL TO EA_APEX
  /
  GRANT SELECT,INSERT,UPDATE,DELETE ON XXWC.XXWC_AP_SUPPLIER_STG_TBL TO EA_APEX
  /
  GRANT SELECT,INSERT,UPDATE,DELETE ON XXWC.XXWC_AP_TRANSACT_TYPE_STG_TBL TO EA_APEX
  /
  GRANT SELECT ON XXWC.XXWC_AP_TRANSACT_TYPE_EXT_TBL TO EA_APEX
  /
  GRANT SELECT,INSERT,UPDATE,DELETE ON XXWC.XXWC_AP_SUPPLIER_INT TO EA_APEX
  /
  GRANT SELECT,INSERT,UPDATE,DELETE ON XXWC.XXWC_AP_SUPPLIER_SITES_INT  TO EA_APEX
  /
  GRANT SELECT,INSERT,UPDATE,DELETE ON XXWC.XXWC_AP_SUP_SITE_CONTACT_INT  TO EA_APEX
  /