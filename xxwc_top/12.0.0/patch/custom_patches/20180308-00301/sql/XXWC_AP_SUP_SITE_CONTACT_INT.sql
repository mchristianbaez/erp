 /********************************************************************************
  FILE NAME: "XXWC"."XXWC_AP_SUP_SITE_CONTACT_INT" 
  PROGRAM TYPE: Table script

  PURPOSE: Conversion purpose for AH Harries

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     05/16/2018    Nancy Pahwa  TMS#20180308-00301  -- Initial Version
  ********************************************************************************/
CREATE TABLE "XXWC"."XXWC_AP_SUP_SITE_CONTACT_INT" 
   (	"VENDOR_NUMBER" VARCHAR2(20), 
	"VENDOR_SITE_CODE" VARCHAR2(100), 
	"FIRST_NAME" VARCHAR2(50), 
	"LAST_NAME" VARCHAR2(100), 
	"AREA_CODE" VARCHAR2(30), 
	"PHONE" VARCHAR2(40), 
	"FAX" VARCHAR2(40), 
	"EMAIL_ADDRESS" VARCHAR2(2000), 
	"STATUS" VARCHAR2(30), 
	"REJECT_CODE" VARCHAR2(500), 
	"ORG_ID" NUMBER DEFAULT NVL(TO_NUMBER(DECODE(SUBSTRB(USERENV('CLIENT_INFO'),1,1), ' ', NULL,SUBSTRB(USERENV('CLIENT_INFO'),1,10))),-99)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 0 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "XXWC_DATA" ;