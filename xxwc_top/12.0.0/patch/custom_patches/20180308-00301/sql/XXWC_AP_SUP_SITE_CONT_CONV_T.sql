 /********************************************************************************
  FILE NAME: XXWC.XXWC_AP_SUP_SITE_CONT_CONV_T.sql

  PROGRAM TYPE: Table script

  PURPOSE: Conversion purpose for AH Harries

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)          DESCRIPTION
  ------- -----------   ---------------    --------------------------------------
  1.0     05/016/2018   Pattabhi Avula     TMS#20180308-00301   -- Initial Version
  ********************************************************************************/ 
  CREATE TABLE XXWC.XXWC_AP_SUP_SITE_CONT_CONV_T 
   (VENDOR_NUMBER 		VARCHAR2(20), 
	VENDOR_SITE_CODE 	VARCHAR2(100), 
	FIRST_NAME 			VARCHAR2(50), 
	LAST_NAME 			VARCHAR2(100), 
	AREA_CODE 			VARCHAR2(30), 
	PHONE 				VARCHAR2(40), 
	FAX 				VARCHAR2(40), 
	EMAIL_ADDRESS 		VARCHAR2(2000), 
	STATUS 				VARCHAR2(30), 
	REJECT_CODE 		VARCHAR2(500)
   );
   /