 /********************************************************************************
  FILE NAME: XXWC.XXWC_INVITEM_XREF_EXT_TBL

  PROGRAM TYPE: External Table script

  PURPOSE: Conversion purpose for AH Harries

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     05/16/2018    Nancy Pahwa  TMS#20180308-00301  -- Initial Version
  ********************************************************************************/
CREATE TABLE "XXWC"."XXWC_INVITEM_XREF_EXT_TBL" 
   (	"PARTNUMBER" VARCHAR2(30), 
	"XREF_PART" VARCHAR2(200), 
	"XREF_TYPE" VARCHAR2(200)
   ) 
   ORGANIZATION EXTERNAL 
    ( TYPE ORACLE_LOADER
      DEFAULT DIRECTORY "XXWC_AR_AHH_CASH_RCPT_CONV_DIR"
      ACCESS PARAMETERS
      ( RECORDS DELIMITED BY NEWLINE CHARACTERSET WE8MSWIN1252
      SKIP 1
    BADFILE 'XXWC_INVITEM_XREF_REF.bad'
    DISCARDFILE 'XXWC_INVITEM_XREF_REF.dsc'
    FIELDS TERMINATED BY '|'
    OPTIONALLY ENCLOSED BY '"' AND '"'
    REJECT ROWS WITH ALL NULL FIELDS
                        )
      LOCATION
       ( "XXWC_AR_AHH_CASH_RCPT_CONV_DIR":'XXWC_INVITEM_XREF_REF.csv'
       )
    )
   REJECT LIMIT UNLIMITED ;