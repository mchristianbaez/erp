 /********************************************************************************
  FILE NAME: XXWC.XXWC_AP_BRANCH_LIST_EXT_TBL.sql

  PROGRAM TYPE: External Table script

  PURPOSE: Conversion purpose for AH Harries

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     05/16/2018    Pattabhi Avula  TMS#20180308-00301  -- Initial Version
  ********************************************************************************/
  CREATE TABLE XXWC.XXWC_AP_BRANCH_LIST_EXT_TBL 
   (AHH_BRANCH_NUMBER		 VARCHAR2(50), 
	AHH_WAREHOUSE_NUMBER	 VARCHAR2(50), 
	ORACLE_BRANCH_NUMBER	 VARCHAR2(50), 
	ORACLE_BW_NUMBER		 VARCHAR2(50), 
	PROCESS_STATUS			 VARCHAR2(1)
   ) 
   ORGANIZATION EXTERNAL 
    ( TYPE ORACLE_LOADER
      DEFAULT DIRECTORY "XXWC_AR_AHH_CASH_RCPT_CONV_DIR"
      ACCESS PARAMETERS
      ( RECORDS DELIMITED BY NEWLINE CHARACTERSET WE8MSWIN1252
      SKIP 1
    BADFILE 'XXWC_AP_BRANCH_LIST_CONV_LOAD.bad'
    DISCARDFILE 'XXWC_AP_BRANCH_LIST_CONV_LOAD.dsc'
    FIELDS TERMINATED BY '|'
    OPTIONALLY ENCLOSED BY '"' AND '"'
    REJECT ROWS WITH ALL NULL FIELDS
                )
      LOCATION
       ( "XXWC_AR_AHH_CASH_RCPT_CONV_DIR":'XXWC_AP_BRANCH_LIST_CONV_LOAD.csv'
       )
    )
   REJECT LIMIT UNLIMITED;
   /