CREATE OR REPLACE PACKAGE BODY APPS.xxwc_ap_invoice_conv_pkg
IS
   /***************************************************************************
    *    script name: xxwc_ap_invoice_conv_pkg.pkb
    *
    *    interface / conversion name: Open/Closed AP Invoice Conversion.
    *
    *    functional purpose: convert AP Invoices using open interface tables.
    *
    *    history:
    *
    *    version    date              author       description
    **********************************************************************
    *    1.0        04-sep-2011   T.Rajaiah        Initial development
    *    1.1        23-Mar-2018   P.Vamshidhar     Added additional parameters to
    *                                              handle attachments.		 
	*    1.2        28-Mar-2018   Ashwin Sridhar   Added for TMS#20180328-00034--AH HARRIS AP Closed Invoices Conversion 
    *********************************************************************/
   g_source_name   VARCHAR2 (100); -- Added in Rev 1.1
   g_import_date   DATE;           -- Added in Rev 1.1

   PROCEDURE xxwc_apinvoice_hdr_valid (p_invoice_type             VARCHAR2,
                                       p_invoice_currency_code    VARCHAR2,
                                       p_invoice_date             DATE,
                                       p_vendor_site_id           NUMBER,
                                       p_terms_name               VARCHAR2,
                                       p_ccid                     NUMBER,
                                       p_pay_method               VARCHAR2)
   AS
      l_vendor_id              NUMBER (15);
      l_invoice_type           VARCHAR2 (15);
      l_currency_code          VARCHAR2 (10);
      l_vendor_site_id         NUMBER (15);
      invoice_type_exception   EXCEPTION;
      l_term_id                NUMBER (15);
      l_code_combination_id    NUMBER (15);
      l_vendor_site_code       VARCHAR2 (15);
      l_group_id               VARCHAR2 (80);
      l_vendor_num             VARCHAR2 (30);
      l_vendor_name            VARCHAR2 (240);
      l_inv_count              NUMBER (15);
      v_errorcode              NUMBER;
      v_errormessage           VARCHAR2 (240);
      l_oracle_term            VARCHAR2 (60);
      l_sob                    NUMBER (15);
      l_lookup_code            VARCHAR2 (30);
      l_lookcode               VARCHAR2 (30);
      l_pay                    VARCHAR2 (30) := NULL;
   BEGIN
      g_invoice_currency_code := NULL;

      BEGIN
         SELECT COUNT (*)
           INTO l_inv_count
           FROM gl_period_statuses
          WHERE     application_id = fnd_global.resp_appl_id
                AND set_of_books_id = l_sob
                AND (p_invoice_date) BETWEEN (start_date) AND (end_date)
                AND closing_status = 'O';

         IF l_inv_count > 0
         THEN
            NULL;
         ELSE
            NULL;
            --l_verify_flag := 'N';
            --l_error_message :=
            --      ',Invoice Date is not in Open Periods or future periods';
            NULL;
         END IF;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_verify_flag := 'N';
            l_error_message :=
               ',Invoice Date is Null or Exception in Open Period Validation';
      END;

      --
      BEGIN
         SELECT vendor_site_id
           INTO l_vendor_site_id
           FROM ap_supplier_sites_all
          WHERE vendor_site_id = p_vendor_site_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_verify_flag := 'N';
            l_error_message :=
               l_error_message || ',Vendor Site ID is not Valid';
      END;

      IF p_invoice_currency_code IS NULL
      THEN
         BEGIN
            SELECT invoice_currency_code
              INTO g_invoice_currency_code
              FROM ap_supplier_sites_all
             WHERE vendor_site_id = p_vendor_site_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_verify_flag := 'N';
               l_error_message :=
                     l_error_message
                  || ',Currency code is null from Legazy  is not valid in Suppliers table';
         END;
      ELSE
         BEGIN
            g_invoice_currency_code := p_invoice_currency_code;

            SELECT currency_code
              INTO l_currency_code
              FROM fnd_currencies
             WHERE currency_code = p_invoice_currency_code;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_verify_flag := 'N';
               l_error_message :=
                  l_error_message || ',Currency code has other errors';
         END;
      END IF;

      IF p_terms_name IS NULL
      THEN
         BEGIN
            IF p_invoice_type = 'CREDIT'
            THEN
               SELECT term_id
                 INTO g_terms_id
                 FROM ap_terms
                WHERE name = 'DUE UPON RECEIPT';
            ELSE
               SELECT terms_id
                 INTO g_terms_id
                 FROM ap_supplier_sites_all
                WHERE vendor_site_id = p_vendor_site_id;

               IF g_terms_id IS NULL
               THEN
                  l_verify_flag := 'N';
                  l_error_message :=
                     l_error_message || ',Payment Terms not valid';
               END IF;
            END IF;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_verify_flag := 'N';
               l_error_message :=
                     l_error_message
                  || ',Payment Terms is null from Legazy  not valid from suppliers table';
         END;
      ELSE
         IF p_invoice_type = 'CREDIT'
         THEN
            SELECT term_id
              INTO g_terms_id
              FROM ap_terms
             WHERE name = 'DUE UPON RECEIPT';
         ELSE
            BEGIN
               --Added in 1.2
               BEGIN
                  SELECT a.oracle_terms_id
                    INTO g_terms_id
                    FROM XXWC.XXWC_AP_PMT_TERMS_STG_T a
                   WHERE TRIM (UPPER (a.ahh_terms_id)) =
                            TRIM (UPPER (p_terms_name));
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     g_terms_id := 10017;
               END;
            -- Commented in 1.2
            /*		 

               SELECT description
                 INTO l_oracle_term
                 FROM fnd_lookup_values
                WHERE     lookup_type = 'XXWC_AP_TERMS_MAPPING_CONV'
                      AND TRIM (UPPER (lookup_code)) =
                             TRIM (UPPER (p_terms_name));

               SELECT term_id
                 INTO g_terms_id
                 FROM ap_terms
                WHERE TRIM (UPPER (NAME)) = TRIM (UPPER (l_oracle_term));
		    */		
            /*
                SELECT   oracle_term
                  INTO   l_oracle_term
                  FROM   xxwc_ap_payment_terms_map
                 WHERE   legacy_term = TRIM (p_terms_name);

                SELECT   term_id
                  INTO   g_terms_id
                  FROM   ap_terms
                 WHERE   name = l_oracle_term;  */
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_verify_flag := 'N';
                  l_error_message :=
                     l_error_message || ',Invalid Payment Terms';
            END;
         END IF;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         v_errorcode := SQLCODE;
         v_errormessage := SUBSTR (SQLERRM, 1, 200);
         fnd_file.put_line (
            fnd_file.LOG,
            'Exception Block of Invoice Header Validation Procedure');
         fnd_file.put_line (fnd_file.LOG,
                            'The Error Code Traced is : ' || v_errorcode);
         fnd_file.put_line (
            fnd_file.LOG,
            'The Error Message Traced is : ' || v_errormessage);
   END xxwc_apinvoice_hdr_valid;

   PROCEDURE xxwc_apinvoice_lines_valid (p_line_type_lookup_code    VARCHAR2,
                                         p_ccid                     NUMBER)
   AS
      l_line_type             VARCHAR2 (15);
      l_code_combination_id   NUMBER (15);
      v_errorcode             NUMBER;
      v_errormessage          VARCHAR2 (240);
   BEGIN
      BEGIN
         SELECT lookup_code
           INTO l_line_type
           FROM ap_lookup_codes
          WHERE     lookup_type(+) = 'INVOICE DISTRIBUTION TYPE'
                AND lookup_code = p_line_type_lookup_code;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_verify_flag := 'N';
            l_error_message :=
               l_error_message || ',Invoice Line Type not found...';
         WHEN OTHERS
         THEN
            l_verify_flag := 'N';
            l_error_message :=
               l_error_message || ',Invoice Line Type has other errors';
      END;
   EXCEPTION
      WHEN OTHERS
      THEN
         v_errorcode := SQLCODE;
         v_errormessage := SUBSTR (SQLERRM, 1, 200);
         fnd_file.put_line (
            fnd_file.LOG,
            'Exception Block of Invoice Lines Validation Procedure');
         fnd_file.put_line (fnd_file.LOG,
                            'The Error Code Traced is : ' || v_errorcode);
         fnd_file.put_line (
            fnd_file.LOG,
            'The Error Message Traced is : ' || v_errormessage);
   END xxwc_apinvoice_lines_valid;

   PROCEDURE xxwc_stage_to_interface
   AS
      l_hdrcnt                NUMBER;
      l_hdrcnt2               NUMBER := 0;
      l_linescnt              NUMBER;
      l_linescnt2             NUMBER := 0;
      l_code_combination_id   NUMBER (15);
      v_errorcode             NUMBER;
      v_errormessage          VARCHAR2 (240);
      l_open_flag1            VARCHAR2 (1);
      l_roll_flag             VARCHAR2 (1);
      l_tblcnt                NUMBER (15);
      l_status                VARCHAR2 (15);
      l_vendor_site_id        NUMBER (15);
      l_terms_id              NUMBER (15);
      l_ccid                  NUMBER;
      l_vendor_id             NUMBER;
      l_invoice_type          VARCHAR2 (20);
      l_invoice_id            NUMBER;
      l_pay                   VARCHAR2 (30);
      l_oracle_term           VARCHAR2 (50);

      TYPE invoivelinesrecord IS RECORD
      (
         status        VARCHAR2 (20),
         reject_code   VARCHAR2 (30),
         r_id          VARCHAR2 (100)
      );

      TYPE invline_tbl IS TABLE OF invoivelinesrecord
         INDEX BY BINARY_INTEGER;

      invlinerecords          invline_tbl;
      dmy_invlinerecords      invline_tbl;

      CURSOR cur_hdr
      IS
             SELECT c.ROWID r_id, c.*
               FROM xxwc.xxwc_ap_invoice_int c
              WHERE NVL (status, 'XXXXX') <> 'PROCESSED' AND invoice_amount <> 0
         FOR UPDATE OF status, reject_code;
   BEGIN
      FOR rec_hdr IN cur_hdr
      LOOP
         l_verify_flag := 'Y';
         l_error_message := NULL;
         l_invoice_type := NULL;
         l_vendor_id := NULL;
         l_vendor_site_id := NULL;
         l_ccid := NULL;
         l_pay := NULL;

         /*Deriving Vendor_ID and Vendor_Site_ID*/
         BEGIN
            SELECT vendor_id
              INTO l_vendor_id
              FROM ap_suppliers
             WHERE TRIM (UPPER (attribute1)) =
                      TRIM (UPPER (rec_hdr.vendor_num));
         EXCEPTION
            WHEN OTHERS
            THEN
               l_verify_flag := 'N';
               l_error_message := 'Vendor Number is Not Valid';
         END;

         BEGIN
            SELECT vendor_site_id, accts_pay_code_combination_id --, payment_method_lookup_code
              INTO l_vendor_site_id, l_ccid                          --, l_pay
              FROM ap_supplier_sites_all
             WHERE     vendor_site_code = TRIM (rec_hdr.vendor_site_code)
                   AND vendor_id = l_vendor_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_verify_flag := 'N';
               l_error_message :=
                  l_error_message || 'Vendor Site Code is Not Valid';
         END;

         BEGIN
            SELECT default_payment_method_code
              INTO l_pay
              FROM iby_external_payees_all
             WHERE     supplier_site_id = l_vendor_site_id
                   AND default_payment_method_code IS NOT NULL;

            IF l_pay IS NULL
            THEN
               --l_verify_flag := 'N';
               l_pay := 'CHECK';
               l_error_message := l_error_message || 'Missing Payment Method';
            END IF;
         EXCEPTION
            WHEN OTHERS
            THEN
               --l_verify_flag := 'N';
               l_pay := 'CHECK';
               l_error_message :=
                  l_error_message || 'Default Payment Method N/A';
         END;

         IF TO_NUMBER (rec_hdr.invoice_amount) < 0
         THEN
            l_invoice_type := 'CREDIT';
         ELSE
            l_invoice_type := 'STANDARD';
         END IF;

         IF TRIM (rec_hdr.invoice_num) IS NULL
         THEN
            l_verify_flag := 'N';
            l_error_message :=
               l_error_message || 'Invoice num should not be null';
         ELSE
            BEGIN
               SELECT invoice_id
                 INTO l_invoice_id
                 FROM ap_invoices_all
                WHERE     invoice_num = TRIM (rec_hdr.invoice_num)
                      AND vendor_id = l_vendor_id;

               l_verify_flag := 'N';
               l_error_message := l_error_message || 'Duplicate Invoice Num';
            EXCEPTION
               WHEN OTHERS
               THEN
                  NULL;
            END;
         END IF;


         /*Calling Validation Procedure based on parameter selected*/
         IF rec_hdr.terms_name IS NULL
         THEN
            BEGIN
               IF l_invoice_type = 'CREDIT'
               THEN
                  SELECT term_id
                    INTO g_terms_id
                    FROM ap_terms
                   WHERE name = 'DUE UPON RECEIPT';
               ELSE
                  SELECT terms_id
                    INTO g_terms_id
                    FROM ap_supplier_sites_all
                   WHERE vendor_site_id = l_vendor_site_id;

                  IF g_terms_id IS NULL
                  THEN
                     l_verify_flag := 'N';
                     l_error_message :=
                        l_error_message || ',Payment Terms not valid';
                  END IF;
               END IF;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_verify_flag := 'N';
                  l_error_message :=
                        l_error_message
                     || ',Payment Terms is null from Legazy  not valid from suppliers table';
            END;
         ELSE
            IF l_invoice_type = 'CREDIT'
            THEN
               SELECT term_id
                 INTO g_terms_id
                 FROM ap_terms
                WHERE name = 'DUE UPON RECEIPT';
            ELSE
               BEGIN
			   
                  --Added in 1.2
                  BEGIN
                     SELECT a.oracle_terms_id
                       INTO g_terms_id
                       FROM XXWC.XXWC_AP_PMT_TERMS_STG_T a
                      WHERE TRIM (UPPER (a.ahh_terms_id)) =
                               TRIM (UPPER (rec_hdr.terms_name));
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        g_terms_id := 10017;
                  END;
               --Commented in 1.2
                /*			   
                  SELECT description
                    INTO l_oracle_term
                    FROM fnd_lookup_values
                   WHERE     lookup_type = 'XXWC_AP_TERMS_MAPPING_CONV'
                         AND TRIM (UPPER (lookup_code)) =
                                TRIM (UPPER (rec_hdr.terms_name));

                  SELECT term_id
                    INTO g_terms_id
                    FROM ap_terms
                   WHERE TRIM (UPPER (NAME)) = TRIM (UPPER (l_oracle_term));
				 */  
               /*
                   SELECT   oracle_term
                     INTO   l_oracle_term
                     FROM   xxwc_ap_payment_terms_map
                    WHERE   legacy_term = TRIM (rec_hdr.terms_name);

                   SELECT   term_id
                     INTO   g_terms_id
                     FROM   ap_terms
                    WHERE   name = l_oracle_term;  */
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_verify_flag := 'N';
                     l_error_message :=
                        l_error_message || ',Invalid Payment Terms';
               END;
            END IF;
         END IF;

         IF g_validate_only = 'Y'
         THEN
            IF l_verify_flag != 'N'
            THEN
               UPDATE XXWC.XXWC_AP_INVOICE_INT
                  SET status = 'VALIDATED', reject_code = l_error_message
                WHERE ROWID = rec_hdr.r_id;
            ELSE
               UPDATE xxwc.xxwc_ap_invoice_int
                  SET status = 'REJECTED', reject_code = l_error_message
                WHERE ROWID = rec_hdr.r_id;
            END IF;
         END IF;

         /*If Validate Only parameter is not selected then both Validation and Import is done*/
         IF g_validate_only != 'Y'
         THEN
            IF l_verify_flag != 'N'
            THEN
               INSERT INTO ap_invoices_interface (invoice_id,
                                                  invoice_num,
                                                  invoice_type_lookup_code,
                                                  invoice_date,
                                                  vendor_id,
                                                  invoice_amount,
                                                  invoice_currency_code,
                                                  terms_id,
                                                  last_update_date,
                                                  last_updated_by,
                                                  creation_date,
                                                  created_by,
                                                  gl_date,
                                                  org_id,
                                                  source,
                                                  vendor_site_id,
                                                  GROUP_ID,
                                                  payment_currency_code,
                                                  exclusive_payment_flag,
                                                  terms_date,
                                                  payment_method_code,
                                                  attribute_category,
                                                  attribute1)
                       VALUES (
                                 ap_invoices_interface_s.NEXTVAL,
                                 TRIM (rec_hdr.invoice_num),
                                 l_invoice_type,
                                 rec_hdr.invoice_date,
                                 l_vendor_id,
                                 TO_NUMBER (rec_hdr.invoice_amount),
                                 'USD',
                                 g_terms_id,
                                 SYSDATE,
                                 -1,
                                 SYSDATE,
                                 -1,
                                 SYSDATE, --add_months(last_day(SYSDATE),-1)-6,
                                 xxwc_ascp_scwb_pkg.get_wc_org_id, --fnd_global.org_id,
                                 'MAS-CONVERSION',
                                 l_vendor_site_id,
                                 'NEW',
                                 'USD',
                                 'N',
                                 rec_hdr.invoice_date,
                                 l_pay,
                                 rec_hdr.attribute_category,
                                 RTRIM (
                                    REPLACE (
                                       REPLACE (rec_hdr.attribute1,
                                                CHR (13),
                                                ' '),
                                       CHR (10),
                                       ' '))              --rec_hdr.attribute1
                                            );

               INSERT
                 INTO ap_invoice_lines_interface (invoice_id,
                                                  invoice_line_id,
                                                  line_number,
                                                  line_type_lookup_code,
                                                  amount,
                                                  accounting_date,
                                                  dist_code_combination_id,
                                                  last_updated_by,
                                                  last_update_date,
                                                  created_by,
                                                  creation_date,
                                                  org_id)
               VALUES (ap_invoices_interface_s.CURRVAL,
                       ap_invoice_lines_interface_s.NEXTVAL,
                       1,
                       'ITEM',
                       TO_NUMBER (rec_hdr.invoice_amount),
                       SYSDATE, --add_months(last_day(SYSDATE),-1)-6,                --rec_hdr.invoice_date
                       g_ccid,
                       -1,
                       SYSDATE,
                       -1,
                       SYSDATE,
                       xxwc_ascp_scwb_pkg.get_wc_org_id    --fnd_global.org_id
                                                       );

               /*Updating the Status*/
               UPDATE xxwc.xxwc_ap_invoice_int
                  SET status = 'PROCESSED', reject_code = l_error_message
                WHERE ROWID = rec_hdr.r_id;
            ELSE
               UPDATE xxwc.xxwc_ap_invoice_int
                  SET status = 'REJECTED', reject_code = l_error_message
                WHERE ROWID = rec_hdr.r_id;
            END IF;
         END IF;
      --- end if;
      END LOOP;

      COMMIT;

      /*Call to Standard Program when Submit Standard Program parameter is selected*/
      IF g_submit = 'Y'
      THEN
         xxwc_apinvoice_standard_prog;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         v_errorcode := SQLCODE;
         v_errormessage := SUBSTR (SQLERRM, 1, 200);
         fnd_file.put_line (
            fnd_file.LOG,
            'Exception Block of Stage to Interface Import Procedure');
         fnd_file.put_line (fnd_file.LOG,
                            'The Error Code Traced is : ' || v_errorcode);
         fnd_file.put_line (
            fnd_file.LOG,
            'The Error Message Traced is : ' || v_errormessage);
   END xxwc_stage_to_interface;

   PROCEDURE xxwc_apinvoice_standard_prog
   AS
      v_conc_request_id   NUMBER;
      l_request_id1       NUMBER;
      v_phase             VARCHAR2 (100);
      v_status            VARCHAR2 (100);
      v_dev_phase         VARCHAR2 (100);
      v_dev_status        VARCHAR2 (100);
      v_message           VARCHAR2 (2000);
      v_wait_outcome      BOOLEAN;
      v_errorcode         NUMBER;
      v_errormessage      VARCHAR2 (240);
   BEGIN
      l_request_id1 :=
         apps.fnd_request.submit_request (
            'SQLAP',                                 -- Applicaiton Short Name
            'APXIIMPT',                       -- Concurrent Program Short Name
            'Payables Open Interface Import',    -- Description of the request
            TO_CHAR (SYSDATE, 'DD-MON-RR HH24:MI:SS'),
            FALSE,
            xxwc_ascp_scwb_pkg.get_wc_org_id,             --fnd_global.org_id,
            'MAS-CONVERSION',
            'NEW',                                                     --Group
            'MAS-CONVERSION ' || TO_CHAR (SYSDATE, 'YYYYMMDD HH24:MI:SS'), --Batch Name
            NULL,                                                  --Hold Name
            NULL,                                                --Hold Reason
            NULL,                                                    --GL Date
            'N',                                                       --Purge
            NULL,                                               --Trace Switch
            NULL,                                               --Debug Switch
            NULL,                                           --Summarize Report
            NULL,                                          --Commit Batch Size
            NULL,                                                    --User ID
            NULL                                                    --Login ID
                );
      COMMIT;
      v_wait_outcome :=
         fnd_concurrent.wait_for_request (request_id   => l_request_id1,
                                          interval     => 10,
                                          phase        => v_phase,
                                          status       => v_status,
                                          dev_phase    => v_dev_phase,
                                          dev_status   => v_dev_status,
                                          MESSAGE      => v_message);
      g_request_id := l_request_id1;
      g_dev_phase := v_dev_phase;
      g_dev_status := v_dev_status;

      /* Call to Document attachment Procedure If Standard program is completed Normal*/
      IF UPPER (v_dev_phase) = 'COMPLETE' AND UPPER (v_dev_status) = 'NORMAL'
      THEN
         xxwc_ap_invoice_conv_pkg.xxwc_doc_attach_proc;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         v_errorcode := SQLCODE;
         v_errormessage := SUBSTR (SQLERRM, 1, 200);
         g_errorcode := v_errorcode;
         g_errormessage := v_errormessage;
         fnd_file.put_line (
            fnd_file.LOG,
            'Exception Block of Standard Program Call Procedure');
         fnd_file.put_line (fnd_file.LOG,
                            'The Error Code Traced is : ' || v_errorcode);
         fnd_file.put_line (
            fnd_file.LOG,
            'The Error Message Traced is : ' || v_errormessage);
   END xxwc_apinvoice_standard_prog;

   PROCEDURE xxwc_doc_attach_proc
   AS
      v_errorcode              NUMBER;
      v_errormessage           VARCHAR2 (240);
      l_rowid                  ROWID;
      l_attached_document_id   NUMBER;
      l_document_id            NUMBER;
      l_media_id               NUMBER;
      l_category_id            NUMBER := 1;
      l_pk1_value              fnd_attached_documents.pk1_value%TYPE;
      l_description            fnd_documents_tl.description%TYPE
                                  := 'MAS-CONVERSION,Document Attach';
      l_seq_num                NUMBER;
      l_documentid             NUMBER;

      CURSOR cur_hdr
      IS
         SELECT *
           FROM ap_invoices_all
          WHERE     attribute1 IS NOT NULL
                AND SOURCE = g_source_name  -- Added in Rev 1.1
                AND TRUNC (CREATION_DATE) = TRUNC (g_import_date); -- Added in Rev 1.1
   BEGIN

      FOR rec_hdr IN cur_hdr
      LOOP
         l_pk1_value := rec_hdr.invoice_id;

         SELECT fnd_documents_s.NEXTVAL INTO l_document_id FROM DUAL;

         SELECT fnd_attached_documents_s.NEXTVAL
           INTO l_attached_document_id
           FROM DUAL;

         SELECT NVL (MAX (seq_num), 0) + 10
           INTO l_seq_num
           FROM fnd_attached_documents
          WHERE pk1_value = l_pk1_value AND entity_name = 'AP_INVOICES';

         fnd_documents_pkg.insert_row (
            x_rowid               => l_rowid,
            x_document_id         => l_document_id,
            x_creation_date       => SYSDATE,
            x_created_by          => fnd_profile.VALUE ('USER_ID'),
            x_last_update_date    => SYSDATE,
            x_last_updated_by     => fnd_profile.VALUE ('USER_ID'),
            x_last_update_login   => fnd_profile.VALUE ('LOGIN_ID'),
            x_datatype_id         => 5,                            -- Web Page
            x_category_id         => l_category_id,
            x_security_type       => 2,
            x_publish_flag        => 'Y',
            x_usage_type          => 'O',
            x_language            => 'US',
            x_description         => l_description,
            x_media_id            => l_media_id,
            x_url                 => TRIM (rec_hdr.attribute1));

         fnd_documents_pkg.insert_tl_row (
            x_document_id         => l_document_id,
            x_creation_date       => SYSDATE,
            x_created_by          => fnd_profile.VALUE ('USER_ID'),
            x_last_update_date    => SYSDATE,
            x_last_updated_by     => fnd_profile.VALUE ('USER_ID'),
            x_last_update_login   => fnd_profile.VALUE ('LOGIN_ID'),
            x_language            => 'US');

         fnd_attached_documents_pkg.insert_row (
            x_rowid                      => l_rowid,
            x_attached_document_id       => l_attached_document_id,
            x_document_id                => l_document_id,
            x_creation_date              => SYSDATE,
            x_created_by                 => fnd_profile.VALUE ('USER_ID'),
            x_last_update_date           => SYSDATE,
            x_last_updated_by            => fnd_profile.VALUE ('USER_ID'),
            x_last_update_login          => fnd_profile.VALUE ('LOGIN_ID'),
            x_seq_num                    => l_seq_num,
            x_entity_name                => 'AP_INVOICES',
            x_column1                    => NULL,
            x_pk1_value                  => l_pk1_value,
            x_pk2_value                  => NULL,
            x_pk3_value                  => NULL,
            x_pk4_value                  => NULL,
            x_pk5_value                  => NULL,
            x_automatically_added_flag   => 'N',
            x_datatype_id                => 5,
            x_category_id                => l_category_id,
            x_security_type              => 2,
            x_publish_flag               => 'Y',
            x_language                   => 'US',
            x_description                => l_description,
            x_media_id                   => l_media_id,
            x_url                        => TRIM (rec_hdr.attribute1));
         COMMIT;

         BEGIN
              SELECT document_id
                INTO l_documentid
                FROM fnd_attached_docs_form_vl
               WHERE     function_name = DECODE (0, 1, NULL, 'APXINWKB')
                     AND function_type = DECODE (0, 1, NULL, 'O')
                     AND (   security_type = 4
                          OR publish_flag = 'Y'
                          OR (security_type = 2 AND security_id = 2042))
                     AND ( (    entity_name = 'AP_INVOICES'
                            AND pk1_value = l_pk1_value))
            ORDER BY user_entity_name, seq_num;

            UPDATE ap_invoices_all
               SET attribute1 = NULL
             WHERE invoice_id = rec_hdr.invoice_id; --TO_NUMBER (l_pk1_value);
         EXCEPTION
            WHEN OTHERS
            THEN
               NULL;
         END;
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         v_errorcode := SQLCODE;
         v_errormessage := SUBSTR (SQLERRM, 1, 200);
         fnd_file.put_line (fnd_file.LOG,
                            'Exception Block of Document Attach Procedure');
         fnd_file.put_line (fnd_file.LOG,
                            'The Error Code Traced is : ' || v_errorcode);
         fnd_file.put_line (
            fnd_file.LOG,
            'The Error Message Traced is : ' || v_errormessage);
   END xxwc_doc_attach_proc;

   /*Main Procedure*/
   PROCEDURE main (errbuf               OUT VARCHAR2,
                   retcode              OUT NUMBER,
                   p_1               IN     VARCHAR2,
                   p_2               IN     VARCHAR2,
                   p_3               IN     VARCHAR2,
                   p_ccid            IN     VARCHAR2,
                   p_validate_only   IN     VARCHAR2,
                   p_submit          IN     VARCHAR2,
                   p_source_name     IN     VARCHAR2,
                   p_import_date     IN     VARCHAR2)
   AS
      l_flag   VARCHAR2 (10) DEFAULT 'Y';

      CURSOR stg_hdr
      IS
         SELECT c.ROWID r_id, c.*
           FROM xxwc.xxwc_ap_invoice_int c
          WHERE NVL (status, 'XXXXX') <> 'PROCESSED';
   BEGIN
      g_import_date := TO_DATE (p_import_date, 'YYYY/MM/DD HH24:MI:SS');
      g_source_name := p_source_name;
      g_ccid := p_ccid;

      /*Initializing Global Variables as per Parameters selected*/
      IF p_validate_only = 'Y'
      THEN
         xxwc_ap_invoice_conv_pkg.g_validate_only := 'Y';
      ELSE
         xxwc_ap_invoice_conv_pkg.g_validate_only := 'N';
      END IF;

      IF p_submit = 'Yes'
      THEN
         xxwc_ap_invoice_conv_pkg.g_submit := 'Y';
      ELSE
         xxwc_ap_invoice_conv_pkg.g_submit := 'N';
      END IF;

      /*Calling Import Procedure Package*/
      xxwc_ap_invoice_conv_pkg.xxwc_stage_to_interface;

      /*Calling Payables Open Interface Import from Backend*/
      IF p_submit = 'Yes'
      THEN
         --xxwc_apinvoice_standard_prog;
         fnd_file.put_line (
            fnd_file.LOG,
               'Request ID for Payables Open Invoice Interface Import Is :'
            || xxwc_ap_invoice_conv_pkg.g_request_id);
         fnd_file.put_line (
            fnd_file.LOG,
               'Program Status :'
            || xxwc_ap_invoice_conv_pkg.g_dev_phase
            || ' / '
            || xxwc_ap_invoice_conv_pkg.g_dev_status);
      END IF;

      xxwc_ap_invoice_conv_pkg.xxwc_doc_attach_proc;

      /*Error Report printing Statememnts*/
      fnd_file.put_line (fnd_file.output, '<HTML><BODY>');
      fnd_file.put_line (fnd_file.output, '<PRE>');
      fnd_file.new_line (fnd_file.output, 1);
      fnd_file.put_line (
         fnd_file.output,
         '********************************************************************************************<BR>');
      fnd_file.put_line (
         fnd_file.output,
         '<H3>                Custom Payable Open Interface ERROR Report                      </H3>');
      fnd_file.put_line (
         fnd_file.output,
         '********************************************************************************************<BR>');
      fnd_file.put_line (
         fnd_file.output,
            '<H4> 1. Program Name           : '
         || ' XXWC_AP_ PAYABLES_OPEN_INTERFACE_IMPORT ');
      fnd_file.put_line (
         fnd_file.output,
            ' 2. Start Date             :  '
         || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));
      fnd_file.put_line (fnd_file.output, '</H4>');
      fnd_file.put_line (
         fnd_file.output,
         '********************************************************************************************<BR>');
      fnd_file.put_line (fnd_file.output, '<BR>');
      fnd_file.put_line (
         fnd_file.output,
         '<b>The following records may causing this failure</b> ');
      fnd_file.put_line (fnd_file.output, '<TABLE BORDER=1>');
      fnd_file.put_line (
         fnd_file.output,
         '<TR><TH>SupplierNumber # </TH><TH>SupplierName # </TH><TH>InvoiceNumber </TH><TH>InvoiceDate </TH><TH>InvoiceAmount # </TH><TH>Rejection Reason # </TH></TR>');

      FOR rec_stg IN stg_hdr
      LOOP
         fnd_file.put_line (
            fnd_file.output,
               '<TR><TD>'
            || rec_stg.vendor_num
            || '</TD><TD>'
            || rec_stg.vendor_name
            || '</TD><TD>'
            || rec_stg.invoice_num
            || '</TD><TD>'
            || rec_stg.invoice_date
            || '</TD><TD>'
            || rec_stg.invoice_amount
            || '</TD><TD>'
            || rec_stg.reject_code
            || '</TD></TR>');
      END LOOP;

      fnd_file.put_line (fnd_file.output, '</TABLE>');
      fnd_file.put_line (fnd_file.output, '</BODY></HTML>');
   EXCEPTION
      WHEN OTHERS
      THEN
         fnd_file.put_line (
            fnd_file.LOG,
               'The Error Code Traced is : '
            || xxwc_ap_invoice_conv_pkg.g_errorcode);
         fnd_file.put_line (
            fnd_file.LOG,
               'The Error Message Traced is : '
            || xxwc_ap_invoice_conv_pkg.g_errormessage);
   END main;

   PROCEDURE xxwc_ap_paid_invoice_exe (errbuf    OUT VARCHAR2,
                                       retcode   OUT NUMBER)
   IS
      l_vendor_id          NUMBER(15);
      l_vendor_site_id     NUMBER(15);
      l_inv_count          NUMBER(15);
      l_verify_flag        VARCHAR2(1);
      l_error_message      VARCHAR2(300);
      v_errorcode          NUMBER;
      v_errormessage       VARCHAR2(240);
      l_vendor_site_code   VARCHAR2(100);
      d_vendor_id          NUMBER;
      i                    NUMBER;
	  ln_count             NUMBER:=0; --Added by Ashwin.S on 28-Mar-2018 for TMS#20180328-00034
	  ln_ok_count          NUMBER:=0; --Added by Ashwin.S on 28-Mar-2018 for TMS#20180328-00034
	  ln_rej_count         NUMBER:=0; --Added by Ashwin.S on 28-Mar-2018 for TMS#20180328-00034
	  ln_t_inv_amount      NUMBER:=0; --Added by Ashwin.S on 28-Mar-2018 for TMS#20180328-00034
	  ln_p_inv_amount      NUMBER:=0; --Added by Ashwin.S on 28-Mar-2018 for TMS#20180328-00034
	  ln_r_inv_amount      NUMBER:=0; --Added by Ashwin.S on 28-Mar-2018 for TMS#20180328-00034

      CURSOR cur_hdr
      IS
             SELECT c.ROWID r_id, c.*
               FROM xxwc_ap_paid_invoice_stg c
              WHERE     NVL (status, 'AA') != 'PROCESSED'
                    AND NVL (status, 'BB') != 'DUP'       --AND ROWNUM < 10001
         FOR UPDATE OF status, reject_code;


      CURSOR d_chk1
      IS
           SELECT vendor_num, invoice_num
             FROM xxwc_ap_paid_invoice_stg c
         GROUP BY vendor_num, invoice_num
           HAVING COUNT (1) > 1;

      CURSOR d_chk (
         i_inv_num       VARCHAR2,
         i_vendor_num    VARCHAR2)
      IS
         SELECT c.ROWID r_id, c.*
           FROM xxwc_ap_paid_invoice_stg c
          WHERE     invoice_num = i_inv_num
                AND vendor_num = i_vendor_num
                AND ROWID NOT IN (SELECT MAX (ROWID)
                                    FROM xxwc_ap_paid_invoice_stg d
                                   WHERE     d.invoice_num = c.invoice_num
                                         AND d.vendor_num = c.vendor_num);
   BEGIN
   
      i := 0;
	  
	  fnd_file.put_line(fnd_file.output,'This is the output file'); --Added by Ashwin.S on 28-Mar-2018 for TMS#20180328-00034
	  fnd_file.put_line(fnd_file.output,'========================'); --Added by Ashwin.S on 28-Mar-2018 for TMS#20180328-00034
	  fnd_file.put_line(fnd_file.output,'Execution Start Time :'||SYSTIMESTAMP); --Added by Ashwin.S on 28-Mar-2018 for TMS#20180328-00034
	  fnd_file.put_line(fnd_file.output,'+-------------------------------+'); --Added by Ashwin.S on 28-Mar-2018 for TMS#20180328-00034
	  	  
	  --Added by Ashwin.S on 28-Mar-2018 for TMS#20180328-00034
	  SELECT COUNT(1)
	  ,      SUM(invoice_amount)
	  INTO ln_count
	  ,    ln_t_inv_amount
	  FROM XXWC.xxwc_ap_paid_invoice_stg c
	  WHERE status IS NULL;
	  
	  fnd_file.put_line (fnd_file.output,'Total Records in the staging table '||ln_count); --Added by Ashwin.S on 28-Mar-2018 for TMS#20180328-00034
	  fnd_file.put_line (fnd_file.output,'Total Invoice Amount is $'||ln_t_inv_amount); --Added by Ashwin.S on 28-Mar-2018 for TMS#20180328-00034
	  fnd_file.put_line(fnd_file.OUTPUT,'+--------------------------------------+'); --Added by Ashwin.S on 28-Mar-2018 for TMS#20180328-00034

      FOR rec_hdr1 IN d_chk1
      LOOP
         FOR rec_hdr2 IN d_chk (rec_hdr1.invoice_num, rec_hdr1.vendor_num)
         LOOP
            UPDATE xxwc_ap_paid_invoice_stg
               SET status = 'DUP'
             WHERE ROWID = rec_hdr2.r_id;
         END LOOP;

         i := i + 1;

         IF i >= 1000
         THEN
            COMMIT;
            i := 0;
         END IF;
      END LOOP;

      COMMIT;

      FOR rec_hdr IN cur_hdr
      LOOP
         l_verify_flag := 'Y';
         v_errorcode := NULL;
         l_error_message := NULL;
         l_vendor_id := NULL;
         l_vendor_site_code := NULL;
         d_vendor_id := NULL;

         BEGIN
            SELECT vendor_id
              INTO l_vendor_id
              FROM ap_suppliers
             WHERE attribute1 = rec_hdr.vendor_num;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_verify_flag := 'N';
               l_error_message :=
                  l_error_message || ',Vendor Number is Not Valid';
         END;

         IF l_verify_flag != 'N'
         THEN
            BEGIN
               INSERT INTO ap_history_invoices_all (invoice_id,
                                                    vendor_id,
                                                    vendor_site_code,
                                                    invoice_num,
                                                    invoice_date,
                                                    invoice_amount,
                                                    org_id)
                    VALUES (ap_invoices_interface_s.NEXTVAL,
                            l_vendor_id,
                            SUBSTR (rec_hdr.vendor_site_code, 1, 15), --l_vendor_site_code,
                            rec_hdr.invoice_num,
                            rec_hdr.invoice_date,
                            rec_hdr.invoice_amount,
                            xxwc_ascp_scwb_pkg.get_wc_org_id --fnd_global.org_id
                                                            );
            EXCEPTION
               WHEN OTHERS
               THEN
                  v_errorcode := SQLCODE;
                  v_errormessage := SUBSTR (SQLERRM, 1, 200);
                  fnd_file.put_line (
                     fnd_file.LOG,
                     'Exception Block of Insert Statement of Closed Invoice');
                  fnd_file.put_line (
                     fnd_file.LOG,
                     'The Error Code Traced is : ' || v_errorcode);
                  fnd_file.put_line (
                     fnd_file.LOG,
                     'The Error Message Traced is : ' || v_errormessage);
            END;

            UPDATE xxwc_ap_paid_invoice_stg
               SET status = 'PROCESSED'
             WHERE ROWID = rec_hdr.r_id;
         ELSE
            UPDATE xxwc_ap_paid_invoice_stg
               SET status = 'REJECTED', reject_code = l_error_message
             WHERE ROWID = rec_hdr.r_id;
         END IF;

         NULL;
      END LOOP;

      COMMIT;
	  
	  --Added by Ashwin.S on 28-Mar-2018 for TMS#20180328-00034
	  SELECT COUNT(1)
	  ,      SUM(invoice_amount)
	  INTO   ln_ok_count
	  ,      ln_p_inv_amount
	  FROM  XXWC.xxwc_ap_paid_invoice_stg c
	  WHERE status ='PROCESSED';
	  
	  --Added by Ashwin.S on 28-Mar-2018 for TMS#20180328-00034
	  SELECT COUNT(1)
	  ,      SUM(invoice_amount)
	  INTO   ln_rej_count
	  ,      ln_r_inv_amount
	  FROM  XXWC.xxwc_ap_paid_invoice_stg c
	  WHERE status ='REJECTED';
	  
	  fnd_file.put_line (fnd_file.OUTPUT,'Total number of Records Processed '||ln_ok_count); --Added by Ashwin.S on 28-Mar-2018 for TMS#20180328-00034
	  fnd_file.put_line (fnd_file.OUTPUT,'Processed Records Total Invoice Amount $'||NVL(ln_p_inv_amount,0)); --Added by Ashwin.S on 28-Mar-2018 for TMS#20180328-00034
	  fnd_file.put_line(fnd_file.OUTPUT,'+--------------------------------------+'); --Added by Ashwin.S on 28-Mar-2018 for TMS#20180328-00034
	  
	  fnd_file.put_line (fnd_file.OUTPUT,'Total number of Records Rejected '||ln_rej_count); --Added by Ashwin.S on 28-Mar-2018 for TMS#20180328-00034
	  fnd_file.put_line (fnd_file.OUTPUT,'Rejected Records Total Invoice Amount $'||NVL(ln_r_inv_amount,0)); --Added by Ashwin.S on 28-Mar-2018 for TMS#20180328-00034
	  fnd_file.put_line(fnd_file.OUTPUT,'+--------------------------------------+'); --Added by Ashwin.S on 28-Mar-2018 for TMS#20180328-00034
	  fnd_file.put_line(fnd_file.output,'Execution End Time :'||SYSTIMESTAMP); --Added by Ashwin.S on 28-Mar-2018 for TMS#20180328-00034
	  
   EXCEPTION
      WHEN OTHERS
      THEN
         v_errorcode := SQLCODE;
         v_errormessage := SUBSTR (SQLERRM, 1, 200);
         fnd_file.put_line (fnd_file.LOG,
                            'Exception Block of Closed Invoice Procedure');
         fnd_file.put_line (fnd_file.LOG,
                            'The Error Code Traced is : ' || v_errorcode);
         fnd_file.put_line (
            fnd_file.LOG,
            'The Error Message Traced is : ' || v_errormessage);
   END xxwc_ap_paid_invoice_exe;
END xxwc_ap_invoice_conv_pkg;
/