/*************************************************************************
Copyright (c) 2012 HD Supply
All rights reserved.
**************************************************************************
   $Header Alter ALTER_IDX_REMOVE_PARALLEL.sql$
  Module Name: ALTER_IDX_REMOVE_PARALLEL
  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)      TMS TASK               DESCRIPTION
  -- ------- -----------   ------------   ---------              -------------------------
  -- 1.0     21-Nov-2016   Rakesh Patel   TMS# 20160927-00112    LOADRUNNER WC- Remove Parallelism from Standard Indexes and Tables
**************************************************************************/
ALTER INDEX AR.HZ_CUST_ACCT_SITES_ALL_T1 NOPARALLEL;

ALTER INDEX ONT.OE_TRANSACTION_TYPES_TL_U2 NOPARALLEL;

ALTER INDEX ONT.OE_PC_ASSIGNMENTS_U2 NOPARALLEL;

ALTER INDEX ONT.OE_PC_EXCLUSIONS_U1 NOPARALLEL;