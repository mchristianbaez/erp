--Report Name            : Rental Utilization and Usage - WC
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data EIS_XXWC_OM_RENTAL_ITEMS_V1
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_OM_RENTAL_ITEMS_V1
xxeis.eis_rsc_ins.v( 'EIS_XXWC_OM_RENTAL_ITEMS_V1',660,'','','','','ANONYMOUS','XXEIS','Eis Xxwc Om Rental Items V','EXORIV','','','VIEW','US','','');
--Delete Object Columns for EIS_XXWC_OM_RENTAL_ITEMS_V1
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_OM_RENTAL_ITEMS_V1',660,FALSE);
--Inserting Object Columns for EIS_XXWC_OM_RENTAL_ITEMS_V1
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_RENTAL_ITEMS_V1','ITEM_DESCRIPTION',660,'Item Description','ITEM_DESCRIPTION','','','','ANONYMOUS','VARCHAR2','','','Item Description','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_RENTAL_ITEMS_V1','ITEM_NUMBER',660,'Item Number','ITEM_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Item Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_RENTAL_ITEMS_V1','LOCATION',660,'Location','LOCATION','','','','ANONYMOUS','VARCHAR2','','','Location','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_RENTAL_ITEMS_V1','QTY_ON_RENT',660,'Qty On Rent','QTY_ON_RENT','','~T~D~2','','ANONYMOUS','NUMBER','','','Qty On Rent','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_RENTAL_ITEMS_V1','REGION',660,'Region','REGION','','','','ANONYMOUS','VARCHAR2','','','Region','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_RENTAL_ITEMS_V1','DEMAND_QTY',660,'Demand Qty','DEMAND_QTY','','~T~D~2','','ANONYMOUS','NUMBER','','','Demand Qty','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_RENTAL_ITEMS_V1','VARIANC',660,'Varianc','VARIANC','','','','ANONYMOUS','NUMBER','','','Varianc','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_RENTAL_ITEMS_V1','UTILIZATION_RATE',660,'Utilization Rate','UTILIZATION_RATE','','~T~D~2','','ANONYMOUS','NUMBER','','','Utilization Rate','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_RENTAL_ITEMS_V1','OWNED',660,'Owned','OWNED','','','','ANONYMOUS','NUMBER','','','Owned','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_RENTAL_ITEMS_V1','ON_HAND',660,'On Hand','ON_HAND','','','','ANONYMOUS','NUMBER','','','On Hand','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_RENTAL_ITEMS_V1','AVERAGE_COST',660,'Average Cost','AVERAGE_COST','','~T~D~2','','ANONYMOUS','NUMBER','','','Average Cost','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_RENTAL_ITEMS_V1','ACTUAL_RENT_DAYS',660,'Actual Rent Days','ACTUAL_RENT_DAYS','','','','ANONYMOUS','NUMBER','','','Actual Rent Days','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_RENTAL_ITEMS_V1','RENT_NR',660,'Rent Nr','RENT_NR','','','','ANONYMOUS','NUMBER','','','Rent Nr','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_RENTAL_ITEMS_V1','RENT_READY',660,'Rent Ready','RENT_READY','','','','ANONYMOUS','NUMBER','','','Rent Ready','','','','');
--Inserting Object Components for EIS_XXWC_OM_RENTAL_ITEMS_V1
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_OM_RENTAL_ITEMS_V1','OE_ORDER_LINES',660,'OE_ORDER_LINES_ALL','OL','OL','ANONYMOUS','ANONYMOUS','-1','Oe Order Lines All Stores Information For All Orde','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_OM_RENTAL_ITEMS_V1','OE_ORDER_HEADERS',660,'OE_ORDER_HEADERS_ALL','OH','OH','ANONYMOUS','ANONYMOUS','-1','Oe Order Headers All Stores Header Information For','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_OM_RENTAL_ITEMS_V1','HZ_PARTIES',660,'HZ_PARTIES','HZP','HZP','ANONYMOUS','ANONYMOUS','-1','Information About Parties Such As Organizations, P','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_OM_RENTAL_ITEMS_V1','MTL_PARAMETERS',660,'MTL_PARAMETERS','MTP','MTP','ANONYMOUS','ANONYMOUS','-1','Inventory Control Options And Defaults','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_OM_RENTAL_ITEMS_V1','MTL_SYSTEM_ITEMS_KFV',660,'MTL_SYSTEM_ITEMS_B','MSI','MSI','ANONYMOUS','ANONYMOUS','-1','Inventory Item Definitions','','','','','','',' ',' ',' ',' ',' ');
--Inserting Object Component Joins for EIS_XXWC_OM_RENTAL_ITEMS_V1
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_OM_RENTAL_ITEMS_V1','OE_ORDER_LINES','OL',660,'EXORIV.LINE_ID','=','OL.LINE_ID(+)','','','','Y','ANONYMOUS');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_OM_RENTAL_ITEMS_V1','OE_ORDER_HEADERS','OH',660,'EXORIV.HEADER_ID','=','OH.HEADER_ID(+)','','','','Y','ANONYMOUS');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_OM_RENTAL_ITEMS_V1','HZ_PARTIES','HZP',660,'EXORIV.PARTY_ID','=','HZP.PARTY_ID(+)','','','','Y','ANONYMOUS');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_OM_RENTAL_ITEMS_V1','MTL_PARAMETERS','MTP',660,'EXORIV.ORGANIZATION_ID','=','MTP.ORGANIZATION_ID(+)','','','','Y','ANONYMOUS');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_OM_RENTAL_ITEMS_V1','MTL_SYSTEM_ITEMS_KFV','MSI',660,'EXORIV.INVENTORY_ITEM_ID','=','MSI.INVENTORY_ITEM_ID(+)','','','','Y','ANONYMOUS');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_OM_RENTAL_ITEMS_V1','MTL_SYSTEM_ITEMS_KFV','MSI',660,'EXORIV.MSI_ORGANIZATION_ID','=','MSI.ORGANIZATION_ID(+)','','','','Y','ANONYMOUS');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
prompt Creating Report LOV Data for Rental Utilization and Usage - WC
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - Rental Utilization and Usage - WC
xxeis.eis_rsc_ins.lov( 660,'SELECT ORGANIZATION_CODE WAREHOUSE,ORGANIZATION_NAME FROM ORG_ORGANIZATION_DEFINITIONS OOD WHERE SYSDATE < NVL(OOD.DISABLE_DATE,SYSDATE+1) AND EXISTS (SELECT 1 FROM XXEIS.EIS_ORG_ACCESS_V  WHERE organization_id = ood.organization_id )','','OM WAREHOUSE','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( '','select msi.segment1 item_number, msi.description item_description from apps.mtl_system_items msi where organization_id=222
and msi.segment1 like ''R%''
UNION
select ''All Rental Items'' item_number,''All Rental Items'' item_description from dual','','XX EIS WC RENTAL ITEM LOV','To display Rental item list','ANONYMOUS',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
prompt Creating Report Data for Rental Utilization and Usage - WC
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Rental Utilization and Usage - WC
xxeis.eis_rsc_utility.delete_report_rows( 'Rental Utilization and Usage - WC' );
--Inserting Report - Rental Utilization and Usage - WC
xxeis.eis_rsc_ins.r( 660,'Rental Utilization and Usage - WC','','Rental Utilization and Usage - WC','','','','SA059956','EIS_XXWC_OM_RENTAL_ITEMS_V1','Y','','','SA059956','','N','White Cap Reports','RTF,PDF,','CSV,HTML,Html Summary,XML,Pivot Excel,EXCEL,','N','','','','','','','','US','','','','');
--Inserting Report Columns - Rental Utilization and Usage - WC
xxeis.eis_rsc_ins.rc( 'Rental Utilization and Usage - WC',660,'DEMAND_QTY','Demand','Demand Qty','','~T~D~0','default','','12','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_RENTAL_ITEMS_V1','','','','US','');
xxeis.eis_rsc_ins.rc( 'Rental Utilization and Usage - WC',660,'ITEM_DESCRIPTION','Item Description','Item Description','','','default','','4','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_RENTAL_ITEMS_V1','','','','US','');
xxeis.eis_rsc_ins.rc( 'Rental Utilization and Usage - WC',660,'ITEM_NUMBER','Item Number','Item Number','','','default','','3','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_RENTAL_ITEMS_V1','','','','US','');
xxeis.eis_rsc_ins.rc( 'Rental Utilization and Usage - WC',660,'LOCATION','Location','Location','','','default','','2','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_RENTAL_ITEMS_V1','','','','US','');
xxeis.eis_rsc_ins.rc( 'Rental Utilization and Usage - WC',660,'OWNED','Owned','Owned','','~T~D~0','default','','5','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_RENTAL_ITEMS_V1','','','','US','');
xxeis.eis_rsc_ins.rc( 'Rental Utilization and Usage - WC',660,'QTY_ON_RENT','On Rent','Qty On Rent','','~T~D~0','default','','13','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_RENTAL_ITEMS_V1','','','','US','');
xxeis.eis_rsc_ins.rc( 'Rental Utilization and Usage - WC',660,'REGION','Region','Region','','','default','','1','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_RENTAL_ITEMS_V1','','','','US','');
xxeis.eis_rsc_ins.rc( 'Rental Utilization and Usage - WC',660,'UTILIZATION_RATE','Utilization Rate%','Utilization Rate','','~T~D~0','default','','15','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_RENTAL_ITEMS_V1','','','','US','');
xxeis.eis_rsc_ins.rc( 'Rental Utilization and Usage - WC',660,'VARIANC','Variance','Varianc','','~T~D~0','default','','14','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_RENTAL_ITEMS_V1','','','','US','');
xxeis.eis_rsc_ins.rc( 'Rental Utilization and Usage - WC',660,'ON_HAND','On Hand','On Hand','','~T~D~0','default','','6','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_RENTAL_ITEMS_V1','','','','US','');
xxeis.eis_rsc_ins.rc( 'Rental Utilization and Usage - WC',660,'RENT_NR','Rent N/R','Rent Nr','','~T~D~0','default','','9','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_RENTAL_ITEMS_V1','','','','US','');
xxeis.eis_rsc_ins.rc( 'Rental Utilization and Usage - WC',660,'RENT_READY','Rent Ready','Rent Ready','','~T~D~0','default','','7','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_RENTAL_ITEMS_V1','','','','US','');
xxeis.eis_rsc_ins.rc( 'Rental Utilization and Usage - WC',660,'Ready_%','Ready %','READY_PERCENT','NUMBER','~~~','default','','8','Y','Y','','','','','','(decode(ON_HAND,0,0,(ROUND((EXORIV.RENT_READY/EXORIV.ON_HAND )*100))))','SA059956','N','N','','','','','','US','');
xxeis.eis_rsc_ins.rc( 'Rental Utilization and Usage - WC',660,'NR%','N/R %','NR_PERCENT','NUMBER','~~~','default','','10','Y','Y','','','','','','(decode(ON_HAND,0,0,(ROUND((EXORIV.RENT_NR/EXORIV.ON_HAND)*100))))','SA059956','N','N','','','','','','US','');
xxeis.eis_rsc_ins.rc( 'Rental Utilization and Usage - WC',660,'Prod_Goal','Prod Goal','PROD_GOAL','NUMBER','~T~D~0','default','','11','Y','Y','','','','','','(decode(-1,sign((EXORIV.ON_HAND/2)-EXORIV.RENT_READY),0,((EXORIV.ON_HAND/2)-EXORIV.RENT_READY)))','SA059956','N','N','','','','','','US','');
--Inserting Report Parameters - Rental Utilization and Usage - WC
xxeis.eis_rsc_ins.rp( 'Rental Utilization and Usage - WC',660,'Warehouse','Warehouse','LOCATION','IN','OM WAREHOUSE','','VARCHAR2','N','Y','1','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_OM_RENTAL_ITEMS_V1','','','US','');
xxeis.eis_rsc_ins.rp( 'Rental Utilization and Usage - WC',660,'Rental Item Number','Item Number','ITEM_NUMBER','IN','XX EIS WC RENTAL ITEM LOV','''All Rental Items''','VARCHAR2','Y','Y','2','','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_OM_RENTAL_ITEMS_V1','','','US','');
--Inserting Dependent Parameters - Rental Utilization and Usage - WC
--Inserting Report Conditions - Rental Utilization and Usage - WC
xxeis.eis_rsc_ins.rcnh( 'Rental Utilization and Usage - WC',660,'LOCATION IN :Warehouse ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','LOCATION','','Warehouse','','','','','EIS_XXWC_OM_RENTAL_ITEMS_V1','','','','','','IN','Y','Y','','','','','1',660,'Rental Utilization and Usage - WC','LOCATION IN :Warehouse ');
xxeis.eis_rsc_ins.rcnh( 'Rental Utilization and Usage - WC',660,'Free Text ','FREE_TEXT','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','','Y','','','','','AND (''All Rental Items'' IN (:Rental Item Number) or (ITEM_NUMBER in (:Rental Item Number)))
AND (QTY_ON_RENT<>0 or ON_HAND<>0 or DEMAND_QTY<>0)','1',660,'Rental Utilization and Usage - WC','Free Text ');
--Inserting Report Sorts - Rental Utilization and Usage - WC
--Inserting Report Triggers - Rental Utilization and Usage - WC
--inserting report templates - Rental Utilization and Usage - WC
--Inserting Report Portals - Rental Utilization and Usage - WC
--inserting report dashboards - Rental Utilization and Usage - WC
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Rental Utilization and Usage - WC','660','EIS_XXWC_OM_RENTAL_ITEMS_V1','EIS_XXWC_OM_RENTAL_ITEMS_V1','N','');
--inserting report security - Rental Utilization and Usage - WC
xxeis.eis_rsc_ins.rsec( 'Rental Utilization and Usage - WC','660','','XXWC_ORDER_MGMT_PRICING_SUPER',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Rental Utilization and Usage - WC','660','','XXWC_ORDER_MGMT_READ_ONLY',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Rental Utilization and Usage - WC','660','','XXWC_RENTAL_OM_PRICING_SUPER',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Rental Utilization and Usage - WC','20005','','XXWC_VIEW_ALL_EIS_REPORTS',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Rental Utilization and Usage - WC','','10010432','',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Rental Utilization and Usage - WC','','SS084202','',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Rental Utilization and Usage - WC','660','','XXWC_ORDER_MGMT_READ_ONLY_COST',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Rental Utilization and Usage - WC','660','','XXWC_ORDER_MGMT_PRICING_STD',660,'SA059956','','','');
--Inserting Report Pivots - Rental Utilization and Usage - WC
xxeis.eis_rsc_ins.rpivot( 'Rental Utilization and Usage - WC',660,'Pivot','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rsc_ins.rpivot_dtls( 'Rental Utilization and Usage - WC',660,'Pivot','OWNED','DATA_FIELD','SUM','','1','','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Rental Utilization and Usage - WC',660,'Pivot','DEMAND_QTY','DATA_FIELD','SUM','','2','','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Rental Utilization and Usage - WC',660,'Pivot','QTY_ON_RENT','DATA_FIELD','SUM','','3','','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Rental Utilization and Usage - WC',660,'Pivot','VARIANC','DATA_FIELD','SUM','','4','','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Rental Utilization and Usage - WC',660,'Pivot','REGION','ROW_FIELD','','','1','1','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Rental Utilization and Usage - WC',660,'Pivot','ITEM_DESCRIPTION','ROW_FIELD','','','2','1','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Rental Utilization and Usage - WC',660,'Pivot','UTILIZATION_RATE','DATA_FIELD','AVE','','5','','xlNormal');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
--Inserting Report   Version details- Rental Utilization and Usage - WC
xxeis.eis_rsc_ins.rv( 'Rental Utilization and Usage - WC','','Rental Utilization and Usage - WC','SA059956');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
