---------------------------------------------------------------------------------------------------------------
/**************************************************************************************************************
  $Header XXEIS.EIS_XXWC_OM_RENTAL_ITEMS_V1 $
  PURPOSE	  : Rental Utilization and Usage - WC
  REVISIONS   :
  VERSION      DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0          nil        			nil   		 	--Initial Version
  1.1	     19-Jul-2017		 	Siva			 TMS#20170711-00154  
**************************************************************************************************************/
CREATE OR REPLACE FORCE VIEW xxeis.eis_xxwc_om_rental_items_v1
(
   location
  ,region
  ,item_number
  ,item_description
  ,average_cost
  ,actual_rent_days
  ,qty_on_rent
  ,on_hand
  ,demand_qty
  ,varianc
  ,owned
  ,utilization_rate
  ,rent_ready  --added for version 1.1
  ,rent_nr    --added for version 1.1
)
AS
   SELECT a.location
         ,a.region
         ,a.item_number
         ,a.item_description
         ,NVL (
             xxeis.eis_rs_xxwc_com_util_pkg.get_unit_cost (a.inventory_item_id
                                                          ,a.organization_id
                                                          ,a.item_number)
            ,0)
             average_cost
         --         ,qty_on_hand
         ,qty_on_rent actual_rent_days
         ,a.qty_on_rent
         ,on_hand
         ,demand_qty
         , (on_hand - demand_qty) varianc
         , (qty_on_rent + on_hand) owned
         , (qty_on_rent * 100) / (DECODE ( (qty_on_rent + on_hand), 0, 1, (qty_on_rent + on_hand)))
             utilization_rate
		 ,nvl((SELECT SUM(NVL(moq1.transaction_quantity,0)) qty
            FROM apps.mtl_onhand_quantities moq1
          WHERE moq1.subinventory_code='Rental'
          AND MOQ1.INVENTORY_ITEM_ID  =A.INVENTORY_ITEM_ID
          AND MOQ1.ORGANIZATION_ID    =A.ORGANIZATION_ID ),0) RENT_READY   --added for version 1.1
         ,nvl((SELECT SUM(NVL(moq1.transaction_quantity,0)) qty
            FROM apps.mtl_onhand_quantities moq1
            WHERE moq1.subinventory_code='Rental N/R'
          AND MOQ1.INVENTORY_ITEM_ID  =A.INVENTORY_ITEM_ID
          and MOQ1.ORGANIZATION_ID    =a.ORGANIZATION_ID ),0) RENT_NR   --added for version 1.1
     FROM (  SELECT mtl.organization_code location
                   ,mtl.attribute9 region
                   ,msi.concatenated_segments item_number
                   ,msi.description item_description
                   , (SELECT NVL (SUM (ordered_quantity), 0)
                        FROM oe_order_lines_all
                       WHERE     inventory_item_id = msi.inventory_item_id
                             AND flow_status_code = 'AWAITING_RETURN'
                             AND ship_from_org_id = msi.organization_id)
                       qty_on_rent
                   ,msi.inventory_item_id
                   ,msi.organization_id
                   --                   ,NVL (SUM (moq.transaction_quantity), 0) qty_on_hand
                   ,NVL (SUM (moq.transaction_quantity), 0) on_hand
                   , (SELECT NVL (open_sales_orders, 0)
                        FROM apps.xxwc_inv_item_search_v
                       WHERE     inventory_item_id = msi.inventory_item_id
                             AND organization_id = msi.organization_id)
                       demand_qty
               FROM mtl_system_items_b_kfv msi, mtl_parameters mtl, apps.mtl_onhand_quantities moq
              WHERE     msi.organization_id = mtl.organization_id
                    AND moq.inventory_item_id = msi.inventory_item_id
                    AND msi.organization_id = moq.organization_id
                    AND msi.segment1 LIKE 'R%' -- Added by Mahender reddy for TMS#20150403-00045 on 04/22/2015
           --                   AND mtl.organization_code = '237'
           GROUP BY mtl.organization_code
                   ,mtl.attribute9
                   ,msi.concatenated_segments
                   ,msi.description
                   ,msi.inventory_item_id
                   ,msi.organization_id
           --Below union Query added by Mahender Reddy for    20150624-00016 on 07/02/2015
           UNION
             SELECT                           -- OL.ORDERED_ITEM,OLRR.ATTRIBUTE12,OL.LINE_ID,RCTL.SALES_ORDER,
                   mtl.organization_code location
                   ,mtl.attribute9 region                          --Added By Mahender for TMS# 20141006-00045
                   ,msi.concatenated_segments item_number
                   ,msi.description item_description
                   , (SELECT NVL (SUM (ordered_quantity), 0)
                        FROM oe_order_lines_all
                       WHERE     inventory_item_id = msi.inventory_item_id
                             AND flow_status_code = 'AWAITING_RETURN'
                             AND ship_from_org_id = msi.organization_id)
                       qty_on_rent                                 --Added By Mahender for TMS# 20141006-00045
                   ,msi.inventory_item_id
                   ,msi.organization_id
                   ,NVL (
                       xxeis.eis_rs_xxwc_com_util_pkg.get_item_onhand_qty (msi.inventory_item_id
                                                                          ,msi.organization_id)
                      ,0)
                       on_hand
                   , (SELECT NVL (open_sales_orders, 0)
                        FROM apps.xxwc_inv_item_search_v
                       WHERE     inventory_item_id = msi.inventory_item_id
                             AND organization_id = msi.organization_id)
                       demand_qty                                  --Added By Mahender for TMS# 20141006-00045
               FROM apps.oe_order_lines_all ol
                   ,apps.oe_order_lines_all olrr
                   ,apps.ra_customer_trx_lines_all rctl
                   ,apps.mtl_system_items_b_kfv msi
                   ,apps.mtl_parameters mtl
              WHERE     rctl.interface_line_context = 'ORDER ENTRY'
                    AND rctl.interface_line_attribute6 = TO_CHAR (ol.line_id)
                    AND ol.link_to_line_id = olrr.line_id
                    AND msi.inventory_item_id = olrr.inventory_item_id
                    AND msi.organization_id = olrr.ship_from_org_id
                    AND mtl.organization_id = msi.organization_id
                    AND msi.segment1 LIKE 'R%'
                    --AND msi.concatenated_segments = 'R128GND6000'
                    AND EXISTS
                           (SELECT 1
                              FROM apps.oe_order_lines_all ol2
                             WHERE ol2.ordered_item = 'Rental Charge' AND ol2.link_to_line_id = olrr.line_id)
           GROUP BY mtl.organization_code
                   ,mtl.attribute9
                   ,msi.concatenated_segments
                   ,msi.description
                   ,msi.inventory_item_id
                   ,msi.organization_id) a;
