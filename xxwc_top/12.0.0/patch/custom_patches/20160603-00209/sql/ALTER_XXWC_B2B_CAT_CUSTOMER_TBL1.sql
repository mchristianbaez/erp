/*************************************************************************
Copyright (c) 2012 HD Supply
All rights reserved.
**************************************************************************
   $Header Alter xxwc.XXWC_B2B_CAT_CUSTOMER_TBL $
  Module Name: xxwc.XXWC_B2B_CAT_CUSTOMER_TBL
  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     22-FEB-2016   Pahwa, Nancy                Initially Created TMS# 20160603-00209  
**************************************************************************/
alter table XXWC.XXWC_B2B_CAT_CUSTOMER_TBL
  add constraint XXWC_CUSTOMER_CATALOG_PK primary key (ID);