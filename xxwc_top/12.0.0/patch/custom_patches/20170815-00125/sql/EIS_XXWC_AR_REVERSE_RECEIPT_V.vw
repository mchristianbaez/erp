/**************************************************************************************************************
  $Header XXEIS.EIS_XXWC_AR_REVERSE_RECEIPT_V.vw $
  Module Name: Receivables
  PURPOSE: Reversed Receipts Report - WC
  TMS Task Id : 20170815-00125 
  REVISIONS:
  Ver        Date        Author                Description
  ---------  ----------  ------------------    ----------------
  1.0		 8/18/2017	 Siva 					Initial Version -- TMS#20170815-00125 
**************************************************************************************************************/
CREATE OR REPLACE VIEW XXEIS.EIS_XXWC_AR_REVERSE_RECEIPT_V (BANK_ACCOUNT_NAME, CUSTOMER_NAME, CUSTOMER_NUMBER, LOCATION, BANK_NAME, BANK_BRANCH_NAME, BANK_ACCOUNT_NUMBER, RECEIPT_NUMBER, REVERSAL_TYPE, MATURITY_DATE, REASON, CURRENCY, REVERSAL_CATEGORY, ORIGINAL_GL_DATE, REVERSAL_GL_DATE, PAYMENT_TYPE, AMOUNT, FUNCTIONAL_AMOUNT, CUSTOMER_CLASS_CODE, COMPANY_NAME, FUNCTIONAL_CURRENCY, FUNCTIONAL_CURRENCY_PRECISION, SET_OF_BOOKS_ID, RECEIPT_DATE, CUSTOMER_SITE_CODE, CUSTOMER_SITE_STATUS, KNOWN_AS, COUNTRY, CUSTOMER_ADDRESS1, CUSTOMER_ADDRESS2, CUSTOMER_ADDRESS3, CUSTOMER_ADDRESS4, CUSTOMER_CITY, CUSTOMER_POSTAL_CODE, CUSTOMER_STATE, CUSTOMER_PROVINCE, PARTY_STATUS, COUNTY, CUSTOMER_EMAIL_ADDRESS, CUSTOMER_STATUS, CUSTOMER_TYPE, OPERATING_UNIT, CUST_ACCOUNT_ID, PARTY_ID, SITE_USE_ID, CURRENCY_CODE, PAYMENT_SCHEDULE_ID, CASH_RECEIPT_HISTORY_ID, REV_CASH_RECEIPT_HISTORY_ID, BANK_ACCT_USE_ID, BANK_ACCOUNT_ID, BANK_PARTY_ID, ORGANIZATION_ID, ORG_ID, CASH_RECEIPT_ID, COPYRIGHT,
  CR#101#AGREEMENT_CODE, CR#102#AGREEMENT_CODE, CR#101#AGREEMENT_YEAR, CR#102#AGREEMENT_YEAR, CR#101#LINE_OF_BUSINESS, CR#102#LINE_OF_BUSINESS, CR#101#REBATE_TYPE, CR#102#REBATE_TYPE, CR#102#TIME_FRAME, CR#101#TIME_FRAME, CBA#CASHPOSIT#CASH_POSITIONI, CBA#DIGITBANK#DIGIT_BANK_NUM, CUST#AUTHORIZED_BUYER_NOTES, CUST#AUTHORIZED_BUYER_REQUIR, CUST#AUTO_APPLY_CREDIT_MEMO, CUST#BRANCH_DESCRIPTION, CUST#CUSTOMER_SOURCE, CUST#LEGAL_COLLECTION_INDICA, CUST#LEGAL_PLACEMENT, CUST#YES#NOTE_LINE_#1, CUST#YES#NOTE_LINE_#2, CUST#YES#NOTE_LINE_#3, CUST#YES#NOTE_LINE_#4, CUST#YES#NOTE_LINE_#5, CUST#PARTY_TYPE, CUST#PREDOMINANT_TRADE, CUST#PRISM_NUMBER, CUST#VNDR_CODE_AND_FRULOC, SU#B2B_ADDRESS, SU#B2B_SHIPPING_WAREHOUSE, SU#CUSTOMER_SITE_CLASSIFICAT, SU#DODGE_NUMBER, SU#FUTURE_USE, SU#GOVERNMENT_FUNDED, SU#PERMIT_NUMBER, SU#SALESREP_#2, SU#SALESREP_SPILT_#1, SU#SALESREP_SPILT_#2, SU#THOMAS_GUIDE_PAGE, PARTY#101#COLLECTOR, PARTY#102#COLLECTOR, PARTY#101#COOP_OVERRIDE, PARTY#102#COOP_OVERRIDE,
  PARTY#101#DEFAULT_COOP_ACCOU, PARTY#102#DEFAULT_COOP_ACCOU, PARTY#102#DEFAULT_COST_CENTE, PARTY#101#DEFAULT_COST_CENTE, PARTY#102#DEFAULT_LOCATION_S, PARTY#101#DEFAULT_LOCATION_S, PARTY#102#DEFAULT_PAYMENT_AC, PARTY#101#DEFAULT_PAYMENT_AC, PARTY#102#DEFAULT_PAYMENT_LO, PARTY#101#DEFAULT_PAYMENT_LO, PARTY#102#DEFAULT_PRODUCT_SE, PARTY#101#DEFAULT_PRODUCT_SE, PARTY#101#DEFAULT_REBATE_ACC, PARTY#102#DEFAULT_REBATE_ACC, PARTY#101#INTERCOMPANY__PAYA, PARTY#102#INTERCOMPANY_PAYAB, PARTY#101#INTERCOMPANY_RECEI, PARTY#102#INTERCOMPANY_RECEI, PARTY#101#PARTY_TYPE, PARTY#102#PARTY_TYPE, PARTY#101#PAYMENT_OVERRIDE, PARTY#102#PAYMENT_OVERRIDE)
AS
  SELECT NVL (cba.bank_account_name, 'None') bank_account_name,
    party.party_name customer_name,
    cust.account_number customer_number,
    su.location location,
    bb.bank_name bank_name,
    bb.bank_branch_name bank_branch_name,
    cba.bank_account_num bank_account_number,
    cr.receipt_number receipt_number,
    l1.meaning reversal_type,
    ps.due_date maturity_date,
    l2.meaning reason,
    cr.currency_code currency,
    l1.lookup_code reversal_category,
    crh1.gl_date original_gl_date,
    DECODE (ps.class, 'DM', ps.due_date, crh2.gl_date ) reversal_gl_date,
    l3.meaning payment_type,
    crh1.amount       + NVL (crh1.factor_discount_amount, 0) amount,
    crh1.acctd_amount + NVL (crh1.acctd_factor_discount_amount, 0) functional_amount,
    cust.customer_class_code customer_class_code,
    gle.name company_name,
    gle.currency_code functional_currency,
    cur.precision functional_currency_precision,
    param.set_of_books_id set_of_books_id,
    cr.receipt_date receipt_date,
    su.site_use_code customer_site_code,
    DECODE (su.status, 'A', 'Active', 'I', 'Inactive') customer_site_status,
    party.known_as,
    party.country,
    party.address1 customer_address1,
    party.address2 customer_address2,
    party.address3 customer_address3,
    party.address4 customer_address4,
    party.city customer_city,
    party.postal_code customer_postal_code,
    party.state customer_state,
    party.province customer_province,
    party.status party_status,
    party.county,
    party.email_address customer_email_address,
    cust.status customer_status,
    cust.customer_type customer_type,
    hou.name operating_unit,
    --Primary Keys
    cust.cust_account_id,
    party.party_id,
    su.site_use_id,
    cur.currency_code,
    ps.payment_schedule_id,
    crh1.cash_receipt_history_id,
    crh2.cash_receipt_history_id rev_cash_receipt_history_id,
    ba.bank_acct_use_id,
    cba.bank_account_id,
    bb.bank_party_id,
    hou.organization_id,
    cr.org_id,
    cr.cash_receipt_id ,
    'Copyright(c) 2001-'
    ||TO_CHAR(sysdate,'YYYY')
    ||' '
    ||'EiS Technologies Inc. All rights reserved.' copyright
    --descr#flexfield#start
    ,
    DECODE(cr.attribute_category ,'101',cr.attribute12, NULL) cr#101#agreement_code ,
    DECODE(cr.attribute_category ,'102',cr.attribute12, NULL) cr#102#agreement_code ,
    DECODE(cr.attribute_category ,'101',cr.attribute11, NULL) cr#101#agreement_year ,
    DECODE(cr.attribute_category ,'102',cr.attribute11, NULL) cr#102#agreement_year ,
    DECODE(cr.attribute_category ,'101',xxeis.eis_rs_dff.decode_valueset( 'XXCUSOZF_BU_CODES',cr.attribute1,'F'), NULL) cr#101#line_of_business ,
    DECODE(cr.attribute_category ,'102',xxeis.eis_rs_dff.decode_valueset( 'XXCUSOZF_BU_CODES',cr.attribute1,'F'), NULL) cr#102#line_of_business ,
    DECODE(cr.attribute_category ,'101',cr.attribute10, NULL) cr#101#rebate_type ,
    DECODE(cr.attribute_category ,'102',cr.attribute10, NULL) cr#102#rebate_type ,
    DECODE(cr.attribute_category ,'102',cr.attribute2, NULL) cr#102#time_frame ,
    DECODE(cr.attribute_category ,'101',cr.attribute2, NULL) cr#101#time_frame ,
    DECODE(cba.attribute_category ,'Cash Positioning',xxeis.eis_rs_dff.decode_valueset( 'Cash Positioning',cba.attribute2,'I'), NULL) cba#cashposit#cash_positioni ,
    DECODE(cba.attribute_category ,'Digit Bank Acct No',cba.attribute1, NULL) cba#digitbank#digit_bank_num ,
    cust.attribute8 cust#authorized_buyer_notes ,
    xxeis.eis_rs_dff.decode_valueset( 'Yes_No',cust.attribute7,'F') cust#authorized_buyer_requir ,
    xxeis.eis_rs_dff.decode_valueset( 'XXWC_ON_OFF',cust.attribute11,'F') cust#auto_apply_credit_memo ,
    cust.attribute3 cust#branch_description ,
    cust.attribute4 cust#customer_source ,
    xxeis.eis_rs_dff.decode_valueset( 'XXWC_LEGAL_COLLECTION',cust.attribute5,'F') cust#legal_collection_indica ,
    cust.attribute10 cust#legal_placement ,
    DECODE(cust.attribute_category ,'Yes',cust.attribute17, NULL) cust#yes#note_line_#1 ,
    DECODE(cust.attribute_category ,'Yes',cust.attribute18, NULL) cust#yes#note_line_#2 ,
    DECODE(cust.attribute_category ,'Yes',cust.attribute19, NULL) cust#yes#note_line_#3 ,
    DECODE(cust.attribute_category ,'Yes',cust.attribute20, NULL) cust#yes#note_line_#4 ,
    DECODE(cust.attribute_category ,'Yes',cust.attribute16, NULL) cust#yes#note_line_#5 ,
    xxeis.eis_rs_dff.decode_valueset( 'XXCUS_PARTY_TYPE',cust.attribute1,'I') cust#party_type ,
    xxeis.eis_rs_dff.decode_valueset( 'XXCUS_PREDOMINANT_TRADE',cust.attribute9,'I') cust#predominant_trade ,
    cust.attribute6 cust#prism_number ,
    cust.attribute2 cust#vndr_code_and_fruloc ,
    su.attribute10 su#b2b_address ,
    su.attribute11 su#b2b_shipping_warehouse ,
    xxeis.eis_rs_dff.decode_valueset( 'XXWC_CUSTOMER_SITE_CLASS',su.attribute1,'F') su#customer_site_classificat ,
    su.attribute4 su#dodge_number ,
    su.attribute5 su#future_use ,
    xxeis.eis_rs_dff.decode_valueset( 'Yes_No',su.attribute2,'F') su#government_funded ,
    su.attribute9 su#permit_number ,
    xxeis.eis_rs_dff.decode_valueset( 'OM: Salesreps',su.attribute6,'F') su#salesrep_#2 ,
    su.attribute7 su#salesrep_spilt_#1 ,
    su.attribute8 su#salesrep_spilt_#2 ,
    su.attribute3 su#thomas_guide_page ,
    DECODE(party.attribute_category ,'101',xxeis.eis_rs_dff.decode_valueset( 'XXCUSOZF_AR_COLLECTORS',party.attribute3,'F'), NULL) party#101#collector ,
    DECODE(party.attribute_category ,'102',xxeis.eis_rs_dff.decode_valueset( 'XXCUSOZF_AR_COLLECTORS',party.attribute3,'F'), NULL) party#102#collector ,
    DECODE(party.attribute_category ,'101',xxeis.eis_rs_dff.decode_valueset( 'QP: Yes/No',party.attribute11,'F'), NULL) party#101#coop_override ,
    DECODE(party.attribute_category ,'102',xxeis.eis_rs_dff.decode_valueset( 'QP: Yes/No',party.attribute11,'F'), NULL) party#102#coop_override ,
    DECODE(party.attribute_category ,'101',xxeis.eis_rs_dff.decode_valueset( 'XXCUS_GL_ACCOUNT',party.attribute6,'I'), NULL) party#101#default_coop_accou ,
    DECODE(party.attribute_category ,'102',xxeis.eis_rs_dff.decode_valueset( 'XXCUS_GL_ACCOUNT',party.attribute6,'I'), NULL) party#102#default_coop_accou ,
    DECODE(party.attribute_category ,'102',xxeis.eis_rs_dff.decode_valueset( 'XXCUS_GL_COSTCENTER',party.attribute8,'I'), NULL) party#102#default_cost_cente ,
    DECODE(party.attribute_category ,'101',xxeis.eis_rs_dff.decode_valueset( 'XXCUS_GL_COSTCENTER',party.attribute8,'I'), NULL) party#101#default_cost_cente ,
    DECODE(party.attribute_category ,'102',xxeis.eis_rs_dff.decode_valueset( 'XXCUS_GL_LOCATION',party.attribute5,'I'), NULL) party#102#default_location_s ,
    DECODE(party.attribute_category ,'101',xxeis.eis_rs_dff.decode_valueset( 'XXCUS_GL_LOCATION',party.attribute5,'I'), NULL) party#101#default_location_s ,
    DECODE(party.attribute_category ,'102',xxeis.eis_rs_dff.decode_valueset( 'XXCUS_GL_ACCOUNT',party.attribute13,'I'), NULL) party#102#default_payment_ac ,
    DECODE(party.attribute_category ,'101',xxeis.eis_rs_dff.decode_valueset( 'XXCUS_GL_ACCOUNT',party.attribute13,'I'), NULL) party#101#default_payment_ac ,
    DECODE(party.attribute_category ,'102',xxeis.eis_rs_dff.decode_valueset( 'XXCUS_GL_LOCATION',party.attribute14,'I'), NULL) party#102#default_payment_lo ,
    DECODE(party.attribute_category ,'101',xxeis.eis_rs_dff.decode_valueset( 'XXCUS_GL_LOCATION',party.attribute14,'I'), NULL) party#101#default_payment_lo ,
    DECODE(party.attribute_category ,'102',xxeis.eis_rs_dff.decode_valueset( 'XXCUS_GL_PRODUCT',party.attribute4,'I'), NULL) party#102#default_product_se ,
    DECODE(party.attribute_category ,'101',xxeis.eis_rs_dff.decode_valueset( 'XXCUS_GL_PRODUCT',party.attribute4,'I'), NULL) party#101#default_product_se ,
    DECODE(party.attribute_category ,'101',xxeis.eis_rs_dff.decode_valueset( 'XXCUS_GL_ACCOUNT',party.attribute7,'I'), NULL) party#101#default_rebate_acc ,
    DECODE(party.attribute_category ,'102',xxeis.eis_rs_dff.decode_valueset( 'XXCUS_GL_ACCOUNT',party.attribute7,'I'), NULL) party#102#default_rebate_acc ,
    DECODE(party.attribute_category ,'101',xxeis.eis_rs_dff.decode_valueset( 'XXCUS_GL_ACCOUNT',party.attribute9,'I'), NULL) party#101#intercompany__paya ,
    DECODE(party.attribute_category ,'102',xxeis.eis_rs_dff.decode_valueset( 'XXCUS_GL_ACCOUNT',party.attribute9,'I'), NULL) party#102#intercompany_payab ,
    DECODE(party.attribute_category ,'101',xxeis.eis_rs_dff.decode_valueset( 'XXCUS_GL_ACCOUNT',party.attribute10,'I'), NULL) party#101#intercompany_recei ,
    DECODE(party.attribute_category ,'102',xxeis.eis_rs_dff.decode_valueset( 'XXCUS_GL_ACCOUNT',party.attribute10,'I'), NULL) party#102#intercompany_recei ,
    DECODE(party.attribute_category ,'101',xxeis.eis_rs_dff.decode_valueset( 'XXCUS_PARTY_TYPE',party.attribute1,'I'), NULL) party#101#party_type ,
    DECODE(party.attribute_category ,'102',xxeis.eis_rs_dff.decode_valueset( 'XXCUS_PARTY_TYPE',party.attribute1,'I'), NULL) party#102#party_type ,
    DECODE(party.attribute_category ,'101',xxeis.eis_rs_dff.decode_valueset( 'QP: Yes/No',party.attribute12,'F'), NULL) party#101#payment_override ,
    DECODE(party.attribute_category ,'102',xxeis.eis_rs_dff.decode_valueset( 'QP: Yes/No',party.attribute12,'F'), NULL) party#102#payment_override
    --descr#flexfield#end
    --gl#accountff#start
    --gl#accountff#end
  FROM ar_cash_receipts cr,
    ar_cash_receipt_history crh1,
    ar_cash_receipt_history crh2,
    ar_payment_schedules ps,
    ce_bank_acct_uses ba,
    ce_bank_accounts cba,
    ce_bank_branches_v bb,
    hz_cust_accounts cust,
    hz_parties party,
    hz_cust_site_uses su,
    ar_lookups l1,
    ar_lookups l2,
    ar_lookups l3,
    gl_ledgers gle,
    ar_system_parameters param,
    fnd_currencies cur,
    hr_operating_units hou
  WHERE cr.cash_receipt_id          = crh1.cash_receipt_id
  AND crh1.first_posted_record_flag = 'Y'
  AND cr.cash_receipt_id            = crh2.cash_receipt_id
  AND crh2.status                   = 'REVERSED'
  AND cr.cash_receipt_id            = ps.cash_receipt_id(+)
  AND cr.remit_bank_acct_use_id     = ba.bank_acct_use_id
  AND ba.bank_account_id            = cba.bank_account_id
  AND cba.bank_branch_id            = bb.branch_party_id
  AND cba.account_classification    = 'INTERNAL'
  AND cr.pay_from_customer          = cust.cust_account_id(+)
  AND cust.party_id                 = party.party_id(+)
  AND cr.customer_site_use_id       = su.site_use_id(+)
  AND cr.reversal_category          = l1.lookup_code
  AND l1.lookup_type                = 'REVERSAL_CATEGORY_TYPE'
  AND cr.reversal_reason_code       = l2.lookup_code
  AND l2.lookup_type                = 'CKAJST_REASON'
  AND cr.type                       = l3.lookup_code
  AND l3.lookup_type                = 'PAYMENT_CATEGORY_TYPE'
  AND ps.org_id                     = hou.organization_id
  AND hou.organization_id           = param.org_id
  AND gle.ledger_id                 = param.set_of_books_id
  AND gle.currency_code             = cur.currency_code
/
  