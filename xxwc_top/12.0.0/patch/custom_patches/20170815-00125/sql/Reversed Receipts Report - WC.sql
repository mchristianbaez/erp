--Report Name            : Reversed Receipts Report - WC
--Import Instructions : 1. If RTF template needs to be uploaded to report automatically then move the RTF template to physical path of APPS_DATA_FILE_DIR oracle directory. Check dba_directories view to get the physical path of oracle directory. 
--                         (If you want to use any other oracle directory other than APPS_DATA_FILE_DIR then replace APPS_DATA_FILE_DIR with your oracle directory name below.)
--                      2. Make sure XXEIS schema has read/write permissions to above used oracle directory and give full permissions to the directory at OS level.
--                      3. Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
exec xxeis.eis_rsc_ins.g_rtf_dir := 'APPS_DATA_FILE_DIR' ;
prompt Creating Object Data EIS_XXWC_AR_REVERSE_RECEIPT_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(222);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_AR_REVERSE_RECEIPT_V
xxeis.eis_rsc_ins.v( 'EIS_XXWC_AR_REVERSE_RECEIPT_V',222,'This view shows the details of receipts that have been reversed including the type; i.e. stop check and non-sufficient funds.','804','','','SA059956','XXEIS','EIS XXWC AR Reversed Receipts','EXARRV','','','VIEW','US','','EIS_AR_REVERSED_RECEIPTS_V','');
--Delete Object Columns for EIS_XXWC_AR_REVERSE_RECEIPT_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_AR_REVERSE_RECEIPT_V',222,FALSE);
--Inserting Object Columns for EIS_XXWC_AR_REVERSE_RECEIPT_V
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','COMPANY_NAME',222,'Accounting books name','COMPANY_NAME','','','','SA059956','VARCHAR2','GL_SETS_OF_BOOKS','NAME','Company Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','OPERATING_UNIT',222,'Operating Unit','OPERATING_UNIT','','','','SA059956','VARCHAR2','HR_ALL_ORGANIZATION_UNITS_TL','NAME','Operating Unit','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','FUNCTIONAL_AMOUNT',222,'Functional Amount','FUNCTIONAL_AMOUNT','','','','SA059956','NUMBER','CALCULATION COLUMN','CALCULATION COLUMN','Functional Amount','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','CUSTOMER_NUMBER',222,'Customer Number','CUSTOMER_NUMBER','','','','SA059956','VARCHAR2','HZ_CUST_ACCOUNTS','ACCOUNT_NUMBER','Customer Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','REVERSAL_GL_DATE',222,'Reversal Gl Date','REVERSAL_GL_DATE','','','','SA059956','DATE','DERIVED COLUMN','DERIVED COLUMN','Reversal Gl Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','BANK_ACCOUNT_NAME',222,'Bank account name','BANK_ACCOUNT_NAME','','','','SA059956','VARCHAR2','AP_BANK_ACCOUNTS_ALL','BANK_ACCOUNT_NAME','Bank Account Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','CUSTOMER_NAME',222,'Customer Name','CUSTOMER_NAME','','','','SA059956','VARCHAR2','HZ_CUST_ACCOUNTS','ACCOUNT_NAME','Customer Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','LOCATION',222,'Site use identifier','LOCATION','','','','SA059956','VARCHAR2','HZ_CUST_SITE_USES_ALL','LOCATION','Location','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','RECEIPT_NUMBER',222,'Receipt Number','RECEIPT_NUMBER','','','','SA059956','VARCHAR2','AR_CASH_RECEIPTS_ALL','RECEIPT_NUMBER','Receipt Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','REVERSAL_TYPE',222,'QuickCode meaning','REVERSAL_TYPE','','','','SA059956','VARCHAR2','FND_LOOKUP_VALUES','MEANING','Reversal Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','MATURITY_DATE',222,'Maturity Date','MATURITY_DATE','','','','SA059956','DATE','','','Maturity Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','REASON',222,'Reason','REASON','','','','SA059956','VARCHAR2','FND_LOOKUP_VALUES','MEANING','Reason','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','CURRENCY',222,'Currency','CURRENCY','','','','SA059956','VARCHAR2','AR_CASH_RECEIPTS_ALL','CURRENCY_CODE','Currency','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','ORIGINAL_GL_DATE',222,'Original Gl Date','ORIGINAL_GL_DATE','','','','SA059956','DATE','','','Original Gl Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','AMOUNT',222,'Amount','AMOUNT','','','','SA059956','NUMBER','AR_CASH_RECEIPT_HISTORY_ALL','AMOUNT','Amount','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','FUNCTIONAL_CURRENCY',222,'Functional Currency','FUNCTIONAL_CURRENCY','','','','SA059956','VARCHAR2','GL_SETS_OF_BOOKS','CURRENCY_CODE','Functional Currency','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','FUNCTIONAL_CURRENCY_PRECISION',222,'Functional Currency Precision','FUNCTIONAL_CURRENCY_PRECISION','','','','SA059956','NUMBER','FND_CURRENCIES','PRECISION','Functional Currency Precision','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','SET_OF_BOOKS_ID',222,'Accounting books defining column','SET_OF_BOOKS_ID','','','','SA059956','NUMBER','GL_SETS_OF_BOOKS','SET_OF_BOOKS_ID','Set Of Books Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','CUSTOMER_SITE_CODE',222,'Customer Site Code','CUSTOMER_SITE_CODE','','','','SA059956','VARCHAR2','HZ_CUST_SITE_USES_ALL','SITE_USE_CODE','Customer Site Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','COUNTRY',222,'The country listed in the TERRITORY_CODE column of the FND_TERRITORY table. for the Identifying address.','COUNTRY','','','','SA059956','VARCHAR2','HZ_PARTIES','COUNTRY','Country','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','COUNTY',222,'County of the Identifying address','COUNTY','','','','SA059956','VARCHAR2','HZ_PARTIES','COUNTY','County','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','CUSTOMER_STATUS',222,'Customer Status','CUSTOMER_STATUS','','','','SA059956','VARCHAR2','HZ_CUST_ACCOUNTS','STATUS','Customer Status','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','CUSTOMER_TYPE',222,'Customer Type','CUSTOMER_TYPE','','','','SA059956','VARCHAR2','HZ_PARTIES','CUSTOMER_TYPE','Customer Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','KNOWN_AS',222,'An alias or other name by which a party is known','KNOWN_AS','','','','SA059956','VARCHAR2','HZ_PARTIES','KNOWN_AS','Known As','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','PARTY_STATUS',222,'Party Status','PARTY_STATUS','','','','SA059956','VARCHAR2','HZ_PARTIES','STATUS','Party Status','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','CUSTOMER_ADDRESS1',222,'Customer Address1','CUSTOMER_ADDRESS1','','','','SA059956','VARCHAR2','HZ_PARTIES','ADDRESS1','Customer Address1','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','CUSTOMER_ADDRESS2',222,'Customer Address2','CUSTOMER_ADDRESS2','','','','SA059956','VARCHAR2','HZ_PARTIES','ADDRESS2','Customer Address2','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','CUSTOMER_ADDRESS3',222,'Customer Address3','CUSTOMER_ADDRESS3','','','','SA059956','VARCHAR2','HZ_PARTIES','ADDRESS3','Customer Address3','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','CUSTOMER_ADDRESS4',222,'Customer Address4','CUSTOMER_ADDRESS4','','','','SA059956','VARCHAR2','HZ_PARTIES','ADDRESS4','Customer Address4','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','CUSTOMER_CITY',222,'Customer City','CUSTOMER_CITY','','','','SA059956','VARCHAR2','HZ_PARTIES','CITY','Customer City','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','CUSTOMER_EMAIL_ADDRESS',222,'Customer Email Address','CUSTOMER_EMAIL_ADDRESS','','','','SA059956','VARCHAR2','HZ_PARTIES','EMAIL_ADDRESS','Customer Email Address','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','CUSTOMER_POSTAL_CODE',222,'Customer Postal Code','CUSTOMER_POSTAL_CODE','','','','SA059956','VARCHAR2','HZ_PARTIES','POSTAL_CODE','Customer Postal Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','CUSTOMER_PROVINCE',222,'Customer Province','CUSTOMER_PROVINCE','','','','SA059956','VARCHAR2','HZ_PARTIES','PROVINCE','Customer Province','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','CUSTOMER_STATE',222,'Customer State','CUSTOMER_STATE','','','','SA059956','VARCHAR2','HZ_PARTIES','STATE','Customer State','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','CUSTOMER_SITE_STATUS',222,'Customer Site Status','CUSTOMER_SITE_STATUS','','','','SA059956','VARCHAR2','HZ_CUST_SITE_USES_ALL','STATUS','Customer Site Status','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','BANK_ACCOUNT_ID',222,'Bank account identifier','BANK_ACCOUNT_ID','','','','SA059956','NUMBER','AP_BANK_ACCOUNTS_ALL','BANK_ACCOUNT_ID','Bank Account Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','BANK_ACCT_USE_ID',222,'Bank Acct Use Id','BANK_ACCT_USE_ID','','','','SA059956','NUMBER','CE_BANK_ACCT_USES_ALL','BANK_ACCT_USE_ID','Bank Acct Use Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','BANK_PARTY_ID',222,'Bank Party Id','BANK_PARTY_ID','','','','SA059956','NUMBER','HZ_ORGANIZATION_PROFILES','PARTY_ID','Bank Party Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','CASH_RECEIPT_HISTORY_ID',222,'Cash receipt history identifier','CASH_RECEIPT_HISTORY_ID','','','','SA059956','NUMBER','AR_CASH_RECEIPT_HISTORY_ALL','CASH_RECEIPT_HISTORY_ID','Cash Receipt History Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','ORGANIZATION_ID',222,'Organization Id','ORGANIZATION_ID','','','','SA059956','NUMBER','HR_ALL_ORGANIZATION_UNITS','ORGANIZATION_ID','Organization Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','PAYMENT_SCHEDULE_ID',222,'Identifies the payment schedule','PAYMENT_SCHEDULE_ID','','','','SA059956','NUMBER','AR_PAYMENT_SCHEDULES_ALL','PAYMENT_SCHEDULE_ID','Payment Schedule Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','REV_CASH_RECEIPT_HISTORY_ID',222,'Rev Cash Receipt History Id','REV_CASH_RECEIPT_HISTORY_ID','','','','SA059956','NUMBER','AR_CASH_RECEIPT_HISTORY_ALL','CASH_RECEIPT_HISTORY_ID','Rev Cash Receipt History Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','CUSTOMER_CLASS_CODE',222,'Customer Class Code','CUSTOMER_CLASS_CODE','','','','SA059956','VARCHAR2','HZ_CUST_ACCOUNTS','CUSTOMER_CLASS_CODE','Customer Class Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','BANK_NAME',222,'Bank name','BANK_NAME','','','','SA059956','VARCHAR2','AP_BANK_BRANCHES','BANK_NAME','Bank Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','BANK_BRANCH_NAME',222,'Bank branch name','BANK_BRANCH_NAME','','','','SA059956','VARCHAR2','AP_BANK_BRANCHES','BANK_BRANCH_NAME','Bank Branch Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','BANK_ACCOUNT_NUMBER',222,'Bank account number','BANK_ACCOUNT_NUMBER','','','','SA059956','VARCHAR2','AP_BANK_ACCOUNTS_ALL','BANK_ACCOUNT_NUM','Bank Account Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','REVERSAL_CATEGORY',222,'Reversal Category','REVERSAL_CATEGORY','','','','SA059956','VARCHAR2','FND_LOOKUP_VALUES','LOOKUP_CODE','Reversal Category','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','PAYMENT_TYPE',222,'Payment Type','PAYMENT_TYPE','','','','SA059956','VARCHAR2','FND_LOOKUP_VALUES','MEANING','Payment Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','CURRENCY_CODE',222,'Currency Code','CURRENCY_CODE','','','','SA059956','VARCHAR2','FND_CURRENCIES','CURRENCY_CODE','Currency Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','CUST_ACCOUNT_ID',222,'Customer account identifier','CUST_ACCOUNT_ID','','','','SA059956','NUMBER','HZ_CUST_ACCOUNTS','CUST_ACCOUNT_ID','Cust Account Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','PARTY_ID',222,'Party identifier','PARTY_ID','','','','SA059956','NUMBER','HZ_PARTIES','PARTY_ID','Party Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','SITE_USE_ID',222,'Site use identifier','SITE_USE_ID','','','','SA059956','NUMBER','HZ_CUST_SITE_USES_ALL','SITE_USE_ID','Site Use Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','CASH_RECEIPT_ID',222,'Identifier of the cash receipt','CASH_RECEIPT_ID','','','','SA059956','NUMBER','AR_CASH_RECEIPTS_ALL','CASH_RECEIPT_ID','Cash Receipt Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','ORG_ID',222,'Org Id','ORG_ID','','','','SA059956','NUMBER','AR_CASH_RECEIPTS_ALL','ORG_ID','Org Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','RECEIPT_DATE',222,'Receipt Date','RECEIPT_DATE','','','','SA059956','DATE','','','Receipt Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','COPYRIGHT',222,'Copyright','COPYRIGHT','','','','SA059956','VARCHAR2','','','Copyright','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','CR#101#Agreement_Code',222,'Descriptive flexfield (DFF): Receipt Information Column Name: Agreement Code Context: 101','CR#101#Agreement_Code','','','','SA059956','VARCHAR2','AR_CASH_RECEIPTS_ALL','ATTRIBUTE12','Cr#101#Agreement Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','CR#102#Agreement_Code',222,'Descriptive flexfield (DFF): Receipt Information Column Name: Agreement Code Context: 102','CR#102#Agreement_Code','','','','SA059956','VARCHAR2','AR_CASH_RECEIPTS_ALL','ATTRIBUTE12','Cr#102#Agreement Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','CR#102#Agreement_Year',222,'Descriptive flexfield (DFF): Receipt Information Column Name: Agreement Year Context: 102','CR#102#Agreement_Year','','','','SA059956','VARCHAR2','AR_CASH_RECEIPTS_ALL','ATTRIBUTE11','Cr#102#Agreement Year','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','CR#101#Agreement_Year',222,'Descriptive flexfield (DFF): Receipt Information Column Name: Agreement Year Context: 101','CR#101#Agreement_Year','','','','SA059956','VARCHAR2','AR_CASH_RECEIPTS_ALL','ATTRIBUTE11','Cr#101#Agreement Year','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','CR#102#Line_Of_Business',222,'Descriptive flexfield (DFF): Receipt Information Column Name: Line Of Business Context: 102','CR#102#Line_Of_Business','','','','SA059956','VARCHAR2','AR_CASH_RECEIPTS_ALL','ATTRIBUTE1','Cr#102#Line Of Business','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','CR#101#Line_Of_Business',222,'Descriptive flexfield (DFF): Receipt Information Column Name: Line Of Business Context: 101','CR#101#Line_Of_Business','','','','SA059956','VARCHAR2','AR_CASH_RECEIPTS_ALL','ATTRIBUTE1','Cr#101#Line Of Business','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','CR#102#Rebate_Type',222,'Descriptive flexfield (DFF): Receipt Information Column Name: Rebate Type Context: 102','CR#102#Rebate_Type','','','','SA059956','VARCHAR2','AR_CASH_RECEIPTS_ALL','ATTRIBUTE10','Cr#102#Rebate Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','CR#101#Rebate_Type',222,'Descriptive flexfield (DFF): Receipt Information Column Name: Rebate Type Context: 101','CR#101#Rebate_Type','','','','SA059956','VARCHAR2','AR_CASH_RECEIPTS_ALL','ATTRIBUTE10','Cr#101#Rebate Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','CR#101#Time_Frame',222,'Descriptive flexfield (DFF): Receipt Information Column Name: Time Frame Context: 101','CR#101#Time_Frame','','','','SA059956','VARCHAR2','AR_CASH_RECEIPTS_ALL','ATTRIBUTE2','Cr#101#Time Frame','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','CR#102#Time_Frame',222,'Descriptive flexfield (DFF): Receipt Information Column Name: Time Frame Context: 102','CR#102#Time_Frame','','','','SA059956','VARCHAR2','AR_CASH_RECEIPTS_ALL','ATTRIBUTE2','Cr#102#Time Frame','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','CBA#CashPosit#Cash_Positioni',222,'Descriptive flexfield (DFF): Bank Accounts Column Name: Cash Positioning Context: Cash Positioning','CBA#CashPosit#Cash_Positioni','','','','SA059956','VARCHAR2','CE_BANK_ACCOUNTS','ATTRIBUTE2','Cba#Cash Positioning#Cash Positioning','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','CBA#DigitBank#Digit_Bank_Num',222,'Descriptive flexfield (DFF): Bank Accounts Column Name: Digit Bank Number (3) Context: Digit Bank Acct No','CBA#DigitBank#Digit_Bank_Num','','','','SA059956','VARCHAR2','CE_BANK_ACCOUNTS','ATTRIBUTE1','Cba#Digit Bank Acct No#Digit Bank Number 3','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','CUST#Authorized_Buyer_Notes',222,'Descriptive flexfield (DFF): Customer Information Column Name: Authorized Buyer Notes','CUST#Authorized_Buyer_Notes','','','','SA059956','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE8','Cust#Authorized Buyer Notes','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','CUST#Authorized_Buyer_Requir',222,'Descriptive flexfield (DFF): Customer Information Column Name: Authorized Buyer Required','CUST#Authorized_Buyer_Requir','','','','SA059956','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE7','Cust#Authorized Buyer Required','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','CUST#Auto_Apply_Credit_Memo',222,'Descriptive flexfield (DFF): Customer Information Column Name: Auto Apply Credit Memo','CUST#Auto_Apply_Credit_Memo','','','','SA059956','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE11','Cust#Auto Apply Credit Memo','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','CUST#Branch_Description',222,'Descriptive flexfield (DFF): Customer Information Column Name: Branch Description','CUST#Branch_Description','','','','SA059956','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE3','Cust#Branch Description','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','CUST#Customer_Source',222,'Descriptive flexfield (DFF): Customer Information Column Name: Customer Source','CUST#Customer_Source','','','','SA059956','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE4','Cust#Customer Source','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','CUST#Legal_Collection_Indica',222,'Descriptive flexfield (DFF): Customer Information Column Name: Legal Collection Indicator','CUST#Legal_Collection_Indica','','','','SA059956','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE5','Cust#Legal Collection Indicator','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','CUST#Legal_Placement',222,'Descriptive flexfield (DFF): Customer Information Column Name: Legal Placement','CUST#Legal_Placement','','','','SA059956','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE10','Cust#Legal Placement','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','CUST#Yes#Note_Line_#1',222,'Descriptive flexfield (DFF): Customer Information Column Name: Note Line #1 Context: Yes','CUST#Yes#Note_Line_#1','','','','SA059956','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE17','Cust#Yes#Note Line #1','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','CUST#Yes#Note_Line_#2',222,'Descriptive flexfield (DFF): Customer Information Column Name: Note Line #2 Context: Yes','CUST#Yes#Note_Line_#2','','','','SA059956','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE18','Cust#Yes#Note Line #2','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','CUST#Yes#Note_Line_#3',222,'Descriptive flexfield (DFF): Customer Information Column Name: Note Line #3 Context: Yes','CUST#Yes#Note_Line_#3','','','','SA059956','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE19','Cust#Yes#Note Line #3','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','CUST#Yes#Note_Line_#4',222,'Descriptive flexfield (DFF): Customer Information Column Name: Note Line #4 Context: Yes','CUST#Yes#Note_Line_#4','','','','SA059956','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE20','Cust#Yes#Note Line #4','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','CUST#Yes#Note_Line_#5',222,'Descriptive flexfield (DFF): Customer Information Column Name: Note Line #5 Context: Yes','CUST#Yes#Note_Line_#5','','','','SA059956','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE16','Cust#Yes#Note Line #5','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','CUST#Party_Type',222,'Descriptive flexfield (DFF): Customer Information Column Name: Party Type','CUST#Party_Type','','','','SA059956','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE1','Cust#Party Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','CUST#Predominant_Trade',222,'Descriptive flexfield (DFF): Customer Information Column Name: Predominant Trade','CUST#Predominant_Trade','','','','SA059956','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE9','Cust#Predominant Trade','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','CUST#Prism_Number',222,'Descriptive flexfield (DFF): Customer Information Column Name: Prism Number','CUST#Prism_Number','','','','SA059956','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE6','Cust#Prism Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','CUST#Vndr_Code_and_FRULOC',222,'Descriptive flexfield (DFF): Customer Information Column Name: Vndr Code and FRULOC','CUST#Vndr_Code_and_FRULOC','','','','SA059956','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE2','Cust#Vndr Code And Fruloc','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','SU#B2B_Address',222,'Descriptive flexfield (DFF): Site Use Information Column Name: B2B Address','SU#B2B_Address','','','','SA059956','VARCHAR2','HZ_CUST_SITE_USES_ALL','ATTRIBUTE10','Su#B2b Address','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','SU#B2B_Shipping_Warehouse',222,'Descriptive flexfield (DFF): Site Use Information Column Name: B2B Shipping Warehouse','SU#B2B_Shipping_Warehouse','','','','SA059956','VARCHAR2','HZ_CUST_SITE_USES_ALL','ATTRIBUTE11','Su#B2b Shipping Warehouse','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','SU#Customer_Site_Classificat',222,'Descriptive flexfield (DFF): Site Use Information Column Name: Customer Site Classification','SU#Customer_Site_Classificat','','','','SA059956','VARCHAR2','HZ_CUST_SITE_USES_ALL','ATTRIBUTE1','Su#Customer Site Classification','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','SU#Dodge_Number',222,'Descriptive flexfield (DFF): Site Use Information Column Name: Dodge Number','SU#Dodge_Number','','','','SA059956','VARCHAR2','HZ_CUST_SITE_USES_ALL','ATTRIBUTE4','Su#Dodge Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','SU#FUTURE_USE',222,'Descriptive flexfield (DFF): Site Use Information Column Name: FUTURE USE','SU#FUTURE_USE','','','','SA059956','VARCHAR2','HZ_CUST_SITE_USES_ALL','ATTRIBUTE5','Su#Future Use','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','SU#Government_Funded',222,'Descriptive flexfield (DFF): Site Use Information Column Name: Government Funded?','SU#Government_Funded','','','','SA059956','VARCHAR2','HZ_CUST_SITE_USES_ALL','ATTRIBUTE2','Su#Government Funded?','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','SU#Permit_Number',222,'Descriptive flexfield (DFF): Site Use Information Column Name: Permit Number','SU#Permit_Number','','','','SA059956','VARCHAR2','HZ_CUST_SITE_USES_ALL','ATTRIBUTE9','Su#Permit Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','SU#Salesrep_#2',222,'Descriptive flexfield (DFF): Site Use Information Column Name: Salesrep #2','SU#Salesrep_#2','','','','SA059956','VARCHAR2','HZ_CUST_SITE_USES_ALL','ATTRIBUTE6','Su#Salesrep #2','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','SU#Salesrep_Spilt_#1',222,'Descriptive flexfield (DFF): Site Use Information Column Name: Salesrep Spilt #1','SU#Salesrep_Spilt_#1','','','','SA059956','VARCHAR2','HZ_CUST_SITE_USES_ALL','ATTRIBUTE7','Su#Salesrep Spilt #1','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','SU#Salesrep_Spilt_#2',222,'Descriptive flexfield (DFF): Site Use Information Column Name: Salesrep Spilt #2','SU#Salesrep_Spilt_#2','','','','SA059956','VARCHAR2','HZ_CUST_SITE_USES_ALL','ATTRIBUTE8','Su#Salesrep Spilt #2','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','SU#Thomas_Guide_Page',222,'Descriptive flexfield (DFF): Site Use Information Column Name: Thomas Guide Page','SU#Thomas_Guide_Page','','','','SA059956','VARCHAR2','HZ_CUST_SITE_USES_ALL','ATTRIBUTE3','Su#Thomas Guide Page','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','PARTY#101#Collector',222,'Descriptive flexfield (DFF): Party Information Column Name: Collector Context: 101','PARTY#101#Collector','','','','SA059956','VARCHAR2','HZ_PARTIES','ATTRIBUTE3','Party#101#Collector','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','PARTY#102#Collector',222,'Descriptive flexfield (DFF): Party Information Column Name: Collector Context: 102','PARTY#102#Collector','','','','SA059956','VARCHAR2','HZ_PARTIES','ATTRIBUTE3','Party#102#Collector','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','PARTY#101#Coop_Override',222,'Descriptive flexfield (DFF): Party Information Column Name: Coop Override Context: 101','PARTY#101#Coop_Override','','','','SA059956','VARCHAR2','HZ_PARTIES','ATTRIBUTE11','Party#101#Coop Override','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','PARTY#102#Coop_Override',222,'Descriptive flexfield (DFF): Party Information Column Name: Coop Override Context: 102','PARTY#102#Coop_Override','','','','SA059956','VARCHAR2','HZ_PARTIES','ATTRIBUTE11','Party#102#Coop Override','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','PARTY#102#Default_Coop_Accou',222,'Descriptive flexfield (DFF): Party Information Column Name: Default Coop Account Segment Context: 102','PARTY#102#Default_Coop_Accou','','','','SA059956','VARCHAR2','HZ_PARTIES','ATTRIBUTE6','Party#102#Default Coop Account Segment','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','PARTY#101#Default_Coop_Accou',222,'Descriptive flexfield (DFF): Party Information Column Name: Default Coop Account Segment Context: 101','PARTY#101#Default_Coop_Accou','','','','SA059956','VARCHAR2','HZ_PARTIES','ATTRIBUTE6','Party#101#Default Coop Account Segment','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','PARTY#101#Default_Cost_Cente',222,'Descriptive flexfield (DFF): Party Information Column Name: Default Cost Center Context: 101','PARTY#101#Default_Cost_Cente','','','','SA059956','VARCHAR2','HZ_PARTIES','ATTRIBUTE8','Party#101#Default Cost Center','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','PARTY#102#Default_Cost_Cente',222,'Descriptive flexfield (DFF): Party Information Column Name: Default Cost Center Context: 102','PARTY#102#Default_Cost_Cente','','','','SA059956','VARCHAR2','HZ_PARTIES','ATTRIBUTE8','Party#102#Default Cost Center','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','PARTY#102#Default_Location_S',222,'Descriptive flexfield (DFF): Party Information Column Name: Default Location Segment Context: 102','PARTY#102#Default_Location_S','','','','SA059956','VARCHAR2','HZ_PARTIES','ATTRIBUTE5','Party#102#Default Location Segment','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','PARTY#101#Default_Location_S',222,'Descriptive flexfield (DFF): Party Information Column Name: Default Location Segment Context: 101','PARTY#101#Default_Location_S','','','','SA059956','VARCHAR2','HZ_PARTIES','ATTRIBUTE5','Party#101#Default Location Segment','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','PARTY#102#Default_Payment_Ac',222,'Descriptive flexfield (DFF): Party Information Column Name: Default Payment Account Context: 102','PARTY#102#Default_Payment_Ac','','','','SA059956','VARCHAR2','HZ_PARTIES','ATTRIBUTE13','Party#102#Default Payment Account','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','PARTY#101#Default_Payment_Ac',222,'Descriptive flexfield (DFF): Party Information Column Name: Default Payment Account Context: 101','PARTY#101#Default_Payment_Ac','','','','SA059956','VARCHAR2','HZ_PARTIES','ATTRIBUTE13','Party#101#Default Payment Account','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','PARTY#102#Default_Payment_Lo',222,'Descriptive flexfield (DFF): Party Information Column Name: Default Payment Location Context: 102','PARTY#102#Default_Payment_Lo','','','','SA059956','VARCHAR2','HZ_PARTIES','ATTRIBUTE14','Party#102#Default Payment Location','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','PARTY#101#Default_Payment_Lo',222,'Descriptive flexfield (DFF): Party Information Column Name: Default Payment Location Context: 101','PARTY#101#Default_Payment_Lo','','','','SA059956','VARCHAR2','HZ_PARTIES','ATTRIBUTE14','Party#101#Default Payment Location','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','PARTY#101#Default_Product_Se',222,'Descriptive flexfield (DFF): Party Information Column Name: Default Product Segment Context: 101','PARTY#101#Default_Product_Se','','','','SA059956','VARCHAR2','HZ_PARTIES','ATTRIBUTE4','Party#101#Default Product Segment','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','PARTY#102#Default_Product_Se',222,'Descriptive flexfield (DFF): Party Information Column Name: Default Product Segment Context: 102','PARTY#102#Default_Product_Se','','','','SA059956','VARCHAR2','HZ_PARTIES','ATTRIBUTE4','Party#102#Default Product Segment','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','PARTY#101#Default_Rebate_Acc',222,'Descriptive flexfield (DFF): Party Information Column Name: Default Rebate Account Segment Context: 101','PARTY#101#Default_Rebate_Acc','','','','SA059956','VARCHAR2','HZ_PARTIES','ATTRIBUTE7','Party#101#Default Rebate Account Segment','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','PARTY#102#Default_Rebate_Acc',222,'Descriptive flexfield (DFF): Party Information Column Name: Default Rebate Account Segment Context: 102','PARTY#102#Default_Rebate_Acc','','','','SA059956','VARCHAR2','HZ_PARTIES','ATTRIBUTE7','Party#102#Default Rebate Account Segment','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','PARTY#101#InterCompany__Paya',222,'Descriptive flexfield (DFF): Party Information Column Name: InterCompany  Payable Context: 101','PARTY#101#InterCompany__Paya','','','','SA059956','VARCHAR2','HZ_PARTIES','ATTRIBUTE9','Party#101#Intercompany  Payable','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','PARTY#102#InterCompany_Payab',222,'Descriptive flexfield (DFF): Party Information Column Name: InterCompany Payable Context: 102','PARTY#102#InterCompany_Payab','','','','SA059956','VARCHAR2','HZ_PARTIES','ATTRIBUTE9','Party#102#Intercompany Payable','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','PARTY#101#InterCompany_Recei',222,'Descriptive flexfield (DFF): Party Information Column Name: InterCompany Receivable Context: 101','PARTY#101#InterCompany_Recei','','','','SA059956','VARCHAR2','HZ_PARTIES','ATTRIBUTE10','Party#101#Intercompany Receivable','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','PARTY#102#InterCompany_Recei',222,'Descriptive flexfield (DFF): Party Information Column Name: InterCompany Receivable Context: 102','PARTY#102#InterCompany_Recei','','','','SA059956','VARCHAR2','HZ_PARTIES','ATTRIBUTE10','Party#102#Intercompany Receivable','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','PARTY#102#Party_Type',222,'Descriptive flexfield (DFF): Party Information Column Name: Party Type Context: 102','PARTY#102#Party_Type','','','','SA059956','VARCHAR2','HZ_PARTIES','ATTRIBUTE1','Party#102#Party Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','PARTY#101#Party_Type',222,'Descriptive flexfield (DFF): Party Information Column Name: Party Type Context: 101','PARTY#101#Party_Type','','','','SA059956','VARCHAR2','HZ_PARTIES','ATTRIBUTE1','Party#101#Party Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','PARTY#101#Payment_Override',222,'Descriptive flexfield (DFF): Party Information Column Name: Payment Override Context: 101','PARTY#101#Payment_Override','','','','SA059956','VARCHAR2','HZ_PARTIES','ATTRIBUTE12','Party#101#Payment Override','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','PARTY#102#Payment_Override',222,'Descriptive flexfield (DFF): Party Information Column Name: Payment Override Context: 102','PARTY#102#Payment_Override','','','','SA059956','VARCHAR2','HZ_PARTIES','ATTRIBUTE12','Party#102#Payment Override','','','','US','');
--Inserting Object Components for EIS_XXWC_AR_REVERSE_RECEIPT_V
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','AR_CASH_RECEIPTS',222,'AR_CASH_RECEIPTS_ALL','CR','CR','SA059956','SA059956','-1','Cash Receipts','','','','','ACR','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','AR_CASH_RECEIPT_HISTORY',222,'AR_CASH_RECEIPT_HISTORY_ALL','CRH1','CRH1','SA059956','SA059956','-1','Cash Receipt History','','','','','ACRH','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','CE_BANK_ACCOUNTS',222,'CE_BANK_ACCOUNTS','CBA','CBA','SA059956','SA059956','-1','Bank Accounts','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','HZ_CUST_SITE_USES',222,'HZ_CUST_SITE_USES_ALL','SU','SU','SA059956','SA059956','-1','Customer Site Uses','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','GL_LEDGERS',222,'GL_LEDGERS','GLE','GLE','SA059956','SA059956','-1','Ledger Definition','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','FND_CURRENCIES',222,'FND_CURRENCIES','CUR','CUR','SA059956','SA059956','-1','Currencies','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','HR_ORGANIZATION_UNITS',222,'HR_ALL_ORGANIZATION_UNITS','HOU','HOU','SA059956','SA059956','-1','Organizations','N','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','HZ_CUST_ACCOUNTS',222,'HZ_CUST_ACCOUNTS','CUST','CUST','SA059956','SA059956','-1','Customer Accounts','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','HZ_PARTIES',222,'HZ_PARTIES','PARTY','PARTY','SA059956','SA059956','-1','Parties','','','','','','',' ',' ',' ',' ',' ');
--Inserting Object Component Joins for EIS_XXWC_AR_REVERSE_RECEIPT_V
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','AR_CASH_RECEIPTS','CR',222,'EXARRV.CASH_RECEIPT_ID','=','CR.CASH_RECEIPT_ID(+)','','','','Y','SA059956');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','AR_CASH_RECEIPT_HISTORY','CRH1',222,'EXARRV.CASH_RECEIPT_HISTORY_ID','=','CRH1.CASH_RECEIPT_HISTORY_ID(+)','','','','Y','SA059956');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','CE_BANK_ACCOUNTS','CBA',222,'EXARRV.BANK_ACCOUNT_ID','=','CBA.BANK_ACCOUNT_ID(+)','','','','Y','SA059956');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','HZ_CUST_SITE_USES','SU',222,'EXARRV.SITE_USE_ID','=','SU.SITE_USE_ID(+)','','','','Y','SA059956');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','GL_LEDGERS','GLE',222,'EXARRV.COMPANY_NAME','=','GLE.NAME(+)','','','','Y','SA059956');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','FND_CURRENCIES','CUR',222,'EXARRV.CURRENCY_CODE','=','CUR.CURRENCY_CODE(+)','','','','Y','SA059956');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','HR_ORGANIZATION_UNITS','HOU',222,'EXARRV.ORGANIZATION_ID','=','HOU.ORGANIZATION_ID(+)','','','','Y','SA059956');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','HZ_CUST_ACCOUNTS','CUST',222,'EXARRV.CUST_ACCOUNT_ID','=','CUST.CUST_ACCOUNT_ID(+)','','','','Y','SA059956');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_AR_REVERSE_RECEIPT_V','HZ_PARTIES','PARTY',222,'EXARRV.PARTY_ID','=','PARTY.PARTY_ID(+)','','','','Y','SA059956');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 222');
END IF;
END;
/
prompt Creating Report LOV Data for Reversed Receipts Report - WC
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(222);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - Reversed Receipts Report - WC
xxeis.eis_rsc_ins.lov( 222,'SELECT hou.NAME organization_name,
       TO_CHAR (date_from,
                NVL (fnd_profile.VALUE (''ICX_DATE_FORMAT_MASK''),
                     ''DD-MON-YYYY'')
               ) date_from,
       TO_CHAR (date_to,
                NVL (fnd_profile.VALUE (''ICX_DATE_FORMAT_MASK''),
                     ''DD-MON-YYYY'')
               ) date_to
  FROM hr_operating_units hou,
       mo_glob_org_access_tmp tmp
 WHERE hou.organization_id = tmp.organization_id','','AR Multi Operating Unit Name LOV','This LOV shows all the operating units, to which user has access.','XXEIS_RS_ADMIN',NULL,'N','','','Y','','','','US');
xxeis.eis_rsc_ins.lov( 222,'select distinct party_name from hz_parties p, hz_cust_accounts c
	where p.party_id = c.party_id','','AR Customer Names LOV','This LOV displays Customer Names.','XXEIS_RS_ADMIN',NULL,'','','','Y','','','','US');
xxeis.eis_rsc_ins.lov( 222,'select CURRENCY_CODE, NAME from
FND_CURRENCIES_VL
WHERE CURRENCY_FLAG = ''Y''
AND ENABLED_FLAG in (''Y'', ''N'')
AND CURRENCY_CODE NOT IN(''ANY'')
ORDER BY CURRENCY_CODE','','AR Currency Codes LOV','This LOV lists all Currency Codes','XXEIS_RS_ADMIN',NULL,'','','','Y','','','','US');
xxeis.eis_rsc_ins.lov( 222,'select account_number Customer_Number from hz_cust_accounts','','AR Customer Number LOV','This LOV lists all Customer Numbers.','XXEIS_RS_ADMIN',NULL,'','','','Y','','','','US');
xxeis.eis_rsc_ins.lov( 222,'select cba.bank_account_name
from  ce_bank_acct_uses ba,
      ce_bank_accounts cba,
      ce_bank_branches_v bb
      where  ba.bank_account_id = cba.bank_account_id
      AND cba.bank_branch_id = bb.branch_party_id
      AND cba.account_classification = ''INTERNAL''','','AR Bank Account Names LOV','This LOV lists all Bank Account Names','XXEIS_RS_ADMIN',NULL,'N','','','Y','','','','US');
xxeis.eis_rsc_ins.lov( 222,'SELECT  MEANING
FROM AR_LOOKUPS
where LOOKUP_TYPE = ''CKAJST_REASON'' order by MEANING','','AR Reversed Receipt Reasons LOV','This LOV displays all Reversed Receipts Reasons','XXEIS_RS_ADMIN',NULL,'','','','Y','','','','US');
xxeis.eis_rsc_ins.lov( 222,'select ''Customer''  ORDER_BY from DUAL
union
select ''Remittance Bank'' ORDER_BY from DUAL','','AR Orber By LOV','','XXEIS_RS_ADMIN',NULL,'N','','','N','N','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 222');
END IF;
END;
/
prompt Creating Report Data for Reversed Receipts Report - WC
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(222);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Reversed Receipts Report - WC
xxeis.eis_rsc_utility.delete_report_rows( 'Reversed Receipts Report - WC',222 );
--Inserting Report - Reversed Receipts Report - WC
xxeis.eis_rsc_ins.r( 222,'Reversed Receipts Report - WC','','This Report is copied from Reversed Receipts Report.This report displays the receipt reversals. The report can be used to monitor receipts that have been reversed when the customer stops payment on a receipt or if a receipt comes from an account with non-sufficient funds. The report can be run by any of Customer name  or  Customer number or Bank Account Name or Reversal GL Start Date or Reversal GL End Date or Currency or Reversed Receipt Reason Parameters.','804','','','SA059956','EIS_XXWC_AR_REVERSE_RECEIPT_V','Y','','','SA059956','','N','Receipts','RTF,PDF,','CSV,EXCEL,Pivot Excel,','N','','','','','','N','','US','','Reversed Receipts Report','','','','','','','','','','','','','','');
--Inserting Report Columns - Reversed Receipts Report - WC
xxeis.eis_rsc_ins.rc( 'Reversed Receipts Report - WC',222,'CUSTOMER_NUMBER','Customer Number','Customer Number','','','default','','2','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_REVERSE_RECEIPT_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Reversed Receipts Report - WC',222,'LOCATION','Location','Site use identifier','','','default','','3','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_REVERSE_RECEIPT_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Reversed Receipts Report - WC',222,'BANK_ACCOUNT_NAME','Bank Account Name','Bank account name','','','default','','4','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_REVERSE_RECEIPT_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Reversed Receipts Report - WC',222,'RECEIPT_NUMBER','Receipt Number','Receipt Number','','','default','','5','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_REVERSE_RECEIPT_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Reversed Receipts Report - WC',222,'REVERSAL_TYPE','Reversal Type','QuickCode meaning','','','default','','8','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_REVERSE_RECEIPT_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Reversed Receipts Report - WC',222,'REASON','Reason','Reason','','','default','','10','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_REVERSE_RECEIPT_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Reversed Receipts Report - WC',222,'CURRENCY','Currency','Currency','','','default','','11','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_REVERSE_RECEIPT_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Reversed Receipts Report - WC',222,'AMOUNT','Amount','Amount','','~~~','default','','12','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_REVERSE_RECEIPT_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Reversed Receipts Report - WC',222,'FUNCTIONAL_AMOUNT','Functional Amount','Functional Amount','','~T~D~2','default','','13','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_REVERSE_RECEIPT_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Reversed Receipts Report - WC',222,'COMPANY_NAME','Company Name','Accounting books name','','','default','','14','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_REVERSE_RECEIPT_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Reversed Receipts Report - WC',222,'CUSTOMER_NAME','Customer Name','Customer Name','','','default','','1','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_REVERSE_RECEIPT_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Reversed Receipts Report - WC',222,'OPERATING_UNIT','Operating Unit','Operating Unit','','','default','','15','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_REVERSE_RECEIPT_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Reversed Receipts Report - WC',222,'REVERSAL_GL_DATE','Reversal Gl Date','Reversal Gl Date','','','default','','9','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_REVERSE_RECEIPT_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Reversed Receipts Report - WC',222,'MATURITY_DATE','Maturity Date','Maturity Date','','','default','','16','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_REVERSE_RECEIPT_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Reversed Receipts Report - WC',222,'ORIGINAL_GL_DATE','Original GL Date','Original Gl Date','','','default','','17','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_REVERSE_RECEIPT_V','','','','US','','');
--Inserting Report Parameters - Reversed Receipts Report - WC
xxeis.eis_rsc_ins.rp( 'Reversed Receipts Report - WC',222,'Operating Unit','Operating Unit','OPERATING_UNIT','IN','AR Multi Operating Unit Name LOV','','VARCHAR2','N','Y','1','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_AR_REVERSE_RECEIPT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Reversed Receipts Report - WC',222,'Customer Name','Customer Name','CUSTOMER_NAME','IN','AR Customer Names LOV','','VARCHAR2','N','Y','7','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_AR_REVERSE_RECEIPT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Reversed Receipts Report - WC',222,'Customer Number','Customer Number','CUSTOMER_NUMBER','IN','AR Customer Number LOV','','NUMBER','N','Y','6','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_AR_REVERSE_RECEIPT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Reversed Receipts Report - WC',222,'Bank Account Name','Bank Account Name','BANK_ACCOUNT_NAME','IN','AR Bank Account Names LOV','','VARCHAR2','N','Y','3','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_AR_REVERSE_RECEIPT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Reversed Receipts Report - WC',222,'Currency','Currency','CURRENCY','IN','AR Currency Codes LOV','','VARCHAR2','N','Y','2','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_AR_REVERSE_RECEIPT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Reversed Receipts Report - WC',222,'Reversed Receipt Reason','Reversed Receipt Reason','REASON','IN','AR Reversed Receipt Reasons LOV','','VARCHAR2','N','Y','8','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_AR_REVERSE_RECEIPT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Reversed Receipts Report - WC',222,'Reversal GL End Date','Reversal GL End Date','REVERSAL_GL_DATE','<=','','','DATE','N','Y','5','N','Y','CONSTANT','SA059956','Y','N','','End Date','','EIS_XXWC_AR_REVERSE_RECEIPT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Reversed Receipts Report - WC',222,'Reversal GL Start Date','Reversal GL Start Date','REVERSAL_GL_DATE','>=','','','DATE','N','Y','4','N','Y','CONSTANT','SA059956','Y','N','','Start Date','','EIS_XXWC_AR_REVERSE_RECEIPT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Reversed Receipts Report - WC',222,'Order By','Order By','','','AR Orber By LOV','','VARCHAR2','N','Y','9','N','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_AR_REVERSE_RECEIPT_V','','','US','');
--Inserting Dependent Parameters - Reversed Receipts Report - WC
--Inserting Report Conditions - Reversed Receipts Report - WC
xxeis.eis_rsc_ins.rcnh( 'Reversed Receipts Report - WC',222,'EXARRV.OPERATING_UNIT IN Operating Unit','SIMPLE','','','Y','','1');
xxeis.eis_rsc_ins.rcnd( '','','OPERATING_UNIT','','Operating Unit','','','','','EIS_XXWC_AR_REVERSE_RECEIPT_V','','','','','','IN','Y','Y','','','','','1',222,'Reversed Receipts Report - WC','EXARRV.OPERATING_UNIT IN Operating Unit');
xxeis.eis_rsc_ins.rcnh( 'Reversed Receipts Report - WC',222,'EXARRV.CURRENCY IN Currency','SIMPLE','','','Y','','2');
xxeis.eis_rsc_ins.rcnd( '','','CURRENCY','','Currency','','','','','EIS_XXWC_AR_REVERSE_RECEIPT_V','','','','','','IN','Y','Y','','','','','1',222,'Reversed Receipts Report - WC','EXARRV.CURRENCY IN Currency');
xxeis.eis_rsc_ins.rcnh( 'Reversed Receipts Report - WC',222,'EXARRV.BANK_ACCOUNT_NAME IN Bank Account Name','SIMPLE','','','Y','','3');
xxeis.eis_rsc_ins.rcnd( '','','BANK_ACCOUNT_NAME','','Bank Account Name','','','','','EIS_XXWC_AR_REVERSE_RECEIPT_V','','','','','','IN','Y','Y','','','','','1',222,'Reversed Receipts Report - WC','EXARRV.BANK_ACCOUNT_NAME IN Bank Account Name');
xxeis.eis_rsc_ins.rcnh( 'Reversed Receipts Report - WC',222,'EXARRV.REVERSAL_GL_DATE >= Reversal GL Start Date','SIMPLE','','','Y','','4');
xxeis.eis_rsc_ins.rcnd( '','','REVERSAL_GL_DATE','','Reversal GL Start Date','','','','','EIS_XXWC_AR_REVERSE_RECEIPT_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',222,'Reversed Receipts Report - WC','EXARRV.REVERSAL_GL_DATE >= Reversal GL Start Date');
xxeis.eis_rsc_ins.rcnh( 'Reversed Receipts Report - WC',222,'EXARRV.REVERSAL_GL_DATE <= Reversal GL End Date','SIMPLE','','','Y','','5');
xxeis.eis_rsc_ins.rcnd( '','','REVERSAL_GL_DATE','','Reversal GL End Date','','','','','EIS_XXWC_AR_REVERSE_RECEIPT_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',222,'Reversed Receipts Report - WC','EXARRV.REVERSAL_GL_DATE <= Reversal GL End Date');
xxeis.eis_rsc_ins.rcnh( 'Reversed Receipts Report - WC',222,'EXARRV.CUSTOMER_NUMBER IN Customer Number','SIMPLE','','','Y','','6');
xxeis.eis_rsc_ins.rcnd( '','','CUSTOMER_NUMBER','','Customer Number','','','','','EIS_XXWC_AR_REVERSE_RECEIPT_V','','','','','','IN','Y','Y','','','','','1',222,'Reversed Receipts Report - WC','EXARRV.CUSTOMER_NUMBER IN Customer Number');
xxeis.eis_rsc_ins.rcnh( 'Reversed Receipts Report - WC',222,'EXARRV.CUSTOMER_NAME IN Customer Name','SIMPLE','','','Y','','7');
xxeis.eis_rsc_ins.rcnd( '','','CUSTOMER_NAME','','Customer Name','','','','','EIS_XXWC_AR_REVERSE_RECEIPT_V','','','','','','IN','Y','Y','','','','','1',222,'Reversed Receipts Report - WC','EXARRV.CUSTOMER_NAME IN Customer Name');
xxeis.eis_rsc_ins.rcnh( 'Reversed Receipts Report - WC',222,'EXARRV.REASON IN Reversed Receipt Reason','SIMPLE','','','Y','','8');
xxeis.eis_rsc_ins.rcnd( '','','REASON','','Reversed Receipt Reason','','','','','EIS_XXWC_AR_REVERSE_RECEIPT_V','','','','','','IN','Y','Y','','','','','1',222,'Reversed Receipts Report - WC','EXARRV.REASON IN Reversed Receipt Reason');
--Inserting Report Sorts - Reversed Receipts Report - WC
xxeis.eis_rsc_ins.rs( 'Reversed Receipts Report - WC',222,'','ASC','SA059956','1','decode( :Order By , ''Customer'', CUSTOMER_NAME, ''Remittance Bank'',nvl(BANK_ACCOUNT_NAME, ''None''),CUSTOMER_NAME),decode( :Order By , ''Customer'', nvl(BANK_ACCOUNT_NAME, ''None''),''Remittance Bank'', CUSTOMER_NAME, nvl(BANK_ACCOUNT_NAME, ''None''))');
--Inserting Report Triggers - Reversed Receipts Report - WC
--inserting report templates - Reversed Receipts Report - WC
xxeis.eis_rsc_ins.r_tem( 'Reversed Receipts Report - WC','Reversed Receipts Report','Seeded template for Reversed Receipts Report','','','','','','','','','','','Reversed Receipts Report.rtf','SA059956','X','','','Y','Y','N','N');
xxeis.eis_rsc_ins.r_tem( 'Reversed Receipts Report - WC','AR Reversed Receipts Report','Seeded Template for AR Reversed Receipts Report','','','','','','','','','','','Reversed Receipts Report.rtf','SA059956','X','','','Y','Y','N','N');
--Inserting Report Portals - Reversed Receipts Report - WC
--inserting report dashboards - Reversed Receipts Report - WC
xxeis.eis_rsc_ins.R_dash( 'Reversed Receipts Report - WC','AR Reversed Receipts Report','AR Reversed Receipts Report','pie','large','Bank Account Name','Bank Account Name','','Amount','Sum','SA059956');
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Reversed Receipts Report - WC','222','EIS_XXWC_AR_REVERSE_RECEIPT_V','EIS_XXWC_AR_REVERSE_RECEIPT_V','N','');
--inserting report security - Reversed Receipts Report - WC
xxeis.eis_rsc_ins.rsec( 'Reversed Receipts Report - WC','222','','XXWC_CRE_ASSOC_CUST_MAINT',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Reversed Receipts Report - WC','222','','XXWC_CRE_ASSOC_COLLECTIONS',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Reversed Receipts Report - WC','222','','XXWC_CRE_ASSOC_CASH_APP',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Reversed Receipts Report - WC','222','','XXWC_CRE_ASSOC_CASH_APP_MGR',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Reversed Receipts Report - WC','222','','XXWC_CRE_CUST_IFACE',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Reversed Receipts Report - WC','222','','XXWC_CRE_CREDIT_COLL_MGR',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Reversed Receipts Report - WC','222','','XXWC_AR_IRECEIVABLES',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Reversed Receipts Report - WC','222','','HDS_RCVBLS_MNGR',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Reversed Receipts Report - WC','222','','HDS_RCVBLS_MNGR_CAN',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Reversed Receipts Report - WC','222','','XXWC_RECEIVABLES_INQUIRY_WC',222,'SA059956','','','');
--Inserting Report Pivots - Reversed Receipts Report - WC
xxeis.eis_rsc_ins.rpivot( 'Reversed Receipts Report - WC',222,'Pivot','1','1,0|1,2,1','1,1,0,0||2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rsc_ins.rpivot_dtls( 'Reversed Receipts Report - WC',222,'Pivot','CUSTOMER_NUMBER','PAGE_FIELD','','Customer Number','0','1','xlNormal','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Reversed Receipts Report - WC',222,'Pivot','LOCATION','PAGE_FIELD','','Location','0','1','xlNormal','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Reversed Receipts Report - WC',222,'Pivot','BANK_ACCOUNT_NAME','ROW_FIELD','','','3','1','xlNormal','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Reversed Receipts Report - WC',222,'Pivot','RECEIPT_NUMBER','ROW_FIELD','','','4','1','xlNormal','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Reversed Receipts Report - WC',222,'Pivot','REVERSAL_TYPE','PAGE_FIELD','','','5','1','xlNormal','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Reversed Receipts Report - WC',222,'Pivot','REASON','PAGE_FIELD','','','6','1','xlNormal','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Reversed Receipts Report - WC',222,'Pivot','CURRENCY','PAGE_FIELD','','','7','1','xlNormal','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Reversed Receipts Report - WC',222,'Pivot','AMOUNT','DATA_FIELD','SUM','','1','1','xlNormal','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Reversed Receipts Report - WC',222,'Pivot','FUNCTIONAL_AMOUNT','DATA_FIELD','SUM','','2','1','xlNormal','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Reversed Receipts Report - WC',222,'Pivot','COMPANY_NAME','PAGE_FIELD','','Company Name','0','1','xlNormal','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Reversed Receipts Report - WC',222,'Pivot','CUSTOMER_NAME','ROW_FIELD','','Customer Name','2','1','xlNormal','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Reversed Receipts Report - WC',222,'Pivot','OPERATING_UNIT','ROW_FIELD','','','1','1','xlNormal','');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
--Inserting Report Distribution Details 
--Inserting Report Distribution Details 
--Inserting Report SubTotals Details 
--Inserting Report aggregations 
--Inserting Report   Version details- Reversed Receipts Report - WC
xxeis.eis_rsc_ins.rv( 'Reversed Receipts Report - WC','804','','SA059956','18-AUG-2017');
xxeis.eis_rsc_ins.rv( 'Reversed Receipts Report - WC','802','','XXEIS_RS_ADMIN','18-AUG-2017');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 222');
END IF;
END;
/
