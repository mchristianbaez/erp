/******************************************************************************
   NAME:       TMS_20170823_00072_DATA_FIX_DELETE_DUPLICATE_CSP.sql
   PURPOSE:    Delete IN PROGRESS records of csp which are duplicate
   
   REVISIONS:
   Ver        Date        Author               Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        23/08/2017  Niraj K Ranjan   Initial Version TMS#20170823-00072   Data fix - delete record for CSP #15521 from dashboard
******************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20170823-00072   , Before Update');

   DELETE FROM apps.xxwc_om_csp_notifications_tbl WHERE ROWID IN
     (SELECT aa.rowid FROM 
      apps.xxwc_om_csp_notifications_tbl aa,
      (SELECT MAX(submitted_date) submitted_date,agreement_id,revision_number 
      FROM apps.xxwc_om_csp_notifications_tbl
      WHERE 1=1
      AND  agreement_status = 'IN PROGRESS'
      GROUP BY agreement_id,revision_number
      HAVING COUNT(1) > 1) bb
      WHERE aa.agreement_id = bb.agreement_id
      AND aa.revision_number = bb.revision_number
      AND aa.submitted_date <> bb.submitted_date);
   DBMS_OUTPUT.put_line ('TMS: 20170823-00072    , Total records deleted: '|| SQL%ROWCOUNT);
   UPDATE XXWC_OM_CSP_NOTIFICATIONS_TBL xocn
   SET  agreement_status = 'AWAITING_APPROVAL' 
   WHERE 1=1
   AND   xocn.agreement_id = 333526
   AND   xocn.revision_number = 0
   AND   agreement_status IN ('IN PROGRESS');
   DBMS_OUTPUT.put_line ('TMS: 20170823-00072    , Total records Updated for agreement id 333526: '|| SQL%ROWCOUNT);
   
   UPDATE XXWC_OM_CSP_NOTIFICATIONS_TBL xocn
   SET  agreement_status = 'AWAITING_APPROVAL' ,
        last_updated_by  = 16290
   WHERE 1=1
   AND   xocn.agreement_id = 322494
   AND   xocn.revision_number = 0
   AND   agreement_status IN ('IN PROGRESS');
   DBMS_OUTPUT.put_line ('TMS: 20170823-00072    , Total records Updated for agreement id 322494: '|| SQL%ROWCOUNT);
   
   UPDATE XXWC_OM_CSP_NOTIFICATIONS_TBL xocn
   SET  agreement_status = 'AWAITING_APPROVAL' ,
        last_updated_by  = 24537
   WHERE 1=1
   AND   xocn.agreement_id = 328500
   AND   xocn.revision_number = 0
   AND   agreement_status IN ('IN PROGRESS');
   DBMS_OUTPUT.put_line ('TMS: 20170823-00072    , Total records Updated for agreement id 328500: '|| SQL%ROWCOUNT);
   
   UPDATE XXWC_OM_CSP_NOTIFICATIONS_TBL xocn
   SET  agreement_status = 'AWAITING_APPROVAL'
   WHERE 1=1
   AND   xocn.agreement_id = 331518
   AND   xocn.revision_number = 0
   AND   agreement_status IN ('IN PROGRESS');
   DBMS_OUTPUT.put_line ('TMS: 20170823-00072    , Total records Updated for agreement id 331518: '|| SQL%ROWCOUNT);

   COMMIT;
   DBMS_OUTPUT.put_line ('TMS: 20170823-00072    , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20170823-00072 , Errors : ' || SQLERRM);
END;
/
