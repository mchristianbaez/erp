  /********************************************************************************
  FILE NAME: XXWC_DLS_CONNECTOR_PKG
  
  Granting previliges on XXWC_DLS_CONNECTOR_PKG package to apps.
  
  HISTORY
  ==========================================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- ----------------------------------------------------
  1.0     01/19/2018  Nancy Pahwa     TMS#20171108-00066 - Issue with populating cross reference
  *******************************************************************************************/

 GRANT ALL ON "DLS"."XXWC_DLS_CONNECTOR_PKG" APPS;
