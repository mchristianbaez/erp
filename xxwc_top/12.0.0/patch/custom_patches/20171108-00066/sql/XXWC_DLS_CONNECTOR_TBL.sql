  /********************************************************************************
  FILE NAME: xxwc.XXWC_DLS_CONNECTOR_TBL
  
  PROGRAM TYPE:table for edqp requests
  
  PURPOSE: table for edqp requests
  
  HISTORY
  ==========================================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- ----------------------------------------------------
  1.0     01/19/2018  Nancy Pahwa     TMS#20171108-00066 - Issue with populating cross reference
  *******************************************************************************************/
create table xxwc.XXWC_DLS_CONNECTOR_TBL
(
  itemnumber        VARCHAR2(32),
  orgid             VARCHAR2(10),
  manu_part_number  VARCHAR2(32),
  upc               VARCHAR2(32),
  jobid             VARCHAR2(10),
  xref_owner        VARCHAR2(100),
  xref_owner_vendor VARCHAR2(100),
  request_id        NUMBER,
  creation_date     DATE default SYSDATE,
  start_time        DATE,
  end_time          DATE,
  process_flag      VARCHAR2(100)
);

CREATE OR REPLACE SYNONYM DLS.XXWC_DLS_CONNECTOR_TBL FOR XXWC.XXWC_DLS_CONNECTOR_TBL;

GRANT ALL ON XXWC.XXWC_DLS_CONNECTOR_TBL TO DLS;


