  /********************************************************************************
  FILE NAME: XXWC_DLS_CONNECTOR_PKG
  
  Creating synonym for XXWC_DLS_CONNECTOR_PKG package in apps.
  
  HISTORY
  ==========================================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- ----------------------------------------------------
  1.0     01/19/2018  Nancy Pahwa     TMS#20171108-00066 - Issue with populating cross reference
  *******************************************************************************************/

  CREATE OR REPLACE SYNONYM "APPS"."XXWC_DLS_CONNECTOR_PKG" FOR "DLS"."XXWC_DLS_CONNECTOR_PKG";
