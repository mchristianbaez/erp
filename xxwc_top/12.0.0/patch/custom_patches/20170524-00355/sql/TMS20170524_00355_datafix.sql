/****************************************************************************************************
  Description: Updating order line fulfilled_quantity. 

  HISTORY
  =====================================================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- ----------------------------------------------------------
  1.0     26-May-2017        P.Vamshidhar    TMS#20170524_00355
  *****************************************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000

BEGIN
   DBMS_OUTPUT.put_line ('Before Update');

   UPDATE oe_order_lines_all
      SET fulfilled_quantity = 4,
          last_update_date = SYSDATE,
          last_updated_by = -26139282
    WHERE line_id = 96415636;

   DBMS_OUTPUT.put_line (
      'Records updated(oe_order_lines_all) :' || SQL%ROWCOUNT);

   UPDATE WSH_DELIVERY_DETAILS
      SET OE_INTERFACED_FLAG = 'Y'
    WHERE     RELEASED_STATUS = 'C'
          AND OE_INTERFACED_FLAG = 'P'
          AND SOURCE_CODE = 'OE'
          AND source_line_id = 96415636;

   DBMS_OUTPUT.put_line (
      'Records updated(WSH_DELIVERY_DETAILS) :' || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('After Update');
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Unable to update ' || SQLERRM);
END;
/