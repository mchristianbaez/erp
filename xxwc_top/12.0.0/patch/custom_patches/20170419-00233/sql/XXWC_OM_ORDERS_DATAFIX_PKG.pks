CREATE or REPLACE 
PACKAGE xxwc_om_orders_datafix_pkg AUTHID CURRENT_USER
AS
   /********************************************************************************************************************************
      $Header XXWC_OM_ORDERS_DATAFIX_PKG.PKS $
      Module Name: XXWC_OM_ORDERS_DATAFIX_PKG.PKS

      PURPOSE:   This package is used for data fix of order management

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------------------------------------------------------------
      1.0        22-Sep-2016 Niraj K Ranjan          Initial Version - TMS#20160911-00002   Data fix scripts into Concurrent program
	  
	  2.0       21-June-2017 Sundaramoorthy         Initial Version for TMS #20170419-00233     
 	 
   *********************************************************************************************************************************/
   
   PROCEDURE execute_data_fixes ( p_errbuf         OUT VARCHAR2
                                 ,p_retcode        OUT NUMBER
                                 ,p_start_date     IN  VARCHAR2
                                 );
	
	--Ver 2.0 Start
   PROCEDURE execute_rental_line_fixes(
                                         p_errbuf      OUT VARCHAR2 
                                        ,p_retcode     OUT NUMBER 
										,p_order_number IN NUMBER
                                        ,p_line_id     IN NUMBER 
                                        ,p_new_line_id IN NUMBER 
									    ,p_quantity    In NUMBER
								        );
     
    PROCEDURE pending_prebill_datafix(
                                         p_errbuf      OUT VARCHAR2 
                                        ,p_retcode     OUT NUMBER 
                                        ,p_header_id  IN NUMBER 
                                        ,p_line_id    IN NUMBER
										,p_shipment_date IN VARCHAR2
								        );	
    PROCEDURE header_status_entered(  
	                                    p_errbuf OUT VARCHAR2
									   ,p_retcode OUT VARCHAR2
									   );
									   
									   
	PROCEDURE insert_record_log (       p_order_header_id IN  NUMBER
                                       ,p_order_line_id   IN  NUMBER
                                       ,p_request_id      IN  NUMBER
                                       ,p_script_name     IN  VARCHAR2
                                       );	
									   
    PROCEDURE RETURN_LINE_RECEIVED (  p_errbuf    OUT VARCHAR2,
                                     p_retcode   OUT  VARCHAR2
									 );								 									   
    									   
	--Ver 2.0 End							 
END xxwc_om_orders_datafix_pkg;
/
