CREATE OR REPLACE PACKAGE APPS.XXCUS_OZF_LOB_ROR_PKG
-- Scope: Oracle Trade Management / Rebates
-- Used by concurrent jobs HDS Rebates: Generate WC Rebate Rate of Return Extract
--
-- Change History:
--  Ticket                                                 Date                      Author                                                       Notes
 --  =========================   =============   ============================== ==========================================================
 --  TMS 20160809-00142                08/09/2016         Balaguru Seshadri                                 ESMS 430286 - New routine to extract White Cap ROR Data
as
  --
  -- Extract Whitecap rebate rate of return always for the current year
  --
  procedure extract_wc_ror
  (
    retcode               out number
   ,errbuf                out varchar2
  );  
  --
end XXCUS_OZF_LOB_ROR_PKG;
/