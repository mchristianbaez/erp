/*
 TMS: 20160624-00201 
 Date: 07/05/2016
 Scope: Update xxwc.xxwc_wsh_shipping_stg.subinventory to 'General' for all records status in ('OUT_FOR_DELIVERY','OUT_FOR_DELIVERY/PARTIAL_BACKORDER').
 Notes: none
*/
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
BEGIN --Main Processing...
   dbms_output.put_line('Before updating xxwc_wsh_shipping_stg table.');
   dbms_output.put_line(' ');

   UPDATE xxwc.xxwc_wsh_shipping_stg
      SET subinventory = 'General'
    WHERE subinventory IS NULL
      AND status IN ('OUT_FOR_DELIVERY','OUT_FOR_DELIVERY/PARTIAL_BACKORDER');

   dbms_output.put_line(' ');
   dbms_output.put_line('Total rows updated : '||SQL%rowcount);
   dbms_output.put_line(' ');

   COMMIT;

EXCEPTION
   WHEN OTHERS THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20160624-00201, Errors =' || SQLERRM);
END;
/