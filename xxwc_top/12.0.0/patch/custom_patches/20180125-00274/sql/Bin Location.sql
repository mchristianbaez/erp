--Report Name            : Bin Location
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data EIS_XXWC_INV_BIN_LOC_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(401);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_INV_BIN_LOC_V
xxeis.eis_rsc_ins.v( 'EIS_XXWC_INV_BIN_LOC_V',401,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Rs Xxwc Inv Bin Loc V','EXIBLV','','','VIEW','US','','','');
--Delete Object Columns for EIS_XXWC_INV_BIN_LOC_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_INV_BIN_LOC_V',401,FALSE);
--Inserting Object Columns for EIS_XXWC_INV_BIN_LOC_V
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','YTD_SALES',401,'Ytd Sales','YTD_SALES','','','','XXEIS_RS_ADMIN','VARCHAR2','DERIVED COLUMN','DERIVED COLUMN','Ytd Sales','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','MTD_SALES',401,'Mtd Sales','MTD_SALES','','','','XXEIS_RS_ADMIN','VARCHAR2','DERIVED COLUMN','DERIVED COLUMN','Mtd Sales','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','ONHAND',401,'Onhand','ONHAND','','','','XXEIS_RS_ADMIN','NUMBER','MTL_ONHAND_QUANTITIES_DETAIL','TRANSACTION_QUANTITY','Onhand','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','AVERAGECOST',401,'Averagecost','AVERAGECOST','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','CALCULATION COLUMN','CALCULATION COLUMN','Averagecost','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','MAX_MINMAX_QUANTITY',401,'Max Minmax Quantity','MAX_MINMAX_QUANTITY','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','MTL_SYSTEM_ITEMS_KFV','MAX_MINMAX_QUANTITY','Max Minmax Quantity','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','MIN_MINMAX_QUANTITY',401,'Min Minmax Quantity','MIN_MINMAX_QUANTITY','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','MTL_SYSTEM_ITEMS_KFV','MIN_MINMAX_QUANTITY','Min Minmax Quantity','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','UOM',401,'Uom','UOM','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_KFV','PRIMARY_UOM_CODE','Uom','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','DESCRIPTION',401,'Description','DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_KFV','DESCRIPTION','Description','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','PART_NUMBER',401,'Part Number','PART_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_KFV','SEGMENT1','Part Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','ORGANIZATION',401,'Organization','ORGANIZATION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Organization','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','ALTERNATE_BIN_LOC',401,'Alternate Bin Loc','ALTERNATE_BIN_LOC','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Alternate Bin Loc','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','BIN_LOC',401,'Bin Loc','BIN_LOC','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Bin Loc','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','LOCATION',401,'Location','LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Location','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','PRIMARY_BIN_LOC',401,'Primary Bin Loc','PRIMARY_BIN_LOC','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Primary Bin Loc','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','OPEN_DEMAND',401,'Open Demand','OPEN_DEMAND','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Open Demand','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','OPEN_ORDER',401,'Open Order','OPEN_ORDER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Open Order','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','STK',401,'Stk','STK','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Stk','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','CATEGORY',401,'Category','CATEGORY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Category','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','CATEGORY_SET_NAME',401,'Category Set Name','CATEGORY_SET_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Category Set Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','LAST_RECEIVED_DATE',401,'Last Received Date','LAST_RECEIVED_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Last Received Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','NO_BIN',401,'No Bin','NO_BIN','','','','XXEIS_RS_ADMIN','VARCHAR2','','','No Bin','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','WEIGHT',401,'Weight','WEIGHT','','','','XXEIS_RS_ADMIN','NUMBER','MTL_SYSTEM_ITEMS_KFV','UNIT_WEIGHT','Weight','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','VENDOR_NUMBER',401,'Vendor Number','VENDOR_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','DERIVED COLUMN','DERIVED COLUMN','Vendor Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','VENDOR_NAME',401,'Vendor Name','VENDOR_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','DERIVED COLUMN','DERIVED COLUMN','Vendor Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','SELLING_PRICE',401,'Selling Price','SELLING_PRICE','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','MTL_SYSTEM_ITEMS_KFV','LIST_PRICE_PER_UNIT','Selling Price','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','CAT',401,'Cat','CAT','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_CATEGORIES','SEGMENT2','Cat','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','INVENTORY_ITEM_ID',401,'Inventory Item Id','INVENTORY_ITEM_ID','','','','XXEIS_RS_ADMIN','NUMBER','MTL_SYSTEM_ITEMS_KFV','INVENTORY_ITEM_ID','Inventory Item Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','INV_ORGANIZATION_ID',401,'Inv Organization Id','INV_ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','MTL_SYSTEM_ITEMS_KFV','ORGANIZATION_ID','Inv Organization Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','ORG_ORGANIZATION_ID',401,'Org Organization Id','ORG_ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','ORG_ORGANIZATION_DEFINITIONS','ORGANIZATION_ID','Org Organization Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','APPLICATION_ID',401,'Application Id','APPLICATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','GL_PERIOD_STATUSES','APPLICATION_ID','Application Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','SET_OF_BOOKS_ID',401,'Set Of Books Id','SET_OF_BOOKS_ID','','','','XXEIS_RS_ADMIN','NUMBER','GL_PERIOD_STATUSES','SET_OF_BOOKS_ID','Set Of Books Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','PROCESS_ID',401,'Process Id','PROCESS_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Process Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','SUBINVENTORY',401,'Subinventory','SUBINVENTORY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Subinventory','','','','','');
--Inserting Object Components for EIS_XXWC_INV_BIN_LOC_V
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_INV_BIN_LOC_V','MTL_SYSTEM_ITEMS_KFV',401,'MTL_SYSTEM_ITEMS_KFV','MSI','MSI','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Inventory Item Definitions','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_INV_BIN_LOC_V','MTL_CATEGORIES',401,'MTL_CATEGORIES_B','MCV','MCV','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Categories','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_INV_BIN_LOC_V','GL_PERIOD_STATUSES',401,'GL_PERIOD_STATUSES','GPS','GPS','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Calendar Period Statuses','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_INV_BIN_LOC_V','GL_CODE_COMBINATIONS_KFV',401,'GL_CODE_COMBINATIONS','GCC','GCC','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','GL Code Combinations','','','','','','',' ',' ',' ',' ',' ');
--Inserting Object Component Joins for EIS_XXWC_INV_BIN_LOC_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 401');
END IF;
END;
/
prompt Creating Report LOV Data for Bin Location
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(401);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - Bin Location
xxeis.eis_rsc_ins.lov( '','Select ''Yes''  yes_no
from dual
Union
Select ''No'' yes_no
from dual','','Yes_No_Lov','This lov provides two values yes,no.','XXEIS_RS_ADMIN',NULL,'N','','','Y','N','','','US');
xxeis.eis_rsc_ins.lov( 401,'SELECT   category_set_name,
         description
    FROM mtl_category_sets
   WHERE mult_item_cat_assign_flag = ''N''
ORDER BY category_set_name','','EIS_INV_CATEGORY_SETS_LOV','Category Sets','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 401,'SELECT DISTINCT concatenated_segments Item_Category,
                description
           FROM mtl_categories_kfv
           order by concatenated_segments','','EIS_INV_ITEM_CATEGORIES_LOV','Item Categories','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 401,'select SECONDARY_INVENTORY_NAME SUB_INVENTORY,HAOU.NAME ORGANIZATION_NAME
from MTL_SECONDARY_INVENTORIES MSI,
     HR_ALL_ORGANIZATION_UNITS HAOU
WHERE 1=1
AND MSI.ORGANIZATION_ID = HAOU.organization_id','','EIS_INV_SUBINVENTORY_LOV','List of All SubInventories.','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 401,'SELECT distinct substr(SEGMENT1,3) bin_location FROM MTL_ITEM_LOCATIONS_KFV','','INV Bin Location','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 401,'','On Hand,No on Hand,ALL,Negative on hand','ON_HAND_TYPE_CSV','','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( '','SELECT ORGANIZATION_CODE WAREHOUSE,ORGANIZATION_NAME
FROM ORG_ORGANIZATION_DEFINITIONS OOD
WHERE SYSDATE < NVL(OOD.DISABLE_DATE,SYSDATE+1) AND EXISTS (SELECT 1 FROM XXEIS.EIS_ORG_ACCESS_V  WHERE organization_id = ood.organization_id )
UNION
SELECT ''All'', ''All Organizations'' FROM DUAL','','XXWC All ORG LIST','XXWC All ORG LIST','ANONYMOUS',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 401');
END IF;
END;
/
prompt Creating Report Data for Bin Location
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(401);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Bin Location
xxeis.eis_rsc_utility.delete_report_rows( 'Bin Location',401 );
--Inserting Report - Bin Location
xxeis.eis_rsc_ins.r( 401,'Bin Location','','Items without Primary Bin Locations Assigned','','','','SA059956','EIS_XXWC_INV_BIN_LOC_V','Y','','','SA059956','','N','White Cap Reports','','EXCEL,Pivot Excel,','N','','','','','','N','','US','','','','','','','','','','','','','','','','');
--Inserting Report Columns - Bin Location
xxeis.eis_rsc_ins.rc( 'Bin Location',401,'PRIMARY_BIN_LOC','Primary Bin','Primary Bin Loc','','','default','','14','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_BIN_LOC_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Bin Location',401,'Primary_Loc_Exists','Primary Loc Exists','Primary Bin Loc','VARCHAR2','~T~D~0','default','','17','Y','','','','','','','(decode(EXIBLV.primary_bin_loc,null,''N'',''Y''))','SA059956','N','N','','EIS_XXWC_INV_BIN_LOC_V','','','SUM','US','','');
xxeis.eis_rsc_ins.rc( 'Bin Location',401,'CATEGORY','Category','Category','','','','','19','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_BIN_LOC_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Bin Location',401,'CATEGORY_SET_NAME','Category Set Name','Category Set Name','','','','','18','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_BIN_LOC_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Bin Location',401,'ORGANIZATION','Organization','Organization','','','default','','1','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_BIN_LOC_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Bin Location',401,'ALTERNATE_BIN_LOC','Alternate Bin Loc','Alternate Bin Loc','','','default','','15','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_BIN_LOC_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Bin Location',401,'Extended_Value','Extended Value','Alternate Bin Loc','NUMBER','~T~D~2','default','','11','Y','','','','','','','(EXIBLV.ONHAND*EXIBLV.AVERAGECOST)','SA059956','N','N','','EIS_XXWC_INV_BIN_LOC_V','','','SUM','US','','');
xxeis.eis_rsc_ins.rc( 'Bin Location',401,'LOCATION','Location','Location','','','default','','16','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_BIN_LOC_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Bin Location',401,'LAST_RECEIVED_DATE','Last Received Date','Last Received Date','','','','','20','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_BIN_LOC_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Bin Location',401,'NO_BIN','No Bin','No Bin','','','','','21','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_BIN_LOC_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Bin Location',401,'AVERAGECOST','Average Cost','Averagecost','','~T~D~2','default','','9','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_BIN_LOC_V','','','SUM','US','','');
xxeis.eis_rsc_ins.rc( 'Bin Location',401,'DESCRIPTION','Description','Description','','','default','','3','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_BIN_LOC_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Bin Location',401,'MAX_MINMAX_QUANTITY','Max','Max Minmax Quantity','','~T~D~0','default','','13','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_BIN_LOC_V','','','SUM','US','','');
xxeis.eis_rsc_ins.rc( 'Bin Location',401,'MIN_MINMAX_QUANTITY','Min','Min Minmax Quantity','','~T~D~0','default','','12','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_BIN_LOC_V','','','SUM','US','','');
xxeis.eis_rsc_ins.rc( 'Bin Location',401,'ONHAND','Qty Onhand','Onhand','','~T~D~0','default','','5','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_BIN_LOC_V','','','SUM','US','','');
xxeis.eis_rsc_ins.rc( 'Bin Location',401,'PART_NUMBER','Item Number','Part Number','','','default','','2','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_BIN_LOC_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Bin Location',401,'UOM','UOM','Uom','','','default','','10','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_BIN_LOC_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Bin Location',401,'Vendor_Number-Name','Vendor Number-Name','Uom','VARCHAR2','','default','','4','Y','','','','','','','(EXIBLV.VENDOR_NUMBER||''-''||EXIBLV.VENDOR_NAME)','SA059956','N','N','','EIS_XXWC_INV_BIN_LOC_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Bin Location',401,'OPEN_DEMAND','Open Demand','Open Demand','','','','','6','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_BIN_LOC_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Bin Location',401,'OPEN_ORDER','Open Order','Open Order','','','','','7','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_BIN_LOC_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Bin Location',401,'STK','Stk','Stk','','','','','8','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_BIN_LOC_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Bin Location',401,'SUBINVENTORY','Subinventory','Subinventory','','','','','22','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_BIN_LOC_V','','','','US','','');
--Inserting Report Parameters - Bin Location
xxeis.eis_rsc_ins.rp( 'Bin Location',401,'Category From','Category From','CATEGORY','>=','EIS_INV_ITEM_CATEGORIES_LOV','','VARCHAR2','N','Y','5','N','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_INV_BIN_LOC_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Bin Location',401,'Category To','Category To','CATEGORY','<=','EIS_INV_ITEM_CATEGORIES_LOV','','VARCHAR2','N','Y','6','N','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_INV_BIN_LOC_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Bin Location',401,'No Bin Location','No Bin Location','NO_BIN','IN','Yes_No_Lov','','VARCHAR2','N','Y','7','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_INV_BIN_LOC_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Bin Location',401,'Quantity On Hand','Quantity On Hand','','IN','ON_HAND_TYPE_CSV','','VARCHAR2','N','Y','8','Y','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_INV_BIN_LOC_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Bin Location',401,'Organization','Organization','ORGANIZATION','IN','XXWC All ORG LIST','','VARCHAR2','Y','Y','1','Y','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_INV_BIN_LOC_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Bin Location',401,'Starting Bin Location','Starting Bin Location','BIN_LOC','>=','INV Bin Location','','VARCHAR2','N','Y','2','Y','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_INV_BIN_LOC_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Bin Location',401,'Ending Bin Location','Ending Bin Location','BIN_LOC','<=','INV Bin Location','','VARCHAR2','N','Y','3','Y','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_INV_BIN_LOC_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Bin Location',401,'Category Set','Category Set','CATEGORY_SET_NAME','IN','EIS_INV_CATEGORY_SETS_LOV','Sales Velocity','VARCHAR2','N','Y','4','Y','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_INV_BIN_LOC_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Bin Location',401,'Subinventory','Subinventory','','','EIS_INV_SUBINVENTORY_LOV','','VARCHAR2','N','Y','9','Y','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_INV_BIN_LOC_V','','','US','');
--Inserting Dependent Parameters - Bin Location
--Inserting Report Conditions - Bin Location
xxeis.eis_rsc_ins.rcnh( 'Bin Location',401,'NO_BIN IN :No Bin Location ','SIMPLE','','','Y','','1');
xxeis.eis_rsc_ins.rcnd( '','','NO_BIN','','No Bin Location','','','','','EIS_XXWC_INV_BIN_LOC_V','','','','','','IN','Y','Y','','','','','1',401,'Bin Location','NO_BIN IN :No Bin Location ');
xxeis.eis_rsc_ins.rcnh( 'Bin Location',401,'Free Text ','FREE_TEXT','','','Y','','2');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','','Y','','','','','and PROCESS_ID=:SYSTEM.PROCESS_ID','1',401,'Bin Location','Free Text ');
--Inserting Report Sorts - Bin Location
xxeis.eis_rsc_ins.rs( 'Bin Location',401,'PART_NUMBER','ASC','SA059956','1','');
--Inserting Report Triggers - Bin Location
xxeis.eis_rsc_ins.rt( 'Bin Location',401,'begin
xxeis.EIS_XXWC_BIN_LOCATION_PKG.g_start_bin := :Starting Bin Location;
xxeis.EIS_XXWC_BIN_LOCATION_PKG.g_end_bin  := :Ending Bin Location;
  XXEIS.EIS_XXWC_BIN_LOCATION_PKG.GET_BIN_LOCATION_INFO(
    P_PROCESS_ID 	=> :SYSTEM.PROCESS_ID,
    P_CATEGORY_FROM => :Category From,
    P_CATEGORY_TO 	=> :Category To,
    P_ORGANIZATION	=> :Organization,
    P_CATEGORY_SET  => :Category Set,
    P_QUANTITY_ON_HAND => :Quantity On Hand,
    P_SUBINVENTORY  => :Subinventory
  );
end;','B','Y','SA059956','AQ');
--inserting report templates - Bin Location
--Inserting Report Portals - Bin Location
--inserting report dashboards - Bin Location
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Bin Location','401','EIS_XXWC_INV_BIN_LOC_V','EIS_XXWC_INV_BIN_LOC_V','N','');
--inserting report security - Bin Location
xxeis.eis_rsc_ins.rsec( 'Bin Location','401','','HDS_INVNTRY',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Bin Location','401','','',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Bin Location','401','','',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Bin Location','401','','',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Bin Location','401','','XXWC_AO_REC',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Bin Location','401','','',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Bin Location','401','','',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Bin Location','20005','','',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Bin Location','401','','INVENTORY',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Bin Location','','LB048272','',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Bin Location','','10012196','',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Bin Location','401','','',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Bin Location','401','','XXWC_INVENTORY_CONTROL_INQUIRY',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Bin Location','401','','XXWC_INVENTORY_CONTROL_SR_MGR',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Bin Location','401','','XXWC_INV_PLANNER',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Bin Location','401','','XXWC_INVENTORY_SPEC_SCC',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Bin Location','401','','XXWC_INVENTORY_SUPER_USER',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Bin Location','20005','','XXWC_VIEW_ALL_EIS_REPORTS',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Bin Location','401','','XXWC_RECEIVING_ASSOCIATE',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Bin Location','401','','XXWC_AO_BIN_MTN_REC',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Bin Location','401','','XXWC_AO_BIN_MTN_CYCLE_REC',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Bin Location','401','','XXWC_AO_INV_ADJ_REC',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Bin Location','660','','XXWC_AO_OEENTRY_REC',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Bin Location','401','','XXWC_AO_BIN_MTN_CYCLE_OEENTRY',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Bin Location','401','','XXWC_AO_BIN_MTN_CYCLE_INV_ADJ',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Bin Location','401','','XXWC_WHSE_MNGR_WC',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Bin Location','401','','XXWC_AO_BIN_MTN_PO_RPT',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Bin Location','401','','XXWC_AO_BIN_MTN_OEENTRY',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Bin Location','401','','XXWC_AO_BIN_MTN',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Bin Location','707','','XXWC_COST_MANAGEMENT_INQ',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Bin Location','401','','XXWC_DATA_MNGT_SC',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Bin Location','401','','',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Bin Location','201','','XXWC_PURCHASING_INQUIRY',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Bin Location','','MM050208','',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Bin Location','','SO004816','',401,'SA059956','','','');
--Inserting Report Pivots - Bin Location
xxeis.eis_rsc_ins.rpivot( 'Bin Location',401,'Pivot','1','0,0|1,0,1','1,1,0,0|PivotStyleDark1|2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rsc_ins.rpivot_dtls( 'Bin Location',401,'Pivot','LOCATION','ROW_FIELD','','Location','1','1','xlNormal','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Bin Location',401,'Pivot','ONHAND','DATA_FIELD','SUM','','1','','','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Bin Location',401,'Pivot','PART_NUMBER','ROW_FIELD','','','2','','','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Bin Location',401,'Pivot','Primary_Loc_Exists','PAGE_FIELD','','','1','','','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Bin Location',401,'Pivot','STK','PAGE_FIELD','','','2','','','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Bin Location',401,'Pivot','OPEN_DEMAND','PAGE_FIELD','','','3','','','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Bin Location',401,'Pivot','PRIMARY_BIN_LOC','DATA_FIELD','COUNT','','2','','','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Bin Location',401,'Pivot','LAST_RECEIVED_DATE','ROW_FIELD','','','3','','','');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
--Inserting Report Distribution Details 
--Inserting Report Distribution Details 
--Inserting Report SubTotals Details 
--Inserting Report aggregations 
--Inserting Report   Version details- Bin Location
xxeis.eis_rsc_ins.rv( 'Bin Location','','Bin Location','PS066716','01-FEB-2018');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 401');
END IF;
END;
/