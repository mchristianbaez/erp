/*
 TMS: 20160126-00179    
 Date: 01/26/2016
 Notes: Could you please change the branch in the header for CSP agreement# 6648 from branch 015 to branch 003. per request of account manager.
*/

SET SERVEROUTPUT ON SIZE 100000;

update  xxwc.xxwc_om_contract_pricing_hdr
set organization_id=230
where agreement_id=6648 
and organization_id=241;

commit;

/