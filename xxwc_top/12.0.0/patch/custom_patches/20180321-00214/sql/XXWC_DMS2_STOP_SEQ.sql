/*************************************************************************
     $Header XXWC_DMS2_STOP_SEQ $
     Module Name: XXWC_DMS2_STOP_SEQ.sql

     PURPOSE:   TMS# 20180321-00214-DMS_2.0_Delivery_Management_Tool

     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        03/26/2018  Rakesh Patel            TMS# 20180321-00214-DMS_2.0_Delivery_Management_Tool
**************************************************************************/
-- Create sequence 
create sequence XXWC.XXWC_DMS2_STOP_SEQ
minvalue 1
maxvalue 999999999999999999999999999
start with 1
increment by 1
nocache;

GRANT ALL ON XXWC.XXWC_DMS2_STOP_SEQ TO INTERFACE_OSO;

