CREATE OR REPLACE PACKAGE BODY APPS.XXWC_OM_DMS_ROUTINES_PKG AS

  /*********************************************************************************
  -- Package Body XXWC_OM_DMS_ROUTINES_PKG
  -- *******************************************************************************
  --
  -- PURPOSE: Package is for support routines used for DMS Process.
  --
  -- HISTORY
  -- ===========================================================================
  -- ===========================================================================
  -- VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- -------------------------------------
  -- 1.0     19-May-2015   Gopi Damuluri   Created this package. TMS 20150518-00036
  -- 1.1     03-Mar-2017   Neha Saini      TMS-20170329-00158 Packing Slip Missing POD's fix 
  *******************************************************************************/

  g_err_callfrom VARCHAR2(75) DEFAULT 'XXWC_OM_DMS_ROUTINES_PKG';
  g_distro_list  VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';

  /********************************************************************************
  -- PROCEDURE: generate_pod
  --
  -- PURPOSE: Procedure to Generate POD files for DocLink
  --
  -- HISTORY
  -- ===========================================================================
  -- ===========================================================================
  -- VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- -------------------------------------
  -- 1.0     19-May-2015   Gopi Damuluri   TMS 20150518-00036
  -- 1.1     03-Mar-2017   Neha Saini      TMS 20170329-00158 Packing Slip Missing POD's fix 
  *******************************************************************************/
  PROCEDURE generate_pod(p_errbuf             OUT VARCHAR2
                       , p_retcode            OUT VARCHAR2
                       , p_run_date            IN  VARCHAR2
                       , p_user_name           IN VARCHAR2
                       , p_responsibility_name IN VARCHAR2
                       ) IS

      ---------------------------------------------------------------------------------------------------------
      -- Cursor to derive the Requests which do not have Request_id updated in XXWC_SIGNATURE_CAPTURE_TBL
      ---------------------------------------------------------------------------------------------------------
      CURSOR cur_sgn(p_run_date DATE)
          IS
      SELECT *
        FROM xxwc.xxwc_print_requests_temp stg
       WHERE 1 = 1 
         AND stg.program IN ('XXWC_OM_SRECEIPT' , 'XXWC_OM_PACK_SLIP', 'XXWC_OM_SRECEIPT_CUSTOMER')
         AND stg.batch_id IN (SELECT MAX(stg2.batch_id) 
                                FROM xxwc.xxwc_print_requests_temp stg2 
                               WHERE stg2.group_id    = stg.group_id 
                               GROUP BY stg2.group_id)
         AND EXISTS (SELECT '1'
                       FROM xxwc.xxwc_signature_capture_tbl sgn
                      WHERE 1 = 1
                        AND TRUNC(sgn.creation_date) = p_run_date
                        AND sgn.request_id IS NULL
                        AND sgn.group_id    = stg.group_id
                        AND NVL(sgn.signature_name, '***CANCEL***') != '***CANCEL***' ); 

      ---------------------------------------------------------------------------------------------------------
      -- Cursor to derive the Arugments for Requests
      ---------------------------------------------------------------------------------------------------------
      CURSOR c_args(p_batch_id in number, p_group_id in number) is
      SELECT batch_id,
             process_flag,
             group_id,
             argument,
             value
        FROM xxwc.xxwc_print_requests_arg_temp
       WHERE group_id = p_group_id
         AND batch_id = p_batch_id;

      l_batch_id               NUMBER;
      l_group_id               NUMBER;
      l_group_new_id           NUMBER;
      l_return                 NUMBER;
      l_arg_value              VARCHAR2(240);
      l_exception              EXCEPTION;
      l_phase                  VARCHAR2 (80);
      l_status                 VARCHAR2 (80);
      l_dev_phase              VARCHAR2 (80);
      l_dev_status             VARCHAR2 (80);
      l_message                VARCHAR2 (4000);
      l_template_code          VARCHAR2(200);
      l_run_date               DATE;
      l_sec                    VARCHAR2(110) DEFAULT 'START';
      l_user_id                fnd_user.user_id%TYPE;
      l_responsibility_id      NUMBER;
      l_resp_application_id    NUMBER;
      l_org_id                 NUMBER;

BEGIN

fnd_file.put_line(fnd_file.log, '=========== Start of Process ===========');


  IF l_org_id IS NULL OR l_org_id = -1 OR l_org_id = -99 THEN
     l_org_id := fnd_profile.value('org_id');
  END IF;
  
    l_sec := 'Set policy context for org_id';
    ---------------------------------------------------------------------------------------------------------
    -- Mandatory initialization for R12
    ---------------------------------------------------------------------------------------------------------
    mo_global.set_policy_context('S', l_org_id);
    mo_global.init('ONT');
  
    ---------------------------------------------------------------------------------------------------------
    -- Deriving Ids from initalization variables
    ---------------------------------------------------------------------------------------------------------
    BEGIN
      SELECT user_id
        INTO l_user_id
        FROM apps.fnd_user
       WHERE user_name = upper(p_user_name)
         AND SYSDATE BETWEEN start_date AND
             nvl(end_date, trunc(SYSDATE) + 1);
    EXCEPTION
      WHEN no_data_found THEN
        l_message := 'UserName ' || p_user_name || ' not defined in Oracle';
        RAISE program_error;
    END;
  
    BEGIN
      SELECT responsibility_id, application_id
        INTO l_responsibility_id, l_resp_application_id
        FROM apps.fnd_responsibility_vl
       WHERE responsibility_name = p_responsibility_name
         AND SYSDATE BETWEEN start_date AND
             nvl(end_date, trunc(SYSDATE) + 1);
    EXCEPTION
      WHEN no_data_found THEN
        l_message := 'Responsibility ' || p_responsibility_name ||
                     ' not defined in Oracle';
        RAISE program_error;
    END;


    l_sec := 'Derive Run Date';
    IF p_run_date IS NULL
    THEN
      SELECT trunc(SYSDATE) INTO l_run_date FROM dual;
    ELSE
      SELECT to_date(p_run_date, 'DD-MON-YYYY') INTO l_run_date FROM dual;
    END IF;

fnd_file.put_line(fnd_file.log, 'l_run_date - '||l_run_date);

    FOR rec_sign IN cur_sgn(l_run_date) LOOP
       l_batch_id     := xxwc_print_requests_s.nextval;
       l_group_new_id := xxwc_print_request_groups_s.nextval;
       l_group_id     := rec_sign.group_id;

       ---------------------------------------------------------------------------------------------------------
       -- Update Signature Capture Table with new Group Id
       ---------------------------------------------------------------------------------------------------------
       l_sec := 'Update Signature Capture Table with new Group Id';
       UPDATE xxwc.xxwc_signature_capture_tbl
          SET group_id = l_group_new_id
        WHERE group_id = l_group_id;

       IF rec_sign.program = 'XXWC_OM_SRECEIPT' THEN
        l_template_code := 'XXWC_OM_SRECEIPT';
       ELSIF rec_sign.program = 'XXWC_OM_PACK_SLIP' THEN
        l_template_code := 'XXWC_OM_PACK_SLIP';
       ELSIF rec_sign.program = 'XXWC_OM_SRECEIPT_CUSTOMER' THEN
        l_template_code := 'XXWC_OM_SRECEIPT_CUSTOMER';
       ELSIF rec_sign.program='XXWC_OM_PACK_SLIP_0_COPIES' THEN --ver 1.1
        l_template_code := 'XXWC_OM_PACK_SLIP';  --ver 1.1
       END IF;

       l_sec := 'Insert into XXWC_PRINT_REQUESTS_TEMP';
       ---------------------------------------------------------------------------------------------------------
       -- Insert into XXWC_PRINT_REQUESTS_TEMP
       ---------------------------------------------------------------------------------------------------------
       INSERT INTO XXWC.XXWC_PRINT_REQUESTS_TEMP
                (CREATED_BY
                ,CREATION_DATE
                ,LAST_UPDATED_BY
                ,LAST_UPDATE_DATE
                ,BATCH_ID
                ,PROCESS_FLAG
                ,GROUP_ID
                ,APPLICATION
                ,PROGRAM
                ,DESCRIPTION
                ,START_TIME
                ,SUB_REQUEST
                ,PRINTER
                ,STYLE
                ,COPIES
                ,SAVE_OUTPUT
                ,PRINT_TOGETHER
                ,VALIDATE_PRINTER
                ,TEMPLATE_APPL_NAME
                ,TEMPLATE_CODE
                ,TEMPLATE_LANGUAGE
                ,TEMPLATE_TERRITORY
                ,OUTPUT_FORMAT
                ,DMS_SIGN_FLAG
                ,NLS_LANGUAGE)
                values
                (rec_sign.created_by                  --CREATED_BY
                ,rec_sign.creation_date               --CREATION_DATE
                ,rec_sign.last_updated_by             --LAST_UDATED_BY
                ,rec_sign.last_update_date            --LAST_UPDATE_DATE
                ,l_batch_id                           --BATCH_ID
                ,'1'                                  --PROCESS_FLAG
                ,l_group_new_id                       --GROUP_ID
                ,'XXWC'                               --APPLICATION
                , rec_sign.program                    --PROGRAM
                , rec_sign.description                --DESCRIPTION
                ,NULL                                 --START_TIME 
                ,'FALSE'                              --SUB_REQUEST
                , 'noprint'                           --PRINTER
                , NULL                                --STYLE
                , 0                                   --COPIES
                ,'TRUE'                               --SAVE_OUTPUT
                ,'N'                                  --PRINT_TOGETHER
                ,'RESOLVE'                            --VALIDATE_PRINTER
                ,'XXWC'                               --TEMPLATE_APPL_NAME
                , l_template_code                     --TEMPLATE_CODE
                ,'en'                                 --TEMPLATE_LANGUAGE
                ,'US'                                 --TEMPLATE_TERRITORY
                ,'PDF'                                --OUTPUT_FORMAT
                ,'2'
                ,'en'                                 --NLS_LANGUAGE
                );

       COMMIT;

       FOR r_args in c_args(rec_sign.batch_id, rec_sign.group_id) LOOP
          l_arg_value := r_args.value;

          BEGIN
             
             IF r_args.argument = '6' THEN
                l_arg_value := '2';
             ELSIF r_args.argument = '7' THEN
                l_arg_value := NULL;
             ELSIF r_args.argument = '8' THEN
                l_arg_value := NULL;
             END IF;

             l_sec := 'Insert into XXWC_PRINT_REQUESTS_ARG_TEMP';
             ---------------------------------------------------------------------------------------------------------
             -- Insert into XXWC_PRINT_REQUESTS_ARG_TEMP table
             ---------------------------------------------------------------------------------------------------------
             INSERT INTO xxwc.xxwc_print_requests_arg_temp(batch_id,
                                   process_flag,
                                   group_id,
                                   argument,
                                   value)
                               VALUES (l_batch_id,
                                   '1',
                                   l_group_new_id,
                                   r_args.argument,
                                   l_arg_value);
          EXCEPTION
          WHEN OTHERS THEN
             l_message := 'Error inserting record into XXWC_PRINT_REQUESTS_ARG_TEMP ' ||SQLCODE||SQLERRM;
             RAISE l_exception;
          END;
       END LOOP; --end r_args loop
       COMMIT;

       l_sec := 'Calling SUBMIT_PRINT_BATCH';
       ---------------------------------------------------------------------------------------------------------
       -- Calling SUBMIT_PRINT_BATCH procedure
       ---------------------------------------------------------------------------------------------------------
       l_return := XXWC_ONT_ROUTINES_PKG.SUBMIT_PRINT_BATCH(l_batch_id, 
                                                            l_group_new_id,
                                                            1,                    -- ProcessFlag
                                                            l_user_id,            --  15986,         -- REQUESTED_BY,
                                                            l_responsibility_id,
                                                            l_resp_application_id);

       COMMIT;
    END LOOP; -- cur_sgn

fnd_file.put_line(fnd_file.log, '=========== End of Process ===========');

EXCEPTION
WHEN l_exception THEN
   fnd_file.put_line(fnd_file.log, l_message);
WHEN OTHERS THEN
   fnd_file.put_line(fnd_file.log, SQLERRM);
   xxcus_error_pkg.xxcus_error_main_api(p_called_from       => 'XXWC_OM_DMS_ROUTINES_PKG.GENERATE_POD'
                                       ,p_calling           => l_sec
                                       ,p_request_id        => fnd_global.conc_request_id
                                       ,p_ora_error_msg     => substr('SQLERRM',1,2000)
                                       ,p_error_desc        => substr('SQLERRM',1,240)
                                       ,p_distribution_list => g_distro_list
                                       ,p_module            => 'ONT');
END generate_pod;

END xxwc_om_dms_routines_pkg;
/