/******************************************************************************
  $Header TMS_20170803_00071_Unable_to_receive_product.sql $
  Module Name:Data Fix script for 20170803-00071

  PURPOSE: Data fix script for 20170803-00071 Unable to receive product

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    -------------------------------
  1.0        24-Aug-2017  Niraj K Ranjan        TMS#20170803-00071

*******************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000

BEGIN
   DBMS_OUTPUT.put_line ('Before Update');

   update 	apps.wsh_delivery_details 
    set 	oe_interfaced_flag = 'Y', 
            last_update_date = sysdate, 
            last_updated_by = -1 
    where 	delivery_detail_id in (23705798,
                                   23705797,
                                   23753932,
                                   23091263,
                                   23091265,
                                   23091267,
                                   23091271,
                                   23091264)
    and 	oe_interfaced_flag = 'P';  


   DBMS_OUTPUT.put_line ('SOL Records update-' || SQL%ROWCOUNT);

   COMMIT;

EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Unable to update record ' || SQLERRM);
END;
/