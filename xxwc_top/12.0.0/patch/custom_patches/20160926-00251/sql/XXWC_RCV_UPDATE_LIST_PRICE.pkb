--
-- XXWC_RCV_UPDATE_LIST_PRICE  (Package Body)
--
CREATE OR REPLACE PACKAGE BODY APPS.XXWC_RCV_UPDATE_LIST_PRICE
AS
   /*************************************************************************************************************************************
      NAME:       xxwc_rcv_update_list_price

      PURPOSE:    Concurrent Program to Update Organization Item's List Price based on the receipt

      Logic:     1) Perform a receiving transactions
                 2) At the end of the day, run the concurrent request to look at the current day's transactions
                 3) Update the list price at the org site

      REVISIONS:
      Ver        Date        Author           Description
      ---------  ----------  ---------------  ----------------------------------------------------------------------------------------
      1.0        07-MAR-2012  Lee Spitzer       1. Create the PL/SQL Package
      1.1        07-NOV-2014  Pattabhi Avula    TMS# 20141002-00072 -- Canada Changes done
      1.2        10-Oct-2016  P.Vamshidhar      TMS#20160926-00251 - Improving XXWC Update Organization Items PO List Price Performance
   **************************************************************************************************************************************/

   G_EXCEPTION   EXCEPTION;
   G_MESSAGE     VARCHAR2 (2000);
   G_NAME        VARCHAR2 (30);

   --Procedure to call the update item list pirce concurrent request
   PROCEDURE UPDATE_ITEM_LIST_PRICE_CCR (ERRBUF         OUT NOCOPY VARCHAR2,
                                         RETCODE        OUT        NUMBER,
                                         P_ORG_ID    IN            NUMBER --Operating Unit
                                                                         ,
                                         P_DATE      IN            VARCHAR2,
                                         P_USER_ID   IN            NUMBER) --User ID
   IS
      x_return_status   NUMBER;
      l_date            DATE;
   BEGIN
      G_NAME := 'UPDATE_ITEM_LIST_PRICE_CCR';

      fnd_file.put_line (fnd_file.LOG, G_NAME || ' Starting program');
      fnd_file.put_line (fnd_file.LOG,
                         G_NAME || ' --------------------------------');
      fnd_file.put_line (fnd_file.LOG, G_NAME || ' Parameters');
      fnd_file.put_line (fnd_file.LOG, G_NAME || ' P_ORG_ID := ' || P_ORG_ID);
      fnd_file.put_line (fnd_file.LOG, G_NAME || ' P_DATE := ' || P_DATE);
      fnd_file.put_line (fnd_file.LOG,
                         G_NAME || ' P_USER_ID  := ' || P_USER_ID);
      fnd_file.put_line (fnd_file.LOG,
                         G_NAME || ' --------------------------------');

      l_date := FND_DATE.CANONICAL_TO_DATE (p_date); --Change the p_date parameter from a varchar2 to a date

      fnd_file.put_line (fnd_file.LOG, G_NAME || ' l_date = ' || l_date);


      fnd_file.put_line (
         fnd_file.LOG,
         G_NAME || ' Calling Procedure UPDATE_ITEM_LIST_PRICE');



      --Call the procedure the procedure Update Item List Price
      UPDATE_ITEM_LIST_PRICE (p_org_id          => p_org_id,
                              p_user_id         => p_user_id,
                              p_date            => l_date,
                              p_return_status   => x_return_status);

      fnd_file.put_line (fnd_file.LOG,
                         G_NAME || ' P_RETURN_STATUS ' || x_return_status);


      --if there is an exception in update_items, error the concurrent request

      IF x_return_status != 0
      THEN
         retcode := x_return_status;
      END IF;

      --If the user passes the parameter to purge the custom table

      fnd_file.put_line (fnd_file.LOG, G_NAME || ' End Concurrent Request');

      retcode := 0;
   END UPDATE_ITEM_LIST_PRICE_CCR;

   --Procedure to call the update item list pirce concurrent request
   PROCEDURE UPDATE_ITEM_LIST_PRICE (P_ORG_ID          IN     NUMBER,
                                     P_DATE            IN     DATE,
                                     P_USER_ID         IN     NUMBER,
                                     P_RETURN_STATUS      OUT VARCHAR2)
   /*************************************************************************************************************************************
      NAME:       UPDATE_ITEM_LIST_PRICE

      PURPOSE:    Concurrent Program to Update Organization Item's List Price based on the receipt

      REVISIONS:
      Ver        Date        Author           Description
      ---------  ----------  ---------------  ----------------------------------------------------------------------------------------
      1.2        10-Oct-2016  P.Vamshidhar      TMS#20160926-00251 - Improving XXWC Update Organization Items PO List Price Performance
   **************************************************************************************************************************************/

   IS
      --Cursor to retrieve items received and po price
      CURSOR c_receipts
      IS
         SELECT msib.inventory_item_id,
                msib.organization_id,
                NVL (inv_convert.inv_um_convert (msib.inventory_item_id,
                                                 5,
                                                 rt.po_unit_price,
                                                 msib.primary_uom_code,
                                                 rt.uom_code,
                                                 NULL,
                                                 NULL),
                     0)
                   list_price,
                rt.transaction_date                            -- Added in 1.2
           FROM rcv_shipment_lines rsl,
                mtl_system_items_b msib,
                rcv_transactions rt,
                apps.po_lines pla                                     -- v 1.1
                                 ,
                apps.po_headers pha                                   -- v 1.1
          WHERE     rsl.item_id = msib.inventory_item_id
                AND rsl.to_organization_id = msib.organization_id
                --AND TRUNC (rt.transaction_date) = TRUNC (p_date) -- Commented in Rev 1.2
                AND rt.transaction_date > p_date -- Added in Rev 1.2
                AND rt.transaction_date < p_date+1 -- Added in Rev 1.2
                AND rt.transaction_type = 'RECEIVE'
                AND rt.shipment_header_id = rsl.shipment_header_id
                AND rt.shipment_line_id = rsl.shipment_line_id
                AND rsl.po_line_id = pla.po_line_id
                AND rsl.po_header_id = pla.po_header_id
                AND rsl.po_header_id = pha.po_header_id
                AND pha.po_header_id = pla.po_header_id;

      --AND pha.org_id = p_org_id;    --  Commented in Rev 1.2

      ln_resp_id        NUMBER;
      ln_resp_appl_id   NUMBER;
      l_item_rec        inv_item_grp.item_rec_type;
      lo_item_rec       inv_item_grp.item_rec_type;
      x_error_tbl       inv_item_grp.error_tbl_type;
      x_message_list    error_handler.error_tbl_type;
      x_return_status   VARCHAR2 (2);
      x_msg_count       NUMBER := 0;
      x_msg_data        VARCHAR2 (2000);
      lv_item_errors    VARCHAR2 (4000);
      l_exception       EXCEPTION;
      l_message         VARCHAR2 (2000);
      l_count           NUMBER;
      --Added for Exception Handling--
      g_user_id         NUMBER := fnd_global.user_id;
      g_login_id        NUMBER := fnd_profile.VALUE ('LOGIN_ID');
      l_err_callfrom    VARCHAR2 (75) DEFAULT 'XXWC_RCV_UPDATE_LIST_PRICE';
      l_err_callpoint   VARCHAR2 (75) DEFAULT 'START';
      l_distro_list     VARCHAR2 (75)
                           DEFAULT 'HDSOracleDevelopers@hdsupply.com';
   BEGIN
      G_NAME := 'UPDATE_ITEMS';

      BEGIN                                                       --Debug only
         --get the responsibility_id and application_id from the Inventory responsibility to initialize
         SELECT responsibility_id, application_id
           INTO ln_resp_id, ln_resp_appl_id
           FROM fnd_responsibility_vl
          WHERE responsibility_name = 'Inventory';
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            g_message :=
               G_NAME || 'Can not find responsibility id or application_id';
            --Raise Exception to not execute program anymore
            RAISE g_exception;
         WHEN OTHERS
         THEN
            g_message :=
                  G_NAME
               || 'Other exception getting respoinsibility id'
               || SQLCODE
               || SQLERRM;
            --Raise Exception to not execute program anymore
            RAISE g_exception;
      END;

      --Initial based on User Name
      fnd_global.apps_initialize (p_user_id, ln_resp_id, ln_resp_appl_id);

      -- Added below code in Rev 1.2
      -- Initializing Org_id
      APPS.MO_GLOBAL.SET_POLICY_CONTEXT ('S', NVL (P_ORG_ID, 162));

      EXECUTE IMMEDIATE
         'ALTER SESSION SET NLS_DATE_FORMAT = ' || '''MM/DD/YYYY HH24:MI:SS''';

      FND_FILE.PUT_LINE (FND_FILE.LOG, ' p_date --> ' || p_date);

      -- Added above code in Rev 1.2

      --Loop through cursor and call the api to update items

      fnd_file.put_line (
         fnd_file.LOG,
         'INVENTORY_ITEM_ID | ORGANIZATION_ID | LIST_PRICE | RETURN_STATUS | MESSAGE');

      FOR r_receipts IN c_receipts
      LOOP
         BEGIN
            x_error_tbl.DELETE;
            l_item_rec.process_item_record := 1;
            l_item_rec.inventory_item_id := r_receipts.inventory_item_id;
            l_item_rec.organization_id := r_receipts.organization_id;
            l_item_rec.list_price_per_unit := r_receipts.list_price;


            inv_item_grp.update_item (
               p_commit             => fnd_api.g_false,
               p_lock_rows          => fnd_api.g_true,
               p_validation_level   => fnd_api.g_valid_level_full,
               p_item_rec           => l_item_rec,
               x_item_rec           => lo_item_rec,
               x_return_status      => x_return_status,
               x_error_tbl          => x_error_tbl,
               p_template_id        => NULL,
               p_template_name      => NULL);

            COMMIT;


            IF x_return_status = 'S'
            THEN
               lv_item_errors := 'Success';
            ELSE
               lv_item_errors := NULL;

               l_count := l_count + 0;
               lv_item_errors := lv_item_errors || ' ';


               FOR i IN 1 .. x_error_tbl.COUNT
               LOOP
                  lv_item_errors :=
                        lv_item_errors
                     || x_error_tbl (i).message_name
                     || ' - '
                     || x_error_tbl (i).MESSAGE_TEXT
                     || CHR (13)
                     || CHR (10);
               END LOOP;
            END IF;

            g_message := ('Return Status ' || x_return_status);

            g_message := (lv_item_errors);

            fnd_file.put_line (
               fnd_file.LOG,
                  R_RECEIPTS.INVENTORY_ITEM_ID
               || ' | '
               || R_RECEIPTS.ORGANIZATION_ID
               || ' | '
               || R_RECEIPTS.LIST_PRICE
               || ' | '
               || x_return_status
               || '|'
               || g_message);
         END;
      END LOOP;


      p_return_status := 0;
   EXCEPTION
      WHEN g_exception
      THEN
         --Return a failure to the concurrent request

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_name || '.' || l_err_callfrom,
            p_calling             => l_err_callpoint,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          => l_message,
            p_distribution_list   => l_distro_list,
            p_module              => 'PO');

         p_return_status := 2;
   END UPDATE_ITEM_LIST_PRICE;
END XXWC_RCV_UPDATE_LIST_PRICE;
/