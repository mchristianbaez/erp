/*
 TMS: 20160719-00078 
 Date: 8/10/2016
 */
SET SERVEROUTPUT ON SIZE 1000000

BEGIN
   DBMS_OUTPUT.PUT_LINE ('Before Update');


   UPDATE oe_order_lines_all
      SET project_id = 1,                                --Drop Project Number
          subinventory = 'Drop'
    WHERE line_id = 76948735;



   DBMS_OUTPUT.PUT_LINE ('Number Rows Updated' || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.PUT_LINE ('After Update');
END;
/