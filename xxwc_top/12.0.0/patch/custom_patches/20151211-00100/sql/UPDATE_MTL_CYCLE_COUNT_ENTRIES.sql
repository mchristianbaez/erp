   /*****************************************************************************************************************************************
   -- File Name: UPDATE_MTL_CYCLE_COUNT_ENTRIES.sql
   --
   -- PROGRAM TYPE: SQL Script to update cycle count entries.
   -- TMS#20151211-00100   - Data Fix for item stuck in Cycle Counts in org 158
   ******************************************************************************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20151211-00100  , Script 1 -Before Update');
UPDATE APPS.mtl_cycle_count_entries
   SET export_flag = NULL
 WHERE     organization_id = 304
       AND entry_status_code IN (1, 3)
       AND export_flag IS NOT NULL;
DBMS_OUTPUT.put_line ('TMS: 20151211-00100, Script 1 -After Update, rows updated: '||sql%rowcount);
COMMIT;
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20151211-00100, Script 1, Errors ='||SQLERRM);
END;
/