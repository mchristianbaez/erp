   /*****************************************************************************************************************************************
   -- File Name: XXWC_PO_LINE_ATTR_UPDATE.sql
   --
   -- PROGRAM TYPE: SQL Script to update stock status null to N
   -- TMS#20151124-00002  - Null Stock Status causing dashboards in OBIEE to be incorrect
   ******************************************************************************************************************************************/


UPDATE XXWC.XXWC_PO_LINE_ADDTIONAL_ATTR_T
   SET STOCK_STATUS = 'N'
 WHERE STOCK_STATUS IS NULL;
/
COMMIT;
/
