CREATE OR REPLACE PACKAGE BODY APPS.XXWC_PO_LINE_ATTR_CAPTURE_PKG
IS
   /*****************************************************************************************************************************************
   -- File Name: XXWC_PO_LINE_ATTR_CAPTURE_PKG.pkb
   --
   -- PROGRAM TYPE: PL/SQL Script   <API>
   --
   -- PURPOSE: To track additional required attributes information for po_lines into custom table.
   -- HISTORY
   -- ========================================================================================================================================
   -- ========================================================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- --------------------------------------------------------------------------------------------------
   -- 1.0     27-AUG-2015   P.vamshidhar    TMS#20150515-00063 - PO - Stocking Flag at time of PO Creation
   --                                       Initial Version.
   -- 1.1     24-Nov-2015   P.Vamshidhar    TMS#20151124-00002  - Null Stock Status causing dashboards in OBIEE to be incorrect
   ******************************************************************************************************************************************/

   PROCEDURE CAPTURE_ADDITIONAL_ATTR (x_err_buf        OUT VARCHAR2,
                                      x_retcode        OUT VARCHAR2,
                                      p_from_date   IN     VARCHAR2)
   IS
      /******************************************************************************************************************************************
        PROCEDURE : CAPTURE_ADDITIONAL_ATTR

        REVISIONS:
        Ver        Date         Author                     Description
        ---------  ----------   ---------------    --------------------------------------------------------------------------------------
        1.0        27-AUG-2015  P.Vamshidhar       TMS#20150515-00063 - PO - Stocking Flag at time of PO Creation
                                                   Initial Version

        1.1        24-Nov-2015  P.Vamshidhar       TMS#20151124-00002  - Null Stock Status causing dashboards in OBIEE to be incorrect

      ******************************************************************************************************************************************/

      lvc_master_org_id   VARCHAR2 (10)
                             := FND_PROFILE.VALUE ('XXWC_ITEM_MASTER_ORG');
      PROGRAM_ERROR       EXCEPTION;

      CURSOR CUR_PO_DATA (
         CP_ORG_ID       NUMBER,
         CP_FROM_DATE    DATE)
      IS
         SELECT PLL.PO_HEADER_ID,
                PLL.PO_LINE_ID,
                PLLA.LINE_LOCATION_ID,
                PLL.ITEM_ID,
                MSIB.ORGANIZATION_ID ORGANIZATION_ID,
                MSIB.LIST_PRICE_PER_UNIT LIST_PRICE,
                PLLA.FROM_HEADER_ID BPA_PO_HEADER_ID,
                PLLA.FROM_LINE_ID BPA_PO_LINE_ID,
                (SELECT SEGMENT1
                   FROM APPS.PO_HEADERS
                  WHERE PO_HEADER_ID = PLLA.FROM_HEADER_ID)
                   BPA_NUMBER,
                NULL STOCK_STATUS,
                NULL SYSTEM_DELIV_PRICE,
                MSIB.MAX_MINMAX_QUANTITY,
                MSIB.MIN_MINMAX_QUANTITY,
                (SELECT mic.CATEGORY_CONCAT_SEGS
                   FROM APPS.MTL_ITEM_CATEGORIES_V MIC
                  WHERE     MIC.INVENTORY_ITEM_ID = PLL.ITEM_ID
                        AND MIC.ORGANIZATION_ID = MSIB.ORGANIZATION_ID
                        AND MIC.CATEGORY_SET_NAME = 'Sales Velocity')
                   CATEGORY_CONCAT_SEGS,
                PLLA.QUANTITY,
                PLLA.NEED_BY_DATE,
                PLLA.CREATION_DATE,
                NULL BPA_PRICE,
                PLLA.SHIP_TO_LOCATION_ID,
                PHA.VENDOR_ID,
                PHA.VENDOR_SITE_ID
           FROM APPS.PO_LINES PLL,
                APPS.PO_HEADERS PHA,
                APPS.PO_LINE_LOCATIONS PLLA,
                APPS.MTL_SYSTEM_ITEMS_B MSIB
          WHERE     PHA.PO_HEADER_ID = PLL.PO_HEADER_ID
                AND PHA.TYPE_LOOKUP_CODE = 'STANDARD'
                AND PLL.CREATION_DATE >= CP_FROM_DATE
                AND PLL.PO_LINE_ID = PLLA.PO_LINE_ID
                AND PLL.PO_HEADER_ID = PLLA.PO_HEADER_ID
                AND PLLA.SHIP_TO_ORGANIZATION_ID = MSIB.ORGANIZATION_ID
                AND PLL.ITEM_ID = MSIB.INVENTORY_ITEM_ID
                AND NOT EXISTS
                       (SELECT 1
                          FROM XXWC.XXWC_PO_LINE_ADDTIONAL_ATTR_T XPLA
                         WHERE XPLA.PO_LINE_ID = PLL.PO_LINE_ID);

      TYPE l_po_data_type IS TABLE OF CUR_PO_DATA%ROWTYPE;

      obj_po_data         l_po_data_type;

      l_record_count      NUMBER;

      l_cur_login         NUMBER := FND_GLOBAL.LOGIN_ID;
      l_user_id           NUMBER := FND_GLOBAL.USER_ID;
      l_err_msg           VARCHAR2 (1000);
      l_procedure         VARCHAR2 (100);
      g_err_callfrom      VARCHAR2 (1000);
      g_distro_list       VARCHAR2 (1000);
      l_sec               VARCHAR2 (1000);
      ln_max              NUMBER;
      ln_min              NUMBER;
      lvc_cat_segs        VARCHAR2 (100);

      x_price_zone        NUMBER;
      x_nprice            NUMBER;
      x_nq_price          NUMBER;
      x_pprice            NUMBER;
      x_zone_price        NUMBER;
      x_zone_qprice       NUMBER;
      x_bprice            NUMBER;
      x_r_status          VARCHAR2 (1000);
      ln_org_id           NUMBER := FND_PROFILE.VALUE ('ORG_ID');
      lp_from_date        DATE;
      ln_count            NUMBER;
   BEGIN
      lp_from_date :=
         TRUNC (
            NVL (TO_DATE (p_from_date, 'YYYY/MM/DD HH24:MI:SS'), SYSDATE));

      FND_FILE.PUT_LINE (
         FND_FILE.LOG,
         'Start Date :' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));

      OPEN CUR_PO_DATA (ln_org_id, lp_from_date);

      LOOP
         FETCH CUR_PO_DATA BULK COLLECT INTO obj_po_data LIMIT 10000;

         EXIT WHEN obj_po_data.COUNT = 0;
         ln_count := NVL (ln_count, 0) + obj_po_data.COUNT;

         FOR l_record_count IN 1 .. obj_po_data.COUNT
         LOOP
            --Stock Flag populate

            IF OBJ_PO_DATA (l_record_count).ITEM_ID IS NULL
            THEN
               OBJ_PO_DATA (l_record_count).STOCK_STATUS := 'NA';
            ELSE
               IF     OBJ_PO_DATA (l_record_count).CATEGORY_CONCAT_SEGS IN ('1',
                                                                            '2',
                                                                            '3',
                                                                            '4',
                                                                            '5',
                                                                            '6',
                                                                            '7',
                                                                            '8',
                                                                            '9',
                                                                            'B',
                                                                            'C',
                                                                            'E')
                  AND NVL (OBJ_PO_DATA (l_record_count).MAX_MINMAX_QUANTITY,
                           0) > 0
                  AND NVL (OBJ_PO_DATA (l_record_count).MIN_MINMAX_QUANTITY,
                           0) > 0
               THEN
                  OBJ_PO_DATA (l_record_count).STOCK_STATUS := 'Y';
               ELSIF     OBJ_PO_DATA (l_record_count).CATEGORY_CONCAT_SEGS IN ('N',
                                                                               'Z',
                                                                               'E')
                     AND NVL (
                            OBJ_PO_DATA (l_record_count).MAX_MINMAX_QUANTITY,
                            0) = 0
                     AND NVL (
                            OBJ_PO_DATA (l_record_count).MIN_MINMAX_QUANTITY,
                            0) = 0
               THEN
                  OBJ_PO_DATA (l_record_count).STOCK_STATUS := 'N';
               ELSE                              -- Added in 1.1 Rev by Vamshi
                  OBJ_PO_DATA (l_record_count).STOCK_STATUS := 'N'; -- Added in 1.1 Rev by Vamshi
               END IF;
            END IF;

            --- BPA Price populate----------------------------------------------------------------

            IF OBJ_PO_DATA (l_record_count).BPA_PO_HEADER_ID IS NULL
            THEN
               BEGIN
                  SELECT PO_HEADER_ID, PO_LINE_ID, SEGMENT1
                    INTO OBJ_PO_DATA (l_record_count).BPA_PO_HEADER_ID,
                         OBJ_PO_DATA (l_record_count).BPA_PO_LINE_ID,
                         OBJ_PO_DATA (L_RECORD_COUNT).BPA_NUMBER
                    FROM (  SELECT PHA.PO_HEADER_ID,
                                   PLL.PO_LINE_ID,
                                   PHA.SEGMENT1
                              FROM APPS.PO_LINES_ALL PLL,
                                   APPS.PO_HEADERS_ALL PHA
                             WHERE     PLL.PO_HEADER_ID = PHA.PO_HEADER_ID
                                   AND PLL.ITEM_ID =
                                          OBJ_PO_DATA (l_record_count).ITEM_ID
                                   AND PHA.VENDOR_ID =
                                          OBJ_PO_DATA (l_record_count).VENDOR_ID
                                   AND PHA.VENDOR_SITE_ID =
                                          OBJ_PO_DATA (l_record_count).VENDOR_SITE_ID
                                   AND PHA.TYPE_LOOKUP_CODE = 'BLANKET'
                                   AND PHA.GLOBAL_AGREEMENT_FLAG = 'Y'
                                   AND PHA.AUTHORIZATION_STATUS = 'APPROVED'
                                   AND NVL (PHA.END_DATE_ACTIVE,
                                            TRUNC (SYSDATE + 1)) >=
                                          TRUNC (SYSDATE)
                                   AND PHA.ENABLED_FLAG = 'Y'
                          ORDER BY PHA.CREATION_DATE DESC)
                   WHERE ROWNUM < 2;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     OBJ_PO_DATA (l_record_count).BPA_PO_HEADER_ID := NULL;
               END;
            END IF;

            x_bprice := 0;

            IF OBJ_PO_DATA (l_record_count).BPA_PO_HEADER_ID IS NOT NULL
            THEN
               xxwc_bpa_price_zone_pkg.get_pricing (
                  p_po_header_id                => OBJ_PO_DATA (l_record_count).BPA_PO_HEADER_ID,
                  p_organization_id             => OBJ_PO_DATA (l_record_count).ORGANIZATION_ID,
                  p_inventory_item_id           => OBJ_PO_DATA (l_record_count).ITEM_ID,
                  p_po_line_id                  => OBJ_PO_DATA (l_record_count).BPA_PO_LINE_ID,
                  p_quantity                    => OBJ_PO_DATA (l_record_count).QUANTITY,
                  p_date                        => OBJ_PO_DATA (l_record_count).NEED_BY_DATE,
                  x_vendor_price_zone           => x_price_zone,
                  x_national_price              => x_nprice,
                  x_national_quantity_price     => x_nq_price,
                  x_promo_price                 => x_pprice,
                  x_price_zone_price            => x_zone_price,
                  x_price_zone_quantity_price   => x_zone_qprice,
                  x_best_price                  => x_bprice     --> Best Price
                                                           );

               OBJ_PO_DATA (l_record_count).BPA_PRICE := x_bprice;
            END IF;



            --System Delivered Price -----------------------------------------------------------------------------------

            SELECT DECODE (NVL (x_bprice, 0),
                           0, OBJ_PO_DATA (l_record_count).LIST_PRICE,
                           NVL (x_bprice, 0))
              INTO OBJ_PO_DATA (l_record_count).SYSTEM_DELIV_PRICE
              FROM DUAL;
         /*
         PO_Custom_Price_PUB.GET_CUSTOM_PO_PRICE (
            p_api_version        => 1.0,
            p_order_quantity     => OBJ_PO_DATA (l_record_count).QUANTITY,
            p_ship_to_org        => OBJ_PO_DATA (l_record_count).ORGANIZATION_ID,
            p_ship_to_loc        => OBJ_PO_DATA (l_record_count).SHIP_TO_LOCATION_ID,
            p_po_line_id         => OBJ_PO_DATA (l_record_count).PO_LINE_ID,
            p_cum_flag           => NULL,
            p_need_by_date       => OBJ_PO_DATA (l_record_count).NEED_BY_DATE,
            p_pricing_date       => OBJ_PO_DATA (l_record_count).CREATION_DATE,
            p_line_location_id   => OBJ_PO_DATA (l_record_count).LINE_LOCATION_ID,
            p_price              => OBJ_PO_DATA (l_record_count).LIST_PRICE,
            x_new_price          => OBJ_PO_DATA (l_record_count).SYSTEM_DELIV_PRICE,
            x_return_status      => x_r_status,
            p_req_line_price     => NULL);
          */
         END LOOP;

         FORALL l_record_count IN obj_po_data.FIRST .. obj_po_data.LAST
            INSERT
              INTO XXWC.XXWC_PO_LINE_ADDTIONAL_ATTR_T (PO_HEADER_ID,
                                                       PO_LINE_ID,
                                                       LINE_LOCATION_ID,
                                                       ORGANIZATION_ID,
                                                       ITEM_ID,
                                                       LIST_PRICE,
                                                       BPA_NUMBER,
                                                       BPA_PRICE,
                                                       STOCK_STATUS,
                                                       SYSTEM_DELIV_PRICE,
                                                       ORG_ID,
                                                       CREATED_BY,
                                                       CREATION_DATE,
                                                       LAST_UPDATED_BY,
                                                       LAST_UPDATED_DATE,
                                                       LAST_UPDATED_LOGIN)
            VALUES (obj_po_data (l_record_count).PO_HEADER_ID,
                    obj_po_data (l_record_count).PO_LINE_ID,
                    obj_po_data (l_record_count).LINE_LOCATION_ID,
                    obj_po_data (l_record_count).ORGANIZATION_ID,
                    obj_po_data (l_record_count).ITEM_ID,
                    obj_po_data (l_record_count).LIST_PRICE,
                    obj_po_data (l_record_count).BPA_NUMBER,
                    obj_po_data (l_record_count).BPA_PRICE,
                    obj_po_data (l_record_count).STOCK_STATUS,
                    OBJ_PO_DATA (l_record_count).SYSTEM_DELIV_PRICE,
                    ln_org_id,
                    l_user_id,
                    SYSDATE,
                    l_user_id,
                    SYSDATE,
                    l_cur_login);

         COMMIT;
         obj_po_data.delete;
      END LOOP;

      FND_FILE.PUT_LINE (
         FND_FILE.OUTPUT,
         '------------------------------------------------------------');
      FND_FILE.PUT_LINE (
         FND_FILE.OUTPUT,
            '          NUMBER OF PO LINES PROCESSED :         '
         || NVL (ln_count, 0));
      FND_FILE.PUT_LINE (
         FND_FILE.OUTPUT,
         '------------------------------------------------------------');
   EXCEPTION
      WHEN PROGRAM_ERROR
      THEN
         l_err_msg :=
               l_err_msg
            || ' ...Error_Stack...'
            || DBMS_UTILITY.format_error_stack ()
            || ' Error_Backtrace...'
            || DBMS_UTILITY.format_error_backtrace ();

         -- Calling ERROR API
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || l_procedure,
            p_calling             => l_sec,
            p_ora_error_msg       => SUBSTR (l_err_msg, 1, 2000),
            p_error_desc          => SUBSTR (l_err_msg, 1, 240),
            p_distribution_list   => g_distro_list,
            p_module              => 'PO');
         x_retcode := 2;
         x_err_buf := SUBSTR (l_err_msg, 1, 500);
         ROLLBACK;
      WHEN OTHERS
      THEN
         l_err_msg :=
               l_err_msg
            || ' ...Error_Stack...'
            || DBMS_UTILITY.format_error_stack ()
            || ' Error_Backtrace...'
            || DBMS_UTILITY.format_error_backtrace ();

         -- Calling ERROR API
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || l_procedure,
            p_calling             => l_sec,
            p_ora_error_msg       => SUBSTR (l_err_msg, 1, 2000),
            p_error_desc          => SUBSTR (l_err_msg, 1, 240),
            p_distribution_list   => g_distro_list,
            p_module              => 'PO');
         x_retcode := 2;
         x_err_buf := SUBSTR (l_err_msg, 1, 500);
         ROLLBACK;
   END;
END XXWC_PO_LINE_ATTR_CAPTURE_PKG;
/