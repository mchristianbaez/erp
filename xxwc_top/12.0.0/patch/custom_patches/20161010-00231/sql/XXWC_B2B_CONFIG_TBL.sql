  /*******************************************************************************
  Table: xxwc.XXWC_B2B_CONFIG_TBL
  Description: B2B Configuration Table- This is combination of two tables
               XXWC.XXWC_B2B_CUST_INFO_TBL and XXWC.XXWC_B2B_POD_INFO_TBL
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     18-Oct-2016        Pahwa Nancy   TMS# 20161010-00231  B2B Configuration Table- This is combination of two tables
                                           XXWC.XXWC_B2B_CUST_INFO_TBL and XXWC.XXWC_B2B_POD_INFO_TBL  
  ********************************************************************************/
-- Create table
create table xxwc.XXWC_B2B_CONFIG_TBL
(
  party_id           NUMBER,
  party_name         VARCHAR2(360),
  party_number       VARCHAR2(30),
  customer_id        NUMBER,
  deliver_soa        VARCHAR2(1),
  deliver_asn        VARCHAR2(1),
  deliver_invoice    VARCHAR2(1),
  start_date_active  DATE,
  end_date_active    DATE,
  creation_date      DATE,
  created_by         NUMBER,
  last_update_date   DATE,
  last_updated_by    NUMBER,
  tp_name            VARCHAR2(50),
  deliver_poa        VARCHAR2(1),
  default_email      VARCHAR2(240),
  status             VARCHAR2(40),
  notification_email VARCHAR2(250),
  notify_account_mgr VARCHAR2(1),
  soa_email          VARCHAR2(240),
  asn_email          VARCHAR2(240),
  comments           VARCHAR2(2000),
  approved_date      DATE,
  deliver_pod        VARCHAR2(1),
  pod_email          VARCHAR2(240),
  pod_frequency      VARCHAR2(50),
  account_number     VARCHAR2(30),
  account_name       VARCHAR2(240),
  cust_account_id    NUMBER,
  pod_last_sent_date DATE,
  pod_next_send_date DATE,
  id                 NUMBER,
  location           VARCHAR2(40),
  site_use_id        NUMBER,
  soa_print_price    VARCHAR2(1),
  pod_print_price    VARCHAR2(1)
);
/