--DROP TRIGGER "APPS"."XXWC_B2B_CONFIG_TRG";
CREATE OR REPLACE TRIGGER "APPS"."XXWC_B2B_CONFIG_TRG" 
  BEFORE INSERT OR UPDATE ON XXWC.XXWC_B2B_CONFIG_TBL
  REFERENCING NEW AS New OLD AS Old
  FOR EACH ROW
DECLARE
  l_party_id        NUMBER DEFAULT NULL;
  l_rec_count       NUMBER := 0;
  l_user_id         NUMBER;
  l_sec             VARCHAR2(100);
  l_cust_account_id NUMBER;
  l_account_number  VARCHAR2(30);
  l_account_name    VARCHAR2(240);
  l_location        VARCHAR2(40);
  l_prog_err EXCEPTION;
  /******************************************************************************
     NAME:      XXWC_B2B_CONFIG_TRG
     PURPOSE : Trigger to populate B2B Customer/POD Information table.
     REVISIONS:
     Ver        Date        Author           Description
     ---------  ----------  ---------------  ------------------------------------
     1.0        20-Oct-2016 Nancy Pahwa    TMS# 20161010-00231.
  ******************************************************************************/
BEGIN

  l_sec := 'Derive User-Id';
  BEGIN
    SELECT user_id
      INTO l_user_id
      FROM apps.fnd_user
     WHERE user_name = NVL(v('APP_USER'), USER);
  EXCEPTION
    WHEN OTHERS THEN
      l_sec := 'Error deriving User-Id';
  END;

  if inserting then
    if :NEW.ID IS NULL THEN
      select XXWC.XXWC_B2B_CONFIG_S.NEXTVAL INTO :NEW.ID from dual;
    end if;
  end if;

  IF :NEW.creation_date IS NULL THEN
    :NEW.creation_date := SYSDATE;
    :NEW.created_by    := l_user_id;
  END IF;

  :NEW.last_update_date := SYSDATE;
  :NEW.last_updated_by  := l_user_id;

  IF :NEW.DELIVER_SOA IS NULL THEN
    :NEW.DELIVER_SOA := 'N';
  END IF;

  IF :NEW.DELIVER_ASN IS NULL THEN
    :NEW.DELIVER_ASN := 'N';
  END IF;

  IF :NEW.DELIVER_INVOICE IS NULL THEN
    :NEW.DELIVER_INVOICE := 'N';
  END IF;

  IF :NEW.DELIVER_POA IS NULL THEN
    :NEW.DELIVER_POA := 'N';
  END IF;

  -- Version# 1.1 > Start
  IF :NEW.DELIVER_POD IS NULL THEN
    :NEW.DELIVER_POD := 'N';
  END IF;
  -- Version# 1.1 < End

  IF :NEW.NOTIFY_ACCOUNT_MGR IS NULL THEN
    :NEW.NOTIFY_ACCOUNT_MGR := 'N';
  END IF;

 /*  IF :OLD.STATUS != 'APPROVED' AND :NEW.STATUS = 'APPROVED' THEN
    :NEW.APPROVED_DATE := SYSDATE;
  END IF;*/
  
  if :new.status is null then 
    :new.status := 'APPROVED';
  end if;


  l_sec := 'Start Date Active';
  IF :NEW.START_DATE_ACTIVE IS NULL THEN
    :NEW.START_DATE_ACTIVE := TRUNC(SYSDATE);
  END IF;

  l_sec := 'End Date Active';
  IF :NEW.END_DATE_ACTIVE IS NULL THEN
    :NEW.END_DATE_ACTIVE := TO_DATE('31-DEC-4099', 'DD-MON-YYYY');
  END IF;
  SELECT party_id
    INTO l_party_id
    FROM apps.hz_parties
   WHERE party_number = NVL(:OLD.party_number, :NEW.party_number)
     AND status = 'A'; --  Version# 1.1

  :NEW.PARTY_ID := l_party_id;

  IF :NEW.DELIVER_POD IS NULL THEN
    :NEW.DELIVER_POD := 'N';
  END IF;

  l_sec := 'Derive Cust_Account_Id';
  BEGIN
    SELECT cust_account_id, account_number, account_name
      INTO l_cust_account_id, l_account_number, l_account_name
      FROM apps.hz_cust_accounts_all
     WHERE Cust_Account_Id = :NEW.CUST_ACCOUNT_ID
       AND ROWNUM = 1;
  END;

  :NEW.CUST_ACCOUNT_ID := l_cust_account_id;
  :NEW.ACCOUNT_NUMBER  := l_account_number;
  :NEW.ACCOUNT_NAME    := l_account_name;

  IF :NEW.POD_LAST_SENT_DATE IS NULL THEN
    :NEW.POD_LAST_SENT_DATE := TRUNC(NVL(:NEW.START_DATE_ACTIVE, SYSDATE)) - 1; -- Version# 1.1
  END IF;

  IF :NEW.POD_NEXT_SEND_DATE IS NULL THEN
    :NEW.POD_NEXT_SEND_DATE := TRUNC(NVL(:NEW.START_DATE_ACTIVE, SYSDATE));
  END IF;

  IF :NEW.SITE_USE_ID IS NOT NULL THEN
    l_sec := 'Derive Cust Location';
    BEGIN
      SELECT location
        INTO l_location
        FROM apps.hz_cust_site_uses_all
       WHERE site_use_id = :NEW.SITE_USE_ID
         AND site_use_code = 'SHIP_TO'
         AND ROWNUM = 1;
    EXCEPTION
      WHEN OTHERS THEN
        RAISE l_prog_err;
    END;
  
    :NEW.LOCATION := l_location;
  END IF;
EXCEPTION
  WHEN OTHERS THEN
    xxcus_error_pkg.xxcus_error_main_api(p_called_from       => 'XXWC_B2B_CONFIG_TRG',
                                         p_calling           => l_sec,
                                         p_request_id        => -1,
                                         p_ora_error_msg     => SUBSTR(SQLERRM,
                                                                       2000),
                                         p_error_desc        => 'Error in trigger - XXWC_B2B_CONFIG_TRG',
                                         p_distribution_list => 'WC-ITDEVALERTS-U1@HDSupply.com',
                                         p_module            => 'XXWC');
  
    raise_application_error(-20001, 'XXWC_B2B_CONFIG_TRG: ' || SQLERRM);
END XXWC_B2B_CONFIG_TRG;
/