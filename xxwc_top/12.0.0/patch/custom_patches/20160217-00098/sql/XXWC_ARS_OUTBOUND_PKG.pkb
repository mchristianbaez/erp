CREATE OR REPLACE PACKAGE BODY APPS.xxwc_ars_outbound_pkg AS

  /********************************************************************************
  FILE NAME: XXWCAR_ARS_OUTBOUND_PKG.pkg
  
  PROGRAM TYPE: PL/SQL Package Body
  
  PURPOSE: ARS Outbound Interfaces.
  
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     07/16/2012    Luong Vu        Initial creation 
  ********************************************************************************/

  --Email Defaults
  pl_dflt_email fnd_user.email_address%TYPE := 'HDSOracleDevelopers@hdsupply.com';
  l_program_error EXCEPTION;



  /********************************************************************************
  FILE NAME: XXWCAR_ARS_OUTBOUND_PKG
  
  PROGRAM TYPE: PL/SQL Package Body
  
  PURPOSE: ARS Outbound Interfaces.  Refresh GETPAID contact table before running
           extract creation.
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     07/16/2012    Luong Vu        Initial creation 
  1.1     07/05/2013    Luong Vu        Incident 414992 - TMS 20130703-01067
                                        Modified main query to use bill to site id
                                        to match site balance.
  1.2     12/10/2013    Harsha          TMS 20131115-00077 to exclude profile class
                                        Branch Cash Account
  1.3     09/17/2014    Maharajan
                        Shunmugam       TMS# 20141001-00153 Canada Multi-org Changes
  1.4     12/30/2014   Maharajan S      TMS# 20141230-00067 ARS file not processing data correctly
  1.5    12/04/2014    Pattabhi
                       Avula            TMS# 20141028-00047 to fixed line break issue
                                        on bill_to and Ship_to columns
  1.6    3/3/2015    Raghavendra S      TMS# 20150227-00240 -- ARS Extract - fix Max Length logic and Exception handling
                                         -- Added trim function for the columns in Select  stmt and exception handle for submit job procedure  
 
  1.7    23/06/2015    Mullamuri 
                       Hari Prasad      TMS# 20150620-00001 to fix UC4 Job received return code 0
  1.8    2/22/2016     Neha Saini       TMS# 20160217-00098 to fix print date issue and replaced and with and due compilation issue in XPATCH.  
  ********************************************************************************/
  PROCEDURE create_ars_extract(p_errbuf  OUT VARCHAR2
                              ,p_retcode OUT NUMBER) IS
  
    l_sec       VARCHAR2(250);
    l_err_msg   VARCHAR2(250);
    l_err_code  NUMBER;
    l_rec_count NUMBER := 0;
  
    l_procedure_name VARCHAR2(50) := 'XXWCAR_ARS_OUTBOUND_PKG.create_ars_extract';
  
  
  BEGIN
    l_sec := 'Truncate table XXWC_GETPAID_CONTACTS_TBL';
    BEGIN
      EXECUTE IMMEDIATE 'TRUNCATE TABLE XXWC.XXWC_GETPAID_CONTACTS_TBL';
    
    EXCEPTION
      WHEN OTHERS THEN
        l_err_msg := 'Error TRUNCATE XXWC_GETPAID_CONTACTS_TBL :' ||
                     SQLERRM;
        RAISE l_program_error;
    END;
  
    l_sec := 'Refresh getpaid contact table';
  
    BEGIN
      INSERT INTO xxwc.xxwc_getpaid_contacts_tbl
        SELECT DISTINCT a.custno
                       ,a.company
                       ,b.contact_id
                       ,b.firstname
                       ,b.lastname
                       ,b.dear
                       ,b.title
                       ,b.fax
                       ,b.phone
                       ,b.hphone
                       ,b.initials
                       ,b.address1
                       ,b.address2
                       ,b.address3
                       ,b.city
                       ,b.state
                       ,b.zip
                       ,b.email
                       ,b.prefcorr
          FROM gpcomp1.gpcust@xxwc_r12_getpaid.hsi.hughessupply.com a
         INNER JOIN gpcomp1.gpcont@xxwc_r12_getpaid.hsi.hughessupply.com b
            ON a.custno = b.custno
         WHERE a.contact_id = b.contact_id;
    
      COMMIT;
    
    EXCEPTION
      WHEN OTHERS THEN
        l_err_msg := 'Error INSERT INTO XXWC_GETPAID_CONTACTS_TBL :' ||
                     SQLERRM;
        RAISE l_program_error;
    END;
  
    l_sec := 'TRUNCATE TABLE XXWC.XXWC_ARS_OB_TBL'; -- INVOICE
  
    BEGIN
      EXECUTE IMMEDIATE 'TRUNCATE TABLE XXWC.XXWC_ARS_OB_TBL';
    
    EXCEPTION
      WHEN OTHERS THEN
        l_err_msg := 'Error TRUNCATE XXWC.XXWC_ARS_OB_TBL :' || SQLERRM;
        RAISE l_program_error;
    END;
  
    l_sec := 'Refresh ARS EXTRACT DATA';
  
    BEGIN
    -- Added TRIM for V 1.6 
      FOR inv_rec IN (SELECT DISTINCT trim(substr(hca.account_name, 1, 150)) account_name
                    --,trim(substr(REGEXP_REPLACE(hl_b.address1,'[[:cntrl:]]') || ' ' ||  -- REGEXP_REPLACE  Function added by Pattabhi for TMS# 20141028-00047  on 04-Dec-2014
                    --                         REGEXP_REPLACE(hl_b.address2,'[[:cntrl:]]'), 1, 150)) bill_addr -- bill to   -- REGEXP_REPLACE  Function added by Pattabhi for TMS# 20141028-00047  on 04-Dec-2014
                          -- commented above bill_addr and added below by Hari for v 1.7
                                            ,trim(substr(REPLACE(REPLACE(REPLACE(REGEXP_REPLACE(hl_b.address1,'[[:cntrl:]]') || ' ' ||  
                                             REGEXP_REPLACE(hl_b.address2,'[[:cntrl:]]'),'�',CHR(34)),'�',CHR(45)),'�',CHR(34)), 1, 150)) bill_addr 
                                     ,trim(substr(hl_b.city, 1, 50)) bill_city
                                     ,trim(substr(hl_b.state, 1, 15)) bill_state
                                     ,trim(substr(hl_b.postal_code, 1, 15)) bill_postal_code
                            --,substr(REGEXP_REPLACE(hl_s.address1,'[[:cntrl:]]') || ' ' ||  -- REGEXP_REPLACE  Function added by Pattabhi for TMS# 20141028-00047  on 04-Dec-2014
                            -- REGEXP_REPLACE(hl_s.address2,'[[:cntrl:]]'), 1, 150), ship_addr */ --  SHIP_TO  -- REGEXP_REPLACE  Function added by Pattabhi for TMS# 20141028-00047  on 04-Dec-2014
                          -- commented above ship_addr and added below by Hari for v 1.7
                                ,trim(substr(REPLACE(REPLACE(REPLACE(REGEXP_REPLACE(hl_s.address1,'[[:cntrl:]]') || ' ' ||  
                                    REGEXP_REPLACE(hl_s.address2,'[[:cntrl:]]'),'�',CHR(34)),'�',CHR(45)),'�',CHR(34)), 1, 150)) ship_addr
                                     ,trim(substr(hl_s.city, 1, 50)) ship_city
                                     ,trim(substr(hl_s.state, 1, 15)) ship_state
                                     ,trim(substr(hl_s.postal_code, 1, 15)) ship_postal_code
                                     ,trim(substr(hcsu_s.location, 1, 150)) location
                                     ,trim(substr(hps_s.party_site_number, 1, 25)) party_site_number
                                     ,bal.balance
                                     ,apsa.amount_due_original
                                     ,trim(substr(rcta.trx_number, 1, 25)) trx_number
                                     ,trim(substr(to_char(rcta.trx_date,
                                                     'MM/DD/YYYY'), 1, 25)) trx_date
                                     ,trim(substr(hca.account_number, 1, 25)) account_number
                                     ,trim(substr(al.description, 1, 15)) description
                                     ,trim(substr(al.attribute1, 1, 15)) attribute1
                                     ,trim(substr(gc.contact_id, 1, 25)) contact_id 
                                     ,trim(substr(gc.firstname, 1, 50)) firstname
                                     ,trim(substr(gc.lastname, 1, 50)) lastname
                                     ,trim(substr(gc.dear, 1, 50)) dear
                                     ,trim(substr(gc.title, 1, 75)) title
                                     ,trim(substr(gc.fax, 1, 50)) fax
                                     ,trim(substr(gc.phone, 1, 50)) phone
                                     ,trim(substr(gc.hphone, 1, 50)) hphone
                                     ,trim(substr(gc.address1, 1, 50)) address1
                                     ,trim(substr(gc.address2, 1, 50)) address2
                                     ,trim(substr(gc.address3, 1, 50)) address3
                                     ,trim(substr(gc.city, 1, 50)) con_city
                                     ,trim(substr(gc.state, 1, 15)) con_state
                                     ,trim(substr(gc.zip, 1, 15)) zip
                                     ,trim(substr(gc.email, 1, 75)) email
                                     ,trim(substr(con_gen.con_first_name, 1, 100)) gc_first_name -- GENERAL CONTRACTOR CONTACT INFO
                                     ,trim(substr(con_gen.con_last_name, 1, 50)) gc_last_name
                                     ,trim(substr(con_gen.email_address, 1, 75)) gc_email_address
                                     ,trim(substr(con_gen.phone, 1, 50)) gc_phone
                                     ,trim(substr(con_gen.fax, 1, 50)) gc_fax
                                     ,trim(substr(con_gen.address1, 1, 50)) gc_address1
                                     ,trim(substr(con_gen.address2, 1, 50)) gc_address2
                                     ,trim(substr(con_gen.city, 1, 50)) gc_city
                                     ,trim(substr(con_gen.county, 1, 50)) gc_county
                                     ,trim(substr(con_gen.state, 1, 15)) gc_state
                                     ,trim(substr(con_gen.postal_code, 1, 15)) gc_postal_code
                                     ,trim(substr(con_gen.responsibility_type, 1,
                                             75)) gc_resp_type
                                     ,trim(substr(con_own.con_first_name, 1, 100)) ownr_first_name -- OWNER CONTACT INFO
                                     ,trim(substr(con_own.con_last_name, 1, 50)) ownr_last_name
                                     ,trim(substr(con_own.email_address, 1, 75)) ownr_email_address
                                     ,trim(substr(con_own.phone, 1, 50)) ownr_phone
                                     ,trim(substr(con_own.fax, 1, 50)) ownr_fax
                                     ,trim(substr(con_own.address1, 1, 50)) ownr_address1
                                     ,trim(substr(con_own.address2, 1, 50)) ownr_address2
                                     ,trim(substr(con_own.city, 1, 50)) ownr_city
                                     ,trim(substr(con_own.county, 1, 50)) ownr_county
                                     ,trim(substr(con_own.state, 1, 15)) ownr_state
                                     ,trim(substr(con_own.postal_code, 1, 15)) ownr_postal_code
                                     ,trim(substr(con_own.responsibility_type, 1,
                                             75)) ownr_resp_type
                                     ,trim(substr(con_bond.con_first_name, 1, 100)) bc_first_name -- BONDING CONTACT INFO
                                     ,trim(substr(con_bond.con_last_name, 1, 50)) bc_last_name
                                     ,trim(substr(con_bond.email_address, 1, 75)) bc_email_address
                                     ,trim(substr(con_bond.phone, 1, 50)) bc_phone
                                     ,trim(substr(con_bond.fax, 1, 50)) bc_fax
                                     ,trim(substr(con_bond.address1, 1, 50)) bc_address1
                                     ,trim(substr(con_bond.address2, 1, 50)) bc_address2
                                     ,trim(substr(con_bond.city, 1, 50)) bc_city
                                     ,trim(substr(con_bond.county, 1, 50)) bc_county
                                     ,trim(substr(con_bond.state, 1, 15)) bc_state
                                     ,trim(substr(con_bond.postal_code, 1, 15)) bc_postal_code
                                     ,trim(substr(con_bond.responsibility_type, 1,
                                             75)) bc_resp_type
                                     ,trim(substr(con_fin.con_first_name, 1, 100)) fi_first_name -- FINANCIAL INST. CONTACT INFO
                                     ,trim(substr(con_fin.con_last_name, 1, 50)) fi_last_name
                                     ,trim(substr(con_fin.email_address, 1, 75)) fi_email_address
                                     ,trim(substr(con_fin.phone, 1, 50)) fi_phone
                                     ,trim(substr(con_fin.fax, 1, 50)) fi_fax
                                     ,trim(substr(con_fin.address1, 1, 50)) fi_address1
                                     ,trim(substr(con_fin.address2, 1, 50)) fi_address2
                                     ,trim(substr(con_fin.city, 1, 50)) fi_city
                                     ,trim(substr(con_fin.county, 1, 50)) fi_county
                                     ,trim(substr(con_fin.state, 1, 15)) fi_state
                                     ,trim(substr(con_fin.postal_code, 1, 15)) fi_postal_code
                                     ,trim(substr(con_fin.responsibility_type, 1,
                                             75)) fi_resp_type
                                     ,rcta.org_id
									 ,rcta.batch_source_id batch_source_id--changed for ver 1.8 for TMS 20160217-00098 
                        FROM apps.hz_parties              p
                            ,apps.ra_customer_trx         rcta
                            ,apps.hz_cust_accounts        hca
                            ,apps.hz_cust_site_uses       hcsu_s
                            ,apps.hz_cust_site_uses       hcsu_s2 -- v1.1
                            ,apps.hz_cust_acct_sites      hcas_s
                            ,apps.hz_party_sites              hps_s
                            ,apps.hz_locations                hl_s
                            ,apps.hz_cust_site_uses       hcsu_b
                            ,apps.hz_cust_acct_sites      hcas_b
                            ,apps.hz_party_sites              hps_b
                            ,apps.hz_locations                hl_b
                            ,apps.ar_payment_schedules    apsa
                            ,apps.hz_customer_profiles        hcp
                            ,apps.ar_collectors               ac
                            ,apps.fnd_user                       u
                            ,apps.ar_lookups                al
                            ,xxwc.xxwc_getpaid_contacts_tbl gc
                            ,apps.hz_customer_profiles        cus_prof
                            ,apps.hz_cust_profile_classes     cus_class
                            ,apps.ra_cust_trx_types       trx_type                             
                            ,(SELECT *
                                FROM apps.xxwc_cust_site_contact_vw
                               WHERE responsibility_type = 'GENERAL') con_gen
                            ,(SELECT *
                                FROM apps.xxwc_cust_site_contact_vw
                               WHERE responsibility_type = 'OWNER') con_own
                            ,(SELECT *
                                FROM apps.xxwc_cust_site_contact_vw
                               WHERE responsibility_type = 'BONDING') con_bond
                            ,(SELECT *
                                FROM apps.xxwc_cust_site_contact_vw
                               WHERE responsibility_type = 'FINANCIAL') con_fin
                            ,(SELECT t.trx_party_site_number
                                    ,SUM(t.transaction_remaining_balance) balance
                                FROM xxeis.xxwc_ar_customer_balance_mv t
                               GROUP BY t.trx_party_site_number) bal
                       WHERE 1 = 1
                         AND rcta.bill_to_customer_id = hca.cust_account_id
                         AND rcta.customer_trx_id = apsa.customer_trx_id
                         AND rcta.ship_to_site_use_id = hcsu_s.site_use_id
                         AND nvl(hcsu_s.bill_to_site_use_id,
                                 hcsu_s.site_use_id) =
                             hcsu_s2.site_use_id(+) -- v1.1
                         AND hcsu_s2.cust_acct_site_id =
                             hcas_s.cust_acct_site_id(+)
                         AND hcas_s.party_site_id = hps_s.party_site_id
                         AND hps_s.party_id = p.party_id
                         AND hps_s.party_site_number =
                             bal.trx_party_site_number(+)
                         AND hps_s.location_id = hl_s.location_id
                         AND hca.cust_account_id = hcas_b.cust_account_id
                         AND hcas_b.cust_acct_site_id =
                             hcsu_b.cust_acct_site_id
                         AND hca.cust_account_id = hcp.cust_account_id
                         AND hcp.collector_id = ac.collector_id
                         AND ac.employee_id = u.employee_id
                         AND u.user_name = al.lookup_code
                         AND hca.account_number = gc.custno(+)
                         AND hcas_s.cust_acct_site_id =
                             con_gen.cust_account_site_id(+)
                         AND hcas_s.cust_acct_site_id =
                             con_own.cust_account_site_id(+)
                         AND hcas_s.cust_acct_site_id =
                             con_bond.cust_account_site_id(+)
                         AND hcas_s.cust_acct_site_id =
                             con_fin.cust_account_site_id(+)
                         AND hca.cust_account_id =
                             cus_prof.cust_account_id(+)
                         AND cus_prof.profile_class_id =
                             cus_class.profile_class_id(+)
                         AND rcta.cust_trx_type_id =
                             trx_type.cust_trx_type_id                            
                         AND hcsu_b.site_use_code = 'BILL_TO'
                         AND hcsu_b.primary_flag = 'Y'
                         AND hcas_b.party_site_id = hps_b.party_site_id
                         AND hps_b.location_id = hl_b.location_id
                         AND hcp.site_use_id IS NULL
                         AND rcta.attribute12 IS NULL
                         AND rcta.ship_to_customer_id IS NOT NULL
                         --AND rcta.org_id = 162
                         --AND cus_prof.profile_class_id NOT IN (1040, 2046, 2053)
                      --   AND cus_class.name NOT IN ('COD Customers','Intercompany Customers','WC Branches','Branch Cash Account') ----COMMENTED BY HARSHA FOR TMS Ticket# 20131115-00077.
                         AND NOT EXISTS (select 1 from fnd_lookup_values FLV where lookup_type='XXWC_ARS_PROFILE_CLASS'  --ADDED BY HARSHA FOR TMS Ticket# 20131115-00077.
                and trunc(sysdate) between trunc(start_date_active) and trunc(nvl(end_date_active,sysdate+1))
                                 AND FLV.MEANING=CUS_CLASS.NAME) --ADDED BY HARSHA FOR TMS Ticket# 20131115-00077.
                         AND cus_prof.site_use_id IS NULL
                         AND cus_prof.account_status <> 'NO NOTICE'
                         AND trx_type.type IN ('INV', 'CM', 'DM')
                         AND hcsu_s.attribute1 IN ('JOB', 'MISC')
                         AND al.lookup_type = 'XXWC_GETPAID_COLLECTOR_XREF'
                         AND al.enabled_flag = 'Y')
      LOOP
      
        INSERT /*+ APPEND */
        INTO xxwc_ars_ob_tbl
        VALUES
          (inv_rec.account_name
          ,inv_rec.bill_addr
          ,inv_rec.bill_city
          ,inv_rec.bill_state
          ,inv_rec.bill_postal_code
          ,inv_rec.ship_addr
          ,inv_rec.ship_city
          ,inv_rec.ship_state
          ,inv_rec.ship_postal_code
          ,inv_rec.location
          ,inv_rec.party_site_number
          ,inv_rec.balance
          ,inv_rec.amount_due_original
          ,inv_rec.trx_number
          ,inv_rec.trx_date
          ,inv_rec.account_number
          ,inv_rec.description
          ,inv_rec.attribute1
          ,inv_rec.contact_id
          ,inv_rec.firstname
          ,inv_rec.lastname
          ,inv_rec.dear
          ,inv_rec.title
          ,inv_rec.fax
          ,inv_rec.phone
          ,inv_rec.hphone
          ,inv_rec.address1
          ,inv_rec.address2
          ,inv_rec.address3
          ,inv_rec.con_city
          ,inv_rec.con_state
          ,inv_rec.zip
          ,inv_rec.email
          ,inv_rec.gc_first_name
          ,inv_rec.gc_last_name
          ,inv_rec.gc_email_address
          ,inv_rec.gc_phone
          ,inv_rec.gc_fax
          ,inv_rec.gc_address1
          ,inv_rec.gc_address2
          ,inv_rec.gc_city
          ,inv_rec.gc_county
          ,inv_rec.gc_state
          ,inv_rec.gc_postal_code
          ,inv_rec.gc_resp_type
          ,inv_rec.ownr_first_name
          ,inv_rec.ownr_last_name
          ,inv_rec.ownr_email_address
          ,inv_rec.ownr_phone
          ,inv_rec.ownr_fax
          ,inv_rec.ownr_address1
          ,inv_rec.ownr_address2
          ,inv_rec.ownr_city
          ,inv_rec.ownr_county
          ,inv_rec.ownr_state
          ,inv_rec.ownr_postal_code
          ,inv_rec.ownr_resp_type
          ,inv_rec.bc_first_name
          ,inv_rec.bc_last_name
          ,inv_rec.bc_email_address
          ,inv_rec.bc_phone
          ,inv_rec.bc_fax
          ,inv_rec.bc_address1
          ,inv_rec.bc_address2
          ,inv_rec.bc_city
          ,inv_rec.bc_county
          ,inv_rec.bc_state
          ,inv_rec.bc_postal_code
          ,inv_rec.bc_resp_type
          ,inv_rec.fi_first_name
          ,inv_rec.fi_last_name
          ,inv_rec.fi_email_address
          ,inv_rec.fi_phone
          ,inv_rec.fi_fax
          ,inv_rec.fi_address1
          ,inv_rec.fi_address2
          ,inv_rec.fi_city
          ,inv_rec.fi_county
          ,inv_rec.fi_state
          ,inv_rec.fi_postal_code
          ,inv_rec.fi_resp_type
          ,inv_rec.org_id
		  ,inv_rec.batch_source_id);   --changed for ver 1.8 for TMS 20160217-00098
      
        l_rec_count := l_rec_count + 1;
      
        IF l_rec_count = 1000 THEN
          COMMIT;
        END IF;
        COMMIT;
      END LOOP;
      -- END;
    
    EXCEPTION
      WHEN OTHERS THEN
        l_err_msg := 'Error populating table xxwc.xxwc_ars_ob_tbl :' ||
                     SQLERRM;
        RAISE l_program_error;
    END;
  
    l_sec := 'Update Invoice attributes';
  
    BEGIN
      FOR c_trx IN (SELECT DISTINCT trx_number,org_id,batch_source --changed for ver 1.8 for TMS 20160217-00098 starts
                      FROM apps.xxwc_ars_ob_tbl)
      LOOP
        UPDATE ra_customer_trx
           SET last_update_date = sysdate,                --changed for ver 1.8 for TMS 20160217-00098 starts
           last_updated_by      = 1290,
           attribute12          = NVL(attribute12 ,trunc(SYSDATE) ) 
           ,attribute_category  = c_trx.org_id            --162
         WHERE trx_number = c_trx.trx_number
		 AND  batch_source_id     = c_trx.batch_source  ;
		 --changed for ver 1.8 for TMS 20160217-00098 ends
      END LOOP;
      COMMIT;
    
    EXCEPTION
      WHEN OTHERS THEN
        l_err_msg := 'Error update invoice attribute12 (NULL) :' || SQLERRM;
        RAISE l_program_error;
    END;
  
    p_retcode := 0;
  
  EXCEPTION
  
    WHEN l_program_error THEN
      p_retcode  := 2;
      l_err_code := 2;
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_procedure_name,
                                           p_calling => l_sec,
                                           p_request_id => fnd_global.conc_request_id,
                                           p_ora_error_msg => l_err_msg,
                                           p_error_desc => 'Error running ARS Outbound package with Program Error Exception',
                                           p_distribution_list => pl_dflt_email,
                                           p_module => 'XXCUS');
    WHEN OTHERS THEN
      p_retcode  := 2;
      l_err_code := 2;
      l_err_msg  := l_sec || l_err_msg || ' ERROR ' || SQLCODE ||
                    substr(SQLERRM, 1, 2000);
    
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_procedure_name,
                                           p_calling => l_sec,
                                           p_request_id => fnd_global.conc_request_id,
                                           p_ora_error_msg => SQLERRM,
                                           p_error_desc => 'Error running ARS Outbound package with Program Error Exception',
                                           p_distribution_list => pl_dflt_email,
                                           p_module => 'XXCUS');
  END;

  FUNCTION get_balance(p_site IN VARCHAR2) RETURN NUMBER IS
    /********************************************************************************
    FILE NAME: XXWCAR_ARS_OUTBOUND_PKG
    
    PROGRAM TYPE: PL/SQL Package Body
    
    PURPOSE: Get job site balance.
    
    HISTORY
    ===============================================================================
    VERSION DATE          AUTHOR(S)       DESCRIPTION
    ------- -----------   --------------- -----------------------------------------
    1.0     07/25/2012    Luong Vu        Initial creation 
    ********************************************************************************/
  
  
    l_balance NUMBER;
  
  BEGIN
  
    SELECT SUM(transaction_remaining_balance)
      INTO l_balance
      FROM xxeis.xxwc_ar_customer_balance_mv
     WHERE trx_party_site_number = p_site;
  
    RETURN l_balance;
  
  END get_balance;

PROCEDURE submit_job(errbuf       OUT  VARCHAR2
                    ,retcode      OUT  NUMBER
                  ,p_resp_name  IN   VARCHAR2
            ,p_user_name  IN   VARCHAR2
               ,p_org_name   IN   VARCHAR2) IS 
 /********************************************************************************
    FILE NAME: XXWCAR_ARS_OUTBOUND_PKG
    
    PROGRAM TYPE: PL/SQL Package Body
    
    PURPOSE: fOR .
    
    HISTORY
    ===============================================================================
    VERSION DATE          AUTHOR(S)                DESCRIPTION
    ------- -----------   --------------- -----------------------------------------
    1.0     09/18/2014  Maharajan Shunmugam        TMS# 20141001-00153 Canada - Initial creation   
    1.2     12/30/2014  Maharajan Shunmugam        TMS# 20141230-00067 ARS file not processing data correctly
    1.6    3/3/2015    Raghavendra S                  TMS# 20150227-00240 -- ARS Extract - fix Max Length logic and Exception handling
                                                    Added trim function for the columns in Select  stmt and exception handle for submit job procedure
    1.7    23/06/2015    Mullamuri 
                       Hari Prasad                  TMS# 20150620-00001 to fixed UC4 Job received return code 0                                                
    ********************************************************************************/
                     
l_resp_id       NUMBER;
l_resp_app_id   NUMBER;
l_user_id       NUMBER;
l_org_id        NUMBER;
l_file1         VARCHAR2(240);
l_file2         VARCHAR2(240);
l_file_path     VARCHAR2(240);
l_req_id   NUMBER;
l_call_status   BOOLEAN; 
l_rphase        VARCHAR2(80); 
l_rstatus       VARCHAR2(80); 
l_dphase        VARCHAR2(30); 
l_dstatus       VARCHAR2(30); 
l_message       VARCHAR2(500);
l_sec       VARCHAR2(250);
l_distro_list VARCHAR2(250) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
--- Added start for  variables  v 1.7
l_statement     VARCHAR2 (9000);
l_err_msg       VARCHAR2 (3000);
l_err_code      NUMBER;
v_phase         VARCHAR2 (50);
v_status        VARCHAR2 (50);
l_err_callfrom  VARCHAR2 (75) DEFAULT 'xxcus_error_pkg';
l_err_callpoint VARCHAR2 (75) DEFAULT 'START';
v_dev_phase     VARCHAR2 (50);
v_dev_status    VARCHAR2 (50);
v_message       VARCHAR2 (250);
v_error_message VARCHAR2 (3000);
--- Added end for  variables  v 1.7
BEGIN

DBMS_OUTPUT.put_line ('Entering submit_job...');
DBMS_OUTPUT.put_line ('p_user_name: ' || p_user_name);
DBMS_OUTPUT.put_line ('p_responsibility_name: ' || p_resp_name);
DBMS_OUTPUT.put_line ('p_operating_unit_name: ' || p_org_name);


BEGIN
  SELECT application_id
  ,      responsibility_id  
  INTO   l_resp_app_id
  ,      l_resp_id
  FROM   apps.fnd_responsibility_tl
  WHERE  responsibility_name =p_resp_name; 
EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_message  :=
                  'Responsibility '
               || p_resp_name
               || ' not defined in Oracle';
            RAISE PROGRAM_ERROR;
         WHEN OTHERS
         THEN
            l_message :=
                  'Error deriving Responsibility_id for '
               || p_resp_name;
            RAISE PROGRAM_ERROR;
      END;


BEGIN
  SELECT user_id 
  INTO   l_user_id 
  FROM   apps.fnd_user 
  WHERE  user_name =p_user_name;
EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_message  :=
                       'UserName ' || p_user_name || ' not defined in Oracle';
            RAISE PROGRAM_ERROR;
         WHEN OTHERS
         THEN
            l_message  :=
                      'Error deriving user_id for UserName - ' || p_user_name;
            RAISE PROGRAM_ERROR;
      END;

BEGIN  
  SELECT organization_id 
  INTO   l_org_id
  FROM   apps.hr_operating_units 
  WHERE   name =p_org_name;
EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_message :=
                  'Operating unit '
               || p_org_name
               || ' is invalid in Oracle';
            RAISE PROGRAM_ERROR;
         WHEN OTHERS
         THEN
            l_message  :=
                  'Other Operating unit validation error for '
               || p_org_name
               || '. Error: '
               || SUBSTR (SQLERRM, 1, 250);
            RAISE PROGRAM_ERROR;
      END;
  
  fnd_global.apps_initialize(l_user_id, l_resp_id, l_resp_app_id);

  mo_global.set_policy_context('S',l_org_id);
 
 
 
  l_sec:='Concurrent Program submiting';


  
  l_req_id :=fnd_request.submit_request (
                                       application   => 'XXWC',
                                       program       => 'XXWCAR_ARS_OUTBOUND',
                                       description   => 'ARS Outbound Interfaces. Refresh GETPAID contact table before running',
                                       start_time    => '',
                                       sub_request   => FALSE
                                       );
  COMMIT;
  
DBMS_OUTPUT.put_line
                      (   'Concurrent Program Request Submitted. Request ID: '
                       || l_req_id
                      );

--- Added start for  wait condition   v 1.7
  IF l_req_id > 0 THEN 

    IF fnd_concurrent.wait_for_request (l_req_id
                                            ,6
                                            ,3600
                                            ,v_phase
                                            ,v_status
                                            ,v_dev_phase
                                            ,v_dev_status
                                            ,v_message)
         THEN
            v_error_message :=
                  CHR (10)
               || 'ReqID='
               || l_req_id
               || ' DPhase '
               || v_dev_phase
               || ' DStatus '
               || v_dev_status
               || CHR (10)
               || ' MSG - '
               || v_message;

            -- Error Returned
            IF v_dev_phase != 'COMPLETE' OR v_dev_status != 'NORMAL'
            THEN
               l_statement :=
                     'An error occured running the xxwc_ars_outbound_pkg'
                  || v_error_message
                  || '.';
               fnd_file.put_line (fnd_file.LOG, l_statement);
               fnd_file.put_line (fnd_file.output, l_statement);
               RAISE PROGRAM_ERROR;
            END IF;
         -- Then Success!
         ELSE
            l_statement :=
                  'An error occured running the xxwc_ars_outbound_pkg'
               || v_error_message
               || '.';
            fnd_file.put_line (fnd_file.LOG, l_statement);
            fnd_file.put_line (fnd_file.output, l_statement);
            RAISE PROGRAM_ERROR;
         END IF;
      ELSE
         l_statement := 'An error occured running the xxwc_ars_outbound_pkg';
         fnd_file.put_line (fnd_file.LOG, l_statement);
         fnd_file.put_line (fnd_file.output, l_statement);
         RAISE PROGRAM_ERROR;
      END IF;

      DBMS_OUTPUT.put_line (l_sec);
      DBMS_OUTPUT.put_line ('Request ID:  ' || l_req_id);
      retcode := 0;
   EXCEPTION
      WHEN PROGRAM_ERROR
      THEN
         ROLLBACK;
         l_err_code := 2;
         l_err_msg := l_sec;
         l_err_msg := l_err_msg || ' ERROR ' || SUBSTR (SQLERRM, 1, 2000);
         DBMS_OUTPUT.put_line (l_err_msg);
         fnd_file.put_line (fnd_file.LOG, l_err_msg);
         fnd_file.put_line (fnd_file.output, l_err_msg);
         l_err_callpoint := l_message;

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_err_callfrom
           ,p_calling             => l_err_callpoint
           ,p_request_id          => l_req_id
           ,p_ora_error_msg       => SQLERRM
           ,p_error_desc          => 'Error running xxwc_ars_outbound_pkg package with PROGRAM ERROR'
           ,p_distribution_list   => l_distro_list
           ,p_module              => 'XXCUS');

         fnd_file.put_line (fnd_file.output, 'Fix the error!');
         retcode := 2;
         --- Added end  for  wait condition   v 1.7
      
WHEN OTHERS
THEN
  l_message := 'Error: ' || SQLERRM;
   retcode:= 2;  -- Added for V 1.6
    fnd_file.put_line (fnd_file.log,'Error in XXWCAR_ARS_OUTBOUND_PKG');
    xxcus_error_pkg.xxcus_error_main_api
         (p_called_from            => 'XXWCAR_ARS_OUTBOUND_PKG',
          p_calling                => l_sec,
          p_request_id             => l_req_id,
          p_ora_error_msg          => l_message,
          p_error_desc             => 'Error running XXWCAR_ARS_OUTBOUND_PKG with PROGRAM ERROR',
          p_distribution_list      => l_distro_list,
          p_module                 => 'AR'
         );      
END submit_job;

END xxwc_ars_outbound_pkg;
/
GRANT EXECUTE ON apps.xxwc_ars_outbound_pkg TO interface_prism
/
GRANT EXECUTE ON apps.xxwc_ars_outbound_pkg TO interface_xxcus
/