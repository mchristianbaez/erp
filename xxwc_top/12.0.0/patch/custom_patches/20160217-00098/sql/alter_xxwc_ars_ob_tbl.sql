  /********************************************************************************
  FILE NAME: alter_xxwc_ars_ob_tbl.sql
  
  PROGRAM TYPE: PL/SQL Package Body
  
  PURPOSE: ARS Outbound Interfaces.
  
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     02/16/2016   NEHA SAINI      Initial creation 
  ********************************************************************************/
Alter Table XXWC.XXWC_ARS_OB_TBL ADD BATCH_SOURCE NUMBER;