  set serveroutput on;
 /*************************************************************************
      PURPOSE:  Close Sales order Line ID 94279068

      REVISIONS:
      Ver        Date          Author             Description
      ---------  ------------  ------------       ------------------
      1.0        05-Oct-2017  Sundaramoorthy     Initial Version - TMS #20170922-00240 
	************************************************************************/ 
 BEGIN
 dbms_output.put_line ('Start Update ');

 UPDATE oe_order_lines_all
  SET  flow_status_code ='CLOSED'
  , open_flag ='N'
  WHERE line_id = 94279068
  AND header_id =57836633;  
  COMMIT;
  
  dbms_output.put_line ('Update count '||SQL%ROWCOUNT);
  EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
	dbms_output.put_line ('Inside Exception '|| SUBSTR(SQLERRM,1,2000));	
END;
/
			   
  