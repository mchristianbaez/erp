/*************************************************************************
  $Header TMS_20160224-00206_Order Line stuck in Awaiting Invoice.sql $
  
  Module Name: TMS_20160224-00206 Progress the line from Awaiting Invoice to Close.

  PURPOSE: Data fix to close the order line.

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        24-FEB-2016  Gopi Damulri        TMS#20160224-00206 

**************************************************************************/
 
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20160224-00206  , Before Update');

   UPDATE apps.oe_order_lines_all
      SET 
          INVOICE_INTERFACE_STATUS_CODE='YES', open_flag='N',flow_status_code='CLOSED',INVOICED_QUANTITY=250
    WHERE line_id=52941582 --Pass the line id
      AND header_id=32225077;

   DBMS_OUTPUT.put_line (
         'TMS: 20160224-00206 Sales Order Line 1.1 progressed and closed ' || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20160224-00206   , End Update');
   
EXCEPTION
   
   WHEN OTHERS
   THEN
   ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20160224-00206, Errors : ' || SQLERRM);
END;
/