/*************************************************************************
 Copyright (c) 2012 HD Supply
 All rights reserved.
**************************************************************************
   $Header APPS.XXWC_MD_SEARCH_MV2_N2 $
  Module Name: APPS.XXWC_MD_SEARCH_MV2_N2

  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     9-MAR-2016   Pahwa, Nancy                Initially Created 
TMS# 20160310-00138 
**************************************************************************/
create index APPS.XXWC_MD_SEARCH_MV2_N2 on XXWC_MD_SEARCH_PRODUCTS_MV2 (shortdescription)
indextype is ctxsys.context parameters ('wordlist XXWC_MD_PRODUCT_SEARCH_PREF');