--Report Name            : Oracle User Access Review - WC
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data EIS_XXWC_USER_ACCESS_REVIEW_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(85000);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_USER_ACCESS_REVIEW_V
xxeis.eis_rsc_ins.v( 'EIS_XXWC_USER_ACCESS_REVIEW_V',85000,'','','','','SA059956','XXEIS','Xxwc User Access Review V','EXUARV','','','VIEW','US','','XXWC_USER_ROLES_VW','');
--Delete Object Columns for EIS_XXWC_USER_ACCESS_REVIEW_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_USER_ACCESS_REVIEW_V',85000,FALSE);
--Inserting Object Columns for EIS_XXWC_USER_ACCESS_REVIEW_V
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_USER_ACCESS_REVIEW_V','EFFECTIVE_END_DATE',85000,'Effective End Date','EFFECTIVE_END_DATE','','','','SA059956','DATE','','','Effective End Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_USER_ACCESS_REVIEW_V','EFFECTIVE_START_DATE',85000,'Effective Start Date','EFFECTIVE_START_DATE','','','','SA059956','DATE','','','Effective Start Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_USER_ACCESS_REVIEW_V','LAST_ASSIGNED_BY',85000,'Last Assigned By','LAST_ASSIGNED_BY','','','','SA059956','VARCHAR2','','','Last Assigned By','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_USER_ACCESS_REVIEW_V','SUPER_NAME',85000,'Super Name','SUPER_NAME','','','','SA059956','VARCHAR2','','','Super Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_USER_ACCESS_REVIEW_V','SUPER_CODE',85000,'Super Code','SUPER_CODE','','','','SA059956','VARCHAR2','','','Super Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_USER_ACCESS_REVIEW_V','FULL_NAME',85000,'Full Name','FULL_NAME','','','','SA059956','VARCHAR2','','','Full Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_USER_ACCESS_REVIEW_V','USER_NAME',85000,'User Name','USER_NAME','','','','SA059956','VARCHAR2','','','User Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_USER_ACCESS_REVIEW_V','EMAIL_ADDRESS',85000,'Email Address','EMAIL_ADDRESS','','','','SA059956','VARCHAR2','','','Email Address','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_USER_ACCESS_REVIEW_V','SUPERVISOR_EMAIL',85000,'Supervisor Email','SUPERVISOR_EMAIL','','','','SA059956','VARCHAR2','','','Supervisor Email','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_USER_ACCESS_REVIEW_V','SUPERVISOR_NAME',85000,'Supervisor Name','SUPERVISOR_NAME','','','','SA059956','VARCHAR2','','','Supervisor Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_USER_ACCESS_REVIEW_V','EMPLOYEE_NUMBER',85000,'Employee Number','EMPLOYEE_NUMBER','','','','SA059956','VARCHAR2','','','Employee Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_USER_ACCESS_REVIEW_V','IS_BUYER',85000,'Is Buyer','IS_BUYER','','','','SA059956','CHAR','','','Is Buyer','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_USER_ACCESS_REVIEW_V','BUYER_JOB',85000,'Buyer Job','BUYER_JOB','','','','SA059956','VARCHAR2','','','Buyer Job','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_USER_ACCESS_REVIEW_V','BUYER_POSITION',85000,'Buyer Position','BUYER_POSITION','','','','SA059956','VARCHAR2','','','Buyer Position','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_USER_ACCESS_REVIEW_V','ASSIGNMENT_REASON',85000,'Assignment Reason','ASSIGNMENT_REASON','','','','SA059956','VARCHAR2','','','Assignment Reason','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_USER_ACCESS_REVIEW_V','GL_SEGMENT',85000,'Gl Segment','GL_SEGMENT','','','','SA059956','VARCHAR2','','','Gl Segment','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_USER_ACCESS_REVIEW_V','SCOPE',85000,'Scope','SCOPE','','','','SA059956','VARCHAR2','','','Scope','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_USER_ACCESS_REVIEW_V','BRANCH',85000,'Branch','BRANCH','','','','SA059956','VARCHAR2','','','Branch','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_USER_ACCESS_REVIEW_V','REGION',85000,'Region','REGION','','','','SA059956','VARCHAR2','','','Region','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_USER_ACCESS_REVIEW_V','FRU',85000,'Fru','FRU','','','','SA059956','VARCHAR2','','','Fru','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_USER_ACCESS_REVIEW_V','LOB',85000,'Lob','LOB','','','','SA059956','VARCHAR2','','','Lob','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_USER_ACCESS_REVIEW_V','FRU_DESCRIPTION',85000,'Fru Description','FRU_DESCRIPTION','','','','SA059956','VARCHAR2','','','Fru Description','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_USER_ACCESS_REVIEW_V','JOB_DESCRIPTION',85000,'Job Description','JOB_DESCRIPTION','','','','SA059956','VARCHAR2','','','Job Description','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_USER_ACCESS_REVIEW_V','LAST_UPDATE_DATE',85000,'Last Update Date','LAST_UPDATE_DATE','','','','SA059956','DATE','','','Last Update Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_USER_ACCESS_REVIEW_V','BUYER_EMPLOYEE_ID',85000,'Buyer Employee Id','BUYER_EMPLOYEE_ID','','','','SA059956','NUMBER','','','Buyer Employee Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_USER_ACCESS_REVIEW_V','ROLE_ORIG_SYSTEM',85000,'Role Orig System','ROLE_ORIG_SYSTEM','','','','SA059956','VARCHAR2','','','Role Orig System','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_USER_ACCESS_REVIEW_V','JOB_FAMILY_DESCR',85000,'Job Family Descr','JOB_FAMILY_DESCR','','','','SA059956','VARCHAR2','','','Job Family Descr','','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_USER_ACCESS_REVIEW_V','REVIEWER',85000,'Reviewer','REVIEWER','','','','SA059956','VARCHAR2','','','Reviewer','','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_USER_ACCESS_REVIEW_V','REVIEWER_EMAIL_ADDRESS',85000,'Reviewer Email Address','REVIEWER_EMAIL_ADDRESS','','','','SA059956','VARCHAR2','','','Reviewer Email Address','','','','','');
--Inserting Object Components for EIS_XXWC_USER_ACCESS_REVIEW_V
--Inserting Object Component Joins for EIS_XXWC_USER_ACCESS_REVIEW_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 85000');
END IF;
END;
/
--There are no LOV/LOV TL entries for the report Oracle User Access Review - WC
prompt Creating Report Data for Oracle User Access Review - WC
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(85000);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Oracle User Access Review - WC
xxeis.eis_rsc_utility.delete_report_rows( 'Oracle User Access Review - WC',85000 );
--Inserting Report - Oracle User Access Review - WC
xxeis.eis_rsc_ins.r( 85000,'Oracle User Access Review - WC','','This Report is copied from WC User Roles and Responsibilities.Create User Role Responsibilities view for reporting.This view shows all responsibilities and roles for all WC users, or users who have been assigned a WC role or Responsibility.
The Parent column shows the role that provides access to the roles/responsibilities shown in the Child column. For responsibilities directly assigned the parent will be the same as the child.
In cases where there is a hierarchy (parent role has a sub-role, and the sub-role has children) it is flattened in this report. All items are included, but shown as rolling up under the top-level parent. Intermediate roles are also shown as rolling up to the parent.','','','','SA059956','EIS_XXWC_USER_ACCESS_REVIEW_V','Y','','','SA059956','','N','WC Audit Reports','','CSV,HTML,EXCEL,','N','','','','','','N','','US','','WC User Roles and Responsibilities','','','','','','','','','','','','','','');
--Inserting Report Columns - Oracle User Access Review - WC
xxeis.eis_rsc_ins.rc( 'Oracle User Access Review - WC',85000,'SUPER_CODE','Parent Code','Super Code','','','default','','12','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_USER_ACCESS_REVIEW_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Oracle User Access Review - WC',85000,'EFFECTIVE_END_DATE','Effective End Date','Effective End Date','','','default','','9','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_USER_ACCESS_REVIEW_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Oracle User Access Review - WC',85000,'EFFECTIVE_START_DATE','Effective Start Date','Effective Start Date','','','default','','8','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_USER_ACCESS_REVIEW_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Oracle User Access Review - WC',85000,'SUPER_NAME','Parent Role','Super Name','','','default','','5','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_USER_ACCESS_REVIEW_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Oracle User Access Review - WC',85000,'USER_NAME','User Name','User Name','','','default','','1','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_USER_ACCESS_REVIEW_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Oracle User Access Review - WC',85000,'EMAIL_ADDRESS','Email Address','Email Address','','','default','','4','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_USER_ACCESS_REVIEW_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Oracle User Access Review - WC',85000,'SUPERVISOR_EMAIL','Supervisor Email','Supervisor Email','','','default','','15','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_USER_ACCESS_REVIEW_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Oracle User Access Review - WC',85000,'SUPERVISOR_NAME','Supervisor Name','Supervisor Name','','','default','','14','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_USER_ACCESS_REVIEW_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Oracle User Access Review - WC',85000,'EMPLOYEE_NUMBER','Employee Number','Employee Number','','','default','','2','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_USER_ACCESS_REVIEW_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Oracle User Access Review - WC',85000,'IS_BUYER','Is Buyer','Is Buyer','','','default','','18','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_USER_ACCESS_REVIEW_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Oracle User Access Review - WC',85000,'BUYER_JOB','Buyer Job','Buyer Job','','','default','','16','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_USER_ACCESS_REVIEW_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Oracle User Access Review - WC',85000,'BUYER_POSITION','Buyer Position','Buyer Position','','','default','','17','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_USER_ACCESS_REVIEW_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Oracle User Access Review - WC',85000,'ASSIGNMENT_REASON','Assignment Reason','Assignment Reason','','','default','','11','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_USER_ACCESS_REVIEW_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Oracle User Access Review - WC',85000,'GL_SEGMENT','GL Segment','Gl Segment','','','default','','19','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_USER_ACCESS_REVIEW_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Oracle User Access Review - WC',85000,'SCOPE','Scope','Scope','','','default','','20','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_USER_ACCESS_REVIEW_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Oracle User Access Review - WC',85000,'BRANCH','Branch','Branch','','','default','','24','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_USER_ACCESS_REVIEW_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Oracle User Access Review - WC',85000,'REGION','Region','Region','','','default','','25','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_USER_ACCESS_REVIEW_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Oracle User Access Review - WC',85000,'FRU','Fru','Fru','','','default','','22','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_USER_ACCESS_REVIEW_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Oracle User Access Review - WC',85000,'LOB','Lob','Lob','','','default','','26','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_USER_ACCESS_REVIEW_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Oracle User Access Review - WC',85000,'FRU_DESCRIPTION','Fru Description','Fru Description','','','default','','23','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_USER_ACCESS_REVIEW_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Oracle User Access Review - WC',85000,'JOB_DESCRIPTION','Job Description','Job Description','','','default','','21','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_USER_ACCESS_REVIEW_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Oracle User Access Review - WC',85000,'LAST_UPDATE_DATE','Last Update Date','Last Update Date','','','default','','7','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_USER_ACCESS_REVIEW_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Oracle User Access Review - WC',85000,'FULL_NAME','Full Name','Full Name','','','default','','3','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_USER_ACCESS_REVIEW_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Oracle User Access Review - WC',85000,'LAST_ASSIGNED_BY','Last Assigned By','Last Assigned By','','','default','','10','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_USER_ACCESS_REVIEW_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Oracle User Access Review - WC',85000,'REVIEWER','Reviewer','Reviewer','VARCHAR2','','default','','27','','Y','','','','','','','SA059956','','','','EIS_XXWC_USER_ACCESS_REVIEW_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Oracle User Access Review - WC',85000,'REVIEWER_EMAIL_ADDRESS','Reviewer Email','Reviewer Email Address','VARCHAR2','','default','','','','Y','','','','','','','SA059956','','','','EIS_XXWC_USER_ACCESS_REVIEW_V','','','','US','','');
--Inserting Report Parameters - Oracle User Access Review - WC
xxeis.eis_rsc_ins.rp( 'Oracle User Access Review - WC',85000,'Specific User NT ID','User''s login name, aka NT ID','USER_NAME','LIKE','','%','VARCHAR2','Y','Y','1','Y','Y','CONSTANT','SA059956','Y','','','','','EIS_XXWC_USER_ACCESS_REVIEW_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Oracle User Access Review - WC',85000,'User Full Name','User''s full name','FULL_NAME','LIKE','','%','VARCHAR2','N','Y','2','Y','Y','CONSTANT','SA059956','Y','','','','','EIS_XXWC_USER_ACCESS_REVIEW_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Oracle User Access Review - WC',85000,'Parent Role','The parent role or responsiiblity','SUPER_NAME','LIKE','','%','VARCHAR2','N','Y','3','Y','Y','CONSTANT','SA059956','Y','','','','','EIS_XXWC_USER_ACCESS_REVIEW_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Oracle User Access Review - WC',85000,'Employee Number','HR Employee ID','EMPLOYEE_NUMBER','LIKE','','%','VARCHAR2','N','Y','6','Y','Y','CONSTANT','SA059956','Y','','','','','EIS_XXWC_USER_ACCESS_REVIEW_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Oracle User Access Review - WC',85000,'Is Buyer (Y/N)','','IS_BUYER','LIKE','','%','VARCHAR2','N','Y','5','Y','Y','CONSTANT','SA059956','Y','','','','','EIS_XXWC_USER_ACCESS_REVIEW_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Oracle User Access Review - WC',85000,'GL Segment','Indicates the LOB','GL_SEGMENT','LIKE','','%','VARCHAR2','N','Y','7','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_USER_ACCESS_REVIEW_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Oracle User Access Review - WC',85000,'Scope','Auditing scope, either WC or GSC','SCOPE','LIKE','','WC','VARCHAR2','N','Y','8','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_USER_ACCESS_REVIEW_V','','','US','');
--Inserting Dependent Parameters - Oracle User Access Review - WC
--Inserting Report Conditions - Oracle User Access Review - WC
xxeis.eis_rsc_ins.rcnh( 'Oracle User Access Review - WC',85000,'EXUARV.USER_NAME LIKE Specific User NT ID','SIMPLE','','','Y','','1');
xxeis.eis_rsc_ins.rcnd( '','','USER_NAME','','Specific User NT ID','','','','','EIS_XXWC_USER_ACCESS_REVIEW_V','','','','','','LIKE','Y','Y','','','','','1',85000,'Oracle User Access Review - WC','EXUARV.USER_NAME LIKE Specific User NT ID');
xxeis.eis_rsc_ins.rcnh( 'Oracle User Access Review - WC',85000,'EXUARV.FULL_NAME LIKE User Full Name','SIMPLE','','','Y','','2');
xxeis.eis_rsc_ins.rcnd( '','','FULL_NAME','','User Full Name','','','','','EIS_XXWC_USER_ACCESS_REVIEW_V','','','','','','LIKE','Y','Y','','','','','1',85000,'Oracle User Access Review - WC','EXUARV.FULL_NAME LIKE User Full Name');
xxeis.eis_rsc_ins.rcnh( 'Oracle User Access Review - WC',85000,'EXUARV.SUPER_NAME LIKE Parent Role','SIMPLE','','','Y','','3');
xxeis.eis_rsc_ins.rcnd( '','','SUPER_NAME','','Parent Role','','','','','EIS_XXWC_USER_ACCESS_REVIEW_V','','','','','','LIKE','Y','Y','','','','','1',85000,'Oracle User Access Review - WC','EXUARV.SUPER_NAME LIKE Parent Role');
xxeis.eis_rsc_ins.rcnh( 'Oracle User Access Review - WC',85000,'EXUARV.IS_BUYER LIKE Is Buyer (Y/N)','SIMPLE','','','Y','','4');
xxeis.eis_rsc_ins.rcnd( '','','IS_BUYER','','Is Buyer (Y/N)','','','','','EIS_XXWC_USER_ACCESS_REVIEW_V','','','','','','LIKE','Y','Y','','','','','1',85000,'Oracle User Access Review - WC','EXUARV.IS_BUYER LIKE Is Buyer (Y/N)');
xxeis.eis_rsc_ins.rcnh( 'Oracle User Access Review - WC',85000,'EXUARV.EMPLOYEE_NUMBER LIKE Employee Number','SIMPLE','','','Y','','5');
xxeis.eis_rsc_ins.rcnd( '','','EMPLOYEE_NUMBER','','Employee Number','','','','','EIS_XXWC_USER_ACCESS_REVIEW_V','','','','','','LIKE','Y','Y','','','','','1',85000,'Oracle User Access Review - WC','EXUARV.EMPLOYEE_NUMBER LIKE Employee Number');
xxeis.eis_rsc_ins.rcnh( 'Oracle User Access Review - WC',85000,'EXUARV.GL_SEGMENT LIKE GL Segment','SIMPLE','','','Y','','6');
xxeis.eis_rsc_ins.rcnd( '','','GL_SEGMENT','','GL Segment','','','','','EIS_XXWC_USER_ACCESS_REVIEW_V','','','','','','LIKE','Y','Y','','','','','1',85000,'Oracle User Access Review - WC','EXUARV.GL_SEGMENT LIKE GL Segment');
xxeis.eis_rsc_ins.rcnh( 'Oracle User Access Review - WC',85000,'EXUARV.SCOPE LIKE Scope','SIMPLE','','','Y','','7');
xxeis.eis_rsc_ins.rcnd( '','','SCOPE','','Scope','','','','','EIS_XXWC_USER_ACCESS_REVIEW_V','','','','','','LIKE','Y','Y','','','','','1',85000,'Oracle User Access Review - WC','EXUARV.SCOPE LIKE Scope');
--Inserting Report Sorts - Oracle User Access Review - WC
xxeis.eis_rsc_ins.rs( 'Oracle User Access Review - WC',85000,'USER_NAME','ASC','SA059956','1','');
--Inserting Report Triggers - Oracle User Access Review - WC
--inserting report templates - Oracle User Access Review - WC
--Inserting Report Portals - Oracle User Access Review - WC
--inserting report dashboards - Oracle User Access Review - WC
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Oracle User Access Review - WC','85000','EIS_XXWC_USER_ACCESS_REVIEW_V','EIS_XXWC_USER_ACCESS_REVIEW_V','N','');
--inserting report security - Oracle User Access Review - WC
--Inserting Report Pivots - Oracle User Access Review - WC
xxeis.eis_rsc_ins.rpivot( 'Oracle User Access Review - WC',85000,'Pivot','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rsc_ins.rpivot_dtls( 'Oracle User Access Review - WC',85000,'Pivot','SUPER_NAME','ROW_FIELD','','Parent Role/Resp','4','1','xlNormal','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Oracle User Access Review - WC',85000,'Pivot','USER_NAME','ROW_FIELD','','NT ID','2','1','xlNormal','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Oracle User Access Review - WC',85000,'Pivot','IS_BUYER','ROW_FIELD','','','3','','','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Oracle User Access Review - WC',85000,'Pivot','FULL_NAME','ROW_FIELD','','Full Name','1','1','xlNormal','');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
--Inserting Report Distribution Details 
--Inserting Report Distribution Details 
--Inserting Report SubTotals Details 
--Inserting Report aggregations 
--Inserting Report   Version details- Oracle User Access Review - WC
xxeis.eis_rsc_ins.rv( 'Oracle User Access Review - WC','','Oracle User Access Review - WC','SA059956','08-AUG-2018');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 85000');
END IF;
END;
/
