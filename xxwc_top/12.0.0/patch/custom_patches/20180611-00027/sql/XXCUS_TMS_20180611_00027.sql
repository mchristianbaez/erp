/*
 TMS:  20180611-00027
 Date: 06/11/2018
 Notes:  Prod Issue: Location ON061 is attached to a wrong property ON060 instead of ON061.
*/
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
 --
 n_loc number :=0;
 l_sql varchar2(2000) :=Null;
 l_property_id Number :=0;
 l_count number :=0;
 --
BEGIN --Main Processing...
   --
   n_loc :=101;
   --
    select distinct property_id into l_property_id from apps.pn_locations_all where location_code ='ON061' ;
   --
   dbms_output.put_line
   (
     'Current Property ID: '||l_property_id
   );       
   --
   n_loc :=102;
   --
	update apps.pn_locations_all
	set property_id =192552
	where 1 =1
	and location_id =498073
	and location_code ='ON061'
   ;
   --
   n_loc :=103;
   --   
   dbms_output.put_line
   (
     case when sql%rowcount >0 then 'Location ON061 is now attached to property ON061.'
     else 'Cannot update location ON061 with property ON061.'
     end 
   );
   --
    select distinct property_id into l_property_id from apps.pn_locations_all where location_code ='ON061' ;
   --
   dbms_output.put_line
   (
     'New Property ID: '||l_property_id
   );       
   --   
   commit;
   --
EXCEPTION
   WHEN OTHERS THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20180611-00027, Errors =' || SQLERRM);
END;
/