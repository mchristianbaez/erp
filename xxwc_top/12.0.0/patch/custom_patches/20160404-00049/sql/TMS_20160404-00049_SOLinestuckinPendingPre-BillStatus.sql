/*************************************************************************
  $Header TMS_20160404-00049.sql $
  Module Name: TMS_20160404-00049  Sales Order Stuck in Pending Pre-Bill Status

  PURPOSE: Data fix to progress the line and close.

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        04-APR-2016  Anitha Mani         TMS#20160404-00049 

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000;
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20160404-00049    , Before Update');

   UPDATE apps.oe_order_lines_all
            SET 
            shipping_quantity = ordered_quantity,
                shipped_quantity = ordered_quantity,
                actual_shipment_date = TO_DATE('23-OCT-2015 12:23:57','DD-MON-YYYY HH24:MI:SS'),
                shipping_quantity_uom = order_quantity_uom,
                fulfilled_flag = 'Y',
                fulfillment_date = TO_DATE('23-OCT-2015 12:23:57','DD-MON-YYYY HH24:MI:SS'),
                fulfilled_quantity = ordered_quantity,
                --invoice_interface_status_code = 'YES',
               -- invoiced_quantity = ordered_quantity,
                --open_flag = 'N',
                --flow_status_code = 'CLOSED',
                last_updated_by = -1,
                last_update_date = SYSDATE
--                ACCEPTED_BY = 4716,
--                REVREC_SIGNATURE = 'TODD MYERS',
--                REVREC_SIGNATURE_DATE = '07-FEB-2013'
          WHERE line_id IN(56945754, 56945789)
          and headeR_id=34691020;


   DBMS_OUTPUT.put_line (
         'TMS: 20160404-00049  Lines Updated and number of records (Expected:51): '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20160404-00049    , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20160404-00049 , Errors : ' || SQLERRM);
END;
/