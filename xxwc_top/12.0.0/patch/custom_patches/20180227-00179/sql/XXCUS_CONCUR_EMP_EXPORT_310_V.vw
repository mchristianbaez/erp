--//============================================================================
--//
--// Object Name         :: XXCUS_CONCUR_EMP_EXPORT_310_V
--//
--// Object Type         :: View
--//
--// Object Description  :: This view shows all the Contractors in HRMS application.
--//
--// Version Control
--//============================================================================
--// Vers    Author             Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Ashwin Sridhar     08/06/2018    Initial Build - Task ID: 20180227-00179-Adding Contractors to Concur Employee Feed
--//============================================================================
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "APPS"."XXCUS_CONCUR_EMP_EXPORT_310_V" ("TRX_TYPE", "EMPLOYEE_ID", "LOGIN_ID", "FIRST_NAME", "MIDDLE_NAME", "LAST_NAME", "EMAIL_ADDRESS", "PASSWD", "LOCALE_CODE", "EXPENSE_USER", "EXPENSE_APPROVER", "INVOICE_USER", "INVOICE_APPROVER", "TRAVEL_USER", "ACTIVE", "NO_MIDDLE_NAME", "LOCATE_AND_ALERT", "EXPENSEIT_USER_ROLE", "FUTURE_USE_5", "FUTURE_USE_6", "FUTURE_USE_7", "FUTURE_USE_8", "FUTURE_USE_9", "FUTURE_USE_10") AS 
  select '310' trx_type
           ,to_char(to_number(regexp_replace(concur_temp.ntid, '[^0-9]+' ))) employee_id
           ,concur_temp.email_addr    login_id
           ,concur_temp.first_name  first_name 
           ,trim(concur_temp.middle_name) middle_name
           ,concur_temp.last_name  last_name
           ,concur_temp.email_addr email_address
           ,to_char(null) passwd
           ,control_table.locle_code locale_code
           ,'N' expense_user
           ,'N' expense_approver
           ,'N'  invoice_user
           ,'N'  invoice_approver
           ,'Y' Travel_user -- Always Y for contractors 
           ,case
               -- The number of days used here is based on the number of days contractors are retained in the view hdscmmn.hr_ad_all_vw after a termination date is set. 
               when (
                                ( 
                                    ( trunc(termination_dt) + 14 ) >= trunc(sysdate) and control_table.business_unit <> 'WW1US'
                                ) 
                                   or user_acct_status_code = 'A'
                          ) then 'Y'
               else 'N'
            end Active  
        ,case
           when trim(concur_temp.middle_name) is null then 'Y'
           else 'N'
         end no_middle_name
        ,'Not enrolled'   locate_and_alert
        ,'N'  expenseIT_user_role
        ,to_char(null) future_use_5
        ,to_char(null) future_use_6
        ,to_char(null) future_use_7
        ,to_char(null) future_use_8
        ,to_char(null) future_use_9
        ,to_char(null) future_use_10    
from   hdscmmn.hr_ad_all_vw@eaapxprd.hsi.hughessupply.com           concur_temp 
           ,hdscmmn.concur_bu_xref@eaapxprd.hsi.hughessupply.com      control_table
where 1 =1
and usertype ='Contractor'
and email_addr is not null
and to_number(regexp_replace(concur_temp.ntid, '[^0-9]+' )) is not null
and to_number(regexp_replace(concur_temp.spvr_ntid, '[^0-9]+' )) is not null
and ( to_number(regexp_replace(concur_temp.spvr_ntid, '[^0-9]+' )) is not null AND to_number(regexp_replace(concur_temp.spvr_emplid, '[^0-9]+' )) is not null )               
          and (
                                ( 
                                    ( trunc(concur_temp.termination_dt) + 14 ) >= trunc(sysdate) and control_table.business_unit <> 'WW1US')
                                   or 
                                   (concur_temp.user_acct_status_code = 'A'  and control_table.business_unit <> 'WW1US')                                
                  )
and control_table.business_unit(+) =concur_temp.ps_business_unit
order by ntid desc;

COMMENT ON TABLE "APPS"."XXCUS_CONCUR_EMP_EXPORT_310_V"  IS 'TMS: 20180227-00179';