CREATE OR REPLACE PACKAGE APPS.XXCUS_CONCUR_EMP_PKG AS
/**************************************************************************
   $Header XXCUS_CONCUR_EMP_PKG $
   Module Name: XXCUS_CONCUR_EMP_PKG.pks

   PURPOSE:   This package is called by the concurrent programs
              HDS CONCUR: Generate Concur Employee Export File
              for generating Employee pipe separated file.
   REVISIONS:
   Ver        Date        Author             Description
   ---------  ----------  ---------------   -------------------------
    1.0       08/05/2018  Ashwin Sridhar    Initial Build - Task ID: 20131016-00419
/*************************************************************************/

PROCEDURE USER_EXPORT(p_errbuf  OUT VARCHAR2
                     ,p_retcode OUT VARCHAR2);

END XXCUS_CONCUR_EMP_PKG;
/