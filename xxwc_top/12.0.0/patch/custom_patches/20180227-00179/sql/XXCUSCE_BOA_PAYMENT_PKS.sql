create or replace PACKAGE APPS.XXCUSCE_BOA_PAYMENT_PKG IS

  /*******************************************************************************
  * Procedure:   UC4_BANK_STMT
  * Description: This is for UC4 to start the concurrent request to submit
  *              Bank Statement Loader for BOA Payment Recon
  *
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)              DESCRIPTION
  ------- -----------   ---------------        ----------------------------------
  1.0     06/12/2018    Vamshi Singirikonda    Initial creation (TMS 20180227-00179)
  ********************************************************************************/

  --Procedure to generate reconcilation file in desired format
  PROCEDURE generate_file(p_errbuf    OUT VARCHAR2
                         ,p_retcode   OUT NUMBER
                         ,p_data_file IN VARCHAR2);

  PROCEDURE uc4_bank_stmt1(p_errbuf              OUT VARCHAR2
                          ,p_retcode             OUT NUMBER
                          ,p_data_file           IN VARCHAR2
                          ,p_user_name           IN VARCHAR2
                          ,p_responsibility_name IN VARCHAR2);

  PROCEDURE uc4_bank_stmt(p_errbuf              OUT VARCHAR2
                         ,p_retcode             OUT NUMBER
                         ,p_data_file           IN VARCHAR2
                         ,p_user_name           IN VARCHAR2
                         ,p_responsibility_name IN VARCHAR2);

END XXCUSCE_BOA_PAYMENT_PKG;
/
Grant EXECUTE on APPS.XXCUSCE_BOA_PAYMENT_PKG to interface_xxcus ;
/
Grant DEBUG on APPS.XXCUSCE_BOA_PAYMENT_PKG to interface_xxcus ;
/