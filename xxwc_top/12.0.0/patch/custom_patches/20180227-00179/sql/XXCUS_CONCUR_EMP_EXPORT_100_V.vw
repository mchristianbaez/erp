--//============================================================================
--//
--// Object Name         :: XXCUS_CONCUR_EMP_EXPORT_100_V
--//
--// Object Type         :: View
--//
--// Object Description  :: This view shows the header info for Employee Extract.
--//
--// Version Control
--//============================================================================
--// Vers    Author             Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Ashwin Sridhar     08/06/2018    Initial Build - Task ID: 20180227-00179--Adding Contractors to Concur Employee Feed
--//============================================================================
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "APPS"."XXCUS_CONCUR_EMP_EXPORT_100_V" ("HEADER") AS 
  select '100|0|SSO|UPDATE|EN|N|N' header
from dual;

   COMMENT ON TABLE "APPS"."XXCUS_CONCUR_EMP_EXPORT_100_V"  IS 'TMS: 20180227-00179';