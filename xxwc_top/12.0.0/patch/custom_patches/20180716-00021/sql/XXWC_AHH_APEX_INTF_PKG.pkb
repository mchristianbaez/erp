CREATE OR REPLACE PACKAGE BODY APPS.XXWC_AHH_APEX_INTF_PKG
IS
   /******************************************************************************************************************************************************
        $Header XXWC_AHH_APEX_INTF_PKG $
        Module Name: XXWC_AHH_APEX_INTF_PKG.pkb

        PURPOSE:   AHH Customer Conversion

        REVISIONS:
        Ver        Date          Author           Description
        ---------  ----------   ---------------  ------------------------------------------------------------------------------------------------
        1.0        06/06/2018   Nancy Pahwa       AH HARRIS Interface TMS 20180716-00021
   ******************************************************************************************************************************************************/
   g_err_callfrom          VARCHAR2 (1000) := 'XXWC_AHH_CUSTOMER_INTF_PKG';
   g_module                VARCHAR2 (100) := 'XXWC';
   g_distro_list           VARCHAR2 (80) := 'wc-itdevalerts-u1@hdsupply.com';
   gvc_debug_enabled       VARCHAR2 (1) := 'Y';
   gn_org_id               NUMBER := '162';
   g_currency_code         VARCHAR2 (30);
   g_country_code          VARCHAR2 (30);
   g_api_name              VARCHAR2 (30) := 'ONT_UI_ADD_CUSTOMER';
   g_msg_data              VARCHAR2 (32767);

   g_msg_index_out         VARCHAR2 (4000);
   g_acct_status           VARCHAR2 (100);
   g_cust_acct_no          HZ_CUST_ACCOUNTS.ACCOUNT_NUMBER%TYPE;
   g_bill_to_site_use_id   NUMBER;
   g_primbillto_exists     VARCHAR2 (1);
   g_term_id               NUMBER;
   g_user_id        FND_USER.USER_ID%TYPE;
     PROCEDURE write_log (p_log_msg VARCHAR2)
   /**********************************************************************************************************
     $Header write_log $
     PURPOSE:     Procedure to write Log messages
     REVISIONS:
        Ver        Date          Author           Description
        ---------  ----------   ---------------  ------------------------------------------------------------------------------------------------
        1.0        06/06/2018   Nancy Pahwa       AH HARRIS Interface TMS 20180716-00021
     **********************************************************************************************************/
   IS
      l_start     NUMBER;
      l_log_msg   VARCHAR2 (4000);
   BEGIN
      IF p_log_msg = 'g_line_open' OR p_log_msg = 'g_line_close'
      THEN
         l_log_msg :=
            '/**************************************************************************/';
      ELSE
         l_log_msg :=
            p_log_msg || ' : ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS');
         xxwc_common_tunning_helpers.elapsed_time (
            'CUSTOMER_CATALOG_PKG',
            SUBSTR (p_log_msg, 1, 500),
            l_start);
      END IF;

      fnd_file.put_line (fnd_file.LOG, l_log_msg);
   END write_log;
   PROCEDURE return_msg (in_msg_count     IN     NUMBER,
                         in_msg_data      IN     VARCHAR2,
                         out_return_msg      OUT VARCHAR2)
   IS
   -- ==============================================================================================================================
   -- Procedure: return_msg
   -- Purpose: Return Message procedure
   -- ==============================================================================================================================
   ---    REVISIONS:
   --     Ver        Date          Author           Description
        ---------  ----------   ---------------  ------------------------------------------------------------------------------------------------
   --     1.0        06/06/2018   Nancy Pahwa       AH HARRIS Interface TMS 20180716-00021
   -- ==============================================================================================================================
   BEGIN
      IF in_msg_count > 1
      THEN
         FOR I IN 1 .. in_msg_count
         LOOP
            Oe_Msg_Pub.get (p_msg_index       => i,
                            p_encoded         => Fnd_Api.G_FALSE,
                            p_data            => g_msg_data,
                            p_msg_index_out   => g_msg_index_out);
            out_return_msg :=
               out_return_msg || ':' || SUBSTR (g_msg_data, 1, 200);
         END LOOP;
      ELSE
         out_return_msg := in_msg_data;
      END IF;
   END;
   PROCEDURE AR_INVOICE_INTERFACE(p_user_NAME    IN VARCHAR2,
                               P_RESP_ID      IN NUMBER,
                               P_RESP_APPL_ID IN NUMBER,
                                x_ret_msg OUT VARCHAR2)
  /*********************************************************************************************************
       $Header AR_INVOICE_INTERFACE $
       PURPOSE:     Procedure to trigger AR_CUSTOMER_INTERFACE update process from Apex.
        REVISIONS:
        Ver        Date          Author           Description
        ---------  ----------   ---------------  ------------------------------------------------------------------------------------------------
        1.0        06/06/2018   Nancy Pahwa       AH HARRIS Interface TMS 20180716-00021
    ---------------------------------------------------------------------------------*/
   IS
    l_request_id NUMBER;
    l_err_mess   VARCHAR2(4000);
  BEGIN
    BEGIN
      SELECT user_id
        INTO g_user_id
        FROM APPS.FND_USER
       WHERE USER_NAME = P_USER_NAME;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        l_err_mess := P_USER_NAME || ' Not Exist';
       write_log(l_err_mess);
    END;

    write_log('Initializing Variables ');

    APPS.MO_GLOBAL.SET_POLICY_CONTEXT('S', 162);
    APPS.fnd_global.apps_initialize(g_user_id, p_resp_id, p_resp_appl_id);

    l_request_id := fnd_request.submit_request(application => 'XXWC',
                                               program     => 'XXWC_PRISM2EBS_AR_INV_INTF',
                                               description => NULL,
                                               start_time  => SYSDATE,
                                               sub_request => FALSE,
                                               argument1   => 'Yes');
         x_ret_msg := 'Submitted Request ' || l_request_id;

    IF l_request_id > 0 THEN
      COMMIT;
    END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         x_ret_msg :=
               'Error occurred please contact IT support team: '
            || SUBSTR (SQLERRM, 1, 250);
   END;
  PROCEDURE AR_LOCKBOX_INTERFACE(p_user_NAME    IN VARCHAR2,
                               P_RESP_ID      IN NUMBER,
                               P_RESP_APPL_ID IN NUMBER,
                                x_ret_msg OUT VARCHAR2)
  /*********************************************************************************************************
       $Header AR_LOCKBOX_INTERFACE $
       PURPOSE:     Procedure to trigger AR_LOCKBOX_INTERFACE update process from Apex.
       REVISIONS:
        Ver        Date          Author           Description
        ---------  ----------   ---------------  ------------------------------------------------------------------------------------------------
        1.0        06/06/2018   Nancy Pahwa       AH HARRIS Interface TMS 20180716-00021
    ---------------------------------------------------------------------------------*/
   IS
    l_request_id NUMBER;
    l_err_mess   VARCHAR2(4000);
  BEGIN
    BEGIN
      SELECT user_id
        INTO g_user_id
        FROM APPS.FND_USER
       WHERE USER_NAME = P_USER_NAME;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        l_err_mess := P_USER_NAME || ' Not Exist';
       write_log(l_err_mess);
    END;

    write_log('Initializing Variables ');

    APPS.MO_GLOBAL.SET_POLICY_CONTEXT('S', 162);
    APPS.fnd_global.apps_initialize(g_user_id, p_resp_id, p_resp_appl_id);

    l_request_id := fnd_request.submit_request(application => 'XXWC',
                                               program     => 'XXWC_AR_AHH_LOCKBOX_PROCESS',
                                               description => NULL,
                                               start_time  => SYSDATE,
                                               sub_request => FALSE);
   x_ret_msg := 'Submitted Request ' || l_request_id;
    IF l_request_id > 0 THEN
      COMMIT;
    END IF;
    EXCEPTION
      WHEN OTHERS
      THEN
         x_ret_msg :=
               'Error occurred please contact IT support team: '
            || SUBSTR (SQLERRM, 1, 250);
   END;
  PROCEDURE AP_INVOICE_INTERFACE(p_user_NAME    IN VARCHAR2,
                               P_RESP_ID      IN NUMBER,
                               P_RESP_APPL_ID IN NUMBER,
                                x_ret_msg OUT VARCHAR2)
  /*********************************************************************************************************
       $Header AP_INVOICE_INTERFACE $
       PURPOSE:     Procedure to trigger AP_INVOICE_INTERFACE update process from Apex.
        REVISIONS:
        Ver        Date          Author           Description
        ---------  ----------   ---------------  ------------------------------------------------------------------------------------------------
        1.0        06/06/2018   Nancy Pahwa       AH HARRIS Interface TMS 20180716-00021
    ---------------------------------------------------------------------------------*/
   IS
    l_request_id NUMBER;
    l_err_mess   VARCHAR2(4000);
  BEGIN
    BEGIN
      SELECT user_id
        INTO g_user_id
        FROM APPS.FND_USER
       WHERE USER_NAME = P_USER_NAME;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        l_err_mess := P_USER_NAME || ' Not Exist';
       write_log(l_err_mess);
    END;

    write_log('Initializing Variables ');

    APPS.MO_GLOBAL.SET_POLICY_CONTEXT('S', 162);
    APPS.fnd_global.apps_initialize(g_user_id, p_resp_id, p_resp_appl_id);

    l_request_id := fnd_request.submit_request(application => 'XXWC',
                                               program     => 'XXWC_PRISM2EBS_AP_INV_INTF',
                                               description => NULL,
                                               start_time  => SYSDATE,
                                               sub_request => FALSE,
                                               argument1   => 'Yes');
   x_ret_msg := 'Submitted Request ' || l_request_id;
    IF l_request_id > 0 THEN
      COMMIT;
    END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         x_ret_msg :=
               'Error occurred please contact IT support team: '
            || SUBSTR (SQLERRM, 1, 250);
   END;
   PROCEDURE AR_DEBIT_MEMO_INTERFACE(p_user_NAME    IN VARCHAR2,
                               P_RESP_ID      IN NUMBER,
                               P_RESP_APPL_ID IN NUMBER,
                                x_ret_msg OUT VARCHAR2)
  /*********************************************************************************************************
       $Header AR_DEBIT_MEMO_INTERFACE $
       PURPOSE:     Procedure to trigger AR_DEBIT_MEMO_INTERFACE update process from Apex.
       REVISIONS:
        Ver        Date          Author           Description
        ---------  ----------   ---------------  ------------------------------------------------------------------------------------------------
        1.0        06/06/2018   Nancy Pahwa       AH HARRIS Interface TMS 20180716-00021
    ---------------------------------------------------------------------------------*/
   IS
    l_request_id NUMBER;
    l_err_mess   VARCHAR2(4000);
  BEGIN
    BEGIN
      SELECT user_id
        INTO g_user_id
        FROM APPS.FND_USER
       WHERE USER_NAME = P_USER_NAME;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        l_err_mess := P_USER_NAME || ' Not Exist';
       write_log(l_err_mess);
    END;

    write_log('Initializing Variables ');

    APPS.MO_GLOBAL.SET_POLICY_CONTEXT('S', 162);
    APPS.fnd_global.apps_initialize(g_user_id, p_resp_id, p_resp_appl_id);

    l_request_id := fnd_request.submit_request(application => 'XXWC',
                                               program     => 'XXWC_PRISM2EBS_AR_DM_INTF',
                                               description => NULL,
                                               start_time  => SYSDATE,
                                               sub_request => FALSE,
                                               argument1   => 'Yes');
   x_ret_msg := 'Submitted Request ' || l_request_id;
    IF l_request_id > 0 THEN
      COMMIT;
    END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         x_ret_msg :=
               'Error occurred please contact IT support team: '
            || SUBSTR (SQLERRM, 1, 250);
   END;
END XXWC_AHH_APEX_INTF_PKG;
/