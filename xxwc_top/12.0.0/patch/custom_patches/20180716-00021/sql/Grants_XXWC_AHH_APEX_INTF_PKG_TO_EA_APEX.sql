/******************************************************************************************************************************************************
        $Header XXWC_AHH_APEX_INTF_PKG $
        Module Name: grants to XXWC_AHH_APEX_INTF_PKG.pkb

        PURPOSE:   AHH Customer Conversion

        REVISIONS:
        Ver        Date          Author           Description
        ---------  ----------   ---------------  ------------------------------------------------------------------------------------------------
        1.0        06/06/2018   Nancy Pahwa       AH HARRIS Interface TMS 20180716-00021
   *****************************************************************************************************************************************************/
grant execute on APPS.XXWC_AHH_APEX_INTF_PKG to ea_apex;
grant execute on APPS.XXWC_AHH_CUSTOMER_INTF_PKG to ea_apex;