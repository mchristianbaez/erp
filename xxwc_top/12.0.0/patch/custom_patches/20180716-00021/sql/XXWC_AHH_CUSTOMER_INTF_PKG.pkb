CREATE OR REPLACE PACKAGE BODY APPS.XXWC_AHH_CUSTOMER_INTF_PKG
IS
   /******************************************************************************************************************************************************
        $Header XXWC_AHH_CUSTOMER_INTF_PKG $
        Module Name: XXWC_AHH_CUSTOMER_INTF_PKG.pkb

        PURPOSE:   AHH Customer Conversion

        REVISIONS:
        Ver        Date          Author           Description
        ---------  ----------   ---------------  ------------------------------------------------------------------------------------------------
        1.0        06-Jun-2018   P.Vamshidhar    AH HARRIS Customer Interface
        1.1        16-Jul-2018    P.Vamshidhar   TMS#20180716-00025 - AHH Customers Interface
        1.2        16-Jul-2018  P.Vamshidhar     TMS#20180716-00021 - Apex Page changes AHH Interfaces
   ******************************************************************************************************************************************************/
   g_err_callfrom          VARCHAR2 (1000) := 'XXWC_AHH_CUSTOMER_INTF_PKG';
   g_module                VARCHAR2 (100) := 'XXWC';
   g_distro_list           VARCHAR2 (80) := 'wc-itdevalerts-u1@hdsupply.com';
   gvc_debug_enabled       VARCHAR2 (1) := 'Y';
   gn_org_id               NUMBER := '162';
   g_currency_code         VARCHAR2 (30);
   g_country_code          VARCHAR2 (30);
   g_api_name              VARCHAR2 (30) := 'ONT_UI_ADD_CUSTOMER';
   g_msg_data              VARCHAR2 (32767);

   g_msg_index_out         VARCHAR2 (4000);
   g_acct_status           VARCHAR2 (100);
   g_cust_acct_no          HZ_CUST_ACCOUNTS.ACCOUNT_NUMBER%TYPE;
   g_bill_to_site_use_id   NUMBER;
   g_primbillto_exists     VARCHAR2 (1);
   g_term_id               NUMBER;
   g_user_id               FND_USER.USER_ID%TYPE;

   PROCEDURE return_msg (in_msg_count     IN     NUMBER,
                         in_msg_data      IN     VARCHAR2,
                         out_return_msg      OUT VARCHAR2)
   IS
   -- ==============================================================================================================================
   -- Procedure: return_msg
   -- Purpose: Return Message procedure
   -- ==============================================================================================================================
   --  REVISIONS:
   --   Ver        Date        Author               Description
   --  ---------  ---------- ---------------  --------------------------------------------------------------------------------------
   --  1.0        06/06/2018  P.Vamshidhar     TMS#20180308-00290 -AH HARRIS Customer Conversion
   -- ==============================================================================================================================
   BEGIN
      IF in_msg_count > 1
      THEN
         FOR I IN 1 .. in_msg_count
         LOOP
            Oe_Msg_Pub.get (p_msg_index       => i,
                            p_encoded         => Fnd_Api.G_FALSE,
                            p_data            => g_msg_data,
                            p_msg_index_out   => g_msg_index_out);
            out_return_msg :=
               out_return_msg || ':' || SUBSTR (g_msg_data, 1, 200);
         END LOOP;
      ELSE
         out_return_msg := in_msg_data;
      END IF;
   END;

   PROCEDURE debug (P_RECORD_ID IN NUMBER, p_msg IN VARCHAR2)
   IS
      -- ==============================================================================================================================
      -- Procedure: debug
      -- Purpose: Debugging procedure
      -- ==============================================================================================================================
      --  REVISIONS:
      --   Ver        Date        Author               Description
      --  ---------  ---------- ---------------  --------------------------------------------------------------------------------------
      --  1.0        06/06/2018  P.Vamshidhar     TMS#20180308-00290 -AH HARRIS Customer Conversion
      -- ==============================================================================================================================

      PRAGMA AUTONOMOUS_TRANSACTION;
      l_sec           VARCHAR2 (1000);
      lvc_procedure   VARCHAR2 (100) := 'DEBUG';
   BEGIN
      IF gvc_debug_enabled = 'Y'
      THEN
         l_sec := ' Inserting Data into Log Table';

         INSERT
           INTO XXWC.XXWC_AR_EQUIFAX_LOG_TBL (LOG_SEQ_NO, RECORD_ID, LOG_MSG)
         VALUES (XXWC.XXWC_AR_EQUIFAX_LOG_SEQ.NEXTVAL, P_RECORD_ID, P_MSG);

         COMMIT;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || lvc_procedure,
            p_calling             => l_sec,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          =>    'Error Occured for '
                                     || p_record_id
                                     || 'record id in XXWC_AR_EQUIFAX_LOG_TBL.',
            p_distribution_list   => g_distro_list,
            p_module              => g_module);
   END;


   PROCEDURE main (x_errbuf          OUT VARCHAR2,
                   x_retcode         OUT VARCHAR2,
                   p_validation   IN     VARCHAR2)
   IS
      -- ==============================================================================================================================
      -- Procedure: Main
      -- Purpose: Main procedure
      -- ==============================================================================================================================
      --  REVISIONS:
      --   Ver        Date        Author               Description
      --  ---------  ---------- ---------------  --------------------------------------------------------------------------------------
      --  1.0        06/06/2018  P.Vamshidhar    AH HARRIS Customer Interface
      --  1.1        07/16/2018  P.Vamshidhar    TMS#20180716-00025 - AHH Customers Interface
      -- ==============================================================================================================================
      CURSOR CUR_MAIN
      IS
         SELECT XACA.ORIG_SYSTEM_REFERENCE CUSTOMER_NUMBER,
                NVL (XACA.ACCOUNT_STATUS, 'ACTIVE') ACCOUNT_STATUS,
                xaca.STANDARD_TERM_NAME
           FROM XXWC.XXWC_AR_CUSTOMER_IFACE_STG XACA
          WHERE XACA.PROCESS_STATUS = 'V';

      l_sec                 VARCHAR2 (32767);
      ln_cust_account_id    HZ_CUST_ACCOUNTS.CUST_ACCOUNT_ID%TYPE;
      ln_party_id           HZ_PARTIES.PARTY_ID%TYPE;
      lvc_cust_exist_flag   VARCHAR2 (1);
      ln_count              NUMBER;
   BEGIN
      mo_global.init ('AR');
      mo_global.set_org_context (162, NULL, 'AR');
      fnd_global.set_nls_context ('AMERICAN');



      IF p_validation = 'Y'
      THEN
         preprocess;
         validate_inbound_data;
      ELSE
         --====================================================================
         -- Fetching the Currency code
         --====================================================================

         l_sec := 'Deriving Currency code, country code';

         BEGIN
            SELECT gsob.currency_code, SUBSTR (gsob.currency_code, 1, 2)
              INTO g_currency_code, g_country_code
              FROM hr_operating_units hou, gl_sets_of_books gsob
             WHERE     1 = 1
                   AND gsob.set_of_books_id = hou.set_of_books_id
                   AND hou.organization_id = gn_org_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               g_currency_code := 'USD';
               g_country_code := 'US';
         END;

         debug (NULL,
                l_sec || ' ' || g_currency_code || ' ' || g_country_code);

         l_sec := 'Main Looping Starting';

         FOR rec_main IN cur_main
         LOOP
            l_sec := 'Customer Number: ' || rec_main.customer_number;
            lvc_cust_exist_flag := 'N';
            g_primbillto_exists := NULL;

            ln_party_id := NULL;
            ln_cust_account_id := NULL;

            BEGIN
               SELECT hca.CUST_ACCOUNT_ID, hca.PARTY_ID
                 INTO ln_cust_account_id, ln_party_id
                 FROM HZ_CUST_ACCOUNTS HCA,
                      XXWC.XXWC_AHH_AR_CUST_CROSS_OVER_T XACC
                WHERE     REPLACE (REPLACE (XACC.CUST_NUM, CHR (13), ''),
                                   CHR (10),
                                   '') = rec_main.CUSTOMER_NUMBER
                      AND REPLACE (
                             REPLACE (XACC.ORACLE_CUST_NUM, CHR (13), ''),
                             CHR (10),
                             '') = HCA.ACCOUNT_NUMBER
                      AND ROWNUM = 1;
            EXCEPTION
               WHEN OTHERS
               THEN
                  BEGIN
                     SELECT hca.CUST_ACCOUNT_ID, hca.PARTY_ID
                       INTO ln_cust_account_id, ln_party_id
                       FROM apps.hz_cust_acct_sites_all hcas,
                            apps.hz_cust_accounts hca
                      WHERE     hcas.cust_account_id = hca.cust_account_id
                            --  AND hca.attribute4 = 'AHH' -- Rev 1.1
                            AND NVL (hcas.attribute17, '123') =
                                   rec_main.CUSTOMER_NUMBER
                            --AND hcas.bill_to_flag = 'P' -- Rev 1.1
                            AND hca.status = 'A'
                            AND ROWNUM = 1;                         -- Rev 1.1
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        ln_cust_account_id := NULL;
                        ln_party_id := NULL;
                        lvc_cust_exist_flag := 'N';
                  END;
            END;

            l_sec := 'Customers Data fetched ' || ln_cust_account_id;

            IF ln_cust_account_id IS NOT NULL
            THEN
               lvc_cust_exist_flag := 'Y';
            END IF;

            l_sec := 'Customer Number: ' || rec_main.customer_number;

            g_acct_status := NULL;
            g_acct_status := rec_main.account_status;

            l_sec :=
                  'Customer Number (customer status): '
               || rec_main.customer_number
               || ' '
               || g_acct_status;

            BEGIN
               SELECT A.TERM_ID
                 INTO g_term_id
                 FROM RA_TERMS A, XXWC.XXWC_AHH_PAYMENT_TERMS_T B
                WHERE     UPPER (TRIM (B.TERM_NAME)) = UPPER (A.NAME)
                      AND UPPER (TRIM (B.AHH_TERM_NAME)) =
                             rec_main.STANDARD_TERM_NAME
                      AND ROWNUM = 1;
            EXCEPTION
               WHEN OTHERS
               THEN
                  BEGIN
                     SELECT TERM_ID
                       INTO g_term_id
                       FROM RA_TERMS
                      WHERE name = 'N30D';
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        g_term_id := NULL;
                  END;
            END;

            IF lvc_cust_exist_flag = 'N'
            THEN
               l_sec := 'Create Customer: ' || rec_main.customer_number;
               create_customer (rec_main.customer_number,
                                ln_cust_account_id,
                                ln_party_id);

               l_sec :=
                     'Create Customer bill to: '
                  || rec_main.customer_number
                  || ' ln_cust_account_id:'
                  || ln_cust_account_id
                  || ' ln_party_id:'
                  || ln_party_id;

               IF NVL (ln_cust_account_id, 0) > 0
               THEN
                  FND_FILE.PUT_LINE (FND_FILE.LOG, l_sec);
                  l_sec :=
                        'Create Customer bill to before: '
                     || rec_main.customer_number;
                  primary_bill_to (rec_main.customer_number,
                                   ln_cust_account_id,
                                   ln_party_id);

                  l_sec :=
                        'Create Customer bill to after: '
                     || rec_main.customer_number;

                  IF g_primbillto_exists = 'Y'
                  THEN
                     FND_FILE.PUT_LINE (FND_FILE.LOG, l_sec);
                     l_sec :=
                           'Create Customer Primary ship to: '
                        || rec_main.customer_number;
                     primary_ship_to (rec_main.customer_number,
                                      ln_cust_account_id,
                                      ln_party_id);
                     l_sec :=
                           'Create Customer Primary ship to after:'
                        || rec_main.customer_number;

                     FND_FILE.PUT_LINE (FND_FILE.LOG, l_sec);

                     multiple_shipto_sites (rec_main.customer_number,
                                            ln_cust_account_id,
                                            ln_party_id);
                     l_sec :=
                           'Create customer multiple ship to after:'
                        || rec_main.customer_number;
                  ELSE
                     UPDATE XXWC.XXWC_AR_CUSTOMER_IFACE_STG
                        SET PROCESS_ERROR = 'Primary Bill To Creation failed',
                            PROCESS_STATUS = 'E'
                      WHERE     ORIG_SYSTEM_REFERENCE =
                                   rec_main.customer_number
                            AND PROCESS_ERROR IS NULL;

                     l_sec := 'Update flag:' || rec_main.customer_number;
                  END IF;
               ELSE
                  UPDATE XXWC.XXWC_AR_CUSTOMER_IFACE_STG
                     SET PROCESS_ERROR = 'Customer Account creation Failure.',
                         PROCESS_STATUS = 'E'
                   WHERE     ORIG_SYSTEM_REFERENCE = rec_main.customer_number
                         AND PROCESS_ERROR IS NULL;

                  l_sec := 'Update flag1:' || rec_main.customer_number;
               END IF;
            ELSE
               multiple_shipto_sites (rec_main.customer_number,
                                      ln_cust_account_id,
                                      ln_party_id);
               l_sec :=
                     'multiple_shipto_sites (cross over):'
                  || rec_main.customer_number;

               BEGIN
                  ln_count := 0;

                  SELECT COUNT (1)
                    INTO ln_count
                    FROM XXWC.XXWC_AR_CUSTOMER_IFACE_STG
                   WHERE     ORIG_SYSTEM_REFERENCE = rec_main.customer_number
                         AND process_status = 'E';
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     ln_count := 0;
               END;

               IF ln_count = 0
               THEN
                  UPDATE XXWC.XXWC_AR_CUSTOMER_IFACE_STG xaca1
                     SET xaca1.PROCESS_STATUS = 'Y'
                   WHERE     ORIG_SYSTEM_REFERENCE = rec_main.customer_number
                         AND process_STATUS = 'V';
               END IF;
            END IF;
         END LOOP;

         DEBUG ('', 'Before Adding Ship to sites');
         add_sites;
         DEBUG ('', 'After Adding Ship to Sites');
      END IF;

      BEGIN
         UPDATE APPS.HZ_CUST_PROFILE_AMTS
            SET OVERALL_CREDIT_LIMIT = NULL
          WHERE     OVERALL_CREDIT_LIMIT = 0
                AND SITE_USE_ID IS NOT NULL
                AND REQUEST_ID = fnd_global.conc_request_id;

         COMMIT;
      EXCEPTION
         WHEN OTHERS
         THEN
            fnd_file.put_line (fnd_file.LOG,
                               'erroring Profile amounts:' || SQLERRM);
      END;

      BEGIN
         UPDATE APPS.hz_cust_site_uses_all
            SET ATTRIBUTE12 = DECODE (ATTRIBUTE12, NULL, '-3', ATTRIBUTE12),
                ATTRIBUTE13 = DECODE (ATTRIBUTE13, NULL, '-3', ATTRIBUTE13)
          WHERE     (ATTRIBUTE12 IS NULL OR ATTRIBUTE13 IS NULL)
                AND REQUEST_ID = FND_GLOBAL.CONC_REQUEST_ID;

         COMMIT;
      EXCEPTION
         WHEN OTHERS
         THEN
            fnd_file.put_line (fnd_file.LOG,
                               'erroring attribute12/13 update:' || SQLERRM);
      END;
   EXCEPTION
      WHEN OTHERS
      THEN
         fnd_file.put_line (fnd_file.LOG, 'error occured (Main):' || l_sec);
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.MAIN',
            p_calling             => l_sec,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          => NULL,
            p_distribution_list   => g_distro_list,
            p_module              => g_module);
   END;

   PROCEDURE preprocess
   IS
   BEGIN
      BEGIN
         UPDATE XXWC.XXWC_AR_CUSTOMER_IFACE_STG
            SET account_name = organization_name
          WHERE     account_name IS NULL
                AND NVL (process_status, 'R') IN ('R', 'E');
      EXCEPTION
         WHEN OTHERS
         THEN
            NULL;
      END;

      -- Trim Spaces on Customer records
      BEGIN
         UPDATE XXWC.XXWC_AR_CUSTOMER_IFACE_STG
            SET ORIG_SYSTEM_REFERENCE = TRIM (ORIG_SYSTEM_REFERENCE),
                ORIG_SYSTEM = TRIM (ORIG_SYSTEM),
                STATUS = TRIM (STATUS),
                ATTRIBUTE_CATEGORY = TRIM (ATTRIBUTE_CATEGORY),
                ATTRIBUTE1 = TRIM (ATTRIBUTE1),
                ATTRIBUTE2 = TRIM (ATTRIBUTE2),
                ATTRIBUTE3 = TRIM (ATTRIBUTE3),
                ATTRIBUTE4 = TRIM (ATTRIBUTE4),
                ATTRIBUTE5 = TRIM (ATTRIBUTE5),
                ATTRIBUTE6 = TRIM (ATTRIBUTE6),
                ATTRIBUTE7 = TRIM (ATTRIBUTE7),
                ATTRIBUTE8 = TRIM (ATTRIBUTE8),
                ATTRIBUTE9 = TRIM (ATTRIBUTE9),
                ATTRIBUTE10 = TRIM (ATTRIBUTE10),
                ATTRIBUTE11 = TRIM (ATTRIBUTE11),
                ATTRIBUTE12 = TRIM (ATTRIBUTE12),
                ATTRIBUTE13 = TRIM (ATTRIBUTE13),
                ATTRIBUTE14 = TRIM (ATTRIBUTE14),
                ATTRIBUTE15 = TRIM (ATTRIBUTE15),
                ATTRIBUTE16 = TRIM (ATTRIBUTE16),
                ATTRIBUTE17 = TRIM (ATTRIBUTE17),
                ATTRIBUTE18 = TRIM (ATTRIBUTE18),
                ATTRIBUTE19 = TRIM (ATTRIBUTE19),
                ATTRIBUTE20 = TRIM (ATTRIBUTE20),
                ATTRIBUTE21 = TRIM (ATTRIBUTE21),
                ATTRIBUTE22 = TRIM (ATTRIBUTE22),
                ATTRIBUTE23 = TRIM (ATTRIBUTE23),
                ATTRIBUTE24 = TRIM (ATTRIBUTE24),
                ORGANIZATION_NAME = TRIM (ORGANIZATION_NAME),
                SIC_CODE_TYPE = TRIM (SIC_CODE_TYPE),
                SIC_CODE = TRIM (SIC_CODE),
                DUNS_NUMBER_C = TRIM (DUNS_NUMBER_C),
                GSA_INDICATOR_FLAG = TRIM (GSA_INDICATOR_FLAG),
                PARTY_TYPE = TRIM (PARTY_TYPE),
                DB_RATING = TRIM (DB_RATING),
                TAX_REFERENCE = TRIM (TAX_REFERENCE),
                DUNS_NUMBER = TRIM (DUNS_NUMBER),
                KNOWN_AS = TRIM (KNOWN_AS),
                EMPLOYEES_TOTAL = TRIM (EMPLOYEES_TOTAL),
                TOTAL_EMPLOYEES_IND = TRIM (TOTAL_EMPLOYEES_IND),
                TOTAL_EMP_MIN_IND = TRIM (TOTAL_EMP_MIN_IND),
                TOTAL_EMP_EST_IND = TRIM (TOTAL_EMP_EST_IND),
                CURR_FY_POTENTIAL_REVENUE = TRIM (CURR_FY_POTENTIAL_REVENUE),
                NEXT_FY_POTENTIAL_REVENUE = TRIM (NEXT_FY_POTENTIAL_REVENUE),
                YEAR_ESTABLISHED = TRIM (YEAR_ESTABLISHED),
                COMPETITOR_FLAG = TRIM (COMPETITOR_FLAG),
                CEO_NAME = TRIM (CEO_NAME),
                CEO_TITLE = TRIM (CEO_TITLE),
                PRINCIPAL_NAME = TRIM (PRINCIPAL_NAME),
                PRINCIPAL_TITLE = TRIM (PRINCIPAL_TITLE),
                MINORITY_OWNED_IND = TRIM (MINORITY_OWNED_IND),
                MINORITY_OWNED_TYPE = TRIM (MINORITY_OWNED_TYPE),
                WOMAN_OWNED_IND = TRIM (WOMAN_OWNED_IND),
                DISADV_8A_IND = TRIM (DISADV_8A_IND),
                SMALL_BUS_IND = TRIM (SMALL_BUS_IND),
                CONTROL_YR = TRIM (CONTROL_YR),
                RENT_OWN_IND = TRIM (RENT_OWN_IND),
                PARTY_NUMBER = TRIM (PARTY_NUMBER),
                PARTY_ID = TRIM (PARTY_ID),
                PROFILE_ID = TRIM (PROFILE_ID),
                HQ_BRANCH_IND = TRIM (HQ_BRANCH_IND),
                CUSTOMER_PROFILE_CLASS_NAME =
                   TRIM (CUSTOMER_PROFILE_CLASS_NAME),
                COLLECTOR_NAME = TRIM (COLLECTOR_NAME),
                CREIDT_CHECKING = TRIM (CREIDT_CHECKING),
                CREDIT_HOLD = TRIM (CREDIT_HOLD),
                CUSTOMER_TYPE = TRIM (CUSTOMER_TYPE),
                CUSTOMER_CLASS_CODE = TRIM (CUSTOMER_CLASS_CODE),
                SALES_CHANNEL_CODE = TRIM (SALES_CHANNEL_CODE),
                ACCOUNT_NAME = TRIM (ACCOUNT_NAME),
                DUNNING_LETTERS = TRIM (DUNNING_LETTERS),
                SEND_STATEMENTS = TRIM (SEND_STATEMENTS),
                CREDIT_BALANCE_STATEMENTS = TRIM (CREDIT_BALANCE_STATEMENTS),
                CREDIT_RATING = TRIM (CREDIT_RATING),
                INTEREST_PERIOD_DAYS = TRIM (INTEREST_PERIOD_DAYS),
                DISCOUNT_GRACE_DAYS = TRIM (DISCOUNT_GRACE_DAYS),
                PAYMENT_GRACE_DAYS = TRIM (PAYMENT_GRACE_DAYS),
                TOLERANCE = TRIM (TOLERANCE),
                STATEMENT_CYCLE_NAME = TRIM (STATEMENT_CYCLE_NAME),
                STANDARD_TERM_NAME = TRIM (STANDARD_TERM_NAME),
                OVERRIDE_TERMS = TRIM (OVERRIDE_TERMS),
                DUNNING_LETTER_SET_NAME = TRIM (DUNNING_LETTER_SET_NAME),
                INTEREST_CHARGES = TRIM (INTEREST_CHARGES),
                TAX_PRINTING_OPTIONS = TRIM (TAX_PRINTING_OPTIONS),
                RISK_CODE = TRIM (RISK_CODE),
                CURRENCY_CODE = TRIM (CURRENCY_CODE),
                PERCENT_COLLECTABLE = TRIM (PERCENT_COLLECTABLE),
                OVERALL_CREDIT_LIMIT = TRIM (OVERALL_CREDIT_LIMIT),
                TRX_CREDIT_LIMIT = TRIM (TRX_CREDIT_LIMIT),
                MIN_DUNNING_AMOUNT = TRIM (MIN_DUNNING_AMOUNT),
                MIN_DUNNING_INVOICE_AMPOUNT =
                   TRIM (MIN_DUNNING_INVOICE_AMPOUNT),
                MAX_INTEREST_CHARGE = TRIM (MAX_INTEREST_CHARGE),
                MIN_STATEMENT_AMOUNT = TRIM (MIN_STATEMENT_AMOUNT),
                CLEARING_DAYS = TRIM (CLEARING_DAYS),
                AUTO_REC_INCL_DISPUTED_FLAG =
                   TRIM (AUTO_REC_INCL_DISPUTED_FLAG),
                AUTO_REC_MIN_RECEIPT_AMOUNT =
                   TRIM (AUTO_REC_MIN_RECEIPT_AMOUNT),
                CHARGE_ON_FINANCE_CHARGE_FLAG =
                   TRIM (CHARGE_ON_FINANCE_CHARGE_FLAG),
                LOCK_BOX_MATCHING_OPTION = TRIM (LOCK_BOX_MATCHING_OPTION),
                AUTOCASH_HIERARCHY_NAME = TRIM (AUTOCASH_HIERARCHY_NAME),
                REVIEW_CYCLE = TRIM (REVIEW_CYCLE),
                CREDIT_CLASSIFICATION = TRIM (CREDIT_CLASSIFICATION),
                AUTOPAY_FLAG = TRIM (AUTOPAY_FLAG),
                CUST_ACCOUNT_ID = TRIM (CUST_ACCOUNT_ID),
                CUST_ACCOUNT_PROFILE_ID = TRIM (CUST_ACCOUNT_PROFILE_ID),
                CREDIT_ANALYST_NAME = TRIM (CREDIT_ANALYST_NAME),
                CUSTOMER_SOURCE = TRIM (CUSTOMER_SOURCE),
                COD_COMMENT = TRIM (COD_COMMENT),
                KEYWORD = TRIM (KEYWORD),
                PROCESS_STATUS = 'R',
                PROCESS_ERROR = NULL
          WHERE NVL (process_status, 'R') IN ('R', 'E');
      EXCEPTION
         WHEN OTHERS
         THEN
            NULL;
      END;

      BEGIN
         UPDATE XXWC.XXWC_AR_CUST_SITE_IFACE_STG
            SET ORIG_SYSTEM_REFERENCE = TRIM (ORIG_SYSTEM_REFERENCE),
                ORIG_SYSTEM = TRIM (ORIG_SYSTEM),
                ATTRIBUTE1 = TRIM (ATTRIBUTE1),
                ATTRIBUTE2 = TRIM (ATTRIBUTE2),
                ATTRIBUTE3 = TRIM (ATTRIBUTE3),
                ATTRIBUTE4 = TRIM (ATTRIBUTE4),
                ATTRIBUTE5 = TRIM (ATTRIBUTE5),
                ATTRIBUTE6 = TRIM (ATTRIBUTE6),
                ATTRIBUTE7 = TRIM (ATTRIBUTE7),
                ATTRIBUTE8 = TRIM (ATTRIBUTE8),
                ATTRIBUTE9 = TRIM (ATTRIBUTE9),
                ATTRIBUTE10 = TRIM (ATTRIBUTE10),
                ATTRIBUTE11 = TRIM (ATTRIBUTE11),
                ATTRIBUTE12 = TRIM (ATTRIBUTE12),
                ATTRIBUTE13 = TRIM (ATTRIBUTE13),
                ATTRIBUTE14 = TRIM (ATTRIBUTE14),
                ATTRIBUTE15 = TRIM (ATTRIBUTE15),
                ATTRIBUTE16 = TRIM (ATTRIBUTE16),
                ATTRIBUTE17 = TRIM (ATTRIBUTE17),
                ATTRIBUTE18 = TRIM (ATTRIBUTE18),
                ATTRIBUTE19 = TRIM (ATTRIBUTE19),
                ATTRIBUTE20 = TRIM (ATTRIBUTE20),
                COUNTRY = TRIM (COUNTRY),
                ADDRESS1 = NVL (TRIM (ADDRESS1), TRIM (ADDRESS2)),
                ADDRESS2 =
                   DECODE (TRIM (ADDRESS1), NULL, NULL, TRIM (ADDRESS2)),
                ADDRESS3 = TRIM (ADDRESS3),
                ADDRESS4 = TRIM (ADDRESS4),
                CITY = TRIM (CITY),
                POSTAL_CODE = LPAD (TRIM (POSTAL_CODE), 5, '0'),
                STATE = TRIM (STATE),
                COUNTY = TRIM (COUNTY),
                LEGACY_CUSTOMER_NUMBER = TRIM (LEGACY_CUSTOMER_NUMBER),
                LEGACY_PARTY_SITE_NUMBER = TRIM (LEGACY_PARTY_SITE_NUMBER),
                LEGACY_PARTY_SITE_NAME = TRIM (LEGACY_PARTY_SITE_NAME),
                PROVINCE = TRIM (PROVINCE),
                LOCATION_ID = TRIM (LOCATION_ID),
                TIMEZONE_ID = TRIM (TIMEZONE_ID),
                IDENTIFYING_ADDRESS_FLAG = TRIM (IDENTIFYING_ADDRESS_FLAG),
                PARTY_SITE_NAME = TRIM (PARTY_SITE_NAME),
                SITE_USE_TYPE = TRIM (SITE_USE_TYPE),
                PRIMARY_PER_TYPE = TRIM (PRIMARY_PER_TYPE),
                TERRITORY = TRIM (TERRITORY),
                BILL_TO_LOCATION = TRIM (BILL_TO_LOCATION),
                PRIMARY_BILL_TO_FLAG = TRIM (PRIMARY_BILL_TO_FLAG),
                GSA_INDICATOR = TRIM (GSA_INDICATOR),
                PRIMARY_SALESREP_NUMBER = TRIM (PRIMARY_SALESREP_NUMBER),
                PRIMARY_SHIP_TO_FLAG = TRIM (PRIMARY_SHIP_TO_FLAG),
                FOB_POINT = TRIM (FOB_POINT),
                FREIGHT_TERM = TRIM (FREIGHT_TERM),
                ORDER_TYPE = TRIM (ORDER_TYPE),
                PRICE_LIST_NAME = TRIM (PRICE_LIST_NAME),
                SHIP_VIA = TRIM (SHIP_VIA),
                SHIP_SETS_INCLUDE_LINES_FLAG =
                   TRIM (SHIP_SETS_INCLUDE_LINES_FLAG),
                ARRIVALSETS_INCLUDE_LINES_FLAG =
                   TRIM (ARRIVALSETS_INCLUDE_LINES_FLAG),
                INVOICE_QUANTITY_RULE = TRIM (INVOICE_QUANTITY_RULE),
                OVER_SHIPMENT_TOLERANCE = TRIM (OVER_SHIPMENT_TOLERANCE),
                UNDER_SHIPMENT_TOLERANCE = TRIM (UNDER_SHIPMENT_TOLERANCE),
                ITEM_CROSS_REF_PREF = TRIM (ITEM_CROSS_REF_PREF),
                DATE_TYPE_PREFERENCE = TRIM (DATE_TYPE_PREFERENCE),
                OVER_RETURN_TOLERANCE = TRIM (OVER_RETURN_TOLERANCE),
                UNDER_RETURN_TOLERANCE = TRIM (UNDER_RETURN_TOLERANCE),
                SCHED_DATE_PUSH_FLAG = TRIM (SCHED_DATE_PUSH_FLAG),
                WAREHOUSE_CODE = TRIM (WAREHOUSE_CODE),
                DATES_NEGATIVE_TOLERANCE = TRIM (DATES_NEGATIVE_TOLERANCE),
                DATES_POSITIVE_TOLERANCE = TRIM (DATES_POSITIVE_TOLERANCE),
                FINCHRG_RECEIVABLES_TRX_NAME =
                   TRIM (FINCHRG_RECEIVABLES_TRX_NAME),
                LEGACY_PARTY_NUMBER = TRIM (LEGACY_PARTY_NUMBER),
                PARTY_SITE_ID = TRIM (PARTY_SITE_ID),
                PARTY_SITE_NUMBER = TRIM (PARTY_SITE_NUMBER),
                PARTY_SITE_USE_ID = TRIM (PARTY_SITE_USE_ID),
                CUST_ACCT_SITE_ID = TRIM (CUST_ACCT_SITE_ID),
                CUST_SITE_USE_ID = TRIM (CUST_SITE_USE_ID),
                LOCATION = TRIM (LOCATION),
                OVERALL_CREDIT_LIMIT = TRIM (OVERALL_CREDIT_LIMIT),
                TRX_CREDIT_LIMIT = TRIM (TRX_CREDIT_LIMIT),
                MIN_DUNNING_AMOUNT = TRIM (MIN_DUNNING_AMOUNT),
                MIN_DUNNING_INVOICE_AMPOUNT =
                   TRIM (MIN_DUNNING_INVOICE_AMPOUNT),
                MAX_INTEREST_CHARGE = TRIM (MAX_INTEREST_CHARGE),
                MIN_STATEMENT_AMOUNT = TRIM (MIN_STATEMENT_AMOUNT),
                CLEARING_DAYS = TRIM (CLEARING_DAYS),
                AUTO_REC_INCL_DISPUTED_FLAG =
                   TRIM (AUTO_REC_INCL_DISPUTED_FLAG),
                AUTO_REC_MIN_RECEIPT_AMOUNT =
                   TRIM (AUTO_REC_MIN_RECEIPT_AMOUNT),
                CHARGE_ON_FINANCE_CHARGE_FLAG =
                   TRIM (CHARGE_ON_FINANCE_CHARGE_FLAG),
                LOCK_BOX_MATCHING_OPTION = TRIM (LOCK_BOX_MATCHING_OPTION),
                AUTOCASH_HIERARCHY_NAME = TRIM (AUTOCASH_HIERARCHY_NAME),
                REVIEW_CYCLE = TRIM (REVIEW_CYCLE),
                CREDIT_CLASSIFICATION = TRIM (CREDIT_CLASSIFICATION),
                AUTOPAY_FLAG = TRIM (AUTOPAY_FLAG),
                HDS_SITE_FLAG = TRIM (HDS_SITE_FLAG),
                JOB_NAME = TRIM (JOB_NAME),
                JOB_NUMBER = TRIM (JOB_NUMBER),
                CREDIT_HOLD = TRIM (CREDIT_HOLD),
                CREDIT_ANALYST_NAME = TRIM (CREDIT_ANALYST_NAME),
                LEGACY_SITE_TYPE = TRIM (LEGACY_SITE_TYPE),
                PROCESS_STATUS = 'R',
                VALIDATED_FLAG = TRIM (VALIDATED_FLAG),
                PROCESS_ERROR = NULL,
                SALESREP_ID = TRIM (SALESREP_ID),
                INSIDE_SALESREP_ID = TRIM (INSIDE_SALESREP_ID),
                FSD =
                   REPLACE (REPLACE (TRIM (FSD), CHR (13), ''), CHR (10), '')
          WHERE NVL (process_status, 'R') IN ('R', 'E');

         COMMIT;
      EXCEPTION
         WHEN OTHERS
         THEN
            NULL;
      END;
   END;

   PROCEDURE validate_inbound_data
   IS
      -- ==============================================================================================================================
      -- Procedure: validate_inbound_data
      -- Purpose: Validating Inbound data
      -- ==============================================================================================================================
      --  REVISIONS:
      --   Ver        Date        Author               Description
      --  ---------  ---------- ---------------  --------------------------------------------------------------------------------------
      --  1.0        06/06/2018  P.Vamshidhar     TMS#20180308-00290 -AH HARRIS Customer Conversion
      --  1.1        07/16/2018  P.Vamshidhar     TMS#20180716-00025 - AHH Customers Interface
      -- ==============================================================================================================================
      CURSOR cur_acct_validate
      IS
         SELECT ORIG_SYSTEM_REFERENCE LEGACY_CUSTOMER_NUMBER,
                DUNS_NUMBER_C,
                ROWID acct_row_id
           FROM XXWC.XXWC_AR_CUSTOMER_IFACE_STG
          WHERE PROCESS_STATUS IN ('R', 'E');


      CURSOR cur_site_validate (
         p_cust_num    VARCHAR2)
      IS
         SELECT ROWID site_row_id,
                LEGACY_CUSTOMER_NUMBER,
                LEGACY_PARTY_SITE_NUMBER,
                LEGACY_PARTY_SITE_NAME,
                COUNTRY,
                ADDRESS1,
                CITY,
                POSTAL_CODE,
                STATE,
                COUNTY
           FROM XXWC.XXWC_AR_CUST_SITE_IFACE_STG
          WHERE     LEGACY_CUSTOMER_NUMBER = P_CUST_NUM
                AND PROCESS_STATUS IN ('R', 'E');


      CURSOR cur_orp_site_cust
      IS
         SELECT DISTINCT LEGACY_CUSTOMER_NUMBER CUSTOMER_NUMBER
           FROM XXWC.XXWC_AR_CUST_SITE_IFACE_STG
          WHERE     LEGACY_CUSTOMER_NUMBER NOT IN (SELECT ORIG_SYSTEM_REFERENCE
                                                     FROM XXWC.XXWC_AR_CUSTOMER_IFACE_STG)
                AND PROCESS_STATUS IN ('R', 'E');

      lvc_procedure        VARCHAR2 (100) := 'validate_inbound_data';
      o_ret_mess           VARCHAR2 (32767);
      ln_err_count         NUMBER := 0;
      ln_valid_num         NUMBER;
      l_sec                VARCHAR2 (32767);
      ln_rec_count         NUMBER := 0;
      ln_count             NUMBER;
      lvc_err_flag         VARCHAR2 (1);
      x_msg_data           VARCHAR2 (32767);
      ln_cust_account_id   NUMBER;

      PROCEDURE err_mess (p_field_name IN VARCHAR2)
      IS
      BEGIN
         IF o_ret_mess IS NULL
         THEN
            o_ret_mess := p_field_name;
            ln_err_count := 1;
         ELSE
            o_ret_mess := o_ret_mess || '*' || p_field_name;
            ln_err_count := ln_err_count + 1;
         END IF;
      END;
   BEGIN
      FOR rec_acct_validate IN cur_acct_validate
      LOOP
         BEGIN
            o_ret_mess := NULL;
            lvc_err_flag := NULL;

            ln_rec_count := ln_rec_count + 1;
            l_sec := 'Customer Number Validation';
            ln_valid_num := NULL;
            lvc_err_flag := NULL;

            IF rec_acct_validate.LEGACY_CUSTOMER_NUMBER IS NOT NULL
            THEN
               BEGIN
                  ln_valid_num := NULL;

                  SELECT 1
                    INTO ln_valid_num
                    FROM DUAL
                   WHERE REGEXP_LIKE (
                            rec_acct_validate.LEGACY_CUSTOMER_NUMBER,
                            '^[[:digit:]]+$');
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     ln_valid_num := NULL;
               END;

               IF ln_valid_num IS NULL
               THEN
                  lvc_err_flag := 'Y';

                  UPDATE XXWC.XXWC_AR_CUSTOMER_IFACE_STG
                     SET PROCESS_STATUS = 'E',
                         PROCESS_ERROR = 'Invalid Customer Number'
                   WHERE ROWID = rec_acct_validate.acct_row_id;
               END IF;
            END IF;

            ln_valid_num := NULL;
            l_sec := 'DUNSNUMBER Validation';
            ln_valid_num := NULL;

            IF rec_acct_validate.DUNS_NUMBER_C IS NOT NULL
            THEN
               BEGIN
                  ln_valid_num := NULL;

                  SELECT 1
                    INTO ln_valid_num
                    FROM DUAL
                   WHERE REGEXP_LIKE (rec_acct_validate.DUNS_NUMBER_C,
                                      '^[[:digit:]]+$');
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     ln_valid_num := NULL;
               END;

               IF ln_valid_num IS NULL
               THEN
                  lvc_err_flag := 'Y';

                  UPDATE XXWC.XXWC_AR_CUSTOMER_IFACE_STG
                     SET PROCESS_STATUS = 'E',
                         PROCESS_ERROR =
                               PROCESS_ERROR
                            || ' - '
                            || rec_acct_validate.DUNS_NUMBER_C
                            || ' Invalid Dunsnumber'
                   WHERE ROWID = rec_acct_validate.acct_row_id;
               END IF;
            END IF;


            IF NVL (lvc_err_flag, 'N') = 'N'
            THEN
               UPDATE XXWC.XXWC_AR_CUSTOMER_IFACE_STG
                  SET PROCESS_STATUS = 'V'
                WHERE ROWID = rec_acct_validate.acct_row_id;
            END IF;

            IF NVL (lvc_err_flag, 'N') = 'N'
            THEN
               FOR rec_site_validate
                  IN cur_site_validate (
                        rec_acct_validate.LEGACY_CUSTOMER_NUMBER)
               LOOP
                  o_ret_mess := NULL;

                  IF rec_site_validate.LEGACY_PARTY_SITE_NAME IS NULL
                  THEN
                     err_mess (
                        'Legacy Party Site Name should not be null/blank.');
                  END IF;

                  IF rec_site_validate.ADDRESS1 IS NULL
                  THEN
                     err_mess ('Address1 is blank.');
                  END IF;

                  l_sec := 'City, County,State and Zipcode Validation';

                  IF     rec_site_validate.CITY IS NULL
                     AND rec_site_validate.COUNTY IS NULL
                     AND rec_site_validate.STATE IS NULL
                     AND rec_site_validate.POSTAL_CODE IS NULL
                  THEN
                     err_mess ('City, County,State,Zip_code are blank.');
                  END IF;

                  IF o_ret_mess IS NOT NULL
                  THEN
                     UPDATE XXWC.XXWC_AR_CUST_SITE_IFACE_STG
                        SET process_ERROR = o_ret_mess, process_STATUS = 'E'
                      WHERE ROWID = rec_site_validate.site_row_id;
                  ELSE
                     UPDATE XXWC.XXWC_AR_CUST_SITE_IFACE_STG
                        SET process_STATUS = 'V'
                      WHERE ROWID = rec_site_validate.site_row_id;
                  END IF;
               END LOOP;
            ELSE
               l_sec :=
                  'Updating child/site records for accounts in error status';

               UPDATE XXWC.XXWC_AR_CUST_SITE_IFACE_STG
                  SET process_STATUS = 'E',
                      process_ERROR =
                         'Account record in error status (Validation Level)'
                WHERE ORIG_SYSTEM_REFERENCE =
                         rec_acct_validate.LEGACY_CUSTOMER_NUMBER;
            END IF;

            COMMIT;
         EXCEPTION
            WHEN OTHERS
            THEN
               x_msg_data := SQLERRM;
               debug (rec_acct_validate.LEGACY_CUSTOMER_NUMBER, x_msg_data);

               --               UPDATE xxwc.XXWC_AR_CONV_CUST_ACCT_TBL
               --                  SET process_flag = 'E', PROCESS_message = x_msg_data
               --                WHERE ROWID = rec_main.row_id;

               xxcus_error_pkg.xxcus_error_main_api (
                  p_called_from         => g_err_callfrom || '.' || lvc_procedure,
                  p_calling             => l_sec,
                  p_ora_error_msg       => SQLERRM,
                  p_error_desc          =>    'Error Occured for '
                                           || rec_acct_validate.LEGACY_CUSTOMER_NUMBER
                                           || 'record id in XXWC.XXWC_AR_CUST_SITE_IFACE_STG.',
                  p_distribution_list   => g_distro_list,
                  p_module              => g_module);
         END;
      END LOOP;


      FOR rec_orp_site_cust IN cur_orp_site_cust
      LOOP
         BEGIN
            ln_cust_account_id := NULL;

            -- Checking customer exists/Active in Cross over or oracle
            BEGIN
               SELECT hca.CUST_ACCOUNT_ID
                 INTO ln_cust_account_id
                 FROM HZ_CUST_ACCOUNTS HCA,
                      XXWC.XXWC_AHH_AR_CUST_CROSS_OVER_T XACC
                WHERE     REPLACE (
                             REPLACE (TRIM (XACC.CUST_NUM), CHR (13), ''),
                             CHR (10),
                             '') = rec_orp_site_cust.CUSTOMER_NUMBER
                      AND REPLACE (
                             REPLACE (TRIM (XACC.ORACLE_CUST_NUM),
                                      CHR (13),
                                      ''),
                             CHR (10),
                             '') = HCA.ACCOUNT_NUMBER
                      AND ROWNUM = 1;
            EXCEPTION
               WHEN OTHERS
               THEN
                  BEGIN
                     SELECT hca.CUST_ACCOUNT_ID
                       INTO ln_cust_account_id
                       FROM apps.hz_cust_acct_sites_all hcas,
                            apps.hz_cust_accounts hca
                      WHERE     hcas.cust_account_id = hca.cust_account_id
                            AND hca.attribute4 = 'AHH'
                            AND NVL (hcas.attribute17, '123') =
                                   rec_orp_site_cust.CUSTOMER_NUMBER
                            AND hcas.bill_to_flag = 'P'
                            AND hca.status = 'A';
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        ln_cust_account_id := NULL;
                  END;
            END;

            l_sec := 'Customers Data fetched ' || ln_cust_account_id;

            IF ln_cust_account_id IS NOT NULL
            THEN
               FOR rec_site_validate
                  IN cur_site_validate (rec_orp_site_cust.CUSTOMER_NUMBER)
               LOOP
                  o_ret_mess := NULL;

                  IF rec_site_validate.LEGACY_PARTY_SITE_NAME IS NULL
                  THEN
                     err_mess (
                        'Legacy Party Site Name should not be null/blank.');
                  END IF;

                  IF rec_site_validate.ADDRESS1 IS NULL
                  THEN
                     err_mess ('Address1 is blank.');
                  END IF;

                  l_sec := 'City, County,State and Zipcode Validation';

                  IF     rec_site_validate.CITY IS NULL
                     AND rec_site_validate.COUNTY IS NULL
                     AND rec_site_validate.STATE IS NULL
                     AND rec_site_validate.POSTAL_CODE IS NULL
                  THEN
                     err_mess ('City, County,State,Zip_code are blank.');
                  END IF;

                  IF o_ret_mess IS NOT NULL
                  THEN
                     UPDATE XXWC.XXWC_AR_CUST_SITE_IFACE_STG
                        SET process_ERROR = o_ret_mess, process_STATUS = 'E'
                      WHERE ROWID = rec_site_validate.site_row_id;
                  ELSE
                     UPDATE XXWC.XXWC_AR_CUST_SITE_IFACE_STG
                        SET process_STATUS = 'V'
                      WHERE ROWID = rec_site_validate.site_row_id;
                  END IF;
               END LOOP;
            ELSE
               UPDATE XXWC.XXWC_AR_CUST_SITE_IFACE_STG
                  SET process_STATUS = 'E',
                      process_error = 'Customer does not Exists in Oracle'
                WHERE     ORIG_SYSTEM_REFERENCE =
                             rec_orp_site_cust.CUSTOMER_NUMBER
                      AND NVL (PROCESS_STATUS, 'R') IN ('R', 'E');
            END IF;

            COMMIT;
         EXCEPTION
            WHEN OTHERS
            THEN
               x_msg_data := SQLERRM;
               debug (rec_orp_site_cust.CUSTOMER_NUMBER, x_msg_data);

               --               UPDATE xxwc.XXWC_AR_CONV_CUST_ACCT_TBL
               --                  SET process_flag = 'E', PROCESS_message = x_msg_data
               --                WHERE ROWID = rec_main.row_id;

               xxcus_error_pkg.xxcus_error_main_api (
                  p_called_from         => g_err_callfrom || '.' || lvc_procedure,
                  p_calling             => l_sec,
                  p_ora_error_msg       => SQLERRM,
                  p_error_desc          =>    'Error Occured for '
                                           || rec_orp_site_cust.CUSTOMER_NUMBER
                                           || 'record id in XXWC.XXWC_AR_CUST_SITE_IFACE_STG.',
                  p_distribution_list   => g_distro_list,
                  p_module              => g_module);
         END;
      END LOOP;
   END;

   PROCEDURE CREATE_CUSTOMER (P_CUSTOMER_NUMBER   IN     VARCHAR2,
                              p_cust_account_id      OUT NUMBER,
                              p_party_id             OUT NUMBER)
   IS
      -- ==============================================================================================================================
      -- Procedure: CREATE_CUSTOMER
      -- Purpose: To Create customer
      -- ==============================================================================================================================
      --  REVISIONS:
      --   Ver        Date        Author               Description
      --  ---------  ---------- ---------------  --------------------------------------------------------------------------------------
      --  1.0        06/06/2018  P.Vamshidhar    AH HARRIS Customer Interface
      --  1.1        07/16/2018     P.Vamshidhar    TMS#20180716-00025 - AHH Customers Interface
      -- ==============================================================================================================================

      CURSOR CUR_MAIN
      IS
         SELECT xaca.ORIG_SYSTEM_REFERENCE RECORD_ID,
                xaca.ORIG_SYSTEM_REFERENCE CUSTOMER_NUMBER,
                xaca.ACCOUNT_NAME BUSINESS_NAME,
                NULL BUSINESS_PHONE,
                NULL BUSINESS_FAX,
                NULL EMAIL_ADDRESS,
                site.PRIMARY_SALESREP_NUMBER OUTSIDE_SALES_REP_ID,
                site.CREDIT_ANALYST_NAME credit_manager,
                SUBSTR (site.CREDIT_HOLD, 1, 1) CREDIT_HOLD,
                'UNKNOWN' PREDOMINANT_TRADE,
                xaca.STATUS STATUS,
                xaca.OVERALL_CREDIT_LIMIT CREDIT_LIMIT,
                NULL DEFAULT_JOB_CREDIT_LIMIT,
                NULL AP_PHONE,
                NULL AP_EMAIL,
                NULL INVOICE_EMAIL_ADDRESS,
                xaca.DUNS_NUMBER_C DUNSNUMBER,
                xaca.YEAR_ESTABLISHED CREATION_DATE,
                xaca.SEND_STATEMENTS SEND_STATEMENT,
                INITCAP (xaca.ATTRIBUTE_CATEGORY) MANDATORY_NOTES,
                xaca.ATTRIBUTE16 MANDATORY_NOTES1,
                xaca.ATTRIBUTE17 MANDATORY_NOTES2,
                xaca.ATTRIBUTE18 MANDATORY_NOTES3,
                xaca.ATTRIBUTE19 MANDATORY_NOTES4,
                xaca.ATTRIBUTE20 MANDATORY_NOTES5,
                xaca.STANDARD_TERM_NAME,
                'Y' AUTO_APPLY,
                xaca.sic_code,
                xaca.ROWID ROW_ID
           FROM XXWC.XXWC_AR_CUSTOMER_IFACE_STG xaca,
                XXWC.XXWC_AR_CUST_SITE_IFACE_STG site
          WHERE     xaca.ORIG_SYSTEM_REFERENCE = site.LEGACY_CUSTOMER_NUMBER
                AND site.LEGACY_PARTY_SITE_NUMBER IS NULL
                AND xaca.PROCESS_status = 'V'
                AND xaca.ORIG_SYSTEM_REFERENCE = P_CUSTOMER_NUMBER;

      porganizationrec          hz_party_v2pub.organization_rec_type;
      pcontactpointrec          hz_contact_point_v2pub.contact_point_rec_type;
      pphonerec                 hz_contact_point_v2pub.phone_rec_type;
      pemailrec                 hz_contact_point_v2pub.email_rec_type;
      pcustproamtrec            hz_customer_profile_v2pub.cust_profile_amt_rec_type;
      pcustaccountrec           hz_cust_account_v2pub.cust_account_rec_type;
      pprofilerec               hz_customer_profile_v2pub.customer_profile_rec_type;
      o_party_id                HZ_PARTIES.PARTY_ID%TYPE;
      o_party_number            HZ_PARTIES.PARTY_NUMBER%TYPE;
      o_profile_id              HZ_CUSTOMER_PROFILES.CUST_ACCOUNT_PROFILE_ID%TYPE;
      ol_profile_id             HZ_CUSTOMER_PROFILES.CUST_ACCOUNT_PROFILE_ID%TYPE;
      o_cust_account_no         HZ_CUST_ACCOUNTS.ACCOUNT_NUMBER%TYPE;
      o_custacctprofileamtid    NUMBER;
      o_contact_pointid         NUMBER;
      p_object_version_number   NUMBER;
      o_cust_account_id         HZ_CUST_ACCOUNTS.CUST_ACCOUNT_ID%TYPE;
      o_msg_data                VARCHAR2 (32767);
      o_return_msg              VARCHAR2 (32767);
      o_ret_status              VARCHAR2 (1);
      x_msg_data                VARCHAR2 (32767);
      x_ret_status              VARCHAR2 (1);
      o_msg_count               NUMBER;
      lvc_sec                   VARCHAR2 (4000);
      ln_salesrep_id            RA_SALESREPS_ALL.SALESREP_ID%TYPE;
      l_res_category            jtf_rs_resource_extns_tl.category%TYPE;
      lvc_procedure             VARCHAR2 (20) := 'CREATE_CUSTOMER';
      ln_profile_class_id       NUMBER;
      ln_collector_id           NUMBER;
      ln_credit_analyst         NUMBER;
      ln_contact_party_id       NUMBER;
   BEGIN
      lvc_sec := 'Procedure Started';

      SAVEPOINT customer_record;
      g_cust_acct_no := NULL;

      FOR rec_main IN cur_main
      LOOP
         -- Party Creation
         lvc_sec := 'Party Creation';
         porganizationrec := NULL;
         porganizationrec.organization_name := rec_main.BUSINESS_NAME;
         porganizationrec.created_by_module := g_api_name;

         IF rec_main.dunsnumber IS NOT NULL
         THEN
            porganizationrec.duns_number_c := rec_main.dunsnumber;
         END IF;

         hz_party_v2pub.create_organization (
            p_init_msg_list      => 'T',
            p_organization_rec   => porganizationrec,
            x_return_status      => o_ret_status,
            x_msg_count          => o_msg_count,
            x_msg_data           => o_msg_data,
            x_party_id           => o_party_id,
            x_party_number       => o_party_number,
            x_profile_id         => o_profile_id);

         IF o_ret_status <> 'S'
         THEN
            return_msg (o_msg_count, o_msg_data, o_return_msg);
            x_msg_data := 'Error Party Creation: ' || o_return_msg;
            debug (p_customer_number, x_msg_data);
            x_ret_status := o_ret_status;
            ROLLBACK TO customer_record;
            GOTO STATUS_UPDATE;
         END IF;

         lvc_sec :=
            'Party Creation Process Completed - API Status ' || o_ret_status;

         debug (p_customer_number, lvc_sec);

         lvc_sec := 'Business Phone Creation';

         IF rec_main.business_phone IS NOT NULL
         THEN
            o_ret_status := NULL;
            o_msg_count := NULL;
            o_msg_data := NULL;
            pcontactpointrec := NULL;
            pcontactpointrec.contact_point_type := 'PHONE';
            pcontactpointrec.owner_table_name := 'HZ_PARTIES';
            pcontactpointrec.owner_table_id := o_party_id;
            pcontactpointrec.primary_flag := 'Y';
            pcontactpointrec.contact_point_purpose := 'BUSINESS';
            pphonerec.phone_country_code := NULL;
            pphonerec.phone_area_code :=
               SUBSTR (rec_main.business_phone, 1, 3);
            pphonerec.phone_number := SUBSTR (rec_main.business_phone, 4);
            pphonerec.phone_extension := NULL;
            pphonerec.phone_line_type := 'GEN';
            pcontactpointrec.created_by_module := g_api_name;

            hz_contact_point_v2pub.create_contact_point (
               p_init_msg_list       => 'T',
               p_contact_point_rec   => pcontactpointrec,
               p_phone_rec           => pphonerec,
               x_contact_point_id    => o_contact_pointid,
               x_return_status       => o_ret_status,
               x_msg_count           => o_msg_count,
               x_msg_data            => o_msg_data);

            IF o_ret_status <> 'S'
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data := 'Error Phone Creation: ' || o_return_msg;
               debug (p_customer_number, x_msg_data);
               x_ret_status := o_ret_status;
               ROLLBACK TO customer_record;
               GOTO STATUS_UPDATE;
            END IF;

            lvc_sec :=
                  'Business Phone Creation Completed - API Status '
               || o_ret_status;
            debug (p_customer_number, lvc_sec);
         END IF;

         lvc_sec := 'Business Fax Creation';

         IF rec_main.business_fax IS NOT NULL
         THEN
            o_ret_status := NULL;
            o_msg_count := NULL;
            o_msg_data := NULL;
            pcontactpointrec := NULL;
            pcontactpointrec.contact_point_type := 'PHONE';
            pcontactpointrec.owner_table_name := 'HZ_PARTIES';
            pcontactpointrec.owner_table_id := o_party_id;
            pcontactpointrec.primary_flag := 'N';
            pcontactpointrec.contact_point_purpose := 'BUSINESS';
            pphonerec.phone_area_code := SUBSTR (rec_main.business_fax, 1, 3);
            pphonerec.phone_number := SUBSTR (rec_main.business_fax, 4);
            pphonerec.phone_extension := NULL;
            pphonerec.phone_line_type := 'FAX';
            pcontactpointrec.created_by_module := g_api_name;

            hz_contact_point_v2pub.create_contact_point (
               p_init_msg_list       => 'T',
               p_contact_point_rec   => pcontactpointrec,
               p_phone_rec           => pphonerec,
               x_contact_point_id    => o_contact_pointid,
               x_return_status       => o_ret_status,
               x_msg_count           => o_msg_count,
               x_msg_data            => o_msg_data);

            IF o_ret_status <> 'S'
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data := 'Error: Fax Creation: ' || o_return_msg;
               debug (p_customer_number, x_msg_data);
               x_ret_status := o_ret_status;
               ROLLBACK TO customer_record;
               GOTO STATUS_UPDATE;
            END IF;

            lvc_sec :=
                  'Business Fax Creation Completed - API Status '
               || o_ret_status;
            debug (p_customer_number, lvc_sec);
         END IF;

         lvc_sec := 'Party Email Creation';

         IF rec_main.email_address IS NOT NULL
         THEN
            o_ret_status := NULL;
            o_msg_count := NULL;
            o_msg_data := NULL;
            pcontactpointrec := NULL;
            pemailrec.email_address := rec_main.email_address;
            pemailrec.email_format := 'MAILHTML';
            pcontactpointrec.contact_point_type := 'EMAIL';
            pcontactpointrec.owner_table_name := 'HZ_PARTIES';
            pcontactpointrec.owner_table_id := o_party_id;
            pcontactpointrec.primary_flag := 'Y';
            pcontactpointrec.contact_point_purpose := 'BUSINESS';
            pcontactpointrec.created_by_module := g_api_name;

            hz_contact_point_v2pub.create_contact_point (
               p_init_msg_list       => 'T',
               p_contact_point_rec   => pcontactpointrec,
               p_email_rec           => pemailrec,
               x_contact_point_id    => o_contact_pointid,
               x_return_status       => o_ret_status,
               x_msg_count           => o_msg_count,
               x_msg_data            => o_msg_data);

            IF o_ret_status <> 'S'
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data := 'Error: Email Creation: ' || o_return_msg;
               debug (p_customer_number, x_msg_data);
               x_ret_status := o_ret_status;
               ROLLBACK TO customer_record;
               GOTO STATUS_UPDATE;
            END IF;

            lvc_sec :=
               'Email Creation Completed - API Status ' || o_ret_status;
            debug (p_customer_number, lvc_sec);
         END IF;

         lvc_sec := 'Invoice Email Creation';

         IF rec_main.invoice_email_address IS NOT NULL
         THEN
            o_ret_status := NULL;
            o_msg_count := NULL;
            o_msg_data := NULL;
            pcontactpointrec := NULL;
            pemailrec.email_address := rec_main.invoice_email_address;
            pemailrec.email_format := 'MAILHTML';
            pcontactpointrec.contact_point_type := 'EMAIL';
            pcontactpointrec.owner_table_name := 'HZ_PARTIES';
            pcontactpointrec.owner_table_id := o_party_id;
            pcontactpointrec.contact_point_purpose := 'INVOICE';
            pcontactpointrec.created_by_module := g_api_name;

            hz_contact_point_v2pub.create_contact_point (
               p_init_msg_list       => 'T',
               p_contact_point_rec   => pcontactpointrec,
               p_email_rec           => pemailrec,
               x_contact_point_id    => o_contact_pointid,
               x_return_status       => o_ret_status,
               x_msg_count           => o_msg_count,
               x_msg_data            => o_msg_data);

            IF o_ret_status <> 'S'
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data := 'Error: Invoice Email Creation: ' || o_return_msg;
               debug (p_customer_number, x_msg_data);
               x_ret_status := o_ret_status;
               ROLLBACK TO customer_record;
               GOTO STATUS_UPDATE;
            END IF;

            lvc_sec :=
                  'Invoice Email Creation Completed - API Status '
               || o_ret_status;
            debug (p_customer_number, lvc_sec);
         END IF;

         lvc_sec := 'Customer Account Creation';
         ln_profile_class_id := NULL;

         IF rec_main.standard_term_name = 'COD'
         THEN
            SELECT PROFILE_CLASS_ID
              INTO ln_profile_class_id
              FROM HZ_CUST_PROFILE_CLASSES
             WHERE NAME = 'COD Customers';
         ELSE
            SELECT PROFILE_CLASS_ID
              INTO ln_profile_class_id
              FROM HZ_CUST_PROFILE_CLASSES
             WHERE NAME = 'Contractor - Non Key';
         END IF;

         DEBUG (p_customer_number,
                'ln_profile_class_id:' || ln_profile_class_id);

         BEGIN
            -- Deriving Sales Rep
            BEGIN
               SELECT A.SALESREP_ID
                 INTO ln_salesrep_id
                 FROM RA_SALESREPS_ALL A, XXWC.XXWC_AHH_AM_T B
                WHERE     A.ORG_ID = 162
                      AND UPPER (TRIM (B.SLSREPOUT)) =
                             UPPER (TRIM (rec_main.OUTSIDE_SALES_REP_ID))
                      AND A.SALESREP_NUMBER = B.SALESREP_ID
                      AND ROWNUM = 1;
            EXCEPTION
               WHEN OTHERS
               THEN
                  BEGIN
                     SELECT A.SALESREP_ID
                       INTO ln_salesrep_id
                       FROM RA_SALESREPS_ALL A, XXWC.XXWC_AHH_AM_T B
                      WHERE     A.ORG_ID = 162
                            AND UPPER (TRIM (B.SLSREPOUT)) = 'DEFAULT'
                            AND A.SALESREP_NUMBER = B.SALESREP_ID
                            AND ROWNUM = 1;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        ln_salesrep_id := -3;
                  END;
            END;

            DEBUG (p_customer_number, 'ln_salesrep_id:' || ln_salesrep_id);

            -- Deriving Collector
            BEGIN
               SELECT ORA_COLL_ID
                 INTO ln_collector_id
                 FROM XXWC.XXWC_AHH_CRD_COLL_ANALYST_T b
                WHERE     UPPER (TRIM (b.ahh_coll_code)) =
                             UPPER (rec_main.credit_manager)
                      AND ROWNUM = 1;
            EXCEPTION
               WHEN OTHERS
               THEN
                  BEGIN
                     SELECT ORA_COLL_ID
                       INTO ln_collector_id
                       FROM XXWC.XXWC_AHH_CRD_COLL_ANALYST_T b
                      WHERE     UPPER (TRIM (b.ahh_coll_code)) = 'DEFAULT'
                            AND ROWNUM = 1;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        ln_collector_id := 1;             -- Default Collector
                  END;
            END;

            DEBUG (p_customer_number, 'ln_collector_id:' || ln_collector_id);

            BEGIN
               SELECT ORA_CRD_ANLST_ID
                 INTO ln_credit_analyst
                 FROM XXWC.XXWC_AHH_CRD_COLL_ANALYST_T b
                WHERE     UPPER (TRIM (b.ahh_coll_code)) =
                             UPPER (rec_main.credit_manager)
                      AND ROWNUM = 1;
            EXCEPTION
               WHEN OTHERS
               THEN
                  BEGIN
                     SELECT ORA_CRD_ANLST_ID
                       INTO ln_credit_analyst
                       FROM XXWC.XXWC_AHH_CRD_COLL_ANALYST_T b
                      WHERE     UPPER (TRIM (b.ahh_coll_code)) = 'DEFAULT'
                            AND ROWNUM = 1;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        ln_credit_analyst := NULL;
                  END;
            END;

            DEBUG (p_customer_number,
                   'ln_credit_analyst:' || ln_credit_analyst);


            --Sic code and sic code type
            IF rec_main.sic_code IS NOT NULL AND rec_main.sic_code <> 0
            THEN
               BEGIN
                  SELECT lookup_code, lookup_type
                    INTO porganizationrec.sic_code,
                         porganizationrec.sic_code_type
                    FROM ar_lookups b
                   WHERE     b.lookup_code = rec_main.sic_code
                         AND b.lookup_type = '1972 SIC';
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     porganizationrec.sic_code_type := NULL;
                     porganizationrec.sic_code := NULL;
               END;
            END IF;

            o_ret_status := NULL;
            o_msg_count := NULL;
            o_msg_data := NULL;
            porganizationrec := NULL;
            pcustaccountrec := NULL;
            pprofilerec := NULL;

            SELECT apps.hz_cust_accounts_s.NEXTVAL
              INTO pcustaccountrec.cust_account_id
              FROM DUAL;

            porganizationrec.party_rec.party_id := o_party_id;
            pcustaccountrec.account_name := rec_main.business_name;
            pcustaccountrec.tax_code := NULL;
            pcustaccountrec.attribute_category := 'No';
            pcustaccountrec.account_established_date := rec_main.CREATION_DATE;
            pcustaccountrec.created_by_module := g_api_name;
            pcustaccountrec.customer_type := 'R';
            pcustaccountrec.attribute4 := 'AHH';
            pcustaccountrec.attribute7 := 'N';
            pcustaccountrec.attribute11 := rec_main.auto_apply;
            pcustaccountrec.customer_class_code := 'UNKNOWN';
            pcustaccountrec.orig_system_reference :=
               pcustaccountrec.cust_account_id;

            debug (p_customer_number,
                   'Cust_account_id:' || pcustaccountrec.cust_account_id);

            pcustaccountrec.attribute_category := rec_main.MANDATORY_NOTES;

            IF rec_main.MANDATORY_NOTES = 'Yes'
            THEN
               pcustaccountrec.attribute17 :=
                  SUBSTR (rec_main.mandatory_notes1, 1, 150);
               pcustaccountrec.attribute18 :=
                  SUBSTR (rec_main.mandatory_notes2, 1, 150);
               pcustaccountrec.attribute19 :=
                  SUBSTR (rec_main.mandatory_notes3, 1, 150);
               pcustaccountrec.attribute20 :=
                  SUBSTR (rec_main.mandatory_notes4, 1, 150);
               pcustaccountrec.attribute16 :=
                  SUBSTR (rec_main.mandatory_notes5, 1, 150);
            END IF;

            pcustaccountrec.status := rec_main.status;
            pcustaccountrec.attribute9 := rec_main.PREDOMINANT_TRADE;
            pprofilerec.profile_class_id := ln_profile_class_id;
            pprofilerec.credit_rating := 'Y';
            pprofilerec.standard_terms := g_term_id;

            IF rec_main.send_statement = 'Y'
            THEN
               pprofilerec.send_statements := rec_main.send_statement;
            ELSE
               pprofilerec.send_statements := rec_main.send_statement;
               pprofilerec.statement_cycle_id := NULL;
               pprofilerec.credit_balance_statements := 'N';
            END IF;



            pprofilerec.credit_checking := 'Y';

            IF ln_collector_id IS NOT NULL
            THEN
               pprofilerec.collector_id := ln_collector_id;
            END IF;

            IF ln_credit_analyst IS NOT NULL
            THEN
               pprofilerec.credit_analyst_id := ln_credit_analyst;
            END IF;

            pprofilerec.account_status := g_acct_status;
            pprofilerec.attribute2 := 2;
            pprofilerec.attribute3 := 'Y';
            pprofilerec.tolerance := 0;
            pprofilerec.credit_hold := rec_main.credit_hold;
            pprofilerec.credit_classification := 'MODERATE';


            BEGIN
               SELECT jrre.category
                 INTO l_res_category
                 FROM apps.jtf_rs_salesreps_mo_v jrs,
                      jtf_rs_resource_extns_tl jrre
                WHERE     1 = 1
                      AND jrs.resource_id = jrre.resource_id
                      AND jrs.resource_id = ln_salesrep_id
                      AND jrs.STATUS = 'A'
                      AND ROWNUM = 1;

               IF     l_res_category = 'OTHER'
                  AND ln_salesrep_id NOT IN (100000617, 100000650)
               THEN
                  pcustaccountrec.sales_channel_code := 'BQU_UNKNOWN';
               ELSIF     l_res_category = 'OTHER'
                     AND ln_salesrep_id IN (100000617, 100000650)
               THEN
                  pcustaccountrec.sales_channel_code := 'ACCT_MGR';
               ELSIF l_res_category = 'EMPLOYEE'
               THEN
                  pcustaccountrec.sales_channel_code := 'ACCT_MGR';
               ELSE
                  pcustaccountrec.sales_channel_code := NULL;
               END IF;
            EXCEPTION
               WHEN OTHERS
               THEN
                  pcustaccountrec.sales_channel_code := NULL;
            END;

            hz_cust_account_v2pub.create_cust_account (
               p_init_msg_list          => 'T',
               p_cust_account_rec       => pcustaccountrec,
               p_organization_rec       => porganizationrec,
               p_customer_profile_rec   => pprofilerec,
               p_create_profile_amt     => fnd_api.g_true,
               x_cust_account_id        => o_cust_account_id,
               x_account_number         => o_cust_account_no,
               x_party_id               => o_party_id,
               x_party_number           => o_party_number,
               x_profile_id             => o_profile_id,
               x_return_status          => o_ret_status,
               x_msg_count              => o_msg_count,
               x_msg_data               => o_msg_data);


            IF o_ret_status <> 'S'
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data := 'Error: Account Creation: ' || o_return_msg;
               debug (p_customer_number, x_msg_data);
               x_ret_status := o_ret_status;
               ROLLBACK TO customer_record;
               GOTO STATUS_UPDATE;
            END IF;

            g_cust_acct_no := o_cust_account_no;
            lvc_sec :=
                  'Customer Account Creation Process Completed - API Status '
               || o_ret_status;
            debug (p_customer_number, lvc_sec);
         END;


         SAVEPOINT customer_record;

         BEGIN
            SELECT CUST_ACCOUNT_PROFILE_ID
              INTO ol_profile_id
              FROM APPS.HZ_CUSTOMER_PROFILES
             WHERE     CUST_ACCOUNT_ID = o_cust_account_id
                   AND SITE_USE_ID IS NULL;
         EXCEPTION
            WHEN OTHERS
            THEN
               ol_profile_id := NULL;
         END;

         IF o_profile_id IS NULL
         THEN
            o_profile_id := ol_profile_id;
         END IF;

         lvc_sec := 'Profile Amounts Update';

         pcustproamtrec := NULL;

         IF ol_profile_id IS NOT NULL
         THEN
            BEGIN
               SELECT cust_acct_profile_amt_id, object_version_number
                 INTO o_custacctprofileamtid, p_object_version_number
                 FROM apps.hz_cust_profile_amts
                WHERE     cust_account_profile_id = ol_profile_id
                      AND currency_code = g_currency_code;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  o_custacctprofileamtid := NULL;
                  p_object_version_number := NULL;
            END;

            IF rec_main.credit_limit = 0
            THEN
               pcustproamtrec.overall_credit_limit := 9999999.99;
            ELSIF rec_main.credit_limit = NULL
            THEN
               pcustproamtrec.overall_credit_limit := NULL;
            ELSE
               pcustproamtrec.overall_credit_limit := rec_main.credit_limit;
            END IF;

            IF o_custacctprofileamtid IS NOT NULL
            THEN
               pcustproamtrec.cust_acct_profile_amt_id :=
                  o_custacctprofileamtid;
               hz_customer_profile_v2pub.update_cust_profile_amt (
                  p_init_msg_list           => 'T',
                  p_cust_profile_amt_rec    => pcustproamtrec,
                  p_object_version_number   => p_object_version_number,
                  x_return_status           => o_ret_status,
                  x_msg_count               => o_msg_count,
                  x_msg_data                => o_msg_data);
            ELSE
               pcustproamtrec.cust_account_profile_id := ol_profile_id;
               pcustproamtrec.cust_account_id := o_cust_account_id;
               pcustproamtrec.currency_code := g_currency_code;
               pcustproamtrec.created_by_module := g_api_name;
               hz_customer_profile_v2pub.create_cust_profile_amt (
                  p_init_msg_list              => 'T',
                  p_check_foreign_key          => 'T',
                  p_cust_profile_amt_rec       => pcustproamtrec,
                  x_cust_acct_profile_amt_id   => o_custacctprofileamtid,
                  x_return_status              => o_ret_status,
                  x_msg_count                  => o_msg_count,
                  x_msg_data                   => o_msg_data);
            END IF;

            IF o_ret_status <> 'S'
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data :=
                  'Error: Account Level Profile amounts: ' || o_return_msg;
               debug (p_customer_number, x_msg_data);
               x_ret_status := o_ret_status;
               ROLLBACK TO customer_record;
               GOTO STATUS_UPDATE;
            END IF;

            IF     NVL (o_custacctprofileamtid, 0) > 0
               AND rec_main.credit_limit IS NULL
            THEN
               BEGIN
                  UPDATE APPS.HZ_CUST_PROFILE_AMTS
                     SET OVERALL_CREDIT_LIMIT = NULL
                   WHERE CUST_ACCT_PROFILE_AMT_ID = o_custacctprofileamtid;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     NULL;
               END;
            END IF;
         END IF;


        <<STATUS_UPDATE>>
         IF NVL (x_ret_status, 'S') = 'S'
         THEN
            p_cust_account_id := o_cust_account_id;
            p_party_id := o_party_id;

            UPDATE XXWC.XXWC_AR_CUSTOMER_IFACE_STG
               SET PROCESS_STATUS = 'Y'
             --ORACLE_CUSTOMER_NUMBER = o_cust_account_no
             WHERE ROWID = rec_main.row_id;
         ELSE
            p_cust_account_id := NULL;
            p_party_id := NULL;

            UPDATE XXWC.XXWC_AR_CUSTOMER_IFACE_STG
               SET PROCESS_STATUS = 'E', PROCESS_ERROR = x_msg_data
             WHERE ROWID = rec_main.row_id;
         END IF;
      END LOOP;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         p_cust_account_id := NULL;
         x_msg_data := SQLERRM;
         debug (p_customer_number, x_msg_data);

         UPDATE XXWC.XXWC_AR_CUSTOMER_IFACE_STG
            SET process_status = 'E', PROCESS_error = x_msg_data
          WHERE ORIG_SYSTEM_REFERENCE = p_customer_number;

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || lvc_procedure,
            p_calling             => lvc_sec,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          =>    'Error Occured for '
                                     || p_customer_number
                                     || 'record id in XXWC_AR_CUSTOMER_IFACE_STG.',
            p_distribution_list   => g_distro_list,
            p_module              => g_module);
   END;

   PROCEDURE primary_bill_to (p_customer_number   IN VARCHAR2,
                              p_cust_account_id   IN NUMBER,
                              p_party_id          IN NUMBER)
   IS
      -- ==============================================================================================================================
      -- Procedure: Primary_bill_to
      -- Purpose: To customer primary bill to
      -- ==============================================================================================================================
      --  REVISIONS:
      --   Ver        Date        Author               Description
      --  ---------  ---------- ---------------  --------------------------------------------------------------------------------------
      --  1.0        06/06/2018  P.Vamshidhar    AH HARRIS Customer Interface
      -- ==============================================================================================================================
      CURSOR CUR_MAIN
      IS
         SELECT xaca.ORIG_SYSTEM_REFERENCE RECORD_ID,
                xaca.ORIG_SYSTEM_REFERENCE CUSTOMER_NUMBER,
                NULL CUSTOMER_SITE_NUMBER,
                site.LEGACY_PARTY_SITE_NAME BUSINESS_NAME,
                NULL BUSINESS_PHONE,
                NULL BUSINESS_FAX,
                NULL EMAIL_ADDRESS,
                site.PRIMARY_SALESREP_NUMBER OUTSIDE_SALES_REP_ID,
                site.CREDIT_ANALYST_NAME credit_manager,
                SUBSTR (site.CREDIT_HOLD, 1, 1) CREDIT_HOLD,
                'UNKNOWN' PREDOMINANT_TRADE,
                xaca.STATUS STATUS,
                NULL CREDIT_LIMIT,
                site.OVERALL_CREDIT_LIMIT DEFAULT_JOB_CREDIT_LIMIT,
                site.ADDRESS1 BILLING_ADDRESS1,
                site.ADDRESS2 BILLING_ADDRESS2,
                site.CITY BILLING_CITY,
                site.COUNTY BILLING_COUNTY,
                site.STATE BILLING_STATE,
                NVL (site.COUNTRY, 'US') BILLING_COUNTRY,
                site.POSTAL_CODE BILLING_ZIP_CODE,
                NULL AP_CONTACT_FIRST_NAME,
                NULL AP_CONTACT_LAST_NAME,
                NULL AP_PHONE,
                NULL AP_EMAIL,
                NULL INVOICE_EMAIL_ADDRESS,
                NULL DUNSNUMBER,
                SUBSTR (site.LEGACY_PARTY_SITE_NAME, 1, 35) JOB_SITE_NAME,
                'Y' purchase_order_required,
                '2' print_prices_on_order,
                'None' TAX_EXEMPTION,
                'Y' AUTO_APPLY,
                'MSTR' yard_job_accnt_project,
                site.ROWID ROW_ID,
                xaca.standard_term_name,
                site.inside_salesrep_id,
                site.fsd
           FROM XXWC.XXWC_AR_CUSTOMER_IFACE_STG xaca,
                XXWC.XXWC_AR_CUST_SITE_IFACE_STG site
          WHERE     xaca.ORIG_SYSTEM_REFERENCE = site.LEGACY_CUSTOMER_NUMBER
                AND site.LEGACY_PARTY_SITE_NUMBER IS NULL
                AND xaca.PROCESS_status = 'Y'
                AND site.PROCESS_STATUS = 'V'
                AND xaca.ORIG_SYSTEM_REFERENCE = P_CUSTOMER_NUMBER;

      lvc_sec                          VARCHAR2 (4000);
      plocationrec                     hz_location_v2pub.location_rec_type;
      ppartysiterec                    hz_party_site_v2pub.party_site_rec_type;
      pcustacctsiterec                 hz_cust_account_site_v2pub.cust_acct_site_rec_type;
      pcustacctsiteuserec              hz_cust_account_site_v2pub.cust_site_use_rec_type;
      pcustomerprofile                 hz_customer_profile_v2pub.customer_profile_rec_type;
      pcustproamtrec                   hz_customer_profile_v2pub.cust_profile_amt_rec_type;
      pcontactpointrec                 hz_contact_point_v2pub.contact_point_rec_type;
      p_create_person_rec              hz_party_v2pub.person_rec_type;
      p_cr_cust_acc_role_rec           hz_cust_account_role_v2pub.cust_account_role_rec_type;
      p_org_contact_rec                hz_party_contact_v2pub.org_contact_rec_type;
      pemailrec                        hz_contact_point_v2pub.email_rec_type;
      pedirec                          hz_contact_point_v2pub.EDI_REC_TYPE;
      pphonerec                        hz_contact_point_v2pub.phone_rec_type;
      ptelexrec                        hz_contact_point_v2pub.TELEX_REC_TYPE;
      pwebrec                          hz_contact_point_v2pub.WEB_REC_TYPE;
      p_ROLE_RESPONSIBILITY_REC_TYPE   HZ_CUST_ACCOUNT_ROLE_V2PUB.role_responsibility_rec_type;
      o_location_id                    HZ_LOCATIONS.location_id%TYPE;
      o_contact_point_id               NUMBER;
      x_msg_count                      NUMBER;
      x_msg_data                       VARCHAR2 (32767);
      x_contact_party_id               NUMBER;
      x_contact_party_number           NUMBER;
      x_contact_profile_id             NUMBER;
      x_rel_party_number               NUMBER;
      x_party_rel_id                   NUMBER;
      x_rel_party_id                   NUMBER;
      x_org_contact_id                 NUMBER;
      x_ret_status                     VARCHAR2 (1);
      o_ret_status                     VARCHAR2 (1);
      o_msg_count                      NUMBER;
      x_cust_account_role_id           NUMBER;
      o_msg_data                       VARCHAR2 (32767);
      o_return_msg                     VARCHAR2 (32767);
      o_party_site_id                  hz_party_sites.party_site_id%TYPE;
      o_party_site_no                  hz_party_sites.party_site_number%TYPE;
      o_cust_acct_site_id              hz_cust_acct_sites_all.cust_acct_site_id%TYPE;
      o_cust_acct_site_use_id          hz_cust_site_uses_all.site_use_id%TYPE;
      o_sprofile_id                    NUMBER;
      p_object_version_number          NUMBER;
      o_cust_acct_profile_amt_id       NUMBER;
      lvc_procedure                    VARCHAR2 (20) := 'PRIMARY_BILL_TO';
      ln_profile_class_id              NUMBER;
      ln_collector_id                  NUMBER;
      ln_credit_analyst                NUMBER;
      ln_contact_party_id              NUMBER;
      o_resp_id                        NUMBER;
   BEGIN
      lvc_sec := 'Primary Bill Procedure Started';
      debug (p_customer_number, lvc_sec);

      lvc_sec := 'Location Creation';

      SAVEPOINT customer_record;

      FOR rec_main IN cur_main
      LOOP
         lvc_sec := 'Deriving country code';

         BEGIN
            BEGIN
               SELECT TERRITORY_CODE
                 INTO plocationrec.country
                 FROM HR_TERRITORIES_V
                WHERE (   UPPER (TERRITORY_SHORT_NAME) =
                             UPPER (rec_main.BILLING_COUNTRY)
                       OR UPPER (TERRITORY_CODE) =
                             UPPER (rec_main.BILLING_COUNTRY));
            EXCEPTION
               WHEN OTHERS
               THEN
                  x_msg_data := 'Invalid Billing Country';
                  debug (p_customer_number, x_msg_data);
                  x_ret_status := 'E';
                  GOTO STATUS_UPDATE;
            END;

            plocationrec.address1 := rec_main.BILLING_ADDRESS1;
            plocationrec.address2 := rec_main.BILLING_ADDRESS2;
            plocationrec.state := rec_main.BILLING_STATE;
            plocationrec.city := rec_main.BILLING_CITY;
            plocationrec.postal_code := rec_main.BILLING_ZIP_CODE;
            plocationrec.county := rec_main.BILLING_COUNTY;
            plocationrec.created_by_module := g_api_name;
            plocationrec.sales_tax_geocode := NULL;

            hz_location_v2pub.create_location (
               p_init_msg_list   => 'T',
               p_location_rec    => plocationrec,
               x_location_id     => o_location_id,
               x_return_status   => o_ret_status,
               x_msg_count       => o_msg_count,
               x_msg_data        => o_msg_data);

            IF o_ret_status <> 'S'
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data :=
                  'Error: Primary Bill to Location creation' || o_return_msg;
               debug (p_customer_number, x_msg_data);
               x_ret_status := o_ret_status;
               ROLLBACK TO customer_record;
               GOTO STATUS_UPDATE;
            END IF;

            lvc_sec :=
                  'Location Account for Bill To Site Creation Process Completed - API Status'
               || x_ret_status;

            debug (p_customer_number, lvc_sec);

            lvc_sec := 'Party Site Creation';

            x_ret_status := NULL;
            o_msg_count := NULL;
            o_msg_data := NULL;
            o_party_site_id := NULL;
            o_party_site_no := NULL;
            o_sprofile_id := NULL;
            ppartysiterec.created_by_module := g_api_name;
            ppartysiterec.party_id := p_party_id;
            ppartysiterec.location_id := o_location_id;
            ppartysiterec.identifying_address_flag := 'Y';

            hz_party_site_v2pub.create_party_site (
               p_init_msg_list       => 'T',
               p_party_site_rec      => ppartysiterec,
               x_party_site_id       => o_party_site_id,
               x_party_site_number   => o_party_site_no,
               x_return_status       => x_ret_status,
               x_msg_count           => o_msg_count,
               x_msg_data            => o_msg_data);

            IF x_ret_status <> 'S'
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data :=
                  'Error:Primary Bill Party Site Creation: ' || o_return_msg;
               debug (p_customer_number, x_msg_data);
               ROLLBACK TO customer_record;
               GOTO STATUS_UPDATE;
            END IF;

            lvc_sec :=
                  'Party Site Creation Process Completed - API Status '
               || x_ret_status;

            debug (p_customer_number, lvc_sec);

            -- Deriving Profile Class
            ln_profile_class_id := NULL;

            IF rec_main.standard_term_name = 'COD'
            THEN
               SELECT PROFILE_CLASS_ID
                 INTO ln_profile_class_id
                 FROM HZ_CUST_PROFILE_CLASSES
                WHERE NAME = 'COD Customers';
            ELSE
               SELECT PROFILE_CLASS_ID
                 INTO ln_profile_class_id
                 FROM HZ_CUST_PROFILE_CLASSES
                WHERE NAME = 'Contractor - Non Key';
            END IF;

            -- Deriving Collector Id
            BEGIN
               SELECT ORA_COLL_ID
                 INTO ln_collector_id
                 FROM XXWC.XXWC_AHH_CRD_COLL_ANALYST_T b
                WHERE     UPPER (TRIM (b.ahh_coll_code)) =
                             UPPER (rec_main.credit_manager)
                      AND ROWNUM = 1;
            EXCEPTION
               WHEN OTHERS
               THEN
                  BEGIN
                     SELECT ORA_COLL_ID
                       INTO ln_collector_id
                       FROM XXWC.XXWC_AHH_CRD_COLL_ANALYST_T b
                      WHERE     UPPER (TRIM (b.ahh_coll_code)) = 'DEFAULT'
                            AND ROWNUM = 1;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        ln_collector_id := 1;             -- Default Collector
                  END;
            END;

            DEBUG (p_customer_number, 'ln_collector_id:' || ln_collector_id);

            BEGIN
               SELECT ORA_CRD_ANLST_ID
                 INTO ln_credit_analyst
                 FROM XXWC.XXWC_AHH_CRD_COLL_ANALYST_T b
                WHERE     UPPER (TRIM (b.ahh_coll_code)) =
                             UPPER (rec_main.credit_manager)
                      AND ROWNUM = 1;
            EXCEPTION
               WHEN OTHERS
               THEN
                  BEGIN
                     SELECT ORA_CRD_ANLST_ID
                       INTO ln_credit_analyst
                       FROM XXWC.XXWC_AHH_CRD_COLL_ANALYST_T b
                      WHERE     UPPER (TRIM (b.ahh_coll_code)) = 'DEFAULT'
                            AND ROWNUM = 1;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        ln_credit_analyst := NULL;
                  END;
            END;

            DEBUG (p_customer_number,
                   'ln_credit_analyst:' || ln_credit_analyst);



            lvc_sec := 'Primary Bill to Account Site Creation';

            pcustacctsiterec := NULL;
            x_ret_status := NULL;
            o_msg_count := NULL;
            o_msg_data := NULL;
            pcustacctsiterec.cust_account_id := p_cust_account_id;
            pcustacctsiterec.party_site_id := o_party_site_id;
            pcustacctsiterec.created_by_module := g_api_name;
            pcustacctsiterec.org_id := gn_org_id;
            pcustacctsiterec.attribute3 :=
               SUBSTR (NVL (rec_main.purchase_order_required, 'N'), 1, 1);
            pcustacctsiterec.attribute_category := 'No';
            pcustacctsiterec.attribute1 := rec_main.print_prices_on_order;
            pcustacctsiterec.attribute15 := 'None';
            pcustacctsiterec.attribute14 := 'N';
            pcustacctsiterec.attribute17 := p_customer_number;

            BEGIN
               SELECT CASE
                         WHEN customer_class_code = 'COMMERCIAL'
                         THEN
                            'PUBLIC'
                         WHEN customer_class_code = 'GOVERNMENT'
                         THEN
                            'PRIVATE'
                         WHEN customer_class_code = 'NON-CONTRACTOR/OTHER'
                         THEN
                            'PUBLIC'
                         WHEN customer_class_code = 'RESELLER'
                         THEN
                            'PUBLIC'
                         WHEN customer_class_code = 'RESIDENTIAL_CONTRACTOR'
                         THEN
                            'RES'
                         WHEN customer_class_code = 'UNKNOWN'
                         THEN
                            NULL
                         ELSE
                            NULL
                      END
                 INTO pcustacctsiterec.customer_category_code
                 FROM hz_cust_accounts
                WHERE cust_account_id = p_cust_account_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  pcustacctsiterec.customer_category_code := NULL;
            END;

            hz_cust_account_site_v2pub.create_cust_acct_site (
               p_init_msg_list        => 'T',
               p_cust_acct_site_rec   => pcustacctsiterec,
               x_cust_acct_site_id    => o_cust_acct_site_id,
               x_return_status        => x_ret_status,
               x_msg_count            => o_msg_count,
               x_msg_data             => o_msg_data);

            IF x_ret_status <> 'S'
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data :=
                  'Error:Primary Bill to Customer Account: ' || o_return_msg;
               debug (p_customer_number, x_msg_data);
               ROLLBACK TO customer_record;
               GOTO STATUS_UPDATE;
            END IF;

            lvc_sec :=
                  'Primary Cust Acct Site Creation Process Completed - API Status '
               || x_ret_status;
            debug (p_customer_number, lvc_sec);

            lvc_sec := 'Primary Bill to Account Site Use Creation';

            pcustacctsiteuserec := NULL;
            x_ret_status := NULL;
            o_msg_count := NULL;
            o_msg_data := NULL;

            pcustacctsiteuserec.site_use_code := 'BILL_TO';
            pcustacctsiteuserec.cust_acct_site_id := o_cust_acct_site_id;
            pcustacctsiteuserec.attribute1 := 'MSTR';
            pcustacctsiteuserec.primary_flag := 'Y';
            pcustacctsiteuserec.location :=
               SUBSTR (rec_main.job_site_name, 1, 35);
            pcustacctsiteuserec.created_by_module := g_api_name;
            pcustomerprofile.credit_checking := 'Y';
            pcustomerprofile.attribute2 := 2;
            pcustomerprofile.attribute3 := 'Y';
            pcustomerprofile.collector_id := ln_collector_id;
            pcustomerprofile.account_status := g_acct_status;
            pcustomerprofile.credit_rating := 'Y';
            pcustomerprofile.credit_hold := rec_main.credit_hold;
            pcustomerprofile.tolerance := 0;
            pcustomerprofile.credit_analyst_id := ln_credit_analyst;
            pcustomerprofile.credit_classification := 'MODERATE';
            pcustomerprofile.profile_class_id := ln_profile_class_id;
            pcustomerprofile.standard_terms := g_term_id;

            BEGIN
               SELECT A.SALESREP_ID
                 INTO pcustacctsiteuserec.primary_salesrep_id
                 FROM RA_SALESREPS_ALL A, XXWC.XXWC_AHH_AM_T B
                WHERE     A.ORG_ID = 162
                      AND UPPER (B.SLSREPOUT) =
                             UPPER (rec_main.OUTSIDE_SALES_REP_ID)
                      AND A.SALESREP_NUMBER = B.SALESREP_ID;
            EXCEPTION
               WHEN OTHERS
               THEN
                  BEGIN
                     SELECT A.SALESREP_ID
                       INTO pcustacctsiteuserec.primary_salesrep_id
                       FROM RA_SALESREPS_ALL A, XXWC.XXWC_AHH_AM_T B
                      WHERE     A.ORG_ID = 162
                            AND UPPER (TRIM (B.SLSREPOUT)) = 'DEFAULT'
                            AND A.SALESREP_NUMBER = B.SALESREP_ID
                            AND ROWNUM = 1;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        pcustacctsiteuserec.primary_salesrep_id := -3;
                  END;
            END;

            --IF rec_main.INSIDE_SALESREP_ID IS NOT NULL THEN -- ?????
            BEGIN
               SELECT A.SALESREP_ID
                 INTO pcustacctsiteuserec.attribute12
                 FROM RA_SALESREPS_ALL A, XXWC.XXWC_AHH_AM_T B
                WHERE     A.ORG_ID = 162
                      AND UPPER (B.SLSREPOUT) =
                             UPPER (rec_main.INSIDE_SALESREP_ID)
                      AND A.SALESREP_NUMBER = B.SALESREP_ID;
            EXCEPTION
               WHEN OTHERS
               THEN
                  pcustacctsiteuserec.attribute12 := -3;
            END;

            --END IF;

            --IF rec_main.FSD IS NOT NULL THEN -- ?????
            BEGIN
               SELECT A.SALESREP_ID
                 INTO pcustacctsiteuserec.attribute13
                 FROM RA_SALESREPS_ALL A, XXWC.XXWC_AHH_AM_T B
                WHERE     A.ORG_ID = 162
                      AND UPPER (B.SLSREPOUT) = UPPER (rec_main.FSD)
                      AND A.SALESREP_NUMBER = B.SALESREP_ID;
            EXCEPTION
               WHEN OTHERS
               THEN
                  pcustacctsiteuserec.attribute13 := -3;
            END;

            --END IF;

            hz_cust_account_site_v2pub.create_cust_site_use (
               p_init_msg_list          => 'T',
               p_cust_site_use_rec      => pcustacctsiteuserec,
               p_customer_profile_rec   => pcustomerprofile,
               p_create_profile         => 'T',
               p_create_profile_amt     => 'T',
               x_site_use_id            => o_cust_acct_site_use_id,
               x_return_status          => x_ret_status,
               x_msg_count              => o_msg_count,
               x_msg_data               => o_msg_data);

            IF x_ret_status <> 'S'
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data :=
                     'Error: Account Site Use Creation(Bill To): '
                  || o_return_msg;
               debug (p_customer_number, x_msg_data);
               ROLLBACK TO customer_record;
               GOTO STATUS_UPDATE;
            END IF;

            lvc_sec :=
                  'Cust Acct Site Use Creation Process Completed - API Status '
               || x_ret_status;

            debug (p_customer_number, lvc_sec);


            lvc_sec := 'Primary Bill to Site Profile Amounts update. ';


            pcustproamtrec := NULL;
            o_sprofile_id := NULL;
            x_ret_status := NULL;
            o_msg_count := NULL;
            o_msg_data := NULL;

            BEGIN
               SELECT cust_account_profile_id
                 INTO o_sprofile_id
                 FROM apps.hz_customer_profiles
                WHERE     cust_account_id = p_cust_account_id
                      AND site_use_id = o_cust_acct_site_use_id;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  o_sprofile_id := NULL;
            END;

            IF o_sprofile_id IS NOT NULL
            THEN
               BEGIN
                  SELECT cust_acct_profile_amt_id, object_version_number
                    INTO o_cust_acct_profile_amt_id, p_object_version_number
                    FROM apps.hz_cust_profile_amts
                   WHERE     cust_account_profile_id = o_sprofile_id
                         AND currency_code = g_currency_code;
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     o_cust_acct_profile_amt_id := NULL;
                     p_object_version_number := NULL;
               END;

               IF rec_main.credit_limit = 0
               THEN
                  pcustproamtrec.overall_credit_limit := 9999999.99;
               ELSIF rec_main.credit_limit = NULL
               THEN
                  pcustproamtrec.overall_credit_limit := NULL;
               ELSE
                  pcustproamtrec.overall_credit_limit := rec_main.credit_limit;
               END IF;

               /*
              IF NVL (rec_main.DEFAULT_JOB_CREDIT_LIMIT, 0) = 0
              THEN
                 pcustproamtrec.overall_credit_limit := NULL;
              ELSE
                 pcustproamtrec.overall_credit_limit :=
                    rec_main.DEFAULT_JOB_CREDIT_LIMIT;
              END IF;
              */

               IF o_cust_acct_profile_amt_id IS NOT NULL
               THEN
                  --Update amount profile
                  pcustproamtrec.cust_acct_profile_amt_id :=
                     o_cust_acct_profile_amt_id;
                  -- pcustproamtrec.created_by_module := l_api_name;  -- commented by Vamshi

                  hz_customer_profile_v2pub.update_cust_profile_amt (
                     p_init_msg_list           => 'T',
                     p_cust_profile_amt_rec    => pcustproamtrec,
                     p_object_version_number   => p_object_version_number,
                     x_return_status           => x_ret_status,
                     x_msg_count               => o_msg_count,
                     x_msg_data                => o_msg_data);
               ELSE
                  --Create Profile Amount
                  pcustproamtrec.cust_account_profile_id := o_sprofile_id;
                  pcustproamtrec.cust_account_id := p_cust_account_id;
                  pcustproamtrec.site_use_id := o_cust_acct_site_use_id;
                  pcustproamtrec.currency_code := g_currency_code;
                  pcustproamtrec.created_by_module := g_api_name;
                  hz_customer_profile_v2pub.create_cust_profile_amt (
                     p_init_msg_list              => 'T',
                     p_check_foreign_key          => 'T',
                     p_cust_profile_amt_rec       => pcustproamtrec,
                     x_cust_acct_profile_amt_id   => o_cust_acct_profile_amt_id,
                     x_return_status              => x_ret_status,
                     x_msg_count                  => o_msg_count,
                     x_msg_data                   => o_msg_data);
               END IF;

               IF x_ret_status <> 'S'
               THEN
                  return_msg (o_msg_count, o_msg_data, o_return_msg);
                  x_msg_data := 'Profile Amount Creation: ' || o_return_msg;
                  debug (p_customer_number, x_msg_data);
                  ROLLBACK TO customer_record;
                  GOTO STATUS_UPDATE;
               END IF;

               IF     NVL (o_cust_acct_profile_amt_id, 0) > 0
                  AND rec_main.credit_limit IS NULL
               THEN
                  BEGIN
                     UPDATE APPS.HZ_CUST_PROFILE_AMTS
                        SET OVERALL_CREDIT_LIMIT = NULL
                      WHERE CUST_ACCT_PROFILE_AMT_ID =
                               o_cust_acct_profile_amt_id;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        NULL;
                  END;
               END IF;

               lvc_sec :=
                     'Profile Amount Creation Process Completed - API Status '
                  || x_ret_status;
               debug (p_customer_number, lvc_sec);

               lvc_sec := 'Primary Bill to Usage Rules - API Status ';

               hz_credit_usages_cascade_pkg.cascade_credit_usage_rules (
                  p_cust_acct_profile_amt_id   => o_cust_acct_profile_amt_id,
                  p_cust_profile_id            => o_sprofile_id,
                  p_profile_class_amt_id       => NULL,
                  p_profile_class_id           => NULL,
                  x_return_status              => x_ret_status,
                  x_msg_count                  => o_msg_count,
                  x_msg_data                   => o_msg_data);

               IF x_ret_status <> 'S'
               THEN
                  return_msg (o_msg_count, o_msg_data, o_return_msg);
                  x_msg_data :=
                        'Primary Bill to Profile Amounts Usage Rules: '
                     || o_return_msg;
                  debug (p_customer_number, x_msg_data);
                  ROLLBACK TO customer_record;
                  GOTO STATUS_UPDATE;
               END IF;

               lvc_sec :=
                     'Primary Bill to Profile Amt uage rules Completed - API Status '
                  || x_ret_status;

               debug (p_customer_number, lvc_sec);
            END IF;

            lvc_sec := 'Party Contact person name Creation';

            IF     rec_main.ap_contact_last_name IS NOT NULL
               AND rec_main.ap_contact_first_name IS NOT NULL
            THEN
               ln_contact_party_id := NULL;
               p_create_person_rec := NULL;
               x_contact_party_id := NULL;
               x_contact_party_number := NULL;
               x_contact_profile_id := NULL;
               p_create_person_rec.person_first_name :=
                  rec_main.ap_contact_first_name;
               p_create_person_rec.person_last_name :=
                  rec_main.ap_contact_last_name;
               p_create_person_rec.created_by_module := g_api_name;
               hz_party_v2pub.create_person ('T',
                                             p_create_person_rec,
                                             ln_contact_party_id,
                                             x_contact_party_number,
                                             x_contact_profile_id,
                                             o_ret_status,
                                             o_msg_count,
                                             o_msg_data);

               IF o_ret_status <> 'S'
               THEN
                  return_msg (o_msg_count, o_msg_data, o_return_msg);
                  x_msg_data :=
                        'Contact Person creation (first and last names): '
                     || o_return_msg;
                  debug (p_customer_number, x_msg_data);
                  x_ret_status := o_ret_status;
                  ROLLBACK TO customer_record;
                  GOTO STATUS_UPDATE;
               END IF;


               lvc_sec :=
                     'Conact Person (Name) Completed - API Status '
                  || o_ret_status;
               debug (p_customer_number, lvc_sec);


               p_org_contact_rec := NULL;
               p_org_contact_rec.created_by_module := g_api_name;
               p_org_contact_rec.party_rel_rec.subject_id :=
                  ln_contact_party_id;
               p_org_contact_rec.party_rel_rec.subject_type := 'PERSON';
               p_org_contact_rec.party_rel_rec.subject_table_name :=
                  'HZ_PARTIES';
               p_org_contact_rec.party_rel_rec.object_id := p_party_id;
               p_org_contact_rec.party_rel_rec.object_type := 'ORGANIZATION';
               p_org_contact_rec.party_rel_rec.object_table_name :=
                  'HZ_PARTIES';
               p_org_contact_rec.party_rel_rec.relationship_code :=
                  'CONTACT_OF';
               p_org_contact_rec.party_rel_rec.relationship_type := 'CONTACT';
               p_org_contact_rec.party_rel_rec.start_date := SYSDATE;
               hz_party_contact_v2pub.create_org_contact ('T',
                                                          p_org_contact_rec,
                                                          x_org_contact_id,
                                                          x_party_rel_id,
                                                          x_rel_party_id,
                                                          x_rel_party_number,
                                                          o_ret_status,
                                                          o_msg_count,
                                                          o_msg_data);

               IF o_ret_status <> 'S'
               THEN
                  return_msg (o_msg_count, o_msg_data, o_return_msg);
                  x_msg_data :=
                     'Creating Org Contact (Name): ' || o_return_msg;
                  debug (p_customer_number, x_msg_data);
                  x_ret_status := o_ret_status;
                  ROLLBACK TO customer_record;
                  GOTO STATUS_UPDATE;
               END IF;

               lvc_sec :=
                     'Conact Person linking to Party is Completed - API Status '
                  || o_ret_status;
               debug (p_customer_number, lvc_sec);

               IF rec_main.ap_email IS NOT NULL
               THEN
                  lvc_sec := 'Creating AP Email for Contact Person '; -- Initializing the Mandatory API parameters
                  pcontactpointrec.contact_point_type := 'EMAIL';
                  pcontactpointrec.owner_table_name := 'HZ_PARTIES';
                  pcontactpointrec.owner_table_id := x_rel_party_id;
                  pcontactpointrec.primary_flag := 'Y';
                  pcontactpointrec.contact_point_purpose := 'BUSINESS';
                  pcontactpointrec.created_by_module := g_api_name;
                  pemailrec.email_format := 'MAILHTML';
                  pemailrec.email_address := rec_main.ap_email;

                  HZ_CONTACT_POINT_V2PUB.CREATE_CONTACT_POINT (
                     p_init_msg_list       => FND_API.G_TRUE,
                     p_contact_point_rec   => pcontactpointrec,
                     p_edi_rec             => pedirec,
                     p_email_rec           => pemailrec,
                     p_phone_rec           => pphonerec,
                     p_telex_rec           => ptelexrec,
                     p_web_rec             => pwebrec,
                     x_contact_point_id    => o_contact_point_id,
                     x_return_status       => o_ret_status,
                     x_msg_count           => o_msg_count,
                     x_msg_data            => o_msg_data);

                  IF o_ret_status <> 'S'
                  THEN
                     return_msg (o_msg_count, o_msg_data, o_return_msg);
                     x_msg_data :=
                           'Creating Email Contact (for Party): '
                        || o_return_msg;
                     debug (p_customer_number, x_msg_data);
                     x_ret_status := o_ret_status;
                     ROLLBACK TO customer_record;
                     GOTO STATUS_UPDATE;
                  END IF;
               END IF;

               lvc_sec :=
                     'Creating AP Email Contact Email Process Completed - API Status '
                  || o_ret_status;


               debug (p_customer_number, lvc_sec);

               lvc_sec := 'Create Party Site for Contact person';

               o_ret_status := NULL;
               o_msg_count := NULL;
               o_msg_data := NULL;
               o_party_site_id := NULL;
               o_party_site_no := NULL;
               o_sprofile_id := NULL;
               ppartysiterec.created_by_module := g_api_name;

               -- create a party site now
               ppartysiterec.party_id := x_rel_party_id;
               ppartysiterec.location_id := o_location_id;
               ppartysiterec.identifying_address_flag := 'Y';

               hz_party_site_v2pub.create_party_site (
                  p_init_msg_list       => 'T',
                  p_party_site_rec      => ppartysiterec,
                  x_party_site_id       => o_party_site_id,
                  x_party_site_number   => o_party_site_no,
                  x_return_status       => o_ret_status,
                  x_msg_count           => o_msg_count,
                  x_msg_data            => o_msg_data);


               IF o_ret_status <> 'S'
               THEN
                  return_msg (o_msg_count, o_msg_data, o_return_msg);
                  x_msg_data := 'Creating contact location: ' || o_return_msg;
                  debug (p_customer_number, x_msg_data);
                  x_ret_status := o_ret_status;
                  ROLLBACK TO customer_record;
                  GOTO STATUS_UPDATE;
               END IF;

               lvc_sec :=
                     'Creating contact location Process Completed - API Status '
                  || o_ret_status;


               debug (p_customer_number, lvc_sec);

               o_ret_status := NULL;
               o_msg_count := NULL;
               o_msg_data := NULL;
               p_cr_cust_acc_role_rec := NULL;
               p_cr_cust_acc_role_rec.party_id := x_rel_party_id;
               p_cr_cust_acc_role_rec.cust_account_id := p_cust_account_id;
               p_cr_cust_acc_role_rec.primary_flag := 'N';
               p_cr_cust_acc_role_rec.role_type := 'CONTACT';
               p_cr_cust_acc_role_rec.created_by_module := g_api_name;
               hz_cust_account_role_v2pub.create_cust_account_role (
                  'T',
                  p_cr_cust_acc_role_rec,
                  x_cust_account_role_id,
                  o_ret_status,
                  o_msg_count,
                  o_msg_data);

               IF o_ret_status <> 'S'
               THEN
                  return_msg (o_msg_count, o_msg_data, o_return_msg);
                  x_msg_data := 'Creating Account role: ' || o_return_msg;
                  debug (p_customer_number, x_msg_data);
                  x_ret_status := o_ret_status;
                  ROLLBACK TO customer_record;
                  RETURN;
               END IF;

               lvc_sec :=
                     'Creating Account role Process Completed - API Status '
                  || o_ret_status;


               debug (p_customer_number, lvc_sec);


               o_ret_status := NULL;
               o_msg_count := NULL;
               o_msg_data := NULL;
               p_ROLE_RESPONSIBILITY_REC_TYPE := NULL;
               p_ROLE_RESPONSIBILITY_REC_TYPE.cust_account_role_id :=
                  x_cust_account_role_id;
               p_ROLE_RESPONSIBILITY_REC_TYPE.responsibility_type := 'ACC_PAY';

               p_ROLE_RESPONSIBILITY_REC_TYPE.created_by_module := g_api_name;

               hz_cust_account_role_v2pub.create_role_responsibility (
                  p_init_msg_list             => 'T',
                  p_role_responsibility_rec   => p_ROLE_RESPONSIBILITY_REC_TYPE,
                  x_responsibility_id         => o_resp_id,
                  x_return_status             => o_ret_status,
                  x_msg_count                 => o_msg_count,
                  x_msg_data                  => o_msg_data);

               IF o_ret_status <> 'S'
               THEN
                  return_msg (o_msg_count, o_msg_data, o_return_msg);
                  x_msg_data := 'Assing AP role: ' || o_return_msg;
                  debug (p_customer_number, x_msg_data);
                  x_ret_status := o_ret_status;
                  ROLLBACK TO customer_record;
                  GOTO STATUS_UPDATE;
               END IF;

               lvc_sec :=
                     'Assing AP role Process Completed - API Status '
                  || o_ret_status;

               debug (p_customer_number, lvc_sec);
            END IF;


           <<STATUS_UPDATE>>
            IF NVL (x_ret_status, 'S') = 'S'
            THEN
               UPDATE XXWC.XXWC_AR_CUST_SITE_IFACE_STG
                  SET PROCESS_STATUS = 'Y'
                --ORACLE_CUSTOMER_NUMBER = g_cust_acct_no
                WHERE ROWID = rec_main.row_id;

               g_primbillto_exists := 'Y';
            ELSE
               g_primbillto_exists := 'N';

               UPDATE XXWC.XXWC_AR_CUST_SITE_IFACE_STG
                  SET PROCESS_STATUS = 'E', PROCESS_ERROR = x_msg_data
                WHERE ROWID = rec_main.row_id;
            END IF;

            COMMIT;
         EXCEPTION
            WHEN OTHERS
            THEN
               x_msg_data := SQLERRM;
               debug (p_customer_number, x_msg_data);

               UPDATE xxwc.XXWC_AR_CUST_SITE_IFACE_STG
                  SET process_STATUS = 'E',
                      PROCESS_error = PROCESS_error || ' ' || x_msg_data
                WHERE 1 = 1        --ORIG_SYSTEM_REFERENCE = p_customer_number
                           AND ROWID = rec_main.row_id;

               xxcus_error_pkg.xxcus_error_main_api (
                  p_called_from         => g_err_callfrom || '.' || lvc_procedure,
                  p_calling             => lvc_sec,
                  p_ora_error_msg       => SQLERRM,
                  p_error_desc          =>    'Error Occured for '
                                           || p_customer_number
                                           || 'record id in XXWC_AR_CUST_SITE_IFACE_STG.',
                  p_distribution_list   => g_distro_list,
                  p_module              => g_module);
         END;
      END LOOP;
   END;

   PROCEDURE primary_ship_to (p_customer_number   IN VARCHAR2,
                              p_cust_account_id   IN NUMBER,
                              p_party_id          IN NUMBER)
   IS
      -- ==============================================================================================================================
      -- Procedure: Primary_ship_to
      -- Purpose: To customer primary ship to
      -- ==============================================================================================================================
      --  REVISIONS:
      --   Ver        Date        Author               Description
      --  ---------  ---------- ---------------  --------------------------------------------------------------------------------------
      --  1.0        06/06/2018  P.Vamshidhar    AH HARRIS Customer Conversion
      -- ==============================================================================================================================

      CURSOR cur_cust
      IS
         SELECT xaca.ORIG_SYSTEM_REFERENCE RECORD_ID,
                xaca.ORIG_SYSTEM_REFERENCE CUSTOMER_NUMBER,
                UPPER (site.LEGACY_PARTY_SITE_NUMBER) CUSTOMER_SITE_NUMBER,
                xaca.ACCOUNT_NAME BUSINESS_NAME,
                NULL BUSINESS_PHONE,
                NULL BUSINESS_FAX,
                NULL EMAIL_ADDRESS,
                site.PRIMARY_SALESREP_NUMBER OUTSIDE_SALES_REP_ID,
                site.CREDIT_ANALYST_NAME credit_manager,
                SUBSTR (site.CREDIT_HOLD, 1, 1) CREDIT_HOLD,
                'UNKNOWN' PREDOMINANT_TRADE,
                xaca.STATUS STATUS,
                site.OVERALL_CREDIT_LIMIT DEFAULT_JOB_CREDIT_LIMIT,
                site.ADDRESS1 SHIPPING_ADDRESS1,
                site.ADDRESS2 SHIPPING_ADDRESS2,
                site.CITY SHIPPING_CITY,
                site.COUNTY SHIPPING_COUNTY,
                site.STATE SHIPPING_STATE,
                NVL (site.COUNTRY, 'US') SHIPPING_COUNTRY,
                site.POSTAL_CODE SHIPPING_ZIP_CODE,
                NULL AP_CONTACT_FIRST_NAME,
                NULL AP_CONTACT_LAST_NAME,
                NULL AP_PHONE,
                NULL AP_EMAIL,
                NULL INVOICE_EMAIL_ADDRESS,
                NULL DUNSNUMBER,
                SUBSTR (site.LEGACY_PARTY_SITE_NAME, 1, 33) || '/Yard'
                   JOB_SITE_NAME,
                'Y' purchase_order_required,
                '2' print_prices_on_order,
                'None' TAX_EXEMPTION,
                'YARD' yard_job_accnt_project,
                xaca.standard_term_name,
                site.inside_salesrep_id,
                site.fsd,
                site.ROWID ROW_ID
           FROM XXWC.XXWC_AR_CUSTOMER_IFACE_STG xaca,
                XXWC.XXWC_AR_CUST_SITE_IFACE_STG site
          WHERE     xaca.ORIG_SYSTEM_REFERENCE = site.LEGACY_CUSTOMER_NUMBER
                AND site.LEGACY_PARTY_SITE_NUMBER IS NULL
                AND xaca.PROCESS_status = 'Y'
                --AND site.PROCESS_STATUS = 'V'
                AND xaca.ORIG_SYSTEM_REFERENCE = P_CUSTOMER_NUMBER;


      lvc_row_id                   VARCHAR2 (100);
      lvc_sec                      VARCHAR2 (32767);
      plocationrec                 hz_location_v2pub.location_rec_type;
      ppartysiterec                hz_party_site_v2pub.party_site_rec_type;
      pcustacctsiterec             hz_cust_account_site_v2pub.cust_acct_site_rec_type;
      pcustacctsiteuserec          hz_cust_account_site_v2pub.cust_site_use_rec_type;
      pcustomerprofile             hz_customer_profile_v2pub.customer_profile_rec_type;
      pcustproamtrec               hz_customer_profile_v2pub.cust_profile_amt_rec_type;
      o_msg_count                  NUMBER;
      o_return_msg                 VARCHAR2 (4000);
      o_msg_data                   VARCHAR2 (4000) := NULL;
      o_location_id                HZ_LOCATIONS.LOCATION_ID%TYPE;
      o_party_site_id              NUMBER;
      o_bill_to_site_use_id        NUMBER;
      o_party_site_no              VARCHAR2 (100);
      x_msg_data                   VARCHAR2 (32767);
      x_ret_status                 VARCHAR2 (1);
      o_ret_status                 VARCHAR2 (1);
      lvc_procedure                VARCHAR2 (20) := 'PRIMARY_SHIP_TO';
      ln_profile_class_id          NUMBER;
      o_cust_acct_site_id          NUMBER;
      o_cust_acct_site_use_id      NUMBER;
      o_sprofile_id                NUMBER;
      o_cust_acct_profile_amt_id   NUMBER;
      p_object_version_number      NUMBER;
      ln_collector_id              NUMBER;
      ln_credit_analyst            NUMBER;
   BEGIN
      lvc_sec := 'Primary Ship TO Procedure Start';
      debug (p_customer_number, lvc_sec);
      SAVEPOINT customer_record;

      FOR REC_CUST IN cur_cust
      LOOP
         BEGIN
            lvc_sec := 'Ship TO Loop Inside ';

            BEGIN
               SELECT territory_code
                 INTO plocationrec.country
                 FROM hr_territories_v
                WHERE (   UPPER (TERRITORY_SHORT_NAME) =
                             UPPER (rec_cust.SHIPPING_COUNTRY)
                       OR UPPER (TERRITORY_CODE) =
                             UPPER (rec_cust.SHIPPING_COUNTRY));
            EXCEPTION
               WHEN OTHERS
               THEN
                  plocationrec.country := NULL;
                  return_msg (o_msg_count, o_msg_data, o_return_msg);
                  x_msg_data := 'Invalid Shipping Country ' || o_return_msg;
                  debug (p_customer_number, x_msg_data);
                  x_ret_status := o_ret_status;
                  ROLLBACK TO customer_record;
                  GOTO STATUS_UPDATE;
            END;

            lvc_sec := 'Primary Ship TO Location Creation';
            debug (p_customer_number, lvc_sec);

            plocationrec.postal_code := rec_cust.shipping_zip_code;
            plocationrec.address1 := rec_cust.shipping_address1;
            plocationrec.address2 := rec_cust.shipping_address2;
            plocationrec.state := rec_cust.shipping_STATE;
            plocationrec.city := rec_cust.shipping_CITY;
            plocationrec.county := rec_cust.shipping_COUNTY;
            plocationrec.created_by_module := g_api_name;
            plocationrec.sales_tax_geocode := NULL;

            hz_location_v2pub.create_location (
               p_init_msg_list   => 'T',
               p_location_rec    => plocationrec,
               x_location_id     => o_location_id,
               x_return_status   => o_ret_status,
               x_msg_count       => o_msg_count,
               x_msg_data        => o_msg_data);

            IF o_ret_status <> 'S'
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data := 'Ship to Location Creation: ' || o_return_msg;
               debug (p_customer_number, x_msg_data);
               x_ret_status := o_ret_status;
               ROLLBACK TO customer_record;
               GOTO STATUS_UPDATE;
            END IF;

            lvc_sec :=
                  'Location for Ship to Creation Process Completed - API Status '
               || o_ret_status;

            debug (p_customer_number, lvc_sec);


            lvc_sec := 'Prmary Ship to Party Site Creation';

            o_ret_status := NULL;
            o_msg_count := NULL;
            o_msg_data := NULL;
            ppartysiterec.created_by_module := g_api_name;
            ppartysiterec.party_id := p_party_id;
            ppartysiterec.location_id := o_location_id;
            --ppartysiterec.identifying_address_flag := 'Y';

            hz_party_site_v2pub.create_party_site (
               p_init_msg_list       => 'T',
               p_party_site_rec      => ppartysiterec,
               x_party_site_id       => o_party_site_id,
               x_party_site_number   => o_party_site_no,
               x_return_status       => o_ret_status,
               x_msg_count           => o_msg_count,
               x_msg_data            => o_msg_data);

            IF o_ret_status <> 'S'
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data :=
                  'Primary Ship to Party Site Creation: ' || o_return_msg;
               debug (p_customer_number, x_msg_data);
               x_ret_status := o_ret_status;
               ROLLBACK TO customer_record;
               GOTO STATUS_UPDATE;
            END IF;

            lvc_sec :=
                  'Party Site for Ship to Creation Process Completed - API Status '
               || o_ret_status;
            debug (p_customer_number, lvc_sec);

            lvc_sec := 'Prmary Ship to Account Site Creation';

            o_ret_status := NULL;
            o_msg_count := NULL;
            o_msg_data := NULL;
            pcustacctsiterec.cust_account_id := p_cust_account_id;
            pcustacctsiterec.party_site_id := o_party_site_id;
            pcustacctsiterec.created_by_module := g_api_name;
            pcustacctsiterec.org_id := gn_org_id;
            pcustacctsiterec.attribute3 :=
               SUBSTR (NVL (rec_cust.purchase_order_required, 'N'), 1, 1);
            pcustacctsiterec.attribute_category := 'No';
            pcustacctsiterec.attribute1 := rec_cust.print_prices_on_order;
            pcustacctsiterec.attribute15 := 'None';
            pcustacctsiterec.attribute14 := 'N';

            --            pcustacctsiterec.attribute17 :=
            --                  rec_cust.customer_number
            --               || '-'
            --               || rec_cust.customer_site_number;

            BEGIN
               SELECT CASE
                         WHEN customer_class_code = 'COMMERCIAL'
                         THEN
                            'PUBLIC'
                         WHEN customer_class_code = 'GOVERNMENT'
                         THEN
                            'PRIVATE'
                         WHEN customer_class_code = 'NON-CONTRACTOR/OTHER'
                         THEN
                            'PUBLIC'
                         WHEN customer_class_code = 'RESELLER'
                         THEN
                            'PUBLIC'
                         WHEN customer_class_code = 'RESIDENTIAL_CONTRACTOR'
                         THEN
                            'RES'
                         WHEN customer_class_code = 'UNKNOWN'
                         THEN
                            NULL
                         ELSE
                            NULL
                      END
                 INTO pcustacctsiterec.customer_category_code
                 FROM hz_cust_accounts
                WHERE cust_account_id = p_cust_account_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  pcustacctsiterec.customer_category_code := NULL;
            END;

            hz_cust_account_site_v2pub.create_cust_acct_site (
               p_init_msg_list        => 'T',
               p_cust_acct_site_rec   => pcustacctsiterec,
               x_cust_acct_site_id    => o_cust_acct_site_id,
               x_return_status        => o_ret_status,
               x_msg_count            => o_msg_count,
               x_msg_data             => o_msg_data);

            IF o_ret_status <> 'S'
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data := 'Error: Primary Ship to Site : ' || o_return_msg;
               debug (p_customer_number, x_msg_data);
               x_ret_status := o_ret_status;
               ROLLBACK TO customer_record;
               GOTO STATUS_UPDATE;
            END IF;

            lvc_sec :=
                  'Cust Acct Site for Ship to Creation Process Completed - API Status '
               || o_ret_status;

            debug (p_customer_number, lvc_sec);

            lvc_sec :=
               'Cust Acct Site Use for Ship to Creation Process Creation';
            o_ret_status := NULL;
            o_msg_count := NULL;
            o_msg_data := NULL;
            pcustacctsiteuserec.site_use_code := 'BILL_TO';
            pcustacctsiteuserec.cust_acct_site_id := o_cust_acct_site_id;
            pcustacctsiteuserec.created_by_module := g_api_name;
            pcustacctsiteuserec.attribute1 := 'YARD';


            pcustacctsiteuserec.location := rec_cust.job_site_name;

            IF rec_cust.OUTSIDE_SALES_REP_ID IS NOT NULL
            THEN
               BEGIN
                  SELECT RSA.SALESREP_ID
                    INTO pcustacctsiteuserec.primary_salesrep_id
                    FROM XXWC.XXWC_AHH_AM_T XAAT, APPS.RA_SALESREPS_ALL RSA
                   WHERE     XAAT.SALESREP_ID = RSA.SALESREP_NUMBER
                         AND UPPER (TRIM (XAAT.SLSREPOUT)) =
                                UPPER (TRIM (rec_cust.OUTSIDE_SALES_REP_ID))
                         AND RSA.ORG_ID = 162
                         AND ROWNUM = 1;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     BEGIN
                        SELECT A.SALESREP_ID
                          INTO pcustacctsiteuserec.primary_salesrep_id
                          FROM RA_SALESREPS_ALL A, XXWC.XXWC_AHH_AM_T B
                         WHERE     A.ORG_ID = 162
                               AND UPPER (TRIM (B.SLSREPOUT)) = 'DEFAULT'
                               AND A.SALESREP_NUMBER = B.SALESREP_ID
                               AND ROWNUM = 1;
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                           pcustacctsiteuserec.primary_salesrep_id := -3;
                     END;
               END;
            END IF;

            -- IF rec_cust.INSIDE_SALESREP_ID IS NOT NULL             THEN
            BEGIN
               SELECT A.SALESREP_ID
                 INTO pcustacctsiteuserec.attribute12
                 FROM RA_SALESREPS_ALL A, XXWC.XXWC_AHH_AM_T B
                WHERE     A.ORG_ID = 162
                      AND UPPER (B.SLSREPOUT) =
                             UPPER (rec_cust.INSIDE_SALESREP_ID)
                      AND A.SALESREP_NUMBER = B.SALESREP_ID;
            EXCEPTION
               WHEN OTHERS
               THEN
                  pcustacctsiteuserec.attribute12 := -3;
            END;

            -- END IF;

            -- IF rec_cust.FSD IS NOT NULL             THEN
            BEGIN
               SELECT A.SALESREP_ID
                 INTO pcustacctsiteuserec.attribute13
                 FROM RA_SALESREPS_ALL A, XXWC.XXWC_AHH_AM_T B
                WHERE     A.ORG_ID = 162
                      AND UPPER (B.SLSREPOUT) = UPPER (rec_cust.FSD)
                      AND A.SALESREP_NUMBER = B.SALESREP_ID;
            EXCEPTION
               WHEN OTHERS
               THEN
                  pcustacctsiteuserec.attribute13 := -3;
            END;

            --  END IF;

            -- Deriving Collector
            BEGIN
               SELECT ORA_COLL_ID
                 INTO ln_collector_id
                 FROM XXWC.XXWC_AHH_CRD_COLL_ANALYST_T b
                WHERE     UPPER (TRIM (b.ahh_coll_code)) =
                             UPPER (rec_cust.credit_manager)
                      AND ROWNUM = 1;
            EXCEPTION
               WHEN OTHERS
               THEN
                  BEGIN
                     SELECT ORA_COLL_ID
                       INTO ln_collector_id
                       FROM XXWC.XXWC_AHH_CRD_COLL_ANALYST_T b
                      WHERE     UPPER (TRIM (b.ahh_coll_code)) = 'DEFAULT'
                            AND ROWNUM = 1;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        ln_collector_id := 1;             -- Default Collector
                  END;
            END;

            DEBUG (p_customer_number, 'ln_collector_id:' || ln_collector_id);

            BEGIN
               SELECT ORA_CRD_ANLST_ID
                 INTO ln_credit_analyst
                 FROM XXWC.XXWC_AHH_CRD_COLL_ANALYST_T b
                WHERE     UPPER (TRIM (b.ahh_coll_code)) =
                             UPPER (rec_cust.credit_manager)
                      AND ROWNUM = 1;
            EXCEPTION
               WHEN OTHERS
               THEN
                  BEGIN
                     SELECT ORA_CRD_ANLST_ID
                       INTO ln_credit_analyst
                       FROM XXWC.XXWC_AHH_CRD_COLL_ANALYST_T b
                      WHERE     UPPER (TRIM (b.ahh_coll_code)) = 'DEFAULT'
                            AND ROWNUM = 1;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        ln_credit_analyst := NULL;
                  END;
            END;

            DEBUG (p_customer_number,
                   'ln_credit_analyst:' || ln_credit_analyst);


            pcustomerprofile.credit_checking := 'Y';
            pcustomerprofile.attribute2 := 2;
            pcustomerprofile.attribute3 := 'Y';
            pcustomerprofile.collector_id := ln_collector_id;
            pcustomerprofile.credit_analyst_id := ln_credit_analyst;
            pcustomerprofile.credit_rating := 'Y';
            pcustomerprofile.account_status := g_acct_status;
            pcustomerprofile.tolerance := 0;
            pcustomerprofile.credit_classification := 'MODERATE';
            pcustomerprofile.standard_terms := g_term_id;


            IF rec_cust.standard_term_name = 'COD'
            THEN
               SELECT PROFILE_CLASS_ID
                 INTO pcustomerprofile.profile_class_id
                 FROM HZ_CUST_PROFILE_CLASSES
                WHERE NAME = 'COD Customers';
            ELSE
               SELECT PROFILE_CLASS_ID
                 INTO pcustomerprofile.profile_class_id
                 FROM HZ_CUST_PROFILE_CLASSES
                WHERE NAME = 'Contractor - Non Key';
            END IF;

            hz_cust_account_site_v2pub.create_cust_site_use (
               p_init_msg_list          => 'T',
               p_cust_site_use_rec      => pcustacctsiteuserec,
               p_customer_profile_rec   => pcustomerprofile,
               p_create_profile         => 'T',
               p_create_profile_amt     => 'T',
               x_site_use_id            => o_bill_to_site_use_id,
               x_return_status          => o_ret_status,
               x_msg_count              => o_msg_count,
               x_msg_data               => o_msg_data);

            g_bill_to_site_use_id := o_bill_to_site_use_id;

            IF o_ret_status <> 'S'
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data :=
                  'Primary Ship TO Site Creation(BillTo1): ' || o_return_msg;
               debug (p_customer_number, x_msg_data);
               x_ret_status := o_ret_status;
               ROLLBACK TO customer_record;
               GOTO STATUS_UPDATE;
            END IF;

            lvc_sec :=
                  'Cust Acct Site Use (Billto) for Ship to Creation Process Completed - API Status '
               || o_ret_status;

            debug (p_customer_number, lvc_sec);


            --====================================================================
            --Create ShipTo site profile amount
            --====================================================================
            o_sprofile_id := NULL;
            o_cust_acct_profile_amt_id := NULL;

            BEGIN
               SELECT cust_account_profile_id
                 INTO o_sprofile_id
                 FROM apps.hz_customer_profiles
                WHERE     cust_account_id = p_cust_account_id
                      AND site_use_id = o_bill_to_site_use_id;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  o_sprofile_id := NULL;
            END;

            IF o_sprofile_id IS NOT NULL
            THEN
               BEGIN
                  SELECT cust_acct_profile_amt_id, object_version_number
                    INTO o_cust_acct_profile_amt_id, p_object_version_number
                    FROM apps.hz_cust_profile_amts
                   WHERE     cust_account_profile_id = o_sprofile_id
                         AND currency_code = g_currency_code;
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     o_cust_acct_profile_amt_id := NULL;
                     p_object_version_number := NULL;
               END;

               IF NVL (rec_cust.default_job_credit_limit, 0) = 0
               THEN
                  pcustproamtrec.overall_credit_limit := NULL;
               ELSE
                  pcustproamtrec.overall_credit_limit :=
                     rec_cust.default_job_credit_limit;
               END IF;

               IF o_cust_acct_profile_amt_id IS NOT NULL
               THEN
                  --Update amount profile
                  pcustproamtrec.cust_acct_profile_amt_id :=
                     o_cust_acct_profile_amt_id;
                  hz_customer_profile_v2pub.update_cust_profile_amt (
                     p_init_msg_list           => 'T',
                     p_cust_profile_amt_rec    => pcustproamtrec,
                     p_object_version_number   => p_object_version_number,
                     x_return_status           => o_ret_status,
                     x_msg_count               => o_msg_count,
                     x_msg_data                => o_msg_data);
               ELSE
                  --Create Profile Amount
                  pcustproamtrec.cust_account_profile_id := o_sprofile_id;
                  pcustproamtrec.cust_account_id := p_cust_account_id;
                  pcustproamtrec.site_use_id := o_cust_acct_site_use_id;
                  pcustproamtrec.currency_code := g_currency_code;
                  pcustproamtrec.created_by_module := g_api_name;
                  hz_customer_profile_v2pub.create_cust_profile_amt (
                     p_init_msg_list              => 'T',
                     p_check_foreign_key          => 'T',
                     p_cust_profile_amt_rec       => pcustproamtrec,
                     x_cust_acct_profile_amt_id   => o_cust_acct_profile_amt_id,
                     x_return_status              => o_ret_status,
                     x_msg_count                  => o_msg_count,
                     x_msg_data                   => o_msg_data);
               END IF;

               IF o_ret_status <> 'S'
               THEN
                  return_msg (o_msg_count, o_msg_data, o_return_msg);
                  x_msg_data :=
                     'Shipto Profile Amount Creation: ' || o_return_msg;
                  debug (p_customer_number, x_msg_data);
                  x_ret_status := o_ret_status;
                  ROLLBACK TO customer_record;
                  GOTO STATUS_UPDATE;
               END IF;

               IF     NVL (o_cust_acct_profile_amt_id, 0) > 0
                  AND NVL (rec_cust.DEFAULT_JOB_CREDIT_LIMIT, 0) = 0
               THEN
                  BEGIN
                     UPDATE APPS.HZ_CUST_PROFILE_AMTS
                        SET OVERALL_CREDIT_LIMIT = NULL
                      WHERE CUST_ACCT_PROFILE_AMT_ID =
                               o_cust_acct_profile_amt_id;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        NULL;
                  END;
               END IF;
            END IF;

            --====================================================================
            -- Create site use for Ship to
            --====================================================================
            pcustacctsiteuserec := NULL;
            o_ret_status := NULL;
            o_msg_count := NULL;
            o_msg_data := NULL;

            pcustacctsiteuserec.site_use_code := 'SHIP_TO';
            pcustacctsiteuserec.cust_acct_site_id := o_cust_acct_site_id;

            BEGIN
               SELECT inv_remit_to_code
                 INTO pcustomerprofile.attribute2
                 FROM xxwc_ar_cm_remitcode_tbl
                WHERE area_code = SUBSTR (rec_cust.business_phone, 1, 3);
            EXCEPTION
               WHEN OTHERS
               THEN
                  pcustomerprofile.attribute2 := 2;
            END;

            pcustomerprofile.attribute3 := 'Y';
            pcustacctsiteuserec.bill_to_site_use_id := g_bill_to_site_use_id;

            pcustacctsiteuserec.primary_flag := 'Y';
            pcustacctsiteuserec.created_by_module := g_api_name;



            IF rec_cust.OUTSIDE_SALES_REP_ID IS NOT NULL
            THEN
               BEGIN
                  SELECT A.SALESREP_ID
                    INTO pcustacctsiteuserec.primary_salesrep_id
                    FROM RA_SALESREPS_ALL A, XXWC.XXWC_AHH_AM_T B
                   WHERE     A.ORG_ID = 162
                         AND UPPER (TRIM (B.SLSREPOUT)) =
                                UPPER (TRIM (rec_cust.OUTSIDE_SALES_REP_ID))
                         AND A.SALESREP_NUMBER = B.SALESREP_ID
                         AND ROWNUM = 1;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     BEGIN
                        SELECT A.SALESREP_ID
                          INTO pcustacctsiteuserec.primary_salesrep_id
                          FROM RA_SALESREPS_ALL A, XXWC.XXWC_AHH_AM_T B
                         WHERE     A.ORG_ID = 162
                               AND UPPER (TRIM (B.SLSREPOUT)) = 'DEFAULT'
                               AND A.SALESREP_NUMBER = B.SALESREP_ID
                               AND ROWNUM = 1;
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                           pcustacctsiteuserec.primary_salesrep_id := -3;
                     END;
               END;
            END IF;

            --IF rec_cust.INSIDE_SALESREP_ID IS NOT NULL             THEN
            BEGIN
               SELECT A.SALESREP_ID
                 INTO pcustacctsiteuserec.attribute12
                 FROM RA_SALESREPS_ALL A, XXWC.XXWC_AHH_AM_T B
                WHERE     A.ORG_ID = 162
                      AND UPPER (B.SLSREPOUT) =
                             UPPER (rec_cust.INSIDE_SALESREP_ID)
                      AND A.SALESREP_NUMBER = B.SALESREP_ID;
            EXCEPTION
               WHEN OTHERS
               THEN
                  pcustacctsiteuserec.attribute12 := -3;
            END;

            -- END IF;

            -- IF rec_cust.FSD IS NOT NULL             THEN
            BEGIN
               SELECT A.SALESREP_ID
                 INTO pcustacctsiteuserec.attribute13
                 FROM RA_SALESREPS_ALL A, XXWC.XXWC_AHH_AM_T B
                WHERE     A.ORG_ID = 162
                      AND UPPER (B.SLSREPOUT) = UPPER (rec_cust.FSD)
                      AND A.SALESREP_NUMBER = B.SALESREP_ID;
            EXCEPTION
               WHEN OTHERS
               THEN
                  pcustacctsiteuserec.attribute13 := -3;
            END;

            -- END IF;

            pcustomerprofile.credit_checking := 'Y';
            pcustomerprofile.collector_id := ln_collector_id;
            pcustomerprofile.credit_analyst_id := ln_credit_analyst;
            pcustomerprofile.account_status := g_acct_status;
            pcustomerprofile.tolerance := 0;
            pcustomerprofile.standard_terms := g_term_id;
            pcustacctsiteuserec.location := rec_cust.job_site_name;
            pcustacctsiteuserec.attribute1 := rec_cust.yard_job_accnt_project;
            pcustomerprofile.credit_classification := 'MODERATE';
            pcustomerprofile.credit_hold := rec_cust.credit_hold;

            IF rec_cust.standard_term_name = 'COD'
            THEN
               SELECT PROFILE_CLASS_ID
                 INTO pcustomerprofile.profile_class_id
                 FROM HZ_CUST_PROFILE_CLASSES
                WHERE NAME = 'COD Customers';
            ELSE
               SELECT PROFILE_CLASS_ID
                 INTO pcustomerprofile.profile_class_id
                 FROM HZ_CUST_PROFILE_CLASSES
                WHERE NAME = 'Contractor - Non Key';
            END IF;

            hz_cust_account_site_v2pub.create_cust_site_use (
               p_init_msg_list          => 'T',
               p_cust_site_use_rec      => pcustacctsiteuserec,
               p_customer_profile_rec   => pcustomerprofile,
               p_create_profile         => 'T',
               p_create_profile_amt     => 'T',
               x_site_use_id            => o_cust_acct_site_use_id,
               x_return_status          => o_ret_status,
               x_msg_count              => o_msg_count,
               x_msg_data               => o_msg_data);

            IF o_ret_status <> 'S'
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data := 'Account Site Creation(SHIPTO): ' || o_return_msg;
               debug (p_customer_number, x_msg_data);
               x_ret_status := o_ret_status;
               ROLLBACK TO customer_record;
               GOTO STATUS_UPDATE;
            END IF;

            lvc_sec :=
                  'Cust Acct Site Use for Ship to Creation Process Completed - API Status '
               || o_ret_status;
            debug (p_customer_number, lvc_sec);

            --====================================================================
            -- Create ShipTo site profile amount
            --====================================================================
            pcustproamtrec := NULL;
            o_sprofile_id := NULL;
            o_cust_acct_profile_amt_id := NULL;

            BEGIN
               SELECT cust_account_profile_id
                 INTO o_sprofile_id
                 FROM apps.hz_customer_profiles
                WHERE     cust_account_id = p_cust_account_id
                      AND site_use_id = o_cust_acct_site_use_id;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  o_sprofile_id := NULL;
            END;

            IF o_sprofile_id IS NOT NULL
            THEN
               BEGIN
                  SELECT cust_acct_profile_amt_id, object_version_number
                    INTO o_cust_acct_profile_amt_id, p_object_version_number
                    FROM apps.hz_cust_profile_amts
                   WHERE     cust_account_profile_id = o_sprofile_id
                         AND currency_code = g_currency_code;
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     o_cust_acct_profile_amt_id := NULL;
                     p_object_version_number := NULL;
               END;


               IF NVL (rec_cust.default_job_credit_limit, 0) = 0
               THEN
                  pcustproamtrec.overall_credit_limit := NULL;
               ELSE
                  pcustproamtrec.overall_credit_limit :=
                     rec_cust.default_job_credit_limit;
               END IF;

               IF o_cust_acct_profile_amt_id IS NOT NULL
               THEN
                  --Update amount profile
                  pcustproamtrec.cust_acct_profile_amt_id :=
                     o_cust_acct_profile_amt_id;
                  hz_customer_profile_v2pub.update_cust_profile_amt (
                     p_init_msg_list           => 'T',
                     p_cust_profile_amt_rec    => pcustproamtrec,
                     p_object_version_number   => p_object_version_number,
                     x_return_status           => o_ret_status,
                     x_msg_count               => o_msg_count,
                     x_msg_data                => o_msg_data);
               ELSE
                  --Create Profile Amount
                  pcustproamtrec.cust_account_profile_id := o_sprofile_id;
                  pcustproamtrec.cust_account_id := p_cust_account_id;
                  pcustproamtrec.site_use_id := o_cust_acct_site_use_id;
                  pcustproamtrec.currency_code := g_currency_code;
                  pcustproamtrec.created_by_module := g_api_name;
                  hz_customer_profile_v2pub.create_cust_profile_amt (
                     p_init_msg_list              => 'T',
                     p_check_foreign_key          => 'T',
                     p_cust_profile_amt_rec       => pcustproamtrec,
                     x_cust_acct_profile_amt_id   => o_cust_acct_profile_amt_id,
                     x_return_status              => o_ret_status,
                     x_msg_count                  => o_msg_count,
                     x_msg_data                   => o_msg_data);
               END IF;

               IF o_ret_status <> 'S'
               THEN
                  return_msg (o_msg_count, o_msg_data, o_return_msg);
                  x_msg_data :=
                     'Shipto Profile Amount Creation: ' || o_return_msg;
                  debug (p_customer_number, x_msg_data);
                  x_ret_status := o_ret_status;
                  ROLLBACK TO customer_record;
                  GOTO STATUS_UPDATE;
               END IF;

               IF     NVL (o_cust_acct_profile_amt_id, 0) > 0
                  AND NVL (rec_cust.default_job_credit_limit, 0) = 0
               THEN
                  BEGIN
                     UPDATE APPS.HZ_CUST_PROFILE_AMTS
                        SET OVERALL_CREDIT_LIMIT = NULL
                      WHERE CUST_ACCT_PROFILE_AMT_ID =
                               o_cust_acct_profile_amt_id;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        NULL;
                  END;
               END IF;
            END IF;

            lvc_sec :=
                  'Profile Amts for Ship to Creation Process Completed - API Status '
               || o_ret_status;
            debug (p_customer_number, lvc_sec);

            lvc_sec := ' Primary Ship TO usage rules';

            o_msg_count := NULL;
            o_msg_data := NULL;
            o_ret_status := NULL;

            hz_credit_usages_cascade_pkg.cascade_credit_usage_rules (
               p_cust_acct_profile_amt_id   => o_cust_acct_profile_amt_id,
               p_cust_profile_id            => o_sprofile_id,
               p_profile_class_amt_id       => NULL,
               p_profile_class_id           => NULL,
               x_return_status              => o_ret_status,
               x_msg_count                  => o_msg_count,
               x_msg_data                   => o_msg_data);

            IF o_ret_status <> 'S'
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data :=
                  'Shipto cascade_credit_usage_rules: ' || o_return_msg;
               debug (p_customer_number, x_msg_data);
               x_ret_status := o_ret_status;
               ROLLBACK TO customer_record;
               GOTO STATUS_UPDATE;
            END IF;

            lvc_sec :=
                  'Profile Amts usage Rules for Ship to Creation Process Completed - API Status '
               || o_ret_status;
            debug (p_customer_number, lvc_sec);


           <<STATUS_UPDATE>>
            IF NVL (x_ret_status, 'S') = 'S'
            THEN
               UPDATE XXWC.XXWC_AR_CUST_SITE_IFACE_STG
                  SET PROCESS_STATUS = 'Y'
                --ORACLE_CUSTOMER_NUMBER = g_cust_acct_no
                WHERE ROWID = rec_cust.row_id;
            ELSE
               UPDATE XXWC.XXWC_AR_CUST_SITE_IFACE_STG
                  SET PROCESS_STATUS = 'E', PROCESS_ERROR = x_msg_data
                WHERE ROWID = rec_cust.row_id;
            END IF;
         EXCEPTION
            WHEN OTHERS
            THEN
               x_msg_data := SQLERRM;
               debug (p_customer_number, x_msg_data);

               UPDATE xxwc.XXWC_AR_CUST_SITE_IFACE_STG
                  SET process_STATUS = 'E', PROCESS_ERROR = x_msg_data
                WHERE ROWID = rec_cust.row_id;

               xxcus_error_pkg.xxcus_error_main_api (
                  p_called_from         => g_err_callfrom || '.' || lvc_procedure,
                  p_calling             => lvc_sec,
                  p_ora_error_msg       => SQLERRM,
                  p_error_desc          =>    'Error Occured for '
                                           || p_customer_number
                                           || 'record id in XXWC_AR_CUST_SITE_IFACE_STG.',
                  p_distribution_list   => g_distro_list,
                  p_module              => g_module);
         END;
      END LOOP;

      COMMIT;
   END;

   PROCEDURE multiple_shipto_sites (p_customer_number   IN VARCHAR2,
                                    p_cust_account_id   IN NUMBER,
                                    p_party_id          IN NUMBER)
   IS
      -- ==============================================================================================================================
      -- Procedure: multiple_shipto_sites
      -- Purpose: To create customer all ship to sites except primary shipto
      -- ==============================================================================================================================
      --  REVISIONS:
      --   Ver        Date        Author               Description
      --  ---------  ---------- ---------------  --------------------------------------------------------------------------------------
      --  1.0        06/06/2018  P.Vamshidhar     TMS#20180308-00290 -AH HARRIS Customer Conversion
      -- ==============================================================================================================================

      CURSOR cust_sites
      IS
         SELECT xaca.ORIG_SYSTEM_REFERENCE RECORD_ID,
                xaca.ORIG_SYSTEM_REFERENCE CUSTOMER_NUMBER,
                UPPER (site.LEGACY_PARTY_SITE_NUMBER) CUSTOMER_SITE_NUMBER,
                xaca.ACCOUNT_NAME BUSINESS_NAME,
                NULL BUSINESS_PHONE,
                NULL BUSINESS_FAX,
                NULL EMAIL_ADDRESS,
                site.PRIMARY_SALESREP_NUMBER OUTSIDE_SALES_REP_ID,
                'UNKNOWN' type_of_business,
                SUBSTR (site.CREDIT_HOLD, 1, 1) CREDIT_HOLD,
                'UNKNOWN' PREDOMINANT_TRADE,
                SUBSTR (STATUS, 1, 1) STATUS,
                site.CREDIT_ANALYST_NAME credit_manager,
                site.ADDRESS1 SHIPPING_ADDRESS1,
                site.ADDRESS2 SHIPPING_ADDRESS2,
                site.CITY SHIPPING_CITY,
                site.COUNTY SHIPPING_COUNTY,
                site.STATE SHIPPING_STATE,
                NVL (site.COUNTRY, 'US') SHIPPING_COUNTRY,
                site.POSTAL_CODE SHIPPING_ZIP_CODE,
                site.OVERALL_CREDIT_LIMIT DEFAULT_JOB_CREDIT_LIMIT,
                NULL AP_CONTACT_FIRST_NAME,
                NULL AP_CONTACT_LAST_NAME,
                NULL AP_PHONE,
                NULL AP_EMAIL,
                'Y' PURCHASE_ORDER_REQUIRED,
                '2' print_prices_on_order,
                SUBSTR (site.LEGACY_PARTY_SITE_NAME, 1, 35) JOB_SITE_NAME,
                'None' TAX_EXEMPTION,
                'JOB' yard_job_accnt_project,
                xaca.standard_term_name,
                site.inside_salesrep_id,
                site.fsd,
                site.ROWID row_id
           FROM XXWC.XXWC_AR_CUSTOMER_IFACE_STG xaca,
                XXWC.XXWC_AR_CUST_SITE_IFACE_STG site
          WHERE     xaca.ORIG_SYSTEM_REFERENCE = site.LEGACY_CUSTOMER_NUMBER
                AND site.LEGACY_PARTY_SITE_NUMBER IS NOT NULL
                AND xaca.PROCESS_status = 'Y'
                AND site.PROCESS_STATUS = 'V'
                AND xaca.ORIG_SYSTEM_REFERENCE = P_CUSTOMER_NUMBER;

      lvc_sec                      VARCHAR2 (32767);
      o_msg_data                   VARCHAR2 (32767);
      o_msg_count                  NUMBER;
      o_location_id                HZ_LOCATIONS.LOCATION_ID%TYPE;
      o_sprofile_id                NUMBER;
      o_party_site_id              NUMBER;
      o_party_site_no              VARCHAR2 (100);
      o_return_msg                 VARCHAR2 (32767);
      o_ret_status                 VARCHAR2 (1);
      plocationrec                 hz_location_v2pub.location_rec_type;
      ppartysiterec                hz_party_site_v2pub.party_site_rec_type;
      pcustacctsiterec             hz_cust_account_site_v2pub.cust_acct_site_rec_type;
      pcustacctsiteuserec          hz_cust_account_site_v2pub.cust_site_use_rec_type;
      pcustomerprofile             hz_customer_profile_v2pub.customer_profile_rec_type;
      pcustproamtrec               hz_customer_profile_v2pub.cust_profile_amt_rec_type;
      o_bill_to_site_use_id        NUMBER;
      o_cust_acct_site_id          NUMBER;
      o_cust_acct_site_use_id      NUMBER;
      o_cust_acct_profile_amt_id   NUMBER;
      x_msg_data                   VARCHAR2 (32767);
      x_ret_status                 VARCHAR2 (1);
      p_object_version_number      NUMBER;
      lvc_procedure                VARCHAR2 (30) := 'MULTIPLE_SHIPTO_SITES';
      ln_profile_class_id          NUMBER;
      ln_collector_id              NUMBER;
      ln_credit_analyst            NUMBER;
      lvc_cross_over_check         VARCHAR2 (1) := 'N';
   BEGIN
      debug (p_customer_number, 'Inside Create Multiple Sites');



      FOR rec_sites IN cust_sites
      LOOP
         BEGIN
            SAVEPOINT customer_record;
            lvc_sec := 'Ship TO location Creation';
            debug (p_customer_number, lvc_sec);
            plocationrec := NULL;
            ppartysiterec := NULL;
            pcustacctsiterec := NULL;
            pcustacctsiteuserec := NULL;
            pcustomerprofile := NULL;
            o_ret_status := NULL;
            o_msg_count := NULL;
            o_msg_data := NULL;
            o_location_id := NULL;
            o_cust_acct_site_use_id := NULL;

            IF rec_sites.SHIPPING_COUNTRY IS NOT NULL
            THEN
               BEGIN
                  SELECT TERRITORY_CODE
                    INTO plocationrec.country
                    FROM HR_TERRITORIES_V
                   WHERE (   UPPER (TERRITORY_SHORT_NAME) =
                                UPPER (rec_sites.SHIPPING_COUNTRY)
                          OR UPPER (TERRITORY_CODE) =
                                UPPER (rec_sites.SHIPPING_COUNTRY));
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     plocationrec.country := NULL;
                     return_msg (o_msg_count, o_msg_data, o_return_msg);
                     o_return_msg :=
                           'Invalid Shipping Country o_return_msg '
                        || o_return_msg;
                     debug (p_customer_number,
                            o_return_msg || ' ' || SQLERRM);
                     ROLLBACK TO customer_record;
                     GOTO STATUS_UPDATE;
               END;
            ELSE
               plocationrec.country := NULL;
            END IF;

            plocationrec.postal_code := rec_sites.SHIPPING_ZIP_CODE;
            plocationrec.address1 := rec_sites.SHIPPING_ADDRESS1;
            plocationrec.address2 := rec_sites.SHIPPING_ADDRESS2;
            plocationrec.state := rec_sites.SHIPPING_STATE;
            plocationrec.city := rec_sites.SHIPPING_CITY;
            plocationrec.county := rec_sites.SHIPPING_COUNTY;
            plocationrec.created_by_module := g_api_name;
            plocationrec.sales_tax_geocode := NULL;

            hz_location_v2pub.create_location (
               p_init_msg_list   => 'T',
               p_location_rec    => plocationrec,
               x_location_id     => o_location_id,
               x_return_status   => o_ret_status,
               x_msg_count       => o_msg_count,
               x_msg_data        => o_msg_data);

            debug (p_customer_number, 'o_ret_status:' || o_ret_status);

            IF o_ret_status <> 'S'
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data := 'Location Creation: ' || o_return_msg;
               debug (p_customer_number, x_msg_data);
               x_ret_status := o_ret_status;
               ROLLBACK TO customer_record;
               GOTO STATUS_UPDATE;
            END IF;

            lvc_sec :=
                  'Location Account for Ship To Site Creation Process Completed - API Status'
               || o_ret_status;

            debug (p_customer_number, lvc_sec);

            lvc_sec := 'Ship TO Party Site Creation';

            o_ret_status := NULL;
            o_msg_count := NULL;
            o_msg_data := NULL;
            o_party_site_id := NULL;
            o_party_site_no := NULL;
            o_sprofile_id := NULL;
            ppartysiterec.created_by_module := g_api_name;

            --Create a party site now
            ppartysiterec.party_id := p_party_id;
            ppartysiterec.location_id := o_location_id;

            hz_party_site_v2pub.create_party_site (
               p_init_msg_list       => 'T',
               p_party_site_rec      => ppartysiterec,
               x_party_site_id       => o_party_site_id,
               x_party_site_number   => o_party_site_no,
               x_return_status       => o_ret_status,
               x_msg_count           => o_msg_count,
               x_msg_data            => o_msg_data);

            IF o_ret_status <> 'S'
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data := 'Error: Ship to Party Creation ' || o_return_msg;
               debug (p_customer_number, x_msg_data);
               x_ret_status := o_ret_status;
               ROLLBACK TO customer_record;
               GOTO STATUS_UPDATE;
            END IF;

            lvc_sec :=
                  'Ship TO Party Site Creation Process Completed - API Status '
               || o_ret_status;

            debug (p_customer_number, lvc_sec);

            lvc_sec := 'Ship to Site Account Creation';

            pcustacctsiterec := NULL;
            o_ret_status := NULL;
            o_msg_count := NULL;
            o_msg_data := NULL;
            o_cust_acct_site_id := NULL;
            pcustacctsiterec.cust_account_id := p_cust_account_id;
            pcustacctsiterec.party_site_id := o_party_site_id;
            pcustacctsiterec.created_by_module := g_api_name;
            pcustacctsiterec.org_id := gn_org_id;
            pcustacctsiterec.attribute3 :=
               SUBSTR (NVL (rec_sites.purchase_order_required, 'N'), 1, 1);
            pcustacctsiterec.attribute_category := 'No';
            pcustacctsiterec.attribute1 := rec_sites.print_prices_on_order;
            pcustacctsiterec.attribute15 := 'None';
            pcustacctsiterec.attribute14 := 'N';
            pcustacctsiterec.attribute17 :=
                  rec_sites.customer_number
               || '-'
               || rec_sites.customer_site_number;

            BEGIN
               SELECT CASE
                         WHEN customer_class_code = 'COMMERCIAL'
                         THEN
                            'PUBLIC'
                         WHEN customer_class_code = 'GOVERNMENT'
                         THEN
                            'PRIVATE'
                         WHEN customer_class_code = 'NON-CONTRACTOR/OTHER'
                         THEN
                            'PUBLIC'
                         WHEN customer_class_code = 'RESELLER'
                         THEN
                            'PUBLIC'
                         WHEN customer_class_code = 'RESIDENTIAL_CONTRACTOR'
                         THEN
                            'RES'
                         WHEN customer_class_code = 'UNKNOWN'
                         THEN
                            NULL
                         ELSE
                            NULL
                      END
                 INTO pcustacctsiterec.customer_category_code
                 FROM hz_cust_accounts
                WHERE cust_account_id = p_cust_account_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  pcustacctsiterec.customer_category_code := NULL;
            END;

            hz_cust_account_site_v2pub.create_cust_acct_site (
               p_init_msg_list        => 'T',
               p_cust_acct_site_rec   => pcustacctsiterec,
               x_cust_acct_site_id    => o_cust_acct_site_id,
               x_return_status        => o_ret_status,
               x_msg_count            => o_msg_count,
               x_msg_data             => o_msg_data);


            IF o_ret_status <> 'S'
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data := 'Ship TO Site Accont: ' || o_return_msg;
               debug (p_customer_number, x_msg_data);
               x_ret_status := o_ret_status;
               ROLLBACK TO customer_record;
               GOTO STATUS_UPDATE;
            END IF;

            lvc_sec :=
                  'Ship To Cust Acct Site Creation Process Completed - API Status '
               || o_ret_status;
            debug (p_customer_number, lvc_sec);

            lvc_sec := 'Creaete Ship TO Use for Bill TO';

            ln_collector_id := NULL;
            ln_credit_analyst := NULL;
            lvc_cross_over_check := 'N';

            BEGIN
               SELECT 'Y'
                 INTO lvc_cross_over_check
                 FROM XXWC.XXWC_AHH_AR_CUST_CROSS_OVER_T
                WHERE REPLACE (REPLACE (TRIM (CUST_NUM), CHR (13), ''),
                               CHR (10),
                               '') = P_CUSTOMER_NUMBER;
            EXCEPTION
               WHEN OTHERS
               THEN
                  lvc_cross_over_check := 'N';
            END;

            IF lvc_cross_over_check = 'Y'
            THEN
               BEGIN
                  SELECT COLLECTOR_ID, CREDIT_ANALYST_ID
                    INTO ln_collector_id, ln_credit_analyst
                    FROM APPS.HZ_CUSTOMER_PROFILES
                   WHERE     SITE_USE_ID IS NULL
                         AND CUST_ACCOUNT_ID = P_CUST_ACCOUNT_ID
                         AND ROWNUM = 1;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     ln_collector_id := NULL;
                     ln_credit_analyst := NULL;
               END;
            ELSE
               -- Deriving Collector
               BEGIN
                  SELECT ORA_COLL_ID
                    INTO ln_collector_id
                    FROM XXWC.XXWC_AHH_CRD_COLL_ANALYST_T b
                   WHERE     UPPER (TRIM (b.ahh_coll_code)) =
                                UPPER (rec_sites.credit_manager)
                         AND ROWNUM = 1;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     BEGIN
                        SELECT ORA_COLL_ID
                          INTO ln_collector_id
                          FROM XXWC.XXWC_AHH_CRD_COLL_ANALYST_T b
                         WHERE     UPPER (TRIM (b.ahh_coll_code)) = 'DEFAULT'
                               AND ROWNUM = 1;
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                           ln_collector_id := 1;          -- Default Collector
                     END;
               END;

               BEGIN
                  SELECT ORA_CRD_ANLST_ID
                    INTO ln_credit_analyst
                    FROM XXWC.XXWC_AHH_CRD_COLL_ANALYST_T b
                   WHERE     UPPER (TRIM (b.ahh_coll_code)) =
                                UPPER (rec_sites.credit_manager)
                         AND ROWNUM = 1;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     BEGIN
                        SELECT ORA_CRD_ANLST_ID
                          INTO ln_credit_analyst
                          FROM XXWC.XXWC_AHH_CRD_COLL_ANALYST_T b
                         WHERE     UPPER (TRIM (b.ahh_coll_code)) = 'DEFAULT'
                               AND ROWNUM = 1;
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                           ln_credit_analyst := NULL;
                     END;
               END;
            END IF;

            DEBUG (p_customer_number, 'ln_collector_id:' || ln_collector_id);
            DEBUG (p_customer_number,
                   'ln_credit_analyst:' || ln_credit_analyst);
            pcustacctsiteuserec := NULL;
            o_ret_status := NULL;
            o_msg_count := NULL;
            o_msg_data := NULL;
            o_cust_acct_site_use_id := NULL;

            pcustacctsiteuserec.site_use_code := 'BILL_TO';
            pcustacctsiteuserec.cust_acct_site_id := o_cust_acct_site_id;
            pcustacctsiteuserec.created_by_module := g_api_name;
            pcustacctsiteuserec.location := rec_sites.job_site_name;
            pcustacctsiteuserec.attribute1 := rec_sites.yard_job_accnt_project;

            BEGIN
               SELECT A.SALESREP_ID
                 INTO pcustacctsiteuserec.primary_salesrep_id
                 FROM RA_SALESREPS_ALL A, XXWC.XXWC_AHH_AM_T B
                WHERE     A.ORG_ID = 162
                      AND UPPER (TRIM (B.SLSREPOUT)) =
                             UPPER (TRIM (rec_sites.OUTSIDE_SALES_REP_ID))
                      AND A.SALESREP_NUMBER = B.SALESREP_ID
                      AND ROWNUM = 1;
            EXCEPTION
               WHEN OTHERS
               THEN
                  BEGIN
                     SELECT A.SALESREP_ID
                       INTO pcustacctsiteuserec.primary_salesrep_id
                       FROM RA_SALESREPS_ALL A, XXWC.XXWC_AHH_AM_T B
                      WHERE     A.ORG_ID = 162
                            AND UPPER (TRIM (B.SLSREPOUT)) = 'DEFAULT'
                            AND A.SALESREP_NUMBER = B.SALESREP_ID
                            AND ROWNUM = 1;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        pcustacctsiteuserec.primary_salesrep_id := -3;
                  END;
            END;


            BEGIN
               SELECT A.SALESREP_ID
                 INTO pcustacctsiteuserec.attribute12
                 FROM RA_SALESREPS_ALL A, XXWC.XXWC_AHH_AM_T B
                WHERE     A.ORG_ID = 162
                      AND UPPER (B.SLSREPOUT) =
                             UPPER (rec_sites.INSIDE_SALESREP_ID)
                      AND A.SALESREP_NUMBER = B.SALESREP_ID;
            EXCEPTION
               WHEN OTHERS
               THEN
                  pcustacctsiteuserec.attribute12 := -3;
            END;

            BEGIN
               SELECT A.SALESREP_ID
                 INTO pcustacctsiteuserec.attribute13
                 FROM RA_SALESREPS_ALL A, XXWC.XXWC_AHH_AM_T B
                WHERE     A.ORG_ID = 162
                      AND UPPER (B.SLSREPOUT) = UPPER (rec_sites.FSD)
                      AND A.SALESREP_NUMBER = B.SALESREP_ID;
            EXCEPTION
               WHEN OTHERS
               THEN
                  pcustacctsiteuserec.attribute13 := -3;
            END;

            pcustomerprofile.credit_checking := 'Y';
            pcustomerprofile.attribute2 := 2;
            pcustomerprofile.attribute3 := 'Y';
            pcustomerprofile.collector_id := ln_collector_id;

            IF ln_credit_analyst IS NOT NULL
            THEN
               pcustomerprofile.credit_analyst_id := ln_credit_analyst;
            END IF;

            pcustomerprofile.credit_rating := 'Y';
            pcustomerprofile.account_status := g_acct_status;
            pcustomerprofile.tolerance := 0;
            pcustomerprofile.credit_classification := 'MODERATE';
            pcustomerprofile.standard_terms := g_term_id;
            pcustomerprofile.credit_hold := rec_sites.credit_hold;

            IF rec_sites.standard_term_name = 'COD'
            THEN
               SELECT PROFILE_CLASS_ID
                 INTO pcustomerprofile.profile_class_id
                 FROM HZ_CUST_PROFILE_CLASSES
                WHERE NAME = 'COD Customers';
            ELSE
               SELECT PROFILE_CLASS_ID
                 INTO pcustomerprofile.profile_class_id
                 FROM HZ_CUST_PROFILE_CLASSES
                WHERE NAME = 'Contractor - Non Key';
            END IF;

            hz_cust_account_site_v2pub.create_cust_site_use (
               p_init_msg_list          => 'T',
               p_cust_site_use_rec      => pcustacctsiteuserec,
               p_customer_profile_rec   => pcustomerprofile,
               p_create_profile         => 'T',
               p_create_profile_amt     => 'T',
               x_site_use_id            => o_bill_to_site_use_id,
               x_return_status          => o_ret_status,
               x_msg_count              => o_msg_count,
               x_msg_data               => o_msg_data);

            IF o_ret_status <> 'S'
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data :=
                  'Account Site Creation(BillTo1): ' || o_return_msg;
               debug (p_customer_number, x_msg_data);
               x_ret_status := o_ret_status;
               ROLLBACK TO customer_record;
               GOTO STATUS_UPDATE;
            END IF;

            lvc_sec :=
                  'Cust Acct Site Use (Billto) for Ship to Creation Process Completed - API Status '
               || o_ret_status;

            debug (p_customer_number, lvc_sec);

            lvc_sec := 'Shipto Site Profile Amounts Create';

            pcustproamtrec := NULL;
            o_sprofile_id := NULL;
            o_cust_acct_profile_amt_id := NULL;

            BEGIN
               SELECT cust_account_profile_id
                 INTO o_sprofile_id
                 FROM apps.hz_customer_profiles
                WHERE     cust_account_id = p_cust_account_id
                      AND site_use_id = o_bill_to_site_use_id;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  o_sprofile_id := NULL;
            END;

            IF o_sprofile_id IS NOT NULL
            THEN
               BEGIN
                  SELECT cust_acct_profile_amt_id, object_version_number
                    INTO o_cust_acct_profile_amt_id, p_object_version_number
                    FROM apps.hz_cust_profile_amts
                   WHERE     cust_account_profile_id = o_sprofile_id
                         AND currency_code = g_currency_code;
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     o_cust_acct_profile_amt_id := NULL;
                     p_object_version_number := NULL;
               END;


               IF NVL (rec_sites.default_job_credit_limit, 0) = 0
               THEN
                  pcustproamtrec.overall_credit_limit := NULL;
               ELSE
                  pcustproamtrec.overall_credit_limit :=
                     rec_sites.default_job_credit_limit;
               END IF;

               IF o_cust_acct_profile_amt_id IS NOT NULL
               THEN
                  --Update amount profile
                  pcustproamtrec.cust_acct_profile_amt_id :=
                     o_cust_acct_profile_amt_id;
                  hz_customer_profile_v2pub.update_cust_profile_amt (
                     p_init_msg_list           => 'T',
                     p_cust_profile_amt_rec    => pcustproamtrec,
                     p_object_version_number   => p_object_version_number,
                     x_return_status           => o_ret_status,
                     x_msg_count               => o_msg_count,
                     x_msg_data                => o_msg_data);
               ELSE
                  --Create Profile Amount
                  pcustproamtrec.cust_account_profile_id := o_sprofile_id;
                  pcustproamtrec.cust_account_id := p_cust_account_id;
                  pcustproamtrec.site_use_id := o_cust_acct_site_use_id;
                  pcustproamtrec.currency_code := g_currency_code;
                  pcustproamtrec.created_by_module := g_api_name;
                  hz_customer_profile_v2pub.create_cust_profile_amt (
                     p_init_msg_list              => 'T',
                     p_check_foreign_key          => 'T',
                     p_cust_profile_amt_rec       => pcustproamtrec,
                     x_cust_acct_profile_amt_id   => o_cust_acct_profile_amt_id,
                     x_return_status              => o_ret_status,
                     x_msg_count                  => o_msg_count,
                     x_msg_data                   => o_msg_data);
               END IF;

               IF o_ret_status <> 'S'
               THEN
                  return_msg (o_msg_count, o_msg_data, o_return_msg);
                  x_msg_data :=
                        'Error:Shipto Profile Amount Creation: '
                     || o_return_msg
                     || ' '
                     || o_ret_status;
                  debug (p_customer_number, x_msg_data);
                  x_ret_status := o_ret_status;
                  ROLLBACK TO customer_record;
                  GOTO STATUS_UPDATE;
               END IF;

               IF     NVL (o_cust_acct_profile_amt_id, 0) > 0
                  AND NVL (rec_sites.default_job_credit_limit, 0) = 0
               THEN
                  BEGIN
                     UPDATE APPS.HZ_CUST_PROFILE_AMTS
                        SET OVERALL_CREDIT_LIMIT = NULL
                      WHERE CUST_ACCT_PROFILE_AMT_ID =
                               o_cust_acct_profile_amt_id;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        NULL;
                  END;
               END IF;
            END IF;

            lvc_sec :=
                  'Cust Acct Site Use (Billto) for Ship to Profile amts update - API Status '
               || o_ret_status;

            debug (p_customer_number, lvc_sec);


            lvc_sec := 'ShipTO Site Create Usage Rules';
            hz_credit_usages_cascade_pkg.cascade_credit_usage_rules (
               p_cust_acct_profile_amt_id   => o_cust_acct_profile_amt_id,
               p_cust_profile_id            => o_sprofile_id,
               p_profile_class_amt_id       => NULL,
               p_profile_class_id           => NULL,
               x_return_status              => o_ret_status,
               x_msg_count                  => o_msg_count,
               x_msg_data                   => o_msg_data);

            IF o_ret_status <> 'S'
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data :=
                     'Ship TO Site Profile Amounts Usage Rules: '
                  || o_return_msg;
               debug (p_customer_number, x_msg_data);
               x_ret_status := o_ret_status;
               ROLLBACK TO customer_record;
               GOTO STATUS_UPDATE;
            END IF;

            lvc_sec :=
                  'Profile Amt usage rules Creation Process Completed - API Status '
               || o_ret_status;

            debug (p_customer_number, lvc_sec);


            lvc_sec := 'Create site use for Ship to (Multiple) ';

            pcustacctsiteuserec := NULL;
            o_ret_status := NULL;
            o_msg_count := NULL;
            o_msg_data := NULL;

            pcustacctsiteuserec.site_use_code := 'SHIP_TO';
            pcustacctsiteuserec.cust_acct_site_id := o_cust_acct_site_id;

            BEGIN
               SELECT inv_remit_to_code
                 INTO pcustomerprofile.attribute2
                 FROM xxwc_ar_cm_remitcode_tbl
                WHERE area_code = SUBSTR (rec_sites.business_phone, 1, 3);
            EXCEPTION
               WHEN OTHERS
               THEN
                  pcustomerprofile.attribute2 := 2;
            END;

            pcustomerprofile.attribute3 := 'Y';
            pcustacctsiteuserec.bill_to_site_use_id := o_bill_to_site_use_id;

            pcustacctsiteuserec.primary_flag := 'Y';
            pcustacctsiteuserec.created_by_module := g_api_name;

            IF rec_sites.OUTSIDE_SALES_REP_ID IS NOT NULL
            THEN
               BEGIN
                  SELECT A.SALESREP_ID
                    INTO pcustacctsiteuserec.primary_salesrep_id
                    FROM RA_SALESREPS_ALL A, XXWC.XXWC_AHH_AM_T B
                   WHERE     A.ORG_ID = 162
                         AND UPPER (TRIM (B.SLSREPOUT)) =
                                UPPER (TRIM (rec_sites.OUTSIDE_SALES_REP_ID))
                         AND A.SALESREP_NUMBER = B.SALESREP_ID
                         AND ROWNUM = 1;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     BEGIN
                        SELECT A.SALESREP_ID
                          INTO pcustacctsiteuserec.primary_salesrep_id
                          FROM RA_SALESREPS_ALL A, XXWC.XXWC_AHH_AM_T B
                         WHERE     A.ORG_ID = 162
                               AND UPPER (TRIM (B.SLSREPOUT)) = 'DEFAULT'
                               AND A.SALESREP_NUMBER = B.SALESREP_ID
                               AND ROWNUM = 1;
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                           pcustacctsiteuserec.primary_salesrep_id := -3;
                     END;
               END;
            END IF;


            BEGIN
               SELECT A.SALESREP_ID
                 INTO pcustacctsiteuserec.attribute12
                 FROM RA_SALESREPS_ALL A, XXWC.XXWC_AHH_AM_T B
                WHERE     A.ORG_ID = 162
                      AND UPPER (B.SLSREPOUT) =
                             UPPER (rec_sites.INSIDE_SALESREP_ID)
                      AND A.SALESREP_NUMBER = B.SALESREP_ID;
            EXCEPTION
               WHEN OTHERS
               THEN
                  pcustacctsiteuserec.attribute12 := -3;
            END;

            BEGIN
               SELECT A.SALESREP_ID
                 INTO pcustacctsiteuserec.attribute13
                 FROM RA_SALESREPS_ALL A, XXWC.XXWC_AHH_AM_T B
                WHERE     A.ORG_ID = 162
                      AND UPPER (B.SLSREPOUT) = UPPER (rec_sites.FSD)
                      AND A.SALESREP_NUMBER = B.SALESREP_ID;
            EXCEPTION
               WHEN OTHERS
               THEN
                  pcustacctsiteuserec.attribute13 := -3;
            END;

            pcustomerprofile.credit_checking := 'Y';
            pcustomerprofile.collector_id := ln_collector_id;

            IF ln_credit_analyst IS NOT NULL
            THEN
               pcustomerprofile.credit_analyst_id := ln_credit_analyst;
            END IF;

            pcustomerprofile.account_status := g_acct_status;
            pcustomerprofile.tolerance := 0;
            pcustacctsiteuserec.location :=
               SUBSTR (rec_sites.job_site_name, 1, 35);
            pcustacctsiteuserec.attribute1 := rec_sites.yard_job_accnt_project;
            pcustomerprofile.credit_classification := 'MODERATE';
            pcustomerprofile.standard_terms := g_term_id;

            IF rec_sites.standard_term_name = 'COD'
            THEN
               SELECT PROFILE_CLASS_ID
                 INTO pcustomerprofile.profile_class_id
                 FROM HZ_CUST_PROFILE_CLASSES
                WHERE NAME = 'COD Customers';
            ELSE
               SELECT PROFILE_CLASS_ID
                 INTO pcustomerprofile.profile_class_id
                 FROM HZ_CUST_PROFILE_CLASSES
                WHERE NAME = 'Contractor - Non Key';
            END IF;

            hz_cust_account_site_v2pub.create_cust_site_use (
               p_init_msg_list          => 'T',
               p_cust_site_use_rec      => pcustacctsiteuserec,
               p_customer_profile_rec   => pcustomerprofile,
               p_create_profile         => 'T',
               p_create_profile_amt     => 'T',
               x_site_use_id            => o_cust_acct_site_use_id,
               x_return_status          => o_ret_status,
               x_msg_count              => o_msg_count,
               x_msg_data               => o_msg_data);

            --ln_ship_site_use_id := o_cust_acct_site_use_id;

            IF o_ret_status <> 'S'
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data := 'Account Site Creation(SHIPTO): ' || o_return_msg;
               debug (p_customer_number, x_msg_data);
               x_ret_status := o_ret_status;
               ROLLBACK TO customer_record;
               GOTO STATUS_UPDATE;
            END IF;

            lvc_sec :=
                  'Cust Acct Site Use for Ship to Creation Process Completed - API Status '
               || o_ret_status;
            debug (p_customer_number, lvc_sec);

            lvc_sec :=
               'Cust Acct Site Use for Ship to Profile Amounts update ';

            pcustproamtrec := NULL;
            o_sprofile_id := NULL;
            o_cust_acct_profile_amt_id := NULL;

            BEGIN
               SELECT cust_account_profile_id
                 INTO o_sprofile_id
                 FROM apps.hz_customer_profiles
                WHERE     cust_account_id = p_cust_account_id
                      AND site_use_id = o_cust_acct_site_use_id;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  o_sprofile_id := NULL;
            END;

            IF o_sprofile_id IS NOT NULL
            THEN
               BEGIN
                  SELECT cust_acct_profile_amt_id, object_version_number
                    INTO o_cust_acct_profile_amt_id, p_object_version_number
                    FROM apps.hz_cust_profile_amts
                   WHERE     cust_account_profile_id = o_sprofile_id
                         AND currency_code = g_currency_code;
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     o_cust_acct_profile_amt_id := NULL;
                     p_object_version_number := NULL;
               END;

               IF NVL (rec_sites.default_job_credit_limit, 0) = 0
               THEN
                  pcustproamtrec.overall_credit_limit := NULL;
               ELSE
                  pcustproamtrec.overall_credit_limit :=
                     rec_sites.default_job_credit_limit;
               END IF;

               IF o_cust_acct_profile_amt_id IS NOT NULL
               THEN
                  --Update amount profile
                  pcustproamtrec.cust_acct_profile_amt_id :=
                     o_cust_acct_profile_amt_id;
                  hz_customer_profile_v2pub.update_cust_profile_amt (
                     p_init_msg_list           => 'T',
                     p_cust_profile_amt_rec    => pcustproamtrec,
                     p_object_version_number   => p_object_version_number,
                     x_return_status           => o_ret_status,
                     x_msg_count               => o_msg_count,
                     x_msg_data                => o_msg_data);
               ELSE
                  --Create Profile Amount
                  pcustproamtrec.cust_account_profile_id := o_sprofile_id;
                  pcustproamtrec.cust_account_id := p_cust_account_id;
                  pcustproamtrec.site_use_id := o_cust_acct_site_use_id;
                  pcustproamtrec.currency_code := g_currency_code;
                  pcustproamtrec.created_by_module := g_api_name;
                  hz_customer_profile_v2pub.create_cust_profile_amt (
                     p_init_msg_list              => 'T',
                     p_check_foreign_key          => 'T',
                     p_cust_profile_amt_rec       => pcustproamtrec,
                     x_cust_acct_profile_amt_id   => o_cust_acct_profile_amt_id,
                     x_return_status              => o_ret_status,
                     x_msg_count                  => o_msg_count,
                     x_msg_data                   => o_msg_data);
               END IF;

               IF o_ret_status <> 'S'
               THEN
                  return_msg (o_msg_count, o_msg_data, o_return_msg);
                  x_msg_data :=
                     'Shipto Profile Amount Creation: ' || o_return_msg;
                  debug (p_customer_number, x_msg_data);
                  x_ret_status := o_ret_status;
                  ROLLBACK TO customer_record;
                  GOTO STATUS_UPDATE;
               END IF;

               IF     NVL (rec_sites.default_job_credit_limit, 0) = 0
                  AND o_cust_acct_profile_amt_id > 0
               THEN
                  BEGIN
                     UPDATE APPS.HZ_CUST_PROFILE_AMTS
                        SET OVERALL_CREDIT_LIMIT = NULL
                      WHERE CUST_ACCT_PROFILE_AMT_ID =
                               o_cust_acct_profile_amt_id;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        NULL;
                  END;
               END IF;
            END IF;

            lvc_sec :=
                  'Profile Amts for Ship to Creation Process Completed - API Status '
               || o_ret_status;
            debug (p_customer_number, lvc_sec);

            lvc_sec := ' Primary Ship TO usage rules';

            o_msg_count := NULL;
            o_msg_data := NULL;
            o_ret_status := NULL;

            hz_credit_usages_cascade_pkg.cascade_credit_usage_rules (
               p_cust_acct_profile_amt_id   => o_cust_acct_profile_amt_id,
               p_cust_profile_id            => o_sprofile_id,
               p_profile_class_amt_id       => NULL,
               p_profile_class_id           => NULL,
               x_return_status              => o_ret_status,
               x_msg_count                  => o_msg_count,
               x_msg_data                   => o_msg_data);

            IF o_ret_status <> 'S'
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data :=
                  'Shipto cascade_credit_usage_rules: ' || o_return_msg;
               debug (p_customer_number, x_msg_data);
               x_ret_status := o_ret_status;
               ROLLBACK TO customer_record;
               GOTO STATUS_UPDATE;
            END IF;

            lvc_sec :=
                  'Profile Amts usage Rules for Ship to Creation Process Completed - API Status '
               || o_ret_status;
            debug (p_customer_number, lvc_sec);


           <<STATUS_UPDATE>>
            IF NVL (x_ret_status, 'S') = 'S'
            THEN
               UPDATE XXWC.XXWC_AR_CUST_SITE_IFACE_STG
                  SET PROCESS_STATUS = 'Y'
                WHERE ROWID = rec_sites.row_id;
            ELSE
               UPDATE XXWC.XXWC_AR_CUST_SITE_IFACE_STG
                  SET PROCESS_STATUS = 'E', PROCESS_ERROR = x_msg_data
                WHERE ROWID = rec_sites.row_id;
            END IF;
         EXCEPTION
            WHEN OTHERS
            THEN
               x_msg_data := SQLERRM;
               debug (p_customer_number, x_msg_data);

               UPDATE xxwc.XXWC_AR_CUST_SITE_IFACE_STG
                  SET PROCESS_STATUS = 'E', PROCESS_ERROR = x_msg_data
                WHERE ROWID = rec_sites.row_id;

               xxcus_error_pkg.xxcus_error_main_api (
                  p_called_from         => g_err_callfrom || '.' || lvc_procedure,
                  p_calling             => lvc_sec,
                  p_ora_error_msg       => SQLERRM,
                  p_error_desc          =>    'Error Occured for '
                                           || p_customer_number
                                           || 'record id in XXWC_AR_CUST_SITE_IFACE_STG.',
                  p_distribution_list   => g_distro_list,
                  p_module              => g_module);
         END;
      END LOOP;

      COMMIT;
   END;


   PROCEDURE add_shipto_sites (p_customer_number   IN VARCHAR2,
                               p_cust_account_id   IN NUMBER,
                               p_party_id          IN NUMBER)
   IS
      -- ==============================================================================================================================
      -- Procedure: multiple_shipto_sites
      -- Purpose: To create customer all ship to sites except primary shipto
      -- ==============================================================================================================================
      --  REVISIONS:
      --   Ver        Date        Author               Description
      --  ---------  ---------- ---------------  --------------------------------------------------------------------------------------
      --  1.0        06/06/2018  P.Vamshidhar     TMS#20180308-00290 -AH HARRIS Customer Conversion
      -- ==============================================================================================================================

      CURSOR cust_sites
      IS
         SELECT site.ORIG_SYSTEM_REFERENCE RECORD_ID,
                site.ORIG_SYSTEM_REFERENCE CUSTOMER_NUMBER,
                UPPER (site.LEGACY_PARTY_SITE_NUMBER) CUSTOMER_SITE_NUMBER,
                NULL BUSINESS_PHONE,
                NULL BUSINESS_FAX,
                NULL EMAIL_ADDRESS,
                site.PRIMARY_SALESREP_NUMBER OUTSIDE_SALES_REP_ID,
                'UNKNOWN' type_of_business,
                SUBSTR (site.CREDIT_HOLD, 1, 1) CREDIT_HOLD,
                'UNKNOWN' PREDOMINANT_TRADE,
                'A' STATUS,
                site.CREDIT_ANALYST_NAME credit_manager,
                site.ADDRESS1 SHIPPING_ADDRESS1,
                site.ADDRESS2 SHIPPING_ADDRESS2,
                site.CITY SHIPPING_CITY,
                site.COUNTY SHIPPING_COUNTY,
                site.STATE SHIPPING_STATE,
                NVL (site.COUNTRY, 'US') SHIPPING_COUNTRY,
                site.POSTAL_CODE SHIPPING_ZIP_CODE,
                site.OVERALL_CREDIT_LIMIT DEFAULT_JOB_CREDIT_LIMIT,
                NULL AP_CONTACT_FIRST_NAME,
                NULL AP_CONTACT_LAST_NAME,
                NULL AP_PHONE,
                NULL AP_EMAIL,
                'Y' PURCHASE_ORDER_REQUIRED,
                '2' print_prices_on_order,
                SUBSTR (site.LEGACY_PARTY_SITE_NAME, 1, 35) JOB_SITE_NAME,
                'None' TAX_EXEMPTION,
                'JOB' yard_job_accnt_project,
                site.ROWID row_id
           FROM XXWC.XXWC_AR_CUST_SITE_IFACE_STG site
          WHERE     1 = 1
                AND site.LEGACY_PARTY_SITE_NUMBER IS NOT NULL
                AND site.PROCESS_STATUS = 'V'
                AND site.ORIG_SYSTEM_REFERENCE = P_CUSTOMER_NUMBER;

      lvc_sec                      VARCHAR2 (32767);
      o_msg_data                   VARCHAR2 (32767);
      o_msg_count                  NUMBER;
      o_location_id                HZ_LOCATIONS.LOCATION_ID%TYPE;
      o_sprofile_id                NUMBER;
      o_party_site_id              NUMBER;
      o_party_site_no              VARCHAR2 (100);
      o_return_msg                 VARCHAR2 (32767);
      o_ret_status                 VARCHAR2 (1);
      plocationrec                 hz_location_v2pub.location_rec_type;
      ppartysiterec                hz_party_site_v2pub.party_site_rec_type;
      pcustacctsiterec             hz_cust_account_site_v2pub.cust_acct_site_rec_type;
      pcustacctsiteuserec          hz_cust_account_site_v2pub.cust_site_use_rec_type;
      pcustomerprofile             hz_customer_profile_v2pub.customer_profile_rec_type;
      pcustproamtrec               hz_customer_profile_v2pub.cust_profile_amt_rec_type;
      o_bill_to_site_use_id        NUMBER;
      o_cust_acct_site_id          NUMBER;
      o_cust_acct_site_use_id      NUMBER;
      o_cust_acct_profile_amt_id   NUMBER;
      x_msg_data                   VARCHAR2 (32767);
      x_ret_status                 VARCHAR2 (1);
      p_object_version_number      NUMBER;
      lvc_procedure                VARCHAR2 (30) := 'ADD_SHIPTO_SITES';
      ln_profile_class_id          NUMBER;
      ln_collector_id              NUMBER;
      ln_credit_analyst            NUMBER;
      ln_payment_term_id           NUMBER;
      lvc_cross_over_check         VARCHAR2 (1) := 'N';
   BEGIN
      debug (p_customer_number, 'Inside Create Multiple Sites');



      FOR rec_sites IN cust_sites
      LOOP
         BEGIN
            SAVEPOINT customer_record;
            lvc_sec := 'Ship TO location Creation';
            plocationrec := NULL;
            ppartysiterec := NULL;
            pcustacctsiterec := NULL;
            pcustacctsiteuserec := NULL;
            pcustomerprofile := NULL;
            o_ret_status := NULL;
            o_msg_count := NULL;
            o_msg_data := NULL;
            o_location_id := NULL;
            o_bill_to_site_use_id := NULL;

            IF rec_sites.SHIPPING_COUNTRY IS NOT NULL
            THEN
               BEGIN
                  SELECT TERRITORY_CODE
                    INTO plocationrec.country
                    FROM HR_TERRITORIES_V
                   WHERE (   UPPER (TERRITORY_SHORT_NAME) =
                                UPPER (rec_sites.SHIPPING_COUNTRY)
                          OR UPPER (TERRITORY_CODE) =
                                UPPER (rec_sites.SHIPPING_COUNTRY));
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     plocationrec.country := NULL;
                     return_msg (o_msg_count, o_msg_data, o_return_msg);
                     o_return_msg :=
                        'Invalid Shipping Country ' || o_return_msg;
                     ROLLBACK TO customer_record;
                     GOTO STATUS_UPDATE;
               END;
            ELSE
               plocationrec.country := NULL;
            END IF;

            plocationrec.postal_code := rec_sites.SHIPPING_ZIP_CODE;
            plocationrec.address1 := rec_sites.SHIPPING_ADDRESS1;
            plocationrec.address2 := rec_sites.SHIPPING_ADDRESS2;
            plocationrec.state := rec_sites.SHIPPING_STATE;
            plocationrec.city := rec_sites.SHIPPING_CITY;
            plocationrec.county := rec_sites.SHIPPING_COUNTY;
            plocationrec.created_by_module := g_api_name;
            plocationrec.sales_tax_geocode := NULL;

            hz_location_v2pub.create_location (
               p_init_msg_list   => 'T',
               p_location_rec    => plocationrec,
               x_location_id     => o_location_id,
               x_return_status   => o_ret_status,
               x_msg_count       => o_msg_count,
               x_msg_data        => o_msg_data);


            IF o_ret_status <> 'S'
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data := 'Location Creation: ' || o_return_msg;
               debug (p_customer_number, x_msg_data);
               x_ret_status := o_ret_status;
               ROLLBACK TO customer_record;
               GOTO STATUS_UPDATE;
            END IF;

            lvc_sec :=
                  'Location Account for Ship To Site Creation Process Completed - API Status'
               || o_ret_status;

            debug (p_customer_number, lvc_sec);

            lvc_sec := 'Ship TO Party Site Creation';

            o_ret_status := NULL;
            o_msg_count := NULL;
            o_msg_data := NULL;
            o_party_site_id := NULL;
            o_party_site_no := NULL;
            o_sprofile_id := NULL;
            ppartysiterec.created_by_module := g_api_name;

            --Create a party site now
            ppartysiterec.party_id := p_party_id;
            ppartysiterec.location_id := o_location_id;
            -- ppartysiterec.identifying_address_flag := 'Y'; -- ?????

            hz_party_site_v2pub.create_party_site (
               p_init_msg_list       => 'T',
               p_party_site_rec      => ppartysiterec,
               x_party_site_id       => o_party_site_id,
               x_party_site_number   => o_party_site_no,
               x_return_status       => o_ret_status,
               x_msg_count           => o_msg_count,
               x_msg_data            => o_msg_data);

            IF o_ret_status <> 'S'
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data := 'Error: Ship to Party Creation ' || o_return_msg;
               debug (p_customer_number, x_msg_data);
               x_ret_status := o_ret_status;
               ROLLBACK TO customer_record;
               GOTO STATUS_UPDATE;
            END IF;

            lvc_sec :=
                  'Ship TO Party Site Creation Process Completed - API Status '
               || o_ret_status;

            debug (p_customer_number, lvc_sec);

            lvc_sec := 'Ship to Site Account Creation';

            pcustacctsiterec := NULL;
            o_ret_status := NULL;
            o_msg_count := NULL;
            o_msg_data := NULL;
            o_cust_acct_site_id := NULL;
            pcustacctsiterec.cust_account_id := p_cust_account_id;
            pcustacctsiterec.party_site_id := o_party_site_id;
            pcustacctsiterec.created_by_module := g_api_name;
            pcustacctsiterec.org_id := gn_org_id;
            pcustacctsiterec.attribute3 :=
               SUBSTR (NVL (rec_sites.purchase_order_required, 'N'), 1, 1);
            pcustacctsiterec.attribute_category := 'No';
            pcustacctsiterec.attribute1 := rec_sites.print_prices_on_order;
            pcustacctsiterec.attribute15 := 'None';
            pcustacctsiterec.attribute14 := 'N';
            pcustacctsiterec.attribute17 :=
                  rec_sites.customer_number
               || '-'
               || rec_sites.customer_site_number;

            BEGIN
               SELECT CASE
                         WHEN customer_class_code = 'COMMERCIAL'
                         THEN
                            'PUBLIC'
                         WHEN customer_class_code = 'GOVERNMENT'
                         THEN
                            'PRIVATE'
                         WHEN customer_class_code = 'NON-CONTRACTOR/OTHER'
                         THEN
                            'PUBLIC'
                         WHEN customer_class_code = 'RESELLER'
                         THEN
                            'PUBLIC'
                         WHEN customer_class_code = 'RESIDENTIAL_CONTRACTOR'
                         THEN
                            'RES'
                         WHEN customer_class_code = 'UNKNOWN'
                         THEN
                            NULL
                         ELSE
                            NULL
                      END
                 INTO pcustacctsiterec.customer_category_code
                 FROM hz_cust_accounts
                WHERE cust_account_id = p_cust_account_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  pcustacctsiterec.customer_category_code := NULL;
            END;

            hz_cust_account_site_v2pub.create_cust_acct_site (
               p_init_msg_list        => 'T',
               p_cust_acct_site_rec   => pcustacctsiterec,
               x_cust_acct_site_id    => o_cust_acct_site_id,
               x_return_status        => o_ret_status,
               x_msg_count            => o_msg_count,
               x_msg_data             => o_msg_data);


            IF o_ret_status <> 'S'
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data := 'Ship TO Site Accont: ' || o_return_msg;
               debug (p_customer_number, x_msg_data);
               x_ret_status := o_ret_status;
               ROLLBACK TO customer_record;
               GOTO STATUS_UPDATE;
            END IF;

            lvc_sec :=
                  'Ship To Cust Acct Site Creation Process Completed - API Status '
               || o_ret_status;
            debug (p_customer_number, lvc_sec);

            lvc_sec := 'Create Ship TO Use for Bill TO';

            ln_collector_id := NULL;
            ln_credit_analyst := NULL;
            lvc_cross_over_check := 'N';

            BEGIN
               SELECT 'Y'
                 INTO lvc_cross_over_check
                 FROM XXWC.XXWC_AHH_AR_CUST_CROSS_OVER_T
                WHERE REPLACE (REPLACE (TRIM (CUST_NUM), CHR (13), ''),
                               CHR (10),
                               '') = P_CUSTOMER_NUMBER;
            EXCEPTION
               WHEN OTHERS
               THEN
                  lvc_cross_over_check := 'N';
            END;

            IF lvc_cross_over_check = 'Y'
            THEN
               BEGIN
                  SELECT COLLECTOR_ID, CREDIT_ANALYST_ID
                    INTO ln_collector_id, ln_credit_analyst
                    FROM APPS.HZ_CUSTOMER_PROFILES
                   WHERE     SITE_USE_ID IS NULL
                         AND CUST_ACCOUNT_ID = P_CUST_ACCOUNT_ID
                         AND ROWNUM = 1;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     ln_collector_id := NULL;
                     ln_credit_analyst := NULL;
               END;
            ELSE
               BEGIN
                  SELECT ORA_COLL_ID
                    INTO ln_collector_id
                    FROM XXWC.XXWC_AHH_CRD_COLL_ANALYST_T b
                   WHERE     UPPER (TRIM (b.ahh_coll_code)) =
                                UPPER (rec_sites.credit_manager)
                         AND ROWNUM = 1;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     BEGIN
                        SELECT ORA_COLL_ID
                          INTO ln_collector_id
                          FROM XXWC.XXWC_AHH_CRD_COLL_ANALYST_T b
                         WHERE     UPPER (TRIM (b.ahh_coll_code)) = 'DEFAULT'
                               AND ROWNUM = 1;
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                           ln_collector_id := 1;          -- Default Collector
                     END;
               END;

               BEGIN
                  SELECT ORA_CRD_ANLST_ID
                    INTO ln_credit_analyst
                    FROM XXWC.XXWC_AHH_CRD_COLL_ANALYST_T b
                   WHERE     UPPER (TRIM (b.ahh_coll_code)) =
                                UPPER (rec_sites.credit_manager)
                         AND ROWNUM = 1;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     BEGIN
                        SELECT ORA_CRD_ANLST_ID
                          INTO ln_credit_analyst
                          FROM XXWC.XXWC_AHH_CRD_COLL_ANALYST_T b
                         WHERE     UPPER (TRIM (b.ahh_coll_code)) = 'DEFAULT'
                               AND ROWNUM = 1;
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                           ln_credit_analyst := NULL;
                     END;
               END;
            END IF;

            DEBUG (p_customer_number, 'ln_collector_id:' || ln_collector_id);

            DEBUG (p_customer_number,
                   'ln_credit_analyst:' || ln_credit_analyst);
            pcustacctsiteuserec := NULL;
            o_ret_status := NULL;
            o_msg_count := NULL;
            o_msg_data := NULL;
            o_cust_acct_site_use_id := NULL;

            pcustacctsiteuserec.site_use_code := 'BILL_TO';
            pcustacctsiteuserec.cust_acct_site_id := o_cust_acct_site_id;
            pcustacctsiteuserec.created_by_module := g_api_name;
            pcustacctsiteuserec.location := rec_sites.job_site_name;
            pcustacctsiteuserec.attribute1 := rec_sites.yard_job_accnt_project;

            BEGIN
               SELECT A.SALESREP_ID
                 INTO pcustacctsiteuserec.primary_salesrep_id
                 FROM RA_SALESREPS_ALL A, XXWC.XXWC_AHH_AM_T B
                WHERE     A.ORG_ID = 162
                      AND UPPER (TRIM (B.SLSREPOUT)) =
                             UPPER (TRIM (rec_sites.OUTSIDE_SALES_REP_ID))
                      AND A.SALESREP_NUMBER = B.SALESREP_ID
                      AND ROWNUM = 1;
            EXCEPTION
               WHEN OTHERS
               THEN
                  BEGIN
                     SELECT A.SALESREP_ID
                       INTO pcustacctsiteuserec.primary_salesrep_id
                       FROM RA_SALESREPS_ALL A, XXWC.XXWC_AHH_AM_T B
                      WHERE     A.ORG_ID = 162
                            AND UPPER (TRIM (B.SLSREPOUT)) = 'DEFAULT'
                            AND A.SALESREP_NUMBER = B.SALESREP_ID
                            AND ROWNUM = 1;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        pcustacctsiteuserec.primary_salesrep_id := -3;
                  END;
            END;

            pcustomerprofile.credit_checking := 'Y';
            pcustomerprofile.attribute2 := 2;
            pcustomerprofile.attribute3 := 'Y';
            pcustomerprofile.collector_id := ln_collector_id;

            IF ln_credit_analyst IS NOT NULL
            THEN
               pcustomerprofile.credit_analyst_id := ln_credit_analyst;
            END IF;

            pcustomerprofile.credit_rating := 'Y';
            pcustomerprofile.account_status := g_acct_status;
            pcustomerprofile.tolerance := 0;
            pcustomerprofile.credit_classification := 'MODERATE';
            pcustomerprofile.standard_terms := g_term_id;
            pcustomerprofile.credit_hold := rec_sites.credit_hold;


            BEGIN
               SELECT PROFILE_CLASS_ID
                 INTO ln_profile_class_id
                 FROM HZ_CUSTOMER_PROFILES
                WHERE     CUST_ACCOUNT_ID = p_cust_account_id
                      AND STATUS = 'A'
                      AND ROWNUM = 1;
            EXCEPTION
               WHEN OTHERS
               THEN
                  SELECT PROFILE_CLASS_ID
                    INTO ln_profile_class_id
                    FROM HZ_CUST_PROFILE_CLASSES
                   WHERE NAME = 'Contractor - Non Key';
            END;

            pcustomerprofile.profile_class_id := ln_profile_class_id;

            hz_cust_account_site_v2pub.create_cust_site_use (
               p_init_msg_list          => 'T',
               p_cust_site_use_rec      => pcustacctsiteuserec,
               p_customer_profile_rec   => pcustomerprofile,
               p_create_profile         => 'T',
               p_create_profile_amt     => 'T',
               x_site_use_id            => o_bill_to_site_use_id,
               x_return_status          => o_ret_status,
               x_msg_count              => o_msg_count,
               x_msg_data               => o_msg_data);

            IF o_ret_status <> 'S'
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data :=
                  'Account Site Creation(BillTo1): ' || o_return_msg;
               debug (p_customer_number, x_msg_data);
               x_ret_status := o_ret_status;
               ROLLBACK TO customer_record;
               GOTO STATUS_UPDATE;
            END IF;

            lvc_sec :=
                  'Cust Acct Site Use (Billto) for Ship to Creation Process Completed - API Status '
               || o_ret_status;

            debug (p_customer_number, lvc_sec);

            lvc_sec := 'Shipto Site Profile Amounts Create';

            pcustproamtrec := NULL;
            o_sprofile_id := NULL;
            o_cust_acct_profile_amt_id := NULL;

            BEGIN
               SELECT cust_account_profile_id
                 INTO o_sprofile_id
                 FROM apps.hz_customer_profiles
                WHERE     cust_account_id = p_cust_account_id
                      AND site_use_id = o_bill_to_site_use_id;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  o_sprofile_id := NULL;
            END;

            IF o_sprofile_id IS NOT NULL
            THEN
               BEGIN
                  SELECT cust_acct_profile_amt_id, object_version_number
                    INTO o_cust_acct_profile_amt_id, p_object_version_number
                    FROM apps.hz_cust_profile_amts
                   WHERE     cust_account_profile_id = o_sprofile_id
                         AND currency_code = g_currency_code;
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     o_cust_acct_profile_amt_id := NULL;
                     p_object_version_number := NULL;
               END;


               IF NVL (rec_sites.default_job_credit_limit, 0) = 0
               THEN
                  pcustproamtrec.overall_credit_limit := NULL;
               ELSE
                  pcustproamtrec.overall_credit_limit :=
                     rec_sites.default_job_credit_limit;
               END IF;

               IF o_cust_acct_profile_amt_id IS NOT NULL
               THEN
                  --Update amount profile
                  pcustproamtrec.cust_acct_profile_amt_id :=
                     o_cust_acct_profile_amt_id;
                  hz_customer_profile_v2pub.update_cust_profile_amt (
                     p_init_msg_list           => 'T',
                     p_cust_profile_amt_rec    => pcustproamtrec,
                     p_object_version_number   => p_object_version_number,
                     x_return_status           => o_ret_status,
                     x_msg_count               => o_msg_count,
                     x_msg_data                => o_msg_data);
               ELSE
                  --Create Profile Amount
                  pcustproamtrec.cust_account_profile_id := o_sprofile_id;
                  pcustproamtrec.cust_account_id := p_cust_account_id;
                  pcustproamtrec.site_use_id := o_cust_acct_site_use_id;
                  pcustproamtrec.currency_code := g_currency_code;
                  pcustproamtrec.created_by_module := g_api_name;
                  hz_customer_profile_v2pub.create_cust_profile_amt (
                     p_init_msg_list              => 'T',
                     p_check_foreign_key          => 'T',
                     p_cust_profile_amt_rec       => pcustproamtrec,
                     x_cust_acct_profile_amt_id   => o_cust_acct_profile_amt_id,
                     x_return_status              => o_ret_status,
                     x_msg_count                  => o_msg_count,
                     x_msg_data                   => o_msg_data);
               END IF;

               IF o_ret_status <> 'S'
               THEN
                  return_msg (o_msg_count, o_msg_data, o_return_msg);
                  x_msg_data :=
                        'Error:Shipto Profile Amount Creation: '
                     || o_return_msg
                     || ' '
                     || o_ret_status;
                  debug (p_customer_number, x_msg_data);
                  x_ret_status := o_ret_status;
                  ROLLBACK TO customer_record;
                  GOTO STATUS_UPDATE;
               END IF;
            END IF;

            lvc_sec :=
                  'Cust Acct Site Use (Billto) for Ship to Profile amts update - API Status '
               || o_ret_status;

            debug (p_customer_number, lvc_sec);


            lvc_sec := 'ShipTO Site Create Usage Rules';
            hz_credit_usages_cascade_pkg.cascade_credit_usage_rules (
               p_cust_acct_profile_amt_id   => o_cust_acct_profile_amt_id,
               p_cust_profile_id            => o_sprofile_id,
               p_profile_class_amt_id       => NULL,
               p_profile_class_id           => NULL,
               x_return_status              => o_ret_status,
               x_msg_count                  => o_msg_count,
               x_msg_data                   => o_msg_data);

            IF o_ret_status <> 'S'
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data :=
                     'Ship TO Site Profile Amounts Usage Rules: '
                  || o_return_msg;
               debug (p_customer_number, x_msg_data);
               x_ret_status := o_ret_status;
               ROLLBACK TO customer_record;
               GOTO STATUS_UPDATE;
            END IF;

            lvc_sec :=
                  'Profile Amt usage rules Creation Process Completed - API Status '
               || o_ret_status;

            debug (p_customer_number, lvc_sec);


            lvc_sec := 'Create site use for Ship to (Multiple) ';

            pcustacctsiteuserec := NULL;
            o_ret_status := NULL;
            o_msg_count := NULL;
            o_msg_data := NULL;

            pcustacctsiteuserec.site_use_code := 'SHIP_TO';
            pcustacctsiteuserec.cust_acct_site_id := o_cust_acct_site_id;

            BEGIN
               SELECT inv_remit_to_code
                 INTO pcustomerprofile.attribute2
                 FROM xxwc_ar_cm_remitcode_tbl
                WHERE area_code = SUBSTR (rec_sites.business_phone, 1, 3);
            EXCEPTION
               WHEN OTHERS
               THEN
                  pcustomerprofile.attribute2 := 2;
            END;

            pcustomerprofile.attribute3 := 'Y';
            pcustacctsiteuserec.bill_to_site_use_id := o_bill_to_site_use_id;

            pcustacctsiteuserec.primary_flag := 'Y';
            pcustacctsiteuserec.created_by_module := g_api_name;

            IF rec_sites.OUTSIDE_SALES_REP_ID IS NOT NULL
            THEN
               BEGIN
                  SELECT A.SALESREP_ID
                    INTO pcustacctsiteuserec.primary_salesrep_id
                    FROM RA_SALESREPS_ALL A, XXWC.XXWC_AHH_AM_T B
                   WHERE     A.ORG_ID = 162
                         AND UPPER (TRIM (B.SLSREPOUT)) =
                                UPPER (TRIM (rec_sites.OUTSIDE_SALES_REP_ID))
                         AND A.SALESREP_NUMBER = B.SALESREP_ID
                         AND ROWNUM = 1;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     BEGIN
                        SELECT A.SALESREP_ID
                          INTO pcustacctsiteuserec.primary_salesrep_id
                          FROM RA_SALESREPS_ALL A, XXWC.XXWC_AHH_AM_T B
                         WHERE     A.ORG_ID = 162
                               AND UPPER (TRIM (B.SLSREPOUT)) = 'DEFAULT'
                               AND A.SALESREP_NUMBER = B.SALESREP_ID
                               AND ROWNUM = 1;
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                           pcustacctsiteuserec.primary_salesrep_id := -3;
                     END;
               END;
            END IF;

            pcustomerprofile.credit_checking := 'Y';
            pcustomerprofile.collector_id := ln_collector_id;

            IF ln_credit_analyst IS NOT NULL
            THEN
               pcustomerprofile.credit_analyst_id := ln_credit_analyst;
            END IF;

            pcustomerprofile.account_status := g_acct_status;
            pcustomerprofile.tolerance := 0;
            pcustacctsiteuserec.location :=
               SUBSTR (rec_sites.JOB_SITE_NAME, 1, 35);
            pcustacctsiteuserec.attribute1 := rec_sites.yard_job_accnt_project;
            pcustomerprofile.credit_classification := 'MODERATE';
            pcustomerprofile.standard_terms := g_term_id;


            pcustomerprofile.profile_class_id := ln_profile_class_id;

            hz_cust_account_site_v2pub.create_cust_site_use (
               p_init_msg_list          => 'T',
               p_cust_site_use_rec      => pcustacctsiteuserec,
               p_customer_profile_rec   => pcustomerprofile,
               p_create_profile         => 'T',
               p_create_profile_amt     => 'T',
               x_site_use_id            => o_cust_acct_site_use_id,
               x_return_status          => o_ret_status,
               x_msg_count              => o_msg_count,
               x_msg_data               => o_msg_data);

            IF o_ret_status <> 'S'
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data := 'Account Site Creation(SHIPTO): ' || o_return_msg;
               debug (p_customer_number, x_msg_data);
               x_ret_status := o_ret_status;
               ROLLBACK TO customer_record;
               GOTO STATUS_UPDATE;
            END IF;

            lvc_sec :=
                  'Cust Acct Site Use for Ship to Creation Process Completed - API Status '
               || o_ret_status;
            debug (p_customer_number, lvc_sec);

            lvc_sec :=
               'Cust Acct Site Use for Ship to Profile Amounts update ';

            pcustproamtrec := NULL;
            o_sprofile_id := NULL;
            o_cust_acct_profile_amt_id := NULL;

            BEGIN
               SELECT cust_account_profile_id
                 INTO o_sprofile_id
                 FROM apps.hz_customer_profiles
                WHERE     cust_account_id = p_cust_account_id
                      AND site_use_id = o_cust_acct_site_use_id;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  o_sprofile_id := NULL;
            END;

            IF o_sprofile_id IS NOT NULL
            THEN
               BEGIN
                  SELECT cust_acct_profile_amt_id, object_version_number
                    INTO o_cust_acct_profile_amt_id, p_object_version_number
                    FROM apps.hz_cust_profile_amts
                   WHERE     cust_account_profile_id = o_sprofile_id
                         AND currency_code = g_currency_code;
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     o_cust_acct_profile_amt_id := NULL;
                     p_object_version_number := NULL;
               END;

               IF NVL (rec_sites.DEFAULT_JOB_CREDIT_LIMIT, 0) = 0
               THEN
                  pcustproamtrec.overall_credit_limit := NULL;
               ELSE
                  pcustproamtrec.overall_credit_limit :=
                     rec_sites.DEFAULT_JOB_CREDIT_LIMIT;
               END IF;

               IF o_cust_acct_profile_amt_id IS NOT NULL
               THEN
                  --Update amount profile
                  pcustproamtrec.cust_acct_profile_amt_id :=
                     o_cust_acct_profile_amt_id;
                  hz_customer_profile_v2pub.update_cust_profile_amt (
                     p_init_msg_list           => 'T',
                     p_cust_profile_amt_rec    => pcustproamtrec,
                     p_object_version_number   => p_object_version_number,
                     x_return_status           => o_ret_status,
                     x_msg_count               => o_msg_count,
                     x_msg_data                => o_msg_data);
               ELSE
                  --Create Profile Amount
                  pcustproamtrec.cust_account_profile_id := o_sprofile_id;
                  pcustproamtrec.cust_account_id := p_cust_account_id;
                  pcustproamtrec.site_use_id := o_cust_acct_site_use_id;
                  pcustproamtrec.currency_code := g_currency_code;
                  pcustproamtrec.created_by_module := g_api_name;
                  hz_customer_profile_v2pub.create_cust_profile_amt (
                     p_init_msg_list              => 'T',
                     p_check_foreign_key          => 'T',
                     p_cust_profile_amt_rec       => pcustproamtrec,
                     x_cust_acct_profile_amt_id   => o_cust_acct_profile_amt_id,
                     x_return_status              => o_ret_status,
                     x_msg_count                  => o_msg_count,
                     x_msg_data                   => o_msg_data);
               END IF;

               IF o_ret_status <> 'S'
               THEN
                  return_msg (o_msg_count, o_msg_data, o_return_msg);
                  x_msg_data :=
                     'Shipto Profile Amount Creation: ' || o_return_msg;
                  debug (p_customer_number, x_msg_data);
                  x_ret_status := o_ret_status;
                  ROLLBACK TO customer_record;
                  GOTO STATUS_UPDATE;
               END IF;

               IF     NVL (rec_sites.default_job_credit_limit, 0) = 0
                  AND o_cust_acct_profile_amt_id > 0
               THEN
                  BEGIN
                     UPDATE APPS.HZ_CUST_PROFILE_AMTS
                        SET OVERALL_CREDIT_LIMIT = NULL
                      WHERE CUST_ACCT_PROFILE_AMT_ID =
                               o_cust_acct_profile_amt_id;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        NULL;
                  END;
               END IF;
            END IF;

            lvc_sec :=
                  'Profile Amts for Ship to Creation Process Completed - API Status '
               || o_ret_status;
            debug (p_customer_number, lvc_sec);

            lvc_sec := ' Primary Ship TO usage rules';

            o_msg_count := NULL;
            o_msg_data := NULL;
            o_ret_status := NULL;

            hz_credit_usages_cascade_pkg.cascade_credit_usage_rules (
               p_cust_acct_profile_amt_id   => o_cust_acct_profile_amt_id,
               p_cust_profile_id            => o_sprofile_id,
               p_profile_class_amt_id       => NULL,
               p_profile_class_id           => NULL,
               x_return_status              => o_ret_status,
               x_msg_count                  => o_msg_count,
               x_msg_data                   => o_msg_data);

            IF o_ret_status <> 'S'
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data :=
                  'Shipto cascade_credit_usage_rules: ' || o_return_msg;
               debug (p_customer_number, x_msg_data);
               x_ret_status := o_ret_status;
               ROLLBACK TO customer_record;
               GOTO STATUS_UPDATE;
            END IF;

            lvc_sec :=
                  'Profile Amts usage Rules for Ship to Creation Process Completed - API Status '
               || o_ret_status;
            debug (p_customer_number, lvc_sec);


           <<STATUS_UPDATE>>
            IF NVL (x_ret_status, 'S') = 'S'
            THEN
               UPDATE XXWC.XXWC_AR_CUST_SITE_IFACE_STG
                  SET PROCESS_STATUS = 'Y'
                WHERE ROWID = rec_sites.row_id;
            ELSE
               UPDATE XXWC.XXWC_AR_CUST_SITE_IFACE_STG
                  SET PROCESS_STATUS = 'E', PROCESS_ERROR = x_msg_data
                WHERE ROWID = rec_sites.row_id;
            END IF;
         EXCEPTION
            WHEN OTHERS
            THEN
               x_msg_data := SQLERRM;
               debug (p_customer_number, x_msg_data);

               UPDATE xxwc.XXWC_AR_CUST_SITE_IFACE_STG
                  SET PROCESS_STATUS = 'E', PROCESS_ERROR = x_msg_data
                WHERE ROWID = rec_sites.row_id;

               xxcus_error_pkg.xxcus_error_main_api (
                  p_called_from         => g_err_callfrom || '.' || lvc_procedure,
                  p_calling             => lvc_sec,
                  p_ora_error_msg       => SQLERRM,
                  p_error_desc          =>    'Error Occured for '
                                           || p_customer_number
                                           || 'record id in XXWC_AR_CUST_SITE_IFACE_STG.',
                  p_distribution_list   => g_distro_list,
                  p_module              => g_module);
         END;
      END LOOP;

      COMMIT;
   END;


   PROCEDURE add_sites
   IS
      CURSOR cur_orp_site_cust
      IS
         SELECT DISTINCT LEGACY_CUSTOMER_NUMBER CUSTOMER_NUMBER
           FROM XXWC.XXWC_AR_CUST_SITE_IFACE_STG
          WHERE     LEGACY_CUSTOMER_NUMBER NOT IN (SELECT ORIG_SYSTEM_REFERENCE
                                                     FROM XXWC.XXWC_AR_CUSTOMER_IFACE_STG)
                -- AND ORIG_SYSTEM_REFERENCE='118600'
                AND PROCESS_STATUS IN ('V');

      ln_party_id           NUMBER;
      ln_cust_account_id    NUMBER;
      l_sec                 VARCHAR2 (32767);
      lvc_cust_exist_flag   VARCHAR2 (1);
   BEGIN
      FOR rec_orp_site_cust IN cur_orp_site_cust
      LOOP
         l_sec := 'Customer Number: ' || rec_orp_site_cust.customer_number;
         lvc_cust_exist_flag := 'N';
         g_primbillto_exists := NULL;

         ln_party_id := NULL;
         ln_cust_account_id := NULL;

         BEGIN
            SELECT hca.CUST_ACCOUNT_ID, hca.PARTY_ID
              INTO ln_cust_account_id, ln_party_id
              FROM HZ_CUST_ACCOUNTS HCA,
                   XXWC.XXWC_AHH_AR_CUST_CROSS_OVER_T XACC
             WHERE     REPLACE (REPLACE (XACC.CUST_NUM, CHR (13), ''),
                                CHR (10),
                                '') = rec_orp_site_cust.CUSTOMER_NUMBER
                   AND REPLACE (REPLACE (XACC.ORACLE_CUST_NUM, CHR (13), ''),
                                CHR (10),
                                '') = HCA.ACCOUNT_NUMBER
                   AND ROWNUM = 1;
         EXCEPTION
            WHEN OTHERS
            THEN
               BEGIN
                  SELECT hca.CUST_ACCOUNT_ID, hca.PARTY_ID
                    INTO ln_cust_account_id, ln_party_id
                    FROM apps.hz_cust_acct_sites_all hcas,
                         apps.hz_cust_accounts hca
                   WHERE     hcas.cust_account_id = hca.cust_account_id
                         AND hca.attribute4 = 'AHH'
                         AND NVL (hcas.attribute17, '123') =
                                rec_orp_site_cust.CUSTOMER_NUMBER
                         AND hcas.bill_to_flag = 'P'
                         AND hca.status = 'A';
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     ln_cust_account_id := NULL;
                     ln_party_id := NULL;
                     lvc_cust_exist_flag := 'N';
               END;
         END;

         IF ln_cust_account_id IS NOT NULL
         THEN
            DEBUG (
               '',
                  'customer:'
               || rec_orp_site_cust.customer_number
               || ' ln_cust_account_id:'
               || ln_cust_account_id
               || ' ln_party_id:'
               || ln_party_id);
            add_shipto_sites (rec_orp_site_cust.customer_number,
                              ln_cust_account_id,
                              ln_party_id);
         END IF;
      END LOOP;
   END;

   PROCEDURE customer_process (p_user_NAME      IN     VARCHAR2,
                               P_RESP_ID        IN     NUMBER,
                               P_RESP_APPL_ID   IN     NUMBER,
                               x_ret_msg           OUT VARCHAR2)
   IS
      ln_request_id   FND_CONCURRENT_REQUESTS.REQUEST_ID%TYPE;
      ln_cust_count   NUMBER := 0;
      ln_site_count   NUMBER := 0;
      l_err_mess      VARCHAR2 (4000);
   BEGIN
      SELECT COUNT (1)
        INTO ln_cust_count
        FROM XXWC.XXWC_AR_CUSTOMER_IFACE_STG
       WHERE NVL (PROCESS_STATUS, 'R') IN ('R', 'E');

      SELECT COUNT (1)
        INTO ln_site_count
        FROM XXWC.XXWC_AR_CUST_SITE_IFACE_STG
       WHERE NVL (PROCESS_STATUS, 'R') IN ('R', 'E');

      BEGIN
         SELECT user_id
           INTO g_user_id
           FROM APPS.FND_USER
          WHERE USER_NAME = P_USER_NAME;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_err_mess := P_USER_NAME || ' Not Exist';
      END;

      IF ln_cust_count > 0 OR ln_site_count > 0
      THEN
         APPS.MO_GLOBAL.SET_POLICY_CONTEXT ('S', 162);
         APPS.fnd_global.apps_initialize (g_user_id,
                                          p_resp_id,
                                          p_resp_appl_id);
         ln_request_id :=
            fnd_request.submit_request (
               application   => 'XXWC',
               program       => 'XXWC_AR_PRISM_CUST_IFACE_SUB',
               description   => NULL,
               start_time    => SYSDATE,
               sub_request   => FALSE);
         x_ret_msg := 'Submitted Request ' || ln_request_id;
      ELSE
         x_ret_msg := 'No Records are ready to process';
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         x_ret_msg :=
               'Error occurred please contact IT support team: '
            || SUBSTR (SQLERRM, 1, 250);
   END;


   PROCEDURE program_submit (x_err_buf       OUT VARCHAR2,
                             x_err_retcode   OUT VARCHAR2)
   IS
      ln_request_id    FND_CONCURRENT_REQUESTS.REQUEST_ID%TYPE;
      ln_cust_count    NUMBER := 0;
      ln_site_count    NUMBER := 0;
      v_phase          VARCHAR2 (100);
      v_status         VARCHAR2 (100);
      v_dev_phase      VARCHAR2 (100);
      v_dev_status     VARCHAR2 (100);
      v_wait_outcome   BOOLEAN;
      v_message        VARCHAR2 (4000);
      lvc_procedure    VARCHAR2 (100) := 'PROGRAM_SUBMIT';
      lvc_sec          VARCHAR2 (2000);
   BEGIN
      lvc_sec := 'Procedure Start';
      ln_request_id :=
         fnd_request.submit_request (
            application   => 'XXWC',
            program       => 'XXWC_AR_PRISM_EBS_CUST_IFACE',
            description   => NULL,
            start_time    => SYSDATE,
            sub_request   => FALSE,
            argument1     => 'Y');

      FND_FILE.PUT_LINE (FND_FILE.LOG,
                         'Validation Request submitted :' || ln_request_id);
      COMMIT;
      lvc_sec := 'Program submitted:' || ln_request_id;
      v_wait_outcome :=
         fnd_concurrent.wait_for_request (request_id   => ln_request_id,
                                          interval     => 10,
                                          max_wait     => 15000,
                                          phase        => v_phase,
                                          status       => v_status,
                                          dev_phase    => v_dev_phase,
                                          dev_status   => v_dev_status,
                                          MESSAGE      => v_message);


      lvc_sec := 'Waiting to complete request';

      SELECT COUNT (1)
        INTO ln_cust_count
        FROM XXWC.XXWC_AR_CUSTOMER_IFACE_STG
       WHERE NVL (PROCESS_STATUS, 'R') IN ('V');

      SELECT COUNT (1)
        INTO ln_site_count
        FROM XXWC.XXWC_AR_CUST_SITE_IFACE_STG
       WHERE NVL (PROCESS_STATUS, 'R') IN ('V');


      IF ln_cust_count > 0 OR ln_site_count > 0
      THEN
         ln_request_id := NULL;
         lvc_sec := 'XXWC_AR_PRISM_EBS_CUST_IFACE';

         ln_request_id :=
            fnd_request.submit_request (
               application   => 'XXWC',
               program       => 'XXWC_AR_PRISM_EBS_CUST_IFACE',
               description   => NULL,
               start_time    => SYSDATE,
               sub_request   => FALSE,
               argument1     => 'N');
         COMMIT;
         FND_FILE.PUT_LINE (FND_FILE.LOG,
                            'Process Request submitted :' || ln_request_id);
      END IF;

      lvc_sec := 'Procedure completed';
   EXCEPTION
      WHEN OTHERS
      THEN
         FND_FILE.PUT_LINE (FND_FILE.LOG, 'Error Occured :' || SQLERRM);
         x_err_retcode := 2;
         x_err_buf := SUBSTR (SQLERRM, 1, 200);
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || lvc_procedure,
            p_calling             => lvc_sec,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          => NULL,
            p_distribution_list   => g_distro_list,
            p_module              => g_module);
   END;
END XXWC_AHH_CUSTOMER_INTF_PKG;
/