Declare
/*  this script will submit all of the concurrent programs for the ASL Conversion
*/

  XXWC_ERROR                           EXCEPTION;
  g_directory_path                     VARCHAR2(80) := '/obase/ebiz/apps/apps_st/appl/xxwc/12.0.0/bin/data/';
  
  l_req                                NUMBER;

  l_phase                              VARCHAR2(80) := 'ASL';
  
  ln_user_id                           NUMBER; -- := FND_GLOBAL.User_ID;
  l_cnt                                NUMBER := 0;
  
Begin

-- Initializing apps variables.
   BEGIN
     SELECT USER_ID INTO ln_user_id from apps.fnd_user where USER_NAME ='XXWC_FF_BSA_FIREFIGHTER';
   exception
   when others then
   dbms_output.put_line('Deriving User id Error occured '||sqlerrm);
   ln_user_id := FND_GLOBAL.USER_ID;  
   END;

   fnd_global.APPS_INITIALIZE (user_id        => ln_user_id,
                               resp_id        => 50884,
                               resp_appl_id   => 401);

   MO_GLOBAL.SET_POLICY_CONTEXT('S',162);					   

  -- set concurrent sequential requests for processing
  If Not FND_PROFILE.Save_User(x_name  => 'CONC_SINGLE_THREAD'
                              ,x_value => 'Y'
                              ) Then
    DBMS_OUTPUT.Put_Line('Unable to set profile value fro CONC_SINGLE_THREAD');
    RAISE XXWC_ERROR;
  End If;
  COMMIT;

  Case l_phase 
    When 'ASL' Then
  
      -- 1. Populate XXWC.XXWC_ASL_CONV_STG table using the below query:

      Insert Into XXWC.XXWC_ASL_CONV_STG
             (organization_code    
             ,item_number          
             ,item_description     
             ,supplier_name        
             ,supplier_site_code   
             ,supplier_item        
             ,blanket_pa_num
             ,status               
             )
      SELECT organization_code
           , partnumber
           , description
           , 'LM SCOFIELD'
           , 'CA-1-LOS ANGELE'
           , vendorpartnumber
           , '46540'
           , 'N'
        From XXWC_INVITEM_STG
       Where Nvl(consigned_flag, 'N')  = 'Y';
      
      l_cnt := SQL%ROWCOUNT;
      DBMS_OUTPUT.Put_Line(l_cnt||' records populated into the XXWC_ASL_CONV_STG table');
      COMMIT;

      -- XXWC ASL Conversion
      l_req := FND_REQUEST.Submit_Request(application => 'XXWC'
                                         ,program     => 'XXWC_ASL_CONVERSION'
                                         ,description => NULL
                                         ,start_time  => SYSDATE
                                         ,sub_request => FALSE
                                         ,argument1   => 'N'                -- validate only
                                         );
      COMMIT;
      If Nvl(l_req,0) = 0 Then
        DBMS_OUTPUT.Put_Line('Unable to submit XXWC ASL Conversion');
      Else
        DBMS_OUTPUT.Put_Line('XXWC ASL Conversion - submitted req '||l_req);
      End If;


    When 'PO' Then
/*
      --  XXWC ASL PO Conversion SQL Loader
      l_req := FND_REQUEST.Submit_Request(application => 'XXWC'
                                         ,program     => 'XXWC_PO_CONV_STG_CTL'
                                         ,description => NULL
                                         ,start_time  => SYSDATE
                                         ,sub_request => FALSE
                                         ,argument1   => g_directory_path||'XXWC_PO_LOAD.txt'
                                         );
      COMMIT;
      If Nvl(l_req,0) = 0 Then
        DBMS_OUTPUT.Put_Line('Unable to submit XXWC ASL PO Conversion SQL Loader');
      Else
        DBMS_OUTPUT.Put_Line('XXWC ASL PO Conversion SQL Loader - submitted req '||l_req);
      End If;
*/
      -- XXWC ASL PO Conversion
      l_req := FND_REQUEST.Submit_Request(application => 'XXWC'
                                         ,program     => 'XXWC_ASL_PO_CONV'
                                         ,description => NULL
                                         ,start_time  => SYSDATE
                                         ,sub_request => FALSE
                                         ,argument1   => 'N'                    -- validate only
                                         );
      COMMIT;
      If Nvl(l_req,0) = 0 Then
        DBMS_OUTPUT.Put_Line('Unable to submit XXWC ASL PO Conversion');
      Else
        DBMS_OUTPUT.Put_Line('XXWC ASL PO Conversion - submitted req '||l_req);
      End If;

      -- Import Standard Purchase Orders
      l_req := FND_REQUEST.Submit_Request(application => 'PO'
                                         ,program     => 'POXPOPDOI'
                                         ,description => NULL
                                         ,start_time  => SYSDATE
                                         ,sub_request => FALSE
                                         ,argument1   => NULL
                                         ,argument2   => 'STANDARD'        -- document type
                                         ,argument3   => NULL
                                         ,argument4   => 'Y'
                                         ,argument5   => NULL
                                         ,argument6   => 'APPROVED'        -- Approval Status
                                         ,argument7   => NULL
                                         ,argument8   => NULL
                                         ,argument9   => NULL
                                         ,argument10  => NULL
                                         ,argument11  => NULL
                                         ,argument12  => NULL
                                         ,argument13  => NULL
                                         ,argument14  => NULL
                                         );
      COMMIT;
      If Nvl(l_req,0) = 0 Then
        DBMS_OUTPUT.Put_Line('Unable to submit Import Standard Purchase Orders');
      Else
        DBMS_OUTPUT.Put_Line('Import Standard Purchase Orders - submitted req '||l_req);
      End If;

--      6. Run the below script to update PO Line Locations before Approving the PO.
    When 'UPDATE' Then
      UPDATE po_line_locations_all 
             SET consigned_flag = 'Y'
             , invoice_close_tolerance = 100
             , inspection_required_flag = 'N'
             , receipt_required_flag = 'N'
             , accrue_on_receipt_flag = 'N'
             , receive_close_tolerance = NULL
       WHERE po_header_id IN (Select PH.po_header_id From PO_HEADERS_INTERFACE  PHI, PO_HEADERS_ALL PH
                               Where PHI.po_header_id = PH.po_header_id
                                 --And PHI.interface_header_id between 55964 and 55984
                                 And PH.segment1 between '67305' and '67325'
                             );

/*
      UPDATE po_line_locations_all 
         SET consigned_flag = 'Y'
       WHERE po_header_id IN (Select PH.po_header_id From PO_HEADERS_INTERFACE  PHI, PO_HEADERS_ALL PH
                               Where PHI.po_header_id = PH.po_header_id
                                 And PHI.interface_header_id between 55964 and 55984
                             );
*/
      l_cnt := SQL%ROWCOUNT;
      DBMS_OUTPUT.Put_Line(l_cnt||' records updated for consignment_flag');
      COMMIT;

    When 'RECEIVE' Then
      -- XXWC Receive ASL PO
      For R1 In (Select PHI.interface_header_id, PH.po_header_id, PH.segment1, PH.created_by
                   From PO_HEADERS_INTERFACE  PHI, PO_HEADERS_ALL PH
                  Where PHI.interface_header_id between 55964 and 55984
                    And PHI.po_header_id = PH.po_header_id
                  Order By 2) Loop

        l_req := FND_REQUEST.Submit_Request(application => 'XXWC'
                                           ,program     => 'XXWC_RECEIVE_ASL_PO'
                                           ,description => NULL
                                           ,start_time  => SYSDATE
                                           ,sub_request => FALSE
                                           ,argument1   => R1.segment1                 -- PO Number  (optional)
                                           );
        COMMIT;
        If Nvl(l_req,0) = 0 Then
          DBMS_OUTPUT.Put_Line('Unable to submit XXWC Receive ASL PO');
        End If;

      End Loop;
      
      --  Receiving Transaction Processor
      l_req := FND_REQUEST.Submit_Request(application => 'PO'
                                         ,program     => 'RVCTP'
                                         ,description => NULL
                                         ,start_time  => SYSDATE
                                         ,sub_request => FALSE
                                         ,argument1   => 'BATCH'
                                         ,argument2   => NULL                   -- group id
                                         ,argument3   => FND_GLOBAL.Org_ID      -- operating unit
                                         );
      COMMIT;
      If Nvl(l_req,0) = 0 Then
        DBMS_OUTPUT.Put_Line('Unable to submit Receiving Transaction Processor');
      End If;
    Else
      DBMS_OUTPUT.Put_Line('Invalid Option.  Use ASL, PO, UPDATE or RECEIVE parameter values');
  End Case;

  -- reset concurrent sequential requests for processing
  If Not FND_PROFILE.Save_User(x_name  => 'CONC_SINGLE_THREAD'
                              ,x_value => 'N'
                              ) Then
    DBMS_OUTPUT.Put_Line('Unable to reset profile value fro CONC_SINGLE_THREAD');
    RAISE XXWC_ERROR;
  End If;
  COMMIT;
  
Exception
  When OTHERS Then
    ROLLBACK;
    DBMS_OUTPUT.Put_Line(SQLERRM);
End;
/