--Report Name            : Time Sensitive Mgmt - WC
--Import Instructions : 1. If RTF template needs to be uploaded to report automatically then move the RTF template to physical path of APPS_DATA_FILE_DIR oracle directory. Check dba_directories view to get the physical path of oracle directory. 
--                         (If you want to use any other oracle directory other than APPS_DATA_FILE_DIR then replace APPS_DATA_FILE_DIR with your oracle directory name below.)
--                      2. Make sure XXEIS schema has read/write permissions to above used oracle directory and give full permissions to the directory at OS level.
--                      3. Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
exec xxeis.eis_rs_ins.g_rtf_dir := 'APPS_DATA_FILE_DIR' ;
prompt Creating View Data for Time Sensitive Mgmt - WC
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_INV_TIME_MGMT_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_INV_TIME_MGMT_V',401,'On-Hand quantities of Inventory Items','','','','MR020532','XXEIS','Eis Inv Onhand Quantity V','EIOQV','','');
--Delete View Columns for EIS_XXWC_INV_TIME_MGMT_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_INV_TIME_MGMT_V',401,FALSE);
--Inserting View Columns for EIS_XXWC_INV_TIME_MGMT_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','ORGANIZATION_CODE',401,'Organization Code','ORGANIZATION_CODE','','','','MR020532','VARCHAR2','','','Organization Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','SUBINVENTORY_CODE',401,'Subinventory Code','SUBINVENTORY_CODE','','','','MR020532','VARCHAR2','','','Subinventory Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','ITEM',401,'Item','ITEM','','','','MR020532','VARCHAR2','','','Item','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','UNIT_OF_MEASURE',401,'Unit Of Measure','UNIT_OF_MEASURE','','','','MR020532','VARCHAR2','','','Unit Of Measure','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','ON_HAND',401,'On Hand','ON_HAND','','','','MR020532','NUMBER','','','On Hand','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','ALTERNATE_BIN_LOC',401,'Alternate Bin Loc','ALTERNATE_BIN_LOC','','','','MR020532','VARCHAR2','','','Alternate Bin Loc','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','BUYER',401,'Buyer','BUYER','','','','MR020532','VARCHAR2','','','Buyer','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','ITEM_DESCRIPTION',401,'Item Description','ITEM_DESCRIPTION','','','','MR020532','VARCHAR2','','','Item Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','OLDEST_BORN_DATE',401,'Oldest Born Date','OLDEST_BORN_DATE','','','','MR020532','DATE','','','Oldest Born Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','PRIMARY_BIN_LOC',401,'Primary Bin Loc','PRIMARY_BIN_LOC','','','','MR020532','VARCHAR2','','','Primary Bin Loc','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','REGION',401,'Region','REGION','','','','MR020532','VARCHAR2','','','Region','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','SHELF_LIFE_DAYS',401,'Shelf Life Days','SHELF_LIFE_DAYS','','','','MR020532','NUMBER','','','Shelf Life Days','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','VENDOR_NAME',401,'Vendor Name','VENDOR_NAME','','','','MR020532','VARCHAR2','','','Vendor Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','VENDOR_NUMBER',401,'Vendor Number','VENDOR_NUMBER','','','','MR020532','VARCHAR2','','','Vendor Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','AT_RISK',401,'At Risk','AT_RISK','','','','MR020532','CHAR','','','At Risk','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','DAYS_AGED',401,'Days Aged','DAYS_AGED','','','','MR020532','NUMBER','','','Days Aged','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','AVGCOST',401,'Avgcost','AVGCOST','','','','MR020532','NUMBER','','','Avgcost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','DATE_RECEIVED',401,'Date Received','DATE_RECEIVED','','','','MR020532','DATE','','','Date Received','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','LOT_NUMBER',401,'Lot Number','LOT_NUMBER','','','','MR020532','VARCHAR2','','','Lot Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','LOT_EXPIRY_DATE',401,'Lot Expiry Date','LOT_EXPIRY_DATE','','','','MR020532','DATE','','','Lot Expiry Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','STATUS_CODE',401,'Status Code','STATUS_CODE','','','','MR020532','VARCHAR2','','','Status Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','UNIT_NUMBER',401,'Unit Number','UNIT_NUMBER','','','','MR020532','VARCHAR2','','','Unit Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','SUBINVENTORY_TYPE',401,'Subinventory Type','SUBINVENTORY_TYPE','','','','MR020532','NUMBER','','','Subinventory Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','INV_ORG_NAME',401,'Inv Org Name','INV_ORG_NAME','','','','MR020532','VARCHAR2','','','Inv Org Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','MASTER_ORG_CODE',401,'Master Org Code','MASTER_ORG_CODE','','','','MR020532','VARCHAR2','','','Master Org Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','LOCATOR',401,'Locator','LOCATOR','','','','MR020532','VARCHAR2','','','Locator','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','REVISION',401,'Revision','REVISION','','','','MR020532','VARCHAR2','','','Revision','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','OWNING_ORGANIZATION',401,'Owning Organization','OWNING_ORGANIZATION','','','','MR020532','VARCHAR2','','','Owning Organization','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','PLANNING_ORGANIZATION',401,'Planning Organization','PLANNING_ORGANIZATION','','','','MR020532','VARCHAR2','','','Planning Organization','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','UNPACKED',401,'Unpacked','UNPACKED','','','','MR020532','NUMBER','','','Unpacked','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','PACKED',401,'Packed','PACKED','','','','MR020532','NUMBER','','','Packed','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','MIL_ORGANIZATION_ID',401,'Mil Organization Id','MIL_ORGANIZATION_ID','','','','MR020532','NUMBER','','','Mil Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','MMS_STATUS_ID',401,'Mms Status Id','MMS_STATUS_ID','','','','MR020532','NUMBER','','','Mms Status Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','MP_ORGANIZATION_ID',401,'Mp Organization Id','MP_ORGANIZATION_ID','','','','MR020532','NUMBER','','','Mp Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','MSIV_INVENTORY_ITEM_ID',401,'Msiv Inventory Item Id','MSIV_INVENTORY_ITEM_ID','','','','MR020532','NUMBER','','','Msiv Inventory Item Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','MSIV_ORGANIZATION_ID',401,'Msiv Organization Id','MSIV_ORGANIZATION_ID','','','','MR020532','NUMBER','','','Msiv Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','MSI_ORGANIZATION_ID',401,'Msi Organization Id','MSI_ORGANIZATION_ID','','','','MR020532','NUMBER','','','Msi Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','OODP_ORGANIZATION_ID',401,'Oodp Organization Id','OODP_ORGANIZATION_ID','','','','MR020532','NUMBER','','','Oodp Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','OODW_ORGANIZATION_ID',401,'Oodw Organization Id','OODW_ORGANIZATION_ID','','','','MR020532','NUMBER','','','Oodw Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','OOD_ORGANIZATION_ID',401,'Ood Organization Id','OOD_ORGANIZATION_ID','','','','MR020532','NUMBER','','','Ood Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','MLN_INVENTORY_ITEM_ID',401,'Mln Inventory Item Id','MLN_INVENTORY_ITEM_ID','','','','MR020532','NUMBER','','','Mln Inventory Item Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','MLN_LOT_NUMBER',401,'Mln Lot Number','MLN_LOT_NUMBER','','','','MR020532','VARCHAR2','','','Mln Lot Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','MLN_ORGANIZATION_ID',401,'Mln Organization Id','MLN_ORGANIZATION_ID','','','','MR020532','NUMBER','','','Mln Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','ORGANIZATION_ID',401,'Organization Id','ORGANIZATION_ID','','','','MR020532','NUMBER','','','Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','LOCATOR_ID',401,'Locator Id','LOCATOR_ID','','','','MR020532','NUMBER','','','Locator Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','INVENTORY_ITEM_ID',401,'Inventory Item Id','INVENTORY_ITEM_ID','','','','MR020532','NUMBER','','','Inventory Item Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','INVENTORY_LOCATION_ID',401,'Inventory Location Id','INVENTORY_LOCATION_ID','','','','MR020532','NUMBER','','','Inventory Location Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','LANGUAGE',401,'Language','LANGUAGE','','','','MR020532','VARCHAR2','','','Language','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','LIST_PRICE_PER_UNIT',401,'List Price Per Unit','LIST_PRICE_PER_UNIT','','','','MR020532','NUMBER','','','List Price Per Unit','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','LPN',401,'Lpn','LPN','','','','MR020532','VARCHAR2','','','Lpn','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','LPN_ID',401,'Lpn Id','LPN_ID','','','','MR020532','NUMBER','','','Lpn Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','MAX_MINMAX_QUANTITY',401,'Max Minmax Quantity','MAX_MINMAX_QUANTITY','','','','MR020532','NUMBER','','','Max Minmax Quantity','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','MIN_MINMAX_QUANTITY',401,'Min Minmax Quantity','MIN_MINMAX_QUANTITY','','','','MR020532','NUMBER','','','Min Minmax Quantity','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','ONHAND_QUANTITIES_ID',401,'Onhand Quantities Id','ONHAND_QUANTITIES_ID','','','','MR020532','NUMBER','','','Onhand Quantities Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','SECONDARY_INVENTORY_NAME',401,'Secondary Inventory Name','SECONDARY_INVENTORY_NAME','','','','MR020532','VARCHAR2','','','Secondary Inventory Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','COST_GROUP_ID',401,'Cost Group Id','COST_GROUP_ID','','','','MR020532','NUMBER','','','Cost Group Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','SUBINVENTORY_STATUS_ID',401,'Subinventory Status Id','SUBINVENTORY_STATUS_ID','','','','MR020532','NUMBER','','','Subinventory Status Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','LOCATOR_STATUS_ID',401,'Locator Status Id','LOCATOR_STATUS_ID','','','','MR020532','NUMBER','','','Locator Status Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','LOT_STATUS_ID',401,'Lot Status Id','LOT_STATUS_ID','','','','MR020532','NUMBER','','','Lot Status Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','PLANNING_TP_TYPE',401,'Planning Tp Type','PLANNING_TP_TYPE','','','','MR020532','NUMBER','','','Planning Tp Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','PLANNING_ORGANIZATION_ID',401,'Planning Organization Id','PLANNING_ORGANIZATION_ID','','','','MR020532','NUMBER','','','Planning Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','OWNING_TP_TYPE',401,'Owning Tp Type','OWNING_TP_TYPE','','','','MR020532','NUMBER','','','Owning Tp Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','OWNING_ORGANIZATION_ID',401,'Owning Organization Id','OWNING_ORGANIZATION_ID','','','','MR020532','NUMBER','','','Owning Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','HAOU_ORGANIZATION_ID',401,'Haou Organization Id','HAOU_ORGANIZATION_ID','','','','MR020532','NUMBER','','','Haou Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','AT_RISK_N',401,'At Risk N','AT_RISK_N','','','','MR020532','CHAR','','','At Risk N','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','SHORT_DATE',401,'Short Date','SHORT_DATE','','','','MR020532','CHAR','','','Short Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','UBD',401,'Ubd','UBD','','','','MR020532','DATE','','','Ubd','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','PROCESS_ID',401,'Process Id','PROCESS_ID','','','','MR020532','NUMBER','','','Process Id','','','');
--Inserting View Components for EIS_XXWC_INV_TIME_MGMT_V
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_TIME_MGMT_V','PA_TASKS',401,'PA_TASKS','MTV','','MR020532','MR020532','-1','','N','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_TIME_MGMT_V','MTL_PARAMETERS',401,'MTL_PARAMETERS','MP','MP','MR020532','MR020532','-1','','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_TIME_MGMT_V','CST_COST_GROUPS',401,'CST_COST_GROUPS','CCG','CCG','MR020532','MR020532','-1','','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_TIME_MGMT_V','MTL_MATERIAL_STATUSES_VL',401,'MTL_MATERIAL_STATUSES_B','MMS','MMS','MR020532','MR020532','-1','','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_TIME_MGMT_V','MTL_ITEM_LOCATIONS_KFV',401,'MTL_ITEM_LOCATIONS','MIL','MIL','MR020532','MR020532','-1','','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_TIME_MGMT_V','PJM_UNIT_NUMBERS',401,'PJM_UNIT_NUMBERS','PUN','PUN','MR020532','MR020532','-1','','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_TIME_MGMT_V','MTL_SECONDARY_INVENTORIES',401,'MTL_SECONDARY_INVENTORIES','MSI','MSI','MR020532','MR020532','-1','','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_TIME_MGMT_V','HR_ORGANIZATION_UNITS',401,'HR_ALL_ORGANIZATION_UNITS','OOD','OOD','MR020532','MR020532','-1','master_organization_id','N','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_TIME_MGMT_V','HR_ORGANIZATION_UNITS',401,'HR_ALL_ORGANIZATION_UNITS','OODW','OODW','MR020532','MR020532','-1','owning_organization_id','N','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_TIME_MGMT_V','MTL_LOT_NUMBERS',401,'MTL_LOT_NUMBERS','MLN','MLN','MR020532','MR020532','-1','','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_TIME_MGMT_V','HR_ORGANIZATION_UNITS',401,'HR_ALL_ORGANIZATION_UNITS','OODP','OOP','MR020532','MR020532','-1','planning_organization_id','N','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_TIME_MGMT_V','MTL_SYSTEM_ITEMS_KFV',401,'MTL_SYSTEM_ITEMS_B','MSIV','MSIV','MR020532','MR020532','-1','','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_TIME_MGMT_V','CST_COST_GROUP_ACCOUNTS',401,'CST_COST_GROUP_ACCOUNTS','CCGA','CCGA','MR020532','MR020532','-1','','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_TIME_MGMT_V','HR_ORGANIZATION_UNITS',401,'HR_ALL_ORGANIZATION_UNITS','HAOU','HAOU','MR020532','MR020532','-1','','N','','','');
--Inserting View Component Joins for EIS_XXWC_INV_TIME_MGMT_V
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_TIME_MGMT_V','MTL_PARAMETERS','MP',401,'EIOQV.MP_ORGANIZATION_ID','=','MP.ORGANIZATION_ID(+)','','','','','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_TIME_MGMT_V','CST_COST_GROUPS','CCG',401,'EIOQV.CCG_COST_GROUP_ID','=','CCG.COST_GROUP_ID(+)','','','','','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_TIME_MGMT_V','MTL_MATERIAL_STATUSES_VL','MMS',401,'EIOQV.MMS_STATUS_ID','=','MMS.STATUS_ID(+)','','','','','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_TIME_MGMT_V','MTL_ITEM_LOCATIONS_KFV','MIL',401,'EIOQV.INVENTORY_LOCATION_ID','=','MIL.INVENTORY_LOCATION_ID(+)','','','','','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_TIME_MGMT_V','MTL_ITEM_LOCATIONS_KFV','MIL',401,'EIOQV.MIL_ORGANIZATION_ID','=','MIL.ORGANIZATION_ID(+)','','','','','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_TIME_MGMT_V','PJM_UNIT_NUMBERS','PUN',401,'EIOQV.UNIT_NUMBER','=','PUN.UNIT_NUMBER(+)','','','','','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_TIME_MGMT_V','MTL_SECONDARY_INVENTORIES','MSI',401,'EIOQV.MSI_ORGANIZATION_ID','=','MSI.ORGANIZATION_ID(+)','','','','','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_TIME_MGMT_V','MTL_SECONDARY_INVENTORIES','MSI',401,'EIOQV.SECONDARY_INVENTORY_NAME','=','MSI.SECONDARY_INVENTORY_NAME(+)','','','','','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_TIME_MGMT_V','HR_ORGANIZATION_UNITS','OOD',401,'EIOQV.OOD_ORGANIZATION_ID','=','OOD.ORGANIZATION_ID(+)','','','','','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_TIME_MGMT_V','HR_ORGANIZATION_UNITS','OODW',401,'EIOQV.OODW_ORGANIZATION_ID','=','OODW.ORGANIZATION_ID(+)','','','','','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_TIME_MGMT_V','MTL_LOT_NUMBERS','MLN',401,'EIOQV.MLN_INVENTORY_ITEM_ID','=','MLN.INVENTORY_ITEM_ID(+)','','','','','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_TIME_MGMT_V','MTL_LOT_NUMBERS','MLN',401,'EIOQV.MLN_LOT_NUMBER','=','MLN.LOT_NUMBER(+)','','','','','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_TIME_MGMT_V','MTL_LOT_NUMBERS','MLN',401,'EIOQV.MLN_ORGANIZATION_ID','=','MLN.ORGANIZATION_ID(+)','','','','','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_TIME_MGMT_V','HR_ORGANIZATION_UNITS','OODP',401,'EIOQV.OODP_ORGANIZATION_ID','=','OODP.ORGANIZATION_ID(+)','','','','','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_TIME_MGMT_V','MTL_SYSTEM_ITEMS_KFV','MSIV',401,'EIOQV.MSIV_INVENTORY_ITEM_ID','=','MSIV.INVENTORY_ITEM_ID(+)','','','','','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_TIME_MGMT_V','MTL_SYSTEM_ITEMS_KFV','MSIV',401,'EIOQV.MSIV_ORGANIZATION_ID','=','MSIV.ORGANIZATION_ID(+)','','','','','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_TIME_MGMT_V','CST_COST_GROUP_ACCOUNTS','CCGA',401,'EIOQV.CCGA_COST_GROUP_ID','=','CCGA.COST_GROUP_ID(+)','','','','','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_TIME_MGMT_V','CST_COST_GROUP_ACCOUNTS','CCGA',401,'EIOQV.CCGA_ORGANIZATION_ID','=','CCGA.ORGANIZATION_ID(+)','','','','','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_TIME_MGMT_V','HR_ORGANIZATION_UNITS','HAOU',401,'EIOQV.HAOU_ORGANIZATION_ID','=','HAOU.ORGANIZATION_ID(+)','','','','','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_TIME_MGMT_V','PA_TASKS','MTV',401,'EIOQV.MTV_TASK_ID','=','MTV.TASK_ID(+)','','','','','MR020532','','');
END;
/
set scan on define on
prompt Creating Report LOV Data for Time Sensitive Mgmt - WC
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Time Sensitive Mgmt - WC
xxeis.eis_rs_ins.lov( 401,'SELECT DISTINCT concatenated_segments item, description FROM mtl_system_items_kfv msi,
                org_organization_definitions ood
          WHERE msi.organization_id = ood.organization_id
            AND ood.operating_unit = fnd_profile.VALUE (''ORG_ID'')
       ORDER BY concatenated_segments','','EIS_INV_ITEM_LOV','List of all inventory items.','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'select SECONDARY_INVENTORY_NAME SUB_INVENTORY,HAOU.NAME ORGANIZATION_NAME
from MTL_SECONDARY_INVENTORIES MSI,
     HR_ALL_ORGANIZATION_UNITS HAOU
WHERE 1=1
AND MSI.ORGANIZATION_ID = HAOU.organization_id','','EIS_INV_SUBINVENTORY_LOV','List of All SubInventories.','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'select list_name from XXWC_PARAM_LIST where list_type = ''Supplier'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Supplier List','','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'select list_name from XXWC_PARAM_LIST where list_type = ''Org'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Org List','','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'select distinct ATTRIBUTE9 Region from mtl_parameters','','Region Lov','','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'select list_name from XXWC_PARAM_LIST where list_type = ''Item'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Item List','','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'select vendor_name from po_vendors','','XXWC Vendors','','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'','Y,N,All','XXWC AT Risk Lov','','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( '','SELECT ORGANIZATION_CODE WAREHOUSE,ORGANIZATION_NAME 
FROM ORG_ORGANIZATION_DEFINITIONS OOD 
WHERE SYSDATE < NVL(OOD.DISABLE_DATE,SYSDATE+1) AND EXISTS (SELECT 1 FROM XXEIS.EIS_ORG_ACCESS_V  WHERE organization_id = ood.organization_id )
UNION
SELECT ''All'', ''All Organizations'' FROM DUAL','','XXWC All ORG LIST','XXWC All ORG LIST','ANONYMOUS',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Time Sensitive Mgmt - WC
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Time Sensitive Mgmt - WC
xxeis.eis_rs_utility.delete_report_rows( 'Time Sensitive Mgmt - WC' );
--Inserting Report - Time Sensitive Mgmt - WC
xxeis.eis_rs_ins.r( 401,'Time Sensitive Mgmt - WC','','The Onhand Quantity Report displays the total quantity of an item in a subinventory.','','','','MR020532','EIS_XXWC_INV_TIME_MGMT_V','Y','','','MR020532','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - Time Sensitive Mgmt - WC
xxeis.eis_rs_ins.rc( 'Time Sensitive Mgmt - WC',401,'ITEM','Item','Item','','','default','','4','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_TIME_MGMT_V','','');
xxeis.eis_rs_ins.rc( 'Time Sensitive Mgmt - WC',401,'UNIT_OF_MEASURE','UOM','Unit Of Measure','','','default','','17','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_TIME_MGMT_V','','');
xxeis.eis_rs_ins.rc( 'Time Sensitive Mgmt - WC',401,'ALTERNATE_BIN_LOC','Bin2','Alternate Bin Loc','','','default','','10','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_TIME_MGMT_V','','');
xxeis.eis_rs_ins.rc( 'Time Sensitive Mgmt - WC',401,'BUYER','Buyer','Buyer','','','default','','2','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_TIME_MGMT_V','','');
xxeis.eis_rs_ins.rc( 'Time Sensitive Mgmt - WC',401,'ITEM_DESCRIPTION','Item Description','Item Description','','','default','','5','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_TIME_MGMT_V','','');
xxeis.eis_rs_ins.rc( 'Time Sensitive Mgmt - WC',401,'OLDEST_BORN_DATE','Oldest Born Date','Oldest Born Date','','','default','','12','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_TIME_MGMT_V','','');
xxeis.eis_rs_ins.rc( 'Time Sensitive Mgmt - WC',401,'ON_HAND','On Hand','On Hand','','~,~.~0','default','','6','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_TIME_MGMT_V','','');
xxeis.eis_rs_ins.rc( 'Time Sensitive Mgmt - WC',401,'ORGANIZATION_CODE','Org','Organization Code','','','default','','1','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_TIME_MGMT_V','','');
xxeis.eis_rs_ins.rc( 'Time Sensitive Mgmt - WC',401,'SUBINVENTORY_CODE','Subinventory Code','Subinventory Code','','','default','','3','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_TIME_MGMT_V','','');
xxeis.eis_rs_ins.rc( 'Time Sensitive Mgmt - WC',401,'SHELF_LIFE_DAYS','SL Days','Shelf Life Days','','~,~.~0','default','','11','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_TIME_MGMT_V','','');
xxeis.eis_rs_ins.rc( 'Time Sensitive Mgmt - WC',401,'VENDOR_NAME','Supplier Name','Vendor Name','','','default','','19','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_TIME_MGMT_V','','');
xxeis.eis_rs_ins.rc( 'Time Sensitive Mgmt - WC',401,'VENDOR_NUMBER','Supplier Number','Vendor Number','','','default','','18','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_TIME_MGMT_V','','');
xxeis.eis_rs_ins.rc( 'Time Sensitive Mgmt - WC',401,'PRIMARY_BIN_LOC','Bin1','Primary Bin Loc','','','default','','9','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_TIME_MGMT_V','','');
xxeis.eis_rs_ins.rc( 'Time Sensitive Mgmt - WC',401,'DAYS_AGED','Days Aged','Days Aged','','~~~','default','','13','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_TIME_MGMT_V','','');
xxeis.eis_rs_ins.rc( 'Time Sensitive Mgmt - WC',401,'AVGCOST','Avgcost','Avgcost','','$~,~.~2','default','','7','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_TIME_MGMT_V','','');
xxeis.eis_rs_ins.rc( 'Time Sensitive Mgmt - WC',401,'EXTENDED COST','Extended Cost','Avgcost','NUMBER','$~.~,~2','default','','8','Y','','','','','','','Avgcost*on_hand','MR020532','N','N','','EIS_XXWC_INV_TIME_MGMT_V','','');
xxeis.eis_rs_ins.rc( 'Time Sensitive Mgmt - WC',401,'DATE_RECEIVED','Date Received','Date Received','','','default','','20','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_TIME_MGMT_V','','');
xxeis.eis_rs_ins.rc( 'Time Sensitive Mgmt - WC',401,'UBD','UBD','Ubd','','','default','','16','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_TIME_MGMT_V','','');
xxeis.eis_rs_ins.rc( 'Time Sensitive Mgmt - WC',401,'AT_RISK_N','At Risk','At Risk N','','','default','','14','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_TIME_MGMT_V','','');
xxeis.eis_rs_ins.rc( 'Time Sensitive Mgmt - WC',401,'SHORT_DATE','Short Date','Short Date','','','default','','15','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_TIME_MGMT_V','','');
--Inserting Report Parameters - Time Sensitive Mgmt - WC
xxeis.eis_rs_ins.rp( 'Time Sensitive Mgmt - WC',401,'Organization','Inventory Organization','ORGANIZATION_CODE','IN','XXWC All ORG LIST','','VARCHAR2','Y','Y','1','','N','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Time Sensitive Mgmt - WC',401,'Item','Inventory Item','ITEM','IN','EIS_INV_ITEM_LOV','','VARCHAR2','N','Y','2','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Time Sensitive Mgmt - WC',401,'SubInventory','Subinventory','SUBINVENTORY_CODE','IN','EIS_INV_SUBINVENTORY_LOV','','VARCHAR2','N','Y','3','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Time Sensitive Mgmt - WC',401,'Region','Region','REGION','IN','Region Lov','','VARCHAR2','N','Y','4','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Time Sensitive Mgmt - WC',401,'Location List','Location List','','IN','XXWC Org List','','VARCHAR2','N','Y','6','','N','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Time Sensitive Mgmt - WC',401,'Supplier List','Supplier List','','=','XXWC Supplier List','','VARCHAR2','N','Y','8','','N','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Time Sensitive Mgmt - WC',401,'Item List','Item List','','IN','XXWC Item List','','VARCHAR2','N','Y','7','','N','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Time Sensitive Mgmt - WC',401,'Supplier','Supplier','VENDOR_NAME','IN','XXWC Vendors','','VARCHAR2','N','Y','5','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Time Sensitive Mgmt - WC',401,'At Risk','At Risk','AT_RISK','IN','XXWC AT Risk Lov','''Y''','VARCHAR2','N','Y','9','','N','CONSTANT','MR020532','Y','N','','','');
--Inserting Report Conditions - Time Sensitive Mgmt - WC
xxeis.eis_rs_ins.rcn( 'Time Sensitive Mgmt - WC',401,'PROJECT_NAME','IN',':Project Name','','','N','2','N','MR020532');
xxeis.eis_rs_ins.rcn( 'Time Sensitive Mgmt - WC',401,'','','','','AND PROCESS_ID   = :SYSTEM.PROCESS_ID
','Y','0','','MR020532');
xxeis.eis_rs_ins.rcn( 'Time Sensitive Mgmt - WC',401,'ITEM','IN',':Item','','','Y','2','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Time Sensitive Mgmt - WC',401,'SUBINVENTORY_CODE','IN',':SubInventory','','','Y','3','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Time Sensitive Mgmt - WC',401,'REGION','IN',':Region','','','Y','4','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Time Sensitive Mgmt - WC',401,'VENDOR_NAME','IN',':Supplier','','','Y','5','Y','MR020532');
--Inserting Report Sorts - Time Sensitive Mgmt - WC
--Inserting Report Triggers - Time Sensitive Mgmt - WC
xxeis.eis_rs_ins.rt( 'Time Sensitive Mgmt - WC',401,'BEGIN  
  xxeis.EIS_XXWC_INV_TIME_MGMT_PKG.MAIN(
    P_PROCESS_ID => :SYSTEM.PROCESS_ID,
    P_ORGANIZATION => :Organization,
    P_ITEM => :Item,
    P_SUBINVENTORY =>:SubInventory,
    P_REGION => :Region,
    P_SUPPLIER => :Supplier,
    P_LOCATION_LIST => :Location List,
    P_ITEM_LIST => :Item List,
    P_SUPPLIER_LIST => :Supplier List,
    P_AT_RISK => :At Risk  );
END;','B','Y','MR020532');
xxeis.eis_rs_ins.rt( 'Time Sensitive Mgmt - WC',401,'begin
xxeis.EIS_XXWC_INV_TIME_MGMT_PKG.CLEAR_TABLE(P_PROCESS_ID => :SYSTEM.PROCESS_ID);
end;','A','Y','MR020532');
--Inserting Report Templates - Time Sensitive Mgmt - WC
xxeis.eis_rs_ins.R_Tem( 'Time Sensitive Mgmt - WC','Time Sensitive Mgmt - WC','Seeded template for Time Sensitive Mgmt - WC','','','','','','','','','','','Time Sensitive Mgmt - WC.rtf','MR020532');
--Inserting Report Portals - Time Sensitive Mgmt - WC
--Inserting Report Dashboards - Time Sensitive Mgmt - WC
xxeis.eis_rs_ins.r_dash( 'Time Sensitive Mgmt - WC','Dynamic 702','Dynamic 702','pie','large','Unit Number','Unit Number','Subinventory Code','Subinventory Code','Count','MR020532');
xxeis.eis_rs_ins.r_dash( 'Time Sensitive Mgmt - WC','Dynamic 706','Dynamic 706','vertical stacked bar','large','Subinventory Code','Subinventory Code','On Hand','On Hand','Sum','MR020532');
xxeis.eis_rs_ins.r_dash( 'Time Sensitive Mgmt - WC','Dynamic 707','Dynamic 707','vertical stacked bar','large','Subinventory Code','Subinventory Code','Item','Item','Count','MR020532');
xxeis.eis_rs_ins.r_dash( 'Time Sensitive Mgmt - WC','Dynamic 701','Dynamic 701','vertical percent bar','large','Cost Group','Cost Group','Cost Group','Cost Group','Count','MR020532');
xxeis.eis_rs_ins.r_dash( 'Time Sensitive Mgmt - WC','Dynamic 703','Dynamic 703','horizontal stacked bar','large','Unit Number','Unit Number','Subinventory Code','Subinventory Code','Count','MR020532');
xxeis.eis_rs_ins.r_dash( 'Time Sensitive Mgmt - WC','Dynamic 704','Dynamic 704','vertical stacked bar','large','Unit Number','Unit Number','Subinventory Code','Subinventory Code','Count','MR020532');
xxeis.eis_rs_ins.r_dash( 'Time Sensitive Mgmt - WC','Dynamic 705','Dynamic 705','vertical stacked bar','large','Subinventory Code','Subinventory Code','On Hand','On Hand','Count','MR020532');
--Inserting Report Security - Time Sensitive Mgmt - WC
--Inserting Report Pivots - Time Sensitive Mgmt - WC
xxeis.eis_rs_ins.rpivot( 'Time Sensitive Mgmt - WC',401,'Pivot','1','1,0|1,1,0','1,1,1,0|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rs_ins.rpivot_dtls( 'Time Sensitive Mgmt - WC',401,'Pivot','SUBINVENTORY_CODE','PAGE_FIELD','','','1','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Time Sensitive Mgmt - WC',401,'Pivot','OLDEST_BORN_DATE','ROW_FIELD','','','3','0','');
xxeis.eis_rs_ins.rpivot_dtls( 'Time Sensitive Mgmt - WC',401,'Pivot','SHELF_LIFE_DAYS','ROW_FIELD','','','6','0','');
xxeis.eis_rs_ins.rpivot_dtls( 'Time Sensitive Mgmt - WC',401,'Pivot','ORGANIZATION_CODE','ROW_FIELD','','','1','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'Time Sensitive Mgmt - WC',401,'Pivot','ITEM','ROW_FIELD','','','4','0','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Time Sensitive Mgmt - WC',401,'Pivot','ON_HAND','DATA_FIELD','SUM','','1','1','xlNormal');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
xxeis.eis_rs_ins.rpivot( 'Time Sensitive Mgmt - WC',401,'At Risk','2','0,0|1,0,0','1,1,0,0|PivotStyleMedium4|2');
--Inserting Report Pivot Details For Pivot - At Risk
xxeis.eis_rs_ins.rpivot_dtls( 'Time Sensitive Mgmt - WC',401,'At Risk','ON_HAND','DATA_FIELD','SUM','Sum of On Hand','1','','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Time Sensitive Mgmt - WC',401,'At Risk','ITEM','ROW_FIELD','','','1','0','');
xxeis.eis_rs_ins.rpivot_dtls( 'Time Sensitive Mgmt - WC',401,'At Risk','ITEM_DESCRIPTION','ROW_FIELD','','','2','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'Time Sensitive Mgmt - WC',401,'At Risk','PRIMARY_BIN_LOC','ROW_FIELD','','','3','0','');
xxeis.eis_rs_ins.rpivot_dtls( 'Time Sensitive Mgmt - WC',401,'At Risk','ALTERNATE_BIN_LOC','ROW_FIELD','','','4','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'Time Sensitive Mgmt - WC',401,'At Risk','AVGCOST','DATA_FIELD','SUM','Sum of Avg Cost','2','','xlNormal');
--Inserting Report Summary Calculation Columns For Pivot- At Risk
END;
/
set scan on define on
