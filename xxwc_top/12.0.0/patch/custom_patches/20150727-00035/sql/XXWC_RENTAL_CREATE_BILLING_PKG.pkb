create or replace
PACKAGE BODY      XXWC_RENTAL_CREATE_BILLING_PKG
AS
/*************************************************************************
   *  Copyright (c) 2011 Lucidity Consulting Group
   *  All rights reserved.
   **************************************************************************
   *   $Header XXWC_RENTAL_CREATE_BILLING_PKG .pkb $
   *   Module Name: XXWC_RENTAL_CREATE_BILLING_PKG .pkb
   *
   *   PURPOSE:   This package is used by the XXWC Create Rental Billing Conc Program
   *              to create Charge Lines for Short Term Rental Returns
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        09/29/2011  Srinivas Malkapuram      Initial Version
   *   1.1        10/30/2013  Gopi Damuluri            TMS# 20130823-00477
   *   1.2        01/07/2014  Raghav Velichetti        TMS# 20140107-00054 Bug fix to stop Rental date cascading to Awaiting Return lines                                                -- Copy Shipping Instructions, Serial Number from ItemLine to ChargeLine
   *   1.3		  21/10/2014  Veera C	               TMS# 20141002-00037 Canada Mulit Org Changes
   *   1.4        12/10/2015  Kishorebabu V            TMS# 20150727-00035 Rental Short Term � Rental Sale is not decrementing in inventory
   * ***************************************************************************/
   PROCEDURE main (
      errbuf           OUT      VARCHAR2,
      retcode          OUT      NUMBER,
      p_order_number   IN       VARCHAR2,
      p_log_enabled    IN       VARCHAR2 DEFAULT 'N'
   )
   IS
/* This Cursor return all the Short Term Rental Orders that have a RMA but not the Charge Line */
      CURSOR cur_get_rma_ret_lines
      IS
         SELECT oeh.order_number, oeh.header_id, oeh.price_list_id,
                oeh.attribute1, oel.line_id, oel.line_number,
                oel.inventory_item_id, oel.ordered_quantity,
                oel.shipped_quantity, oel.ship_from_org_id,
                oel.actual_shipment_date,
                oel.attribute4 rentaloverridecharge, oel.unit_list_price,
                oel.order_quantity_uom,

                --07/12/2012 CG Change for rental dates using DFF
                TO_DATE (oel.attribute12,
                         'YYYY/MM/DD HH24:MI:SS'
                        ) new_rental_date
           FROM apps.oe_order_headers oeh, apps.oe_order_lines oel
          WHERE order_number = NVL (p_order_number, order_number)
            --07/12/2012 CG Change for rental dates using DFF
            -- AND oel.actual_shipment_date IS NOT NULL
            AND NVL (oel.attribute12, oel.actual_shipment_date) IS NOT NULL
            AND oeh.flow_status_code NOT IN ('CANCELLED', 'CLOSED')
            AND oel.flow_status_code NOT IN ('CANCELLED')
            AND UPPER (oeh.attribute1) = g_oeh_attr1_short_term
            --(g_oeh_attr1_long_term, g_oeh_attr1_short_term)
            AND oeh.header_id = oel.header_id
            AND EXISTS                          -- To check if RMA Line exists
                      (
                   SELECT line_id
                     FROM apps.oe_order_lines
                    WHERE return_attribute1 = oel.header_id
                      AND return_attribute2 = oel.line_id
                      AND return_reason_code = g_return_reason_code
                      AND flow_status_code IN ('CLOSED', 'RETURNED'))
            AND NOT EXISTS (     --To check if Charge Line does not exists and
                   SELECT oel1.line_id
                     FROM apps.oe_order_lines oel1, apps.oe_order_lines oel2
                    WHERE oel1.header_id = oel.header_id
                      AND oel1.link_to_line_id = oel2.line_id
                      AND oel2.return_attribute2 = oel.line_id
                      AND oel2.return_reason_code = g_return_reason_code
                      AND oel2.flow_status_code IN ('CLOSED', 'RETURNED')
                      AND oel1.flow_status_code NOT IN ('CANCELLED')
                      AND oel2.flow_status_code NOT IN ('CANCELLED')
                      AND oel1.fulfilled_quantity = oel.fulfilled_quantity);
         /* If Charge Line Fulfilled Qty != Ordered Line Fulfilled Quantity */

      CURSOR cur_rma_line_details (p_headerid NUMBER, p_lineid NUMBER)
      IS
         SELECT   oel.line_number, oel.shipment_number, oel.line_id,
                  oel.link_to_line_id, oel.ordered_quantity
             FROM apps.oe_order_lines oel
            WHERE oel.return_attribute1 = p_headerid
              AND oel.return_attribute2 = p_lineid
              AND oel.return_reason_code = g_return_reason_code
              AND oel.flow_status_code IN ('CLOSED', 'RETURNED')
         ORDER BY oel.shipment_number;

/* Local Variables */
      l_ship                 VARCHAR2 (1)        := 'N';
      l_rma                  VARCHAR2 (1)        := 'N';
      l_charge_item          NUMBER;
      l_unit_price           NUMBER;
      l_rma_line_id          NUMBER;
      l_rma_qty              NUMBER;
      l_ship_date            DATE;
      l_sub_inv              VARCHAR2 (20);
      l_return_date          DATE;
      l_unit_selling_price   NUMBER;
      l_charge_line_exists   NUMBER;
      l_rcv_qty              NUMBER;
      l_rental_days          NUMBER;
      l_api_name             VARCHAR2 (30)       := 'MAIN';
      l_module_name          VARCHAR2 (120);
      l_debug_msg            VARCHAR2 (2000);
      lx_return_status       VARCHAR2 (1);
      lx_charge_line_id      NUMBER;
      lxx_return_status      VARCHAR2 (1);
      l_serial_status        BOOLEAN;
      l_reservation_record   reservation_int_rec;
      l_tmp_rental_date      DATE;
      --Define user defined exception
      skip_record            EXCEPTION;
      validation_error       EXCEPTION;
   BEGIN
      l_module_name := g_module_name || '.' || l_api_name;
      l_debug_msg := 'START PROGRAM ';
      errbuf := NULL;
      retcode := '0';
      oe_debug_pub.setdebuglevel (fnd_profile.VALUE ('OE: Debug Level'));
      xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_procedure,
                                      p_mod_name         => l_module_name,
                                      p_debug_msg        => l_debug_msg
                                     );

      FOR get_rma_ret_lines_rec IN cur_get_rma_ret_lines
      LOOP
         BEGIN
            l_debug_msg :=
               (   '*********Processing Sales Order ********* '
                || get_rma_ret_lines_rec.order_number
               );
            xxwc_rental_engine_pkg.log_msg
                                          (p_debug_level      => g_level_procedure,
                                           p_mod_name         => l_module_name,
                                           p_debug_msg        => l_debug_msg
                                          );

            /* Get the RMA Line Details */
            FOR get_rma_line_details_rec IN
               cur_rma_line_details (get_rma_ret_lines_rec.header_id,
                                     get_rma_ret_lines_rec.line_id
                                    )
            LOOP
               BEGIN
                  l_debug_msg :=
                     (   'Processing Line Number '
                      || get_rma_line_details_rec.line_number
                     );
                  xxwc_rental_engine_pkg.log_msg
                                          (p_debug_level      => g_level_procedure,
                                           p_mod_name         => l_module_name,
                                           p_debug_msg        => l_debug_msg
                                          );

                  SELECT COUNT (line_id)
                    INTO l_charge_line_exists
                    FROM apps.oe_order_lines
                   WHERE link_to_line_id = get_rma_line_details_rec.line_id
                     AND flow_status_code NOT IN ('CANCELLED');

                  IF l_charge_line_exists = 0
                  THEN
                     l_debug_msg :=
                        (   'Rental Charge Line does not exist for '
                         || get_rma_line_details_rec.line_number
                         || '.'
                         || get_rma_line_details_rec.shipment_number
                        );
                     xxwc_rental_engine_pkg.log_msg
                                          (p_debug_level      => g_level_procedure,
                                           p_mod_name         => l_module_name,
                                           p_debug_msg        => l_debug_msg
                                          );
                     l_rma_line_id := get_rma_line_details_rec.line_id;
                     l_rma_qty := get_rma_line_details_rec.ordered_quantity;
                     /* Get ChargeItem for the Inventory Item*/
                     l_charge_item :=
                        xxwc_rental_engine_pkg.get_charge_item
                                     (get_rma_ret_lines_rec.inventory_item_id,
                                      get_rma_ret_lines_rec.ship_from_org_id
                                     );

                     /* Get the Unit Price Based on the Price List Associated to Line Item */
                     IF get_rma_ret_lines_rec.rentaloverridecharge IS NULL
                     THEN
                        --03/29/2012 CGonzalez: Corrected to pass the original lines unit price list price instead
                        /*l_unit_price :=  XXWC_RENTAL_ENGINE_PKG.GET_CHARGE_ITEM_UNIT_PRICE(l_charge_item,
                                                      Get_RMA_Line_Details_REC.link_to_line_id);*/
                        l_unit_price := get_rma_ret_lines_rec.unit_list_price;
                     ELSE
                        l_unit_price :=
                                   get_rma_ret_lines_rec.rentaloverridecharge;
                     END IF;

                     /* Get the SubInventory Details */
                     BEGIN
                        -- 07/12/2012 CG: Added to use new DFF for rental dates
                        l_tmp_rental_date := NULL;
                        l_tmp_rental_date :=
                           NVL (get_rma_ret_lines_rec.new_rental_date,
                                get_rma_ret_lines_rec.actual_shipment_date
                               );
                        xxwc_rental_engine_pkg.get_return_subinventory
                                             (get_rma_ret_lines_rec.header_id,
                                              l_rma_line_id
                                                           -- 07/12/2012 CG: Changed to accomodate Rental Dates DFF Changes
                                                           -- ,TRUNC(Get_RMA_RET_Lines_REC.actual_shipment_date)
                        ,
                                              l_tmp_rental_date,
                                              l_sub_inv,
                                              l_return_date,
                                              l_rental_days,
                                              l_rcv_qty
                                             );
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                           l_debug_msg :=
                              (   'ERROR while Obtaining Receiving Transactions Data for Line#'
                               || get_rma_ret_lines_rec.line_number
                              );
                           xxwc_rental_engine_pkg.log_msg
                                          (p_debug_level      => g_level_statement,
                                           p_mod_name         => l_module_name,
                                           p_debug_msg        => l_debug_msg
                                          );
                           RAISE skip_record;
                     END;

                     /* Get the Rental Charges */
                     -- 07/12/2012 CG: Added to use new DFF for rental dates
                     l_tmp_rental_date := NULL;
                     l_tmp_rental_date :=
                        NVL (get_rma_ret_lines_rec.new_rental_date,
                             get_rma_ret_lines_rec.actual_shipment_date
                            );
                     l_unit_selling_price :=
                        get_short_term_rental_charge
                                     (TRUNC (l_return_date)
                                                           -- 07/12/2012 CG: Changed to accomodate Rental Dates DFF Changes
                                                           --,TRUNC(Get_RMA_RET_Lines_REC.actual_shipment_date)
                        ,
                                      l_tmp_rental_date,
                                      l_unit_price,
                                      UPPER (l_sub_inv),
                                      l_charge_item,
                                      get_rma_ret_lines_rec.inventory_item_id,
                                      get_rma_ret_lines_rec.ship_from_org_id
                                     );

                     IF xxwc_rental_engine_pkg.get_wc_trxn_id (g_oel_bill_only) >
                                                                             0
                     THEN
                        create_charge_line
                                    (get_rma_ret_lines_rec.header_id,
                                     l_rma_line_id,
                                     l_unit_selling_price,
                                     l_rcv_qty,                   --l_rma_qty,
                                     l_charge_item,
                                     get_rma_ret_lines_rec.ship_from_org_id,
                                     l_unit_price,
                                     l_sub_inv,
                                     get_rma_ret_lines_rec.inventory_item_id,
                                     lx_charge_line_id,
                                     lx_return_status
                                    );
                        l_serial_status :=
                           xxwc_rental_engine_pkg.is_item_serialized
                              (l_rma_line_id
                                            --Get_Rental_Line_Info_REC.line_id
                                            ,
                               get_rma_ret_lines_rec.inventory_item_id,
                               get_rma_ret_lines_rec.ship_from_org_id,
                               g_rma_trxn_type,
                               UPPER (l_sub_inv),
                               g_rma_source_code
                              );

                        IF     (UPPER (l_sub_inv) IN
                                              (g_lost_sub_inv, g_dbr_sub_inv)
                               )
                           AND l_serial_status
                        THEN
                           fnd_file.put_line (fnd_file.LOG,
                                              'Creating Reservations.... '
                                             );
                           l_reservation_record.v_reservation_interface_id :=
                                          mtl_reservations_interface_s.NEXTVAL;
                           l_reservation_record.v_reservation_batch_id := 10;
                           l_reservation_record.v_requirement_date := SYSDATE;
                           l_reservation_record.v_organization_id :=
                                        get_rma_ret_lines_rec.ship_from_org_id;
                           l_reservation_record.v_inventory_item_id :=
                                       get_rma_ret_lines_rec.inventory_item_id;
                           l_reservation_record.v_demand_source_type_id := 2;
                           l_reservation_record.v_demand_source_header_id :=
                              xxwc_rental_engine_pkg.get_mtl_sales_order_id
                                           (get_rma_ret_lines_rec.order_number);
                           l_reservation_record.v_demand_source_line_id :=
                                                             lx_charge_line_id;
                                          -- Get_Rental_Line_Info_REC.line_id;
                           l_reservation_record.v_reservation_uom_code :=
                                      get_rma_ret_lines_rec.order_quantity_uom;
                           l_reservation_record.v_reservation_quantity :=
                                                                     l_rcv_qty;
                                 -- Get_Rental_Line_Info_REC.ordered_quantity;
                           l_reservation_record.v_subinventory_code :=
                                                                     l_sub_inv;
                           l_reservation_record.v_serial_number := NULL;
                           l_reservation_record.v_supply_source_type_id := 13;
                           l_reservation_record.v_row_status_code := 1;
                           l_reservation_record.v_reservation_action_code := 1;
                           l_reservation_record.v_transaction_mode := 3;
                           l_reservation_record.v_last_update_date := SYSDATE;
                           l_reservation_record.v_last_updated_by := g_user_id;
                           l_reservation_record.v_creation_date := SYSDATE;
                           l_reservation_record.v_created_by := g_user_id;
                           create_reservation_line
                              (l_reservation_record,
                               get_rma_line_details_rec.line_id
                                                               --l_rma_line_id
                                                               ,
                               get_rma_ret_lines_rec.inventory_item_id,
                               UPPER (l_sub_inv),
                               lx_return_status
                              );

                           IF lx_return_status = fnd_api.g_ret_sts_success
                           THEN
                              xxwc_rental_engine_pkg.log_msg
                                 (p_debug_level      => g_level_procedure,
                                  p_mod_name         => l_module_name,
                                  p_debug_msg        =>    'Reservation Line Created for Line ID '
                                                        || l_rma_line_id
                                 );
                              xxwc_rental_engine_pkg.log_msg
                                 (p_debug_level      => g_level_statement,
                                  p_mod_name         => l_module_name,
                                  p_debug_msg        => '###################################################'
                                 );
                           ELSE
                              xxwc_rental_engine_pkg.log_msg
                                 (p_debug_level      => g_level_error,
                                  p_mod_name         => l_module_name,
                                  p_debug_msg        => 'ERROR: Reservation Line Not created.'
                                 );
                              xxwc_rental_engine_pkg.log_msg
                                 (p_debug_level      => g_level_statement,
                                  p_mod_name         => l_module_name,
                                  p_debug_msg        => '###################################################'
                                 );
                           END IF;
                        END IF;

                        IF lx_return_status = fnd_api.g_ret_sts_success
                        THEN
                           l_debug_msg :=
                              (   'Charge Line Created for '
                               || get_rma_line_details_rec.line_number
                               || '.'
                               || get_rma_line_details_rec.shipment_number
                              );
                           xxwc_rental_engine_pkg.log_msg
                                          (p_debug_level      => g_level_statement,
                                           p_mod_name         => l_module_name,
                                           p_debug_msg        => l_debug_msg
                                          );

                           IF UPPER (l_sub_inv) IN
                                 (g_lost_sub_inv, g_dfr_sub_inv,
                                  g_dbr_sub_inv, g_rent_sale_sub_inv)  --Ver#1.4 20150727-00035 Added "g_rent_sale_sub_inv" to the condition
                           THEN
                              --Create 2nd Charge Line

                              -- 07/12/2012 CG: Added to use new DFF for rental dates
                              l_tmp_rental_date := NULL;
                              l_tmp_rental_date :=
                                 NVL
                                    (get_rma_ret_lines_rec.new_rental_date,
                                     get_rma_ret_lines_rec.actual_shipment_date
                                    );
                              l_unit_selling_price :=
                                 get_short_term_rental_charge
                                     (TRUNC (l_return_date)
                                                           -- 07/12/2012 CG: Changed to accomodate Rental Dates DFF Changes
                                                           -- ,TRUNC(Get_RMA_RET_Lines_REC.actual_shipment_date)
                                 ,
                                      l_tmp_rental_date,
                                      l_unit_price,
                                      'RENTAL',
                                      l_charge_item,
                                      get_rma_ret_lines_rec.inventory_item_id,
                                      get_rma_ret_lines_rec.ship_from_org_id
                                     );
                              create_charge_line
                                     (get_rma_ret_lines_rec.header_id,
                                      l_rma_line_id,
                                      l_unit_selling_price,
                                      l_rcv_qty,                  --l_rma_qty,
                                      l_charge_item,
                                      get_rma_ret_lines_rec.ship_from_org_id,
                                      l_unit_price,
                                      'Rental',
                                      get_rma_ret_lines_rec.inventory_item_id,
                                      lx_charge_line_id,
                                      lxx_return_status
                                     );

                              IF lxx_return_status = fnd_api.g_ret_sts_success
                              THEN
                                 l_debug_msg :=
                                    (   '2nd Charge Line Created for '
                                     || get_rma_line_details_rec.line_number
                                     || '.'
                                     || get_rma_line_details_rec.shipment_number
                                    );
                                 xxwc_rental_engine_pkg.log_msg
                                          (p_debug_level      => g_level_statement,
                                           p_mod_name         => l_module_name,
                                           p_debug_msg        => l_debug_msg
                                          );
                              END IF;
                           END IF;
                        END IF;
                     ELSE
                        l_debug_msg :=
                           (   'Transaction Type Not Defined for '
                            || g_oel_bill_only
                           );
                        xxwc_rental_engine_pkg.log_msg
                                          (p_debug_level      => g_level_statement,
                                           p_mod_name         => l_module_name,
                                           p_debug_msg        => l_debug_msg
                                          );
                        RAISE skip_record;
                     END IF;
                  ELSE
                     l_debug_msg :=
                        (   'Rental Charge Line Exists for '
                         || get_rma_line_details_rec.line_number
                         || '.'
                         || get_rma_line_details_rec.shipment_number
                        );
                     xxwc_rental_engine_pkg.log_msg
                                          (p_debug_level      => g_level_procedure,
                                           p_mod_name         => l_module_name,
                                           p_debug_msg        => l_debug_msg
                                          );
                  END IF;
               EXCEPTION
                  WHEN skip_record
                  THEN
                     l_debug_msg :=
                        (   '*********Skipping further Processing of Sales Order *********'
                         || get_rma_ret_lines_rec.order_number
                        );
                     xxwc_rental_engine_pkg.log_msg
                                          (p_debug_level      => g_level_statement,
                                           p_mod_name         => l_module_name,
                                           p_debug_msg        => l_debug_msg
                                          );
                     retcode := '1';
                  WHEN validation_error
                  THEN
                     --l_Debug_Msg := fnd_message.get;
                     l_debug_msg := SQLERRM;
                     xxwc_rental_engine_pkg.log_msg
                                         (p_debug_level      => g_level_statement,
                                          p_mod_name         => l_module_name,
                                          p_debug_msg        => l_debug_msg
                                         );
                     retcode := '1';
                     errbuf := l_debug_msg;
                  WHEN NO_DATA_FOUND
                  THEN
                     l_debug_msg :=
                        ('*********NO Sales Orders Found to Process  *********');
                     xxwc_rental_engine_pkg.log_msg
                                         (p_debug_level      => g_level_statement,
                                          p_mod_name         => l_module_name,
                                          p_debug_msg        => l_debug_msg
                                         );
                     retcode := '1';
                     errbuf := l_debug_msg;
                  WHEN OTHERS
                  THEN
                     l_debug_msg := SQLERRM;
                     xxwc_rental_engine_pkg.log_msg
                                         (p_debug_level      => g_level_statement,
                                          p_mod_name         => l_module_name,
                                          p_debug_msg        => l_debug_msg
                                         );
                     retcode := '1';
                     errbuf := SQLERRM;
               END;
            END LOOP;
         END;

         l_debug_msg :=
            (   '*********End of Processing Sales Order *********'
             || get_rma_ret_lines_rec.order_number
            );
         xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_procedure,
                                         p_mod_name         => l_module_name,
                                         p_debug_msg        => l_debug_msg
                                        );
      END LOOP;
   END main;

   /*************************************************************************
     *   Procedure : CREATE_CHARGE_LINE
     *
     *   PURPOSE:   This Procedure Creates Charge Lines
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
     *   1.0        09/29/2011  Srinivas Malkapuram             Initial Version
     * ************************************************************************/
   PROCEDURE create_charge_line (
      p_header_id            IN              NUMBER,
      p_charge_line_id       IN              NUMBER,
      p_unit_selling_price   IN              NUMBER,
      p_rma_qty              IN              NUMBER,
      p_charge_item          IN              NUMBER,
      p_ship_from_org_id     IN              NUMBER,
      p_unit_list_price      IN              NUMBER,
      p_return_subinv        IN              VARCHAR2,
      p_inventory_item_id    IN              NUMBER,
      lx_charge_line_id      OUT NOCOPY      NUMBER,
      lx_return_status       OUT NOCOPY      VARCHAR2
   )
   IS
      v_api_version_number           NUMBER                              := 1;
      v_return_status                VARCHAR2 (2000);
      v_msg_count                    NUMBER;
      v_msg_data                     VARCHAR2 (2000);
      v_msg_index                    NUMBER;
      v_data                         VARCHAR2 (2000);
      v_loop_count                   NUMBER;
      v_debug_file                   VARCHAR2 (200);
      b_return_status                VARCHAR2 (200);
      b_msg_count                    NUMBER;
      b_msg_data                     VARCHAR2 (2000);
      l_hold_return_status           VARCHAR2 (20);
      l_item_description             VARCHAR2 (50);
      l_api_name                     VARCHAR2 (30)    := 'CREATE_CHARGE_LINE';
      l_module_name                  VARCHAR2 (120);
      -- IN Variables --
      v_header_rec                   oe_order_pub.header_rec_type;
      v_line_tbl                     oe_order_pub.line_tbl_type;
      v_action_request_tbl           oe_order_pub.request_tbl_type;
      v_line_adj_tbl                 oe_order_pub.line_adj_tbl_type;
      -- OUT Variables --
      v_header_rec_out               oe_order_pub.header_rec_type;
      v_header_val_rec_out           oe_order_pub.header_val_rec_type;
      v_header_adj_tbl_out           oe_order_pub.header_adj_tbl_type;
      v_header_adj_val_tbl_out       oe_order_pub.header_adj_val_tbl_type;
      v_header_price_att_tbl_out     oe_order_pub.header_price_att_tbl_type;
      v_header_adj_att_tbl_out       oe_order_pub.header_adj_att_tbl_type;
      v_header_adj_assoc_tbl_out     oe_order_pub.header_adj_assoc_tbl_type;
      v_header_scredit_tbl_out       oe_order_pub.header_scredit_tbl_type;
      v_header_scredit_val_tbl_out   oe_order_pub.header_scredit_val_tbl_type;
      v_line_tbl_out                 oe_order_pub.line_tbl_type;
      v_line_val_tbl_out             oe_order_pub.line_val_tbl_type;
      v_line_adj_tbl_out             oe_order_pub.line_adj_tbl_type;
      v_line_adj_val_tbl_out         oe_order_pub.line_adj_val_tbl_type;
      v_line_price_att_tbl_out       oe_order_pub.line_price_att_tbl_type;
      v_line_adj_att_tbl_out         oe_order_pub.line_adj_att_tbl_type;
      v_line_adj_assoc_tbl_out       oe_order_pub.line_adj_assoc_tbl_type;
      v_line_scredit_tbl_out         oe_order_pub.line_scredit_tbl_type;
      v_line_scredit_val_tbl_out     oe_order_pub.line_scredit_val_tbl_type;
      v_lot_serial_tbl_out           oe_order_pub.lot_serial_tbl_type;
      v_lot_serial_val_tbl_out       oe_order_pub.lot_serial_val_tbl_type;
      v_action_request_tbl_out       oe_order_pub.request_tbl_type;

-- Version# 1.1 > Start
   l_est_ret_date           VARCHAR2(240);
   l_serial_num             VARCHAR2(240);
   l_rental_date            VARCHAR2(240);
   l_shipping_instructions  VARCHAR2(2000);
   l_packing_instructions   VARCHAR2(2000);
-- Version# 1.1 < End

   BEGIN
      l_module_name := g_module_name || '.' || l_api_name;
      xxwc_rental_engine_pkg.log_msg
                               (p_debug_level      => g_level_procedure,
                                p_mod_name         => l_module_name,
                                p_debug_msg        => 'Inside Creating Charge Line '
                               );
      v_action_request_tbl (1) := oe_order_pub.g_miss_request_rec;
      -- Line Record --
      v_line_tbl (1) := oe_order_pub.g_miss_line_rec;
      v_line_tbl (1).operation := oe_globals.g_opr_create;
      v_line_tbl (1).header_id := p_header_id;
      v_line_tbl (1).inventory_item_id := p_charge_item;
      v_line_tbl (1).ordered_quantity := p_rma_qty;
      v_line_tbl (1).unit_selling_price := p_unit_selling_price;
      v_line_tbl (1).calculate_price_flag := 'N';
      v_line_tbl (1).ship_from_org_id := p_ship_from_org_id;
      v_line_tbl (1).link_to_line_id := p_charge_line_id;
      v_line_tbl (1).line_type_id :=
                       xxwc_rental_engine_pkg.get_wc_trxn_id (g_oel_bill_only);
      v_line_tbl (1).unit_list_price := p_unit_selling_price;
                                                          --p_Unit_List_Price;
      v_line_tbl (1).subinventory := p_return_subinv;

      IF UPPER (p_return_subinv) IN (g_lost_sub_inv, g_dbr_sub_inv, g_rent_sale_sub_inv)   --Ver#1.4 20150727-00035 Added "g_rent_sale_sub_inv" to the condition
      THEN
         v_line_tbl (1).inventory_item_id := p_inventory_item_id;
         v_line_tbl (1).line_type_id :=
                   xxwc_rental_engine_pkg.get_wc_trxn_id (g_oel_wc_bill_only);
         v_line_tbl (1).schedule_ship_date := SYSDATE;
      END IF;

      /* Get Item Description */
      l_item_description :=
                xxwc_rental_engine_pkg.get_item_description (p_charge_line_id);

      IF UPPER (p_return_subinv) = g_lost_sub_inv
      THEN
         v_line_tbl (1).user_item_description :=
                                'Lost Charge for Item ' || l_item_description;
      ELSIF UPPER (p_return_subinv) = g_dfr_sub_inv
      THEN
         v_line_tbl (1).user_item_description :=
                   'Charge for Damaged But Repairable ' || l_item_description;
      ELSIF UPPER (p_return_subinv) = g_dbr_sub_inv
      THEN
         v_line_tbl (1).user_item_description :=
                'Charge for Damaged Beyond Repairable ' || l_item_description;

      --Ver#1.4 20150727-00035 Added for new subinv definition  --Start
      ELSIF UPPER (p_return_subinv) = g_rent_sale_sub_inv
      THEN
         v_line_tbl (1).user_item_description :=
                                 'Sale of Rental Item ' || l_item_description;
      --Ver#1.4 20150727-00035 Added for new subinv definition  --End

      ELSE
         v_line_tbl (1).user_item_description :=
                              'Rental Charge for Item ' || l_item_description;
      END IF;

      -- Version# 1.1 > Start
        BEGIN
        SELECT attribute1
             , attribute7
--             , attribute12 TMS # 20140107-00054 V1.2
             , shipping_instructions
             , packing_instructions
          INTO l_est_ret_date
             , l_serial_num
--             , l_rental_date TMS # 20140107-00054 V1.2
             , l_shipping_instructions
             , l_packing_instructions
          FROM apps.oe_order_lines
         WHERE line_id = p_charge_line_id;

--         fnd_file.put_line (fnd_file.LOG,'l_est_ret_date - ' || l_est_ret_date);
--         fnd_file.put_line (fnd_file.LOG,'l_serial_num - ' || l_serial_num);
--         fnd_file.put_line (fnd_file.LOG,'l_rental_date - ' || l_rental_date);
--         fnd_file.put_line (fnd_file.LOG,'l_shipping_instructions - ' || l_shipping_instructions);
--         fnd_file.put_line (fnd_file.LOG,'l_packing_instructions - ' || l_packing_instructions);

        EXCEPTION
        WHEN OTHERS THEN
         fnd_file.put_line (fnd_file.LOG,'Error deriving ShippingInstructions from ItemLine ' || SQLERRM);
        END;


        v_line_tbl (1).attribute1             := l_est_ret_date;
        v_line_tbl (1).attribute7             := l_serial_num;
--        v_line_tbl (1).attribute12            := l_rental_date; TMS # 20140107-00054 V1.2
        v_line_tbl (1).shipping_instructions  := l_shipping_instructions;
        v_line_tbl (1).packing_instructions   := l_packing_instructions;
      -- Version# 1.1 < End

      -- Calling the API to add a new line to an existing Order --
      oe_order_pub.process_order
                    (p_api_version_number          => v_api_version_number,
                     p_header_rec                  => v_header_rec,
                     p_line_tbl                    => v_line_tbl,
                     p_action_request_tbl          => v_action_request_tbl,
                     p_line_adj_tbl                => v_line_adj_tbl,
                     x_header_rec                  => v_header_rec_out,
                                                               --Out Variables
                     x_header_val_rec              => v_header_val_rec_out,
                     x_header_adj_tbl              => v_header_adj_tbl_out,
                     x_header_adj_val_tbl          => v_header_adj_val_tbl_out,
                     x_header_price_att_tbl        => v_header_price_att_tbl_out,
                     x_header_adj_att_tbl          => v_header_adj_att_tbl_out,
                     x_header_adj_assoc_tbl        => v_header_adj_assoc_tbl_out,
                     x_header_scredit_tbl          => v_header_scredit_tbl_out,
                     x_header_scredit_val_tbl      => v_header_scredit_val_tbl_out,
                     x_line_tbl                    => v_line_tbl_out,
                     x_line_val_tbl                => v_line_val_tbl_out,
                     x_line_adj_tbl                => v_line_adj_tbl_out,
                     x_line_adj_val_tbl            => v_line_adj_val_tbl_out,
                     x_line_price_att_tbl          => v_line_price_att_tbl_out,
                     x_line_adj_att_tbl            => v_line_adj_att_tbl_out,
                     x_line_adj_assoc_tbl          => v_line_adj_assoc_tbl_out,
                     x_line_scredit_tbl            => v_line_scredit_tbl_out,
                     x_line_scredit_val_tbl        => v_line_scredit_val_tbl_out,
                     x_lot_serial_tbl              => v_lot_serial_tbl_out,
                     x_lot_serial_val_tbl          => v_lot_serial_val_tbl_out,
                     x_action_request_tbl          => v_action_request_tbl_out,
                     x_return_status               => v_return_status,
                     x_msg_count                   => v_msg_count,
                     x_msg_data                    => v_msg_data
                    );

      IF v_return_status = fnd_api.g_ret_sts_success
      THEN
         COMMIT;
         lx_return_status := 'S';
         lx_charge_line_id := v_line_tbl_out (1).line_id;
         xxwc_rental_engine_pkg.log_msg
                   (p_debug_level      => g_level_statement,
                    p_mod_name         => l_module_name,
                    p_debug_msg        => 'Line Addition to Existing Order Success '
                   );
         xxwc_rental_engine_pkg.apply_hold_charge_line
                                                   (p_header_id,
                                                    v_line_tbl_out (1).line_id,
                                                    l_hold_return_status
                                                   );
      ELSE
         lx_return_status := 'E';
         xxwc_rental_engine_pkg.log_msg
                 (p_debug_level      => g_level_statement,
                  p_mod_name         => l_module_name,
                  p_debug_msg        =>    'Line Addition to Existing Order Failed '
                                        || v_msg_data
                 );
         ROLLBACK;

         FOR i IN 1 .. b_msg_count
         LOOP
            b_msg_data := oe_msg_pub.get (p_msg_index      => i,
                                          p_encoded        => 'F');
            fnd_file.put_line (fnd_file.LOG, i || ') ' || b_msg_data);
         END LOOP;
      END IF;
   END create_charge_line;

/*************************************************************************
   *   Function : GET_SHORT_TERM_RENTAL_CHARGE
   *
   *   PURPOSE:   This Function Returns Rental Charge
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        09/29/2011  Srinivas Malkapuram             Initial Version
   * ************************************************************************/
   FUNCTION get_short_term_rental_charge (
      p_returndate           IN   DATE,
      p_actualshipmentdate   IN   DATE,
      p_unitprice            IN   NUMBER,
      p_subinventory         IN   VARCHAR2,
      p_chargeitem           IN   NUMBER,
      p_rentalitem           IN   NUMBER,
      p_organizationid       IN   NUMBER
   )
      RETURN NUMBER
   IS
      l_api_name             VARCHAR2 (30)  := 'GET_SHORT_TERM_RENTAL_CHARGE';
      l_module_name          VARCHAR2 (120);
      l_totalrentaldays      NUMBER;
      l_weeks                NUMBER;
      l_extradays            NUMBER;
      l_rentalcharge         NUMBER;
      l_debug_msg            VARCHAR2 (2000);
      -- 03/29/2012 CG Added to determine the standard item for the rentatl item
      l_rental_item_number   VARCHAR2 (40);
      l_standard_item        VARCHAR2 (40);
      l_standard_item_id     NUMBER;
   BEGIN
      l_module_name := g_module_name || '.' || l_api_name;
      xxwc_rental_engine_pkg.log_msg
                     (p_debug_level      => g_level_procedure,
                      p_mod_name         => l_module_name,
                      p_debug_msg        => 'Getting Charge for Short Term Rentals '
                     );
      -- 03/29/2012 CGonzalez: Added to determine the base item for the rental item
      l_rental_item_number := NULL;
      l_standard_item := NULL;
      l_standard_item_id := NULL;

      BEGIN
         SELECT segment1
           INTO l_rental_item_number
           FROM mtl_system_items_b
          WHERE inventory_item_id = p_rentalitem
            AND organization_id = p_organizationid;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_rental_item_number := NULL;
      END;

      IF l_rental_item_number IS NOT NULL
      THEN
         BEGIN
            SELECT inventory_item_id, segment1
              INTO l_standard_item_id, l_standard_item
              FROM mtl_system_items_b
             WHERE organization_id = p_organizationid
               AND segment1 =
                      (CASE
                          WHEN l_rental_item_number LIKE 'RR%'
                             THEN SUBSTR (l_rental_item_number, 3)
                          WHEN l_rental_item_number LIKE 'R%'
                             THEN SUBSTR (l_rental_item_number, 2)
                          ELSE l_rental_item_number
                       END
                      );
         EXCEPTION
            WHEN OTHERS
            THEN
               l_standard_item_id := p_rentalitem;
               l_standard_item := l_rental_item_number;
         END;
      ELSE
         l_standard_item_id := p_rentalitem;
      END IF;

      xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_procedure,
                                      p_mod_name         => l_module_name,
                                      p_debug_msg        =>    'Rental Item '
                                                            || l_rental_item_number
                                                            || ' - '
                                                            || p_rentalitem
                                     );
      xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_procedure,
                                      p_mod_name         => l_module_name,
                                      p_debug_msg        =>    'Standard Item '
                                                            || l_standard_item
                                                            || ' - '
                                                            || l_standard_item_id
                                     );

      -- 03/29/2012 CGonzalez
      SELECT (p_returndate - p_actualshipmentdate) + 1
        INTO l_totalrentaldays
        FROM DUAL;

      SELECT FLOOR (l_totalrentaldays / 7),
             CASE
                WHEN MOD (l_totalrentaldays, 7) = 5
                   THEN MOD (l_totalrentaldays, 7) - 1
                WHEN MOD (l_totalrentaldays, 7) = 6
                   THEN MOD (l_totalrentaldays, 7) - 2
                WHEN MOD (l_totalrentaldays, 7) = 7
                   THEN MOD (l_totalrentaldays, 7) - 3
                ELSE MOD (l_totalrentaldays, 7)
             END extradays
        INTO l_weeks,
             l_extradays
        FROM DUAL;

      IF UPPER (p_subinventory) IN (g_lost_sub_inv, g_dbr_sub_inv)
      THEN
         BEGIN
            SELECT   item_cost
                   * NVL (fnd_profile.VALUE ('XXWC_RENTAL_SUBINV_DMG_CHARGES'),
                          1
                         )
              INTO l_rentalcharge
              FROM cst_item_costs
             WHERE inventory_item_id =
                               l_standard_item_id
                                                 -- 03/29/2012 CG p_RentalItem
               AND organization_id = p_organizationid
               AND cost_type_id = 2;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               l_debug_msg :=
                  (   'Lost Sub - No Data Found for Item Avg Cost for '
                   || p_rentalitem
                   || ' in Organization id '
                   || p_organizationid
                  );
               xxwc_rental_engine_pkg.log_msg
                                          (p_debug_level      => g_level_statement,
                                           p_mod_name         => l_module_name,
                                           p_debug_msg        => l_debug_msg
                                          );
               -- 04/24/2012 CGonzalez: Fro m UAT2 testing when not found return 300
               --l_RentalCharge := 0;
               l_rentalcharge := 300;
            WHEN TOO_MANY_ROWS
            THEN
               l_debug_msg :=
                  (   'Lost Sub - Duplicate Data Found for Item Avg Cost for '
                   || p_rentalitem
                   || ' in Organization id '
                   || p_organizationid
                  );
               xxwc_rental_engine_pkg.log_msg
                                          (p_debug_level      => g_level_statement,
                                           p_mod_name         => l_module_name,
                                           p_debug_msg        => l_debug_msg
                                          );
               -- 04/24/2012 CGonzalez: Fro m UAT2 testing when not found return 300
               --l_RentalCharge := 0;
               l_rentalcharge := 300;
            WHEN OTHERS
            THEN
               l_debug_msg :=
                  (   'Lost Sub - ERROR: Obtaining Data for Item Avg Cost for '
                   || p_rentalitem
                   || ' in Organization id '
                   || p_organizationid
                  );
               xxwc_rental_engine_pkg.log_msg
                                          (p_debug_level      => g_level_statement,
                                           p_mod_name         => l_module_name,
                                           p_debug_msg        => l_debug_msg
                                          );
               -- 04/24/2012 CGonzalez: Fro m UAT2 testing when not found return 300
               --l_RentalCharge := 0;
               l_rentalcharge := 300;
         END;
      ELSIF UPPER (p_subinventory) = g_dfr_sub_inv
      THEN
         BEGIN
            SELECT item_cost
              INTO l_rentalcharge
              FROM cst_item_costs
             WHERE inventory_item_id =
                               l_standard_item_id
                                                 -- 03/29/2012 CG p_RentalItem
               AND organization_id = p_organizationid
               AND cost_type_id = 2;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               l_debug_msg :=
                  (   'Reg Sub - No Data Found for Item Avg Cost for '
                   || p_rentalitem
                   || ' in Organization id '
                   || p_organizationid
                  );
               xxwc_rental_engine_pkg.log_msg
                                          (p_debug_level      => g_level_statement,
                                           p_mod_name         => l_module_name,
                                           p_debug_msg        => l_debug_msg
                                          );
               -- 04/24/2012 CGonzalez: Fro m UAT2 testing when not found return 300
               --l_RentalCharge := 0;
               l_rentalcharge := 300;
            WHEN TOO_MANY_ROWS
            THEN
               l_debug_msg :=
                  (   'Reg Sub - Duplicate Data Found for Item Avg Cost for '
                   || p_rentalitem
                   || ' in Organization id '
                   || p_organizationid
                  );
               xxwc_rental_engine_pkg.log_msg
                                          (p_debug_level      => g_level_statement,
                                           p_mod_name         => l_module_name,
                                           p_debug_msg        => l_debug_msg
                                          );
               -- 04/24/2012 CGonzalez: Fro m UAT2 testing when not found return 300
               --l_RentalCharge := 0;
               l_rentalcharge := 300;
            WHEN OTHERS
            THEN
               l_debug_msg :=
                  (   'Reg Sub - ERROR: Obtaining Data for Item Avg Cost for '
                   || p_rentalitem
                   || ' in Organization id '
                   || p_organizationid
                  );
               xxwc_rental_engine_pkg.log_msg
                                          (p_debug_level      => g_level_statement,
                                           p_mod_name         => l_module_name,
                                           p_debug_msg        => l_debug_msg
                                          );
               -- 04/24/2012 CGonzalez: Fro m UAT2 testing when not found return 300
               --l_RentalCharge := 0;
               l_rentalcharge := 300;
         END;
      ELSE
         l_rentalcharge :=
                      (p_unitprice * 4 * l_weeks)
                    + (p_unitprice * l_extradays);
      END IF;

      xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_procedure,
                                      p_mod_name         => l_module_name,
                                      p_debug_msg        =>    'Rental Charge is '
                                                            || l_rentalcharge
                                     );
/* TMS: 20140220-00056 update  SELECT DECODE (l_rentalcharge, 0, 300, l_rentalcharge) to  SELECT DECODE (l_rentalcharge, 0, 0, l_rentalcharge)   */
      IF l_rentalcharge = 0
      THEN
         SELECT DECODE (l_rentalcharge, 0, 0, l_rentalcharge)
           INTO l_rentalcharge
           FROM DUAL;
      END IF;

      RETURN l_rentalcharge;
   END get_short_term_rental_charge;

   PROCEDURE create_reservation_line (
      v_reservation_record   IN              reservation_int_rec,
      p_rma_line_id          IN              NUMBER,
      p_inventory_item_id    IN              NUMBER,
      p_return_subinv        IN              VARCHAR2,
      lx_return_status       OUT NOCOPY      VARCHAR2
   )
   IS
      v_reservation_rec            inv_reservation_global.mtl_reservation_rec_type;
      v_reserv_serial_rec          inv_reservation_global.serial_number_tbl_type;
      v_reservation_quantity       NUMBER;
      v_reservation_interface_id   NUMBER;
      l_reservation_id             NUMBER;
      lx_msg_count                 NUMBER;
      lx_msg_data                  VARCHAR2 (2000);
      l_reserv_serial_rec          inv_reservation_global.serial_number_tbl_type;
      l_api_name                   VARCHAR2 (30) := 'CREATE_RESERVATION_LINE';
      l_module_name                VARCHAR2 (120)
                                                 := 'Calling Reservation API';
      l_debug_msg                  VARCHAR2 (2000);
      serial_ctr                   NUMBER                                := 1;

/* Cursor to get the Serial Numbers */
      CURSOR cur_get_serial_no (
         p_line_id                 NUMBER,
         p_inventory_item_id       NUMBER,
         p_mtl_transaction_type    VARCHAR2,
         p_mtl_subinventory_code   VARCHAR2,
         p_mtl_source_code         VARCHAR2
      )
      IS
         SELECT msn.serial_number
           FROM mtl_serial_numbers msn,
                mtl_material_transactions mmt,
                mtl_transaction_types mtt
          WHERE mmt.transaction_id = msn.last_transaction_id
            AND mmt.inventory_item_id = p_inventory_item_id
            AND mmt.trx_source_line_id = p_line_id
            AND mmt.transaction_type_id = mtt.transaction_type_id
            AND UPPER (mtt.transaction_type_name) = p_mtl_transaction_type
            AND UPPER (mmt.subinventory_code) = p_mtl_subinventory_code
            AND UPPER (mmt.source_code) = p_mtl_source_code;
   BEGIN
      v_reservation_quantity := v_reservation_record.v_reservation_quantity;
      v_reservation_interface_id :=
                              v_reservation_record.v_reservation_interface_id;

      FOR get_get_serial_no IN cur_get_serial_no (p_rma_line_id,
                                                  p_inventory_item_id,
                                                  g_rma_trxn_type,
                                                  p_return_subinv,
                                                  g_rma_source_code
                                                 )
      LOOP
         v_reserv_serial_rec (serial_ctr).serial_number :=
                                              get_get_serial_no.serial_number;
         fnd_file.put_line (fnd_file.LOG,
                               'Reservation serial Num for '
                            || ' Counter '
                            || serial_ctr
                            || ','
                            || v_reserv_serial_rec (serial_ctr).serial_number
                           );
         v_reserv_serial_rec (serial_ctr).inventory_item_id :=
                                                           p_inventory_item_id;
         serial_ctr := serial_ctr + 1;
      END LOOP;

      v_reservation_rec.requirement_date :=
                                       v_reservation_record.v_requirement_date;
      v_reservation_rec.organization_id :=
                                        v_reservation_record.v_organization_id;
      v_reservation_rec.inventory_item_id :=
                                      v_reservation_record.v_inventory_item_id;
      v_reservation_rec.demand_source_type_id :=
                                  v_reservation_record.v_demand_source_type_id;
      v_reservation_rec.demand_source_name := NULL;
      v_reservation_rec.demand_source_header_id :=
                                v_reservation_record.v_demand_source_header_id;
      v_reservation_rec.demand_source_line_id :=
                                  v_reservation_record.v_demand_source_line_id;
      v_reservation_rec.primary_uom_code :=
                                   v_reservation_record.v_reservation_uom_code;
      v_reservation_rec.primary_uom_id := NULL;
      v_reservation_rec.reservation_uom_code :=
                                   v_reservation_record.v_reservation_uom_code;
      v_reservation_rec.reservation_uom_id := NULL;
      v_reservation_rec.reservation_quantity :=
                                   v_reservation_record.v_reservation_quantity;
      v_reservation_rec.primary_reservation_quantity :=
                                   v_reservation_record.v_reservation_quantity;
      v_reservation_rec.supply_source_type_id :=
                                  v_reservation_record.v_supply_source_type_id;
      v_reservation_rec.ship_ready_flag := 1;
      v_reservation_rec.attribute15 := NULL;
      v_reservation_rec.attribute14 := NULL;
      v_reservation_rec.attribute13 := NULL;
      v_reservation_rec.attribute12 := NULL;
      v_reservation_rec.attribute11 := NULL;
      v_reservation_rec.attribute10 := NULL;
      v_reservation_rec.attribute9 := NULL;
      v_reservation_rec.attribute8 := NULL;
      v_reservation_rec.attribute7 := NULL;
      v_reservation_rec.attribute6 := NULL;
      v_reservation_rec.attribute5 := NULL;
      v_reservation_rec.attribute4 := NULL;
      v_reservation_rec.attribute3 := NULL;
      v_reservation_rec.attribute2 := NULL;
      v_reservation_rec.attribute1 := NULL;
      v_reservation_rec.attribute_category := NULL;
      v_reservation_rec.lpn_id := NULL;
      v_reservation_rec.pick_slip_number := NULL;
      v_reservation_rec.lot_number_id := NULL;
      v_reservation_rec.lot_number := NULL;
      v_reservation_rec.locator_id := NULL;
      v_reservation_rec.subinventory_id := NULL;
      v_reservation_rec.subinventory_code :=
                                      v_reservation_record.v_subinventory_code;
      v_reservation_rec.revision := NULL;
      v_reservation_rec.supply_source_line_id := NULL;
      v_reservation_rec.supply_source_header_id := NULL;
      v_reservation_rec.supply_source_line_detail := NULL;
      v_reservation_rec.supply_source_name := NULL;
      v_reservation_rec.external_source_line_id := NULL;
      v_reservation_rec.external_source_code := NULL;
      v_reservation_rec.autodetail_group_id := NULL;
      v_reservation_rec.demand_source_delivery := NULL;
      inv_reservation_pub.create_reservation
                               (p_api_version_number      => 1.0,
                                x_return_status           => lx_return_status,
                                x_msg_count               => lx_msg_count,
                                x_msg_data                => lx_msg_data,
                                p_rsv_rec                 => v_reservation_rec,
                                p_serial_number           => v_reserv_serial_rec,
                                x_serial_number           => l_reserv_serial_rec,
                                x_quantity_reserved       => v_reservation_quantity,
                                x_reservation_id          => l_reservation_id
                               );

      IF lx_msg_count >= 1
      THEN
         FOR i IN 1 .. lx_msg_count
         LOOP
            l_debug_msg :=
                         fnd_msg_pub.get (p_msg_index      => i,
                                          p_encoded        => 'F');
            fnd_file.put_line (fnd_file.LOG, i || ') ' || l_debug_msg);
         END LOOP;
      ELSE
         fnd_file.put_line (fnd_file.LOG,
                            'Reservation ID Created ' || l_reservation_id
                           );
      END IF;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         l_debug_msg :=
            (   'No Data Found for Item ID  '
             || v_reservation_record.v_inventory_item_id
            );
         xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_statement,
                                         p_mod_name         => l_module_name,
                                         p_debug_msg        => l_debug_msg
                                        );
      WHEN TOO_MANY_ROWS
      THEN
         l_debug_msg :=
            (   'Duplicate Data for Item ID '
             || v_reservation_record.v_inventory_item_id
            );
         xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_statement,
                                         p_mod_name         => l_module_name,
                                         p_debug_msg        => l_debug_msg
                                        );
      WHEN OTHERS
      THEN
         l_debug_msg :=
            (   'ERROR while Creating Reservation '
             || v_reservation_record.v_inventory_item_id
            );
         xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_statement,
                                         p_mod_name         => l_module_name,
                                         p_debug_msg        => l_debug_msg
                                        );
   END create_reservation_line;
END xxwc_rental_create_billing_pkg;
/