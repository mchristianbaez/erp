CREATE OR REPLACE
PACKAGE apps.xxwc_rental_create_billing_pkg
AS
/*************************************************************************
   *  Copyright (c) 2011 Lucidity Consulting Group
   *  All rights reserved.
   **************************************************************************
   *   $Header XXWC_RENTAL_CREATE_BILLING_PKG .pkb $
   *   Module Name: XXWC_RENTAL_CREATE_BILLING_PKG .pkb
   *
   *   PURPOSE:   This package is used by the XXWC Create Rental Billing Conc Program
   *              to create Charge Lines for Short Term Rental Returns
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        09/29/2011  Srinivas Malkapuram      Initial Version
   *   1.1        12/10/2015  Kishorebabu V            TMS# 20150727-00035  Rental Short Term � Rental Sale is not decrementing in inventory
   * ***************************************************************************/
   c_level_unexpected                    NUMBER
                                         DEFAULT fnd_log.level_unexpected;
   c_level_exception                     NUMBER
                                         DEFAULT fnd_log.level_exception;
   c_level_event                         NUMBER   DEFAULT fnd_log.level_event;
   c_yes                        CONSTANT VARCHAR2 (1)   := 'Y';
   c_no                         CONSTANT VARCHAR2 (1)   := 'N';
   c_return_status_success      CONSTANT VARCHAR2 (1)   := 'S';
   c_return_status_error        CONSTANT VARCHAR2 (1)   := 'E';
   c_return_status_unexpected   CONSTANT VARCHAR2 (1)   := 'U';
   g_resp_name                           VARCHAR2 (100)
                                           := fnd_profile.VALUE ('RESP_NAME');
   g_resp_id                             NUMBER
                                             := fnd_profile.VALUE ('RESP_ID');
   g_user_id                             NUMBER
                                             := fnd_profile.VALUE ('USER_ID');
   g_module_name                CONSTANT VARCHAR2 (60)
                                    := 'PLSQL.XXWC_RENTAL_CREATE_BILLING_PKG';
   g_level_statement                     NUMBER
                                              DEFAULT fnd_log.level_statement;
   g_level_procedure                     NUMBER
                                              DEFAULT fnd_log.level_procedure;
   g_level_error                         NUMBER   DEFAULT fnd_log.level_error;
   g_oeh_attr1_long_term                 VARCHAR2 (40)  := 'LONG TERM';
   g_oeh_attr1_short_term                VARCHAR2 (40)  := 'SHORT TERM';
   g_oel_bill_only                       VARCHAR2 (40)  := 'BILL ONLY';
   g_oel_wc_bill_only                    VARCHAR2 (40)  := 'WC BILL ONLY';
   g_return_reason_code                  VARCHAR2 (40)  := 'RETURN_RETURN';
   g_lost_sub_inv                        VARCHAR2 (40)  := 'LOSTRENTAL';
   g_dfr_sub_inv                         VARCHAR2 (40)  := 'DRRENTAL';
   g_dbr_sub_inv                         VARCHAR2 (40)  := 'DBRENTAL';
   g_rma_trxn_type                       VARCHAR2 (40)  := 'RMA RECEIPT';
   g_rma_source_code                     VARCHAR2 (40)  := 'RCV';
   g_rent_sale_sub_inv                   VARCHAR2 (40)  := 'RENTALSALE';  --Ver#1.1 TMS# 20150727-00035

   TYPE reservation_int_rec IS RECORD (
      v_reservation_interface_id   NUMBER,
      v_reservation_batch_id       NUMBER,
      v_requirement_date           DATE,
      v_organization_id            NUMBER,
      v_inventory_item_id          NUMBER,
      v_demand_source_type_id      NUMBER,
      v_demand_source_header_id    NUMBER,
      v_demand_source_line_id      NUMBER,
      v_reservation_uom_code       VARCHAR2 (3),
      v_reservation_quantity       NUMBER,
      v_subinventory_code          VARCHAR2 (10),
      v_serial_number              VARCHAR2 (30),
      v_supply_source_type_id      NUMBER,
      v_row_status_code            NUMBER,
      v_reservation_action_code    NUMBER,
      v_transaction_mode           NUMBER,
      v_last_update_date           DATE,
      v_last_updated_by            NUMBER,
      v_creation_date              DATE,
      v_created_by                 NUMBER
   );

/*************************************************************************
   *   Procedure : MAIN
   *
   *   PURPOSE:   This Procedure is the Main Routine called by XXWC Create Rental Billing
   *          IN
   *              Order Number     --  Can run for Individual or ALL Orders
   *              p_Log_Enabled    --  More Detailed Log Messaging
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        09/29/2011  Srinivas Malkapuram             Initial Version
   * ************************************************************************/
   PROCEDURE main (
      errbuf           OUT      VARCHAR2,
      retcode          OUT      NUMBER,
      p_order_number   IN       VARCHAR2,
      p_log_enabled    IN       VARCHAR2 DEFAULT 'N'
   );

/*************************************************************************
   *   Procedure : CREATE_CHARGE_LINE
   *
   *   PURPOSE:   This Procedure Creates Charge Lines
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        09/29/2011  Srinivas Malkapuram             Initial Version
   * ************************************************************************/
   PROCEDURE create_charge_line (
      p_header_id            IN              NUMBER,
      p_charge_line_id       IN              NUMBER,
      p_unit_selling_price   IN              NUMBER,
      p_rma_qty              IN              NUMBER,
      p_charge_item          IN              NUMBER,
      p_ship_from_org_id     IN              NUMBER,
      p_unit_list_price      IN              NUMBER,
      p_return_subinv        IN              VARCHAR2,
      p_inventory_item_id    IN              NUMBER,
      lx_charge_line_id      OUT NOCOPY      NUMBER,
      lx_return_status       OUT NOCOPY      VARCHAR2
   );

/*************************************************************************
   *   Function : GET_SHORT_TERM_RENTAL_CHARGE
   *
   *   PURPOSE:   This Function Returns Rental Charge
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        09/29/2011  Srinivas Malkapuram             Initial Version
   * ************************************************************************/
   FUNCTION get_short_term_rental_charge (
      p_returndate           IN   DATE,
      p_actualshipmentdate   IN   DATE,
      p_unitprice            IN   NUMBER,
      p_subinventory         IN   VARCHAR2,
      p_chargeitem           IN   NUMBER,
      p_rentalitem           IN   NUMBER,
      p_organizationid       IN   NUMBER
   )
      RETURN NUMBER;

   PROCEDURE create_reservation_line (
      v_reservation_record   IN              reservation_int_rec,
      p_rma_line_id          IN              NUMBER,
      p_inventory_item_id    IN              NUMBER,
      p_return_subinv        IN              VARCHAR2,
      lx_return_status       OUT NOCOPY      VARCHAR2
   );
END xxwc_rental_create_billing_pkg;
/