/******************************************************************************
     $Header XXWC_OM_DMS2_POD_DBDIR$
     Module Name: XXWC_OM_DMS2_POD_DBDIR.sql

     PURPOSE:   DESCARTES Project to create db directory

     REVISIONS:
     Ver        Date        Author                Description
     ---------  ----------  ---------------       -----------------------------
     1.0        12/19/2017  Rakesh Patel          TMS#20170901-00010 DMS Phase-2.0 Inbound Extract
*******************************************************************************/
DECLARE
l_db_name VARCHAR2(20) :=NULL;
l_path VARCHAR2(240) :=NULL;
l_sql VARCHAR2(240) :=NULL;
BEGIN

SELECT lower(name) 
  INTO l_db_name
  FROM v$database;
  
  l_path :='/xx_iface/'||l_db_name||'/inbound/dms2/pod';
  
  l_sql :='CREATE OR REPLACE DIRECTORY XXWC_DMS2_POD_FILES as'||' '||''''||l_path||'''';

  dbms_output.put_line('DBA Directory Path: '||l_path);
  dbms_output.put_line(' ');
  dbms_output.put_line('SQL: '||l_sql);
  dbms_output.put_line(' ');  
  dbms_output.put_line('Begin setup of directory XXWC_DMS2_POD_FILES');  
  EXECUTE IMMEDIATE  l_sql;
  dbms_output.put_line('End setup of directory XXWC_DMS2_POD_FILES');    
  
  l_path :='/xx_iface/'||l_db_name||'/inbound/dms2/pod_images';
  
  l_sql :='CREATE OR REPLACE DIRECTORY XXWC_DMS2_POD_IMAGES as'||' '||''''||l_path||'''';

  dbms_output.put_line('DBA Directory Path: '||l_path);
  dbms_output.put_line(' ');
  dbms_output.put_line('SQL: '||l_sql);
  dbms_output.put_line(' ');  
  dbms_output.put_line('Begin setup of directory XXWC_DMS2_POD_IMAGES');  
  EXECUTE IMMEDIATE  l_sql;
  dbms_output.put_line('End setup of directory XXWC_DMS2_POD_IMAGES');    
END;
/