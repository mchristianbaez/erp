CREATE OR REPLACE PACKAGE APPS.XXWC_ONT_ROUTINES_PKG
AS
/*************************************************************************
  $Header xxwc_ont_routines_pkg$
  Module Name: xxwc_ont_routines_pkg.pks

  PURPOSE:   This package will contain all Order Management specific routines

  REVISIONS:
  Ver        Date        Author                     Description
  ---------  ----------  ---------------         -------------------------
  1.0       5/9/2012     LCG Consulting           Initial Version
  2.0       8/30/2013    Ram Talluri              Task ID: 20130830-00571 added Parameter P_INCL_HAWAII_BRANCHES
  3.0      9/5/2013     Ram Talluri               Task ID: 20130830-00571 added Parameter P_INCL_HAWAII_BRANCHES - Removed LCG copy right comment
  4.0        2/2/2014     Gopi Damuluri           TMS# 20130912-00853
                                                  Performance improvement for "XXWC: OM Procedure to Release Pricing Change Hold" process.
  5.0        02/09/2015   Gopi Damuluri           TMS# 20150210-00007 Performance Tuning - XXWC_ONT_ROUTINES_PKG.CHECK_PRINT_PRICE
                                                  TMS# 20141119-00027 Performance Tuning - XXWC_ONT_ROUTINES_PKG.COUNT_ORDER_HOLDS
  9.0        01/23/2017   Rakesh Patel            TMS# 20161116-00261 User item description not updated based on the line status
  12.0       04/28/2017   Niraj K Ranjan          TMS#20160815-00078   Branch Visit - Pick Ticket Reprint Improvements
  13.0       12/13/2017   Rakesh Patel            TMS#20170811-00005-DMS Phase-2.0 Outbound Extract(Standard and Internal orders)
**************************************************************************/
   -- This package will contain all Order Management specific routines
   --Define variables for logging debug messages
   c_level_unexpected        CONSTANT NUMBER := 6;
   c_level_error             CONSTANT NUMBER := 5;
   c_level_exception         CONSTANT NUMBER := 4;
   c_level_event             CONSTANT NUMBER := 3;
   c_level_procedure         CONSTANT NUMBER := 2;
   c_level_statement         CONSTANT NUMBER := 1;
   c_current_runtime_level   CONSTANT NUMBER
                                           := fnd_log.g_current_runtime_level;
   c_site_level              CONSTANT NUMBER := 10004;

   /*************************************************************************
    *   Procedure : GET_ORDER_TOTAL
    *
    *   PURPOSE:   This procedure return order total excluding Freight and Tzxes
    *   Parameter:
    *          IN
    *              p_Header_id       --  SO Header ID
    * ************************************************************************/
   FUNCTION get_order_total (p_header_id IN NUMBER)
      RETURN NUMBER;

   /*************************************************************************
    *   Procedure : GET_PO_REQD_FLAG
    *
    *   PURPOSE:   This procedure return DFF Attribute3 from Customer Account Sites,
    *              If PO is required the attribute will have Y value
    *   Parameter:
    *          IN
    *              p_bill_to_site_use_id       -- Bill to Site Use Id
    *              p_ship_to_site_use_id       -- Ship TO Site Use Id
    * ************************************************************************/
   FUNCTION get_po_reqd_flag (
      p_bill_to_site_use_id   IN   NUMBER,
      p_ship_to_site_use_id   IN   NUMBER
   )
      RETURN VARCHAR2;

   /*************************************************************************
    *   Function : GET_SERIAL_CONTROL_CODE
    *
    *   PURPOSE:   This procedure return Serial Control Code
    *          IN
    *              p_inventory_item_id      -- Inventory Item ID
    *              p_ship_from_org_id       -- Ship From Org ID
    * ************************************************************************/
   FUNCTION get_serial_control_code (
      p_inventory_item_id   IN   NUMBER,
      p_ship_from_org_id    IN   NUMBER
   )
      RETURN NUMBER;

   /*************************************************************************
    *   Function : GET_LOT_CONTROL_CODE
    *
    *   PURPOSE:   This procedure return Lot Control Code value for an Item and Organization
    *          IN
    *              p_inventory_item_id      -- Inventory Item ID
    *              p_ship_from_org_id       -- Ship From Org ID
    * ************************************************************************/
   FUNCTION get_lot_control_code (
      p_inventory_item_id   IN   NUMBER,
      p_ship_from_org_id    IN   NUMBER
   )
      RETURN NUMBER;

/*************************************************************************
    *   Function : GET_RESERVATION_QTY
    *
    *   PURPOSE:   This procedure return reserved qty for a sales order line
    *          IN
    *              p_header_id      -- Sales Order Header ID
    *              p_line_id       -- Sales Order Line Id
    * ************************************************************************/
   FUNCTION get_reservation_qty (p_header_id IN NUMBER, p_line_id IN NUMBER)
      RETURN NUMBER;

   /*************************************************************************
      *   Function : GGET_DELVRY_CHARGE_EXEMPT_INFO
      *
      *   PURPOSE:   This procedure returns Delivery Charge  Excempt info : DFF Attribute14
      *   Parameter:
      *          IN
      *              p_Organization_ID       --  Warehouse
      * ************************************************************************/
   FUNCTION get_delvry_charge_exempt_info (p_organization_id IN NUMBER)
      RETURN VARCHAR2;

    /*************************************************************************
   *   Function : GET_CC_NUMBER
   *
   *   PURPOSE:   This procedure parses the string and return CC number
   *   Parameter:
   *          IN
   *              P_String        --  Credit Card Swiper String
   * ************************************************************************/
   FUNCTION get_cc_number (p_cc_string IN VARCHAR2)
      RETURN VARCHAR2;

    /*************************************************************************
   *   Function : GET_CC_EXP_DATE
   *
   *   PURPOSE:   This procedure parses the string and return CC Expiration Date value
   *   Parameter:
   *          IN
   *              P_String        --  Credit Card Swiper String
   * ************************************************************************/
   FUNCTION get_cc_exp_date (p_cc_string IN VARCHAR2)
      RETURN VARCHAR2;

    /*************************************************************************
   *   Function : GET_CC_HOLDER_NAME
   *
   *   PURPOSE:   This procedure parses the string and return CC Holder Name
   *   Parameter:
   *          IN
   *              P_String        --  Credit Card Swiper String
   * ************************************************************************/
   FUNCTION get_cc_holder_name (p_cc_string IN VARCHAR2)
      RETURN VARCHAR2;

   /*************************************************************************
   *   Function : get_credit_card_type
   *
   *   PURPOSE:   This procedure parses the string and return CC TYpe
   *   Parameter:
   *          IN
   *              P_String        --  Credit Card Swiper String
   * ************************************************************************/
   -- FUnction Returns Credit Card Type for a given Credit Card Number
   FUNCTION get_credit_card_type (p_cc_string IN VARCHAR2)
      RETURN VARCHAR2;

    /*************************************************************************
    *   Function : get_credit_card_code
    *
    *   PURPOSE:   This procedure parses the string and return CC Code
    *   Parameter:
    *          IN
    *              P_String        --  Credit Card Swiper String
    * ************************************************************************/
   -- Function Retuns Credit Card Type String based on Credit Card Number
   FUNCTION get_credit_card_code (p_credit_card_number IN VARCHAR2)
      RETURN VARCHAR2;

   /*************************************************************************
   *   Function : get_credit_card_exp_date
   *
   *   PURPOSE:   This procedure parses the string and return CC Expiration Date
   *   Parameter:
   *          IN
   *              P_String        --  Credit Card Swiper String
   * ************************************************************************/
   FUNCTION get_credit_card_exp_date (p_cc_exp_date IN VARCHAR2)
      RETURN VARCHAR2;

     /*************************************************************************
   *   Function : Check_Pending_Counter_Sales
   *
   *   PURPOSE:   This procedure is called from WF Node, takes Line_Is as input parameter
   *              and checks if this any pending Material transaction in interface and also checks if
   *              there is any couter sales line for same item and organization that is not fulfilled.
   *
   * ************************************************************************/
   PROCEDURE check_pending_counter_sales (
      itemtype    IN              VARCHAR2,
      itemkey     IN              VARCHAR2,
      actid       IN              NUMBER,
      funcmode    IN              VARCHAR2,
      resultout   IN OUT NOCOPY   VARCHAR2
   );

   --- PRocedure Get_Salesrep_Name
   -- Gets Salesrep Person name for a given Site_USE_ID
   --  P_Return_TYpe Can have values 'NAME' or 'ID'
   -- Function return Salesrep Name if p_RETURN_TYPE is NAME
   -- Function return Salesrep ID if p_RETURN_TYPE is ID
   FUNCTION get_salesrep_info (p_site_use_id NUMBER, p_return_type VARCHAR2)
      RETURN VARCHAR2;

   -- 05/03/2012 CGonzalez
   -- Get Credit Card Brand for Order Payment Form
   FUNCTION get_credit_card_brand (p_cc_string IN VARCHAR2)
      RETURN VARCHAR2;

 /*****************************************************************************************************************************
     Procedure progress_so_lines
     Purpose   :  Procedure is called from Concurrent Program to progress Sales Order lines that are waiting at
                  'WC Wait Until Progressed' sub process.
     Parameters  :
     P_HEADER_ID    : If User passes parameter  then program will look for all Sales Order lines with in given Sales Order Header,
                      else it will look for all Sales Order lines that are waiting at sub process 'WC Wait Until Progressed'
     Change History :
     Resource Name    : Change Date          Change Purpose and change description
     -------------      ------------         --------------------------------------
     Satish U           21-DEC-2012          TMS # 20121214-00966   : Defined Constant for Process name, as we have two sub process with same name.
     Satish U           31-JAN-2013          MS# 20130201-01025     : Code was modified to consider SO lines in Cancelled status too.
   **********************************************************************************************************************************
   -- Following Procredure is called to Progress Sales Order Lines that are waiting for external pgograms to be called to pgoress them
   -- If User Passes value for P_Header_ID then its SO LInes that are waiting to progressed will be progressed Else  SO Lines of All the Orders will be progressed
   **********************************************************************************************************************************/
   PROCEDURE progress_so_lines (
      errbuf        OUT      VARCHAR2,
      retcode       OUT      VARCHAR2,
      p_header_id   IN       NUMBER DEFAULT NULL
   );

   /*****************************************************************************************************************************
     Procedure progress_Counter_so_lines
     Purpose   :  Procedure is called from Concurrent Program to progress Sales Order lines that are waiting at
                  'WC Wait Until Progressed' sub process.
     Parameters  :
     P_HEADER_ID    : If User passes parameter  then program will look for all Sales Order lines with in given Sales Order Header,
                      else it will look for all Sales Order lines that are waiting at sub process 'WC Wait Until Progressed'
     Change History :
     Resource Name    : Change Date          Change Purpose and change description
     -------------      ------------         --------------------------------------
     Satish U           21-DEC-2012          TMS # 20121214-00966   : Initial extension was written
     ****************************************************************************************************************************
    -- Following Procredure is called to Progress Counter Sales Order Lines that are waiting for external programs to be called to pgoress them
   -- If User Passes value for P_Header_ID then its SO LInes that are waiting to progressed will be progressed Else  SO Lines of All the Orders will be progressed
   --  Satish U: 12/21/2012 : TMS # 20121214-00966
   --Ram Talluri8/30/2013 : Task ID: 20130830-00571 added Parameter P_INCL_HAWAII_BRANCHES
   *****************************************************************************************************************************/
   PROCEDURE progress_counter_so_lines (
      errbuf        OUT      VARCHAR2,
      retcode       OUT      VARCHAR2,
      p_header_id   IN       NUMBER DEFAULT NULL,
      P_INCL_HAWAII_BRANCHES IN VARCHAR2
   );


      /*************************************************************************
   *   Function : Check_SO_LINES_CLOSED
   *
   *   PURPOSE:   This procedure is called from WF Node at Header Level.
   *   Checks if all the lines are created for rental Orders and also checks if all the SO lines are closed or not
   *   If not then moves Header to wait mode until next day.
   * ************************************************************************/
   PROCEDURE check_so_lines_closed (
      itemtype    IN              VARCHAR2,
      itemkey     IN              VARCHAR2,
      actid       IN              NUMBER,
      funcmode    IN              VARCHAR2,
      resultout   IN OUT NOCOPY   VARCHAR2
   );

   -- 06/28/2012 CGonzalez: Added procedure to pick release order
   FUNCTION xxwc_pick_release_call (
      p_order_number   IN   NUMBER,
      p_user_id        IN   NUMBER,
      p_resp_id        IN   NUMBER,
      p_resp_appl_id   IN   NUMBER,
      p_session_id     IN   NUMBER
   )
      RETURN VARCHAR2;

   PROCEDURE xxwc_pick_release_order (
      p_order_number   IN   NUMBER,
      p_organization_id   IN   NUMBER, --added 10/26/2012
      p_user_id        IN   NUMBER,
      p_resp_id        IN   NUMBER,
      p_resp_appl_id   IN   NUMBER,
      p_session_id     IN   NUMBER
   );

   PROCEDURE xxwc_pack_slip_report (
      p_order_number   IN   NUMBER,
      p_print_price    IN   NUMBER,
      p_user_id        IN   NUMBER,
      p_resp_id        IN   NUMBER,
      p_resp_appl_id   IN   NUMBER,
      p_session_id     IN   NUMBER
   );

/*
    FUNCTION get_onhand used to retrieve the on-hand quantity for an item, organization, subinventory level
        Used for XXWC Packing Slip Report
        P_INVENTORY_ITEM_ID  IN NUMBER --Inventory Item Id
        P_ORGANIZATION_ID    IN NUMBER --Organization ID
        P_SUBINVENTORY       IN VARCHAR2 --Subinventory Code
        P_RETURN_TYPE        IN VARCHAR2 --Possible Values
                                             = H --On-Hand
                                             = R --Available to Reserve
                                             = T --Available to Transact
*/
   FUNCTION get_onhand (
      p_inventory_item_id   IN   NUMBER,
      p_organization_id     IN   NUMBER,
      p_subinventory        IN   VARCHAR2,
      p_return_type         IN   VARCHAR2,
      p_lot_number          IN   VARCHAR2 DEFAULT NULL
   )
      RETURN NUMBER;

   FUNCTION count_order_holds (p_header_id IN NUMBER)
      RETURN NUMBER;

   /*
   Function to submit a print batch from the table XXWC_PRINT_REQUESTS_TEMP
     P_BATCH_ID IN NUMBER -- Batch Id used in the table
     P_PROCESS_FLAG IN NUMBER - 1 = Not Processed, 2 - = PROCESSED
   */
   FUNCTION submit_print_batch (
      p_batch_id       IN   NUMBER,
      p_group_id       IN   NUMBER,
      p_process_flag   IN   NUMBER,
      p_user_id        IN   NUMBER,
      p_resp_id        IN   NUMBER,
      p_resp_appl_id   IN   NUMBER
   )
      RETURN NUMBER;

   /*
     Function to pull the default printer selection for a report, user, and organization combination
         P_CCP_APPLICATION       -- Short Name for the Application of the Concurrent Request
         P_CONCURRENT_REQUEST_ID -- Concurrent program short name
         P_ORGANIZATION_ID       -- Organization ID
         P_USER_ID               -- USER ID
         P_RESP_ID               -- Responsibility ID
         P_REPS_APPL_ID          -- Responsibility Application Id
   */
   FUNCTION get_printer (
      p_ccp_application           IN   VARCHAR2,
      p_concurrent_program_name   IN   VARCHAR2,
      p_organization_id           IN   NUMBER,
      p_user_id                   IN   NUMBER,
      p_resp_id                   IN   NUMBER,
      p_resp_appl_id              IN   NUMBER
   )
      RETURN VARCHAR2;

   FUNCTION default_org_printer (
      p_ccp_application           IN   VARCHAR2,
      p_concurrent_program_name   IN   VARCHAR2,
      p_organization_id           IN   NUMBER,
      p_user_id                   IN   NUMBER,
      p_resp_id                   IN   NUMBER,
      p_resp_appl_id              IN   NUMBER
   )
      RETURN VARCHAR2;

   FUNCTION default_user_printer (
      p_ccp_application           IN   VARCHAR2,
      p_concurrent_program_name   IN   VARCHAR2,
      p_organization_id           IN   NUMBER,
      p_user_id                   IN   NUMBER,
      p_resp_id                   IN   NUMBER,
      p_resp_appl_id              IN   NUMBER
   )
      RETURN VARCHAR2;

     /*
     Procedure Check Available used to find order lines that will backorder prior to Confirm for Shipping
                     P_HEADER_ID     IN NUMBER - Order Number
                     X_COUNT            OUT NUMBER - Number of lines that will backorder
                     X_MESSAGE       OUT VARCHAR2 - String used to display the line number, item, and potential backorder qty

   */
   PROCEDURE check_available (
      p_header_id   IN       NUMBER,
      p_organization_id IN   NUMBER, --added 10/26/2012
      x_count       OUT      NUMBER,
      x_message     OUT      VARCHAR2
   );

/*   Procedure Check Lot Reseration used to find order lines that don't have a lot reserversed against the order
                    P_HEADER_ID     IN NUMBER - Order Number
                    X_COUNT            OUT NUMBER - Number of lines that will backorder
                    X_CHECK            OUT VARCHAR2 - N - means all lines have a reservation; Y - means there is a line missing a reservation
                    X_MESSAGE       OUT VARCHAR2 - String used to display the line number, item, and potential backorder qty

  */
   PROCEDURE check_lot_reservation (
      p_header_id   IN       NUMBER,
      p_organization_id IN   NUMBER, --Added 10/26/2012
      x_lot_count   OUT      NUMBER,
      x_check       OUT      VARCHAR2,
      x_message     OUT      VARCHAR2
   );

   PROCEDURE check_lot_fs_reservation (
      p_header_id   IN       NUMBER,
      p_organization_id IN   NUMBER, --Added 10/26/2012
      x_lot_count   OUT      NUMBER,
      x_check       OUT      VARCHAR2,
      x_message     OUT      VARCHAR2
   );

/*
  ORG_PRINTER - this is an override if the concurrent program needs to over ride the organization parameter on the concurrent request based on printer

     P_CONCURRENT_NAME IN VARCHAR2
     P_CCP_APPLICATION         IN VARCHAR2
     P_PRINTER_NAME IN VARCHAR2
     RETURN NUMBER -- ORGANIZATION_ID

*/
   FUNCTION org_printer (
      p_ccp_application           IN   VARCHAR2,
      p_concurrent_program_name   IN   VARCHAR2,
      p_printer_name              IN   VARCHAR2
   )
      RETURN NUMBER;

   /*
   INTERNAL_ORDER_CHECK

     Function used to retrieve Planned or Stock Internal Order

     Parameters
         P_ORDER_HEADER_ID IN NUMBER -- Order Header Id

     Return
         VARCHAR2 - Value return will be S for Stock or P for Planned and N for Not an internal order
   */
   FUNCTION internal_order_check (p_header_id IN NUMBER)
      RETURN VARCHAR2;

   /*
   submit_internal_order_pick_rpt

     Procedure defined to submit pick slip reports at the shipping/receiving
     orgs for sold internal reqs

     Parameters
         p_order_number IN NUMBER -- Order Number (optional)
         p_creation_date IN VARCHAR2 -- Order Creation Date (optional)

   */
   PROCEDURE submit_internal_order_pick_rpt (
      errbuf            OUT      VARCHAR2,
      retcode           OUT      VARCHAR2,
      p_order_number    IN       NUMBER,
      p_creation_date   IN       VARCHAR2,
      p_copies          IN       NUMBER
   );

   PROCEDURE insert_char_arg (
      batch_id   IN   NUMBER,
      GROUP_ID   IN   NUMBER,
      argument   IN   NUMBER,
      VALUE      IN   VARCHAR2
   );

   FUNCTION get_user_profile_printer (p_user_id IN NUMBER)
      RETURN VARCHAR2;

   PROCEDURE fulfillment_acceptance(
      errbuf            OUT      VARCHAR2,
      retcode           OUT      VARCHAR2,
      p_header_id       IN       NUMBER);

   PROCEDURE GET_RVP_INFO
      (p_organization_id      IN NUMBER
      ,x_name           OUT VARCHAR2
      ,x_phone          OUT VARCHAR2);

   PROCEDURE GET_CC_INFO
      (P_HEADER_ID      IN NUMBER
      ,P_TRXN_EXTENSION_ID IN NUMBER
      ,X_MASKED_CC_NUMBER      OUT VARCHAR2
      ,X_CHNAME            OUT VARCHAR2
      ,X_CARD_ISSUER_CODE   OUT VARCHAR2
      ,X_AUTH_CODE      OUT VARCHAR2
      ,X_AMOUNT         OUT NUMBER);

   FUNCTION CHECK_DELIVERY_DOCS_SUBMIT
      (P_HEADER_ID      IN NUMBER)
       RETURN VARCHAR2;

-- Added by Shankar Hariharan 29-Sep-2012 for Adding OM lines from AIS
   PROCEDURE ADD_LINES_TO_ORDER (i_line_tbl IN OE_ORDER_PUB.Line_Tbl_Type,
                                 o_msg_data OUT varchar2,
                                 o_return_status OUT varchar2);
   FUNCTION CHECK_LOT_RESERVATION_FNC
   (   p_header_id   IN       NUMBER,
      p_organization_id IN   NUMBER, --Added 10/26/2012
      p_return           IN    VARCHAR2
   ) RETURN VARCHAR2;


/* Version# 5.0 > Start
   --Added by Lee Spitzer 01-15-2013 to check print price on customer ship to DFF
   --Pass the order header id and return 1 or 2 to restrict pricing
   FUNCTION CHECK_PRINT_PRICE (p_header_id IN NUMBER)
        --Removed 06192013 per TMS Ticket 20130619-01155   ,p_name      IN VARCHAR2) --Added TMS Ticket 20130501-04183
      RETURN NUMBER;
*/

   FUNCTION CHECK_PRINT_PRICE (p_ship_to_org_id IN NUMBER) RETURN NUMBER;
-- Version# 5.0 < End

--  Shankar TMS  [20130115-01230]: 01/16/2013
PROCEDURE progress_inv_hold_lines (
      errbuf        OUT      VARCHAR2,
      retcode       OUT      VARCHAR2,
      p_header_id   IN       NUMBER DEFAULT NULL
   );
PROCEDURE om_assign_order_line_wf (
      errbuf        OUT      VARCHAR2,
      retcode       OUT      VARCHAR2,
      p_header_id   IN       NUMBER DEFAULT NULL
   );

    -- Following Procredure is called to assign workflows to the order lines.
   -- If User Passes value for P_Header_ID then its SO LInes that are missing workflows are get assigned.
   --TMS 20130201-01304
   --  Ram Talluri

/*************************************************************************
  Function : xxwc_rel_price_chg_hold_prc

  PURPOSE:   Procedure to release XXWC_PRICE_CHANGE_HOLD hold on the order lines
             TMS Ticket 20130121-00447

  REVISIONS:
  Ver        Date        Author                     Description
  ---------  ----------  ---------------         -------------------------
  1.0        02/05/2013  Consuelo Gonzalez      Initial Version
*************************************************************************/
PROCEDURE xxwc_rel_price_chg_hold_prc ( errbuf                OUT VARCHAR2,
                                        retcode               OUT VARCHAR2,
                                        p_header_id            IN NUMBER DEFAULT NULL,
                                        p_order_type           IN NUMBER,      -- Added. Version# 4.0
                                        p_incl_hawaii_branches IN VARCHAR2,    -- Added. Version# 4.0
                                        p_last_update_date     IN VARCHAR2     -- Added. Version# 4.0
                                        );

  /*************************************************************************
*   Pocedure: Apply_Price_Change_Hold
*
*   PURPOSE:   This procedure is called from Standard Line WF Node at line Level.
*   Checks if XXWC_PRICE_CHANGE_HOLD exists at line level, if not then creates a hold at the line level.
*   REVISIONS:
*  Ver        Date        Author                     Description
*  ---------  ----------  ---------------         -------------------------
* 1.0        02/05/2013    Satish U      Initial Version  TMS Ticket 20130121-00447
* ************************************************************************/
   PROCEDURE Apply_Price_Change_Hold (
      itemtype    IN              VARCHAR2,
      itemkey     IN              VARCHAR2,
      actid       IN              NUMBER,
      funcmode    IN              VARCHAR2,
      resultout   IN OUT NOCOPY   VARCHAR2
   );

 -- 06/19/2013 CG: TMS 20130617-01047: Added to be used by the Backorder Report to determine if a lot line has lot reservation or not
function is_lot_line_reserved (p_line_id in NUMBER) return varchar2;

/*************************************************************************
  Function : xxwc_bko_visibility_prc

  PURPOSE:   Procedure to check backorder status and update user item
             description for lines that have not been processed through
             the shipping tables
             TMS 20130715-00472

  REVISIONS:
  Ver        Date        Author                     Description
  ---------  ----------  ---------------         -------------------------
  1.0        08/20/2013  Consuelo Gonzalez      Initial Version
*************************************************************************/
PROCEDURE xxwc_bko_visibility_prc ( RETCODE     OUT VARCHAR2
                                   , ERRMSG     OUT VARCHAR2);

-- Version# 9.0 > Start
/*************************************************************************
   *   Function : update_user_item_description
   *
   *   PURPOSE:   This procedure is called from WF Node, takes Line_Id as input parameter
   *              and update user_item_description at line level
   *
   * ************************************************************************/
   PROCEDURE xxwc_update_user_item_desc (
      itemtype    IN              VARCHAR2,
      itemkey     IN              VARCHAR2,
      actid       IN              NUMBER,
      funcmode    IN              VARCHAR2,
      resultout   IN OUT NOCOPY VARCHAR2
   );
-- Version# 9.0 < End
  /*************************************************************************
   *   Procedure : XXWC_LOG_PICKSLIP_PRINT_HIST
   *
   *   PURPOSE:   This procedure insert print log of pick slip report into custom tables.
   *
   * REVISIONS:
   *  Ver        Date         Author            Description
   *  ---------  ----------   ---------------   -------------------------
   *  12.0        04/27/2017   Niraj K Ranjan   TMS#20160815-00078   Branch Visit - Pick Ticket Reprint Improvements
   * ************************************************************************/
   PROCEDURE xxwc_log_pickslip_print_hist (p_header_id    IN NUMBER,
                                           p_printer      IN VARCHAR2,
                                           p_copies       IN NUMBER,
                                           p_prnt_org_id  IN NUMBER
                                          );
   /*************************************************************************
   *   Function : xxwc_get_line_priority
   *
   *   PURPOSE:   This procedure is called from XXWC_OM_PICK_SLIP report to display lines
   *              changes according to priority.
   *  REVISIONS:
   *  Ver        Date         Author            Description
   *  ---------  ----------   ---------------   -------------------------
   *  12.0       04/27/2017   Niraj K Ranjan   TMS#20160815-00078   Branch Visit - Pick Ticket Reprint Improvements
   * ************************************************************************/
   FUNCTION xxwc_get_line_priority(p_header_id        IN NUMBER
                                  ,p_line_id          IN NUMBER
								  ,p_line_number      IN NUMBER
								  ,p_max_seq          IN NUMBER
                                  ,p_pre_max_seq      IN NUMBER
                                  ,p_report_prog_name IN VARCHAR2
                                  ,p_ship_from_org_id IN NUMBER
                                  ,p_scenario         IN VARCHAR2)
   RETURN NUMBER;

   /*************************************************************************
   *   Function : xxwc_get_line_exception
   *
   *   PURPOSE:   This procedure is called from XXWC_OM_PICK_SLIP report to display line exception
   *              for dms2 project.
   *  REVISIONS:
   *  Ver        Date         Author            Description
   *  ---------  ----------   ---------------   -------------------------
   *  13.0       12/13/2017   Rakesh Patel      TMS#20170811-00005-DMS Phase-2.0 Outbound Extract(Standard and Internal orders)
   * ************************************************************************/
   FUNCTION xxwc_get_line_exception(p_header_id         IN NUMBER
                                   ,p_delivery_id       IN NUMBER
                                   ,p_line_number       IN NUMBER
                                   ,p_line_shpmt_num    IN VARCHAR2
                                   ,p_report_prog_name  IN VARCHAR2
   )
   RETURN VARCHAR2;
   
    /*************************************************************************
   *   Function : xxwc_get_line_exception_qty
   *
   *   PURPOSE:   This procedure is called from XXWC_OM_PICK_SLIP report to display line exception qty
   *              for dms2 project.
   *  REVISIONS:
   *  Ver        Date         Author            Description
   *  ---------  ----------   ---------------   -------------------------
   *  13.0       12/13/2017   Rakesh Patel      TMS#20170811-00005-DMS Phase-2.0 Outbound Extract(Standard and Internal orders)
   * ************************************************************************/
   FUNCTION xxwc_get_line_exception_qty( p_header_id         IN NUMBER
                                        ,p_delivery_id       IN NUMBER
                                        ,p_line_number       IN NUMBER
                                        ,p_line_shpmt_num    IN VARCHAR2
                                        ,p_report_prog_name  IN VARCHAR2
   )
   RETURN NUMBER;
   
END xxwc_ont_routines_pkg;
/