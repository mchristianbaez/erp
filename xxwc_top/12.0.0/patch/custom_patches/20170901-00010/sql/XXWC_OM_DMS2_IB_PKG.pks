CREATE OR REPLACE PACKAGE APPS.XXWC_OM_DMS2_IB_PKG IS
 /*************************************************************************
     $Header XXWC_OM_DMS2_IB_PKG $
     Module Name: XXWC_OM_DMS2_IB_PKG.pks

     PURPOSE:   DESCARTES Project    
                       
     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        12/19/2017  Rakesh Patel            TMS#20170901-00010 DMS Phase-2.0 Inbound Extract
**************************************************************************/
  
   TYPE xxwc_log_rec IS RECORD (
      SERVICE_NAME            VARCHAR2(150)
     ,ORDER_NUMBER            NUMBER
     ,HEADER_ID               NUMBER
     ,LINE_ID                 NUMBER
     ,DELIVERY_ID             NUMBER
     ,ORDER_TYPE              VARCHAR2(150)  
     ,PO_NUMBER               NUMBER
     ,PO_HEADER_ID            NUMBER
     ,SCHEDULE_DELIVERY_DATE  DATE             
     ,WS_RESPONSECODE         VARCHAR2(500)
     ,WS_RESPONSEDESCRIPTION  VARCHAR2(4000)
     ,HTTP_STATUS_CODE        VARCHAR2(500)
     ,HTTP_REASON_PHRASE      VARCHAR2(4000)
     ,SEQUENCE_ID             NUMBER
	 ,CMD                     NUMBER
	 ,STYPE                   NUMBER
	 ,LCODE                   NUMBER
     ,REQUEST_PAYLOAD         CLOB
     ,RESPONSE_PAYLOAD        CLOB 
     ,REQUEST_PAYLOAD_XMLTYPE XMLTYPE
   );
   
PROCEDURE DEBUG_LOG (p_xxwc_log_rec IN OUT xxwc_log_rec);
   
PROCEDURE debug_on;
PROCEDURE debug_off;
  
--This procedure will be called by the internal order ship confirm process
PROCEDURE resubmit_requests(p_header_id                IN NUMBER
                             ,p_delivery_id            IN NUMBER
                             ,p_order_type_id          IN NUMBER  
                             ,p_concurrent_request_id  IN NUMBER
                             ,p_batch_size             IN NUMBER 
                             --,p_application_short_name IN VARCHAR2 
                              );
  
--1. Download the image from stage table and put into file system.
--   This procedure will create a POD pdf file on the server location /xx_iface/ebsdev/inbound/descartes/pod_images
PROCEDURE create_image_file_on_server(p_header_id              IN NUMBER
                                     ,p_concurrent_request_id  IN NUMBER
                                     ,p_resub_req_id           IN NUMBER
                                      );
                                                                             
--2. Submit the bursting program and wait for this to finish. 
--The program will create a POD pdf file on the server location /xx_iface/ebsdev/inbound/descartes/pod                            
PROCEDURE submit_Bursting_program(p_header_id              IN NUMBER
                                 ,p_concurrent_request_id  IN NUMBER
                                 ,p_application_short_name IN VARCHAR2 
                                 ,p_batch_size             IN NUMBER 
                                  );
                                        
--3. Attach POD to sales order / purchase order
PROCEDURE attach_POD_file(p_header_id              IN NUMBER
                         ,p_concurrent_request_id  IN NUMBER
                         ,p_delivery_id            IN NUMBER   
                         ,p_application_short_name IN VARCHAR2 
                          );    
                          
PROCEDURE insert_shipment_data(p_sequence_id   IN NUMBER
                              ,p_xxwc_log_rec IN OUT xxwc_log_rec
                              ,p_order_number  OUT NUMBER
                              ,p_order_type_id OUT NUMBER
                              ,p_delivery_id   OUT NUMBER
                              ,p_cmd           OUT NUMBER
                              ,p_stype         OUT NUMBER
                              ,p_route_id      OUT VARCHAR2
                              ,p_stop_id       OUT NUMBER
							  ,p_lcode         OUT NUMBER
							  ,p_err_msg       OUT VARCHAR2
                              );                        
  
FUNCTION hex_to_blob(p_hex_data CLOB) RETURN BLOB;

PROCEDURE ship_confirm(p_errbuf              OUT VARCHAR2
                      ,p_retcode             OUT VARCHAR2
                      ,p_user_name           IN VARCHAR2
                      ,p_responsibility_name IN VARCHAR2
                      ,p_order_type_id       IN NUMBER  
                      ,p_run_zone            IN VARCHAR2
                      ,p_order_number        IN VARCHAR2
                      ,p_debug_flag          IN VARCHAR2 
                      );

PROCEDURE email_errors(p_header_id   IN NUMBER
                      ,p_delivery_id IN NUMBER
                      ,p_error_msg   VARCHAR2); 
                      
PROCEDURE Generate_and_attach_POD(p_errbuf              OUT VARCHAR2
                                 ,p_retcode             OUT VARCHAR2
                                 ,p_user_name           IN VARCHAR2
                                 ,p_responsibility_name IN VARCHAR2
                                 ,p_run_zone            IN VARCHAR2
								 ,p_order_type_id       IN VARCHAR2
								 ,p_order_number        IN VARCHAR2
								 ,p_batch_size          IN NUMBER
								 ,p_debug_flag          IN VARCHAR2 
								 );

END XXWC_OM_DMS2_IB_PKG;
/