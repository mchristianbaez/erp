CREATE OR REPLACE VIEW APPS.XXWC_PRINT_LOG_VW as
         select fcr.request_id
               ,fcr.concurrent_program_id 
               ,fcr.program_application_id 
               ,fcp.concurrent_program_name
               ,fcpt.user_concurrent_program_name
               ,fcr.printer
               ,fpt.description printer_description 
               ,fcr.requested_by
               ,fu.user_name
               ,fu.description user_description
                        ,(case   when x1.header_id = 1 then fcr.argument1
                        when x1.header_id = 2 then fcr.argument2
                        when x1.header_id = 3 then fcr.argument3
                        when x1.header_id = 4 then fcr.argument4
                        when x1.header_id = 5 then fcr.argument5
                        when x1.header_id = 6 then fcr.argument6
                        when x1.header_id = 7 then fcr.argument7
                        when x1.header_id = 8 then fcr.argument8
                        when x1.header_id = 9 then fcr.argument9
                        WHEN x1.header_id = 10 THEN fcr.argument10
                        WHEN x1.header_id = 11 THEN fcr.argument11
                        WHEN x1.header_id = 12 THEN fcr.argument12
                        when x1.header_id = 13 then fcr.argument13
                        WHEN x1.header_id = 14 THEN fcr.argument14
                        WHEN x1.header_id = 15 THEN fcr.argument15
                        WHEN x1.header_id = 16 THEN fcr.argument16
                        WHEN x1.header_id = 17 THEN fcr.argument17
                        WHEN x1.header_id = 18 THEN fcr.argument18
                        WHEN x1.header_id = 19 THEN fcr.argument19
                        WHEN x1.header_id = 20 THEN fcr.argument20
                        WHEN x1.header_id = 21 THEN fcr.argument21
                        WHEN x1.header_id = 22 THEN fcr.argument22
                        WHEN x1.header_id = 23 THEN fcr.argument23
                        WHEN x1.header_id = 24 THEN fcr.argument24
                        when x1.header_id = 25 then fcr.argument25
                  else null
                  end) header_id
               , (case  when x1.organization_id = 1 then fcr.argument1
                        when x1.organization_id = 2 then fcr.argument2
                        when x1.organization_id = 3 then fcr.argument3
                        when x1.organization_id = 4 then fcr.argument4
                        when x1.organization_id = 5 then fcr.argument5
                        when x1.organization_id = 6 then fcr.argument6
                        when x1.organization_id = 7 then fcr.argument7
                        when x1.organization_id = 8 then fcr.argument8
                        when x1.organization_id = 9 then fcr.argument9
                        WHEN x1.organization_id = 10 THEN fcr.argument10
                        WHEN x1.organization_id = 11 THEN fcr.argument11
                        WHEN x1.organization_id = 12 THEN fcr.argument12
                        WHEN x1.organization_id = 13 THEN fcr.argument13
                        WHEN x1.organization_id = 14 THEN fcr.argument14
                        when x1.organization_id = 15 then fcr.argument15
                        WHEN x1.organization_id = 16 THEN fcr.argument16
                        WHEN x1.organization_id = 17 THEN fcr.argument17
                        WHEN x1.organization_id = 18 THEN fcr.argument18
                        WHEN x1.organization_id = 19 THEN fcr.argument19
                        WHEN x1.organization_id = 20 THEN fcr.argument20
                        WHEN x1.organization_id = 21 THEN fcr.argument21
                        WHEN x1.organization_id = 22 THEN fcr.argument22
                        WHEN x1.organization_id = 23 THEN fcr.argument23
                        WHEN x1.organization_id = 24 THEN fcr.argument24
                        when x1.organization_id = 25 then fcr.argument25
                  else null
                  END) organization_id,
               x1.TABLE_NAME,
               fcr.actual_completion_date,
               fcr.phase_code,
               fcr.status_code,
               fcr.number_of_copies,
               trim(ccr_phase.meaning) phase,
               trim(ccr_status.meaning) status,
               fcr.ARGUMENT1, 
               fcr.ARGUMENT2,
               fcr.ARGUMENT3, 
               fcr.ARGUMENT4, 
               fcr.ARGUMENT5, 
               fcr.ARGUMENT6, 
               fcr.ARGUMENT7, 
               fcr.ARGUMENT8, 
               fcr.ARGUMENT9,
               fcr.ARGUMENT10, 
               fcr.ARGUMENT11, 
               fcr.ARGUMENT12, 
               fcr.ARGUMENT13, 
               fcr.ARGUMENT14, 
               fcr.ARGUMENT15, 
               fcr.ARGUMENT16, 
               fcr.ARGUMENT17, 
               fcr.ARGUMENT18, 
               fcr.ARGUMENT19, 
               fcr.ARGUMENT20, 
               fcr.ARGUMENT21, 
               fcr.ARGUMENT22, 
               fcr.ARGUMENT23, 
               fcr.ARGUMENT24, 
               fcr.ARGUMENT25
               , (SELECT signature_name
                    FROM xxwc.xxwc_signature_capture_tbl
                   WHERE request_id = fcr.request_id
                     AND signature_name != '***CANCEL***'
                     AND rownum = 1) SIGNATURE_NAME -- TMS# 20140606-00082
			 , (SELECT SCHEDULE_DELIVERY_DATE
                    FROM XXWC.XXWC_DMS2_LOG_TBL
                   WHERE RPT_CON_REQUEST_ID = fcr.request_id
                     AND rownum = 1) SCHEDULE_DELIVERY_DATE -- TMS#20170901-00010-DMS Phase-2.0 Inbound Extract
        from   fnd_concurrent_requests fcr,
               fnd_printer_tl fpt,
               fnd_user fu,
               fnd_concurrent_programs fcp,
               fnd_concurrent_programs_tl fcpt,
               xxwc.xxwc_print_log_ccp_tbl x1,
               fnd_lookup_values ccr_phase,
               fnd_lookup_values ccr_status
        where  fcr.CONCURRENT_PROGRAM_ID = x1.concurrent_program_id
        and   fcr.program_application_id = x1.application_id
        and   fcr.printer = fpt.PRINTER_NAME
        and   fpt.LANGUAGE = 'US'
        and   fcpt.LANGUAGE = 'US'
        and   fu.user_id = fcr.REQUESTED_BY 
        and   fcr.CONCURRENT_PROGRAM_ID = fcp.CONCURRENT_PROGRAM_ID
        and   fcr.PROGRAM_APPLICATION_ID = fcp.APPLICATION_ID
        and   fcpt.APPLICATION_ID = fcp.application_id
        and   fcpt.CONCURRENT_PROGRAM_ID = fcp.CONCURRENT_PROGRAM_ID
        and   ccr_phase.view_application_id = 0 --FND
        and   ccr_status.view_application_id = 0 --FND
        and   ccr_phase.lookup_type = 'CP_PHASE_CODE'
        and   ccr_status.lookup_type = 'CP_STATUS_CODE'
        and   ccr_phase.lookup_code = fcr.PHASE_CODE
        AND   ccr_status.lookup_code = fcr.STATUS_CODE
        AND   NOT EXISTS (SELECT request_id 
                          FROM   xxwc.xxwc_print_log_tbl
                          where  request_id = fcr.request_id)
  UNION
      select * from XXWC.XXWC_PRINT_LOG_TBL
      WHERE 1 = 1
           AND NVL(signature_name,'*&*^&*') != '***CANCEL***';