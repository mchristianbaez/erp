  /*******************************************************************************
  Table:   XXWC_OM_DMS2_SHIP_CONFIRM_TBL
  Description: This table is used to load data from Descartes with the delivered 
               Orders
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     13-Nov-2017        Rakesh Patel    TMS#20170901-00010 DMS Phase-2.0 Inbound Extract
  ********************************************************************************/
CREATE TABLE XXWC.XXWC_OM_DMS2_SHIP_CONFIRM_TBL
(
  ORDER_NUMBER                 NUMBER,
  LINE_NUMBER                  NUMBER,
  ITEM_NUMBER                  VARCHAR2(40 BYTE),
  DELIVERY_ID                  NUMBER,
  BRANCH                       VARCHAR2(50 BYTE),
  DELIVERED_QTY                NUMBER,
  ORDERED_QUANTITY             NUMBER,
  STOP_EXCEPTION               VARCHAR2(50 BYTE),
  LINE_EXCEPTION               VARCHAR2(50 BYTE),
  SIGNED_BY                    VARCHAR2(60 BYTE),
  NOTES                        VARCHAR2(500 BYTE),
  DRIVER_NAME                  VARCHAR2(60 BYTE),
  ROUTE_NUMBER                 VARCHAR2(60 BYTE),
  ROUTE_NAME                   VARCHAR2(60 BYTE),
  DISPATCH_DATE                DATE,
  CONCURRENT_REQUEST_ID        NUMBER,
  CREATION_DATE                DATE,
  CREATED_BY                   NUMBER,
  LAST_UPDATE_DATE             DATE,
  LAST_UPDATED_BY              NUMBER,
  SHIP_CONFIRM_STATUS          VARCHAR2(10 BYTE),
  SHIP_CONFIRM_EXCEPTION       VARCHAR2(240 BYTE),
  ORG_ID                       NUMBER,
  DMS2_ITEM_ID                 NUMBER,
  HEADER_ID                    NUMBER,
  INVENTORY_ITEM_ID            NUMBER,
  SIGNED_BY_FILE_UPDATE_FLAG   VARCHAR2(10 BYTE),
  SHIP_CONFIRM_REQUEST_ID      NUMBER,
  SEQUENCE_ID                  NUMBER,
  SIGNED_BY_FILE_UPDATE_ERROR  VARCHAR2(240 BYTE)
);

CREATE INDEX XXWC.XXWC_OM_DMS2_SC_TBL_N1 ON XXWC.XXWC_OM_DMS2_SHIP_CONFIRM_TBL(ORDER_NUMBER);

CREATE INDEX XXWC.XXWC_OM_DMS2_SC_TBL_N2 ON XXWC.XXWC_OM_DMS2_SHIP_CONFIRM_TBL(DELIVERY_ID);
  
CREATE INDEX XXWC.XXWC_OM_DMS2_SC_TBL_N3 ON XXWC.XXWC_OM_DMS2_SHIP_CONFIRM_TBL(ORDER_NUMBER, SHIP_CONFIRM_STATUS);

CREATE INDEX XXWC.XXWC_OM_DMS2_SC_TBL_N4 ON XXWC.XXWC_OM_DMS2_SHIP_CONFIRM_TBL(TRUNC("CREATION_DATE"));  

CREATE INDEX XXWC.XXWC_OM_DMS2_SC_TBL_N5 ON XXWC.XXWC_OM_DMS2_SHIP_CONFIRM_TBL(CONCURRENT_REQUEST_ID);  

CREATE INDEX XXWC.XXWC_OM_DMS2_SC_TBL_N6 ON XXWC.XXWC_OM_DMS2_SHIP_CONFIRM_TBL(HEADER_ID, DELIVERY_ID);  

CREATE OR REPLACE SYNONYM APPS.XXWC_OM_DMS_SHIP_CONFIRM_TBL FOR XXWC.XXWC_OM_DMS_SHIP_CONFIRM_TBL;