  /*******************************************************************************
  Table:   XXWC_OM_DMS2_SIGN_CAP_GTT_TBL
  Description: This table is used to load data from Descartes with the Signature image
  
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     13-Nov-2017        Rakesh Patel    TMS#20170901-00010 DMS Phase-2.0 Inbound Extract
  ********************************************************************************/
DROP TABLE XXWC.XXWC_OM_DMS2_SIGN_CAP_GTT_TBL CASCADE CONSTRAINTS;

CREATE GLOBAL TEMPORARY TABLE XXWC.XXWC_OM_DMS2_SIGN_CAP_GTT_TBL
(
  ORDER_NUMBER              NUMBER,
  DELIVERY_ID               NUMBER,
  CONCURRENT_REQUEST_ID     NUMBER,
  SIGNATURE_NAME            VARCHAR2(60 BYTE),
  SIGNATURE_IMAGE_BLOB      BLOB,
  CREATION_DATE             DATE,
  CREATED_BY                NUMBER,
  LAST_UPDATE_DATE          DATE,
  LAST_UPDATED_BY           NUMBER,
  SHIP_CONFIRM_STATUS       VARCHAR2(50 BYTE),
  SHIP_CONFIRM_EXCEPTION    VARCHAR2(240 BYTE),
  ORG_ID                    NUMBER,
  HEADER_ID                 NUMBER,
  LCODE                     VARCHAR2(150 BYTE),
  BURST_POD_CON_REQ_ID      NUMBER,
  SEQUENCE_ID               NUMBER,
  RESUBMIT_POD_CON_REQ_ID   NUMBER,
  POD_METHOD                VARCHAR2(150 BYTE),
  POD_METHOD_IMAGE_TYPE     VARCHAR2(150 BYTE),
  GEN_ATTCH_POD_CON_REQ_ID  NUMBER,
  ORDER_TYPE_ID             NUMBER
)
ON COMMIT PRESERVE ROWS
NOCACHE;
