/*
 TMS: 20151116-00038 
 Date: 12/07/2015
 */
SET SERVEROUTPUT ON SIZE 1000000

DECLARE
   v_count   NUMBER;
BEGIN
   DBMS_OUTPUT.put_line ('TMS:20151116-00038   , Script 1 -Before delete');

   BEGIN
      SELECT COUNT (stg.po_number)
        INTO v_count
        FROM xxwc.XXWC_BPA_PZ_STAGING_TBL stg, apps.po_headers_all pha
       WHERE     stg.po_number = pha.segment1
             AND pha.type_lookup_code != 'BLANKET';

      DELETE FROM xxwc.XXWC_BPA_PZ_STAGING_TBL
            WHERE po_number IN (SELECT stg.po_number
                                  FROM xxwc.XXWC_BPA_PZ_STAGING_TBL stg,
                                       apps.po_headers_all pha
                                 WHERE     stg.po_number = pha.segment1
                                       AND pha.type_lookup_code != 'BLANKET');
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         DBMS_OUTPUT.PUT_LINE (
            'Error in deleting records from stage table-' || SQLERRM);
   END;

   COMMIT;

   DBMS_OUTPUT.put_line (
         'TMS: 20151116-00038, Script 1 -After delete, rows deleted: '
      || v_count);
   
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.PUT_LINE ('Error is' || SQLERRM);
END;
/