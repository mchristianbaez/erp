-- TMS#20170831-00067 - PCAM NPR104058 error
-- Created by P.Vamshidhar
SET SERVEROUT ON

BEGIN
   DELETE FROM APPS.MTL_SYSTEM_ITEMS_INTERFACE
         WHERE TRANSACTION_ID IN (136875102,
                                  136875104,
                                  136875106,
                                  136875108);

   DBMS_OUTPUT.PUT_LINE ('Number of rows deleted ' || SQL%ROWCOUNT);

   UPDATE APPS.MTL_SYSTEM_ITEMS_INTERFACE
      SET segment1 = '12113821917',
          TRANSACTION_TYPE = 'CREATE',
          process_flag = '1',
          inventory_item_id = NULL
    WHERE TRANSACTION_ID = 136875100;

   DBMS_OUTPUT.PUT_LINE ('Number of rows updated ' || SQL%ROWCOUNT);

   COMMIT;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.PUT_LINE ('Error occured ' || SUBSTR (SQLERRM, 1, 250));
      ROLLBACK;
END;
/





