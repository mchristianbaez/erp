/***********************************************************************************************************************************************
   NAME:       TMS_20180907-00008_Datafix.sql
   PURPOSE:  Data fix

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ----------------------------------------------------------------------------------------------------
   1.0        09/07/2018  Rakesh Patel     TMS#20180907-00008
************************************************************************************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000

create table xxwc.xxwc_ms_backup_table as
select ms.rowid rwid, ms.*
from   mtl_supply ms
where  quantity < 0
union
select ms.rowid rwid, ms.*
from   mtl_supply ms,
       rcv_shipment_lines rsl
where  ms.shipment_line_id = rsl.shipment_line_id
and    rsl.shipment_line_status_code = 'CANCELLED'
union
select ms.rowid rwid, ms.*
from   mtl_supply ms
where  ms.shipment_line_id is not null
and    not exists (select 1 
                   from   rcv_shipment_lines rsl 
                   where  rsl.shipment_line_id = ms.shipment_line_id)
union
select ms.rowid rwid, ms.*
from   mtl_supply            ms,
       po_line_locations_all pll
where  pll.line_location_id = ms.po_line_location_id
and    ms.supply_type_code in ('SHIPMENT', 'RECEIVING')
and    (nvl(pll.closed_code, 'OPEN') in ('FINALLY CLOSED') OR  nvl(pll.cancel_flag,'N') = 'Y')
union
select ms.rowid rwid, ms.*
from   mtl_supply                 ms,
       po_requisition_headers_all prh
where  ms.req_header_id = prh.requisition_header_id
and    ms.supply_type_code in ('SHIPMENT', 'RECEIVING')
and    prh.closed_code = 'FINALLY CLOSED'
union
select ms.rowid rwid, ms.*
from   mtl_supply                 ms,
       po_requisition_lines_all   prl
where  ms.req_line_id is not null 
and    ms.req_line_id = prl.requisition_line_id
and    ms.supply_type_code in ('REQ') 
and    prl.quantity_received >= prl.quantity
and    prl.quantity_delivered >= prl.quantity
UNION
select ms.rowid rwid, ms.*
from   mtl_supply              ms,
       po_line_locations_all   pll
where  ms.po_line_location_id is not NULL
and    ms.po_line_location_id = pll.line_location_id
and    ms.supply_type_code in ('PO') 
and    pll.quantity_received >= pll.quantity;

delete from mtl_supply
where  rowid in (select rwid from xxwc.xxwc_ms_backup_table);

COMMIT;