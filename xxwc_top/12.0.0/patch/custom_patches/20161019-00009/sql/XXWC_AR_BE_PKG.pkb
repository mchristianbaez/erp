/* Formatted on 10/25/2016 3:37:22 PM (QP5 v5.265.14096.38000) */
CREATE OR REPLACE PACKAGE BODY APPS.XXWC_AR_BE_PKG
AS
   /********************************************************************************************************************************
      $Header XXWC_AR_BE_PKG.PKB $
      Module Name: XXWC_AR_BE_PKG.PKB

      PURPOSE:   This package is used for replacing triggers on hz_cust_acct_sites_all with business events

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------------------------------------------------------------
       1.0        31-Aug-2016  Neha Saini              TMS#20160826-00318-IT - replacing triggers on hz_cust_acct_sites_all with business events
       1.1        10/24/2016   Neha Saini              TMS# 20161019-00009  Adding logic to use profile option to debug more on why
       1.2        11/05/2016   Rakesh Patel            TMS#20161019-00009 - increae the varaible lenght l_distro_tax.
   *********************************************************************************************************************************/

   PROCEDURE raise_ar_tax_insert_process (
      p_org_id            IN     NUMBER,
      p_cust_account_id   IN     NUMBER,
      p_party_site_id     IN     NUMBER,
      p_attribute15       IN     VARCHAR2,
      p_error_msg            OUT VARCHAR2)
   /**************************************************************************
     PROCEDURE Name: raise_ar_tax_insert_process

     PURPOSE:   This Procedure is used to raise the business event for insert

     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        31-Aug-2016  Neha Saini              TMS#20160826-00318-IT - replacing triggers on hz_cust_acct_sites_all with business events
     1.1        10/24/2016   Neha Saini              TMS# 20161019-00009  Adding logic to use profile option to debug more on why
                                                       business events failed .XXWC_AR_TRIGGERS_LOG
     **************************************************************************/



   IS
      PRAGMA AUTONOMOUS_TRANSACTION;


      l_parameter_list   wf_parameter_list_t;
      l_event_data       CLOB;
      l_sec              VARCHAR2 (100);
   BEGIN
      l_sec := 'procedure raise_ar_tax_insert_process started';

      l_sec := 'Set values for Parameter List';

      l_parameter_list :=
         wf_parameter_list_t (
            wf_parameter_t ('p_org_id', p_org_id),
            wf_parameter_t ('p_cust_account_id', p_cust_account_id),
            wf_parameter_t ('p_party_site_id', p_party_site_id),
            wf_parameter_t ('p_attribute15', p_attribute15));

      l_sec := 'Raise business Event';

      INSERT INTO XXWC.XXWC_AR_BE_TEMP_TBL
           VALUES (NULL,
                   NULL,
                   NULL,
                   NULL,
                   NULL,
                   NULL,
                   'calling insert subscription now ',
                   fnd_global.user_id,
                   SYSDATE,
                   SYSDATE);                                          --ver1.1

      COMMIT;                                                         --ver1.1

      wf_event.raise (
         p_event_name   => 'xxwc.oracle.apps.ar.hzcust.insert.taxware.process',
         p_event_key    => SYS_GUID (),
         p_event_data   => l_event_data,
         p_parameters   => l_parameter_list);

      COMMIT;


      l_sec := 'Raise business Event Procedure completed';
   EXCEPTION
      WHEN OTHERS
      THEN
         p_error_msg := SQLERRM;
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_AR_BE_PKG.raise_ar_tax_insert_process',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (SQLERRM, 1, 240),
            p_distribution_list   => 'WC-ITDEVALERTS-U1@HDSupply.com', -- g_dflt_email,
            p_module              => 'OM');
   END raise_ar_tax_insert_process;

   PROCEDURE raise_ar_tax_update_process (
      p_org_id            IN     NUMBER,
      p_cust_account_id   IN     NUMBER,
      p_party_site_id     IN     NUMBER,
      p_attribute15       IN     VARCHAR2,
      p_old_attribute15   IN     VARCHAR2,
      p_error_msg            OUT VARCHAR2)
   /**************************************************************************
     PROCEDURE Name: raise_ar_tax_update_process

     PURPOSE:   This Procedure is used to raise the business event for update

     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        31-Aug-2016  Neha Saini              TMS#20160826-00318-IT - replacing triggers on hz_cust_acct_sites_all with business events
     1.1        10/24/2016   Neha Saini              TMS# 20161019-00009  Adding logic to use profile option to debug more on why
                                                       business events failed .XXWC_AR_TRIGGERS_LOG
     **************************************************************************/



   IS
      PRAGMA AUTONOMOUS_TRANSACTION;


      l_parameter_list   wf_parameter_list_t;
      l_event_data       CLOB;
      l_sec              VARCHAR2 (100);
   BEGIN
      l_sec := 'procedure raise_ar_taxware_process started';
      l_sec := 'Set values for Parameter List';


      l_parameter_list :=
         wf_parameter_list_t (
            wf_parameter_t ('p_org_id', p_org_id),
            wf_parameter_t ('p_cust_account_id', p_cust_account_id),
            wf_parameter_t ('p_party_site_id', p_party_site_id),
            wf_parameter_t ('p_attribute15', p_attribute15),
            wf_parameter_t ('p_old_attribute15', p_old_attribute15));

      l_sec := 'Raise business Event';

      INSERT INTO XXWC.XXWC_AR_BE_TEMP_TBL
           VALUES (NULL,
                   NULL,
                   NULL,
                   NULL,
                   NULL,
                   NULL,
                   'calling update subscription now ',
                   fnd_global.user_id,
                   SYSDATE,
                   SYSDATE);                                          --ver1.1

      COMMIT;                                                         --ver1.1

      wf_event.raise (
         p_event_name   => 'xxwc.oracle.apps.ar.hzcust.update.taxware.process',
         p_event_key    => SYS_GUID (),
         p_event_data   => l_event_data,
         p_parameters   => l_parameter_list);

      COMMIT;



      l_sec := 'Raise business Event Procedure completed';
   EXCEPTION
      WHEN OTHERS
      THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_AR_BE_PKG.raise_ar_tax_update_process',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (SQLERRM, 1, 240),
            p_distribution_list   => 'WC-ITDEVALERTS-U1@HDSupply.com', --g_dflt_email,
            p_module              => 'OM');
   END raise_ar_tax_update_process;

   FUNCTION SUBS_AR_CBE_INSERT_PROCESS (
      p_subscription_guid   IN     RAW,
      p_event               IN OUT wf_event_t)
      RETURN VARCHAR2
   IS
      /**************************************************************************
       FUCNTION Name: SUBS_AR_CBE_INSERT_PROCESS

       PURPOSE:   This Function is used to raise the business event
                  to start hz cust account sites insert trigger
       REVISIONS:
       Ver        Date        Author                     Description
       ---------  ----------  ---------------         -------------------------
          1.0        31-Aug-2016  Neha Saini              TMS#20160826-00318-IT  - replacing triggers on hz_cust_acct_sites_all with business events
          1.1        10/24/2016   Neha Saini              TMS# 20161019-00009  Adding logic to use profile option to debug more on why
                                                       business events failed .XXWC_AR_TRIGGERS_LOG
       **************************************************************************/



      --initialize
      l_record_exists     VARCHAR2 (1) DEFAULT 'F';
      l_product_exists    VARCHAR2 (1) DEFAULT 'F';
      l_prod_string       VARCHAR2 (500) DEFAULT NULL;
      l_party_name        VARCHAR2 (200) DEFAULT NULL;
      l_party_sitenum     VARCHAR2 (200) DEFAULT NULL;
      l_addr1             VARCHAR2 (50) DEFAULT NULL;
      l_addr2             VARCHAR2 (50) DEFAULT NULL;
      l_city              VARCHAR2 (100) DEFAULT NULL;
      l_state             VARCHAR2 (50) DEFAULT NULL;
      l_country           VARCHAR2 (10) DEFAULT NULL;
      l_postal_code       VARCHAR2 (50) DEFAULT NULL;
      l_specialrate       VARCHAR2 (50) DEFAULT 'F';
      l_tax_state         VARCHAR2 (50) DEFAULT NULL;
      l_process           BOOLEAN DEFAULT TRUE;
      l_tax_flex_value    VARCHAR2 (100) DEFAULT 'XXWC_TAX_EXEMPTION_TYPE';

      -- Error DEBUG
      l_sec               VARCHAR2 (150);
      l_error_msg         VARCHAR2 (2000);
      l_err_callfrom      VARCHAR2 (75) DEFAULT 'SUBS_AR_CBE_INSERT_PROCESS';
      l_distro_list       VARCHAR2 (75)
                             DEFAULT 'hdsoracledevelopers@hdsupply.com';
      l_distro_tax        VARCHAR2 (2000) --Increase the veriable lenght to 2000 TMS#20161019-00009
                             DEFAULT 'hdstaxwaresupport@hdsupply.com';
      l_host              VARCHAR2 (256) := 'mailoutrelay.hdsupply.net';
      l_hostport          VARCHAR2 (20) := '25';
      l_sender            VARCHAR2 (100);
      l_sid               VARCHAR2 (8);
      l_attribute15       apps.hz_cust_acct_sites_all.attribute15%TYPE;
      l_cust_account_id   apps.hz_cust_acct_sites_all.cust_account_id%TYPE;
      l_org_id            apps.hz_cust_acct_sites_all.org_id%TYPE;
      l_party_site_id     NUMBER;
      --changes for ver1.1 starts
      v_user_id           NUMBER;
      v_app_id            NUMBER;
      v_resp_id           NUMBER;
      v_resp_name         VARCHAR2 (100);
      v_app_name          VARCHAR2 (20);
      --changes for ver1.1 ends
      PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      l_sec := 'Starting Subscription for insert one';

      l_cust_account_id :=
         TO_NUMBER (p_event.getvalueforparameter ('p_cust_account_id'));
      l_party_site_id :=
         TO_NUMBER (p_event.getvalueforparameter ('p_party_site_id'));
      l_attribute15 := p_event.getvalueforparameter ('p_attribute15');
      l_org_id := TO_NUMBER (p_event.getvalueforparameter ('p_org_id'));
      -- changes for ver1.1 starts

      v_user_id := apps.FND_GLOBAL.USER_ID;


      BEGIN
         -- mo_global.init('AR');
         mo_global.set_policy_context ('S', 162);

         SELECT fa.application_id,
                fr.responsibility_id,
                fr.responsibility_name,
                fa.application_short_name
           INTO v_app_id,
                v_resp_id,
                v_resp_name,
                v_app_name
           FROM fnd_responsibility_vl fr, fnd_application fa
          WHERE     responsibility_name LIKE 'HDS Receivables Manager - WC'
                AND fr.application_id = fa.application_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            v_app_id := NULL;
            v_resp_id := NULL;
            v_resp_name := NULL;
            v_app_name := NULL;
      END;

      -- Initializing apps
      fnd_global.apps_initialize (user_id        => v_user_id,
                                  resp_id        => v_resp_id,
                                  resp_appl_id   => v_app_id);

      --changes for ver1.1 ends
      INSERT INTO XXWC.XXWC_AR_BE_TEMP_TBL
           VALUES (l_cust_account_id,
                   NULL,
                   NULL,
                   l_org_id,
                   l_party_site_id,
                   l_attribute15,
                   'inside insert subscription ',
                   fnd_global.user_id,
                   SYSDATE,
                   SYSDATE);                                          --ver1.1

      COMMIT;                                                         --ver1.1

      IF l_cust_account_id != -1 AND l_org_id = 162
      THEN
         -- get email machine.
         SELECT LOWER (name) INTO l_sid FROM v$database;

         l_sender := 'Oracle.Applications_' || l_sid || '@hdsupply.com';


         l_sec := 'Gathering information';


         SELECT SUBSTR (party.party_name, 1, 30),
                psite.party_site_number,
                REGEXP_REPLACE (
                   REPLACE (
                      REPLACE (SUBSTR (TRIM (loc.address1), 1, 30),
                               '?',
                               CHR (39)),
                      '?',
                      CHR (34)),
                   '[~!@#$%^&*()_+=\\{}[\]:?;?<,>.\/?]',
                   ''),
                REGEXP_REPLACE (
                   REPLACE (
                      REPLACE (SUBSTR (TRIM (loc.address2), 1, 30),
                               '?',
                               CHR (39)),
                      '?',
                      CHR (34)),
                   '[~!@#$%^&*()_+=\\{}[\]:?;?<,>.\/?]',
                   ''),
                SUBSTR (loc.city, 1, 26),
                loc.state,
                SUBSTR (loc.postal_code, 1, 5),
                loc.country
           INTO l_party_name,
                l_party_sitenum,
                l_addr1,
                l_addr2,
                l_city,
                l_state,
                l_postal_code,
                l_country
           FROM hz_parties party,
                hz_cust_accounts acct,
                hz_party_sites psite,
                hz_locations loc
          WHERE     party.party_id = acct.party_id
                AND acct.cust_account_id = l_cust_account_id
                AND acct.party_id = party.party_id
                AND psite.party_site_id = l_party_site_id
                AND psite.location_id = loc.location_id;


         INSERT INTO XXWC.XXWC_AR_BE_TEMP_TBL
              VALUES (l_cust_account_id,
                      l_party_sitenum,
                      NULL,
                      l_org_id,
                      l_party_site_id,
                      l_attribute15,
                      'got the party site num ',
                      fnd_global.user_id,
                      SYSDATE,
                      SYSDATE);                                       --ver1.1

         COMMIT;                                                      --ver1.1

         IF l_country = 'US'
         THEN
            l_sec :=
               'Checking to see if record already exists.  Will not overwrite if record exists.';



            BEGIN
               SELECT 'T'
                 INTO l_record_exists
                 FROM taxware.steptec_tbl
                WHERE key_1 = l_party_sitenum;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  l_record_exists := 'F';
               WHEN TOO_MANY_ROWS
               THEN
                  l_process := FALSE;
                  l_sec := 'Tax trigger error during Certificate processing.';

                  xxcus_misc_pkg.html_email (
                     p_to              => l_distro_tax,
                     p_from            => l_sender,
                     p_text            => 'test',
                     p_html            =>    '<p><strong>Error on the Customer table related to taxware.</strong><br>
  <br>Retreieved more certificates than expected for Party Site : </p>'
                                          || l_party_sitenum,
                     p_subject         => 'ALERT** Tax trigger error during Certificate processing.',
                     p_smtp_hostname   => l_host,
                     p_smtp_portnum    => l_hostport);
            END;


            BEGIN
               SELECT stcode
                 INTO l_tax_state
                 FROM taxware.stepstcn_tbl
                WHERE stalphacode = l_state;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  l_process := FALSE;


                  xxcus_misc_pkg.html_email (
                     p_to              => l_distro_tax,
                     p_from            => l_sender,
                     p_text            => 'test',
                     p_html            =>    '<p><strong>Could not find a state in STEP.</strong><br>
  <br>Could not find an associated STATE record in STEP for Party Site : </p>'
                                          || l_party_sitenum,
                     p_subject         => 'ALERT** Tax trigger error during Certificate processing.',
                     p_smtp_hostname   => l_host,
                     p_smtp_portnum    => l_hostport);
               WHEN OTHERS
               THEN
                  l_error_msg := ' Duplicate Data for St Code=' || l_state;
                  RAISE PROGRAM_ERROR;
            END;



            IF l_record_exists = 'F' AND l_process
            THEN
               l_sec :=
                     'Checking to see if products exists for the tax exemption.'
                  || l_record_exists;



               BEGIN
                  SELECT 'T'
                    INTO l_product_exists
                    FROM apps.fnd_flex_value_norm_hierarchy
                   WHERE parent_flex_value = l_attribute15 AND ROWNUM = 1;
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     l_product_exists := 'F';
                  WHEN TOO_MANY_ROWS
                  THEN
                     l_error_msg :=
                           'Multiple/duplicate prodcut exists for Party in table fnd_flex_value_norm_hierarchy for Flex Value= '
                        || l_attribute15;
                     RAISE PROGRAM_ERROR;
                  WHEN OTHERS
                  THEN
                     l_error_msg :=
                           'checking prodcut exists for Party in table fnd_flex_value_norm_hierarchy for Flex Value='
                        || l_attribute15;
                     RAISE PROGRAM_ERROR;
               END;


               l_sec := 'Check to see if this is a TEC with special rates.';



               -- l_specialrate will be ='F' when there are no special rates(most cases)
               BEGIN
                  SELECT ffv.attribute1
                    INTO l_specialrate
                    FROM apps.fnd_flex_values ffv,
                         apps.fnd_flex_value_sets fvs
                   WHERE     ffv.flex_value_set_id = fvs.flex_value_set_id
                         AND fvs.flex_value_set_name = l_tax_flex_value
                         AND ffv.flex_value = l_attribute15
                         AND ffv.attribute1 IS NOT NULL;
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     l_specialrate := 'F';
                  WHEN OTHERS
                  THEN
                     l_error_msg :=
                           'Checking TEC with special rates in fnd_flex_values for Party site Special rate ='
                        || l_attribute15;

                     RAISE PROGRAM_ERROR;
               END;



               l_sec :=
                  'the point where I load the details for this record to STEP.';

               INSERT INTO XXWC.XXWC_AR_BE_TEMP_TBL
                       VALUES (
                                 l_cust_account_id,
                                 l_party_sitenum,
                                 NULL,
                                 l_org_id,
                                 l_party_site_id,
                                 l_attribute15,
                                    'now inserting into taxware.steptec_tbl value '
                                 || DECODE (
                                       l_product_exists,
                                       'T', DECODE (l_specialrate,
                                                    'F', 'I',
                                                    NULL),
                                       NULL),
                                 fnd_global.user_id,
                                 SYSDATE,
                                 SYSDATE);                            --ver1.1

               COMMIT;                                                --ver1.1

               INSERT INTO taxware.steptec_tbl step (step.scompanyid,
                                                     step.key_1,
                                                     step.ctransactflag,
                                                     step.key_2,
                                                     step.key_3,
                                                     step.sstatecode,
                                                     step.cjurislevel,
                                                     step.skeyoccurnum,
                                                     step.cproductflag,
                                                     step.cdeflttostcert,
                                                     step.cchkexpirdate,
                                                     step.scustomername,
                                                     step.scustomeraddr1,
                                                     step.scustomeraddr2,
                                                     step.scustomerctyname,
                                                     step.scustomerstatecode,
                                                     step.scustomerzipcode,
                                                     step.scustomeravpgeo,
                                                     step.staxcertifnum,
                                                     step.sreasoncode,
                                                     step.datecertifreceived,
                                                     step.dateeffective,
                                                     step.dateexpiration,
                                                     step.cactivecompflag,
                                                     step.keycode,
                                                     step.cspecialrateflag)
                       VALUES (
                                 'WCI',                     --step.scompanyid,
                                 l_party_sitenum,                --step.key_1,
                                 ' ',                   -- step.ctransactflag,
                                 ' ',                           -- step.key_2,
                                 ' ',                           -- step.key_3,
                                 l_tax_state,               --step.sstatecode,
                                 1,                        --step.cjurislevel,
                                 0,                       --step.skeyoccurnum,
                                 DECODE (
                                    l_product_exists,
                                    'T', DECODE (l_specialrate,
                                                 'F', 'I',
                                                 NULL),
                                    NULL), -- (IF products exist THEN I ELSE null)  --step.cproductflag,
                                 'D', --step.cdeflttostcert,  cactivecompflag = A(complete), B( active), C(certificate active) D(neither checkbox ticked)
                                 NULL,                   --step.cchkexpirdate,
                                 l_party_name,
                                 l_addr1                 --step.scustomername,
                                        ,
                                 l_addr2                --step.scustomeraddr1,
                                        ,               --step.scustomeraddr2,
                                 l_city,              --step.scustomerctyname,
                                 l_state,           --step.scustomerstatecode,
                                 l_postal_code,       --step.scustomerzipcode,
                                 NULL,                 --step.scustomeravpgeo,
                                 'N/A',                  --step.staxcertifnum,
                                 'BW',                     --step.sreasoncode,
                                 SYSDATE,           --step.datecertifreceived,
                                 SYSDATE,                --step.dateeffective,
                                 NULL,  --SYSDATE+1460,  --step.dateexpiration
                                 DECODE (NVL (l_attribute15, 'None'),
                                         'None', 'D',
                                         'A'), -- Cert complete =CC, Cert Active =CA || A(CC ON,CA ON), B(CC OFF,CA ON), C(CC ON,CA ON) D(CC OFF, CA OFF)
                                 'R',                          --,step.keycode
                                 DECODE (l_specialrate, 'F', NULL, 'Y'));

               COMMIT;

               IF l_product_exists = 'T'
               THEN
                  BEGIN
                     DELETE FROM taxware.steprate_tbl
                           WHERE key_1 = l_party_sitenum;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        NULL; -- no action, no rows to delete, so must be a new change to special rate.
                  END;

                  FOR c_products
                     IN (                        --query to load product codes
                         SELECT child_flex_value_low
                           FROM apps.fnd_flex_value_norm_hierarchy
                          WHERE parent_flex_value = l_attribute15)
                  LOOP
                     l_prod_string :=
                           l_prod_string
                        || RPAD (c_products.child_flex_value_low, 25);


                     --special rates are actually normalized and not rpadd'ed.  wierd.
                     IF l_specialrate != 'F'
                     THEN
                        -- if special rates



                        INSERT
                          INTO taxware.steprate_tbl c (c.companyid,
                                                       c.key_1,
                                                       c.stcode,
                                                       c.steptecoccurnumber,
                                                       c.productcode,
                                                       c.basispercent,
                                                       c.federaltaxrate,
                                                       c.statetaxrate,
                                                       c.secstatetaxrate,
                                                       c.countytaxrate,
                                                       c.citytaxrate,
                                                       c.seccountytaxrate,
                                                       c.seccitytaxrate,
                                                       c.districttaxrate,
                                                       c.countytaxind)
                        VALUES ('WCI',
                                l_party_sitenum,                         --key
                                l_tax_state,                       --statecode
                                0,
                                c_products.child_flex_value_low, --product code
                                1,                                    --basis,
                                0,                                --fedtaxrate
                                0,                              --statetaxrate
                                0,                           --secstatetaxrate
                                TO_NUMBER (l_specialrate),     --countytaxrate
                                0,                               --citytaxrate
                                0,                            --seccounty rate
                                0,                              --seccity rate
                                0,                         --district tax rate
                                1                            --county tax rate
                                 );

                        COMMIT;
                     END IF;
                  END LOOP;


                  IF l_specialrate = 'F'
                  THEN
                     -- INSERT INTO PRODUCT CODE TABLE.
                     INSERT
                       INTO taxware.stepprod_tbl prod (
                               prod.companyid,
                               prod.key_1,
                               prod.stcode,
                               prod.jurisdictionlevel,
                               prod.steptecoccurnumber,
                               prod.stepprodoccurnumber,
                               prod.productentries)
                     VALUES ('WCI',                          --prod.companyid,
                             l_party_sitenum,                    --prod.key_1,
                             (SELECT stcode
                                FROM taxware.stepstcn_tbl
                               WHERE stalphacode = l_state),    --prod.stcode,
                             1,                      --prod.jurisdictionlevel,
                             0,                     --prod.steptecoccurnumber,
                             1,                    --prod.stepprodoccurnumber,
                             l_prod_string               --prod.productentries
                                          );

                     COMMIT;
                  END IF;
               END IF;
            END IF;
         END IF;
      END IF;

      l_sec := 'end of insert subscription ';

      INSERT INTO XXWC.XXWC_AR_BE_TEMP_TBL
           VALUES (l_cust_account_id,
                   l_party_sitenum,
                   NULL,
                   l_org_id,
                   l_party_site_id,
                   l_attribute15,
                   'insert subscription end ',
                   fnd_global.user_id,
                   SYSDATE,
                   SYSDATE);                                          --ver1.1

      COMMIT;                                                         --ver1.1

      RETURN 'SUCCESS';
   EXCEPTION
      WHEN PROGRAM_ERROR
      THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_err_callfrom,
            p_calling             => l_sec,
            p_ora_error_msg       => l_error_msg || ' Error ' || SQLERRM,
            p_error_desc          =>    'Error insert into values into Step at '
                                     || l_sec
                                     || ' for Party Site Number='
                                     || l_party_sitenum
                                     || ' With New Party Site Id = '
                                     || l_party_site_id,
            p_distribution_list   => l_distro_list,
            p_module              => 'AR');
         wf_core.context ('XXWC_AR_BE_PKG',
                          'SUBS_AR_CBE_UPDATE_PROCESS',
                          p_event.getEventName (),
                          p_event.getEventKey ());
         wf_event.setErrorInfo (p_event, 'ERROR');

         RETURN 'ERROR';
      WHEN OTHERS
      THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_err_callfrom,
            p_calling             => 'STEP Taxware',
            p_ora_error_msg       => SQLERRM,
            p_error_desc          =>    'Error insert into values into Step at '
                                     || l_sec
                                     || ' for cust acct id:'
                                     || l_cust_account_id,
            p_distribution_list   => l_distro_list,
            p_module              => 'AR');
         wf_core.context ('XXWC_AR_BE_PKG',
                          'SUBS_AR_CBE_UPDATE_PROCESS',
                          p_event.getEventName (),
                          p_event.getEventKey ());
         wf_event.setErrorInfo (p_event, 'ERROR');

         RETURN 'ERROR';
   END;

   FUNCTION SUBS_AR_CBE_UPDATE_PROCESS (
      p_subscription_guid   IN     RAW,
      p_event               IN OUT wf_event_t)
      RETURN VARCHAR2
   IS
      --PRAGMA autonomous_transaction;
      /**************************************************************************
        FUNCTION Name: SUBS_AR_CBE_UPDATE_PROCESS

        PURPOSE:   This FUCNTION is used to raise the business event
                   to start hz cust account sites update trigger
        REVISIONS:
        Ver        Date        Author                     Description
        ---------  ----------  ---------------         -------------------------
           1.0        31-Aug-2016  Neha Saini              TMS#20160826-00318-IT  - replacing triggers on hz_cust_acct_sites_all with business events
           1.1        10/24/2016   Neha Saini              TMS# 20161019-00009  Adding logic to use profile option to debug more on why
                                                       business events failed .XXWC_AR_TRIGGERS_LOG
        **************************************************************************/



      --initialize
      l_record_exists     VARCHAR2 (1) DEFAULT 'F';
      l_product_exists    VARCHAR2 (1) DEFAULT 'F';
      l_step_prod_exist   VARCHAR2 (1) DEFAULT 'F';
      l_prod_string       VARCHAR2 (500) DEFAULT NULL;
      l_party_name        VARCHAR2 (200) DEFAULT NULL;
      l_party_sitenum     VARCHAR2 (200) DEFAULT NULL;
      l_addr1             VARCHAR2 (30) DEFAULT NULL;
      l_addr2             VARCHAR2 (30) DEFAULT NULL;
      l_city              VARCHAR2 (100) DEFAULT NULL;
      l_state             VARCHAR2 (50) DEFAULT NULL;
      l_postal_code       VARCHAR2 (50) DEFAULT NULL;
      l_country           VARCHAR2 (10) DEFAULT NULL;
      l_specialrate       VARCHAR2 (50) DEFAULT 'F';
      l_tax_state         VARCHAR2 (50) DEFAULT NULL;
      l_tax_flex_value    VARCHAR2 (100) DEFAULT 'XXWC_TAX_EXEMPTION_TYPE';
      l_process           BOOLEAN DEFAULT TRUE;

      -- Error DEBUG
      l_sec               VARCHAR2 (150);
      l_error_msg         VARCHAR2 (2000);
      l_err_callfrom      VARCHAR2 (75) DEFAULT 'SUBS_AR_CBE_UPDATE_PROCESS';
      l_distro_list       VARCHAR2 (75)
                             DEFAULT 'hdsoracledevelopers@hdsupply.com';
      -- l_distro_tax        VARCHAR2 (75)DEFAULT 'hdstaxwaresupport@hdsupply.com';  
      l_host              VARCHAR2 (256) := 'mailoutrelay.hdsupply.net';
      l_hostport          VARCHAR2 (20) := '25';
      l_sender            VARCHAR2 (100);
      l_sid               VARCHAR2 (8);
      l_attribute15       apps.hz_cust_acct_sites_all.attribute15%TYPE;
      l_old_attribute15   apps.hz_cust_acct_sites_all.attribute15%TYPE;
      l_cust_account_id   apps.hz_cust_acct_sites_all.cust_account_id%TYPE;
      l_org_id            apps.hz_cust_acct_sites_all.org_id%TYPE;
      l_party_site_id     NUMBER;

      -- taxware email changes starts
      l_distro_tax        VARCHAR2 (2000)--Increase the veriable lenght to 2000 TMS#20161019-00009
                             DEFAULT 'credittaxexempt@whitecap.net;';
      l_party_id          NUMBER;
      l_party_number      VARCHAR (30);
      l_party_name1       VARCHAR (360);
      l_city1             VARCHAR (60);
      l_county1           VARCHAR (60);
      l_state1            VARCHAR (60);
      l_postal_code1      VARCHAR (60);
      --changes for ver1.1 starts
      v_user_id           NUMBER;
      v_app_id            NUMBER;
      v_resp_id           NUMBER;
      v_resp_name         VARCHAR2 (100);
      v_app_name          VARCHAR2 (20);
      --changes for ver1.1 ends
      PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      l_cust_account_id :=
         TO_NUMBER (p_event.getvalueforparameter ('p_cust_account_id'));
      l_party_site_id :=
         TO_NUMBER (p_event.getvalueforparameter ('p_party_site_id'));
      l_attribute15 := p_event.getvalueforparameter ('p_attribute15');
      l_org_id := TO_NUMBER (p_event.getvalueforparameter ('p_org_id'));
      l_old_attribute15 := p_event.getvalueforparameter ('p_old_attribute15');
      -- changes for ver1.1 starts

      v_user_id := apps.FND_GLOBAL.USER_ID;


      BEGIN
         -- mo_global.init('AR');
         mo_global.set_policy_context ('S', 162);

         SELECT fa.application_id,
                fr.responsibility_id,
                fr.responsibility_name,
                fa.application_short_name
           INTO v_app_id,
                v_resp_id,
                v_resp_name,
                v_app_name
           FROM fnd_responsibility_vl fr, fnd_application fa
          WHERE     responsibility_name LIKE 'HDS Receivables Manager - WC'
                AND fr.application_id = fa.application_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            v_app_id := NULL;
            v_resp_id := NULL;
            v_resp_name := NULL;
            v_app_name := NULL;
      END;

      -- Initializing apps
      fnd_global.apps_initialize (user_id        => v_user_id,
                                  resp_id        => v_resp_id,
                                  resp_appl_id   => v_app_id);

      --changes for ver1.1 ends

      INSERT INTO XXWC.XXWC_AR_BE_TEMP_TBL
           VALUES (l_cust_account_id,
                   NULL,
                   l_old_attribute15,
                   l_org_id,
                   l_party_site_id,
                   l_attribute15,
                   'Inside the UPDATE subscription ',
                   fnd_global.user_id,
                   SYSDATE,
                   SYSDATE);                                          --ver1.1

      COMMIT;                                                         --ver1.1

      IF     (NVL (l_attribute15, 'x') != NVL (l_old_attribute15, 'x'))
         AND l_org_id = 162
      THEN
         -- get email machine.
         SELECT LOWER (name) INTO l_sid FROM v$database;

         l_sender := 'Oracle.Applications_' || l_sid || '@hdsupply.com';



         IF l_sid IN ('ebsdev', 'ebsqa', 'ebssbx')
         THEN
            l_distro_tax :=
               'Lenora.Allen@hdsupply.com;neha.saini@hdsupply.com';
         ELSE
            l_distro_tax :=
               'credittaxexempt@whitecap.net;Leslie.glaize@hdsupply.com;Stacie.ganganna@hdsupply.com';
         END IF;


         l_sec := 'loading party site number.';



         BEGIN
            SELECT psite.party_site_number, party_id
              INTO l_party_sitenum, l_party_id
              FROM hz_party_sites psite
             WHERE psite.party_site_id = l_party_site_id;

            INSERT INTO XXWC.XXWC_AR_BE_TEMP_TBL
                 VALUES (l_cust_account_id,
                         l_party_sitenum,
                         l_old_attribute15,
                         l_org_id,
                         l_party_site_id,
                         l_attribute15,
                         'got the party site num ',
                         fnd_global.user_id,
                         SYSDATE,
                         SYSDATE);                                    --ver1.1

            COMMIT;                                                   --ver1.1
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               l_party_sitenum := NULL;
            --email changes starts start
            WHEN TOO_MANY_ROWS
            THEN
               SELECT party_number,
                      party_name,
                      city,
                      county,
                      state,
                      postal_code
                 INTO l_party_number,
                      l_party_name1,
                      l_city1,
                      l_county1,
                      l_state1,
                      l_postal_code1
                 FROM hz_parties hcaa
                WHERE party_id = l_party_id;

               l_error_msg :=
                     'Multiple/Duplicate Party Site Numbers for Party Site Id ='
                  || l_party_site_id;
               xxcus_misc_pkg.html_email (
                  p_to              => l_distro_tax,
                  p_from            => l_sender,
                  p_text            => 'test',
                  p_html            =>    '<p><strong>Multiple/Duplicate Party Site Numbers error on the Customer table related to taxware.</strong> <br>
  <br>'
                                       || '<p><strong> Customer Site Number:</strong> ('
                                       || l_party_sitenum
                                       || ') <br> <br>'
                                       || '<p><strong> Customer Account Number:</strong> ('
                                       || l_party_number
                                       || ')  <br> <br> '
                                       || '<p><strong> Customer Site Name:</strong> ('
                                       || l_party_name1
                                       || ') <br> <br> '
                                       || '<p><strong> City:</strong> ('
                                       || l_city1
                                       || ')  <br> <br> '
                                       || '<p><strong> County:</strong> ('
                                       || l_county1
                                       || ')  <br> <br> '
                                       || '<p><strong> State:</strong> ('
                                       || l_state1
                                       || ')  <br> <br> '
                                       || '<p><strong> Zip:</strong> ('
                                       || l_postal_code1
                                       || ')  </p>',
                  p_subject         => 'STEPPROD_TBL errors',
                  p_smtp_hostname   => l_host,
                  p_smtp_portnum    => l_hostport);
            --email changes end
            WHEN OTHERS
            THEN
               l_error_msg :=
                     'Multiple/Duplicate Party Site Numbers for Party Site Id ='
                  || l_party_site_id;
               RAISE PROGRAM_ERROR;
         END;


         l_sec :=
            'Checking to see if record already exists.  Will not overwrite if record exists.';

         INSERT INTO XXWC.XXWC_AR_BE_TEMP_TBL
                 VALUES (
                           l_cust_account_id,
                           l_party_sitenum,
                           l_old_attribute15,
                           l_org_id,
                           l_party_site_id,
                           l_attribute15,
                           'Checking to see if record already exists.  Will not overwrite if record exists. ',
                           fnd_global.user_id,
                           SYSDATE,
                           SYSDATE);                                  --ver1.1

         COMMIT;                                                      --ver1.1

         BEGIN
            SELECT 'T'
              INTO l_record_exists
              FROM taxware.steptec_tbl
             WHERE key_1 = l_party_sitenum;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               l_record_exists := 'F';
            WHEN TOO_MANY_ROWS
            THEN
               SELECT party_number,
                      party_name,
                      city,
                      county,
                      state,
                      postal_code
                 INTO l_party_number,
                      l_party_name1,
                      l_city1,
                      l_county1,
                      l_state1,
                      l_postal_code1
                 FROM hz_parties hcaa
                WHERE party_id = l_party_id;

               l_error_msg :=
                     'Multiple/Duplicate record already exists for Party in table steptec_tbl for party site Num='
                  || l_party_sitenum;
               xxcus_misc_pkg.html_email (
                  p_to              => l_distro_tax,
                  p_from            => l_sender,
                  p_text            => 'test',
                  p_html            =>    '<p><strong>Multiple/Duplicate Party Site Numbers error on the Customer table related to taxware.</strong> <br>
  <br>'
                                       || '<p><strong> Customer Site Number:</strong> ('
                                       || l_party_sitenum
                                       || ') <br> <br>'
                                       || '<p><strong> Customer Account Number:</strong> ('
                                       || l_party_number
                                       || ')  <br> <br> '
                                       || '<p><strong> Customer Site Name:</strong> ('
                                       || l_party_name1
                                       || ') <br> <br> '
                                       || '<p><strong> City:</strong> ('
                                       || l_city1
                                       || ')  <br> <br> '
                                       || '<p><strong> County:</strong> ('
                                       || l_county1
                                       || ')  <br> <br> '
                                       || '<p><strong> State:</strong> ('
                                       || l_state1
                                       || ')  <br> <br> '
                                       || '<p><strong> Zip:</strong> ('
                                       || l_postal_code1
                                       || ')  </p>',
                  p_subject         => 'STEPPROD_TBL errors',
                  p_smtp_hostname   => l_host,
                  p_smtp_portnum    => l_hostport);
            --email changes end
            WHEN OTHERS
            THEN
               l_error_msg :=
                     'checking record already exists for Party  site in table steptec_tbl for party site Num=='
                  || l_party_sitenum;
               RAISE PROGRAM_ERROR;
         END;



         l_sec := 'Checking to see if products exists for the tax exemption.';

         INSERT INTO XXWC.XXWC_AR_BE_TEMP_TBL
                 VALUES (
                           l_cust_account_id,
                           l_party_sitenum,
                           l_old_attribute15,
                           l_org_id,
                           l_party_site_id,
                           l_attribute15,
                              'Checking to see if products exists for the tax exemption.l_record_exists '
                           || l_record_exists,
                           fnd_global.user_id,
                           SYSDATE,
                           SYSDATE);                                  --ver1.1

         COMMIT;                                                      --ver1.1


         BEGIN
            SELECT 'T'
              INTO l_product_exists
              FROM apps.fnd_flex_value_norm_hierarchy
             WHERE parent_flex_value = l_attribute15 AND ROWNUM = 1;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               l_product_exists := 'F';
            WHEN TOO_MANY_ROWS
            THEN
               SELECT party_number,
                      party_name,
                      city,
                      county,
                      state,
                      postal_code
                 INTO l_party_number,
                      l_party_name1,
                      l_city1,
                      l_county1,
                      l_state1,
                      l_postal_code1
                 FROM hz_parties hcaa
                WHERE party_id = l_party_id;

               l_error_msg :=
                     'Multiple/duplicate product exists for Party in table fnd_flex_value_norm_hierarchy for Flex Value= '
                  || l_attribute15;
               xxcus_misc_pkg.html_email (
                  p_to              => l_distro_tax,
                  p_from            => l_sender,
                  p_text            => 'test',
                  p_html            =>    '<p><strong>Multiple/Duplicate Party Site Numbers error on the Customer table related to taxware.</strong> <br>
  <br>'
                                       || '<p><strong> Customer Site Number:</strong> ('
                                       || l_party_sitenum
                                       || ') <br> <br>'
                                       || '<p><strong> Customer Account Number:</strong> ('
                                       || l_party_number
                                       || ')  <br> <br> '
                                       || '<p><strong> Customer Site Name:</strong> ('
                                       || l_party_name1
                                       || ') <br> <br> '
                                       || '<p><strong> City:</strong> ('
                                       || l_city1
                                       || ')  <br> <br> '
                                       || '<p><strong> County:</strong> ('
                                       || l_county1
                                       || ')  <br> <br> '
                                       || '<p><strong> State:</strong> ('
                                       || l_state1
                                       || ')  <br> <br> '
                                       || '<p><strong> Zip:</strong> ('
                                       || l_postal_code1
                                       || ')  </p>',
                  p_subject         => 'STEPPROD_TBL errors',
                  p_smtp_hostname   => l_host,
                  p_smtp_portnum    => l_hostport);
            --email changes end
            WHEN OTHERS
            THEN
               l_error_msg :=
                     'checking prodcut exists for Party in table fnd_flex_value_norm_hierarchy for Flex Value='
                  || l_attribute15;
               RAISE PROGRAM_ERROR;
         END;



         l_sec :=
            'Checking to see if products exists in STEP for the tax exemption.';

         INSERT INTO XXWC.XXWC_AR_BE_TEMP_TBL
                 VALUES (
                           l_cust_account_id,
                           l_party_sitenum,
                           l_old_attribute15,
                           l_org_id,
                           l_party_site_id,
                           l_attribute15,
                              'Checking to see if products exists in STEP for the tax exemption. l_product_exists '
                           || l_product_exists,
                           fnd_global.user_id,
                           SYSDATE,
                           SYSDATE);                                  --ver1.1

         COMMIT;                                                      --ver1.1

         BEGIN
            SELECT 'T'
              INTO l_step_prod_exist
              FROM taxware.stepprod_tbl stepprods
             WHERE stepprods.key_1 = l_party_sitenum;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               l_step_prod_exist := 'F';
            WHEN TOO_MANY_ROWS
            THEN
               SELECT party_number,
                      party_name,
                      city,
                      county,
                      state,
                      postal_code
                 INTO l_party_number,
                      l_party_name1,
                      l_city1,
                      l_county1,
                      l_state1,
                      l_postal_code1
                 FROM hz_parties hcaa
                WHERE party_id = l_party_id;

               l_error_msg :=
                     'Multiple/duplicate product exists in STEP for tax exemption in STEPPROD_TBL for Party site num='
                  || l_party_sitenum;
               xxcus_misc_pkg.html_email (
                  p_to              => l_distro_tax,
                  p_from            => l_sender,
                  p_text            => 'test',
                  p_html            =>    '<p><strong>Multiple/Duplicate Party Site Numbers error on the Customer table related to taxware.</strong> <br>
  <br>'
                                       || '<p><strong> Customer Site Number:</strong> ('
                                       || l_party_sitenum
                                       || ') <br> <br>'
                                       || '<p><strong> Customer Account Number:</strong> ('
                                       || l_party_number
                                       || ')  <br> <br> '
                                       || '<p><strong> Customer Site Name:</strong> ('
                                       || l_party_name1
                                       || ') <br> <br> '
                                       || '<p><strong> City:</strong> ('
                                       || l_city1
                                       || ')  <br> <br> '
                                       || '<p><strong> County:</strong> ('
                                       || l_county1
                                       || ')  <br> <br> '
                                       || '<p><strong> State:</strong> ('
                                       || l_state1
                                       || ')  <br> <br> '
                                       || '<p><strong> Zip:</strong> ('
                                       || l_postal_code1
                                       || ')  </p>',
                  p_subject         => 'STEPPROD_TBL errors',
                  p_smtp_hostname   => l_host,
                  p_smtp_portnum    => l_hostport);
            --email changes end
            WHEN OTHERS
            THEN
               l_error_msg :=
                     'Checking prodcut exists in STEP for tax exemption in STEPPROD_TBL for Party site num='
                  || l_party_sitenum;
               RAISE PROGRAM_ERROR;
         END;



         l_sec := 'Check to see if this is a TEC with special rates.';

         INSERT INTO XXWC.XXWC_AR_BE_TEMP_TBL
                 VALUES (
                           l_cust_account_id,
                           l_party_sitenum,
                           l_old_attribute15,
                           l_org_id,
                           l_party_site_id,
                           l_attribute15,
                              'Check to see if this is a TEC with special rates.l_step_prod_exist  '
                           || l_step_prod_exist,
                           fnd_global.user_id,
                           SYSDATE,
                           SYSDATE);                                  --ver1.1

         COMMIT;                                                      --ver1.1


         -- l_specialrate will be ='F' when there are no special rates(most cases)
         BEGIN
            SELECT ffv.attribute1
              INTO l_specialrate
              FROM apps.fnd_flex_values ffv, apps.fnd_flex_value_sets fvs
             WHERE     ffv.flex_value_set_id = fvs.flex_value_set_id
                   AND fvs.flex_value_set_name = l_tax_flex_value
                   AND ffv.flex_value = l_attribute15
                   AND ffv.attribute1 IS NOT NULL;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               l_specialrate := 'F';
            WHEN OTHERS
            THEN
               l_error_msg :=
                     'Checking TEC with special rates in fnd_flex_values for Party site Special rate ='
                  || l_attribute15;

               RAISE PROGRAM_ERROR;
         END;



         l_sec := 'the point where I update the main for this record to STEP.';

         INSERT INTO XXWC.XXWC_AR_BE_TEMP_TBL
                 VALUES (
                           l_cust_account_id,
                           l_party_sitenum,
                           l_old_attribute15,
                           l_org_id,
                           l_party_site_id,
                           l_attribute15,
                              'the point where I update the main for this record to STEP. l_specialrate '
                           || l_specialrate,
                           fnd_global.user_id,
                           SYSDATE,
                           SYSDATE);                                  --ver1.1

         COMMIT;                                                      --ver1.1

         SELECT SUBSTR (party.party_name, 1, 30),
                SUBSTR (loc.address1, 1, 30),
                SUBSTR (loc.address2, 1, 30),
                SUBSTR (loc.city, 1, 26),
                loc.state,
                SUBSTR (loc.postal_code, 1, 5),
                loc.country
           INTO l_party_name,
                l_addr1,
                l_addr2,
                l_city,
                l_state,
                l_postal_code,
                l_country
           FROM hz_parties party,
                hz_cust_accounts acct,
                hz_party_sites psite,
                hz_locations loc
          WHERE     party.party_id = acct.party_id
                AND acct.cust_account_id = l_cust_account_id
                AND acct.party_id = party.party_id
                AND psite.party_site_id = l_party_site_id
                AND psite.location_id = loc.location_id;

         INSERT INTO XXWC.XXWC_AR_BE_TEMP_TBL
              VALUES (l_cust_account_id,
                      l_party_sitenum,
                      l_old_attribute15,
                      l_org_id,
                      l_party_site_id,
                      l_attribute15,
                      'l_party_name  ' || l_party_name,
                      fnd_global.user_id,
                      SYSDATE,
                      SYSDATE);                                       --ver1.1

         COMMIT;                                                      --ver1.1

         BEGIN
            SELECT stcode
              INTO l_tax_state
              FROM taxware.stepstcn_tbl
             WHERE stalphacode = l_state;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               l_process := FALSE;

               xxcus_misc_pkg.html_email (
                  p_to              => l_distro_tax,
                  p_from            => l_sender,
                  p_text            => 'test',
                  p_html            =>    '<p><strong>Could not find a state in STEP.</strong><br>
  <br>Could not find an associated STATE record in STEP for Party Site : </p>'
                                       || l_party_sitenum,
                  p_subject         => 'ALERT** Tax trigger error during Certificate processing.',
                  p_smtp_hostname   => l_host,
                  p_smtp_portnum    => l_hostport);
            WHEN OTHERS
            THEN
               l_error_msg := ' Duplicate Data for St Code=' || l_state;
               RAISE PROGRAM_ERROR;
         END;

         INSERT INTO XXWC.XXWC_AR_BE_TEMP_TBL
              VALUES (l_cust_account_id,
                      l_party_sitenum,
                      l_old_attribute15,
                      l_org_id,
                      l_party_site_id,
                      l_attribute15,
                      'l_tax_state  ' || l_tax_state,
                      fnd_global.user_id,
                      SYSDATE,
                      SYSDATE);                                       --ver1.1

         COMMIT;                                                      --ver1.1


         IF l_country = 'US' AND l_process
         THEN
            l_sec := 'the point where I actually do the update';


            IF l_record_exists = 'T'
            THEN
               UPDATE taxware.steptec_tbl step
                  SET step.sstatecode = l_tax_state,
                      step.cproductflag =
                         DECODE (l_product_exists,
                                 'T', DECODE (l_specialrate, 'F', 'I', NULL),
                                 NULL),                   --step.cproductflag,
                      step.scustomeraddr1 = l_addr1,    --step.scustomeraddr1,
                      step.scustomeraddr2 = l_addr2,    --step.scustomeraddr2,
                      step.scustomerctyname = l_city, --step.scustomerctyname,
                      step.scustomerstatecode = l_state, --step.scustomerstatecode,
                      step.cactivecompflag =
                         DECODE (NVL (l_attribute15, 'None'),
                                 'None', 'D',
                                 'A'),
                      step.scustomerzipcode = l_postal_code, --step.scustomerzipcode,
                      step.cspecialrateflag =
                         DECODE (l_specialrate, 'F', NULL, 'Y') -- handles special rate
                WHERE step.key_1 = l_party_sitenum;

               COMMIT;

               INSERT INTO XXWC.XXWC_AR_BE_TEMP_TBL
                       VALUES (
                                 l_cust_account_id,
                                 l_party_sitenum,
                                 l_old_attribute15,
                                 l_org_id,
                                 l_party_site_id,
                                 l_attribute15,
                                    'update if product exists T '
                                 || DECODE (NVL (l_attribute15, 'None'),
                                            'None', 'D',
                                            'A'),
                                 fnd_global.user_id,
                                 SYSDATE,
                                 SYSDATE);                            --ver1.1

               COMMIT;                                                --ver1.1
            ELSE
               -- new record, do insert
               BEGIN
                  INSERT
                    INTO taxware.steptec_tbl step (step.scompanyid,
                                                   step.key_1,
                                                   step.ctransactflag,
                                                   step.key_2,
                                                   step.key_3,
                                                   step.sstatecode,
                                                   step.cjurislevel,
                                                   step.skeyoccurnum,
                                                   step.cproductflag,
                                                   step.cdeflttostcert,
                                                   step.cchkexpirdate,
                                                   step.scustomername,
                                                   step.scustomeraddr1,
                                                   step.scustomeraddr2,
                                                   step.scustomerctyname,
                                                   step.scustomerstatecode,
                                                   step.scustomerzipcode,
                                                   step.scustomeravpgeo,
                                                   step.staxcertifnum,
                                                   step.sreasoncode,
                                                   step.datecertifreceived,
                                                   step.dateeffective,
                                                   step.dateexpiration,
                                                   step.cactivecompflag,
                                                   step.keycode,
                                                   step.cspecialrateflag)
                     VALUES (
                               'WCI',                       --step.scompanyid,
                               l_party_sitenum,                  --step.key_1,
                               ' ',                     -- step.ctransactflag,
                               ' ',                             -- step.key_2,
                               ' ',                             -- step.key_3,
                               l_tax_state,                 --step.sstatecode,
                               1,                          --step.cjurislevel,
                               0,                         --step.skeyoccurnum,
                               DECODE (
                                  l_product_exists,
                                  'T', DECODE (l_specialrate, 'F', 'I', NULL),
                                  NULL), -- (IF products exist THEN I ELSE null)  --step.cproductflag,
                               'D',                     --step.cdeflttostcert,
                               NULL,                     --step.cchkexpirdate,
                               l_party_name,             --step.scustomername,
                               l_addr1,                 --step.scustomeraddr1,
                               l_addr2,                 --step.scustomeraddr2,
                               l_city,                --step.scustomerctyname,
                               l_state,             --step.scustomerstatecode,
                               l_postal_code,         --step.scustomerzipcode,
                               NULL,                   --step.scustomeravpgeo,
                               'N/A',                    --step.staxcertifnum,
                               'BW',                       --step.sreasoncode,
                               SYSDATE,             --step.datecertifreceived,
                               SYSDATE,                  --step.dateeffective,
                               NULL,    --SYSDATE+1460,  --step.dateexpiration
                               DECODE (NVL (l_attribute15, 'None'),
                                       'None', 'D',
                                       'A'), --step.cactivecompflag  Cert complete =CC, Cert Active =CA || A(CC ON,CA ON), B(CC OFF,CA ON), C(CC ON,CA ON) D(CC OFF, CA OFF)
                               'R',                            --,step.keycode
                               DECODE (l_specialrate, 'F', NULL, 'Y') -- handles special rate
                                                                     );

                  COMMIT;

                  INSERT INTO XXWC.XXWC_AR_BE_TEMP_TBL
                          VALUES (
                                    l_cust_account_id,
                                    l_party_sitenum,
                                    l_old_attribute15,
                                    l_org_id,
                                    l_party_site_id,
                                    l_attribute15,
                                       'it did not existed so inserting value '
                                    || DECODE (
                                          l_product_exists,
                                          'T', DECODE (l_specialrate,
                                                       'F', 'I',
                                                       NULL),
                                          NULL),
                                    fnd_global.user_id,
                                    SYSDATE,
                                    SYSDATE);                         --ver1.1

                  COMMIT;                                             --ver1.1
               EXCEPTION
                  WHEN DUP_VAL_ON_INDEX
                  THEN
                     UPDATE taxware.steptec_tbl step
                        SET step.sstatecode = l_tax_state,
                            step.cproductflag =
                               DECODE (
                                  l_product_exists,
                                  'T', DECODE (l_specialrate, 'F', 'I', NULL),
                                  NULL),                  --step.cproductflag,
                            step.scustomeraddr1 = l_addr1, --step.scustomeraddr1,
                            step.scustomeraddr2 = l_addr2, --step.scustomeraddr2,
                            step.scustomerctyname = l_city, --step.scustomerctyname,
                            step.scustomerstatecode = l_state, --step.scustomerstatecode,
                            step.cactivecompflag =
                               DECODE (NVL (l_attribute15, 'None'),
                                       'None', 'D',
                                       'A'),
                            step.scustomerzipcode = l_postal_code, --step.scustomerzipcode,
                            step.cspecialrateflag =
                               DECODE (l_specialrate, 'F', NULL, 'Y') -- handles special rate
                      WHERE step.key_1 = l_party_sitenum;

                     COMMIT;

                     INSERT INTO XXWC.XXWC_AR_BE_TEMP_TBL
                          VALUES (l_cust_account_id,
                                  l_party_sitenum,
                                  l_old_attribute15,
                                  l_org_id,
                                  l_party_site_id,
                                  l_attribute15,
                                  'DUP_VALINDX exception ',
                                  fnd_global.user_id,
                                  SYSDATE,
                                  SYSDATE);                           --ver1.1

                     COMMIT;                                          --ver1.1
                  WHEN OTHERS
                  THEN
                     l_error_msg :=
                           'Error: at when others exception while insert '
                        || SQLERRM;
                     RAISE PROGRAM_ERROR;
               END;
            END IF;

            IF l_product_exists = 'T'
            THEN
               BEGIN
                  DELETE FROM taxware.steprate_tbl
                        WHERE key_1 = l_party_sitenum;

                  COMMIT;

                  INSERT INTO XXWC.XXWC_AR_BE_TEMP_TBL
                       VALUES (l_cust_account_id,
                               l_party_sitenum,
                               l_old_attribute15,
                               l_org_id,
                               l_party_site_id,
                               l_attribute15,
                               'deleted if product exists =T ',
                               fnd_global.user_id,
                               SYSDATE,
                               SYSDATE);                              --ver1.1

                  COMMIT;                                             --ver1.1
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     NULL; -- no action, no rows to delete, so must be a new change to special rate.
               END;

               FOR c_products IN (               --query to load product codes
                                  SELECT child_flex_value_low
                                    FROM apps.fnd_flex_value_norm_hierarchy
                                   WHERE parent_flex_value = l_attribute15)
               LOOP
                  l_prod_string :=
                        l_prod_string
                     || RPAD (c_products.child_flex_value_low, 25);

                  --special rates are actually normalized and not rpadd'ed.  wierd.
                  INSERT INTO XXWC.XXWC_AR_BE_TEMP_TBL
                          VALUES (
                                    l_cust_account_id,
                                    l_party_sitenum,
                                    l_old_attribute15,
                                    l_org_id,
                                    l_party_site_id,
                                    l_attribute15,
                                    'special rates are actually normalized and not rpadd ',
                                    fnd_global.user_id,
                                    SYSDATE,
                                    SYSDATE);                         --ver1.1

                  COMMIT;                                             --ver1.1

                  IF l_specialrate != 'F'
                  THEN
                     BEGIN
                        -- if special rates

                        INSERT
                          INTO taxware.steprate_tbl c (c.companyid,
                                                       c.key_1,
                                                       c.stcode,
                                                       c.steptecoccurnumber,
                                                       c.productcode,
                                                       c.basispercent,
                                                       c.federaltaxrate,
                                                       c.statetaxrate,
                                                       c.secstatetaxrate,
                                                       c.countytaxrate,
                                                       c.citytaxrate,
                                                       c.seccountytaxrate,
                                                       c.seccitytaxrate,
                                                       c.districttaxrate,
                                                       c.countytaxind)
                        VALUES ('WCI',
                                l_party_sitenum,                         --key
                                l_tax_state,                       --statecode
                                0,
                                c_products.child_flex_value_low, --product code
                                1,                                    --basis,
                                0,                                --fedtaxrate
                                0,                              --statetaxrate
                                0,                           --secstatetaxrate
                                TO_NUMBER (l_specialrate),     --countytaxrate
                                0,                               --citytaxrate
                                0,                            --seccounty rate
                                0,                              --seccity rate
                                0,                         --district tax rate
                                1                            --county tax rate
                                 );

                        COMMIT;

                        INSERT INTO XXWC.XXWC_AR_BE_TEMP_TBL
                             VALUES (l_cust_account_id,
                                     l_party_sitenum,
                                     l_old_attribute15,
                                     l_org_id,
                                     l_party_site_id,
                                     l_attribute15,
                                     'insert  if special rates ',
                                     fnd_global.user_id,
                                     SYSDATE,
                                     SYSDATE);                        --ver1.1

                        COMMIT;                                       --ver1.1
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                           l_error_msg :=
                              'Error at special rates insert: ' || SQLERRM;
                           RAISE PROGRAM_ERROR;
                     END;
                  END IF;
               END LOOP;


               l_sec :=
                  'the point where I update the STEP CHILD for this record to STEP.';



               -- update  PRODUCT CODE TABLE.
               INSERT INTO XXWC.XXWC_AR_BE_TEMP_TBL
                    VALUES (l_cust_account_id,
                            l_party_sitenum,
                            l_old_attribute15,
                            l_org_id,
                            l_party_site_id,
                            l_attribute15,
                            'update  PRODUCT CODE TABLE. ',
                            fnd_global.user_id,
                            SYSDATE,
                            SYSDATE);                                 --ver1.1

               COMMIT;                                                --ver1.1

               IF l_step_prod_exist = 'T'
               THEN
                  IF l_specialrate = 'F'
                  THEN
                     --if not special rate
                     UPDATE taxware.stepprod_tbl prod
                        SET prod.stcode =
                               (SELECT stcode
                                  FROM taxware.stepstcn_tbl
                                 WHERE stalphacode = l_state),  --prod.stcode,
                            prod.productentries = l_prod_string --prod.productentries
                      WHERE prod.key_1 = l_party_sitenum;

                     COMMIT;

                     INSERT INTO XXWC.XXWC_AR_BE_TEMP_TBL
                             VALUES (
                                       l_cust_account_id,
                                       l_party_sitenum,
                                       l_old_attribute15,
                                       l_org_id,
                                       l_party_site_id,
                                       l_attribute15,
                                          'if not special rate update :  l_prod_string  '
                                       || l_prod_string,
                                       fnd_global.user_id,
                                       SYSDATE,
                                       SYSDATE);                      --ver1.1

                     COMMIT;                                          --ver1.1
                  END IF;
               ELSE
                  IF l_specialrate = 'F'
                  THEN
                     -- if not special rate

                     BEGIN
                        INSERT
                          INTO taxware.stepprod_tbl prod (
                                  prod.companyid,
                                  prod.key_1,
                                  prod.stcode,
                                  prod.jurisdictionlevel,
                                  prod.steptecoccurnumber,
                                  prod.stepprodoccurnumber,
                                  prod.productentries)
                        VALUES ('WCI',                       --prod.companyid,
                                l_party_sitenum,                 --prod.key_1,
                                (SELECT stcode
                                   FROM taxware.stepstcn_tbl
                                  WHERE stalphacode = l_state), --prod.stcode,
                                1,                   --prod.jurisdictionlevel,
                                0,                  --prod.steptecoccurnumber,
                                1,                 --prod.stepprodoccurnumber,
                                l_prod_string            --prod.productentries
                                             );

                        COMMIT;

                        INSERT INTO XXWC.XXWC_AR_BE_TEMP_TBL
                                VALUES (
                                          l_cust_account_id,
                                          l_party_sitenum,
                                          l_old_attribute15,
                                          l_org_id,
                                          l_party_site_id,
                                          l_attribute15,
                                             'if not special rate:  l_prod_string  '
                                          || l_prod_string,
                                          fnd_global.user_id,
                                          SYSDATE,
                                          SYSDATE);                   --ver1.1

                        COMMIT;                                       --ver1.1
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                           l_error_msg :=
                                 'Error at if not special rates insert: '
                              || SQLERRM;
                           RAISE PROGRAM_ERROR;
                     END;
                  END IF;
               END IF;
            ELSE        -- products in oracle do not exist, make taxware match
               DELETE FROM taxware.stepprod_tbl prod
                     WHERE prod.key_1 = l_party_sitenum;

               COMMIT;

               INSERT INTO XXWC.XXWC_AR_BE_TEMP_TBL
                       VALUES (
                                 l_cust_account_id,
                                 l_party_sitenum,
                                 l_old_attribute15,
                                 l_org_id,
                                 l_party_site_id,
                                 l_attribute15,
                                 'products in oracle do not exist, make taxware match ',
                                 fnd_global.user_id,
                                 SYSDATE,
                                 SYSDATE);                            --ver1.1

               COMMIT;                                                --ver1.1
            END IF;
         END IF;
      END IF;

      INSERT INTO XXWC.XXWC_AR_BE_TEMP_TBL
           VALUES (l_cust_account_id,
                   l_party_sitenum,
                   l_old_attribute15,
                   l_org_id,
                   l_party_site_id,
                   l_attribute15,
                   'update subscription end ',
                   fnd_global.user_id,
                   SYSDATE,
                   SYSDATE);                                          --ver1.1

      COMMIT;                                                         --ver1.1

      RETURN 'SUCCESS';
   EXCEPTION
      WHEN PROGRAM_ERROR
      THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_err_callfrom,
            p_calling             => l_sec,
            p_ora_error_msg       => l_error_msg || ' Error ' || SQLERRM,
            p_error_desc          =>    'Error insert into values into Step at '
                                     || l_sec
                                     || ' for Party Site Number='
                                     || l_party_sitenum
                                     || ' With New Party Site Id = '
                                     || l_party_site_id,
            p_distribution_list   => l_distro_list,
            p_module              => 'AR');
         wf_core.context ('XXWC_AR_BE_PKG',
                          'SUBS_AR_CBE_UPDATE_PROCESS',
                          p_event.getEventName (),
                          p_event.getEventKey ());
         wf_event.setErrorInfo (p_event, 'ERROR');
         RETURN 'ERROR';
      WHEN OTHERS
      THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_err_callfrom,
            p_calling             => l_sec,
            p_ora_error_msg       => l_error_msg || ' Error ' || SQLERRM,
            p_error_desc          =>    'Error insert into values into Step at '
                                     || l_sec
                                     || ' for Party Site Number='
                                     || l_party_sitenum
                                     || ' With New Party Site Id = '
                                     || l_party_site_id,
            p_distribution_list   => l_distro_list,
            p_module              => 'AR');
         wf_core.context ('XXWC_AR_BE_PKG',
                          'SUBS_AR_CBE_UPDATE_PROCESS',
                          p_event.getEventName (),
                          p_event.getEventKey ());
         apps.wf_event.setErrorInfo (p_event, 'ERROR');
         RETURN 'ERROR';
   END;
END XXWC_AR_BE_PKG;
/