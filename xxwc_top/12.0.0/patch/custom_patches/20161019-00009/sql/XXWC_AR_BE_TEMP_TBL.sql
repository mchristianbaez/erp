/**************************************************************************
TABLE Name: XXWC_AR_BE_TEMP_TBL.sql

PURPOSE:   This is a temp table to get log messages
REVISIONS:
Ver        Date        Author                     Description
---------  ----------  ---------------         -------------------------
1.0        10/20/2016   Neha Saini              TMS # 20161019-00009 table created for Adding log message
**************************************************************************/


CREATE TABLE XXWC.XXWC_AR_BE_TEMP_TBL
(
   cust_account_id    NUMBER,
   party_site_num     VARCHAR2 (500),
   attribute15_old    VARCHAR2 (255),
   org_id             NUMBER,
   party_site_id      NUMBER,
   attribute15        VARCHAR2 (225),
   msg                VARCHAR2 (1000),
   created_by         varchar2 (20),
   creation_date      DATE,
   last_update_date   DATE
);