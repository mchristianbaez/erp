CREATE OR REPLACE TRIGGER xxwc_cust_acct_sites_all_trg
   AFTER INSERT
   ON ar.hz_cust_acct_sites_all
   REFERENCING NEW AS new OLD AS old
   FOR EACH ROW
DECLARE
   --PRAGMA autonomous_transaction;
   /**********************************************************************************************
    File Name: APPS.hr_cust_acct_sites_all_trg

    PROGRAM TYPE: TRIGGER

    PURPOSE: Triiger to load new data into STEP taxware system

    HISTORY
    =============================================================================
           Last Update Date : 06/25/2012
    =============================================================================
    =============================================================================
    VERSION DATE          AUTHOR(S)       DESCRIPTION
    ------- -----------   --------------- ----------------------------------------
    1.0     03-MAR-2012   Manny Rodriguez  Created.
    1.1     25-MAR-2012   Manny Rodriguez  Changed certificates to Active.
    1.2     03-APR-2012   Manny Rodriguez  Changed error handling to show cust_id
    1.3     20-APR-2012   Manny Rodriguez  Changed the trigger to look for 'None'
                                           changed Geocode and Use Date to Null
    1.4     25-APR-2012   Manny rodriguez  Changed to create records all the time
                                           but with dynamic certificate checkboxes
    1.5     10-MAY-2012   Manny Rodriguez  Blocking international addresses.
    1.6     25-Jun-2012   Manny Rodriguez  Added state filtering and emails
    1.7     18-AUG-2014   Manny Rodriguez  Changed variables for address to 30char #260988
    1.8     16-OCT-2014   Maharajan Shunmugam TMS#20141001-00161 Canada Multi Org changes
    1.9     22-APR-2015   Raghavendra S    TMS# 20150421-00198 - trigger special characters in address field need to be fixed
    2.0     4/25/2016     Neha Saini       TMS# 20160128-00232 - added more filter to replace special characters and increase address length.
    2.1     10/24/2016    Neha Saini       TMS# 20161019-00009  Adding logic to use profile option to debug more on why
                                           business events failed .XXWC_AR_TRIGGERS_LOG
   ***********************************************************************************************/

   --initialize
   l_record_exists    VARCHAR2 (1) DEFAULT 'F';
   l_product_exists   VARCHAR2 (1) DEFAULT 'F';
   l_prod_string      VARCHAR2 (500) DEFAULT NULL;
   l_party_name       VARCHAR2 (200) DEFAULT NULL;
   l_party_sitenum    VARCHAR2 (200) DEFAULT NULL;
   l_addr1            VARCHAR2 (50) DEFAULT NULL;--ver 2.0
   l_addr2            VARCHAR2 (50) DEFAULT NULL;--ver 2.0
   l_city             VARCHAR2 (100) DEFAULT NULL;
   l_state            VARCHAR2 (50) DEFAULT NULL;
   l_country          VARCHAR2 (10) DEFAULT NULL;
   l_postal_code      VARCHAR2 (50) DEFAULT NULL;
   l_specialrate      VARCHAR2 (50) DEFAULT 'F';
   l_tax_state        VARCHAR2 (50) DEFAULT NULL;
   l_process          BOOLEAN DEFAULT TRUE;
   l_tax_flex_value   VARCHAR2 (100) DEFAULT 'XXWC_TAX_EXEMPTION_TYPE';

   -- Error DEBUG
   l_sec              VARCHAR2 (150);
   l_error_msg         VARCHAR2 (2000);
   l_err_callfrom     VARCHAR2 (75) DEFAULT 'xxwc_cust_acct_sites_all_trg';
   l_distro_list      VARCHAR2 (75) DEFAULT 'hdsoracledevelopers@hdsupply.com';
   l_distro_tax       VARCHAR2 (75) DEFAULT 'hdstaxwaresupport@hdsupply.com';
   l_host             VARCHAR2 (256) := 'mailoutrelay.hdsupply.net';
   l_hostport         VARCHAR2 (20) := '25';
   l_sender           VARCHAR2 (100);
   l_sid              VARCHAR2 (8);
BEGIN
   --v1.3 -- adding 'None' to logic.
   --v1.4 -- removed None and null logic
   --IF :new.attribute15 IS NOT NULL /*AND :new.attribute15 !='None' */ THEN
   --ver2.1 starts
   IF FND_PROFILE.VALUE('XXWC_AR_TRIGGERS_LOG')='Y' THEN
      l_sec :=
      'calling apps.XXWC_AR_BE_PKG.raise_ar_tax_insert_process to invoke business events';

   apps.XXWC_AR_BE_PKG.raise_ar_tax_insert_process (
      p_org_id            => :new.org_id,
      p_cust_account_id   => :new.cust_account_id,
      p_party_site_id     => :new.party_site_id,
      p_attribute15       => :new.attribute15,
      p_error_msg         => l_error_msg);

   insert into XXWC.XXWC_AR_BE_TEMP_TBL values(:new.cust_account_id,null,null,:new.org_id,:new.party_site_id,:new.attribute15,'called businees event from update trigger',fnd_global.user_id,sysdate,sysdate);
  
   l_sec := 'trigger processing completed.';
   
   ELSE
   --ver2.1 ends
   IF :new.cust_account_id != -1 AND :new.org_id = 162
   THEN
      -- get email machine.
      SELECT LOWER (name) INTO l_sid FROM v$database;

      l_sender := 'Oracle.Applications_' || l_sid || '@hdsupply.com';


      l_sec := 'Gathering information';
      
      SELECT SUBSTR (party.party_name, 1, 30)
            ,psite.party_site_number
            ,REGEXP_REPLACE(replace(replace(SUBSTR (TRIM(loc.address1), 1, 30),'�',CHR(39)),'�',CHR(34)),'[~!@#$%^&*()_+=\\{}[\]:�;�<,>.\/?]', '') -- Added Replace function for V 1.9 ----changed for ver 2.0 by Neha
            ,REGEXP_REPLACE(replace(replace(SUBSTR (TRIM(loc.address2), 1, 30),'�',CHR(39)),'�',CHR(34)),'[~!@#$%^&*()_+=\\{}[\]:�;�<,>.\/?]', '') -- Added Replace function for V 1.9 ----changed for ver 2.0 by Neha
            ,SUBSTR (loc.city, 1, 26)
            ,loc.state
            ,SUBSTR (loc.postal_code, 1, 5)
            ,loc.country
        INTO l_party_name
            ,l_party_sitenum
            ,l_addr1
            ,l_addr2
            ,l_city
            ,l_state
            ,l_postal_code
            ,l_country
        FROM hz_parties party
            ,hz_cust_accounts acct
            ,hz_party_sites psite
            ,hz_locations loc
       WHERE     party.party_id = acct.party_id
             AND acct.cust_account_id = :new.cust_account_id
             AND acct.party_id = party.party_id
             AND psite.party_site_id = :new.party_site_id
             AND psite.location_id = loc.location_id;

      --v1.5
      IF l_country = 'US'
      THEN
         l_sec := 'Checking to see if record already exists.  Will not overwrite if record exists.';

         BEGIN
            SELECT 'T'
              INTO l_record_exists
              FROM taxware.steptec_tbl
             WHERE key_1 = l_party_sitenum;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               l_record_exists := 'F';
            WHEN TOO_MANY_ROWS
            THEN
               l_process := FALSE;



               l_sec := 'Tax trigger error during Certificate processing.';

               xxcus_misc_pkg.html_email (
                  p_to              => l_distro_tax
                 ,p_from            => l_sender
                 ,p_text            => 'test'
                 ,p_html            =>    '<p><strong>Error on the Customer table related to taxware.</strong><br>
  <br>Retreieved more certificates than expected for Party Site : </p>'
                                       || l_party_sitenum
                 ,p_subject         => 'ALERT** Tax trigger error during Certificate processing.'
                 ,p_smtp_hostname   => l_host
                 ,p_smtp_portnum    => l_hostport);
         /*        xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_err_callfrom,
                                                      p_calling => 'STEP Taxware',
                                                      --p_request_id => l_req_id,
                                                      p_ora_error_msg => SQLERRM,
                                                      p_error_desc => 'Error:  Retreieved more certificates than expected for '||l_sec||' for party site :'||l_party_sitenum,
                                                      p_distribution_list => l_distro_tax,
                                                      p_module => 'AR');    */


         END;


         BEGIN
            SELECT stcode
              INTO l_tax_state
              FROM taxware.stepstcn_tbl
             WHERE stalphacode = l_state;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               l_process := FALSE;
               /*        xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_err_callfrom,
                                                            p_calling => 'STEP Taxware Insert Trigger',
                                                            --p_request_id => l_req_id,
                                                            p_ora_error_msg => SQLERRM,
                                                            p_error_desc => 'Error: could not find a state in STEP '||l_sec||' for party site :'||l_party_sitenum,
                                                            p_distribution_list => l_distro_tax,
                                                            p_module => 'AR');      */

               xxcus_misc_pkg.html_email (
                  p_to              => l_distro_tax
                 ,p_from            => l_sender
                 ,p_text            => 'test'
                 ,p_html            =>    '<p><strong>Could not find a state in STEP.</strong><br>
  <br>Could not find an associated STATE record in STEP for Party Site : </p>'
                                       || l_party_sitenum
                 ,p_subject         => 'ALERT** Tax trigger error during Certificate processing.'
                 ,p_smtp_hostname   => l_host
                 ,p_smtp_portnum    => l_hostport);
            WHEN OTHERS
            THEN                                                           -- Added others Exception for V 1.8
               l_error_msg := ' Duplicate Data for St Code=' || l_state;
               RAISE PROGRAM_ERROR;
         END;



         IF l_record_exists = 'F' AND l_process
         THEN
            l_sec := 'Checking to see if products exists for the tax exemption.';

            BEGIN
               SELECT 'T'
                 INTO l_product_exists
                 FROM apps.fnd_flex_value_norm_hierarchy
                WHERE parent_flex_value = :new.attribute15 AND ROWNUM = 1;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  l_product_exists := 'F';
               WHEN TOO_MANY_ROWS
               THEN                                                 -- Added too_many_rows Exception for V 1.8
                  l_error_msg :=
                        'Multiple/duplicate prodcut exists for Party in table fnd_flex_value_norm_hierarchy for Flex Value= '
                     || :new.attribute15;
                  RAISE PROGRAM_ERROR;
               WHEN OTHERS
               THEN                                                        -- Added others Exception for V 1.8
                  l_error_msg :=
                        'checking prodcut exists for Party in table fnd_flex_value_norm_hierarchy for Flex Value='
                     || :new.attribute15;
                  RAISE PROGRAM_ERROR;
            END;


            l_sec := 'Check to see if this is a TEC with special rates.';

            -- l_specialrate will be ='F' when there are no special rates(most cases)
            BEGIN
               SELECT ffv.attribute1
                 INTO l_specialrate
                 FROM apps.fnd_flex_values ffv, apps.fnd_flex_value_sets fvs
                WHERE     ffv.flex_value_set_id = fvs.flex_value_set_id
                      AND fvs.flex_value_set_name = l_tax_flex_value
                      AND ffv.flex_value = :new.attribute15
                      AND ffv.attribute1 IS NOT NULL;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  l_specialrate := 'F';
               WHEN OTHERS
               THEN                                                        -- Added others Exception for V 1.8
                  l_error_msg :=
                        'Checking TEC with special rates in fnd_flex_values for Party site Special rate ='
                     || :new.attribute15;

                  RAISE PROGRAM_ERROR;
            END;



            l_sec := 'the point where I load the details for this record to STEP.';

            INSERT INTO taxware.steptec_tbl step (step.scompanyid
                                                 ,step.key_1
                                                 ,step.ctransactflag
                                                 ,step.key_2
                                                 ,step.key_3
                                                 ,step.sstatecode
                                                 ,step.cjurislevel
                                                 ,step.skeyoccurnum
                                                 ,step.cproductflag
                                                 ,step.cdeflttostcert
                                                 ,step.cchkexpirdate
                                                 ,step.scustomername
                                                 ,step.scustomeraddr1
                                                 ,step.scustomeraddr2
                                                 ,step.scustomerctyname
                                                 ,step.scustomerstatecode
                                                 ,step.scustomerzipcode
                                                 ,step.scustomeravpgeo
                                                 ,step.staxcertifnum
                                                 ,step.sreasoncode
                                                 ,step.datecertifreceived
                                                 ,step.dateeffective
                                                 ,step.dateexpiration
                                                 ,step.cactivecompflag
                                                 ,step.keycode
                                                 ,step.cspecialrateflag)
                 VALUES ('WCI'
                        ,                                                                   --step.scompanyid,
                         l_party_sitenum
                        ,                                                                        --step.key_1,
                         ' '
                        ,                                                               -- step.ctransactflag,
                         ' '
                        ,                                                                       -- step.key_2,
                         ' '
                        ,                                                                       -- step.key_3,
                         l_tax_state
                        ,                                                                   --step.sstatecode,
                         1
                        ,                                                                  --step.cjurislevel,
                         0
                        ,                                                                 --step.skeyoccurnum,
                         DECODE (l_product_exists, 'T', DECODE (l_specialrate, 'F', 'I', NULL), NULL)
                        ,                        -- (IF products exist THEN I ELSE null)  --step.cproductflag,
                         'D'
                        , --step.cdeflttostcert,  cactivecompflag = A(complete), B( active), C(certificate active) D(neither checkbox ticked)
                         NULL
                        ,                                                          --step.cchkexpirdate,  v1.3
                         l_party_name
                        ,l_addr1                                                          --step.scustomername,
                        ,l_addr2                                                  --step.scustomeraddr1,
                        ,                                                 --step.scustomeraddr2,
                         l_city
                         ,                                                 --step.scustomerctyname,
                         l_state
                        ,                                                           --step.scustomerstatecode,
                         l_postal_code
                        ,                                                             --step.scustomerzipcode,
                         NULL
                        ,                                                         --step.scustomeravpgeo, v1.3
                         'N/A'
                        ,                                                                --step.staxcertifnum,
                         'BW'
                        ,                                                                  --step.sreasoncode,
                         SYSDATE
                        ,                                                           --step.datecertifreceived,
                         SYSDATE
                        ,                                                                --step.dateeffective,
                         NULL
                        ,                                               --SYSDATE+1460,  --step.dateexpiration
                         DECODE (NVL (:new.attribute15, 'None'), 'None', 'D', 'A')
                        , --step.cactivecompflag V1.4 Change to A. Cert complete =CC, Cert Active =CA || A(CC ON,CA ON), B(CC OFF,CA ON), C(CC ON,CA ON) D(CC OFF, CA OFF)
                         'R'
                        ,                                                                      --,step.keycode
                         DECODE (l_specialrate, 'F', NULL, 'Y'));



            IF l_product_exists = 'T'
            THEN
               BEGIN
                  DELETE FROM taxware.steprate_tbl
                        WHERE key_1 = l_party_sitenum;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     NULL;           -- no action, no rows to delete, so must be a new change to special rate.
               END;

               FOR c_products IN (                                               --query to load product codes
                                  SELECT child_flex_value_low
                                    FROM apps.fnd_flex_value_norm_hierarchy
                                   WHERE parent_flex_value = :new.attribute15)
               LOOP
                  l_prod_string := l_prod_string || RPAD (c_products.child_flex_value_low, 25);


                  --special rates are actually normalized and not rpadd'ed.  wierd.
                  IF l_specialrate != 'F'
                  THEN
                     -- if special rates


                     INSERT INTO taxware.steprate_tbl c (c.companyid
                                                        ,c.key_1
                                                        ,c.stcode
                                                        ,c.steptecoccurnumber
                                                        ,c.productcode
                                                        ,c.basispercent
                                                        ,c.federaltaxrate
                                                        ,c.statetaxrate
                                                        ,c.secstatetaxrate
                                                        ,c.countytaxrate
                                                        ,c.citytaxrate
                                                        ,c.seccountytaxrate
                                                        ,c.seccitytaxrate
                                                        ,c.districttaxrate
                                                        ,c.countytaxind)
                          VALUES ('WCI'
                                 ,l_party_sitenum
                                 ,                                                                       --key
                                  l_tax_state
                                 ,                                                                 --statecode
                                  0
                                 ,c_products.child_flex_value_low
                                 ,                                                              --product code
                                  1
                                 ,                                                                    --basis,
                                  0
                                 ,                                                                --fedtaxrate
                                  0
                                 ,                                                              --statetaxrate
                                  0
                                 ,                                                           --secstatetaxrate
                                  TO_NUMBER (l_specialrate)
                                 ,                                                             --countytaxrate
                                  0
                                 ,                                                               --citytaxrate
                                  0
                                 ,                                                            --seccounty rate
                                  0
                                 ,                                                              --seccity rate
                                  0
                                 ,                                                         --district tax rate
                                  1                                                          --county tax rate
                                   );
                  END IF;
               END LOOP;


               IF l_specialrate = 'F'
               THEN
                  -- INSERT INTO PRODUCT CODE TABLE.
                  INSERT INTO taxware.stepprod_tbl prod (prod.companyid
                                                        ,prod.key_1
                                                        ,prod.stcode
                                                        ,prod.jurisdictionlevel
                                                        ,prod.steptecoccurnumber
                                                        ,prod.stepprodoccurnumber
                                                        ,prod.productentries)
                       VALUES ('WCI'
                              ,                                                              --prod.companyid,
                               l_party_sitenum
                              ,                                                                  --prod.key_1,
                                (SELECT stcode
                                   FROM taxware.stepstcn_tbl
                                  WHERE stalphacode = l_state)
                              ,                                                                 --prod.stcode,
                               1
                              ,                                                      --prod.jurisdictionlevel,
                               0
                              ,                                                     --prod.steptecoccurnumber,
                               1
                              ,                                                    --prod.stepprodoccurnumber,
                               l_prod_string                                             --prod.productentries
                                            );
               END IF;
            END IF;
         END IF;
      END IF;
   END IF;
END IF;--ver2.1
EXCEPTION
   WHEN PROGRAM_ERROR
   THEN                                                                                     -- Added for V 1.8
      xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => l_err_callfrom
        ,p_calling             => l_sec
        ,                                                                          --p_request_id => l_req_id,
         p_ora_error_msg       => l_error_msg || ' Error ' || SQLERRM
        ,p_error_desc          =>    'Error insert into values into Step at '
                                  || l_sec
                                  || ' for Party Site Number='
                                  || l_party_sitenum
                                  || ' With New Party Site Id = '
                                  || :new.party_site_id
        ,p_distribution_list   => l_distro_list
        ,p_module              => 'AR');
   WHEN OTHERS
   THEN
      xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => l_err_callfrom
        ,p_calling             => 'STEP Taxware'
        ,                                                                          --p_request_id => l_req_id,
         p_ora_error_msg       => SQLERRM
        ,p_error_desc          =>    'Error insert into values into Step at '
                                  || l_sec
                                  || ' for cust acct id:'
                                  || :new.cust_account_id
        ,p_distribution_list   => l_distro_list
        ,p_module              => 'AR');
END;
/