CREATE OR REPLACE 
PACKAGE BODY APPS.XXWC_GENERATE_FORECAST_ADI_PKG
AS
   /*************************************************************************
   *   $Header xxwc_generate_forecast_adi_pkg $
   *   Module Name: xxwc_generate_forecast_adi_pkg
   *
   *   PURPOSE:   Package used to submit XXWC Generate Demand Forecast via Web ADI Upload TMS Ticket # 20130201-01386 
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        08/12/2013  Lee Spitzer                Initial Version
   * ***************************************************************************/

PROCEDURE submit_ccp           (p_org_code  in varchar2
                               ,p_forecast_designator in varchar2
                               ,p_branch_dc_mode in varchar2
                               ,p_item_range    in varchar2
                               ,p_item_category in varchar2
                               ,p_item_number   in varchar2
                               ,p_bucket_type   in number
                               ,p_start_date    in date
                               ,p_num_of_fc_periods    in number
                               ,p_num_of_prev_periods in number
                               ,p_constant_seasonality in number
                               ,p_seasonality1 in number
                               ,p_seasonality2 in number
                               ,p_seasonality3 in number
                               ,p_seasonality4 in number
                               ,p_seasonality5 in number
                               ,p_seasonality6 in number
                               ,p_seasonality7 in number
                               ,p_seasonality8 in number
                               ,p_seasonality9 in number
                               ,p_seasonality10 in number
                               ,p_seasonality11 in number
                               ,p_seasonality12 in number)
                            IS
                            
                            
    l_error_msg varchar2(240);
    l_req_id NUMBER;
    l_date   VARCHAR2(30);
    l_organization_id number;
                            
    BEGIN
    
      l_date := to_char(p_start_date,'YYYY/MM/DD HH24:MI:SS');
      
        BEGIN
            select organization_id
            into   l_organization_id
            from   mtl_parameters
            where   organization_code = p_org_code;
        EXCEPTION
             when others then
                    l_error_msg := substr('Invalid org code ' || p_org_code || ' ' || sqlerrm,1,240); 
                    RAISE_APPLICATION_ERROR(-20001,l_error_msg); 

        END;
    
  
              l_req_id := NULL;
              l_req_id :=
                 fnd_request.submit_request (application       => 'XXWC'
                                             ,program          => 'XXWC_GENERATE_FORECAST'
                                             ,description      => NULL
                                             ,start_time       => SYSDATE
                                             ,sub_request      => FALSE
                                             ,argument1        => l_organization_id
                                             ,argument2        => p_forecast_designator
                                             ,argument3        => p_branch_dc_mode 
                                             ,argument4        => p_item_range   
                                             ,argument5        => p_item_category
                                             ,argument6        => p_item_number
                                             ,argument7        => p_bucket_type
                                             ,argument8        => l_date --p_start_date
                                             ,argument9        => p_num_of_fc_periods
                                             ,argument10       => p_num_of_prev_periods
                                             ,argument11       => p_constant_seasonality
                                             ,argument12       => p_seasonality1
                                             ,argument13       => p_seasonality2
                                             ,argument14       => p_seasonality3
                                             ,argument15       => p_seasonality4
                                             ,argument16       => p_seasonality5
                                             ,argument17       => p_seasonality6
                                             ,argument18       => p_seasonality7
                                             ,argument19       => p_seasonality8
                                             ,argument20       => p_seasonality9
                                             ,argument21       => p_seasonality10
                                             ,argument22       => p_seasonality11
                                             ,argument23       => p_seasonality12
                                            );

              -- commit;
              IF NVL (l_req_id, 0) > 0 THEN
              
                NULL;
              
              ELSE
                 
                l_error_msg := substr('Could not submit request for forecast ' || p_forecast_designator,1,240);
                RAISE_APPLICATION_ERROR(-20001,l_error_msg);
                
              END IF;

        
    EXCEPTION
       when others then
         l_error_msg := substr(sqlerrm,1,240); 
         RAISE_APPLICATION_ERROR(-20001,l_error_msg); 
    END submit_ccp;
    
END XXWC_GENERATE_FORECAST_ADI_PKG;
/
