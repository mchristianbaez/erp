------------------------------------------------------------------------------------
/******************************************************************************
   NAME       :  XXEIS.eis_xxwc_ap_ar_open_balance_v
   REVISIONS:
	Ver         Date            Author             Description
   ---------  ----------    ---------------  ------------------------------------
    1.0       nil      			nil	            Initial Version 
	1.1       4-Jul-2017       Siva	            TMS#20170518-00144  
******************************************************************************/
CREATE OR REPLACE FORCE VIEW xxeis.eis_xxwc_ap_ar_open_balance_v
(
   customer_name
  ,customer_number
  ,customer_addr_street_address1
  ,customer_addr_street_address2
  ,customer_city_address
  ,customer_state_address
  ,customer_address_zipcode
  ,customer_telephone_number
  ,vendor_name
  ,vendor_number
  ,vendor_address_street
  ,vendor_address_city
  ,vendor_address_state
  ,vendor_address_zipcode
  ,vendor_telephone_number
  ,vendor_tax_id
  ,customer_tax_id
  ,vendor_site_code
  ,ar_open_balnace
  ,ap_open_balnace
)
AS
     SELECT hca.account_name customer_name
           ,hca.account_number customer_number
           ,hp.address1 customer_addr_street_address1
           ,hp.address2 customer_addr_street_address2
           ,hp.city customer_city_address
           ,hp.state customer_state_address
           ,hp.postal_code customer_address_zipcode
           ,hp.primary_phone_area_code || '-' || hp.primary_phone_number
               customer_telephone_number
           ,pv.vendor_name vendor_name
           ,pv.segment1 vendor_number
           ,pvs.address_line1 vendor_address_street
           ,pvs.city vendor_address_city
           ,pvs.state vendor_address_state
           ,pvs.zip vendor_address_zipcode
           ,pvs.phone vendor_telephone_number
           ,pv.num_1099 vendor_tax_id
           ,hp.jgzz_fiscal_code customer_tax_id
           ,pvs.vendor_site_code vendor_site_code
		   ,(SELECT SUM(NVL(ps.amount_due_remaining,0)) ar_open_balnace
    			FROM ra_customer_trx trx,
      				ar_payment_schedules ps
    			WHERE trx.bill_to_customer_id = hca.cust_account_id
    			AND trx.customer_trx_id       = ps.customer_trx_id
    		) ar_open_balnace  --Added for version 1.1
    	   ,(SELECT SUM(NVL(aps.amount_remaining,0)) ap_open_balnace
    			FROM ap_invoices api,
      				ap_payment_schedules aps
    			WHERE pv.vendor_id      = api.vendor_id
    			AND api.org_id          = 162
    			AND api.invoice_id      = aps.invoice_id
    			AND api.org_id          = aps.org_id
    			AND api.cancelled_date IS NULL
    		) ap_open_balnace  --Added for version 1.1
           --,SUM (ps.amount_due_remaining) ar_open_balnace  --Commented for version 1.1
           --,SUM (aps.amount_remaining) ap_open_balnace  --Commented for version 1.1
       FROM hz_parties hp
           ,hz_cust_accounts hca
           ,                                     --HZ_CUST_ACCT_SITES    HCAS,
                                                 --HZ_CUST_SITE_USES     HCSU,
                                                  --HZ_PARTY_SITES        HPS,
                                                   --HZ_LOCATIONS          HL,
                                                  --HZ_CONTACT_POINTS     HCP,
            --ra_customer_trx trx  --Commented for version 1.1
           --,ar_payment_schedules ps --Commented for version 1.1
           --,  --Commented for version 1.1
			po_vendors pv
           ,po_vendor_sites pvs
           --,ap_invoices api  --Commented for version 1.1
           --,ap_payment_schedules aps  --Commented for version 1.1
           --,hr_operating_units hou  --Commented for version 1.1
      WHERE     hp.party_id = hca.party_id
            --AND HCA.CUST_ACCOUNT_ID         = HCAS.CUST_ACCOUNT_ID
            --AND HCAS.CUST_ACCT_SITE_ID      = HCSU.CUST_ACCT_SITE_ID
            --AND HPS.PARTY_SITE_ID           = HCAS.PARTY_SITE_ID
            --AND HPS.LOCATION_ID             = HL.LOCATION_ID
            --and HP.PARTY_ID                 = HCP.OWNER_TABLE_ID(+)
            --AND HCP.OWNER_TABLE_NAME        = 'HZ_PARTY_SITES'--'HZ_PARTIES'
            --AND HCP.PRIMARY_FLAG(+)         = 'Y'
            --AND HCSU.SITE_USE_CODE          = 'BILL_TO'
            --AND HCP.PHONE_NUMBER IS NOT NULL
            --AND trx.bill_to_customer_id = hca.cust_account_id  --Commented for version 1.1
            --AND TRX.BILL_TO_SITE_USE_ID     = HCSU.SITE_USE_ID
            --AND trx.customer_trx_id = ps.customer_trx_id  --Commented for version 1.1
            AND (   pv.vendor_name = hp.party_name
                 OR pvs.phone = hp.primary_phone_number)
            AND pv.vendor_id = pvs.vendor_id
			AND NVL (pvs.pay_site_flag, 'Y') = 'Y'
		 /*  --Commented Start for version 1.1
            AND api.vendor_id = pv.vendor_id
            AND api.org_id = pvs.org_id
            AND api.invoice_id = aps.invoice_id
            AND api.org_id = aps.org_id
            AND hou.name = 'HDS White Cap - Org'
            AND hou.organization_id = pvs.org_id            
            --AND HP.PARTY_NAME='HD SUPPLY WATERWORKS'
            AND api.cancelled_date IS NULL
   GROUP BY hca.account_name
           ,hca.account_number
           ,hp.address1
           ,hp.address2
           ,hp.city
           ,hp.state
           ,hp.postal_code
           ,hp.primary_phone_area_code || '-' || hp.primary_phone_number
           ,pv.vendor_name
           ,pv.segment1
           ,pvs.address_line1
           ,pvs.city
           ,pvs.state
           ,pvs.zip
           ,pvs.phone
           ,pv.num_1099
           ,hp.jgzz_fiscal_code
           ,pvs.vendor_site_code*/  --Commented End for version 1.1
  AND EXISTS
    (SELECT 1
    FROM ra_customer_trx trx
    WHERE trx.bill_to_customer_id = hca.cust_account_id
    )  --Added for version 1.1
  AND EXISTS
    (SELECT 1
    FROM ap_invoices api
    WHERE api.vendor_id     = pv.vendor_id
    AND api.org_id          = 162
    AND api.cancelled_date IS NULL
    )  --Added for version 1.1
/
