/*************************************************************************
  $Header TMS_20170727-00249_LINE_STUCKED_IN_PICKED.sql $
  Module Name: TMS_20170727-00249 Data Fix for Line stuck in picked 

  PURPOSE: Data Fix for Line stuck in picked

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        19-SEP-2017  Pattabhi Avula         TMS#20170727-00249

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000;

DECLARE

l_line_id     NUMBER := 98290250;
l_org_id      NUMBER;
l_count       NUMBER;
l_activity_id NUMBER;
l_result      VARCHAR2(30);
rcnt          NUMBER := 0;

BEGIN

dbms_output.enable(1000000);

    rcnt:=rcnt+1;

    dbms_output.put_line('Correcting Line_id : '||l_line_id);

    BEGIN

    OE_Standard_WF.OEOL_SELECTOR
	   (p_itemtype => 'OEOL'
	   ,p_itemkey => TO_CHAR(l_line_id)
	   ,p_actid => 12345
	   ,p_funcmode => 'SET_CTX'
	   ,p_result => l_result
	   );

    wf_engine.CompleteActivityInternalName('OEOL',TO_CHAR(l_line_id), 'SHIP_LINE', 'SHIP_CONFIRM');

    UPDATE OE_ORDER_LINES_ALL OEL
    SET    OEL.SHIPPED_QUANTITY = OEL.ORDERED_QUANTITY,
           OEL.ACTUAL_SHIPMENT_DATE =  (SELECT MAX(NVL(wnd.initial_pickup_date,SYSDATE))
                                        FROM   wsh_new_deliveries wnd,wsh_delivery_assignments wda,wsh_delivery_details wdd
                                        WHERE  wnd.delivery_id = wda.delivery_id
                                        AND    wda.delivery_detail_id=wdd.delivery_detail_id
                                        AND    wdd.source_code='OE'
                                        AND    wdd.source_line_id=oel.line_id
                                        AND    wdd.released_status='C'),
           OEL.FLOW_STATUS_CODE='SHIPPED'
    WHERE  OEL.LINE_ID = l_line_Id;
	
	UPDATE oe_order_lines_all 
       SET open_flag = 'N', 
           last_updated_by = -99999999, 
           last_update_date = SYSDATE 
     WHERE line_id = 98248155; 

 
    
    IF rcnt > 100 THEN
       rcnt := 0;
       COMMIT;
    END IF;
    EXCEPTION
       WHEN OTHERS THEN
	   dbms_output.put_line('Error : '||SQLERRM);
    END;

End;
/
commit;
/