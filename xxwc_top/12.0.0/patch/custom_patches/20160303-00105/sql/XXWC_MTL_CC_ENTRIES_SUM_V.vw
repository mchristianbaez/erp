CREATE OR REPLACE VIEW APPS.XXWC_MTL_CC_ENTRIES_SUM_V as
/********************************************************************************************************************************
      $Header XXWC_MTL_CC_ENTRIES_SUM_V $    
      Module Name: XXWC_MTL_CC_ENTRIES_SUM_V

      PURPOSE:   This view is used in XXWC_CC_ENTRY form

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------------------------------------------------------------
      1.0        10-Jul-2013 Lee spitzer          Initial Version - TMS#20130214-01096  Implement Physical Inventory enhancements 
                                                                    @SC @FIN / Implement Physical Inventory enhancements 
      1.1        11-Nov-2016 Pattabhi Avula       TMS#20160303-00105 -- Added new condition to pick return_status null
   *********************************************************************************************************************************/
(SELECT mcce.COUNT_LIST_SEQUENCE,
       mcce.cycle_count_entry_id,
       mcce.cycle_count_header_id,
       mcce.inventory_item_id,
       mcce.organization_id,
       msib.segment1 item_number,
       mcce.primary_uom_code,
       mcce.lot_number,
       mcce.subinventory,
       mcce.entry_status_code,
       --sum(nvl(mcce.count_quantity_current,0)) sum_count_quantity_current-- primary_quantity
       nvl(sum(xcet.quantity),0) primary_quantity
FROM   MTL_CYCLE_COUNT_ENTRIES_V mcce,
       MTL_SYSTEM_ITEMS_B MSIB,
       XXWC.XXWC_CC_ENTRY_TABLE XCET
WHERE  mcce.inventory_item_id = msib.inventory_item_id
AND    mcce.organization_id = msib.organization_id
AND    mcce.cycle_count_header_id = xcet.cycle_count_header_id(+)
AND    mcce.cycle_count_entry_id = xcet.cycle_count_entry_id(+)
AND    xcet.process_flag(+) = 1
AND    xcet.return_status IS NULL -- Ver#1.1
GROUP BY mcce.COUNT_LIST_SEQUENCE,
       mcce.cycle_count_entry_id,
       mcce.cycle_count_header_id,
       mcce.inventory_item_id,
       mcce.organization_id,
       msib.segment1,
       mcce.primary_uom_code,
       mcce.lot_number,
       mcce.subinventory,
       mcce.entry_status_code
      )
/