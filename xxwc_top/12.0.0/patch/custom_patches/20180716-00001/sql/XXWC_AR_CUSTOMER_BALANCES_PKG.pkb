CREATE OR REPLACE 
PACKAGE BODY      XXWC_AR_CUSTOMER_BALANCES_PKG
AS
   /*************************************************************************
   *   Module Name: xxwc_ar_customer_balances_pkg.pkg
   *
   *   PURPOSE:   Customer open balances outbound interface
   *
   *   REVISIONS:
   *   Version    Date        Author               Description
   *   ---------  ----------  ---------------      -------------------------
   *   1.0        12/19/2011  Srini Gutha          Initial Version
   *   1.1        09/24/2014  Pattabhi Avula	   TMS# 20141001-00031  - 
   *											   WC Canada: MultiOrg: Week 
   *											   of Sep 22 (Pattabhi)
   *   1.2        06/27/2018  Pattabhi Avula	   TMS#20180716-00001 - Customer 
   *                                               Balances Program and View Correction
   * ************************************************************************/

   /*************************************************************************
      Procedure : write_error

      PURPOSE:   This procedure logs error message
      Parameter:
             IN
                 p_debug_msg      -- Debug Message
    ************************************************************************/

   --add message to concurrent output file
   PROCEDURE write_error (p_debug_msg IN VARCHAR2)
   IS
      l_req_id          NUMBER := fnd_global.conc_request_id;
      l_err_callfrom    VARCHAR2 (75)
         DEFAULT 'XXWC_AR_CUSTOMER_BALANCES_PKG.OPEN_BALANCE';
      l_err_callpoint   VARCHAR2 (75) DEFAULT 'START';
      l_distro_list     VARCHAR2 (75) DEFAULT 'HDSOracleDevelopers@hdsupply.com'; -- 'HDSOracleDevelopers@hdsupply.com';
   BEGIN
      fnd_file.put_line (fnd_file.LOG, p_debug_msg);
      xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => l_err_callfrom
        ,p_calling             => l_err_callpoint
        ,p_request_id          => l_req_id
        ,p_ora_error_msg       => SUBSTR (p_debug_msg, 1, 2000)
        ,p_error_desc          => 'Error running XXWC_AR_CUSTOMER_BALANCES_PKG with PROGRAM ERROR'
        ,p_distribution_list   => l_distro_list
        ,p_module              => 'AR');
   END write_error;

   /*************************************************************************
    Procedure : open_balance

    PURPOSE:   Create file for customer open balances
    Parameter:
           IN
                p_directory_name      -- Directory Name
                p_file_name           -- File Name
                p_org_id              -- Operating Unit
				
   *   Version    Date        Author               Description
   *   ---------  ----------  ---------------      -------------------------
   *   1.2    06/27/2018  Pattabhi Avula	   TMS#20180716-00001 - Customer 
   *                                           Balances Program and View Correction
  **************************************************************************/
   PROCEDURE open_balance (p_errbuf              OUT VARCHAR2
                          ,p_retcode             OUT VARCHAR2
                          ,p_directory_name   IN OUT VARCHAR2
                          ,p_file_name        IN OUT VARCHAR2
                          ,p_org_id           IN     NUMBER   
						  )
   IS
      --
      l_file   UTL_FILE.file_type;

      --
      -- 05/30/2012 CGonzalez: Modified the last bucket to include the over 120 days bucket
      --        excluded in first code
      CURSOR c_customer_balances
      IS
         SELECT                                      -- sub_acct_number||'|'||
		        org_id
			    || '|'
			    || account_number
			    || '|'
			    ||PRISM_CUST_NUMBER
			    || '|'			   
                ||prism_legacy_party_site_number
                || '|'
                || LTRIM (TO_CHAR (current_balance, '9999990.99'))
                || '|'
                || LTRIM (TO_CHAR (thirty_days_bal, '9999990.99'))
                || '|'
                || LTRIM (TO_CHAR (sixty_days_bal, '9999990.99'))
                || '|'
                || LTRIM (TO_CHAR (ninety_days_bal, '9999990.99'))
                || '|'
                || LTRIM (TO_CHAR (onetwenty_days_bal, '9999990.99'))  -- Ver#1.2
				|| '|'
				|| LTRIM (TO_CHAR (over_onetwenty_days_bal, '9999990.99'))    -- Ver#1.2               
				|| '|'	
				|| LTRIM (TO_CHAR (total_due, '9999990.99')) balances        -- Ver#1.2                                    --||'|'||
           -- LTRIM(TO_CHAR(over_onetwenty_days_bal, '9999990.99')) balances
           FROM xxwc_customer_balance_vw
          WHERE org_id = p_org_id;
		    
   --
   BEGIN
      fnd_file.put_line (fnd_file.LOG, 'Start of the procedure...  ');
      --
      fnd_file.put_line (fnd_file.LOG
                        ,' p_directory_name =>' || p_directory_name);
      fnd_file.put_line (fnd_file.LOG, ' p_file_name      =>' || p_file_name);
      fnd_file.put_line (fnd_file.LOG, ' p_org_id         =>' || p_org_id);

      --
      IF p_file_name IS NULL
      THEN
         fnd_file.put_line (
            fnd_file.LOG
           ,' File name is not passed. Setting the file name to ARCUSTUPD.txt. ');
         p_file_name := 'ARCUSTUPD.txt';
      END IF;

      --
      IF p_directory_name IS NULL
      THEN
         fnd_file.put_line (
            fnd_file.LOG
           ,' Directory name is not passed. Setting the directory to ORACLE_INT_UC4. ');
         p_directory_name := 'ORACLE_INT_UC4';
      END IF;

      --
      fnd_file.put_line (fnd_file.LOG, ' Opening the File handler.');
      --Open the file handler
      l_file :=
         UTL_FILE.fopen (location    => p_directory_name
                        ,filename    => p_file_name
                        ,open_mode   => 'w');
      --
      fnd_file.put_line (fnd_file.LOG, ' Writing to the file ...');

      --
      FOR cur_rec IN c_customer_balances
      LOOP
         UTL_FILE.put_line (l_file, cur_rec.balances);
      END LOOP;

      --
      fnd_file.put_line (fnd_file.LOG, ' Closing the the file ...');
      --Closing the file handler
      UTL_FILE.fclose (l_file);
      fnd_file.put_line (fnd_file.LOG, 'End of procedure...  ');
   EXCEPTION
      WHEN UTL_FILE.invalid_path
      THEN
         UTL_FILE.fclose (l_file);
         p_errbuf := 'File Path is invalid.';
         p_retcode := '2';
         write_error (p_errbuf);
      WHEN UTL_FILE.invalid_mode
      THEN
         UTL_FILE.fclose (l_file);
         p_errbuf := 'The open_mode parameter in FOPEN is invalid.';
         p_retcode := '2';
         write_error (p_errbuf);
      WHEN UTL_FILE.invalid_filehandle
      THEN
         UTL_FILE.fclose (l_file);
         p_errbuf := 'File handle is invalid..';
         p_retcode := '2';
         write_error (p_errbuf);
      WHEN UTL_FILE.invalid_operation
      THEN
         UTL_FILE.fclose (l_file);
         p_errbuf := 'File could not be opened or operated on as requested';
         p_retcode := '2';
         write_error (p_errbuf);
      WHEN UTL_FILE.write_error
      THEN
         UTL_FILE.fclose (l_file);
         p_errbuf :=
            'Operating system error occurred during the write operation';
         p_retcode := '2';
         write_error (p_errbuf);
      WHEN UTL_FILE.internal_error
      THEN
         UTL_FILE.fclose (l_file);
         p_errbuf := 'Unspecified PL/SQL error.';
         p_retcode := '2';
         write_error (p_errbuf);
      WHEN UTL_FILE.file_open
      THEN
         UTL_FILE.fclose (l_file);
         p_errbuf :=
            'The requested operation failed because the file is open.';
         p_retcode := '2';
         write_error (p_errbuf);
      WHEN UTL_FILE.invalid_filename
      THEN
         UTL_FILE.fclose (l_file);
         p_errbuf := 'The filename parameter is invalid.';
         p_retcode := '2';
         write_error (p_errbuf);
      WHEN UTL_FILE.access_denied
      THEN
         UTL_FILE.fclose (l_file);
         p_errbuf := 'Permission to access to the file location is denied.';
         p_retcode := '2';
         write_error (p_errbuf);
      WHEN OTHERS
      THEN
         UTL_FILE.fclose (l_file);
         p_errbuf := 'Error Msg :' || SQLERRM;
         p_retcode := '2';
         write_error (p_errbuf);
   END open_balance;

   /*************************************************************************
     Procedure : submit_request

     PURPOSE:   Submits the concurrent request
     Parameter:
            IN
                 p_directory_name       -- Directory Name
                 p_file_name            -- File Name
                 p_user_name            -- User Name
                 p_responsibility_name  -- Responsibility Name
                 p_org_name             -- Operating Unit Name
   **************************************************************************/
   PROCEDURE submit_request (p_directory_name        IN VARCHAR2
                            ,p_file_name             IN VARCHAR2
                            ,p_user_name             IN VARCHAR2
                            ,p_responsibility_name   IN VARCHAR2
                            ,p_org_name              IN VARCHAR2)
   IS
      --
      -- Package Variables
      --

      l_package               VARCHAR2 (50) := 'XXWC_AR_CUSTOMER_BALANCES_PKG';
      l_dflt_email            VARCHAR2 (200) := 'HDSOracleDevelopers@hdsupply.com'; 
      l_email                 fnd_user.email_address%TYPE;

      l_req_id                NUMBER NULL;
      v_phase                 VARCHAR2 (50);
      v_status                VARCHAR2 (50);
      v_dev_status            VARCHAR2 (50);
      v_dev_phase             VARCHAR2 (50);
      v_message               VARCHAR2 (250);
      v_error_message         VARCHAR2 (3000);
      v_supplier_id           NUMBER;
      v_rec_cnt               NUMBER := 0;
      l_message               VARCHAR2 (150);
      l_errormessage          VARCHAR2 (3000);
      pl_errorstatus          NUMBER;
      l_can_submit_request    BOOLEAN := TRUE;
      l_globalset             VARCHAR2 (100);
      l_err_msg               VARCHAR2 (3000);
      l_err_code              NUMBER;
      l_sec                   VARCHAR2 (255);
      l_statement             VARCHAR2 (9000);
      l_user_id               NUMBER;
      l_responsibility_id     NUMBER;
      l_resp_application_id   NUMBER;
      l_org_id                NUMBER;

      -- Error DEBUG
      l_err_callfrom          VARCHAR2 (75)
         DEFAULT 'XXWC_AR_CUSTOMER_BALANCES_PKG.submit_request';
      l_err_callpoint         VARCHAR2 (75) DEFAULT 'START';
      l_distro_list           VARCHAR2 (75)
                                 DEFAULT 'HDSOracleDevelopers@hdsupply.com'; 
   BEGIN
      --------------------------------------------------------------------------
      -- Derive Org Id
      --------------------------------------------------------------------------
      BEGIN
         SELECT organization_id
           INTO l_org_id
           FROM hr_operating_units
          WHERE name = p_org_name;
      EXCEPTION
         WHEN OTHERS
         THEN
            RAISE PROGRAM_ERROR;
      END;

      --------------------------------------------------------------------------
      -- Deriving UserId
      --------------------------------------------------------------------------
      BEGIN
         SELECT user_id
           INTO l_user_id
           FROM fnd_user
          WHERE     1 = 1
                AND user_name = UPPER (p_user_name)
                AND TRUNC (SYSDATE) BETWEEN start_date
                                        AND NVL (end_date
                                                ,TRUNC (SYSDATE) + 1);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_err_msg :=
               'UserName - ' || p_user_name || ' not defined in Oracle';
            RAISE PROGRAM_ERROR;
         WHEN OTHERS
         THEN
            l_err_msg :=
               'Error deriving user_id for UserName - ' || p_user_name;
            RAISE PROGRAM_ERROR;
      END;

      --------------------------------------------------------------------------
      -- Deriving ResponsibilityId and ResponsibilityApplicationId
      --------------------------------------------------------------------------
      BEGIN
         SELECT responsibility_id, application_id
           INTO l_responsibility_id, l_resp_application_id
           FROM fnd_responsibility_vl
          WHERE     responsibility_name = p_responsibility_name
                AND TRUNC (SYSDATE) BETWEEN start_date
                                        AND NVL (end_date
                                                ,TRUNC (SYSDATE) + 1);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_err_msg :=
                  'Responsibility - '
               || p_responsibility_name
               || ' not defined in Oracle';
            RAISE PROGRAM_ERROR;
         WHEN OTHERS
         THEN
            l_err_msg :=
                  'Error deriving Responsibility_id for ResponsibilityName - '
               || p_responsibility_name;
            RAISE PROGRAM_ERROR;
      END;

      --------------------------------------------------------------------------
      -- Apps Initialize
      --------------------------------------------------------------------------

      fnd_global.apps_initialize (l_user_id
                                 ,l_responsibility_id
                                 ,l_resp_application_id);

      --------------------------------------------------------------------------
      -- Submit "XXWC AR Customer Balances Outbound Interface"
      --------------------------------------------------------------------------

      l_req_id :=
         fnd_request.submit_request (application   => 'XXWC'
                                    ,program       => 'XXWC_AR_CUST_BALANCE'
                                    ,description   => NULL
                                    ,start_time    => SYSDATE
                                    ,sub_request   => FALSE
                                    ,argument1     => p_directory_name
                                    ,argument2     => p_file_name
                                    ,argument3     => l_org_id);

      COMMIT;
      DBMS_OUTPUT.put_line ('After fnd_request ' || l_req_id);

      IF (l_req_id != 0)
      THEN
         IF fnd_concurrent.wait_for_request (l_req_id
                                            ,6
                                            ,15000
                                            ,v_phase
                                            ,v_status
                                            ,v_dev_phase
                                            ,v_dev_status
                                            ,v_message)
         THEN
            v_error_message :=
                  CHR (10)
               || 'ReqID='
               || l_req_id
               || ' DPhase '
               || v_dev_phase
               || ' DStatus '
               || v_dev_status
               || CHR (10)
               || ' MSG - '
               || v_message;

            -- Error Returned
            IF v_dev_phase != 'COMPLETE' OR v_dev_status != 'NORMAL'
            THEN
               l_statement :=
                     'An error occured running the XXWC_AR_CUSTOMER_BALANCES_PKG.'
                  || v_error_message
                  || '.';
               fnd_file.put_line (fnd_file.LOG, l_statement);
               fnd_file.put_line (fnd_file.output, l_statement);
               RAISE PROGRAM_ERROR;
            END IF;
         -- Then Success!
         ELSE
            l_statement :=
                  'An error occured running the XXWC_AR_CUSTOMER_BALANCES_PKG.'
               || v_error_message
               || '.';
            fnd_file.put_line (fnd_file.LOG, l_statement);
            fnd_file.put_line (fnd_file.output, l_statement);
            RAISE PROGRAM_ERROR;
         END IF;
      ELSE
         l_statement :=
            'An error occured running the XXWC_AR_CUSTOMER_BALANCES_PKG.';
         fnd_file.put_line (fnd_file.LOG, l_statement);
         fnd_file.put_line (fnd_file.output, l_statement);
         RAISE PROGRAM_ERROR;
      END IF;

      -- dbms_output.put_line(l_sec);
      DBMS_OUTPUT.put_line ('Request ID:  ' || l_req_id);
   EXCEPTION
      WHEN PROGRAM_ERROR
      THEN
         ROLLBACK;
         l_err_code := 2;
         -- l_err_msg  := l_sec;
         l_err_msg := l_err_msg || ' ERROR ' || SUBSTR (SQLERRM, 1, 2000);
         DBMS_OUTPUT.put_line (l_err_msg);
         fnd_file.put_line (fnd_file.LOG, l_err_msg);
         fnd_file.put_line (fnd_file.output, l_err_msg);
         l_err_callpoint := 'Program Error.';

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_err_callfrom
           ,p_calling             => l_err_callpoint
           ,p_request_id          => l_req_id
           ,p_ora_error_msg       => SQLERRM
           ,p_error_desc          => 'Error running XXWC_AR_CUSTOMER_BALANCES_PKG with PROGRAM ERROR'
           ,p_distribution_list   => l_distro_list
           ,p_module              => 'AR');

         fnd_file.put_line (fnd_file.output, 'Fix the error!');
      WHEN OTHERS
      THEN
         -- fnd_file.put_line(fnd_file.log, l_sec);
         -- fnd_file.put_line(fnd_file.output, l_sec);
         l_err_code := 2;
         -- l_err_msg  := l_sec;
         l_err_msg :=
               l_sec
            || l_err_msg
            || ' ERROR '
            || SQLCODE
            || SUBSTR (SQLERRM, 1, 2000);
         DBMS_OUTPUT.put_line (l_err_msg);
         fnd_file.put_line (fnd_file.LOG, l_err_msg);
         fnd_file.put_line (fnd_file.output, l_err_msg);
         l_err_callpoint := 'When Others.';

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_err_callfrom
           ,p_calling             => l_err_callpoint
           ,p_request_id          => l_req_id
           ,p_ora_error_msg       => SUBSTR (l_err_msg, 1, 2000)
           ,p_error_desc          => 'Error running XXWC_AR_CUSTOMER_BALANCES_PKG with OTHERS Exception'
           ,p_distribution_list   => l_distro_list
           ,p_module              => 'AR');
   END submit_request;
END XXWC_AR_CUSTOMER_BALANCES_PKG;
/