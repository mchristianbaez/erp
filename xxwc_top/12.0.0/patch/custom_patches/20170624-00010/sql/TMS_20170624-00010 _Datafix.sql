/***********************************************************************************************************************************************
   NAME:       TMS_20170624-00010_Datafix.sql
   PURPOSE:  Data fix

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ----------------------------------------------------------------------------------------------------
   1.0        06/24/2017  Rakesh P.        TMS#20170624-00010
************************************************************************************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000

BEGIN
   DBMS_OUTPUT.put_line ('Before Update');

   update apps.wsh_delivery_details
      set OE_INTERFACED_FLAG='Y'
    where delivery_detail_id=22596598;

    DBMS_OUTPUT.put_line ('Records Updated -' || SQL%ROWCOUNT);

   COMMIT;

   EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Unable to update record ' || SQLERRM);
	  ROLLBACK;
END;
/