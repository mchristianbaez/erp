/*************************************************************************
Copyright (c) 2012 HD Supply
All rights reserved.
**************************************************************************
   $Header Alter XXWC_B2B_CAT_STG_TBL$
  Module Name: XXWC_B2B_CAT_STG_TBL
  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)       TMS TASK             DESCRIPTION
  -- ------- -----------   ------------ -----------------      -------------------------------------
  -- 1.0     12-SEP-2016   P.vamshidhar TMS#20160603-00067 -   Kiewit Phase 2 - Upload Data to PDH
**************************************************************************/
ALTER TABLE XXWC.XXWC_B2B_CAT_STG_TBL
  ADD (Request_id NUMBER);