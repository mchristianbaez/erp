/********************************************************************************
   $Header XXWC_B2B_POD_INFO_TBL_U1.SQL $
   Module Name: XXWC_B2B_POD_INFO_TBL_U1

   PURPOSE:   This trigger is used to update WHO columns and set default values.

   REVISIONS:
   Ver        Date        Author                     Description
   ---------  ----------  ---------------         -------------------------
   1.0        01/04/2015  Gopi Damuluri           TMS# 20160120-00169 - B2B POD Enhancement
********************************************************************************/
CREATE UNIQUE INDEX XXWC.XXWC_B2B_POD_INFO_TBL_U1 ON XXWC.XXWC_B2B_POD_INFO_TBL(CUST_ACCOUNT_ID, SITE_USE_ID);