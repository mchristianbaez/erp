CREATE OR REPLACE TRIGGER APPS.XXWC_B2B_POD_INFO_TRG
/********************************************************************************
   $Header XXWC_B2B_POD_INFO_TRG.TRG $
   Module Name: XXWC_B2B_POD_INFO_TRG

   PURPOSE:   This trigger is used to update WHO columns and set default values of table XXWC_B2B_POD_INFO_TBL.

   REVISIONS:
   Ver        Date        Author                     Description
   ---------  ----------  ---------------         -------------------------
   1.0        01/04/2015  Gopi Damuluri           TMS# 20160120-00169 - B2B POD Enhancement
********************************************************************************/
   BEFORE INSERT OR UPDATE ON XXWC.XXWC_B2B_POD_INFO_TBL FOR EACH ROW 
DECLARE
   l_user_id                NUMBER;
   l_sec                    VARCHAR2 (100);
   l_cust_account_id        NUMBER;
   l_account_number         VARCHAR2 (30);
   l_account_name           VARCHAR2 (240);
   l_location               VARCHAR2 (40);

   l_prog_err               EXCEPTION;
BEGIN
   IF NVL(:NEW.LAST_UPDATED_BY, -1) != 15986 THEN  

   l_sec := 'Derive User-Id';
   BEGIN
   SELECT user_id
     INTO l_user_id
     FROM apps.fnd_user
     WHERE user_name = NVL(v('APP_USER'),USER)
       AND ROWNUM = 1;
   EXCEPTION
   WHEN OTHERS THEN
   l_sec := 'Error deriving User-Id';
   END;

   IF :NEW.creation_date IS NULL THEN
     :NEW.creation_date := SYSDATE;
     :NEW.created_by := l_user_id;
   END IF;

   :NEW.last_update_date := SYSDATE;
   :NEW.last_updated_by := l_user_id;

   IF :NEW.DELIVER_POD IS NULL THEN
     :NEW.DELIVER_POD := 'N';
   END IF;

   l_sec := 'Derive Cust_Account_Id';
   BEGIN
   SELECT cust_account_id
        , account_number
        , account_name
     INTO l_cust_account_id
        , l_account_number
        , l_account_name
     FROM apps.hz_cust_accounts_all
     WHERE Cust_Account_Id = :NEW.CUST_ACCOUNT_ID
       AND ROWNUM = 1;
   END;
   
   :NEW.CUST_ACCOUNT_ID := l_cust_account_id;
   :NEW.ACCOUNT_NUMBER  := l_account_number;
   :NEW.ACCOUNT_NAME    := l_account_name;
   
   IF :NEW.POD_LAST_SENT_DATE IS NULL THEN
     :NEW.POD_LAST_SENT_DATE := TRUNC(SYSDATE) - 1;
   END IF;
   
   IF :NEW.POD_NEXT_SEND_DATE IS NULL THEN
     :NEW.POD_NEXT_SEND_DATE := TRUNC(:NEW.START_DATE_ACTIVE);
   END IF;
   
   IF :NEW.SITE_USE_ID IS NOT NULL THEN
   l_sec := 'Derive Cust Location';
   BEGIN
   SELECT location
     INTO l_location
     FROM apps.hz_cust_site_uses_all
    WHERE site_use_id = :NEW.SITE_USE_ID
      AND site_use_code = 'SHIP_TO'
      AND ROWNUM = 1;
   EXCEPTION
   WHEN OTHERS THEN
     RAISE l_prog_err;
   END;
   
   :NEW.LOCATION    := l_location;
  END IF;
  
  END IF; -- IF NVL(:NEW.LAST_UPDATED_BY, :OLD.LAST_UPDATED_BY) != 15986 THEN

EXCEPTION
WHEN l_prog_err THEN
   raise_application_error (-20001, 'XXWC_B2B_POD_INFO_TRG Prog Err: ' || SQLERRM);
WHEN OTHERS THEN
   xxcus_error_pkg.xxcus_error_main_api (
      p_called_from         => 'XXWC_B2B_POD_INFO_TRG',
      p_calling             => l_sec,
      p_request_id          => -1,
      p_ora_error_msg       => SUBSTR (SQLERRM, 2000),
      p_error_desc          => 'Error in trigger - XXWC_B2B_POD_INFO_TRG',
      p_distribution_list   => 'WC-ITDEVALERTS-U1@HDSupply.com',
      p_module              => 'XXWC');

   raise_application_error (-20001, 'XXWC_B2B_POD_INFO_TRG: ' || SQLERRM);
END;
/