/**************************************************************************
   $Header XXWC_OM_DMS_IB_POD_DIR $
  Module Name: XXWC_OM_DMS_IB_POD_DIR

  PURPOSE: Directory to process B2B POD Files

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)        DESCRIPTION
  -- ------- -----------   --------------   -------------------------
  -- 1.0     26-AUG-2015   Gopi Damuluri    TMS# 20160120-00169 - B2B POD Enhancement
**************************************************************************/
CREATE DIRECTORY XXWC_OM_DMS_IB_POD_DIR AS '/xx_iface/ebsqa/outbound/DMS/pod/pod2cust/files';