/**************************************************************************
   $Header XXWC_B2B_POD_INFO_ID_SEQ $
  Module Name: XXWC_B2B_POD_INFO_ID_SEQ

  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)        DESCRIPTION
  -- ------- -----------   --------------   -------------------------
  -- 1.0     26-AUG-2015   Gopi Damuluri    TMS# 20160120-00169 - B2B POD Enhancement
**************************************************************************/
CREATE SEQUENCE EA_APEX.XXWC_B2B_POD_INFO_ID_SEQ START WITH 1001 INCREMENT BY 1 NOCACHE;