  /*******************************************************************************
  Table: XXWC.XXWC_B2B_POD_FILES_TBL
  Description: This table is used to maintain POD File information for B2B
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     11-Dec-2015        Gopi Damuluri   TMS# 20160120-00169 - B2B POD Enhancement
  ********************************************************************************/
 create table XXWC.XXWC_B2B_POD_FILES_TBL
 (
   file_permissions  VARCHAR2(50),
   file_type         VARCHAR2(64),
   file_owner        VARCHAR2(64),
   file_group        VARCHAR2(64),
   file_size         VARCHAR2(64),
   file_month        VARCHAR2(64),
   file_day          VARCHAR2(64),
   file_time  	      VARCHAR2(64),
   file_name         VARCHAR2(612)
 )
 organization external
 (
   type ORACLE_LOADER
   default directory XXWC_OM_DMS_IB_POD_DIR
   access parameters 
   (
     RECORDS DELIMITED BY NEWLINE
     LOAD WHEN file_permissions != 'total'
     PREPROCESSOR XXWC_OM_DMS_IB_POD_DIR: 'list_files.sh'
	 NOBADFILE
     NODISCARDFILE
     NOLOGFILE
     FIELDS TERMINATED BY WHITESPACE
   )
   location (XXWC_OM_DMS_IB_POD_DIR:'list_files_dummy_source.txt')
 )
 reject limit UNLIMITED;