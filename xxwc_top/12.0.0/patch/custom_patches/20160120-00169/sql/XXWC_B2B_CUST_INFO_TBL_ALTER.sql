  /*******************************************************************************
  Table: XXWC_B2B_CUST_INFO_TBL	
  Description: This table is used to maintain Trading Partner information for B2B Integration
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     15-Dec-2015        Gopi Damuluri   TMS# 20160120-00169 - B2B POD Enhancement
  ********************************************************************************/

ALTER TABLE XXWC.XXWC_B2B_CUST_INFO_TBL ADD (DELIVER_POD         VARCHAR2(1 BYTE),
                                             POD_EMAIL           VARCHAR2(240 BYTE),
                                             POD_FREQUENCY       VARCHAR2(50 BYTE)
                                             );
/