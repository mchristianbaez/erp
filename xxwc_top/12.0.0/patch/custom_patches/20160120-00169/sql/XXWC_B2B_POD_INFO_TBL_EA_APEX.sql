/*
-- ********************************************************************************************
-- HISTORY
-- ============================================================================================
-- ============================================================================================
-- VERSION DATE          AUTHOR(S)       DESCRIPTION
-- ------- -----------   --------------- ------------------------------------------------------
-- 1.0     04-Jan-2016   Gopi Damuluri   TMS# 20160120-00169 - B2B POD Enhancement
*/
GRANT ALL ON XXWC.XXWC_B2B_POD_INFO_TBL TO EA_APEX;
/
