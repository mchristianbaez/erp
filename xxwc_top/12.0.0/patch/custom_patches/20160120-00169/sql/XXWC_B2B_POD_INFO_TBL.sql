  /*******************************************************************************
  Table: XXWC.XXWC_B2B_POD_INFO_TBL
  Description: This table is used to maintain POD information for B2B
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     11-Dec-2015        Gopi Damuluri   TMS# 20160120-00169 - B2B POD Enhancement
  ********************************************************************************/
DROP TABLE XXWC.XXWC_B2B_POD_INFO_TBL;
/

CREATE TABLE XXWC.XXWC_B2B_POD_INFO_TBL
(
  PARTY_ID            NUMBER,
  CUST_ACCOUNT_ID     NUMBER,
  ACCOUNT_NUMBER      VARCHAR2(30 BYTE),
  ACCOUNT_NAME        VARCHAR2(240 BYTE),
  DELIVER_POD         VARCHAR2(1 BYTE),
  POD_EMAIL           VARCHAR2(240 BYTE),
  POD_FREQUENCY       VARCHAR2(50 BYTE),
  POD_LAST_SENT_DATE  DATE,
  POD_NEXT_SEND_DATE  DATE,
  CREATION_DATE       DATE,
  CREATED_BY          NUMBER,
  LAST_UPDATE_DATE    DATE,
  LAST_UPDATED_BY     NUMBER,
  ID                  NUMBER,
  LOCATION            VARCHAR2(40 BYTE),
  SITE_USE_ID         NUMBER,
  NOTIFY_ACCOUNT_MGR  VARCHAR2(1 BYTE),
  START_DATE_ACTIVE   DATE,
  END_DATE_ACTIVE     DATE
);
