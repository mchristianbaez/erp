set serveroutput on;
 /*************************************************************************
      PURPOSE:  close out orders

      REVISIONS:
      Ver        Date          Author             Description
      ---------  ------------  ------------       ------------------
      1.0        02-MAY-2018  Sundaramoorthy     Initial Version TMS #20180305-00184
	                                             Initial Version TMS #20180305-00074
                                                 Initial Version TMS #20180302-00183
                                                 Initial Version TMS #20180301-00088
												 Initial Version TMS #20180418-00063 
												 Initial Version TMS #20180424-00316
												 Initial Version TMS #20180226-00423
												 Initial Version TMS #20180319-00133 
                                                 Initial Version TMS #20180418-00063 
												 Initial Version TMS #20180423-00228 
************************************************************************************
Initial Version TMS#20180305-00184
**********************************************************************************/
BEGIN
 dbms_output.put_line ('Start Update 1st Script ');

 UPDATE oe_order_lines_all
  SET  flow_status_code ='CANCELLED'
      , open_flag ='N'
   WHERE line_id in (118010929,118010940,118011006,118010988)
   and header_id = 69952996;  
  
  dbms_output.put_line ('Update count '||SQL%ROWCOUNT);
  COMMIT;
  
  EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
	dbms_output.put_line ('Inside Exception '|| SUBSTR(SQLERRM,1,2000));	
END;
/
/*************************************************************************
Initial Version TMS #20180305-00074
Initial Version TMS #20180302-00183
Initial Version TMS #20180301-00088 
Initial Version TMS #20180226-00423
Initial Version TMS #20180424-00316 
Initial Version TMS #20180423-00228 
*****************************************************************/
BEGIN
 dbms_output.put_line ('Start Update 2nd Script ');

 UPDATE oe_order_headers_all
  SET  flow_status_code ='CANCELLED'
     , open_flag ='N'
   WHERE header_id in (69840167,68684350,68216865,69189629,70895179,70015330);  
  
  dbms_output.put_line ('Update count '||SQL%ROWCOUNT);
  COMMIT;
  
  EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
	dbms_output.put_line ('Inside Exception '|| SUBSTR(SQLERRM,1,2000));	
END;
/	
/*************************************************************************
Initial Version TMS #20180319-00133 
*****************************************************************/
BEGIN
 dbms_output.put_line ('Start Update 3rd Script ');

 UPDATE oe_order_lines_all
  SET  flow_status_code ='CLOSED'
     , open_flag ='N'
   WHERE line_id = 111061848;
    
  
  dbms_output.put_line ('Update count '||SQL%ROWCOUNT);
  COMMIT;
  
  EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
	dbms_output.put_line ('Inside Exception '|| SUBSTR(SQLERRM,1,2000));	
END;
/
/*************************************************************************
Initial Version TMS #20180418-00063 
*****************************************************************/
DECLARE

l_line_id    NUMBER := 121721389;
l_org_id     NUMBER;
l_count      NUMBER;
l_result     VARCHAR2(30);
l_file_val   VARCHAR2(60);

BEGIN

   oe_debug_pub.debug_on; 
   oe_debug_pub.initialize; 
   l_file_val := OE_DEBUG_PUB.Set_Debug_Mode('FILE'); 

   SELECT ORG_ID
   INTO   l_org_id
   FROM   OE_ORDER_LINES_ALL
   WHERE  LINE_ID = l_line_id;

   delete from oe_line_sets where line_id = l_line_id;

   UPDATE OE_ORDER_LINES_ALL
   SET    INVOICE_INTERFACE_STATUS_CODE=NULL,
              fulfilled_quantity      = ordered_quantity,
              fulfilled_flag          = 'Y',
              actual_fulfillment_date = SYSDATE,
              flow_status_code        = 'FULFILLED'
   WHERE  LINE_ID = l_line_id;

   fnd_client_info.set_org_context(to_char(l_org_id));
   DBMS_OUTPUT.PUT_LINE('Output Debug File is stored at : '||OE_DEBUG_PUB.G_DIR||'/'||OE_DEBUG_PUB.G_FILE);
   OE_DEBUG_PUB.SETDEBUGLEVEL(5);
   WF_ENGINE.HANDLEERROR('OEOL', to_char(l_line_id), 'INVOICE_INTERFACE', 'RETRY', NULL);
COMMIT;
  
  EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
	dbms_output.put_line ('Inside Exception '|| SUBSTR(SQLERRM,1,2000));

END;
/