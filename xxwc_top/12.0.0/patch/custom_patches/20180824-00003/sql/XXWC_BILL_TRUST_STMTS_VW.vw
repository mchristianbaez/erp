/*****************************************************************************************************************************
 Copyright (c) 2012 HD Supply
 All rights reserved.
*****************************************************************************************************************************
   $Header xxwc_bill_trust_stmts_vw$
  Module Name: xxwc_bill_trust_stmts_vw

  PURPOSE: Bill Trust

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)     TMS TASK              DESCRIPTION
  -- ------- -----------   ------------ ---------------      -----------------------------------------------------------------
  -- 1.0                                                       Initially Created
  -- 1.1     28-Aug-2018   P.vamshidhar  20180824-00003        Billtrust Statement defects related to AHH 
*****************************************************************************************************************************/
CREATE OR REPLACE FORCE VIEW apps.xxwc_bill_trust_stmts_vw (cust_account_id,
                                                            customer_account_number,
                                                            customer_name,
                                                            cust_payment_term,
                                                            prim_bill_to_id,
                                                            prim_bill_to_site,
                                                            prim_bill_to_address1,
                                                            prim_bill_to_address2,
                                                            prim_bill_to_city,
                                                            prim_bill_to_city_province,
                                                            prim_bill_to_zip_code,
                                                            prim_bill_to_country,
                                                            account_manager,
                                                            acct_mgr_office_number,
                                                            remit_to_address_code,
                                                            remit_to_ln1,
                                                            remit_to_ln2,
                                                            remit_to_ln3,
                                                            remit_to_ln4,
                                                            remit_to_ln5,
                                                            stmt_by_job,
                                                            send_statement_flag,
                                                            send_credit_bal_flag,
                                                            payment_schedule_id,
                                                            trx_bill_to_site,
                                                            trx_customer_id,
                                                            trx_bill_site_use_id,
                                                            trx_site_use_code,
                                                            trx_addr_type,
                                                            customer_job_number,
                                                            trx_number,
                                                            trx_date,
                                                            customer_po_number,
                                                            pmt_status,
                                                            trx_type,
                                                            amount_due_original,
                                                            amount_due_remaining,
                                                            trx_age,
                                                            current_balance,
                                                            thirty_days_bal,
                                                            sixty_days_bal,
                                                            ninety_days_bal,
                                                            over_ninety_days_bal
                                                           )
AS
   SELECT   hca.cust_account_id, hca.account_number customer_account_number,
            hp.party_name customer_name,
                                        -- 03/23/2012 CGonzalez: Added to exclude COD and Preffered Cash
                                        rtt.description cust_payment_term,            
            pb_hcsua.site_use_id prim_bill_to_id,
            pb_hcsua.LOCATION prim_bill_to_site,
            pb_hl.address1 prim_bill_to_address1,
            (pb_hl.address2 || ' ' || pb_hl.address3) prim_bill_to_address2,
            pb_hl.city prim_bill_to_city,
            NVL (pb_hl.state, pb_hl.province) prim_bill_to_city_province,
            pb_hl.postal_code prim_bill_to_zip_code,
            pb_hl.country prim_bill_to_country,
            NVL (jrs.NAME, papf.full_name) account_manager,            
            -- 03/08/12 CGonzalez: added Acct Manager Phone Number
            -- 06/22/2012 CG: Changed to pull the phone from the Collector record
            -- pp.phone_number acct_mgr_office_number,
            xxwc_mv_routines_pkg.format_phone_number
               (NVL (ac.telephone_number,                 --jrre.source_phone,
                     fnd_profile.VALUE ('XXWC_BILL_TRUST_STMTS_DEF_PHONE')
                    )
               ) acct_mgr_office_number,            
            -- 02/23/2012 CG Added remit to address lines
            --, hcp.attribute2 remit_to_address_code
            NVL (hcp.attribute2, '2') remit_to_address_code,            
            -- 02/23/2012 CG Added remit to address lines
            SUBSTR (flv.description,
                    1,
                    INSTR (flv.description, ',', 1, 1) - 1
                   ) remit_to_ln1,
            SUBSTR (flv.description,
                    INSTR (flv.description, ',', 1, 1) + 2,
                      INSTR (flv.description, ',', 1, 2)
                    - INSTR (flv.description, ',', 1, 1)
                    - 2
                   ) remit_to_ln2,
            SUBSTR (flv.description,
                    INSTR (flv.description, ',', 1, 2) + 2,
                      INSTR (flv.description, ',', 1, 3)
                    - INSTR (flv.description, ',', 1, 2)
                    - 2
                   ) remit_to_ln3,
            DECODE (INSTR (flv.description, ',', 1, 4),
                    0, SUBSTR (flv.description,
                               INSTR (flv.description, ',', 1, 3) + 2,
                                 LENGTH (flv.description)
                               - INSTR (flv.description, ',', 1, 3)
                               - 1
                              ),
                    SUBSTR (flv.description,
                            INSTR (flv.description, ',', 1, 3) + 2,
                              INSTR (flv.description, ',', 1, 4)
                            - INSTR (flv.description, ',', 1, 3)
                            - 2
                           )
                   ) remit_to_ln4,
            (CASE
                WHEN INSTR (flv.description, ',', 1, 4) = 0
                AND INSTR (flv.description, ',', 1, 5) = 0
                   THEN NULL
                ELSE SUBSTR (flv.description,
                             INSTR (flv.description, ',', 1, 4) + 2,
                               LENGTH (flv.description)
                             - INSTR (flv.description, ',', 1, 5)
                            )
             END
            ) remit_to_ln5,
            NVL (hcp.attribute3, 'N') stmt_by_job,            
            -- 03/08/2012
            NVL (hcp.send_statements, 'N') send_statement_flag,
            NVL (hcp.credit_balance_statements, 'N') send_credit_bal_flag,            
            -- 03/08/2012
            apsa.payment_schedule_id, trx_hcsua.LOCATION trx_bill_to_site,
            apsa.customer_id trx_customer_id,
            apsa.customer_site_use_id trx_bill_site_use_id,
            trx_hcsua.site_use_code trx_site_use_code,
            trx_hcsua.attribute1 trx_addr_type,            
            -- 02/23/2012 CGonzalez
            /*, (CASE WHEN NVL (trx_hcsua.attribute1, 'UNKNOWN') = 'JOB'
                        THEN (TRIM (NVL(
                                        (SUBSTR (trx_hcsua.LOCATION,(INSTR (trx_hcsua.LOCATION,'-',1,1)+ 1),LENGTH (trx_hcsua.LOCATION)))
                                        ,trx_hcsua.LOCATION
                                        )
                                   )
                             )
                    ELSE NULL
               END ) customer_job_number */
            /*(TRIM (NVL ((SUBSTR (trx_hcsua.LOCATION,
                                 (INSTR (trx_hcsua.LOCATION, '-', 1, 1) + 1
                                 ),
                                 LENGTH (trx_hcsua.LOCATION)
                                )
                        ),
                        trx_hcsua.LOCATION
                       )
                  )
            )*/
            trx_hps.party_site_number customer_job_number
                                                         --, trx_hl.address1 Trx_Bill_To_Address1
                                                         --, (trx_hl.address2||' '||pb_hl.address3) Trx_Bill_To_Address2
                                                         --, trx_hl.city Trx_Bill_To_City
                                                         --, NVL(trx_hl.state, trx_hl.province) Trx_Bill_To_City_Province
                                                         --, trx_hl.postal_code Trx_Bill_To_Zip_Code
                                                         --, trx_hl.country Trx_Bill_To_Country
            , apsa.trx_number, apsa.trx_date,
            NVL (rcta.purchase_order,
                 acra.customer_receipt_reference
                ) customer_po_number,
            acra.status pmt_status,            
            --03/08/2012 CG
            --apsa.CLASS trx_type,
            (CASE
			     WHEN rbs.name='HARRIS' and apsa.CLASS = 'INV'  AND upper(rcta.interface_header_attribute8) like '%RENTAL%' THEN
				 'REN'  -- added by Vamshi in Rev 1.1
                WHEN apsa.CLASS = 'INV'
                AND rcta.interface_header_attribute2 LIKE '%RENTAL%'
                   THEN 'REN'
                WHEN apsa.CLASS = 'CM'
                   THEN 'CRM'
                WHEN apsa.CLASS = 'DM'
                   THEN 'DBM'
                ELSE apsa.CLASS
             END
            ) trx_type,
            apsa.amount_due_original, apsa.amount_due_remaining,
            (TRUNC (SYSDATE) - TRUNC (apsa.due_date)) trx_age,
            (CASE
                WHEN (TRUNC (SYSDATE) - TRUNC (apsa.due_date)) <= 0
                   THEN apsa.amount_due_remaining
                ELSE 0
             END
            ) current_balance,
            (CASE
                WHEN (TRUNC (SYSDATE) - TRUNC (apsa.due_date)) BETWEEN 1 AND 30
                   THEN apsa.amount_due_remaining
                ELSE 0
             END
            ) thirty_days_bal,
            (CASE
                WHEN (TRUNC (SYSDATE) - TRUNC (apsa.due_date)) BETWEEN 31 AND 60
                   THEN apsa.amount_due_remaining
                ELSE 0
             END
            ) sixty_days_bal,
            (CASE
                WHEN (TRUNC (SYSDATE) - TRUNC (apsa.due_date)) BETWEEN 61 AND 90
                   THEN apsa.amount_due_remaining
                ELSE 0
             END
            ) ninety_days_bal,
            (CASE
                WHEN (TRUNC (SYSDATE) - TRUNC (apsa.due_date)) >= 91
                   THEN apsa.amount_due_remaining
                ELSE 0
             END
            ) over_ninety_days_bal
       FROM hz_cust_accounts hca,
            hz_parties hp,
            hz_cust_acct_sites_all pb_hcasa,
            hz_party_sites pb_hps,
            hz_cust_site_uses_all pb_hcsua,
            hz_locations pb_hl,
            jtf.jtf_rs_salesreps jrs,
            hr.per_all_people_f papf,
            hz_customer_profiles hcp,
            ar_payment_schedules_all apsa,
            ar.ra_customer_trx_all rcta,
            ar.ar_cash_receipts_all acra,
            hz_cust_site_uses_all trx_hcsua,
            hz_cust_acct_sites_all trx_hcasa,
            hz_party_sites trx_hps,
            --, hz_locations trx_hl
            -- 02/23/2012 Added Lookup Address Parsing
            fnd_lookup_values flv,
            -- 06/22/2012 CG: Changed to pull the phone from the Collector record
            -- per_phones pp,
            ra_terms_tl rtt,
            -- 06/22/2012 CG: Changed to pull the phone from the Collector record
            ar_collectors ac,
			ra_batch_sources rbs -- Added by Vamshi - Rev 1.1
      -- jtf_rs_resource_extns jrre
   WHERE    hca.party_id = hp.party_id
        AND hca.cust_account_id = pb_hcasa.cust_account_id
        AND pb_hcasa.party_site_id = pb_hps.party_site_id
        AND pb_hcasa.status = 'A'                                -- 05/30/2012
        AND pb_hcasa.bill_to_flag = 'P'                          -- 05/30/2012
        AND pb_hcasa.cust_acct_site_id = pb_hcsua.cust_acct_site_id
        AND pb_hcsua.site_use_code = 'BILL_TO'
        AND NVL (pb_hcsua.primary_flag, 'N') = 'Y'
        AND pb_hps.location_id = pb_hl.location_id
        AND pb_hcsua.primary_salesrep_id = jrs.salesrep_id(+)
        AND pb_hcsua.org_id = jrs.org_id(+)
        AND jrs.person_id = papf.person_id(+)
        AND TRUNC (papf.effective_start_date(+)) <= TRUNC (SYSDATE)
        AND NVL (TRUNC (papf.effective_end_date(+)), TRUNC (SYSDATE)) >=
                                                               TRUNC (SYSDATE)
        -- 03/08/2012 CG To pull employee work phone number
        -- 06/22/2012 CG: Changed to pull the phone from the Collector record
        -- AND papf.person_id = pp.parent_id(+)
        -- AND pp.parent_table(+) = 'PER_ALL_PEOPLE_F'
        -- AND pp.phone_type(+) = 'W1'
        AND hca.cust_account_id = hcp.cust_account_id
        AND hca.party_id = hcp.party_id                          -- 05/30/2012
        AND hcp.site_use_id IS NULL
        -- 03/08/2012
        --AND NVL (hcp.send_statements(+), 'N') = 'Y'
        -- 06/22/2012 CG: Changed to pull the phone from the Collector record
        AND hcp.collector_id = ac.collector_id(+)
        -- AND ac.resource_id = jrre.resource_id(+)
        -- 06/22/2012 CG: Changed to pull the phone from the Collector record
        AND hca.cust_account_id = apsa.customer_id
        AND apsa.amount_due_remaining != 0
        AND apsa.customer_trx_id = rcta.customer_trx_id(+)
        AND apsa.cash_receipt_id = acra.cash_receipt_id(+)
        AND apsa.customer_site_use_id = trx_hcsua.site_use_id
-- 02/23/2012 CG
        AND NVL (hcp.attribute2, '2') = flv.lookup_code
        AND flv.lookup_type = 'XXWC_REMIT_TO_ADDRESS_CODES'
/*        AND hca.account_number in ('7100'
                                    ,'69064000'
                                    ,'35729000'
                                    ,'16547000'
                                    ,'4861000'
                                    ,'162404'
                                    ,'156299000'
                                    )*/
        AND trx_hcsua.cust_acct_site_id = trx_hcasa.cust_acct_site_id(+)
        AND trx_hcasa.party_site_id = trx_hps.party_site_id(+)
--and       trx_hps.location_id = trx_hl.location_id
--and       trx_hcsua.attribute1 = 'JOB'
        -- 03/23/2012 CGonzalez
        AND hcp.standard_terms = rtt.term_id(+)
		and rcta.batch_source_id = rbs.batch_source_id (+) -- Added by Vamshi in Rev 1.1
   ORDER BY hp.party_name, trx_hcsua.LOCATION, apsa.trx_date;


