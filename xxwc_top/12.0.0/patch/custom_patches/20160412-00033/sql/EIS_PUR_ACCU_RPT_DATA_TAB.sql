--------------------------------------------------------------------------------------
/*******************************************************************************
  Table: EIS_PUR_ACCU_RPT_DATA_TAB
  Description: This table is used to get data from XXEIS.EIS_XXWC_PURCHASE_VEN_RPT_PKG Package.
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     12-Apr-2016        Pramod   	    --TMS#20160412-00033  Performance Tuning
********************************************************************************/
DROP TABLE XXEIS.EIS_PUR_ACCU_RPT_DATA_TAB CASCADE CONSTRAINTS;

CREATE TABLE XXEIS.EIS_PUR_ACCU_RPT_DATA_TAB
  (
    PROCESS_ID     NUMBER,
    MVID           VARCHAR2(150 BYTE),
    VENDOR         VARCHAR2(150 BYTE),
    LOB            VARCHAR2(150 BYTE),
    BU             VARCHAR2(150 BYTE),
    AGREEMENT_YEAR NUMBER,
    PURCHASES      NUMBER,
    COOP           NUMBER,
    REBATE         NUMBER,
    TOTAL_ACCRUALS NUMBER
)
/
