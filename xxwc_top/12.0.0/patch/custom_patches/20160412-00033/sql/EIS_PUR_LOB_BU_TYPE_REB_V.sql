------------------------------------------------------------------------------------
/******************************************************************************
   NAME       :  XXEIS.EIS_PUR_LOB_BU_TYPE_REB_V
   PURPOSE    :  Replaced the old view definition with new view.This view is created based on 
				 custom table XXEIS.EIS_PUR_ACCU_RPT_DATA_TAB and this custom table data
				 gets populated by XXEIS.EIS_XXWC_PURCHASE_VEN_RPT_PKG package.
   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0		 04/12/2016   Pramod  			--TMS#20160412-00033  Performance Tuning                                         
******************************************************************************/
DROP VIEW XXEIS.EIS_PUR_LOB_BU_TYPE_REB_V;

CREATE OR REPLACE VIEW XXEIS.EIS_PUR_LOB_BU_TYPE_REB_V 
AS
  SELECT PROCESS_ID,    
    MVID ,
    VENDOR ,
    LOB ,
    BU ,
    AGREEMENT_YEAR ,
    SUM(PURCHASES) PURCHASES ,
    SUM(COOP) COOP ,
    SUM(REBATE) REBATE ,
    SUM(TOTAL_ACCRUALS) TOTAL_ACCRUALS
  FROM
    (SELECT PROCESS_ID,
      MVID ,
      VENDOR ,
      LOB ,
      BU ,
      AGREEMENT_YEAR ,
      PURCHASES ,
      coop ,
      REBATE ,
      TOTAL_ACCRUALS
    FROM XXEIS.EIS_PUR_ACCU_RPT_DATA_TAB
    )
  WHERE 1=1
  GROUP BY PROCESS_ID,
    MVID ,
    VENDOR ,
    LOB ,
    BU ,
    AGREEMENT_YEAR
/
