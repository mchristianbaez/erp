--Report Name            : PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, AGREEMENT_YEAR, REBATE TYPE(NEW)
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating View Data for PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, AGREEMENT_YEAR, REBATE TYPE(NEW)
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_PUR_LOB_BU_TYPE_REB_V
xxeis.eis_rs_ins.v( 'EIS_PUR_LOB_BU_TYPE_REB_V',682,'','','','','PK059658','XXEIS','Eis Pur Lob Bu Typ V','EPLBTV','','');
--Delete View Columns for EIS_PUR_LOB_BU_TYPE_REB_V
xxeis.eis_rs_utility.delete_view_rows('EIS_PUR_LOB_BU_TYPE_REB_V',682,FALSE);
--Inserting View Columns for EIS_PUR_LOB_BU_TYPE_REB_V
xxeis.eis_rs_ins.vc( 'EIS_PUR_LOB_BU_TYPE_REB_V','TOTAL_ACCRUALS',682,'Total Accruals','TOTAL_ACCRUALS','','','','PK059658','NUMBER','','','Total Accruals','','','');
xxeis.eis_rs_ins.vc( 'EIS_PUR_LOB_BU_TYPE_REB_V','REBATE',682,'Rebate','REBATE','','','','PK059658','NUMBER','','','Rebate','','','');
xxeis.eis_rs_ins.vc( 'EIS_PUR_LOB_BU_TYPE_REB_V','COOP',682,'Coop','COOP','','','','PK059658','NUMBER','','','Coop','','','');
xxeis.eis_rs_ins.vc( 'EIS_PUR_LOB_BU_TYPE_REB_V','PURCHASES',682,'Purchases','PURCHASES','','','','PK059658','NUMBER','','','Purchases','','','');
xxeis.eis_rs_ins.vc( 'EIS_PUR_LOB_BU_TYPE_REB_V','AGREEMENT_YEAR',682,'Agreement Year','AGREEMENT_YEAR','','','','PK059658','NUMBER','','','Agreement Year','','','');
xxeis.eis_rs_ins.vc( 'EIS_PUR_LOB_BU_TYPE_REB_V','BU',682,'Bu','BU','','','','PK059658','VARCHAR2','','','Bu','','','');
xxeis.eis_rs_ins.vc( 'EIS_PUR_LOB_BU_TYPE_REB_V','LOB',682,'Lob','LOB','','','','PK059658','VARCHAR2','','','Lob','','','');
xxeis.eis_rs_ins.vc( 'EIS_PUR_LOB_BU_TYPE_REB_V','VENDOR',682,'Vendor','VENDOR','','','','PK059658','VARCHAR2','','','Vendor','','','');
xxeis.eis_rs_ins.vc( 'EIS_PUR_LOB_BU_TYPE_REB_V','MVID',682,'Mvid','MVID','','','','PK059658','VARCHAR2','','','Mvid','','','');
xxeis.eis_rs_ins.vc( 'EIS_PUR_LOB_BU_TYPE_REB_V','PROCESS_ID',682,'Process Id','PROCESS_ID','','','','PK059658','NUMBER','','','Process Id','','','');
--Inserting View Components for EIS_PUR_LOB_BU_TYPE_REB_V
--Inserting View Component Joins for EIS_PUR_LOB_BU_TYPE_REB_V
END;
/
set scan on define on
prompt Creating Report LOV Data for PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, AGREEMENT_YEAR, REBATE TYPE(NEW)
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, AGREEMENT_YEAR, REBATE TYPE(NEW)
xxeis.eis_rs_ins.lov( 682,'select distinct nvl(attribute7, ''NULL'') agreement_year
from apps.QP_LIST_HEADERS_VL','','LOV AGREEMENT_YEAR','LOV attribute7 from apps.QP_LIST_HEADERS_VL  table is the agreement year','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 682,'select distinct party_name
from ar.HZ_PARTIES
where ATTRIBUTE1 = ''HDS_MVID''','','LOV VENDOR_NAME','VENDOR NAME FROM ar.HZ_PARTIES
','ID020048',NULL,'N','','');
xxeis.eis_rs_ins.lov( 682,'select distinct party_name
from ar.HZ_PARTIES
where ATTRIBUTE1 = ''HDS_LOB''
and party_name not in (''PLUMBING null'', ''INDUSTRIAL PVF'')','','HDS LOB_NAME','LOV party_name From ar.HZ_PARTIES is the LOB','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 682,'SELECT hp.party_name LOB
 FROM apps.hz_parties hp
WHERE  hp.attribute1=''HDS_BU''','','HDS BU','BU FROM apps.hz_parties it''s party_name = LOB BUSINESS UNIT','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 682,'SELECT customer_attribute2 MVID
  FROM XXCUS.xxcus_rebate_customers
  WHERE 1=1
  AND party_attribute1=''HDS_MVID''
  order by 1','','MVID','MVID number from XXCUS.XXCUS_REBATE_CUSTOMERS','DV003828',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, AGREEMENT_YEAR, REBATE TYPE(NEW)
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, AGREEMENT_YEAR, REBATE TYPE(NEW)
xxeis.eis_rs_utility.delete_report_rows( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, AGREEMENT_YEAR, REBATE TYPE(NEW)' );
--Inserting Report - PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, AGREEMENT_YEAR, REBATE TYPE(NEW)
xxeis.eis_rs_ins.r( 682,'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, AGREEMENT_YEAR, REBATE TYPE(NEW)','','PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, AGREEMENT_YEAR, REBATE TYPE(NEW)','','','','PK059658','EIS_PUR_LOB_BU_TYPE_REB_V','Y','','','PK059658','','N','ADHOC - SUMMARY ACCRUAL DETAILS','RTF,PDF,','CSV,HTML,Html Summary,XML,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, AGREEMENT_YEAR, REBATE TYPE(NEW)
xxeis.eis_rs_ins.rc( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, AGREEMENT_YEAR, REBATE TYPE(NEW)',682,'AGREEMENT_YEAR','Agreement Year','Agreement Year','','~~~','default','','5','N','','','','','','','','PK059658','N','N','','EIS_PUR_LOB_BU_TYPE_REB_V','','');
xxeis.eis_rs_ins.rc( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, AGREEMENT_YEAR, REBATE TYPE(NEW)',682,'BU','Bu','Bu','','','default','','4','N','','','','','','','','PK059658','N','N','','EIS_PUR_LOB_BU_TYPE_REB_V','','');
xxeis.eis_rs_ins.rc( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, AGREEMENT_YEAR, REBATE TYPE(NEW)',682,'COOP','Coop','Coop','','~,~.~2','default','','8','N','','','','','','','','PK059658','N','N','','EIS_PUR_LOB_BU_TYPE_REB_V','','');
xxeis.eis_rs_ins.rc( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, AGREEMENT_YEAR, REBATE TYPE(NEW)',682,'LOB','Lob','Lob','','','default','','3','N','','','','','','','','PK059658','N','N','','EIS_PUR_LOB_BU_TYPE_REB_V','','');
xxeis.eis_rs_ins.rc( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, AGREEMENT_YEAR, REBATE TYPE(NEW)',682,'MVID','Mvid','Mvid','','','default','','1','N','','','','','','','','PK059658','N','N','','EIS_PUR_LOB_BU_TYPE_REB_V','','');
xxeis.eis_rs_ins.rc( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, AGREEMENT_YEAR, REBATE TYPE(NEW)',682,'PURCHASES','Purchases','Purchases','','~,~.~2','default','','6','N','','','','','','','','PK059658','N','N','','EIS_PUR_LOB_BU_TYPE_REB_V','','');
xxeis.eis_rs_ins.rc( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, AGREEMENT_YEAR, REBATE TYPE(NEW)',682,'REBATE','Rebate','Rebate','','~,~.~2','default','','7','N','','','','','','','','PK059658','N','N','','EIS_PUR_LOB_BU_TYPE_REB_V','','');
xxeis.eis_rs_ins.rc( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, AGREEMENT_YEAR, REBATE TYPE(NEW)',682,'TOTAL_ACCRUALS','Total Accruals','Total Accruals','','~,~.~2','default','','9','N','','','','','','','','PK059658','N','N','','EIS_PUR_LOB_BU_TYPE_REB_V','','');
xxeis.eis_rs_ins.rc( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, AGREEMENT_YEAR, REBATE TYPE(NEW)',682,'VENDOR','Vendor','Vendor','','','default','','2','N','','','','','','','','PK059658','N','N','','EIS_PUR_LOB_BU_TYPE_REB_V','','');
--Inserting Report Parameters - PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, AGREEMENT_YEAR, REBATE TYPE(NEW)
xxeis.eis_rs_ins.rp( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, AGREEMENT_YEAR, REBATE TYPE(NEW)',682,'Mvid','','','IN','MVID','','VARCHAR2','N','Y','3','','N','CONSTANT','PK059658','Y','N','','','');
xxeis.eis_rs_ins.rp( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, AGREEMENT_YEAR, REBATE TYPE(NEW)',682,'Bu','','','IN','HDS BU','','VARCHAR2','N','Y','2','','N','CONSTANT','PK059658','Y','N','','','');
xxeis.eis_rs_ins.rp( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, AGREEMENT_YEAR, REBATE TYPE(NEW)',682,'Agreement Year','','','IN','LOV AGREEMENT_YEAR','','VARCHAR2','Y','Y','1','','N','CONSTANT','PK059658','Y','N','','','');
xxeis.eis_rs_ins.rp( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, AGREEMENT_YEAR, REBATE TYPE(NEW)',682,'Lob','','','IN','HDS LOB_NAME','','VARCHAR2','N','Y','4','','N','CONSTANT','PK059658','Y','N','','','');
xxeis.eis_rs_ins.rp( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, AGREEMENT_YEAR, REBATE TYPE(NEW)',682,'Vendor','','','IN','LOV VENDOR_NAME','','VARCHAR2','N','Y','5','','N','CONSTANT','PK059658','Y','N','','','');
--Inserting Report Conditions - PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, AGREEMENT_YEAR, REBATE TYPE(NEW)
xxeis.eis_rs_ins.rcn( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, AGREEMENT_YEAR, REBATE TYPE(NEW)',682,'','','','','and PROCESS_ID = :SYSTEM.PROCESS_ID','Y','1','','PK059658');
--Inserting Report Sorts - PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, AGREEMENT_YEAR, REBATE TYPE(NEW)
--Inserting Report Triggers - PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, AGREEMENT_YEAR, REBATE TYPE(NEW)
xxeis.eis_rs_ins.rt( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, AGREEMENT_YEAR, REBATE TYPE(NEW)',682,'BEGIN
XXEIS.EIS_XXWC_PURCHASE_VEN_RPT_PKG.GET_ACC_PUR_DTLS (p_process_id  =>  :SYSTEM.PROCESS_ID,
                             p_Mvid => :Mvid,
                            p_Bu => :Bu,
                            p_Agreement_Year =>	:Agreement Year,
                            P_LOB 	 =>	:Lob,
                            P_VENDOR   =>	:Vendor
	);
END;','B','Y','PK059658');
xxeis.eis_rs_ins.rt( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, AGREEMENT_YEAR, REBATE TYPE(NEW)',682,'begin
XXEIS.EIS_XXWC_PURCHASE_VEN_RPT_PKG.clear_temp_tables(P_PROCESS_ID=> :SYSTEM.PROCESS_ID);
end;','A','Y','PK059658');
--Inserting Report Templates - PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, AGREEMENT_YEAR, REBATE TYPE(NEW)
--Inserting Report Portals - PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, AGREEMENT_YEAR, REBATE TYPE(NEW)
--Inserting Report Dashboards - PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, AGREEMENT_YEAR, REBATE TYPE(NEW)
--Inserting Report Security - PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, AGREEMENT_YEAR, REBATE TYPE(NEW)
--Inserting Report Pivots - PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, AGREEMENT_YEAR, REBATE TYPE(NEW)
END;
/
set scan on define on
