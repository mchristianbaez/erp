 /********************************************************************************
  FILE NAME: "XXWC"."XXWC_ONHAND_BAL_EXT_TBL" 

  PROGRAM TYPE: External Table script

  PURPOSE: Conversion purpose for AH Harries

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     05/16/2018    Nancy Pahwa  TMS#20180308-00301  -- Initial Version
  1.1     07/16/2018    Naveen K     TMS# 20180716-00087
  ********************************************************************************/
CREATE TABLE "XXWC"."XXWC_ONHAND_BAL_EXT_TBL" 
   (	"ITEM_SEGMENT" VARCHAR2(40), 
	"ORGANIZATION_CODE" VARCHAR2(10), 
	"TRANSACTION_COST" VARCHAR2(20), 
	"HOW_PRICED" VARCHAR2(1), 
	"TRANSACTION_DATE" VARCHAR2(40), 
	"TRANSACTION_QUANTITY" VARCHAR2(40)
   ) 
   ORGANIZATION EXTERNAL 
    ( TYPE ORACLE_LOADER
      DEFAULT DIRECTORY "XXWC_AR_AHH_CASH_RCPT_CONV_DIR"
      ACCESS PARAMETERS
      ( RECORDS DELIMITED BY NEWLINE CHARACTERSET WE8MSWIN1252
      SKIP 1
    BADFILE 'XXWC_ONHAND_BAL_CNV.bad'
    DISCARDFILE 'XXWC_ONHAND_BAL_CNV.dsc'
    FIELDS TERMINATED BY '|'
    OPTIONALLY ENCLOSED BY '"' AND '"'
    REJECT ROWS WITH ALL NULL FIELDS
          )
      LOCATION
       ( "XXWC_AR_AHH_CASH_RCPT_CONV_DIR":'XXWC_ONHAND_BAL_CNV.csv'
       )
    )
   REJECT LIMIT UNLIMITED ;