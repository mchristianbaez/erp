 /********************************************************************************
  FILE NAME: XXWC.XXWC_AHH_INV_CONV_GRANTS_TO_APEX_USER.sql

  PROGRAM TYPE: Grant scripts

  PURPOSE: INV Conversion purpose in APEX application

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     05/09/2018    Naveen K  		TMS#20180713-00073  -- Initial Version
  ********************************************************************************/
  GRANT ALTER,SELECT,INSERT,UPDATE,DELETE ON "XXWC"."XXWC_INVITEM_STG" TO EA_APEX
/
  GRANT SELECT ON "XXWC"."XXWC_INVITEM_STG_EXT_TBL" TO EA_APEX
/
  GRANT ALTER,SELECT,INSERT,UPDATE,DELETE ON "XXWC"."XXWC_ONHAND_BAL_CNV" TO EA_APEX
/
  GRANT SELECT ON "XXWC"."XXWC_ONHAND_BAL_EXT_TBL" TO EA_APEX
/
