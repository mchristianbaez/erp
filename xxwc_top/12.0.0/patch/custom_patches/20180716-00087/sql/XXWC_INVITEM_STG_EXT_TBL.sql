 /********************************************************************************
  FILE NAME: XXWC.XXWC_INVITEM_STG_EXT_TBL

  PROGRAM TYPE: External Table script

  PURPOSE: Conversion purpose for AH Harries

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     05/16/2018    Nancy Pahwa  TMS#20180308-00301  -- Initial Version
  ********************************************************************************/
CREATE TABLE "XXWC"."XXWC_INVITEM_STG_EXT_TBL" 
   (	"AHH_PARTNUMBER" VARCHAR2(500), 
	"DESCRIPTION" VARCHAR2(240), 
	"EXTENDEDDESCRIPTION" VARCHAR2(2000), 
	"MST_ITEM_STATUS_CODE" VARCHAR2(30), 
	"COMPANY_NUM" VARCHAR2(20), 
	"ORG_ITEM_STATUS_CODE" VARCHAR2(30), 
	"SHELF_LIFE_CODE" VARCHAR2(50), 
	"VENDOR_OWNER_NUM" VARCHAR2(30), 
	"VENDOR_NAME" VARCHAR2(200), 
	"VENDORPARTNUMBER" VARCHAR2(50), 
	"FIXED_LOT" VARCHAR2(100), 
	"UNITOFMEASURE" VARCHAR2(10), 
	"PRODUCT_LINE" VARCHAR2(200), 
	"LISTPRICE" VARCHAR2(100), 
	"BLANK1" VARCHAR2(200), 
	"UNITLENGTH" VARCHAR2(100), 
	"UNITHEIGHT" VARCHAR2(100), 
	"UNITWIDTH" VARCHAR2(100), 
	"UNITWEIGHT" VARCHAR2(100), 
	"PRODUCT_CATEGORY" VARCHAR2(200), 
	"ORGANIZATION_CODE" VARCHAR2(20), 
	"ORG_ITEM_STATUS_CODE1" VARCHAR2(30), 
	"SOURCING_FROM" VARCHAR2(150), 
	"INV_ORG" VARCHAR2(150), 
	"BLANK2" VARCHAR2(200), 
	"ORG_FIXED_LOT" VARCHAR2(100), 
	"VELOCITYCLASSIFICATION" VARCHAR2(10), 
	"INVENTORY_PLANNING_CODE" VARCHAR2(150), 
	"THRESHOLD" VARCHAR2(150), 
	"MINMAXMIN" VARCHAR2(100), 
	"MINMAXMAX" VARCHAR2(100), 
	"BLANK3" VARCHAR2(200), 
	"SOURCE_ORG_VENDOR_NUM" VARCHAR2(200), 
	"BLANK4" VARCHAR2(200), 
	"BLANK5" VARCHAR2(200), 
	"USAGE" VARCHAR2(200), 
	"LEADTIME" VARCHAR2(100), 
	"REVIEWTIME" VARCHAR2(100), 
	"BLANK6" VARCHAR2(200), 
	"BUYER" VARCHAR2(50), 
	"BLANK7" VARCHAR2(200), 
	"BLANK8" VARCHAR2(200), 
	"BLANK9" VARCHAR2(200), 
	"BLANK10" VARCHAR2(200), 
	"BLANK11" VARCHAR2(200), 
	"BLANK12" VARCHAR2(200), 
	"BLANK13" VARCHAR2(200), 
	"BLANK14" VARCHAR2(200), 
	"BLANK15" VARCHAR2(200), 
	"BLANK16" VARCHAR2(200), 
	"BLANK17" VARCHAR2(200), 
	"BLANK18" VARCHAR2(200), 
	"BLANK19" VARCHAR2(200), 
	"BLANK20" VARCHAR2(200), 
	"BLANK21" VARCHAR2(200), 
	"CATEGORY" VARCHAR2(50), 
	"BLANK22" VARCHAR2(200), 
	"NONTAXTY" VARCHAR2(200), 
	"TAXABLETY" VARCHAR2(200), 
	"BLANK23" VARCHAR2(200), 
	"TAXGROUP" VARCHAR2(200), 
	"BLANK24" VARCHAR2(200), 
	"TAXTYPE" VARCHAR2(200), 
	"ITEMLOCATION1" VARCHAR2(50), 
	"ITEMLOCATION2" VARCHAR2(50), 
	"ITEMLOCATION3" VARCHAR2(50), 
	"ITEMLOCATION4" VARCHAR2(200), 
	"ITEMLOCATION5" VARCHAR2(200), 
	"QTY_ONHAND" VARCHAR2(200), 
	"QTY_UNAVAILABLE" VARCHAR2(200), 
	"AVGCOST" VARCHAR2(200), 
	"VAL_FROM_ICRTT" VARCHAR2(200), 
	"SALESVELOCITYYEARLYSTORECOUNT" VARCHAR2(150), 
	"DOTCODE" VARCHAR2(200), 
	"SHIPPINGNM" VARCHAR2(200), 
	"FREIGHTCLASS" VARCHAR2(200), 
	"HMCOLUMNENTRY" VARCHAR2(200), 
	"UNNUMBER" VARCHAR2(50), 
	"ERGNO" VARCHAR2(200), 
	"TECHEMNM" VARCHAR2(200), 
	"ATTRIBUTE17" VARCHAR2(240), 
	"HAZARDYN" VARCHAR2(5), 
	"HAZARDCLASS" VARCHAR2(50), 
	"ATTRIBUTE9" VARCHAR2(30), 
	"NMFCCODE" VARCHAR2(200), 
	"ENTERDT" VARCHAR2(100),
	"ICSW_MINTHRESEXDT" VARCHAR2(100)
   ) 
   ORGANIZATION EXTERNAL 
    ( TYPE ORACLE_LOADER
      DEFAULT DIRECTORY "XXWC_AR_AHH_CASH_RCPT_CONV_DIR"
      ACCESS PARAMETERS
      ( RECORDS DELIMITED BY NEWLINE CHARACTERSET WE8MSWIN1252
      SKIP 1
    BADFILE 'XXWC_INVITEM_STG.bad'
    DISCARDFILE 'XXWC_INVITEM_STG.dsc'
    FIELDS TERMINATED BY '|'
    OPTIONALLY ENCLOSED BY '"' AND '"'
    REJECT ROWS WITH ALL NULL FIELDS
                        )
      LOCATION
       ( "XXWC_AR_AHH_CASH_RCPT_CONV_DIR":'XXWC_INVITEM_STG.csv'
       )
    )
   REJECT LIMIT UNLIMITED ;
   GRANT ALL ON XXWC.XXWC_INVITEM_STG_EXT_TBL TO EA_APEX;