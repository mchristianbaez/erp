/*************************************************************************
Copyright (c) 2012 HD Supply
All rights reserved.
**************************************************************************
   $Header XXWC_MTL_FORECAST_PARAM_SEQ $
  Module Name: XXWC_MTL_FORECAST_PARAM_SEQ
  PURPOSE:  To generate sno in XXWC_MTL_FORECAST_PARAM_TBL
  -- VERSION DATE          AUTHOR(S)           DESCRIPTION
  -- ------- -----------   ------------      -----------------------------------------
  -- 1.0     15-JUN-2017   P.Vamshidhar       TMS#20170228-00077 - Performance issue with XXWC MTL Generate Demand Forecast Program
************************************************************************************************/
CREATE SEQUENCE APPS.XXWC_MTL_FORECAST_PARAM_SEQ
  START WITH 1
  MAXVALUE 9999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  CACHE 20
  NOORDER;
/