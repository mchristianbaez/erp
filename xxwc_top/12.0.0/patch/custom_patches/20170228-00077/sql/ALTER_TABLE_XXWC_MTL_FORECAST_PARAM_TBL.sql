 /****************************************************************************************************
  Table: ALTER_TABLE_XXWC_MTL_FORECAST_PARAM_TBL
  Description: Alter table XXWC_MTL_FORECAST_PARAM_TBL adding sno column 
  HISTORY
  =====================================================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- ----------------------------------------------------------
  -- 1.0     15-JUN-2017   P.Vamshidhar       TMS#20170228-00077 - Performance issue with XXWC MTL Generate Demand Forecast Program
  *****************************************************************************************************/
  ALTER TABLE XXWC.XXWC_MTL_FORECAST_PARAM_TBL ADD (SNO NUMBER);  
/