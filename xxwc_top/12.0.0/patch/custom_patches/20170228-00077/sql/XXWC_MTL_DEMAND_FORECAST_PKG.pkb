CREATE OR REPLACE PACKAGE BODY APPS.XXWC_MTL_DEMAND_FORECAST_PKG
/********************************************************************************************************************************************
$Header XXWC_MTL_DEMAND_FORECAST_PKG.pkb $
Module Name: XXWC_MTL_DEMAND_FORECAST_PKG
PURPOSE: Generate Demand Forecast
REVISIONS:
Ver        Date         Author                Description
---------  -----------  ------------------    ---------------------------------------------------------------------------------------------
1.0        31-Jul-2015  Manjula Chellappan    Initial Version TMS # 20150701-00047 Generate Demand Forecast Performance Tuning and Redesign
1.1        26-Mar-2017  P.Vamshidhar          TMS#20170228-00077 - Performance issue with XXWC MTL Generate Demand Forecast Program
*******************************************************************************************************************************************/
IS
   g_start        NUMBER;
   g_dflt_email   fnd_user.email_address%TYPE
                     := 'HDSOracleDevelopers@hdsupply.com';
   g_sec          VARCHAR2 (200);
   g_errbuf       VARCHAR2 (2000);
   g_line_open    VARCHAR2 (100)
      := '/*************************************************************************';
   g_line_close   VARCHAR2 (100)
      := '**************************************************************************/';	  
  

   PROCEDURE write_log (p_log_msg VARCHAR2)
   /*************************************************************************
   $Header XXWC_MTL_DEMAND_FORECAST_PKG.write_log $
   Module Name: XXWC_MTL_DEMAND_FORECAST_PKG
   PURPOSE: Write Log for Generate Demand Forecast
   REVISIONS:
   Ver        Date         Author                Description
   ---------  -----------  ------------------    ----------------
   1.0        31-Jul-2015  Manjula Chellappan    Initial Version TMS # 20150701-00047 Generate Demand Forecast Performance Tuning and Redesign
   **************************************************************************/
   IS
      l_log_msg   VARCHAR2 (2000);
   BEGIN
      IF p_log_msg = g_line_open OR p_log_msg = g_line_close
      THEN
         l_log_msg := p_log_msg;
      ELSE
         l_log_msg :=
            p_log_msg || ' : ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS');
         xxwc_common_tunning_helpers.elapsed_time ('FORECAST',
                                                   p_log_msg,
                                                   g_start);
         COMMIT;
      END IF;

      fnd_file.put_line (fnd_file.LOG, l_log_msg);
   END write_log;


   PROCEDURE drop_temp_table (p_owner VARCHAR2, p_table_name VARCHAR2)
   /*************************************************************************
    $Header XXWC_MTL_DEMAND_FORECAST_PKG.drop_temp_table $
    Module Name: XXWC_MTL_DEMAND_FORECAST_PKG
    PURPOSE: Write Log for Generate Demand Forecast
    REVISIONS:
    Ver        Date         Author                Description
    ---------  -----------  ------------------    ----------------
    1.0        31-Jul-2015  Manjula Chellappan    Initial Version TMS # 20150701-00047 Generate Demand Forecast Performance Tuning and Redesign
    **************************************************************************/
   IS
      l_table_count       NUMBER := 0;
      l_drop_stmt         VARCHAR2 (2000);
      l_table_full_name   VARCHAR2 (60) := p_owner || '.' || p_table_name;
      l_success_msg       VARCHAR2 (200)
                             := l_table_full_name || ' Table Dropped ';
      l_error_msg         VARCHAR (200)
                             := l_table_full_name || ' Table Drop failed ';
   BEGIN
      g_sec := 'DROP TABLE ' || p_owner || '.' || p_table_name;


      SELECT COUNT (*)
        INTO l_table_count
        FROM all_tables
       WHERE     table_name = UPPER (p_table_name)
             AND owner = UPPER (p_owner)
             AND UPPER (p_owner) LIKE 'XX%';

      IF l_table_count > 0
      THEN
         l_drop_stmt := 'DROP TABLE ' || p_owner || '.' || p_table_name;

         EXECUTE IMMEDIATE l_drop_stmt;

         write_log (l_success_msg);
      ELSE
         write_log (l_table_full_name || ' Do not exist ');
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         g_errbuf := l_error_msg || CHR (10) || SQLERRM;
         write_log (g_errbuf);
   END;



   PROCEDURE cleanup_log
   /*************************************************************************
   $Header XXWC_MTL_DEMAND_FORECAST_PKG.cleanup_log $
   Module Name: XXWC_MTL_DEMAND_FORECAST_PKG
   PURPOSE: cleanup Log for Generate Demand Forecast
   REVISIONS:
   Ver        Date         Author                Description
   ---------  -----------  ------------------    ----------------
   1.0        31-Jul-2015  Manjula Chellappan    Initial Version TMS # 20150701-00047 Generate Demand Forecast Performance Tuning and Redesign
   **************************************************************************/
   IS
      l_cleanup_before   NUMBER := 100;
   BEGIN
      DELETE FROM xxwc.xxwc_interfaces##log
            WHERE     interface_name = 'FORECAST'
                  AND TRUNC (insert_date) <
                         TRUNC (SYSDATE) - l_cleanup_before;

      write_log ('xxwc.xxwc_interfaces##log cleanup completed');
   EXCEPTION
      WHEN OTHERS
      THEN
         write_log (
               'xxwc.xxwc_interfaces##log cleanup failed '
            || SUBSTR (SQLERRM, 1, 1900));
   END cleanup_log;



   FUNCTION Get_History_Start_Date (p_calendar_code   IN VARCHAR2,
                                    p_start_date      IN DATE,
                                    p_bucket_type     IN NUMBER,
                                    p_prev_periods    IN NUMBER)
      RETURN DATE
   /*************************************************************************
   $Header XXWC_MTL_DEMAND_FORECAST_PKG.Get_History_Start_Date $
   Module Name: XXWC_MTL_DEMAND_FORECAST_PKG
   PURPOSE: Write Log for Generate Demand Forecast
   REVISIONS:
   Ver        Date         Author                Description
   ---------  -----------  ------------------    ----------------
   1.0        31-Jul-2015  Manjula Chellappan    Initial Version TMS # 20150701-00047 Generate Demand Forecast Performance Tuning and Redesign
   **************************************************************************/
   IS
      -- use 3 selects based upon bucket type in single cursor
      CURSOR C_Dates
      IS
         SELECT period_start_date
           FROM BOM_PERIOD_START_DATES
          WHERE     calendar_code = p_calendar_code
                AND period_start_date <= p_start_date
                AND p_bucket_type = 3
         UNION
         SELECT week_start_date
           FROM BOM_CAL_WEEK_START_DATES
          WHERE     calendar_code = p_calendar_code
                AND week_start_date <= p_start_date
                AND p_bucket_type = 2
         UNION
         SELECT calendar_date
           FROM BOM_CALENDAR_DATES
          WHERE     calendar_code = p_calendar_code
                AND calendar_date <= p_start_date
                AND p_bucket_type = 1
         ORDER BY 1 DESC;

      l_hist_date   DATE := p_start_date; -- default starting history date to p_start_date
      l_date        DATE;
   BEGIN
      -- open cursor based upon bucket type
      OPEN C_Dates;

      -- for the necessary number of iterations (skip current bucket date range), loop thru fetched records
      FOR indx IN 1 .. p_prev_periods + 1
      LOOP
         FETCH C_Dates INTO l_date;

         -- if number of previous buckets exceeds number available, return earliest date found
         EXIT WHEN C_Dates%NOTFOUND;
         -- assign fetched date into return variable after successful fetch
         l_hist_date := l_date;
      END LOOP;

      CLOSE C_Dates;

      RETURN l_hist_date;
   END Get_History_Start_Date;

   PROCEDURE load_forecast_items (p_retcode OUT NUMBER)
   /*************************************************************************
   $Header XXWC_MTL_DEMAND_FORECAST_PKG.Load_forecast_items $
   Module Name: XXWC_MTL_DEMAND_FORECAST_PKG
   PURPOSE: Local Procedure to drop temporary table
   REVISIONS:
   Ver        Date         Author                Description
   ---------  -----------  ------------------    ----------------
   1.0        31-Jul-2015  Manjula Chellappan    Initial Version TMS # 20150701-00047 Generate Demand Forecast Performance Tuning and Redesign
   **************************************************************************/
   IS
      l_retcode     NUMBER := 0;
      l_error_msg   VARCHAR2 (200);
   BEGIN
      drop_temp_table ('XXWC', 'XXWC_MTL_FORECAST_ITEMS_TBL##');

      g_sec := 'Process 1 : Load Forecast Items - Create ';
      l_error_msg := 'Failed to Load Forecast Items - Create ';

      EXECUTE IMMEDIATE
         ' CREATE TABLE XXWC.XXWC_MTL_FORECAST_ITEMS_TBL## AS 	
         SELECT msi.inventory_item_id,
                msi.segment1 item_number,
                mp.forecast_designator,
                msi.organization_id,
                mp.organization_id source_organization_id,
                0 total_demand,
                0 avg_demand,
                0 amu,
                0 final_demand,
                0 final_avg_demand,
                mp.num_of_forecast_periods,
                mp.num_of_previous_periods,
                mp.seasonality_1 season_factor_1,
                0 avg_demand_1,
                mp.seasonality_2 season_factor_2,
                0 avg_demand_2,
                mp.seasonality_3 season_factor_3,
                0 avg_demand_3, 
                mp.seasonality_4 season_factor_4, 
                0 avg_demand_4,
                mp.seasonality_5 season_factor_5,
                0 avg_demand_5, 
                mp.seasonality_6 season_factor_6,
                0 avg_demand_6,
                mp.seasonality_7 season_factor_7,
                0 avg_demand_7,
                mp.seasonality_8 season_factor_8,
                0 avg_demand_8,
                mp.seasonality_9 season_factor_9,
                0 avg_demand_9,
                mp.seasonality_10 season_factor_10,
                0 avg_demand_10,
                mp.seasonality_11 season_factor_11,
                0 avg_demand_11,
                mp.seasonality_12 season_factor_12,
                0 avg_demand_12,
                mp.bucket_type
           FROM mtl_system_items_b msi,
                (SELECT a.category_id,
                        a.inventory_item_id,
                        a.organization_id,
                        a.category_set_id
                   FROM mtl_item_categories a,
                        xxwc.xxwc_mtl_forecast_param_tbl b
                  WHERE     a.organization_id = b.organization_id
                        AND a.category_id = b.category_id
                        AND category_set_id = 1100000062) mic,
                xxwc.xxwc_mtl_forecast_param_tbl mp
          WHERE     msi.source_organization_id = mp.organization_id
                AND mp.branch_dc_mode = ''DC''
                AND msi.source_organization_id <> msi.organization_id
                AND msi.inventory_item_id =
                       NVL (mp.inventory_item_id, msi.inventory_item_id)
                AND msi.inventory_item_id = mic.inventory_item_id(+)
                AND NVL (msi.source_organization_id, msi.organization_id) =
                       mic.organization_id(+)
                AND ( mp.item_range = ''BRANCH'' OR 
				    ( mp.item_range = ''FORECAST'' AND EXISTS
				       (SELECT 1 FROM mrp_forecast_items mfi
				         where mfi.organization_id = mp.organization_id
				           AND mfi.inventory_item_id = msi.organization_id
				           AND mfi.forecast_designator = mp.forecast_designator)) )
         UNION
         SELECT msi.inventory_item_id,
                msi.segment1 item_number,
                mp.forecast_designator,
                msi.organization_id,
                mp.organization_id source_organization_id,
                0 total_demand,
                0 avg_demand,
                0 amu,
                0 final_demand,
                0 final_avg_demand,
                mp.num_of_forecast_periods,
                mp.num_of_previous_periods,
                mp.seasonality_1 season_factor_1,
                0 avg_demand_1,
                mp.seasonality_2 season_factor_2,
                0 avg_demand_2,
                mp.seasonality_3 season_factor_3,
                0 avg_demand_3, 
                mp.seasonality_4 season_factor_4, 
                0 avg_demand_4,
                mp.seasonality_5 season_factor_5,
                0 avg_demand_5, 
                mp.seasonality_6 season_factor_6,
                0 avg_demand_6,
                mp.seasonality_7 season_factor_7,
                0 avg_demand_7,
                mp.seasonality_8 season_factor_8,
                0 avg_demand_8,
                mp.seasonality_9 season_factor_9,
                0 avg_demand_9,
                mp.seasonality_10 season_factor_10,
                0 avg_demand_10,
                mp.seasonality_11 season_factor_11,
                0 avg_demand_11,
                mp.seasonality_12 season_factor_12,
                0 avg_demand_12,
                mp.bucket_type
           FROM mtl_system_items_b msi,
                (SELECT a.category_id,
                        a.inventory_item_id,
                        a.organization_id,
                        a.category_set_id
                   FROM mtl_item_categories a,
                        xxwc.xxwc_mtl_forecast_param_tbl b
                  WHERE     a.organization_id = b.organization_id
                        AND a.category_id = b.category_id
                        AND category_set_id = 1100000062) mic,
                xxwc.xxwc_mtl_forecast_param_tbl mp
          WHERE     msi.organization_id = mp.organization_id
                AND msi.inventory_item_id =
                       NVL (mp.inventory_item_id, msi.inventory_item_id)
                AND msi.inventory_item_id = mic.inventory_item_id(+)
                AND NVL (msi.source_organization_id, msi.organization_id) =
                       mic.organization_id(+)
                AND ( mp.item_range = ''BRANCH'' OR 
				    ( mp.item_range = ''FORECAST'' AND EXISTS
				       (SELECT 1 FROM mrp_forecast_items mfi
				         where mfi.organization_id = mp.organization_id
				           AND mfi.inventory_item_id = msi.organization_id
				           AND mfi.forecast_designator = mp.forecast_designator)) )';

      write_log (g_sec);


      g_sec := 'Process 1 : Load Forecast Items - Insert ';
      l_error_msg := 'Failed to Load Forecast Items - Insert';


      EXECUTE IMMEDIATE 'TRUNCATE TABLE XXWC.XXWC_MTL_FORECAST_ITEMS_TBL';

      EXECUTE IMMEDIATE 'INSERT /*+ append */
            INTO  XXWC.XXWC_MTL_FORECAST_ITEMS_TBL
			SELECT * FROM XXWC.XXWC_MTL_FORECAST_ITEMS_TBL## ';

      write_log (SQL%ROWCOUNT || ' Forecast items Loaded');
      p_retcode := l_retcode;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         l_retcode := 2;
         p_retcode := l_retcode;
         g_errbuf := l_error_msg || CHR (10) || SQLERRM;
         write_log (g_errbuf);
   END load_forecast_items;

   PROCEDURE load_avg_demand (p_retcode OUT NUMBER)
   /*************************************************************************
   $Header XXWC_MTL_DEMAND_FORECAST_PKG.Load_avg_demand $
   Module Name: XXWC_MTL_DEMAND_FORECAST_PKG
   PURPOSE: Procedure to Load Average demand from mtl_demand_histories
   REVISIONS:
   Ver        Date         Author                Description
   ---------  -----------  ------------------    ----------------
   1.0        31-Jul-2015  Manjula Chellappan    Initial Version TMS # 20150701-00047 Generate Demand Forecast Performance Tuning and Redesign
   **************************************************************************/
   IS
      l_retcode                   NUMBER := 0;
      l_error_msg                 VARCHAR2 (400);
      l_history_start_date        DATE;
      l_forecast_start_date       DATE;
      l_num_of_previous_periods   NUMBER;
      l_error_flag                NUMBER := 0;
      l_amu_filter_factor         NUMBER
         := NVL (fnd_profile.VALUE ('XXWC_AMU_FILTER_FACTOR'), 1);
   BEGIN
      drop_temp_table ('XXWC', 'XXWC_MTL_DEMAND_TBL##');

      g_sec := 'Process 2 : Load Demand - Create';

      l_error_msg := 'Failed to Load Demand - Create';

      EXECUTE IMMEDIATE
         ' CREATE TABLE XXWC.XXWC_MTL_DEMAND_TBL## AS 	
           SELECT mdh.period_start_date,
                  mfi.source_organization_id,
                  mp.forecast_designator,
                  mdh.inventory_item_id,
                  SUM (
                       (NVL (mdh.sales_order_demand, 0))
                     + (NVL (mdh.std_wip_usage, 0)))
                     demand,
                  0 amu, 
                  0 final_demand
             FROM xxwc.xxwc_mtl_forecast_param_tbl mp,
                  mtl_demand_histories mdh,
                  xxwc.xxwc_mtl_forecast_items_tbl mfi
            WHERE     mfi.source_organization_id = mp.organization_id
                  AND mdh.period_start_date >= mp.history_start_date
                  AND mdh.period_start_date < mp.forecast_start_date
                  AND mdh.organization_id = mfi.organization_id
                  AND mdh.inventory_item_id = mfi.inventory_item_id
                  AND mfi.forecast_designator = mp.forecast_designator
         GROUP BY mdh.period_start_date,
                  mfi.source_organization_id,
                  mp.forecast_designator,
                  mdh.inventory_item_id ';

      write_log (g_sec);

      COMMIT;

      g_sec := 'Process 2 : Load Demand - Insert';
      l_error_msg := 'Failed to Load Demand - Insert';

      EXECUTE IMMEDIATE 'TRUNCATE TABLE xxwc.xxwc_mtl_demand_tbl ';

      EXECUTE IMMEDIATE 'INSERT /*+ append */
            INTO  XXWC.XXWC_MTL_DEMAND_TBL
			SELECT * FROM XXWC.XXWC_MTL_DEMAND_TBL##';

      write_log (g_sec);

      g_sec := 'Process 3 : Update Total Demand for Items';
      l_error_msg := 'Failed to Update Total Demand for Items';


      UPDATE xxwc.xxwc_mtl_forecast_items_tbl mfi
         SET total_demand =
                (SELECT NVL (SUM (demand), 0)
                   FROM xxwc.xxwc_mtl_demand_tbl
                  WHERE     source_organization_id = 
				                mfi.source_organization_id
                        AND inventory_item_id = mfi.inventory_item_id
                        AND forecast_designator = mfi.forecast_designator)
	   WHERE source_organization_id = organization_id;

      COMMIT;

      write_log (g_sec);

      g_sec := 'Process 4 : Update Average Demand for Items';
      l_error_msg := 'Failed to Update Average Demand for Items';

      UPDATE xxwc.xxwc_mtl_forecast_items_tbl mfi
         SET avg_demand =
                ROUND (
                   NVL (
                        (SELECT NVL (SUM (demand), 0)
                           FROM xxwc.xxwc_mtl_demand_tbl
                          WHERE     source_organization_id =
                                       mfi.source_organization_id
                                AND inventory_item_id = mfi.inventory_item_id
                                AND forecast_designator =
                                       mfi.forecast_designator) , 0)
                      / mfi.num_of_previous_periods
                      )
	   WHERE source_organization_id = organization_id;

      COMMIT;


      write_log (g_sec);

      g_sec := 'Process 5 : Calculate AMU for Items';
      l_error_msg := 'Failed to Calculate AMU for Items';

      UPDATE xxwc.xxwc_mtl_forecast_items_tbl
         SET amu = avg_demand * l_amu_filter_factor
	   WHERE source_organization_id = organization_id;

      COMMIT;

      write_log (g_sec);

      g_sec := 'Process 6 : Update AMU for Demand';
      l_error_msg := 'Failed to Update AMU for Demand';


      UPDATE xxwc.xxwc_mtl_demand_tbl md
         SET amu =
                (SELECT amu
                   FROM xxwc.xxwc_mtl_forecast_items_tbl
                  WHERE     inventory_item_id = md.inventory_item_id
                        AND organization_id = md.source_organization_id
                        AND source_organization_id =
                               md.source_organization_id
                        AND forecast_designator = md.forecast_designator);

      COMMIT;

      write_log (g_sec);

      g_sec := 'Process 7 : Calculate Final Demand';
      l_error_msg := 'Failed to Calculate Final Demand';


      UPDATE xxwc.xxwc_mtl_demand_tbl
         SET final_demand = CASE WHEN demand > amu THEN amu ELSE demand END;

      COMMIT;

      write_log (g_sec);

      g_sec := 'Process 8 : Update Final Demand for Items';
      l_error_msg := 'Failed to Update Final Demand for Items';

      UPDATE xxwc.xxwc_mtl_forecast_items_tbl mfi
         SET final_demand =
                NVL (
                   (  SELECT SUM (final_demand)
                        FROM xxwc.xxwc_mtl_demand_tbl
                       WHERE     inventory_item_id = mfi.inventory_item_id
                             AND source_organization_id =
                                    mfi.source_organization_id
                             AND forecast_designator = mfi.forecast_designator
                    GROUP BY inventory_item_id, source_organization_id),
                   0)
	   WHERE source_organization_id = organization_id;

      COMMIT;


      write_log (g_sec);

      g_sec := 'Process 9 : Update Final Avg Demand for Items';
      l_error_msg := 'Failed to Update Final Avg Demand for Items';

      UPDATE xxwc.xxwc_mtl_forecast_items_tbl mfi
         SET final_avg_demand =
                ROUND (
                     NVL((  SELECT SUM (final_demand)
                          FROM xxwc.xxwc_mtl_demand_tbl
                         WHERE     inventory_item_id = mfi.inventory_item_id
                               AND source_organization_id =
                                      mfi.source_organization_id
                               AND forecast_designator =
                                      mfi.forecast_designator
                      GROUP BY inventory_item_id, source_organization_id),0)
                   / num_of_previous_periods)
	   WHERE source_organization_id = organization_id;

      COMMIT;

      write_log (g_sec);

      g_sec := 'Process 10 : Update Seasonal Demand for Items';
      l_error_msg := 'Failed to Update Seasonal Demand for Items';

      UPDATE xxwc.xxwc_mtl_forecast_items_tbl mfi
         SET avg_demand_1 =
                GREATEST (
                   ROUND (
                      final_avg_demand * ( (100 + season_factor_1) / 100)),
                   0),
             avg_demand_2 =
                GREATEST (
                   ROUND (
                      final_avg_demand * ( (100 + season_factor_2) / 100)),
                   0),
             avg_demand_3 =
                GREATEST (
                   ROUND (
                      final_avg_demand * ( (100 + season_factor_3) / 100)),
                   0),
             avg_demand_4 =
                GREATEST (
                   ROUND (
                      final_avg_demand * ( (100 + season_factor_4) / 100)),
                   0),
             avg_demand_5 =
                GREATEST (
                   ROUND (
                      final_avg_demand * ( (100 + season_factor_5) / 100)),
                   0),
             avg_demand_6 =
                GREATEST (
                   ROUND (
                      final_avg_demand * ( (100 + season_factor_6) / 100)),
                   0),
             avg_demand_7 =
                GREATEST (
                   ROUND (
                      final_avg_demand * ( (100 + season_factor_7) / 100)),
                   0),
             avg_demand_8 =
                GREATEST (
                   ROUND (
                      final_avg_demand * ( (100 + season_factor_8) / 100)),
                   0),
             avg_demand_9 =
                GREATEST (
                   ROUND (
                      final_avg_demand * ( (100 + season_factor_9) / 100)),
                   0),
             avg_demand_10 =
                GREATEST (
                   ROUND (
                      final_avg_demand * ( (100 + season_factor_10) / 100)),
                   0),
             avg_demand_11 =
                GREATEST (
                   ROUND (
                      final_avg_demand * ( (100 + season_factor_11) / 100)),
                   0),
             avg_demand_12 =
                GREATEST (
                   ROUND (
                      final_avg_demand * ( (100 + season_factor_12) / 100)),
                   0)
	   WHERE source_organization_id = organization_id;

      COMMIT;

      write_log (g_sec);

      p_retcode := l_retcode;
   EXCEPTION
      WHEN OTHERS
      THEN
         l_retcode := 2;
         p_retcode := l_retcode;
         l_error_msg := g_sec;
         g_errbuf := l_error_msg || CHR (10) || SQLERRM;
         write_log (g_errbuf);
   END load_avg_demand;

   PROCEDURE generate_forecast (errbuf                 OUT VARCHAR2,
                                retcode                OUT VARCHAR2,
                                p_organization_id          NUMBER,
                                p_forecast          IN     VARCHAR2)
   IS
      /*************************************************************************
      $Header XXWC_MTL_DEMAND_FORECAST_PKG.Generate_forecast $
      Module Name: XXWC_MTL_DEMAND_FORECAST_PKG
      PURPOSE: Procedure to Load Average demand from mtl_demand_histories
      REVISIONS:
      Ver        Date         Author                Description
      ---------  -----------  ------------------    ----------------
      1.0        31-Jul-2015  Manjula Chellappan    Initial Version TMS # 20150701-00047 Generate Demand Forecast Performance Tuning and Redesign
      **************************************************************************/
      l_sec                   VARCHAR2 (100);
      l_success               BOOLEAN := TRUE;
      l_forecast_interface    MRP_FORECAST_INTERFACE_PK.t_forecast_interface;
      l_forecast_designator   MRP_FORECAST_INTERFACE_PK.t_forecast_designator;
      l_ndx                   NUMBER := 0;
      l_forecast_periods      NUMBER;
      l_forecast_start_date   DATE;
      l_next_forecast_date    DATE;
      l_organization_id       NUMBER;
      l_period_avg            NUMBER;
      l_retcode               NUMBER;
      l_process_flag          NUMBER := 0;
      l_forecast              VARCHAR2 (10);
      l_rec_count             NUMBER := 0;
      l_error_msg             VARCHAR2 (100);

      CURSOR c_forecast
      IS
         SELECT *
           FROM xxwc.xxwc_mtl_forecast_param_tbl
          WHERE     organization_id = p_organization_id
                AND forecast_designator = p_forecast;

      CURSOR c_items
      IS
         SELECT i.forecast_designator,
                i.organization_id,
                i.inventory_item_id,
                NVL (i.final_avg_demand, 0) final_avg_demand,
                NVL (i.avg_demand_1, 0) avg_demand_1,
                NVL (i.avg_demand_2, 0) avg_demand_2,
                NVL (i.avg_demand_3, 0) avg_demand_3,
                NVL (i.avg_demand_4, 0) avg_demand_4,
                NVL (i.avg_demand_5, 0) avg_demand_5,
                NVL (i.avg_demand_6, 0) avg_demand_6,
                NVL (i.avg_demand_7, 0) avg_demand_7,
                NVL (i.avg_demand_8, 0) avg_demand_8,
                NVL (i.avg_demand_9, 0) avg_demand_9,
                NVL (i.avg_demand_10, 0) avg_demand_10,
                NVL (i.avg_demand_11, 0) avg_demand_11,
                NVL (i.avg_demand_12, 0) avg_demand_12
           FROM xxwc.xxwc_mtl_forecast_items_tbl i
          WHERE     i.organization_id = p_organization_id
                AND i.organization_id = i.source_organization_id
                AND i.forecast_designator = p_forecast;
   BEGIN
      FOR r_forecast IN c_forecast
      LOOP
         l_rec_count := 0;
         l_organization_id := r_forecast.organization_id;
         l_forecast_start_date := r_forecast.forecast_start_date;
         l_forecast_periods := r_forecast.num_of_forecast_periods;
         l_forecast := r_forecast.forecast_designator;
         g_sec := 'Load forecast for ' || l_forecast;
         write_log (g_sec);

         FOR R_items IN C_Items
         LOOP
            l_next_forecast_date := l_forecast_start_date;

            l_forecast_interface.DELETE;
            l_forecast_designator.DELETE;


            FOR l_ndx IN 1 .. l_forecast_periods
            LOOP
               l_forecast_interface (l_ndx).inventory_item_id :=
                  R_Items.inventory_item_id;
               l_forecast_interface (l_ndx).forecast_designator :=
                  R_Items.forecast_designator;
               l_forecast_interface (l_ndx).organization_id :=
                  R_Items.organization_id;
               l_forecast_interface (l_ndx).forecast_date :=
                  l_next_forecast_date;
               l_period_avg := R_Items.final_avg_demand;

               CASE l_ndx
                  WHEN 1
                  THEN
                     l_forecast_interface (l_ndx).quantity :=
                        R_Items.avg_demand_1;
                  WHEN 2
                  THEN
                     l_forecast_interface (l_ndx).quantity :=
                        R_Items.avg_demand_2;
                  WHEN 3
                  THEN
                     l_forecast_interface (l_ndx).quantity :=
                        R_Items.avg_demand_3;
                  WHEN 4
                  THEN
                     l_forecast_interface (l_ndx).quantity :=
                        R_Items.avg_demand_4;
                  WHEN 5
                  THEN
                     l_forecast_interface (l_ndx).quantity :=
                        R_Items.avg_demand_5;
                  WHEN 6
                  THEN
                     l_forecast_interface (l_ndx).quantity :=
                        R_Items.avg_demand_6;
                  WHEN 7
                  THEN
                     l_forecast_interface (l_ndx).quantity :=
                        R_Items.avg_demand_7;
                  WHEN 8
                  THEN
                     l_forecast_interface (l_ndx).quantity :=
                        R_Items.avg_demand_8;
                  WHEN 9
                  THEN
                     l_forecast_interface (l_ndx).quantity :=
                        R_Items.avg_demand_9;
                  WHEN 10
                  THEN
                     l_forecast_interface (l_ndx).quantity :=
                        R_Items.avg_demand_10;
                  WHEN 11
                  THEN
                     l_forecast_interface (l_ndx).quantity :=
                        R_Items.avg_demand_11;
                  WHEN 12
                  THEN
                     l_forecast_interface (l_ndx).quantity :=
                        R_Items.avg_demand_12;
               END CASE;

               l_forecast_interface (l_ndx).confidence_percentage := 100;
               l_forecast_interface (l_ndx).process_status := 2;
               l_forecast_interface (l_ndx).workday_control := 2; -- 1=reject  2=shift forword  3=shift backwards
               l_forecast_interface (l_ndx).bucket_type :=
                  R_forecast.bucket_type;        -- 1=days  2=weeks  3=periods
               l_forecast_interface (l_ndx).comments :=
                  'Created from demand history';
               l_forecast_interface (l_ndx).source_code := NULL;
               l_forecast_interface (l_ndx).source_line_id := NULL;
               l_forecast_interface (l_ndx).last_update_date := SYSDATE;
               l_forecast_interface (l_ndx).last_updated_by :=
                  FND_GLOBAL.user_id;
               l_forecast_interface (l_ndx).creation_date := SYSDATE;
               l_forecast_interface (l_ndx).created_by := FND_GLOBAL.user_id;
               l_forecast_interface (l_ndx).last_update_login :=
                  FND_GLOBAL.login_id;
               l_next_forecast_date :=
                  MRP_CALENDAR.Date_Offset (r_forecast.organization_id,
                                            r_forecast.bucket_type,
                                            l_next_forecast_date,
                                            1);
            END LOOP;

            l_forecast_designator (1).organization_id :=
               r_items.organization_id;
            l_forecast_designator (1).forecast_designator :=
               r_items.forecast_designator;
            l_forecast_designator (1).inventory_item_id :=
               R_Items.inventory_item_id;
            g_sec := 'Before API Call';
            l_success :=
               MRP_FORECAST_INTERFACE_PK.MRP_Forecast_Interface (
                  forecast_interface    => l_forecast_interface,
                  forecast_designator   => l_forecast_designator);

            IF NOT l_success
            THEN
               write_log (
                  'Forecast Failed for ' || R_Items.inventory_item_id);
            END IF;

            FOR i IN 1 .. l_ndx
            LOOP
               IF l_forecast_interface (i).error_message IS NOT NULL
               THEN
                  l_success := FALSE;
                  l_error_msg :=
                        l_forecast_interface (i).organization_id
                     || ' '
                     || l_forecast_interface (i).forecast_designator
                     || ' '
                     || l_forecast_interface (i).inventory_item_id
                     || ' '
                     || l_forecast_interface (i).forecast_date
                     || ' '
                     || l_forecast_interface (i).error_message;
                  write_log (l_error_msg);
                  GOTO laststep;
               END IF;

               COMMIT;
            END LOOP;

            l_rec_count := l_rec_count + 1;
         END LOOP;

         write_log (
               r_forecast.forecast_designator
            || ' : Load completed : No Of Items - '
            || l_rec_count);
         g_sec := 'Update AMU in MTL_SYSTEM_ITEMS_B for Forecast : '||l_forecast;

         CASE r_forecast.num_of_forecast_periods
            WHEN 1
            THEN
               UPDATE mtl_system_items_b msi
                  SET attribute20 =
                         (SELECT ROUND (
                                      (NVL (avg_demand_1, 0) / 1)
                                    * DECODE (bucket_type,  1, 28,  2, 4,  1))
                            FROM xxwc.xxwc_mtl_forecast_items_tbl mfi
                           WHERE     mfi.organization_id =
                                        msi.organization_id
								 AND mfi.source_organization_id =
                                        msi.organization_id	
                                 AND mfi.inventory_item_id =
                                        msi.inventory_item_id
                                 AND mfi.forecast_designator =
                                        r_forecast.forecast_designator)
                WHERE     msi.organization_id = r_forecast.organization_id
                      AND EXISTS
                             (SELECT 'x'
                                FROM xxwc.xxwc_mtl_forecast_items_tbl
                               WHERE     organization_id =
                                            r_forecast.organization_id
									 AND source_organization_id =
                                            r_forecast.organization_id
                                     AND forecast_designator =
                                            r_forecast.forecast_designator
                                     AND inventory_item_id =
                                            msi.inventory_item_id);
            WHEN 2
            THEN
               UPDATE mtl_system_items_b msi
                  SET attribute20 =
                         (SELECT ROUND (
                                      (  (  NVL (avg_demand_1, 0)
                                          + NVL (avg_demand_2, 0))
                                       / 2)
                                    * DECODE (bucket_type,  1, 28,  2, 4,  1))
                            FROM xxwc.xxwc_mtl_forecast_items_tbl mfi
                           WHERE     mfi.organization_id =
                                        msi.organization_id
								 AND mfi.source_organization_id =
                                        msi.organization_id	
                                 AND mfi.inventory_item_id =
                                        msi.inventory_item_id
                                 AND mfi.forecast_designator =
                                        r_forecast.forecast_designator)
                WHERE     msi.organization_id = r_forecast.organization_id
                      AND EXISTS
                             (SELECT 'x'
                                FROM xxwc.xxwc_mtl_forecast_items_tbl
                               WHERE     organization_id =
                                            r_forecast.organization_id
									 AND source_organization_id =
                                            r_forecast.organization_id
                                     AND forecast_designator =
                                            r_forecast.forecast_designator
                                     AND inventory_item_id =
                                            msi.inventory_item_id);
            WHEN 3
            THEN
               UPDATE mtl_system_items_b msi
                  SET attribute20 =
                         (SELECT ROUND (
                                      (  (  NVL (avg_demand_1, 0)
                                          + NVL (avg_demand_2, 0)
                                          + NVL (avg_demand_3, 0))
                                       / 3)
                                    * DECODE (bucket_type,  1, 28,  2, 4,  1))
                            FROM xxwc.xxwc_mtl_forecast_items_tbl mfi
                           WHERE     mfi.organization_id =
                                        msi.organization_id
								 AND mfi.source_organization_id =
                                        msi.organization_id	
                                 AND mfi.inventory_item_id =
                                        msi.inventory_item_id
                                 AND mfi.forecast_designator =
                                        r_forecast.forecast_designator)
                WHERE     msi.organization_id = r_forecast.organization_id
                      AND EXISTS
                             (SELECT 'x'
                                FROM xxwc.xxwc_mtl_forecast_items_tbl
                               WHERE     organization_id =
                                            r_forecast.organization_id
									 AND source_organization_id =
                                            r_forecast.organization_id
                                     AND forecast_designator =
                                            r_forecast.forecast_designator
                                     AND inventory_item_id =
                                            msi.inventory_item_id);
            WHEN 4
            THEN
               UPDATE mtl_system_items_b msi
                  SET attribute20 =
                         (SELECT ROUND (
                                      (  (  NVL (avg_demand_1, 0)
                                          + NVL (avg_demand_2, 0)
                                          + NVL (avg_demand_3, 0)
                                          + NVL (avg_demand_4, 0))
                                       / 4)
                                    * DECODE (bucket_type,  1, 28,  2, 4,  1))
                            FROM xxwc.xxwc_mtl_forecast_items_tbl mfi
                           WHERE     mfi.organization_id =
                                        msi.organization_id
								 AND mfi.source_organization_id =
                                        msi.organization_id	
                                 AND mfi.inventory_item_id =
                                        msi.inventory_item_id
                                 AND mfi.forecast_designator =
                                        r_forecast.forecast_designator)
                WHERE     msi.organization_id = r_forecast.organization_id
                      AND EXISTS
                             (SELECT 'x'
                                FROM xxwc.xxwc_mtl_forecast_items_tbl
                               WHERE     organization_id =
                                            r_forecast.organization_id
									 AND source_organization_id =
                                            r_forecast.organization_id
                                     AND forecast_designator =
                                            r_forecast.forecast_designator
                                     AND inventory_item_id =
                                            msi.inventory_item_id);
            WHEN 5
            THEN
               UPDATE mtl_system_items_b msi
                  SET attribute20 =
                         (SELECT ROUND (
                                      (  (  NVL (avg_demand_1, 0)
                                          + NVL (avg_demand_2, 0)
                                          + NVL (avg_demand_3, 0)
                                          + NVL (avg_demand_4, 0)
                                          + NVL (avg_demand_5, 0))
                                       / 5)
                                    * DECODE (bucket_type,  1, 28,  2, 4,  1))
                            FROM xxwc.xxwc_mtl_forecast_items_tbl mfi
                           WHERE     mfi.organization_id =
                                        msi.organization_id
								 AND mfi.source_organization_id =
                                        msi.organization_id	
                                 AND mfi.inventory_item_id =
                                        msi.inventory_item_id
                                 AND mfi.forecast_designator =
                                        r_forecast.forecast_designator)
                WHERE     msi.organization_id = r_forecast.organization_id
                      AND EXISTS
                             (SELECT 'x'
                                FROM xxwc.xxwc_mtl_forecast_items_tbl
                               WHERE     organization_id =
                                            r_forecast.organization_id
									 AND source_organization_id =
                                            r_forecast.organization_id
                                     AND forecast_designator =
                                            r_forecast.forecast_designator
                                     AND inventory_item_id =
                                            msi.inventory_item_id);
            WHEN 6
            THEN
               UPDATE mtl_system_items_b msi
                  SET attribute20 =
                         (SELECT ROUND (
                                      (  (  NVL (avg_demand_1, 0)
                                          + NVL (avg_demand_2, 0)
                                          + NVL (avg_demand_3, 0)
                                          + NVL (avg_demand_4, 0)
                                          + NVL (avg_demand_5, 0)
                                          + NVL (avg_demand_6, 0))
                                       / 6)
                                    * DECODE (bucket_type,  1, 28,  2, 4,  1))
                            FROM xxwc.xxwc_mtl_forecast_items_tbl mfi
                           WHERE     mfi.organization_id =
                                        msi.organization_id
								 AND mfi.source_organization_id =
                                        msi.organization_id	
                                 AND mfi.inventory_item_id =
                                        msi.inventory_item_id
                                 AND mfi.forecast_designator =
                                        r_forecast.forecast_designator)
                WHERE     msi.organization_id = r_forecast.organization_id
                      AND EXISTS
                             (SELECT 'x'
                                FROM xxwc.xxwc_mtl_forecast_items_tbl
                               WHERE     organization_id =
                                            r_forecast.organization_id
									 AND source_organization_id =
                                            r_forecast.organization_id
                                     AND forecast_designator =
                                            r_forecast.forecast_designator
                                     AND inventory_item_id =
                                            msi.inventory_item_id);
            WHEN 7
            THEN
               UPDATE mtl_system_items_b msi
                  SET attribute20 =
                         (SELECT ROUND (
                                      (  (  NVL (avg_demand_1, 0)
                                          + NVL (avg_demand_2, 0)
                                          + NVL (avg_demand_3, 0)
                                          + NVL (avg_demand_4, 0)
                                          + NVL (avg_demand_5, 0)
                                          + NVL (avg_demand_6, 0)
                                          + NVL (avg_demand_7, 0))
                                       / 7)
                                    * DECODE (bucket_type,  1, 28,  2, 4,  1))
                            FROM xxwc.xxwc_mtl_forecast_items_tbl mfi
                           WHERE     mfi.organization_id =
                                        msi.organization_id
								 AND mfi.source_organization_id =
                                        msi.organization_id	
                                 AND mfi.inventory_item_id =
                                        msi.inventory_item_id
                                 AND mfi.forecast_designator =
                                        r_forecast.forecast_designator)
                WHERE     msi.organization_id = r_forecast.organization_id
                      AND EXISTS
                             (SELECT 'x'
                                FROM xxwc.xxwc_mtl_forecast_items_tbl
                               WHERE     organization_id =
                                            r_forecast.organization_id
									 AND source_organization_id =
                                            r_forecast.organization_id
                                     AND forecast_designator =
                                            r_forecast.forecast_designator
                                     AND inventory_item_id =
                                            msi.inventory_item_id);
            WHEN 8
            THEN
               UPDATE mtl_system_items_b msi
                  SET attribute20 =
                         (SELECT ROUND (
                                      (  (  NVL (avg_demand_1, 0)
                                          + NVL (avg_demand_2, 0)
                                          + NVL (avg_demand_3, 0)
                                          + NVL (avg_demand_4, 0)
                                          + NVL (avg_demand_5, 0)
                                          + NVL (avg_demand_6, 0)
                                          + NVL (avg_demand_7, 0)
                                          + NVL (avg_demand_8, 0))
                                       / 8)
                                    * DECODE (bucket_type,  1, 28,  2, 4,  1))
                            FROM xxwc.xxwc_mtl_forecast_items_tbl mfi
                           WHERE     mfi.organization_id =
                                        msi.organization_id
								 AND mfi.source_organization_id =
                                        msi.organization_id	
                                 AND mfi.inventory_item_id =
                                        msi.inventory_item_id
                                 AND mfi.forecast_designator =
                                        r_forecast.forecast_designator)
                WHERE     msi.organization_id = r_forecast.organization_id
                      AND EXISTS
                             (SELECT 'x'
                                FROM xxwc.xxwc_mtl_forecast_items_tbl
                               WHERE     organization_id =
                                            r_forecast.organization_id
									 AND source_organization_id =
                                            r_forecast.organization_id
                                     AND forecast_designator =
                                            r_forecast.forecast_designator
                                     AND inventory_item_id =
                                            msi.inventory_item_id);
            WHEN 9
            THEN
               UPDATE mtl_system_items_b msi
                  SET attribute20 =
                         (SELECT ROUND (
                                      (  (  NVL (avg_demand_1, 0)
                                          + NVL (avg_demand_2, 0)
                                          + NVL (avg_demand_3, 0)
                                          + NVL (avg_demand_4, 0)
                                          + NVL (avg_demand_5, 0)
                                          + NVL (avg_demand_6, 0)
                                          + NVL (avg_demand_7, 0)
                                          + NVL (avg_demand_8, 0)
                                          + NVL (avg_demand_9, 0))
                                       / 9)
                                    * DECODE (bucket_type,  1, 28,  2, 4,  1))
                            FROM xxwc.xxwc_mtl_forecast_items_tbl mfi
                           WHERE     mfi.organization_id =
                                        msi.organization_id
								 AND mfi.source_organization_id =
                                        msi.organization_id	
                                 AND mfi.inventory_item_id =
                                        msi.inventory_item_id
                                 AND mfi.forecast_designator =
                                        r_forecast.forecast_designator)
                WHERE     msi.organization_id = r_forecast.organization_id
                      AND EXISTS
                             (SELECT 'x'
                                FROM xxwc.xxwc_mtl_forecast_items_tbl
                               WHERE     organization_id =
                                            r_forecast.organization_id
									 AND source_organization_id =
                                            r_forecast.organization_id
                                     AND forecast_designator =
                                            r_forecast.forecast_designator
                                     AND inventory_item_id =
                                            msi.inventory_item_id);
            WHEN 10
            THEN
               UPDATE mtl_system_items_b msi
                  SET attribute20 =
                         (SELECT ROUND (
                                      (  (  NVL (avg_demand_1, 0)
                                          + NVL (avg_demand_2, 0)
                                          + NVL (avg_demand_3, 0)
                                          + NVL (avg_demand_4, 0)
                                          + NVL (avg_demand_5, 0)
                                          + NVL (avg_demand_6, 0)
                                          + NVL (avg_demand_7, 0)
                                          + NVL (avg_demand_8, 0)
                                          + NVL (avg_demand_9, 0)
                                          + NVL (avg_demand_10, 0))
                                       / 10)
                                    * DECODE (bucket_type,  1, 28,  2, 4,  1))
                            FROM xxwc.xxwc_mtl_forecast_items_tbl mfi
                           WHERE     mfi.organization_id =
                                        msi.organization_id
								 AND mfi.source_organization_id =
                                        msi.organization_id	
                                 AND mfi.inventory_item_id =
                                        msi.inventory_item_id
                                 AND mfi.forecast_designator =
                                        r_forecast.forecast_designator)
                WHERE     msi.organization_id = r_forecast.organization_id
                      AND EXISTS
                             (SELECT 'x'
                                FROM xxwc.xxwc_mtl_forecast_items_tbl
                               WHERE     organization_id =
                                            r_forecast.organization_id
									 AND source_organization_id =
                                            r_forecast.organization_id
                                     AND forecast_designator =
                                            r_forecast.forecast_designator
                                     AND inventory_item_id =
                                            msi.inventory_item_id);
            WHEN 11
            THEN
               UPDATE mtl_system_items_b msi
                  SET attribute20 =
                         (SELECT ROUND (
                                      (  (  NVL (avg_demand_1, 0)
                                          + NVL (avg_demand_2, 0)
                                          + NVL (avg_demand_3, 0)
                                          + NVL (avg_demand_4, 0)
                                          + NVL (avg_demand_5, 0)
                                          + NVL (avg_demand_6, 0)
                                          + NVL (avg_demand_7, 0)
                                          + NVL (avg_demand_8, 0)
                                          + NVL (avg_demand_9, 0)
                                          + NVL (avg_demand_10, 0)
                                          + NVL (avg_demand_11, 0))
                                       / 11)
                                    * DECODE (bucket_type,  1, 28,  2, 4,  1))
                            FROM xxwc.xxwc_mtl_forecast_items_tbl mfi
                           WHERE     mfi.organization_id =
                                        msi.organization_id
								 AND mfi.source_organization_id =
                                        msi.organization_id	
                                 AND mfi.inventory_item_id =
                                        msi.inventory_item_id
                                 AND mfi.forecast_designator =
                                        r_forecast.forecast_designator)
                WHERE     msi.organization_id = r_forecast.organization_id
                      AND EXISTS
                             (SELECT 'x'
                                FROM xxwc.xxwc_mtl_forecast_items_tbl
                               WHERE     organization_id =
                                            r_forecast.organization_id
									 AND source_organization_id =
                                            r_forecast.organization_id
                                     AND forecast_designator =
                                            r_forecast.forecast_designator
                                     AND inventory_item_id =
                                            msi.inventory_item_id);
            WHEN 12
            THEN
               UPDATE mtl_system_items_b msi
                  SET attribute20 =
                         (SELECT ROUND (
                                      (  (  NVL (avg_demand_1, 0)
                                          + NVL (avg_demand_2, 0)
                                          + NVL (avg_demand_3, 0)
                                          + NVL (avg_demand_4, 0)
                                          + NVL (avg_demand_5, 0)
                                          + NVL (avg_demand_6, 0)
                                          + NVL (avg_demand_7, 0)
                                          + NVL (avg_demand_8, 0)
                                          + NVL (avg_demand_9, 0)
                                          + NVL (avg_demand_10, 0)
                                          + NVL (avg_demand_11, 0)
                                          + NVL (avg_demand_12, 0))
                                       / num_of_forecast_periods)
                                    * DECODE (bucket_type,  1, 28,  2, 4,  1))
                            FROM xxwc.xxwc_mtl_forecast_items_tbl mfi
                           WHERE     mfi.organization_id =
                                        msi.organization_id
								 AND mfi.source_organization_id =
                                        msi.organization_id	
                                 AND mfi.inventory_item_id =
                                        msi.inventory_item_id
                                 AND mfi.forecast_designator =
                                        r_forecast.forecast_designator)
                WHERE     msi.organization_id = r_forecast.organization_id
                      AND EXISTS
                             (SELECT 'x'
                                FROM xxwc.xxwc_mtl_forecast_items_tbl
                               WHERE     organization_id =
                                            r_forecast.organization_id
									 AND source_organization_id =
                                            r_forecast.organization_id
                                     AND forecast_designator =
                                            r_forecast.forecast_designator
                                     AND inventory_item_id =
                                            msi.inventory_item_id);
         END CASE;

         write_log (
               r_forecast.forecast_designator
            || ' : '
            || SQL%ROWCOUNT
            || ' Items AMU update Completed in MTL_SYSTEM_ITEMS_B ');

         g_sec := 'Delete Parameter Record from xxwc_mtl_forecast_param_tbl ';

         DELETE FROM xxwc.xxwc_mtl_forecast_param_tbl
               WHERE forecast_designator = r_forecast.forecast_designator;

         write_log (
               r_forecast.forecast_designator
            || ' : '
            || 'Record Deleted From XXWC.XXWC_MTL_FORECAST_PARAM_TBL');

         COMMIT;
      END LOOP;

     <<laststep>>
      IF l_retcode = 2
      THEN
         retcode := 2;
         errbuf := g_errbuf;

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_MTL_DEMAND_FORECAST_PKG.Generate_forecast',
            p_calling             => g_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          DBMS_UTILITY.format_error_stack ()
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => l_error_msg,
            p_distribution_list   => g_dflt_email,
            p_module              => 'INV');
         COMMIT;

         BEGIN
            g_sec :=
               'Delete Parameter Record from xxwc_mtl_forecast_param_tbl ';

            DELETE FROM xxwc.xxwc_mtl_forecast_param_tbl
                  WHERE forecast_designator = p_forecast;

            COMMIT;
         EXCEPTION
            WHEN OTHERS
            THEN
               xxcus_error_pkg.xxcus_error_main_api (
                  p_called_from         => 'XXWC_MTL_DEMAND_FORECAST_PKG.Generate_forecast',
                  p_calling             => g_sec,
                  p_request_id          => fnd_global.conc_request_id,
                  p_ora_error_msg       => SUBSTR (
                                                DBMS_UTILITY.format_error_stack ()
                                             || DBMS_UTILITY.format_error_backtrace (),
                                             1,
                                             2000),
                  p_error_desc          => 'OTHERS EXCEPTION',
                  p_distribution_list   => g_dflt_email,
                  p_module              => 'INV');
               COMMIT;
         END;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         retcode := 2;
         errbuf := g_errbuf;

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_MTL_DEMAND_FORECAST_PKG.Generate_forecast',
            p_calling             => g_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          DBMS_UTILITY.format_error_stack ()
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => 'OTHERS EXCEPTION',
            p_distribution_list   => g_dflt_email,
            p_module              => 'INV');
         COMMIT;

         BEGIN
            g_sec :=
               'Delete Parameter Record from xxwc_mtl_forecast_param_tbl ';

            DELETE FROM xxwc.xxwc_mtl_forecast_param_tbl
                  WHERE forecast_designator = p_forecast;

            COMMIT;
         EXCEPTION
            WHEN OTHERS
            THEN
               xxcus_error_pkg.xxcus_error_main_api (
                  p_called_from         => 'XXWC_MTL_DEMAND_FORECAST_PKG.Generate_forecast',
                  p_calling             => g_sec,
                  p_request_id          => fnd_global.conc_request_id,
                  p_ora_error_msg       => SUBSTR (
                                                DBMS_UTILITY.format_error_stack ()
                                             || DBMS_UTILITY.format_error_backtrace (),
                                             1,
                                             2000),
                  p_error_desc          => 'OTHERS EXCEPTION',
                  p_distribution_list   => g_dflt_email,
                  p_module              => 'INV');
               COMMIT;
         END;
   END generate_forecast;

   PROCEDURE generate_forecast_main (errbuf    OUT VARCHAR2,
                                     retcode   OUT VARCHAR2)
   IS
      /**********************************************************************************************************************************************
      $Header XXWC_MTL_DEMAND_FORECAST_PKG.Generate_forecast_main $
      Module Name: XXWC_MTL_DEMAND_FORECAST_PKG
      PURPOSE: Procedure to Load Average demand from mtl_demand_histories
      REVISIONS:
      Ver        Date         Author                Description
      ---------  -----------  ------------------    ------------------------------------------------------------------------------------------------
      1.0        31-Jul-2015  Manjula Chellappan    Initial Version TMS # 20150701-00047 Generate Demand Forecast Performance Tuning and Redesign
      1.1        26-Mar-2017  P.Vamshidhar          TMS#20170228-00077 - Performance issue with XXWC MTL Generate Demand Forecast Program	  
      **********************************************************************************************************************************************/
      l_sec            VARCHAR2 (100);
      l_retcode        NUMBER;
      l_process_flag   NUMBER := 0;
      l_rec_count      NUMBER := 0;
      l_request_id     NUMBER;
      l_program_name   VARCHAR2 (100) := 'XXWC_MTL_DEMAND_FORECAST';
      l_request_data        VARCHAR2 (20) := '';           -- Added in Rev 1.1
      l_parent_request_id   NUMBER;                        -- Added in Rev 1.1
      l_org_count       NUMBER;                            --Added in Rev 1.1
      l_tbl_org_count   NUMBER;                            --Added in Rev 1.1
      l_index_flag      VARCHAR2 (10);                     --Added in Rev 1.1

      CURSOR c_forecast
      IS
         --SELECT * FROM xxwc.xxwc_mtl_forecast_param_tbl a ORDER BY a.rowid; commented in Rev 1.1
           SELECT * FROM xxwc.xxwc_mtl_forecast_param_tbl a ORDER BY a.sno; --commented in Rev 1.1		 
   BEGIN
      -- <START>  Ver#1.1
      l_request_data := fnd_conc_global.request_data;
      l_parent_request_id := fnd_global.conc_request_id;

      IF l_request_data IS NULL
      THEN
         l_request_data := '1';
         cleanup_log;	  
         BEGIN
			SELECT flv.MEANING INTO l_index_flag
			  FROM apps.fnd_lookup_types flt, apps.fnd_lookup_values flv
			 WHERE     flt.lookup_type = 'XXWC_MTL_DEMAND_FORECAST_LKP'
				   AND flt.lookup_type = flv.lookup_type
				   AND flv.enabled_flag = 'Y'
				   AND SYSDATE BETWEEN NVL (flv.start_date_active, SYSDATE - 1)
								   AND NVL (flv.end_date_active, SYSDATE + 1)
				   AND flv.lookup_code = 'INDEX_RECREATE';

            FND_FILE.PUT_LINE (FND_FILE.LOG,
                               'Value Set Index Drop Flag:' || l_index_flag);
         EXCEPTION
            WHEN OTHERS
            THEN
               l_index_flag := 'N';
               FND_FILE.PUT_LINE (FND_FILE.LOG,
                                  'Ex - l_index_flag ' || l_index_flag);
               FND_FILE.PUT_LINE (
                  FND_FILE.LOG,
                     'Error occured while deriving values from value set (INDEX_DROP)'
                  || SUBSTR (SQLERRM, 1, 250));
         END;
		 
		 IF l_index_flag ='I' THEN
		 CREATE_DROP_INDEXES ('CREATE');
		 l_retcode :=3;
		 GOTO laststep;
		 END IF;
		 
		 
         -- <END>  Ver#1.1
      load_forecast_items (l_retcode);

      IF l_retcode = 2
      THEN
         GOTO laststep;
      END IF;

      load_avg_demand (l_retcode);

      IF l_retcode = 2
      THEN
         GOTO laststep;
      END IF;

         -- <START>  Ver#1.1

         BEGIN
			SELECT TO_NUMBER (flv.MEANING)
			  INTO l_org_count
			  FROM apps.fnd_lookup_types flt, apps.fnd_lookup_values flv
			 WHERE     flt.lookup_type = 'XXWC_MTL_DEMAND_FORECAST_LKP'
				   AND flt.lookup_type = flv.lookup_type
				   AND flv.enabled_flag = 'Y'
				   AND SYSDATE BETWEEN NVL (flv.start_date_active, SYSDATE - 1)
								   AND NVL (flv.end_date_active, SYSDATE + 1)
				   AND flv.lookup_code = 'ORG_COUNT';


            FND_FILE.PUT_LINE (FND_FILE.LOG,
                               'Value Set Org count vlaue: ' || l_org_count);
         EXCEPTION
            WHEN OTHERS
            THEN
               l_org_count := 0;
               FND_FILE.PUT_LINE (FND_FILE.LOG,
                                  'Ex - l_org_count ' || l_org_count);
               FND_FILE.PUT_LINE (
                  FND_FILE.LOG,
                     'Error occured while deriving values from value set (ORG_COUNT)'
                  || SUBSTR (SQLERRM, 1, 250));
         END;

         BEGIN
            SELECT COUNT (DISTINCT ORGANIZATION_ID)
              INTO l_tbl_org_count
              FROM xxwc.xxwc_mtl_forecast_param_tbl;

            FND_FILE.PUT_LINE (
               FND_FILE.LOG,
               'Distinct Org count from Table:' || l_tbl_org_count);
         EXCEPTION
            WHEN OTHERS
            THEN
               l_tbl_org_count := 0;
               FND_FILE.PUT_LINE (
                  FND_FILE.LOG,
                  'Ex - Distinct Org count from Table:' || l_tbl_org_count);
         END;



         IF     NVL (l_index_flag, 'N') = 'Y'
            AND NVL (l_tbl_org_count, 0) >= NVL (l_org_count, 0)
         THEN
            CREATE_DROP_INDEXES ('DROP');
         END IF;

         -- <END>  Ver#1.1

      FOR r_forecast IN c_forecast
      LOOP
         l_request_id :=
            fnd_request.submit_request ('XXWC',
                                        l_program_name,
                                        NULL,
                                        NULL,
                                        --FALSE,  -- commented in rev 1.1
										TRUE, -- Added in Rev 1.1
                                        r_forecast.organization_id,
                                        r_forecast.forecast_designator);

         IF l_request_id <= 0
         THEN
            l_retcode := 2;
            write_log (
                  'Failed to submit XXWC MTL Generate Demand Forecast Program for '
               || r_forecast.forecast_designator);
         ELSE
            l_rec_count := l_rec_count + 1;
           -- COMMIT;  Commented in 1.1
         END IF;

            -- <START>  Ver#1.1

            fnd_conc_global.set_req_globals (conc_status    => 'PAUSED',
                                             request_data   => l_request_data);


            l_request_data := TO_CHAR (TO_NUMBER (l_request_data) + 1);
         -- <END>  Ver#1.1	 
		 
      END LOOP;

	  COMMIT; -- Added in 1.1
	  
	  
      write_log (
            'XXWC MTL Generate Demand Forecast Program Submitted for '
         || l_rec_count
         || ' Branches');

     <<laststep>>
      IF l_retcode = 2
      THEN
         retcode := 2;
         errbuf := g_errbuf;

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_MTL_DEMAND_FORECAST_PKG.Generate_forecast_main',
            p_calling             => g_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          DBMS_UTILITY.format_error_stack ()
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => 'Process Error',
            p_distribution_list   => g_dflt_email,
            p_module              => 'INV');

         BEGIN
            g_sec :=
               'Delete Parameter Records from xxwc_mtl_forecast_param_tbl ';

            DELETE FROM xxwc.xxwc_mtl_forecast_param_tbl;

            COMMIT;
         EXCEPTION
            WHEN OTHERS
            THEN
               xxcus_error_pkg.xxcus_error_main_api (
                  p_called_from         => 'XXWC_MTL_DEMAND_FORECAST_PKG.Generate_forecast_main',
                  p_calling             => g_sec,
                  p_request_id          => fnd_global.conc_request_id,
                  p_ora_error_msg       => SUBSTR (
                                                DBMS_UTILITY.format_error_stack ()
                                             || DBMS_UTILITY.format_error_backtrace (),
                                             1,
                                             2000),
                  p_error_desc          => 'OTHERS Exception',
                  p_distribution_list   => g_dflt_email,
                  p_module              => 'INV');
               COMMIT;
         END;
      END IF;

      IF l_retcode = 3 THEN
          FND_FILE.PUT_LINE(FND_FILE.LOG,'Program existing');
      END IF; 	  
	  
	  -- Below code added in Rev 1.1
	  ELSE
	   write_log ('Child Programs completed');

	   BEGIN
			SELECT flv.MEANING INTO l_index_flag
			  FROM apps.fnd_lookup_types flt, apps.fnd_lookup_values flv
			 WHERE     flt.lookup_type = 'XXWC_MTL_DEMAND_FORECAST_LKP'
				   AND flt.lookup_type = flv.lookup_type
				   AND flv.enabled_flag = 'Y'
				   AND SYSDATE BETWEEN NVL (flv.start_date_active, SYSDATE - 1)
								   AND NVL (flv.end_date_active, SYSDATE + 1)
				   AND flv.lookup_code = 'INDEX_RECREATE';

            FND_FILE.PUT_LINE (FND_FILE.LOG,
                               'Value Set Index Drop Flag:' || l_index_flag);
         EXCEPTION
            WHEN OTHERS
            THEN
               l_index_flag := 'N';
               FND_FILE.PUT_LINE (FND_FILE.LOG,
                                  'Ex - l_index_flag ' || l_index_flag);
               FND_FILE.PUT_LINE (
                  FND_FILE.LOG,
                     'Error occured while deriving values from value set (INDEX_DROP)'
                  || SUBSTR (SQLERRM, 1, 250));
         END;

         BEGIN
            g_sec :=
               'Delete Parameter Records from xxwc_mtl_forecast_param_tbl ';

            DELETE FROM xxwc.xxwc_mtl_forecast_param_tbl;

            COMMIT;
         EXCEPTION
            WHEN OTHERS
            THEN
               xxcus_error_pkg.xxcus_error_main_api (
                  p_called_from         => 'XXWC_MTL_DEMAND_FORECAST_PKG.Generate_forecast_main',
                  p_calling             => g_sec,
                  p_request_id          => fnd_global.conc_request_id,
                  p_ora_error_msg       => SUBSTR (
                                                DBMS_UTILITY.format_error_stack ()
                                             || DBMS_UTILITY.format_error_backtrace (),
                                             1,
                                             2000),
                  p_error_desc          => 'OTHERS Exception',
                  p_distribution_list   => g_dflt_email,
                  p_module              => 'INV');
               COMMIT;
         END;

         BEGIN
            IF     NVL (l_index_flag, 'N') = 'Y'
            THEN
               CREATE_DROP_INDEXES ('CREATE');
            END IF;
         END;
      END IF;
	  
   EXCEPTION
      WHEN OTHERS
      THEN
         retcode := 2;
         errbuf := g_errbuf;

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_MTL_DEMAND_FORECAST_PKG.Generate_forecast_main',
            p_calling             => g_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          DBMS_UTILITY.format_error_stack ()
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => 'OTHERS Exception',
            p_distribution_list   => g_dflt_email,
            p_module              => 'INV');
         COMMIT;
         -- Added Below code in Rev 1.1
         BEGIN
            IF     NVL (l_index_flag, 'N') = 'Y'
            THEN
               CREATE_DROP_INDEXES ('CREATE');
            END IF;
         END;		 

         BEGIN
            g_sec :=
               'Delete Parameter Records from xxwc_mtl_forecast_param_tbl ';

            DELETE FROM xxwc.xxwc_mtl_forecast_param_tbl;

            COMMIT;
         EXCEPTION
            WHEN OTHERS
            THEN
               xxcus_error_pkg.xxcus_error_main_api (
                  p_called_from         => 'XXWC_MTL_DEMAND_FORECAST_PKG.Generate_forecast_main',
                  p_calling             => g_sec,
                  p_request_id          => fnd_global.conc_request_id,
                  p_ora_error_msg       => SUBSTR (
                                                DBMS_UTILITY.format_error_stack ()
                                             || DBMS_UTILITY.format_error_backtrace (),
                                             1,
                                             2000),
                  p_error_desc          => 'OTHERS Exception',
                  p_distribution_list   => g_dflt_email,
                  p_module              => 'INV');
               COMMIT;
         END;
   END generate_forecast_main;



   PROCEDURE Submit_forecast (
      errbuf                      OUT VARCHAR2,
      retcode                     OUT VARCHAR2,
      p_organization_id        IN     NUMBER,
      p_forecast_designator    IN     VARCHAR2,
      p_branch_dc_mode         IN     VARCHAR2,
      p_item_range             IN     VARCHAR2,
      p_item_category          IN     VARCHAR2,
      p_inventory_item_id      IN     NUMBER DEFAULT NULL,
      p_bucket_type            IN     NUMBER DEFAULT 3,
      p_start_date             IN     VARCHAR2,
      p_forecast_periods       IN     NUMBER DEFAULT 12,
      p_history_periods        IN     NUMBER DEFAULT 12,
      p_constant_seasonality   IN     NUMBER DEFAULT 0,
      p_seasonality_1          IN     NUMBER DEFAULT 0,
      p_seasonality_2          IN     NUMBER DEFAULT 0,
      p_seasonality_3          IN     NUMBER DEFAULT 0,
      p_seasonality_4          IN     NUMBER DEFAULT 0,
      p_seasonality_5          IN     NUMBER DEFAULT 0,
      p_seasonality_6          IN     NUMBER DEFAULT 0,
      p_seasonality_7          IN     NUMBER DEFAULT 0,
      p_seasonality_8          IN     NUMBER DEFAULT 0,
      p_seasonality_9          IN     NUMBER DEFAULT 0,
      p_seasonality_10         IN     NUMBER DEFAULT 0,
      p_seasonality_11         IN     NUMBER DEFAULT 0,
      p_seasonality_12         IN     NUMBER DEFAULT 0)
   IS
      /*************************************************************************
           $Header XXWC_MTL_DEMAND_FORECAST_PKG.Submit_forecast $
           Module Name: XXWC_MTL_DEMAND_FORECAST_PKG
           PURPOSE: Procedure for the concurrent program to generate forecast
           REVISIONS:
           Ver        Date         Author                Description
           ---------  -----------  ------------------    ----------------
           1.0        01-Oct-2015  Manjula Chellappan    Initial Version TMS # 20150701-00047 Generate Demand Forecast Performance Tuning and Redesign

       **************************************************************************/

      l_error_msg                   VARCHAR2 (240);
      l_req_id                      NUMBER;
      l_start_date                  DATE := TO_DATE (p_start_date, 'YYYY/MM/DD HH24:MI:SS');
      l_item_number                 VARCHAR2 (30);
      l_calendar_code               VARCHAR2 (10);
      l_calendar_exception_set_id   NUMBER;
      l_forecast_start_date         DATE;
      l_organization_code           VARCHAR2 (10);
      l_history_start_date          DATE;
      l_forecast_periods            NUMBER;
      l_history_periods             NUMBER;
      l_exception                   EXCEPTION;
      l_error_flag                  NUMBER := 0;
      l_sec                         VARCHAR2 (100);
      l_distribution_list           fnd_user.email_address%TYPE
                                       := 'HDSOracleDevelopers@hdsupply.com';
      l_inventory_item_id           NUMBER;
      l_category_id                 NUMBER;
      l_retcode                     NUMBER;
      l_request_id                  NUMBER;
      l_program_name                VARCHAR2 (100)
                                       := 'XXWC_MTL_DEMAND_FORECAST_MAIN';
      xxwc_exception                EXCEPTION;
   BEGIN
      l_sec := 'Get Organization Details';

      BEGIN
         SELECT organization_code, calendar_code, calendar_exception_set_id
           INTO l_organization_code,
                l_calendar_code,
                l_calendar_exception_set_id
           FROM mtl_parameters
          WHERE organization_id = p_organization_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_error_flag := 1;
            l_error_msg :=
               SUBSTR (
                     'Failed to get Org Details for '
                  || p_organization_id
                  || ' '
                  || SQLERRM,
                  1,
                  240);
      END;

      l_sec := 'Get History Start Date ';

      l_history_start_date :=
         XXWC_MTL_DEMAND_FORECAST_PKG.Get_History_Start_Date (
            l_calendar_code,
            l_start_date,
            p_bucket_type,
            p_history_periods);

      IF l_history_start_date IS NULL
      THEN
         l_error_flag := 1;
         l_error_msg :=
            SUBSTR (
                  'Failed to get history_start_date for '
               || l_organization_code
               || ' '
               || SQLERRM,
               1,
               240);
      END IF;

      l_sec := 'Get Forecast Start Date ';

      l_forecast_start_date :=
         MRP_CALENDAR.Prev_Work_Day (p_organization_id, 1, l_start_date);

      IF l_forecast_start_date IS NULL
      THEN
         l_error_flag := 1;
         l_error_msg :=
            SUBSTR (
                  'Failed to get forecast_start_date for '
               || l_organization_code
               || ' '
               || SQLERRM,
               1,
               240);
      END IF;

      l_sec := 'Correct the No. of forecast Periods ';

      IF p_forecast_periods > 12
      THEN
         l_forecast_periods := 12;
      ELSIF p_forecast_periods < 1
      THEN
         l_forecast_periods := 1;
      ELSE
         l_forecast_periods := p_forecast_periods;
      END IF;

      l_sec := 'Correct the No. of history Periods ';

      IF p_history_periods < 1
      THEN
         l_history_periods := 1;
      ELSE
         l_history_periods := p_history_periods;
      END IF;

      l_sec := 'Get Item Number';

      IF p_inventory_item_id IS NOT NULL
      THEN
         BEGIN
            SELECT segment1
              INTO l_item_number
              FROM mtl_system_items_b
             WHERE     inventory_item_id = p_inventory_item_id
                   AND organization_id = p_organization_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_error_flag := 1;
               l_error_msg :=
                  SUBSTR (
                        'Failed to get Inventory Item Id for Item Id'
                     || p_inventory_item_id
                     || ' in Branch '
                     || l_organization_code
                     || ' '
                     || SQLERRM,
                     1,
                     240);
         END;
      END IF;

      l_sec := 'Get Category Id ';

      IF p_item_category IS NOT NULL
      THEN
         BEGIN
            SELECT mc.category_id
              INTO l_category_id
              FROM mtl_categories_kfv mc, mtl_category_sets mcs
             WHERE     mcs.structure_id = mc.structure_id
                   AND mcs.category_set_name = 'Inventory Category'
                   AND mc.concatenated_segments = p_item_category;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_error_flag := 1;
               l_error_msg :=
                  SUBSTR (
                        'Failed to get Category Id for category '
                     || p_item_category
                     || ' in Branch '
                     || l_organization_code
                     || ' '
                     || SQLERRM,
                     1,
                     240);
         END;
      END IF;

      IF l_error_flag = 0
      THEN
	  
        l_sec := 'Delete From XXWC.XXWC_MTL_FORECAST_PARAM_TBL ';

         DELETE FROM XXWC.XXWC_MTL_FORECAST_PARAM_TBL
               WHERE     organization_id = p_organization_id
                     AND forecast_designator = p_forecast_designator;

       l_sec := 'Insert into XXWC.XXWC_MTL_FORECAST_PARAM_TBL ';
	  
         INSERT
           INTO XXWC.XXWC_MTL_FORECAST_PARAM_TBL (ORGANIZATION_ID,
                                                  ORGANIZATION_CODE,
                                                  FORECAST_DESIGNATOR,
                                                  CALENDAR_CODE,
                                                  CALENDAR_EXCEPTION_SET_ID,
                                                  BRANCH_DC_MODE,
                                                  ITEM_RANGE,
                                                  CATEGORY_ID,
                                                  ITEM_CATEGORY,
                                                  INVENTORY_ITEM_ID,
                                                  ITEM_NUMBER,
                                                  BUCKET_TYPE,
                                                  START_DATE,
                                                  FORECAST_START_DATE,
                                                  HISTORY_START_DATE,
                                                  NUM_OF_FORECAST_PERIODS,
                                                  NUM_OF_PREVIOUS_PERIODS,
                                                  CONSTANT_SEASONALITY,
                                                  SEASONALITY_1,
                                                  SEASONALITY_2,
                                                  SEASONALITY_3,
                                                  SEASONALITY_4,
                                                  SEASONALITY_5,
                                                  SEASONALITY_6,
                                                  SEASONALITY_7,
                                                  SEASONALITY_8,
                                                  SEASONALITY_9,
                                                  SEASONALITY_10,
                                                  SEASONALITY_11,
                                                  SEASONALITY_12,
                                                  INSERT_DATE)
         VALUES (p_organization_id,
                 l_organization_code,
                 p_forecast_designator,
                 l_calendar_code,
                 l_calendar_exception_set_id,
                 p_branch_dc_mode,
                 p_item_range,
                 l_category_id,
                 p_item_category,
                 p_inventory_item_id,
                 l_item_number,
                 p_bucket_type,
                 l_start_date,
                 l_forecast_start_date,
                 l_history_start_date,
                 l_forecast_periods,
                 l_history_periods,
                 p_constant_seasonality,
                 p_seasonality_1,
                 p_seasonality_2,
                 p_seasonality_3,
                 p_seasonality_4,
                 p_seasonality_5,
                 p_seasonality_6,
                 p_seasonality_7,
                 p_seasonality_8,
                 p_seasonality_9,
                 p_seasonality_10,
                 p_seasonality_11,
                 p_seasonality_12,
                 SYSDATE);

         COMMIT;

         l_sec :=
            'Submit XXWC MTL Generate Demand Forecast Program - All Orgs';

         l_request_id :=
            fnd_request.submit_request ('XXWC',
                                        l_program_name,
                                        NULL,
                                        NULL,
                                        FALSE);

         IF l_request_id <= 0
         THEN
            l_error_flag := 1;
            l_retcode := 2;
            l_error_msg :=
                  'Failed to submit XXWC MTL Generate Demand Forecast Program - All Orgs : '
               || SQLERRM;
         END IF;
      END IF;

      IF l_error_flag = 1
      THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_GENERATE_FORECAST_ADI_PKG.Submit_forecast',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          DBMS_UTILITY.format_error_stack ()
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => l_error_msg,
            p_distribution_list   => l_distribution_list,
            p_module              => 'INV');
         COMMIT;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         l_error_msg :=
            SUBSTR (
                  'Failed to Load the data for Branch '
               || l_organization_code
               || ' '
               || SQLERRM,
               1,
               240);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_GENERATE_FORECAST_ADI_PKG.Submit_forecast',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          DBMS_UTILITY.format_error_stack ()
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => l_error_msg,
            p_distribution_list   => l_distribution_list,
            p_module              => 'INV');
         COMMIT;

         retcode := 2;
         errbuf := l_error_msg;
   END Submit_forecast;

   PROCEDURE create_drop_indexes (p_event IN VARCHAR2)
   IS
      /*************************************************************************
      $Header XXWC_MTL_DEMAND_FORECAST_PKG.Generate_forecast_main $
      Module Name: create_drop_indexes
      PURPOSE: Procedure to DROP and CREATING the indexes
      REVISIONS:
      Ver        Date         Author                Description
      ---------  -----------  ------------------    ----------------
      1.1        26-Mar-2017  P.Vamshidhar          TMS#20170228-00077 - Performance issue with XXWC MTL Generate Demand Forecast Program
      **************************************************************************/
      ln_index_count   NUMBER;
   BEGIN
      IF p_event = 'DROP'
      THEN
         FND_FILE.PUT_LINE (FND_FILE.LOG, 'Dropping indexes begin');
         ln_index_count := 0;

         SELECT COUNT (1)
           INTO ln_index_count
           FROM dba_indexes
          WHERE index_name = 'MRP_FORECAST_DATES_N4';

         IF ln_index_count = 1
         THEN
            EXECUTE IMMEDIATE 'DROP INDEX MRP.MRP_FORECAST_DATES_N4';

            FND_FILE.PUT_LINE (FND_FILE.LOG,
                               'MRP.MRP_FORECAST_DATES_N4 Dropped index');
         END IF;

         ln_index_count := 0;

         SELECT COUNT (1)
           INTO ln_index_count
           FROM dba_indexes
          WHERE index_name = 'MRP_FORECAST_DATES_U1';

         IF ln_index_count = 1
         THEN
            EXECUTE IMMEDIATE 'DROP INDEX MRP.MRP_FORECAST_DATES_U1';

            FND_FILE.PUT_LINE (FND_FILE.LOG,
                               'MRP.MRP_FORECAST_DATES_U1 Dropped index');
         END IF;

         FND_FILE.PUT_LINE (FND_FILE.LOG, 'Dropping indexes end');
      END IF;

      IF p_event = 'CREATE'
      THEN
         FND_FILE.PUT_LINE (FND_FILE.LOG, 'Creating indexes begin');
         ln_index_count := 0;

         SELECT COUNT (1)
           INTO ln_index_count
           FROM dba_indexes
          WHERE index_name = 'MRP_FORECAST_DATES_N4';

         IF ln_index_count = 0
         THEN
            FND_FILE.PUT_LINE (FND_FILE.LOG,
                               'MRP.MRP_FORECAST_DATES_N4 Creating index');

            EXECUTE IMMEDIATE
               'CREATE INDEX MRP.MRP_FORECAST_DATES_N4 ON MRP.MRP_FORECAST_DATES (ORIGINATION_TYPE)  PARALLEL 8';
			   
			EXECUTE IMMEDIATE 'ALTER INDEX MRP.MRP_FORECAST_DATES_N4 NOPARALLEL';
			
         END IF;

         ln_index_count := 0;

         SELECT COUNT (1)
           INTO ln_index_count
           FROM dba_indexes
          WHERE index_name = 'MRP_FORECAST_DATES_U1';

         IF ln_index_count = 0
         THEN
            FND_FILE.PUT_LINE (FND_FILE.LOG,
                               'MRP.MRP_FORECAST_DATES_U1 Creating index');

            EXECUTE IMMEDIATE
               'CREATE UNIQUE INDEX MRP.MRP_FORECAST_DATES_U1 ON MRP.MRP_FORECAST_DATES (TRANSACTION_ID)  PARALLEL 8';
			   
			EXECUTE IMMEDIATE 'ALTER INDEX MRP.MRP_FORECAST_DATES_U1 NOPARALLEL';   
			
         END IF;

         FND_FILE.PUT_LINE (FND_FILE.LOG, 'Creating indexes end');
      END IF;
   END;
--  <END> -- Ver#1.1   

   
END XXWC_MTL_DEMAND_FORECAST_PKG;
/