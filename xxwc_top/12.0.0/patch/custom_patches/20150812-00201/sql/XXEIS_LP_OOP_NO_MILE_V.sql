CREATE OR REPLACE FORCE VIEW APPS.XXEIS_LP_OOP_NO_MILE_V
   /* ************************************************************************
    HD Supply
    All rights reserved.
    Ticket                    Ver          Date         Author                       Description
    ----------------------    ----------   ----------   ------------------------     -------------------------
    Not available             1.0          08/21/2015   Production version     
    TMS 20150812-00201        1.1          08/21/2015   Balaguru Seshadri            Add oracle_location, creation_date and vendor_name
   ************************************************************************ */
(
   FULL_NAME,
   EMPLOYEE_NUMBER,
   EXPENSE_REPORT_NUMBER,
   TRANSACTION_DATE,
   DOW,
   AMOUNT_SPENT,
   ITEM_DESCRIPTION,
   SUB_CATEGORY,
   MERCHANT_NAME,
   MERCHANT_LOCATION,
   MCC_CODE,
   MCC_CODE_NO,
   JUSTIFICATION,
   BUSINESS_PURPOSE,
   REMARKS,
   ATTENDEES,
   APPROVER_NAME,
   QUERY_DESCR,
   CARD_PROGRAM_NAME,
   CARDMEMBER_NAME,
   ORACLE_PRODUCT,
   ORACLE_COST_CENTER,
   ORACLE_LOCATION, --Ver 1.1
   CREATION_DATE, --Ver 1.1
   VENDOR_NAME, --Ver 1.1
   PERIOD_NAME,
   JOB_FAMILY,
   JOB_FAMILY_DESCR,
   LOB_NAME,
   RPT_GRP
)
AS
   (SELECT BT.full_name,
           BT.employee_number,
           BT.expense_report_number,
           BT.transaction_date,
           TO_CHAR (BT.transaction_date, 'Day') AS DOW,
           TO_CHAR (BT.line_amount, '$999,999,999.99') AS Amount_Spent,
           BT.item_description,
           BT.sub_category,
           BT.merchant_name,
           BT.merchant_location,
           BT.mcc_code,
           BT.mcc_code_no,
           BT.justification,
           BT.business_purpose,
           BT.remarks,
           BT.attendees,
           BT.approver_name,
           BT.query_descr,
           BT.card_program_name,
           BT.cardmember_name,
           BT.oracle_product,
           BT.oracle_cost_center,
		   BT.oracle_location, --Ver 1.1
           BT.creation_date, --Ver 1.1
           BT.vendor_name,  --Ver 1.1		   
           BT.period_name,
           EMPL.JOB_FAMILY,
           EMPL.JOB_FAMILY_DESCR,
           CASE LOB.LOB_NAME
              WHEN 'ELECTRICAL' THEN 'POWER SOLUTIONS'
              WHEN 'UTILITIES' THEN 'POWER SOLUTIONS'
              ELSE LOB.LOB_NAME
           END
              AS LOB_NAME,
           CASE
              WHEN EMPL.JOB_FAMILY = 'E01' THEN 'SLT'
              WHEN EMPL.JOB_FAMILY = 'V02' THEN 'OFFICER'
              WHEN LOB.LOB_NAME = 'ELECTRICAL' THEN 'POWER SOLUTIONS'
              WHEN LOB.LOB_NAME = 'UTILITIES' THEN 'POWER SOLUTIONS'
              ELSE LOB.LOB_NAME
           END
              AS RPT_GRP
      FROM XXCUS.Xxcus_Bullet_Iexp_Tbl BT,
           XXCUS.XXCUSHR_PS_EMP_ALL_TBL EMPL,
           APPS.AP_CREDIT_CARD_TRXNS_ALL CC,
           APPS.XXEIS_LOB_NAMES_V LOB
     WHERE     BT.query_num IN ('4', '6')
           AND BT.EMPLOYEE_NUMBER = EMPL.EMPLOYEE_NUMBER
           AND BT.CREDIT_CARD_TRX_ID = CC.TRX_ID(+)
           AND BT.ORACLE_PRODUCT = LOB.ORACLE_PRODUCT
           AND (   UPPER (CC.CATEGORY) NOT IN ('PERSONAL')
                OR CC.CATEGORY IS NULL)
           AND (UPPER (BT.sub_category) NOT LIKE ('%MILEAGE%'))
           AND (UPPER (BT.sub_category) NOT LIKE ('%TIPS%'))
           AND (UPPER (BT.sub_category) NOT LIKE ('%TOLLS%'))
           AND (UPPER (BT.sub_category) NOT LIKE ('%TELEPHONE%'))
           AND (UPPER (BT.sub_category) NOT LIKE ('%PARKING%'))
           AND (UPPER (BT.item_description) NOT LIKE ('%MILEAGE%'))
           AND (UPPER (BT.item_description) NOT LIKE ('%TIPS%'))
           AND (UPPER (BT.item_description) NOT LIKE ('%TOLLS%'))
           AND (UPPER (BT.item_description) NOT LIKE ('%TELEPHONE%'))
           AND (UPPER (BT.item_description) NOT LIKE ('%PARKING%')))
   --AND BT.PERIOD_NAME = 'Oct-2012'
   --AND BT.EMPLOYEE_NUMBER = '43635'
   --AND upper(BT.full_name) LIKE 'DOMANICO%'
   ORDER BY                                                         --RPT_GRP,
                                                                --MCC_CODE_NO,
    BT.full_name, BT.line_amount DESC;
--
COMMENT ON TABLE APPS.XXEIS_LP_OOP_NO_MILE_V IS 'TMS 20150812-00201 / ESMS 288693';
--