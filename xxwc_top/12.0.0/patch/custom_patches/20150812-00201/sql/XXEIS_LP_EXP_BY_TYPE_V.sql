CREATE OR REPLACE FORCE VIEW APPS.XXEIS_LP_EXP_BY_TYPE_V
   /* ************************************************************************
    HD Supply
    All rights reserved.
    Ticket                    Ver          Date         Author                       Description
    ----------------------    ----------   ----------   ------------------------     -------------------------
    Not available             1.0          08/21/2015   Production version     
    TMS 20150812-00201        1.1          08/21/2015   Balaguru Seshadri            Add oracle_location
   ************************************************************************ */
(
   RPT_GRP,
   SUB_CATEGORY,
   AMOUNT_SPENT,
   PERIOD_NAME,
   ORACLE_LOCATION --Ver 1.1
)
AS
   (  SELECT RPT_GRP,
             SUB_CATEGORY,
             TO_CHAR (SUM (AMOUNT_SPENT), '$999,999,999.99') AMOUNT_SPENT,
             PERIOD_NAME,
			 ORACLE_LOCATION --Ver 1.1
        FROM (SELECT BT.line_amount AS AMOUNT_SPENT,
                     BT.sub_category,
                     BT.EMPLOYEE_NUMBER,
                     BT.FULL_NAME,
                     BT.PERIOD_NAME,
                     BT.ORACLE_COST_CENTER,
					 BT.ORACLE_LOCATION, --Ver 1.1
                     EMPL.JOB_FAMILY,
                     EMPL.JOB_FAMILY_DESCR,
                     CASE LOB.LOB_NAME
                        WHEN 'ELECTRICAL' THEN 'POWER SOLUTIONS'
                        WHEN 'UTILITIES' THEN 'POWER SOLUTIONS'
                        ELSE LOB.LOB_NAME
                     END
                        AS LOB_NAME,
                     CASE
                        WHEN EMPL.JOB_FAMILY = 'E01' THEN 'SLT'
                        WHEN EMPL.JOB_FAMILY = 'V02' THEN 'OFFICER'
                        WHEN LOB.LOB_NAME = 'ELECTRICAL' THEN 'POWER SOLUTIONS'
                        WHEN LOB.LOB_NAME = 'UTILITIES' THEN 'POWER SOLUTIONS'
                        ELSE LOB.LOB_NAME
                     END
                        AS RPT_GRP
                FROM XXCUS.XXCUS_BULLET_IEXP_TBL BT,
                     APPS.AP_CREDIT_CARD_TRXNS_ALL AP,
                     XXCUS.XXCUSHR_PS_EMP_ALL_TBL EMPL,
                     APPS.XXEIS_LOB_NAMES_V LOB
               WHERE     BT.CREDIT_CARD_TRX_ID = AP.TRX_ID(+)
                     AND BT.EMPLOYEE_NUMBER = EMPL.EMPLOYEE_NUMBER(+)
                     AND BT.ORACLE_PRODUCT = LOB.ORACLE_PRODUCT(+)
                     AND BT.QUERY_NUM IN ('1', '4', '6')) rsl
    GROUP BY SUB_CATEGORY, PERIOD_NAME, RPT_GRP
	,ORACLE_LOCATION --Ver 1.1
	)
   ORDER BY AMOUNT_SPENT DESC, RPT_GRP;
--
COMMENT ON TABLE APPS.XXEIS_LP_EXP_BY_TYPE_V IS 'TMS 20150812-00201 / ESMS 288693';
--