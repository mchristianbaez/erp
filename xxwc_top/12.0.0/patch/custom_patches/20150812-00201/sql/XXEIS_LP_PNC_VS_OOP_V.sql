CREATE OR REPLACE FORCE VIEW APPS.XXEIS_LP_PNC_VS_OOP_V
   /* ************************************************************************
    HD Supply
    All rights reserved.
    Ticket                    Ver          Date         Author                       Description
    ----------------------    ----------   ----------   ------------------------     -------------------------
    Not available             1.0          08/21/2015   Production version     
    TMS 20150812-00201        1.1          08/21/2015   Balaguru Seshadri            Add oracle_location
   ************************************************************************ */
(
   RPT_GRP,
   FULL_NAME,
   EMPLOYEE_NUMBER,
   PERIOD_NAME,
   ORACLE_LOCATION, --Ver 1.1
   OOP_AMOUNT,
   PNC_AMOUNT
)
AS
   (  SELECT RPT_GRP,
             FULL_NAME,
             EMPLOYEE_NUMBER,
             PERIOD_NAME,
			 ORACLE_LOCATION, --Ver 1.1
             SUM (OOP_AMOUNT) OOP_AMOUNT,
             --to_char(SUM(OOP_AMOUNT), '$999,999,990.99') OOP_AMOUNT,
             TO_CHAR (SUM (PNC_AMOUNT), '$999,999,990.99') PNC_AMOUNT
        FROM (SELECT CASE
                        WHEN EMPL.JOB_FAMILY = 'E01' THEN 'SLT'
                        WHEN EMPL.JOB_FAMILY = 'V02' THEN 'OFFICER'
                        WHEN LOB.LOB_NAME = 'ELECTRICAL' THEN 'POWER SOLUTIONS'
                        WHEN LOB.LOB_NAME = 'UTILITIES' THEN 'POWER SOLUTIONS'
                        ELSE LOB.LOB_NAME
                     END
                        AS RPT_GRP,
                     CASE LOB.LOB_NAME
                        WHEN 'ELECTRICAL' THEN 'POWER SOLUTIONS'
                        WHEN 'UTILITIES' THEN 'POWER SOLUTIONS'
                        ELSE LOB.LOB_NAME
                     END
                        AS LOB_NAME,
                     BT.full_name,
                     BT.employee_number,
                     BT.PERIOD_NAME,
					 BT.ORACLE_LOCATION, -- Ver 1.1
                     CASE query_num WHEN '1' THEN BT.line_amount ELSE 0 END
                        PNC_AMOUNT,
                     CASE query_num
                        WHEN '4' THEN BT.line_amount
                        WHEN '6' THEN BT.line_amount
                        ELSE 0
                     END
                        OOP_AMOUNT
                FROM XXEIS.XXCUS_BULLET_IEXP_TBL BT,
                     XXCUS.XXCUSHR_PS_EMP_ALL_TBL EMPL,
                     APPS.XXEIS_LOB_NAMES_V LOB
               WHERE     BT.EMPLOYEE_NUMBER = EMPL.EMPLOYEE_NUMBER(+)
                     AND BT.ORACLE_PRODUCT = LOB.ORACLE_PRODUCT(+)
                     AND query_num IN ('1', '4', '6')
                     AND UPPER (item_description) NOT LIKE '%MILEAGE%') rsl
    GROUP BY RPT_GRP,
             FULL_NAME,
             EMPLOYEE_NUMBER,
			 ORACLE_LOCATION, --Ver 1.1
             PERIOD_NAME)
   ORDER BY OOP_AMOUNT DESC;
--
COMMENT ON TABLE APPS.XXEIS_LP_PNC_VS_OOP_V IS 'TMS 20150812-00201 / ESMS 288693';
--