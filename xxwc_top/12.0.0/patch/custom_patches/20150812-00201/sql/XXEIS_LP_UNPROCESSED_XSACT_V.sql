CREATE OR REPLACE FORCE VIEW APPS.XXEIS_LP_UNPROCESSED_XSACT_V
   /* ************************************************************************
    HD Supply
    All rights reserved.
    Ticket                    Ver          Date         Author                       Description
    ----------------------    ----------   ----------   ------------------------     -------------------------
    Not available             1.0          08/21/2015   Production version     
    TMS 20150812-00201        1.1          08/21/2015   Balaguru Seshadri            Add oracle_location
   ************************************************************************ */
(
   RPT_GRP,
   ORACLE_PRODUCT,
   ORACLE_LOCATION, --Ver 1.1
   CARD_PROGRAM_NAME,
   FULL_NAME,
   EMPLOYEE_NUMBER,
   TOTAL_OUTSTANDING_AMOUNT,
   NUM_OF_OUTSTANDING_TRANS
)
AS
   (SELECT CASE
              WHEN EMPL.JOB_FAMILY = 'E01' THEN 'SLT'
              WHEN EMPL.JOB_FAMILY = 'V02' THEN 'OFFICER'
              WHEN LOB.LOB_NAME = 'ELECTRICAL' THEN 'POWER SOLUTIONS'
              WHEN LOB.LOB_NAME = 'UTILITIES' THEN 'POWER SOLUTIONS'
              ELSE LOB.LOB_NAME
           END
              AS RPT_GRP,
           XBT.oracle_product,
		   XBT.ORACLE_LOCATION, --Ver 1.1
           XBT.card_program_name,
           XBT.Full_name,
           XBT.employee_number,
           TO_CHAR (XBT.Total_Outstanding_Amount, '$999,999,999.99')
              AS Total_Outstanding_Amount,
           XBT.Num_Of_Outstanding_Trans
      FROM (  SELECT oracle_product,
	                 oracle_location, --Ver 1.1
                     card_program_name,
                     Full_name,
                     employee_number,
                     SUM (Line_Amount) AS Total_Outstanding_Amount,
                     COUNT (*) AS Num_Of_Outstanding_Trans
                FROM XXEIS.XXCUS_BULLET_IEXP_TBL XBIT
               WHERE     query_num IN ('2', '3', '5')
                     AND (   card_program_name LIKE
                                ('WC PNC Inventory Card Program')
                          OR card_program_name LIKE ('HDS Company Pay Program'))
                     AND (TRUNC (SYSDATE, 'J') - TRUNC (posted_date, 'J') > 60)
            GROUP BY oracle_product,
                     oracle_location, --Ver 1.1
                     Card_Program_Name,
                     Full_Name,
                     employee_number
            UNION
              --Employee Pay Cards--
              SELECT oracle_product,
                     oracle_location, --Ver 1.1			  
                     card_program_name,
                     Full_name,
                     employee_number,
                     SUM (Line_Amount) AS Total_Outstanding_Amount,
                     COUNT (*) AS Num_Of_Outstanding_Trans
                FROM XXEIS.XXCUS_BULLET_IEXP_TBL XBIT
               WHERE     query_num IN ('2', '3')
                     AND (   card_program_name LIKE
                                ('CB PNC Employee-Pay Card Program')
                          OR card_program_name LIKE
                                ('FM PNC Employee-Pay Card Program')
                          OR card_program_name LIKE
                                ('WW PNC Employee-Pay Card Program'))
                     AND (TRUNC (SYSDATE, 'J') - TRUNC (posted_date, 'J') > 90)
            GROUP BY oracle_product,
			         oracle_location, --Ver 1.1
                     Card_Program_Name,
                     Full_Name,
                     employee_number) XBT,
           XXCUS.XXCUSHR_PS_EMP_ALL_TBL EMPL,
           APPS.XXEIS_LOB_NAMES_V LOB
     WHERE     XBT.EMPLOYEE_NUMBER = EMPL.EMPLOYEE_NUMBER
           AND XBT.ORACLE_PRODUCT = LOB.ORACLE_PRODUCT)
   ORDER BY XBT.Num_Of_Outstanding_Trans DESC;
--
COMMENT ON TABLE APPS.XXEIS_LP_UNPROCESSED_XSACT_V IS 'TMS 20150812-00201 / ESMS 288693';
--