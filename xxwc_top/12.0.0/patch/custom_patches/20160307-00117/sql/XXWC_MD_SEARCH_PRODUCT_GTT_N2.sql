  /*******************************************************************************
  Index: XXWC_MD_SEARCH_PRODUCT_GTT_N2 
  Description: This index is used on XXWC_MD_SEARCH_PRODUCT_GTT_TBL table 
  and is used on AIS form
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     01-Mar-2016        Pahwa Nancy   TMS# 20160307-00117  Performance Tuning
  ********************************************************************************/
DROP INDEX XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_N2;
CREATE INDEX XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_N2 ON XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL
(SEQUENCE);
/