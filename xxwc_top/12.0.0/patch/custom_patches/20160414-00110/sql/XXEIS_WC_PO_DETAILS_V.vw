/* ***************************************************************
$Header XXEIS.XXEIS_WC_PO_DETAILS_V.vw $
Module Name: Purchasing
PURPOSE: Purchase Order Detail By Received PO - WC
TMS Task Id :
REVISIONS:
Ver        Date        Author                Description
---------  ----------  ------------------    ----------------
1.0        NA          NA                     Initial Version
1.1  07/15/2014        Mahender Reddy         TMS#20140407-00181
1.2  07/15/2014        Mahender Reddy         TMS#20150810-00083
1.3  11/02/2015        Mahender Reddy         TMS#20150929-00055
1.4  25-May-2016         Siva                 TMS#20160414-00110   Performance Tuning
****************************************************************/
DROP VIEW XXEIS.XXEIS_WC_PO_DETAILS_V;

CREATE OR REPLACE VIEW XXEIS.XXEIS_WC_PO_DETAILS_V (PO_HEADER_ID, PO_LAST_UPDATE_DATE, PO_LAST_UPDATED_BY, PO_CREATION_DATE, PO_CREATED_BY, PO_BUYER_NAME, PO_NUMBER, PO_TYPE, PO_SUPPLIER_NAME, PO_SUPPLIER_SITE_CODE, SUPPLIER_CONTACT_FIRST_NAME, SUPPLIER_CONTACT_LAST_NAME, SUPPLIER_CONTACT_AREA_CODE, SUPPLIER_CONTACT_PHONE, PVC_VENDOR_CONTACT_ID, PO_SUMMARY, PO_ENABLED, PO_START_DATE, PO_END_DATE, PO_SHIP_TO_LOCATION, PO_TERMS, PO_SHIP_VIA, PO_FOB, PO_FREIGHT_TERMS, PO_STATUS, PO_REVISION_NUM, PO_REVISED_DATE, PO_PRINTED_DATE, PO_APPROVED_DATE, PO_CURRENCY_CODE, PO_ACCEPTANCE_DUE_DATE, PO_ON_HOLD, PO_ACCEPTANCE_REQUIRED, PO_NOTE_TO_AUTHORIZER, PO_NOTE_TO_SUPPLIER, PO_NOTE_TO_RECEIVER, PO_COMMENTS, PO_CLOSED_CODE, PO_CLOSED_DATE, PO_LINE_ID, POL_LAST_UPDATE_DATE, POL_LAST_UPDATED_BY, POL_CREATION_DATE, POL_CREATED_BY, PO_LINE_TYPE, PO_LINE_NUM, C_FLEX_ITEM, MSI_PADDED_CONCAT_SEGMENTS, PO_ITEM_NUMBER, MSI_SEGMENT2, MSI_SEGMENT3, MSI_SEGMENT4, MSI_SEGMENT5, MSI_SEGMENT6, MSI_SEGMENT7,
  MSI_SEGMENT8, MSI_SEGMENT9, MSI_SEGMENT10, MSI_SEGMENT11, MSI_SEGMENT12, MSI_SEGMENT13, MSI_SEGMENT14, MSI_SEGMENT15, OUTSIDE_OPERATION_UNIT_TYPE, UNIT_OF_ISSUE, PARENT_ID, EXPENSE_ID, PROCESSING_TIME, RECEIPT_TOLERANCE, PRICE_TOLERANCE, INVOICING_CLOSE_TOLERANCE, RECEIVING_CLOSE_TOLERANCE, DAYS_EARLY_RECEIPT_ALLOWED, LIST_PRICE, DAYS_LATE_RECEIPT_ALLOWED, MARKET_PRICE, ROUNDING_FACTOR, ROUTING_STEPS, PO_ITEM_DESCRIPTION, PO_UNIT_PRICE, PO_QUANTITY_ORDERED, PO_QUANTITY_CANCELLED, PO_QUANTITY_RECEIVED, PO_QUANTITY_DUE, PO_QUANTITY_BILLED, LINE_AMOUNT, SHIPMENT_AMOUNT, PO_ITEM_REVISION, PO_UOM, PO_LIST_PRICE, PO_MARKET_PRICE, POL_CANCEL, POL_CANCEL_DATE, POL_CANCEL_REASON, PO_TAXABLE_FLAG, PO_TAX_NAME, PO_TYPE_1099, POL_CLOSED_CODE, POL_CLOSED_DATE, POL_CLOSED_REASON, LINE_LOCATION_ID, PO_SHIPMENT_NUM, PO_SHIPMENT_TYPE, PO_NEED_BY_DATE, PO_PROMISED_DATE, PO_LAST_ACCEPT_DATE, PO_4_WAY_MATCH, PO_3_WAY_MATCH, PO_QTY_RCV_TOLERANCE, PO_DAYS_EARLY_RECEIPT_ALLOWED,
  PO_DAYS_LATE_RECEIPT_ALLOWED, PO_INVOICE_CLOSE_TOLERANCE, PO_RECEIVE_CLOSE_TOLERANCE, PO_ACCRUE_ON_RECEIPT, PO_DISTRIBUTION_ID, PO_DISTRIBUTION_NUM, PO_SET_OF_BOOKS, PO_CHARGE_CCID, PO_CHARGE_ACCOUNT, PO_ACCRUAL_CCID, PO_ACCRUAL_ACCOUNT, PO_VARIANCE_CCID, PO_VARIANCE_ACCOUNT, PROJECT_NAME, PROJECT_NUMBER, HRL_LOCATION_ID, SOB_SET_OF_BOOKS_ID, PROJECT_ID, C_FLEX_CAT, MCA_PADDED_CONCAT_SEGMENTS, MCA_STRUCTURE_ID, MCA_SEGMENT1, MCA_SEGMENT2, MCA_SEGMENT3, MCA_SEGMENT4, MCA_SEGMENT5, MCA_SEGMENT6, MCA_SEGMENT7, MCA_SEGMENT8, MCA_SEGMENT9, MCA_SEGMENT10, MCA_SEGMENT11, MCA_SEGMENT12, MCA_SEGMENT13, MCA_SEGMENT14, MCA_SEGMENT15, STATUS, TYPE, OPERATING_UNIT, ORG_ID, ORG_CODE, VENDOR_ID, AGENT_ID, CODE_COMBINATION_ID, PVS_VENDOR_SITE_ID, PLT_LINE_TYPE_ID, MCA_CATEGORY_ID, MSI_ORGANIZATION_ID, QUANTITY_OVER_BILLED, REQUESTOR_NAME, GCC2#BRANCH, GCC1#BRANCH, GCC#BRANCH, MCA#COGS_ACCOUNT, MCA#SALES_ACCOUNT, MSI#HDS#LOB, MSI#HDS#DROP_SHIPMENT_ELIGAB, MSI#HDS#INVOICE_UOM, MSI#HDS#PRODUCT_ID,
MSI#HDS#VENDOR_PART_NUMBER, MSI#HDS#UNSPSC_CODE, MSI#HDS#UPC_PRIMARY, MSI#HDS#SKU_DESCRIPTION, MSI#WC#CA_PROP_65, MSI#WC#COUNTRY_OF_ORIGIN, MSI#WC#ORM_D_FLAG, MSI#WC#STORE_VELOCITY, MSI#WC#DC_VELOCITY, MSI#WC#YEARLY_STORE_VELOCITY, MSI#WC#YEARLY_DC_VELOCITY, MSI#WC#PRISM_PART_NUMBER, MSI#WC#HAZMAT_DESCRIPTION, MSI#WC#HAZMAT_CONTAINER, MSI#WC#GTP_INDICATOR, MSI#WC#LAST_LEAD_TIME, MSI#WC#AMU, MSI#WC#RESERVE_STOCK, MSI#WC#TAXWARE_CODE, MSI#WC#AVERAGE_UNITS, MSI#WC#PRODUCT_CODE, MSI#WC#IMPORT_DUTY_, MSI#WC#KEEP_ITEM_ACTIVE, MSI#WC#PESTICIDE_FLAG, MSI#WC#CALC_LEAD_TIME, MSI#WC#VOC_GL, MSI#WC#PESTICIDE_FLAG_STATE, MSI#WC#VOC_CATEGORY, MSI#WC#VOC_SUB_CATEGORY, MSI#WC#MSDS_#, MSI#WC#HAZMAT_PACKAGING_GROU, POH#STANDARDP#NEED_BY_DATE, POH#STANDARDP#FREIGHT_TERMS_, POH#STANDARDP#CARRIER_TERMS_, POH#STANDARDP#FOB_TERMS_TAB, MCA#1#ACCOUNTINGCATEGORY, MCA#1#ACCOUNTINGCATEGORY_DESC, MCA#3#CODE, MCA#3#CODE_DESC, MCA#4#CODE, MCA#4#CODE_DESC, MCA#5#CODE, MCA#5#CODE_DESC, MCA#6#CODE, MCA#6#CODE_DESC,
MCA#101#CATEGORY, MCA#101#CATEGORY_DESC, MCA#101#CATEGORYCLASS, MCA#101#CATEGORYCLASS_DESC, MCA#201#ITEMCATEGORY, MCA#201#ITEMCATEGORY_DESC, MCA#50136#INTERESTTYPE, MCA#50136#INTERESTTYPE_DESC, MCA#50136#PRIMARYCODE, MCA#50136#PRIMARYCODE_DESC, MCA#50136#SECONDARYCODE, MCA#50136#SECONDARYCODE_DESC, MCA#50152#DUMMY, MCA#50152#DUMMY_DESC, MCA#50153#CARTONGROUP, MCA#50153#CARTONGROUP_DESC, MCA#50168#CLASS, MCA#50168#CLASS_DESC, MCA#50168#SUBCLASS, MCA#50168#SUBCLASS_DESC, MCA#50169#CONTRACTCATEGORY, MCA#50169#CONTRACTCATEGORY_DES, MCA#50190#CLASSIFICATION, MCA#50190#CLASSIFICATION_DESC, MCA#50190#COMMODITY, MCA#50190#COMMODITY_DESC, MCA#50190#COUNTRY, MCA#50190#COUNTRY_DESC, MCA#50190#DEFAULTCATEGORY, MCA#50190#DEFAULTCATEGORY_DESC, MCA#50208#PRODUCT, MCA#50208#PRODUCT_DESC, MCA#50210#PRODUCT, MCA#50210#PRODUCT_DESC, MCA#50229#PRODUCT, MCA#50229#PRODUCT_DESC, MCA#50268#PRODUCT, MCA#50268#PRODUCT_DESC, MCA#50272#SEQUENCEDEPENDENTCLA, MCA#50272#SEQUENCEDEPENDENTCL_,
MCA#50308#GPCCATALOGCATEGORYDE, MCA#50308#GPCCATALOGCATEGORYD_, MCA#50308#GPCCATALOGCATEGORYCO, MCA#50308#GPCCATALOGCATEGORYC_, MCA#50348#SUPPLIERCODE, MCA#50348#SUPPLIERCODE_DESC, MCA#50348#SUPPLIERCLASS, MCA#50348#SUPPLIERCLASS_DESC, MCA#50388#CLIENT, MCA#50388#CLIENT_DESC, MCA#50408#PURCHASEFLAG, MCA#50408#PURCHASEFLAG_DESC, MCA#50409#PRIVATELABEL, MCA#50409#PRIVATELABEL_DESC, MCA#50410#VELOCITY, MCA#50410#VELOCITY_DESC, MSI#101#ITEM, MSI#101#ITEM_DESC, GCC#50328#ACCOUNT, GCC#50328#ACCOUNT#DESCR, GCC#50328#COST_CENTER, GCC#50328#COST_CENTER#DESCR, GCC#50328#FURTURE_USE, GCC#50328#FURTURE_USE#DESCR, GCC#50328#FUTURE_USE_2, GCC#50328#FUTURE_USE_2#DESCR, GCC#50328#LOCATION, GCC#50328#LOCATION#DESCR, GCC#50328#PRODUCT, GCC#50328#PRODUCT#DESCR, GCC#50328#PROJECT_CODE, GCC#50328#PROJECT_CODE#DESCR, GCC1#50328#ACCOUNT, GCC1#50328#ACCOUNT#DESCR, GCC1#50328#COST_CENTER, GCC1#50328#COST_CENTER#DESCR, GCC1#50328#FURTURE_USE, GCC1#50328#FURTURE_USE#DESCR, GCC1#50328#FUTURE_USE_2,
GCC1#50328#FUTURE_USE_2#DESCR, GCC1#50328#LOCATION, GCC1#50328#LOCATION#DESCR, GCC1#50328#PRODUCT, GCC1#50328#PRODUCT#DESCR, GCC1#50328#PROJECT_CODE, GCC1#50328#PROJECT_CODE#DESCR, GCC2#50328#ACCOUNT, GCC2#50328#ACCOUNT#DESCR, GCC2#50328#COST_CENTER, GCC2#50328#COST_CENTER#DESCR, GCC2#50328#FURTURE_USE, GCC2#50328#FURTURE_USE#DESCR, GCC2#50328#FUTURE_USE_2, GCC2#50328#FUTURE_USE_2#DESCR, GCC2#50328#LOCATION, GCC2#50328#LOCATION#DESCR, GCC2#50328#PRODUCT, GCC2#50328#PRODUCT#DESCR, GCC2#50328#PROJECT_CODE, GCC2#50328#PROJECT_CODE#DESCR, GCC#50368#ACCOUNT, GCC#50368#ACCOUNT#DESCR, GCC#50368#DEPARTMENT, GCC#50368#DEPARTMENT#DESCR, GCC#50368#DIVISION, GCC#50368#DIVISION#DESCR, GCC#50368#FUTURE_USE, GCC#50368#FUTURE_USE#DESCR, GCC#50368#PRODUCT, GCC#50368#PRODUCT#DESCR, GCC#50368#SUBACCOUNT, GCC#50368#SUBACCOUNT#DESCR, GCC1#50368#ACCOUNT, GCC1#50368#ACCOUNT#DESCR, GCC1#50368#DEPARTMENT, GCC1#50368#DEPARTMENT#DESCR, GCC1#50368#DIVISION, GCC1#50368#DIVISION#DESCR, GCC1#50368#FUTURE_USE,
GCC1#50368#FUTURE_USE#DESCR, GCC1#50368#PRODUCT, GCC1#50368#PRODUCT#DESCR, GCC1#50368#SUBACCOUNT, GCC1#50368#SUBACCOUNT#DESCR, GCC2#50368#ACCOUNT, GCC2#50368#ACCOUNT#DESCR, GCC2#50368#DEPARTMENT, GCC2#50368#DEPARTMENT#DESCR, GCC2#50368#DIVISION, GCC2#50368#DIVISION#DESCR, GCC2#50368#FUTURE_USE, GCC2#50368#FUTURE_USE#DESCR, GCC2#50368#PRODUCT, GCC2#50368#PRODUCT#DESCR, GCC2#50368#SUBACCOUNT, GCC2#50368#SUBACCOUNT#DESCR, EDI, ACCEPTED_Y_N, CHANGES_REQUESTED_Y_N, NOTE_TO_RECEIVER, RECEIVING_REASON, RECEIVER_NAME, RECEIPT_NUM, RECEIPT_DATE, ACCEPTANCE_TYPE, ACCEPTED_BY, CONF_BFR_PO_REC)
AS
  SELECT 
	--DISTINCT  --commented for version 1.4
	poh.po_header_id ,  
    poh.last_update_date po_last_update_date ,
    poh.last_updated_by po_last_updated_by ,
    TRUNC (poh.creation_date) po_creation_date ,
    poh.created_by po_created_by ,
    poav.agent_name po_buyer_name ,
    poh.segment1 po_number ,
    poh.type_lookup_code po_type ,
    pv.vendor_name po_supplier_name ,
    pvs.vendor_site_code po_supplier_site_code ,
    pvc.first_name supplier_contact_first_name ,
    pvc.last_name supplier_contact_last_name ,
    pvc.area_code supplier_contact_area_code ,
    pvc.phone supplier_contact_phone ,
    pvc.vendor_contact_id pvc_vendor_contact_id ,
    DECODE (poh.summary_flag, 'Y', 'Yes', 'N', 'No') po_summary ,
    DECODE (poh.enabled_flag, 'Y', 'Yes', 'N', 'No') po_enabled ,
    poh.start_date_active po_start_date ,
    poh.end_date_active po_end_date ,
    hrl.location_code po_ship_to_location ,
    apt.name po_terms ,
    poh.ship_via_lookup_code po_ship_via ,
    poh.fob_lookup_code po_fob ,
    poh.freight_terms_lookup_code po_freight_terms ,
    poh.authorization_status po_status ,
    poh.revision_num po_revision_num ,
    poh.revised_date po_revised_date ,
    poh.printed_date po_printed_date ,
    poh.approved_date po_approved_date ,
    poh.currency_code po_currency_code ,
    poh.acceptance_due_date po_acceptance_due_date ,
    DECODE (poh.user_hold_flag, 'Y', 'Yes', 'N', 'No', NULL) po_on_hold ,
    DECODE (poh.acceptance_required_flag, 'Y', 'Yes', 'N', 'No', NULL) po_acceptance_required ,
    poh.note_to_authorizer po_note_to_authorizer ,
    poh.note_to_vendor po_note_to_supplier ,
    poh.note_to_receiver po_note_to_receiver ,
    poh.comments po_comments ,
    poh.closed_code po_closed_code ,
    poh.closed_date po_closed_date ,
    pol.po_line_id ,
    pol.last_update_date pol_last_update_date ,
    pol.last_updated_by pol_last_updated_by ,
    pol.creation_date pol_creation_date ,
    pol.created_by pol_created_by ,
    plt.line_type po_line_type ,
    pol.line_num po_line_num ,
    msi.concatenated_segments c_flex_item ,
    msi.padded_concatenated_segments msi_padded_concat_segments ,
    msi.segment1 po_item_number ,
    msi.segment2 msi_segment2 ,
    msi.segment3 msi_segment3 ,
    msi.segment4 msi_segment4 ,
    msi.segment5 msi_segment5 ,
    msi.segment6 msi_segment6 ,
    msi.segment7 msi_segment7 ,
    msi.segment8 msi_segment8 ,
    msi.segment9 msi_segment9 ,
    msi.segment10 msi_segment10 ,
    msi.segment11 msi_segment11 ,
    msi.segment12 msi_segment12 ,
    msi.segment13 msi_segment13 ,
    msi.segment14 msi_segment14 ,
    msi.segment15 msi_segment15 ,
    msi.outside_operation_uom_type outside_operation_unit_type ,
    msi.unit_of_issue unit_of_issue ,
    msi.inventory_item_id parent_id ,
    msi.expense_account expense_id ,
    msi.full_lead_time processing_time ,
    msi.qty_rcv_tolerance receipt_tolerance ,
    msi.price_tolerance_percent price_tolerance ,
    msi.invoice_close_tolerance invoicing_close_tolerance ,
    msi.receive_close_tolerance receiving_close_tolerance ,
    msi.days_early_receipt_allowed days_early_receipt_allowed ,
    msi.list_price_per_unit list_price ,
    msi.days_late_receipt_allowed days_late_receipt_allowed ,
    msi.market_price market_price ,
    msi.rounding_factor rounding_factor ,
    msi.receiving_routing_id routing_steps ,
    REPLACE (pol.item_description, '~') po_item_description ,
    TO_NUMBER ( DECODE (plt.order_type_lookup_code, 'AMOUNT', 1, NVL (poll.price_override, pol.unit_price))) po_unit_price ,
    ROUND ( DECODE (plt.order_type_lookup_code ,'RATE', pod.amount_ordered ,'FIXED PRICE', pod.amount_ordered ,pod.quantity_ordered) ,xxeis.eis_rs_po_fin_com_util_pkg.get_qty_precision) po_quantity_ordered ,
    ROUND ( DECODE (plt.order_type_lookup_code ,'RATE', pod.amount_cancelled ,'FIXED PRICE', pod.amount_cancelled ,pod.quantity_cancelled) ,xxeis.eis_rs_po_fin_com_util_pkg.get_qty_precision) po_quantity_cancelled ,
    ROUND ( DECODE (plt.order_type_lookup_code ,'RATE', pod.amount_delivered ,'FIXED PRICE', pod.amount_delivered ,pod.quantity_delivered) ,xxeis.eis_rs_po_fin_com_util_pkg.get_qty_precision) po_quantity_received ,
    ROUND ( ( (DECODE (plt.order_type_lookup_code ,'RATE', pod.amount_ordered ,'FIXED PRICE', pod.amount_ordered ,pod.quantity_ordered)) - (DECODE (plt.order_type_lookup_code ,'RATE', pod.amount_cancelled ,'FIXED PRICE', pod.amount_cancelled ,pod.quantity_cancelled)) - (DECODE (plt.order_type_lookup_code ,'RATE', pod.amount_delivered ,'FIXED PRICE', pod.amount_delivered ,pod.quantity_delivered))) ,xxeis.eis_rs_po_fin_com_util_pkg.get_qty_precision) po_quantity_due ,
    ROUND ( DECODE (plt.order_type_lookup_code ,'RATE', pod.amount_billed ,'FIXED PRICE', pod.amount_billed ,pod.quantity_billed) ,xxeis.eis_rs_po_fin_com_util_pkg.get_qty_precision) po_quantity_billed ,
    DECODE (plt.order_type_lookup_code ,'RATE', pol.amount ,'FIXED PRICE', pol.amount , (pol.quantity) * NVL (pol.base_unit_price, pol.unit_price)) line_amount ,
    DECODE (plt.order_type_lookup_code ,'RATE', poll.amount                                            - NVL (poll.amount_cancelled, 0) ,'FIXED PRICE', poll.amount - NVL (poll.amount_cancelled, 0) , (poll.price_override * (poll.quantity - NVL (poll.quantity_cancelled, 0)))) shipment_amount ,
    pol.item_revision po_item_revision ,
    pol.unit_meas_lookup_code po_uom ,
    pol.list_price_per_unit po_list_price ,
    pol.market_price po_market_price ,
    pol.cancel_flag pol_cancel ,
    pol.cancel_date pol_cancel_date ,
    pol.cancel_reason pol_cancel_reason ,
    NVL (pol.taxable_flag, poll.taxable_flag) po_taxable_flag ,
    pol.tax_name po_tax_name ,
    pol.type_1099 po_type_1099 ,
    pol.closed_code pol_closed_code ,
    pol.closed_date pol_closed_date ,
    pol.closed_reason pol_closed_reason ,
    poll.line_location_id ,
    poll.shipment_num po_shipment_num ,
    poll.shipment_type po_shipment_type ,
    poll.need_by_date po_need_by_date ,
    poll.promised_date po_promised_date
    -- ,poll.last_accept_date po_last_accept_date --Commented for Ver 1.3
    ,
    --    (SELECT MAX(ACTION_DATE)
    --    FROM PO_ACCEPTANCES
    --    WHERE po_header_id = poh.po_header_id
    --    ) po_last_accept_date --Added for Ver 1.3
    --    , --commented for version 1.4
    TO_DATE(XXEIS.EIS_PO_XXWC_PUR_DET_REC_PO_PKG.GET_ACCEPTANCE_DETAILS(POH.PO_HEADER_ID,'ACTION_DATE')) PO_LAST_ACCEPT_DATE --added for version 1.4
    ,
    DECODE (poll.inspection_required_flag, 'Y', 'Yes', 'N', 'No') po_4_way_match ,
    DECODE (poll.receipt_required_flag, 'Y', 'Yes', 'N', 'No') po_3_way_match ,
    poll.qty_rcv_tolerance po_qty_rcv_tolerance ,
    poll.days_early_receipt_allowed po_days_early_receipt_allowed ,
    poll.days_late_receipt_allowed po_days_late_receipt_allowed ,
    poll.invoice_close_tolerance po_invoice_close_tolerance ,
    poll.receive_close_tolerance po_receive_close_tolerance ,
    DECODE (poll.accrue_on_receipt_flag, 'Y', 'Yes', 'N', 'No') po_accrue_on_receipt ,
    pod.po_distribution_id ,
    pod.distribution_num po_distribution_num ,
    sob.name po_set_of_books ,
    gcc.code_combination_id po_charge_ccid ,
    gcc.concatenated_segments po_charge_account ,
    GCC1.CODE_COMBINATION_ID PO_ACCRUAL_CCID ,
    gcc1.concatenated_segments po_accrual_account ,
    GCC2.CODE_COMBINATION_ID PO_VARIANCE_CCID ,
    gcc2.concatenated_segments po_variance_account ,
    pp.name project_name ,
    pp.segment1 project_number , -- Added PK Columns
    hrl.location_id hrl_location_id ,
    sob.set_of_books_id sob_set_of_books_id ,
    pp.project_id , ------- mtl categories kfv data
    mca.concatenated_segments c_flex_cat ,
    mca.padded_concatenated_segments mca_padded_concat_segments ,
    mca.structure_id mca_structure_id ,
    mca.segment1 mca_segment1 ,
    mca.segment2 mca_segment2 ,
    mca.segment3 mca_segment3 ,
    mca.segment4 mca_segment4 ,
    mca.segment5 mca_segment5 ,
    mca.segment6 mca_segment6 ,
    mca.segment7 mca_segment7 ,
    mca.segment8 mca_segment8 ,
    mca.segment9 mca_segment9 ,
    mca.segment10 mca_segment10 ,
    mca.segment11 mca_segment11 ,
    mca.segment12 mca_segment12 ,
    mca.segment13 mca_segment13 ,
    mca.segment14 mca_segment14 ,
    mca.segment15 mca_segment15 ,
    -- plc_sta.displayed_field
    -- || DECODE (poh.cancel_flag, 'Y', ', '
    -- || plc_can.displayed_field, NULL)
    -- || DECODE (NVL (poh.closed_code, 'OPEN'), 'OPEN', NULL, ', '
    -- || plc_clo.displayed_field)
    -- || DECODE (poh.frozen_flag, 'Y', ', '
    -- || plc_fro.displayed_field, NULL)
    -- || DECODE (poh.user_hold_flag, 'Y', ', '
    -- || plc_hld.displayed_field, NULL) status , --commented for version 1.4
    XXEIS.EIS_PO_XXWC_PUR_DET_REC_PO_PKG.GET_authorization_status(NVL(poh.authorization_status,'INCOMPLETE'))
    || DECODE (poh.cancel_flag, 'Y', ', '
    || XXEIS.EIS_PO_XXWC_PUR_DET_REC_PO_PKG.GET_LOOKUP_MEANING('CANCELLED','DOCUMENT STATE'), NULL)
    || DECODE (NVL (poh.closed_code, 'OPEN'), 'OPEN', NULL, ', '
    || XXEIS.EIS_PO_XXWC_PUR_DET_REC_PO_PKG.GET_LOOKUP_MEANING(NVL(POH.CLOSED_CODE,'OPEN'),'DOCUMENT STATE'))
    || DECODE (poh.frozen_flag, 'Y', ', '
    || XXEIS.EIS_PO_XXWC_PUR_DET_REC_PO_PKG.GET_LOOKUP_MEANING('FROZEN','DOCUMENT STATE'), NULL)
    || DECODE (POH.USER_HOLD_FLAG, 'Y', ', '
    || XXEIS.EIS_PO_XXWC_PUR_DET_REC_PO_PKG.GET_LOOKUP_MEANING('ON HOLD','DOCUMENT STATE'), NULL) status , --added for version 1.4
    -- pdt.type_name TYPE , --commented for version 1.4
    XXEIS.EIS_PO_XXWC_PUR_DET_REC_PO_PKG.GET_PO_TYPE(POH.TYPE_LOOKUP_CODE,POH.ORG_ID) TYPE , --added for version 1.4
    hou.name operating_unit ,
    hou.organization_id org_id ,
    ood.organization_code org_code --Added for TMS#20140407-00181 by Mahender on 15/07/14
    ,
    pv.vendor_id ,
    poav.agent_id ,
    gcc.code_combination_id ,
    pvs.vendor_site_id pvs_vendor_site_id ,
    plt.line_type_id plt_line_type_id ,
    mca.category_id mca_category_id ,
    msi.organization_id msi_organization_id ,
    CASE
      WHEN (pod.quantity_billed - pod.quantity_ordered) > 0
      THEN (pod.quantity_billed - pod.quantity_ordered)
    END quantity_over_billed,
    XXEIS.EIS_PO_XXWC_PUR_DET_REC_PO_PKG.GET_EMPLOYEE_NAME(POD.DELIVER_TO_PERSON_ID) requestor_name --added for version 1.4
    --  papf.full_name requestor_name  --commented for version 1.4
    --descr#flexfield#start
    ,
    xxeis.eis_rs_dff.decode_valueset ('XXCUS_FRU', gcc2.attribute1, 'I') gcc2#branch ,
    xxeis.eis_rs_dff.decode_valueset ('XXCUS_FRU', gcc1.attribute1, 'I') gcc1#branch ,
    xxeis.eis_rs_dff.decode_valueset ('XXCUS_FRU', gcc.attribute1, 'I') gcc#branch ,
    mca.attribute1 mca#cogs_account ,
    mca.attribute2 mca#sales_account ,
    DECODE (msi.attribute_category, 'HDS', msi.attribute1, NULL) msi#hds#lob ,
    DECODE (msi.attribute_category ,'HDS', xxeis.eis_rs_dff.decode_valueset ('Yes_No', msi.attribute10, 'F') ,NULL) msi#hds#drop_shipment_eligab ,
    DECODE (msi.attribute_category, 'HDS', msi.attribute15, NULL) msi#hds#invoice_uom ,
    DECODE (msi.attribute_category, 'HDS', msi.attribute2, NULL) msi#hds#product_id ,
    DECODE (msi.attribute_category, 'HDS', msi.attribute3, NULL) msi#hds#vendor_part_number ,
    DECODE (msi.attribute_category, 'HDS', msi.attribute4, NULL) msi#hds#unspsc_code ,
    DECODE (msi.attribute_category, 'HDS', msi.attribute5, NULL) msi#hds#upc_primary ,
    DECODE (msi.attribute_category, 'HDS', msi.attribute6, NULL) msi#hds#sku_description ,
    DECODE (msi.attribute_category, 'WC', msi.attribute1, NULL) msi#wc#ca_prop_65 ,
    DECODE (msi.attribute_category, 'WC', msi.attribute10, NULL) msi#wc#country_of_origin ,
    DECODE (msi.attribute_category ,'WC', xxeis.eis_rs_dff.decode_valueset ('Yes_No', msi.attribute11, 'F') ,NULL) msi#wc#orm_d_flag ,
    DECODE (msi.attribute_category, 'WC', msi.attribute12, NULL) msi#wc#store_velocity ,
    DECODE (msi.attribute_category, 'WC', msi.attribute13, NULL) msi#wc#dc_velocity ,
    DECODE (msi.attribute_category, 'WC', msi.attribute14, NULL) msi#wc#yearly_store_velocity ,
    DECODE (msi.attribute_category, 'WC', msi.attribute15, NULL) msi#wc#yearly_dc_velocity ,
    DECODE (msi.attribute_category, 'WC', msi.attribute16, NULL) msi#wc#prism_part_number ,
    DECODE (msi.attribute_category, 'WC', msi.attribute17, NULL) msi#wc#hazmat_description ,
    DECODE (msi.attribute_category ,'WC', xxeis.eis_rs_dff.decode_valueset ('XXWC HAZMAT CONTAINER', msi.attribute18, 'I') ,NULL) msi#wc#hazmat_container ,
    DECODE (msi.attribute_category, 'WC', msi.attribute19, NULL) msi#wc#gtp_indicator ,
    DECODE (msi.attribute_category, 'WC', msi.attribute2, NULL) msi#wc#last_lead_time ,
    DECODE (msi.attribute_category, 'WC', msi.attribute20, NULL) msi#wc#amu ,
    DECODE (msi.attribute_category, 'WC', msi.attribute21, NULL) msi#wc#reserve_stock ,
    DECODE (msi.attribute_category ,'WC', xxeis.eis_rs_dff.decode_valueset ('XXWC TAXWARE CODE', msi.attribute22, 'I') ,NULL) msi#wc#taxware_code ,
    DECODE (msi.attribute_category, 'WC', msi.attribute25, NULL) msi#wc#average_units ,
    DECODE (msi.attribute_category, 'WC', msi.attribute26, NULL) msi#wc#product_code ,
    DECODE (msi.attribute_category, 'WC', msi.attribute27, NULL) msi#wc#import_duty_ ,
    DECODE (msi.attribute_category ,'WC', xxeis.eis_rs_dff.decode_valueset ('Yes_No', msi.attribute29, 'F') ,NULL) msi#wc#keep_item_active ,
    DECODE (msi.attribute_category ,'WC', xxeis.eis_rs_dff.decode_valueset ('Yes_No', msi.attribute3, 'F') ,NULL) msi#wc#pesticide_flag ,
    DECODE (msi.attribute_category, 'WC', msi.attribute30, NULL) msi#wc#calc_lead_time ,
    DECODE (msi.attribute_category, 'WC', msi.attribute4, NULL) msi#wc#voc_gl ,
    DECODE (msi.attribute_category, 'WC', msi.attribute5, NULL) msi#wc#pesticide_flag_state ,
    DECODE (msi.attribute_category, 'WC', msi.attribute6, NULL) msi#wc#voc_category ,
    DECODE (msi.attribute_category, 'WC', msi.attribute7, NULL) msi#wc#voc_sub_category ,
    DECODE (msi.attribute_category, 'WC', msi.attribute8, NULL) msi#wc#msds_# ,
    DECODE (msi.attribute_category ,'WC', xxeis.eis_rs_dff.decode_valueset ('XXWC_HAZMAT_PACKAGE_GROUP', msi.attribute9, 'I') ,NULL) msi#wc#hazmat_packaging_grou ,
    DECODE (poh.attribute_category, 'Standard Purchase Order', poh.attribute1, NULL) poh#standardp#need_by_date ,
    DECODE (poh.attribute_category, 'Standard Purchase Order', poh.attribute2, NULL) poh#standardp#freight_terms_ ,
    DECODE (poh.attribute_category, 'Standard Purchase Order', poh.attribute3, NULL) poh#standardp#carrier_terms_ ,
    DECODE (poh.attribute_category, 'Standard Purchase Order', poh.attribute4, NULL) poh#standardp#fob_terms_tab
    --descr#flexfield#end
    --kff#start
    ,
    mca.segment1 "MCA#1#ACCOUNTINGCATEGORY" ,
    xxeis.eis_rs_dff.decode_valueset ('AX_INV_ACCOUNTING_CATEGORY', mca.segment1, 'I') "MCA#1#ACCOUNTINGCATEGORY_DESC" ,
    mca.segment1 "MCA#3#CODE" ,
    xxeis.eis_rs_dff.decode_valueset ('30 Characters', mca.segment1, 'N') "MCA#3#CODE_DESC" ,
    mca.segment1 "MCA#4#CODE" ,
    xxeis.eis_rs_dff.decode_valueset ('30 Characters', mca.segment1, 'N') "MCA#4#CODE_DESC" ,
    mca.segment1 "MCA#5#CODE" ,
    xxeis.eis_rs_dff.decode_valueset ('30 Characters', mca.segment1, 'N') "MCA#5#CODE_DESC" ,
    mca.segment1 "MCA#6#CODE" ,
    xxeis.eis_rs_dff.decode_valueset ('30 Characters', mca.segment1, 'N') "MCA#6#CODE_DESC" ,
    mca.segment1 "MCA#101#CATEGORY" ,
    xxeis.eis_rs_dff.decode_valueset ('XXWC_CATEGORY_NUMBER', mca.segment1, 'I') "MCA#101#CATEGORY_DESC" ,
    mca.segment2 "MCA#101#CATEGORYCLASS" ,
    xxeis.eis_rs_dff.decode_valueset ('XXWC_CATEGORY_CLASS', mca.segment2, 'I') "MCA#101#CATEGORYCLASS_DESC" ,
    mca.segment1 "MCA#201#ITEMCATEGORY" ,
    xxeis.eis_rs_dff.decode_valueset ('PO_ITEM_CATEGORY', mca.segment1, 'N') "MCA#201#ITEMCATEGORY_DESC" ,
    mca.segment1 "MCA#50136#INTERESTTYPE" ,
    xxeis.eis_rs_dff.decode_valueset ('AS_INT_TYPES_VS', mca.segment1, 'F') "MCA#50136#INTERESTTYPE_DESC" ,
    mca.segment2 "MCA#50136#PRIMARYCODE" ,
    xxeis.eis_rs_dff.decode_valueset ('AS_PRIM_CODES_VS', mca.segment2, 'F') "MCA#50136#PRIMARYCODE_DESC" ,
    mca.segment3 "MCA#50136#SECONDARYCODE" ,
    xxeis.eis_rs_dff.decode_valueset ('AS_SEC_CODES_VS', mca.segment3, 'F') "MCA#50136#SECONDARYCODE_DESC" ,
    mca.segment1 "MCA#50152#DUMMY" ,
    xxeis.eis_rs_dff.decode_valueset ('', mca.segment1, '') "MCA#50152#DUMMY_DESC" ,
    mca.segment1 "MCA#50153#CARTONGROUP" ,
    xxeis.eis_rs_dff.decode_valueset ('WMS_CARTONIZATION_GROUP', mca.segment1, 'N') "MCA#50153#CARTONGROUP_DESC" ,
    mca.segment1 "MCA#50168#CLASS" ,
    xxeis.eis_rs_dff.decode_valueset ('', mca.segment1, '') "MCA#50168#CLASS_DESC" ,
    mca.segment2 "MCA#50168#SUBCLASS" ,
    xxeis.eis_rs_dff.decode_valueset ('', mca.segment2, '') "MCA#50168#SUBCLASS_DESC" ,
    mca.segment1 "MCA#50169#CONTRACTCATEGORY" ,
    xxeis.eis_rs_dff.decode_valueset ('CONTRACT_CATEGORY_ALPHANUMERIC', mca.segment1, 'N') "MCA#50169#CONTRACTCATEGORY_DES" ,
    mca.segment1 "MCA#50190#CLASSIFICATION" ,
    xxeis.eis_rs_dff.decode_valueset ('WSH_COMMODITY_CLASSIFICATION', mca.segment1, 'F') "MCA#50190#CLASSIFICATION_DESC" ,
    mca.segment2 "MCA#50190#COMMODITY" ,
    xxeis.eis_rs_dff.decode_valueset ('', mca.segment2, '') "MCA#50190#COMMODITY_DESC" ,
    mca.segment3 "MCA#50190#COUNTRY" ,
    xxeis.eis_rs_dff.decode_valueset ('', mca.segment3, '') "MCA#50190#COUNTRY_DESC" ,
    mca.segment4 "MCA#50190#DEFAULTCATEGORY" ,
    xxeis.eis_rs_dff.decode_valueset ('IEX_YES_NO', mca.segment4, 'F') "MCA#50190#DEFAULTCATEGORY_DESC" ,
    mca.segment1 "MCA#50208#PRODUCT" ,
    xxeis.eis_rs_dff.decode_valueset ('', mca.segment1, '') "MCA#50208#PRODUCT_DESC" ,
    mca.segment1 "MCA#50210#PRODUCT" ,
    xxeis.eis_rs_dff.decode_valueset ('4 Digits', mca.segment1, 'N') "MCA#50210#PRODUCT_DESC" ,
    mca.segment1 "MCA#50229#PRODUCT" ,
    xxeis.eis_rs_dff.decode_valueset ('', mca.segment1, '') "MCA#50229#PRODUCT_DESC" ,
    mca.segment1 "MCA#50268#PRODUCT" ,
    xxeis.eis_rs_dff.decode_valueset ('4 Digits', mca.segment1, 'N') "MCA#50268#PRODUCT_DESC" ,
    mca.segment1 "MCA#50272#SEQUENCEDEPENDENTCLA" ,
    xxeis.eis_rs_dff.decode_valueset ('', mca.segment1, '') "MCA#50272#SEQUENCEDEPENDENTCL_" ,
    mca.segment1 "MCA#50308#GPCCATALOGCATEGORYDE" ,
    xxeis.eis_rs_dff.decode_valueset ('', mca.segment1, '') "MCA#50308#GPCCATALOGCATEGORYD_" ,
    mca.segment2 "MCA#50308#GPCCATALOGCATEGORYCO" ,
    xxeis.eis_rs_dff.decode_valueset ('', mca.segment2, '') "MCA#50308#GPCCATALOGCATEGORYC_" ,
    mca.segment1 "MCA#50348#SUPPLIERCODE" ,
    xxeis.eis_rs_dff.decode_valueset ('HDS_SUPPLIER_CODE', mca.segment1, 'I') "MCA#50348#SUPPLIERCODE_DESC" ,
    mca.segment2 "MCA#50348#SUPPLIERCLASS" ,
    xxeis.eis_rs_dff.decode_valueset ('HDS_SUPPLIER_CLASS', mca.segment2, 'D') "MCA#50348#SUPPLIERCLASS_DESC" ,
    mca.segment1 "MCA#50388#CLIENT" ,
    xxeis.eis_rs_dff.decode_valueset ('INV_LSP_CLIENTS', mca.segment1, 'F') "MCA#50388#CLIENT_DESC" ,
    mca.segment1 "MCA#50408#PURCHASEFLAG" ,
    xxeis.eis_rs_dff.decode_valueset ('XXWC_PURCHASE_FLAG', mca.segment1, 'I') "MCA#50408#PURCHASEFLAG_DESC" ,
    mca.segment1 "MCA#50409#PRIVATELABEL" ,
    xxeis.eis_rs_dff.decode_valueset ('30 char', mca.segment1, 'N') "MCA#50409#PRIVATELABEL_DESC" ,
    mca.segment1 "MCA#50410#VELOCITY" ,
    xxeis.eis_rs_dff.decode_valueset ('XXWC_SALES_VELOCITY', mca.segment1, 'I') "MCA#50410#VELOCITY_DESC" ,
    msi.segment1 "MSI#101#ITEM" ,
    xxeis.eis_rs_dff.decode_valueset ('XXWC_SYSTEM_ITEMS', msi.segment1, 'N') "MSI#101#ITEM_DESC"
    --kff#end
    --gl#accountff#start
    ,
    gcc.segment4 gcc#50328#account ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment4, 'XXCUS_GL_ACCOUNT') gcc#50328#account#descr ,
    gcc.segment3 gcc#50328#cost_center ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment3, 'XXCUS_GL_COSTCENTER') gcc#50328#cost_center#descr ,
    gcc.segment6 gcc#50328#furture_use ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment6, 'XXCUS_GL_FUTURE_USE1') gcc#50328#furture_use#descr ,
    gcc.segment7 gcc#50328#future_use_2 ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment7, 'XXCUS_GL_FUTURE_USE_2') gcc#50328#future_use_2#descr ,
    gcc.segment2 gcc#50328#location ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment2, 'XXCUS_GL_LOCATION') gcc#50328#location#descr ,
    gcc.segment1 gcc#50328#product ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment1, 'XXCUS_GL_PRODUCT') gcc#50328#product#descr ,
    gcc.segment5 gcc#50328#project_code ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment5, 'XXCUS_GL_PROJECT') gcc#50328#project_code#descr ,
    gcc1.segment4 gcc1#50328#account ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment4, 'XXCUS_GL_ACCOUNT') gcc1#50328#account#descr ,
    gcc1.segment3 gcc1#50328#cost_center ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment3, 'XXCUS_GL_COSTCENTER') gcc1#50328#cost_center#descr ,
    gcc1.segment6 gcc1#50328#furture_use ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment6, 'XXCUS_GL_FUTURE_USE1') gcc1#50328#furture_use#descr ,
    gcc1.segment7 gcc1#50328#future_use_2 ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment7, 'XXCUS_GL_FUTURE_USE_2') gcc1#50328#future_use_2#descr ,
    gcc1.segment2 gcc1#50328#location ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment2, 'XXCUS_GL_LOCATION') gcc1#50328#location#descr ,
    gcc1.segment1 gcc1#50328#product ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment1, 'XXCUS_GL_PRODUCT') gcc1#50328#product#descr ,
    gcc1.segment5 gcc1#50328#project_code ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment5, 'XXCUS_GL_PROJECT') gcc1#50328#project_code#descr ,
    gcc2.segment4 gcc2#50328#account ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc2.segment4, 'XXCUS_GL_ACCOUNT') gcc2#50328#account#descr ,
    gcc2.segment3 gcc2#50328#cost_center ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc2.segment3, 'XXCUS_GL_COSTCENTER') gcc2#50328#cost_center#descr ,
    gcc2.segment6 gcc2#50328#furture_use ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc2.segment6, 'XXCUS_GL_FUTURE_USE1') gcc2#50328#furture_use#descr ,
    gcc2.segment7 gcc2#50328#future_use_2 ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc2.segment7, 'XXCUS_GL_FUTURE_USE_2') gcc2#50328#future_use_2#descr ,
    gcc2.segment2 gcc2#50328#location ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc2.segment2, 'XXCUS_GL_LOCATION') gcc2#50328#location#descr ,
    gcc2.segment1 gcc2#50328#product ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc2.segment1, 'XXCUS_GL_PRODUCT') gcc2#50328#product#descr ,
    gcc2.segment5 gcc2#50328#project_code ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc2.segment5, 'XXCUS_GL_PROJECT') gcc2#50328#project_code#descr ,
    gcc.segment4 gcc#50368#account ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment4, 'XXCUS_GL_ LTMR _ACCOUNT') gcc#50368#account#descr ,
    gcc.segment3 gcc#50368#department ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment3, 'XXCUS_GL_ LTMR _DEPARTMENT') gcc#50368#department#descr ,
    gcc.segment2 gcc#50368#division ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment2, 'XXCUS_GL_ LTMR _DIVISION') gcc#50368#division#descr ,
    gcc.segment6 gcc#50368#future_use ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment6, 'XXCUS_GL_ LTMR _FUTUREUSE') gcc#50368#future_use#descr ,
    gcc.segment1 gcc#50368#product ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment1, 'XXCUS_GL_LTMR_PRODUCT') gcc#50368#product#descr ,
    gcc.segment5 gcc#50368#subaccount ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment5, 'XXCUS_GL_ LTMR _SUBACCOUNT') gcc#50368#subaccount#descr ,
    gcc1.segment4 gcc1#50368#account ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment4, 'XXCUS_GL_ LTMR _ACCOUNT') gcc1#50368#account#descr ,
    gcc1.segment3 gcc1#50368#department ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment3, 'XXCUS_GL_ LTMR _DEPARTMENT') gcc1#50368#department#descr ,
    gcc1.segment2 gcc1#50368#division ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment2, 'XXCUS_GL_ LTMR _DIVISION') gcc1#50368#division#descr ,
    gcc1.segment6 gcc1#50368#future_use ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment6, 'XXCUS_GL_ LTMR _FUTUREUSE') gcc1#50368#future_use#descr ,
    gcc1.segment1 gcc1#50368#product ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment1, 'XXCUS_GL_LTMR_PRODUCT') gcc1#50368#product#descr ,
    gcc1.segment5 gcc1#50368#subaccount ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment5, 'XXCUS_GL_ LTMR _SUBACCOUNT') gcc1#50368#subaccount#descr ,
    gcc2.segment4 gcc2#50368#account ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc2.segment4, 'XXCUS_GL_ LTMR _ACCOUNT') gcc2#50368#account#descr ,
    gcc2.segment3 gcc2#50368#department ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc2.segment3, 'XXCUS_GL_ LTMR _DEPARTMENT') gcc2#50368#department#descr ,
    gcc2.segment2 gcc2#50368#division ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc2.segment2, 'XXCUS_GL_ LTMR _DIVISION') gcc2#50368#division#descr ,
    gcc2.segment6 gcc2#50368#future_use ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc2.segment6, 'XXCUS_GL_ LTMR _FUTUREUSE') gcc2#50368#future_use#descr ,
    gcc2.segment1 gcc2#50368#product ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc2.segment1, 'XXCUS_GL_LTMR_PRODUCT') gcc2#50368#product#descr ,
    gcc2.segment5 gcc2#50368#subaccount ,
    XXEIS.EIS_RS_FIN_UTILITY.DECODE_VSET (GCC2.SEGMENT5, 'XXCUS_GL_ LTMR _SUBACCOUNT') GCC2#50368#SUBACCOUNT#DESCR
    --gl#accountff#end
    /*,CASE
    WHEN (xxeis.eis_rs_xxwc_com_util_pkg.get_site_code (poh.po_header_id) NOT LIKE ('%NON%EDI%'))
    THEN
    'Y'
    ELSE
    'N'
    END*/
    ,
    (SELECT xml_or_edi
    FROM xxwc.xxwc_po_comm_hist
    WHERE po_header_id = poh.po_header_id
    AND ROWNUM         = 1
    ) edi ,--Added for TMS#20140407-00181 by Mahender on 15/07/14
    /*,NVL ( (SELECT accepted_flag
    FROM po_acceptances_v
    WHERE po_header_id = poh.po_header_id AND ROWNUM = 1)
    ,'N')
    --              , Null
    accepted_y_n    --Added for TMS#20140407-00181 by Mahender on 15/07/14 */
    --commented for ver 1.3
    --    POA.ACCEPTED_FLAG ACCEPTED_Y_N --commented for version 1.4
    XXEIS.EIS_PO_XXWC_PUR_DET_REC_PO_PKG.GET_ACCEPTANCE_DETAILS(POH.PO_HEADER_ID,'ACCEPTED_FLAG') ACCEPTED_Y_N,            --added for version 1.4
    XXEIS.EIS_PO_XXWC_PUR_DET_REC_PO_PKG.GET_ACCEPTANCE_DETAILS(POH.PO_HEADER_ID,'ACCEPTANCE_TYPE') changes_requested_y_n, --added for version 1.4
    --  (SELECT acceptance_type
    --  FROM po_acceptances_v
    --  WHERE po_header_id = poh.po_header_id
    --  AND ROWNUM         = 1
    --  )
    --             , Null
    --  changes_requested_y_n1 --commented for version 1.4
    --START added for ver 1.3
    poh.note_to_receiver note_to_receiver,
    --    poll.note_to_receiver receiving_reason ,
    (
    SELECT ffv.description
    FROM FND_FLEX_VALUES_VL ffv,
      fnd_flex_value_sets ffs
    WHERE NVL(ENABLED_FLAG,'N')             ='Y'
    AND NVL(END_DATE_ACTIVE,TRUNC(SYSDATE))>=TRUNC(SYSDATE)
    AND ffs.flex_value_set_name             ='XXWC_PO_ACCEPTANCE_DEFER_REASONS'
    AND ffv.FLEX_VALUE_SET_ID               = ffs.FLEX_VALUE_SET_ID
    AND ffv.flex_value                      = RSH.attribute2
    AND rownum                              = 1
    ) receiving_reason ,
    --  (SELECT prf.full_name
    --  FROM per_all_people_f prf
    --  WHERE PRF.PERSON_ID = rsh.employee_id
    --  AND TRUNC(sysdate) BETWEEN PRF.EFFECTIVE_START_DATE AND PRF.EFFECTIVE_END_DATE
    --  ) receiver_name , --commented for version 1.4
    XXEIS.EIS_PO_XXWC_PUR_DET_REC_PO_PKG.GET_EMPLOYEE_NAME(rsh.employee_id) receiver_name , --added for version 1.4
    rsh.receipt_num receipt_num ,
    TRUNC(RSL.CREATION_DATE) RECEIPT_DATE ,
    --    poa.displayed_field acceptance_type , --commented for version 1.4
    XXEIS.EIS_PO_XXWC_PUR_DET_REC_PO_PKG.GET_ACCEPTANCE_DETAILS(POH.PO_HEADER_ID,'ACCEPTANCE_TYPE') acceptance_type, --added for version 1.4
    --    hr.full_name accepted_by , --commented for version 1.4
    XXEIS.EIS_PO_XXWC_PUR_DET_REC_PO_PKG.GET_ACCEPTANCE_DETAILS(POH.PO_HEADER_ID,'ACCEPTANCE_BY') accepted_by, --added for version 1.4
    CASE
      WHEN (SELECT 1
        FROM po_acceptances
        WHERE NVL(ACCEPTED_FLAG,'N')='Y'
        AND po_header_id            = poh.po_header_id --2248455
        AND last_update_date       <=
          (SELECT creation_date
          FROM rcv_transactions
          WHERE po_header_id  = poh.po_header_id --2248455
          AND po_line_id      = pol.po_line_id   --4165248
          AND transaction_type='DELIVER'
          AND ROWNUM          = 1
          )
        AND ROWNUM = 1) IS NULL
      THEN 'Y'
      ELSE 'N'
    END Conf_bfr_PO_rec
    --END Added VER 1.3
  FROM po_headers poh ,
    po_lines pol ,
    PO_LINE_LOCATIONS POLL ,
    po_distributions pod,
    po_vendors pv ,
    PO_VENDOR_SITES PVS ,
    po_vendor_contacts pvc,
    po_line_types plt ,
    MTL_SYSTEM_ITEMS_B_KFV MSI ,
    financials_system_parameters fsp ,
    MTL_CATEGORIES_B_KFV MCA ,
    HR_LOCATIONS_ALL HRL ,
    PO_AGENTS_V POAV ,
    ap_terms apt ,
    gl_sets_of_books sob ,
    gl_code_combinations_kfv gcc ,
    GL_CODE_COMBINATIONS_KFV GCC1 ,
    GL_CODE_COMBINATIONS_KFV GCC2 ,
    PA_PROJECTS PP ,
    APPS.HR_OPERATING_UNITS HOU,
    MTL_PARAMETERS OOD,
    --  PER_ALL_PEOPLE_F PAPF, --commented for version 1.4
    --  START Added VER 1.3
    --  RCV_TRANSACTIONS RT, --commented for version 1.4
    RCV_SHIPMENT_HEADERS RSH ,
    RCV_SHIPMENT_LINES RSL
    /*--commented for version 1.4
    --    po_lookup_codes plc_sta
    --    po_lookup_codes plc_can ,
    --    po_lookup_codes plc_clo ,
    --    po_lookup_codes plc_fro ,
    --    po_lookup_codes plc_hld
    --    po_document_types pdt ,
    --    org_organization_definitions ood --Added for TMS#20140407-00181 by Mahender on 15/07/14
    --  ,
    --  (SELECT pa1.PO_HEADER_ID,
    --    pa1.ACCEPTANCE_LOOKUP_CODE,
    --    pa1.EMPLOYEE_ID,
    --    pa1.PO_LINE_LOCATION_ID,
    --    pa1.ACCEPTED_FLAG,
    --    POC.LOOKUP_CODE,
    --    POC.LOOKUP_TYPE,
    --    POC.DISPLAYED_FIELD
    --  FROM PO_ACCEPTANCES pa1,
    --    (SELECT po_header_id,
    --      MAX(acceptance_id) max_acceptance_id
    --    FROM po_acceptances
    --    GROUP BY po_header_id
    --    ) pa2,
    --    PO_LOOKUP_CODES poc
    --  WHERE pa1.po_header_id = pa2.po_header_id
    --  AND pa1.acceptance_id  = pa2.max_acceptance_id
    --  AND POC.LOOKUP_CODE(+) = PA1.ACCEPTANCE_LOOKUP_CODE
    --  AND POC.LOOKUP_TYPE(+) = 'ACCEPTANCE TYPE'
    --  ) POA ,
    --  PER_PEOPLE_F HR ,
    --  PO_AGENTS PA
    --END Added VER 1.3*/ --commented for version 1.4
  WHERE poh.po_header_id            = pol.po_header_id
  AND POL.PO_LINE_ID                = POLL.PO_LINE_ID(+)
  AND poll.line_location_id         = pod.line_location_id
  AND poh.vendor_id                 = pv.vendor_id
  AND POH.VENDOR_SITE_ID            = PVS.VENDOR_SITE_ID
  AND POH.VENDOR_SITE_ID            = PVC.VENDOR_SITE_ID(+)
  AND poh.vendor_contact_id         = pvc.vendor_contact_id(+)
  AND pol.line_type_id              = plt.line_type_id
  AND POL.ITEM_ID                   = MSI.INVENTORY_ITEM_ID(+)
  AND FSP.INVENTORY_ORGANIZATION_ID = NVL(MSI.ORGANIZATION_ID, FSP.INVENTORY_ORGANIZATION_ID)
  AND POH.ORG_ID                    = FSP.ORG_ID(+)
  AND POL.CATEGORY_ID               = MCA.CATEGORY_ID(+)
  AND POLL.SHIP_TO_LOCATION_ID      = HRL.LOCATION_ID(+)
  AND POH.AGENT_ID                  = POAV.AGENT_ID(+)
  AND poh.terms_id                  = apt.term_id(+)
  AND pod.set_of_books_id           = sob.set_of_books_id
  AND pod.code_combination_id       = gcc.code_combination_id
  AND POD.ACCRUAL_ACCOUNT_ID        = GCC1.CODE_COMBINATION_ID
  AND POD.VARIANCE_ACCOUNT_ID       = GCC2.CODE_COMBINATION_ID
  AND POD.PROJECT_ID                = PP.PROJECT_ID(+)
  AND POH.ORG_ID                    = HOU.ORGANIZATION_ID
  AND POLL.SHIP_TO_ORGANIZATION_ID  = OOD.ORGANIZATION_ID --Added for TMS#20140407-00181 by Mahender on 15/07/14
    /*--commented for version 1.4
    --  AND plc_sta.lookup_code           = NVL (poh.authorization_status, 'INCOMPLETE')
    --  AND plc_sta.lookup_type          IN ('PO APPROVAL', 'DOCUMENT STATE')
    --  AND plc_can.lookup_code           = 'CANCELLED'
    --  AND plc_can.lookup_type           = 'DOCUMENT STATE'
    --  AND plc_clo.lookup_code           = NVL (poh.closed_code, 'OPEN')
    --  AND plc_clo.lookup_type           = 'DOCUMENT STATE'
    --  AND plc_fro.lookup_code           = 'FROZEN'
    --  AND plc_fro.lookup_type           = 'DOCUMENT STATE'
    --  AND plc_hld.lookup_code           = 'ON HOLD'
    --  AND plc_hld.lookup_type           = 'DOCUMENT STATE'
    --AND pdt.document_type_code        = 'PO'
    --AND pdt.document_subtype          = poh.type_lookup_code
    --AND poh.org_id                    = pdt.org_id --tjx
    --AND hou.organization_id          = ood.operating_unit  --Added for TMS#20140407-00181 by Mahender on 15/07/14
    --AND POD.DELIVER_TO_PERSON_ID = PAPF.PERSON_ID(+) --Outer Join Added for TMS#20150810-00083 by Mahender on 08/20/15
    --AND TRUNC(SYSDATE) BETWEEN TRUNC(NVL(PAPF.EFFECTIVE_START_DATE,SYSDATE)) AND TRUNC(NVL(PAPF.EFFECTIVE_END_DATE, SYSDATE))
    --START Added VER 1.3
    --AND RT.SHIPMENT_LINE_ID   = RSL.SHIPMENT_LINE_ID(+)
    --AND POH.PO_HEADER_ID          = POA.PO_HEADER_ID(+)
    --AND PA.AGENT_ID               = HR.PERSON_ID(+)
    --AND POA.EMPLOYEE_ID           = PA.AGENT_ID(+)
    --AND POA.PO_LINE_LOCATION_ID  IS NULL
    --AND TRUNC(sysdate) BETWEEN NVL(HR.EFFECTIVE_START_DATE, TRUNC(sysdate)) AND NVL(HR.EFFECTIVE_END_DATE, TRUNC(sysdate))
    --END Added VER 1.3 */ --commented for version 1.4
  AND POLL.LINE_LOCATION_ID  = RSL.PO_LINE_LOCATION_ID
  AND RSL.SHIPMENT_HEADER_ID = RSH.SHIPMENT_HEADER_ID(+) 
/
