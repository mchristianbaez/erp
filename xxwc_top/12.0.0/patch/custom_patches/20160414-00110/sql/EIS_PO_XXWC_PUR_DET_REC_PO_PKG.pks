CREATE OR REPLACE PACKAGE  XXEIS.EIS_PO_XXWC_PUR_DET_REC_PO_PKG
IS
--//============================================================================
--//  
--// Change Request 			:: Performance Issue  
--//
--// Object Usage 				:: This Object Referred by "Purchase Order Detail By Received PO - WC"
--//
--// Object Name         		:: XXEIS.EIS_PO_XXWC_PUR_DET_REC_PO_PKG
--//
--// Object Type         		:: Package Specification
--//
--// Object Description  		:: This Package programs will call in XXEIS_WC_PO_DETAILS_V view to populate data
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        Siva  	      18-may-2016       Initial Build TMS#20160414-00110
--//============================================================================
type t_varchar2_vldn_tbl IS  TABLE OF VARCHAR2(100) INDEX BY VARCHAR2(100);
  g_po_type_vldn_tbl 		t_varchar2_vldn_tbl;
  g_author_status_vldn_tbl  t_varchar2_vldn_tbl;
  g_lookup_meaning_vldn_tbl t_varchar2_vldn_tbl;
  g_actn_date_vldn_tbl 		t_varchar2_vldn_tbl;
  g_accep_flag_vldn_tbl 	t_varchar2_vldn_tbl;
  g_accep_type_vldn_tbl 	t_varchar2_vldn_tbl;
  G_ACCEP_BY_VLDN_TBL 		T_VARCHAR2_VLDN_TBL;
  g_full_name_vldn_tbl 		t_varchar2_vldn_tbl;
  
  g_po_type              	VARCHAR2(200);
  g_authorization_status 	VARCHAR2(200);
  g_lookup_meaning      	VARCHAR2(200);
  g_action_date          	VARCHAR2(200);
  G_ACCEPTED_FLAG        	VARCHAR2(200);
  g_acceptance_type      	VARCHAR2(200); 
  G_ACCEPTED_BY          	VARCHAR2(200);
  G_FULL_NAME	            VARCHAR2(200);
 
    FUNCTION get_po_type(
      p_type_lookup_code IN VARCHAR2,
      p_org_id           IN NUMBER)
    RETURN VARCHAR2;
--//============================================================================
--//
--// Object Name         :: get_po_type  
--//
--// Object Usage 		 :: This Object Referred by "Purchase Order Detail By Received PO - WC"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Siva        	18-May-2016       Initial Build 
--//============================================================================  
    FUNCTION get_authorization_status(
      p_lookup_code IN VARCHAR2)
    RETURN VARCHAR2;
--//============================================================================
--//
--// Object Name         :: get_authorization_status  
--//
--// Object Usage 		 :: This Object Referred by "Purchase Order Detail By Received PO - WC"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Siva        	18-May-2016       Initial Build 
--//============================================================================	
    FUNCTION get_lookup_meaning(
      p_lookup_code IN VARCHAR2,
      p_lookup_type IN VARCHAR2)
    RETURN VARCHAR2;
--//============================================================================
--//
--// Object Name         :: get_lookup_meaning  
--//
--// Object Usage 		 :: This Object Referred by "Purchase Order Detail By Received PO - WC"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Siva        	18-May-2016       Initial Build 
--//============================================================================	
   FUNCTION get_acceptance_details(
      p_po_header_id IN NUMBER,
      p_request_type IN VARCHAR2)
    RETURN VARCHAR2;
--//============================================================================
--//
--// Object Name         :: get_acceptance_details  
--//
--// Object Usage 		 :: This Object Referred by "Purchase Order Detail By Received PO - WC"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Siva        	18-May-2016       Initial Build 
--//============================================================================    
   FUNCTION get_employee_name(
      p_person_id IN NUMBER)
    RETURN VARCHAR2;  
--//============================================================================
--//
--// Object Name         :: get_employee_name  
--//
--// Object Usage 		 :: This Object Referred by "Purchase Order Detail By Received PO - WC"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Siva        	18-May-2016       Initial Build 
--//============================================================================ 
END EIS_PO_XXWC_PUR_DET_REC_PO_PKG;
/
