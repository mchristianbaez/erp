/*************************************************************************
Copyright (c) 2012 HD Supply
All rights reserved.
**************************************************************************
   $Header XXWC.XXWC_B2B_CAT_ITEM_LOAD_EXT_TBL$
  Module Name: XXWC.XXWC_B2B_CAT_ITEM_LOAD_EXT_TBL
  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     26-MAY-2015   Christian Baez             Initially Created TMS# 20160223-00029
                                                      NEW EXTERNAL TABLE FOR ETL - ITEM DATA LOAD
**************************************************************************/
--Drop existing table
--DROP TABLE "XXWC"."XXWC_B2B_CAT_ITEM_LOAD_EXT_TBL";
/
--Recreate external table
CREATE TABLE "XXWC"."XXWC_B2B_CAT_ITEM_LOAD_EXT_TBL"
   (	"ITEM_NUMBER" VARCHAR2(30 BYTE),
	"ICC" VARCHAR2(50 BYTE),
	"ATTR_GROUP_DISP_NAME" VARCHAR2(100 BYTE),
	"ATTR_DISPLAY_NAME" VARCHAR2(200 BYTE),
	"ATTRIBUTE_VALUE" VARCHAR2(200 BYTE)
   )
   ORGANIZATION EXTERNAL
    ( TYPE ORACLE_LOADER
      DEFAULT DIRECTORY "XXWC_PDH_ITEM_LOAD_DIR"
      ACCESS PARAMETERS
      ( RECORDS DELIMITED BY NEWLINE
        SKIP 1
        FIELDS TERMINATED BY ','
        OPTIONALLY ENCLOSED BY '"' AND '"'
                        )
      LOCATION
       ( 'XXWC_Product_Data_Load.csv'
       )
    )
   REJECT LIMIT UNLIMITED ;
/
