/*************************************************************************
Copyright (c) 2012 HD Supply
All rights reserved.
**************************************************************************
   $Header XXWC.XXWC_B2B_CAT_ITEM_LOAD_EXT_TBL$
  Module Name: XXWC.XXWC_B2B_CAT_ITEM_LOAD_EXT_TBL
  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)      TMS TASK        DESCRIPTION
  -- ------- -----------   ------------   ---------      -------------------------
  -- 1.0     26-MAY-2015   Christian Baez 20160223-00029  Initially Created  
**************************************************************************/
GRANT SELECT ON XXWC.XXWC_B2B_CAT_ITEM_LOAD_EXT_TBL to EA_APEX;