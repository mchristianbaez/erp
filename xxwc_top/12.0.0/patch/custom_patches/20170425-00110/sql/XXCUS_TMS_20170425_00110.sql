/*
 TMS: 20170425-00110 / ESMS 557148
 Description: Remove sensitive GL attachment from PRD, DEV and SBX environments
 Date: 04/25/2017
*/
set serveroutput on size 1000000;
declare
 --
   l_entity_name            VARCHAR2 (20) :='GL_JE_HEADERS';
   l1_pk1_value             VARCHAR2 (20) :='2075292';
   l1_pk2_value             VARCHAR2 (20) :='2079392';  
   l2_pk1_value             VARCHAR2 (20) :='2065823';
   l2_pk2_value             VARCHAR2 (20) :='2058219';    
   l_delete_document_flag   VARCHAR2 (1) := 'Y';
   l_loc number :=0;
 --
     procedure print_log(p_message in varchar2) is
     begin
      if apps.fnd_global.conc_request_id >0 then
       apps.fnd_file.put_line(fnd_file.log, p_message);
      else
       dbms_output.put_line(p_message);
      end if;
     end;
 --
begin
     --
	 Savepoint square1;
	 --
     print_log('');     
     print_log(' Begin delete gl attachment for PK1 key value =>'||l1_pk1_value||', PK2 key value =>'||l1_pk2_value);
     print_log('');
     --         
   fnd_attached_documents2_pkg.delete_attachments 
    (
      X_entity_name            =>l_entity_name,
      X_pk1_value              =>l1_pk1_value,
      X_pk2_value              =>l1_pk2_value,
      X_delete_document_flag   =>l_delete_document_flag
    );	 
	 --
	 l_loc :=100;
	 --
     print_log('');     
     print_log(' End delete gl attachment for PK1 key value =>'||l1_pk1_value||', PK2 key value =>'||l1_pk2_value||', rows deleted '||sql%rowcount);
     print_log('');
     --  
	 print_log('');     
     print_log(' Begin delete gl attachment for PK1 key value =>'||l2_pk1_value||', PK2 key value =>'||l2_pk2_value);
     print_log('');
     --         
   fnd_attached_documents2_pkg.delete_attachments 
    (
      X_entity_name            =>l_entity_name,
      X_pk1_value              =>l2_pk1_value,
      X_pk2_value              =>l2_pk2_value,
      X_delete_document_flag   =>l_delete_document_flag
    );	 
	 --
	 l_loc :=101;
	 --	 
     print_log('');     
     print_log(' End delete gl attachment for PK1 key value =>'||l2_pk1_value||', PK2 key value =>'||l2_pk2_value||', rows deleted '||sql%rowcount);
     print_log('');
     --  
	 Commit;
	 --
exception
 when others then
  rollback to square1;
  print_log('Outer block, l_loc =>'||l_loc||', message ='||sqlerrm);
end;
/