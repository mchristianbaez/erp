/*************************************************************************
  $Header TMS_20160421-00015_Datafix_script_Delivery_5644202.sql $
  Module Name: TMS#20160421-00015  Datafix for Unprocessed Shipping Transactions - SO 20282654

  PURPOSE: Data fix to update the Devliery Details

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        28-APR-2016  Niraj Ranjan         TMS#20160421-00015

**************************************************************************/ 

SET SERVEROUTPUT ON SIZE 100000;
SET VERIFY OFF;

BEGIN
DBMS_OUTPUT.put_line ('TMS: 20160421-00015    , Before Update');
UPDATE wsh_delivery_details 
SET released_status = 'D', 
src_requested_quantity = 0, 
requested_quantity = 0, 
shipped_quantity = 0, 
cycle_count_quantity = 0, 
cancelled_quantity = 0, 
subinventory = null, 
locator_id = null, 
lot_number = null, 
revision = null, 
inv_interfaced_flag = 'X', 
oe_interfaced_flag = 'X' 
WHERE delivery_detail_id =15412564 ; 

DBMS_OUTPUT.put_line ('TMS: 20160421-00015   - number of records updated: '|| SQL%ROWCOUNT);
COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20160421-00015     , End Update');
   
END;
/	
