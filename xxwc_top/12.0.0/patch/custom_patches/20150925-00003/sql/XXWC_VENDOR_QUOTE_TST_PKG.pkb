CREATE OR REPLACE
PACKAGE BODY XXEIS.xxwc_vendor_quote_tst_pkg
  /*************************************************************************
  $Header XXWC_VENDOR_QUOTE_TST_PKG.pkb $
  Module Name: XXWC_VENDOR_QUOTE_TST_PKG
  PURPOSE: EIS Vendor Qoute Batch Summary Report
  REVISIONS:
  Ver        Date         Author                Description
  ---------- -----------  ------------------    ----------------
  1.0        NA     EIS        Initial Version
  1.1  07/05/15  Mahender Reddy  TMS#20150312-00153
  1.2  08/07/15  Mahender Reddy  TMS#20150805-00061
  1.3  10/07/2015  Mahender Reddy  TMS#20150925-00003
  **************************************************************************/
AS
  g_dflt_email VARCHAR2 (50) := 'HDSOracleDevelopers@hdsupply.com'; -- Added for Ver 1.3
PROCEDURE get_header_id(
    p_process_id     IN NUMBER,
    p_inv_start_date IN DATE,
    p_inv_end_date   IN DATE)
  /*************************************************************************
  $Header XXWC_VENDOR_QUOTE_TST_PKG.get_header_id $
  Module Name: XXWC_VENDOR_QUOTE_TST_PKG
  PURPOSE: EIS Vendor Qoute Batch Summary Report
  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    ----------------
  1.0         NA     EIS        Initial Version
  1.1   07/05/15  Mahender Reddy  TMS#20150312-00153
  1.2   08/07/15  Mahender Reddy  TMS#20150805-00061
  1.3   10/07/2015  Mahender Reddy  TMS#20150925-00003
  **************************************************************************/
AS
-- Added For Ver 1.3
  l_err_msg       VARCHAR2 (3000);
  l_err_callfrom  VARCHAR2 (50);
  l_err_callpoint VARCHAR2 (50);
  -- End of Ver 1.3
BEGIN
-- Added For Ver 1.3
  l_err_callfrom  := 'xxwc_vendor_quote_tst_pkg.get_header_id';
  l_err_callpoint := 'get header id';
  -- End of Ver 1.3
  --  fnd_file.put_line(fnd_file.log,'in insert' ||l_name);
  INSERT
    /*+ APPEND */
  INTO xxeis.eis_xxwc_om_vendor_quote_v --cp_EIS_XXWC_OM_VENDOR_QUOTE_V
  SELECT NAME,
    SALESREP_NAME,
    SALESREP_NUMBER,
    ACCOUNT_NAME,
    ACCOUNT_NUMBER,
    SHIP_TO,
    ORGANIZATION_NAME, --Added by Mahender for 20150312-00153 on 07/05/15
    SOURCE_TYPE_CODE,  --Added by Mahender for 20150312-00153 on 07/05/15
    PARTY_SITE_NUMBER,
    VENDOR_NUMBER,
    VENDOR_NAME,
    ORACLE_QUOTE_NUMBER,
    INVOICE_NUMBER,
    INVOICE_DATE,
    PART_NUMBER,
    UOM,
    DESCRIPTION,
    BPA,
    PO_COST,
    AVERAGE_COST,
    SPECIAL_COST,
    (PO_COST - SPECIAL_COST) UNIT_CLAIM_VALUE,
    (PO_COST - SPECIAL_COST) * QTY REBATE,
    GL_CODING,
    GL_STRING,
    LOC,
    CREATED_BY,
    CUSTOMER_TRX_ID,
    ORDER_NUMBER,
    LINE_NUMBER,
    CREATION_DATE,
    LOCATION,
    QTY,
    P_PROCESS_ID,
    1 COMMMON_OUTPUT_ID
  FROM
    (SELECT NAME,
      SALESREP_NAME,
      SALESREP_NUMBER,
      ACCOUNT_NAME,
      ACCOUNT_NUMBER,
      SHIP_TO,
      ORGANIZATION_NAME, --Added by Mahender for 20150312-00153 on 07/05/15
      SOURCE_TYPE_CODE,  --Added by Mahender for 20150312-00153 on 07/05/15
      PARTY_SITE_NUMBER,
      VENDOR_NUMBER,
      VENDOR_NAME,
      ORACLE_QUOTE_NUMBER,
      INVOICE_NUMBER,
      INVOICE_DATE,
      PART_NUMBER,
      UOM,
      DESCRIPTION,
      CASE
        WHEN UPPER (VENDOR_NAME) LIKE 'SIMPSON%'
        THEN XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_BEST_BUY ( INVENTORY_ITEM_ID, ORGANIZATION_ID, VENDOR_ID)
        ELSE NULL
      END BPA,
      /*----Modified and added PO cost logic by Mahender for 20150312-00153 on 07/05/15*/
      NVL(
      (SELECT a.BPA_COST
      FROM
        ----XXEIS.EIS_PO_XXWC_ISR_BPA_COST##Z  a, ---- Commented For Ver 1.3
        XXEIS.EIS_XXWC_PO_ISR_TAB a, -- Added For Ver 1.3
        OE_ORDER_LINES OL1
      WHERE OL1.ROWID         = OL_ROWID
      AND a.INVENTORY_ITEM_ID = OL1.INVENTORY_ITEM_ID
      AND a.ORGANIZATION_ID   = OL1.SHIP_FROM_ORG_ID
      ), NVL (
      (SELECT SUM ( DECODE (COST_ELEMENT_ID, 1, ACTUAL_COST, 0))
      FROM MTL_CST_ACTUAL_COST_DETAILS MCD,
        OE_ORDER_LINES OL1
      WHERE OL1.ROWID    = OL_ROWID
      AND TRANSACTION_ID =
        (SELECT
          /*+ no_unnest hash_sj */
          MAX (TRANSACTION_ID)
        FROM MTL_CST_ACTUAL_COST_DETAILS MCD1
        WHERE MCD1.INVENTORY_ITEM_ID   = OL1.INVENTORY_ITEM_ID
        AND MCD1.ORGANIZATION_ID       = OL1.SHIP_FROM_ORG_ID
        AND (TRANSACTION_COSTED_DATE) <= INVOICE_DATE
          --TRUNC (OL1.CREATION_DATE) + 0.9999
        )
      AND MCD.INVENTORY_ITEM_ID      = OL1.INVENTORY_ITEM_ID
      AND MCD.ORGANIZATION_ID        = OL1.SHIP_FROM_ORG_ID
      AND (TRANSACTION_COSTED_DATE) <= INVOICE_DATE
        --TRUNC (OL1.CREATION_DATE) + 0.9999
      ), LIST_PRICE_PER_UNIT)) PO_COST,
      --Commneted by Mahender for 20150312-00153 on 07/05/15
      /*NVL (
      (SELECT SUM (
      DECODE (COST_ELEMENT_ID, 1, ACTUAL_COST, 0))
      FROM MTL_CST_ACTUAL_COST_DETAILS MCD,
      OE_ORDER_LINES OL1
      WHERE     OL1.ROWID = OL_ROWID
      AND TRANSACTION_ID =
      (SELECT /*+ no_unnest hash_sj */
      /*     MAX (TRANSACTION_ID)
      FROM MTL_CST_ACTUAL_COST_DETAILS MCD1
      WHERE     MCD1.INVENTORY_ITEM_ID =
      OL1.INVENTORY_ITEM_ID
      AND MCD1.ORGANIZATION_ID =
      OL1.SHIP_FROM_ORG_ID
      AND (TRANSACTION_COSTED_DATE) <= INVOICE_DATE
      --TRUNC (OL1.CREATION_DATE) + 0.9999
      )
      AND MCD.INVENTORY_ITEM_ID =
      OL1.INVENTORY_ITEM_ID
      AND MCD.ORGANIZATION_ID = OL1.SHIP_FROM_ORG_ID
      AND (TRANSACTION_COSTED_DATE) <= INVOICE_DATE
      --TRUNC (OL1.CREATION_DATE) + 0.9999
      ),
      LIST_PRICE_PER_UNIT) PO_COST,*/
      AVERAGE_COST,
      SPECIAL_COST,
      1 UNIT_CLAIM_VALUE,
      1 REBATE,
      GL_CODING,
      GL_STRING,
      LOC,
      CREATED_BY,
      CUSTOMER_TRX_ID,
      ORDER_NUMBER,
      LINE_NUMBER,
      CREATION_DATE,
      LOCATION,
      QTY,
      P_PROCESS_ID,
      1 COMMMON_OUTPUT_ID
    FROM
      (SELECT NAME,
        LINE_NUMBER,
        OL_ROWID,
        ORDER_NUMBER,
        VENDOR_NUMBER,
        VENDOR_NAME,
        VENDOR_ID,
        INVENTORY_ITEM_ID,
        ORGANIZATION_ID,
        ORGANIZATION_NAME, --Added by Mahender for 20150312-00153 on 07/05/15
        SOURCE_TYPE_CODE,  --Added by Mahender for 20150312-00153 on 07/05/15
        ORACLE_QUOTE_NUMBER,
        PART_NUMBER,
        UOM,
        DESCRIPTION,
        AVERAGE_COST,
        SPECIAL_COST,
        CREATION_DATE,
        LOCATION,
        QTY,
        CUSTOMER_TRX_ID,
        INVOICE_NUMBER,
        INVOICE_DATE,
        LOC,
        GL_CODING,
        GL_STRING,
        CREATED_BY,
        SALESREP_ID,
        CUSTOMER_ID,
        CUSTOMER_SITE_ID,
        HEADER_ID,
        FULL_NAME,
        SALESREP_NUMBER,
        SALESREP_NAME,
        ACCOUNT_NAME,
        ACCOUNT_NUMBER,
        SHIP_TO,
        PARTY_SITE_NUMBER,
        LIST_PRICE_PER_UNIT,
        (ROW_NUMBER () OVER (PARTITION BY HEADER_ID, LINE_ID ORDER BY QUALIFIER_ATTRIBUTE)) LIST_NUM
      FROM
        (WITH OL AS
        (SELECT
          /*+ MATERIALIZE leading(rctlgl rctl ol oh fu ppf1 ras.jrs hzcs_ship_to HCAS_SHIP_TO hcs hp HZPS_SHIP_TO) use_nl(rctlgl rctl ol oh fu ppf1 ras.jrs) no_index_ss(rctlgl RA_CUST_TRX_LINE_GL_DIST_N2) */
          OL.LINE_ID,
          OL.ROWID OL_ROWID,
          OL.INVENTORY_ITEM_ID,
          OL.SHIP_FROM_ORG_ID,
          OL.LINE_NUMBER,
          OL.CREATION_DATE CREATION_DATE,
          OL.SOURCE_TYPE_CODE SOURCE_TYPE_CODE, --Added by Mahender for 20150312-00153 on 07/05/15
          RCTLGL.GL_DATE,
          RCTLGL.CUSTOMER_TRX_LINE_ID,
          RCTLGL.CODE_COMBINATION_ID,
          RCTL.CUSTOMER_TRX_ID,
          NVL (RCTL.QUANTITY_INVOICED, RCTL.QUANTITY_CREDITED) QTY,
          OH.HEADER_ID,
          OH.ORDER_NUMBER,
          OH.CREATION_DATE OH_CREATION_DATE,
          OH.CREATED_BY,
          OH.SALESREP_ID,
          PPF1.FULL_NAME,
          RAS.SALESREP_NUMBER,
          RAS.NAME SALESREP_NAME,
          HCS.ACCOUNT_NAME,
          HCS.ACCOUNT_NUMBER,
          HCS.CUST_ACCOUNT_ID,
          HZCS_SHIP_TO.LOCATION SHIP_TO,
          HZPS_SHIP_TO.PARTY_SITE_NUMBER PARTY_SITE_NUMBER,
          TO_CHAR (OH.SHIP_TO_ORG_ID) SHIP_TO_ORG_ID,
          TO_CHAR (OH.SOLD_TO_ORG_ID) SOLD_TO_ORG_ID
          --leading(rctlgl rctl ol oh fu ras.jrs  hzcs_ship_to hcs hp) push_pred(fu)  */
        FROM APPS.OE_ORDER_LINES OL,
          APPS.RA_CUSTOMER_TRX_LINES RCTL,
          APPS.RA_CUST_TRX_LINE_GL_DIST RCTLGL,
          OE_ORDER_HEADERS OH,
          FND_USER FU,
          PER_ALL_PEOPLE_F PPF1,
          RA_SALESREPS RAS,
          HZ_CUST_ACCOUNTS HCS,
          HZ_PARTIES HP,
          HZ_CUST_SITE_USES HZCS_SHIP_TO,
          HZ_CUST_ACCT_SITES HCAS_SHIP_TO,
          HZ_PARTY_SITES HZPS_SHIP_TO,
          HZ_LOCATIONS HZL_SHIP_TO
        WHERE OL.FLOW_STATUS_CODE          = 'CLOSED'
        AND RCTL.INTERFACE_LINE_CONTEXT    = 'ORDER ENTRY'
        AND RCTL.INTERFACE_LINE_ATTRIBUTE6 = TO_CHAR (OL.LINE_ID)
        AND RCTL.LINE_TYPE                 = 'LINE'
        AND RCTLGL.CUSTOMER_TRX_LINE_ID    = RCTL.CUSTOMER_TRX_LINE_ID
        AND RCTLGL.ACCOUNT_CLASS           = 'REV'
        AND RCTLGL.GL_DATE BETWEEN p_inv_start_date --'01-may-14' --
        AND p_inv_end_date                          --'31-may-14'
        AND OH.HEADER_ID = OL.HEADER_ID
        AND FU.USER_ID   = OH.CREATED_BY
        AND FU.EMPLOYEE_ID--(+)
          = PPF1.PERSON_ID
        AND SYSTIMESTAMP BETWEEN PPF1.EFFECTIVE_START_DATE AND PPF1.EFFECTIVE_END_DATE
        AND RAS.SALESREP_ID                = OH.SALESREP_ID
        AND HCAS_SHIP_TO.CUST_ACCOUNT_ID   = HCS.CUST_ACCOUNT_ID
        AND HCS.PARTY_ID                   = HP.PARTY_ID
        AND OH.SHIP_TO_ORG_ID              = HZCS_SHIP_TO.SITE_USE_ID(+)
        AND HZCS_SHIP_TO.CUST_ACCT_SITE_ID = HCAS_SHIP_TO.CUST_ACCT_SITE_ID(+)
        AND HCAS_SHIP_TO.PARTY_SITE_ID     = HZPS_SHIP_TO.PARTY_SITE_ID(+)
        AND HZL_SHIP_TO.LOCATION_ID(+)     = HZPS_SHIP_TO.LOCATION_ID
        )
      SELECT
        /*+  use_concat  leading(ol qq qlh xxcph xxcpl POV.pav pov.hp gcc.GL_CODE_COMBINATIONS  RCT QLL MSI @subq OOD) use_nl(ol qq qlh.b qlh.t)  */
        QQ.QUALIFIER_ATTRIBUTE,
        QLH.NAME,
        OL.LINE_NUMBER,
        OL_ROWID,
        OL.ORDER_NUMBER,
        POV.SEGMENT1 VENDOR_NUMBER,
        POV.VENDOR_NAME VENDOR_NAME,
        POV.VENDOR_ID,
        XXCPL.VENDOR_QUOTE_NUMBER ORACLE_QUOTE_NUMBER,
        MSI.CONCATENATED_SEGMENTS PART_NUMBER,
        MSI.PRIMARY_UNIT_OF_MEASURE UOM,
        MSI.DESCRIPTION DESCRIPTION,
        MSI.LIST_PRICE_PER_UNIT,
        MSI.INVENTORY_ITEM_ID,
        MSI.ORGANIZATION_ID,
        OOD.ORGANIZATION_NAME ORGANIZATION_NAME, --Added by Mahender for 20150312-00153 on 07/05/15
        XXCPL.AVERAGE_COST AVERAGE_COST,
        XXCPL.SPECIAL_COST SPECIAL_COST,
        OL.OH_CREATION_DATE CREATION_DATE,
        -- MP.ORGANIZATION_CODE LOCATION,  --Commented by Mahender for 20150312-00153 on 07/05/15
        OOD.ORGANIZATION_CODE LOCATION,       --Added by Mahender for 20150312-00153 on 07/05/15
        OL.SOURCE_TYPE_CODE SOURCE_TYPE_CODE, --Added by Mahender for 20150312-00153 on 07/05/15
        OL.QTY,
        RCT.CUSTOMER_TRX_ID CUSTOMER_TRX_ID,
        RCT.TRX_NUMBER INVOICE_NUMBER,
        TRUNC (RCT.TRX_DATE) INVOICE_DATE,
        GCC.SEGMENT2 LOC,
        '400-900'
        || '-'
        || GCC.SEGMENT2 GL_CODING,
        GCC.CONCATENATED_SEGMENTS GL_STRING,
        OL.CREATED_BY,
        OL.SALESREP_ID,
        XXCPH.CUSTOMER_ID,
        XXCPH.CUSTOMER_SITE_ID,
        OL.HEADER_ID,
        OL.FULL_NAME,
        OL.SALESREP_NUMBER,
        OL.SALESREP_NAME,
        OL.ACCOUNT_NAME,
        OL.ACCOUNT_NUMBER,
        OL.SHIP_TO,
        OL.PARTY_SITE_NUMBER PARTY_SITE_NUMBER,
        TRUNC (XXCPL.START_DATE) START_DATE,
        -- TRUNC (OL.GL_DATE) END_DATE, --commeneted by Mahender for TMS#20150805-00061 on 08/07/15 which was added by Mahender for 20150312-00153 on 07/05/15
        NVL (TRUNC (XXCPL.END_DATE), TRUNC (OL.GL_DATE)) END_DATE, --ADD by Mahender for for TMS#20150805-00061 on 08/07/15 which was Commented by Mahender for 20150312-00153 on 07/05/15
        TRUNC (OL.GL_DATE) GL_DATE,
        OL.LINE_ID
      FROM OL,
        QP_QUALIFIERS QQ,
        QP_LIST_HEADERS QLH,
        APPS.XXWC_OM_CONTRACT_PRICING_HDR XXCPH,
        APPS.XXWC_OM_CONTRACT_PRICING_LINES XXCPL,
        --CP_OM_CONTRACT_PRICING_LINES XXCPL,
        QP_LIST_LINES QLL,
        MTL_SYSTEM_ITEMS_B_KFV MSI,
        PO_VENDORS POV,
        -- MTL_PARAMETERS MP,  --Commented by Mahender for 20150312-00153 on 07/05/15
        RA_CUSTOMER_TRX RCT,
        GL_CODE_COMBINATIONS_KFV GCC,
        ORG_ORGANIZATION_DEFINITIONS OOD --Added by Mahender for 20150312-00153 on 07/05/15
      WHERE 1                         = 1
      AND QQ.QUALIFIER_CONTEXT        = 'CUSTOMER'
      AND QQ.COMPARISON_OPERATOR_CODE = '='
      AND QQ.ACTIVE_FLAG              = 'Y'
      AND ( ( QLH.ATTRIBUTE10         = ('Contract Pricing')
      AND QQ.QUALIFIER_ATTRIBUTE      = 'QUALIFIER_ATTRIBUTE11'
      AND (QQ.QUALIFIER_ATTR_VALUE    = OL.SHIP_TO_ORG_ID))
      OR ( QLH.ATTRIBUTE10            = ('Contract Pricing')
      AND QQ.QUALIFIER_ATTRIBUTE      = 'QUALIFIER_ATTRIBUTE32'
      AND (QQ.QUALIFIER_ATTR_VALUE    = OL.SOLD_TO_ORG_ID))
      OR ( QLH.ATTRIBUTE10            = ('CSP-NATIONAL')
      AND QQ.QUALIFIER_ATTRIBUTE      = 'QUALIFIER_ATTRIBUTE32'
      AND (QQ.QUALIFIER_ATTR_VALUE    = OL.SOLD_TO_ORG_ID)))
      AND QLH.LIST_HEADER_ID          = QQ.LIST_HEADER_ID
      AND QLH.ACTIVE_FLAG             = 'Y'
        --                                  AND TO_CHAR (XXCPH.AGREEMENT_ID) =
        --                                         (QLH.ATTRIBUTE14)
      AND (XXCPH.AGREEMENT_ID)              = to_number(QLH.ATTRIBUTE14)
      AND XXCPL.AGREEMENT_ID                = XXCPH.AGREEMENT_ID
      AND XXCPL.AGREEMENT_TYPE              = 'VQN'
      AND QLH.LIST_HEADER_ID                = QLL.LIST_HEADER_ID
      AND XXCPL.PRODUCT_VALUE               = MSI.SEGMENT1
      AND TO_CHAR (XXCPL.AGREEMENT_LINE_ID) = (QLL.ATTRIBUTE2)
      AND MSI.INVENTORY_ITEM_ID             = OL.INVENTORY_ITEM_ID
      AND MSI.ORGANIZATION_ID               = OL.SHIP_FROM_ORG_ID
      AND POV.VENDOR_ID                     = XXCPL.VENDOR_ID
        --AND MP.ORGANIZATION_ID = MSI.ORGANIZATION_ID
      AND OOD.ORGANIZATION_ID     = MSI.ORGANIZATION_ID --Added by Mahender for 20150312-00153 on 07/05/15
      AND RCT.CUSTOMER_TRX_ID     = OL.CUSTOMER_TRX_ID
      AND GCC.CODE_COMBINATION_ID = OL.CODE_COMBINATION_ID
        /*  AND NOT EXISTS
        (SELECT /*+ qb_name(subq) * 'Y'
        FROM MTL_SYSTEM_ITEMS_B_KFV
        WHERE     INVENTORY_ITEM_ID =
        MSI.INVENTORY_ITEM_ID
        AND ORGANIZATION_ID =
        MSI.ORGANIZATION_ID
        AND SEGMENT1 LIKE 'SP%') */
        --Commented by Mahender for TMS#20150805-00061 on 08/07/15
        ) A
      WHERE GL_DATE <= END_DATE
      AND GL_DATE   >= START_DATE
      )
    WHERE LIST_NUM = 1
    );
  COMMIT;
EXCEPTION
WHEN OTHERS THEN
  FND_FILE.PUT_LINE (FND_FILE.LOG, 'in insert exception' || SQLERRM);
  -- Added For Ver 1.3
  APPS.XXCUS_ERROR_PKG.XXCUS_ERROR_MAIN_API ( P_CALLED_FROM => L_ERR_CALLFROM , P_CALLING => L_ERR_CALLPOINT , P_REQUEST_ID => fnd_global.conc_request_id , P_ORA_ERROR_MSG => SQLERRM , P_ERROR_DESC => L_ERR_MSG , P_DISTRIBUTION_LIST => G_DFLT_EMAIL , p_module => 'OM');
  -- End of Ver 1.3
END;
END XXWC_VENDOR_QUOTE_TST_PKG;
/