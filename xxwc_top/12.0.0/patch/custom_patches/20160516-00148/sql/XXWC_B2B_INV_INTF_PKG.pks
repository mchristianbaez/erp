--
-- XXWC_B2B_INV_INTF_PKG  (Package) 
--
CREATE OR REPLACE PACKAGE APPS.XXWC_B2B_INV_INTF_PKG
AS
   /*************************************************************************
     $Header XXWC_B2B_INV_INTF_PKG $
     Module Name: XXWC_B2B_INV_INTF_PKG.pks

     PURPOSE:   XXWC B2B Open Invoice Outbound Interface

     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        05/01/2014  Gopi Damuluri           Initial Version TMS# 20140219-00148
     1.4        05/16/2016  Gopi Damuluri           TMS# 20160516-00148 - Issue with B2B Stats showing Tax Amounts
   **************************************************************************/

   /*************************************************************************
     Procedure : gen_open_invoice_file

     PURPOSE:   This procedure creates file for the open invoices for
                B2B Liason
     Parameter:

   ************************************************************************/
   PROCEDURE gen_open_invoice_file (
      errbuf             OUT      VARCHAR2,
      retcode            OUT      VARCHAR2,
      p_directory_name   IN OUT   VARCHAR2,
      p_from_date        IN       VARCHAR2,
      p_to_date          IN       VARCHAR2,
      p_tp_name          IN       VARCHAR2
   );

   -- Function to derive DeliveryId detals
   FUNCTION get_delivery_id (p_cust_trx_id IN NUMBER) return VARCHAR2;

-- Version# 1.4 > Start
   /*************************************************************************
     Function: get_invoice_count

     PURPOSE:   To derive Invoice Counts for B2B Stats Page

     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.4        05/16/2016  Gopi Damuluri           TMS# 20160516-00148 - Issue with B2B Stats showing Tax Amounts
   ************************************************************************/
   FUNCTION get_invoice_count (p_cust_acount_id IN NUMBER
                             , p_start_Date     IN DATE
                             , p_end_date       IN DATE
                              ) return NUMBER;
   
   /*************************************************************************
     Function: get_invoice_amt

     PURPOSE:   To derive Invoice Amounts for B2B Stats Page

     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.4        05/16/2016  Gopi Damuluri           TMS# 20160516-00148 - Issue with B2B Stats showing Tax Amounts
   ************************************************************************/
   FUNCTION get_invoice_amt (p_cust_acount_id IN NUMBER
                           , p_start_Date     IN DATE
                           , p_end_date       IN DATE
                            ) return NUMBER;
-- Version# 1.4 < End

END XXWC_B2B_INV_INTF_PKG;
/