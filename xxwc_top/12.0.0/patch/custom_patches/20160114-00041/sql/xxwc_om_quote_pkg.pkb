CREATE OR REPLACE PACKAGE BODY APPS.xxwc_om_quote_pkg
AS
   /*************************************************************************
   *   $Header xxwc_om_quote_pkg.pkb $
   *   Module Name: xxwc OM Custom Quote package
   *
   *   PURPOSE:   Used in Quotes form
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *  1.0        01/25/2013  Shankar Hariharan         Initial Version
   *  2.0        10/08/2013  Ram Talluri               TMS-20131003-00186 - Custom Quote to SO Imports - Price Adjustments Missing
   *  2.1        11/12/2013  Ram Talluri               TMS#20131112-00332
   *  2.2        02/19/2014  Ram Talluri               TMS #20140219-00158 - Imoort price adjustments correctly
   *  2.3        02/26/2014  Shankar Hariharan         TMS #20140107-00097 - Rounding off selling price to 2 decimals
   *  2.4        04/01/2014  Ram Talluri                TMS #20140308-00014 - Price rounding to three decimal places.
   *  2.5        04/10/2014  Maharajan Shunmugam        TMS# 20140407-00346 - Performance issue
   *  2.6        04/14/2014  Ram Talluri               TMS #20140414-00019 4/14/2014- Price rounding from 3 to 5.
   *  2.7        10/20/2014  Maharajan Shunmgam        TMS# 20141001-00162 Canada Multi Org Changes  
   *  2.8        03/10/2016  Rakesh Patel              TMS#  20160114-00041 WC Quote notes to sales order  
   * ***************************************************************************/

   PROCEDURE calculate_tax (i_quote_number    IN     NUMBER,
                            i_call_mode       IN     VARCHAR,
                            o_return_status      OUT VARCHAR,
                            o_msg_data           OUT VARCHAR)
   IS
      /* Initialize the proper Context */
      --l_org_id                 NUMBER := fnd_profile.VALUE ('ORG_ID');          -- commented and added below for ver# 2.4
      --l_application_id           NUMBER := fnd_profile.VALUE ('RESP_APPL_ID');
      l_org_id                   NUMBER := MO_GLOBAL.GET_CURRENT_ORG_ID;
      l_application_id           NUMBER := fnd_global.resp_appl_id; 
      /* MZ4Md211 */
      --l_responsibility_id        NUMBER := fnd_profile.VALUE ('RESP_ID');        -- commented and added below for ver# 2.4
      --l_user_id                  NUMBER := fnd_profile.VALUE ('USER_ID');
        l_responsibility_id        NUMBER := fnd_global.resp_id;
        l_user_id                  NUMBER := fnd_global.user_id;

      /* Initialize the record to G_MISS to enable defaulting */
      l_header_rec               OE_ORDER_PUB.Header_Rec_Type
                                    := OE_ORDER_PUB.G_MISS_HEADER_REC;
      l_old_header_rec           OE_ORDER_PUB.Header_Rec_Type;

      l_line_tbl                 OE_ORDER_PUB.Line_Tbl_Type;
      l_old_line_tbl             OE_ORDER_PUB.Line_Tbl_Type;

      l_action_request_tbl       OE_ORDER_PUB.Request_Tbl_Type;

      x_header_rec               OE_ORDER_PUB.Header_Rec_Type;
      x_header_val_rec           OE_ORDER_PUB.Header_Val_Rec_Type;
      x_Header_Adj_tbl           OE_ORDER_PUB.Header_Adj_Tbl_Type;
      x_Header_Adj_val_tbl       OE_ORDER_PUB.Header_Adj_Val_Tbl_Type;
      x_Header_price_Att_tbl     OE_ORDER_PUB.Header_Price_Att_Tbl_Type;
      x_Header_Adj_Att_tbl       OE_ORDER_PUB.Header_Adj_Att_Tbl_Type;
      x_Header_Adj_Assoc_tbl     OE_ORDER_PUB.Header_Adj_Assoc_Tbl_Type;
      x_Header_Scredit_tbl       OE_ORDER_PUB.Header_Scredit_Tbl_Type;
      x_Header_Scredit_val_tbl   OE_ORDER_PUB.Header_Scredit_Val_Tbl_Type;
      x_line_tbl                 OE_ORDER_PUB.Line_Tbl_Type;
      x_line_val_tbl             OE_ORDER_PUB.Line_Val_Tbl_Type;
      x_Line_Adj_tbl             OE_ORDER_PUB.Line_Adj_Tbl_Type;
      x_Line_Adj_val_tbl         OE_ORDER_PUB.Line_Adj_Val_Tbl_Type;
      x_Line_price_Att_tbl       OE_ORDER_PUB.Line_Price_Att_Tbl_Type;
      x_Line_Adj_Att_tbl         OE_ORDER_PUB.Line_Adj_Att_Tbl_Type;
      x_Line_Adj_Assoc_tbl       OE_ORDER_PUB.Line_Adj_Assoc_Tbl_Type;
      x_Line_Scredit_tbl         OE_ORDER_PUB.Line_Scredit_Tbl_Type;
      x_Line_Scredit_val_tbl     OE_ORDER_PUB.Line_Scredit_Val_Tbl_Type;
      x_Lot_Serial_tbl           OE_ORDER_PUB.Lot_Serial_Tbl_Type;
      x_Lot_Serial_val_tbl       OE_ORDER_PUB.Lot_Serial_Val_Tbl_Type;
      x_action_request_tbl       OE_ORDER_PUB.Request_Tbl_Type;
      
      l_om_tax_tbl_type       xxwc_om_quote_pkg.XXWC_OM_TAX_TBL_TYPE := xxwc_om_quote_pkg.G_MISS_XXWC_OM_TAX_TBL; --Added by Maha for ver 2.5

      l_return_status            VARCHAR2 (2000);
      l_msg_count                NUMBER;
      l_msg_data                 VARCHAR2 (2000);
      l_return_msg               VARCHAR2 (2000);
      lv_msg_data                VARCHAR2 (2000);

      l_line_cnt                 NUMBER := 0;
      l_top_model_line_index     NUMBER;
      l_link_to_line_index       NUMBER;

      l_msg_index_out            NUMBER (10);
      

      --PRAGMA AUTONOMOUS_TRANSACTION;

      CURSOR c1
      IS
         SELECT *
           FROM xxwc_om_quote_headers
          WHERE quote_number = i_quote_number;

      CURSOR c2
      IS
         SELECT *
           FROM xxwc_om_quote_lines
          WHERE quote_number = i_quote_number;
          
--Added below cursor by Maha for ver 2.5
      CURSOR cur_load (p_header_id NUMBER)
      IS SELECT b.line_number,    
                b.attribute4,           
                b.ordered_item_id,                
                b.tax_value,
                b.ordered_quantity,
                b.attribute18                 
          FROM oe_order_lines b
           WHERE header_id = p_header_id;
   BEGIN
      fnd_global.apps_initialize (l_user_id,
                                  l_responsibility_id,
                                  l_application_id,
                                  NULL);
      mo_global.init ('ONT');
      mo_global.set_policy_context ('S', l_org_id);


      FOR c1_rec IN c1
      LOOP
         l_header_rec := oe_order_pub.g_miss_header_rec;
         l_header_rec.order_type_id := 1001;
         l_header_rec.sold_to_org_id := c1_rec.cust_account_id;
         l_header_rec.ship_to_org_id := c1_rec.site_use_id;
         l_header_rec.shipping_method_code := c1_rec.shipping_method;
         l_header_rec.ship_from_org_id := c1_rec.organization_id;
         l_header_rec.invoice_to_contact_id := c1_rec.contact_id;
         l_header_rec.ship_to_contact_id := c1_rec.contact_id;
         l_header_rec.cust_po_number :=
            'QUOTE:' || i_quote_number || '-' || SYSDATE;
         --l_header_rec.price_list_id := 26463;                             --commented and added belowe for ver#2.3
           l_header_rec.price_list_id := fnd_profile.VALUE('XXWC_GLOBAL_PRICE_LIST');
         l_header_rec.operation := oe_globals.g_opr_create;
         l_header_rec.shipping_instructions := c1_rec.notes;

         FOR c2_rec IN c2
         LOOP
            l_line_cnt := l_line_cnt + 1;
            l_line_tbl (l_line_cnt) := OE_ORDER_PUB.G_MISS_LINE_REC;
            l_line_tbl (l_line_cnt).operation := OE_GLOBALS.G_OPR_CREATE;
            --l_line_tbl(l_line_cnt).header_id := i_line_tbl(i).header_id;
            l_line_tbl (l_line_cnt).inventory_item_id :=
               c2_rec.inventory_item_id;
            l_line_tbl (l_line_cnt).ship_from_org_id := c1_rec.organization_id;
            l_line_tbl (l_line_cnt).ordered_quantity := c2_rec.line_quantity;
            l_line_tbl (l_line_cnt).unit_selling_price :=
               --ROUND (c2_rec.gm_selling_price, 3);--Rounding factor chnaged from 2 to 3 by Ram Talluri on 4/1/2014 for TMS #20140308-00014--Version 2.6
               ROUND (c2_rec.gm_selling_price, 5);--Version 2.6
            l_line_tbl (l_line_cnt).unit_list_price :=
               --ROUND (c2_rec.list_price, 3);--Rounding factor chnaged from 2 to 3 by Ram Talluri on 4/1/2014 for TMS #20140308-00014--Version 2.6
               ROUND (c2_rec.list_price, 5);--Version 2.6
            --l_line_tbl (l_line_cnt).calculate_price_flag := 'N';--Commented by Ram Talluri on 10/8/2013 for TMS-20131003-00186
            l_line_tbl (l_line_cnt).calculate_price_flag := 'Y'; --Added by Ram Talluri on 10/8/2013 for TMS-20131003-00186
            l_line_tbl (l_line_cnt).attribute18 := c2_rec.line_seq; --Added by Ram Talluri on 10/8/2013 for TMS-20131003-00186
            l_line_tbl (l_line_cnt).packing_instructions := c2_rec.notes; --Added by Rakesh Patel on 03/10/2016 for TMS-20160114-00041 


            --IF i_call_mode <> 'O'   --commented and added below by Maha for ver 2.5
            IF i_call_mode  = 'T'
            THEN
               l_line_tbl (l_line_cnt).attribute4 := c2_rec.line_seq;
               l_line_tbl (l_line_cnt).calculate_price_flag := 'N'; -- Ram Talluri 11/12/2013 TMS #20131112-00332
            END IF;
         END LOOP;
         

         OE_ORDER_PUB.Process_Order (
            p_api_version_number       => 1,
            p_org_id                   => l_org_id,
            p_init_msg_list            => FND_API.G_TRUE,
            p_return_values            => FND_API.G_TRUE,
            p_action_commit            => FND_API.G_TRUE,
            x_return_status            => l_return_status,
            x_msg_count                => l_msg_count,
            x_msg_data                 => l_msg_data,
            p_action_request_tbl       => l_action_request_tbl,
            p_header_rec               => l_header_rec,
            p_old_header_rec           => l_old_header_rec,
            p_line_tbl                 => l_line_tbl,
            p_old_line_tbl             => l_old_line_tbl,
            x_header_rec               => x_header_rec,
            x_header_val_rec           => x_header_val_rec,
            x_Header_Adj_tbl           => x_Header_Adj_tbl,
            x_Header_Adj_val_tbl       => x_Header_Adj_val_tbl,
            x_Header_price_Att_tbl     => x_Header_Price_Att_Tbl,
            x_Header_Adj_Att_tbl       => x_Header_Adj_Att_Tbl,
            x_Header_Adj_Assoc_tbl     => x_Header_Adj_Assoc_Tbl,
            x_Header_Scredit_tbl       => x_Header_Scredit_Tbl,
            x_Header_Scredit_val_tbl   => x_Header_Scredit_Val_Tbl,
            x_line_tbl                 => x_Line_Tbl,
            x_line_val_tbl             => x_Line_Val_Tbl,
            x_Line_Adj_tbl             => x_Line_Adj_Tbl,
            x_Line_Adj_val_tbl         => x_Line_Adj_Val_Tbl,
            x_Line_price_Att_tbl       => x_Line_Price_Att_Tbl,
            x_Line_Adj_Att_tbl         => x_Line_Adj_Att_Tbl,
            x_Line_Adj_Assoc_tbl       => x_Line_Adj_Assoc_Tbl,
            x_Line_Scredit_tbl         => x_Line_Scredit_Tbl,
            x_Line_Scredit_val_tbl     => x_Line_Scredit_Val_Tbl,
            x_Lot_Serial_tbl           => x_Lot_Serial_Tbl,
            x_Lot_Serial_val_tbl       => x_Lot_Serial_Val_Tbl,
            x_action_request_tbl       => x_action_request_tbl);

         IF l_return_status = FND_API.G_RET_STS_SUCCESS
         THEN
            o_msg_data := NULL;
            o_return_status := l_return_status;
            
            COMMIT;
            
            om_tax_util.calculate_tax (x_header_rec.header_id,
                                       l_return_status);
          
            IF     l_return_status = FND_API.G_RET_STS_SUCCESS
               --AND i_call_mode <> 'O' ---Ram Talluri 11/12/2013 TMS -20131112-00332
              --commented above and added below by Maha for ver 2.5
             AND i_call_mode  = 'T'
            THEN
            --commented below by Maha for ver 2.5
            /*   FOR c3_rec IN (SELECT b.line_number,
                                     b.attribute4,
                                     b.ordered_item_id,
                                     b.tax_value,
                                     b.attribute18 --Added by Ram Talluri on 10/8/2013 for TMS-20131003-00186
                                FROM oe_order_lines_all b
                               WHERE header_id = x_header_rec.header_id)
               LOOP
                  UPDATE xxwc_om_quote_lines
                     SET tax_amount = c3_rec.tax_value
                   WHERE     quote_number = i_quote_number
                         AND TO_CHAR (line_seq) = c3_rec.attribute18 --Added by Ram Talluri on 10/8/2013 for TMS-20131003-00186
                         AND inventory_item_id = c3_rec.ordered_item_id;
               END LOOP;*/
         
         --Added below by Maha for ver 2.5
               
             OPEN cur_load(x_header_rec.header_id);
        LOOP
            FETCH cur_load BULK COLLECT INTO l_om_tax_tbl_type;
            FORALL n IN 1..l_om_tax_tbl_type.COUNT
            
            UPDATE xxwc_om_quote_lines
                     SET tax_amount = l_om_tax_tbl_type(n).tax_amount
                   WHERE     quote_number = i_quote_number
                         AND TO_CHAR (line_seq) = l_om_tax_tbl_type(n).attribute18 
                         AND inventory_item_id =l_om_tax_tbl_type(n).inventory_item_id;
                         
            EXIT WHEN cur_load%NOTFOUND;
        END LOOP;
        CLOSE cur_load;      

               COMMIT;               
             
            ELSE
               o_msg_data := 'Error during tax calculation';
               o_return_status := l_return_status;
            END IF;

            --IF i_call_mode <> 'O' --Commented and added below by Maha for ver 2.5
            IF i_call_mode = 'T'
            THEN
               oe_order_pub.Delete_Order (
                  p_header_id        => x_header_rec.header_id,
                  p_org_id           => l_org_id,
                  p_operating_unit   => NULL                           -- MOAC
                                            ,
                  x_return_status    => l_return_status,
                  x_msg_count        => l_msg_count,
                  x_msg_data         => l_msg_data);

               IF l_return_status = FND_API.G_RET_STS_SUCCESS
               THEN
                  o_msg_data := NULL;
                  o_return_status := l_return_status;
                  COMMIT;
               ELSE
                  o_msg_data :=
                        'Issue with deleting Order '
                     || x_header_rec.order_number;
                  o_return_status := l_return_status;
               END IF;          
    
  
           -- ELSE     --commented and added ELSIF for ver 2.5

              ELSIF i_call_mode = 'O'  THEN
               UPDATE xxwc_om_quote_headers
                  SET attribute2 = x_header_rec.order_number
                WHERE quote_number = i_quote_number; 
     

               COMMIT;
               o_msg_data :=
                  'Order Number ' || x_header_rec.order_number || ' created.';
               o_return_status := l_return_status;

--commented below for ver 2.5
--            END IF;
--            IF i_call_mode = 'O'   
--            THEN --Added by start Ram Talluri on 10/8/2013 for TMS-20131003-00186

               BEGIN
                  xxwc_om_quote_pkg.process_price_adjustments (
                     i_quote_number,
                     l_return_status,
                     l_return_msg);

                  IF l_return_status <> FND_API.G_RET_STS_SUCCESS
                  THEN
                     o_msg_data :=
                        'API failure in updating calculate price flag and price adjustments';
                     o_return_status := l_return_status;
                  ELSE         --Added else and commit by Maha
                  COMMIT;
                  END IF;
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     NULL;
                  WHEN OTHERS
                  THEN
                     o_msg_data :=
                        'API failure in updating calculate price flag and price adjustments';
                     o_return_status := l_return_status;
               END;

--commented below by Maha for ver 2.5

/*               BEGIN
                  xxwc_om_quote_pkg.update_calc_price_flag (i_quote_number,
                                                            l_return_status,
                                                            l_return_msg);

                  IF l_return_status <> FND_API.G_RET_STS_SUCCESS
                  THEN
                     o_msg_data :=
                        'API failure in updating calculate price flag';
                     o_return_status := l_return_status;
                  END IF;
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     NULL;
                  WHEN OTHERS
                  THEN
                     o_msg_data :=
                        'API failure in updating calculate price flag';
                     o_return_status := l_return_status;
               END; */

            END IF; --Added by end Ram Talluri on 10/8/2013 for TMS-20131003-00186

         ELSE
            -- Retrieve messages
            FOR i IN 1 .. l_msg_count
            LOOP
               Oe_Msg_Pub.get (p_msg_index       => i,
                               p_encoded         => Fnd_Api.G_FALSE,
                               p_data            => l_msg_data,
                               p_msg_index_out   => l_msg_index_out);
               lv_msg_data :=
                  lv_msg_data || ':' || SUBSTR (l_msg_data, 1, 100);
            END LOOP;

            o_msg_data := lv_msg_data;
            o_return_status := l_return_status;
         END IF;
      END LOOP;
   END calculate_tax;

   /*************************************************************************
      *   Function: xxwc_to_number (p_in_string VARCHAR2)
      *   PURPOSE:   Used to convert char value to to number.
      *
      *   REVISIONS:
      *   Ver        Date        Author                     Description
      *   ---------  ----------  ---------------         -------------------------
      *  1.0        10/08/2013  Ram Talluri               TMS-20131003-00186 - Custom Quote to SO Imports - Price Adjustments Missing
      **********************************************************************/
   FUNCTION xxwc_to_number (p_in_string VARCHAR2)
      RETURN NUMBER
   IS
      v_in_string       VARCHAR2 (2000);
      v_return_number   NUMBER := NULL;
   BEGIN
      --v_in_string := NVL (p_in_string, '0');
      v_in_string := p_in_string;

      BEGIN
         SELECT TO_NUMBER (v_in_string) INTO v_return_number FROM DUAL;
      EXCEPTION
         WHEN OTHERS
         THEN
            v_return_number := NULL;
      END;

      RETURN v_return_number;
   END xxwc_to_number;

   /*************************************************************************
      *  Procedure: process_price_adjustments
      *   PURPOSE:   Used to convert char value to to number.
      *
      *   REVISIONS:
      *   Ver        Date        Author                     Description
      *   ---------  ----------  ---------------         -------------------------
      *  1.0        10/08/2013  Ram Talluri               Added new function and a procedure -TMS-20131003-00186 - Custom Quote to SO Imports - Price Adjustments Missing
      *  2.2        02/19/2014  Ram Talluri                 TMS #20140219-00158 - Imoort price adjustments correctly
      *  2.3        04/10/2014  Maharajan Shunmugam         TMS# 20140407-00346  - Performace issue
      *  2.4        10/20/2014  Maharajan Shunmugam         TMS# 20141001-00162 Canada Multi Org changes
      **********************************************************************/
   PROCEDURE process_price_adjustments (i_quote_number    IN     NUMBER,
                                        o_return_status      OUT VARCHAR,
                                        o_msg_data           OUT VARCHAR)
   IS

        /* Initialize the proper Context */
--      l_org_id                         NUMBER := fnd_profile.VALUE ('ORG_ID');      -- commented and added below for ver# 2.4
--      l_application_id                 NUMBER := fnd_profile.VALUE ('RESP_APPL_ID');
        l_org_id                         NUMBER := MO_GLOBAL.GET_CURRENT_ORG_ID;
        l_application_id                 NUMBER := fnd_global.resp_appl_id; 
      /* MZ4Md211 */
     -- l_responsibility_id              NUMBER := fnd_profile.VALUE ('RESP_ID');         -- commented and added below for ver# 2.4
     -- l_user_id                        NUMBER := fnd_profile.VALUE ('USER_ID');
        l_responsibility_id              NUMBER := fnd_global.resp_id;
        l_user_id                        NUMBER := fnd_global.user_id;

      /* Initialize the record to G_MISS to enable defaulting */
      l_header_rec                     OE_ORDER_PUB.Header_Rec_Type
                                          := OE_ORDER_PUB.G_MISS_HEADER_REC;
      l_old_header_rec                 OE_ORDER_PUB.Header_Rec_Type;

      l_line_tbl                       OE_ORDER_PUB.Line_Tbl_Type;
      l_old_line_tbl                   OE_ORDER_PUB.Line_Tbl_Type;

      l_action_request_tbl             OE_ORDER_PUB.Request_Tbl_Type;

      l_line_Adj_Tbl                   oe_order_pub.line_adj_tbl_type;

      x_header_rec                     OE_ORDER_PUB.Header_Rec_Type;
      x_header_val_rec                 OE_ORDER_PUB.Header_Val_Rec_Type;
      x_Header_Adj_tbl                 OE_ORDER_PUB.Header_Adj_Tbl_Type;
      x_Header_Adj_val_tbl             OE_ORDER_PUB.Header_Adj_Val_Tbl_Type;
      x_Header_price_Att_tbl           OE_ORDER_PUB.Header_Price_Att_Tbl_Type;
      x_Header_Adj_Att_tbl             OE_ORDER_PUB.Header_Adj_Att_Tbl_Type;
      x_Header_Adj_Assoc_tbl           OE_ORDER_PUB.Header_Adj_Assoc_Tbl_Type;
      x_Header_Scredit_tbl             OE_ORDER_PUB.Header_Scredit_Tbl_Type;
      x_Header_Scredit_val_tbl         OE_ORDER_PUB.Header_Scredit_Val_Tbl_Type;
      x_line_tbl                       OE_ORDER_PUB.Line_Tbl_Type;
      x_line_val_tbl                   OE_ORDER_PUB.Line_Val_Tbl_Type;
      x_Line_Adj_tbl                   OE_ORDER_PUB.Line_Adj_Tbl_Type;
      x_Line_Adj_val_tbl               OE_ORDER_PUB.Line_Adj_Val_Tbl_Type;
      x_Line_price_Att_tbl             OE_ORDER_PUB.Line_Price_Att_Tbl_Type;
      x_Line_Adj_Att_tbl               OE_ORDER_PUB.Line_Adj_Att_Tbl_Type;
      x_Line_Adj_Assoc_tbl             OE_ORDER_PUB.Line_Adj_Assoc_Tbl_Type;
      x_Line_Scredit_tbl               OE_ORDER_PUB.Line_Scredit_Tbl_Type;
      x_Line_Scredit_val_tbl           OE_ORDER_PUB.Line_Scredit_Val_Tbl_Type;
      x_Lot_Serial_tbl                 OE_ORDER_PUB.Lot_Serial_Tbl_Type;
      x_Lot_Serial_val_tbl             OE_ORDER_PUB.Lot_Serial_Val_Tbl_Type;
      x_action_request_tbl             OE_ORDER_PUB.Request_Tbl_Type;
      
      l_om_tax_tbl_type       xxwc_om_quote_pkg.XXWC_OM_TAX_TBL_TYPE := xxwc_om_quote_pkg.G_MISS_XXWC_OM_TAX_TBL;  --Added below by Maha for ver 2.3

      l_return_status                  VARCHAR2 (2000);
      l_msg_count                      NUMBER;
      l_msg_data                       VARCHAR2 (2000);
      lv_msg_data                      VARCHAR2 (2000);

      l_line_cnt                       NUMBER := 0;
      l_top_model_line_index           NUMBER;
      l_link_to_line_index             NUMBER;

      l_msg_index_out                  NUMBER (10);

      l_record_exists                  VARCHAR2 (1) := 'N';

      l_control_rec                    oe_globals.control_rec_type;

      l_header_id                      NUMBER; ---Ram Talluri 11/12/2013 TMS -20131112-00332

      --added below variables  02/19/2014  Ram Talluri                 TMS #20140219-00158 - Imoort price adjustments correctly
      l_old_operand                    NUMBER;
      l_old_adjusted_amount            NUMBER;
      l_old_operand_per_pqty           NUMBER;
      l_old_adjusted_amount_per_pqty   NUMBER;
      l_old_arithmetic_operator        VARCHAR2 (100);
      
      CURSOR c1
      IS
         SELECT /*+ index(a XXWC_OE_ORDER_LN_LUD1) */--Ram Talluri 7/30/2014 TMS #20140609-00203 index hint updated to reflect the latest index name changes.
                oha.header_id,
                oha.order_number,
                ola.line_id,
                ola.UNIT_SELLING_PRICE,
                ola.unit_list_price,
                xoql.line_seq,
                xoql.inventory_item_id,
                xoql.average_cost,
                xoql.special_cost,
                --ROUND(xoql.gm_selling_price,3) gm_selling_price,--Rounding factor chnaged from 2 to 3 by Ram Talluri on 4/1/2014 for TMS #20140308-00014--version 2.6
                ROUND(xoql.gm_selling_price,5) gm_selling_price,
                xoql.attribute1,
                xoql.line_quantity,
                xoql.gm_percent,
                xoql.list_price,
                xoql.tax_amount,
                --added below column by Maha for ver 2.3
                /*NVL (      ROUND (
                             xxwc_om_quote_pkg.xxwc_to_number (
                                xoql.attribute1),
                             3),
                          ROUND (ola.unit_selling_price, 3)) gm_changed_selling_price*/--version 2.6
                NVL (      ROUND (
                             xxwc_om_quote_pkg.xxwc_to_number (
                                xoql.attribute1),
                             5),
                          ROUND (ola.unit_selling_price, 5)) gm_changed_selling_price--version 2.6
           FROM xxwc_om_quote_headers xoqh,
                oe_order_headers oha,
                oe_order_lines ola,
                xxwc_om_quote_lines xoql
          WHERE     1 = 1
                AND xoqh.attribute2 = TO_CHAR (oha.order_number)
                AND xoqh.quote_number = xoql.quote_number
                AND xoql.inventory_item_id = ola.inventory_item_id
                AND xoql.LINE_QUANTITY = ola.ordered_quantity
                AND xoqh.organization_id = ola.ship_from_org_id
                AND oha.header_id = ola.header_id
                --AND ola.creation_date >= SYSDATE - 0.006 -- commented and Added below by Maha for ver 2.3
              AND TRUNC(ola.last_update_date)>= TRUNC(SYSDATE)
               AND xoqh.quote_number = i_quote_number       --Added and commented below by Maha for ver 2.3
               -- AND xoql.quote_number = i_quote_number
                AND TO_CHAR (xoql.line_seq) = ola.attribute18;
                /*AND ROUND (xoql.gm_selling_price, 2) <>
                       NVL (
                          ROUND (
                             xxwc_om_quote_pkg.xxwc_to_number (
                                xoql.attribute1),
                             5),
                          ROUND (ola.unit_selling_price, 2));*/--Commented by Ram Talluri on 3/20/2014 for TMS #20140308-00014
--commented below by Maha for ver 2.3
   /*             AND ROUND (xoql.gm_selling_price, 3) <>
                       NVL (
                          ROUND ( xxwc_om_quote_pkg.xxwc_to_number (
                                xoql.attribute1),
                             3),
                          ROUND (ola.unit_selling_price, 3));*/--Added by Ram Talluri on 3/20/2014 for TMS #20140308-00014
   --Added below by Maha for ver 2.3
   CURSOR cur_load(p_header_id NUMBER) IS
     SELECT b.line_number,
            b.attribute4,
            b.ordered_item_id,
            b.tax_value,
            b.ordered_quantity,
            b.attribute18
     FROM   oe_order_lines b
      WHERE header_id = p_header_id ;

 BEGIN
      fnd_global.apps_initialize (l_user_id,
                                  l_responsibility_id,
                                  l_application_id,
                                  NULL);
      mo_global.init ('ONT');
      mo_global.set_policy_context ('S', l_org_id);

      oe_msg_pub.initialize;

      l_record_exists := 'N';
   
      FOR c1_rec IN c1
      LOOP
         EXIT WHEN c1%NOTFOUND;

       --Added below by Maha for ver 2.3
       IF c1_rec.gm_selling_price <> c1_rec.gm_changed_selling_price
       THEN

         --added below block  02/19/2014  Ram Talluri                 TMS #20140219-00158 - Imoort price adjustments correctly

         BEGIN
            SELECT operand,
                   adjusted_amount,
                   operand_per_pqty,
                   adjusted_amount_per_pqty,
                   arithmetic_operator
              INTO l_old_operand,
                   l_old_adjusted_amount,
                   l_old_operand_per_pqty,
                   l_old_adjusted_amount_per_pqty,
                   l_old_arithmetic_operator
              FROM oe_price_adjustments opj
             WHERE     1 = 1
                   AND header_id = c1_rec.header_id
                   AND line_id = c1_rec.line_id
                   AND LIST_LINE_TYPE_CODE = 'DIS'
                   AND automatic_flag = 'Y';
         EXCEPTION
            WHEN OTHERS
            THEN
               l_old_operand := NULL;
               l_old_adjusted_amount := NULL;
               l_old_operand_per_pqty := NULL;
               l_old_adjusted_amount_per_pqty := NULL;
               l_old_arithmetic_operator := NULL;
               l_old_arithmetic_operator := NULL;
         END;


         l_line_cnt := l_line_cnt + 1;
         l_line_tbl (l_line_cnt) := OE_ORDER_PUB.G_MISS_LINE_REC;
         l_line_tbl (l_line_cnt).header_id := c1_rec.header_id;
         l_line_tbl (l_line_cnt).line_id := c1_rec.line_id;
         l_line_tbl (l_line_cnt).calculate_price_flag := 'N';
         l_line_tbl (l_line_cnt).unit_selling_price := c1_rec.gm_selling_price;
         l_line_tbl (l_line_cnt).operation := OE_GLOBALS.G_OPR_UPDATE;

         --Price adjustments
         l_line_Adj_Tbl (l_line_cnt) := OE_ORDER_PUB.G_MISS_LINE_ADJ_REC;
         l_line_Adj_Tbl (l_line_cnt).operation := OE_GLOBALS.G_OPR_CREATE;
         l_line_Adj_Tbl (l_line_cnt).price_adjustment_id := fnd_api.g_miss_num;
         l_Line_Adj_Tbl (l_line_cnt).header_id := c1_rec.header_id; --header_id of the sales order
         l_line_Adj_Tbl (l_line_cnt).line_id := c1_rec.line_id; --line_id of the sales order line
         l_line_Adj_Tbl (l_line_cnt).automatic_flag := 'N';
         l_line_Adj_Tbl (l_line_cnt).applied_flag := 'Y';
         l_line_Adj_Tbl (l_line_cnt).updated_flag := 'Y'; --Optional, this is the fixed flag.
         l_line_Adj_Tbl (l_line_cnt).list_header_id := 26473; --list_header_id of the adjustment -NEW_PRICE_DISCOUNT modifier
         l_line_Adj_Tbl (l_line_cnt).list_line_id := 22909; --list_line_id of the adjustment - NEW_PRICE_DISCOUNT modifier line
         l_line_Adj_Tbl (l_line_cnt).list_line_type_code := 'DIS';
         l_line_Adj_tbl (l_line_cnt).modifier_level_code := 'LINE';


         --added below logic for price adjustments-  02/19/2014  Ram Talluri TMS #20140219-00158 - Imoort price adjustments correctly
         --Rounding factor chnaged from 2 to 3 by Ram Talluri on 4/1/2014 for TMS #20140308-00014
         --Version 2.6 rounding factor changed from 3 to 5

         IF l_old_OPERAND IS NULL
         THEN
            l_line_Adj_Tbl (l_line_cnt).operand :=
               ROUND (
                    c1_rec.unit_list_price
                  - (c1_rec.unit_list_price - c1_rec.gm_selling_price),
                  5);

            l_line_Adj_Tbl (l_line_cnt).OPERAND_PER_PQTY :=
               ROUND (
                    c1_rec.unit_list_price
                  - (c1_rec.unit_list_price - c1_rec.gm_selling_price),
                  5);

            l_Line_Adj_Tbl (l_line_cnt).adjusted_amount :=
               ROUND (c1_rec.gm_selling_price - c1_rec.unit_list_price, 5);

            l_Line_Adj_Tbl (l_line_cnt).adjusted_amount_per_pqty :=
               ROUND (c1_rec.gm_selling_price - c1_rec.unit_list_price, 5);

            l_line_Adj_Tbl (l_line_cnt).arithmetic_operator := 'NEWPRICE';
         ELSIF     l_old_OPERAND IS NOT NULL
               AND l_old_ARITHMETIC_OPERATOR NOT IN ('%', 'AMT')
         THEN
            l_line_Adj_Tbl (l_line_cnt).operand :=
               ROUND (
                    c1_rec.unit_list_price
                  - (l_old_OPERAND - c1_rec.gm_selling_price),
                  5);

            l_line_Adj_Tbl (l_line_cnt).OPERAND_PER_PQTY :=
               ROUND (
                    c1_rec.unit_list_price
                  - (l_old_OPERAND - c1_rec.gm_selling_price),
                  5);

            l_Line_Adj_Tbl (l_line_cnt).adjusted_amount :=
               ROUND (c1_rec.gm_selling_price - l_old_OPERAND, 5);

            l_Line_Adj_Tbl (l_line_cnt).adjusted_amount_per_pqty :=
               ROUND (c1_rec.gm_selling_price - l_old_OPERAND, 5);

            l_line_Adj_Tbl (l_line_cnt).arithmetic_operator := 'NEWPRICE';
         ELSIF     l_old_OPERAND IS NOT NULL
               AND l_old_ARITHMETIC_OPERATOR IN ('%', 'AMT')
         THEN
            l_line_Adj_Tbl (l_line_cnt).operand :=
               ROUND (
                    c1_rec.unit_list_price
                  - (c1_rec.unit_selling_price - c1_rec.gm_selling_price),
                  5);

            l_line_Adj_Tbl (l_line_cnt).OPERAND_PER_PQTY :=
               ROUND (
                    c1_rec.unit_list_price
                  - (c1_rec.unit_selling_price - c1_rec.gm_selling_price),
                  5);

            l_Line_Adj_Tbl (l_line_cnt).adjusted_amount :=
               ROUND (c1_rec.gm_selling_price - c1_rec.unit_selling_price, 5);

            l_Line_Adj_Tbl (l_line_cnt).adjusted_amount_per_pqty :=
               ROUND (c1_rec.gm_selling_price - c1_rec.unit_selling_price, 5);

            l_line_Adj_Tbl (l_line_cnt).arithmetic_operator := 'NEWPRICE';
         END IF;

         --Commented old logic for price adjustments-  02/19/2014  Ram Talluri TMS #20140219-00158 - Imoort price adjustments correctly
         /*

                 l_Line_Adj_Tbl (l_line_cnt).adjusted_amount :=
                      c1_rec.gm_selling_price
                    - NVL (xxwc_om_quote_pkg.xxwc_to_number (c1_rec.attribute1),
                           c1_rec.unit_selling_price);

                 l_Line_Adj_Tbl (l_line_cnt).adjusted_amount_per_pqty :=
                      c1_rec.gm_selling_price
                    - NVL (xxwc_om_quote_pkg.xxwc_to_number (c1_rec.attribute1),
                           c1_rec.unit_selling_price);

                 l_line_Adj_Tbl (l_line_cnt).arithmetic_operator := 'NEWPRICE';

                 l_line_Adj_Tbl (l_line_cnt).operand :=
                      c1_rec.unit_list_price
                    + (  c1_rec.gm_selling_price
                       - NVL (xxwc_om_quote_pkg.xxwc_to_number (c1_rec.attribute1),
                              c1_rec.unit_selling_price));

                 l_line_Adj_Tbl (l_line_cnt).OPERAND_PER_PQTY :=
                      c1_rec.unit_list_price
                    + (  c1_rec.gm_selling_price
                       - NVL (xxwc_om_quote_pkg.xxwc_to_number (c1_rec.attribute1),
                              c1_rec.unit_selling_price));*/

       
         l_record_exists := 'Y';

    --ADDED below ELSIF by Maha for ver 2.3
        ELSIF c1_rec.gm_selling_price =  c1_rec.gm_changed_selling_price
        THEN
        
          l_line_cnt := l_line_cnt + 1;
         l_line_tbl (l_line_cnt) := OE_ORDER_PUB.G_MISS_LINE_REC;
         l_line_tbl (l_line_cnt).operation := OE_GLOBALS.G_OPR_UPDATE;
         l_line_tbl (l_line_cnt).header_id := c1_rec.header_id;
         l_line_tbl (l_line_cnt).line_id := c1_rec.line_id;
         l_line_tbl (l_line_cnt).calculate_price_flag := 'N';

         l_record_exists := 'Y';
         
         END IF;

      END LOOP;

      IF l_record_exists = 'Y' AND l_line_cnt > 0 
      THEN
         OE_ORDER_PUB.Process_Order (
            p_api_version_number       => 1,
            p_org_id                   => l_org_id,
            p_init_msg_list            => FND_API.G_TRUE,
            p_return_values            => FND_API.G_TRUE,
            p_action_commit            => FND_API.G_TRUE,
            x_return_status            => l_return_status,
            x_msg_count                => l_msg_count,
            x_msg_data                 => l_msg_data,
            p_action_request_tbl       => l_action_request_tbl,
            p_header_rec               => l_header_rec,
            --p_old_header_rec           => l_old_header_rec,
            p_line_tbl                 => l_line_tbl,
            --p_old_line_tbl             => l_old_line_tbl,
            p_line_adj_tbl             => l_line_Adj_tbl,
            x_header_rec               => x_header_rec,
            x_header_val_rec           => x_header_val_rec,
            x_Header_Adj_tbl           => x_Header_Adj_tbl,
            x_Header_Adj_val_tbl       => x_Header_Adj_val_tbl,
            x_Header_price_Att_tbl     => x_Header_Price_Att_Tbl,
            x_Header_Adj_Att_tbl       => x_Header_Adj_Att_Tbl,
            x_Header_Adj_Assoc_tbl     => x_Header_Adj_Assoc_Tbl,
            x_Header_Scredit_tbl       => x_Header_Scredit_Tbl,
            x_Header_Scredit_val_tbl   => x_Header_Scredit_Val_Tbl,
            x_line_tbl                 => x_Line_Tbl,
            x_line_val_tbl             => x_Line_Val_Tbl,
            x_Line_Adj_tbl             => x_Line_Adj_Tbl,
            x_Line_Adj_val_tbl         => x_Line_Adj_Val_Tbl,
            x_Line_price_Att_tbl       => x_Line_Price_Att_Tbl,
            x_Line_Adj_Att_tbl         => x_Line_Adj_Att_Tbl,
            x_Line_Adj_Assoc_tbl       => x_Line_Adj_Assoc_Tbl,
            x_Line_Scredit_tbl         => x_Line_Scredit_Tbl,
            x_Line_Scredit_val_tbl     => x_Line_Scredit_Val_Tbl,
            x_Lot_Serial_tbl           => x_Lot_Serial_Tbl,
            x_Lot_Serial_val_tbl       => x_Lot_Serial_Val_Tbl,
            x_action_request_tbl       => x_action_request_tbl);
      END IF;

      IF l_return_status = FND_API.G_RET_STS_SUCCESS        --Changed condition for ver 2.3 as API status is U
      THEN
         l_msg_data := 'Order_line Price adjustments created successfully';
         l_return_status := l_return_status;
         
        COMMIT;
         
         --commented below for ver 2.3
        /*   
         BEGIN                   ---Ram Talluri 11/12/2013 TMS -20131112-00332
            SELECT header_id
              INTO l_header_id
              FROM oe_order_headers_all
             WHERE order_number = (SELECT MAX (TO_NUMBER (attribute2))
                                     FROM xxwc.xxwc_om_quote_headers
                                    WHERE quote_number = i_quote_number);
         EXCEPTION
            WHEN OTHERS
            THEN
               l_msg_data := 'ERROR IN RETRIEVING ORDER NUMBER';
         END;

         om_tax_util.calculate_tax (l_header_id, l_return_status);
         
       
         COMMIT;                 ---Ram Talluri 11/12/2013 TMS -20131112-00332

         IF l_return_status = FND_API.G_RET_STS_SUCCESS
         THEN
            FOR c2_rec IN (SELECT b.line_number,
                                  b.attribute4,
                                  b.ordered_item_id,
                                  b.tax_value,
                                  b.ordered_quantity,
                                  b.attribute18
                             FROM oe_order_lines_all b
                            WHERE header_id = x_header_rec.header_id)
            LOOP
            
               UPDATE xxwc_om_quote_lines
                  SET tax_amount = c2_rec.tax_value
                WHERE     quote_number = i_quote_number
                      AND inventory_item_id = c2_rec.ordered_item_id
                      AND line_quantity = c2_rec.ordered_quantity
                      AND TO_CHAR (line_seq) = c2_rec.attribute18;
            END LOOP;
            
            COMMIT;
            
         ELSE 
            l_msg_data := 'Error during tax calculation';
            l_return_status := l_return_status;
            NULL;
         END IF; */
         
      ELSE
         -- Retrieve messages
         FOR i IN 1 .. l_msg_count
         LOOP
            Oe_Msg_Pub.get (p_msg_index       => i,
                            p_encoded         => Fnd_Api.G_FALSE,
                            p_data            => l_msg_data,
                            p_msg_index_out   => l_msg_index_out);
            lv_msg_data := lv_msg_data || ':' || SUBSTR (l_msg_data, 1, 100);
         END LOOP;

         l_msg_data := lv_msg_data;
         l_return_status := l_return_status;
      END IF;
    --commented below for ver 2.3  
  /*    IF l_record_exists = 'Y' AND l_line_cnt > 0
      THEN
         OE_ORDER_PUB.Process_Order (
            p_api_version_number       => 1,
            p_org_id                   => l_org_id,
            p_init_msg_list            => FND_API.G_TRUE,
            p_return_values            => FND_API.G_TRUE,
            p_action_commit            => FND_API.G_TRUE,
            x_return_status            => l_return_status,
            x_msg_count                => l_msg_count,
            x_msg_data                 => l_msg_data,
            p_action_request_tbl       => l_action_request_tbl,
            p_header_rec               => l_header_rec,
            --p_old_header_rec           => l_old_header_rec,
            p_line_tbl                 => l_line_tbl,
            --p_old_line_tbl             => l_old_line_tbl,
            --p_line_adj_tbl             => l_line_Adj_tbl,
            x_header_rec               => x_header_rec,
            x_header_val_rec           => x_header_val_rec,
            x_Header_Adj_tbl           => x_Header_Adj_tbl,
            x_Header_Adj_val_tbl       => x_Header_Adj_val_tbl,
            x_Header_price_Att_tbl     => x_Header_Price_Att_Tbl,
            x_Header_Adj_Att_tbl       => x_Header_Adj_Att_Tbl,
            x_Header_Adj_Assoc_tbl     => x_Header_Adj_Assoc_Tbl,
            x_Header_Scredit_tbl       => x_Header_Scredit_Tbl,
            x_Header_Scredit_val_tbl   => x_Header_Scredit_Val_Tbl,
            x_line_tbl                 => x_Line_Tbl,
            x_line_val_tbl             => x_Line_Val_Tbl,
            x_Line_Adj_tbl             => x_Line_Adj_Tbl,
            x_Line_Adj_val_tbl         => x_Line_Adj_Val_Tbl,
            x_Line_price_Att_tbl       => x_Line_Price_Att_Tbl,
            x_Line_Adj_Att_tbl         => x_Line_Adj_Att_Tbl,
            x_Line_Adj_Assoc_tbl       => x_Line_Adj_Assoc_Tbl,
            x_Line_Scredit_tbl         => x_Line_Scredit_Tbl,
            x_Line_Scredit_val_tbl     => x_Line_Scredit_Val_Tbl,
            x_Lot_Serial_tbl           => x_Lot_Serial_Tbl,
            x_Lot_Serial_val_tbl       => x_Lot_Serial_Val_Tbl,
            x_action_request_tbl       => x_action_request_tbl);
      END IF;

      IF l_return_status = FND_API.G_RET_STS_SUCCESS
      THEN
         l_msg_data := 'Order_line calculate price flag update is success';
         l_return_status := l_return_status;
         COMMIT;
      ELSE
         -- Retrieve messages
         FOR i IN 1 .. l_msg_count
         LOOP
            Oe_Msg_Pub.get (p_msg_index       => i,
                            p_encoded         => Fnd_Api.G_FALSE,
                            p_data            => l_msg_data,
                            p_msg_index_out   => l_msg_index_out);
            lv_msg_data := lv_msg_data || ':' || SUBSTR (l_msg_data, 1, 100);
         END LOOP;

         l_msg_data := lv_msg_data;
         l_return_status := l_return_status;
      END IF;  */
      
    --  END IF;
      
   EXCEPTION
      WHEN OTHERS
      THEN
         NULL;
   END process_price_adjustments;

   /*************************************************************************
      *  Procedure: update_calc_price_flag
      *   PURPOSE:   Used to convert char value to to number.
      *
      *   REVISIONS:
      *   Ver        Date        Author                     Description
      *   ---------  ----------  ---------------         -------------------------
      *  1.0        10/08/2013  Ram Talluri               Added new function and a procedure -TMS-20131003-00186 - Custom Quote to SO Imports - Price Adjustments Missing
      *  1.1        10/20/2014  Maharajan Shunmugam       TMS# 20141001-00162 Canada Multi Org Changes
      **********************************************************************/
   PROCEDURE update_calc_price_flag (i_quote_number    IN     NUMBER,
                                     o_return_status      OUT VARCHAR,
                                     o_msg_data           OUT VARCHAR)
   IS
      /* Initialize the proper Context */
     -- l_org_id                   NUMBER := fnd_profile.VALUE ('ORG_ID');              --commented and added below for ver#1.1
     -- l_application_id           NUMBER := fnd_profile.VALUE ('RESP_APPL_ID');
        l_org_id                   NUMBER := MO_GLOBAL.GET_CURRENT_ORG_ID;
        l_application_id           NUMBER := fnd_global.resp_appl_id; 

      /* MZ4Md211 */
     -- l_responsibility_id        NUMBER := fnd_profile.VALUE ('RESP_ID');               --commented and added below for ver#1.1
     -- l_user_id                  NUMBER := fnd_profile.VALUE ('USER_ID');
        l_responsibility_id        NUMBER := fnd_global.resp_id;
        l_user_id                  NUMBER := fnd_global.user_id;


      /* Initialize the record to G_MISS to enable defaulting */
      l_header_rec               OE_ORDER_PUB.Header_Rec_Type
                                    := OE_ORDER_PUB.G_MISS_HEADER_REC;
      l_old_header_rec           OE_ORDER_PUB.Header_Rec_Type;

      l_line_tbl                 OE_ORDER_PUB.Line_Tbl_Type;
      l_old_line_tbl             OE_ORDER_PUB.Line_Tbl_Type;

      l_action_request_tbl       OE_ORDER_PUB.Request_Tbl_Type;

      l_line_Adj_Tbl             oe_order_pub.line_adj_tbl_type;

      x_header_rec               OE_ORDER_PUB.Header_Rec_Type;
      x_header_val_rec           OE_ORDER_PUB.Header_Val_Rec_Type;
      x_Header_Adj_tbl           OE_ORDER_PUB.Header_Adj_Tbl_Type;
      x_Header_Adj_val_tbl       OE_ORDER_PUB.Header_Adj_Val_Tbl_Type;
      x_Header_price_Att_tbl     OE_ORDER_PUB.Header_Price_Att_Tbl_Type;
      x_Header_Adj_Att_tbl       OE_ORDER_PUB.Header_Adj_Att_Tbl_Type;
      x_Header_Adj_Assoc_tbl     OE_ORDER_PUB.Header_Adj_Assoc_Tbl_Type;
      x_Header_Scredit_tbl       OE_ORDER_PUB.Header_Scredit_Tbl_Type;
      x_Header_Scredit_val_tbl   OE_ORDER_PUB.Header_Scredit_Val_Tbl_Type;
      x_line_tbl                 OE_ORDER_PUB.Line_Tbl_Type;
      x_line_val_tbl             OE_ORDER_PUB.Line_Val_Tbl_Type;
      x_Line_Adj_tbl             OE_ORDER_PUB.Line_Adj_Tbl_Type;
      x_Line_Adj_val_tbl         OE_ORDER_PUB.Line_Adj_Val_Tbl_Type;
      x_Line_price_Att_tbl       OE_ORDER_PUB.Line_Price_Att_Tbl_Type;
      x_Line_Adj_Att_tbl         OE_ORDER_PUB.Line_Adj_Att_Tbl_Type;
      x_Line_Adj_Assoc_tbl       OE_ORDER_PUB.Line_Adj_Assoc_Tbl_Type;
      x_Line_Scredit_tbl         OE_ORDER_PUB.Line_Scredit_Tbl_Type;
      x_Line_Scredit_val_tbl     OE_ORDER_PUB.Line_Scredit_Val_Tbl_Type;
      x_Lot_Serial_tbl           OE_ORDER_PUB.Lot_Serial_Tbl_Type;
      x_Lot_Serial_val_tbl       OE_ORDER_PUB.Lot_Serial_Val_Tbl_Type;
      x_action_request_tbl       OE_ORDER_PUB.Request_Tbl_Type;

      l_return_status            VARCHAR2 (2000);
      l_msg_count                NUMBER;
      l_msg_data                 VARCHAR2 (2000);
      lv_msg_data                VARCHAR2 (2000);

      l_line_cnt                 NUMBER := 0;
      l_top_model_line_index     NUMBER;
      l_link_to_line_index       NUMBER;

      l_msg_index_out            NUMBER (10);

      l_record_exists            VARCHAR2 (1) := 'N';

      CURSOR c1
      IS
         SELECT oha.header_id,
                oha.order_number,
                ola.line_id,
                ola.UNIT_SELLING_PRICE,
                ola.unit_list_price,
                xoql.line_seq,
                xoql.inventory_item_id,
                xoql.average_cost,
                xoql.special_cost,
                xoql.gm_selling_price,
                xoql.attribute1,
                xoql.line_quantity,
                xoql.gm_percent,
                xoql.list_price,
                xoql.tax_amount
           FROM xxwc_om_quote_headers xoqh,
                oe_order_headers oha,
                oe_order_lines ola,
                xxwc_om_quote_lines xoql
          WHERE     1 = 1
                AND xoqh.attribute2 = TO_CHAR (oha.order_number)
                AND xoqh.quote_number = xoql.quote_number
                AND xoql.inventory_item_id = ola.inventory_item_id
                AND xoql.LINE_QUANTITY = ola.ordered_quantity
                AND xoqh.organization_id = ola.ship_from_org_id
                AND oha.header_id = ola.header_id
                AND ola.creation_date >= SYSDATE - 0.005
                --AND xoql.quote_number = i_quote_number Maha
                AND xoqh.quote_number = i_quote_number
                AND TO_CHAR (xoql.line_seq) = ola.attribute18
                /*AND ROUND (xoql.gm_selling_price, 3) =--Rounding factor chnaged from 2 to 3 by Ram Talluri on 4/1/2014 for TMS #20140308-00014
                       NVL (
                          ROUND (
                             xxwc_om_quote_pkg.xxwc_to_number (
                                xoql.attribute1),
                             3),
                          ROUND (ola.unit_selling_price, 3));--Rounding factor chnaged from 2 to 3 by Ram Talluri on 4/1/2014 for TMS #20140308-00014*/--version 2.6
                AND ROUND (xoql.gm_selling_price, 5) =--Rounding factor chnaged from 2 to 3 by Ram Talluri on 4/1/2014 for TMS #20140308-00014
                       NVL (
                          ROUND (
                             xxwc_om_quote_pkg.xxwc_to_number (
                                xoql.attribute1),
                             5),
                          ROUND (ola.unit_selling_price, 5));--version 2.6
   BEGIN
      fnd_global.apps_initialize (l_user_id,
                                  l_responsibility_id,
                                  l_application_id,
                                  NULL);
      mo_global.init ('ONT');
      mo_global.set_policy_context ('S', l_org_id);

      oe_msg_pub.initialize;

      l_record_exists := 'N';


      FOR c1_rec IN c1
      LOOP
         EXIT WHEN c1%NOTFOUND;
         l_line_cnt := l_line_cnt + 1;
         l_line_tbl (l_line_cnt) := OE_ORDER_PUB.G_MISS_LINE_REC;
         l_line_tbl (l_line_cnt).operation := OE_GLOBALS.G_OPR_UPDATE;
         l_line_tbl (l_line_cnt).header_id := c1_rec.header_id;
         l_line_tbl (l_line_cnt).line_id := c1_rec.line_id;
         l_line_tbl (l_line_cnt).calculate_price_flag := 'N';

         l_record_exists := 'Y';
      END LOOP;

      IF l_record_exists = 'Y' AND l_line_cnt > 0
      THEN
         OE_ORDER_PUB.Process_Order (
            p_api_version_number       => 1,
            p_org_id                   => l_org_id,
            p_init_msg_list            => FND_API.G_TRUE,
            p_return_values            => FND_API.G_TRUE,
            p_action_commit            => FND_API.G_TRUE,
            x_return_status            => l_return_status,
            x_msg_count                => l_msg_count,
            x_msg_data                 => l_msg_data,
            p_action_request_tbl       => l_action_request_tbl,
            p_header_rec               => l_header_rec,
            --p_old_header_rec           => l_old_header_rec,
            p_line_tbl                 => l_line_tbl,
            --p_old_line_tbl             => l_old_line_tbl,
            --p_line_adj_tbl             => l_line_Adj_tbl,
            x_header_rec               => x_header_rec,
            x_header_val_rec           => x_header_val_rec,
            x_Header_Adj_tbl           => x_Header_Adj_tbl,
            x_Header_Adj_val_tbl       => x_Header_Adj_val_tbl,
            x_Header_price_Att_tbl     => x_Header_Price_Att_Tbl,
            x_Header_Adj_Att_tbl       => x_Header_Adj_Att_Tbl,
            x_Header_Adj_Assoc_tbl     => x_Header_Adj_Assoc_Tbl,
            x_Header_Scredit_tbl       => x_Header_Scredit_Tbl,
            x_Header_Scredit_val_tbl   => x_Header_Scredit_Val_Tbl,
            x_line_tbl                 => x_Line_Tbl,
            x_line_val_tbl             => x_Line_Val_Tbl,
            x_Line_Adj_tbl             => x_Line_Adj_Tbl,
            x_Line_Adj_val_tbl         => x_Line_Adj_Val_Tbl,
            x_Line_price_Att_tbl       => x_Line_Price_Att_Tbl,
            x_Line_Adj_Att_tbl         => x_Line_Adj_Att_Tbl,
            x_Line_Adj_Assoc_tbl       => x_Line_Adj_Assoc_Tbl,
            x_Line_Scredit_tbl         => x_Line_Scredit_Tbl,
            x_Line_Scredit_val_tbl     => x_Line_Scredit_Val_Tbl,
            x_Lot_Serial_tbl           => x_Lot_Serial_Tbl,
            x_Lot_Serial_val_tbl       => x_Lot_Serial_Val_Tbl,
            x_action_request_tbl       => x_action_request_tbl);
      END IF;

      IF l_return_status = FND_API.G_RET_STS_SUCCESS
      THEN
         l_msg_data := 'Order_line calculate price flag update is success';
         l_return_status := l_return_status;
         COMMIT;
      ELSE
         -- Retrieve messages
         FOR i IN 1 .. l_msg_count
         LOOP
            Oe_Msg_Pub.get (p_msg_index       => i,
                            p_encoded         => Fnd_Api.G_FALSE,
                            p_data            => l_msg_data,
                            p_msg_index_out   => l_msg_index_out);
            lv_msg_data := lv_msg_data || ':' || SUBSTR (l_msg_data, 1, 100);
         END LOOP;

         l_msg_data := lv_msg_data;
         l_return_status := l_return_status;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         NULL;
   END update_calc_price_flag;
END xxwc_om_quote_pkg;
/
