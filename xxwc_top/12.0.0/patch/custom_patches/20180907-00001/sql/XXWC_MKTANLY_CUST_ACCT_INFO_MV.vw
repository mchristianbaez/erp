CREATE MATERIALIZED VIEW APPS.XXWC_MKTANLY_CUST_ACCT_INFO_MV AS 
/********************************************************************************************************************************
--   NAME:       apps.XXWC_MKTANLY_CUST_ACCT_INFO_MV
-- REVISIONS:
--   Ver        Date        Author           Description
--   ---------  ----------  ---------------  ------------------------------------
--   1.0        09/05/2018   P.Vamshidhar      TMS#20180907-00001 -  Building Customer Outbound MV's for Marketing Analytics
**********************************************************************************************************************************/
(SELECT * FROM APPS.XXWC_MKTANLY_CUST_ACCT_INFO_VW)
/