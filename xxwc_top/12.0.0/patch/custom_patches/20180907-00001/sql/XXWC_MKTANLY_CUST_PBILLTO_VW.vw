CREATE OR REPLACE VIEW APPS.XXWC_MKTANLY_CUST_PBILLTO_VW
AS
  /********************************************************************************************************************************
  --   NAME:       apps.XXWC_MKTANLY_CUST_PBILLTO_VW
  -- REVISIONS:
  --   Ver        Date        Author           Description
  --   ---------  ----------  ---------------  ------------------------------------
  --   1.0        09/05/2018   P.Vamshidhar      TMS#20180907-00001 -  Building Customer Outbound MV's for Marketing Analytics
  **********************************************************************************************************************************/
  SELECT hca.account_number Account_Number,
    hl.address1 Primary_Bill_To_Address1,
    hl.address2 Primary_Bill_To_Address2,
    hl.city Primary_Bill_To_City,
    hl.county Primary_Bill_To_County,
    hl.state Primary_Bill_To_State_Code,
    hl.postal_code Primary_Bill_To_Zip_Code,
    (SELECT phone_area_code
      ||phone_number
    FROM apps.hz_contact_Points
    WHERE contact_point_type='PHONE'
    AND OWNER_TABLE_NAME    ='HZ_PARTY_SITES'
    AND owner_table_id      =TO_CHAR(hps.party_site_id)
    AND rownum              =1
    ) Primary_Bill_To_Phone_Num,
    hcp.credit_classification Site_Classification,
    TRUNC(hcas.Last_Update_Date) Last_Update_Date,
    TRUNC(hcas.creation_date) Created_Date
  FROM APPS.HZ_CUST_ACCOUNTS hca,
    apps.hz_party_sites hps,
    apps.hz_cust_acct_sites_all hcas,
    apps.hz_cust_site_uses_all hcsu,
    apps.hz_locations hl,
    apps.hz_customer_profiles hcp
  WHERE 1=1
  AND hca.party_id           = hps.party_id
  AND hca.cust_account_id    = hcas.cust_account_id
  AND hcas.party_site_id     = hps.party_site_id
  AND hcas.cust_acct_site_id = hcsu.cust_acct_site_id
  AND hcsu.site_use_code     ='BILL_TO'
  AND hcsu.primary_flag      ='Y'
  AND hcsu.org_id            =162
  AND hps.location_id        = hl.location_id
  AND hcsu.site_use_id       = hcp.site_use_id (+) 
  /