CREATE OR REPLACE VIEW APPS.XXWC_MKTANLY_CUST_CON_ADDRS_VW
AS
  /********************************************************************************************************************************
  --   NAME:       apps.XXWC_MKTANLY_CUST_CON_ADDRS_VW
  -- REVISIONS:
  --   Ver        Date        Author           Description
  --   ---------  ----------  ---------------  ------------------------------------
  --   1.0        09/05/2018   P.Vamshidhar      TMS#20180907-00001 -  Building Customer Outbound MV's for Marketing Analytics
  **********************************************************************************************************************************/
  SELECT hca.account_number Account_Number,
    hp_cont.party_id contact_party_id,
    hp_cont.PERSON_FIRST_NAME First_Name,
    hp_cont.PERSON_LAST_NAME Last_Name,
    hp_cont.status Status,
    hp_add.address1 Address_1,
    hp_add.address2 Address_2,
    hp_add.city City,
    hp_add.state State,
    hp_add.postal_code Zip,
    hp_add.last_update_date Last_Update_Date,
    hp_add.creation_date created_date
  FROM apps.hz_cust_accounts hca,
    apps.hz_relationships hr,
    apps.hz_parties hp_cont,
    apps.hz_parties hp_add
  WHERE 1                   =1
  AND hca.party_id          = hr.object_id
  AND hr.object_table_name  ='HZ_PARTIES'
  AND hr.object_type        ='ORGANIZATION'
  AND hr.subject_id         = hp_cont.party_id
  AND hr.subject_type       ='PERSON'
  AND hr.subject_table_name ='HZ_PARTIES'
  AND hp_cont.party_type    ='PERSON'
  AND hr.party_id           = hp_add.party_id
  AND hp_add.party_type     ='PARTY_RELATIONSHIP'
  ORDER BY HCA.ACCOUNT_NUMBER 
  /