CREATE OR REPLACE VIEW APPS.XXWC_MKTANLY_CUST_SALESREP_VW
AS
  /********************************************************************************************************************************
  --   NAME:       apps.XXWC_MKTANLY_CUST_SALESREP_VW
  -- REVISIONS:
  --   Ver        Date        Author           Description
  --   ---------  ----------  ---------------  ------------------------------------
  --   1.0        09/05/2018   P.Vamshidhar      TMS#20180907-00001 -  Building Customer Outbound MV's for Marketing Analytics
  **********************************************************************************************************************************/
  SELECT DISTINCT RSA.SALESREP_NUMBER Salesrep,
    pers.Salesrep_phonenum Salesrep_Phone_number,
    pers.nt_id Salesrep_NTID,
    pers.email_address Salesrep_Email_address,
    pers.job_descr Salesrep_jobdesc,
    (SELECT MGR_NAME
    FROM APPS.XXWC_SR_SALESREP_HIERARCHY XSSH
    WHERE SR_ID = RSA.SALESREP_ID
    AND MGT_DESC='DM'
    ) Salesrep_District_Manager,
    (SELECT MGR_NAME
    FROM APPS.XXWC_SR_SALESREP_HIERARCHY XSSH
    WHERE SR_ID = RSA.SALESREP_ID
    AND MGT_DESC='DSM'
    ) Salesrep_Dist_Sales_Manager,
    (SELECT MGR_NAME
    FROM APPS.XXWC_SR_SALESREP_HIERARCHY XSSH
    WHERE SR_ID = RSA.SALESREP_ID
    AND MGT_DESC='RSM'
    ) Salesrep_Region_Sales_Manager,
    (SELECT MGR_NAME
    FROM APPS.XXWC_SR_SALESREP_HIERARCHY XSSH
    WHERE SR_ID = RSA.SALESREP_ID
    AND MGT_DESC='RVP'
    ) Salesrep_Region_Vice_President,
    RSA.LAST_UPDATE_DATE LAST_UPDATE_DATE,
    RSA.CREATION_DATE CREATED_DATE
  FROM apps.ra_salesreps_all RSA,
    (SELECT papf.person_id,
      xpea.work_telephone Salesrep_phonenum,
      papf.full_name,
      UPPER(xpea.nt_id) nt_id,
      xpea.email_address,
      xpea.job_descr
    FROM apps.per_all_people_f papf,
      XXCUS.XXCUSHR_PS_EMP_ALL_TBL xpea
    WHERE papf.employee_number = xpea.employee_number
    AND sysdate BETWEEN papf.effective_start_date AND papf.effective_end_date
    ) pers
  WHERE 1=1
    --AND RSA.org_id  =162
  AND rsa.person_id = pers.person_id (+)
  ORDER BY 1 
  /