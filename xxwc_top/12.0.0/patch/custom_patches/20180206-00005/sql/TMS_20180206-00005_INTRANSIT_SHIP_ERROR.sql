/*************************************************************************
  $Header TMS_20180206-00005_INTRANSIT_SHIP_ERROR.sql $
  Module Name: TMS_20180206-00005 Data Fix Script

  PURPOSE: Data Fix for transfer will not come off the intransit report

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        08-Mar-2018  Pattabhi Avula         TMS#20180206-00005

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000

BEGIN
DBMS_OUTPUT.put_line ('Before delete1');

DELETE FROM apps.rcv_headers_interface
WHERE GROUP_ID IN (SELECT GROUP_ID
FROM rcv_transactions_interface
WHERE requisition_line_id = 49312027);

DBMS_OUTPUT.put_line ('Records deleted1-' || SQL%ROWCOUNT);
COMMIT;

DBMS_OUTPUT.put_line ('Before delete2');

DELETE FROM apps.rcv_transactions_interface
WHERE requisition_line_id = 49312027;

DBMS_OUTPUT.put_line ('Records deleted2-' || SQL%ROWCOUNT);
COMMIT;
EXCEPTION
WHEN OTHERS
THEN
DBMS_OUTPUT.put_line ('Unable to delete ' || SQLERRM);
END;
/