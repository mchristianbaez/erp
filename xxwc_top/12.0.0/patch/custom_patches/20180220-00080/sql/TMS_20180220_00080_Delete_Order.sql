/******************************************************************************
  $Header TMS_20180220_00080_Data_Fix.sql $
  Module Name:Data Fix script for 20180220-00080

  PURPOSE: Data fix script for 20180220_00080 Delete Counter Order

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    -------------------------------
  1.0        21-Feb-2018  Krishna Kumar         20180220-00080

*******************************************************************************/

ALTER SESSION SET CURRENT_SCHEMA=apps;
/
BEGIN
   mo_global.set_policy_context ('S', '162');
END;
/
SET serveroutput ON SIZE 500000;
DECLARE

BEGIN

DELETE FROM apps.oe_order_headers_all WHERE order_number = 27286966;

dbms_output.put_line('Number of Rows Deleted '||SQL%ROWCOUNT);

COMMIT;

EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      dbms_output.put_line(substr(sqlerrm, 1, 240));
END;
/