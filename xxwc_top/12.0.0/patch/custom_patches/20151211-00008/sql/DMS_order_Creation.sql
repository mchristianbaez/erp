DECLARE
   dummy NUMBER;
   i number:=12345;
   cursor c_ord_hdr IS
       select        ORDER_SOURCE_ID,
                     CUST_PO_NUMBER,
                     SHIPPING_METHOD_CODE,
                     INVOICE_TO_CONTACT_ID,
                     SHIP_TO_CONTACT_ID,
                     SOLD_TO_CONTACT_ID,
                     ORIG_SYS_DOCUMENT_REF||'ORD08_2017' ORIG_SYS_DOCUMENT_REF,
                     ORDERED_DATE,
                     ORDER_TYPE_id,
                  --   sold_to_org_id,--CUSTOMER_ID,
                     CREATED_BY,
                     sysdate,--CREATION_DATE,
                     LAST_UPDATED_BY,
                     LAST_UPDATE_DATE,
                     SOLD_TO_ORG_ID,
                     SHIP_FROM_ORG_ID,
                     SHIP_TO_ORG_ID,
                     INVOICE_TO_ORG_ID,
                     BOOKED_FLAG ,
                     ATTRIBUTE7,
                     ORG_ID,
                     header_id
                     from oe_order_headers_all a
                     where 1=1
                     and order_type_id =1001
                     and trunc(creation_date) between to_date('05/01/2017','MM/DD/YYYY') and to_date('05/30/2017','MM/DD/YYYY')
                     and Shipping_method_code in ('000001_Our Truck_P_LTL', '000001_Our Truck_P_GND', '000001_WCD_P_LTL', '000001_BRANCH_LOO_P_GND', '000001_DC_LOOP_TR_P_GND')
                     and a.ship_from_org_id in (244,259)
					 and a.flow_status_code='CLOSED'
                     and exists (select 1 from oe_order_lines_all b
                         where a.header_id=b.header_id
						 and b.line_type_id in (1002,1003));
                 /*and exists (select 1 from mtl_system_items_b ms,oe_order_lines_all ol
                                    where 1=1
                                    and ol.header_id=a.header_id
                                    and ol.org_id=a.org_id
                                    and lot_control_code='1'
                                    and reservable_type='2'
                                    and SERIAL_NUMBER_CONTROL_CODE='1'
                                    and ms.organization_id=ol.ship_from_org_id
                                    and ms.inventory_item_id=ol.inventory_item_id);  */
    
   cursor c_ord_line(p_header_id number) IS             
   select ORDER_SOURCE_ID,
            INVOICE_TO_CONTACT_ID,
            SHIP_TO_CONTACT_ID,
            ORIG_SYS_DOCUMENT_REF||'ORD08_2017' ORIG_SYS_DOCUMENT_REF,
            ORIG_SYS_LINE_REF||'ORD08_2017' ORIG_SYS_LINE_REF,
            LINE_NUMBER,
            LINE_TYPE_id,
            INVENTORY_ITEM_ID,
            ORDERED_QUANTITY,
            UNIT_SELLING_PRICE,
            ATTRIBUTE4,
            UNIT_LIST_PRICE,
            CALCULATE_PRICE_FLAG,
            SOLD_TO_ORG_ID,
            SHIP_FROM_ORG_ID,
            SHIP_TO_ORG_ID,
            INVOICE_TO_ORG_ID,
            CREATED_BY,
            sysdate,--CREATION_DATE,
            LAST_UPDATED_BY,
            LAST_UPDATE_DATE,
            ATTRIBUTE2,
            ATTRIBUTE12,
            ORG_ID
        from oe_order_lines_all ol
        where 1=1
        and ol.header_id=p_header_id;
          /* and ol.line_type_id in (1002,1003)
        and header_id in (select Header_id     
                 from oe_order_headers_all
                 where  1=1
                 and order_type_id in (1001,1004)
                 and trunc(creation_date)=to_date('11/20/2013','MM/DD/YYYY')
                     and exists (select 1 from oe_order_lines_all b
                         where a.header_id=b.header_id
                         and b.line_type_id in (1002,1003));
                 /*and exists (select 1 from mtl_system_items_b ms
                                    where 1=1
                                    and lot_control_code='1'
                                    and reservable_type='2'
                                    and SERIAL_NUMBER_CONTROL_CODE='1'
                                    and ms.organization_id=ol.ship_from_org_id
                                    and ms.inventory_item_id=ol.inventory_item_id) ) ;*/  
                         
BEGIN
      

     FOR rec_so_hdr in c_ord_hdr LOOP
     
           i:=i+1;
     
        INSERT INTO OE_HEADERS_IFACE_ALL(ORDER_SOURCE_ID,
                 CUSTOMER_PO_NUMBER,
                 SHIPPING_METHOD_CODE,
                 INVOICE_TO_CONTACT_ID,
                 SHIP_TO_CONTACT_ID,
                 SOLD_TO_CONTACT_ID,
                 ORIG_SYS_DOCUMENT_REF,
                 ORDERED_DATE,
                 ORDER_TYPE_ID,
                 CUSTOMER_ID,
                 CREATED_BY,
                 CREATION_DATE,
                 LAST_UPDATED_BY,
                 LAST_UPDATE_DATE,
                 SOLD_TO_ORG_ID,
                 SHIP_FROM_ORG_ID,
                 SHIP_TO_ORG_ID,
                 INVOICE_TO_ORG_ID,
                 BOOKED_FLAG ,
                 --ATTRIBUTE1,
                 --ATTRIBUTE7,
				 ORG_ID)
                 VALUES
                (9,
                i,--rec_so_hdr.CUST_PO_NUMBER,
            rec_so_hdr.SHIPPING_METHOD_CODE,
            rec_so_hdr.INVOICE_TO_CONTACT_ID,
            rec_so_hdr.SHIP_TO_CONTACT_ID,
                rec_so_hdr.SOLD_TO_CONTACT_ID,
            rec_so_hdr.ORIG_SYS_DOCUMENT_REF,
            rec_so_hdr.ORDERED_DATE,
            rec_so_hdr.ORDER_TYPE_ID,
            rec_so_hdr.sold_to_org_id,
            5200,
            sysdate,
        5200,
            sysdate,
            rec_so_hdr.SOLD_TO_ORG_ID,
            rec_so_hdr.SHIP_FROM_ORG_ID,
            rec_so_hdr.SHIP_TO_ORG_ID,
            rec_so_hdr.INVOICE_TO_ORG_ID,
        rec_so_hdr.BOOKED_FLAG ,
        --rec_so_hdr.ATTRIBUTE1,
            --rec_so_hdr.ATTRIBUTE7,
        rec_so_hdr.ORG_ID);
       

      ---------------------------------------------------------------------------------
      -- Insert into OE_ACTIONS_IFACE_ALL
      ----------------------------------------------------------------------------------
        INSERT INTO OE_ACTIONS_IFACE_ALL
        (ORDER_SOURCE_ID,
        ORIG_SYS_DOCUMENT_REF,
        OPERATION_CODE,
        ORG_ID)
        VALUES
        (rec_so_hdr.ORDER_SOURCE_ID,
        rec_so_hdr.ORIG_SYS_DOCUMENT_REF,
        'BOOKED',
        rec_so_hdr.ORG_ID);
      
      
      FOR rec_ord_line IN c_ord_line(rec_so_hdr.header_id) LOOP
      

       INSERT INTO OE_LINES_IFACE_ALL
            (ORDER_SOURCE_ID,
            INVOICE_TO_CONTACT_ID,
            SHIP_TO_CONTACT_ID,
            ORIG_SYS_DOCUMENT_REF,
            ORIG_SYS_LINE_REF,
            LINE_NUMBER,
            LINE_TYPE_ID,
            INVENTORY_ITEM_ID,
            ORDERED_QUANTITY,
            UNIT_SELLING_PRICE,
            ATTRIBUTE4,
            UNIT_LIST_PRICE,
            CALCULATE_PRICE_FLAG,
            SOLD_TO_ORG_ID,
            SHIP_FROM_ORG_ID,
            SHIP_TO_ORG_ID,
            INVOICE_TO_ORG_ID,
            CREATED_BY,
            CREATION_DATE,
            LAST_UPDATED_BY,
            LAST_UPDATE_DATE,
            ATTRIBUTE2,
            ATTRIBUTE12,
			ORG_ID)
        VALUES(9,
rec_ord_line.INVOICE_TO_CONTACT_ID,
rec_ord_line.SHIP_TO_CONTACT_ID,
rec_ord_line.ORIG_SYS_DOCUMENT_REF,
rec_ord_line.ORIG_SYS_LINE_REF,
rec_ord_line.LINE_NUMBER,
rec_ord_line.LINE_TYPE_ID,
rec_ord_line.INVENTORY_ITEM_ID,
rec_ord_line.ORDERED_QUANTITY,
rec_ord_line.UNIT_SELLING_PRICE,
rec_ord_line.ATTRIBUTE4,
rec_ord_line.UNIT_LIST_PRICE,
rec_ord_line.CALCULATE_PRICE_FLAG,
rec_ord_line.SOLD_TO_ORG_ID,
rec_ord_line.SHIP_FROM_ORG_ID,
rec_ord_line.SHIP_TO_ORG_ID,
rec_ord_line.INVOICE_TO_ORG_ID,
5200,
sysdate,
5200,
sysdate,
rec_ord_line.ATTRIBUTE2,
rec_ord_line.ATTRIBUTE12,
rec_ord_line.ORG_ID);

      
      END LOOP; 
      
      END LOOP;
      
      COMMIT;

EXCEPTION
   WHEN NO_DATA_FOUND THEN
      NULL;
   WHEN OTHERS THEN
      NULL;
END;
/

