/*
 TMS:  20170825-00087
 Date: 08/28/2017
 Notes:  Increase field size for emp_full_name and accrual_status
*/
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
 --
 n_loc number :=0;
 l_sql varchar2(400) :=Null;
 --
BEGIN --Main Processing...
   --
   n_loc :=101;
   --
   begin
    --
    l_sql :='alter table xxcus.xxcus_concur_accruals_elig_je modify emp_full_name varchar2(150)';
    --
    EXECUTE IMMEDIATE l_sql ; 
    --
    dbms_output.put_line('Emp_Full_Name field size increased to 150');
    --
    n_loc :=102;
    --
   exception
    when others then
     --
     n_loc :=103;
     --
     dbms_output.put_line('@ n_loc ='||n_loc||', message ='||sqlerrm);
     --
     rollback to start1;
     --     
   end;
   --
   --
   begin
    --
    l_sql :='alter table xxcus.xxcus_concur_accruals_elig_je modify accrual_status varchar2(60)';
    --
    EXECUTE IMMEDIATE l_sql ; 
    --
    dbms_output.put_line('Accrual_status field size increased to 60');
    --
    n_loc :=102;
    --
   exception
    when others then
     --
     n_loc :=103;
     --
     dbms_output.put_line('@ n_loc ='||n_loc||', message ='||sqlerrm);
     --
     rollback to start1;
     --     
   end;
   --   
EXCEPTION
   WHEN OTHERS THEN
      DBMS_OUTPUT.put_line ('TMS:  20170825-00087, Errors =' || SQLERRM);
END;
/