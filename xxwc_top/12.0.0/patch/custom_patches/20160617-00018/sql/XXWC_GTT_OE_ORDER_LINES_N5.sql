---------------------------------------------------------------------------------------------------------------------------
/*************************************************************************************************************************
  $Header XXWC_GTT_OE_ORDER_LINES_N5
  File Name: XXWC_GTT_OE_ORDER_LINES_N5.sql
  PURPOSE:   
  REVISIONS:
     Ver        Date         Author         Description
     ---------  ----------   --------    -----------------------------------------------------------------------------------
     1.0        20-Jun-16  Siva        TMS# 20160617-00018  --performance Tuning
****************************************************************************************************************************/
CREATE INDEX XXEIS.XXWC_GTT_OE_ORDER_LINES_N5 ON XXEIS.XXWC_GTT_OE_ORDER_LINES_TBL
  (
    PROCESS_ID,
    HEADER_ID,
    LINE_ID,
    INVENTORY_ITEM_ID,
    SHIP_FROM_ORG_ID
  )
  TABLESPACE APPS_TS_TX_DATA
/
