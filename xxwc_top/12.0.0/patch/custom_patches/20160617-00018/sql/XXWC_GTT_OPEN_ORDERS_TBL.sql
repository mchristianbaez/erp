---------------------------------------------------------------------------------------------------------------
/**************************************************************************************************************
  $Header XXEIS.XXWC_GTT_OPEN_ORDERS_TBL $
  Module Name : Order Management
  PURPOSE	  : Open Sales Orders Report
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0 	  20-June-2016         Venu			  TMS# 20160617-00018  --performance Tuning
**************************************************************************************************************/
CREATE TABLE XXEIS.XXWC_GTT_OPEN_ORDERS_TBL
  (
    PROCESS_ID             NUMBER,
    WAREHOUSE              VARCHAR2(3 BYTE),
    SALES_PERSON_NAME      VARCHAR2(240 BYTE),
    CREATED_BY             VARCHAR2(4000 BYTE),
    ORDER_NUMBER           NUMBER,
    ORDERED_DATE           DATE,
    LINE_CREATION_DATE     DATE,
    ORDER_TYPE             VARCHAR2(30 BYTE),
    QUOTE_NUMBER           NUMBER,
    ORDER_LINE_STATUS      VARCHAR2(4000 BYTE),
    ORDER_HEADER_STATUS    VARCHAR2(80 BYTE),
    CUSTOMER_NUMBER        VARCHAR2(30 BYTE),
    CUSTOMER_NAME          VARCHAR2(360 BYTE),
    CUSTOMER_JOB_NAME      VARCHAR2(40 BYTE),
    ORDER_AMOUNT           NUMBER,
    SHIPPING_METHOD        VARCHAR2(80 BYTE),
    SHIP_TO_CITY           VARCHAR2(60 BYTE),
    ZIP_CODE               VARCHAR2(60 BYTE),
    SCHEDULE_SHIP_DATE     DATE,
    HEADER_STATUS          VARCHAR2(30 BYTE),
    TRANSACTION_PHASE_CODE VARCHAR2(30 BYTE),
    ITEM_NUMBER            VARCHAR2(40 BYTE),
    ITEM_DESCRIPTION       VARCHAR2(240 BYTE),
    QTY                    NUMBER,
    PAYMENT_TERMS          VARCHAR2(15 BYTE),
    REQUEST_DATE           DATE,
    ORDER_HEADER_ID        NUMBER,
    ORDER_LINE_ID          NUMBER,
    CUST_ACCOUNT_ID        NUMBER,
    CUST_ACCT_SITE_ID      NUMBER,
    SALESREP_ID            NUMBER,
    PARTY_ID               NUMBER,
    TRANSACTION_TYPE_ID    NUMBER,
    MTP_ORGANIZATION_ID    NUMBER,
    HZCS_SHIP_TO_STE_ID    NUMBER,
    HZPS_SHP_TO_PRT_ID     NUMBER,
    USER_ITEM_DESCRIPTION  VARCHAR2(1000 BYTE),
    INVENTORY_ITEM_ID      NUMBER,
    HDR_SHIP_FROM_ORG_ID   NUMBER,
    LIN_SHIP_FROM_ORG_ID   NUMBER,
    HEADER_ID              NUMBER,
    LINE_ID                NUMBER,
    DELIVERY_DOC_DATE      DATE
  )
/
