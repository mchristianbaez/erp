---------------------------------------------------------------------------------------------------------------
/**************************************************************************************************************
  $Header XXEIS.XXWC_GTT_WSH_SHIPPING_STG_TBL $
  Module Name : Order Management
  PURPOSE	  : Open Sales Orders Report
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0 	  20-Jun-16         Siva			  TMS# 20160617-00018  --performance Tuning
**************************************************************************************************************/
CREATE TABLE XXEIS.XXWC_GTT_WSH_SHIPPING_STG_TBL
  (
    PROCESS_ID         NUMBER,
    HEADER_ID          NUMBER,
    LINE_ID            NUMBER,
    DELIVERY_ID        NUMBER,
    DELIVERY_DETAIL_ID NUMBER,
    INVENTORY_ITEM_ID  NUMBER,
    SHIP_FROM_ORG_ID   NUMBER,
    ORDERED_QUANTITY   NUMBER,
    FORCE_SHIP_QTY     NUMBER,
    TRANSACTION_QTY    NUMBER,
    LOT_NUMBER         VARCHAR2(80 BYTE),
    RESERVATION_ID     NUMBER,
    STATUS             VARCHAR2(240 BYTE),
    CREATED_BY         NUMBER,
    CREATION_DATE      DATE,
    LAST_UPDATED_BY    NUMBER,
    LAST_UPDATE_DATE   DATE,
    LAST_UPDATE_LOGIN  NUMBER,
    REQUEST_ID         NUMBER,
    ORG_ID             NUMBER
  )
/
