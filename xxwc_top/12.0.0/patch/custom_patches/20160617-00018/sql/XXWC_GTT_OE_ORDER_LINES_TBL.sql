---------------------------------------------------------------------------------------------------------------
/**************************************************************************************************************
  $Header XXEIS.XXWC_GTT_OE_ORDER_LINES_TBL $
  Module Name : Order Management
  PURPOSE	  : Open Sales Orders Report
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0 	  20-Jun-16         Siva			  TMS# 20160617-00018  --performance Tuning
**************************************************************************************************************/
CREATE TABLE XXEIS.XXWC_GTT_OE_ORDER_LINES_TBL
  (
    PROCESS_ID            NUMBER,
    LINE_ID               NUMBER NOT NULL ENABLE,
    HEADER_ID             NUMBER NOT NULL ENABLE,
    SOLD_FROM_ORG_ID      NUMBER,
    SHIP_FROM_ORG_ID      NUMBER,
    WAREHOUSE             VARCHAR2(30 BYTE),
    SHIPPING_METHOD_CODE  VARCHAR2(30 BYTE),
    ORDERED_QUANTITY      NUMBER,
    CREATION_DATE         DATE NOT NULL ENABLE,
    FLOW_STATUS_CODE      VARCHAR2(30 BYTE),
    UNIT_SELLING_PRICE    NUMBER,
    LINE_CATEGORY_CODE    VARCHAR2(30 BYTE) NOT NULL ENABLE,
    SCHEDULE_SHIP_DATE    DATE,
    INVENTORY_ITEM_ID     NUMBER NOT NULL ENABLE,
    ITEM_NUMBER           VARCHAR2(50 BYTE),
    USER_ITEM_DESCRIPTION VARCHAR2(1000 BYTE)
  )
/
