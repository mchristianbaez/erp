---------------------------------------------------------------------------------------------------------------
/**************************************************************************************************************
  $Header XXEIS.XXWC_GTT_EIS_PRINT_LOG_TBL $
  Module Name : Order Management
  PURPOSE	  : Open Sales Orders Report
  VERSION 	DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0 	  20-Jun-16         	Siva			  TMS# 20160617-00018  --performance Tuning
**************************************************************************************************************/
CREATE TABLE XXEIS.XXWC_GTT_EIS_PRINT_LOG_TBL
  (
    PROCESS_ID                   NUMBER,
    REQUEST_ID                   NUMBER,
    CONCURRENT_PROGRAM_ID        NUMBER,
    PROGRAM_APPLICATION_ID       NUMBER,
    CONCURRENT_PROGRAM_NAME      VARCHAR2(30 BYTE),
    USER_CONCURRENT_PROGRAM_NAME VARCHAR2(240 BYTE),
    PRINTER                      VARCHAR2(30 BYTE),
    PRINTER_DESCRIPTION          VARCHAR2(240 BYTE),
    REQUESTED_BY                 NUMBER,
    USER_NAME                    VARCHAR2(100 BYTE),
    USER_DESCRIPTION             VARCHAR2(240 BYTE),
    HEADER_ID                    VARCHAR2(240 BYTE),
    ORGANIZATION_ID              VARCHAR2(240 BYTE),
    TABLE_NAME                   VARCHAR2(30 BYTE),
    ACTUAL_COMPLETION_DATE       DATE,
    PHASE_CODE                   VARCHAR2(1 BYTE),
    STATUS_CODE                  VARCHAR2(1 BYTE),
    NUMBER_OF_COPIES             NUMBER,
    PHASE                        VARCHAR2(80 BYTE),
    STATUS                       VARCHAR2(80 BYTE),
    ARGUMENT1                    VARCHAR2(240 BYTE),
    ARGUMENT2                    VARCHAR2(240 BYTE),
    ARGUMENT3                    VARCHAR2(240 BYTE),
    ARGUMENT4                    VARCHAR2(240 BYTE),
    ARGUMENT5                    VARCHAR2(240 BYTE),
    ARGUMENT6                    VARCHAR2(240 BYTE),
    ARGUMENT7                    VARCHAR2(240 BYTE),
    ARGUMENT8                    VARCHAR2(240 BYTE),
    ARGUMENT9                    VARCHAR2(240 BYTE),
    ARGUMENT10                   VARCHAR2(240 BYTE),
    ARGUMENT11                   VARCHAR2(240 BYTE),
    ARGUMENT12                   VARCHAR2(240 BYTE),
    ARGUMENT13                   VARCHAR2(240 BYTE),
    ARGUMENT14                   VARCHAR2(240 BYTE),
    ARGUMENT15                   VARCHAR2(240 BYTE),
    ARGUMENT16                   VARCHAR2(240 BYTE),
    ARGUMENT17                   VARCHAR2(240 BYTE),
    ARGUMENT18                   VARCHAR2(240 BYTE),
    ARGUMENT19                   VARCHAR2(240 BYTE),
    ARGUMENT20                   VARCHAR2(240 BYTE),
    ARGUMENT21                   VARCHAR2(240 BYTE),
    ARGUMENT22                   VARCHAR2(240 BYTE),
    ARGUMENT23                   VARCHAR2(240 BYTE),
    ARGUMENT24                   VARCHAR2(240 BYTE),
    ARGUMENT25                   VARCHAR2(240 BYTE)
  )
/
