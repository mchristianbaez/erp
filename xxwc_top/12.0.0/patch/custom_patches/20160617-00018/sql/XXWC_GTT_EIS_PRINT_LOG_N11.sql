---------------------------------------------------------------------------------------------------------------------------
/*************************************************************************************************************************
  $Header XXWC_GTT_EIS_PRINT_LOG_N11
  File Name: XXWC_GTT_EIS_PRINT_LOG_N11.sql
  PURPOSE:   
  REVISIONS:
     Ver        Date         Author         Description
     ---------  ----------   --------    -----------------------------------------------------------------------------------
     1.0        20-Jun-16    Siva        TMS# 20160617-00018  --performance Tuning
****************************************************************************************************************************/
CREATE INDEX XXEIS.XXWC_GTT_EIS_PRINT_LOG_N11 ON XXEIS.XXWC_GTT_EIS_PRINT_LOG_TBL
  (
    PROCESS_ID,
    HEADER_ID,
    ORGANIZATION_ID
  )
  TABLESPACE APPS_TS_TX_DATA
/

