CREATE OR REPLACE 
PACKAGE BODY  XXEIS.EIS_XXWC_OM_OPEN_SALES_ORD_PKG
AS
     --//============================================================================
--//  
--// Change Request 			:: Performance Issue  
--//
--// Object Usage 				:: This Object Referred by "OPEN SALES ORDERS REPORT"
--//
--// Object Name         		:: EIS_XXWC_OM_OPEN_SALES_ORD_PKG
--//
--// Object Type         		:: Package Body
--//
--// Object Description  		:: This Package will trigger in before report and insert the values into Table
--//								and also provides some values in the view through functions.
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        SIVA  	    06/17/2016     Initial Build  --TMS#20160617-00018  --Performance Tuning 
--//============================================================================
FUNCTION get_created_by_name(p_employee_id IN NUMBER, p_date IN DATE) RETURN varchar2
IS
   --//============================================================================
--//
--// Object Name         :: get_created_by_name  
--//
--// Object Usage 		 :: This Object Referred by "OPEN SALES ORDERS REPORT"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This  function is created for EIS_XXWC_OM_OPEN_ORDERS_V View to get the Created by name value.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        SIVA  	  06/17/2016      Initial Build  --TMS#20160617-00018   --Performance Tuning 
--//============================================================================
L_HASH_INDEX varchar2(100);
L_SQL varchar2(32000);
begin
G_empolyee_name := NULL;


l_sql :='SELECT ppf.full_name
        FROM per_all_people_f ppf,fnd_user fu
       WHERE ppf.person_id = fu.employee_id
             and fu.user_id=:1
             and (TRUNC (:2) between TRUNC (PPF.EFFECTIVE_START_DATE)
                                     AND NVL (ppf.effective_end_date, :3))';
    BEGIN
        l_hash_index:=p_employee_id||'-'||p_date;
        G_EMPOLYEE_NAME := G_EMPOLYEE_NAME_VLDN_TBL(L_HASH_INDEX);
 exception
    when no_data_found
    THEN
    begin      
    EXECUTE IMMEDIATE L_SQL INTO G_empolyee_name USING p_employee_id,p_date,p_date;
    EXCEPTION WHEN NO_DATA_FOUND THEN
    G_EMPOLYEE_NAME :=null;
    WHEN OTHERS THEN
    G_EMPOLYEE_NAME :=null;
    END;      
                      l_hash_index:=p_employee_id||'-'||p_date;
                       G_EMPOLYEE_NAME_VLDN_TBL(L_HASH_INDEX) := G_EMPOLYEE_NAME;
    END;
     return  G_EMPOLYEE_NAME;
     EXCEPTION when OTHERS then
      G_EMPOLYEE_NAME:=null;
      RETURN  G_EMPOLYEE_NAME;

END GET_CREATED_BY_NAME;

   PROCEDURE OPEN_SALES_ORDER_PRE_PROC(P_PROCESS_ID          NUMBER,
                                       p_org_id              VARCHAR2,
                                       p_salesrep_name       varchar2,
                                       p_ordered_date_from   date,
                                       p_ordered_date_to     date,
                                       p_order_line_status   varchar2,
                                       p_order_header_status varchar2,
                                       p_order_type          varchar2,
                                       p_cust_num            varchar2,
                                       p_cust_name           varchar2,      
                                       p_cust_job_name       VARCHAR2,
                                       P_CREATED_BY          varchar2,
                                       p_shipping_method     varchar2,
                                       p_schedule_date_from  date,
                                       p_schedule_date_to    date,
                                       p_payment_terms       varchar2)
   IS
   --//============================================================================
--//
--// Object Name         :: OPEN_SALES_ORDER_PRE_PROC  
--//
--// Object Usage 		 :: This Object Referred by "OPEN SALES ORDERS REPORT"
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This  procedure is created based on the EIS_XXWC_OM_OPEN_ORDERS_V View.Due to the 
--// 						performance issue ,spilt the view definition based on the driving tables and driving data.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        SIVA  	       06/17/2016       Initial Build  --TMS#20160617-00018   --Performance Tuning 
--//============================================================================

      l_doc_date        DATE;
      V_ERRBUF          VARCHAR2(2000);
      L_SQL1            VARCHAR2(32000);
      L_SQL2            VARCHAR2(32000);
      L_SQL3            VARCHAR2(32000);
      L_SQL4            VARCHAR2(32000);
      L_SQL5            VARCHAR2(32000);
      L_SQL6            VARCHAR2(32000);
      
      
      L_CURSOR1 CURSOR_TYPE;
      L_CURSOR2 CURSOR_TYPE;
      L_CURSOR3 CURSOR_TYPE;
      L_CURSOR4 CURSOR_TYPE;
      L_CURSOR5 CURSOR_TYPE;
      L_CURSOR6 CURSOR_TYPE;
      
        L_PARM_SALESREP      VARCHAR2(32000);
        L_PARM_ORG           VARCHAR2(32000);
        L_PARM_OL_STATUS     VARCHAR2(32000);
        L_PARM_OH_STATUS     VARCHAR2(32000);
        L_PARM_ORDER_TYPE    VARCHAR2(32000);
        L_PARM_CUST_NUM      VARCHAR2(32000);
        L_PARM_CUST_NAME     VARCHAR2(32000);
        L_PARM_CUST_JOB_NAME VARCHAR2(32000);
        L_PARM_CREATED_BY    varchar2(32000);
        l_parm_shipping_method  varchar2(32000);
        l_parm_line_status      varchar2(32000);
        L_PARM_PAYMENT_TERMS    VARCHAR2(32000);
        
   Type View_Tab Is Table Of XXEIS.XXWC_GTT_OE_ORDER_HEADERS_TBL%Rowtype Index By Binary_Integer;
   G_VIEW_TAB      VIEW_TAB ;
   G_VIEW_RECORD   VIEW_TAB ;
   
   TYPE VIEW_TAB2 IS TABLE OF XXEIS.XXWC_GTT_OE_ORDER_LINES_TBL%ROWTYPE INDEX BY BINARY_INTEGER;
   G_VIEW_TAB2      VIEW_TAB2 ;
   G_VIEW_RECORD2   VIEW_TAB2 ;
   
   TYPE VIEW_TAB3 IS TABLE OF XXEIS.XXWC_GTT_WSH_SHIPPING_STG_TBL%ROWTYPE INDEX BY BINARY_INTEGER;
   G_VIEW_TAB3      VIEW_TAB3 ;
   G_VIEW_RECORD3   VIEW_TAB3 ;

   TYPE VIEW_TAB4 IS TABLE OF XXEIS.XXWC_GTT_EIS_PRINT_LOG_TBL%ROWTYPE INDEX BY BINARY_INTEGER;
   G_VIEW_TAB4      VIEW_TAB4 ;
   G_VIEW_RECORD4   VIEW_TAB4 ;
   
   TYPE VIEW_TAB5 IS TABLE OF XXEIS.XXWC_GTT_OPEN_ORDERS_TBL%ROWTYPE INDEX BY BINARY_INTEGER;
   G_VIEW_TAB5      VIEW_TAB5 ;
   G_VIEW_RECORD5   VIEW_TAB5 ;      
        
        
      CURSOR C1
      IS
         SELECT A.ROWID RID, A.*
           FROM XXEIS.XXWC_GTT_OPEN_ORDERS_TBL A
           where process_id = P_PROCESS_ID
            ;
 BEGIN
 
 G_VIEW_TAB.DELETE;
 G_VIEW_TAB2.DELETE;
 G_VIEW_TAB3.DELETE;
 G_VIEW_TAB4.DELETE;
 G_VIEW_TAB5.DELETE;
 
fnd_file.put_line(fnd_file.log,'before inserts xxwc_open_sales_order_pre_trig');
      
IF p_salesrep_name   !='All' THEN
     L_PARM_SALESREP:=' and name in  ('||xxeis.eis_rs_utility.get_param_values(p_salesrep_name)||' )'; 
ELSE
    L_PARM_SALESREP:=' and 1=1';
END IF;


IF p_order_header_status IS NOT NULL THEN

L_PARM_OH_STATUS:= ' and  OH.FLOW_STATUS_CODE in  ('||xxeis.eis_rs_utility.get_param_values(p_order_header_status)||' )';

else

L_PARM_OH_STATUS:='AND OH.FLOW_STATUS_CODE IN (''BOOKED'',
                            ''DRAFT'',
                            ''ENTERED'',
                            ''PENDING_CUSTOMER_ACCEPTANCE'')'  ;

end if;

IF p_order_type IS NOT NULL THEN

L_PARM_ORDER_TYPE:= ' AND OTT.NAME  in ('||xxeis.eis_rs_utility.get_param_values(p_order_type)||' )';

else

L_PARM_ORDER_TYPE:='  AND OTT.NAME IN (''STANDARD ORDER'',
                                  ''COUNTER ORDER'',
                                  ''RETURN ORDER'',
                                  ''REPAIR ORDER'',
                                  ''WC LONG TERM RENTAL'',
                                  ''WC SHORT TERM RENTAL''
                                  )'  ;

end if;

                 
IF p_cust_num IS NOT NULL THEN

L_PARM_CUST_NUM:= ' AND OH.CUSTOMER_NUMBER in ('||xxeis.eis_rs_utility.get_param_values(p_cust_num)||' )';

end if;


if p_cust_name is not null then

L_PARM_CUST_NAME:= ' AND OH.CUSTOMER_NAME in  ('||xxeis.eis_rs_utility.get_param_values(p_cust_name)||' )';

end if;


IF p_cust_job_name IS NOT NULL THEN

L_PARM_CUST_JOB_NAME:= ' AND OH.CUSTOMER_JOB_NAME in  ('||xxeis.eis_rs_utility.get_param_values(p_cust_job_name)||' )';

end if;

IF p_created_by IS NOT NULL THEN

L_PARM_CREATED_BY:= ' AND OH.CREATED_BY in  ('||xxeis.eis_rs_utility.get_param_values(p_created_by)||' )';

END IF;


IF p_shipping_method IS NOT NULL THEN

L_PARM_SHIPPING_METHOD:= ' AND flv_ship_mtd.meaning in  ('||xxeis.eis_rs_utility.get_param_values(p_shipping_method)||' )';

END IF;

                    
IF p_order_line_status IS NOT NULL THEN

L_PARM_LINE_STATUS:= ' AND apps.OE_LINE_STATUS_PUB.get_line_status (ol.line_id,ol.flow_status_code) in  ('||xxeis.eis_rs_utility.get_param_values(p_order_line_status)||' )';

END IF;

IF p_payment_terms IS NOT NULL THEN

L_PARM_PAYMENT_TERMS:= ' AND RAB.NAME in  ('||xxeis.eis_rs_utility.get_param_values(p_payment_terms)||' )';

END IF;



      --------------------------------------------------------------------------
      -- Insert into staging table XXEIS.XXWC_GTT_OE_ORDER_HEADERS_TBL
      --------------------------------------------------------------------------

       BEGIN

 L_SQL1:='SELECT /*+INDEX(HZCS_SHIP_TO HZ_CUST_SITE_USES_U1) INDEX(HZL_SHIP_TO HZ_LOCATIONS_U1) INDEX(HZPS_SHIP_TO HZ_PARTY_SITES_U1) INDEX(REP JTF_RS_SALESREPS_U1)*/
  '||P_PROCESS_ID||' process_id,
  Oh.Header_Id,
  OH.ORG_ID,
  OH.ORDER_TYPE_ID,
  OH.ORDER_NUMBER,
  OH.ORDERED_DATE,
  OH.REQUEST_DATE,
  OH.CREATION_DATE,
  OH.CREATED_BY,
  xxeis.EIS_XXWC_OM_OPEN_SALES_ORD_PKG.get_created_by_name(oh.created_by,oh.creation_date) CREATED_BY_NAME,
  OH.QUOTE_NUMBER,
  OH.SOLD_TO_ORG_ID,
  HZP.PARTY_ID,
  HCA.CUST_ACCOUNT_ID,
  HCA.ACCOUNT_NUMBER,
  NVL (HCA.ACCOUNT_NAME, HZP.PARTY_NAME)party_name,
  HZCS_SHIP_TO.LOCATION,
  HCAS_SHIP_TO.CUST_ACCT_SITE_ID,
  HZL_SHIP_TO.CITY,
  HZCS_SHIP_TO.SITE_USE_ID,
  HZPS_SHIP_TO.PARTY_SITE_ID,
  HZL_SHIP_TO.POSTAL_CODE,
  oh.FLOW_STATUS_CODE,
  OH.TRANSACTION_PHASE_CODE,
  OH.PAYMENT_TERM_ID,
  RAB.NAME PAYMENT_TERMS,
  OH.SALESREP_ID,
  REP.SALESREP_ID,
  REP.NAME SALES_PERSON_NAME,
  OH.SHIP_FROM_ORG_ID,
  OH.SHIP_TO_ORG_ID
FROM  OE_ORDER_HEADERS_ALL        OH,
      HZ_CUST_ACCOUNTS            HCA,
      HZ_PARTIES                  HZP,
      HZ_CUST_SITE_USES_ALL       HZCS_SHIP_TO,
      HZ_CUST_ACCT_SITES          HCAS_SHIP_TO,
      HZ_PARTY_SITES              HZPS_SHIP_TO,
      HZ_LOCATIONS                HZL_SHIP_TO,
      RA_TERMS_VL                 RAB,
      JTF_RS_SALESREPS                REP
--RA_SALESREPS                REP
WHERE HCA.PARTY_ID                 = HZP.PARTY_ID(+)
AND OH.SOLD_TO_ORG_ID              = HCA.CUST_ACCOUNT_ID(+)
AND oh.ship_to_org_id              = hzcs_ship_to.site_use_id(+)
AND hzcs_ship_to.cust_acct_site_id = hcas_ship_to.cust_acct_site_id(+)
AND HCAS_SHIP_TO.PARTY_SITE_ID     = HZPS_SHIP_TO.PARTY_SITE_ID(+)
AND HZL_SHIP_TO.LOCATION_ID(+)        = HZPS_SHIP_TO.LOCATION_ID
AND RAB.TERM_ID(+)                    = OH.PAYMENT_TERM_ID
AND OH.SALESREP_ID                 = REP.SALESREP_ID(+)
AND OH.ORG_ID                      = REP.ORG_ID(+)
and REP.ORG_ID =162
AND ('''||p_ordered_date_from||''' IS NULL
     OR (TRUNC(NVL(oh.ordered_date,oh.creation_date)) BETWEEN to_date('''||p_ordered_date_from||''',''DD-MON-YYYY'') AND to_date('''||p_ordered_date_to||''',''DD-MON-YYYY''))) -- Version# 1.0
             AND EXISTS (SELECT 1 -- Version# 1.0
                           FROM xxeis.ra_salesreps ras  
						   --xxeis.xxwc_gtt_salesreps_tbl stg
                          WHERE oh.salesrep_id = ras.salesrep_id
                          and ras.ORG_ID =162
                          '||L_PARM_SALESREP||')
                          '||L_PARM_CREATED_BY||'
                          '||L_PARM_PAYMENT_TERMS||'
                          '||L_PARM_OH_STATUS||'
--             AND OH.FLOW_STATUS_CODE IN (''BOOKED'',
--                                         ''DRAFT'',
--                                         ''ENTERED'',
--                                         ''PENDING_CUSTOMER_ACCEPTANCE'')
'  
;    


OPEN L_CURSOR1 FOR L_SQL1;
LOOP
  FETCH L_CURSOR1 BULK COLLECT INTO G_VIEW_TAB LIMIT 10000;
  IF G_VIEW_TAB.count >= 1 THEN
    FORALL I IN 1..G_VIEW_TAB.COUNT
    INSERT INTO XXEIS.XXWC_GTT_OE_ORDER_HEADERS_TBL VALUES G_VIEW_TAB(I);
    COMMIT;
  END IF;
  IF L_CURSOR1%NOTFOUND THEN
    CLOSE L_CURSOR1;
    EXIT;
  END IF;
END LOOP;
COMMIT;


EXCEPTION
WHEN OTHERS THEN
  FND_FILE.PUT_LINE (FND_FILE.LOG,'In others of xxwc_open_sales_order_pre_trig for headers' || SQLERRM);
End;


IF P_ORG_ID !='All' THEN

L_PARM_ORG:=' and organization_code in  ('||XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(P_ORG_ID)||' )';
ELSE
L_PARM_ORG:=' and 1=1';
END IF;

fnd_file.put_line (FND_FILE.LOG,'L_PARM_ORG  ' || p_org_id||'    '||L_PARM_ORG);
 


--------------------------------------------------------------------------
-- Insert into staging table XXEIS.XXWC_GTT_OE_ORDER_LINES_TBL
--------------------------------------------------------------------------
BEGIN

L_SQL2:='SELECT '||P_PROCESS_ID||' process_id,
                OL.LINE_ID,
                 OL.HEADER_ID,
                 OL.SOLD_FROM_ORG_ID,
                 ol.ship_from_org_id,
                 (SELECT mtp.organization_code
                    FROM mtl_parameters mtp
                   WHERE mtp.organization_id = ol.ship_from_org_id),
                 OL.SHIPPING_METHOD_CODE,
                 OL.ORDERED_QUANTITY,
                 OL.CREATION_DATE,
                 OL.FLOW_STATUS_CODE,
                 OL.UNIT_SELLING_PRICE,
                 OL.LINE_CATEGORY_CODE,
                 OL.SCHEDULE_SHIP_DATE,
                 OL.INVENTORY_ITEM_ID,
                 MSI.SEGMENT1,
                 MSI.DESCRIPTION
--                 ,(SELECT MSI.SEGMENT1
--                    FROM MTL_SYSTEM_ITEMS_B MSI
--                   WHERE     MSI.ORGANIZATION_ID = OL.SHIP_FROM_ORG_ID
--                         AND MSI.INVENTORY_ITEM_ID = OL.INVENTORY_ITEM_ID),
--                 (SELECT MSI.DESCRIPTION
--                    FROM MTL_SYSTEM_ITEMS_B MSI
--                   WHERE     MSI.ORGANIZATION_ID = OL.SHIP_FROM_ORG_ID
--                         AND MSI.INVENTORY_ITEM_ID = OL.INVENTORY_ITEM_ID)
            FROM OE_ORDER_LINES_ALL OL, XXEIS.XXWC_GTT_OE_ORDER_HEADERS_TBL OH,MTL_SYSTEM_ITEMS_B MSI
           WHERE     OL.HEADER_ID = OH.HEADER_ID
                 AND OH.process_id = '||P_PROCESS_ID||'
                 AND MSI.ORGANIZATION_ID(+) = OL.SHIP_FROM_ORG_ID
                 AND MSI.INVENTORY_ITEM_ID(+) = OL.INVENTORY_ITEM_ID
                 AND EXISTS (SELECT 1 
                                FROM MTL_PARAMETERS mp
                                   -- ,XXEIS.XXWC_GTT_ORGANIZATIONS_TEMP a 
                 WHERE ol.ship_from_org_id =mp.organization_id 
                 AND ('''||p_schedule_date_from||''' IS NULL
     OR (TRUNC(NVL(oh.ordered_date,oh.creation_date)) BETWEEN to_date('''||p_schedule_date_from||''',''DD-MON-YYYY'') AND to_date('''||p_schedule_date_to||''',''DD-MON-YYYY''))) -- Version# 1.0
 
                 '||L_PARM_ORG||'
--                 and mp.organization_id=a.ORGANIZATION_ID 
                 )
                 AND OL.FLOW_STATUS_CODE NOT IN (''CANCELLED'', ''CLOSED'')';


OPEN L_CURSOR2 FOR L_SQL2;
LOOP
FETCH L_CURSOR2 BULK COLLECT INTO G_VIEW_TAB2 LIMIT 10000;
IF G_VIEW_TAB2.count >= 1 THEN
FORALL I IN 1..G_VIEW_TAB2.COUNT
INSERT INTO XXEIS.XXWC_GTT_OE_ORDER_LINES_TBL VALUES G_VIEW_TAB2(I);
COMMIT;
END IF;
IF L_CURSOR2%NOTFOUND THEN
CLOSE L_CURSOR2;
EXIT;
END IF;
END LOOP;
COMMIT;

EXCEPTION
WHEN OTHERS THEN
 FND_FILE.put_line (FND_FILE.LOG, 'In others of xxwc_open_sales_order_pre_trig for lines' || SQLERRM);
END;

 FND_FILE.PUT_LINE (FND_FILE.LOG,'Start the error code');
      --------------------------------------------------------------------------
      -- Insert into staging table XXEIS.XXWC_GTT_WSH_SHIPPING_STG_TBL
      --------------------------------------------------------------------------
    begin

  L_SQL3:='SELECT '||P_PROCESS_ID||' process_id,
         XXWSS.HEADER_ID
        ,XXWSS.LINE_ID
        ,XXWSS.DELIVERY_ID
        ,XXWSS.DELIVERY_DETAIL_ID
        ,XXWSS.INVENTORY_ITEM_ID
        ,XXWSS.SHIP_FROM_ORG_ID
        ,XXWSS.ORDERED_QUANTITY
        ,XXWSS.FORCE_SHIP_QTY
        ,XXWSS.TRANSACTION_QTY
        ,XXWSS.LOT_NUMBER
        ,XXWSS.RESERVATION_ID
        ,XXWSS.STATUS
        ,XXWSS.CREATED_BY
        ,XXWSS.CREATION_DATE
        ,XXWSS.LAST_UPDATED_BY
        ,XXWSS.LAST_UPDATE_DATE
        ,XXWSS.LAST_UPDATE_LOGIN
        ,XXWSS.REQUEST_ID
        ,XXWSS.ORG_ID 
            FROM xxeis.XXWC_GTT_OE_ORDER_LINES_TBL XXWCOL,
                 xxwc.XXWC_WSH_SHIPPING_STG XXWSS
           WHERE     XXWCOL.process_id = '||P_PROCESS_ID||'    
                 AND XXWCOL.HEADER_ID = XXWSS.HEADER_ID
                 AND XXWCOL.LINE_ID   = XXWSS.LINE_ID
                 AND XXWCOL.INVENTORY_ITEM_ID = XXWSS.INVENTORY_ITEM_ID
                 AND XXWCOL.ship_from_org_id  = XXWSS.ship_from_org_id';
                 
                 

OPEN L_CURSOR3 FOR L_SQL3;
LOOP
FETCH L_CURSOR3 BULK COLLECT INTO G_VIEW_TAB3 LIMIT 10000;
IF G_VIEW_TAB3.count >= 1 THEN
FORALL I IN 1..G_VIEW_TAB3.COUNT
INSERT INTO XXEIS.XXWC_GTT_WSH_SHIPPING_STG_TBL VALUES G_VIEW_TAB3(I);
COMMIT;
END IF;
IF L_CURSOR3%NOTFOUND THEN
CLOSE L_CURSOR3;
EXIT;
END IF;
END LOOP;
COMMIT;

exception
when others then
 FND_FILE.put_line (FND_FILE.LOG,'In others of xxwc_open_sales_order_pre_trig for SHIPPING_TEMP_STG' || SQLERRM);           
 end;


      --------------------------------------------------------------------------
      -- Insert into staging table XXEIS.XXWC_GTT_EIS_PRINT_LOG_TBL
      --------------------------------------------------------------------------
 begin

L_SQL4:='SELECT  /*+ INDEX(xpl XXWC_PRINT_LOG_TBL_N7)*/
  '||P_PROCESS_ID||' process_id,              
  xpl.REQUEST_ID            
,xpl.CONCURRENT_PROGRAM_ID            
,xpl.PROGRAM_APPLICATION_ID            
,xpl.CONCURRENT_PROGRAM_NAME            
,xpl.USER_CONCURRENT_PROGRAM_NAME            
,xpl.PRINTER            
,xpl.PRINTER_DESCRIPTION            
,xpl.REQUESTED_BY            
,xpl.USER_NAME            
,xpl.USER_DESCRIPTION            
,xpl.HEADER_ID            
,xpl.ORGANIZATION_ID            
,xpl.TABLE_NAME            
,xpl.ACTUAL_COMPLETION_DATE            
,xpl.PHASE_CODE            
,xpl.STATUS_CODE            
,xpl.NUMBER_OF_COPIES            
,xpl.PHASE            
,xpl.STATUS            
,xpl.ARGUMENT1            
,xpl.ARGUMENT2            
,xpl.ARGUMENT3            
,xpl.ARGUMENT4            
,xpl.ARGUMENT5            
,xpl.ARGUMENT6            
,xpl.ARGUMENT7            
,xpl.ARGUMENT8            
,xpl.ARGUMENT9            
,xpl.ARGUMENT10            
,xpl.ARGUMENT11            
,xpl.ARGUMENT12            
,xpl.ARGUMENT13            
,xpl.ARGUMENT14            
,xpl.ARGUMENT15            
,xpl.ARGUMENT16            
,xpl.ARGUMENT17            
,xpl.ARGUMENT18            
,xpl.ARGUMENT19            
,xpl.ARGUMENT20            
,xpl.ARGUMENT21            
,xpl.ARGUMENT22            
,xpl.ARGUMENT23            
,xpl.ARGUMENT24            
,xpl.ARGUMENT25            
FROM XXWC.XXWC_PRINT_LOG_TBL xpl           
WHERE    xpl.concurrent_program_id IN (70401, 75404) --packing slip and internal packslip                 
AND xpl.program_application_id = 20005                              
AND EXISTS
  ( SELECT 1 FROM 
      XXEIS.XXWC_GTT_OE_ORDER_HEADERS_TBL OH            
WHERE   OH.PROCESS_ID     = '||P_PROCESS_ID||'                  
AND  OH.HEADER_ID         = XPL.HEADER_ID                  
AND OH.SHIP_FROM_ORG_ID   = XPL.ORGANIZATION_ID)               
';

                 
OPEN L_CURSOR4 FOR L_SQL4;
LOOP
FETCH L_CURSOR4 BULK COLLECT INTO G_VIEW_TAB4 LIMIT 10000;
IF G_VIEW_TAB4.count >= 1 THEN
FORALL I IN 1..G_VIEW_TAB4.COUNT
INSERT INTO XXEIS.XXWC_GTT_EIS_PRINT_LOG_TBL VALUES G_VIEW_TAB4(I);
COMMIT;
END IF;
IF L_CURSOR4%NOTFOUND THEN
CLOSE L_CURSOR4;
EXIT;
END IF;
END LOOP;
COMMIT;
         

exception
WHEN OTHERS THEN
FND_FILE.PUT_LINE (FND_FILE.LOG,'In others of xxwc_open_sales_order_pre_trig for log' || SQLERRM);

end;

      --------------------------------------------------------------------------
      -- Insert into staging table XXEIS.XXWC_GTT_OPEN_ORDERS_TBL
      --------------------------------------------------------------------------
      

begin

        L_SQL5:='SELECT /*+ INDEX(OL XXWC_GTT_OE_ORDER_LINES_N2)*/
                 '||P_PROCESS_ID||' process_id,
                 OL.warehouse,
                 OH.SALES_PERSON_NAME,
                 OH.CREATED_BY_NAME,
                 OH.ORDER_NUMBER,
                 TRUNC (OH.ORDERED_DATE),
                 TRUNC (ol.creation_date),
                 OTT.NAME,
                 OH.QUOTE_NUMBER,
                 apps.OE_LINE_STATUS_PUB.get_line_status (
                    ol.line_id,
                    ol.flow_status_code),
                 flv_order_status.meaning,
                 OH.CUSTOMER_NUMBER,
                 OH.CUSTOMER_NAME,
                 OH.CUSTOMER_JOB_NAME,
                 ROUND (
                    (  OL.UNIT_SELLING_PRICE
                     * DECODE (OL.LINE_CATEGORY_CODE,
                               ''RETURN'', OL.ORDERED_QUANTITY * -1,
                               OL.ORDERED_QUANTITY)),
                    2),
                 flv_ship_mtd.meaning,
                 OH.ship_to_city,
                 OH.zip_code,
                 OL.SCHEDULE_SHIP_DATE,
                 OH.FLOW_STATUS_CODE,
                 OH.TRANSACTION_PHASE_CODE,
                 OL.Item_number,
                 OL.USER_ITEM_DESCRIPTION,
                 DECODE (ol.line_category_code,
                         ''RETURN'', (NVL (Ol.ordered_quantity, 0) * -1),
                         NVL (Ol.ordered_quantity, 0)),
                 OH.Payment_terms,
                 oh.request_date,
                 Oh.Header_Id,
                 Ol.Line_Id,
                 OH.cust_account_id,
                 OH.Cust_Acct_Site_Id,
                 OH.ORG_SALESREP_ID,
                 OH.PARTY_ID,
                 ott.transaction_type_id,
                 (SELECT Mtp.Organization_Id
                    FROM mtl_parameters mtp
                   WHERE mtp.organization_id = ol.ship_from_org_id),
                 OH.SHIP_TO_STE_ID,
                 OH.SHP_TO_PRT_ID,
                 OL.USER_ITEM_DESCRIPTION,
                 ol.INVENTORY_ITEM_ID,
                 oh.ship_from_org_id,
                 ol.ship_from_org_id,
                 oh.header_id,
                 ol.line_id,
                 NULL
            FROM XXEIS.XXWC_GTT_OE_ORDER_HEADERS_TBL OH,
                 XXEIS.XXWC_GTT_OE_ORDER_LINES_TBL ol,
                 OE_TRANSACTION_TYPES_VL OTT,
                 FND_LOOKUP_VALUES_VL FLV_SHIP_MTD,
                 OE_LOOKUPS FLV_ORDER_STATUS
           WHERE     oh.process_id = '||P_PROCESS_ID||'
                 AND oh.header_id  = ol.header_id
                 AND oh.process_id = ol.process_id
                 AND OH.ORDER_TYPE_ID = OTT.TRANSACTION_TYPE_ID
                 AND OL.FLOW_STATUS_CODE NOT IN (''CLOSED'', ''CANCELLED'')
                 AND FLV_SHIP_MTD.LOOKUP_TYPE(+) = ''SHIP_METHOD''
                 AND FLV_SHIP_MTD.LOOKUP_CODE(+) = OL.SHIPPING_METHOD_CODE
                 AND flv_order_status.lookup_type = ''FLOW_STATUS''
                 AND flv_order_status.lookup_code = Oh.flow_status_code
                 '||L_PARM_SHIPPING_METHOD||'
                 '||l_parm_line_status||'
                 '||L_PARM_ORDER_TYPE||'
                 AND (   (    ol.FLOW_STATUS_CODE = ''ENTERED''
                          AND TRUNC (oh.creation_date) < TRUNC (SYSDATE))
                      OR OL.FLOW_STATUS_CODE <> ''ENTERED'')';
                      
                      
                      
OPEN L_CURSOR5 FOR L_SQL5;
LOOP
FETCH L_CURSOR5 BULK COLLECT INTO G_VIEW_TAB5 LIMIT 10000;
IF G_VIEW_TAB5.count >= 1 THEN
FORALL I IN 1..G_VIEW_TAB5.COUNT
INSERT INTO XXEIS.XXWC_GTT_OPEN_ORDERS_TBL VALUES G_VIEW_TAB5(I);
COMMIT;
END IF;
IF L_CURSOR5%NOTFOUND THEN
CLOSE L_CURSOR5;
EXIT;
END IF;
END LOOP;
COMMIT;

  exception
   when others then
   fnd_file.put_line (fnd_file.log,'In others of xxwc_open_sales_order_pre_trig for tmp' || sqlerrm);
   end;

   FND_FILE.PUT_LINE (FND_FILE.LOG,'L_SQL1'||L_SQL1);
   FND_FILE.PUT_LINE (FND_FILE.LOG,'L_SQL2'||L_SQL2);
   FND_FILE.PUT_LINE (FND_FILE.LOG,'L_SQL3'||L_SQL3);
   FND_FILE.PUT_LINE (FND_FILE.LOG,'L_SQL4'||L_SQL4);
   FND_FILE.PUT_LINE (FND_FILE.LOG,'L_SQL5'||L_SQL5);

      FOR i IN c1
      LOOP

     BEGIN
    L_SQL6:='SELECT MIN (xpl.actual_completion_date)
           FROM XXEIS.XXWC_GTT_EIS_PRINT_LOG_TBL xpl
          WHERE     xpl.process_id = '||P_PROCESS_ID||'
                AND xpl.header_id  = :1--i.header_id
                AND xpl.organization_id = :2--i.HDR_SHIP_FROM_ORG_ID
                AND EXISTS
                       (SELECT 1
                          FROM XXEIS.XXWC_GTT_WSH_SHIPPING_STG_TBL xws
                         WHERE     xws.process_id = '||P_PROCESS_ID||'
                               AND xws.header_id = :3 --i.header_id
                               AND xws.line_id   = :4 --i.line_id
                               --AND xws.inventory_item_id = i.ITEM_NUMBER
                               AND xws.inventory_item_id = :5 --i.inventory_item_id  --TMS#20140317-00281 Error in Open Sales Orders Report
                               AND xws.ship_from_org_id  = :6 --i.LIN_SHIP_FROM_ORG_ID
                               AND xpl.argument10 = TO_CHAR (xws.delivery_id))';
                               
  OPEN L_CURSOR6 FOR L_SQL6 USING I.HEADER_ID,I.HDR_SHIP_FROM_ORG_ID,I.HEADER_ID,I.LINE_ID,I.INVENTORY_ITEM_ID,I.LIN_SHIP_FROM_ORG_ID;
  FETCH L_CURSOR6  INTO L_DOC_DATE;
   EXECUTE IMMEDIATE  'UPDATE XXEIS.XXWC_GTT_OPEN_ORDERS_TBL xgot
            SET xgot.DELIVERY_DOC_DATE = :l_doc_date
          WHERE xgot.ROWID = :rid
              and xgot.process_id = '||P_PROCESS_ID||'
                ' using l_doc_date,i.rid;
   CLOSE L_CURSOR6; 
    commit; 

exception
when others then
 fnd_file.put_line (fnd_file.log,'In others of xxwc_open_sales_order_pre_trig xpl.actual_completion_date i.header_id...i.line_id...i.LIN_SHIP_FROM_ORG_ID...i.inventory_item_id..' || sqlerrm||i.header_id||'...'||i.line_id||'...'||i.item_number||'...'||i.lin_ship_from_org_id||i.inventory_item_id);
 end;
      END LOOP;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN


        v_errbuf :=
               'Error_Stack...'
            || CHR (10)
            || DBMS_UTILITY.format_error_stack ()
            || CHR (10)
            || ' Error_Backtrace...'
            || CHR (10)
            || DBMS_UTILITY.format_error_backtrace ();

   END OPEN_SALES_ORDER_PRE_PROC;
   -- Version# 1.3 < End

PROCEDURE CLEAR_TEMP_TABLES ( P_PROCESS_ID IN NUMBER)
AS
--//============================================================================
--//
--// Object Name         :: CLEAR_TEMP_TABLES  
--//
--// Object Usage 		 :: This Object Referred by "OPEN SALES ORDERS REPORT"
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This Procedure will call in After report and delete data from Table
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Venu  				05-May-2016 	Initial Build   
--//============================================================================
  BEGIN  
  DELETE FROM XXEIS.XXWC_GTT_OE_ORDER_HEADERS_TBL WHERE PROCESS_ID=P_PROCESS_ID;
  DELETE FROM XXEIS.XXWC_GTT_OE_ORDER_LINES_TBL   WHERE PROCESS_ID=P_PROCESS_ID;
  DELETE FROM XXEIS.XXWC_GTT_WSH_SHIPPING_STG_TBL WHERE PROCESS_ID=P_PROCESS_ID;
  DELETE FROM XXEIS.XXWC_GTT_EIS_PRINT_LOG_TBL    WHERE PROCESS_ID=P_PROCESS_ID;
  DELETE FROM XXEIS.XXWC_GTT_OPEN_ORDERS_TBL      WHERE PROCESS_ID=P_PROCESS_ID;
  COMMIT;
  
END ;                                        


END EIS_XXWC_OM_OPEN_SALES_ORD_PKG;
/
