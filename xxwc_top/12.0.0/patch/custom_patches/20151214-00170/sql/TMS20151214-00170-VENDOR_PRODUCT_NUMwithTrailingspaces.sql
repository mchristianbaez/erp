/* Formatted on 12/14/2015 1:23:30 PM (QP5 v5.269.14213.34769) */
/*
 TMS: 20151214-00170  Unable to reapprove drop ship PO #1308330 
 vendor_product_num field has values with trailing spaces that needs to be trimmed.
 Po_line_id 5418527, 5418528
 */
SET SERVEROUTPUT ON SIZE 1000000


BEGIN
   DBMS_OUTPUT.put_line ('Before update');

   UPDATE po_lines_all
      SET VENDOR_PRODUCT_NUM = TRIM (VENDOR_PRODUCT_NUM)
    WHERE po_line_id IN (5418527, 5418528);

   COMMIT;
   DBMS_OUTPUT.put_line ('Records update-' || SQL%ROWCOUNT);
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Unable to create Supply record ' || SQLERRM);
END;
/
