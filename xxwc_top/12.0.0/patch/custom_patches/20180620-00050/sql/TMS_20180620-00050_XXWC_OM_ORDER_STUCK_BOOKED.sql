/*************************************************************************
 $Header TMS_20180620-00050_XXWC_OM_ORDER_STUCK_BOOKED.sql $
 Module Name: TMS_XXWC_OM_ORDER_STUCK_BOOKED.sql

 PURPOSE: Created to process the order headers stucked in booked status
 and lines are in CLOSED or CANCELLED status.

 REVISIONS:
 Ver        Date       Author           Description
 --------- ----------  ---------------  -------------------------
 1.0       07/02/2018  Pattabhi Avula   TMS#20180620-00050 - Header is stuck 
                                        in entered 27655442
 **************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000;
DECLARE
BEGIN
 DBMS_OUTPUT.put_line ('TMS: Datafix script , Before Update');
 
 -- Updating the headers table
	UPDATE oe_order_headers_all
	   SET flow_status_code = 'CLOSED'
           ,open_flag       = 'N'
	 WHERE order_number     = 27655442;
   
	COMMIT;


DBMS_OUTPUT.put_line ('Records update-' || SQL%ROWCOUNT);

	  DBMS_OUTPUT.put_line ('TMS: 20180620-00050  , End Update');
EXCEPTION
 WHEN OTHERS THEN
 DBMS_OUTPUT.put_line ('TMS: 20180620-00050 , Errors : ' || SQLERRM);
END;
/