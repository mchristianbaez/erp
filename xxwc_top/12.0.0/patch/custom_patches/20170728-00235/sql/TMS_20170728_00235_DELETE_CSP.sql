/*************************************************************************
 $Header TMS_20170728-00235_DELETE_CSP.sql $
 Module Name: TMS 20170728-00235 Data Fix script for CSP Deletion

 PURPOSE: Unable to import CSP # 315549--No permanent fix in process (But patch available per oracle)

 REVISIONS:
 Ver        Date        Author              Description
 ---------  ----------- ------------------  --------------------------
 1.0        03-Aug-2017 Krishna            TMS 20170728-00235 CSP Deletion 

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
BEGIN
 DBMS_OUTPUT.put_line ('TMS 20170728-00235 , Before Deletion');

delete from xxwc.xxwc_om_contract_pricing_lines
where agreement_id=315549;
--208 Rows deleted
 DBMS_OUTPUT.put_line (
 'TMS 20170728-00235 -delete the CSP (Expected:208): '
 || SQL%ROWCOUNT);

delete from xxwc.xxwc_om_contract_pricing_hdr
where agreement_id=315549;


 DBMS_OUTPUT.put_line (
 'TMS 20170728-00235 -delete the CSP (Expected:1): '
 || SQL%ROWCOUNT);

 COMMIT;

 DBMS_OUTPUT.put_line ('TMS 20170728-00235  , End Deletion');
EXCEPTION
 WHEN OTHERS
 THEN
 ROLLBACK;
 DBMS_OUTPUT.put_line ('TMS 20170728-00235 , Errors : ' || SQLERRM);
END;
/