/******************************************************************************
TABLE Name: XXWC_OM_SO_LINE_PICK_AUD_TBL_ALTR

PURPOSE: Added additional columns for XXWC_OM_SO_LINE_PICK_AUD_TBL table

HISTORY
===============================================================================
VERSION DATE          AUTHOR(S)              DESCRIPTION
------- -----------   --------------- -----------------------------------------
1.0     01/23/2018    Pattabhi Avula      TMS#20171011-00108 - Picking QC
                                          project - EBS - order information
   									      to APEX & Pick ticket changes -
                                          Initial Version
******************************************************************************/
ALTER TABLE XXWC.XXWC_OM_SO_LINE_PICK_AUD_TBL ADD (attribute11 VARCHAR2(240))
/