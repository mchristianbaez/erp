create or replace 
PACKAGE BODY      apps.xxwc_pick_qc_impr_mnts_pkg
AS
   /*************************************************************************
     $Header xxwc_pick_qc_impr_mnts_pkg$
     Module Name: xxwc_pick_qc_impr_mnts_pkg.pkb

     PURPOSE:   This package will call in Apex application to submit the PICk
                slip.

     REVISIONS:
     Ver     Date        Author            Description
     ------  ----------  ---------------   -------------------------
     1.0     03/05/2018    Pattabhi Avula    TMS#20171011-00108 - Initial Version
     *************************************************************************/
   g_pkg_name   VARCHAR2 (100) := 'xxwc_pick_qc_impr_mnts_pkg';

   /*************************************************************************
      *   Procedure : submit_pick_ticket
      *
      *   PURPOSE:   This procedure is called from APEX application to submit
      *              the pick slip report
      *  REVISIONS:
      *  Ver     Date        Author            Description
      *  ------  ----------  ---------------   -------------------------
      *  1.0     03/05/2018    Pattabhi Avula    TMS#20171011-00108 - Initial Version
      * ************************************************************************/
   PROCEDURE submit_pick_ticket (p_ntid           IN     VARCHAR2,
                                 p_order_number   IN     NUMBER,
                                 p_printer        IN     VARCHAR2,
                                 p_copies         IN     NUMBER,
                                 retcode             OUT VARCHAR2,
                                 errbuf              OUT VARCHAR2)
   IS
      V_BATCH_ID          NUMBER;
      V_GROUP_ID          NUMBER;
      V_USER_ID           NUMBER;
      V_RESP_ID           NUMBER;
      V_RESP_APPL_ID      NUMBER;
      V_HEADER_ID         NUMBER;
      V_ORDER_TYPE_ID     NUMBER;
      V_PRINTER           VARCHAR2 (150);
      V_DEFAULT_SHIPORG   NUMBER;
      l_print_batch_val   NUMBER;
      V_PRINT_PRICE       NUMBER;
      V_SHIP_TO_ORG_ID    NUMBER;
      V_ORG_PRINT_CHECK   VARCHAR2 (1);
      l_err_msg           VARCHAR2 (2000);
   BEGIN
      SELECT apps.xxwc_print_requests_s.NEXTVAL INTO V_BATCH_ID FROM DUAL;

      SELECT apps.xxwc_print_request_groups_s.NEXTVAL
        INTO V_GROUP_ID
        FROM DUAL;

      -- Fetching User Id details
      SELECT user_id
        INTO V_USER_ID
        FROM APPS.FND_USER
       WHERE user_name = UPPER (P_NTID);

      -- Fetching OM Responsibility details for the particular user
      SELECT r.application_id, r.responsibility_id
        INTO v_resp_appl_id, v_resp_id
        FROM apps.fnd_user u,
             apps.fnd_responsibility_tl r,
             apps.fnd_application_tl a,
             apps.fnd_application f,
             apps.wf_local_roles ur,
             apps.wf_user_role_assignments wu
       WHERE     u.user_name = wu.user_name
             AND wu.role_name = ur.name
             AND ur.display_name = r.responsibility_name
             AND a.application_id = r.application_id
             AND a.application_id = f.application_id
             AND wu.user_name = UPPER (p_ntid)
             AND (wu.end_date IS NULL OR wu.end_date >= SYSDATE)
             AND a.language = 'US'
             AND f.application_short_name = 'ONT'
             AND user_id = V_USER_ID                                   --30437
             AND ROWNUM = 1;

      -- Fetching default shipping org details for the user
      SELECT fpov.profile_option_value
        INTO V_DEFAULT_SHIPORG
        FROM fnd_profile_options fpo,
             fnd_profile_option_values fpov,
             fnd_user fu
       WHERE     fpov.profile_option_id = fpo.profile_option_id
             AND fpov.level_value = fu.user_id
             AND fpov.level_id = 10004
             AND fu.user_id = V_USER_ID
             AND fpo.PROFILE_OPTION_NAME = 'XXWC_OM_DEFAULT_SHIPPING_ORG';

      -- Fetching order details
      SELECT HEADER_ID, ORDER_TYPE_ID, SHIP_TO_ORG_ID
        INTO V_HEADER_ID, V_ORDER_TYPE_ID, V_SHIP_TO_ORG_ID
        FROM apps.oe_order_headers_all
       WHERE order_number = p_order_number;

      SELECT apps.xxwc_ont_routines_pkg.check_print_price (V_SHIP_TO_ORG_ID)
        INTO V_PRINT_PRICE
        FROM DUAL;

      -- Checking Reprint the report for multiple branches
      SELECT fpov.profile_option_value
        INTO V_ORG_PRINT_CHECK
        FROM fnd_profile_options fpo, fnd_profile_option_values fpov
       WHERE     fpov.profile_option_id = fpo.profile_option_id
             AND fpov.level_id = 10001
             AND fpo.PROFILE_OPTION_NAME = 'XXWC_REPRINT_MULTIPLE_BRANCHES';


      IF V_ORDER_TYPE_ID = 1011
      THEN
         BEGIN
            SELECT apps.xxwc_ont_routines_pkg.get_printer (
                      'XXWC',
                      'XXWC_INT_ORD_PICK',
                      V_DEFAULT_SHIPORG,
                      V_USER_ID,
                      V_RESP_ID,
                      V_RESP_APPL_ID)
              INTO V_PRINTER
              FROM DUAL;

            INSERT INTO XXWC.XXWC_PRINT_REQUESTS_TEMP (CREATED_BY,
                                                       CREATION_DATE,
                                                       LAST_UPDATED_BY,
                                                       LAST_UPDATE_DATE,
                                                       BATCH_ID,
                                                       PROCESS_FLAG,
                                                       GROUP_ID,
                                                       APPLICATION,
                                                       PROGRAM,
                                                       DESCRIPTION,
                                                       START_TIME,
                                                       SUB_REQUEST,
                                                       PRINTER,
                                                       STYLE,
                                                       COPIES,
                                                       SAVE_OUTPUT,
                                                       PRINT_TOGETHER,
                                                       VALIDATE_PRINTER,
                                                       TEMPLATE_APPL_NAME,
                                                       TEMPLATE_CODE,
                                                       TEMPLATE_LANGUAGE,
                                                       TEMPLATE_TERRITORY,
                                                       OUTPUT_FORMAT,
                                                       NLS_LANGUAGE)
                 VALUES (V_USER_ID                                --created_by
                                  ,
                         SYSDATE,
                         V_USER_ID                            --last_udated_by
                                  ,
                         SYSDATE,
                         V_BATCH_ID                                 --BATCH_ID
                                   ,
                         '1'                                    --PROCESS_FLAG
                            ,
                         V_GROUP_ID                                 --GROUP_ID
                                   ,
                         'XXWC'                                  --APPLICATION
                               ,
                         'XXWC_INT_ORD_PICK'                         --PROGRAM
                                            ,
                         'XXWC Internal Pick Slip Report'        --DESCRIPTION
                                                         ,
                         NULL                                     --START_TIME
                             ,
                         'FALSE'                                 --SUB_REQUEST
                                ,
                         NVL (UPPER (p_printer), V_PRINTER)          --PRINTER
                                                           ,
                         NULL                                          --STYLE
                             ,
                         p_copies                                     --COPIES
                                 ,
                         'TRUE'                                  --SAVE_OUTPUT
                               ,
                         'N'                                  --PRINT_TOGETHER
                            ,
                         'RESOLVE'                          --VALIDATE_PRINTER
                                  ,
                         'XXWC'                           --TEMPLATE_APPL_NAME
                               ,
                         'XXWC_INT_ORD_PICK'                   --TEMPLATE_CODE
                                            ,
                         'en'                              --TEMPLATE_LANGUAGE
                             ,
                         'US'                             --TEMPLATE_TERRITORY
                             ,
                         'PDF'                                 --OUTPUT_FORMAT
                              ,
                         'en'                                   --NLS_LANGUAGE
                             );

            -- Call this procedure to eliminate unwanted popup commit message
            xxwc_ascp_scwb_pkg.proccommit;

            INSERT INTO XXWC.XXWC_PRINT_REQUESTS_ARG_TEMP
                 VALUES (V_BATCH_ID,
                         1,
                         V_GROUP_ID,
                         1,
                         V_DEFAULT_SHIPORG);

            INSERT INTO XXWC.XXWC_PRINT_REQUESTS_ARG_TEMP
                 VALUES (V_BATCH_ID,
                         1,
                         V_GROUP_ID,
                         2,
                         V_HEADER_ID);

            INSERT INTO XXWC.XXWC_PRINT_REQUESTS_ARG_TEMP
                 VALUES (V_BATCH_ID,
                         1,
                         V_GROUP_ID,
                         3,
                         2);

            INSERT INTO XXWC.XXWC_PRINT_REQUESTS_ARG_TEMP
                 VALUES (V_BATCH_ID,
                         1,
                         V_GROUP_ID,
                         4,
                         NULL);

            INSERT INTO XXWC.XXWC_PRINT_REQUESTS_ARG_TEMP
                 VALUES (V_BATCH_ID,
                         1,
                         V_GROUP_ID,
                         5,
                         NULL);

            INSERT INTO XXWC.XXWC_PRINT_REQUESTS_ARG_TEMP
                 VALUES (V_BATCH_ID,
                         1,
                         V_GROUP_ID,
                         6,
                         2);

            INSERT INTO XXWC.XXWC_PRINT_REQUESTS_ARG_TEMP
                 VALUES (V_BATCH_ID,
                         1,
                         V_GROUP_ID,
                         7,
                         NULL);

            INSERT INTO XXWC.XXWC_PRINT_REQUESTS_ARG_TEMP
                 VALUES (V_BATCH_ID,
                         1,
                         V_GROUP_ID,
                         8,
                         NULL);

            INSERT INTO XXWC.XXWC_PRINT_REQUESTS_ARG_TEMP
                 VALUES (V_BATCH_ID,
                         1,
                         V_GROUP_ID,
                         9,
                         2);

            INSERT INTO XXWC.XXWC_PRINT_REQUESTS_ARG_TEMP
                 VALUES (V_BATCH_ID,
                         1,
                         V_GROUP_ID,
                         10,
                         NULL);

            INSERT INTO XXWC.XXWC_PRINT_REQUESTS_ARG_TEMP
                 VALUES (V_BATCH_ID,
                         1,
                         V_GROUP_ID,
                         11,
                         2);

            -- Call this procedure to eliminate unwanted popup commit message
            xxwc_ascp_scwb_pkg.proccommit;


            l_print_batch_val :=
               xxwc_ont_routines_pkg.submit_print_batch (V_BATCH_ID,
                                                         V_GROUP_ID,
                                                         1,
                                                         V_USER_ID,
                                                         V_RESP_ID,
                                                         V_RESP_APPL_ID);
         END;
      ELSIF V_ORDER_TYPE_ID = 1001
      THEN
         BEGIN
            SELECT apps.xxwc_ont_routines_pkg.get_printer (
                      'XXWC',
                      'XXWC_OM_PICK_SLIP',
                      V_DEFAULT_SHIPORG,
                      V_USER_ID,
                      V_RESP_ID,
                      V_RESP_APPL_ID)
              INTO V_PRINTER
              FROM DUAL;

            INSERT INTO XXWC.XXWC_PRINT_REQUESTS_TEMP (CREATED_BY,
                                                       CREATION_DATE,
                                                       LAST_UPDATED_BY,
                                                       LAST_UPDATE_DATE,
                                                       BATCH_ID,
                                                       PROCESS_FLAG,
                                                       GROUP_ID,
                                                       APPLICATION,
                                                       PROGRAM,
                                                       DESCRIPTION,
                                                       START_TIME,
                                                       SUB_REQUEST,
                                                       PRINTER,
                                                       STYLE,
                                                       COPIES,
                                                       SAVE_OUTPUT,
                                                       PRINT_TOGETHER,
                                                       VALIDATE_PRINTER,
                                                       TEMPLATE_APPL_NAME,
                                                       TEMPLATE_CODE,
                                                       TEMPLATE_LANGUAGE,
                                                       TEMPLATE_TERRITORY,
                                                       OUTPUT_FORMAT,
                                                       NLS_LANGUAGE)
                 VALUES (V_USER_ID                                --created_by
                                  ,
                         SYSDATE,
                         V_USER_ID                            --last_udated_by
                                  ,
                         SYSDATE,
                         V_BATCH_ID                                 --BATCH_ID
                                   ,
                         '1'                                    --PROCESS_FLAG
                            ,
                         V_GROUP_ID                                 --GROUP_ID
                                   ,
                         'XXWC'                                  --APPLICATION
                               ,
                         'XXWC_OM_PICK_SLIP'                         --PROGRAM
                                            ,
                         'XXWC OM Pick Slip Report'              --DESCRIPTION
                                                   ,
                         NULL                                     --START_TIME
                             ,
                         'FALSE'                                 --SUB_REQUEST
                                ,
                         NVL (UPPER (p_printer), V_PRINTER)          --PRINTER
                                                           ,
                         NULL                                          --STYLE
                             ,
                         p_copies                                     --COPIES
                                 ,
                         'TRUE'                                  --SAVE_OUTPUT
                               ,
                         'N'                                  --PRINT_TOGETHER
                            ,
                         'RESOLVE'                          --VALIDATE_PRINTER
                                  ,
                         'XXWC'                           --TEMPLATE_APPL_NAME
                               ,
                         'XXWC_OM_PICK_SLIP'                   --TEMPLATE_CODE
                                            ,
                         'en'                              --TEMPLATE_LANGUAGE
                             ,
                         'US'                             --TEMPLATE_TERRITORY
                             ,
                         'PDF'                                 --OUTPUT_FORMAT
                              ,
                         'en'                                   --NLS_LANGUAGE
                             );

            -- Call this procedure to eliminate unwanted popup commit message
            xxwc_ascp_scwb_pkg.proccommit;

            INSERT INTO XXWC.XXWC_PRINT_REQUESTS_ARG_TEMP
                 VALUES (V_BATCH_ID,
                         1,
                         V_GROUP_ID,
                         1,
                         V_DEFAULT_SHIPORG);

            INSERT INTO XXWC.XXWC_PRINT_REQUESTS_ARG_TEMP
                 VALUES (V_BATCH_ID,
                         1,
                         V_GROUP_ID,
                         2,
                         V_HEADER_ID);

            INSERT INTO XXWC.XXWC_PRINT_REQUESTS_ARG_TEMP
                 VALUES (V_BATCH_ID,
                         1,
                         V_GROUP_ID,
                         3,
                         V_PRINT_PRICE);

            INSERT INTO XXWC.XXWC_PRINT_REQUESTS_ARG_TEMP
                 VALUES (V_BATCH_ID,
                         1,
                         V_GROUP_ID,
                         4,
                         NULL);

            INSERT INTO XXWC.XXWC_PRINT_REQUESTS_ARG_TEMP
                 VALUES (V_BATCH_ID,
                         1,
                         V_GROUP_ID,
                         5,
                         NULL);

            INSERT INTO XXWC.XXWC_PRINT_REQUESTS_ARG_TEMP
                 VALUES (V_BATCH_ID,
                         1,
                         V_GROUP_ID,
                         6,
                         2);

            INSERT INTO XXWC.XXWC_PRINT_REQUESTS_ARG_TEMP
                 VALUES (V_BATCH_ID,
                         1,
                         V_GROUP_ID,
                         7,
                         NULL);

            INSERT INTO XXWC.XXWC_PRINT_REQUESTS_ARG_TEMP
                 VALUES (V_BATCH_ID,
                         1,
                         V_GROUP_ID,
                         8,
                         NULL);

            INSERT INTO XXWC.XXWC_PRINT_REQUESTS_ARG_TEMP
                 VALUES (V_BATCH_ID,
                         1,
                         V_GROUP_ID,
                         9,
                         2);

            INSERT INTO XXWC.XXWC_PRINT_REQUESTS_ARG_TEMP
                 VALUES (V_BATCH_ID,
                         1,
                         V_GROUP_ID,
                         10,
                         NULL);

            INSERT INTO XXWC.XXWC_PRINT_REQUESTS_ARG_TEMP
                 VALUES (V_BATCH_ID,
                         1,
                         V_GROUP_ID,
                         11,
                         2);

            -- Call this procedure to eliminate unwanted popup commit message
            xxwc_ascp_scwb_pkg.proccommit;

            l_print_batch_val :=
               xxwc_ont_routines_pkg.submit_print_batch (V_BATCH_ID,
                                                         V_GROUP_ID,
                                                         1,
                                                         V_USER_ID,
                                                         V_RESP_ID,
                                                         V_RESP_APPL_ID);
         END;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         l_err_msg := SQLERRM;
         retcode := 1;
         errbuf := l_err_msg;
   END submit_pick_ticket;

   /*************************************************************************
      *   Procedure : submit_badge_label
      *
      *   PURPOSE:   This procedure is called from APEX application to submit
      *              the badge label reports
      *  REVISIONS:
      *  Ver     Date        Author            Description
      *  ------  ----------  ---------------   -------------------------
      *  1.0     03/05/2018    Pattabhi Avula    TMS#20171011-00108 - Initial Version
      * ************************************************************************/
   PROCEDURE submit_badge_label (p_ntid          IN     VARCHAR2,
                                 p_action_type   IN     VARCHAR2,
                                 p_printer       IN     VARCHAR2,
                                 p_copies        IN     NUMBER,
                                 retcode            OUT VARCHAR2,
                                 errbuf             OUT VARCHAR2)
   IS
      V_USER_ID           NUMBER;
      V_RESP_ID           NUMBER;
      V_RESP_APPL_ID      NUMBER;
      V_DEFAULT_SHIPORG   NUMBER;
      l_err_msg           VARCHAR2 (2000);
      l_request_id        NUMBER;
      l_option_return     BOOLEAN;
      ln_request_id       NUMBER;
   BEGIN
      -- Fetching User Id details
      SELECT user_id
        INTO V_USER_ID
        FROM APPS.FND_USER
       WHERE user_name = UPPER (P_NTID);

      -- Fetching OM Responsibility details for the particular user
      SELECT r.application_id, r.responsibility_id
        INTO v_resp_appl_id, v_resp_id
        FROM apps.fnd_user u,
             apps.fnd_responsibility_tl r,
             apps.fnd_application_tl a,
             apps.fnd_application f,
             apps.wf_local_roles ur,
             apps.wf_user_role_assignments wu
       WHERE     u.user_name = wu.user_name
             AND wu.role_name = ur.name
             AND ur.display_name = r.responsibility_name
             AND a.application_id = r.application_id
             AND a.application_id = f.application_id
             AND wu.user_name = UPPER (p_ntid)
             AND (wu.end_date IS NULL OR wu.end_date >= SYSDATE)
             AND a.language = 'US'
             AND f.application_short_name = 'ONT'
             AND user_id = V_USER_ID                                   --30437
             AND ROWNUM = 1;

      fnd_global.apps_initialize (user_id        => V_USER_ID,
                                  resp_id        => v_resp_id,
                                  resp_appl_id   => v_resp_appl_id);
      /* Adding Template to the Concurrent Program */

      l_option_return :=
         fnd_request.add_layout (
            template_appl_name   => 'XXWC',
            template_code        => 'XXWC_OM_PQC_ACTIONS',
            template_language    => 'En',
            template_territory   => 'US',
            output_format        => 'PDF');

      /* Setting Printer Options , if we want print output of the Concurrent Program on Particular Printer*/
      l_option_return :=
         fnd_request.set_print_options (printer          => UPPER (p_printer),
                                        style            => 'LANDSCAPE',
                                        copies           => p_copies,
                                        save_output      => TRUE,
                                        print_together   => 'N');

      ln_request_id :=
         fnd_request.submit_request ('XXWC',                    -- application
                                     'XXWC_OM_PQC_ACTIONS', --'XXWC_OM_PROGRESS_CSO_LINES_CP', -- program short name--XXWC_OM_PROGRESS_SO_LINES
                                     '',                        -- description
                                     '',                         -- start time
                                     FALSE,                     -- sub request
                                     UPPER (p_ntid),       -- NTID - argument1
                                     p_action_type,      --Actions-- argument2
                                     p_copies,  --P_NUM_OF_COPIES - argument3,
                                     'S',           --P_STATUS_FLAG -argument4
                                     CHR (0)    -- represents end of arguments
                                            );
      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         l_err_msg := SQLERRM;
         retcode := 1;
         errbuf := l_err_msg;
   END submit_badge_label;

   /*************************************************************************
      *   Procedure : submit_load_check_label
      *
      *   PURPOSE:   This procedure is called from APEX application to submit
      *              the load check label report
      *  REVISIONS:
      *  Ver     Date        Author            Description
      *  ------  ----------  ---------------   -------------------------
      *  1.0     03/05/2018    Pattabhi Avula    TMS#20171011-00108 - Initial Version
      * ************************************************************************/
   PROCEDURE submit_load_check_label (p_request_date    IN     DATE,
                                      p_prepared_by     IN     VARCHAR2,
                                      p_checked_by      IN     VARCHAR2,
                                      p_customer_name   IN     VARCHAR2,
                                      p_customer_city   IN     VARCHAR2,
                                      p_order_number    IN     NUMBER,
                                      p_pallets         IN     NUMBER,
                                      p_bundles         IN     NUMBER,
                                      p_tools           IN     NUMBER,
                                      p_units           IN     NUMBER,
                                      p_num_of_labels   IN     NUMBER,
                                      p_status_flag     IN     VARCHAR2,
                                      p_print_date      IN     DATE,
                                      p_printer         IN     VARCHAR2,
                                      retcode              OUT VARCHAR2,
                                      errbuf               OUT VARCHAR2)
   IS
      V_USER_ID           NUMBER;
      V_RESP_ID           NUMBER;
      V_RESP_APPL_ID      NUMBER;
      V_DEFAULT_SHIPORG   NUMBER;
      l_err_msg           VARCHAR2 (2000);
      l_request_id        NUMBER;
      l_option_return     BOOLEAN;
      ln_request_id       NUMBER;
   BEGIN
      -- Fetching User Id details
      SELECT user_id
        INTO V_USER_ID
        FROM APPS.FND_USER
       WHERE description = p_checked_by;

      -- Fetching OM Responsibility details for the particular user
      SELECT r.application_id, r.responsibility_id
        INTO v_resp_appl_id, v_resp_id
        FROM apps.fnd_user u,
             apps.fnd_responsibility_tl r,
             apps.fnd_application_tl a,
             apps.fnd_application f,
             apps.wf_local_roles ur,
             apps.wf_user_role_assignments wu
       WHERE     u.user_name = wu.user_name
             AND wu.role_name = ur.name
             AND ur.display_name = r.responsibility_name
             AND a.application_id = r.application_id
             AND a.application_id = f.application_id
             AND (wu.end_date IS NULL OR wu.end_date >= SYSDATE)
             AND a.language = 'US'
             AND f.application_short_name = 'ONT'
             AND user_id = V_USER_ID                                   --30437
             AND ROWNUM = 1;

      fnd_global.apps_initialize (user_id        => V_USER_ID,
                                  resp_id        => v_resp_id,
                                  resp_appl_id   => v_resp_appl_id);
      /* Adding Template to the Concurrent Program */

      l_option_return :=
         fnd_request.add_layout (
            template_appl_name   => 'XXWC',
            template_code        => 'XXWC_OM_PQC_LOAD_CHK_LABLE',
            template_language    => 'En',
            template_territory   => 'US',
            output_format        => 'PDF');

      /* Setting Printer Options , if we want print output of the Concurrent Program on Particular Printer*/
      l_option_return :=
         fnd_request.set_print_options (printer          => UPPER (p_printer),
                                        style            => 'LANDSCAPE',
                                        copies           => p_num_of_labels,
                                        save_output      => TRUE,
                                        print_together   => 'N');

      ln_request_id :=
         fnd_request.submit_request ('XXWC',                    -- application
                                     'XXWC_OM_PQC_LOAD_CHK_LABLE', --'XXWC_OM_PQC_LOAD_CHK_LABLE', -- program short name--XXWC_OM_PQC_LOAD_CHK_LABLE
                                     '',                        -- description
                                     '',                         -- start time
                                     FALSE,                     -- sub request
                                     p_request_date,
                                     p_prepared_by,
                                     p_checked_by,
                                     p_customer_name,
                                     p_customer_city,
                                     p_order_number,
                                     p_pallets,
                                     p_bundles,
                                     p_tools,
                                     p_units,
                                     p_num_of_labels,
                                     p_status_flag,
                                     p_print_date,
                                     CHR (0)    -- represents end of arguments
                                            );
      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         l_err_msg := SQLERRM;
         retcode := 1;
         errbuf := l_err_msg;
   END submit_load_check_label;

	/*************************************************************************
      *   Procedure : save_updated_pick_ticket
      *
      *   PURPOSE:   This procedure is called from APEX application to save
    *                the pick ticket with the correct information or actions
      *  REVISIONS:
      *  Ver     Date        Author            Description
      *  ------  ----------  ---------------   -------------------------
      *  1.0     03/05/2018    Pattabhi Avula    TMS#20171011-00108 - Initial Version
      * ************************************************************************/

   PROCEDURE save_updated_pick_ticket (l_number_of_lines   IN     NUMBER, --Example: l_number_of_lines
                                       l_ordernum          IN     NUMBER, --Example: l_ordernum
                                       l_version           IN     NUMBER,
                                       l_ntid              IN     VARCHAR2, --Example: l_ntid
                                       l_action_type       IN     VARCHAR2, --Example: l_action_type
                                       l_action            IN     VARCHAR2,
                                       l_creation_date     IN     VARCHAR2,
                                       l_pallets           IN     NUMBER,
                                       l_bundles           IN     NUMBER,
                                       l_tools             IN     NUMBER,
                                       l_units             IN     NUMBER,
                                       l_line_errors       IN     NUMBER,
                                       l_notes             IN     VARCHAR2,
                                       retcode                OUT VARCHAR2,
                                       errbuf                 OUT VARCHAR2)
   IS
      l_original_lines   xxwc.XXWC_OM_PQC_PICKING_TBL.NUMBER_OF_LINES_PULLED%TYPE;
      l_partial_remain   xxwc.XXWC_OM_PQC_PICKING_TBL.NUMBER_OF_LINES_PULLED%TYPE;
   BEGIN
      IF l_action = 'ST_PCK' OR l_action_type = 'ST_PCK'
      THEN
         SELECT TOTAL_NUMBER_OF_LINES
           INTO l_original_lines
           FROM xxwc.XXWC_OM_PQC_PICKING_TBL
          WHERE     order_number = l_ordernum
                AND PICK_COMPLETION_DATE IS NULL
                AND PICK_TICKET_VERSION =
                       (SELECT MAX (PICK_TICKET_VERSION)
                          FROM XXWC.XXWC_OM_PQC_PICKING_TBL
                         WHERE order_number = l_ordernum)
                AND PICK_STATUS = 'PICK_OPEN';

         l_partial_remain := (l_original_lines - l_number_of_lines);



         IF l_partial_remain > 0
         THEN
            UPDATE xxwc.XXWC_OM_PQC_PICKING_TBL
               SET PICKED_BY_USER_NTID = UPPER (l_ntid),
                   PICKED_BY_USER_NAME =
                      (SELECT description
                         FROM APPLSYS.fnd_user
                        WHERE user_name = UPPER (l_ntid)),
                   PICK_START_DATE = SYSDATE,
                   TOTAL_NUMBER_OF_LINES = l_number_of_lines,
                   NUMBER_OF_LINES_PULLED = l_number_of_lines,
                   PICK_STATUS = 'BEING_PICKED_PARTIAL',
                   last_update_date = SYSDATE,
                   LAST_UPDATED_BY =
                      (SELECT USER_ID
                         FROM APPLSYS.fnd_user
                        WHERE user_name = UPPER (l_ntid))
             WHERE     order_number = l_ordernum
                   AND PICK_TICKET_VERSION =
                          (SELECT MAX (PICK_TICKET_VERSION)
                             FROM XXWC.XXWC_OM_PQC_PICKING_TBL
                            WHERE order_number = l_ordernum)
                   AND PICK_STATUS = 'PICK_OPEN'
                   AND PICK_COMPLETION_DATE IS NULL;

            COMMIT;



            INSERT INTO XXWC.XXWC_OM_PQC_PICKING_TBL (header_id,
                                                      order_number,
                                                      pick_ticket_version,
                                                      customer_name,
                                                      customer_number,
                                                      cust_account_id,
                                                      ship_from_org_id,
                                                      ship_from_org_code,
                                                      request_date,
                                                      freight_carrier_code,
                                                      customer_city,
                                                      total_number_of_lines,
                                                      number_of_lines_pulled,
                                                      pick_status,
                                                      picked_by_user_ntid,
                                                      picked_by_user_name,
                                                      pick_start_date,
                                                      pick_completion_date,
                                                      creation_date,
                                                      created_by,
                                                      last_update_date,
                                                      last_updated_by,
                                                      request_id)
               (SELECT header_id,
                       order_number,
                       pick_ticket_version,
                       customer_name,
                       customer_number,
                       cust_account_id,
                       ship_from_org_id,
                       ship_from_org_code,
                       request_date,
                       freight_carrier_code,
                       customer_city,
                       l_partial_remain,
                       '',
                       'PICK_OPEN',
                       '',
                       '',
                       '',
                       '',
                       creation_date,
                       created_by,
                       last_update_date,
                       last_updated_by,
                       request_id
                  FROM XXWC.XXWC_OM_PQC_PICKING_TBL
                 WHERE     order_number = l_ordernum
                       AND PICKED_BY_USER_NTID = UPPER (l_ntid)
                       AND PICK_STATUS = 'BEING_PICKED_PARTIAL'
                       AND PICK_TICKET_VERSION =
                              (SELECT (MAX (PICK_TICKET_VERSION))
                                 FROM XXWC.XXWC_OM_PQC_PICKING_TBL
                                WHERE order_number = l_ordernum));

            COMMIT;
         ELSE
            IF l_partial_remain = 0
            THEN --This is the START PICK stage where user enters that they are picking all of the lines -- This is a standard line.
               UPDATE xxwc.XXWC_OM_PQC_PICKING_TBL
                  SET PICKED_BY_USER_NTID = UPPER (l_ntid),
                      PICKED_BY_USER_NAME =
                         (SELECT description
                            FROM APPLSYS.fnd_user
                           WHERE user_name = UPPER (l_ntid)),
                      PICK_START_DATE = SYSDATE,
                      NUMBER_OF_LINES_PULLED = l_number_of_lines,
                      PICK_STATUS = 'BEING_PICKED',
                      last_update_date = SYSDATE,
                      LAST_UPDATED_BY =
                         (SELECT USER_ID
                            FROM APPLSYS.fnd_user
                           WHERE user_name = UPPER (l_ntid))
                WHERE     order_number = l_ordernum
                      AND PICK_TICKET_VERSION =
                             (SELECT MAX (PICK_TICKET_VERSION)
                                FROM XXWC.XXWC_OM_PQC_PICKING_TBL
                               WHERE order_number = l_ordernum)
                      AND PICK_STATUS = 'PICK_OPEN';
            END IF;
         END IF;



         COMMIT;
      ELSIF l_action = 'CX_PCK' OR l_action_type = 'CX_PCK'
      THEN
         SELECT COUNT (1)
           INTO l_original_lines
           FROM xxwc.XXWC_OM_PQC_PICKING_TBL
          WHERE     order_number = l_ordernum
                AND PICKED_BY_USER_NTID = UPPER (l_ntid)
                AND pick_ticket_version =
                       (SELECT MAX (PICK_TICKET_VERSION)
                          FROM XXWC.XXWC_OM_PQC_PICKING_TBL
                         WHERE order_number = l_ordernum)
                AND PICK_STATUS IN ('BEING_PICKED', 'BEING_PICKED_PARTIAL');

         IF l_original_lines > 0
         THEN
            UPDATE xxwc.XXWC_OM_PQC_PICKING_TBL
               SET PICKED_BY_USER_NTID = NULL,
                   PICKED_BY_USER_NAME = NULL,
                   PICK_START_DATE = NULL,
                   PICK_STATUS = 'PICK_OPEN',
                   NUMBER_OF_LINES_PULLED = NULL,
                   TOTAL_NUMBER_OF_LINES =
                      CASE
                         WHEN PICK_STATUS = 'BEING_PICKED'
                         THEN
                            NUMBER_OF_LINES_PULLED
                         WHEN PICK_STATUS = 'BEING_PICKED_PARTIAL'
                         THEN
                              NVL (
                                 (SELECT SUM (TOTAL_NUMBER_OF_LINES)
                                    FROM XXWC.XXWC_OM_PQC_PICKING_TBL
                                   WHERE     order_number = l_ordernum
                                         AND pick_ticket_version =
                                                (SELECT MAX (
                                                           PICK_TICKET_VERSION)
                                                   FROM XXWC.XXWC_OM_PQC_PICKING_TBL
                                                  WHERE order_number =
                                                           l_ordernum)
                                         AND PICK_STATUS = 'PICK_OPEN'),
                                 0)
                            + NUMBER_OF_LINES_PULLED
                      END,
                   LAST_UPDATE_DATE = SYSDATE,
                   LAST_UPDATED_BY =
                      (SELECT USER_ID
                         FROM APPLSYS.fnd_user
                        WHERE user_name = UPPER (l_ntid))
             WHERE     order_number = l_ordernum
                   AND pick_ticket_version =
                          (SELECT MAX (PICK_TICKET_VERSION)
                             FROM XXWC.XXWC_OM_PQC_PICKING_TBL
                            WHERE order_number = l_ordernum)
                   AND PICK_STATUS IN ('BEING_PICKED', 'BEING_PICKED_PARTIAL')
                   AND PICKED_BY_USER_NTID = UPPER (l_ntid);

            COMMIT;

            l_original_lines := NULL;

            SELECT MAX (TOTAL_NUMBER_OF_LINES)
              INTO l_original_lines
              FROM xxwc.XXWC_OM_PQC_PICKING_TBL
             WHERE     order_number = l_ordernum
                   AND pick_ticket_version =
                          (SELECT MAX (PICK_TICKET_VERSION)
                             FROM XXWC.XXWC_OM_PQC_PICKING_TBL
                            WHERE order_number = l_ordernum)
                   AND PICK_STATUS IN ('PICK_OPEN');

            BEGIN
               DELETE FROM xxwc.XXWC_OM_PQC_PICKING_TBL
                     WHERE     order_number = l_ordernum
                           AND pick_ticket_version =
                                  (SELECT MAX (PICK_TICKET_VERSION)
                                     FROM XXWC.XXWC_OM_PQC_PICKING_TBL
                                    WHERE order_number = l_ordernum)
                           AND PICK_STATUS IN ('PICK_OPEN')
                           AND TOTAL_NUMBER_OF_LINES <> l_original_lines;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  NULL;
            END;

            COMMIT;
         END IF;
      ELSIF l_action = 'C_PCK' OR l_action_type = 'C_PCK'
      THEN
         SELECT TOTAL_NUMBER_OF_LINES
           INTO l_original_lines
           FROM xxwc.XXWC_OM_PQC_PICKING_TBL
          WHERE     order_number = l_ordernum
                AND PICKED_BY_USER_NTID = UPPER (l_ntid)
                AND PICK_COMPLETION_DATE IS NULL
                AND PICK_TICKET_VERSION =
                       (SELECT MAX (PICK_TICKET_VERSION)
                          FROM XXWC.XXWC_OM_PQC_PICKING_TBL
                         WHERE order_number = l_ordernum)
                AND PICK_STATUS IN ('BEING_PICKED', 'BEING_PICKED_PARTIAL');

         l_partial_remain := (l_original_lines - l_number_of_lines);

         IF l_partial_remain = 0
         THEN
            UPDATE xxwc.XXWC_OM_PQC_PICKING_TBL
               SET PICKED_BY_USER_NTID = UPPER (l_ntid),
                   PICK_STATUS =
                      CASE
                         WHEN PICK_STATUS = 'BEING_PICKED'
                         THEN
                            'PICK_COMPLETE'
                         WHEN PICK_STATUS = 'BEING_PICKED_PARTIAL'
                         THEN
                            'PICKED_PARTIAL'
                      END,
                   PICK_COMPLETION_DATE = SYSDATE,
                   LAST_UPDATE_DATE = SYSDATE,
                   LAST_UPDATED_BY =
                      (SELECT USER_ID
                         FROM APPLSYS.fnd_user
                        WHERE user_name = UPPER (l_ntid))
             WHERE     order_number = l_ordernum
                   AND PICKED_BY_USER_NTID = UPPER (l_ntid)
                   AND PICK_TICKET_VERSION =
                          (SELECT MAX (PICK_TICKET_VERSION)
                             FROM XXWC.XXWC_OM_PQC_PICKING_TBL
                            WHERE order_number = l_ordernum)
                   AND PICK_STATUS IN ('BEING_PICKED', 'BEING_PICKED_PARTIAL');
         ELSE
            IF l_partial_remain > 0
            THEN
               UPDATE xxwc.XXWC_OM_PQC_PICKING_TBL
                  SET PICKED_BY_USER_NTID = UPPER (l_ntid),
                      PICK_STATUS = 'PICKED_PARTIAL',
                      PICK_COMPLETION_DATE = SYSDATE,
                      NUMBER_OF_LINES_PULLED = l_number_of_lines,
                      TOTAL_NUMBER_OF_LINES = l_number_of_lines,
                      LAST_UPDATE_DATE = SYSDATE,
                      LAST_UPDATED_BY =
                         (SELECT USER_ID
                            FROM APPLSYS.fnd_user
                           WHERE user_name = UPPER (l_ntid))
                WHERE     order_number = l_ordernum
                      AND PICKED_BY_USER_NTID = UPPER (l_ntid)
                      AND PICK_TICKET_VERSION =
                             (SELECT MAX (PICK_TICKET_VERSION)
                                FROM XXWC.XXWC_OM_PQC_PICKING_TBL
                               WHERE order_number = l_ordernum)
                      AND PICK_STATUS IN ('BEING_PICKED',
                                          'BEING_PICKED_PARTIAL');

               INSERT INTO XXWC.XXWC_OM_PQC_PICKING_TBL (
                              header_id,
                              order_number,
                              pick_ticket_version,
                              customer_name,
                              customer_number,
                              cust_account_id,
                              ship_from_org_id,
                              ship_from_org_code,
                              request_date,
                              freight_carrier_code,
                              customer_city,
                              total_number_of_lines,
                              number_of_lines_pulled,
                              pick_status,
                              picked_by_user_ntid,
                              picked_by_user_name,
                              pick_start_date,
                              pick_completion_date,
                              creation_date,
                              created_by,
                              last_update_date,
                              last_updated_by,
                              request_id)
                  (SELECT header_id,
                          order_number,
                          pick_ticket_version,
                          customer_name,
                          customer_number,
                          cust_account_id,
                          ship_from_org_id,
                          ship_from_org_code,
                          request_date,
                          freight_carrier_code,
                          customer_city,
                          l_partial_remain,
                          '',
                          'PICK_OPEN',
                          '',
                          '',
                          '',
                          '',
                          creation_date,
                          created_by,
                          last_update_date,
                          last_updated_by,
                          request_id
                     FROM XXWC.XXWC_OM_PQC_PICKING_TBL
                    WHERE     order_number = l_ordernum
                          AND PICKED_BY_USER_NTID = UPPER (l_ntid)
                          AND PICK_STATUS = 'PICKED_PARTIAL'
                          AND PICK_TICKET_VERSION =
                                 (SELECT (MAX (PICK_TICKET_VERSION))
                                    FROM XXWC.XXWC_OM_PQC_PICKING_TBL
                                   WHERE order_number = l_ordernum));
            END IF;
         END IF;

         SELECT TOTAL_NUMBER_OF_LINES
           INTO l_original_lines
           FROM xxwc.XXWC_OM_PQC_PICKING_TBL
          WHERE     order_number = l_ordernum
                AND PICKED_BY_USER_NTID = UPPER (l_ntid)
                AND pick_ticket_version =
                       (SELECT MAX (PICK_TICKET_VERSION)
                          FROM XXWC.XXWC_OM_PQC_PICKING_TBL
                         WHERE order_number = l_ordernum)
                AND PICK_STATUS IN ('PICK_COMPLETE', 'PICKED_PARTIAL')
                AND LAST_UPDATE_DATE = (SELECT MAX (LAST_UPDATE_DATE)
                                          FROM XXWC.XXWC_OM_PQC_PICKING_TBL
                                         WHERE order_number = l_ordernum);

         l_partial_remain := (l_original_lines - l_number_of_lines);

         IF l_partial_remain = 0
         THEN
            INSERT INTO XXWC.XXWC_OM_PQC_LOAD_CHECK_TBL (
                           HEADER_ID,
                           ORDER_NUMBER,
                           PICK_TICKET_VERSION,
                           CUSTOMER_NAME,
                           CUSTOMER_NUMBER,
                           CUSTOMER_CITY,
                           CUST_ACCOUNT_ID,
                           SHIP_FROM_ORG_ID,
                           SHIP_FROM_ORG_CODE,
                           REQUEST_DATE,
                           LOAD_CHECK_STATUS,
                           PICKED_BY_USER_NTID,
                           PICKED_BY_USER_NAME,
                           FREIGHT_CARRIER_CODE,
                           TOTAL_NUMBER_OF_LINES,
                           NUMBER_OF_LINES_PULLED,
                           CREATION_DATE,
                           CREATED_BY,
                           LAST_UPDATE_DATE,
                           LAST_UPDATED_BY)
               (SELECT header_id,
                       order_number,
                       pick_ticket_version,
                       customer_name,
                       customer_number,
                       CUSTOMER_CITY,
                       cust_account_id,
                       ship_from_org_id,
                       ship_from_org_code,
                       request_date,
                       'LOAD_CHECK_OPEN',
                       UPPER (PICKED_BY_USER_NTID),
                       PICKED_BY_USER_NAME,
                       freight_carrier_code,
                       TOTAL_NUMBER_OF_LINES,
                       NUMBER_OF_LINES_PULLED,
                       SYSDATE,
                       (SELECT USER_ID
                          FROM APPLSYS.fnd_user
                         WHERE user_name = UPPER (l_ntid)),
                       SYSDATE,
                       (SELECT USER_ID
                          FROM APPLSYS.fnd_user
                         WHERE user_name = UPPER (l_ntid))
                  FROM XXWC.XXWC_OM_PQC_PICKING_TBL
                 WHERE     order_number = l_ordernum
                       AND PICKED_BY_USER_NTID = UPPER (l_ntid)
                       AND pick_ticket_version =
                              (SELECT MAX (PICK_TICKET_VERSION)
                                 FROM XXWC.XXWC_OM_PQC_PICKING_TBL
                                WHERE order_number = l_ordernum)
                       AND PICK_STATUS IN ('PICK_COMPLETE', 'PICKED_PARTIAL')
                       AND LAST_UPDATE_DATE =
                              (SELECT MAX (LAST_UPDATE_DATE)
                                 FROM XXWC.XXWC_OM_PQC_PICKING_TBL
                                WHERE order_number = l_ordernum));
         ELSIF l_partial_remain > 0
         THEN
            INSERT INTO XXWC.XXWC_OM_PQC_LOAD_CHECK_TBL (
                           HEADER_ID,
                           ORDER_NUMBER,
                           PICK_TICKET_VERSION,
                           CUSTOMER_NAME,
                           CUSTOMER_NUMBER,
                           CUSTOMER_CITY,
                           CUST_ACCOUNT_ID,
                           SHIP_FROM_ORG_ID,
                           SHIP_FROM_ORG_CODE,
                           REQUEST_DATE,
                           LOAD_CHECK_STATUS,
                           PICKED_BY_USER_NTID,
                           PICKED_BY_USER_NAME,
                           FREIGHT_CARRIER_CODE,
                           TOTAL_NUMBER_OF_LINES,
                           NUMBER_OF_LINES_PULLED,
                           CREATION_DATE,
                           CREATED_BY,
                           LAST_UPDATE_DATE,
                           LAST_UPDATED_BY)
               (SELECT header_id,
                       order_number,
                       pick_ticket_version,
                       customer_name,
                       customer_number,
                       CUSTOMER_CITY,
                       cust_account_id,
                       ship_from_org_id,
                       ship_from_org_code,
                       request_date,
                       'LOAD_CHECK_OPEN',
                       UPPER (PICKED_BY_USER_NTID),
                       PICKED_BY_USER_NAME,
                       freight_carrier_code,
                       TOTAL_NUMBER_OF_LINES,
                       NUMBER_OF_LINES_PULLED,
                       SYSDATE,
                       (SELECT USER_ID
                          FROM APPLSYS.fnd_user
                         WHERE user_name = UPPER (l_ntid)),
                       SYSDATE,
                       (SELECT USER_ID
                          FROM APPLSYS.fnd_user
                         WHERE user_name = UPPER (l_ntid))
                  FROM XXWC.XXWC_OM_PQC_PICKING_TBL
                 WHERE     order_number = l_ordernum
                       AND PICKED_BY_USER_NTID = UPPER (l_ntid)
                       AND pick_ticket_version =
                              (SELECT MAX (PICK_TICKET_VERSION)
                                 FROM XXWC.XXWC_OM_PQC_PICKING_TBL
                                WHERE order_number = l_ordernum)
                       AND PICK_STATUS = 'PICKED_PARTIAL'
                       AND LAST_UPDATE_DATE =
                              (SELECT MAX (LAST_UPDATE_DATE)
                                 FROM XXWC.XXWC_OM_PQC_PICKING_TBL
                                WHERE order_number = l_ordernum));
         END IF;
      ELSIF l_action = 'ST_LC' OR l_action_type = 'ST_LC'
      THEN
         UPDATE XXWC.XXWC_OM_PQC_LOAD_CHECK_TBL
            SET NO_OF_LNS_LOAD_CHECKED = l_number_of_lines,
                LOAD_CHECKED_BY_USER_NTID = UPPER (l_ntid),
                LOAD_CHECKED_BY_USER_NAME =
                   (SELECT description
                      FROM APPLSYS.fnd_user
                     WHERE user_name = UPPER (l_ntid)),
                LOAD_CHECK_STATUS = 'BEING_LOAD_CHECKED',
                LOAD_CHECK_START_DATE = SYSDATE,
                LAST_UPDATE_DATE = SYSDATE,
                LAST_UPDATED_BY =
                   (SELECT USER_ID
                      FROM APPLSYS.fnd_user
                     WHERE user_name = UPPER (l_ntid))
          WHERE     order_number = l_ordernum
                AND LOAD_CHECK_STATUS = 'LOAD_CHECK_OPEN'
                AND pick_ticket_version = l_version
                AND creation_date =
                       TO_DATE (
                          REPLACE (REPLACE (l_creation_date, 'T', ' '),
                                   'Z',
                                   ''),
                          'YYYY-MM-DD HH24:MI:SS');


         COMMIT;
      --This is the completed stage of the Load Process
      ELSIF     (l_action = 'C_LC' OR l_action_type = 'C_LC')
            AND NVL (l_line_errors, 0) = 0
      THEN
         UPDATE XXWC.XXWC_OM_PQC_LOAD_CHECK_TBL
            SET NO_OF_LNS_LOAD_CHECKED = l_number_of_lines,
                LOAD_CHECK_STATUS = 'LOAD_CHECK_COMPLETE',
                LOAD_CHECK_COMPLETION_DATE = SYSDATE,
                LAST_UPDATE_DATE = SYSDATE,
                LAST_UPDATED_BY =
                   (SELECT USER_ID
                      FROM APPLSYS.fnd_user
                     WHERE user_name = UPPER (l_ntid)),
                PALLETS = l_pallets,
                BUNDLES = l_bundles,
                TOOLS = l_tools,
                UNITS = l_units,
                NOTES = l_notes,
                LINE_ERRORS = l_line_errors
          WHERE     order_number = l_ordernum
                AND pick_ticket_version = l_version
                AND creation_date =
                       TO_DATE (
                          REPLACE (REPLACE (l_creation_date, 'T', ' '),
                                   'Z',
                                   ''),
                          'YYYY-MM-DD HH24:MI:SS')
                AND LOAD_CHECKED_BY_USER_NTID = UPPER (l_ntid) --Only the person who started load check can finish it.
                AND LOAD_CHECK_STATUS IN ('BEING_LOAD_CHECKED',
                                          'BEING_LOAD_CHECKED_PARTIAL'); --Only close tickets that have been started.


         COMMIT;
      ELSIF     (l_action = 'C_LC' OR l_action_type = 'C_LC')
            AND NVL (l_line_errors, 0) > 0
      THEN
         UPDATE XXWC.XXWC_OM_PQC_LOAD_CHECK_TBL
            SET NO_OF_LNS_LOAD_CHECKED = l_number_of_lines - l_line_errors,
                TOTAL_NUMBER_OF_LINES = TOTAL_NUMBER_OF_LINES - l_line_errors,
                NUMBER_OF_LINES_PULLED =
                   NUMBER_OF_LINES_PULLED - l_line_errors,
                LOAD_CHECK_STATUS = 'LOAD_CHECK_COMPLETE',
                LOAD_CHECK_COMPLETION_DATE = SYSDATE,
                LAST_UPDATE_DATE = SYSDATE,
                LAST_UPDATED_BY =
                   (SELECT USER_ID
                      FROM APPLSYS.fnd_user
                     WHERE user_name = UPPER (l_ntid)),
                PALLETS = l_pallets,
                BUNDLES = l_bundles,
                TOOLS = l_tools,
                UNITS = l_units,
                NOTES = l_notes,
                LINE_ERRORS = l_line_errors
          WHERE     order_number = l_ordernum
                AND pick_ticket_version = l_version
                AND creation_date =
                       TO_DATE (
                          REPLACE (REPLACE (l_creation_date, 'T', ' '),
                                   'Z',
                                   ''),
                          'YYYY-MM-DD HH24:MI:SS')
                AND LOAD_CHECKED_BY_USER_NTID = UPPER (l_ntid) --Only the person who started load check can finish it.
                AND LOAD_CHECK_STATUS IN ('BEING_LOAD_CHECKED',
                                          'BEING_LOAD_CHECKED_PARTIAL')
                AND (l_number_of_lines - l_line_errors) >= 0; --Only close tickets that have been started.


         COMMIT;

         IF (l_number_of_lines - l_line_errors) >= 0
         THEN
            INSERT INTO XXWC.XXWC_OM_PQC_PICKING_TBL (header_id,
                                                      order_number,
                                                      pick_ticket_version,
                                                      customer_name,
                                                      customer_number,
                                                      cust_account_id,
                                                      ship_from_org_id,
                                                      ship_from_org_code,
                                                      request_date,
                                                      freight_carrier_code,
                                                      customer_city,
                                                      total_number_of_lines,
                                                      number_of_lines_pulled,
                                                      pick_status,
                                                      picked_by_user_ntid,
                                                      picked_by_user_name,
                                                      pick_start_date,
                                                      pick_completion_date,
                                                      creation_date,
                                                      created_by,
                                                      last_update_date,
                                                      last_updated_by,
                                                      request_id)
               (SELECT a.header_id,
                       a.order_number,
                       a.pick_ticket_version,
                       a.customer_name,
                       a.customer_number,
                       a.cust_account_id,
                       a.ship_from_org_id,
                       a.ship_from_org_code,
                       a.request_date,
                       a.freight_carrier_code,
                       a.customer_city,
                       l_line_errors,
                       l_line_errors,
                       'BEING_PICKED',
                       b.PICKED_BY_USER_NTID,
                       b.PICKED_BY_USER_NAME,
                       SYSDATE,
                       NULL,
                       SYSDATE,
                       (SELECT USER_ID
                          FROM APPLSYS.fnd_user
                         WHERE user_name = UPPER (l_ntid)),
                       a.last_update_date,
                       (SELECT USER_ID
                          FROM APPLSYS.fnd_user
                         WHERE user_name = UPPER (l_ntid)),
                       a.request_id
                  FROM XXWC.XXWC_OM_PQC_PICKING_TBL a,
                       XXWC.XXWC_OM_PQC_LOAD_CHECK_TBL b
                 WHERE     a.order_number = l_ordernum
                       AND a.order_number = b.order_number
                       AND a.pick_ticket_version = b.pick_ticket_version
                       AND b.LOAD_CHECK_STATUS = 'LOAD_CHECK_COMPLETE'
                       AND b.line_errors = l_line_errors
                       AND a.PICK_STATUS IN ('PICKED_PARTIAL',
                                             'PICK_COMPLETE')
                       AND a.PICK_TICKET_VERSION =
                              (SELECT (MAX (PICK_TICKET_VERSION))
                                 FROM XXWC.XXWC_OM_PQC_PICKING_TBL
                                WHERE order_number = l_ordernum)
                       AND ROWNUM = 1);

            COMMIT;

            UPDATE XXWC.XXWC_OM_PQC_PICKING_TBL a
               SET TOTAL_NUMBER_OF_LINES = l_number_of_lines - l_line_errors,
                   NUMBER_OF_LINES_PULLED = l_number_of_lines - l_line_errors,
                   LAST_UPDATED_BY =
                      (SELECT USER_ID
                         FROM APPLSYS.fnd_user
                        WHERE user_name = UPPER (l_ntid)),
                   LAST_UPDATE_DATE = SYSDATE
             WHERE     a.order_number = l_ordernum
                   AND a.PICK_TICKET_VERSION =
                          (SELECT (MAX (PICK_TICKET_VERSION))
                             FROM XXWC.XXWC_OM_PQC_PICKING_TBL
                            WHERE order_number = l_ordernum)
                   AND a.PICK_STATUS IN ('PICKED_PARTIAL', 'PICK_COMPLETE')
                   AND EXISTS
                          (SELECT 1
                             FROM XXWC.XXWC_OM_PQC_LOAD_CHECK_TBL b
                            WHERE     a.order_number = b.order_number
                                  AND b.LOAD_CHECK_STATUS =
                                         'LOAD_CHECK_COMPLETE'
                                  AND b.line_errors = l_line_errors
                                  AND b.PICK_TICKET_VERSION =
                                         b.PICK_TICKET_VERSION
                                  AND a.PICKED_BY_USER_NTID =
                                         b.PICKED_BY_USER_NTID
                                  AND LOAD_CHECKED_BY_USER_NTID =
                                         UPPER (l_ntid)
                                  AND ROWNUM = 1);

            COMMIT;
         END IF;
      ELSIF l_action = 'CX_LC' OR l_action_type = 'CX_LC'
      THEN
         UPDATE XXWC.XXWC_OM_PQC_LOAD_CHECK_TBL
            SET NO_OF_LNS_LOAD_CHECKED = NULL,
                LOAD_CHECKED_BY_USER_NTID = NULL,
                LOAD_CHECKED_BY_USER_NAME = NULL,
                LOAD_CHECK_STATUS = 'LOAD_CHECK_OPEN',
                LOAD_CHECK_START_DATE = NULL,
                LOAD_CHECK_COMPLETION_DATE=NULL,
                LAST_UPDATE_DATE = SYSDATE,
                LAST_UPDATED_BY =
                   (SELECT USER_ID
                      FROM APPLSYS.fnd_user
                     WHERE user_name = UPPER (l_ntid)),
                PALLETS = NULL,
                BUNDLES = NULL,
                TOOLS = NULL,
                UNITS = NULL,
                NOTES = NULL,
                LINE_ERRORS = NULL
          WHERE     order_number = l_ordernum
                AND pick_ticket_version = l_version
                AND creation_date =
                       TO_DATE (
                          REPLACE (REPLACE (l_creation_date, 'T', ' '),
                                   'Z',
                                   ''),
                          'YYYY-MM-DD HH24:MI:SS')
                AND LOAD_CHECKED_BY_USER_NTID = UPPER (l_ntid) --Only the person who started load check can cancel it.
                AND LOAD_CHECK_STATUS IN ('BEING_LOAD_CHECKED',
                                          'BEING_LOAD_CHECKED_PARTIAL',
                                          'LOAD_CHECK_COMPLETE');

         COMMIT;
      END IF;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         COMMIT;
   END save_updated_pick_ticket;
END xxwc_pick_qc_impr_mnts_pkg;
/