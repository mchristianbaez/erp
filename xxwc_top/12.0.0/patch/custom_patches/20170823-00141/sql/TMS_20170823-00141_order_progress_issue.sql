/******************************************************************************
  $Header TMS_20170823-00141_order_progress_issue.sql $
  Module Name:Data Fix script for 20170823-00141

  PURPOSE: Data fix script for 20170823-00141 Order progress issue

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    -------------------------------
  1.0       30-AUG-2017  Pattabhi Avula         TMS#20170823-00141

*******************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
   l_line_id       NUMBER:=102712252;
   l_result        VARCHAR2(30);
   l_file_val      VARCHAR2(500);

 BEGIN
 --oe_debug_pub.debug_on;
 --oe_debug_pub.initialize;
 --l_file_val:=OE_DEBUG_PUB.Set_Debug_Mode('FILE');
 --oe_Debug_pub.setdebuglevel(5);
 dbms_output.put_line(' Inside the script');
 --dbms_output.put_line('file path is:'||l_file_val);
    OE_Standard_WF.OEOL_SELECTOR
    (p_itemtype => 'OEOL'
    ,p_itemkey => TO_CHAR(l_line_id)
    ,p_actid => 12345
    ,p_funcmode => 'SET_CTX'
    ,p_result => l_result
    );
 -- oe_debug_pub.add('Retrun value of OEOL_selector function is'||l_result);
   dbms_output.put_line('Retrun value of OEOL_selector function is '||l_result);
    wf_engine.handleError('OEOL', TO_CHAR(l_line_id),'INVOICE_INTERFACE','SKIP','COMPLETE');
--oe_debug_pub.debug_off;

EXCEPTION
 WHEN OTHERS THEN
       Dbms_Output.put_line('Error occurred: ' || SQLERRM);
      -- Oe_debug_pub.ADD('Error occurred: ' || SQLERRM);
       ROLLBACK;
END;
/