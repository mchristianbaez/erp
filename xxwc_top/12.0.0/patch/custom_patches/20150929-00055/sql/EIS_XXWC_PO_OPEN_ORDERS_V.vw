CREATE OR REPLACE FORCE VIEW "XXEIS"."EIS_XXWC_PO_OPEN_ORDERS_V" ("PO_HEADER_ID", "ATTRIBUTE2", "LAST_UPDATE_DATE", "LAST_UPDATED_BY", "PO_CREATION_DATE", "CREATED_BY", "ORG_CODE", "SHIP_TO", "ORGANIZATION_CODE", "BUYER_NAME", "PO_NUMBER", "PO_NUMBER_RELEASE", "RELEASE_NUM", "PO_SUPPLIER_NAME", "SUPPLIER_SITE", "SUPPLIER_CONTACT_FIRST_NAME", "SUPPLIER_CONTACT_LAST_NAME", "SUPPLIER_CONTACT_AREA_CODE", "SUPPLIER_CONTACT_PHONE", "PO_SUMMARY", "PO_ENABLED", "START_DATE", "END_DATE", "SHIP_TO_LOCATION", "PO_TERMS", "SHIP_VIA", "FOB", "FREIGHT_TERMS", "REV", "REVISED_DATE", "PRINTED_DATE", "APPROVED_DATE", "CURRENCY_CODE", "ACCEPTANCE_DUE_DATE", "HOLD", "ACCEPTANCE_REQUIRED", "NOTE_TO_AUTHORIZER",
 "NOTE_TO_SUPPLIER", "NOTE_TO_RECEIVER", "COMMENTS", "CLOSED_CODE", "CLOSED_DATE", "LINE_ID", "LINE_LAST_UPDATE_DATE", "LINE_LAST_UPDATED_BY", "LINE_CREATION_DATE", "LINE_CREATED_BY", "LINE_TYPE", "LINE_NUM", "CATEGORY", "ITEM", "ITEM_DESCRIPTION", "PO_UNIT_PRICE", "QUANTITY_ORDERED", "QUANTITY_CANCELLED", "QUANTITY_RECEIVED", "QUANTITY_DUE", "QUANTITY_BILLED", "AMOUNT_ORDERED", "AMOUNT_CANCELLED", "AMOUNT_RECEIVED", "AMOUNT_DUE", "AMOUNT_BILLED", "OPEN_INVOICE_AMOUNT", "OPEN_FOR", "ITEM_REVISION", "LINE_UNIT", "LINE_LIST_PRICE", "MARKET_PRICE", "LINE_CANCEL", "LINE_CANCEL_DATE", "LINE_CANCEL_REASON", "TAXABLE_FLAG", "TAX_NAME", "TYPE_1099", "LINE_CLOSED_CODE", "LINE_CLOSED_DATE", 
 "LINE_CLOSED_REASON", "LINE_LOCATION_ID", "SHIPMENT_NUM", "SHIPMENT_TYPE", "SHIP_NEED_BY_DATE", "SHIP_PROMISED_DATE", "SHIP_LAST_ACCEPT_DATE", "PO_4_WAY_MATCH", "PO_3_WAY_MATCH", "QTY_RCV_TOLERANCE", "DAYS_EARLY_RECEIPT_ALLOWED", "DAYS_LATE_RECEIPT_ALLOWED", "INVOICE_CLOSE_TOLERANCE", "RECEIVE_CLOSE_TOLERANCE", "PRICE_OVERRIDE", "ACCRUE_ON_RECEIPT", "DISTRIBUTION_ID", "DISTRIBUTION_NUM", "SHIP_DIST", "RATE", "COMPANY", "CHARGE_CCID", "PO_CHARGE_ACCOUNT", "ACCRUAL_CCID", "ACCRUAL_ACCOUNT", "VARIANCE_CCID", "VARIANCE_ACCOUNT", "PO_DATA_TYPE", "PO_STATUS", "PO_TYPE", "REQ_NUM", "REQ_CREATED_ON", "REQ_LINE_CREATION_DATE", "SEQ#", "REQUIRED_BY", "UNIT_MEAS_LOOKUP_CODE", "UNIT_PRICE", "QUANTITY", "REQ_QUANTITY_CANCELLED", "REQ_QUANTITY_DELIVERED", "CANCEL_FLAG", "SOURCE_TYPE_CODE", "TRANSFERRED_TO_OE_FLAG", "SOURCE_ORGANIZATION_ID",
 "DESTINATION_ORGANIZATION_ID", "ITEM_ID", "PREPARER_ID", "OPERATING_UNIT", "ORG_PO_LINES", "HEADER_NEED_BY_DATE", "HEADER_PROMISE_DATE", "LINE_PROMISE_DATE", "SHIP_CLOSED_CODE", "REQUISITION_HEADER_ID", "REQUISITION_LINE_ID", "VENDOR_ID", "VENDOR_SITE_ID", "SHIP_TO_LOCATION_ID", "SET_OF_BOOKS_ID", "LOCATION_ID", "PO_RELEASE_ID", "VENDOR_CONTACT_ID", "LINE_TYPE_ID", "EMPLOYEE_ID", "AGENT_ID", "TERM_ID", "INVENTORY_ITEM_ID", "CATEGORY_ID", "PVS_VENDOR_ID", "PVC_VENDOR_SITE_ID", "PSP_ORG_ID", "PDT_DOCUMENT_TYPE_CODE", "PDT_DOCUMENT_SUBTYPE", "PDT_ORG_ID", "MSI_ORGANIZATION_ID", "GCC2#BRANCH", "GCC1#BRANCH", "GCC#BRANCH", "MSI#HDS#LOB", "MSI#HDS#DROP_SHIPMENT", "MSI#HDS#INVOICE_UOM",
 "MSI#HDS#PRODUCT_ID", "MSI#HDS#VENDOR_PART_NUMBER", "MSI#HDS#UNSPSC_CODE", "MSI#HDS#UPC_PRIMARY", "MSI#HDS#SKU_DESCRIPTION", "MSI#WC#CA_PROP_65", "MSI#WC#COUNTRY_OF_ORIGIN", "MSI#WC#ORM_D_FLAG", "MSI#WC#STORE_VELOCITY", "MSI#WC#DC_VELOCITY", "MSI#WC#YEARLY_STORE_VELOCITY", "MSI#WC#YEARLY_DC_VELOCITY", "MSI#WC#PRISM_PART_NUMBER", "MSI#WC#HAZMAT_DESCRIPTION", "MSI#WC#HAZMAT_CONTAINER", "MSI#WC#GTP_INDICATOR", "MSI#WC#LAST_LEAD_TIME", "MSI#WC#AMU", "MSI#WC#RESERVE_STOCK", "MSI#WC#TAXWARE_CODE", "MSI#WC#AVERAGE_UNITS", "MSI#WC#PRODUCT_CODE", "MSI#WC#IMPORT_DUTY_", "MSI#WC#KEEP_ITEM_ACTIVE", "MSI#WC#PESTICIDE_FLAG", "MSI#WC#CALC_LEAD_TIME", "MSI#WC#VOC_GL", "MSI#WC#PESTICIDE_FLAG_STATE", "MSI#WC#VOC_CATEGORY",
 "MSI#WC#VOC_SUB_CATEGORY", "MSI#WC#MSDS_#", "MSI#WC#HAZMAT_PACKAGING_GROU", "POH#STANDARDP#NEED_BY_DATE", "POH#STANDARDP#FREIGHT_TERMS_", "POH#STANDARDP#CARRIER_TERMS_", "POH#STANDARDP#FOB_TERMS_TAB", "PV#R11VENDOR#R11_VENDOR_ID", "PV#TAXEXEMPT#EXEMPTIONS", "PVS#IWOFIPS#FIPS_CODE", "PVS#R11VENDOR#R11_VENDOR_SIT", "PVS#R11VENDOR#R11_VENDOR_ID", "MCA#COGS_ACCOUNT", "MCA#SALES_ACCOUNT", "MSI#HDS#LOB1", "MSI#HDS#DROP_SHIPMENT_ELIGAB", "MSI#HDS#DROP_SHIPMENT_ELIGAB1", "MSI#HDS#INVOICE_UOM1", "MSI#HDS#PRODUCT_ID1", "MSI#HDS#VENDOR_PART_NUMBER1", "MSI#HDS#UNSPSC_CODE1", "MSI#HDS#UPC_PRIMARY1", "MSI#HDS#SKU_DESCRIPTION1", "MSI#WC#CA_PROP_651", "MSI#WC#COUNTRY_OF_ORIGIN1", "MSI#WC#ORM_D_FLAG1",
 "MSI#WC#STORE_VELOCITY1", "MSI#WC#DC_VELOCITY1", "MSI#WC#YEARLY_STORE_VELOCITY1", "MSI#WC#YEARLY_DC_VELOCITY1", "MSI#WC#PRISM_PART_NUMBER1", "MSI#WC#HAZMAT_DESCRIPTION1", "MSI#WC#HAZMAT_CONTAINER1", "MSI#WC#GTP_INDICATOR1", "MSI#WC#LAST_LEAD_TIME1", "MSI#WC#AMU1", "MSI#WC#RESERVE_STOCK1", "MSI#WC#TAXWARE_CODE1", "MSI#WC#AVERAGE_UNITS1", "MSI#WC#PRODUCT_CODE1", "MSI#WC#IMPORT_DUTY_1", "MSI#WC#KEEP_ITEM_ACTIVE1", "MSI#WC#PESTICIDE_FLAG1", "MSI#WC#CALC_LEAD_TIME1", "MSI#WC#VOC_GL1", "MSI#WC#PESTICIDE_FLAG_STATE1", "MSI#WC#VOC_CATEGORY1", "MSI#WC#VOC_SUB_CATEGORY1", "MSI#WC#MSDS_#1", "MSI#WC#HAZMAT_PACKAGING_GROU1", "PV#LEGACY#LEGACY_SUPPLIER_NU",
 "PV#PRISM#PRISM_SUPPLIER_NUM", "GCC#50328#ACCOUNT", "GCC#50328#ACCOUNT#DESCR", "GCC#50328#COST_CENTER", "GCC#50328#COST_CENTER#DESCR", "GCC#50328#FURTURE_USE", "GCC#50328#FURTURE_USE#DESCR", "GCC#50328#FUTURE_USE_2", "GCC#50328#FUTURE_USE_2#DESCR", "GCC#50328#LOCATION", "GCC#50328#LOCATION#DESCR", "GCC#50328#PRODUCT", "GCC#50328#PRODUCT#DESCR", "GCC#50328#PROJECT_CODE", "GCC#50328#PROJECT_CODE#DESCR", "GCC1#50328#ACCOUNT", "GCC1#50328#ACCOUNT#DESCR", "GCC1#50328#COST_CENTER", "GCC1#50328#COST_CENTER#DESCR", "GCC1#50328#FURTURE_USE", "GCC1#50328#FURTURE_USE#DESCR", "GCC1#50328#FUTURE_USE_2", "GCC1#50328#FUTURE_USE_2#DESCR", "GCC1#50328#LOCATION",
 "GCC1#50328#LOCATION#DESCR", "GCC1#50328#PRODUCT", "GCC1#50328#PRODUCT#DESCR", "GCC1#50328#PROJECT_CODE", "GCC1#50328#PROJECT_CODE#DESCR", "GCC2#50328#ACCOUNT", "GCC2#50328#ACCOUNT#DESCR", "GCC2#50328#COST_CENTER", "GCC2#50328#COST_CENTER#DESCR", "GCC2#50328#FURTURE_USE", "GCC2#50328#FURTURE_USE#DESCR", "GCC2#50328#FUTURE_USE_2", "GCC2#50328#FUTURE_USE_2#DESCR", "GCC2#50328#LOCATION", "GCC2#50328#LOCATION#DESCR", "GCC2#50328#PRODUCT", "GCC2#50328#PRODUCT#DESCR", "GCC2#50328#PROJECT_CODE", "GCC2#50328#PROJECT_CODE#DESCR", "GCC#50368#ACCOUNT", "GCC#50368#ACCOUNT#DESCR", "GCC#50368#DEPARTMENT", "GCC#50368#DEPARTMENT#DESCR", "GCC#50368#DIVISION",
 "GCC#50368#DIVISION#DESCR", "GCC#50368#FUTURE_USE", "GCC#50368#FUTURE_USE#DESCR", "GCC#50368#PRODUCT", "GCC#50368#PRODUCT#DESCR", "GCC#50368#SUBACCOUNT", "GCC#50368#SUBACCOUNT#DESCR", "GCC1#50368#ACCOUNT", "GCC1#50368#ACCOUNT#DESCR", "GCC1#50368#DEPARTMENT", "GCC1#50368#DEPARTMENT#DESCR", "GCC1#50368#DIVISION", "GCC1#50368#DIVISION#DESCR", "GCC1#50368#FUTURE_USE", "GCC1#50368#FUTURE_USE#DESCR", "GCC1#50368#PRODUCT", "GCC1#50368#PRODUCT#DESCR", "GCC1#50368#SUBACCOUNT", "GCC1#50368#SUBACCOUNT#DESCR", "GCC2#50368#ACCOUNT", "GCC2#50368#ACCOUNT#DESCR", "GCC2#50368#DEPARTMENT", "GCC2#50368#DEPARTMENT#DESCR", "GCC2#50368#DIVISION",
 "GCC2#50368#DIVISION#DESCR", "GCC2#50368#FUTURE_USE", "GCC2#50368#FUTURE_USE#DESCR", "GCC2#50368#PRODUCT", "GCC2#50368#PRODUCT#DESCR", "GCC2#50368#SUBACCOUNT", "GCC2#50368#SUBACCOUNT#DESCR", "ACTION_DATE", "ACCEPTED_FLAG", "ACCEPTANCE_TYPE", "ACC_DUE_DATE", "ACCEPTED_BY")
/**************************************************************************************************************
  $Header XXEIS.EIS_XXWC_PO_OPEN_ORDERS_V.vw $
  Module Name: Purchasing
  PURPOSE: Open Purchase Orders Listing - WC
  TMS Task Id :
  REVISIONS:
  Ver        Date        Author                Description
  ---------  ----------  ------------------    ----------------
  1.0        NA    NA            Initial Version
  1.1  06/22/2014  Mahender Reddy         TMS#20140410-00214
  1.2  10/27/2015  Mahender Reddy         TMS#20150929-00055
  **************************************************************************************************************/
AS 
  SELECT poh.po_header_id ,
    poh.attribute2 --Added for TMS#20140410-00214 by Mahender on 06-22-2014
    ,
    poh.last_update_date last_update_date ,
    poh.last_updated_by last_updated_by ,
    TRUNC (poh.creation_date) po_creation_date ,
    poh.created_by created_by ,
    ood.organization_code org_code --Added for TMS#20140410-00214 by Mahender on 06-22-2014
    ,
    DECODE ( NVL (poh.attribute2, 'STANDARD') ,'STANDARD', SUBSTR (hrl.location_code, 1, 3) ,
    (SELECT location
    FROM hz_cust_site_uses_all
    WHERE site_use_id =
      (SELECT ooh.ship_to_org_id
      FROM oe_drop_ship_sources ods ,
        oe_order_headers_all ooh ,
        po_lines_all pl ,
        po_headers_all ph
      WHERE ph.po_header_id = ods.po_header_id
      AND ooh.header_id     = ods.header_id
      AND ph.po_header_id   = pl.po_header_id
      AND pl.po_line_id     = pol.po_line_id
      AND ph.po_header_id   = poh.po_header_id
      AND ROWNUM            = 1
      )
    )) ship_to --Added for TMS#20140410-00214 by Mahender on 06-22-2014
    ,
    SUBSTR (hou1.name, 1, 3) organization_code --Added for TMS#20140410-00214 by Mahender on 06-22-2014
    ,
    poav.agent_name buyer_name ,
    poh.segment1 po_number ,
    poh.segment1
    || DECODE (por1.release_num, NULL, '', '-')
    || por1.release_num po_number_release ,
    por1.release_num release_num ,
    pv.vendor_name po_supplier_name ,
    pvs.vendor_site_code supplier_site ,
    pvc.first_name supplier_contact_first_name ,
    pvc.last_name supplier_contact_last_name ,
    pvc.area_code supplier_contact_area_code ,
    pvs.phone supplier_contact_phone ,
    DECODE (poh.summary_flag,'Y', 'Yes','N', 'No') po_summary ,
    DECODE (poh.enabled_flag,'Y', 'Yes','N', 'No') po_enabled ,
    poh.start_date_active start_date ,
    poh.end_date_active end_date ,
    hrl.location_code ship_to_location ,
    apt.name po_terms ,
    poh.ship_via_lookup_code ship_via ,
    poh.fob_lookup_code fob ,
    poh.freight_terms_lookup_code freight_terms ,
    poh.revision_num rev ,
    TO_DATE (poh.revised_date) revised_date ,
    poh.printed_date printed_date ,
    poh.approved_date approved_date ,
    poh.currency_code currency_code ,
    poh.acceptance_due_date acceptance_due_date ,
    DECODE (poh.user_hold_flag,'Y', 'Yes','N', 'No',NULL) hold ,
    DECODE (poh.acceptance_required_flag,'Y', 'Yes','N', 'No',NULL) acceptance_required ,
    poh.note_to_authorizer note_to_authorizer ,
    poh.note_to_vendor note_to_supplier ,
    poh.note_to_receiver note_to_receiver ,
    poh.comments comments ,
    poh.closed_code closed_code ,
    poh.closed_date closed_date ,
    pol.po_line_id line_id ,
    pol.last_update_date line_last_update_date ,
    pol.last_updated_by line_last_updated_by ,
    pol.creation_date line_creation_date ,
    pol.created_by line_created_by ,
    plt.line_type line_type ,
    pol.line_num line_num ,
    mca.concatenated_segments category ,
    msi.concatenated_segments item ,
    REPLACE (pol.item_description, '~') item_description ,
    TO_NUMBER ( DECODE (plt.order_type_lookup_code, 'AMOUNT', NULL, NVL (poll.price_override, pol.unit_price))) po_unit_price ,
    NVL (pod.quantity_ordered, NVL (poll.quantity, pol.quantity)) quantity_ordered ,
    NVL (pod.quantity_cancelled, 0) quantity_cancelled ,
    NVL (pod.quantity_delivered, 0) quantity_received ,
    NVL (pod.quantity_ordered, NVL (poll.quantity, pol.quantity)) - NVL (pod.quantity_cancelled, 0) - NVL (pod.quantity_delivered, 0) quantity_due ,
    NVL (pod.quantity_billed, 0) quantity_billed ,
    ( TO_NUMBER ( DECODE (plt.order_type_lookup_code ,'AMOUNT', NULL ,NVL (poll.price_override, pol.unit_price))) * NVL (pod.quantity_ordered, NVL (poll.quantity, pol.quantity))) amount_ordered ,
    ( TO_NUMBER ( DECODE (plt.order_type_lookup_code ,'AMOUNT', NULL ,NVL (poll.price_override, pol.unit_price))) * NVL (pod.quantity_cancelled, 0)) amount_cancelled ,
    ( TO_NUMBER ( DECODE (plt.order_type_lookup_code ,'AMOUNT', NULL ,NVL (poll.price_override, pol.unit_price))) * NVL (pod.quantity_delivered, 0)) amount_received ,
    ( TO_NUMBER ( DECODE (plt.order_type_lookup_code ,'AMOUNT', NULL ,NVL (poll.price_override, pol.unit_price))) * ( NVL (pod.quantity_ordered, NVL (poll.quantity, pol.quantity)) - NVL (pod.quantity_cancelled, 0) - NVL (pod.quantity_delivered, 0))) amount_due ,
    ( TO_NUMBER ( DECODE (plt.order_type_lookup_code ,'AMOUNT', NULL ,NVL (poll.price_override, pol.unit_price))) * pod.quantity_billed) amount_billed ,
    ( (pod.quantity_ordered                                                                                       - pod.quantity_cancelled - pod.quantity_billed) * price_override) open_invoice_amount ,
    plc.displayed_field open_for ,
    pol.item_revision item_revision ,
    pol.unit_meas_lookup_code line_unit ,
    pol.list_price_per_unit line_list_price ,
    pol.market_price market_price ,
    pol.cancel_flag line_cancel ,
    pol.cancel_date line_cancel_date ,
    pol.cancel_reason line_cancel_reason ,
    NVL (pol.taxable_flag, poll.taxable_flag) taxable_flag ,
    pol.tax_name tax_name ,
    pol.type_1099 type_1099 ,
    pol.closed_code line_closed_code ,
    pol.closed_date line_closed_date ,
    pol.closed_reason line_closed_reason ,
    poll.line_location_id line_location_id ,
    poll.shipment_num shipment_num ,
    poll.shipment_type shipment_type ,
    TRUNC (poll.need_by_date) ship_need_by_date ,
    poll.promised_date ship_promised_date ,
    poll.last_accept_date ship_last_accept_date ,
    DECODE (poll.inspection_required_flag,'Y', 'Yes','N', 'No') po_4_way_match ,
    DECODE (poll.receipt_required_flag,'Y', 'Yes','N', 'No') po_3_way_match ,
    poll.qty_rcv_tolerance qty_rcv_tolerance ,
    poll.days_early_receipt_allowed days_early_receipt_allowed ,
    poll.days_late_receipt_allowed days_late_receipt_allowed ,
    poll.invoice_close_tolerance invoice_close_tolerance ,
    poll.receive_close_tolerance receive_close_tolerance ,
    poll.price_override price_override ,
    DECODE (poll.accrue_on_receipt_flag,'Y', 'Yes','N', 'No') accrue_on_receipt ,
    pod.po_distribution_id distribution_id ,
    pod.distribution_num distribution_num ,
    poll.shipment_num
    || '-'
    || pod.distribution_num ship_dist ,
    pod.rate rate ,
    sob.name company ,
    gcc.code_combination_id charge_ccid ,
    gcc.concatenated_segments po_charge_account ,
    gcc1.code_combination_id accrual_ccid ,
    gcc1.concatenated_segments accrual_account ,
    gcc2.code_combination_id variance_ccid ,
    gcc2.concatenated_segments variance_account ,
    psp.manual_po_num_type po_data_type ,
    plc_sta.displayed_field
    || DECODE (poh.cancel_flag, 'Y', ', '
    || plc_can.displayed_field, NULL)
    || DECODE (NVL (poh.closed_code, 'OPEN'), 'OPEN', NULL, ', '
    || plc_clo.displayed_field)
    || DECODE (poh.frozen_flag, 'Y', ', '
    || plc_fro.displayed_field, NULL)
    || DECODE (poh.user_hold_flag, 'Y', ', '
    || plc_hld.displayed_field, NULL) po_status ,
    pdt.type_name po_type ,
    prh.segment1 req_num ,
    TRUNC (prh.creation_date) req_created_on ,
    TRUNC (prl.creation_date) req_line_creation_date ,
    prl.line_num SEQ# ,
    TRUNC (prl.need_by_date) required_by ,
    prl.unit_meas_lookup_code ,
    prl.unit_price ,
    prl.quantity ,
    prl.quantity_cancelled req_quantity_cancelled ,
    prl.quantity_delivered req_quantity_delivered ,
    prl.cancel_flag ,
    prl.source_type_code ,
    prh.transferred_to_oe_flag ,
    prl.source_organization_id ,
    prl.destination_organization_id ,
    prl.item_id ,
    prh.preparer_id ,
    hou.name operating_unit ,
    (SELECT COUNT (*)
    FROM po_lines pol1
    WHERE pol1.po_header_id = poh.po_header_id
    ) org_po_lines ,
    DECODE (poh.attribute_category ,'Standard Purchase Order', fnd_date.canonical_to_date (poh.attribute1) ,NULL) header_need_by_date ,
    DECODE (poh.attribute_category ,'Standard Purchase Order', fnd_date.canonical_to_date (poh.attribute6) ,NULL) header_promise_date --Added for TMS#20140410-00214 by Mahender on 06-22-2014
    ,
    TRUNC (poll.promised_date) line_promise_date --Added for TMS#20140410-00214 by Mahender on 06-22-2014                                            --Mahender
    ,
    poll.closed_code ship_closed_code , -- Added PK Columns
    prh.requisition_header_id ,
    prl.requisition_line_id ,
    pv.vendor_id vendor_id ,
    pvs.vendor_site_id vendor_site_id ,
    hrl.ship_to_location_id ship_to_location_id ,
    sob.set_of_books_id set_of_books_id ,
    hrl.location_id ,
    por1.po_release_id ,
    pvc.vendor_contact_id ,
    plt.line_type_id ,
    hre.employee_id ,
    poav.agent_id ,
    apt.term_id ,
    msi.inventory_item_id ,
    mca.category_id , --primary keys added for component joins
    pvs.vendor_id pvs_vendor_id ,
    pvc.vendor_site_id pvc_vendor_site_id ,
    psp.org_id psp_org_id ,
    pdt.document_type_code pdt_document_type_code ,
    pdt.document_subtype pdt_document_subtype ,
    pdt.org_id pdt_org_id ,
    msi.organization_id msi_organization_id , --descr#flexfield#start
    xxeis.eis_rs_dff.decode_valueset ('XXCUS_FRU', gcc2.attribute1, 'I') gcc2#branch ,
    xxeis.eis_rs_dff.decode_valueset ('XXCUS_FRU', gcc1.attribute1, 'I') gcc1#branch ,
    xxeis.eis_rs_dff.decode_valueset ('XXCUS_FRU', gcc.attribute1, 'I') gcc#branch ,
    DECODE (msi.attribute_category, 'HDS', msi.attribute1, NULL) msi#hds#lob ,
    DECODE (msi.attribute_category ,'HDS', xxeis.eis_rs_dff.decode_valueset ('Yes_No', msi.attribute10, 'F') ,NULL) msi#hds#drop_shipment ,
    DECODE (msi.attribute_category, 'HDS', msi.attribute15, NULL) msi#hds#invoice_uom ,
    DECODE (msi.attribute_category, 'HDS', msi.attribute2, NULL) msi#hds#product_id ,
    DECODE (msi.attribute_category, 'HDS', msi.attribute3, NULL) msi#hds#vendor_part_number ,
    DECODE (msi.attribute_category, 'HDS', msi.attribute4, NULL) msi#hds#unspsc_code ,
    DECODE (msi.attribute_category, 'HDS', msi.attribute5, NULL) msi#hds#upc_primary ,
    DECODE (msi.attribute_category, 'HDS', msi.attribute7, NULL) msi#hds#sku_description ,
    DECODE (msi.attribute_category, 'WC', msi.attribute1, NULL) msi#wc#ca_prop_65 ,
    DECODE (msi.attribute_category, 'WC', msi.attribute10, NULL) msi#wc#country_of_origin ,
    DECODE (msi.attribute_category ,'WC', xxeis.eis_rs_dff.decode_valueset ('Yes_No', msi.attribute11, 'F') ,NULL) msi#wc#orm_d_flag ,
    DECODE (msi.attribute_category, 'WC', msi.attribute12, NULL) msi#wc#store_velocity ,
    DECODE (msi.attribute_category, 'WC', msi.attribute13, NULL) msi#wc#dc_velocity ,
    DECODE (msi.attribute_category, 'WC', msi.attribute14, NULL) msi#wc#yearly_store_velocity ,
    DECODE (msi.attribute_category, 'WC', msi.attribute15, NULL) msi#wc#yearly_dc_velocity ,
    DECODE (msi.attribute_category, 'WC', msi.attribute16, NULL) msi#wc#prism_part_number ,
    DECODE (msi.attribute_category, 'WC', msi.attribute17, NULL) msi#wc#hazmat_description ,
    DECODE (msi.attribute_category ,'WC', xxeis.eis_rs_dff.decode_valueset ('XXWC HAZMAT CONTAINER', msi.attribute18, 'I') ,NULL) msi#wc#hazmat_container ,
    DECODE (msi.attribute_category, 'WC', msi.attribute19, NULL) msi#wc#gtp_indicator ,
    DECODE (msi.attribute_category, 'WC', msi.attribute2, NULL) msi#wc#last_lead_time ,
    DECODE (msi.attribute_category, 'WC', msi.attribute20, NULL) msi#wc#amu ,
    DECODE (msi.attribute_category, 'WC', msi.attribute21, NULL) msi#wc#reserve_stock ,
    DECODE (msi.attribute_category ,'WC', xxeis.eis_rs_dff.decode_valueset ('XXWC TAXWARE CODE', msi.attribute22, 'I') ,NULL) msi#wc#taxware_code ,
    DECODE (msi.attribute_category, 'WC', msi.attribute25, NULL) msi#wc#average_units ,
    DECODE (msi.attribute_category, 'WC', msi.attribute26, NULL) msi#wc#product_code ,
    DECODE (msi.attribute_category, 'WC', msi.attribute27, NULL) msi#wc#import_duty_ ,
    DECODE (msi.attribute_category ,'WC', xxeis.eis_rs_dff.decode_valueset ('Yes_No', msi.attribute29, 'F') ,NULL) msi#wc#keep_item_active ,
    DECODE (msi.attribute_category ,'WC', xxeis.eis_rs_dff.decode_valueset ('Yes_No', msi.attribute3, 'F') ,NULL) msi#wc#pesticide_flag ,
    DECODE (msi.attribute_category, 'WC', msi.attribute30, NULL) msi#wc#calc_lead_time ,
    DECODE (msi.attribute_category, 'WC', msi.attribute4, NULL) msi#wc#voc_gl ,
    DECODE (msi.attribute_category, 'WC', msi.attribute5, NULL) msi#wc#pesticide_flag_state ,
    DECODE (msi.attribute_category, 'WC', msi.attribute6, NULL) msi#wc#voc_category ,
    DECODE (msi.attribute_category, 'WC', msi.attribute7, NULL) msi#wc#voc_sub_category ,
    DECODE (msi.attribute_category, 'WC', msi.attribute8, NULL) msi#wc#msds_# ,
    DECODE (msi.attribute_category ,'WC', xxeis.eis_rs_dff.decode_valueset ('XXWC_HAZMAT_PACKAGE_GROUP', msi.attribute9, 'I') ,NULL) msi#wc#hazmat_packaging_grou ,
    DECODE (poh.attribute_category ,'Standard Purchase Order', fnd_date.canonical_to_date (poh.attribute1) ,NULL) poh#standardp#need_by_date ,
    DECODE (poh.attribute_category, 'Standard Purchase Order', poh.attribute2, NULL) poh#standardp#freight_terms_ ,
    DECODE (poh.attribute_category, 'Standard Purchase Order', poh.attribute3, NULL) poh#standardp#carrier_terms_ ,
    DECODE (poh.attribute_category, 'Standard Purchase Order', poh.attribute4, NULL) poh#standardp#fob_terms_tab ,
    DECODE (pv.attribute_category, 'R11 Vendor ID', pv.attribute15, NULL) pv#r11vendor#r11_vendor_id ,
    DECODE (pv.attribute_category, 'Tax Exempt Flag', pv.attribute1, NULL) pv#taxexempt#exemptions ,
    DECODE (pvs.attribute_category, 'IWO FIPS', pvs.attribute1, NULL) pvs#iwofips#fips_code ,
    DECODE (pvs.attribute_category, 'R11 Vendor IDs', pvs.attribute14, NULL) pvs#r11vendor#r11_vendor_sit ,
    DECODE (pvs.attribute_category, 'R11 Vendor IDs', pvs.attribute15, NULL) pvs#r11vendor#r11_vendor_id ,
    mca.attribute1 mca#cogs_account ,
    mca.attribute2 mca#sales_account ,
    DECODE (msi.attribute_category, 'HDS', msi.attribute1, NULL) msi#hds#lob1 ,
    DECODE (msi.attribute_category ,'HDS', xxeis.eis_rs_dff.decode_valueset ('Yes_No', msi.attribute10, 'F') ,NULL) msi#hds#drop_shipment_eligab ,
    DECODE (msi.attribute_category ,'HDS', xxeis.eis_rs_dff.decode_valueset ('Yes_No', msi.attribute10, 'F') ,NULL) msi#hds#drop_shipment_eligab1 ,
    DECODE (msi.attribute_category, 'HDS', msi.attribute15, NULL) msi#hds#invoice_uom1 ,
    DECODE (msi.attribute_category, 'HDS', msi.attribute2, NULL) msi#hds#product_id1 ,
    DECODE (msi.attribute_category, 'HDS', msi.attribute3, NULL) msi#hds#vendor_part_number1 ,
    DECODE (msi.attribute_category, 'HDS', msi.attribute4, NULL) msi#hds#unspsc_code1 ,
    DECODE (msi.attribute_category, 'HDS', msi.attribute5, NULL) msi#hds#upc_primary1 ,
    DECODE (msi.attribute_category, 'HDS', msi.attribute6, NULL) msi#hds#sku_description1 ,
    DECODE (msi.attribute_category, 'WC', msi.attribute1, NULL) msi#wc#ca_prop_651 ,
    DECODE (msi.attribute_category, 'WC', msi.attribute10, NULL) msi#wc#country_of_origin1 ,
    DECODE (msi.attribute_category ,'WC', xxeis.eis_rs_dff.decode_valueset ('Yes_No', msi.attribute11, 'F') ,NULL) msi#wc#orm_d_flag1 ,
    DECODE (msi.attribute_category, 'WC', msi.attribute12, NULL) msi#wc#store_velocity1 ,
    DECODE (msi.attribute_category, 'WC', msi.attribute13, NULL) msi#wc#dc_velocity1 ,
    DECODE (msi.attribute_category, 'WC', msi.attribute14, NULL) msi#wc#yearly_store_velocity1 ,
    DECODE (msi.attribute_category, 'WC', msi.attribute15, NULL) msi#wc#yearly_dc_velocity1 ,
    DECODE (msi.attribute_category, 'WC', msi.attribute16, NULL) msi#wc#prism_part_number1 ,
    DECODE (msi.attribute_category, 'WC', msi.attribute17, NULL) msi#wc#hazmat_description1 ,
    DECODE (msi.attribute_category ,'WC', xxeis.eis_rs_dff.decode_valueset ('XXWC HAZMAT CONTAINER', msi.attribute18, 'I') ,NULL) msi#wc#hazmat_container1 ,
    DECODE (msi.attribute_category, 'WC', msi.attribute19, NULL) msi#wc#gtp_indicator1 ,
    DECODE (msi.attribute_category, 'WC', msi.attribute2, NULL) msi#wc#last_lead_time1 ,
    DECODE (msi.attribute_category, 'WC', msi.attribute20, NULL) msi#wc#amu1 ,
    DECODE (msi.attribute_category, 'WC', msi.attribute21, NULL) msi#wc#reserve_stock1 ,
    DECODE (msi.attribute_category ,'WC', xxeis.eis_rs_dff.decode_valueset ('XXWC TAXWARE CODE', msi.attribute22, 'I') ,NULL) msi#wc#taxware_code1 ,
    DECODE (msi.attribute_category, 'WC', msi.attribute25, NULL) msi#wc#average_units1 ,
    DECODE (msi.attribute_category, 'WC', msi.attribute26, NULL) msi#wc#product_code1 ,
    DECODE (msi.attribute_category, 'WC', msi.attribute27, NULL) msi#wc#import_duty_1 ,
    DECODE (msi.attribute_category ,'WC', xxeis.eis_rs_dff.decode_valueset ('Yes_No', msi.attribute29, 'F') ,NULL) msi#wc#keep_item_active1 ,
    DECODE (msi.attribute_category ,'WC', xxeis.eis_rs_dff.decode_valueset ('Yes_No', msi.attribute3, 'F') ,NULL) msi#wc#pesticide_flag1 ,
    DECODE (msi.attribute_category, 'WC', msi.attribute30, NULL) msi#wc#calc_lead_time1 ,
    DECODE (msi.attribute_category, 'WC', msi.attribute4, NULL) msi#wc#voc_gl1 ,
    DECODE (msi.attribute_category, 'WC', msi.attribute5, NULL) msi#wc#pesticide_flag_state1 ,
    DECODE (msi.attribute_category, 'WC', msi.attribute6, NULL) msi#wc#voc_category1 ,
    DECODE (msi.attribute_category, 'WC', msi.attribute7, NULL) msi#wc#voc_sub_category1 ,
    DECODE (msi.attribute_category, 'WC', msi.attribute8, NULL) msi#wc#msds_#1 ,
    DECODE (msi.attribute_category ,'WC', xxeis.eis_rs_dff.decode_valueset ('XXWC_HAZMAT_PACKAGE_GROUP', msi.attribute9, 'I') ,NULL) msi#wc#hazmat_packaging_grou1 ,
    DECODE (pv.attribute_category, 'Legacy', pv.attribute1, NULL) pv#legacy#legacy_supplier_nu ,
    DECODE (pv.attribute_category, 'PRISM', pv.attribute2, NULL) pv#prism#prism_supplier_num , --descr#flexfield#end
    --gl#accountff#start
    gcc.segment4 gcc#50328#account ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment4, 'XXCUS_GL_ACCOUNT') gcc#50328#account#descr ,
    gcc.segment3 gcc#50328#cost_center ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment3, 'XXCUS_GL_COSTCENTER') gcc#50328#cost_center#descr ,
    gcc.segment6 gcc#50328#furture_use ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment6, 'XXCUS_GL_FUTURE_USE1') gcc#50328#furture_use#descr ,
    gcc.segment7 gcc#50328#future_use_2 ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment7, 'XXCUS_GL_FUTURE_USE_2') gcc#50328#future_use_2#descr ,
    gcc.segment2 gcc#50328#location ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment2, 'XXCUS_GL_LOCATION') gcc#50328#location#descr ,
    gcc.segment1 gcc#50328#product ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment1, 'XXCUS_GL_PRODUCT') gcc#50328#product#descr ,
    gcc.segment5 gcc#50328#project_code ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment5, 'XXCUS_GL_PROJECT') gcc#50328#project_code#descr ,
    gcc1.segment4 gcc1#50328#account ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment4, 'XXCUS_GL_ACCOUNT') gcc1#50328#account#descr ,
    gcc1.segment3 gcc1#50328#cost_center ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment3, 'XXCUS_GL_COSTCENTER') gcc1#50328#cost_center#descr ,
    gcc1.segment6 gcc1#50328#furture_use ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment6, 'XXCUS_GL_FUTURE_USE1') gcc1#50328#furture_use#descr ,
    gcc1.segment7 gcc1#50328#future_use_2 ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment7, 'XXCUS_GL_FUTURE_USE_2') gcc1#50328#future_use_2#descr ,
    gcc1.segment2 gcc1#50328#location ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment2, 'XXCUS_GL_LOCATION') gcc1#50328#location#descr ,
    gcc1.segment1 gcc1#50328#product ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment1, 'XXCUS_GL_PRODUCT') gcc1#50328#product#descr ,
    gcc1.segment5 gcc1#50328#project_code ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment5, 'XXCUS_GL_PROJECT') gcc1#50328#project_code#descr ,
    gcc2.segment4 gcc2#50328#account ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc2.segment4, 'XXCUS_GL_ACCOUNT') gcc2#50328#account#descr ,
    gcc2.segment3 gcc2#50328#cost_center ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc2.segment3, 'XXCUS_GL_COSTCENTER') gcc2#50328#cost_center#descr ,
    gcc2.segment6 gcc2#50328#furture_use ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc2.segment6, 'XXCUS_GL_FUTURE_USE1') gcc2#50328#furture_use#descr ,
    gcc2.segment7 gcc2#50328#future_use_2 ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc2.segment7, 'XXCUS_GL_FUTURE_USE_2') gcc2#50328#future_use_2#descr ,
    gcc2.segment2 gcc2#50328#location ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc2.segment2, 'XXCUS_GL_LOCATION') gcc2#50328#location#descr ,
    gcc2.segment1 gcc2#50328#product ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc2.segment1, 'XXCUS_GL_PRODUCT') gcc2#50328#product#descr ,
    gcc2.segment5 gcc2#50328#project_code ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc2.segment5, 'XXCUS_GL_PROJECT') gcc2#50328#project_code#descr ,
    gcc.segment4 gcc#50368#account ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment4, 'XXCUS_GL_ LTMR _ACCOUNT') gcc#50368#account#descr ,
    gcc.segment3 gcc#50368#department ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment3, 'XXCUS_GL_ LTMR _DEPARTMENT') gcc#50368#department#descr ,
    gcc.segment2 gcc#50368#division ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment2, 'XXCUS_GL_ LTMR _DIVISION') gcc#50368#division#descr ,
    gcc.segment6 gcc#50368#future_use ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment6, 'XXCUS_GL_ LTMR _FUTUREUSE') gcc#50368#future_use#descr ,
    gcc.segment1 gcc#50368#product ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment1, 'XXCUS_GL_LTMR_PRODUCT') gcc#50368#product#descr ,
    gcc.segment5 gcc#50368#subaccount ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment5, 'XXCUS_GL_ LTMR _SUBACCOUNT') gcc#50368#subaccount#descr ,
    gcc1.segment4 gcc1#50368#account ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment4, 'XXCUS_GL_ LTMR _ACCOUNT') gcc1#50368#account#descr ,
    gcc1.segment3 gcc1#50368#department ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment3, 'XXCUS_GL_ LTMR _DEPARTMENT') gcc1#50368#department#descr ,
    gcc1.segment2 gcc1#50368#division ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment2, 'XXCUS_GL_ LTMR _DIVISION') gcc1#50368#division#descr ,
    gcc1.segment6 gcc1#50368#future_use ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment6, 'XXCUS_GL_ LTMR _FUTUREUSE') gcc1#50368#future_use#descr ,
    gcc1.segment1 gcc1#50368#product ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment1, 'XXCUS_GL_LTMR_PRODUCT') gcc1#50368#product#descr ,
    gcc1.segment5 gcc1#50368#subaccount ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment5, 'XXCUS_GL_ LTMR _SUBACCOUNT') gcc1#50368#subaccount#descr ,
    gcc2.segment4 gcc2#50368#account ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc2.segment4, 'XXCUS_GL_ LTMR _ACCOUNT') gcc2#50368#account#descr ,
    gcc2.segment3 gcc2#50368#department ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc2.segment3, 'XXCUS_GL_ LTMR _DEPARTMENT') gcc2#50368#department#descr ,
    gcc2.segment2 gcc2#50368#division ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc2.segment2, 'XXCUS_GL_ LTMR _DIVISION') gcc2#50368#division#descr ,
    gcc2.segment6 gcc2#50368#future_use ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc2.segment6, 'XXCUS_GL_ LTMR _FUTUREUSE') gcc2#50368#future_use#descr ,
    gcc2.segment1 gcc2#50368#product ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc2.segment1, 'XXCUS_GL_LTMR_PRODUCT') gcc2#50368#product#descr ,
    GCC2.SEGMENT5 GCC2#50368#SUBACCOUNT ,
    xxeis.eis_rs_fin_utility.decode_vset (gcc2.segment5, 'XXCUS_GL_ LTMR _SUBACCOUNT') gcc2#50368#subaccount#descr,
    --gl#accountff#end
    --START Added VER 1.2
    
    POA.ACTION_DATE ACTION_DATE,
    NVL(POA.ACCEPTED_FLAG,'N') ACCEPTED_FLAG,
    POA.DISPLAYED_FIELD ACCEPTANCE_TYPE,
    POH.ACCEPTANCE_DUE_DATE ACC_DUE_DATE,
    HR.FULL_NAME Accepted_by
    --END Added VER 1.2
  FROM po_headers poh ,
    po_lines pol ,
    po_line_locations poll ,
    po_distributions pod ,
    po_releases por1 ,
    po_vendors pv ,
    po_vendor_sites pvs ,
    po_vendor_contacts pvc ,
    po_line_types plt ,
    po_requisition_headers prh ,
    po_requisition_lines prl ,
    po_req_distributions prd ,
    mtl_system_items_kfv msi ,
    mtl_categories_kfv mca ,
    hr_locations hrl ,
    hr_employees hre ,
    po_agents_v poav ,
    ap_terms apt ,
    gl_sets_of_books sob ,
    gl_code_combinations_kfv gcc ,
    gl_code_combinations_kfv gcc1 ,
    gl_code_combinations_kfv gcc2 ,
    po_lookup_codes plc ,
    po_system_parameters psp ,
    po_lookup_codes plc_sta ,
    po_lookup_codes plc_can ,
    po_lookup_codes plc_clo ,
    po_lookup_codes plc_fro ,
    po_lookup_codes plc_hld ,
    po_document_types pdt ,
    hr_operating_units hou ,
    financials_system_parameters fsp ,
    hr_organization_units hou1 ,
    org_organization_definitions ood --Added for TMS#20140410-00214 by Mahender on 06-22-2014
    --START Added VER 1.2
    --  PO_ACCEPTANCES POA ,
    --PO_LOOKUP_CODES POC ,
	,(select 
pa1.PO_HEADER_ID,
pa1.ACCEPTANCE_LOOKUP_CODE,
pa1.EMPLOYEE_ID,
pa1.PO_LINE_LOCATION_ID,
pa1.ACCEPTED_FLAG,
POC.LOOKUP_CODE,
POC.LOOKUP_TYPE,
POC.DISPLAYED_FIELD,
pa1.ACTION_DATE
from PO_ACCEPTANCES pa1,
(select po_header_id, max(acceptance_id) max_acceptance_id from po_acceptances group by po_header_id) pa2,
PO_LOOKUP_CODES poc
where pa1.po_header_id = pa2.po_header_id 
AND pa1.acceptance_id = pa2.max_acceptance_id
AND POC.LOOKUP_CODE(+)        = PA1.ACCEPTANCE_LOOKUP_CODE
AND POC.LOOKUP_TYPE(+)        = 'ACCEPTANCE TYPE') POA,
    PER_PEOPLE_F HR ,
    PO_AGENTS PA
    --END Added VER 1.2
  WHERE poh.agent_id        = hre.employee_id
  AND poh.po_header_id      = pol.po_header_id(+)
  AND pol.po_line_id        = poll.po_line_id(+)
  AND poll.line_location_id = pod.line_location_id(+)
  AND poh.vendor_id         = pv.vendor_id
  AND poh.vendor_site_id    = pvs.vendor_site_id
  AND poh.vendor_contact_id = pvc.vendor_contact_id(+)
  AND poh.vendor_site_id    = pvc.vendor_site_id(+)
  AND pol.line_type_id      = plt.line_type_id
  AND pol.item_id           = msi.inventory_item_id(+)
    -- AND pol.org_id = msi.organization_id(+)
  AND pol.category_id                     = mca.category_id
  AND poll.ship_to_location_id            = hrl.location_id(+)
  AND poll.ship_to_organization_id        = ood.organization_id --Added for TMS#20140410-00214 by Mahender on 06-22-2014
  AND hrl.location_id                     = hou1.location_id(+)
  AND poh.agent_id                        = poav.agent_id(+)
  AND poh.terms_id                        = apt.term_id(+)
  AND poll.po_release_id                  = por1.po_release_id(+)
  AND pod.set_of_books_id                 = sob.set_of_books_id
  AND pod.code_combination_id             = gcc.code_combination_id
  AND pod.accrual_account_id              = gcc1.code_combination_id
  AND pod.variance_account_id             = gcc2.code_combination_id
  AND poh.type_lookup_code               IN ('STANDARD', 'BLANKET', 'PLANNED')
  AND NVL (poh.closed_code, 'OPEN') NOT  IN ('FINALLY CLOSED', 'CLOSED')
  AND NVL (poh.cancel_flag, 'N')          = 'N'
  AND NVL (por1.cancel_flag, 'N')         = 'N'
  AND pdt.document_subtype                = poh.type_lookup_code
  AND plc_sta.lookup_code                 = NVL (poh.authorization_status, 'INCOMPLETE')
  AND plc_sta.lookup_type                IN ('PO APPROVAL', 'DOCUMENT STATE')
  AND plc_can.lookup_code                 = 'CANCELLED'
  AND plc_can.lookup_type                 = 'DOCUMENT STATE'
  AND plc_clo.lookup_code                 = NVL (poh.closed_code, 'OPEN')
  AND plc_clo.lookup_type                 = 'DOCUMENT STATE'
  AND plc_fro.lookup_code                 = 'FROZEN'
  AND plc_fro.lookup_type                 = 'DOCUMENT STATE'
  AND plc_hld.lookup_code                 = 'ON HOLD'
  AND plc_hld.lookup_type                 = 'DOCUMENT STATE'
  AND pdt.document_type_code             IN ('PO', 'PA')
  AND NVL (poll.closed_code, 'OPEN') NOT IN ('CLOSED', 'FINALLY CLOSED')
  AND NVL (pol.closed_code, 'OPEN') NOT  IN ('CLOSED', 'FINALLY CLOSED')
  AND poll.shipment_type                 IN ('STANDARD', 'BLANKET', 'SCHEDULED')
  AND NVL (poll.closed_code, 'OPEN')      = plc.lookup_code
  AND plc.lookup_type                     = 'DOCUMENT STATE'
  AND NVL (poll.cancel_flag, 'N')         = 'N'
  AND NVL (pol.cancel_flag, 'N')          = 'N'
  AND pod.req_distribution_id             = prd.distribution_id(+)
  AND prd.requisition_line_id             = prl.requisition_line_id(+)
  AND prl.requisition_header_id           = prh.requisition_header_id(+)
  AND EXISTS
    (SELECT 'x'
    FROM po_lines pl,
      po_line_locations pll,
      po_releases por
    WHERE pl.po_header_id                  = poh.po_header_id
    AND pl.po_line_id                      = pll.po_line_id
    AND NVL (pll.closed_code, 'OPEN') NOT IN ('FINALLY CLOSED', 'CLOSED')
    AND NVL (pl.closed_code, 'OPEN') NOT  IN ('FINALLY CLOSED', 'CLOSED')
    AND NVL (por.closed_code, 'OPEN') NOT IN ('FINALLY CLOSED', 'CLOSED')
    AND NVL (pll.cancel_flag, 'N')         = 'N'
    AND NVL (pl.cancel_flag, 'N')          = 'N'
    AND NVL (por.cancel_flag, 'N')         = 'N'
    AND pll.shipment_type                 IN ('STANDARD', 'BLANKET', 'SCHEDULED')
    AND pll.po_release_id                  = por.po_release_id(+)
    )
  AND fsp.inventory_organization_id = NVL (msi.organization_id, fsp.inventory_organization_id)
  AND fsp.org_id                    = poh.org_id
  AND hou.organization_id           = poh.org_id
  AND psp.org_id                    = poh.org_id
  AND pdt.org_id                    = poh.org_id
    --START Added VER 1.2
  and POH.PO_HEADER_ID         = POA.PO_HEADER_ID(+)
 -- and POC.LOOKUP_CODE(+)       = POA.ACCEPTANCE_LOOKUP_CODE
 -- and POC.LOOKUP_TYPE(+)       = 'ACCEPTANCE TYPE'
  and PA.AGENT_ID              = HR.PERSON_ID(+)
  and POA.EMPLOYEE_ID          = PA.AGENT_ID(+)
  and POA.PO_LINE_LOCATION_ID is null
  AND TRUNC(SYSDATE) BETWEEN NVL(HR.EFFECTIVE_START_DATE, TRUNC(SYSDATE)) AND NVL(HR.EFFECTIVE_END_DATE, TRUNC(SYSDATE))
  --ENDAdded VER 1.2
/