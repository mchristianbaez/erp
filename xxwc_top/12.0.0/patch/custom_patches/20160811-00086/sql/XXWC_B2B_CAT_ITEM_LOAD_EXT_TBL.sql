/*********************************************************************************
Copyright (c) 2016 HD Supply
All Rights Reserved

HEADER:
	XXWC_B2B_CAT_ITEM_LOAD_EXT_TBL

PROGRAM NAME:
	CUSTOMER CATALOG - APEX WC

DESCRIPTION:
	TMS# 20160811-00086
	External Table for Item loading into Customer Catalog Application

LAST UPDATE DATE: 27-MAY-2016

HISTORY
=======
VERSION	DATE          	 AUTHOR(S)    		  DESCRIPTION
-------	---------------	------------------- -------------------------------------
1.0		  24-AUG-2016	    Christian Baez		  Creation
1.2     25-AUG-2016     Christian Baez      Changes made to web long desc length (Task ID: 20160824-00046)
1.3     31-AUG-2016     Christian Baez      Changes made to characterset (TaskID: 20160811-00086)
1.4     14-SEP-2016     Christian Baez      Changes made to header to reflect current TMS task

*********************************************************************************/

DROP TABLE "XXWC"."XXWC_B2B_CAT_ITEM_LOAD_EXT_TBL";

CREATE TABLE "XXWC"."XXWC_B2B_CAT_ITEM_LOAD_EXT_TBL"
   (	"ITEM_NUMBER" VARCHAR2(255 BYTE),
	"ICC" VARCHAR2(255 BYTE),
	"ATTR_GROUP_DISP_NAME" VARCHAR2(1000 BYTE),
	"ATTR_DISPLAY_NAME" VARCHAR2(1000 BYTE),
	"ATTRIBUTE_VALUE" VARCHAR2(2000 BYTE)
   )
   ORGANIZATION EXTERNAL
    ( TYPE ORACLE_LOADER
      DEFAULT DIRECTORY "XXWC_PDH_ITEM_LOAD_DIR"
      ACCESS PARAMETERS
      ( RECORDS DELIMITED BY NEWLINE CHARACTERSET WE8MSWIN1252
        SKIP 1
        FIELDS TERMINATED BY ','
        OPTIONALLY ENCLOSED BY '"' AND '"'
                            )
      LOCATION
       ( 'XXWC_Product_Data_Load.csv'
       )
    )
   REJECT LIMIT UNLIMITED;

   GRANT ALL ON "XXWC"."XXWC_B2B_CAT_ITEM_LOAD_EXT_TBL" TO EA_APEX;
/
