/**************************************************************************
      *
      * PACKAGE
      * XXCUS_AP_FREIGHT_DATA_FIX1
      *
      * DESCRIPTION
      *  To process stucked freight invoices from staging
      *
      *
      * HISTORY
      * =======
      *
      * VERSION DATE        AUTHOR(S)       DESCRIPTION
      * ------- ----------- --------------- ------------------------------------
      * 1.00    08/12/2015   Maharajan S     Initial creation TMS# 20150812-00113
      *
      *************************************************************************/
update xxcus.XXCUSAP_TMS_INV_STG_TBL
set status = 'NEW_1'
WHERE status = 'NEW'
/
commit
/
update xxcus.XXCUSAP_TMS_INV_STG_TBL
set status = 'NEW'
WHERE status = 'NEW_1'
and batch_no = 'TMS_10-AUG-2015-1321'
/
commit
/
