CREATE OR REPLACE PACKAGE BODY APPS.XXWC_INV_ITEM_STK_LOCATORS_PKG
AS
   /*************************************************************************************************************************************************************
   -- File Name: XXWC_INV_ITEM_STK_LOCATORS_PKG.pkb
   --
   -- PROGRAM TYPE: Package Body
   --
   -- PURPOSE:  Package developed to delete General Stock locators and assign Branch Relo stock locators as General Stock Locators
   -- HISTORY
   -- ===========================================================================================================================================================
   -- ===========================================================================================================================================================
   -- VERSION    DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- ---------------------------------------------------------------------------------------------------------------------
   -- 1.0     26-OCT-2015   P.Vamshidhar    TMS#20150430-00020 - Delete or End Date and Reassign Stock Locators in General Subinventory for Branch Relocation

   ***************************************************************************************************************************************************************/

   -- Declaring Package Global Variables

   g_err_callfrom   VARCHAR2 (100) := 'XXWC_INV_ITEM_STK_LOCATORS_PKG';
   g_dflt_email     VARCHAR2 (200) := 'HDSOracleDevelopers@hdsupply.com';
   g_sec            VARCHAR2 (2000);
   g_request_id     NUMBER := NVL (FND_GLOBAL.CONC_REQUEST_ID, 0);


   PROCEDURE STKLOC_MAIN_PRC (X_ERRORBUF      OUT VARCHAR2,
                              X_RETCODE       OUT NUMBER,
                              P_ORG_ID     IN     NUMBER)
   IS
      /*******************************************************************************************************************************
        PROCEDURE : STKLOC_MAIN_PRC

        REVISIONS:
        Ver           Date        Author                     Description
        ---------  -----------  ---------------    ------------------------------------------------------------------------------------
        1.0        26-OCT-2015  P.Vamshidhar       TMS#20150430-00020 - Delete or End Date and Reassign Stock Locators in General
                                                   Subinventory for Branch Relocation
                                                   Initial Version
      *********************************************************************************************************************************/
      -- Declaring variables.
      l_procedure            VARCHAR2 (100) := 'STKLOC_MAIN_PRC';
      l_disable_process      VARCHAR2 (10);
      l_deleted_process      VARCHAR2 (10);
      l_purge_process        VARCHAR2 (10);
      l_return_mess          VARCHAR2 (1000);
      l_new_loc_assignment   VARCHAR2 (10);
      l_org_code             ORG_ORGANIZATION_DEFINITIONS.ORGANIZATION_CODE%TYPE;
      l_err_mess             VARCHAR2 (2000);
      l_org_mess             VARCHAR2 (2000);
      PROGRAM_ERROR          EXCEPTION;
      EXECUTION_ERROR        EXCEPTION;
      ln_update_count        NUMBER;

      CURSOR cur_print_output
      IS
           SELECT DECODE (process_status,
                          'Success', 'Success',
                          'Removed', 'Removed',
                          'Added', 'Added',
                          NULL, ' ',
                          'Process Failed')
                     process_status,
                  Item_number,
                  subinventory,
                  stock_locator
             FROM XXWC.XXWC_ITEM_STK_LOCATORS_TBL
            WHERE REQUEST_ID = G_REQUEST_ID
         ORDER BY item_number, Stock_locator;

      CURSOR cur_physical_locations (
         lp_org_id    NUMBER)
      IS
         SELECT INVENTORY_LOCATION_ID,
                ORGANIZATION_ID,
                SUBINVENTORY_CODE,
                PHYSICAL_LOCATION_ID
           FROM apps.mtl_item_locations
          WHERE     segment19 = '1'                              -- 1 for Drop
                AND segment1 != 'Drop'
                AND organization_id = lp_org_id
                AND PHYSICAL_LOCATION_ID IS NOT NULL
                AND SUBINVENTORY_CODE = 'General';
   BEGIN
      g_sec := ' STKLOC_MAIN_PRC - Start ';

      g_sec := ' Validating Responsibility and Execution Date';

      -- Validating submitted responsibility _name and execution day.
      IF fnd_global.RESP_NAME <> 'HDS Inventory Super User - WC'
      THEN
         l_err_mess :=
            'Program Needs to be Executed from Only HDS Inventory Super User - WC Responsibility';
         RAISE EXECUTION_ERROR;
      ELSE
                  IF TO_CHAR (SYSDATE, 'Day') NOT LIKE 'S%'
                  THEN
                     l_err_mess :=
                        'Programs needs to executed from HDS Inventory Super User - WC Responsibility Only during Weekend.';
                     RAISE EXECUTION_ERROR;
                  END IF;         
      END IF;

      -- Organization_id Validation
      g_sec := ' Organization Id Vallidation ';

      BEGIN
         SELECT organization_code
           INTO l_org_code
           FROM apps.MTL_PARAMETERS
          WHERE ORGANIZATION_ID = p_org_id;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_err_mess :=
                  ' Input Organization '
               || p_org_id
               || ' Not Found - '
               || SUBSTR (SQLERRM, 1, 250);
            RAISE EXECUTION_ERROR;
         WHEN OTHERS
         THEN
            l_err_mess :=
                  ' Error occured while validating Input Org id'
               || p_org_id
               || ' '
               || SUBSTR (SQLERRM, 1, 250);
            RAISE EXECUTION_ERROR;
      END;

      -- Purging history records from custom table

      g_sec := ' Purging History records from Custom table';

      PURGE_HISTORY_RECORDS (l_purge_process);

      IF l_purge_process <> 'S'
      THEN
         l_err_mess := 'Purging Records from Custom table was failed.';
         RAISE EXECUTION_ERROR;
      END IF;

      -- Updating physical_location_id to null for drop locators for respective org.

      g_sec :=
         ' Updating physical_location_id to null for drop locators for respective org';

      SELECT COUNT (1)
        INTO ln_update_count
        FROM apps.mtl_item_locations
       WHERE     segment19 = '1'                                 -- 1 for Drop
             AND segment1 != 'Drop'
             AND organization_id = p_org_id
             AND PHYSICAL_LOCATION_ID IS NOT NULL
             AND SUBINVENTORY_CODE = 'General';

      IF NVL (ln_update_count, 0) > 0
      THEN
         g_sec := ' Updating physical_location_id in Progress';

         FOR rec_physical_locations IN cur_physical_locations (p_org_id)
         LOOP
            UPDATE apps.mtl_item_locations
               SET PHYSICAL_LOCATION_ID = NULL
             WHERE     INVENTORY_LOCATION_ID =
                          rec_physical_locations.INVENTORY_LOCATION_ID
                   AND ORGANIZATION_ID =
                          rec_physical_locations.ORGANIZATION_ID
                   AND SUBINVENTORY_CODE =
                          rec_physical_locations.SUBINVENTORY_CODE
                   AND PHYSICAL_LOCATION_ID =
                          rec_physical_locations.PHYSICAL_LOCATION_ID;

            IF NVL (SQL%ROWCOUNT, 0) >= 1
            THEN
               FND_FILE.PUT_LINE (
                  FND_FILE.LOG,
                     rec_physical_locations.INVENTORY_LOCATION_ID
                  || ' - '
                  || rec_physical_locations.ORGANIZATION_ID
                  || ' - '
                  || rec_physical_locations.SUBINVENTORY_CODE
                  || ' - '
                  || rec_physical_locations.PHYSICAL_LOCATION_ID);
            ELSE
               FND_FILE.PUT_LINE (
                  FND_FILE.LOG,
                  'Error occured: ' || SUBSTR (SQLERRM, 1, 250));

               FND_FILE.PUT_LINE (
                  FND_FILE.LOG,
                     rec_physical_locations.INVENTORY_LOCATION_ID
                  || ' - '
                  || rec_physical_locations.ORGANIZATION_ID
                  || ' - '
                  || rec_physical_locations.SUBINVENTORY_CODE
                  || ' - '
                  || rec_physical_locations.PHYSICAL_LOCATION_ID);
            END IF;
         END LOOP;

         COMMIT;
      END IF;

      fnd_file.put_line (fnd_file.LOG,
                         ' Physical Location Id Update Completed');

      g_sec := ' ITEM_STKLOC_REM_ASSIGN - Calling';

      -- Calling Locator Disable Process
      ITEM_STKLOC_REM_ASSIGN (p_org_id              => p_org_id,
                              p_subinventory        => 'General',
                              p_temp_table_insert   => 'Y',
                              x_return_status       => l_disable_process,
                              x_return_mess         => l_return_mess);

      -- Locator Disable Process Completed and Validating Results.
      fnd_file.put_line (
         fnd_file.LOG,
            ' Locator Disable Process Completed and Return Status '
         || l_disable_process);

      g_sec := 'Locator Disable Process Results Validation';

      IF l_disable_process = 'S'
      THEN
         g_sec := 'Deleting General Locators for Org id ' || p_org_id;

         STK_LOCATOR_DELETE_PRC (p_org_id          => p_org_id,
                                 p_org_code        => l_org_code,
                                 x_return_status   => l_deleted_process);

         IF l_deleted_process <> 'S'
         THEN
            l_err_mess := 'Stock Locator Deletion Process Failed';
            RAISE PROGRAM_ERROR;
         END IF;
      ELSE
         g_sec := 'Disabling locators Process Failed for Org id' || p_org_id;

         l_err_mess :=
               l_return_mess
            || ' - Disabling Locators Processed Failed for Org id'
            || p_org_id;

         RAISE PROGRAM_ERROR;
      END IF;

      g_sec := 'Creating General Locators for Org id ' || p_org_id || ' ';

      -- Creating General Locators and Assiging to Items.  Source to create locators is BranchRelo Locators

      CREATE_ASSIGN_LOCATORS_PRC (p_org_id          => p_org_id,
                                  x_return_status   => l_new_loc_assignment);

      IF l_new_loc_assignment <> 'S'
      THEN
         FND_FILE.PUT_LINE (FND_FILE.LOG,
                            ' New Locators created and assigned');
         l_err_mess := ' New Locators Creation/Assignment Process Failed';
         RAISE PROGRAM_ERROR;
      END IF;

      -- Printing Output
      g_sec := ' Printing Output ';

      FND_FILE.PUT_LINE (
         FND_FILE.OUTPUT,
         ' -----------------------------------------------------------------------------------------------------------------');
      FND_FILE.PUT_LINE (
         FND_FILE.OUTPUT,
         '   Branch Number     Step                Item Number         Sub-Inventory        Stock Locator                   ');
      FND_FILE.PUT_LINE (
         FND_FILE.OUTPUT,
         ' -----------------------------------------------------------------------------------------------------------------');

      FOR rec_print_output IN cur_print_output
      LOOP
         FND_FILE.PUT_LINE (
            FND_FILE.OUTPUT,
               '    '
            || RPAD (l_org_code, 14, ' ')
            || '  '
            || RPAD (rec_print_output.process_status, 17, ' ')
            || '   '
            || RPAD (rec_print_output.item_number, 23, ' ')
            || ' '
            || RPAD (rec_print_output.subinventory, 15, ' ')
            || '   '
            || rec_print_output.stock_locator);
      END LOOP;
   EXCEPTION
      WHEN EXECUTION_ERROR
      THEN
         FND_FILE.PUT_LINE (FND_FILE.LOG, 'Error Message: ' || l_err_mess);
         X_RETCODE := 2;
      WHEN PROGRAM_ERROR
      THEN
         l_org_mess := SUBSTR (SQLERRM, 1, 250);

         XXCUS_error_pkg.XXCUS_error_main_api (
            p_called_from         => g_err_callfrom || '.' || l_procedure,
            p_calling             => g_sec,
            p_request_id          => g_request_id,
            p_ora_error_msg       => l_org_mess,
            p_error_desc          => l_err_mess,
            p_distribution_list   => g_dflt_email,
            p_module              => 'XXWC');
         X_RETCODE := 2;
      WHEN OTHERS
      THEN
         l_org_mess := SUBSTR (SQLERRM, 1, 250);

         XXCUS_error_pkg.XXCUS_error_main_api (
            p_called_from         => g_err_callfrom || '.' || l_procedure,
            p_calling             => g_sec,
            p_request_id          => g_request_id,
            p_ora_error_msg       => l_org_mess,
            p_error_desc          => NULL,
            p_distribution_list   => g_dflt_email,
            p_module              => 'XXWC');
         X_RETCODE := 2;
   END;


   PROCEDURE ITEM_STKLOC_REM_ASSIGN (p_org_id              IN     NUMBER,
                                     p_subinventory        IN     VARCHAR2,
                                     p_temp_table_insert   IN     VARCHAR2,
                                     x_return_status          OUT VARCHAR2,
                                     x_return_mess            OUT VARCHAR2)
   /**********************************************************************************************************************************
    PROCEDURE : ITEM_STKLOC_REM_ASSIGN

    REVISIONS:
    Ver           Date        Author                     Description
   ---------  -----------  ---------------    ----------------------------------------------------------------------------------------
    1.0       27-OCT-2015   P.Vamshidhar      TMS#20150430-00020 - Delete or End Date and Reassign Stock Locators in
                                              General Subinventory for Branch Relocation
                                              Initial Version
   ***********************************************************************************************************************************/
   IS
      -- Variables Declaration

      l_return_status   VARCHAR2 (10);
      l_msg_count       NUMBER := 0;
      l_msg_data        VARCHAR2 (2000);
      l_disable_date    MTL_ITEM_LOCATIONS.DISABLE_DATE%TYPE := SYSDATE;
      l_err_mess        VARCHAR2 (2000);
      l_status          VARCHAR2 (100);
      l_procedure       VARCHAR2 (100) := 'ITEM_STKLOC_REM_ASSIGN';
      PROGRAM_ERROR     EXCEPTION;

      -- Cursor to Pickup General Sub-Inventory Locators to Disable

      CURSOR cur_update_loc (
         lp_subinventory    VARCHAR2)
      IS
           SELECT MIL.STOCK_LOCATOR_ID, MIL.SUBINVENTORY, MIL.STOCK_LOCATOR
             FROM XXWC.XXWC_ITEM_STK_LOCATORS_TBL MIL
            WHERE     MIL.SUBINVENTORY = lp_subinventory
                  AND REQUEST_ID = G_REQUEST_ID
         GROUP BY MIL.STOCK_LOCATOR_ID, MIL.SUBINVENTORY, MIL.STOCK_LOCATOR;
   BEGIN
      g_sec := 'XXWC_INV_ITEM_REM_ASSIGNMENT - Start';

      FND_FILE.PUT_LINE (FND_FILE.LOG, 'ITEM_STKLOC_REM_ASSIGN - Started');

      -- Storing Item wise Locators Information into Global Temporaray Table
      IF p_temp_table_insert = 'Y'
      THEN
         BEGIN
            g_sec := 'Inserting Locators information into Global Temp Table';

            INSERT INTO XXWC.XXWC_ITEM_STK_LOCATORS_TBL (INVENTORY_ITEM_ID,
                                                         ITEM_NUMBER,
                                                         ORGANIZATION_ID,
                                                         SUBINVENTORY,
                                                         STOCK_LOCATOR,
                                                         STOCK_LOCATOR_ID,
                                                         CREATION_DATE,
                                                         REQUEST_ID)
               SELECT MSL.INVENTORY_ITEM_ID,
                      MSIB.SEGMENT1 ITEM_NUMBER,
                      p_org_id,
                      MSL.SUBINVENTORY_CODE,
                      MIL.SEGMENT1,
                      MIL.INVENTORY_LOCATION_ID,
                      SYSDATE,
                      G_REQUEST_ID
                 FROM APPS.MTL_SECONDARY_LOCATORS MSL,
                      APPS.MTL_SYSTEM_ITEMS_B MSIB,
                      APPS.MTL_ITEM_LOCATIONS MIL
                WHERE     MSL.SUBINVENTORY_CODE IN ('General', 'BranchRelo')
                      AND MSL.ORGANIZATION_ID = P_ORG_ID
                      AND MSL.INVENTORY_ITEM_ID = MSIB.INVENTORY_ITEM_ID
                      AND MSL.ORGANIZATION_ID = MSIB.ORGANIZATION_ID
                      AND MSL.SECONDARY_LOCATOR = INVENTORY_LOCATION_ID;

            FND_FILE.PUT_LINE (
               FND_FILE.LOG,
               ' Lines inserted into Table: ' || SQL%ROWCOUNT);
            COMMIT;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_err_mess := SUBSTR (SQLERRM, 1, 250);
               RAISE PROGRAM_ERROR;
         END;
      END IF;

      -- Locators Disable Process Start.

      g_sec :=
            ' General Locators Disable Process Started for Organization_id '
         || p_org_id;

      FOR rec_update_loc IN cur_update_loc (p_subinventory)
      LOOP
         -- Initilizing variables
         fnd_msg_pub.delete_msg;
         l_msg_data := NULL;
         l_msg_count := 0;
         l_return_status := NULL;
         l_status := NULL;

         g_sec :=
               ' Locators Disable Process for Locator_id: '
            || rec_update_loc.STOCK_LOCATOR_ID;

         -- Calling standard API to disable

         inv_loc_wms_pub.UPDATE_LOCATOR (
            x_return_status           => l_return_status,
            x_msg_count               => l_msg_count,
            x_msg_data                => l_msg_data,
            p_organization_id         => p_org_id,
            p_organization_code       => NULL,
            p_inventory_location_id   => rec_update_loc.STOCK_LOCATOR_ID,
            p_concatenated_segments   => NULL,
            p_disabled_date           => l_disable_date);

         -- Standard API Process Completed and results validation started.
         g_sec :=
               'Started API Process completed for Locator '
            || rec_update_loc.STOCK_LOCATOR_ID;

         IF l_return_status = 'S'
         THEN
            l_status := 'Success';
         ELSE
            IF NVL (L_msg_count, 0) > 0
            THEN
               FOR i IN 1 .. L_msg_count
               LOOP
                  l_msg_data :=
                        l_msg_Data
                     || SUBSTR (
                           fnd_msg_pub.get (p_msg_index   => i,
                                            p_encoded     => fnd_api.g_false),
                           1,
                           255);
               END LOOP;
            END IF;

            fnd_file.put_line (
               fnd_file.LOG,
               l_msg_data || ' ' || rec_update_loc.STOCK_LOCATOR_ID);
            l_status := 'Failed';
            ROLLBACK;
         END IF;

         g_sec :=
               'Updating Results into Custom table '
            || rec_update_loc.STOCK_LOCATOR_ID;

         UPDATE XXWC.XXWC_ITEM_STK_LOCATORS_TBL
            SET PROCESS_STATUS = l_status,
                error_mess =
                   SUBSTR (DECODE (error_mess, NULL, l_msg_data, error_mess),
                           1,
                           500)
          WHERE     STOCK_LOCATOR_ID = rec_update_loc.STOCK_LOCATOR_ID
                AND SUBINVENTORY = p_subinventory
                AND REQUEST_ID = G_REQUEST_ID;

         COMMIT;
      END LOOP;

      X_RETURN_STATUS := 'S';

      FND_FILE.PUT_LINE (FND_FILE.LOG, 'ITEM_STKLOC_REM_ASSIGN - Completed');
   EXCEPTION
      WHEN PROGRAM_ERROR
      THEN
         x_return_mess := l_err_mess;
         x_return_status := 'E';
         ROLLBACK;
      WHEN OTHERS
      THEN
         l_err_mess := SUBSTR (SQLERRM, 1, 500);
         x_return_mess := l_err_mess;
         XXCUS_error_pkg.XXCUS_error_main_api (
            p_called_from         => g_err_callfrom || '.' || l_procedure,
            p_calling             => g_sec,
            p_request_id          => g_request_id,
            p_ora_error_msg       => l_err_mess,
            p_error_desc          => 'Disabling Stock Locators Process - Failed.',
            p_distribution_list   => g_dflt_email,
            p_module              => 'XXWC');
         x_return_status := 'E';
         ROLLBACK;
   END;


   PROCEDURE STK_LOCATOR_DELETE_PRC (p_org_id          IN     NUMBER,
                                     p_org_code        IN     VARCHAR2,
                                     x_return_status      OUT VARCHAR2)
   /*******************************************************************************************************************************
     PROCEDURE : STK_LOCATOR_DELETE_PRC

     REVISIONS:
     Ver           Date        Author                     Description
     ---------  -----------  ---------------    ------------------------------------------------------------------------------------
     1.0        26-OCT-2015  P.Vamshidhar       TMS#20150430-00020 - Delete or End Date and Reassign Stock Locators in General
                                                Subinventory for Branch Relocation
                                                Initial Version
   *********************************************************************************************************************************/
   IS
      -- Initializing Variables
      l_procedure            VARCHAR2 (100) := 'STK_LOCATOR_DELETE_PRC';
      l_return_status        VARCHAR2 (10);
      l_msg_count            NUMBER := 0;
      l_msg_data             VARCHAR2 (2000);
      l_msg_data1            VARCHAR2 (2000);
      l_err_mess             VARCHAR2 (200);
      l_new_loc_assignment   VARCHAR2 (100);
      l_status               VARCHAR2 (10);
      ln_delete_count        NUMBER := 0;

      CURSOR cur_tobe_delete_stkloc
      IS
           SELECT stock_locator_id,
                  stock_locator,
                  inventory_item_id,
                  subinventory
             FROM XXWC.XXWC_ITEM_STK_LOCATORS_TBL
            WHERE PROCESS_STATUS = 'Success' AND REQUEST_ID = G_REQUEST_ID
         GROUP BY STOCK_LOCATOR_ID,
                  STOCK_LOCATOR,
                  inventory_item_id,
                  subinventory;
   BEGIN
      g_sec := ' STK_LOCATOR_DELETE_PRC - Start ';

      FND_FILE.PUT_LINE (FND_FILE.LOG, 'STK_LOCATOR_DELETE_PRC - Started');

      -- Deleting process start for Disabled Locators

      FOR rec_tobe_delete_stkloc IN cur_tobe_delete_stkloc
      LOOP
         -- Initializing Variables
         fnd_msg_pub.delete_msg;
         l_return_status := NULL;
         l_msg_count := 0;
         l_msg_data := NULL;
         l_msg_data1 := NULL;

         g_sec :=
               ' Deletion Process start for Stk Locator id: '
            || rec_tobe_delete_stkloc.stock_locator_id;

         fnd_file.put_line (
            fnd_file.LOG,
               ' Stock Locator Id: '
            || rec_tobe_delete_stkloc.stock_locator_id
            || ' Stock Locator '
            || rec_tobe_delete_stkloc.stock_locator
            || ' Org id'
            || p_org_id);

         -- Calling Standard API to delete disabled Stock Locator.

         INV_LOC_WMS_PUB.delete_locator (
            x_return_status           => l_return_status,
            x_msg_count               => l_msg_count,
            x_msg_data                => l_msg_data,
            p_inventory_location_id   => rec_tobe_delete_stkloc.stock_locator_id,
            p_concatenated_segments   => rec_tobe_delete_stkloc.stock_locator,
            p_organization_id         => p_org_id,
            p_organization_code       => NULL,
            p_validation_req_flag     => 'N');

         -- Deletion Process validation
         g_sec :=
               ' Deletion Process Completed for Stk Locator id: '
            || rec_tobe_delete_stkloc.stock_locator_id;

         IF l_return_status = 'S'
         THEN
            COMMIT;
            l_status := 'Removed';
            -- Deleting stock locators data from Secodary Locators table.
            -- Since No standard api to delete, deleting from base table.
            g_sec :=
                  ' Deleting data from secondary locators table for Stock Locator id: '
               || rec_tobe_delete_stkloc.stock_locator_id
               || ' '
               || rec_tobe_delete_stkloc.subinventory;

            BEGIN
               DELETE FROM apps.mtl_secondary_locators
                     WHERE     secondary_locator =
                                  rec_tobe_delete_stkloc.stock_locator_id
                           AND subinventory_code =
                                  rec_tobe_delete_stkloc.subinventory
                           AND organization_id = p_org_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_status := 'Failed';
                  l_msg_data1 :=
                        'Deleting stock Locator Processed failed'
                     || rec_tobe_delete_stkloc.stock_locator_id
                     || ' '
                     || SUBSTR (SQLERRM, 1, 250);
            END;

            FND_FILE.PUT_LINE (
               FND_FILE.LOG,
                  'Locator id'
               || rec_tobe_delete_stkloc.stock_locator_id
               || ' '
               || rec_tobe_delete_stkloc.subinventory
               || ' Deleted- '
               || SQL%ROWCOUNT);

            COMMIT;
         ELSE
            l_status := 'Failed';
            ROLLBACK;
            FND_FILE.PUT_LINE (
               FND_FILE.LOG,
                  rec_tobe_delete_stkloc.stock_locator_id
               || ' l_msg_count '
               || l_msg_count);

            fnd_file.put_line (
               fnd_file.LOG,
                  l_msg_data
               || ' -  '
               || rec_tobe_delete_stkloc.stock_locator_id);

            IF NVL (l_msg_count, 0) > 0
            THEN
               FOR i IN 1 .. l_msg_count
               LOOP
                  l_msg_data1 :=
                        l_msg_data1
                     || SUBSTR (
                           fnd_msg_pub.get (p_msg_index   => i,
                                            p_encoded     => fnd_api.g_false),
                           1,
                           255);
               END LOOP;
            END IF;

            fnd_file.put_line (
               fnd_file.LOG,
               l_msg_data1 || ' ' || rec_tobe_delete_stkloc.stock_locator_id);
         END IF;

         UPDATE XXWC.XXWC_ITEM_STK_LOCATORS_TBL
            SET PROCESS_STATUS = l_status,
                error_mess =
                   SUBSTR (
                      DECODE (error_mess, NULL, l_msg_data1, error_mess),
                      1,
                      500)
          WHERE     STOCK_LOCATOR_ID =
                       rec_tobe_delete_stkloc.stock_locator_id
                AND STOCK_LOCATOR = rec_tobe_delete_stkloc.stock_locator
                AND REQUEST_ID = G_REQUEST_ID;

         COMMIT;
      END LOOP;

      x_return_status := 'S';
      FND_FILE.PUT_LINE (FND_FILE.LOG, 'STK_LOCATOR_DELETE_PRC - Completed');
   EXCEPTION
      WHEN OTHERS
      THEN
         l_err_mess := SUBSTR (SQLERRM, 1, 500);
         ROLLBACK;
         XXCUS_error_pkg.XXCUS_error_main_api (
            p_called_from         => g_err_callfrom || '.' || l_procedure,
            p_calling             => g_sec,
            p_request_id          => g_request_id,
            p_ora_error_msg       => l_err_mess,
            p_error_desc          => 'Disabling Stock Locators Process - Failed.',
            p_distribution_list   => g_dflt_email,
            p_module              => 'XXWC');
         x_return_status := 'E';
   END;

   PROCEDURE CREATE_ASSIGN_LOCATORS_PRC (p_org_id          IN     NUMBER,
                                         X_RETURN_STATUS      OUT VARCHAR2)
   /*******************************************************************************************************************************
     PROCEDURE : CREATE_ASSIGN_LOCATORS_PRC

     REVISIONS:
     Ver           Date        Author                     Description
     ---------  -----------  ---------------    ------------------------------------------------------------------------------------
     1.0        26-OCT-2015  P.Vamshidhar       TMS#20150430-00020 - Delete or End Date and Reassign Stock Locators in General
                                                Subinventory for Branch Relocation
                                                Initial Version
   *********************************************************************************************************************************/
   IS
      -- Cursor to pickup branchrelo locators for creating General Relocators.
      CURSOR cur_branchrelo_lck
      IS
           SELECT STOCK_LOCATOR, STOCK_LOCATOR_ID, SUBINVENTORY
             FROM XXWC.XXWC_ITEM_STK_LOCATORS_TBL
            WHERE SUBINVENTORY = 'BranchRelo' AND REQUEST_ID = G_REQUEST_ID
         GROUP BY STOCK_LOCATOR, STOCK_LOCATOR_ID, SUBINVENTORY;

      -- Cursor to pickup items for assigning Genral Locators
      CURSOR cur_item_br_loca (
         lp_stk_locator_id    NUMBER)
      IS
         SELECT INVENTORY_ITEM_ID, ITEM_NUMBER, STOCK_LOCATOR
           FROM XXWC.XXWC_ITEM_STK_LOCATORS_TBL
          WHERE     SUBINVENTORY = 'BranchRelo'
                AND REQUEST_ID = G_REQUEST_ID
                AND STOCK_LOCATOR_ID = lp_stk_locator_id;


      -- Cursor to delete Branch Relo sub-inventory from item level

      CURSOR cur_item_sub_inven (
         lp_org_id    NUMBER)
      IS
         SELECT a.ROWID row_id,
                a.inventory_item_id,
                a.organization_id,
                a.ITEM_NUMBER
           FROM XXWC.XXWC_ITEM_STK_LOCATORS_TBL a
          WHERE     a.request_id = g_request_id
                AND process_status = 'Removed'
                AND SUBINVENTORY = 'BranchRelo'
                AND organization_id = lp_org_id;

      -- Variables Declaration

      l_new_subinventory            XXWC.XXWC_ITEM_STK_LOCATORS_TBL.SUBINVENTORY%TYPE
                                       := 'General';
      l_procedure                   VARCHAR2 (100) := 'CREATE_ASSIGN_LOCATORS_PRC';
      l_return_status               VARCHAR2 (10);
      l_msg_count                   NUMBER := 0;
      l_msg_data                    VARCHAR2 (2000);
      l_new_inventory_location_id   MTL_ITEM_LOCATIONS.INVENTORY_LOCATION_ID%TYPE;
      l_locator_exists              VARCHAR2 (10);
      l_disable_date                DATE := TRUNC (SYSDATE);
      l_error_mess                  VARCHAR2 (1000);
      l_ora_err_mess                VARCHAR2 (1000);
      l_update_status               VARCHAR2 (1000);
      l_msg_data1                   VARCHAR2 (1000);
   BEGIN
      FND_FILE.PUT_LINE (FND_FILE.LOG,
                         'CREATE_ASSIGN_LOCATORS_PRC - Started');

      g_sec := 'Branch Relo Deletion Process Started';

      FOR rec_branchrelo_lck IN cur_branchrelo_lck
      LOOP
         --Initializing Variables.
         fnd_msg_pub.DELETE_MSG;
         l_return_status := NULL;
         l_msg_count := 0;
         l_msg_data := NULL;

         g_sec := 'Calling Standard API to Update Locator';

         -- Enddating Loctor Assignment at Item level
         inv_loc_wms_pub.UPDATE_LOCATOR (
            x_return_status           => l_return_status,
            x_msg_count               => l_msg_count,
            x_msg_data                => l_msg_data,
            p_organization_id         => p_org_id,
            p_organization_code       => NULL,
            p_inventory_location_id   => rec_branchrelo_lck.stock_locator_id,
            p_concatenated_segments   => NULL,
            p_disabled_date           => l_disable_date);


         IF l_return_status = 'S'
         THEN
            COMMIT;

            FND_FILE.PUT_LINE (
               FND_FILE.LOG,
               rec_branchrelo_lck.stock_locator || ' Stock Locator Disabled');

            --Initializing Variables to delete stock locators.
            g_sec := 'Deleting Branch Relo Stock Locator';
            fnd_msg_pub.delete_msg;
            l_return_status := NULL;
            l_msg_count := 0;
            l_msg_data := NULL;

            INV_LOC_WMS_PUB.delete_locator (
               x_return_status           => l_return_status,
               x_msg_count               => l_msg_count,
               x_msg_data                => l_msg_data,
               p_inventory_location_id   => rec_branchrelo_lck.stock_locator_id,
               p_concatenated_segments   => rec_branchrelo_lck.stock_locator,
               p_organization_id         => p_org_id,
               p_organization_code       => NULL,
               p_validation_req_flag     => 'N');

            IF l_return_status = 'S'
            THEN
               COMMIT;
               l_update_status := 'Removed';

               g_sec :=
                     ' Deleting data from secondary locators table for Stock Locator id: '
                  || rec_branchrelo_lck.stock_locator_id
                  || ' '
                  || rec_branchrelo_lck.subinventory;

               BEGIN
                  DELETE FROM apps.mtl_secondary_locators
                        WHERE     secondary_locator =
                                     rec_branchrelo_lck.stock_locator_id
                              AND subinventory_code =
                                     rec_branchrelo_lck.subinventory
                              AND organization_id = p_org_id;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_update_status := 'Failed';
                     l_msg_data1 :=
                           'Deleting stock Locator Processed failed - '
                        || rec_branchrelo_lck.stock_locator_id
                        || ' '
                        || SUBSTR (SQLERRM, 1, 250);
                     l_update_status := 'Failed';
               END;

               FND_FILE.PUT_LINE (
                  FND_FILE.LOG,
                     'Locator id'
                  || rec_branchrelo_lck.stock_locator_id
                  || ' '
                  || rec_branchrelo_lck.subinventory
                  || ' Deleted- '
                  || SQL%ROWCOUNT);

               g_sec :=
                  'Updating Stock Locator status in Custom Table (Success)';

               UPDATE XXWC.XXWC_ITEM_STK_LOCATORS_TBL
                  SET Process_status = l_update_status,
                      error_mess = NVL (l_msg_data1, error_mess)
                WHERE     stock_locator_id =
                             rec_branchrelo_lck.stock_locator_id
                      AND REQUEST_ID = G_REQUEST_ID;

               COMMIT;

               -- Creating Locator.
               fnd_msg_pub.delete_msg;
               l_return_status := NULL;
               l_msg_count := 0;
               l_msg_data := NULL;

               g_sec :=
                  'Creating Locator ' || rec_branchrelo_lck.stock_locator;

               INV_LOC_WMS_PUB.CREATE_LOCATOR (
                  x_return_status              => l_return_status,
                  x_msg_count                  => l_msg_count,
                  x_msg_data                   => l_msg_data,
                  x_inventory_location_id      => l_new_inventory_location_id,
                  x_locator_exists             => l_locator_exists,
                  p_organization_id            => p_org_id,
                  p_organization_code          => NULL,
                  p_concatenated_segments      =>    rec_branchrelo_lck.stock_locator
                                                  || '.',
                  p_description                => NULL,
                  p_inventory_location_type    => 3,
                  p_picking_order              => NULL,
                  p_location_maximum_units     => NULL,
                  p_SUBINVENTORY_CODE          => l_new_subinventory,
                  p_LOCATION_WEIGHT_UOM_CODE   => NULL,
                  p_mAX_WEIGHT                 => NULL,
                  p_vOLUME_UOM_CODE            => NULL,
                  p_mAX_CUBIC_AREA             => NULL,
                  p_x_COORDINATE               => NULL,
                  p_Y_COORDINATE               => NULL,
                  p_Z_COORDINATE               => NULL,
                  p_PHYSICAL_LOCATION_ID       => NULL,
                  p_PICK_UOM_CODE              => NULL,
                  p_DIMENSION_UOM_CODE         => NULL,
                  p_LENGTH                     => NULL,
                  p_WIDTH                      => NULL,
                  p_HEIGHT                     => NULL,
                  p_STATUS_ID                  => 1,
                  p_dropping_order             => NULL);

               COMMIT;

               FND_FILE.PUT_LINE (
                  FND_FILE.LOG,
                     rec_branchrelo_lck.stock_locator
                  || ' Locator Creation Status '
                  || l_return_status
                  || ' Error Count '
                  || NVL (l_msg_count, 0)
                  || ' New Locator Id '
                  || l_new_inventory_location_id);

               IF     l_return_status = 'S'
                  AND l_new_inventory_location_id IS NOT NULL
               THEN
                  FOR rec_item_br_loca
                     IN cur_item_br_loca (
                           rec_branchrelo_lck.stock_locator_id)
                  LOOP
                     -- Initializing Variables
                     fnd_msg_pub.delete_msg;
                     l_return_status := NULL;
                     l_msg_count := 0;
                     l_msg_data := NULL;

                     g_sec := 'Assigning Stock Locator to Item ';

                     -- Assiging items to above created Stock Locator.
                     INV_LOC_WMS_PUB.create_loc_item_tie (
                        x_return_status           => l_return_status,
                        x_msg_count               => l_msg_count,
                        x_msg_data                => l_msg_data,
                        P_INVENTORY_ITEM_ID       => rec_item_br_loca.INVENTORY_ITEM_ID,
                        p_item                    => NULL,
                        P_ORGANIZATION_ID         => p_org_id,
                        p_organization_code       => NULL,
                        p_subinventory_code       => l_new_subinventory,
                        p_inventory_location_id   => l_new_inventory_location_id,
                        p_locator                 => NULL,
                        p_status_id               => NULL);

                     FND_FILE.PUT_LINE (
                        FND_FILE.LOG,
                           rec_branchrelo_lck.stock_locator
                        || ' Locator Item Assiging Status '
                        || l_return_status
                        || ' Error Count '
                        || NVL (l_msg_count, 0));

                     IF l_return_status = 'S'
                     THEN
                        g_sec := 'Inserting Data into Custom table ';

                        INSERT
                          INTO XXWC.XXWC_ITEM_STK_LOCATORS_TBL (
                                  INVENTORY_ITEM_ID,
                                  ITEM_NUMBER,
                                  SUBINVENTORY,
                                  STOCK_LOCATOR,
                                  STOCK_LOCATOR_ID,
                                  PROCESS_STATUS,
                                  ORGANIZATION_ID,
                                  CREATION_DATE,
                                  REQUEST_ID)
                        VALUES (rec_item_br_loca.INVENTORY_ITEM_ID,
                                rec_item_br_loca.ITEM_NUMBER,
                                l_new_subinventory,
                                rec_branchrelo_lck.stock_locator,
                                l_new_inventory_location_id,
                                'Added',
                                p_org_id,
                                SYSDATE,
                                G_REQUEST_ID);
                     ELSE
                        INSERT
                          INTO XXWC.XXWC_ITEM_STK_LOCATORS_TBL (
                                  INVENTORY_ITEM_ID,
                                  ITEM_NUMBER,
                                  SUBINVENTORY,
                                  STOCK_LOCATOR,
                                  STOCK_LOCATOR_ID,
                                  PROCESS_STATUS,
                                  ORGANIZATION_ID,
                                  CREATION_DATE,
                                  REQUEST_ID)
                        VALUES (rec_item_br_loca.INVENTORY_ITEM_ID,
                                rec_item_br_loca.ITEM_NUMBER,
                                l_new_subinventory,
                                rec_branchrelo_lck.stock_locator,
                                l_new_inventory_location_id,
                                'Not Added',
                                p_org_id,
                                SYSDATE,
                                G_REQUEST_ID);

                        FND_FILE.PUT_LINE (
                           FND_FILE.LOG,
                              rec_branchrelo_lck.stock_locator
                           || ' Stock Locator Creation process Failed '
                           || rec_item_br_loca.ITEM_NUMBER
                           || ' '
                           || l_return_status
                           || ' '
                           || l_msg_count);

                        IF l_msg_count > 0
                        THEN
                           FOR i IN 1 .. l_msg_count
                           LOOP
                              l_msg_data :=
                                    l_msg_Data
                                 || SUBSTR (
                                       fnd_msg_pub.get (
                                          p_msg_index   => i,
                                          p_encoded     => fnd_api.g_false),
                                       1,
                                       255);
                           END LOOP;
                        END IF;
                     END IF;

                     COMMIT;
                  END LOOP;
               ELSE
                  g_sec := 'Stock Creation Process Failed';

                  FND_FILE.PUT_LINE (
                     FND_FILE.LOG,
                        rec_branchrelo_lck.stock_locator
                     || ' Stock Locator Creation process Failed '
                     || l_return_status
                     || ' '
                     || l_msg_count);

                  IF l_msg_count > 0
                  THEN
                     FOR i IN 1 .. l_msg_count
                     LOOP
                        l_msg_data :=
                              l_msg_Data
                           || SUBSTR (
                                 fnd_msg_pub.get (
                                    p_msg_index   => i,
                                    p_encoded     => fnd_api.g_false),
                                 1,
                                 255);
                     END LOOP;
                  END IF;
               END IF;
            ELSE
               g_sec :=
                  'Updating Stock Locator status in Custom Table (Failed)';

               UPDATE XXWC.XXWC_ITEM_STK_LOCATORS_TBL
                  SET Process_status = 'Deletion Process Failed',
                      error_mess =
                         SUBSTR (
                            DECODE (error_mess, NULL, l_msg_data, error_mess),
                            1,
                            500)
                WHERE     stock_locator_id =
                             rec_branchrelo_lck.stock_locator_id
                      AND REQUEST_ID = G_REQUEST_ID;

               COMMIT;
               g_sec :=
                     'Stock Locator Deletion Process Failed'
                  || rec_branchrelo_lck.stock_locator;

               FND_FILE.PUT_LINE (
                  FND_FILE.LOG,
                     rec_branchrelo_lck.stock_locator
                  || ' Stock Locator Deletion process Failed '
                  || l_return_status
                  || ' '
                  || l_msg_count);

               IF l_msg_count > 0
               THEN
                  FOR i IN 1 .. l_msg_count
                  LOOP
                     l_msg_data :=
                           l_msg_Data
                        || SUBSTR (
                              fnd_msg_pub.get (
                                 p_msg_index   => i,
                                 p_encoded     => fnd_api.g_false),
                              1,
                              255);
                  END LOOP;
               END IF;
            END IF;
         ELSE
            UPDATE XXWC.XXWC_ITEM_STK_LOCATORS_TBL
               SET Process_status = 'Disable Process Failed',
                   error_mess =
                      SUBSTR (
                         DECODE (error_mess, NULL, l_msg_data, error_mess),
                         1,
                         500)
             WHERE     stock_locator_id = rec_branchrelo_lck.stock_locator_id
                   AND REQUEST_ID = G_REQUEST_ID;

            COMMIT;

            g_sec := 'Stock Locator Disabled Process Failed';
            l_error_mess :=
                  'Disable Process Failed to Stock Locator '
               || rec_branchrelo_lck.stock_locator;

            FND_FILE.PUT_LINE (
               FND_FILE.LOG,
                  rec_branchrelo_lck.stock_locator
               || ' Stock Locator Disabled process Failed '
               || l_return_status
               || ' '
               || l_msg_count);

            IF l_msg_count > 0
            THEN
               FOR i IN 1 .. l_msg_count
               LOOP
                  l_msg_data :=
                        l_msg_Data
                     || SUBSTR (
                           fnd_msg_pub.get (p_msg_index   => i,
                                            p_encoded     => fnd_api.g_false),
                           1,
                           255);
               END LOOP;
            END IF;
         END IF;
      END LOOP;

      FND_FILE.PUT_LINE (FND_FILE.LOG,
                         'Stock Locator Process Creation Process Completed');


      g_sec := 'Deleting Data from MTL_ITEM_SUB_INVENTORIES table';

      -- Since there no api to delete from below table, used direct delete and printing in log.
      FOR rec_item_sub_inven IN cur_item_sub_inven (p_org_id)
      LOOP
         BEGIN
            DELETE FROM MTL_ITEM_SUB_INVENTORIES
                  WHERE     inventory_item_id =
                               rec_item_sub_inven.inventory_item_id
                        AND organization_id =
                               rec_item_sub_inven.organization_id
                        AND SECONDARY_INVENTORY = 'BranchRelo';
         EXCEPTION
            WHEN OTHERS
            THEN
               FND_FILE.PUT_LINE (
                  FND_FILE.LOG,
                     ' Error occured while delting data from MTL_ITEM_SUB_INVENTORIES - Inventory Item Id'
                  || rec_item_sub_inven.inventory_item_id);
               ROLLBACK;
         END;


         IF NVL (SQL%ROWCOUNT, 0) > 0
         THEN
            FND_FILE.PUT_LINE (
               FND_FILE.LOG,
                  rec_item_sub_inven.item_number
               || ' BrachRelow subinventory deleted from MTL_ITEM_SUB_INVENTORIES table');
         ELSE
            FND_FILE.PUT_LINE (
               FND_FILE.LOG,
                  rec_item_sub_inven.item_number
               || ' BrachRelow subinventory not deleted from MTL_ITEM_SUB_INVENTORIES table');
         END IF;
      END LOOP;

      FND_FILE.PUT_LINE (
         FND_FILE.LOG,
         'Deletion data from MTL_ITEM_SUB_INVENTORIES table completed');

      COMMIT;

      x_return_status := 'S';
      FND_FILE.PUT_LINE (FND_FILE.LOG,
                         'CREATE_ASSIGN_LOCATORS_PRC - Completed');
   EXCEPTION
      WHEN OTHERS
      THEN
         l_ora_err_mess := SUBSTR (SQLERRM, 1, 500);

         XXCUS_error_pkg.XXCUS_error_main_api (
            p_called_from         => g_err_callfrom || '.' || l_procedure,
            p_calling             => g_sec,
            p_request_id          => g_request_id,
            p_ora_error_msg       => l_ora_err_mess,
            p_error_desc          => l_error_mess,
            p_distribution_list   => g_dflt_email,
            p_module              => 'XXWC');
         x_return_status := 'E';
   END;



   PROCEDURE PURGE_HISTORY_RECORDS (x_return_status OUT VARCHAR2)
   /*******************************************************************************************************************************
     PROCEDURE : CREATE_ASSIGN_LOCATORS_PRC

     REVISIONS:
     Ver           Date        Author                     Description
     ---------  -----------  ---------------    ------------------------------------------------------------------------------------
     1.0        26-OCT-2015  P.Vamshidhar       TMS#20150430-00020 - Delete or End Date and Reassign Stock Locators in General
                                                Subinventory for Branch Relocation
                                                Initial Version
   *********************************************************************************************************************************/
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;

      l_procedure      VARCHAR2 (100) := 'PURGE_HISTORY_RECORDS';
      l_ora_err_mess   VARCHAR2 (1000);
      l_error_mess     VARCHAR2 (1000) := 'Purging Records from Custom Table';
   BEGIN
      g_sec := 'Purging Records from Custom Table';

      DELETE FROM XXWC.XXWC_ITEM_STK_LOCATORS_TBL
            WHERE TRUNC (CREATION_DATE) <= TRUNC (SYSDATE) - 7;

      FND_FILE.PUT_LINE (
         FND_FILE.LOG,
            ' Number of Records Purged from XXWC.XXWC_ITEM_STK_LOCATORS_TBL : '
         || SQL%ROWCOUNT);
      COMMIT;
      x_return_status := 'S';
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         l_ora_err_mess := SUBSTR (SQLERRM, 1, 500);
         FND_FILE.PUT_LINE (FND_FILE.LOG,
                            ' l_ora_err_mess ' || l_ora_err_mess);
         XXCUS_error_pkg.XXCUS_error_main_api (
            p_called_from         => g_err_callfrom || '.' || l_procedure,
            p_calling             => g_sec,
            p_request_id          => g_request_id,
            p_ora_error_msg       => l_ora_err_mess,
            p_error_desc          => l_error_mess,
            p_distribution_list   => g_dflt_email,
            p_module              => 'XXWC');
         x_return_status := 'E';
   END;
END XXWC_INV_ITEM_STK_LOCATORS_PKG;
/