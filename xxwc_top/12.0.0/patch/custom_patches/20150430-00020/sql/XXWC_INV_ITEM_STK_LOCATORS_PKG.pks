/* Formatted on 11/8/2015 11:03:45 PM (QP5 v5.265.14096.38000) */
CREATE OR REPLACE PACKAGE APPS.XXWC_INV_ITEM_STK_LOCATORS_PKG
AS
   /*************************************************************************************************************************************************************
   -- File Name: XXWC_INV_ITEM_STK_LOCATORS_PKG.pks
   --
   -- PROGRAM TYPE: PL/SQL Script   <API>
   --
   -- PURPOSE:  Package developed to delete General Stock Locators and assign stock locators from BranchRelo to General.
   -- HISTORY
   -- ===========================================================================================================================================================
   -- ===========================================================================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- ---------------------------------------------------------------------------------------------------------------------
   -- 1.0     26-OCT-2015   P.Vamshidhar    TMS#20150430-00020 - Delete or End Date and Reassign Stock Locators in General Subinventory for Branch Relocation

   ***************************************************************************************************************************************************************/

   PROCEDURE STKLOC_MAIN_PRC (X_ERRORBUF      OUT VARCHAR2,
                              X_RETCODE       OUT NUMBER,
                              P_ORG_ID     IN     NUMBER);

   PROCEDURE ITEM_STKLOC_REM_ASSIGN (p_org_id              IN     NUMBER,
                                     p_subinventory        IN     VARCHAR2,
                                     p_temp_table_insert   IN     VARCHAR2,
                                     x_return_status          OUT VARCHAR2,
                                     x_return_mess            OUT VARCHAR2);

   PROCEDURE STK_LOCATOR_DELETE_PRC (p_org_id          IN     NUMBER,
                                     p_org_code        IN     VARCHAR2,
                                     x_return_status      OUT VARCHAR2);

   PROCEDURE CREATE_ASSIGN_LOCATORS_PRC (p_org_id          IN     NUMBER,
                                         X_RETURN_STATUS      OUT VARCHAR2);

   PROCEDURE PURGE_HISTORY_RECORDS (X_RETURN_STATUS OUT VARCHAR2);
END XXWC_INV_ITEM_STK_LOCATORS_PKG;
/