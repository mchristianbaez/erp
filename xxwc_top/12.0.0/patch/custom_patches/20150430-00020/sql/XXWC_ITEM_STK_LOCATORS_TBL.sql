/*************************************************************************************************************************************************************
   -- File Name:  XXWC_ITEM_STK_LOCATORS_TBL.sql
   --
   -- PROGRAM TYPE: Table
   --
   -- PURPOSE:  Table to Maintain Modified Stock locators information while concurrent Program in process.
   -- HISTORY
   -- ===========================================================================================================================================================
   -- ===========================================================================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- ---------------------------------------------------------------------------------------------------------------------
   -- 1.0     26-OCT-2015   P.Vamshidhar    TMS#20150430-00020 - Delete or End Date and Reassign Stock Locators in General Subinventory for Branch Relocation

  ***************************************************************************************************************************************************************/

CREATE TABLE XXWC.XXWC_ITEM_STK_LOCATORS_TBL
(
  INVENTORY_ITEM_ID  NUMBER,
  ITEM_NUMBER        VARCHAR2(20 BYTE),
  SUBINVENTORY       VARCHAR2(30 BYTE),
  STOCK_LOCATOR      VARCHAR2(50 BYTE),
  STOCK_LOCATOR_ID   NUMBER,
  PROCESS_STATUS     VARCHAR2(100 BYTE),
  ORGANIZATION_ID    NUMBER,
  REQUEST_ID         NUMBER,
  CREATION_DATE      DATE,
  ERROR_MESS         VARCHAR2(1000 BYTE)
);
/

