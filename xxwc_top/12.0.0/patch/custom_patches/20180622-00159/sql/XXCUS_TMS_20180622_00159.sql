/*
 TMS:  20180622-00159
 Date: 06/22/2018
 Notes:  update GL INTERFACE accounting date
*/
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
 --
 n_loc number :=0;
 l_sql varchar2(2000) :=Null;
 --
BEGIN --Main Processing...
   --
   n_loc :=101;
   --
   --
   begin
    --
    n_loc :=104;
    --        
    UPDATE GL_INTERFACE SET accounting_date ='28-MAY-2018', status='NEW'
    where 1 =1
    and user_je_source_name ='PeopleSoft'
    and user_je_category_name ='Payroll'
	and to_char(accounting_date, 'Mon-YYYY') ='May-2018'
    ;
    --
    n_loc :=105;  
    --        
    dbms_output.put_line('Rows updated :'||sql%rowcount);
    --
    n_loc :=107;
    --        
   exception
    when others then
     --
     n_loc :=108;
     --
     dbms_output.put_line('@ n_loc ='||n_loc||', message ='||sqlerrm);
     --  
   end;
   --
   commit;
   --
EXCEPTION
   WHEN OTHERS THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20180622-00159, Errors =' || SQLERRM);
END;
/