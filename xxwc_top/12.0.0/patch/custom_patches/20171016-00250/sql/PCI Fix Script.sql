DECLARE

CURSOR cur IS
SELECT /*+ INDEX(xxwc.xxwc_oracle_attach_arch_tbl xxwc.xxwc_oracle_attach_arch_n1)*/ arch.old_document_id , ap.document_id
                         FROM xxwc.xxwc_oracle_attach_arch_tbl arch
                            , apps.fnd_attached_documents ap 
                        WHERE 1 = 1                          
                          and ap.ENTITY_NAME = 'AP_INVOICES'
                          and ap.ENTITY_NAME = arch.ENTITY_NAME
                          and ap.PK1_VALUE = arch.PK1_VALUE
                          and ap.SEQ_NUM = arch.SEQ_NUM
                          ;

l_main_ctr NUMBER := 0;                          
l_Cntr NUMBER := 0;                          

BEGIN

FOR rec IN CUR LOOP

UPDATE fnd_attached_documents oie
SET oie.document_id = rec.document_id
, last_update_date = sysdate
WHERE 1=1
and oie.ENTITY_NAME = 'OIE_LINE_ATTACHMENTS'
and oie.document_id = rec.old_document_id;

l_Cntr := l_Cntr + 1;
l_main_ctr := l_main_ctr + 1;

IF l_Cntr = 1000 THEN
COMMIT;
l_Cntr := 0;
END IF;

END LOOP;

dbms_output.put_line(' Number of records updated - '||l_main_ctr);

EXCEPTION
WHEN OTHERS THEN
dbms_output.put_line(' Error - '||SQLERRM);
END;  
/