-- TMS#20171212-00222 - ARS (Attribute12 ra_customer_trx_all) data script -  Chile Request
-- Created by P.Vamshidhar
SET SERVEROUT ON
BEGIN
   DBMS_OUTPUT.PUT_LINE ('Data fix Begin');

   UPDATE RA_CUSTOMER_TRX_ALL A
      SET A.ATTRIBUTE12 = '09-DEC-17'
    WHERE     EXISTS
                 (SELECT 1
                    FROM XXWC.XXWC_ARS_OB_DATA_FIX_TBL B
                   WHERE B.TRX_NUMBER = A.TRX_NUMBER)
          AND A.ATTRIBUTE12 IS NULL;

   DBMS_OUTPUT.PUT_LINE ('Number of rows Updated ' || SQL%ROWCOUNT);

   COMMIT;
   DBMS_OUTPUT.PUT_LINE ('Data Fix Completed');   
   
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.PUT_LINE ('Error occured ' || SUBSTR (SQLERRM, 1, 250));
      ROLLBACK;
END;
/





