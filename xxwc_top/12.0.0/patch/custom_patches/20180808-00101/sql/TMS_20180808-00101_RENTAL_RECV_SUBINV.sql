/*************************************************************************
  $Header TMS_20180808-00101_RENTAL_RECV_SUBINV.sql $
  Module Name: TMS_20180808-00101_RENTAL_RECV_SUBINV

  PURPOSE: Data fix to add rental as receiving subinvetory for 
           all rental and re-rent items.
  
  ----------------------------------------------------------------
  ADD RENTAL AS RECEIVING
  ----------------------------------------------------------------

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        16-AUG-2018  Pattabhi Avula        TMS#20180808-00101 

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000;

DECLARE
   v_count    NUMBER := 0;
   v_count1   NUMBER := 0;

   CURSOR C1
   IS
      SELECT DISTINCT segment1,
                      item_type,
                      inventory_item_id,
                      a.organization_id,
                      b.organization_code
        FROM mtl_system_items_b a, org_organization_definitions b
       WHERE     a.organization_id = b.organization_id
             AND a.segment1 LIKE 'R%'             
             AND a.item_type IN ('RENTAL','RE_RENT')
             AND b.operating_unit=162
             AND b.organization_code NOT IN ('MST','WCC')
             and NVL(b.disable_date,trunc(sysdate))>=trunc(sysdate)
             --AND a.segment1='R1132330660U'
             AND NOT EXISTS
                    (SELECT 1
                       FROM mtl_item_sub_defaults
                      WHERE     inventory_item_id = a.inventory_item_id
                            AND organization_id = a.organization_id
                            AND default_type = '2');

   CURSOR C2
   IS
      SELECT DISTINCT segment1,
                      item_type,
                      inventory_item_id,
                      a.organization_id,
                      b.organization_code
        FROM mtl_system_items_b a, org_organization_definitions b
       WHERE     a.organization_id = b.organization_id
             AND a.segment1 LIKE 'R%'
             AND a.item_type IN ('RENTAL', 'RE_RENT')
             AND a.item_type IN ('RENTAL','RE_RENT')
             AND b.operating_unit=162
             AND b.organization_code NOT IN ('MST','WCC')             
             --AND a.segment1='R1132330660U'
             AND EXISTS
                    (SELECT 1
                       FROM mtl_item_sub_defaults
                      WHERE     inventory_item_id = a.inventory_item_id
                            AND organization_id = a.organization_id
                            AND default_type = '2'
                            AND subinventory_code != 'Rental');
BEGIN
   fnd_global.apps_initialize (user_id        => 50485,
                               resp_id        => 50884,
                               resp_appl_id   => 401);

   mo_global.set_policy_context ('S', 162);

   FOR i IN C1
   LOOP
      BEGIN
         INSERT INTO mtl_item_sub_defaults (inventory_item_id,
                                            organization_id,
                                            subinventory_code,
                                            default_type,
                                            last_update_date,
                                            last_updated_by,
                                            creation_date,
                                            created_by,
                                            last_update_login)
              VALUES (i.inventory_item_id,
                      i.organization_id,
                      'Rental',
                      2,                                           --receiving
                      SYSDATE,
                      50485,
                      SYSDATE,
                      50485,
                      -1);

         v_count := v_count + 1;
      EXCEPTION
         WHEN OTHERS
         THEN
            DBMS_OUTPUT.put_line (
                  'Item-'
               || i.segment1
               || ' Orgn-'
               || i.organization_code
               || 'Error-'
               || SQLERRM);
            NULL;
      END;
   END LOOP;

   DBMS_OUTPUT.put_line ('Total Number of records inserted-' || v_count);
   COMMIT;

   FOR j IN C2
   LOOP
      BEGIN
         UPDATE mtl_item_sub_defaults
            SET subinventory_code = 'Rental',
                last_updated_by = 50485,
                last_update_date = SYSDATE
          WHERE     inventory_item_id = j.inventory_item_id
                AND organization_id = j.organization_id;

         v_count1 := v_count1 + 1;
      EXCEPTION
         WHEN OTHERS
         THEN
            DBMS_OUTPUT.put_line (
                  'Item-'
               || j.segment1
               || ' Orgn-'
               || j.organization_code
               || 'Error-'
               || SQLERRM);
            NULL;
      END;
   END LOOP;

   DBMS_OUTPUT.put_line ('Total Number of records updated-' || v_count1);
   COMMIT;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Error-' || SQLERRM);
END;
/