create or replace procedure apps.xxcus_gsc_pp_checkrecon (retcode out varchar2, errbuf out varchar2, p_days in varchar2) as
/*
 -- Author: Balaguru Seshadri
 -- Parameters: Number of days to go back
 -- Scope: Used by HR Peoplesoft sysetm for payroll check reconciliaton process for outstanding items
 -- Modification History
 -- ESMS        TMS              Date          Version  Comments
 -- =========== ===============  ==========    =======  ========================
 -- 192733      20160225-00147   01-MAR-2016   1.0      Created.
 */ 
--
 cursor all_records (l_days in  number) is 
  select '061000052' as fld1,
         ba.bank_account_num fld2,
         bsl.bank_trx_number fld3,
         bsl.amount fld4
    from ce.ce_statement_lines bsl
             ,ce.ce_statement_headers bsh
             ,apps.ce_bank_accounts    ba
   where 1 =1
     and ba.bank_account_id =bsh.bank_account_id   
     and bsh.bank_account_id = 10017
     and bsh.statement_date like sysdate - l_days
     and bsh.statement_header_id = bsl.statement_header_id
     and bsl.trx_code = 581
  union  
  select '024156792',
         ba.bank_account_num,                
         bsl.bank_trx_number,
         bsl.amount 
    from ce.ce_statement_lines bsl
             ,ce.ce_statement_headers bsh
             ,apps.ce_bank_accounts ba
   where 1 =1
     and ba.bank_account_id =bsh.bank_account_id
     and bsh.bank_account_id = 19010
     and bsh.statement_date like sysdate -l_days
     and bsh.statement_header_id = bsl.statement_header_id
     and bsl.trx_code = 581; 
--
    file_id         utl_file.file_type;
    b_proceed boolean :=null;
    l_req_id number;
    l_path VARCHAR2(240) :=Null;
    --
    l_days number :=p_days;
    l_command VARCHAR2(4000) :=NULL;
    l_result VARCHAR2(4000) :=NULL;
    l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
   --
   l_header varchar2(240) :='TRANSIT,ACCOUNT,PAYCHECK_NUM,NET_PAY';
   --
    l_buffer           varchar2(32767);
    l_tmp_name  varchar2(150);
    l_name            varchar2(150);
  --    
    l_lines              pls_integer :=0;
    c_eol             constant varchar2(1) :=chr(10);
    c_eol_len   constant pls_integer :=length(c_eol);
    c_maxline  constant pls_integer :=32767;   
   --    
 --
     procedure print_log(p_message in varchar2) is
     begin
      if fnd_global.conc_request_id >0 then
       fnd_file.put_line(fnd_file.log, p_message);
       fnd_file.put_line(fnd_file.output, p_message);       
      else
       dbms_output.put_line(p_message);  
      end if;
     end print_log;
     --    
begin 
     --
     b_proceed :=true;
     --
     begin
        select directory_path
        into    l_path
        from sys.all_directories 
        where 1 =1
             and directory_name ='XXCUS_GSC_PP_CHECKRECON_OB_DIR';
     exception
      when others then
       b_proceed :=FALSE;
     end;
     --
     if fnd_global.conc_request_id >0 then
      l_req_id :=fnd_global.conc_request_id;
     else
      l_req_id :=0;     
     end if;
     --
     print_log('');
     print_log(' Number of days to go back from current date :'||l_days); 
     print_log(''); 
     --
     print_log('Begin extract');
     print_log('');  
      --
      --Assign the file name 
       l_tmp_name :='TMP_XXCUS_GSC_PP_CHECKRECON_'||l_req_id||'.csv';
       --
       l_name :='XXCUS_GSC_PP_CHECKRECON_'||l_req_id||'.csv';
       --
      print_log('Temp file =>'||l_tmp_name);
      print_log(' ');
      print_log('Final file =>'||l_name);
      --
       --Get the file pointer
       file_id :=utl_file.fopen('XXCUS_GSC_PP_CHECKRECON_OB_DIR', l_tmp_name, 'W', c_maxline);   --first we create temp file so that UC4 job doesn't pick up while the file is still in progress...
       --
       utl_file.put_line(file_id, l_header);
       --
       print_log('Header copied to file...');   
       -- 
     if (b_proceed) then 
      --
         begin 
            --
            for rec in all_records (l_days =>p_days) loop
               --
               l_buffer :=rec.fld1||','||rec.fld2||','||rec.fld3||','||rec.fld4;
               --
               utl_file.put_line(file_id, l_buffer);
               --             
            end loop;  
            --     
          exception
           --
           when others then
            --
            print_log('Error in loop, message:  '||sqlerrm);
            --
         end;
         --   
      --
     end if; 
     --     
     utl_file.fclose(file_id);
     --   
     b_proceed :=TRUE;
     --
     if (b_proceed) then
         --
         begin
                l_command :='chmod 755'
                           ||' '
                         ||l_path
                         ||'/' 
                         ||l_tmp_name;
                 --                     
                 print_log('Set Permissions on temp csv file');
                 print_log('===========================');
                 print_log(l_command);                  
                 --
                 select xxwc_edi_iface_pkg.xxwc_oscommand_run (l_command)
                 into   l_result 
                 from   dual;             
                --
                l_command :=null;
                 --                
                b_proceed :=TRUE;
                --
         exception 
          when others then
            print_log('Failed to modify permissions for temp csv file '||l_tmp_name);
            xxcus_error_pkg.xxcus_error_main_api(p_called_from       => 'apps.xxcus_gsc_pp_checkrecon'
                                            ,p_calling           => 'Set Permissions on temp csv file'
                                            ,p_request_id        => fnd_global.conc_request_id
                                            ,p_ora_error_msg     => substr(' Error_Stack...' ||
                                                                           dbms_utility.format_error_stack() ||
                                                                           ' Error_Backtrace...' ||
                                                                           dbms_utility.format_error_backtrace()
                                                                          ,1
                                                                          ,2000)
                                            ,p_error_desc        => substr(sqlerrm
                                                                          ,1
                                                                          ,240)
                                            ,p_distribution_list => l_distro_list
                                            ,p_module            => 'GSC PSOFT PAYROLL CHECKRECON');            
            b_proceed :=FALSE;       
         end; 
         --   
     end if;     
     --     
     if (b_proceed) then
         --
         begin
                 l_command :='mv'
                           ||' '
                           ||l_path
                           ||'/' 
                           ||l_tmp_name --temp csv file
                           ||' '
                           ||l_path
                           ||'/' 
                           ||l_name; --final csv file
                 --                     
                 print_log('MV command:');
                 print_log('============');
                 print_log(l_command);                  
                 --             
                 select xxwc_edi_iface_pkg.xxwc_oscommand_run (l_command)
                 into   l_result 
                 from   dual;             
                --
         exception 
          when others then
            print_log('Failed to rename the temp csv file '||l_tmp_name||' to final csv file '||l_name);
            xxcus_error_pkg.xxcus_error_main_api(p_called_from       =>  'apps.xxcus_gsc_pp_checkrecon'
                                            ,p_calling           => 'Rename temp file to final'
                                            ,p_request_id        => fnd_global.conc_request_id
                                            ,p_ora_error_msg     => substr(' Error_Stack...' ||
                                                                           dbms_utility.format_error_stack() ||
                                                                           ' Error_Backtrace...' ||
                                                                           dbms_utility.format_error_backtrace()
                                                                          ,1
                                                                          ,2000)
                                            ,p_error_desc        => substr(sqlerrm
                                                                          ,1
                                                                          ,240)
                                            ,p_distribution_list => l_distro_list
                                            ,p_module            => 'GSC PSOFT PAYROLL CHECKRECON');                         
         end; 
         --   
     end if;
     --
     print_log('End extract');
     --
exception
 when others then
  print_log ('Main Block, Error =>'||sqlerrm);
        xxcus_error_pkg.xxcus_error_main_api(p_called_from       => 'apps.xxcus_gsc_pp_checkrecon'
                                            ,p_calling           => 'Outer block'
                                            ,p_request_id        => fnd_global.conc_request_id
                                            ,p_ora_error_msg     => substr(' Error_Stack...' ||
                                                                           dbms_utility.format_error_stack() ||
                                                                           ' Error_Backtrace...' ||
                                                                           dbms_utility.format_error_backtrace()
                                                                          ,1
                                                                          ,2000)
                                            ,p_error_desc        => substr(sqlerrm
                                                                          ,1
                                                                          ,240)
                                            ,p_distribution_list => l_distro_list
                                            ,p_module            => 'GSC PSOFT PAYROLL CHECKRECON');  
end xxcus_gsc_pp_checkrecon;
/