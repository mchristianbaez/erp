/******************************************************************************************************
-- File Name: ALTER_XXWC_ITEM_STK_LOCATORS_TBL.sql
--
-- PROGRAM TYPE: Table script
--
-- PURPOSE: Altering table 
-- HISTORY
-- ==========================================================================================================
-- ==========================================================================================================
-- VERSION DATE          AUTHOR(S)       DESCRIPTION
-- ------- -----------   --------------- --------------------------------------------------------------------
-- 1.0     05-Dec-2016   P.Vamshidhar    TMS#20161205-00204 Correct item character length issue in 
--                                       XXWC Stock Locators Reassignment program

************************************************************************************************************/
ALTER TABLE XXWC.XXWC_ITEM_STK_LOCATORS_TBL
   MODIFY ( ITEM_NUMBER VARCHAR2 (40) );
/