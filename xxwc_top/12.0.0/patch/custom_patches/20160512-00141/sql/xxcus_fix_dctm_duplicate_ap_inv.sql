/*
 TMS: 20160512-00141
 Date: 05/12/2016
 Notes: Script to  
                1) Remove duplicate invoice id rows in ap_invoices_interface and ap_invoice_lines_interface for specific invoice_id.
                2) Update interfaced_flag  records for group_id 20160509230269 in the table xxcus.xxcusap_dctm_inv_header_tbl  to "X" -- Cancelled so that they do not get pulled for the subsequent runs
*/
set serveroutput on size 1000000
declare
 --
 n_loc number;
 --
begin --Main Processing...
   --
   n_loc :=101;
   --
   begin 
    --
    update xxcus.xxcusap_dctm_inv_header_tbl set interfaced_flag ='X'
    where 1 =1
    and invoice_id IN 
    (
     6524843
    ,6524847
    ,6524848
    ,6524849
    ,6524850
    ,6524851
    );
    --
    dbms_output.put_line('Total rows updated in table xxcus.xxcusap_dctm_inv_header_tbl ='||sql%rowcount);
    --
    n_loc :=102;
    --
    commit;
    --
   exception
    when others then
     --
     n_loc :=103;
     --
     dbms_output.put_line('@ Failed to update table xxcus.xxcusap_dctm_inv_header_tbl, @'||n_loc||', message ='||sqlerrm);
     --   
     rollback;
     --
   end;
   --
   begin 
    --
    n_loc :=104;
    --
    delete  apps.ap_invoice_lines_interface
    where 1 =1
    and invoice_id IN 
    (
     6524843
    ,6524847
    ,6524848
    ,6524849
    ,6524850
    ,6524851
    );
    --
    dbms_output.put_line('Total rows deleted from table apps.ap_invoice_lines_interface for specific invoice_id ='||sql%rowcount);
    --
    n_loc :=105;
    --
    commit;
    --
   exception
    when others then
     --
     n_loc :=106;
     --
     dbms_output.put_line('@ Failed to delete from table apps.ap_invoice_lines_interface, @'||n_loc||', message ='||sqlerrm);
     --   
     rollback;
     --
   end;
   --  
   begin 
    --
    n_loc :=107;
    --
    delete  apps.ap_invoices_interface
    where 1 =1
    and invoice_id IN 
    (
     6524843
    ,6524847
    ,6524848
    ,6524849
    ,6524850
    ,6524851
    );
    --
    dbms_output.put_line('Total rows deleted from table apps.ap_invoices_interface for specific invoice_id ='||sql%rowcount);
    --
    n_loc :=108;
    --
    commit;
    --
   exception
    when others then
     --
     n_loc :=109;
     --
     dbms_output.put_line('@ Failed to delete from table apps.ap_invoices_interface, @'||n_loc||', message ='||sqlerrm);
     --   
     rollback;
     --
   end;
   --       
exception
   when others then
      dbms_output.put_line ('TMS: 20160512-00141 , Outer block errors =' || sqlerrm);
end;
/