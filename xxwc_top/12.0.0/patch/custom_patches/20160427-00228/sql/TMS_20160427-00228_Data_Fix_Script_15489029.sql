/*************************************************************************
  $Header TMS_20160427-00228_Data_Fix_Script_15489029.sql $
  Module Name: TMS#20160427-00228   DataFix for unprocessed shipping transactions SO-20335408

  PURPOSE: Data fix to update the Devliery Details

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        28-APR-2016  Niraj Ranjan         TMS#20160427-00228

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 100000;
SET VERIFY OFF;

BEGIN

DBMS_OUTPUT.put_line ('TMS: 20160427-00228    , Before Update');
UPDATE wsh_delivery_details 
SET released_status = 'D', 
src_requested_quantity = 0, 
requested_quantity = 0, 
shipped_quantity = 0, 
cycle_count_quantity = 0, 
cancelled_quantity = 0, 
subinventory = null, 
locator_id = null, 
lot_number = null, 
revision = null, 
inv_interfaced_flag = 'X', 
oe_interfaced_flag = 'X' 
WHERE delivery_detail_id =15489029 ; 

DBMS_OUTPUT.put_line ('TMS: 20160427-00228   - number of records updated: '|| SQL%ROWCOUNT);
COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20160427-00228     , End Update');
   
END;
/	  
