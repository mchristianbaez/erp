/*************************************************************************
  $Header TMS_20180725-00038_UPDATE_CUSTOMER_FSD_DFF.sql $
  Module Name: TMS_20180725-00038  UPDATE CUSTOMER FSD DFF SCRIPT

  PURPOSE: Data fix to update the UPDATE CUSTOMER FSD DFF SCRIPT
  
  ----------------------------------------------------------------
  UPDATE CUSTOMER FSD DFF SCRIPT
  ----------------------------------------------------------------

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        25-JUL-2018  Pattabhi Avula        TMS#20180725-00038 

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 100000;
SET VERIFY OFF;

DECLARE
  
BEGIN

DBMS_OUTPUT.put_line ('TMS: 20180725-00038    , Before Update');

FOR REC IN (SELECT DISTINCT customer_number
                  ,customer_site_number
                  ,fsd
                  ,a.salesrep_id
                  ,hcas1.cust_acct_site_id
              FROM XXWC.XXWC_AR_CONV_CUST_ACCT_TBL stg
                  ,RA_SALESREPS_ALL A
                  ,XXWC.XXWC_AHH_AM_T B
                  ,apps.hz_cust_acct_sites_all hcas1
             WHERE 1=1
               AND stg.fsd IS NOT NULL
               AND A.ORG_ID = 162
               AND UPPER (B.SLSREPOUT) = UPPER (stg.FSD)
               AND A.SALESREP_NUMBER = B.SALESREP_ID
               AND hcas1.attribute17 =  customer_number||'-'||customer_site_number
            UNION
            SELECT DISTINCT stg.ORIG_SYSTEM_REFERENCE customer_number
                         ,stg.ATTRIBUTE17 customer_site_number
                         ,fsd
                         ,a.salesrep_id
                         ,hcas1.cust_acct_site_id
                     FROM xxwc.xxwc_ar_cust_site_iface_stg stg
                         ,RA_SALESREPS_ALL A
                         ,XXWC.XXWC_AHH_AM_T B
                         ,apps.hz_cust_acct_sites_all hcas1
                         ,apps.hz_cust_site_uses_all hcsu1 
                    WHERE 1=1
                      AND stg.fsd IS NOT NULL
                      AND A.ORG_ID = 162
                      AND UPPER (B.SLSREPOUT) = UPPER (stg.FSD)
                      AND A.SALESREP_NUMBER = B.SALESREP_ID
                      AND hcas1.cust_acct_Site_id= hcsu1.cust_acct_Site_id
                      AND hcsu1.attribute13 != a.salesrep_id  
                      AND hcas1.attribute17 =  stg.ORIG_SYSTEM_REFERENCE||'-'||stg.ATTRIBUTE17
            UNION
            SELECT DISTINCT customer_number
                         ,customer_site_number
                         ,fsd
                         ,a.salesrep_id
                         ,hcas1.cust_acct_site_id
                     FROM XXWC.XXWC_AR_CONV_CUST_ACCT_TBL_BKP stg
                         ,RA_SALESREPS_ALL A
                         ,XXWC.XXWC_AHH_AM_T B
                         ,apps.hz_cust_acct_sites_all hcas1
                    WHERE 1=1
                      AND stg.fsd IS NOT NULL
                      AND A.ORG_ID = 162
                      AND UPPER (B.SLSREPOUT) = UPPER (stg.FSD)
                      AND A.SALESREP_NUMBER = B.SALESREP_ID
                  AND hcas1.attribute17 =  customer_number||'-'||customer_site_number
            ) 
 LOOP
  UPDATE apps.hz_cust_site_uses_all hcsu
     SET attribute13 = REC.salesrep_id
   WHERE cust_acct_site_id = REC.cust_acct_site_id
     AND attribute13 = -3
     AND apps.xxwc_om_force_ship_pkg.is_row_locked (hcsu.ROWID, 'HZ_CUST_SITE_USES_ALL')  = 'N';
 
END LOOP;

COMMIT;

DBMS_OUTPUT.put_line (
         'TMS: 20180725-00038 - number of records updated: '
      || SQL%ROWCOUNT);

DBMS_OUTPUT.put_line ('TMS: 20180725-00038   , End Update');
EXCEPTION
 WHEN OTHERS THEN
  DBMS_OUTPUT.put_line ('Unexpected error while executing the script: '||SQLERRM);
END; 
/