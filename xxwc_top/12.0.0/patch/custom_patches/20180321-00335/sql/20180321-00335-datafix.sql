REM INV12-datafix.sql is renamed for clearer identification but is the same as subnoloc_moqloc.sql...
REM
REM  Copyright (c) 2001, Oracle Corporation, Redwood Shores, California, USA
REM                           All rights reserved.
REM
REM
REM FILE
REM subnoloc_moqloc.sql
REM
REM HISTORY
REM
REM Issue Details :
REM Onhand records with locator populated when sub inventory is not locator controlled.
REM Onhand is not usable because of this discrepancy.
REM
REM Script Pre-requisites:
REM (1) Organization is not WMS enabled.
REM (2) Consigned Inventory is not used
REM (3) Locator control is none at  Sub inventory or Organization level
REM (4) Customer does not care date received for the corrupt onhand rows.
REM
REM Future avoidance of the issue : 
REM Ct. needs to be apply patches 3778483 & 4113020 or equivalent patches on their patchset level as mentioned in RPL
REM before running the data fix script.If patch is not applied then issue will appear again.
REM
REM Data fix :
REM Please take backup of mtl_onhand_quantities_detail
REM                       Mtl_material_transactions
REM
REM Steps to enter a value for distribution_account_id :
REM Go to Misc Transactions form .Enter Item/sub/loc and Account on TRansaction lines block.
REM Now go to examine and check value of distribution_account_id and enter it at prompt.
REM
REM Please apply the script in test instance and validate the data.
REM
REM
REM ARGUMENTS
REM None
REM
REM HISTORY
REM
REM VENKATA PUTCHA         Created.
REM +========================================================================+

WHENEVER SQLERROR EXIT FAILURE ROLLBACK;

set serveroutput on size 999999
spool loc_nolocsub_fix.log; 
declare
   cursor C1 is select /*+ index(moqd MTL_ONHAND_QUANTITIES_N5) */
                moqd.organization_id org, moqd.subinventory_code sub, moqd.inventory_item_id item, moqd.locator_id loc,
                moqd.lot_number, moqd.revision,
                sum(primary_transaction_quantity) trx_qty 
                from mtl_onhand_quantities_detail moqd
                where locator_id is not null
                AND organization_id=970--TMS#20180321-00335
                AND PLANNING_ORGANIZATION_ID = ORGANIZATION_ID
                AND OWNING_ORGANIZATION_ID = ORGANIZATION_ID
                AND PLANNING_TP_TYPE = 2
                AND OWNING_TP_TYPE = 2
                AND NVL(SECONDARY_TRANSACTION_QUANTITY,0) = 0
                and exists (select 1 from mtl_secondary_inventories msi
                            where msi.secondary_inventory_name = moqd.subinventory_code
                            and   msi.organization_id = moqd.organization_id
                            and   msi.locator_type = 1)
                and exists (select 1 from mtl_parameters
                            where organization_id = moqd.organization_id
                            and  STOCK_LOCATOR_CONTROL_CODE in (1, 4)
                            AND  NOT (wms_enabled_flag = 'Y' OR process_enabled_flag = 'Y'))
                and exists (select 1 from mtl_system_items msit
                            where msit.organization_id = moqd.organization_id
                            and   msit.inventory_item_id = moqd.inventory_item_id
                            and   msit.serial_number_control_code in (1, 6) )
                group by moqd.organization_id, moqd.subinventory_code, moqd.inventory_item_id, moqd.locator_id,
                moqd.lot_number, moqd.revision;
l_temp_id    Number;
l_header_id  Number;
l_prdid Number;
l_description VARCHAR2(2000);
l_location_control_code  Number;
l_restrict_subinv_code Number;
l_restrict_locators_code Number; 
l_revision_qty_control_code Number;
l_primary_uom_code VARCHAR2(3);
l_shelf_life_code Number;
l_shelf_life_days Number;
l_lot_control_code Number;
l_serial_control_code Number;
l_allowed_units_lookup_code Number;
l_dist_account_id  NUMBER;
begin

  dbms_output.put_line('Inside loc_nolocsub_fix.sql');
  dbms_output.put_line('please enter valid Distribution account_id ');
  l_dist_account_id :=589213;--TMS#20180321-00335

  select mtl_material_transactions_s.nextval
  into l_header_id from dual;

  for c1_rec in C1 loop

      exit when C1%notfound;

      select 
             description 
            ,location_control_code
            ,restrict_subinventories_code
            ,restrict_locators_code
            ,revision_qty_control_code
            ,primary_uom_code
            ,shelf_life_code
            ,shelf_life_days
            ,nvl(lot_control_code,1)
            ,serial_number_control_code 
            ,allowed_units_lookup_code 
      into   l_description
            ,l_location_control_code
            ,l_restrict_subinv_code
            ,l_restrict_locators_code
            ,l_revision_qty_control_code
            ,l_primary_uom_code
            ,l_shelf_life_code
            ,l_shelf_life_days
            ,l_lot_control_code
            ,l_serial_control_code
            ,l_allowed_units_lookup_code
      from mtl_system_items
      where  inventory_item_id = c1_rec.item
      and    organization_id =  c1_rec.org;

      l_prdid := INV_TXN_MANAGER_PUB.get_open_period(c1_rec.org,sysdate,0);

      if (c1_rec.trx_qty > 0) then
        dbms_output.put_line('**Inserting sub-transfer record into MMTT with locator_id='||c1_rec.loc||' for item_id='||c1_rec.item||', org_id='||c1_rec.org||' and sub='||c1_rec.sub||', qty='||c1_rec.trx_qty);

        select mtl_material_transactions_s.nextval
        into l_temp_id from dual; 

        INSERT INTO mtl_material_transactions_temp
         (transaction_header_id
          ,transaction_temp_id
          ,transaction_mode
          ,lock_flag
          ,Process_flag
          ,last_update_date
          ,last_updated_by
          ,creation_date
          ,created_by
          ,last_update_login
          ,request_id
          ,program_application_id
          ,program_id
          ,program_update_date
          ,inventory_item_id
          ,revision
          ,organization_id
          ,subinventory_code
          ,locator_id
          ,transfer_subinventory
          ,transfer_to_location
          ,transfer_organization
          ,transaction_quantity
          ,primary_quantity
          ,transaction_uom
          ,transaction_type_id
          ,transaction_action_id
          ,transaction_source_type_id
          ,transaction_date
          ,acct_period_id
          ,distribution_account_id
          ,item_description
          ,item_location_control_code
          ,item_restrict_subinv_code
          ,item_restrict_locators_code
          ,item_revision_qty_control_code
          ,item_primary_uom_code
          ,item_shelf_life_code
          ,item_shelf_life_days
          ,item_lot_control_code
          ,item_serial_control_code
          ,allowed_units_lookup_code
          ,parent_transaction_temp_id
          ,lpn_id
          ,transfer_lpn_id
          ,cost_group_id)
     VALUES
          ( l_header_id,
            l_temp_id,
            3,
            'N',
            'Y',
            sysdate,
            -3782485,
            sysdate,
            -3782485,
            -3782485,
            NULL,
            NULL,
            NULL,
            NULL,
            c1_rec.item,
            c1_rec.revision, --revision
            c1_rec.org,
            c1_rec.sub,
            c1_rec.loc, --locator_id
            c1_rec.sub,
            NULL, --transfer_locator
            c1_rec.org,
            c1_rec.trx_qty, --l_mti_csr.transaction_quantity
            c1_rec.trx_qty, --l_mti_csr.primary_quantity
            l_primary_uom_code, --l_mti_csr.transaction_uom
            2, --l_mti_csr.transaction_type_id
            2, --l_mti_csr.transaction_action_id
            13, --l_mti_csr.transaction_source_type_id
            sysdate,
            l_prdid, --l_mti_csr.acct_period_id
            NULL, --l_mti_csr.distribution_account_id
            l_description,
            l_location_control_code,
            l_restrict_subinv_code,
            l_restrict_locators_code,
            l_revision_qty_control_code,
            l_primary_uom_code,
            l_shelf_life_code,
            l_shelf_life_days,
            l_lot_control_code,
            l_serial_control_code,
            l_allowed_units_lookup_code,
            NULL, --l_mti_csr.parent_id
            NULL, --l_mti_csr.lpn_id
            NULL, --l_mti_csr.transfer_lpn_id
            NULL);
 
            if c1_rec.lot_number is not null and l_lot_control_code =2 then
                INSERT INTO mtl_transaction_lots_temp
                (
                 transaction_temp_id
               , last_update_date
               , last_updated_by
               , creation_date
               , created_by
               , transaction_quantity
               , primary_quantity
               , lot_number
               , lot_expiration_date
               , serial_transaction_temp_id)
                  values (l_temp_id,sysdate,-1,sysdate,-1,1*c1_rec.trx_qty,1*c1_rec.trx_qty,
                  c1_rec.lot_number,null,null);
            end if;

       elsif (c1_rec.trx_qty < 0) then
        dbms_output.put_line('**Inserting misc. receipt record into MMTT with locator_id='||c1_rec.loc||', for item_id='||c1_rec.item||', org_id='||c1_rec.org||' and sub='||c1_rec.sub||', qty='||-1*c1_rec.trx_qty);

        select mtl_material_transactions_s.nextval
        into l_temp_id from dual;

        INSERT INTO mtl_material_transactions_temp
         (transaction_header_id
          ,transaction_temp_id
          ,transaction_mode
          ,lock_flag
          ,Process_flag
          ,last_update_date
          ,last_updated_by
          ,creation_date
          ,created_by
          ,last_update_login
          ,request_id
          ,program_application_id
          ,program_id
          ,program_update_date
          ,inventory_item_id
          ,revision
          ,organization_id
          ,subinventory_code
          ,locator_id
          ,transaction_quantity
          ,primary_quantity
          ,transaction_uom
          ,transaction_type_id
          ,transaction_action_id
          ,transaction_source_type_id
          ,transaction_date
          ,acct_period_id
          ,distribution_account_id
          ,item_description
          ,item_location_control_code
          ,item_restrict_subinv_code
          ,item_restrict_locators_code
          ,item_revision_qty_control_code
          ,item_primary_uom_code
          ,item_shelf_life_code
          ,item_shelf_life_days
          ,item_lot_control_code
          ,item_serial_control_code
          ,allowed_units_lookup_code
          ,parent_transaction_temp_id
          ,lpn_id
          ,transfer_lpn_id
          ,cost_group_id
	  ,transaction_cost)
     VALUES
          ( l_header_id,
            l_temp_id,
            3,
            'N',
            'Y',
            sysdate,
            -3782485,
            sysdate,
            -3782485,
            -3782485,
            NULL,
            NULL,
            NULL,
            NULL,
            c1_rec.item,
            c1_rec.revision, --revision
            c1_rec.org,
            c1_rec.sub,
            c1_rec.loc, --locator_id
            -1*c1_rec.trx_qty, --l_mti_csr.transaction_quantity
            -1*c1_rec.trx_qty, --l_mti_csr.primary_quantity
            l_primary_uom_code, --l_mti_csr.transaction_uom
            42, --l_mti_csr.transaction_type_id
            27, --l_mti_csr.transaction_action_id
            13, --l_mti_csr.transaction_source_type_id
            sysdate,
            l_prdid, --l_mti_csr.acct_period_id
            l_dist_account_id, --l_mti_csr.distribution_account_id
            l_description,
            l_location_control_code,
            l_restrict_subinv_code,
            l_restrict_locators_code,
            l_revision_qty_control_code,
            l_primary_uom_code,
            l_shelf_life_code,
            l_shelf_life_days,
            l_lot_control_code,
            l_serial_control_code,
            l_allowed_units_lookup_code,
            NULL, --l_mti_csr.parent_id
            NULL, --l_mti_csr.lpn_id
            NULL, --l_mti_csr.transfer_lpn_id
            NULL,
	    0);

            if c1_rec.lot_number is not null and l_lot_control_code =2 then
                INSERT INTO mtl_transaction_lots_temp
                (
                 transaction_temp_id
               , last_update_date
               , last_updated_by
               , creation_date
               , created_by
               , transaction_quantity
               , primary_quantity
               , lot_number
               , lot_expiration_date
               , serial_transaction_temp_id)
                  values (l_temp_id,sysdate,-1,sysdate,-1,-1*c1_rec.trx_qty,-1*c1_rec.trx_qty,
                  c1_rec.lot_number,null,null);
            end if;

        dbms_output.put_line('**Inserting misc. issue record into MMTT with NULL locator_id for item_id='||c1_rec.item||', org_id='||c1_rec.org||' and sub='||c1_rec.sub||', qty='||c1_rec.trx_qty);

        select mtl_material_transactions_s.nextval
        into l_temp_id from dual;

        INSERT INTO mtl_material_transactions_temp
         (transaction_header_id
          ,transaction_temp_id
          ,transaction_mode
          ,lock_flag
          ,Process_flag
          ,last_update_date
          ,last_updated_by
          ,creation_date
          ,created_by
          ,last_update_login
          ,request_id
          ,program_application_id
          ,program_id
          ,program_update_date
          ,inventory_item_id
          ,revision
          ,organization_id
          ,subinventory_code
          ,locator_id
          ,transaction_quantity
          ,primary_quantity
          ,transaction_uom
          ,transaction_type_id
          ,transaction_action_id
          ,transaction_source_type_id
          ,transaction_date
          ,acct_period_id
          ,distribution_account_id
          ,item_description
          ,item_location_control_code
          ,item_restrict_subinv_code
          ,item_restrict_locators_code
          ,item_revision_qty_control_code
          ,item_primary_uom_code
          ,item_shelf_life_code
          ,item_shelf_life_days
          ,item_lot_control_code
          ,item_serial_control_code
          ,allowed_units_lookup_code
          ,parent_transaction_temp_id
          ,lpn_id
          ,transfer_lpn_id
          ,cost_group_id
	  ,transaction_cost)
     VALUES
          ( l_header_id,
            l_temp_id,
            3,
            'N',
            'Y',
            sysdate,
            -3782485,
            sysdate,
            -3782485,
            -3782485,
            NULL,
            NULL,
            NULL,
            NULL,
            c1_rec.item,
            c1_rec.revision, --revision
            c1_rec.org,
            c1_rec.sub,
            NULL, --locator_id
            c1_rec.trx_qty, --l_mti_csr.transaction_quantity
            c1_rec.trx_qty, --l_mti_csr.primary_quantity
            l_primary_uom_code, --l_mti_csr.transaction_uom
            32, --l_mti_csr.transaction_type_id
            1, --l_mti_csr.transaction_action_id
            13, --l_mti_csr.transaction_source_type_id
            sysdate,
            l_prdid, --l_mti_csr.acct_period_id
            l_dist_account_id, --l_mti_csr.distribution_account_id
            l_description,
            l_location_control_code,
            l_restrict_subinv_code,
            l_restrict_locators_code,
            l_revision_qty_control_code,
            l_primary_uom_code,
            l_shelf_life_code,
            l_shelf_life_days,
            l_lot_control_code,
            l_serial_control_code,
            l_allowed_units_lookup_code,
            NULL, --l_mti_csr.parent_id
            NULL, --l_mti_csr.lpn_id
            NULL, --l_mti_csr.transfer_lpn_id
            NULL,
	    0);

            if c1_rec.lot_number is not null and l_lot_control_code =2 then
                INSERT INTO mtl_transaction_lots_temp
                (
                 transaction_temp_id
               , last_update_date
               , last_updated_by
               , creation_date
               , created_by
               , transaction_quantity
               , primary_quantity
               , lot_number
               , lot_expiration_date
               , serial_transaction_temp_id)
                  values (l_temp_id,sysdate,-1,sysdate,-1,1*c1_rec.trx_qty,1*c1_rec.trx_qty,
                  c1_rec.lot_number,null,null);
            end if;

       elsif (c1_rec.trx_qty = 0) then
        dbms_output.put_line('Deleting from MOQD with locator_id='||c1_rec.loc||', for item_id='||c1_rec.item||', org_id='||c1_rec.org||' and sub='||c1_rec.sub||', qty='||c1_rec.trx_qty);

	delete from mtl_onhand_quantities_detail moqd
	where inventory_item_id = c1_rec.item
	and   organization_id = c1_rec.org
	and   subinventory_code = c1_rec.sub
	and   locator_id is not null and locator_id = c1_rec.loc
        and exists (select 1 from mtl_secondary_inventories msi
                    where msi.secondary_inventory_name = moqd.subinventory_code
                    and   msi.organization_id = moqd.organization_id
                    and   msi.locator_type = 1)
        and exists (select 1 from mtl_parameters
                    where organization_id = moqd.organization_id
                    and  STOCK_LOCATOR_CONTROL_CODE in (1, 4)
                    and  wms_enabled_flag = 'N');

        dbms_output.put_line('Total number of rows deleted from MOQD = '||SQL%ROWCOUNT);

       end if; --c1_rec.trx_qty > 0

    end loop;
 commit;
end;
/

spool off;
