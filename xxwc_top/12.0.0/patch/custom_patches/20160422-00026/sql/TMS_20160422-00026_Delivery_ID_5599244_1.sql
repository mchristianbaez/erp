SET serveroutput ON;
SET verify OFF;

SPOOL om_datafix.lst;

DECLARE

 l_shipped_quantity NUMBER := 0;
 l_shipping_quantity NUMBER := 0;
 l_requested_quantity_uom VARCHAR2(50);
 l_src_requested_quantity_uom  VARCHAR2(50);
 l_conversion_rate NUMBER := 0;
 l_records_affected NUMBER := 0;
 l_file_name 	    VARCHAR2(500);
 l_item_key       VARCHAR2(50);
 l_result        VARCHAR2(50);
 l_INVENTORY_ITEM_ID   NUMBER;

 CURSOR LINES IS
SELECT oel.line_id
FROM oe_order_lines_all oel
WHERE oel.booked_flag = 'Y'
AND oel.open_flag = 'N'
AND oel.shipping_interfaced_flag = 'Y'
AND oel.shippable_flag = 'Y'
AND NVL(oel.shipped_quantity,0)=0
AND NVL(oel.shipping_quantity,0)=0
AND oel.flow_status_code = 'CLOSED'
AND oel.line_id= 68085140
AND Nvl(oel.invoice_interface_status_code,'N') IN ( 'YES' , 'NOT_ELIGIBLE')
AND EXISTS (SELECT 1 FROM wsh_delivery_details wdd
            WHERE oel.line_id = wdd.source_line_id
            AND wdd.released_status = 'C'
            AND wdd.source_code = 'OE'
            AND NVL(wdd.shipped_quantity, 0) > 0
	    )
AND NOT EXISTS(SELECT 1
                 FROM wsh_delivery_details wdd
               WHERE oel.line_id = wdd.source_line_id
               AND wdd.released_status <> 'C'
               AND wdd.source_code = 'OE'             
	    );



BEGIN


 Oe_debug_pub.debug_ON;
 Oe_debug_pub.initialize;
 Oe_debug_pub.setdebuglevel(5);

 l_file_name := Oe_debug_pub.set_debug_mode('FILE');

 Dbms_Output.put_line('Debug log is located at: ' || Oe_debug_pub.g_dir || '/' ||Oe_debug_pub.g_file);
 Oe_debug_pub.ADD('Selecting Lines for update...');

 FOR x in LINES
 LOOP
	Oe_debug_pub.ADD('Updating line ID :  ' || x.line_id);


	SELECT Sum(shipped_quantity)
	INTO   l_shipping_quantity
	FROM   wsh_delivery_details
	WHERE  source_line_id = x.line_id
	AND    source_code = 'OE'
	AND    released_status = 'C';

  Oe_debug_pub.ADD('Select Requeted Qty UOM');


	SELECT DISTINCT requested_quantity_uom  , src_requested_quantity_uom ,INVENTORY_ITEM_ID
	INTO   l_requested_quantity_uom   ,l_src_requested_quantity_uom  ,l_INVENTORY_ITEM_ID
	FROM   wsh_delivery_details
	WHERE  source_line_id = x.line_id
	AND    source_code = 'OE'
	AND    released_status = 'C';

    Oe_debug_pub.ADD('Select  UOM conversion rate');

     l_shipped_quantity:=OE_Order_Misc_Util.Convert_Uom
                          (p_item_id           => l_INVENTORY_ITEM_ID,
                          p_from_uom_code     => l_requested_quantity_uom,
                          p_to_uom_code       => l_src_requested_quantity_uom,
                          p_from_qty          => l_shipping_quantity);


  Oe_debug_pub.ADD('Updating shipping details in oe_order_lines_all...');

	UPDATE  oe_order_lines_all SET
                shipped_quantity     = l_shipped_quantity,
                shipping_quantity    = l_shipping_quantity,
                shipping_quantity_uom= l_requested_quantity_uom,
                fulfilled_quantity   = ordered_quantity,
                fulfilled_flag       = 'Y',
                actual_shipment_date = (SELECT MIN(wts.actual_departure_date)
				        FROM   wsh_delivery_details wdd, wsh_delivery_assignments wda, wsh_delivery_legs wdl, wsh_trip_stops wts
			                WHERE  wdd.delivery_detail_id = wda.delivery_detail_id
				        AND    wda.delivery_id=wdl.delivery_id
				        AND    wdl.pick_up_stop_id = wts.stop_id
				        AND    wdd.source_line_id = x.line_id
				        AND    wdd.source_code = 'OE'
				        AND    wdd.released_status = 'C'),
                last_updated_by      =  -1,
                last_update_date     =  SYSDATE
	WHERE line_id = x.line_id;

	  Oe_debug_pub.ADD('Updating wsh_delivery_details table ');

	l_records_affected := l_records_affected+1;

       UPDATE wsh_delivery_details
          SET oe_interfaced_flag = 'Y'
        WHERE source_line_id = x.line_id
          and released_status = 'C'
          and source_code = 'OE';

 END LOOP;
 COMMIT;

 IF l_records_affected = 0 THEN
 	Oe_debug_pub.ADD('No data to be cleaned up.');
 ELSE
 	Oe_debug_pub.ADD(l_records_affected || ' lines updated.');
 END IF;

EXCEPTION
 WHEN OTHERS THEN
       Oe_debug_pub.ADD('Error occurred: ' || SQLERRM);
       Dbms_output.put_line('Error occurred: ' || SQLERRM);
       ROLLBACK;
END;
/

spool OFF;
/