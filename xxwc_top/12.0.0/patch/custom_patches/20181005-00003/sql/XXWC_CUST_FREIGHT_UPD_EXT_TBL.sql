/*******************************************************************************************************
  -- Table Name XXWC_CUST_FREIGHT_UPD_EXT_TBL.sql  
  -- ***************************************************************************************************
  --
  -- PURPOSE: External table used to store Freight Terms details
  -- HISTORY
  -- ===================================================================================================
  -- ===================================================================================================
  -- VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- -------------------------------------------------------------
  -- 1.0     05-Oct-2018   Rakesh Patel    TMS# 20181005-00003 Freight Terms Exempt Data Script for AHH Customers
********************************************************************************************************/
CREATE TABLE XXWC.XXWC_CUST_FREIGHT_UPD_EXT_TBL
(
  ORACLE_ACCOUNT_NAME                  VARCHAR2(500 BYTE),
  ORACLE_ACCOUNT_NUMBER                NUMBER,
  ORACLE_SITE_NUMBER                   NUMBER,  
  CUST_ACCT_SITE_ID                    NUMBER,
  FREIGHT_TERMS                        VARCHAR2(240 BYTE)
)
ORGANIZATION EXTERNAL
  (  TYPE ORACLE_LOADER
     DEFAULT DIRECTORY XXWC_AHH_USER_CONV_DIR
     ACCESS PARAMETERS 
       ( RECORDS DELIMITED BY NEWLINE CHARACTERSET WE8MSWIN1252
      SKIP 1
    BADFILE 'XWC_CUST_FREIGHT_UPD_EXT_TBL.bad'
    DISCARDFILE 'XWC_CUST_FREIGHT_UPD_EXT_TBL.dsc'
    FIELDS TERMINATED BY '|'
    OPTIONALLY ENCLOSED BY '"' AND '"'
    REJECT ROWS WITH ALL NULL FIELDS
                     )
     LOCATION (XXWC_AHH_USER_CONV_DIR:'XXWC_CUST_FREIGHT_UPD.csv')
  )
REJECT LIMIT UNLIMITED;