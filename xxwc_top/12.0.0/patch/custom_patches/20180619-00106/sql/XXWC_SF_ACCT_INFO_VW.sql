CREATE OR REPLACE VIEW APPS.XXWC_SF_ACCT_INFO_VW AS
/*************************************************************************
 Copyright (c) 2012 HD Supply
 All rights reserved.
**************************************************************************
   $Header XXWC_SF_ACCT_INFO_VW$
  Module Name: XXWC_SF_ACCT_INFO_VW

  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     23-JUL-2018   Pahwa, Nancy                Initially Created
TMS# 20180619-00106 
**************************************************************************/
SELECT a.cust_account_id customer_id,nvl(su.primary_salesrep_id,-3) primary_salesrep_id,
          a.account_number
       || ' - '
       || p.party_name
       || ' '
       || l.address1
       || ','
       || l.city
       || ','
       || l.state
       || ','
       || l.country
       || ','
       || l.postal_code
       || ' - '
       || (select r.sr_name
          from xxwc_sr_salesreps r
         where r.salesrep_id = a.primary_salesrep_id)   account_details
  FROM hz_locations           l,
       hz_party_sites         s,
       hz_parties             p,
       hz_cust_accounts       a,
       hz_cust_acct_sites_all sa,
      hz_cust_site_uses_all  su
WHERE sa.cust_account_id   = a.cust_account_id
   AND sa.bill_to_flag      = 'P'
   AND p.party_id           = a.party_id
   AND s.party_site_id      = sa.party_site_id
   AND a.status             = 'A'
   AND sa.status            = 'A'
   AND l.location_id        = s.location_id
   AND sa.cust_acct_site_id = su.cust_acct_site_id
   and su.site_use_code = 'SHIP_TO'
   and nvl(a.CUSTOMER_TYPE,9999999) not in ('I')
   and sa.org_id = 162;