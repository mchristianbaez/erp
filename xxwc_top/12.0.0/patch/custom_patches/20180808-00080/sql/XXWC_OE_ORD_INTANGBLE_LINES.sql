 /********************************************************************************
  FILE NAME: XXWC_OE_ORD_INTANGBLE_LINES.sql

  PROGRAM TYPE: Backup table scripts

  PURPOSE: Intangible items update

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     08/10/2018    Pattabhi Avula  TMS#20180808-00080 Apply data fix to correct 
                                        intangibles having incorrect status
  ********************************************************************************/
create table xxwc.xxwc_oe_ord_intangble_lines
as SELECT * FROM oe_order_lines_all where 1>2
/
create table xxwc.xxwc_po_headers_intang_tbl
 as SELECT * FROM po_headers_all where 1>2
/
create table xxwc.xxwc_po_intangible_lines 
as select * from po_lines_all where 1>2
/
create table xxwc.xxwc_po_line_loc_intangibles
as select * from po_line_locations_all where 1>2
/