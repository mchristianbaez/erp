/***************************************************************************************************************
Table: APPS.XXWC_AP_EXPENSE_PO_WRT_OFF_TBL
Description: Creating this table to track expense PO closure process.

================================================================================================================
VERSION DATE               AUTHOR(S)       DESCRIPTION
------- -----------------  --------------- ---------------------------------------------------------------------
 1.0     17-Nov-2016        P.Vamshidhar    TMS#20181023-00001 Mass Closure of Uninvoiced Received Intangibles
****************************************************************************************************************/
SET SERVEROUT ON

ALTER SESSION SET CURRENT_SCHEMA=APPS
/
BEGIN MO_GLOBAL.SET_POLICY_CONTEXT('S','162'); END; 
/
CREATE TABLE XXWC.XXWC_AP_EXPENSE_PO_WRT_OFF_BKP AS
  (SELECT * FROM XXWC.XXWC_AP_EXPENSE_PO_WRT_OFF_TBL
  )
/
DROP TABLE XXWC.XXWC_AP_EXPENSE_PO_WRT_OFF_TBL
/ 
CREATE TABLE XXWC.XXWC_AP_EXPENSE_PO_WRT_OFF_TBL
AS
   SELECT pv.vendor_name,
          pv.enabled_flag,
          pv.end_date_Active,
          pv.hold_flag,
          pvs.vendor_site_code,
          pvs.inactive_date po_site_inactive_date,
          poh.org_id,
          poh.TERMS_ID,
          poh.vendor_id,
          (SELECT vendor_site_id
             FROM po_vendor_sites_all
            WHERE     vendor_id = poh.vendor_id
                  AND primary_pay_site_flag = 'Y'
                  AND org_id = 162)
             pay_vendor_site_id,
          (SELECT vendor_site_code
             FROM po_vendor_sites_all
            WHERE     vendor_id = poh.vendor_id
                  AND primary_pay_site_flag = 'Y'
                  AND org_id = 162)
             pay_vendor_site_code,
          (SELECT inactive_date
             FROM po_vendor_sites_all
            WHERE     vendor_id = poh.vendor_id
                  AND primary_pay_site_flag = 'Y'
                  AND org_id = 162)
             pay_site_inactive_date,
          poh.vendor_site_id po_vendor_site_id,
          poh.po_header_id,
          pol.po_line_id,
          poll.line_location_id,
          pod.po_distribution_id,
          pol.item_id INVENTORY_ITEM_ID,
          poh.currency_code currency_code,
          poll.SHIP_TO_LOCATION_CODE,
          poh.segment1 PO_NUMBER,
          pol.line_num,
          pol.item_number,
          (NVL (poll.quantity_received, 0) - NVL (poll.quantity_billed, 0))
             qty,
          (  (NVL (poll.quantity_received, 0) - NVL (poll.quantity_billed, 0))
           * pol.unit_price)
             amt,
          poll.quantity_received,
          poll.quantity_billed,
          poll.quantity,
          poll.price_override,
          pol.unit_price,
          poll.shipment_num,
          pod.distribution_num,
          (SELECT MAX (transaction_date) receipt_date
             FROM rcv_transactions
            WHERE     1 = 1
                  AND po_line_location_id = poll.line_location_id
                  AND transaction_type = 'RECEIVE')
             Receipt_date,
          0 INVOICE_ID,
          0 INVOICE_LINE_ID,
          ' ' SITE_ACTIVATED,
          ' ' PROCESS_FLAG,
          ' ' Error_Msg
     FROM po_distributions_all pod,
          po_line_locations_v poll,
          po_lines_v pol,
          po_headers_all poh,
          po_vendors pv,
          po_vendor_sites_all pvs
    WHERE     pol.po_header_id = poh.po_header_id
          AND poh.vendor_id = pv.vendor_id
          AND poh.vendor_site_id = pvs.vendor_site_id
          AND pv.vendor_id = pvs.vendor_id
          AND poll.po_line_id = pol.po_line_id
          AND pod.line_location_id = poll.line_location_id
          AND poh.type_lookup_code = 'STANDARD'
          AND poh.segment1 <> '289340'
          AND poh.org_id = 162
          AND poll.closed_date IS NULL
          AND NVL (poll.accrue_on_receipt_flag, 'N') = 'N'
          AND pod.destination_type_code = 'EXPENSE'
          AND NVL (pod.accrued_flag, 'N') = 'N'
          AND (   (  NVL (poll.quantity_received, 0)
                   - NVL (poll.quantity_billed, 0)) > 0
               OR (    NVL (poll.quantity_billed, 0) > 0
                   AND NOT EXISTS
                          (SELECT 1
                             FROM ap_invoice_lines_all
                            WHERE po_line_location_id = poll.line_location_id)))
/
alter table XXWC.XXWC_AP_EXPENSE_PO_WRT_OFF_TBL modify (SITE_ACTIVATED VARCHAR2(20), PROCESS_FLAG VARCHAR2(100), ERROR_MSG VARCHAR2(100))
/