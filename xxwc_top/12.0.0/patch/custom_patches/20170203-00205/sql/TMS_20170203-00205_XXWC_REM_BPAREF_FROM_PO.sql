/*************************************************************************
  $Header TMS_20170203-00205_XXWC_REM_BPAREF_FROM_PO.sql $
  Module Name: 20170203-00205  Data Fix script for 20170203-00205

  PURPOSE: Data fix script for 20170203-00205--No permanent fix in process (But patch available per oracle)

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        14-MAR-2017  Ashwin Sridhar        TMS#20170203-00205

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000;

DECLARE

ln_rec_counter NUMBER:=0;

--Cursor for getting POs linked to BPAs
CURSOR cu_bpa_po IS
SELECT ship_to_location
,      buyer_name
,      po_number
,      rev
,      line_num
,      shipment_num
,      po_supplier_name
,      supplier_site
,      approved_date
,      revised_date
,      closed_code
,      item
,      quantity_ordered
,      quantity_cancelled
,      quantity_received
,      quantity_due
,      quantity_billed
,      open_invoice_amount
,      open_for
,      line_unit
,      line_closed_code
,      po_type
,      po_status
,  (SELECT c.segment1
    || '-'
    || d.line_num
    FROM po_lines_all   b
    ,    po_headers_all c
    ,    po_lines_all   d
  WHERE  b.po_line_id    = a.line_id
  AND    b.from_header_id IS NOT NULL
  AND    b.from_header_id  = c.po_header_id
  AND    b.from_line_id    = d.po_line_id
  AND    c.po_header_id    = d.po_header_id
  AND    b.from_line_id   IS NOT NULL
  ) "BPA-LINE_REFERENCE"
,  po_header_id
,  line_id
,  line_location_id
,  distribution_id
FROM xxeis.eis_xxwc_po_open_orders_v a
WHERE item IN ('4344BAR40', '43522060'
             , '43532040', '43532060'
             , '43510BAR60', '43533060'
             , '43534060', '43542040'
             , '43542060', '43511BAR60'
             , '43543060', '43544060'
             , '43546060', '43552040'
             , '43552060', '43531040'
             , '43531060', '43532020'
             , '43553060', '43554060'
             , '43556060', '43562060'
             , '43564060', '43566060'
             , '43572060', '43573060'
             , '43574060', '43576060'
             , '43582060', '4353BAR60'
             , '43583060', '43584060'
             , '43586060', '43541040'
             , '43541060', '43592060'
             , '43593060', '43594060'
             , '43596060', '435102060'
             , '435104060', '435106060'
             , '435112060', '43514BAR60'
             , '4352BAR60', '4354BAR40'
             , '4354BAR60', '43551060'
             , '4353BAR40', '4355BAR60'
             , '43561060', '4356BAR60'
             , '4355BAR40', '4357BAR60'
             , '4358BAR60', '4359BAR60')
AND EXISTS
  (SELECT 1
   FROM po_lines_all
   WHERE po_line_id = a.line_id
   AND FROM_HEADER_ID IS NOT NULL
   AND from_line_id   IS NOT NULL
   );
   
BEGIN

  --Setting the OU...
  BEGIN 
  
    mo_global.set_policy_context('S','162'); 
  
  END;

  DBMS_OUTPUT.put_line ('Starting the LOOP...');

  FOR rec_bpa_po IN cu_bpa_po LOOP
  
    ln_rec_counter:=ln_rec_counter+1;
  
    DBMS_OUTPUT.put_line ('Inside the LOOP...');
    DBMS_OUTPUT.put_line ('PO Number...'||rec_bpa_po.po_number);
    DBMS_OUTPUT.put_line ('PO Line ID...'||rec_bpa_po.line_id);
    DBMS_OUTPUT.put_line ('Line Number...'||rec_bpa_po.line_num);
  
    --Updating the PO Lines...
    UPDATE po_lines_all
    SET    from_header_id = NULL
    ,      from_line_id   = NULL
    WHERE  po_line_id     = rec_bpa_po.line_id;
  
  END LOOP;
  
  DBMS_OUTPUT.put_line ('Records updated-' ||ln_rec_counter);
 
  COMMIT;
   
EXCEPTION
WHEN OTHERS THEN

  DBMS_OUTPUT.put_line ('Unable to update ' || SQLERRM);
  
  ROLLBACK;

END;
/