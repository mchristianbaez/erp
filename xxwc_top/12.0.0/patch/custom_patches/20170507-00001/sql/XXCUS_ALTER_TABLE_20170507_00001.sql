/*
 TMS:  20170507-00001
 Date: 05/07/2017
*/
set serveroutput on size 1000000;
declare
 --
 l_sql_string varchar2(2000) :=Null; 
 --
     procedure print_log(p_message in varchar2) is
     begin
      if apps.fnd_global.conc_request_id >0 then
       apps.fnd_file.put_line(fnd_file.log, p_message);
      else
       dbms_output.put_line(p_message);
      end if;
     end;
 --
begin
     --
     print_log('');     
     print_log(' Begin alter field size for SAE detail and history tables');
     print_log('');
     --      
     begin
          --
          l_sql_string :='ALTER TABLE XXCUS.XXCUS_SRC_CONCUR_SAE_N MODIFY (CC_TRANS_ACQUIRER_REF_NO VARCHAR2(50), CC_TRANS_PROCESSOR_REF_NO VARCHAR2(64))';
          --
          execute immediate l_sql_string;
          --
          print_log('Altered XXCUS SAE detail table XXCUS_SRC_CONCUR_SAE_N');
          --
          l_sql_string :='ALTER TABLE XXCUS.XXCUS_SRC_CONCUR_SAE_N_HIST MODIFY (CC_TRANS_ACQUIRER_REF_NO VARCHAR2(50), CC_TRANS_PROCESSOR_REF_NO VARCHAR2(64))';
          --
          execute immediate l_sql_string;
          -- 
          print_log('Altered XXCUS SAE detail history table XXCUS_SRC_CONCUR_SAE_N_HIST');
          --          
     exception
      when others then
       print_log('@ when-others-1, error ='||sqlerrm);       
     end;
    --  
     print_log('');     
     print_log(' End alter field size for SAE detail and history tables');
     print_log('');
     --    
exception
 when others then
  print_log('Outer block, message ='||sqlerrm);
end;
/