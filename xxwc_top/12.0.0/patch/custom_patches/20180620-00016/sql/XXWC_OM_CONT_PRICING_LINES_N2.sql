/*************************************************************************
 Copyright (c) 2012 HD Supply
 All rights reserved.
**************************************************************************
$Header xxwc.XXWC_OM_CONT_PRICING_LINES_N2 $
 Module Name: xxwc.XXWC_OM_CONT_PRICING_LINES_N2

 PURPOSE:

REVISIONS:
Ver        Date        Author              Description
---------  ----------  ---------------     -------------------------
1.0        06/20/2018  Pattabhi Avula      TMS#20180620-00016 - XXWC CSP Extract Process to include only CSP and VQN
**************************************************************************/
CREATE INDEX XXWC.XXWC_OM_CONT_PRICING_LINES_N2 ON XXWC.XXWC_OM_CONTRACT_PRICING_LINES(AGREEMENT_TYPE);
/