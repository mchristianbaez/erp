/******************************************************************************
  $Header TMS_20170416-00005_XXWC_DEL_OE_MSGS.sql $
  Module Name: 20170416-00005  Data Fix script for 20170416-00005

  PURPOSE: Data fix script for 20170416-00005 This script will delete the old
           messages from oe workflow --No permanent fix in process 
		  (But patch available per oracle)

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    -------------------------------
  1.0        17-MAY-2017  Pattabhi Avula        TMS#20170416-00005

*******************************************************************************/
set serveroutput on size 1000000;

CREATE INDEX ONT.XXWC_OE_PROCESSING_MSGS_N5 ON ONT.oe_processing_msgs(TRUNC(creation_date));
/
CREATE TABLE XXWC.xxwc_oe_process_msgs_t_bkup14 AS (SELECT opm.rowid row_id,opm.* 
                                                  FROM ONT.oe_processing_msgs_tl opm
                                                  WHERE creation_date < TO_DATE('01-JAN-2015'));
/                                                 
CREATE TABLE XXWC.xxwc_oe_process_msgs_bkup14 AS (SELECT opm.rowid row_id,opm.* 
                                                FROM ONT.oe_processing_msgs opm
                                                WHERE creation_date < TO_DATE('01-JAN-2015'));
/
DECLARE
   CURSOR cur_opm
   IS
      SELECT row_id FROM XXWC.xxwc_oe_process_msgs_bkup14;

   CURSOR cur_opmt
   IS
      SELECT row_id FROM XXWC.xxwc_oe_process_msgs_t_bkup14;

   TYPE rec_Table1 IS TABLE OF cur_opm%ROWTYPE
      INDEX BY PLS_INTEGER;

   TYPE rec_Table2 IS TABLE OF cur_opmt%ROWTYPE
      INDEX BY PLS_INTEGER;

   working_rec_table_1   rec_Table1;
   working_rec_table_2   rec_Table2;
   
   ln_counter1 NUMBER:=0;
   ln_counter2 NUMBER:=0;

BEGIN
      DBMS_OUTPUT.put_line ('Process oe_processing_msgs begin '||TO_CHAR(SYSDATE,'DD-MON-YYYY HH24:MI:SS'));

   OPEN cur_opm;

   LOOP
      FETCH cur_opm BULK COLLECT INTO working_rec_table_1 LIMIT 50000;
	  
	  DBMS_OUTPUT.put_line ('working_rec_table_1 count '||working_rec_table_1.COUNT);
	  ln_counter1:=ln_counter1+working_rec_table_1.COUNT;

      FORALL i IN working_rec_table_1.FIRST .. working_rec_table_1.LAST
         DELETE /*+ PARALLEL */  FROM ONT.oe_processing_msgs
          WHERE ROWID = working_rec_table_1 (i).row_id;

    COMMIT;
      working_rec_table_1.delete;
	  EXIT WHEN cur_opm%NOTFOUND;
   END LOOP;


   CLOSE CUR_OPM;
   
      DBMS_OUTPUT.put_line ('Total Records Deleted in table oe_processing_msgs -'||ln_counter1);
      DBMS_OUTPUT.put_line ('Process oe_processing_msgs end '||TO_CHAR(SYSDATE,'DD-MON-YYYY HH24:MI:SS'));   
      DBMS_OUTPUT.put_line ('Process oe_processing_msgs_tl begin'||TO_CHAR(SYSDATE,'DD-MON-YYYY HH24:MI:SS'));   	  
   
   OPEN CUR_OPMT;

   LOOP
      FETCH cur_opmt BULK COLLECT INTO working_rec_table_2 LIMIT 50000;
	  
	        DBMS_OUTPUT.put_line ('working_rec_table_2 count '||working_rec_table_2.COUNT);

           ln_counter2:=ln_counter2+working_rec_table_2.COUNT;

      FORALL i IN working_rec_table_2.FIRST .. working_rec_table_2.LAST
         DELETE /*+ PARALLEL */  FROM ont.oe_processing_msgs_tl
               WHERE ROWID = working_rec_table_2 (i).row_id;

      
    COMMIT;
      WORKING_REC_TABLE_2.DELETE;
	  
	  EXIT WHEN cur_opmt%NOTFOUND;
   END LOOP;

   CLOSE cur_opmt;
    DBMS_OUTPUT.put_line ('Total Records Deleted in table oe_processing_msgs_tl -'||ln_counter2);

      DBMS_OUTPUT.put_line ('Process oe_processing_msgs_tl end'||TO_CHAR(SYSDATE,'DD-MON-YYYY HH24:MI:SS'));   	  
   

EXCEPTION
   WHEN OTHERS
   THEN
      IF CUR_OPM%ISOPEN
      THEN
         CLOSE CUR_OPM;
      END IF;

      IF cur_opmt%ISOPEN
      THEN
         CLOSE cur_opmt;
      END IF;

      ROLLBACK;
      DBMS_OUTPUT.put_line ('Exception '||TO_CHAR(SYSDATE,'DD-MON-YYYY HH24:MI:SS'));   	  	  
      DBMS_OUTPUT.put_line ('Unable to delete the records ' || SQLERRM);
END;
/
DROP INDEX ONT.XXWC_OE_PROCESSING_MSGS_N5;
/