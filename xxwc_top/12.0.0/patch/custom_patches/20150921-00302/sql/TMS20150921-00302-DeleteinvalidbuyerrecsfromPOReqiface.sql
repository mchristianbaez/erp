/*
 TMS: 20150921-00302 
 Date: 09/21/2015
 */
SET SERVEROUTPUT ON SIZE 1000000

DECLARE
   v_count   NUMBER;
BEGIN
   DBMS_OUTPUT.put_line ('TMS:20150921-00302   , Script 1 -Before delete');

   BEGIN
      SELECT COUNT (1)
        INTO v_count
        FROM po_requisitions_interface_all pri
       WHERE     org_id = 162
             AND EXISTS
                    (SELECT *
                       FROM po_interface_errors
                      WHERE     column_name = 'SUGGESTED_BUYER_ID'
                            AND interface_type = 'REQIMPORT'
                            AND interface_transaction_id = pri.transaction_id);

      DELETE FROM po_requisitions_interface_all pri
            WHERE     org_id = 162
                  AND EXISTS
                         (SELECT *
                            FROM po_interface_errors
                           WHERE     column_name = 'SUGGESTED_BUYER_ID'
                                 AND interface_type = 'REQIMPORT'
                                 AND interface_transaction_id =
                                        pri.transaction_id);
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         DBMS_OUTPUT.PUT_LINE (
            'Error in deleting records from po req interface-' || SQLERRM);
   END;

   COMMIT;

   DBMS_OUTPUT.put_line (
         'TMS: 20150921-00302, Script 1 -After delete, rows deleted: '
      || v_count);

   DBMS_OUTPUT.put_line ('TMS:20150921-00302  , Script 2 -Before delete');

   BEGIN
      v_count := 0;

      SELECT COUNT (1)
        INTO v_count
        FROM po_interface_errors
       WHERE     column_name = 'SUGGESTED_BUYER_ID'
             AND interface_type = 'REQIMPORT';

      DELETE FROM po_interface_errors
            WHERE     column_name = 'SUGGESTED_BUYER_ID'
                  AND interface_type = 'REQIMPORT';
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         DBMS_OUTPUT.PUT_LINE (
            'Error in deleting po interface error records-' || SQLERRM);
   END;

   COMMIT;

   DBMS_OUTPUT.put_line (
         'TMS: 20140715-00036, Script 2 -After delete, rows deleted: '
      || v_count);
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.PUT_LINE ('Error is' || SQLERRM);
END;
/