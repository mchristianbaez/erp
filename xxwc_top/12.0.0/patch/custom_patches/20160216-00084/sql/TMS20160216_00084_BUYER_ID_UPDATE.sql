/*******************************************************************************************************************************************************
  $Header TMS20160216_00084_BUYER_ID_UPDATE.sql $

  PURPOSE Data fix to move Hazmat attributes from Inventory to PDH. 

  REVISIONS
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        26-FEB-2016  Niraj Ranjan          TMS#20160216-00084 - Initially Created 

*******************************************************************************************************************************************************/
SET serveroutput ON;

prompt Taking backup for first update;
CREATE TABLE xxwc.xxwc_mtl_system_items_222
   AS (SELECT inventory_item_id, organization_id, buyer_id
        FROM mtl_system_items_b msib
        WHERE     1 = 1
          AND msib.organization_id = 222
          AND msib.buyer_id IS NOT NULL
	  );
	  	  
prompt Taking backup for Second update;
CREATE TABLE xxwc.xxmtl_system_items_not_222
   AS (SELECT inventory_item_id, organization_id, buyer_id
        FROM mtl_system_items_b msib
        WHERE 1 = 1
        AND organization_id <> 222
        AND organization_id IN (SELECT organization_id
                                 FROM org_organization_definitions
                                WHERE operating_unit = 162
							   )
        AND EXISTS(SELECT 1
                   FROM po_agents
                   WHERE  NVL (end_date_active, SYSDATE + 1) < SYSDATE
                   AND agent_id = msib.buyer_id
				  )
	  );
	  	  
BEGIN
   DBMS_OUTPUT.put_line ('Before first update');

   UPDATE mtl_system_items_b msib
      SET msib.buyer_id = NULL
    WHERE 1 = 1 AND msib.organization_id = 222 AND msib.buyer_id IS NOT NULL;

   DBMS_OUTPUT.put_line (
      'Records updated for organization_id 222: ' || SQL%ROWCOUNT);
   COMMIT;
   DBMS_OUTPUT.put_line ('After first update');

   DBMS_OUTPUT.put_line ('Before Second update');

   UPDATE mtl_system_items_b msib
      SET msib.buyer_id = NULL
    WHERE     1 = 1
          AND organization_id <> 222
          AND organization_id IN (SELECT organization_id
                                    FROM org_organization_definitions
                                   WHERE operating_unit = 162)
          AND EXISTS
                 (SELECT 1
                    FROM po_agents
                   WHERE     NVL (end_date_active, SYSDATE + 1) < SYSDATE
                         AND agent_id = msib.buyer_id);

   DBMS_OUTPUT.put_line (
      'Records updated for organization_id <> 222: ' || SQL%ROWCOUNT);
   COMMIT;
   DBMS_OUTPUT.put_line ('After Second update');
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Unable to update ' || SQLERRM);
END;
/