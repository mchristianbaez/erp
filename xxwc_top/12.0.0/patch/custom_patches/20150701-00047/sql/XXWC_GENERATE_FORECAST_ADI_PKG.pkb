CREATE OR REPLACE PACKAGE BODY APPS.XXWC_GENERATE_FORECAST_ADI_PKG
AS
   /*************************************************************************
   *   $Header xxwc_generate_forecast_adi_pkg $
   *   Module Name: xxwc_generate_forecast_adi_pkg
   *
   *   PURPOSE:   Package used to submit XXWC Generate Demand Forecast via Web ADI Upload TMS Ticket # 20130201-01386
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        08/12/2013  Lee Spitzer                Initial Version
   *   1.1        08/04/2015  Manjula Chellappan         TMS# 20150701-00047 Generate Demand Forecast Performance Tuning and Redesign
   * ***************************************************************************/

   PROCEDURE submit_ccp (p_org_code               IN VARCHAR2,
                         p_forecast_designator    IN VARCHAR2,
                         p_branch_dc_mode         IN VARCHAR2,
                         p_item_range             IN VARCHAR2,
                         p_item_category          IN VARCHAR2,
                         p_item_number            IN VARCHAR2,
                         p_bucket_type            IN NUMBER,
                         p_start_date             IN DATE,
                         p_num_of_fc_periods      IN NUMBER,
                         p_num_of_prev_periods    IN NUMBER,
                         p_constant_seasonality   IN NUMBER,
                         p_seasonality1           IN NUMBER,
                         p_seasonality2           IN NUMBER,
                         p_seasonality3           IN NUMBER,
                         p_seasonality4           IN NUMBER,
                         p_seasonality5           IN NUMBER,
                         p_seasonality6           IN NUMBER,
                         p_seasonality7           IN NUMBER,
                         p_seasonality8           IN NUMBER,
                         p_seasonality9           IN NUMBER,
                         p_seasonality10          IN NUMBER,
                         p_seasonality11          IN NUMBER,
                         p_seasonality12          IN NUMBER)
   IS
      l_error_msg                   VARCHAR2 (240);
      l_req_id                      NUMBER;
      l_date                        VARCHAR2 (30);
      l_organization_id             NUMBER;

      --Added for Rev 1.1 Begin <<
      l_calendar_code               VARCHAR2 (10);
      l_calendar_exception_set_id   NUMBER;
      l_forecast_start_date         DATE;
      l_history_start_date          DATE;
      l_forecast_periods            NUMBER;
      l_history_periods             NUMBER;
      l_exception                   EXCEPTION;
      l_error_flag                  NUMBER := 0;
      l_sec                         VARCHAR2 (100);
      l_distribution_list           fnd_user.email_address%TYPE
                                       := 'HDSOracleDevelopers@hdsupply.com';
      l_inventory_item_id           NUMBER;
      l_category_id                 NUMBER;
      l_retcode                     NUMBER;
   --Added for Rev 1.1 End >>

   BEGIN
      -- Added for Rev 1.1 Begin <<

      l_sec := 'Get Organization Details';

      BEGIN
         SELECT organization_id, calendar_code, calendar_exception_set_id
           INTO l_organization_id,
                l_calendar_code,
                l_calendar_exception_set_id
           FROM mtl_parameters
          WHERE organization_code = p_org_code;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_error_flag := 1;
            l_error_msg :=
               SUBSTR (
                     'Failed to get Org Details for '
                  || p_org_code
                  || ' '
                  || SQLERRM,
                  1,
                  240);
      END;

      l_sec := 'Get History Start Date ';

      l_history_start_date :=
         XXWC_MTL_DEMAND_FORECAST_PKG.Get_History_Start_Date (
            l_calendar_code,
            p_start_date,
            p_bucket_type,
            p_num_of_prev_periods);

      IF l_history_start_date IS NULL
      THEN
         l_error_flag := 1;
         l_error_msg :=
            SUBSTR (
                  'Failed to get history_start_date for '
               || p_org_code
               || ' '
               || SQLERRM,
               1,
               240);
      END IF;

      l_sec := 'Get Forecast Start Date ';

      l_forecast_start_date :=
         MRP_CALENDAR.Prev_Work_Day (l_organization_id, 1, p_start_date);

      IF l_forecast_start_date IS NULL
      THEN
         l_error_flag := 1;
         l_error_msg :=
            SUBSTR (
                  'Failed to get forecast_start_date for '
               || p_org_code
               || ' '
               || SQLERRM,
               1,
               240);
      END IF;

      l_sec := 'Correct the No. of forecast Periods ';

      IF p_num_of_fc_periods > 12
      THEN
         l_forecast_periods := 12;
      ELSIF p_num_of_fc_periods < 1
      THEN
         l_forecast_periods := 1;
      ELSE
         l_forecast_periods := p_num_of_fc_periods;
      END IF;

      l_sec := 'Correct the No. of history Periods ';

      IF p_num_of_prev_periods < 1
      THEN
         l_history_periods := 1;
      ELSE
         l_history_periods := p_num_of_prev_periods;
      END IF;

      l_sec := 'Get Inventory Item Id';



      IF p_item_number IS NOT NULL
      THEN
         BEGIN
            SELECT inventory_item_id
              INTO l_inventory_item_id
              FROM MTL_SYSTEM_ITEMS_B
             WHERE     segment1 = p_item_number
                   AND organization_id = l_organization_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_error_flag := 1;
               l_error_msg :=
                  SUBSTR (
                        'Failed to get Inventory Item Id for Item '
                     || p_item_number
                     || ' in Branch '
                     || p_org_code
                     || ' '
                     || SQLERRM,
                     1,
                     240);
         END;
      END IF;

      l_sec := 'Get Category Id ';

      IF p_item_category IS NOT NULL
      THEN
         BEGIN
            SELECT mc.category_id
              INTO l_category_id
              FROM mtl_categories_kfv mc, mtl_category_sets mcs
             WHERE     mcs.structure_id = mc.structure_id
                   AND mcs.category_set_name = 'Inventory Category'
                   AND mc.concatenated_segments = p_item_category;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_error_flag := 1;
               l_error_msg :=
                  SUBSTR (
                        'Failed to get Category Id for category '
                     || p_item_category
                     || ' in Branch '
                     || p_org_code
                     || ' '
                     || SQLERRM,
                     1,
                     240);
         END;
      END IF;



      IF l_error_flag = 0
      THEN
         l_sec := 'Delete From XXWC.XXWC_MTL_FORECAST_PARAM_TBL ';

         DELETE FROM XXWC.XXWC_MTL_FORECAST_PARAM_TBL
               WHERE     organization_id = l_organization_id
                     AND forecast_designator = p_forecast_designator;

         l_sec := 'Insert into XXWC.XXWC_MTL_FORECAST_PARAM_TBL ';

         INSERT
           INTO XXWC.XXWC_MTL_FORECAST_PARAM_TBL (ORGANIZATION_ID,
                                                  ORGANIZATION_CODE,
                                                  FORECAST_DESIGNATOR,
                                                  CALENDAR_CODE,
                                                  CALENDAR_EXCEPTION_SET_ID,
                                                  BRANCH_DC_MODE,
                                                  ITEM_RANGE,
                                                  CATEGORY_ID,
                                                  ITEM_CATEGORY,
                                                  INVENTORY_ITEM_ID,
                                                  ITEM_NUMBER,
                                                  BUCKET_TYPE,
                                                  START_DATE,
                                                  FORECAST_START_DATE,
                                                  HISTORY_START_DATE,
                                                  NUM_OF_FORECAST_PERIODS,
                                                  NUM_OF_PREVIOUS_PERIODS,
                                                  CONSTANT_SEASONALITY,
                                                  SEASONALITY_1,
                                                  SEASONALITY_2,
                                                  SEASONALITY_3,
                                                  SEASONALITY_4,
                                                  SEASONALITY_5,
                                                  SEASONALITY_6,
                                                  SEASONALITY_7,
                                                  SEASONALITY_8,
                                                  SEASONALITY_9,
                                                  SEASONALITY_10,
                                                  SEASONALITY_11,
                                                  SEASONALITY_12,
                                                  INSERT_DATE)
         VALUES (l_organization_id,
                 p_org_code,
                 p_forecast_designator,
                 l_calendar_code,
                 l_calendar_exception_set_id,
                 p_branch_dc_mode,
                 p_item_range,
                 l_category_id,
                 p_item_category,
                 l_inventory_item_id,
                 p_item_number,
                 p_bucket_type,
                 p_start_date,
                 l_forecast_start_date,
                 l_history_start_date,
                 l_forecast_periods,
                 l_history_periods,
                 p_constant_seasonality,
                 p_seasonality1,
                 p_seasonality2,
                 p_seasonality3,
                 p_seasonality4,
                 p_seasonality5,
                 p_seasonality6,
                 p_seasonality7,
                 p_seasonality8,
                 p_seasonality9,
                 p_seasonality10,
                 p_seasonality11,
                 p_seasonality12,
                 SYSDATE);

         COMMIT;
      ELSE
         fnd_message.set_name ('XXWC', 'XXWC_ADI_ERROR_MSG');
         fnd_message.set_token ('ERROR_MESSAGE', l_error_msg);
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         l_error_flag := 1;
         l_error_msg :=
            SUBSTR (
                  'Failed to Load the data for Branch '
               || p_org_code
               || ' '
               || SQLERRM,
               1,
               240);

         fnd_message.set_name ('XXWC', 'XXWC_ADI_ERROR_MSG');
         fnd_message.set_token ('ERROR_MESSAGE', l_error_msg);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_GENERATE_FORECAST_ADI_PKG.submit_ccp',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          DBMS_UTILITY.format_error_stack ()
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => l_error_msg,
            p_distribution_list   => l_distribution_list,
            p_module              => 'INV');
         COMMIT;
   -- Added for Rev 1.1 End >>


   -- Commented for Rev 1.1 Begin <<
   /*
         l_date := to_char(p_start_date,'YYYY/MM/DD HH24:MI:SS');

           BEGIN
               select organization_id
               into   l_organization_id
               from   mtl_parameters
               where   organization_code = p_org_code;
           EXCEPTION
                when others then
                       l_error_msg := substr('Invalid org code ' || p_org_code || ' ' || sqlerrm,1,240);
                       RAISE_APPLICATION_ERROR(-20001,l_error_msg);

           END;


                 l_req_id := NULL;
                 l_req_id :=
                    fnd_request.submit_request (application       => 'XXWC'
                                                ,program          => 'XXWC_GENERATE_FORECAST'
                                                ,description      => NULL
                                                ,start_time       => SYSDATE
                                                ,sub_request      => FALSE
                                                ,argument1        => l_organization_id
                                                ,argument2        => p_forecast_designator
                                                ,argument3        => p_branch_dc_mode
                                                ,argument4        => p_item_range
                                                ,argument5        => p_item_category
                                                ,argument6        => p_item_number
                                                ,argument7        => p_bucket_type
                                                ,argument8        => l_date --p_start_date
                                                ,argument9        => p_num_of_fc_periods
                                                ,argument10       => p_num_of_prev_periods
                                                ,argument11       => p_constant_seasonality
                                                ,argument12       => p_seasonality1
                                                ,argument13       => p_seasonality2
                                                ,argument14       => p_seasonality3
                                                ,argument15       => p_seasonality4
                                                ,argument16       => p_seasonality5
                                                ,argument17       => p_seasonality6
                                                ,argument18       => p_seasonality7
                                                ,argument19       => p_seasonality8
                                                ,argument20       => p_seasonality9
                                                ,argument21       => p_seasonality10
                                                ,argument22       => p_seasonality11
                                                ,argument23       => p_seasonality12
                                               );

                 -- commit;
                 IF NVL (l_req_id, 0) > 0 THEN

                   NULL;

                 ELSE

                   l_error_msg := substr('Could not submit request for forecast ' || p_forecast_designator,1,240);
                   RAISE_APPLICATION_ERROR(-20001,l_error_msg);

                 END IF;


       EXCEPTION
          when others then
            l_error_msg := substr(sqlerrm,1,240);
            RAISE_APPLICATION_ERROR(-20001,l_error_msg);
   */

   -- Commented for Rev 1.1 End >>

   END submit_ccp;
END XXWC_GENERATE_FORECAST_ADI_PKG;
/