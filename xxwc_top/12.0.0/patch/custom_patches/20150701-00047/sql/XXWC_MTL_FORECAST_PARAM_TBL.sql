/*************************************************************************
  $Header XXWC_MTL_FORECAST_PARAM_TBL.sql $
  Module Name: XXWC_MTL_FORECAST_PARAM_TBL

  PURPOSE: Table to maintain Generate Demand Forecast Program Parameters

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    ----------------
  1.0        31-Jul-2015  Manjula Chellappan    Initial Version TMS # 20150701-00047

**************************************************************************/ 
 CREATE TABLE XXWC.XXWC_MTL_FORECAST_PARAM_TBL
  (
    ORGANIZATION_ID         	NUMBER NOT NULL ,
    ORGANIZATION_CODE       	VARCHAR2(3),
    FORECAST_DESIGNATOR     	VARCHAR2(10) NOT NULL ,    
    CALENDAR_CODE           	VARCHAR2(10),
    CALENDAR_EXCEPTION_SET_ID   NUMBER, 
    BRANCH_DC_MODE          	VARCHAR2(20),
    ITEM_RANGE              	VARCHAR2(20),
    CATEGORY_ID             	NUMBER,
    ITEM_CATEGORY           	CHAR(100),
    INVENTORY_ITEM_ID       	NUMBER,
    ITEM_NUMBER             	VARCHAR2(40),
    BUCKET_TYPE             	NUMBER,
    START_DATE              	DATE,
    FORECAST_START_DATE			DATE,
    HISTORY_START_DATE 			DATE,
    NUM_OF_FORECAST_PERIODS 	NUMBER,
    NUM_OF_PREVIOUS_PERIODS 	NUMBER,
    CONSTANT_SEASONALITY    	NUMBER,
    SEASONALITY_1           	NUMBER,
    SEASONALITY_2           	NUMBER,
    SEASONALITY_3           	NUMBER,
    SEASONALITY_4           	NUMBER,
    SEASONALITY_5           	NUMBER,
    SEASONALITY_6           	NUMBER,
    SEASONALITY_7           	NUMBER,
    SEASONALITY_8           	NUMBER,
    SEASONALITY_9           	NUMBER,
    SEASONALITY_10          	NUMBER,
    SEASONALITY_11          	NUMBER,
    SEASONALITY_12          	NUMBER,
	INSERT_DATE					DATE
  );
  
	CREATE UNIQUE INDEX xxwc.XXWC_MTL_FORECAST_PARAM_TBL_u1 ON XXWC.XXWC_MTL_FORECAST_PARAM_TBL(organization_id,forecast_designator) ;
	
    CREATE INDEX xxwc.XXWC_MTL_FORECAST_PARAM_TBL_n1 ON XXWC.XXWC_MTL_FORECAST_PARAM_TBL(organization_id,forecast_designator,ITEM_RANGE) ;
   
    CREATE INDEX xxwc.XXWC_MTL_FORECAST_PARAM_TBL_n2 ON XXWC.XXWC_MTL_FORECAST_PARAM_TBL(organization_id,forecast_designator,history_start_date, forecast_start_date) ;
    
    CREATE INDEX xxwc.XXWC_MTL_FORECAST_PARAM_TBL_n3 ON XXWC.XXWC_MTL_FORECAST_PARAM_TBL(organization_id, forecast_designator, inventory_item_id, category_id, item_range) ; 
    
    