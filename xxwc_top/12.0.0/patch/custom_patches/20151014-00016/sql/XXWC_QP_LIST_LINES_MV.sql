DROP MATERIALIZED VIEW XXWC.XXWC_QP_LIST_LINES_MV  /* Added by Pattabhi on 01-Dec-2015 for TMS 20151014-00016 */
/

CREATE MATERIALIZED VIEW XXWC.XXWC_QP_LIST_LINES_MV (LIST_HEADER_ID,
                                                     LIST_LINE_ID,
						     PRODUCT_ID,
						      OPERAND,
						      START_DATE_ACTIVE,
						      END_DATE_ACTIVE)
  BUILD IMMEDIATE
  REFRESH COMPLETE ON DEMAND START WITH sysdate+0 NEXT trunc(SYSDATE+1)+4/24  /* Added by Pattabhi on 01-Dec-2015 for TMS 20151014-00016 */
  USING DEFAULT LOCAL ROLLBACK SEGMENT  /* Added by Pattabhi on 01-Dec-2015 for TMS 20151014-00016 */
  AS select list_header_id,
  list_line_id,
  product_id,
  operand,
  start_date_active,
  end_date_active 
  from apps.qp_list_lines_v  
/

COMMENT ON MATERIALIZED VIEW XXWC.XXWC_QP_LIST_LINES_MV IS 'snapshot table for snapshot XXWC.XXWC_QP_LIST_LINES_MV'
/