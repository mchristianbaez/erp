CREATE OR REPLACE TRIGGER XXWC.XXWC_MS_RESPONSIBILITY2_TRG
AFTER DELETE
   ON "XXWC"."XXWC_MS_RESPONSIBILITY_TBL"
   FOR EACH ROW
/*************************************************************************
 Copyright (c) 2012 HD Supply
 All rights reserved.
**************************************************************************
   $Header XXWC_MS_RESPONSIBILITY2_TRG $
  Module Name: XXWC_MS_RESPONSIBILITY2_TRG

  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     26-AUG-2015   Pahwa, Nancy                Initially Created 
TMS# 20150901-00129   
**************************************************************************/
declare
  l_journal_notes varchar2(32000);
  l_changed_check varchar2(1) default 'N';
begin
   if deleting then
        l_journal_notes := l_journal_notes || ' Responsibility Name deleted: ' ||
                         :old.responsibility_name;
      l_changed_check := 'Y';

     if l_changed_check = 'Y' then
      insert into XXWC_MS_RESP_JOURNAL_TBL
      (resp_id, notes,app_id)
    values
      (:old.RESPONSIBILITY_ID, l_journal_notes,:old.app_id);
    end if;
  end if;
end;
/