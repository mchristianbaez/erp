create or replace trigger "XXWC"."XXWC_MS_RESPONSIBILITY_TRG"
  before insert or update on "XXWC"."XXWC_MS_RESPONSIBILITY_TBL"
  for each row
/*************************************************************************
 Copyright (c) 2012 HD Supply
 All rights reserved.
**************************************************************************
   $Header XXWC_MS_RESPONSIBILITY_TRG $
  Module Name: XXWC_MS_RESPONSIBILITY_TRG

  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     26-AUG-2015   Pahwa, Nancy                Initially Created 
TMS# 20150901-00129   
**************************************************************************/
declare
  l_journal_notes varchar2(32000);
  l_changed_check varchar2(1) default 'N';
begin
  if inserting then
  if :NEW."RESPONSIBILITY_ID" is null then
    select "XXWC_MS_RESPONSIBILITY_SEQ".nextval into :NEW."RESPONSIBILITY_ID" from dual;
  end if;
  if :NEW."APP_ID" is null then
  :NEW.APP_ID:= v('APP_ID');
   end if;
    :NEW.CREATED_ON := SYSDATE;
   :NEW.CREATED_BY := nvl(v('APP_USER'),USER);
    l_journal_notes := 'New Responsibility Added - ' ||  :new.responsibility_name;
    insert into XXWC_MS_RESP_JOURNAL_TBL
      (resp_id, notes, app_id)
    values
      (:new.RESPONSIBILITY_ID, l_journal_notes, :new.app_id);
  end if;
  if updating then
    :NEW.UPDATED_ON := SYSDATE;
    :NEW.UPDATED_BY := nvl(v('APP_USER'),USER);
     if :NEW.RESPONSIBILITY_NAME != :old.responsibility_name then
      l_journal_notes := l_journal_notes || ' Responsibility Name from: ' ||
                         :old.responsibility_name || ' => ' || :new.responsibility_name || '.';
      l_changed_check := 'Y';
    end if;
     if l_changed_check = 'Y' then
      insert into XXWC_MS_RESP_JOURNAL_TBL
      (resp_id, notes, app_id)
    values
      (:new.RESPONSIBILITY_ID, l_journal_notes,:new.app_id);
    end if;
  end if;
end;
/