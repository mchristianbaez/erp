CREATE OR REPLACE TRIGGER XXWC.XXWC_MS_USER_ROLES2_TRG
AFTER DELETE
   ON "XXWC"."XXWC_MS_USER_ROLES_TBL"
   FOR EACH ROW
/*************************************************************************
 Copyright (c) 2012 HD Supply
 All rights reserved.
**************************************************************************
   $Header XXWC_MS_USER_ROLES2_TRG $
  Module Name: XXWC_MS_USER_ROLES2_TRG
  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     26-AUG-2015   Pahwa, Nancy                Initially Created 
TMS# 20150901-00129   
**************************************************************************/
declare
  l_journal_notes varchar2(32000);
  l_changed_check varchar2(1) default 'N';
begin
  if deleting then

     l_journal_notes := l_journal_notes || 'Removed Role ' ||
                         apps.xxwc_ms_master_admin_pkg.get_role_name(:old.role_id) || ' from user ' ||
                         apps.xxwc_ms_master_admin_pkg.get_user_name(:old.user_id);
      l_changed_check := 'Y';
  end if;
    if l_changed_check = 'Y' then
     insert into XXWC_MS_USER_ROLES_JOURNAL_TBL
      (user_roles_id, notes,user_id)
    values
      (:old.USER_ROLE_ID, l_journal_notes,:old.user_id);
    end if;
  --end if;
 /* if :NEW."APP_ID" is null then
    :NEW.APP_ID := v('APP_ID');
  end if;*/
end;
/