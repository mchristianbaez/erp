CREATE OR REPLACE TRIGGER "XXWC"."XXWC_MS_ROLES_JOURNAL_TRG"
BEFORE
insert or update on "XXWC"."XXWC_MS_ROLES_JOURNAL_TBL"
for each row
/*************************************************************************
 Copyright (c) 2012 HD Supply
 All rights reserved.
**************************************************************************
   $Header XXWC_MS_ROLES_JOURNAL_TRG $
  Module Name: XXWC_MS_ROLES_JOURNAL_TRG
  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     26-AUG-2015   Pahwa, Nancy                Initially Created 
TMS# 20150901-00129   
**************************************************************************/
begin
      if inserting then
         if :NEW.JOURNAL_ID is null then
            select XXWC_MS_ROLES_JOURNAL_SEQ.NEXTVAL
            into :NEW.JOURNAL_ID
            from DUAL;
         end if;
         :NEW.CREATED_ON := SYSDATE;
         :NEW.CREATED_BY := (nvl(v('APP_USER'),USER));
      end if;
      if updating then
         :NEW.UPDATED_ON := SYSDATE;
         :NEW.UPDATED_BY := (nvl(v('APP_USER'),USER));
      end if;
   end;
/