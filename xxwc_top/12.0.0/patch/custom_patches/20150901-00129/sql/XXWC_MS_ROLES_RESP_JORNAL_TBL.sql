create table XXWC.XXWC_MS_ROLES_RESP_JOURNAL_TBL
/*************************************************************************
 Copyright (c) 2012 HD Supply
 All rights reserved.
**************************************************************************
   $Header XXWC_MS_ROLES_RESP_JOURNAL_TBL $
  Module Name: XXWC_MS_ROLES_RESP_JOURNAL_TBL

  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     26-AUG-2015   Pahwa, Nancy                Initially Created 
TMS# 20150901-00129   
**************************************************************************/
(
  journal_id    NUMBER not null,
  roles_resp_id NUMBER not null,
  notes         VARCHAR2(1000) not null,
  created_by    VARCHAR2(50) not null,
  created_on    DATE not null,
  updated_by    VARCHAR2(50),
  updated_on    DATE,
  app_id        NUMBER
)
tablespace XXWC_DATA
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );