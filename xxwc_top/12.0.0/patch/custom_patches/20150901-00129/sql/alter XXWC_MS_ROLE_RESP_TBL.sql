/*************************************************************************
 Copyright (c) 2012 HD Supply
 All rights reserved.
**************************************************************************
   $Header XXWC_MS_ROLE_RESP_TBL $
  Module Name: XXWC_MS_ROLE_RESP_TBL

  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     26-AUG-2015   Pahwa, Nancy                Initially Created 
TMS# 20150901-00129   
**************************************************************************/
alter table XXWC.XXWC_MS_ROLE_RESP_TBL 
  add constraint XXWC_WC_ROLE_RESP_PK primary key (ROLE_RESP_ID)
  using index 
  tablespace XXWC_DATA
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
alter table XXWC.XXWC_MS_ROLE_RESP_TBL 
  add constraint XXWC_WC_ROLE_RESP_FK foreign key (RESPONSIBILITY_ID)
  references XXWC_MS_RESPONSIBILITY_TBL (RESPONSIBILITY_ID) on delete cascade;
alter table XXWC.XXWC_MS_ROLE_RESP_TBL 
  add constraint XXWC_ROLE_RESP_FK2 foreign key (ROLE_ID)
  references XXWC_MS_ROLES_TBL (ROLE_ID) on delete cascade
  disable;