create or replace trigger "XXWC"."XXWC_MS_ROLE_RESP_TRG"
  before insert or update on "XXWC"."XXWC_MS_ROLE_RESP_TBL"
  for each row
/*************************************************************************
 Copyright (c) 2012 HD Supply
 All rights reserved.
**************************************************************************
   $Header XXWC_MS_ROLE_RESP_TRG $
  Module Name: XXWC_MS_ROLE_RESP_TRG
  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     26-AUG-2015   Pahwa, Nancy                Initially Created 
TMS# 20150901-00129   
**************************************************************************/
declare
  l_journal_notes varchar2(32000);
  l_changed_check varchar2(1) default 'N';
begin
  if inserting then
    if :NEW."ROLE_RESP_ID" is null then
      select "XXWC_MS_ROLE_RESP_SEQ".nextval into :NEW."ROLE_RESP_ID" from dual;
    end if;
    if :NEW."APP_ID" is null then
      :NEW.APP_ID := v('APP_ID');
    end if;
    :NEW.CREATED_ON := SYSDATE;
    :NEW.CREATED_BY := nvl(v('APP_USER'), USER);
    l_journal_notes :=  'Responsibility - ' ||
                       apps.xxwc_ms_master_admin_pkg.get_responsibility_name(:new.responsibility_id,:new.app_id)
                       || ' ' || ' mapped to Role - ' || apps.xxwc_ms_master_admin_pkg.get_role_name(:new.role_id);
    insert into XXWC_MS_ROLES_RESP_JOURNAL_TBL
      (ROLES_RESP_ID, notes, app_id)
    values
      (:new.ROLE_RESP_ID, l_journal_notes, :new.app_id);
  end if;
  if updating then
    :NEW.UPDATED_ON := SYSDATE;
    :NEW.UPDATED_BY := nvl(v('APP_USER'), USER);
    l_journal_notes := null;
    if :NEW.RESPONSIBILITY_ID != :old.responsibility_id and :new.role_id = :old.role_id then
      l_journal_notes := l_journal_notes || 'Changed Responsibility from: ' ||
                         apps.xxwc_ms_master_admin_pkg.get_responsibility_name(:old.responsibility_id,
                                                                 :old.app_id) ||
                         ' => ' ||
                         apps.xxwc_ms_master_admin_pkg.get_responsibility_name(:new.responsibility_id,
                                                                 :new.app_id) ||
                                                                 ' for Role: ' ||
                          apps.xxwc_ms_master_admin_pkg.get_role_name(:old.role_id);
      l_changed_check := 'Y';
    end if;
    if :NEW.RESPONSIBILITY_ID != :old.responsibility_id and :new.role_id != :old.role_id then
      l_journal_notes := l_journal_notes || 'Changed Responsibility from: ' ||
                         apps.xxwc_ms_master_admin_pkg.get_responsibility_name(:old.responsibility_id,
                                                                 :old.app_id) ||
                         ' => ' ||
                         apps.xxwc_ms_master_admin_pkg.get_responsibility_name(:new.responsibility_id,
                                                                 :new.app_id) ||
                                                                 ' Changed Role from: ' ||
                          apps.xxwc_ms_master_admin_pkg.get_role_name(:old.role_id) || '=> ' ||
                           apps.xxwc_ms_master_admin_pkg.get_role_name(:new.role_id);
      l_changed_check := 'Y';
    end if;
    if :NEW.ROLE_ID != :old.role_id and :new.responsibility_id = :old.responsibility_id then
      l_journal_notes := l_journal_notes || 'Changed Role from: ' ||
                         apps.xxwc_ms_master_admin_pkg.get_role_name(:old.role_id) ||
                         ' => ' ||
                         apps.xxwc_ms_master_admin_pkg.get_role_name(:new.role_id) || ' for Responsibility: '
                         || apps.xxwc_ms_master_admin_pkg.get_responsibility_name(:old.responsibility_id,
                                                                 :old.app_id) ;
      l_changed_check := 'Y';
    end if;
    if l_changed_check = 'Y' then
      insert into XXWC_MS_ROLES_RESP_JOURNAL_TBL
        (ROLES_RESP_ID, notes, app_id)
      values
        (:new.ROLE_RESP_ID, l_journal_notes, :new.app_id);
    end if;
  end if;
end;
/