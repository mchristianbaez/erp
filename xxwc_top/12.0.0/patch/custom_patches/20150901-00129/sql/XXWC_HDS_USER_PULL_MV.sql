/**************************************************************************
File Name:XXWC_HDS_USER_PULL_MV.sql
TYPE:     Materialized View
Description: Materialized View for User data from HR source.

VERSION DATE          AUTHOR(S)       DESCRIPTION
------- -----------   --------------- ---------------------------------
1.0     15-Sep-2015   Nancy Pahwa   TMS# 20150901-00129
                                      Created
**************************************************************************/
create materialized view APPS.XXWC_HDS_USER_PULL_MV
refresh complete on demand
start with sysdate next trunc(SYSDATE)+1+20/24 
as
select employee_id,
first_name,
last_name,
last_name || ', ' || first_name name,
email,
position
    from apxcmmn.hr_ad_all_users_vw@APEXWC_LNK;
/
 CREATE UNIQUE INDEX "APPS"."SYS_C001252669" ON "APPS"."XXWC_HDS_USER_PULL_MV" ("EMPLOYEE_ID");
/