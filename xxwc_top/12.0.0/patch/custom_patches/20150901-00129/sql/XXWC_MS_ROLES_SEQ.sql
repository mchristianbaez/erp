/*************************************************************************
 Copyright (c) 2012 HD Supply
 All rights reserved.
**************************************************************************
   $Header XXWC_MS_ROLES_SEQ$
  Module Name: XXWC_MS_ROLES_SEQ

  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     26-AUG-2015   Pahwa, Nancy                Initially Created 
TMS# 20150901-00129   
**************************************************************************/
create sequence XXWC.XXWC_MS_ROLES_SEQ
minvalue 1
maxvalue 999999999999999999999999999
start with 1
increment by 1
nocache;