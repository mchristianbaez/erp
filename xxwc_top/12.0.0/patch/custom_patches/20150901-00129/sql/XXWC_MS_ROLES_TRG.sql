create or replace trigger "XXWC"."XXWC_MS_ROLES_TRG"
  before insert or update on "XXWC"."XXWC_MS_ROLES_TBL"
  for each row
/*************************************************************************
 Copyright (c) 2012 HD Supply
 All rights reserved.
**************************************************************************
   $Header XXWC_MS_ROLES_TRG $
  Module Name: XXWC_MS_ROLES_TRG
  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     26-AUG-2015   Pahwa, Nancy                Initially Created 
TMS# 20150901-00129   
**************************************************************************/
declare
  l_journal_notes varchar2(32000);
  l_changed_check varchar2(1) default 'N';
begin
  if inserting then
    if :NEW."ROLE_ID" is null then
      select "XXWC_MS_ROLES_SEQ".nextval into :NEW."ROLE_ID" from dual;
    end if;
    /* if :NEW."APP_ID" is null then
      :NEW.APP_ID := v('APP_ID');
    end if;*/
    :NEW.CREATED_ON := SYSDATE;
    :NEW.CREATED_BY := nvl(v('APP_USER'), USER);
    l_journal_notes := 'New Role Added - ' || :new.role_name;
    insert into XXWC_MS_ROLES_JOURNAL_TBL
      (roles_id, notes)
    values
      (:new.role_id, l_journal_notes);
  end if;
  if updating then
    :NEW.UPDATED_ON := SYSDATE;
    :NEW.UPDATED_BY := nvl(v('APP_USER'), USER);
    l_journal_notes := null;
    if :NEW.ROLE_NAME != :old.role_name then
      l_journal_notes := l_journal_notes || ' Role Name from: ' ||
                         :old.role_name || ' => ' || :new.role_name;
      l_changed_check := 'Y';
    end if;
    if :NEW.EMAIL != :old.email then
      l_journal_notes := l_journal_notes || ' Email from: ' || :old.email ||
                         ' => ' || :new.email;
      l_changed_check := 'Y';
    end if;
    if :NEW.USER_ID != :old.user_id then
      l_journal_notes := l_journal_notes || ' User ID from: ' ||
                         :old.user_id || ' => ' || :new.user_id ;
      l_changed_check := 'Y';
    end if;
    if l_changed_check = 'Y' then
      insert into XXWC_MS_ROLES_JOURNAL_TBL
        (roles_id, notes)
      values
        (:new.role_id, l_journal_notes);
    end if;
  end if;
end;
/