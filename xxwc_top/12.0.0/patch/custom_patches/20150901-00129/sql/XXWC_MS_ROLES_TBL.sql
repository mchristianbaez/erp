create table XXWC.XXWC_MS_ROLES_TBL
/*************************************************************************
 Copyright (c) 2012 HD Supply
 All rights reserved.
**************************************************************************
   $Header XXWC_MS_ROLES_TBL$
  Module Name: XXWC_MS_ROLES_TBL

  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     26-AUG-2015   Pahwa, Nancy                Initially Created 
TMS# 20150901-00129   
**************************************************************************/
(
  role_id    NUMBER not null,
  role_name  VARCHAR2(250),
  email      VARCHAR2(250),
  user_id    VARCHAR2(50),
  app_id     NUMBER,
  created_by VARCHAR2(50),
  created_on DATE,
  updated_by VARCHAR2(50),
  updated_on DATE
)
tablespace XXWC_DATA
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );