CREATE OR REPLACE TRIGGER APPS.XXWC_CUST_ACCT_SITES_ALL_TRG
   AFTER INSERT
   ON ar.hz_cust_acct_sites_all
   REFERENCING NEW AS new OLD AS old
   FOR EACH ROW
DECLARE
   --PRAGMA autonomous_transaction;
   /**********************************************************************************************
    File Name: APPS.hr_cust_acct_sites_all_trg

    PROGRAM TYPE: TRIGGER

    PURPOSE: Triiger to load new data into STEP taxware system

    HISTORY
    =============================================================================
           Last Update Date : 06/25/2012
    =============================================================================
    =============================================================================
    VERSION DATE          AUTHOR(S)       DESCRIPTION
    ------- -----------   --------------- ----------------------------------------
    1.0     03-MAR-2012   Manny Rodriguez  Created.
    1.1     25-MAR-2012   Manny Rodriguez  Changed certificates to Active.
    1.2     03-APR-2012   Manny Rodriguez  Changed error handling to show cust_id
    1.3     20-APR-2012   Manny Rodriguez  Changed the trigger to look for 'None'
                                           changed Geocode and Use Date to Null
    1.4     25-APR-2012   Manny rodriguez  Changed to create records all the time
                                           but with dynamic certificate checkboxes
    1.5     10-MAY-2012   Manny Rodriguez  Blocking international addresses.
    1.6     25-Jun-2012   Manny Rodriguez  Added state filtering and emails
    1.7     18-AUG-2014   Manny Rodriguez  Changed variables for address to 30char #260988
    1.8     16-OCT-2014   Maharajan Shunmugam TMS#20141001-00161 Canada Multi Org changes
    1.9     22-APR-2015   Raghavendra S    TMS# 20150421-00198 - trigger special characters in address field need to be fixed
    2.0     4/25/2016     Neha Saini       TMS# 20160128-00232 - added more filter to replace special characters and increase address length.
    2.1    31-Aug-2016    Neha Saini       TMS# 20160826-00318-IT  - replacing triggers on hz_cust_acct_sites_all with business events
                                                We are keeping this trigger Because we cannot use standard Business Events we need this trigger to invoke business event which is custom one.
                                                As Standard Business events are based on cust_site_id and we cannot access :new and :old values in standard Business event however we need those
                                                value since our logic hence we chose Custom Business events.

   ***********************************************************************************************/
   l_distro_list    VARCHAR2 (75) DEFAULT 'hdsoracledevelopers@hdsupply.com';
   l_error_msg      VARCHAR2 (5000);
   l_err_callfrom   VARCHAR2 (5000) := 'APPS.XXWC_CUST_ACCT_SITES_ALL_TRG';
   l_sec            VARCHAR2 (5000);
BEGIN
   l_sec :=
      'calling apps.XXWC_AR_BE_PKG.raise_ar_tax_insert_process to invoke business events';

   apps.XXWC_AR_BE_PKG.raise_ar_tax_insert_process (
      p_org_id            => :new.org_id,
      p_cust_account_id   => :new.cust_account_id,
      p_party_site_id     => :new.party_site_id,
      p_attribute15       => :new.attribute15,
      p_error_msg         => l_error_msg);


   l_sec := 'trigger processing completed.';
EXCEPTION
   WHEN OTHERS
   THEN
      l_error_msg := SUBSTR (SQLERRM, 1, 1000);
      xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => l_err_callfrom,
         p_calling             => 'STEP Taxware',
         p_ora_error_msg       => SQLERRM,
         p_error_desc          =>    'Error  at '
                                  || l_sec
                                  || ' for cust acct id:'
                                  || :new.cust_account_id,
         p_distribution_list   => l_distro_list,
         p_module              => 'AR');
END;
/