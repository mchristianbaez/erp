CREATE OR REPLACE PACKAGE APPS.XXWC_AR_BE_PKG
AS
   /********************************************************************************************************************************
      $Header XXWC_AR_BE_PKG.PKG $
      Module Name: XXWC_AR_BE_PKG.PKS
      PURPOSE:   This package is used for replacing triggers on hz_cust_acct_sites_all with business events

      REVISIONS:
      Ver        Date         Author                     Description
      ---------  ----------  ---------------         -------------------------------------------------------------------------------
      1.0        31-Aug-2016  Neha Saini              TMS#20160826-00318-IT  - replacing triggers on hz_cust_acct_sites_all with business events
   *********************************************************************************************************************************/
   PROCEDURE raise_ar_tax_insert_process (
      p_org_id            IN     NUMBER,
      p_cust_account_id   IN     NUMBER,
      p_party_site_id     IN     NUMBER,
      p_attribute15       IN     VARCHAR2,
      p_error_msg            OUT VARCHAR2);

   PROCEDURE raise_ar_tax_update_process (
      p_org_id            IN     NUMBER,
      p_cust_account_id   IN     NUMBER,
      p_party_site_id     IN     NUMBER,
      p_attribute15       IN     VARCHAR2,
      p_old_attribute15   IN     VARCHAR2,
      p_error_msg            OUT VARCHAR2);

   FUNCTION SUBS_AR_CBE_INSERT_PROCESS (
      p_subscription_guid   IN     RAW,
      p_event               IN OUT wf_event_t)
      RETURN VARCHAR2;

   FUNCTION SUBS_AR_CBE_UPDATE_PROCESS (
      p_subscription_guid   IN     RAW,
      p_event               IN OUT wf_event_t)
      RETURN VARCHAR2;
END XXWC_AR_BE_PKG;
/