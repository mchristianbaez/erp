CREATE OR REPLACE TRIGGER APPS.XXWC_CUST_ACCT_SITES_ALL_UTRG
   AFTER UPDATE
   ON ar.hz_cust_acct_sites_all
   REFERENCING NEW AS new OLD AS old
   FOR EACH ROW
DECLARE
   --PRAGMA autonomous_transaction;
   /**********************************************************************************************
    File Name: APPS.hz_cust_acct_sites_all_utrg

    PROGRAM TYPE: TRIGGER

    PURPOSE: Triiger to update tax exemptions into STEP taxware system

    HISTORY
    =============================================================================
           Last Update Date : 06/25/2012
    =============================================================================
    =============================================================================
    VERSION DATE          AUTHOR(S)               DESCRIPTION
    ------- -----------   ---------------         ----------------------------------------
    1.0     03-MAR-2012   Manny Rodriguez          Created.
    1.1     10-MAY-2012   Manny Rodriguez          blocking internationals.
    1.2     30-MAY-2013   Manny Rodriguez          lsec error message #397540
    1.3     18-AUG-2014   Manny Rodriguez          Changed variables for address to 30char   #260988
    1.4     16-OCT-2014   Maharajan Shunmugam     TMS#20141001-00161 Canada Multi Org changes
    1.5     06-APR-2015   Raghavendra S          TMS#20150317-00055 FIN xxwc_cust_acct_sites_all_utrg
                                                 trigger not having enough exceptions handled.
    1.6     10-JUN-2015   Pattabhi Avula         TMS#20150515-00081 FIN xxwc_cust_acct_sites_all_utrg
                                                 Commented the duplicate exception RAISE PROGRAM_ERROR
    1.7     31-Aug-2016  Neha Saini             TMS#20160826-00318-IT  - replacing triggers on hz_cust_acct_sites_all with business events
                                                We are keeping this trigger Because we cannot use standard Business Events we need this trigger to invoke business event which is custom one.
                                                As Standard Business events are based on cust_site_id and we cannot access :new and :old values in standard Business event however we need those value sin our logic hence we chose Custom Business events.
   ***********************************************************************************************/

   l_distro_list    VARCHAR2 (75) DEFAULT 'hdsoracledevelopers@hdsupply.com';
   l_error_msg      VARCHAR2 (5000);
   l_err_callfrom   VARCHAR2 (5000) := 'APPS.XXWC_CUST_ACCT_SITES_ALL_UTRG';
   l_sec            VARCHAR2 (32000);
BEGIN
   l_sec :=
      'calling apps.XXWC_AR_BE_PKG.raise_ar_tax_update_process to invoke business events';
   l_sec :=
      ':new.cust_account_id '||:new.cust_account_id||' :new.party_site_id '||:new.party_site_id ||' :new.attribute15 '||:new.attribute15 ||' :old.attribute15 '||:old.attribute15||' :new.org_id '||:new.org_id;

   apps.XXWC_AR_BE_PKG.raise_ar_tax_update_process (
      p_org_id            => :new.org_id,
      p_cust_account_id   => :new.cust_account_id,
      p_party_site_id     => :new.party_site_id,
      p_attribute15       => :new.attribute15,
      p_old_attribute15   => :old.attribute15,
      p_error_msg         => l_error_msg);

   l_sec := 'trigger processing completed.';
EXCEPTION
   WHEN OTHERS
   THEN
      l_error_msg := SUBSTR (SQLERRM, 1, 1000);
      xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => l_err_callfrom,
         p_calling             => 'STEP Taxware',
         p_ora_error_msg       => SQLERRM,
         p_error_desc          =>    'Error  at '
                                  || l_sec
                                  || ' for cust acct id:'
                                  || :new.cust_account_id,
         p_distribution_list   => l_distro_list,
         p_module              => 'AR');
END;
/