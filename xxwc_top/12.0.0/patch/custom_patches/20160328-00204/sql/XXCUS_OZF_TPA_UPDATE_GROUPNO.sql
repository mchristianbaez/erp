set serveroutput on size 1000000;
declare
/*
    TMS: 20160328-00125
    Incident: 665656
    Author: Balaguru Seshadri
    Date; 03/28/2016
    Scope: Update group numbers for individual partner account id's upto 10 at a time
*/
 --
 cursor tpa is select rowid row_id from xxcus.xxcus_ozf_tpa_runs_tmp
 order by org_id asc;
 --
 l_records_per_group number :=10; 
 l_grp_counter number :=0;
 l_grp_no varchar2(30) :=Null;
 l_fetch number :=0;
 v_current_rowid varchar2(150):=Null;
 --
begin
     for rec in tpa loop 
      --
      v_current_rowid :=rec.row_id;
      --
      l_fetch :=l_fetch + 1;
      --
        if (l_grp_counter =0) then
          l_grp_counter :=l_grp_counter+1;
          dbms_output.put_line('Set l_grp_no to 1');
        end if;
      --
      if (mod(l_fetch,10) =0) then
             --
             l_grp_no :='REB-'||to_char(sysdate, 'YYYYMMDD')||'-'||to_char(l_grp_counter,  'FM9000');
             --
             l_grp_counter :=l_grp_counter +1;  --if we reached 10 records then reset the group no to current group no plus 1
             --             
             dbms_output.put_line('Set l_grp_no to '||l_grp_no);
             --
      else
             --
             l_grp_no :='REB-'||to_char(sysdate, 'YYYYMMDD')||'-'||to_char(l_grp_counter,  'FM9000');
             --
             dbms_output.put_line('Set l_grp_no to '||l_grp_no);
             --
      end if;
      --
      update xxcus.xxcus_ozf_tpa_runs_tmp
      set group_no =l_grp_no
      where rowid =rec.row_id;
      --
     end loop;
     --
     commit;
     --
exception
 when others then
  dbms_output.put_line('Outer block, rowid ='||v_current_rowid||', error message ='||sqlerrm);     
end;
/