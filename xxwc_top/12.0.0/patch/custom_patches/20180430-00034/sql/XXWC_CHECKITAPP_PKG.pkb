CREATE OR REPLACE PACKAGE BODY APPS.XXWC_CHECKITAPP_PKG

AS
/*************************************************************************
     $Header XXWC_CHECKITAPP_PKG $
     Module Name: XXWC_CHECKITAPP_PKG.pkb

     PURPOSE:   Mobile App    
                       
     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        11/02/2017  Niraj K Ranjan          TMS#20171102-00074-Mobile apps new plsql procedures for WOC                
     2.0        01/23/2018  Rakesh Patel            TMS#20180123-00267-WOC - Order created from Quote should always have 078 as the header branch.
     3.0        01/24/2009  Rakesh Patel            TMS#20180124-00147-Fix to update payment method for WOC branch (078)
	 4.0        02/16/2018  Niraj K Ranjan          TMS#20180205-00161   @CheckIT Ability to subscribe a customer for Order Status Updates - OSU through CheckIT app
	 5.0        03/12/2018  Rakesh P.               TMS#20180302-00245-Replacement Cost Enhancement 
     6.0        03/23/2018  Rakesh P.	            TMS# 20180411-00083-Disclaimer needs to be added to Checkit for Custom Quote 
     7.0        05/02/2018  Rakesh P.               TMS#TMS#20180430-00034-Modify CheckIT customer prospect procedure to better accommodate cash customers
**************************************************************************/
   --Email Defaults
   g_dflt_email   fnd_user.email_address%TYPE := 'WC-ITDEVALERTS-U1@HDSupply.com';
   g_pkg_name     VARCHAR2 (50) := 'XXWC_CHECKITAPP_PKG';
   g_xxwc_woc_debug  VARCHAR2(30) := fnd_profile.value('XXWC_OSO_DEBUG');
   

/*************************************************************************
     $Header clone_part_prc $
     Module Name: clone_part_prc

     PURPOSE:   Clone item from one organization to other organization    
                       
     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        11/02/2017  Niraj K Ranjan          TMS#20171102-00074-Mobile apps new plsql procedures for WOC               
**************************************************************************/
   PROCEDURE clone_part_prc (  p_transaction_id      IN   NUMBER DEFAULT NULL
                              ,p_inventory_item_id   IN   NUMBER
                              ,p_to_org_id           IN   NUMBER
                              ,p_from_org_id         IN   NUMBER
                              ,p_return_status       OUT  VARCHAR2
                              ,p_return_message      OUT  VARCHAR2)
   IS
      l_transaction_id NUMBER;
      l_inventory_item_id NUMBER;
      l_to_org_id      NUMBER;
      l_from_org_id    NUMBER;
      l_primary_uom_code mtl_system_items_b.primary_uom_code%TYPE;
      l_return_status VARCHAR2(1);
      l_msg_data      VARCHAR2(2000);
      l_item_cost     mtl_system_items.list_price_per_unit%type;
      e_biz_exception EXCEPTION;
   BEGIN
      l_return_status := '';
      l_msg_data      := '';
      l_item_cost     := NULL;
      l_primary_uom_code := NULL;
      /*select organization_id
      into l_from_org_id
      from org_organization_definitions 
      where organization_name=:xxwc_clone_parts.from_org_code;*/
      
      BEGIN
         SELECT  a.list_price_per_unit,
                 a.primary_uom_code
         INTO l_item_cost,l_primary_uom_code
         FROM mtl_system_items a
         WHERE 1=1
         AND a.inventory_item_id= p_inventory_item_id
         AND a.organization_id = p_from_org_id;
      EXCEPTION
         WHEN OTHERS THEN 
            l_return_status := 'E';
            l_msg_data := 'Item not found for org id '||p_from_org_id;
           RAISE e_biz_exception;
      END;
      XXWC_INV_NEW_PROD_REQ_PKG.process_item_org_assignments(p_transaction_id,
                                                          p_inventory_item_id, 
                                                          p_to_org_id,
                                                          l_primary_uom_code,
                                                          l_item_cost,-- Shankar TMS 20130401-00537 01-Apr-2013
                                                          p_from_org_id,
                                                          l_return_status,
                                                          l_msg_data);
      p_return_status  := l_return_status;
      p_return_message := l_msg_data;
  EXCEPTION
    WHEN e_biz_exception THEN
       p_return_status  := l_return_status;
       p_return_message := l_msg_data;
    WHEN OTHERS THEN
       p_return_status := 'E';
       p_return_message := SUBSTR(SQLERRM,1,2000);
   END clone_part_prc;
   
   /*************************************************************************
     $Header create_so_from_quote $
     Module Name: create_so_from_quote

     PURPOSE:   Create order from a quote
                       
     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        11/21/2017  Niraj K Ranjan          TMS#20171102-00074-Mobile apps new plsql procedures for WOC               
**************************************************************************/
   PROCEDURE create_so_from_quote (p_quote_number   IN   NUMBER
                                  ,p_header_id      OUT  NUMBER
                                  ,p_return_status  OUT  VARCHAR2
                                  ,p_return_message OUT  VARCHAR2)
   IS
      l_wf_type VARCHAR2(4) := 'OENH';
      l_activity_name VARCHAR2(21) := 'SUBMIT_DRAFT_ELIGIBLE';
      l_result VARCHAR2(8) := 'COMPLETE';
      l_quote_status OE_ORDER_HEADERS_ALL.FLOW_STATUS_CODE%TYPE;
      e_biz_exception EXCEPTION;
      l_return_status VARCHAR2(1);
      l_return_message VARCHAR2(2000);
      l_header_id oe_order_headers.header_id%TYPE;
   BEGIN
      l_quote_status := null;
      l_header_id := null;
      BEGIN
         SELECT flow_status_code,header_id
         INTO l_quote_status,l_header_id
         FROM oe_order_headers
         WHERE order_number = p_quote_number
         AND   flow_status_code IN ('DRAFT','PENDING_CUSTOMER_ACCEPTANCE');
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            l_return_message := 'Quote# '||p_quote_number||' not found';
            l_return_status := 'E';
            RAISE e_biz_exception;
         WHEN OTHERS THEN
            l_return_message := SUBSTR(SQLERRM,1,2000);
            l_return_status := 'E';
            RAISE e_biz_exception;
      END;
      IF l_quote_status = 'DRAFT' THEN
         BEGIN
         fnd_wf_engine.CompleteActivityInternalName(l_wf_type,
                                                    l_header_id,
                                                    l_activity_name,
                                                    l_result);
         COMMIT;
         EXCEPTION
            WHEN OTHERS THEN
               l_return_message := SUBSTR(SQLERRM,1,2000);
               l_return_status := 'E';
               RAISE e_biz_exception;
         END;
         LOOP
            SELECT flow_status_code
             INTO l_quote_status
             FROM oe_order_headers
             WHERE header_id = l_header_id; 
            EXIT WHEN l_quote_status = 'PENDING_CUSTOMER_ACCEPTANCE';
         END LOOP;
      END IF;
      IF l_quote_status = 'PENDING_CUSTOMER_ACCEPTANCE' THEN        
         oe_negotiate_wf.Customer_Accepted(l_header_id,
                                           l_return_status); 
         IF l_return_status = 'S' THEN
            COMMIT;
            p_header_id := l_header_id;
         ELSE
            ROLLBACK;
            l_return_message := 'Order not created from quote - CUSTOMER_ACCEPTANCE failed for quote#'||p_quote_number;
            l_return_status := 'E';
            RAISE e_biz_exception;
         END IF;
      END IF;
      p_return_status := 'S';
      p_return_message := '';
   EXCEPTION
    WHEN e_biz_exception THEN
       p_return_status  := l_return_status;
       p_return_message := l_return_message;
    WHEN OTHERS THEN
       p_return_status := 'E';
       p_return_message := SUBSTR(SQLERRM,1,2000);
   END create_so_from_quote;
   
   /*************************************************************************
     $Header create_so_from_quote $
     Module Name: create_so_from_quote

     PURPOSE:   Create order from a quote
                       
     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        11/21/2017  Niraj K Ranjan          TMS#20171102-00074-Mobile apps new plsql procedures for WOC               
**************************************************************************/
   PROCEDURE update_order_branch ( p_header_id       IN   NUMBER
                                  ,p_transfer_org_id IN   NUMBER
                                  ,p_return_status   OUT  VARCHAR2
                                  ,p_return_message  OUT  VARCHAR2)
   IS
      e_biz_exception EXCEPTION;
      l_return_status VARCHAR2(1);
      l_return_message VARCHAR2(2000);

      -- IN Variables --
      l_api_version_number           NUMBER  := 1;
      l_header_rec                   oe_order_pub.header_rec_type;
      l_line_tbl                     oe_order_pub.line_tbl_type;
      l_action_request_tbl           oe_order_pub.request_tbl_type;
      l_line_adj_tbl                 oe_order_pub.line_adj_tbl_type;

      -- OUT Variables --
      l_header_rec_out               oe_order_pub.header_rec_type;
      l_header_val_rec_out           oe_order_pub.header_val_rec_type;
      l_header_adj_tbl_out           oe_order_pub.header_adj_tbl_type;
      l_header_adj_val_tbl_out       oe_order_pub.header_adj_val_tbl_type;
      l_header_price_att_tbl_out     oe_order_pub.header_price_att_tbl_type;
      l_header_adj_att_tbl_out       oe_order_pub.header_adj_att_tbl_type;
      l_header_adj_assoc_tbl_out     oe_order_pub.header_adj_assoc_tbl_type;
      l_header_scredit_tbl_out       oe_order_pub.header_scredit_tbl_type;
      l_header_scredit_val_tbl_out   oe_order_pub.header_scredit_val_tbl_type;
      l_line_tbl_out                 oe_order_pub.line_tbl_type;
      l_line_val_tbl_out             oe_order_pub.line_val_tbl_type;
      l_line_adj_tbl_out             oe_order_pub.line_adj_tbl_type;
      l_line_adj_val_tbl_out         oe_order_pub.line_adj_val_tbl_type;
      l_line_price_att_tbl_out       oe_order_pub.line_price_att_tbl_type;
      l_line_adj_att_tbl_out         oe_order_pub.line_adj_att_tbl_type;
      l_line_adj_assoc_tbl_out       oe_order_pub.line_adj_assoc_tbl_type;
      l_line_scredit_tbl_out         oe_order_pub.line_scredit_tbl_type;
      l_line_scredit_val_tbl_out     oe_order_pub.line_scredit_val_tbl_type;
      l_lot_serial_tbl_out           oe_order_pub.lot_serial_tbl_type;
      l_lot_serial_val_tbl_out       oe_order_pub.lot_serial_val_tbl_type;
      l_action_request_tbl_out       oe_order_pub.request_tbl_type;
      
      --l_msg_index                    NUMBER;
      --l_data                         VARCHAR2 (2000);
      --l_loop_count                   NUMBER;
      --l_debug_file                   VARCHAR2 (200);
      l_ret_status                   VARCHAR2 (200);
      l_msg_count                    NUMBER;
      l_msg_data                     VARCHAR2 (2000);
      l_line_tbl_index               NUMBER;
      l_ord_count                    NUMBER;   
      l_call_api_ind                 VARCHAR2(1) := 'N';      
      
      CURSOR cr_line
      IS (SELECT line_id 
          FROM  oe_order_lines
          WHERE header_id = p_header_id
          AND   ship_from_org_id <> p_transfer_org_id);
   BEGIN
      l_return_status := 'S';
      l_return_message := '';
      SELECT COUNT(1) INTO l_ord_count
      FROM  oe_order_headers
      WHERE header_id = p_header_id
      AND   ship_from_org_id = p_transfer_org_id;
      IF l_ord_count = 0 THEN
         -- Header Record --
         l_header_rec                        := oe_order_pub.g_miss_header_rec;
         l_header_rec.operation              := OE_GLOBALS.G_OPR_UPDATE;
        -- l_header_rec.ship_from_org_id       := p_transfer_org_id; --Rev#2.0
         l_header_rec.header_id              := p_header_id;
         l_call_api_ind                      := 'Y';
      END IF;
      --l_action_request_tbl (1)            := oe_order_pub.g_miss_request_rec;

      -- Line Record --
      l_line_tbl_index                    := 0;
      l_line_tbl (1)                      := oe_order_pub.g_miss_line_rec;
      FOR rec_line IN cr_line
      LOOP
         l_line_tbl_index := l_line_tbl_index + 1; 
         -- Changed attributes 
         l_line_tbl(l_line_tbl_index) := OE_ORDER_PUB.G_MISS_LINE_REC; 
         --l_line_tbl(l_line_tbl_index).invoice_to_org_id := 322; 
         l_line_tbl(l_line_tbl_index).ship_from_org_id := p_transfer_org_id; 
         -- Primary key of the entity i.e. the order line 
         l_line_tbl(l_line_tbl_index).line_id := rec_line.line_id; 
         l_line_tbl(l_line_tbl_index).change_reason := 'Update Fullfillment Org'; 
         -- Indicates to process order that this is an update operation 
         l_line_tbl(l_line_tbl_index).operation := OE_GLOBALS.G_OPR_UPDATE;
         l_call_api_ind := 'Y';
      END LOOP;
       
      -- Added below code to book the sales order.
      /**l_call_api_ind                            := 'Y';
      l_action_request_tbl (1)                  := oe_order_pub.g_miss_request_rec;
      l_action_request_tbl (1).request_type     := oe_globals.g_book_order;
      l_action_request_tbl (1).entity_code      := oe_globals.G_ENTITY_HEADER;
      l_action_request_tbl(1).entity_id         := p_header_id;
      l_call_api_ind                            := 'Y';**/
      
      -- Calling the API to update the header details of an existing Order --
     IF l_call_api_ind = 'Y' THEN
        OE_ORDER_PUB.PROCESS_ORDER 
           ( p_api_version_number          => l_api_version_number
           , p_header_rec                  => l_header_rec
           , p_line_tbl                    => l_line_tbl
           , p_action_request_tbl          => l_action_request_tbl
           , p_line_adj_tbl                => l_line_adj_tbl
           -- OUT variables
           , x_header_rec                  => l_header_rec_out
           , x_header_val_rec              => l_header_val_rec_out
           , x_header_adj_tbl              => l_header_adj_tbl_out
           , x_header_adj_val_tbl          => l_header_adj_val_tbl_out
           , x_header_price_att_tbl        => l_header_price_att_tbl_out
           , x_header_adj_att_tbl          => l_header_adj_att_tbl_out
           , x_header_adj_assoc_tbl        => l_header_adj_assoc_tbl_out
           , x_header_scredit_tbl          => l_header_scredit_tbl_out
           , x_header_scredit_val_tbl      => l_header_scredit_val_tbl_out
           , x_line_tbl                    => l_line_tbl_out
           , x_line_val_tbl                => l_line_val_tbl_out
           , x_line_adj_tbl                => l_line_adj_tbl_out
           , x_line_adj_val_tbl            => l_line_adj_val_tbl_out
           , x_line_price_att_tbl          => l_line_price_att_tbl_out
           , x_line_adj_att_tbl            => l_line_adj_att_tbl_out
           , x_line_adj_assoc_tbl          => l_line_adj_assoc_tbl_out
           , x_line_scredit_tbl            => l_line_scredit_tbl_out
           , x_line_scredit_val_tbl        => l_line_scredit_val_tbl_out
           , x_lot_serial_tbl              => l_lot_serial_tbl_out
           , x_lot_serial_val_tbl          => l_lot_serial_val_tbl_out
           , x_action_request_tbl          => l_action_request_tbl_out
           , x_return_status               => l_ret_status
           , x_msg_count                   => l_msg_count
           , x_msg_data                    => l_msg_data
           );

          IF l_ret_status = fnd_api.g_ret_sts_success THEN
              COMMIT;
              l_return_status := 'S';
                l_return_message := '';
          ELSE
              FOR i IN 1 .. l_msg_count
              LOOP
                l_msg_data := oe_msg_pub.get( p_msg_index => i, p_encoded => 'F');
                l_return_message := SUBSTR(l_return_message||' '||i||'.'||l_msg_data,1,2000);
              END LOOP;
                l_return_status := 'E';
                ROLLBACK;
          END IF;
       END IF;
       p_return_status  := l_return_status;
       p_return_message := l_return_message;
   EXCEPTION
      WHEN e_biz_exception THEN
       p_return_status  := l_return_status;
       p_return_message := l_return_message;
    WHEN OTHERS THEN
       p_return_status := 'E';
       p_return_message := SUBSTR(SQLERRM,1,2000);
   END update_order_branch;
   /*******************************************************************************
   Function Name   :   IS_VALID_OM_USER
   Description     :   This function will be used to valid if user has access to valid om resp.

   Change History  :
   DATE         NAME              Modification
   --------     -------           ------------------------------------------
   15-Mar-17    Rakesh Patel      TMS#20170313-00308 - Validate the user for resp access in order create api, enable debug message
   ******************************************************************************/
   FUNCTION IS_VALID_OM_USER (p_user_id IN NUMBER)
      RETURN VARCHAR2
   AS 
      l_count NUMBER :=0;
   BEGIN
      SELECT count(1) 
        INTO l_count 
        FROM FND_USER fuser,
             PER_PEOPLE_F per,
             FND_USER_RESP_GROUPS furg,
             FND_RESPONSIBILITY_TL frt
       WHERE fuser.EMPLOYEE_ID = per.PERSON_ID
         AND fuser.USER_ID = furg.USER_ID
         AND fuser.USER_ID = p_user_id
         AND (TO_CHAR (fuser.END_DATE) IS NULL OR fuser.END_DATE > SYSDATE)
         AND frt.RESPONSIBILITY_ID = furg.RESPONSIBILITY_ID
         AND (TO_CHAR (furg.END_DATE) IS NULL OR furg.END_DATE > SYSDATE)
         AND frt.LANGUAGE = 'US'
         AND frt.RESPONSIBILITY_NAME IN ('HDS Order Mgmt Pricing Full - WC'
                                        ,'HDS Order Mgmt Pricing Limited - WC'
                                        ,'HDS Order Mgmt Pricing Standard - WC'
                                        ,'HDS Order Mgmt Pricing Super - WC'
                                        ,'HDS Rental Order Mgmt Pricing Super - WC'
                                        );
        IF l_count > 0 THEN
           RETURN 'Y';
        ELSE
           RETURN 'N';
        END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 'N';
   END IS_VALID_OM_USER;
   
   /*************************************************************************
    Input Parameters:  a. p_xxwc_address_rec - address info.

    Output Parameters: o_order_out_rec is a record type and will have following column
                       o_error_code - 'E' - Error or 'S'- Success
                       o_error_msg  - Error message, reason for the failure.
**************************************************************************/
   PROCEDURE CREATE_JOB_SITE
   (
     p_xxwc_address_rec     IN  xxwc.xxwc_address_rec
   -- ,p_resp_id              IN  NUMBER
   -- ,p_resp_appl_id         IN  NUMBER
    ,p_created_by_module    IN  VARCHAR2 DEFAULT NULL
    ,o_status               OUT VARCHAR2
    ,o_ship_to_site_use_id  OUT NUMBER
    ,o_bill_to_site_use_id  OUT NUMBER
    ,o_message              OUT VARCHAR2
   ) IS
   l_proc_name              VARCHAR2(100):= g_pkg_name||'.'||'CREATE_JOB_SITE';
   l_exception              EXCEPTION;
   l_party_id               NUMBER;
   l_user_id                NUMBER;
   l_resp_id                NUMBER;
   l_resp_appl_id           NUMBER;
   l_sec                    VARCHAR2(4000);
   l_error_message          VARCHAR2(4000);
   x_return_status          VARCHAR2 (2000);
   x_msg_count              NUMBER;
   x_msg_data               VARCHAR2 (2000);

   --location api variable
   P_location_rec           HZ_LOCATION_V2PUB.LOCATION_REC_TYPE;
   x_location_id            NUMBER;

   --party site api variable
   p_party_site_rec         hz_party_site_v2pub.party_site_rec_type;
   x_party_site_id          NUMBER;
   x_party_site_number      VARCHAR2(2000);

   --cust account site api variable
   p_cust_acct_site_rec     hz_cust_account_site_v2pub.cust_acct_site_rec_type;
   x_cust_acct_site_id      NUMBER;

   --cust account site use api variable
   p_cust_site_use_rec      HZ_CUST_ACCOUNT_SITE_V2PUB.CUST_SITE_USE_REC_TYPE;
   p_customer_profile_rec   HZ_CUSTOMER_PROFILE_V2PUB.CUSTOMER_PROFILE_REC_TYPE;
   x_party_site_use_id      NUMBER;
   x_site_use_id            NUMBER;
   l_user_name              apps.fnd_user.user_name%TYPE;--TMS#20170313-00308 
   l_created_by_module      HZ_CUST_ACCOUNTS.CREATED_BY_MODULE%TYPE := 'ONT_UI_ADD_CUSTOMER';
   l_responsibility_key     FND_RESPONSIBILITY_VL.RESPONSIBILITY_KEY%TYPE := 'XXWC_ORDER_MGMT_PRICING_FULL';
BEGIN
      l_sec := 'Derive Customer Party Id';

      -- Setting the Context --
      MO_GLOBAL.INIT('AR');
      L_USER_NAME := upper(p_xxwc_address_rec.user_ntid);
      
       BEGIN 
           SELECT USER_ID
             INTO l_user_id
            FROM APPS.FND_USER
           WHERE user_name = L_USER_NAME;
        EXCEPTION
        WHEN OTHERS THEN
           l_user_id := NULL;
        END; 

     IF IS_VALID_OM_USER (L_USER_ID ) = 'Y' THEN --TMS#20170313-00308 
      NULL;
      -- l_user_id := p_xxwc_address_rec.user_id;     
     ELSE
        --TMS#20170313-00308 Start
        BEGIN 
           SELECT USER_NAME    
             INTO l_user_name
            FROM apps.fnd_user
           WHERE user_id = p_xxwc_address_rec.user_ntid;
        EXCEPTION
        WHEN OTHERS THEN
           l_user_name := NULL;
        END; 
        o_status  := 'E';
        o_message := 'Responsibility not assigned to NTID : ' ||l_user_name||' to create/ update sales order ';
        RETURN;
        --TMS#20170313-00308 End
     END IF;
   
     BEGIN 
         SELECT responsibility_id,application_id 
         INTO l_resp_id,l_resp_appl_id
         FROM fnd_responsibility_vl 
         WHERE responsibility_key = l_responsibility_key;
      EXCEPTION
      WHEN OTHERS THEN
         o_status  := 'E';
         o_message := 'Responsibility '||l_responsibility_key||' does not exist in ERP';
         RETURN;
      END;

      mo_global.set_org_context ( 162, NULL, 'AR' ) ;
  
      --INITIALIZE ENVIRONMENT
      fnd_global.apps_initialize (l_user_id, l_resp_id, l_resp_appl_id);

      mo_global.set_policy_context('S',162);
      fnd_global.set_nls_context('AMERICAN');
      --debug info
      IF NVL(g_xxwc_woc_debug,'N') = 'Y' THEN
         xxwc_checkitapp_pkg.xxwc_log_debug_info
                    (P_API_NAME         =>l_proc_name,
                     P_CUSTOMER_ID      => p_xxwc_address_rec.customer_id,
                     P_DEBUG_TEXT       => 'Start CREATE_JOB_SITE',
                     P_USER_ID          => l_user_id
                    );
      END IF;
      ------------------------------------
      -- Derive Customer Party Id
      ------------------------------------
      BEGIN
        SELECT party_id
          INTO l_party_id
          FROM hz_cust_accounts_all hca
         WHERE cust_account_id = p_xxwc_address_rec.customer_id;
      EXCEPTION
      WHEN OTHERS THEN
         o_message := 'INVALID CUSTOMER '||' p_xxwc_address_rec.customer_id '||x_msg_data;
         RAISE l_exception;
      END;

      l_sec := 'Create a location';

      ------------------------------------
      -- 1. Create a location
      ------------------------------------
       p_location_rec.country           := p_xxwc_address_rec.country;--'US';
       p_location_rec.address1          := p_xxwc_address_rec.address1;--'Shareoracleapps';
       p_location_rec.address2          := p_xxwc_address_rec.address2;
       p_location_rec.address3          := p_xxwc_address_rec.address3;
       p_location_rec.address4          := p_xxwc_address_rec.address4;
       p_location_rec.city              := p_xxwc_address_rec.city;--san Mateo';
       p_location_rec.county            := p_xxwc_address_rec.county;--san Mateo';
       p_location_rec.postal_code       := p_xxwc_address_rec.postal_code;--94401';
       p_location_rec.state             := p_xxwc_address_rec.state;--'CA';
       p_location_rec.attribute_category :='No';
       p_location_rec.attribute1        := '1';
       p_location_rec.attribute3        := 'N';
       p_location_rec.created_by_module := NVL(p_created_by_module,l_created_by_module);
       --debug info
      IF NVL(g_xxwc_woc_debug,'N') = 'Y' THEN
         xxwc_checkitapp_pkg.xxwc_log_debug_info
                    (P_API_NAME         =>l_proc_name,
                     P_CUSTOMER_ID      => p_xxwc_address_rec.customer_id,
                     P_DEBUG_TEXT       => 'Start HZ_LOCATION_V2PUB.CREATE_LOCATION',
                     P_USER_ID          => l_user_id
                    );
      END IF;
       HZ_LOCATION_V2PUB.CREATE_LOCATION
           (
             p_init_msg_list => FND_API.G_TRUE,
             p_location_rec  => p_location_rec,
             x_location_id   => x_location_id,
             x_return_status => x_return_status,
             x_msg_count     => x_msg_count,
             x_msg_data      => x_msg_data);

      --debug info
      IF NVL(g_xxwc_woc_debug,'N') = 'Y' THEN
         xxwc_checkitapp_pkg.xxwc_log_debug_info
                    (P_API_NAME         =>l_proc_name,
                     P_CUSTOMER_ID      => p_xxwc_address_rec.customer_id,
                     P_DEBUG_TEXT       => 'End HZ_LOCATION_V2PUB.CREATE_LOCATION',
                     P_USER_ID          => l_user_id
                    );
      END IF;
      IF x_return_status != 'S'
      THEN
         o_message := 'CREATE_LOCATION'||x_msg_data;
         RAISE l_exception;
      END IF;

      l_sec := 'Create a party site';
      ------------------------------------
      -- 2. Create party site
      ------------------------------------
       -- Initializing the Mandatory API parameters
       p_party_site_rec.party_id                 := l_party_id;
       p_party_site_rec.location_id              := x_location_id;
       p_party_site_rec.identifying_address_flag := 'Y';
       --p_party_site_rec.party_site_name          := p_xxwc_address_rec.location;
       p_party_site_rec.created_by_module        := NVL(p_created_by_module,l_created_by_module);

       l_sec:= 'Calling the API hz_party_site_v2pub.create_party_site';
       --debug info
      IF NVL(g_xxwc_woc_debug,'N') = 'Y' THEN
         xxwc_checkitapp_pkg.xxwc_log_debug_info
                    (P_API_NAME         =>l_proc_name,
                     P_CUSTOMER_ID      => p_xxwc_address_rec.customer_id,
                     P_DEBUG_TEXT       => 'Start HZ_PARTY_SITE_V2PUB.CREATE_PARTY_SITE',
                     P_USER_ID          => l_user_id
                    );
      END IF;
       HZ_PARTY_SITE_V2PUB.CREATE_PARTY_SITE
                   (
                    p_init_msg_list     => FND_API.G_TRUE,
                    p_party_site_rec    => p_party_site_rec,
                    x_party_site_id     => x_party_site_id,
                    x_party_site_number => x_party_site_number,
                    x_return_status     => x_return_status,
                    x_msg_count         => x_msg_count,
                    x_msg_data          => x_msg_data
                           );
      IF x_return_status != 'S'
      THEN
         o_message := 'CREATE_PARTY_SITE'||x_msg_data;
         RAISE l_exception;
      END IF;
      --debug info
      IF NVL(g_xxwc_woc_debug,'N') = 'Y' THEN
         xxwc_checkitapp_pkg.xxwc_log_debug_info
                    (P_API_NAME         =>l_proc_name,
                     P_CUSTOMER_ID      => p_xxwc_address_rec.customer_id,
                     P_DEBUG_TEXT       => 'End HZ_PARTY_SITE_V2PUB.CREATE_PARTY_SITE',
                     P_USER_ID          => l_user_id
                    );
      END IF;
      l_sec := 'Create Cust acct site';
      ------------------------------------
      -- 4. Cust acct site use
      ------------------------------------
      p_cust_acct_site_rec.cust_account_id   := p_xxwc_address_rec.customer_id;
      p_cust_acct_site_rec.party_site_id     := x_party_site_id;
      p_cust_acct_site_rec.attribute_category := 'No';
      p_cust_acct_site_rec.attribute1        := '1';
      p_cust_acct_site_rec.attribute3        := 'N';
      p_cust_acct_site_rec.created_by_module := NVL(p_created_by_module,l_created_by_module);

      l_sec:= 'Calling the API hz_cust_account_site_v2pub.create_cust_acct_site';
      --debug info
      IF NVL(g_xxwc_woc_debug,'N') = 'Y' THEN
         xxwc_checkitapp_pkg.xxwc_log_debug_info
               (P_API_NAME         =>l_proc_name,
                P_CUSTOMER_ID      => p_xxwc_address_rec.customer_id,
                P_DEBUG_TEXT       => 'Start HZ_CUST_ACCOUNT_SITE_V2PUB.CREATE_CUST_ACCT_SITE',
                P_USER_ID          => l_user_id
               );
      END IF;
      hz_cust_account_site_v2pub.create_cust_acct_site (    'T'                   ,
                                                                p_cust_acct_site_rec  ,
                                                                x_cust_acct_site_id   ,
                                                                x_return_status       ,
                                                                x_msg_count           ,
                                                                x_msg_data
                                                            ) ;
     IF x_return_status != 'S'
     THEN
       o_message := 'CREATE_CUST_ACCT_SITE'||x_msg_data;
       RAISE l_exception;
     END IF;
     --debug info
      IF NVL(g_xxwc_woc_debug,'N') = 'Y' THEN
         xxwc_checkitapp_pkg.xxwc_log_debug_info
               (P_API_NAME         =>l_proc_name,
                P_CUSTOMER_ID      => p_xxwc_address_rec.customer_id,
                P_JOB_SITE_ID      => x_cust_acct_site_id,
                P_DEBUG_TEXT       => 'End HZ_CUST_ACCOUNT_SITE_V2PUB.CREATE_CUST_ACCT_SITE',
                P_USER_ID          => l_user_id
               );
      END IF;
     IF p_xxwc_address_rec.ship_to_flag = 'Y' OR p_xxwc_address_rec.bill_to_flag = 'Y' THEN
        BEGIN
           SELECT salesrep_id
             INTO p_cust_site_use_rec.primary_salesrep_id
             FROM apps.jtf_rs_salesreps jrs, apps.fnd_user fu-- jtf_rs_salesreps_mo_v
            WHERE jrs.person_id = fu.employee_id
              AND fu.user_name = l_user_name;
        EXCEPTION
        WHEN OTHERS THEN
           p_cust_site_use_rec.primary_salesrep_id := NULL;
        END;
     END IF;   
     --debug info
      IF NVL(g_xxwc_woc_debug,'N') = 'Y' THEN
         xxwc_checkitapp_pkg.xxwc_log_debug_info
               (P_API_NAME         =>l_proc_name,
                P_CUSTOMER_ID      => p_xxwc_address_rec.customer_id,
                P_JOB_SITE_ID      => x_cust_acct_site_id,
                P_DEBUG_TEXT       => 'Start HZ_CUST_ACCOUNT_SITE_V2PUB.CREATE_CUST_SITE_USE',
                P_USER_ID          => l_user_id
               );
      END IF;
     IF p_xxwc_address_rec.ship_to_flag = 'Y' THEN
        p_cust_site_use_rec.cust_acct_site_id := x_cust_acct_site_id;
        p_cust_site_use_rec.site_use_code     := 'SHIP_TO';
       -- p_cust_site_use_rec.primary_flag      := 'Y';
        p_cust_site_use_rec.location          :=  p_xxwc_address_rec.location;
        p_cust_site_use_rec.attribute1        := 'JOB';
        p_cust_site_use_rec.attribute2        := 'N';
        p_cust_site_use_rec.created_by_module := p_created_by_module;

        l_sec:= 'Calling the API hz_party_site_v2pub.Create_party_site_use';

         hz_cust_account_site_v2pub.create_cust_site_use (  p_init_msg_list          =>   'T'                     ,
                                                            p_cust_site_use_rec      =>   p_cust_site_use_rec     ,
                                                            p_customer_profile_rec   =>   p_customer_profile_rec  ,
                                                            p_create_profile         =>    fnd_api.g_false         ,
                                                            p_create_profile_amt     =>   fnd_api.g_false         ,
                                                            x_site_use_id            =>   x_site_use_id           ,
                                                            x_return_status          =>   x_return_status         ,
                                                            x_msg_count              =>   x_msg_count             ,
                                                            x_msg_data               =>   x_msg_data
                                                         );
        IF x_return_status != 'S'
        THEN
          o_message := 'CREATE_CUST_SITE_USE'||x_msg_data;
          RAISE l_exception;
        END IF;

        IF x_return_status = 'S' THEN
           COMMIT;
           o_status          := x_return_status;
           o_ship_to_site_use_id := x_site_use_id;
        END IF;

     END IF; --SHIP_TO

     IF p_xxwc_address_rec.bill_to_flag = 'Y' THEN
        p_cust_site_use_rec.cust_acct_site_id := x_cust_acct_site_id;
        p_cust_site_use_rec.site_use_code     := 'BILL_TO';
        p_cust_site_use_rec.location          :=  p_xxwc_address_rec.location;
        p_cust_site_use_rec.attribute1        := 'JOB';
        p_cust_site_use_rec.attribute2        := 'N';
        p_cust_site_use_rec.created_by_module := NVL(p_created_by_module,l_created_by_module);

        l_sec:= 'Calling the API hz_party_site_v2pub.Create_party_site_use';

         hz_cust_account_site_v2pub.create_cust_site_use (  p_init_msg_list          =>   'T'                     ,
                                                            p_cust_site_use_rec      =>   p_cust_site_use_rec     ,
                                                            p_customer_profile_rec   =>   p_customer_profile_rec  ,
                                                            p_create_profile         =>   FND_API.G_TRUE          ,
                                                            p_create_profile_amt     =>   FND_API.G_TRUE          ,
                                                            x_site_use_id            =>   x_site_use_id           ,
                                                            x_return_status          =>   x_return_status         ,
                                                            x_msg_count              =>   x_msg_count             ,
                                                            x_msg_data               =>   x_msg_data
                                                         );
        IF x_return_status != 'S'
        THEN
          o_message := 'CREATE_CUST_SITE_USE'||x_msg_data;
          RAISE l_exception;
        END IF;

        IF x_return_status = 'S' THEN
           COMMIT;
           o_status          := x_return_status;
           o_bill_to_site_use_id := x_site_use_id;
        END IF;

     END IF; --BILL_TO
     --debug info
      IF NVL(g_xxwc_woc_debug,'N') = 'Y' THEN
         xxwc_checkitapp_pkg.xxwc_log_debug_info
               (P_API_NAME         =>l_proc_name,
                P_CUSTOMER_ID      => p_xxwc_address_rec.customer_id,
                P_JOB_SITE_ID      => x_cust_acct_site_id,
                P_DEBUG_TEXT       => 'End HZ_CUST_ACCOUNT_SITE_V2PUB.CREATE_CUST_SITE_USE',
                P_USER_ID          => l_user_id
               );
      END IF;
      --debug info
      IF NVL(g_xxwc_woc_debug,'N') = 'Y' THEN
         xxwc_checkitapp_pkg.xxwc_log_debug_info
               (P_API_NAME         =>l_proc_name,
                P_CUSTOMER_ID      => p_xxwc_address_rec.customer_id,
                P_JOB_SITE_ID      => x_cust_acct_site_id,
                P_DEBUG_TEXT       => 'End CREATE_JOB_SITE',
                P_USER_ID          => l_user_id
               );
      END IF;
   EXCEPTION
      WHEN l_exception
      THEN
         o_status          := 'E';
         /*fnd_file.put_line (
            fnd_file.LOG,
               'customer_id - '
            || p_xxwc_address_rec.customer_id
            || ' in L_EXCEPTION, p_error_message - '
            || l_error_message);*/
      WHEN OTHERS
      THEN
         ROLLBACK;
         o_ship_to_site_use_id := NULL;
         o_bill_to_site_use_id := NULL;
         o_status          := 'E';
         o_message         :=  'customer_id - '
            || p_xxwc_address_rec.customer_id
            || ' error in CREATE_JOB_SITE procedure - '
            || SUBSTR(SQLERRM,1,1000);
         /*DBMS_OUTPUT.put_line (
               'customer_id - '
            || p_xxwc_address_rec.customer_id
            || ' error in CREATE_CONTACT procedure - '
            || SUBSTR(SQLERRM,1,1000));*/
   END CREATE_JOB_SITE;
   
   /*************************************************************************
      PROCEDURE Name: create_contact

      PURPOSE:   To create contact when contact does not exist in Oracle

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------
      1.0        11/14/2017  Niraj K Ranjan             Initial Version
   ****************************************************************************/
   PROCEDURE create_contact (p_customer_id       IN     NUMBER,
                             p_first_name        IN     VARCHAR2,
                             p_last_name         IN     VARCHAR2,
                             p_contact_num       IN     VARCHAR2,
                             p_email_id          IN     VARCHAR2,
                             p_fax_num           IN     VARCHAR2,
                             p_created_by_module IN     VARCHAR2,
                             p_cust_contact_id   OUT    NUMBER,
                             p_error_code        OUT    VARCHAR2,
                             p_error_message     OUT    VARCHAR2)
   IS
      l_party_id               NUMBER;
      p_create_person_rec      HZ_PARTY_V2PUB.person_rec_type;
      x_party_id               NUMBER;
      x_party_number           VARCHAR2 (2000);
      x_profile_id             NUMBER;
      x_return_status          VARCHAR2 (2000);
      x_msg_count              NUMBER;
      x_msg_data               VARCHAR2 (2000);

      p_org_contact_rec        HZ_PARTY_CONTACT_V2PUB.ORG_CONTACT_REC_TYPE;
      x_org_contact_id         NUMBER;
      x_party_rel_id           NUMBER;
      x_party_id2              NUMBER;
      x_party_number2          VARCHAR2 (2000);

      p_cr_cust_acc_role_rec   HZ_CUST_ACCOUNT_ROLE_V2PUB.cust_account_role_rec_type;
      x_cust_account_role_id   NUMBER;
      l_exception              EXCEPTION;

      p_contact_point_rec      HZ_CONTACT_POINT_V2PUB.CONTACT_POINT_REC_TYPE;

      p_edi_rec                HZ_CONTACT_POINT_V2PUB.EDI_REC_TYPE;
      p_email_rec              HZ_CONTACT_POINT_V2PUB.EMAIL_REC_TYPE;
      p_phone_rec              HZ_CONTACT_POINT_V2PUB.PHONE_REC_TYPE;
      p_telex_rec              HZ_CONTACT_POINT_V2PUB.TELEX_REC_TYPE;
      p_web_rec                HZ_CONTACT_POINT_V2PUB.WEB_REC_TYPE;

      x_contact_point_id       NUMBER;
      l_sec                    VARCHAR2 (100);
   BEGIN
      p_error_code := '';
      l_sec := 'Derive Customer Party Id';

      ------------------------------------
      --0. Derive Customer Party Id
      ------------------------------------
      SELECT party_id
        INTO l_party_id
        FROM hz_cust_accounts_all hca
       WHERE cust_account_id = p_customer_id;

      l_sec := 'Create a definition contact';

      ------------------------------------
      -- 1. Create a definition contact
      ------------------------------------
      IF p_first_name IS NULL OR p_last_name IS NULL
      THEN
         p_error_message := 'First Name or Last Name is empty';
         RAISE l_exception;
      ELSE
         p_create_person_rec.person_first_name := p_first_name;
         p_create_person_rec.person_last_name := p_last_name;
      END IF;

      p_create_person_rec.created_by_module := p_created_by_module;

      HZ_PARTY_V2PUB.create_person ('T',
                                    p_create_person_rec,
                                    x_party_id,
                                    x_party_number,
                                    x_profile_id,
                                    x_return_status,
                                    x_msg_count,
                                    x_msg_data);

      IF x_return_status != 'S'
      THEN
         p_error_message := x_msg_data;
         RAISE l_exception;
      END IF;

      l_sec := 'Create a relation cont-org using party_id from step 1';
      ------------------------------------
      -- 2. Create a relation cont-org using party_id from step 1
      ------------------------------------
      p_org_contact_rec.created_by_module := p_created_by_module;
      p_org_contact_rec.party_rel_rec.subject_id := x_party_id; --<<value for party_id from step 1>
      p_org_contact_rec.party_rel_rec.subject_type := 'PERSON';
      p_org_contact_rec.party_rel_rec.subject_table_name := 'HZ_PARTIES';
      p_org_contact_rec.party_rel_rec.object_id := l_party_id; --<<value for party_id from step 0>
      p_org_contact_rec.party_rel_rec.object_type := 'ORGANIZATION';
      p_org_contact_rec.party_rel_rec.object_table_name := 'HZ_PARTIES';
      p_org_contact_rec.party_rel_rec.relationship_code := 'CONTACT_OF';
      p_org_contact_rec.party_rel_rec.relationship_type := 'CONTACT';
      p_org_contact_rec.party_rel_rec.start_date := SYSDATE;
      --         p_org_contact_rec.contact_number                   := p_contact_num;

      hz_party_contact_v2pub.create_org_contact ('T',
                                                 p_org_contact_rec,
                                                 x_org_contact_id,
                                                 x_party_rel_id,
                                                 x_party_id2,
                                                 x_party_number,
                                                 x_return_status,
                                                 x_msg_count,
                                                 x_msg_data);

      IF x_return_status != 'S'
      THEN
         p_error_message := x_msg_data;
         RAISE l_exception;
      END IF;

      IF p_contact_num IS NOT NULL
      THEN
         l_sec := 'Create a Phone Contact Point using party_id';
         ------------------------------------------------------------------------------------------------------------
         -- 3.1 Create a Phone Contact Point using party_id
         ------------------------------------------------------------------------------------------------------------
         -- Initializing the Mandatory API parameters
         p_contact_point_rec.contact_point_type := 'PHONE';
         p_contact_point_rec.owner_table_name := 'HZ_PARTIES';
         p_contact_point_rec.owner_table_id := x_party_id2;      --l_party_id;
         p_contact_point_rec.primary_flag := 'Y';
         p_contact_point_rec.contact_point_purpose := 'BUSINESS';
         p_contact_point_rec.created_by_module := p_created_by_module;

         p_phone_rec.phone_area_code := NULL;
         p_phone_rec.phone_country_code := '1';
         p_phone_rec.phone_number := p_contact_num;
         p_phone_rec.phone_line_type := 'MOBILE';

         fnd_file.put_line (
            fnd_file.LOG,
            'Calling the API hz_contact_point_v2pub.create_contact_point');

         HZ_CONTACT_POINT_V2PUB.CREATE_CONTACT_POINT (
            p_init_msg_list       => FND_API.G_TRUE,
            p_contact_point_rec   => p_contact_point_rec,
            p_edi_rec             => p_edi_rec,
            p_email_rec           => p_email_rec,
            p_phone_rec           => p_phone_rec,
            p_telex_rec           => p_telex_rec,
            p_web_rec             => p_web_rec,
            x_contact_point_id    => x_contact_point_id,
            x_return_status       => x_return_status,
            x_msg_count           => x_msg_count,
            x_msg_data            => x_msg_data);

         IF x_return_status != 'S'
         THEN
            p_error_message := x_msg_data;
            RAISE l_exception;
         END IF;
      END IF;                             -- IF p_contact_num IS NOT NULL THEN

      l_sec := 'Create a Email Contact Point using party_id';

      ------------------------------------------------------------------------------------------------------------
      -- 3.2 Create a Email Contact Point using party_id
      ------------------------------------------------------------------------------------------------------------
      IF p_email_id IS NOT NULL
      THEN
         -- Initializing the Mandatory API parameters
         p_contact_point_rec.contact_point_type := 'EMAIL';
         p_contact_point_rec.owner_table_name := 'HZ_PARTIES';
         p_contact_point_rec.owner_table_id := x_party_id2;      --l_party_id;
         p_contact_point_rec.primary_flag := 'Y';
         p_contact_point_rec.contact_point_purpose := 'BUSINESS';
         p_contact_point_rec.created_by_module := p_created_by_module;

         p_email_rec.email_format := 'MAILHTML';
         p_email_rec.email_address := p_email_id;

         fnd_file.put_line (
            fnd_file.LOG,
            'Calling the API hz_contact_point_v2pub.create_contact_point');

         HZ_CONTACT_POINT_V2PUB.CREATE_CONTACT_POINT (
            p_init_msg_list       => FND_API.G_TRUE,
            p_contact_point_rec   => p_contact_point_rec,
            p_edi_rec             => p_edi_rec,
            p_email_rec           => p_email_rec,
            p_phone_rec           => p_phone_rec,
            p_telex_rec           => p_telex_rec,
            p_web_rec             => p_web_rec,
            x_contact_point_id    => x_contact_point_id,
            x_return_status       => x_return_status,
            x_msg_count           => x_msg_count,
            x_msg_data            => x_msg_data);

         IF x_return_status != 'S'
         THEN
            p_error_message := x_msg_data;
            RAISE l_exception;
         END IF;
      END IF;                                -- IF p_email_id IS NOT NULL THEN

      l_sec := 'Create a contact using party_id you get in step 3';
      ------------------------------------------------------------------------------------------------------------
      -- 4. Create a contact using party_id you get in step 3
      ------------------------------------------------------------------------------------------------------------
      p_cr_cust_acc_role_rec.party_id := x_party_id2; --<<value for party_id from step 2>
      p_cr_cust_acc_role_rec.cust_account_id := p_customer_id; --<<value for cust_account_id from step 0>
      p_cr_cust_acc_role_rec.role_type := 'CONTACT';
      p_cr_cust_acc_role_rec.created_by_module := p_created_by_module;

      HZ_CUST_ACCOUNT_ROLE_V2PUB.CREATE_CUST_ACCOUNT_ROLE (
         'T',
         p_cr_cust_acc_role_rec,
         x_cust_account_role_id,
         x_return_status,
         x_msg_count,
         x_msg_data);

      IF x_return_status != 'S'
      THEN
         p_error_message := x_msg_data;
         RAISE l_exception;
      ELSE
         p_cust_contact_id := x_cust_account_role_id;
         COMMIT;
      END IF;
      p_error_code := 'S';
   EXCEPTION
      WHEN l_exception
      THEN
         p_error_code := 'E';
      WHEN OTHERS
      THEN
         ROLLBACK;
         p_cust_contact_id := NULL;
         p_error_code := 'E';
         p_error_message := 'p_customer_id - '|| 
                                 p_customer_id|| 
                            ' error in  CREATE_CONTACT procedure - '||SQLERRM;
   END create_contact;
   
   /*************************************************************************
   *   $Header xxwc_ar_credit_profiles_pkg.pks $
   *   Module Name: xxwc update credit profiles
   *
   *   PURPOSE:   Procedure used to update profile classes and customer account
   *              for cash on the fly customers
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        11/17/2017  Niraj K Ranjan             Initial Version
   *   
   * ***************************************************************************/

   PROCEDURE update_customer_profile (
      errbuf                     OUT VARCHAR2,
      retcode                    OUT VARCHAR2,
      p_cust_account_id          IN  VARCHAR2,
      p_profile_class_id         IN   NUMBER,
      p_customer_class_code      IN   VARCHAR2,
      p_remit_to_code            IN   VARCHAR2)
   IS
      p_customer_profile_rec          HZ_CUSTOMER_PROFILE_V2PUB.customer_profile_rec_type;
      x_cust_account_profile_id       NUMBER;
      x_return_status                 VARCHAR2 (2000);
      x_msg_count                     NUMBER;
      x_msg_data                      VARCHAR2 (2000);
      p_obj_version_number            NUMBER;
      p_cust_account_rec              HZ_CUST_ACCOUNT_V2PUB.CUST_ACCOUNT_REC_TYPE;
      x_profile_class_id              NUMBER;
      l_account_name                  VARCHAR2 (2000);
      l_location_name                 VARCHAR2 (2000);
      l_party_name                    VARCHAR2 (2000);
      p_cust_site_use_rec             HZ_CUST_ACCOUNT_SITE_V2PUB.cust_site_use_rec_type;
      p_organization_rec              HZ_PARTY_V2PUB.ORGANIZATION_REC_TYPE;
      p_party_rec                     HZ_PARTY_V2PUB.PARTY_REC_TYPE;
      l_object_version_number         NUMBER;
      l_party_object_version_number   NUMBER;
      l_profile_id                    NUMBER;
      l_org_id                        NUMBER;
      l_sec                        VARCHAR2 (200);
      l_msg                         VARCHAR2 (150);
      l_distro_list                 VARCHAR2 (75)   DEFAULT 'HDSOracleDevelopers@hdsupply.com';
      l_req_id                        NUMBER := fnd_global.conc_request_id;

      CURSOR c1
      IS
         SELECT a.cust_account_profile_id,
                a.object_Version_number,
                b.account_number,
                a.standard_terms
           FROM hz_customer_profiles a, hz_cust_accounts b
          WHERE     a.profile_class_id = 0
                AND a.created_by_module = 'ONT_UI_ADD_CUSTOMER'
                AND a.cust_account_id = b.cust_account_id
                AND b.cust_account_id = p_cust_account_id;


      CURSOR c2
      IS
         SELECT b.cust_account_id,
                b.account_number,
                b.creation_date,
                b.object_version_number,
                a.party_name,
                a.party_id,
                a.object_version_number party_object_version_number
           FROM hz_cust_accounts b, hz_parties a
          WHERE     b.created_by_module = 'ONT_UI_ADD_CUSTOMER'
                AND b.account_name IS NULL
                AND b.attribute4 IS NULL
                AND b.account_established_date IS NULL
                AND a.party_id = b.party_id
                AND b.cust_account_id = p_cust_account_id;

      CURSOR c3
      IS
         SELECT a.cust_account_profile_id,
                a.object_Version_number,
                b.account_number,
                a.standard_terms  -- Added for ver#1.4
           FROM hz_customer_profiles a, hz_cust_accounts b
          WHERE     a.profile_class_id = NVL (p_profile_class_id, 1040)
                AND a.credit_classification <> 'CSALE'
                AND a.cust_account_id = b.cust_account_id
                AND b.cust_account_id = p_cust_account_id;


      CURSOR c4 (
         p_account_id    NUMBER)                           --Added for ver#1.2
      IS
         SELECT hcsu.object_version_number,
                hcsu.site_use_id,
                hcsu.cust_acct_site_id,
                hcsu.location,
                hcsu.primary_salesrep_id
           FROM hz_cust_acct_sites hcas, hz_cust_site_uses hcsu
          WHERE     hcas.cust_acct_site_id = hcsu.cust_acct_site_id
                AND NVL (hcas.status, 'A') = 'A'
                AND NVL (hcsu.status, 'A') = 'A'
                AND hcas.cust_account_id = p_account_id;
   BEGIN

   /*SELECT   mo_global.get_current_org_id () INTO l_org_id FROM DUAL;

   fnd_file.put_line (
               fnd_file.LOG,
                  'Current MO Org_id='
               || l_org_id);

      --Initialize the apps
      --mo_global.init ('AR');
      mo_global.set_policy_context ('S', l_org_id);
      fnd_global.apps_initialize (fnd_global.user_id,
                                  fnd_global.resp_id,
                                  fnd_global.resp_appl_id,
                                  0);*/
      errbuf  := '';
      retcode := 'S';
      l_sec := 'Opening Cursor C1';
      FOR c1_rec IN c1
      LOOP
         p_customer_profile_rec.cust_account_profile_id :=
            c1_rec.cust_account_profile_id;
         p_customer_profile_rec.profile_class_id := p_profile_class_id;
         p_customer_profile_rec.attribute2 := p_remit_to_code;          --'2';
         p_customer_profile_rec.attribute3 := 'N';
         p_customer_profile_rec.account_status := 'ACTIVE';
         p_obj_version_number := c1_rec.object_version_number;

         IF c1_rec.standard_terms = 1019 --PFRDCASH                                        --Added for ver#1.4 <<START>>
         THEN
         p_customer_profile_rec.standard_terms := c1_rec.standard_terms;
         END IF;                                                     -- <<END>>

        BEGIN
         HZ_CUSTOMER_PROFILE_V2PUB.update_customer_profile (
            p_init_msg_list           => 'T',
            p_customer_profile_rec    => p_customer_profile_rec,
            p_object_version_number   => p_obj_version_number,
            x_return_status           => x_return_status,
            x_msg_count               => x_msg_count,
            x_msg_data                => x_msg_data);

            p_customer_profile_rec.standard_terms :=NULL;  --Added for ver#1.4

         IF x_return_status <> 'S'
         THEN
            retcode := 'E';
            errbuf :=
                  'Issue updating Profile for customer '
               || c1_rec.account_number
               || ' '
               || x_msg_data;
         END IF;
         EXCEPTION
            WHEN OTHERS
            THEN
               retcode := 'E'; 
               errbuf := 'Error while updating Profile: ' || SQLERRM;
         END;
      END LOOP;                                                           --C1

      l_sec := 'Opening Cursor C2';
      FOR c2_rec IN c2
      LOOP
         p_cust_account_rec.cust_account_id := c2_rec.cust_account_id;
         p_cust_account_rec.account_established_date := c2_rec.creation_date;

         --Added below for ver#1.2 <<Start>>
         IF    SUBSTR (UPPER (c2_rec.party_name), 1, 5) NOT LIKE 'CASH/%'
            OR SUBSTR (UPPER (c2_rec.party_name), 1, 6) NOT LIKE 'CASH /%'
         THEN
            SELECT REPLACE (
                      REPLACE (
                            'CASH/'
                         || TRIM (
                               REPLACE (
                                  REGEXP_REPLACE (UPPER (c2_rec.party_name),
                                                  '[/,%,;,*,?,\,]',
                                                  ' '),
                                  'CASH',
                                  '')),
                         '/.',
                         '/'),
                      '/-',
                      '/')
              INTO l_account_name
              FROM DUAL;

            p_cust_account_rec.account_name := l_account_name;
         ELSE
            p_cust_account_rec.account_name := c2_rec.party_name;
         END IF;

         --         p_cust_account_rec.account_name := c2_rec.party_name;


         p_cust_account_rec.attribute4 := 'WCI';
         p_cust_account_rec.customer_class_code := p_customer_class_code; --'GEN_CONTRACT';

         BEGIN
         HZ_CUST_ACCOUNT_V2PUB.update_cust_account (
            p_init_msg_list           => 'T',
            p_cust_account_rec        => p_cust_account_rec,
            p_object_version_number   => c2_rec.object_version_number,
            x_return_status           => x_return_status,
            x_msg_count               => x_msg_count,
            x_msg_data                => x_msg_data);

         IF x_return_status <> 'S'
         THEN
            retcode := 'E'; 
            errbuf :=
                  'Issue updating Account for customer '
               || c2_rec.account_number
               || ' '
               || x_msg_data;
         END IF;
         EXCEPTION
            WHEN OTHERS
            THEN
               retcode := 'E'; 
               errbuf := 'Error while updating Account: ' || SQLERRM;

         END;

         BEGIN
            p_party_rec.party_id := c2_rec.party_id;
            p_organization_rec.party_rec := p_party_rec;
            l_party_object_version_number :=
               c2_rec.party_object_version_number;

            IF    SUBSTR (UPPER (c2_rec.party_name), 1, 5) NOT LIKE 'CASH/%'
               OR SUBSTR (UPPER (c2_rec.party_name), 1, 6) NOT LIKE 'CASH /%'
            THEN
               SELECT REPLACE (
                         REPLACE (
                               'CASH/'
                            || TRIM (
                                  REPLACE (
                                     REGEXP_REPLACE (
                                        UPPER (c2_rec.party_name),
                                        '[/,%,;,*,?,\,]',
                                        ' '),
                                     'CASH',
                                     '')),
                            '/.',
                            '/'),
                         '/-',
                         '/')
                 INTO l_party_name
                 FROM DUAL;

               p_organization_rec.organization_name := l_party_name;
            ELSE
               p_organization_rec.organization_name := l_party_name;
            END IF;

            BEGIN
            HZ_PARTY_V2PUB.update_organization (
               p_init_msg_list                 => FND_API.G_FALSE,
               p_organization_rec              => p_organization_rec,
               p_party_object_version_number   => l_party_object_version_number,
               x_profile_id                    => l_profile_id,
               x_return_status                 => x_return_status,
               x_msg_count                     => x_msg_count,
               x_msg_data                      => x_msg_data);


            IF x_return_status <> 'S'
            THEN
               retcode := 'E'; 
               errbuf := 
                     'Issue updating Organization name '
                  || c2_rec.party_name
                  || ' '
                  || x_msg_data;
            END IF;
           EXCEPTION
            WHEN OTHERS
            THEN
               retcode := 'E'; 
               errbuf := 'Error while updating Organization: ' || SQLERRM;
         END;

         END;

         l_sec := 'Opening Cursor C4';

         FOR c4_rec IN c4 (c2_rec.cust_account_id) --Added for ver#1.2 <<Start>>
         LOOP
            BEGIN
               p_cust_site_use_rec.site_use_id := c4_rec.site_use_id;
               p_cust_site_use_rec.cust_acct_site_id :=
                  c4_rec.cust_acct_site_id;
               --p_cust_site_use_rec.location       := c4_rec.location;
               l_object_version_number := c4_rec.object_version_number;

               IF    SUBSTR (UPPER (c4_rec.location), 1, 5) NOT LIKE 'CASH/%'
                  OR SUBSTR (UPPER (c4_rec.location), 1, 6) NOT LIKE
                        'CASH /%'
               THEN
                  SELECT SUBSTR(REPLACE (
                            REPLACE (
                                  'CASH/'
                               || TRIM (
                                     REPLACE (
                                        REGEXP_REPLACE (
                                           UPPER (c4_rec.location),
                                           '[/,%,;,*,?,\,]',
                                           ' '),
                                        'CASH',
                                        '')),
                               '/.',
                               '/'),
                            '/-',
                            '/'),1,40)  -- Added for Ver# 1.3
                    INTO l_location_name
                    FROM DUAL;

                  p_cust_site_use_rec.location := l_location_name;
               ELSE
                  p_cust_site_use_rec.location := c4_rec.location;
               END IF;

               BEGIN
               HZ_CUST_ACCOUNT_SITE_V2PUB.update_cust_site_use (
                  p_init_msg_list           => FND_API.G_FALSE,
                  p_cust_site_use_rec       => p_cust_site_use_rec,
                  p_object_version_number   => l_object_version_number,
                  x_return_status           => x_return_status,
                  x_msg_count               => x_msg_count,
                  x_msg_data                => x_msg_data);

               IF x_return_status <> 'S'
               THEN
                  retcode := 'E'; 
                  errbuf:= 
                        'Issue updating location '
                     || c4_rec.location
                     || ' '
                     || x_msg_data;
               END IF;
            EXCEPTION
            WHEN OTHERS THEN
               retcode := 'E'; 
               errbuf := 'Error while updating location: ' || SQLERRM;
         END;
            END;
         END LOOP;                                                        --C4
      END LOOP;                                                           --C2

      -- ADded by Shankar 21-Jan-2013 to update credit class for COD customers
      -- coming through interface from PRISM
       l_sec := 'Opening Cursor C3';
      FOR c3_rec IN c3
      LOOP
         p_customer_profile_rec.cust_account_profile_id :=
            c3_rec.cust_account_profile_id;
         p_customer_profile_rec.credit_classification := 'CSALE';
         p_obj_version_number := c3_rec.object_version_number;

         IF c3_rec.standard_terms = 1019 --PFRDCASH                                        --Added for ver#1.4 <<START>>
         THEN
            p_customer_profile_rec.standard_terms:=c3_rec.standard_terms;
         END IF;          --  <<END>>

         BEGIN
         HZ_CUSTOMER_PROFILE_V2PUB.update_customer_profile (
            p_init_msg_list           => 'T',
            p_customer_profile_rec    => p_customer_profile_rec,
            p_object_version_number   => p_obj_version_number,
            x_return_status           => x_return_status,
            x_msg_count               => x_msg_count,
            x_msg_data                => x_msg_data);

            p_customer_profile_rec.standard_terms:=NULL;   --Added for ver#1.4

         IF x_return_status <> 'S'
         THEN
            retcode := 'E'; 
            errbuf:= 
                  'Issue updating Profile for customer '
               || c3_rec.account_number
               || ' '
               || x_msg_data;
         END IF;
         EXCEPTION
            WHEN OTHERS THEN
               retcode := 'E'; 
               errbuf := 'Error2 while updating Profile ' || SQLERRM;
         END;
      END LOOP;
      COMMIT;  -- Added for Ver# 1.3
   END update_customer_profile;
   /*************************************************************************
      PROCEDURE Name: xxwc_create_b2b_setup

      PURPOSE:   This procedure will create B2B setup when a new account is created for Trading Partner ('KPT','NPL')

      REVISIONS:
      Ver        Date          Author             Description
      ---------  ------------  ------------       ------------------
      1.0        11/02/2017  Niraj K Ranjan       TMS#20171102-00074-Mobile apps new plsql procedures for WOC    
   ****************************************************************************/
   PROCEDURE xxwc_create_b2b_setup( p_b2b_config_tbl_typ IN XXWC.XXWC_B2B_CONFIG_TBL%ROWTYPE
                                   ,p_return_status       OUT  VARCHAR2
                                   ,p_return_message      OUT  VARCHAR2
       )IS

          l_proc_name               VARCHAR2(2000) := 'xxwc_create_b2b_setup';
          l_b2b_config_tbl_typ      XXWC.XXWC_B2B_CONFIG_TBL%ROWTYPE;
          l_sec                     VARCHAR2(200);
          l_custaccount_id          NUMBER;
          l_party_id                hz_cust_accounts.party_id%TYPE := NULL;
          l_party_name              XXWC.XXWC_B2B_CONFIG_TBL.PARTY_NAME%TYPE := NULL;
          l_party_number            XXWC.XXWC_B2B_CONFIG_TBL.PARTY_NUMBER%TYPE := NULL;
          l_cust_account_id         XXWC.XXWC_B2B_CONFIG_TBL.CUST_ACCOUNT_ID%TYPE := NULL;
          e_biz_exception           EXCEPTION;
          l_count                   NUMBER;
          l_cust_count              NUMBER;
          l_return_status           VARCHAR2(1);
          l_return_message          VARCHAR2(2000);
     BEGIN
        l_sec := 'Start xxwc_create_b2b_setup';
        --l_custaccount_id  := p_event.GetValueForParameter('CUST_ACCOUNT_ID');
        --mandatory check for required fields
        IF p_b2b_config_tbl_typ.account_number IS NULL 
        THEN
           l_return_status := 'E';
           l_return_message := 'Customer Account Number is null';
           Raise e_biz_exception;
        ELSIF NVL(p_b2b_config_tbl_typ.deliver_soa,'N') = 'N' AND
              NVL(p_b2b_config_tbl_typ.deliver_asn,'N') = 'N' AND
              NVL(p_b2b_config_tbl_typ.deliver_pod,'N') = 'N'
        THEN
           l_return_status := 'E';
           l_return_message := 'Either of SOA,ASN,POD flag should be Y';
           Raise e_biz_exception;
        ELSIF p_b2b_config_tbl_typ.notification_email IS NULL 
        THEN
           l_return_status := 'E';
           l_return_message := 'Email id is null';
           Raise e_biz_exception;
        END IF;
        
        l_b2b_config_tbl_typ := p_b2b_config_tbl_typ;
        l_sec := 'Fetch party id for account# '||p_b2b_config_tbl_typ.account_number;
        --To check if the part is setup as b2b customer
        BEGIN
           /*SELECT party_id
             INTO l_party_id
             FROM hz_cust_accounts
            WHERE account_number = p_b2b_config_tbl_typ.customer_number;*/
            
            SELECT hp.party_id,hp.party_name,hp.party_number,hca.cust_account_id 
            INTO  l_party_id,l_party_name,l_party_number,l_cust_account_id
            FROM hz_parties hp,hz_cust_accounts hca
            WHERE hca.party_id = hp.party_id
            AND hca.account_number = l_b2b_config_tbl_typ.account_number;
        EXCEPTION WHEN OTHERS THEN
           l_party_id := NULL;
           l_party_name := NULL;
           l_party_number := NULL;
           l_cust_account_id := NULL;
        END;
        l_sec := 'After Derive l_party_id'||l_party_id;
        
        SELECT COUNT(1) INTO l_cust_count
        FROM xxwc.xxwc_b2b_config_tbl
        WHERE account_number = l_b2b_config_tbl_typ.account_number
        AND party_id =l_party_id;
        
        IF l_cust_count = 0 THEN
           l_sec := 'After Derive l_count'||l_count;
           
           l_b2b_config_tbl_typ.PARTY_ID               := l_party_id ;
           l_b2b_config_tbl_typ.PARTY_NAME             := l_party_name ;
           l_b2b_config_tbl_typ.PARTY_NUMBER           := l_party_number ;
           l_b2b_config_tbl_typ.CUSTOMER_ID            := l_cust_account_id ;
           --l_b2b_config_tbl_typ.DELIVER_SOA            := provided by web page
           --l_b2b_config_tbl_typ.DELIVER_ASN            := provided by web page
           --l_b2b_config_tbl_typ.DELIVER_INVOICE        := ''
           l_b2b_config_tbl_typ.START_DATE_ACTIVE      := SYSDATE ;
           l_b2b_config_tbl_typ.END_DATE_ACTIVE        := '';
           --l_b2b_config_tbl_typ.CREATION_DATE          := This will be drived by the trigger on this table
           --l_b2b_config_tbl_typ.LAST_UPDATE_DATE       := This will be drived by the trigger on this table
           --l_b2b_config_tbl_typ.LAST_UPDATED_BY        := This will be drived by the trigger on this table
           --l_b2b_config_tbl_typ.TP_NAME                := ''
           --l_b2b_config_tbl_typ.DELIVER_POA            := provided by web page
           --l_b2b_config_tbl_typ.DEFAULT_EMAIL          := future use
           --l_b2b_config_tbl_typ.STATUS                 := future use
           --l_b2b_config_tbl_typ.NOTIFICATION_EMAIL     := provided by web page
           --l_b2b_config_tbl_typ.NOTIFY_ACCOUNT_MGR     := future use
           --l_b2b_config_tbl_typ.SOA_EMAIL              := future use
           --l_b2b_config_tbl_typ.ASN_EMAIL              := future use
           --l_b2b_config_tbl_typ.COMMENTS               := future use
           --l_b2b_config_tbl_typ.APPROVED_DATE          := future use
           --l_b2b_config_tbl_typ.DELIVER_POD            := provided by web page
           IF l_b2b_config_tbl_typ.DELIVER_POD = 'Y' THEN
              l_b2b_config_tbl_typ.POD_EMAIL              := l_b2b_config_tbl_typ.NOTIFICATION_EMAIL;
           END IF;
           /*IF NVL(l_b2b_config_tbl_typ.DELIVER_SOA,'N') = 'N' AND 
              NVL(l_b2b_config_tbl_typ.DELIVER_ASN,'N') = 'N' AND
              NVL(l_b2b_config_tbl_typ.DELIVER_POD,'N') = 'Y'
           THEN
              l_b2b_config_tbl_typ.NOTIFICATION_EMAIL      := NULL;
           END IF;*/
           --l_b2b_config_tbl_typ.POD_FREQUENCY          := provided by web page
           --l_b2b_config_tbl_typ.ACCOUNT_NUMBER         := provided by web page
           --l_b2b_config_tbl_typ.ACCOUNT_NAME           := This will be drived by the trigger on this table
           l_b2b_config_tbl_typ.CUST_ACCOUNT_ID        := l_cust_account_id;
           --l_b2b_config_tbl_typ.POD_LAST_SENT_DATE     := future use
           --l_b2b_config_tbl_typ.POD_NEXT_SEND_DATE     := future use
           --l_b2b_config_tbl_typ.ID                     := future use
           --l_b2b_config_tbl_typ.LOCATION               := future use
           --l_b2b_config_tbl_typ.SITE_USE_ID            := future use
           --l_b2b_config_tbl_typ.SOA_PRINT_PRICE        := future use
           --l_b2b_config_tbl_typ.POD_PRINT_PRICE        := future use
        
           INSERT INTO XXWC.XXWC_B2B_CONFIG_TBL
           VALUES l_b2b_config_tbl_typ;
           l_sec := 'After insert';
           COMMIT;
        END IF;
        p_return_status  := 'S';
        p_return_message := '';
     EXCEPTION
          WHEN e_biz_exception THEN
             p_return_status  := l_return_status;
             p_return_message := l_return_message;
             ROLLBACK;
          WHEN OTHERS THEN
             xxcus_error_pkg.xxcus_error_main_api (
                                                p_called_from         => UPPER(g_pkg_name)||'.'||UPPER(l_proc_name)
                                               ,p_calling             => l_sec
                                               ,p_request_id          => fnd_global.conc_request_id
                                               ,p_ora_error_msg       => SUBSTR(SQLERRM,1,2000)
                                               ,p_error_desc          => 'Error in procedure '||l_proc_name
                                               ,p_distribution_list   => g_dflt_email
                                               ,p_module              => 'XXWC'
                                              );
     END xxwc_create_b2b_setup;
   /*************************************************************************
     $Header create_customer $
     Module Name: create_customer

     PURPOSE:   create customer account,site and contacts    
                       
     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        11/14/2017  Niraj K Ranjan          TMS#20171102-00074-Mobile apps new plsql procedures for WOC             
   **************************************************************************/
   PROCEDURE create_customer ( p_cust_rec             IN   CustRecTyp
                              ,p_customer_number      OUT  VARCHAR2
                              ,p_cust_account_id      OUT  NUMBER
                              ,p_bill_to_site_use_id  OUT  NUMBER
                              ,p_ship_to_site_use_id  OUT  NUMBER
                              ,p_cust_account_role_id OUT  NUMBER
                              ,p_return_status        OUT  VARCHAR2
                              ,p_return_message       OUT  VARCHAR2)
   IS
      --constants
      l_proc_name          VARCHAR2(100):= g_pkg_name||'.'||'CREATE_CUSTOMER';
      l_status             HZ_CUST_ACCOUNTS.STATUS%TYPE := 'A';
      --l_customer_type      HZ_CUST_ACCOUNTS.CUSTOMER_TYPE%TYPE := 'R';
      l_attribute_category HZ_CUST_ACCOUNTS.ATTRIBUTE_CATEGORY%TYPE := 'No';
      l_attribute11        HZ_CUST_ACCOUNTS.ATTRIBUTE11 %TYPE := 'Y';
      l_gsa_indicator_flag VARCHAR2(1) := 'N';
      l_created_by_module  HZ_CUST_ACCOUNTS.CREATED_BY_MODULE%TYPE := 'ONT_UI_ADD_CUSTOMER';
      l_responsibility_key FND_RESPONSIBILITY_VL.RESPONSIBILITY_KEY%TYPE := 'XXWC_ORDER_MGMT_PRICING_FULL';
      --common variables
      l_return_status      VARCHAR2(1);
      l_return_message     VARCHAR2(2000); 
      x_return_status      VARCHAR2(2000);
      x_msg_count          NUMBER;
      x_msg_data           VARCHAR2(2000);
      l_user_id            NUMBER;
      l_resp_id            NUMBER; --HDS Order Mgmt Pricing Full - WC
      l_appl_id            NUMBER;   --ONT
      l_customer_class_code  AR_LOOKUPS.LOOKUP_CODE%TYPE := 'UNKNOWN';
      l_remit_to_code      AR_LOOKUPS.LOOKUP_CODE%TYPE := '2';
      l_profile_class_id   hz_cust_profile_classes.profile_class_id%TYPE;
      e_biz_exception EXCEPTION;
      
      --customer account related variables
      l_cust_account_rec     HZ_CUST_ACCOUNT_V2PUB.CUST_ACCOUNT_REC_TYPE;
      l_organization_rec     HZ_PARTY_V2PUB.ORGANIZATION_REC_TYPE;
      l_customer_profile_rec HZ_CUSTOMER_PROFILE_V2PUB.CUSTOMER_PROFILE_REC_TYPE;
               
      x_cust_account_id      NUMBER;
      x_account_number       VARCHAR2(2000);
      x_party_id             NUMBER;
      x_party_number         VARCHAR2(2000);
      x_profile_id           NUMBER;
      
      l_account_number     HZ_CUST_ACCOUNTS.ACCOUNT_NUMBER%TYPE;
      l_cust_account_id    HZ_CUST_ACCOUNTS.CUST_ACCOUNT_ID%TYPE;
      l_party_number       HZ_PARTIES.PARTY_NUMBER%TYPE;
      l_party_id           HZ_PARTIES.PARTY_ID%TYPE;
      
      --Party primary contact related variables
      l_contact_point_rec      HZ_CONTACT_POINT_V2PUB.CONTACT_POINT_REC_TYPE;
      l_edi_rec                HZ_CONTACT_POINT_V2PUB.EDI_REC_TYPE;
      l_email_rec              HZ_CONTACT_POINT_V2PUB.EMAIL_REC_TYPE;
      l_phone_rec              HZ_CONTACT_POINT_V2PUB.PHONE_REC_TYPE;
      l_telex_rec              HZ_CONTACT_POINT_V2PUB.TELEX_REC_TYPE;
      l_web_rec                HZ_CONTACT_POINT_V2PUB.WEB_REC_TYPE;
      x_contact_point_id       NUMBER;
      
      --site related variables
      l_ship_to_site_use_id              NUMBER;
      l_bill_to_site_use_id              NUMBER;
      l_xxwc_address_rec       xxwc.xxwc_address_rec;
      l_site_count                       NUMBER;
      
      --contacts related variables
      l_cust_contact_id NUMBER;
      l_email_contact_cnt NUMBER;
      l_phone_contact_cnt NUMBER;
      l_email_id HZ_CONTACT_POINTS.EMAIL_ADDRESS%TYPE;
      l_phone_no HZ_CONTACT_POINTS.PHONE_NUMBER%TYPE;
      
      --B2B customer flag set up to get notification for order update
      l_b2b_config_tbl_typ XXWC.XXWC_B2B_CONFIG_TBL%ROWTYPE;

   BEGIN
      BEGIN 
         SELECT USER_ID
         INTO l_user_id
         FROM apps.fnd_user
        WHERE UPPER(USER_NAME) = UPPER(p_cust_rec.ntid);
      EXCEPTION
         WHEN OTHERS THEN
            l_return_status := 'E';
            l_return_message := 'User NTID ' ||p_cust_rec.ntid||' does not exist in ERP';
            RAISE e_biz_exception;
      END;
      BEGIN 
         SELECT responsibility_id,application_id 
         INTO L_resp_id,L_appl_id
         FROM fnd_responsibility_vl 
         WHERE responsibility_key = l_responsibility_key;
      EXCEPTION
         WHEN OTHERS THEN
            l_return_status := 'E';
            l_return_message := 'Responsibility '||l_responsibility_key||' does not exist in ERP';
            RAISE e_biz_exception;
      END;
      Fnd_global.Apps_initialize (L_user_id, L_resp_id, L_appl_id);
      Mo_global.Init ('AR');
      Mo_global.Set_policy_context ('S', 162);
      --debug message
      IF NVL(g_xxwc_woc_debug,'N') = 'Y' THEN
         xxwc_checkitapp_pkg.xxwc_log_debug_info
                    (P_API_NAME         =>l_proc_name,
                     --P_ORDER_NUMBER     =>p_quote_number,
                     P_DEBUG_TEXT       => 'Start create_customer',
                     P_USER_ID          => l_user_id
                    );
      END IF;
      l_return_status := '';
      l_return_message := '';
      l_account_number := '';
      --START OF ORGANIZATION AND ACCOUNT CREATION
      /*IF p_cust_rec.account_name IS NULL  THEN
         l_return_status := 'E';
         l_return_message := 'Account Name is null';
         RAISE e_biz_exception;
      ELS*/IF p_cust_rec.organization_name IS NULL THEN
         l_return_status := 'E';
         l_return_message := 'Party Organization Name is null';
         RAISE e_biz_exception;
      END IF;
      --check if party organization exists
      BEGIN
         SELECT hp.party_number,hp.party_id
         INTO  l_party_number,l_party_id
         FROM HZ_PARTIES HP
         WHERE 1=1
         AND HP.STATUS = 'A'
         AND HP.PARTY_NAME = p_cust_rec.organization_name;
      EXCEPTION
         WHEN OTHERS THEN
            l_party_number := NULL;
            l_party_id := NULL;
      END;
      --check if customer account exists
      BEGIN
         SELECT hca.account_number,hca.cust_account_id
         INTO  l_account_number,l_cust_account_id
         FROM HZ_CUST_ACCOUNTS HCA
         WHERE HCA.PARTY_ID = l_party_id
         AND HCA.STATUS = 'A'
         AND (HCA.ACCOUNT_NAME = p_cust_rec.organization_name OR
         HCA.ACCOUNT_NAME ='CASH/'||p_cust_rec.organization_name);
         /*AND (HCA.ACCOUNT_NAME = p_cust_rec.account_name OR
         HCA.ACCOUNT_NAME ='CASH/'||p_cust_rec.account_name);*/
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            l_account_number := NULL;
            l_cust_account_id := NULL;
         WHEN TOO_MANY_ROWS THEN
            l_return_status := 'E';
            l_return_message := 'Multiple active accounts found with name - '||p_cust_rec.organization_name;
            RAISE e_biz_exception;
      END;
      
      IF (l_party_id IS NULL AND l_cust_account_id IS NULL) OR
         (l_party_id IS NOT NULL AND l_cust_account_id IS NULL)
      THEN
         --populate account record
         --l_cust_account_rec.account_name := p_cust_rec.account_name;
         /*IF p_cust_rec.account_type = 'CASH' THEN
            l_cust_account_rec.account_name := 'CASH/'||p_cust_rec.account_name;
         END IF;*/
         l_cust_account_rec.created_by_module := NVL(p_cust_rec.created_by_module,l_created_by_module);
         l_cust_account_rec.status := l_status;
         --l_cust_account_rec.customer_type := l_customer_type; --External- lookup is CUSTOMER_TYPE
         l_cust_account_rec.attribute_category := l_attribute_category; --lookup is CUSTOMER CLASS
         l_cust_account_rec.attribute11 := l_attribute11;
         
         --populate party organization record
         IF l_party_id IS NULL THEN
            l_organization_rec.organization_name := p_cust_rec.organization_name;
            l_organization_rec.created_by_module := NVL(p_cust_rec.created_by_module,l_created_by_module);
            --l_organization_rec.gsa_indicator_flag := l_gsa_indicator_flag;
         ELSE
            l_organization_rec.party_rec.party_id := l_party_id;
         END IF;
         -- create customer
         --debug message
         IF NVL(g_xxwc_woc_debug,'N') = 'Y' THEN
            xxwc_checkitapp_pkg.xxwc_log_debug_info
                    (P_API_NAME         =>l_proc_name,
                     --P_ORDER_NUMBER     =>p_quote_number,
                     P_DEBUG_TEXT       => 'Start HZ_CUST_ACCOUNT_V2PUB.create_cust_account',
                     P_USER_ID          => l_user_id
                    );
         END IF;
         HZ_CUST_ACCOUNT_V2PUB.create_cust_account(p_init_msg_list => FND_API.G_FALSE,                               p_cust_account_rec => l_cust_account_rec,
                                        p_organization_rec => l_organization_rec,
                                        p_customer_profile_rec => l_customer_profile_rec,
                                        p_create_profile_amt => FND_API.G_TRUE,
                                        x_cust_account_id => x_cust_account_id,
                                        x_account_number => x_account_number,
                                        x_party_id => x_party_id,
                                        x_party_number => x_party_number,
                                        x_profile_id => x_profile_id,
                                        x_return_status => x_return_status,
                                        x_msg_count => x_msg_count,
                                        x_msg_data => x_msg_data);
         --debug message
         IF NVL(g_xxwc_woc_debug,'N') = 'Y' THEN
            xxwc_checkitapp_pkg.xxwc_log_debug_info
                    (P_API_NAME         =>l_proc_name,
                     P_CUSTOMER_ID     =>x_cust_account_id,
                     P_DEBUG_TEXT       => 'End HZ_CUST_ACCOUNT_V2PUB.create_cust_account',
                     P_USER_ID          => l_user_id
                    );
         END IF;
         IF x_return_status = FND_API.G_RET_STS_SUCCESS  --success
         THEN
            l_return_status   := 'S';
            l_account_number  := x_account_number;
            l_cust_account_id := x_cust_account_id ;
            l_party_number    := x_party_number;
            l_party_id        := x_party_id;
         ELSIF x_return_status IN (fnd_api.g_ret_sts_error,fnd_api.g_ret_sts_unexp_error) 
         --error,unexpected error
         THEN
            l_return_status := 'E';
         
         END IF;
         IF x_msg_count > 1
         THEN
            FOR i IN 1 .. x_msg_count
            LOOP
             IF l_return_message IS NULL THEN
                l_return_message := SUBSTR(i||'. '||SUBSTR (FND_MSG_PUB.Get (p_encoded => FND_API.G_FALSE), 1, 255),1,2000);
             ELSE
                  l_return_message := SUBSTR(l_return_message||chr(30)||i||'. '
                  || SUBSTR (FND_MSG_PUB.Get (p_encoded => FND_API.G_FALSE), 1, 255),1,2000);
             END IF;
            END LOOP;
         ELSIF x_msg_count = 1 
         THEN
            l_return_message := SUBSTR(x_msg_data,1,2000);
         END IF;
         
         IF l_return_status = 'E' THEN
            l_return_message := SUBSTR('Account not created.'||chr(30)||l_return_message,1,2000);
            RAISE e_biz_exception;
         END IF;
      END IF;
      --END OF ORGANIZATION AND ACCOUNT CREATION
      --debug message
         IF NVL(g_xxwc_woc_debug,'N') = 'Y' THEN
            xxwc_checkitapp_pkg.xxwc_log_debug_info
                    (P_API_NAME         =>l_proc_name,
                     P_CUSTOMER_ID     =>l_cust_account_id,
                     P_DEBUG_TEXT       => 'Start Site Creation',
                     P_USER_ID          => l_user_id
                    );
         END IF;
      --START OF SITE CREATION
      IF l_cust_account_id IS NOT NULL THEN
         l_return_status := '';
         l_return_message := '';
         IF p_cust_rec.location IS NULL OR 
            p_cust_rec.country  IS NULL OR 
            p_cust_rec.address1 IS NULL
         THEN
            l_return_status := 'E'; 
            l_return_message := 'Site Name / Country / Address1 is missing';    
            RAISE e_biz_exception;            
         END IF;
         select count(1) into l_site_count
          from hz_parties hp,
               hz_cust_accounts hca,
               hz_locations hl,
               hz_party_sites hps,
               hz_cust_acct_sites_all hcas
          where 1=1
          and hp.party_id = l_party_id
          and hca.cust_account_id = l_cust_account_id
          AND hp.party_id = hca.party_id
          AND HL.LOCATION_ID = HPS.LOCATION_ID
          and hp.party_id = hps.party_id
          and hps.party_site_name = p_cust_rec.location
          AND hca.cust_account_id = hcas.cust_account_id
          AND hps.party_site_id = hcas.party_site_id
          AND hp.Status ='A'
          AND hca.Status ='A'
          --AND hl.Status ='A'
          AND hps.Status ='A'
          AND hcas.Status ='A'
          and HL.country                 = p_cust_rec.country                           
          and HL.address1                = p_cust_rec.address1            
          and NVL(HL.address2,'$')       = NVL(p_cust_rec.address2 ,'$')          
          and NVL(HL.address3,'$')       = NVL(p_cust_rec.address3,'$')            
          and NVL(HL.address4 ,'$')      = NVL(p_cust_rec.address4,'$')            
          and NVL(HL.city ,'$')          = NVL(p_cust_rec.city ,'$')                    
          and NVL(HL.postal_code,'$')    = NVL(p_cust_rec.postal_code,'$')
          and NVL(HL.state,'$')          = NVL(p_cust_rec.state ,'$')                           
          and NVL(HL.county,'$')         = NVL(p_cust_rec.county ,'$');
         IF l_site_count = 0 THEN
            l_xxwc_address_rec                  := xxwc_address_rec.g_miss_null;
            l_xxwc_address_rec.customer_id      := l_cust_account_id;
            l_xxwc_address_rec.country          := p_cust_rec.country;
            l_xxwc_address_rec.address1         := p_cust_rec.address1;
            l_xxwc_address_rec.address2         := p_cust_rec.address2; 
            l_xxwc_address_rec.address3         := p_cust_rec.address3; 
            l_xxwc_address_rec.address4         := p_cust_rec.address4; 
            l_xxwc_address_rec.city             := p_cust_rec.city; 
            l_xxwc_address_rec.county           := p_cust_rec.county; 
            l_xxwc_address_rec.state            := p_cust_rec.state;
            l_xxwc_address_rec.postal_code      := p_cust_rec.postal_code; 
            l_xxwc_address_rec.sold_to_flag     := p_cust_rec.sold_to_flag; 
            l_xxwc_address_rec.ship_to_flag     := p_cust_rec.ship_to_flag; 
            l_xxwc_address_rec.bill_to_flag     := p_cust_rec.bill_to_flag; 
            l_xxwc_address_rec.deliver_to_flag  := p_cust_rec.deliver_to_flag; 
            l_xxwc_address_rec.location         := p_cust_rec.location;
            l_xxwc_address_rec.user_ntid          := UPPER(p_cust_rec.ntid) ;
             
     
            CREATE_JOB_SITE
            (
              p_xxwc_address_rec     =>  l_xxwc_address_rec
             ,p_created_by_module    =>  NVL(p_cust_rec.created_by_module,l_created_by_module)
             ,o_status               =>  l_return_status
             ,o_ship_to_site_use_id  =>  l_ship_to_site_use_id
             ,o_bill_to_site_use_id  =>  l_bill_to_site_use_id
             ,o_message              =>  l_return_message
            );
            IF l_return_status = 'E' THEN
               l_return_message := SUBSTR('Site not created.'||chr(30)||l_return_message,1,2000);
               RAISE e_biz_exception;
            END IF;
         END IF;
      END IF;
      --END OF SITE CREATION
      --debug message
         IF NVL(g_xxwc_woc_debug,'N') = 'Y' THEN
            xxwc_checkitapp_pkg.xxwc_log_debug_info
                    (P_API_NAME         =>l_proc_name,
                     P_CUSTOMER_ID     =>l_cust_account_id,
                     P_JOB_SITE_ID     => l_ship_to_site_use_id,
                     P_DEBUG_TEXT       => 'End Site Creation',
                     P_USER_ID          => l_user_id
                    );
         END IF;
      --debug message
         IF NVL(g_xxwc_woc_debug,'N') = 'Y' THEN
            xxwc_checkitapp_pkg.xxwc_log_debug_info
                    (P_API_NAME         =>l_proc_name,
                     P_CUSTOMER_ID     =>l_cust_account_id,
                     P_JOB_SITE_ID     => l_ship_to_site_use_id,
                     P_DEBUG_TEXT       => 'Start Primary Contact Creation',
                     P_USER_ID          => l_user_id
                    );
         END IF;
      --START PARTY PRIMARY CONTACT CREATION
      IF l_party_id IS NOT NULL THEN
         IF p_cust_rec.party_email_id IS NOT NULL THEN
            SELECT COUNT(1) INTO l_email_contact_cnt
            FROM hz_parties hp,
                 hz_contact_points hcp
            WHERE hp.party_id = l_party_id   
            AND hp.party_id = hcp.owner_table_id
            AND hcp.owner_table_name = 'HZ_PARTIES'
            AND hcp.primary_flag = 'Y'
            AND hcp.contact_point_type = 'EMAIL'
            AND hp.Status ='A'
            AND hcp.status = 'A'
            AND hp.email_address = p_cust_rec.party_email_id;
         END IF;
         IF p_cust_rec.party_phone_number IS NOT NULL AND
            p_cust_rec.party_phone_area_code IS NOT NULL
         THEN
            SELECT COUNT(1) INTO l_phone_contact_cnt
            FROM hz_parties hp,
                 hz_contact_points HCP
            WHERE HP.PARTY_ID = l_party_id  
            AND HP.PARTY_ID = HCP.OWNER_TABLE_ID
            AND HCP.OWNER_TABLE_NAME = 'HZ_PARTIES'
            AND HCP.PRIMARY_FLAG = 'Y'
            AND HCP.CONTACT_POINT_TYPE = 'PHONE'
            AND hp.Status ='A'
            AND hcp.status = 'A'
            AND HP.PRIMARY_PHONE_AREA_CODE = p_cust_rec.party_phone_area_code
            AND HP.PRIMARY_PHONE_NUMBER = p_cust_rec.party_phone_number
            AND NVL(HP.PRIMARY_PHONE_EXTENSION,'$') = NVL(p_cust_rec.party_phone_extension,'$');
         END IF;
         IF l_phone_contact_cnt = 0 THEN
            x_contact_point_id := NULL;
            x_return_status := NULL;
            x_msg_count := NULL;
            x_msg_data := NULL;
            l_return_status := NULL;
            l_return_message := NULL;
            
            l_contact_point_rec.contact_point_type := 'PHONE';
            l_contact_point_rec.owner_table_name := 'HZ_PARTIES';
            l_contact_point_rec.owner_table_id := l_party_id;
            l_contact_point_rec.primary_flag := 'Y';
            l_contact_point_rec.created_by_module := NVL(p_cust_rec.created_by_module,l_created_by_module);
            
            l_phone_rec.phone_area_code := p_cust_rec.party_phone_area_code;
            --p_phone_rec.phone_country_code := '1';
            l_phone_rec.phone_number := p_cust_rec.party_phone_number;
            l_phone_rec.phone_line_type := 'GEN';
            
            HZ_CONTACT_POINT_V2PUB.CREATE_CONTACT_POINT (
               p_init_msg_list       => FND_API.G_TRUE,
               p_contact_point_rec   => l_contact_point_rec,
               p_edi_rec             => l_edi_rec,
               p_email_rec           => l_email_rec,
               p_phone_rec           => l_phone_rec,
               p_telex_rec           => l_telex_rec,
               p_web_rec             => l_web_rec,
               x_contact_point_id    => x_contact_point_id,
               x_return_status       => x_return_status,
               x_msg_count           => x_msg_count,
               x_msg_data            => x_msg_data);
            IF x_return_status != 'S'
            THEN
               l_return_status := x_return_status;
               l_return_message := substr('Primary Party Phone Contact not created'||chr(30)||x_msg_data,1,2000);
               RAISE e_biz_exception;
            END IF;
         END IF;
         IF l_email_contact_cnt = 0 THEN
            x_contact_point_id := NULL;
            x_return_status := NULL;
            x_msg_count := NULL;
            x_msg_data := NULL;
            l_phone_rec := NULL;
            l_contact_point_rec := NULL;
            l_return_status := NULL;
            l_return_message := NULL;
            l_contact_point_rec.contact_point_type := 'EMAIL';
            l_contact_point_rec.owner_table_name := 'HZ_PARTIES';
            l_contact_point_rec.owner_table_id := l_party_id; 
            l_contact_point_rec.primary_flag := 'Y';
            l_contact_point_rec.created_by_module := NVL(p_cust_rec.created_by_module,l_created_by_module);
            
            l_email_rec.email_format := 'MAILHTML';
            l_email_rec.email_address := p_cust_rec.party_email_id ;
            
            HZ_CONTACT_POINT_V2PUB.CREATE_CONTACT_POINT (
               p_init_msg_list       => FND_API.G_TRUE,
               p_contact_point_rec   => l_contact_point_rec,
               p_edi_rec             => l_edi_rec,
               p_email_rec           => l_email_rec,
               p_phone_rec           => l_phone_rec,
               p_telex_rec           => l_telex_rec,
               p_web_rec             => l_web_rec,
               x_contact_point_id    => x_contact_point_id,
               x_return_status       => x_return_status,
               x_msg_count           => x_msg_count,
               x_msg_data            => x_msg_data);
            IF x_return_status != 'S'
            THEN
               l_return_status := x_return_status;
               l_return_message := substr('Primary Party Email Contact not created'||chr(30)||x_msg_data,1,2000);
               RAISE e_biz_exception;
            END IF;
         END IF;
      END IF;
      --END PARTY PRIMARY CONTACT CREATION
      --debug message
         IF NVL(g_xxwc_woc_debug,'N') = 'Y' THEN
            xxwc_checkitapp_pkg.xxwc_log_debug_info
                    (P_API_NAME         =>l_proc_name,
                     P_CUSTOMER_ID     =>l_cust_account_id,
                     P_JOB_SITE_ID     => l_ship_to_site_use_id,
                     P_DEBUG_TEXT       => 'End Primary Contact Creation',
                     P_USER_ID          => l_user_id
                    );
         END IF;
        --debug message
         IF NVL(g_xxwc_woc_debug,'N') = 'Y' THEN
            xxwc_checkitapp_pkg.xxwc_log_debug_info
                    (P_API_NAME         =>l_proc_name,
                     P_CUSTOMER_ID     =>l_cust_account_id,
                     P_JOB_SITE_ID     => l_ship_to_site_use_id,
                     P_DEBUG_TEXT       => 'Start Account Contact Creation',
                     P_USER_ID          => l_user_id
                    );
         END IF;
      --START ACCOUNT LEVEL CONTACT CREATION
      IF l_cust_account_id IS NOT NULL THEN
         l_return_status := '';
         l_return_message := '';
         l_email_id := '';
         l_phone_no := '';
         IF p_cust_rec.contact_num IS NULL AND p_cust_rec.email IS NULL THEN
            l_return_status := 'E'; 
            l_return_message := 'Contact Point is missing - phone / email';    
            RAISE e_biz_exception;
         END IF;
         IF p_cust_rec.contact_num IS NOT NULL THEN
            SELECT count(1) INTO l_phone_contact_cnt
            FROM  hz_contact_points cont_point,
                  hz_cust_account_roles acct_role,
                  hz_parties party,
                  hz_parties rel_party,
                  hz_relationships rel,
                  hz_org_contacts org_cont ,
                  hz_cust_accounts role_acct
            WHERE  1=1
              and role_acct.cust_account_id = l_cust_account_id
              and acct_role.party_id = rel.party_id
              AND  acct_role.role_type = 'CONTACT'
              AND  org_cont.party_relationship_id = rel.relationship_id
              AND  rel.subject_id = party.party_id
              AND  rel_party.party_id = rel.party_id
              AND  cont_point.owner_table_id(+) = rel_party.party_id    
              AND  acct_role.cust_account_id = role_acct.cust_account_id
              AND  Role_Acct.Party_Id = Rel.Object_Id
              AND  cont_point.owner_table_name(+)='HZ_PARTIES'    
              AND  Rel.Subject_Table_Name ='HZ_PARTIES'  
              AND  Rel.Object_Table_Name ='HZ_PARTIES' 
              AND  Party.Status ='A'
              AND  Rel_Party.Status ='A'
              AND  Rel.Status ='A'
              and role_acct.status = 'A'
              and cont_point.status = 'A'
              and cont_point.contact_point_type = 'PHONE'
              and party.PERSON_FIRST_NAME = p_cust_rec.first_name
              and party.PERSON_LAST_NAME = p_cust_rec.last_name
              and cont_point.PHONE_NUMBER = p_cust_rec.contact_num;
         END IF;
         IF p_cust_rec.email IS NOT NULL THEN
            SELECT count(1) INTO l_email_contact_cnt
            FROM  hz_contact_points cont_point,
                  hz_cust_account_roles acct_role,
                  hz_parties party,
                  hz_parties rel_party,
                  hz_relationships rel,
                  hz_org_contacts org_cont ,
                  hz_cust_accounts role_acct
            WHERE  1=1
              and role_acct.cust_account_id = l_cust_account_id
              and acct_role.party_id = rel.party_id
              AND  acct_role.role_type = 'CONTACT'
              AND  org_cont.party_relationship_id = rel.relationship_id
              AND  rel.subject_id = party.party_id
              AND  rel_party.party_id = rel.party_id
              AND  cont_point.owner_table_id(+) = rel_party.party_id    
              AND  acct_role.cust_account_id = role_acct.cust_account_id
              AND  Role_Acct.Party_Id = Rel.Object_Id
              AND  cont_point.owner_table_name(+)='HZ_PARTIES'    
              AND  Rel.Subject_Table_Name ='HZ_PARTIES'  
              AND  Rel.Object_Table_Name ='HZ_PARTIES' 
              AND  Party.Status ='A'
              AND  Rel_Party.Status ='A'
              AND  Rel.Status ='A'
              and role_acct.status = 'A'
              and cont_point.status = 'A'
              and cont_point.contact_point_type = 'EMAIL'
              and party.PERSON_FIRST_NAME = p_cust_rec.first_name
              and party.PERSON_LAST_NAME = p_cust_rec.last_name
              and cont_point.email_address = p_cust_rec.email;
         END IF;  
         IF l_email_contact_cnt > 0 THEN
            l_email_id := NULL;
         ELSE
            l_email_id := p_cust_rec.email;
         END IF;
         IF l_phone_contact_cnt > 0 THEN
            l_phone_no := NULL;
         ELSE
            l_phone_no := p_cust_rec.contact_num;
         END IF;
         IF (l_email_id IS NOT NULL) OR (l_phone_no IS NOT NULL) THEN
            CREATE_CONTACT (p_customer_id       => l_cust_account_id,
                            p_first_name        => p_cust_rec.first_name,
                            p_last_name         => p_cust_rec.last_name,
                            p_contact_num       => l_phone_no,
                            p_email_id          => l_email_id,
                            p_fax_num           => p_cust_rec.fax_num,
                            p_created_by_module => NVL(p_cust_rec.created_by_module,l_created_by_module),
                            p_cust_contact_id   => l_cust_contact_id,
                            p_error_code        => l_return_status,
                            p_error_message     => l_return_message);
            
            IF l_return_status = 'E' THEN
               l_return_message := SUBSTR('Contact not created.'||chr(30)||l_return_message,1,2000);
               RAISE e_biz_exception;
            END IF;
         END IF;
      END IF;
      --END CONTACT CREATION
      --debug message
         IF NVL(g_xxwc_woc_debug,'N') = 'Y' THEN
            xxwc_checkitapp_pkg.xxwc_log_debug_info
                    (P_API_NAME         =>l_proc_name,
                     P_CUSTOMER_ID     =>l_cust_account_id,
                     P_JOB_SITE_ID     => l_ship_to_site_use_id,
                     P_DEBUG_TEXT       => 'End Account Contact Creation',
                     P_USER_ID          => l_user_id
                    );
         END IF;
      --debug message
         IF NVL(g_xxwc_woc_debug,'N') = 'Y' THEN
            xxwc_checkitapp_pkg.xxwc_log_debug_info
                    (P_API_NAME         =>l_proc_name,
                     P_CUSTOMER_ID     =>l_cust_account_id,
                     P_JOB_SITE_ID     => l_ship_to_site_use_id,
                     P_DEBUG_TEXT       => 'Start customer Profile update',
                     P_USER_ID          => l_user_id
                    );
         END IF;
      --START PROFILE UPDATE
      IF l_cust_account_id IS NOT NULL THEN
         BEGIN
            select profile_class_id
            INTO  l_profile_class_id
            from hz_cust_profile_classes 
            where name = 'COD Customers'
            and status='A';
         EXCEPTION
            WHEN OTHERS THEN
               l_profile_class_id := NULL;
         END;
         
         IF l_profile_class_id IS NOT NULL THEN
            update_customer_profile (
                 errbuf                     => l_return_message
                ,retcode                    => l_return_status
                ,p_cust_account_id          => l_cust_account_id
                ,p_profile_class_id         => l_profile_class_id
                ,p_customer_class_code      => l_customer_class_code--'UNKNOWN'
                ,p_remit_to_code            => l_remit_to_code);--'2');
            IF l_return_status = 'E' THEN
               RAISE e_biz_exception;
            END IF;
         END IF;
      END IF;
      --END PROFILE UPDATE
      --debug message
         IF NVL(g_xxwc_woc_debug,'N') = 'Y' THEN
            xxwc_checkitapp_pkg.xxwc_log_debug_info
                    (P_API_NAME         =>l_proc_name,
                     P_CUSTOMER_ID     =>l_cust_account_id,
                     P_JOB_SITE_ID     => l_ship_to_site_use_id,
                     P_DEBUG_TEXT       => 'End customer Profile update',
                     P_USER_ID          => l_user_id
                    );
         END IF;
        --debug message
         IF NVL(g_xxwc_woc_debug,'N') = 'Y' THEN
            xxwc_checkitapp_pkg.xxwc_log_debug_info
                    (P_API_NAME         =>l_proc_name,
                     P_CUSTOMER_ID     =>l_cust_account_id,
                     P_JOB_SITE_ID     => l_ship_to_site_use_id,
                     P_DEBUG_TEXT       => 'Start customer B2B Setup',
                     P_USER_ID          => l_user_id
                    );
         END IF;
      --Start Call xxwc_create_b2b_setup
      IF p_cust_rec.email IS NOT NULL AND l_account_number IS NOT NULL THEN
         l_return_status := '';
         l_return_message := '';
         l_b2b_config_tbl_typ.account_number := l_account_number;
         l_b2b_config_tbl_typ.deliver_soa := 'Y';
         l_b2b_config_tbl_typ.deliver_asn := 'Y';
         l_b2b_config_tbl_typ.deliver_pod := 'Y';
         l_b2b_config_tbl_typ.notification_email := p_cust_rec.email;
         l_b2b_config_tbl_typ.created_by := l_user_id;
         l_b2b_config_tbl_typ.last_updated_by := l_user_id;
         xxwc_create_b2b_setup( p_b2b_config_tbl_typ    => l_b2b_config_tbl_typ
                               ,p_return_status         => l_return_status
                               ,p_return_message        => l_return_message
                              );
         IF l_return_status = 'E' THEN
            RAISE e_biz_exception;
         END IF;
      END IF;
      --End Call xxwc_create_b2b_setup
      --debug message
         IF NVL(g_xxwc_woc_debug,'N') = 'Y' THEN
            xxwc_checkitapp_pkg.xxwc_log_debug_info
                    (P_API_NAME         =>l_proc_name,
                     P_CUSTOMER_ID     =>l_cust_account_id,
                     P_JOB_SITE_ID     => l_ship_to_site_use_id,
                     P_DEBUG_TEXT       => 'End customer B2B Setup',
                     P_USER_ID          => l_user_id
                    );
         END IF;
      COMMIT;      
      --To be returned at last
      p_return_status        := l_return_status;
      p_return_message       := l_return_message;
      p_customer_number      := l_account_number;
      p_cust_account_id      := l_cust_account_id;
      p_bill_to_site_use_id  := l_bill_to_site_use_id;
      p_ship_to_site_use_id  := l_ship_to_site_use_id;
      p_cust_account_role_id := l_cust_contact_id;
      --debug message
         IF NVL(g_xxwc_woc_debug,'N') = 'Y' THEN
            xxwc_checkitapp_pkg.xxwc_log_debug_info
                    (P_API_NAME         =>l_proc_name,
                     P_CUSTOMER_ID     =>l_cust_account_id,
                     P_JOB_SITE_ID     => l_ship_to_site_use_id,
                     P_DEBUG_TEXT       => 'End create_customer',
                     P_USER_ID          => l_user_id
                    );
         END IF;
   EXCEPTION
      WHEN e_biz_exception THEN
         ROLLBACK;
         p_return_status  := l_return_status;
         p_return_message := l_return_message;
      WHEN OTHERS THEN
         ROLLBACK;
         p_return_status := 'E';
         p_return_message := SUBSTR(SQLERRM,1,2000);
   END create_customer; 
   
   /*************************************************************************
     $Header create_woc_order $
     Module Name: create_woc_order

     PURPOSE:   create WSO order    
                       
     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        11/22/2017  Niraj K Ranjan          TMS#20171102-00074-Mobile apps new plsql procedures for WOC             
   **************************************************************************/
   PROCEDURE create_woc_order ( p_quote_number         IN   NUMBER
                               ,p_transfer_branch      IN   VARCHAR2
                               ,p_ntid                 IN   VARCHAR2  
                               ,p_ord_payment_tbl      IN   XXWC.XXWC_ORDER_PAYMENTS_TBL
                               ,p_order_number         OUT  NUMBER
                               ,p_header_id            OUT  NUMBER
                               ,p_return_status        OUT  VARCHAR2
                               ,p_return_message       OUT  VARCHAR2)
   IS
      --common variables
      l_proc_name          VARCHAR2(100):= g_pkg_name||'.'||'CREATE_WOC_ORDER';
      l_return_status      VARCHAR2(1);
      l_return_message     VARCHAR2(2000); 
      l_responsibility_key FND_RESPONSIBILITY_VL.RESPONSIBILITY_KEY%TYPE := 'XXWC_ORDER_MGMT_PRICING_FULL';
      l_user_id            NUMBER;
      l_resp_id            NUMBER;   --HDS Order Mgmt Pricing Full - WC
      l_appl_id            NUMBER;   --ONT
      l_operating_unit     NUMBER := 162;
      e_biz_exception      EXCEPTION;
      --variables related to clone part
      l_to_org_id          NUMBER;
      
      --variable related to quote conversion to order
      --l_order_number       NUMBER;
      l_header_id          NUMBER;
      l_ord_count          NUMBER;
      
      --variable relate to order payment
      l_header_rec         xxwc.xxwc_order_header_rec;
      
      l_hdr_branch_code    MTL_PARAMETERS.ORGANIZATION_CODE%TYPE; --Rev3
      
      --to check if the customer or site is on hard hold
      l_hard_hold_count    NUMBER;
      
      CURSOR cr_clone_part(p_to_org_id NUMBER)
      IS (SELECT ooh.header_id,
                 msi.inventory_item_id,
                 msi.segment1,
                 ool.ship_from_org_id
          FROM oe_order_headers ooh,
               oe_order_lines ool,
               mtl_system_items_b msi
          WHERE ooh.order_number = p_quote_number
          AND   ooh.header_id = ool.header_id
          AND   ool.inventory_item_id = msi.inventory_item_id
          AND   ool.ship_from_org_id = msi.organization_id 
          AND   NOT EXISTS(SELECT 1 
                           FROM mtl_system_items_b msi2,
                                org_organization_definitions ood
                           WHERE msi2.inventory_item_id = msi.inventory_item_id
                           AND   msi2.organization_id   = ood.organization_id
                           AND   ood.organization_id = p_to_org_id));
   BEGIN
      BEGIN 
         SELECT USER_ID
         INTO l_user_id
         FROM apps.fnd_user
        WHERE UPPER(USER_NAME) = UPPER(p_ntid);
      EXCEPTION
         WHEN OTHERS THEN
            l_return_status := 'E';
            l_return_message := 'User NTID ' ||p_ntid||' does not exist in ERP';
            RAISE e_biz_exception;
      END;
      BEGIN 
         SELECT responsibility_id,application_id 
         INTO L_resp_id,L_appl_id
         FROM fnd_responsibility_vl 
         WHERE responsibility_key = l_responsibility_key;
      EXCEPTION
         WHEN OTHERS THEN
            l_return_status := 'E';
            l_return_message := 'Responsibility '||l_responsibility_key||' does not exist in ERP';
            RAISE e_biz_exception;
      END;
      Fnd_global.Apps_initialize (L_user_id, L_resp_id, L_appl_id);
      Mo_global.Init ('ONT');
      Mo_global.Set_policy_context ('S', 162);
      --log debug info
      IF NVL(g_xxwc_woc_debug,'N') = 'Y' THEN
         xxwc_checkitapp_pkg.xxwc_log_debug_info
                    (P_API_NAME         =>l_proc_name,
                     P_ORDER_NUMBER     =>p_quote_number,
                     P_DEBUG_TEXT       => 'Start create_woc_order',
                     P_USER_ID => l_user_id
                    );
      END IF;
      --Clone item from one org to another org
      l_return_status := '';
      l_return_message := '';
      BEGIN
         SELECT organization_id 
         INTO l_to_org_id
         FROM org_organization_definitions 
         WHERE organization_code = p_transfer_branch;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            l_return_status := 'E';
            l_return_message := 'Transfer org '||p_transfer_branch||' not found';
            RAISE e_biz_exception;
      END;
      
      -- Version# 3.0> Start
      -- Derive Header Branch Code
      BEGIN
         SELECT organization_code
         INTO l_hdr_branch_code
         FROM org_organization_definitions ood
            , oe_order_headers ooh
         WHERE ood.organization_id = ooh.ship_from_org_id
           AND ooh.order_number = p_quote_number;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            l_return_status := 'E';
            l_return_message := 'Header org not found';
            RAISE e_biz_exception;
      END; 
      -- Version# 3.0 < End
      
      l_hard_hold_count := 0;
      
      SELECT count(1) 
        INTO l_hard_hold_count
        FROM apps.hz_customer_profiles hcp
              ,apps.hz_cust_accounts hca
              ,apps.oe_order_headers ooh
       WHERE 1=1
         AND hcp.cust_account_id = hca.cust_account_id
         AND hca.cust_account_id = ooh.sold_to_org_id
         AND ooh.quote_number    = p_quote_number
         AND credit_hold         = 'Y'
         AND site_use_id IS NULL;
      
      IF l_hard_hold_count > 0 THEN
         l_return_status := 'E';
         l_return_message := 'Customer is on Credit Hold. Order cannot be created. Contact Credit Associate';
         RAISE e_biz_exception;
      END IF;
      
      l_hard_hold_count := 0;
      
      SELECT count(1) 
        INTO l_hard_hold_count
        FROM apps.hz_customer_profiles hcp
              ,apps.hz_cust_accounts hca
              ,apps.oe_order_headers ooh
       WHERE 1=1
         AND hcp.cust_account_id = hca.cust_account_id
         AND hca.cust_account_id = ooh.sold_to_org_id
         AND ooh.quote_number    = p_quote_number
         AND hcp.credit_hold     = 'Y'
         AND hcp.site_use_id     = ooh.invoice_to_org_id;
      
      IF l_hard_hold_count > 0 THEN
         l_return_status := 'E';
         l_return_message := 'Customer Bill To is on Credit Hold. Order cannot be created. Contact Credit Associate';
         RAISE e_biz_exception;
      END IF;
      
      --log debug info
      IF NVL(g_xxwc_woc_debug,'N') = 'Y' THEN
         xxwc_checkitapp_pkg.xxwc_log_debug_info
                    (P_API_NAME         =>l_proc_name,
                     P_ORDER_NUMBER     =>p_quote_number,
                     P_DEBUG_TEXT       => 'Start Clone Part',
                     P_USER_ID => l_user_id
                    );
      END IF;
      FOR rec_clone IN cr_clone_part(p_to_org_id => l_to_org_id)
      LOOP
         clone_part_prc(  p_transaction_id      => rec_clone.header_id
                         ,p_inventory_item_id   => rec_clone.inventory_item_id
                         ,p_to_org_id           => l_to_org_id
                         ,p_from_org_id         => rec_clone.ship_from_org_id
                         ,p_return_status       => l_return_status
                         ,p_return_message      => l_return_message);
         IF l_return_status = 'E' THEN
            l_return_message := SUBSTR('Cloning of part# '||rec_clone.segment1||' to org '||p_transfer_branch||' has failed:Error=>'||l_return_message,1,2000);
            RAISE e_biz_exception;
         END IF;
      END LOOP;
      --log debug info
      IF NVL(g_xxwc_woc_debug,'N') = 'Y' THEN
         xxwc_checkitapp_pkg.xxwc_log_debug_info
                    (P_API_NAME         =>l_proc_name,
                     P_ORDER_NUMBER     =>p_quote_number,
                     P_DEBUG_TEXT       => 'End Clone Part',
                     P_USER_ID => l_user_id
                    );
      END IF;
      --End Clone item from one org to another org
      
      --Start Convert quote into order
      l_return_status := '';
      l_return_message := '';
      SELECT COUNT(1)
         INTO l_ord_count
         FROM oe_order_headers
         WHERE order_number = p_quote_number
         AND   flow_status_code IN ('DRAFT','PENDING_CUSTOMER_ACCEPTANCE');
      IF l_ord_count = 0 THEN
         l_return_status := 'E';
         l_return_message := SUBSTR('Either quote does not exist or transfer for this quote has already happended',1,2000);
         RAISE e_biz_exception;
      END IF;
      --log debug info
      IF NVL(g_xxwc_woc_debug,'N') = 'Y' THEN
         xxwc_checkitapp_pkg.xxwc_log_debug_info
                    (P_API_NAME         =>l_proc_name,
                     P_ORDER_NUMBER     =>p_quote_number,
                     P_DEBUG_TEXT       => 'Start create_so_from_quote',
                     P_USER_ID => l_user_id
                    );
      END IF;
      create_so_from_quote (p_quote_number   => p_quote_number
                           ,p_header_id      => l_header_id
                           ,p_return_status  => l_return_status
                           ,p_return_message => l_return_message);
      --log debug info
      IF NVL(g_xxwc_woc_debug,'N') = 'Y' THEN
         xxwc_checkitapp_pkg.xxwc_log_debug_info
                    (P_API_NAME         =>l_proc_name,
                     P_ORDER_NUMBER     =>p_quote_number,
                     P_DEBUG_TEXT       => 'End create_so_from_quote',
                     P_USER_ID => l_user_id
                    );
      END IF;
      IF l_return_status = 'E' THEN
         l_return_message := SUBSTR('Order creation for quote# '||p_quote_number||' has failed:Error=>'||l_return_message,1,2000);
         RAISE e_biz_exception;
      END IF;
      --End Convert quote into order
      
      --Start update fullfilment org onto order
      IF l_header_id IS NOT NULL AND l_to_org_id IS NOT NULL THEN
         --log debug info
         IF NVL(g_xxwc_woc_debug,'N') = 'Y' THEN
            xxwc_checkitapp_pkg.xxwc_log_debug_info
                    (P_API_NAME         =>l_proc_name,
                     P_ORDER_NUMBER     =>p_quote_number,
                     P_DEBUG_TEXT       => 'Start update_order_branch',
                     P_USER_ID => l_user_id
                    );
         END IF;
         update_order_branch ( p_header_id       => l_header_id
                              ,p_transfer_org_id => l_to_org_id
                              ,p_return_status   => l_return_status
                              ,p_return_message  => l_return_message);
         --log debug info
         IF NVL(g_xxwc_woc_debug,'N') = 'Y' THEN
            xxwc_checkitapp_pkg.xxwc_log_debug_info
                    (P_API_NAME         =>l_proc_name,
                     P_ORDER_NUMBER     =>p_quote_number,
                     P_DEBUG_TEXT       => 'End update_order_branch',
                     P_USER_ID => l_user_id
                    );
         END IF;
         IF l_return_status = 'E' THEN
            l_return_message := SUBSTR('Update of fulfillment org# '||p_transfer_branch||' has failed:Error=>'||l_return_message,1,2000);
            RAISE e_biz_exception;
         END IF;
      END IF;
      --End update fullfilment org onto order
      
      --Start Order Payment
      IF p_ord_payment_tbl.count > 0 THEN
         -- INITIALIZE HEADER RECORD
         l_header_rec                       := xxwc_order_header_rec.g_miss_null;
         l_header_rec.order_number          := p_quote_number;
         l_header_rec.org_id                := l_operating_unit;
         l_header_rec.created_by            := p_ntid;
         l_header_rec.branch_code           := l_hdr_branch_code; -- Rev#3.0
         --log debug info
         IF NVL(g_xxwc_woc_debug,'N') = 'Y' THEN
            xxwc_checkitapp_pkg.xxwc_log_debug_info
                    (P_API_NAME         =>l_proc_name,
                     P_ORDER_NUMBER     =>p_quote_number,
                     P_DEBUG_TEXT       => 'Start xxwc_apply_payment_to_so',
                     P_USER_ID => l_user_id
                    );
         END IF;
         XXWC_CHECKITAPP_PKG.XXWC_APPLY_PAYMENT_TO_SO
            ( p_order_header_rec     => l_header_rec
             ,p_order_payments_tbl   => p_ord_payment_tbl
             ,o_status               => l_return_status
             ,o_message              => l_return_message
            );
         --log debug info
         IF NVL(g_xxwc_woc_debug,'N') = 'Y' THEN
            xxwc_checkitapp_pkg.xxwc_log_debug_info
                    (P_API_NAME         =>l_proc_name,
                     P_ORDER_NUMBER     =>p_quote_number,
                     P_DEBUG_TEXT       => 'End xxwc_apply_payment_to_so',
                     P_USER_ID => l_user_id
                    );
         END IF;
         IF l_return_status = 'E' THEN
            l_return_message := SUBSTR('payment detail processing for order# '||p_quote_number||' has failed:Error=>'||l_return_message,1,2000);
            RAISE e_biz_exception;
         END IF;
      END IF;
      --End Order Payment
      p_order_number   := p_quote_number;
      p_header_id      := l_header_id;
      p_return_status  := 'S';
      p_return_message := '';
      --log debug info
      IF NVL(g_xxwc_woc_debug,'N') = 'Y' THEN
         xxwc_checkitapp_pkg.xxwc_log_debug_info
                    (P_API_NAME         =>l_proc_name,
                     P_ORDER_NUMBER     =>p_quote_number,
                     P_DEBUG_TEXT       => 'End create_woc_order',
                     P_USER_ID => l_user_id
                    );
      END IF;
   EXCEPTION
      WHEN e_biz_exception THEN
         ROLLBACK;
         p_return_status  := l_return_status;
         p_return_message := l_return_message;
      WHEN OTHERS THEN
         ROLLBACK;
         p_return_status := 'E';
         p_return_message := SUBSTR(SQLERRM,1,2000);
   END create_woc_order;
   
/*************************************************************************
      PROCEDURE Name: OE_PAYMENTS_Insert

      PURPOSE:   This procedure will insert record into OE_PAYMENTS table

      REVISIONS:
      Ver        Date          Author             Description
      ---------  ------------  ------------       ------------------
      1.0        11/10/2017    Rakesh Patel       TMS#20171102-00074-Mobile apps new plsql procedures for WOC    
   ****************************************************************************/
PROCEDURE OE_PAYMENTS_Insert (
     p_header_id            IN  NUMBER
    ,p_payment_number       IN  NUMBER 
    ,p_receipt_method_id    IN  NUMBER
    ,p_receipt_number       IN  NUMBER
    ,p_payment_amount       IN  NUMBER
    ,p_payment_set_id       IN  NUMBER
    ,o_status               IN OUT VARCHAR2
    ,o_message              IN OUT VARCHAR2   
    )
 IS
      l_row_id             VARCHAR2(1000);
      l_userid               NUMBER;
      l_loginid               NUMBER;
      l_payment_type_code  OE_PAYMENTS.payment_type_code%TYPE;
      l_check_number       VARCHAR2(80);
      l_sqlcode            VARCHAR2(4000);
      l_sqlerrm            VARCHAR2(4000);
      l_message            VARCHAR2(4000);
      l_sec                VARCHAR2(4000); 
BEGIN 
    l_sec := 'Start OE_PAYMENTS_Insert'; 
    o_status := 'S';
    l_userid := FND_GLOBAL.USER_ID;
    l_loginid := FND_GLOBAL.LOGIN_ID;
    l_payment_type_code := 'CHECK';
    
    l_sec := 'Before insert into OE_PAYMENTS';      
    INSERT INTO OE_PAYMENTS
    (    payment_number
        ,payment_level_code
        ,payment_type_code
        ,HEADER_ID
        ,check_number
        ,receipt_method_id
        ,payment_collection_event
        ,payment_set_id
        ,payment_amount
        ,prepaid_amount
        ,CREATION_DATE
        ,CREATED_BY
        ,LAST_UPDATE_DATE
        ,LAST_UPDATED_BY
        ,LAST_UPDATE_LOGIN
     )
     VALUES
    (   p_payment_number
        ,'ORDER'
        ,l_payment_type_code
        ,p_header_id
        ,p_receipt_number
        ,p_receipt_method_id
        ,'PREPAY'
        ,p_payment_set_id
        ,p_payment_amount
        ,p_payment_amount
        ,sysdate
        ,l_userid
        ,sysdate
        ,l_userid
        ,l_loginid
        );
     l_sec := 'After insert into OE_PAYMENTS';     
EXCEPTION
    WHEN OTHERS THEN
       l_sqlcode := SQLCODE;
       l_sqlerrm := SUBSTR (SQLERRM, 1, 500);
       l_message := 'Error in OE_PAYMENTS_Insert ';
       o_status  := 'E';
       o_message := l_message || l_sqlcode || l_sqlerrm;
 
       xxcus_error_pkg.xxcus_error_main_api (  p_called_from         =>  UPPER(g_pkg_name)||'.'||UPPER('OE_PAYMENTS_Insert')
                                              , p_calling             => l_sec
                                              , p_ora_error_msg       => SQLERRM
                                              , p_error_desc          => l_message || l_sqlcode || l_sqlerrm
                                              , p_distribution_list   => g_dflt_email
                                              , p_module              => 'XXWC');
END OE_PAYMENTS_Insert;

/*************************************************************************
      PROCEDURE Name: XXWC_PROCESS_PAYMENT

      PURPOSE:   This procedure will call pre-payment oublic api to apply payment to Sales Order.

      REVISIONS:
      Ver        Date          Author             Description
      ---------  ------------  ------------       ------------------
      1.0        11/10/2017    Rakesh Patel       TMS#20171102-00074-Mobile apps new plsql procedures for WOC    
   ****************************************************************************/
PROCEDURE XXWC_PROCESS_PAYMENT
   (
     p_org_id               IN NUMBER
    ,p_order_number         IN  NUMBER
    ,p_header_id            IN  NUMBER
    ,p_invoice_to_org_id    IN  NUMBER
    ,p_sold_to_org_id       IN  NUMBER
    ,p_trans_curr_code      IN  VARCHAR2 
    ,p_conversion_type_code IN  VARCHAR2
    ,p_conversion_rate      IN  NUMBER  
    ,p_conversion_rate_date IN  DATE    
    ,p_receipt_method_id    IN  NUMBER
    ,p_receipt_number       IN  NUMBER
    ,p_payment_amount       IN  NUMBER
    ,p_payment_number       IN  NUMBER
    ,o_status               OUT VARCHAR2
    ,o_message              OUT VARCHAR2    
   )
IS
   l_sec                          VARCHAR2(3000);
   l_err_msg                      VARCHAR2(3000);
   l_return_status                VARCHAR2 (2000);
   l_msg_count                    NUMBER;
   l_msg_data                     VARCHAR2 (2000);
   -- PARAMETERS
   l_org                          VARCHAR2 (20)                          := '162'; -- OPERATING UNIT
   l_result_out                   VARCHAR2 (2000);
   l_header_id                    oe_order_headers_all.header_id%TYPE;
   l_order_number                 oe_order_headers_all.order_number%TYPE;
   l_receipt_number               ar_cash_receipts_all.receipt_number%TYPE;
   l_hdr_inv_to_cust_id           NUMBER;
   l_cr_id                        NUMBER;
   l_receivable_application_id    NUMBER;
   l_exchange_rate_type           VARCHAR2(30);
   l_exchange_rate                NUMBER;
   l_exchange_rate_date           DATE;
   l_currency_code                OE_GL_SETS_OF_BOOKS_V.CURRENCY_CODE%TYPE;
   l_call_payment_processor       VARCHAR2(30);
   l_remittance_bank_account_id   NUMBER;
   l_called_from                  VARCHAR2(30);
   l_secondary_application_ref_id NUMBER;
   l_bank_account_id              NUMBER;
   l_payment_server_order_num      VARCHAR2(80);
   l_approval_code                  VARCHAR2(80);
   l_trxn_id                      NUMBER;
   l_trxn_extension_id            NUMBER;
   l_receipt_method_id            NUMBER;
   l_set_of_books_id              NUMBER;
   l_payment_set_id               NUMBER;
   l_bank_acct_id                 NUMBER;
   l_payment_response_error_code  VARCHAR2(80);
   l_msg_text                     VARCHAR2(4000); 
   l_msg_index_out                NUMBER;
   lv_msg_data                    VARCHAR2 (4000);
BEGIN
  l_sec := 'Start XXWC_PROCESS_PAYMENT';
  
  l_header_id         := p_header_id;
  l_order_number      := p_order_number;
  l_receipt_method_id := p_receipt_method_id;
  l_receipt_number    := p_receipt_number;
  
  l_result_out := 'PASS' ;
  l_return_status := FND_API.G_RET_STS_SUCCESS;
  
  l_sec := 'Before fetching customer info';                           
      BEGIN
     SELECT acct_site.cust_account_id
        INTO   l_hdr_inv_to_cust_id
     FROM   hz_cust_acct_sites_all acct_site, hz_cust_site_uses_all site
     WHERE  SITE.SITE_USE_CODE = 'BILL_TO'
        AND SITE.CUST_ACCT_SITE_ID = ACCT_SITE.CUST_ACCT_SITE_ID
        AND SITE.SITE_USE_ID  = p_invoice_to_org_id;
    EXCEPTION
    WHEN OTHERS THEN
         l_hdr_inv_to_cust_id := p_sold_to_org_id;
    END;
   
    l_sec := 'Before fetching set_of_books_id';
  
    BEGIN
       SELECT hou.set_of_books_id, currency_code
         INTO l_set_of_books_id, l_currency_code
         FROM hr_operating_units hou, gl_sets_of_books gsob
        WHERE hou.organization_id = l_org
          AND gsob.set_of_books_id = hou.set_of_books_id;
    EXCEPTION
    WHEN OTHERS THEN
       l_currency_code := NULL;
       l_set_of_books_id := NULL;
    END; 

    l_sec := 'Before transactional_curr_code';
    IF p_trans_curr_code = l_currency_code THEN
       l_exchange_rate_type := null;
       l_exchange_rate := null;
       l_exchange_rate_date := null;
    ELSE
       l_exchange_rate_type := p_conversion_type_code;
       l_exchange_rate := p_conversion_rate;
       l_exchange_rate_date := p_conversion_rate_date;
    END IF;

    l_sec := 'Before fetching date from oe_payment';
    BEGIN            
       SELECT trxn_extension_id ,payment_set_id
         INTO l_trxn_extension_id, l_payment_set_id
         FROM OE_PAYMENTS 
         WHERE header_id = p_header_id
           AND payment_type_code = 'CHECK'
           AND receipt_method_id = l_receipt_method_id;
    EXCEPTION 
    WHEN OTHERS THEN
        l_trxn_extension_id := NULL;
        l_payment_set_id := null;
    END;  
          
    --IF l_trxn_extension_id IS NOT NULL THEN
       --l_call_payment_processor := FND_API.G_TRUE;
    --ELSE
      l_call_payment_processor := FND_API.G_FALSE;
    --END IF;
  
    l_sec := 'Before fetching bank remit info.';
    -- To get the remittance bank account id.
    BEGIN
       SELECT ba.bank_account_id
       INTO   l_remittance_bank_account_id
       FROM   ar_receipt_methods rm,
              ap_bank_accounts ba,
              ar_receipt_method_accounts rma ,
              ar_receipt_classes rc
       WHERE  rm.receipt_method_id = l_receipt_method_id
       and    rm.receipt_method_id = rma.receipt_method_id
       and    rc.receipt_class_id = rm.receipt_class_id
       and    rc.creation_method_code = 'AUTOMATIC'
       and    rma.remit_bank_acct_use_id = ba.bank_account_id
       and    ba.account_type = 'INTERNAL'
       and    ba.currency_code = decode(ba.receipt_multi_currency_flag, 'Y'
                                    ,ba.currency_code
                                    ,p_trans_curr_code)
       and    rma.primary_flag = 'Y';

    EXCEPTION WHEN NO_DATA_FOUND THEN
      null;
    END;
    
    l_sec := 'Before calling AR_PREPAYMENTS_PUB.create_prepayment';
    
    AR_PREPAYMENTS_PUB.create_prepayment(
            p_api_version                  => 1.0,
            p_commit                       => FND_API.G_FALSE,
            p_validation_level               => FND_API.G_VALID_LEVEL_FULL,
            x_return_status                   => l_return_status,--x_return_status
            x_msg_count                    => l_msg_count,-- x_msg_count
            x_msg_data                     => l_msg_data, --x_msg_data
            p_init_msg_list                => FND_API.G_TRUE,
            p_receipt_number               => l_receipt_number,
            p_amount                       => p_payment_amount, -- pending_amount,
            p_receipt_method_id            => l_receipt_method_id, 
            p_customer_id                  => l_hdr_inv_to_cust_id,  
            p_customer_site_use_id            => p_invoice_to_org_id,
            p_customer_bank_account_id        => l_bank_acct_id,
            p_currency_code                => p_trans_curr_code,
            p_exchange_rate                => l_exchange_rate,
            p_exchange_rate_type            => l_exchange_rate_type,
            p_exchange_rate_date           => l_exchange_rate_date,
            p_applied_payment_schedule_id  => -7,  -- hard coded.
            p_application_ref_type         => 'OM',--l_application_ref_type ,
            p_application_ref_num            => l_order_number,--l_application_ref_num, --Order Number 
            p_application_ref_id            => l_header_id, --l_application_ref_id, --Order Id 
            p_cr_id                        => l_cr_id, --OUT
            p_receivable_application_id    => l_receivable_application_id, --OUT
            p_call_payment_processor        => l_call_payment_processor,
            p_remittance_bank_account_id   => l_remittance_bank_account_id,
            p_called_from                   => 'OM',
            p_payment_server_order_num     => l_payment_server_order_num,
            p_approval_code                   => l_approval_code,
            p_secondary_application_ref_id => l_secondary_application_ref_id,
            p_payment_response_error_code  => l_payment_response_error_code,
            p_payment_set_id                 => l_payment_set_id,
            p_org_id                       => p_org_id,
            p_payment_trxn_extension_id    => l_trxn_extension_id
            );
            
      DBMS_OUTPUT.PUT_LINE( 'OEXVPPYB: AFTER AR CREATE_PREPAYMENT CASH_RECEIPT_ID IS: '||L_CR_ID ) ;
      DBMS_OUTPUT.PUT_LINE(  'OEXVPPYB: AFTER AR CREATE_PREPAYMENT PAYMENT_SET_ID IS: '||l_PAYMENT_SET_ID  ) ;
      DBMS_OUTPUT.PUT_LINE(  'OEXVPPYB: AFTER AR CREATE_PREPAYMENT CHECK NUMBER IS: '||l_RECEIPT_NUMBER  ) ;
      DBMS_OUTPUT.PUT_LINE(  'OEXVPPYB: AFTER AR CREATE_PREPAYMENT x_payment_response_error_code  IS: '||l_payment_response_error_code  ) ;
      DBMS_OUTPUT.PUT_LINE(  'OEXVPPYB: AFTER AR CREATE_PREPAYMENT approval_code IS: '||l_approval_code ) ;
      DBMS_OUTPUT.PUT_LINE(  'OEXVPPYB: AFTER AR CREATE_PREPAYMENT x_msg_count IS: '||l_msg_count  ) ;
      DBMS_OUTPUT.PUT_LINE(  'OEXVPPYB: AFTER AR CREATE_PREPAYMENT RETURN STATUS IS: '||l_RETURN_STATUS  ) ;
  
      IF l_return_status <> FND_API.G_RET_STS_SUCCESS THEN
        lv_msg_data := 'APT Error : ';
        FOR I IN 1..FND_MSG_PUB.Count_Msg LOOP
          l_msg_text := FND_MSG_PUB.Get(i,'F');
          lv_msg_data := lv_msg_data || ':' || SUBSTR(l_msg_text,1 , 500);
        END LOOP;
        
        IF lv_msg_data IS NOT NULL THEN
           o_message := lv_msg_data;
        END IF;
        o_status       :='E';  
        ROLLBACK;
      ELSE 
        
        l_sec := 'Before calling OE_PAYMENTS_Insert';
        OE_PAYMENTS_Insert (
                             p_header_id         => p_header_id
                            ,p_payment_number    => p_payment_number 
                            ,p_receipt_method_id => l_receipt_method_id
                            ,p_receipt_number    => l_receipt_number
                            ,p_payment_amount    => p_payment_amount
                            ,p_payment_set_id    => l_PAYMENT_SET_ID
                            ,o_status            => o_status
                            ,o_message           => o_message
                          );
                
        IF o_status <> 'S' THEN                     
          o_status  := 'E';
          o_message := o_message;
          ROLLBACK;
        ELSE  
          o_status  := 'S';
          o_message := o_message;
          COMMIT;
        END IF;  
        
     END IF;  

      RETURN;
    EXCEPTION
    WHEN OTHERS THEN

      DBMS_OUTPUT.put_line ('Error ...'||SQLERRM || 'l_sec'|| l_sec ); 

      xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => 'XXWC_OM_SO_CREATE.PROCESS_PAYMENT'
        ,p_calling             => l_sec
        ,p_request_id          => fnd_global.conc_request_id
        ,p_ora_error_msg       => SQLERRM
        ,p_error_desc          => 'Error processing payments'
        ,p_distribution_list   => g_dflt_email
        ,p_module              => 'XXWC');
       RETURN;
    END XXWC_PROCESS_PAYMENT; --end of procedure

/*************************************************************************
      PROCEDURE Name: XXWC_APPLY_PAYMENT_TO_SO

      PURPOSE:   This procedure will be called by mobile app to apply payment to sales order.

      REVISIONS:
      Ver        Date          Author             Description
      ---------  ------------  ------------       ------------------
      1.0        11/10/2017    Rakesh Patel       TMS#20171102-00074-Mobile apps new plsql procedures for WOC    
   ****************************************************************************/   
PROCEDURE XXWC_APPLY_PAYMENT_TO_SO
(
     p_order_header_rec     IN  xxwc.xxwc_order_header_rec
    ,p_order_payments_tbl   IN  XXWC.XXWC_ORDER_PAYMENTS_TBL
    ,o_status               OUT VARCHAR2
    ,o_message              OUT VARCHAR2
 )
IS
   l_proc_name          VARCHAR2(100):= g_pkg_name||'.'||'XXWC_APPLY_PAYMENT_TO_SO';
   l_sec     VARCHAR2(3000);
   l_err_msg VARCHAR2(3000);
   l_order_source_id              NUMBER;
   l_check_order_source_id        NUMBER;
   l_line_type_id                 NUMBER; 
   l_header_id                    NUMBER; 
   l_user_id                      apps.fnd_user.user_id%TYPE;
   l_user_name                    apps.fnd_user.user_name%TYPE; 
   l_org_id                       VARCHAR2 (20)                          := '162'; -- OPERATING UNIT
   l_resp_id                      NUMBER;
   l_resp_appl_id                 NUMBER;
   l_payment_number               NUMBER;  
   l_payment_type_code            apps.oe_payment_types_vl.payment_type_code%TYPE; 
   l_payment_type                 apps.oe_payment_types_vl.payment_type_code%TYPE;   
   l_receipt_method_id            apps.ar_receipt_methods.receipt_method_id%TYPE;   
   l_credit_card_type             VARCHAR2(50);
   l_invoice_to_org_id            oe_order_headers_all.invoice_to_org_id%TYPE;
   l_sold_to_org_id               oe_order_headers_all.sold_to_org_id%TYPE;
   l_trans_curr_code              oe_order_headers_all.transactional_curr_code%TYPE;   
   l_exchange_rate_type           oe_order_headers_all.conversion_type_code%TYPE;
   l_exchange_rate                oe_order_headers_all.conversion_rate%TYPE;
   l_exchange_rate_date           oe_order_headers_all.conversion_rate_date%TYPE;
BEGIN
   l_sec := 'Start APPLY_PAYMENT_TO_SO';
   l_header_id := NULL;
   
   IF p_order_header_rec.org_id IS NOT NULL THEN
      l_org_id := p_order_header_rec.org_id; 
   END IF;
   
   BEGIN 
      SELECT USER_ID
        INTO l_user_id
        FROM apps.fnd_user
       WHERE UPPER(USER_NAME) = UPPER(p_order_header_rec.created_by);
      EXCEPTION
      WHEN OTHERS THEN
         o_status := 'E';
         o_message := 'User NTID ' ||p_order_header_rec.created_by||' not exist in ERP';
         RETURN;
   END; 
   --debug info
    IF NVL(g_xxwc_woc_debug,'N') = 'Y' THEN
        xxwc_checkitapp_pkg.xxwc_log_debug_info
                    (P_API_NAME         =>l_proc_name,
                     P_ORDER_NUMBER     => p_order_header_rec.order_number,
                     P_DEBUG_TEXT       => 'Start XXWC_APPLY_PAYMENT_TO_SO',
                     P_USER_ID          => l_user_id
                    );
    END IF;
      
   IF IS_VALID_OM_USER ( l_user_id ) != 'Y' THEN
      o_message := 'Responsibility not assigned to NTID : ' ||p_order_header_rec.created_by||' to create/ update sales order ';
      o_status := 'E';
      RETURN;
   END IF;  
  
   IF p_order_header_rec.order_number IS NULL THEN
      o_message      := 'Sales Order number is not provided, please provide a validate order_number: '||p_order_header_rec.order_number;
      o_status       :='E';
      RETURN;
   END IF;
   
   IF p_order_header_rec.order_number IS NOT NULL THEN
      BEGIN
        SELECT header_id, order_source_id, invoice_to_org_id, sold_to_org_id, transactional_curr_code, conversion_type_code, conversion_rate,  conversion_rate_date
          INTO l_header_id ,l_order_source_id, l_invoice_to_org_id, l_sold_to_org_id , l_trans_curr_code, l_exchange_rate_type, l_exchange_rate, l_exchange_rate_date               
          FROM apps.oe_order_headers_all ooh
         WHERE ooh.order_number = TRIM(p_order_header_rec.order_number);
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
         o_message      := 'Sales Order '||p_order_header_rec.order_number||' not exist in ERP';
         o_status       :='E';
         RETURN;
      WHEN OTHERS THEN
         o_message      := 'Sales Order '||p_order_header_rec.order_number||' not exist in ERP';
         o_status       :='E';
          RETURN;
     END;
     DBMS_OUTPUT.put_line ('Sales Order header_id : '||l_header_id);
   END IF;
   DBMS_OUTPUT.put_line ('Sales Order order_source_id : '||l_order_source_id);
   
   BEGIN
     SELECT order_source_id
       INTO l_check_order_source_id
       FROM apps.oe_order_sources oos
      WHERE oos.enabled_flag = 'Y'
        AND UPPER(name) = 'CHECKIT';
   EXCEPTION
   WHEN NO_DATA_FOUND THEN
      o_message      := 'Sales Order Source CHECKIT not exist in ERP';
      o_status       :='E';
      RETURN;
   WHEN OTHERS THEN
      o_message      := 'Sales Order Source CHECKIT not exist in ERP';
      o_status       :='E';
      RETURN;
   END;
   
   IF l_check_order_source_id <> l_order_source_id THEN
     o_message      := 'You are not allowed to apply payment to this sales order, It is not created from CHECKIT app: '||p_order_header_rec.order_number;
     o_status       :='E';
     RETURN;
   END IF;

   -- INITIALIZATION REQUIRED FOR R12
   mo_global.set_policy_context ('S', l_org_id);
   mo_global.init ('ONT');

   l_resp_id      := 50857;
   l_resp_appl_id := 660;

   --INITIALIZE ENVIRONMENT
   fnd_global.apps_initialize (l_user_id, l_resp_id, l_resp_appl_id);

   -- INITIALIZE ACTION REQUEST RECORD
   IF p_order_payments_tbl.count <= 0 THEN
      o_message      := 'No records provided in the  p_order_payments_tbl';
      o_status       :='E';
      RETURN;

      DBMS_OUTPUT.put_line ('p_order_payments_tbl.count '||p_order_payments_tbl.count);
   ELSE
      FOR l_tbl_idx IN p_order_payments_tbl.first..p_order_payments_tbl.last LOOP
        
        IF p_order_payments_tbl(l_tbl_idx).PAYMENT_TYPE = 'CASH' THEN
           l_payment_type :=  'CASH' ;
        ELSIF p_order_payments_tbl(l_tbl_idx).PAYMENT_TYPE = 'CREDIT_CARD' THEN
          l_payment_type :=  'CHECK' ;
           
          BEGIN
             SELECT receipt_method_id
               INTO l_receipt_method_id
               FROM ar_receipt_methods arm
              WHERE receipt_class_id = 4006 --WC Credit Cards-Temporary-Manual
                AND arm.attribute2 = p_order_header_rec.branch_code 
                AND name like '%'||p_order_payments_tbl(l_tbl_idx).CREDIT_CARD_TYPE||'%'
                AND TRUNC(sysdate) BETWEEN TRUNC(NVL(arm.start_date, sysdate)) AND TRUNC(NVL(arm.end_date, sysdate));
          EXCEPTION WHEN NO_DATA_FOUND THEN
             o_status := 'E';
             o_status := 'Receipt method not found for branch ' || p_order_header_rec.branch_code || ' and credit card type ' ||p_order_payments_tbl(1).CREDIT_CARD_TYPE ;
             RETURN;
          END;
          DBMS_OUTPUT.put_line ('l_receipt_method_id : '||l_receipt_method_id);
          
          BEGIN
             SELECT MAX(payment_number)+1
               INTO l_payment_number
               FROM oe_payments arm
              WHERE header_id = l_header_id;
          EXCEPTION WHEN OTHERS THEN
             l_payment_number := l_tbl_idx;
          END;
          DBMS_OUTPUT.put_line ('l_receipt_method_id : '||l_receipt_method_id);
          
          IF l_payment_number IS NULL THEN
             l_payment_number := l_tbl_idx;
          END IF;
          DBMS_OUTPUT.put_line ('l_payment_number : '||l_payment_number);
          --debug info
          IF NVL(g_xxwc_woc_debug,'N') = 'Y' THEN
             xxwc_checkitapp_pkg.xxwc_log_debug_info
                    (P_API_NAME         => l_proc_name,
                     P_ORDER_NUMBER     => p_order_header_rec.order_number,
                     P_DEBUG_TEXT       => 'Start XXWC_PROCESS_PAYMENT',
                     P_USER_ID          => l_user_id
                    );
          END IF;
          XXWC_PROCESS_PAYMENT
                        ( p_org_id               => l_org_id
                         ,p_order_number         => p_order_header_rec.order_number
                         ,p_header_id            => l_header_id
                         ,p_invoice_to_org_id    => l_invoice_to_org_id
                         ,p_sold_to_org_id       => l_sold_to_org_id
                         ,p_trans_curr_code      => l_trans_curr_code  
                         ,p_conversion_type_code => l_exchange_rate_type 
                         ,p_conversion_rate      => l_exchange_rate
                         ,p_conversion_rate_date => l_exchange_rate_date
                         ,p_receipt_method_id    => l_receipt_method_id
                         ,p_receipt_number       => p_order_payments_tbl(l_tbl_idx).identifying_number
                         ,p_payment_amount       => p_order_payments_tbl(l_tbl_idx).payment_amount
                         ,p_payment_number       => l_payment_number
                         ,o_status               => o_status
                         ,o_message              => o_message 
                        );
          --debug info
          IF NVL(g_xxwc_woc_debug,'N') = 'Y' THEN
             xxwc_checkitapp_pkg.xxwc_log_debug_info
                    (P_API_NAME         => l_proc_name,
                     P_ORDER_NUMBER     => p_order_header_rec.order_number,
                     P_DEBUG_TEXT       => 'End XXWC_PROCESS_PAYMENT',
                     P_USER_ID          => l_user_id
                    );
          END IF;
        ELSE
           o_status := 'E';
           o_status := 'Invalid payment type for payment amount ' || p_order_payments_tbl(1).PAYMENT_AMOUNT;
        END IF;
     
      END LOOP; 

      RETURN;
    END IF;
     --debug info
     IF NVL(g_xxwc_woc_debug,'N') = 'Y' THEN
         xxwc_checkitapp_pkg.xxwc_log_debug_info
                    (P_API_NAME         => l_proc_name,
                     P_ORDER_NUMBER     => p_order_header_rec.order_number,
                     P_DEBUG_TEXT       => 'End XXWC_APPLY_PAYMENT_TO_SO',
                     P_USER_ID          => l_user_id
                    );
     END IF;
    EXCEPTION
    WHEN OTHERS THEN

      DBMS_OUTPUT.put_line ('Error ...'||SQLERRM);

      xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => 'XXWC_OM_SO_CREATE.APPLY_PAYMENT_TO_SO'
        ,p_calling             => l_sec
        ,p_request_id          => fnd_global.conc_request_id
        ,p_ora_error_msg       => SQLERRM
        ,p_error_desc          => 'Error creating text file with File Names'
        ,p_distribution_list   => g_dflt_email
        ,p_module              => 'XXWC');
       RETURN;
    END XXWC_APPLY_PAYMENT_TO_SO; --end of procedure

    /*************************************************************************
      PROCEDURE Name: get_customer_info_woc

      PURPOSE:   This procedure will be called by mobile app to search customer

      REVISIONS:
      Ver        Date          Author             Description
      ---------  ------------  ------------       ------------------
      1.0        11/30/2017    Niraj K Ranjan     TMS#20171102-00074-Mobile apps new plsql procedures for WOC    
   ****************************************************************************/
    FUNCTION get_customer_info_woc( p_cust_account_num            IN VARCHAR2
                                   ,p_cust_name_search_string     IN VARCHAR2
                                  ) return sys_refcursor IS
    v_cust_rc sys_refcursor;
    l_proc_name          VARCHAR2(100):= g_pkg_name||'.'||'GET_CUSTOMER_INFO_WOC';
    l_user_id            NUMBER := FND_PROFILE.VALUE('USER_ID');
    BEGIN
       mo_global.set_policy_context('S',162);
       --debug info
       IF NVL(g_xxwc_woc_debug,'N') = 'Y' THEN
          xxwc_checkitapp_pkg.xxwc_log_debug_info
                    (P_API_NAME         =>l_proc_name,
                     P_CUSTOMER_NUMBER  => p_cust_account_num,
                     P_SEARCH_STRING    => p_cust_name_search_string,
                     P_DEBUG_TEXT       => 'Start GET_CUSTOMER_INFO_WOC',
                     P_USER_ID => l_user_id
                    );
       END IF;
       OPEN v_cust_rc FOR 'SELECT 
                             hp.party_id
                            ,hp.party_name
                            ,hp.party_type
                            ,hp.primary_phone_contact_pt_id
                            ,hp.primary_phone_line_type
                            ,hp.primary_phone_country_code
                            ,hp.primary_phone_area_code
                            ,hp.primary_phone_number
                            ,hp.primary_phone_extension
                            ,hp.email_address
                            ,hca.cust_account_id
                            ,hca.account_name
                            ,hca.account_number
                            ,hca.attribute4
                            ,hca.attribute11
                            ,hca.status
                            ,hca.customer_type
                            ,hca.customer_class_code
                            --,hca.primary_salesrep_id
                            ,hca.account_established_date
                            ,hca.account_termination_date
                            ,hca.account_activation_date
                            ,hca.hold_bill_flag
                            ,loc.address1
                            ,loc.address2
                            ,loc.address3
                            ,loc.address4
                            ,loc.city
                            ,loc.state
                            ,loc.province
                            ,loc.country
                            ,loc.county
                            ,loc.Postal_Code
                            ,SITE.location SITE_NAME
                            ,(select PRIMARY_SALESREP_ID
                                   from hz_Cust_site_uses_all
                                 where site_use_code = ''BILL_TO''
                                  and cust_acct_site_id = acct_site.cust_acct_site_id
                                  AND status=''A'')  PRIMARY_SALESREP_ID
                              ,XXWC_B2B_SO_IB_PKG.get_sales_rep (hca.cust_account_id ) sales_rep_name
                              ,XXWC_OM_SO_CREATE.get_sales_rep_id (hca.cust_account_id ) sales_rep_id
                            ,decode(SUBSTR(hp.PARTY_NAME,1,4),''CASH'',''Y'',''N'') CASH_ACCT_IND
                            ,NVL(hca.attribute7,''N'') AUTH_BUYER
                      FROM hz_cust_accounts hca,
                           hz_parties hp,
                           HZ_LOCATIONS               LOC,
                           HZ_PARTY_SITES             PARTY_SITE,
                           HZ_CUST_ACCT_SITES         ACCT_SITE,
                           HZ_CUST_SITE_USES_ALL      SITE
                      WHERE 1=1
                      AND SITE.SITE_USE_CODE         = ''SHIP_TO''
                      AND hca.party_id = hp.party_id
                          AND   SITE.CUST_ACCT_SITE_ID     = ACCT_SITE.CUST_ACCT_SITE_ID
                          AND   ACCT_SITE.PARTY_SITE_ID    = PARTY_SITE.PARTY_SITE_ID
                          AND   PARTY_SITE.LOCATION_ID     = LOC.LOCATION_ID
                          AND   ACCT_SITE.CUST_ACCOUNT_ID=hca.CUST_ACCOUNT_ID
                          and    acct_site.status=''A''
                          and   SITE.PRIMARY_FLAG = ''Y''
                      AND   hp.status = ''A''
                      AND hca.status = ''A''
                      AND HCA.account_number = NVL(:p_cust_account_num,HCA.account_number)
                      AND UPPER(HP.PARTY_NAME) LIKE UPPER(''%''||:p_cust_name_search_string||''%'')'
                USING p_cust_account_num, p_cust_name_search_string;
       --debug info
       IF NVL(g_xxwc_woc_debug,'N') = 'Y' THEN
          xxwc_checkitapp_pkg.xxwc_log_debug_info
                    (P_API_NAME         =>l_proc_name,
                     P_CUSTOMER_NUMBER  => p_cust_account_num,
                     P_SEARCH_STRING    => p_cust_name_search_string,
                     P_DEBUG_TEXT       => 'End GET_CUSTOMER_INFO_WOC',
                     P_USER_ID          => l_user_id
                    );
       END IF;
                RETURN v_cust_rc; 
       
    EXCEPTION
       WHEN OTHERS THEN
          RETURN v_cust_rc; 
    END get_customer_info_woc;
    
    /*************************************************************************
      PROCEDURE Name: get_customer_info_woc

      PURPOSE:   This procedure will be called by mobile app to search customer

      REVISIONS:
      Ver        Date          Author             Description
      ---------  ------------  ------------       ------------------
      1.0        11/30/2017    Niraj K Ranjan     TMS#20171102-00074-Mobile apps new plsql procedures for WOC    
   ****************************************************************************/
    FUNCTION get_jobsite_info_woc( p_cust_account_id            IN NUMBER
                                  ,p_jobsite_name_search_string IN VARCHAR2
                                  ) RETURN sys_refcursor IS
    v_site_rc sys_refcursor;
    l_proc_name          VARCHAR2(100):= g_pkg_name||'.'||'GET_JOBSITE_INFO_WOC';
    l_user_id            NUMBER := FND_PROFILE.VALUE('USER_ID');
    BEGIN
       mo_global.set_policy_context('S',162);
       --debug info
       IF NVL(g_xxwc_woc_debug,'N') = 'Y' THEN
          xxwc_checkitapp_pkg.xxwc_log_debug_info
                    (P_API_NAME         => l_proc_name,
                     P_CUSTOMER_ID      => p_cust_account_id,
                     P_SEARCH_STRING    => p_jobsite_name_search_string,
                     P_DEBUG_TEXT       => 'Start GET_JOBSITE_INFO_WOC',
                     P_USER_ID          => l_user_id
                    );
       END IF;
       OPEN v_site_rc FOR 'SELECT hp.party_id
                                 ,hca.cust_account_id
                                 ,hca.account_number
                                 ,hl.location_id
                                 ,hl.country
                                 ,hl.address1
                                 ,hl.address2
                                 ,hl.address3
                                 ,hl.address4
                                 ,hl.city
                                 ,hl.postal_code
                                 ,hl.state
                                 ,hl.province
                                 ,hl.county
                                 ,hl.validated_flag
                                 ,hps.party_site_id
                                 ,hps.party_site_number
                                 ,hcas.cust_acct_site_id
                                 ,hcas.bill_to_flag
                                 ,hcas.ship_to_flag
                                 ,hcas.attribute_category
                                 --,hcas.attribute1
                                 --,hcas.attribute3
                                 --,hcas.attribute15
                                 ,hcsu.site_use_id
                                 ,hcsu.site_use_code
                                 ,hcsu.primary_flag
                                 ,hcsu.location
                                 ,hcsu.bill_to_site_use_id
                                 ,hcsu.gsa_indicator
                                 --,hcsu.attribute2
                                 ,hcsu.ship_sets_include_lines_flag
                                 ,hcsu.arrivalsets_include_lines_flag
                                 ,hcsu.sched_date_push_flag
                     FROM hz_parties hp,
                          hz_cust_accounts hca,
                          hz_locations hl,
                          hz_party_sites hps,
                          hz_cust_acct_sites_all hcas,
                          HZ_CUST_SITE_USES_ALL hcsu
                     WHERE 1=1
                     AND hca.cust_account_id = :p_cust_account_id
                     AND hp.party_id = hca.party_id
                     AND HL.LOCATION_ID = HPS.LOCATION_ID
                     AND hp.party_id = hps.party_id
                     AND hca.cust_account_id = hcas.cust_account_id
                     AND hps.party_site_id = hcas.party_site_id
                     AND hcas.CUST_ACCT_SITE_ID = hcsu.CUST_ACCT_SITE_ID
                     AND hp.Status =''A''
                     AND hca.Status =''A''
                     AND hps.Status =''A''
                     AND hcas.Status =''A''
                     AND hcsu.status = ''A''
                     AND UPPER(hcsu.location) LIKE UPPER(''%''||:p_jobsite_name_search_string||''%'')'
              USING p_cust_account_id, p_jobsite_name_search_string;
       --debug info
       IF NVL(g_xxwc_woc_debug,'N') = 'Y' THEN
          xxwc_checkitapp_pkg.xxwc_log_debug_info
                    (P_API_NAME         => l_proc_name,
                     P_CUSTOMER_ID      => p_cust_account_id,
                     P_SEARCH_STRING    => p_jobsite_name_search_string,
                     P_DEBUG_TEXT       => 'End GET_JOBSITE_INFO_WOC',
                     P_USER_ID          => l_user_id
                    );
       END IF;
              RETURN v_site_rc;
    EXCEPTION
       WHEN OTHERS THEN
          RETURN v_site_rc; 
    END get_jobsite_info_woc;

    /*************************************************************************
      PROCEDURE Name: get_woc_order_info

      PURPOSE:   This procedure will be called by mobile app to search order

      REVISIONS:
      Ver        Date          Author             Description
      ---------  ------------  ------------       ------------------
      1.0        12/07/2017    Niraj K Ranjan     TMS#20171102-00074-Mobile apps new plsql procedures for WOC    
   ****************************************************************************/
    FUNCTION get_woc_order_info(  --p_ntid         IN VARCHAR2
                                p_order_number IN NUMBER
                               ) return sys_refcursor IS
   v_rec sys_refcursor;
   l_org    VARCHAR2 (20)  := '162'; -- OPERATING UNIT
   l_cust_account_id  NUMBER;
   l_proc_name          VARCHAR2(100):= g_pkg_name||'.'||'GET_WOC_ORDER_INFO';
   l_user_id            NUMBER := FND_PROFILE.VALUE('USER_ID');
   BEGIN
     mo_global.set_policy_context ('S', l_org);
     --debug info
     IF NVL(g_xxwc_woc_debug,'N') = 'Y' THEN
        xxwc_checkitapp_pkg.xxwc_log_debug_info
                    (P_API_NAME         =>l_proc_name,
                     P_ORDER_NUMBER     => p_order_number,
                     P_SEARCH_STRING    => p_order_number,
                     P_DEBUG_TEXT       => 'Start GET_WOC_ORDER_INFO',
                     P_USER_ID          => l_user_id
                    );
     END IF;
     IF p_order_number IS NOT NULL THEN
       OPEN
       v_rec for
       'SELECT ooh.order_number
      ,ooh.sales_document_name order_name
      ,ooh.request_date--TO_CHAR (ooh.request_date, ''MM/DD/YYYY'') request_date
      ,ooh.ordered_date--TO_CHAR (ooh.quote_date, ''MM/DD/YYYY'') quote_date
      ,ooh.expiration_date
      ,cust_acct.account_number cust_account_code
      ,ooh.header_id
      ,ooh.creation_date
      ,ot.transaction_type_id order_type_id
      ,ot.name order_type
      ,ooh.order_source_id
      ,oos.name order_source_type
      ,ooh.sold_to_org_id
      ,ooh.ship_to_org_id
      ,ooh.invoice_to_org_id
      ,ooh.ship_from_org_id
      ,ooh.booked_flag
      ,ooh.salesrep_id
      ,XXWC_GEN_ROUTINES_PKG.salesman(ooh.salesrep_id,''SHORT'') SALES_REP_NAME
      ,term.NAME terms
      ,ooh.shipping_method_code
      ,wcs.ship_method_meaning
      ,ooh.freight_carrier_code
      ,ooh.cust_po_number
      ,ooh.flow_status_code
      ,party.party_name customer_name
      ,NULL email_address
      ,ship_su.location
      ,ship_ps.party_site_number
      ,(ship_su.location  || '', ''|| ship_ps.party_site_number) Ship_num
      ,ship_loc.address1 ship_address1
      ,ship_loc.address2 ship_address2
      ,ship_loc.address3 ship_address3
      ,ship_loc.address4 ship_address4
      ,DECODE (ship_loc.city, NULL, NULL, ship_loc.city || '', '')
                || DECODE (ship_loc.state,
                           NULL, ship_loc.province || '', '',
                           ship_loc.state || '', '')
                || DECODE (ship_loc.postal_code, NULL, NULL, ship_loc.postal_code)
                   ship_address5
      ,ship_loc.city     ship_city
      ,ship_loc.state    ship_state
      ,ship_loc.postal_code ship_to_postal_code
      ,bill_loc.address1 billto_address1
      ,bill_loc.address2 billto_address2
      ,bill_loc.address3 billto_address3
      ,bill_loc.address4 billto_address4
      ,DECODE (bill_loc.city, NULL, NULL, bill_loc.city || '', '')
                || DECODE (bill_loc.state,
                           NULL, bill_loc.province || '', '',
                           bill_loc.state || '', '')
                || DECODE (bill_loc.postal_code, NULL, NULL, bill_loc.postal_code)
                   billto_address5
      ,bill_loc.city     billto_city
      ,bill_loc.state    billto_state
      ,bill_loc.postal_code billto_postal_code
      ,bill_hcp.phone_area_code
      ,bill_hcp.phone_number
      ,CASE WHEN INSTR (bill_hcp.phone_number, ''-'', 4) = 0 THEN bill_hcp.phone_area_code
                || ''-''
                || SUBSTR (bill_hcp.phone_number, 1, 3)
                || ''-''
                || SUBSTR (bill_hcp.phone_number, 4, 7)
                ELSE bill_hcp.phone_area_code
                || ''-''
                || bill_hcp.phone_number 
                
                END as billto_phone
      ,ooh.transactional_curr_code currency_code
      ,ooh.fob_point_code
      ,ooh.freight_terms_code
      ,ooh.ship_to_contact_id
      ,TRIM(ship_party.person_title || ''  '' || ship_party.PERSON_FIRST_NAME || ''  '' || ship_party.PERSON_LAST_NAME) SHIP_TO_CONTACT
      ,ooh.invoice_to_contact_id
      ,XXWC_OM_SO_CREATE.get_job_site_contact(p_ship_to_contact_id => ooh.ship_to_contact_id) SHIP_TO_PHONE
      ,XXWC_OM_SO_CREATE.Get_Sold_to_contact_phone(p_sold_to_contact_id =>  ooh.sold_to_contact_id ) SOLD_TO_PHONE
      ,TRIM(invoice_party.person_title || ''  '' || invoice_party.PERSON_FIRST_NAME || ''  '' || invoice_party.PERSON_LAST_NAME) INVOICE_TO_CONTACT
      ,hl.LOCATION_CODE
      ,hl.address_line_1 branch_street
      ,DECODE (hl.town_or_city, NULL, NULL, hl.town_or_city || '',''||'''')
      || DECODE (hl.region_2, NULL, hl.region_2 || '',''||'' , hl.region_2 || '',''||'' )
      || DECODE (hl.postal_code, NULL, NULL, hl.postal_code) branch_city
      ,hl.telephone_number_1 branch_tel1
      ,hl.telephone_number_2 branch_tel2
      ,XXWC_GEN_ROUTINES_PKG.TAKENBY(ooh.created_by,''SHORT'') created_by
      ,ship_su.attribute3 map
      ,XXWC_OM_SO_CREATE.Payment_term(ooh.payment_term_id) CCTDHR
      ,ooh.attribute11 auth_buyer_id
      ,CUST_ACCT.attribute7 AUTH_BUYER
      ,XXWC_OM_SO_CREATE.get_auth_buyer (CUST_ACCT.attribute7, ooh.attribute11) AUTH_BUYER_NAME
      ,ooh.ORIG_SYS_DOCUMENT_REF ORDER_SOURCE_REFERENCE
      ,ROUND(NVL(oe_totals_grp.get_order_total (ooh.header_id, NULL, ''LINES''),0), 2) LINES_TOTAL
      ,ROUND(NVL(oe_totals_grp.get_order_total (ooh.header_id, NULL, ''CHARGES''),0), 2) CHARGES_TOTAL
      ,(ROUND(NVL(oe_totals_grp.get_order_total (ooh.header_id, NULL, ''TAXES''),0) -
        NVL(XXWC_OM_CFD_PKG.secondary_tax(ooh.HEADER_ID , ooh.ship_to_org_id , ooh.ship_from_org_id, NULL, NULL),0), 2))  TAXES_TOTAL
      ,ROUND(NVL(XXWC_OM_CFD_PKG.secondary_tax(ooh.HEADER_ID , ooh.ship_to_org_id , ooh.ship_from_org_id, NULL, NULL),0),2) LUMBER_TAXES
      ,XXWC_OM_SO_CREATE.GET_LUMBER_TAX_RATE LUMBER_TAX_RATE
      ,ROUND(NVL(oe_totals_grp.get_order_total (ooh.header_id, NULL, ''ALL''),0), 2) QUOTE_TOTAL
      ,ooh.SHIPPING_INSTRUCTIONS SHIPPING_INSTRUCTIONS
      ,XXWC_OM_SO_CREATE.GET_DOC_HEADER(ooh.header_id) DOC_HEADER
      ,ROUND(XXWC_OM_SO_CREATE.GET_UNITWGT_TOTAL(ooh.header_id),2) UNITWGT_TOTAL
      FROM oe_order_headers_all ooh
       , apps.oe_order_sources oos
       , oe_transaction_types_tl ot
       , hz_cust_accounts cust_acct
       , hz_parties party
       , wsh_carrier_services_v wcs
       , hz_cust_site_uses_all ship_su
       , hz_cust_acct_sites_all ship_cas
       , hz_party_sites ship_ps
       , hz_locations ship_loc
       , hz_cust_site_uses_all bill_su
       , hz_cust_acct_sites_all bill_cas
       , hz_party_sites bill_ps
       , hz_locations bill_loc
       , hz_contact_points bill_hcp
       , hz_cust_account_roles ship_roles
       , hz_relationships ship_rel
       , hz_parties ship_party
       , hz_cust_accounts ship_acct
       , hz_cust_account_roles invoice_roles
       , hz_parties invoice_party
       , hz_relationships invoice_rel
       , hz_cust_accounts invoice_acct
       , hr_all_organization_units ship_from_org
       , hr_locations_all hl
      -- , fnd_user fu
       , ra_terms_tl term
      WHERE ooh.order_source_id             = oos.order_source_id
        AND ooh.order_type_id               = ot.transaction_type_id
        AND ooh.sold_to_org_id              = cust_acct.cust_account_id
        AND cust_acct.party_id              = party.party_id(+)
        AND wcs.ship_method_code            = ooh.shipping_method_code
        AND ooh.ship_to_org_id              = ship_su.site_use_id(+)
        AND ship_su.cust_acct_site_id       = ship_cas.cust_acct_site_id(+)
        AND ship_cas.party_site_id          = ship_ps.party_site_id(+)
        AND ship_loc.location_id(+)         = ship_ps.location_id
        AND ooh.invoice_to_org_id           = bill_su.site_use_id(+)
        AND bill_su.cust_acct_site_id       = bill_cas.cust_acct_site_id(+)
        AND bill_cas.party_site_id          = bill_ps.party_site_id(+)
        AND bill_loc.location_id(+)         = bill_ps.location_id
        AND bill_hcp.owner_table_name(+)    = ''HZ_PARTY_SITES''
        AND bill_hcp.primary_flag(+)        = ''Y''
        AND bill_hcp.owner_table_id(+)      = bill_ps.party_site_id
        AND ooh.ship_to_contact_id          = ship_roles.cust_account_role_id(+)
        AND ship_roles.party_id             = ship_rel.party_id(+)
        AND ship_roles.role_type(+)         = ''CONTACT''
        AND ship_roles.cust_account_id      = ship_acct.cust_account_id(+)
        AND NVL (ship_rel.object_id, -1)    = NVL (ship_acct.party_id, -1)
        AND ship_rel.subject_id             = ship_party.party_id(+)
        AND ooh.invoice_to_contact_id       = invoice_roles.cust_account_role_id(+)
        AND invoice_roles.party_id          = invoice_rel.party_id(+)
        AND invoice_roles.role_type(+)      = ''CONTACT''
        AND invoice_roles.cust_account_id   = invoice_acct.cust_account_id(+)
        AND NVL (invoice_rel.object_id, -1) = NVL (invoice_acct.party_id, -1)
        AND invoice_rel.subject_id          = invoice_party.party_id(+)
        AND ooh.ship_from_org_id            = ship_from_org.organization_id(+)
        AND ship_from_org.location_id       = hl.location_id
      --  AND ooh.created_by                  = fu.user_id
        AND oos.name                        = ''CHECKIT'' 
        AND ooh.payment_term_id             = term.term_id(+)
        AND term.LANGUAGE(+)                = USERENV (''LANG'')
        --AND UPPER(fu.user_name)            = UPPER(:p_ntid)
        --AND ooh.quote_number               = 25798070;--:p_quote_number
        AND ooh.order_number               = :p_order_number'
        using p_order_number;
     END IF;
     --debug info
     IF NVL(g_xxwc_woc_debug,'N') = 'Y' THEN
        xxwc_checkitapp_pkg.xxwc_log_debug_info
                    (P_API_NAME         =>l_proc_name,
                     P_ORDER_NUMBER     => p_order_number,
                     P_SEARCH_STRING    => p_order_number,
                     P_DEBUG_TEXT       => 'End GET_WOC_ORDER_INFO',
                     P_USER_ID          => l_user_id
                    );
     END IF;
     RETURN v_rec;
     
     CLOSE v_rec;
    EXCEPTION   
    WHEN OTHERS  THEN
      RETURN NULL;      
   END get_woc_order_info;
   
   /*************************************************************************
      PROCEDURE Name: get_order_line_info

      PURPOSE:   This procedure will be called by mobile app to search order line

      REVISIONS:
      Ver        Date          Author             Description
      ---------  ------------  ------------       ------------------
      1.0        12/07/2017    Niraj K Ranjan     TMS#20171102-00074-Mobile apps new plsql procedures for WOC    
   ****************************************************************************/
   FUNCTION get_woc_order_line_info(  p_header_id     IN NUMBER
                                   ) return sys_refcursor IS
   v_rec sys_refcursor;
   l_org    VARCHAR2 (20)  := '162'; -- OPERATING UNIT
   l_proc_name          VARCHAR2(100):= g_pkg_name||'.'||'GET_WOC_ORDER_LINE_INFO';
   l_user_id            NUMBER := FND_PROFILE.VALUE('USER_ID');
   --l_cust_account_id  NUMBER;
   BEGIN

      mo_global.set_policy_context ('S', l_org);
      mo_global.init ('ONT');

      --debug info
      IF NVL(g_xxwc_woc_debug,'N') = 'Y' THEN
         xxwc_checkitapp_pkg.xxwc_log_debug_info
                    (P_API_NAME         =>l_proc_name,
                     P_ORDER_HEADER_ID  => p_header_id,
                     P_SEARCH_STRING    => p_header_id,
                     P_DEBUG_TEXT       => 'Start GET_WOC_ORDER_LINE_INFO',
                     P_USER_ID => l_user_id
                    );
     END IF;
     OPEN
       v_rec for
       'SELECT ool.line_id
              ,ool.line_number|| ''.'' || ool.shipment_number LINE_NUMBER
              ,ool.ordered_item SEGMENT1
              ,msib.description part_description
              ,ool.ship_from_org_id    line_ship_from_org_id
              ,ool.inventory_item_id
              ,NVL (ool.ordered_quantity, 0) QTY_ORDERED 
              ,XXWC_OM_SO_CREATE.get_onhand (      p_inventory_item_id => ool.inventory_item_id
                                ,p_organization_id  =>  ool.ship_from_org_id
                                ,p_base_uom         => msib.primary_unit_of_measure
                                ,p_ord_uom          => um.unit_of_measure
                                ,p_return_type      =>  ''H''
                                ) onhand
              ,XXWC_OM_SO_CREATE.get_onhand (      p_inventory_item_id => ool.inventory_item_id
                                ,p_organization_id  =>  ool.ship_from_org_id
                                ,p_base_uom         => msib.primary_unit_of_measure
                                ,p_ord_uom          => um.unit_of_measure
                                ,p_return_type      =>  ''T''
                                ) available
              ,ool.attribute11 Forceship
              ,ool.shipped_quantity QTY_SHIPPED
              ,ool.cancelled_quantity
              ,ool.cancelled_quantity
              ,ool.fulfilled_quantity
              ,ROUND(NVL (ool.unit_list_price, 0), 2) unit_list_price 
              ,ROUND(NVL (ool.unit_selling_price, 0) ,2) unit_selling_price 
              ,ROUND(NVL (ool.ordered_quantity, 0) * NVL (ool.unit_selling_price, 0) ,2) extended_price
              ,ROUND(NVL (ool.tax_value, 0), 2) tax_value
              ,ool.ORDER_QUANTITY_UOM UOM
              ,ool.shipping_quantity_uom
              ,ool.line_type_id
              ,ool.ship_to    line_ship_to
              ,ool.ship_from_location line_ship_from_location
              ,ool.ship_to_address1    line_ship_to_address1
              ,ool.ship_to_address2 line_ship_to_address2
              ,ool.deliver_to line_deliver_to
              ,ool.deliver_to_location    line_deliver_to_location
              ,ool.deliver_to_address1    line_deliver_to_address1
              ,ool.invoice_to line_invoice_to
              ,ool.invoice_to_location    line_invoice_to_location
              ,ool.invoice_to_address1    line_invoice_to_address1
              ,ool.invoice_to_address2 line_invoice_to_address2
              ,ool.flow_status_code line_status
              ,ool.user_item_description
              ,(msib.unit_weight|| '' '' ||weight_uom_code) unit_weight
              ,( SELECT  LISTAGG(l.segment1, ''|''||'' '') WITHIN GROUP (ORDER BY l.segment1)
                   FROM mtl_item_locations l
                  , mtl_secondary_locators_all_v v
                WHERE l.inventory_location_id = v.secondary_locator
                  AND v.inventory_item_id     = msib.inventory_item_id
                  AND v.organization_id       = ool.ship_from_org_id) LOCATORS
              ,xxwc_om_cfd_pkg.vendor_part_num(ool.inventory_item_id) vendor_part_num
              ,um.unit_of_measure ord_uom
              ,msib.primary_unit_of_measure base_uom
              ,round(NVL(ool.ordered_quantity,0)*NVL(ool.unit_selling_price,0), 2) LINE_TOTAL
              ,msib.hazard_class_id
              ,DECODE(msib.hazard_class_id, NULL, NULL, ''HAZMAT'') HAZMAT
              ,xxwc_om_cfd_pkg.get_country_of_origin(ool.INVOICE_TO_ORG_ID, ool.inventory_item_id) country_of_origin
              ,ool.SHIPPING_INSTRUCTIONS SHIPPING_INSTRUCTIONS
              ,DECODE(msib.item_type, ''SPECIAL'', ''THIS ITEM IS SPECIAL ORDER AND MAY BE NON-RETURNABLE'', NULL) SPECIAL
              ,XXWC_OM_SO_CREATE.GET_UN_NUMBER( p_inventory_item_id => ool.inventory_item_id
                                               ,p_ship_from_org_id  => ool.ship_from_org_id) HAZMAT_UN_NUMBER
              ,XXWC_OM_CFD_PKG.notify_gsa(ool.header_id,  ool.line_id) gsa_notify                                 
         FROM oe_order_lines_v ool
            , mtl_system_items_b msib
            , mtl_units_of_measure_vl um
        WHERE 1=1
          AND msib.inventory_item_id = ool.inventory_item_id
          AND msib.organization_id   = ool.ship_from_org_id
          AND ool.order_quantity_uom = um.uom_code
          AND ool.header_id          = :p_header_id
          ORDER BY ool.line_number'
       using p_header_id;
       --debug info
       IF NVL(g_xxwc_woc_debug,'N') = 'Y' THEN
           xxwc_checkitapp_pkg.xxwc_log_debug_info
                    (P_API_NAME         =>l_proc_name,
                     P_ORDER_HEADER_ID  => p_header_id,
                     P_SEARCH_STRING    => p_header_id,
                     P_DEBUG_TEXT       => 'End GET_WOC_ORDER_LINE_INFO',
                     P_USER_ID          => l_user_id
                    );
       END IF;
       RETURN v_rec;
   
       CLOSE v_rec;
   EXCEPTION   
    WHEN OTHERS  THEN
      RETURN NULL;    
   END get_woc_order_line_info;
   /*************************************************************************
      PROCEDURE Name: CREATE_WOC_ITEM

      PURPOSE:   This procedure will be called by mobile app to create item

      REVISIONS:
      Ver        Date          Author             Description
      ---------  ------------  ------------       ------------------
      1.0        12/07/2017    Niraj K Ranjan     TMS#20171102-00074-Mobile apps new plsql procedures for WOC    
   ****************************************************************************/
   PROCEDURE CREATE_WOC_ITEM (
      p_Parts_On_Fly_Rec   IN OUT NOCOPY xxwc_Parts_On_Fly_Rec,
      p_NTID                   IN VARCHAR2, 
      x_Return_Status         OUT NOCOPY VARCHAR2,
      x_Msg_Data              OUT NOCOPY VARCHAR2,
      x_Msg_Count             OUT NOCOPY NUMBER,
      x_part_number           OUT NOCOPY VARCHAR2,
      x_inventory_item_id     OUT NOCOPY NUMBER,
      x_list_price            OUT NOCOPY NUMBER,
      x_primary_uom_code      OUT NOCOPY VARCHAR2
    ) IS
      l_PARTS_ON_FLY_REC  XXWC_INV_PARTS_ON_FLY_PKG.PARTS_ON_FLY_REC;
      l_responsibility_key FND_RESPONSIBILITY_VL.RESPONSIBILITY_KEY%TYPE := 'XXWC_ORDER_MGMT_PRICING_FULL';
      l_user_id number;
      L_resp_id number;
      L_appl_id number;
      l_return_status VARCHAR2(1);
      l_return_message VARCHAR2(2000);
      e_biz_exception EXCEPTION;
      l_proc_name VARCHAR2(100) := g_pkg_name||'.'||'CREATE_WOC_ITEM';
   BEGIN
      BEGIN 
         SELECT USER_ID
         INTO l_user_id
         FROM apps.fnd_user
        WHERE UPPER(USER_NAME) = UPPER(p_ntid);
      EXCEPTION
         WHEN OTHERS THEN
            l_return_status := 'E';
            l_return_message := 'User NTID ' ||p_ntid||' does not exist in ERP';
            RAISE e_biz_exception;
      END;
      BEGIN 
         SELECT responsibility_id,application_id 
         INTO L_resp_id,L_appl_id
         FROM fnd_responsibility_vl 
         WHERE responsibility_key = l_responsibility_key;
      EXCEPTION
         WHEN OTHERS THEN
            l_return_status := 'E';
            l_return_message := 'Responsibility '||l_responsibility_key||' does not exist in ERP';
            RAISE e_biz_exception;
      END;
      Fnd_global.Apps_initialize (L_user_id, L_resp_id, L_appl_id);
      Mo_global.Init ('ONT');
      Mo_global.Set_policy_context ('S', 162);
      --debug info
      IF NVL(g_xxwc_woc_debug,'N') = 'Y' THEN
         xxwc_checkitapp_pkg.xxwc_log_debug_info
                    (P_API_NAME         =>l_proc_name,
                     P_ITEM_NUMBER     =>p_Parts_On_Fly_Rec.item_number,
                     P_DEBUG_TEXT       => 'Start CREATE_WOC_ITEM',
                     P_USER_ID => l_user_id
                    );
      END IF;
   l_PARTS_ON_FLY_REC.item_number                  := p_Parts_On_Fly_Rec.item_number;
   l_PARTS_ON_FLY_REC.item_description             := p_Parts_On_Fly_Rec.item_description;
   l_PARTS_ON_FLY_REC.mst_organization_id          := p_Parts_On_Fly_Rec.mst_organization_id;--Master Organization id
   l_PARTS_ON_FLY_REC.assgn_organization_id        := p_Parts_On_Fly_Rec.assgn_organization_id;--assign Organization id
   l_parts_on_fly_rec.price_list_header_id         := p_parts_on_fly_rec.price_list_header_id;
   l_parts_on_fly_rec.item_type_code               := p_parts_on_fly_rec.item_type_code;
   l_parts_on_fly_rec.item_template_id             := p_parts_on_fly_rec.item_template_id;
   l_parts_on_fly_rec.primary_uom_code             := p_parts_on_fly_rec.primary_uom_code;
   l_parts_on_fly_rec.weight_uom_code              := p_parts_on_fly_rec.weight_uom_code;
   l_parts_on_fly_rec.unit_weight                  := p_parts_on_fly_rec.unit_weight;
   l_parts_on_fly_rec.buyer_id                     := p_parts_on_fly_rec.buyer_id;
   l_parts_on_fly_rec.vendor_id                    := p_parts_on_fly_rec.vendor_id;
   l_parts_on_fly_rec.vendor_site_id               := p_parts_on_fly_rec.vendor_site_id;
   l_parts_on_fly_rec.manufacturer_part_number     := p_parts_on_fly_rec.manufacturer_part_number;
   l_parts_on_fly_rec.org_id                       := p_parts_on_fly_rec.org_id;
   l_parts_on_fly_rec.category_id                  := p_parts_on_fly_rec.category_id;
   l_parts_on_fly_rec.hazardous_material_flag      := p_parts_on_fly_rec.hazardous_material_flag;
   l_parts_on_fly_rec.hazard_class_id              := p_parts_on_fly_rec.hazard_class_id;
   l_parts_on_fly_rec.list_price                   := p_parts_on_fly_rec.list_price;
   l_parts_on_fly_rec.market_price                 := p_parts_on_fly_rec.market_price;
   l_parts_on_fly_rec.item_cost                    := p_parts_on_fly_rec.item_cost;
   l_parts_on_fly_rec.radio_group_value            := p_parts_on_fly_rec.radio_group_value; --part or fly
   l_parts_on_fly_rec.transaction_id               := p_parts_on_fly_rec.transaction_id;
   l_parts_on_fly_rec.organization_code            := p_parts_on_fly_rec.organization_code;
    --debug info
      IF NVL(g_xxwc_woc_debug,'N') = 'Y' THEN
         xxwc_checkitapp_pkg.xxwc_log_debug_info
                    (P_API_NAME         =>l_proc_name,
                     P_ITEM_NUMBER     =>p_Parts_On_Fly_Rec.item_number,
                     P_DEBUG_TEXT       => 'Start XXWC_INV_PARTS_ON_FLY_PKG.CREATE_ITEM',
                     P_USER_ID => l_user_id
                    );
      END IF;
    APPS.XXWC_INV_PARTS_ON_FLY_PKG.CREATE_ITEM (
      p_Parts_On_Fly_Rec   => l_PARTS_ON_FLY_REC,
      x_Return_Status      => x_return_status,
      x_Msg_Data           => x_Msg_Data,
      x_Msg_Count          => x_msg_count);
      --debug info
      IF NVL(g_xxwc_woc_debug,'N') = 'Y' THEN
         xxwc_checkitapp_pkg.xxwc_log_debug_info
                    (P_API_NAME         =>l_proc_name,
                     P_ITEM_NUMBER     =>p_Parts_On_Fly_Rec.item_number,
                     P_DEBUG_TEXT       => 'End XXWC_INV_PARTS_ON_FLY_PKG.CREATE_ITEM',
                     P_USER_ID => l_user_id
                    );
      END IF;
    IF (x_return_status = 'S')
    THEN
       BEGIN
          SELECT msi.segment1
                ,msi.inventory_item_id
                ,primary_uom_code
                ,p_Parts_On_Fly_Rec.list_price
            INTO x_part_number
                ,x_inventory_item_id
                ,x_primary_uom_code
                ,x_list_price
            FROM apps.mtl_system_items_b msi
           WHERE segment1        = p_Parts_On_Fly_Rec.item_number
             AND organization_id = p_Parts_On_Fly_Rec.assgn_organization_id;
       EXCEPTION
       WHEN OTHERS THEN
          x_part_number       := NULL;
          x_inventory_item_id := NULL;
          x_primary_uom_code  := NULL;
          x_list_price        := NULL;
       END;


    END If;
    --debug info
      IF NVL(g_xxwc_woc_debug,'N') = 'Y' THEN
         xxwc_checkitapp_pkg.xxwc_log_debug_info
                    (P_API_NAME         =>l_proc_name,
                     P_ITEM_ID         => x_inventory_item_id,
                     P_ITEM_NUMBER     =>p_Parts_On_Fly_Rec.item_number,
                     P_DEBUG_TEXT       => 'End CREATE_WOC_ITEM',
                     P_USER_ID => l_user_id
                    );
      END IF;
   EXCEPTION
      WHEN e_biz_exception THEN
         ROLLBACK;
         x_Return_Status  := l_return_status;
         x_Msg_Data := l_return_message;
      WHEN OTHERS THEN
         ROLLBACK;
         x_Return_Status := 'E';
         x_Msg_Data := SUBSTR(SQLERRM,1,2000);
   END CREATE_WOC_ITEM;
   /*************************************************************************
      PROCEDURE Name: GET_PRODUCT_INFO

      PURPOSE:   This procedure will be called by mobile app to get item info

      REVISIONS:
      Ver        Date          Author             Description
      ---------  ------------  ------------       ------------------
      1.0        03/01/2017    Niraj K Ranjan     TMS#20171102-00074-Mobile apps new plsql procedures for WOC    
   ****************************************************************************/
   PROCEDURE GET_PRODUCT_INFO(   p_branch_id   IN NUMBER
                             ,p_customer_id IN NUMBER
                             ,p_jobsiteid   IN NUMBER
                             ,p_item_search IN VARCHAR2
                             ,p_filter      IN VARCHAR2
                             ,p_Sort        IN VARCHAR2
                             ,p_warhouse_num_tbl IN  xxwc.xxwc_warhouse_num_tbl DEFAULT NULL --Rev 2.2
                             ,p_filter_stock IN VARCHAR2 DEFAULT NULL --Rev 2.5
                             ,o_cur_output  OUT SYS_REFCURSOR -- return sys_refcursor
                             )
                             IS
   v_rec sys_refcursor;
   l_count NUMBER;
   l_proc_name          VARCHAR2(100):= g_pkg_name||'.'||'GET_PRODUCT_INFO';
   l_user_id            NUMBER := FND_PROFILE.VALUE('USER_ID');
  BEGIN
     --Rev 2.2 > Start
     --debug info
     IF NVL(g_xxwc_woc_debug,'N') = 'Y' THEN
        xxwc_checkitapp_pkg.xxwc_log_debug_info
                    (P_API_NAME         => l_proc_name,
                     P_BRANCH_ID        => p_branch_id,
                     P_CUSTOMER_ID      => p_customer_id,
                     P_JOB_SITE_ID      => p_jobsiteid,
                     P_SEARCH_STRING    => p_item_search,
                     P_DEBUG_TEXT       => 'Start GET_PRODUCT_INFO',
                     P_USER_ID          => l_user_id
                    );
     END IF;
     IF p_warhouse_num_tbl.count >0 THEN
        DELETE FROM xxwc.xxwc_item_avbl_price_tbl;
        DELETE FROM xxwc.xxwc_md_search_product_gtt_tbl;
        --debug info
        IF NVL(g_xxwc_woc_debug,'N') = 'Y' THEN
           xxwc_checkitapp_pkg.xxwc_log_debug_info
                    (P_API_NAME         =>l_proc_name,
                     P_BRANCH_ID        => p_branch_id,
                     P_CUSTOMER_ID      => p_customer_id,
                     P_JOB_SITE_ID      => p_jobsiteid,
                     P_SEARCH_STRING    => p_item_search,
                     P_DEBUG_TEXT       => 'Start XXWC_OM_SO_CREATE.GET_PRODUCT_INFO_AIS',
                     P_USER_ID          => l_user_id
                    );
        END IF;
        XXWC_OM_SO_CREATE.GET_PRODUCT_INFO_AIS( p_branch_id   =>  p_branch_id 
                             ,p_customer_id =>  p_customer_id
                             ,p_jobsiteid   =>  p_jobsiteid
                             ,p_item_search =>  p_item_search
                             ,p_filter      =>  p_filter
                             ,p_Sort        =>  p_Sort
                             ,p_call_pr_api =>  'N'
                          );
        --debug info
        IF NVL(g_xxwc_woc_debug,'N') = 'Y' THEN
           xxwc_checkitapp_pkg.xxwc_log_debug_info
                    (P_API_NAME         =>l_proc_name,
                     P_BRANCH_ID        => p_branch_id,
                     P_CUSTOMER_ID      => p_customer_id,
                     P_JOB_SITE_ID      => p_jobsiteid,
                     P_SEARCH_STRING    => p_item_search,
                     P_DEBUG_TEXT       => 'End XXWC_OM_SO_CREATE.GET_PRODUCT_INFO_AIS',
                     P_USER_ID          => l_user_id
                    );
        END IF;
       --Rev 2.5 < Start
       IF p_filter_stock IS NOT NULL THEN
          IF p_filter_stock = 'STOCKABLE' THEN
             DELETE FROM xxwc.xxwc_md_search_product_gtt_tbl xmsp
              WHERE EXISTS (SELECT 1
                              FROM apps.mtl_system_items_b msib
                             WHERE msib.organization_id    = 222
                               AND msib.inventory_item_id  = xmsp.inventory_item_id
                               AND MSIB.stock_enabled_flag = 'N'
                            );
          ELSIF p_filter_stock = 'NONSTOCKABLE' THEN
             DELETE FROM xxwc.xxwc_md_search_product_gtt_tbl xmsp
              WHERE EXISTS (SELECT 1
                              FROM apps.mtl_system_items_b msib
                             WHERE msib.organization_id    = 222
                               AND msib.inventory_item_id  = xmsp.inventory_item_id
                               AND MSIB.stock_enabled_flag = 'Y'
                            );  
          END IF;
       END IF;
       --Rev 2.5 > End                          
                          
        -- insert the records from AIS search into the xxwc_item_avbl_price_tbl
           BEGIN
               FOR j IN p_warhouse_num_tbl.FIRST .. p_warhouse_num_tbl.LAST
               LOOP
                  BEGIN
                     INSERT INTO xxwc.xxwc_item_avbl_price_tbl (
                                 organization_code,
                                 organization_id,
                                 segment1,
                                 part_description,
                                 inventory_item_id, 
                                 primary_uom_code)
                          SELECT mp.organization_code
                                ,mp.organization_id
                                ,xmsp.partnumber
                                ,xmsp.shortdescription 
                                ,xmsp.inventory_item_id
                                ,xmsp.primary_uom_code
                            FROM  xxwc.xxwc_md_search_product_gtt_tbl xmsp
                                , mtl_parameters mp
                                , apps.mtl_system_items_b msib
                           WHERE 1=1
                             AND msib.organization_id   = mp.organization_id
                             AND msib.inventory_item_id = xmsp.inventory_item_id
                             AND mp.organization_code   = p_warhouse_num_tbl (j);
                  EXCEPTION
                  WHEN OTHERS
                  THEN
                     NULL;
                  END;
               END LOOP; --p_warhouse_num_tbl
           EXCEPTION
           WHEN OTHERS  THEN
               NULL;
           END;
        
        xxwc_om_so_create.line_pricing_data (p_cust_account_id   => p_customer_id,
                           p_site_use_id       => p_jobsiteid);
        
        --Rev 2.5 < Start
        IF p_filter_stock IS NOT NULL AND p_filter_stock = 'AVAILABLE' THEN
           --debug info
           IF NVL(g_xxwc_woc_debug,'N') = 'Y' THEN
              xxwc_checkitapp_pkg.xxwc_log_debug_info
                    (P_API_NAME         =>l_proc_name,
                     P_BRANCH_ID        => p_branch_id,
                     P_CUSTOMER_ID      => p_customer_id,
                     P_JOB_SITE_ID      => p_jobsiteid,
                     P_SEARCH_STRING    => p_item_search,
                     P_DEBUG_TEXT       => '1',
                     P_USER_ID          => l_user_id
                    );
           END IF;
           OPEN o_cur_output FOR
                'SELECT aiap.segment1
                      , aiap.part_description
                      , aiap.inventory_item_id
                      , aiap.organization_id
                      , xxwc_inv_ais_pkg.shipto_last_price_paid (:p_jobsiteid, aiap.inventory_item_id) last_price_paid
                      --, aiap.modifier
                      ,aiap.modifier_type modifier                      
                      , aiap.SELLING_PRICE list_price
                      , aiap.primary_uom_code primary_uom_code
                      , (NVL(xxwc_ascp_scwb_pkg.get_on_hand(aiap.inventory_item_id, aiap.organization_id, ''G''),0) -
                        NVL(xxwc_ascp_scwb_pkg.get_open_orders_qty(aiap.inventory_item_id, aiap.organization_id),0) ) available_qty
                          FROM xxwc.xxwc_item_avbl_price_tbl aiap
                   WHERE (NVL(xxwc_ascp_scwb_pkg.get_on_hand(aiap.inventory_item_id, aiap.organization_id, ''G''),0) -
                        NVL(xxwc_ascp_scwb_pkg.get_open_orders_qty(aiap.inventory_item_id, aiap.organization_id),0) ) >0      
                  ORDER BY aiap.segment1, aiap.organization_id'
            using p_jobsiteid;
        ELSE --Rev 2.5 > End
           --debug info
           IF NVL(g_xxwc_woc_debug,'N') = 'Y' THEN
              xxwc_checkitapp_pkg.xxwc_log_debug_info
                    (P_API_NAME         =>l_proc_name,
                     P_BRANCH_ID        => p_branch_id,
                     P_CUSTOMER_ID      => p_customer_id,
                     P_JOB_SITE_ID      => p_jobsiteid,
                     P_SEARCH_STRING    => p_item_search,
                     P_DEBUG_TEXT       => '2',
                     P_USER_ID          => l_user_id
                    );
           END IF;
           OPEN o_cur_output FOR
                'SELECT aiap.segment1
                      , aiap.part_description
                      , aiap.inventory_item_id
                      , aiap.organization_id
                      , xxwc_inv_ais_pkg.shipto_last_price_paid (:p_jobsiteid, aiap.inventory_item_id) last_price_paid
                      --, aiap.modifier  
                      ,aiap.modifier_type modifier
                      , aiap.SELLING_PRICE list_price
                      , aiap.primary_uom_code primary_uom_code
                      , (NVL(xxwc_ascp_scwb_pkg.get_on_hand(aiap.inventory_item_id, aiap.organization_id, ''G''),0) -
                        NVL(xxwc_ascp_scwb_pkg.get_open_orders_qty(aiap.inventory_item_id, aiap.organization_id),0) ) available_qty
                          FROM xxwc.xxwc_item_avbl_price_tbl aiap
                  ORDER BY aiap.segment1, aiap.organization_id'
            using p_jobsiteid;
        END IF;   --Rev 2.5
     ELSE 
     --Rev 2.2 < End
        --Rev 2.5 < Start
        IF p_filter_stock IS NOT NULL THEN
           IF p_filter_stock = 'STOCKABLE' THEN
               SELECT count(1)
                 INTO l_count
                 FROM MTL_SYSTEM_ITEMS_B msi
                WHERE segment1 = UPPER(p_item_search)
                  AND organization_id = p_branch_id
                  AND msi.stock_enabled_flag = 'Y';
           ELSIF p_filter_stock = 'NONSTOCKABLE' THEN
              SELECT count(1)
              INTO l_count
              FROM MTL_SYSTEM_ITEMS_B msi
             WHERE segment1 = UPPER(p_item_search)
               AND organization_id = p_branch_id
               AND msi.stock_enabled_flag = 'N';
           ELSIF p_filter_stock = 'AVAILABLE' THEN
              SELECT count(1)
              INTO l_count
              FROM MTL_SYSTEM_ITEMS_B msi
             WHERE segment1 = UPPER(p_item_search)
               AND organization_id = p_branch_id
               AND (NVL(xxwc_ascp_scwb_pkg.get_on_hand(msi.inventory_item_id, msi.organization_id, 'G'),0) -
                   NVL(xxwc_ascp_scwb_pkg.get_open_orders_qty(msi.inventory_item_id, msi.organization_id),0) ) >0;
           
           END IF;                  
        ELSE --Rev 2.5 > End    
           SELECT count(1)
             INTO l_count
             FROM MTL_SYSTEM_ITEMS_B msi
            WHERE segment1 = UPPER(p_item_search)
              AND organization_id = p_branch_id;
        END IF;--Rev 2.5
             
        IF l_count = 1 THEN
           --debug info
           IF NVL(g_xxwc_woc_debug,'N') = 'Y' THEN
              xxwc_checkitapp_pkg.xxwc_log_debug_info
                    (P_API_NAME         =>l_proc_name,
                     P_BRANCH_ID        => p_branch_id,
                     P_CUSTOMER_ID      => p_customer_id,
                     P_JOB_SITE_ID      => p_jobsiteid,
                     P_SEARCH_STRING    => p_item_search,
                     P_DEBUG_TEXT       => '3',
                     P_USER_ID          => l_user_id
                    );
           END IF;
           OPEN o_cur_output FOR 'SELECT msi.segment1,
             msi.description part_description,
             msi.inventory_item_id,
             msi.organization_id,
             xxwc_inv_ais_pkg.shipto_last_price_paid (:p_jobsiteid, msi.inventory_item_id) last_price_paid,
             NULL,
             XXWC_OM_SO_CREATE.get_list_price( msi.organization_id, msi.inventory_item_id  ) list_price,
             msi.primary_uom_code primary_uom_code,
             (NVL(xxwc_ascp_scwb_pkg.get_on_hand(msi.inventory_item_id, msi.organization_id, ''G''),0) -
             NVL(xxwc_ascp_scwb_pkg.get_open_orders_qty(msi.inventory_item_id, msi.organization_id),0) ) available_qty
              FROM MTL_SYSTEM_ITEMS_B msi
             WHERE segment1 = UPPER(:p_item_search)
               AND msi.organization_id = :p_branch_id
               AND EXISTS ( SELECT 1
                     FROM APPS.XXWC_MD_SEARCH_PRODUCTS_VW xmsp
                    WHERE xmsp.PARTNUMBER = msi.segment1
                      AND xmsp.organization_id = 222)'
          using p_jobsiteid, p_item_search, p_branch_id;
        ELSE
          --debug info
           IF NVL(g_xxwc_woc_debug,'N') = 'Y' THEN
              xxwc_checkitapp_pkg.xxwc_log_debug_info
                    (P_API_NAME         =>l_proc_name,
                     P_BRANCH_ID        => p_branch_id,
                     P_CUSTOMER_ID      => p_customer_id,
                     P_JOB_SITE_ID      => p_jobsiteid,
                     P_SEARCH_STRING    => p_item_search,
                     P_DEBUG_TEXT       => '4',
                     P_USER_ID          => l_user_id
                    );
           END IF;
          XXWC_OM_SO_CREATE.GET_PRODUCT_INFO_AIS( p_branch_id   =>  p_branch_id --Rev 2.2
                               ,p_customer_id =>  p_customer_id
                               ,p_jobsiteid   =>  p_jobsiteid
                               ,p_item_search =>  p_item_search
                               ,p_filter      =>  p_filter
                               ,p_Sort        =>  p_Sort
                               ,p_call_pr_api =>  'Y' --Rev 2.2
                               );

          --Rev 2.5 < Start
          IF p_filter_stock IS NOT NULL THEN
             IF p_filter_stock = 'STOCKABLE' THEN
                DELETE FROM xxwc.xxwc_md_search_product_gtt_tbl xmsp
                WHERE EXISTS (SELECT 1
                              FROM apps.mtl_system_items_b msib
                             WHERE msib.organization_id    = 222
                               AND msib.inventory_item_id  = xmsp.inventory_item_id
                               AND MSIB.stock_enabled_flag = 'N'
                            );
             ELSIF p_filter_stock = 'NONSTOCKABLE' THEN
                DELETE FROM xxwc.xxwc_md_search_product_gtt_tbl xmsp
                 WHERE EXISTS (SELECT 1
                              FROM apps.mtl_system_items_b msib
                             WHERE msib.organization_id    = 222
                               AND msib.inventory_item_id  = xmsp.inventory_item_id
                               AND MSIB.stock_enabled_flag = 'Y'
                            );  
             END IF;
          END IF;
          --Rev 2.5 > End                          
          
          --Rev 2.5 < Start
          IF p_filter_stock IS NOT NULL AND p_filter_stock = 'AVAILABLE' THEN 
             --debug info
             IF NVL(g_xxwc_woc_debug,'N') = 'Y' THEN
                xxwc_checkitapp_pkg.xxwc_log_debug_info
                    (P_API_NAME         =>l_proc_name,
                     P_BRANCH_ID        => p_branch_id,
                     P_CUSTOMER_ID      => p_customer_id,
                     P_JOB_SITE_ID      => p_jobsiteid,
                     P_SEARCH_STRING    => p_item_search,
                     P_DEBUG_TEXT       => '5',
                     P_USER_ID          => l_user_id
                    );
             END IF;          
             OPEN o_cur_output FOR 'SELECT msi.segment1,
                msi.description part_description,
                msi.inventory_item_id,
                msi.organization_id,
                xxwc_inv_ais_pkg.shipto_last_price_paid (:p_jobsiteid, msi.inventory_item_id) last_price_paid,
                --xmsp.modifier,
                xmsp.modifier_type modifier,
                xmsp.SELLING_PRICE list_price,
                --XXWC_OM_SO_CREATE.get_list_price( msi.organization_id, msi.inventory_item_id  ) list_price,
                xmsp.primary_uom_code primary_uom_code,
                (NVL(xxwc_ascp_scwb_pkg.get_on_hand(msi.inventory_item_id, msi.organization_id, ''G''),0) -
                NVL(xxwc_ascp_scwb_pkg.get_open_orders_qty(msi.inventory_item_id, msi.organization_id),0) ) available_qty
                FROM MTL_SYSTEM_ITEMS_B msi, xxwc.xxwc_md_search_product_gtt_tbl xmsp
               WHERE xmsp.inventory_item_id = msi.inventory_item_id
                 AND (NVL(xxwc_ascp_scwb_pkg.get_on_hand(msi.inventory_item_id, msi.organization_id, ''G''),0) -
                      NVL(xxwc_ascp_scwb_pkg.get_open_orders_qty(msi.inventory_item_id, msi.organization_id),0) ) >0
                 AND msi.organization_id = :p_branch_id'
             using p_jobsiteid, p_branch_id;
           ELSE
              --debug info
             IF NVL(g_xxwc_woc_debug,'N') = 'Y' THEN
                 xxwc_checkitapp_pkg.xxwc_log_debug_info
                    (P_API_NAME         =>l_proc_name,
                     P_BRANCH_ID        => p_branch_id,
                     P_CUSTOMER_ID      => p_customer_id,
                     P_JOB_SITE_ID      => p_jobsiteid,
                     P_SEARCH_STRING    => p_item_search,
                     P_DEBUG_TEXT       => '6',
                     P_USER_ID          => l_user_id
                    );
             END IF;
             OPEN o_cur_output FOR 'SELECT msi.segment1,
                msi.description part_description,
                msi.inventory_item_id,
                msi.organization_id,
                xxwc_inv_ais_pkg.shipto_last_price_paid (:p_jobsiteid, msi.inventory_item_id) last_price_paid,
                --xmsp.modifier,
                xmsp.modifier_type modifier,
                xmsp.SELLING_PRICE list_price,
                --XXWC_OM_SO_CREATE.get_list_price( msi.organization_id, msi.inventory_item_id  ) list_price,
                xmsp.primary_uom_code primary_uom_code,
                (NVL(xxwc_ascp_scwb_pkg.get_on_hand(msi.inventory_item_id, msi.organization_id, ''G''),0) -
                NVL(xxwc_ascp_scwb_pkg.get_open_orders_qty(msi.inventory_item_id, msi.organization_id),0) ) available_qty
                FROM MTL_SYSTEM_ITEMS_B msi, xxwc.xxwc_md_search_product_gtt_tbl xmsp
               WHERE xmsp.inventory_item_id = msi.inventory_item_id
                 AND msi.organization_id = :p_branch_id'
             using p_jobsiteid, p_branch_id;
           END IF;  
        END IF;
     END IF; --Rev 2.2   
     --debug info
     IF NVL(g_xxwc_woc_debug,'N') = 'Y' THEN
         xxwc_checkitapp_pkg.xxwc_log_debug_info
                    (P_API_NAME         =>l_proc_name,
                     P_BRANCH_ID        => p_branch_id,
                     P_CUSTOMER_ID      => p_customer_id,
                     P_JOB_SITE_ID      => p_jobsiteid,
                     P_SEARCH_STRING    => p_item_search,
                     P_DEBUG_TEXT       => 'End GET_PRODUCT_INFO',
                     P_USER_ID          => l_user_id
                    );
     END IF;     
   END GET_PRODUCT_INFO;
   
   /*************************************************************************
   *   Procedure : xxwc_log_debug_info
   *
   *   PURPOSE:   This procedure is called to log debug info.
   *
   * REVISIONS:
   *  Ver        Date         Author            Description
   *  ---------  ----------   ---------------   -------------------------
   *  1.0       01/16/2017   Niraj K Ranjan   TMS#20171102-00074-Mobile apps new plsql procedures for WOC
   * ************************************************************************/
   PROCEDURE xxwc_log_debug_info(P_API_NAME         IN VARCHAR2,
                                 P_BRANCH_ID        IN VARCHAR2 DEFAULT NULL,
                                 P_ORDER_HEADER_ID  IN NUMBER DEFAULT NULL,
                                 P_ORDER_NUMBER     IN NUMBER DEFAULT NULL,
                                 P_ITEM_ID          IN NUMBER DEFAULT NULL,
                                 P_ITEM_NUMBER      IN VARCHAR2 DEFAULT NULL,
                                 P_JOB_SITE_ID      IN NUMBER DEFAULT NULL,
                                 P_CUSTOMER_ID      IN NUMBER DEFAULT NULL,
                                 P_CUSTOMER_NUMBER  IN VARCHAR2 DEFAULT NULL,
                                 P_SEARCH_STRING    IN VARCHAR2 DEFAULT NULL,
                                 P_DEBUG_TEXT       IN VARCHAR2 DEFAULT NULL,
                                 P_USER_ID          IN NUMBER        
                                 )
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      INSERT INTO XXWC.XXWC_WOC_APP_LOG_TBL(SEQUENCE_ID,
                                            API_NAME,
                                            LOG_TIME,
                                            BRANCH_ID,
                                            ORDER_HEADER_ID,
                                            ORDER_NUMBER,
                                            ITEM_ID,
                                            ITEM_NUMBER,
                                            JOB_SITE_ID,
                                            CUSTOMER_ID,
                                            CUSTOMER_NUMBER,
                                            SEARCH_STRING,
                                            DEBUG_TEXT,
                                            CREATION_DATE,
                                            CREATED_BY,
                                            UPDATE_DATE,
                                            UPDATED_BY
                                            )
                                   VALUES(XXWC.XXWC_WOC_APP_LOG_TBL_SEQ.NEXTVAL,
                                          P_API_NAME,
                                          SYSDATE,
                                          P_BRANCH_ID,
                                          P_ORDER_HEADER_ID,
                                          P_ORDER_NUMBER,
                                          P_ITEM_ID,
                                          P_ITEM_NUMBER,
                                          P_JOB_SITE_ID,
                                          P_CUSTOMER_ID,
                                          P_CUSTOMER_NUMBER,
                                          P_SEARCH_STRING,
                                          P_DEBUG_TEXT,
                                          SYSDATE,
                                          P_USER_ID,
                                          SYSDATE,
                                          P_USER_ID
                                         );
      COMMIT;
   EXCEPTION
      WHEN OTHERS THEN 
         ROLLBACK;
   END;
   /*************************************************************************
      PROCEDURE Name: get_b2b_setup_info

      PURPOSE:   This procedure will be called by CheckIT mobile app to get info about B2B setup of a customer.

      REVISIONS:
      Ver        Date          Author             Description
      ---------  ------------  ------------       ------------------
      4.0        02/16/2018  Niraj K Ranjan          TMS#20180205-00161   @CheckIT Ability to subscribe a customer for Order Status Updates - OSU through CheckIT app
   ****************************************************************************/
    FUNCTION get_b2b_setup_info( p_cust_account_id     IN NUMBER
                                ,p_cust_site_use_id    IN NUMBER
                               ) return sys_refcursor IS
    v_b2b_rc sys_refcursor;
	l_proc_name          VARCHAR2(100):= g_pkg_name||'.'||'GET_B2B_SETUP_INFO';
	l_user_id            NUMBER := FND_PROFILE.VALUE('USER_ID');
    BEGIN
       mo_global.set_policy_context('S',162);
       --debug info
	   IF NVL(g_xxwc_woc_debug,'N') = 'Y' THEN
          xxwc_checkitapp_pkg.xxwc_log_debug_info
                    (P_API_NAME         =>l_proc_name,
                     P_CUSTOMER_ID      => p_cust_account_id,
                     P_JOB_SITE_ID      => p_cust_site_use_id,
                     P_DEBUG_TEXT       => 'Start GET_B2B_SETUP_INFO',
                     P_USER_ID          => l_user_id
                    );
       END IF;
	   IF  p_cust_site_use_id IS NULL THEN
          OPEN v_b2b_rc FOR 'SELECT * FROM xxwc.xxwc_b2b_config_tbl 
                           WHERE cust_account_id = :p_cust_account_id'
                USING p_cust_account_id;
       ELSE
	      OPEN v_b2b_rc FOR 'SELECT * FROM xxwc.xxwc_b2b_config_tbl 
                           WHERE cust_account_id = :p_cust_account_id 
                           AND site_use_id = :p_cust_site_use_id'
			    USING p_cust_account_id, p_cust_site_use_id;
       END IF;
       --debug info
	   IF NVL(g_xxwc_woc_debug,'N') = 'Y' THEN
          xxwc_checkitapp_pkg.xxwc_log_debug_info
                    (P_API_NAME         =>l_proc_name,
                     P_CUSTOMER_ID      => p_cust_account_id,
                     P_JOB_SITE_ID      => p_cust_site_use_id,
                     P_DEBUG_TEXT       => 'End GET_B2B_SETUP_INFO',
                     P_USER_ID          => l_user_id
                    );
       END IF;
	   RETURN v_b2b_rc;
    EXCEPTION
       WHEN OTHERS THEN
          RETURN v_b2b_rc; 
    END get_b2b_setup_info;
	/*************************************************************************
      PROCEDURE Name: Set_b2b_account

      PURPOSE:   This procedure will be called by CheckIT mobile app to set account flags/email info.

      REVISIONS:
      Ver        Date          Author             Description
      ---------  ------------  ------------       ------------------
      4.0        02/16/2018  Niraj K Ranjan          TMS#20180205-00161   @CheckIT Ability to subscribe a customer for Order Status Updates - OSU through CheckIT app 
   ****************************************************************************/
   PROCEDURE Set_b2b_account( p_b2b_config_tbl_typ  IN   XXWC.XXWC_B2B_CONFIG_TBL%ROWTYPE
                             ,p_ntid                IN   VARCHAR2
                             ,p_return_status       OUT  VARCHAR2
                             ,p_return_message      OUT  VARCHAR2
       )IS

          l_proc_name               VARCHAR2(50) := g_pkg_name||'.'||'SET_B2B_ACCOUNT';
          l_b2b_config_tbl_typ      XXWC.XXWC_B2B_CONFIG_TBL%ROWTYPE;
          l_sec                     VARCHAR2(200);
          l_custaccount_id          NUMBER;
          l_party_id                hz_cust_accounts.party_id%TYPE := NULL;
          l_party_name              XXWC.XXWC_B2B_CONFIG_TBL.PARTY_NAME%TYPE := NULL;
          l_party_number            XXWC.XXWC_B2B_CONFIG_TBL.PARTY_NUMBER%TYPE := NULL;
          l_cust_account_number     XXWC.XXWC_B2B_CONFIG_TBL.ACCOUNT_NUMBER%TYPE := NULL;
          e_biz_exception           EXCEPTION;
          l_count                   NUMBER;
          l_cust_count              NUMBER;
		  l_cust_site_count         NUMBER;
		  l_location                hz_cust_site_uses_all.location%TYPE;
		  l_user_id                 NUMBER;
          l_return_status           VARCHAR2(1);
          l_return_message          VARCHAR2(2000);
     BEGIN
        l_sec := 'Start Set_b2b_account';
        --l_custaccount_id  := p_event.GetValueForParameter('CUST_ACCOUNT_ID');
        --mandatory check for required fields
        IF p_b2b_config_tbl_typ.customer_id IS NULL 
        THEN
           l_return_status := 'E';
           l_return_message := 'Customer ID is null';
           Raise e_biz_exception;
        END IF;
		--Fetch Userid
		IF p_ntid IS NULL 
        THEN
           l_return_status := 'E';
           l_return_message := 'NTID is null';
           Raise e_biz_exception;
		ELSE
		   BEGIN
		      SELECT user_id INTO l_user_id
			  FROM fnd_user 
			  WHERE user_name = UPPER(p_ntid);
		   EXCEPTION
		      WHEN NO_DATA_FOUND THEN
			     l_user_id:= NULL;
				 l_return_status := 'E';
                 l_return_message := 'NTID '||p_ntid||' does not exists';
                 Raise e_biz_exception;
		   END;
        END IF;
        IF NVL(g_xxwc_woc_debug,'N') = 'Y' THEN
          xxwc_checkitapp_pkg.xxwc_log_debug_info
                    (P_API_NAME         =>l_proc_name,
                     P_CUSTOMER_ID      => p_b2b_config_tbl_typ.customer_id,
                     P_JOB_SITE_ID      => l_b2b_config_tbl_typ.site_use_id,
                     P_DEBUG_TEXT       => 'Start SET_B2B_ACCOUNT',
                     P_USER_ID          => l_user_id
                    );
        END IF;
        l_b2b_config_tbl_typ := p_b2b_config_tbl_typ;
        l_sec := 'Fetch party id for account id: '||p_b2b_config_tbl_typ.customer_id;
        --To check if the part is setup as b2b customer
        BEGIN        
            SELECT hp.party_id,hp.party_name,hp.party_number,hca.account_number 
            INTO  l_party_id,l_party_name,l_party_number,l_cust_account_number
            FROM hz_parties hp,hz_cust_accounts hca
            WHERE hca.party_id = hp.party_id
            AND hca.cust_account_id = l_b2b_config_tbl_typ.customer_id;
        EXCEPTION WHEN OTHERS THEN
           l_party_id := NULL;
           l_party_name := NULL;
           l_party_number := NULL;
           l_cust_account_number := NULL;
        END;
		
		IF l_b2b_config_tbl_typ.site_use_id IS NOT NULL THEN
		   BEGIN
		      select hcsu.location INTO l_location
              from hz_parties hp,
                   hz_cust_accounts hca,
                   hz_locations hl,
                   hz_party_sites hps,
                   hz_cust_acct_sites_all hcas,
                   hz_cust_site_uses_all hcsu
              where 1=1
              and hp.party_id = l_party_id
              and hca.cust_account_id = l_b2b_config_tbl_typ.customer_id
              and hcsu.site_use_id = l_b2b_config_tbl_typ.site_use_id
              AND hp.party_id = hca.party_id
              AND HL.location_id = HPS.location_id
              and hp.party_id = hps.party_id
              and hcsu.site_use_code = 'SHIP_TO'
              AND hca.cust_account_id = hcas.cust_account_id
              AND hps.party_site_id = hcas.party_site_id
              and hcas.cust_acct_site_id = hcsu.cust_acct_site_id
              AND hp.Status ='A'
              AND hca.Status ='A'
              AND hps.Status ='A'
              AND hcas.Status ='A'
              and hcsu.status = 'A';
		   EXCEPTION WHEN OTHERS THEN
		      l_location := NULL;
			  l_return_status := 'E';
              l_return_message := 'No active location found for Site_use_id '||l_b2b_config_tbl_typ.site_use_id;
              Raise e_biz_exception;
		   END;
		ELSE
		   l_location := NULL;
		END IF;
		
        l_sec := 'After Derive l_party_id'||l_party_id;
        IF l_b2b_config_tbl_typ.site_use_id IS NULL THEN
		   SELECT COUNT(1) INTO l_cust_count
           FROM xxwc.xxwc_b2b_config_tbl
           WHERE cust_account_id = l_b2b_config_tbl_typ.customer_id
           AND party_id =l_party_id
		   AND SITE_USE_ID IS NULL;
		ELSE
           SELECT COUNT(1) INTO l_cust_site_count
           FROM xxwc.xxwc_b2b_config_tbl
           WHERE cust_account_id = l_b2b_config_tbl_typ.customer_id
           AND party_id =l_party_id
		   AND SITE_USE_ID IS NOT NULL
		   AND SITE_USE_ID = l_b2b_config_tbl_typ.site_use_id;
		END IF;
        
        IF (l_b2b_config_tbl_typ.site_use_id IS NULL AND l_cust_count = 0) OR 
		   (l_b2b_config_tbl_typ.site_use_id IS NOT NULL AND l_cust_site_count = 0) 
		THEN
           l_sec := 'After Derive l_count'||l_count;
		   
           l_b2b_config_tbl_typ.PARTY_ID               := l_party_id ;
           l_b2b_config_tbl_typ.PARTY_NAME             := l_party_name ;
           l_b2b_config_tbl_typ.PARTY_NUMBER           := l_party_number ;
           l_b2b_config_tbl_typ.START_DATE_ACTIVE      := SYSDATE ;
		   l_b2b_config_tbl_typ.CREATED_BY             := l_user_id;
           l_b2b_config_tbl_typ.LAST_UPDATED_BY        := l_user_id;
           l_b2b_config_tbl_typ.APPROVED_DATE          := SYSDATE;
		   IF l_b2b_config_tbl_typ.ASN_EMAIL IS NOT NULL THEN
		      l_b2b_config_tbl_typ.DELIVER_ASN := NVL(l_b2b_config_tbl_typ.DELIVER_ASN,'Y');
		   END IF;
		   IF l_b2b_config_tbl_typ.SOA_EMAIL IS NOT NULL THEN
              l_b2b_config_tbl_typ.SOA_PRINT_PRICE        := NVL(l_b2b_config_tbl_typ.SOA_PRINT_PRICE,'N');
			  l_b2b_config_tbl_typ.DELIVER_SOA            := NVL(l_b2b_config_tbl_typ.DELIVER_SOA,'Y');
		   END IF;
		   IF l_b2b_config_tbl_typ.POD_EMAIL IS NOT NULL THEN
		      l_b2b_config_tbl_typ.DELIVER_POD := NVL(l_b2b_config_tbl_typ.DELIVER_POD,'Y');
              l_b2b_config_tbl_typ.POD_FREQUENCY          := NVL(l_b2b_config_tbl_typ.POD_FREQUENCY,'DAILY');
		      l_b2b_config_tbl_typ.POD_LAST_SENT_DATE     := NVL(l_b2b_config_tbl_typ.POD_LAST_SENT_DATE,SYSDATE);
		      l_b2b_config_tbl_typ.POD_NEXT_SEND_DATE     := NVL(l_b2b_config_tbl_typ.POD_NEXT_SEND_DATE,SYSDATE+1);
			  l_b2b_config_tbl_typ.POD_PRINT_PRICE        := NVL(l_b2b_config_tbl_typ.POD_PRINT_PRICE,'N');
           END IF;
           l_b2b_config_tbl_typ.ACCOUNT_NUMBER         := l_cust_account_number;
           --l_b2b_config_tbl_typ.ACCOUNT_NAME           := This will be drived by the trigger on this table
		   l_b2b_config_tbl_typ.notify_account_mgr     := NVL(l_b2b_config_tbl_typ.notify_account_mgr,'N');
           l_b2b_config_tbl_typ.CUST_ACCOUNT_ID        := l_b2b_config_tbl_typ.customer_id;
           l_b2b_config_tbl_typ.ID                     := xxwc.xxwc_b2b_config_s.nextval;
           l_b2b_config_tbl_typ.LOCATION               := l_location;
           --l_b2b_config_tbl_typ.SITE_USE_ID            := It will come as input
		   l_b2b_config_tbl_typ.business_event_eligible  := NVL(l_b2b_config_tbl_typ.business_event_eligible,'N');
        
           INSERT INTO XXWC.XXWC_B2B_CONFIG_TBL
           VALUES l_b2b_config_tbl_typ;
           l_sec := 'After insert';
		ELSIF (l_b2b_config_tbl_typ.site_use_id IS NULL AND l_cust_count > 0) OR 
		      (l_b2b_config_tbl_typ.site_use_id IS NOT NULL AND l_cust_site_count > 0) 
		THEN
		   UPDATE XXWC.XXWC_B2B_CONFIG_TBL SET       
            DELIVER_SOA        = l_b2b_config_tbl_typ.DELIVER_SOA        
           ,DELIVER_ASN        = l_b2b_config_tbl_typ.DELIVER_ASN  
           ,DELIVER_POA        = NVL(l_b2b_config_tbl_typ.DELIVER_POA,DELIVER_POA)
		   ,DELIVER_INVOICE    = NVL(l_b2b_config_tbl_typ.DELIVER_INVOICE,DELIVER_INVOICE)   
           ,END_DATE_ACTIVE    = l_b2b_config_tbl_typ.END_DATE_ACTIVE      
           ,LAST_UPDATE_DATE   = SYSDATE     
           ,LAST_UPDATED_BY    = l_user_id     
           ,TP_NAME            = NVL(l_b2b_config_tbl_typ.TP_NAME,TP_NAME)            
           ,DEFAULT_EMAIL      = NVL(l_b2b_config_tbl_typ.DEFAULT_EMAIL,DEFAULT_EMAIL) 
           ,NOTIFICATION_EMAIL = NVL(l_b2b_config_tbl_typ.NOTIFICATION_EMAIL,NOTIFICATION_EMAIL)
           ,NOTIFY_ACCOUNT_MGR = NVL(l_b2b_config_tbl_typ.NOTIFY_ACCOUNT_MGR,NOTIFY_ACCOUNT_MGR) 
           ,SOA_EMAIL          = l_b2b_config_tbl_typ.SOA_EMAIL         
           ,ASN_EMAIL          = l_b2b_config_tbl_typ.ASN_EMAIL            
           ,COMMENTS           = NVL(l_b2b_config_tbl_typ.COMMENTS,COMMENTS)        
           ,DELIVER_POD        = l_b2b_config_tbl_typ.DELIVER_POD         
           ,POD_EMAIL          = l_b2b_config_tbl_typ.POD_EMAIL           
           ,POD_FREQUENCY      = l_b2b_config_tbl_typ.POD_FREQUENCY        
           ,SOA_PRINT_PRICE    = l_b2b_config_tbl_typ.SOA_PRINT_PRICE      
           ,POD_PRINT_PRICE    = l_b2b_config_tbl_typ.POD_PRINT_PRICE      
           ,BUSINESS_EVENT_ELIGIBLE = NVL(l_b2b_config_tbl_typ.BUSINESS_EVENT_ELIGIBLE,BUSINESS_EVENT_ELIGIBLE)
           WHERE PARTY_ID        = l_party_id
           AND   CUST_ACCOUNT_ID = l_b2b_config_tbl_typ.CUSTOMER_ID
		   AND   ((l_b2b_config_tbl_typ.SITE_USE_ID IS NULL AND SITE_USE_ID IS NULL) OR
                  (l_b2b_config_tbl_typ.SITE_USE_ID IS NOT NULL AND SITE_USE_ID = l_b2b_config_tbl_typ.SITE_USE_ID)
				 );	   
        END IF;
		COMMIT;
		IF NVL(g_xxwc_woc_debug,'N') = 'Y' THEN
          xxwc_checkitapp_pkg.xxwc_log_debug_info
                    (P_API_NAME         =>l_proc_name,
                     P_CUSTOMER_ID      => p_b2b_config_tbl_typ.customer_id,
                     P_JOB_SITE_ID      => l_b2b_config_tbl_typ.site_use_id,
                     P_DEBUG_TEXT       => 'End SET_B2B_ACCOUNT',
                     P_USER_ID          => l_user_id
                    );
        END IF;
        p_return_status  := 'S';
        p_return_message := '';
     EXCEPTION
          WHEN e_biz_exception THEN
             p_return_status  := l_return_status;
             p_return_message := l_return_message;
             ROLLBACK;
          WHEN OTHERS THEN
             p_return_status  := 'E';
             p_return_message := SUBSTR(SQLERRM,1,2000);
             ROLLBACK;
     END Set_b2b_account;
     
--Rev# 5.0 <Start 
PROCEDURE CREATE_CUSTOM_QUOTE
(
     p_order_header_rec     IN   xxwc.xxwc_order_header_rec
    ,p_order_lines_tbl      IN   xxwc.xxwc_order_lines_tbl
    ,o_status               OUT VARCHAR2
    ,o_quote_number         OUT NUMBER
    ,o_quote_total          OUT NUMBER  
    ,o_message              OUT VARCHAR2
    ,p_debug_mode           IN  VARCHAR2 DEFAULT NULL    --E/T/NULL 
)
IS
   l_sec     VARCHAR2(3000);
   l_err_msg VARCHAR2(3000);
   l_return_status                VARCHAR2 (2000);
   l_return_msg                   VARCHAR2 (4000);
   l_org                          VARCHAR2 (20)                          := '162'; -- OPERATING UNIT
   l_user_id                      NUMBER;
   l_resp_id                      NUMBER;
   l_resp_appl_id                 NUMBER;
   l_cust_contact_id              NUMBER;
   l_header_id                    NUMBER := NULL;
   l_line_id                      NUMBER := NULL;
   l_order_source_id              NUMBER;
   l_oracle_quote_total           NUMBER; 
   l_invoice_to_org_id            NUMBER;
   l_user_name                    apps.fnd_user.user_name%TYPE; 
   l_xxwc_oso_debug               VARCHAR2(30) := fnd_profile.value('XXWC_OSO_DEBUG');
   l_ship_from_org_id             NUMBER; 
   l_new_qn                       NUMBER;
   l_segment1                     mtl_system_items_b.segment1%TYPE;
   l_description                  mtl_system_items_b.description%TYPE;
   l_unit_replacement_cost        NUMBER; 
   l_average_cost                 NUMBER;  
   l_gm_percent                   NUMBER;
   l_quote_gm                     NUMBER;
   l_quote_gm_percent             NUMBER;    
   l_la                           NUMBER :=0;
   l_ta                           NUMBER :=0; 
BEGIN
   l_sec := 'Start CREATE_CUSTOM_QUOTE';
   
   BEGIN 
      SELECT USER_ID
        INTO l_user_id
        FROM apps.fnd_user
       WHERE UPPER(USER_NAME) = UPPER(p_order_header_rec.created_by);
      EXCEPTION
      WHEN OTHERS THEN
         o_status := 'E';
         o_message := 'User NTID ' ||p_order_header_rec.created_by||' not exist in ERP';
         RETURN;
   END; 
   DBMS_OUTPUT.put_line ('Valid NTID : '||p_order_header_rec.created_by);
      
   IF IS_VALID_OM_USER ( l_user_id ) != 'Y' THEN
      o_message := 'Responsibility not assigned to NTID : ' ||p_order_header_rec.created_by||' to create/ update sales order ';
      o_status := 'E';
      RETURN;
   END IF;  
    
   BEGIN
     SELECT order_source_id
       INTO l_order_source_id
       FROM apps.oe_order_sources oos
      WHERE oos.enabled_flag = 'Y'
        AND UPPER(name) = UPPER(TRIM(p_order_header_rec.order_source_type));
   EXCEPTION
   WHEN NO_DATA_FOUND THEN
      o_message      := 'Sales Order Source '||p_order_header_rec.order_source_type||' not exist in ERP';
      o_status       :='E';
      RETURN;
   WHEN OTHERS THEN
      o_message      := 'Sales Order Source '||p_order_header_rec.order_source_type||' not exist in ERP';
      o_status       :='E';
      RETURN;
   END;

   DBMS_OUTPUT.put_line ('Sales Order order_source_id : '||l_order_source_id);
   
   BEGIN
     SELECT organization_id
       INTO l_ship_from_org_id
       FROM apps.mtl_parameters mp
      WHERE UPPER(organization_code) = UPPER(TRIM(p_order_header_rec.branch_code));
   EXCEPTION
   WHEN NO_DATA_FOUND THEN
      o_message      := 'Branch Code '||p_order_header_rec.branch_code||' not exist in ERP';
      o_status       :='E';
      RETURN;
   WHEN OTHERS THEN
      o_message      := 'Branch Code '||p_order_header_rec.branch_code||' not exist in ERP';
      o_status       :='E';
      RETURN;
   END;
   
   DBMS_OUTPUT.put_line ('Sales Order l_ship_from_org_id : '||l_ship_from_org_id);
   
   IF p_order_header_rec.ship_to_org_id IS NULL THEN
     o_message      := 'Ship to site is not provided, please validate customer ship to site site setup - ship_to_org_id: '||p_order_header_rec.ship_to_org_id;
     o_status       :='E';
     RETURN;
   END IF;
   
   -- INITIALIZATION REQUIRED FOR R12
   mo_global.set_policy_context ('S', l_org);
   mo_global.init ('ONT');

   l_resp_id      := 50857;
   l_resp_appl_id := 660;

   --INITIALIZE ENVIRONMENT
   fnd_global.apps_initialize (l_user_id, l_resp_id, l_resp_appl_id);
   
   SELECT xxwc_om_quote_header_s.NEXTVAL INTO l_new_qn FROM DUAL;
   
   IF p_order_header_rec.invoice_to_org_id IS NULL  THEN
         BEGIN
             SELECT hcsu.site_use_id
              INTO l_invoice_to_org_id
              FROM hz_cust_acct_sites_all   hcas
                 , hz_cust_site_uses_all    hcsu
             WHERE 1                           = 1
               AND hcas.cust_account_id        = p_order_header_rec.sold_to_org_id
               AND hcas.cust_acct_site_id      = hcsu.cust_acct_site_id
               AND hcas.status                 = 'A'
               AND hcsu.status                 = 'A'
               AND hcsu.site_use_code          = 'BILL_TO'
               AND hcas.org_id                 = 162
               AND hcsu.primary_flag           = 'Y'
               AND ROWNUM = 1;  
         EXCEPTION
         WHEN OTHERS THEN
            SELECT hcsu_b.site_use_id
              INTO l_invoice_to_org_id
              FROM hz_Cust_site_uses_all hcsu_b
                 , hz_Cust_site_uses_all hcsu_s
             WHERE hcsu_s.site_use_id       = p_order_header_rec.ship_to_org_id
               AND hcsu_s.site_use_code     = 'SHIP_TO'
               AND hcsu_b.cust_acct_site_id = hcsu_s.cust_acct_site_id
               AND hcsu_b.site_use_code     = 'BILL_TO'
               AND hcsu_s.status            = 'A'
               AND hcsu_b.status            = 'A'
               AND ROWNUM                   = 1;
         END;
      ELSE
         l_invoice_to_org_id := p_order_header_rec.invoice_to_org_id;
      END IF;
    
   INSERT INTO apps.XXWC_OM_QUOTE_HEADERS (QUOTE_NUMBER
                                       , VALID_UNTIL
                                       , CUST_ACCOUNT_ID
                                       , PRIMARY_SITE_USE_ID
                                       , SITE_USE_ID
                                       , CONTACT_ID
                                       , SALESREP_ID
                                       , SHIPPING_METHOD
                                       , ORGANIZATION_ID
                                       , NOTES
                                       , CREATION_DATE
                                       , CREATED_BY
                                       , LAST_UPDATE_DATE
                                       , LAST_UPDATED_BY
                                       , LAST_UPDATE_LOGIN
                                       , ORG_ID
                                       , ATTRIBUTE15)
       VALUES  ( l_new_qn
               , TO_DATE( p_order_header_rec.expiration_date ,'YY-MM-DD')
               , p_order_header_rec.sold_to_org_id
               , l_invoice_to_org_id --PRIMARY_SITE_USE_ID
               , p_order_header_rec.ship_to_org_id--SITE_USE_ID
               , p_order_header_rec.invoice_to_contact_id
               , NVL(p_order_header_rec.salesrep_id, XXWC_OM_SO_CREATE.get_sales_rep_id (p_order_header_rec.sold_to_org_id ))
               , p_order_header_rec.shipping_method_code
               , l_ship_from_org_id
               , p_order_header_rec.shipping_instructions
               , SYSDATE
               , l_user_id
               , SYSDATE
               , L_USER_ID
               , -1
               , l_org
               , p_order_header_rec.orig_sys_document_ref 
       );

  -- XXWC_PO_HELPERS.XXWC_FIND_USER_NAME (l_user_id); --Created by
   FOR l_tbl_idx IN p_order_lines_tbl.first..p_order_lines_tbl.last LOOP
    
        BEGIN
           SELECT msib.segment1
                 ,msib.description  
             INTO l_segment1
                 ,l_description   
             FROM mtl_system_items_b msib
            WHERE msib.inventory_item_id   = p_order_lines_tbl(l_tbl_idx).inventory_item_id
              AND organization_id = 222;
        EXCEPTION
        WHEN OTHERS THEN
           o_message      := 'inventory_item_id '||p_order_lines_tbl(l_tbl_idx).inventory_item_id||' not exist in ERP';
           o_status       := 'E';
           RETURN;
        END;     
        
        l_unit_replacement_cost :=  XXWC_OM_SO_CREATE.Get_replacement_cost( p_ship_from_org_id    => l_ship_from_org_id
                                                                           ,p_inventory_item_id   => p_order_lines_tbl(l_tbl_idx).inventory_item_id
                                                                          );
        
        IF NVL(l_unit_replacement_cost, 0) <> 0 THEN
           l_average_cost := NULL;
		   IF ((p_order_lines_tbl(l_tbl_idx).unit_selling_price IS NOT NULL) AND p_order_lines_tbl(l_tbl_idx).unit_selling_price <> 0 ) THEN --Rev# 6.0
              l_gm_percent := round((p_order_lines_tbl(l_tbl_idx).unit_selling_price-l_unit_replacement_cost)*100/p_order_lines_tbl(l_tbl_idx).unit_selling_price,2);
		   ELSE--Rev# 6.0
		     l_gm_percent:= NULL;--Rev# 6.0
		   END IF; --Rev# 6.0
        ELSE
           l_average_cost := round(cst_cost_api.get_item_cost(1,p_order_lines_tbl(l_tbl_idx).inventory_item_id,l_ship_from_org_id),5);	
		   IF ((p_order_lines_tbl(l_tbl_idx).unit_selling_price IS NOT NULL) AND p_order_lines_tbl(l_tbl_idx).unit_selling_price <> 0 ) THEN --Rev# 6.0
              l_gm_percent := round((p_order_lines_tbl(l_tbl_idx).unit_selling_price-l_average_cost)*100/p_order_lines_tbl(l_tbl_idx).unit_selling_price,2);
		   ELSE--Rev# 6.0
		     l_gm_percent:= NULL;--Rev# 6.0
		   END IF; --Rev# 6.0
        END IF;
                        
        INSERT INTO apps.XXWC_OM_QUOTE_LINES (QUOTE_NUMBER
                                     , LINE_SEQ
                                     , INVENTORY_ITEM_ID
                                     , ITEM_DESC
                                     , AVERAGE_COST
                                     , SPECIAL_COST
                                     , LINE_QUANTITY
                                     , GM_PERCENT
                                     , GM_SELLING_PRICE
                                     , LIST_PRICE
                                     , EXTENDED_AMOUNT
                                     , NOTES
                                     , CREATION_DATE
                                     , CREATED_BY
                                     , LAST_UPDATE_DATE
                                     , LAST_UPDATED_BY
                                     , LAST_UPDATE_LOGIN
                                     , ORG_ID
                                     , replacement_cost)
         VALUES( l_new_qn
               , p_order_lines_tbl(l_tbl_idx).line_id
               , p_order_lines_tbl(l_tbl_idx).inventory_item_id
               , l_description
               , l_average_cost     
               , NULL --Checkit user can not enter the vendor cost
               , p_order_lines_tbl(l_tbl_idx).ordered_quantity
               , l_gm_percent
               , p_order_lines_tbl(l_tbl_idx).unit_selling_price--GM_SELLING_PRICE
               , XXWC_OM_SO_CREATE.get_list_price
                                                 ( i_organization_id   => l_ship_from_org_id
                                                 , i_inventory_item_id => p_order_lines_tbl(l_tbl_idx).inventory_item_id)
               , nvl(p_order_lines_tbl(l_tbl_idx).ordered_quantity,0)*nvl(p_order_lines_tbl(l_tbl_idx).unit_selling_price,0)
               , p_order_lines_tbl(l_tbl_idx).shipping_instructions
               , SYSDATE
               , l_user_id
               , SYSDATE
               , L_USER_ID
               , -1
               , l_org
               , l_unit_replacement_cost
              );

        l_line_id := NULL;
     END LOOP;
     
     l_return_status := 'S';
     
     -- CHECK RETURN STATUS
     IF l_return_status = fnd_api.g_ret_sts_success
     THEN
         DBMS_OUTPUT.put_line ('Quote Successfully Created');
        
         o_quote_number :=l_new_qn;
         o_status       :=l_return_status;
         
         DBMS_OUTPUT.put_line ('Calcuate Tax');
         COMMIT;
         
         xxwc_om_quote_pkg.calculate_tax(l_new_qn,'T',l_return_status,l_return_msg);

         BEGIN
            SELECT ROUND (
                SUM (
	                    nvl(line_quantity,0)*nvl(gm_selling_price,0)
	                   ) 
	           , 4)
	         ,
	           ROUND (
                SUM (
	                    nvl(tax_amount,0)
	                 ) 
	           , 4)
                INTO l_la, l_ta
             FROM xxwc.xxwc_om_quote_lines
             WHERE quote_number = l_new_qn;
            
            l_oracle_quote_total := ROUND (l_la + l_ta,2);
         EXCEPTION
         WHEN OTHERS THEN
            l_oracle_quote_total := 0;
         END;
         
         XXWC_OM_SO_CREATE.XXWC_Get_Quote_Margin( p_quote_number         => l_new_qn
                                                 ,x_order_margin_percent => l_quote_gm_percent
                                                 ,x_order_margin_amount  => l_quote_gm
                                                );
         UPDATE apps.XXWC_OM_QUOTE_HEADERS
            SET GROSS_MARGIN = l_quote_gm_percent,
                line_amount = l_la,
                tax_amount = l_ta
          WHERE quote_number = l_new_qn;
          
         o_quote_total := l_oracle_quote_total;
         
         IF NVL(p_debug_mode, 'N') = 'T' OR NVL(l_xxwc_oso_debug, 'X') = 'Y' THEN
            XXWC_OM_SO_CREATE.DEBUG_LOG( p_order_header_rec     => p_order_header_rec
                      ,p_order_lines_tbl      => p_order_lines_tbl
                      ,p_status               => o_status
                      ,p_order_number         => o_quote_number
                      ,p_order_total          => l_oracle_quote_total 
                      ,p_message              => o_message
                     );
         END IF;
         COMMIT;
      END IF;

      RETURN;
    EXCEPTION
    WHEN OTHERS THEN
      DBMS_OUTPUT.put_line ('Failed to Create Sales Order');
      o_message      := 'Failed to Create Sales Order';
      o_status       :='E';
      
      IF NVL(p_debug_mode, 'N') = 'T' OR NVL(l_xxwc_oso_debug, 'X') = 'Y' THEN
            XXWC_OM_SO_CREATE.DEBUG_LOG( p_order_header_rec     => p_order_header_rec
                      ,p_order_lines_tbl      => p_order_lines_tbl
                      ,p_status               => o_status
                      ,p_order_number         => o_quote_number
                      ,p_order_total          => l_oracle_quote_total  
                      ,p_message              => o_message
                     );
      END IF;
      ROLLBACK;
      xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => 'XXWC_CHECKITAPP_PKG.CREATE_CUSTOM_QUOTE'
        ,p_calling             => l_sec
        ,p_request_id          => fnd_global.conc_request_id
        ,p_ora_error_msg       => SQLERRM
        ,p_error_desc          => 'Error creating custom quote'
        ,p_distribution_list   => g_dflt_email
        ,p_module              => 'XXWC');
       RETURN;
    END CREATE_CUSTOM_QUOTE; --end of procedure
    
    FUNCTION GET_UNITWGT_TOTAL_WC_QUOTE(  p_quote_number     IN NUMBER
                                 ,p_ship_from_org_id IN NUMBER     
                                )  RETURN NUMBER AS
        v_unit_weight  NUMBER;
    BEGIN
      SELECT SUM(NVL(unit_weight,0)*NVL(xoql.line_quantity,0))
        INTO v_unit_weight
        FROM mtl_system_items_b msi
          ,xxwc_om_quote_lines xoql
      WHERE msi.inventory_item_id = xoql.inventory_item_id
        AND msi.organization_id = p_ship_from_org_id
        AND xoql.quote_number = p_quote_number;
   
      RETURN round(v_unit_weight,2);

    EXCEPTION
    WHEN OTHERS THEN
       RETURN 0;
    END;

    FUNCTION GET_WC_QUOTE_TOTAL(  p_quote_number     IN NUMBER
                                )  RETURN NUMBER AS
        v_quote_total  NUMBER;
    BEGIN
      SELECT SUM(NVL(xoqh.line_amount,0)+NVL(xoqh.tax_amount,0))
        INTO v_quote_total
        FROM xxwc.xxwc_om_quote_headers xoqh
      WHERE xoqh.quote_number = p_quote_number;
   
      RETURN round(v_quote_total,2);

    EXCEPTION
    WHEN OTHERS THEN
       RETURN 0;
    END;
    
   --Rev# 6.0 <Start
   /*******************************************************************************
   Function Name   :   PRINT_QUOTE_DISCLAIMER
   Description     :   This function will be used to print quote disclaimer
   ******************************************************************************/
   FUNCTION PRINT_QUOTE_DISCLAIMER (  p_quote_number     IN NUMBER 
                                     ,p_ship_from_org_id IN NUMBER
                                    )
      RETURN VARCHAR2
   AS 
      l_count NUMBER :=0;
   BEGIN
      SELECT count(1) 
        INTO l_count 
        FROM apps.fnd_lookup_values  flv
            ,xxwc.xxwc_qp_replacement_cost_tbl xqrc
            ,APPS.ORG_ORGANIZATION_DEFINITIONS OOD 
            ,APPS.xxwc_om_quote_lines xoql
      WHERE flv.lookup_type        = 'WC_REPLACEMENT_COST_HIERARCHY'
        and UPPER(FLV.DESCRIPTION) = UPPER(XQRC.DISTRICT)
        AND xqrc.inventory_item_id = xoql.inventory_item_id 
        AND flv.meaning            = ood.organization_code
        AND flv.enabled_flag       = 'Y'
        AND TRUNC(SYSDATE) BETWEEN TRUNC(flv.start_date_active) AND TRUNC(NVL(flv.end_date_active,SYSDATE))
        and TRUNC(sysdate) between TRUNC(XQRC.START_DATE_ACTIVE) and TRUNC(NVL(XQRC.END_DATE_ACTIVE,sysdate))
        AND xoql.quote_number      = p_quote_number
        AND ood.organization_id    = p_ship_from_org_id
		AND ROWNUM = 1;
		
        IF l_count > 0 THEN
           RETURN 'Y';
        ELSE
           RETURN 'N';
        END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 'N';
   END PRINT_QUOTE_DISCLAIMER;
   --Rev# 6.0 >End

    FUNCTION GET_CUSTOM_QUOTE_INFO(  p_ntid      IN VARCHAR2
                          ,p_quote_number IN NUMBER DEFAULT NULL
                        ) return sys_refcursor IS
   v_rec sys_refcursor;
   l_org    VARCHAR2 (20)  := '162'; -- OPERATING UNIT
   l_cust_account_id  NUMBER;
   BEGIN
     mo_global.set_policy_context ('S', l_org);

     IF p_ntid IS NULL AND p_quote_number IS NULL THEN
       RETURN NULL;
     ELSIF p_ntid IS NOT NULL AND p_quote_number IS NULL THEN
     OPEN
       v_rec for
       'SELECT xoqh.quote_number
      ,xoqh.creation_date quote_date
      ,xoqh.valid_until expiration_date
      ,cust_acct.account_number cust_account_code
      ,xoqh.cust_account_id sold_to_org_id
      ,xoqh.site_use_id ship_to_org_id
      ,xoqh.primary_site_use_id invoice_to_org_id
      ,xoqh.organization_id ship_from_org_id
      ,xoqh.salesrep_id
      ,NVL(XXWC_GEN_ROUTINES_PKG.salesman(xoqh.salesrep_id,''SHORT'')
          ,XXWC_MV_ROUTINES_PKG.get_salesrep_info(sr.salesrep_number)) SALES_REP_NAME--Rev 6.0
      ,xoqh.shipping_method shipping_method_code
      ,wcs.ship_method_meaning
      ,party.party_name customer_name
      ,NULL email_address
      ,ship_su.location
      ,ship_ps.party_site_number
      ,(ship_su.location  || '', ''|| ship_ps.party_site_number) Ship_num
      ,ship_loc.address1 ship_address1
      ,ship_loc.address2 ship_address2
      ,ship_loc.address3 ship_address3
      ,ship_loc.address4 ship_address4
      ,DECODE (ship_loc.city, NULL, NULL, ship_loc.city || '', '')
                || DECODE (ship_loc.state,
                           NULL, ship_loc.province || '', '',
                           ship_loc.state || '', '')
                || DECODE (ship_loc.postal_code, NULL, NULL, ship_loc.postal_code)
                   ship_address5
      ,ship_loc.city     ship_city
      ,ship_loc.state    ship_state
      ,ship_loc.postal_code ship_to_postal_code
      ,bill_loc.address1 billto_address1
      ,bill_loc.address2 billto_address2
      ,bill_loc.address3 billto_address3
      ,bill_loc.address4 billto_address4
      ,DECODE (bill_loc.city, NULL, NULL, bill_loc.city || '', '')
                || DECODE (bill_loc.state,
                           NULL, bill_loc.province || '', '',
                           bill_loc.state || '', '')
                || DECODE (bill_loc.postal_code, NULL, NULL, bill_loc.postal_code)
                   billto_address5
      ,bill_loc.city     billto_city
      ,bill_loc.state    billto_state
      ,bill_loc.postal_code billto_postal_code
      ,bill_hcp.phone_area_code
      ,bill_hcp.phone_number
      ,CASE WHEN INSTR (bill_hcp.phone_number, ''-'', 4) = 0 THEN bill_hcp.phone_area_code
                || ''-''
                || SUBSTR (bill_hcp.phone_number, 1, 3)
                || ''-''
                || SUBSTR (bill_hcp.phone_number, 4, 7)
                ELSE bill_hcp.phone_area_code
                || ''-''
                || bill_hcp.phone_number END as billto_phone
      ,xoqh.contact_id invoice_to_contact_id
      ,XXWC_OM_SO_CREATE.get_job_site_contact(p_ship_to_contact_id => xoqh.contact_id) SHIP_TO_PHONE
      ,XXWC_OM_SO_CREATE.Get_Sold_to_contact_phone(p_sold_to_contact_id =>  xoqh.contact_id ) SOLD_TO_PHONE
      ,TRIM(invoice_party.person_title || ''  '' || invoice_party.PERSON_FIRST_NAME || ''  '' || invoice_party.PERSON_LAST_NAME) INVOICE_TO_CONTACT
      ,hl.LOCATION_CODE
      ,hl.address_line_1 branch_street
      ,DECODE (hl.town_or_city, NULL, NULL, hl.town_or_city || '','')
       ||DECODE (hl.region_2, NULL, NULL, hl.region_2 || '','')
       || DECODE (hl.postal_code, NULL, NULL, hl.postal_code) branch_city --Rev 6.0
      ,hl.telephone_number_1 branch_tel1
      ,hl.telephone_number_2 branch_tel2
      ,XXWC_GEN_ROUTINES_PKG.TAKENBY(xoqh.created_by,''SHORT'') created_by
      ,ship_su.attribute3 map
      ,XXWC_OM_SO_CREATE.Payment_term( bill_su.payment_term_id) CCTDHR
      ,xoqh.attribute15 ORDER_SOURCE_REFERENCE
      ,ROUND(NVL(xoqh.line_amount,0), 2) LINES_TOTAL
      ,(ROUND(NVL(xoqh.tax_amount,0) -
        NVL(XXWC_OM_CFD_PKG.secondary_tax_wc_quote(xoqh.quote_number , xoqh.site_use_id , xoqh.organization_id, NULL),0),2))  TAXES_TOTAL
      ,ROUND(NVL(XXWC_OM_CFD_PKG.secondary_tax_wc_quote(xoqh.quote_number , xoqh.site_use_id , xoqh.organization_id, NULL),0),2) LUMBER_TAXES
      ,XXWC_OM_SO_CREATE.GET_LUMBER_TAX_RATE LUMBER_TAX_RATE
      ,XXWC_CHECKITAPP_PKG.GET_WC_QUOTE_TOTAL(xoqh.quote_number) QUOTE_TOTAL
      ,xoqh.notes SHIPPING_INSTRUCTIONS
      ,XXWC_CHECKITAPP_PKG.GET_UNITWGT_TOTAL_wc_quote(xoqh.quote_number, xoqh.organization_id) UNITWGT_TOTAL
      ,XXWC_CHECKITAPP_PKG.PRINT_QUOTE_DISCLAIMER (xoqh.quote_number, xoqh.organization_id) PRN_DISC --Rev 6.0  
      FROM xxwc.xxwc_om_quote_headers xoqh
       , hz_cust_accounts cust_acct
       , hz_parties party
       , wsh_carrier_services_v wcs 
       , hz_cust_site_uses_all ship_su
       , hz_cust_acct_sites_all ship_cas
       , hz_party_sites ship_ps
       , hz_locations ship_loc
       , hz_cust_site_uses_all bill_su
       , hz_cust_acct_sites_all bill_cas
       , hz_party_sites bill_ps
       , hz_locations bill_loc
       , hz_contact_points bill_hcp
       , hz_cust_account_roles invoice_roles
       , hz_parties invoice_party
       , hz_relationships invoice_rel
       , hz_cust_accounts invoice_acct
       , hr_all_organization_units ship_from_org
       , hr_locations_all hl
       , fnd_user fu
       , jtf_rs_salesreps sr --Rev# 6.0  
      WHERE 1=1
        AND xoqh.salesrep_id                = sr.salesrep_id(+) --Rev# 6.0
        AND xoqh.cust_account_id            = cust_acct.cust_account_id
        AND cust_acct.party_id              = party.party_id(+)
        AND wcs.ship_method_code            = xoqh.shipping_method
        AND xoqh.site_use_id                = ship_su.site_use_id(+)
        AND ship_su.cust_acct_site_id       = ship_cas.cust_acct_site_id(+)
        AND ship_cas.party_site_id          = ship_ps.party_site_id(+)
        AND ship_loc.location_id(+)         = ship_ps.location_id
        AND xoqh.primary_site_use_id        = bill_su.site_use_id(+)
        AND bill_su.cust_acct_site_id       = bill_cas.cust_acct_site_id(+)
        AND bill_cas.party_site_id          = bill_ps.party_site_id(+)
        AND bill_loc.location_id(+)         = bill_ps.location_id
        AND bill_hcp.owner_table_name(+)    = ''HZ_PARTY_SITES''
        AND bill_hcp.primary_flag(+)        = ''Y''
        AND bill_hcp.owner_table_id(+)      = bill_ps.party_site_id
        AND xoqh.contact_id                 = invoice_roles.cust_account_role_id(+)
        AND invoice_roles.party_id          = invoice_rel.party_id(+)
        AND invoice_roles.role_type(+)      = ''CONTACT''
        AND invoice_roles.cust_account_id   = invoice_acct.cust_account_id(+)
        AND NVL (invoice_rel.object_id, -1) = NVL (invoice_acct.party_id, -1)
        AND invoice_rel.subject_id          = invoice_party.party_id(+)
        AND xoqh.organization_id            = ship_from_org.organization_id(+)
        AND ship_from_org.location_id       = hl.location_id
        AND xoqh.attribute15                = ''CHECKIT''
        AND xoqh.created_by                 = fu.user_id
        AND UPPER(fu.user_name)             = UPPER(:p_ntid)'
       using p_ntid;
     ELSIF p_quote_number IS NOT NULL THEN
       OPEN
       v_rec for
       'SELECT xoqh.quote_number
      ,xoqh.creation_date quote_date
      ,xoqh.valid_until expiration_date
      ,cust_acct.account_number cust_account_code
      ,xoqh.cust_account_id sold_to_org_id
      ,xoqh.site_use_id ship_to_org_id
      ,xoqh.primary_site_use_id invoice_to_org_id
      ,xoqh.organization_id ship_from_org_id
      ,xoqh.salesrep_id
      ,NVL(XXWC_GEN_ROUTINES_PKG.salesman(xoqh.salesrep_id,''SHORT'')
          ,XXWC_MV_ROUTINES_PKG.get_salesrep_info(sr.salesrep_number)) SALES_REP_NAME --Rev 6.0
      ,xoqh.shipping_method shipping_method_code
      ,wcs.ship_method_meaning
      ,party.party_name customer_name
      ,NULL email_address
      ,ship_su.location
      ,ship_ps.party_site_number
      ,(ship_su.location  || '', ''|| ship_ps.party_site_number) Ship_num
      ,ship_loc.address1 ship_address1
      ,ship_loc.address2 ship_address2
      ,ship_loc.address3 ship_address3
      ,ship_loc.address4 ship_address4
      ,DECODE (ship_loc.city, NULL, NULL, ship_loc.city || '', '')
                || DECODE (ship_loc.state,
                           NULL, ship_loc.province || '', '',
                           ship_loc.state || '', '')
                || DECODE (ship_loc.postal_code, NULL, NULL, ship_loc.postal_code)
                   ship_address5
      ,ship_loc.city     ship_city
      ,ship_loc.state    ship_state
      ,ship_loc.postal_code ship_to_postal_code
      ,bill_loc.address1 billto_address1
      ,bill_loc.address2 billto_address2
      ,bill_loc.address3 billto_address3
      ,bill_loc.address4 billto_address4
      ,DECODE (bill_loc.city, NULL, NULL, bill_loc.city || '', '')
                || DECODE (bill_loc.state,
                           NULL, bill_loc.province || '', '',
                           bill_loc.state || '', '')
                || DECODE (bill_loc.postal_code, NULL, NULL, bill_loc.postal_code)
                   billto_address5
      ,bill_loc.city     billto_city
      ,bill_loc.state    billto_state
      ,bill_loc.postal_code billto_postal_code
      ,bill_hcp.phone_area_code
      ,bill_hcp.phone_number
      ,CASE WHEN INSTR (bill_hcp.phone_number, ''-'', 4) = 0 THEN bill_hcp.phone_area_code
                || ''-''
                || SUBSTR (bill_hcp.phone_number, 1, 3)
                || ''-''
                || SUBSTR (bill_hcp.phone_number, 4, 7)
                ELSE bill_hcp.phone_area_code
                || ''-''
                || bill_hcp.phone_number END as billto_phone
      ,xoqh.contact_id invoice_to_contact_id
      ,XXWC_OM_SO_CREATE.get_job_site_contact(p_ship_to_contact_id => xoqh.contact_id) SHIP_TO_PHONE
      ,XXWC_OM_SO_CREATE.Get_Sold_to_contact_phone(p_sold_to_contact_id =>  xoqh.contact_id ) SOLD_TO_PHONE
      ,TRIM(invoice_party.person_title || ''  '' || invoice_party.PERSON_FIRST_NAME || ''  '' || invoice_party.PERSON_LAST_NAME) INVOICE_TO_CONTACT
      ,hl.LOCATION_CODE
      ,hl.address_line_1 branch_street
      ,DECODE (hl.town_or_city, NULL, NULL, hl.town_or_city || '','')
       ||DECODE (hl.region_2, NULL, NULL, hl.region_2 || '','')
       || DECODE (hl.postal_code, NULL, NULL, hl.postal_code) branch_city --Rev 6.0
      ,hl.telephone_number_1 branch_tel1
      ,hl.telephone_number_2 branch_tel2
      ,XXWC_GEN_ROUTINES_PKG.TAKENBY(xoqh.created_by,''SHORT'') created_by
      ,ship_su.attribute3 map
      ,XXWC_OM_SO_CREATE.Payment_term( bill_su.payment_term_id) CCTDHR
      ,xoqh.attribute15 ORDER_SOURCE_REFERENCE
      ,ROUND(NVL(xoqh.line_amount,0), 2) LINES_TOTAL
      ,(ROUND(NVL(xoqh.tax_amount,0) -
        NVL(XXWC_OM_CFD_PKG.secondary_tax_wc_quote(xoqh.quote_number , xoqh.site_use_id , xoqh.organization_id, NULL),0),2))  TAXES_TOTAL
      ,ROUND(NVL(XXWC_OM_CFD_PKG.secondary_tax_wc_quote(xoqh.quote_number , xoqh.site_use_id , xoqh.organization_id, NULL),0),2) LUMBER_TAXES
      ,XXWC_OM_SO_CREATE.GET_LUMBER_TAX_RATE LUMBER_TAX_RATE
      ,XXWC_CHECKITAPP_PKG.GET_WC_QUOTE_TOTAL(xoqh.quote_number) QUOTE_TOTAL
      ,xoqh.notes SHIPPING_INSTRUCTIONS
      ,XXWC_CHECKITAPP_PKG.GET_UNITWGT_TOTAL_wc_quote(xoqh.quote_number, xoqh.organization_id) UNITWGT_TOTAL
      ,XXWC_CHECKITAPP_PKG.PRINT_QUOTE_DISCLAIMER (xoqh.quote_number, xoqh.organization_id) PRN_DISC --Rev 6.0
      FROM xxwc.xxwc_om_quote_headers xoqh
       , hz_cust_accounts cust_acct
       , hz_parties party
       , wsh_carrier_services_v wcs 
       , hz_cust_site_uses_all ship_su
       , hz_cust_acct_sites_all ship_cas
       , hz_party_sites ship_ps
       , hz_locations ship_loc
       , hz_cust_site_uses_all bill_su
       , hz_cust_acct_sites_all bill_cas
       , hz_party_sites bill_ps
       , hz_locations bill_loc
       , hz_contact_points bill_hcp
       , hz_cust_account_roles invoice_roles
       , hz_parties invoice_party
       , hz_relationships invoice_rel
       , hz_cust_accounts invoice_acct
       , hr_all_organization_units ship_from_org
       , hr_locations_all hl
       , fnd_user fu
       , jtf_rs_salesreps sr --Rev# 6.0
      WHERE 1=1
        AND xoqh.salesrep_id                = sr.salesrep_id(+) --Rev# 6.0
        AND xoqh.cust_account_id            = cust_acct.cust_account_id
        AND cust_acct.party_id              = party.party_id(+)
        AND wcs.ship_method_code            = xoqh.shipping_method
        AND xoqh.site_use_id                = ship_su.site_use_id(+)
        AND ship_su.cust_acct_site_id       = ship_cas.cust_acct_site_id(+)
        AND ship_cas.party_site_id          = ship_ps.party_site_id(+)
        AND ship_loc.location_id(+)         = ship_ps.location_id
        AND xoqh.primary_site_use_id        = bill_su.site_use_id(+)
        AND bill_su.cust_acct_site_id       = bill_cas.cust_acct_site_id(+)
        AND bill_cas.party_site_id          = bill_ps.party_site_id(+)
        AND bill_loc.location_id(+)         = bill_ps.location_id
        AND bill_hcp.owner_table_name(+)    = ''HZ_PARTY_SITES''
        AND bill_hcp.primary_flag(+)        = ''Y''
        AND bill_hcp.owner_table_id(+)      = bill_ps.party_site_id
        AND xoqh.contact_id                 = invoice_roles.cust_account_role_id(+)
        AND invoice_roles.party_id          = invoice_rel.party_id(+)
        AND invoice_roles.role_type(+)      = ''CONTACT''
        AND invoice_roles.cust_account_id   = invoice_acct.cust_account_id(+)
        AND NVL (invoice_rel.object_id, -1) = NVL (invoice_acct.party_id, -1)
        AND invoice_rel.subject_id          = invoice_party.party_id(+)
        AND xoqh.organization_id            = ship_from_org.organization_id(+)
        AND ship_from_org.location_id       = hl.location_id
        AND xoqh.created_by                 = fu.user_id
        AND UPPER(fu.user_name)             = UPPER(:p_ntid)
        AND xoqh.quote_number               = :p_quote_number'
        using p_ntid, p_quote_number;
     END IF;

     RETURN v_rec;
     
     CLOSE v_rec;
    EXCEPTION   
    WHEN OTHERS  THEN
      RETURN NULL;      
   END;
   
   FUNCTION GET_CUSTOM_QUOTE_LINE_INFO(  p_quote_number   IN NUMBER
                           ) return sys_refcursor IS
   v_rec sys_refcursor;
   l_org    VARCHAR2 (20)  := '162'; -- OPERATING UNIT
   l_cust_account_id  NUMBER;
   BEGIN

      mo_global.set_policy_context ('S', l_org);
      mo_global.init ('ONT');

     OPEN
       v_rec for
       'SELECT xoql.line_seq LINE_NUMBER
              ,msib.SEGMENT1 SEGMENT1
              ,xoql.item_desc part_description
              ,xoqh.organization_id   line_ship_from_org_id
              ,xoql.inventory_item_id
              ,NVL (xoql.line_quantity, 0) QTY_ORDERED 
              ,XXWC_OM_SO_CREATE.get_onhand (      p_inventory_item_id => xoql.inventory_item_id
                                ,p_organization_id  =>  xoqh.organization_id
                                ,p_base_uom         => msib.primary_unit_of_measure
                                ,p_ord_uom          => um.unit_of_measure
                                ,p_return_type      =>  ''H''
                                ) onhand
              ,XXWC_OM_SO_CREATE.get_onhand (      p_inventory_item_id => xoql.inventory_item_id
                                ,p_organization_id  =>  xoqh.organization_id
                                ,p_base_uom         => msib.primary_unit_of_measure
                                ,p_ord_uom          => um.unit_of_measure
                                ,p_return_type      =>  ''T''
                                ) available
              ,ROUND(NVL (xoql.list_price, 0), 2) unit_list_price 
              ,ROUND(NVL (xoql.gm_selling_price, 0) ,2) unit_selling_price 
              ,ROUND(NVL (xoql.line_quantity, 0) * NVL (xoql.gm_selling_price, 0) ,2) extended_price
              ,ROUND(NVL (xoql.tax_amount, 0), 2) tax_value
              ,um.unit_of_measure  UOM
              ,(msib.unit_weight|| '' '' ||weight_uom_code) unit_weight
              ,( SELECT  LISTAGG(l.segment1, ''|''||'' '') WITHIN GROUP (ORDER BY l.segment1)
                   FROM mtl_item_locations l
                  , mtl_secondary_locators_all_v v
                WHERE l.inventory_location_id = v.secondary_locator
                  AND v.inventory_item_id     = msib.inventory_item_id
                  AND v.organization_id       = xoqh.organization_id) LOCATORS
              ,xxwc_om_cfd_pkg.vendor_part_num(xoql.inventory_item_id) vendor_part_num
              ,um.unit_of_measure ord_uom
              ,msib.primary_unit_of_measure base_uom
              ,round(NVL(xoql.line_quantity,0)*NVL(xoql.gm_selling_price,0), 2) LINE_TOTAL
              ,msib.hazard_class_id
              ,DECODE(msib.hazard_class_id, NULL, NULL, ''HAZMAT'') HAZMAT
              ,xxwc_om_cfd_pkg.get_country_of_origin(xoqh.primary_site_use_id, msib.inventory_item_id) country_of_origin
              ,xoql.notes SHIPPING_INSTRUCTIONS
              ,DECODE(msib.item_type, ''SPECIAL'', ''THIS ITEM IS SPECIAL ORDER AND MAY BE NON-RETURNABLE'', NULL) SPECIAL
              ,XXWC_OM_SO_CREATE.GET_UN_NUMBER( p_inventory_item_id => xoql.inventory_item_id
                                               ,p_ship_from_org_id  => xoqh.organization_id) HAZMAT_UN_NUMBER
         FROM xxwc.xxwc_om_quote_headers xoqh
            , xxwc.xxwc_om_quote_lines xoql
            , mtl_system_items_b msib
            , mtl_units_of_measure_vl um
        WHERE 1=1
          AND xoqh.quote_number            = xoql.quote_number
          AND msib.inventory_item_id       = xoql.inventory_item_id
          AND msib.organization_id         = xoqh.organization_id
          AND msib.primary_unit_of_measure = um.unit_of_measure	
          AND xoqh.quote_number            = :p_quote_number
          ORDER BY xoql.line_seq'
       using p_quote_number;
       RETURN v_rec;
   
       CLOSE v_rec;
   EXCEPTION   
    WHEN OTHERS  THEN
      RETURN NULL;    
   END;
   --Rev# 5.0 < End
    
--Rev# 7.0 > Start 
/*
Name: GET_CUSTOMER_PROSPECT_INFO
Date: 08/21/2017
Author: Christian Baez, HD Supply
Description: Oracle Function to return sales rep name and phone number for specific customer account name
Parameters: 
    Input: Customer Name
    Output: Customer Name, Primary Address (BILL_TO), Sales Rep Name, Sales Rep Phone Number
*/
FUNCTION GET_CUSTOMER_PROSPECT_INFO ( p_customer_name     IN VARCHAR2
                                   ) return sys_refcursor
IS
   l_sec       VARCHAR2 (3000);
   v_rc sys_refcursor;
BEGIN
   l_sec := 'Start GET_CUSTOMER_PROSPECT_INFO';

   mo_global.set_policy_context('S',162);

   IF p_customer_name IS NOT NULL THEN

        OPEN v_rc for

        'SELECT                         
            PARTY.PARTY_NAME customer_name
            ,LOC.ADDRESS1||'' ''||
                LOC.ADDRESS2||'' ''||
                LOC.CITY||'' ''||
                (CASE
                    WHEN LENGTH (NVL (LOC.state, LOC.province)) > 2 THEN NULL
                        ELSE NVL (LOC.state, LOC.province)
                END)||'' ''||
                LOC.Postal_Code address    
                ,site.ATTRIBUTE1 
            ,NVL(XXWC_CHECKITAPP_PKG.get_master_acct_sales_rep (CUST_ACCT.cust_account_id), 
                 XXWC_CHECKITAPP_PKG.get_acct_sales_rep (CUST_ACCT.cust_account_id)
                ) sales_rep_name 
            ,XXWC_CHECKITAPP_PKG.get_salesrep_phone (CUST_ACCT.cust_account_id ) sales_rep_phone                       
            ,XXWC_CHECKITAPP_PKG.get_sales_rep_states_active (:p_customer_name ) sales_rep_states      
        FROM
            HZ_CUST_ACCT_SITES     ACCT_SITE,
            HZ_PARTY_SITES             PARTY_SITE,
            HZ_LOCATIONS               LOC,
            HZ_CUST_SITE_USES_ALL       SITE,
            HZ_PARTIES           PARTY,
            HZ_CUST_ACCOUNTS      CUST_ACCT
        WHERE 1 = 1 
        AND   SITE.SITE_USE_CODE         = ''BILL_TO''
        AND   SITE.CUST_ACCT_SITE_ID     = ACCT_SITE.CUST_ACCT_SITE_ID
        AND   ACCT_SITE.PARTY_SITE_ID    = PARTY_SITE.PARTY_SITE_ID
        AND   PARTY_SITE.LOCATION_ID     = LOC.LOCATION_ID
        and   acct_site.status=''A''
        AND   ACCT_SITE.CUST_ACCOUNT_ID=CUST_ACCT.CUST_ACCOUNT_ID
        AND   CUST_ACCT.PARTY_ID=PARTY.PARTY_ID
        AND   CUST_ACCT.status=''A''
        AND   UPPER(PARTY.PARTY_NAME)     LIKE UPPER(:p_customer_name)||''%'''
        USING p_customer_name,p_customer_name; 
        RETURN v_rc;
    END IF;

EXCEPTION
   WHEN OTHERS
   THEN
      RETURN NULL;
END GET_CUSTOMER_PROSPECT_INFO;

/*
Name: GET_SALES_REP_PHONE
Date: 08/21/2017
Author: Christian Baez, HD Supply
Description: Oracle Function to return sales rep phone number
Parameters: 
    Input: Customer Account ID
    Output: Sales Rep Phone Number
*/
FUNCTION GET_SALESREP_PHONE (p_cust_account_id   IN NUMBER) RETURN VARCHAR2
     AS
     l_source_phone  VARCHAR2(2000);
     CURSOR cur_salesrep_phone (p_cust_account_id IN NUMBER)
         IS
     SELECT DISTINCT jrre.source_phone
       FROM hz_cust_acct_sites_all     hcas
          , hz_cust_site_uses_all      hcsu
          , jtf_rs_salesreps           jrs
          , jtf_rs_resource_extns      jrre
      WHERE 1 = 1
	    AND hcsu.ATTRIBUTE1          = 'MSTR' --revision 3.0
        AND hcas.cust_account_id     = p_cust_account_id
        AND hcas.cust_acct_site_id   = hcsu.cust_acct_site_id
        AND hcsu.primary_salesrep_id = jrs.salesrep_id
        AND jrs.resource_id          = jrre.resource_id
        AND jrs.org_id               = hcas.org_id
        AND jrs.salesrep_id          != -3 -- No Sales Credit
        AND hcas.status              = 'A'
        AND hcsu.status              = 'A'
        AND jrre.source_phone       IS NOT NULL
        AND SYSDATE BETWEEN jrre.start_Date_active and NVL(jrre.end_date_active, SYSDATE+1)
        ;

       l_sec                     VARCHAR2 (200);
   BEGIN

     l_sec := 'Derive Salesrep Phones';
     FOR rec IN cur_salesrep_phone(p_cust_account_id) LOOP
       IF l_source_phone IS NULL THEN
          l_source_phone := rec.source_phone;
       ELSE
          l_source_phone := rec.source_phone||','||l_source_phone;
       END IF;
     END LOOP;

     RETURN l_source_phone;

   EXCEPTION
   WHEN OTHERS THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_CHECKITAPP_PKG.GET_SALES_REP_PHONE',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (SQLERRM, 2000),
            p_error_desc          => 'Error in deriving Salesrep phones ',
            p_distribution_list   => 'WC-ITDEVALERTS-U1@HDSupply.com',
            p_module              => 'XXWC');

     RETURN NULL;
   END get_salesrep_phone;
   
   /*
  Name: GET_SALES_REP_STATES_ACTIVE
  Date: 08/21/2017
  Author: Christian Baez, HD Supply
  Description: Oracle Function to return sales rep states that active
  Parameters: 
      Input: Customer Account ID
      Output: Sales Rep State
  */
  
  FUNCTION GET_SALES_REP_STATES_ACTIVE (p_customer_name IN VARCHAR2) RETURN VARCHAR2
     AS
     l_state  VARCHAR2(2000);
     CURSOR cur_salesrep_state (p_customer_name IN VARCHAR2)
         IS
     SELECT DISTINCT (CASE
                    WHEN LENGTH (NVL (LOC.state, LOC.province)) > 2 THEN NULL
                        ELSE NVL (LOC.state, LOC.province)
                    END) Site_State
       FROM apps.HZ_CUST_ACCT_SITES_ALL     ACCT_SITE,
            apps.HZ_PARTY_SITES             PARTY_SITE,
            apps.HZ_LOCATIONS               LOC,
            apps.HZ_CUST_SITE_USES_ALL       SITE,
            apps.HZ_PARTIES           PARTY,
            apps.HZ_CUST_ACCOUNTS      CUST_ACCT
      WHERE 1 = 1
        AND   SITE.SITE_USE_CODE         = 'BILL_TO'
        AND   SITE.CUST_ACCT_SITE_ID     = ACCT_SITE.CUST_ACCT_SITE_ID
        AND   ACCT_SITE.PARTY_SITE_ID    = PARTY_SITE.PARTY_SITE_ID
        AND   PARTY_SITE.LOCATION_ID     = LOC.LOCATION_ID
        AND   acct_site.status='A'
        AND   ACCT_SITE.CUST_ACCOUNT_ID=CUST_ACCT.CUST_ACCOUNT_ID
        AND   CUST_ACCT.PARTY_ID=PARTY.PARTY_ID
        AND   CUST_ACCT.status='A'
        AND   UPPER(PARTY.PARTY_NAME)     LIKE UPPER(p_customer_name)||'%'
        ;

       l_sec                     VARCHAR2 (200);

   BEGIN

     l_sec := 'Derive Salesrep States';
     FOR rec IN cur_salesrep_state(p_customer_name) LOOP
       IF l_state IS NULL THEN
          l_state := rec.site_state;
       ELSE
          l_state := rec.site_state||';'||l_state;
       END IF;
     END LOOP;

     RETURN l_state;

   EXCEPTION
   WHEN OTHERS THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_CHECKITAPP_PKG.GET_SALES_REP_STATES_ACTIVE',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (SQLERRM, 2000),
            p_error_desc          => 'Error in deriving Salesrep states ',
            p_distribution_list   => 'WC-ITDEVALERTS-U1@HDSupply.com',
            p_module              => 'XXWC');

     RETURN NULL;
   END GET_SALES_REP_STATES_ACTIVE;
  

    /****************************************************************************************************************
    FUNCTION Name: GET_MASTER_ACCT_SALES_REP

      PURPOSE:   To derive Master Acount Level Salesrep Names associated to P_PARTY_ID (Customer)

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------
      1.0        08/24/2017  Christian Baez           Created function to retrieve master acct salesrep
   ****************************************************************************************************************/
   FUNCTION get_master_acct_sales_rep (p_cust_account_id   IN NUMBER) RETURN VARCHAR2
     AS
     l_sales_rep_name  VARCHAR2(32767);

     CURSOR cur_salesrep (p_customer_id IN NUMBER)
         IS
     SELECT DISTINCT REPLACE(jrre.source_name, ',', '')  sales_rep_name
       FROM apps.hz_cust_acct_sites_all     hcas
          , apps.hz_cust_site_uses_all      hcsu
          , apps.jtf_rs_salesreps           jrs
          , apps.jtf_rs_resource_extns      jrre
      WHERE 1 = 1
        AND hcsu.ATTRIBUTE1          = 'MSTR'
        AND hcas.cust_account_id     = p_customer_id
        AND hcas.cust_acct_site_id   = hcsu.cust_acct_site_id
        AND hcsu.primary_salesrep_id = jrs.salesrep_id
        AND hcas.status              = 'A'
        AND hcsu.status              = 'A'
        AND jrs.resource_id          = jrre.resource_id
        AND jrs.org_id               = hcas.org_id
        AND jrs.salesrep_id          != -3 -- No Sales Credit
        AND SYSDATE BETWEEN jrre.start_Date_active and NVL(jrre.end_date_active, SYSDATE+1)
        ;
       l_sec                     VARCHAR2 (200);
   BEGIN

     l_sec := 'Derive Master Acct Salesrep Names';

     FOR rec IN cur_salesrep(p_cust_account_id) LOOP
       IF l_sales_rep_name IS NULL THEN
          l_sales_rep_name := rec.sales_rep_name;
       ELSE
           IF rec.sales_rep_name IS NOT NULL THEN
              l_sales_rep_name := rec.sales_rep_name||' ; '||l_sales_rep_name;
           END IF;
       END IF;
     END LOOP;
     RETURN l_sales_rep_name;

   EXCEPTION
   WHEN OTHERS THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_CHECKITAPP_PKG.GET_MASTER_ACCT_SALES_REP',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (SQLERRM, 2000),
            p_error_desc          => 'Error in deriving Master Account Salesrep Names ',
            p_distribution_list   => g_dflt_email,
            p_module              => 'XXWC');

     RETURN NULL;
   END get_master_acct_sales_rep;
   
   /****************************************************************************************************************
    FUNCTION Name: GET_ACCT_SALES_REP

      PURPOSE:   To derive Acount Level Salesrep Names associated to P_PARTY_ID (Customer)

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------
      1.0        05/01/2018  Rakesh Patel            Created function to retrieve acct salesrep
   ****************************************************************************************************************/
   FUNCTION get_acct_sales_rep (p_cust_account_id   IN NUMBER) RETURN VARCHAR2
     AS
     l_sales_rep_name  VARCHAR2(32767);

     CURSOR cur_salesrep (p_customer_id IN NUMBER)
         IS
     SELECT DISTINCT REPLACE(jrre.source_name, ',', '')  sales_rep_name
       FROM apps.hz_cust_acct_sites_all     hcas
          , apps.hz_cust_site_uses_all      hcsu
          , apps.jtf_rs_salesreps           jrs
          , apps.jtf_rs_resource_extns      jrre
      WHERE 1 = 1
        AND hcas.cust_account_id     = p_customer_id
        AND hcas.cust_acct_site_id   = hcsu.cust_acct_site_id
        AND hcsu.primary_salesrep_id = jrs.salesrep_id
        AND hcas.status              = 'A'
        AND hcsu.status              = 'A'
        AND jrs.resource_id          = jrre.resource_id
        AND jrs.org_id               = hcas.org_id
        AND jrs.salesrep_id          != -3 -- No Sales Credit
        AND SYSDATE BETWEEN jrre.start_Date_active and NVL(jrre.end_date_active, SYSDATE+1)
        AND jrre.source_name IS NOT NULL;
       l_sec                     VARCHAR2 (200);
   BEGIN

     l_sec := 'Derive Acct Salesrep Names';

     FOR rec IN cur_salesrep(p_cust_account_id) LOOP
       IF l_sales_rep_name IS NULL THEN
          l_sales_rep_name := rec.sales_rep_name;
       ELSE
           IF rec.sales_rep_name IS NOT NULL THEN
              l_sales_rep_name := rec.sales_rep_name||' ; '||l_sales_rep_name;
           END IF;
       END IF;
     END LOOP;
     RETURN l_sales_rep_name;

   EXCEPTION
   WHEN OTHERS THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_CHECKITAPP_PKG.GET_ACCT_SALES_REP',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (SQLERRM, 2000),
            p_error_desc          => 'Error in deriving Account Salesrep Names ',
            p_distribution_list   => g_dflt_email,
            p_module              => 'XXWC');

     RETURN NULL;
   END get_acct_sales_rep;
   --Rev# 7.0 < End
     
END XXWC_CHECKITAPP_PKG;
/