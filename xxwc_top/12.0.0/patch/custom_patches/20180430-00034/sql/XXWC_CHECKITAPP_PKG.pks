CREATE OR REPLACE PACKAGE APPS.XXWC_CHECKITAPP_PKG
AS
/*************************************************************************
     $Header XXWC_CHECKITAPP_PKG $
     Module Name: XXWC_CHECKITAPP_PKG.pkb

     PURPOSE:   Handwrite Project	
					   
     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        11/02/2017  Niraj K Ranjan          TMS#20171102-00074-Mobile apps new plsql procedures for WOC 
     4.0        02/16/2018  Niraj K Ranjan          TMS#20180205-00161   @CheckIT Ability to subscribe a customer for Order Status Updates - OSU through CheckIT app
     5.0        03/12/2018  Rakesh P.               TMS#20180302-00245-Replacement Cost Enhancement  
     6.0        04/12/2018  Rakesh P.	            TMS#20180411-00083-Disclaimer needs to be added to Checkit for Custom Quote
     7.0        05/02/2018  Rakesh P.               TMS#TMS#20180430-00034-Modify CheckIT customer prospect procedure to better accommodate cash customers 
**************************************************************************/
    -- Record type to take customer info through mobile app as parameter
    TYPE CustRecTyp IS RECORD ( organization_name HZ_PARTIES.PARTY_NAME%TYPE
                               ,account_name      HZ_CUST_ACCOUNTS.ACCOUNT_NAME%TYPE
                               --,account_type      VARCHAR2(10) := 'CASH'
							   ,party_email_id    HZ_PARTIES.EMAIL_ADDRESS%TYPE
							   ,party_phone_area_code HZ_PARTIES.PRIMARY_PHONE_AREA_CODE%TYPE
							   ,party_phone_number HZ_PARTIES.PRIMARY_PHONE_NUMBER%TYPE
							   ,party_phone_extension HZ_PARTIES.PRIMARY_PHONE_EXTENSION%TYPE
							   ,created_by_module HZ_CUST_ACCOUNTS.CREATED_BY_MODULE%TYPE
                               ,country           HZ_LOCATIONS.COUNTRY%TYPE
							   ,address1          HZ_LOCATIONS.ADDRESS1%TYPE
							   ,address2          HZ_LOCATIONS.ADDRESS2%TYPE
							   ,address3          HZ_LOCATIONS.ADDRESS3%TYPE
							   ,address4          HZ_LOCATIONS.ADDRESS4%TYPE
                               ,city              HZ_LOCATIONS.CITY%TYPE
                               ,postal_code       HZ_LOCATIONS.POSTAL_CODE%TYPE
                               ,state             HZ_LOCATIONS.STATE%TYPE
							   ,county            HZ_LOCATIONS.county%TYPE
							   ,sold_to_flag      VARCHAR2(1) := 'N'
							   ,ship_to_flag      VARCHAR2(1) := 'Y'
							   ,bill_to_flag      VARCHAR2(1) := 'Y'
							   ,deliver_to_flag   VARCHAR2(1) := 'N'
							   ,location          HZ_PARTY_SITES.PARTY_SITE_NAME%TYPE
                               ,first_name        HZ_PARTIES.PERSON_FIRST_NAME%TYPE
							   ,last_name         HZ_PARTIES.PERSON_LAST_NAME%TYPE
                               ,title             HZ_PARTIES.PERSON_TITLE%TYPE
                               ,email             HZ_CONTACT_POINTS.EMAIL_ADDRESS%TYPE
                               ,contact_num       HZ_CONTACT_POINTS.PHONE_NUMBER%TYPE
                               ,fax_num           HZ_CONTACT_POINTS.TELEX_NUMBER%TYPE
							   ,ntid              FND_USER.USER_NAME%TYPE);
   
   /*************************************************************************
        $Header clone_part_prc $
        Module Name: clone_part_prc
   
        PURPOSE:   Clone item from one organization to other organization	
   					   
        REVISIONS:
        Ver        Date        Author                     Description
        ---------  ----------  ---------------         -------------------------
        1.0        11/02/2017  Niraj K Ranjan          TMS#20171102-00074-Mobile apps new plsql procedures for WOC                 
   **************************************************************************/
   PROCEDURE clone_part_prc (    p_transaction_id      IN   NUMBER DEFAULT NULL
                              ,p_inventory_item_id   IN   NUMBER
                              ,p_to_org_id           IN   NUMBER
                              ,p_from_org_id         IN   NUMBER
                              ,p_return_status       OUT  VARCHAR2
                              ,p_return_message      OUT  VARCHAR2);
   
   /*************************************************************************
     $Header create_customer $
     Module Name: create_customer

     PURPOSE:   create customer account,site and contacts	
					   
     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        11/14/2017  Niraj K Ranjan          TMS#20171102-00074-Mobile apps new plsql procedures for WOC             
   **************************************************************************/
   PROCEDURE create_customer ( p_cust_rec             IN   CustRecTyp
                              ,p_customer_number      OUT  VARCHAR2
							  ,p_cust_account_id      OUT  NUMBER
							  ,p_bill_to_site_use_id  OUT  NUMBER
							  ,p_ship_to_site_use_id  OUT  NUMBER
							  ,p_cust_account_role_id OUT  NUMBER
                              ,p_return_status        OUT  VARCHAR2
                              ,p_return_message       OUT  VARCHAR2);
							  
   /*************************************************************************
     $Header create_wso_order $
     Module Name: create_wso_order

     PURPOSE:   create WSO order	
					   
     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        11/22/2017  Niraj K Ranjan          TMS#20171102-00074-Mobile apps new plsql procedures for WOC             
   **************************************************************************/
   PROCEDURE create_woc_order ( p_quote_number         IN   NUMBER
                               ,p_transfer_branch      IN   VARCHAR2
							   ,p_ntid                 IN   VARCHAR2  
							   ,p_ord_payment_tbl      IN   XXWC.XXWC_ORDER_PAYMENTS_TBL
							   ,p_order_number         OUT  NUMBER
							   ,p_header_id            OUT  NUMBER
                               ,p_return_status        OUT  VARCHAR2
                               ,p_return_message       OUT  VARCHAR2);
   
   /*************************************************************************
      PROCEDURE Name: XXWC_APPLY_PAYMENT_TO_SO

      PURPOSE:   This procedure will apply pre-payment to SO

      REVISIONS:
      Ver        Date          Author             Description
      ---------  ------------  ------------       ------------------
      1.0        10-Nov-2017   Niraj K Ranjan     TMS#20171102-00074-Mobile apps new plsql procedures for WOC
   ****************************************************************************/                               
    PROCEDURE XXWC_APPLY_PAYMENT_TO_SO
   (
     p_order_header_rec     IN  xxwc.xxwc_order_header_rec
    ,p_order_payments_tbl   IN  XXWC.XXWC_ORDER_PAYMENTS_TBL
    ,o_status               OUT VARCHAR2
    ,o_message              OUT VARCHAR2    
   );
   
   /*************************************************************************
      PROCEDURE Name: get_customer_info_woc

      PURPOSE:   This procedure will be called by mobile app to search customer

      REVISIONS:
      Ver        Date          Author             Description
      ---------  ------------  ------------       ------------------
      1.0        11/30/2017    Niraj K Ranjan     TMS#20171102-00074-Mobile apps new plsql procedures for WOC    
   ****************************************************************************/
   FUNCTION get_customer_info_woc( p_cust_account_num            IN VARCHAR2
                                  ,p_cust_name_search_string     IN VARCHAR2
                                  ) 
   RETURN sys_refcursor ;
   
   /*************************************************************************
      PROCEDURE Name: get_customer_info_woc

      PURPOSE:   This procedure will be called by mobile app to search customer

      REVISIONS:
      Ver        Date          Author             Description
      ---------  ------------  ------------       ------------------
      1.0        11/30/2017    Niraj K Ranjan     TMS#20171102-00074-Mobile apps new plsql procedures for WOC    
   ****************************************************************************/
	FUNCTION get_jobsite_info_woc( p_cust_account_id            IN NUMBER
                                  ,p_jobsite_name_search_string IN VARCHAR2
                                  ) 
    RETURN sys_refcursor;
	
	/*************************************************************************
      PROCEDURE Name: get_woc_order_info

      PURPOSE:   This procedure will be called by mobile app to search order

      REVISIONS:
      Ver        Date          Author             Description
      ---------  ------------  ------------       ------------------
      1.0        12/07/2017    Niraj K Ranjan     TMS#20171102-00074-Mobile apps new plsql procedures for WOC    
   ****************************************************************************/
   FUNCTION get_woc_order_info(  --p_ntid         IN VARCHAR2
                                p_order_number IN NUMBER
                              ) 
   RETURN sys_refcursor;
   
   /*************************************************************************
      PROCEDURE Name: get_order_line_info

      PURPOSE:   This procedure will be called by mobile app to search order line

      REVISIONS:
      Ver        Date          Author             Description
      ---------  ------------  ------------       ------------------
      1.0        12/07/2017    Niraj K Ranjan     TMS#20171102-00074-Mobile apps new plsql procedures for WOC    
   ****************************************************************************/
   FUNCTION get_woc_order_line_info(  p_header_id     IN NUMBER) 
   RETURN sys_refcursor;
   /*************************************************************************
      PROCEDURE Name: CREATE_WOC_ITEM

      PURPOSE:   This procedure will be called by mobile app to create item

      REVISIONS:
      Ver        Date          Author             Description
      ---------  ------------  ------------       ------------------
      1.0        12/07/2017    Niraj K Ranjan     TMS#20171102-00074-Mobile apps new plsql procedures for WOC    
   ****************************************************************************/
   PROCEDURE CREATE_WOC_ITEM (
      p_Parts_On_Fly_Rec   IN OUT NOCOPY xxwc_Parts_On_Fly_Rec,
	  p_NTID                   IN VARCHAR2, 
      x_Return_Status         OUT NOCOPY VARCHAR2,
      x_Msg_Data              OUT NOCOPY VARCHAR2,
      x_Msg_Count             OUT NOCOPY NUMBER,
      x_part_number           OUT NOCOPY VARCHAR2,
      x_inventory_item_id     OUT NOCOPY NUMBER,
      x_list_price            OUT NOCOPY NUMBER,
      x_primary_uom_code      OUT NOCOPY VARCHAR2
    );
	/*************************************************************************
      PROCEDURE Name: GET_PRODUCT_INFO

      PURPOSE:   This procedure will be called by mobile app to get item info

      REVISIONS:
      Ver        Date          Author             Description
      ---------  ------------  ------------       ------------------
      1.0        03/01/2017    Niraj K Ranjan     TMS#20171102-00074-Mobile apps new plsql procedures for WOC    
   ****************************************************************************/
   PROCEDURE GET_PRODUCT_INFO(   p_branch_id   IN NUMBER
                             ,p_customer_id IN NUMBER
                             ,p_jobsiteid   IN NUMBER
                             ,p_item_search IN VARCHAR2
                             ,p_filter      IN VARCHAR2
                             ,p_Sort        IN VARCHAR2
                             ,p_warhouse_num_tbl IN  xxwc.xxwc_warhouse_num_tbl DEFAULT NULL --Rev 2.2
                             ,p_filter_stock IN VARCHAR2 DEFAULT NULL --Rev 2.5
                             ,o_cur_output  OUT SYS_REFCURSOR -- return sys_refcursor
                             );
   /*************************************************************************
   *   Procedure : xxwc_log_debug_info
   *
   *   PURPOSE:   This procedure is called to log debug info.
   *
   * REVISIONS:
   *  Ver        Date         Author            Description
   *  ---------  ----------   ---------------   -------------------------
   *  1.0       01/16/2017   Niraj K Ranjan   TMS#20171102-00074-Mobile apps new plsql procedures for WOC
   * ************************************************************************/
   PROCEDURE xxwc_log_debug_info(P_API_NAME         IN VARCHAR2,
                                 P_BRANCH_ID        IN VARCHAR2 DEFAULT NULL,
                                 P_ORDER_HEADER_ID  IN NUMBER DEFAULT NULL,
								 P_ORDER_NUMBER     IN NUMBER DEFAULT NULL,
								 P_ITEM_ID          IN NUMBER DEFAULT NULL,
								 P_ITEM_NUMBER      IN VARCHAR2 DEFAULT NULL,
								 P_JOB_SITE_ID      IN NUMBER DEFAULT NULL,
								 P_CUSTOMER_ID      IN NUMBER DEFAULT NULL,
								 P_CUSTOMER_NUMBER  IN VARCHAR2 DEFAULT NULL,
								 P_SEARCH_STRING    IN VARCHAR2 DEFAULT NULL,
								 P_DEBUG_TEXT       IN VARCHAR2 DEFAULT NULL,
								 P_USER_ID          IN NUMBER        
								 );
   /*************************************************************************
   *   Procedure : CREATE_JOB_SITE
   *
   *   PURPOSE:   This procedure is called to log debug info.
   *
   * REVISIONS:
   *  Ver        Date         Author            Description
   *  ---------  ----------   ---------------   -------------------------
   *  1.0       01/16/2017   Niraj K Ranjan   TMS#20171102-00074-Mobile apps new plsql procedures for WOC
   * ************************************************************************/ 
  PROCEDURE CREATE_JOB_SITE
   (
     p_xxwc_address_rec     IN  xxwc.xxwc_address_rec
    ,p_created_by_module    IN  VARCHAR2 DEFAULT NULL
    ,o_status               OUT VARCHAR2
    ,o_ship_to_site_use_id  OUT NUMBER
    ,o_bill_to_site_use_id  OUT NUMBER
    ,o_message              OUT VARCHAR2
   );
   /*************************************************************************
      PROCEDURE Name: get_b2b_setup_info

      PURPOSE:   This procedure will be called by CheckIT mobile app to get info about B2B setup of a customer.

      REVISIONS:
      Ver        Date          Author             Description
      ---------  ------------  ------------       ------------------
      4.0        02/16/2018  Niraj K Ranjan          TMS#20180205-00161   @CheckIT Ability to subscribe a customer for Order Status Updates - OSU through CheckIT app
   ****************************************************************************/
    FUNCTION get_b2b_setup_info( p_cust_account_id     IN NUMBER
                                ,p_cust_site_use_id    IN NUMBER
                               ) return sys_refcursor;
    /*************************************************************************
      PROCEDURE Name: Set_b2b_account

      PURPOSE:   This procedure will be called by CheckIT mobile app to set account flags/email info.

      REVISIONS:
      Ver        Date          Author             Description
      ---------  ------------  ------------       ------------------
      4.0        02/16/2018  Niraj K Ranjan          TMS#20180205-00161   @CheckIT Ability to subscribe a customer for Order Status Updates - OSU through CheckIT app 
   ****************************************************************************/
   PROCEDURE Set_b2b_account( p_b2b_config_tbl_typ  IN   XXWC.XXWC_B2B_CONFIG_TBL%ROWTYPE
                             ,p_ntid                IN   VARCHAR2
                             ,p_return_status       OUT  VARCHAR2
                             ,p_return_message      OUT  VARCHAR2);
 
    --Rev# 5.0 < Start
    /*************************************************************************
      PROCEDURE Name: CREATE_CUSTOM_QUOTE

      PURPOSE:   This procedure will be called by CheckIT mobile app to create custom quote

      REVISIONS:
      Ver        Date          Author             Description
      ---------  ------------  ------------       ------------------
      5.0        03/12/2018  Rakesh P.            TMS#20180302-00245-Replacement Cost Enhancement   
   ****************************************************************************/
	PROCEDURE CREATE_CUSTOM_QUOTE
    (
     p_order_header_rec     IN  xxwc.xxwc_order_header_rec
    ,p_order_lines_tbl      IN  xxwc.xxwc_order_lines_tbl
    ,o_status               OUT VARCHAR2
    ,o_quote_number         OUT NUMBER
    ,o_quote_total          OUT NUMBER  
    ,o_message              OUT VARCHAR2    
	,p_debug_mode           IN  VARCHAR2  DEFAULT NULL   --E/T/NULL 
    );
    
    FUNCTION GET_UNITWGT_TOTAL_WC_QUOTE(  p_quote_number     IN NUMBER
                                 ,p_ship_from_org_id IN NUMBER     
                                )  RETURN NUMBER;
                                
    FUNCTION GET_WC_QUOTE_TOTAL(  p_quote_number     IN NUMBER
                                )  RETURN NUMBER;    
                            
    FUNCTION GET_CUSTOM_QUOTE_INFO(  p_ntid         IN VARCHAR2
                                    ,p_quote_number IN NUMBER DEFAULT NULL
                            ) return sys_refcursor;
     
    FUNCTION GET_CUSTOM_QUOTE_LINE_INFO(  p_quote_number IN NUMBER
                           ) return sys_refcursor;
    --Rev# 5.0 < End                     
						  
    --Rev# 6.0 < Start
   FUNCTION PRINT_QUOTE_DISCLAIMER (  p_quote_number     IN NUMBER 
                                     ,p_ship_from_org_id IN NUMBER
                                    )
   RETURN VARCHAR2;
    --Rev# 6.0 < End   
    
   --Rev# 7.0 > Start
   FUNCTION GET_CUSTOMER_PROSPECT_INFO ( p_customer_name  IN VARCHAR2) return sys_refcursor;
   
   FUNCTION GET_SALESREP_PHONE (p_cust_account_id   IN NUMBER) RETURN VARCHAR2;
   
   FUNCTION GET_SALES_REP_STATES_ACTIVE (p_customer_name IN VARCHAR2) RETURN VARCHAR2;

   FUNCTION get_master_acct_sales_rep (p_cust_account_id   IN NUMBER) RETURN VARCHAR2;
   
   FUNCTION get_acct_sales_rep (p_cust_account_id   IN NUMBER) RETURN VARCHAR2;
   --Rev# 7.0 < End
                        
END XXWC_CHECKITAPP_PKG;
/