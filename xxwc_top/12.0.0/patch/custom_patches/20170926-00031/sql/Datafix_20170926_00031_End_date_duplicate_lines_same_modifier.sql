/******************************************************************************
   NAME:       TMS_20170926_00031_CSP_DATA_FIX.sql
   PURPOSE:    Data fix script to end date duplicate lines for the same modifier.
   
   REVISIONS:
   Ver        Date        Author               Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        27/09/2017  Niraj K Ranjan   Initial Version TMS#20170926-00031   Data fix script to end date duplicate lines for the same modifier
******************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
   l_total_rec_updated number;
   CURSOR cr_list_line
   IS
      SELECT min(LIST_LINE_ID) list_line_id
	        ,msi.segment1
			,b.agreement_id
            ,b.agreement_line_id
            ,a.attribute2
      FROM apps.qp_modifier_summary_v a,
           xxwc.xxwc_om_contract_pricing_lines b,
           apps.mtl_system_items  msi
      WHERE 1=1 
      AND msi.inventory_item_id= a.product_attr_val
      AND msi.organization_id = 222
      AND a.attribute2 = b.agreement_line_id
      AND b.end_date  IS NULL 
      AND a.end_date_active IS NULL
      AND a.excluder_flag = 'N' 
      GROUP BY msi.segment1
              ,b.agreement_id
              ,b.agreement_line_id
              ,a.attribute2
      HAVING COUNT(1) > 1;
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20170926-00031   , Start Update');
   DBMS_OUTPUT.put_line ('Following records has been updated:');
   DBMS_OUTPUT.put_line ('LIST_LINE_ID|ITEM|AGREEMENT_ID|AGREEMENT_LINE_ID|LIST_LINE_ATTRIBUTE2');
   l_total_rec_updated := 0;
   FOR rec IN cr_list_line
   LOOP
      UPDATE qp_list_lines 
      SET end_date_active = sysdate-1,
          last_update_date = sysdate,
          last_updated_by = -1       
       WHERE list_line_id = rec.list_line_id;
      DBMS_OUTPUT.put_line (rec.list_line_id||'|'||rec.segment1||'|'||rec.agreement_id||'|'||rec.agreement_line_id||'|'||rec.attribute2);
	  l_total_rec_updated := l_total_rec_updated + 1;
   END LOOP;
   DBMS_OUTPUT.put_line ('TMS: 20170926-00031 Total Records updated : '|| l_total_rec_updated);

   COMMIT;
   DBMS_OUTPUT.put_line ('TMS: 20170926-00031    , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20170926-00031 , Errors : ' || SQLERRM);
END;
/
