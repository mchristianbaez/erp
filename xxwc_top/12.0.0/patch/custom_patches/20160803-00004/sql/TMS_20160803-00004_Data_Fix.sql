--SET SERVEROUTPUT ON SIZE 1000000

/**************************************************************************************************************************
-- TMS#20160803-00004   EBSPRDDaily Oracle Interface Errors Report - AUG
-- Creatd by : Niraj K Ranjan
-- Date: 25-Aug-2016
-- Purpose:  Data fix script to progress lines which are Unprocessed shipping
****************************************************************************************************************************
Previously executed script
BEGIN

   DBMS_OUTPUT.put_line ('UPDATE apps.wsh_delivery_details');

   UPDATE apps.wsh_delivery_details
      SET released_status = 'D',
          src_requested_quantity = 0,
          requested_quantity = 0,
          shipped_quantity = 0,
          cycle_count_quantity = 0,
          cancelled_quantity = 0,
          subinventory = NULL,
          locator_id = NULL,
          lot_number = NULL,
          revision = NULL,
          inv_interfaced_flag = 'X',
          oe_interfaced_flag = 'X'
    WHERE delivery_detail_id IN (17735983, 17677781);

   DBMS_OUTPUT.put_line ('Number of Rows Updated :' || SQL%ROWCOUNT);

   UPDATE apps.wsh_delivery_assignments
      SET delivery_id = NULL, parent_delivery_detail_id = NULL
    WHERE delivery_detail_id IN (17735983, 17677781);

   DBMS_OUTPUT.put_line ('Number of Rows Updated :' || SQL%ROWCOUNT);

   COMMIT;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.PUT_LINE ('Error Occured ' || SUBSTR (SQLERRM, 200));
END;
/ */
-- New Script executed on 21-Sep-2016

/*************************************************************************
  $Header TMS_20160803-00004_DAILY_ORACLE_INTERFACE_ERRORS.sql $
  Module Name: TMS_20160803-00004  Data Fix script 

  PURPOSE: Data fix script for aily Oracle Interface Errors Report 

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        21-SEP-2016  Pattabhi Avula        TMS#20160803-00004

************************************************************************** 
SET SERVEROUTPUT ON SIZE 1000000;
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('20160803-00004    , Before Delivery Detail lines Update');
UPDATE apps.wsh_delivery_details
SET released_status = 'D',
src_requested_quantity = 0,
requested_quantity = 0,
shipped_quantity = 0,
cycle_count_quantity = 0,
cancelled_quantity = 0,
subinventory = null,
locator_id = null,
lot_number = null,
revision = null,
inv_interfaced_flag = 'X',
oe_interfaced_flag = 'X'
WHERE delivery_detail_id in (17176786,17245951,16864071,16864072,17258040,17335581,17385565,17382447,17418726,17418728,17607300,17607301,17607302,17590772);

DBMS_OUTPUT.put_line (
         'TMS: 20160803-00004  Delilvery detail lines updated (Expected:14): '
      || SQL%ROWCOUNT);

   COMMIT;
--14 row expected to be updated

   DBMS_OUTPUT.put_line ('20160803-00004    , Before Delivery assignments lines Update');
   
update apps.wsh_delivery_assignments
set delivery_id = null,
    parent_delivery_detail_id = null
WHERE delivery_detail_id in (17176786,17245951,16864071,16864072,17258040,17335581,17385565,17382447,17418726,17418728,17607300,17607301,17607302,17590772);
DBMS_OUTPUT.put_line (
         'TMS: 20160803-00004  Delilvery detail assignments updated (Expected:14): '
      || SQL%ROWCOUNT);
COMMIT;
   DBMS_OUTPUT.put_line ('20160803-00004    , Before Delivery assignments lines Update');
   update 
 		apps.wsh_delivery_details
 		set OE_INTERFACED_FLAG ='Y'
 		where delivery_detail_id in (17268850, 17329764);
		DBMS_OUTPUT.put_line (
         'TMS: 20160803-00004  Delilvery detail assignments updated (Expected:2): '
      || SQL%ROWCOUNT);
COMMIT;
--2 row expected to be updated 

DBMS_OUTPUT.put_line ('TMS: 20160803-00004    , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS:  20160803-00004 , Errors : ' || SQLERRM);
END; */
-- One more new Script
/*************************************************************************
  $Header TMS_20160803-00004_DAILY_ORACLE_INTERFACE_ERRORS.sql $
  Module Name: TMS_20160803-00004  Data Fix script 

  PURPOSE: Data fix script for aily Oracle Interface Errors Report 

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        21-SEP-2016  Pattabhi Avula        TMS#20160803-00004

**************************************************************************
SET SERVEROUTPUT ON SIZE 1000000;
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('20160803-00004    , Before Delivery Detail lines Update');
UPDATE apps.wsh_delivery_details
SET released_status = 'D',
src_requested_quantity = 0,
requested_quantity = 0,
shipped_quantity = 0,
cycle_count_quantity = 0,
cancelled_quantity = 0,
subinventory = null,
locator_id = null,
lot_number = null,
revision = null,
inv_interfaced_flag = 'X',
oe_interfaced_flag = 'X'
WHERE delivery_detail_id in (18091110, 18096865,18131682);
DBMS_OUTPUT.put_line (
         'TMS: 20160803-00004  Delilvery detail assignments updated (Expected:3): '
      || SQL%ROWCOUNT);

--3 row expected to be updated
COMMIT;

 DBMS_OUTPUT.put_line ('20160803-00004    , Before Delivery Detail lines Update');
update apps.wsh_delivery_assignments
set delivery_id = null,
parent_delivery_detail_id = null
where delivery_detail_id in (18091110, 18096865,18131682);
DBMS_OUTPUT.put_line (
         'TMS: 20160803-00004  Delilvery detail assignments updated (Expected:3): '
      || SQL%ROWCOUNT);
--3 row expected to be updated
COMMIT;

  DBMS_OUTPUT.put_line ('20160803-00004    , Before Delivery Detail lines Update');
update apps.oe_order_lines_all
set flow_status_code='CANCELLED',
cancelled_flag='Y'
where line_id=79764305
and headeR_id=48805357;
DBMS_OUTPUT.put_line (
         'TMS: 20160803-00004  Delilvery detail assignments updated (Expected:1): '
      || SQL%ROWCOUNT);
COMMIT;
--1 row expected to be updated . 

DBMS_OUTPUT.put_line ('TMS: 20160803-00004    , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS:  20160803-00004 , Errors : ' || SQLERRM);
END; */
-- One more new Script on 23092016
/*************************************************************************
  $Header TMS_20160803-00004_DAILY_ORACLE_INTERFACE_ERRORS.sql $
  Module Name: TMS_20160803-00004  Data Fix script 

  PURPOSE: Data fix script for aily Oracle Interface Errors Report 

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        23-SEP-2016  Pattabhi Avula        TMS#20160803-00004

**************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000;
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('20160803-00004    , Before Delivery Detail lines Update');
UPDATE apps.wsh_delivery_details
SET released_status = 'D',
src_requested_quantity = 0,
requested_quantity = 0,
shipped_quantity = 0,
cycle_count_quantity = 0,
cancelled_quantity = 0,
subinventory = null,
locator_id = null,
lot_number = null,
revision = null,
inv_interfaced_flag = 'X',
oe_interfaced_flag = 'X'
WHERE delivery_detail_id =18121646;
DBMS_OUTPUT.put_line (
         'TMS: 20160803-00004  Delilvery detail assignments updated (Expected:1): '
      || SQL%ROWCOUNT);

update apps.wsh_delivery_assignments
set delivery_id = null,
parent_delivery_detail_id = null
where delivery_detail_id = 18121646;
DBMS_OUTPUT.put_line (
         'TMS: 20160803-00004  Delilvery detail assignments updated (Expected:1): '
      || SQL%ROWCOUNT);
	  
update apps.oe_order_lines_all
set flow_status_code='CANCELLED',
cancelled_flag='Y'
where line_id=79864460
and headeR_id=48866696;  
	  
DBMS_OUTPUT.put_line ('Number of Rows Updated :' || SQL%ROWCOUNT);
	  
	  
COMMIT;
--1 row expected to be updated . 

DBMS_OUTPUT.put_line ('TMS: 20160803-00004    , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS:  20160803-00004 , Errors : ' || SQLERRM);
END;
/