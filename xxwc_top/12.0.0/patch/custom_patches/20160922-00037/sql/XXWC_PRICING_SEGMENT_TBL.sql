/*************************************************************************
  $Header xxwc_pricing_segment_tbl$
  Module Name: xxwc_pricing_segment_tbl.sql

  PURPOSE:   This package is used for the extensions in Market Pricing

  REVISIONS:
  Ver        Date        Author                     Description
  ---------  ----------  ---------------  -------------------------
  1.0        10/25/2016  Gopi Damuluri    TMS# 20160922-00037 - Matrix Pricing revert to Standard functionality
**************************************************************************/

ALTER TABLE xxwc.xxwc_pricing_segment_tbl ADD SHIP_TO VARCHAR2(240);