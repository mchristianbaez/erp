/*************************************************************************
  $Header TMS_20181024-00001_UnProcessed_Shipping.sql $
  Module Name: TMS_20181024-00001 UnProcessed Shipping Transactions

  PURPOSE: Data Script Fix to Close Delivery ID: 32006865

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        24-OCT-2018  Sundaramoorthy        TMS#20181024-00001

**************************************************************************/ 
SET SERVEROUTPUT ON
SET VERIFY OFF;

BEGIN
 DBMS_OUTPUT.PUT_LINE ('Start Update Script ');

  UPDATE apps.wsh_delivery_details
     SET oe_interfaced_flag='Y'
   WHERE delivery_detail_id=32006865;

 DBMS_OUTPUT.PUT_LINE ('Update count '||SQL%ROWCOUNT);
  COMMIT;
  
  EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
 DBMS_OUTPUT.PUT_LINE ('Inside Exception '|| SUBSTR(SQLERRM,1,2000));	
END;
/