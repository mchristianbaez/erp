/*************************************************************************
  $Header TMS_20160901-00010_Daily_Oracle_Interface_Error_Report.sql $
  Module Name: TMS_220160901-00010  Data Fix script  

  PURPOSE: Data Fix script 

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        16-SEP-2016  Pattabhi Avula        TMS#20160901-00010

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 100000;
SET VERIFY OFF;

declare
BEGIN
   DBMS_OUTPUT.put_line ('Before update');

   /*update apps.oe_order_lines_all
   set flow_status_code='CANCELLED',
   cancelled_flag='Y'
   where line_id=78978559
   and headeR_id=48314528;

--1 row expected to be updated
 
    update apps.wsh_delivery_details
    set OE_INTERFACED_FLAG='Y'
    where delivery_Detail_id = 17574557;
    --1 rows expected to be updated  */
	
	UPDATE apps.wsh_delivery_details
    SET released_status = 'D',
    src_requested_quantity = 0,
    requested_quantity = 0,
    shipped_quantity = 0,
    cycle_count_quantity = 0,
    cancelled_quantity = 0,
    subinventory = null,
    locator_id = null,
    lot_number = null,
    revision = null,
    inv_interfaced_flag = 'X',
    oe_interfaced_flag = 'X'
    WHERE delivery_detail_id  in (17092295,17752148,17859296,17902701,17930574,17993771,18006627);
    
    --7 rows expected to be updated
    
    update apps.wsh_delivery_assignments
    set delivery_id = null,
    parent_delivery_detail_id = null
    where delivery_detail_id in (17092295,17752148,17859296,17902701,17930574,17993771,18006627);
    
    --7 row expected to be updated
    

   DBMS_OUTPUT.put_line ('Records update-' || SQL%ROWCOUNT);
   COMMIT;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Unable to update ' || SQLERRM);
END;
/