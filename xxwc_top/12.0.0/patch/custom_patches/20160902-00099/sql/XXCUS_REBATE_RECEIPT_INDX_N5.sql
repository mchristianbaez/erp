/*
History
======
Version Date       Author                 Notes
======  ========== ====================== =========================================
1.0     08/22/2016 Balaguru Seshadri      TMS 20160902-00099  ESMS 449491  - Rebate Data Reconcilation
*/
CREATE INDEX XXCUS.XXCUS_REBATE_RECEIPT_TBL_N5 ON XXCUS.XXCUS_REBATE_RECEIPT_TBL
(BU_NM, current_week_record)
  PCTFREE    10
  INITRANS   2
  MAXTRANS   255
  STORAGE    (
              BUFFER_POOL      DEFAULT
              FLASH_CACHE      DEFAULT
              CELL_FLASH_CACHE DEFAULT
             )
NOLOGGING
LOCAL (  
  PARTITION ELE
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION UTL_USA
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION CTI
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION WW
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION RR
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION FM
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION WCC
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION WHCP
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION USABB
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION OTHER
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               )
)
NOPARALLEL;