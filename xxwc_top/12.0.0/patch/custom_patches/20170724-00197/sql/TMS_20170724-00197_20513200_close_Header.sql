/*************************************************************************
  $Header TMS_20170724-00197_20028338_close_Header.sql $
  Module Name: TMS_20170724-00197  


  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        29-AUG-2017  Pattabhi Avula         TMS#20170724-00197 

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20170724-00197, Before Update');

UPDATE apps.oe_order_headers_all
   SET flow_status_code='CLOSED',
       open_flag='N'
 WHERE HEADER_ID =42875037;

   DBMS_OUTPUT.put_line (
         'TMS: 20170724-00197  Sales order lines updated (Expected:1): '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20170724-00197    , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20170724-00197 , Errors : ' || SQLERRM);
END;
/