/*
TMS# 20151222-00162 Datafix Manjula Chellappan 22-Dec-2015
*/

SET SERVEROUTPUT ON SIZE 1000000

DECLARE
      l_sec                  VARCHAR2 (100);

      l_apply_hold_flag      NUMBER := 0;
      l_hold                 VARCHAR2 (50)
                                := fnd_profile.VALUE ('XXWC_OM_PG_HOLD');
      l_header_id            NUMBER := 0;
      l_prev_order_amount    NUMBER := 0;

      l_hold_source_rec      OE_HOLDS_PVT.HOLD_SOURCE_REC_TYPE;
      l_return_status        VARCHAR2 (30);
      l_msg_data             VARCHAR2 (4);
      l_msg_count            NUMBER;
      l_error_msg            VARCHAR2 (4000);

      l_order_amount         NUMBER;
      l_rec_count            NUMBER;

      l_add_layout           BOOLEAN;
      l_program_name         VARCHAR2 (30) := 'XXWC_OM_PGR_ERR_REP';
      l_request_id           NUMBER;
      l_exception            EXCEPTION;


      CURSOR cur_order_lines
      IS
          SELECT header_id, order_number
			FROM apps.oe_order_headers_all 
			WHERE header_id IN
			 ( SELECT ooh.header_id
						  FROM apps.oe_order_holds ooh,
							apps.oe_hold_sources ohs
						  WHERE 1                =1
						  AND ohs.Hold_Id        = 1003
						  AND ooh.hold_source_ID =ohs.Hold_Source_id
						  AND NOT EXISTS
							(SELECT 'x'
							FROM apps.oe_order_holds
							WHERE header_id = ooh.header_id
							AND line_id    IS NULL
							)
						  AND TRUNC(ooh.creation_date) = trunc(sysdate)); 
   BEGIN     	  
          
mo_global.set_policy_context('S','162');
fnd_global.apps_initialize(15986, 51045 , 660);		
mo_global.init('ONT');  

 dbms_output.put_line ('Begin');
 
      FOR rec_order_lines IN cur_order_lines
      LOOP
         
		  dbms_output.put_line ('Order Number ' || rec_order_lines.order_number);
            l_hold_source_rec := OE_HOLDS_PVT.G_MISS_HOLD_SOURCE_REC;
            l_hold_source_rec.hold_id := 1003;
            l_hold_source_rec.hold_entity_code := 'O';
            l_hold_source_rec.hold_entity_id := rec_order_lines.header_id;
            l_hold_source_rec.header_id := rec_order_lines.header_id;
            l_hold_source_rec.line_id := NULL;
            l_hold_source_rec.hold_until_date := NULL;
            l_return_status := NULL;
            l_msg_data := NULL;
            l_msg_count := NULL;
			
			  OE_Holds_PUB.Apply_Holds (
                  p_api_version       => 1.0,
                  p_init_msg_list     => FND_API.G_FALSE,
                  p_commit            => FND_API.G_TRUE,
                  p_hold_source_rec   => l_hold_source_rec,
                  x_return_status     => l_return_status,
                  x_msg_count         => l_msg_count,
                  x_msg_data          => l_msg_data);

               IF l_return_status = FND_API.G_RET_STS_SUCCESS
               THEN
                  COMMIT;

                 dbms_output.put_line (
                        'Hold applied on Sales Order : '
                     || rec_order_lines.order_number);
               ELSE
                  l_error_msg := l_msg_data;

                  IF NVL (l_msg_count, 0) > 0
                  THEN
                     FOR i IN 1 .. l_msg_count
                     LOOP
                        l_error_msg :=
                              l_error_msg
                           || ' - '
                           || oe_msg_pub.get (p_msg_index   => i,
                                              p_encoded     => 'F');
                     END LOOP;
                  END IF;

                  ROLLBACK;

                  dbms_output.put_line (
                        'Hold not applied on Sales Order : '
                     || rec_order_lines.order_number);
					 
					 dbms_output.put_line('l_error_msg '||l_error_msg);
					 					 
         END IF;
      END LOOP;
	  dbms_output.put_line ('End');
    END;
/
	  