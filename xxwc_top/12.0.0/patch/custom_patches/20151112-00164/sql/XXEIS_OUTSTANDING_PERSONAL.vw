/*
 Ticket                Version Date           Author                                     Note
 --------------------- ------- -------------- ------------------------------------------ ---------------------------------------------------------------------
                       1.0                                                               History Missing
 TMS 20151112-00164	   1.1     05/18/2016     Balaguru Seshadri                          Add product, location, cost center and natural account segments for the employee. 
*/
CREATE OR REPLACE FORCE VIEW xxeis.xxeis_outstanding_personal
(
   vendor_number
  ,vendor_name
  ,emp_expense_product   -- Ver 1.1
  ,emp_expense_prod_desc -- Ver 1.1
  ,emp_expense_location -- Ver 1.1
  ,emp_expense_loc_desc -- Ver 1.1
  ,emp_expense_cost_center -- Ver 1.1
  ,emp_expense_costcenter_desc -- Ver 1.1
  ,emp_expense_account -- Ver 1.1
  ,emp_expense_acct_desc -- Ver 1.1
  ,invoice_num
  ,invoice_date
  ,invoice_amount
  ,creation_date
  ,amount_remaining
  ,invoice_type_lookup_code
  ,termination_status
  ,termination_date
  ,payment_status_flag
  ,vendor_type_lookup_code
  ,cancelled_date
)
AS
   SELECT vnd.segment1 vendor_number --Ver 1.1
         ,vnd.vendor_name
         ,glcc.segment1 -- Ver 1.1
         ,(
            select /*+ RESULT_CACHE */ description
            from  apps.fnd_flex_values_vl
            where 1 =1
            and flex_value_set_id =1014547
            and flex_value =glcc.segment1
          ) emp_expense_prod_desc  -- Ver 1.1         
         ,glcc.segment2 -- Ver 1.1
         ,(
            select /*+ RESULT_CACHE */ description
            from  apps.fnd_flex_values_vl
            where 1 =1
            and flex_value_set_id =1014548
            and flex_value =glcc.segment2
          ) emp_expense_loc_desc  -- Ver 1.1         
         ,glcc.segment3 -- Ver 1.1
         ,(
            select /*+ RESULT_CACHE */ description
            from  apps.fnd_flex_values_vl
            where 1 =1
            and flex_value_set_id =1014549
            and flex_value =glcc.segment3
          ) emp_expense_costcenter_desc -- Ver 1.1           
         ,glcc.segment4  -- Ver 1.1
         ,(
            select /*+ RESULT_CACHE */ description
            from  apps.fnd_flex_values_vl
            where 1 =1
            and flex_value_set_id =1014550
            and flex_value =glcc.segment4
          ) emp_expense_acct_desc -- Ver 1.1
         ,inv.invoice_num
         ,inv.invoice_date
         ,inv.invoice_amount
         ,inv.creation_date
         ,pmt.amount_remaining
         ,inv.invoice_type_lookup_code
         ,DECODE (NVL (TO_CHAR (pex.inactive_date), 'NONTERM')
                 ,'NONTERM', NULL
                 ,'Terminated')
             termination_status
         ,pex.inactive_date termination_date
         ,inv.payment_status_flag
         ,vnd.vendor_type_lookup_code
         ,inv.cancelled_date
     FROM ap.ap_invoices_all inv
         ,ap.ap_suppliers vnd
         ,apps.per_employees_x pex
         ,ap.ap_payment_schedules_all pmt
		 ,gl.gl_code_combinations glcc --Ver 1.1
    WHERE     inv.vendor_id = vnd.vendor_id
          AND vnd.employee_id = pex.employee_id
          AND inv.invoice_id = pmt.invoice_id
		  AND glcc.code_combination_id(+) =pex.default_code_combination_id --Ver 1.1
;		  
-- 
-- Begin Ver 1.1
COMMENT ON TABLE xxeis.xxeis_outstanding_personal IS 'TMS 20151112-00164 / ESMS 306952'; 
-- End Ver 1.1
          