DROP VIEW APPS.XXWC_B2B_SO_DLVRY_INFO_DTLS_VW;
/

/********************************************************************************
   $Header XXWC_B2B_SO_DLVRY_INFO_DTLS_VW.VW $
   Module Name: XXWC_B2B_SO_DLVRY_INFO_DTLS_VW

   PURPOSE:   View to derive B2B BAM Information.

   REVISIONS:
   Ver        Date        Author                     Description
   ---------  ----------  ---------------         -------------------------
   1.0        08/09/2015  Gopi Damuluri           TMS# 20150615-00088
                                                  Initial Version
********************************************************************************/

CREATE OR REPLACE FORCE VIEW APPS.XXWC_B2B_SO_DLVRY_INFO_DTLS_VW
(
   TRX_TYPE,
   COMMUNICATION_METHOD,
   DOCUMENT_NUM,
   BRANCH,
   TIME_STAMP,
   STATUS,
   EMAIL_ADDRESS,
   HEADER_ID,
   ASSOCIATE,
   REQUEST_ID
)
AS
   SELECT 'Purchase Order' TRX_TYPE,
          'XML' COMMUNICATION_METHOD,
          b2bh.customer_po_number DOCUMENT_NUM,
          (SELECT organization_code FROM mtl_parameters mp WHERE mp.organization_id = ooh.ship_from_org_id and ROWNUM = 1) BRANCH,
          b2bh.creation_date TIME_STAMP,
          'Success' STATUS,
          'N/A' EMAIL_ADDRESS,
          ooh.header_id HEADER_ID,
          NULL ASSOCIATE,
          NULL REQUEST_ID
     FROM xxwc.xxwc_b2b_so_hdrs_stg_tbl b2bh, oe_order_headers_all ooh
    WHERE     b2bh.customer_po_number = ooh.cust_po_number
          AND ooh.order_source_id = 1021
   UNION
   SELECT 'PO Acknowledgement' TRX_TYPE,
          'XML' COMMUNICATION_METHOD,
          b2bh.customer_po_number DOCUMENT_NUM,
          (SELECT organization_code FROM mtl_parameters mp WHERE mp.organization_id = ooh.ship_from_org_id and ROWNUM = 1) BRANCH,
          b2bh.last_update_date TIME_STAMP,
          'Success' STATUS,
          'N/A' EMAIL_ADDRESS,
          ooh.header_id HEADER_ID,
          NULL ASSOCIATE,
          NULL REQUEST_ID
     FROM xxwc.xxwc_b2b_so_hdrs_stg_tbl b2bh, oe_order_headers_all ooh
    WHERE     b2bh.deliver_poa = '1'
          AND b2bh.customer_po_number = ooh.cust_po_number
          AND ooh.order_source_id = 1021
   UNION
   SELECT 'SO Acknowledgement' TRX_TYPE,
          'Email' COMMUNICATION_METHOD,
          TO_CHAR (ooh.order_number) DOCUMENT_NUM,
          (SELECT organization_code FROM mtl_parameters mp WHERE mp.organization_id = ooh.ship_from_org_id and ROWNUM = 1) BRANCH,
          bdi.request_date TIME_STAMP,
          'Sent' STATUS,
          bdi.cc_email EMAIL_ADDRESS,
          bdi.header_id HEADER_ID,
          fu_pri.user_name || ' - ' || fu_pri.description ASSOCIATE,
          bdi.request_id REQUEST_ID
     FROM xxwc.xxwc_b2b_so_delivery_info_tbl bdi, oe_order_headers_all ooh, fnd_user fu_pri
    WHERE     1 = 1
          AND bdi.header_id = ooh.header_id
          AND bdi.soa_delivered = '1'
          AND fu_pri.user_id = bdi.created_by
          AND bdi.request_id IS NOT NULL
   UNION
   SELECT 'Advance Ship Notice' TRX_TYPE,
          'Email' COMMUNICATION_METHOD,
          TO_CHAR (bdi.delivery_id) DOCUMENT_NUM,
          (SELECT mp.organization_code 
             FROM mtl_parameters mp
                , xxwc.xxwc_wsh_shipping_stg shp 
            WHERE mp.organization_id = shp.ship_from_org_id 
              AND shp.delivery_id = bdi.delivery_id
              AND ROWNUM = 1) BRANCH,
          bdi.request_date TIME_STAMP,
          'Sent' STATUS,
          bdi.cc_email EMAIL_ADDRESS,
          bdi.header_id HEADER_ID,
          fu_pri.user_name || ' - ' || fu_pri.description ASSOCIATE,
          bdi.request_id REQUEST_ID
     FROM xxwc.xxwc_b2b_so_delivery_info_tbl bdi, fnd_user fu_pri
    WHERE     1 = 1
          AND fu_pri.user_id = bdi.created_by
          AND bdi.asn_delivered IN ('1', '3')
          AND bdi.request_id IS NOT NULL
   UNION
   SELECT 'Invoice' TRX_TYPE,
          'XML' COMMUNICATION_METHOD,
          TO_CHAR (rcta.trx_number) DOCUMENT_NUM,
          (SELECT organization_code FROM mtl_parameters mp WHERE to_char(mp.organization_id) = rcta.interface_Header_attribute10 and ROWNUM = 1) BRANCH,
          TO_DATE (rcta.attribute10, 'MM/DD/YYYY') TIME_STAMP,
          'Success' STATUS,
          'N/A' EMAIL_ADDRESS,
          ooh.header_id HEADER_ID,
          NULL ASSOCIATE,
          NULL REQUEST_ID
     FROM ra_customer_trx_all rcta, oe_order_headers_all ooh
    WHERE     1 = 1
          AND rcta.interface_header_attribute1 = TO_CHAR (ooh.order_number)
          AND rcta.attribute10 IS NOT NULL;
/

GRANT SELECT ON APPS.XXWC_B2B_SO_DLVRY_INFO_DTLS_VW TO EA_APEX;
/