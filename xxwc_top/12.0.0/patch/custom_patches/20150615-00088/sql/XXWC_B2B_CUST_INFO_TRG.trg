CREATE OR REPLACE TRIGGER APPS.XXWC_B2B_CUST_INFO_TRG
   BEFORE INSERT OR UPDATE ON XXWC.XXWC_B2B_CUST_INFO_TBL FOR EACH ROW
/********************************************************************************
   $Header XXWC_B2B_CUST_INFO_TRG.TRG $
   Module Name: XXWC_B2B_CUST_INFO_TRG

   PURPOSE:   This trigger is used to update WHO columns and set default values.

   REVISIONS:
   Ver        Date        Author                     Description
   ---------  ----------  ---------------         -------------------------
   1.0        08/09/2015  Gopi Damuluri           TMS# 20150615-00088
                                                  Initial Version
********************************************************************************/
DECLARE
   l_user_id                NUMBER;
   l_sec                    VARCHAR2 (100);
BEGIN

   l_sec := 'Derive User-Id';
   BEGIN
   SELECT user_id 
     INTO l_user_id 
     FROM apps.fnd_user 
     WHERE user_name = NVL(v('APP_USER'),USER);
   EXCEPTION
   WHEN OTHERS THEN
   l_sec := 'Error deriving User-Id';
   END;
   
   IF :NEW.creation_date IS NULL THEN
     :NEW.creation_date := SYSDATE;   
     :NEW.created_by := l_user_id;
   END IF;

   :NEW.last_update_date := SYSDATE;
   :NEW.last_updated_by := l_user_id;

   IF :NEW.DELIVER_SOA IS NULL THEN
     :NEW.DELIVER_SOA := 'N';
   END IF;

   IF :NEW.DELIVER_ASN IS NULL THEN
     :NEW.DELIVER_ASN := 'N';
   END IF;

   IF :NEW.DELIVER_INVOICE IS NULL THEN
     :NEW.DELIVER_INVOICE := 'N';
   END IF;

   IF :NEW.DELIVER_POA IS NULL THEN
     :NEW.DELIVER_POA := 'N';
   END IF;

   IF :NEW.NOTIFY_ACCOUNT_MGR IS NULL THEN
     :NEW.NOTIFY_ACCOUNT_MGR := 'N';
   END IF;

   IF :OLD.STATUS != 'APPROVED' AND :NEW.STATUS = 'APPROVED' THEN
     :NEW.APPROVED_DATE := SYSDATE;
   END IF;

   l_sec := 'Start Date Active';
   IF :NEW.START_DATE_ACTIVE IS NULL THEN
     :NEW.START_DATE_ACTIVE := TRUNC(SYSDATE);
   END IF;

   l_sec := 'End Date Active';
   IF :NEW.END_DATE_ACTIVE IS NULL THEN
     :NEW.END_DATE_ACTIVE := TO_DATE('31-DEC-4099','DD-MON-YYYY');
   END IF;

EXCEPTION
WHEN OTHERS THEN
   xxcus_error_pkg.xxcus_error_main_api (
      p_called_from         => 'XXWC_B2B_CUST_INFO_TRG',
      p_calling             => l_sec,
      p_request_id          => -1,
      p_ora_error_msg       => SUBSTR (SQLERRM, 2000),
      p_error_desc          => 'Error in trigger - XXWC_B2B_CUST_INFO_TRG',
      p_distribution_list   => 'WC-ITDEVALERTS-U1@HDSupply.com',
      p_module              => 'XXWC');

   raise_application_error (-20001, 'XXWC_B2B_CUST_INFO_TRG: ' || SQLERRM);
END;
/