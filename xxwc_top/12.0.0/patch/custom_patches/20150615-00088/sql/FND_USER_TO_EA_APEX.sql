/********************************************************************************
   $Header FND_USER_TO_EA_APEX.sql $
   Module Name: FND_USER

   PURPOSE:  Grant to Apex on Table - FND_USER.

   REVISIONS:
   Ver        Date        Author                     Description
   ---------  ----------  ---------------         -------------------------
   1.0        08/09/2015  Gopi Damuluri           TMS# 20150615-00088
                                                  Initial Version
********************************************************************************/

GRANT SELECT ON APPS.FND_USER TO EA_APEX;

/