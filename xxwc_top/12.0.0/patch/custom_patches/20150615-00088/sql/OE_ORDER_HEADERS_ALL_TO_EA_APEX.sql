/********************************************************************************
   $Header OE_ORDER_HEADERS_ALL_TO_EA_APEX.sql $
   Module Name: OE_ORDER_HEADERS_ALL

   PURPOSE:  Grant to Apex on table - OE_ORDER_HEADERS_ALL.

   REVISIONS:
   Ver        Date        Author                     Description
   ---------  ----------  ---------------         -------------------------
   1.0        08/09/2015  Gopi Damuluri           TMS# 20150615-00088
                                                  Initial Version
********************************************************************************/

GRANT SELECT ON ONT.OE_ORDER_HEADERS_ALL TO EA_APEX WITH GRANT OPTION;

/