DROP VIEW APPS.XXWC_B2B_SO_DLVRY_INFO_HDR_VW;
/

/********************************************************************************
   $Header XXWC_B2B_SO_DLVRY_INFO_HDR_VW.VW $
   Module Name: XXWC_B2B_SO_DLVRY_INFO_HDR_VW

   PURPOSE:   View to derive B2B Sales Order Header Information.

   REVISIONS:
   Ver        Date        Author                     Description
   ---------  ----------  ---------------         -------------------------
   1.0        08/09/2015  Gopi Damuluri           TMS# 20150615-00088
                                                  Initial Version
********************************************************************************/

/* Formatted on 8/12/2015 11:26:55 AM (QP5 v5.265.14096.38000) */
CREATE OR REPLACE FORCE VIEW APPS.XXWC_B2B_SO_DLVRY_INFO_HDR_VW
(
   ORDER_NUMBER,
   B2B_CUSTOMER_PO_NUMBER,
   CUSTOMER_NUMBER,
   CUSTOMER_NAME,
   ORDERED_DATE,
   DELIVERY_ID,
   SOA_REQUEST_ID,
   SOA_RUN_DATE,
   ASN_REQUEST_ID,
   ASN_RUN_DATE,
   ASSOCIATE_NAME,
   TRACKING_NUM,
   EXPECTED_RECEIVED_DATE,
   SO_CREATED_BY,
   ORDER_SOURCE,
   EMAIL_ID,
   ORGANIZATION_CODE,
   HEADER_ID
)
AS
   SELECT ooh.order_number ORDER_NUMBER,
          --DECODE (ooh.order_source_id, 1021, ooh.cust_po_number, NULL)
          ooh.cust_po_number b2b_customer_po_number,
          hca.account_number Customer_Number,
          hca.account_name Customer_Name,
          ooh.ordered_date Ordered_Date,
          bdi.delivery_id,
          (SELECT MAX (bdi_soa.request_id)
             FROM xxwc.xxwc_b2b_so_delivery_info_tbl bdi_soa
            WHERE     1 = 1
                  AND bdi_soa.soa_delivered = '1'
                  AND bdi_soa.header_id = bdi.header_id
                  AND bdi_soa.request_id IS NOT NULL)
             soa_request_id,
          (SELECT MAX (bdi_soa.request_date)
             FROM xxwc.xxwc_b2b_so_delivery_info_tbl bdi_soa
            WHERE     1 = 1
                  AND bdi_soa.soa_delivered = '1'
                  AND bdi_soa.header_id = bdi.header_id
                  AND bdi_soa.request_id IS NOT NULL)
             soa_run_date,
          (SELECT MAX (bdi_asn.request_id)
             FROM xxwc.xxwc_b2b_so_delivery_info_tbl bdi_asn
            WHERE     1 = 1
                  AND bdi_asn.asn_delivered IN ('1', '3')
                  AND bdi_asn.header_id = bdi.header_id
                  AND bdi_asn.request_id IS NOT NULL)
             asn_request_id,
          (SELECT MAX (bdi_asn.request_date)
             FROM xxwc.xxwc_b2b_so_delivery_info_tbl bdi_asn
            WHERE     1 = 1
                  AND bdi_asn.asn_delivered IN ('1', '3')
                  AND bdi_asn.header_id = bdi.header_id
                  AND bdi_asn.request_id IS NOT NULL)
             asn_run_date,
          fu_pri.user_name || ' - ' || fu_pri.description Associate_NAME,
          bdi.tracking_num Tracking_Num,
          bdi.expected_received_date Expected_Received_Date,
          -- fu_cb.user_name || ' - ' || fu_cb.description SO_CREATED_BY,
          fu_cb.description SO_CREATED_BY,
          oos.name order_source,
          bdi.cc_email email_id,
          (SELECT organization_code
             FROM mtl_parameters
            WHERE organization_id = ooh.ship_from_org_id AND ROWNUM = 1)
             organization_code,
          ooh.header_id
     FROM apps.oe_order_headers_all ooh,
          hz_cust_accounts_all hca,
          xxwc.xxwc_b2b_so_delivery_info_tbl bdi,
          oe_order_sources oos,
          fnd_user fu_cb,
          fnd_user fu_pri
    WHERE     1 = 1
          --                    AND ooh.order_number IN ( '16284992', '16268011')
          AND ooh.order_source_id = oos.order_source_id
          AND ooh.flow_status_code NOT IN ('CANCELLED')
          AND ooh.sold_to_org_id = hca.cust_account_id
          AND ooh.header_id = bdi.header_id
          AND fu_cb.user_id = ooh.created_by
          AND fu_pri.user_id = bdi.created_by
          AND bdi.request_id IN (SELECT MAX (bdi_max.request_id)
                                   FROM xxwc.xxwc_b2b_so_delivery_info_tbl bdi_max
                                  WHERE     1 = 1
                                        AND ooh.header_id = bdi_max.header_id
                                        AND bdi_max.request_id IS NOT NULL);


GRANT ALL ON APPS.XXWC_B2B_SO_DLVRY_INFO_HDR_VW TO EA_APEX;









--GRANT SELECT ON APPS.XXWC_B2B_SO_DLVRY_INFO_HDR_VW TO INTERFACE_APEXWC;

--GRANT SELECT ON xxwc.xxwc_b2b_so_delivery_info_tbl TO EA_APEX;

--GRANT SELECT ON xxwc.xxwc_b2b_cust_info_tbl TO EA_APEX;