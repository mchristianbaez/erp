/********************************************************************************
   $Header XXWC_B2B_CUST_INFO_TBL_TO_EA_APEX.sql $
   Module Name: XXWC_B2B_SO_DELIVERY_INFO_TBL

   PURPOSE:  Grant to Apex on table - XXWC_B2B_CUST_INFO_TBL.

   REVISIONS:
   Ver        Date        Author                     Description
   ---------  ----------  ---------------         -------------------------
   1.0        08/09/2015  Gopi Damuluri           TMS# 20150615-00088
                                                  Initial Version
********************************************************************************/

GRANT ALL ON XXWC.XXWC_B2B_CUST_INFO_TBL TO EA_APEX;
/