/********************************************************************************
   $Header FND_PRINTER_TL_TO_EA_APEX.sql $
   Module Name: FND_PRINTER_TL

   PURPOSE:  Grant to Apex on Table - FND_PRINTER_TL.

   REVISIONS:
   Ver        Date        Author                     Description
   ---------  ----------  ---------------         -------------------------
   1.0        08/09/2015  Gopi Damuluri           TMS# 20150615-00088
                                                  Initial Version
********************************************************************************/

GRANT SELECT ON APPS.FND_PRINTER_TL TO EA_APEX;

/