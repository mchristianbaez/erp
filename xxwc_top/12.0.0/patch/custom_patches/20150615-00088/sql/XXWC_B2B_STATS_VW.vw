DROP VIEW APPS.XXWC_B2B_STATS_VW;
/

/********************************************************************************
   $Header XXWC_B2B_STATS_VW.VW $
   Module Name: XXWC_B2B_STATS_VW

   PURPOSE:   View to derive B2B Statistics Information.

   REVISIONS:
   Ver        Date        Author                     Description
   ---------  ----------  ---------------         -------------------------
   1.0        08/09/2015  Gopi Damuluri           TMS# 20150615-00088
                                                  Initial Version
********************************************************************************/

CREATE OR REPLACE FORCE VIEW APPS.XXWC_B2B_STATS_VW
(
   PARTY_NUMBER,
   ACCOUNT_NUMBER,
   PARTY_NAME,
   ACCOUNT_NAME,
   TP_NAME,
   ORDER_NUMBER,
   ORDER_TYPE,
   PERIOD,
   ORDER_SOURCE,
   SOA_DELIVERED,
   ASN_DELIVERED,
   CREATION_DATE,
   SALE_AMOUNT
)
AS
   SELECT hp.party_number,
          hca.account_number,
          hp.party_name,
          hca.account_name,
          b2bc.tp_name,
          ooh.order_number,
          'STANDARD ORDER' Order_Type,
          TO_CHAR (ooh.creation_date, 'MM-MONTH') PERIOD,
          'B2B LIASON'              ORDER_SOURCE,
          DECODE(SOA_DELIVERED, '1',1,0) SOA_DELIVERED,
          DECODE(ASN_DELIVERED, '1',1,'3',1,0) ASN_DELIVERED,
          ooh.creation_date,
          (SELECT SUM (
                     ROUND (
                        (ORDERED_QUANTITY * UNIT_SELLING_PRICE) + TAX_VALUE,
                        2))
             FROM oe_order_lines_all ool
            WHERE ool.header_id = ooh.header_id)
             SALE_AMOUNT
     FROM oe_order_headers_all ooh,
          xxwc.xxwc_b2b_so_delivery_info_tbl b2bd,
          xxwc.xxwc_b2b_cust_info_tbl b2bc,
          hz_cust_accounts_all hca,
          hz_parties hp
    WHERE     1 = 1
          AND ooh.header_id = b2bd.header_id
          AND ooh.sold_to_org_id = hca.cust_account_id
          AND hca.party_id = b2bc.party_id
          AND hca.party_id = hp.party_id
          AND ooh.order_type_id IN (1001, 1004)
          AND ooh.flow_status_code != 'CANCELLED'
   UNION
   SELECT hp.party_number,
          hca.account_number,
          hp.party_name,
          hca.account_name,
          b2bc.tp_name,
          ooh.order_number,
          'COUNTER ORDER' Order_Type,
          TO_CHAR (ooh.creation_date, 'MM-MONTH') PERIOD,
          'NON-B2B' ORDER_SOURCE,
          0 SOA_DELIVERED,
          0 ASN_DELIVERED,
          ooh.creation_date,
          (SELECT SUM (
                     ROUND (
                        (ORDERED_QUANTITY * UNIT_SELLING_PRICE) + TAX_VALUE,
                        2))
             FROM oe_order_lines_all ool
            WHERE ool.header_id = ooh.header_id)
             SALE_AMOUNT
     FROM oe_order_headers_all ooh,
          xxwc.xxwc_b2b_cust_info_tbl b2bc,
          hz_cust_accounts_all hca,
          hz_parties hp
    WHERE     1 = 1
          AND ooh.sold_to_org_id = hca.cust_account_id
          AND hca.party_id = b2bc.party_id
          AND hca.party_id = hp.party_id
          AND ooh.order_type_id IN (1001, 1004)
          AND NOT EXISTS (SELECT '1' FROM xxwc.xxwc_b2b_so_delivery_info_tbl b2bd WHERE b2bd.header_id = ooh.header_id)
          AND ooh.flow_status_code != 'CANCELLED';
/