/*************************************************************************
   Module Name: XXWC_B2B_CUST_INFO_TBL

   PURPOSE:   Update STATUS of XXWC.XXWC_B2B_CUST_INFO_TBL table to APPROVED.

   REVISIONS:
   Ver        Date        Author                     Description
   ---------  ----------  ---------------         -------------------------
   1.0        09/18/2015  Gopi Damuluri           Initial Version
****************************************************************************/

UPDATE xxwc.xxwc_b2b_cust_info_tbl
   SET status = 'APPROVED'
 WHERE status IS NULL;
/
