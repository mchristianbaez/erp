/********************************************************************************
   $Header FND_PROFILE_TO_EA_APEX.sql $
   Module Name: FND_PROFILE

   PURPOSE:  Grant to Apex on Package - FND_PROFILE.

   REVISIONS:
   Ver        Date        Author                     Description
   ---------  ----------  ---------------         -------------------------
   1.0        08/09/2015  Gopi Damuluri           TMS# 20150615-00088
                                                  Initial Version
********************************************************************************/

GRANT SELECT ON APPS.FND_PROFILE TO EA_APEX;

/