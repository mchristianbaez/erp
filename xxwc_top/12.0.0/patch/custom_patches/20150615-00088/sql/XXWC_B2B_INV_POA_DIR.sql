/*************************************************************************************************
$Header XXWC_B2B_INV_POA_DIR.sql $

Module Name: XXWC_B2B_INV_POA_DIR

PURPOSE: Directory to maintain Customer B2B POA Files.

REVISIONS:
Ver        Date         Author                Description
---------  -----------  ------------------    ----------------------------------------------
      1.0  06/11/2015   Gopi Damuluri         TMS# 20150615-00088 
                                              Additional columns for B2B Maintenance Form.
**************************************************************************************************/
CREATE OR REPLACE DIRECTORY XXWC_B2B_INV_POA_DIR as '/xx_iface/ebizprd/outbound/liason_b2b_poa';