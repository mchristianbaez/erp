/********************************************************************************
   $Header HZ_PARTIES_TO_EA_APEX.sql $
   Module Name: HZ_PARTIES

   PURPOSE:  Grant to Apex on table - HZ_PARTIES.

   REVISIONS:
   Ver        Date        Author                     Description
   ---------  ----------  ---------------         -------------------------
   1.0        08/09/2015  Gopi Damuluri           TMS# 20150615-00088
                                                  Initial Version
********************************************************************************/

GRANT SELECT ON AR.HZ_PARTIES TO EA_APEX WITH GRANT OPTION;

/