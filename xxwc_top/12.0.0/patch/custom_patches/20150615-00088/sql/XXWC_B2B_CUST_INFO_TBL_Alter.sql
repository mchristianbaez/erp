/*************************************************************************************************
$Header XXWC_B2B_CUST_INFO_TBL.sql $

Module Name: XXWC_B2B_CUST_INFO_TBL

PURPOSE: Table to maintain Customer B2B Information.

REVISIONS:
Ver        Date         Author                Description
---------  -----------  ------------------    ----------------------------------------------
      1.0  06/11/2015   Gopi Damuluri         TMS# 20150615-00088 
                                              Additional columns for B2B Maintenance Form.
**************************************************************************************************/

ALTER TABLE XXWC.XXWC_B2B_CUST_INFO_TBL ADD   (DELIVER_POA         VARCHAR2(1    BYTE)          DEFAULT 'N',
                                               DEFAULT_EMAIL       VARCHAR2(240  BYTE),
                                               STATUS              VARCHAR2(40   BYTE),
                                               NOTIFICATION_EMAIL  VARCHAR2(250  BYTE),
                                               NOTIFY_ACCOUNT_MGR  VARCHAR2(1    BYTE),
                                               SOA_EMAIL           VARCHAR2(240  BYTE),
                                               ASN_EMAIL           VARCHAR2(240  BYTE),
                                               COMMENTS            VARCHAR2(2000 BYTE),
                                               APPROVED_DATE       DATE);
/