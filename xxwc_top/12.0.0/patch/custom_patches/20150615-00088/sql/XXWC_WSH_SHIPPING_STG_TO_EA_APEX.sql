/********************************************************************************
   $Header XXWC.XXWC_WSH_SHIPPING_STG_TO_EA_APEX.sql $
   Module Name: XXWC.XXWC_WSH_SHIPPING_STG

   PURPOSE:  Grant to Apex on Table - XXWC.XXWC_WSH_SHIPPING_STG.

   REVISIONS:
   Ver        Date        Author                     Description
   ---------  ----------  ---------------         -------------------------
   1.0        08/09/2015  Gopi Damuluri           TMS# 20150615-00088
                                                  Initial Version
********************************************************************************/

GRANT SELECT ON XXWC.XXWC_WSH_SHIPPING_STG TO EA_APEX;

/