DROP VIEW APPS.XXWC_B2B_SO_DELVRY_INFO_VW;

/********************************************************************************
   $Header XXWC_B2B_SO_DELVRY_INFO_VW.VW $
   Module Name: XXWC_B2B_SO_DELVRY_INFO_VW

   PURPOSE:   View to derive B2B Sales Order Line Information.

   REVISIONS:
   Ver        Date        Author                     Description
   ---------  ----------  ---------------         -------------------------
   1.0        08/09/2015  Gopi Damuluri           TMS# 20150615-00088
                                                  Initial Version
********************************************************************************/

/* Formatted on 8/12/2015 11:27:45 AM (QP5 v5.265.14096.38000) */
CREATE OR REPLACE FORCE VIEW APPS.XXWC_B2B_SO_DELVRY_INFO_VW
(
   SO_NTID,
   SO_CREATION_NAME,
   CUSTOMER_NUMBER,
   CUSTOMER_NAME,
   ORDER_NUMBER,
   ORDERED_DATE,
   SALES_CHANNEL,
   LINE_NUMBER,
   ITEM_ORDERED,
   SHIPPING_BRANCH,
   LINE_ORDERED_QUANTITY,
   LINE_SHIPPED_QUANTITY,
   SALES,
   COST,
   LINE_TAX,
   HEADER_ID,
   PRICE_LIST_NAME
)
AS
     SELECT "SO_NTID",
            "SO_CREATION_NAME",
            "CUSTOMER_NUMBER",
            "CUSTOMER_NAME",
            "ORDER_NUMBER",
            "ORDERED_DATE",
            "SALES_CHANNEL",
            "LINE_NUMBER",
            "ITEM_ORDERED",
            "SHIPPING_BRANCH",
            "LINE_ORDERED_QUANTITY",
            "LINE_SHIPPED_QUANTITY",
            "SALES",
            "COST",
            "LINE_TAX",
            "HEADER_ID",
            "PRICE_LIST_NAME"
       FROM (SELECT DISTINCT
                    fu_cb.user_name SO_NTID,
                    fu_cb.description SO_Creation_Name,
                    hca.account_number Customer_Number,
                    hca.account_name Customer_Name,
                    ooh.order_number order_number,
                    ooh.ordered_date Ordered_Date,
                    ooh.attribute2 Sales_Channel,
                    ool.line_number || '.' || ool.shipment_number Line_Number,
                    msib.segment1 Item_Ordered,
                    NULL Shipping_Region,
                    (SELECT organization_code
                       FROM org_organization_definitions ood
                      WHERE     ood.organization_id = ool.ship_from_org_id
                            AND ROWNUM = 1)
                       Shipping_Branch,
                    ool.ORDERED_QUANTITY Line_Ordered_Quantity,
                    ool.SHIPPED_QUANTITY Line_Shipped_Quantity,
                    ROUND (
                         ool.UNIT_SELLING_PRICE
                       * NVL (ool.SHIPPED_QUANTITY, ool.ordered_quantity),
                       2)
                       Sales,
                    ROUND (
                         ool.UNIT_COST
                       * NVL (ool.SHIPPED_QUANTITY, ool.ordered_quantity),
                       2)
                       Cost,
                    ool.TAX_VALUE Line_Tax,
                    ooh.header_id,
                    (SELECT qlh.name
                       FROM qp_list_headers qlh, oe_price_adjustments opj
                      WHERE     1 = 1
                            AND opj.list_header_id = qlh.list_header_id
                            AND opj.automatic_flag = 'Y'
                            AND opj.line_id = ool.line_id
                            AND ROWNUM = 1)
                       Price_List_Name
               FROM apps.oe_order_headers_all ooh,
                    apps.oe_order_lines_all ool,
                    hz_cust_accounts_all hca,
                    fnd_user fu_cb,
                    mtl_system_items_b msib
              WHERE     1 = 1     --     AND ooh.order_number IN ( '16268011')
                    AND ooh.flow_status_code NOT IN ('CANCELLED')
                    AND ool.flow_status_code NOT IN ('CANCELLED')
                    AND ooh.header_id = ool.header_id
                    AND ool.inventory_item_id = msib.inventory_item_id
                    AND ool.ship_from_org_id = msib.organization_id
                    AND ooh.sold_to_org_id = hca.cust_account_id
                    AND fu_cb.user_id = ooh.created_by
                    AND EXISTS
                           (SELECT '1'
                              FROM xxwc.xxwc_b2b_so_delivery_info_tbl bdi_soa
                             WHERE     1 = 1
                                   AND ooh.header_id = bdi_soa.header_id
                                   AND bdi_soa.soa_delivered = '1'))
   ORDER BY ORDER_NUMBER, Line_Number;


GRANT SELECT ON APPS.XXWC_B2B_SO_DELVRY_INFO_VW TO EA_APEX;

GRANT SELECT ON APPS.XXWC_B2B_SO_DELVRY_INFO_VW TO INTERFACE_APEXWC;