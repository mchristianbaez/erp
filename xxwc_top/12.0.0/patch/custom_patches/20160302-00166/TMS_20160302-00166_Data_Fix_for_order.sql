/*
 TMS: 20160302-00166        
 Date: 03/02/2016
 Notes: Bug fix check
*/

SET SERVEROUTPUT ON SIZE 100000;


update XXWC.XXWC_OM_DMS_SHIP_CONFIRM_TBL
set SHIP_CONFIRM_STATUS = null
where order_number='18842832';

COMMIT;


commit;

/ 