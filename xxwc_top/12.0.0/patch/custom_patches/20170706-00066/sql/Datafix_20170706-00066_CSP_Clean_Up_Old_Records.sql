/******************************************************************************
   NAME:       TMS_20170706-00066_CSP_DATA_FIX.sql
   PURPOSE:    Update CSP wich is approved but in dash board it shows  AWAITING APPROVAL  and IN PROGRESS
   
   REVISIONS:
   Ver        Date        Author               Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        04/07/2017  Niraj K Ranjan   Initial Version TMS#20170706-00066   
                                            Clean up old records in the CSP dashboard
******************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20170706-00066   , Before Update');
   UPDATE XXWC_OM_CSP_NOTIFICATIONS_TBL xocn
   SET  agreement_status = 'APPROVED' 
       ,SELECTED_CSP     = 'Y'
       ,LAST_UPDATE_DATE = sysdate
   	,last_updated_by = 0
   WHERE 1=1
   AND   agreement_status IN ('IN PROGRESS','AWAITING_APPROVAL')
   AND   SELECTED_CSP     = 'Y'
   AND   EXISTS(SELECT 1 
                FROM  xxwc_om_contract_pricing_hdr xocp
   			    WHERE xocn.agreement_id = xocp.agreement_id
   			    AND   xocp.agreement_status = 'APPROVED' 
                ); 
   DBMS_OUTPUT.put_line (
         'TMS: 20170706-00066 Total Records updated for Agreement status APPROVED: '|| SQL%ROWCOUNT);
		 
   UPDATE XXWC_OM_CSP_NOTIFICATIONS_TBL xocn
   SET  agreement_status = 'REJECTED' 
       ,SELECTED_CSP     = 'Y'
       ,LAST_UPDATE_DATE = sysdate
   	   ,last_updated_by = 0
   WHERE 1=1
   AND   agreement_status IN ('IN PROGRESS','AWAITING_APPROVAL')
   AND   SELECTED_CSP     = 'Y'
   AND   EXISTS(SELECT 1 
                FROM  xxwc_om_contract_pricing_hdr xocp
   			    WHERE xocn.agreement_id = xocp.agreement_id
   			    AND   xocp.agreement_status = 'REJECTED' 
                ); 
   DBMS_OUTPUT.put_line (
         'TMS: 20170706-00066 Total Records updated for Agreement status REJECTED: '|| SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20170706-00066    , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20170706-00066 , Errors : ' || SQLERRM);
END;
/
