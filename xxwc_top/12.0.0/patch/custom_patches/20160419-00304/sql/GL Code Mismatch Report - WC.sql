--Report Name            : GL Code Mismatch Report - WC
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating View Data for GL Code Mismatch Report - WC
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_INV_GL_MISMATCH_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_INV_GL_MISMATCH_V',401,'','','','','SA059956','XXEIS','Eis Xxwc Item Gl Mismatch V','EXIGMV','','');
--Delete View Columns for EIS_XXWC_INV_GL_MISMATCH_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_INV_GL_MISMATCH_V',401,FALSE);
--Inserting View Columns for EIS_XXWC_INV_GL_MISMATCH_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_GL_MISMATCH_V','EXPENSE_ACCOUNT',401,'Expense Account','EXPENSE_ACCOUNT','','','','SA059956','VARCHAR2','','','Expense Account','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_GL_MISMATCH_V','SALES_DEFAULT',401,'Sales Default','SALES_DEFAULT','','','','SA059956','VARCHAR2','','','Sales Default','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_GL_MISMATCH_V','EXPENSE_ACCOUNT_FULL',401,'Expense Account Full','EXPENSE_ACCOUNT_FULL','','','','SA059956','VARCHAR2','','','Expense Account Full','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_GL_MISMATCH_V','SALES_ACCOUNT',401,'Sales Account','SALES_ACCOUNT','','','','SA059956','VARCHAR2','','','Sales Account','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_GL_MISMATCH_V','SALES_ACCOUNT_FULL',401,'Sales Account Full','SALES_ACCOUNT_FULL','','','','SA059956','VARCHAR2','','','Sales Account Full','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_GL_MISMATCH_V','COGS_DEFAULT',401,'Cogs Default','COGS_DEFAULT','','','','SA059956','VARCHAR2','','','Cogs Default','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_GL_MISMATCH_V','COGS_ACCOUNT',401,'Cogs Account','COGS_ACCOUNT','','','','SA059956','VARCHAR2','','','Cogs Account','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_GL_MISMATCH_V','COGS_ACCOUNT_FULL',401,'Cogs Account Full','COGS_ACCOUNT_FULL','','','','SA059956','VARCHAR2','','','Cogs Account Full','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_GL_MISMATCH_V','ITEM_CREATED_BY',401,'Item Created By','ITEM_CREATED_BY','','','','SA059956','VARCHAR2','','','Item Created By','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_GL_MISMATCH_V','CREATION_DATE',401,'Creation Date','CREATION_DATE','','','','SA059956','DATE','','','Creation Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_GL_MISMATCH_V','CATEGORY_CLASS_DESC',401,'Category Class Desc','CATEGORY_CLASS_DESC','','','','SA059956','VARCHAR2','','','Category Class Desc','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_GL_MISMATCH_V','CATEGORY_CLASS',401,'Category Class','CATEGORY_CLASS','','','','SA059956','VARCHAR2','','','Category Class','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_GL_MISMATCH_V','ITEM_TYPE',401,'Item Type','ITEM_TYPE','','','','SA059956','VARCHAR2','','','Item Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_GL_MISMATCH_V','ITEM_STATUS',401,'Item Status','ITEM_STATUS','','','','SA059956','VARCHAR2','','','Item Status','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_GL_MISMATCH_V','ORG',401,'Org','ORG','','','','SA059956','VARCHAR2','','','Org','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_GL_MISMATCH_V','DESCRIPTION',401,'Description','DESCRIPTION','','','','SA059956','VARCHAR2','','','Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_GL_MISMATCH_V','ITEM_NUM',401,'Item Num','ITEM_NUM','','','','SA059956','VARCHAR2','','','Item Num','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_GL_MISMATCH_V','LAST_UPDATE_DATE',401,'Last Update Date','LAST_UPDATE_DATE','','','','SA059956','DATE','','','Last Update Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_GL_MISMATCH_V','ORG_ACT_Y_N',401,'Org Act Y N','ORG_ACT_Y_N','','','','SA059956','VARCHAR2','','','Org Act Y N','','','');
--Inserting View Components for EIS_XXWC_INV_GL_MISMATCH_V
--Inserting View Component Joins for EIS_XXWC_INV_GL_MISMATCH_V
END;
/
set scan on define on
prompt Creating Report LOV Data for GL Code Mismatch Report - WC
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - GL Code Mismatch Report - WC
xxeis.eis_rs_ins.lov( 401,'SELECT DISTINCT concatenated_segments Item_Category,
                description
           FROM mtl_categories_kfv
           order by concatenated_segments','','EIS_INV_ITEM_CATEGORIES_LOV','Item Categories','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 401,'SELECT DISTINCT concatenated_segments item, description FROM mtl_system_items_kfv msi,
                org_organization_definitions ood
          WHERE msi.organization_id = ood.organization_id
            AND ood.operating_unit = fnd_profile.VALUE (''ORG_ID'')
       ORDER BY concatenated_segments','','EIS_INV_ITEM_LOV','List of all inventory items.','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'SELECT meaning
FROM mfg_lookups
WHERE lookup_type = ''SYS_YES_NO''','','EIS_INV_YES_NO_LOV','YES NO LOV','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 401,'select list_name from XXWC_PARAM_LIST where list_type = ''Item'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Item List','','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'select distinct organization_code
from apps.mtl_parameters','','XXWC Organization Code','Organization Code from the mtl_parameters table','ID020048',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'SELECT COGS.segment4 cogs_account
FROM mtl_system_items_b MSIB ,
  apps.gl_code_combinations_kfv COGS
WHERE 1                              =1
AND MSIB.COST_OF_SALES_ACCOUNT       = cogs.code_combination_id
AND MSIB.INVENTORY_ITEM_STATUS_CODE <> ''Inactive''
GROUP BY COGS.segment4
ORDER BY 1','','XXWC INV COGS ACCOUNT','This  will provide list of cogs accounts','PK059658',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'SELECT SALES.segment4 sales_account
FROM mtl_system_items_b MSIB ,
  apps.gl_code_combinations_kfv SALES
WHERE 1                              =1
AND MSIB.sales_account               = sales.code_combination_id
AND MSIB.inventory_item_status_code <> ''Inactive''
GROUP BY SALES.segment4
ORDER BY 1','','XXWC INV SALES ACCOUNT','This will proved list of sales accounts','PK059658',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'SELECT EXPEN.segment4 expense_account
FROM mtl_system_items_b MSIB ,
  apps.gl_code_combinations_kfv EXPEN
WHERE 1                              =1
AND MSIB.expense_account             = EXPEN.code_combination_id
AND MSIB.inventory_item_status_code <> ''Inactive''
GROUP BY EXPEN.segment4
ORDER BY 1','','XXWC INV EXPENSE ACCOUNT','This will provide list of expense accounts','PK059658',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for GL Code Mismatch Report - WC
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - GL Code Mismatch Report - WC
xxeis.eis_rs_utility.delete_report_rows( 'GL Code Mismatch Report - WC' );
--Inserting Report - GL Code Mismatch Report - WC
xxeis.eis_rs_ins.r( 401,'GL Code Mismatch Report - WC','','To provide a GL audit report between MST and Child Orgs for data governance improvement.','','','','SA059956','EIS_XXWC_INV_GL_MISMATCH_V','Y','','','SA059956','','N','White Cap Reports','','CSV,EXCEL,','');
--Inserting Report Columns - GL Code Mismatch Report - WC
xxeis.eis_rs_ins.rc( 'GL Code Mismatch Report - WC',401,'CATEGORY_CLASS','Category Class','Category Class','','','default','','6','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_GL_MISMATCH_V','','');
xxeis.eis_rs_ins.rc( 'GL Code Mismatch Report - WC',401,'CATEGORY_CLASS_DESC','Category Class Desc','Category Class Desc','','','default','','7','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_GL_MISMATCH_V','','');
xxeis.eis_rs_ins.rc( 'GL Code Mismatch Report - WC',401,'COGS_ACCOUNT','Cogs Account','Cogs Account','','','default','','11','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_GL_MISMATCH_V','','');
xxeis.eis_rs_ins.rc( 'GL Code Mismatch Report - WC',401,'COGS_ACCOUNT_FULL','Cogs Account Full','Cogs Account Full','','','default','','10','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_GL_MISMATCH_V','','');
xxeis.eis_rs_ins.rc( 'GL Code Mismatch Report - WC',401,'COGS_DEFAULT','Cogs Default','Cogs Default','','','default','','12','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_GL_MISMATCH_V','','');
xxeis.eis_rs_ins.rc( 'GL Code Mismatch Report - WC',401,'CREATION_DATE','Creation Date','Creation Date','','','default','','8','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_GL_MISMATCH_V','','');
xxeis.eis_rs_ins.rc( 'GL Code Mismatch Report - WC',401,'DESCRIPTION','Description','Description','','','default','','2','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_GL_MISMATCH_V','','');
xxeis.eis_rs_ins.rc( 'GL Code Mismatch Report - WC',401,'EXPENSE_ACCOUNT','Expense Account','Expense Account','','','default','','17','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_GL_MISMATCH_V','','');
xxeis.eis_rs_ins.rc( 'GL Code Mismatch Report - WC',401,'EXPENSE_ACCOUNT_FULL','Expense Account Full','Expense Account Full','','','default','','16','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_GL_MISMATCH_V','','');
xxeis.eis_rs_ins.rc( 'GL Code Mismatch Report - WC',401,'ITEM_CREATED_BY','Item Created By','Item Created By','','','default','','9','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_GL_MISMATCH_V','','');
xxeis.eis_rs_ins.rc( 'GL Code Mismatch Report - WC',401,'ITEM_NUM','Item Number','Item Num','','','default','','1','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_GL_MISMATCH_V','','');
xxeis.eis_rs_ins.rc( 'GL Code Mismatch Report - WC',401,'ITEM_STATUS','Item Status','Item Status','','','default','','4','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_GL_MISMATCH_V','','');
xxeis.eis_rs_ins.rc( 'GL Code Mismatch Report - WC',401,'ITEM_TYPE','Item Type','Item Type','','','default','','5','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_GL_MISMATCH_V','','');
xxeis.eis_rs_ins.rc( 'GL Code Mismatch Report - WC',401,'ORG','Organization','Org','','','default','','3','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_GL_MISMATCH_V','','');
xxeis.eis_rs_ins.rc( 'GL Code Mismatch Report - WC',401,'SALES_ACCOUNT','Sales Account','Sales Account','','','default','','14','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_GL_MISMATCH_V','','');
xxeis.eis_rs_ins.rc( 'GL Code Mismatch Report - WC',401,'SALES_ACCOUNT_FULL','Sales Account Full','Sales Account Full','','','default','','13','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_GL_MISMATCH_V','','');
xxeis.eis_rs_ins.rc( 'GL Code Mismatch Report - WC',401,'SALES_DEFAULT','Sales Default','Sales Default','','','default','','15','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_GL_MISMATCH_V','','');
xxeis.eis_rs_ins.rc( 'GL Code Mismatch Report - WC',401,'LAST_UPDATE_DATE','Last Update Date','Last Update Date','','','default','','18','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_GL_MISMATCH_V','','');
xxeis.eis_rs_ins.rc( 'GL Code Mismatch Report - WC',401,'ORG_ACT_Y_N','Orgs Active Y/N','Org Act Y N','','','default','','19','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_GL_MISMATCH_V','','');
--Inserting Report Parameters - GL Code Mismatch Report - WC
xxeis.eis_rs_ins.rp( 'GL Code Mismatch Report - WC',401,'Category Class','Category Class','CATEGORY_CLASS','IN','EIS_INV_ITEM_CATEGORIES_LOV','','VARCHAR2','N','Y','3','','Y','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'GL Code Mismatch Report - WC',401,'COGS Account','COGS Account','COGS_ACCOUNT','IN','XXWC INV COGS ACCOUNT','','VARCHAR2','N','Y','4','','Y','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'GL Code Mismatch Report - WC',401,'Expense Account','Expense Account','EXPENSE_ACCOUNT','IN','XXWC INV EXPENSE ACCOUNT','','VARCHAR2','N','Y','6','','Y','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'GL Code Mismatch Report - WC',401,'Item Number','Item Number','ITEM_NUM','IN','EIS_INV_ITEM_LOV','','VARCHAR2','N','Y','1','','Y','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'GL Code Mismatch Report - WC',401,'Organization','Organization','ORG','IN','XXWC Organization Code','','VARCHAR2','N','Y','2','','Y','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'GL Code Mismatch Report - WC',401,'Sales Account','Sales Account','SALES_ACCOUNT','IN','XXWC INV SALES ACCOUNT','','VARCHAR2','N','Y','5','','Y','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'GL Code Mismatch Report - WC',401,'Item List','Item List','','IN','XXWC Item List','','VARCHAR2','N','Y','7','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'GL Code Mismatch Report - WC',401,'Creation Date From','Creation Date From','CREATION_DATE','>=','','','DATE','N','Y','8','','Y','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'GL Code Mismatch Report - WC',401,'Last Update Date From','Last Update Date From','LAST_UPDATE_DATE','>=','','','DATE','N','Y','10','','Y','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'GL Code Mismatch Report - WC',401,'Creation Date To','Creation Date To','CREATION_DATE','<=','','','DATE','N','Y','9','','Y','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'GL Code Mismatch Report - WC',401,'Last Update Date To','Last Update Date To','LAST_UPDATE_DATE','<=','','','DATE','N','Y','11','','Y','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'GL Code Mismatch Report - WC',401,'Orgs Active Y/N','Orgs Active Y/N','ORG_ACT_Y_N','=','EIS_INV_YES_NO_LOV','','VARCHAR2','N','Y','12','','Y','CONSTANT','SA059956','Y','N','','','');
--Inserting Report Conditions - GL Code Mismatch Report - WC
xxeis.eis_rs_ins.rcn( 'GL Code Mismatch Report - WC',401,'CATEGORY_CLASS','IN',':Category Class','','','Y','3','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'GL Code Mismatch Report - WC',401,'EXPENSE_ACCOUNT','IN',':Expense Account','','','Y','6','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'GL Code Mismatch Report - WC',401,'SALES_ACCOUNT','IN',':Sales Account','','','Y','5','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'GL Code Mismatch Report - WC',401,'COGS_ACCOUNT','IN',':COGS Account','','','Y','4','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'GL Code Mismatch Report - WC',401,'ITEM_NUM','IN',':Item Number','','','Y','1','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'GL Code Mismatch Report - WC',401,'ORG','IN',':Organization','','','Y','2','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'GL Code Mismatch Report - WC',401,'CREATION_DATE','>=',':Creation Date From','','','Y','8','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'GL Code Mismatch Report - WC',401,'CREATION_DATE','<=',':Creation Date To','','','Y','9','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'GL Code Mismatch Report - WC',401,'LAST_UPDATE_DATE','>=',':Last Update Date From','','','Y','10','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'GL Code Mismatch Report - WC',401,'LAST_UPDATE_DATE','<=',':Last Update Date To','','','Y','11','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'GL Code Mismatch Report - WC',401,'','','','','AND (:Item List is null or EXISTS (SELECT 1  FROM
                         APPS.XXWC_PARAM_LIST
                         WHERE LIST_NAME = :Item List
                            AND LIST_TYPE =''Item''
                         AND DBMS_LOB.INSTR(LIST_VALUES,
                          EXIGMV.ITEM_NUM, 1, 1)<>0))
','Y','0','','SA059956');
xxeis.eis_rs_ins.rcn( 'GL Code Mismatch Report - WC',401,'ORG_ACT_Y_N','=',':Orgs Active Y/N','','','Y','12','Y','SA059956');
--Inserting Report Sorts - GL Code Mismatch Report - WC
xxeis.eis_rs_ins.rs( 'GL Code Mismatch Report - WC',401,'ITEM_NUM','ASC','SA059956','1','');
xxeis.eis_rs_ins.rs( 'GL Code Mismatch Report - WC',401,'ORG','ASC','SA059956','2','');
--Inserting Report Triggers - GL Code Mismatch Report - WC
--Inserting Report Templates - GL Code Mismatch Report - WC
--Inserting Report Portals - GL Code Mismatch Report - WC
--Inserting Report Dashboards - GL Code Mismatch Report - WC
--Inserting Report Security - GL Code Mismatch Report - WC
xxeis.eis_rs_ins.rsec( 'GL Code Mismatch Report - WC','401','','50990',401,'SA059956','','');
--Inserting Report Pivots - GL Code Mismatch Report - WC
END;
/
set scan on define on
