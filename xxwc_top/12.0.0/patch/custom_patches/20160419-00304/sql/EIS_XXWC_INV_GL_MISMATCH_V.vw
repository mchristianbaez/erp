---------------------------------------------------------------------------------------------------------------
/**************************************************************************************************************
  $Header XXEIS.EIS_XXWC_INV_GL_MISMATCH_V $
  Module Name : Inventory
  PURPOSE	  : GL Code Mismatch Report – WC
  TMS Task Id : 20160419-00304  
  REVISIONS   :
  VERSION 		DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     	27-May-2016        		Siva   		 Initial version--TMS#20160419-00304 
**************************************************************************************************************/
CREATE OR REPLACE VIEW XXEIS.EIS_XXWC_INV_GL_MISMATCH_V (ITEM_NUM, DESCRIPTION, ORG, ITEM_STATUS, ITEM_TYPE, CATEGORY_CLASS, 
CATEGORY_CLASS_DESC, CREATION_DATE, ITEM_CREATED_BY, COGS_ACCOUNT_FULL, COGS_ACCOUNT, COGS_DEFAULT, SALES_ACCOUNT_FULL, SALES_ACCOUNT, 
EXPENSE_ACCOUNT_FULL, SALES_DEFAULT, EXPENSE_ACCOUNT, LAST_UPDATE_DATE, ORG_ACT_Y_N)
AS
  SELECT mib.segment1 item_num,
    mib.description,
    orgs.organization_code org,
    mib.inventory_item_status_code item_status,
    mib.item_type,
    mic.category_concat_segs category_class,
    mcv.description category_class_desc,
    TRUNC(mib.creation_date) creation_date,
    fuse.description item_created_by,
    cogs.concatenated_segments cogs_account_full,
    cogs.segment4 cogs_account,
    mcv.attribute1 cogs_default,
    sales.concatenated_segments sales_account_full,
    sales.segment4 sales_account,
    expense.concatenated_segments expense_account_full,
    mcv.attribute2 sales_default,
    expense.segment4 expense_account,
    TRUNC(mib.last_update_date) last_update_date,
    CASE
      WHEN date_to IS NULL
      THEN 'Yes'
      WHEN date_to >=sysdate
      THEN 'Yes'
      ELSE'No'
    END org_act_y_n
  FROM apps.mtl_system_items_b mib,
    apps.mtl_parameters orgs,
    apps.mtl_item_categories_v mic,
    apps.fnd_user fuse,
    apps.mtl_categories_v mcv,
    apps.gl_code_combinations_kfv cogs,
    apps.gl_code_combinations_kfv sales,
    apps.gl_code_combinations_kfv expense,
    hr_organization_units hou
  WHERE mib.organization_id = orgs.organization_id
  AND mib.inventory_item_id = mic.inventory_item_id
  AND mib.organization_id   = mic.organization_id
  AND mib.created_by        = fuse.user_id(+)
    --and MIB.item_type = 'INTANGIBLE'
  AND mic.category_concat_segs = mcv.category_concat_segs
    --  and MCV.structure_name = 'Item Categories'
  AND mcv.structure_id =101
    --  and MIC.category_set_name = 'Inventory Category'
  AND mic.category_set_id         =1100000062
  AND orgs.master_organization_id = 222
  AND orgs.organization_code NOT LIKE ('9%')
    --and MIC.category_concat_segs = 'PR.PRMO'
  AND mib.cost_of_sales_account       = cogs.code_combination_id
  AND mib.sales_account               = sales.code_combination_id
  AND mib.expense_account             = expense.code_combination_id
  AND mib.inventory_item_status_code <> 'Inactive'
  AND cogs.segment4                  <> mcv.attribute1
  AND hou.organization_id             = orgs.organization_id
  UNION
  SELECT mib.segment1 item_num,
    mib.description,
    orgs.organization_code org,
    mib.inventory_item_status_code item_status,
    mib.item_type,
    mic.category_concat_segs category_class,
    mcv.description category_class_desc,
    TRUNC(mib.creation_date) creation_date,
    fuse.description item_created_by,
    cogs.concatenated_segments cogs_account_full,
    cogs.segment4 cogs_account,
    mcv.attribute1 cogs_default,
    sales.concatenated_segments sales_account_full,
    sales.segment4 sales_account,
    expense.concatenated_segments expense_account_full,
    mcv.attribute2 sales_default,
    expense.segment4 expense_account,
    TRUNC(mib.last_update_date) last_update_date,
    CASE
      WHEN hou.date_to IS NULL
      THEN 'Yes'
      WHEN hou.date_to >=sysdate
      THEN 'Yes'
      ELSE'No'
    END org_act_y_n
  FROM apps.mtl_system_items_b mib,
    apps.mtl_parameters orgs,
    apps.mtl_item_categories_v mic,
    apps.fnd_user fuse,
    apps.mtl_categories_v mcv,
    apps.gl_code_combinations_kfv cogs,
    apps.gl_code_combinations_kfv sales,
    apps.gl_code_combinations_kfv expense,
    hr_organization_units hou
  WHERE mib.organization_id = orgs.organization_id
  AND mib.inventory_item_id = mic.inventory_item_id
  AND mib.organization_id   = mic.organization_id
  AND mib.created_by        = fuse.user_id(+)
    --and MIB.item_type = 'INTANGIBLE'
  AND mic.category_concat_segs = mcv.category_concat_segs
    --  and MCV.structure_name = 'Item Categories'
  AND mcv.structure_id =101
    --  and MIC.category_set_name = 'Inventory Category'
  AND mic.category_set_id         =1100000062
  AND orgs.master_organization_id = 222
  AND orgs.organization_code NOT LIKE ('9%')
    --and MIC.category_concat_segs = 'PR.PRMO'
  AND mib.cost_of_sales_account       = cogs.code_combination_id
  AND mib.sales_account               = sales.code_combination_id
  AND mib.expense_account             = expense.code_combination_id
  AND mib.inventory_item_status_code <> 'Inactive'
  AND sales.segment4                 <> mcv.attribute2
  AND hou.organization_id             = orgs.organization_id
/
