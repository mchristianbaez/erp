CREATE OR REPLACE PACKAGE BODY APPS.XXCUSOZF_UTIL_PKG IS
-- ======================================================
-- ESMS     RFC      Date         Note
-- 238834   39803    21-MAR-2014  Pass lob name to fnd_request in the routine create_deduction_documents so we can
--                                rename the output files to include the lob names.
-- 238834   39803    21-MAR-2014  Added two new routines create_lob_deduction_docs and lob_wrapper [ESMS 238834 / RFC 39803 ]
-- 245324            06-APR-2014  Routines lob_wrapper, create_lob_deduction_docs, deduction_exists 
--                                and create_deduction_documents require filters by deduction invoice type REBATE_DEDUCTION. 
-- 245324   40003    07-APR-2014  Add filter transaction_type in the routines lob_wrapper, create_lob_deduction_docs, deduction_exists 
--                                and create_deduction_documents. 
-- 486906   40529    15-MAY-2014  Added XXCUSOZF_GET_MAX_US_LOB function per Incident# 486906
--          40529    16-MAY-2014  Modified XXCUSOZF_GET_MAX_US_LOB and XXCUSOZF_GET_MAX_LOB function per Incident# 486906
-- 269629   43035    05-MAR-2015  Changes to routine create_lob_deduction_docs, new routines beforereport and 
--                                afterreport.
-- I 566538          05-MAR-2015  comment print_flag ='N'
-- 
-- 281984            23-JUN-2015  Massage timeframe field if the offer calendar year on invoice is for prior fiscal year
--TMS#20180927-00004 25-Jul-2018  Timeframe Issue Fix by Ashwin.S 
--TMS#20180507-00352 26-Jul-2018  Timeframe Issue Fix by Ashwin.S  
   
-- ==========================================================================
  --
  -- package body global variables
  --
  G_DEBUG BOOLEAN := FND_MSG_PUB.check_msg_level(FND_MSG_PUB.g_msg_lvl_debug_high);
  --
  procedure print_log(p_message in varchar2) is
  begin 
   if fnd_global.conc_request_id >0 then
    fnd_file.put_line(fnd_file.log, p_message);
   else
    dbms_output.put_line(p_message);
   end if;
  exception
   when others then
    fnd_file.put_line(fnd_file.log, 'Issue in print_log routine ='||sqlerrm);
  end print_log;  
  --
 /*The Function to get the LOB that has the maximum spend*/
  --
  FUNCTION XXCUSOZF_GET_MAX_LOB(p_vendor_id IN NUMBER) RETURN NUMBER IS
  
    TYPE chararray is TABLE OF VARCHAR2(3000) INDEX BY BINARY_INTEGER;
    l_expense            chararray;
    l_bill_to_party_id   chararray;
    l_bill_to_party_name chararray;
  
    ----Get the party that has inccured maximum expense-----
  
    CURSOR max_expense_csr(cv_vendor_account_id IN NUMBER) IS
      select cbs.end_cust_party_id -- party number
        from XXCUSOZF_BU_SPEND_MV cbs
       WHERE CBS.SPEND IN
             (SELECT MAX(CBS2.SPEND)
                FROM APPS.XXCUSOZF_BU_SPEND_MV CBS2
               WHERE CBS2.SOLD_FROM_CUST_ACCOUNT_ID = CV_VENDOR_ACCOUNT_ID
                     AND CBS.SOLD_FROM_CUST_ACCOUNT_ID =CBS2.SOLD_FROM_CUST_ACCOUNT_ID   --486906 
               );
  
    l_max_expense_party VARCHAR2(50);
  
  BEGIN
  
    ----Get the Party with Maximum Expense---
  
    OPEN max_expense_csr(p_vendor_id);
    FETCH max_expense_csr
      INTO l_max_expense_party;
    CLOSE max_expense_csr;
  
    IF l_max_expense_party IS NOT NULL THEN
      RETURN l_max_expense_party;
    ELSE
      RETURN(-1);
    
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      --raise_application_error(-20001,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
      RETURN(-1);
    
  END XXCUSOZF_GET_MAX_LOB;
  --
  FUNCTION XXCUSOZF_GET_MAX_US_LOB(p_vendor_id IN NUMBER) RETURN NUMBER IS  -- Incident 486906
  
    TYPE chararray is TABLE OF VARCHAR2(3000) INDEX BY BINARY_INTEGER;
    l_expense            chararray;
    l_bill_to_party_id   chararray;
    l_bill_to_party_name chararray;
  
    ----Get the party that has inccured maximum expense-----
  
    CURSOR max_expense_csr(cv_vendor_account_id IN NUMBER) IS
      SELECT CBS.END_CUST_PARTY_ID 
        from apps.XXCUSOZF_BU_SPEND_MV cbs
       WHERE  CBS.SPEND IN
             (SELECT MAX(CBS2.SPEND)
                FROM APPS.XXCUSOZF_BU_SPEND_MV CBS2
               WHERE CBS2.SOLD_FROM_CUST_ACCOUNT_ID = CV_VENDOR_ACCOUNT_ID
                     AND CBS.SOLD_FROM_CUST_ACCOUNT_ID =CBS2.SOLD_FROM_CUST_ACCOUNT_ID    --486906 
                    AND END_CUST_PARTY_ID NOT IN (SELECT DISTINCT LOOKUP_CODE           
                                                      FROM   FND_LOOKUP_VALUES_VL
                                                      WHERE  1 =1
                                                      AND  LOOKUP_TYPE = 'XXCUS_CAD_BU')
               );
  
    l_max_expense_party VARCHAR2(50);
  
  BEGIN
  
    ----Get the Party with Maximum Expense---
  
    OPEN max_expense_csr(p_vendor_id);
    FETCH max_expense_csr
      INTO l_max_expense_party;
    CLOSE max_expense_csr;
  
    IF l_max_expense_party IS NOT NULL THEN
      RETURN l_max_expense_party;
    ELSE
      RETURN(-1);
    
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      --raise_application_error(-20001,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
      RETURN(-1);
    
  END XXCUSOZF_GET_MAX_US_LOB;
  --
  /*Procedure is caled by Workflow Settlement Process*/
  --
  PROCEDURE Get_Settlement_Method(itemtype IN VARCHAR2,
                                  itemkey  IN VARCHAR2,
                                  accitd   IN NUMBER,
                                  funcmode IN VARCHAR2,
                                  RESULT   OUT VARCHAR2) IS
    l_payment_method VARCHAR2(100);
  
  BEGIN
    IF (funcmode != wf_engine.eng_run) THEN
      RESULT := wf_engine.eng_null;
      RETURN;
    END IF;
  
    --Get the Claim Payment Method
    l_payment_method := wf_engine.getitemattrtext(itemtype => 'OZF_CSTL',
                                                  itemkey  => itemkey,
                                                  ANAME    => 'OZF_PAYMENT_METHOD_CODE');
  
    --IF l_payment_method IS NOT NULL THEN
    --INSERT INTO CUST_LOG VALUES ('l_payment_method', l_payment_method);
    --END IF;
  
    --wf_item.ClearCache;
    --Call to Custom Settlement Process based on the Settlement Method
  
    IF l_payment_method = 'HDS_REB_INV' THEN
      RESULT := 'COMPLETE:AR_INVOICE';
      /*ELSIF l_payment_method = 'RX_CREDIT' THEN
       RESULT := 'COMPLETE:RX_CREDIT';
      ELSIF l_payment_method = 'RX_ACCNTING' THEN
       RESULT := 'COMPLETE:RX_ACCNTING';*/
    END IF;
  
  EXCEPTION
    WHEN OTHERS THEN
      wf_core.CONTEXT('XXCUS_UTIL_PKG',
                      'Get_Settlement_Method',
                      itemtype,
                      itemkey,
                      to_char(l_payment_method),
                      funcmode);
      RESULT := wf_engine.eng_null;
  END Get_Settlement_Method;
  --
  PROCEDURE Create_AR_Debit(itemtype IN VARCHAR2,
                            itemkey  IN VARCHAR2,
                            accitd   IN NUMBER,
                            funcmode IN VARCHAR2,
                            RESULT   OUT VARCHAR2) IS
    l_claim_id     NUMBER;
    l_claim_number apps.ozf_claims_all.claim_number%TYPE;
    l_ap_credit_id NUMBER;
  
    l_return_status VARCHAR2(10);
    l_msg_count     NUMBER;
    l_msg_data      VARCHAR2(20000);
    l_api_version   number := 1.0;
    l_error_message VARCHAR2(20000);
  
    CURSOR c_get_credit_id(p_claim_id IN NUMBER) IS
      SELECT claim_number, attribute1
        FROM apps.ozf_claims_all
       WHERE claim_id = p_claim_id;
  
  BEGIN
    IF (funcmode != wf_engine.eng_run) THEN
      RESULT := wf_engine.eng_null;
      RETURN;
    END IF;
  
    l_claim_id := wf_engine.GetItemAttrNumber(itemtype => 'OZF_CSTL',
                                              itemkey  => itemkey,
                                              ANAME    => 'OZF_CLAIM_ID');
  
    --IF l_claim_id IS NOT NULL THEN
    --INSERT INTO CUST_LOG VALUES ('Claim ID', l_claim_id);
    -- END IF;
  
    FND_FILE.PUT_LINE(FND_FILE.LOG, 'l_claim_id' || l_claim_id);
    /*
    UPDATE ozf_claims_all
       SET payment_method = 'DEBIT_MEMO'
     WHERE claim_id = l_claim_id; */
    FND_FILE.PUT_LINE(FND_FILE.LOG, 'Entering the Interface Claim');
    XXCUS_OZF_AR_INTF_PVT.Interface_Claim(p_api_version      => l_api_version,
                                          p_init_msg_list    => FND_API.g_false,
                                          p_commit           => FND_API.g_false,
                                          p_validation_level => FND_API.g_valid_level_full,
                                          x_return_status    => l_return_status,
                                          x_msg_data         => l_msg_data,
                                          x_msg_count        => l_msg_count,
                                          P_CLAIM_ID         => L_CLAIM_ID);
    FND_FILE.PUT_LINE(FND_FILE.LOG, 'end the Interface Claim');
    IF l_return_status IN
       (FND_API.g_ret_sts_error, FND_API.g_ret_sts_unexp_error) THEN
    
      fnd_msg_pub.count_and_get(p_encoded => fnd_api.g_false,
                                p_count   => l_msg_count,
                                p_data    => l_msg_data);
    
      FOR i IN 1 .. l_msg_count LOOP
        l_error_message := l_error_message || CHR(10) ||
                           SUBSTR(fnd_msg_pub.get(p_msg_index => i,
                                                  p_encoded   => 'F'),
                                  1,
                                  254);
      END LOOP;
      wf_engine.setitemattrtext(itemtype => 'OZF_CSTL',
                                itemkey  => itemkey,
                                aname    => 'OZF_CSETL_ERR_SUBJ',
                                avalue   => 'Error in Interface Claim:-' ||
                                            l_claim_id);
      wf_engine.setitemattrtext(itemtype => 'OZF_CSTL',
                                itemkey  => itemkey,
                                aname    => 'OZF_CSETL_ERR_MSG',
                                avalue   => NVL(SUBSTR(l_error_message,
                                                       -1000),
                                                l_error_message));
      RESULT := 'COMPLETE:ERROR';
      RETURN;
      /*ELSIF l_return_status = FND_API.g_ret_sts_unexp_error THEN
      RESULT := 'COMPLETE:ERROR';
      RETURN;     */
    END IF;
  
    RESULT := 'COMPLETE:SUCCESS';
    RETURN;
  
  EXCEPTION
    WHEN OTHERS THEN
      wf_core.CONTEXT('XXCUS_UTIL_PKG',
                      'Create_AR_Debit',
                      itemtype,
                      itemkey,
                      to_char(l_claim_number),
                      funcmode);
      RESULT := 'COMPLETE:ERROR';
  END Create_AR_Debit;
  --
  PROCEDURE Create_AR_Credit(itemtype IN VARCHAR2,
                             itemkey  IN VARCHAR2,
                             accitd   IN NUMBER,
                             funcmode IN VARCHAR2,
                             RESULT   OUT VARCHAR2) IS
    l_claim_id     NUMBER;
    l_claim_number apps.ozf_claims_all.claim_number%TYPE;
    l_ap_credit_id NUMBER;
  
    l_return_status VARCHAR2(10);
    l_msg_count     NUMBER;
    l_msg_data      VARCHAR2(100);
    l_api_version   number := 1.0;
    l_error_message VARCHAR2(4000);
  
    CURSOR c_get_credit_id(p_claim_id IN NUMBER) IS
      SELECT claim_number, attribute1
        FROM apps.ozf_claims_all
       WHERE claim_id = p_claim_id;
  
  BEGIN
    IF (funcmode != wf_engine.eng_run) THEN
      RESULT := wf_engine.eng_null;
      RETURN;
    END IF;
  
    l_claim_id := wf_engine.GetItemAttrNumber(itemtype => 'OZF_CSTL',
                                              itemkey  => itemkey,
                                              ANAME    => 'OZF_CLAIM_ID');
    
  
    FND_FILE.PUT_LINE(FND_FILE.LOG, 'l_claim_id' || l_claim_id);
    /*
    UPDATE ozf_claims_all
       SET payment_method = 'DEBIT_MEMO'
     WHERE claim_id = l_claim_id; */
  
    FND_FILE.PUT_LINE(FND_FILE.LOG, 'Entering the Interface Claim');
  
    XXCUS_OZF_AR_INTF_PVT.Interface_Claim(p_api_version      => l_api_version,
                                          p_init_msg_list    => FND_API.g_false,
                                          p_commit           => FND_API.g_false,
                                          p_validation_level => FND_API.g_valid_level_full,
                                          x_return_status    => l_return_status,
                                          x_msg_data         => l_msg_data,
                                          x_msg_count        => l_msg_count,
                                          P_CLAIM_ID         => L_CLAIM_ID);
    FND_FILE.PUT_LINE(FND_FILE.LOG, ' End the Interface Claim');
    IF l_return_status IN
       (FND_API.g_ret_sts_error, FND_API.g_ret_sts_unexp_error) THEN
    
      fnd_msg_pub.count_and_get(p_encoded => fnd_api.g_false,
                                p_count   => l_msg_count,
                                p_data    => l_msg_data);
    
      FOR i IN 1 .. l_msg_count LOOP
        l_error_message := l_error_message || CHR(10) ||
                           SUBSTR(fnd_msg_pub.get(p_msg_index => i,
                                                  p_encoded   => 'F'),
                                  1,
                                  254);
      END LOOP;
      wf_engine.setitemattrtext(itemtype => 'OZF_CSTL',
                                itemkey  => itemkey,
                                aname    => 'OZF_CSETL_ERR_SUBJ',
                                avalue   => 'Error in Interface Claim:-' ||
                                            l_claim_id);
      wf_engine.setitemattrtext(itemtype => 'OZF_CSTL',
                                itemkey  => itemkey,
                                aname    => 'OZF_CSETL_ERR_MSG',
                                avalue   => NVL(SUBSTR(l_error_message,
                                                       -1000),
                                                l_error_message));
      RESULT := 'COMPLETE:ERROR';
      RETURN;
      /*ELSIF l_return_status = FND_API.g_ret_sts_unexp_error THEN
      RESULT := 'COMPLETE:ERROR';
      RETURN;     */
    END IF;
  
    RESULT := 'COMPLETE:SUCCESS';
    RETURN;
  
  EXCEPTION
    WHEN OTHERS THEN
      wf_core.CONTEXT('XXCUS_UTIL_PKG ',
                      'Create_AR_Debit',
                      itemtype,
                      itemkey,
                      to_char(l_claim_number),
                      funcmode);
      RESULT := 'COMPLETE:ERROR';
  END Create_AR_Credit;
  --
  --
  --------------------------------------------------------------------------------
  --    API name   : create_deduction_document
  --    Type       : Public
  --    Pre-reqs   : None
  --    Function   : Package that created the deduction document and emails the deduction document
  --    Parameters :
  --
  --    IN         : p_trx_number                     IN VARCHAR2
  --               : P_lob_id                         IN VARCHAR2,
  --               : p_email_address                  IN VARCHAR2
  --               
  --               
  --               
  --               
  --
  --    Version    : Current version     1.0 
  --
  --------------------------------------------------------------------------------
  --
  --
  PROCEDURE create_deduction_documents
                                    (
                                      ERRBUF          OUT NOCOPY VARCHAR2,
                                      RETCODE         OUT NOCOPY NUMBER,
                                      p_trx_number    IN VARCHAR2,
                                      p_lob_id        IN VARCHAR2,
                                      p_email_address IN VARCHAR2,
                                      p_cc_email_id   IN  VARCHAR2                                      
                                     ) is
  
    lv_req_id NUMBER(30) := 0;
    vflag2    BOOLEAN;
  
    CURSOR chk_trsfr_lob_flg(cv_trx_number IN VARCHAR2, cv_lob_id IN VARCHAR2) IS
      SELECT  rcta.trx_number
        FROM ra_customer_trx_all rcta
            ,ra_cust_trx_types_all rctt
       WHERE 1=1
       AND rctt.name ='REBATE_DEDUCTION'
       AND rcta.cust_trx_type_id =rcta.cust_trx_type_id
       AND rcta.customer_trx_id = nvl(cv_trx_number, rcta.customer_trx_id)
         AND to_char(rcta.attribute1) = nvl(cv_lob_id, rcta.attribute1)
         AND NVL(rcta.attribute11, 'N') = 'N'
       ORDER BY rcta.trx_number;
  
    vreqphase                   VARCHAR2(50);
    vreqstatus                  VARCHAR2(50);
    vreqdevphase                VARCHAR2(50);
    vreqdevstatus               VARCHAR2(50);
    vreqmessage                 VARCHAR2(50);
    fhandle                     UTL_FILE.file_type;
    vtextout                    VARCHAR2(32000);
    text                        VARCHAR2(32000);
    v_file                      VARCHAR2(500);
    v_request_id                NUMBER := NULL;
    v_request_id_1              NUMBER := NULL;
    v_request_status            BOOLEAN;
    v_phase                     VARCHAR2(2000);
    v_wait_status               VARCHAR2(2000);
    v_dev_phase                 VARCHAR2(2000);
    v_dev_status                VARCHAR2(2000);
    v_message                   VARCHAR2(2000);
    v_application_id            NUMBER;
    v_concurrent_program_id     NUMBER;
    v_conc_prog_short_name      VARCHAR2(100);
    v_conc_prog_appl_short_name VARCHAR2(100);
    v_output_file_path          VARCHAR2(200);
    l_transfr_lob               VARCHAR2(3);
    l_err_msg                   VARCHAR2(3000);
    l_err_code                  NUMBER;
    l_err_callpoint             VARCHAR2(75);
    l_err_callfrom              VARCHAR2(75);
    l_sec                       VARCHAR2(500);
    l_message                   VARCHAR2(75);
    l_value_1                   VARCHAR2(30);
    l_value_2                   VARCHAR2(30);
    l_value_3                   hz_parties.customer_key%type;    
    l_trx_number                VARCHAR2(30);
    l_counter                   NUMBER := 0;
    l_req_id                    NUMBER := FND_GLOBAL.CONC_REQUEST_ID;
    l_distro_list               VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
    xml_layout                  boolean;
  BEGIN
  
    errbuf  := NULL;
    retcode := 0;
  
    SAVEPOINT create_deduction_document;
    RETCODE := 0;
  
    FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                      '*======================================================================================================*');
    FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                      'Execution Starts On: ' ||
                      to_char(sysdate, 'MM-DD-YYYY HH24:MI:SS'));
    FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                      '*------------------------------------------------------------------------------------------------------*');
    FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                      'Request Id                 : ' ||
                      FND_GLOBAL.CONC_REQUEST_ID);
    FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                      '-------------------------------------------------------------------------------------------------------*');
  
    fnd_file.put_line(fnd_file.LOG, 'CHECK0');
    fnd_file.put_line(fnd_file.LOG, 'trx number' || p_trx_number);
    fnd_file.put_line(fnd_file.LOG, 'LOB ID' || p_lob_id);
    
OPEN chk_trsfr_lob_flg(p_trx_number,
                   p_lob_id);
                                 
               LOOP
                  FETCH chk_trsfr_lob_flg
                     INTO l_trx_number;
                     
                EXIT WHEN chk_trsfr_lob_flg%NOTFOUND;
END LOOP;

IF l_trx_number IS NOT NULL THEN
  
    IF p_trx_number IS NULL THEN
      l_value_1 := NULL;
    ELSE
      l_value_1 := p_trx_number;
    END IF;
  
    IF p_lob_id IS NULL THEN
      l_value_2 := NULL;
    ELSE
      l_value_2 := p_lob_id;
    END IF;
    --
    -- Derive the lob name so we can rename the output file to include the lob name for ease of identification
    -- 
     begin 
      select customer_key
      into   l_value_3
      from   hz_parties
      where  1 =1
        and  party_id   =p_lob_id; 
     exception
      when no_data_found then
       l_value_3 :=Null;
      when others then
       l_value_3 :=Null; 
     end;    
    --  
    xml_layout := FND_REQUEST.ADD_LAYOUT('XXCUS',
                                         'XXCUSOZFARINV',
                                         'en',
                                         'US',
                                         'PDF');
    fnd_file.put_line(fnd_file.LOG, 'CHECK3');
    IF xml_layout THEN
    
      fnd_file.put_line(fnd_file.LOG, 'CHECK4');
    
      v_request_id := Fnd_Request.Submit_Request(Application => 'XXCUS',
                                                 Program     => 'XXCUSOZFARINV',
                                                 Description => NULL,
                                                 Sub_Request => FALSE,
                                                 Argument1   => l_value_1,
                                                 Argument2   => l_value_2,
                                                 Argument3   => 'SINGLE',
                                                 Argument4   => 'REBATE_DEDUCTION');
    
      COMMIT;
      fnd_file.put_line(fnd_file.LOG, 'CHECK5');
      fnd_file.put_line(fnd_file.LOG,
                        'Concurrent Request Submitted Successfully:' ||
                        v_request_id);
    ELSE
    
      fnd_file.put_line(fnd_file.LOG, 'XML Layout Failed');
    END IF;
  
    IF v_request_id = 0 THEN
      fnd_file.put_line(fnd_file.LOG,
                        'Request Not Submitted due to "' || fnd_message.get || '".');
    END IF;
  
    IF v_request_id IS NOT NULL THEN
      --Wait for the request to complete successfully---
      v_request_status := fnd_concurrent.wait_for_request(request_id => v_request_id,
                                                          interval   => 0,
                                                          max_wait   => 0,
                                                          phase      => v_phase,
                                                          status     => v_wait_status,
                                                          dev_phase  => v_dev_phase,
                                                          dev_status => v_dev_status,
                                                          message    => v_message);
      v_dev_phase      := NULL;
      v_dev_status     := NULL;
    
    END IF;
  
    IF v_request_status THEN
      --Submit the Program to send the pdf as email ---
      v_request_id_1 := Fnd_Request.Submit_Request(
                                                   Application => 'XXCUS',
                                                   Program     => 'XXCUSOZFEMAIL',
                                                   Description => NULL,
                                                   Sub_Request => FALSE,
                                                   Argument1   => v_request_id,
                                                   Argument2   => p_email_address,
                                                   Argument3   => TO_CHAR(SYSDATE, 'DDMONYYYYHH24MISS'),
                                                   Argument4   => nvl(l_value_3, 'NA'), 
                                                   Argument5   => p_cc_email_id,
                                                   Argument6   => 'XXCUSOZFARINV'
                                                  );
      COMMIT;
    
    END IF;
    --l_counter := l_counter+1;
    --END IF;
    UPDATE ra_customer_trx_all rcta
       SET ATTRIBUTE11 = 'Y'
     WHERE 1 =1
      AND  rcta.customer_trx_id = nvl(p_trx_number, rcta.customer_trx_id)
      AND  rcta.attribute1 = nvl(p_lob_id, rcta.attribute1);
    --
    fnd_file.put_line(fnd_file.output,'Number of Deductions Printed :' ||SQL%ROWCOUNT);
    fnd_file.put_line(fnd_file.output, '');    
    --
    fnd_file.put_line(fnd_file.output,'List of deductions printed:');
    fnd_file.put_line(fnd_file.output,'===========================');
    --
    for c_deduction_processed in (select 
                                         rcta.trx_number
                                    from ra_customer_trx_all rcta
                                   where 1 =1
                                     and rcta.customer_trx_id =nvl(p_trx_number, rcta.customer_trx_id)
                                     and rcta.attribute1      =nvl(p_lob_id, rcta.attribute1)
                                     and attribute11          ='Y'
                                 ) 
    loop
      l_counter := l_counter + 1;
      fnd_file.put_line(fnd_file.output,'#' || l_counter || ' ' ||c_deduction_processed.trx_number);
    end loop;
    --
    END IF;
    --
  EXCEPTION
    WHEN fnd_api.g_exc_error THEN
      l_err_code := 2;
      l_err_msg  := l_sec;
      l_err_msg  := l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000);
    
      fnd_file.put_line(fnd_file.LOG, 'l_err_msg' || l_err_msg);
      retcode         := l_err_code;
      errbuf          := l_err_msg;
      l_err_callpoint := l_message;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom,
                                           p_calling           => l_err_callpoint,
                                           p_request_id        => l_req_id,
                                           p_ora_error_msg     => SQLERRM,
                                           p_error_desc        => 'Error Running XXCUSOZF_UTIL_PKG.create_deduction_document',
                                           p_distribution_list => l_distro_list,
                                           p_module            => 'TM');
    
      ROLLBACK TO create_deduction_document;
      retcode := 1;
    WHEN fnd_api.g_exc_unexpected_error THEN
      l_err_code := 2;
      l_err_msg  := l_sec;
      l_err_msg  := l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000);
    
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode         := l_err_code;
      errbuf          := l_err_msg;
      l_err_callpoint := l_message;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom,
                                           p_calling           => l_err_callpoint,
                                           p_request_id        => l_req_id,
                                           p_ora_error_msg     => SQLERRM,
                                           p_error_desc        => 'Error Running XXCUSOZF_UTIL_PKG.create_deduction_document package with PROGRAM ERROR',
                                           p_distribution_list => l_distro_list,
                                           p_module            => 'TM');
    
      ROLLBACK TO create_deduction_documents;
      retcode := 1;
    WHEN OTHERS THEN
      l_err_code := 2;
      l_err_msg  := l_sec;
      l_err_msg  := l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000);
    
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode         := l_err_code;
      errbuf          := l_err_msg;
      l_err_callpoint := l_message;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom,
                                           p_calling           => l_err_callpoint,
                                           p_request_id        => l_req_id,
                                           p_ora_error_msg     => SQLERRM,
                                           p_error_desc        => 'Error Running XXCUSOZF_UTIL_PKG.create_deduction_document package with PROGRAM ERROR',
                                           p_distribution_list => l_distro_list,
                                           p_module            => 'TM');
    
      ROLLBACK TO create_deduction_document;
      retcode := 1;
    
  END create_deduction_documents;
  -- ============================================================ 
  function deduction_exists
   (
     p_lob_id in number
   ) return boolean is
   --
    n_party_id number;
   --
  begin 
   --
    select party_id
    into   n_party_id
    from   hz_parties
    where  1 =1
     and  party_type ='ORGANIZATION'
     and  status     ='A'
     and  attribute1 ='HDS_BU'
     and  party_id   =nvl(p_lob_id, party_id)
     and  exists
      (
        select /*+ PARALLEL (rct 8) PARALLEL (rctt 8) */ rct.attribute1
        from RA_CUSTOMER_TRX_ALL rct
            ,RA_CUST_TRX_TYPES_ALL rctt
        where 1 =1
          and rct.org_id in (101, 102)
          and rctt.name ='REBATE_DEDUCTION'
          and rctt.cust_trx_type_id =rct.cust_trx_type_id  ---deduction payments
          and nvl(rct.attribute11, 'N') <> 'Y' --deduction invoices not printed
          and rct.attribute1 =to_char(party_id)
      ); 
   --
   return TRUE; 
   --what this means is we have got this far and found some parties if for all LOB's or a particular LOB with deduction invoices pending printing  
   --
  exception
   when no_data_found then
    print_log('No data fetched from function deduction_exists, message ='||sqlerrm); 
    return FALSE;   
   when others then
    print_log('Issue in function deduction_exists, message ='||sqlerrm);
    return FALSE;    
  end deduction_exists;
  -- 
  PROCEDURE create_lob_deduction_docs
                                     (
                                      retcode         OUT VARCHAR2,
                                      errbuf          OUT VARCHAR2,
                                      p_lob_id        IN NUMBER,
                                      p_email_address IN VARCHAR2,
                                      p_cc_email_id   IN VARCHAR2,
                                      p_parent_req_id IN NUMBER
                                     ) is
  
    lv_req_id NUMBER(30) := 0;
    vflag2    BOOLEAN;
    --
    vreqphase                   VARCHAR2(50);
    vreqstatus                  VARCHAR2(50);
    vreqdevphase                VARCHAR2(50);
    vreqdevstatus               VARCHAR2(50);
    vreqmessage                 VARCHAR2(50);
    fhandle                     UTL_FILE.file_type;
    vtextout                    VARCHAR2(32000);
    text                        VARCHAR2(32000);
    v_file                      VARCHAR2(500);
    v_request_id                NUMBER :=0;
    v_request_id_1              NUMBER :=0;
    v_request_status            BOOLEAN;
    --
    v_phase                     VARCHAR2(2000);
    v_wait_status               VARCHAR2(2000);
    v_dev_phase                 VARCHAR2(2000);
    v_dev_status                VARCHAR2(2000);
    v_message                   VARCHAR2(2000);
    v_application_id            NUMBER;
    v_concurrent_program_id     NUMBER;
    v_conc_prog_short_name      VARCHAR2(100);
    v_conc_prog_appl_short_name VARCHAR2(100);
    v_output_file_path          VARCHAR2(200);
    l_transfr_lob               VARCHAR2(3);
    l_err_msg                   VARCHAR2(3000);
    --
    l_err_code                  NUMBER;
    l_err_callpoint             VARCHAR2(75);
    l_err_callfrom              VARCHAR2(75);
    l_sec                       VARCHAR2(500);
    l_message                   VARCHAR2(75);
    l_value_1                   VARCHAR2(30);
    l_value_2                   VARCHAR2(30);
    l_value_3                   hz_parties.customer_key%type;
    l_trx_number                VARCHAR2(30);
    l_counter                   NUMBER := 0;
    l_req_id                    NUMBER := FND_GLOBAL.CONC_REQUEST_ID;
    l_distro_list               VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
    xml_layout                  boolean;
 --  
   lc_request_data VARCHAR2(20) :='';
   ln_request_id   NUMBER :=0;
   n_req_id NUMBER :=0;
   l_org_id NUMBER :=0; 
   n_count_child_requests NUMBER :=0; 
   N_conc_pgm_id NUMBER :=0;
   N_running_count NUMBER :=0;
   b_keep_going BOOLEAN;
   n_program_app_id NUMBER :=0;
   N_user_id NUMBER :=0;
   v_email       fnd_user.email_address%type :=Null;
   v_cp_long_name   fnd_concurrent_programs_tl.user_concurrent_program_name%type :=Null; 
   v_cp_phase       Varchar2(40) :=Null;
   v_cp_status      Varchar2(40) :=Null; 
   v_child_requests Varchar2(240) :=Null;
   output_file_id   UTL_FILE.FILE_TYPE;
   v_path varchar2(80);
   p_period_id NUMBER :=0;
   n_high_priority NUMBER :=40;
   n_low_priority NUMBER :=50;
 --
  BEGIN 
   --
   lc_request_data :=fnd_conc_global.request_data;
   --  
   if lc_request_data is null then     
    --   
       lc_request_data :='1';       
    -- 
    --
    -- When running for a LOB, we will set the individual invoice parameter [l_value_1] to blank
    --
      l_value_1 := Null;
    -- 
    -- Assign the lob id from the parameter to the variable l_value_2 before calling fnd_request
    -- 
      l_value_2 := p_lob_id;
    --
    -- Derive the lob name so we can rename the output file to include the lob name for ease of identification
    -- 
     begin 
      select customer_key
      into   l_value_3
      from   hz_parties
      where  1 =1
        and  party_id   =p_lob_id; 
     exception
      when no_data_found then
       l_value_3 :=Null;
      when others then
       l_value_3 :=Null; 
     end;    
    --
    print_log('Customer Key ='||l_value_3);
    --
--    xml_layout := FND_REQUEST.ADD_LAYOUT('XXCUS',
--                                         'XXCUSOZFARINV',
--                                         'en',
--                                         'US',
--                                         'PDF');    
    --
    xml_layout := FND_REQUEST.ADD_LAYOUT('XXCUS',
                                         'XXCUS_OZF_SINGLE_DED',
                                         'en',
                                         'US',
                                         'PDF'); 
                                         
    IF xml_layout THEN
    --                                  
            v_request_id :=fnd_request.submit_request
                  (
                   application      =>'XXCUS',
                   program          =>'XXCUS_OZF_SINGLE_DED',
                   description      =>'',
                   start_time       =>'',
                   sub_request      =>TRUE,
                   argument1        =>l_value_2, -- LOB party id
                   argument2        =>'N',       -- Reprint flag set always to N for new print document
                   argument3        =>l_req_id,   -- Current request id  [LOB program that kicked off the new format] 
                   argument4        =>'XXCUS_OZF_SINGLE_DED'   --Concurrent Program Short Code           
                  );  
              --           
              if v_request_id >0 then 
                print_log('Submitted HDS Generate and Email Deduction Statement -By LOB for customer_key ='
                                                 ||l_value_3
                                                 ||', Request Id ='
                                                 ||v_request_id
                         );                              
                 --
                  begin  
                   insert into xxcus.xxcus_ozf_wrapper_requests 
                     (
                        parent_req_id
                       ,child_req_id
                       ,argument1
                       ,argument2
                       ,argument3
                     ) 
                   values 
                     (
                       p_parent_req_id  -- LOB wrapper request id
                      ,v_request_id     -- child requests for the actual PDF output
                      ,l_value_1
                      ,l_value_2
                      ,l_value_3
                     );
                  exception
                   when others then
                    print_log('Issue in insert of xxcus.xxcus_ozf_wrapper_requests , msg ='||sqlerrm);
                  end;  
                 --                       
              else
                print_log('Failed to submit HDS Generate and Email Deduction Statement -By LOB for customer_key = '||l_value_3);
              end if;       
       
       fnd_conc_global.set_req_globals(conc_status =>'PAUSED' ,request_data =>lc_request_data);
            
       lc_request_data :=to_char(to_number(lc_request_data)+1);
    ELSE  
    --
      v_request_id :=0; 
      print_log('Failed to set the bi publisher template.');  
    --           
    --      retcode :=0;
    --      errbuf :='Child request completed. Exit HDS Generate and Email Deduction Statement -By LOB';          
    END IF;
   else
    --       
      retcode :=0;
      errbuf :='Child request completed. Exit HDS Generate and Email Deduction Statement -By LOB';
      print_log(errbuf);
    --   
   end if;      
   --
  EXCEPTION
    WHEN fnd_api.g_exc_error THEN
      l_err_code := 2;
      l_err_msg  := l_sec;
      l_err_msg  := l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000);
    
      fnd_file.put_line(fnd_file.LOG, 'l_err_msg' || l_err_msg);
      retcode         := l_err_code;
      errbuf          := l_err_msg;
      l_err_callpoint := l_message;
    
      /* 
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom,
                                           p_calling           => l_err_callpoint,
                                           p_request_id        => l_req_id,
                                           p_ora_error_msg     => SQLERRM,
                                           p_error_desc        => 'Error Running XXCUSOZF_UTIL_PKG.create_lob_deduction_docs',
                                           p_distribution_list => l_distro_list,
                                           p_module            => 'TM'); 
      */
    
      --ROLLBACK TO create_deduction_document;
      retcode := 1;
    WHEN fnd_api.g_exc_unexpected_error THEN
      l_err_code := 2;
      l_err_msg  := l_sec;
      l_err_msg  := l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000);
    
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode         := l_err_code;
      errbuf          := l_err_msg;
      l_err_callpoint := l_message;
      /*
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom,
                                           p_calling           => l_err_callpoint,
                                           p_request_id        => l_req_id,
                                           p_ora_error_msg     => SQLERRM,
                                           p_error_desc        => 'Error Running XXCUSOZF_UTIL_PKG.create_lob_deduction_docs package with PROGRAM ERROR',
                                           p_distribution_list => l_distro_list,
                                           p_module            => 'TM');
      */
      --ROLLBACK TO create_deduction_documents;
      retcode := 1;
    WHEN OTHERS THEN
      l_err_code := 2;
      l_err_msg  := l_sec;
      l_err_msg  := l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000);
    
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode         := l_err_code;
      errbuf          := l_err_msg;
      l_err_callpoint := l_message;
      /*
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom,
                                           p_calling           => l_err_callpoint,
                                           p_request_id        => l_req_id,
                                           p_ora_error_msg     => SQLERRM,
                                           p_error_desc        => 'Error Running XXCUSOZF_UTIL_PKG.create_lob_deduction_docs package with PROGRAM ERROR',
                                           p_distribution_list => l_distro_list,
                                           p_module            => 'TM');
       */
      --ROLLBACK TO create_deduction_document;
      retcode := 1;
    
  END create_lob_deduction_docs;
 --
  PROCEDURE lob_wrapper (
    retcode               OUT VARCHAR2
   ,errbuf                OUT VARCHAR2
   ,p_lob_id              IN  VARCHAR2
   ,p_email_address       IN  VARCHAR2
   ,p_cc_email_id         IN  VARCHAR2   
  ) is
    
    type lc_refcursor is ref cursor;
    my_offers             lc_refcursor; 
    REBATES_SQL           varchar2(20000); 
    
   cursor hds_lob is
   select party_id, party_name
   from   hz_parties
   where  1 =1
     and  party_type ='ORGANIZATION'
     and  status     ='A'
     and  attribute1 ='HDS_BU'
     and  party_id   =nvl(p_lob_id, party_id)
     and  exists
      (
        select /*+ PARALLEL (rct 8) PARALLEL (rctt 8) */ rct.attribute1
        from RA_CUSTOMER_TRX_ALL rct
            ,RA_CUST_TRX_TYPES_ALL rctt
        where 1 =1
          and rct.org_id in (101, 102)
          and rctt.name ='REBATE_DEDUCTION'
          and rctt.cust_trx_type_id =rct.cust_trx_type_id  ---deduction payments
          and nvl(rct.attribute11, 'N') <> 'Y' --deduction invoices not printed
          and rct.attribute1 =to_char(party_id)
          and exists
           (
             select 1
             from   ar_payment_schedules_all
             where  1 =1
               and  customer_trx_id =rct.customer_trx_id
               and  amount_due_original >0
           )
      );
 --  
   lc_request_data VARCHAR2(20) :='';
   ln_request_id   NUMBER :=0;
   n_req_id NUMBER :=0;
   l_org_id NUMBER :=0; 
   n_count_child_requests NUMBER :=0; 
   N_conc_pgm_id NUMBER :=0;
   N_running_count NUMBER :=0;
   b_keep_going BOOLEAN;
   n_program_app_id NUMBER :=0;
   N_user_id NUMBER :=0;
   v_email       fnd_user.email_address%type :=Null;
   v_cp_long_name   fnd_concurrent_programs_tl.user_concurrent_program_name%type :=Null; 
   v_cp_phase       Varchar2(40) :=Null;
   v_cp_status      Varchar2(40) :=Null; 
   v_child_requests Varchar2(240) :=Null;
   output_file_id   UTL_FILE.FILE_TYPE;
   v_path varchar2(80);
   p_period_id NUMBER :=0;
   n_high_priority NUMBER :=40;
   n_low_priority NUMBER :=50;
 --   
 begin
   lc_request_data :=fnd_conc_global.request_data;
   
   n_req_id :=fnd_global.conc_request_id;  
   
   if lc_request_data is null then     
       
       lc_request_data :='1';
  
       --l_org_id :=mo_global.get_current_org_id;    
       
       N_user_id :=fnd_global.user_id;         
       
       print_log('');
       print_log('Parameters: ');
       print_log('===========');              
       print_log('p_lob_id ='||p_lob_id);              
       print_log('p_email_address ='||p_email_address);
       print_log('p_cc_email_id ='||p_cc_email_id);
       print_log(''); 
           --    
           for rec in hds_lob loop                                    
            --                                   
            ln_request_id :=fnd_request.submit_request
                  (
                   application      =>'XXCUS',
                   program          =>'XXCUSOZF_GEN_DEDUCTION_BY_LOB',
                   description      =>'',
                   start_time       =>'',
                   sub_request      =>TRUE,
                   argument1        =>TO_CHAR(rec.party_id),
                   argument2        =>p_email_address,
                   argument3        =>p_cc_email_id,
                   argument4        =>n_req_id                                                                                                                                                                                                                                                                                 
                  ); 
                          
              if ln_request_id >0 then 
                print_log('Submitted HDS Generate and Email Deduction Statement -By LOB for '
                                                 ||rec.party_name
                                                 ||', Request Id ='
                                                 ||ln_request_id
                         );
              else
                print_log('Failed to submit HDS Generate and Email Deduction Statement -By LOB for party_id ='||rec.party_id);
              end if;     
                       
             fnd_conc_global.set_req_globals(conc_status =>'PAUSED' ,request_data =>lc_request_data);
               
             lc_request_data :=to_char(to_number(lc_request_data)+1);      
           end loop;            
           --                                 
   else     
    --
    begin 
     -- All of our requests for the actual deduction reports by LOB is stored in the xxcus.xxcus_ozf_wrapper_requests table.
      for rec in ( 
                   select child_req_id
                         ,argument3
                   from   xxcus.xxcus_ozf_wrapper_requests
                   where  1 =1
                     and  parent_req_id =n_req_id --LOB wrapper request id
                     and  exists
                      (
                        select 1
                        from   xxcus.xxcus_ozf_single_ded_hdr_all
                        where  1 =1
                          and  request_id =child_req_id
                          --and  print_flag ='N' --Incident 566538, do not activate this condition
                          and  nvl(invoice_amount, 0) >0
                      )
                 ) 
      loop 
       --
       -- Submit the Program to send the pdf as email
       --
        ln_request_id := Fnd_Request.Submit_Request(Application =>'XXCUS',
                                                   Program     => 'XXCUSOZFEMAIL',
                                                   Description => NULL,
                                                   Sub_Request => TRUE,
                                                   Argument1   => rec.child_req_id,
                                                   Argument2   => p_email_address,
                                                   Argument3   => TO_CHAR(SYSDATE, 'DDMONYYYYHH24MISS'),
                                                   Argument4   => nvl(rec.argument3, 'NA'), 
                                                   Argument5   => p_cc_email_id,
                                                   Argument6   => 'XXCUS_OZF_SINGLE_DED'                                            
                                                  );
       --
              if ln_request_id >0 then 
                print_log('Submitted HDS Email Deduction Document for customer_key ='
                                                 ||rec.argument3
                                                 ||', Request Id ='
                                                 ||ln_request_id
                         );
              else
                print_log('Failed to submit HDS Email Deduction Document for customer_key ='||rec.argument3);
              end if;       
       --                                                  
      end loop;  
     --
     begin 
      --
      delete xxcus.xxcus_ozf_wrapper_requests
      where  1 =1
        and  parent_req_id =n_req_id;
      --
        print_log('Total rows removed for wrapper request id '||n_req_id||' ='||sql%rowcount);
      --
     exception
      when others then
       print_log('Issue in delete of rows corresponding to wrapper request id ='||n_req_id||', msg ='||sqlerrm);
     end;
     --                                                           
    exception
     when others then
      print_log('Issue in select of row from xxcus.xxcus_ozf_wrapper_gtt, msg ='||sqlerrm);
    end;    
    --      
      retcode :=0;
      errbuf :='Child request[s] completed. Exit HDS Kickoff Deduction Statement -Master';
      print_log(errbuf);
    --
   end if;       
 exception
  when others then
    fnd_file.put_line(fnd_file.log, 'Error in caller Apps.XXCUSOZF_UTIL_PKG.lob_wrapper ='||sqlerrm);
    rollback;  
 end lob_wrapper; 
 --
    function get_hds_divisions
      (
        p_cust_account_id in number
       ,p_calendar_year   in varchar2
       ,p_trx_type_id     in number
       ,p_list_header_id  in number
      ) return varchar2 as
            cursor my_divisions is  
            select /*+ PARALLEL (qlhv 8) PARALLEL (hca 8) PARALLEL (orl 8) PARALLEL (rct 8) PARALLEL (ofu 8) PARALLEL (oclu 8) PARALLEL (ocl 8) PARALLEL (hp 8) */
                 orl.end_cust_party_name hds_div
            from ra_customer_trx_all rct,
                 hz_parties hp,
                 hz_cust_accounts hca,
                 ozf_claims_all oca,
                 ozf_claim_lines_all ocl,
                 ozf_claim_lines_util_all oclu,
                 ozf_funds_utilized_all_b ofu,
                 ozf_resale_lines_all orl,
                 qp_list_headers_vl qlhv,
                 ozf_offers oo,
                 ams_media_vl med     
            where 1 =1
              and rct.cust_trx_type_id    =p_trx_type_id --parameter            
              and rct.bill_to_customer_id =hca.cust_account_id
              and to_char(oca.claim_id)   =rct.interface_header_attribute2
              and oca.claim_id            =ocl.claim_id
              and ocl.claim_line_id       =oclu.claim_line_id
              and oclu.utilization_id     =ofu.utilization_id
              and ofu.object_id           =orl.resale_line_id
              and hca.party_id            =hp.party_id
              and ofu.plan_id             =p_list_header_id --parameter
              and oo.qp_list_header_id    =ofu.plan_id
              and oo.qp_list_header_id    =qlhv.list_header_id
              and oo.activity_media_id    =med.media_id
              and med.media_type_code     ='DEAL'
              and hca.cust_account_id     =p_cust_account_id --parameter
              and qlhv.attribute7         =p_calendar_year --parameter 
            group by orl.end_cust_party_name;
         --    
         v_hds_div varchar2(2000);
         --
         n_counter number :=1;
         --
         v_division apps.fnd_lookup_values.attribute4%Type :=Null;
         --
         v_prev_div_name ozf_resale_lines_all.end_cust_party_name%type;
         --
         v_flag varchar2(1) :='N';
         
    function get_attribute4 (p_end_cust_party_name in varchar2) return varchar2 is
     v_division_name apps.fnd_lookup_values.attribute4%Type :=Null;
    begin 
        SELECT /*+ RESULT_CACHE */
               attribute4
        INTO   v_division_name
        FROM   apps.fnd_lookup_values
        WHERE  1 =1
           AND lookup_type  ='XXCUS_REBATE_BU_XREF'
           AND meaning      =p_end_cust_party_name;
           --AND enabled_flag ='Y'
           --AND nvl(end_date_active, TRUNC(SYSDATE)) >= TRUNC(SYSDATE);
       return v_division_name;     
    exception
     when no_data_found then
      return Null;
     when others then 
      print_log('@get_attribute4, When others error in fetching hds_divisions for end_cust_party_name ='||p_end_cust_party_name);     
      print_log('@get_attribute4, When others error in fetching hds_divisions for end_cust_party_name, message ='||sqlerrm);      
      return ''; 
    end get_attribute4;         
 
    begin
     --
         begin 
          --
           for rec in my_divisions loop  
             --
             if v_prev_div_name is null then
              v_prev_div_name :=rec.hds_div;  
              v_flag :='Y';           
             else
              --
                  if v_prev_div_name =rec.hds_div then
                   null;
                   v_flag :='N';
                  else
                   v_prev_div_name :=rec.hds_div; 
                   v_flag :='Y';            
                  end if;
              --
             end if;
             --
             if v_flag ='Y' then
              -- 
                  if v_hds_div is null then
                   v_hds_div :=get_attribute4(rec.hds_div);
                  else
                   v_hds_div :=v_hds_div||', '||get_attribute4(rec.hds_div);
                  end if;
              --
             else
              Null;
             end if; 
             --   
           end loop;
            --
           return nvl(substr(v_hds_div, 1, 150), '');
           --
         exception
          when no_data_found then
           return Null;
          when others then
           return Null;
         end;
    exception
     when others then
      print_log('Critical error in get_hds_divisions, Please check for cust_account_id '||p_cust_account_id);
      return Null;
    end get_hds_divisions; 
 --
-- ======================================================
-- Function: beforereport
-- ESMS     ver      Date         Note
--
-- 281984   1.0     01-JU1-2015  Add error email api 
-- ====================================================== 
 function beforereport return boolean as      
 -- 
  cursor all_pending_inv 
            (
               p_trx_id         IN NUMBER
              ,p_lob_id         IN VARCHAR2
              ,p_req_id         IN NUMBER
              ,p_lob_request_id IN NUMBER
              ,p_user_id        IN NUMBER
              ,p_org_id         IN NUMBER
            ) is
        SELECT 
          hp.party_name                             mvid_name
        , replace(hca.account_number, '~MSTR', '')  mvid_number 
        , rcta.trx_number                           invoice_number
        , rcta.trx_date                             invoice_date
        , null                                      invoice_type
        , rcta.invoice_currency_code                invoice_currency
        , apsa.amount_due_original                  invoice_amount
        , to_number(null)                           balance_due
        --, sum(nvl(apsa.amount_due_remaining, 0))    balance_due
        , qph.name                                  offer_code
        , qph.description                           offer_name
        , qph.attribute7                            offer_cal_year 
        , rctl.line_number                          invoice_line_num 
        , rcta.attribute1                           lob_id
        , rcta.attribute2                           lob_name
        , rcta.attribute3                           pmt_freq_code
        , fl.meaning                                pmt_freq_desc 
        , rcta.org_id                               org_id                
        , hca.cust_account_id                       cust_account_id
        , rcta.customer_trx_id                      customer_trx_id
        , rcta.cust_trx_type_id                     cust_trx_type_id
        , rctl.customer_trx_line_id                 customer_trx_line_id
        , rctl.interface_line_attribute2            claim_id
        , rctl.interface_line_attribute3            claim_line_id
        , cld.activity_id                           list_header_id
        , p_req_id                                  request_id
        , p_lob_request_id                          lob_request_id
        , p_user_id                                 created_by
        , sysdate                                   creation_date
        --                
        FROM ra_customer_trx rcta
            ,ra_customer_trx_lines rctl
            ,ra_cust_trx_types ractt
           , hz_cust_accounts hca
           , ar_payment_schedules apsa
           , fnd_lookup_values_vl fl
           , hz_parties hp
           , ozf_claim_lines cld
           , qp_list_headers_vl qph
        --
        WHERE 1 =1
        --
        AND rcta.customer_trx_id       =nvl(p_trx_id, rcta.customer_trx_id)
        AND rcta.attribute1            =nvl(p_lob_id, rcta.attribute1) --LOB party id
        AND nvl(rcta.attribute11, 'N') ='N' 
        --
        AND rcta.bill_to_customer_id   =hca.cust_account_id
        AND hp.party_id                =hca.party_id
        AND fl.lookup_type             ='XXCUS_PAYMENT_FREQ'
        --
        AND rcta.attribute3            =fl.lookup_code
        AND rcta.customer_trx_id       =rctl.customer_trx_id
        AND ractt.cust_trx_type_id     =rcta.cust_trx_type_id
        --
        AND ractt.name                 ='REBATE_DEDUCTION'
        AND rctl.interface_line_context ='CLAIM'
        AND apsa.customer_trx_id       =rcta.customer_trx_id
        --
        AND apsa.amount_due_original   >=0 --For TYPE deduction, amount is positive indicating an invoice and not an reimbursement
        AND cld.claim_line_id(+)       =to_number(rctl.interface_line_attribute3)
        AND qph.list_header_id(+)      =cld.activity_id;
        --
--        GROUP BY 
--         hp.party_name
--        ,replace(hca.account_number, '~MSTR', '')
--        ,rcta.trx_number
--        ,rcta.trx_date
--        ,rcta.invoice_currency_code
--        ,apsa.amount_due_original
--        ,qph.name        
--        ,qph.description
--        ,qph.attribute7
--        ,rctl.line_number                                                
--        ,rcta.attribute1 
--        ,rcta.attribute2
--        ,rcta.attribute3
--        ,fl.meaning
--        ,rcta.org_id 
--        ,hca.cust_account_id
--        ,rcta.customer_trx_id
--        ,rcta.cust_trx_type_id
--        ,rctl.customer_trx_line_id
--        ,rctl.interface_line_attribute2
--        ,rctl.interface_line_attribute3
--        ,cld.activity_id; 
 --
  type print_pending_inv_tbl is table of all_pending_inv%rowtype index by binary_integer;
  print_pending_inv_rows print_pending_inv_tbl; 
 --
  cursor all_stg_inv (l_req_id in number, l_user_id in number) is
    select 
     invoice_number        invoice_number       
    ,invoice_date          invoice_date
    ,invoice_currency      currency_code
    ,invoice_amount        invoice_amount
    ,0                     ytd_rebate
    ,0                     ytd_purchases
    ,0                     ytd_collection_rate
    ,lob_name              posting_lob
    ,lob_id                lob_id
    ,mvid_number           mvid_number
    ,mvid_name             mvid_name
    ,null                  hds_divisions
    ,pmt_freq_desc         payment_frequency
    ,offer_name            offer_name            
    ,customer_trx_id       hdr_customer_trx_id
    ,offer_cal_year        calendar_year
    ,cust_account_id       cust_account_id
    ,cust_trx_type_id      cust_trx_type_id
    ,list_header_id        list_header_id    
    ,org_id                org_id
    ,'N'                   print_flag
    ,Null                  original_print_date    
    ,l_req_id              request_id
    ,l_user_id             created_by
    ,sysdate               creation_date
    ,null                  reprint_user_id
    ,null                  reprint_date
    ,0                     reprint_count      
    from  xxcus.xxcus_ozf_print_pending_inv 
    where 1 =1
      and request_id =l_req_id; 
 --
  type my_stg_inv_tbl is table of xxcus.xxcus_ozf_single_ded_hdr_all%rowtype index by binary_integer;
  my_stg_inv_rec  my_stg_inv_tbl;
  --
  type inv_hdr_rec_tbl is table of xxcus.xxcus_ozf_print_pending_inv%rowtype index by binary_integer;  
  inv_hdr_rec     inv_hdr_rec_tbl;         
 --
     cursor hds_header is
        select /*+ PARALLEL (hzca 8) PARALLEL (hzp 8) */
            replace(account_number, '~MSTR', '') mv_number
           ,hzp.party_name vendor_name
           ,hzca.cust_account_id cust_account_id
           ,hzp.party_id
        from  hz_cust_accounts hzca
             ,hz_parties hzp
        --
        where  1 =1
        --
        and  hzca.cust_account_id =nvl(xxozf_print_ded_stmnt_pkg.p_mvid, hzca.cust_account_id)
        and  hzca.attribute1      ='HDS_MVID'
        and  hzp.party_id         =hzca.party_id
        --
        and  exists
             (
               select /*+ PARALLEL (trx 8) */
                      1
               from   ra_customer_trx trx
               where  1 =1
                 and  trx.cust_trx_type_id          =xxozf_print_ded_stmnt_pkg.g_trx_type_id
                 and  trx.bill_to_customer_id       =hzca.cust_account_id
                 and  trx.interface_header_context  ='CLAIM'
                 and  exists                 
                  (
                    select /*+ PARALLEL (a 8) PARALLEL (b 8) */
                           '1'
                     from ozf_claims_all clh,
                          ozf_claim_lines_all cld,
                          qp_list_headers_all qph
                    where     1 = 1                          
                          and clh.claim_id       =to_number(trx.interface_header_attribute2)
                          and cld.claim_id       =clh.claim_id
                          and cld.claim_line_id  =to_number(trx.interface_header_attribute3)
                          and cld.activity_id    =xxozf_print_ded_stmnt_pkg.p_agreement_id
                          and qph.list_header_id =cld.activity_id
                          and qph.attribute7     =xxozf_print_ded_stmnt_pkg.p_calendar_year                   
                  )
             );     
     --     
     type my_hds_hdr_tbl is table of hds_header%rowtype index by binary_integer; 
     my_hds_hdr_rec my_hds_hdr_tbl;
     --
     v_hds_divisions varchar2(240) :=Null;
     --
     cursor get_plan_id
        (
          p_customer_id   in number
         ,p_year          in varchar2
         ,p_rebate_type   in varchar2
         ,p_agreement_id  in number
        ) is
     select agreement_id
           ,agreement_desc
           ,agreement_code
           ,agreement_pay_frequency
           ,rebate_type
     --
     from   apps.xxcus_calyear_claim_agreements
     --
     where  1 =1
       and  cust_account_id =p_customer_id
       and  calendar_year   =xxozf_print_ded_stmnt_pkg.p_calendar_year
       and  rebate_type     =nvl(xxozf_print_ded_stmnt_pkg.p_rebate_type, rebate_type)
       and  agreement_id    =nvl(p_agreement_id, agreement_id); --     
     --
     n_trx_type_id number :=0;
     p_limit number :=5000;
     --
     cursor hds_deductions 
             (
               l_trx_type_id    in number
              ,l_customer_id    in number
              ,l_cal_year       in varchar2
              ,l_list_header_id in number
              ,l_lob_id         IN VARCHAR2
             ) is
    select min(start_receipt_date)  start_date,
           max(end_receipt_date)    end_date,
--           lob_party_id,
           rebate_type,
           deduction_date invoice_date,
           --to_char(add_months(deduction_date, -1), 'Mon-YYYY') timeframe, --Commented by Ashwin.S on 26-Jul-2018 for TMS#20180507-00352  
           to_char(add_months(deduction_date+7, -1), 'Mon-YYYY') timeframe, --Added by Ashwin.S on 26-Jul-2018 for TMS#20180507-00352   
           ded_print_flag inv_print_flag,
           case
            when ded_print_flag ='N' then 'Y'
            else 'N'
           end inv_display_flag,
           deduction_id customer_trx_id,
           deduction_num invoice_num,
           calendar_year,
           replace(mvid_num, '~MSTR', '') mvid_num,
           mvid,
           cust_account_id,
           inv_cal_year, --ESMS 281984
           ded_amount invoice_amount,
           --round(sum(purchases), 2) purchases    --Commented by Ashwin.S on 25-Jul-2018 for TMS#20180927-00004
           round(max(purchases), 2) purchases      --Added by Ashwin.S on 25-Jul-2018 for TMS#20180927-00004
    from 
     (
           select /*+ PARALLEL (aps 8) PARALLEL (qlhv 8) PARALLEL (hca 8) PARALLEL (orl 8) PARALLEL (rct 8) PARALLEL (ofu 8) PARALLEL (oclu 8) PARALLEL (ocl 8) PARALLEL (hp 8) */
           orl.date_ordered                                start_receipt_date,
           orl.date_ordered                                end_receipt_date,
           trunc(rct.trx_date)                             deduction_date,
           case
            when upper(med.description) 
             IN ('COOP', 'COOP_MIN') 
              then 'COOP'
            else 'REBATE'
           end                                             rebate_type,
           --
           rct.attribute1                                  lob_party_id,
           nvl(rct.attribute11, 'N')                       ded_print_flag,                                        
           rct.trx_number                                  deduction_num,
           rct.customer_trx_id                             deduction_id,
           qlhv.attribute7                                 calendar_year,
           qlhv.description                                agreement,
           hp.party_number                                 mvid_num,
           hp.party_name                                   mvid, 
           hca.cust_account_id                             cust_account_id,
           nvl(aps.amount_due_original, 0)                 ded_amount,
           -- Begin ESMS 281984 
           (
             select glp.period_year
             from   gl_periods glp
             where  1 =1
               and  aps.gl_date between glp.start_date and glp.end_date
               and  adjustment_period_flag ='N'
             
           )                                               inv_cal_year,
           -- End ESMS 281984
         /*  (case 
              when ofu.utilization_type = 'ACCRUAL' then
                nvl(orl.selling_price*orl.quantity, 0)
             else
                0
             end )  */ --Commented the CASE statement by Ashwin.S on 25-Jul-2018 for TMS#20180927-00004
           (case 
              when OO.OFFER_TYPE in ( 'ACCRUAL','VOLUME_OFFER') then
                (SELECT SUM(NVL(ORL1.SELLING_PRICE*ORL1.QUANTITY, 0))
                 FROM OZF_RESALE_LINES_ALL ORL1
                 ,xxcus.xxcus_pam_bu_name_tbl xpbn
                 WHERE 1 =1
                 AND ORL1.SOLD_FROM_CUST_ACCOUNT_ID =l_customer_id
                 AND ORL1.DATE_ORDERED BETWEEN TO_DATE('01-JAN-'||l_cal_year) and TO_DATE('31-DEC-'||l_cal_year)
                 AND ORL1.LINE_ATTRIBUTE2 = xpbn.BU_NAME
                 AND xpbn.bu_id=l_lob_id) --Added the CASE statement by Ashwin.S on 25-Jul-2018
             else
                0
             end ) purchases
    from ra_customer_trx_all rct,
         hz_parties hp,
         hz_cust_accounts hca,
         ar_payment_schedules_all aps,
         ozf_claims_all oca,
         ozf_claim_lines_all ocl,
         ozf_claim_lines_util_all oclu,
         ozf_funds_utilized_all_b ofu,
         ozf_resale_lines_all orl,
         qp_list_headers_vl qlhv,
         ozf_offers oo,
         ams_media_vl med 
    --    
    where 1 =1
      --
      and rct.bill_to_customer_id   =l_customer_id --2594 --2034 --
      and qlhv.attribute7           =l_cal_year --2014
      -- 
      and rct.cust_trx_type_id     =l_trx_type_id
      and rct.bill_to_customer_id  =hca.cust_account_id
      and aps.customer_trx_id      =rct.customer_trx_id
      and aps.class                ='INV'
      and aps.amount_due_original  >0 --Positive Deductions Only
      --
      and to_char(oca.claim_id)    =rct.interface_header_attribute2
      and oca.claim_id             =ocl.claim_id
      and ocl.claim_line_id        =oclu.claim_line_id
      --
      and oclu.utilization_id      =ofu.utilization_id
      and ofu.object_id            =orl.resale_line_id
      and hca.party_id             =hp.party_id
      and ofu.plan_id              =l_list_header_id --907346 --907108
      --
      and oo.qp_list_header_id     =ofu.plan_id
      and oo.qp_list_header_id     =qlhv.list_header_id
      and oo.activity_media_id     =med.media_id
      and med.media_type_code      ='DEAL'
      --
     )
      where 1=1
      group by 
           deduction_id,
           deduction_num,
           deduction_date,
           rebate_type,
--           lob_party_id,           
           ded_print_flag,           
           calendar_year,
           mvid_num,
           mvid,
           cust_account_id,
           inv_cal_year, --ESMS 281984
           ded_amount
      order by
       case
        when ded_print_flag ='N' then 1
        else 2
       end asc,
       to_number(to_char(add_months(deduction_date, -1), 'MMYYYY')) asc;
     --
     type lines_tbl is table of hds_deductions%rowtype index by binary_integer;
     lines_rec lines_tbl;
     -- 
     type dtl_tbl is table of xxcus.xxcus_ozf_single_ded_dtl_all%rowtype index by binary_integer;
     dtl_rec dtl_tbl;
     --        
     -- ========================
      b_move_fwd         boolean :=FALSE;
      b_success          boolean :=FALSE;
      v_detail_exists    varchar2(1) :=Null;
      n_header_no        number :=0;
      --
      n_ytd_collection_rate number :=0;
      n_ytd_purchases    number :=0;
      n_ytd_rebates      number :=0;
      n_req_id           number :=fnd_global.conc_request_id;
      --
      n_line             number :=0;
      n_collection_rate  number :=0;
      n_user_id          number :=fnd_global.user_id;
      -- Begin  ver 1.0
      l_err_callfrom  VARCHAR2(75) DEFAULT 'XXCUSOZF_UTIL_PKG';
      l_err_callpoint VARCHAR2(75) DEFAULT 'Before Report';
      l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
      l_err_msg       VARCHAR2(3000);
      l_err_code      NUMBER;
      l_sec           VARCHAR2(255);      
      -- End  ver 1.0      
     -- ========================  
  BEGIN
   --
    begin
     --
     savepoint square1; 
     --
     print_log('Before delete of xxcus.xxcus_ozf_print_pending_inv for lob_id ='||xxcusozf_util_pkg.g_lob_id);
     --
     delete xxcus.xxcus_ozf_print_pending_inv 
     where  1 =1
       and  lob_id =xxcusozf_util_pkg.g_lob_id;
     --
     b_success :=TRUE;
     --
     commit;
     -- 
    exception
     when others then
      print_log(
                 '@Delete of staging table xxcus.xxcus_ozf_print_pending_inv data for lob_id '
                ||xxcusozf_util_pkg.g_lob_id
                ||', error ='
                ||sqlerrm
               );
      rollback to square1;
      b_success :=FALSE;
    end;
    --
    if (b_success) then 
        --
        begin                      
          --          
          for bu_rec in (select organization_id org_id, name from hr_operating_units where organization_id in (101, 102)) loop
           --
           print_log('Org_Id =>'||bu_rec.org_id||', Name =>'||bu_rec.name);
           --
           mo_global.set_policy_context('S', bu_rec.org_id);
           --
--            print_log(' ');
--            print_log('p_trx_id         =>'||xxcusozf_util_pkg.g_trx_id);
--            print_log('p_lob_id         =>'||xxcusozf_util_pkg.g_lob_id);
--            print_log('p_req_id         =>'||fnd_global.conc_request_id);
--            print_log('p_lob_request_id =>'||nvl(xxcusozf_util_pkg.g_lob_req_id, 0));
--            print_log('p_user_id        =>'||fnd_global.user_id);
--            print_log('p_org_id         =>'||mo_global.get_current_org_id);
--            print_log(' '); 
--           --           
              open all_pending_inv 
              (
                 p_trx_id         =>xxcusozf_util_pkg.g_trx_id
                ,p_lob_id         =>xxcusozf_util_pkg.g_lob_id
                ,p_req_id         =>fnd_global.conc_request_id
                ,p_lob_request_id =>nvl(xxcusozf_util_pkg.g_lob_req_id, 0)
                ,p_user_id        =>fnd_global.user_id
                ,p_org_id         =>mo_global.get_current_org_id
              );
             --
              fetch all_pending_inv bulk collect into print_pending_inv_rows;
             --
              close all_pending_inv;
             --
              print_log('print_pending_inv_rows.count =>'||print_pending_inv_rows.count||', for org_id ='||bu_rec.org_id||',current lob_id ='||xxcusozf_util_pkg.g_lob_id);
              print_log(' ');             
             --
            if (print_pending_inv_rows.count >0) then
            --
             begin  
              forall indx in print_pending_inv_rows.first .. print_pending_inv_rows.last 
               insert into xxcus.xxcus_ozf_print_pending_inv values print_pending_inv_rows(indx);
             exception
              when others then
               print_log('@Bulk insert of print_pending_inv to the staging table, lob_id ='||xxcusozf_util_pkg.g_lob_id);
                print_log('@Bulk insert of print_pending_inv to the staging table, error ='||sqlerrm);                      
             end;
            --
            else
             Null;             
            end if;
            --                                   
          end loop; --loop bu_rec
          --
        exception  
         when others then
          print_log('@cursor all_pending_inv, error ='||sqlerrm);
          b_move_fwd :=FALSE; 
        end;
        --    
    else
     print_log('Variable b_success is FALSE, exit now');    
    end if;
    --
    --COMMIT; --First Commit -At this stage we will save all our staging data before populating the reporting tables
    --
    begin 
     --
     open all_stg_inv (l_req_id =>n_req_id, l_user_id =>fnd_global.user_id);
     fetch all_stg_inv bulk collect into my_stg_inv_rec; 
     close all_stg_inv;   
     --
     b_success :=TRUE;
     -- 
    exception
     when no_data_found then
      b_success :=FALSE;    
     when others then
      print_log ('@cursor all_stg_inv, error ='||sqlerrm);
      b_success :=FALSE;
    end;  
    --
    if (b_success) then 
    --
     for idx in 1 .. my_stg_inv_rec.count loop
       --
       mo_global.set_policy_context('S', my_stg_inv_rec(idx).org_id);
       --     
       my_stg_inv_rec(idx).hds_divisions 
                        := xxcusozf_util_pkg.get_hds_divisions
                           (
                              p_cust_account_id =>my_stg_inv_rec(idx).cust_account_id
                             ,p_calendar_year   =>my_stg_inv_rec(idx).calendar_year
                             ,p_trx_type_id     =>my_stg_inv_rec(idx).cust_trx_type_id
                             ,p_list_header_id  =>my_stg_inv_rec(idx).list_header_id
                           );
       --                           
     end loop;
    --
     begin  
      forall indx in my_stg_inv_rec.first .. my_stg_inv_rec.last 
       insert into xxcus.xxcus_ozf_single_ded_hdr_all values my_stg_inv_rec(indx);
     exception
      when others then
       print_log('@Bulk insert of print_pending_inv to the staging table, lob_id ='||xxcusozf_util_pkg.g_lob_id);
        print_log('@Bulk insert of print_pending_inv to the staging table, error ='||sqlerrm);                      
     end;
     --
    end if;
    --
    b_success :=Null;
    --
    begin  
     --
     select * 
     bulk collect into inv_hdr_rec
     from xxcus.xxcus_ozf_print_pending_inv 
     where 1 =1
       and request_id =n_req_id;
     --
    exception
     when no_data_found then
      print_log ('@select inv_hdr_rec fetch, no data found');
      null;
     when others then
      print_log ('@select inv_hdr_rec fetch, error ='||sqlerrm);
    end;
    --
    print_log ('inv_hdr_rec.count ='||inv_hdr_rec.count);    
    --
    if inv_hdr_rec.count >0 then
     --
     for idx1 in 1 .. inv_hdr_rec.count loop
      --
         dtl_rec.delete; --plsql record for the resting detail table
         --
         lines_rec.delete; --working plsql table
      --
         begin
           --
           mo_global.set_policy_context('S', inv_hdr_rec(idx1).org_id);
           --
            print_log(' ');
            print_log('l_trx_type_id    =>'||inv_hdr_rec(idx1).cust_trx_type_id);
            print_log('l_customer_id    =>'||inv_hdr_rec(idx1).cust_account_id);
            print_log('l_cal_year       =>'||inv_hdr_rec(idx1).offer_cal_year);
            print_log('l_list_header_id =>'||nvl(inv_hdr_rec(idx1).list_header_id, 0));
            print_log(' '); 
           --           
           open hds_deductions 
                 (
                   l_trx_type_id    =>inv_hdr_rec(idx1).cust_trx_type_id --in number
                  ,l_customer_id    =>inv_hdr_rec(idx1).cust_account_id  --in number
                  ,l_cal_year       =>inv_hdr_rec(idx1).offer_cal_year   --in varchar2
                  ,l_list_header_id =>inv_hdr_rec(idx1).list_header_id   --in number
                  ,l_lob_id         =>inv_hdr_rec(idx1).lob_id           --in varchar2 , bu_id --Added by Ashwin.S on 25-Jul-2018
                 );
           fetch hds_deductions bulk collect into lines_rec; 
           close hds_deductions;
          --
         exception
          when no_data_found then
           b_success :=FALSE;
           print_log ('No Data Found, lines_rec.count ='||lines_rec.count);
          when others then      
           print_log
            (
                'Failed to fetch detail records for customer_id ='
              ||inv_hdr_rec(idx1).cust_account_id
              ||', calendar year'
              ||inv_hdr_rec(idx1).offer_cal_year
              ||', cust_trx_type_id ='
              ||inv_hdr_rec(idx1).cust_trx_type_id 
              ||', list_header_id ='
              ||inv_hdr_rec(idx1).list_header_id
            );      
           print_log('Failed to fetch detail records, error: '||sqlerrm);
         end;      
      --
          print_log ('lines_rec.count ='||lines_rec.count);
      --      
         if lines_rec.count >0 then
          --
          n_line :=0;
          n_collection_rate :=0;
          --
          n_ytd_purchases :=0;
          n_ytd_rebates :=0;          
          n_ytd_collection_rate :=0;
          --
          for idx2 in 1 .. lines_rec.count loop 
           --
           n_line :=n_line +1;
           --
           if nvl(lines_rec(idx2).purchases, 0) =0 then
            n_collection_rate :=0;
           elsif (((lines_rec(idx2).invoice_amount / lines_rec(idx2).purchases)*100) = 0) then
            n_collection_rate :=0;
           else
            n_collection_rate :=(lines_rec(idx2).invoice_amount / lines_rec(idx2).purchases);            
           end if;
           --
           n_ytd_purchases :=n_ytd_purchases + lines_rec(idx2).purchases;
           --
           n_ytd_rebates   :=n_ytd_rebates + lines_rec(idx2).invoice_amount;
           -- Begin ESMS 281984  
           if (inv_hdr_rec(idx1).offer_cal_year =lines_rec(idx2).inv_cal_year) then 
            Null;
           else
            lines_rec(idx2).timeframe :=inv_hdr_rec(idx1).offer_cal_year; --We are printing prior fiscal year only as timeframe
           end if;
           -- End ESMS 281984         
            dtl_rec(n_line).invoice_num         :=lines_rec(idx2).invoice_num;       -- VARCHAR2(30)
            dtl_rec(n_line).invoice_date        :=lines_rec(idx2).invoice_date;      -- DATE
            dtl_rec(n_line).start_date          :=lines_rec(idx2).start_date;        -- DATE
            dtl_rec(n_line).end_date            :=lines_rec(idx2).end_date;          -- DATE
            dtl_rec(n_line).rebate_type         :=lines_rec(idx2).rebate_type;       -- VARCHAR2(60)
            dtl_rec(n_line).timeframe           :=lines_rec(idx2).timeframe;         -- VARCHAR2(10)
            dtl_rec(n_line).rebate_amount       :=lines_rec(idx2).invoice_amount;    -- NUMBER
            dtl_rec(n_line).purchase_amount     :=lines_rec(idx2).purchases;         -- NUMBER
            dtl_rec(n_line).collection_rate     :=n_collection_rate;                 -- NUMBER
            dtl_rec(n_line).mvid_number         :=lines_rec(idx2).mvid_num;          -- VARCHAR2(30)
            dtl_rec(n_line).mvid_name           :=lines_rec(idx2).mvid;              -- VARCHAR2(150)
            dtl_rec(n_line).display_flag        :=lines_rec(idx2).inv_display_flag;  -- VARCHAR2(1)
            dtl_rec(n_line).hdr_customer_trx_id :=inv_hdr_rec(idx1).customer_trx_id; -- NUMBER
            dtl_rec(n_line).customer_trx_id     :=lines_rec(idx2).customer_trx_id;   -- NUMBER
            dtl_rec(n_line).org_id              :=inv_hdr_rec(idx1).org_id;          -- NUMBER
            dtl_rec(n_line).request_id          :=n_req_id;                          -- NUMBER
            dtl_rec(n_line).created_by          :=n_user_id;                         -- NUMBER
            dtl_rec(n_line).creation_date       :=sysdate;                           -- DATE
           --
          end loop; --idx2 loop
          --
          if (dtl_rec.count >0) then 
            --
             begin  
              forall indx in dtl_rec.first .. dtl_rec.last 
               insert into xxcus.xxcus_ozf_single_ded_dtl_all values dtl_rec(indx);
               commit;
             exception
              when others then
               print_log('@Bulk insert of dtl_rec plsql table to the table xxcus_ozf_single_ded_dtl_all, customer_trx_id ='||inv_hdr_rec(idx1).customer_trx_id);
                print_log('@Bulk insert of dtl_rec plsql table to the table xxcus_ozf_single_ded_dtl_all, error ='||sqlerrm);                      
             end;
             --
          else
           print_log('PLSQL table dtl_rec.count =0 for customer_trx_id ='||inv_hdr_rec(idx1).customer_trx_id);
          end if;
            --          
           if nvl(n_ytd_purchases, 0) =0 then
            n_ytd_collection_rate :=0;
           elsif (((n_ytd_rebates / n_ytd_purchases)*100) = 0) then
            n_ytd_collection_rate :=0;
           else
            n_ytd_collection_rate :=(n_ytd_rebates / n_ytd_purchases);            
           end if;
           -- 
           begin
            --
            savepoint init_here;
            --
            update xxcus.xxcus_ozf_single_ded_hdr_all
               set ytd_rebate           =n_ytd_rebates
                  ,ytd_purchases        =n_ytd_purchases
                  ,ytd_collection_rate  =n_ytd_collection_rate
              where 1 =1
                and hdr_customer_trx_id =inv_hdr_rec(idx1).customer_trx_id;
            --
            commit;
            --
           exception
            when others then
             print_log 
              (
                'Issue in updating header table for ytd data, customer_trx_id '||inv_hdr_rec(idx1).customer_trx_id
                ||', error ='||sqlerrm
              );
             rollback to init_here;             
           end;
           --        
         end if; --dtl_rec.count >0
      --
     end loop; --idx1 loop
     --
    end if;
    --
    RETURN TRUE;
    --
  EXCEPTION
   --
   WHEN OTHERS THEN
    print_log('Error in beforereport '||sqlerrm);
      l_err_msg  := '@xxcusozf_util_pkg.before report: ' ||
                    ' Error_Stack...' || dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace();
      l_err_callpoint := l_sec;    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'Rebates');    
    RETURN FALSE;
   --
  END beforereport;
 --
-- ======================================================
-- Function: afterreport
-- ESMS     ver      Date         Note
--
-- 281984   1.0     01-JU1-2015  Add error email api 
-- ======================================================  
  FUNCTION afterreport RETURN boolean AS  
    -- 
    l_value_3    VARCHAR2(30);
    l_request_id NUMBER :=0;
    -- Begin  ver 1.0
      l_err_callfrom  VARCHAR2(75) DEFAULT 'XXCUSOZF_UTIL_PKG';
      l_err_callpoint VARCHAR2(75) DEFAULT 'After Report';
      l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
      l_err_msg       VARCHAR2(3000);
      l_err_code      NUMBER;
      l_sec           VARCHAR2(255);      
    -- End  ver 1.0       
  BEGIN 
   --
   if xxcusozf_util_pkg.g_reprint_flag ='N' then
    --
    begin  
     --
     savepoint start1;
     --
     update ra_customer_trx_all a
        set a.attribute11 ='Y'
      where 1 =1
        and a.customer_trx_id IN
         (
          select hdr_customer_trx_id
          from   xxcus.xxcus_ozf_single_ded_hdr_all
          where  1 =1
            and  request_id =fnd_global.conc_request_id
            and  print_flag ='N'
         );
     --
    exception
     when others then
      rollback to start1;
    end;
    --
    begin 
     --
     savepoint start2;
     --
     update xxcus.xxcus_ozf_single_ded_hdr_all
        set print_flag ='Y'
           ,original_print_date =sysdate
     where 1 =1
        and request_id =fnd_global.conc_request_id;     
     --
    exception
     when others then
      rollback to start2;
    end;
    --    
   elsif xxcusozf_util_pkg.g_reprint_flag ='Y' then
    --
     begin  
       --
       savepoint start3;
       --
       update xxcus.xxcus_ozf_single_ded_hdr_all
          set reprint_user_id  =fnd_global.user_id
             ,reprint_date     =sysdate
             ,reprint_count    =(reprint_count + 1)
        where 1 =1
          and hdr_customer_trx_id =xxcusozf_util_pkg.g_trx_id;
       --
     exception
       when others then
         rollback to start3;    
     end;
    -- 
     begin 
      select customer_key
      into   l_value_3
      from   hz_parties
      where  1 =1
        and  party_id   =(
                            select to_number(lob_id) 
                            from   xxcus.xxcus_ozf_single_ded_hdr_all 
                            where  1 =1
                              and  hdr_customer_trx_id =xxcusozf_util_pkg.g_trx_id        
                         );
      --
      l_request_id := Fnd_Request.Submit_Request(
                                                   Application => 'XXCUS',
                                                   Program     => 'XXCUSOZFEMAIL',
                                                   Description => NULL,
                                                   Sub_Request => FALSE,
                                                   Argument1   => fnd_global.conc_request_id,
                                                   Argument2   => xxcusozf_util_pkg.g_to_email,
                                                   Argument3   => TO_CHAR(SYSDATE, 'DDMONYYYYHH24MISS'),
                                                   Argument4   => nvl(l_value_3, 'NA'), 
                                                   Argument5   => xxcusozf_util_pkg.g_cc_email,
                                                   Argument6   => xxcusozf_util_pkg.g_cp_short_code --'XXCUS_OZF_SINGLE_DED_REPRINT'
                                                  );
      --
      if l_request_id >0 then
       commit;
      else
       print_log('Failed to kickoff email program for reprint of invoice id '||xxcusozf_util_pkg.g_trx_id);      
      end if;      
      --                          
     exception
      when no_data_found then
       l_value_3 :=Null;
      when others then
       l_value_3 :=Null; 
     end;    
    --      
   else
    --
    null;
    --
   end if;
   --
     RETURN TRUE;
   --
  EXCEPTION
   --
   WHEN OTHERS THEN
    print_log('Error in afterreport '||sqlerrm);
      l_err_msg  := '@xxcusozf_util_pkg.after report: ' ||
                    ' Error_Stack...' || dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace(); 
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'Rebates');   
    RETURN FALSE;
   --
  END afterreport;  
 --   
  /*Create Reimbursemeent Document*/
 --
 PROCEDURE create_reimbursement_document(ERRBUF          OUT NOCOPY VARCHAR2,
                                         RETCODE         OUT NOCOPY NUMBER,
                                         p_trx_number    IN VARCHAR2,
                                         p_lob_id        IN VARCHAR2,
                                         p_email_address IN VARCHAR2) is
    --
    lv_req_id NUMBER(30) := 0;
    vflag2    BOOLEAN;
    --
    CURSOR chk_trsfr_lob_flg(cv_trx_number IN VARCHAR2, cv_lob_id IN VARCHAR2) IS
      SELECT  rcta.trx_number
      FROM ra_customer_trx_all rcta,
             ra_customer_trx_lines_all rctl
      WHERE 1=1
        AND rcta.customer_trx_id = rctl.customer_trx_id
        AND rctl.revenue_amount<0
        AND rcta.attribute11 = 'N'
      ORDER BY rcta.trx_number;
    --       
    vreqphase                   VARCHAR2(50);
    vreqstatus                  VARCHAR2(50);
    vreqdevphase                VARCHAR2(50);
    vreqdevstatus               VARCHAR2(50);
    vreqmessage                 VARCHAR2(50);
    --
    fhandle                     UTL_FILE.file_type;
    vtextout                    VARCHAR2(32000);
    text                        VARCHAR2(32000);
    v_file                      VARCHAR2(500);
    --
    v_request_id                NUMBER := NULL;
    v_request_id_1              NUMBER := NULL;
    v_request_status            BOOLEAN;
    v_phase                     VARCHAR2(2000);
    v_wait_status               VARCHAR2(2000);
    --
    v_dev_phase                 VARCHAR2(2000);
    v_dev_status                VARCHAR2(2000);
    v_message                   VARCHAR2(2000);
    v_application_id            NUMBER;
    v_concurrent_program_id     NUMBER;
    --
    v_conc_prog_short_name      VARCHAR2(100);
    v_conc_prog_appl_short_name VARCHAR2(100);
    v_output_file_path          VARCHAR2(200);
    l_transfr_lob               VARCHAR2(3);
    l_err_msg                   VARCHAR2(3000);
    --
    l_err_code                  NUMBER;
    l_err_callpoint             VARCHAR2(75);
    l_err_callfrom              VARCHAR2(75);
    l_sec                       VARCHAR2(500);
    l_message                   VARCHAR2(75);
    --
    l_value_1                   VARCHAR2(30);
    l_value_2                   VARCHAR2(30);
    l_trx_number                VARCHAR2(30);
    l_counter                   NUMBER := 0;
    l_req_id                    NUMBER := FND_GLOBAL.CONC_REQUEST_ID;
    --
    l_distro_list               VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
    xml_layout                  boolean;
  --
  BEGIN
  
    errbuf  := NULL;
    retcode := 0;
  
    SAVEPOINT create_reimbursement_document;
    RETCODE := 0;
  
    FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                      '*======================================================================================================*');
    FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                      'Execution Starts On: ' ||
                      to_char(sysdate, 'MM-DD-YYYY HH24:MI:SS'));
    FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                      '*------------------------------------------------------------------------------------------------------*');
    FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                      'Request Id                 : ' ||
                      FND_GLOBAL.CONC_REQUEST_ID);
    FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                      '-------------------------------------------------------------------------------------------------------*');
  
    fnd_file.put_line(fnd_file.LOG, 'CHECK0');
    fnd_file.put_line(fnd_file.LOG, 'trx number' || p_trx_number);
    fnd_file.put_line(fnd_file.LOG, 'LOB ID' || p_lob_id);
    
    OPEN chk_trsfr_lob_flg(p_trx_number,
                       p_lob_id);                                     
    LOOP
     FETCH chk_trsfr_lob_flg INTO l_trx_number;                   
     EXIT WHEN chk_trsfr_lob_flg%NOTFOUND;
    END LOOP;
   --
   fnd_file.put_line(fnd_file.LOG, 'l_trx_number'||l_trx_number);
   --
   IF l_trx_number IS NOT NULL THEN
  
    IF p_trx_number IS NULL THEN
      l_value_1 := NULL;
    ELSE
      l_value_1 := p_trx_number;
    END IF;
  
    IF p_lob_id IS NULL THEN
      l_value_2 := NULL;
    ELSE
      l_value_2 := p_lob_id;
    END IF;
    
    fnd_file.put_line(fnd_file.LOG, 'l_value_1'||l_value_1);
    
    fnd_file.put_line(fnd_file.LOG, 'l_value_2'||l_value_2);
    
  
    xml_layout := FND_REQUEST.ADD_LAYOUT('XXCUS',
                                         'XXCUSOZFREMBINV',
                                         'EN',
                                         'US',
                                         'PDF');
    fnd_file.put_line(fnd_file.LOG, 'CHECK3');
    IF xml_layout THEN
    
      fnd_file.put_line(fnd_file.LOG, 'CHECK4');
    
      v_request_id := Fnd_Request.Submit_Request(Application => 'XXCUS',
                                                 Program     => 'XXCUSOZFREMBINV',
                                                 Description => NULL,
                                                 Sub_Request => FALSE,
                                                 Argument1   => l_value_1,
                                                 Argument2   => l_value_2,
                                                 Argument3   => '',
                                                 Argument4   => '');
    
      COMMIT;
      fnd_file.put_line(fnd_file.LOG, 'CHECK5');
      fnd_file.put_line(fnd_file.LOG,
                        'Concurrent Request Submitted Successfully:' ||
                        v_request_id);
    ELSE
    
      fnd_file.put_line(fnd_file.LOG, 'XML Layout Failed');
    END IF;
  
    IF v_request_id = 0 THEN
      fnd_file.put_line(fnd_file.LOG,
                        'Request Not Submitted due to "' || fnd_message.get || '".');
    END IF;
  
    IF v_request_id IS NOT NULL THEN
      --Wait for the request to complete successfully---
      v_request_status := fnd_concurrent.wait_for_request(request_id => v_request_id,
                                                          interval   => 0,
                                                          max_wait   => 0,
                                                          phase      => v_phase,
                                                          status     => v_wait_status,
                                                          dev_phase  => v_dev_phase,
                                                          dev_status => v_dev_status,
                                                          message    => v_message);
      v_dev_phase      := NULL;
      v_dev_status     := NULL;
    
    END IF;
  
    IF v_request_status THEN
      --Submit the Program to send the pdf as email ---
      v_request_id_1 := Fnd_Request.Submit_Request(Application => 'XXCUS',
                                                   Program     => 'XXCUSOZFREIMMAIL',
                                                   Description => NULL,
                                                   Sub_Request => FALSE,
                                                   Argument1   => v_request_id,
                                                   Argument2   => p_email_address,
                                                   Argument3   => SYSDATE,
                                                   Argument4   => NULL);
      COMMIT;
    
    END IF;
    --l_counter := l_counter+1;
    --END IF;
  
    UPDATE ra_customer_trx_all rcta
       SET ATTRIBUTE11 = 'Y'
     WHERE rcta.customer_trx_id = nvl(p_trx_number, rcta.customer_trx_id)
      AND  rcta.attribute1 = nvl(p_lob_id, rcta.attribute1);
  
    FOR c_deduction_processed IN (SELECT rcta.trx_number
                                    FROM ra_customer_trx_all rcta
                                   WHERE rcta.customer_trx_id =
                                         nvl(p_trx_number,
                                             rcta.customer_trx_id)
                                     AND rcta.attribute3 =
                                         nvl(p_lob_id, rcta.attribute3)
                                     AND attribute11 = 'Y'
                                   ORDER BY rcta.trx_number) 
    LOOP
     --
      l_counter := l_counter + 1;
    
      FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                        'Number of Deductions Printed :' || l_counter);
    
      FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                        'Following are the list of deductions that are printed :');
    
      FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                        'Deduction #' || l_counter || ':' ||
                        c_deduction_processed.trx_number);
     --
    END LOOP; --loop FOR c_deduction_processed
   END IF;
    
   --
  EXCEPTION
    WHEN fnd_api.g_exc_error THEN
      l_err_code := 2;
      l_err_msg  := l_sec;
      l_err_msg  := l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000);
    
      fnd_file.put_line(fnd_file.LOG, 'l_err_msg' || l_err_msg);
      retcode         := l_err_code;
      errbuf          := l_err_msg;
      l_err_callpoint := l_message;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom,
                                           p_calling           => l_err_callpoint,
                                           p_request_id        => l_req_id,
                                           p_ora_error_msg     => SQLERRM,
                                           p_error_desc        => 'Error Running XXCUSOZF_UTIL_PKG.create_deduction_document',
                                           p_distribution_list => l_distro_list,
                                           p_module            => 'TM');
    
      ROLLBACK TO create_deduction_document;
      retcode := 1;
    WHEN fnd_api.g_exc_unexpected_error THEN
      l_err_code := 2;
      l_err_msg  := l_sec;
      l_err_msg  := l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000);
    
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode         := l_err_code;
      errbuf          := l_err_msg;
      l_err_callpoint := l_message;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom,
                                           p_calling           => l_err_callpoint,
                                           p_request_id        => l_req_id,
                                           p_ora_error_msg     => SQLERRM,
                                           p_error_desc        => 'Error Running XXCUSOZF_UTIL_PKG.create_deduction_document package with PROGRAM ERROR',
                                           p_distribution_list => l_distro_list,
                                           p_module            => 'TM');
    
      ROLLBACK TO create_deduction_document;
      retcode := 1;
    WHEN OTHERS THEN
      l_err_code := 2;
      l_err_msg  := l_sec;
      l_err_msg  := l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000);
    
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode         := l_err_code;
      errbuf          := l_err_msg;
      l_err_callpoint := l_message;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom,
                                           p_calling           => l_err_callpoint,
                                           p_request_id        => l_req_id,
                                           p_ora_error_msg     => SQLERRM,
                                           p_error_desc        => 'Error Running XXCUSOZF_UTIL_PKG.create_deduction_document package with PROGRAM ERROR',
                                           p_distribution_list => l_distro_list,
                                           p_module            => 'TM');
    
      ROLLBACK TO create_reimbursement_document;
      retcode := 1;
  END create_reimbursement_document;

END XXCUSOZF_UTIL_PKG;
/