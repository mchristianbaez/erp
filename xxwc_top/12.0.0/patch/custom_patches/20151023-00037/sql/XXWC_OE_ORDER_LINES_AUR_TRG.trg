CREATE OR REPLACE TRIGGER APPS.XXWC_OE_ORDER_LINES_AUR_TRG
   /**************************************************************************
     $Header XXWC_OE_ORDER_LINES_AUR_TRG $
     Module Name: XXWC_OE_ORDER_LINES_AUR_TRG
     PURPOSE:   This Trigger is used to raise the business event sync 
	            the subinventory transfer with quantity update
     REVISIONS:
     Ver        Date         Author                  Description
     ---------  -----------  ---------------         -------------------------
     1.0        25-Jan-2015  Manjula Chellappan      TMS #20151023-00037 - Shipping Extension Modification
                                                     for Counter Order Removal and PUBD
     **************************************************************************/
AFTER UPDATE ON APPS.oe_order_lines_all 
FOR EACH ROW
WHEN ( new.flow_status_code <>'ENTERED'
 AND old.ordered_quantity <> new.ordered_quantity)

DECLARE
      
      l_error_msg               VARCHAR2 (240);      
      l_sec                     VARCHAR2 (100);
	  l_dflt_email            fnd_user.email_address%TYPE
                          := 'HDSOracleDevelopers@hdsupply.com';	  
BEGIN

	IF :new.line_type_id = fnd_profile.VALUE ('XXWC_COUNTER_LINE_TYPE') THEN

	xxwc_wsh_shipping_extn_pkg.raise_sync_counter_line(:new.line_id,:new.ordered_quantity,l_error_msg);

		IF l_error_msg IS NOT NULL THEN
				  raise_application_error (
					 -20000, 'Line_number '||:new.line_number||'.'||:new.shipment_number||' - '||
					 l_error_msg);
		END IF;
	
    END IF;
	
   EXCEPTION
      WHEN OTHERS
      THEN	 		 
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_OE_ORDER_LINES_AUR_TRG',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (SQLERRM, 1, 240),
            p_distribution_list   => l_dflt_email,
            p_module              => 'OM');

		  raise_application_error (
				 -20000,
				 'Subivnentory Transfer Failed for Line_number '||:new.line_number||'.'||:new.shipment_number||' - '|| SUBSTR (SQLERRM, 1, 240));
				 				
END;
/