CREATE OR REPLACE TRIGGER APPS.OE_AUDIT_ATTR_HISTORY_AIR_TRG
   /**************************************************************************
     $Header OE_AUDIT_ATTR_HISTORY_AIR_TRG $
     Module Name: OE_AUDIT_ATTR_HISTORY_AIR_TRG
     PURPOSE:   This Trigger is used to catch the Order line cancel and delete the records from custom table
     REVISIONS:
     Ver        Date         Author                  Description
     ---------  -----------  ---------------         -------------------------
     1.0        01-Mar-2016  Manjula Chellappan      TMS #20151023-00037 - Shipping Extension Modification
                                                     for Counter Order Removal and PUBD
     **************************************************************************/
AFTER INSERT ON APPS.oe_audit_attr_history 
FOR EACH ROW
WHEN ( new.order_type_id      IN ('1001','1012') -- Standard Line , Internal Order Line
   AND new.entity_id = 2 -- Order Line
   AND new.attribute_id      = 1230 -- Ordered quantity
   AND new.new_attribute_value = 0 )

DECLARE
      
      l_error_msg               VARCHAR2 (240);      
      l_sec                     VARCHAR2 (100);
	  l_dflt_email            fnd_user.email_address%TYPE
                          := 'HDSOracleDevelopers@hdsupply.com';	  
BEGIN
		
        l_sec		 := 'Delete Record From xxwc_oe_open_order_lines '; 
		
		DELETE FROM XXWC.xxwc_oe_open_order_lines		   
	     WHERE line_id = :new.entity_number;	    
	
   EXCEPTION
      WHEN OTHERS
      THEN	 		 
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'OE_AUDIT_ATTR_HISTORY_AIR_TRG',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (SQLERRM, 1, 240),
            p_distribution_list   => l_dflt_email,
            p_module              => 'OM');
				 				
END;
/
