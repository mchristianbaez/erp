create or replace package body APPS.XXWC_OE_OPEN_ORDERS_PKG is
/*************************************************************************
  $Header XXWC_OE_OPEN_ORDERS_PKG $
  Module Name: XXWC_OE_OPEN_ORDERS_PKG.pkb

  PURPOSE:   This package is called by the concurrent programs
             to Load data to the custom table XXWC_OE_OPEN_ORDER_LINES

  REVISIONS:
  Ver        Date        Author                     Description
  ---------  ----------  ---------------         -------------------------  
  1.0        15-Mar-2015 Manjula Chellappan      TMS #20151023-00037 - Shipping Extension Modification
                         /Rakesh Patel           for Counter Order Removal and PUBD
**************************************************************************/

  PROCEDURE load_open_order_lines 
    ( errbuf                  OUT VARCHAR2,
      retcode                 OUT VARCHAR2,
      p_organization_id       IN NUMBER,
      p_inventory_item_id     IN NUMBER) IS
/*************************************************************************
  $Header XXWC_OE_OPEN_ORDERS_PKG $
  Module Name: XXWC_OE_OPEN_ORDERS_PKG.load_open_order_lines

  PURPOSE:   This procedure is called by the concurrent programs
             to Load data to the custom table XXWC_OE_OPEN_ORDER_LINES

  REVISIONS:
  Ver        Date        Author                     Description
  ---------  ----------  ---------------         -------------------------  
  1.0        15-Mar-2015 Manjula Chellappan      TMS #20151023-00037 - Shipping Extension Modification
                                                     for Counter Order Removal and PUBD
**************************************************************************/   
l_sec VARCHAR2(100);
   
BEGIN

FND_FILE.PUT_LINE(fnd_file.log, 'p_organization_id : '||p_organization_id);
FND_FILE.PUT_LINE(fnd_file.log, 'p_inventory_item_id : '||p_inventory_item_id);

l_sec := 'Delete from xxwc_oe_open_order_lines';

DELETE FROM XXWC.xxwc_oe_open_order_lines 
WHERE organization_id = NVL(p_organization_id,organization_id) 
AND inventory_item_id = NVL(p_inventory_item_id,inventory_item_id);
  
FND_FILE.PUT_LINE(fnd_file.log, 'No. Of Lines Deleted : '||sql%rowcount);

l_sec := 'Insert into xxwc_oe_open_order_lines 1';

	INSERT
	INTO XXWC.xxwc_oe_open_order_lines
	  (
	    line_id ,
	    header_id ,
	    line_type_id ,
	    inventory_item_id ,
	    organization_id ,
	    ordered_quantity ,
	    org_id ,
	    created_by ,
	    last_updated_by ,
	    creation_date ,
	    last_update_date ,
	    last_update_login
	  )
	SELECT ol.line_id ,
	  ol.header_id ,
	  ol.line_type_id ,
	  ol.inventory_item_id ,
	  ol.ship_from_org_id ,
	  (ol.ordered_quantity
                     * po_uom_s.po_uom_convert (um.unit_of_measure
                                                , msib.primary_unit_of_measure
                                                , ol.inventory_item_id
                                               ) 
       ),
	  ol.org_id ,
	  fnd_global.user_id ,
	  fnd_global.user_id ,
	  sysdate ,
	  sysdate ,
	  fnd_global.login_id
	FROM oe_order_lines ol
	   , mtl_system_items_b msib
	   , mtl_units_of_measure_vl um
	WHERE ol.flow_status_code = ('AWAITING_SHIPPING')
	AND   ol.user_item_description <>'OUT_FOR_DELIVERY'
	AND ol.line_type_id       IN (fnd_profile.value('XXWC_STANDARD_LINE_TYPE')) 
    AND ol.inventory_item_id = msib.inventory_item_id
	AND ol.ship_from_org_id = msib.organization_id
	AND ol.order_quantity_uom = um.uom_code 
	AND ol.open_flag = 'Y'
    AND ol.cancelled_flag = 'N'	
    AND ol.ship_from_org_id = NVL(p_organization_id,ol.ship_from_org_id) 
    AND ol.inventory_item_id = NVL(p_inventory_item_id,ol.inventory_item_id);
	
	commit;
    FND_FILE.PUT_LINE(fnd_file.log, 'No. Of Standard Lines Loaded : '||sql%rowcount);

l_sec := 'Insert into xxwc_oe_open_order_lines 2';	
	INSERT
	INTO XXWC.xxwc_oe_open_order_lines
	  (
	    line_id ,
	    header_id ,
	    line_type_id ,
	    inventory_item_id ,
	    organization_id ,
	    ordered_quantity ,
	    org_id ,
	    created_by ,
	    last_updated_by ,
	    creation_date ,
	    last_update_date ,
	    last_update_login
	  )
	SELECT ol.line_id ,
	  ol.header_id ,
	  ol.line_type_id ,
	  ol.inventory_item_id ,
	  ol.ship_from_org_id ,
	  (ol.ordered_quantity
                     * po_uom_s.po_uom_convert (um.unit_of_measure
                                                , msib.primary_unit_of_measure
                                                , ol.inventory_item_id
                                               ) 
       ),
	  ol.org_id ,
	  fnd_global.user_id ,
	  fnd_global.user_id ,
	  sysdate ,
	  sysdate ,
	  fnd_global.login_id
	FROM oe_order_lines ol
	   , mtl_system_items_b msib
	   , mtl_units_of_measure_vl um
	WHERE ol.flow_status_code = ('AWAITING_SHIPPING')
	AND   ol.user_item_description NOT IN ('OUT_FOR_DELIVERY','OUT_FOR_DELIVERY/PARTIAL_BACKORDER')
	AND ol.line_type_id       IN (fnd_profile.value('XXWC_INTERNAL_ORDER_LINE_TYPE')) 
    AND ol.inventory_item_id = msib.inventory_item_id
	AND ol.ship_from_org_id = msib.organization_id
	AND ol.order_quantity_uom = um.uom_code 
	AND ol.open_flag = 'Y'
    AND ol.cancelled_flag = 'N'	
    AND ol.ship_from_org_id = NVL(p_organization_id,ol.ship_from_org_id) 
    AND ol.inventory_item_id = NVL(p_inventory_item_id,ol.inventory_item_id);
	
    FND_FILE.PUT_LINE(fnd_file.log, 'No. Of Internal Lines Loaded : '||sql%rowcount);
    commit;
	
	l_sec := 'Insert into xxwc_oe_open_order_lines 3';
	INSERT
	INTO XXWC.xxwc_oe_open_order_lines
	  (
	    line_id ,
	    header_id ,
	    line_type_id ,
	    inventory_item_id ,
	    organization_id ,
	    ordered_quantity ,
	    org_id ,
	    created_by ,
	    last_updated_by ,
	    creation_date ,
	    last_update_date ,
	    last_update_login
	  )
	SELECT ol.line_id ,
	  ol.header_id ,
	  ol.line_type_id ,
	  ol.inventory_item_id ,
	  ol.ship_from_org_id ,
	  ((ol.ordered_quantity-NVL(    ( SELECT xws.transaction_qty 
	                                    FROM XXWC.XXWC_WSH_SHIPPING_STG xws
							           WHERE xws.line_id = ol.line_id
	                                ),
							   0)
	    )
                     * po_uom_s.po_uom_convert (um.unit_of_measure
                                                , msib.primary_unit_of_measure
                                                , ol.inventory_item_id
                                               ) 
       ),
	  ol.org_id ,
	  fnd_global.user_id ,
	  fnd_global.user_id ,
	  sysdate ,
	  sysdate ,
	  fnd_global.login_id
	FROM oe_order_lines_all ol
       , mtl_system_items_b msib
	   , mtl_units_of_measure_vl um
	WHERE ol.flow_status_code = ('AWAITING_SHIPPING')
	AND   ol.user_item_description  = 'OUT_FOR_DELIVERY/PARTIAL_BACKORDER'
   -- AND   ol.ordered_quantity > TO_NUMBER(ol.attribute11)
	AND ol.line_type_id       IN (fnd_profile.value('XXWC_INTERNAL_ORDER_LINE_TYPE'))
	AND ol.inventory_item_id = msib.inventory_item_id
	AND ol.ship_from_org_id = msib.organization_id
	AND ol.open_flag = 'Y'
    AND ol.cancelled_flag = 'N'	
	AND ol.order_quantity_uom = um.uom_code 
    AND ol.ship_from_org_id = NVL(p_organization_id,ol.ship_from_org_id) 
    AND ol.inventory_item_id = NVL(p_inventory_item_id,ol.inventory_item_id);

FND_FILE.PUT_LINE(fnd_file.log, 'No. Of Partial Internal Lines Loaded : '||sql%rowcount);

	   COMMIT;  

EXCEPTION WHEN OTHERS THEN

FND_FILE.PUT_LINE(fnd_file.log, 'Error at '||l_sec ||' : '|| SQLERRM);

           xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_OE_OPEN_ORDERS_PKG.load_open_order_lines',
            p_calling             => l_sec,
            p_request_id          => fnd_global.CONC_REQUEST_ID,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => 'Others Exception',
            p_distribution_list   => g_dflt_email,
            p_module              => 'OE');

            ROLLBACK;
		retcode := 2;
		errbuf  := 'Error at '||l_sec ||' : '|| SQLERRM ; 

END load_open_order_lines;     
      
end XXWC_OE_OPEN_ORDERS_PKG;
/
