   /*************************************************************************
   *   $Header XXWC_OE_COUNTER_LINES_TBL.sql $
   *   Module Name: XXWC_OE_COUNTER_LINES_TBL
   *
   *   PURPOSE:   To store the Error Records from XXWC OM pricing Guard Rail Hold Program
   *
   *   REVISIONS:
   *   Ver        Date         Author                Description
   *   ---------  ----------   ---------------       -------------------------
   *   1.0        21-Dec-2015  Manjula Chellappan    Initial Version
   *                                                  TMS# 20151023-00037 Shipping Extension Modification for Counter Order Removal and PUBD
   * ***************************************************************************/

CREATE TABLE "XXWC"."XXWC_OE_COUNTER_LINES_TBL"
(
   "LINE_ID"                 		NUMBER,  
   "LINE_NUM"                       VARCHAR2 (60 BYTE),
   "HEADER_ID"               		NUMBER,
   "ORDER_NUMBER"                   NUMBER,
   "INVENTORY_ITEM_ID"       		NUMBER,      
   "ORDERED_QUANTITY"        		NUMBER,
   "PRIMARY_UOM_CODE"               VARCHAR2(10),
   "LINE_TYPE_ID"            		NUMBER,   
   "SHIP_FROM_ORG_ID"               NUMBER,
   "SUBINVENTORY"   	      	    VARCHAR2 (10 BYTE),
   "STOCK_ENABLED_FLAG"      		VARCHAR2(1),
   "MTL_TRANSACTIONS_ENABLED_FLAG"  VARCHAR2(1),
   "CREATED_BY"              		NUMBER,
   "LAST_UPDATED_BY"         		NUMBER,
   "CREATION_DATE"           		DATE,
   "LAST_UPDATE_DATE"        		DATE,
   "LAST_UPDATE_LOGIN"       		NUMBER
)
SEGMENT CREATION DEFERRED
PCTFREE 10
PCTUSED 40
INITRANS 1
MAXTRANS 255
NOCOMPRESS
LOGGING
TABLESPACE "XXWC_DATA";

CREATE INDEX XXWC.XXWC_OE_COUNTER_LINES_TBL_N1 ON XXWC.XXWC_OE_COUNTER_LINES_TBL (line_id, ship_from_org_id);
