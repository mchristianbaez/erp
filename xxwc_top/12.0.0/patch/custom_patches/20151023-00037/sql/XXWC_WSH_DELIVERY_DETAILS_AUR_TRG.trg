CREATE OR REPLACE TRIGGER APPS.XXWC_WSH_DELIVERY_DET_AUR_TRG
   /**************************************************************************
     $Header XXWC_WSH_DELIVERY_DET_AIR_TRG $
     Module Name: XXWC_WSH_DELIVERY_DET_AIR_TRG
     PURPOSE:   This trigger is used to insert new record to the custom table 
	            at the time of new delivery line creation for internal order lines
     REVISIONS:
     Ver        Date         Author                  Description
     ---------  -----------  ---------------         -------------------------
     1.0        4-Mar-2016  Manjula Chellappan      TMS #20151023-00037 - Shipping Extension Modification
                                                     for Counter Order Removal and PUBD
     **************************************************************************/
AFTER UPDATE ON APPS.wsh_delivery_details 
FOR EACH ROW
WHEN ( new.split_from_delivery_detail_id IS NOT NULL
AND new.source_line_id <> old.source_line_id)

DECLARE
      
      l_error_msg               VARCHAR2 (240);      
      l_sec                     VARCHAR2 (100);
	  l_dflt_email            fnd_user.email_address%TYPE
                          := 'HDSOracleDevelopers@hdsupply.com';      
	  
BEGIN	

    l_sec := 'Call audit_split_int_line_qty';
	xxwc_wsh_shipping_extn_pkg.audit_split_int_line_qty(:new.source_line_id,l_error_msg);

		IF l_error_msg IS NOT NULL THEN
				  raise_application_error (
					 -20000, 'Delivery_detail_id '||:new.delivery_detail_id||' '||
					 l_error_msg);
		END IF;
	   
   EXCEPTION
      WHEN OTHERS
      THEN	 		 
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_WSH_DELIVERY_DET_AIR_TRG',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (SQLERRM, 1, 240),
            p_distribution_list   => l_dflt_email,
            p_module              => 'OM');

		  raise_application_error (
					 -20000, 'Delivery_detail_id '||:new.delivery_detail_id||' '||
					 l_error_msg);
					 
		  raise_application_error (
				 -20000,
				 'Record not created in the audit table for delivery_detail_id'||:new.delivery_detail_id||SUBSTR (SQLERRM, 1, 240));
				 				
END;
/
