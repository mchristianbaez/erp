CREATE OR REPLACE PACKAGE BODY APPS.XXWC_MD_SEARCH_REFRESH_PKG
AS        
   /*************************************************************************
    Copyright (c) 2012 Lucidity Consulting Group
    All rights reserved.
   **************************************************************************
     $Header XXWC_MD_SEARCH_REFRESH_PKG $
     Module Name: XXWC_MD_SEARCH_REFRESH_PKG.pkb

     PURPOSE:   This package is used by AIS process to refresh MVs , Indexes and create Synonym
          MV      : XXWC_MD_SEARCH_PRODUCTS_MV
          MV2     : XXWC_MD_SEARCH_PRODUCTS_MV2
          SYNONYM : XXWC_MD_SEARCH_PRODUCTS_MV_S

     REVISIONS:
     Ver        Date        Author                  Description
     ---------  ----------  ---------------         -------------------------
     1.0        06/28/2016  Gopi Damuluri           Initial Version TMS# 20160628-00001
     **************************************************************************/

   /*************************************************************************
     Procedure : refresh_ais_mv

     PURPOSE:   This procedure is to refresh AIS XXWC_MD_SEARCH_PRODUCTS_MV and its Indexes 

     REVISIONS:
     Ver        Date        Author                  Description
     ---------  ----------  ---------------         -------------------------
     1.0        06/28/2016  Gopi Damuluri           Initial Version TMS# 20160628-00001
     2.0        11/21/2016  Nancy Pahwa             TMS 20161121-00176
************************************************************************/
   PROCEDURE refresh_ais_mv (p_err_buf     OUT VARCHAR2,
                             p_return_code OUT NUMBER)
          IS
   l_sec           VARCHAR2(100);
   BEGIN
        l_sec := 'Droping APPS.XXWC_MD_SEARCH_PRODUCTS_MV';
        fnd_file.put_line (fnd_file.LOG, l_sec||' - '||TO_CHAR(SYSDATE,'DD-MON-YYYY HH24:MI:SS'));
        EXECUTE IMMEDIATE 'drop MATERIALIZED VIEW APPS.XXWC_MD_SEARCH_PRODUCTS_MV';

        l_sec := 'Create APPS.XXWC_MD_SEARCH_PRODUCTS_MV';
        fnd_file.put_line (fnd_file.LOG, l_sec||' - '||TO_CHAR(SYSDATE,'DD-MON-YYYY HH24:MI:SS'));
--2.0 Start
        EXECUTE IMMEDIATE '   CREATE MATERIALIZED VIEW APPS.XXWC_MD_SEARCH_PRODUCTS_MV REFRESH COMPLETE ON DEMAND

            AS
              select inventory_item_id,
                     organization_id,
                     partnumber,
                     type,
                     manufacturerpartnumber,
                     manufacturer,
                     sequence,
                     currencycode,
                     name,
                     shortdescription,
                     longdescription,
                     thumbnail,
                     fullimage,
                     quantitymeasure,
                     weightmeasure,
                     weight,
                     buyable,
                     keyword,
                     creation_date,
                     item_type,
                     cross_reference,
                     dummy,
                     primary_uom_code,
                       dummy2, partnumber2 
                from APPS.XXWC_MD_SEARCH_PRODUCTS_VW';
 -- 2.0 end
        l_sec := 'Create index APPS.XXWC_MD_SEARCH_MV_N1';
        fnd_file.put_line (fnd_file.LOG, l_sec||' - '||TO_CHAR(SYSDATE,'DD-MON-YYYY HH24:MI:SS'));
        EXECUTE IMMEDIATE 'create index APPS.XXWC_MD_SEARCH_MV_N1 on XXWC_MD_SEARCH_PRODUCTS_MV(partnumber) indextype is ctxsys.context parameters(''
                                wordlist XXWC_MD_PRODUCT_SEARCH_PREF '')';

        l_sec := 'Create index APPS.XXWC_MD_SEARCH_MV_N2';
        fnd_file.put_line (fnd_file.LOG, l_sec||' - '||TO_CHAR(SYSDATE,'DD-MON-YYYY HH24:MI:SS'));
        EXECUTE IMMEDIATE 'create index APPS.XXWC_MD_SEARCH_MV_N2 on XXWC_MD_SEARCH_PRODUCTS_MV(shortdescription) indextype is ctxsys.context parameters(''
                                wordlist XXWC_MD_PRODUCT_SEARCH_PREF '')';

        l_sec := 'Create index APPS.XXWC_MD_SEARCH_MV_N3';
        fnd_file.put_line (fnd_file.LOG, l_sec||' - '||TO_CHAR(SYSDATE,'DD-MON-YYYY HH24:MI:SS'));
        EXECUTE IMMEDIATE 'create index APPS.XXWC_MD_SEARCH_MV_N3 on XXWC_MD_SEARCH_PRODUCTS_MV(cross_reference) indextype is ctxsys.context parameters(''
                                wordlist XXWC_MD_PRODUCT_SEARCH_PREF '')';

        l_sec := 'Create index APPS.XXWC_MD_SEARCH_MV_N6';
        fnd_file.put_line (fnd_file.LOG, l_sec||' - '||TO_CHAR(SYSDATE,'DD-MON-YYYY HH24:MI:SS'));
        EXECUTE IMMEDIATE 'create index APPS.XXWC_MD_SEARCH_MV_N6 on XXWC_MD_SEARCH_PRODUCTS_MV (inventory_item_id)'; --1.1

        l_sec := 'Create index APPS.XXWC_MD_SEARCH_MV_N5';
        fnd_file.put_line (fnd_file.LOG, l_sec||' - '||TO_CHAR(SYSDATE,'DD-MON-YYYY HH24:MI:SS'));
        EXECUTE IMMEDIATE 'create index APPS.XXWC_MD_SEARCH_MV_N5 on XXWC_MD_SEARCH_PRODUCTS_MV(dummy) indextype is ctxsys.context parameters(''
                                DATASTORE XXWC_MD_PRODUCT_STORE_PREF lexer
                                XXWC_MD_PRODUCT_STORE_LEX1 section group
                                XXWC_MD_PRODUCT_STORE_SG '')';
--2.0 start
    l_sec := 'Create index APPS.XXWC_MD_SEARCH_MV_N7';
    fnd_file.put_line(fnd_file.LOG,
                      l_sec || ' - ' ||
                      TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));
    EXECUTE IMMEDIATE 'CREATE INDEX "APPS"."XXWC_MD_SEARCH_MV_N7" ON "APPS"."XXWC_MD_SEARCH_PRODUCTS_MV" ("PARTNUMBER2") 
   INDEXTYPE IS "CTXSYS"."CONTEXT"  PARAMETERS (''wordlist XXWC_MD_PRODUCT_SEARCH_PREF'')'; 
  
    l_sec := 'Create index APPS.XXWC_MD_SEARCH_MV_N8';
    fnd_file.put_line(fnd_file.LOG,
                      l_sec || ' - ' ||
                      TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));
    EXECUTE IMMEDIATE 'CREATE INDEX APPS.XXWC_MD_SEARCH_MV_N8 ON XXWC_MD_SEARCH_PRODUCTS_MV (DUMMY2) INDEXTYPE IS CTXSYS.CONTEXT PARAMETERS (''
                              DATASTORE XXWC_MD_PRODUCT_STORE_PREF1 lexer    
                              XXWC_MD_PRODUCT_STORE_LEX2 section group  
                              XXWC_MD_PRODUCT_STORE_SG1'')'; -- 2.0 end
        fnd_file.put_line (fnd_file.LOG, 'End REFRESH_AIS_MV '||' - '||TO_CHAR(SYSDATE,'DD-MON-YYYY HH24:MI:SS'));

   EXCEPTION
   WHEN OTHERS THEN
      p_err_buf     := SQLERRM || l_sec;
      p_return_code := 2;
   END refresh_ais_mv;

   /*************************************************************************
     Procedure : create_syn_for_ais_mv

     PURPOSE:   This procedure is create synonym XXWC_MD_SEARCH_PRODUCTS_MV_S pointing to XXWC_MD_SEARCH_PRODUCTS_MV

     REVISIONS:
     Ver        Date        Author                  Description
     ---------  ----------  ---------------         -------------------------
     1.0        06/28/2016  Gopi Damuluri           Initial Version TMS# 20160628-00001
   ************************************************************************/
   PROCEDURE create_syn_for_ais_mv (p_err_buf     OUT VARCHAR2,
                             p_return_code OUT NUMBER)
          IS
   l_sec           VARCHAR2(100);
   l_mv_rec_cnt    NUMBER;
   BEGIN
        
        BEGIN
        SELECT COUNT(1)
          INTO l_mv_rec_cnt
          FROM APPS.XXWC_MD_SEARCH_PRODUCTS_MV;
        EXCEPTION
        WHEN OTHERS THEN
           l_mv_rec_cnt := 0;
        END;

        l_sec := 'l_mv_rec_cnt - '||l_mv_rec_cnt;
        fnd_file.put_line (fnd_file.LOG, l_sec||' - '||TO_CHAR(SYSDATE,'DD-MON-YYYY HH24:MI:SS'));

        IF l_mv_rec_cnt > 0 THEN
        l_sec := 'Crate Synonym APPS.XXWC_MD_SEARCH_PRODUCTS_MV_S for XXWC_MD_SEARCH_PRODUCTS_MV';
        fnd_file.put_line (fnd_file.LOG, l_sec||' - '||TO_CHAR(SYSDATE,'DD-MON-YYYY HH24:MI:SS'));
        EXECUTE IMMEDIATE 'CREATE OR REPLACE SYNONYM APPS.XXWC_MD_SEARCH_PRODUCTS_MV_S FOR APPS.XXWC_MD_SEARCH_PRODUCTS_MV';
        ELSE
           create_syn_for_ais_bkp(p_err_buf, p_return_code);
        END IF;

        fnd_file.put_line (fnd_file.LOG, 'End CREATE_SYN_FOR_AIS_MV '||' - '||TO_CHAR(SYSDATE,'DD-MON-YYYY HH24:MI:SS'));

   EXCEPTION
   WHEN OTHERS THEN
      p_err_buf     := SQLERRM || l_sec;
      p_return_code := 2;
   END create_syn_for_ais_mv;

   /*************************************************************************
     Procedure : backup_ais_mv

     PURPOSE:   This procedure is to refresh AIS XXWC_MD_SEARCH_PRODUCTS_MV2 and its Indexes 

     REVISIONS:
     Ver        Date        Author                  Description
     ---------  ----------  ---------------         -------------------------
     1.0        06/28/2016  Gopi Damuluri           Initial Version TMS# 20160628-00001
     2.0       11/21/2016   Nancy Pahwa             TMS 20161121-00176
     3.0       11/22/2016  Nancy Pahwa             TMS 20161121-00176 Index 8 to use pref1 under XXWC schema

    ************************************************************************/
   PROCEDURE backup_ais_mv (p_err_buf     OUT VARCHAR2,
                             p_return_code OUT NUMBER)
          IS
   l_sec           VARCHAR2(100);
   BEGIN
        
        BEGIN
        l_sec := 'Droping table XXWC.XXWC_MD_SEARCH_PRODUCTS_TBL';
        fnd_file.put_line (fnd_file.LOG, l_sec||' - '||TO_CHAR(SYSDATE,'DD-MON-YYYY HH24:MI:SS'));
        EXECUTE IMMEDIATE 'DROP TABLE XXWC.XXWC_MD_SEARCH_PRODUCTS_TBL';
        EXCEPTION
        WHEN OTHERS THEN
          fnd_file.put_line (fnd_file.LOG, SQLERRM);
        END;

        l_sec := 'Create Table XXWC.XXWC_MD_SEARCH_PRODUCTS_TBL';
        fnd_file.put_line (fnd_file.LOG, l_sec||' - '||TO_CHAR(SYSDATE,'DD-MON-YYYY HH24:MI:SS'));
        EXECUTE IMMEDIATE 'CREATE TABLE XXWC.XXWC_MD_SEARCH_PRODUCTS_TBL
            AS select *
                from APPS.XXWC_MD_SEARCH_PRODUCTS_MV';

        l_sec := 'Create index XXWC.XXWC_MD_SEARCH_PRODUCT_N1';
        fnd_file.put_line (fnd_file.LOG, l_sec||' - '||TO_CHAR(SYSDATE,'DD-MON-YYYY HH24:MI:SS'));
        EXECUTE IMMEDIATE 'create index XXWC.XXWC_MD_SEARCH_PRODUCT_N1 on XXWC.XXWC_MD_SEARCH_PRODUCTS_TBL(partnumber) indextype is ctxsys.context parameters(''
                                wordlist XXWC.XXWC_MD_PRODUCT_SEARCH_PREF '')';

        l_sec := 'Create index XXWC.XXWC_MD_SEARCH_PRODUCT_N2';
        fnd_file.put_line (fnd_file.LOG, l_sec||' - '||TO_CHAR(SYSDATE,'DD-MON-YYYY HH24:MI:SS'));
        EXECUTE IMMEDIATE 'create index XXWC.XXWC_MD_SEARCH_PRODUCT_N2 on XXWC.XXWC_MD_SEARCH_PRODUCTS_TBL(shortdescription) indextype is ctxsys.context parameters(''
                                wordlist XXWC.XXWC_MD_PRODUCT_SEARCH_PREF '')';

        l_sec := 'Create index XXWC.XXWC_MD_SEARCH_PRODUCT_N3';
        fnd_file.put_line (fnd_file.LOG, l_sec||' - '||TO_CHAR(SYSDATE,'DD-MON-YYYY HH24:MI:SS'));
        EXECUTE IMMEDIATE 'create index XXWC.XXWC_MD_SEARCH_PRODUCT_N3 on XXWC.XXWC_MD_SEARCH_PRODUCTS_TBL(cross_reference) indextype is ctxsys.context parameters(''
                                wordlist XXWC.XXWC_MD_PRODUCT_SEARCH_PREF '')';

        l_sec := 'Create index XXWC.XXWC_MD_SEARCH_PRODUCT_N6';
        fnd_file.put_line (fnd_file.LOG, l_sec||' - '||TO_CHAR(SYSDATE,'DD-MON-YYYY HH24:MI:SS'));
        EXECUTE IMMEDIATE 'create index XXWC.XXWC_MD_SEARCH_PRODUCT_N6 on XXWC.XXWC_MD_SEARCH_PRODUCTS_TBL (inventory_item_id)'; --1.1

        l_sec := 'Create index XXWC.XXWC_MD_SEARCH_PRODUCT_N5';
        fnd_file.put_line (fnd_file.LOG, l_sec||' - '||TO_CHAR(SYSDATE,'DD-MON-YYYY HH24:MI:SS'));
        EXECUTE IMMEDIATE 'create index XXWC.XXWC_MD_SEARCH_PRODUCT_N5 on XXWC.XXWC_MD_SEARCH_PRODUCTS_TBL(dummy) indextype is ctxsys.context parameters(''
                                DATASTORE XXWC.XXWC_MD_PRODUCT_STORE_PREF lexer
                                XXWC_MD_PRODUCT_STORE_LEX1 section group
                                XXWC_MD_PRODUCT_STORE_SG '')';
 --2.0 start 
   l_sec := 'Create index XXWC.XXWC_MD_SEARCH_PRODUCT_N7';
    fnd_file.put_line(fnd_file.LOG,
                      l_sec || ' - ' ||
                      TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));
    EXECUTE IMMEDIATE 'create index XXWC.XXWC_MD_SEARCH_PRODUCT_N7 on XXWC.XXWC_MD_SEARCH_PRODUCTS_TBL(partnumber2) indextype is ctxsys.context parameters(''
                                wordlist XXWC.XXWC_MD_PRODUCT_SEARCH_PREF '')';
  
    l_sec := 'Create index XXWC.XXWC_MD_SEARCH_PRODUCT_N8';
    fnd_file.put_line(fnd_file.LOG,
                      l_sec || ' - ' ||
                      TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));
    EXECUTE IMMEDIATE 'CREATE INDEX XXWC.XXWC_MD_SEARCH_PRODUCT_N8 ON XXWC.XXWC_MD_SEARCH_PRODUCTS_TBL(DUMMY2) INDEXTYPE IS CTXSYS.CONTEXT PARAMETERS (''
                              DATASTORE XXWC.XXWC_MD_PRODUCT_STORE_PREF1 lexer    
                              XXWC_MD_PRODUCT_STORE_LEX2 section group  
                              XXWC_MD_PRODUCT_STORE_SG1'')'; 
-- 2.0 end
        fnd_file.put_line (fnd_file.LOG, 'End BACKUP_AIS_MV '||' - '||TO_CHAR(SYSDATE,'DD-MON-YYYY HH24:MI:SS'));

   EXCEPTION
   WHEN OTHERS THEN
      p_err_buf     := SQLERRM || l_sec;
      p_return_code := 2;
   END backup_ais_mv;

   /*************************************************************************
     Procedure : create_syn_for_ais_bkp

     PURPOSE:   This procedure is create synonym XXWC_MD_SEARCH_PRODUCTS_MV_S pointing to XXWC_MD_SEARCH_PRODUCTS_MV2

     REVISIONS:
     Ver        Date        Author                  Description
     ---------  ----------  ---------------         -------------------------
     1.0        06/28/2016  Gopi Damuluri           Initial Version TMS# 20160628-00001
   ************************************************************************/
   PROCEDURE create_syn_for_ais_bkp (p_err_buf     OUT VARCHAR2,
                             p_return_code OUT NUMBER)
          IS
   l_sec           VARCHAR2(100);
   l_bkp_rec_cnt   NUMBER;
   BEGIN

        BEGIN
          SELECT COUNT(1)
            INTO l_bkp_rec_cnt
            FROM XXWC.XXWC_MD_SEARCH_PRODUCTS_TBL;
        EXCEPTION
        WHEN OTHERS THEN
          l_bkp_rec_cnt := 0;
        END;

        fnd_file.put_line (fnd_file.LOG, 'l_bkp_rec_cnt '||l_bkp_rec_cnt);

        IF l_bkp_rec_cnt > 0 THEN
           l_sec := 'Crate Synonym APPS.XXWC_MD_SEARCH_PRODUCTS_MV_S for XXWC.XXWC_MD_SEARCH_PRODUCTS_TBL';
           fnd_file.put_line (fnd_file.LOG, l_sec||' - '||TO_CHAR(SYSDATE,'DD-MON-YYYY HH24:MI:SS'));
           EXECUTE IMMEDIATE 'CREATE OR REPLACE SYNONYM APPS.XXWC_MD_SEARCH_PRODUCTS_MV_S FOR XXWC.XXWC_MD_SEARCH_PRODUCTS_TBL';
        END IF;
        
        fnd_file.put_line (fnd_file.LOG, 'End CREATE_SYN_FOR_AIS_BKP '||' - '||TO_CHAR(SYSDATE,'DD-MON-YYYY HH24:MI:SS'));
   EXCEPTION
   WHEN OTHERS THEN
      p_err_buf     := SQLERRM || l_sec;
      p_return_code := 2;
   END create_syn_for_ais_bkp;

   /*************************************************************************
     Procedure : main

     PURPOSE:   This procedure is the main procedure for creating synonym XXWC_MD_SEARCH_PRODUCTS_MV_S 
                pointing to XXWC_MD_SEARCH_PRODUCTS_MV or XXWC_MD_SEARCH_PRODUCTS_MV2

     REVISIONS:
     Ver        Date        Author                  Description
     ---------  ----------  ---------------         -------------------------
     1.0        06/28/2016  Gopi Damuluri           Initial Version TMS# 20160628-00001
   ************************************************************************/
   PROCEDURE main (p_err_buf     OUT VARCHAR2,
                   p_return_code OUT NUMBER,
                   p_process_step IN VARCHAR2)
   IS
      l_exception     EXCEPTION;
      l_err_buf       VARCHAR2(2000);
      l_return_code   NUMBER;
   BEGIN
      
      fnd_file.put_line (fnd_file.LOG, 'p_process_step - '||p_process_step);
      
      IF p_process_step       = 'REFRESH_AIS_MV' THEN
         refresh_ais_mv(l_err_buf , l_return_code);
      ELSIF p_process_step    = 'CREATE_SYN_FOR_AIS_MV' THEN
         create_syn_for_ais_mv(l_err_buf , l_return_code);
      ELSIF p_process_step    = 'BACKUP_AIS_MV' THEN
         backup_ais_mv(l_err_buf , l_return_code);
      ELSIF p_process_step    = 'CREATE_SYN_FOR_AIS_BKP' THEN
         create_syn_for_ais_bkp(l_err_buf , l_return_code);
      END IF;
      
      IF l_return_code = 2 THEN
        RAISE l_exception;
      END IF;
      
      fnd_file.put_line (fnd_file.LOG, 'End MAIN '||' - '||TO_CHAR(SYSDATE,'DD-MON-YYYY HH24:MI:SS'));
      
   EXCEPTION
   WHEN l_exception THEN
      p_err_buf     := l_err_buf;
      p_return_code := 2;
   WHEN OTHERS THEN
      p_err_buf     := SQLERRM;
      p_return_code := 2;
   END main;

END XXWC_MD_SEARCH_REFRESH_PKG;
/