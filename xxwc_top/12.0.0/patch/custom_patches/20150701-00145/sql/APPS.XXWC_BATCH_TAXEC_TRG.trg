CREATE OR REPLACE TRIGGER APPS.XXWC_BATCH_TAXEC_Trg
   BEFORE UPDATE OR INSERT OR DELETE
   ON FND_FLEX_VALUE_NORM_HIERARCHY
   REFERENCING NEW AS NEW OLD AS OLD
   FOR EACH ROW
DECLARE
   --PRAGMA autonomous_transaction;
   /**********************************************************************************************
    File Name: xxwc.XXWC_BATCH_TAXEC_Trg

    PROGRAM TYPE: TRIGGER

    PURPOSE: Triiger to update flat table with customers which need to be changed
             because of changes to this flexfield value set

    HISTORY
    =============================================================================
           Last Update Date : 03/07/2012
    =============================================================================
    =============================================================================
    VERSION DATE          AUTHOR(S)       DESCRIPTION
    ------- -----------   --------------- ----------------------------------------
    1.0     03-MAR-2012   Manny Rodriguez  Created.
    1.1     26-APR-2012   Manny Rodriguez  Changed to insert or Delete capable
    1.2     16-OCT-2014   Maharajan Shunmugam TMS#20141001-00161 Canada Multi Org changes
    1.3     16-JUN-2015   Maharajan Shunmugam TMS#20150317-00066 Tax - XXWC STEP Taxware Batch process for Hierachy changes' not working

   ***********************************************************************************************/

   --initialize
   l_flex_value_set_id   NUMBER DEFAULT NULL;
   l_tax_flex_value      VARCHAR2 (100) DEFAULT 'XXWC_TAX_EXEMPTION_TYPE';
   -- Error DEBUG
   l_sec                 VARCHAR2 (150);
   l_err_callfrom        VARCHAR2 (75) DEFAULT 'XXWC_BATCH_TAXEC_Trg';
   l_distro_list         VARCHAR2 (75)
                            DEFAULT 'hdsoracledevelopers@hdsupply.com';
BEGIN

   mo_global.set_policy_context('S',162);   --Added for ver# 1.3


   l_sec := ' getting tax exemption flexfield.  ';

   SELECT flex_value_set_id
     INTO l_flex_value_set_id
     FROM applsys.fnd_flex_value_sets
    WHERE flex_value_set_name = l_tax_flex_value;



   -- only do this piece if the tax excemption was changed.
   -- should only fire for XXWC_TAX_EXEMPTION_TYPE.
   --IF :old.flex_value_set_id = l_flex_value_set_id THEN


   IF UPDATING OR DELETING
   THEN
      l_sec := 'looping and inserting ';

      FOR c_customer
         IN (SELECT psites.party_site_number
               FROM hz_cust_acct_sites sites, hz_party_sites psites
              WHERE     sites.party_site_id = psites.party_site_id
                    AND sites.attribute15 = :old.Parent_Flex_Value)
      LOOP
         BEGIN
            INSERT INTO XXWC.XXWC_BATCH_TAXEC_TBL
                 VALUES (
                           c_customer.party_site_number
                          ,:old.parent_flex_value);
         EXCEPTION
            WHEN OTHERS
            THEN                                       --values already exist.
               NULL;
         END;
      END LOOP;
   END IF;



   IF INSERTING
   THEN
      l_sec := 'looping and inserting ';

      FOR c_customer
         IN (SELECT psites.party_site_number
               FROM hz_cust_acct_sites sites, hz_party_sites psites
              WHERE     sites.party_site_id = psites.party_site_id
                    AND sites.attribute15 = :new.Parent_Flex_Value)
      LOOP
         BEGIN
            INSERT INTO XXWC.XXWC_BATCH_TAXEC_TBL
                 VALUES (
                           c_customer.party_site_number
                          ,:new.parent_flex_value);
         EXCEPTION
            WHEN OTHERS
            THEN                                       --values already exist.
               NULL;
         END;
      END LOOP;
   END IF;
--end IF;

EXCEPTION
   WHEN OTHERS
   THEN
      xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => l_err_callfrom
        ,p_calling             => 'STEP Taxware flex table monitor'
        ,--p_request_id => l_req_id,
         p_ora_error_msg       => SQLERRM
        ,p_error_desc          =>    'Error insert into values into XXWC_BATCH_TAXEC_TBL at '
                                  || l_sec
        ,p_distribution_list   => l_distro_list
        ,p_module              => 'AR');
END;
/
