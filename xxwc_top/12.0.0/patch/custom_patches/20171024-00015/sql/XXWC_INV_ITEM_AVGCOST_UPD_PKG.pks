CREATE OR REPLACE PACKAGE APPS.XXWC_INV_ITEM_AVGCOST_UPD_PKG
IS
   /*****************************************************************************************************************************************
   -- File Name: XXWC_INV_ITEM_AVGCOST_UPD_PKG.pks
   --
   -- PROGRAM TYPE: PL/SQL Script   <API>
   --
   -- PURPOSE: To update Average cost information for items.
   --
   -- HISTORY
   -- ========================================================================================================================================
   -- ========================================================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- --------------------------------------------------------------------------------------------------
   -- 1.0     11-SEP-2015   P.vamshidhar    TMS#20150609-00199 - Cost - Create mass update tool for average cost updates
   --                                       Initial Version.
   -- 1.1     02-MAR-2018   A.Pattabhi    TMS#20171024-00015 - Automating inventory adjustment transactions

   ******************************************************************************************************************************************/

   PROCEDURE IMPORT_ADI_DATA_PRC  (p_org_code    IN VARCHAR2,
                                   p_account     IN VARCHAR2,
                                   p_Item_num    IN VARCHAR2,
                                   p_cost_type   IN VARCHAR2,
                                   p_new_cost    in NUMBER); 

   PROCEDURE AVG_COST_UPDATE_PRC (p_org_code    IN VARCHAR2,
                                  p_account     IN VARCHAR2,
                                  p_Item_num    IN VARCHAR2,
                                  p_cost_type   IN VARCHAR2,
                                  p_new_cost    IN NUMBER,
                                  o_ret_msg     OUT VARCHAR2);								   

                                
END XXWC_INV_ITEM_AVGCOST_UPD_PKG;
/
