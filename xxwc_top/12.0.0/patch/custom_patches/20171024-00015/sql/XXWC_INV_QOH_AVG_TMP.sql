/*************************************************************************
*   SCRIPT Name: XXWC_INV_QOH_AVG_TMP
*
*   PURPOSE: Created Table XXWC_INV_QOH_AVG_TMP, this table will be
             used in XXWC_INV_QOH_AVG_TRANS_PRC procedure

HISTORY
===============================================================================
VERSION DATE          AUTHOR(S)           DESCRIPTION
------- -----------   --------------- -----------------------------------------
1.0     03/01/2018    P.Vamshidhar        TMS#20171024-00015 Initial version 
*****************************************************************************/
CREATE GLOBAL TEMPORARY TABLE XXWC.XXWC_INV_QOH_AVG_TMP
   (ORG_CODE VARCHAR2(10)
   ) ON COMMIT DELETE ROWS
/