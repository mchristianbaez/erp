CREATE OR REPLACE PACKAGE BODY xxwc_inv_qoh_avg_trans_pkg AS
  /*******************************************************************************
  * Package:   XXWC_INV_QOH_AVG_TRANS_PKG
  * Description: This package is calling from TRANSFER_BUTTON in
                 XXWC_INV_ITEM_AVG_ONH_TRANS.fmb.
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)        DESCRIPTION
  ------- -----------------  ---------------  -----------------------------------------
  1.0     10-Mar-2018        A.Pattabhi       Initial creation of the procedure
                                              TMS#20171024-00015
  2.0     02-May-2018        Naveen Kalidindi Overhaul. TMS#20171024-00015
  ********************************************************************************/



  /*******************************************************************************
  * Procedure:   Populate_Report_Table
  * Description: This procedure populates Report Table for the XXWC Onhand Transfer 
                 and Average cost update form Report.
  ********************************************************************************/
  PROCEDURE populate_report_table(p_xxwc_inv_item_avg_onh_trans t_xxwc_inv_item_avg_onh_trans
                                 ,o_ret_msg                     OUT VARCHAR2) IS
  BEGIN
  
    --
    INSERT INTO xxwc.xxwc_inv_item_avg_onh_trans_t
      (process_id
      ,branch
      ,from_item
      ,from_item_desc
      ,from_primary_uom
      ,from_avg_cost
      ,from_onhand_qty
      ,to_item
      ,to_item_desc
      ,to_primary_uom
      ,to_avg_cost
      ,to_onhand_qty
      ,SOURCE
      ,last_update_date
      ,last_updated_by
      ,creation_date
      ,created_by
      ,from_flag)
    VALUES
      (p_xxwc_inv_item_avg_onh_trans.process_id
      ,p_xxwc_inv_item_avg_onh_trans.branch
      ,p_xxwc_inv_item_avg_onh_trans.from_item
      ,p_xxwc_inv_item_avg_onh_trans.from_item_desc
      ,p_xxwc_inv_item_avg_onh_trans.from_primary_uom
      ,p_xxwc_inv_item_avg_onh_trans.from_avg_cost
      ,p_xxwc_inv_item_avg_onh_trans.from_onhand_qty --nvl(r_frmvalidrec_stg.transaction_quantity          ,0) * -1
      ,p_xxwc_inv_item_avg_onh_trans.to_item
      ,p_xxwc_inv_item_avg_onh_trans.to_item_desc
      ,p_xxwc_inv_item_avg_onh_trans.to_primary_uom
      ,p_xxwc_inv_item_avg_onh_trans.to_avg_cost
      ,p_xxwc_inv_item_avg_onh_trans.to_onhand_qty
      ,'Account alias'
      ,SYSDATE
      ,p_xxwc_inv_item_avg_onh_trans.last_updated_by
      ,SYSDATE
      ,p_xxwc_inv_item_avg_onh_trans.created_by
      ,p_xxwc_inv_item_avg_onh_trans.from_flag);
    --
  EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      o_ret_msg := 'Unknown error in populate_report_table, Error: ' || SQLERRM;
    
  END populate_report_table;

  /*******************************************************************************
  * Procedure:   Onhand_Transfer
  * Description: This procedure will get called when TRANSFER_BUTTON in
                 XXWC_INV_ITEM_AVG_ONH_TRANS.fmb is used.
  ********************************************************************************/
  PROCEDURE onhand_transfer(p_from_item_id         IN NUMBER
                           ,p_to_item_id           IN NUMBER
                           ,p_to_item              IN VARCHAR2
                           ,p_org_code             IN VARCHAR2
                           ,p_organization_id      IN NUMBER
                           ,p_frm_uom              IN VARCHAR2
                           ,p_to_uom               IN VARCHAR2
                           ,p_from_item_cost       IN VARCHAR2
                           ,p_avg_cost             IN NUMBER
                           ,p_transaction_quantity IN NUMBER
                           ,p_to_item_qty          IN NUMBER
                           ,p_user_id              IN NUMBER
                           ,o_ret_msg              OUT VARCHAR2) IS
  
    ln_process_flag NUMBER := 1;
    ln_lock_flag    NUMBER := 2;
  
    lvc_source                  VARCHAR2(100) := 'TYPE 14';
    ln_transaction_type_id      NUMBER;
    ln_source_line_id           NUMBER := 99;
    ln_source_header_id         NUMBER := 99;
    ln_transaction_mode         NUMBER := 3;
    ln_accts_pay_code_comb_id   gl_code_combinations.code_combination_id%TYPE;
    lvc_new_acct                VARCHAR2(10) := '501010';
    lvc_ret_msg                 VARCHAR2(32767);
    ln_transaction_header_id_ex VARCHAR2(100) := to_char(SYSDATE
                                                        ,'DDHH24MISS');
    lvc_err_msg                 VARCHAR2(32767);
    ln_tran_interface_id        NUMBER;
    ln_transaction_source_id    NUMBER;
  
  BEGIN
    lvc_ret_msg := NULL;
  
    -- Average cost update.
    xxwc_inv_item_avgcost_upd_pkg.avg_cost_update_prc(p_org_code  => p_org_code
                                                     ,p_account   => lvc_new_acct
                                                     ,p_item_num  => p_to_item
                                                     ,p_cost_type => 'MATERIAL'
                                                     ,p_new_cost  => p_avg_cost
                                                     ,o_ret_msg   => lvc_ret_msg);
  
    IF lvc_ret_msg IS NOT NULL THEN
      lvc_err_msg := lvc_ret_msg;
      RAISE program_error;
    END IF;
  
  
    BEGIN
      SELECT disposition_id
            ,distribution_account
        INTO ln_transaction_source_id
            ,ln_accts_pay_code_comb_id
        FROM mtl_generic_dispositions
       WHERE organization_id = p_organization_id
         AND segment1 = lvc_source;
    EXCEPTION
      WHEN OTHERS THEN
        lvc_err_msg := 'Error occured while checking trx source';
        RAISE program_error;
    END;
  
    -- Onhand transfer for From Item 
    ln_transaction_type_id := 31;
    ln_tran_interface_id   := mtl_material_transactions_s.nextval;
  
    BEGIN
      INSERT INTO mtl_transactions_interface
        (transaction_interface_id
        ,transaction_header_id
        ,transaction_uom
        ,transaction_date
        ,source_code
        ,source_line_id
        ,source_header_id
        ,process_flag
        ,transaction_mode
        ,lock_flag
        ,locator_id
        ,last_update_date
        ,last_updated_by
        ,creation_date
        ,created_by
        ,inventory_item_id
        ,subinventory_code
        ,organization_id
        ,transaction_source_name
        ,transaction_source_id
        ,transaction_quantity
        ,primary_quantity
        ,transaction_type_id
        ,transaction_cost
        ,transaction_reference)
      VALUES
        (ln_tran_interface_id -- transaction_interface_id
        ,ln_transaction_header_id_ex
        ,p_frm_uom
        ,SYSDATE --transaction date
        ,'Account alias' --source code
        ,ln_source_line_id --source line id
        ,ln_source_header_id --source header id
        ,ln_process_flag --process flag
        ,ln_transaction_mode --transaction mode
        ,ln_lock_flag --lock flag
        ,NULL --locator id
        ,SYSDATE --last update date
        ,p_user_id --last updated by
        ,SYSDATE --creation date
        ,p_user_id --created by
        ,p_from_item_id --inventory item id
        ,'General' --From subinventory code
        ,p_organization_id --organization id
        ,lvc_source --transaction source
        ,ln_transaction_source_id --transaction source id
        ,nvl(p_transaction_quantity
            ,0) * -1 --transaction quantity
        ,p_transaction_quantity --Primary quantity
        ,ln_transaction_type_id --transaction type id
        ,p_from_item_cost
        ,p_to_item);
    EXCEPTION
      WHEN OTHERS THEN
        lvc_err_msg := 'Error occured: Inserting data into trx interface1 ' ||
                       substr(SQLERRM
                             ,1
                             ,250);
        RAISE program_error;
    END;
  
  
    ln_tran_interface_id   := mtl_material_transactions_s.nextval;
    ln_transaction_type_id := 41;
  
    --- Inserting MTL_TRANSACTIONS_INTERFACE to receive items as To Item.
    BEGIN
      INSERT INTO mtl_transactions_interface
        (transaction_interface_id
        ,transaction_header_id
        ,transaction_uom
        ,transaction_date
        ,source_code
        ,source_line_id
        ,source_header_id
        ,process_flag
        ,transaction_mode
        ,lock_flag
        ,locator_id
        ,last_update_date
        ,last_updated_by
        ,creation_date
        ,created_by
        ,inventory_item_id
        ,subinventory_code
        ,organization_id
        ,transaction_source_name
        ,transaction_source_id
        ,transaction_quantity
        ,primary_quantity
        ,transaction_type_id
        ,transaction_cost)
      VALUES
        (ln_tran_interface_id -- transaction_interface_id
        ,ln_transaction_header_id_ex
        ,p_to_uom
        ,SYSDATE --transaction date
        ,'Account alias' --source code
        ,ln_source_line_id --source line id
        ,ln_source_header_id --source header id
        ,ln_process_flag --process flag
        ,ln_transaction_mode --transaction mode
        ,ln_lock_flag --lock flag
        ,NULL --locator id
        ,SYSDATE --last update date
        ,p_user_id --last updated by
        ,SYSDATE --creation date
        ,p_user_id --created by
        ,p_to_item_id --inventory item id
        ,'General' --From subinventory code
        ,p_organization_id --organization id
        ,lvc_source --transaction source
        ,ln_transaction_source_id --transaction source id
        ,p_to_item_qty --transaction quantity
        ,p_to_item_qty --Primary quantity
        ,ln_transaction_type_id --transaction type id
        ,p_avg_cost --transactional_cost
         );
    EXCEPTION
      WHEN OTHERS THEN
        lvc_err_msg := 'Error occured: Inserting data into trx interface2 ' ||
                       substr(SQLERRM
                             ,1
                             ,250);
        RAISE program_error;
    END;
    --
  
  EXCEPTION
    --
    WHEN program_error THEN
      ROLLBACK;
      o_ret_msg := lvc_err_msg;
      --
    WHEN OTHERS THEN
      ROLLBACK;
      o_ret_msg := substr('Unknown Error, onhand_transfer: ' || SQLERRM
                         ,1
                         ,250);
    
  END onhand_transfer;

  /*******************************************************************************
  * Procedure:   Process_Request
  * Description: This procedure will get called from both TRANSFER_BUTTON and 
                 REPORT_BUTTON in XXWC_INV_ITEM_AVG_ONH_TRANS.fmb.
                 For Transfer: p_mode = 'T'
                 For Report: p_mode = 'R'
  ********************************************************************************/
  PROCEDURE process_request(p_process_id       IN NUMBER
                           ,p_from_item_id     IN NUMBER
                           ,p_from_item        IN VARCHAR2
                           ,p_from_description IN VARCHAR2
                           ,p_from_primary_uom IN VARCHAR2
                           ,p_from_std_rate    IN NUMBER
                           ,p_from_std_uom     IN VARCHAR2
                           ,p_to_item_id       IN NUMBER
                           ,p_to_item          IN VARCHAR2
                           ,p_to_description   IN VARCHAR2
                           ,p_to_primary_uom   IN VARCHAR2
                           ,p_to_std_rate      IN NUMBER
                           ,p_to_std_uom       IN VARCHAR2
                           ,p_cust_frm_rate    IN NUMBER
                           ,p_org_or_branchs   IN VARCHAR2
                           ,p_user_id          IN NUMBER
                           ,p_mode             IN VARCHAR2
                           ,o_ret_msg          OUT VARCHAR2
                           ,o_err_flag         OUT VARCHAR2) IS
  
    -- Get Items(OnHand and Orgs) list to be processed.
    CURSOR cur_validrec_stg IS
      SELECT msib.inventory_item_id
            ,msib.organization_id
            ,msib.primary_uom_code
            ,mp.organization_code
            ,msib.segment1
            ,SUM(nvl(moq.transaction_quantity
                    ,0)) transaction_quantity
        FROM apps.mtl_system_items_b    msib
            ,apps.mtl_onhand_quantities moq
            ,apps.mtl_parameters        mp
       WHERE msib.inventory_item_id = moq.inventory_item_id
         AND msib.organization_id = moq.organization_id
         AND msib.inventory_item_id = p_from_item_id
         AND moq.subinventory_code = 'General'
         AND msib.organization_id = mp.organization_id
         AND moq.transaction_quantity > 0
         AND EXISTS (SELECT 1
                FROM xxwc.xxwc_inv_qoh_avg_tmp xiqa
                    ,apps.mtl_system_items_b   msib
               WHERE xiqa.org_code = mp.organization_code
                    -- check for only those where To Item is assigned.
                 AND inventory_item_id = p_to_item_id
                 AND msib.organization_id = mp.organization_id)
       GROUP BY msib.inventory_item_id
               ,msib.organization_id
               ,msib.primary_uom_code
               ,mp.organization_code
               ,msib.segment1;
  
    CURSOR cur_org_list IS
      SELECT org_code
        FROM xxwc.xxwc_inv_qoh_avg_tmp;
  
    ln_from_item_cost              NUMBER;
    ln_to_item_cost                NUMBER;
    ln_go_item_avg_cost            NUMBER;
    lvc_org_list                   VARCHAR2(32767);
    lvc_org_code                   mtl_parameters.organization_code%TYPE;
    ln_uom_rate                    NUMBER;
    ln_from_item_qty               NUMBER;
    ln_to_item_qty                 NUMBER := 0;
    lvc_ret_msg                    VARCHAR2(32767);
    lr_xxwc_inv_item_avg_onh_trans t_xxwc_inv_item_avg_onh_trans;
    lvc_err_msg                    VARCHAR2(32767);
    lvc_war_msg                    VARCHAR2(32767);
    ln_count                       NUMBER := 0;
    ln_process_id                  NUMBER;
    l_sec                          VARCHAR2(10);
  
  
    lv_from_uom_code mtl_units_of_measure_tl.uom_code%TYPE;
    lv_to_uom_code   mtl_units_of_measure_tl.uom_code%TYPE;
  
  BEGIN
  
    o_err_flag    := 'N';
    lvc_org_list  := p_org_or_branchs;
    ln_process_id := p_process_id;
  
    <<populate_temp_table>>
    BEGIN
      -- EXECUTE IMMEDIATE ('truncate table XXWC.XXWC_INV_QOH_AVG_TMP');
      l_sec := '10';
      IF lvc_org_list IS NOT NULL THEN
        LOOP
          IF nvl(instr(lvc_org_list
                      ,','
                      ,1)
                ,0) = 0 THEN
            lvc_org_code := lvc_org_list;
            --
            INSERT INTO xxwc.xxwc_inv_qoh_avg_tmp
              (org_code)
            VALUES
              (lvc_org_code);
            --
            lvc_org_list := NULL;
            --
          ELSE
            lvc_org_code := substr(lvc_org_list
                                  ,1
                                  ,instr(lvc_org_list
                                        ,','
                                        ,1) - 1);
            lvc_org_list := substr(lvc_org_list
                                  ,instr(lvc_org_list
                                        ,','
                                        ,1) + 1);
          
            INSERT INTO xxwc.xxwc_inv_qoh_avg_tmp
              (org_code)
            VALUES
              (lvc_org_code);
          END IF;
        
          EXIT WHEN lvc_org_list IS NULL;
        END LOOP;
        --
      ELSE
        --
        INSERT INTO xxwc.xxwc_inv_qoh_avg_tmp
          (org_code)
          (SELECT DISTINCT organization_code
             FROM mtl_onhand_quantities a
                 ,apps.mtl_parameters   b
            WHERE a.organization_id = b.organization_id
              AND a.inventory_item_id = p_from_item_id);
      END IF;
    
    END populate_temp_table;
    l_sec := '20';
    <<validations>>
    BEGIN
      -- Get UOM rate
      IF (p_from_std_rate IS NULL OR p_to_std_rate IS NULL)
         AND p_cust_frm_rate IS NULL THEN
        --
        lvc_err_msg := 'UOM conversion has not been defined for' || p_from_std_rate || ' to ' ||
                       p_to_primary_uom;
        --
        RAISE program_error;
      END IF;
    
      -- Check From and To Item Org Assignments, but only for Transfer
      IF p_mode = 'T'
         AND p_org_or_branchs IS NOT NULL THEN
        --
        FOR rec_org_list IN cur_org_list LOOP
          BEGIN
            ln_count := 0;
          
            SELECT COUNT(1)
              INTO ln_count
              FROM mtl_system_items_b  msib
                  ,apps.mtl_parameters mp
             WHERE msib.organization_id = mp.organization_id
               AND mp.organization_code = rec_org_list.org_code
               AND msib.inventory_item_id = p_from_item_id;
          
            IF ln_count = 0 THEN
              lvc_err_msg := lvc_err_msg || '*' || p_from_item || ' Item not assigned to ' ||
                             rec_org_list.org_code || '*';
            END IF;
          EXCEPTION
            WHEN OTHERS THEN
              lvc_err_msg := 'Unknown Error occured: From Item assignment checking ' ||
                             substr(SQLERRM
                                   ,1
                                   ,250);
          END;
          --
          BEGIN
            ln_count := 0;
          
            SELECT COUNT(1)
              INTO ln_count
              FROM mtl_system_items_b  msib
                  ,apps.mtl_parameters mp
             WHERE msib.organization_id = mp.organization_id
               AND mp.organization_code = rec_org_list.org_code
               AND msib.inventory_item_id = p_to_item_id;
          
            IF ln_count = 0 THEN
              lvc_err_msg := lvc_err_msg || '*' || p_to_item || ' Item not assigned to ' ||
                             rec_org_list.org_code || '*';
            END IF;
          EXCEPTION
            WHEN OTHERS THEN
              lvc_err_msg := 'Error occured: To Item assignment checking ' ||
                             substr(SQLERRM
                                   ,1
                                    
                                   ,250);
          END;
        END LOOP;
        --
        IF lvc_err_msg IS NOT NULL THEN
          lvc_err_msg := lvc_err_msg || ' - *** Transfer Cancelled! ***';
          RAISE program_error;
        END IF;
        --
      
      ELSE
        -- Warning if TO ITEM is not assigned to From Organization.
        FOR recw_org_list IN cur_org_list LOOP
          BEGIN
            ln_count := 0;
          
            SELECT COUNT(1)
              INTO ln_count
              FROM mtl_system_items_b  msib
                  ,apps.mtl_parameters mp
             WHERE msib.organization_id = mp.organization_id
               AND mp.organization_code = recw_org_list.org_code
               AND msib.inventory_item_id = p_to_item_id;
          
            IF ln_count = 0 THEN
              lvc_war_msg := lvc_war_msg || ' * ' || p_to_item || ' Item not assigned to ' ||
                             recw_org_list.org_code || ' * ';
            END IF;
          EXCEPTION
            WHEN OTHERS THEN
              lvc_err_msg := 'Error occured: *Warning* To Item assignment checking ' ||
                             substr(SQLERRM
                                   ,1
                                    
                                   ,250);
          END;
        END LOOP;
      END IF;
      --
      -- Get UOM Codes
      BEGIN
        SELECT uom.uom_code
          INTO lv_from_uom_code
          FROM mtl_units_of_measure_tl uom
         WHERE uom.unit_of_measure = p_from_std_uom
           AND LANGUAGE = userenv('LANG');
      
        SELECT uom.uom_code
          INTO lv_to_uom_code
          FROM mtl_units_of_measure_tl uom
         WHERE uom.unit_of_measure = p_to_std_uom
           AND LANGUAGE = userenv('LANG');
      
      EXCEPTION
        WHEN OTHERS THEN
          lvc_err_msg := 'Error deriving UOM Codes (From, To): (' || p_from_std_uom || ',' ||
                         p_to_std_uom || ')' || substr(SQLERRM
                                                      ,1
                                                      ,250);
          RAISE program_error;
      END;
    END validations;
  
    l_sec := '30';
    FOR rec_validrec_stg IN cur_validrec_stg LOOP
    
      --
      ln_from_item_qty    := rec_validrec_stg.transaction_quantity;
      ln_to_item_qty      := 0;
      ln_go_item_avg_cost := 0;
      ln_from_item_cost   := 0;
      ln_to_item_cost     := 0;
      ln_uom_rate         := NULL;
      --
    
      l_sec := '40';
    
      <<calculate_std_uom>>
    --*** Redundant. Already calculated in form.
      BEGIN
        IF p_from_primary_uom <> p_to_primary_uom THEN
        
          ln_uom_rate := inv_convert.inv_um_convert(p_from_item_id
                                                   ,5
                                                   ,ln_from_item_qty
                                                   ,rec_validrec_stg.primary_uom_code
                                                   ,p_to_std_uom
                                                   ,NULL
                                                   ,NULL);
        
        END IF;
      END calculate_std_uom;
    
      l_sec := '50';
      <<get_current_cost>>
      BEGIN
      
        -- Get Cost for From Item
        BEGIN
          --
          SELECT to_number(substr(material_cost
                                 ,1
                                 ,6))
            INTO ln_from_item_cost
            FROM apps.cst_item_cost_type_v
           WHERE inventory_item_id = p_from_item_id
             AND material_cost IS NOT NULL
             AND organization_id = rec_validrec_stg.organization_id
             AND cost_type = 'Average';
          --
        EXCEPTION
          WHEN no_data_found THEN
            --
            ln_from_item_cost := 0;
            --
          WHEN OTHERS THEN
            --
            lvc_err_msg := 'Unknown: Error during derivation of Average cost for "From Item" ' ||
                           p_from_item || ', please contact System Administrator';
            --
            RAISE program_error;
        END;
      
        l_sec := '60';
        -- Get Cost for To Item
        BEGIN
          SELECT to_number(substr(material_cost
                                 ,1
                                 ,6))
            INTO ln_to_item_cost
            FROM apps.cst_item_cost_type_v
           WHERE inventory_item_id = p_to_item_id
             AND material_cost IS NOT NULL
             AND organization_id = rec_validrec_stg.organization_id
             AND cost_type = 'Average';
          --
        EXCEPTION
          WHEN no_data_found THEN
            --
            ln_to_item_cost := 0;
            --
          WHEN OTHERS THEN
            --
            lvc_err_msg := 'Unknown: Error during derivation of Average cost for "To Item" ' ||
                           p_to_item || ', please contact System Administrator';
            --
            RAISE program_error;
        END;
      
      END get_current_cost;
      l_sec := '70';
      <<calculate_cost_qty>>
      BEGIN
      
        -- If both the UOMs are same, push the "From Item" Avg Cost and Item Qty to "To Item"
        IF p_from_std_uom = p_to_std_uom THEN
          --
          ln_go_item_avg_cost := ln_from_item_cost;
          ln_to_item_qty      := rec_validrec_stg.transaction_quantity;
          --
        ELSE
          -- When Std Conversion Rates are defined, these take precedence over custom rate.
          IF p_from_std_rate IS NOT NULL
             AND p_to_std_rate IS NOT NULL THEN
          
            -- When std conversion rates are different.
            IF p_from_std_rate <> p_to_std_rate THEN
              --
              ln_go_item_avg_cost := round((ln_from_item_cost / p_to_std_rate)
                                          ,5);
              ln_to_item_qty      := rec_validrec_stg.transaction_quantity * p_to_std_rate;
            
              -- When std conversion rates are equal.
            ELSE
              --
              ln_go_item_avg_cost := round(ln_from_item_cost
                                          ,5);
              ln_to_item_qty      := rec_validrec_stg.transaction_quantity;
              --
            END IF;
          
            -- When Custom Rate is defined in form
          ELSIF (p_from_std_rate IS NULL OR p_to_std_rate IS NULL)
                AND p_cust_frm_rate IS NOT NULL THEN
            --
            ln_go_item_avg_cost := round((ln_from_item_cost * p_cust_frm_rate)
                                        ,5);
            ln_to_item_qty      := rec_validrec_stg.transaction_quantity / p_cust_frm_rate;
          
            -- Catch All
          ELSE
            --
            ln_go_item_avg_cost := ln_from_item_cost;
            ln_to_item_qty      := rec_validrec_stg.transaction_quantity;
            --
          END IF;
          --
        END IF;
        --
      END calculate_cost_qty;
      --
    
    
      l_sec := '80';
      <<populate_report>>
      BEGIN
        lvc_ret_msg                    := NULL;
        lr_xxwc_inv_item_avg_onh_trans := NULL;
      
        lr_xxwc_inv_item_avg_onh_trans.process_id       := ln_process_id;
        lr_xxwc_inv_item_avg_onh_trans.branch           := rec_validrec_stg.organization_code;
        lr_xxwc_inv_item_avg_onh_trans.from_item        := p_from_item;
        lr_xxwc_inv_item_avg_onh_trans.from_item_desc   := p_from_description;
        lr_xxwc_inv_item_avg_onh_trans.from_primary_uom := p_from_primary_uom;
        lr_xxwc_inv_item_avg_onh_trans.from_avg_cost    := ln_from_item_cost;
        lr_xxwc_inv_item_avg_onh_trans.from_onhand_qty  := nvl(rec_validrec_stg.transaction_quantity
                                                              ,0) * -1;
        lr_xxwc_inv_item_avg_onh_trans.to_item          := p_to_item;
        lr_xxwc_inv_item_avg_onh_trans.to_item_desc     := p_to_description;
        lr_xxwc_inv_item_avg_onh_trans.to_primary_uom   := p_to_primary_uom;
        lr_xxwc_inv_item_avg_onh_trans.to_avg_cost      := ln_go_item_avg_cost;
        lr_xxwc_inv_item_avg_onh_trans.to_onhand_qty    := ln_to_item_qty;
        lr_xxwc_inv_item_avg_onh_trans.last_updated_by  := p_user_id;
        lr_xxwc_inv_item_avg_onh_trans.created_by       := p_user_id;
        lr_xxwc_inv_item_avg_onh_trans.from_flag        := p_mode;
      
        populate_report_table(lr_xxwc_inv_item_avg_onh_trans
                             ,lvc_ret_msg);
      
        IF lvc_ret_msg IS NOT NULL THEN
          lvc_err_msg := lvc_ret_msg;
          RAISE program_error;
        END IF;
      
      END populate_report;
    
    
    
      l_sec := '90';
      IF p_mode = 'T' THEN
      
        lvc_ret_msg := NULL;
        <<onhand_transfer>>
        BEGIN
          --
          xxwc_inv_qoh_avg_trans_pkg.onhand_transfer(p_from_item_id         => p_from_item_id
                                                    ,p_to_item_id           => p_to_item_id
                                                    ,p_to_item              => p_to_item
                                                    ,p_org_code             => rec_validrec_stg.organization_code
                                                    ,p_organization_id      => rec_validrec_stg.organization_id
                                                    ,p_frm_uom              => lv_from_uom_code
                                                    ,p_to_uom               => lv_to_uom_code
                                                    ,p_from_item_cost       => ln_from_item_cost
                                                    ,p_avg_cost             => ln_go_item_avg_cost
                                                    ,p_transaction_quantity => rec_validrec_stg.transaction_quantity
                                                    ,p_to_item_qty          => ln_to_item_qty
                                                    ,p_user_id              => p_user_id
                                                    ,o_ret_msg              => lvc_ret_msg);
          --
          IF lvc_ret_msg IS NOT NULL THEN
            lvc_err_msg := lvc_ret_msg;
            RAISE program_error;
          END IF;
        
          l_sec := '100';
          --
        END onhand_transfer;
        --
      END IF;
      --
    END LOOP;
    l_sec := '110';
    COMMIT;
  
    -- Return Messages
    o_err_flag := 'N';
    o_ret_msg := (CASE
                   WHEN lvc_war_msg IS NOT NULL THEN
                    '*Warning* - ' || lvc_war_msg
                   ELSE
                    NULL
                 END);
  
    --
  
  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
      o_err_flag := 'Y';
      o_ret_msg  := lvc_err_msg;
    WHEN OTHERS THEN
      ROLLBACK;
      o_err_flag := 'Y';
      o_ret_msg  := l_sec || ': ' || substr(SQLERRM
                                           ,1
                                           ,250);
  END;

END xxwc_inv_qoh_avg_trans_pkg;
/
