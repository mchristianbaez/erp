--Report Name            : Rental Utilization and Usage - WC
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating View Data for Rental Utilization and Usage - WC
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_OM_RENTAL_ITEMS_V1
xxeis.eis_rs_ins.v( 'EIS_XXWC_OM_RENTAL_ITEMS_V1',660,'','','','','SA059956','XXEIS','Eis Xxwc Om Rental Items V','EXORIV','','');
--Delete View Columns for EIS_XXWC_OM_RENTAL_ITEMS_V1
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_OM_RENTAL_ITEMS_V1',660,FALSE);
--Inserting View Columns for EIS_XXWC_OM_RENTAL_ITEMS_V1
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_RENTAL_ITEMS_V1','ITEM_DESCRIPTION',660,'Item Description','ITEM_DESCRIPTION','','','','SA059956','VARCHAR2','','','Item Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_RENTAL_ITEMS_V1','ITEM_NUMBER',660,'Item Number','ITEM_NUMBER','','','','SA059956','VARCHAR2','','','Item Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_RENTAL_ITEMS_V1','LOCATION',660,'Location','LOCATION','','','','SA059956','VARCHAR2','','','Location','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_RENTAL_ITEMS_V1','QTY_ON_RENT',660,'Qty On Rent','QTY_ON_RENT','','','','SA059956','NUMBER','','','Qty On Rent','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_RENTAL_ITEMS_V1','REGION',660,'Region','REGION','','','','SA059956','VARCHAR2','','','Region','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_RENTAL_ITEMS_V1','DEMAND_QTY',660,'Demand Qty','DEMAND_QTY','','','','SA059956','NUMBER','','','Demand Qty','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_RENTAL_ITEMS_V1','VARIANC',660,'Varianc','VARIANC','','','','SA059956','NUMBER','','','Varianc','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_RENTAL_ITEMS_V1','UTILIZATION_RATE',660,'Utilization Rate','UTILIZATION_RATE','','','','SA059956','NUMBER','','','Utilization Rate','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_RENTAL_ITEMS_V1','OWNED',660,'Owned','OWNED','','','','SA059956','NUMBER','','','Owned','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_RENTAL_ITEMS_V1','ON_HAND',660,'On Hand','ON_HAND','','','','SA059956','NUMBER','','','On Hand','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_RENTAL_ITEMS_V1','AVERAGE_COST',660,'Average Cost','AVERAGE_COST','','','','SA059956','NUMBER','','','Average Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_RENTAL_ITEMS_V1','ACTUAL_RENT_DAYS',660,'Actual Rent Days','ACTUAL_RENT_DAYS','','','','SA059956','NUMBER','','','Actual Rent Days','','','');
--Inserting View Components for EIS_XXWC_OM_RENTAL_ITEMS_V1
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_RENTAL_ITEMS_V1','OE_ORDER_LINES',660,'OE_ORDER_LINES_ALL','OL','OL','SA059956','SA059956','-1','Oe Order Lines All Stores Information For All Orde','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_RENTAL_ITEMS_V1','OE_ORDER_HEADERS',660,'OE_ORDER_HEADERS_ALL','OH','OH','SA059956','SA059956','-1','Oe Order Headers All Stores Header Information For','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_RENTAL_ITEMS_V1','HZ_PARTIES',660,'HZ_PARTIES','HZP','HZP','SA059956','SA059956','-1','Information About Parties Such As Organizations, P','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_RENTAL_ITEMS_V1','MTL_PARAMETERS',660,'MTL_PARAMETERS','MTP','MTP','SA059956','SA059956','-1','Inventory Control Options And Defaults','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_RENTAL_ITEMS_V1','MTL_SYSTEM_ITEMS_KFV',660,'MTL_SYSTEM_ITEMS_B','MSI','MSI','SA059956','SA059956','-1','Inventory Item Definitions','','','','');
--Inserting View Component Joins for EIS_XXWC_OM_RENTAL_ITEMS_V1
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_RENTAL_ITEMS_V1','OE_ORDER_LINES','OL',660,'EXORIV.LINE_ID','=','OL.LINE_ID(+)','','','','Y','SA059956','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_RENTAL_ITEMS_V1','OE_ORDER_HEADERS','OH',660,'EXORIV.HEADER_ID','=','OH.HEADER_ID(+)','','','','Y','SA059956','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_RENTAL_ITEMS_V1','HZ_PARTIES','HZP',660,'EXORIV.PARTY_ID','=','HZP.PARTY_ID(+)','','','','Y','SA059956','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_RENTAL_ITEMS_V1','MTL_PARAMETERS','MTP',660,'EXORIV.ORGANIZATION_ID','=','MTP.ORGANIZATION_ID(+)','','','','Y','SA059956','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_RENTAL_ITEMS_V1','MTL_SYSTEM_ITEMS_KFV','MSI',660,'EXORIV.INVENTORY_ITEM_ID','=','MSI.INVENTORY_ITEM_ID(+)','','','','Y','SA059956','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_RENTAL_ITEMS_V1','MTL_SYSTEM_ITEMS_KFV','MSI',660,'EXORIV.MSI_ORGANIZATION_ID','=','MSI.ORGANIZATION_ID(+)','','','','Y','SA059956','','');
END;
/
set scan on define on
prompt Creating Report LOV Data for Rental Utilization and Usage - WC
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Rental Utilization and Usage - WC
xxeis.eis_rs_ins.lov( 660,'SELECT ORGANIZATION_CODE WAREHOUSE,ORGANIZATION_NAME FROM ORG_ORGANIZATION_DEFINITIONS OOD WHERE SYSDATE < NVL(OOD.DISABLE_DATE,SYSDATE+1) AND EXISTS (SELECT 1 FROM XXEIS.EIS_ORG_ACCESS_V  WHERE organization_id = ood.organization_id )','','OM WAREHOUSE','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( '','select msi.segment1 item_number, msi.description item_description from apps.mtl_system_items msi where organization_id=222
and msi.segment1 like ''R%''
UNION
select ''All Rental Items'' item_number,''All Rental Items'' item_description from dual','','XX EIS WC RENTAL ITEM LOV','To display Rental item list','ANONYMOUS',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Rental Utilization and Usage - WC
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Rental Utilization and Usage - WC
xxeis.eis_rs_utility.delete_report_rows( 'Rental Utilization and Usage - WC' );
--Inserting Report - Rental Utilization and Usage - WC
xxeis.eis_rs_ins.r( 660,'Rental Utilization and Usage - WC','','Rental Utilization and Usage - WC','','','','SA059956','EIS_XXWC_OM_RENTAL_ITEMS_V1','Y','','','SA059956','','N','White Cap Reports','RTF,PDF,','CSV,HTML,Html Summary,XML,Pivot Excel,EXCEL,','');
--Inserting Report Columns - Rental Utilization and Usage - WC
xxeis.eis_rs_ins.rc( 'Rental Utilization and Usage - WC',660,'DEMAND_QTY','Quantity Demand','Demand Qty','','~~~','default','','7','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_RENTAL_ITEMS_V1','','');
xxeis.eis_rs_ins.rc( 'Rental Utilization and Usage - WC',660,'ITEM_DESCRIPTION','Item Description','Item Description','','','default','','4','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_RENTAL_ITEMS_V1','','');
xxeis.eis_rs_ins.rc( 'Rental Utilization and Usage - WC',660,'ITEM_NUMBER','Item Number','Item Number','','','default','','3','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_RENTAL_ITEMS_V1','','');
xxeis.eis_rs_ins.rc( 'Rental Utilization and Usage - WC',660,'LOCATION','Location','Location','','','default','','2','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_RENTAL_ITEMS_V1','','');
xxeis.eis_rs_ins.rc( 'Rental Utilization and Usage - WC',660,'OWNED','Owned','Owned','','~~~','default','','5','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_RENTAL_ITEMS_V1','','');
xxeis.eis_rs_ins.rc( 'Rental Utilization and Usage - WC',660,'QTY_ON_RENT','Quantity On Rent','Qty On Rent','','~~~','default','','8','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_RENTAL_ITEMS_V1','','');
xxeis.eis_rs_ins.rc( 'Rental Utilization and Usage - WC',660,'REGION','Region','Region','','','default','','1','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_RENTAL_ITEMS_V1','','');
xxeis.eis_rs_ins.rc( 'Rental Utilization and Usage - WC',660,'UTILIZATION_RATE','Utilization Rate%','Utilization Rate','','~,~.~2','default','','10','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_RENTAL_ITEMS_V1','','');
xxeis.eis_rs_ins.rc( 'Rental Utilization and Usage - WC',660,'VARIANC','Varianc','Varianc','','~~~','default','','9','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_RENTAL_ITEMS_V1','','');
xxeis.eis_rs_ins.rc( 'Rental Utilization and Usage - WC',660,'ON_HAND','Quantity On Hand','On Hand','','~~~','default','','6','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_RENTAL_ITEMS_V1','','');
--Inserting Report Parameters - Rental Utilization and Usage - WC
xxeis.eis_rs_ins.rp( 'Rental Utilization and Usage - WC',660,'Warehouse','Warehouse','LOCATION','IN','OM WAREHOUSE','','VARCHAR2','N','Y','1','','Y','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Rental Utilization and Usage - WC',660,'Rental Item Number','Item Number','ITEM_NUMBER','IN','XX EIS WC RENTAL ITEM LOV','''All Rental Items''','VARCHAR2','Y','Y','2','','N','CONSTANT','SA059956','Y','N','','','');
--Inserting Report Conditions - Rental Utilization and Usage - WC
xxeis.eis_rs_ins.rcn( 'Rental Utilization and Usage - WC',660,'LOCATION','IN',':Warehouse','','','Y','2','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'Rental Utilization and Usage - WC',660,'','','','','AND (''All Rental Items'' IN (:Rental Item Number) or (ITEM_NUMBER in (:Rental Item Number)))
AND (QTY_ON_RENT<>0 or ON_HAND<>0 or DEMAND_QTY<>0)','Y','0','','SA059956');
--Inserting Report Sorts - Rental Utilization and Usage - WC
--Inserting Report Triggers - Rental Utilization and Usage - WC
--Inserting Report Templates - Rental Utilization and Usage - WC
--Inserting Report Portals - Rental Utilization and Usage - WC
--Inserting Report Dashboards - Rental Utilization and Usage - WC
--Inserting Report Security - Rental Utilization and Usage - WC
xxeis.eis_rs_ins.rsec( 'Rental Utilization and Usage - WC','660','','50886',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Rental Utilization and Usage - WC','660','','50860',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Rental Utilization and Usage - WC','660','','51044',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Rental Utilization and Usage - WC','20005','','50900',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Rental Utilization and Usage - WC','','10010432','',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Rental Utilization and Usage - WC','','SS084202','',660,'SA059956','','');
--Inserting Report Pivots - Rental Utilization and Usage - WC
xxeis.eis_rs_ins.rpivot( 'Rental Utilization and Usage - WC',660,'Pivot','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rs_ins.rpivot_dtls( 'Rental Utilization and Usage - WC',660,'Pivot','OWNED','DATA_FIELD','SUM','','1','','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Rental Utilization and Usage - WC',660,'Pivot','DEMAND_QTY','DATA_FIELD','SUM','','2','','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Rental Utilization and Usage - WC',660,'Pivot','QTY_ON_RENT','DATA_FIELD','SUM','','3','','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Rental Utilization and Usage - WC',660,'Pivot','VARIANC','DATA_FIELD','SUM','','4','','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Rental Utilization and Usage - WC',660,'Pivot','REGION','ROW_FIELD','','','1','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'Rental Utilization and Usage - WC',660,'Pivot','ITEM_DESCRIPTION','ROW_FIELD','','','2','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'Rental Utilization and Usage - WC',660,'Pivot','UTILIZATION_RATE','DATA_FIELD','AVE','','5','','xlNormal');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
END;
/
set scan on define on
