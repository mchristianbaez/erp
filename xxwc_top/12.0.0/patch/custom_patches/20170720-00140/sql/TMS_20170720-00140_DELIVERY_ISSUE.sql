/*************************************************************************
  $Header TMS_20170720-00140_DELIVERY_ISSUE.sql $
  Module Name: 20170720-00140  DELETE Delivery issue records 

  PURPOSE: Delivery documents are printed for the order and the delivery 
           has been created, however the delivery lines shows as cancelled 
		   status and user unable to progress the order   

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        25-JUL-2017  Pattabhi Avula        TMS# 20170720-00140 

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20170720-00140   , Before Update');

   DELETE FROM xxwc.xxwc_wsh_shipping_stg 
         WHERE header_id = 61367059
           AND delivery_id=7478147;


   DBMS_OUTPUT.put_line (
         'TMS: 20170720-00140 Number of records deleted (Expected:40): '
      || SQL%ROWCOUNT);	  


   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20170720-00140   , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20170720-00140, Errors : ' || SQLERRM);
END;
/