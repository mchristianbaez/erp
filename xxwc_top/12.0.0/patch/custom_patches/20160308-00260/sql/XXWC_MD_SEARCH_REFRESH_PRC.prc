CREATE OR REPLACE PROCEDURE XXWC_MD_SEARCH_REFRESH_PRC(p_err_buf     OUT VARCHAR2,
                                                       p_return_code OUT NUMBER) IS
  /*******************************************************************************
  * Procedure:   XXWC_MD_SEARCH_REFRESH_PRC
  * Description: TMS# 20160225-00123
                 This procedure is being called by UC4 job to refresh the MV
                 XXWC_MD_SEARCH_PRODUCTS_MV
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     25-Feb-2016        Pahwa Nancy   TMS# 20160225-00123
                                             Initial creation of the procedure
  1.1     03-Mar-2016        Pahwa Nancy   TMS# 20160307-00126  Added N6 index
  ********************************************************************************/
  l_sec           VARCHAR2(100);
begin
  l_sec := 'Droping APPS.XXWC_MD_SEARCH_PRODUCTS_MV';
  EXECUTE IMMEDIATE '
drop MATERIALIZED VIEW APPS.XXWC_MD_SEARCH_PRODUCTS_MV';
  l_sec := 'Create APPS.XXWC_MD_SEARCH_PRODUCTS_MV';
  EXECUTE IMMEDIATE '   CREATE MATERIALIZED VIEW APPS.XXWC_MD_SEARCH_PRODUCTS_MV REFRESH COMPLETE ON DEMAND
      
      AS
        select inventory_item_id,
               organization_id,
               partnumber,
               type,
               manufacturerpartnumber,
               manufacturer,
               sequence,
               currencycode,
               name,
               shortdescription,
               longdescription,
               thumbnail,
               fullimage,
               quantitymeasure,
               weightmeasure,
               weight,
               buyable,
               keyword,
               creation_date,
               item_type,
               cross_reference,
               dummy,
               primary_uom_code
          from APPS.XXWC_MD_SEARCH_PRODUCTS_VW';
  l_sec := 'Create index APPS.XXWC_MD_SEARCH_MV_N1';
  EXECUTE IMMEDIATE 'create index APPS.XXWC_MD_SEARCH_MV_N1 on XXWC_MD_SEARCH_PRODUCTS_MV(partnumber) indextype is ctxsys.context parameters(''
                          wordlist XXWC_MD_PRODUCT_SEARCH_PREF '')';
  l_sec := 'Create index APPS.XXWC_MD_SEARCH_MV_N2';
  EXECUTE IMMEDIATE 'create index APPS.XXWC_MD_SEARCH_MV_N2 on XXWC_MD_SEARCH_PRODUCTS_MV(shortdescription) indextype is ctxsys.context parameters(''
                          wordlist XXWC_MD_PRODUCT_SEARCH_PREF '')';
  l_sec := 'Create index APPS.XXWC_MD_SEARCH_MV_N3';
  EXECUTE IMMEDIATE 'create index APPS.XXWC_MD_SEARCH_MV_N3 on XXWC_MD_SEARCH_PRODUCTS_MV(cross_reference) indextype is ctxsys.context parameters(''
                          wordlist XXWC_MD_PRODUCT_SEARCH_PREF '')';
   l_sec := 'Create index APPS.XXWC_MD_SEARCH_MV_N6';
  EXECUTE IMMEDIATE 'create index APPS.XXWC_MD_SEARCH_MV_N6 on XXWC_MD_SEARCH_PRODUCTS_MV (inventory_item_id)'; --1.1
  /*
  create index APPS.XXWC_MD_SEARCH_MV_N4 on XXWC_MD_SEARCH_PRODUCTS_MV (name)
  indextype is ctxsys.context parameters ('wordlist XXWC_MD_PRODUCT_SEARCH_PREF');*/
  l_sec := 'Create index APPS.XXWC_MD_SEARCH_MV_N5';
  EXECUTE IMMEDIATE 'create index APPS.XXWC_MD_SEARCH_MV_N5 on XXWC_MD_SEARCH_PRODUCTS_MV(dummy) indextype is ctxsys.context parameters(''
                          DATASTORE XXWC_MD_PRODUCT_STORE_PREF lexer
                          XXWC_MD_PRODUCT_STORE_LEX1 section group
                          XXWC_MD_PRODUCT_STORE_SG '')';
EXCEPTION
  WHEN OTHERS THEN
    p_err_buf     := SQLERRM || l_sec;
    p_return_code := 2;
END XXWC_MD_SEARCH_REFRESH_PRC;
/
