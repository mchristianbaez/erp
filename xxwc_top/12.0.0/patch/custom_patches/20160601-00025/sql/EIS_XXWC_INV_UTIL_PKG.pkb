CREATE OR REPLACE 
PACKAGE BODY  XXEIS.EIS_XXWC_INV_UTIL_PKG
AS
  --//============================================================================
  --// Object Name          :: EIS_XXWC_INV_UTIL_PKG
  --//
  --// Object Type          :: Package Body
  --//
  --// Object Description   :: This Package will trigger in view and populate the values into View.
  --//
  --// Version Control
  --//============================================================================
  --// Version    Author           Date          Description
  --//----------------------------------------------------------------------------
  --// 1.0        Siva        06/06/2016         Initial Build  -- Added for TMS#20160601-00025.
  --//============================================================================
FUNCTION GET_ONHAND_QTY(
    P_INVENTORY_ITEM_ID NUMBER,
    P_ORGANIZATION_ID   NUMBER,
    P_SUBINVENTORY_CODE VARCHAR2)
  RETURN number
IS
  --//============================================================================
  --// Object Usage         :: This Object Referred by "Inventory - Onhand Quantity - WC"
  --//
  --// Object Name          :: GET_ONHAND_QTY
  --//
  --// Object Type          :: Function
  --//
  --// Object Description   :: This Function will populate the values in the view.
  --//
  --// Version Control
  --//============================================================================
  --// Version    Author           Date          Description
  --//----------------------------------------------------------------------------
  --// 1.0        Siva        06/06/2016      Initial Build  -- Added for TMS#20160601-00025.
  --//============================================================================
L_HASH_INDEX varchar2(100);
L_SQL varchar2(32000);
begin
g_onhand_qty := NULL;

l_sql :='SELECT SUM (NVL(moq1.Primary_Transaction_Quantity,0))
    FROM Mtl_Onhand_Quantities_Detail Moq1
    WHERE Moq1.Inventory_Item_Id = :1
    AND Moq1.Organization_Id     = :2
    AND Moq1.subinventory_code   = :3';
    BEGIN
        l_hash_index:=P_INVENTORY_ITEM_ID||'-'||P_ORGANIZATION_ID||'-'||P_SUBINVENTORY_CODE;
        g_onhand_qty := G_ONHAND_QTY_VLDN_TBL(L_HASH_INDEX);
 exception
    when no_data_found
    THEN
    begin      
    EXECUTE IMMEDIATE L_SQL INTO g_onhand_qty USING P_INVENTORY_ITEM_ID,P_ORGANIZATION_ID,P_SUBINVENTORY_CODE;
    EXCEPTION WHEN NO_DATA_FOUND THEN
    g_onhand_qty :=0;
    WHEN OTHERS THEN
    g_onhand_qty :=0;
    END;      
                      l_hash_index:=P_INVENTORY_ITEM_ID||'-'||P_ORGANIZATION_ID||'-'||P_SUBINVENTORY_CODE;
                       G_ONHAND_QTY_VLDN_TBL(L_HASH_INDEX) := g_onhand_qty;
    END;
     return  g_onhand_qty;
     EXCEPTION when OTHERS then
      g_onhand_qty:=0;
      RETURN  g_onhand_qty;

end GET_ONHAND_QTY;
END EIS_XXWC_INV_UTIL_PKG;
/
