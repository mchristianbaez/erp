CREATE OR REPLACE PACKAGE BODY APPS.XXWC_RMA_CREATION_PKG
AS
   /*************************************************************************
      $Header XXWC_RMA_CREATION_PKG.PKG $
      Module Name: XXWC_RMA_CREATION_PKG.PKG

      PURPOSE:   This package is used for return order creation

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------
      1.0        04/12/2016  Niraj K Ranjan          Initial Version for TMS#20160323-00165
      
   ****************************************************************************/
   
   /*************************************************************************
      PROCEDURE Name: create_return_order

      PURPOSE:   To create return order

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------
      1.0        04/12/2016  Niraj K Ranjan          Initial Version for TMS#20160323-00165
   ****************************************************************************/
   PROCEDURE create_return_order (p_order_header_id       IN     NUMBER,
                                  p_order_line_rec        IN     line_tab,
                                  p_credit_only           IN     VARCHAR2,
                                  p_credit_rebill         IN     VARCHAR2,
                                  p_org_id                IN     NUMBER,
                                  P_user_id               IN     NUMBER,
                                  P_resp_id               IN     NUMBER,
                                  P_resp_appl_id          IN     NUMBER,
                                  p_new_sales_order       OUT    NUMBER,
                                  p_new_order_hdr_id      OUT    NUMBER,
                                  p_status                OUT    VARCHAR2,
                                  p_retmsg                OUT    api_err_msg_tab
                                 )
   IS
      l_return_status                VARCHAR2 (2000);
      l_msg_count                    NUMBER;
      l_msg_data                     VARCHAR2 (2000);
      l_msg_index_out                NUMBER(10); 
      -- PARAMETERS
      l_debug_level                  NUMBER  := 0; --5 -- OM DEBUG LEVEL (MAX 5)
      l_org                          VARCHAR2 (20)                          := p_org_id; -- OPERATING UNIT
      l_no_orders                    NUMBER                                   := 1; -- NO OF ORDERS
      -- INPUT VARIABLES FOR PROCESS_ORDER API
      l_header_rec                   oe_order_pub.header_rec_type;
      l_line_tbl                     oe_order_pub.line_tbl_type;
      l_action_request_tbl           oe_order_pub.request_tbl_type;
      -- OUT VARIABLES FOR PROCESS_ORDER API
      l_header_rec_out               oe_order_pub.header_rec_type;
      l_header_val_rec_out           oe_order_pub.header_val_rec_type;
      l_header_adj_tbl_out           oe_order_pub.header_adj_tbl_type;
      l_header_adj_val_tbl_out       oe_order_pub.header_adj_val_tbl_type;
      l_header_price_att_tbl_out     oe_order_pub.header_price_att_tbl_type;
      l_header_adj_att_tbl_out       oe_order_pub.header_adj_att_tbl_type;
      l_header_adj_assoc_tbl_out     oe_order_pub.header_adj_assoc_tbl_type;
      l_header_scredit_tbl_out       oe_order_pub.header_scredit_tbl_type;
      l_header_scredit_val_tbl_out   oe_order_pub.header_scredit_val_tbl_type;
      l_line_tbl_out                 oe_order_pub.line_tbl_type;
      l_line_val_tbl_out             oe_order_pub.line_val_tbl_type;
      l_line_adj_tbl_out             oe_order_pub.line_adj_tbl_type;
      l_line_adj_val_tbl_out         oe_order_pub.line_adj_val_tbl_type;
      l_line_price_att_tbl_out       oe_order_pub.line_price_att_tbl_type;
      l_line_adj_att_tbl_out         oe_order_pub.line_adj_att_tbl_type;
      l_line_adj_assoc_tbl_out       oe_order_pub.line_adj_assoc_tbl_type;
      l_line_scredit_tbl_out         oe_order_pub.line_scredit_tbl_type;
      l_line_scredit_val_tbl_out     oe_order_pub.line_scredit_val_tbl_type;
      l_lot_serial_tbl_out           oe_order_pub.lot_serial_tbl_type;
      l_lot_serial_val_tbl_out       oe_order_pub.lot_serial_val_tbl_type;
      l_action_request_tbl_out       oe_order_pub.request_tbl_type;
      l_msg_index                    NUMBER;
      l_data                         VARCHAR2 (2000);
      l_loop_count                   NUMBER;
      l_debug_file                   VARCHAR2 (200);
      l_user_id                      NUMBER   := p_user_id;
      l_resp_id                      NUMBER   := p_resp_id;
      l_resp_appl_id                 NUMBER   := P_resp_appl_id;
      l_line_id                      NUMBER;
      l_quantity                     NUMBER;
      l_return_reason_code           VARCHAR2(30);
      l_credit_only_trans_id         NUMBER;
      l_bill_only_trans_id           NUMBER;
      l_return_rcpt_trans_id         NUMBER;
      l_line_number                  NUMBER;
	  l_ship_from_org_id             NUMBER;
	  l_check_cust_credit_hold       NUMBER;
	  l_check_cust_site_credit_hold  NUMBER;
	  e_validation_excep             EXCEPTION;
	  l_returnable_flag              mtl_system_items_b.returnable_flag%type;
	  l_item_segment1                mtl_system_items_b.segment1%type;
      l_api_err_msg_tab              api_err_msg_tab;
      
      CURSOR cr_header
      IS
         SELECT cust_po_number,
                sold_to_org_id,
                price_list_id,
                sold_from_org_id,
                ship_from_org_id,
                ship_to_org_id,
                ship_to_contact_id,
                invoice_to_org_id,
                invoice_to_contact_id,
                salesrep_id,
                --'BOOKED' flow_status_code,
				'ENTERED' flow_status_code,
                header_id,
                (SELECT transaction_type_id 
                 FROM OE_TRANSACTION_TYPES_TL 
                 WHERE name = 'RETURN ORDER' 
                 AND language = userenv('LANG')) order_type_id
         FROM OE_ORDER_HEADERS_ALL 
         WHERE header_id = p_order_header_id;

      CURSOR cr_lines(p_line_id NUMBER)
      IS
         SELECT org_id,
                ship_from_org_id,
                ordered_item,
                --ordered_quantity,
                orig_sys_document_ref,
                order_quantity_uom,
                inventory_item_id,
                price_list_id,
                'ORDER' xml_transaction_type_code,
                orig_sys_line_ref,
                line_number ,
                line_id,
                --'RETURN' return_reason_code,
                'ORDER'  return_context,
                --'BOOKED' flow_status_code,
				'ENTERED' flow_status_code,
                NULL     subinventory,
                NULL     line_type_id
         FROM oe_order_lines_all 
         WHERE header_id = p_order_header_id
         AND   line_id = p_line_id;

      CURSOR cr_trans_type(p_name oe_transaction_types_tl.name%type)
      IS
         SELECT transaction_type_id 
         FROM oe_transaction_types_tl 
         WHERE name = p_name --decode(p_credit_only,'N',decode(p_credit_rebill,'N','RETURN WITH RECEIPT','Y',),'Y','CREDIT ONLY') 
         AND language = userenv('LANG'); 
   BEGIN
      -- INITIALIZATION REQUIRED FOR R12
      mo_global.set_policy_context ('S', l_org);
      mo_global.init ('ONT');
      
      -- INITIALIZE DEBUG INFO
      IF (l_debug_level > 0)
      THEN
         l_debug_file := oe_debug_pub.set_debug_mode ('FILE');
         oe_debug_pub.initialize;
         oe_msg_pub.initialize;
         oe_debug_pub.setdebuglevel (l_debug_level);
      END IF;
      
      --  INITIALIZE ENVIRONMENT
      fnd_global.apps_initialize (l_user_id, l_resp_id, l_resp_appl_id);
      
      -- INITIALIZE HEADER RECORD
      l_header_rec := oe_order_pub.g_miss_header_rec;
	  --As per issue raised by Lenora - branch should be defaulting to the shipping branch within 
	  --the Profile Option since that is where the material will be returned to.
      l_ship_from_org_id := FND_PROFILE.VALUE_SPECIFIC('XXWC_OM_DEFAULT_SHIPPING_ORG',l_user_id);
	  
	  --Check customer on credit hold
	  l_check_cust_credit_hold := 0;
	  select count(1) into l_check_cust_credit_hold
      from  hz_customer_profiles hcp,
            oe_order_headers_all ooh
      where header_id = p_order_header_id
      and   ooh.sold_to_org_id = hcp.cust_account_id
      and   site_use_id is null
      and   credit_hold = 'Y';

	  IF l_check_cust_credit_hold > 0 THEN
         l_msg_data := 'Customer is on Credit Hold. Order cannot be created. Contact Credit Associate.';
	     RAISE e_validation_excep;
	  END IF;
	  
	  --Check customer site on credit hold
	  l_check_cust_site_credit_hold := 0;
	  select count(1) into l_check_cust_site_credit_hold
      from hz_customer_profiles hcp,
           oe_order_headers_all ooh
      where header_id = p_order_header_id
      and   ooh.sold_to_org_id = hcp.cust_account_id
      and   hcp.site_use_id = ooh.invoice_to_org_id
      and   credit_hold = 'Y';

	  IF l_check_cust_site_credit_hold > 0 THEN
         l_msg_data := 'Customer site is on Credit Hold. Order cannot be created. Contact Credit Associate.';
	     RAISE e_validation_excep;
	  END IF;
      -- POPULATE REQUIRED ATTRIBUTES OF HEADER
      FOR rec_header IN cr_header
      LOOP
         l_header_rec.operation                := oe_globals.g_opr_create;
         l_header_rec.pricing_date             := SYSDATE;
         l_header_rec.cust_po_number           := rec_header.cust_po_number;
         l_header_rec.sold_to_org_id           := rec_header.sold_to_org_id;
         l_header_rec.price_list_id            := rec_header.price_list_id;
         l_header_rec.ordered_date             := SYSDATE;
         l_header_rec.sold_from_org_id         := rec_header.sold_from_org_id;
         l_header_rec.ship_from_org_id         := l_ship_from_org_id;--rec_header.ship_from_org_id;
         l_header_rec.ship_to_org_id           := rec_header.ship_to_org_id;
         l_header_rec.ship_to_contact_id       := rec_header.ship_to_contact_id;
         l_header_rec.invoice_to_org_id        := rec_header.invoice_to_org_id;
         l_header_rec.invoice_to_contact_id    := rec_header.invoice_to_contact_id;
         l_header_rec.salesrep_id              := rec_header.salesrep_id;
         l_header_rec.flow_status_code         := rec_header.flow_status_code;
         l_header_rec.order_type_id            := rec_header.order_type_id;
      END LOOP;
      
      -- INITIALIZE LINE RECORD 
      IF p_order_line_rec.COUNT > 0 THEN
         --fetch transaction_type_id
         IF p_credit_only = 'Y' THEN
            OPEN cr_trans_type('CREDIT ONLY');
            FETCH cr_trans_type INTO l_credit_only_trans_id;
            CLOSE cr_trans_type;
         ELSIF p_credit_rebill = 'Y' THEN
            OPEN cr_trans_type('CREDIT ONLY');
            FETCH cr_trans_type INTO l_credit_only_trans_id;
            CLOSE cr_trans_type;
            OPEN cr_trans_type('BILL ONLY');
            FETCH cr_trans_type INTO l_bill_only_trans_id;
            CLOSE cr_trans_type;
         ELSE
            OPEN cr_trans_type('RETURN WITH RECEIPT');
            FETCH cr_trans_type INTO l_return_rcpt_trans_id;
            CLOSE cr_trans_type;
         END IF;

         --Loop start for line insert
         l_line_number := 0;
         FOR idx IN 1..p_order_line_rec.COUNT
         LOOP
            l_line_number         :=  l_line_number + 1;
            l_line_id             :=  p_order_line_rec(idx).line_id;       
            l_quantity            :=  p_order_line_rec(idx).quantity; 
            l_return_reason_code  :=  p_order_line_rec(idx).return_reason_code;
			l_returnable_flag     :=  'Y';
			l_item_segment1       :=  '';
			
			BEGIN
			   select msi.returnable_flag,msi.segment1 
			    into l_returnable_flag,l_item_segment1
                from mtl_system_items_b msi,
                     oe_order_lines_all ool
                where ool.line_id = l_line_id
                and   ool.inventory_item_id = msi.inventory_item_id
                and   ool.ship_from_org_id = msi.organization_id;
                --and   msi.organization_id = 222
                --and   msi.returnable_flag = 'N';
			EXCEPTION
			   when others then
			      l_returnable_flag := 'Y';
				  l_item_segment1   := '';
			END;
			
			IF l_returnable_flag = 'N' THEN
			   EXIT;
			END IF;
			
            FOR rec_lines IN cr_lines(l_line_id)
            LOOP
                l_line_tbl(l_line_number)                           := oe_order_pub.g_miss_line_rec; 
                l_line_tbl(l_line_number).org_id                    := rec_lines.org_id;
                l_line_tbl(l_line_number).ship_from_org_id          := l_ship_from_org_id;--rec_lines.ship_from_org_id; 
                l_line_tbl(l_line_number).ordered_item              := rec_lines.ordered_item;
                l_line_tbl(l_line_number).ordered_quantity          := l_quantity; 
                l_line_tbl(l_line_number).orig_sys_document_ref     := rec_lines.orig_sys_document_ref;
                l_line_tbl(l_line_number).order_quantity_uom        := rec_lines.order_quantity_uom; 
                l_line_tbl(l_line_number).inventory_item_id         := rec_lines.inventory_item_id;
                l_line_tbl(l_line_number).price_list_id             := rec_lines.price_list_id;
                l_line_tbl(l_line_number).xml_transaction_type_code := rec_lines.xml_transaction_type_code; 
                l_line_tbl(l_line_number).orig_sys_line_ref         := rec_lines.orig_sys_line_ref;
                --l_line_tbl(idx).line_number                       := rec_lines.line_number; 
                l_line_tbl(l_line_number).line_number               := l_line_number;
                l_line_tbl(l_line_number).return_reason_code        := l_return_reason_code;
                l_line_tbl(l_line_number).return_attribute1         := p_order_header_id;
                l_line_tbl(l_line_number).return_attribute2         := l_line_id;
                l_line_tbl(l_line_number).reference_header_id       := p_order_header_id;
                l_line_tbl(l_line_number).reference_line_id         := l_line_id;
                l_line_tbl(l_line_number).return_context            := rec_lines.return_context;
                l_line_tbl(l_line_number).flow_status_code          := rec_lines.flow_status_code;
                l_line_tbl(l_line_number).subinventory              := rec_lines.subinventory;
                --l_line_tbl(idx).line_type_id                      := rec_lines.line_type_id;	
                IF p_credit_only = 'Y' OR p_credit_rebill = 'Y' THEN
                   l_line_tbl(l_line_number).line_type_id            := l_credit_only_trans_id;
                ELSE
                   l_line_tbl(l_line_number).line_type_id            := l_return_rcpt_trans_id;
                END IF;
                l_line_tbl(l_line_number).operation                  := oe_globals.g_opr_create; 
                
                -- INITIALIZE ACTION REQUEST RECORD
                l_action_request_tbl(l_line_number)                 := oe_order_pub.g_miss_request_rec;
                --l_action_request_tbl(l_line_number).request_type    := oe_globals.g_book_order; 07dec
                l_action_request_tbl(l_line_number).entity_code     := oe_globals.g_entity_header;
                
                --Insert one more line if credit rebill option is selected by user
                IF p_credit_rebill = 'Y' THEN
                   l_line_number                                       := l_line_number + 1;	
                   l_line_tbl(l_line_number)                           := oe_order_pub.g_miss_line_rec; 
                   l_line_tbl(l_line_number).org_id                    := rec_lines.org_id;
                   l_line_tbl(l_line_number).ship_from_org_id          := l_ship_from_org_id;--rec_lines.ship_from_org_id; 
                   l_line_tbl(l_line_number).ordered_item              := rec_lines.ordered_item;
                   l_line_tbl(l_line_number).ordered_quantity          := l_quantity; 
                   l_line_tbl(l_line_number).orig_sys_document_ref     := rec_lines.orig_sys_document_ref;
                   l_line_tbl(l_line_number).order_quantity_uom        := rec_lines.order_quantity_uom; 
                   l_line_tbl(l_line_number).inventory_item_id         := rec_lines.inventory_item_id;
                   l_line_tbl(l_line_number).price_list_id             := rec_lines.price_list_id;
                   l_line_tbl(l_line_number).xml_transaction_type_code := rec_lines.xml_transaction_type_code; 
                   l_line_tbl(l_line_number).orig_sys_line_ref         := rec_lines.orig_sys_line_ref;
                   --l_line_tbl(idx).line_number                       := rec_lines.line_number; 
                   l_line_tbl(l_line_number).line_number               := l_line_number;
                   l_line_tbl(l_line_number).return_reason_code        := l_return_reason_code;
                   --l_line_tbl(idx).return_attribute1                 := p_order_header_id;
                   --l_line_tbl(idx).return_attribute2                 := l_line_id;
                   --l_line_tbl(idx).reference_header_id               := p_order_header_id;
                   --l_line_tbl(idx).reference_line_id                 := l_line_id;
                   --l_line_tbl(l_line_number).return_context            := rec_lines.return_context;
                   l_line_tbl(l_line_number).flow_status_code          := rec_lines.flow_status_code;
                   l_line_tbl(l_line_number).subinventory              := rec_lines.subinventory;
                   --l_line_tbl(idx).line_type_id                      := rec_lines.line_type_id;	
                   l_line_tbl(l_line_number).line_type_id              := l_bill_only_trans_id;
                   l_line_tbl(l_line_number).operation                 := oe_globals.g_opr_create; 
                   
                   -- INITIALIZE ACTION REQUEST RECORD
                   l_action_request_tbl(l_line_number)                 := oe_order_pub.g_miss_request_rec;
                   --l_action_request_tbl(l_line_number).request_type    := oe_globals.g_book_order; --07dec
                   l_action_request_tbl(l_line_number).entity_code     := oe_globals.g_entity_header;
               END IF;
            END LOOP;
         END LOOP; 
      END IF;
	  IF l_returnable_flag <> 'N' THEN
         -- CALLTO PROCESS ORDER API
         oe_order_pub.process_order (
                                     p_org_id=> l_org,
                                     --     p_operating_unit           => NULL,
                                     p_api_version_number=> 1.0,
                                     p_header_rec=> l_header_rec,
                                     p_line_tbl=> l_line_tbl,
                                     p_action_request_tbl=> l_action_request_tbl,
                                     -- OUT variables
                                     x_header_rec=> l_header_rec_out,
                                     x_header_val_rec=> l_header_val_rec_out,
                                     x_header_adj_tbl=> l_header_adj_tbl_out,
                                     x_header_adj_val_tbl=> l_header_adj_val_tbl_out,
                                     x_header_price_att_tbl=> l_header_price_att_tbl_out,
                                     x_header_adj_att_tbl=> l_header_adj_att_tbl_out,
                                     x_header_adj_assoc_tbl=> l_header_adj_assoc_tbl_out,
                                     x_header_scredit_tbl=> l_header_scredit_tbl_out,
                                     x_header_scredit_val_tbl=> l_header_scredit_val_tbl_out,
                                     x_line_tbl=> l_line_tbl_out,
                                     x_line_val_tbl=> l_line_val_tbl_out,
                                     x_line_adj_tbl=> l_line_adj_tbl_out,
                                     x_line_adj_val_tbl=> l_line_adj_val_tbl_out,
                                     x_line_price_att_tbl=> l_line_price_att_tbl_out,
                                     x_line_adj_att_tbl=> l_line_adj_att_tbl_out,
                                     x_line_adj_assoc_tbl=> l_line_adj_assoc_tbl_out,
                                     x_line_scredit_tbl=> l_line_scredit_tbl_out,
                                     x_line_scredit_val_tbl=> l_line_scredit_val_tbl_out,
                                     x_lot_serial_tbl=> l_lot_serial_tbl_out,
                                     x_lot_serial_val_tbl=> l_lot_serial_val_tbl_out,
                                     x_action_request_tbl=> l_action_request_tbl_out,
                                     x_return_status=> l_return_status,
                                     x_msg_count=> l_msg_count,
                                     x_msg_data=> l_msg_data
                                    );
      ELSE
	     l_return_status :=  fnd_api.g_ret_sts_error;
      END IF;
      IF l_return_status = fnd_api.g_ret_sts_success THEN 
         p_new_sales_order  := l_header_rec_out.order_number;
         p_new_order_hdr_id := l_header_rec_out.header_id;
         COMMIT; 
      ELSE 
         l_api_err_msg_tab.DELETE;
		 IF l_returnable_flag <> 'N' THEN
            FOR i IN 1..l_msg_count
            LOOP
               oe_msg_pub.get (
                               p_msg_index     => i,
                               p_encoded       => fnd_api.g_false,
                               p_data          => l_data,
                               p_msg_index_out => l_msg_index
                              );
               l_api_err_msg_tab(i).msg_index  := l_msg_index;
               l_api_err_msg_tab(i).errmsg     := l_data;
            END LOOP;
         ELSE
		    l_msg_index := 1;
			l_data := 'Can not create return order because item - '||l_item_segment1||' is not returnable';
		    l_api_err_msg_tab(1).msg_index  := l_msg_index;
            l_api_err_msg_tab(1).errmsg     := l_data;
		 END IF;
         ROLLBACK; 
      END IF; 
  
      p_status    := l_return_status;
      p_retmsg    := l_api_err_msg_tab;
  
   EXCEPTION
      WHEN e_validation_excep THEN
	     p_status  := fnd_api.g_ret_sts_error;
         l_api_err_msg_tab.DELETE;
         l_api_err_msg_tab(1).msg_index  := 1;
         l_api_err_msg_tab(1).errmsg     := l_msg_data;
         p_retmsg := l_api_err_msg_tab;
         ROLLBACK;
      WHEN OTHERS THEN
         p_status  := 'UE';
         l_api_err_msg_tab.DELETE;
         l_api_err_msg_tab(1).msg_index  := 1;
         l_api_err_msg_tab(1).errmsg     := SQLERRM;
         p_retmsg     := l_api_err_msg_tab;
         ROLLBACK;
   END create_return_order;
   
END XXWC_RMA_CREATION_PKG;
/
