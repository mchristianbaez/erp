/*************************************************************************
  $Header XXWC_QP_MODIFIERS_EXT_TBL_ALTER.sql $
  Module Name: XXWC_INV_ONHAND_QUANTITY_MV_ALTER.sql

  PURPOSE:   To remove Parallelism 

  REVISIONS:
  Ver        Date        Author                     Description
  ---------  ----------  ---------------         -------------------------
  1.0        04/20/2016  Rakesh Patel            TMS-20160407-00195 - Remove Parallelism - XXWC_QP_MODIFIERS_EXT_TBL
**************************************************************************/
ALTER TABLE XXWC_QP_MODIFIERS_EXT_TBL NOPARALLEL;