CREATE OR REPLACE PACKAGE APPS.xxcus_concur_pkg is
  --
 /*
   Ticket#                                                                    Date                                 Author                                                      Notes
   -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   TMS  20170407-00204 / ESMS 554216      02/17/2017                     Balaguru Seshadri                               Concur process related routines
   TMS  20170821-00206 / RITM0011184     09/28/2017                     Balaguru Seshadri                               Concur actuals enhancements
   TMS 20180124-00282                    01/24/2018                     Balaguru Seshadri                               Add GL import alert routine
 */
  --
    g_limit number :=2500;
    g_rpt_crt_btch_id number :=0;
    g_rpt_concur_batch_id number :=0;
    g_rpt_run_id number :=0;
    g_srs_run_id number :=0; -- TMS  20170821-00206 / RITM0011184
  --
    procedure process_new_batch --need here declared because we need this from the concurrent program
    (
        retcode out varchar2
       ,errbuf out varchar2
       ,p_non_prod_email in varchar2
       ,p_push_to_sharepoint in varchar2
       ,p_set_job_id in  number -- TMS  20170821-00206 / RITM0011184
       ,p_main_req_id in number -- TMS  20170821-00206 / RITM0011184
       ,p_run_id in number -- TMS  20170821-00206 / RITM0011184
    )
    ;
    --
    -- Begin TMS  20170821-00206 / RITM0011184
  PROCEDURE GL_WRAPPER (
    retcode               out number
   ,errbuf                out varchar2
   ,p_run_id           in number
  );
    -- Begin TMS  20180124-00282
  PROCEDURE GL_ALERT (
    retcode               out number
   ,errbuf                out varchar2
   ,p_run_id           in number
   ,p_non_prod_email in varchar2
  );  
    -- End TMS  20180124-00282  
  --
  procedure INS_BI_AUDIT_INTERCO_RPT
   (
     p_report_type in varchar2
    ,p_extract_type in varchar2
    ,p_seq in number
    ,p_item_name in varchar2
    ,p_item_amount in number
    ,p_line_comments in varchar2
   );
  -- End TMS  20170821-00206 / RITM0011184
    --
    procedure set_sae_hdr_status (p_status in varchar2); --need here declared because we need this from the BI publisher report
    --
    function beforeReport return boolean;
    --
    function afterreport return boolean;
    --
    procedure DELETE_REQUEST_SET_LOG;
    --
    procedure INSERT_REQUEST_SET_LOG (p_set_req_id IN NUMBER, p_wrapper_req_id IN NUMBER);
    --
      procedure submit_request_set
    (
        retcode out varchar2
       ,errbuf  out varchar2
       ,p_non_prod_email in varchar2
       ,p_push_to_sharepoint in varchar2
    );
    --
end xxcus_concur_pkg;
/