/*
 TMS:  20170724-00106
 Date: 07/25/2017
 Notes:  Several fixes
         1) Set status to Audit reprint report generated for batches 14 and 15.
		 2) Delete duplicate SAE header for concur_batch_id 14 and crt_btch_id 1784095. This is required in order to create am unique index.
		 3) Remove delete access for SAE header for user INTERFACE_DSTAGE
		 4) Create a new index on SAE header on the column concur_batch_id
*/
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
 --
 n_loc number :=0;
 l_sql varchar2(2000) :=Null;
 --
BEGIN --Main Processing...
   --
   -- Update status so the new audit report will not pick these old records during the next run.
   --
   begin
    --
    n_loc :=101.1;
    --
    update xxcus.xxcus_concur_tie_out_report
	set status ='Audit report generated manually using reprint.'
	where 1 =1
	  and concur_batch_id IN (14, 15)
	  and status ='NEW'
	  ;
    -- 
	dbms_output.put_line('@ n_loc ='||n_loc||', updated xxcus_concur_tie_out_report status for batches 14 and 15, total rows :'||sql%rowcount);
    --	
    n_loc :=101.2;
    --	
    commit;
    --	
    n_loc :=101.3;
    --	
   exception
    when others then
     --
     n_loc :=101.4;
     --
     dbms_output.put_line('@ n_loc ='||n_loc||', message ='||sqlerrm);
     --  
   end;
   --
   -- Delete SAE header concur batch id 14 and crt_btch_id 1784095
   -- 
   begin
    --
    n_loc :=102.1;
    --
    delete xxcus.xxcus_concur_sae_header
	where 1 =1
	  and concur_batch_id =14
	  and crt_btch_id =1784095
	  ;
    -- 
	dbms_output.put_line('@ n_loc ='||n_loc||', deleted SAE header concur_batch_id 14 and crt_btch_id 1784095, total rows :'||sql%rowcount);
	--
    n_loc :=102.2;
    --	
    commit;
    --	
    n_loc :=102.3;
    --	
   exception
    when others then
     --
     n_loc :=102.4;
     --
     dbms_output.put_line('@ n_loc ='||n_loc||', message ='||sqlerrm);
     --  
   end;
   --
   -- Revoke delete access to SAE header table for user INTERFACE_DSTAGE
   -- 
   begin
    --
    n_loc :=103.1;
    --
    l_sql :='REVOKE DELETE ON xxcus.xxcus_concur_sae_header FROM INTERFACE_DSTAGE';
    -- 
    EXECUTE IMMEDIATE l_sql;
    --
	dbms_output.put_line('@ n_loc ='||n_loc||', revoked delete access for SAE header table from INTERFACE_DSTAGE.');
	--
    n_loc :=103.2;
    --	
   exception
    when others then
     --
     n_loc :=103.3;
     --
     dbms_output.put_line('@ n_loc ='||n_loc||', message ='||sqlerrm);
     --  
   end;
   -- 
   -- Create unique index on SAE header table column concur_batch_id
   --    
   begin
    --
    n_loc :=104.1;
    -- 	
    l_sql :='CREATE UNIQUE INDEX XXCUS.XXCUS_CONCUR_SAE_HEADER_U1 ON XXCUS.XXCUS_CONCUR_SAE_HEADER (CONCUR_BATCH_ID)';
    --
    n_loc :=104.2;
    --    
    EXECUTE IMMEDIATE l_sql;
    --
    n_loc :=104.3;
    --        
    dbms_output.put_line('Unique index created on SAE header table.');
    --
    n_loc :=104.4;
    --       
   exception
    when others then
     --
     n_loc :=104.5;
     --
     dbms_output.put_line('@ n_loc ='||n_loc||', message ='||sqlerrm);
     --  
   end;
   --
   commit;
   --
EXCEPTION
   WHEN OTHERS THEN
      DBMS_OUTPUT.put_line ('Calling block, TMS: 20170724-00106, Errors =' || SQLERRM);
END;
/