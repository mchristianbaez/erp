/*
   Ticket#                               Date         Author             Notes
   -------------------------------------------------------------------------------------------------------------------------------------------------------
   TMS: 20170523-00220     05/08/2017   Balaguru Seshadri  Put 2016 rebate agreements to on hold.
*/
declare
    cursor offers is
    select a.offer_id, a.org_id, a.offer_code, a.qp_list_header_id, a.object_version_number obj_ver_num
    from ozf_offers a
             ,qp_list_headers_b c
    where 1 =1
    and a.org_id in (101, 102)
    and a.status_code ='ACTIVE'
    and a.user_status_id =1604
    and c.list_header_id =a.qp_list_header_id
    and c.attribute7 ='2016'
	and c.attribute5 ='DEDUCTION'
    ;

    x_offer_adjustment_id   NUMBER;
    x_offer_adjst_tier_id   NUMBER;
    x_offer_adj_line_id     NUMBER;
    x_object_version_number NUMBER;
  
    x_qp_list_header_id NUMBER;
    x_err_location      NUMBER;
  
    l_offer_adj_rec        ozf_offer_adjustment_pvt.offer_adj_rec_type;
    l_offadj_tier_rec_type ozf_offer_adj_tier_pvt.offadj_tier_rec_type;
  
    l_offadj_line_rec_type ozf_offer_adj_line_pvt.offadj_line_rec_type;
  
    l_modifier_list_rec ozf_offer_pub.modifier_list_rec_type;
    l_modifier_line_tbl ozf_offer_pub.modifier_line_tbl_type;
    l_na_qualifier_tbl  ozf_offer_pub.na_qualifier_tbl_type;
    l_prod_rec_tbl      ozf_offer_pub.prod_rec_tbl_type;
    l_offer_tier_tbl    ozf_offer_pub.offer_tier_tbl_type;
    l_excl_rec_tbl      ozf_offer_pub.excl_rec_tbl_type;
    l_discount_line_tbl ozf_offer_pub.discount_line_tbl_type;
    l_act_product_tbl   ozf_offer_pub.act_product_tbl_type;
    l_vo_pbh_tbl        ozf_offer_pub.vo_disc_tbl_type;
    l_vo_dis_tbl        ozf_offer_pub.vo_disc_tbl_type;
    l_vo_prod_tbl       ozf_offer_pub.vo_prod_tbl_type;
    l_qualifier_tbl     ozf_offer_pub.qualifiers_tbl_type;
    l_vo_mo_tbl         ozf_offer_pub.vo_mo_tbl_type;
    l_budget_tbl        ozf_offer_pub.budget_tbl_type;
  
    l_object_version_number NUMBER;
    l_obj_version           NUMBER;
    l_start_date            DATE;
    l_offer_id              NUMBER;
    l_description           qp_list_headers_vl.description%TYPE;
    l_offer_type            VARCHAR2(50);
    l_user_status_id        NUMBER;
  
    x_errbuf  VARCHAR2(2000);
    x_retcode VARCHAR2(2000);
  
    x_return_status VARCHAR2(1);
    x_msg_count     NUMBER;
    x_msg_data      VARCHAR2(4000);
    
begin
 
 for rec in offers loop
  mo_global.set_policy_context('S', rec.org_id);
  savepoint here_we_start;
  
      l_modifier_list_rec.qp_list_header_id     :=rec.qp_list_header_id;
      l_modifier_list_rec.object_version_number :=rec.obj_ver_num;     
      l_modifier_list_rec.status_code           :='ONHOLD';
      l_modifier_list_rec.user_status_id        :=1607;
      l_modifier_list_rec.modifier_operation    :='UPDATE';
      l_modifier_list_rec.offer_operation       :='UPDATE';
      l_modifier_list_rec.offer_id              :=rec.offer_id;
    
      ozf_offer_pub.process_modifiers(p_init_msg_list     => fnd_api.g_false
                                     ,p_api_version       => 1.0
                                     ,p_commit            => fnd_api.g_false
                                     ,x_return_status     => x_return_status
                                     ,x_msg_count         => x_msg_count
                                     ,x_msg_data          => x_msg_data
                                     ,p_offer_type        => l_offer_type
                                     ,p_modifier_list_rec => l_modifier_list_rec
                                     ,p_modifier_line_tbl => l_modifier_line_tbl
                                     ,p_qualifier_tbl     => l_qualifier_tbl
                                     ,p_budget_tbl        => l_budget_tbl
                                     ,p_act_product_tbl   => l_act_product_tbl
                                     ,p_discount_tbl      => l_discount_line_tbl
                                     ,p_excl_tbl          => l_excl_rec_tbl
                                     ,p_offer_tier_tbl    => l_offer_tier_tbl
                                     ,p_prod_tbl          => l_prod_rec_tbl
                                     ,p_na_qualifier_tbl  => l_na_qualifier_tbl
                                     ,x_qp_list_header_id => x_qp_list_header_id
                                     ,x_error_location    => x_err_location);
    
      IF ((x_return_status = fnd_api.g_ret_sts_error) OR (x_return_status = fnd_api.g_ret_sts_unexp_error)) THEN        
        dbms_output.put_line('offer id ='||rec.offer_id||', failed to update');
        rollback to here_we_start;      
      ELSE       
        --dbms_output.put_line('offer id ='||rec.offer_id||', updated successfully to ONHOLD'); 
        Null;		
      END IF;          
 end loop;
 
 commit;
 dbms_output.put_line('Commit Complete...');
exception
 when others then
  rollback to here_we_start;       
end;
/