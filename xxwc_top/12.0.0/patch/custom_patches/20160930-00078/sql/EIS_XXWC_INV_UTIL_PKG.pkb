CREATE OR REPLACE 
PACKAGE BODY  XXEIS.EIS_XXWC_INV_UTIL_PKG
AS
  --//============================================================================
  --// Object Name          :: EIS_XXWC_INV_UTIL_PKG
  --//
  --// Object Type          :: Package Body
  --//
  --// Object Description   :: This Package will trigger in view and populate the values into View.
  --//
  --// Version Control
  --//============================================================================
  --// Version    Author           Date          Description
  --//----------------------------------------------------------------------------
  --// 1.0        Siva        06/06/2016         Initial Build  -- Added for TMS#20160601-00025.
  --// 1.1        Siva        10/17/2016         TMS#20160930-00078 
  --//============================================================================
FUNCTION GET_ONHAND_QTY(
    P_INVENTORY_ITEM_ID NUMBER,
    P_ORGANIZATION_ID   NUMBER,
    P_SUBINVENTORY_CODE VARCHAR2)
  RETURN number
IS
  --//============================================================================
  --// Object Usage         :: This Object Referred by "Inventory - Onhand Quantity - WC"
  --//
  --// Object Name          :: GET_ONHAND_QTY
  --//
  --// Object Type          :: Function
  --//
  --// Object Description   :: This Function will populate the values in the view.
  --//
  --// Version Control
  --//============================================================================
  --// Version    Author           Date          Description
  --//----------------------------------------------------------------------------
  --// 1.0        Siva        06/06/2016      Initial Build  -- Added for TMS#20160601-00025.
  --//============================================================================
L_HASH_INDEX varchar2(100);
L_SQL varchar2(32000);
begin
g_onhand_qty := NULL;

l_sql :='SELECT SUM (NVL(moq1.Primary_Transaction_Quantity,0))
    FROM Mtl_Onhand_Quantities_Detail Moq1
    WHERE Moq1.Inventory_Item_Id = :1
    AND Moq1.Organization_Id     = :2
    AND Moq1.subinventory_code   = :3';
    BEGIN
        l_hash_index:=P_INVENTORY_ITEM_ID||'-'||P_ORGANIZATION_ID||'-'||P_SUBINVENTORY_CODE;
        g_onhand_qty := G_ONHAND_QTY_VLDN_TBL(L_HASH_INDEX);
 exception
    when no_data_found
    THEN
    begin      
    EXECUTE IMMEDIATE L_SQL INTO g_onhand_qty USING P_INVENTORY_ITEM_ID,P_ORGANIZATION_ID,P_SUBINVENTORY_CODE;
    EXCEPTION WHEN NO_DATA_FOUND THEN
    g_onhand_qty :=0;
    WHEN OTHERS THEN
    g_onhand_qty :=0;
    END;      
                      l_hash_index:=P_INVENTORY_ITEM_ID||'-'||P_ORGANIZATION_ID||'-'||P_SUBINVENTORY_CODE;
                       G_ONHAND_QTY_VLDN_TBL(L_HASH_INDEX) := g_onhand_qty;
    END;
     return  g_onhand_qty;
     EXCEPTION when OTHERS then
      g_onhand_qty:=0;
      RETURN  g_onhand_qty;

end GET_ONHAND_QTY;

FUNCTION get_invoice_url(
    p_po_distribution_id NUMBER,
    p_period_name        VARCHAR2,
    p_request_type       VARCHAR2)
  RETURN VARCHAR2
IS
  --//============================================================================
  --// Object Usage         :: This Object Referred by "HDS Account Analysis Subledger Detail Report"
  --//
  --// Object Name          :: get_invoice_url
  --//
  --// Object Type          :: Function
  --//
  --// Object Description   :: This Function will populate the values in the view.
  --//
  --// Version Control
  --//============================================================================
  --// Version    Author           Date          Description
  --//----------------------------------------------------------------------------
  --// 1.1        Siva        10/07/2016        TMS#20160930-00078
  --//============================================================================  
  L_HASH_INDEX VARCHAR2(100);
  l_sql        VARCHAR2(32000);
  CURSOR v_inv_url_cur
  IS
    SELECT DISTINCT a.url,
      ai.invoice_num
    FROM fnd_attached_docs_form_vl a,
      ap_invoice_distributions b,
      ap_invoices_all ai
    WHERE b.po_distribution_id = p_po_distribution_id
    AND b.period_name          = p_period_name
    AND b.invoice_id           = ai.invoice_id
    AND a.entity_name          = 'AP_INVOICES'
    AND a.pk1_value            = TO_CHAR (b.invoice_id)
    AND a.datatype_name        = 'Web Page'
    AND A.category_description = 'Invoice Internal'
    AND a.document_description = 'Documentum Image URL'
    AND A.function_name        = 'APXINWKB' ;
BEGIN
  g_url         := NULL;
  g_invoice_num := NULL;
  BEGIN
    l_hash_index       :=p_po_distribution_id||'-'||p_period_name;
    IF P_request_type   ='URL' THEN
      g_url            := g_url_vldn_tbl(L_HASH_INDEX);
    ELSif P_request_type='INVOICENUM' THEN
      g_invoice_num    := G_invoice_num_VLDN_TBL(L_HASH_INDEX);
    END IF;
  EXCEPTION
  WHEN no_data_found THEN
    BEGIN
      FOR rec IN v_inv_url_cur
      LOOP
        g_url         := g_url || ' , ' || rec.url;
        g_invoice_num := g_invoice_num || ' , ' || rec.invoice_num;
      END LOOP;
      g_url         := SUBSTR (g_url, 4);
      g_invoice_num := SUBSTR (g_invoice_num, 4);
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
      IF P_request_type   ='URL' THEN
        g_url            := NULL;
      elsif P_request_type='INVOICENUM' THEN
        g_invoice_num    := NULL;
      END IF;
    END;
    l_hash_index                         :=p_po_distribution_id||'-'||p_period_name;
    g_url_vldn_tbl(l_hash_index)         := g_url;
    G_invoice_num_VLDN_TBL(L_HASH_INDEX) := g_invoice_num;
  END;
  IF P_request_type='URL' THEN
    RETURN g_url ;
  elsif P_request_type='INVOICENUM' THEN
    RETURN g_invoice_num ;
  END IF;
EXCEPTION
WHEN OTHERS THEN
  IF P_request_type='URL' THEN
    g_url         := NULL;
    RETURN g_url ;
  elsif P_request_type='INVOICENUM' THEN
    g_invoice_num    := NULL;
    RETURN g_invoice_num ;
  END IF;
END get_invoice_url;

END EIS_XXWC_INV_UTIL_PKG;
/
