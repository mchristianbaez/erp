--Report Name            : HDS Account Analysis Subledger Detail Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating View Data for HDS Account Analysis Subledger Detail Report
set scan off define off
DECLARE
BEGIN 
--Inserting View XXHDS_EIS_GL_SL_180_V
xxeis.eis_rs_ins.v( 'XXHDS_EIS_GL_SL_180_V',101,'','','','','SA059956','XXEIS','Xxhds Eis Gl Sl 180 V','XEGS1V','','');
--Delete View Columns for XXHDS_EIS_GL_SL_180_V
xxeis.eis_rs_utility.delete_view_rows('XXHDS_EIS_GL_SL_180_V',101,FALSE);
--Inserting View Columns for XXHDS_EIS_GL_SL_180_V
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','SLA_DIST_ACCOUNTED_DR',101,'Sla Dist Accounted Dr','SLA_DIST_ACCOUNTED_DR','','','','SA059956','NUMBER','','','Sla Dist Accounted Dr','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','PERIOD_NAME',101,'Period Name','PERIOD_NAME','','','','SA059956','VARCHAR2','','','Period Name','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','EFFECTIVE_PERIOD_NUM',101,'Effective Period Num','EFFECTIVE_PERIOD_NUM','','','','SA059956','NUMBER','','','Effective Period Num','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','ACCOUNTED_CR',101,'Accounted Cr','ACCOUNTED_CR','','','','SA059956','NUMBER','','','Accounted Cr','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','SLA_LINE_ENTERED_CR',101,'Sla Line Entered Cr','SLA_LINE_ENTERED_CR','','','','SA059956','NUMBER','','','Sla Line Entered Cr','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','SLA_DIST_ACCOUNTED_NET',101,'Sla Dist Accounted Net','SLA_DIST_ACCOUNTED_NET','','','','SA059956','NUMBER','','','Sla Dist Accounted Net','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','SLA_LINE_ACCOUNTED_DR',101,'Sla Line Accounted Dr','SLA_LINE_ACCOUNTED_DR','','','','SA059956','NUMBER','','','Sla Line Accounted Dr','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','SLA_DIST_ACCOUNTED_CR',101,'Sla Dist Accounted Cr','SLA_DIST_ACCOUNTED_CR','','','','SA059956','NUMBER','','','Sla Dist Accounted Cr','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','LINE_ACCTD_CR',101,'Line Acctd Cr','LINE_ACCTD_CR','','','','SA059956','NUMBER','','','Line Acctd Cr','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','ACC_DATE',101,'Acc Date','ACC_DATE','','','','SA059956','DATE','','','Acc Date','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','JE_CATEGORY',101,'Je Category','JE_CATEGORY','','','','SA059956','VARCHAR2','','','Je Category','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','ENTERED_DR',101,'Entered Dr','ENTERED_DR','','','','SA059956','NUMBER','','','Entered Dr','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','SLA_LINE_ENTERED_NET',101,'Sla Line Entered Net','SLA_LINE_ENTERED_NET','','','','SA059956','NUMBER','','','Sla Line Entered Net','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','LLINE',101,'Lline','LLINE','','','','SA059956','NUMBER','','','Lline','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','SLA_LINE_ENTERED_DR',101,'Sla Line Entered Dr','SLA_LINE_ENTERED_DR','','','','SA059956','NUMBER','','','Sla Line Entered Dr','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','TRANSACTION_NUM',101,'Transaction Num','TRANSACTION_NUM','','','','SA059956','VARCHAR2','','','Transaction Num','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','CURRENCY_CODE',101,'Currency Code','CURRENCY_CODE','','','','SA059956','VARCHAR2','','','Currency Code','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','JE_LINE_NUM',101,'Je Line Num','JE_LINE_NUM','','','','SA059956','NUMBER','','','Je Line Num','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','CUSTOMER_OR_VENDOR',101,'Customer Or Vendor','CUSTOMER_OR_VENDOR','','','','SA059956','VARCHAR2','','','Customer Or Vendor','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','SLA_DIST_ENTERED_DR',101,'Sla Dist Entered Dr','SLA_DIST_ENTERED_DR','','','','SA059956','NUMBER','','','Sla Dist Entered Dr','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','LSEQUENCE',101,'Lsequence','LSEQUENCE','','','','SA059956','VARCHAR2','','','Lsequence','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','ENTRY',101,'Entry','ENTRY','','','','SA059956','VARCHAR2','','','Entry','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','ASSOCIATE_NUM',101,'Associate Num','ASSOCIATE_NUM','','','','SA059956','VARCHAR2','','','Associate Num','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','LINE_DESCR',101,'Line Descr','LINE_DESCR','','','','SA059956','VARCHAR2','','','Line Descr','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','ACCOUNTED_DR',101,'Accounted Dr','ACCOUNTED_DR','','','','SA059956','NUMBER','','','Accounted Dr','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','BATCH',101,'Batch','BATCH','','','','SA059956','NUMBER','','','Batch','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','SLA_DIST_ENTERED_CR',101,'Sla Dist Entered Cr','SLA_DIST_ENTERED_CR','','','','SA059956','NUMBER','','','Sla Dist Entered Cr','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','HNUMBER',101,'Hnumber','HNUMBER','','','','SA059956','NUMBER','','','Hnumber','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','SLA_LINE_ACCOUNTED_CR',101,'Sla Line Accounted Cr','SLA_LINE_ACCOUNTED_CR','','','','SA059956','NUMBER','','','Sla Line Accounted Cr','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','SLA_LINE_ACCOUNTED_NET',101,'Sla Line Accounted Net','SLA_LINE_ACCOUNTED_NET','','','','SA059956','NUMBER','','','Sla Line Accounted Net','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','PO_NUMBER',101,'Po Number','PO_NUMBER','','','','SA059956','VARCHAR2','','','Po Number','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','GL_ACCOUNT_STRING',101,'Gl Account String','GL_ACCOUNT_STRING','','','','SA059956','VARCHAR2','','','Gl Account String','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','LINE_ENT_CR',101,'Line Ent Cr','LINE_ENT_CR','','','','SA059956','NUMBER','','','Line Ent Cr','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','SLA_DIST_ENTERED_NET',101,'Sla Dist Entered Net','SLA_DIST_ENTERED_NET','','','','SA059956','NUMBER','','','Sla Dist Entered Net','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','TYPE',101,'Type','TYPE','','','','SA059956','VARCHAR2','','','Type','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','LINE_ACCTD_DR',101,'Line Acctd Dr','LINE_ACCTD_DR','','','','SA059956','NUMBER','','','Line Acctd Dr','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','ENTERED_CR',101,'Entered Cr','ENTERED_CR','','','','SA059956','NUMBER','','','Entered Cr','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','BATCH_NAME',101,'Batch Name','BATCH_NAME','','','','SA059956','VARCHAR2','','','Batch Name','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','NAME',101,'Name','NAME','','','','SA059956','VARCHAR2','','','Name','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','SEQ_NUM',101,'Seq Num','SEQ_NUM','','','','SA059956','NUMBER','','','Seq Num','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','LINE_ENT_DR',101,'Line Ent Dr','LINE_ENT_DR','','','','SA059956','NUMBER','','','Line Ent Dr','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','SOURCE',101,'Source','SOURCE','','','','SA059956','VARCHAR2','','','Source','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','GCC50328ACCOUNT',101,'Gcc50328account','GCC50328ACCOUNT','','','','SA059956','VARCHAR2','','','Gcc50328account','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','GCC50328PRODUCT',101,'Gcc50328product','GCC50328PRODUCT','','','','SA059956','VARCHAR2','','','Gcc50328product','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','CODE_COMBINATION_ID',101,'Code Combination Id','CODE_COMBINATION_ID','','','','SA059956','NUMBER','','','Code Combination Id','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','H_SEQ_ID',101,'H Seq Id','H_SEQ_ID','','','','SA059956','NUMBER','','','H Seq Id','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','JE_HEADER_ID',101,'Je Header Id','JE_HEADER_ID','','','','SA059956','NUMBER','','','Je Header Id','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','SEQ_ID',101,'Seq Id','SEQ_ID','','','','SA059956','NUMBER','','','Seq Id','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','SLA_EVENT_TYPE',101,'Sla Event Type','SLA_EVENT_TYPE','','','','SA059956','VARCHAR2','','','Sla Event Type','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','GL_SL_LINK_ID',101,'Gl Sl Link Id','GL_SL_LINK_ID','','','','SA059956','NUMBER','','','Gl Sl Link Id','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','GCC50328ACCOUNTDESCR',101,'Gcc50328accountdescr','GCC50328ACCOUNTDESCR','','','','SA059956','VARCHAR2','','','Gcc50328accountdescr','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','GCC50328COST_CENTER',101,'Gcc50328cost Center','GCC50328COST_CENTER','','','','SA059956','VARCHAR2','','','Gcc50328cost Center','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','GCC50328COST_CENTERDESCR',101,'Gcc50328cost Centerdescr','GCC50328COST_CENTERDESCR','','','','SA059956','VARCHAR2','','','Gcc50328cost Centerdescr','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','GCC50328FURTURE_USE',101,'Gcc50328furture Use','GCC50328FURTURE_USE','','','','SA059956','VARCHAR2','','','Gcc50328furture Use','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','GCC50328FURTURE_USEDESCR',101,'Gcc50328furture Usedescr','GCC50328FURTURE_USEDESCR','','','','SA059956','VARCHAR2','','','Gcc50328furture Usedescr','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','GCC50328FUTURE_USE_2',101,'Gcc50328future Use 2','GCC50328FUTURE_USE_2','','','','SA059956','VARCHAR2','','','Gcc50328future Use 2','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','GCC50328FUTURE_USE_2DESCR',101,'Gcc50328future Use 2descr','GCC50328FUTURE_USE_2DESCR','','','','SA059956','VARCHAR2','','','Gcc50328future Use 2descr','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','GCC50328LOCATION',101,'Gcc50328location','GCC50328LOCATION','','','','SA059956','VARCHAR2','','','Gcc50328location','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','GCC50328LOCATIONDESCR',101,'Gcc50328locationdescr','GCC50328LOCATIONDESCR','','','','SA059956','VARCHAR2','','','Gcc50328locationdescr','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','GCC50328PRODUCTDESCR',101,'Gcc50328productdescr','GCC50328PRODUCTDESCR','','','','SA059956','VARCHAR2','','','Gcc50328productdescr','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','GCC50328PROJECT_CODE',101,'Gcc50328project Code','GCC50328PROJECT_CODE','','','','SA059956','VARCHAR2','','','Gcc50328project Code','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','GCC50328PROJECT_CODEDESCR',101,'Gcc50328project Codedescr','GCC50328PROJECT_CODEDESCR','','','','SA059956','VARCHAR2','','','Gcc50328project Codedescr','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','GCC50368ACCOUNT',101,'Gcc50368account','GCC50368ACCOUNT','','','','SA059956','VARCHAR2','','','Gcc50368account','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','GCC50368ACCOUNTDESCR',101,'Gcc50368accountdescr','GCC50368ACCOUNTDESCR','','','','SA059956','VARCHAR2','','','Gcc50368accountdescr','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','GCC50368DEPARTMENT',101,'Gcc50368department','GCC50368DEPARTMENT','','','','SA059956','VARCHAR2','','','Gcc50368department','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','GCC50368DEPARTMENTDESCR',101,'Gcc50368departmentdescr','GCC50368DEPARTMENTDESCR','','','','SA059956','VARCHAR2','','','Gcc50368departmentdescr','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','GCC50368DIVISION',101,'Gcc50368division','GCC50368DIVISION','','','','SA059956','VARCHAR2','','','Gcc50368division','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','GCC50368DIVISIONDESCR',101,'Gcc50368divisiondescr','GCC50368DIVISIONDESCR','','','','SA059956','VARCHAR2','','','Gcc50368divisiondescr','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','GCC50368FUTURE_USE',101,'Gcc50368future Use','GCC50368FUTURE_USE','','','','SA059956','VARCHAR2','','','Gcc50368future Use','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','GCC50368FUTURE_USEDESCR',101,'Gcc50368future Usedescr','GCC50368FUTURE_USEDESCR','','','','SA059956','VARCHAR2','','','Gcc50368future Usedescr','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','GCC50368PRODUCT',101,'Gcc50368product','GCC50368PRODUCT','','','','SA059956','VARCHAR2','','','Gcc50368product','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','GCC50368PRODUCTDESCR',101,'Gcc50368productdescr','GCC50368PRODUCTDESCR','','','','SA059956','VARCHAR2','','','Gcc50368productdescr','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','GCC50368SUBACCOUNT',101,'Gcc50368subaccount','GCC50368SUBACCOUNT','','','','SA059956','VARCHAR2','','','Gcc50368subaccount','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','GCC50368SUBACCOUNTDESCR',101,'Gcc50368subaccountdescr','GCC50368SUBACCOUNTDESCR','','','','SA059956','VARCHAR2','','','Gcc50368subaccountdescr','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','AP_INV_DATE',101,'Ap Inv Date','AP_INV_DATE','','','','SA059956','DATE','','','Ap Inv Date','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','AP_INV_SOURCE',101,'Ap Inv Source','AP_INV_SOURCE','','','','SA059956','VARCHAR2','','','Ap Inv Source','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','BOL_NUMBER',101,'Bol Number','BOL_NUMBER','','','','SA059956','VARCHAR2','','','Bol Number','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','CUSTOMER_OR_VENDOR_NUMBER',101,'Customer Or Vendor Number','CUSTOMER_OR_VENDOR_NUMBER','','','','SA059956','VARCHAR2','','','Customer Or Vendor Number','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','ITEM_MATERIAL_COST',101,'Item Material Cost','ITEM_MATERIAL_COST','','','','SA059956','NUMBER','','','Item Material Cost','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','ITEM_OVRHD_COST',101,'Item Ovrhd Cost','ITEM_OVRHD_COST','','','','SA059956','NUMBER','','','Item Ovrhd Cost','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','ITEM_TXN_QTY',101,'Item Txn Qty','ITEM_TXN_QTY','','','','SA059956','NUMBER','','','Item Txn Qty','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','ITEM_UNIT_WT',101,'Item Unit Wt','ITEM_UNIT_WT','','','','SA059956','NUMBER','','','Item Unit Wt','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','PART_DESCRIPTION',101,'Part Description','PART_DESCRIPTION','','','','SA059956','VARCHAR2','','','Part Description','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','PART_NUMBER',101,'Part Number','PART_NUMBER','','','','SA059956','VARCHAR2','','','Part Number','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','SALES_ORDER',101,'Sales Order','SALES_ORDER','','','','SA059956','VARCHAR2','','','Sales Order','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','XXCUS_IMAGE_LINK',101,'Xxcus Image Link','XXCUS_IMAGE_LINK','','','','SA059956','VARCHAR2','','','Xxcus Image Link','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','XXCUS_LINE_DESC',101,'Xxcus Line Desc','XXCUS_LINE_DESC','','','','SA059956','VARCHAR2','','','Xxcus Line Desc','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','XXCUS_PO_CREATED_BY',101,'Xxcus Po Created By','XXCUS_PO_CREATED_BY','','','','SA059956','VARCHAR2','','','Xxcus Po Created By','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','APPLIED_TO_SOURCE_ID_NUM_1',101,'Applied To Source Id Num 1','APPLIED_TO_SOURCE_ID_NUM_1','','','','SA059956','NUMBER','','','Applied To Source Id Num 1','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','FETCH_SEQ',101,'Fetch Seq','FETCH_SEQ','','','','SA059956','NUMBER','','','Fetch Seq','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','SOURCE_DIST_ID_NUM_1',101,'Source Dist Id Num 1','SOURCE_DIST_ID_NUM_1','','','','SA059956','NUMBER','','','Source Dist Id Num 1','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','SOURCE_DIST_TYPE',101,'Source Dist Type','SOURCE_DIST_TYPE','','','','SA059956','VARCHAR2','','','Source Dist Type','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_SL_180_V','WAYBILL',101,'Waybill','WAYBILL','','','','SA059956','VARCHAR2','','','Waybill','','','');
--Inserting View Components for XXHDS_EIS_GL_SL_180_V
--Inserting View Component Joins for XXHDS_EIS_GL_SL_180_V
END;
/
set scan on define on
prompt Creating Report LOV Data for HDS Account Analysis Subledger Detail Report
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - HDS Account Analysis Subledger Detail Report
xxeis.eis_rs_ins.lov( 101,'SELECT distinct user_je_source_name FROM gl_je_sources','','EIS_GL_JE_SOURCES_LOV','LOV of all Available Journal Sources','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( '','SELECT ffv.flex_value, ffvtl.description
            FROM
                fnd_flex_value_sets ffvs ,
                fnd_flex_values ffv,
                fnd_flex_values_tl ffvtl
            WHERE   upper(ffvs.flex_value_set_name) = upper(''XXCUS_GL_PRODUCT'')
             and ffv.flex_value_set_id = ffvs.flex_value_set_id
             and ffv.FLEX_VALUE_ID = ffvtl.FLEX_VALUE_ID
            AND ffv.enabled_flag = upper(''Y'')
            AND ffv.summary_flag <> upper(''Y'')
            AND ffvtl.LANGUAGE = USERENV(''LANG'')
            order by ffv.flex_value    ','','XXCUS_GL_PRODUCT','XXCUS_GL_PRODUCT','MM050208',NULL,'','','');
xxeis.eis_rs_ins.lov( '','SELECT ffv.flex_value, ffvtl.description
            FROM
                fnd_flex_value_sets ffvs ,
                fnd_flex_values ffv,
                fnd_flex_values_tl ffvtl
            WHERE   upper(ffvs.flex_value_set_name) = upper(''XXCUS_GL_LOCATION'')
             and ffv.flex_value_set_id = ffvs.flex_value_set_id
             and ffv.FLEX_VALUE_ID = ffvtl.FLEX_VALUE_ID
            AND ffv.enabled_flag = upper(''Y'')
            AND ffv.summary_flag <> upper(''Y'')
            AND ffvtl.LANGUAGE = USERENV(''LANG'')
            order by ffv.flex_value    ','','XXCUS_GL_LOCATION','XXCUS_GL_LOCATION','MM050208',NULL,'','','');
xxeis.eis_rs_ins.lov( '','SELECT ffv.flex_value, ffvtl.description
			FROM
				fnd_flex_value_sets ffvs ,
				fnd_flex_values ffv,
				fnd_flex_values_tl ffvtl
			WHERE   upper(ffvs.flex_value_set_name) = upper(''XXCUS_GL_COSTCENTER'')
			 and ffv.flex_value_set_id = ffvs.flex_value_set_id
			 and ffv.FLEX_VALUE_ID = ffvtl.FLEX_VALUE_ID
			AND ffv.enabled_flag = upper(''Y'')
			AND ffv.summary_flag <> upper(''Y'')
			AND ffvtl.LANGUAGE = USERENV(''LANG'')
			order by ffv.flex_value	','','XXCUS_GL_COSTCENTER','XXCUS_GL_COSTCENTER','MM050208',NULL,'','','');
xxeis.eis_rs_ins.lov( '','SELECT ffv.flex_value, ffvtl.description
            FROM
                fnd_flex_value_sets ffvs ,
                fnd_flex_values ffv,
                fnd_flex_values_tl ffvtl
            WHERE   upper(ffvs.flex_value_set_name) = upper(''XXCUS_GL_ACCOUNT'')
             and ffv.flex_value_set_id = ffvs.flex_value_set_id
             and ffv.FLEX_VALUE_ID = ffvtl.FLEX_VALUE_ID
            AND ffv.enabled_flag = upper(''Y'')
            AND ffv.summary_flag <> upper(''Y'')
            AND ffvtl.LANGUAGE = USERENV(''LANG'')
            order by ffv.flex_value    ','','XXCUS_GL_ACCOUNT','XXCUS_GL_ACCOUNT','MM050208',NULL,'','','');
xxeis.eis_rs_ins.lov( '','SELECT ffv.flex_value, ffvtl.description
            FROM
                fnd_flex_value_sets ffvs ,
                fnd_flex_values ffv,
                fnd_flex_values_tl ffvtl
            WHERE   upper(ffvs.flex_value_set_name) = upper(''XXCUS_GL_ACCOUNT'')
             and ffv.flex_value_set_id = ffvs.flex_value_set_id
             and ffv.FLEX_VALUE_ID = ffvtl.FLEX_VALUE_ID
            AND ffv.enabled_flag = upper(''Y'')
            AND ffv.summary_flag <> upper(''Y'')
            AND ffvtl.LANGUAGE = USERENV(''LANG'')
            order by ffv.flex_value    ','','XXCUS_GL_ACCOUNT','XXCUS_GL_ACCOUNT','MM050208',NULL,'','','');
xxeis.eis_rs_ins.lov( '','SELECT ffv.flex_value, ffvtl.description
			FROM
				fnd_flex_value_sets ffvs ,
				fnd_flex_values ffv,
				fnd_flex_values_tl ffvtl
			WHERE   upper(ffvs.flex_value_set_name) = upper(''XXCUS_GL_PROJECT'')
			 and ffv.flex_value_set_id = ffvs.flex_value_set_id
			 and ffv.FLEX_VALUE_ID = ffvtl.FLEX_VALUE_ID
			AND ffv.enabled_flag = upper(''Y'')
			AND ffv.summary_flag <> upper(''Y'')
			AND ffvtl.LANGUAGE = USERENV(''LANG'')
			order by ffv.flex_value	','','XXCUS_GL_PROJECT','XXCUS_GL_PROJECT','MM050208',NULL,'','','');
xxeis.eis_rs_ins.lov( 101,'select distinct period_name
from gl_je_headers','','HDS_GL_PERIOD_NAME','GL Period Name','XXEIS_RS_ADMIN',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for HDS Account Analysis Subledger Detail Report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - HDS Account Analysis Subledger Detail Report
xxeis.eis_rs_utility.delete_report_rows( 'HDS Account Analysis Subledger Detail Report' );
--Inserting Report - HDS Account Analysis Subledger Detail Report
xxeis.eis_rs_ins.r( 101,'HDS Account Analysis Subledger Detail Report','','This report will provide detail from the subledger for all entries posted for the account in general ledger.

Created from ESMS#281615','','','','SA059956','XXHDS_EIS_GL_SL_180_V','Y','','','SA059956','','N','HDS Standard Reports','','CSV,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - HDS Account Analysis Subledger Detail Report
xxeis.eis_rs_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'SLA_DIST_ENT_NET','Sla Dist Ent Net','','NUMBER','~~~','default','','30','Y','','','','','','','XEGS1V.SLA_DIST_ENTERED_NET*-1','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'SLA_EVENT_TYPE','Sla Event Type','Sla Event Type','','','default','','31','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'ACCOUNTED_CR','Accounted Cr','Accounted Cr','','~~~','default','','13','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'ACCOUNTED_DR','Accounted Dr','Accounted Dr','','~~~','default','','12','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'ACC_DATE','Acc Date','Acc Date','','','default','','6','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'BATCH_NAME','Batch Name','Batch Name','','','default','','3','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'CURRENCY_CODE','Currency Code','Currency Code','','','default','','15','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'CUSTOMER_OR_VENDOR','Customer Or Vendor','Customer Or Vendor','','','default','','11','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'EFFECTIVE_PERIOD_NUM','Effective Period Num','Effective Period Num','','~~~','default','','16','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'ENTERED_CR','Entered Cr','Entered Cr','','~~~','default','','18','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'ENTERED_DR','Entered Dr','Entered Dr','','~~~','default','','17','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'ENTRY','Entry','Entry','','','default','','19','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'GL_ACCOUNT_STRING','Gl Account String','Gl Account String','','','default','','2','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'HNUMBER','Hnumber','Hnumber','','~~~','default','','20','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'JE_CATEGORY','Je Category','Je Category','','','default','','4','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'JE_LINE_NUM','Je Line Num','Je Line Num','','~~~','default','','21','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'LINE_ACCTD_CR','Line Acctd Cr','Line Acctd Cr','','~~~','default','','23','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'LINE_ACCTD_DR','Line Acctd Dr','Line Acctd Dr','','~~~','default','','22','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'LINE_DESCR','Line Descr','Line Descr','','','default','','5','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'LINE_ENT_CR','Line Ent Cr','Line Ent Cr','','~~~','default','','25','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'LINE_ENT_DR','Line Ent Dr','Line Ent Dr','','~~~','default','','24','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'NAME','Name','Name','','','default','','26','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'PERIOD_NAME','Period Name','Period Name','','','default','','1','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'PO_NUMBER','Po Number','Po Number','','','default','','8','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'SLA_DIST_ACCOUNTED_CR','Sla Dist Accounted Cr','Sla Dist Accounted Cr','','~~~','default','','28','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'SLA_DIST_ACCOUNTED_DR','Sla Dist Accounted Dr','Sla Dist Accounted Dr','','~~~','default','','27','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'SLA_DIST_ACCOUNTED_NET','Sla Dist Accounted Net','Sla Dist Accounted Net','','~~~','default','','29','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'SOURCE','Source','Source','','','default','','7','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'TRANSACTION_NUM','Transaction Num','Transaction Num','','','default','','9','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'TYPE','Type','Type','','','default','','10','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'GCC50328ACCOUNT','Gcc50328account','Gcc50328account','','','default','','32','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'GCC50328ACCOUNTDESCR','Gcc50328accountdescr','Gcc50328accountdescr','','','default','','33','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'GCC50328LOCATION','Gcc50328location','Gcc50328location','','','default','','34','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'GCC50328LOCATIONDESCR','Gcc50328locationdescr','Gcc50328locationdescr','','','default','','35','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'GCC50328PRODUCT','Gcc50328product','Gcc50328product','','','default','','36','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'GCC50328PROJECT_CODE','Gcc50328project Code','Gcc50328project Code','','','default','','37','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'GL_SL_LINK_ID','Gl Sl Link Id','Gl Sl Link Id','','~~~','default','','38','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'AP_INV_DATE','Ap Inv Date','Ap Inv Date','','','default','','39','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'BOL_NUMBER','Bol Number','Bol Number','','','default','','40','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'CUSTOMER_OR_VENDOR_NUMBER','Customer Or Vendor Number','Customer Or Vendor Number','','','default','','41','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'XXCUS_IMAGE_LINK','Xxcus Image Link','Xxcus Image Link','','','default','','42','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'XXCUS_LINE_DESC','Xxcus Line Desc','Xxcus Line Desc','','','default','','43','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'XXCUS_PO_CREATED_BY','Xxcus Po Created By','Xxcus Po Created By','','','default','','44','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'WAYBILL','Waybill','Waybill','','','default','','45','','Y','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','');
--Inserting Report Parameters - HDS Account Analysis Subledger Detail Report
xxeis.eis_rs_ins.rp( 'HDS Account Analysis Subledger Detail Report',101,'Location','Gcc50328location','GCC50328LOCATION','IN','XXCUS_GL_LOCATION','','VARCHAR2','N','Y','3','','Y','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'HDS Account Analysis Subledger Detail Report',101,'Period Name','Period Name','PERIOD_NAME','IN','HDS_GL_PERIOD_NAME','','VARCHAR2','Y','Y','1','','Y','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'HDS Account Analysis Subledger Detail Report',101,'Product','Gcc50328product','GCC50328PRODUCT','IN','XXCUS_GL_PRODUCT','','VARCHAR2','Y','Y','2','','Y','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'HDS Account Analysis Subledger Detail Report',101,'Account Low','Gcc50328account','GCC50328ACCOUNT','>=','XXCUS_GL_ACCOUNT','','VARCHAR2','Y','Y','4','','Y','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'HDS Account Analysis Subledger Detail Report',101,'Cost Center','Gcc50328cost Center','GCC50328COST_CENTER','IN','XXCUS_GL_COSTCENTER','','VARCHAR2','N','Y','6','','Y','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'HDS Account Analysis Subledger Detail Report',101,'Project Code','Gcc50328project Code','GCC50328PROJECT_CODE','IN','XXCUS_GL_PROJECT','','VARCHAR2','N','Y','7','','Y','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'HDS Account Analysis Subledger Detail Report',101,'Source','Source','SOURCE','IN','EIS_GL_JE_SOURCES_LOV','','VARCHAR2','N','Y','8','','Y','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'HDS Account Analysis Subledger Detail Report',101,'Account High','Gcc50328account','GCC50328ACCOUNT','<=','XXCUS_GL_ACCOUNT','','VARCHAR2','Y','Y','5','','Y','CONSTANT','SA059956','Y','N','','','');
--Inserting Report Conditions - HDS Account Analysis Subledger Detail Report
xxeis.eis_rs_ins.rcn( 'HDS Account Analysis Subledger Detail Report',101,'PERIOD_NAME','IN',':Period Name','','','Y','1','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'HDS Account Analysis Subledger Detail Report',101,'GCC50328PRODUCT','IN',':Product','','','Y','2','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'HDS Account Analysis Subledger Detail Report',101,'GCC50328LOCATION','IN',':Location','','','Y','3','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'HDS Account Analysis Subledger Detail Report',101,'GCC50328ACCOUNT','>=',':Account Low','','','Y','4','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'HDS Account Analysis Subledger Detail Report',101,'GCC50328ACCOUNT','<=',':Account High','','','Y','5','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'HDS Account Analysis Subledger Detail Report',101,'GCC50328COST_CENTER','IN',':Cost Center','','','Y','6','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'HDS Account Analysis Subledger Detail Report',101,'GCC50328PROJECT_CODE','IN',':Project Code','','','Y','7','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'HDS Account Analysis Subledger Detail Report',101,'SOURCE','IN',':Source','','','Y','8','Y','SA059956');
--Inserting Report Sorts - HDS Account Analysis Subledger Detail Report
--Inserting Report Triggers - HDS Account Analysis Subledger Detail Report
--Inserting Report Templates - HDS Account Analysis Subledger Detail Report
--Inserting Report Portals - HDS Account Analysis Subledger Detail Report
--Inserting Report Dashboards - HDS Account Analysis Subledger Detail Report
--Inserting Report Security - HDS Account Analysis Subledger Detail Report
xxeis.eis_rs_ins.rsec( 'HDS Account Analysis Subledger Detail Report','101','','50618',101,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'HDS Account Analysis Subledger Detail Report','101','','50617',101,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'HDS Account Analysis Subledger Detail Report','101','','50668',101,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'HDS Account Analysis Subledger Detail Report','101','','50667',101,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'HDS Account Analysis Subledger Detail Report','101','','50982',101,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'HDS Account Analysis Subledger Detail Report','101','','51350',101,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'HDS Account Analysis Subledger Detail Report','101','','51349',101,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'HDS Account Analysis Subledger Detail Report','101','','50780',101,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'HDS Account Analysis Subledger Detail Report','101','','50826',101,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'HDS Account Analysis Subledger Detail Report','101','','50825',101,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'HDS Account Analysis Subledger Detail Report','101','','50722',101,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'HDS Account Analysis Subledger Detail Report','101','','50824',101,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'HDS Account Analysis Subledger Detail Report','101','','50665',101,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'HDS Account Analysis Subledger Detail Report','101','','50801',101,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'HDS Account Analysis Subledger Detail Report','101','','50723',101,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'HDS Account Analysis Subledger Detail Report','101','','50822',101,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'HDS Account Analysis Subledger Detail Report','101','','50720',101,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'HDS Account Analysis Subledger Detail Report','101','','50662',101,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'HDS Account Analysis Subledger Detail Report','101','','50823',101,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'HDS Account Analysis Subledger Detail Report','101','','50821',101,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'HDS Account Analysis Subledger Detail Report','101','','50721',101,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'HDS Account Analysis Subledger Detail Report','','SS084202','',101,'SA059956','','');
--Inserting Report Pivots - HDS Account Analysis Subledger Detail Report
END;
/
set scan on define on
