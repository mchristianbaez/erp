  /********************************************************************************
  FILE NAME: XXWC_ORDER_API_GRANTS.sql
  
  PROGRAM TYPE: Order API grants for online sales order form
  
  PURPOSE: Order API grants for online sales order form
  
  HISTORY
  ==========================================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- ----------------------------------------------------
  1.0     03/07/2017    Rakesh P.       TMS#20170111-00059-Hand Write Oracle API for Web Service
                                        Initial Version
  2.0     08/29/2017    Rakesh P.       TMS#20170822-00145-Mobile Quote PLSQL api changes	
  4.1     09/28/2017    Rakesh P.       TMS#20170928-00125-Mobile Quote app not populating the UNIT_LIST_PRICE (Market Price)  
  *******************************************************************************************/
GRANT ALL ON xxwc.xxwc_order_header_rec TO INTERFACE_OSO;

GRANT ALL ON xxwc.xxwc_order_line_rec TO INTERFACE_OSO;

GRANT ALL ON xxwc.XXWC_ORDER_LINES_TBL TO INTERFACE_OSO;

GRANT ALL ON xxwc.xxwc_address_rec TO INTERFACE_OSO;

GRANT ALL ON xxwc.xxwc_parts_on_fly_rec to INTERFACE_OSO;

GRANT ALL ON APPS.XXWC_INV_PARTS_ON_FLY_PKG to INTERFACE_OSO;

GRANT SELECT ON XXWC.XXWC_OE_AIS_LPP_TBL to INTERFACE_MSSQL;

GRANT ALL ON APPS.XXWC_OM_SO_CREATE to INTERFACE_OSO;