  /********************************************************************************
  FILE NAME: XXWC_ORDER_HEADER_REC.sql
  
  PROGRAM TYPE: Order header for online sales order form
  
  PURPOSE: Order header for online sales order form
  
  HISTORY
  ==========================================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- ----------------------------------------------------
  1.0     03/07/2017    Rakesh P.       TMS#20170111-00059-Hand Write Oracle API for Web Service
                                        Initial Version
  2.0     08/24/2017    Rakesh P.       TMS#TMS#20170822-00145-Mobile Quote PLSQL api changes   
  4.1     09/28/2017    Rakesh P.       TMS#20170928-00125-Mobile Quote app not populating the UNIT_LIST_PRICE (Market Price)  
  *******************************************************************************************/
CREATE OR REPLACE TYPE xxwc.xxwc_order_header_rec
AS
  OBJECT (   order_number           NUMBER
            ,header_id              NUMBER      
            ,order_type_id          NUMBER
            ,order_source_type      VARCHAR2(3000)
            ,org_id                 NUMBER
	        ,orig_sys_document_ref  VARCHAR2(3000)
	        ,ordered_date           DATE
	        ,payment_term_id        NUMBER
	        ,shipping_method_code   VARCHAR2(3000)
	        ,freight_carrier_code   VARCHAR2(3000)
	        ,ship_to_contact_id     NUMBER --ship to contact
	        ,booked_flag            VARCHAR2(3000)
            ,cust_po_number         VARCHAR2(3000)
            ,sold_to_org_id         NUMBER --Customer
            ,ship_to_org_id         NUMBER --Ship to
            ,ship_from_org_id       NUMBER --Warehouse
            ,invoice_to_org_id      NUMBER --bill to 	
            ,invoice_to_contact_id  NUMBER --bill to contact
            ,salesrep_id			NUMBER
            ,header_action          VARCHAR2(200)   
            ,user_id                NUMBER
            ,auth_buyer_id          VARCHAR2(3000) 
            ,offline_order_total    NUMBER
            ,quote_name             VARCHAR2(240)  --Ver 2.0
            ,quote_number           NUMBER         --Ver 2.0
            ,request_date           VARCHAR2(10)   --Ver 2.0
            ,branch_code            VARCHAR2(3)    --Ver 2.0
            ,created_by             VARCHAR2(100)  --Ver 2.0
            ,expiration_date        VARCHAR2(10)   --Ver 2.0
            ,SHIPPING_INSTRUCTIONS  VARCHAR2(2000) --Ver 2.0
            ,STATIC FUNCTION g_miss_null RETURN xxwc_order_header_rec
            );
/

CREATE OR REPLACE TYPE BODY xxwc.xxwc_order_header_rec
AS
   STATIC FUNCTION g_miss_null
       RETURN xxwc_order_header_rec
   AS
   BEGIN
      RETURN xxwc_order_header_rec ( order_number           => NULL
                                    ,header_id              => NULL  
                                    ,order_type_id          => NULL
                                    ,order_source_type       => NULL
                                    ,org_id                 => NULL
									,orig_sys_document_ref  => NULL
			                        ,ordered_date           => NULL
			                        ,payment_term_id        => NULL
			                        ,shipping_method_code   => NULL
			                        ,freight_carrier_code   => NULL   
			                        ,ship_to_contact_id     => NULL --ship to contact
			                        ,booked_flag            => NULL
                                    ,cust_po_number         => NULL
                                    ,sold_to_org_id         => NULL
                                    ,ship_to_org_id         => NULL --Ship to
                                    ,ship_from_org_id       => NULL --Warehouse
                                    ,invoice_to_org_id      => NULL --bill to
                                    ,invoice_to_contact_id  => NULL --bill to contact 	
                                    ,salesrep_id			=> NULL
                                    ,header_action          => NULL  
                                    ,user_id                => NULL
                                    ,auth_buyer_id          => NULL  
                                    ,offline_order_total    => NULL  
                                    ,quote_name             => NULL --Ver 2.0
                                    ,quote_number           => NULL --Ver 2.0
                                    ,request_date           => NULL --Ver 2.0 
                                    ,branch_code            => NULL --Ver 2.0  
                                    ,created_by             => NULL --Ver 2.0 
                                    ,expiration_date        => NULL --Ver 2.0
                                    ,SHIPPING_INSTRUCTIONS  => NULL --Ver 2.0
                                   );
   END g_miss_null;
END;
/

CREATE PUBLIC SYNONYM xxwc_order_header_rec FOR xxwc.xxwc_order_header_rec;
/

show err;
EXIT;