  /********************************************************************************
  FILE NAME: XXWC_ORDER_LINES_TBL.sql
  
  PROGRAM TYPE: Order lines for online sales order form
  
  PURPOSE: Order lines table type for online sales order form
  
  HISTORY
  ==========================================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- ----------------------------------------------------
  1.0     03/07/2017    Rakesh P.       TMS#20170111-00059-Hand Write Oracle API for Web Service
                                        Initial Version
  2.0     08/24/2017    Rakesh P.       TMS#TMS#20170822-00145-Mobile Quote PLSQL api changes	
  4.1     09/28/2017    Rakesh P.       TMS#20170928-00125-Mobile Quote app not populating the UNIT_LIST_PRICE (Market Price)  
  *******************************************************************************************/
SET serveroutput ON;

WHENEVER SQLERROR CONTINUE

CREATE OR REPLACE TYPE XXWC.XXWC_ORDER_LINES_TBL AS TABLE OF XXWC.xxwc_order_line_rec;
/

CREATE PUBLIC SYNONYM XXWC_ORDER_LINES_TBL FOR XXWC.XXWC_ORDER_LINES_TBL;
/
