---------------------------------------------------------------------------------------------------------------
/**************************************************************************************************************
  $Header XXEIS.EIS_XXWC_GL_FREIGHT_ADDER_V $
  REVISIONS   :
  VERSION 		DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -------------------------------------
  1.0    	   01-Jan-2017        	Siva   		 TMS#20161220-00127 
  1.1    	   04-May-2017        	Siva         TMS#20170501-00191 
**************************************************************************************************************/
DROP VIEW XXEIS.EIS_XXWC_GL_FREIGHT_ADDER_V;
CREATE OR REPLACE VIEW XXEIS.EIS_XXWC_GL_FREIGHT_ADDER_V (ORGANIZATION_CODE, VENDOR_NUMBER, FREIGHT_DUTY,
 IMPORT_FLAG, FREIGHT_PER_LB, FREIGHT_BASIS, VENDOR_TYPE, REGION, DISTRICT, POV_VENDOR_NUM, POV_VENDOR_ID, MP_ORGANIZATION_ID)
AS
  SELECT /*+ INDEX(pov AP_SUPPLIERS_U1) INDEX(mp XXWC_OBIEE_MTL_PARAMETERS1)*/
    mp.organization_code,
    decode(povs.vendor_site_code,null,decode(pov.segment1,null,null,(pov.segment1
    ||'-'||pov.vendor_name)),(pov.segment1
    ||'-'||pov.vendor_name
    ||' ### '||povs.vendor_site_code )) vendor_number,  --added for version 1.1
    --pov.segment1 vendor_number,  --commented for version 1.1
    --pov.vendor_name,  --commented for version 1.1
    xvm.freight_duty,
    xvm.import_flag,
    xvm.freight_per_lb,
    xvm.freight_basis,
    xvm.vendor_type,
    mp.attribute9 region,
    mp.attribute8 district,
    pov.segment1 pov_vendor_num,   --added for version 1.1
    --Primary Keys
    pov.vendor_id pov_vendor_id,
    mp.organization_id mp_organization_id
    --descr#flexfield#start
    --descr#flexfield#end
  FROM xxwc.xxwc_po_freight_burden_tbl xvm,
    apps.ap_suppliers pov,
    apps.ap_supplier_sites_all povs,  --added for version 1.1
    apps.mtl_parameters mp
  WHERE xvm.vendor_id     = pov.vendor_id(+)
  AND xvm.vendor_site_id  = povs.vendor_site_id(+)   --added for version 1.1
  AND xvm.organization_id = mp.organization_id
/
