/*************************************************************************
   *   $Header XXWCCycleCountQueryManager.java $
   *   Module Name: XXWCCycleCountQueryManager
   *   
   *   Package: package xxwc.oracle.apps.inv.count.server;
   *   
   *   Imports Classes:
   *   
   *   import java.sql.*;
   *   import oracle.apps.inv.utilities.server.UtilFns;
   *   import oracle.apps.mwa.beans.*;
   *   import oracle.apps.mwa.container.MWALib;
   *   import oracle.apps.mwa.container.Session;
   *   import oracle.apps.mwa.eventmodel.*;
   *   import oracle.apps.mwa.container.FileLogger;
   *   import oracle.apps.fnd.flexj.FlexException;
   *   import oracle.apps.inv.lov.server.ItemLOV;
   *   import oracle.apps.inv.lov.server.LocatorKFF;
   *   import oracle.apps.inv.lov.server.SubinventoryLOV;
   *   import xxwc.oracle.apps.inv.lov.server.*;
   *
   *   PURPOSE:   Java Class for XXWC Cycle Count Page Layout.
   *    
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
        1.0       01-JUN-2015   Lee Spitzer             Initial Version - 
                                                            TMS Ticket Number 20150302-00173 - RF - Cyclecount
        1.1       03-MAY-2016   Lee Spitzer             TMS Ticket Number 20151210-00014 - RF - PI Enhancements
**************************************************************************/

package xxwc.oracle.apps.inv.count.server;

import java.sql.*;
import java.util.Hashtable;
import java.util.Vector;
import oracle.apps.fnd.common.VersionInfo;
import oracle.apps.inv.invinq.server.ItemOnhand;
import oracle.apps.inv.utilities.server.UtilFns;
import oracle.apps.mwa.container.FileLogger;
import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OraclePreparedStatement;

import xxwc.oracle.apps.inv.count.server.XXWCCycleCount;
import xxwc.oracle.apps.inv.invinq.server.XXWCItemOnHand;

/*************************************************************************
 *   NAME: public class XXWCCycleCountQueryManager
 *
 *   PURPOSE:   Main class for XXWCCycleCountQueryManager
 *
 *   REVISIONS:
 *   Ver        Date        Author                     Description
 *   ---------  ----------  ---------------         -------------------------
        1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                            TMS Ticket Number 20150302-00173 - RF - Cyclecount
 **************************************************************************/

public class XXWCCycleCountQueryManager {
    

    public static final String RCS_ID =
        "$Header: XXWCCycleCountQueryManager.java 120.5 2007/12/19 15:33:21 vssrivat ship $";
    public static final boolean RCS_ID_RECORDED =
        VersionInfo.recordClassVersion("$Header: XXWCCycleCountQueryManager.java 120.5 2007/12/19 15:33:21 vssrivat ship $",
                                       "xxwc.oracle.apps.inv.count.server");
    XXWCCycleCount currentRecord;
    Connection con;

    /*************************************************************************
     *   NAME: public class gPackage
     *
     *   PURPOSE:   private method to default gPackage value for debugging
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150302-00173 - RF - Cyclecount
     **************************************************************************/


     private static String gPackage = "xxwc.oracle.apps.inv.count.server";

    /*************************************************************************
     *   NAME: public class gCallFrom
     *
     *   PURPOSE:   private method to default gCallFrom value for debugging
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150302-00173 - RF - Cyclecount
     **************************************************************************/


     private static String gCallFrom = "XXWCCycleCountQueryManager";


 
    /*************************************************************************
     *   NAME: public class gCallFrom
     *
     *   PURPOSE:   private method to default gCallFrom value for debugging
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150302-00173 - RF - Cyclecount
            1.1       03-MAY-2016   Lee Spitzer             TMS Ticket Number 20151210-00014 - RF - PI Enhancements
     **************************************************************************/


     public static String mWCCycleCount =
        //"BEGIN   XXWC_CC_MOBILE_PKG.GET_WC_ITEM_LOV (:1, :2, :3, :4, :5, :6, :7); END; "; //Removed 05/03/2016 TMS Ticket 20151210-00014
        "BEGIN   XXWC_CC_MOBILE_PKG.GET_WC_ITEM_LOV (:1, :2, :3, :4, :5, :6, :7, :8 ,:9); END; "; //Added 05/03/2016 TMS Ticket 20151210-00014

    /*************************************************************************
     *   NAME: public XXWCCycleCountQueryManager(Connection connection)
     *
     *   PURPOSE:   private method to default gCallFrom value for debugging
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150302-00173 - RF - Cyclecount
     **************************************************************************/



    public XXWCCycleCountQueryManager(Connection connection) {
        con = connection;
    }
    

    /*************************************************************************
     *   NAME: public getmWCCycleCount(Connection connection)
     *
     *   PURPOSE:   private method to default gCallFrom value for debugging
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150302-00173 - RF - Cyclecount
            1.1       03-MAY-2016   Lee Spitzer             TMS Ticket Number 20151210-00014 - RF - PI Enhancements
      **************************************************************************/
    
      //public Vector getmWCCycleCount(String p_organization_id, String p_item, String p_cycle_count_header_id, String p_entry_status_code, String p_wc_count_list_sequence, String p_wc_locator) { //Removed 05/03/2016 TMS Ticket 20151210-00014
      public Vector getmWCCycleCount(String p_organization_id, String p_item, String p_cycle_count_header_id, String p_entry_status_code, String p_wc_count_list_sequence, String p_wc_locator, String p_subinventory, String p_doctype) { //Added 05/03/2016 TMS Ticket 20151210-00014
        String gMethod = "getmWCCycleCount";
        String gCallPoint = "Start";
        FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
        Vector vector;
        OracleCallableStatement oraclecallablestatement;
        ResultSet resultset;
        vector = new Vector();
        oraclecallablestatement = null;
        resultset = null;
        try {
            gCallPoint = "Trying mWCCycleCount";
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
            gCallPoint = "Getting resultset";
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " p_organization_id " +
                                               p_organization_id);
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " p_item" +
                                               p_item);
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " p_cycle_count_header_id " +
                                               p_cycle_count_header_id);
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " p_entry_status_code " +
                                               p_entry_status_code);
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " p_wc_count_list_sequence " +
                                               p_wc_count_list_sequence);
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " p_wc_locator " +
                                               p_wc_locator);
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " p_subinventory " + //Added 05/03/2016 TMS Ticket 20151210-00014
                                               p_subinventory); //Added 05/03/2016 TMS Ticket 20151210-00014
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " p_doctype " + //Added 05/03/2016 TMS Ticket 20151210-00014
                                               p_doctype); //Added 05/03/2016 TMS Ticket 20151210-00014
            oraclecallablestatement = (OracleCallableStatement)con.prepareCall(mWCCycleCount);
            oraclecallablestatement.registerOutParameter(1, -10);
            oraclecallablestatement.setString(2, p_organization_id);
            oraclecallablestatement.setString(3, p_item);
            oraclecallablestatement.setString(4, p_cycle_count_header_id);
            oraclecallablestatement.setString(5, p_entry_status_code);
            oraclecallablestatement.setString(6, p_wc_count_list_sequence);
            oraclecallablestatement.setString(7, p_wc_locator);
            oraclecallablestatement.setString(8, p_subinventory); //Added 05/03/2016 TMS Ticket 20151210-00014
            oraclecallablestatement.setString(9, p_doctype); //Added 05/03/2016 TMS Ticket 20151210-00014
            oraclecallablestatement.execute();
            resultset = (ResultSet)oraclecallablestatement.getObject(1);
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " resultset " +
                                               resultset);

            do {
                gCallPoint = "do";
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);

                if (!resultset.next()) {
                    gCallPoint = "!resultset.next";
                    FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
                    break;
                }
                gCallPoint = "Getting resultset";
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
                
                String x_inventory_item_id  = resultset.getString(1);           //1
                String x_concatenated_segments = resultset.getString(2);        //2
                String x_primary_uom_code = resultset.getString(3);             //3
                String x_description  = resultset.getString(4);                 //4
                String x_lot_control_code = resultset.getString(5);             //5
                String x_serial_number_control_code = resultset.getString(6);   //6
                String x_shelf_life_days  = resultset.getString(7);             //7
                String x_min_minmax_quantity = resultset.getString(8);          //8
                String x_max_minmax_quantity = resultset.getString(9);          //9
                String x_sv = resultset.getString(10);                          //10
                String x_wc_count_list_sequence = resultset.getString(11);      //11
                String x_subinventory = resultset.getString(12);                //12
                String x_stock_locator = resultset.getString(13);               //13
                String x_wc_stock_locator = resultset.getString(14);            //14
                String x_inventory_location_id = resultset.getString(15);       //15
                String x_cycle_count_entry_id = resultset.getString(16);        //16
                String x_lot_number = resultset.getString(17);                  //17
                String x_count_list_sequence = resultset.getString(18);         //18
                String x_adjustment_amount = resultset.getString(19);           //19
                String x_cost_group_id  = resultset.getString(20);              //20
                String x_count_quantity = resultset.getString(21);              //21
                String x_due_date = resultset.getString(22);                    //22
                String x_prior_count_quantity = resultset.getString(23);        //23
                String x_snap_shot_quantity = resultset.getString(24);          //24
                
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " x_inventory_item_id " + x_inventory_item_id);
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " x_concatenated_segments " + x_concatenated_segments);
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " x_primary_uom_code " + x_primary_uom_code);
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " x_description " + x_description);
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " x_lot_control_code " + x_lot_control_code);
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " x_serial_number_control_code " + x_serial_number_control_code);
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " x_shelf_life_days " + x_shelf_life_days);
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " x_min_minmax_quantity " + x_min_minmax_quantity);
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " x_max_minmax_quantity " + x_max_minmax_quantity);
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " x_sv " + x_sv);
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " x_wc_count_list_sequence " + x_wc_count_list_sequence);
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " x_subinventory " + x_subinventory);
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " x_stock_locator " + x_stock_locator);
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " x_wc_stock_locator " + x_wc_stock_locator);
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " x_inventory_location_id " + x_inventory_location_id);
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " x_cycle_count_entry_id " + x_cycle_count_entry_id);
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " x_lot_number " + x_lot_number);
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " x_count_list_sequence " + x_count_list_sequence);
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " x_adjustment_amount " + x_adjustment_amount);
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " x_cost_group_id " + x_cost_group_id);
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " x_count_quantity " + x_count_quantity);
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " x_due_date " + x_due_date);
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " x_prior_count_quantity " + x_prior_count_quantity);
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " x_snap_shot_quantity " + x_snap_shot_quantity);
                
                currentRecord =
                        new XXWCCycleCount(x_inventory_item_id, x_concatenated_segments, x_primary_uom_code, x_description, x_lot_control_code, x_serial_number_control_code, x_shelf_life_days, x_min_minmax_quantity, x_max_minmax_quantity,
                                           x_sv, x_wc_count_list_sequence, x_subinventory, x_stock_locator, x_wc_stock_locator, x_inventory_location_id, x_cycle_count_entry_id, x_lot_number, x_count_list_sequence, x_adjustment_amount, x_cost_group_id, x_count_quantity, x_due_date, x_prior_count_quantity, x_snap_shot_quantity);
                vector.addElement(currentRecord);
            } while (true);
            gCallPoint = "while";
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
            if (resultset != null) {
                gCallPoint = "resultset.close";
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
                resultset.close();
            }
            if (oraclecallablestatement != null) {
                gCallPoint = "oraclecallablestatement is not null";
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint +
                                                   " oraclecallablestatement " + oraclecallablestatement);
                oraclecallablestatement.close();
            }
        } catch (Exception e) {
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " " + e);
        }
        gCallPoint = "Vector";
        FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " " + vector);
        return vector;
    }

}
