create or replace PACKAGE           XXWC_CC_MOBILE_PKG AS

   /*****************************************************************************************************************************************
   *   $Header XXWC_CC_MOBILE_PKG $                                                                                                         *
   *   Module Name: XXWC_CC_MOBILE_PKG                                                                                                      *
   *                                                                                                                                        *
   *   PURPOSE:   This package is used by the extensions in MSCA                                                                            *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        01-JUN-2015  Lee Spitzer               TMS Ticket 20150302-00173                                                          *
   *                                                     RF - Cycle Counting                                                                *
   *   1.1        17-MAR-2016  Lee Spitzer               TMS Ticket Number 20150810-00022 - RF - Cyclecount Research Report
   *   1.2        03-MAY-2016  Lee Spitzer               TMS Ticket Number 20151210-00014 - RF - PI Enhancements
   *****************************************************************************************************************************************/

  g_package                   VARCHAR2(30) := 'XXWC_CC_MOBILE_PKG';
  g_call_from                 VARCHAR2(175);
  g_call_point                VARCHAR2(175);
  g_distro_list               VARCHAR2 (80) := 'hdsoracledevelopers@hdsupply.com';
  g_module                    VARCHAR2 (80) := 'inv';
  g_sequence                  NUMBER;
  g_debug                     VARCHAR2(1) := nvl(FND_PROFILE.VALUE('AFLOG_ENABLED'),'N'); --Fnd Debug Log = Yes / No
  g_log_level                 NUMBER := nvl(FND_PROFILE.VALUE('AFLOG_LEVEL'),6); --Fnd Debug Log Level
  g_sqlerrm                   VARCHAR2(1000);
  g_sqlcode                   NUMBER;   
  g_message                   VARCHAR2(2000);
  g_exception                 EXCEPTION; 
  g_language                  VARCHAR2(10) := userenv('LANG');
  g_gtin_cross_ref_type VARCHAR2(25) := fnd_profile.VALUE('INV:GTIN_CROSS_REFERENCE_TYPE');
  g_gtin_code_length NUMBER := 14;
  TYPE t_genref IS REF CURSOR;
  
                              
   /*****************************************************************************************************************************************
   *   PROCEDURE GET_YES_NO_LOV                                                                                                             *
   *                                                                                                                                        *
   *   PURPOSE:  This procedure is used to get the Yes or No List of Values                                                                 *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        01-JUN-2015  Lee Spitzer               TMS Ticket 20150302-00173                                                          *
   *                                                     RF - Cycle Counting                                                                *
   *****************************************************************************************************************************************/
  
    PROCEDURE GET_YES_NO_LOV  ( x_default                   OUT    NOCOPY t_genref --0
                              ,  p_description               IN      VARCHAR2
                              );

   /*****************************************************************************************************************************************
   *   PROCEDURE GET_WC_COUNT_LIST_SEQUENCE_LOV                                                                                             *
   *                                                                                                                                        *
   *   PURPOSE:  This procedure is used to get the Count List Sequence List of Values                                                       *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        01-JUN-2015  Lee Spitzer               TMS Ticket 20150302-00173                                                          *
   *                                                     RF - Cycle Counting                                                                *
   *****************************************************************************************************************************************/
  
    PROCEDURE GET_WC_COUNT_LIST_SEQUENCE_LOV  ( x_default                   OUT    NOCOPY t_genref --0
                                              ,  p_organization_id          IN      VARCHAR2
                                              ,  p_cycle_count_header_id    IN      VARCHAR2
                                              ,  p_wc_count_list_sequence   IN      VARCHAR2
                                              ,  p_subinventory_code        IN      VARCHAR2
                                              ,  p_locator_id               IN      VARCHAR2
                                              ,  p_sort_by                  IN      VARCHAR2
                                              --,  p_inventory_item_id        IN      VARCHAR2
                                              );
                                              

   /*****************************************************************************************************************************************
   *   PROCEDURE GET_WC_SORT_BY_LOV                                                                                                         *
   *                                                                                                                                        *
   *   PURPOSE:  This procedure is used to get the Sort List of Values                                                                      *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        01-JUN-2015  Lee Spitzer               TMS Ticket 20150302-00173                                                          *
   *                                                     RF - Cycle Counting                                                                *
   *****************************************************************************************************************************************/
                                              
    PROCEDURE GET_WC_SORT_BY_LOV  ( x_default      OUT NOCOPY t_genref
                                  , p_column_number IN VARCHAR2
                                  );


   /*****************************************************************************************************************************************
   *   FUNCTION GET_DEFAULT_CC_HEADER_ID                                                                                                    *
   *                                                                                                                                        *
   *   PURPOSE:  This function is used to get the default cycle count header id for the organization                                        *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        01-JUN-2015  Lee Spitzer               TMS Ticket 20150302-00173                                                          *
   *                                                     RF - Cycle Counting                                                                *
   *****************************************************************************************************************************************/
                                              
    FUNCTION GET_DEFAULT_CC_HEADER_ID  ( p_organization_id IN NUMBER
                                       )
        RETURN NUMBER;
        
     /*****************************************************************************************************************************************
     *  PROCEDURE GET_WC_ITEM_LOV                                                                                                             *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure used for items and upc code                                                                                *
     *                                                                                                                                        *
     *    Parameters: None                                                                                                                    *
     *    Return : XXWC_LABEL_PRINTER profile                                                                                                 *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        01-JUN-2015  Lee Spitzer               TMS Ticket 20150302-00173                                                          *
     *                                                     RF - Cycle Counting                                                                *
     *   1.1        03-MAY-2016  Lee Spitzer               TMS Ticket Number 20151210-00014 - RF - PI Enhancements
     *****************************************************************************************************************************************/


    PROCEDURE GET_WC_ITEM_LOV ( x_item                    OUT    NOCOPY t_genref --0
                              , p_organization_id         IN     VARCHAR2
                              , p_item                    IN     VARCHAR2
                              , p_cycle_count_header_id   IN VARCHAR2
                              , p_entry_status_code       IN VARCHAR2
                              , p_wc_count_list_sequence  IN VARCHAR2
                              , p_wc_locator              IN VARCHAR2
                              , p_subinventory            IN VARCHAR2 --Added 05/03/2016 TMS Ticket 20151210-00014
                              , p_doctype                 IN VARCHAR2 --Added 05/03/2016 TMS Ticket 20151210-00014
                              );
   
   
   /*****************************************************************************************************************************************
   *   FUNCTION GET_SV_VALUE                                                                                                                *
   *                                                                                                                                        *
   *   PURPOSE:  This function is used to get the default cycle count header id for the organization                                        *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        01-JUN-2015  Lee Spitzer               TMS Ticket 20150302-00173                                                          *
   *                                                     RF - Cycle Counting                                                                *
   *****************************************************************************************************************************************/
                                              
    FUNCTION GET_SV_VALUE  ( p_organization_id IN NUMBER
                           , p_inventory_item_id IN NUMBER
                           )
        RETURN VARCHAR2;
        
        
   /*****************************************************************************************************************************************
   *   FUNCTION PROCESS_CC_ENTRIES_TBL                                                                                                      *
   *                                                                                                                                        *
   *   PURPOSE:  Process Cycle Entries Table - inserts and updates into XXWC.XXWC_CC_ENTRIES_TABLE                                          *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        01-JUN-2015  Lee Spitzer               TMS Ticket 20150302-00173                                                          *
   *                                                     RF - Cycle Counting                                                                *
   *****************************************************************************************************************************************/
                                              

        
    PROCEDURE PROCESS_CC_ENTRIES_TBL ( p_cycle_count_header_id  IN VARCHAR2 --1
                                     , p_cycle_count_entry_id   IN VARCHAR2 --2
                                     , p_wc_count_list_sequence IN VARCHAR2 --3
                                     , p_count_list_sequence    IN VARCHAR2 --4
                                     , p_organization_id        IN VARCHAR2 --5
                                     , p_inventory_item_id      IN VARCHAR2 --6
                                     , p_revision               IN VARCHAR2 --7
                                     , p_quantity               IN VARCHAR2 --8
                                     , p_primary_uom_code       IN VARCHAR2 --9
                                     , p_subinventory           IN VARCHAR2 --10
                                     , p_locator_id             IN VARCHAR2 --11
                                     , p_lot_number             IN VARCHAR2 --12
                                     , p_cost_group_id          IN VARCHAR2 --13
                                     , p_user_id                IN VARCHAR2 --14
                                     , x_return                 OUT NUMBER --15
                                     , x_message                OUT VARCHAR2 --16
                                     );
   

   
   /*****************************************************************************************************************************************
   *   FUNCTION GET_CC_VALUE                                                                                                                *
   *                                                                                                                                        *
   *   PURPOSE:  This function is used to get the default cycle count header id for the organization                                        *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        01-JUN-2015  Lee Spitzer               TMS Ticket 20150302-00173                                                          *
   *                                                     RF - Cycle Counting                                                                *
   *****************************************************************************************************************************************/
                                              
    FUNCTION GET_CC_VALUE  ( p_cycle_count_header_id  IN NUMBER --1
                           , p_cycle_count_entry_id   IN NUMBER --2
                           , p_wc_count_list_sequence IN NUMBER --3
                           , p_count_list_sequence    IN NUMBER --4
                           , p_organization_id IN NUMBER
                           , p_inventory_item_id IN NUMBER
                           )
        RETURN VARCHAR2;
        

   /*****************************************************************************************************************************************
   *   FUNCTION PROCESS_API                                                                                                                 *
   *                                                                                                                                        *
   *   PURPOSE:  This function is used to get the default cycle count header id for the organization                                        *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        01-JUN-2015  Lee Spitzer               TMS Ticket 20150302-00173                                                          *
   *                                                     RF - Cycle Counting                                                                *
   *****************************************************************************************************************************************/
    
    
    PROCEDURE PROCESS_API ( p_organization_id       IN VARCHAR2 --1
                          , p_cycle_count_header_id IN VARCHAR2 --2
                          , p_entry_status_code     IN VARCHAR2 --3
                          , p_user_id               IN VARCHAR2 --4
                          , x_return                OUT NUMBER --5
                          , x_message               OUT VARCHAR2); --6
                          
                          

   /*****************************************************************************************************************************************
   *   FUNCTION GET_SS_OH_QTY                                                                                                                *
   *                                                                                                                                        *
   *   PURPOSE:  This function is used to get the default cycle count header id for the organization                                        *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        01-JUN-2015  Lee Spitzer               TMS Ticket 20150302-00173                                                          *
   *                                                     RF - Cycle Counting                                                                *
   *****************************************************************************************************************************************/
                                              
    FUNCTION GET_SS_OH_QTY  ( p_cycle_count_header_id  IN NUMBER    --1
                            , p_cycle_count_entry_id   IN NUMBER    --2
                            , p_organization_id        IN NUMBER    --3
                            , p_inventory_item_id      IN NUMBER    --4
                            , p_subinventory_code      IN VARCHAR2  --5
                            , p_lot_number             IN VARCHAR2  --6
                           )
        RETURN NUMBER;
                        
                        
   /*****************************************************************************************************************************************
   *   FUNCTION GET_TOTAL_COUNT_QTY                                                                                                         *
   *                                                                                                                                        *
   *   PURPOSE:  This function is used to get the default cycle count header id for the organization                                        *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        01-JUN-2015  Lee Spitzer               TMS Ticket 20150302-00173                                                          *
   *                                                     RF - Cycle Counting                                                                *
   *****************************************************************************************************************************************/
                                              
    FUNCTION GET_TOTAL_COUNT_QTY  ( p_cycle_count_header_id  IN NUMBER    --1
                                  , p_cycle_count_entry_id   IN NUMBER    --2
                                  , p_organization_id        IN NUMBER    --3
                                  , p_inventory_item_id      IN NUMBER    --4
                                  , p_subinventory_code      IN VARCHAR2  --5
                                  , p_lot_number             IN VARCHAR2  --6
                                  )
        RETURN NUMBER;
        
        
    
   /*****************************************************************************************************************************************
   *   FUNCTION GET_OPEN_COUNT_QTY                                                                                                          *
   *                                                                                                                                        *
   *   PURPOSE:  This function is used to get the number of open counts                                                                     *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        01-JUN-2015  Lee Spitzer               TMS Ticket 20150302-00173                                                          *
   *                                                     RF - Cycle Counting                                                                *
   *****************************************************************************************************************************************/
                                              
    FUNCTION GET_OPEN_COUNT_QTY  ( p_cycle_count_header_id  IN NUMBER    --1
                                 , p_organization_id        IN NUMBER    --2
                                 , p_entry_status_code      IN NUMBER    --3)
                                 )
        RETURN NUMBER;

 /*****************************************************************************************************************************************
   *   PROCEDURE GET_WC_BIN_LOC                                                                                                             *
   *                                                                                                                                        *
   *   PURPOSE:   This procedure is used to get the branch bin assignments for an item organization combination                             *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        01-JUN-2015  Lee Spitzer               TMS Ticket 20150302-00152                                                          *
   *                                                     RF - Cycle Counting                                                                 *
   *****************************************************************************************************************************************/
  
    PROCEDURE GET_BIN_LOC           ( x_locators               OUT    NOCOPY t_genref --0
                                    , p_organization_id        IN     NUMBER    --1
                                    , p_subinventory_code      IN     VARCHAR2  --2
                                    , p_restrict_locators_code IN     NUMBER    --3
                                    , p_inventory_item_id      IN     VARCHAR2    --4
                                    , p_concatenated_segments  IN     VARCHAR2  --5
                                    , p_project_id             IN     NUMBER    --6
                                    , p_task_id                IN     NUMBER    --7
                                    , p_hide_prefix            IN     VARCHAR2  --8
                                    , p_cycle_count_header_id  IN     VARCHAR2  --9
                                    , p_entry_status_code      IN     VARCHAR2  --10
                                    --, p_alias                  IN     VARCHAR2
                                    );


   /*****************************************************************************************************************************************
   *   FUNCTION GET_COST_GROUP_ID                                                                                                           *
   *                                                                                                                                        *
   *   PURPOSE:  This function is used to get the number of open counts                                                                     *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        01-JUN-2015  Lee Spitzer               TMS Ticket 20150302-00173                                                          *
   *                                                     RF - Cycle Counting                                                                *
   *****************************************************************************************************************************************/
                                              
    FUNCTION GET_COST_GROUP_ID   ( p_organization_id        IN NUMBER    --1
                                 )
        RETURN NUMBER;
        
        
   /*****************************************************************************************************************************************
   *   FUNCTION GET_ITEM_OPEN_COUNT_QTY                                                                                                     *
   *                                                                                                                                        *
   *   PURPOSE:  This function is used to get the number of open counts                                                                     *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        01-JUN-2015  Lee Spitzer               TMS Ticket 20150302-00173                                                          *
   *                                                     RF - Cycle Counting                                                                *
   *****************************************************************************************************************************************/
                                              
    FUNCTION GET_ITEM_OPEN_COUNT_QTY  ( p_cycle_count_header_id  IN NUMBER    --1
                                      , p_organization_id        IN NUMBER    --2
                                      , p_inventory_item_id      IN NUMBER    --3)
                                      )
        RETURN NUMBER;
        
    /*****************************************************************************************************************************************
    *   PROCEDURE PROCESS_CC_RESEARCH_CCR                                                                                                    *
    *                                                                                                                                        *
    *   PURPOSE:   This procedure is used to launch the Cycle Count Research Report                                                          *
    *                                                                                                                                        *
    *                                                                                                                                        *
    *   REVISIONS:                                                                                                                           *
    *   Ver        Date        Author                     Description                                                                        *
    *   ---------  ----------  ---------------         -------------------------                                                             *
    *   1.0        17-MAR-2016  Lee Spitzer               TMS Ticket 20150810-00022  RF - Cyclecount Research Report                         *
    *                                                                                                                                        *
    *****************************************************************************************************************************************/
  
        PROCEDURE PROCESS_CC_RESEARCH_CCR ( p_organization_id       IN NUMBER
                                          , p_cycle_count_header_id IN NUMBER
                                          , x_return                OUT NUMBER
                                          , x_message               OUT VARCHAR2
                                          );
       
END XXWC_CC_MOBILE_PKG;
/