CREATE OR REPLACE PACKAGE APPS.XXWC_AR_CUST_MV_REFRESH_PKG
AS
   /*************************************************************************
   Copyright (c) 2012 HD Supply
   All rights reserved.
  **************************************************************************
    $Header XXWC_MD_SEARCH_REFRESH_PKG $
    Module Name: XXWC_MD_SEARCH_REFRESH_PKG
  
    PURPOSE:
  
    REVISIONS:
    -- VERSION DATE          AUTHOR(S)    DESCRIPTION
    -- ------- -----------   ------------ -------------------------
     1.0       08/30/2018   Rakesh Patel  TMS#20180828-00002-Sales Support Form Enhancement- EBS Objects
     **************************************************************************/

   PROCEDURE refresh_cust_mv (p_err_buf     OUT VARCHAR2,
                              p_return_code OUT NUMBER);

  
  
   PROCEDURE refresh_apex_mv_table (p_err_buf     OUT VARCHAR2,
                                    p_return_code OUT NUMBER);
 

   /*************************************************************************
     Procedure : main

     PURPOSE:   This procedure is the main procedure for refreshing ebs mv and refreshing apex_mv_table

     REVISIONS:
     Ver        Date        Author                  Description
     ---------  ----------  ---------------         -------------------------
          1.0       08/30/2018   Rakesh Patel  TMS#20180828-00002-Sales Support Form Enhancement- EBS Objects
   ************************************************************************/
   PROCEDURE MAIN (p_err_buf              OUT VARCHAR2,
                   p_return_code          OUT NUMBER,
                   p_refresh_cust_mv       IN VARCHAR2 DEFAULT 'N',
                   p_refresh_apex_mv_tbl   IN VARCHAR2 DEFAULT 'Y'  
                   );

END XXWC_AR_CUST_MV_REFRESH_PKG;
/