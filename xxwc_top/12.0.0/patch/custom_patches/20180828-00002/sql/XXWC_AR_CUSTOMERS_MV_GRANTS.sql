/*************************************************************************
  PURPOSE:   Grants provided to required objects to EA_APEX/INTERFACE_APEXWC user

  REVISIONS:
  Ver        Date        Author               Description
  ---------  ----------  ---------------      -------------------------
  1.0        08/28/2018  Rakesh Patel         TMS#20180828-00002-Sales Support Form Enhancement- EBS Objects
**************************************************************************/

GRANT SELECT ON APPS.XXWC_AR_CUSTOMERS_MV TO INTERFACE_APEXWC;

GRANT SELECT ON APPS.hr_operating_units TO INTERFACE_APEXWC;

GRANT SELECT ON APPS.hz_parties TO INTERFACE_APEXWC;

GRANT SELECT ON APPS.hz_cust_accounts TO INTERFACE_APEXWC;

GRANT SELECT ON APPS.hz_locations TO INTERFACE_APEXWC;

GRANT SELECT ON APPS.hz_party_sites TO INTERFACE_APEXWC;

GRANT SELECT ON APPS.hz_cust_acct_sites_all TO INTERFACE_APEXWC;

GRANT SELECT ON APPS.hz_cust_site_uses_all TO INTERFACE_APEXWC;

GRANT SELECT ON APPS.jtf_rs_salesreps TO INTERFACE_APEXWC;

GRANT SELECT ON APPS.xxwc_ar_payment_terms_vw TO INTERFACE_APEXWC;

GRANT SELECT ON APPS.hz_cust_profile_amts TO INTERFACE_APEXWC;

GRANT SELECT ON APPS.hz_customer_profiles TO INTERFACE_APEXWC;

GRANT SELECT ON APPS.fnd_lookup_values TO INTERFACE_APEXWC;

GRANT SELECT ON APPS.jtf_rs_resource_extns TO INTERFACE_APEXWC;

GRANT SELECT ON APPS.XXWC_AR_CUSTOMERS_MV TO EA_APEX;

GRANT SELECT ON APPS.hr_operating_units TO EA_APEX;

GRANT SELECT ON APPS.hz_parties TO EA_APEX;

GRANT SELECT ON APPS.hz_cust_accounts TO EA_APEX;

GRANT SELECT ON APPS.hz_locations TO EA_APEX;

GRANT SELECT ON APPS.hz_party_sites TO EA_APEX;

GRANT SELECT ON APPS.hz_cust_acct_sites_all TO EA_APEX;

GRANT SELECT ON APPS.hz_cust_site_uses_all TO EA_APEX;

GRANT SELECT ON APPS.jtf_rs_salesreps TO EA_APEX;

GRANT SELECT ON APPS.xxwc_ar_payment_terms_vw TO EA_APEX;

GRANT SELECT ON APPS.hz_cust_profile_amts TO EA_APEX;

GRANT SELECT ON APPS.hz_customer_profiles TO EA_APEX;

GRANT SELECT ON APPS.fnd_lookup_values TO EA_APEX;

GRANT SELECT ON APPS.jtf_rs_resource_extns TO EA_APEX;





