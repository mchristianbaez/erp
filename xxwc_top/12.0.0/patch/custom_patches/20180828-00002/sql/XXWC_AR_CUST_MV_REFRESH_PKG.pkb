CREATE OR REPLACE PACKAGE BODY APPS.XXWC_AR_CUST_MV_REFRESH_PKG
AS
    /*************************************************************************
    Copyright (c) 2012 HD Supply
    All rights reserved.
  **************************************************************************
    $Header XXWC_AR_CUST_MV_REFRESH_PKG $
    Module Name: XXWC_AR_CUST_MV_REFRESH_PKG
  
    PURPOSE:
  
    REVISIONS:
    -- VERSION DATE          AUTHOR(S)    DESCRIPTION
    -- ------- -----------   ------------ -------------------------
     1.0       08/30/2018   Rakesh Patel  TMS#20180828-00002-Sales Support Form Enhancement- EBS Objects
     **************************************************************************/

   /*************************************************************************
        Procedure : refresh_ais_mv

        PURPOSE:   This procedure is to refresh AIS XXWC_MD_SEARCH_PRODUCTS_MV and its Indexes

        REVISIONS:
        Ver        Date        Author                  Description
        ---------  ----------  ---------------         -------------------------
        1.0       08/30/2018   Rakesh Patel            TMS#20180828-00002-Sales Support Form Enhancement- EBS Objects
   ************************************************************************/
   PROCEDURE refresh_cust_mv (p_err_buf       OUT VARCHAR2,
                             p_return_code   OUT NUMBER)
   IS
      l_sec          VARCHAR2 (100);
   BEGIN
      l_sec := 'Start APPS.XXWC_AR_CUSTOMER_MV refresh';
      fnd_file.put_line (
               fnd_file.LOG,
               l_sec || ' - ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));
               
      DBMS_SNAPSHOT.REFRESH(
        LIST                 => 'APPS.XXWC_AR_CUSTOMERS_MV'
       ,PUSH_DEFERRED_RPC    => TRUE
       ,REFRESH_AFTER_ERRORS => FALSE
       ,PURGE_OPTION         => 1
       ,PARALLELISM          => 0
       ,ATOMIC_REFRESH       => TRUE
       ,NESTED               => FALSE
    );

      l_sec := 'End APPS.XXWC_AR_CUSTOMER_MV refresh';
      
      fnd_file.put_line (
               fnd_file.LOG,
               l_sec || ' - ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));
               
       p_err_buf     := NULL;
       p_return_code := 0;
    
   EXCEPTION
   WHEN OTHERS THEN
         p_err_buf := SQLERRM || l_sec;
         p_return_code := 2;
   END refresh_cust_mv;

  /*************************************************************************
     Procedure : refresh_apex_mv_table

     PURPOSE:   This procedure is used for refreshing apex_mv_table

     REVISIONS:
     Ver        Date        Author                  Description
     ---------  ----------  ---------------         -------------------------
     1.0       08/30/2018   Rakesh Patel  TMS#20180828-00002-Sales Support Form Enhancement- EBS Objects
   ************************************************************************/
   PROCEDURE refresh_apex_mv_table (p_err_buf       OUT VARCHAR2,
                                    p_return_code   OUT NUMBER)
   IS
      l_sec          VARCHAR2 (100);
      l_record_count NUMBER := 0;
      TYPE t_bulk_collect_ar_cust_tab IS TABLE OF interface_apexwc.XXWC_AR_CUSTOMERS_MV@apexwc.HSI.HUGHESSUPPLY.COM%ROWTYPE;

      l_tab    t_bulk_collect_ar_cust_tab;

          CURSOR c_data IS
             SELECT  BUSINESS_UNIT,
                     SOURCE_SYSTEM,
                     CUST_UNIQUE_IDENTIFIER,
                     CUST_ACCOUNT_CODE,
                     CUST_ACCOUNT_NAME,
                     CUST_ACOUNT_TYPE,
                     CUST_ACCOUNT_CLASS,
                     CUST_ROLLUP_LEVEL1_LABEL,
                     CUST_ROLLUP_LEVEL1_CODE,
                     CUST_ROLLUP_LEVEL1_DESC,
                     CUST_ROLLUP_LEVEL2_LABEL,
                     CUST_ROLLUP_LEVEL2_CODE,
                     CUST_ROLLUP_LEVEL2_DESC,
                     CUST_ROLLUP_LEVEL3_LABEL,
                     CUST_ROLLUP_LEVEL3_CODE,
                     CUST_ROLLUP_LEVEL3_VALUE,
                     REPORTING_GROUP_IDENTIFIER,
                     HOMELOCATION,
                     IN_FREIGHT_EXEMPT_FLAG,
                     OUT_FREIGHT_EXEMPT_FLAG,
                     PASSTHROUGH_DISC_ELIGIBLE_FLAG,
                     REBATEPERCENTAGE,
                     PAYMENT_TERMS_DESC,
                     PAYMENT_TERM_DISC_PERC,
                     CREDIT_LIMIT_AMT,
                     TAXID,
                     CREATION_DATE,
                     LAST_UPDATE_DATE,
                     FIRSTSALEDATE,
                     LASTSALEDATE,
                     CUST_ACCOUNT_STATUS,
                     PARTY_SITE_NUMBER,
                     PARTY_SITE_ID,
                     PARTY_ID,
                     SITE_USE_ID,
                     SITE_USE_CODE,
                     SITE_NAME,
                     ADDRESS1,
                     ADDRESS2,
                     ADDRESS3,
                     ADDRESS4,
                     CITY,
                     STATE_PROVINCE,
                     POSTAL_CODE,
                     COUNTRY,
                     MAIN_PHONE_NUMBER,
                     MAIN_FAX_NUMBER,
                     MAIN_EMAIL,
                     INSIDE_SALESREP_ID,
                     OUTSIDE_SALESREP_ID,
                     DEFAULT_BILL_TO_CUSTOMER,
                     DEFAULT_BILL_TO_PARTY_SITE_NUM,
                     DEFAULT_BILL_TO_SITE_NAME,
                     BILL_TO_ADDRESS1,
                     BILL_TO_ADDRESS2,
                     BILL_TO_ADDRESS3,
                     BILL_TO_ADDRESS4,
                     BILL_TO_CITY,
                     BILL_TO_STATE_PROVINCE,
                     BILL_TO_POSTAL_CODE,
                     BILL_TO_COUNTRY,
                     DEF_BILL_TO_TELEPHONE,
                     DEF_BILL_TO_FAX,
                     CUSTOM_CUST_ATTR1_LABEL,
                     CUSTOM_CUST_ATTR1_VALUE,
                     CUSTOM_CUST_ATTR2_LABEL,
                     CUSTOM_CUST_ATTR2_VALUE,
                     CUSTOM_CUST_ATTR3_LABEL,
                     CUSTOM_CUST_ATTR3_VALUE,
                     CUSTOM_CUST_ATTR4_LABEL,
                     CUSTOM_CUST_ATTR4_VALUE,
                     CUSTOM_CUST_ATTR5_LABEL,
                     CUSTOM_CUST_ATTR5_VALUE,
                     DATE_DELETED,
                     OPERATING_UNIT_ID,
                     OPERATING_UNIT,
                     CUSTOMER_NAME,
                     SITE_USE_LOCATION_NAME,
                     PRISM_CUSTOMER_NUMBER,
                     PRISM_SITE_NUMBER,
                     HCSUA_PRIMARY_FLAG,
                     HCASA_SITE_STATUS,
                     HP_PARTY_NUMBER,
                     HP_PARTY_STATUS,
                     HP_PARTY_ADDRESS1,
                     HP_PARTY_ADDRESS2,
                     HP_PARTY_ADDRESS3,
                     HP_PARTY_ADDRESS4,
                     HP_PARTY_CITY,
                     HP_PARTY_COUNTY_PROVINCE,
                     HP_PARTY_STATE,
                     HP_PARTY_COUNTRY,
                     HP_PARTY_POSTAL_CODE,
                     HCA_LAST_UPDATE_DATE,
                     HCASA_LAST_UPDATE_DATE,
                     HCSUA_LAST_UPDATE_DATE,
                     HP_LAST_UPDATE_DATE,
                     HPS_LAST_UPDATE_DATE,
                     HL_LAST_UPDATE_DATE,
                     HCASA_CUST_ACCT_SITE_ID,
                     SITE_USE_TYPE,
                     ACCOUNT_ESTABLISHED_DATE,
                     TAX_EXEMPT_CERT_NUM,
                     TAX_EXEMPT_CERT_EXP_DATE,
                     TAX_LOCALITY,
                     PRICING_PROFIILE,
                     NAICS_CODE,
                     DODGE_NUMBER,
                     CUST_ACCOUNT_ID,
                     (SELECT salesrep_id
                        FROM apps.jtf_rs_salesreps
                       WHERE salesrep_number = xacm.inside_salesrep_id
                         AND rownum=1) salesrep_id
                     ,(cust_account_code || ' - ' || cust_account_name || ' ' ||address1 || ' , ' || city || ' , ' || state_province || ' - ' ||postal_code || ' ' || ' - ' || 
                                                     (SELECT jrre.source_name
                                                       FROM jtf_rs_salesreps           jrs
                                                          , jtf_rs_resource_extns      jrre
                                                      WHERE 1 = 1
                                                        AND jrs.resource_id          = jrre.resource_id
                                                        AND jrs.salesrep_number      = xacm.inside_salesrep_id
                                                        AND rownum=1 ))    
      FROM APPS.XXWC_AR_CUSTOMERS_MV xacm
	 WHERE xacm.cust_account_status <> 'Inactive' --active customers
       AND xacm.site_use_code = 'BILL_TO' --bill to side
       AND xacm.hcsua_primary_flag = 'Y'
       AND xacm.hcasa_site_status = 'A'--primary site
       AND xacm.hp_party_status = 'A';
       
   BEGIN
     l_sec := 'Start refresh_apex_mv_table';
   
     fnd_file.put_line (
         fnd_file.LOG,
         l_sec || ' - ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));
         
      DELETE FROM interface_apexwc.XXWC_AR_CUSTOMERS_MV@apexwc.HSI.HUGHESSUPPLY.COM;
 
      OPEN c_data;
      LOOP
         FETCH c_data
         BULK COLLECT INTO l_tab LIMIT 10000;
    
         FOR i IN l_tab.FIRST..l_tab.LAST LOOP
           INSERT INTO interface_apexwc.XXWC_AR_CUSTOMERS_MV@apexwc.HSI.HUGHESSUPPLY.COM
             VALUES l_tab(i);
         END LOOP;
     
         COMMIT;

         l_sec := 'Start l_tab.count rows: ' || l_tab.count;
    
         fnd_file.put_line (
           fnd_file.LOG,
           l_sec || ' - ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));
         
         l_record_count := l_record_count + l_tab.count;
         l_tab.delete;
         EXIT WHEN c_data%NOTFOUND;
         END LOOP;
      CLOSE c_data;

    
      l_sec := 'Total records inserted into WC_APPS.XXWC_AR_CUSTOMERS_MV: ' || l_record_count;
    
         fnd_file.put_line (
           fnd_file.LOG,
           l_sec || ' - ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));

       p_err_buf     := NULL;
       p_return_code := 0;
   EXCEPTION
      WHEN OTHERS
      THEN
         p_err_buf := SQLERRM || l_sec;
         p_return_code := 2;
   END refresh_apex_mv_table;

   /*************************************************************************
     Procedure : main

     PURPOSE:   This procedure is the main procedure for refreshing ebs mv and refreshing apex_mv_table

     REVISIONS:
     Ver        Date        Author                  Description
     ---------  ----------  ---------------         -------------------------
          1.0       08/30/2018   Rakesh Patel  TMS#20180828-00002-Sales Support Form Enhancement- EBS Objects
   ************************************************************************/
   PROCEDURE MAIN (p_err_buf              OUT VARCHAR2,
                   p_return_code          OUT NUMBER,
                   p_refresh_cust_mv       IN VARCHAR2 DEFAULT 'N',
                   p_refresh_apex_mv_tbl   IN VARCHAR2 DEFAULT 'Y'  
                   )
   IS
      l_exception     EXCEPTION;
      l_err_buf       VARCHAR2 (2000);
      l_return_code   NUMBER;
   BEGIN
      fnd_file.put_line (fnd_file.LOG, 'p_refresh_cust_mv - ' || p_refresh_cust_mv);
      fnd_file.put_line (fnd_file.LOG, 'p_refresh_apex_mv_tbl - ' || p_refresh_apex_mv_tbl);

      IF p_refresh_cust_mv = 'Y'
      THEN
         refresh_cust_mv (l_err_buf, l_return_code);
      END IF;

      IF p_refresh_apex_mv_tbl = 'Y'
      THEN
         refresh_apex_mv_table (l_err_buf, l_return_code);
      END IF;
      
      IF l_return_code = 2 
      THEN
         RAISE l_exception;
      END IF;

      fnd_file.put_line (
         fnd_file.LOG,
         'End MAIN ' || ' - ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));
   EXCEPTION
      WHEN l_exception
      THEN
         p_err_buf := l_err_buf;
         p_return_code := 2;
      WHEN OTHERS
      THEN
         p_err_buf := SQLERRM;
         p_return_code := 2;
   END main;
END XXWC_AR_CUST_MV_REFRESH_PKG;
/