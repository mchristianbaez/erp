CREATE OR REPLACE VIEW APPS.XXCUSAPEX_P2P_PR_PO_VW 
/******************************************************************************
   NAME:       APPS.XXCUSAPEX_P2P_PR_PO_VW 
   PURPOSE:  This view is used to extract PO information from iprocurement

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        4/07/2015    Pahwa Nancy   ESMS-RFC # 42130 New View 
   1.1        2/2/2017      Bala Seshadri    TMS 20170202-00150 / ESMS 479451
******************************************************************************/
AS
     SELECT DISTINCT
            pha.segment1 po_number,
            POS_TOTALS_PO_SV.get_po_archive_total (pha.PO_HEADER_ID,
                                                   pha.REVISION_NUM,
                                                   pha.TYPE_LOOKUP_CODE)
               po_total                                               --, rd.*
                       --         , pla.quantity
                       -- , rd.REQ_LINE_QUANTITY
                       -- , pla.unit_price
            ,
            pha.creation_date,
            pha.po_header_id                         -- , pla.item_description
                            ,
            (rd.req_line_quantity * prla.unit_price) pr_total,
            pda.po_distribution_id,
            rd.code_combination_id,
            prha.requisition_header_id,
            plla.amount_billed,
            pha.vendor_id,
            pha.vendor_site_id,
            pla.item_description,
            prla.requisition_line_id,
            pha.authorization_status
       FROM po_headers_all pha,
            po_lines_all pla,
            po_requisition_headers_all prha,
            po_requisition_lines_all prla,
            po_distributions_all pda,
            po_req_distributions_all rd,
            PO_LINE_LOCATIONS_ALL PLLA
WHERE 1 =1 --Ver 1.1
       AND pha.po_header_id = pla.po_header_id
       AND pha.po_header_id = pda.po_header_id
       AND pla.po_line_id = pda.po_line_id
       AND rd.distribution_id(+) = pda.req_distribution_id        --Ver 1.1
       AND prla.requisition_line_id(+) = rd.requisition_line_id --Ver 1.1
       AND prha.requisition_header_id(+) = prla.requisition_header_id --Ver 1.1
       AND plla.po_line_id = pla.po_line_id
       AND plla.po_header_id = pha.po_header_id
       AND pha.org_id = 163
--order by po_number --Ver 1.1
;
--
GRANT SELECT ON APPS.XXCUSAPEX_P2P_PR_PO_VW TO INTERFACE_P2P -- Ver 1.1
;
--
COMMENT ON TABLE APPS.XXCUSAPEX_P2P_PR_PO_VW IS 'TMS 20170202-00150 / ESMS 479451' --Ver 1.1
;
--