/*************************************************************************
 Copyright (c) 2012 HD Supply
 All rights reserved.
**************************************************************************
   $Header APPS.XXWC_HZ_GEOGRAPHY_RANGES_N1 $
  Module Name: APPS.XXWC_HZ_GEOGRAPHY_RANGES_N1

  PURPOSE: Research Large order booking action delay issue

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)        TMS TASK      DESCRIPTION
  -- ------- -----------   ------------     ---------     --------------------
  -- 1.0     07-FEB-2017   Pattabhi Avula                 Initially Created 
                                                          TMS# 20170124-00193
**************************************************************************/

DROP INDEX APPS.XXWC_HZ_GEOGRAPHY_RANGES_N1;

CREATE INDEX APPS.XXWC_HZ_GEOGRAPHY_RANGES_N1 ON AR.HZ_GEOGRAPHY_RANGES
(GEOGRAPHY_ID, GEOGRAPHY_FROM, GEOGRAPHY_TO, START_DATE, END_DATE)
LOGGING
TABLESPACE APPS_TS_TX_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             128K
            MAXSIZE          UNLIMITED
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
NOPARALLEL;

DROP INDEX APPS.XXWC_HZ_GEOGRAPHY_RANGES_N2;

CREATE INDEX APPS.XXWC_HZ_GEOGRAPHY_RANGES_N2 ON AR.HZ_GEOGRAPHY_RANGES
(GEOGRAPHY_ID, GEOGRAPHY_TYPE, GEOGRAPHY_USE)
LOGGING
TABLESPACE APPS_TS_TX_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             128K
            MAXSIZE          UNLIMITED
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
NOPARALLEL
/