-- ******************************************************************************
-- *  Copyright (c) 2011 Lucidity Consulting Group
-- *  All rights reserved.
-- ***************************************************************************************************
-- *   $Header XXWC_AR_DM_STG_TBL_SQLLDR.ctl $
-- *   Module Name: XXWC_AR_DM_STG_TBL_SQLLDR.ctl
-- *
-- *   PURPOSE:   This is the control file for loading the staging table XXWC.XXWCAR_CASH_RCPTS_TBL
-- *
-- *   REVISIONS:
-- *   Ver        Date        Author                     Description
-- *   ---------  ----------  ---------------         ---------------------------------------------------
-- *   1.0        27/04/2018  Ashwin Sridhar             Initial Version
-- * ****************************************************************************************************
OPTIONS (SKIP=0)
LOAD DATA
APPEND
INTO TABLE XXWC.XXWCAR_CASH_RCPTS_TBL
FIELDS TERMINATED BY "|" OPTIONALLY ENCLOSED BY '"' 
TRAILING NULLCOLS
( 
DEPOSIT_DATE           "TO_DATE(:DEPOSIT_DATE,'mm/dd/RR')"       
,OPERATOR_CODE          "TRIM(:OPERATOR_CODE)"         
,CHECK_NBR              "TRIM(:CHECK_NBR)"         
,CHECK_AMT              "TRIM(:CHECK_AMT)"         
,URL_LINK               "TRIM(:URL_LINK)"         
,CUSTOMER_NBR           "TRIM(:CUSTOMER_NBR)"         
,INVOICE_NBR            "TRIM(:INVOICE_NBR)"         
,FILL                   "TRIM(:FILL)"         
,INVOICE_AMT            "TRIM(:INVOICE_AMT)"         
,DISC_AMT               "TRIM(:DISC_AMT)"         
,GL_ACCT_NBR            "TRIM(:GL_ACCT_NBR)"        
,SHORT_PAY_CODE         "TRIM(:SHORT_PAY_CODE)"       
,BILL_TO_LOCATION       "TRIM(:BILL_TO_LOCATION)"     
,COMMENTS               "TRIM(:COMMENTS)"     
,SHORT_CODE_INTERFACED  "TRIM(:SHORT_CODE_INTERFACED)"     
,SHORT_CODE_INT_DATE    "TO_DATE(:SHORT_CODE_INT_DATE,'MM/DD/RR')"    
,RECEIPT_TYPE           CONSTANT "DEBIT"
,ORACLE_ACCOUNT_NBR     "TRIM(:ORACLE_ACCOUNT_NBR)"     
,CUST_ACCOUNT_ID        "TRIM(:CUST_ACCOUNT_ID)"     
,ORACLE_FLAG            "TRIM(:ORACLE_FLAG)"     
,RECEIPT_ID             "TRIM(:RECEIPT_ID)"     
,ABA_NUMBER             "TRIM(:ABA_NUMBER)"    
,BANK_ACCOUNT_NUM       "TRIM(:BANK_ACCOUNT_NUM)"    
,STATUS                 CONSTANT "NEW"  
,CONTROL_NBR            "APPS.XXWC_AR_INV_STG_TBL_SEQ.NEXTVAL"
,CREATION_DATE          SYSDATE       
,UPDATED_DATE           SYSDATE  
)