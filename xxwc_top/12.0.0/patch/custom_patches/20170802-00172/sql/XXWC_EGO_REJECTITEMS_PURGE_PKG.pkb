CREATE OR REPLACE PACKAGE BODY APPS.XXWC_EGO_REJECTITEMS_PURGE_PKG
IS
   /******************************************************************************************************
   -- File Name: XXWC_REJECTED_ITEMS_PURGE_PKG.pkb
   --
   -- PROGRAM TYPE: PL/SQL Script   <API>
   --
   -- PURPOSE:
   -- HISTORY
   -- =====================================================================================================
   -- =====================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- ---------------------------------------------------------------
   -- 1.0     12-Jun-2015   P.Vamshidhar    TMS#20150608-00031  - Single Item UI Enhancements
                                            Initial version.  Only rejected items will purged.
   -- 1.1     02-Aug-2017   P.Vamshidhar    TMS# 20170802-00172 - EGO: Item deletion from Staging Table - fix
    *******************************************************************************************************/

   g_err_callfrom   VARCHAR2 (100) := 'XXWC_REJECTED_ITEMS_PURGE_PKG';
   g_distro_list    VARCHAR2 (100) := 'HDSOracleDevelopers@hdsupply.com';

   PROCEDURE Main (x_errbuf           OUT VARCHAR2,
                   x_retcode          OUT NUMBER,
                   p_item_number   IN     VARCHAR2,
                   p_noof_days     IN     NUMBER)
   IS
      CURSOR cur_days (
         p_noof_days1   IN NUMBER)
      IS
         SELECT xmig.segment1 Item_number,
                ec.change_notice,
                ec.change_id,
                ec.change_mgmt_type_code,
                xmig.inventory_item_id
           FROM APPS.ENG_ENGINEERING_CHANGES ec,
                APPS.ENG_CHANGE_ROUTES ecr,
                XXWC.XXWC_MTL_SY_ITEMS_CHG_B xmig
          WHERE     ec.change_id = ecr.object_id1
                AND ecr.status_code = 'REJECTED'
                AND ec.change_id = xmig.change_id
                AND TRUNC (ecr.last_update_date) <=
                       TRUNC (SYSDATE) - p_noof_days1;


      l_noof_days       NUMBER;
      l_sec             VARCHAR2 (2000);
      l_procedure       VARCHAR2 (100) := 'Main';
      l_err_msg         VARCHAR2 (2000);
      lvc_status        VARCHAR2 (40);
      l_change_id       NUMBER;
      l_change_notice   ENG_ENGINEERING_CHANGES.CHANGE_NOTICE%TYPE;
      PRM_ERROR         EXCEPTION;
      l_change_type     ENG_ENGINEERING_CHANGES.change_mgmt_type_code%TYPE;
      l_item_id         MTL_SYSTEM_ITEMS_B.INVENTORY_ITEM_ID%TYPE;
   BEGIN
      FND_FILE.PUT_LINE (
         FND_FILE.OUTPUT,
         '                            Rejected Items Purging Execution Report                         ');
      FND_FILE.PUT_LINE (FND_FILE.OUTPUT, '   ');
      FND_FILE.PUT_LINE (
         FND_FILE.OUTPUT,
         '--------------------------------------------------------------------------------------------');
      FND_FILE.PUT_LINE (
         FND_FILE.OUTPUT,
         '|    Chage Number          |          Change Type                |         Item Number     |');
      FND_FILE.PUT_LINE (
         FND_FILE.OUTPUT,
         '--------------------------------------------------------------------------------------------');


      FND_FILE.PUT_LINE (FND_FILE.LOG, 'Procedure start - Main');

      l_sec := ' Start Purging Item Data ';

      IF p_item_number IS NOT NULL AND p_noof_days IS NOT NULL
      THEN
         l_sec := ' Item Number Purging';

         --Added begin and end clause in 1.1  
         BEGIN
            SELECT ec.change_notice,
                   ec.change_id,
                   ec.change_mgmt_type_code,
                   inventory_item_id
              INTO l_change_notice,
                   l_change_id,
                   l_change_type,
                   l_item_id
              FROM APPS.ENG_ENGINEERING_CHANGES ec,
                   APPS.ENG_CHANGE_ROUTES ecr,
                   XXWC.XXWC_MTL_SY_ITEMS_CHG_B xmig
             WHERE     ec.change_id = ecr.object_id1
                   AND ecr.status_code = 'REJECTED'
                   AND ec.change_id = xmig.change_id
                   AND xmig.segment1 = p_item_number
                   AND ROWNUM = 1;   -- Added in rev 1.1
         EXCEPTION
            WHEN OTHERS
            THEN
               RAISE PRM_ERROR;
               x_retcode := 2;
         END;

         IF l_change_id IS NOT NULL
         THEN
            l_sec := ' Calling Purge process';

            lvc_status :=
               xxwc_delete_item (p_item_number,
                                 l_change_notice,
                                 l_change_id,
                                 l_change_type,
                                 l_item_id);
         END IF;
      END IF;

      IF lvc_status <> 'SUCCESS'
      THEN
         RAISE PRM_ERROR;
      END IF;


      IF p_item_number IS NULL AND p_noof_days IS NOT NULL
      THEN
         l_sec := ' Item Number Purging (Number of days)';

         FOR rec_days IN cur_days (p_noof_days)
         LOOP
            l_sec := ' Calling Purge process';

            FND_FILE.PUT_LINE (
               FND_FILE.LOG,
                  rec_days.item_number
               || ' Item Number data Deletion process Started');


            lvc_status :=
               xxwc_delete_item (rec_days.item_number,
                                 rec_days.change_notice,
                                 rec_days.change_id,
                                 rec_days.change_mgmt_type_code,
                                 rec_days.inventory_item_id);

            FND_FILE.PUT_LINE (
               FND_FILE.LOG,
                  rec_days.item_number
               || ' Item Number data Deletion process Completed');
         END LOOP;

         FND_FILE.PUT_LINE (
            FND_FILE.OUTPUT,
            '--------------------------------------------------------------------------------------------');

         FND_FILE.PUT_LINE (FND_FILE.LOG, 'lvc_status' || lvc_status);

         IF lvc_status <> 'SUCCESS'
         THEN
            RAISE PRM_ERROR;
            x_retcode := 2;
         END IF;
      END IF;

      FND_FILE.PUT_LINE (
         FND_FILE.OUTPUT,
         '--------------------------------------------------------------------------------------------');
   EXCEPTION
      WHEN PRM_ERROR
      THEN
         l_err_msg :=
               'Item Number:'
            || p_item_number
            || ' Error:'
            || SUBSTR (SQLERRM, 1, 1000);
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || l_procedure,
            p_calling             => l_sec,
            p_request_id          => FND_GLOBAL.CONC_REQUEST_ID,
            p_ora_error_msg       => SUBSTR (SQLERRM, 1, 1000),
            p_error_desc          => SUBSTR (l_err_msg, 1, 240),
            p_distribution_list   => g_distro_list,
            p_module              => 'XXWC');

         COMMIT;
      WHEN OTHERS
      THEN
         x_retcode := 2;
         l_err_msg :=
               'Item Number:'
            || p_item_number
            || ' Error:'
            || SUBSTR (SQLERRM, 1, 1000);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || l_procedure,
            p_calling             => l_sec,
            p_request_id          => FND_GLOBAL.CONC_REQUEST_ID,
            p_ora_error_msg       => SUBSTR (SQLERRM, 1, 1000),
            p_error_desc          => SUBSTR (l_err_msg, 1, 240),
            p_distribution_list   => g_distro_list,
            p_module              => 'XXWC');
         COMMIT;
   END;

   FUNCTION xxwc_delete_item (p_item_number     IN VARCHAR2,
                              p_change_notice   IN VARCHAR2,
                              p_change_id       IN NUMBER,
                              p_change_type     IN VARCHAR2,
                              p_item_id         IN NUMBER)
      RETURN VARCHAR2
   IS
      ln_result         VARCHAR2 (100);
      l_procedure       VARCHAR2 (100) := 'xxwc_delete_item';
      l_err_msg         VARCHAR2 (2000);
      l_sec             VARCHAR2 (1000);
      l_change_notice   ENG_ENGINEERING_CHANGES.CHANGE_NOTICE%TYPE;
      l_change_type     ENG_ENGINEERING_CHANGES.change_mgmt_type_code%TYPE;
      l_change_id       ENG_ENGINEERING_CHANGES.CHANGE_ID%TYPE;
   BEGIN
      FND_FILE.PUT_LINE (
         FND_FILE.LOG,
         'change request ' || p_change_id || ' change type ' || p_change_type);

      IF p_change_type = 'CHANGE_REQUEST'
      THEN
         l_sec := 'Item_delete_std_tab';

         ln_result := apps.XXWC_PIM_CHG_ITEM_MGMT.delete_item (p_item_number);
         COMMIT;
      END IF;

      l_sec := 'Item_delete_custom_tab';
      XXWC_EGO_ITEM_BE.purge_temp_data (p_change_id,
                                        p_item_id,
                                        p_change_type);

      COMMIT;
      FND_FILE.PUT_LINE (
         FND_FILE.OUTPUT,
            '| '
         || RPAD (p_change_notice, 15, ' ')
         || '          | '
         || RPAD (p_change_type, 25, ' ')
         || '           | '
         || RPAD (p_item_number, 20, ' ')
         || '    |');

      RETURN 'SUCCESS';
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         l_err_msg :=
               'Item Number:'
            || p_item_number
            || ' Error:'
            || SUBSTR (SQLERRM, 1, 1000);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || l_procedure,
            p_calling             => l_sec,
            p_request_id          => FND_GLOBAL.CONC_REQUEST_ID,
            p_ora_error_msg       => SUBSTR (SQLERRM, 1, 1000),
            p_error_desc          => SUBSTR (l_err_msg, 1, 240),
            p_distribution_list   => g_distro_list,
            p_module              => 'XXWC');
         COMMIT;
         RETURN 'NOT SUCCESS';
   END;
END XXWC_EGO_REJECTITEMS_PURGE_PKG;
/
SHOW ERRORS;