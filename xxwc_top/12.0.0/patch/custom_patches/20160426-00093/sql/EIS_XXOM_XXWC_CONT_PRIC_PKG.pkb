create or replace 
PACKAGE BODY          XXEIS.EIS_XXOM_XXWC_CONT_PRIC_PKG as
 --//============================================================================
--//  
--// Change Request 			:: Performance Issue  
--//
--// Object Usage 				:: This Object Referred by "CONTRACT PRICING REPORT - INTERNAL"
--//
--// Object Name         		:: EIS_XXOM_XXWC_CONT_PRIC_PKG
--//
--// Object Type         		:: Package Body
--//
--// Object Description  		:: This Package will trigger in before report and insert the values into Table
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        PRAMOD  	   04/20/2016      Initial Build  --TMS#20160426-00093    --Performance Tuning
--//============================================================================
PROCEDURE get_curr_sold_sales (p_item_id          IN     NUMBER
                                 ,p_cat_id           IN     NUMBER
                                 ,p_cat              IN     VARCHAR2
                                 ,p_cust_acct_id     IN     NUMBER
                                 ,p_list_header_id   IN     NUMBER
                                 ,P_START_DATE       IN     DATE
                                 ,P_END_DATE         IN     DATE
                                 ,p_units_sold          OUT NUMBER
                                 ,p_actual_sales        OUT NUMBER
                                 ,p_unit_cost           OUT NUMBER
                                 )
   IS
    --//============================================================================
--//  
--// Change Request 			:: Performance Issue  
--//
--// Object Usage 				:: This Object Referred by "CONTRACT PRICING REPORT - INTERNAL"
--//
--// Object Name         		:: GET_CURR_SOLD_SALES
--//
--// Object Type         		:: Procedure
--//
--// Object Description  		:: This Package will populate UNITS_SOLD,CONTRACT_PRICE,ACTUAL_SALES values for the table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        PRAMOD  	   04/20/2016      Initial Build  --TMS#20160426-00093   --Performance Tuning
--//============================================================================
    l_start_date   VARCHAR2 (32000);
	L_END_DATE     varchar2 (32000);
    l_sql varchar2(32000);
	l_sql1 varchar2(32000);
	L_SQL2 varchar2(32000);
	l_sql3 varchar2(32000);	  
     
   begin
      IF p_item_id != 0
      THEN
         begin
         -- DBMS_OUTPUT.PUT_LINE('FIRST');
                                               
     L_SQL:=   ' Select  NVL(SUM(DECODE(LINE_CATEGORY_CODE, ''RETURN'',NVL(OL.SHIPPED_QUANTITY,NVL(OL.FULFILLED_QUANTITY,OL.ORDERED_QUANTITY))*-1,NVL(OL.SHIPPED_QUANTITY,NVL(OL.FULFILLED_QUANTITY,OL.ORDERED_QUANTITY)))),0) UNITS_SOLD,
                                      NVL(SUM((NVL(DECODE (LINE_CATEGORY_CODE,''RETURN'',NVL(OL.SHIPPED_QUANTITY,NVL(OL.FULFILLED_QUANTITY,OL.ORDERED_QUANTITY))*-1,NVL(OL.SHIPPED_QUANTITY,NVL(OL.FULFILLED_QUANTITY,OL.ORDERED_QUANTITY))),0)*OL.UNIT_SELLING_PRICE)),0) ACTUAL_SALES,
                                      AVG(NVL(ol.unit_cost,0)) unit_cost 
                                 from OE_ORDER_LINES_all              OL,
                                      OE_ORDER_HEADERS_all            OH,
                                      oe_price_adjustments            OP,
                                      qp_list_headers                 qh,
                                      xxeis.EIS_XXWC_ITEM_DATA            exid
                                where 1=1
                                  and OL.FLOW_STATUS_CODE       = ''CLOSED''
                                  AND ol.inventory_item_id  =exid.inventory_item_id                                           
                                  AND oh.header_id              = ol.header_id 
                                  AND ol.line_id                = op.line_id
                                  and OP.HEADER_ID              = OH.HEADER_ID 
                                  AND QH.attribute10            = ''Contract Pricing'' 
                                  and QH.LIST_HEADER_ID         = OP.LIST_HEADER_ID
									AND ((ol.line_type_id != 1005 AND  Trunc(ol.Actual_Shipment_Date) >= '''||P_START_DATE||''')
                                    OR (ol.line_type_id = 1005 and  Trunc(ol.fulfillment_Date) >= '''||P_START_DATE||'''))
                                  AND ol.inventory_item_id =:1 
                                  AND ol.sold_to_org_id =:2
                                  and QH.LIST_HEADER_ID = :3                                            
                                  group by ol.sold_to_org_id, ol.inventory_item_id,op.list_header_id';
                 

        execute immediate l_sql into p_units_sold, p_actual_sales,p_unit_cost using p_item_id,p_cust_acct_id,p_list_header_id;
     
         EXCEPTION
            WHEN OTHERS
            then
               p_units_sold := 0;
               p_actual_sales := 0;
               p_unit_cost := 0;
              -- DBMS_OUTPUT.PUT_LINE(SQLERRM);
         END;
      ELSIF p_cat_id = 0
      THEN
         begin
            --dbms_output.put_line('SECOND');

          L_SQL2:='SELECT SUM (exusd.units_sold), SUM (exusd.actual_sales), AVG (0)                
            from(
           Select  NVL(SUM(DECODE(LINE_CATEGORY_CODE, ''RETURN'',NVL(OL.SHIPPED_QUANTITY,NVL(OL.FULFILLED_QUANTITY,OL.ORDERED_QUANTITY))*-1,NVL(OL.SHIPPED_QUANTITY,NVL(OL.FULFILLED_QUANTITY,OL.ORDERED_QUANTITY)))),0) UNITS_SOLD,
                                      NVL(SUM((NVL(DECODE (LINE_CATEGORY_CODE, ''RETURN'',NVL(OL.SHIPPED_QUANTITY,NVL(OL.FULFILLED_QUANTITY,OL.ORDERED_QUANTITY))*-1,NVL(OL.SHIPPED_QUANTITY,NVL(OL.FULFILLED_QUANTITY,OL.ORDERED_QUANTITY))),0)*OL.UNIT_SELLING_PRICE)),0) ACTUAL_SALES,
                                      AVG(NVL(ol.unit_cost,0)) unit_cost,
                                       ol.inventory_item_id inventory_item_id,
                                      ol.sold_to_org_id cust_account_id,
                                      max(0),
                                      OP.LIST_HEADER_ID LIST_HEADER_ID,
                                    min(EXID.CAT) CAT,
                                    min(EXID.CATEGORY_ID) CATEGORY_ID
                                 from OE_ORDER_LINES_all              OL,
                                      OE_ORDER_HEADERS_all            OH,
                                      oe_price_adjustments            OP,
                                      qp_list_headers                 qh,
                                      xxeis.EIS_XXWC_ITEM_DATA            exid
                                where 1=1
									and OL.FLOW_STATUS_CODE       = ''CLOSED''
									AND ol.inventory_item_id    = exid.inventory_item_id                                 
									AND oh.header_id              = ol.header_id 
									AND ol.line_id                = op.line_id
									AND op.header_id              = oh.header_id 
									AND QH.attribute10            = ''Contract Pricing'' 
									AND qh.list_header_id         = op.list_header_id
									AND ((ol.line_type_id != 1005 AND  Trunc(ol.Actual_Shipment_Date) >= '''||P_START_DATE||''')
                                    OR (ol.line_type_id = 1005 and  Trunc(ol.fulfillment_Date) >= '''||P_START_DATE||'''))
									AND ol.sold_to_org_id =:1
									and QH.LIST_HEADER_ID = :2  
                                   group by ol.sold_to_org_id, ol.inventory_item_id,op.list_header_id) exusd
                             WHERE exusd.cat ='''|| P_CAT ||'''
                             GROUP BY  exusd.cat';
                             
             execute immediate l_sql2 into p_units_sold, p_actual_sales, p_unit_cost using p_cust_acct_id,p_list_header_id;
         EXCEPTION
            WHEN OTHERS
            then
               p_units_sold := 0;
               p_actual_sales := 0;
               p_unit_cost := 0;
         END;
      ELSE
         begin
         -- DBMS_OUTPUT.PUT_LINE('THIRD');
         
       L_SQL3:='SELECT SUM (exusd.units_sold), SUM (exusd.actual_sales), AVG (0)
            from(
           Select  NVL(SUM(DECODE(LINE_CATEGORY_CODE, ''RETURN'',NVL(OL.SHIPPED_QUANTITY,NVL(OL.FULFILLED_QUANTITY,OL.ORDERED_QUANTITY))*-1,NVL(OL.SHIPPED_QUANTITY,NVL(OL.FULFILLED_QUANTITY,OL.ORDERED_QUANTITY)))),0) UNITS_SOLD,
                                      NVL(SUM((NVL(DECODE (LINE_CATEGORY_CODE, ''RETURN'',NVL(OL.SHIPPED_QUANTITY,NVL(OL.FULFILLED_QUANTITY,OL.ORDERED_QUANTITY))*-1,NVL(OL.SHIPPED_QUANTITY,NVL(OL.FULFILLED_QUANTITY,OL.ORDERED_QUANTITY))),0)*OL.UNIT_SELLING_PRICE)),0) ACTUAL_SALES,
                                      AVG(NVL(ol.unit_cost,0)) unit_cost,
                                       ol.inventory_item_id inventory_item_id,
                                      ol.sold_to_org_id cust_account_id,
                                      max(0),
                                      OP.LIST_HEADER_ID LIST_HEADER_ID,
                                    min(EXID.CAT) CAT,
                                    MIN(exid.category_id) category_id
                                 from OE_ORDER_LINES_all              OL,
                                      OE_ORDER_HEADERS_all            OH,
                                      oe_price_adjustments            OP,
                                      qp_list_headers                 qh,
                                      xxeis.EIS_XXWC_ITEM_DATA            exid
                                where 1=1
                                  and OL.FLOW_STATUS_CODE       = ''CLOSED''
                                  AND ol.inventory_item_id = exid.inventory_item_id                                         
                                  AND oh.header_id              = ol.header_id 
                                  AND ol.line_id                = op.line_id
                                  AND op.header_id              = oh.header_id 
                                  AND QH.attribute10            = ''Contract Pricing'' 
                                  AND qh.list_header_id         = op.list_header_id
                                   AND ((ol.line_type_id != 1005 AND  Trunc(ol.Actual_Shipment_Date) >= '''||P_START_DATE||''')
                                    OR (ol.line_type_id = 1005 and  Trunc(ol.fulfillment_Date) >= '''||P_START_DATE||'''))
                                  AND ol.sold_to_org_id =:1
                                  and QH.LIST_HEADER_ID = :2   
                                  group by ol.sold_to_org_id, ol.inventory_item_id,op.list_header_id) exusd
								 WHERE exusd.category_id ='|| P_CAT_ID ||'
                 GROUP BY exusd.category_id';
       
      execute immediate l_sql3 into p_units_sold, p_actual_sales, p_unit_cost using p_cust_acct_id,p_list_header_id;

         EXCEPTION
            WHEN OTHERS
            then
               p_units_sold := 0;
               p_actual_sales := 0;
               p_unit_cost := 0;
         END;
      end if;
   END get_curr_sold_sales; 

procedure  CONTRACT_PRICING_MODIFIER (         P_PROCESS_ID                   in number,
                                               P_START_DATE                   in date default null,
                                               P_END_DATE                     in date default null,
                                               P_Current_Loc                  in varchar2 default null,
                                               P_CATCLASS                     in varchar2 default null,
                                               P_CAT_PRICED_ITEMS             in varchar2  default null,
                                               P_RVP                          in varchar2  default null,
                                               P_DM                           in varchar2  default null,
                                               P_SALESPERSON                  IN VARCHAR2  DEFAULT NULL,
                                               P_JOB_ACCOUNT                  varchar2,
                                               P_MASTER_ACCOUNT               VARCHAR2,
                                               P_CUST_ID 					  in Varchar2 default null,
                                               P_customer_id                  in Varchar2 default null,
                                               P_MODIFIER_NAME                VARCHAR2     default null
                                               ) IS
--//============================================================================
--//
--// Object Name         :: contract_pricing_modifier  
--//
--// Object Usage 		 :: This Object Referred by "CONTRACT PRICING REPORT - INTERNAL"
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This  procedure is created based on the EIS_XXWC_CONT_MODI_PRICING_V1 View.Due to the 
--// 						performance issue ,spilt the view definition based on the driving tables and driving data.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        PRAMOD  	  04/22/2016       Initial Build  --TMS#20160426-00093    by PRAMOD  on 04-20-2016
--//============================================================================
    L_SALESREP_STR                varchar2(32000);
    l_customer_name               varchar2(32000);
    l_main_sql                    varchar2(32000);
    l_transaction_type            varchar2(32000);
    l_customer_number             varchar2(32000);
    l_modifier_name               varchar2(32000);
    l_count                       NUMBER;
    l_start_date                  varchar2(32000);
    l_end_date                    varchar2(32000);
    l_ref_cursor4                 CURSOR_TYPE4;
    l_units_sold                  NUMBER;
    l_actual_sales                NUMBER;
    l_unit_cost                   NUMBER;
    l_REF_CURSOR7                 CURSOR_TYPE7;  
    L_UNIT_SALES_DATA             VARCHAR2(32000);   
    l_list_price                  number;
    l_max_list_price              number;
    l_max_line_id                 number;
    L_CONTRACT_PRICE              NUMBER;
    l_inventory_item_id           NUMBER;
    L_ORG_ID                      NUMBER;  
    --Added--
    L_LIST_HEADER_ID              NUMBER;
    l_cust_account_id             NUMBER;
    l_category_id                 number;
    --End Added-----

	l_cust_id number;
	l_max_price_sql varchar2(32000);
	l_max_price_sql2 varchar2(32000);
    
BEGIN
fnd_file.put_line(fnd_file.log,'Start');

     L_SALESREP_STR:='And 1=1';
	 
l_max_price_sql:='select max(XLPT.LIST_PRICE)
                  FROM XXEIS.EIS_XXWC_LIST_PRICE XLPT                             
                  where QUALIFIER_ATTR_VALUE is not null
                  and INVENTORY_ITEM_ID = :1
                  and QUALIFIER_ATTR_VALUE = :2';
l_max_price_sql2:='select max(XLPT.LIST_PRICE)
                   FROM XXEIS.EIS_XXWC_LIST_PRICE XLPT                             
                   where QUALIFIER_ATTR_VALUE is null
                   and INVENTORY_ITEM_ID =:1';
    
   BEGIN
   

     Select ORGANIZATION_ID
       INTO l_org_id
       from MTL_PARAMETERS ODD
      WHERE ODD.ORGANIZATION_Code in (P_Current_Loc);
   EXCEPTION 
     WHEN OTHERS THEN
      l_org_id := null;
   END;
   

   if P_SALESPERSON is not null then
          L_SALESREP_STR:=L_SALESREP_STR||''||' AND EOXC.SALESREP IN ('||XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(P_SALESPERSON)||')';
    END IF;
  
    IF P_RVP IS  NOT NULL THEN 
        L_SALESREP_STR :=L_SALESREP_STR||''||' AND EOXC.person_id IN (SELECT distinct papf.person_id
                                                                       FROM per_all_assignments_f paaf, PER_ALL_PEOPLE_F PAPF
                                                                       WHERE paaf.person_id = papf.person_id   
                                                                         and ASSIGNMENT_TYPE=''E''
                                                                  CONNECT BY paaf.supervisor_id = PRIOR papf.person_id
                                                START WITH TRIM(papf.full_name) IN ('||XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(P_RVP)||'))'; 
    END IF;
    
    if P_DM is not null then
    L_SALESREP_STR :=L_SALESREP_STR||''|| '  AND EOXC.person_id IN (SELECT distinct papf.person_id
                                                                       FROM per_all_assignments_f paaf, PER_ALL_PEOPLE_F PAPF
                                                                       WHERE paaf.person_id = papf.person_id   
                                                                         and ASSIGNMENT_TYPE=''E''
                                                                  CONNECT BY paaf.supervisor_id = PRIOR papf.person_id
                                                START WITH TRIM(papf.full_name) IN ('||XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(P_DM)||'))'; 

    end if;
    
	IF P_CUSTOMER_ID IS NOT NULL  THEN --customer number
		l_customer_name := l_customer_name ||' and EOXC.ACCOUNT_NUMBER in ('||xxeis.eis_rs_utility.get_param_values(P_customer_id)||' )';
	End if;
    
	IF  p_cust_id IS NOT NULL THEN ---customer name
		l_customer_name := l_customer_name ||' and EOXC.CUSTOMER_NAME in ('||xxeis.eis_rs_utility.get_param_values(p_cust_id)||' )';
	End if;
  
  fnd_file.put_line(fnd_file.log,'l_customer_name  '||l_customer_name);
    
    IF P_MODIFIER_NAME IS NOT NULL THEN               
        l_modifier_name := ' and EOXC.MODIFIER_NAME in ('||xxeis.eis_rs_utility.get_param_values(P_MODIFIER_NAME)||' )';    

    end if;
    
    IF  P_CAT_PRICED_ITEMS = 'Include' THEN 
     
		 IF (P_JOB_ACCOUNT ='Include' AND P_MASTER_ACCOUNT ='Exclude') THEN 
			l_transaction_type := ' AND (EOXC.TRANSACTION_CODE = 1 OR EOXC.TRANSACTION_CODE = 3) ';
		 ELSIF (P_JOB_ACCOUNT ='Include' and P_MASTER_ACCOUNT ='Include') then 
			l_transaction_type := 'AND 1=1 ';
		 ELSE     
			l_transaction_type := ' AND (EOXC.TRANSACTION_CODE = 2 OR EOXC.TRANSACTION_CODE = 4) ';
		 END IF;
	 
    ELSE
		 IF (P_JOB_ACCOUNT ='Include' AND P_MASTER_ACCOUNT ='Exclude') THEN
			l_transaction_type := ' AND EOXC.TRANSACTION_CODE = 1'; 
		 ELSIF (P_JOB_ACCOUNT ='Include' AND P_MASTER_ACCOUNT ='Include')  THEN  
			l_transaction_type := ' AND (EOXC.TRANSACTION_CODE = 1 OR EOXC.TRANSACTION_CODE = 2) ';
		 ELSE     
			l_transaction_type := ' AND EOXC.TRANSACTION_CODE = 2';
		 END IF;    
    END IF; 
   
  L_MAIN_SQL := 'SELECT 
  EOXC.MODIFIER_NAME           MODIFIER_NAME         
                         ,EOXC.CUSTOMER_NAME           CUSTOMER_NAME          
                         ,EOXC.QUALIFIER_ATTRIBUTE     QUALIFIER_ATTRIBUTE  
                         ,EOXC.PRODUCT                 PRODUCT
                         ,EOXC.DESCRIPTION             DESCRIPTION              
                         ,EOXC.CAT_CLASS               CAT_CLASS     
                         ,XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_INV_GM(decode(
						 EOXC.INVENTORY_ITEM_ID,
						 0,NULL,EOXC.INVENTORY_ITEM_ID),
						 XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_COST_LOCATION,
						 EOXC.CATEGORY_ID)                      GM 
                         ,EOXC.LIST_PRICE             LIST_PRICE
                         ,EOXC.APP                     APP 
                         ,EOXC.VALUE1                  VALUE1
                         ,EOXC.CONTRACT_PRICE          CONTRACT_PRICE  
                         ,EOXC.UNITS_SOLD              UNITS_SOLD  
                         ,EOXC.ACTUAL_SALES            ACTUAL_SALES  
                         ,EOXC.ACTUAL_GROSS_MARGIN     ACTUAL_GROSS_MARGIN  
                         ,CASE WHEN EOXC.INVENTORY_ITEM_ID = 0 THEN
                           0
                        ELSE  ROUND(APPS.CST_COST_API.GET_ITEM_COST(1,EOXC.INVENTORY_ITEM_ID,XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_COST_LOCATION),2)  
                        END CURRENT_LOCATION_COST
                         ,EOXC.NEW_PRICE_OR_NEW_GM_DISC  NEW_PRICE_OR_NEW_GM_DISC
                         ,EOXC.INVENTORY_ITEM_ID         INVENTORY_ITEM_ID
                         ,EOXC.LIST_HEADER_ID            LIST_HEADER_ID
                         ,EOXC.PRODUCT_ATTRIBUTE_TYPE    PRODUCT_ATTRIBUTE_TYPE
                         ,EOXC.CATEGORY_ID               CATEGORY_ID
                         ,EOXC.CUST_ACCOUNT_ID           CUST_ACCOUNT_ID
                         ,EOXC.type                      type
                          ,'||P_PROCESS_ID||'             PROCESS_ID
                       ,EOXC.ARITHMETIC_OPERATOR       ARITHMETIC_OPERATOR
                       ,EOXC.OPERAND                   OPERAND
                       ,EOXC.unit_cost                 unit_cost
                       ,EOXC.CAT                       CAT
                       ,EOXC.CAT_DESC                  CAT_DESC
                       ,EOXC.PRICE_BY_CAT              PRICE_BY_CAT
                       ,EOXC.contract_type             contract_type
                       ,EOXC.FORMULA_NAME              FORMULA_NAME
                       ,EOXC.salesrep                  salesrep
                       ,EOXC.cat_class_desc            cat_class_desc
                       ,EOXC.Transaction_code          Transaction_code
                       ,EOXC.creation_Date             creation_Date
                       ,EOXC.person_id                 person_id
                       ,EOXC.account_number            account_number
                       ,EOXC.common_output_id          common_output_id
                       ,EOXC.location                  location
                       ,EOXC.party_site_number         party_site_number
                       ,EOXC.version_no                version_no
                       ,EOXC.price_type                price_type                       
                   FROM XXEIS.eis_xxwc_cont_modi_pricing EOXC
                   WHERE 1=1
                  '||L_SALESREP_STR||'
                  '||l_transaction_type||'
                  '||l_customer_name||'
                  '||l_modifier_name||'
                  ';
           
               FND_FILE.PUT_LINE(FND_FILE.LOG,'L_MAIN_SQL  '||L_MAIN_SQL);
--               DBMS_OUTPUT.put_line('OVER QUERY   '||L_MAIN_SQL);            
  OPEN L_REF_CURSOR4  FOR L_MAIN_SQL;
--  DBMS_OUTPUT.put_line('eNTERED'); 
  LOOP
   FETCH L_REF_CURSOR4 bulk collect into G_VIEW_RECORD4 limit 10000;          
                 L_COUNT := 0;
--                  DBMS_OUTPUT.put_line('L_COUNT  '||L_COUNT);  
              FOR j in 1..G_VIEW_RECORD4.count 
                   Loop
                      l_units_sold := 0;
                      l_actual_sales := 0;
                      l_unit_cost := 0;
                      l_inventory_item_id := g_view_record4(j).inventory_item_id;
                      L_LIST_HEADER_ID     := G_VIEW_RECORD4(J).LIST_HEADER_ID;      
                      l_cust_account_id    := g_view_record4(j).cust_account_id ;       
                      L_CATEGORY_ID         := G_VIEW_RECORD4(J).CATEGORY_ID ;      
                      
                
				begin
                    if( (L_INVENTORY_ITEM_ID is not null) and (L_ORG_ID is not null))then
						execute immediate l_max_price_sql into L_MAX_LIST_PRICE using L_INVENTORY_ITEM_ID,L_ORG_ID;
                    END IF;
                    

                    
                     if ((L_MAX_LIST_PRICE is null)) then
						execute immediate l_max_price_sql2 into L_MAX_LIST_PRICE using L_INVENTORY_ITEM_ID;
					end if; 
          
--DBMS_OUTPUT.put_line(' l_max_price_sql2   '||l_max_price_sql);
          
					if L_MAX_LIST_PRICE is null then 
                          L_MAX_LIST_PRICE := 20000;
                    end if;
                    
               EXCEPTION when OTHERS then
                          L_MAX_LIST_PRICE := 20000;
               end;
                                              
                      l_contract_price := 0;
                      IF G_VIEW_RECORD4(j).ARITHMETIC_OPERATOR ='AMT' THEN
                        l_contract_price := (l_max_list_price - G_VIEW_RECORD4(j).OPERAND);
                      ELSIF  G_VIEW_RECORD4(j).ARITHMETIC_OPERATOR = '%' THEN  
                        l_contract_price :=((l_max_list_price )-((G_VIEW_RECORD4(j).OPERAND*l_max_list_price)/100));
                      ELSIF G_VIEW_RECORD4(j).ARITHMETIC_OPERATOR = 'NEWPRICE'  THEN
                        l_contract_price := G_VIEW_RECORD4(j).OPERAND;      
                      ELSIF G_VIEW_RECORD4(j).APP is not null THEN   
                        l_contract_price :=((l_max_list_price)/(1-0.15));           
                      END IF; 
--                             DBMS_OUTPUT.put_line('START SOLD SALES');            
    fnd_file.put_line(fnd_file.log,'START SOLD SALES');
                      EIS_XXOM_XXWC_CONT_PRIC_PKG.get_curr_sold_sales
					  (	G_VIEW_RECORD4(J).INVENTORY_ITEM_ID,
						G_VIEW_RECORD4(J).CATEGORY_ID,
						G_VIEW_RECORD4(J).CAT,
						G_VIEW_RECORD4(J).CUST_ACCOUNT_ID,
						G_VIEW_RECORD4(J).LIST_HEADER_ID,
						P_START_DATE,
						P_END_DATE,
						L_UNITS_SOLD,
						L_ACTUAL_SALES,
						L_UNIT_COST
					);

  fnd_file.put_line(fnd_file.log,'END SOLD SALES');
  
    SELECT xxeis.eis_rs_common_outputs_s.NEXTVAL
                        INTO G_VIEW_TAB4(J).COMMON_OUTPUT_ID
                        FROM DUAL;
                        
--                     fnd_file.put_line(fnd_file.log,'G_VIEW_TAB4.count  '||G_VIEW_TAB4.count);    
                        
                        G_VIEW_TAB4(j).MODIFIER_NAME              := G_VIEW_RECORD4(j).MODIFIER_NAME;
                        G_VIEW_TAB4(j).CUSTOMER_NAME              := G_VIEW_RECORD4(j).CUSTOMER_NAME;
                        G_VIEW_TAB4(j).QUALIFIER_ATTRIBUTE        := G_VIEW_RECORD4(j).QUALIFIER_ATTRIBUTE;
                        G_VIEW_TAB4(j).PRODUCT                    := G_VIEW_RECORD4(j).PRODUCT;
                        G_VIEW_TAB4(j).DESCRIPTION                := G_VIEW_RECORD4(j).DESCRIPTION;    
                        G_VIEW_TAB4(j).CAT_CLASS                  := G_VIEW_RECORD4(j).CAT_CLASS;
                        G_VIEW_TAB4(J).GM                         := G_VIEW_RECORD4(J).GM;
                        G_VIEW_TAB4(j).LIST_PRICE                 := l_max_list_price;
                        G_VIEW_TAB4(j).APP                        := G_VIEW_RECORD4(j).APP;
                        G_VIEW_TAB4(j).VALUE1                     := G_VIEW_RECORD4(j).VALUE1;
                        G_VIEW_TAB4(j).CONTRACT_PRICE             := l_contract_price;
                        G_VIEW_TAB4(j).UNITS_SOLD                 := l_units_sold;
                        G_VIEW_TAB4(j).ACTUAL_SALES               := l_actual_sales;
                        G_VIEW_TAB4(j).ACTUAL_GROSS_MARGIN        := G_VIEW_RECORD4(j).ACTUAL_GROSS_MARGIN;
                        G_VIEW_TAB4(j).CURRENT_LOCATION_COST      := G_VIEW_RECORD4(j).CURRENT_LOCATION_COST;
                        G_VIEW_TAB4(j).NEW_PRICE_OR_NEW_GM_DISC   := G_VIEW_RECORD4(j).NEW_PRICE_OR_NEW_GM_DISC;
                        G_VIEW_TAB4(j).INVENTORY_ITEM_ID          := G_VIEW_RECORD4(j).INVENTORY_ITEM_ID;
                        G_VIEW_TAB4(j).LIST_HEADER_ID             := G_VIEW_RECORD4(j).LIST_HEADER_ID;
                        G_VIEW_TAB4(J).PRODUCT_ATTRIBUTE_TYPE     := G_VIEW_RECORD4(J).PRODUCT_ATTRIBUTE_TYPE;
                        G_VIEW_TAB4(J).CATEGORY_ID                := G_VIEW_RECORD4(J).CATEGORY_ID;
                        G_VIEW_TAB4(J).CUST_ACCOUNT_ID            := G_VIEW_RECORD4(J).CUST_ACCOUNT_ID;
                        G_VIEW_TAB4(J).TYPE                       := G_VIEW_RECORD4(J).TYPE;
                        G_VIEW_TAB4(J).PROCESS_ID                 := G_VIEW_RECORD4(J).PROCESS_ID;
                      G_VIEW_TAB4(j).ARITHMETIC_OPERATOR        := G_VIEW_RECORD4(j).ARITHMETIC_OPERATOR;
                      G_VIEW_TAB4(j).OPERAND                    := G_VIEW_RECORD4(j).OPERAND;
                      G_VIEW_TAB4(j).unit_cost                  := l_unit_cost;
                      G_VIEW_TAB4(j).CAT                        := G_VIEW_RECORD4(j).CAT;
                      G_VIEW_TAB4(j).CAT_DESC                   := G_VIEW_RECORD4(j).CAT_DESC;
                      G_VIEW_TAB4(j).PRICE_BY_CAT               := G_VIEW_RECORD4(j).PRICE_BY_CAT;
                      G_VIEW_TAB4(j).contract_type              := G_VIEW_RECORD4(j).contract_type;
                      G_VIEW_TAB4(j).FORMULA_NAME               := G_VIEW_RECORD4(j).FORMULA_NAME;
                      G_VIEW_TAB4(J).SALESREP                   := G_VIEW_RECORD4(J).SALESREP;
                      G_VIEW_TAB4(j).cat_class_desc             := G_VIEW_RECORD4(j).cat_class_desc;
                      G_VIEW_TAB4(J).TRANSACTION_CODE           := G_VIEW_RECORD4(J).TRANSACTION_CODE;
                      G_VIEW_TAB4(J).creation_Date              := G_VIEW_RECORD4(J).creation_Date;
                      G_VIEW_TAB4(j).person_id                  := G_VIEW_RECORD4(j).person_id;
                      G_VIEW_TAB4(J).ACCOUNT_NUMBER             := G_VIEW_RECORD4(J).ACCOUNT_NUMBER;
--                      G_VIEW_TAB (j).common_output_id           := xxeis.eis_rs_common_outputs_s.nextval;
                      G_VIEW_TAB4(j).location                   := G_VIEW_RECORD4(j).location;
                      G_VIEW_TAB4(j).party_site_number          := G_VIEW_RECORD4(j).party_site_number;
                      G_VIEW_TAB4(j).version_no                 := G_VIEW_RECORD4(j).version_no;
                      G_VIEW_TAB4(j).price_type                 := G_VIEW_RECORD4(j).price_type;
                 l_count := l_count+1;
              END LOOP;   
 
         fnd_file.put_line(fnd_file.log,'G_VIEW_TAB4.count  '||G_VIEW_TAB4.count);
               IF l_count >= 1  then
                  forall j in 1 .. G_VIEW_TAB4.count 
                     INSERT INTO XXEIS.EIS_XXWC_CONT_MODI_PRICING_V1
                          values g_view_tab4 (j);    
                   COMMIT;       
               end if;  
         
               G_VIEW_TAB4.delete;
               G_VIEW_RECORD4.delete;
               IF l_ref_cursor4%NOTFOUND Then
               CLOSE l_ref_cursor4;
               EXIT;
               End if;
           END LOOP; 
 COMMIT;
 EXCEPTION WHEN OTHERS THEN
 Fnd_File.Put_Line(FND_FILE.log,'THE ERROR IS'||SQLCODE||SQLERRM);
                  
end CONTRACT_PRICING_MODIFIER;

end EIS_XXOM_XXWC_CONT_PRIC_PKG;
/
