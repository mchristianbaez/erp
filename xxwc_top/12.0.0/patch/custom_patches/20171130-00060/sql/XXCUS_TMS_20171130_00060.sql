/*
 TMS : 20171130_00060.sql
 Date: 12/3/2017
 Notes:  Backup the 9 gl interface records we are going to remove from gl interface.
*/
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
 --
 n_loc number :=0;
 v_db varchar2(30) :=Null;
 v_ok boolean :=Null;
 --
BEGIN --Main Processing...
   --
   n_loc :=101;
   --
   BEGIN 
     --
     n_loc :=102;
     --
     execute immediate 'create table xxcus.xxcus_tms_20171130_00060_bkp as  
        select *
        from apps.gl_interface
        where 1 =2';   
     --
     v_ok :=TRUE;
     --    
     n_loc :=103;
     --
     DBMS_OUTPUT.put_line ('Backup table xxcus.xxcus_tms_20171130_00060_bkp created.');
     --
   EXCEPTION
    WHEN OTHERS THEN 
     --
     v_ok :=FALSE;
     --
     n_loc :=104;
     --
   END;
   --
   if (v_ok) then
       -- 
       n_loc :=105;
       --
       v_ok :=Null;       
       --
       begin
        --
        n_loc :=106;
        --
        execute immediate 'INSERT INTO xxcus.xxcus_tms_20171130_00060_bkp 
           (
             SELECT * 
             FROM APPS.GL_INTERFACE 
             WHERE 1 =1
                    AND USER_JE_SOURCE_NAME =''Spreadsheet''
                    AND USER_JE_CATEGORY_NAME =''Adjustment''
                    AND REQUEST_ID IS NULL
                    AND STATUS =''NEW''
           
           )';        
         --
         dbms_output.put_line('@ n_loc ='||n_loc||', INSERTED RECORDS ='||sql%rowcount);
         --     
         if sql%rowcount >0 then 
              delete apps.gl_interface 
              where 1 =1
                    and user_je_source_name ='Spreadsheet'
                    and user_je_category_name ='Adjustment'
                    and request_id is null
                    and status ='NEW';
             --
             dbms_output.put_line('@ n_loc ='||n_loc||', deleted records ='||sql%rowcount);
             --                        
         else
             --
             dbms_output.put_line('@ n_loc ='||n_loc||', inserted records =0');
             --          
         end if;
         --   
        n_loc :=107;
        --
       exception
        when others then
         --
         n_loc :=103;
         --
         dbms_output.put_line('@ n_loc ='||n_loc||', message ='||sqlerrm);
         --
       end;
       --
   else
    DBMS_OUTPUT.put_line ('v_ok is FALSE...check xpatch log files for error messages.');
   end if;
   --
  commit;
   --
EXCEPTION
   WHEN OTHERS THEN
      DBMS_OUTPUT.put_line ('TMS: 20171130_00060_bkp-00081, Errors =' || SQLERRM);
      rollback;
END;
/