/*
 TMS:  20170607-00083
 Date: 06/07/2017
 Notes:  BACKUP GL INTERFACE TABLE AND TRUNCATE IT.
*/
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
 --
 n_loc number :=0;
 l_sql varchar2(2000) :=Null;
 --
BEGIN --Main Processing...
   --
   n_loc :=101;
   --
   --
   begin
    --
    l_sql :='CREATE TABLE XXCUS.XXCUS_TMS_20170607_00083 AS SELECT * FROM GL.GL_DAILY_RATES_INTERFACE';
    --
    n_loc :=102;
    --    
    EXECUTE IMMEDIATE l_sql;
    --
    n_loc :=103;
    --        
    dbms_output.put_line('Table GL.GL_DAILY_RATES_INTERFACE archived, backup table name :  XXCUS.XXCUS_TMS_20170607_00083');
    --
    n_loc :=104;
    --        
    l_sql :='TRUNCATE TABLE GL.GL_DAILY_RATES_INTERFACE';
    --
    n_loc :=105;
    --        
    EXECUTE IMMEDIATE l_sql;
    --
    n_loc :=106;
    --        
    dbms_output.put_line('Table GL.GL_DAILY_RATES_INTERFACE truncated successfully');
    --
    n_loc :=107;
    --        
   exception
    when others then
     --
     n_loc :=108;
     --
     dbms_output.put_line('@ n_loc ='||n_loc||', message ='||sqlerrm);
     --  
   end;
   --
   commit;
   --
EXCEPTION
   WHEN OTHERS THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20170607-00083, Errors =' || SQLERRM);
END;
/