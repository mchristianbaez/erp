CREATE OR REPLACE PACKAGE BODY APPS."XXCUS_OIE_MISC_PKG" AS

  /*******************************************************************************
  * Package:   This is common OIE Data cleanup automation package
  * Description: This is common OIE Data cleanup automation package

  PACKAGE HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     09/30/2009    Luong Vu        original (SR 14729, RFC 8914)
                                        original (SR 18759, RFC 8914)
                        Murali Krishnan Original for  change_ap_invoice_num
                                                 (SR 18679, RFC 8914)
          invoice_num store pl/sql error fixed
          l_dup_report VARCHAR2(100); 3 to 100
          RFC 8932
  2.0     09/30/2009    Luong Vu        Migrated from 11i.
  2.1     07/10/2015    Bala Seshadri   Add transaction types 52 and 61  ESMS 285859 / TMS 20150706-00146
  2.2     07/10/2015    Bala Seshadri   Remove unused variables for  ESMS 285859 / TMS 20150706-00146
  2.4     08/05/2016    Bala Seshadri   TMS 20160803-00236 and ESMS 422886. 
  ********************************************************************************/

  program_error EXCEPTION;
  incorrect_report EXCEPTION;

  -- Error DEBUG
  g_err_callfrom  VARCHAR2(175) DEFAULT 'XXCUS_OIE_MISC_PKG';
  g_err_callpoint VARCHAR2(175) DEFAULT 'START';
  g_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
  --g_distro_list   VARCHAR2(75)  DEFAULT 'luong.vu@hdsupply.com';


  /*******************************************************************************
  * Procedure:   update_cc_payflag
  * Description: NSF payment transaction has a transaction type of 30.  The Visa import
  *              program set the payment_flag for this type as "N".  This cause it to appear
  *              in the user worklist to reconcile.  We don't want user to reconcile this type
  *              of transactions.  The work around is to set it to "Y".
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     09/30/2009    Luong Vu        original (SR 14729, RFC 8914)
  1.1     07/13/2010    Luong Vu        SR-47812 - Add trx type 62
  2.1     07/10/2015    Bala Seshadri   Add transaction types 52 and 61
  2.2    07/21/2015     Bala Seshadri  Comment unused variables
  2.3    05/16/2016     Bala Seshadri  TMS 20160412-00162 / ESMS 285859
 2.4     08/05/2016    Bala Seshadri   TMS 20160803-00236 and ESMS 422886. 
  ********************************************************************************/
  PROCEDURE update_cc_payflag(errbuff OUT VARCHAR2
                             ,retcode OUT NUMBER) IS

    l_errbuf VARCHAR2(2000);
  --n_count number :=0; --Ver 2.2
  BEGIN
    UPDATE ap.ap_credit_card_trxns_all acct
       SET acct.payment_flag = 'Y'
    --     WHERE acct.transaction_type = 30
     WHERE 1 =1 -- Ver 2.4
       AND acct.transaction_type IN (30, 52, 61, 62) -- SR 47812 --Ver 2.1
       AND acct.payment_flag = 'N'
       AND (acct.expensed_amount = 0 OR acct.expensed_amount IS NULL)
        AND acct.payment_due_from_code = 'INDIVIDUAL'; -- Ver 2.4      
       /* -- Begin Ver 2.4
       -- Begin Ver 2.3
       AND acct.card_program_id IN 
           (
             10004 --WW PNC Employee-Pay Card Program
            ,10040 --CAD FM Employee-Pay Card Program
            ,10002 -- FM PNC Employee-Pay Card Program       
           );
        -- End Ver 2.3
        */  -- End Ver 2.4
    COMMIT;
  EXCEPTION
    WHEN OTHERS THEN
      l_errbuf := substr(SQLERRM, 1, 2000);
      fnd_file.put_line(fnd_file.log, 'The Program Has Errored:');
      fnd_file.put_line(fnd_file.output, 'The Program Has Errored:');
      xxcus_error_pkg.xxcus_error_main_api(p_called_from => g_err_callfrom,
                                           p_calling => g_err_callpoint,
                                           p_ora_error_msg => l_errbuf,
                                           p_distribution_list => g_distro_list,
                                           p_module => 'OIE');
      errbuff := l_errbuf;
      retcode := 2;

  END;

  /*******************************************************************************
  * Procedure:   update_cc_trans_dt
  * Description: fix credit card transaction where the trx_available_date is greater
  *              than the transaction post date.
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     10/12/2009    Luong Vu         original (SR 18759, RFC 8914)
  ********************************************************************************/
  PROCEDURE update_cc_trans_dt(errbuff OUT VARCHAR2
                              ,retcode OUT NUMBER) IS

    l_errbuf VARCHAR2(2000);

  BEGIN
    UPDATE ap.ap_credit_card_trxns_all acct
       SET acct.trx_available_date = acct.posted_date
     WHERE acct.trx_available_date > acct.posted_date;

    COMMIT;

  EXCEPTION
    WHEN OTHERS THEN
      l_errbuf := substr(SQLERRM, 1, 2000);
      fnd_file.put_line(fnd_file.log, 'The Program Has Errored:');
      fnd_file.put_line(fnd_file.output, 'The Program Has Errored:');
      xxcus_error_pkg.xxcus_error_main_api(p_called_from => g_err_callfrom,
                                           p_calling => g_err_callpoint,
                                           p_ora_error_msg => l_errbuf,
                                           p_distribution_list => g_distro_list,
                                           p_module => 'OIE');
      errbuff := l_errbuf;
      retcode := 2;

  END;

  /*******************************************************************************
  * Procedure:   change_ap_invoice_num
  * Description: change invoice number for Duplicate Report Exception ERs.
  * Conc program Name: HDS Change Invoice Number for Duplicate Report Exception

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     10/12/2009    Murali Krishnan original (SR 18679, RFC 8914)
  ********************************************************************************/
  PROCEDURE change_ap_invoice_num(errbuf           OUT VARCHAR2
                                 ,retcode          OUT NUMBER
                                 ,invoice_num_from IN VARCHAR2
                                 ,invoice_num_to   IN VARCHAR2) IS
    l_err_msg    VARCHAR2(2000);
    l_dup_report VARCHAR2(100);
    CURSOR dupreport IS
      SELECT invoice_num
        FROM ap_expense_report_headers_all
       WHERE expense_status_code = 'INVOICED'
         AND reject_code = 'Duplicate Report'
         AND invoice_num = invoice_num_from;

  BEGIN

    -- Check the "from invoice" is in the exception report
    l_dup_report := NULL;
    OPEN dupreport;
    FETCH dupreport
      INTO l_dup_report;
    CLOSE dupreport;

    -- if ok  all, update the invoice_num to new one and commit
    IF l_dup_report IS NOT NULL THEN
      UPDATE ap_expense_report_headers_all
         SET invoice_num = nvl(invoice_num_to, invoice_num || 'A')
       WHERE expense_status_code = 'INVOICED'
         AND reject_code = 'Duplicate Report'
         AND invoice_num = invoice_num_from;
    ELSE
      RAISE incorrect_report;
    END IF;


    COMMIT;
    fnd_file.put_line(fnd_file.output,
                      'Invoice Number Changed from ' || invoice_num_from ||
                       ' To ' || nvl(invoice_num_to, l_dup_report || 'A'));
    fnd_file.put_line(fnd_file.output,
                      'Submit Expense Report Import to bring this invoice into AP');
  EXCEPTION
    WHEN program_error THEN

      l_err_msg := substr(SQLERRM, 1, 2000);
      fnd_file.put_line(fnd_file.log, 'The Program Has Errored:');
      fnd_file.put_line(fnd_file.output, 'The Program Has Errored:');
      xxcus_error_pkg.xxcus_error_main_api(p_called_from => g_err_callfrom,
                                           p_calling => g_err_callpoint,
                                           p_ora_error_msg => l_err_msg,
                                           p_distribution_list => g_distro_list,
                                           p_module => 'OIE');
      errbuf  := l_err_msg;
      retcode := 2;

    WHEN incorrect_report THEN
      l_err_msg := 'This is not a Duplicate Expense Report';
      fnd_file.put_line(fnd_file.log, l_err_msg);
      --fnd_file.put_line(fnd_file.output, l_fulltext);
      retcode := 2;
      errbuf  := l_err_msg;

    WHEN OTHERS THEN
      l_err_msg := substr(SQLERRM, 1, 2000);
      fnd_file.put_line(fnd_file.log, 'The Program Has Errored:');
      fnd_file.put_line(fnd_file.output, 'The Program Has Errored:');
      xxcus_error_pkg.xxcus_error_main_api(p_called_from => g_err_callfrom,
                                           p_calling => g_err_callpoint,
                                           p_ora_error_msg => l_err_msg,
                                           p_distribution_list => g_distro_list,
                                           p_module => 'OIE');
      errbuf  := l_err_msg;
      retcode := 2;

  END change_ap_invoice_num;

  /*******************************************************************************
  * Procedure:   UPDATE_EXPENSE_DISTRIBUTION
  * Description: This program will update expense report distribution CCID
  *
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     07-13-2010    Luong Vu        Initial creation of the procedure
  1.1     08-30-2010    Luong Vu        Add segment5, segment6  SR 51831
  2.0     05-29-2012    Luong Vu        Add segment7 for R12
  ********************************************************************************/

  PROCEDURE update_expense_dist_ccid(errbuf    OUT VARCHAR2
                                    ,retcode   OUT NUMBER
                                    ,p_exp_rep NUMBER) IS

    -- Intialize Variables
    l_err_msg VARCHAR2(2000);
    l_sec     VARCHAR2(150);
    l_count   NUMBER := 0;

    -- Start Main Program
  BEGIN

    l_sec := 'find missing CCID line in ap_expense_report_lines_all table';
    -- Is CCID missing in lines_all table?
    SELECT COUNT(*)
      INTO l_count
      FROM ap.ap_expense_report_lines_all l
     WHERE l.report_header_id = p_exp_rep
       AND l.code_combination_id IS NULL;

    -- If CCID is null in lines_all table, make correction
    IF l_count > 0 THEN

      l_sec := 'update CCID in ap_expense_report_lines_all table';

      FOR c_rec IN (SELECT l.report_header_id
                          ,l.report_line_id
                      FROM ap.ap_expense_report_lines_all l
                     WHERE l.report_header_id = p_exp_rep
                       AND l.code_combination_id IS NULL)
      LOOP

        UPDATE ap.ap_expense_report_lines_all line
           SET line.code_combination_id =
               (SELECT gcc.code_combination_id
                  FROM gl.gl_code_combinations    gcc
                      ,ap.ap_exp_report_dists_all erd
                 WHERE erd.segment1 = gcc.segment1
                   AND erd.segment2 = gcc.segment2
                   AND erd.segment3 = gcc.segment3
                   AND erd.segment4 = gcc.segment4
                   AND erd.segment5 = gcc.segment5 --SR 51831
                   AND erd.segment6 = gcc.segment6
                   AND erd.segment7 = gcc.segment7
                   AND erd.report_line_id = c_rec.report_line_id)
         WHERE line.report_line_id = c_rec.report_line_id;

        COMMIT;
      END LOOP;
    END IF;

    l_sec := 'update CCID in ap_exp_report_dists_all table';

    -- Update CCID in ap_exp_report_dists_all table
    FOR c_rec2 IN (SELECT erd.report_header_id
                         ,erd.report_line_id
                     FROM ap.ap_exp_report_dists_all erd
                    WHERE erd.report_header_id = p_exp_rep
                      AND erd.code_combination_id IS NULL)
    LOOP

      UPDATE ap.ap_exp_report_dists_all erd
         SET erd.code_combination_id =
             (SELECT gcc.code_combination_id
                FROM gl.gl_code_combinations    gcc
                    ,ap.ap_exp_report_dists_all erd
               WHERE erd.segment1 = gcc.segment1
                 AND erd.segment2 = gcc.segment2
                 AND erd.segment3 = gcc.segment3
                 AND erd.segment4 = gcc.segment4
                 AND erd.segment5 = gcc.segment5 --SR 51831
                 AND erd.segment6 = gcc.segment6
                 AND erd.segment7 = gcc.segment7
                 AND erd.report_line_id = c_rec2.report_line_id)
       WHERE erd.report_line_id = c_rec2.report_line_id;

      COMMIT;
    END LOOP;

  EXCEPTION
    WHEN OTHERS THEN
      l_err_msg       := substr(SQLERRM, 1, 2000);
      g_err_callpoint := l_sec;

      fnd_file.put_line(fnd_file.log, 'The Program Has Errored:');
      fnd_file.put_line(fnd_file.output, 'The Program Has Errored:');
      xxcus_error_pkg.xxcus_error_main_api(p_called_from => g_err_callfrom,
                                           p_calling => g_err_callpoint,
                                           p_ora_error_msg => l_err_msg,
                                           p_distribution_list => g_distro_list,
                                           p_module => 'OIE');
      errbuf  := l_err_msg;
      retcode := 2;

  END update_expense_dist_ccid;

END xxcus_oie_misc_pkg;
/