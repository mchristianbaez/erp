/*
 TMS: 20160803-00236
 ESMS: 422886
 Date: 08/05/2016
*/
set serveroutput on size 1000000;
declare
 --
    cursor get_pmt_flag is
    select trx_id, payment_flag
    from apps.ap_credit_card_trxns_all
    where 1 =1
    and trx_id in 
    (
     '3391252',
    '3391060',
    '3381754',
    '3381755',
    '3323458',
    '3323491',
    '3323324',
    '3319434',
    '3294163',
    '3258288',
    '3245022',
    '3125713',
    '3125938'
    ); 
 --
     procedure print_log(p_message in varchar2) is
     begin
      if apps.fnd_global.conc_request_id >0 then
       apps.fnd_file.put_line(fnd_file.log, p_message);
      else
       dbms_output.put_line(p_message);
      end if;
     end;
 --
begin
     --
     print_log(' ');    
     print_log('****** Print pre update payment flag values *******');     
     print_log(' ');       
     --
     begin
      for rec in get_pmt_flag loop
       print_log('Trx ID: '||rec.trx_id||', Payment flag before update: '||rec.payment_flag);
      end loop;
     exception
      when others then
       print_log('@100 : Error: '||sqlerrm);
     end;
     --
     print_log(' ');      
     print_log('Begin SQL update...');
     --   
      begin
       --
       savepoint start_here;
       -- 
        update apps.ap_credit_card_trxns_all
        set payment_flag = 'N'
        where trx_id in 
        (
            '3391252',
            '3391060',
            '3381754',
            '3381755',
            '3323458',
            '3323491',
            '3323324',
            '3319434',
            '3294163',
            '3258288',
            '3245022',
            '3125713',
            '3125938'
        )
        and payment_flag !='N';
        --
       print_log('.....Records updated : '||sql%rowcount);
        --
        if (sql%rowcount >0) then
         --
         commit;
         --                 
        end if;
        --
      exception
       when others then
        rollback to start_here;
        print_log('Error SQL update : msg ='||sqlerrm);
      end;
    --
     print_log('End SQL mass update...');
     print_log(' ');
     print_log('****** Print post update payment flag values *******');  
     print_log(' ');        
     --
     begin
      for rec in get_pmt_flag loop
       print_log('Trx ID: '||rec.trx_id||', Payment flag after update: '||rec.payment_flag);
      end loop;
      print_log(' ');       
     exception
      when others then
       print_log('@100 : Error: '||sqlerrm);
     end;
     --     
exception
 when others then
  print_log('Outer block, message ='||sqlerrm);
end;
/