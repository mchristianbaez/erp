  /*  
  Module: HDS Rebates [Trade Management]
  Date: 01/06/2016
  Ticket: TMS 20160106-00100 / Incident 642066
  Description: Split partition OTHER into USABB and the rest into the OTHER partition 
  */
ALTER TABLE XXCUS.XXCUS_REBATE_PRODUCT_HIST_TBL
     SPLIT PARTITION OTHER VALUES('USABLUEBOOK') INTO (
          PARTITION USABB,
          PARTITION OTHER
     );
--
COMMENT ON TABLE XXCUS.XXCUS_REBATE_PRODUCT_HIST_TBL IS 'TMS 20160106-00100 / Incident 642066';
--