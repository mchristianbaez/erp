SET SERVEROUT ON

BEGIN
   DBMS_OUTPUT.PUT_LINE ('Before Sripts execution');

   UPDATE ar_payment_schedules_all
      SET due_Date = due_Date + 60, term_id = 1017
    WHERE     created_by = 33710
          AND term_id = 5
          AND customer_trx_id IN (SELECT customer_trx_id
                                    FROM ra_customer_trx_all
                                   WHERE     1 = 1
                                         AND term_id = 5
                                         AND batch_source_id = 1012);

   DBMS_OUTPUT.PUT_LINE (
      'Number of rows updated - ar_payment_schedules_all:' || SQL%ROWCOUNT);

   COMMIT;

   UPDATE ra_customer_trx_all
      SET term_id = 1017
    WHERE     1 = 1
          AND term_id = 5
          AND created_by = 33710
          AND batch_source_id = 1012;

   DBMS_OUTPUT.PUT_LINE (
      'Number of rows updated - ra_customer_trx_all:' || SQL%ROWCOUNT);

   COMMIT;
   
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.PUT_LINE ('Error occured:' || SQLERRM);
      ROLLBACK;
END;
/