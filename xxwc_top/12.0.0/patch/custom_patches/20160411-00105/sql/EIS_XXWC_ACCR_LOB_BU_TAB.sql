--------------------------------------------------------------------------------------
/*******************************************************************************
  Table: EIS_XXWC_ACCR_LOB_BU_TAB
  Description: This table is used to get data from XXEIS.EIS_XXWC_PUR_ACC_LOB_BU_PKG Package.
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     12-Apr-2016        Siva   		TMS#20160411-00105  Performance Tuning
********************************************************************************/
DROP TABLE XXEIS.EIS_XXWC_ACCR_LOB_BU_TAB CASCADE CONSTRAINTS;

CREATE TABLE XXEIS.EIS_XXWC_ACCR_LOB_BU_TAB
  (
    PROCESS_ID         NUMBER,
    AGREEMENT_YEAR     NUMBER,
    MVID               VARCHAR2(150 BYTE),
    VENDOR_NAME        VARCHAR2(150 BYTE),
    LOB                VARCHAR2(150 BYTE),
    BU                 VARCHAR2(150 BYTE),
    CAL_YEAR           NUMBER,
    CAL_MONTH          VARCHAR2(150 BYTE),
    CALENDAR_PURCHASES NUMBER,
    ACCRUAL_PURCHASES  NUMBER,
    REBATE             NUMBER,
    COOP               NUMBER,
    TOTAL_ACCRUALS     NUMBER
)
/
  