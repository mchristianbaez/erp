CREATE OR REPLACE package body      XXEIS.EIS_XXWC_PUR_ACC_LOB_BU_PKG as
 --//============================================================================
--//  
--// Change Request 			:: Performance Issue  --TMS#20160411-00105  by Pramod on 04-12-2016
--//
--// Object Usage 				:: This Object Referred by "PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU(NEW)"
--//
--// Object Name         		:: XXEIS.EIS_XXWC_PUR_ACC_LOB_BU_PKG
--//
--// Object Type         		:: Package Body
--//
--// Object Description  		:: This Package will trigger in before report and insert the values into Table
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Pramod  	04/12/2016   Initial Build --TMS#20160411-00105  by Pramod on 04-12-2016
--//============================================================================
procedure GET_ACC_lob_bu_DTLS (	p_process_id 	in number,
								p_cal_month 	in varchar2,
								p_Lob			in varchar2,
								p_mvid			in varchar2,
								P_Calendar_Year in number,
								P_Agreement_Year	in number
							) as
--//============================================================================
--//
--// Object Name         :: GET_ACC_LOB_BU_DTLS  
--//
--// Object Usage 		 :: This Object Referred by "PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU(NEW)"
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This  procedure is created based on the XXEIS.EIS_PUR_ACCRAL_LOB_BU_V View.Due to the 
--// 						performance issue ,spilt the view definition based on the driving tables and driving data.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Pramod  	04/12/2016   Initial Build --TMS#20160411-00105  by Pramod on 04-12-2016
--//============================================================================							
L_CUST_SQL             VARCHAR2(32000);
l_accural_sql          VARCHAR2(32000);
l_purchase_sql         VARCHAR2(32000);
L_CALENDAR_YEAR_PARAM  VARCHAR2(32000);
L_LOB_PARAM            VARCHAR2(32000);
L_VENDOR_PARAM         VARCHAR2(32000);
L_MVID_PARM            VARCHAR2(32000);
L_CAL_MONTH_PARAM      VARCHAR2(32000);
L_AGREEMENT_YEAR_PARAM VARCHAR2(32000);
 L_CALENDAR_YEAR_PARAMS varchar2(32000);
 L_CAL_MONTH_PARAMS varchar2(32000);
 L_MVID_PARMS VARCHAR2(32000);
 L_LOB_PARAMS varchar2(32000);
 L_AGREEMENT_YEAR_PARAMs varchar2(32000);
  
    type CUSTOMER_REC is RECORD
  (   customer_id 	number,
      party_name 	varchar2(240)
  );
type ACCURAL_REC
IS
  RECORD
  (
    PROCESS_ID         NUMBER,
    AGREEMENT_YEAR     NUMBER,
    MVID               VARCHAR2(150),
    VENDOR_NAME        VARCHAR2(150),
    LOB                VARCHAR2(150),
    BU                 VARCHAR2(150),
    CAL_YEAR           NUMBER,
    CAL_MONTH          VARCHAR2(150),
    CALENDAR_PURCHASES NUMBER,
    ACCRUAL_PURCHASES  NUMBER,
    REBATE             NUMBER,
    COOP               NUMBER,
    TOTAL_ACCRUALS     NUMBER );
    
type purchase_rec is RECORD
    (
	  PROCESS_ID 			NUMBER,
    AGREEMENT_YEAR     	NUMBER,
    MVID               	VARCHAR2(150),
    VENDOR_NAME        	VARCHAR2(150),
    LOB                	VARCHAR2(150),
    BU                 	VARCHAR2(150),
    CAL_YEAR           	NUMBER,
    CAL_MONTH          	VARCHAR2(150),
    CALENDAR_PURCHASES 	NUMBER,
    ACCRUAL_PURCHASES  	NUMBER,
    REBATE             	NUMBER,
    COOP               	NUMBER,
    TOTAL_ACCRUALS     	NUMBER
  );
    
    
	  l_PROCESS_ID 		    	NUMBER;
    l_AGREEMENT_YEAR     	NUMBER;
    l_MVID               	VARCHAR2(150);
    l_VENDOR_NAME        	VARCHAR2(150);
    l_LOB                	VARCHAR2(150);
    l_BU                 	VARCHAR2(150);
    l_CAL_YEAR           	NUMBER;
    l_CAL_MONTH          	VARCHAR2(150);
    l_CALENDAR_PURCHASES 	NUMBER;
    l_ACCRUAL_PURCHASES  	NUMBER;
    l_REBATE             	NUMBER;
    l_COOP               	NUMBER;
    L_TOTAL_ACCRUALS     	NUMBER;
	  L_PARTY_NAME 			    VARCHAR2(150);
	  l_party_id 			     	number;

  
  L_REF_CURSOR1         CURSOR_TYPE4;
  l_ref_cursor2 				CURSOR_TYPE4;
  L_REF_CURSOR3 				CURSOR_TYPE4;
  
	type customer_rec_tab is table of customer_rec Index By Binary_Integer;
	customer_tab customer_rec_tab;
	type accural_REC_tab is table of accural_REC;  
	accural_tab accural_REC_tab  :=  accural_REC_tab();
	type purchase_rec_tab is table of purchase_rec;  
    PURCHASE_TAB PURCHASE_REC_TAB  :=  PURCHASE_REC_TAB();
    
    L_COUNTER NUMBER;
    A_COUNTER NUmber;
  BEGIN
  
  L_COUNTER:=1;
  A_COUNTER:=1;
  
  fnd_file.put_line(fnd_file.log,'Started');
        IF P_Calendar_Year IS NOT NULL THEN  
      L_CALENDAR_YEAR_PARAM:= L_CALENDAR_YEAR_PARAM||'  and CALENDAR_YEAR  in  ('||xxeis.eis_rs_utility.get_param_values(P_Calendar_Year)||' )';
        else
          L_CALENDAR_YEAR_PARAM:=' and 1=1';
    end if;
  
  -- fnd_file.put_line(fnd_file.log,'L_CALENDAR_YEAR_PARAM '||L_CALENDAR_YEAR_PARAM);
  

  
     if p_cal_month is not null then  
      L_Cal_Month_PARAM:= L_Cal_Month_PARAM||'  and (DECODE(CALENDAR_MONTH_ID,1,''Jan'',2,''Feb'',3,''Mar'',4,''Apr'',5,''May'',6,''Jun'',7,''Jul'',8,''Aug'',9,''Sep'',10,''Oct'',11,''Nov'',12,''Dec'')||''-''||CALENDAR_YEAR)  in  ('||xxeis.eis_rs_utility.get_param_values(P_Cal_Month)||' )';
        else
          L_Cal_Month_PARAM:=' and 1=1';
    end if;
    
  --   fnd_file.put_line(fnd_file.log,'P_CAL_MONTH '||P_CAL_MONTH);
    
   if p_mvid is not null then  
        L_Mvid_parm:= L_Mvid_parm||' and MVID   in   ('||xxeis.eis_rs_utility.get_param_values(p_Mvid)||' )';
          else
         L_Mvid_parm:=' and 1=1';
      end if;   
      
     --  fnd_file.put_line(fnd_file.log,'p_mvid '||p_mvid);
    
          IF P_LOB IS NOT NULL THEN   
        L_LOB_PARAM:= L_LOB_PARAM||'  and lob  in  ('||xxeis.eis_rs_utility.get_param_values(P_LOB)||' )';
        else
          L_LOB_PARAM:=' and 1=1';
        end if;
        
   -- fnd_file.put_line(fnd_file.log,'P_LOB '||P_LOB);
    
        IF P_AGREEMENT_YEAR IS NOT NULL THEN  
        L_AGREEMENT_YEAR_PARAM:= L_AGREEMENT_YEAR_PARAM||' and AGREEMENT_YEAR   in   ('||xxeis.eis_rs_utility.get_param_values(P_AGREEMENT_YEAR)||' )';
          else
         L_Agreement_Year_PARAM:=' and 1=1';
      end if;
	  
 --fnd_file.put_line(fnd_file.log,'P_AGREEMENT_YEAR '||P_AGREEMENT_YEAR);
  
  L_CUST_SQL :='select C.CUSTOMER_ID,
                    c.party_name
              from XXCUS.XXCUS_REBATE_CUSTOMERS C
              where C.PARTY_ATTRIBUTE1   =''HDS_MVID''
        ';
			  
    OPEN l_ref_cursor1  FOR l_cust_sql;

       FETCH L_REF_CURSOR1 bulk collect into CUSTOMER_TAB;

	CLOSE L_REF_CURSOR1;
	
--fnd_file.put_line(fnd_file.log,'CUSTOMER_TAB.count '||CUSTOMER_TAB.count);

FOR J IN 1..CUSTOMER_TAB.count
LOOP

 	L_ACCURAL_SQL:=
  'SELECT 
   AGREEMENT_YEAR,
     MVID,
    :party_name,
    LOB,
    BU,
    CALENDAR_YEAR CAL_YEAR,
    DECODE(CALENDAR_MONTH_ID,1,''Jan'',2,''Feb'',3,''Mar'',4,''Apr'',5,''May'',6,''Jun'',7,''Jul'',8,''Aug'',9,''Sep'',10,''Oct'',11,''Nov'',12,''Dec'',null)||''-''|| CALENDAR_YEAR CAL_MONTH,
    0 CALENDAR_PURCHASES,
    sum(case when utilization_type=''ACCRUAL'' then PURCHASES else 0 end) ACCRUAL_PURCHASES,
    SUM(CASE WHEN REBATE_TYPE =''REBATE'' THEN ACCRUAL_AMOUNT ELSE 0 END) REBATE,
    SUM(CASE WHEN REBATE_TYPE =''COOP'' THEN ACCRUAL_AMOUNT ELSE 0 END) COOP,
    SUM(ACCRUAL_AMOUNT) TOTAL_ACCRUALS
  FROM XXCUS.XXCUS_YTD_INCOME_B
	WHERE OFU_CUST_ACCOUNT_ID =:CUST
 '||L_CALENDAR_YEAR_PARAM||'
 '||L_Cal_Month_PARAM||'
 '||L_Mvid_parm||'
 '||L_LOB_PARAM||'
 '||L_AGREEMENT_YEAR_PARAM||'
	GROUP BY
	  AGREEMENT_YEAR,
    MVID,
    :party_name,
    LOB,
    BU,
    CALENDAR_MONTH_ID,
    CALENDAR_YEAR
    ';
    
    
 
	OPEN l_ref_cursor2  FOR l_accural_sql using CUSTOMER_TAB(J).party_name,CUSTOMER_TAB(J).CUSTOMER_ID,CUSTOMER_TAB(J).party_name;
    LOOP
    fetch l_ref_cursor2 into
    l_AGREEMENT_YEAR     ,
    l_MVID               ,
    l_VENDOR_NAME        ,
    l_LOB                ,
    l_BU                 ,
    l_CAL_YEAR           ,
    l_CAL_MONTH          ,
    l_CALENDAR_PURCHASES ,
    l_ACCRUAL_PURCHASES  ,
    l_REBATE             ,
    l_COOP               ,
    l_TOTAL_ACCRUALS   ;  
    

	    accural_tab.extend;
        
accural_tab(l_counter).process_id         := p_process_id;
ACCURAL_TAB(L_COUNTER).AGREEMENT_YEAR     := L_AGREEMENT_YEAR;
ACCURAL_TAB(L_COUNTER).MVID               := L_MVID;
ACCURAL_TAB(L_COUNTER).VENDOR_NAME        := L_VENDOR_NAME;
ACCURAL_TAB(L_COUNTER).LOB                := L_LOB;
accural_tab(L_COUNTER).BU                 := l_BU;
ACCURAL_TAB(L_COUNTER).CAL_YEAR           := L_CAL_YEAR;
accural_tab(L_COUNTER).CAL_MONTH          := l_CAL_MONTH;
accural_tab(L_COUNTER).CALENDAR_PURCHASES := l_CALENDAR_PURCHASES;
ACCURAL_TAB(L_COUNTER).ACCRUAL_PURCHASES  := L_ACCRUAL_PURCHASES;
accural_tab(l_counter).rebate             := l_rebate;
accural_tab(l_counter).coop               := l_coop;
accural_tab(l_counter).total_accruals     := l_total_accruals;
  
		 l_counter    := l_counter+1;
       exit when L_REF_CURSOR2%NOTFOUND;
            end LOOP;
            close L_REF_CURSOR2;
 END LOOP;

--fnd_file.put_line(fnd_file.log,'accural_tab '||accural_tab.count);

 if P_CALENDAR_YEAR is not null then  
      L_CALENDAR_YEAR_PARAMS:= L_CALENDAR_YEAR_PARAMS||'  and to_number(M.CALENDAR_YEAR)  in  ('||xxeis.eis_rs_utility.get_param_values(P_Calendar_Year)||' )';
        else
          L_CALENDAR_YEAR_PARAMS:=' and 1=1';
    end if;
  
     if p_cal_month is not null then  
      L_Cal_Month_PARAMS:= L_Cal_Month_PARAMS||'  and DECODE(SUBSTR(CALENDAR_PERIOD,6,2),1,''Jan'',2,''Feb'',3,''Mar'',4,''Apr'',5,''May'',6,''Jun'',7,''Jul'',8,''Aug'',9,''Sep'',10,''Oct'',11,''Nov'',12,''Dec'')||''-''||CALENDAR_YEAR  in  ('||xxeis.eis_rs_utility.get_param_values(P_Cal_Month)||' )';
        else
          L_Cal_Month_PARAMS:=' and 1=1';
    end if;
    
   if P_MVID is not null then  
        L_Mvid_parmS:= L_Mvid_parmS||' and C.CUSTOMER_ATTRIBUTE2   in   ('||xxeis.eis_rs_utility.get_param_values(p_Mvid)||' )';
          else
         L_Mvid_parmS:=' and 1=1';
      end if;   
    
          if P_LOB is not null then   
        L_LOB_PARAMS:= L_LOB_PARAMS||'  and Z.PARTY_NAME  in  ('||xxeis.eis_rs_utility.get_param_values(P_LOB)||' )';
        else
          L_LOB_PARAMS:=' and 1=1';
        end if;
        
    
    
        if P_AGREEMENT_YEAR is not null then  
        L_AGREEMENT_YEAR_PARAMs:= L_AGREEMENT_YEAR_PARAMs||' and to_number(M.CALENDAR_YEAR)   in   ('||xxeis.eis_rs_utility.get_param_values(P_AGREEMENT_YEAR)||' )';
          else
         L_AGREEMENT_YEAR_PARAMs:=' and 1=1';
      end if;
 
  -- fnd_file.put_line(fnd_file.log,'PURCHASE_TAB');
	
	FOR J IN 1..CUSTOMER_TAB.COUNT  LOOP

 	
	l_purchase_sql:='
		SELECT 
		to_number(M.CALENDAR_YEAR) AGREEMENT_YEAR,
		C.CUSTOMER_ATTRIBUTE2 MVID,
		:party_name VENDOR_name  
		,Z.PARTY_NAME LOB
		,Z1.PARTY_NAME BU
		,to_number(M.CALENDAR_YEAR) CAL_YEAR
		,DECODE(SUBSTR(CALENDAR_PERIOD,6,2),1,''Jan'',2,''Feb'',3,''Mar'',4,''Apr'',5,''May'',6,''Jun'',7,''Jul'',8,''Aug'',9,''Sep'',10,''Oct'',11,''Nov'',12,''Dec'')||''-''||M.CALENDAR_YEAR CAL_MONTH
		,M.TOTAL_PURCHASES CALENDAR_PURCHASES
		,0 ACCRUAL_PURCHASES
		,0 REBATE
		,0 COOP
		,0 TOTAL
 FROM  
        APPS.XXCUSOZF_PURCHASES_MV M
        ,XXCUS.XXCUS_REBATE_CUSTOMERS C
		,APPS.HZ_PARTIES Z
        ,APPS.HZ_PARTIES Z1
        
WHERE 1=1
        AND M.LOB_ID=Z.PARTY_ID
        AND M.MVID=C.CUSTOMER_ID
        and C.CUSTOMER_ID=:CUSTOMER_ID
        AND M.BU_ID=Z1.PARTY_ID
        AND GRP_MVID       = 0
        AND grp_branch     = 1
        AND GRP_QTR        = 1
        AND grp_year       = 1
        AND GRP_LOB        = 0
        AND  GRP_BU_ID     = 0
        AND grp_period     = 1
        AND GRP_CAL_YEAR   = 0
        AND GRP_CAL_PERIOD = 0
		AND Z.ATTRIBUTE1=''HDS_LOB''
        AND Z1.ATTRIBUTE1=''HDS_BU''
 '||L_CALENDAR_YEAR_PARAMS||'
 '||L_Cal_Month_PARAMS||'
 '||L_MVID_PARMS||'
 '||L_LOB_PARAMS||'
 '||L_AGREEMENT_YEAR_PARAMS||'
		';
OPEN l_ref_cursor3  FOR l_purchase_sql using CUSTOMER_TAB(J).party_name,CUSTOMER_TAB(J).CUSTOMER_ID;
    LOOP
    fetch l_ref_cursor3 into
    l_AGREEMENT_YEAR ,
    l_MVID          ,
    l_VENDOR_NAME  ,
    l_LOB   ,
    l_BU   ,
    l_CAL_YEAR  ,
    l_CAL_MONTH  ,
    l_CALENDAR_PURCHASES,
    l_ACCRUAL_PURCHASES ,
    l_REBATE ,
    l_COOP ,
    l_TOTAL_ACCRUALS;  

           PURCHASE_TAB.extend;
        
      PURCHASE_TAB(A_COUNTER).PROCESS_ID         := P_PROCESS_ID;
      PURCHASE_TAB(A_COUNTER).AGREEMENT_YEAR     := L_AGREEMENT_YEAR;
      PURCHASE_TAB(A_COUNTER).MVID               := l_MVID;
      PURCHASE_TAB(A_COUNTER).VENDOR_NAME        := l_VENDOR_NAME;
      PURCHASE_TAB(A_COUNTER).LOB                := l_LOB;
      PURCHASE_TAB(A_COUNTER).BU                 := l_BU;
      PURCHASE_TAB(A_COUNTER).CAL_YEAR           := l_CAL_YEAR;
      PURCHASE_TAB(A_COUNTER).CAL_MONTH          := l_CAL_MONTH;
      PURCHASE_TAB(A_COUNTER).CALENDAR_PURCHASES := l_CALENDAR_PURCHASES;
      PURCHASE_TAB(A_COUNTER).ACCRUAL_PURCHASES  := l_ACCRUAL_PURCHASES;
      PURCHASE_TAB(A_COUNTER).REBATE             := l_REBATE;
      PURCHASE_TAB(A_COUNTER).COOP               := l_COOP;
      PURCHASE_TAB(A_COUNTER).TOTAL_ACCRUALS     := L_TOTAL_ACCRUALS;
      
  		 A_COUNTER    := A_COUNTER+1;
		   exit when L_REF_CURSOR3%NOTFOUND;
            end LOOP;
            close L_REF_CURSOR3;
 END LOOP;

     
      if L_COUNTER  >= 1  then
                  FORALL J IN 1..ACCURAL_TAB.COUNT 
                     INSERT INTO XXEIS.EIS_XXWC_ACCR_LOB_BU_TAB
                          VALUES accural_tab (J);                    
               END IF; 
               commit;
			   
	      if A_COUNTER >= 1  then
                  FORALL J IN 1..PURCHASE_TAB.COUNT 
                     INSERT INTO XXEIS.EIS_XXWC_ACCR_LOB_BU_TAB
                          VALUES PURCHASE_TAB (J);    
                
               end if; 
			   
    COMMIT;      
EXCEPTION WHEN OTHERS THEN
 fnd_file.put_line(fnd_file.log,'THE ERROR IS'||SQLCODE||SQLERRM);
END GET_ACC_LOB_BU_DTLS;
    
PROCEDURE CLEAR_TEMP_TABLES ( P_PROCESS_ID IN NUMBER)
AS
--//============================================================================
--//
--// Object Name         :: clear_temp_tables
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This procedure will trigger in After Report level and delete all the records from the table based on that particular process_id.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Pramod  	04/12/2016   Initial Build --TMS#20160411-00105  by Pramod on 04-12-2016
--//============================================================================  
  BEGIN  
  DELETE FROM XXEIS.EIS_XXWC_ACCR_LOB_BU_TAB WHERE PROCESS_ID=P_PROCESS_ID;
  COMMIT;
END ;  
end EIS_XXWC_PUR_ACC_lob_bu_PKG;
/
