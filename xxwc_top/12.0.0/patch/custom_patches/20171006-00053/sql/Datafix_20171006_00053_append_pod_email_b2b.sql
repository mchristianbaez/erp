/******************************************************************************
   NAME:       Datafix_20171006_00053_append_pod_email_b2b.sql
   PURPOSE:    Data fix script to end date duplicate lines for the same modifier.
   
   REVISIONS:
   Ver        Date        Author               Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        10/10/2017  Niraj K Ranjan   Initial Version TMS#20171006-00053   Script to append email to all customers who have deliver POD_flag as Y
******************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20171006_00053   , Start Update');
   UPDATE xxwc.xxwc_b2b_config_tbl
   SET POD_EMAIL = POD_EMAIL||','||'perry.njuguna@hdsupply.com',
       LAST_UPDATE_DATE = SYSDATE,
       LAST_UPDATED_BY = -1
   WHERE 1=1
   --AND trunc(creation_date) >= trunc(sysdate-30)
   AND DELIVER_POD = 'Y'
   AND POD_EMAIL IS NOT NULL;
   --AND LOWER(POD_EMAIL) NOT LIKE '%perry.njuguna@hdsupply.com%';
   DBMS_OUTPUT.put_line ('TMS: 20171006_00053 Total Records updated : '|| SQL%ROWCOUNT);
   COMMIT;
   DBMS_OUTPUT.put_line ('TMS: 20171006_00053    , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20171006_00053 , Errors : ' || SQLERRM);
END;
/
