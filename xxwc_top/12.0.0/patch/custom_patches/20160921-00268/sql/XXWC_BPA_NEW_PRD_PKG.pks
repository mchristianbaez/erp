CREATE OR REPLACE PACKAGE APPS.XXWC_BPA_NEW_PRD_PKG
AS

   /**************************************************************************
      $Header XXWC_BPA_NEW_PRD_PKG $
      Module Name: XXWC_BPA_NEW_PRD_PKG.pks

      PURPOSE:   This package is called by the concurrent programs
                 XXWC BPA New Product Build Program 
      REVISIONS:
      Ver        Date        Author             Description
      ---------  ----------  ---------------   -------------------------
       1.0       09/29/2016  Neha Saini         Initial Build - Task ID: 20160921-00268
    /*************************************************************************/

   /*************************************************************************
     Procedure : create BPA
     PARAMETERS:  IN - None
                  OUT - errbuf,retcode
     PURPOSE:  This procedure creates file for the concurrent program   XXWC BPA New Product Build Program 
     REVISIONS:
       Ver        Date        Author             Description
     ---------  ----------  ---------------   -------------------------
      1.0       09/29/2016  Neha Saini         Initial Build - Task ID: 20160921-00268
   ************************************************************************/
   PROCEDURE process_bpa (errbuf OUT VARCHAR2, retcode OUT VARCHAR2,p_item_number varchar2);

   PROCEDURE submit_bpa (errbuf OUT VARCHAR2, retcode OUT VARCHAR2,p_request_id IN number);
   
   PROCEDURE write_log (p_debug_msg IN VARCHAR2);   
   
END XXWC_BPA_NEW_PRD_PKG;
/