   /*************************************************************************
     Procedure : insert_prog_XXCUS_MONITOR_CP_B
     PARAMETERS:  IN - None
                  OUT - errbuf,retcode
     PURPOSE:  This script inserts data for  concurrent program  XXWC BPA New Product Build Program
     REVISIONS:
       Ver        Date        Author             Description
     ---------  ----------  ---------------   -------------------------
      1.0       10/11/2017  Neha Saini         Initial Build - Task ID: 20160921-00268
   ************************************************************************/

INSERT INTO XXCUS.XXCUS_MONITOR_CP_B (CONCURRENT_PROGRAM_ID,
                                      EMAIL_WHEN_WARNING,
                                      EMAIL_WHEN_ERROR,
                                      USER_ID,
                                      CREATED_BY,
                                      CREATION_DATE,
                                      LAST_UPDATED_BY,
                                      LAST_UPDATE_DATE,
                                      WARNING_EMAIL_ADDRESS,
                                      ERROR_EMAIL_ADDRESS,
                                      OWNED_BY,
                                      REQUEST_SET_ID,
                                      APPLICATION_ID)
     VALUES (361422,
             'Y',
             'Y',
             15986,
             15837,
             TO_DATE ('01/09/2015 05:58:51', 'MM/DD/YYYY HH24:MI:SS'),
             16991,
             TO_DATE ('07/22/2016 17:27:59', 'MM/DD/YYYY HH24:MI:SS'),
             'WC-ITBSAAlerts-U1@HDSupply.com',
             'WC-ITBSAAlerts-U1@HDSupply.com',
             'SALES AND FULFILLMENT',
             NULL,
             20005);

COMMIT;