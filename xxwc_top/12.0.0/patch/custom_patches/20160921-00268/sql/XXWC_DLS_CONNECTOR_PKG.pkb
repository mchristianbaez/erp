CREATE OR REPLACE PACKAGE BODY DLS.XXWC_DLS_CONNECTOR_PKG
AS
   g_dlsjobid   NUMBER;

   /***************************************************************************************************************************************
        $Header XXWC_DLS_CONNECTOR_PKG $
        Module Name: XXWC_DLS_CONNECTOR_PKG.pkb

        PURPOSE:    Extension of DLS Connector PUB

        REVISIONS:
        Ver        Date        Author                     Description
        ---------  ----------  ---------------         ------------------------------------------------------------------------------------
        1.0        11/01/2014  RK                      Initial Version TMS # 20141110-00037 - Mass Item Import Tool

        1.1        03/02/2015  Vamshidhar              TMS#20150226-00196 -- Added new function (is_category_class_valid)
                                                       to package to validate Category Class

       1.2         19/03/2015  P.Vamshidhar            TMS#20150317-00079 - EDQP- Add check validation of Item in Interface table.
                                                       Added additional validation to check item exist in Item interface.

       1.3         07/04/2015  P.Vamshidhar            TMS#20150223-00026 - Improve exception error handling in EDQP

       1.4         09/04/2015  P.Vamshidhar            TMS#20150310-00048-EDQP - Mass Item Uploads showing Interface User
                                                       Added function importjobbatches and
                                                       procedures importbatch and logmessage

       1.5         10/08/2015  P.Vamshidhar            TMS#20150817-00105  - EDQP Bundle changes
	   
	   1.6         10/30/2017  P.Vamshidhar            TMS#20160921-00268 - BPA Enhancements - New Product Build Ins

    *************************************************************************************************************************************/

   /*************************************************************************************************************************************
      Procedure : get_taxware_codes

     PURPOSE:   Retreive all the  Taxware codes
     Parameter:
            IN
                x_taxware_codes_tbl
                x_return_status
                x_msg_count
                x_msg_data

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------
      1.0        11/01/2014  RK                      Initial Version TMS # 20141110-00037 - Mass Item Import Tool

    *************************************************************************************************************************************/

   PROCEDURE get_taxware_codes (
      x_taxware_codes_tbl      OUT NOCOPY xxwc_dls_connector_pkg.lookup_tbl_type,
      x_return_status          OUT NOCOPY VARCHAR2,
      x_msg_count              OUT NOCOPY NUMBER,
      x_msg_data               OUT NOCOPY VARCHAR2)
   IS
      CURSOR cur_tax
      IS
         SELECT ffv.flex_value, ffvt.description
           FROM apps.fnd_flex_value_sets ffvs,
                apps.fnd_flex_values ffv,
                apps.fnd_flex_values_tl ffvt
          WHERE     ffv.flex_value_id = ffvt.flex_value_id
                AND ffvt.language = USERENV ('LANG')
                AND ffvs.flex_value_set_id = ffv.flex_value_set_id
                AND ffvs.flex_value_set_name = 'XXWC TAXWARE CODE'
                AND ffv.enabled_flag = 'Y';

      l_taxware_code_tbl   xxwc_dls_connector_pkg.lookup_tbl_type;
   BEGIN
      x_return_status := 'S';

      OPEN cur_tax;

      FETCH cur_tax BULK COLLECT INTO l_taxware_code_tbl;

      CLOSE cur_tax;

      x_taxware_codes_tbl := l_taxware_code_tbl;
   EXCEPTION
      WHEN OTHERS
      THEN
         x_return_status := 'E';
         x_msg_data := SQLCODE || SQLERRM;
         x_msg_count := 1;
   END get_taxware_codes;


   /*************************************************************************************************************************************
        Procedure : get_country_codes

       PURPOSE:   Retreive all the  Country  codes
       Parameter:
              IN
                  x_country_codes_tbl
                  x_return_status
                  x_msg_count
                  x_msg_data

        REVISIONS:
        Ver        Date        Author                     Description
        ---------  ----------  ---------------         -------------------------
        1.0        11/01/2014  RK                      Initial Version TMS # 20141110-00037 - Mass Item Import Tool

    *************************************************************************************************************************************/

   PROCEDURE get_country_codes (
      x_country_codes_tbl      OUT NOCOPY xxwc_dls_connector_pkg.lookup_tbl_type,
      x_return_status          OUT NOCOPY VARCHAR2,
      x_msg_count              OUT NOCOPY NUMBER,
      x_msg_data               OUT NOCOPY VARCHAR2)
   IS
      CURSOR cur_country
      IS
         SELECT lookup_code, meaning
           FROM apps.fnd_lookup_values
          WHERE     enabled_flag = 'Y'
                AND language = USERENV ('LANG')
                AND lookup_type = 'XXWC_TERRITORIES'
                AND security_group_id = 0
                AND view_application_id = 700
                AND start_date_active <= TRUNC (SYSDATE)
                AND NVL (end_date_active, TRUNC (SYSDATE)) >= TRUNC (SYSDATE);

      l_country_code_tbl   xxwc_dls_connector_pkg.lookup_tbl_type;
   BEGIN
      x_return_status := 'S';

      OPEN cur_country;

      FETCH cur_country BULK COLLECT INTO l_country_code_tbl;

      CLOSE cur_country;

      x_country_codes_tbl := l_country_code_tbl;
   EXCEPTION
      WHEN OTHERS
      THEN
         x_return_status := 'E';
         x_msg_data := SQLCODE || SQLERRM;
         x_msg_count := 1;
   END get_country_codes;


   /*************************************************************************
        Procedure : get_vendor_codes

       PURPOSE:   Retreive all the  Vendor  codes
       Parameter:
              IN
                  x_Vendor_codes_tbl
                  x_return_status
                  x_msg_count
                  x_msg_data
        REVISIONS:
        Ver        Date        Author                     Description
        ---------  ----------  ---------------         -------------------------
        1.0        11/01/2014  RK                 Initial Version TMS # 20141110-00037 -

     ************************************************************************/

   PROCEDURE get_vendor_codes (
      x_vendor_codes_tbl      OUT NOCOPY xxwc_dls_connector_pkg.lookup_tbl_type,
      x_return_status         OUT NOCOPY VARCHAR2,
      x_msg_count             OUT NOCOPY NUMBER,
      x_msg_data              OUT NOCOPY VARCHAR2)
   IS
      CURSOR cur_vendor
      IS
         SELECT DISTINCT aps.segment1, aps.segment1 || '-' || aps.vendor_name
           FROM apps.ap_suppliers aps
          WHERE     aps.enabled_flag = 'Y'
                AND aps.start_date_active <= SYSDATE
                AND NVL (aps.end_date_active, SYSDATE) >= SYSDATE;

      l_vendor_code_tbl   xxwc_dls_connector_pkg.lookup_tbl_type;
   BEGIN
      x_return_status := 'S';

      OPEN cur_vendor;

      FETCH cur_vendor BULK COLLECT INTO l_vendor_code_tbl;

      CLOSE cur_vendor;

      x_vendor_codes_tbl := l_vendor_code_tbl;
   EXCEPTION
      WHEN OTHERS
      THEN
         x_return_status := 'E';
         x_msg_data := SQLCODE || SQLERRM;
         x_msg_count := 1;
   END get_vendor_codes;


   /**********************************************************************************************************************
        Function : is_item_number_valid

       PURPOSE:   validate item number
       Parameter:
              IN
                  p_item_number
                  p_organization_id
       REVISIONS:
       Ver        Date        Author                     Description
       ---------  ----------  ---------------    -----------------------------------------------------------------------
       1.0        11/01/2014  RK                 Initial Version TMS # 20141110-00037 -
       1.2        19/03/2015  P.Vamshidhar       TMS#20150317-00079 - EDQP- Add check validation of Item in Interface table.
                                                 Added additional validation to check item exist in Item interface.

       1.3        07/04/2015  P.Vamshidhar       TMS#20150223-00026--To track exceptions added item number to return parameters.
     *************************************************************************************************************************/



   FUNCTION is_item_number_valid (p_item_number       IN VARCHAR2,
                                  p_organization_id   IN NUMBER)
      RETURN VARCHAR2
   IS
      --      l_exist   VARCHAR2 (100);   commented by Vamshi in 1.2 Version -  TMS#20150317-00079
      l_count   NUMBER; -- Added by Vamshi in 1.2 Version -  TMS#20150317-00079
   BEGIN
      IF (p_item_number IS NULL)
      THEN
         RETURN p_item_number;
      END IF;

      /*  -- Commented below code and wrote in new way. --  by Vamshi in 1.2 Version -  TMS#20150317-00079
            SELECT segment1
              INTO l_exist
              FROM apps.mtl_system_items_b
             WHERE segment1 = p_item_number AND organization_id = p_organization_id;
            RETURN 'Y';
      */

      -- Added below code by Vamshi in 1.2 Version -  TMS#20150317-00079 -- Start

      SELECT COUNT (1)
        INTO l_count
        FROM apps.mtl_system_items_b
       WHERE segment1 = p_item_number AND organization_id = p_organization_id;

      IF l_count = 0
      THEN
         SELECT COUNT (1)
           INTO l_count
           FROM apps.mtl_system_items_interface
          WHERE     segment1 = p_item_number
                AND organization_id = p_organization_id
                AND process_flag = 7;
      END IF;

      IF l_count > 0
      THEN
         RETURN TRIM ('Y-' || p_item_number); -- Added by Vamshi - TMS#20150223-00026 - V 1.3
      ELSE
         RETURN p_item_number;
      END IF;
   -- Added Above code by Vamshi in 1.2 Version -  TMS#20150317-00079 -- End

   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN p_item_number;
   END is_item_number_valid;


   /***************************************************************************************************************************
        Function : is_icc_valid

       PURPOSE:   validate icc code
       Parameter:
              IN
                  p_icc_name
       REVISIONS:
       Ver        Date        Author                     Description
       ---------  ----------  ---------------         -------------------------
       1.0        11/01/2014  RK                 Initial Version TMS # 20141110-00037 -
       1.3        07/04/2015  P.Vamshidhar       TMS#20150223-00026--To track exceptions added icc_value to return parameters.

     ***************************************************************************************************************************/


   FUNCTION is_icc_valid (p_icc_name IN VARCHAR2)
      RETURN VARCHAR2
   IS
      l_exist   VARCHAR2 (100);
   BEGIN
      IF (p_icc_name IS NULL)
      THEN
         RETURN p_icc_name;
      END IF;


      SELECT segment1
        INTO l_exist
        FROM apps.mtl_item_catalog_groups_b
       WHERE segment1 = p_icc_name;

      RETURN l_exist;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN TRIM ('N-' || p_icc_name); -- TMS#20150223-00026 -- Changed by Vamshi in 1.3
   END is_icc_valid;


   /*************************************************************************
        Function : is_uom_valid

       PURPOSE:   validate uom code
       Parameter:
              IN
                  p_primary_uom_code
      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------
      1.0        11/01/2014  RK                 Initial Version TMS # 20141110-00037 -
      1.3        07/04/2015  P.Vamshidhar       TMS#20150223-00026--To track exceptions added UOM value to return parameters.

     ************************************************************************/

   FUNCTION is_uom_valid (p_primary_uom_code IN VARCHAR2)
      RETURN VARCHAR2
   IS
      l_exist   VARCHAR2 (100);
   BEGIN
      IF (p_primary_uom_code IS NULL)
      THEN
         RETURN p_primary_uom_code;
      END IF;

      SELECT unit_of_measure_tl
        INTO l_exist
        FROM apps.mtl_units_of_measure_tl
       WHERE unit_of_measure_tl = p_primary_uom_code AND ROWNUM = 1;

      RETURN l_exist;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN ('N-' || p_primary_uom_code); -- -- TMS#20150223-00026 -- Changed by Vamshi in 1.3
   END is_uom_valid;



   /*************************************************************************
        Function : is_acc_valid

       PURPOSE:   validate alternate catalog category
       Parameter:
              IN
                  p_acc_name
     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        11/01/2014  RK                 Initial Version TMS # 20141110-00037 -
     ************************************************************************/

   FUNCTION is_acc_valid (p_acc_name IN VARCHAR2)
      RETURN VARCHAR2
   IS
      l_exist   VARCHAR2 (100);
   BEGIN
      IF (p_acc_name IS NULL)
      THEN
         RETURN p_acc_name;
      END IF;


      SELECT concatenated_segments
        INTO l_exist
        FROM apps.mtl_categories_kfv
       WHERE concatenated_segments = p_acc_name AND ROWNUM = 1;

      RETURN l_exist;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 'N';
   END is_acc_valid;



   /*************************************************************************
        Function : is_country_code_valid

       PURPOSE:   validate country code
       Parameter:
              IN
                  p_country_code
     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        11/01/2014  RK                 Initial Version TMS # 20141110-00037 -
     1.3        07/04/2015  P.Vamshidhar       TMS#20150223-00026--To track exceptions added country code value to return parameters.

     ************************************************************************/

   FUNCTION is_country_code_valid (p_country_code IN VARCHAR2)
      RETURN VARCHAR2
   IS
      l_exist   VARCHAR2 (100);
   BEGIN
      IF (p_country_code IS NULL)
      THEN
         RETURN p_country_code;
      END IF;

      SELECT meaning
        INTO l_exist
        FROM apps.fnd_lookup_values
       WHERE     enabled_flag = 'Y'
             AND language = USERENV ('LANG')
             AND lookup_type = 'XXWC_TERRITORIES'
             AND security_group_id = 0
             AND view_application_id = 700
             AND start_date_active <= TRUNC (SYSDATE)
             AND NVL (end_date_active, TRUNC (SYSDATE)) >= TRUNC (SYSDATE)
             AND MEANING = p_country_code;

      RETURN l_exist;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN ('N-' || p_country_code); -- changed by Vamshi in 1.3 -- TMS#20150223-00026
   END is_country_code_valid;


   /*************************************************************************************************
          PROCEDURE : process_xref

         PURPOSE:   Process the xref
         Parameter:
                IN
                    p_item_number
                    p_organization_id
                    p_upc_xref_type
                    p_upc_xref_value
                    p_upc_ref_trxn_type
                    p_upc_xref_vendor    -- Added in 1.5 Rev by Vamshi
            INOUT   x_return_status
      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------
      1.0        11/01/2014  RK                      Initial Version TMS # 20141110-00037
      1.5        10/08/2015  P.Vamshidhar            TMS#20150817-00105  - EDQP Bundle changes

       *********************************************************************************************/

   PROCEDURE process_xref (p_item_number         IN            VARCHAR2,
                           p_organization_id     IN            NUMBER,
                           p_upc_xref_type       IN            VARCHAR2,
                           p_upc_xref_value      IN            VARCHAR2,
                           p_upc_ref_trxn_type   IN            VARCHAR2,
                           p_upc_xref_vendor     IN            VARCHAR2, -- Added in 1.5 Rev by Vamshi
                           x_return_status       IN OUT NOCOPY VARCHAR2)
   AS
      l_item_id               NUMBER;
      l_upc_code              VARCHAR2 (255);
      l_xref_type             VARCHAR2 (25);
      l_xref_id               NUMBER;
      l_xref_tbl              apps.mtl_cross_references_pub.xref_tbl_type;
      l_message_list          error_handler.error_tbl_type;

      l_module                VARCHAR2 (100) := 'process_xref';
      l_err_msg               VARCHAR2 (4000);
      l_api_error_msg         VARCHAR2 (4000);
      l_err_callfrom          VARCHAR2 (175) := 'XXWC_DLS_CONNECTOR_PKG';
      l_err_callpoint         VARCHAR2 (175) := 'START';
      l_distro_list           VARCHAR2 (80)
                                 := 'OracleDevelopmentGroup@hdsupply.com';
      l_display_msg           VARCHAR2 (4000) := NULL;

      l_upc_validation        VARCHAR2 (1) := 'Y';
      l_mst_vend_validation   VARCHAR2 (1) := 'Y';

      l_xref_count            NUMBER := 0;
      l_mst_xref_id           NUMBER;
      l_upc_xref_id           NUMBER;
      lx_msg_count            NUMBER;
      l_called_step           VARCHAR2 (100);
      lx_msg_data             VARCHAR2 (1000);
   BEGIN
      x_return_status := 'S';
      -- validate inventory item number
      l_called_step := '600';

      BEGIN
         SELECT inventory_item_id
           INTO l_item_id
           FROM apps.mtl_system_items_b
          WHERE     organization_id = p_organization_id
                AND segment1 = p_item_number;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            x_return_status := 'E';
      --x_msg_data     := l_display_msg || ' Item ' || p_item_number || ' is not valid.';
      END;

      l_called_step := '601';

      IF (p_upc_ref_trxn_type IN ('UPDATE', 'DELETE'))
      THEN
         BEGIN
            SELECT cross_reference_id
              INTO l_upc_xref_id
              FROM apps.mtl_cross_references_b
             WHERE     cross_reference_type = p_upc_xref_type
                   AND inventory_item_id = l_item_id
                   AND cross_reference = p_upc_xref_value;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               x_return_status := 'E';
               l_upc_validation := 'N';
         END;
      END IF;

      l_called_step := '602';

      IF (l_upc_validation = 'Y')
      THEN
         l_xref_count := l_xref_count + 1;
         l_xref_tbl (l_xref_count).inventory_item_id := l_item_id;
         l_xref_tbl (l_xref_count).cross_reference_type := p_upc_xref_type;
         l_xref_tbl (l_xref_count).cross_reference := p_upc_xref_value;
         l_xref_tbl (l_xref_count).transaction_type := p_upc_ref_trxn_type;
         l_xref_tbl (l_xref_count).cross_reference_id := l_upc_xref_id;
         l_xref_tbl (l_xref_count).last_update_date := SYSDATE;
         l_xref_tbl (l_xref_count).org_independent_flag := 'Y';

         l_xref_tbl (l_xref_count).last_updated_by := fnd_global.user_id;
         l_xref_tbl (l_xref_count).creation_date := SYSDATE;
         l_xref_tbl (l_xref_count).created_by := fnd_global.user_id;
         l_xref_tbl (l_xref_count).last_update_login := fnd_global.login_id;
         l_xref_tbl (l_xref_count).ATTRIBUTE1 := p_upc_xref_vendor; -- Added by Vamshi in 1.5

         l_called_step := '603';
      END IF;

      IF (l_xref_count > 0)
      THEN
         l_called_step := '604';

         apps.mtl_cross_references_pub.process_xref (
            p_api_version     => 1.0,
            p_init_msg_list   => fnd_api.g_true,
            p_commit          => fnd_api.g_false,
            p_xref_tbl        => l_xref_tbl,
            x_return_status   => x_return_status,
            x_msg_count       => lx_msg_count,
            x_message_list    => l_message_list);
         l_called_step := '605';

         IF x_return_status <> 'S'
         THEN
            FOR ndx IN 1 .. l_message_list.COUNT
            LOOP
               l_api_error_msg :=
                     l_api_error_msg
                  || ', '
                  || l_message_list (ndx).MESSAGE_TEXT;
            END LOOP;
         END IF;

         COMMIT;
      END IF;

      l_called_step := '606';
   EXCEPTION
      WHEN OTHERS
      THEN
         x_return_status := 'E';
         lx_msg_data := SQLCODE || SQLERRM;

         apps.xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_DLS_CONNECTOR_PKG.process_xref',
            p_calling             => l_called_step,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (SQLERRM, 1, 2000),
            p_error_desc          => l_api_error_msg || lx_msg_data,
            p_distribution_list   => l_distro_list,
            p_module              => 'process_xref');
   END process_xref;

   /*************************************************************************
             Function : process_xref

            PURPOSE:   Process the xref
            Parameter:
                   IN
                       p_item_number
                       p_organization_id
                       p_upc_xref_type
                       p_upc_xref_value
                       p_upc_ref_trxn_type
                       p_upc_xref_vendor    -- Added in 1.5 Rev by Vamshi

         REVISIONS:
         Ver        Date        Author                     Description
         ---------  ----------  ---------------         -------------------------
         1.0        11/01/2014  RK                      Initial Version TMS # 20141110-00037 -
         1.5        10/08/2015  P.Vamshidhar            TMS#20150817-00105  - EDQP Bundle changes

   ************************************************************************/


   FUNCTION process_xref (p_item_number         IN VARCHAR2,
                          p_organization_id     IN NUMBER,
                          p_upc_xref_type       IN VARCHAR2,
                          p_upc_xref_value      IN VARCHAR2,
                          p_upc_ref_trxn_type   IN VARCHAR2,
                          p_upc_xref_vendor     IN VARCHAR2 -- Added in 1.5 Rev by Vamshi
                                                           )
      RETURN VARCHAR2
   IS
      lx_return_status   VARCHAR2 (100);
      l_called_step      VARCHAR2 (100);
      l_distro_list      VARCHAR2 (80)
                            := 'OracleDevelopmentGroup@hdsupply.com';
   BEGIN
      l_called_step := '701';

      process_xref (p_item_number         => p_item_number,
                    p_organization_id     => p_organization_id,
                    p_upc_xref_type       => p_upc_xref_type,
                    p_upc_xref_value      => p_upc_xref_value,
                    p_upc_ref_trxn_type   => p_upc_ref_trxn_type,
                    p_upc_xref_vendor     => p_upc_xref_vendor, -- Added by Vamshi in Rev 1.5
                    x_return_status       => lx_return_status);

      l_called_step := '702';

      RETURN lx_return_status;
   EXCEPTION
      WHEN OTHERS
      THEN
         apps.xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_DLS_CONNECTOR_PKG.process_xref',
            p_calling             => l_called_step,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (SQLERRM, 1, 2000),
            p_error_desc          => 'Error in Process Xref procedure, When Others Exception',
            p_distribution_list   => l_distro_list,
            p_module              => 'process_xref');
         RETURN 'E';
   END process_xref;


   /*************************************************************************
        Function : is_category_class_valid

       PURPOSE:   To validate Category Class
       Parameter:
              IN p_cat_class   -- (This value will be received from DSA).
       REVISIONS:
       Ver        Date        Author                     Description
       ---------  ----------  ---------------         -------------------------
       1.1        03/02/2015  Vamshidhar              Initial Version TMS#20150226-00196
       1.3        07/04/2015  P.Vamshidhar            TMS#20150223-00026--To track exceptions added cat class value to return parameters.

     ************************************************************************/

   FUNCTION is_category_class_valid (p_cat_class IN VARCHAR2)
      RETURN VARCHAR2
   IS
      l_exist   VARCHAR2 (100);
   BEGIN
      IF (p_cat_class IS NULL)
      THEN
         RETURN p_cat_class;
      END IF;

      SELECT TRIM (segment1 || '.' || segment2)
        INTO l_exist
        FROM apps.mtl_categories
       WHERE TRIM (segment1 || '.' || segment2) = TRIM (p_cat_class);

      RETURN l_exist;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN ('N-' || p_cat_class); -- changed by Vamshi in 1.3 -- TMS#20150223-00026
   END is_category_class_valid;



   /***********************************************************************************************************************
      Procedure : importjobbatches

     PURPOSE:   To import batches
     Parameter:
            IN
                (p_user_id
                p_job_id)


      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         --------------------------------------------------------------------
      1.4        09/04/2015  P.Vamshidhar            TMS#20150310-00048-EDQP - Mass Item Uploads showing Interface User

   **********************************************************************************************************************/

   FUNCTION importjobbatches (p_user_id IN VARCHAR2, p_job_id IN VARCHAR2)
      RETURN VARCHAR2
   IS
      CURSOR c_batchlist
      IS
         SELECT batch_id
           FROM dls_job_batches
          WHERE job_id = p_job_id;
   BEGIN
      FOR rec IN c_batchlist
      LOOP
         importbatch (p_user_id   => p_user_id,
                      p_jobid     => p_job_id,
                      p_batchid   => rec.batch_id);
      END LOOP;

      RETURN 'S';
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 'E';
   END;



   /*************************************************************************************************************************
      Procedure : importbatch

     PURPOSE:   To import batches
     Parameter:
            IN
                (p_user_id
                 p_jobid,
                 p_batchid)


      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------------------------------------------------
      1.4        09/04/2015  P.Vamshidhar            TMS#20150310-00048-EDQP - Mass Item Uploads showing Interface User
	  
      1.6        10/30/2017  P.Vamshidhar            TMS#20160921-00268 - BPA Enhancements - New Product Build Ins
   *************************************************************************************************************************/

   PROCEDURE importbatch (p_user_id   IN VARCHAR2,
                          p_jobid     IN NUMBER,
                          p_batchid   IN INTEGER)
   IS
      v_rc            NUMBER;
      err_msg         VARCHAR2 (2000);

      l_resp_id       FND_RESPONSIBILITY.RESPONSIBILITY_ID%TYPE;
      l_user_id       FND_USER.USER_ID%TYPE;
      l_appl_id       FND_APPLICATION.APPLICATION_ID%TYPE;
      lv_request_id   NUMBER;

      --g_dlsjobid   NUMBER;
      CURSOR c1 (p_request_id NUMBER)
      IS
         SELECT segment1
           FROM apps.MTL_SYSTEM_ITEMS_INTERFACE
          WHERE request_id = p_request_id;
   BEGIN
      g_dlsjobid := p_jobid;
      fnd_message.clear;

      BEGIN
         SELECT user_id
           INTO l_user_id
           FROM apps.fnd_user
          WHERE user_name = UPPER (p_user_id);
      EXCEPTION
         WHEN OTHERS
         THEN
            SELECT user_id
              INTO l_user_id
              FROM apps.fnd_user
             WHERE user_name = 'DLSMANAGER';
      END;

      SELECT APPLICATION_ID
        INTO l_appl_id
        FROM apps.fnd_application
       WHERE APPLICATION_SHORT_NAME = 'EGO';

      BEGIN
         SELECT RESPONSIBILITY_ID
           INTO l_resp_id
           FROM apps.fnd_responsibility
          WHERE responsibility_key = 'XXWC_EGO_DEV_MANAGER_USER';
      EXCEPTION
         WHEN OTHERS
         THEN
            SELECT user_id
              INTO l_user_id
              FROM apps.fnd_user
             WHERE user_name = 'DLSMANAGER';

            SELECT RESPONSIBILITY_ID
              INTO l_resp_id
              FROM apps.fnd_responsibility
             WHERE responsibility_key = 'EGO_PIM_DATA_LIBRARIAN ';
      END;


      FND_GLOBAL.APPS_INITIALIZE (l_user_id, l_resp_id, l_appl_id);

      v_rc :=
         fnd_request.submit_request (application   => 'EGO',
                                     program       => 'EGOIJAVA',
                                     sub_request   => FALSE,
                                     argument1     => NULL,
                                     --Result Format Usage Id (Non-Required numeric value set)
                                     argument2     => fnd_global.user_id,
                                     --User Id (Non-Required numeric value set)
                                     argument3     => 'US',
                                     --Language (value set FND_LANGUAGES, Language Codes)
                                     argument4     => fnd_global.resp_id,
                                     --Responsibility Id(Non-Required numeric value set)
                                     argument5     => fnd_global.prog_appl_id,
                                     --Application Id(Non-Required numeric value set)
                                     argument6     => 3,
                                     --Run From (Integers Between 1 and 9)
                                     argument7     => 'N',
                                     --Create New Batch (EGO_YES_NO)
                                     argument8     => p_batchid,
                                     --Batch Id (Non-Required numeric value set)
                                     argument9     => '',
                                     --Batch Name (240 characters, unvalidated)
                                     argument10    => 'N',
                                     --Auto Import On Data Load (EGO_YES_NO)
                                     argument11    => 'N',
                                     --Auto Match On Data Load (EGO_YES_NO)
                                     argument12    => 'N',
                                     --Change Order Option (EGO_YES_NO)
                                     argument13    => 'Y',
                                     --Add All Imported Items To CO (EGO_YES_NO)
                                     argument14    => '',
                                     --Change Order Category (240 characters, unvalidated)
                                     argument15    => '',
                                     --Change Order Type (240 characters, unvalidated)
                                     argument16    => '',
                                     --Change Order Name (240 characters, unvalidated)
                                     argument17    => '',
                                     --Change Order Number (240 characters, unvalidated)
                                     argument18    => '' --Change Order Description (240 characters, unvalidated)
                                                        );

      IF v_rc = 0
      THEN
         fnd_message.retrieve (err_msg);
         logmessage (
            p_text          => err_msg,
            p_log_level     => fnd_log.level_unexpected,
            p_module_name   => 'XXWC_DLS_CONNECTOR_PKG' || '.ImportBatch');
      ELSE
         COMMIT;
		 
        -- Added below code in Rev 1.6
		
         BEGIN
            lv_request_id :=
               fnd_request.submit_request (
                  application   => 'XXWC',
                  program       => 'XXWC_INV_BPA_PRG_SUBMIT',
                  description   => NULL,
                  start_time    => TO_CHAR (SYSDATE + 5 / 1440,
                                            'DD-MON-YYYY HH24:MI:SS'), -- Start Time
                  sub_request   => FALSE,
                  argument1     => v_rc);
            COMMIT;
         END;

        -- Added above code in Rev 1.6

         logmessage (
            p_text          =>    'Import request ID -> '
                               || v_rc
                               || ' issued for batch id -> '
                               || p_batchid,
            p_log_level     => fnd_log.level_procedure,
            p_module_name   => 'XXWC_DLS_CONNECTOR_PKG' || '.ImportBatch');
      END IF;
   END importbatch;


   /***************************************************************************************************************************************
   -- PROCEDURE: LogMessage
   -- REVISIONS:
   --   Ver        Date        Author                     Description
   -- ---------  ----------  ---------------         -------------------------
   --  1.4       09/04/2015  P.Vamshidhar            TMS#20150310-00048-EDQP - Mass Item Uploads showing Interface User
   --                                                Description : Write a message to the PIM output log using FND_LOG
   -- |
    *************************************************************************************************************************************/

   PROCEDURE logmessage (
      p_text          IN VARCHAR2,
      p_log_level     IN NUMBER DEFAULT fnd_log.level_statement,
      p_module_name   IN VARCHAR2 DEFAULT NULL)
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
      lc_module_name   VARCHAR2 (250);
   BEGIN
      lc_module_name := 'dls.' || p_module_name;
      fnd_log.string (p_log_level, lc_module_name, p_text);

      IF    p_log_level = fnd_log.level_unexpected
         OR p_log_level = fnd_log.level_procedure
      THEN
         INSERT INTO dls_job_messages (job_id, module_name, MESSAGE)
              VALUES (g_dlsjobid, p_module_name, p_text);

         COMMIT;
      END IF;
   END logmessage;
END XXWC_DLS_CONNECTOR_PKG;
/
