
/*************************************************************************
  $Header TMS_20160418-00177 _Interface_Trip_Stop.sql $
  Module Name: TMS 20160418-00177     Update the Unprocesed Shipping Transactions
                for sales order # 19984745 

  PURPOSE: Data fix to update the Devliery Details

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        18-APR-2016  ANITHA MANI         TMS#20160418-00177 

**************************************************************************/ 


SET SERVEROUTPUT ON SIZE 100000;
SET VERIFY OFF;

DECLARE
BEGIN

DBMS_OUTPUT.put_line ('TMS: 20160418-00177     , Before Update');

UPDATE apps.wsh_delivery_details
SET released_status = 'D',
src_requested_quantity = 0,
requested_quantity = 0,
shipped_quantity = 0,
cycle_count_quantity = 0,
cancelled_quantity = 0,
subinventory = null,
locator_id = null,
lot_number = null,
revision = null,
inv_interfaced_flag = 'X',
oe_interfaced_flag = 'X'
WHERE delivery_detail_id = 14950598;--Pass the delivery detail ID


DBMS_OUTPUT.put_line (
         'TMS: 20160418-00177    - number of records updated: '
      || SQL%ROWCOUNT);


update apps.wsh_delivery_assignments
set delivery_id = null,
parent_delivery_detail_id = null
where delivery_detail_id = 14950598;   --Pass the delivery detail ID



DBMS_OUTPUT.put_line (
         'TMS: 20160418-00177    - number of records updated: '
      || SQL%ROWCOUNT);

COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20160418-00177   , End Update');
   
END;
/   