--Report Name            : White Cap End of Month Commissions Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating View Data for White Cap End of Month Commissions Report
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_AR_TBD_OUTBND_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_AR_TBD_OUTBND_V',222,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Ar Tbd Outbound V','EXATOV','','');
--Delete View Columns for EIS_XXWC_AR_TBD_OUTBND_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_AR_TBD_OUTBND_V',222,FALSE);
--Inserting View Columns for EIS_XXWC_AR_TBD_OUTBND_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','OPERATING_UNIT',222,'Operating Unit','OPERATING_UNIT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Operating Unit','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','AVERAGECOST',222,'Averagecost','AVERAGECOST','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Averagecost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','DIRECTFLAG',222,'Directflag','DIRECTFLAG','','','','XXEIS_RS_ADMIN','NUMBER','','','Directflag','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','CATCLASS',222,'Catclass','CATCLASS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Catclass','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','PARTNO',222,'Partno','PARTNO','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Partno','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','REGIONNAME',222,'Regionname','REGIONNAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Regionname','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','LOC',222,'Loc','LOC','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Loc','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','SALESREPNAME',222,'Salesrepname','SALESREPNAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Salesrepname','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','SALESREPNUMBER',222,'Salesrepnumber','SALESREPNUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Salesrepnumber','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','CUSTOMERNAME',222,'Customername','CUSTOMERNAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customername','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','JOBNUMBER',222,'Jobnumber','JOBNUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Jobnumber','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','MASTERNAME',222,'Mastername','MASTERNAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Mastername','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','MASTERNUMBER',222,'Masternumber','MASTERNUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Masternumber','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','EXTSALE',222,'Extsale','EXTSALE','','','','XXEIS_RS_ADMIN','NUMBER','','','Extsale','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','UNITPRICE',222,'Unitprice','UNITPRICE','','','','XXEIS_RS_ADMIN','NUMBER','','','Unitprice','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','QTY',222,'Qty','QTY','','','','XXEIS_RS_ADMIN','NUMBER','','','Qty','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','DESCRIPTION',222,'Description','DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','LINENO',222,'Lineno','LINENO','','','','XXEIS_RS_ADMIN','NUMBER','','','Lineno','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','INVOICENUMBER',222,'Invoicenumber','INVOICENUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Invoicenumber','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','BUSINESSDATE',222,'Businessdate','BUSINESSDATE','','','','XXEIS_RS_ADMIN','DATE','','','Businessdate','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','TRADER',222,'Trader','TRADER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Trader','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','INVOICE_SOURCE',222,'Invoice Source','INVOICE_SOURCE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Invoice Source','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','REPORT_TYPE',222,'Report Type','REPORT_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Report Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','PERIOD_NAME',222,'Period Name','PERIOD_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Period Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','SHIPPED_QUANTITY',222,'Shipped Quantity','SHIPPED_QUANTITY','','','','XXEIS_RS_ADMIN','NUMBER','','','Shipped Quantity','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','ORDER_NUMBER',222,'Order Number','ORDER_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Order Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','MFGADJUST',222,'Mfgadjust','MFGADJUST','','','','XXEIS_RS_ADMIN','NUMBER','','','Mfgadjust','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','CUSTOMER_TRX_ID',222,'Customer Trx Id','CUSTOMER_TRX_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Customer Trx Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','ORG_ID',222,'Org Id','ORG_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Org Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','PARTY_ID',222,'Party Id','PARTY_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Party Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','SALESREP_TYPE',222,'Salesrep Type','SALESREP_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Salesrep Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','INTERFACE_LINE_ATTRIBUTE2',222,'Interface Line Attribute2','INTERFACE_LINE_ATTRIBUTE2','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Interface Line Attribute2','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','INSIDESALESREP',222,'Insidesalesrep','INSIDESALESREP','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Insidesalesrep','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','ATTRIBUTE7',222,'Attribute7','ATTRIBUTE7','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Attribute7','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','PRISMSRNUM',222,'Prismsrnum','PRISMSRNUM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Prismsrnum','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','LINETYPE',222,'Linetype','LINETYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Linetype','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','HRINSIDESRNAME',222,'Hrinsidesrname','HRINSIDESRNAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Hrinsidesrname','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','HRINSIDESRNUM',222,'Hrinsidesrnum','HRINSIDESRNUM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Hrinsidesrnum','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','ORGANIZATION_ID',222,'Organization Id','ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','INVENTORY_ITEM_ID',222,'Inventory Item Id','INVENTORY_ITEM_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Inventory Item Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','MP_ORGANIZATION_ID',222,'Mp Organization Id','MP_ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Mp Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','SPECIAL_COST',222,'Special Cost','SPECIAL_COST','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Special Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','SALES_TYPE',222,'Sales Type','SALES_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Sales Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','LIST_PRICE',222,'List Price','LIST_PRICE','','','','XXEIS_RS_ADMIN','NUMBER','','','List Price','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','INSIDESRNUM',222,'Insidesrnum','INSIDESRNUM','','','','XXEIS_RS_ADMIN','NUMBER','','','Insidesrnum','','','');
--Inserting View Components for EIS_XXWC_AR_TBD_OUTBND_V
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_AR_TBD_OUTBND_V','RA_CUSTOMER_TRX',222,'RA_CUSTOMER_TRX_ALL','CT','CT','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Header-Level Information About Invoices, Debit Mem','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_AR_TBD_OUTBND_V','MTL_SYSTEM_ITEMS_KFV',222,'MTL_SYSTEM_ITEMS_B','MSI','MSI','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Inventory Item Definitions','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_AR_TBD_OUTBND_V','HZ_PARTIES',222,'HZ_PARTIES','HP','HP','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Party Information','','','','');
--Inserting View Component Joins for EIS_XXWC_AR_TBD_OUTBND_V
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_AR_TBD_OUTBND_V','RA_CUSTOMER_TRX','CT',222,'EXATOV.CUSTOMER_TRX_ID','=','CT.CUSTOMER_TRX_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_AR_TBD_OUTBND_V','MTL_SYSTEM_ITEMS_KFV','MSI',222,'EXATOV.INVENTORY_ITEM_ID','=','MSI.INVENTORY_ITEM_ID(+)','','','','','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_AR_TBD_OUTBND_V','MTL_SYSTEM_ITEMS_KFV','MSI',222,'EXATOV.ORGANIZATION_ID','=','MSI.ORGANIZATION_ID(+)','','','','','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_AR_TBD_OUTBND_V','HZ_PARTIES','HP',222,'EXATOV.PARTY_ID','=','HP.PARTY_ID(+)','','','','','XXEIS_RS_ADMIN','','');
END;
/
set scan on define on
prompt Creating Report LOV Data for White Cap End of Month Commissions Report
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - White Cap End of Month Commissions Report
xxeis.eis_rs_ins.lov( 222,'select  per.period_name , led.name ledger_name, per.period_year, per.period_num, per.start_date, per.end_date
from    gl_periods per,gl_ledgers led
where  per.period_set_name = led.period_set_name
and     GL_SECURITY_PKG.VALIDATE_ACCESS(led.ledger_ID) = ''TRUE''
and     led.accounted_period_type = per.period_type','','AR_PERIOD_NAMES','AR_PERIOD_NAMES','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 222,'SELECT hou.NAME organization_name,
       TO_CHAR (date_from,
                NVL (fnd_profile.VALUE (''ICX_DATE_FORMAT_MASK''),
                     ''DD-MON-YYYY'')
               ) date_from,
       TO_CHAR (date_to,
                NVL (fnd_profile.VALUE (''ICX_DATE_FORMAT_MASK''),
                     ''DD-MON-YYYY'')
               ) date_to
  FROM hr_operating_units hou,
       mo_glob_org_access_tmp tmp
 WHERE hou.organization_id = tmp.organization_id','','EIS_MULTI_OPERATING_UNIT_LOV','This LOV shows all the operating units, to which user has access.','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 222,'select DISTINCT NAME from RA_BATCH_SOURCES_ALL','','AR Batch Source Name LOV','Displays Batch Sources information','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 222,'select distinct DECODE(B.category,''EMPLOYEE'',''Employee'',''OTHER'',''House Acct'',''House Acct'') type , B.category
from  JTF.JTF_RS_Resource_Extns_TL b','','SalesRepType','','ANONYMOUS',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for White Cap End of Month Commissions Report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - White Cap End of Month Commissions Report
xxeis.eis_rs_utility.delete_report_rows( 'White Cap End of Month Commissions Report' );
--Inserting Report - White Cap End of Month Commissions Report
xxeis.eis_rs_ins.r( 222,'White Cap End of Month Commissions Report','','The purpose of this report is to extract the necessary information on posted sales to enable the accurate calculation of commissions to White Cap Sales Representatives for a defined period of time (e.g. one month). Note:  The parameter �GL Period to Report� correlates to the range of dates that will be reported (e.g. select APR-20yy) will report all transactions within the respective GL period.

','','','','XXEIS_RS_ADMIN','EIS_XXWC_AR_TBD_OUTBND_V','Y','','','XXEIS_RS_ADMIN','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','');
--Inserting Report Columns - White Cap End of Month Commissions Report
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'BUSINESSDATE','BusinessDate','Businessdate','','','default','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'CATCLASS','CatClass','Catclass','','','default','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'CUSTOMERNAME','CustomerName','Customername','','','default','','29','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'DESCRIPTION','Description','Description','','','default','','14','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'DIRECTFLAG','DirectFlag','Directflag','','~~~','default','','28','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'INVOICENUMBER','InvoiceNumber','Invoicenumber','','','default','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'JOBNUMBER','JobNumber','Jobnumber','','','default','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'LINENO','LineNo','Lineno','','~~~','default','','12','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'LOC','Loc','Loc','','','default','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'MASTERNAME','MasterName','Mastername','','','default','','30','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'MASTERNUMBER','MasterNumber','Masternumber','','','default','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'PARTNO','PartNo','Partno','','','default','','13','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'QTY','QTY','Qty','','~~~','default','','15','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'REGIONNAME','RegionName','Regionname','','','default','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'SALESREPNAME','SalesRepName','Salesrepname','','','default','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'SALESREPNUMBER','SalesRepNumber','Salesrepnumber','','','default','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'UNITPRICE','UnitPrice','Unitprice','','~T~D~2','default','','16','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'TRADER','Trader','Trader','','','default','','27','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'REPORT_TYPE','ReportTypeName','Report Type','','','default','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'EXT_AVG_COST','ExtAvgCost','Report Type','NUMBER','~T~D~2','default','','21','Y','','','','','','','((CASE WHEN EXATOV.AVERAGECOST = 0 AND EXATOV.REPORT_TYPE =''Product'' THEN nvl(EXATOV.list_price,0) ELSE EXATOV.AVERAGECOST END)   * EXATOV.QTY)','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'GM_AVG_COST','GMAvgCost','Report Type','NUMBER','~T~D~2','default','','23','Y','','','','','','','decode(EXATOV.ExtSale ,0,0,((EXATOV.ExtSale - ((CASE WHEN EXATOV.AVERAGECOST = 0 AND EXATOV.REPORT_TYPE =''Product'' THEN nvl(EXATOV.list_price,0) ELSE EXATOV.AVERAGECOST END) * EXATOV.QTY)) / EXATOV.ExtSale))','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'EXT_SPECIAL_COST','ExtSpecialCost','Report Type','NUMBER','~T~D~9','default','','24','Y','','','','','','','(EXATOV.SPECIAL_COST * EXATOV.QTY)','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'GPS_SPECIAL_COST','GPSpecialCost','Report Type','NUMBER','~T~D~2','default','','25','Y','','','','','','','DECODE((EXATOV.SPECIAL_COST * EXATOV.QTY),0,0,(EXATOV.ExtSale - (EXATOV.SPECIAL_COST * EXATOV.QTY)))','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'GMS_SPECIAL_COST','GMSpecialCost','Report Type','NUMBER','~T~D~2','default','','26','Y','','','','','','','DECODE((EXATOV.SPECIAL_COST * EXATOV.QTY),0,0,((EXATOV.ExtSale - (EXATOV.SPECIAL_COST * EXATOV.QTY))/(EXATOV.ExtSale)))','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'GP_AVG_COST','GPAvgCost','Report Type','NUMBER','~T~D~2','default','','22','Y','','','','','','','(EXATOV.ExtSale - ((CASE WHEN EXATOV.AVERAGECOST = 0 AND EXATOV.REPORT_TYPE =''Product'' THEN nvl(EXATOV.list_price,0) ELSE EXATOV.AVERAGECOST END) * EXATOV.QTY))','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'MFGADJUST','Mfgadjust','Mfgadjust','','~T~D~2','default','','17','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'EXTSALE','Extsale','Extsale','','~T~D~2','default','','18','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'ORDER_NUMBER','Order Number','Order Number','','~~~','default','','31','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'SALESREP_TYPE','SalesRep Type','Salesrep Type','','','default','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'AVERAGECOST','AverageCost','Averagecost','NUMBER','~T~D~9','default','','20','Y','','','','','','','(CASE WHEN EXATOV.AVERAGECOST = 0 AND EXATOV.REPORT_TYPE =''Product'' THEN nvl(EXATOV.list_price,0) ELSE EXATOV.AVERAGECOST END)','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'INTERFACE_LINE_ATTRIBUTE2','SalesOrder Type','Interface Line Attribute2','','','default','','32','','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'INSIDESALESREP','InsideSalesRep','Insidesalesrep','','','default','','34','','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
--Inserting Report Parameters - White Cap End of Month Commissions Report
xxeis.eis_rs_ins.rp( 'White Cap End of Month Commissions Report',222,'Operating Unit','Operating Unit','OPERATING_UNIT','IN','EIS_MULTI_OPERATING_UNIT_LOV','select name from hr_operating_units where organization_id=fnd_profile.value(''ORG_ID'')','VARCHAR2','Y','Y','1','','N','SQL','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'White Cap End of Month Commissions Report',222,'GL Period to Report','GL Period to Report','PERIOD_NAME','=','AR_PERIOD_NAMES','select gps.period_name from gl_period_statuses gps where gps.application_id = 101  and gps.set_of_books_id = fnd_profile.value(''GL_SET_OF_BKS_ID'') and trunc(sysdate) between gps.start_date and gps.end_date','VARCHAR2','Y','Y','3','','N','SQL','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'White Cap End of Month Commissions Report',222,'Order Line Quantity Shipped','Order Line Quantity Shipped','SHIPPED_QUANTITY','IN','','','NUMERIC','N','N','5','','Y','CONSTANT','XXEIS_RS_ADMIN','N','N','','','');
xxeis.eis_rs_ins.rp( 'White Cap End of Month Commissions Report',222,'Order Line Extended Amount','Order Line Extended Amount','EXTSALE','IN','','','NUMERIC','N','N','6','','Y','CONSTANT','XXEIS_RS_ADMIN','N','N','','','');
xxeis.eis_rs_ins.rp( 'White Cap End of Month Commissions Report',222,'Order Line Extended Cost','Order Line Extended Cost','AVERAGECOST','IN','','','NUMERIC','N','N','7','','Y','CONSTANT','XXEIS_RS_ADMIN','N','N','','','');
xxeis.eis_rs_ins.rp( 'White Cap End of Month Commissions Report',222,'Invoice Source','Invoice Source','INVOICE_SOURCE','IN','AR Batch Source Name LOV','''ORDER MANAGEMENT'',''STANDARD OM SOURCE'',''REPAIR OM SOURCE'',''WC MANUAL''','VARCHAR2','Y','Y','2','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'White Cap End of Month Commissions Report',222,'SalesRepType','SalesRepType','SALESREP_TYPE','IN','SalesRepType','','VARCHAR2','N','Y','4','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
--Inserting Report Conditions - White Cap End of Month Commissions Report
xxeis.eis_rs_ins.rcn( 'White Cap End of Month Commissions Report',222,'SHIPPED_QUANTITY','IN',':Order Line Quantity Shipped','','','Y','5','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'White Cap End of Month Commissions Report',222,'EXTSALE','IN',':Order Line Extended Amount','','','Y','6','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'White Cap End of Month Commissions Report',222,'AVERAGECOST','IN',':Order Line Extended Cost','','','Y','7','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'White Cap End of Month Commissions Report',222,'SALESREP_TYPE','IN',':SalesRepType','','','Y','4','Y','XXEIS_RS_ADMIN');
--Inserting Report Sorts - White Cap End of Month Commissions Report
xxeis.eis_rs_ins.rs( 'White Cap End of Month Commissions Report',222,'REPORT_TYPE','ASC','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rs( 'White Cap End of Month Commissions Report',222,'SALESREPNUMBER','ASC','XXEIS_RS_ADMIN','','');
--Inserting Report Triggers - White Cap End of Month Commissions Report
xxeis.eis_rs_ins.rt( 'White Cap End of Month Commissions Report',222,'begin
xxeis.eis_rs_xxwc_com_rep_pkg_ext.comm_rep_par(
    p_period_name =>:GL Period to Report,
    p_operating_unit => :Operating Unit,
    p_invoice_source =>:Invoice Source);
end;','B','Y','XXEIS_RS_ADMIN');
--Inserting Report Templates - White Cap End of Month Commissions Report
--Inserting Report Portals - White Cap End of Month Commissions Report
--Inserting Report Dashboards - White Cap End of Month Commissions Report
--Inserting Report Security - White Cap End of Month Commissions Report
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','222','','50920',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','222','','50919',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','222','','50921',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','222','','50922',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','222','','50923',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','222','','51030',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','401','','50941',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','222','','50846',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','222','','50845',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','222','','50848',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','222','','50849',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','222','','50944',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','222','','50871',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','20005','','50880',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','','10010432','',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','','RB054040','',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','','RV003897','',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','','SS084202','',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','','SE012733','',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','','SG019472','',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','20005','','51207',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','','GG050582','',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','','JT021060','',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','','PP018915','',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','20005','','50900',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','','EG022422','',222,'XXEIS_RS_ADMIN','','');
--Inserting Report Pivots - White Cap End of Month Commissions Report
xxeis.eis_rs_ins.rpivot( 'White Cap End of Month Commissions Report',222,'Pivot','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rs_ins.rpivot_dtls( 'White Cap End of Month Commissions Report',222,'Pivot','LOC','COL_FIELD','','Branch Id','','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'White Cap End of Month Commissions Report',222,'Pivot','SALESREPNUMBER','ROW_FIELD','','','','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'White Cap End of Month Commissions Report',222,'Pivot','EXTSALE','DATA_FIELD','SUM','','','1','xlNormal');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
xxeis.eis_rs_ins.rpivot( 'White Cap End of Month Commissions Report',222,'Total Sales by SalesRep and Bra','2','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Total Sales by SalesRep and Bra
xxeis.eis_rs_ins.rpivot_dtls( 'White Cap End of Month Commissions Report',222,'Total Sales by SalesRep and Bra','LOC','ROW_FIELD','','','1','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'White Cap End of Month Commissions Report',222,'Total Sales by SalesRep and Bra','SALESREPNUMBER','ROW_FIELD','','','2','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'White Cap End of Month Commissions Report',222,'Total Sales by SalesRep and Bra','EXTSALE','DATA_FIELD','SUM','','1','','');
--Inserting Report Summary Calculation Columns For Pivot- Total Sales by SalesRep and Bra
END;
/
set scan on define on
