---------------------------------------------------------------------------------------------------------------
/**************************************************************************************************************
  $Header XXEIS.EIS_XXWC_AR_TBD_OUTBND_V $
  Module Name : Receivables
  PURPOSE	  : White Cap End of Month Commissions Report
  TMS Task Id : 20160412-00032 
  REVISIONS   :
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     12-Apr-2016        Siva   		 TMS#20160412-00032  -- Performance Tuning
**************************************************************************************************************/
DROP VIEW XXEIS.EIS_XXWC_AR_TBD_OUTBND_V;

CREATE OR REPLACE VIEW XXEIS.EIS_XXWC_AR_TBD_OUTBND_V (BUSINESSDATE, REPORT_TYPE, ORDER_NUMBER, INVOICENUMBER, LINENO, DESCRIPTION, QTY, UNITPRICE, EXTSALE,
 MASTERNUMBER, MASTERNAME, JOBNUMBER, CUSTOMERNAME, SALESREPNUMBER, SALESREPNAME, LOC, MP_ORGANIZATION_ID, REGIONNAME, PARTNO, CATCLASS, INVOICE_SOURCE,
 SHIPPED_QUANTITY, DIRECTFLAG, AVERAGECOST, TRADER, SPECIAL_COST, OPERATING_UNIT, LIST_PRICE, ORG_ID, INVENTORY_ITEM_ID, ORGANIZATION_ID, CUSTOMER_TRX_ID,
 PARTY_ID, PERIOD_NAME, MFGADJUST, SALESREP_TYPE, SALES_TYPE, INTERFACE_LINE_ATTRIBUTE2, INSIDESALESREP, INSIDESRNUM, PRISMSRNUM, LINETYPE, HRINSIDESRNAME, HRINSIDESRNUM) AS 
  SELECT BUSINESSDATE  
         ,REPORT_TYPE
         ,ORDER_NUMBER
         ,INVOICENUMBER
         ,LINENO
         ,DESCRIPTION
         ,QTY
         ,UNITPRICE
         ,EXTSALE
         ,MASTERNUMBER
         ,MASTERNAME
         ,JOBNUMBER
         ,CUSTOMERNAME
         ,SALESREPNUMBER
         ,SALESREPNAME
         ,LOC
         ,MP_ORGANIZATION_ID
         ,REGIONNAME
         ,PARTNO
         ,CATCLASS
         ,INVOICE_SOURCE
         ,SHIPPED_QUANTITY
         ,DIRECTFLAG
         ,AVERAGECOST
         ,TRADER
         ,SPECIAL_COST
         ,OPERATING_UNIT
         ,LIST_PRICE
         ,ORG_ID
         ,INVENTORY_ITEM_ID
         ,ORGANIZATION_ID
         ,CUSTOMER_TRX_ID
         ,PARTY_ID
         ,PERIOD_NAME
         ,MFGADJUST
         ,SALESREP_TYPE
         ,SALES_TYPE
         ,INTERFACE_LINE_ATTRIBUTE2
         ,INSIDESALESREP
         ,INSIDESRNUM
         ,PRISMSRNUM
         ,LINETYPE
         ,HRINSIDESRNAME
         ,HRINSIDESRNUM
FROM xxeis.xxwc_eom_comm_rep_tbl
/
