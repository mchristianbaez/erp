CREATE OR REPLACE PACKAGE    XXEIS.EIS_RS_XXWC_COM_REP_PKG_EXT
   AUTHID CURRENT_USER
--//============================================================================
--//  
--// Reference Object Name      :: xxeis.eis_rs_xxwc_com_rep_pkg
--// Change Request 			:: Performance Issue
--// Object Name         		:: xxeis.eis_rs_xxwc_com_rep_pkg_ext
--//
--// Object Type         		:: Package Specification
--//
--// Object Description  		:: This Package will trigger in before report and insert the values into Temp Table
--//
--// Version Control
--//============================================================================
--// Vers    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     siva  	04/12/2016   Initial Build --TMS#20160412-00032	--Performance tuning
--//============================================================================
IS
   PROCEDURE comm_rep_par (p_period_name      IN VARCHAR2
                          ,p_operating_unit   IN VARCHAR2
                          ,p_invoice_source   IN VARCHAR2
                          );
--//============================================================================
--//
--// Object Name         :: comm_rep_par
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This procedure gets the record based on parameter and insert into temp table
--//
--// Version Control
--//============================================================================
--// Vers    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     siva  	04/12/2016    Initial Build --TMS#20160412-00032 --Performance tuning
--//============================================================================					  
END eis_rs_xxwc_com_rep_pkg_ext;
/
