DROP MATERIALIZED VIEW APPS.XXWC_QP_ECOMMERCE_ITEMS_MV;

/***********************************************************************************************************
  Materialized View Name: XXWC_QP_ECOMMERCE_ITEMS_MV

  PURPOSE:   This Materialized View is used for ECommerce Outbout Extract

  REVISIONS:
  Ver        Date        Author             Description
  ---------  ----------  ---------------    ---------------------------------------------------------------
  1.0        02/24/2015  Shankar Hariharan  TMS# 20150219-00234 Tuning of Speed Build Forms Personazliation
  1.1        03/10/2015  P.Vamshidhar       TMS# 20150309-00325 added additional columns to improve ecomm 
                                            extract performance.
  1.2        12/04/2017 Pattabhi Avula      TMS#20171203-00012 -- Removed the blank space in line#2											
************************************************************************************************************/
CREATE MATERIALIZED VIEW APPS.XXWC_QP_ECOMMERCE_ITEMS_MV
   (
   INVENTORY_ITEM_ID,
   ORGANIZATION_ID,
   ITEM_NUMBER,
   DESCRIPTION,
   LAST_UPDATE_DATE,
   CALL_FOR_PRICE_FLAG,
   WEB_FLAG,
   LAST_UPDATE_DATE_MSIB
   )
   TABLESPACE APPS_TS_TX_DATA
   PCTUSED 0
   PCTFREE 10
   INITRANS 2
   MAXTRANS 255
   STORAGE (INITIAL 128 K
            NEXT 128 K
            MINEXTENTS 1
            MAXEXTENTS UNLIMITED
            PCTINCREASE 0
            BUFFER_POOL DEFAULT
            FLASH_CACHE DEFAULT
            CELL_FLASH_CACHE DEFAULT)
   NOCACHE
   LOGGING
   NOCOMPRESS
   NOPARALLEL
   BUILD IMMEDIATE
   REFRESH FORCE
           ON DEMAND
           WITH PRIMARY KEY
           USING TRUSTED CONSTRAINTS                    -- TMS# 20150219-00234
AS
   SELECT msi.inventory_item_id,                       -- web-enabled items  -- , -- Ver#1.2
          msi.organization_id,
          msi.segment1 item_number,
          msi.description,
          mic.last_update_date,    -- TMS# 20150219-00234 > Start
          xxwc_qp_market_price_util_pkg.get_call_for_price_flag (
             msi.inventory_item_id,
             msi.organization_id),  -- TMS# 20150219-00234 < End
          -- TMS# 20150309-00325 >Start
          mc.segment1,
          msi.last_update_date
     -- TMS# 20150309-00325 >End
     FROM mtl_item_categories mic,
          mtl_categories_b mc,
          mtl_category_sets mcs,
          mtl_system_items_b msi,
          mtl_parameters mp
    WHERE     msi.inventory_item_id = mic.inventory_item_id
          AND msi.organization_id = mic.organization_id
          AND mic.category_id = mc.category_id
          AND mic.category_set_id = mcs.category_set_id
          AND mcs.structure_id = mc.structure_id
          AND mcs.category_set_name = 'Website Item'
          -- AND mc.segment1 = 'Y'     -- TMS# 20140211-00049
          AND mc.segment1 IN ('Y', 'C')                 -- TMS# 20140211-00049
          AND msi.organization_id = mp.organization_id
          AND mp.organization_code = 'MST';
		  
COMMENT ON MATERIALIZED VIEW APPS.XXWC_QP_ECOMMERCE_ITEMS_MV IS 'snapshot table for snapshot APPS.XXWC_QP_ECOMMERCE_ITEMS_MV';
