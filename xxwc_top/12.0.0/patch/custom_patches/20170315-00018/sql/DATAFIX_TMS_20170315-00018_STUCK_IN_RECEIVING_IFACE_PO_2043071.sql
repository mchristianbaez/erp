/********************************************************************************
FILE NAME: TMS_20170315-00018_XXWC_CUS_TMP_TBL_PURGE.sql

PROGRAM TYPE: PL/SQL 

PURPOSE: Data fix script for TMS#20170315-00018

HISTORY
===============================================================================
VERSION DATE          AUTHOR(S)              DESCRIPTION
------- -----------   --------------- -----------------------------------------
1.0     03/24/2017    Pattabhi Avula         Initial Release for TMS#20170315-00018
********************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000


BEGIN
   DBMS_OUTPUT.put_line ('Before update');

   UPDATE rcv_transactions_interface
      SET request_id = NULL,
          processing_request_id = NULL,
          processing_status_code = 'PENDING',
          transaction_status_code = 'PENDING',
          processing_mode_code = 'BATCH',
          last_update_date = SYSDATE,
          Last_updated_by = 16991
    WHERE     PO_HEADER_ID=3404338
       AND PROCESSING_STATUS_CODE = 'PENDING'
       AND PROCESSING_MODE_CODE = 'ONLINE'
       AND TRANSACTION_STATUS_CODE = 'PENDING';
       
   DBMS_OUTPUT.put_line ('Records update-' || SQL%ROWCOUNT);
   COMMIT;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Unable to update ' || SQLERRM);
END;
/