/* TMS 20151106-00050 Datafix for internal Requisition Line.ESMS S293852 *
SR 3-11145492411 */
SET SERVEROUTPUT ON;

BEGIN
   UPDATE po_requisition_lines_all
      SET quantity_cancelled = 192,
          CANCEL_FLAG = 'Y',
          CANCEL_DATE = SYSDATE,
          last_update_date = SYSDATE,
          last_updated_by = 16991
    WHERE requisition_line_id = 13668367;

   COMMIT;

   DBMS_OUTPUT.put_line ('Req line cancelled');
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Error received-' || SQLERRM);
END;
/