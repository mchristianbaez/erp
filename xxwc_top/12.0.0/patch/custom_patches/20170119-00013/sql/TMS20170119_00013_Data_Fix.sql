 /****************************************************************************************************
  Table: TMS20170119_00013_Data_Fix.sql     
  Description: Need One time data fix to update Expense 4th segment
  HISTORY
  =====================================================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- ----------------------------------------------------------
  1.0     27-Apr-2017        P.Vamshidhar    Initial version TMS#20170119-00013
                                             Need One time data fix to update Expense 4th segment
  *****************************************************************************************************/
SET SERVEROUT ON
  
BEGIN
   INSERT INTO XXWC.XXWC_MTL_INV_CATE_UPDATE_LOG (CATEGORY_ID,
                                                  EXPN_ACCT,
                                                  PROCESS_STATUS,
                                                  CREATION_DATE)
      SELECT DISTINCT a.category_id,
                      a.attribute8,
                      'NEW',
                      SYSDATE
        FROM (SELECT mcb.category_id,
                     a.expense_account,
                     b.expense_account,
                     gcc.segment4,
                     mcb.attribute8
                FROM apps.mtl_system_items_b a,
                     apps.mtl_parameters b,
                     apps.mtl_item_categories_v c,
                     apps.gl_code_combinations gcc,
                     apps.mtl_categories_v mcb
               WHERE     a.organization_id = 222
                     AND a.organization_id = b.organization_id
                     AND a.expense_account = gcc.code_combination_id
                     AND a.organization_id = c.organization_id
                     AND a.inventory_item_id = c.inventory_item_id
                     AND c.category_set_name = 'Inventory Category'
                     AND mcb.structure_name = 'Item Categories'
                     AND a.inventory_item_status_code <> 'Inactive'
					 AND c.category_id in (2561 ,2595 ,2650 ,3431 ,3473 ,3492 ,3640 ,2787)
                     AND c.category_id = mcb.category_id) a
       WHERE a.segment4 <> a.attribute8;

   DBMS_OUTPUT.PUT_LINE ('Number of rows inserted ' || SQL%ROWCOUNT);

   COMMIT;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.PUT_LINE ('Error occured ' || SUBSTR (SQLERRM, 1, 250));
      ROLLBACK;
END;
/