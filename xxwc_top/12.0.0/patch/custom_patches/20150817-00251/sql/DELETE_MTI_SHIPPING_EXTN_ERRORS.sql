/*
 TMS: 20150817-00251  
 Date: 08/17/2015
 */
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20150817-00251  , Script 1 -Before Delete');
   DELETE FROM mtl_transactions_interface
      WHERE source_code = 'Subinventory Transfer' AND process_flag = '3' AND error_code='Invalid item';
   DBMS_OUTPUT.put_line ('TMS: 20150817-00251, Script 1 -After Delete, rows deleted: '||sql%rowcount);
   COMMIT;
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20150817-00251, Script 1, Errors ='||SQLERRM);
END;
/