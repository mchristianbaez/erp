/*
 TMS: 20150817-00251  
 Date: 08/17/2015
 */
SET SERVEROUTPUT ON SIZE 1000000

DECLARE
   l_item_cost   NUMBER;
   v_count       NUMBER := 0;

   CURSOR mti_recs
   IS
      SELECT *
        FROM mtl_transactions_interface mt
       WHERE     mt.source_code = 'ACU'
             AND process_flag = '3'
             AND EXISTS
                    (SELECT 1
                       FROM cst_item_costs cic
                      WHERE     1 = 1
                            AND cic.organization_id = mt.organization_id
                            AND cic.cost_type_id = 2
                            AND cic.inventory_item_id = mt.inventory_item_id
                            AND cic.item_cost = 0) AND ROWNUM<5;
BEGIN

DBMS_OUTPUT.put_line ('TMS: 20150817-00251  , Script 1 -Before Update');
   FOR i IN mti_recs
   LOOP
      BEGIN
         l_item_cost := 0;

         SELECT NVL (LIST_PRICE_PER_UNIT, 0)
           INTO l_item_cost
           FROM mtl_system_items_b
          WHERE     organization_id = 222
                AND inventory_item_id = i.inventory_item_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            DBMS_OUTPUT.PUT_LINE (
                  'Error in finding list price for item id-'
               || i.inventory_item_id
               || SQLERRM);
      END;

      BEGIN
         UPDATE mtl_transactions_interface
            SET process_flag = 1,
                lock_flag = 2,
                transaction_mode = 3,
                validation_required = 1,
                ERROR_CODE = NULL,
                error_explanation = NULL,
                request_id = NULL,
                transaction_date = SYSDATE,
                NEW_AVERAGE_COST = l_item_cost
          WHERE     1 = 1
                AND process_flag = 3
                AND transaction_interface_id = i.transaction_interface_id
                AND source_code = 'ACU'
                AND inventory_item_id = i.inventory_item_id;

         v_count := v_count + 1;
      EXCEPTION
         WHEN OTHERS
         THEN
            DBMS_OUTPUT.PUT_LINE (
                  'Error in updating average cost_to MTI for transaction_interface_id-'
               || i.transaction_interface_id
               || SQLERRM);
      END;
   END LOOP;
   
    COMMIT;

   DBMS_OUTPUT.put_line (
         'TMS: 20150817-00251, Script 1 -After update, rows updated: '
      || v_count);
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.PUT_LINE ('Error is' || SQLERRM);
END;
/