create or replace PACKAGE BODY  APPS.XXWC_SUB_INV_NOTI_PUBD_PKG
IS
   /*************************************************************************
   *   $Header XXWC_SUB_INV_NOTI_PUBD_PKG.pkb $
   *   Module Name: XXWC_SUB_INV_NOTI_PUBD_PKG.pkb
   *
   *   PURPOSE:   This package is used to send a email notification to the branch operations manager,
   *              Whenever there is transaction happened to 'PUBD' subinventory.
   *
   *   REVISIONS:
   *   Ver        Date         Author                Description
   *   ---------  ----------   ---------------       -------------------------
   *   1.0        18-Nov-2015  Kishorebabu V          Initial Version
   *                                                  TMS# 20151023-00047 Subinventory Transfer Notification for PUBD
   * ***************************************************************************/

-- Declaring Package Global Variables

g_req_id       NUMBER(20)    := fnd_global.conc_request_id;
g_distro_list  VARCHAR2(100) := 'HDSOracleDevelopers@hdsupply.com';


   PROCEDURE XXWC_SUB_INV_NOTI_PUBD_PRC (errbuf   OUT VARCHAR2
                                        ,retcode  OUT VARCHAR2)
   IS
      /* **************************************************************************
         *   Procedure Name: XXWC_SUB_INV_NOTI_PUBD
         *
         *   PURPOSE:   This procedure called from a conc prog to to send a email notification to the branch operations manager
         *              whenever there is transaction happened to 'PUBD' subinventory.
         *
         *   REVISIONS:
         *   Ver        Date          Author               Description
         *   ---------  ----------    ---------------      -------------------------
         *   1.0        18-Nov-2015  Kishorebabu V          Initial Version
         *                                                  TMS# 20151023-00047 Subinventory Transfer Notification for PUBD
         * ***************************************************************************/

      l_sec                  VARCHAR2 (1000);
      l_error_msg            VARCHAR2 (4000);
      l_exception            EXCEPTION;
      l_result               VARCHAR2(2000);
      l_result_msg           VARCHAR2(2000);
      ln_count               NUMBER := 0;
	  ln_trans_cnt           NUMBER := 0;
      wfile_handle           UTL_FILE.FILE_TYPE;
      l_file_location        VARCHAR2(256) := 'XXWC_SUB_INV_NOTI_PUBD';
	  l_file_name            VARCHAR2(256);
      l_directory_path       VARCHAR2(1000);
      l_email_address        VARCHAR2(100);
      max_line_length         BINARY_INTEGER                         := 32767;

      CURSOR cur_pubd_trans (cp_organization_id IN NUMBER)
      IS
        SELECT org
        ,      item_number
        ,      description
        ,      shelf_life_days
        ,      SUM(quantity_transferred_to_pubd) quantity_transferred_to_pubd
        ,      remaining_on_hand_quantity
        ,      uom
        ,      SUM(total_value_of_transfer) total_value_of_transfer
        FROM   (SELECT TO_CHAR(ood.organization_code)    org
                ,      msib.segment1                     item_number
                ,      REPLACE(msib.description,'-',' ') description
                ,      msib.shelf_life_days              shelf_life_days
                ,      mmt.transaction_quantity          quantity_transferred_to_pubd
                ,      (SELECT SUM(transaction_quantity)
                        FROM   mtl_onhand_quantities
                        WHERE  inventory_item_id        = moq.inventory_item_id
                        AND    organization_id          = moq.organization_id
                        AND    UPPER(subinventory_code)  <> 'PUBD') remaining_on_hand_quantity
                ,      msib.primary_uom_code                        uom
                ,      (mmt.transaction_quantity*cic.item_cost)     total_value_of_transfer
                ,      mmt.transaction_date
                FROM   mtl_material_transactions    mmt
                ,      org_organization_definitions ood
                ,      mtl_system_items_b           msib
                ,      mtl_onhand_quantities_detail moq
                ,      cst_item_costs               cic
                ,      cst_cost_types               cct
                WHERE  mmt.organization_id          = msib.organization_id
                AND    msib.inventory_item_id       = mmt.inventory_item_id
                AND    TRUNC(mmt.transaction_date)  = (TRUNC(SYSDATE)-1)
                AND    mmt.organization_id          = ood.organization_id
        		AND    ood.organization_id          = cp_organization_id
                AND    mmt.inventory_item_id        = moq.inventory_item_id
                AND    mmt.organization_id          = moq.organization_id
                AND    mmt.transaction_id           = moq.update_transaction_id
                AND    ((mmt.subinventory_code      = 'PUBD'
                         AND mmt.transaction_type_id = 2)
                       OR(mmt.subinventory_code = 'PUBD'
                          AND mmt.transaction_type_id = 15))
                AND    msib.organization_id          = cic.organization_id
                AND    msib.inventory_item_id        = cic.inventory_item_id
                AND    cct.cost_type_id              = cic.cost_type_id
                AND    UPPER(cct.cost_type)          = 'AVERAGE')
        GROUP BY org
        ,      item_number
        ,      description
        ,      shelf_life_days
        ,      remaining_on_hand_quantity
        ,      uom;

		CURSOR cur_pubd_org
      IS
        SELECT DISTINCT ood.organization_id
        ,      ood.organization_code
        ,      ood.organization_name
        FROM   mtl_material_transactions    mmt
        ,      org_organization_definitions ood
        WHERE  TRUNC(mmt.transaction_date)  = (TRUNC(SYSDATE)-1)
        AND    mmt.organization_id          = ood.organization_id
        AND    ((mmt.subinventory_code      = 'PUBD'
                 AND mmt.transaction_type_id = 2)
               OR(mmt.subinventory_code = 'PUBD'
                  AND mmt.transaction_type_id = 15))
        ORDER BY ood.organization_id;

	  CURSOR cur_email_addr(cp_organization_id IN NUMBER)
	  IS
	    SELECT DISTINCT fu.email_address
        FROM   mtl_parameters mp
        ,      fnd_user fu
        WHERE  mp.attribute13             = fu.employee_id
        AND    mp.master_organization_id  = 222
        AND    mp.organization_id         = cp_organization_id
        AND    fu.email_address           IS NOT NULL
        AND    SYSDATE BETWEEN NVL(fu.start_date,SYSDATE) AND NVL(fu.end_date,SYSDATE);

   BEGIN

     BEGIN
	   l_sec := 'Fetchng the directory path';
       SELECT directory_path
       INTO   l_directory_path
       FROM   all_directories
       WHERE  directory_name  = 'XXWC_SUB_INV_NOTI_PUBD';
     EXCEPTION
       WHEN NO_DATA_FOUND THEN
         l_error_msg := ' No Data Found' || SUBSTR (SQLERRM, 1, 250);
         RAISE l_exception;
       WHEN OTHERS THEN
         l_error_msg := ' Error occured while fetch the directory path' ||SQLERRM;
         RAISE l_exception;
     END;

	 apps.fnd_file.put_line(fnd_file.LOG,'l_directory_path :'|| l_directory_path);
	 
	 BEGIN
	   l_sec := 'Opening the utl file';
	   FOR rec_pubd_org IN cur_pubd_org LOOP
	     apps.fnd_file.put_line(fnd_file.LOG,'Fetching all the eligible records and writing in to the file');
		 
	     ln_trans_cnt := ln_trans_cnt +1;
         l_file_name  := NULL;
	     l_file_name  := 'PUBD'||'_'||rec_pubd_org.organization_code||'_'|| TO_CHAR (SYSDATE, 'YYYYMMDDHH24MISS')||'.csv';
		 apps.fnd_file.put_line(fnd_file.LOG,'l_file_name: '|| l_file_name);
         wfile_handle := utl_file.fopen(l_file_location,l_file_name,'W', max_line_length);
	     utl_file.put_line(wfile_handle,'Org'||','||
                                        'Item Number'||','||
                                        'Description'||','||
                                        'Shelf Life Days'||','||
                                        'Quantity Transferred to PUBD'||','||
                                        'UOM'||','||
                                        'Remaining On Hand Quantity'||','||
                                        'Total Value of Transfer');
         l_sec := 'Fetching all the eligible records and writing in to the file';
         FOR rec_pubd_trans IN cur_pubd_trans(rec_pubd_org.organization_id) LOOP
           ln_count := ln_count+1;
           utl_file.put_line(wfile_handle,rec_pubd_trans.org||',"'||
                                          rec_pubd_trans.item_number||'",'||
                                          REPLACE(rec_pubd_trans.description,',')||','||
                                          rec_pubd_trans.shelf_life_days||','||
                                          rec_pubd_trans.quantity_transferred_to_pubd||','||
                                          rec_pubd_trans.uom||','||
                                          rec_pubd_trans.remaining_on_hand_quantity||','||
                                          rec_pubd_trans.total_value_of_transfer);
         END LOOP;
	     COMMIT;
         utl_file.fclose(wfile_handle);
	     l_sec := 'closing the utl file';

         l_email_address := NULL;
	       FOR rec_email_addr IN cur_email_addr (rec_pubd_org.organization_id) LOOP
             l_email_address := rec_email_addr.email_address||';'|| l_email_address;
           END LOOP;

         IF ln_count = 0 THEN
           apps.fnd_file.put_line(fnd_file.LOG,'*****************NO DATA FOUND***********************');
	     ELSE
		   apps.fnd_file.put_line(fnd_file.LOG,'rec_pubd_org.organization_name :' ||rec_pubd_org.organization_name);
		   apps.fnd_file.put_line(fnd_file.LOG,'Sending email to :' ||l_email_address);
           xxcus_misc_pkg.send_email_attachment (p_sender        => 'no-reply@whitecap.net'
                                                ,p_recipients    => l_email_address
                                                ,p_subject       => 'PUBD Subinventory Transfers for '||rec_pubd_org.organization_name||' on '||(TRUNC(SYSDATE)-1)
                                                ,p_message       => 'The attached items were transferred into the PUBD Subinventory yesterday:'
                                                ,p_attachments   => l_directory_path||'/'||l_file_name
                                                ,x_result        => l_result
                                                ,x_result_msg    => l_result_msg
                                                ,p_directory     => 'XXWC_SUB_INV_NOTI_PUBD');
           apps.fnd_file.put_line(fnd_file.LOG,'l_result: ' ||l_result);
           apps.fnd_file.put_line(fnd_file.LOG,'l_result_msg: ' ||l_result_msg);                                     
         END IF;
       END LOOP;
	   IF ln_trans_cnt = 0 THEN
	     apps.fnd_file.put_line(fnd_file.LOG,'*****************NO DATA FOUND***********************');
	   END IF;

	 EXCEPTION
	   WHEN UTL_FILE.INVALID_PATH THEN
         apps.fnd_file.put_line(fnd_file.LOG,'INVALID PATH');
         utl_file.fclose_all;
       WHEN UTL_FILE.INVALID_MODE THEN
        apps.fnd_file.put_line(fnd_file.LOG,'INVALID MODE');
        utl_file.fclose_all;
	 END;

   EXCEPTION
     WHEN l_exception THEN
	  apps.fnd_file.put_line(fnd_file.LOG,'Error '||SUBSTR (DBMS_UTILITY.format_error_stack ()||DBMS_UTILITY.format_error_backtrace (),1,2000));
	 
      xxcus_error_pkg.xxcus_error_main_api (p_called_from         => 'XXWC_SUB_INV_NOTI_PUBD_PKG.XXWC_SUB_INV_NOTI_PUBD_PRC'
                                           ,p_calling             => l_sec
                                           ,p_request_id          => g_req_id
                                           ,p_ora_error_msg       => SUBSTR (DBMS_UTILITY.format_error_stack ()||
                                                                             DBMS_UTILITY.format_error_backtrace (),1,2000)
                                           ,p_error_desc          => l_error_msg
                                           ,p_distribution_list   => g_distro_list
                                           ,p_module              => 'INV');
     WHEN OTHERS THEN
	  apps.fnd_file.put_line(fnd_file.LOG,'Error '||SUBSTR (DBMS_UTILITY.format_error_stack ()||DBMS_UTILITY.format_error_backtrace (),1,2000));
      xxcus_error_pkg.xxcus_error_main_api (p_called_from         => 'XXWC_SUB_INV_NOTI_PUBD_PKG.XXWC_SUB_INV_NOTI_PUBD_PRC'
                                           ,p_calling             => l_sec
                                           ,p_request_id          => g_req_id
                                           ,p_ora_error_msg       => SUBSTR (DBMS_UTILITY.format_error_stack ()||
                                                                             DBMS_UTILITY.format_error_backtrace (),1,2000)
                                           ,p_error_desc          => l_error_msg
                                           ,p_distribution_list   => g_distro_list
                                           ,p_module              => 'INV');

   END XXWC_SUB_INV_NOTI_PUBD_PRC;
END XXWC_SUB_INV_NOTI_PUBD_PKG;
/