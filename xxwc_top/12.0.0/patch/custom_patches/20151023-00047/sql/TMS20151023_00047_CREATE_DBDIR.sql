/*************************************************************************
   *   $Header TMS20151023_00047_CREATE_DBDIR.sql $
   *   Module Name: TMS20151023_00047_CREATE_DBDIR.sql
   *
   *   PURPOSE:   This script is used to create db directory for XXWC_SUB_INV_NOTI_PUBD_PKG
   *
   *   REVISIONS:
   * Created By           TMS              Date         Version  Comments
   * ===========         ===============  ==========    =======  ===========
   * Niraj K Ranjan      20151023-00047   18-MAR-2016   1.0      Created.
  *******************************************************************************/
DECLARE
l_db_name VARCHAR2(20) :=NULL;
l_path VARCHAR2(240) :=NULL;
l_sql VARCHAR2(240) :=NULL;
--
BEGIN

SELECT lower(name) 
  INTO l_db_name
  FROM v$database;

  l_path :='/xx_iface/'||l_db_name||'/outbound/pubd';

  l_sql :='CREATE OR REPLACE DIRECTORY XXWC_SUB_INV_NOTI_PUBD as'||' '||''''||l_path||'''';

  dbms_output.put_line('DBA Directory Path: '||l_path);
  dbms_output.put_line(' ');
  dbms_output.put_line('SQL: '||l_sql);
  dbms_output.put_line(' ');  
  dbms_output.put_line('Begin setup of directory XXWC_SUB_INV_NOTI_PUBD');  
  EXECUTE IMMEDIATE l_sql;
  dbms_output.put_line('End setup of directory XXWC_SUB_INV_NOTI_PUBD');    
  dbms_output.put_line('Begin GRANT of directory XXWC_SUB_INV_NOTI_PUBD');  
  l_sql :='GRANT ALL ON DIRECTORY XXWC_SUB_INV_NOTI_PUBD TO PUBLIC WITH GRANT OPTION';
  EXECUTE IMMEDIATE l_sql;
  dbms_output.put_line('End GRANT of directory XXWC_SUB_INV_NOTI_PUBD');
END;

/
