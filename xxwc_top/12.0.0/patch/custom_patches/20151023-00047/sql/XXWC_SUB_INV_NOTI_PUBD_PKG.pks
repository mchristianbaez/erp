create or replace PACKAGE  APPS.XXWC_SUB_INV_NOTI_PUBD_PKG
IS
   /*************************************************************************
   *   $Header XXWC_SUB_INV_NOTI_PUBD_PKG.pks $
   *   Module Name: XXWC_SUB_INV_NOTI_PUBD_PKG.pks
   *
   *   PURPOSE    : This package is used to send a email notification to the branch operations manager,
   *                Whenever there is transaction happened to 'PUBD' subinventory.
   *
   *   REVISIONS  :
   *   Ver        Date         Author                Description
   *   ---------  ----------   ---------------       -------------------------
   *   1.0        18-Nov-2015  Kishorebabu V          Initial Version
   *                                                  TMS# 20151023-00047 Subinventory Transfer Notification for PUBD
   * ***************************************************************************/

   /* **************************************************************************
      *   Procedure Name: XXWC_SUB_INV_NOTI_PUBD
      *
      *   PURPOSE:   This procedure called from a conc prog to to send a email notification to the branch operations manager
      *              whenever there is transaction happened to 'PUBD' subinventory.
      *
      *   REVISIONS:
      *   Ver        Date         Author                Description
      *   ---------  ----------   ---------------       -------------------------
      *   1.0        18-Nov-2015  Kishorebabu V          Initial Version
      *                                                  TMS# 20151023-00047 Subinventory Transfer Notification for PUBD
      * ***************************************************************************/
   PROCEDURE XXWC_SUB_INV_NOTI_PUBD_PRC (errbuf   OUT VARCHAR2
                                        ,retcode  OUT VARCHAR2);
END XXWC_SUB_INV_NOTI_PUBD_PKG;
/