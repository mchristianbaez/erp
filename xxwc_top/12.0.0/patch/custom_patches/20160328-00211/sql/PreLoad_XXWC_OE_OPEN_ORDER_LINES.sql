/*
 TMS: 20151023-00037 
 Date: 03/01/2016
 Notes: Shipping Extension Modification for Counter Order Removal and PUBD
*/
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
BEGIN
   	DBMS_OUTPUT.put_line ('TMS: 20151023-00037 , Script 1 -Before Insert');
	
	DELETE FROM XXWC.xxwc_oe_open_order_lines;
	
	DBMS_OUTPUT.put_line ('TMS: 20151023-00037, After Delete, records Deleted : '||sql%rowcount);
    COMMIT;
   
	INSERT
	INTO XXWC.xxwc_oe_open_order_lines
	  (
	    line_id ,
	    header_id ,
	    line_type_id ,
	    inventory_item_id ,
	    organization_id ,
	    ordered_quantity ,
	    org_id ,
	    created_by ,
	    last_updated_by ,
	    creation_date ,
	    last_update_date ,
	    last_update_login
	  )
	SELECT ol.line_id ,
	  ol.header_id ,
	  ol.line_type_id ,
	  ol.inventory_item_id ,
	  ol.ship_from_org_id ,
	  (ol.ordered_quantity
                     * po_uom_s.po_uom_convert (um.unit_of_measure
                                                , msib.primary_unit_of_measure
                                                , ol.inventory_item_id
                                               ) 
       ),
	  ol.org_id ,
	  fnd_global.user_id ,
	  fnd_global.user_id ,
	  sysdate ,
	  sysdate ,
	  fnd_global.login_id
	FROM oe_order_lines_all ol
        , mtl_system_items_b msib
	    , mtl_units_of_measure_vl um
	WHERE ol.flow_status_code = ('AWAITING_SHIPPING')
	  AND ol.user_item_description <>'OUT_FOR_DELIVERY'
	  AND ol.inventory_item_id = msib.inventory_item_id
	  AND ol.ship_from_org_id = msib.organization_id
	  AND ol.order_quantity_uom = um.uom_code  
      AND open_flag = 'Y'
      AND cancelled_flag = 'N'	  
	  AND line_type_id       IN (1002); --Standard line 
	
	DBMS_OUTPUT.put_line ('TMS: 20151023-00037, Script 1 -After Insert, records Inserted standard lines : '||sql%rowcount);
	COMMIT;
	
	INSERT
	INTO XXWC.xxwc_oe_open_order_lines
	  (
	    line_id ,
	    header_id ,
	    line_type_id ,
	    inventory_item_id ,
	    organization_id ,
	    ordered_quantity ,
	    org_id ,
	    created_by ,
	    last_updated_by ,
	    creation_date ,
	    last_update_date ,
	    last_update_login
	  )
	SELECT ol.line_id ,
	  ol.header_id ,
	  ol.line_type_id ,
	  ol.inventory_item_id ,
	  ol.ship_from_org_id ,
	  (ol.ordered_quantity
                     * po_uom_s.po_uom_convert (um.unit_of_measure
                                                , msib.primary_unit_of_measure
                                                , ol.inventory_item_id
                                               ) 
       ),
	  ol.org_id ,
	  fnd_global.user_id ,
	  fnd_global.user_id ,
	  sysdate ,
	  sysdate ,
	  fnd_global.login_id
	FROM oe_order_lines_all ol
        , mtl_system_items_b msib
	    , mtl_units_of_measure_vl um
	WHERE ol.flow_status_code = ('AWAITING_SHIPPING')
	  AND ol.user_item_description NOT IN ('OUT_FOR_DELIVERY','OUT_FOR_DELIVERY/PARTIAL_BACKORDER')
	  AND ol.inventory_item_id = msib.inventory_item_id
	  AND ol.ship_from_org_id = msib.organization_id
	  AND ol.order_quantity_uom = um.uom_code    
	  AND open_flag = 'Y'
      AND cancelled_flag = 'N'
	  AND line_type_id       IN (1012);--internal line
	  
	 DBMS_OUTPUT.put_line ('TMS: 20151023-00037, Script 1 -After Insert, records Inserted internal lines : '||sql%rowcount);
	COMMIT;
	
	INSERT
	INTO XXWC.xxwc_oe_open_order_lines
	  (
	    line_id ,
	    header_id ,
	    line_type_id ,
	    inventory_item_id ,
	    organization_id ,
	    ordered_quantity ,
	    org_id ,
	    created_by ,
	    last_updated_by ,
	    creation_date ,
	    last_update_date ,
	    last_update_login
	  )
	SELECT ol.line_id ,
	  ol.header_id ,
	  ol.line_type_id ,
	  ol.inventory_item_id ,
	  ol.ship_from_org_id ,
	  ((ol.ordered_quantity-NVL(( SELECT xws.transaction_qty 
	                                FROM XXWC.XXWC_WSH_SHIPPING_STG xws
							       WHERE xws.line_id = ol.line_id
	                             )
							  ,0)
        )
        * po_uom_s.po_uom_convert (um.unit_of_measure
                                   , msib.primary_unit_of_measure
                                   , ol.inventory_item_id
                                   ) 
       ),
	  ol.org_id ,
	  fnd_global.user_id ,
	  fnd_global.user_id ,
	  sysdate ,
	  sysdate ,
	  fnd_global.login_id
	FROM oe_order_lines_all ol
       , mtl_system_items_b msib
	   , mtl_units_of_measure_vl um
	WHERE ol.flow_status_code = ('AWAITING_SHIPPING')
	AND   ol.user_item_description ='OUT_FOR_DELIVERY/PARTIAL_BACKORDER'
	AND   ol.inventory_item_id = msib.inventory_item_id
	AND   ol.ship_from_org_id = msib.organization_id
	AND   ol.order_quantity_uom = um.uom_code
	AND   open_flag = 'Y'
    AND   cancelled_flag = 'N'
   -- AND   ol.ordered_quantity > TO_NUMBER(ol.attribute11)
	AND   ol.line_type_id       IN (1012);--internal line

	DBMS_OUTPUT.put_line ('TMS: 20151023-00037, Script 2 -After Insert, records Inserted internal lines: '||sql%rowcount);
	COMMIT;
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20151023-00037, Errors ='||SQLERRM);
END;
/