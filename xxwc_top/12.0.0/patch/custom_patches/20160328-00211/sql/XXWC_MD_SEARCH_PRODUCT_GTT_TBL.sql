/*********************************************************************************
-- Table Name XXWC_MD_SEARCH_PRODUCT_GTT_TBL
-- *******************************************************************************
--
-- PURPOSE: Global temporary table used by AIS Lite form
-- HISTORY
-- ===========================================================================
-- ===========================================================================
-- VERSION DATE          AUTHOR(S)       DESCRIPTION
-- ------- -----------   --------------- -------------------------------------
-- 1.0     04-Apr-2016   Rakesh P        TMS# 20160328-00211 - Global temporary 
--                                       table used by AIS Lite form
*******************************************************************************/
DROP TABLE XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL
/

CREATE GLOBAL TEMPORARY TABLE XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL
(  SCORE                   NUMBER,
  INVENTORY_ITEM_ID       NUMBER,
  ORGANIZATION_ID         NUMBER,
  PARTNUMBER              VARCHAR2(40 BYTE),
  TYPE                    CHAR(7 BYTE),
  MANUFACTURERPARTNUMBER  VARCHAR2(150 BYTE),
  MANUFACTURER            VARCHAR2(150 BYTE),
  SEQUENCE                NUMBER,
  SHORTDESCRIPTION        VARCHAR2(240 BYTE),
  QUANTITYONHAND          NUMBER,
  open_sales_orders       NUMBER,
  LIST_PRICE              NUMBER,
  BUYABLE                 CHAR(1 BYTE),
  KEYWORD                 VARCHAR2(1000 BYTE),
  ITEM_TYPE               VARCHAR2(30 BYTE),
  CROSS_REFERENCE         VARCHAR2(4000 BYTE),
  PRIMARY_UOM_CODE        VARCHAR2(3 BYTE),
  SELLING_PRICE           NUMBER,
  MODIFIER                VARCHAR2(240 BYTE),
  MODIFIER_TYPE           VARCHAR2(200 BYTE)
)
ON COMMIT PRESERVE ROWS RESULT_CACHE (MODE DEFAULT) NOCACHE
/