   /*************************************************************************
   *   $Header XXWC_OE_OPEN_ORDER_LINES.sql $
   *   Module Name: XXWC_OE_OPEN_ORDER_LINES
   *
   *   PURPOSE:   To Maintain the open SO lines quantity
   *
   *   REVISIONS:
   *   Ver        Date         Author                Description
   *   ---------  ----------   ---------------       -------------------------
   *   1.0        25-Feb-2016  Manjula Chellappan    Initial Version
   *                                                  TMS# 20151023-00037/TMS# 20160328-00211 Shipping Extension Modification for Counter Order Removal and PUBD
   * ***************************************************************************/

CREATE TABLE "XXWC"."XXWC_OE_OPEN_ORDER_LINES"
(
   "LINE_ID"                 		NUMBER,     
   "HEADER_ID"               		NUMBER,  
   "LINE_TYPE_ID"           		NUMBER, 
   "INVENTORY_ITEM_ID"       		NUMBER,  
   "ORGANIZATION_ID"        		NUMBER,       
   "ORDERED_QUANTITY"        		NUMBER,
   "ORG_ID"                 		NUMBER 
                               DEFAULT NVL (
                                TO_NUMBER (
                                   DECODE (
                                      SUBSTRB (USERENV ('CLIENT_INFO'), 1, 1),
                                      ' ', NULL,
                                      SUBSTRB (USERENV ('CLIENT_INFO'),
                                               1,
                                               10))),
                                -99)
                     NOT NULL, 
   "CREATED_BY"              		NUMBER,
   "LAST_UPDATED_BY"         		NUMBER,
   "CREATION_DATE"           		DATE,
   "LAST_UPDATE_DATE"        		DATE,
   "LAST_UPDATE_LOGIN"       		NUMBER
)
SEGMENT CREATION DEFERRED
PCTFREE 10
PCTUSED 40
INITRANS 1
MAXTRANS 255
NOCOMPRESS
LOGGING
TABLESPACE "XXWC_DATA";

CREATE INDEX XXWC.XXWC_OE_OPEN_ORDER_LINES_N1 ON XXWC.XXWC_OE_OPEN_ORDER_LINES (inventory_item_id, organization_id);
