  /********************************************************************************
  FILE NAME: XXWC_AUTH_BUYER_INFO_HW_TBL.sql
  
  PROGRAM TYPE: Auth Buyer for online sales order form
  
  PURPOSE: Auth Buyer for online sales order form
  
  HISTORY
  ==========================================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- ----------------------------------------------------
  1.0     03/07/2017    Rakesh P.       TMS#20170111-00059-Hand Write Oracle API for Web Service
                                        Initial Version
  *******************************************************************************************/
CREATE TABLE XXWC.XXWC_AUTH_BUYER_INFO_HW_TBL
(
  CUST_ACCOUNT_ID         NUMBER(15)               NOT NULL,
  AUTH_BUYER              VARCHAR2(150 BYTE),
  auth_buyer_id           NUMBER(15),
  Status                  VARCHAR2(100 BYTE)       NOT NULL,
  CREATION_DATE           DATE                     NOT NULL,
  CREATED_BY              NUMBER(15)               NOT NULL,
  LAST_UPDATE_DATE        DATE                     NOT NULL,
  LAST_UPDATED_BY         NUMBER(15)               NOT NULL,
  REQUEST_ID              NUMBER(15),
  ATTRIBUTE1              VARCHAR2(150 BYTE),
  ATTRIBUTE2              VARCHAR2(150 BYTE),
  ATTRIBUTE3              VARCHAR2(150 BYTE),
  ATTRIBUTE4              VARCHAR2(150 BYTE),
  ATTRIBUTE5              VARCHAR2(150 BYTE),
  ATTRIBUTE6              VARCHAR2(150 BYTE),
  ATTRIBUTE7              VARCHAR2(150 BYTE),
  ATTRIBUTE8              VARCHAR2(150 BYTE),
  ATTRIBUTE9              VARCHAR2(150 BYTE),
  ATTRIBUTE10             VARCHAR2(150 BYTE)
);

GRANT SELECT ON XXWC.XXWC_AUTH_BUYER_INFO_HW_TBL TO INTERFACE_MSSQL;