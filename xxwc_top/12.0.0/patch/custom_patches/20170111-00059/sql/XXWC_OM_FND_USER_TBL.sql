  /********************************************************************************
  FILE NAME: XXWC_OM_FND_USER.sql
  
  PROGRAM TYPE: User table for online sales order form
  
  PURPOSE: User table type for online sales order form
  
  HISTORY
  ==========================================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- ----------------------------------------------------
  1.0     03/07/2017    Rakesh P.       TMS#20170111-00059-Hand Write Oracle API for Web Service
                                        Initial Version
  *******************************************************************************************/
CREATE TABLE XXWC.XXWC_OM_FND_USER
(
  USER_ID                        NUMBER(15)     NOT NULL,
  USER_NAME                      VARCHAR2(100 BYTE) NOT NULL,
  LAST_UPDATE_DATE               DATE           NOT NULL,
  LAST_UPDATED_BY                NUMBER(15)     NOT NULL,
  CREATION_DATE                  DATE           NOT NULL,
  CREATED_BY                     NUMBER(15)     NOT NULL,
  LAST_UPDATE_LOGIN              NUMBER(15),
  ENCRYPTED_FOUNDATION_PASSWORD  VARCHAR2(100 BYTE) NOT NULL,
  ENCRYPTED_USER_PASSWORD        VARCHAR2(100 BYTE) NOT NULL,
  SESSION_NUMBER                 NUMBER         NOT NULL,
  START_DATE                     DATE           NOT NULL,
  END_DATE                       DATE,
  DESCRIPTION                    VARCHAR2(240 BYTE),
  LAST_LOGON_DATE                DATE,
  PASSWORD_DATE                  DATE,
  PASSWORD_ACCESSES_LEFT         NUMBER(15),
  PASSWORD_LIFESPAN_ACCESSES     NUMBER(15),
  PASSWORD_LIFESPAN_DAYS         NUMBER(15),
  EMPLOYEE_ID                    NUMBER(15),
  EMAIL_ADDRESS                  VARCHAR2(240 BYTE),
  FAX                            VARCHAR2(80 BYTE),
  CUSTOMER_ID                    NUMBER(15),
  SUPPLIER_ID                    NUMBER(15),
  WEB_PASSWORD                   VARCHAR2(240 BYTE),
  USER_GUID                      RAW(16),
  GCN_CODE_COMBINATION_ID        NUMBER(15),
  PERSON_PARTY_ID                NUMBER,
  USER_Warehouse_id              NUMBER
);

GRANT SELECT ON XXWC.XXWC_OM_FND_USER TO INTERFACE_MSSQL;