CREATE OR REPLACE PACKAGE APPS.XXWC_OM_SO_CREATE IS

 /*************************************************************************
     $Header XXWC_OM_SO_CREATE $
     Module Name: XXWC_OM_SO_CREATE.pks

     PURPOSE:   Handwrite Project	
    
	Input Parameters:  a. p_order_header_rec - p_order_header_rec is a record type 
                                       be used for order header information.
									   
                       b. p_order_lines_tbl - p_order_lines_tbl is a table type       
                                         will be used to store line level information.
                       
	Output Parameters: o_order_out_rec is a record type and will have following column
	                   o_error_code - 'E' - Error or 'S'- Success
                       o_error_msg  - Error message, reason for the failure.
					   
     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        11/09/2016  Rakesh Patel            Handwrite Project	
**************************************************************************/
   FUNCTION get_onhand (
       p_inventory_item_id   IN NUMBER
      ,p_organization_id     IN NUMBER
      ,p_base_uom            IN VARCHAR2
      ,p_ord_uom             IN VARCHAR2
      ,p_return_type         IN VARCHAR2 
    ) RETURN NUMBER;

   PROCEDURE CREATE_SALES_ORDER
   (
     p_order_header_rec     IN  xxwc.xxwc_order_header_rec
    ,p_order_lines_tbl      IN  xxwc.xxwc_order_lines_tbl
    --,o_order_out_rec        OUT  xxwc.xxwc_order_out_rec
    ,o_status               OUT VARCHAR2
    ,o_order_number         OUT NUMBER
    ,o_message              OUT VARCHAR2    
   );
   
   /*************************************************************************
     PROCEDURE GET_CUSTOMER_INFO
    
	Input Parameters:  a. p_cust_search_rec - customer search data.
									   
                       b. o_cust_data_tbl_out_rec - customer data
     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        11/09/2016  Rakesh Patel            Handwrite Project	
**************************************************************************/
 /**  PROCEDURE GET_CUSTOMER_INFO
   (
      p_cust_account_code       IN VARCHAR2
    -- p_cust_search_rec         IN  xxwc.xxwc_customer_search_rec
    ,o_cust_data_tbl_out_rec   OUT xxwc.XXWC_CUSTOMER_DATA_OUT_TBL
   );
   
   FUNCTION GET_CUSTOMER_INFO (p_cust_account_code IN VARCHAR2) return CLOB;**/
   
   FUNCTION GET_CUSTOMER_INFO_RS(p_cust_account_code IN VARCHAR2
                                ,p_customer_name     IN VARCHAR2) return sys_refcursor;
   
   FUNCTION GET_BRANCH_INFO ( p_user_nt_id IN VARCHAR2 ) return sys_refcursor;
   
   FUNCTION GET_BRANCH_INFO(  p_user_nt_id IN VARCHAR2
                             ,p_email_id IN VARCHAR2 ) return sys_refcursor;
							 
   FUNCTION GET_BRANCH_DETAILS(  p_organization_id IN VARCHAR2 DEFAULT NULL ) return sys_refcursor;      
   
   FUNCTION GET_CUSTOMER_CONTACTS(p_cust_account_id IN NUMBER) return sys_refcursor;
   
   FUNCTION GET_AUTH_BUYER(p_cust_account_id IN NUMBER) return sys_refcursor;
   
   PROCEDURE GET_PRODUCT_INFO( p_branch_id   IN NUMBER
                             ,p_customer_id IN NUMBER
                             ,p_jobsiteid   IN NUMBER
                             ,p_item_search IN VARCHAR2
                             ,p_filter      IN VARCHAR2
                             ,p_Sort        IN VARCHAR2     
                             ,o_cur_output  OUT SYS_REFCURSOR
                             );
   
   PROCEDURE GET_PRODUCT_INFO( p_branch_id   IN NUMBER
                             ,p_customer_id IN NUMBER
                             ,p_jobsiteid   IN NUMBER
                             ,p_item_search IN VARCHAR2
                             ,p_filter      IN VARCHAR2
                             ,p_Sort        IN VARCHAR2     
                             );                                                  

   PROCEDURE line_pricing_data ( p_organization_id NUMBER
                              , p_cust_account_id NUMBER
                              , p_site_use_id NUMBER
                             );
                             
   FUNCTION get_list_price(   i_organization_id   in number
                             , i_inventory_item_id in NUMBER
                             ) return NUMBER;

   FUNCTION GET_ORDER_INFO(  p_order_number     IN NUMBER
                            ,p_order_type_id    IN NUMBER   
                            ,p_created_by       IN NUMBER  
                            ,p_customer_number  IN VARCHAR2
                            ,p_order_from_date  IN VARCHAR2
                            ,p_order_to_date    IN VARCHAR2  
                            ,p_Warehouse_id     IN NUMBER
                            ,p_status           IN VARCHAR2
                           ) return sys_refcursor;
  
  FUNCTION GET_LINE_INFO(  p_header_id     IN NUMBER
                           ) return sys_refcursor;
                                                    
  PROCEDURE CALCUALTE_TAX
    (
     p_order_header_rec     IN  OE_Order_PUB.Header_Rec_Type
    ,p_order_line_rec       IN  OE_Order_PUB.Line_Rec_Type
    ,o_x_tax_value          OUT NUMBER 
   );
   
   PROCEDURE create_contact (p_customer_id       IN     NUMBER,
                             p_first_name        IN     VARCHAR2,
                             p_last_name         IN     VARCHAR2,
                             p_contact_num       IN     VARCHAR2,
                             p_email_id          IN     VARCHAR2,
                             p_fax_num           IN     VARCHAR2,
                             p_cust_contact_id      OUT NUMBER,
                             p_error_message        OUT VARCHAR2);
   
   /*************************************************************************
	Input Parameters:  a. p_xxwc_address_rec - address info.
                       
	Output Parameters: o_order_out_rec is a record type and will have following column
	                   o_error_code - 'E' - Error or 'S'- Success
                       o_error_msg  - Error message, reason for the failure.
**************************************************************************/
   PROCEDURE CREATE_JOB_SITE
   (
     p_xxwc_address_rec     IN  xxwc.xxwc_address_rec
    ,o_status               OUT VARCHAR2
    ,o_ship_to_site_use_id      OUT NUMBER
    ,o_bill_to_site_use_id      OUT NUMBER
    ,o_message              OUT VARCHAR2    
   );
             
    PROCEDURE CREATE_ITEM (
      p_Parts_On_Fly_Rec   IN OUT NOCOPY xxwc_Parts_On_Fly_Rec,
      x_Return_Status         OUT NOCOPY VARCHAR2,
      x_Msg_Data              OUT NOCOPY VARCHAR2,
      x_Msg_Count             OUT NOCOPY NUMBER,
      x_part_number           OUT NOCOPY VARCHAR2,
      x_inventory_item_id     OUT NOCOPY NUMBER,
      x_list_price            OUT NOCOPY NUMBER,
      x_primary_uom_code      OUT NOCOPY VARCHAR2
    );                                                  
    
    FUNCTION get_sales_rep_id (p_cust_account_id IN NUMBER) RETURN NUMBER;
    
    FUNCTION get_payment_term_id (p_cust_account_id IN NUMBER) RETURN NUMBER;
    
    FUNCTION get_payment_term_nm (p_cust_account_id IN NUMBER) RETURN VARCHAR2;
    
    FUNCTION payment_term (p_term_id IN NUMBER)  RETURN VARCHAR2;
           
END XXWC_OM_SO_CREATE;
/