CREATE OR REPLACE PACKAGE BODY APPS.xxwc_customer_extract_hw_pkg
AS
  /********************************************************************************
  FILE NAME: xxwc_customer_extract_hw_pkg.sql
  
  PROGRAM TYPE: Customer extract hw for online sales order form
  
  PURPOSE: Customer Info / Customer Job Info / Product + Description /Product + warehouse + market price /User info
  
  HISTORY
  ==========================================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- ----------------------------------------------------
  1.0     03/07/2017    Rakesh P.       TMS#20170111-00059-Hand Write Oracle API for Web Service
                                        Initial Version
  *******************************************************************************************/
   xxwc_error                  EXCEPTION;

/********************************************************************************
ProcedureName : customer_info_full
Purpose       : API to generate Full Customer Info
HISTORY
===============================================================================
VERSION DATE          AUTHOR(S)       DESCRIPTION
------- -----------   --------------- -----------------------------------------
 1.0    16-Feb-2017 Rakesh Patel    Initial Version
********************************************************************************/
  -- customer extract logic for full extracts
   PROCEDURE customer_info_full (errbuf OUT VARCHAR2, retcode OUT VARCHAR2)
   IS
      l_cnt             NUMBER := 0;
      l_role_cnt        NUMBER := 0;
      l_group_cnt       NUMBER := 0;
      l_ownerdn         VARCHAR2 (240);
      l_err_callfrom    VARCHAR2 (75) := 'XXWC_CUSTOMER_EXTRACT_HW_PKG.Customer_Info_Full';
      l_err_callpoint   VARCHAR2 (75) := 'START';
      l_distro_list     VARCHAR2 (75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
      l_message         VARCHAR2 (2000);
   BEGIN
      errbuf := 'Success';
      retcode := 0;

      fnd_file.put_line (fnd_file.LOG,'Creating Customer Info date : ' || sysdate);

   INSERT INTO XXWC.XXWC_CUSTOMER_INFO_HW_TBL
( 
  CUSTOMER_NAME           ,--PARTY_NAME 
  CUST_ACCOUNT_ID         ,
  PARTY_ID                ,
  CUST_ACCOUNT_CODE       , --ACCOUNT_NUMBER
  MAIN_PHONE_NUMBER       ,
  MAIN_EMAIL              ,
  CUST_ACCOUNT_STATUS     ,
  SALES_REP_NAME          , 
  SALES_REP_ID            , --sales rep id
  payment_term_id         , --payment term id
  payment_term_name       , --payment term name
  AUTH_BUYER_REQUIRED     , --auth buyer required 
  CREATION_DATE           ,
  CREATED_BY              ,
  LAST_UPDATE_DATE        ,
  LAST_UPDATED_BY         
  )
 SELECT PARTY.PARTY_NAME customer_name,
       CUST_ACCT.cust_account_id,
       PARTY.party_id,
       CUST_ACCT.account_number CUST_ACCOUNT_CODE,
       SUBSTR (xxwc_mv_routines_pkg.get_phone_fax_number ('PHONE',
                                                          PARTY.party_id,
                                                          NULL --, hcasa.party_site_id
                                                              ,
                                                          'GEN'),
               1,
               80)
          main_phone_number,
       PARTY.email_address main_email,
       (DECODE (CUST_ACCT.status,
                'A', 'Active',
                'I', 'Inactive',
                CUST_ACCT.status))
          cust_account_status,
          XXWC_B2B_SO_IB_PKG.get_sales_rep (CUST_ACCT.cust_account_id)    sales_rep_name,
          XXWC_OM_SO_CREATE.get_sales_rep_id (CUST_ACCT.cust_account_id ) sales_rep_id,--sales rep id
          XXWC_OM_SO_CREATE.get_payment_term_id (CUST_ACCT.cust_account_id) payment_term_id,  --payment_term_id  
          XXWC_OM_SO_CREATE.get_payment_term_nm (CUST_ACCT.cust_account_id) payment_term_name,  --payment_term_name
          CUST_ACCT.attribute7, --auth buyer required 
          sysdate,
          -1,
          sysdate,
          -1
  FROM HZ_PARTIES PARTY, HZ_CUST_ACCOUNTS CUST_ACCT
 WHERE     1 = 1
       AND CUST_ACCT.PARTY_ID = PARTY.PARTY_ID
       AND CUST_ACCT.status = 'A';
      COMMIT;
      fnd_file.put_line (fnd_file.LOG, 'Process Complete');
   EXCEPTION
      WHEN OTHERS
      THEN
         errbuf := 'Error';
         retcode := 2;
         fnd_file.put_line (fnd_file.LOG, SQLERRM);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_err_callfrom
           ,p_calling             => l_err_callpoint
           ,p_ora_error_msg       => SQLERRM
           ,p_error_desc          => l_message
           ,p_distribution_list   => l_distro_list
           ,p_module              => 'XXWC');
   END customer_info_full;

/********************************************************************************
ProcedureName : branch_info_full
Purpose       : API to generate Full Customer Info
HISTORY
===============================================================================
VERSION DATE          AUTHOR(S)       DESCRIPTION
------- -----------   --------------- -----------------------------------------
 1.0    22-Feb-2017 Rakesh Patel    Initial Version
********************************************************************************/
   PROCEDURE branch_info_full (errbuf OUT VARCHAR2, retcode OUT VARCHAR2)
   IS
      l_cnt             NUMBER := 0;
      l_role_cnt        NUMBER := 0;
      l_group_cnt       NUMBER := 0;
      l_ownerdn         VARCHAR2 (240);
      l_err_callfrom    VARCHAR2 (75) := 'XXWC_CUSTOMER_EXTRACT_HW_PKG.branch_info_full';
      l_err_callpoint   VARCHAR2 (75) := 'START';
      l_distro_list     VARCHAR2 (75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
      l_message         VARCHAR2 (2000);
   BEGIN
      errbuf := 'Success';
      retcode := 0;

      fnd_file.put_line (fnd_file.LOG,'Creating Branch Info date : ' || sysdate);

   INSERT INTO XXWC.XXWC_BRANCH_INFO_HW_TBL
( 
  ORGANIZATION_ID      ,
  ORGANIZATION_CODE    ,
  LOCATION_CODE        ,
  BRANCH_STREET        ,
  BRANCH_CITY          ,
  BRANCH_TEL1          ,
  BRANCH_TEL2          ,
  ATTRIBUTE6           ,
  ATTRIBUTE7           ,
  ATTRIBUTE8           ,
  ATTRIBUTE9           ,
  ATTRIBUTE10          ,
  ATTRIBUTE11          ,
  ATTRIBUTE12          ,
  ATTRIBUTE13          ,
  ATTRIBUTE14          ,
  ATTRIBUTE15          ,
  CREATION_DATE        ,
  CREATED_BY           ,
  LAST_UPDATE_DATE     ,
  LAST_UPDATED_BY      ,
  REQUEST_ID           
  )
 SELECT  mp.organization_id
       ,mp.organization_code
       ,hl.LOCATION_CODE
       ,hl.address_line_1 branch_street
       ,DECODE (hl.town_or_city, NULL, NULL, hl.town_or_city || ','||'') ||
       DECODE (hl.region_2, NULL, hl.region_2 || ','||'' , hl.region_2 || ','||'' ) || 
       DECODE (hl.postal_code, NULL, NULL, hl.postal_code) branch_city
      ,hl.telephone_number_1 branch_tel1
      ,hl.telephone_number_2 branch_tel2
       ,mp.attribute6
       ,mp.attribute7
       ,mp.attribute8
       ,mp.attribute9
       ,mp.attribute10
       ,mp.attribute11
       ,mp.attribute12
       ,mp.attribute13
       ,mp.attribute14
       ,mp.attribute15
       ,sysdate
       ,-1
       ,sysdate
       ,-1
       ,fnd_profile.value('CONCURRENT_REQUEST_ID')
FROM apps.mtl_parameters mp
   , apps.hr_all_organization_units haou
   , apps.hr_locations_all hl
WHERE mp.organization_id        = haou.organization_id(+)
  AND haou.location_id          = hl.location_id
  AND mp.master_organization_id = 222;
      COMMIT;
      fnd_file.put_line (fnd_file.LOG, 'Process Complete');
   EXCEPTION
      WHEN OTHERS
      THEN
         errbuf := 'Error';
         retcode := 2;
         fnd_file.put_line (fnd_file.LOG, SQLERRM);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_err_callfrom
           ,p_calling             => l_err_callpoint
           ,p_ora_error_msg       => SQLERRM
           ,p_error_desc          => l_message
           ,p_distribution_list   => l_distro_list
           ,p_module              => 'XXWC');
   END branch_info_full;

/********************************************************************************
ProcedureName : customer_job_info_full
Purpose       : API to generate Full Customer Site Info
HISTORY
===============================================================================
VERSION DATE          AUTHOR(S)       DESCRIPTION
------- -----------   --------------- -----------------------------------------
 1.0    19-Feb-2017 Rakesh Patel    Initial Version
********************************************************************************/
  -- customer extract logic for full extracts
   PROCEDURE customer_job_info_full (errbuf OUT VARCHAR2, retcode OUT VARCHAR2)
   IS
      l_cnt             NUMBER := 0;
      l_role_cnt        NUMBER := 0;
      l_group_cnt       NUMBER := 0;
      l_ownerdn         VARCHAR2 (240);
      l_err_callfrom    VARCHAR2 (75) := 'XXWC_CUSTOMER_EXTRACT_HW_PKG.customer_job_info_full';
      l_err_callpoint   VARCHAR2 (75) := 'START';
      l_distro_list     VARCHAR2 (75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
      l_message         VARCHAR2 (2000);
   BEGIN
      errbuf := 'Success';
      retcode := 0;

      fnd_file.put_line (fnd_file.LOG,'Creating customer_job_info_full: ' || sysdate);

   INSERT INTO XXWC.XXWC_CUSTOMER_JOB_INFO_HW_TBL
( 
  CUST_ACCOUNT_ID         ,
  COUNTRY                 ,
  ADDRESS1                ,
  ADDRESS2                ,
  ADDRESS3                ,
  ADDRESS4                ,
  CITY                    ,
  POSTAL_CODE             ,
  STATE_PROVINCE          ,
  COUNTY                  ,
  SITE_STATUS             ,
  HCASA_CUST_ACCT_SITE_ID , 
  SHIP_TO_SITE_USE_ID     ,
  BILL_TO_SITE_USE_ID     ,
  SITE_NAME               ,   
  PRIMARY_SALESREP_ID     ,
  sales_rep_name          ,
  CREATION_DATE           ,
  CREATED_BY              ,
  LAST_UPDATE_DATE        ,
  LAST_UPDATED_BY         
  )
 SELECT  CUST_ACCT.cust_account_id,
         LOC.COUNTRY,
         LOC.ADDRESS1,
         LOC.ADDRESS2,
         LOC.ADDRESS3,
         LOC.ADDRESS4,
         LOC.CITY,
         LOC.Postal_Code,
         (CASE
               WHEN LENGTH (NVL (LOC.state, LOC.province)) > 2 THEN NULL
                                 ELSE NVL (LOC.state, LOC.province)
         END)  state_province,
         LOC.COUNTY,
         (DECODE (acct_site.status,  'A', 'Active',  'I', 'Inactive',  acct_site.status)) SITE_STATUS,
         acct_site.cust_acct_site_id hcasa_cust_acct_site_id,
         SITE.SITE_USE_ID SHIP_TO_SITE_USE_ID,
         NVL((select site_use_id
                                  from hz_Cust_site_uses_all
                                   where site_use_code = 'BILL_TO'
                                    and cust_acct_site_id = acct_site.cust_acct_site_id
                                    AND status='A' ),
                                    (select hcsu.site_use_id
                                  from hz_Cust_site_uses_all hcsu
                                     , hz_Cust_acct_sites_all hcas
                                   where hcsu.site_use_code = 'BILL_TO'
                                    and hcsu.cust_acct_site_id = hcas.cust_acct_site_id
                                    AND hcas.cust_account_id  = acct_site.cust_account_id
                                    AND hcsu.status='A'
                                    AND hcas.status='A'
                                    AND ROWNUM = 1 )) BILL_TO_SITE_USE_ID,
         SITE.location SITE_NAME,
         (SELECT PRIMARY_SALESREP_ID
           FROM hz_Cust_site_uses_all 
          WHERE site_use_code = 'BILL_TO'
            AND cust_acct_site_id = acct_site.cust_acct_site_id 
            AND status='A' )  PRIMARY_SALESREP_ID,
          XXWC_B2B_SO_IB_PKG.get_sales_rep (CUST_ACCT.cust_account_id ) sales_rep_name,
          sysdate,
          -1,
          sysdate,
          -1
     FROM
         HZ_CUST_ACCT_SITES    ACCT_SITE,
         HZ_PARTY_SITES        PARTY_SITE,
         HZ_LOCATIONS          LOC,
         HZ_CUST_SITE_USES_ALL SITE,
         HZ_CUST_ACCOUNTS      CUST_ACCT
     WHERE SITE.SITE_USE_CODE         = 'SHIP_TO'
       AND SITE.CUST_ACCT_SITE_ID     = ACCT_SITE.CUST_ACCT_SITE_ID
       AND ACCT_SITE.PARTY_SITE_ID    = PARTY_SITE.PARTY_SITE_ID
       AND PARTY_SITE.LOCATION_ID     = LOC.LOCATION_ID
       AND acct_site.status='A'
       AND ACCT_SITE.CUST_ACCOUNT_ID=CUST_ACCT.CUST_ACCOUNT_ID
       AND CUST_ACCT.status='A'
       AND site.status='A';
      COMMIT;
      fnd_file.put_line (fnd_file.LOG, 'customer_job_info_full extract Complete: ' || sysdate);
   EXCEPTION
      WHEN OTHERS
      THEN
         errbuf := 'Error';
         retcode := 2;
         fnd_file.put_line (fnd_file.LOG, SQLERRM);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_err_callfrom
           ,p_calling             => l_err_callpoint
           ,p_ora_error_msg       => SQLERRM
           ,p_error_desc          => l_message
           ,p_distribution_list   => l_distro_list
           ,p_module              => 'XXWC');
   END customer_job_info_full;
   
/********************************************************************************
ProcedureName : Products_Info_full
Purpose       : API to generate Full Product Info
HISTORY
===============================================================================
VERSION DATE          AUTHOR(S)       DESCRIPTION
------- -----------   --------------- -----------------------------------------
 1.0    20-Feb-2017 Rakesh Patel    Initial Version
********************************************************************************/
   PROCEDURE Products_Info_full (errbuf OUT VARCHAR2, retcode OUT VARCHAR2)
   IS
      l_cnt             NUMBER := 0;
      l_role_cnt        NUMBER := 0;
      l_group_cnt       NUMBER := 0;
      l_ownerdn         VARCHAR2 (240);
      l_err_callfrom    VARCHAR2 (75) := 'XXWC_CUSTOMER_EXTRACT_HW_PKG.products_full';
      l_err_callpoint   VARCHAR2 (75) := 'START';
      l_distro_list     VARCHAR2 (75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
      l_message         VARCHAR2 (2000);
   BEGIN
      errbuf := 'Success';
      retcode := 0;

      fnd_file.put_line (fnd_file.LOG,'Creating Product Info date : ' || sysdate);

   INSERT INTO XXWC.XXWC_PRODUCTS_HW_TBL
( 
  PARTNUMBER           , 
  INVENTORY_ITEM_ID    ,
  ORGANIZATION_ID      ,
  PART_DESCRIPTION     ,
  PRIMARY_UOM_CODE     ,
  CREATION_DATE        ,
  CREATED_BY           ,
  LAST_UPDATE_DATE     ,
  LAST_UPDATED_BY         
  )
 SELECT partnumber,
       inventory_item_id,
       organization_id,
       shortdescription,
       primary_uom_code,
       sysdate,
       -1,
       sysdate,
       -1
   FROM APPS.XXWC_MD_SEARCH_PRODUCTS_VW;
    
   COMMIT;

      fnd_file.put_line (fnd_file.LOG, 'Process Complete');
   EXCEPTION
      WHEN OTHERS
      THEN
         errbuf := 'Error';
         retcode := 2;
         fnd_file.put_line (fnd_file.LOG, SQLERRM);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_err_callfrom
           ,p_calling             => l_err_callpoint
           ,p_ora_error_msg       => SQLERRM
           ,p_error_desc          => l_message
           ,p_distribution_list   => l_distro_list
           ,p_module              => 'XXWC');
   END Products_Info_full;   

/********************************************************************************
ProcedureName : Products_Price_Info_full
Purpose       : API to generate Full Product Price Info
HISTORY
===============================================================================
VERSION DATE          AUTHOR(S)       DESCRIPTION
------- -----------   --------------- -----------------------------------------
 1.0    21-Feb-2017 Rakesh Patel    Initial Version
********************************************************************************/
   PROCEDURE Products_Price_Info_full ( errbuf            OUT VARCHAR2
                                       ,retcode           OUT VARCHAR2
                                       ,p_organization_id IN  NUMBER)
   
   IS
      l_cnt             NUMBER := 0;
      l_role_cnt        NUMBER := 0;
      l_group_cnt       NUMBER := 0;
      l_ownerdn         VARCHAR2 (240);
      l_err_callfrom    VARCHAR2 (75) := 'XXWC_CUSTOMER_EXTRACT_HW_PKG.Products_Price_Info_full';
      l_err_callpoint   VARCHAR2 (75) := 'START';
      l_distro_list     VARCHAR2 (75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
      l_message         VARCHAR2 (2000);
   BEGIN
      errbuf := 'Success';
      retcode := 0;
     
      fnd_file.put_line (fnd_file.LOG,'Creating Product Info date : ' || sysdate);
      
      INSERT INTO XXWC.XXWC_PRODUCTS_PRICE_HW_TBL
        ( 
        INVENTORY_ITEM_ID    ,
        ORGANIZATION_ID      ,
        MARKET_PRICE         ,
        ON_HAND_QTY          ,
        OPEN_ORDERS_QTY      ,
        CREATION_DATE        ,
        CREATED_BY           ,
        LAST_UPDATE_DATE     ,
        LAST_UPDATED_BY      ,
        request_id
        )
         SELECT msi.inventory_item_id,
             msi.organization_id,
             xxwc_qp_market_price_util_pkg.get_market_list_price(msi.inventory_item_id, msi.organization_id),
             xxwc_ascp_scwb_pkg.get_on_hand (msi.inventory_item_id,msi.organization_id,'G')ON_HAND_QTY,
             NVL(xxwc_ascp_scwb_pkg.get_open_orders_qty(msi.inventory_item_id, msi.organization_id),0) open_orders_qty,
             SYSDATE,
             -1,
             SYSDATE,
             -1,
             fnd_global.conc_request_id 
         FROM MTL_SYSTEM_ITEMS_B msi
        WHERE msi.organization_id = p_organization_id
          AND EXISTS ( SELECT 1
                     FROM XXWC.XXWC_PRODUCTS_HW_TBL xmsp
                    WHERE xmsp.PARTNUMBER = msi.segment1
                      AND xmsp.organization_id = 222);
     
         UPDATE XXWC.XXWC_PRODUCTS_PRICE_HW_TBL
            SET AVAILABLE_QTY	= NVL (ON_HAND_QTY, 0) - NVL (OPEN_ORDERS_QTY, 0)
          WHERE organization_id = p_organization_id;
         
      COMMIT;
   
      fnd_file.put_line (fnd_file.LOG, 'Process Products_Price_Info_full Complete');
   EXCEPTION
      WHEN OTHERS
      THEN
         errbuf := 'Error';
         retcode := 2;
         fnd_file.put_line (fnd_file.LOG, SQLERRM);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_err_callfrom
           ,p_calling             => l_err_callpoint
           ,p_ora_error_msg       => SQLERRM
           ,p_error_desc          => l_message
           ,p_distribution_list   => l_distro_list
           ,p_module              => 'XXWC');
   END Products_Price_Info_full;   
 
/********************************************************************************
ProcedureName : Auth_buyer_Info_full
Purpose       : API to generate Full Product Price Info
HISTORY
===============================================================================
VERSION DATE          AUTHOR(S)       DESCRIPTION
------- -----------   --------------- -----------------------------------------
 1.0    21-Feb-2017 Rakesh Patel    Initial Version
********************************************************************************/
   PROCEDURE Auth_buyer_Info_full (     errbuf            OUT VARCHAR2
                                       ,retcode           OUT VARCHAR2
)
   
   IS
      l_cnt             NUMBER := 0;
      l_role_cnt        NUMBER := 0;
      l_group_cnt       NUMBER := 0;
      l_ownerdn         VARCHAR2 (240);
      l_err_callfrom    VARCHAR2 (75) := 'XXWC_CUSTOMER_EXTRACT_HW_PKG.Auth_buyer_Info_full';
      l_err_callpoint   VARCHAR2 (75) := 'START';
      l_distro_list     VARCHAR2 (75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
      l_message         VARCHAR2 (2000);
   BEGIN
      errbuf := 'Success';
      retcode := 0;
     
      fnd_file.put_line (fnd_file.LOG,'Creating Auth Buyer info date : ' || sysdate);
      
      mo_global.set_policy_context ('S', 162);
      mo_global.init ('ONT');
   
      INSERT INTO XXWC.XXWC_AUTH_BUYER_INFO_HW_TBL
        ( 
        CUST_ACCOUNT_ID      ,
        AUTH_BUYER           ,
        auth_buyer_id        ,
        Status               ,
        CREATION_DATE        ,
        CREATED_BY           ,
        LAST_UPDATE_DATE     ,
        LAST_UPDATED_BY      ,
        request_id
        )
         SELECT cont.customer_id,
                hp.party_name auth_buyer, 
                cont.contact_id auth_buyer_id, 
                DECODE(cont.status, 'I','Inactive','A','Active', cont.status) Status,
                SYSDATE,
                -1,
               SYSDATE,
               -1,
               fnd_global.conc_request_id 
           FROM ar_contacts_v cont, hz_parties hp, hz_role_responsibility cont_role 
          WHERE cont.contact_party_id = hp.party_id 
            AND cont.contact_id = cont_role.cust_account_role_id 
            AND cont_role.responsibility_type = 'AUTH_BUYER' AND NVL(cont.status,'A') = 'A';
         
      COMMIT;
   
      fnd_file.put_line (fnd_file.LOG, 'Process Auth_buyer_Info_full Complete');
   EXCEPTION
      WHEN OTHERS
      THEN
         errbuf := 'Error';
         retcode := 2;
         fnd_file.put_line (fnd_file.LOG, SQLERRM);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_err_callfrom
           ,p_calling             => l_err_callpoint
           ,p_ora_error_msg       => SQLERRM
           ,p_error_desc          => l_message
           ,p_distribution_list   => l_distro_list
           ,p_module              => 'XXWC');
   END Auth_buyer_Info_full;   
     
/********************************************************************************
ProcedureName : customer_contacts_info_full
Purpose       : API to generate Full Customer Site Info
HISTORY
===============================================================================
VERSION DATE          AUTHOR(S)       DESCRIPTION
------- -----------   --------------- -----------------------------------------
 1.0    21-Feb-2017 Rakesh Patel    Initial Version
********************************************************************************/
   PROCEDURE customer_contacts_info_full (errbuf OUT VARCHAR2, retcode OUT VARCHAR2)
   IS
      l_cnt             NUMBER := 0;
      l_role_cnt        NUMBER := 0;
      l_group_cnt       NUMBER := 0;
      l_ownerdn         VARCHAR2 (240);
      l_err_callfrom    VARCHAR2 (75) := 'XXWC_CUSTOMER_EXTRACT_HW_PKG.customer_contact_info_full';
      l_err_callpoint   VARCHAR2 (75) := 'START';
      l_distro_list     VARCHAR2 (75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
      l_message         VARCHAR2 (2000);
   BEGIN
      errbuf := 'Success';
      retcode := 0;

      fnd_file.put_line (fnd_file.LOG,'Creating customer_contact_info_full: ' || sysdate);

   INSERT INTO XXWC.XXWC_CUSTOMER_CONT_INFO_HW_TBL
( 
  CUST_ACCOUNT_ID         ,
  PERSON_FIRST_NAME       ,
  PERSON_LAST_NAME        ,
  EMAIL_ADDRESS           ,
  PRIMARY_PHONE_NUMBER    ,
  cust_account_role_id    ,
  CONTACT_STATUS          ,
  CREATION_DATE           ,
  CREATED_BY              ,
  LAST_UPDATE_DATE        ,
  LAST_UPDATED_BY         
  )
 SELECT role_acct.cust_account_id, 
       party.PERSON_FIRST_NAME,
       party.PERSON_LAST_NAME,
       PARTY.EMAIL_ADDRESS,
       party.PRIMARY_PHONE_NUMBER,
       acct_role.cust_account_role_id,
       (DECODE (acct_role.status,  'A', 'Active',  'I', 'Inactive',  acct_role.status)) CONTACT_STATUS,
         sysdate,
       -1,
       sysdate,
       -1
  FROM hz_cust_account_roles acct_role,
       hz_parties party,
       hz_parties rel_party,
       hz_relationships rel,
       hz_org_contacts org_cont,
       hz_cust_accounts role_acct
 WHERE     acct_role.party_id = rel.party_id
       AND NOT EXISTS
              (SELECT '1'
                 FROM hz_role_responsibility rr
                WHERE rr.cust_account_role_id =
                         acct_role.cust_account_role_id)
       AND acct_role.role_type = 'CONTACT'
       AND org_cont.party_relationship_id = rel.relationship_id
       AND rel.subject_id = party.party_id
       AND rel_party.party_id = rel.party_id
       AND acct_role.cust_account_id = role_acct.cust_account_id
       AND role_acct.party_id = rel.object_id
       AND rel.subject_table_name = 'HZ_PARTIES'
       AND rel.object_table_name = 'HZ_PARTIES'
       AND (org_cont.job_title IS NULL AND org_cont.job_title_code IS NULL)
       AND party.status = 'A'
       AND rel_party.status = 'A'
       AND rel.status = 'A'
       AND acct_role.status = 'A';
       
      COMMIT;
      
      fnd_file.put_line (fnd_file.LOG, 'customer_contact_info_full extract Complete: ' || sysdate);
   EXCEPTION
      WHEN OTHERS
      THEN
         errbuf := 'Error';
         retcode := 2;
         fnd_file.put_line (fnd_file.LOG, SQLERRM);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_err_callfrom
           ,p_calling             => l_err_callpoint
           ,p_ora_error_msg       => SQLERRM
           ,p_error_desc          => l_message
           ,p_distribution_list   => l_distro_list
           ,p_module              => 'XXWC');
   END customer_contacts_info_full;


/********************************************************************************
ProcedureName : xxwc_om_fnd_user_full
Purpose       : API to generate Full Order user info 
HISTORY
===============================================================================
VERSION DATE          AUTHOR(S)       DESCRIPTION
------- -----------   --------------- -----------------------------------------
 1.0    27-Feb-2017 Rakesh Patel    Initial Version
********************************************************************************/
   PROCEDURE xxwc_om_fnd_user_full (errbuf OUT VARCHAR2, retcode OUT VARCHAR2)
   IS
      l_cnt             NUMBER := 0;
      l_role_cnt        NUMBER := 0;
      l_group_cnt       NUMBER := 0;
      l_ownerdn         VARCHAR2 (240);
      l_err_callfrom    VARCHAR2 (75) := 'XXWC_CUSTOMER_EXTRACT_HW_PKG.xxwc_om_fnd_user_full';
      l_err_callpoint   VARCHAR2 (75) := 'START';
      l_distro_list     VARCHAR2 (75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
      l_message         VARCHAR2 (2000);
   BEGIN
      errbuf := 'Success';
      retcode := 0;

      fnd_file.put_line (fnd_file.LOG,'Creating xxwc_om_fnd_user_full: ' || sysdate);

   INSERT INTO XXWC.XXWC_OM_FND_HW_USER
( 
  USER_ID                        ,
  USER_NAME                      ,
  LAST_UPDATE_DATE               ,
  LAST_UPDATED_BY                ,
  CREATION_DATE                  ,
  CREATED_BY                     ,
  LAST_UPDATE_LOGIN              ,
  ENCRYPTED_FOUNDATION_PASSWORD  ,
  ENCRYPTED_USER_PASSWORD        ,
  SESSION_NUMBER                 ,
  START_DATE                     ,
  END_DATE                       ,
  DESCRIPTION                    ,
  LAST_LOGON_DATE                ,
  PASSWORD_DATE                  ,
  PASSWORD_ACCESSES_LEFT         ,
  PASSWORD_LIFESPAN_ACCESSES     ,
  PASSWORD_LIFESPAN_DAYS         ,
  EMPLOYEE_ID                    ,
  EMAIL_ADDRESS                  ,
  FAX                            ,
  CUSTOMER_ID                    ,
  SUPPLIER_ID                    ,
  WEB_PASSWORD                   ,
  USER_GUID                      ,
  GCN_CODE_COMBINATION_ID        ,
  PERSON_PARTY_ID                ,
  USER_WAREHOUSE_ID	             
  )
   SELECT distinct fuser.USER_ID       ,
  fuser.USER_NAME                      ,
  fuser.LAST_UPDATE_DATE               ,
  fuser.LAST_UPDATED_BY                ,
  fuser.CREATION_DATE                  ,
  fuser.CREATED_BY                     ,
  fuser.LAST_UPDATE_LOGIN              ,
  fuser.ENCRYPTED_FOUNDATION_PASSWORD  ,
  fuser.ENCRYPTED_USER_PASSWORD        ,
  fuser.SESSION_NUMBER                 ,
  fuser.START_DATE                     ,
  fuser.END_DATE                       ,
  fuser.DESCRIPTION                    ,
  fuser.LAST_LOGON_DATE                ,
  fuser.PASSWORD_DATE                  ,
  fuser.PASSWORD_ACCESSES_LEFT         ,
  fuser.PASSWORD_LIFESPAN_ACCESSES     ,
  fuser.PASSWORD_LIFESPAN_DAYS         ,
  fuser.EMPLOYEE_ID                    ,
  fuser.EMAIL_ADDRESS                  ,
  fuser.FAX                            ,
  fuser.CUSTOMER_ID                    ,
  fuser.SUPPLIER_ID                    ,
  fuser.WEB_PASSWORD                   ,
  fuser.USER_GUID                      ,
  fuser.GCN_CODE_COMBINATION_ID        ,
  fuser.PERSON_PARTY_ID                ,
  NVL((SELECT fpov.PROFILE_OPTION_VALUE 
     FROM apps.fnd_profile_option_values fpov,
       apps.fnd_profile_options fpo
    WHERE PROFILE_OPTION_NAME = 'XXWC_OM_DEFAULT_SHIPPING_ORG'
      AND  fpov.level_value = fuser.user_id
      AND fpo.profile_option_id = fpov.profile_option_id), 223) USER_WAREHOUSE_ID
  FROM   FND_USER fuser,
         PER_PEOPLE_F per,
         FND_USER_RESP_GROUPS furg,
         FND_RESPONSIBILITY_TL frt
   WHERE     fuser.EMPLOYEE_ID = per.PERSON_ID
         AND fuser.USER_ID = furg.USER_ID
         AND (TO_CHAR (fuser.END_DATE) IS NULL OR fuser.END_DATE > SYSDATE)
         AND frt.RESPONSIBILITY_ID = furg.RESPONSIBILITY_ID
         AND (TO_CHAR (furg.END_DATE) IS NULL OR furg.END_DATE > SYSDATE)
         AND frt.LANGUAGE = 'US'
         AND frt.RESPONSIBILITY_NAME LIKE 'HDS%Order%'
    ORDER by   fuser.USER_ID     ;
  
      COMMIT;
      
      fnd_file.put_line (fnd_file.LOG, 'xxwc_om_fnd_user_full extract Complete: ' || sysdate);
   EXCEPTION
      WHEN OTHERS
      THEN
         errbuf := 'Error';
         retcode := 2;
         fnd_file.put_line (fnd_file.LOG, SQLERRM);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_err_callfrom
           ,p_calling             => l_err_callpoint
           ,p_ora_error_msg       => SQLERRM
           ,p_error_desc          => l_message
           ,p_distribution_list   => l_distro_list
           ,p_module              => 'XXWC');
   END xxwc_om_fnd_user_full;
      
/********************************************************************************
ProcedureName : ADD_INFO_HAND_WRITE
Purpose       : Wrapper API to generate Customer_Info
HISTORY
===============================================================================
VERSION DATE          AUTHOR(S)       DESCRIPTION
------- -----------   --------------- -----------------------------------------
 1.00   27-Feb-2017   Rakesh Patel    Initial Version
********************************************************************************/
   PROCEDURE ADD_INFO_HAND_WRITE (
      errbuf                   OUT VARCHAR2
     ,retcode                  OUT VARCHAR2
     ,p_extract_method      IN     VARCHAR2 DEFAULT 'DELTA'
     ,p_last_extract_date   IN     VARCHAR2)
   IS
      l_last_extract_date   DATE
         := TRUNC (
               NVL (fnd_date.canonical_to_date (p_last_extract_date)
                   ,SYSDATE - 3));

      l_errbuf              VARCHAR2 (80);
      l_retcode             VARCHAR2 (80);

      l_err_callfrom        VARCHAR2 (75)
                               := 'XXWC_CUSTOMER_EXTRACT_PKG.ADD_INFO_HAND_WRITE';
      l_err_callpoint       VARCHAR2 (75) := 'START';
      l_distro_list         VARCHAR2 (75)
                               DEFAULT 'HDSOracleDevelopers@hdsupply.com';
      l_message             VARCHAR2 (2000);
   BEGIN
      errbuf := 'Success';
      retcode := 0;

      fnd_file.put_line (fnd_file.LOG,'P_EXTRACT_METHOD = ' || p_extract_method);
      fnd_file.put_line (fnd_file.LOG,'P_LAST_EXTRACT_DATE = ' || p_last_extract_date);

      -- split logic into seperate procedures for performance and to simplify the queries  
      IF p_extract_method = 'FULL'
      THEN
         EXECUTE IMMEDIATE 'TRUNCATE TABLE XXWC.XXWC_OM_FND_HW_USER';
         EXECUTE IMMEDIATE 'TRUNCATE TABLE XXWC.XXWC_BRANCH_INFO_HW_TBL'; 
         EXECUTE IMMEDIATE 'TRUNCATE TABLE XXWC.XXWC_AUTH_BUYER_INFO_HW_TBL'; 

         branch_info_full (l_errbuf, l_retcode);
         xxwc_om_fnd_user_full (l_errbuf, l_retcode);
         Auth_buyer_Info_full(l_errbuf, l_retcode);
      ELSE
         NULL;
      END IF;

      errbuf := l_errbuf;
      retcode := l_retcode;
   EXCEPTION
      WHEN OTHERS
      THEN
         errbuf := 'Error';
         retcode := 2;
         fnd_file.put_line (fnd_file.LOG, SQLERRM);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_err_callfrom
           ,p_calling             => l_err_callpoint
           ,p_ora_error_msg       => SQLERRM
           ,p_error_desc          => l_message
           ,p_distribution_list   => l_distro_list
           ,p_module              => 'XXWC');
   END ADD_INFO_HAND_WRITE;
   
   
/********************************************************************************
ProcedureName : Customer_Info
Purpose       : Wrapper API to generate Customer_Info
HISTORY
===============================================================================
VERSION DATE          AUTHOR(S)       DESCRIPTION
------- -----------   --------------- -----------------------------------------
 1.00   16-Feb-2017   Rakesh Patel    Initial Version
********************************************************************************/
   PROCEDURE Customer_Info (
      errbuf                   OUT VARCHAR2
     ,retcode                  OUT VARCHAR2
     ,p_extract_method      IN     VARCHAR2 DEFAULT 'DELTA'
     ,p_last_extract_date   IN     VARCHAR2)
   IS
      l_last_extract_date   DATE
         := TRUNC (
               NVL (fnd_date.canonical_to_date (p_last_extract_date)
                   ,SYSDATE - 3));

      l_errbuf              VARCHAR2 (80);
      l_retcode             VARCHAR2 (80);

      l_err_callfrom        VARCHAR2 (75)
                               := 'XXWC_CUSTOMER_EXTRACT_PKG.Customer_Info';
      l_err_callpoint       VARCHAR2 (75) := 'START';
      l_distro_list         VARCHAR2 (75)
                               DEFAULT 'HDSOracleDevelopers@hdsupply.com';
      l_message             VARCHAR2 (2000);
   BEGIN
      errbuf := 'Success';
      retcode := 0;

      fnd_file.put_line (fnd_file.LOG,'P_EXTRACT_METHOD = ' || p_extract_method);
      fnd_file.put_line (fnd_file.LOG,'P_LAST_EXTRACT_DATE = ' || p_last_extract_date);

      -- split logic into seperate procedures for performance and to simplify the queries  
      IF p_extract_method = 'FULL'
      THEN
         EXECUTE IMMEDIATE 'TRUNCATE TABLE XXWC.XXWC_BRANCH_INFO_HW_TBL'; 
         EXECUTE IMMEDIATE 'TRUNCATE TABLE XXWC.XXWC_CUSTOMER_INFO_HW_TBL';
         EXECUTE IMMEDIATE 'TRUNCATE TABLE XXWC.XXWC_CUSTOMER_JOB_INFO_HW_TBL';
         
         branch_info_full (l_errbuf, l_retcode);
         customer_info_full (l_errbuf, l_retcode);
         customer_job_info_full (l_errbuf, l_retcode);
         
      ELSE
         NULL;
         --ecomm_extract_delta (l_errbuf, l_retcode, l_last_extract_date);
      END IF;

      errbuf := l_errbuf;
      retcode := l_retcode;
   EXCEPTION
      WHEN OTHERS
      THEN
         errbuf := 'Error';
         retcode := 2;
         fnd_file.put_line (fnd_file.LOG, SQLERRM);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_err_callfrom
           ,p_calling             => l_err_callpoint
           ,p_ora_error_msg       => SQLERRM
           ,p_error_desc          => l_message
           ,p_distribution_list   => l_distro_list
           ,p_module              => 'XXWC');
   END Customer_Info;
   
/********************************************************************************
ProcedureName : Products
Purpose       : Wrapper API to generate Products_info
HISTORY
===============================================================================
VERSION DATE          AUTHOR(S)       DESCRIPTION
------- -----------   --------------- -----------------------------------------
 1      20-Feb-2017   Rakesh Patel    Initial Version
********************************************************************************/
   PROCEDURE Products_Info (
      errbuf                   OUT VARCHAR2
     ,retcode                  OUT VARCHAR2
     ,p_extract_method      IN     VARCHAR2 DEFAULT 'DELTA'
     ,p_last_extract_date   IN     VARCHAR2)
   IS
      l_last_extract_date   DATE := TRUNC ( NVL (fnd_date.canonical_to_date (p_last_extract_date), SYSDATE - 3));
      l_errbuf              VARCHAR2 (80);
      l_retcode             VARCHAR2 (80);
      l_err_callfrom        VARCHAR2 (75) := 'XXWC_CUSTOMER_EXTRACT_PKG.Products_Info';
      l_err_callpoint       VARCHAR2 (75) := 'START';
      l_distro_list         VARCHAR2 (75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
      l_message             VARCHAR2 (2000);
   BEGIN
      errbuf := 'Success';
      retcode := 0;

      fnd_file.put_line (fnd_file.LOG,'P_EXTRACT_METHOD = ' || p_extract_method);
      fnd_file.put_line (fnd_file.LOG,'P_LAST_EXTRACT_DATE = ' || p_last_extract_date);

      IF p_extract_method = 'FULL'
      THEN
         EXECUTE IMMEDIATE 'TRUNCATE TABLE XXWC.XXWC_PRODUCTS_HW_TBL';
         Products_Info_full (l_errbuf, l_retcode);
      ELSE
         NULL;
      END IF;

      errbuf := l_errbuf;
      retcode := l_retcode;
   EXCEPTION
      WHEN OTHERS
      THEN
         errbuf := 'Error';
         retcode := 2;
         fnd_file.put_line (fnd_file.LOG, SQLERRM);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_err_callfrom
           ,p_calling             => l_err_callpoint
           ,p_ora_error_msg       => SQLERRM
           ,p_error_desc          => l_message
           ,p_distribution_list   => l_distro_list
           ,p_module              => 'XXWC');
   END Products_Info;
 
/********************************************************************************
ProcedureName : Contacts_Info
Purpose       : Wrapper API to generate Contacts_Info
HISTORY
===============================================================================
VERSION DATE          AUTHOR(S)       DESCRIPTION
------- -----------   --------------- -----------------------------------------
 1      20-Feb-2017   Rakesh Patel    Initial Version
********************************************************************************/
   PROCEDURE Contacts_Info (
      errbuf                   OUT VARCHAR2
     ,retcode                  OUT VARCHAR2
     ,p_extract_method      IN     VARCHAR2 DEFAULT 'DELTA'
     ,p_last_extract_date   IN     VARCHAR2)
   IS
      l_last_extract_date   DATE := TRUNC ( NVL (fnd_date.canonical_to_date (p_last_extract_date), SYSDATE - 3));
      l_errbuf              VARCHAR2 (80);
      l_retcode             VARCHAR2 (80);
      l_err_callfrom        VARCHAR2 (75) := 'XXWC_CUSTOMER_EXTRACT_PKG.Contacts_Info';
      l_err_callpoint       VARCHAR2 (75) := 'START';
      l_distro_list         VARCHAR2 (75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
      l_message             VARCHAR2 (2000);
   BEGIN
      errbuf := 'Success';
      retcode := 0;

      fnd_file.put_line (fnd_file.LOG,'P_EXTRACT_METHOD = ' || p_extract_method);
      fnd_file.put_line (fnd_file.LOG,'P_LAST_EXTRACT_DATE = ' || p_last_extract_date);

      IF p_extract_method = 'FULL'
      THEN
         EXECUTE IMMEDIATE 'TRUNCATE TABLE XXWC.XXWC_CUSTOMER_CONT_INFO_HW_TBL';
         customer_contacts_info_full (l_errbuf, l_retcode);
      ELSE
         NULL;
      END IF;

      errbuf := l_errbuf;
      retcode := l_retcode;
   EXCEPTION
      WHEN OTHERS
      THEN
         errbuf := 'Error';
         retcode := 2;
         fnd_file.put_line (fnd_file.LOG, SQLERRM);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_err_callfrom
           ,p_calling             => l_err_callpoint
           ,p_ora_error_msg       => SQLERRM
           ,p_error_desc          => l_message
           ,p_distribution_list   => l_distro_list
           ,p_module              => 'XXWC');
   END Contacts_Info;
     
    PROCEDURE wait_for_running_request ( p_request_id IN NUMBER )
   IS
      CURSOR cu_running_requests ( p_request_id IN NUMBER)
      IS
         SELECT /*+ Index(fnd_concurrent_requests_N7) */ request_id
           FROM apps.fnd_concurrent_requests fcr
          WHERE fcr.concurrent_program_id IN ( SELECT CONCURRENT_PROGRAM_ID 
                                                 FROM FND_CONCURRENT_PROGRAMS_VL 
                                                WHERE CONCURRENT_PROGRAM_NAME ='XXWC_PRODUCT_PRICE_CHILD_HW')
            AND (status_code = 'R' OR status_code = 'I')
            AND (phase_code = 'R' OR phase_code ='P')
            AND request_id = p_request_id;

      l_request_id    NUMBER;
      lc_phase        VARCHAR2 (100);
      lc_status       VARCHAR2 (100);
      lc_dev_phase    VARCHAR2 (100);
      lc_dev_status   VARCHAR2 (100);
      lc_message      VARCHAR2 (100);
      lc_waited BOOLEAN;
   BEGIN
      LOOP
         OPEN cu_running_requests( p_request_id );

         FETCH cu_running_requests
          INTO l_request_id;
         
         IF cu_running_requests%NOTFOUND
         THEN
            CLOSE cu_running_requests;

            EXIT;
         END IF;

         CLOSE cu_running_requests;

         IF l_request_id > 0
         THEN
            lc_waited:=fnd_concurrent.wait_for_request (
                                             request_id      => l_request_id,
                                             INTERVAL        => 2,
                                             max_wait        => 180,
                                             phase           => lc_phase,
                                             status          => lc_status,
                                             dev_phase       => lc_dev_phase,
                                             dev_status      => lc_dev_status,
                                             MESSAGE         => lc_message
                                            );
         END IF;
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         fnd_file.put_line (fnd_file.LOG,'Error in wait_for_running_request order header id ' || p_request_id);
         fnd_file.put_line (fnd_file.LOG,'Error: ' || SQLERRM);
         RAISE;
   END wait_for_running_request;
   
  /********************************************************************************
ProcedureName : Products_Price_Info
Purpose       : Wrapper API to generate Products_Price_Info
HISTORY
===============================================================================
VERSION DATE          AUTHOR(S)       DESCRIPTION
------- -----------   --------------- -----------------------------------------
 1      21-Feb-2017   Rakesh Patel    Initial Version
********************************************************************************/
   PROCEDURE Products_Price_Info (
      errbuf                   OUT VARCHAR2
     ,retcode                  OUT VARCHAR2
     ,p_extract_method      IN     VARCHAR2 DEFAULT 'DELTA'
     ,p_last_extract_date   IN     VARCHAR2)
   IS
      l_last_extract_date   DATE := TRUNC ( NVL (fnd_date.canonical_to_date (p_last_extract_date), SYSDATE - 3));
      l_errbuf              VARCHAR2 (80);
      l_retcode             VARCHAR2 (80);
      l_err_callfrom        VARCHAR2 (75) := 'XXWC_CUSTOMER_EXTRACT_PKG.Products_Price_Info';
      l_err_callpoint       VARCHAR2 (75) := 'START';
      l_distro_list         VARCHAR2 (75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
      l_message             VARCHAR2 (2000);
      l_req_id              NUMBER;
      l_request_id_count    NUMBER;
      l_return_status       VARCHAR2 (4000);
      
      TYPE request_ids_tbl IS TABLE OF NUMBER INDEX BY PLS_INTEGER;
      l_request_ids_tbl   request_ids_tbl;
   
      CURSOR c_child_organization
      IS
       SELECT organization_id 
         FROM mtl_parameters
        WHERE MASTER_ORGANIZATION_ID = 222
        AND organization_id <> 222;
   BEGIN
      errbuf := 'Success';
      retcode := 0;
      l_request_id_count := 0;
      
      fnd_file.put_line (fnd_file.LOG,'P_EXTRACT_METHOD = ' || p_extract_method);
      fnd_file.put_line (fnd_file.LOG,'P_LAST_EXTRACT_DATE = ' || p_last_extract_date);

      IF p_extract_method = 'FULL'
      THEN
        EXECUTE IMMEDIATE 'TRUNCATE TABLE XXWC.XXWC_PRODUCTS_PRICE_HW_TBL';
        
        FOR r_child_org IN c_child_organization
        LOOP 
             BEGIN
              --------------------------------------------------------------------------
	          -- Submit "XXWC Create Outbound File Common Program"
	          --------------------------------------------------------------------------
	          l_req_id := fnd_request.submit_request(application => 'XXWC'
	                                       , program     => 'XXWC_PRODUCT_PRICE_CHILD_HW'
	                                       , description => NULL
	                                       , start_time  => SYSDATE
	                                       , sub_request => FALSE
	                                       , argument1   => r_child_org.Organization_id
	                                       );
	
	         COMMIT;

             IF NVL (l_req_id, 0) > 0
             THEN
    		    l_request_id_count := l_request_id_count+1;
			    l_request_ids_tbl (l_request_id_count) := l_req_id;
				
			    fnd_file.put_line (fnd_file.LOG,l_request_ids_tbl (l_request_id_count));
			    
  			    l_return_status := 'Concurrent request '  || l_req_id || ' has been submitted to organization id ' || r_child_org.Organization_id;
				fnd_file.put_line (fnd_file.LOG,l_return_status); 
             ELSE
               l_return_status := 'Could not submit request to pick order ' ||r_child_org.Organization_id;
			   fnd_file.put_line (fnd_file.LOG,l_return_status); 
             END IF;
			 
             fnd_file.put_line (fnd_file.LOG, 'l_request_ids_tbl.COUNT : ' || l_request_ids_tbl.COUNT);
			 IF l_request_ids_tbl.COUNT >= 50 THEN
			   
			    FOR i IN l_request_ids_tbl.FIRST .. l_request_ids_tbl.LAST
			    LOOP
			       wait_for_running_request ( p_request_id => l_request_ids_tbl(i) );
				END LOOP;  
				l_request_ids_tbl.delete;
				l_request_id_count := 0;
			 END IF;
               
            EXCEPTION
               WHEN OTHERS
               THEN
                  fnd_file.put_line (fnd_file.LOG,'NO record found  in with xxwc_print_log_tbl table rganization id ' || r_child_org.Organization_id);
            END;
         END LOOP;  
         fnd_file.put_line (fnd_file.LOG, 'l_request_ids_tbl.COUNT after loop : ' || l_request_ids_tbl.COUNT);
         IF l_request_ids_tbl.COUNT > 0 THEN
			   
	        FOR i IN l_request_ids_tbl.FIRST .. l_request_ids_tbl.LAST
		    LOOP
		       wait_for_running_request ( p_request_id => l_request_ids_tbl(i) );
			END LOOP;  
		    l_request_ids_tbl.delete;
			l_request_id_count := 0;
		 END IF;
      ELSE
         NULL;  
      END IF;
      
      fnd_file.put_line (fnd_file.LOG, 'Products_Price_Info extract Complete: ' || sysdate);
      errbuf := l_errbuf;
      retcode := l_retcode;
   EXCEPTION
      WHEN OTHERS
      THEN
         errbuf := 'Error';
         retcode := 2;
         fnd_file.put_line (fnd_file.LOG, SQLERRM);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_err_callfrom
           ,p_calling             => l_err_callpoint
           ,p_ora_error_msg       => SQLERRM
           ,p_error_desc          => l_message
           ,p_distribution_list   => l_distro_list
           ,p_module              => 'XXWC');
   END Products_Price_Info;

end xxwc_customer_extract_hw_pkg;
/