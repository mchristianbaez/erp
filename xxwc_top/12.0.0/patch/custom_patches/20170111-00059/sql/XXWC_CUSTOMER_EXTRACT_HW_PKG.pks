CREATE OR REPLACE PACKAGE APPS.xxwc_customer_extract_hw_pkg
AS
/**************************************************************************
 *
 * HEADER  
 *    Customer Info / Customer Job Info / Product + Description /Product + warehouse + market price 
 *
 * PROGRAM NAME
 *  xxwc_customer_extract_hw_pkg.pks
 * 
 * DESCRIPTION
 *  this package contains all of the procedures/functions used to generate customer, Customer Job Info, Product + Description 
 *    and Product + warehouse + market price for the Handwrite /WC Online Sales Form
 *
 * HISTORY
 * =======
 *
 * VERSION DATE        AUTHOR(S)       DESCRIPTION
 * ------- ----------- --------------- ------------------------------------
 * 1.00    16-Feb-2017 Rakesh Patel    Initial Version
 *
 *************************************************************************/

   /**************************************************************************
    *
    * PROCEDURE
    *  Customer_Info
    *
    * DESCRIPTION
    *  This procedure is called from a concurrent request to generate the customer
    *    extract date for the WC Online Sales Form
    *
    * PARAMETERS
    * ==========
    * NAME                 TYPE      DESCRIPTION
   .* -----------------    --------  ---------------------------------------------
    * ERRBUF               VARCAHR2  completion comment
    * RETCODE              VARCHAR2  completion code (0=success  1=warning  2=error)
    * P_EXTRACT_METHOD     VARCHAR2  type of extract : FULL or DELTA
    * p_LAST_EXTRACT_DATE  VARCHAR2  canonical date format of last extract
    *
    *
    * CALLED BY
    *  XXWC Price Lists Extract to ECOMM concurrent program
    *
    *************************************************************************/
   PROCEDURE Customer_Info (
      errbuf                   OUT VARCHAR2
     ,retcode                  OUT VARCHAR2
     ,p_extract_method      IN     VARCHAR2 DEFAULT 'DELTA'
     ,p_last_extract_date   IN     VARCHAR2);


   /**************************************************************************
    *
    * PROCEDURE
    *  Extract_Wrapper
    *
    * DESCRIPTION
    *  This procedure is called from UC4 to submit the concurrent request to generate the organization/role
    *    extract files for the Commerce interface
    *
    * PARAMETERS
    * ==========
    * NAME                 TYPE      DESCRIPTION
   .* -----------------    --------  ---------------------------------------------
    * ERRBUF               VARCAHR2  completion comment
    * RETCODE              VARCHAR2  completion code (0=success  1=warning  2=error)
    * P_EXTRACT_METHOD     VARCHAR2  type of extract : FULL or DELTA
    * p_LAST_EXTRACT_DATE  VARCHAR2  canonical date format of last extract
    *
    *
    * CALLED BY
    *  UC4 scheduling program
    *
    *************************************************************************/
 
  PROCEDURE Products_Info (
      errbuf                   OUT VARCHAR2
     ,retcode                  OUT VARCHAR2
     ,p_extract_method      IN     VARCHAR2 DEFAULT 'DELTA'
     ,p_last_extract_date   IN     VARCHAR2);
     
  PROCEDURE Contacts_Info (
      errbuf                   OUT VARCHAR2
     ,retcode                  OUT VARCHAR2
     ,p_extract_method      IN     VARCHAR2 DEFAULT 'DELTA'
     ,p_last_extract_date   IN     VARCHAR2);

  PROCEDURE Products_Price_Info (
      errbuf                   OUT VARCHAR2
     ,retcode                  OUT VARCHAR2
     ,p_extract_method      IN     VARCHAR2 DEFAULT 'DELTA'
     ,p_last_extract_date   IN     VARCHAR2);
     
  PROCEDURE ADD_INFO_HAND_WRITE (
      errbuf                   OUT VARCHAR2
     ,retcode                  OUT VARCHAR2
     ,p_extract_method      IN     VARCHAR2 DEFAULT 'DELTA'
     ,p_last_extract_date   IN     VARCHAR2);
     
 PROCEDURE Products_Price_Info_full (  errbuf OUT VARCHAR2
                                      ,retcode OUT VARCHAR2
                                      ,p_organization_id IN NUMBER);

END xxwc_customer_extract_hw_pkg;
/