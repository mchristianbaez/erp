/************************************
--This Script is created for TMS Task ID: 20170523-00323
--Author Neha Saini
--Date 5/24/2017
--comments Table Scripts for TMS Task ID: 20170523-00323
****************************************/
drop table BACKUP_TAXAUDIT_HEADER;
drop table BACKUP_TAXAUDIT_detail;
drop table BACKUP_TAXAUDIT_tax;
drop table BACKUP_TAXAUDIT_juris;


PROMPT Creating TABLE BACKUP_TAXAUDIT_HEADER
CREATE TABLE BACKUP_TAXAUDIT_HEADER
(
headerno        NUMBER (15),
oracleid                NUMBER(15)       NOT NULL,
invnum            VARCHAR2 (20)    NULL,
divcode            VARCHAR2 (20)    NULL,
compid            VARCHAR2 (20)    NULL,
custnum            VARCHAR2 (20)    NULL,
custname        VARCHAR2 (20)    NULL,
billtocustid        VARCHAR2 (20)    NULL,
billtocustname        VARCHAR2 (20)    NULL,
invdate            DATE        NOT NULL,
transdate        DATE        NOT NULL,
fiscaldate        DATE        NULL,
batch_id VARCHAR2 (12)    NULL
);


PROMPT Creating TABLE BACKUP_TAXAUDIT_DETAIL
CREATE TABLE BACKUP_TAXAUDIT_DETAIL
(
headerno        NUMBER (15),
detailno        NUMBER (15),
jurisno            NUMBER (15)    NULL,
invlinenum        NUMBER (5)    NULL,
deliverydate        DATE        NULL,
grossamt        NUMBER(14,2)    NULL,
frghtamt        NUMBER(14,2)    NULL,
discountamt             NUMBER(14,2)       NULL,
calctypecode        VARCHAR2 (1)    NULL,
custprodcode        VARCHAR2 (25)    NULL,
newtwiprodcode        VARCHAR2 (9)    NULL,
oldtwiprodcode        VARCHAR2 (5)    NULL,
prodcodeconv        VARCHAR2 (1)    NULL,
prodcodeflag        VARCHAR2 (1)    NULL,
numitem            NUMBER (7)    NULL,
misc            VARCHAR2 (50)    NULL,
cmplcode        VARCHAR2 (2)    NULL,
prodtaxind        VARCHAR2 (1)    NULL,
credind            VARCHAR2 (1)    NULL,
shipfromst        VARCHAR2 (2)    NULL,
tax035sw        VARCHAR2 (1)    NULL,
stepjobno        VARCHAR2 (10)    NULL,
stepcriterion        VARCHAR2 (1)    NULL,
locncode        VARCHAR2 (13)    NULL,
juristype        VARCHAR2(1)        NULL,
basepercent        NUMBER (6,5)    NULL,
volume            VARCHAR2 (15)    NULL,
volexp            VARCHAR2 (3)    NULL,
UOM            VARCHAR2 (15)    NULL,
roundind        VARCHAR2 (1)    NULL,
dropshipind        VARCHAR2 (1)    NULL,
endinvind        VARCHAR2 (1)    NULL,
freightind        VARCHAR2 (1)    NULL,
serviceind        VARCHAR2 (1)    NULL,
inoutind        VARCHAR2 (1)    NULL,
shortcitynameind    VARCHAR2 (1)    NULL,
currcode1        VARCHAR2 (3)    NULL,
currcode2        VARCHAR2 (3)    NULL,
currconvfact        VARCHAR2 (15)    NULL,
contractamt        NUMBER (14,2)    NULL,
installamt        NUMBER (14,2)    NULL,
partnum            VARCHAR2 (20)    NULL,
costcenter        VARCHAR2 (10)    NULL,
AFEworkorder        VARCHAR2 (26)    NULL,
POT            VARCHAR2 (1)    NULL,
movementcode        VARCHAR2 (1)    NULL,
storagecode        VARCHAR2 (1)    NULL,
jurreturncode        VARCHAR2 (2)    NULL,
extracmplcd1        VARCHAR2 (2)    NULL,
extracmplcd2        VARCHAR2 (2)    NULL,
extracmplcd3        VARCHAR2 (2)    NULL,
extracmplcd4        VARCHAR2 (2)    NULL
);



PROMPT Creating TABLE BACKUP_TAXAUDIT_JURIS
CREATE TABLE BACKUP_TAXAUDIT_JURIS
(
headerno        NUMBER (15),
jurisno            NUMBER (15),
jurlevel        VARCHAR2 (1),
prisec            VARCHAR2 (1),
countrycode             VARCHAR2 (3)    NULL,
stcode            VARCHAR2 (2)    NULL,
cntycode        VARCHAR2 (3)    NULL,
zipcode            VARCHAR2 (5)    NULL,
geocode            VARCHAR2 (2)    NULL,
zipextn            VARCHAR2 (4)    NULL,
loclname        VARCHAR2 (26)    NULL,
cntyname        VARCHAR2 (26)    NULL,
busloclind        VARCHAR2 (1)    NULL,
inoutind        VARCHAR2(1)    NULL 
);





PROMPT Creating TABLE BACKUP_TAXAUDIT_TAX
CREATE TABLE BACKUP_TAXAUDIT_TAX
(
headerno        NUMBER (15),
detailno        NUMBER (15),
taxlevel        VARCHAR2 (2),
taxtype            VARCHAR2 (1)    NULL,
taxamt            NUMBER(16,4)    NULL,
taxrate            NUMBER(5,5)    NULL,
cmplcode        VARCHAR2 (2)    NULL,
exmptamt        NUMBER(14,2)    NULL,
admncode        VARCHAR2 (1)    NULL,
taxcert            VARCHAR2 (25)    NULL,
reascode        VARCHAR2 (2)    NULL,
baseamt            NUMBER (14,2)    NULL,
taxcode            VARCHAR2 (10)    NULL,
excpcode        VARCHAR2 (1)    NULL,
stepstatus        VARCHAR2 (1)    NULL,
stepcomment        VARCHAR2 (1)    NULL,
EXCPRULE                                           VARCHAR2(2),
FLAG1                                              VARCHAR2(1),
RATE1                                              NUMBER(7,7),
AMOUNT1                                            NUMBER(13,2),
CODE1                                              VARCHAR2(7),
DESC1                                              VARCHAR2(26),
FLAG2                                              VARCHAR2(1),
RATE2                                              NUMBER(7,7),
AMOUNT2                                            NUMBER(13,2),
CODE2                                              VARCHAR2(7),
DESC2                                              VARCHAR2(26),
FLAG3                                              VARCHAR2(1),
RATE3                                              NUMBER(7,7),
AMOUNT3                                            NUMBER(13,2),
CODE3                                              VARCHAR2(7),
DESC3                                              VARCHAR2(26),
FLAG4                                              VARCHAR2(1),
RATE4                                              NUMBER(7,7),
AMOUNT4                                            NUMBER(13,2),
CODE4                                              VARCHAR2(7),
DESC4                                              VARCHAR2(26),
FLAG5                                              VARCHAR2(1),
RATE5                                              NUMBER(7,7),
AMOUNT5                                            NUMBER(13,2),
CODE5                                              VARCHAR2(7),
DESC5                                              VARCHAR2(26),
FLAG6                                              VARCHAR2(1),
RATE6                                              NUMBER(7,7),
AMOUNT6                                            NUMBER(13,2),
CODE6                                              VARCHAR2(7),
DESC6                                              VARCHAR2(26)
);




INSERT into BACKUP_TAXAUDIT_header select * from taxware.taxaudit_header where headerno in (select headerno from taxware.taxaudit_tax where TaxLevel='ST' and baseamt>0 and taxamt/taxrate/baseamt >1.02 and headerno>'12950000');
INSERT into BACKUP_TAXAUDIT_detail select * from taxware.taxaudit_detail where headerno in (select headerno from taxware.taxaudit_tax where TaxLevel='ST' and baseamt>0 and taxamt/taxrate/baseamt >1.02 and headerno>'12950000');
INSERT into BACKUP_TAXAUDIT_tax select * from taxware.taxaudit_tax where headerno in (select headerno from taxware.taxaudit_tax where TaxLevel='ST' and baseamt>0 and taxamt/taxrate/baseamt >1.02 and headerno>'12950000');
INSERT into BACKUP_TAXAUDIT_juris select * from taxware.taxaudit_juris where headerno in (select headerno from taxware.taxaudit_tax where TaxLevel='ST' and baseamt>0 and taxamt/taxrate/baseamt >1.02 and headerno>'12950000');
