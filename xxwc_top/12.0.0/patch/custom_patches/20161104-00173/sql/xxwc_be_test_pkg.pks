CREATE OR REPLACE PACKAGE APPS.xxwc_be_test_pkg
AS
 /********************************************************************************************************************************
      $Header xxwc_be_test_pkg.pks $
      Module Name: xxwc_be_test_pkg

      PURPOSE:   This package is used for testing of business event triggered by oracle.apps.ar.hz.CustAccount.create

      REVISIONS:
      Ver        Date         Author                 Description
      ---------  ----------   ---------------        -------------------------------------------------------------------------------
      1.0        11-Oct-2016  Rakesh Patel           TMS#20161104-00173-B2B Business Events Subscription Validation
   *********************************************************************************************************************************/

   FUNCTION xx_insert (p_subscription_guid IN RAW, p_event IN OUT wf_event_t)
      RETURN VARCHAR2;
END xxwc_be_test_pkg;
/