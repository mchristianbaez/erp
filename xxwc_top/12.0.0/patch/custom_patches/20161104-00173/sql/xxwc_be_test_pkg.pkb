CREATE OR REPLACE PACKAGE BODY xxwc_be_test_pkg
AS
  /********************************************************************************************************************************
      $Header xxwc_be_test_pkg.pks $
      Module Name: xxwc_be_test_pkg

      PURPOSE:   This package is used for testing of business event triggered by oracle.apps.ar.hz.CustAccount.create

      REVISIONS:
      Ver        Date         Author                 Description
      ---------  ----------   ---------------        -------------------------------------------------------------------------------
      1.0        11-Oct-2016  Rakesh Patel           TMS#20161104-00173-B2B Business Events Subscription Validation
   *********************************************************************************************************************************/

   FUNCTION xx_insert (p_subscription_guid IN RAW, p_event IN OUT wf_event_t)
      RETURN VARCHAR2
   IS
      l_param_list    wf_parameter_list_t;
      l_param_name    VARCHAR2 (240);
      l_param_value   VARCHAR2 (2000);
      l_event_name    VARCHAR2 (2000);
      l_event_key     VARCHAR2 (2000);
	  l_event_data    VARCHAR2 (4000);
   BEGIN
      l_param_list := p_event.getparameterlist;
      l_event_name := p_event.geteventname ();
      l_event_key  := p_event.geteventkey ();
      l_event_data := p_event.geteventdata ();
 
      insert into XXWC.XXWC_AR_BE_TEMP_TBL values
      (null,null,null,null,null,null,'EVENT NAME: ' || l_event_name,fnd_global.user_id,sysdate,sysdate);

      insert into XXWC.XXWC_AR_BE_TEMP_TBL values
      (null,null,null,null,null,null,'EVENT KEY: ' || l_event_key,fnd_global.user_id,sysdate,sysdate);

      insert into XXWC.XXWC_AR_BE_TEMP_TBL values
      (null,null,null,null,null,null,'EVENT DATA: ' || l_event_data,fnd_global.user_id,sysdate,sysdate);

       IF l_param_list IS NOT NULL
       THEN
          FOR i IN l_param_list.FIRST .. l_param_list.LAST
          LOOP
             l_param_name := l_param_list (i).getname;
             l_param_value := l_param_list (i).getvalue;
  
            insert into XXWC.XXWC_AR_BE_TEMP_TBL values
            (null,null,null,null,null,null,l_param_name || ': ' || l_param_value,fnd_global.user_id,sysdate,sysdate);
      
            COMMIT;
         END LOOP;
      END IF;
 
      COMMIT;
      RETURN 'SUCCESS';
   EXCEPTION
      WHEN OTHERS
      THEN
         --Provide context information that helps locate the source of an error.
         wf_core.CONTEXT (pkg_name       => 'XX_BE_TEST_PKG',
                          proc_name      => 'XX_INSERT',
                          arg1           => p_event.geteventname (),
                          arg2           => p_event.geteventkey (),
                          arg3           => p_subscription_guid
                         );
         
         --Retrieves error information from the error stack and sets it into the event message.
         wf_event.seterrorinfo (p_event => p_event, p_type => 'ERROR');
         RETURN 'ERROR';
   END xx_insert;
END xxwc_be_test_pkg;
/