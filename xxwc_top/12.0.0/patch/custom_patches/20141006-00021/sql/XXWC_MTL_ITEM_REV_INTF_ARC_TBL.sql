/**********************************************************************************************
-- Table XXWC_MTL_ITEM_REV_INTF_ARC_TBL
-- ********************************************************************************************
-- HISTORY
-- ============================================================================================
-- ============================================================================================
-- VERSION DATE          AUTHOR(S)       DESCRIPTION
-- ------- -----------   --------------- ------------------------------------------------------
-- 1.0     05-Feb-2015   P.Vamshidhar    Created this table to archive data before deleting
--                                       from standard interface table.
--                                       TMS: 20141120-00077
-- 2.0     12-Feb-2015   P.vamshidhar    Added insert_date column 
-- 3.0     17-Feb-2015   P.vamshidhar    script with column names.
-- 4.0     16-Oct-2015   P.Vamshidhar    TMS#20141006-00021 
--                                       Re-created table due to patch

*/
/*  Commented by below code in TMS#20141006-00021 
CREATE TABLE XXWC.XXWC_MTL_ITEM_REV_INTF_ARC_TBL
(
  INVENTORY_ITEM_ID          NUMBER,
  ORGANIZATION_ID            NUMBER,
  REVISION                   VARCHAR2(3 BYTE),
  LAST_UPDATE_DATE           DATE,
  LAST_UPDATED_BY            NUMBER,
  CREATION_DATE              DATE,
  CREATED_BY                 NUMBER,
  LAST_UPDATE_LOGIN          NUMBER,
  CHANGE_NOTICE              VARCHAR2(10 BYTE),
  ECN_INITIATION_DATE        DATE,
  IMPLEMENTATION_DATE        DATE,
  IMPLEMENTED_SERIAL_NUMBER  VARCHAR2(30 BYTE),
  EFFECTIVITY_DATE           DATE,
  ATTRIBUTE_CATEGORY         VARCHAR2(30 BYTE),
  ATTRIBUTE1                 VARCHAR2(150 BYTE),
  ATTRIBUTE2                 VARCHAR2(150 BYTE),
  ATTRIBUTE3                 VARCHAR2(150 BYTE),
  ATTRIBUTE4                 VARCHAR2(150 BYTE),
  ATTRIBUTE5                 VARCHAR2(150 BYTE),
  ATTRIBUTE6                 VARCHAR2(150 BYTE),
  ATTRIBUTE7                 VARCHAR2(150 BYTE),
  ATTRIBUTE8                 VARCHAR2(150 BYTE),
  ATTRIBUTE9                 VARCHAR2(150 BYTE),
  ATTRIBUTE10                VARCHAR2(150 BYTE),
  ATTRIBUTE11                VARCHAR2(150 BYTE),
  ATTRIBUTE12                VARCHAR2(150 BYTE),
  ATTRIBUTE13                VARCHAR2(150 BYTE),
  ATTRIBUTE14                VARCHAR2(150 BYTE),
  ATTRIBUTE15                VARCHAR2(150 BYTE),
  REQUEST_ID                 NUMBER,
  PROGRAM_APPLICATION_ID     NUMBER,
  PROGRAM_ID                 NUMBER,
  PROGRAM_UPDATE_DATE        DATE,
  REVISED_ITEM_SEQUENCE_ID   NUMBER,
  DESCRIPTION                VARCHAR2(240 BYTE),
  ITEM_NUMBER                VARCHAR2(700 BYTE),
  ORGANIZATION_CODE          VARCHAR2(3 BYTE),
  TRANSACTION_ID             NUMBER,
  PROCESS_FLAG               NUMBER,
  TRANSACTION_TYPE           VARCHAR2(10 BYTE),
  SET_PROCESS_ID             NUMBER             NOT NULL,
  REVISION_ID                NUMBER,
  REVISION_LABEL             VARCHAR2(80 BYTE),
  REVISION_REASON            VARCHAR2(30 BYTE),
  LIFECYCLE_ID               NUMBER,
  CURRENT_PHASE_ID           NUMBER,
  SOURCE_SYSTEM_ID           NUMBER,
  SOURCE_SYSTEM_REFERENCE    VARCHAR2(255 BYTE),
  CHANGE_ID                  NUMBER,
  INTERFACE_TABLE_UNIQUE_ID  NUMBER,
  TEMPLATE_ID                NUMBER,
  TEMPLATE_NAME              VARCHAR2(30 BYTE),
  INSERT_DATE                DATE
)
TABLESPACE XXWC_DATA
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MAXSIZE          UNLIMITED
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;
/
GRANT ALTER, DELETE, INDEX, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON XXWC.XXWC_MTL_ITEM_REV_INTF_ARC_TBL TO APPS
/

*/
-- Added below code in TMS#20141006-00021 
DROP TABLE XXWC.XXWC_MTL_ITEM_REV_INTF_ARC_TBL;
/
CREATE TABLE XXWC.XXWC_MTL_ITEM_REV_INTF_ARC_TBL
(
  INVENTORY_ITEM_ID          NUMBER,
  ORGANIZATION_ID            NUMBER,
  REVISION                   VARCHAR2(3 BYTE),
  LAST_UPDATE_DATE           DATE,
  LAST_UPDATED_BY            NUMBER,
  CREATION_DATE              DATE,
  CREATED_BY                 NUMBER,
  LAST_UPDATE_LOGIN          NUMBER,
  CHANGE_NOTICE              VARCHAR2(10 BYTE),
  ECN_INITIATION_DATE        DATE,
  IMPLEMENTATION_DATE        DATE,
  IMPLEMENTED_SERIAL_NUMBER  VARCHAR2(30 BYTE),
  EFFECTIVITY_DATE           DATE,
  ATTRIBUTE_CATEGORY         VARCHAR2(30 BYTE),
  ATTRIBUTE1                 VARCHAR2(150 BYTE),
  ATTRIBUTE2                 VARCHAR2(150 BYTE),
  ATTRIBUTE3                 VARCHAR2(150 BYTE),
  ATTRIBUTE4                 VARCHAR2(150 BYTE),
  ATTRIBUTE5                 VARCHAR2(150 BYTE),
  ATTRIBUTE6                 VARCHAR2(150 BYTE),
  ATTRIBUTE7                 VARCHAR2(150 BYTE),
  ATTRIBUTE8                 VARCHAR2(150 BYTE),
  ATTRIBUTE9                 VARCHAR2(150 BYTE),
  ATTRIBUTE10                VARCHAR2(150 BYTE),
  ATTRIBUTE11                VARCHAR2(150 BYTE),
  ATTRIBUTE12                VARCHAR2(150 BYTE),
  ATTRIBUTE13                VARCHAR2(150 BYTE),
  ATTRIBUTE14                VARCHAR2(150 BYTE),
  ATTRIBUTE15                VARCHAR2(150 BYTE),
  REQUEST_ID                 NUMBER,
  PROGRAM_APPLICATION_ID     NUMBER,
  PROGRAM_ID                 NUMBER,
  PROGRAM_UPDATE_DATE        DATE,
  REVISED_ITEM_SEQUENCE_ID   NUMBER,
  DESCRIPTION                VARCHAR2(240 BYTE),
  ITEM_NUMBER                VARCHAR2(700 BYTE),
  ORGANIZATION_CODE          VARCHAR2(3 BYTE),
  TRANSACTION_ID             NUMBER,
  PROCESS_FLAG               NUMBER,
  TRANSACTION_TYPE           VARCHAR2(10 BYTE),
  SET_PROCESS_ID             NUMBER             NOT NULL,
  REVISION_ID                NUMBER,
  REVISION_LABEL             VARCHAR2(80 BYTE),
  REVISION_REASON            VARCHAR2(30 BYTE),
  LIFECYCLE_ID               NUMBER,
  CURRENT_PHASE_ID           NUMBER,
  SOURCE_SYSTEM_ID           NUMBER,
  SOURCE_SYSTEM_REFERENCE    VARCHAR2(255 BYTE),
  CHANGE_ID                  NUMBER,
  INTERFACE_TABLE_UNIQUE_ID  NUMBER,
  TEMPLATE_ID                NUMBER,
  TEMPLATE_NAME              VARCHAR2(30 BYTE),
  BUNDLE_ID                  NUMBER,
  INSERT_DATE                DATE
);
/
grant all on XXWC.XXWC_MTL_ITEM_REV_INTF_ARC_TBL to APPS;
/