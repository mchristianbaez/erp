 /*
   Ticket#                               Date         Author             Notes
   -------------------------------------------------------------------------------------------------------------------------------------------------------
   TMS 20170412-00106 / ESMS 554216      02/17/2017   Balaguru Seshadri  Concur process related tables
 */
-- 
DROP TABLE XXCUS.XXCUS_CONCUR_REPORT_SET_REQS;
--
CREATE TABLE XXCUS.XXCUS_CONCUR_REPORT_SET_REQS
(
  REPORT_SET_REQUEST_ID  NUMBER                 DEFAULT 0,
  WRAPPER_REQ_ID         NUMBER                 DEFAULT 0
)
;
--
COMMENT ON TABLE XXCUS.XXCUS_CONCUR_REPORT_SET_REQS IS 'TMS 20170412-00106 / ESMS 554216';
