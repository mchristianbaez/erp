/*
   Ticket#                               Date         Author             Notes
   -------------------------------------------------------------------------------------------------------------------------------------------------------
   TMS 20170412-00106 / ESMS 554216      02/17/2017   Balaguru Seshadri  Concur process related tables
*/
--
DROP TABLE XXCUS.XXCUS_CONCUR_EXTRACT_CONTROLS;
--
CREATE TABLE XXCUS.XXCUS_CONCUR_EXTRACT_CONTROLS
(
  EMPLOYEE_GROUP            VARCHAR2(48 BYTE),
  GL_GROUP_ID               NUMBER,
  JE_ACTUALS_FILE           VARCHAR2(240 BYTE),
  JE_DETAILS_FILE           VARCHAR2(240 BYTE),
  FILE_TYPE                 VARCHAR2(60 BYTE),
  SHAREPOINT_LIBRARY_NAME   VARCHAR2(60 BYTE),
  SHAREPOINT_EMAIL_ADDRESS  VARCHAR2(150 BYTE),
  SHAREPOINT_LINK           VARCHAR2(400 BYTE),
  SHAREPOINT_OWNER          VARCHAR2(60 BYTE),
  COMMENTS                  VARCHAR2(400 BYTE),
  LAST_UPDATED_BY           NUMBER,
  LAST_UPDATE_DATE          DATE,
  TYPE                      VARCHAR2(20 BYTE),
  OOP_FILE                  VARCHAR2(150 BYTE),
  CARD_FILE                 VARCHAR2(150 BYTE),
  AUDIT_FILE                VARCHAR2(150 BYTE),
  PERSONAL_CHARGES_FILE     VARCHAR2(150 BYTE)
)
;
--
COMMENT ON TABLE XXCUS.XXCUS_CONCUR_EXTRACT_CONTROLS IS 'TMS 20170412-00106 / ESMS 554216';
--


