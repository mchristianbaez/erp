/*
   Ticket#                               Date         Author             Notes
   -------------------------------------------------------------------------------------------------------------------------------------------------------
   TMS 20170412-00106 / ESMS 554216      02/17/2017   Balaguru Seshadri  Concur process related tables
*/
--
DROP TABLE XXCUS.XXCUS_CONCUR_TIE_OUT_DETAILS;
--
CREATE TABLE XXCUS.XXCUS_CONCUR_TIE_OUT_DETAILS
(
  JE_TYPE                VARCHAR2(30 BYTE),
  EMPL_ERP_GROUP         VARCHAR2(48 BYTE),
  ALLOCATION_ERP_GROUP   VARCHAR2(48 BYTE),
  ALLOCATION_ERP_ACTIVE  VARCHAR2(1 BYTE),
  CRT_BTCH_ID            NUMBER,
  CONCUR_BATCH_ID        NUMBER,
  OOP_CARD               VARCHAR2(30 BYTE),
  JOURNAL_AMOUNT         NUMBER,
  JOURNAL_LINES          NUMBER,
  CAD_TAXES              NUMBER,
  INACTIVE_ERP_DOLLARS   NUMBER,
  CREATION_DATE          DATE,
  CREATED_BY             NUMBER,
  REQUEST_ID             NUMBER,
  EMAIL_FLAG             VARCHAR2(1 BYTE)       DEFAULT 'N',
  STATUS                 VARCHAR2(60 BYTE),
  RUN_ID                 NUMBER
)
;
--
COMMENT ON TABLE XXCUS.XXCUS_CONCUR_TIE_OUT_DETAILS IS 'TMS 20170412-00106 / ESMS 554216';
--
