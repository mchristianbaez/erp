/*
   Ticket#                               Date         Author             Notes
   -------------------------------------------------------------------------------------------------------------------------------------------------------
   TMS 20170412-00106 / ESMS 554216      02/17/2017   Balaguru Seshadri  Concur process related tables
*/
--
DROP TABLE XXCUS.XXCUS_CONCUR_WW_BR_PROJ_REF;
--
CREATE TABLE XXCUS.XXCUS_CONCUR_WW_BR_PROJ_REF
(
  PROJECT_CODE      VARCHAR2(30 BYTE)           NOT NULL,
  BRANCH_CODE       VARCHAR2(30 BYTE)           NOT NULL,
  ENABLED_FLAG      VARCHAR2(1 BYTE)            NOT NULL,
  CREATED_BY        NUMBER,
  CREATION_DATE     DATE,
  LAST_UPDATED_BY   NUMBER,
  LAST_UPDATE_DATE  DATE,
  ID                NUMBER,
  OVERRIDE_DEPT     VARCHAR2(10 BYTE)           DEFAULT '030'
)
;
--
COMMENT ON TABLE XXCUS.XXCUS_CONCUR_WW_BR_PROJ_REF IS 'TMS 20170412-00106 / ESMS 554216';
--
