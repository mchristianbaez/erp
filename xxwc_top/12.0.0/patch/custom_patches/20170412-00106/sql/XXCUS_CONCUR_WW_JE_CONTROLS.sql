/*
   Ticket#                               Date         Author             Notes
   -------------------------------------------------------------------------------------------------------------------------------------------------------
   TMS 20170412-00106 / ESMS 554216      02/17/2017   Balaguru Seshadri  Concur process related tables
*/
--
DROP TABLE XXCUS.XXCUS_CONCUR_WW_JE_CONTROLS;
--
CREATE TABLE XXCUS.XXCUS_CONCUR_WW_JE_CONTROLS
(
  REQUEST_ID       NUMBER,
  LAST_JE_NUMBER   NUMBER,
  CREATED_BY       NUMBER,
  CREATION_DATE    DATE,
  FISCAL_PERIOD    VARCHAR2(10 BYTE),
  CONCUR_BATCH_ID  NUMBER
)
;
--
COMMENT ON TABLE XXCUS.XXCUS_CONCUR_WW_JE_CONTROLS IS 'TMS 20170412-00106 / ESMS 554216';
--
