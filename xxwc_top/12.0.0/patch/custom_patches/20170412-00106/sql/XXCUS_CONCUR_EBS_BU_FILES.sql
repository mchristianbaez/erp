 /*
   Ticket#                               Date         Author             Notes
   -------------------------------------------------------------------------------------------------------------------------------------------------------
   TMS 20170412-00106 / ESMS 554216      02/17/2017   Balaguru Seshadri  Concur process related tables
 */
-- 
DROP TABLE XXCUS.XXCUS_CONCUR_EBS_BU_FILES CASCADE CONSTRAINTS;
--
CREATE TABLE XXCUS.XXCUS_CONCUR_EBS_BU_FILES
(
  REQUEST_ID             NUMBER,
  CONCUR_BATCH_ID        VARCHAR2(20 BYTE),
  EMP_GROUP              VARCHAR2(48 BYTE),
  EXTRACTED_FILE_NAME    VARCHAR2(150 BYTE),
  EXTRACTED_FILE_FOLDER  VARCHAR2(400 BYTE),
  CREATED_BY             NUMBER,
  CREATION_DATE          DATE,
  CRT_BTCH_ID            NUMBER
)
;
--
COMMENT ON TABLE XXCUS.XXCUS_CONCUR_EBS_BU_FILES IS 'TMS 20170412-00106 / ESMS 554216';
--
