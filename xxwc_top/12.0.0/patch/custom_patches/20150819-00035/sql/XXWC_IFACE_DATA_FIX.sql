/*************************************************************************
   *   $Header XXWC_IFACE_DATA_FIX.sql $
   *   Module Name: OM
   *
   *   PURPOSE:   delete records from IFACE tables 
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        20/08/2015  Hari Prasad M           Initial Version
   *   
   * ***************************************************************************/

delete  from OE_HEADERS_IFACE_ALL  where orig_sys_document_ref in 
 ( select to_char(requisition_header_id) from po_requisition_headers_all a
where authorization_status in ('APPROVED','CANCELLED') 
and type_lookup_code='INTERNAL' 
and TRANSFERRED_TO_OE_FLAG='Y'
and to_char(requisition_header_id) in (select orig_sys_document_ref from apps.oe_headers_iface_all)
and exists (select * from oe_order_headers_all where orig_sys_document_ref=to_char(a.segment1)))
/
delete  from OE_LINES_IFACE_ALL  where orig_sys_document_ref in 
 ( select to_char(requisition_header_id) from po_requisition_headers_all a
where authorization_status in ('APPROVED','CANCELLED') 
and type_lookup_code='INTERNAL' 
and TRANSFERRED_TO_OE_FLAG='Y'
AND to_char(REQUISITION_HEADER_ID) IN (SELECT ORIG_SYS_DOCUMENT_REF FROM APPS.OE_HEADERS_IFACE_ALL)
AND EXISTS (SELECT * FROM OE_ORDER_HEADERS_ALL WHERE ORIG_SYS_DOCUMENT_REF=TO_CHAR(A.SEGMENT1)))
/
delete  from OE_ACTIONS_IFACE_ALL  where orig_sys_document_ref in 
 ( select to_char(requisition_header_id) from po_requisition_headers_all a
where authorization_status in ('APPROVED','CANCELLED') 
and type_lookup_code='INTERNAL' 
and TRANSFERRED_TO_OE_FLAG='Y'
AND to_char(REQUISITION_HEADER_ID) IN (SELECT ORIG_SYS_DOCUMENT_REF FROM APPS.OE_HEADERS_IFACE_ALL)
AND EXISTS (SELECT * FROM OE_ORDER_HEADERS_ALL WHERE ORIG_SYS_DOCUMENT_REF=TO_CHAR(A.SEGMENT1)))
/
commit;
/