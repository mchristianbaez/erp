/*************************************************************************
  $Header ALT_XXWC_B2B_SO_DELIVERY_INFO_TBL.sql $
  Module Name: ALT_XXWC_B2B_SO_DELIVERY_INFO_TBL.sql

  PURPOSE:   To Add the column subinventory

  REVISIONS:
  Ver        Date        Author                     Description
  ---------  ----------  ---------------         -------------------------
  1.0        04/17/2016  Rakesh Patel            TMS#20170417-00084-B2B mulitple email id's exist causing issue with Packslip and SOA email generation
**************************************************************************/
ALTER TABLE XXWC.XXWC_B2B_SO_DELIVERY_INFO_TBL modify (CC_EMAIL VARCHAR2(2500));