/*
 TMS:  20180423-00285
 Date: 04/23/2018
 Notes:  Prod Grid Alert: Excessive parallelism on ozf and xxcus indexes because of persistent rebuilds causing spike in CPU every Tuesday on or after 10PM EST when rebates FAE runs.
*/
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
 --
 n_loc number :=0;
 l_sql varchar2(240) :=Null;
 --
    cursor c_indexes is
	select 'ALTER INDEX '||owner||'.'||index_name||' NOPARALLEL' ALTER_IDX 
	from all_indexes
	where 1 =1
	and owner IN ('OZF', 'APPS', 'XXCUS')
	and (table_name like 'XXCUS%' or table_name like 'OZF%') 
	and to_number(trim(degree)) >0
	;
 --
BEGIN --Main Processing...
   --
   n_loc :=101;
   --
   begin
        for rec in c_indexes loop
          l_sql :=rec.alter_idx;
          execute immediate l_sql;
           dbms_output.put_line
           (
             'Stmnt : '||l_sql||', msg =>'||sqlerrm
           );	  
        end loop;
   exception   
    when others then 
	   dbms_output.put_line
	   (
		 n_loc||', l_sql ='||l_sql||', msg =>'||sqlerrm
	   );
   end;
   --
   n_loc :=102;
   --   
   commit;
   --
EXCEPTION
   WHEN OTHERS THEN
      DBMS_OUTPUT.put_line ('TMS: 20180423-00285, Errors =' || SQLERRM);
END;
/