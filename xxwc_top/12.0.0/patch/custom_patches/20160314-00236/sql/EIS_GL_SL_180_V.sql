DROP VIEW XXEIS.EIS_GL_SL_180_V;

/* Formatted on 3/14/2016 4:54:10 PM (QP5 v5.240.12305.39446) */
CREATE OR REPLACE FORCE VIEW XXEIS.EIS_GL_SL_180_V
(
   SOURCE,
   JE_CATEGORY,
   JE_HEADER_ID,
   JE_LINE_NUM,
   ACC_DATE,
   ENTRY,
   BATCH,
   LINE_DESCR,
   LSEQUENCE,
   HNUMBER,
   LLINE,
   LINE_ACCTD_DR,
   LINE_ACCTD_CR,
   LINE_ENT_DR,
   LINE_ENT_CR,
   SEQ_ID,
   SEQ_NUM,
   H_SEQ_ID,
   GL_SL_LINK_ID,
   CUSTOMER_OR_VENDOR,
   ENTERED_DR,
   ENTERED_CR,
   ACCOUNTED_DR,
   ACCOUNTED_CR,
   SLA_LINE_ACCOUNTED_DR,
   SLA_LINE_ACCOUNTED_CR,
   SLA_LINE_ACCOUNTED_NET,
   SLA_LINE_ENTERED_DR,
   SLA_LINE_ENTERED_CR,
   SLA_LINE_ENTERED_NET,
   SLA_DIST_ENTERED_CR,
   SLA_DIST_ENTERED_DR,
   SLA_DIST_ENTERED_NET,
   SLA_DIST_ACCOUNTED_CR,
   SLA_DIST_ACCOUNTED_DR,
   SLA_DIST_ACCOUNTED_NET,
   ASSOCIATE_NUM,
   TRANSACTION_NUM,
   GL_ACCOUNT_STRING,
   CURRENCY_CODE,
   PERIOD_NAME,
   TYPE,
   EFFECTIVE_PERIOD_NUM,
   NAME,
   BATCH_NAME,
   CODE_COMBINATION_ID,
   GCC#BRANCH,
   GCC#50328#ACCOUNT,
   GCC#50328#ACCOUNT#DESCR,
   GCC#50328#COST_CENTER,
   GCC#50328#COST_CENTER#DESCR,
   GCC#50328#FURTURE_USE,
   GCC#50328#FURTURE_USE#DESCR,
   GCC#50328#FUTURE_USE_2,
   GCC#50328#FUTURE_USE_2#DESCR,
   GCC#50328#LOCATION,
   GCC#50328#LOCATION#DESCR,
   GCC#50328#PRODUCT,
   GCC#50328#PRODUCT#DESCR,
   GCC#50328#PROJECT_CODE,
   GCC#50328#PROJECT_CODE#DESCR,
   GCC#50368#ACCOUNT,
   GCC#50368#ACCOUNT#DESCR,
   GCC#50368#DEPARTMENT,
   GCC#50368#DEPARTMENT#DESCR,
   GCC#50368#DIVISION,
   GCC#50368#DIVISION#DESCR,
   GCC#50368#FUTURE_USE,
   GCC#50368#FUTURE_USE#DESCR,
   GCC#50368#PRODUCT,
   GCC#50368#PRODUCT#DESCR,
   GCC#50368#SUBACCOUNT,
   GCC#50368#SUBACCOUNT#DESCR
)
AS
   (SELECT jh.je_source SOURCE,
           jh.je_category,
           jl.je_header_id je_header_id,
           jl.je_line_num je_line_num,
           jl.effective_date acc_date,
           jh.NAME entry,
           jh.je_batch_id batch,
           SUBSTR (jl.description, 1, 240) line_descr,
           NULL LSequence,
           jh.doc_sequence_value hnumber,
           jl.je_line_num lline,
           jl.accounted_dr line_acctd_dr,
           jl.accounted_cr line_acctd_cr,
           jl.entered_dr line_ent_dr,
           jl.entered_cr line_ent_cr,
           NULL seq_id,
           NULL seq_num,
           jh.doc_sequence_id h_seq_id,
           NULL gl_sl_link_id,
           NULL customer_or_vendor,
           jl.entered_dr ENTERED_DR,
           jl.entered_cr ENTERED_CR,
           jl.accounted_cr ACCOUNTED_DR,
           jl.accounted_dr ACCOUNTED_CR,
           jl.accounted_dr sla_line_accounted_dr,
           jl.accounted_cr sla_line_accounted_cr,
           NVL (jl.accounted_dr, 0) - NVL (jl.accounted_cr, 0)
              sla_line_accounted_net,
           jl.entered_dr sla_line_entered_dr,
           jl.entered_cr sla_line_entered_cr,
           NVL (jl.entered_dr, 0) - NVL (jl.entered_cr, 0)
              sla_line_entered_net,
           jl.entered_cr sla_dist_entered_cr,
           jl.entered_dr sla_dist_entered_dr,
           NVL (jl.entered_cr, 0) - NVL (jl.entered_dr, 0)
              sla_dist_entered_net,
           jl.accounted_cr sla_dist_accounted_cr,
           jl.accounted_dr sla_dist_accounted_dr,
           NVL (jl.accounted_cr, 0) - NVL (jl.accounted_dr, 0)
              sla_dist_accounted_net,
           '' associate_num,
           NULL transaction_num,
           gcc.CONCATENATED_SEGMENTS gl_account_string,
           jh.currency_code,
           jh.period_name,
           NULL TYPE,
           gps.effective_period_num,
           gle.name,
           gb.name batch_name,
           gcc.code_combination_id                     --descr#flexfield#start
                                  ,
           xxeis.eis_rs_dff.decode_valueset ('XXCUS_FRU',
                                             GCC.ATTRIBUTE1,
                                             'I')
              GCC#Branch                                 --descr#flexfield#end
                        --gl#accountff#start
           ,
           GCC.SEGMENT4 GCC#50328#ACCOUNT,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT4,
                                                 'XXCUS_GL_ACCOUNT')
              GCC#50328#ACCOUNT#DESCR,
           GCC.SEGMENT3 GCC#50328#COST_CENTER,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT3,
                                                 'XXCUS_GL_COSTCENTER')
              GCC#50328#COST_CENTER#DESCR,
           GCC.SEGMENT6 GCC#50328#FURTURE_USE,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT6,
                                                 'XXCUS_GL_FUTURE_USE1')
              GCC#50328#FURTURE_USE#DESCR,
           GCC.SEGMENT7 GCC#50328#FUTURE_USE_2,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT7,
                                                 'XXCUS_GL_FUTURE_USE_2')
              GCC#50328#FUTURE_USE_2#DESCR,
           GCC.SEGMENT2 GCC#50328#LOCATION,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT2,
                                                 'XXCUS_GL_LOCATION')
              GCC#50328#LOCATION#DESCR,
           GCC.SEGMENT1 GCC#50328#PRODUCT,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT1,
                                                 'XXCUS_GL_PRODUCT')
              GCC#50328#PRODUCT#DESCR,
           GCC.SEGMENT5 GCC#50328#PROJECT_CODE,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT5,
                                                 'XXCUS_GL_PROJECT')
              GCC#50328#PROJECT_CODE#DESCR,
           GCC.SEGMENT4 GCC#50368#ACCOUNT,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT4,
                                                 'XXCUS_GL_ LTMR _ACCOUNT')
              GCC#50368#ACCOUNT#DESCR,
           GCC.SEGMENT3 GCC#50368#DEPARTMENT,
           xxeis.eis_rs_fin_utility.decode_vset (
              GCC.SEGMENT3,
              'XXCUS_GL_ LTMR _DEPARTMENT')
              GCC#50368#DEPARTMENT#DESCR,
           GCC.SEGMENT2 GCC#50368#DIVISION,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT2,
                                                 'XXCUS_GL_ LTMR _DIVISION')
              GCC#50368#DIVISION#DESCR,
           GCC.SEGMENT6 GCC#50368#FUTURE_USE,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT6,
                                                 'XXCUS_GL_ LTMR _FUTUREUSE')
              GCC#50368#FUTURE_USE#DESCR,
           GCC.SEGMENT1 GCC#50368#PRODUCT,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT1,
                                                 'XXCUS_GL_LTMR_PRODUCT')
              GCC#50368#PRODUCT#DESCR,
           GCC.SEGMENT5 GCC#50368#SUBACCOUNT,
           xxeis.eis_rs_fin_utility.decode_vset (
              GCC.SEGMENT5,
              'XXCUS_GL_ LTMR _SUBACCOUNT')
              GCC#50368#SUBACCOUNT#DESCR
      --gl#accountff#end

      FROM gl_je_lines jl,
           gl_je_headers jh,
           -- gl_import_references ir,
           --  FND_DOCUMENT_SEQUENCES  ds,
           gl_code_combinations_kfv gcc,
           gl_ledgers gle,
           gl_period_statuses gps,
           gl_je_batches gb
     WHERE     jl.status = 'P'
           AND jh.status = 'P'
           AND jh.actual_flag = 'A'
           AND jh.je_header_id = jl.je_header_id
           -- AND ir.je_header_id(+) = jl.je_header_id
           -- AND ir.je_line_num(+) = jl.je_line_num
           --  and ds.doc_sequence_id (+) = ir.subledger_doc_sequence_id
           AND jh.je_source NOT IN ('Payables', 'Receivables')
           AND gcc.code_combination_id = jl.code_combination_id
           AND gle.ledger_id = gps.ledger_id
           AND gps.application_id = 101
           AND gps.period_name = jh.period_name
           AND gle.chart_of_Accounts_id = gcc.chart_of_accounts_id
           AND gps.ledger_id = gle.ledger_id
           AND gle.ledger_id = jh.ledger_id                             --bala
           AND jh.je_batch_id = gb.je_batch_id
    UNION ALL
    SELECT jh.je_source SOURCE,
           jh.je_category,
           jir.je_header_id je_header_id,
           jir.je_line_num je_line_num,
           jl.effective_date acc_date,
           jh.NAME entry,
           jh.je_batch_id batch,
           SUBSTR (jl.description, 1, 240) line_descr,
           NULL LSequence,
           jh.doc_sequence_value hnumber,
           jl.je_line_num lline,
           jl.accounted_dr line_acctd_dr,
           jl.accounted_cr line_acctd_cr,
           jl.entered_dr line_ent_dr,
           jl.entered_cr line_ent_cr,
           jir.subledger_doc_sequence_id seq_id,
           jir.subledger_doc_sequence_value seq_num,
           jh.doc_sequence_id h_seq_id,
           jir.gl_sl_link_id gl_sl_link_id,
           (SELECT cu.customer_name
              FROM ra_customers cu
             WHERE cu.customer_id = TO_NUMBER (jir.reference_7))
              customer_or_vendor,
           DECODE (
              ra_dist.cust_trx_line_gl_dist_id,
              NULL, dist.AMOUNT_DR,
              TO_NUMBER (
                 DECODE (
                    ra_dist.account_class,
                    'REC', DECODE (SIGN (NVL (ra_dist.amount, 0)),
                                   -1, NULL,
                                   NVL (ra_dist.amount, 0)),
                    DECODE (SIGN (NVL (ra_dist.amount, 0)),
                            -1, -NVL (ra_dist.amount, 0),
                            NULL))))
              Entered_DR,
           DECODE (
              ra_dist.cust_trx_line_gl_dist_id,
              NULL, dist.AMOUNT_cR,
              TO_NUMBER (
                 DECODE (
                    ra_dist.account_class,
                    'REC', DECODE (SIGN (NVL (ra_dist.amount, 0)),
                                   -1, -NVL (ra_dist.amount, 0),
                                   NULL),
                    DECODE (SIGN (NVL (ra_dist.amount, 0)),
                            -1, NULL,
                            NVL (ra_dist.amount, 0)))))
              Entered_CR,
           DECODE (
              ra_dist.cust_trx_line_gl_dist_id,
              NULL, dist.ACCTD_AMOUNT_DR,
              TO_NUMBER (
                 DECODE (
                    ra_dist.account_class,
                    'REC', DECODE (SIGN (NVL (ra_dist.amount, 0)),
                                   -1, NULL,
                                   NVL (ra_dist.acctd_amount, 0)),
                    DECODE (SIGN (NVL (ra_dist.amount, 0)),
                            -1, -NVL (ra_dist.acctd_amount, 0),
                            NULL))))
              Accounted_DR,
           DECODE (
              ra_dist.cust_trx_line_gl_dist_id,
              NULL, dist.ACCTD_AMOUNT_CR,
              TO_NUMBER (
                 DECODE (
                    ra_dist.account_class,
                    'REC', DECODE (SIGN (NVL (ra_dist.amount, 0)),
                                   -1, -NVL (ra_dist.acctd_amount, 0),
                                   NULL),
                    DECODE (SIGN (NVL (ra_dist.amount, 0)),
                            -1, NULL,
                            NVL (ra_DIST.acctd_amount, 0)))))
              Accounted_CR,
           ael.unrounded_accounted_dr sla_line_accounted_dr,
           ael.unrounded_accounted_cr sla_line_accounted_cr,
             NVL (ael.unrounded_accounted_dr, 0)
           - NVL (ael.unrounded_accounted_cr, 0)
              sla_line_accounted_net,
           ael.unrounded_entered_dr sla_line_entered_dr,
           ael.unrounded_entered_cr sla_line_entered_cr,
             NVL (ael.unrounded_entered_dr, 0)
           - NVL (ael.unrounded_entered_cr, 0)
              sla_line_entered_net,
           xdl.unrounded_entered_cr sla_dist_entered_cr,
           xdl.unrounded_entered_dr sla_dist_entered_dr,
             NVL (xdl.unrounded_entered_cr, 0)
           - NVL (xdl.unrounded_entered_dr, 0)
              sla_dist_entered_net,
           xdl.unrounded_accounted_cr sla_dist_accounted_cr,
           xdl.unrounded_accounted_dr sla_dist_accounted_dr,
             NVL (xdl.unrounded_accounted_cr, 0)
           - NVL (xdl.unrounded_accounted_dr, 0)
              sla_dist_accounted_net,
           DECODE (
              jIR.reference_10,
              'AR_ADJUSTMENTS', jIR.reference_4,
              'AR_RECEIVABLE_APPLICATIONS', DECODE (jir.reference_8,
                                                    'TRADE', NULL,
                                                    jIR.reference_5))
              associate_num,
           DECODE (
              jir.reference_10,
              'AR_ADJUSTMENTS', jir.reference_5,
              'AR_CASH_RECEIPT_HISTORY', jir.reference_4,
              'AR_RECEIVABLE_APPLICATIONS', DECODE (jir.reference_8,
                                                    'CMAPP', jir.reference_4,
                                                    'TRADE', jir.reference_4,
                                                    NULL),
              jir.reference_4)
              transaction_num,
           gcc.CONCATENATED_SEGMENTS gl_account_string,
           jh.currency_code,
           jh.period_name,
           NVL (dist.source_type, NULL) TYPE,
           gps.effective_period_num,
           gle.name,
           gb.name batch_name,
           gcc.code_combination_id                     --descr#flexfield#start
                                  ,
           xxeis.eis_rs_dff.decode_valueset ('XXCUS_FRU',
                                             GCC.ATTRIBUTE1,
                                             'I')
              GCC#Branch                                 --descr#flexfield#end
                        --gl#accountff#start
           ,
           GCC.SEGMENT4 GCC#50328#ACCOUNT,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT4,
                                                 'XXCUS_GL_ACCOUNT')
              GCC#50328#ACCOUNT#DESCR,
           GCC.SEGMENT3 GCC#50328#COST_CENTER,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT3,
                                                 'XXCUS_GL_COSTCENTER')
              GCC#50328#COST_CENTER#DESCR,
           GCC.SEGMENT6 GCC#50328#FURTURE_USE,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT6,
                                                 'XXCUS_GL_FUTURE_USE1')
              GCC#50328#FURTURE_USE#DESCR,
           GCC.SEGMENT7 GCC#50328#FUTURE_USE_2,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT7,
                                                 'XXCUS_GL_FUTURE_USE_2')
              GCC#50328#FUTURE_USE_2#DESCR,
           GCC.SEGMENT2 GCC#50328#LOCATION,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT2,
                                                 'XXCUS_GL_LOCATION')
              GCC#50328#LOCATION#DESCR,
           GCC.SEGMENT1 GCC#50328#PRODUCT,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT1,
                                                 'XXCUS_GL_PRODUCT')
              GCC#50328#PRODUCT#DESCR,
           GCC.SEGMENT5 GCC#50328#PROJECT_CODE,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT5,
                                                 'XXCUS_GL_PROJECT')
              GCC#50328#PROJECT_CODE#DESCR,
           GCC.SEGMENT4 GCC#50368#ACCOUNT,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT4,
                                                 'XXCUS_GL_ LTMR _ACCOUNT')
              GCC#50368#ACCOUNT#DESCR,
           GCC.SEGMENT3 GCC#50368#DEPARTMENT,
           xxeis.eis_rs_fin_utility.decode_vset (
              GCC.SEGMENT3,
              'XXCUS_GL_ LTMR _DEPARTMENT')
              GCC#50368#DEPARTMENT#DESCR,
           GCC.SEGMENT2 GCC#50368#DIVISION,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT2,
                                                 'XXCUS_GL_ LTMR _DIVISION')
              GCC#50368#DIVISION#DESCR,
           GCC.SEGMENT6 GCC#50368#FUTURE_USE,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT6,
                                                 'XXCUS_GL_ LTMR _FUTUREUSE')
              GCC#50368#FUTURE_USE#DESCR,
           GCC.SEGMENT1 GCC#50368#PRODUCT,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT1,
                                                 'XXCUS_GL_LTMR_PRODUCT')
              GCC#50368#PRODUCT#DESCR,
           GCC.SEGMENT5 GCC#50368#SUBACCOUNT,
           xxeis.eis_rs_fin_utility.decode_vset (
              GCC.SEGMENT5,
              'XXCUS_GL_ LTMR _SUBACCOUNT')
              GCC#50368#SUBACCOUNT#DESCR
      --gl#accountff#end

      FROM gl_ledgers gle,
           gl_je_headers jh,
           gl_je_lines jl,
           gl_code_combinations_kfv gcc,
           gl_import_references jir,
           xla_distribution_links xdl,
           xla_ae_lines ael,
           xla_ae_headers aeh,
           ar_distributions_all dist,
           ra_cust_trx_line_gl_dist_all ra_dist,
           gl_period_statuses gps,
           gl_je_batches gb
     WHERE     jh.je_header_id = jl.je_header_id
           AND jl.code_combination_id = gcc.code_combination_id
           AND jh.je_header_id = jir.je_header_id
           AND jl.je_line_num = jir.je_line_num
           AND jh.ledger_id = gle.ledger_id
           AND jh.je_source = 'Receivables'
           AND jir.gl_sl_link_id = ael.gl_sl_link_id(+)
           AND jir.gl_sl_link_table = ael.gl_sl_link_table(+)
           AND ael.ae_line_num = xdl.ae_line_num(+)
           AND ael.ae_header_id = xdl.ae_header_id(+)
           AND ael.ae_header_id = aeh.ae_header_id(+)
           AND dist.line_id(+) =
                  DECODE (
                     xdl.source_distribution_type,
                     'AR_DISTRIBUTIONS_ALL', xdl.source_distribution_id_num_1,
                     NULL)
           AND ra_dist.cust_trx_line_gl_dist_id(+) =
                  DECODE (xdl.source_distribution_type,
                          'AR_DISTRIBUTIONS_ALL', NULL,
                          xdl.source_distribution_id_num_1)
           AND gps.application_id = 101
           AND gps.period_name = jh.period_name
           AND gps.ledger_id = gle.ledger_id
           AND jh.je_batch_id = gb.je_batch_id
    UNION ALL
    SELECT jh.je_source SOURCE,
           jh.je_category,
           gir.je_header_id je_header_id,
           gir.je_line_num je_line_num,
           jl.effective_date acc_date,
           jh.NAME entry,
           jh.je_batch_id batch,
           SUBSTR (jl.description, 1, 240) line_descr,
           ds.name LSequence,
           jh.doc_sequence_value hnumber,
           jl.je_line_num lline,
           jl.accounted_dr line_acctd_dr,
           jl.accounted_cr line_acctd_cr,
           jl.entered_dr ent_dr,
           jl.entered_cr ent_cr,
           gir.subledger_doc_sequence_id seq_id,
           gir.subledger_doc_sequence_value seq_num,
           jh.doc_sequence_id h_seq_id,
           gir.gl_sl_link_id gl_sl_link_id,
           pov.vendor_name customer_or_vendor,
           jl.entered_dr ENTERED_DR,
           jl.entered_cr ENTERED_CR,
           jl.accounted_cr ACCOUNTED_DR,
           jl.accounted_dr ACCOUNTED_CR,
           ael.unrounded_accounted_dr sla_line_accounted_dr,
           ael.unrounded_accounted_cr sla_line_accounted_cr,
             NVL (ael.unrounded_accounted_dr, 0)
           - NVL (ael.unrounded_accounted_cr, 0)
              sla_line_accounted_net,
           ael.unrounded_entered_dr sla_line_entered_dr,
           ael.unrounded_entered_cr sla_line_entered_cr,
             NVL (ael.unrounded_entered_dr, 0)
           - NVL (ael.unrounded_entered_cr, 0)
              sla_line_entered_net,
           adl.unrounded_entered_cr sla_dist_entered_cr,
           adl.unrounded_entered_dr sla_dist_entered_dr,
             NVL (adl.unrounded_entered_cr, 0)
           - NVL (adl.unrounded_entered_dr, 0)
              sla_dist_entered_net,
           adl.unrounded_accounted_cr sla_dist_accounted_cr,
           adl.unrounded_accounted_dr sla_dist_accounted_dr,
             NVL (adl.unrounded_accounted_cr, 0)
           - NVL (adl.unrounded_accounted_dr, 0)
              sla_dist_accounted_net,
           '' associate_num,
           AI.INVOICE_NUM transaction_mum,
           gcc.CONCATENATED_SEGMENTS gl_account_string,
           jh.currency_code,
           jh.period_name,
           ael.accounting_class_code TYPE,
           gps.effective_period_num,
           le.name,
           gb.name batch_name,
           gcc.code_combination_id                     --descr#flexfield#start
                                  ,
           xxeis.eis_rs_dff.decode_valueset ('XXCUS_FRU',
                                             GCC.ATTRIBUTE1,
                                             'I')
              GCC#Branch                                 --descr#flexfield#end
                        --gl#accountff#start
           ,
           GCC.SEGMENT4 GCC#50328#ACCOUNT,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT4,
                                                 'XXCUS_GL_ACCOUNT')
              GCC#50328#ACCOUNT#DESCR,
           GCC.SEGMENT3 GCC#50328#COST_CENTER,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT3,
                                                 'XXCUS_GL_COSTCENTER')
              GCC#50328#COST_CENTER#DESCR,
           GCC.SEGMENT6 GCC#50328#FURTURE_USE,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT6,
                                                 'XXCUS_GL_FUTURE_USE1')
              GCC#50328#FURTURE_USE#DESCR,
           GCC.SEGMENT7 GCC#50328#FUTURE_USE_2,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT7,
                                                 'XXCUS_GL_FUTURE_USE_2')
              GCC#50328#FUTURE_USE_2#DESCR,
           GCC.SEGMENT2 GCC#50328#LOCATION,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT2,
                                                 'XXCUS_GL_LOCATION')
              GCC#50328#LOCATION#DESCR,
           GCC.SEGMENT1 GCC#50328#PRODUCT,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT1,
                                                 'XXCUS_GL_PRODUCT')
              GCC#50328#PRODUCT#DESCR,
           GCC.SEGMENT5 GCC#50328#PROJECT_CODE,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT5,
                                                 'XXCUS_GL_PROJECT')
              GCC#50328#PROJECT_CODE#DESCR,
           GCC.SEGMENT4 GCC#50368#ACCOUNT,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT4,
                                                 'XXCUS_GL_ LTMR _ACCOUNT')
              GCC#50368#ACCOUNT#DESCR,
           GCC.SEGMENT3 GCC#50368#DEPARTMENT,
           xxeis.eis_rs_fin_utility.decode_vset (
              GCC.SEGMENT3,
              'XXCUS_GL_ LTMR _DEPARTMENT')
              GCC#50368#DEPARTMENT#DESCR,
           GCC.SEGMENT2 GCC#50368#DIVISION,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT2,
                                                 'XXCUS_GL_ LTMR _DIVISION')
              GCC#50368#DIVISION#DESCR,
           GCC.SEGMENT6 GCC#50368#FUTURE_USE,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT6,
                                                 'XXCUS_GL_ LTMR _FUTUREUSE')
              GCC#50368#FUTURE_USE#DESCR,
           GCC.SEGMENT1 GCC#50368#PRODUCT,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT1,
                                                 'XXCUS_GL_LTMR_PRODUCT')
              GCC#50368#PRODUCT#DESCR,
           GCC.SEGMENT5 GCC#50368#SUBACCOUNT,
           xxeis.eis_rs_fin_utility.decode_vset (
              GCC.SEGMENT5,
              'XXCUS_GL_ LTMR _SUBACCOUNT')
              GCC#50368#SUBACCOUNT#DESCR
      --gl#accountff#end

      FROM gl_ledgers le,
           gl_je_headers jh,
           gl_je_lines jl,
           gl_import_references gir,
           gl_code_combinations_kfv gcc,
           xla_ae_lines ael,
           gl_code_combinations_kfv gcc1,
           xla_ae_headers aeh,
           xla_distribution_links adl,
           AP_INVOICES_ALL AI,
           AP_INVOICE_DISTRIBUTIONS_ALL AID,
           --  AP_CHECKS_ALL AC,
           -- AP_INVOICE_PAYMENTS_ALL AIP,
           po_vendors pov,
           gl_period_statuses gps,
           FND_DOCUMENT_SEQUENCES ds,
           gl_je_batches gb
     WHERE     le.ledger_id = jh.ledger_id
           AND jh.je_header_id = jl.je_header_id
           AND gir.je_header_id = jh.je_header_id
           AND gir.je_line_num = jl.je_line_num
           AND jl.code_combination_id = gcc.code_combination_id
           AND gir.gl_sl_link_id = ael.gl_sl_link_id(+)
           AND gir.gl_sl_link_table = ael.gl_sl_link_table(+)
           AND ael.ae_header_id = aeh.ae_header_id(+)
           AND ael.code_combination_id = gcc1.code_combination_id(+)
           AND ael.ae_header_id = adl.ae_header_id(+)
           AND ael.ae_line_num = adl.ae_line_num(+)
           AND adl.SOURCE_DISTRIBUTION_TYPE = 'AP_INV_DIST'
           AND adl.SOURCE_DISTRIBUTION_ID_NUM_1 = AID.INVOICE_DISTRIBUTION_ID
           -- AND aip.invoice_id(+)=AID.INVOICE_DISTRIBUTION_ID
           -- AND AIP.CHECK_ID=AC.CHECK_ID(+)
           AND AID.INVOICE_ID = AI.INVOICE_ID
           AND POV.VENDOR_ID = AI.VENDOR_ID
           AND jh.je_source = 'Payables'
           AND le.object_type_code = 'L'
           AND gps.application_id = 101
           AND gps.period_name = jh.period_name
           AND ds.doc_sequence_id(+) = gir.subledger_doc_sequence_id
           AND gps.ledger_id = le.ledger_id
           AND jh.je_batch_id = gb.je_batch_id
    UNION ALL
    SELECT jh.je_source SOURCE,
           jh.je_category,
           gir.je_header_id je_header_id,
           gir.je_line_num je_line_num,
           jl.effective_date acc_date,
           jh.NAME entry,
           jh.je_batch_id batch,
           SUBSTR (jl.description, 1, 240) line_descr,
           ds.name LSequence,
           jh.doc_sequence_value hnumber,
           jl.je_line_num lline,
           jl.accounted_dr line_acctd_dr,
           jl.accounted_cr line_acctd_cr,
           jl.entered_dr ent_dr,
           jl.entered_cr ent_cr,
           gir.subledger_doc_sequence_id seq_id,
           gir.subledger_doc_sequence_value seq_num,
           jh.doc_sequence_id h_seq_id,
           gir.gl_sl_link_id gl_sl_link_id,
           pov.vendor_name customer_or_vendor,
           jl.entered_dr ENTERED_DR,
           jl.entered_cr ENTERED_CR,
           jl.accounted_cr ACCOUNTED_DR,
           jl.accounted_dr ACCOUNTED_CR,
           ael.unrounded_accounted_dr sla_line_accounted_dr,
           ael.unrounded_accounted_cr sla_line_accounted_cr,
             NVL (ael.unrounded_accounted_dr, 0)
           - NVL (ael.unrounded_accounted_cr, 0)
              sla_line_accounted_net,
           ael.unrounded_entered_dr sla_line_entered_dr,
           ael.unrounded_entered_cr sla_line_entered_cr,
             NVL (ael.unrounded_entered_dr, 0)
           - NVL (ael.unrounded_entered_cr, 0)
              sla_line_entered_net,
           adl.unrounded_entered_cr sla_dist_entered_cr,
           adl.unrounded_entered_dr sla_dist_entered_dr,
             NVL (adl.unrounded_entered_cr, 0)
           - NVL (adl.unrounded_entered_dr, 0)
              sla_dist_entered_net,
           adl.unrounded_accounted_cr sla_dist_accounted_cr,
           ADL.UNROUNDED_ACCOUNTED_DR SLA_DIST_ACCOUNTED_DR,
             NVL (adl.unrounded_accounted_cr, 0)
           - NVL (adl.unrounded_accounted_dr, 0)
              sla_dist_accounted_net,
           TO_CHAR (
              DECODE (adl.SOURCE_DISTRIBUTION_TYPE,
                      'AP_PMT_DIST', AC.CHECK_NUMBER,
                      ''))
              associate_num,
           AI.INVOICE_NUM transaction_mum,
           gcc.CONCATENATED_SEGMENTS gl_account_string,
           jh.currency_code,
           jh.period_name,
           ael.accounting_class_code TYPE,
           gps.effective_period_num,
           le.name,
           gb.name batch_name,
           gcc.code_combination_id                     --descr#flexfield#start
                                  ,
           xxeis.eis_rs_dff.decode_valueset ('XXCUS_FRU',
                                             GCC.ATTRIBUTE1,
                                             'I')
              GCC#Branch                                 --descr#flexfield#end
                        --gl#accountff#start
           ,
           GCC.SEGMENT4 GCC#50328#ACCOUNT,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT4,
                                                 'XXCUS_GL_ACCOUNT')
              GCC#50328#ACCOUNT#DESCR,
           GCC.SEGMENT3 GCC#50328#COST_CENTER,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT3,
                                                 'XXCUS_GL_COSTCENTER')
              GCC#50328#COST_CENTER#DESCR,
           GCC.SEGMENT6 GCC#50328#FURTURE_USE,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT6,
                                                 'XXCUS_GL_FUTURE_USE1')
              GCC#50328#FURTURE_USE#DESCR,
           GCC.SEGMENT7 GCC#50328#FUTURE_USE_2,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT7,
                                                 'XXCUS_GL_FUTURE_USE_2')
              GCC#50328#FUTURE_USE_2#DESCR,
           GCC.SEGMENT2 GCC#50328#LOCATION,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT2,
                                                 'XXCUS_GL_LOCATION')
              GCC#50328#LOCATION#DESCR,
           GCC.SEGMENT1 GCC#50328#PRODUCT,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT1,
                                                 'XXCUS_GL_PRODUCT')
              GCC#50328#PRODUCT#DESCR,
           GCC.SEGMENT5 GCC#50328#PROJECT_CODE,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT5,
                                                 'XXCUS_GL_PROJECT')
              GCC#50328#PROJECT_CODE#DESCR,
           GCC.SEGMENT4 GCC#50368#ACCOUNT,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT4,
                                                 'XXCUS_GL_ LTMR _ACCOUNT')
              GCC#50368#ACCOUNT#DESCR,
           GCC.SEGMENT3 GCC#50368#DEPARTMENT,
           xxeis.eis_rs_fin_utility.decode_vset (
              GCC.SEGMENT3,
              'XXCUS_GL_ LTMR _DEPARTMENT')
              GCC#50368#DEPARTMENT#DESCR,
           GCC.SEGMENT2 GCC#50368#DIVISION,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT2,
                                                 'XXCUS_GL_ LTMR _DIVISION')
              GCC#50368#DIVISION#DESCR,
           GCC.SEGMENT6 GCC#50368#FUTURE_USE,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT6,
                                                 'XXCUS_GL_ LTMR _FUTUREUSE')
              GCC#50368#FUTURE_USE#DESCR,
           GCC.SEGMENT1 GCC#50368#PRODUCT,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT1,
                                                 'XXCUS_GL_LTMR_PRODUCT')
              GCC#50368#PRODUCT#DESCR,
           GCC.SEGMENT5 GCC#50368#SUBACCOUNT,
           xxeis.eis_rs_fin_utility.decode_vset (
              GCC.SEGMENT5,
              'XXCUS_GL_ LTMR _SUBACCOUNT')
              GCC#50368#SUBACCOUNT#DESCR
      --gl#accountff#end

      FROM gl_ledgers le,
           gl_je_headers jh,
           gl_je_lines jl,
           gl_import_references gir,
           gl_code_combinations_kfv gcc,
           xla_ae_lines ael,
           gl_code_combinations_kfv gcc1,
           xla_ae_headers aeh,
           xla_distribution_links adl,
           ap_payment_hist_dists APHD,
           AP_INVOICES_ALL AI,
           AP_INVOICE_DISTRIBUTIONS_ALL AID,
           AP_CHECKS_ALL AC,
           AP_INVOICE_PAYMENTS_ALL AIP,
           po_vendors pov,
           gl_period_statuses gps,
           FND_DOCUMENT_SEQUENCES ds,
           gl_je_batches gb
     WHERE     le.ledger_id = jh.ledger_id
           AND jh.je_header_id = jl.je_header_id
           AND gir.je_header_id = jh.je_header_id
           AND gir.je_line_num = jl.je_line_num
           AND jl.code_combination_id = gcc.code_combination_id
           AND gir.gl_sl_link_id = ael.gl_sl_link_id(+)
           AND gir.gl_sl_link_table = ael.gl_sl_link_table(+)
           AND ael.ae_header_id = aeh.ae_header_id(+)
           AND ael.code_combination_id = gcc1.code_combination_id(+)
           AND ael.ae_header_id = adl.ae_header_id(+)
           AND ael.ae_line_num = adl.ae_line_num(+)
           AND adl.SOURCE_DISTRIBUTION_TYPE = 'AP_PMT_DIST'
           AND adl.SOURCE_DISTRIBUTION_ID_NUM_1 = APHD.PAYMENT_HIST_DIST_ID
           AND APHD.INVOICE_PAYMENT_ID = AIP.INVOICE_PAYMENT_ID
           AND AIP.CHECK_ID = AC.CHECK_ID(+)
           AND AID.INVOICE_ID = AI.INVOICE_ID
           AND APHD.INVOICE_DISTRIBUTION_ID = AID.INVOICE_DISTRIBUTION_ID
           AND POV.VENDOR_ID = AI.VENDOR_ID
           AND jh.je_source = 'Payables'
           AND le.object_type_code = 'L'
           AND gps.application_id = 101
           AND gps.period_name = jh.period_name
           AND ds.doc_sequence_id(+) = gir.subledger_doc_sequence_id
           AND gps.ledger_id = le.ledger_id
           AND jh.je_batch_id = gb.je_batch_id
    UNION ALL
    SELECT jh.je_source SOURCE,
           jh.je_category,
           gir.je_header_id je_header_id,
           gir.je_line_num je_line_num,
           jl.effective_date acc_date,
           jh.NAME entry,
           jh.je_batch_id batch,
           SUBSTR (jl.description, 1, 240) line_descr,
           ds.name LSequence,
           jh.doc_sequence_value hnumber,
           jl.je_line_num lline,
           jl.accounted_dr line_acctd_dr,
           jl.accounted_cr line_acctd_cr,
           jl.entered_dr ent_dr,
           jl.entered_cr ent_cr,
           gir.subledger_doc_sequence_id seq_id,
           gir.subledger_doc_sequence_value seq_num,
           jh.doc_sequence_id h_seq_id,
           gir.gl_sl_link_id gl_sl_link_id,
           pov.vendor_name customer_or_vendor,
           jl.entered_dr ENTERED_DR,
           jl.entered_cr ENTERED_CR,
           jl.accounted_cr ACCOUNTED_DR,
           jl.accounted_dr ACCOUNTED_CR,
           ael.unrounded_accounted_dr sla_line_accounted_dr,
           ael.unrounded_accounted_cr sla_line_accounted_cr,
             NVL (ael.unrounded_accounted_dr, 0)
           - NVL (ael.unrounded_accounted_cr, 0)
              sla_line_accounted_net,
           ael.unrounded_entered_dr sla_line_entered_dr,
           ael.unrounded_entered_cr sla_line_entered_cr,
             NVL (ael.unrounded_entered_dr, 0)
           - NVL (ael.unrounded_entered_cr, 0)
              sla_line_entered_net,
           adl.unrounded_entered_cr sla_dist_entered_cr,
           adl.unrounded_entered_dr sla_dist_entered_dr,
             NVL (adl.unrounded_entered_cr, 0)
           - NVL (adl.unrounded_entered_dr, 0)
              sla_dist_entered_net,
           adl.unrounded_accounted_cr sla_dist_accounted_cr,
           adl.unrounded_accounted_dr sla_dist_accounted_dr,
             NVL (adl.unrounded_accounted_cr, 0)
           - NVL (adl.unrounded_accounted_dr, 0)
              sla_dist_accounted_net,
           '' associate_num,
           AI.INVOICE_NUM transaction_mum,
           gcc.CONCATENATED_SEGMENTS gl_account_string,
           jh.currency_code,
           jh.period_name,
           ael.accounting_class_code TYPE,
           gps.effective_period_num,
           le.name,
           gb.name batch_name,
           gcc.code_combination_id                     --descr#flexfield#start
                                  ,
           xxeis.eis_rs_dff.decode_valueset ('XXCUS_FRU',
                                             GCC.ATTRIBUTE1,
                                             'I')
              GCC#Branch                                 --descr#flexfield#end
                        --gl#accountff#start
           ,
           GCC.SEGMENT4 GCC#50328#ACCOUNT,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT4,
                                                 'XXCUS_GL_ACCOUNT')
              GCC#50328#ACCOUNT#DESCR,
           GCC.SEGMENT3 GCC#50328#COST_CENTER,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT3,
                                                 'XXCUS_GL_COSTCENTER')
              GCC#50328#COST_CENTER#DESCR,
           GCC.SEGMENT6 GCC#50328#FURTURE_USE,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT6,
                                                 'XXCUS_GL_FUTURE_USE1')
              GCC#50328#FURTURE_USE#DESCR,
           GCC.SEGMENT7 GCC#50328#FUTURE_USE_2,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT7,
                                                 'XXCUS_GL_FUTURE_USE_2')
              GCC#50328#FUTURE_USE_2#DESCR,
           GCC.SEGMENT2 GCC#50328#LOCATION,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT2,
                                                 'XXCUS_GL_LOCATION')
              GCC#50328#LOCATION#DESCR,
           GCC.SEGMENT1 GCC#50328#PRODUCT,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT1,
                                                 'XXCUS_GL_PRODUCT')
              GCC#50328#PRODUCT#DESCR,
           GCC.SEGMENT5 GCC#50328#PROJECT_CODE,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT5,
                                                 'XXCUS_GL_PROJECT')
              GCC#50328#PROJECT_CODE#DESCR,
           GCC.SEGMENT4 GCC#50368#ACCOUNT,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT4,
                                                 'XXCUS_GL_ LTMR _ACCOUNT')
              GCC#50368#ACCOUNT#DESCR,
           GCC.SEGMENT3 GCC#50368#DEPARTMENT,
           xxeis.eis_rs_fin_utility.decode_vset (
              GCC.SEGMENT3,
              'XXCUS_GL_ LTMR _DEPARTMENT')
              GCC#50368#DEPARTMENT#DESCR,
           GCC.SEGMENT2 GCC#50368#DIVISION,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT2,
                                                 'XXCUS_GL_ LTMR _DIVISION')
              GCC#50368#DIVISION#DESCR,
           GCC.SEGMENT6 GCC#50368#FUTURE_USE,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT6,
                                                 'XXCUS_GL_ LTMR _FUTUREUSE')
              GCC#50368#FUTURE_USE#DESCR,
           GCC.SEGMENT1 GCC#50368#PRODUCT,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT1,
                                                 'XXCUS_GL_LTMR_PRODUCT')
              GCC#50368#PRODUCT#DESCR,
           GCC.SEGMENT5 GCC#50368#SUBACCOUNT,
           xxeis.eis_rs_fin_utility.decode_vset (
              GCC.SEGMENT5,
              'XXCUS_GL_ LTMR _SUBACCOUNT')
              GCC#50368#SUBACCOUNT#DESCR
      --gl#accountff#end


      FROM gl_ledgers le,
           gl_je_headers jh,
           gl_je_lines jl,
           gl_import_references gir,
           gl_code_combinations_kfv gcc,
           xla_ae_lines ael,
           gl_code_combinations_kfv gcc1,
           xla_ae_headers aeh,
           xla_distribution_links adl,
           ap_prepay_app_dists apad,
           AP_INVOICES_ALL AI,
           AP_INVOICE_DISTRIBUTIONS_ALL AID,
           po_vendors pov,
           gl_period_statuses gps,
           FND_DOCUMENT_SEQUENCES ds,
           gl_je_batches gb
     WHERE     le.ledger_id = jh.ledger_id
           AND jh.je_header_id = jl.je_header_id
           AND gir.je_header_id = jh.je_header_id
           AND gir.je_line_num = jl.je_line_num
           AND jl.code_combination_id = gcc.code_combination_id
           AND gir.gl_sl_link_id = ael.gl_sl_link_id(+)
           AND gir.gl_sl_link_table = ael.gl_sl_link_table(+)
           AND ael.ae_header_id = aeh.ae_header_id(+)
           AND ael.code_combination_id = gcc1.code_combination_id(+)
           AND ael.ae_header_id = adl.ae_header_id(+)
           AND ael.ae_line_num = adl.ae_line_num(+)
           AND adl.SOURCE_DISTRIBUTION_TYPE = 'AP_PREPAY'
           AND adl.SOURCE_DISTRIBUTION_ID_NUM_1 = apad.prepay_app_dist_id
           AND apad.invoice_distribution_id = aid.invoice_distribution_id
           AND AID.INVOICE_ID = AI.INVOICE_ID
           AND POV.VENDOR_ID = AI.VENDOR_ID
           AND jh.je_source = 'Payables'
           AND le.object_type_code = 'L'
           AND gps.application_id = 101
           AND gps.period_name = jh.period_name
           AND ds.doc_sequence_id(+) = gir.subledger_doc_sequence_id
           AND gps.ledger_id = le.ledger_id
           AND JH.JE_BATCH_ID = GB.JE_BATCH_ID);
