/*
 -- Author: Balaguru Seshadri
 -- Parameters: None
 -- Modification History
 -- ESMS        TMS              Date          Version  Comments
 -- =========== ===============  ===========   =======  ========================
 --             20180507-00231   07-MAY-2018   1.0      Created.
 */ 
SET SERVEROUTPUT ON SIZE 1000000
declare
  --
  b_proceed BOOLEAN;
  --
 procedure print_log(p_message in varchar2) is
 begin
     if  fnd_global.conc_request_id >0 then
      fnd_file.put_line(fnd_file.log, p_message);
     else
      dbms_output.put_line(p_message);
     end if;
 end print_log;
 --
 --
 procedure print_output(p_message in varchar2) is
 begin
     if  fnd_global.conc_request_id >0 then
      fnd_file.put_line(fnd_file.output, p_message);
     else
      dbms_output.put_line(p_message);
     end if;
 end print_output;
 --         
begin
  --
  print_log('Start: Main');
  --
  begin 
    --
	execute immediate 'create table XXCUS.XXCUS_TMS_20180507_00231_TBL_1 as select * from ap.ap_exp_report_dists_all where 1 =1
                       and report_header_id =1101715 and report_line_id =9911714';
	--
    print_log('Back up table XXCUS.XXCUS_TMS_20180507_00231_TBL_1 created to hold the ap expense distribution data for report_header_id =1101715 and report_line_id 9911714');
    --
    b_proceed :=TRUE;
	--
  exception
   when others then
    print_log('@Failed to create backup table XXCUS.XXCUS_TMS_20180507_00231_TBL_1, error msg ='||sqlerrm);
	--
    b_proceed :=FALSE;
	--
  end;   
  -- 
  if (b_proceed) then
      --
	  begin 
		 --
		savepoint square1;
		--
		delete ap.ap_exp_report_dists_all
		where 1 =1
		and report_header_id =1101715 
        and report_line_id =9911714
        ;
		--
		print_log('Deleted records from table ap_exp_report_dists_all for report_header_id =1101715 and report_line_id 9911714, row count ='||sql%rowcount);
		--
		commit;
		-- 
	  exception
	   when others then
		print_log('@Failed to delete distribution records from ap_exp_report_dists_all for report_header_id =1101715 and report_line_id 9911714, msg ='||sqlerrm);
		rollback to square1;
	  end;   
	  --   
  end if;  
  --
  begin 
    --
	execute immediate 'create table XXCUS.XXCUS_TMS_20180507_00231_TBL_2 as select * from ap.ap_expense_report_lines_all where report_header_id =1101715 and report_line_id =9911714';
    --
    print_log('Back up table XXCUS.XXCUS_TMS_20180507_00231_TBL_2 created to hold the ap expense lines data for report_header_id =1101715 and report_line_id =9911714');
    --    
    b_proceed :=TRUE;
	--
  exception
   when others then
    print_log('@Failed to create backup table XXCUS.XXCUS_TMS_20180507_00231_TBL_2, error msg ='||sqlerrm);
	--
    b_proceed :=FALSE;
	--
  end;   
  -- 
    -- 
  if (b_proceed) then
      --
	  begin 
		 --
		savepoint square1;
		--
		delete ap.ap_expense_report_lines_all 
		where 1 =1
              and report_header_id =1101715
		      and report_line_id = 9911714  
		;
		--
		print_log('Deleted records from table ap.ap_expense_report_lines_all with report_header_id =1101715 report_line_id = 9911714  , row count ='||sql%rowcount);
		--
		commit;
		--      
	  exception
	   when others then
		print_log('@Failed to delete records from table ap.ap_expense_report_lines_all with report_header_id =1101715 report_line_id = 9911714, msg ='||sqlerrm);
		rollback to square1;
	  end;   
	  --   
  end if;  
  --
 print_log('End: Main');
 -- 
exception
 when others then
  print_log('Outer block of SQL script, message ='||sqlerrm);
end;
/