SET SERVEROUTPUT ON

-- 3. Run below script

DECLARE
   v_count   NUMBER := 0;
BEGIN
   DBMS_OUTPUT.put_line ('Starting update...');

   FOR c1
      IN (SELECT ohi.ROWID, ohi.*
            FROM oe_headers_iface_all ohi
           WHERE     1 = 1
                 AND ROWID NOT IN (SELECT MIN (ROWID)
                                     FROM oe_headers_iface_all ohi2
                                    WHERE     1 = 1
                                          AND ohi.ORDER_SOURCE_ID =
                                                 ohi2.ORDER_SOURCE_ID
                                          AND ohi.ORIG_SYS_DOCUMENT_REF =
                                                 ohi2.ORIG_SYS_DOCUMENT_REF
                                          AND ohi.ORG_ID = ohi2.ORG_ID
                                          AND ohi.ORDERED_DATE =
                                                 ohi2.ORDERED_DATE
                                          AND ohi.SOLD_TO_ORG_ID =
                                                 ohi2.SOLD_TO_ORG_ID
                                          AND ohi.SHIP_FROM_ORG_ID =
                                                 ohi2.SHIP_FROM_ORG_ID
                                          AND ohi.SHIP_TO_ORG_ID =
                                                 ohi2.SHIP_TO_ORG_ID
                                          AND ohi.SHIPPING_METHOD_CODE =
                                                 ohi2.SHIPPING_METHOD_CODE
                                          AND ohi.order_type_id =
                                                 ohi2.order_type_id
                                          AND ohi.ATTRIBUTE7 =
                                                 ohi2.ATTRIBUTE7
                                          AND ohi.CREATED_BY =
                                                 ohi2.CREATED_BY
                                          AND ohi.REQUEST_DATE =
                                                 ohi2.REQUEST_DATE
                                          AND ohi.ERROR_FLAG =
                                                 ohi2.ERROR_FLAG)
                 AND TRUNC (ohi.creation_date) >= TO_DATE('17-AUG-2015','DD-MON-YYYY')
                 )
   LOOP
      BEGIN
         v_count := v_count + 1;

         UPDATE oe_headers_iface_all
            SET error_flag = NULL,
                request_id=NULL
          WHERE orig_sys_document_ref = c1.orig_sys_document_ref;

         COMMIT;

         DELETE FROM oe_headers_iface_all
               WHERE ROWID = c1.ROWID;
      EXCEPTION
         WHEN OTHERS
         THEN
            DBMS_OUTPUT.put_line (
               'Could not delete record: ' || c1.ORIG_SYS_DOCUMENT_REF);
      END;
   END LOOP;

   COMMIT;

   DBMS_OUTPUT.put_line ('Number of header records deleted are-' || v_count);
   
   v_count:=0;

   FOR c2
      IN (SELECT oli.ROWID, oli.*
            FROM oe_lines_iface_all oli
           WHERE     1 = 1
                 AND ROWID NOT IN (SELECT MIN (ROWID)
                                     FROM oe_lines_iface_all oli2
                                    WHERE     1 = 1
                                          AND oli.ORDER_SOURCE_ID =
                                                 oli2.ORDER_SOURCE_ID
                                          AND oli.ORIG_SYS_DOCUMENT_REF =
                                                 oli2.ORIG_SYS_DOCUMENT_REF
                                          AND oli.ORIG_SYS_LINE_REF =
                                                 oli2.ORIG_SYS_LINE_REF
                                          AND oli.ORG_ID = oli2.ORG_ID
                                          AND oli.line_number =
                                                 oli2.line_number
                                          AND oli.ITEM_TYPE_CODE =
                                                 oli2.ITEM_TYPE_CODE
                                          AND oli.INVENTORY_ITEM_ID =
                                                 oli2.INVENTORY_ITEM_ID
                                          AND oli.ORDERED_QUANTITY =
                                                 oli2.ORDERED_QUANTITY
                                          AND oli.SOLD_TO_ORG_ID =
                                                 oli2.SOLD_TO_ORG_ID
                                          AND oli.SHIP_TO_ORG_ID =
                                                 oli2.SHIP_TO_ORG_ID
                                          AND oli.SHIP_FROM_ORG_ID =
                                                 oli2.SHIP_FROM_ORG_ID)
                 AND TRUNC (oli.creation_date) >= TO_DATE('17-AUG-2015','DD-MON-YYYY')
                 )
   LOOP
      BEGIN
         v_count := v_count + 1;
         
          UPDATE oe_lines_iface_all
            SET error_flag = NULL,
                request_id=NULL
          WHERE orig_sys_document_ref = c2.orig_sys_document_ref;
          
          COMMIT;

         DELETE FROM oe_lines_iface_all
               WHERE ROWID = c2.ROWID;
      EXCEPTION
         WHEN OTHERS
         THEN
            DBMS_OUTPUT.put_line (
                  'Could not delete record: '
               || c2.ORIG_SYS_LINE_REF
               || ',oli.line_number'
               || c2.line_number);
      END;
   END LOOP;

   COMMIT;

   DBMS_OUTPUT.put_line ('Number of line records deleted are-' || v_count);
   
   v_count:=0;

   FOR c3
      IN (SELECT oai.ROWID, oai.*
            FROM OE_ACTIONS_IFACE_ALL oai
           WHERE     1 = 1
                 AND ROWID NOT IN (SELECT MIN (ROWID)
                                     FROM OE_ACTIONS_IFACE_ALL oai2
                                    WHERE     1 = 1
                                          AND oai.ORDER_SOURCE_ID =
                                                 oai2.ORDER_SOURCE_ID
                                          AND oai.ORIG_SYS_DOCUMENT_REF =
                                                 oai2.ORIG_SYS_DOCUMENT_REF
                                          AND oai.ORG_ID = oai2.ORG_ID
                                          AND oai.OPERATION_CODE =
                                                 oai2.OPERATION_CODE
                                          AND oai.SOLD_TO_ORG_ID =
                                                 oai2.SOLD_TO_ORG_ID)
                 )
   LOOP
      BEGIN
         v_count := v_count + 1;
         
         UPDATE oe_actions_iface_all
            SET error_flag = NULL,
                request_id=NULL
          WHERE orig_sys_document_ref = c3.orig_sys_document_ref;
          
          COMMIT;

         DELETE FROM oe_actions_iface_all
               WHERE ROWID = c3.ROWID;
      EXCEPTION
         WHEN OTHERS
         THEN
            DBMS_OUTPUT.put_line (
               'Could not delete record: ' || c3.ORIG_SYS_DOCUMENT_REF);
      END;
   END LOOP;

   COMMIT;

   DBMS_OUTPUT.put_line ('Number of action records deleted are-' || v_count);
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Error in main: ' || SQLERRM);
      ROLLBACK;
      RAISE;
END;