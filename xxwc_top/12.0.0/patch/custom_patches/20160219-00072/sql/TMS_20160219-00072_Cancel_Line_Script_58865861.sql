REM  $Header: ontd0008.sql 115.0.1159.8 2009/11/25 08:55:27 sahvivek noship $
REM +=======================================================================+
REM |    Copyright (c) 2003 Oracle Corporation, Redwood Shores, CA, USA     |
REM |                         All rights reserved.                          |
REM +=======================================================================+
REM | FILENAME                                                              |
REM |     ontd0008.sql                                                      |
REM |                                                                       |
REM | DESCRIPTION                                                           |
REM |     This script has been created to allow cancelling those lines      |
REM |     which are stuck due to some data corruption  and can neither      |
REM |     be progressed nor cancelled from the application. You may only use| 
REM |     it on order lines as authorized by Oracle Support.                |
REM |     This script will do the following:                                |
REM |                                                                       |
REM |     1. Update Lines to show as if Cancelled.                          |
REM |     2. Cancel Delivery Details(if any) associated to order lines.     |
REM |     3. Marks the Move Order Lines as 'Cancelled by Source' for any    |
REM |        'Released to Warehouse' details (for INV / OPM Orgs)           |
REM |     4. Deletes any Serial Number(s) associated with Staged/Shipped    |
REM |        Delivery Details and Unmarks them                              |
REM |     5. Cancel OPM Inventory data in case of OPM Org                   |
REM |     6. Progress flow(if exists) to CLOSE.                             |
REM |     7. Removes the line from any fulfillment set.                     |
REM |                                                                       |
REM |     Note that this script DOES NOT:                                   |
REM |                                                                       |
REM |     1. Cancel Line if it belongs to (a)WMS Organization; and          |
REM |        (b)has open delivery details                                   |
REM |     2. Cancel Line if OTM Integration is enabled for Organization     |
REM |     3. Delink the Configured Items  that  may exist on  an order.     |
REM |     4. Update the  history tables with  cancellation information.     |
REM |     5. Unschedule the Lines. Inventory  patch # 2471362 should be     |
REM |        applied  after running this  script to relieve the demand.     |
REM |     6. Update Move Order Lines, log a clean bug against inventory     |
REM |     7. Update Supply.                                                 |
REM |                                                                       |
REM | DISCLAIMER                                                            |
REM |     Do not use  this script as a replacement for OM Cancellation      |
REM |     functionality as it is  strictly meant for those stuck  order     |
REM |     lines which can neither be progressed nor cancelled from the      |
REM |     application.                                                      |
REM |                                                                       |
REM |     Use this script at your own risk. The script has been tested      |
REM |     and appears to works as intended. However, you should always      |
REM |     test any script before relying on it.                             |
REM |                                                                       |
REM |     Proofread the script prior to running it. Due to differences      |
REM |     in the way text editors,email packages and operating systems      |
REM |     handle text  formatting (spaces, tabs and carriage returns),      |
REM |     this script may not be in an executable state when you first      |
REM |     receive it.  Check over the script to  ensure that errors of      |
REM |     this type are corrected.                                          |
REM |                                                                       |
REM |     Do not remove disclaimer paragraph.                               |
REM |                                                                       |
REM | INPUT/OUTPUT                                                          |
REM |     Inputs : Line Id(Required)                                        |
REM |                                                                       |
REM |     Output : Report is printed to an O/S file named line_id.lst       |
REM |                                                                       |
REM | NOTE                                                                  |
REM |     This script should be tested in TEST Instance first.              |
REM |     If results are  satisfactory in TEST, then only use in            | 
REM |     PRODUCTION Instance.                                              |
REM |                                                                       |
REM | HISTORY                                                               |
REM |     14-MAR-2002              Tarun Bharti                 Created     |
REM +=======================================================================+

REM dbdrv:none
SET VERIFY OFF;
WHENEVER OSERROR EXIT FAILURE ROLLBACK;
WHENEVER SQLERROR EXIT FAILURE ROLLBACK;

clear buffer;
set serveroutput on size 500000
rem set feed off
set pagesize 1000
set linesize 120
set underline '='

Prompt !!! MAKE SURE YOU READ, UNDERSTAND AND FOLLOW THE DISCLAIMER PARAGRAPH INSIDE THIS SCRIPT !!!


declare

  l_lin_id        number := 58865861;
  
  l_lin_key       varchar2(30) := to_char(l_lin_id);

  l_ordered_qty   number;
  l_flow_exists   varchar2(1);
  l_user_id       number;
  l_resp_id       number;
  l_resp_appl_id  number;

  cursor line_check is
  select line_id 
  from   oe_order_lines_all  
  where  line_id = l_lin_id
  and     ( open_flag = 'N' 
            or nvl(invoiced_quantity,0) > 0
            or INVOICE_INTERFACE_STATUS_CODE = 'YES' ) ;
  cursor service_check is
  select count(*)
  from   oe_order_lines_all  
  where  service_reference_type_code = 'ORDER'
  and    service_reference_line_id = l_lin_id
  and    open_flag = 'Y'; 

  -- Cursors to fetch details and their Serial Number(s) 
  cursor wdd is
  select delivery_detail_id, transaction_temp_id, serial_number, inventory_item_id, to_serial_number
  from   wsh_delivery_details
  where  source_line_id = l_lin_id
  and    source_code = 'OE'
  and    released_status in ('Y','C');

  cursor msnt(txn_temp_id number) is
  select fm_serial_number, to_serial_number
  from   mtl_serial_numbers_temp
  where  transaction_temp_id = txn_temp_id;

  l_line_check        NUMBER;
  l_oe_interfaced     NUMBER;
  l_wms_org           VARCHAR2(1);
  l_otm_installed     VARCHAR2(1);
  l_otm_enabled       VARCHAR2(1);
  l_cursor            INTEGER;                
  l_stmt              VARCHAR2(4000);
  l_up_cursor         INTEGER;                
  l_up_stmt           VARCHAR2(4000);
  l_ignore            NUMBER;
  l_ship_from_org_id  NUMBER;
  l_opm_enabled       BOOLEAN;
  l_fm_serial         VARCHAR2(30);
  l_to_serial         VARCHAR2(30);

  Line_Cannot_Be_Updated EXCEPTION;

  l_heading       varchar2(1) := 'N';

 cursor wsh_ifaced
  is
  select
    substr(wdd.source_line_number, 1, 15) line_num
  , substr(wdd.item_description, 1, 30) item_name
  , wdd.shipped_quantity
  , wdd.source_line_id line_id
  from  wsh_delivery_details wdd, oe_order_lines_all oel
  where wdd.inv_interfaced_flag     = 'Y'
  and   nvl(wdd.shipped_quantity,0) > 0
  and   oel.line_id                 = wdd.source_line_id
  and   oel.open_flag               = 'N'
  and   oel.ordered_quantity        = 0
  and   wdd.source_code             = 'OE'
  and   oel.line_id                 = l_lin_id
  and   exists
      ( select 'x'
        from  mtl_material_transactions mmt
        where wdd.delivery_detail_id   = mmt.picking_line_id
        and   mmt.trx_source_line_id   = wdd.source_line_id
        and   mmt.transaction_source_type_id in ( 2,8 ));


begin

  dbms_output.put_line('Updating Line ID: '||l_lin_id);

  -- Check if line can be canceled 
  open line_check ;
  fetch line_check into l_line_check ;
  if line_check%found then 
     close line_check;
     dbms_output.put_line('Line is closed or Invoiced');
     raise Line_Cannot_Be_Updated;
  end if;
  close line_check;
  open service_check ;
  fetch service_check into l_line_check ;
  if l_line_check > 0 then
     close service_check;
     dbms_output.put_line('There exist open service lines referencing this order line.');
     raise Line_Cannot_Be_Updated;
  end if;
  close service_check;
 
  -- Check if line belongs to WMS Org 
  begin
    select WSH_UTIL_VALIDATE.CHECK_WMS_ORG(ship_from_org_id), ship_from_org_id
    into   l_wms_org, l_ship_from_org_id
    from   oe_order_lines_all
    where  line_id = l_lin_id;
  exception 
    when no_data_found then
         dbms_output.put_line('Unable to get the Organization');
         raise Line_Cannot_Be_Updated;
  end;
  if l_wms_org = 'Y' then
     -- Disallow cancellation if and only if there exist open delivery detail(s) for line 
     -- under consideration. (Bug 6196723)
     DECLARE 
       l_exist_count number := -1;
     BEGIN 
        SELECT  Count(*) 
            INTO l_exist_count
          FROM    wsh_delivery_details wdd, oe_order_lines_all line
          WHERE   line.line_id     = wdd.source_line_id
          AND     wdd.source_code  =  'OE'       
          AND     line.line_id     = l_lin_id
          AND     Nvl(wdd.released_status, 'N') <> 'D'
        ;
        IF ( l_exist_count > 0 ) THEN 
          dbms_output.put_line('This line belongs to a WMS Organization');
          raise Line_Cannot_Be_Updated;
        END IF;
     END;
  end if;

  -- Check if OTM is installed or OTM is enabled for the Organization
  --{
  l_otm_installed := NVL(FND_PROFILE.VALUE('WSH_OTM_INSTALLED'), 'N');
  begin
    l_cursor := dbms_sql.open_cursor;
    l_stmt   := 'select wsp.otm_enabled from wsh_shipping_parameters wsp, oe_order_lines_all ol '||
                'where wsp.organization_id = ol.ship_from_org_id '||
                'and   ol.line_id = :line_id ';

    dbms_sql.parse(l_cursor, l_stmt, dbms_sql.v7);
    dbms_sql.define_column(l_cursor, 1, l_otm_enabled, 1);
    dbms_sql.bind_variable(l_cursor, ':line_id', l_lin_id);
    l_ignore := dbms_sql.execute(l_cursor);
    if dbms_sql.fetch_rows(l_cursor) > 0 then
       dbms_sql.column_value(l_cursor, 1, l_otm_enabled);
       dbms_output.put_line('l_otm_enabled '||l_otm_enabled);
    end if;
    dbms_sql.close_cursor(l_cursor);
  exception
    when others then
      if dbms_sql.is_open(l_cursor) then
         dbms_sql.close_cursor(l_cursor);
      end if;
      if l_otm_installed in ('Y','O') then
         dbms_output.put_line('l_otm_installed '||l_otm_installed);
         dbms_output.put_line('OTM: Integration Enabled is enabled for Order Management');
         raise Line_Cannot_Be_Updated;
      end if;
  end;
  if NVL(l_otm_enabled, 'N') = 'Y' and l_otm_installed in ('Y','O') then
     dbms_output.put_line('l_otm_installed '||l_otm_installed||' , l_otm_enabled '||l_otm_enabled);
     dbms_output.put_line('OTM: Integration Enabled is enabled for Organization for which this Line belongs');
     raise Line_Cannot_Be_Updated;
  end if;
  --}
        
  l_flow_exists := 'Y';

  update oe_order_lines_all
  set    flow_status_code    = 'CANCELLED'
  ,      open_flag           = 'N'
  ,      cancelled_flag      = 'Y'
  ,      ordered_quantity    = 0
  ,      ordered_quantity2   = decode(ordered_quantity2,NULL,NULL,0)  -- OPM related
  ,      cancelled_quantity  = ordered_quantity + nvl(cancelled_quantity, 0)
  ,      visible_demand_flag = NULL
  ,      last_updated_by     = -6156992
  ,      last_update_date    = sysdate
  where  line_id             = l_lin_id;

  -- Added for bug 8532859
  delete from oe_line_sets
  where  line_id = l_lin_id;
  
  Begin
    select number_value
    into   l_user_id
    from   wf_item_attribute_values
    where  item_type = 'OEOL'
    and    item_key  = l_lin_key
    and    name      = 'USER_ID';

    select number_value
    into   l_resp_id
    from   wf_item_attribute_values
    where  item_type = 'OEOL'
    and    item_key  = l_lin_key
    and    name      = 'RESPONSIBILITY_ID';

    select number_value
    into   l_resp_appl_id
    from   wf_item_attribute_values
    where  item_type = 'OEOL'
    and    item_key  = l_lin_key
    and    name      = 'APPLICATION_ID';

    Exception
      When No_Data_Found Then
        l_flow_exists := 'N';
  End;

  if l_flow_exists = 'Y' then 

    fnd_global.apps_initialize(l_user_id, l_resp_id, l_resp_appl_id);

    wf_engine.handleerror( OE_Globals.G_WFI_LIN
                         , l_lin_key
                         , 'CLOSE_LINE'
                         , 'RETRY'
                         , 'CANCEL'
                         );
  end if;

 for wsh_ifaced_rec in wsh_ifaced loop
    if l_heading = 'N' then
      dbms_output.put_line(' ');
      dbms_output.put_line('Following Cancelled Lines have already been Interfaced to Inventory.');
      dbms_output.put_line('Onhand Qty must be manually adjusted for these Items and Quantities.');
      dbms_output.put_line(' ');
      dbms_output.put_line('+---------------+------------------------------+---------------+---------------+');
      dbms_output.put_line('|Line No.       |Item Name                     |    Shipped Qty|        Line Id|');
      dbms_output.put_line('+---------------+------------------------------+---------------+---------------+');
      l_heading := 'Y';
    end if;
    dbms_output.put_line('|'||rpad(wsh_ifaced_rec.line_num, 15)||
                         '|'||rpad(wsh_ifaced_rec.item_name, 30)||
                         '|'||lpad(to_char(wsh_ifaced_rec.shipped_quantity), 15)||
                           '|'||lpad(to_char(wsh_ifaced_rec.line_id), 15)||'|');
  end loop;



  update wsh_delivery_assignments 
  set    delivery_id               = null
  ,      parent_delivery_detail_id = null
  ,      last_updated_by           = -1
  ,      last_update_date          = sysdate
  where  delivery_detail_id        in 
        (select wdd.delivery_detail_id
         from   wsh_delivery_details wdd, oe_order_lines_all oel
         where  wdd.source_line_id   = oel.line_id
          and   wdd.source_code      = 'OE'
          and   oel.cancelled_flag   = 'Y'
          and   oel.line_id          = l_lin_id
          and   released_status      <> 'D');

  -- Check if Org is an OPM or Inventory Org
  l_opm_enabled := INV_GMI_RSV_BRANCH.PROCESS_BRANCH(p_organization_id => l_ship_from_org_id);

  if not l_opm_enabled then 
  --{
     -- Inventory Org
     -- Updating Move Order lines for Released To Warehouse details as 'Cancelled by Source'
     update mtl_txn_request_lines
     set    line_status = 9
     where  line_id in ( select move_order_line_id
                         from   wsh_delivery_details 
                         where  source_line_id = l_lin_id
                         and    released_status = 'S'
                         and    source_code = 'OE' );

     -- Removing Serial Number(s) and Unmarking them
     for rec in wdd loop
     --{
         if rec.serial_number is not null then
            update mtl_serial_numbers
            set    group_mark_id = null,
                   line_mark_id = null,
                   lot_line_mark_id = null
            where  inventory_item_id = rec.inventory_item_id
            and    serial_number  between rec.serial_number and NVL(rec.to_serial_number, rec.serial_number);
         elsif rec.transaction_temp_id is not null then
         --{
            for msnt_rec in msnt(rec.transaction_temp_id) loop
                update mtl_serial_numbers
                set    group_mark_id = null,
                       line_mark_id = null,
                       lot_line_mark_id = null
                where  inventory_item_id = rec.inventory_item_id
                and    serial_number  between msnt_rec.fm_serial_number and NVL(msnt_rec.to_serial_number, msnt_rec.fm_serial_number);
            end loop;
            delete from mtl_serial_numbers_temp
            where  transaction_temp_id = rec.transaction_temp_id;
            begin
            --{
              l_cursor := dbms_sql.open_cursor;
              l_stmt   := 'select fm_serial_number, to_serial_number '||
                          'from   wsh_serial_numbers '||
                          'where  delivery_detail_id = :delivery_detail_id ';
              dbms_sql.parse(l_cursor, l_stmt, dbms_sql.v7);
              dbms_sql.define_column(l_cursor, 1, l_fm_serial, 1);
              dbms_sql.define_column(l_cursor, 2, l_to_serial, 1);
              dbms_sql.bind_variable(l_cursor, ':delivery_detail_id', rec.delivery_detail_id);
              l_ignore := dbms_sql.execute(l_cursor);
              loop
                if dbms_sql.fetch_rows(l_cursor) > 0 then
                   dbms_sql.column_value(l_cursor, 1, l_fm_serial);
                   dbms_sql.column_value(l_cursor, 2, l_to_serial);
                   l_up_cursor := dbms_sql.open_cursor;
                   l_up_stmt   := 'update mtl_serial_numbers msn '||
                                  'set    msn.group_mark_id    = null,  '||
                                  '       msn.line_mark_id     = null,  '||
                                  '       msn.lot_line_mark_id = null   '||
                                  'where  msn.inventory_item_id = :inventory_item_id '||
                                  'and    msn.serial_number between :fm_serial and :to_serial ';
                   dbms_sql.parse(l_up_cursor, l_up_stmt, dbms_sql.v7);
                   dbms_sql.bind_variable(l_up_cursor, ':inventory_item_id', rec.inventory_item_id);
                   dbms_sql.bind_variable(l_up_cursor, ':fm_serial', l_fm_serial);
                   dbms_sql.bind_variable(l_up_cursor, ':to_serial', NVL(l_to_serial, l_fm_serial));
                   l_ignore := dbms_sql.execute(l_up_cursor);
                   dbms_sql.close_cursor(l_up_cursor);
                else
                  exit;
                end if;
              end loop;
              dbms_sql.close_cursor(l_cursor);
              l_cursor := dbms_sql.open_cursor;
              l_stmt   := 'delete from wsh_serial_numbers '||
                          'where delivery_detail_id = :delivery_detail_id ';
              dbms_sql.parse(l_cursor, l_stmt, dbms_sql.v7);
              dbms_sql.bind_variable(l_cursor, ':delivery_detail_id', rec.delivery_detail_id);
              l_ignore := dbms_sql.execute(l_cursor);
              dbms_sql.close_cursor(l_cursor);
            exception
              when others then
                if dbms_sql.is_open(l_up_cursor) then
                   dbms_sql.close_cursor(l_up_cursor);
                end if;
                if dbms_sql.is_open(l_cursor) then
                   dbms_sql.close_cursor(l_cursor);
                end if;
            --}
            end;
         --}
         end if;
     --}
     end loop;
  --}
  else
     --{
     -- OPM Org 
     update ic_txn_request_lines 
     set    line_status = 9
     where  line_id in ( select move_order_line_id
                         from   wsh_delivery_details 
                         where  source_line_id  = l_lin_id
                         and    released_status = 'S'
                         and    source_code     = 'OE' );
                    
     update ic_tran_pnd
     set    delete_mark   = 1
     where  line_id       = l_lin_id
     and    doc_type      = 'OMSO'
     and    trans_qty     < 0
     and    delete_mark   = 0
     and    completed_ind = 0;
     --}
  end if;

  update wsh_delivery_details
  set    released_status         = 'D'
  ,      src_requested_quantity  = 0
  ,      src_requested_quantity2 = decode(src_requested_quantity2,NULL,NULL,0)
  ,      requested_quantity      = 0
  ,      requested_quantity2     = decode(requested_quantity2,NULL,NULL,0) 
  ,      shipped_quantity        = 0
  ,      shipped_quantity2       = decode(shipped_quantity2,NULL,NULL,0) 
  ,      picked_quantity         = 0
  ,      picked_quantity2        = decode(picked_quantity2,NULL,NULL,0) 
  ,      cycle_count_quantity    = 0
  ,      cycle_count_quantity2   = decode(src_requested_quantity2,NULL,NULL,0) 
  ,      cancelled_quantity      = decode(requested_quantity,0,cancelled_quantity,requested_quantity)
  ,      cancelled_quantity2     = decode(requested_quantity2,NULL,NULL,0,cancelled_quantity2,requested_quantity2) 
  ,      subinventory            = null
  ,      locator_id              = null
  ,      lot_number              = null
  ,      serial_number           = null
  ,      to_serial_number        = null
  ,      transaction_temp_id     = null
  ,      revision                = null
  ,      ship_set_id             = null
  ,      inv_interfaced_flag     = 'X'
  ,      oe_interfaced_flag      = 'X'
  ,      last_updated_by         = -1
  ,      last_update_date        = sysdate
  where source_line_id   = l_lin_id
  and   source_code      = 'OE'
  and   released_status  <> 'D'
  and   exists  
       (select 'x'
        from   oe_order_lines_all oel
        where  source_line_id       = oel.line_id
        and    oel.cancelled_flag   = 'Y');

  Exception
    when Line_Cannot_Be_Updated then
      rollback;
      dbms_output.put_line('This script cannot cancel this Order Line, please contact Oracle Support');
    when others then
      rollback;
      dbms_output.put_line(substr(sqlerrm, 1, 240));
end;
/

Prompt
Prompt You must enter Commit to Save the Changes and Rollback to Revert.
Prompt
Prompt If a Commit is issued, please apply Inventory patch 2471362 to clean up any reservations
Prompt


commit;
/
