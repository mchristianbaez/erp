CREATE OR REPLACE PACKAGE APPS.XXWC_SMARTSHEET_EXTRACT_PKG 
AS
/*************************************************************************
     $Header XXWC_SMARTSHEET_EXTRACT_PKG  $
     Module Name: XXWC_SMARTSHEET_EXTRACT_PKG.pkb

     PURPOSE:   Smartsheet Extract    
                       
     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        24/07/2018  Niraj K Ranjan          TMS#20180725-00025   SmartSheets Extract              
**************************************************************************/

   /*************************************************************************
      Function Name: get_smartsheet_info

      PURPOSE:   This function will be called by mobile app to get smartsheet information

      REVISIONS:
      Ver        Date          Author             Description
      ---------  ------------  ------------       ------------------
      1.0        07/26/2017    Niraj K Ranjan     TMS#20180725-00025   SmartSheets Extract    
      2.0        08/03/2018    Rakesh Patel       TMS#20180803-00095 Add p_strat_date parameter to SmartSheets Extract 
   ****************************************************************************/
    FUNCTION get_smartsheet_info (p_start_date  IN DATE)
	 return sys_refcursor;
	 
END XXWC_SMARTSHEET_EXTRACT_PKG;
/