/*************************************************************************
  $Header TMS_20161123-00201_CANCEL_DROPSHIP_ORDER_LINE.sql $
  Module Name: 20161123-00201  Data Fix script for 20161123-00201

  PURPOSE: Data fix script for 20161123-00201--No permanent fix in process (But patch available per oracle)

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        21-DEC-2016  Ashwin Sridhar        TMS#20161123-00201

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000;
DECLARE
BEGIN
   
  DBMS_OUTPUT.put_line ('TMS: 20160825-00139    , Before Update');

  UPDATE apps.oe_order_lines_all
  SET    FLOW_STATUS_CODE='CANCELLED'
  ,      CANCELLED_FLAG='Y'
  WHERE  line_id IN (83114989, 83115004) 
  AND    header_id= 50906751;

   DBMS_OUTPUT.put_line (
         'TMS: 20161123-00201  Sales order lines updated (Expected:2): '
      || SQL%ROWCOUNT);

  COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20161123-00201    , End Update');
   
EXCEPTION
WHEN others THEN
     
  ROLLBACK;
     
  DBMS_OUTPUT.put_line ('TMS: 20161123-00201 , Errors : ' || SQLERRM);

END;
/