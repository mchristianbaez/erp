/*
 TMS:  20180301-00296
 Date: 03/01/2018
 Notes:  Prod Issue: Rebates Checks were not processed
*/
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
 --
 n_loc number :=0;
 l_sql varchar2(2000) :=Null;
 l_count number :=0;
 --
BEGIN --Main Processing...
   --
   n_loc :=101;
   --
  delete xxcus.xxcusozf_rbt_payments_tbl
  where 1 = 1
    and payment_method = 'REBATE_CHECK' 
   and status = 'Y'
   and to_char(creation_date,'MM/DD/YYYY HH24:MI:SS') = '02/27/2018 16:41:10'
   ;
   --
   n_loc :=102;
   --   
   dbms_output.put_line('Rows deleted :'||sql%rowcount);
   --
   update xxcus.xxcusozf_rbt_payments_tbl set status ='N'
   where 1 =1
	 and payment_number IN
	(
	'38426',
	'11767',
	'38425',
	'221981',
	'222010'
	)
   ;
   --
   n_loc :=103;
   --   
   dbms_output.put_line('Rows updated to status of N :'||sql%rowcount);
   --
   commit;
   --
EXCEPTION
   WHEN OTHERS THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20180227-00061, Errors =' || SQLERRM);
END;
/