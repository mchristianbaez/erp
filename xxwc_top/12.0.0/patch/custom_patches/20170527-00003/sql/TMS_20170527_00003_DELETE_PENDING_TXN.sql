/****************************************************************************************************
  Description: Deleting invalid record from MMTT. 

  HISTORY
  =====================================================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- ----------------------------------------------------------
  1.0     27-May-2017        Ram Talluri    TMS#20170527-00003
  *****************************************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000

BEGIN
   DBMS_OUTPUT.put_line ('Before Delete');

  DELETE FROM  mtl_material_transactions_temp
  WHERE transaction_header_id=578690735
    AND source_line_id=SOURCE_LINE_ID
    AND TRANSACTION_TEMP_ID=578690735
    AND source_code='RCV';
  
   DBMS_OUTPUT.put_line (
      'Records deleted from MMTT :' || SQL%ROWCOUNT);
   
   COMMIT;

   DBMS_OUTPUT.put_line ('After Delete');
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Unable to Delete ' || SQLERRM);
END;
/