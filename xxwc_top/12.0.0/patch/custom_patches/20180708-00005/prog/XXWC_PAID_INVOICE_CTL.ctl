--*********************************************************
--*File Name: XXWC_PAID_INVOICE_CTL.ctl                   *
--*Author: Thiruvengadam Rajaiah.N                        *
--*Creation Date: 15-Sep-2011                                    *
--*Last Updated Date: 16-Sep-2011                                     *
--*Data Source: PMC_ORDER_IMPORT_STG                                  *
--*           TMS#20180708-00005 - AH Harries Paid Invoices Conversion    * 
--*                                Added SKIP=1                       * 
--*                                Date format changed MM/DD/RR       *
--*********************************************************************
OPTIONS(SKIP=0,DIRECT=FALSE,PARALLEL=TRUE,ROWS=30000,BINDSIZE=20970000)
LOAD DATA
REPLACE INTO TABLE XXWC_AP_PAID_INVOICE_STG
FIELDS TERMINATED BY '|' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(  INVOICE_NUM                     "TRIM(:INVOICE_NUM)"
 , VENDOR_NUM                        "TRIM(:VENDOR_NUM)"
 , VENDOR_SITE_CODE                  "TRIM(:VENDOR_SITE_CODE)"
  , INVOICE_AMOUNT                   "TRIM(:INVOICE_AMOUNT)"
 , INVOICE_DATE                     "to_date(:INVOICE_DATE,'MM/DD/RR')"
 ,org_id                            "TRIM(:ORG_ID)" )
