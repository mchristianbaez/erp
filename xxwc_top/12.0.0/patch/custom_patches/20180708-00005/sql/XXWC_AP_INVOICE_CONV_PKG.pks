CREATE OR REPLACE PACKAGE APPS.XXWC_AP_INVOICE_CONV_PKG
AS
   /***************************************************************************
      *    script name: xxwc_ap_invoice_conv_pkg.pks
      *
      *    interface / conversion name: Open/Closed AP Invoice Conversion.
      *
      *    functional purpose: convert AP Invoices using open interface tables.
      *
      *    history:
      *
      *    version    date              author             description
      *************************************************************************
      *    1.0        04-sep-2011   T.Rajaiah     initial development
      *    1.1        23-Mar-2018   P.Vamshidhar  Added additional parameters to
	  *                                           TMS#20180708-00005 - AH Harries Paid Invoices Conversion
      *                                           handle attachments.
      ***********************************************************************************************************/

   l_verify_flag             VARCHAR2 (1);
   l_error_message           VARCHAR2 (240);
   g_invoice_currency_code   VARCHAR2 (10);
   g_terms_id                NUMBER (15);
   g_ccid                    NUMBER (15);
   g_org_id                  NUMBER (10) := xxwc_ascp_scwb_pkg.get_wc_org_id;
   g_vendor_id               NUMBER (15);
   g_validate_only           VARCHAR2 (1);
   g_import_only             VARCHAR2 (1);
   g_hdrcount                NUMBER (15) := 0;
   g_request_id              NUMBER;
   g_dev_phase               VARCHAR2 (100);
   g_dev_status              VARCHAR2 (100);
   g_submit                  VARCHAR2 (1);
   g_errorcode               NUMBER;
   g_errormessage            VARCHAR2 (240);

   PROCEDURE xxwc_apinvoice_hdr_valid (p_invoice_type             VARCHAR2,
                                       p_invoice_currency_code    VARCHAR2,
                                       p_invoice_date             DATE,
                                       p_vendor_site_id           NUMBER,
                                       p_terms_name               VARCHAR2,
                                       p_ccid                     NUMBER,
                                       p_pay_method               VARCHAR2);

   PROCEDURE xxwc_apinvoice_lines_valid (p_line_type_lookup_code    VARCHAR2,
                                         p_ccid                     NUMBER);

   PROCEDURE xxwc_doc_attach_proc;

   PROCEDURE xxwc_apinvoice_standard_prog;

   PROCEDURE xxwc_stage_to_interface;

   PROCEDURE xxwc_ap_paid_invoice_exe (errbuf    OUT VARCHAR2,
                                       retcode   OUT NUMBER);

   PROCEDURE main (errbuf               OUT VARCHAR2,
                   retcode              OUT NUMBER,
                   p_1               IN     VARCHAR2,
                   p_2               IN     VARCHAR2,
                   p_3               IN     VARCHAR2,
                   p_ccid            IN     VARCHAR2,
                   p_validate_only   IN     VARCHAR2,
                   p_submit          IN     VARCHAR2,
                   p_source_name     IN     VARCHAR2,    -- Added in Rev 1.1
                   p_import_date     IN     VARCHAR2);   -- Added in Rev 1.1
END xxwc_ap_invoice_conv_pkg;
/
