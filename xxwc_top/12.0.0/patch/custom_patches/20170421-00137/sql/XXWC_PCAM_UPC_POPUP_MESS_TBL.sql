/******************************************************************************************************************
   NAME:       XXWC_PCAM_UPC_POPUP_MESS_TBL.sql
   PURPOSE:    Table to store PCAM UPC error message to user.
   
   REVISIONS:
   Ver        Date          Author               Description
   ---------  -----------  ---------------  ---------------------------------------------------------------------
   1.0        15-May-2017  P.Vamshidhar     TMS#20170421-00137 - UPC Validation(Product Creation and Maintenance)
******************************************************************************************************************/
CREATE TABLE XXWC.XXWC_PCAM_UPC_POPUP_MESS_TBL
(
  ITEM_NUMBER      VARCHAR2(30 BYTE),
  CROSS_REFERENCE  VARCHAR2(150 BYTE),
  CREATED_BY       NUMBER,
  CREATION_DATE    DATE,
  CREATED_LOGIN    NUMBER,
  MESSAGE          VARCHAR2(3000 BYTE)
);