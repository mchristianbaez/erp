  /********************************************************************************
  FILE NAME: XXWC_DROP_OBJECTS.sql
  
  PROGRAM TYPE: Drop custom objects to add new columns for TMS#20170822-00145-Mobile Quote PLSQL api changes 
  
  PURPOSE: Drop custom objects to add new columns for TMS#20170822-00145-Mobile Quote PLSQL api changes 
  
  HISTORY
  ==========================================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- ----------------------------------------------------
  2.0     08/24/2017    Rakesh P.       TMS#20170822-00145-Mobile Quote PLSQL api changes 
  *******************************************************************************************/
SET serveroutput ON; 
 
DECLARE
  l_count        NUMBER;
  sql_stmt       VARCHAR2(4000);
BEGIN
  DBMS_OUTPUT.ENABLE(1000000);

  BEGIN
    SELECT count(1)
      INTO l_count
      FROM DBA_OBJECTS
     WHERE object_name = 'XXWC_ORDER_HEADER_REC'
	   AND object_type = 'TYPE'
	   AND TRUNC(CREATED) < TRUNC(sysdate);
   
	 DBMS_OUTPUT.put_line('XXWC_ORDER_HEADER_REC l_count : '|| l_count);
	 
	 IF l_count > 0 THEN
	    BEGIN
	       DBMS_OUTPUT.put_line('DROP XXWC.XXWC_HW_LOG_TBL ... ');
		   sql_stmt := 'DROP TABLE XXWC.XXWC_HW_LOG_TBL';
           EXECUTE IMMEDIATE sql_stmt;		
		EXCEPTION
		WHEN OTHERS THEN
		  NULL;
		END;
    	
        BEGIN
	       DBMS_OUTPUT.put_line('DROP PUBLIC SYNONYM XXWC_HW_LOG_TBL ... ');
		   sql_stmt := 'DROP PUBLIC SYNONYM XXWC_HW_LOG_TBL';
           EXECUTE IMMEDIATE sql_stmt;		
		EXCEPTION
		WHEN OTHERS THEN
		  NULL;
		END;
			
			
		BEGIN
		   DBMS_OUTPUT.put_line('DROP XXWC.XXWC_ORDER_HEADER_REC ... ');
	       sql_stmt := 'DROP TYPE XXWC.XXWC_ORDER_HEADER_REC FORCE';
           EXECUTE IMMEDIATE sql_stmt;		
		EXCEPTION
		WHEN OTHERS THEN
		  NULL;
		END;
		
		BEGIN
	       DBMS_OUTPUT.put_line('DROP PUBLIC SYNONYM XXWC_ORDER_HEADER_REC ... ');
		   sql_stmt := 'DROP PUBLIC SYNONYM XXWC_ORDER_HEADER_REC';
           EXECUTE IMMEDIATE sql_stmt;		
		EXCEPTION
		WHEN OTHERS THEN
		  NULL;
		END;
		
	 END IF;
  END;

  BEGIN
    SELECT count(1)
      INTO l_count
      FROM DBA_OBJECTS
     WHERE object_name = 'XXWC_ORDER_LINE_REC'
	   AND object_type = 'TYPE'
	   AND TRUNC(CREATED) < TRUNC(sysdate);
    
	 DBMS_OUTPUT.put_line('XXWC_ORDER_LINE_REC l_count : '|| l_count);
	 
	 IF l_count > 0 THEN
	    BEGIN
 		   DBMS_OUTPUT.put_line('DROP XXWC.XXWC_HW_LOG_TBL ... ');
	       sql_stmt := 'DROP TABLE XXWC.XXWC_HW_LOG_TBL';
           EXECUTE IMMEDIATE sql_stmt;		
		EXCEPTION
		WHEN OTHERS THEN
		  NULL;
		END;
		
		BEGIN
	       DBMS_OUTPUT.put_line('DROP PUBLIC SYNONYM XXWC_HW_LOG_TBL ... ');
		   sql_stmt := 'DROP PUBLIC SYNONYM XXWC_HW_LOG_TBL';
           EXECUTE IMMEDIATE sql_stmt;		
		EXCEPTION
		WHEN OTHERS THEN
		  NULL;
		END;
		
		BEGIN  
 		   DBMS_OUTPUT.put_line('DROP XXWC.XXWC_ORDER_LINES_TBL ... ');
	       sql_stmt := 'DROP TYPE XXWC.XXWC_ORDER_LINES_TBL';
           EXECUTE IMMEDIATE sql_stmt;		
		EXCEPTION
		WHEN OTHERS THEN
		  NULL;
		END;
		
	    BEGIN
	       DBMS_OUTPUT.put_line('DROP PUBLIC SYNONYM XXWC_ORDER_LINES_TBL ... ');
		   sql_stmt := 'DROP PUBLIC SYNONYM XXWC_ORDER_LINES_TBL';
           EXECUTE IMMEDIATE sql_stmt;		
		EXCEPTION
		WHEN OTHERS THEN
		  NULL;
		END;
		
		BEGIN
 		   DBMS_OUTPUT.put_line('DROP XXWC.XXWC_ORDER_LINE_REC ... ');
	       sql_stmt := 'DROP TYPE XXWC.XXWC_ORDER_LINE_REC FORCE';
           EXECUTE IMMEDIATE sql_stmt;		
		EXCEPTION
		WHEN OTHERS THEN
		  NULL;
		END;
		
	    BEGIN
	       DBMS_OUTPUT.put_line('DROP PUBLIC SYNONYM XXWC_ORDER_LINE_REC ... ');
		   sql_stmt := 'DROP PUBLIC SYNONYM XXWC_ORDER_LINE_REC';
           EXECUTE IMMEDIATE sql_stmt;		
		EXCEPTION
		WHEN OTHERS THEN
		  NULL;
		END;
		
	 END IF;
  END;
END;
/
