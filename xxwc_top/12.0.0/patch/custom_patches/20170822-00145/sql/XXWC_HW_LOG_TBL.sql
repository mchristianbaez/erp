  /********************************************************************************
  FILE NAME: XXWC_HW_LOG_TBL.sql
  
  PROGRAM TYPE: Log table for online sales order form
  
  PURPOSE: Log table for online sales order form
  
  HISTORY
  ==========================================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- ----------------------------------------------------
  1.0     03/15/2017    Rakesh P.       TMS#20170313-00308 -Validate the user for resp access in order create api, enable debug message
  2.0     08/24/2017    Rakesh P.       TMS#TMS#20170822-00145-Mobile Quote PLSQL api changes
  *******************************************************************************************/
CREATE TABLE XXWC.XXWC_HW_LOG_TBL
            (
             ORDER_HEADER_REC        XXWC.XXWC_ORDER_HEADER_REC,
             ORDER_LINES_TBL         XXWC.XXWC_ORDER_LINES_TBL,
             HEADER_ACTION           VARCHAR2(150 BYTE),
             ORDER_SOURCE_TYPE       VARCHAR2(3000),
             WAREHOUSE_ID            NUMBER,
             ORDER_TYPE_ID           NUMBER,  
             ORACLE_ORDER_NUMBER     NUMBER,  
             ORACLE_ORDER_TOTAL      NUMBER,
             ORIG_SYS_DOCUMENT_REF   VARCHAR2(150 BYTE),
             OFFLINE_ORDER_TOTAL     NUMBER,
             ORDER_CREATED_BY        NUMBER,
             API_RETURN_STATUS       VARCHAR2(150 BYTE),
             API_RETURN_MSG          VARCHAR2(4000 BYTE),
             CREATION_DATE           DATE                     NOT NULL,
             CREATED_BY              NUMBER(15)               NOT NULL,
             LAST_UPDATE_DATE        DATE                     NOT NULL,
             LAST_UPDATED_BY         NUMBER(15)               NOT NULL,
             ATTRIBUTE1              VARCHAR2(150 BYTE),
             ATTRIBUTE2              VARCHAR2(150 BYTE),
             ATTRIBUTE3              VARCHAR2(150 BYTE),
             ATTRIBUTE4              VARCHAR2(150 BYTE),
             ATTRIBUTE5              VARCHAR2(150 BYTE) 
) NESTED TABLE ORDER_LINES_TBL STORE AS ORD_LINES_TBL;

GRANT SELECT ON XXWC.XXWC_HW_LOG_TBL TO INTERFACE_MSSQL;

GRANT SELECT ON XXWC.XXWC_HW_LOG_TBL TO INTERFACE_OSO;

CREATE PUBLIC SYNONYM XXWC_HW_LOG_TBL FOR xxwc.XXWC_HW_LOG_TBL;

CREATE INDEX XXWC.XXWC_HW_LOG_TBL_N1 ON XXWC.XXWC_HW_LOG_TBL
(ORACLE_ORDER_NUMBER, ORDER_SOURCE_TYPE);      

CREATE INDEX XXWC.XXWC_HW_LOG_TBL_N2 ON XXWC.XXWC_HW_LOG_TBL
(ORACLE_ORDER_NUMBER);                    

CREATE INDEX XXWC.XXWC_HW_LOG_TBL_N3 ON XXWC.XXWC_HW_LOG_TBL
(WAREHOUSE_ID, ORDER_SOURCE_TYPE, HEADER_ACTION, API_RETURN_STATUS);  

