  /********************************************************************************
  FILE NAME: XXWC_ORDER_LINE_REC.sql
  
  PROGRAM TYPE: Order line for online sales order form
  
  PURPOSE: Order line for online sales order form
  
  HISTORY
  ==========================================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- ----------------------------------------------------
  1.0     03/07/2017    Rakesh P.       TMS#20170111-00059-Hand Write Oracle API for Web Service
                                        Initial Version
  2.0     08/24/2017    Rakesh P.       TMS#TMS#20170822-00145-Mobile Quote PLSQL api changes
  *******************************************************************************************/
CREATE OR REPLACE TYPE xxwc.xxwc_order_line_rec
AS
  OBJECT (   inventory_item_id      NUMBER
            ,item_number            VARCHAR2(3000)
            ,ordered_quantity       NUMBER
            ,unit_selling_price     NUMBER          --Ver 2.0
			,ship_from_org_id       VARCHAR2(3000)
			,subinventory           DATE
			,line_type_id           NUMBER
			,UOM                    VARCHAR2(3000)
			,line_id                NUMBER
			,line_action            VARCHAR2(200) 
			,branch_code            VARCHAR2(3)    --Ver 2.0
			,SHIPPING_INSTRUCTIONS  VARCHAR2(2000) --Ver 2.0
			,STATIC FUNCTION g_miss_null RETURN xxwc_order_line_rec
            );
/

CREATE OR REPLACE TYPE BODY xxwc.xxwc_order_line_rec
AS
   STATIC FUNCTION g_miss_null
       RETURN xxwc_order_line_rec
   AS
   BEGIN
      RETURN xxwc_order_line_rec ( inventory_item_id      => NULL
                                  ,item_number            => NULL
                                  ,ordered_quantity       => NULL
                                  ,unit_selling_price     => NULL --Ver 2.0
							  	  ,ship_from_org_id       => NULL
			                      ,subinventory           => NULL
			                      ,line_type_id           => NULL
			                      ,UOM                    => NULL
			                      ,line_id                => NULL  
			                      ,line_action            => NULL
			                      ,branch_code            => NULL --Ver 2.0  
			                      ,SHIPPING_INSTRUCTIONS  => NULL --Ver 2.0
                                   );
   END g_miss_null;
END;
/

CREATE PUBLIC SYNONYM xxwc_order_line_rec FOR xxwc.xxwc_order_line_rec;

show err;
EXIT;