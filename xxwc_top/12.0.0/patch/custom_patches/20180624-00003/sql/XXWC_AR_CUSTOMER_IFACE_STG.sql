 /*******************************************************************************************
  FILE NAME: XXWC.XXWC_AR_CUSTOMER_IFACE_STG
  PROGRAM TYPE: Table script

  PURPOSE: Conversion purpose for AH Harries

  HISTORY
  ============================================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- ------------------------------------------------------
  1.0     06/20/2018    P.Vamshidhar    TMS#20180624-00003 - AH Harries Customer Interface 
  *********************************************************************************************/
ALTER TABLE XXWC.XXWC_AR_CUSTOMER_IFACE_STG
 DROP PRIMARY KEY CASCADE;

DROP TABLE XXWC.XXWC_AR_CUSTOMER_IFACE_STG CASCADE CONSTRAINTS;

CREATE TABLE XXWC.XXWC_AR_CUSTOMER_IFACE_STG
(
  CUSTOMER_IFACE_STG_ID          NUMBER,
  ORIG_SYSTEM_REFERENCE          VARCHAR2(240 BYTE),
  ORIG_SYSTEM                    VARCHAR2(30 BYTE),
  STATUS                         VARCHAR2(1 BYTE),
  ATTRIBUTE_CATEGORY             VARCHAR2(30 BYTE),
  ATTRIBUTE1                     VARCHAR2(150 BYTE),
  ATTRIBUTE2                     VARCHAR2(150 BYTE),
  ATTRIBUTE3                     VARCHAR2(150 BYTE),
  ATTRIBUTE4                     VARCHAR2(150 BYTE),
  ATTRIBUTE5                     VARCHAR2(150 BYTE),
  ATTRIBUTE6                     VARCHAR2(150 BYTE),
  ATTRIBUTE7                     VARCHAR2(150 BYTE),
  ATTRIBUTE8                     VARCHAR2(150 BYTE),
  ATTRIBUTE9                     VARCHAR2(150 BYTE),
  ATTRIBUTE10                    VARCHAR2(150 BYTE),
  ATTRIBUTE11                    VARCHAR2(150 BYTE),
  ATTRIBUTE12                    VARCHAR2(150 BYTE),
  ATTRIBUTE13                    VARCHAR2(150 BYTE),
  ATTRIBUTE14                    VARCHAR2(150 BYTE),
  ATTRIBUTE15                    VARCHAR2(150 BYTE),
  ATTRIBUTE16                    VARCHAR2(150 BYTE),
  ATTRIBUTE17                    VARCHAR2(150 BYTE),
  ATTRIBUTE18                    VARCHAR2(150 BYTE),
  ATTRIBUTE19                    VARCHAR2(150 BYTE),
  ATTRIBUTE20                    VARCHAR2(150 BYTE),
  ATTRIBUTE21                    VARCHAR2(150 BYTE),
  ATTRIBUTE22                    VARCHAR2(150 BYTE),
  ATTRIBUTE23                    VARCHAR2(150 BYTE),
  ATTRIBUTE24                    VARCHAR2(150 BYTE),
  ORGANIZATION_NAME              VARCHAR2(630 BYTE),
  SIC_CODE_TYPE                  NUMBER,
  SIC_CODE                       VARCHAR2(20 BYTE),
  DUNS_NUMBER_C                  VARCHAR2(30 BYTE),
  GSA_INDICATOR_FLAG             VARCHAR2(10 BYTE),
  PARTY_TYPE                     VARCHAR2(30 BYTE),
  DB_RATING                      VARCHAR2(5 BYTE),
  TAX_REFERENCE                  VARCHAR2(50 BYTE),
  DUNS_NUMBER                    NUMBER,
  KNOWN_AS                       VARCHAR2(240 BYTE),
  EMPLOYEES_TOTAL                NUMBER,
  TOTAL_EMPLOYEES_IND            VARCHAR2(30 BYTE),
  TOTAL_EMP_MIN_IND              VARCHAR2(30 BYTE),
  TOTAL_EMP_EST_IND              VARCHAR2(30 BYTE),
  CURR_FY_POTENTIAL_REVENUE      NUMBER,
  NEXT_FY_POTENTIAL_REVENUE      NUMBER,
  YEAR_ESTABLISHED               DATE,
  COMPETITOR_FLAG                VARCHAR2(1 BYTE),
  CEO_NAME                       VARCHAR2(240 BYTE),
  CEO_TITLE                      VARCHAR2(240 BYTE),
  PRINCIPAL_NAME                 VARCHAR2(240 BYTE),
  PRINCIPAL_TITLE                VARCHAR2(240 BYTE),
  MINORITY_OWNED_IND             VARCHAR2(30 BYTE),
  MINORITY_OWNED_TYPE            VARCHAR2(30 BYTE),
  WOMAN_OWNED_IND                VARCHAR2(30 BYTE),
  DISADV_8A_IND                  VARCHAR2(30 BYTE),
  SMALL_BUS_IND                  VARCHAR2(30 BYTE),
  CONTROL_YR                     NUMBER,
  RENT_OWN_IND                   VARCHAR2(30 BYTE),
  PARTY_NUMBER                   VARCHAR2(30 BYTE),
  PARTY_ID                       NUMBER,
  PROFILE_ID                     NUMBER,
  HQ_BRANCH_IND                  VARCHAR2(30 BYTE),
  CUSTOMER_PROFILE_CLASS_NAME    VARCHAR2(30 BYTE),
  COLLECTOR_NAME                 VARCHAR2(30 BYTE),
  CREIDT_CHECKING                VARCHAR2(1 BYTE),
  CREDIT_HOLD                    VARCHAR2(1 BYTE),
  CUSTOMER_TYPE                  VARCHAR2(30 BYTE),
  CUSTOMER_CLASS_CODE            VARCHAR2(30 BYTE),
  SALES_CHANNEL_CODE             VARCHAR2(30 BYTE),
  ACCOUNT_NAME                   VARCHAR2(30 BYTE),
  DUNNING_LETTERS                VARCHAR2(1 BYTE),
  SEND_STATEMENTS                VARCHAR2(1 BYTE),
  CREDIT_BALANCE_STATEMENTS      VARCHAR2(1 BYTE),
  CREDIT_RATING                  VARCHAR2(30 BYTE),
  INTEREST_PERIOD_DAYS           NUMBER,
  DISCOUNT_GRACE_DAYS            NUMBER,
  PAYMENT_GRACE_DAYS             NUMBER,
  TOLERANCE                      NUMBER,
  STATEMENT_CYCLE_NAME           VARCHAR2(15 BYTE),
  STANDARD_TERM_NAME             VARCHAR2(15 BYTE),
  OVERRIDE_TERMS                 VARCHAR2(1 BYTE),
  DUNNING_LETTER_SET_NAME        VARCHAR2(30 BYTE),
  INTEREST_CHARGES               VARCHAR2(1 BYTE),
  TAX_PRINTING_OPTIONS           VARCHAR2(30 BYTE),
  RISK_CODE                      VARCHAR2(30 BYTE),
  CURRENCY_CODE                  VARCHAR2(15 BYTE),
  PERCENT_COLLECTABLE            NUMBER,
  OVERALL_CREDIT_LIMIT           NUMBER,
  TRX_CREDIT_LIMIT               NUMBER,
  MIN_DUNNING_AMOUNT             NUMBER,
  MIN_DUNNING_INVOICE_AMPOUNT    NUMBER,
  MAX_INTEREST_CHARGE            NUMBER,
  MIN_STATEMENT_AMOUNT           NUMBER,
  CLEARING_DAYS                  NUMBER,
  AUTO_REC_INCL_DISPUTED_FLAG    VARCHAR2(1 BYTE),
  AUTO_REC_MIN_RECEIPT_AMOUNT    NUMBER,
  CHARGE_ON_FINANCE_CHARGE_FLAG  VARCHAR2(1 BYTE),
  LOCK_BOX_MATCHING_OPTION       VARCHAR2(30 BYTE),
  AUTOCASH_HIERARCHY_NAME        VARCHAR2(30 BYTE),
  REVIEW_CYCLE                   VARCHAR2(30 BYTE),
  CREDIT_CLASSIFICATION          VARCHAR2(30 BYTE),
  AUTOPAY_FLAG                   VARCHAR2(1 BYTE),
  CUST_ACCOUNT_ID                NUMBER,
  CUST_ACCOUNT_PROFILE_ID        NUMBER,
  CREDIT_ANALYST_NAME            VARCHAR2(50 BYTE),
  CUSTOMER_SOURCE                VARCHAR2(10 BYTE),
  COD_COMMENT                    VARCHAR2(12 BYTE),
  KEYWORD                        VARCHAR2(12 BYTE),
  PROCESS_STATUS                 VARCHAR2(15 BYTE),
  PROCESS_ERROR                  VARCHAR2(4000 BYTE),
  CREATION_DATE                  DATE,
  CREATED_BY                     NUMBER,
  LAST_UPDATE_DATE               DATE,
  LAST_UPDATED_BY                NUMBER,
  PROCESSING_DATE                DATE,
  ARCHIVE_DATE                   DATE,
  REQUEST_ID                     NUMBER,
  ORG_ID                         NUMBER         DEFAULT NVL(TO_NUMBER(DECODE(SUBSTRB(USERENV('CLIENT_INFO'),1,1), ' ', NULL,SUBSTRB(USERENV('CLIENT_INFO'),1,10))),162),
  ACCOUNT_STATUS                 VARCHAR2(20 BYTE),
  STAGE_TABLE_RECORD_STATUS      VARCHAR2(10 BYTE)
)
TABLESPACE XXWC_DATA
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MAXSIZE          UNLIMITED
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX XXWC.XXWC_AR_CUSTOMER_IFACE_STG_PK ON XXWC.XXWC_AR_CUSTOMER_IFACE_STG
(CUSTOMER_IFACE_STG_ID)
LOGGING
TABLESPACE XXWC_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MAXSIZE          UNLIMITED
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
NOPARALLEL;

ALTER INDEX XXWC.XXWC_AR_CUSTOMER_IFACE_STG_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER XXWC.XXWC_AR_CUSTOMER_IFACE_STG_T1
   BEFORE INSERT
   ON XXWC.XXWC_AR_CUSTOMER_IFACE_STG
   REFERENCING NEW AS New OLD AS Old
   FOR EACH ROW
DECLARE
   Customer_Iface_Stg_ID   NUMBER;
/******************************************************************************
   NAME:
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        3/29/2012             1. Created this trigger.

   NOTES:

   Automatically available Auto Replace Keywords:
      Object Name:
      Sysdate:         3/29/2012
      Date and Time:   3/29/2012, 3:54:27 PM, and 3/29/2012 3:54:27 PM
      Username:         (set in TOAD Options, Proc Templates)
      Table Name:      XXWC_AR_CUSTOMER_IFACE_STG_TRG1 (set in the "New PL/SQL Object" dialog)
      Trigger Options:  (set in the "New PL/SQL Object" dialog)
******************************************************************************/
BEGIN
   Customer_Iface_Stg_ID := 0;

   SELECT XXWC_AR_CUSTOMER_IFACE_S.NEXTVAL
     INTO Customer_Iface_Stg_ID
     FROM DUAL;

   :NEW.CUSTOMER_IFACE_STG_ID := Customer_Iface_Stg_ID;

   :NEW.CREATION_DATE := SYSDATE;
   :NEW.CREATED_BY := 1;
EXCEPTION
   WHEN OTHERS
   THEN
      -- Consider logging the error and then re-raise
      RAISE;
END;
/


CREATE OR REPLACE SYNONYM APPS.XXWC_AR_CUSTOMER_IFACE_STG FOR XXWC.XXWC_AR_CUSTOMER_IFACE_STG;


CREATE OR REPLACE PUBLIC SYNONYM XXWC_AR_CUSTOMER_IFACE_STG FOR XXWC.XXWC_AR_CUSTOMER_IFACE_STG;


ALTER TABLE XXWC.XXWC_AR_CUSTOMER_IFACE_STG ADD (
  CONSTRAINT XXWC_AR_CUSTOMER_IFACE_STG_PK
  PRIMARY KEY
  (CUSTOMER_IFACE_STG_ID)
  USING INDEX XXWC.XXWC_AR_CUSTOMER_IFACE_STG_PK
  ENABLE VALIDATE);

GRANT DELETE, INSERT, SELECT, UPDATE ON XXWC.XXWC_AR_CUSTOMER_IFACE_STG TO EA_APEX;

GRANT DELETE, INSERT, SELECT, UPDATE ON XXWC.XXWC_AR_CUSTOMER_IFACE_STG TO INTERFACE_APEXWC;

GRANT SELECT ON XXWC.XXWC_AR_CUSTOMER_IFACE_STG TO INTERFACE_PRISM;

GRANT DELETE, INSERT, SELECT, UPDATE ON XXWC.XXWC_AR_CUSTOMER_IFACE_STG TO XXWC_DEV_ADMIN_ROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON XXWC.XXWC_AR_CUSTOMER_IFACE_STG TO XXWC_EDIT_IFACE_ROLE;

GRANT INSERT ON XXWC.XXWC_AR_CUSTOMER_IFACE_STG TO XXWC_PRISM_INSERT_ROLE;

GRANT SELECT ON XXWC.XXWC_AR_CUSTOMER_IFACE_STG TO XXWC_PRISM_SELECT_ROLE;
/