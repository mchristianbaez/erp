/*************************************************************************
$Header XXWC_PRICING_SEG_GTT_TBL.sql $
Module Name: XXWC_PRICING_SEG_GTT_TBL.sql

PURPOSE:   This Global Temporary Table is used by Pricing Segmentation processi

REVISIONS:
Ver        Date        Author                  Description
---------  ----------  ---------------         -------------------------
1.0        10/09/2015  Gopi Damuluri           Initial Version
                                               TMS# 20140609-00256
****************************************************************************/

CREATE GLOBAL TEMPORARY TABLE XXWC.XXWC_PRICING_SEG_GTT_TBL
(
  LIST_HEADER_ID        NUMBER,
  LIST_LINE_ID          NUMBER,
  CUSTOMER_ID           NUMBER,
  CUSTOMER_SITE_ID      NUMBER,
  MODIFIER_TYPE         VARCHAR2(200),
  UPDATE_LEVEL          VARCHAR2(1)
)
ON COMMIT PRESERVE ROWS
RESULT_CACHE (MODE DEFAULT)
NOCACHE;
/