CREATE OR REPLACE PACKAGE APPS.XXWC_PRICING_SEGMENT_PKG
AS
   /*************************************************************************
      $Header XXWC_PRICING_SEGMENT_PKG.PKS $
      Module Name: XXWC_PRICING_SEGMENT_PKG.PKS

      PURPOSE:   This package is used by Pricing Segmentation process

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------
      1.0        10/09/2015  Gopi Damuluri           Initial Version
                                                     TMS# 20151111-00161
   ****************************************************************************/

   /*************************************************************************
      PROCEDURE Name: main

      PURPOSE:   To validate B2B Customer Staging data and load the same into
                 Interface table.

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------
      1.0        10/09/2015  Gopi Damuluri             Initial Version
   ***************************************************************************/
   PROCEDURE main (p_errbuf                  OUT VARCHAR2,
                   p_retcode                 OUT NUMBER,
                   p_process_date             IN VARCHAR2,
                   p_in_seg_list_header_id    IN NUMBER,
                   p_in_csp_list_header_id    IN NUMBER,
                   p_process_method           IN VARCHAR2);

   /*************************************************************************
      FUNCTION Name: is_valid_qualifier

      PURPOSE:   Check if the qualifier if valid for Segment Modifier.

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------
      1.0        10/09/2015  Gopi Damuluri             Initial Version
   ***************************************************************************/
FUNCTION is_valid_qualifier (p_qual_product_context   IN VARCHAR2
                           , p_qual_product_attr_val  IN VARCHAR2
                           , p_seg_list_line_id       IN NUMBER
                              )
RETURN VARCHAR2;

   /*************************************************************************
      FUNCTION Name: get_line_exists

      PURPOSE:   To derive CSP lines to be Excluded from Segment Modifier.

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------
      1.0        10/09/2015  Gopi Damuluri             Initial Version
   ***************************************************************************/
FUNCTION get_line_exists (p_bpw_product_attr      IN VARCHAR2
                             , p_bpw_product_attr_val  IN VARCHAR2
                             , p_seg_list_header_id    IN NUMBER
                              )
RETURN VARCHAR2;

   /*************************************************************************
      PROCEDURE Name: import_csp

      PURPOSE:   To update Matrix Modifier when importing CSP.

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------
      1.0        10/09/2015  Gopi Damuluri             Initial Version
   ***************************************************************************/
   PROCEDURE import_csp (p_agreement_id             IN NUMBER);

END XXWC_PRICING_SEGMENT_PKG;
/
