/*************************************************************************
  $Header TMS_20160621-00017_AWAITING_INVOICE_TO_CLOSE.sql $
  Module Name: TMS_20160621-00017  Data Fix script for I675908

  PURPOSE: Data fix script for I675908--No permanent fix in process (But patch available per oracle)

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        17-Aug-2016  Neha Saini         TMS#20160621-00017 

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20160621-00017    , Before Update');

update apps.oe_order_lines_all
set FLOW_STATUS_CODE='CANCELLED'
where line_id =55710866;
--and open_Flag='N' --commented for TMS#20160621-00017 
--and cancelled_flag='Y'; --commented for TMS#20160621-00017 

   DBMS_OUTPUT.put_line (
         'TMS: 20160621-00017  Sales order lines updated (Expected:1): '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20160621-00017    , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20160621-00017 , Errors : ' || SQLERRM);
END;
/