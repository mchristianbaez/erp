---------------------------------------------------------------------------------------------------------------
/**************************************************************************************************************
  $Header XXEIS.XXWC_INV_RF_METRIC_RPT_TBL $
  Description	  : This table is used to get data from XXEIS.EIS_RS_XXWC_RF_METRICS_PKG Package.
					All rows for each process are deleted after each run. This table should normally have 0 rows
  REVISIONS   :
  VERSION 	DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     2-Aug-2016       		Siva   		 TMS#20160712-00024 
**************************************************************************************************************/
CREATE TABLE XXEIS.XXWC_INV_RF_METRIC_RPT_TBL
(
    PROCESS_ID                NUMBER,
    QUERY_TYPE                VARCHAR2(50),
    REC_TRANSACTION_EMPLOYEE  VARCHAR2(240),
    REC_ORG                   VARCHAR2(3),
    REC_TRANSACTION_TYPE_NAME VARCHAR2(150) ,
    REC_REASON_NAME           VARCHAR2(50) ,
    REC_DESCRIPTION           VARCHAR2(240) ,
    REC_DOC_NUM               VARCHAR2(30) ,
    REC_TOTAL_LINES           NUMBER ,
    REC_RCVD_DATE             DATE ,
    REC_VENDOR_NUM            VARCHAR2(30) ,
    REC_VENDOR                VARCHAR2(240),
    REC_SUBINVENTORY_CODE     VARCHAR2(30) ,
    CC_ORG                    VARCHAR2(3) ,
    CC_SUB_INV                VARCHAR2(30) ,
    CC_ITEM_NUM               VARCHAR2(50) ,
    CC_TRANSACTION_EMPLOYEE   VARCHAR2(240) ,
    CC_COUNT_DATE_CURRENT     DATE ,
    BC_TRANSACTION_EMPLOYEE   VARCHAR2(240) ,
    BC_ORG                    VARCHAR2(3) ,
    BC_SUB_INV                VARCHAR2(30) ,
    BC_BIN                    VARCHAR2(50) ,
    BC_CREATED_VIA            VARCHAR2(50) ,
    BC_BIN_CREATE_DATE        DATE ,
    BA_TRANSACTION_EMPLOYEE   VARCHAR2(240) ,
    BA_ORG                    VARCHAR2(3) ,
    BA_ITEM_NUM               VARCHAR2(50) ,
    BA_SUB_INV                VARCHAR2(30) ,
    BA_BIN                    VARCHAR2(50) ,
    BA_CREATED_VIA            VARCHAR2(30) ,
    BA_BIN_MAINT_DATE         DATE ,
    AB_TRANSACTION_EMPLOYEE   VARCHAR2(240) ,
    AB_ITEM_NUM               VARCHAR2(50) ,
    AB_DESCRIPTION            VARCHAR2(240) ,
    AB_XREF_TYPE              VARCHAR2(50) ,
    AB_XREF_VALUE             VARCHAR2(300) ,
    AB_XREF_LENGTH            NUMBER ,
    AB_XREF_CREATE_DATE       DATE ,
    DAYS                      NUMBER
);

COMMENT ON TABLE XXEIS.XXWC_INV_RF_METRIC_RPT_TBL IS 'This table is used to get data from XXEIS.EIS_RS_XXWC_RF_METRICS_PKG Package.
All rows for each process are deleted after each run. This table should normally have 0 rows.'                                                                                                                                                                                                                                            
/
