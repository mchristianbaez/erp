CREATE OR REPLACE PACKAGE BODY  XXEIS.EIS_RS_XXWC_RF_METRICS_PKG
--//============================================================================
--//  
--// Object Name         		:: xxeis.EIS_RS_XXWC_RF_METRICS_PKG
--//
--// Object Type         		:: Package Body
--//
--// Object Description  		:: This Package will trigger in before report and insert the values into Temp Table
--//
--// Version Control
--//============================================================================
--// Vers    	Author           Date          Description
--//----------------------------------------------------------------------------
--//  1.0     2-Aug-2016       		Siva   		 TMS#20160712-00024 
--//============================================================================
IS
   PROCEDURE RF_METRIC_REP_PRC (P_PROCESS_ID       IN NUMBER
                                ,P_DATE_FROM       IN DATE
                                ,P_DATE_TO         IN DATE
                                ,p_Org             IN VARCHAR2
                                )
--//============================================================================
--//
--// Object Name         :: RF_METRIC_REP_PRC
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This procedure gets the record based on parameter and insert into table
--//
--// Version Control
--//============================================================================
--// Vers    Author             Date            Description
--//----------------------------------------------------------------------------
--//  1.0     2-Aug-2016       		Siva   		 TMS#20160712-00024 
--//============================================================================	
AS
  l_metric_rep_csr sys_refcursor;
  l_main_qry  VARCHAR2 (32000);
  l_Receiving_Data_qry VARCHAR2 (32000);
  l_Cycle_Count_qry VARCHAR2 (32000);
  l_bin_create_qry VARCHAR2 (32000);
  l_bin_assign_qry VARCHAR2 (32000);
  l_barcode_qry VARCHAR2 (32000);
  LC_ORG_COND VARCHAR2 (32000);
  lc_rcv_data_date_cond VARCHAR2 (10000);
  lc_cycle_count_date_cond VARCHAR2 (10000);
  lc_bin_create_date_cond VARCHAR2 (10000);
  lc_bin_assign_date_cond VARCHAR2 (10000);
  lc_barcode_date_cond VARCHAR2 (10000);

  TYPE rf_metric_rep_type_tbl
IS
  TABLE OF xxeis.XXWC_INV_RF_METRIC_RPT_TBL%ROWTYPE INDEX BY BINARY_INTEGER;
  rf_metric_rep_rec_tab rf_metric_rep_type_tbl;

  BEGIN
  lc_org_cond               :=null;
  lc_rcv_data_date_cond     :=null;
  lc_cycle_count_date_cond  :=null;
  lc_bin_create_date_cond   :=null;
  lc_bin_assign_date_cond   :=null;
  lc_barcode_date_cond      :=null;
  
  rf_metric_rep_rec_tab.DELETE;
  
  IF P_DATE_FROM IS NOT NULL and P_DATE_TO IS NOT NULL THEN
    LC_RCV_DATA_DATE_COND:= LC_RCV_DATA_DATE_COND||' AND TRUNC(MMT.CREATION_DATE)  >= '||XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(P_DATE_FROM)||'';
    LC_RCV_DATA_DATE_COND:= LC_RCV_DATA_DATE_COND||' AND TRUNC(MMT.CREATION_DATE)  <= '||XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(P_DATE_TO)||'';
    
    LC_CYCLE_COUNT_DATE_COND:= LC_CYCLE_COUNT_DATE_COND||' AND TRUNC(cce.count_due_date) >= '||XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(P_DATE_FROM)||'';
    lc_cycle_count_date_cond:= lc_cycle_count_date_cond||' AND TRUNC(cce.count_due_date) <= '||xxeis.eis_rs_utility.get_param_values(P_DATE_TO)||'';
    
    LC_BIN_CREATE_DATE_COND:= LC_BIN_CREATE_DATE_COND||' AND TRUNC(mil.last_update_date) >=  '||XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(P_DATE_FROM)||'';
    lc_bin_create_date_cond:= lc_bin_create_date_cond||' AND TRUNC(mil.last_update_date) <=  '||xxeis.eis_rs_utility.get_param_values(P_DATE_TO)||'';
    
    LC_BIN_ASSIGN_DATE_COND:= LC_BIN_ASSIGN_DATE_COND||' AND TRUNC(msla.last_update_date) >=  '||XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(P_DATE_FROM)||'';
    LC_BIN_ASSIGN_DATE_COND:= LC_BIN_ASSIGN_DATE_COND||' AND TRUNC(msla.last_update_date) <=  '||XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(P_DATE_TO)||'';    
    
    LC_BARCODE_DATE_COND:= LC_BARCODE_DATE_COND||' AND TRUNC(XREF.CREATION_DATE) >='||XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(P_DATE_FROM)||'';
    lc_barcode_date_cond:= lc_barcode_date_cond||' AND TRUNC(XREF.CREATION_DATE) <='||xxeis.eis_rs_utility.get_param_values(P_DATE_TO)||'';
  END IF;
  

  IF P_ORG  IS NOT NULL THEN
    LC_ORG_COND:= LC_ORG_COND||' and orgs.organization_code in ('||xxeis.eis_rs_utility.get_param_values(p_Org)||' )';
  END IF;
  
   l_receiving_data_qry:='SELECT 
    '||p_process_id ||' process_id, 
    ''Receiving Data'' QUERY_TYPE,
    TRANSACTION_EMPLOYEE REC_TRANSACTION_EMPLOYEE,
    ORG REC_ORG,
    TRANSACTION_TYPE_NAME REC_TRANSACTION_TYPE_NAME,
    REASON_NAME REC_REASON_NAME ,
    DESCRIPTION REC_DESCRIPTION ,
    DOC_NUM REC_DOC_NUM ,
    SUM (DOC_LINES) REC_TOTAL_LINES ,
    RCVD_DATE REC_RCVD_DATE ,
    VENDOR_NUM REC_VENDOR_NUM ,
    VENDOR REC_VENDOR ,
    SUBINVENTORY_CODE REC_SUBINVENTORY_CODE,
    NULL CC_ORG,
    NULL CC_SUB_INV,
    NULL CC_ITEM_NUM,
    NULL CC_TRANSACTION_EMPLOYEE,
    TO_DATE(NULL) CC_COUNT_DATE_CURRENT,
    NULL BC_TRANSACTION_EMPLOYEE,
    NULL BC_ORG,
    NULL BC_SUB_INV,
    NULL BC_BIN,
    NULL BC_CREATED_VIA,
    TO_DATE(NULL) BC_BIN_CREATE_DATE,
    NULL BA_TRANSACTION_EMPLOYEE,
    NULL BA_ORG,
    NULL BA_ITEM_NUM,
    NULL BA_SUB_INV,
    NULL BA_BIN,
    NULL BA_CREATED_VIA,
    TO_DATE(NULL) BA_BIN_MAINT_DATE,
    NULL AB_TRANSACTION_EMPLOYEE,
    NULL AB_ITEM_NUM,
    NULL AB_DESCRIPTION,
    NULL AB_XREF_TYPE,
    NULL AB_XREF_VALUE,
    TO_NUMBER(NULL) AB_XREF_LENGTH,
    TO_DATE(NULL) AB_XREF_CREATE_DATE,
    ROUND((sysdate-rcvd_date),2) days
  FROM
    (SELECT fuse.description TRANSACTION_EMPLOYEE,
      fuse.user_name,
      orgs.organization_code org,
      mtt.transaction_type_name,
      mtr.reason_name,
      mtr.description,
      CASE
        WHEN mtt.transaction_type_name = ''PO Receipt''
        THEN
          (SELECT poh.segment1
          FROM apps.PO_HEADERS_ALL poh
          WHERE mmt.transaction_source_id = poh.po_header_id
          )
        WHEN mtt.transaction_type_name = ''Int Req Intr Rcpt''
        THEN
          (SELECT por.segment1
          FROM apps.PO_REQUISITION_HEADERS_ALL por
          WHERE mmt.transaction_source_id = por.requisition_header_id
          )
        WHEN mtt.transaction_type_name = ''RMA Receipt''
        THEN ''RMA Receipt''
      END doc_num,
      CASE
        WHEN mtt.transaction_type_name = ''PO Receipt''
        THEN
          (SELECT COUNT (1)
          FROM apps.po_lines_all pol
          WHERE pol.po_header_id    = mmt.transaction_source_id
          AND mmt.inventory_item_id = pol.item_id
          )
        WHEN mtt.transaction_type_name = ''Int Req Intr Rcpt''
        THEN
          (SELECT COUNT (1)
          FROM apps.PO_REQUISITION_LINES_ALL porl
          WHERE porl.requisition_header_id = mmt.transaction_source_id
          AND mmt.inventory_item_id        = porl.item_id
          )
        WHEN mtt.transaction_type_name = ''RMA Receipt''
        THEN NULL
      END doc_lines,
      TRUNC (mmt.creation_date) RCVD_DATE,
      CASE
        WHEN mtt.transaction_type_name = ''PO Receipt''
        THEN
          (SELECT pov.segment1
          FROM apps.PO_VENDORS pov,
            apps.PO_HEADERS_ALL poh
          WHERE mmt.transaction_source_id = poh.po_header_id
          AND poh.vendor_id               = pov.vendor_id
          )
        WHEN mtt.transaction_type_name = ''Int Req Intr Rcpt''
        THEN ''''
        WHEN mtt.transaction_type_name = ''RMA Receipt''
        THEN ''''
      END vendor_num,
      CASE
        WHEN mtt.transaction_type_name = ''PO Receipt''
        THEN
          (SELECT pov.VENDOR_NAME
          FROM apps.PO_VENDORS pov,
            apps.PO_HEADERS_ALL poh
          WHERE mmt.transaction_source_id = poh.po_header_id
          AND poh.vendor_id               = pov.vendor_id
          )
        WHEN mtt.transaction_type_name = ''Int Req Intr Rcpt''
        THEN ''''
        WHEN mtt.transaction_type_name = ''RMA Receipt''
        THEN ''''
      END vendor,
      mmt.subinventory_code
    FROM apps.mtl_material_transactions mmt,
      apps.mtl_transaction_types mtt,
      apps.fnd_user fuse,
      apps.mtl_transaction_reasons mtr,
      apps.mtl_parameters orgs
    WHERE mtt.transaction_type_id  = mmt.transaction_type_id
    AND mtt.transaction_type_name IN (''PO Receipt'', ''RMA Receipt'', ''Int Req Intr Rcpt'')
    AND mmt.reason_id              = mtr.reason_id(+)
    AND FUSE.user_id               = mmt.created_by
    AND MMT.ORGANIZATION_ID        = ORGS.ORGANIZATION_ID
    '||lc_rcv_data_date_cond||'
    '||lc_org_cond||'
    )
  WHERE user_name IN
    (SELECT DISTINCT (a.user_name)
    FROM apps.wf_local_roles rsub,
      apps.wf_user_role_assignments a
    WHERE rsub.parent_orig_system = a.role_orig_system
    AND rsub.name                 = a.role_name
    AND rsub.display_name LIKE ''HDS%Mobile%''
    )
  AND subinventory_code NOT IN (''Drop'',''Rental'')
  GROUP BY TRANSACTION_EMPLOYEE,
    org,
    transaction_type_name,
    reason_name,
    description,
    doc_num,
    rcvd_date,
    vendor_num,
    vendor,
    subinventory_code';

l_Cycle_Count_qry :='SELECT 
    '||p_process_id ||' process_id, 
    ''Cycle Count Data'' QUERY_TYPE,
    NULL REC_TRANSACTION_EMPLOYEE,
    NULL REC_ORG,
    NULL REC_TRANSACTION_TYPE_NAME,
    NULL REC_REASON_NAME,
    NULL REC_DESCRIPTION,
    NULL REC_DOC_NUM,
    TO_NUMBER(NULL) REC_TOTAL_LINES,
    TO_DATE(NULL) REC_RCVD_DATE,
    NULL REC_VENDOR_NUM,
    NULL REC_VENDOR,
    NULL REC_SUBINVENTORY_CODE,
    ORGS.ORGANIZATION_CODE CC_ORG,
    CCE.SUBINVENTORY CC_SUB_INV,
    MIB.SEGMENT1 CC_ITEM_NUM,
    FUSE.DESCRIPTION CC_TRANSACTION_EMPLOYEE,
    TRUNC(CCE.COUNT_DATE_CURRENT) CC_COUNT_DATE_CURRENT,
    NULL BC_TRANSACTION_EMPLOYEE,
    NULL BC_ORG,
    NULL BC_SUB_INV,
    NULL BC_BIN,
    NULL BC_CREATED_VIA,
    TO_DATE(NULL) BC_BIN_CREATE_DATE,
    NULL BA_TRANSACTION_EMPLOYEE,
    NULL BA_ORG,
    NULL BA_ITEM_NUM,
    NULL BA_SUB_INV,
    NULL BA_BIN,
    NULL BA_CREATED_VIA,
    TO_DATE(NULL) BA_BIN_MAINT_DATE,
    NULL AB_TRANSACTION_EMPLOYEE,
    NULL AB_ITEM_NUM,
    NULL AB_DESCRIPTION,
    NULL AB_XREF_TYPE,
    NULL AB_XREF_VALUE,
    TO_NUMBER(NULL) AB_XREF_LENGTH,
    TO_DATE(NULL) AB_XREF_CREATE_DATE,
    ROUND((sysdate-CCE.COUNT_DATE_CURRENT),2) days
  FROM APPS.mtl_cycle_count_entries_v cce,
    apps.mtl_parameters orgs,
    apps.mtl_system_items_b mib,
    apps.fnd_user fuse
  WHERE orgs.organization_id = cce.organization_id
  AND mib.inventory_item_id  = cce.inventory_item_id
  AND mib.organization_id    = cce.organization_id
  AND fuse.employee_id       = cce.counted_by_employee_id_first
  AND fuse.user_name        IN
    (SELECT DISTINCT (a.user_name)
    FROM apps.wf_local_roles rsub,
      apps.wf_user_role_assignments a
    WHERE rsub.parent_orig_system = a.role_orig_system
    AND rsub.name                 = a.role_name
    AND rsub.display_name LIKE ''HDS%Mobile%''
    )
  '||LC_ORG_COND||'
  '||lc_cycle_count_date_cond||'  
  ';

l_bin_create_qry :='SELECT 
    '||p_process_id ||' process_id,
    ''Bin Create Data'' QUERY_TYPE,
    NULL REC_TRANSACTION_EMPLOYEE,
    NULL REC_ORG,
    NULL REC_TRANSACTION_TYPE_NAME,
    NULL REC_REASON_NAME,
    NULL REC_DESCRIPTION,
    NULL REC_DOC_NUM,
    TO_NUMBER(NULL) REC_TOTAL_LINES,
    TO_DATE(NULL) REC_RCVD_DATE,
    NULL REC_VENDOR_NUM,
    NULL REC_VENDOR,
    NULL REC_SUBINVENTORY_CODE,
    NULL CC_ORG,
    NULL CC_SUB_INV,
    NULL CC_ITEM_NUM,
    NULL CC_TRANSACTION_EMPLOYEE,
    to_date(NULL) cc_COUNT_DATE_CURRENT,
    FUSE.DESCRIPTION BC_TRANSACTION_EMPLOYEE,
    ORGS.ORGANIZATION_CODE BC_ORG,
    MIL.SUBINVENTORY_CODE BC_SUB_INV,
    MIL.SEGMENT1 BC_BIN,
    DECODE(MIL.ATTRIBUTE1, ''54'', ''RF Mobile Transaction'') BC_CREATED_VIA,
    TRUNC(MIL.CREATION_DATE) BC_BIN_CREATE_DATE,
    NULL BA_TRANSACTION_EMPLOYEE,
    NULL BA_ORG,
    NULL BA_ITEM_NUM,
    NULL BA_SUB_INV,
    NULL BA_BIN,
    NULL BA_CREATED_VIA,
    TO_DATE(NULL) BA_BIN_MAINT_DATE,
    NULL AB_TRANSACTION_EMPLOYEE,
    NULL AB_ITEM_NUM,
    NULL AB_DESCRIPTION,
    NULL AB_XREF_TYPE,
    NULL AB_XREF_VALUE,
    TO_NUMBER(NULL) AB_XREF_LENGTH,
    TO_DATE(NULL) AB_XREF_CREATE_DATE,
    ROUND((sysdate-MIL.CREATION_DATE),2) days
  FROM apps.MTL_ITEM_LOCATIONS mil,
    apps.mtl_parameters orgs,
    apps.fnd_user fuse
  WHERE orgs.organization_id = mil.organization_id
  AND fuse.user_id           = mil.created_by
  AND fuse.user_name        IN
    (SELECT DISTINCT (a.user_name)
    FROM apps.wf_local_roles rsub,
      apps.wf_user_role_assignments a
    WHERE rsub.parent_orig_system = a.role_orig_system
    AND rsub.name                 = a.role_name
    AND rsub.display_name LIKE ''HDS%Mobile%''
    )
  AND mil.subinventory_code NOT   IN (''Drop'',''Rental'')
  '||LC_ORG_COND||'
  '||lc_bin_create_date_cond||'
';


l_bin_assign_qry:='SELECT
    '||p_process_id ||' process_id,
    ''Bin Assign Data'' QUERY_TYPE,
    NULL REC_TRANSACTION_EMPLOYEE,
    NULL REC_ORG,
    NULL REC_TRANSACTION_TYPE_NAME,
    NULL REC_REASON_NAME,
    NULL REC_DESCRIPTION,
    NULL REC_DOC_NUM,
    TO_NUMBER(NULL) REC_TOTAL_LINES,
    TO_DATE(NULL) REC_RCVD_DATE,
    NULL REC_VENDOR_NUM,
    NULL REC_VENDOR,
    NULL REC_SUBINVENTORY_CODE,
    NULL CC_ORG,
    NULL CC_SUB_INV,
    NULL CC_ITEM_NUM,
    NULL CC_TRANSACTION_EMPLOYEE,
    TO_DATE(NULL) CC_COUNT_DATE_CURRENT,
    NULL BC_TRANSACTION_EMPLOYEE,
    NULL BC_ORG,
    NULL BC_SUB_INV,
    NULL BC_BIN,
    NULL BC_CREATED_VIA,
    TO_DATE(NULL) BC_BIN_CREATE_DATE,
    FUSE.DESCRIPTION BA_TRANSACTION_EMPLOYEE,
    ORGS.ORGANIZATION_CODE BA_ORG,
    MIB.SEGMENT1 BA_ITEM_NUM,
    MSLA.SUBINVENTORY_CODE BA_SUB_INV,
    MIL.SEGMENT1 BA_BIN,
    DECODE(MSLA.PROGRAM_ID, ''54'', ''RF Mobile Transaction'') BA_CREATED_VIA,
    TRUNC(MSLA.LAST_UPDATE_DATE) BA_BIN_MAINT_DATE,
    NULL AB_TRANSACTION_EMPLOYEE,
    NULL AB_ITEM_NUM,
    NULL AB_DESCRIPTION,
    NULL AB_XREF_TYPE,
    NULL AB_XREF_VALUE,
    TO_NUMBER(NULL) AB_XREF_LENGTH,
    TO_DATE(NULL) AB_XREF_CREATE_DATE,
    ROUND((sysdate-MSLA.LAST_UPDATE_DATE),2) days
  FROM apps.mtl_secondary_locators msla,
    apps.mtl_parameters orgs,
    apps.mtl_system_items_b mib,
    apps.mtl_item_locations mil,
    apps.fnd_user fuse
  WHERE orgs.organization_id = msla.organization_id
  AND orgs.organization_id   = mib.organization_id
  AND msla.inventory_item_id = mib.inventory_item_id
  AND msla.secondary_locator = mil.inventory_location_id
  AND fuse.user_id           = msla.created_by
  AND fuse.user_name        IN
    (SELECT DISTINCT (a.user_name)
    FROM apps.wf_local_roles rsub,
      apps.wf_user_role_assignments a
    WHERE rsub.parent_orig_system = a.role_orig_system
    AND rsub.name                 = a.role_name
    AND rsub.display_name LIKE ''HDS%Mobile%''
    )
  '||LC_ORG_COND||'
  '||lc_bin_assign_date_cond||'
';
  
  
 l_barcode_qry :=
    'SELECT '||p_process_id ||' process_id, 
    ''Barcode Data'' QUERY_TYPE,
    NULL REC_TRANSACTION_EMPLOYEE,
    NULL REC_ORG,
    NULL REC_TRANSACTION_TYPE_NAME,
    NULL REC_REASON_NAME,
    NULL REC_DESCRIPTION,
    NULL REC_DOC_NUM,
    TO_NUMBER(NULL) REC_TOTAL_LINES,
    TO_DATE(NULL) REC_RCVD_DATE,
    NULL REC_VENDOR_NUM,
    NULL REC_VENDOR,
    NULL REC_SUBINVENTORY_CODE,
    NULL CC_ORG,
    NULL CC_SUB_INV,
    NULL CC_ITEM_NUM,
    NULL CC_TRANSACTION_EMPLOYEE,
    TO_DATE(NULL) CC_COUNT_DATE_CURRENT,
    NULL BC_TRANSACTION_EMPLOYEE,
    NULL BC_ORG,
    NULL BC_SUB_INV,
    NULL BC_BIN,
    NULL BC_CREATED_VIA,
    TO_DATE(NULL) BC_BIN_CREATE_DATE,
    NULL BA_TRANSACTION_EMPLOYEE,
    NULL BA_ORG,
    NULL BA_ITEM_NUM,
    NULL BA_SUB_INV,
    NULL BA_BIN,
    NULL BA_CREATED_VIA,
    TO_DATE(NULL) BA_BIN_MAINT_DATE,
    FUSE.DESCRIPTION AB_TRANSACTION_EMPLOYEE,
    MIB.SEGMENT1 AB_ITEM_NUM,
    MIB.DESCRIPTION AB_DESCRIPTION,
    XREF.CROSS_REFERENCE_TYPE AB_XREF_TYPE,
    XREF.CROSS_REFERENCE AB_XREF_VALUE,
    LENGTH(XREF.CROSS_REFERENCE) AB_XREF_LENGTH,
    TRUNC(XREF.CREATION_DATE) AB_XREF_CREATE_DATE,
    ROUND((sysdate-XREF.CREATION_DATE),2) days
  FROM apps.MTL_CROSS_REFERENCES_B xref,
    apps.MTL_SYSTEM_ITEMS_B MIB,
    apps.mtl_parameters orgs,
    apps.fnd_user fuse
  WHERE mib.organization_id = orgs.organization_id
  AND mib.inventory_item_id = xref.inventory_item_id
  AND fuse.user_id          = xref.created_by
  AND fuse.user_name       IN
    (SELECT DISTINCT (a.user_name)
    FROM apps.wf_local_roles rsub,
      apps.wf_user_role_assignments a
    WHERE rsub.parent_orig_system = a.role_orig_system
    AND rsub.name                 = a.role_name
    AND rsub.display_name LIKE ''HDS%Mobile%''
    )
  AND XREF.CROSS_REFERENCE_TYPE  = ''PENDING''
  '||LC_ORG_COND||'
  '||lc_barcode_date_cond||'
';
 
 
  l_main_qry:= l_Receiving_Data_qry||chr(13)||chr(10)||'UNION ALL'||chr(13)||chr(10) ||l_Cycle_Count_qry||chr(13)||chr(10)||'UNION ALL'||chr(13)||chr(10)||l_bin_create_qry||chr(13)||chr(10)||'UNION ALL'||chr(13)||chr(10)||l_bin_assign_qry||chr(13)||chr(10)||'UNION ALL'||chr(13)||chr(10)||l_barcode_qry;

  fnd_file.put_line(fnd_file.log,'l_main_qry'||l_main_qry);

OPEN l_metric_rep_csr FOR l_main_qry ;
LOOP
  fetch l_metric_rep_csr bulk collect into rf_metric_rep_rec_tab limit 10000;
  fnd_file.put_line(fnd_file.log,'rf_metric_rep_rec_tab'||rf_metric_rep_rec_tab.count);
  
  IF rf_metric_rep_rec_tab.count >0 THEN
    FORALL j IN 1 .. rf_metric_rep_rec_tab.count
    INSERT INTO xxeis.XXWC_INV_RF_METRIC_RPT_TBL VALUES rf_metric_rep_rec_tab(j);
    COMMIT;
  END IF;
  IF l_metric_rep_csr%notfound THEN
    CLOSE l_metric_rep_csr;
    EXIT;
  end if;
END LOOP; 
  
EXCEPTION WHEN OTHERS THEN
 fnd_file.put_line(fnd_file.log,'THE ERROR IS'||sqlcode||sqlerrm);
end RF_METRIC_REP_PRC;

PROCEDURE CLEAR_TEMP_TABLES ( P_PROCESS_ID IN NUMBER)
--//============================================================================
--//
--// Object Name         :: clear_temp_tables
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This procedure will trigger in After Report level and delete all the records from the table based on that particular process_id.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--//  1.0     2-Aug-2016       		Siva   		 TMS#20160712-00024 
--//============================================================================    
AS
BEGIN  
  DELETE FROM XXEIS.XXWC_INV_RF_METRIC_RPT_TBL WHERE PROCESS_ID=P_PROCESS_ID;
  COMMIT;
END ;   

END EIS_RS_XXWC_RF_METRICS_PKG;
/
