--Report Name            : RF Metrics
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating View Data for RF Metrics
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_INV_RF_METRIC_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_INV_RF_METRIC_V',401,'','','','','SA059956','XXEIS','Eis Xxwc Inv Rf Metric V','EXIRMV','','');
--Delete View Columns for EIS_XXWC_INV_RF_METRIC_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_INV_RF_METRIC_V',401,FALSE);
--Inserting View Columns for EIS_XXWC_INV_RF_METRIC_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_RF_METRIC_V','DAYS',401,'Days','DAYS','','','','SA059956','NUMBER','','','Days','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_RF_METRIC_V','AB_XREF_CREATE_DATE',401,'Ab Xref Create Date','AB_XREF_CREATE_DATE','','','','SA059956','DATE','','','Ab Xref Create Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_RF_METRIC_V','AB_XREF_LENGTH',401,'Ab Xref Length','AB_XREF_LENGTH','','','','SA059956','NUMBER','','','Ab Xref Length','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_RF_METRIC_V','AB_XREF_VALUE',401,'Ab Xref Value','AB_XREF_VALUE','','','','SA059956','VARCHAR2','','','Ab Xref Value','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_RF_METRIC_V','AB_XREF_TYPE',401,'Ab Xref Type','AB_XREF_TYPE','','','','SA059956','VARCHAR2','','','Ab Xref Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_RF_METRIC_V','AB_DESCRIPTION',401,'Ab Description','AB_DESCRIPTION','','','','SA059956','VARCHAR2','','','Ab Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_RF_METRIC_V','AB_ITEM_NUM',401,'Ab Item Num','AB_ITEM_NUM','','','','SA059956','VARCHAR2','','','Ab Item Num','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_RF_METRIC_V','AB_TRANSACTION_EMPLOYEE',401,'Ab Transaction Employee','AB_TRANSACTION_EMPLOYEE','','','','SA059956','VARCHAR2','','','Ab Transaction Employee','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_RF_METRIC_V','BA_BIN_MAINT_DATE',401,'Ba Bin Maint Date','BA_BIN_MAINT_DATE','','','','SA059956','DATE','','','Ba Bin Maint Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_RF_METRIC_V','BA_CREATED_VIA',401,'Ba Created Via','BA_CREATED_VIA','','','','SA059956','VARCHAR2','','','Ba Created Via','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_RF_METRIC_V','BA_BIN',401,'Ba Bin','BA_BIN','','','','SA059956','VARCHAR2','','','Ba Bin','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_RF_METRIC_V','BA_SUB_INV',401,'Ba Sub Inv','BA_SUB_INV','','','','SA059956','VARCHAR2','','','Ba Sub Inv','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_RF_METRIC_V','BA_ITEM_NUM',401,'Ba Item Num','BA_ITEM_NUM','','','','SA059956','VARCHAR2','','','Ba Item Num','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_RF_METRIC_V','BA_ORG',401,'Ba Org','BA_ORG','','','','SA059956','VARCHAR2','','','Ba Org','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_RF_METRIC_V','BA_TRANSACTION_EMPLOYEE',401,'Ba Transaction Employee','BA_TRANSACTION_EMPLOYEE','','','','SA059956','VARCHAR2','','','Ba Transaction Employee','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_RF_METRIC_V','BC_BIN_CREATE_DATE',401,'Bc Bin Create Date','BC_BIN_CREATE_DATE','','','','SA059956','DATE','','','Bc Bin Create Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_RF_METRIC_V','BC_CREATED_VIA',401,'Bc Created Via','BC_CREATED_VIA','','','','SA059956','VARCHAR2','','','Bc Created Via','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_RF_METRIC_V','BC_BIN',401,'Bc Bin','BC_BIN','','','','SA059956','VARCHAR2','','','Bc Bin','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_RF_METRIC_V','BC_SUB_INV',401,'Bc Sub Inv','BC_SUB_INV','','','','SA059956','VARCHAR2','','','Bc Sub Inv','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_RF_METRIC_V','BC_ORG',401,'Bc Org','BC_ORG','','','','SA059956','VARCHAR2','','','Bc Org','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_RF_METRIC_V','BC_TRANSACTION_EMPLOYEE',401,'Bc Transaction Employee','BC_TRANSACTION_EMPLOYEE','','','','SA059956','VARCHAR2','','','Bc Transaction Employee','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_RF_METRIC_V','CC_COUNT_DATE_CURRENT',401,'Cc Count Date Current','CC_COUNT_DATE_CURRENT','','','','SA059956','DATE','','','Cc Count Date Current','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_RF_METRIC_V','CC_TRANSACTION_EMPLOYEE',401,'Cc Transaction Employee','CC_TRANSACTION_EMPLOYEE','','','','SA059956','VARCHAR2','','','Cc Transaction Employee','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_RF_METRIC_V','CC_ITEM_NUM',401,'Cc Item Num','CC_ITEM_NUM','','','','SA059956','VARCHAR2','','','Cc Item Num','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_RF_METRIC_V','CC_SUB_INV',401,'Cc Sub Inv','CC_SUB_INV','','','','SA059956','VARCHAR2','','','Cc Sub Inv','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_RF_METRIC_V','CC_ORG',401,'Cc Org','CC_ORG','','','','SA059956','VARCHAR2','','','Cc Org','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_RF_METRIC_V','REC_SUBINVENTORY_CODE',401,'Rec Subinventory Code','REC_SUBINVENTORY_CODE','','','','SA059956','VARCHAR2','','','Rec Subinventory Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_RF_METRIC_V','REC_VENDOR',401,'Rec Vendor','REC_VENDOR','','','','SA059956','VARCHAR2','','','Rec Vendor','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_RF_METRIC_V','REC_VENDOR_NUM',401,'Rec Vendor Num','REC_VENDOR_NUM','','','','SA059956','VARCHAR2','','','Rec Vendor Num','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_RF_METRIC_V','REC_RCVD_DATE',401,'Rec Rcvd Date','REC_RCVD_DATE','','','','SA059956','DATE','','','Rec Rcvd Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_RF_METRIC_V','REC_TOTAL_LINES',401,'Rec Total Lines','REC_TOTAL_LINES','','','','SA059956','NUMBER','','','Rec Total Lines','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_RF_METRIC_V','REC_DOC_NUM',401,'Rec Doc Num','REC_DOC_NUM','','','','SA059956','VARCHAR2','','','Rec Doc Num','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_RF_METRIC_V','REC_DESCRIPTION',401,'Rec Description','REC_DESCRIPTION','','','','SA059956','VARCHAR2','','','Rec Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_RF_METRIC_V','REC_REASON_NAME',401,'Rec Reason Name','REC_REASON_NAME','','','','SA059956','VARCHAR2','','','Rec Reason Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_RF_METRIC_V','REC_TRANSACTION_TYPE_NAME',401,'Rec Transaction Type Name','REC_TRANSACTION_TYPE_NAME','','','','SA059956','VARCHAR2','','','Rec Transaction Type Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_RF_METRIC_V','REC_ORG',401,'Rec Org','REC_ORG','','','','SA059956','VARCHAR2','','','Rec Org','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_RF_METRIC_V','REC_TRANSACTION_EMPLOYEE',401,'Rec Transaction Employee','REC_TRANSACTION_EMPLOYEE','','','','SA059956','VARCHAR2','','','Rec Transaction Employee','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_RF_METRIC_V','QUERY_TYPE',401,'Query Type','QUERY_TYPE','','','','SA059956','VARCHAR2','','','Query Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_RF_METRIC_V','PROCESS_ID',401,'Process Id','PROCESS_ID','','','','SA059956','NUMBER','','','Process Id','','','');
--Inserting View Components for EIS_XXWC_INV_RF_METRIC_V
--Inserting View Component Joins for EIS_XXWC_INV_RF_METRIC_V
END;
/
set scan on define on
prompt Creating Report LOV Data for RF Metrics
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - RF Metrics
xxeis.eis_rs_ins.lov( 401,'SELECT organization_code code,organization_name name
FROM ORG_ORGANIZATION_DEFINITIONS OOD
WHERE EXISTS
  (SELECT 1
  FROM XXEIS.EIS_ORG_ACCESS_V
  WHERE organization_id = ood.organization_id
  )
ORDER BY organization_code','','XXWC INV ORGANIZATIONS LOV','List of All Inventory Orgs under a given operating unit.','ANONYMOUS',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for RF Metrics
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - RF Metrics
xxeis.eis_rs_utility.delete_report_rows( 'RF Metrics' );
--Inserting Report - RF Metrics
xxeis.eis_rs_ins.r( 401,'RF Metrics','','Create new RF Metrics for tracking adoption related to Receiving, Bin Creation, bin Assignment Data, Cycle Count Data, and New Barcode Entry Data','','','','SA059956','EIS_XXWC_INV_RF_METRIC_V','Y','','','SA059956','','N','White Cap Reports','','Pivot Excel,','N');
--Inserting Report Columns - RF Metrics
xxeis.eis_rs_ins.rc( 'RF Metrics',401,'AB_DESCRIPTION','Barcode Description','Ab Description','','','default','','29','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_RF_METRIC_V','','');
xxeis.eis_rs_ins.rc( 'RF Metrics',401,'AB_ITEM_NUM','Barcode Item Num','Ab Item Num','','','default','','28','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_RF_METRIC_V','','');
xxeis.eis_rs_ins.rc( 'RF Metrics',401,'AB_TRANSACTION_EMPLOYEE','Barcode Transaction Employee','Ab Transaction Employee','','','default','','27','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_RF_METRIC_V','','');
xxeis.eis_rs_ins.rc( 'RF Metrics',401,'AB_XREF_CREATE_DATE','Xref Create Date','Ab Xref Create Date','','','default','','33','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_RF_METRIC_V','','');
xxeis.eis_rs_ins.rc( 'RF Metrics',401,'AB_XREF_LENGTH','Xref Length','Ab Xref Length','','~~~','default','','32','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_RF_METRIC_V','','');
xxeis.eis_rs_ins.rc( 'RF Metrics',401,'AB_XREF_TYPE','Xref Type','Ab Xref Type','','','default','','30','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_RF_METRIC_V','','');
xxeis.eis_rs_ins.rc( 'RF Metrics',401,'AB_XREF_VALUE','Xref Value','Ab Xref Value','','','default','','31','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_RF_METRIC_V','','');
xxeis.eis_rs_ins.rc( 'RF Metrics',401,'BA_BIN','Bin Assign Bin','Ba Bin','','','default','','24','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_RF_METRIC_V','','');
xxeis.eis_rs_ins.rc( 'RF Metrics',401,'BA_BIN_MAINT_DATE','Bin Assign Maint Date','Ba Bin Maint Date','','','default','','25','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_RF_METRIC_V','','');
xxeis.eis_rs_ins.rc( 'RF Metrics',401,'BA_CREATED_VIA','Bin Assign Created Via','Ba Created Via','','','default','','26','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_RF_METRIC_V','','');
xxeis.eis_rs_ins.rc( 'RF Metrics',401,'BA_ITEM_NUM','Bin Assign Item Num','Ba Item Num','','','default','','22','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_RF_METRIC_V','','');
xxeis.eis_rs_ins.rc( 'RF Metrics',401,'BA_ORG','Bin Assign Org','Ba Org','','','default','','21','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_RF_METRIC_V','','');
xxeis.eis_rs_ins.rc( 'RF Metrics',401,'BA_SUB_INV','Bin Assign Subinventory Code','Ba Sub Inv','','','default','','23','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_RF_METRIC_V','','');
xxeis.eis_rs_ins.rc( 'RF Metrics',401,'BA_TRANSACTION_EMPLOYEE','Bin Assign Transaction Employee','Ba Transaction Employee','','','default','','20','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_RF_METRIC_V','','');
xxeis.eis_rs_ins.rc( 'RF Metrics',401,'BC_BIN','Bin Create Bin','Bc Bin','','','default','','17','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_RF_METRIC_V','','');
xxeis.eis_rs_ins.rc( 'RF Metrics',401,'BC_BIN_CREATE_DATE','Bin Create Date','Bc Bin Create Date','','','default','','19','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_RF_METRIC_V','','');
xxeis.eis_rs_ins.rc( 'RF Metrics',401,'BC_CREATED_VIA','Bin Created Via','Bc Created Via','','','default','','18','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_RF_METRIC_V','','');
xxeis.eis_rs_ins.rc( 'RF Metrics',401,'BC_ORG','Bin Create Org','Bc Org','','','default','','15','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_RF_METRIC_V','','');
xxeis.eis_rs_ins.rc( 'RF Metrics',401,'BC_SUB_INV','Bin Create Subinventory Code','Bc Sub Inv','','','default','','16','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_RF_METRIC_V','','');
xxeis.eis_rs_ins.rc( 'RF Metrics',401,'BC_TRANSACTION_EMPLOYEE','Bin Create Transaction Employee','Bc Transaction Employee','','','default','','14','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_RF_METRIC_V','','');
xxeis.eis_rs_ins.rc( 'RF Metrics',401,'CC_COUNT_DATE_CURRENT','Count Date Current','Cc Count Date Current','','','default','','38','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_RF_METRIC_V','','');
xxeis.eis_rs_ins.rc( 'RF Metrics',401,'CC_ITEM_NUM','Cycle Count Item Num','Cc Item Num','','','default','','36','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_RF_METRIC_V','','');
xxeis.eis_rs_ins.rc( 'RF Metrics',401,'CC_ORG','Cycle Count Org','Cc Org','','','default','','34','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_RF_METRIC_V','','');
xxeis.eis_rs_ins.rc( 'RF Metrics',401,'CC_SUB_INV','Cycle Count Subinventory Code','Cc Sub Inv','','','default','','35','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_RF_METRIC_V','','');
xxeis.eis_rs_ins.rc( 'RF Metrics',401,'CC_TRANSACTION_EMPLOYEE','Cycle Count Transaction Employee','Cc Transaction Employee','','','default','','37','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_RF_METRIC_V','','');
xxeis.eis_rs_ins.rc( 'RF Metrics',401,'DAYS','Days','Days','','~~~','default','','2','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_RF_METRIC_V','','');
xxeis.eis_rs_ins.rc( 'RF Metrics',401,'QUERY_TYPE','Query Type','Query Type','','','default','','1','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_RF_METRIC_V','','');
xxeis.eis_rs_ins.rc( 'RF Metrics',401,'REC_DESCRIPTION','Rcvd Description','Rec Description','','','default','','7','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_RF_METRIC_V','','');
xxeis.eis_rs_ins.rc( 'RF Metrics',401,'REC_DOC_NUM','Doc Num','Rec Doc Num','','','default','','8','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_RF_METRIC_V','','');
xxeis.eis_rs_ins.rc( 'RF Metrics',401,'REC_ORG','Rcvd Org','Rec Org','','','default','','4','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_RF_METRIC_V','','');
xxeis.eis_rs_ins.rc( 'RF Metrics',401,'REC_RCVD_DATE','Rcvd Date','Rec Rcvd Date','','','default','','10','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_RF_METRIC_V','','');
xxeis.eis_rs_ins.rc( 'RF Metrics',401,'REC_REASON_NAME','Reason Name','Rec Reason Name','','','default','','6','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_RF_METRIC_V','','');
xxeis.eis_rs_ins.rc( 'RF Metrics',401,'REC_SUBINVENTORY_CODE','Subinventory Code','Rec Subinventory Code','','','default','','13','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_RF_METRIC_V','','');
xxeis.eis_rs_ins.rc( 'RF Metrics',401,'REC_TOTAL_LINES','Total Lines','Rec Total Lines','','~~~','default','','9','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_RF_METRIC_V','','');
xxeis.eis_rs_ins.rc( 'RF Metrics',401,'REC_TRANSACTION_EMPLOYEE','Rcvd Transaction Employee','Rec Transaction Employee','','','default','','3','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_RF_METRIC_V','','');
xxeis.eis_rs_ins.rc( 'RF Metrics',401,'REC_TRANSACTION_TYPE_NAME','Rcvd Transaction Type Name','Rec Transaction Type Name','','','default','','5','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_RF_METRIC_V','','');
xxeis.eis_rs_ins.rc( 'RF Metrics',401,'REC_VENDOR','Vendor','Rec Vendor','','','default','','12','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_RF_METRIC_V','','');
xxeis.eis_rs_ins.rc( 'RF Metrics',401,'REC_VENDOR_NUM','Vendor Num','Rec Vendor Num','','','default','','11','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_RF_METRIC_V','','');
--Inserting Report Parameters - RF Metrics
xxeis.eis_rs_ins.rp( 'RF Metrics',401,'Date From','Date From','','>=','','','DATE','Y','Y','2','','N','CONSTANT','SA059956','Y','N','','Start Date','');
xxeis.eis_rs_ins.rp( 'RF Metrics',401,'Org','Org','','IN','XXWC INV ORGANIZATIONS LOV','','VARCHAR2','Y','Y','1','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'RF Metrics',401,'Date To','Date To','','<=','','','DATE','Y','Y','3','','N','CONSTANT','SA059956','Y','N','','End Date','');
--Inserting Report Conditions - RF Metrics
xxeis.eis_rs_ins.rcn( 'RF Metrics',401,'','','','','and PROCESS_ID = :SYSTEM.PROCESS_ID','Y','1','','SA059956');
--Inserting Report Sorts - RF Metrics
xxeis.eis_rs_ins.rs( 'RF Metrics',401,'REC_RCVD_DATE','ASC','SA059956','1','');
xxeis.eis_rs_ins.rs( 'RF Metrics',401,'REC_TRANSACTION_EMPLOYEE','ASC','SA059956','2','');
xxeis.eis_rs_ins.rs( 'RF Metrics',401,'REC_TRANSACTION_TYPE_NAME','ASC','SA059956','3','');
xxeis.eis_rs_ins.rs( 'RF Metrics',401,'REC_DESCRIPTION','ASC','SA059956','4','');
xxeis.eis_rs_ins.rs( 'RF Metrics',401,'CC_ITEM_NUM','ASC','SA059956','5','');
xxeis.eis_rs_ins.rs( 'RF Metrics',401,'CC_COUNT_DATE_CURRENT','ASC','SA059956','6','');
--Inserting Report Triggers - RF Metrics
xxeis.eis_rs_ins.rt( 'RF Metrics',401,'begin
XXEIS.EIS_RS_XXWC_RF_METRICS_PKG.RF_METRIC_REP_PRC(
p_process_id       => :SYSTEM.PROCESS_ID,
P_DATE_FROM    => :Date From,
P_DATE_TO        => :Date To,
p_Org     	         => :Org
);
end;','B','Y','SA059956');
xxeis.eis_rs_ins.rt( 'RF Metrics',401,'begin
XXEIS.EIS_RS_XXWC_RF_METRICS_PKG.CLEAR_TEMP_TABLES(
p_process_id =>:SYSTEM.PROCESS_ID);
end;','A','Y','SA059956');
--Inserting Report Templates - RF Metrics
--Inserting Report Portals - RF Metrics
--Inserting Report Dashboards - RF Metrics
--Inserting Report Security - RF Metrics
xxeis.eis_rs_ins.rsec( 'RF Metrics','401','','51973',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'RF Metrics','401','','50884',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'RF Metrics','401','','50855',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'RF Metrics','401','','50981',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'RF Metrics','401','','50882',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'RF Metrics','401','','50883',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'RF Metrics','401','','51004',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'RF Metrics','401','','50619',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'RF Metrics','401','','52150',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'RF Metrics','401','','51491',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'RF Metrics','401','','50867',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'RF Metrics','20005','','50900',401,'SA059956','','');
--Inserting Report Pivots - RF Metrics
xxeis.eis_rs_ins.rpivot( 'RF Metrics',401,'Receiving Data','1','0,0|0,1,0','1,1,0,0|PivotStyleLight22|2');
--Inserting Report Pivot Details For Pivot - Receiving Data
xxeis.eis_rs_ins.rpivot_dtls( 'RF Metrics',401,'Receiving Data','REC_DESCRIPTION','ROW_FIELD','','','5','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'RF Metrics',401,'Receiving Data','REC_DOC_NUM','ROW_FIELD','','','6','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'RF Metrics',401,'Receiving Data','REC_ORG','ROW_FIELD','','','2','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'RF Metrics',401,'Receiving Data','REC_RCVD_DATE','ROW_FIELD','','','8','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'RF Metrics',401,'Receiving Data','REC_REASON_NAME','ROW_FIELD','','','4','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'RF Metrics',401,'Receiving Data','REC_SUBINVENTORY_CODE','ROW_FIELD','','','11','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'RF Metrics',401,'Receiving Data','REC_TOTAL_LINES','ROW_FIELD','','','7','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'RF Metrics',401,'Receiving Data','REC_TRANSACTION_EMPLOYEE','ROW_FIELD','','','1','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'RF Metrics',401,'Receiving Data','REC_TRANSACTION_TYPE_NAME','ROW_FIELD','','','3','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'RF Metrics',401,'Receiving Data','REC_VENDOR','ROW_FIELD','','','10','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'RF Metrics',401,'Receiving Data','REC_VENDOR_NUM','ROW_FIELD','','','9','1','');
--Inserting Report Summary Calculation Columns For Pivot- Receiving Data
xxeis.eis_rs_ins.rpivot( 'RF Metrics',401,'Bin Create Data','2','0,0|0,1,0','1,1,0,0|PivotStyleLight22|2');
--Inserting Report Pivot Details For Pivot - Bin Create Data
xxeis.eis_rs_ins.rpivot_dtls( 'RF Metrics',401,'Bin Create Data','BC_BIN','ROW_FIELD','','','4','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'RF Metrics',401,'Bin Create Data','BC_BIN_CREATE_DATE','ROW_FIELD','','','6','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'RF Metrics',401,'Bin Create Data','BC_CREATED_VIA','ROW_FIELD','','','5','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'RF Metrics',401,'Bin Create Data','BC_ORG','ROW_FIELD','','','2','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'RF Metrics',401,'Bin Create Data','BC_SUB_INV','ROW_FIELD','','','3','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'RF Metrics',401,'Bin Create Data','BC_TRANSACTION_EMPLOYEE','ROW_FIELD','','','1','1','');
--Inserting Report Summary Calculation Columns For Pivot- Bin Create Data
xxeis.eis_rs_ins.rpivot( 'RF Metrics',401,'Bin Assign Data','3','0,0|0,1,0','0,0,0,0|PivotStyleLight22|2');
--Inserting Report Pivot Details For Pivot - Bin Assign Data
xxeis.eis_rs_ins.rpivot_dtls( 'RF Metrics',401,'Bin Assign Data','BA_BIN','ROW_FIELD','','','5','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'RF Metrics',401,'Bin Assign Data','BA_BIN_MAINT_DATE','ROW_FIELD','','','7','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'RF Metrics',401,'Bin Assign Data','BA_CREATED_VIA','ROW_FIELD','','','6','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'RF Metrics',401,'Bin Assign Data','BA_ITEM_NUM','ROW_FIELD','','','3','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'RF Metrics',401,'Bin Assign Data','BA_ORG','ROW_FIELD','','','2','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'RF Metrics',401,'Bin Assign Data','BA_SUB_INV','ROW_FIELD','','','4','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'RF Metrics',401,'Bin Assign Data','BA_TRANSACTION_EMPLOYEE','ROW_FIELD','','','1','1','');
--Inserting Report Summary Calculation Columns For Pivot- Bin Assign Data
xxeis.eis_rs_ins.rpivot( 'RF Metrics',401,'Barcode Data','4','0,0|0,1,0','0,0,0,0|PivotStyleLight22|2');
--Inserting Report Pivot Details For Pivot - Barcode Data
xxeis.eis_rs_ins.rpivot_dtls( 'RF Metrics',401,'Barcode Data','AB_DESCRIPTION','ROW_FIELD','','','3','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'RF Metrics',401,'Barcode Data','AB_ITEM_NUM','ROW_FIELD','','','2','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'RF Metrics',401,'Barcode Data','AB_TRANSACTION_EMPLOYEE','ROW_FIELD','','','1','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'RF Metrics',401,'Barcode Data','AB_XREF_CREATE_DATE','ROW_FIELD','','','7','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'RF Metrics',401,'Barcode Data','AB_XREF_LENGTH','ROW_FIELD','','','6','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'RF Metrics',401,'Barcode Data','AB_XREF_TYPE','ROW_FIELD','','','4','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'RF Metrics',401,'Barcode Data','AB_XREF_VALUE','ROW_FIELD','','','5','1','');
--Inserting Report Summary Calculation Columns For Pivot- Barcode Data
xxeis.eis_rs_ins.rpivot( 'RF Metrics',401,'Cycle Count Data','5','0,0|0,1,0','0,1,0,0|PivotStyleLight22|2');
--Inserting Report Pivot Details For Pivot - Cycle Count Data
xxeis.eis_rs_ins.rpivot_dtls( 'RF Metrics',401,'Cycle Count Data','CC_COUNT_DATE_CURRENT','ROW_FIELD','','','5','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'RF Metrics',401,'Cycle Count Data','CC_ITEM_NUM','ROW_FIELD','','','3','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'RF Metrics',401,'Cycle Count Data','CC_ORG','ROW_FIELD','','','1','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'RF Metrics',401,'Cycle Count Data','CC_SUB_INV','ROW_FIELD','','','2','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'RF Metrics',401,'Cycle Count Data','CC_TRANSACTION_EMPLOYEE','ROW_FIELD','','','4','1','');
--Inserting Report Summary Calculation Columns For Pivot- Cycle Count Data
END;
/
set scan on define on
