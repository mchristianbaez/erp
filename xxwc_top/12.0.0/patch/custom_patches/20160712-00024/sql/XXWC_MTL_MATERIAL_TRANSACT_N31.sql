-----------------------------------------------------------------------------------------------
/*************************************************************************************************************************
  $Header XXWC_MTL_MATERIAL_TRANSACT_N31
  File Name: XXWC_MTL_MATERIAL_TRANSACT_N31.sql
  PURPOSE:   
  REVISIONS:
     Ver        Date         Author         Description
     ---------  ----------   --------    -----------------------------------------------------------------------------------
      1.0      02-Aug-16      Siva            TMS#20160712-00024 --RF Metrics Report
****************************************************************************************************************************/
CREATE INDEX INV.XXWC_MTL_MATERIAL_TRANSACT_N31 ON INV.MTL_MATERIAL_TRANSACTIONS (TRUNC(CREATION_DATE)) TABLESPACE APPS_TS_TX_DATA
/
