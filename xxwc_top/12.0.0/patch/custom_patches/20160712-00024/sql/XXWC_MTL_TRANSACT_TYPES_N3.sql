-----------------------------------------------------------------------------------------------
/*************************************************************************************************************************
  $Header XXWC_MTL_TRANSACT_TYPES_N3
  File Name: XXWC_MTL_TRANSACT_TYPES_N3.sql
  PURPOSE:   
  REVISIONS:
     Ver        Date         Author         Description
     ---------  ----------   --------    -----------------------------------------------------------------------------------
      1.0      02-Aug-16    Siva            TMS#20160712-00024 --RF Metrics Report
****************************************************************************************************************************/
CREATE INDEX INV.XXWC_MTL_TRANSACT_TYPES_N3 ON INV.MTL_TRANSACTION_TYPES (TRANSACTION_TYPE_NAME) TABLESPACE APPS_TS_TX_DATA
/
