CREATE OR REPLACE PACKAGE  XXEIS.EIS_RS_XXWC_RF_METRICS_PKG AUTHID CURRENT_USER
--//============================================================================
--//  
--// Object Name         		:: xxeis.EIS_RS_XXWC_RF_METRICS_PKG
--//
--// Object Type         		:: Package Specification
--//
--// Object Description  		:: This Package will trigger in before report and insert the values into custom Table
--//
--// Version Control
--//============================================================================
--// Vers    	Author           Date          Description
--//----------------------------------------------------------------------------
--//  1.0     2-Aug-2016       		Siva   		 TMS#20160712-00024 
--//============================================================================
IS
   PROCEDURE RF_METRIC_REP_PRC (P_PROCESS_ID       IN NUMBER
                                ,P_DATE_FROM       IN DATE
                                ,P_DATE_TO         IN DATE
                                ,p_Org             IN VARCHAR2
                                );
--//============================================================================
--//
--// Object Name         :: RF_METRIC_REP_PRC
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This procedure gets the record based on parameter and insert into table
--//
--// Version Control
--//============================================================================
--// Vers    Author             Date            Description
--//----------------------------------------------------------------------------
--//  1.0     2-Aug-2016       		Siva   		 TMS#20160712-00024 
--//============================================================================	
 PROCEDURE CLEAR_TEMP_TABLES ( P_PROCESS_ID IN NUMBER);
--//============================================================================
--//
--// Object Name         :: clear_temp_tables
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This procedure will trigger in After Report level and delete all the records from the table based on that particular process_id.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--//  1.0     2-Aug-2016       		Siva   		 TMS#20160712-00024 
--//============================================================================    

END EIS_RS_XXWC_RF_METRICS_PKG;
/
