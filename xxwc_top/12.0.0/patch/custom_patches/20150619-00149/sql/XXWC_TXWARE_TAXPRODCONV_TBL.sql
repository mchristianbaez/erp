   /* ************************************************************************
    HD Supply
    All rights reserved.
   **************************************************************************
     PURPOSE: Audit inserts or updates to specific taxware related tables.
     REVISIONS:
     Ticket           Ver         Date         Author                     Description
     ---------        ----------  ----------   ------------------------   -------------------------
     20150619-00149   1.0        6/19/2015    Bala Seshadri               1. Created    
   ************************************************************************* */
CREATE TABLE XXWC.XXWC_TXWARE_TAXPRODCONV
(
  MERCHANTID     VARCHAR2(20 BYTE),
  BUSNLOCN       VARCHAR2(13 BYTE),
  USERPRODCODE1  VARCHAR2(25 BYTE),
  USERPRODCODE2  VARCHAR2(25 BYTE),
  TWIPRODCODE    VARCHAR2(9 BYTE)               NOT NULL
)
TABLESPACE XXWC_DATA
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;
COMMENT ON TABLE XXWC.XXWC_TXWARE_TAXPRODCONV IS 'TMS: 20150619-00149';