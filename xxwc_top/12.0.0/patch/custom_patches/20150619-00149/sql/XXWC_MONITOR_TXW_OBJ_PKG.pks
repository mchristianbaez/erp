CREATE OR REPLACE PACKAGE APPS.XXWC_MONITOR_TXW_OBJ_PKG
AS
   /* ************************************************************************
    HD Supply
    All rights reserved.
   **************************************************************************
     PURPOSE: Audit inserts or updates to specific taxware related tables.
     REVISIONS:
     Ticket           Ver         Date         Author                     Description
     ---------        ----------  ----------   ------------------------   -------------------------
     20150619-00149   1.0        6/19/2015    Bala Seshadri               1. Created
   **************************************************************************** */
   --
   procedure flush_txw_prod_conv
     (
       retcode out varchar2
      ,errbuf  out varchar2                      
     );    
     --     
  procedure uc4_wrapper
   (  
      p_user_name            in  varchar2
    , p_responsibility_name  in  varchar2
   );                            
END XXWC_MONITOR_TXW_OBJ_PKG;
/