-- -----------------------------------------------------------------------------
      --
      -- {Start Of Comments}
      --
      -- Description:
      --   This is a script to descrease length for productentries field
      /**********************************************************************************************
      File Name: APPS.XXWC_TAX_BATCHTOSTEP

      PROGRAM TYPE: PACKAGE BODY

          HISTORY
         =============================================================================
      VERSION DATE          AUTHOR(S)       DESCRIPTION
      ------- -----------   --------------- ----------------------------------------
      1.0    9-Sept-2016   Neha Saini    TMS# 20160315-00152 descrease length for productentries

     ***********************************************************************************************/
ALTER TABLE 
  TAXWARE.STEPPROD_TBL
MODIFY 
   ( 
   PRODUCTENTRIES varchar2(500 BYTE) 
   );