/*************************************************************************
  $Header TMS_20180111-00297_Cleanup_activity_for_OM_workflow.sql $
  Module Name: 20180111-00297   @SF Cleanup activity for order management workflow as per Workflow Analyzer
  
  PURPOSE: Data fix to Cleanup activity for order management workflow as per Workflow Analyzer

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        02-Jan-2016  Rakesh Patel          TMS#20180111-00297 Workflow Purge Activities as per Workflow Analyzer
**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 100000;
SET VERIFY OFF;

DECLARE
  l_item_type     VARCHAR2(30) := 'OEOL';
  l_hdr_item_type VARCHAR2(30) := 'OEOH';

  CURSOR header_items
  IS
    SELECT item_key,
      parent_item_key,
      root_activity
    FROM wf_items wfi
    WHERE item_type = 'OEOH'
    AND end_date   IS NULL
    AND NOT EXISTS
      (SELECT 1
      FROM oe_order_headers_all
      WHERE header_id = TO_NUMBER(wfi.item_key)
      );
  CURSOR line_items
  IS
    SELECT item_key,
      parent_item_key,
      root_activity
    FROM wf_items wfi
    WHERE item_type = 'OEOL'
    AND end_date   IS NULL
    AND NOT EXISTS
      ( SELECT 1 FROM oe_order_lines_all WHERE line_id = TO_NUMBER(wfi.item_key)
      );
BEGIN
  DBMS_OUTPUT.ENABLE(10000000);
  FOR hdr IN header_items
  LOOP
    /* header_items */
    wf_engine.abortprocess(itemtype => l_hdr_item_type, itemkey => hdr.item_key, process => hdr.root_activity);
  END LOOP;
  /* header_items */

  FOR lin IN line_items
  LOOP
    /* line_items */
    wf_engine.abortprocess(itemtype => l_item_type, itemkey => lin.item_key, process => lin.root_activity);
  END LOOP;
  /* line_items */
 COMMIT;
EXCEPTION
WHEN OTHERS THEN
  ROLLBACK;
  dbms_output.put_line(SUBSTR(SQLERRM, 1, 240));
END;
/
