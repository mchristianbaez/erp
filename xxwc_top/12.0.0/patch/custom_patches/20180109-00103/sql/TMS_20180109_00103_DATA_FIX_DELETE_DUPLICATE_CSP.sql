/******************************************************************************
   NAME:       TMS_20180109_00103_DATA_FIX_DELETE_DUPLICATE_CSP.sql
   PURPOSE:    Delete IN PROGRESS records of csp which are duplicate
   
   REVISIONS:
   Ver        Date        Author               Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        16/01/2018  Niraj K Ranjan   TMS#220180109-00103 OLSEN CSP
******************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 220180109-00103   , Before Update');

   DELETE FROM apps.xxwc_om_csp_notifications_tbl WHERE ROWID IN
     (SELECT aa.rowid FROM 
      apps.xxwc_om_csp_notifications_tbl aa,
      (SELECT MAX(submitted_date) submitted_date,agreement_id,revision_number 
      FROM apps.xxwc_om_csp_notifications_tbl
      WHERE 1=1
	  AND  AGREEMENT_ID = 10387
      AND  agreement_status = 'IN PROGRESS'
      GROUP BY agreement_id,revision_number
      HAVING COUNT(1) > 1) bb
      WHERE aa.agreement_id = bb.agreement_id
      AND aa.revision_number = bb.revision_number
      AND aa.submitted_date <> bb.submitted_date);
   DBMS_OUTPUT.put_line ('TMS: 220180109-00103    , Total records deleted: '|| SQL%ROWCOUNT);
   UPDATE XXWC_OM_CSP_NOTIFICATIONS_TBL xocn
   SET  agreement_status = 'AWAITING_APPROVAL' 
   WHERE 1=1
   AND   xocn.agreement_id = 10387
   AND   xocn.revision_number = 54
   AND   agreement_status IN ('IN PROGRESS');
   DBMS_OUTPUT.put_line ('TMS: 220180109-00103    , Total records Updated for agreement id 10387: '|| SQL%ROWCOUNT);

   COMMIT;
   DBMS_OUTPUT.put_line ('TMS: 220180109-00103    , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 220180109-00103 , Errors : ' || SQLERRM);
END;
/
