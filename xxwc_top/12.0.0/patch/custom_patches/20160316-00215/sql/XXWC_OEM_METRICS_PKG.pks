CREATE OR REPLACE PACKAGE APPS.XXWC_OEM_METRICS_PKG
/******************************************************************************************************
-- File Name: XXWC_OEM_METRICS_PKG.pks
--
-- PROGRAM TYPE: PL/SQL Script   <API>
--
-- PURPOSE: Developed to hold procedures supporting OEM metrics and alerting
-- HISTORY
-- ==========================================================================================================
-- ==========================================================================================================
-- VERSION DATE          AUTHOR(S)       DESCRIPTION
-- ------- -----------   --------------- --------------------------------------------------------------------
-- 1.0     17-MAR-2016   Andre Rivas     TMS#20160316-00215 - Create Job to reset CP statistics daily
************************************************************************************************************/
IS

   PROCEDURE RESET_CONC_PROG_ONSITE (errbuf OUT VARCHAR2, retcode OUT NUMBER);
                                    
END XXWC_OEM_METRICS_PKG;
/
