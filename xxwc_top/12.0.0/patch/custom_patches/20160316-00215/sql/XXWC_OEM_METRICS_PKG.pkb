CREATE OR REPLACE PACKAGE BODY APPS.XXWC_OEM_METRICS_PKG
/******************************************************************************************************
-- File Name: XXWC_OEM_METRICS_PKG.pkb
--
-- PROGRAM TYPE: PL/SQL Script   <API>
--
-- PURPOSE: Developed to hold procedures supporting OEM metrics and alerting
-- HISTORY
-- ==========================================================================================================
-- ==========================================================================================================
-- VERSION DATE          AUTHOR(S)       DESCRIPTION
-- ------- -----------   --------------- --------------------------------------------------------------------
-- 1.0     17-MAR-2016   Andre Rivas     TMS#20160316-00215 - Create Job to reset CP statistics daily
************************************************************************************************************/
IS
   g_err_callfrom    VARCHAR2 (100) := 'XXWC_OEM_METRICS_PKG';
   g_distro_list     VARCHAR2 (100) := 'HDSOracleDevelopers@hdsupply.com';


  
   /******************************************************************************************************
    -- Procedure Name: RESET_CONC_PROG_ONSITE
    --
    -- PURPOSE: This procedure will be called by the concurrent program executable XXWC_RESET_CONC_PROG_ONSITE 
    -- and the concurrent program "XXWC Reset Concurrent Program Statistics Table" (XXWC_RESET_CONC_PROG_ONSITE)
    -- Request groups: System Administrator Reports, XXWC Sys Administrator Reports, XXCUS Request Group
    -- HISTORY
    -- ==========================================================================================================
    -- ==========================================================================================================
    -- VERSION DATE          AUTHOR(S)       DESCRIPTION
    -- ------- -----------   --------------- --------------------------------------------------------------------
    -- 1.0     17-MAR-2016   Andre Rivas     TMS#20160316-00215 - Create Job to reset CP statistics daily
    ************************************************************************************************************/
   PROCEDURE RESET_CONC_PROG_ONSITE (errbuf OUT VARCHAR2, retcode OUT NUMBER)
   IS
      l_err_msg        VARCHAR2 (2000);
      l_procedure      VARCHAR2 (100)   := 'reset_conc_prog_onsite';
      l_req_id         NUMBER           := fnd_global.conc_request_id;
      l_sec            VARCHAR2 (255)   := 'reset_conc_prog_onsite';
      l_err_code       NUMBER;
   BEGIN
      -- Resetting Process status flag for current back for new and error records.
      UPDATE fnd_conc_prog_onsite_info
         SET reset_date = SYSDATE
            ,min_run_time = NULL
            ,max_run_time = NULL
            ,avg_run_time = NULL
            ,successful_completion = 0
            ,error_completion = 0
            ,warning_completion = 0
       WHERE 1 = 1;                                                                         -- Update ALL rows

      COMMIT;

      retcode := 0;
      
   EXCEPTION
      WHEN PROGRAM_ERROR
      THEN
         ROLLBACK;
         l_err_msg := l_err_msg || ' ERROR ' || SUBSTR (SQLERRM, 1, 2000);
         --dbms_output.put_line(l_sec);
         fnd_file.put_line (fnd_file.LOG, l_err_msg);
         fnd_file.put_line (fnd_file.output, l_err_msg);
         retcode := 2;
         errbuf := l_err_msg;

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom
           ,p_calling             => l_procedure
           ,p_request_id          => l_req_id
           ,p_ora_error_msg       => SQLERRM
           ,p_error_desc          => 'Error running reset_conc_prog_onsite with PROGRAM ERROR'
           ,p_distribution_list   => g_distro_list
           ,p_module              => 'FND');
      WHEN OTHERS
      THEN
         ROLLBACK;
         --dbms_output.put_line(l_sec);

         fnd_file.put_line (fnd_file.LOG, l_sec);
         fnd_file.put_line (fnd_file.output, l_sec);
         l_err_code := 2;
         l_err_msg := l_sec || l_err_msg || ' ERROR ' || SQLCODE || SUBSTR (SQLERRM, 1, 2000);
         fnd_file.put_line (fnd_file.LOG, l_err_msg);
         fnd_file.put_line (fnd_file.output, l_err_msg);
         retcode := l_err_code;
         errbuf := l_err_msg;


         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom
           ,p_calling             => l_procedure
           ,p_request_id          => l_req_id
           ,p_ora_error_msg       => SUBSTR (l_err_msg, 1, 2000)
           ,p_error_desc          => 'Error running reset_conc_prog_onsite with OTHERS Exception'
           ,p_distribution_list   => g_distro_list
           ,p_module              => 'FND');
   END;
END XXWC_OEM_METRICS_PKG;
/