/* Ver        Date         Author           Description
   ---------  -----------  ---------------  ------------------------------------
   1.0        09-Dec-2015  Kishorebabu V    TMS#20151207-00046 @SF data Fix script for I635834
                                            Initial Version   */

SET SERVEROUTPUT ON SIZE 100000;
SET VERIFY OFF;

BEGIN
  UPDATE apps.oe_order_lines_all
  SET    invoice_interface_status_code = 'NOT_ELIGIBLE'
  ,      open_flag                     = 'N'
  ,      flow_status_code              = 'CLOSED'
  --,INVOICED_QUANTITY=1
  WHERE  line_id                       IN (39943747,39943750,39943754)
  AND    header_id                     = 24064262;
--1 row expected to be updated
COMMIT;
EXCEPTION
  WHEN others THEN
    dbms_output.put_line('Error occured while updating invoice_interface_status_code,open_flag,flow_status_code: '||SQLERRM);
END;
/