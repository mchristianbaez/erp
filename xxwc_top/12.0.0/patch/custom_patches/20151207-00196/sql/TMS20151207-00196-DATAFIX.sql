/*
 TMS: 20151207-00196 - Purge non 0Purchasing site records from vendor minimums table 
 Date: 12/8/2015
 */
SET SERVEROUTPUT ON SIZE 1000000

DECLARE
   v_count   NUMBER;
BEGIN
   DBMS_OUTPUT.put_line ('TMS:20151207-00196   , Script 1 -Before delete');

   BEGIN
      SELECT COUNT (1)
        INTO v_count
        FROM xxwc.XXWC_PO_VENDOR_MINIMUM xpvm,
             po_vendor_sites_all pvs,
             po_vendors pv,
             org_organization_definitions ood
       WHERE     xpvm.vendor_id = pv.vendor_id
             AND xpvm.vendor_site_id = pvs.vendor_site_id
             AND xpvm.organization_id = ood.organization_id
             AND pv.vendor_id = pvs.vendor_id
             AND NVL (UPPER (pvs.vendor_site_code), 'NONE') != '0PURCHASING';

      DELETE FROM xxwc.XXWC_PO_VENDOR_MINIMUM
            WHERE ROWID IN (SELECT xpvm.ROWID
                              FROM xxwc.XXWC_PO_VENDOR_MINIMUM xpvm,
                                   po_vendor_sites_all pvs,
                                   po_vendors pv,
                                   org_organization_definitions ood
                             WHERE     xpvm.vendor_id = pv.vendor_id
                                   AND xpvm.vendor_site_id =
                                          pvs.vendor_site_id
                                   AND xpvm.organization_id =
                                          ood.organization_id
                                   AND pv.vendor_id = pvs.vendor_id
                                   AND NVL (UPPER (pvs.vendor_site_code),
                                            'NONE') != '0PURCHASING');
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         DBMS_OUTPUT.PUT_LINE (
            'Error in deleting records from vendor minimum table-' || SQLERRM);
   END;

   COMMIT;

   DBMS_OUTPUT.put_line (
         'TMS: 20151207-00196, Script 1 -After delete, rows deleted: '
      || v_count);
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.PUT_LINE ('Error is' || SQLERRM);
END;
/