/*
TMS: 20150925-00109 
 Date: 09/25/2015
*/
SET SERVEROUTPUT ON SIZE 1000000

DECLARE
   v_count   NUMBER;
BEGIN
   DBMS_OUTPUT.put_line ('TMS:20150925-00109  , Script 1 -Before delete');

   BEGIN
       SELECT count(1)
       INTO   v_count
       FROM po.PO_PURGE_REQ_LIST a
       WHERE     1 = 1
       AND NOT EXISTS
                  (SELECT 1
                     FROM po.po_requisition_headers_all
                    WHERE     TYPE_LOOKUP_CODE = 'PURCHASE'
                          AND requisition_header_id = a.requisition_header_id
                          AND authorization_status = 'CANCELLED');

       DELETE
       FROM    po.PO_PURGE_REQ_LIST a
       WHERE     1 = 1
       AND NOT EXISTS
                  (SELECT 1
                     FROM po.po_requisition_headers_all
                    WHERE     TYPE_LOOKUP_CODE = 'PURCHASE'
                          AND requisition_header_id = a.requisition_header_id
                          AND authorization_status = 'CANCELLED');
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         DBMS_OUTPUT.PUT_LINE (
            'Error in deleting records from po po purge req list-' || SQLERRM);
         ROLLBACK;
   END;

   COMMIT;

   DBMS_OUTPUT.put_line (
         'TMS: 20150925-00109, Script 1 -After delete, rows deleted: '|| v_count);

EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.PUT_LINE ('Error is' || SQLERRM);
   ROLLBACK;
END;
/

