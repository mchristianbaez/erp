CREATE OR REPLACE PACKAGE BODY APPS.XXWC_B2B_SO_IB_PKG
AS
   /*************************************************************************
      $Header XXWC_B2B_SO_IB_PKG.PKG $
      Module Name: XXWC_B2B_SO_IB_PKG.PKG

      PURPOSE:   This package is used for B2B Customer SalesOrder Inbound Interface

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------
      1.0        04/09/2014  Gopi Damuluri           Initial Version
      1.1        06/09/2014  Gopi Damuluri           TMS# 20140609-00256
                                                     Change default Shipping Method to "Our Truck"
      1.2        08/12/2014  Gopi Damuluri           TMS# 20140812-00199
                                                     Clear LineTable Cache for Order Import
      1.3        09/11/2014  Gopi Damuluri           TMS# 20140911-00299
                                                     Add Ship_To_Contact_First_Name and Ship_To_Contact_Last_Name fields to B2B PO Import file
      1.4        09/30/2014  Gopi Damulrui           TMS# 20140930-00321
                                                     Validate ShipTo Address fields in addition to ShipToNumber
      1.5        01/08/2015  Gopi Damuluri           TMS# 20150317-00069
                                                     Add Notes in Shipping Instructions field at Order Header and Line levels
      1.6        06/11/2015  Manjula Chellappan      TMS# 20150615-00088 B2B for POA
                             Gopi Damuluri           Add Notification Email for B2B PO Import Success and Failures
                                                     Add functions for deriving SalesRep Information
      1.7        09/21/2015  Gopi Damuluri           TMS# 20150921-00334
                                                     Resolve error while "Validate Customer POA Delivery Flag".
      1.8        09/21/2015  Gopi Damuluri           TMS# 20151019-00065
                                                     Allow any user to setup B2B access when Customer has no Sales Rep associated.
      1.9        12/15/2015  Gopi Damuluri           TMS# 20160120-00169 - B2B POD Enhancement
      1.10       02/15/2016  Gopi Damuluri           TMS# 20160215-00282 - Changes to B2B POD Notification email
      1.11       02/22/2016  Gopi Damuluri           TMS# 20160217-00165 - Changes to B2B POD - Include DMS Delivery_Id
      1.12       03/14/2016  Gopi Damuluri           TMS# 20160314-00156 - Issue with Weekly POD
      1.13       04/04/2016  Gopi Damuluri           TMS# 20160404-00040 - Incorrect POD_LAST_SENT_DATE for WEEKLY POD
      1.14       04/21/2016  Gopi Damuluri           TMS# 20160421-00033 - Remove Apex URL Hard Coding in PO Inbound Process
      1.15       06/16/2016  Pattabhi Avula          TMS# 20160616-00043 - Increased l_email_addr length in CREATE_FILE procedure
      1.16       06/20/2016  Rakesh Patel            TMS#20160617-00076-XXWC B2B Create POD Files Program Issues Fix
	  1.17       10/04/2016  Pattabhi Avula          TMS#20160926-00059 -- Index's created and Changed the update statement for performance improve
	  1.18       10/27/2016  Rakesh Patel            TMS#20161010-00242 --  Change SOA and ASN to reflect priced copy settings on B2B Setup page
   	1.19         04/27/2017    Nancy Pahwa      TMS#20170206-00257 -- OSU POD Mass Update and Unsubscribe
    1.20       06/14/2017  Nancy Pahwa             Task ID: 20170523-00145 - XML Error
    1.21       06/23/2017  Rakesh Patel            TMS#20170509-00055-Inconsistent delivery of PODs where frequency's differ at josbite and account level
	  1.22      08/17/2017  Pattabhi Avula         TMS#20170817-00062 -- Modified the delete statement for performance improve
	                                               added Time stamp messages 
****************************************************************************/

   --Email Defaults
   g_dflt_email   fnd_user.email_address%TYPE    := 'WC-ITDEVALERTS-U1@HDSupply.com';

   /*************************************************************************
      PROCEDURE Name: create_contact

      PURPOSE:   To create contact when contact does not exist in Oracle

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------
      1.0        04/09/2014  Gopi Damuluri             Initial Version
   ****************************************************************************/
   PROCEDURE create_contact (p_customer_id       IN     NUMBER,
                             p_first_name        IN     VARCHAR2,
                             p_last_name         IN     VARCHAR2,
                             p_contact_num       IN     VARCHAR2,
                             p_email_id          IN     VARCHAR2,
                             p_fax_num           IN     VARCHAR2,
                             p_cust_contact_id      OUT NUMBER,
                             p_error_message        OUT VARCHAR2)
   IS
      l_party_id               NUMBER;
      p_create_person_rec      HZ_PARTY_V2PUB.person_rec_type;
      x_party_id               NUMBER;
      x_party_number           VARCHAR2 (2000);
      x_profile_id             NUMBER;
      x_return_status          VARCHAR2 (2000);
      x_msg_count              NUMBER;
      x_msg_data               VARCHAR2 (2000);

      p_org_contact_rec        HZ_PARTY_CONTACT_V2PUB.ORG_CONTACT_REC_TYPE;
      x_org_contact_id         NUMBER;
      x_party_rel_id           NUMBER;
      x_party_id2              NUMBER;
      x_party_number2          VARCHAR2 (2000);

      p_cr_cust_acc_role_rec   HZ_CUST_ACCOUNT_ROLE_V2PUB.cust_account_role_rec_type;
      x_cust_account_role_id   NUMBER;
      l_exception              EXCEPTION;

      p_contact_point_rec      HZ_CONTACT_POINT_V2PUB.CONTACT_POINT_REC_TYPE;

      p_edi_rec                HZ_CONTACT_POINT_V2PUB.EDI_REC_TYPE;
      p_email_rec              HZ_CONTACT_POINT_V2PUB.EMAIL_REC_TYPE;
      p_phone_rec              HZ_CONTACT_POINT_V2PUB.PHONE_REC_TYPE;
      p_telex_rec              HZ_CONTACT_POINT_V2PUB.TELEX_REC_TYPE;
      p_web_rec                HZ_CONTACT_POINT_V2PUB.WEB_REC_TYPE;

      x_contact_point_id       NUMBER;
      l_sec                    VARCHAR2 (100);
   BEGIN
      l_sec := 'Derive Customer Party Id';

      ------------------------------------
      -- Derive Customer Party Id
      ------------------------------------
      SELECT party_id
        INTO l_party_id
        FROM hz_cust_accounts_all hca
       WHERE cust_account_id = p_customer_id;

      l_sec := 'Create a definition contact';

      ------------------------------------
      -- 1. Create a definition contact
      ------------------------------------
      IF p_first_name IS NULL OR p_last_name IS NULL
      THEN
         p_error_message := 'First Name or Last Name is empty';
         RAISE l_exception;
      ELSE
         p_create_person_rec.person_first_name := p_first_name;
         p_create_person_rec.person_last_name := p_last_name;
      END IF;

      p_create_person_rec.created_by_module := 'TCA_V1_API';

      HZ_PARTY_V2PUB.create_person ('T',
                                    p_create_person_rec,
                                    x_party_id,
                                    x_party_number,
                                    x_profile_id,
                                    x_return_status,
                                    x_msg_count,
                                    x_msg_data);

      IF x_return_status != 'S'
      THEN
         p_error_message := x_msg_data;
         RAISE l_exception;
      END IF;

      l_sec := 'Create a relation cont-org using party_id from step 1';
      ------------------------------------
      -- 2. Create a relation cont-org using party_id from step 1
      ------------------------------------
      p_org_contact_rec.created_by_module := 'TCA_V1_API';
      p_org_contact_rec.party_rel_rec.subject_id := x_party_id; --<<value for party_id from step 1>
      p_org_contact_rec.party_rel_rec.subject_type := 'PERSON';
      p_org_contact_rec.party_rel_rec.subject_table_name := 'HZ_PARTIES';
      p_org_contact_rec.party_rel_rec.object_id := l_party_id; --<<value for party_id from step 0>
      p_org_contact_rec.party_rel_rec.object_type := 'ORGANIZATION';
      p_org_contact_rec.party_rel_rec.object_table_name := 'HZ_PARTIES';
      p_org_contact_rec.party_rel_rec.relationship_code := 'CONTACT_OF';
      p_org_contact_rec.party_rel_rec.relationship_type := 'CONTACT';
      p_org_contact_rec.party_rel_rec.start_date := SYSDATE;
      --         p_org_contact_rec.contact_number                   := p_contact_num;

      hz_party_contact_v2pub.create_org_contact ('T',
                                                 p_org_contact_rec,
                                                 x_org_contact_id,
                                                 x_party_rel_id,
                                                 x_party_id2,
                                                 x_party_number,
                                                 x_return_status,
                                                 x_msg_count,
                                                 x_msg_data);

      IF x_return_status != 'S'
      THEN
         p_error_message := x_msg_data;
         RAISE l_exception;
      END IF;

      IF p_contact_num IS NOT NULL
      THEN
         l_sec := 'Create a Phone Contact Point using party_id';
         ------------------------------------------------------------------------------------------------------------
         -- 3.1 Create a Phone Contact Point using party_id
         ------------------------------------------------------------------------------------------------------------
         -- Initializing the Mandatory API parameters
         p_contact_point_rec.contact_point_type := 'PHONE';
         p_contact_point_rec.owner_table_name := 'HZ_PARTIES';
         p_contact_point_rec.owner_table_id := x_party_id2;      --l_party_id;
         p_contact_point_rec.primary_flag := 'Y';
         p_contact_point_rec.contact_point_purpose := 'BUSINESS';
         p_contact_point_rec.created_by_module := 'BO_API';

         p_phone_rec.phone_area_code := NULL;
         p_phone_rec.phone_country_code := '1';
         p_phone_rec.phone_number := p_contact_num;
         p_phone_rec.phone_line_type := 'MOBILE';

         fnd_file.put_line (
            fnd_file.LOG,
            'Calling the API hz_contact_point_v2pub.create_contact_point');

         HZ_CONTACT_POINT_V2PUB.CREATE_CONTACT_POINT (
            p_init_msg_list       => FND_API.G_TRUE,
            p_contact_point_rec   => p_contact_point_rec,
            p_edi_rec             => p_edi_rec,
            p_email_rec           => p_email_rec,
            p_phone_rec           => p_phone_rec,
            p_telex_rec           => p_telex_rec,
            p_web_rec             => p_web_rec,
            x_contact_point_id    => x_contact_point_id,
            x_return_status       => x_return_status,
            x_msg_count           => x_msg_count,
            x_msg_data            => x_msg_data);

         IF x_return_status != 'S'
         THEN
            p_error_message := x_msg_data;
            RAISE l_exception;
         END IF;
      END IF;                             -- IF p_contact_num IS NOT NULL THEN

      l_sec := 'Create a Email Contact Point using party_id';

      ------------------------------------------------------------------------------------------------------------
      -- 3.2 Create a Email Contact Point using party_id
      ------------------------------------------------------------------------------------------------------------
      IF p_email_id IS NOT NULL
      THEN
         -- Initializing the Mandatory API parameters
         p_contact_point_rec.contact_point_type := 'EMAIL';
         p_contact_point_rec.owner_table_name := 'HZ_PARTIES';
         p_contact_point_rec.owner_table_id := x_party_id2;      --l_party_id;
         p_contact_point_rec.primary_flag := 'Y';
         p_contact_point_rec.contact_point_purpose := 'BUSINESS';
         p_contact_point_rec.created_by_module := 'BO_API';

         p_email_rec.email_format := 'MAILHTML';
         p_email_rec.email_address := p_email_id;

         fnd_file.put_line (
            fnd_file.LOG,
            'Calling the API hz_contact_point_v2pub.create_contact_point');

         HZ_CONTACT_POINT_V2PUB.CREATE_CONTACT_POINT (
            p_init_msg_list       => FND_API.G_TRUE,
            p_contact_point_rec   => p_contact_point_rec,
            p_edi_rec             => p_edi_rec,
            p_email_rec           => p_email_rec,
            p_phone_rec           => p_phone_rec,
            p_telex_rec           => p_telex_rec,
            p_web_rec             => p_web_rec,
            x_contact_point_id    => x_contact_point_id,
            x_return_status       => x_return_status,
            x_msg_count           => x_msg_count,
            x_msg_data            => x_msg_data);

         IF x_return_status != 'S'
         THEN
            p_error_message := x_msg_data;
            RAISE l_exception;
         END IF;
      END IF;                                -- IF p_email_id IS NOT NULL THEN

      l_sec := 'Create a contact using party_id you get in step 3';
      ------------------------------------------------------------------------------------------------------------
      -- 4. Create a contact using party_id you get in step 3
      ------------------------------------------------------------------------------------------------------------
      p_cr_cust_acc_role_rec.party_id := x_party_id2; --<<value for party_id from step 2>
      p_cr_cust_acc_role_rec.cust_account_id := p_customer_id; --<<value for cust_account_id from step 0>
      --p_cr_cust_acc_role_rec.primary_flag    := 'Y';
      p_cr_cust_acc_role_rec.role_type := 'CONTACT';
      p_cr_cust_acc_role_rec.created_by_module := 'TCA_V1_API';

      HZ_CUST_ACCOUNT_ROLE_V2PUB.CREATE_CUST_ACCOUNT_ROLE (
         'T',
         p_cr_cust_acc_role_rec,
         x_cust_account_role_id,
         x_return_status,
         x_msg_count,
         x_msg_data);

      IF x_return_status != 'S'
      THEN
         p_error_message := x_msg_data;
         RAISE l_exception;
      ELSE
         p_cust_contact_id := x_cust_account_role_id;
      END IF;
   EXCEPTION
      WHEN l_exception
      THEN
         fnd_file.put_line (
            fnd_file.LOG,
               'p_customer_id - '
            || p_customer_id
            || ' in L_EXCEPTION, p_error_message - '
            || p_error_message);
      WHEN OTHERS
      THEN
         ROLLBACK;
         p_cust_contact_id := NULL;
         fnd_file.put_line (
            fnd_file.LOG,
               'p_customer_id - '
            || p_customer_id
            || ' error in  CREATE_CONTACT procedure - '
            || SQLERRM);
   END create_contact;

   /*************************************************************************
      PROCEDURE Name: load_interface

      PURPOSE:   To validate B2B Customer Staging data and load the same into
                 Interface table.

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------
      1.0        04/09/2014  Gopi Damuluri             Initial Version
      1.1        06/09/2014  Gopi Damuluri           TMS# 20140609-00256
                                                     Change default Shipping Method to "Our Truck"
      1.2        08/12/2014  Gopi Damuluri           TMS# 20140812-00199
                                                     Clear LineTable Cache for Order Import
      1.3        09/11/2014  Gopi Damuluri           TMS# 20140911-00299
                                                     Add Ship_To_Contact_First_Name and Ship_To_Contact_Last_Name fields to B2B PO Import file
      1.4        09/30/2014  Gopi Damulrui           TMS# 20140930-00321
                                                     Validate ShipTo Address fields in addition to ShipToNumber
      1.6        06/11/2015  Manjula Chellappan      TMS# 20150615-00088 B2B for POA
                             Gopi Damuluri           Add Notification Email for B2B PO Import Success and Failures
      1.7        09/21/2015  Gopi Damuluri           TMS# 20150921-00334
                                                     Resolve error while "Validate Customer POA Delivery Flag"
      1.11       02/22/2016  Gopi Damuluri           TMS# 20160217-00165 - Changes to B2B POD - Include DMS Delivery_Id
      1.16       06/20/2016  Rakesh Patel            TMS#20160617-00076-XXWC B2B Create POD Files Program Issues Fix
      1.18       10/27/2016  Rakesh Patel            TMS#20161010-00242 --  Change SOA and ASN to reflect priced copy settings on B2B Setup page
      1.20       06/14/2017  Nancy Pahwa             Task ID: 20170523-00145 - XML Error
   ***************************************************************************/
   PROCEDURE load_interface (p_errbuf                  OUT VARCHAR2,
                             p_retcode                 OUT NUMBER,
                             p_validate_only        IN     VARCHAR2,
                             p_notification_email   IN     VARCHAR2,
                             p_cust_po_number       IN     VARCHAR2,
                             p_apex_url             IN     VARCHAR2)
   IS
      ----------------------------------------------------------
      -- Order Header Cursor
      ----------------------------------------------------------
      CURSOR cur_so_hdr
      IS
         SELECT stg.customer_po_number,
                stg.order_total,
                stg.customer_number,
                stg.ship_to_number,
                UPPER (stg.ship_to_address1) ship_to_address1  -- Version# 1.4
                                                             ,
                stg.ship_to_address2,
                stg.ship_to_address3,
                stg.ship_to_address4,
                UPPER (stg.ship_to_city) ship_to_city          -- Version# 1.4
                                                     ,
                UPPER (stg.ship_to_state) ship_to_state        -- Version# 1.4
                                                       ,
                stg.ship_to_country,
                stg.ship_to_postal_code,
                stg.bill_to_address1,
                stg.bill_to_address2,
                stg.bill_to_address3,
                stg.bill_to_address4,
                stg.bill_to_city,
                stg.bill_to_state,
                stg.bill_to_country,
                stg.bill_to_postal_code,
                stg.deliver_to_address1,
                stg.deliver_to_address2,
                stg.deliver_to_address3,
                stg.deliver_to_address4,
                stg.deliver_to_city,
                stg.deliver_to_state,
                stg.deliver_to_country,
                stg.deliver_to_postal_code,
                stg.shipping_instructions,
                stg.contact_first_name,
                stg.contact_last_name,
                stg.ship_to_contact_first_name                 -- Version# 1.3
                                              ,
                stg.ship_to_contact_last_name                  -- Version# 1.3
                                             ,
                stg.notes header_notes                         -- Version# 1.5
                                      ,
                stg.email,
                stg.contact_num,
                stg.fax_num,
                stg.process_flag,
                stg.ROWID
           FROM XXWC.XXWC_B2B_SO_HDRS_STG_TBL stg
          WHERE     1 = 1
                AND (   p_cust_po_number IS NULL
                     OR stg.customer_po_number = p_cust_po_number)
                AND NVL (stg.process_flag, 'N') IN ('V', 'N', 'E');

      ----------------------------------------------------------
      -- Order Lines Cursor
      ----------------------------------------------------------
      CURSOR cur_so_lines (p_customer_po_number VARCHAR2)
      IS
           SELECT stg.customer_po_number,
                  stg.line_number,
                  stg.inventory_item,
                  stg.ordered_quantity,
                  stg.unit_of_measure,
                  stg.unit_selling_price,
                  stg.request_date,
                  stg.item_description,
                  stg.notes line_notes                         -- Version# 1.5
                                      ,
                  stg.process_flag,
                  stg.ROWID
             FROM xxwc.xxwc_b2b_so_lines_stg_tbl stg
            WHERE 1 = 1 AND stg.customer_po_number = p_customer_po_number
         ORDER BY stg.line_number;

      --------------------------------------------------------------
      -- Local Variables
      --------------------------------------------------------------
      l_order_source_id          NUMBER := 1021;                        -- B2B
      l_def_inv_org              VARCHAR2 (3);
      l_dflt_ship_method_code    VARCHAR2 (50) := '000001_Our Truck_P_LTL'; -- Version# 1.1
      l_freight_carrier_code     VARCHAR2 (50) := 'Our Truck'; -- Version# 1.1

      l_err_flag                 VARCHAR2 (1) := 'N';
      g_org_id                   NUMBER := 162;
      l_order_type               oe_transaction_types_tl.name%TYPE
                                    := 'STANDARD ORDER';
      l_cust_contact_id          NUMBER;
      l_shipto_cust_contact_id   NUMBER;                       -- Version# 1.3
      l_inv_org_id               NUMBER;
      l_ship_to_site_use_id      NUMBER;
      l_customer_id              NUMBER;
      l_customer_number          VARCHAR2 (30);
      l_customer_name            VARCHAR2 (240);
      l_orig_sys_document_ref    VARCHAR2 (240);
      l_bill_to_site_use_id      NUMBER;
      l_inv_item_id              NUMBER;
      l_shipping_item_id         NUMBER;
      l_shipping_line_number     NUMBER;
      l_order_cnt                NUMBER := 0;
      l_shipping_instructions    VARCHAR2 (2000);
      l_line_shipping_instr      VARCHAR2 (2000);              -- Version# 1.5
      l_error_message            VARCHAR2 (2000);
      l_line_error_message       VARCHAR2 (2000);
      l_exception                EXCEPTION;

      --------------------------------------------------------------
      -- Email Variables
      --------------------------------------------------------------
      l_host                     VARCHAR2 (256)
                                    := 'mailoutrelay.hdsupply.net';
      l_hostport                 VARCHAR2 (20) := '25';

      l_sender                   VARCHAR2 (100) := 'no-reply@whitecap.net';

      l_subject                  VARCHAR2 (32767) DEFAULT NULL;
      l_body                     VARCHAR2 (32767) DEFAULT NULL;
      l_body_header              VARCHAR2 (32767) DEFAULT NULL;
      l_body_detail              VARCHAR2 (32767) DEFAULT NULL;
      l_body_footer              VARCHAR2 (32767) DEFAULT NULL;
      l_instance                 VARCHAR2 (50);
      l_def_notif_email          VARCHAR2 (250); -- Version# 1.6

      --------------------------------------------------------------
      -- OE_ORDER_PUB.PROCESS_ORDER Variables
      --------------------------------------------------------------
      l_header_rec               OE_ORDER_PUB.Header_Rec_Type
                                    := OE_ORDER_PUB.G_MISS_HEADER_REC;
      l_old_header_rec           OE_ORDER_PUB.Header_Rec_Type;
      l_line_tbl                 OE_ORDER_PUB.Line_Tbl_Type;
      l_old_line_tbl             OE_ORDER_PUB.Line_Tbl_Type;
      l_action_request_tbl       OE_ORDER_PUB.Request_Tbl_Type;
      x_header_rec               OE_ORDER_PUB.Header_Rec_Type;
      x_header_val_rec           OE_ORDER_PUB.Header_Val_Rec_Type;
      x_Header_Adj_tbl           OE_ORDER_PUB.Header_Adj_Tbl_Type;
      x_Header_Adj_val_tbl       OE_ORDER_PUB.Header_Adj_Val_Tbl_Type;
      x_Header_price_Att_tbl     OE_ORDER_PUB.Header_Price_Att_Tbl_Type;
      x_Header_Adj_Att_tbl       OE_ORDER_PUB.Header_Adj_Att_Tbl_Type;
      x_Header_Adj_Assoc_tbl     OE_ORDER_PUB.Header_Adj_Assoc_Tbl_Type;
      x_Header_Scredit_tbl       OE_ORDER_PUB.Header_Scredit_Tbl_Type;
      x_Header_Scredit_val_tbl   OE_ORDER_PUB.Header_Scredit_Val_Tbl_Type;
      x_line_tbl                 OE_ORDER_PUB.Line_Tbl_Type;
      x_line_val_tbl             OE_ORDER_PUB.Line_Val_Tbl_Type;
      x_Line_Adj_tbl             OE_ORDER_PUB.Line_Adj_Tbl_Type;
      x_Line_Adj_val_tbl         OE_ORDER_PUB.Line_Adj_Val_Tbl_Type;
      x_Line_price_Att_tbl       OE_ORDER_PUB.Line_Price_Att_Tbl_Type;
      x_Line_Adj_Att_tbl         OE_ORDER_PUB.Line_Adj_Att_Tbl_Type;
      x_Line_Adj_Assoc_tbl       OE_ORDER_PUB.Line_Adj_Assoc_Tbl_Type;
      x_Line_Scredit_tbl         OE_ORDER_PUB.Line_Scredit_Tbl_Type;
      x_Line_Scredit_val_tbl     OE_ORDER_PUB.Line_Scredit_Val_Tbl_Type;
      x_Lot_Serial_tbl           OE_ORDER_PUB.Lot_Serial_Tbl_Type;
      x_Lot_Serial_val_tbl       OE_ORDER_PUB.Lot_Serial_Val_Tbl_Type;
      x_action_request_tbl       OE_ORDER_PUB.Request_Tbl_Type;

      l_return_status            VARCHAR2 (2000);
      l_msg_count                NUMBER;
      l_msg_data                 VARCHAR2 (2000);
      l_return_msg               VARCHAR2 (2000);
      lv_msg_data                VARCHAR2 (2000);

      l_line_cnt                 NUMBER := 0;
      l_top_model_line_index     NUMBER;
      l_link_to_line_index       NUMBER;
      l_msg_index_out            NUMBER (10);
      l_sec                      VARCHAR2 (100);

   --<< Added for Rev 1.6 Begin
      l_retcode                  VARCHAR2 (5);
      l_errbuf                   VARCHAR2 (2000);
   -- Added for Rev 1.6 End >>

   -- Version# 1.16 > Start
      l_inventory_item_status_code mtl_system_items_b.inventory_item_status_code%TYPE;
   -- Version# 1.16 < End
   BEGIN
      fnd_file.put_line (
         fnd_file.LOG,
         '----------------------------------------------------------------------------------');
      fnd_file.put_line (fnd_file.LOG, 'Start of Procedure : LOAD_INTERFACE');
      fnd_file.put_line (
         fnd_file.LOG,
         '----------------------------------------------------------------------------------');

      l_sec := 'Derive Oracle Instance';

      ----------------------------------------------------------------------------------
      -- Derive Oracle Instance
      ----------------------------------------------------------------------------------
      BEGIN
         SELECT SUBSTR (UPPER (instance_name), 1, 7)
           INTO l_instance
           FROM v$instance;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_instance := NULL;
      END;


      l_sec := 'Order Header Cursor';

      ----------------------------------------------------------------------------------
      -- Order Header Cursor
      ----------------------------------------------------------------------------------
      FOR rec_so_hdr IN cur_so_hdr
      LOOP
         l_error_message := ' ';
         l_err_flag := 'N';
         l_order_cnt := l_order_cnt + 1;
         l_orig_sys_document_ref := NULL;

         BEGIN                                        -- rec_so_hdr LOOP BEGIN
            l_ship_to_site_use_id := NULL;
            l_customer_number := NULL;
            l_customer_name := NULL;
            l_customer_id := NULL;
            l_bill_to_site_use_id := NULL;
            l_def_inv_org := NULL;
            l_def_notif_email  := p_notification_email; -- Version# 1.7
            l_sec := 'Check if the Customer is set up as B2B';

            ----------------------------------------------------------------------------------
            -- Check if the Customer is set up as B2B
            ----------------------------------------------------------------------------------
            BEGIN
               SELECT 'N'
                    , stg.notification_email -- Version# 1.6
                 INTO l_err_flag
                    , l_def_notif_email      -- Version# 1.6
                 FROM xxwc.xxwc_b2b_config_tbl stg, -- Version# 1.18
                      hz_cust_accounts_all hca,
                      hz_cust_acct_sites_all hcas,
                      hz_cust_site_uses_all hcsu,
                      hz_party_sites hps                       -- Version# 1.4
                                        ,
                      hz_locations hl                          -- Version# 1.4
                WHERE     1 = 1
                      AND hcsu.attribute10 = rec_so_hdr.ship_to_number
                      AND hcas.cust_acct_site_id = hcsu.cust_acct_site_id
                      AND hcsu.site_use_code = 'SHIP_TO'
                      AND hps.location_id = hl.location_id     -- Version# 1.4
                      AND hcas.party_site_id = hps.party_site_id -- Version# 1.4
                      AND UPPER (hl.state) = rec_so_hdr.ship_to_state -- Version# 1.4
                      AND hcas.status = 'A'
                      AND hcsu.status = 'A'
                      AND hcas.org_id = g_org_id
                      AND hca.cust_account_id = hcas.cust_account_id
                      AND stg.cust_account_id = hca.cust_account_id -- Version# 1.18
                      AND NVL(stg.status, 'DRAFT')  = 'APPROVED' -- Version# 1.6
                      AND SYSDATE BETWEEN NVL (stg.start_date_active,
                                               SYSDATE - 10)
                                      AND NVL (stg.end_date_active,
                                               SYSDATE + 10)
                      AND ROWNUM = 1                          -- Version# 1.7
                      ;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  l_err_flag := 'Y';
                  l_error_message :=
                     l_error_message || '-- Customer not setup as B2B';
                  RAISE l_exception;
               WHEN OTHERS
               THEN
                  l_err_flag := 'Y';
                  l_error_message :=
                        l_error_message
                     || '-- Error validating B2B Customer - '
                     || SQLERRM;
                  RAISE l_exception;
            END;

            -- Version# 1.6 > Start
            IF l_def_notif_email IS NULL THEN
               l_def_notif_email := p_notification_email;
            END IF;
            -- Version# 1.6 < End

            l_ship_to_site_use_id := NULL;
            l_customer_number := NULL;
            l_customer_name := NULL;
            l_customer_id := NULL;
            l_bill_to_site_use_id := NULL;
            l_def_inv_org := NULL;

            l_sec := 'Derive Ship-To Address using Liason Site#';

            ----------------------------------------------------------------------------------
            -- Derive Ship-To Address using Liason Site#
            ----------------------------------------------------------------------------------
            BEGIN
               SELECT hcsu.site_use_id,
                      hca.account_number,
                      UPPER (hca.account_name),
                      hcas.cust_account_id,
                      hcsu.bill_to_site_use_id,
                      hcsu.attribute11
                 INTO l_ship_to_site_use_id,
                      l_customer_number,
                      l_customer_name,
                      l_customer_id,
                      l_bill_to_site_use_id,
                      l_def_inv_org
                 FROM hz_cust_acct_sites_all hcas,
                      hz_cust_site_uses_all hcsu,
                      hz_cust_accounts_all hca,
                      hz_party_sites hps                       -- Version# 1.4
                                        ,
                      hz_locations hl                          -- Version# 1.4
                WHERE     1 = 1
                      AND hcsu.attribute10 = rec_so_hdr.ship_to_number
                      AND hca.cust_account_id = hcas.cust_account_id
                      AND hcas.cust_acct_site_id = hcsu.cust_acct_site_id
                      AND hcsu.site_use_code = 'SHIP_TO'
                      AND hps.location_id = hl.location_id     -- Version# 1.4
                      AND hcas.party_site_id = hps.party_site_id -- Version# 1.4
                      AND UPPER (hl.state) = rec_so_hdr.ship_to_state -- Version# 1.4
                      AND hcas.status = 'A'
                      AND hcsu.status = 'A'
                      AND hca.status = 'A'  --Version# 1.20
                      AND hcas.org_id = g_org_id
                      AND ROWNUM = 1                          -- Version# 1.7
                      ;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  l_err_flag := 'Y';
                  l_error_message :=
                     l_error_message || '-- B2B Customer Site not in Oracle';
               WHEN TOO_MANY_ROWS
               THEN
                  l_err_flag := 'Y';
                  l_error_message :=
                        l_error_message
                     || '-- Multiple Oracle Sites are associated with same B2B Customer Site';
               WHEN OTHERS
               THEN
                  l_err_flag := 'Y';
                  l_error_message :=
                        l_error_message
                     || '-- Error deriving SHIP-TO Site details '
                     || SQLERRM;
            END;

            l_sec :=
               'If Bill-To Address is not associated with Ship-To Address, derive Primary Bill-To';

            -------------------------------------------------------------------------------------
            -- If Bill-To Address is not associated with Ship-To Address, derive Primary Bill-To
            -------------------------------------------------------------------------------------
            IF l_bill_to_site_use_id IS NULL
            THEN
               BEGIN
                  SELECT hcsu.site_use_id
                    INTO l_bill_to_site_use_id
                    FROM hz_cust_acct_sites_all hcas,
                         hz_cust_site_uses_all hcsu
                   WHERE     1 = 1
                         AND hcas.status = 'A'
                         AND hcsu.status = 'A'
                         AND hcas.cust_account_id = l_customer_id
                         AND hcas.cust_acct_site_id = hcsu.cust_acct_site_id
                         AND hcsu.site_use_code = 'BILL_TO'
                         AND hcsu.primary_flag = 'Y'
                         AND hcas.org_id = g_org_id;
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     l_err_flag := 'Y';
                     l_error_message :=
                           l_error_message
                        || '-- No Primary Bill To Site for Customer';
                  WHEN TOO_MANY_ROWS
                  THEN
                     l_err_flag := 'Y';
                     l_error_message :=
                           l_error_message
                        || '-- Multiple Primary Bill To Site for Customer';
                  WHEN OTHERS
                  THEN
                     l_err_flag := 'Y';
                     l_error_message :=
                           l_error_message
                        || '-- Error deriving BILL-TO Site details '
                        || SQLERRM;
               END;
            END IF;

            l_sec := 'Derive Inventory Org Id';
            ----------------------------------------------------------------------------------
            -- Derive Inventory Org Id
            ----------------------------------------------------------------------------------
            l_inv_org_id := NULL;

            BEGIN
               SELECT organization_id
                 INTO l_inv_org_id
                 FROM org_organization_definitions ood
                WHERE organization_code = l_def_inv_org;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_err_flag := 'Y';
                  l_error_message :=
                     l_error_message || '-- Invalid branch_number';
            END;

            IF    rec_so_hdr.contact_first_name IS NULL
               OR rec_so_hdr.contact_last_name IS NULL
            THEN
               l_err_flag := 'Y';
               l_error_message :=
                  l_error_message || '-- Contact First or Last Name is Empty';
            END IF;

            IF rec_so_hdr.email IS NULL
            THEN
               l_err_flag := 'Y';
               l_error_message :=
                  l_error_message || '-- Email Address is Empty';
            END IF;

            l_cust_contact_id := NULL;

            IF (    rec_so_hdr.contact_first_name IS NOT NULL
                AND rec_so_hdr.contact_last_name IS NOT NULL
                AND rec_so_hdr.email IS NOT NULL)
            THEN
               l_sec := 'Derive Contact Details';

               ----------------------------------------------------------------------------------
               -- Derive Contact Details
               ----------------------------------------------------------------------------------
               BEGIN
                  SELECT acct_role.cust_account_role_id
                    INTO l_cust_contact_id
                    FROM hz_cust_account_roles acct_role,
                         --    hz_contact_points      hcp,
                         hz_parties party,
                         hz_parties rel_party,
                         hz_relationships rel,
                         hz_org_contacts org_cont,
                         hz_cust_accounts role_acct
                   WHERE     acct_role.party_id = rel.party_id
                         -- AND hcp.owner_table_id             = acct_role.party_id
                         -- AND hcp.contact_point_type         = 'EMAIL'
                         -- AND hcp.owner_table_name           = 'HZ_PARTIES'
                         -- AND hcp.email_address              = rec_so_hdr.email
                         AND NOT EXISTS
                                    (SELECT '1'
                                       FROM hz_role_responsibility rr
                                      WHERE rr.cust_account_role_id =
                                               acct_role.cust_account_role_id)
                         AND acct_role.role_type = 'CONTACT'
                         AND org_cont.party_relationship_id =
                                rel.relationship_id
                         AND rel.subject_id = party.party_id
                         AND rel_party.party_id = rel.party_id
                         AND acct_role.cust_account_id =
                                role_acct.cust_account_id
                         AND role_acct.party_id = rel.object_id
                         AND rel.subject_table_name = 'HZ_PARTIES'
                         AND rel.object_table_name = 'HZ_PARTIES'
                         AND UPPER (party.person_first_name) =
                                UPPER (rec_so_hdr.contact_first_name)
                         AND UPPER (party.person_last_name) =
                                UPPER (rec_so_hdr.contact_last_name)
                         AND (    org_cont.job_title IS NULL
                              AND org_cont.job_title_code IS NULL)
                         AND party.status = 'A'
                         AND rel_party.status = 'A'
                         AND rel.status = 'A'
                         AND acct_role.status = 'A'
                         AND (role_acct.account_number = l_customer_number)
                         AND ROWNUM = 1;
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     l_sec := 'Create new Contact';
                     ----------------------------------------------------------------------------------
                     -- Create new Contact
                     ----------------------------------------------------------------------------------
                     create_contact (l_customer_id,
                                     rec_so_hdr.contact_first_name,
                                     rec_so_hdr.contact_last_name,
                                     rec_so_hdr.contact_num,
                                     NULL -- rec_so_hdr.email   -- Version# 1.3
                                         ,
                                     NULL -- rec_so_hdr.fax_num -- Version# 1.3
                                         ,
                                     l_cust_contact_id,
                                     l_error_message);
                  WHEN OTHERS
                  THEN
                     l_err_flag := 'Y';
                     l_error_message :=
                           l_error_message
                        || '-- Error deriving Customer Contact details '
                        || SQLERRM;
               END;
            END IF; -- IF rec_so_hdr.contact_first_name IS NOT NULL AND rec_so_hdr.contact_last_name IS NOT NULL THEN

            -- Version# 1.3 > Start
            l_shipto_cust_contact_id := NULL;

            IF (    rec_so_hdr.ship_to_contact_first_name IS NOT NULL
                AND rec_so_hdr.ship_to_contact_last_name IS NOT NULL)
            THEN
               l_sec := 'Ship-To Derive Contact Details';

               ----------------------------------------------------------------------------------
               -- Derive Contact Details
               ----------------------------------------------------------------------------------
               BEGIN
                  SELECT acct_role.cust_account_role_id
                    INTO l_shipto_cust_contact_id
                    FROM hz_cust_account_roles acct_role,
                         --    hz_contact_points      hcp,
                         hz_parties party,
                         hz_parties rel_party,
                         hz_relationships rel,
                         hz_org_contacts org_cont,
                         hz_cust_accounts role_acct
                   WHERE     acct_role.party_id = rel.party_id
                         -- AND hcp.owner_table_id             = acct_role.party_id
                         -- AND hcp.contact_point_type         = 'EMAIL'
                         -- AND hcp.owner_table_name           = 'HZ_PARTIES'
                         -- AND hcp.email_address              = rec_so_hdr.email
                         AND NOT EXISTS
                                    (SELECT '1'
                                       FROM hz_role_responsibility rr
                                      WHERE rr.cust_account_role_id =
                                               acct_role.cust_account_role_id)
                         AND acct_role.role_type = 'CONTACT'
                         AND org_cont.party_relationship_id =
                                rel.relationship_id
                         AND rel.subject_id = party.party_id
                         AND rel_party.party_id = rel.party_id
                         AND acct_role.cust_account_id =
                                role_acct.cust_account_id
                         AND role_acct.party_id = rel.object_id
                         AND rel.subject_table_name = 'HZ_PARTIES'
                         AND rel.object_table_name = 'HZ_PARTIES'
                         AND UPPER (party.person_first_name) =
                                UPPER (rec_so_hdr.ship_to_contact_first_name)
                         AND UPPER (party.person_last_name) =
                                UPPER (rec_so_hdr.ship_to_contact_last_name)
                         AND (    org_cont.job_title IS NULL
                              AND org_cont.job_title_code IS NULL)
                         AND party.status = 'A'
                         AND rel_party.status = 'A'
                         AND rel.status = 'A'
                         AND acct_role.status = 'A'
                         AND (role_acct.account_number = l_customer_number)
                         AND ROWNUM = 1;
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     l_sec := 'Create new Ship-TO Contact';
                     ----------------------------------------------------------------------------------
                     -- Create new Contact
                     ----------------------------------------------------------------------------------
                     create_contact (l_customer_id,
                                     rec_so_hdr.ship_to_contact_first_name,
                                     rec_so_hdr.ship_to_contact_last_name,
                                     rec_so_hdr.contact_num,
                                     NULL,
                                     NULL,
                                     l_shipto_cust_contact_id,
                                     l_error_message);
                  WHEN OTHERS
                  THEN
                     l_err_flag := 'Y';
                     l_error_message :=
                           l_error_message
                        || '-- Error deriving Ship-To Customer Contact details '
                        || SQLERRM;
               END;
            END IF; -- IF rec_so_hdr.contact_first_name IS NOT NULL AND rec_so_hdr.contact_last_name IS NOT NULL THEN

            -- Version# 1.3 < End

            IF TRIM (l_error_message) IS NOT NULL
            THEN
               fnd_file.put_line (fnd_file.LOG,
                                  '-- l_error_message ' || l_error_message);
               l_err_flag := 'Y';
            END IF;

            l_sec := 'Deliver To Address';
            ----------------------------------------------------------------------------------
            -- Deliver To Address
            ----------------------------------------------------------------------------------
            l_shipping_instructions :=
                  'Deliver To Address : '
               || CHR (10)
               || rec_so_hdr.deliver_to_address1
               || ' '
               || rec_so_hdr.deliver_to_address2
               || ' '
               || rec_so_hdr.deliver_to_address3
               || ' '
               || rec_so_hdr.deliver_to_address4
               || ' '
               || rec_so_hdr.deliver_to_city
               || ' '
               || rec_so_hdr.deliver_to_state
               || ' '
               || rec_so_hdr.deliver_to_country
               || ' '
               || rec_so_hdr.deliver_to_postal_code;

            IF rec_so_hdr.contact_num IS NOT NULL
            THEN
               l_shipping_instructions :=
                     l_shipping_instructions
                  || CHR (10)
                  || 'Contact# - '
                  || rec_so_hdr.contact_num;
            END IF;

            IF rec_so_hdr.fax_num IS NOT NULL
            THEN
               l_shipping_instructions :=
                     l_shipping_instructions
                  || CHR (10)
                  || 'Fax# - '
                  || rec_so_hdr.fax_num;
            END IF;

            -- Version# 1.5 > Start
            IF rec_so_hdr.header_notes IS NOT NULL
            THEN
               l_shipping_instructions :=
                     l_shipping_instructions
                  || CHR (10)
                  || 'Notes - '
                  || rec_so_hdr.header_notes;
            END IF;

            -- Version# 1.5 < End

            fnd_file.put_line (
               fnd_file.LOG,
                  '-- Inserting into Order Header interface tables ');
            fnd_file.put_line (fnd_file.LOG,
                               '-- l_err_flag - b4 ' || l_err_flag);
            l_sec := 'Insert Order Header details';

            ---------------------------------------------------------------------------------
            -- Insert Order Header details
            ----------------------------------------------------------------------------------
            BEGIN
               IF p_validate_only = 'N' AND l_err_flag != 'Y'
               THEN
                  l_orig_sys_document_ref :=
                     l_customer_number || rec_so_hdr.customer_po_number;
                  l_header_rec := oe_order_pub.g_miss_header_rec;
                  l_header_rec.order_type_id := 1001;
                  l_header_rec.order_source_id := l_order_source_id;
                  l_header_rec.sold_to_org_id := l_customer_id;
                  l_header_rec.ship_to_org_id := l_ship_to_site_use_id;
                  l_header_rec.invoice_to_org_id := l_bill_to_site_use_id;
                  l_header_rec.ship_from_org_id := l_inv_org_id;
                  l_header_rec.invoice_to_contact_id := l_cust_contact_id;
                  -- l_header_rec.ship_to_contact_id                  := l_cust_contact_id;      -- Version# 1.3
                  l_header_rec.ship_to_contact_id := l_shipto_cust_contact_id; -- Version# 1.3
                  l_header_rec.cust_po_number := rec_so_hdr.customer_po_number;
                  l_header_rec.operation := oe_globals.g_opr_create;
                  l_header_rec.orig_sys_document_ref :=
                     l_orig_sys_document_ref;
                  l_header_rec.shipping_instructions :=
                     l_shipping_instructions;
                  l_header_rec.ordered_date := TRUNC (SYSDATE);
                  l_header_rec.booked_flag := 'N';
                  l_header_rec.attribute2 := 'B2B';
                  l_header_rec.attribute7 := 'XXWC_INT_SALESFULFILLMENT';
                  l_header_rec.org_id := g_org_id;
                  l_header_rec.shipping_method_code := l_dflt_ship_method_code; -- Version# 1.1
                  l_header_rec.freight_carrier_code := l_freight_carrier_code; -- Version# 1.1
               END IF;                   --      IF p_validate_only = 'N' THEN
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_err_flag := 'Y';
                  l_error_message :=
                        l_error_message
                     || '-- Error inserting into Order Header interface tables';
                  fnd_file.put_line (
                     fnd_file.LOG,
                        '-- Error inserting into Order Header interface tables - '
                     || SQLERRM);
            END;

            fnd_file.put_line (
               fnd_file.LOG,
                  '-- Inserting into Order Header interface tables ');
            fnd_file.put_line (fnd_file.LOG,
                               '-- l_err_flag - after ' || l_err_flag);

            IF l_err_flag = 'Y'
            THEN
               RAISE l_exception;
            END IF;

            l_line_cnt := 0;
            l_line_tbl.DELETE;                                 -- Version# 1.2

            FOR rec_so_lns IN cur_so_lines (rec_so_hdr.customer_po_number)
            LOOP
               l_line_error_message := NULL;
               l_line_shipping_instr := NULL;                  -- Version# 1.5

               l_sec := 'Check if RequestDate is NULL';

               ---------------------------------------------------------------------------------
               -- Check if RequestDate is NULL
               ----------------------------------------------------------------------------------
               IF rec_so_lns.request_date IS NULL
               THEN
                  l_err_flag := 'Y';
                  l_line_error_message :=
                     l_line_error_message || '-- RequestDate is Empty';
               END IF;

               l_sec := 'Derive Inventory Item details';
               ---------------------------------------------------------------------------------
               -- Derive Inventory Item details
               ----------------------------------------------------------------------------------
               l_inv_item_id := NULL;
               -- Version# 1.16 > Start
               l_inventory_item_status_code := NULL;
               -- Version# 1.16 < End

               IF rec_so_lns.inventory_item IS NOT NULL
               THEN
                  BEGIN
                     SELECT inventory_item_id
                           ,inventory_item_status_code  --Added for Version# 1.16
                       INTO l_inv_item_id
                           ,l_inventory_item_status_code --Added for Version# 1.16
                       FROM mtl_system_items_b msib
                      WHERE     organization_id = l_inv_org_id
                            AND enabled_flag = 'Y'
                            AND segment1 = rec_so_lns.inventory_item;

                     -- Version# 1.16 > Start
                      SELECT inventory_item_id
                            ,inventory_item_status_code
                        INTO l_inv_item_id
                            ,l_inventory_item_status_code
                        FROM mtl_system_items_b msib
                       WHERE organization_id             = l_inv_org_id
                         AND enabled_flag                = 'Y'
                         AND customer_order_enabled_flag = 'Y'
                         AND segment1                    = rec_so_lns.inventory_item;
                     -- Version# 1.16 < End

                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        l_err_flag := 'Y';
                        l_line_error_message := '-- Invalid Item '
                           || rec_so_lns.inventory_item    --Added for Version# 1.16
                           || ' Item Status '              --Added for Version# 1.16
                           || l_inventory_item_status_code --Added for Version# 1.16
                           || '   Error - '                --Added for Version# 1.16
                           || SUBSTR(SQLERRM, 1000);       --Added for Version# 1.16
                        fnd_file.put_line (
                           fnd_file.LOG,
                              'Error validating item - '
                           || rec_so_lns.inventory_item
                           || ' Item Status '              --Added for Version# 1.16
                           || l_inventory_item_status_code --Added for Version# 1.16
                           || '   Error - '
                           || SQLERRM);
                  END;
               ELSE
                  l_err_flag := 'Y';
                  l_line_error_message :=
                     l_line_error_message || '-- Invalid Item';
               END IF;

               IF     p_validate_only = 'N'
                  AND l_err_flag != 'Y'
                  AND NVL (rec_so_lns.ordered_quantity, 0) != 0
               THEN
                  BEGIN
                     l_sec := 'Insert Order Item Line';
                     ---------------------------------------------------------------------------------
                     -- Insert Order Item Line
                     ----------------------------------------------------------------------------------
                     l_line_cnt := l_line_cnt + 1;

                     l_line_tbl (l_line_cnt) := OE_ORDER_PUB.G_MISS_LINE_REC;
                     l_line_tbl (l_line_cnt).orig_sys_document_ref :=
                        l_orig_sys_document_ref;
                     l_line_tbl (l_line_cnt).orig_sys_line_ref :=
                           l_orig_sys_document_ref
                        || '_'
                        || rec_so_lns.line_number;
                     l_line_tbl (l_line_cnt).operation :=
                        OE_GLOBALS.G_OPR_CREATE;
                     l_line_tbl (l_line_cnt).line_number := l_line_cnt;
                     l_line_tbl (l_line_cnt).line_type_id := 1002;
                     l_line_tbl (l_line_cnt).inventory_item_id :=
                        l_inv_item_id;
                     l_line_tbl (l_line_cnt).ship_from_org_id := l_inv_org_id;
                     l_line_tbl (l_line_cnt).ordered_quantity :=
                        rec_so_lns.ordered_quantity;
                     l_line_tbl (l_line_cnt).unit_selling_price :=
                        ROUND (rec_so_lns.unit_selling_price, 3);
                     l_line_tbl (l_line_cnt).unit_list_price :=
                        ROUND (rec_so_lns.unit_selling_price, 3);
                     l_line_tbl (l_line_cnt).calculate_price_flag := 'P';

                     -- Version# 1.5 > Start
                     l_line_shipping_instr :=
                        'RequestDate - ' || rec_so_lns.request_date;

                     IF rec_so_lns.line_notes IS NOT NULL
                     THEN
                        l_line_shipping_instr :=
                              l_line_shipping_instr
                           || CHR (10)
                           || 'Notes - '
                           || rec_so_lns.line_notes;
                     END IF;

                     l_line_tbl (l_line_cnt).shipping_instructions :=
                        l_line_shipping_instr;
                  -- Version# 1.5 < End
                  --   l_line_tbl (l_line_cnt).attribute4 := rec_so_lns.line_seq;

                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        l_err_flag := 'Y';
                        l_error_message :=
                              l_error_message
                           || '-- Error inserting into Order Lines interface table';
                  END;
               END IF;                           -- p_validate_only = 'N' THEN

               IF l_err_flag = 'Y'
               THEN
                  BEGIN
                     l_sec := 'UPDATE staging table with Error Status';

                     ----------------------------------------------------------------------------------
                     -- UPDATE staging table with Error Status
                     ----------------------------------------------------------------------------------
                     UPDATE xxwc.xxwc_b2b_so_lines_stg_tbl stg
                        SET process_flag = 'E',
                            error_message = l_line_error_message
                      WHERE ROWID = rec_so_lns.ROWID;

                     l_sec :=
                        'Delete the Order details that have been inserted into Interface tables';

                     ----------------------------------------------------------------------------------
                     -- Delete the Order details that have been inserted into Interface tables
                     ----------------------------------------------------------------------------------
                     DELETE FROM OE_HEADERS_IFACE_ALL
                           WHERE ORIG_SYS_DOCUMENT_REF =
                                    l_orig_sys_document_ref;

                     DELETE FROM OE_LINES_IFACE_ALL
                           WHERE ORIG_SYS_DOCUMENT_REF =
                                    l_orig_sys_document_ref;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        NULL;
                  END;
               ELSE
                  BEGIN
                     l_sec := 'UPDATE staging table with Success status';

                     ----------------------------------------------------------------------------------
                     -- UPDATE staging table with Success status
                     ----------------------------------------------------------------------------------
                     UPDATE xxwc.xxwc_b2b_so_lines_stg_tbl stg
                        SET process_flag = 'V'
                      WHERE ROWID = rec_so_lns.ROWID;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        NULL;
                  END;
               END IF;
            END LOOP;                                            -- rec_so_lns

            IF l_err_flag = 'Y'
            THEN
               --      l_error_message := 'Order Line level error';
               RAISE l_exception;
            ELSE
               BEGIN
                  l_sec := 'UPDATE staging table with Success status';

                  ----------------------------------------------------------------------------------
                  -- UPDATE staging table with Success status
                  ----------------------------------------------------------------------------------
                  IF p_validate_only = 'N'
                  THEN
                     OE_ORDER_PUB.Process_Order (
                        p_api_version_number       => 1,
                        p_org_id                   => g_org_id,
                        p_init_msg_list            => FND_API.G_TRUE,
                        p_return_values            => FND_API.G_TRUE,
                        p_action_commit            => FND_API.G_TRUE,
                        x_return_status            => l_return_status,
                        x_msg_count                => l_msg_count,
                        x_msg_data                 => l_msg_data,
                        p_action_request_tbl       => l_action_request_tbl,
                        p_header_rec               => l_header_rec,
                        p_old_header_rec           => l_old_header_rec,
                        p_line_tbl                 => l_line_tbl,
                        p_old_line_tbl             => l_old_line_tbl,
                        x_header_rec               => x_header_rec,
                        x_header_val_rec           => x_header_val_rec,
                        x_Header_Adj_tbl           => x_Header_Adj_tbl,
                        x_Header_Adj_val_tbl       => x_Header_Adj_val_tbl,
                        x_Header_price_Att_tbl     => x_Header_Price_Att_Tbl,
                        x_Header_Adj_Att_tbl       => x_Header_Adj_Att_Tbl,
                        x_Header_Adj_Assoc_tbl     => x_Header_Adj_Assoc_Tbl,
                        x_Header_Scredit_tbl       => x_Header_Scredit_Tbl,
                        x_Header_Scredit_val_tbl   => x_Header_Scredit_Val_Tbl,
                        x_line_tbl                 => x_Line_Tbl,
                        x_line_val_tbl             => x_Line_Val_Tbl,
                        x_Line_Adj_tbl             => x_Line_Adj_Tbl,
                        x_Line_Adj_val_tbl         => x_Line_Adj_Val_Tbl,
                        x_Line_price_Att_tbl       => x_Line_Price_Att_Tbl,
                        x_Line_Adj_Att_tbl         => x_Line_Adj_Att_Tbl,
                        x_Line_Adj_Assoc_tbl       => x_Line_Adj_Assoc_Tbl,
                        x_Line_Scredit_tbl         => x_Line_Scredit_Tbl,
                        x_Line_Scredit_val_tbl     => x_Line_Scredit_Val_Tbl,
                        x_Lot_Serial_tbl           => x_Lot_Serial_Tbl,
                        x_Lot_Serial_val_tbl       => x_Lot_Serial_Val_Tbl,
                        x_action_request_tbl       => x_action_request_tbl);

                     COMMIT;                                                --

                     IF l_return_status = FND_API.G_RET_STS_SUCCESS
                     THEN
                        UPDATE xxwc.xxwc_b2b_so_hdrs_stg_tbl stg
                           SET process_flag = 'S', error_message = NULL
                         WHERE customer_po_number =
                                  rec_so_hdr.customer_po_number;

                        l_sec := 'Send Email Notification';
                        ------------------------------------------
                        -- Send Email Notification
                        ------------------------------------------
                        l_subject :=
                           ' B2B Order# ' || x_header_rec.order_number;
                        l_body_header :=
                           '<style type="text/css">.style1 {font-family: Arial, Helvetica, sans-serif;}</style><p class="style1">';
                        l_body_detail :=
                              '<BR> <B> '
                           || l_instance
                           || ' </B> </BR> <BR> </BR> <BR> Customer Name : <B>'         -- Version# 1.11
                           || l_customer_name
                           || ' </B> </BR> <BR> </BR> <BR> Customer Number: <B>'        -- Version# 1.11
                           || l_customer_number                                         -- Version# 1.11
                           || '</B> </BR>'
                           || ' <BR> </BR> <BR> B2B Sales Order# '
                           || x_header_rec.order_number
                           || ' for CustomerPO# '
                           || rec_so_hdr.customer_po_number
                           || ' has been created in Oracle. </BR>';
                        l_body_footer :=
                           '<BR></BR> <BR></BR> <BR> Please contact WC2020SalesSupport for any questions. </BR>';
                        l_body :=
                              l_body_header
                           || l_body_detail
                           || CHR (10)
                           || l_body_footer;

                        xxcus_misc_pkg.html_email (
                           p_to              => l_def_notif_email,   -- p_notification_email, -- Version# 1.6
                           p_from            => l_sender,
                           p_text            => 'Order Created',
                           p_subject         => l_subject,
                           p_html            => l_body,
                           p_smtp_hostname   => l_host,
                           p_smtp_portnum    => l_hostport);
                     ELSE
                        fnd_file.put_line (
                           fnd_file.LOG,
                              'Error in  OrderAPI procedure - '
                           || l_orig_sys_document_ref);

                        -- Retrieve messages
                        lv_msg_data := NULL;

                        FOR i IN 1 .. l_msg_count
                        LOOP
                           Oe_Msg_Pub.get (
                              p_msg_index       => i,
                              p_encoded         => Fnd_Api.G_FALSE,
                              p_data            => l_msg_data,
                              p_msg_index_out   => l_msg_index_out);
                           lv_msg_data :=
                                 lv_msg_data
                              || ':'
                              || SUBSTR (l_msg_data, 1, 100);
                        END LOOP;

                        l_err_flag := 'Y';
                        l_error_message := lv_msg_data;
                        fnd_file.put_line (fnd_file.LOG,
                                           'lv_msg_data - ' || lv_msg_data);

                        RAISE l_exception;
                     END IF;    -- l_return_status = FND_API.G_RET_STS_SUCCESS
                  ELSE                        -- IF p_validate_only = 'N' THEN
                     UPDATE xxwc.xxwc_b2b_so_hdrs_stg_tbl stg
                        SET process_flag = 'V', error_message = lv_msg_data
                      WHERE customer_po_number =
                               rec_so_hdr.customer_po_number;
                  END IF;                     -- IF p_validate_only = 'N' THEN
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     -- Version# 1.16 > Start
                     fnd_file.put_line ( fnd_file.LOG, 'CustPO# - ' || rec_so_hdr.customer_po_number);
                     fnd_file.put_line (fnd_file.LOG, 'Error l_sec - ' || l_sec);
                     l_error_message := l_error_message || SUBSTR(SQLERRM, 1000);
                     fnd_file.put_line (fnd_file.LOG, 'l_error_message - ' || l_error_message);

                     l_sec := 'UPDATE staging table with Error Status';

                     ----------------------------------------------------------------------------------
                     -- UPDATE staging table with Error Status
                     ----------------------------------------------------------------------------------
                     UPDATE xxwc.xxwc_b2b_so_hdrs_stg_tbl stg
                        SET process_flag  = 'E'
                          , error_message = l_error_message
                      WHERE customer_po_number = rec_so_hdr.customer_po_number;
                   -- Version# 1.16 < End
               END;
            END IF;                                -- IF l_err_flag = 'Y' THEN
         EXCEPTION
            WHEN l_exception
            THEN
               BEGIN
                  fnd_file.put_line (
                     fnd_file.LOG,
                     'CustPO# - ' || rec_so_hdr.customer_po_number);
                  fnd_file.put_line (fnd_file.LOG, 'Error l_sec - ' || l_sec);
                  l_sec := 'UPDATE staging table with Error Status';

                  ----------------------------------------------------------------------------------
                  -- UPDATE staging table with Error Status
                  ----------------------------------------------------------------------------------
                  UPDATE xxwc.xxwc_b2b_so_hdrs_stg_tbl stg
                     SET process_flag = 'E', error_message = l_error_message
                   WHERE customer_po_number = rec_so_hdr.customer_po_number;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     NULL;
               END;

               IF NVL (rec_so_hdr.process_flag, 'N') = 'N'
               THEN
                  l_sec := 'Send Email Notification';
                  ------------------------------------------
                  -- Send Email Notification
                  ------------------------------------------
                  l_subject :=
                        'Error importing Customer PO# '
                     || rec_so_hdr.customer_po_number;
                  l_body_header :=
                     '<style type="text/css">.style1 {font-family: Arial, Helvetica, sans-serif;}</style><p class="style1">';
                  l_body_detail :=
                        '<BR> <B> '
                     || l_instance
                     || ' </B> </BR> <BR> </BR> <BR> Customer Name : <B>'       -- Version# 1.11
                     || l_customer_name
                     || ' </B> </BR> <BR> </BR> <BR> Customer Number: <B>'      -- Version# 1.11
                     || l_customer_number                                       -- Version# 1.11
                     || '</B> </BR>'
                     || ' <BR> Error importing CustomerPO# '
                     || rec_so_hdr.customer_po_number
                     || '</BR>'
                     || ' <BR> Click the following link for details : '
                     || ' <a href="'
                     || p_apex_url
                     || '">Error Details</a> </BR>';
                  --                          ' <a href="http://devwcapps.hdsupply.net/pls/wcapxdev/f?p=125:1">Error Details</a> </BR>';
                  l_body_footer :=
                     '<BR></BR> <BR></BR> <BR> Please contact WC2020SalesSupport for any questions. </BR>';
                  l_body :=
                        l_body_header
                     || l_body_detail
                     || CHR (10)
                     || l_body_footer;

                  xxcus_misc_pkg.html_email (p_to              => l_def_notif_email,   -- p_notification_email, -- Version# 1.6
                                             p_from            => l_sender,
                                             p_text            => 'test',
                                             p_subject         => l_subject,
                                             p_html            => l_body,
                                             p_smtp_hostname   => l_host,
                                             p_smtp_portnum    => l_hostport);
               END IF;
         END;                                                    -- rec_so_hdr

/*
         -- << Added for Rev 1.6 Begin
         BEGIN
            XXWC_B2B_SO_IB_PKG.Generate_POA_KPT (
               l_errbuf,
               l_retcode,
               g_org_id,
               rec_so_hdr.customer_po_number,
               'N');

            IF l_retcode = 2
            THEN
               l_error_message := l_errbuf;
               RAISE l_exception;
            END IF;
         END;

      -- Added for Rev 1.6 End >>
*/

      END LOOP;                                                  -- rec_so_hdr
      COMMIT;

      -- Version# 1.6 > Start
      Generate_POA_KPT (p_errbuf,
                        p_retcode,
                        g_org_id,
                        NULL,                          -- p_cust_po_number
                        'N'                            -- p_resend_flag
                        );

      IF l_retcode = 2
      THEN
         l_error_message := l_errbuf;
         RAISE l_exception;
      END IF;
      -- Version# 1.6 < End

   EXCEPTION
      WHEN l_exception
      THEN
         fnd_file.put_line (
            fnd_file.LOG,
            'In L_EXCEPTION, ErrorMessage - ' || l_error_message);
      WHEN OTHERS
      THEN
         ROLLBACK;

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_B2B_SO_IB_PKG.LOAD_INTERFACE',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          => 'Error in B2B Customer LOAD_INTERFACE procedure, When Others Exception',
            p_distribution_list   => g_dflt_email,
            p_module              => 'XXWC');

         fnd_file.put_line (
            fnd_file.LOG,
            'Error in  LOAD_INTERFACE procedure - ' || SQLERRM);
   END load_interface;

   /*************************************************************************
      PROCEDURE Name: submit_job

      PURPOSE:   To create contact when contact does not exist in Oracle

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------
      1.0        04/09/2014  Gopi Damuluri             Initial Version
      1.14       04/21/2016  Gopi Damuluri             TMS# 20160421-00033 - Remove Apex URL Hard Coding in PO Inbound Process
   ****************************************************************************/
   PROCEDURE submit_job (p_errbuf                   OUT VARCHAR2,
                         p_retcode                  OUT NUMBER,
                         p_user_name             IN     VARCHAR2,
                         p_responsibility_name   IN     VARCHAR2,
                         p_process_type          IN     VARCHAR2)
   IS
      ----------------------------------------------------------------------------------
      -- Variable definitions
      ----------------------------------------------------------------------------------
      l_req_id                NUMBER NULL;
      l_phase                 VARCHAR2 (50);
      l_status                VARCHAR2 (50);
      l_dev_status            VARCHAR2 (50);
      l_dev_phase             VARCHAR2 (50);
      l_message               VARCHAR2 (250);
      l_error_message         VARCHAR2 (3000);
      l_err_msg               VARCHAR2 (3000);
      l_err_code              NUMBER;
      l_statement             VARCHAR2 (9000);
      l_user_id               NUMBER;
      l_responsibility_id     NUMBER;
      l_resp_application_id   NUMBER;
      l_instance              VARCHAR2 (50);
      l_apex_url              VARCHAR2 (500);

      l_err_callfrom          VARCHAR2 (75) DEFAULT 'XXCUS_ERROR_PKG';
      l_err_callpoint         VARCHAR2 (75) DEFAULT 'START';
      l_distro_list           VARCHAR2 (75)
                                 DEFAULT 'HDSOracleDevelopers@hdsupply.com';
      l_sec                   VARCHAR2 (100);
   BEGIN
      l_sec := 'Derive UserId';

      ----------------------------------------------------------------------------------
      -- Derive UserId
      ----------------------------------------------------------------------------------
      BEGIN
         SELECT user_id
           INTO l_user_id
           FROM fnd_user
          WHERE     user_name = UPPER (p_user_name)
                AND SYSDATE BETWEEN start_date
                                AND NVL (end_date, TRUNC (SYSDATE) + 1);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_err_msg :=
               'UserName ' || p_user_name || ' not defined in Oracle';
            RAISE PROGRAM_ERROR;
         WHEN OTHERS
         THEN
            l_err_msg :=
               'Error deriving user_id for UserName - ' || p_user_name;
            RAISE PROGRAM_ERROR;
      END;

      l_sec := 'Derive Instance';

      ----------------------------------------------------------------------------------
      -- Derive Instance
      ----------------------------------------------------------------------------------
      BEGIN
         SELECT SUBSTR (UPPER (instance_name), 1, 7)
           INTO l_instance
           FROM v$instance;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_instance := NULL;
      END;

      l_apex_url := NULL;


      -- Version# 1.14 > Start
      l_apex_url := fnd_profile.VALUE ('XXWC_WCAPPS_APEX_URL');
      l_apex_url := l_apex_url||'125:1::::::';
      /* IF (l_instance = 'EBSDEV' OR l_instance = 'EBSQA')
      THEN
         l_apex_url := 'http://devwcapps.hdsupply.net/pls/wcapxdev/f?p=125:1';
      ELSIF l_instance = 'EBSPRD'
      THEN
         l_apex_url := 'http://wcapps.hdsupply.net/pls/wcapxprd/f?p=125:1';
      END IF; */
      -- Version# 1.14 < End

      l_sec := 'Derive ResponsibilityId and ApplicationId';

      ----------------------------------------------------------------------------------
      -- Derive ResponsibilityId and ApplicationId
      ----------------------------------------------------------------------------------
      BEGIN
         SELECT responsibility_id, application_id
           INTO l_responsibility_id, l_resp_application_id
           FROM fnd_responsibility_vl
          WHERE     responsibility_name = p_responsibility_name
                AND SYSDATE BETWEEN start_date
                                AND NVL (end_date, TRUNC (SYSDATE) + 1);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_err_msg :=
                  'Responsibility '
               || p_responsibility_name
               || ' not defined in Oracle';
            RAISE PROGRAM_ERROR;
         WHEN OTHERS
         THEN
            l_err_msg :=
                  'Error deriving Responsibility_id for '
               || p_responsibility_name;
            RAISE PROGRAM_ERROR;
      END;

      l_sec := 'Apps Initialize';
      ----------------------------------------------------------------------------------
      -- Apps Initialize
      ----------------------------------------------------------------------------------
      FND_GLOBAL.APPS_INITIALIZE (l_user_id,
                                  l_responsibility_id,
                                  l_resp_application_id);

      l_sec :=
         'Submitting program XXWC B2B Inbound SO Interface';
      ----------------------------------------------------------------------------------
      -- Submitting program XXWC B2B Inbound SO Interface
      ----------------------------------------------------------------------------------
      l_req_id :=
         fnd_request.submit_request (
            application   => 'XXWC',
            program       => 'XXWC_B2B_SO_IB_INTF',
            description   => NULL,
            start_time    => SYSDATE,
            sub_request   => FALSE,
            argument1     => 'N',
            argument2     => 'WC-B2Bprogram-U1@HDSupply.com',
            argument3     => NULL,
            argument4     => l_apex_url);

      COMMIT;

      p_errbuf := l_req_id;

      fnd_file.put_line (
         fnd_file.LOG,
         'Concurrent Program Request Submitted. Request ID: ' || l_req_id);

      IF (l_req_id != 0)
      THEN
         IF fnd_concurrent.wait_for_request (l_req_id,
                                             6,
                                             3600 -- 04/17/2012 Changed from 15000
                                                 ,
                                             l_phase,
                                             l_status,
                                             l_dev_phase,
                                             l_dev_status,
                                             l_message)
         THEN
            l_error_message :=
                  'ReqID:'
               || l_req_id
               || '-DPhase:'
               || l_dev_phase
               || '-DStatus:'
               || l_dev_status
               || CHR (10)
               || 'MSG:'
               || l_message;

            -- Error Returned
            IF l_dev_phase != 'COMPLETE' OR l_dev_status != 'NORMAL'
            THEN
               l_statement :=
                     'EBS Conc Prog Completed with problems '
                  || l_error_message
                  || '.';
               l_err_msg := l_statement;
               fnd_file.put_line (fnd_file.LOG, l_statement);
               RAISE PROGRAM_ERROR;
            --            ELSE
            --                 p_retcode := 1;
            END IF;
         ELSE
            l_statement := 'EBS Conc Program Wait timed out';
            l_err_msg := l_statement;
            fnd_file.put_line (fnd_file.LOG, l_statement);
            RAISE PROGRAM_ERROR;
         END IF;
      ELSE
         l_statement := 'EBS Conc Program not initated';
         l_err_msg := l_statement;
         fnd_file.put_line (fnd_file.LOG, l_statement);
         RAISE PROGRAM_ERROR;
      END IF;
   EXCEPTION
      WHEN PROGRAM_ERROR
      THEN
         ROLLBACK;
         l_err_code := 2;
         l_err_msg :=
            SUBSTR ( (l_err_msg || ' ERROR ' || SUBSTR (SQLERRM, 1, 2000)),
                    1,
                    3000);
         fnd_file.put_line (fnd_file.LOG, l_err_msg);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_B2B_SO_IB_PKG.SUBMIT_JOB',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (SQLERRM, 1, 2000),
            p_error_desc          => l_err_msg,
            p_distribution_list   => g_dflt_email,
            p_module              => 'XXWC');
         p_retcode := l_err_code;
         p_errbuf := l_err_msg;
      WHEN OTHERS
      THEN
         l_err_code := 2;
         l_err_msg :=
            SUBSTR ( (l_err_msg || ' ERROR ' || SUBSTR (SQLERRM, 1, 2000)),
                    1,
                    3000);
         fnd_file.put_line (fnd_file.LOG, l_err_msg);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_B2B_SO_IB_PKG.SUBMIT_JOB',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (SQLERRM, 1, 2000),
            p_error_desc          => 'Error in B2B Customer SUBMIT_JOB procedure, When Others Exception',
            p_distribution_list   => g_dflt_email,
            p_module              => 'XXWC');

         p_retcode := l_err_code;
         p_errbuf := l_err_msg;

         fnd_file.put_line (
            fnd_file.LOG,
            'Error in  LOAD_INTERFACE procedure - ' || SQLERRM);
   END submit_job;

   /*************************************************************************
      PROCEDURE Name: load_staging

      PURPOSE:   To load files from Liaison to Staging Tables

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------
      1.0        04/09/2014  Gopi Damuluri             Initial Version
   ****************************************************************************/
   /*
      PROCEDURE load_staging(p_errbuf                      OUT VARCHAR2,
                             p_retcode                     OUT NUMBER)
         IS
           l_err_msg               VARCHAR2(3000);
           l_err_code              NUMBER;
           l_directory_name        VARCHAR2(50);
           l_exception             EXCEPTION;
      BEGIN

         ----------------------------------------------------------------------------------
         -- Read file names from XXWC_OM_SO_IB_FILES_TBL
         ----------------------------------------------------------------------------------
         FOR rec IN (SELECT file_name FROM xxwc.xxwc_om_so_ib_files_tbl WHERE file_name like 'XXWC_B2B%') LOOP

           BEGIN

            fnd_file.put_line (fnd_file.LOG,'###########################################################===');
            fnd_file.put_line (fnd_file.LOG,'FileName'|| rec.file_name);
            ----------------------------------------------------------------------------------
            -- Rename file to 'WC_B2B_SO.TXT'
            ----------------------------------------------------------------------------------
             BEGIN
                UTL_FILE.FRENAME (l_directory_name,
                                  rec.file_name,
                                  l_directory_name,
                                  'WC_B2B_SO.TXT');
             EXCEPTION
             WHEN OTHERS THEN
                fnd_file.put_line (fnd_file.LOG,'Exception...'|| rec.file_name|| ' file does not exist to rename...'||SQLERRM);
                RAISE l_exception;
             END;

            ----------------------------------------------------------------------------------
            -- Inserting into XXWC_B2B_SO_HDRS_STG_TBL
            ----------------------------------------------------------------------------------
             BEGIN
             INSERT INTO XXWC.XXWC_B2B_SO_HDRS_STG_TBL (CUSTOMER_PO_NUMBER
                  , ORDER_TOTAL
                  , CUSTOMER_NUMBER
                  , SHIP_TO_NUMBER
                  , SHIP_TO_ADDRESS1
                  , SHIP_TO_ADDRESS2
                  , SHIP_TO_ADDRESS3
                  , SHIP_TO_ADDRESS4
                  , SHIP_TO_CITY
                  , SHIP_TO_STATE
                  , SHIP_TO_COUNTRY
                  , SHIP_TO_POSTAL_CODE
                  , BILL_TO_ADDRESS1
                  , BILL_TO_ADDRESS2
                  , BILL_TO_ADDRESS3
                  , BILL_TO_ADDRESS4
                  , BILL_TO_CITY
                  , BILL_TO_STATE
                  , BILL_TO_COUNTRY
                  , BILL_TO_POSTAL_CODE
                  , DELIVER_TO_ADDRESS1
                  , DELIVER_TO_ADDRESS2
                  , DELIVER_TO_ADDRESS3
                  , DELIVER_TO_ADDRESS4
                  , DELIVER_TO_CITY
                  , DELIVER_TO_STATE
                  , DELIVER_TO_COUNTRY
                  , DELIVER_TO_POSTAL_CODE
                  , SHIPPING_INSTRUCTIONS
                  , CONTACT_FIRST_NAME
                  , CONTACT_LAST_NAME
                  , EMAIL
                  , CONTACT_NUM
                  , FAX_NUM
                  , PROCESS_FLAG
                  , CREATION_DATE
                  , CREATED_BY
                  , LAST_UPDATE_DATE
                 , LAST_UPDATED_BY)
             SELECT CUSTOMER_PO_NUMBER
                 , ORDER_TOTAL
                 , CUSTOMER_NUMBER
                 , SHIP_TO_NUMBER
                 , SHIP_TO_ADDRESS1
                 , SHIP_TO_ADDRESS2
                 , SHIP_TO_ADDRESS3
                 , SHIP_TO_ADDRESS4
                 , SHIP_TO_CITY
                 , SHIP_TO_STATE
                 , SHIP_TO_COUNTRY
                 , SHIP_TO_POSTAL_CODE
                 , BILL_TO_ADDRESS1
                 , BILL_TO_ADDRESS2
                 , BILL_TO_ADDRESS3
                 , BILL_TO_ADDRESS4
                 , BILL_TO_CITY
                 , BILL_TO_STATE
                 , BILL_TO_COUNTRY
                 , BILL_TO_POSTAL_CODE
                 , DELIVER_TO_ADDRESS1
                 , DELIVER_TO_ADDRESS2
                 , DELIVER_TO_ADDRESS3
                 , DELIVER_TO_ADDRESS4
                 , DELIVER_TO_CITY
                 , DELIVER_TO_STATE
                 , DELIVER_TO_COUNTRY
                 , DELIVER_TO_POSTAL_CODE
                 , SHIPPING_INSTRUCTIONS
                 , CONTACT_FIRST_NAME
                 , CONTACT_LAST_NAME
                 , EMAIL
                 , CONTACT_NUM
                 , FAX_NUM
                 , 'N' PROCESS_FLAG
                 , SYSDATE CREATION_DATE
                 , -1 CREATED_BY
                 , SYSDATE LAST_UPDATE_DATE
                 , -1 LAST_UPDATED_BY
              FROM XXWC.XXWC_B2B_SO_HDRS_EXT_TBL;
             EXCEPTION
             WHEN OTHERS THEN
                fnd_file.put_line (fnd_file.LOG,'Error inserting into XXWC_B2B_SO_HDRS_STG_TBL...'|| SQLERRM);
                RAISE l_exception;
             END;

            ----------------------------------------------------------------------------------
            -- Inserting into XXWC_B2B_SO_LINES_STG_TBL
            ----------------------------------------------------------------------------------
             BEGIN
             INSERT INTO XXWC.XXWC_B2B_SO_LINES_STG_TBL (CUSTOMER_PO_NUMBER
                  , LINE_NUMBER
                  , INVENTORY_ITEM
                  , ORDERED_QUANTITY
                  , UNIT_OF_MEASURE
                  , UNIT_SELLING_PRICE
                  , REQUEST_DATE
                  , ITEM_DESCRIPTION
                  , CREATION_DATE
                  , LAST_UPDATE_DATE)
             SELECT CUSTOMER_PO_NUMBER
                  , LINE_NUMBER
                  , INVENTORY_ITEM
                  , ORDERED_QUANTITY
                  , UNIT_OF_MEASURE
                  , UNIT_SELLING_PRICE
                  , REQUEST_DATE
                  , ITEM_DESCRIPTION
                  , SYSDATE CREATION_DATE
                  , SYSDATE LAST_UPDATE_DATE
               FROM XXWC.XXWC_B2B_SO_LINES_EXT_TBL;
             EXCEPTION
             WHEN OTHERS THEN
                fnd_file.put_line (fnd_file.LOG,'Error inserting into XXWC_B2B_SO_LINES_STG_TBL...'|| SQLERRM);
                RAISE l_exception;
             END;

            ----------------------------------------------------------------------------------
            -- Archive the file by pre-fixing ARCH_ to the file name
            ----------------------------------------------------------------------------------
             BEGIN
                UTL_FILE.FRENAME (l_directory_name,
                                  'WC_B2B_SO.TXT',
                                  l_directory_name,
                                  'ARCH_'||rec.file_name);
             EXCEPTION
             WHEN OTHERS THEN
                fnd_file.put_line (fnd_file.LOG,'Error while archiving the file - '||SQLERRM);
                RAISE l_exception;
             END;

           COMMIT;
           EXCEPTION
           WHEN l_exception THEN
              ROLLBACK;
           END;
         END LOOP;
         fnd_file.put_line (fnd_file.LOG,'###########################################################===');

      EXCEPTION
      WHEN OTHERS THEN
         fnd_file.put_line(fnd_file.log,'Error in  LOAD_STAGING procedure - '||SQLERRM);
         xxcus_error_pkg.xxcus_error_main_api(p_called_from       => 'XXWC_B2B_SO_IB_PKG.LOAD_STAGING'
                                             ,p_calling           => DBMS_UTILITY.FORMAT_ERROR_BACKTRACE
                                             ,p_request_id        => fnd_global.conc_request_id
                                             ,p_ora_error_msg     => substr(SQLERRM, 1, 2000)
                                             ,p_error_desc        => 'Error in B2B Customer LOAD_STAGING procedure, When Others Exception'
                                             ,p_distribution_list => g_dflt_email
                                             ,p_module            => 'XXWC');

          p_retcode := 2;
          p_errbuf := substr(SQLERRM, 1, 2000);
      END load_staging;
   */

   /*************************************************************************
      PROCEDURE Name: uc4_call

      PURPOSE:   To load files from XXIFACE Directory to Staging Tables

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------
      1.0        04/09/2014  Gopi Damuluri             Initial Version
   ****************************************************************************/
   PROCEDURE uc4_call (p_errbuf              OUT VARCHAR2,
                       p_retcode             OUT VARCHAR2,
                       p_conc_prog_name   IN     VARCHAR2,
                       p_user_name        IN     VARCHAR2,
                       p_resp_name        IN     VARCHAR2,
                       p_b2b_email        IN     VARCHAR2,
                       p_apex_url         IN     VARCHAR2)
   AS
      l_request_id          NUMBER;
      l_phase               VARCHAR2 (80);
      l_status              VARCHAR2 (80);
      l_dev_phase           VARCHAR2 (80);
      l_dev_status          VARCHAR2 (80);
      l_message             VARCHAR2 (4000);
      l_success             BOOLEAN;

      l_user_id             NUMBER;
      l_resp_id             NUMBER;
      l_appl_id             NUMBER := 660;                  -- OrderManagement
      l_ou_id               NUMBER := 162;              -- HDS White Cap - Org
      l_exception           EXCEPTION;

      -- File Variables
      l_data_file           VARCHAR2 (200);
      l_data_file_path      VARCHAR2 (200);
      l_control_file        VARCHAR2 (200);
      l_control_file_path   VARCHAR2 (200);
      l_log_file_path       VARCHAR2 (200);
      l_sec                 VARCHAR2 (100);
   BEGIN
      p_errbuf := 'Success';
      p_retcode := '0';

      l_sec := 'Derive UserId';

      ----------------------------------------------------------------------------------
      -- Derive UserId
      ----------------------------------------------------------------------------------
      BEGIN
         SELECT user_id
           INTO l_user_id
           FROM fnd_user
          WHERE user_name = p_user_name;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_user_id := 15986;
      END;

      l_sec := 'Derive ResponsibilityId';

      ----------------------------------------------------------------------------------
      -- Derive ResponsibilityId
      ----------------------------------------------------------------------------------
      BEGIN
         SELECT responsibility_id
           INTO l_resp_id
           FROM fnd_responsibility_vl
          WHERE     responsibility_name = p_resp_name
                AND application_id = l_appl_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_resp_id := 50857;            -- HDS Order Mgmt Pricing Full - WC
      END;

      l_sec := 'Derive OperatingUnitId';

      ----------------------------------------------------------------------------------
      -- Derive OperatingUnitId
      ----------------------------------------------------------------------------------
      BEGIN
         SELECT organization_id
           INTO l_ou_id
           FROM hr_operating_units
          WHERE short_code = 'HDSWCORG';
      EXCEPTION
         WHEN OTHERS
         THEN
            l_ou_id := 162;                             -- HDS White Cap - Org
      END;

      l_sec := 'Apps Initialize';
      ----------------------------------------------------------------------------------
      -- Apps Initialize
      ----------------------------------------------------------------------------------
      apps.fnd_global.apps_initialize (user_id        => l_user_id,
                                       resp_id        => l_resp_id,
                                       resp_appl_id   => l_appl_id);

      apps.mo_global.set_policy_context ('S', l_ou_id);
      apps.mo_global.init ('ONT');

      IF p_conc_prog_name = 'XXWCGLP'
      THEN
         BEGIN
            SELECT attribute1 dat_file,
                   attribute2 dat_file_path,
                   attribute3 ctl_file,
                   attribute4 ctl_file_path,
                   attribute5 log_file_path
              INTO l_data_file,
                   l_data_file_path,
                   l_control_file,
                   l_control_file_path,
                   l_log_file_path
              FROM fnd_lookup_values
             WHERE     lookup_type = 'XXWC_LOADER_COMMON_LOOKUP'
                   AND LANGUAGE = USERENV ('LANG')
                   AND MEANING = 'XXWC_B2B_SO_HDRS_STG_TBL_CTL'
                   AND ENABLED_FLAG = 'Y'
                   AND TRUNC (SYSDATE) BETWEEN TRUNC (START_DATE_ACTIVE)
                                           AND TRUNC (
                                                  NVL (END_DATE_ACTIVE,
                                                       SYSDATE));
         EXCEPTION
            WHEN OTHERS
            THEN
               fnd_file.put_line (fnd_file.LOG,
                                  'In Others Of Common Lookup..' || SQLERRM);
         --v_errordetails := 'In Others Of Common Lookup..' || SQLERRM;
         END;

         l_sec := 'Call Concurrent Program - XXWC Genric Loader Program';
         ----------------------------------------------------------------------------------
         -- Call Concurrent Program - XXWC Genric Loader Program
         ----------------------------------------------------------------------------------
         l_request_id :=
            fnd_request.submit_request (application   => 'XXWC',
                                        program       => p_conc_prog_name,
                                        description   => NULL,
                                        start_time    => SYSDATE,
                                        sub_request   => FALSE,
                                        argument1     => l_data_file,
                                        argument2     => l_data_file_path,
                                        argument3     => l_control_file,
                                        argument4     => l_control_file_path,
                                        argument5     => l_log_file_path);
      ELSIF p_conc_prog_name = 'XXWC_B2B_SO_IB_INTF'
      THEN
         l_sec := 'Call Concurrent Program - XXWC B2B Inbound SO Interface';
         ----------------------------------------------------------------------------------
         -- Call Concurrent Program - XXWC B2B Inbound SO Interface
         ----------------------------------------------------------------------------------
         l_request_id :=
            fnd_request.submit_request (application   => 'XXWC',
                                        program       => p_conc_prog_name,
                                        description   => NULL,
                                        start_time    => SYSDATE,
                                        sub_request   => FALSE,
                                        argument1     => 'N'  -- Validate Only
                                                            ,
                                        argument2     => p_b2b_email -- Default Notification Email
                                                                    ,
                                        argument3     => NULL -- Customer PO Number
                                                             ,
                                        argument4     => p_apex_url);
      ELSIF p_conc_prog_name = 'XXWC_B2B_INV_INTF'
      THEN
         l_sec :=
            'Call Concurrent Program - XXWC B2B Invoice Outbound Interface';

         l_data_file_path := NULL;

         BEGIN
            SELECT DEFAULT_VALUE
              INTO l_data_file_path
              FROM fnd_descr_flex_col_usage_vl
             WHERE     1 = 1
                   AND descriptive_flexfield_name = '$SRS$.XXWC_B2B_INV_INTF'
                   AND application_column_name = 'ATTRIBUTE1';
         EXCEPTION
            WHEN OTHERS
            THEN
               l_data_file_path := 'XXWC_B2B_INV_OB_DIR';
         END;

         ----------------------------------------------------------------------------------
         -- Call Concurrent Program - XXWC B2B Invoice Outbound Interface
         ----------------------------------------------------------------------------------
         l_request_id :=
            fnd_request.submit_request (
               application   => 'XXWC',
               program       => p_conc_prog_name,
               description   => NULL,
               start_time    => SYSDATE,
               sub_request   => FALSE,
               argument1     => l_data_file_path         -- Outbound Directory
                                                ,
               argument2     => TO_CHAR (TRUNC (SYSDATE) - 1,
                                         'YYYY/MM/DD HH24:MI:SS') -- From Date
                                                                 ,
               argument3     => TO_CHAR (TRUNC (SYSDATE),
                                         'YYYY/MM/DD HH24:MI:SS')   -- To Date
                                                                 ,
               argument4     => NULL);
-- Version# 1.9 > Start
      ELSIF p_conc_prog_name = 'XXWC_B2B_INSERT_POD_INFO'
      THEN
         l_sec := 'Call Concurrent Program - XXWC B2B Insert POD Information';

         ----------------------------------------------------------------------------------
         -- Call Concurrent Program - XXWC B2B Insert POD Information
         ----------------------------------------------------------------------------------
         l_request_id :=
            fnd_request.submit_request (
               application   => 'XXWC',
               program       => p_conc_prog_name,
               description   => 'XXWC B2B Insert POD Information',
               start_time    => SYSDATE,
               sub_request   => FALSE);
-- Version# 1.9 < End
      END IF;

      COMMIT;

      IF NVL (l_request_id, 0) = 0
      THEN
         -- submission failed
         DBMS_OUTPUT.PUT_LINE ('Unable to submit concurrent program');
         RAISE l_exception;
      ELSE
         DBMS_OUTPUT.PUT_LINE ('Concurrent Request = ' || l_request_id);
      END IF;

      l_sec := 'Wait for Concurrent Program';

      ----------------------------------------------------------------------------------
      -- Wait for Concurrent Program
      ----------------------------------------------------------------------------------
      IF fnd_concurrent.wait_for_request (request_id   => l_request_id,
                                          interval     => 60,
                                          max_wait     => 0,
                                          phase        => l_phase,
                                          status       => l_status,
                                          dev_phase    => l_dev_phase,
                                          dev_status   => l_dev_status,
                                          MESSAGE      => l_message)
      THEN
         DBMS_OUTPUT.put_line (
            'Phase = ' || l_phase || ' and status = ' || l_status);
         DBMS_OUTPUT.put_line ('Request Completion Message = ' || l_message);

         -- return completion information
         IF l_dev_phase = 'COMPLETE'
         THEN
            CASE l_dev_status
               WHEN 'NORMAL'
               THEN
                  p_errbuf := l_status;
                  p_retcode := '0';
               WHEN 'ERROR'
               THEN
                  p_errbuf := l_status;
                  p_retcode := '2';
               WHEN 'WARNING'
               THEN
                  p_errbuf := l_status;
                  p_retcode := '1';
               ELSE
                  p_errbuf := l_status;
                  p_retcode := '2';
            END CASE;
         ELSE
            DBMS_OUTPUT.put_line (
               'Request Has Not Completed prior to wait process max_wait');
            DBMS_OUTPUT.put_line (
                  'DEV Phase = '
               || l_dev_phase
               || ' and DEV status = '
               || l_dev_status);
            p_errbuf := l_status;
            p_retcode := '1';
         END IF;
      ELSE
         DBMS_OUTPUT.put_line (
            'Unable to determine completion status of concurrent program');
         p_errbuf := 'Unable to determine completion status';
         p_retcode := '2';
      END IF;
   EXCEPTION
      WHEN l_exception
      THEN
         p_errbuf := 'Error';
         p_retcode := '2';
      WHEN OTHERS
      THEN
         p_errbuf := 'Error';
         p_retcode := '2';
         DBMS_OUTPUT.put_line (SQLERRM);
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_B2B_SO_IB_PKG.UC4_CALL',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (SQLERRM, 1, 2000),
            p_error_desc          => 'Error in B2B Customer UC4_CALL procedure, When Others Exception',
            p_distribution_list   => g_dflt_email,
            p_module              => 'XXWC');
   END uc4_call;

   -- Version# 1.6 > Start
   /*************************************************************************
       PROCEDURE Name: Generate_POA

       PURPOSE:   To Generate POA file


       REVISIONS:
       Ver        Date        Author                     Description
       ---------  ----------  ---------------         -------------------------
       1.6        06/11/2015  Manjula Chellappan      TMS# XXXXXX B2B for POA
       1.7        09/21/2015  Gopi Damuluri           TMS# 20150921-00334
                                                      Resolve error while "Validate Customer POA Delivery Flag".
       1.18       10/27/2016  Rakesh Patel            TMS#20161010-00242 --  Change SOA and ASN to reflect priced copy settings on B2B Setup page
    ***************************************************************************/
   PROCEDURE Generate_POA_KPT (errbuf              OUT VARCHAR2,
                               retcode             OUT VARCHAR2,
                               p_org_id           IN     NUMBER,
                               p_cust_po_number   IN     VARCHAR2,
                               p_resend_flag      IN     VARCHAR2)
   IS
      CURSOR cur_so_headers (p_po_number VARCHAR2,p_resend_flag VARCHAR2 )
      IS
-- Version# 1.7 > Start
         SELECT *
           FROM XXWC.XXWC_B2B_SO_HDRS_STG_TBL
          WHERE customer_po_number = p_po_number
            AND deliver_poa = DECODE(p_resend_flag,'N', 0,deliver_poa)
         UNION
         SELECT *
           FROM XXWC.XXWC_B2B_SO_HDRS_STG_TBL
          WHERE p_po_number IS NULL
            AND deliver_poa = DECODE(p_resend_flag,'N', 0,deliver_poa)
            AND TRUNC(creation_date) = TRUNC(SYSDATE)
            ;

--         SELECT *
--           FROM XXWC.XXWC_B2B_SO_HDRS_STG_TBL
--          WHERE (p_po_number IS NULL OR customer_po_number = p_po_number)
--            AND deliver_poa = DECODE(p_resend_flag,'N', 0,deliver_poa) ;
-- Version# 1.7 < End

      CURSOR cur_so_lines (p_po_number VARCHAR2)
      IS
         SELECT *
           FROM XXWC.XXWC_B2B_SO_LINES_STG_TBL
          WHERE customer_po_number = p_po_number;

      l_directory_name          VARCHAR2 (50) := 'XXWC_B2B_POA_DIR';
      l_filename                VARCHAR2 (50);
      l_file                    UTL_FILE.file_type;
      l_error_msg               VARCHAR2 (2000);
      l_poa_flag                VARCHAR2 (1);
      l_exception               EXCEPTION;
      l_tp_name                 VARCHAR2 (10);
      l_ship_from               VARCHAR2 (40) := 'HDS CONSTRUCTION AND INDUSTRIAL';
      l_ship_from_address1      VARCHAR2 (240) := '501 W Church St';
      l_ship_from_address2      VARCHAR2 (240);
      l_ship_from_address3      VARCHAR2 (240);
      l_ship_from_address4      VARCHAR2 (240);
      l_ship_from_city          VARCHAR2 (30) := 'ORLANDO';
      l_ship_from_state         VARCHAR2 (60) := 'FL';
      l_ship_from_country       VARCHAR2 (60) := 'USA';
      l_ship_from_postal_code   VARCHAR2 (30) := '32805';
      l_line_type               VARCHAR2 (10);
      l_delimiter               VARCHAR2 (5) := '' || '|' || '';
      g_dflt_email              VARCHAR2 (100)
                                   := 'HDSOracleDevelopers@hdsupply.com';
      l_sec                     VARCHAR2 (200);
   BEGIN

      fnd_file.put_line (fnd_file.LOG, ' Begining of Procedure - GENERATE_POA_KPT ');

      IF p_cust_po_number IS NOT NULL THEN
         fnd_file.put_line (fnd_file.LOG, ' P_CUST_PO_NUMBER - '||p_cust_po_number);
      END IF;

      FOR rec_so_headers IN cur_so_headers (p_cust_po_number,p_resend_flag)
      LOOP
         fnd_file.put_line (fnd_file.LOG, ' Loop# 1  ');
         l_sec := 'Validate Customer POA Delivery Flag ';
         l_error_msg := NULL;

         BEGIN
            SELECT stg.deliver_poa, stg.tp_name
              INTO l_poa_flag, l_tp_name
              FROM xxwc.xxwc_b2b_config_tbl stg, -- Version# 1.18
                   hz_cust_accounts_all hca,
                   hz_cust_acct_sites_all hcas,
                   hz_cust_site_uses_all hcsu,
                   hz_party_sites hps,
                   hz_locations hl
             WHERE     1 = 1
                   AND hcsu.attribute10 = rec_so_headers.ship_to_number
                   AND hcas.cust_acct_site_id = hcsu.cust_acct_site_id
                   AND hcsu.site_use_code = 'SHIP_TO'
                   AND hps.location_id = hl.location_id        -- Version# 1.4
                   AND hcas.party_site_id = hps.party_site_id  -- Version# 1.4
                   AND UPPER (hl.state) = rec_so_headers.ship_to_state -- Version# 1.4
                   AND hcas.status = 'A'
                   AND hcsu.status = 'A'
                   AND hcas.org_id = p_org_id
                   AND hca.cust_account_id = hcas.cust_account_id
                   AND stg.cust_account_id = hca.cust_account_id-- Version# 1.18
                   AND stg.tp_name = 'KPT'
                   AND NVL(stg.status, 'DRAFT')  = 'APPROVED' -- Version# 1.6
                   AND SYSDATE BETWEEN NVL (stg.start_date_active,
                                            SYSDATE - 10)
                                   AND NVL (stg.end_date_active,
                                            SYSDATE + 10)
                   AND ROWNUM = 1                          -- Version# 1.7
                   ;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               l_poa_flag := 'N';
               l_error_msg := l_error_msg || '--Customer not setup to Deliver POA';
               -- RAISE l_exception;
            WHEN OTHERS
            THEN
               l_poa_flag := 'N';
               l_error_msg :=
                  l_error_msg || 'Error Validating Customer' || SQLERRM;
               RAISE l_exception;
         END;


         fnd_file.put_line (fnd_file.LOG, ' *** Customer_Po_Number *** '||rec_so_headers.customer_po_number);
         fnd_file.put_line (fnd_file.LOG, ' *** l_poa_flag *** '||l_poa_flag);
         fnd_file.put_line (fnd_file.LOG, ' *** l_tp_name *** '||l_tp_name);

         fnd_file.put_line (fnd_file.LOG, ' *** 2 *** ');

         IF l_poa_flag = 'Y'
         THEN
            DBMS_OUTPUT.put_line ('Inside SO Headers');

            l_sec := 'Generate POA File Name ';

            l_filename :=
                  l_tp_name
               || '_POA_'
               || rec_so_headers.customer_po_number
               || '_'
               || lpad(NVL(rec_so_headers.deliver_poa,0)+1,3,'0')
               || '.txt';

            l_sec := 'Open File for POA';

            l_file :=
               UTL_FILE.fopen (location    => l_directory_name,
                               filename    => l_filename,
                               open_mode   => 'w');

            l_sec := 'Write POA to File ';

            UTL_FILE.put_line (
               l_file,
                  'OH|'
               || rec_so_headers.customer_po_number
               || l_delimiter
               || rec_so_headers.order_total
               || l_delimiter
               || rec_so_headers.customer_number
               || l_delimiter
               || rec_so_headers.ship_to_number
               || l_delimiter
               || REGEXP_REPLACE (rec_so_headers.ship_to_address1,
                                  '[[:cntrl:]]',
                                  ' ')
               || l_delimiter
               || REGEXP_REPLACE (rec_so_headers.ship_to_address2,
                                  '[[:cntrl:]]',
                                  ' ')
               || l_delimiter
               || REGEXP_REPLACE (rec_so_headers.ship_to_address3,
                                  '[[:cntrl:]]',
                                  ' ')
               || l_delimiter
               || REGEXP_REPLACE (rec_so_headers.ship_to_address4,
                                  '[[:cntrl:]]',
                                  ' ')
               || l_delimiter
               || rec_so_headers.ship_to_city
               || l_delimiter
               || rec_so_headers.ship_to_state
               || l_delimiter
               || rec_so_headers.ship_to_country
               || l_delimiter
               || rec_so_headers.ship_to_postal_code
               || l_delimiter
               || REGEXP_REPLACE (rec_so_headers.bill_to_address1,
                                  '[[:cntrl:]]',
                                  ' ')
               || l_delimiter
               || REGEXP_REPLACE (rec_so_headers.bill_to_address2,
                                  '[[:cntrl:]]',
                                  ' ')
               || l_delimiter
               || REGEXP_REPLACE (rec_so_headers.bill_to_address3,
                                  '[[:cntrl:]]',
                                  ' ')
               || l_delimiter
               || REGEXP_REPLACE (rec_so_headers.bill_to_address4,
                                  '[[:cntrl:]]',
                                  ' ')
               || l_delimiter
               || rec_so_headers.bill_to_city
               || l_delimiter
               || rec_so_headers.bill_to_state
               || l_delimiter
               || rec_so_headers.bill_to_country
               || l_delimiter
               || rec_so_headers.bill_to_postal_code
               || l_delimiter
               || REGEXP_REPLACE (rec_so_headers.deliver_to_address1,
                                  '[[:cntrl:]]',
                                  ' ')
               || l_delimiter
               || REGEXP_REPLACE (rec_so_headers.deliver_to_address2,
                                  '[[:cntrl:]]',
                                  ' ')
               || l_delimiter
               || REGEXP_REPLACE (rec_so_headers.deliver_to_address3,
                                  '[[:cntrl:]]',
                                  ' ')
               || l_delimiter
               || REGEXP_REPLACE (rec_so_headers.deliver_to_address4,
                                  '[[:cntrl:]]',
                                  ' ')
               || l_delimiter
               || rec_so_headers.deliver_to_city
               || l_delimiter
               || rec_so_headers.deliver_to_state
               || l_delimiter
               || rec_so_headers.deliver_to_country
               || l_delimiter
               || rec_so_headers.deliver_to_postal_code
               || l_delimiter
               || rec_so_headers.shipping_instructions
               || l_delimiter
               || rec_so_headers.email
               || l_delimiter
               || rec_so_headers.contact_num
               || l_delimiter
               || rec_so_headers.fax_num
               || l_delimiter
               || l_ship_from
               || l_delimiter
               || l_ship_from_address1
               || l_delimiter
               || l_ship_from_address2
               || l_delimiter
               || l_ship_from_address3
               || l_delimiter
               || l_ship_from_address4
               || l_delimiter
               || l_ship_from_city
               || l_delimiter
               || l_ship_from_state
               || l_delimiter
               || l_ship_from_country
               || l_delimiter
               || l_ship_from_postal_code);

            -- FOR rec_so_lines IN cur_so_lines (p_cust_po_number)
            FOR rec_so_lines IN cur_so_lines (rec_so_headers.customer_po_number)
            LOOP

               fnd_file.put_line (fnd_file.LOG, ' Loop# 2  ');

               l_line_type := NULL;

               l_sec := 'Validate Line Type';

               IF rec_so_lines.process_flag = 'E'
               THEN
                  l_line_type := 'reject';
               ELSE
                  l_line_type := 'accept';
               END IF;

               UTL_FILE.put_line (
                  l_file,
                     'OL|'
                  || rec_so_lines.line_number
                  || l_delimiter
                  || rec_so_lines.inventory_item
                  || l_delimiter
                  || rec_so_lines.ordered_quantity
                  || l_delimiter
                  || rec_so_lines.unit_of_measure
                  || l_delimiter
                  || rec_so_lines.unit_selling_price
                  || l_delimiter
                  || TO_CHAR (rec_so_lines.request_date,
                              'MMDDYYYY HH24:MI:SS')
                  || l_delimiter
                  || REGEXP_REPLACE (rec_so_lines.item_description,
                                     '[[:cntrl:]]',
                                     ' ')
                  || l_delimiter
                  || l_line_type);
            END LOOP;

            UTL_FILE.fclose (l_file);

            l_sec :=
               'Update deliver_poa flag in XXWC.XXWC_B2B_SO_HDRS_STG_TBL';

            BEGIN
               UPDATE XXWC.XXWC_B2B_SO_HDRS_STG_TBL
                  SET deliver_poa = 1
                    , last_update_date = SYSDATE
                WHERE customer_po_number = rec_so_headers.customer_po_number;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_error_msg :=
                     SUBSTR (
                           '--Failed to Update deliver_poa flag in XXWC.XXWC_B2B_SO_HDRS_STG_TBL--'
                        || SQLERRM,
                        1,
                        2000);
                  RAISE l_exception;
            END;

            retcode := 1;
         ELSE
            retcode := 0;
            -- errbuf := l_error_msg;
         END IF;
      END LOOP;
   EXCEPTION
      WHEN l_exception
      THEN
         errbuf := l_error_msg;
         retcode := '1';
         fnd_file.put_line (fnd_file.LOG, ' l_error_msg - '||l_error_msg);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_B2B_SO_IB_PKG.Generate_POA_KPT',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (l_error_msg, 2000),
            p_error_desc          => 'Error in generating the PO Acknowledgement for KPT Orders',
            p_distribution_list   => g_dflt_email,
            p_module              => 'XXWC');
      WHEN OTHERS
      THEN
         l_error_msg := '--' || SQLERRM;
         errbuf := l_error_msg;
         retcode := '2';
         fnd_file.put_line (fnd_file.LOG, ' l_error_msg '||l_error_msg);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_B2B_SO_IB_PKG.Generate_POA_KPT',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (l_error_msg, 2000),
            p_error_desc          => 'Error in generating the PO Acknowledgement for KPT Orders',
            p_distribution_list   => g_dflt_email,
            p_module              => 'XXWC');
   END Generate_POA_KPT;

   /****************************************************************************************************************
      FUNCTION Name: GET_SALES_REP

      PURPOSE:   To derive Salesrep Names associated to P_PARTY_ID (Customer)

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------
      1.6        06/11/2015  Gopi Damuluri           TMS# 20150615-00088
                                                     Add functions for deriving SalesRep Information
   ****************************************************************************************************************/
   FUNCTION get_sales_rep (p_cust_account_id   IN NUMBER) RETURN VARCHAR2
     AS
     l_sales_rep_name  VARCHAR2(32767);

     CURSOR cur_salesrep (p_customer_id IN NUMBER)
         IS
     SELECT DISTINCT REPLACE(jrre.source_name, ',', '')  sales_rep_name
       FROM hz_cust_acct_sites_all     hcas
          , hz_cust_site_uses_all      hcsu
          , jtf_rs_salesreps           jrs
          , jtf_rs_resource_extns      jrre
      WHERE 1 = 1
        AND hcas.cust_account_id     = p_customer_id
        AND hcas.cust_acct_site_id   = hcsu.cust_acct_site_id
        AND hcsu.primary_salesrep_id = jrs.salesrep_id
        AND hcas.status              = 'A'
        AND hcsu.status              = 'A'
        AND jrs.resource_id          = jrre.resource_id
        AND jrs.org_id               = hcas.org_id
        AND jrs.salesrep_id          != -3 -- No Sales Credit
        AND SYSDATE BETWEEN jrre.start_Date_active and NVL(jrre.end_date_active, SYSDATE+1)
        ;
       l_sec                     VARCHAR2 (200);
   BEGIN

     l_sec := 'Derive Salesrep Names';

     FOR rec IN cur_salesrep(p_cust_account_id) LOOP
       IF l_sales_rep_name IS NULL THEN
          l_sales_rep_name := rec.sales_rep_name;
       ELSE
           IF rec.sales_rep_name IS NOT NULL THEN
              l_sales_rep_name := rec.sales_rep_name||' ; '||l_sales_rep_name;
           END IF;
       END IF;
     END LOOP;
     RETURN l_sales_rep_name;

   EXCEPTION
   WHEN OTHERS THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_B2B_SO_IB_PKG.GET_SALES_REP',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (SQLERRM, 2000),
            p_error_desc          => 'Error in deriving Salesrep Names ',
            p_distribution_list   => g_dflt_email,
            p_module              => 'XXWC');

     RETURN NULL;
   END get_sales_rep;

   /****************************************************************************************************************
      FUNCTION Name: GET_SALES_REP_EMAIL

      PURPOSE:   To derive emails of all Salesreps associated to P_PARTY_ID (Customer)

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------
      1.6        06/11/2015  Gopi Damuluri           TMS# 20150615-00088
                                                     Add functions for deriving SalesRep Information
      1.18       11/02/2016  Rakesh Patel            TMS#20161010-00242 --  Change SOA and ASN to reflect priced copy settings on B2B Setup page
   ****************************************************************************************************************/
   FUNCTION get_sales_rep_email (p_cust_account_id   IN NUMBER) RETURN VARCHAR2-- Version# 1.18
     AS
     l_source_email  VARCHAR2(2000); -- Length Changed for Ver# 1.16
     CURSOR cur_salesrep_email (p_cust_account_id IN NUMBER)-- Version# 1.18
         IS
     SELECT DISTINCT REPLACE(jrre.source_email, ',', '')  source_email
       FROM hz_cust_acct_sites_all     hcas
          , hz_cust_site_uses_all      hcsu
          , jtf_rs_salesreps           jrs
          , jtf_rs_resource_extns      jrre
      WHERE 1 = 1
        AND hcas.cust_account_id     = p_cust_account_id-- Version# 1.18
        AND hcas.cust_acct_site_id   = hcsu.cust_acct_site_id
        AND hcsu.primary_salesrep_id = jrs.salesrep_id
        AND jrs.resource_id          = jrre.resource_id
        AND jrs.org_id               = hcas.org_id
        -- AND jrs.salesrep_id          != -3 -- No Sales Credit
        AND hcas.status              = 'A'
        AND hcsu.status              = 'A'
        AND jrre.source_email        IS NOT NULL
        AND SYSDATE BETWEEN jrre.start_Date_active and NVL(jrre.end_date_active, SYSDATE+1)
        ;

       l_sec                     VARCHAR2 (200);
   BEGIN

     l_sec := 'Derive Salesrep Email-Ids';
     FOR rec IN cur_salesrep_email(p_cust_account_id) LOOP-- Version# 1.18
       IF l_source_email IS NULL THEN
          l_source_email := rec.source_email;
       ELSE
          l_source_email := rec.source_email||','||l_source_email;
       END IF;
     END LOOP;

     RETURN l_source_email;

   EXCEPTION
   WHEN OTHERS THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_B2B_SO_IB_PKG.GET_SALES_REP_EMAIL',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (SQLERRM, 2000),
            p_error_desc          => 'Error in deriving Salesrep Emails ',
            p_distribution_list   => g_dflt_email,
            p_module              => 'XXWC');

     RETURN NULL;
   END get_sales_rep_email;

   /****************************************************************************************************************
      FUNCTION Name: GET_SALES_MGR_EMAIL

      PURPOSE:   To derive all Manager emails of salesreps associated to P_CUST_ACCOUNT_ID (Customer)

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------
      1.6        06/11/2015  Gopi Damuluri           TMS# 20150615-00088
                                                     Add functions for deriving SalesRep Information
      1.8        09/21/2015  Gopi Damuluri           TMS# 20151019-00065
                                                     Allow any user to setup B2B access when Customer has no Sales Rep associated.
      1.18       11/02/2016  Rakesh Patel            TMS#20161010-00242 --  Change SOA and ASN to reflect priced copy settings on B2B Setup page
   ****************************************************************************************************************/
   FUNCTION get_sales_mgr_email ( p_cust_account_id  IN NUMBER -- Version# 1.18
                                , p_user_name        IN VARCHAR2) RETURN VARCHAR2 -- Version# 1.8
     AS
     l_source_email  VARCHAR2(2000);-- Length Changed for Ver# 1.16

     CURSOR cur_salesrep_mgr_email ( p_cust_account_id IN NUMBER -- Version# 1.18
                                   , p_salesrep_id IN NUMBER) -- Version# 1.8
         IS
-- Version# 1.8 > Start
     SELECT DISTINCT REPLACE(jrre.source_email, ',', '')  source_email
       FROM xxwc_sr_salesrep_hierarchy xsrh
          , xxwc_sr_salesreps          xss
          , jtf_rs_salesreps           jrs
          , jtf_rs_resource_extns      jrre
      WHERE 1 = 1
        AND xsrh.sr_id               = p_salesrep_id
        AND xsrh.mgr_eeid            = xss.eeid
        AND xsrh.mgt_lvl             = (SELECT min(xsrh2.mgt_lvl) from xxwc_sr_salesrep_hierarchy xsrh2 where xsrh2.sr_id = xsrh.sr_id )
        AND xss.salesrep_id          = jrs.salesrep_id
        AND jrs.resource_id          = jrre.resource_id
        AND jrs.org_id               = 162
        AND jrre.source_email        IS NOT NULL
        AND SYSDATE BETWEEN jrre.start_Date_active and NVL(jrre.end_date_active, SYSDATE+1)
        AND ROWNUM = 1
        ;

/*
     SELECT DISTINCT REPLACE(jrre.source_email, ',', '')  source_email
       FROM hz_cust_acct_sites_all     hcas
          , hz_cust_site_uses_all      hcsu
          , xxwc_sr_salesrep_hierarchy xsrh
          , xxwc_sr_salesreps          xss
          , jtf_rs_salesreps           jrs
          , jtf_rs_resource_extns      jrre
      WHERE 1 = 1
        AND hcas.cust_account_id     IN (SELECT hca.cust_account_id FROM hz_cust_accounts_all hca WHERE hca.party_id = p_party_id)
        AND hcas.cust_acct_site_id   = hcsu.cust_acct_site_id
        AND hcsu.primary_salesrep_id = xsrh.sr_id
        AND xsrh.mgr_eeid            = xss.eeid
        AND xsrh.mgt_lvl             = (SELECT min(xsrh2.mgt_lvl) from xxwc_sr_salesrep_hierarchy xsrh2 where xsrh2.sr_id = xsrh.sr_id )
        AND xss.salesrep_id          = jrs.salesrep_id
        AND jrs.resource_id          = jrre.resource_id
        AND jrs.org_id               = hcas.org_id
        AND hcas.status              = 'A'
        AND hcsu.status              = 'A'
        AND jrre.source_email        IS NOT NULL
        AND SYSDATE BETWEEN jrre.start_Date_active and NVL(jrre.end_date_active, SYSDATE+1)
        ;
*/

-- Version# 1.8 < End

       l_sec                     VARCHAR2 (200);
       l_salesrep_id             NUMBER; -- Version# 1.8

   BEGIN

-- Version# 1.8 > Start
     SELECT jrs_s.salesrep_id
       INTO l_salesrep_id
       FROM jtf_rs_salesreps           jrs_s
          , jtf_rs_resource_extns      jrre_s
      WHERE 1 = 1
        AND jrre_s.user_name         = p_user_name
        AND jrs_s.resource_id        = jrre_s.resource_id
        AND ROWNUM = 1;
-- Version# 1.8 < End

     l_sec := 'Derive Salesrep Manager Email-Ids';

     FOR rec IN cur_salesrep_mgr_email(p_cust_account_id, l_salesrep_id) LOOP -- Version# 1.8
       IF l_source_email IS NULL THEN
          l_source_email := rec.source_email;
       ELSE
          l_source_email := rec.source_email||','||l_source_email;
       END IF;
     END LOOP;

     RETURN l_source_email;

   EXCEPTION
   WHEN OTHERS THEN

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_B2B_SO_IB_PKG.GET_SALES_MGR_EMAIL',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (SQLERRM, 2000),
            p_error_desc          => 'Error in deriving Salesrep Manager Emails ',
            p_distribution_list   => g_dflt_email,
            p_module              => 'XXWC');

     RETURN NULL;
   END get_sales_mgr_email;

   /****************************************************************************************************************
      FUNCTION Name: IS_SALES_REP

      PURPOSE:   To check if P_USER_NAME is salesrep of P_PARTY_ID (Customer)

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------
      1.6        06/11/2015  Gopi Damuluri           TMS# 20150615-00088
                                                     Add functions for deriving SalesRep Information
      1.8        09/21/2015  Gopi Damuluri           TMS# 20151019-00065
                                                     Allow any user to setup B2B access when Customer has no Sales Rep associated.
      1.18       11/02/2016  Rakesh Patel            TMS#20161010-00242 --  Change SOA and ASN to reflect priced copy settings on B2B Setup page
   ****************************************************************************************************************/
   FUNCTION is_sales_rep (p_cust_account_id   IN NUMBER-- Version# 1.18
                        , p_user_name         IN VARCHAR2) RETURN VARCHAR2
     AS

     l_is_sales_rep_cnt  NUMBER;
     l_sec               VARCHAR2 (200);
   BEGIN

     -- Version# 1.8 > Start
     l_sec := 'Check if customer has atleast one SalesRep associated';
     SELECT COUNT(1)
       INTO l_is_sales_rep_cnt
       FROM hz_cust_acct_sites_all     hcas
          , hz_cust_site_uses_all      hcsu
          , jtf_rs_salesreps           jrs
          , jtf_rs_resource_extns      jrre
      WHERE 1 = 1
        AND hcas.cust_account_id     = p_cust_account_id -- Version# 1.18
        AND hcas.cust_acct_site_id   = hcsu.cust_acct_site_id
        AND hcsu.primary_salesrep_id = jrs.salesrep_id
        AND jrs.resource_id          = jrre.resource_id
        AND jrs.org_id               = hcas.org_id
        AND jrre.user_name           IS NOT NULL  -- Version# 1.8
        AND hcas.status              = 'A'
        AND hcsu.status              = 'A'
        AND SYSDATE BETWEEN jrre.start_Date_active and NVL(jrre.end_date_active, SYSDATE+1)
        ;

     IF l_is_sales_rep_cnt > 0 THEN

        l_is_sales_rep_cnt := 0;
     -- Version# 1.8 < End

        l_sec := 'Check if user is a SalesRep';

        SELECT COUNT(1)
          INTO l_is_sales_rep_cnt
          FROM hz_cust_acct_sites_all     hcas
             , hz_cust_site_uses_all      hcsu
             , jtf_rs_salesreps           jrs
             , jtf_rs_resource_extns      jrre
         WHERE 1 = 1
           AND hcas.cust_account_id     = p_cust_account_id -- Version# 1.18
           AND hcas.cust_acct_site_id   = hcsu.cust_acct_site_id
           AND hcsu.primary_salesrep_id = jrs.salesrep_id
           AND jrs.resource_id          = jrre.resource_id
           AND jrs.org_id               = hcas.org_id
           AND jrre.user_name           = p_user_name
           AND hcas.status              = 'A'
           AND hcsu.status              = 'A'
           AND SYSDATE BETWEEN jrre.start_Date_active and NVL(jrre.end_date_active, SYSDATE+1)
           ;

        IF l_is_sales_rep_cnt = 0 THEN
           RETURN '0'; -- Version# 1.8
        ELSE
           RETURN 'Y';
        END IF;

     -- Version# 1.8 > Start
     ELSE
        RETURN '0';
     END IF;
     -- Version# 1.8 < End

   EXCEPTION
   WHEN OTHERS THEN

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_B2B_SO_IB_PKG.IS_SALES_REP',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (SQLERRM, 2000),
            p_error_desc          => 'Error in checking if User is a Salesrep',
            p_distribution_list   => g_dflt_email,
            p_module              => 'XXWC');

     RETURN NULL;
   END is_sales_rep;

   /****************************************************************************************************************
      FUNCTION Name: GET_APEX_URL

      PURPOSE:   To derive the Apex On EBS URL

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------
      1.6        06/11/2015  Gopi Damuluri           TMS# 20150615-00088
                                                     Derive the Apex On EBS URL
   ****************************************************************************************************************/
   FUNCTION get_apex_url RETURN VARCHAR2
   AS
     l_sec               VARCHAR2 (200);
     l_url               VARCHAR2 (200);
   BEGIN

     l_sec := 'Derive Apex on EBS URL';

     SELECT fnd_profile.VALUE ('XXWC_APEX_ON_EBS_URL')
       INTO l_url
       FROM DUAL
       ;

     RETURN l_url;

   EXCEPTION
   WHEN OTHERS THEN

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_B2B_SO_IB_PKG.GET_APEX_URL',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (SQLERRM, 2000),
            p_error_desc          => 'Error in deriving Apex on EBS URL',
            p_distribution_list   => g_dflt_email,
            p_module              => 'XXWC');

     RETURN NULL;
   END get_apex_url;
   -- Version# 1.6 < End

   -- Version# 1.8 > Start
   /****************************************************************************************************************
      FUNCTION Name: get_sales_rep_name

      PURPOSE:   To get salesrep name when P_USER_NAME is salesrep of P_CUST_ACCOUNT_ID (Customer)

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------
      1.8        09/21/2015  Gopi Damuluri           TMS# 20151019-00065
                                                     Allow any user to setup B2B access when Customer has no Sales Rep associated.
      1.18       11/01/2016  Rakesh Patel            TMS#20161010-00242 --  Change SOA and ASN to reflect priced copy settings on B2B Setup page
   ****************************************************************************************************************/
   FUNCTION get_sales_rep_name (p_cust_account_id   IN NUMBER -- Version# 1.18
                              , p_user_name         IN VARCHAR2) RETURN VARCHAR2
     AS

     l_sales_rep_name  VARCHAR2 (200);
     l_sec             VARCHAR2 (200);
     l_rec_count       NUMBER;
   BEGIN

     IF p_cust_account_id IS NOT NULL  -- Version# 1.18
      AND p_user_name IS NOT NULL THEN
        l_sec := 'Check if user is the associated SalesRep to this customer';

        BEGIN
        SELECT REPLACE(jrre.source_name, ',', '')
          INTO l_sales_rep_name
          FROM apps.hz_cust_acct_sites_all     hcas
             , apps.hz_cust_accounts_all       hca
             , apps.hz_cust_site_uses_all      hcsu
             , apps.jtf_rs_salesreps           jrs
             , apps.jtf_rs_resource_extns      jrre
         WHERE 1 = 1
           AND hcas.cust_account_id     = hca.cust_account_id
           AND hca.cust_account_id      = p_cust_account_id -- Version# 1.18
           AND hcas.cust_acct_site_id   = hcsu.cust_acct_site_id
           AND hcsu.primary_salesrep_id = jrs.salesrep_id
           AND jrs.resource_id          = jrre.resource_id
           AND jrs.org_id               = hcas.org_id
           AND jrre.user_name           = p_user_name
           AND jrre.source_name         IS NOT NULL
           AND hcas.status              = 'A'
           AND hcsu.status              = 'A'
           AND hcas.org_id              = hcsu.org_id
           AND SYSDATE BETWEEN jrre.start_Date_active and NVL(jrre.end_date_active, SYSDATE+1)
           AND ROWNUM                   = 1
           ;
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
           -- If only one SalesRep exists make him the default
           l_sec := 'If only one SalesRep exists make him the default';
           BEGIN
              SELECT DISTINCT REPLACE(jrre.source_name, ',', '')
                INTO l_sales_rep_name
                FROM apps.hz_cust_acct_sites_all     hcas
                   , apps.hz_cust_accounts_all       hca
                   , apps.hz_cust_site_uses_all      hcsu
                   , apps.jtf_rs_salesreps           jrs
                   , apps.jtf_rs_resource_extns      jrre
               WHERE 1 = 1
                 AND hcas.cust_account_id     = hca.cust_account_id
                 AND hca.cust_account_id      = p_cust_account_id -- Version# 1.18
                 AND hcas.cust_acct_site_id   = hcsu.cust_acct_site_id
                 AND hcsu.primary_salesrep_id = jrs.salesrep_id
                 AND jrs.resource_id          = jrre.resource_id
                 AND jrs.org_id               = hcas.org_id
                 AND jrre.user_name           IS NOT NULL
                 AND jrre.source_name         IS NOT NULL
                 AND hcas.status              = 'A'
                 AND hcsu.status              = 'A'
                 AND hcas.org_id              = hcsu.org_id
                 AND SYSDATE BETWEEN jrre.start_Date_active and NVL(jrre.end_date_active, SYSDATE+1)
                 ;
           EXCEPTION
           WHEN OTHERS THEN
             NULL;
           END;
        WHEN OTHERS THEN
           NULL;
        END;
     END IF;

     IF p_cust_account_id IS NULL  -- Version# 1.18
     AND p_user_name IS NOT NULL THEN
        l_sec := 'Derive SalesRep Name for UserName provided';

        BEGIN
        SELECT REPLACE(jrre.source_name, ',', '')
          INTO l_sales_rep_name
          FROM apps.jtf_rs_salesreps           jrs
             , apps.jtf_rs_resource_extns      jrre
         WHERE 1 = 1
           AND jrre.user_name           = p_user_name
           AND jrs.resource_id          = jrre.resource_id
           AND jrs.org_id               = 162
           AND jrre.source_name         IS NOT NULL
           AND SYSDATE BETWEEN jrre.start_Date_active and NVL(jrre.end_date_active, SYSDATE+1)
           AND ROWNUM                   = 1
           ;
        EXCEPTION
        WHEN OTHERS THEN
           NULL;
        END;
     END IF;

     IF l_sales_rep_name IS NULL AND p_user_name IS NOT NULL THEN
        SELECT REPLACE(description,',', '')
          INTO l_sales_rep_name
          FROM apps.fnd_user
         WHERE user_name = p_user_name
           AND ROWNUM    = 1;
     END IF;

     RETURN l_sales_rep_name;

   EXCEPTION
   WHEN OTHERS THEN

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_B2B_SO_IB_PKG.get_sales_rep_name',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (SQLERRM, 2000),
            p_error_desc          => 'Error in checking if User is a Salesrep',
            p_distribution_list   => g_dflt_email,
            p_module              => 'XXWC');

     RETURN NULL;
   END get_sales_rep_name;
   -- Version# 1.8 < End

-- Version# 1.9 > Start

   /*************************************************************************
      PROCEDURE Name: insert_pod_info

      PURPOSE:   To insert POD File info to Staging Table.

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------
      1.0        04/09/2014  Gopi Damuluri             Initial Version
      1.10       02/15/2016  Gopi Damuluri             TMS# 20160215-00282 - Changes to B2B POD Notification email
      1.11       02/22/2016  Gopi Damuluri             TMS# 20160217-00165 - Changes to B2B POD - Include DMS Delivery_Id
    1.17       10/04/2016  Pattabhi Avula            TMS#20160926-00059 -- Index's created and Changed the update statement for performance improve
    1.18       10/27/2016  Rakesh Patel              TMS#20161010-00242 --  Change SOA and ASN to reflect priced copy settings on B2B Setup page
      1.22      08/17/2017  Pattabhi Avula           TMS#20170817-00062 -- Modified the delete statement for performance improve
	                                                 added Time stamp messages
   ****************************************************************************/
   PROCEDURE INSERT_POD_INFO (p_errbuf                   OUT VARCHAR2,
                             p_retcode                  OUT NUMBER)
   IS

      l_sec                   VARCHAR2 (100);
      l_err_msg               VARCHAR2 (3000);
      l_err_code              NUMBER;

   BEGIN

      l_sec := 'Insert POD File info to Staging Table';
	  fnd_file.put_line (fnd_file.LOG,'Time Capture Before Insert into POD info table '||TO_CHAR(SYSDATE, 'DD-MM-YYYY HH24:MI:SS')); -- Ver#1.22
      ----------------------------------------------------------------------------------
      -- Insert POD File info to Staging Table
      ----------------------------------------------------------------------------------
      INSERT INTO xxwc.xxwc_b2b_pod_so_info_tbl( order_number
                                            , pod_date
                                            , file_name
                                            , status)
      SELECT SUBSTR(file_name,1,8) Order_number
           , TO_DATE(SUBSTR(file_name,10,8),'MMDDYYYY') pod_date
           , file_name
           , 'NEW'
        FROM xxwc.xxwc_b2b_pod_files_tbl stg
       WHERE file_name LIKE '%PDF'
         AND file_name NOT LIKE 'XXWC%PDF'
         AND INSTR(file_name,'_') = 9
         AND NOT EXISTS (SELECT '1' FROM xxwc.xxwc_b2b_pod_so_info_tbl stg2 WHERE stg.file_name = stg2.file_name);

      COMMIT;
      fnd_file.put_line (fnd_file.LOG,'Time Capture After Insert into POD info table '||TO_CHAR(SYSDATE, 'DD-MM-YYYY HH24:MI:SS')); -- Ver#1.22
-- Version# 1.11 > Start
      l_sec := 'Update Staging Table - DELIVERY_ID for My Logistics Orders';
      ----------------------------------------------------------------------------------
      -- Update Staging Table - DELIVERY_ID for My Logistics Orders
      ----------------------------------------------------------------------------------
  /* <START> Commented for Ver#1.17
      UPDATE xxwc.xxwc_b2b_pod_so_info_tbl pod
         SET (pod.delivery_id) = (SELECT delivery_id FROM xxwc.xxwc_om_dms_ship_confirm_tbl shp WHERE shp.file_name||'.PDF' = pod.file_name AND ROWNUM = 1)
       WHERE pod.delivery_id   IS NULL
         AND pod.status        = 'NEW';
     <END> Ver#1.17 */
     -- <START>  Changed code for Ver#1.17
    UPDATE /* +INDEX(pod XXWC.XXWC_B2B_POD_SO_INFO_N1) */ xxwc.xxwc_b2b_pod_so_info_tbl pod
          SET pod.delivery_id =
           (SELECT /* +INDEX(shp xxwc.XXWC_OM_DMS_SHIP_CONFIRM_N5) */ delivery_id
             -- FROM XXWC_OM_DMS_SHIP_CONFIRM_TBL shp  -- Ver#1.22
			  FROM XXWC.XXWC_OM_DMS_SHIP_CONFIRM_TBL shp  -- Ver#1.22
                WHERE     shp.file_name = REPLACE (pod.FILE_NAME, '.PDF', '')
                  AND ROWNUM = 1)
        WHERE pod.status = 'NEW' AND NVL (pod.delivery_id, 0) = 0;
        -- <END>  Changed code for Ver#1.17
COMMIT;
      fnd_file.put_line (fnd_file.LOG,'Time Capture After update xxwc_b2b_pod_so_info_tbl table '||TO_CHAR(SYSDATE, 'DD-MM-YYYY HH24:MI:SS')); -- Ver#1.22
      l_sec := 'Delete files that have EXCEPTION status in XXWC_OM_DMS_SHIP_CONFIRM_TBL';
      ----------------------------------------------------------------------------------
      -- Delete files that have EXCEPTION status in XXWC_OM_DMS_SHIP_CONFIRM_TBL
      ----------------------------------------------------------------------------------
 --<START>  Ver#1.22 
 /* DELETE FROM xxwc.xxwc_b2b_pod_so_info_tbl pod
       WHERE EXISTS (SELECT '1'
                       FROM xxwc.xxwc_om_dms_ship_confirm_tbl shp
                      WHERE shp.file_name||'.PDF' = pod.file_name
                        AND shp.ship_confirm_status = 'EXCEPTION'); */
						
      DELETE /* +INDEX(pod XXWC.XXWC_B2B_POD_SO_INFO_N2) */  FROM XXWC.XXWC_B2B_POD_SO_INFO_TBL POD
        WHERE EXISTS (SELECT /* +INDEX(shp xxwc.XXWC_OM_DMS_SHIP_CONFIRM_N3) */ '1'
                                FROM XXWC.XXWC_OM_DMS_SHIP_CONFIRM_TBL SHP
                               WHERE shp.file_name||'.PDF' = pod.file_name
                                 AND shp.ship_confirm_status = 'EXCEPTION');
  --<END>  Ver#1.22 
COMMIT;

    fnd_file.put_line (fnd_file.LOG,'Time Capture After delete EXCEPTION status in XXWC_OM_DMS_SHIP_CONFIRM_TBL '||TO_CHAR(SYSDATE, 'DD-MM-YYYY HH24:MI:SS')); -- Ver#1.22
-- Version# 1.11 < End

      l_sec := 'Update Staging Table - CUST_ACCOUNT_ID for My Logistics Orders';
      ----------------------------------------------------------------------------------
      -- Update Staging Table - CUST_ACCOUNT_ID for My Logistics Orders
      ----------------------------------------------------------------------------------
      UPDATE xxwc.XXWC_B2B_POD_SO_INFO_TBL pod
         SET (pod.cust_account_id , pod.site_use_id) = (SELECT sold_to_org_id, ship_to_org_id FROM oe_order_headers_all ooh WHERE ooh.order_number = pod.order_number)
       WHERE pod.order_number    IS NOT NULL
         AND pod.cust_account_id IS NULL
         AND pod.status           = 'NEW';

      COMMIT;
      fnd_file.put_line (fnd_file.LOG,'Time Capture After Update CUST_ACCOUNT_ID for My Logistics Orders '||TO_CHAR(SYSDATE, 'DD-MM-YYYY HH24:MI:SS')); -- Ver#1.22
      l_sec := 'Update Staging Table - CUST_ACCOUNT_ID for Signature Capture Orders';
      ----------------------------------------------------------------------------------
      -- Update Staging Table - CUST_ACCOUNT_ID for Signature Capture Orders
      ----------------------------------------------------------------------------------
      UPDATE xxwc.xxwc_b2b_pod_so_info_tbl pod
         SET (pod.cust_account_id , pod.site_use_id) = (SELECT sold_to_org_id, ship_to_org_id FROM oe_order_headers_all ooh WHERE ooh.header_id = pod.header_id)
       WHERE pod.header_id       IS NOT NULL
         AND pod.cust_account_id IS NULL
         AND pod.status           = 'NEW';

      COMMIT;
      fnd_file.put_line (fnd_file.LOG,'Time Capture After Update tbl CUST_ACCOUNT_ID for Signature Capture Orders '||TO_CHAR(SYSDATE, 'DD-MM-YYYY HH24:MI:SS')); -- Ver#1.22
      l_sec := 'Delete from non POD Customers from XXWC_B2B_POD_SO_INFO_TBL';
      ----------------------------------------------------------------------------------
      -- Delete from non POD Customers from XXWC_B2B_POD_SO_INFO_TBL
      ----------------------------------------------------------------------------------
      DELETE FROM xxwc.xxwc_b2b_pod_so_info_tbl pod_so
            WHERE NOT EXISTS (SELECT '1'
                                FROM xxwc.xxwc_b2b_config_tbl pod -- Version# 1.18
                               WHERE pod_so.cust_account_id   = pod.cust_account_id
                                 AND TRUNC(SYSDATE)     BETWEEN TRUNC(pod.start_date_active) AND TRUNC(pod.end_date_active)
                             );
      COMMIT;
      fnd_file.put_line (fnd_file.LOG,'Time Capture After Delete from non POD Customers from XXWC_B2B_POD_SO_INFO_TBL '||TO_CHAR(SYSDATE, 'DD-MM-YYYY HH24:MI:SS')); -- Ver#1.22
      l_sec := 'Update Staging Table - PARTY_ID';
      ----------------------------------------------------------------------------------
      -- Update Staging Table - PARTY_ID
      ----------------------------------------------------------------------------------
      UPDATE xxwc.xxwc_b2b_config_tbl pod -- Version# 1.18
         SET pod.account_number   = (SELECT hca.account_number FROM hz_cust_accounts_all hca WHERE hca.cust_account_id = pod.cust_account_id)
           , last_updated_by      = 15986 -- XXWC_INT_SALESFULFILLMENT
       WHERE pod.account_number  IS NULL
         AND TRUNC(SYSDATE) BETWEEN TRUNC(pod.start_date_active) AND TRUNC(pod.end_date_active)
         AND pod.cust_account_id IS NOT NULL;

      COMMIT;
      fnd_file.put_line (fnd_file.LOG,'Time Capture After Update Staging Table - PARTY_ID '||TO_CHAR(SYSDATE, 'DD-MM-YYYY HH24:MI:SS')); -- Ver#1.22
      -- Version# 1.10 > Start
      ----------------------------------------------------------------------------
      -- Updating Order-Number and Order-Id
      ----------------------------------------------------------------------------
      l_sec := 'Updating Order-Number and Order-Id';
      BEGIN
         UPDATE xxwc.xxwc_b2b_pod_so_info_tbl stg
            SET stg.header_id = (SELECT ooha.header_id FROM oe_order_headers_all ooha WHERE ooha.order_number = stg.order_number)
          WHERE status = 'NEW'
            AND header_id IS NULL;
         COMMIT;
        fnd_file.put_line (fnd_file.LOG,'Time Capture After Updating Order-Number and Order-Id '||TO_CHAR(SYSDATE, 'DD-MM-YYYY HH24:MI:SS')); -- Ver#1.22
         UPDATE xxwc.xxwc_b2b_pod_so_info_tbl stg
            SET stg.order_number = (SELECT ooha.order_number FROM oe_order_headers_all ooha WHERE ooha.header_id = stg.header_id)
          WHERE status = 'NEW'
            AND order_number IS NULL;
         COMMIT;
		 fnd_file.put_line (fnd_file.LOG,'Time Capture After Updating Order-Number and Order-Id '||TO_CHAR(SYSDATE, 'DD-MM-YYYY HH24:MI:SS')); -- Ver#1.22
      EXCEPTION
      WHEN OTHERS THEN
        l_err_msg := 'Error Updating Order-Number and Order-Id'||SQLERRM;
      END;
      -- Version# 1.10 < End

   EXCEPTION

      WHEN OTHERS
      THEN
         l_err_code := 2;
         l_err_msg := SUBSTR ( (l_err_msg || ' ERROR ' || SUBSTR (SQLERRM, 1, 2000)), 1, 3000);
         fnd_file.put_line (fnd_file.LOG, l_err_msg);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_B2B_SO_IB_PKG.insert_pod_info',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (SQLERRM, 1, 2000),
            p_error_desc          => 'Error in B2B Customer insert_pod_info procedure, When Others Exception',
            p_distribution_list   => g_dflt_email,
            p_module              => 'XXWC');

         p_retcode := l_err_code;
         p_errbuf := l_err_msg;

         fnd_file.put_line (fnd_file.LOG, 'Error in  LOAD_INTERFACE procedure - ' || SQLERRM);
   END INSERT_POD_INFO;

   /********************************************************************************
   ProcedureName : CREATE_FILE
   Purpose       : Create POD List and Email-Body files.

   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     10/20/2011    Gopi Damuluri    Initial creation of the procedure
   1.10    02/15/2016    Gopi Damuluri    TMS# 20160215-00282 - Changes to B2B POD Notification email
   1.12    03/14/2016    Gopi Damuluri    TMS# 20160314-00156 - Issue with Weekly POD
   1.13    04/04/2016    Gopi Damuluri    TMS# 20160404-00040 - Incorrect POD_LAST_SENT_DATE for WEEKLY POD
   1.15    06/16/2016    Pattabhi Avula   TMS# 20160616-00043 - Increased l_email_addr length in CREATE_FILE procedure
   1.16    06/20/2016    Rakesh Patel     TMS#20160617-00076-XXWC B2B Create POD Files Program Issues Fix
   1.18    10/27/2016    Rakesh Patel     TMS#20161010-00242 --  Change SOA and ASN to reflect priced copy settings on B2B Setup page
   1.19    04/27/2017    Nancy Pahwa      TMS#20170206-00257 -- OSU POD Mass Update and Unsubscribe
   ********************************************************************************/
   PROCEDURE CREATE_FILE (p_errbuf           OUT VARCHAR2
                         ,p_retcode          OUT NUMBER
                         ,p_directory_path    IN VARCHAR2)
   IS
      -- Intialize Variables
      l_err_msg          VARCHAR2 (2000);
      l_err_code         NUMBER;
      l_sec              VARCHAR2 (150);
      l_invoice_num      VARCHAR2 (20);
      l_invoice_exists   VARCHAR2 (20); -- Version# 1.10
      l_pod_next_send_date DATE;

      --File Variables
      l_file_handle      UTL_FILE.file_type;
      l_lst_file_name        VARCHAR2 (150);
      l_file_dir         VARCHAR2 (100);
      l_lst_file_name_temp   VARCHAR2 (150);

    --  l_email_addr          VARCHAR2(200);  -- Version# 1.15
      l_email_addr          VARCHAR2(2500);   -- Version# 1.15
      l_mb_file_handle      UTL_FILE.file_type;
      l_mb_file_name        VARCHAR2 (150);
      l_mb_file_name_temp   VARCHAR2 (150);
      l_email_subj          VARCHAR2 (150);
      l_mail_body_text      VARCHAR2 (2000);

      l_wk_file_handle      UTL_FILE.file_type; -- Version# 1.12
      l_wk_file_name        VARCHAR2 (150);     -- Version# 1.12
      l_wk_file_name_temp   VARCHAR2 (150);     -- Version# 1.12

      l_dir_exists       NUMBER;

      l_program_error    EXCEPTION;
      p_url varchar2(500); --1.19
   BEGIN
      p_url := 'https://www.whitecap.com/shop/wc/order-status-unsubscribe'; --1.19
      fnd_file.put_line (fnd_file.LOG ,'**********************************************************************************');
      fnd_file.put_line (fnd_file.LOG ,'----------------------------------------------------------------------------------');
      fnd_file.put_line (fnd_file.LOG , 'Start of Procedure : create_file');
      fnd_file.put_line (fnd_file.LOG ,'----------------------------------------------------------------------------------');

      fnd_file.put_line (fnd_file.LOG,'p_directory_path     :' || p_directory_path);

      l_sec := 'Check if directory path exists';
      ------------------------------------------------------------------------------------
      -- Check if directory path exists
      ------------------------------------------------------------------------------------
      l_dir_exists := 0;

      BEGIN
         SELECT COUNT (1)
           INTO l_dir_exists
           FROM all_directories
          WHERE directory_name = p_directory_path;
      EXCEPTION
      WHEN OTHERS THEN
         l_err_msg := 'Error validating directory   :' || SQLERRM;
         RAISE l_program_error;
      END;

      -- Raise ProgramError if directory path does not exist
      IF l_dir_exists = 0 THEN
         l_err_msg := 'Directory does not exist in Oracle :' || p_directory_path;
         RAISE l_program_error;
      END IF;

      -- fnd_file.put_line (fnd_file.LOG,'*** 1 ***');

      FOR rec_pod IN (SELECT *
                        FROM xxwc.xxwc_b2b_config_tbl pod -- Version# 1.18
                       WHERE pod.deliver_pod          = 'Y'
                         AND TRUNC(SYSDATE) BETWEEN TRUNC(pod.start_date_active) AND TRUNC(pod.end_date_active)
                         AND pod.pod_next_send_date  <= TRUNC(SYSDATE)
                         AND EXISTS (SELECT '1'
                                       FROM xxwc.xxwc_b2b_pod_so_info_tbl so
                                      WHERE so.cust_account_id          = pod.cust_account_id
                                        AND (pod.site_use_id IS NULL OR so.site_use_id = pod.site_use_id)
                                        AND so.status                   = 'NEW')
                       ORDER BY cust_account_id) LOOP
      BEGIN -- Added for Version# 1.16
         l_sec := 'Inside POD Setup Loop';
         -- fnd_file.put_line (fnd_file.LOG,'*** 2 ***');
         l_email_addr := NULL;
         l_err_msg := NULL; -- Added for Version# 1.16
         ------------------------------------------------------------------------------------
         -- Inside POD Setup Loop
         ------------------------------------------------------------------------------------

         IF rec_pod.site_use_id IS NULL THEN
            l_lst_file_name       := rec_pod.account_number||'.lst';
            l_lst_file_name_temp  := 'TEMP_' || l_lst_file_name;

            l_mb_file_name        := rec_pod.account_number||'.html';
            l_mb_file_name_temp   := 'TEMP_' || l_mb_file_name;
         ELSE
            l_lst_file_name       := rec_pod.account_number||'_'||rec_pod.site_use_id||'.lst';
            l_lst_file_name_temp  := 'TEMP_' || l_lst_file_name;

            l_mb_file_name        := rec_pod.account_number||'_'||rec_pod.site_use_id||'.html';
            l_mb_file_name_temp   := 'TEMP_' || l_mb_file_name;
         END IF;

         l_sec := 'Delete if file already exists';

         -----------------------------------------------------------------------------------
         -- Delete if file already exists
         ------------------------------------------------------------------------------------
         BEGIN
            UTL_FILE.FREMOVE (p_directory_path, l_lst_file_name);
            UTL_FILE.FREMOVE (p_directory_path, l_lst_file_name_temp);

            UTL_FILE.FREMOVE (p_directory_path, l_mb_file_name);
            UTL_FILE.FREMOVE (p_directory_path, l_mb_file_name_temp);
         EXCEPTION
            WHEN OTHERS
            THEN
               NULL;
         END;

         -- ###########################################################
         l_sec := 'Start creating the list file';
         -- ###########################################################

         ------------------------------------------------------------------------------------
         -- Start creating the file
         ------------------------------------------------------------------------------------
         l_file_handle := UTL_FILE.fopen (p_directory_path
                                         , l_lst_file_name_temp
                                         , 'w'
                                         , 32767);

         IF rec_pod.notify_account_mgr = 'Y' THEN
            l_email_addr := xxwc_b2b_so_ib_pkg.get_sales_rep_email(rec_pod.cust_account_id); -- Version# 1.18

            IF l_email_addr IS NOT NULL THEN
               l_email_addr := l_email_addr||','||rec_pod.pod_email;
            END IF;
         ELSE
            l_email_addr := rec_pod.pod_email;
         END IF;

         UTL_FILE.put_line (l_file_handle, l_email_addr);

         -- fnd_file.put_line (fnd_file.LOG,'*** 4 ***');

         -- ###########################################################
         l_sec := 'Start creating the MAIL BODY file';
         -- ###########################################################
         l_email_subj     := NULL;
         l_mail_body_text := NULL;

         ------------------------------------------------------------------------------------
         -- Start creating the file
         ------------------------------------------------------------------------------------
         l_mb_file_handle := UTL_FILE.fopen (p_directory_path
                                         , l_mb_file_name_temp
                                         , 'w'
                                         , 32767);

         -- Subject Line
         l_email_subj := 'C And I White Cap '||TO_CHAR(SYSDATE,'MM/DD/YYYY')||' PODs';
         UTL_FILE.put_line (l_mb_file_handle, l_email_subj);

         -- Email Body - Start Line
         l_mail_body_text := 'Dear '||rec_pod.account_name||',';
         UTL_FILE.put_line (l_mb_file_handle, l_mail_body_text);

         -- Email Body - Table Header
         l_mail_body_text := '<BR></BR>';
         UTL_FILE.put_line (l_mb_file_handle, l_mail_body_text);

         l_mail_body_text := '<BR>Below is the summary of Proof of Delivery (POD) documents included in this email</BR>';
         UTL_FILE.put_line (l_mb_file_handle, l_mail_body_text);

         l_mail_body_text := '<table style="width:100%"> <tr>';
         UTL_FILE.put_line (l_mb_file_handle, l_mail_body_text);

         -- Version# 1.10 > Start
         l_mail_body_text := '<td><B><U>Invoice Number</U></B></td>';
         UTL_FILE.put_line (l_mb_file_handle, l_mail_body_text);

         l_mail_body_text := '<td><B><U>Order Number</U></B></td>';
         UTL_FILE.put_line (l_mb_file_handle, l_mail_body_text);

         l_mail_body_text := '<td><B><U>Delivery Id</U></B></td>';
         UTL_FILE.put_line (l_mb_file_handle, l_mail_body_text);

         l_mail_body_text := '<td><B><U>File Name</U></B></td>';
         UTL_FILE.put_line (l_mb_file_handle, l_mail_body_text);

         l_mail_body_text := '<td><B><U>PO Number</U></B></td>';
         UTL_FILE.put_line (l_mb_file_handle, l_mail_body_text);

         l_mail_body_text := '<td><B><U>Shipment Date</U></B></td>';
         UTL_FILE.put_line (l_mb_file_handle, l_mail_body_text);

         l_mail_body_text := '<td><B><U>Project Name / Project Number</U></B></td></tr>';
         UTL_FILE.put_line (l_mb_file_handle, l_mail_body_text);

          /*
          l_mail_body_text := '<td><B><U>Order Number</U></B></td>';
          UTL_FILE.put_line (l_mb_file_handle, l_mail_body_text);

          l_mail_body_text := '<td><B><U>Shipment Date</U></B></td>';
          UTL_FILE.put_line (l_mb_file_handle, l_mail_body_text);

          l_mail_body_text := '<td><B><U>Project Name / Project Number</U></B></td>';
          UTL_FILE.put_line (l_mb_file_handle, l_mail_body_text);

          l_mail_body_text := '<td><B><U>PO Number</U></B></td>';
          UTL_FILE.put_line (l_mb_file_handle, l_mail_body_text);

          l_mail_body_text := '<td><B><U>File Name</U></B></td>';
          UTL_FILE.put_line (l_mb_file_handle, l_mail_body_text);

          l_mail_body_text := '<td><B><U>Invoice Number</U></B></td></tr>';
          UTL_FILE.put_line (l_mb_file_handle, l_mail_body_text);
           */
          -- Version# 1.10 < End
   
         l_invoice_exists := 'N'; -- Version# 1.10
         FOR rec_mb_file IN (SELECT * FROM (SELECT DISTINCT NVL(TRUNC(oola.ACTUAL_SHIPMENT_DATE),TRUNC(oola.last_update_date))     ship_date -- Version# 1.10
                                   , hcsu.location                 job_site
                                   , ooha.cust_po_number           po_number
                                   , shp.delivery_id               delivery_id
                                   , NULL                          invoice_id
                                   , ooha.order_number              order_number
                                   , ooha.header_id                header_id
                                   , stg.file_name                 file_name
                                FROM xxwc.xxwc_b2b_pod_so_info_tbl stg
                                   , oe_order_headers_all          ooha
                                   , xxwc.xxwc_wsh_shipping_stg    shp
                                   --, hz_party_sites                hps
                                   , hz_cust_site_uses_all         hcsu
                                   , hz_cust_acct_sites_all        hcas
                                   , oe_order_lines_all            oola
                               WHERE stg.cust_account_id         = rec_pod.cust_account_id
                                 AND hcsu.site_use_id            = NVL(rec_pod.site_use_id, hcsu.site_use_id)
                                 AND stg.status                  = 'NEW'
                                 AND stg.order_number            = ooha.order_number
                                 AND shp.header_id               = ooha.header_id
                                 AND hcsu.site_use_id            = ooha.ship_to_org_id
                                 AND hcas.cust_acct_site_id      = hcsu.cust_acct_site_id
                                 AND stg.delivery_id             = shp.delivery_id
                                 --AND hps.party_site_id           = hcas.party_site_id
                                 AND oola.line_id                = shp.line_id
                                 AND oola.flow_status_code       = 'CLOSED'
                                 AND shp.delivery_id            IS NOT NULL
                        UNION
                              SELECT DISTINCT NVL(TRUNC(oola.ACTUAL_SHIPMENT_DATE),TRUNC(oola.last_update_date))     ship_date -- Version# 1.10
                                   , hcsu.location                 job_site
                                   , ooha.cust_po_number           po_number
                                   , NULL                          delivery_id
                                   , NULL                          invoice_id
                                   , ooha.order_number              order_number
                                   , ooha.header_id                header_id
                                   , stg.file_name                 file_name
                                FROM xxwc.xxwc_b2b_pod_so_info_tbl stg
                                   , oe_order_headers_all          ooha
                                   , hz_cust_site_uses_all         hcsu
                                   , hz_cust_acct_sites_all        hcas
                                   , oe_order_lines_all            oola
                               WHERE stg.cust_account_id         = rec_pod.cust_account_id
                                 AND hcsu.site_use_id            = NVL(rec_pod.site_use_id, hcsu.site_use_id)
                                 AND stg.status                  = 'NEW'
                                 AND oola.flow_status_code       = 'CLOSED'
                                 -- AND oola.creation_date          like sysdate
                                 AND stg.header_id               = ooha.header_id
                                 AND hcsu.site_use_id            = ooha.ship_to_org_id
                                 AND hcas.cust_acct_site_id      = hcsu.cust_acct_site_id
                                 AND oola.header_id              = ooha.header_id
                                 -- AND NOT EXISTS (SELECT '1' FROM xxwc.xxwc_wsh_shipping_stg shp WHERE shp.header_id= oola.header_id AND shp.line_id= oola.line_id) -- Version# 1.10
                                 AND stg.delivery_id            IS NULL -- Version# 1.10
                                 ) ORDER BY order_number
                                 )
         ------------------------------------------------------------------------------------
         -- Write data into the File
         ------------------------------------------------------------------------------------
         LOOP
            -- fnd_file.put_line (fnd_file.LOG,'*** 5 ***');

            l_invoice_num := NULL;
            IF rec_mb_file.delivery_id IS NOT NULL THEN
               BEGIN
                  SELECT rcta.trx_number
                    INTO l_invoice_num
                    FROM ra_customer_trx_lines_all        rctl
                       , xxwc.xxwc_wsh_shipping_stg       shp
                       , ra_customer_trx_all              rcta
                   WHERE rctl.INTERFACE_LINE_ATTRIBUTE6 = to_char(shp.line_id)
                     AND rcta.customer_trx_id           = rctl.customer_trx_id
                     AND shp.delivery_id                = rec_mb_file.delivery_id
                     AND rownum = 1;
               EXCEPTION
               WHEN OTHERS THEN
                  NULL;
               END;
            ELSE
               BEGIN
                  SELECT rcta.trx_number
                    INTO l_invoice_num
                    FROM ra_customer_trx_all              rcta
                   WHERE rcta.interface_header_attribute1 = to_char(rec_mb_file.order_number)
                     AND rownum = 1;
               EXCEPTION
               WHEN OTHERS THEN
                  NULL;
               END;
            END IF;

            IF l_invoice_num IS NOT NULL THEN
               l_invoice_exists := 'Y'; -- Version# 1.10

               ------------------------------------------------------------------------------------
               -- Write to List File
               ------------------------------------------------------------------------------------
               l_sec := 'Write to List File';
               UTL_FILE.put_line (l_file_handle, rec_mb_file.file_name);

               ------------------------------------------------------------------------------------
               -- Write to Mail-Body File
               ------------------------------------------------------------------------------------
               -- Version# 1.10 > Start
               l_sec := 'Write to Mail-Body File';

               l_mail_body_text := '<tr><td>'||l_invoice_num||'</td>';
               UTL_FILE.put_line (l_mb_file_handle, l_mail_body_text);

               l_mail_body_text := '<td>'||rec_mb_file.order_number||'</td>';
               UTL_FILE.put_line (l_mb_file_handle, l_mail_body_text);

               l_mail_body_text := '<td>'||rec_mb_file.delivery_id||'</td>';
               UTL_FILE.put_line (l_mb_file_handle, l_mail_body_text);

               l_mail_body_text := '<td>'||rec_mb_file.file_name||'</td>';
               UTL_FILE.put_line (l_mb_file_handle, l_mail_body_text);

               l_mail_body_text := '<td>'||rec_mb_file.po_number||'</td>';
               UTL_FILE.put_line (l_mb_file_handle, l_mail_body_text);

               l_mail_body_text := '<td>'||rec_mb_file.ship_date||'</td>';
               UTL_FILE.put_line (l_mb_file_handle, l_mail_body_text);

               l_mail_body_text := '<td>'||rec_mb_file.job_site||'</td></tr>';
               UTL_FILE.put_line (l_mb_file_handle, l_mail_body_text);


              /*
                l_mail_body_text := '<tr><td>'||rec_mb_file.order_number||'</td>';
                UTL_FILE.put_line (l_mb_file_handle, l_mail_body_text);

                l_mail_body_text := '<td>'||rec_mb_file.ship_date||'</td>';
                UTL_FILE.put_line (l_mb_file_handle, l_mail_body_text);

                l_mail_body_text := '<td>'||rec_mb_file.job_site||'</td>';
                UTL_FILE.put_line (l_mb_file_handle, l_mail_body_text);

                l_mail_body_text := '<td>'||rec_mb_file.po_number||'</td>';
                UTL_FILE.put_line (l_mb_file_handle, l_mail_body_text);

                l_mail_body_text := '<td>'||rec_mb_file.file_name||'</td>';
                UTL_FILE.put_line (l_mb_file_handle, l_mail_body_text);

                l_mail_body_text := '<td>'||l_invoice_num||'</td></tr>';

                l_sec := 'Write to Mail-Body File';
                UTL_FILE.put_line (l_mb_file_handle, l_mail_body_text);
              */

               IF rec_mb_file.delivery_id IS NULL THEN
                  UPDATE xxwc.xxwc_b2b_pod_so_info_tbl
                     SET status               = 'PROCESSED'
                       , last_update_date     = SYSDATE
                   WHERE cust_account_id      = rec_pod.cust_account_id
                     AND status               = 'NEW'
                     AND header_id            = rec_mb_file.header_id
                     AND (rec_pod.site_use_id IS NULL OR site_use_id = rec_pod.site_use_id);
               ELSE
                  UPDATE xxwc.xxwc_b2b_pod_so_info_tbl
                     SET status               = 'PROCESSED'
                       , last_update_date     = SYSDATE
                   WHERE cust_account_id      = rec_pod.cust_account_id
                     AND status               = 'NEW'
                     AND header_id            = rec_mb_file.header_id
                     AND (rec_pod.site_use_id IS NULL OR site_use_id = rec_pod.site_use_id)
                     AND delivery_id          = rec_mb_file.delivery_id;
               END IF;

               -- Version# 1.10 > End

            END IF; -- IF l_invoice_num IS NOT NULL THEN
         END LOOP; -- rec_mb_file
         COMMIT; -- Version# 1.10

         l_mail_body_text := '</table>';

         UTL_FILE.put_line (l_mb_file_handle, l_mail_body_text);
         -- 1.19 Start
         l_mail_body_text := '<BR></BR>';
         l_mail_body_text := '<BR>Click '  || '<a href="' || p_url || '"> here </a>  ' || 'to make changes to your subscription or to unsubscribe from the POD email service.</BR>';
         UTL_FILE.put_line (l_mb_file_handle, l_mail_body_text);
         -- 1.19 End
         ------------------------------------------------------------------------------------
         -- Close the List file
         ------------------------------------------------------------------------------------
         UTL_FILE.fclose (l_file_handle);

         IF l_invoice_exists = 'Y' THEN -- Version# 1.10
            ------------------------------------------------------------------------------------
            -- 'Rename list file for pickup';
            ------------------------------------------------------------------------------------
            UTL_FILE.frename (p_directory_path
                           ,l_lst_file_name_temp
                           ,p_directory_path
                           ,l_lst_file_name);
            -- Version# 1.10 > Start
         ELSE
            ------------------------------------------------------------------------------------
            -- 'Delete Temp list file for pickup';
            ------------------------------------------------------------------------------------
            UTL_FILE.FREMOVE (p_directory_path, l_lst_file_name_temp);
         END IF;
         -- Version# 1.10 < End
         -- ###########################################################
         l_sec := 'End creating the list file';
         -- ###########################################################

         ------------------------------------------------------------------------------------
         -- Close the Mail-Body file
         ------------------------------------------------------------------------------------
         UTL_FILE.fclose (l_mb_file_handle);

         IF l_invoice_exists = 'Y' THEN -- Version# 1.10
          ------------------------------------------------------------------------------------
          -- 'Rename Mail-Body file for pickup';
          ------------------------------------------------------------------------------------
            UTL_FILE.frename (p_directory_path
                             ,l_mb_file_name_temp
                             ,p_directory_path
                             ,l_mb_file_name);
          -- Version# 1.10 > Start
         ELSE
          ------------------------------------------------------------------------------------
          -- 'Delete Temp Mail-Body file for pickup';
          ------------------------------------------------------------------------------------
             UTL_FILE.FREMOVE (p_directory_path, l_mb_file_name_temp);
         END IF;
         -- Version# 1.10 < End
          -- ###########################################################
         l_sec := 'End creating the MAIL BODY file';
          -- ###########################################################
          -- fnd_file.put_line (fnd_file.LOG,'*** 6 ***');

          ------------------------------------------------------------------------------------
          -- Update POD_NEXT_SEND_DATE in the Staging Table
          ------------------------------------------------------------------------------------
         l_pod_next_send_date := NULL;

         IF rec_pod.pod_frequency   = 'DAILY' THEN
            l_pod_next_send_date    := TRUNC(SYSDATE) + 1;
         ELSIF rec_pod.pod_frequency = 'WEEKLY' THEN
            -- l_pod_next_send_date    := TRUNC(rec_pod.pod_last_sent_date) + 7; -- Version# 1.13
            l_pod_next_send_date    := TRUNC(rec_pod.pod_next_send_date) + 7;    -- Version# 1.13
         ELSIF rec_pod.pod_frequency = 'MONTHLY' THEN
            l_pod_next_send_date    := ADD_MONTHS(TRUNC(rec_pod.pod_last_sent_date),1);
         END IF;

         -- fnd_file.put_line (fnd_file.LOG,'*** 7 ***');
         UPDATE xxwc.xxwc_b2b_config_tbl pod -- Version# 1.18
            SET pod_next_send_date = l_pod_next_send_date
              , pod_last_sent_date = SYSDATE
              , last_updated_by    = 15986 -- XXWC_INT_SALESFULFILLMENT
          WHERE cust_account_id = rec_pod.cust_account_id
            AND TRUNC(SYSDATE) BETWEEN TRUNC(pod.start_date_active) AND TRUNC(pod.end_date_active)
            AND ( (rec_pod.site_use_id IS NULL AND pod.site_use_id IS NULL )-- Rev# 1.21 
            OR site_use_id = rec_pod.site_use_id);

          -- Version# 1.10 > Start
          /*
          UPDATE xxwc.xxwc_b2b_pod_so_info_tbl
             SET status               = 'PROCESSED'
               , last_update_date     = SYSDATE
           WHERE cust_account_id      = rec_pod.cust_account_id
             AND status               = 'NEW'
           --  AND order_number       = rec_pod.order_number
             AND (rec_pod.site_use_id IS NULL OR site_use_id = rec_pod.site_use_id);

          -- fnd_file.put_line (fnd_file.LOG,'*** 8 ***');
          COMMIT;
           */
           -- Version# 1.10 < End

      -- Version# 1.16 > Start
      EXCEPTION
      WHEN OTHERS THEN
         l_err_msg :=l_sec || l_err_msg || ' ERROR ' || SQLCODE || SUBSTR (SQLERRM, 1, 2000);

         fnd_file.put_line (fnd_file.LOG, 'l_err_msg - ' || l_err_msg);

         xxcus_error_pkg.xxcus_error_main_api (
          p_called_from         => 'XXWC_B2B_SO_IB_PKG.CREATE_FILE'
         ,p_calling             => l_sec
         ,p_request_id          => fnd_global.conc_request_id
         ,p_ora_error_msg       => l_err_msg
         ,p_error_desc          => 'Error is For Loop - rec_pod'
         ,p_distribution_list   => g_dflt_email
         ,p_module              => 'XXWC');

         -----------------------------------------------------------------------------------
         -- Delete if file already exists
         ------------------------------------------------------------------------------------
         BEGIN
            UTL_FILE.fclose (l_file_handle);
            UTL_FILE.fclose (l_mb_file_handle);

            --Removed first temp file
            UTL_FILE.FREMOVE (p_directory_path, l_lst_file_name_temp);
            UTL_FILE.FREMOVE (p_directory_path, l_mb_file_name_temp);

         EXCEPTION
            WHEN OTHERS
            THEN
               NULL;
         END;
      END;
      -- Version# 1.16 < End
      END LOOP; -- rec_pod


         -- Version# 1.12 > Start
         -- ###########################################################
         l_sec := 'Start creating Weekly list file';
         -- ###########################################################

         ------------------------------------------------------------------------------------
         -- Inside POD Weekly Loop
         ------------------------------------------------------------------------------------

         l_wk_file_name       := 'B2B_POD_WEEKLY.wk';
         l_wk_file_name_temp  := 'TEMP_' || l_wk_file_name;

         l_sec := 'Delete if file already exists';
         ------------------------------------------------------------------------------------
         -- Delete if file already exists
         ------------------------------------------------------------------------------------
         BEGIN
            UTL_FILE.FREMOVE (p_directory_path, l_wk_file_name);
            UTL_FILE.FREMOVE (p_directory_path, l_wk_file_name_temp);
         EXCEPTION
         WHEN OTHERS THEN
            NULL;
         END;

         ------------------------------------------------------------------------------------
         -- Start creating the Weekly file
         ------------------------------------------------------------------------------------
         l_wk_file_handle := UTL_FILE.fopen (p_directory_path
                                         , l_wk_file_name_temp
                                         , 'w'
                                         , 32767);

         l_invoice_exists := 'N';

         FOR rec_wk_pod IN (SELECT *
                            FROM xxwc.xxwc_b2b_config_tbl pod -- Version# 1.18
                           WHERE pod.deliver_pod          = 'Y'
                             AND pod.pod_frequency        = 'WEEKLY'
                             AND TRUNC(pod.pod_next_send_date) != TRUNC(SYSDATE)
                             AND TRUNC(SYSDATE)     BETWEEN TRUNC(pod.start_date_active) AND TRUNC(pod.end_date_active)
                             AND EXISTS (SELECT '1'
                                           FROM xxwc.xxwc_b2b_pod_so_info_tbl so
                                          WHERE so.cust_account_id          = pod.cust_account_id
                                            AND (pod.site_use_id IS NULL OR so.site_use_id = pod.site_use_id)
                                            AND so.status                   = 'NEW')
                           ORDER BY cust_account_id) LOOP
         BEGIN -- Added for Version# 1.16

            l_sec := 'Inside POD Weekly Loop';
            l_err_msg := NULL; -- Added for Version# 1.16

            FOR rec_wk_file IN (SELECT * FROM (SELECT DISTINCT NVL(TRUNC(oola.ACTUAL_SHIPMENT_DATE),TRUNC(oola.last_update_date))     ship_date -- Version# 1.10
                                   , hcsu.location                 job_site
                                   , ooha.cust_po_number           po_number
                                   , shp.delivery_id               delivery_id
                                   , NULL                          invoice_id
                                   , ooha.order_number              order_number
                                   , ooha.header_id                header_id
                                   , stg.file_name                 file_name
                                FROM xxwc.xxwc_b2b_pod_so_info_tbl stg
                                   , oe_order_headers_all          ooha
                                   , xxwc.xxwc_wsh_shipping_stg    shp
                                   --, hz_party_sites                hps
                                   , hz_cust_site_uses_all         hcsu
                                   , hz_cust_acct_sites_all        hcas
                                   , oe_order_lines_all            oola
                               WHERE stg.cust_account_id         = rec_wk_pod.cust_account_id
                                 AND hcsu.site_use_id            = NVL(rec_wk_pod.site_use_id, hcsu.site_use_id)
                                 AND stg.status                  = 'NEW'
                                 AND stg.order_number            = ooha.order_number
                                 AND shp.header_id               = ooha.header_id
                                 AND hcsu.site_use_id            = ooha.ship_to_org_id
                                 AND hcas.cust_acct_site_id      = hcsu.cust_acct_site_id
                                 AND stg.delivery_id             = shp.delivery_id
                                 --AND hps.party_site_id           = hcas.party_site_id
                                 AND oola.line_id                = shp.line_id
                                 AND oola.flow_status_code       = 'CLOSED'
                                 AND shp.delivery_id            IS NOT NULL
                        UNION
                              SELECT DISTINCT NVL(TRUNC(oola.ACTUAL_SHIPMENT_DATE),TRUNC(oola.last_update_date))     ship_date -- Version# 1.10
                                   , hcsu.location                 job_site
                                   , ooha.cust_po_number           po_number
                                   , NULL                          delivery_id
                                   , NULL                          invoice_id
                                   , ooha.order_number              order_number
                                   , ooha.header_id                header_id
                                   , stg.file_name                 file_name
                                FROM xxwc.xxwc_b2b_pod_so_info_tbl stg
                                   , oe_order_headers_all          ooha
                                   , hz_cust_site_uses_all         hcsu
                                   , hz_cust_acct_sites_all        hcas
                                   , oe_order_lines_all            oola
                               WHERE stg.cust_account_id         = rec_wk_pod.cust_account_id
                                 AND hcsu.site_use_id            = NVL(rec_wk_pod.site_use_id, hcsu.site_use_id)
                                 AND stg.status                  = 'NEW'
                                 AND oola.flow_status_code       = 'CLOSED'
                                 -- AND oola.creation_date          like sysdate
                                 AND stg.header_id               = ooha.header_id
                                 AND hcsu.site_use_id            = ooha.ship_to_org_id
                                 AND hcas.cust_acct_site_id      = hcsu.cust_acct_site_id
                                 AND oola.header_id              = ooha.header_id
                                 -- AND NOT EXISTS (SELECT '1' FROM xxwc.xxwc_wsh_shipping_stg shp WHERE shp.header_id= oola.header_id AND shp.line_id= oola.line_id) -- Version# 1.10
                                 AND stg.delivery_id            IS NULL -- Version# 1.10
                                 ) ORDER BY order_number
                                 )
            LOOP
               ------------------------------------------------------------------------------------
               -- Write data into the File
               ------------------------------------------------------------------------------------
               l_invoice_num := NULL;
               IF rec_wk_file.delivery_id IS NOT NULL THEN
                  BEGIN
                     SELECT rcta.trx_number
                       INTO l_invoice_num
                       FROM ra_customer_trx_lines_all        rctl
                          , xxwc.xxwc_wsh_shipping_stg       shp
                          , ra_customer_trx_all              rcta
                      WHERE rctl.INTERFACE_LINE_ATTRIBUTE6 = to_char(shp.line_id)
                        AND rcta.customer_trx_id           = rctl.customer_trx_id
                        AND shp.delivery_id                = rec_wk_file.delivery_id
                        AND rownum = 1;
                  EXCEPTION
                  WHEN OTHERS THEN
                     NULL;
                  END;
               ELSE
                  BEGIN
                     SELECT rcta.trx_number
                       INTO l_invoice_num
                       FROM ra_customer_trx_all              rcta
                      WHERE rcta.interface_header_attribute1 = to_char(rec_wk_file.order_number)
                        AND rownum = 1;
                  EXCEPTION
                  WHEN OTHERS THEN
                     NULL;
                  END;
               END IF;

               IF l_invoice_num IS NOT NULL THEN
                  l_invoice_exists := 'Y';
                  ------------------------------------------------------------------------------------
                  -- Write to Weekly File
                  ------------------------------------------------------------------------------------
                  l_sec := 'Write to Weekly File';
                  UTL_FILE.put_line (l_wk_file_handle, rec_wk_file.file_name);
               END IF;
            END LOOP; -- rec_wk_file

        -- Version# 1.16 > Start
        EXCEPTION
        WHEN OTHERS THEN
           l_err_msg :=l_sec || l_err_msg || ' ERROR ' || SQLCODE || SUBSTR (SQLERRM, 1, 2000);
           fnd_file.put_line (fnd_file.LOG, 'l_err_msg - ' || l_err_msg);

            xxcus_error_pkg.xxcus_error_main_api (
             p_called_from         => 'XXWC_B2B_SO_IB_PKG.CREATE_FILE'
            ,p_calling             => l_sec
            ,p_request_id          => fnd_global.conc_request_id
            ,p_ora_error_msg       => l_err_msg
            ,p_error_desc          => 'Error is For Loop - rec_wk_pod'
            ,p_distribution_list   => g_dflt_email
            ,p_module              => 'XXWC');
         END;
         -- Version# 1.16 < End
         END LOOP; -- rec_wk_pod

         ------------------------------------------------------------------------------------
         -- Close the Weekly file
         ------------------------------------------------------------------------------------
         UTL_FILE.fclose (l_wk_file_handle);

         IF l_invoice_exists = 'Y' THEN
         ------------------------------------------------------------------------------------
         -- 'Rename Weekly file for pickup';
         ------------------------------------------------------------------------------------
         UTL_FILE.frename (p_directory_path
                           ,l_wk_file_name_temp
                           ,p_directory_path
                           ,l_wk_file_name);
         ELSE
         ------------------------------------------------------------------------------------
         -- 'Delete Temp Weekly file for pickup';
         ------------------------------------------------------------------------------------
            UTL_FILE.FREMOVE (p_directory_path, l_wk_file_name_temp);
         END IF;
         -- ###########################################################
         l_sec := 'End creating Weekly list file';
         -- ###########################################################
         -- Version# 1.12 < End

         ----------------------------------------------------------------------------
         -- Updating Order-Number and Order-Id
         ----------------------------------------------------------------------------
         l_sec := 'Updating Order-Number and Order-Id';
         BEGIN
            UPDATE xxwc.xxwc_b2b_pod_so_info_tbl stg
               SET stg.header_id = (SELECT ooha.header_id FROM oe_order_headers_all ooha WHERE ooha.order_number = stg.order_number)
             WHERE status = 'PROCESSED'
               AND header_id IS NULL;

            UPDATE xxwc.xxwc_b2b_pod_so_info_tbl stg
               SET stg.order_number = (SELECT ooha.order_number FROM oe_order_headers_all ooha WHERE ooha.header_id = stg.header_id)
             WHERE status = 'PROCESSED'
               AND order_number IS NULL;

            COMMIT;
         EXCEPTION
         WHEN OTHERS THEN
            l_err_msg := 'Error Updating Order-Number and Order-Id'||SQLERRM;
         END;

      fnd_file.put_line (fnd_file.output ,'----------------------------------------------------------------------------------');
      fnd_file.put_line (fnd_file.output ,'**********************************************************************************');

      fnd_file.put_line (fnd_file.LOG,'----------------------------------------------------------------------------------');
      fnd_file.put_line (fnd_file.LOG, 'End of Procedure : create_file');
      fnd_file.put_line (fnd_file.LOG ,'----------------------------------------------------------------------------------');
   EXCEPTION
   WHEN l_program_error THEN
      l_err_code := 2;
      fnd_file.put_line (fnd_file.LOG, l_err_msg);
      fnd_file.put_line (fnd_file.output, l_err_msg);

      UTL_FILE.fclose (l_file_handle);
      UTL_FILE.fremove (l_file_dir, l_lst_file_name);
      dbms_output.put_line('Error ...'||SQLERRM);

      xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => 'XXWC_B2B_SO_IB_PKG.CREATE_FILE'
        ,p_calling             => l_sec
        ,p_request_id          => fnd_global.conc_request_id
        ,p_ora_error_msg       => l_err_msg
        ,p_error_desc          => 'Error creating text file with File Names'
        ,p_distribution_list   => g_dflt_email
        ,p_module              => 'XXWC');
   WHEN OTHERS THEN
      l_err_code := 2;
      l_err_msg :=l_sec || l_err_msg || ' ERROR ' || SQLCODE || SUBSTR (SQLERRM, 1, 2000);

      fnd_file.put_line (fnd_file.LOG, l_err_msg);
      fnd_file.put_line (fnd_file.output, l_err_msg);
      dbms_output.put_line('Error ...'||SQLERRM);

      UTL_FILE.fclose (l_file_handle);
      UTL_FILE.fremove (l_file_dir, l_lst_file_name);

      xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => 'XXWC_B2B_SO_IB_PKG.CREATE_FILE'
        ,p_calling             => l_sec
        ,p_request_id          => fnd_global.conc_request_id
        ,p_ora_error_msg       => SQLERRM
        ,p_error_desc          => 'Error creating text file with File Names'
        ,p_distribution_list   => g_dflt_email
        ,p_module              => 'XXWC');
   END CREATE_FILE;

-- Version# 1.9 < End

END XXWC_B2B_SO_IB_PKG;
/