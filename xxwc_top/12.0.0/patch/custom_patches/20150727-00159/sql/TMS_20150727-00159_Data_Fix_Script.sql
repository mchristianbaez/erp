/* Formatted on 8/3/2015 11:44:59 AM (QP5 v5.256.13226.35538) */
/*
 TMS: 20150727-00159 
 Date: 08/03/2015
 Notes: Month End - July month end close maintainance
*/
SET SERVEROUTPUT ON SIZE 1000000

DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20150721-00037, Update 1 -Before Update');
   

delete from xxwc.xxwc_wsh_shipping_stg where delivery_id in (3304827, 3448688);
   
   --2 rows expected to be updated
   

   DBMS_OUTPUT.put_line (
         'TMS: 20150721-00037, update 2 -After Update, rows modified: ' || SQL%ROWCOUNT);
		 
  COMMIT;
END;
/



