/*************************************************************************
  PURPOSE:   Grants provided to all custom objects to  EA_APEX user

  REVISIONS:
  Ver        Date        Author               Description
  ---------  ----------  ---------------      -------------------------
  1.0        03/09/2018  Pattabhi Avula       TMS#20180302-00245-Replacement Cost Enhancement
**************************************************************************/

GRANT SELECT, INSERT, UPDATE, DELETE ON XXWC.XXWC_QP_REPLACEMENT_COST_TBL TO EA_APEX
/

GRANT SELECT, INSERT, UPDATE, DELETE ON APPS.XXWC_QP_REPLACEMENT_COST_TBL TO EA_APEX
/

GRANT SELECT ON APPS.fnd_lookup_values  TO EA_APEX
/
