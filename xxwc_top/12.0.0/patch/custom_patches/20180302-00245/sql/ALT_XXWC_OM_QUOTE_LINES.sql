/*************************************************************************
  $Header ALT_XXWC_OM_QUOTE_LINES.sql $
  Module Name: ALT_XXWC_OM_QUOTE_LINES.sql

  PURPOSE:   TMS#20180302-00245-Replacement Cost Enhancement

  REVISIONS:
  Ver        Date        Author                     Description
  ---------  ----------  ---------------         -------------------------
  1.0        03/02/2018  Rakesh Patel            TMS#20180302-00245-Replacement Cost Enhancement
**************************************************************************/
ALTER TABLE XXWC.XXWC_OM_QUOTE_LINES ADD ( REPLACEMENT_COST   NUMBER                                              
								         );
