/*************************************************************************
  $Header ALTER_XXWC_ITEM_AVBL_PRICE_TBL.sql $
  Module Name: ALTER_XXWC_ITEM_AVBL_PRICE_TBL.sql

  PURPOSE:   TMS#20180302-00245-Replacement Cost Enhancement

  REVISIONS:
  Ver        Date        Author                     Description
  ---------  ----------  ---------------         -------------------------
  1.0        03/13/2018  Rakesh Patel            TMS#20180302-00245-Replacement Cost Enhancement
**************************************************************************/
ALTER TABLE XXWC.XXWC_ITEM_AVBL_PRICE_TBL ADD ( REPLACEMENT_COST   NUMBER                                              
								         );