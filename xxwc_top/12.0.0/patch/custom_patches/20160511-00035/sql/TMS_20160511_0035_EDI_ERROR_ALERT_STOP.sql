/*************************************************************************
  $Header TMS_20160511_0035_EDI_ERROR_ALERT_STOP.sql $
  Module Name: TMS_20160511-0035  Data Fix script

  PURPOSE: Data Fix script to stop the error alerts for EDI terminated
           programs

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        11-MAY-2016  Pattabhi Avula        TMS#20160511-0035 

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000;
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20160511-0035    , Before Update');

UPDATE xxcus.xxcus_errors_tbl
SET email_flag1='Y'
WHERE called_from='xxwc_edi_iface_pkg'
AND calling='send_error_email' 
AND TRUNC(creation_date)=TRUNC(SYSDATE) 
AND email_flag1='N';

   DBMS_OUTPUT.put_line (
         'TMS: 20160511-0035  EMAIL_FLAG1 flag changed record count: '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20160511-0035     , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20160511-0035 , Errors : ' || SQLERRM);
END;
/