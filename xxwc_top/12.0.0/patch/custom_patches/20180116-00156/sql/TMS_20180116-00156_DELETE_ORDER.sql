/*************************************************************************
 $Header TMS_20180116-00156_DELETE_ORDER.sql $
 Module Name: TMS_20180116-00156_DELETE_ORDER.sql

 PURPOSE: Delete the issue order

 REVISIONS:
 Ver        Date       Author           Description
 --------- ----------  ---------------  -------------------------
 1.0       07/26/2018  Pattabhi Avula   TMS#20180116-00156 - order 26987501
 **************************************************************************/
 SET SERVEROUTPUT ON SIZE 1000000;
 
 DECLARE

BEGIN
DBMS_OUTPUT.PUT_LINE('TMS: Datafix script , Before start script');

delete from oe_order_headers_all where header_id=67597527;
DBMS_OUTPUT.put_line ('Records updated  -' || SQL%ROWCOUNT);
		   
COMMIT;

	  DBMS_OUTPUT.put_line ('TMS: 20180116-00156  , End Update');
EXCEPTION
 WHEN OTHERS THEN
 DBMS_OUTPUT.put_line ('TMS: 20180116-00156 , Errors : ' || SQLERRM);
END;
/