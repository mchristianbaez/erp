/*************************************************************************
   *   $Header TMS_20180321-00054_Data_Fix_Activating Conv_Conct_Prog.sql $
   *   Module Name: OM
   *
   *   PURPOSE:   Activating Inactivated Conversion programs.
   *
   *   REVISIONS:
   *   Ver        Date        Author                   Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        26/03/2018  Ashwin Sridhar           Initial Version
   *   
   * ***************************************************************************/

SET SERVEROUTPUT ON SIZE 100000;
SET VERIFY OFF;

DECLARE

CURSOR cur_prog IS
SELECT CONCURRENT_PROGRAM_ID
,      USER_CONCURRENT_PROGRAM_NAME
FROM   FND_CONCURRENT_PROGRAMS_VL
WHERE  USER_CONCURRENT_PROGRAM_NAME IN ('XXWC AR Customer Conversion'
,'XXWC AR Customer OE Ship To Conversion'
,'XXWC AR Inactivate Customers - (Converted)'
,'XXWC AP Supplier Conversion Import Program'
,'XXWC AP Supplier Banks Conversion Import Program'
,'XXWC INV Item Conversion Program'
,'XXWC INV Item Conversion SQL Loader Program'
,'XXWC INV Item Locator Conversion Program'
,'XXWC INV Item X - Reference Conversion Program'
,'XXWC INV Item Xref Conversion SQL Loader Program'
,'XXWC AR Invoice Conversion Import Program'
,'XXWC AR Invoice Conversion SQL Loader Program'
,'XXWC AR Receipts Conversion Import Program'
,'XXWC AR Receipts Conversion SQL Loader Program'
,'XXWC AP Open Invoice Conversion Import Program'
,'XXWC AP Open Invoice Conversion SQL Loader Program'
,'XXWC AP Closed Invoice Conversion Import Program'
,'XXWC AP Closed Invoice Conversion SQL Loader Program'
,'XXWC INV Onhand Balance Conversion Program'
,'XXWC INV Onhand Balance Conversion SQL Loader Program'
,'XXWC INV PUBD Update BOD after on hand conversion'
,'XXWC INV Update BOD after on hand conversion'
,'XXWC ASL Conversion'
,'XXWC ASL PO Conversion'
,'XXWC ASL PO Conversion SQL Loader'
,'XXWC PO Sourcing Rules Conversion Program'
,'XXWC Vendor Cost Improvements and BPA Conversions'
,'XXWC QP Price List Conversion Progam'
,'XXWC QP Price List Header Conversion Staging Load'
,'XXWC QP Price List Line Conversion Staging Load'
,'XXWC CSP MASS CONVERSION'
,'XXWC Load Rental Conversion Data'
,'XXWC Rental Sales Order Conversion'
,'XXWC INV PUBD Onhand Conversion Program'
,'XXWC INV PUBD Onhand Conversion SQL Loader Program'
);

ln_count NUMBER:=0;

BEGIN

  DBMS_OUTPUT.PUT_LINE('Activating De-activated Conversion concurrent programs...');
  
  FOR rec_prog IN cur_prog LOOP
  
    DBMS_OUTPUT.PUT_LINE('Concurrent Program Name -'||rec_prog.USER_CONCURRENT_PROGRAM_NAME);  
  
    ln_count:=ln_count+1;
  
    UPDATE FND_CONCURRENT_PROGRAMS_VL
	SET    enabled_flag='Y'
	WHERE  CONCURRENT_PROGRAM_ID=rec_prog.CONCURRENT_PROGRAM_ID;

  END LOOP;
  
  COMMIT;
  
  DBMS_OUTPUT.PUT_LINE('Count of concurrent programs updated '||ln_count);
 
EXCEPTION
WHEN others THEN

  DBMS_OUTPUT.PUT_LINE('Inside Exception '||SQLERRM); 
 
END;
/