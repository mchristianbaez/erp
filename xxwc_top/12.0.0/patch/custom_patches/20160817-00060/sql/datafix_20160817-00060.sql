
SET SERVEROUTPUT ON SIZE 100000;
SET VERIFY OFF;


begin

Delete from XXWC.XXWC_B2B_CAT_STG_TBL;

DBMS_OUTPUT.PUT_LINE('Rows Deleted '||sql%rowcount);

commit;

EXCEPTION
  WHEN others THEN
    dbms_output.put_line('Error occured  '||SQLERRM);
END;
/