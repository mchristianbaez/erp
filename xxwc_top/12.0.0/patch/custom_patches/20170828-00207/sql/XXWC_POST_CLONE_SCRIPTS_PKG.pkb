create or replace 
PACKAGE BODY      APPS.XXWC_POST_CLONE_SCRIPTS_PKG
AS
   /*************************************************************************
   *  Copyright (c) 2016 HD Supply Inc.
   *  All rights reserved.
   **************************************************************************
   *   $Header XXWC_POST_CLONE_SCRIPTS_PKG.pkb $
   *   Module Name: XXWC_POST_CLONE_SCRIPTS_PKG.pkb
   *
   *   PURPOSE:    This package is used to Automate Post Cloning activities.
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        09/04/2017  Krishna                 Initial Version TMS# 20170828-00207
   * ***************************************************************************/
   /*****************************
 -- GLOBAL VARIABLES
 ******************************/

   g_error_message      VARCHAR2 (32000);
   g_location           VARCHAR2 (15000);
   G_EXCEPTION          EXCEPTION;
   LVC_DIRECTORY_PATH   DBA_DIRECTORIES.DIRECTORY_PATH%TYPE;
   g_user_id            NUMBER := FND_GLOBAL.USER_ID;
   --Global variable to store the org id and conc request id
   g_conc_request_id    NUMBER := FND_GLOBAL.CONC_REQUEST_ID;
   g_org_id             NUMBER := FND_PROFILE.VALUE ('ORG_ID');


   /*************************************************************************
   *  Procedure : Write_Error
   *
   * PURPOSE:   This procedure logs error message in Custom Error table
   * Parameter:
   *        IN
   *            p_debug_msg      -- Debug Message
   * REVISIONS:
   * Ver        Date        Author                     Description
   *  ---------  ----------  ---------------         -------------------------
   *  1.0        09/05/2017  Krishna                 Initial Version TMS# 20170828-00207
  ************************************************************************/
   --add message to Error table
   PROCEDURE write_error (p_debug_msg IN VARCHAR2)
   IS
      l_err_callfrom    VARCHAR2 (100) DEFAULT 'XXWC_POST_CLONE_SCRIPTS_PKG';
      l_err_callpoint   VARCHAR2 (100) DEFAULT 'START';
      l_distro_list     VARCHAR2 (100)
                           DEFAULT 'HDSOracleDevelopers@hdsupply.com';
   BEGIN
      xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => l_err_callfrom,
         p_calling             => l_err_callpoint,
         p_request_id          => g_conc_request_id,
         p_ora_error_msg       => SUBSTR (p_debug_msg, 1, 2000),
         p_error_desc          => 'Error running XXWC_POST_CLONE_SCRIPTS_PKG with PROGRAM ERROR ',
         p_distribution_list   => l_distro_list,
         p_module              => 'INV');
   END write_error;

   /*************************************************************************
   *   Procedure : Write_output
   *
   *   PURPOSE:   This procedure is used for adding message to concurrent output file
   *   Parameter:
   *          IN
   *              p_debug_msg      -- Debug Message
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *  ---------  ----------  ---------------         -------------------------
   *   1.0        09/05/2017  Krishna                 Initial Version TMS# 20170828-00207
   * ************************************************************************/

   PROCEDURE Write_output (p_debug_msg IN VARCHAR2)
   IS
   BEGIN
      IF NVL (g_conc_request_id, 0) <= 0
      THEN
         DBMS_OUTPUT.PUT_LINE (p_debug_msg);
      ELSE
         fnd_file.put_line (fnd_file.output, p_debug_msg);
         fnd_file.put_line (fnd_file.LOG, p_debug_msg);
      END IF;
   END Write_output;

   PROCEDURE get_environment_var
   IS
      lv_environment_var   VARCHAR2 (100);
   BEGIN
      SELECT LOWER (name) INTO lv_environment_var FROM v$database;

      IF lv_environment_var = 'ebsprd'
      THEN
         g_location := 'This Script created for Non-Prod environments';
         write_output (g_location);
      ELSE
         LVC_DIRECTORY_PATH := lv_environment_var;
      END IF;
   END get_environment_var;

   /*************************************************************************
   *   Procedure : Upd_profile_val
   *
   *   PURPOSE:   This procedure is used to update profile values
   *   Parameter:
   *          IN None
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *  ---------  ----------  ---------------         -------------------------
   *   1.0        09/05/2017  Krishna                 Initial Version TMS# 20170828-00207
   * ************************************************************************/
   PROCEDURE upd_profile_val
   IS
   --LVC_DIRECTORY_PATH   DBA_DIRECTORIES.DIRECTORY_PATH%TYPE;
   BEGIN
      g_location := 'Start Update Profile Values';
      write_output (g_location);

      --SELECT LOWER (name) INTO LVC_DIRECTORY_PATH FROM v$database;
      /*IF LVC_DIRECTORY_PATH = 'ebsprd'
         THEN
            g_location := 'This Script created for Non-Prod environments';
            write_output (g_location);
     ELSE*/
      --Update Profile values for ECE inbound and outbound directories
      UPDATE applsys.fnd_profile_option_values v
         SET v.profile_option_value =
                '/xx_iface/' || LVC_DIRECTORY_PATH || '/inbound/EDI'
       WHERE     1 = 1
             AND EXISTS
                    (SELECT p.profile_option_id
                       FROM applsys.fnd_profile_options p
                      WHERE     1 = 1
                            AND p.profile_option_id = v.profile_option_id
                            AND p.profile_option_name IN ('ECE_IN_FILE_PATH',
                                                          'ECE_OUT_FILE_PATH'));

      UPDATE APPLSYS.fnd_profile_option_values
         SET profile_option_value =
                REPLACE (profile_option_value, 'ebsprd', LVC_DIRECTORY_PATH)
       WHERE profile_option_value LIKE '%ebsprd%';

      g_location := 'End Update profile values';
      write_output (g_location);
      -- END IF;

      g_location := 'Start Update lookup values';
      write_output (g_location);

      UPDATE fnd_lookup_values
         SET attribute2 = REPLACE (attribute2, 'ebsprd', LVC_DIRECTORY_PATH),
             attribute5 = REPLACE (attribute5, 'ebsprd', LVC_DIRECTORY_PATH)
       WHERE (attribute2 LIKE '%ebsprd%' OR attribute5 LIKE '%ebsprd%');

      g_location := 'End Update lookup values';
      write_output (g_location);
      COMMIT;
   END upd_profile_val;

   /*************************************************************************
      *   Procedure : upd_db_directories
      *
      *   PURPOSE:   This procedure is used to update DB directories
      *   Parameter:
      *          IN None
      *
      *   REVISIONS:
      *   Ver        Date        Author                     Description
      *  ---------  ----------  ---------------         -------------------------
      *   1.0        09/05/2017  Krishna                 Initial Version TMS# 20170828-00207
      * ************************************************************************/
   PROCEDURE upd_db_directories
   IS
      lvc_environment   VARCHAR2 (100);

      CURSOR c1
      IS
         SELECT *
           FROM dba_directories
          WHERE directory_path LIKE '%ebsprd%';

      --LVC_DIRECTORY_PATH   DBA_DIRECTORIES.DIRECTORY_PATH%TYPE;

      l_sql             VARCHAR2 (1000);
   BEGIN
      g_location := 'Start Update DB Directories';
      write_output (g_location);

      --SELECT LOWER (name) INTO lvc_environment FROM v$database;

      /*IF lvc_environment IS NOT NULL
      THEN
         IF lvc_environment = 'ebsprd'
         THEN
            g_location := 'This Script created for Non-Prod environments';
            write_output (g_location);
         ELSE*/
      FOR c2 IN c1
      LOOP
         LVC_DIRECTORY_PATH :=
            REPLACE (c2.DIRECTORY_PATH, 'ebsprd', lvc_environment);

         l_sql :=
               'CREATE OR REPLACE directory '
            || c2.directory_name
            || ' AS '
            || ''''
            || LVC_DIRECTORY_PATH
            || '''';

         g_location := l_sql;
         write_output (g_location);

         EXECUTE IMMEDIATE l_sql;
      END LOOP;

      --END IF;
      --END IF;
      g_location := 'End Update DB Driectories';
      write_output (g_location);
   EXCEPTION
      WHEN OTHERS
      THEN
         g_location := 'Error Occured ' || SUBSTR (SQLERRM, 1, 250);
         write_output (g_location);
   END upd_db_directories;

   /**************************************************************************
   *   procedure Name: update_user_email_address
   *  *   Parameter:
   *          IN    None
   *          OUT   None
   *   PURPOSE:  This procedure is used to update the user email addresses.
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        09/05/2017  Krishna                 Initial Version TMS# 20170828-00207
   * ***************************************************************************/
   PROCEDURE update_user_email_address
   IS
   BEGIN
      g_location := 'Start Update User Email Adress';
      write_output (g_location);

      UPDATE apps.fnd_user
         SET email_address = 'noreply@hdsupply.com'
       WHERE email_address IS NOT NULL;

      g_location := 'Records updated (FND_USERS): ' || SQL%ROWCOUNT;
      write_output (g_location);

      UPDATE apps.per_all_people_f
         SET email_address = 'noreply@hdsupply.com'
       WHERE email_address IS NOT NULL;

      g_location := 'Records updated (PER_ALL_PEOPLE_F): ' || SQL%ROWCOUNT;
      write_output (g_location);

      UPDATE APPS.WF_ROLES
         SET EMAIL_ADDRESS = 'noreply@hdsupply.com'
       WHERE email_address IS NOT NULL;

      g_location := 'Records updated (WF_ROLES): ' || SQL%ROWCOUNT;
      write_output (g_location);

      COMMIT;

      g_location := 'End Update User Email Adress';
      write_output (g_location);
   END update_user_email_address;

   /**************************************************************************
      *   procedure Name: conc_prog_param_upd
      *  *   Parameter:
      *          IN   None
      *   PURPOSE:  This procedure is used to update the concurrrent program parameters
      *   REVISIONS:
      *   Ver        Date        Author                     Description
      *   ---------  ----------  ---------------         -------------------------
      *   1.0        09/05/2017  Krishna                 Initial Version TMS# 20170828-00207
      * ***************************************************************************/
   PROCEDURE conc_prog_param_upd
   IS
   --LVC_DIRECTORY_PATH   DBA_DIRECTORIES.DIRECTORY_PATH%TYPE;
   BEGIN
      --SELECT LOWER (name) INTO LVC_DIRECTORY_PATH FROM v$database;

      /*IF LVC_DIRECTORY_PATH = 'ebsprd'
         THEN
            g_location := 'This Script created for Non-Prod environments';
            write_output (g_location);
      ELSE*/
      UPDATE FND_DESCR_FLEX_COLUMN_USAGES
         SET DEFAULT_VALUE =
                REPLACE (DEFAULT_VALUE, 'ebsprd', LVC_DIRECTORY_PATH)
       WHERE UPPER (DEFAULT_VALUE) LIKE '%EBSPRD%';
   --END IF;

   END conc_prog_param_upd;

   /**************************************************************************
   *   procedure Name: db_packages
   *  *   Parameter:
   *          IN   None
   *   PURPOSE:  This procedure is used to update the database packages
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        09/05/2017  Krishna                 Initial Version TMS# 20170828-00207
   * ***************************************************************************/
   PROCEDURE db_packages
   IS
      CURSOR c_pkg
      IS
         SELECT name, text
           FROM SYS.all_source
          WHERE text LIKE '%ebsprd%';
   BEGIN
      /*update sys.all_source
      set   text = replace(text,'ebsprd','ebsdev')
      where text like '%ebsprd%';
      */
      FOR J IN c_pkg
      LOOP
         g_location := 'Package ' || j.name || ' Text ' || j.text;
         write_output (g_location);
      END LOOP;
   END db_packages;

   /*************************************************************************
     *   Procedure : main
     *
     *   PURPOSE:   This procedure is called to execute the post clone scripts.
     *   Parameter:
     *          IN    None
     *          OUT   ERRBUF,RETCODE
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
     *   1.0        09/05/2017  Krishna                 Initial Version TMS# 20170828-00207
     * ************************************************************************/
   PROCEDURE main (X_ERRBUF OUT VARCHAR2, X_RETCODE OUT NUMBER)
   IS
      lvc_environment   VARCHAR2 (100);
   BEGIN
      --initializing variables
      g_error_message := NULL;
      X_ERRBUF := NULL;
      X_RETCODE := 0;
      --printing IN parameters
      g_location := 'Start main for XXWC_POST_CLONE_SCRIPTS_PKG';
      write_output (g_location);

      SELECT LOWER (name) INTO lvc_environment FROM v$database;

      IF lvc_environment LIKE '%prd%'
      THEN
         -- Actual logic started here
         g_location := 'Start updating profile values';
         write_output (g_location);

         upd_profile_val;

         g_location := 'Profile values updated';
         write_output (g_location);

         g_location := 'Start updating DB directories';
         write_output (g_location);

         upd_db_directories;

         g_location := 'DB directories updated';
         write_output (g_location);

         g_location := 'Start updating user emails';
         write_output (g_location);

         update_user_email_address;

         g_location := 'User Emails updated';
         write_output (g_location);

         -- Actual logic started here
         g_location := 'Start updating concurrent program parameters';
         write_output (g_location);

         conc_prog_param_upd;

         g_location := 'concurrent program parameters updated';
         write_output (g_location);
      ELSE
         fnd_file.put_line (
            fnd_file.LOG,
            'Program should be executed in Non-Prod enviornment only.');
         X_RETCODE := 2;
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_POST_CLONE_SCRIPTS_PKG.MAIN',
            p_calling             => 'MAIN',
            p_ora_error_msg       => 'Post Clonning script Program should not be executed in Production',
            p_error_desc          => 'Post Clonning script Program executed in Production',
            p_distribution_list   => 'wc-itdevalerts-u1@hdsupply.com',
            p_module              => 'XXWC');
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         FND_FILE.PUT_LINE (FND_FILE.LOG,
                            'Error occured ' || SUBSTR (SQLERRM, 1, 250));
         X_RETCODE := 2;
   END Main;
END XXWC_POST_CLONE_SCRIPTS_PKG;