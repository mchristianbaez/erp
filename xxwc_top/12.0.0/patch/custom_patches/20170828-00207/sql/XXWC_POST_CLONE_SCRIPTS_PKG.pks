create or replace 
PACKAGE      APPS.XXWC_POST_CLONE_SCRIPTS_PKG
IS
   /*************************************************************************
   *  Copyright (c) 2016 HD Supply Inc.
   *  All rights reserved.
   **************************************************************************
   *   $Header XXWC_POST_CLONE_SCRIPTS_PKG.pks $
   *   Module Name: XXWC_POST_CLONE_SCRIPTS_PKG.pks
   *
   *   PURPOSE:    This package is used to Automate Post Cloning activities.
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        09/04/2017  Krishna                 Initial Version TMS# 20170828-00207
   * ***************************************************************************/
   /**************************************************************************
 *   procedure Name: update_user_email_address
 *  *   Parameter:
 *          IN    None
 *          OUT   None
 *   PURPOSE:  This procedure is used to update the user email addresses.
 *   REVISIONS:
 *   Ver        Date        Author                     Description
 *   ---------  ----------  ---------------         -------------------------
 *   1.0        09/05/2017  Krishna                 Initial Version TMS# 20170828-00207
 * ***************************************************************************/
   PROCEDURE update_user_email_address;

   /**************************************************************************
   *   procedure Name: upd_db_directories
   *  *   Parameter:
   *          IN   None
   *   PURPOSE:  This procedure is used to update the db directories
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        09/05/2017  Krishna                 Initial Version TMS# 20170828-00207
   * ***************************************************************************/
   PROCEDURE upd_db_directories;

   /**************************************************************************
 *   procedure Name: upd_profile_val
 *  *   Parameter:
 *          IN    None
 *   PURPOSE:  This procedure is used to update profile values
 *   REVISIONS:
 *   Ver        Date        Author                     Description
 *   ---------  ----------  ---------------         -------------------------
 *   1.0        09/05/2017  Krishna                 Initial Version TMS# 20170828-00207
 * ***************************************************************************/
   PROCEDURE upd_profile_val;

   /**************************************************************************
     *   procedure Name: conc_prog_param_upd
     *  *   Parameter:
     *          IN   None
     *   PURPOSE:  This procedure is used to update the concurrrent program parameters
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
     *   1.0        09/05/2017  Krishna                 Initial Version TMS# 20170828-00207
     * ***************************************************************************/
   PROCEDURE conc_prog_param_upd;

   /**************************************************************************
   *   procedure Name: db_packages
   *  *   Parameter:
   *          IN   None
   *   PURPOSE:  This procedure is used to update the database packages
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        09/05/2017  Krishna                 Initial Version TMS# 20170828-00207
   * ***************************************************************************/
   PROCEDURE db_packages;

   /*************************************************************************
   *   Procedure : main
   *
   *   PURPOSE:   This procedure is called to execute the post clone scripts.
   *   Parameter:
   *          IN    None
   *          OUT   ERRBUF,RETCODE
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        09/05/2017  Krishna                 Initial Version TMS# 20170828-00207
   * ************************************************************************/

   PROCEDURE main (X_ERRBUF OUT VARCHAR2, X_RETCODE OUT NUMBER);
END XXWC_POST_CLONE_SCRIPTS_PKG;