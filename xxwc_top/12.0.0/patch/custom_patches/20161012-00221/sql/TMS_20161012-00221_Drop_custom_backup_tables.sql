/*************************************************************************
  $Header TMS_20161012-00221_Drop_custom_backup_tables.sql $
  Module Name: TMS#20161012-00221  --Data fix Script 

  PURPOSE: Data fix to drop the custom backup tables

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        03-NOV-2016 Pattabhi Avula         TMS#20161012-00221 

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 100000;
SET VERIFY OFF;
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20161012-00221    , Before Drop');
   
Execute immediate 'Drop table xxwc.xxwc_ms_backup_table';

Execute immediate 'Drop table xxwc.XXWC_INV_MIN_MAX_TEMP_BKP_1209';


   DBMS_OUTPUT.put_line ('TMS: 20161012-00221    , End Drop');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20161012-00221 , Errors : ' || SQLERRM);
END;
/