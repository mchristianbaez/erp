/*
 TMS:  20180910-00002
 Date: 09/10/2018
 Notes:  Add WELLSFARGO Payroll account to custom table for current day cash
*/
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
 --
 n_loc number :=0;
 l_sql varchar2(2000) :=Null;
 l_count number :=0;
 --
BEGIN --Main Processing...
   --
   n_loc :=101;
   --
	Insert into XXCUS.XXCUS_BANKS_BAI_CODES_MAPPING
	   (BANK_ACCOUNT_ID, BANK_ACCOUNT_NAME, BANK_ACCOUNT_NUM, BAI_CODE, REQUIRED_FLAG, 
		CREATED_BY, CREATION_DATE, LAST_UPDATED_BY, LAST_UPDATE_DATE)
	 Values
	   (54008, 'Well Fargo Corporate Payroll Tax', '4157737552', '060', 'R', 
		15837, SYSDATE, 15837, SYSDATE);
   --
   n_loc :=102;
   --   
   dbms_output.put_line
   (
     case when sql%rowcount >0 then 'Added Well Fargo Corporate Payroll Tax to table XXCUS.XXCUS_BANKS_BAI_CODES_MAPPING'
     else 'Failed to insert Well Fargo Corporate Payroll Tax to table XXCUS.XXCUS_BANKS_BAI_CODES_MAPPING'
     end 
   );
   --
   commit;
   --
EXCEPTION
   WHEN OTHERS THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20180910-00002, loc ='||n_loc||', Errors =' || SQLERRM);
END;
/