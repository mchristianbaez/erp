/*
   Ticket#                               Date         Author             Notes
   -------------------------------------------------------------------------------------------------------------------------------------------------------
   TMS 20180910-00002                    09/10/2018   Balaguru Seshadri  Grant so Finance IT team can maintain the custom table for new codes.
*/
--
GRANT SELECT,INSERT,UPDATE,DELETE ON XXCUS.XXCUS_BANKS_BAI_CODES_MAPPING TO BS006141;

GRANT SELECT,INSERT,UPDATE,DELETE ON XXCUS.XXCUS_BANKS_BAI_CODES_MAPPING TO ID020048;

GRANT SELECT,INSERT,UPDATE,DELETE ON XXCUS.XXCUS_BANKS_BAI_CODES_MAPPING TO KP059700;

GRANT SELECT,INSERT,UPDATE,DELETE ON XXCUS.XXCUS_BANKS_BAI_CODES_MAPPING TO AS066570;

GRANT SELECT,INSERT,UPDATE,DELETE ON XXCUS.XXCUS_BANKS_BAI_CODES_MAPPING TO VR073604;

--