   /* ************************************************************************
    HD Supply
    All rights reserved.
    Ticket                    Ver          Date         Author                       Description
    ----------------------    ----------   ----------   ------------------------     -------------------------
    Not available             1.0          08/21/2015   Production version     
    TMS 20151001-00046        1.5          11/18/2015   Balaguru Seshadri            Add two new fields
   ************************************************************************ */ 
alter table xxcus.XXCUSOZF_RBT_PAYMENTS_TBL add agreement_code varchar2(30);
alter table xxcus.XXCUSOZF_RBT_PAYMENTS_TBL add agreement_name varchar2(240);
--
comment on table xxcus.xxcusozf_rbt_payments_tbl  is 'TMS 20151001-00046';
--