   /* ************************************************************************
    HD Supply
    All rights reserved.
    Ticket                    Ver          Date         Author                       Description
    ----------------------    ----------   ----------   ------------------------     -------------------------
    Not available             1.0          08/21/2015   Production version     
    TMS 20151001-00046        1.5          11/18/2015   Balaguru Seshadri            Remove comemnted fields listed in Ver 1.5
   ************************************************************************ */
CREATE OR REPLACE FORCE VIEW "APPS"."XXCUS_PAM_PAYMENT_DET_V"
(
   "CUST_ID",
   "MVID",
   "MV_NAME",
   "PAYMENT_DATE",
   "GL_DATE",
   "CREATION_DATE",
   "RECEIPT_TIMEFRAME",
   "PAYMENT_AMOUNT",
   "CURRENCY_CODE",
   "BUSINESS_UNIT",
   "PAYMENT_METHOD",
   "RECEIPT_REFERENCE",
   "PAYMENT_NUMBER",
   "IMAGE_URL",
   "AMOUNT_DUE_REMAINING"
   ,"AGREEMENT_CODE" --Ver 1.5
   ,"AGREEMENT_NAME" --Ver 1.5
   ,"AGREEMENT_YEAR" --Ver 1.5
   ,"REBATE_TYPE" --Ver 1.5
)
AS
   SELECT cr.pay_from_customer cust_id,
          --hca.attribute2 mvid,
          hca.customer_attribute2 mvid, --new          
          --hp.party_name MV_NAME,
          hca.party_name MV_NAME,          
          CR.RECEIPT_DATE PAYMENT_DATE,
          sch.gl_date gl_date,
          cr.creation_date creation_date,
          cr.attribute2 receipt_timeframe,
          cr.amount payment_amount,
          cr.currency_code currency_code,
          hp1.party_name business_unit,
          rm.printed_name payment_method,
          cr.customer_receipt_reference receipt_reference,
          cr.receipt_number payment_number,
          cr.comments image_url,
          SCH.AMOUNT_DUE_REMAINING
          ,cr.attribute12 agreement_code --Ver 1.5
          ,cr.attribute13 agreement_name  --Ver 1.5
          ,cr.attribute11 agreement_year --Ver 1.5
          ,cr.attribute10 rebate_type --Ver 1.5   
     FROM APPS.AR_CASH_RECEIPTS_ALL CR,
          APPS.AR_RECEIPT_METHODS RM, --NEW
          --APPS.HZ_PARTIES HP,
          --APPS.HZ_CUST_ACCOUNTS HCA,
          APPS.HZ_PARTIES HP1,
          apps.AR_PAYMENT_SCHEDULES_ALL sch
          ,XXCUS.XXCUS_REBATE_CUSTOMERS hca--new
    WHERE     1 = 1
          --AND cr.attribute1 = TO_CHAR (hp1.party_id)
          AND hp1.attribute1 ='HDS_BU' --New
          AND to_number(cr.attribute1) =hp1.party_id          
          AND cr.status NOT IN ('REV')
          AND cr.receipt_method_id = rm.receipt_method_id
          --AND hca.cust_account_id = cr.pay_from_customer
          AND hca.customer_id = cr.pay_from_customer --new          
          --AND hca.party_id = hp.party_id 
          AND cr.org_id IN (101, 102)
          AND sch.CASH_RECEIPT_ID = cr.CASH_RECEIPT_ID;
		  /* -- Begin Ver 1.5
   SELECT cr.pay_from_customer cust_id,
          hca.attribute2 mvid,
          hp.party_name MV_NAME,
          CR.RECEIPT_DATE PAYMENT_DATE,
          sch.gl_date gl_date,
          cr.creation_date creation_date,
          cr.attribute2 receipt_timeframe,
          cr.amount payment_amount,
          cr.currency_code currency_code,
          hp1.party_name business_unit,
          rm.printed_name payment_method,
          cr.customer_receipt_reference receipt_reference,
          cr.receipt_number payment_number,
          cr.comments image_url,
          SCH.AMOUNT_DUE_REMAINING   
     FROM APPS.AR_CASH_RECEIPTS_ALL CR,
          APPS.AR_RECEIPT_METHODS RM,
          APPS.HZ_PARTIES HP,
          APPS.HZ_CUST_ACCOUNTS HCA,
          APPS.HZ_PARTIES HP1,
          apps.AR_PAYMENT_SCHEDULES_ALL sch
    WHERE     1 = 1
          AND cr.attribute1 = TO_CHAR (hp1.party_id)
          AND cr.status NOT IN ('REV')
          AND cr.receipt_method_id = rm.receipt_method_id
          AND hca.cust_account_id = cr.pay_from_customer
          AND hca.party_id = hp.party_id
          AND cr.org_id IN (101, 102)
          AND sch.CASH_RECEIPT_ID = cr.CASH_RECEIPT_ID
          AND hp1.party_name NOT LIKE '%PLUMB%'
          AND HP1.PARTY_NAME NOT LIKE '%INDUSTR%';
         */ -- End Ver 1.5
--
COMMENT ON TABLE APPS.XXCUS_PAM_PAYMENT_DET_V IS 'TMS 20151001-00046';
--