CREATE OR REPLACE PACKAGE BODY APPS.xxcusgl_khalix_extracts_pkg IS

  /********************************************************************************
  
  File Name: XXCUSGL_KHALIX_EXTRACTS_PKG
  
  PROGRAM TYPE: PL/SQL Package spec and body
  
  PURPOSE: Procedure to extracting GL Balances for Khalix Reporting
  
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     06/22/2011    Mani Kumar       Initial creation of the package with copied 
                                         compliance to R12 ledger and product setup
  1.1     03/28/2012    Luong Vu         CAD fix - RFC 33260         
  1.2     03/23/2012    Luong Vu         RFC 33225 - Change output directory name
                                         - Change output write method to write to temp
                                         file and then rename the file. (SR 135911)    
  1.3     10/22/2012    Luong Vu         SR 163293 - mod. utl_file.frename to overwrite
                                         existing file for both procedures.   
  1.4     08/17/2015   Balaguru Seshadri New routines for Anixter                                                                                                                                                                                                  
  ********************************************************************************/

  PROCEDURE create_khalix_extract(p_errbuf      OUT VARCHAR2
                                 ,p_retcode     OUT NUMBER
                                 ,p_period_name IN VARCHAR2
                                 ,p_line_of_bus IN VARCHAR2) IS
    l_procedure_name VARCHAR2(75) := 'XXCUSGL_KHALIX_EXTRACTS_PKG.create_khalix_extract';
    l_file_handle    utl_file.file_type;
    l_file_dir       VARCHAR2(100);
    l_file_name      VARCHAR2(150);
    l_file_name_f    VARCHAR2(150);
    l_sid            VARCHAR2(50); --v$database.NAME%type;
    l_period_val     VARCHAR2(10);
    l_line_cnt       NUMBER := 0;
    l_location       VARCHAR2(100) := NULL;
    l_req_id         NUMBER := fnd_global.conc_request_id;
    l_distro_list    VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
    --l_distro_list VARCHAR2(75) DEFAULT 'luong.vu@hdsupply.com';
  
    CURSOR balance_extr IS
      SELECT 'A' || acct.segment4 account
            , --Account
             'A' ||
             substr(glbal.period_year, length(glbal.period_year) - 1) ||
             lpad(glbal.period_num, 2, '0') || 'YTD' timeper
            , --TIMEPER      
             'P' || acct.segment1 product
            , --Product
             'C' || ffv.attribute2 currency
            , --Currency
             'CC' || acct.segment3 cost_center
            , --Cost Center,
             acct.segment2 location
            , --Location
             'PC' || acct.segment5 project_code_details
            , --Project Code/Details          
             decode(decode(substr(ffv_account.compiled_value_attributes, 5,
                                  1), 'L', 'Y', 'R', 'Y', 'O', 'Y', 'N'), 'Y',
                    ((SUM(nvl(glbal.begin_balance_dr, 0) -
                           nvl(glbal.begin_balance_cr, 0) +
                           (nvl(glbal.period_net_dr, 0) -
                            nvl(glbal.period_net_cr, 0)))) * -1),
                    SUM(nvl(glbal.begin_balance_dr, 0) -
                         nvl(glbal.begin_balance_cr, 0) +
                         (nvl(glbal.period_net_dr, 0) -
                          nvl(glbal.period_net_cr, 0)))) ytd_amount --YTD Amount
        FROM gl.gl_code_combinations acct
            ,gl.gl_balances          glbal
            ,gl.gl_ledgers           gl_led
            ,fnd_flex_values         ffv
            ,fnd_flex_values         ffv_account
            ,fnd_flex_value_sets     ffvs_account
       WHERE acct.code_combination_id = glbal.code_combination_id
         AND glbal.actual_flag = 'A'
         AND ffv.flex_value = acct.segment1
         AND ffv.value_category = 'XXCUS_GL_PRODUCT'
         AND gl_led.currency_code = ffv.attribute2
         AND gl_led.ledger_id = glbal.ledger_id
         AND upper(ffvs_account.flex_value_set_name) =
             upper('XXCUS_GL_ACCOUNT')
         AND ffv_account.flex_value_set_id = ffvs_account.flex_value_set_id
         AND ffv_account.flex_value = acct.segment4
         AND (acct.segment1 IN
             (SELECT fxvnh.child_flex_value_low
                 FROM fnd_flex_value_norm_hierarchy fxvnh
                WHERE upper(fxvnh.parent_flex_value) =
                      upper(nvl(p_line_of_bus, fxvnh.parent_flex_value)) --Parameter: Business
               ) OR
             acct.segment1 = upper(nvl(p_line_of_bus, acct.segment1))) --Parameter: Business
         AND upper(glbal.period_name) = upper(p_period_name) --Parameter: Period name
         AND ffv.attribute1 = glbal.ledger_id --v1.1 START
         AND ffv.attribute2 = glbal.currency_code --END
       GROUP BY acct.segment4
               ,glbal.period_year
               ,glbal.period_num
               ,acct.segment1
               ,ffv.attribute2
               ,acct.segment3
               ,acct.segment2
               ,acct.segment5
               ,ffv_account.compiled_value_attributes
      HAVING((SUM(nvl(glbal.begin_balance_dr, 0) - nvl(glbal.begin_balance_cr, 0) + (nvl(glbal.period_net_dr, 0) - nvl(glbal.period_net_cr, 0))) <> 0) OR (SUM(nvl(glbal.period_net_dr, 0) - nvl(glbal.period_net_cr, 0)) <> 0))
       ORDER BY acct.segment5;
  
  BEGIN
    l_location := 'Extract Database Name';
    SELECT lower(NAME)
      INTO l_sid
      FROM v$database;
  
    l_location := 'Find Period Values for File Name';
    SELECT substr(glp.period_year, length(glp.period_year) - 1) ||
           lpad(glp.period_num, 2, '0')
      INTO l_period_val
      FROM gl_periods glp
     WHERE glp.period_name = p_period_name
       AND glp.period_set_name =
           (SELECT period_set_name
              FROM gl_access_sets
             WHERE access_set_id = fnd_profile.value('GL_ACCESS_SET_ID'));
  
    l_location := 'Outbound Directory on UNIX';
    --l_file_dir  := 'ORACLE_INT_UC4'; --'/xx_iface/' || l_sid || '/outbound';--Open Issue
  
    -- v1.2 START
    l_file_dir  := 'ORACLE_INT_UC4_GL_KHALIX';
    l_file_name := 'TEMP_' || 'P' || p_line_of_bus || '_GL_ACTUALS_' ||
                   l_period_val || '.txt';
  
    l_file_name_f := 'P' || p_line_of_bus || '_GL_ACTUALS_' || l_period_val ||
                     '.txt';
    -- v1.2 END
  
    fnd_file.put_line(fnd_file.log, 'Filename generated : ' || l_file_name);
    l_file_handle := utl_file.fopen(l_file_dir, l_file_name, 'w');
  
    l_location := 'Opening For Loop';
    FOR c_feed_rec IN balance_extr
    LOOP
    
      utl_file.put_line(l_file_handle,
                        c_feed_rec.account || '{' || c_feed_rec.timeper || '{' ||
                         c_feed_rec.product || '{' || c_feed_rec.currency || '{' ||
                         c_feed_rec.cost_center || '{' ||
                         c_feed_rec.location || '{' ||
                         c_feed_rec.project_code_details || '{' || 'DIM7SET' || '{' || 'N' || '{' ||
                         c_feed_rec.ytd_amount);
    
      fnd_file.put_line(fnd_file.output,
                        c_feed_rec.account || '{' || c_feed_rec.timeper || '{' ||
                         c_feed_rec.product || '{' || c_feed_rec.currency || '{' ||
                         c_feed_rec.cost_center || '{' ||
                         c_feed_rec.location || '{' ||
                         c_feed_rec.project_code_details || '{' || 'DIM7SET' || '{' || 'N' || '{' ||
                         c_feed_rec.ytd_amount);
      l_line_cnt := l_line_cnt + 1;
    END LOOP;
    fnd_file.put_line(fnd_file.log,
                      'No. of Records written to ' || l_file_name || ' :' ||
                       l_line_cnt);
    utl_file.fclose(l_file_handle);
  
    utl_file.frename(l_file_dir, l_file_name, l_file_dir, l_file_name_f,
                     TRUE); --v1.2 v1.3
  
  EXCEPTION
    WHEN too_many_rows THEN
      p_retcode := 900021;
      p_errbuf  := 'XXCUSGL: ' || p_retcode || substr(SQLERRM, 1, 1900) ||
                   ' in ' || l_procedure_name || ' at ' || l_location;
      fnd_file.put_line(fnd_file.log, p_errbuf);
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_procedure_name,
                                           p_calling => 'Exception for XXCUSGL_KHALIX_EXTRACTS_PKG',
                                           p_request_id => l_req_id,
                                           p_ora_error_msg => p_retcode,
                                           p_error_desc => p_errbuf,
                                           p_distribution_list => l_distro_list,
                                           p_module => 'XXCUS');
    WHEN OTHERS THEN
      p_retcode := 900022;
      p_errbuf  := 'XXCUSGL: ' || p_retcode ||
                   ': Error During file extract in ' || l_procedure_name ||
                   ' at ' || l_location || ' - ' ||
                   substr(SQLERRM, 1, 1900);
      fnd_file.put_line(fnd_file.log, p_errbuf);
      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_procedure_name,
                                           p_calling => 'Exception for XXCUSGL_KHALIX_EXTRACTS_PKG',
                                           p_request_id => l_req_id,
                                           p_ora_error_msg => p_retcode,
                                           p_error_desc => p_errbuf,
                                           p_distribution_list => l_distro_list,
                                           p_module => 'XXCUS');
  END create_khalix_extract;
  -- Begin Ver 1.4
  PROCEDURE create_khalix_extract_anixter(p_errbuf      OUT VARCHAR2
                                 ,p_retcode     OUT NUMBER
                                 ,p_period_name IN VARCHAR2
                                 ,p_line_of_bus IN VARCHAR2) IS
    l_procedure_name VARCHAR2(75) := 'XXCUSGL_KHALIX_EXTRACTS_PKG.create_khalix_extract_anixter';
    l_file_handle    utl_file.file_type;
    l_file_dir       VARCHAR2(100);
    l_file_name      VARCHAR2(150);
    l_file_name_f    VARCHAR2(150);
    l_sid            VARCHAR2(50); --v$database.NAME%type;
    l_period_val     VARCHAR2(10);
    l_line_cnt       NUMBER := 0;
    l_location       VARCHAR2(100) := NULL;
    l_req_id         NUMBER := fnd_global.conc_request_id;
    l_distro_list    VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
    --l_distro_list VARCHAR2(75) DEFAULT 'luong.vu@hdsupply.com';
  
    CURSOR balance_extr IS
      SELECT 'A' || acct.segment4 account
            , --Account
             'A' ||
             substr(glbal.period_year, length(glbal.period_year) - 1) ||
             lpad(glbal.period_num, 2, '0') || 'YTD' timeper
            , --TIMEPER      
             'P' || acct.segment1 product
            , --Product
             'C' || ffv.attribute2 currency
            , --Currency
             'CC' || acct.segment3 cost_center
            , --Cost Center,
             acct.segment2 location
            , --Location
             'PC' || acct.segment5 project_code_details
            , --Project Code/Details          
             decode(decode(substr(ffv_account.compiled_value_attributes, 5,
                                  1), 'L', 'Y', 'R', 'Y', 'O', 'Y', 'N'), 'Y',
                    ((SUM(nvl(glbal.begin_balance_dr, 0) -
                           nvl(glbal.begin_balance_cr, 0) +
                           (nvl(glbal.period_net_dr, 0) -
                            nvl(glbal.period_net_cr, 0)))) * -1),
                    SUM(nvl(glbal.begin_balance_dr, 0) -
                         nvl(glbal.begin_balance_cr, 0) +
                         (nvl(glbal.period_net_dr, 0) -
                          nvl(glbal.period_net_cr, 0)))) ytd_amount --YTD Amount
        FROM gl.gl_code_combinations acct
            ,gl.gl_balances          glbal
            ,gl.gl_ledgers           gl_led
            ,fnd_flex_values         ffv
            ,fnd_flex_values         ffv_account
            ,fnd_flex_value_sets     ffvs_account
       WHERE acct.code_combination_id = glbal.code_combination_id
         AND glbal.actual_flag = 'A'
         AND ffv.flex_value = acct.segment1
         AND ffv.value_category = 'XXCUS_GL_PRODUCT'
         AND gl_led.currency_code = ffv.attribute2
         AND gl_led.ledger_id = glbal.ledger_id
         AND upper(ffvs_account.flex_value_set_name) =
             upper('XXCUS_GL_ACCOUNT')
         AND ffv_account.flex_value_set_id = ffvs_account.flex_value_set_id
         AND ffv_account.flex_value = acct.segment4
         AND (acct.segment1 IN
             (SELECT fxvnh.child_flex_value_low
                 FROM fnd_flex_value_norm_hierarchy fxvnh
                WHERE upper(fxvnh.parent_flex_value) =
                      upper(nvl(p_line_of_bus, fxvnh.parent_flex_value)) --Parameter: Business
               ) OR
             acct.segment1 = upper(nvl(p_line_of_bus, acct.segment1))) --Parameter: Business
         AND upper(glbal.period_name) = upper(p_period_name) --Parameter: Period name
         AND ffv.attribute1 = glbal.ledger_id --v1.1 START
         AND ffv.attribute2 = glbal.currency_code --END
       GROUP BY acct.segment4
               ,glbal.period_year
               ,glbal.period_num
               ,acct.segment1
               ,ffv.attribute2
               ,acct.segment3
               ,acct.segment2
               ,acct.segment5
               ,ffv_account.compiled_value_attributes
      HAVING((SUM(nvl(glbal.begin_balance_dr, 0) - nvl(glbal.begin_balance_cr, 0) + (nvl(glbal.period_net_dr, 0) - nvl(glbal.period_net_cr, 0))) <> 0) OR (SUM(nvl(glbal.period_net_dr, 0) - nvl(glbal.period_net_cr, 0)) <> 0))
       ORDER BY acct.segment5;
  
  BEGIN
    l_location := 'Extract Database Name';
    SELECT lower(NAME)
      INTO l_sid
      FROM v$database;
  
    l_location := 'Find Period Values for File Name';
    SELECT substr(glp.period_year, length(glp.period_year) - 1) ||
           lpad(glp.period_num, 2, '0')
      INTO l_period_val
      FROM gl_periods glp
     WHERE glp.period_name = p_period_name
       AND glp.period_set_name =
           (SELECT period_set_name
              FROM gl_access_sets
             WHERE access_set_id = fnd_profile.value('GL_ACCESS_SET_ID'));
  
    l_location := 'Outbound Directory on UNIX';
    --l_file_dir  := 'ORACLE_INT_UC4'; --'/xx_iface/' || l_sid || '/outbound';--Open Issue
  
    -- v1.2 START
    l_file_dir  := 'ORACLE_INT_UC4_GL_KHALIX';
    l_file_name := 'TEMP_' || 'P' || p_line_of_bus || '_GL_ANIXTER_' ||
                   l_period_val || '.txt';
  
    l_file_name_f := 'P' || p_line_of_bus || '_GL_ANIXTER_' || l_period_val ||
                     '.txt';
    -- v1.2 END
  
    fnd_file.put_line(fnd_file.log, 'Filename generated : ' || l_file_name);
    l_file_handle := utl_file.fopen(l_file_dir, l_file_name, 'w');
  
    l_location := 'Opening For Loop';
    FOR c_feed_rec IN balance_extr
    LOOP
    
      utl_file.put_line(l_file_handle,
                        c_feed_rec.account || '{' || c_feed_rec.timeper || '{' ||
                         c_feed_rec.product || '{' || c_feed_rec.currency || '{' ||
                         c_feed_rec.cost_center || '{' ||
                         c_feed_rec.location || '{' ||
                         c_feed_rec.project_code_details || '{' || 'ANIXTER' || '{' || 'N' || '{' ||
                         c_feed_rec.ytd_amount);
    
      fnd_file.put_line(fnd_file.output,
                        c_feed_rec.account || '{' || c_feed_rec.timeper || '{' ||
                         c_feed_rec.product || '{' || c_feed_rec.currency || '{' ||
                         c_feed_rec.cost_center || '{' ||
                         c_feed_rec.location || '{' ||
                         c_feed_rec.project_code_details || '{' || 'ANIXTER' || '{' || 'N' || '{' ||
                         c_feed_rec.ytd_amount);
      l_line_cnt := l_line_cnt + 1;
    END LOOP;
    fnd_file.put_line(fnd_file.log,
                      'No. of Records written to ' || l_file_name || ' :' ||
                       l_line_cnt);
    utl_file.fclose(l_file_handle);
  
    utl_file.frename(l_file_dir, l_file_name, l_file_dir, l_file_name_f,
                     TRUE); --v1.2 v1.3
  
  EXCEPTION
    WHEN too_many_rows THEN
      p_retcode := 900021;
      p_errbuf  := 'XXCUSGL: ' || p_retcode || substr(SQLERRM, 1, 1900) ||
                   ' in ' || l_procedure_name || ' at ' || l_location;
      fnd_file.put_line(fnd_file.log, p_errbuf);
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_procedure_name,
                                           p_calling => 'Exception for XXCUSGL_KHALIX_EXTRACTS_PKG',
                                           p_request_id => l_req_id,
                                           p_ora_error_msg => p_retcode,
                                           p_error_desc => p_errbuf,
                                           p_distribution_list => l_distro_list,
                                           p_module => 'XXCUS');
    WHEN OTHERS THEN
      p_retcode := 900022;
      p_errbuf  := 'XXCUSGL: ' || p_retcode ||
                   ': Error During file extract in ' || l_procedure_name ||
                   ' at ' || l_location || ' - ' ||
                   substr(SQLERRM, 1, 1900);
      fnd_file.put_line(fnd_file.log, p_errbuf);
      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_procedure_name,
                                           p_calling => 'Exception for XXCUSGL_KHALIX_EXTRACTS_PKG',
                                           p_request_id => l_req_id,
                                           p_ora_error_msg => p_retcode,
                                           p_error_desc => p_errbuf,
                                           p_distribution_list => l_distro_list,
                                           p_module => 'XXCUS');
  END create_khalix_extract_anixter;   
  -- End Ver 1.4
  /********************************************************************************
  
  File Name: XXCUSGL_KHALIX_EXTRACTS_PKG
  
  PROGRAM TYPE: PL/SQL Package spec and body
  
  PURPOSE: Procedure to extracting GL Balances for Khalix Reporting
  
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     06/22/2011    Luong Vu        GL Khalix Extract for IPVF 
                                        RFC #   33478
                                                                            
  ********************************************************************************/

  PROCEDURE create_khalix_extract_ipvf(p_errbuf      OUT VARCHAR2
                                      ,p_retcode     OUT NUMBER
                                      ,p_period_name IN VARCHAR2
                                      ,p_line_of_bus IN VARCHAR2) IS
    l_procedure_name VARCHAR2(75) := 'XXCUSGL_KHALIX_EXTRACTS_PKG.create_khalix_extract_ipvf';
    l_file_handle    utl_file.file_type;
    l_file_dir       VARCHAR2(100);
    l_file_name      VARCHAR2(150);
    l_file_name_f    VARCHAR2(150);
    l_sid            VARCHAR2(50); --v$database.NAME%type;
    l_period_val     VARCHAR2(10);
    l_line_cnt       NUMBER := 0;
    l_sec            VARCHAR2(100) := NULL;
    l_req_id         NUMBER := fnd_global.conc_request_id;
    l_distro_list    VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
  
    CURSOR balance_extr IS
      SELECT 'A' || acct.segment4 account
            , --Account
             'A' ||
             substr(glbal.period_year, length(glbal.period_year) - 1) ||
             lpad(glbal.period_num, 2, '0') || 'YTD' timeper
            , --TIMEPER      
             'P' || acct.segment1 product
            , --Product
             'C' || ffv.attribute2 currency
            , --Currency
             'CC' || acct.segment3 cost_center
            , --Cost Center,
             acct.segment2 location
            , --Location
             'PC' || acct.segment5 project_code_details
            , --Project Code/Details          
             decode(decode(substr(ffv_account.compiled_value_attributes, 5,
                                  1), 'L', 'Y', 'R', 'Y', 'O', 'Y', 'N'), 'Y',
                    ((SUM(nvl(glbal.begin_balance_dr, 0) -
                           nvl(glbal.begin_balance_cr, 0) +
                           (nvl(glbal.period_net_dr, 0) -
                            nvl(glbal.period_net_cr, 0)))) * -1),
                    SUM(nvl(glbal.begin_balance_dr, 0) -
                         nvl(glbal.begin_balance_cr, 0) +
                         (nvl(glbal.period_net_dr, 0) -
                          nvl(glbal.period_net_cr, 0)))) ytd_amount --YTD Amount
        FROM gl.gl_code_combinations acct
            ,gl.gl_balances          glbal
            ,gl.gl_ledgers           gl_led
            ,fnd_flex_values         ffv
            ,fnd_flex_values         ffv_account
            ,fnd_flex_value_sets     ffvs_account
       WHERE acct.code_combination_id = glbal.code_combination_id
         AND glbal.actual_flag = 'A'
         AND ffv.flex_value = acct.segment1
         AND ffv.value_category = 'XXCUS_GL_PRODUCT'
         AND gl_led.currency_code = ffv.attribute2
         AND gl_led.ledger_id = glbal.ledger_id
         AND upper(ffvs_account.flex_value_set_name) =
             upper('XXCUS_GL_ACCOUNT')
         AND ffv_account.flex_value_set_id = ffvs_account.flex_value_set_id
         AND ffv_account.flex_value = acct.segment4
         AND (acct.segment1 IN
             (SELECT fxvnh.child_flex_value_low
                 FROM fnd_flex_value_norm_hierarchy fxvnh
                WHERE upper(fxvnh.parent_flex_value) =
                      upper(nvl(p_line_of_bus, fxvnh.parent_flex_value)) --Parameter: Business
               ) OR
             acct.segment1 = upper(nvl(p_line_of_bus, acct.segment1))) --Parameter: Business
         AND upper(glbal.period_name) = upper(p_period_name) --Parameter: Period name
         AND ffv.attribute1 = glbal.ledger_id
         AND ffv.attribute2 = glbal.currency_code
       GROUP BY acct.segment4
               ,glbal.period_year
               ,glbal.period_num
               ,acct.segment1
               ,ffv.attribute2
               ,acct.segment3
               ,acct.segment2
               ,acct.segment5
               ,ffv_account.compiled_value_attributes
      HAVING((SUM(nvl(glbal.begin_balance_dr, 0) - nvl(glbal.begin_balance_cr, 0) + (nvl(glbal.period_net_dr, 0) - nvl(glbal.period_net_cr, 0))) <> 0) OR (SUM(nvl(glbal.period_net_dr, 0) - nvl(glbal.period_net_cr, 0)) <> 0))
       ORDER BY acct.segment5;
  
  BEGIN
    l_sec := 'Extract Database Name';
    SELECT lower(NAME)
      INTO l_sid
      FROM v$database;
  
    l_sec := 'Find Period Values for File Name';
    SELECT substr(glp.period_year, length(glp.period_year) - 1) ||
           lpad(glp.period_num, 2, '0')
      INTO l_period_val
      FROM gl_periods glp
     WHERE glp.period_name = p_period_name
       AND glp.period_set_name =
           (SELECT period_set_name
              FROM gl_access_sets
             WHERE access_set_id = fnd_profile.value('GL_ACCESS_SET_ID'));
  
    l_sec := 'Outbound Directory on UNIX';
  
    l_file_dir  := 'ORACLE_INT_UC4_GL_KHALIX';
    l_file_name := 'TEMP_' || 'P' || 'SHALE' || '_GL_ACTUALS_' ||
                   l_period_val || '.txt';
  
    l_file_name_f := 'P' || 'SHALE' || '_GL_ACTUALS_' || l_period_val ||
                     '.txt';
  
    fnd_file.put_line(fnd_file.log, 'Filename generated : ' || l_file_name);
    l_file_handle := utl_file.fopen(l_file_dir, l_file_name, 'w');
  
    l_sec := 'Opening For Loop';
    FOR c_feed_rec IN balance_extr
    LOOP
    
      utl_file.put_line(l_file_handle,
                        c_feed_rec.account || '{' || c_feed_rec.timeper || '{' ||
                         c_feed_rec.product || '{' || c_feed_rec.currency || '{' ||
                         c_feed_rec.cost_center || '{' ||
                         c_feed_rec.location || '{' ||
                         c_feed_rec.project_code_details || '{' || 'SHALE' || '{' || 'N' || '{' ||
                         c_feed_rec.ytd_amount);
    
      fnd_file.put_line(fnd_file.output,
                        c_feed_rec.account || '{' || c_feed_rec.timeper || '{' ||
                         c_feed_rec.product || '{' || c_feed_rec.currency || '{' ||
                         c_feed_rec.cost_center || '{' ||
                         c_feed_rec.location || '{' ||
                         c_feed_rec.project_code_details || '{' || 'SHALE' || '{' || 'N' || '{' ||
                         c_feed_rec.ytd_amount);
      l_line_cnt := l_line_cnt + 1;
    END LOOP;
    fnd_file.put_line(fnd_file.log,
                      'No. of Records written to ' || l_file_name || ' :' ||
                       l_line_cnt);
    utl_file.fclose(l_file_handle);
  
    utl_file.frename(l_file_dir, l_file_name, l_file_dir, l_file_name_f,
                     TRUE); --v1.2 v1.3
  
  EXCEPTION
    WHEN too_many_rows THEN
      p_retcode := 900021;
      p_errbuf  := 'XXCUSGL: ' || p_retcode || substr(SQLERRM, 1, 1900) ||
                   ' in ' || l_procedure_name || ' at ' || l_sec;
      fnd_file.put_line(fnd_file.log, p_errbuf);
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_procedure_name,
                                           p_calling => 'Exception for XXCUSGL_KHALIX_EXTRACTS_PKG',
                                           p_request_id => l_req_id,
                                           p_ora_error_msg => p_retcode,
                                           p_error_desc => p_errbuf,
                                           p_distribution_list => l_distro_list,
                                           p_module => 'XXCUS');
    WHEN OTHERS THEN
      p_retcode := 900022;
      p_errbuf  := 'XXCUSGL: ' || p_retcode ||
                   ': Error During file extract in ' || l_procedure_name ||
                   ' at ' || l_sec || ' - ' || substr(SQLERRM, 1, 1900);
      fnd_file.put_line(fnd_file.log, p_errbuf);
      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_procedure_name,
                                           p_calling => 'Exception for XXCUSGL_KHALIX_EXTRACTS_PKG',
                                           p_request_id => l_req_id,
                                           p_ora_error_msg => p_retcode,
                                           p_error_desc => p_errbuf,
                                           p_distribution_list => l_distro_list,
                                           p_module => 'XXCUS');
  END create_khalix_extract_ipvf;
END xxcusgl_khalix_extracts_pkg;
/