/*************************************************************************
  $Header TMS_20161212-00028_Incomplete_reversal_entries_in_subledger.sql $
   PURPOSE: Data fix to update the Period End Accrual issue records

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        13-DEC-2016 Ram Talluri         TMS#20161212-00028  

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 100000;
SET VERIFY OFF;
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20161212-00028    , Before Update');
   
UPDATE xla_ae_headers
   SET gl_transfer_status_code = 'N'  -- (Not Transferable)
 WHERE     application_id = 707
       AND event_type_code IN ('PERIOD_END_ACCRUAL')
       AND ledger_id = 2061
       AND GL_TRANSFER_STATUS_CODE = 'NT'
       AND accounting_date =to_date('13-DEC-2016', 'DD-MON-YYYY');

 DBMS_OUTPUT.put_line (
         'TMS: 20161212-00028  Period End Accrual Fix updated Records: '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20161212-00028    , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20161212-00028 , Errors : ' || SQLERRM);
END;
/