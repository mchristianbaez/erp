/*************************************************************************
  $Header TMS_20161212-00028_Data_Fix_clear_mti.sql $
  Module Name: TMS_20161212-00028  Data Fix script 

  PURPOSE: Data Fix script 

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        12-DEC-2016  Ram Talluri        TMS#20161212-00028

**************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000;

DECLARE
   l_sql1      VARCHAR2 (32000);
   l_sql2      VARCHAR2 (32000);
   l_sql3      VARCHAR2 (32000);
   l_cnt       NUMBER;
   l_cnt1      NUMBER;
   l_tab_cnt   NUMBER := 0;
BEGIN
   BEGIN
      SELECT COUNT (1)
        INTO l_tab_cnt
        FROM all_tables
       WHERE table_name = 'XXWC_MTI_QA_BKP' AND owner = 'XXWC';

      IF l_tab_cnt > 0
      THEN
         EXECUTE IMMEDIATE 'DROP TABLE XXWC.XXWC_MTI_QA_BKP';

         DBMS_OUTPUT.put_line ('TMS: 20161212-00028    , Table Dropped');
      ELSE
         BEGIN
            DBMS_OUTPUT.put_line (
               'TMS: 20161212-00028    , Before Create table');
            l_sql1 :=
                  'CREATE TABLE XXWC.XXWC_MTI_QA_BKP AS '
               || 'select * from mtl_transactions_interface where source_code =''ORDER ENTRY''';

            EXECUTE IMMEDIATE l_sql1;

            COMMIT;
         END;

         DBMS_OUTPUT.put_line (
            'TMS: 20161212-00028    , After Created table');

         BEGIN
            SELECT COUNT (1)
              INTO l_cnt
              FROM all_tables
             WHERE table_name = 'XXWC_MTI_QA_BKP' AND owner = 'XXWC';

            IF l_cnt > 0
            THEN
               DBMS_OUTPUT.put_line ('TMS: 20161212-00028, Table exists');

               l_sql2 := 'SELECT COUNT(1) 
                    FROM XXWC.XXWC_MTI_QA_BKP';

               EXECUTE IMMEDIATE l_sql2 INTO l_cnt1;

               DBMS_OUTPUT.put_line (
                  'TMS: 20161212-00028  Count :' || l_cnt1);

               IF l_cnt1 > 0
               THEN
                  l_sql3 :=
                     'DELETE FROM mtl_transactions_interface where source_code =''ORDER ENTRY''';

                  EXECUTE IMMEDIATE l_sql3;

                  COMMIT;
               END IF;
            END IF;

            DBMS_OUTPUT.put_line (
               'TMS: 20161212-00028  After DELETE Count : ' || l_cnt1);
         END;
      END IF;
   END;
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20161212-00028 , Errors : ' || SQLERRM);
END;
/