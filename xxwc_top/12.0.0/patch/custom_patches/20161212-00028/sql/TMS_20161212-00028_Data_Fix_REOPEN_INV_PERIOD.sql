/*************************************************************************
  $Header TMS_20161212-00028_Data_Fix_REOPEN_INV_PERIOD.sql $
  Module Name: TMS_20161212-00028  Data Fix script 

  PURPOSE: Data Fix script 

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        12-DEC-2016  Ram Talluri        TMS#20161212-00028

**************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000;



DECLARE
   dummy   NUMBER;

   CURSOR c_branches
   IS
        SELECT acct_period_id,
               open_flag,
               period_name name,
               organization_id ,
               schedule_close_date,
               period_close_date
          FROM org_acct_periods
         WHERE     organization_id IN (SELECT organization_id
                                         FROM org_organization_definitions
                                        WHERE     operating_unit = 162
                                              AND NVL (disable_date,
                                                       SYSDATE + 1) >= SYSDATE)
               AND period_name = 'Dec-2016'
			   AND period_close_date IS NOT NULL
               --AND organization_id=228
      ORDER BY 1, 2;
BEGIN
   FOR i IN c_branches
   LOOP
      UPDATE org_acct_periods
         SET open_flag = 'Y', period_close_date = NULL, summarized_flag = 'N'
       WHERE     organization_id = i.organization_id
             AND acct_period_id = i.acct_period_id;

      DELETE mtl_period_summary
       WHERE     organization_id = i.organization_id
             AND acct_period_id = i.acct_period_id;

      DELETE mtl_period_cg_summary
       WHERE     organization_id = i.organization_id
             AND acct_period_id = i.acct_period_id;

      DELETE mtl_per_close_dtls
       WHERE     organization_id = i.organization_id
             AND acct_period_id = i.acct_period_id;

      DELETE cst_period_close_summary
       WHERE     organization_id = i.organization_id
             AND acct_period_id = i.acct_period_id;
   END LOOP;

   COMMIT;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      DBMS_OUTPUT.PUT_LINE ('Error occured: ' || SQLERRM);
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.PUT_LINE ('Error occured1: ' || SQLERRM);
END;
/