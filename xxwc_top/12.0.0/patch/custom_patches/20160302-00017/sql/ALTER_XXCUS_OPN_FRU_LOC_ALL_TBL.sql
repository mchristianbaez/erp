  /*  
  Module: HDS Real Estate [OPN -Oracle Property Manager]
  Date: 03/04/2016
  Ticket: TMS 20160302-00017 / ESMS 313314
  Description: -- alter table xxcus.xxcus_opn_fru_loc_all and xxcus.xxcus_opn_rer_locations_all
  Date        Author                 Notes
  =========== ====================   =================================
  03/04/2016  Balaguru Seshadri      TMS 20160302-00017  										   
  */
alter table xxcus.xxcus_opn_fru_loc_all add (hds_bldg_count number);
alter table xxcus.xxcus_opn_fru_loc_all add (hds_active_bldg_total_sf_prp number);
alter table xxcus.xxcus_opn_fru_loc_all add (hds_active_acres_prp number);
alter table xxcus.xxcus_opn_fru_loc_all add (hds_yard_count number);
alter table xxcus.xxcus_opn_fru_loc_all add (hds_pri_rer_type varchar2(30));
alter table xxcus.xxcus_opn_fru_loc_all add (hds_pri_rer_id varchar2(80));
alter table xxcus.xxcus_opn_fru_loc_all add (hds_pri_rer_expiry date);
alter table xxcus.xxcus_opn_fru_loc_all add (hds_active_bldg_sf_fru_prp number default 0);
alter table xxcus.xxcus_opn_fru_loc_all add (extract_date date);
alter table xxcus.xxcus_opn_rer_locations_all add (extract_date date);