CREATE OR REPLACE PACKAGE BODY xxwc_ahh_item_onhand_conv_pkg IS
  /***************************************************************************
  *    Script Name: Xxwc_Ahh_Item_Onhand_Conv_Pkg.Pkb
  *
  *    Interface / Conversion Name: Item On-Hand Conversion.
  *
  *    Functional Purpose: Convert On-Hand Qty Using Interface
  *
  *    History:
  *
  *    Version    Date              Author             Description
  ***************************************************************************
  *    1.0        14-Apr-2018       Naveen K           Initial development.
  ***************************************************************************/
  ---
  -- PROCEDURE: Prints Debug/Log Message - Checks for Debug Flag.
  ---
  PROCEDURE print_debug(p_print_str IN VARCHAR2) IS
  BEGIN
    --  IF g_debug = 'Y' THEN
    fnd_file.put_line(fnd_file.log
                     ,p_print_str);
    -- END IF;
    -- dbms_output.put_line(p_print_str);
  END print_debug;

  ---
  -- FUNCTION: Derive_Orcl_Org, Get Oracle Org Code from AHH Org Code.
  ---
  FUNCTION derive_orcl_org(ahh_org_code IN VARCHAR2) RETURN VARCHAR2 IS
    l_org_code mtl_parameters.organization_code%TYPE;
  BEGIN
    --
    SELECT br.oracle_branch_number
      INTO l_org_code
      FROM xxwc.xxwc_ap_branch_list_stg_tbl br
          ,mtl_parameters                   mp
     WHERE upper(br.ahh_warehouse_number) = upper(TRIM(ahh_org_code))
       AND mp.organization_code = br.oracle_branch_number
	   AND br.process_status = 'Y';
  
    --
    IF l_org_code IS NULL THEN
      RETURN NULL;
      --
    ELSE
      RETURN l_org_code;
    END IF;
  
  EXCEPTION
    WHEN OTHERS THEN
      RETURN NULL;
    
  END derive_orcl_org;

  ---
  -- PROCEDURE: Convert Varchar2 into Number function 
  ---
  FUNCTION convert_number(p_value IN VARCHAR2) RETURN NUMBER IS
    l_ret_value NUMBER;
  BEGIN
    SELECT to_number(TRIM(REPLACE(p_value
                                 ,chr(13)
                                 ,'')))
      INTO l_ret_value
      FROM dual;
    RETURN l_ret_value;
  EXCEPTION
    WHEN invalid_number THEN
      RETURN NULL;
    WHEN OTHERS THEN
      RETURN NULL;
  END convert_number;


  ---
  -- PROCEDURE: Calculate Oh-Hand Conversion factor for Transaction Cost and Qty 
  ---
  FUNCTION get_oh_conv_factor(p_oracle_item_number IN VARCHAR2
                             ,p_ahh_item_number    IN VARCHAR2) RETURN NUMBER IS
    l_item_type     VARCHAR2(100);
    l_int_item_type VARCHAR2(100);
    l_factor        NUMBER;
  BEGIN
  
    BEGIN
      SELECT convert_number(itmorg.ebs_value)
        INTO l_factor
        FROM xxwc.xxwc_org_item_ref itmorg
       WHERE itmorg.ahh_attribute = p_oracle_item_number
         AND itmorg.ahh_attr_value = p_ahh_item_number
         AND rownum < 2;
    
    EXCEPTION
      WHEN no_data_found THEN
        SELECT convert_number(itmorg.ebs_value)
          INTO l_factor
          FROM xxwc.xxwc_org_item_ref itmorg
         WHERE itmorg.ahh_attribute = p_oracle_item_number
           AND rownum < 2;
    END;
  
    IF l_factor IS NOT NULL THEN
      RETURN l_factor;
    ELSE
      RETURN 1;
    END IF;
  
  EXCEPTION
    WHEN OTHERS THEN
      RETURN 1;
  END get_oh_conv_factor;


  ---
  -- PROCEDURE: Convert Varchar2 into Number function 
  ---
  PROCEDURE update_org_item IS
    CURSOR c_get_all IS
      SELECT c.rowid                  rid
            ,c.source_code
            ,c.organization_id
            ,c.status
            ,c.batch_name
            ,c.subinventory_code
            ,c.attribute1             item_segment
            ,c.attribute2             organization_code
            ,c.attribute3
            ,c.attribute4
            ,c.attribute5
            ,c.item_segment           ahh_item_number
            ,c.attribute6
            ,c.organization_code      ahh_org_code
            ,c.transaction_quantity
            ,c.attribute7
            ,c.transaction_uom
            ,c.transaction_typename
            ,c.accts_code_combination
            ,c.error_message
            ,c.transaction_source
            ,c.transaction_type
            ,c.locator_name
            ,c.transaction_reference
            ,c.primary_quantity
            ,c.lot_expiration_date
            ,c.origination_date
            ,c.lot_number
            ,c.fm_serial_number
            ,c.to_serial_number
            ,c.attribute8
            ,c.transaction_cost
            ,c.transaction_date
            ,c.how_priced
        FROM xxwc_onhand_bal_cnv c
       WHERE status = 'N';
  
    lorg        VARCHAR2(10);
    lpartnumber VARCHAR2(30);
    l_err_msg   VARCHAR2(500);
    l_err_flag  VARCHAR2(1);
    l_sec       VARCHAR2(10);
  
  BEGIN
    /*    UPDATE xxwc_onhand_bal_cnv
      SET status = 'N'
    WHERE item_segment = '00009'
    ;*/
    print_debug(' ****** : Update_Org_Item: Start  ****** ');
    l_sec := '10';
  
    FOR rec IN c_get_all LOOP
      BEGIN
        print_debug(rpad(' ****** '
                        ,100
                        ,'-'));
        print_debug(rpad(' ****** Record : Item | Org | TxnQty : '
                        ,45
                        ,' ') || rec.ahh_item_number || '|' || rec.ahh_org_code || '|' ||
                    rec.transaction_quantity);
      
        l_err_msg   := NULL;
        lpartnumber := NULL;
        l_err_flag  := 'N';
      
        l_sec := '20';
        -- Update Oracle Part Number
        BEGIN
        
          l_sec := '30';
          SELECT TRIM(item)
            INTO lpartnumber
            FROM xxwc.xxwc_item_master_ref
           WHERE upper(icsp_prod) = upper(rec.ahh_item_number)
          --AND rownum < 2
          ;
        
          IF lpartnumber IS NOT NULL THEN
            l_sec := '40';
            UPDATE xxwc_onhand_bal_cnv c
               SET attribute1        = lpartnumber
                  ,status            = 'R'
                  ,c.error_message   = 'Item Ref Found'
                  ,subinventory_code = decode(substr(lpartnumber
                                                    ,1
                                                    ,1)
                                             ,'R'
                                             ,'Rental'
                                             ,subinventory_code)
             WHERE ROWID = rec.rid;
          END IF;
          l_err_flag := 'N';
        
        EXCEPTION
          WHEN no_data_found THEN
            l_sec     := '50';
            l_err_msg := 'Item Reference Not Found';
            UPDATE xxwc_onhand_bal_cnv c
               SET status          = 'E'
                  ,c.error_message = l_err_msg
             WHERE ROWID = rec.rid;
            l_err_flag := 'Y';
          WHEN too_many_rows THEN
            l_sec     := '60';
            l_err_msg := 'Item Reference-Multiple records';
            UPDATE xxwc_onhand_bal_cnv c
               SET status          = 'E'
                  ,c.error_message = l_err_msg
             WHERE ROWID = rec.rid;
            l_err_flag := 'Y';
          WHEN OTHERS THEN
            l_sec     := '70';
            l_err_msg := 'UnknownItemRefError- ' || SQLERRM;
            UPDATE xxwc_onhand_bal_cnv c
               SET status          = 'E'
                  ,c.error_message = l_err_msg
             WHERE ROWID = rec.rid;
            l_err_flag := 'Y';
        END;
      
        l_sec := '80';
        print_debug(' ****** : ' || nvl(l_err_msg
                                       ,'Item Ref Successful: '||lpartnumber));
      
      
        IF l_err_flag = 'N' THEN
          BEGIN
            l_sec := '90';
            lorg  := derive_orcl_org(rec.ahh_org_code);
          
            IF lorg IS NOT NULL THEN
              l_sec := '100';
              UPDATE xxwc_onhand_bal_cnv c
                 SET attribute2      = lorg
                    ,status          = 'R'
                    ,c.error_message = error_message || '/' || 'Org Ref Found'
               WHERE ROWID = rec.rid;
            ELSE
              l_sec := '110';
              UPDATE xxwc_onhand_bal_cnv c
                 SET status          = 'E'
                    ,c.error_message = error_message || '/' || 'Org Reference Not Found'
               WHERE ROWID = rec.rid;
            END IF;
          EXCEPTION
            WHEN OTHERS THEN
              l_sec     := '120';
              l_err_msg := 'UnknownOrgRefError- ' || SQLERRM;
              UPDATE xxwc_onhand_bal_cnv c
                 SET status          = 'E'
                    ,c.error_message = l_err_msg
               WHERE ROWID = rec.rid;
          END;
          print_debug(' ****** : ' || nvl(l_err_msg
                                         ,'Org Ref Successful: '||lorg));
        ELSE
          l_sec := '130';
          print_debug(' ****** : Skipping Org Ref Items ');
        END IF;
        l_sec := '140';
      
      EXCEPTION
        WHEN OTHERS THEN
          print_debug(' ****** : Unknown Error in Loop Update_Org_Items: ' || l_sec || '-' ||
                      substr(SQLERRM
                            ,1
                            ,100));
        
      
      END;
    END LOOP;
    l_sec := '150';
    print_debug(rpad(' ****** '
                    ,100
                    ,'-'));
    print_debug(' ****** : Update_Org_Item: End  ****** ');
    print_debug('  ');
  
  EXCEPTION
    WHEN OTHERS THEN
      print_debug(' ****** : Unknown error in Update Org Items ' || SQLERRM);
      print_debug(' ****** : Update_Org_Item: ABORT  ****** ');
      print_debug('  ');
  END update_org_item;
  --

  ---
  -- PROCEDURE: Validations 
  ---
  PROCEDURE validations IS
  
    CURSOR c_onhadbal_stg IS
      SELECT ROWID                       rid
            ,xobc.source_code
            ,xobc.organization_id
            ,xobc.status
            ,xobc.batch_name
            ,xobc.subinventory_code
            ,xobc.attribute1             item_segment
            ,xobc.attribute2             organization_code
            ,xobc.attribute3
            ,xobc.attribute4
            ,xobc.attribute5
            ,xobc.item_segment           ahh_item_number
            ,xobc.attribute6
            ,xobc.organization_code      ahh_org_code
            ,xobc.transaction_quantity
            ,xobc.attribute7
            ,xobc.transaction_uom
            ,xobc.transaction_typename
            ,xobc.accts_code_combination
            ,xobc.error_message
            ,xobc.transaction_source
            ,xobc.transaction_type
            ,xobc.locator_name
            ,xobc.transaction_reference
            ,xobc.primary_quantity
            ,xobc.lot_expiration_date
            ,xobc.origination_date
            ,xobc.lot_number
            ,xobc.fm_serial_number
            ,xobc.to_serial_number
            ,xobc.attribute8
            ,xobc.transaction_cost
            ,xobc.transaction_date
            ,xobc.how_priced
        FROM xxwc_onhand_bal_cnv xobc
       WHERE status = 'R'; --onhand qty
  
    l_errmsg                 VARCHAR2(4000);
    l_item_error             VARCHAR2(2) := 'N';
    l_item_err_details       VARCHAR2(3000);
    l_transaction_type_id    NUMBER;
    l_organization_id        NUMBER;
    l_inventory_item_id      NUMBER;
    l_uom                    NUMBER;
    l_uom_code               VARCHAR2(5);
    l_count                  NUMBER := 0;
    l_srl_num_control        NUMBER;
    l_lotcontrol             NUMBER;
    l_trans_action_id        NUMBER;
    l_trans_source_type_id   NUMBER;
    l_accts_pay_code_comb_id VARCHAR2(30);
    l_processing_stg         NUMBER := 0;
    l_item_cnt               NUMBER := 0;
    l_error_stg              NUMBER := 0;
    l_validated_stg          NUMBER := 0;
    l_exist                  VARCHAR2(1);
  
  BEGIN
    print_debug(' ****** Validations: Start  ****** ');
    --
    --
    update_org_item();
    --
    --
    print_debug(' ****** : Rest Of Validations: Start  ****** ');
    print_debug(rpad(' ****** '
                    ,100
                    ,'-'));
  
    SELECT COUNT(1)
      INTO l_processing_stg
      FROM xxwc_onhand_bal_cnv xobc
     WHERE status IN ('R'
                     ,'E');
    print_debug(' ****** : Total recs gathered');
  
    UPDATE xxwc_onhand_bal_cnv a
       SET transaction_uom =
           (SELECT b.primary_uom_code
              FROM mtl_system_items b
                  ,mtl_parameters   c
             WHERE a.attribute1 = b.segment1
               AND b.organization_id = c.organization_id
               AND a.attribute2 = c.organization_code)
     WHERE transaction_uom IS NULL;
    print_debug(' ****** : Transactoin UOM defaulted, ' || SQL%ROWCOUNT);
  
  
    UPDATE xxwc_onhand_bal_cnv
       SET transaction_uom = decode(transaction_uom
                                   ,'PAR'
                                   ,'PR'
                                   ,'ROL'
                                   ,'RL'
                                   ,'PK'
                                   ,'PKG'
                                   ,'LB'
                                   ,'LBS'
                                   ,'PAL'
                                   ,'PL'
                                   ,'CAS'
                                   ,'CS'
                                   ,transaction_uom)
     WHERE transaction_uom IS NOT NULL;
    print_debug(' ****** : Transactoin UOM translated, ' || SQL%ROWCOUNT);
    --
    COMMIT;
    --
    print_debug(' ****** : Loop Start -------------------');
    FOR r_onhadbal_stg IN c_onhadbal_stg LOOP
    
    
      print_debug(rpad(' ****** : '
                      ,100
                      ,'-'));
    
      l_errmsg                            := NULL;
      l_item_error                        := NULL;
      l_organization_id                   := NULL;
      l_inventory_item_id                 := NULL;
      r_onhadbal_stg.transaction_quantity := nvl(xxwc_ahh_item_onhand_conv_pkg.convert_number(r_onhadbal_stg.transaction_quantity)
                                                ,0);
      r_onhadbal_stg.transaction_cost     := round(convert_number(r_onhadbal_stg.transaction_cost)
                                                  ,5);
      print_debug(' ****** : ' || 'OrclItem, Org, TrxQty, TrxCost: '||r_onhadbal_stg.item_segment||', '||r_onhadbal_stg.organization_code||', '||r_onhadbal_stg.transaction_quantity||', '||r_onhadbal_stg.transaction_cost);
      --Organization Validation
      IF r_onhadbal_stg.organization_code IS NOT NULL THEN
        BEGIN
          SELECT organization_id
            INTO l_organization_id
            FROM mtl_parameters
           WHERE organization_code = r_onhadbal_stg.organization_code;
        
          print_debug(' ****** : ' || 'Org Code Successful');
        EXCEPTION
          WHEN no_data_found THEN
            l_item_error       := 'Y';
            l_errmsg           := l_errmsg || '\' || 'Invalid Organization Code';
            l_item_err_details := '\' || ' ' || ' Organization Code is not valid';
            print_debug(' ****** : ' || l_item_err_details);
          WHEN OTHERS THEN
            l_item_error       := 'Y';
            l_errmsg           := l_errmsg || '\' || SQLCODE || '  ' || SQLERRM;
            l_item_err_details := '\' || ' ' || '-' || SQLCODE || '  ' || SQLERRM;
            print_debug(' ****** : ' || l_item_err_details);
        END;
      ELSE
        l_item_error       := 'Y';
        l_errmsg           := l_errmsg || '\' || 'Organization Code is NULL';
        l_item_err_details := '\' || ' ' || 'Organization Code cannot be Null';
        print_debug(' ****** : ' || l_item_err_details);
      END IF;
    
      --Item Validation
      IF r_onhadbal_stg.item_segment IS NOT NULL THEN
        BEGIN
          SELECT inventory_item_id
                ,primary_uom_code --count(1)
            INTO l_inventory_item_id
                ,l_uom_code
            FROM mtl_system_items_b
           WHERE upper(segment1) = upper(r_onhadbal_stg.item_segment)
             AND organization_id = l_organization_id
                --l_organization_id
             AND enabled_flag = 'Y'
             AND SYSDATE BETWEEN nvl(start_date_active
                                    ,SYSDATE) AND nvl(end_date_active
                                                     ,SYSDATE);
          print_debug(' ****** : ' || 'Inventory Item Id Successful');
        EXCEPTION
          WHEN no_data_found THEN
            l_item_error       := 'Y';
            l_errmsg           := l_errmsg || '\' || 'Inventory Item Id doesnt exist';
            l_item_err_details := '\' || ' ' || 'Inventory Item Id cannot be Null';
            print_debug(' ****** : ' || l_item_err_details);
          WHEN OTHERS THEN
            l_item_error       := 'Y';
            l_errmsg           := l_errmsg || '\' || SQLCODE || '  ' || SQLERRM;
            l_item_err_details := '\' || ' ' || '-' || SQLCODE || '  ' || SQLERRM;
            print_debug(' ****** : ' || l_item_err_details);
        END;
      ELSE
        l_item_error       := 'Y';
        l_errmsg           := l_errmsg || '\' || 'Inventory Item Id is NULL';
        l_item_err_details := '\' || ' ' || 'Inventory Item Id cannot be Null';
        print_debug(' ****** : ' || l_item_err_details);
      END IF;
    
      --UOM Validation
      IF r_onhadbal_stg.transaction_uom IS NOT NULL THEN
        BEGIN
          SELECT COUNT(1)
            INTO l_uom
            FROM mtl_units_of_measure
           WHERE upper(uom_code) = upper(r_onhadbal_stg.transaction_uom);
          print_debug(' ****** : ' || 'Item UOM Successful');
        EXCEPTION
          WHEN no_data_found THEN
            l_item_error       := 'Y';
            l_errmsg           := l_errmsg || '\' || 'Item UOM doesnt exist';
            l_item_err_details := '\' || ' ' || 'Item  UOM doesnt exist';
            print_debug(' ****** : ' || l_item_err_details);
          WHEN OTHERS THEN
            l_item_error       := 'Y';
            l_errmsg           := l_errmsg || '\' || SQLCODE || '  ' || SQLERRM;
            l_item_err_details := '\' || ' ' || '-' || SQLCODE || '  ' || SQLERRM;
            print_debug(' ****** : ' || l_item_err_details);
        END;
      ELSE
        l_item_error       := 'Y';
        l_errmsg           := l_errmsg || '\' || 'Item UOM is NULL';
        l_item_err_details := '\' || ' ' || 'Item UOM is NULL';
        print_debug(' ****** : ' || l_item_err_details);
      END IF;
    
      l_lotcontrol      := 0;
      l_srl_num_control := 0;
      IF l_inventory_item_id IS NOT NULL THEN
        SELECT lot_control_code
              ,serial_number_control_code
          INTO l_lotcontrol
              ,l_srl_num_control
          FROM mtl_system_items_b msb
         WHERE inventory_item_id = l_inventory_item_id
           AND organization_id = l_organization_id;
      END IF;
    
      IF l_lotcontrol = 2 THEN
        BEGIN
          SELECT 'x'
            INTO l_exist
            FROM xxwc_onhand_lot_cnv
           WHERE organization_code = r_onhadbal_stg.organization_code
             AND item_segment = r_onhadbal_stg.item_segment
             AND rownum = 1;
        EXCEPTION
          WHEN OTHERS THEN
            IF r_onhadbal_stg.transaction_quantity != 0 THEN
              -- If Onhand Qty != 0 for a lot controlled item, and the item does not exist in Lot File then report error. -- Gopi Damuluri
              l_item_error := 'Y';
              l_errmsg     := l_errmsg || '\' || 'Lot Information do not exist';
            END IF;
        END;
      END IF;
    
      --Transaction Qty Validation
      IF r_onhadbal_stg.transaction_quantity >= 0 THEN
        print_debug(' ****** : ' || 'Transaction Qty Successful');
      ELSE
        l_item_error       := 'Y';
        l_errmsg           := l_errmsg || '\' || 'TRANSACTION QUANTITY Does Not Exist';
        l_item_err_details := '\' || ' ' || 'TRANSACTION QUANTITY Does Not Exist';
        print_debug(' ****** : ' || l_item_err_details);
      END IF;
    
    
      --Transaction Cost for Asset Subinventory Validation
      IF upper(r_onhadbal_stg.subinventory_code) = upper('Asset Subinventory') THEN
        IF r_onhadbal_stg.transaction_cost > 0 THEN
          print_debug(' ****** : ' || 'Transaction Cost Successful');
        ELSE
          l_item_error       := 'Y';
          l_errmsg           := l_errmsg || '\' || 'Transaction Cost cannot be NULL';
          l_item_err_details := '\' || ' ' || 'Transaction Cost cannot be Null';
          print_debug(' ****** : ' || l_item_err_details);
        END IF;
      END IF;
    
      print_debug(' ****** : ' || 'Updating Staging Tables...');
    
      --Processing Error Information
      IF nvl(l_item_error
            ,'N') = 'Y' THEN
        -- Update Item Staging Table with error flag and error message
        UPDATE xxwc_onhand_bal_cnv
           SET status        = 'E'
              ,error_message = substr(l_errmsg
                                     ,1
                                     ,2000)
         WHERE ROWID = r_onhadbal_stg.rid;
      ELSE
        UPDATE xxwc_onhand_bal_cnv
           SET status = 'V'
         WHERE ROWID = r_onhadbal_stg.rid;
      END IF;
      print_debug(' ****** : ' || '*Record, Done*');
      COMMIT;
    END LOOP;
    print_debug(' ****** : Loop END -------------------');
    print_debug(' ****** : ' || 'Number of Records Processed: ' || l_processing_stg);
  
    SELECT COUNT(1)
      INTO l_error_stg
      FROM xxwc_onhand_bal_cnv xobc
     WHERE status = 'E';
  
    print_debug(' ****** : ' || 'Number of Records Errored out : ' || l_error_stg);
  
    SELECT COUNT(1)
      INTO l_validated_stg
      FROM xxwc_onhand_bal_cnv xobc
     WHERE status = 'V';
  
    print_debug(' ****** : ' || 'Number of Records Validated ' || l_validated_stg);
    --display the processed information
  
    print_debug(' ****** Validations: Complete  ****** ');
  END validations;
  --


  ---
  -- PROCEDURE: Process Onhand 
  ---
  PROCEDURE process_onhand IS
    CURSOR c_validrec_stg IS
      SELECT ROWID
            ,organization_id
            ,xxonhand.transaction_quantity  transaction_quantity
            ,transaction_uom
            ,transaction_cost
            ,xxonhand.status                status
            ,attribute2                     organization_code
            ,attribute1                     item_segment
            ,item_segment                   ahh_item_number
            ,organization_code              ahh_org_code
            ,subinventory_code
            ,transaction_typename
            ,accts_code_combination
            ,error_message
            ,xxonhand.source_code           transaction_source
            ,transaction_type
            ,locator_name
            ,lot_number
            ,to_serial_number
            ,fm_serial_number
            ,lot_expiration_date
            ,transaction_date
            ,xxonhand.transaction_reference transaction_reference
            ,primary_quantity
            ,origination_date
            ,how_priced
        FROM xxwc_onhand_bal_cnv xxonhand
       WHERE xxonhand.status = 'V'
       ORDER BY organization_code
               ,item_segment
               ,nvl(transaction_date
                   ,SYSDATE);
  
    l_transaction_type_id    NUMBER;
    l_0_transaction_type_id  NUMBER;
    l_organization_id        NUMBER;
    l_inventory_item_id      NUMBER;
    l_lotcontrol             NUMBER;
    l_srl_num_control        NUMBER;
    l_trans_action_id        NUMBER;
    l_trans_source_type_id   NUMBER;
    l_flow_schedule          VARCHAR2(1) := 'Y';
    l_scheduled_flag         NUMBER := 2;
    l_transaction_mode       NUMBER := 3;
    l_process_flag           NUMBER := 1;
    l_lock_mode              NUMBER := 2;
    l_accts_pay_code_comb_id VARCHAR2(30);
    l_interfaceprocess_count NUMBER := 0;
    l_interfaceerror_count   NUMBER := 0;
    l_interfacerec_count     NUMBER := 0;
    l_errormsg               VARCHAR2(1000);
    l_organization_code      VARCHAR2(10);
    l_transaction_source_id  NUMBER;
    l_count                  NUMBER;
    l_total_qty              NUMBER;
    l_bal_qty                NUMBER;
    l_trx_qty                NUMBER;
    l_mat_account            NUMBER;
    l_transaction_cost       NUMBER := 0;
    l_cost_group_id          NUMBER;
    l_lot_number             VARCHAR2(80); -- Added by Gopi Damuluri
    l_exist                  VARCHAR2(1); -- Added by Gopi Damuluri
  
  BEGIN
    --    --Deleting all the interface information
    --    delete from MTL_TRANSACTIONS_INTERFACE;
    --    delete from MTL_TRANSACTION_LOTS_INTERFACE;
    --    delete from MTL_SERIAL_NUMBERS_INTERFACE;
    --    commit;
    SELECT COUNT(1)
      INTO l_interfaceprocess_count
      FROM xxwc_onhand_bal_cnv xxobc
     WHERE xxobc.status = 'V';
  
    print_debug(' Before Insertion Process.....Loop Start!');
  
    FOR r_validrec_stg IN c_validrec_stg LOOP
      print_debug(rpad(' -'
                      ,100
                      ,'-'));
      print_debug(rpad(' Item,OrgCode | AHHItem,Org : '
                      ,30
                      ,' ') || r_validrec_stg.item_segment || ', ' ||
                  r_validrec_stg.organization_code || ' | ' || r_validrec_stg.ahh_item_number || ',' ||
                  r_validrec_stg.ahh_org_code);
    
      --      l_errormsg                          := NULL;
      r_validrec_stg.transaction_source   := 'CONVERSION';
      l_mat_account                       := NULL;
      l_cost_group_id                     := NULL;
      l_transaction_cost                  := round(get_oh_conv_factor(r_validrec_stg.item_segment
                                                                     ,r_validrec_stg.ahh_item_number) *
                                                   convert_number(r_validrec_stg.transaction_cost)
                                                  ,5);
      r_validrec_stg.transaction_quantity := round(nvl(xxwc_ahh_item_onhand_conv_pkg.convert_number(r_validrec_stg.transaction_quantity)
                                                      ,0) /
                                                   get_oh_conv_factor(r_validrec_stg.item_segment
                                                                     ,r_validrec_stg.ahh_item_number));
      print_debug(rpad(' > Trx Qty, Trx Cost : '
                      ,30
                      ,' ') || r_validrec_stg.transaction_quantity || ', ' || l_transaction_cost);
      /*
                  BEGIN
                  SELECT transaction_type_id
                    INTO l_0_transaction_type_id
                    FROM mtl_transaction_types
                   WHERE description = 'Update average cost information';
                  WHEN OTHERS THEN
                    NULL;
                  END;
      */
    
      IF nvl(r_validrec_stg.transaction_quantity
            ,0) = 0 THEN
        l_transaction_type_id := 80; -- For Zero Onhand Quantity
      
        SELECT material_account
              ,default_cost_group_id
          INTO l_mat_account
              ,l_cost_group_id
          FROM mtl_parameters
         WHERE organization_code = r_validrec_stg.organization_code;
      
      ELSE
        l_transaction_type_id := 41;
      END IF;
    
      print_debug(rpad(' > Trx Type : '
                      ,30
                      ,' ') || r_validrec_stg.transaction_quantity || ', ' || l_transaction_cost);
    
    
      --getting organization id
      SELECT organization_id
        INTO l_organization_id
        FROM org_organization_definitions
       WHERE organization_code = r_validrec_stg.organization_code
         AND nvl(disable_date
                ,trunc(SYSDATE)) >= trunc(SYSDATE);
    
      print_debug(rpad(' > Org ID : '
                      ,30
                      ,' ') || l_organization_id);
    
      --getting transaction source and transaction id
      SELECT transaction_source_type_id
            ,transaction_action_id
        INTO l_trans_source_type_id
            ,l_trans_action_id
        FROM mtl_transaction_types
       WHERE transaction_type_id = l_transaction_type_id;
      print_debug(rpad(' > Transaction SourceTypeID, ActionID : '
                      ,30
                      ,' ') || l_trans_source_type_id || ', ' || l_trans_action_id);
    
      --getting inventory item id
      SELECT inventory_item_id
        INTO l_inventory_item_id
        FROM mtl_system_items_b
       WHERE segment1 = r_validrec_stg.item_segment
         AND organization_id = l_organization_id
         AND enabled_flag = 'Y'
         AND SYSDATE BETWEEN nvl(start_date_active
                                ,SYSDATE) AND nvl(end_date_active
                                                 ,SYSDATE);
    
      print_debug(rpad(' > Item Id : '
                      ,30
                      ,' ') || l_inventory_item_id);
    
      SELECT disposition_id
            ,distribution_account
        INTO l_transaction_source_id
            ,l_accts_pay_code_comb_id
        FROM mtl_generic_dispositions mgd
       WHERE organization_id = l_organization_id -- (SELECT organization_id
            -- FROM org_organization_definitions
            -- WHERE organization_code =
            -- r_validrec_stg.organization_code)
         AND segment1 = 'CONVERSION'
         AND nvl(mgd.disable_date
                ,SYSDATE) >= trunc(SYSDATE)
         AND nvl(mgd.effective_date
                ,trunc(SYSDATE)) <= SYSDATE;
    
      print_debug(rpad(' > Transaction Source ID,Acct: '
                      ,30
                      ,' ') || l_transaction_source_id || ', ' || l_accts_pay_code_comb_id);
    
      print_debug(' > Inserting into mtl_transactions_interface table...');
    
      IF nvl(r_validrec_stg.transaction_quantity
            ,0) = 0 THEN
        INSERT INTO mtl_transactions_interface
          (source_code
          ,process_flag
          ,transaction_mode
          ,transaction_uom
          ,transaction_type_id
          ,flow_schedule
          ,scheduled_flag
          ,created_by
          ,last_updated_by
          ,organization_id
          ,subinventory_code
          ,transaction_date
          ,new_average_cost
          ,transaction_reference
          ,inventory_item_id
          ,source_header_id
          ,source_line_id
          ,transaction_quantity
          ,transaction_source_id
          ,distribution_account_id
          ,locator_name
          ,lock_flag
          ,transaction_action_id
          ,transaction_source_type_id
          ,creation_date
          ,last_update_date
          ,transaction_interface_id
          ,transaction_header_id
          ,material_account
          ,cost_group_id
          ,attribute15 --born on date
           )
        VALUES
          (r_validrec_stg.transaction_source
          ,l_process_flag
          ,l_transaction_mode
          ,r_validrec_stg.transaction_uom
          ,l_transaction_type_id
          ,l_flow_schedule
          ,l_scheduled_flag
          ,fnd_profile.value('USER_ID')
          , --,0
           fnd_profile.value('USER_ID')
          , --,-1
           l_organization_id
          ,nvl(r_validrec_stg.subinventory_code
              ,'General')
          ,SYSDATE
          , --add_months(last_day(SYSDATE), -1)-7,
           decode(r_validrec_stg.how_priced
                 ,'E'
                 ,l_transaction_cost
                 ,'C'
                 ,l_transaction_cost / 100
                 ,'M'
                 ,l_transaction_cost / 1000
                 ,l_transaction_cost)
          , -- new_average_cost
           r_validrec_stg.transaction_reference
          ,l_inventory_item_id
          ,99
          , --p_source_header_id
           98
          , --p_source_line_id
           nvl(r_validrec_stg.transaction_quantity
              ,0)
          ,l_transaction_source_id
          ,l_accts_pay_code_comb_id
          ,nvl(r_validrec_stg.locator_name
              ,'')
          ,l_lock_mode
          , --add
           l_trans_action_id
          ,l_trans_source_type_id
          ,SYSDATE
          ,SYSDATE
          ,mtl_material_transactions_s.nextval
          , --l_txn_int_id -- transaction_interface_id
           to_char(r_validrec_stg.organization_id) ||
           to_char(SYSDATE
                  ,'ddmmyyyy')
          ,l_mat_account
          ,l_cost_group_id
          ,r_validrec_stg.transaction_date);
      ELSE
        INSERT INTO mtl_transactions_interface
          (source_code
          ,process_flag
          ,transaction_mode
          ,transaction_uom
          ,transaction_type_id
          ,flow_schedule
          ,scheduled_flag
          ,created_by
          ,last_updated_by
          ,organization_id
          ,subinventory_code
          ,transaction_date
          ,transaction_cost
          ,transaction_reference
          ,inventory_item_id
          ,source_header_id
          ,source_line_id
          ,transaction_quantity
          ,transaction_source_id
          ,distribution_account_id
          ,locator_name
          ,lock_flag
          ,transaction_action_id
          ,transaction_source_type_id
          ,creation_date
          ,last_update_date
          ,transaction_interface_id
          ,transaction_header_id
          ,attribute15 --born on date
           )
        VALUES
          (r_validrec_stg.transaction_source
          ,l_process_flag
          ,l_transaction_mode
          ,r_validrec_stg.transaction_uom
          ,l_transaction_type_id
          ,l_flow_schedule
          ,l_scheduled_flag
          ,fnd_profile.value('USER_ID')
          , --,0
           fnd_profile.value('USER_ID')
          , --,-1
           l_organization_id
          ,nvl(r_validrec_stg.subinventory_code
              ,'General')
          ,SYSDATE
          , --add_months(last_day(SYSDATE), -1)-7,
           decode(r_validrec_stg.how_priced
                 ,'E'
                 ,l_transaction_cost
                 ,'C'
                 ,l_transaction_cost / 100
                 ,'M'
                 ,l_transaction_cost / 1000
                 ,l_transaction_cost)
          , -- transaction_cost
           r_validrec_stg.transaction_reference
          ,l_inventory_item_id
          ,99
          , --p_source_header_id
           98
          , --p_source_line_id
           nvl(r_validrec_stg.transaction_quantity
              ,0)
          ,l_transaction_source_id
          ,l_accts_pay_code_comb_id
          ,nvl(r_validrec_stg.locator_name
              ,'')
          ,l_lock_mode
          , --add
           l_trans_action_id
          ,l_trans_source_type_id
          ,SYSDATE
          ,SYSDATE
          ,mtl_material_transactions_s.nextval
          , --l_txn_int_id -- transaction_interface_id
           to_char(r_validrec_stg.organization_id) ||
           to_char(SYSDATE
                  ,'ddmmyyyy')
          ,r_validrec_stg.transaction_date);
        print_debug(' > transaction qty NULL insertion...');
      END IF;
    
      print_debug(' > Lot Or Serial Control Verification');
      print_debug(' > ' || l_inventory_item_id || l_organization_id);
    
      SELECT lot_control_code
            ,serial_number_control_code
        INTO l_lotcontrol
            ,l_srl_num_control
        FROM mtl_system_items_b msb
       WHERE inventory_item_id = l_inventory_item_id
         AND organization_id = l_organization_id;
    
      print_debug(' > lotcontrol,serialnumber: ' || l_lotcontrol || ', ' || l_srl_num_control);
    
      IF l_lotcontrol = 2 THEN
        -- for full control if 1 no control     --need to derive
        print_debug(' > ...Inserting into mtl_transaction_lots_interface table');
        l_total_qty := 0;
        l_trx_qty   := 0;
        l_bal_qty   := 0;
      
        -- Check if the Item exists in the LotControl File
        -- Added by Gopi Damuluri > Start
        BEGIN
          SELECT 'x'
            INTO l_exist
            FROM xxwc_onhand_lot_cnv
           WHERE organization_code = r_validrec_stg.organization_code
             AND item_segment = r_validrec_stg.item_segment
             AND rownum = 1;
        EXCEPTION
          WHEN OTHERS THEN
            IF r_validrec_stg.transaction_quantity = 0 THEN
              -- Use Default Lot - LOT0 for Zero OnHand Quantity Lot Transactions
              INSERT INTO mtl_transaction_lots_interface
                (transaction_interface_id
                ,lot_number
                ,transaction_quantity
                ,last_update_date
                ,last_updated_by
                ,creation_date
                ,created_by
                ,
                 --product_code,
                 last_update_login
                ,product_transaction_id
                ,
                 --primary_quantity,
                 lot_expiration_date)
              VALUES
                (mtl_material_transactions_s.currval
                , --transaction_interface_id
                 'LOT0'
                , --r_validrec_stg.lot_number -- lot_number
                 0
                , --lot_rec.transaction_quantity,
                 SYSDATE
                ,0
                , --userid
                 SYSDATE
                ,-1
                , --userid
                 0
                , --userid
                 mtl_material_transactions_s.currval
                , -- product_transaction_id
                 SYSDATE + 1);
            END IF;
        END;
        -- Added by Gopi Damuluri < End
      
        FOR lot_rec IN (SELECT a.rowid
                              ,a.transaction_quantity
                              ,a.lot_expiration_date
                              ,a.balance_quantity
                              ,a.lot_number -- Gopi Damuluri
                          FROM xxwc_onhand_lot_cnv a
                         WHERE a.organization_code = r_validrec_stg.organization_code
                           AND a.item_segment = r_validrec_stg.item_segment
                           AND a.status IS NULL
                         ORDER BY a.lot_expiration_date ASC) LOOP
          IF l_total_qty + lot_rec.balance_quantity > r_validrec_stg.transaction_quantity THEN
            l_trx_qty := r_validrec_stg.transaction_quantity - l_total_qty;
            l_bal_qty := lot_rec.balance_quantity - l_trx_qty;
          ELSE
            l_trx_qty := lot_rec.balance_quantity;
            l_bal_qty := 0;
          END IF;
          l_total_qty := l_total_qty + l_trx_qty;
          --l_total_qty := l_total_qty + lot_rec.transaction_quantity;
          l_lot_number := 'L' || nvl(lot_rec.lot_number
                                    ,xxwc_onhand_lot_s.nextval); -- Use PRISM lot_number provided in the file.If no value is provided then use the default sequence. -- Gopi Damuluri
        
          INSERT INTO mtl_transaction_lots_interface
            (transaction_interface_id
            ,lot_number
            ,transaction_quantity
            ,last_update_date
            ,last_updated_by
            ,creation_date
            ,created_by
            ,
             --product_code,
             last_update_login
            ,product_transaction_id
            ,
             --primary_quantity,
             lot_expiration_date)
          VALUES
            (mtl_material_transactions_s.currval
            , --transaction_interface_id
             'L' || nvl(lot_rec.lot_number
                       ,xxwc_onhand_lot_s.nextval)
            , --r_validrec_stg.lot_number -- lot_number
             l_trx_qty
            , --lot_rec.transaction_quantity,
             SYSDATE
            ,0
            , --userid
             SYSDATE
            ,-1
            , --userid
             0
            , --userid
             mtl_material_transactions_s.currval
            , -- transaction_interface_id
             --r_validrec_stg.primary_quantity,
             lot_rec.lot_expiration_date);
        
          IF l_bal_qty = 0 THEN
            UPDATE xxwc_onhand_lot_cnv
               SET status           = 'P'
                  ,balance_quantity = 0
             WHERE ROWID = lot_rec.rowid;
          ELSE
            UPDATE xxwc_onhand_lot_cnv
               SET balance_quantity = l_bal_qty
             WHERE ROWID = lot_rec.rowid;
          END IF;
          IF l_total_qty = r_validrec_stg.transaction_quantity THEN
            EXIT;
          END IF;
        END LOOP;
      ELSE
        NULL;
      END IF;
    
      print_debug(' > Lot Or Serial Control Verification : DONE');
    
      print_debug(' > Updating Staging Table...');
      -- Update Item Staging Table with process status
      IF l_errormsg IS NOT NULL THEN
        --ROLLBACK;
        --print_debug ('Updating the Staging Table with status E');
        UPDATE xxwc_onhand_bal_cnv
           SET status        = 'E'
              ,error_message = l_errormsg
         WHERE ROWID = r_validrec_stg.rowid;
      
        --COMMIT;
      ELSE
        -- print_debug ('Updating the Staging Table with status P');
        UPDATE xxwc_onhand_bal_cnv
           SET status = 'P'
         WHERE ROWID = r_validrec_stg.rowid;
      
        --COMMIT;
      END IF;
    
      COMMIT;
      print_debug(' > *End*');
      --- FND_FILE.PUT_LINE(FND_FILE.OUTPUT, 'One record inserted into interface table -> mtl_transactions_interface');
    END LOOP;
    print_debug(rpad('-'
                    ,100
                    ,'-'));
  
    print_debug(' Insertion Process.....Loop Complete!');
  
    print_debug(rpad(' -'
                    ,100
                    ,'-'));
  
    print_debug('');
  
    --processing information
    print_debug(' Number Of Records being Processed: ' || l_interfaceprocess_count);
  
    SELECT COUNT(1)
      INTO l_interfaceerror_count
      FROM xxwc_onhand_bal_cnv
     WHERE status = 'E';
  
    print_debug(' Number Of Records with Error in Interface Table: ' || l_interfaceerror_count);
  
    SELECT COUNT(1)
      INTO l_interfacerec_count
      FROM xxwc_onhand_bal_cnv
     WHERE status = 'P';
  
    print_debug(' Number Of Valid Records in Interface Table: ' || l_interfacerec_count);
    print_debug(rpad(' -'
                    ,100
                    ,'-'));
  
  
  EXCEPTION
    WHEN OTHERS THEN
      print_debug(SQLCODE || '  ' || SQLERRM);
      print_debug('Exception Part....!');
      --processing information
      print_debug('Number Of Records being Processed: ' || l_interfaceprocess_count);
    
      SELECT COUNT(1)
        INTO l_interfaceerror_count
        FROM xxwc_onhand_bal_cnv
       WHERE status = 'E';
    
      print_debug('Number Of Records with Error in Interface Table: ' || l_interfaceerror_count);
    
      SELECT COUNT(1)
        INTO l_interfacerec_count
        FROM xxwc_onhand_bal_cnv
       WHERE status = 'P';
    
      print_debug('Number Of Valid Records in Interface Table: ' || l_interfacerec_count);
  END process_onhand;
  --


  ---
  -- PROCEDURE: Item On-Hand Main Conversion Program
  ---
  PROCEDURE itemonhand_conv_proc(errbuf          OUT VARCHAR2
                                ,retcode         OUT VARCHAR2
                                ,p_validate_only IN VARCHAR2) IS
    l_count        NUMBER;
    l_count_valid  NUMBER;
    v_errorcode    NUMBER;
    v_errormessage VARCHAR2(240);
  
    CURSOR c_master IS
      SELECT DISTINCT organization_code
        FROM xxwc_onhand_bal_cnv
       WHERE status = 'R';
  
    CURSOR c_items(p_org VARCHAR2) IS
      SELECT *
        FROM xxwc_onhand_bal_cnv
       WHERE upper(organization_code) = upper(p_org)
         AND status = 'R';
    l_reset VARCHAR2(10) := 'Y';
  BEGIN
    print_debug('**** XXWC AHH INV Item Conversion Program - Started');
    print_debug(rpad('-'
                    ,100
                    ,'-'));
  
    print_debug(' Reseting .........!');
    -- Reset
    IF l_reset = 'Y' THEN
      UPDATE xxwc_onhand_bal_cnv
         SET status = 'N';
      COMMIT;
    
      xxwc_ahh_item_conv_pkg.cleanup_tables();
    
    END IF;
  
    SELECT COUNT(1)
      INTO l_count
      FROM xxwc_onhand_bal_cnv
     WHERE status IN ('N');
  
    print_debug(' **** TOTAL Rows - ' || l_count || ' ****');
  
    -- Validate
    IF l_count > 0 THEN
      print_debug(' Performing Validations.....');
      --
      validations();
      --
    ELSE
      print_debug(' *** No Records To Process.....');
    END IF;
  
    -- Process
    IF nvl(p_validate_only
          ,'Y') = 'N' THEN
      SELECT COUNT(1)
        INTO l_count_valid
        FROM xxwc_onhand_bal_cnv
       WHERE status = 'V';
      print_debug(' **** TOTAL Validated Rows - ' || l_count || ' ****');
    
      IF l_count_valid > 0 THEN
        print_debug(' Performing Insertion.....');
        --
        process_onhand();
        --
      ELSE
        print_debug(' *** No Records To Insert.....');
      END IF;
    ELSE
      print_debug(' Validation Only, Skipping Insertion.....');
    END IF;
  
    print_debug(rpad('-'
                    ,100
                    ,'-'));
    print_debug('**** XXWC AHH INV Item Conversion Program - Completed');
  EXCEPTION
    WHEN OTHERS THEN
      retcode := 2;
      errbuf  := SQLERRM;
      print_debug('**** Error: ' || SQLERRM);
      print_debug('**** XXWC AHH INV Item Conversion Program - Abort');
    
  END itemonhand_conv_proc;

--
END xxwc_ahh_item_onhand_conv_pkg;
/
