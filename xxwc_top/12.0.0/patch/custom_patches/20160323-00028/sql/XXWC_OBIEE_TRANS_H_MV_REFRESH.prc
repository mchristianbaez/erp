 /*************************************************************************

   **************************************************************************
     $Header xxwc_obiee_trans_h_mv_refresh $
     Module Name: xxwc_obiee_trans_h_mv_refresh.prc

     PURPOSE:   This procedure is to refresh all Historical MVS and SubMVs for OBIEE

     REVISIONS:
     Ver        Date        Author             Description
     ---------  ----------  ---------------   -------------------------
      1.0       29/03/2016  Neha Saini         Initial Build - TMS# 20160323-00028

   /*************************************************************************/

CREATE OR REPLACE PROCEDURE APPS.xxwc_obiee_trans_h_mv_refresh (
   errbuf    OUT VARCHAR2,
   retcode   OUT VARCHAR2)
IS
   l_error_message2      CLOB;
   l_message             CLOB;
   l_conc_req_id         NUMBER := fnd_global.conc_request_id;
   g_email_distro_list   VARCHAR2 (500) := 'HDSWCITOBIEEBISupport@HDSsupply.com';
   g_status              VARCHAR2 (5);

   /*************************************************************************
  Procedure : send_email

  PURPOSE:   This procedure is to send email to stake holder in case of error
  Parameter: IN
             p_debug_msg      -- Error Message
************************************************************************/
   --add message to concurrent log file
   PROCEDURE send_email (p_error_msg IN VARCHAR2)
   IS
      l_subject       VARCHAR2 (400);
      l_body_header   VARCHAR2 (4000);
      l_body_detail   VARCHAR2 (4000);
      l_body_footer   VARCHAR2 (4000);
      l_body          VARCHAR2 (32000);
      l_sender        VARCHAR2 (100) := 'no-reply@whitecap.net';
      l_hostport      VARCHAR2 (20) := '25';
      l_host          VARCHAR2 (256) := 'mailoutrelay.hdsupply.net';
      l_instance      VARCHAR2 (50);
      l_request_id    NUMBER;
   BEGIN
      ----------------------------------------------------------------------------------
      -- Derive Oracle Instance
      ----------------------------------------------------------------------------------
      BEGIN
         SELECT SUBSTR (UPPER (instance_name), 1, 7)
           INTO l_instance
           FROM v$instance;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_instance := NULL;
      END;

      l_request_id := fnd_global.conc_request_id;

      ------------------------------------------
      -- Send Email Notification
      ------------------------------------------
      IF g_status = 'F'
      THEN
         l_subject :=
               l_instance
            || ' : '
            || 'XXWC OBIEE Customer Transaction Historical MV Refresh failure: ';
         l_body_header :=
            '<style type="text/css">.style1 {font-family: Arial, Helvetica, sans-serif;}</style><p class="style1">';
         l_body_detail := '<BR> ' || p_error_msg || '</BR>';
         l_body_footer :=
               '<BR></BR> Concurrent program XXWC OBIEE Customer Transaction Historical MV Refresh failed.<BR></BR> Please see the log file for more information. <BR></BR> Request ID: '
            || l_request_id
            || ' <BR></BR>';
         l_body := l_body_header || l_body_detail || CHR (10) || l_body_footer;
      ELSIF g_status = 'S'
      THEN
         l_subject :=
               l_instance
            || ' : '
            || 'XXWC OBIEE Customer Transaction Historical MV Refresh completed Successfully ';
         l_body_header :=
            '<style type="text/css">.style1 {font-family: Arial, Helvetica, sans-serif;}</style><p class="style1">';
         l_body_detail := '<BR></BR>';
         l_body_footer :=
               '<BR></BR> Concurrent program XXWC OBIEE Customer Transaction Historical MV Refresh Completed Successfully.<BR></BR> Request ID: '
            || l_request_id
            || ' <BR></BR>';
         l_body := l_body_header || l_body_detail || CHR (10) || l_body_footer;
      END IF;


      xxcus_misc_pkg.html_email (
         p_to              => g_email_distro_list, 
         p_from            => l_sender,
         p_text            => 'XXWC OBIEE TRANSACTIONAL HISTORICAL MV REFRESH FAILURE:',
         p_subject         => l_subject,
         p_html            => l_body,
         p_smtp_hostname   => l_host,
         p_smtp_portnum    => l_hostport);
   EXCEPTION
      WHEN OTHERS
      THEN
         fnd_file.put_line (fnd_file.LOG, p_error_msg || SQLERRM);
         DBMS_OUTPUT.put_line (p_error_msg || SQLERRM);
   END send_email;
BEGIN
   errbuf := NULL;
   retcode := 0;
   g_status := 'S';
   fnd_file.put_line(fnd_file.log,'Starting OBIEE Customer TRX Hidtorical MV Refresh');
   fnd_file.put_line(fnd_file.log,' ');

   fnd_file.put_line(fnd_file.log,'Altering Session for _delay_index_maintain attribute');
   execute immediate 'ALTER SESSION SET "_delay_index_maintain"=FALSE';

   fnd_file.put_line(fnd_file.log,' ');
   fnd_file.put_line(fnd_file.log,'Executing group of alter materialized view - COMPILE on ALL related Historical SubMVs and IncMV');

   execute immediate 'ALTER MATERIALIZED VIEW APPS.XXWC_OBIEE_CT__PO_PRICE_MV     COMPILE';
   execute immediate 'ALTER MATERIALIZED VIEW APPS.XXWC_OBIEE_CT__PRC_ADJ_MV      COMPILE';
   execute immediate 'ALTER MATERIALIZED VIEW APPS.XXWC_OBIEE_CT__ORD_LN_COST_MV  COMPILE';
   execute immediate 'ALTER MATERIALIZED VIEW APPS.XXWC_OBIEE_CT__VNDR_QUOTE_H_MV COMPILE';
   execute immediate 'ALTER MATERIALIZED VIEW APPS.XXWC_OBIEE_CT__ORD_LN_MV       COMPILE';
   execute immediate 'ALTER MATERIALIZED VIEW APPS.XXWC_OBIEE_CT__MTL_ITEM_CAT_MV COMPILE';
   execute immediate 'ALTER MATERIALIZED VIEW APPS.XXWC_OBIEE_CT__CTL_MV          COMPILE';
   execute immediate 'ALTER MATERIALIZED VIEW APPS.XXWC_OBIEE_CUST_TRX_HIST_MV    COMPILE';

   fnd_file.put_line(fnd_file.log,' ');
   fnd_file.put_line(fnd_file.log,'Executing DBMS_SNAPSHOT.REFRESH for list of Historical MVs sequentially');
   fnd_file.put_line(fnd_file.log,'Refresh Start Time '||to_char(sysdate, 'MM/DD/YYYY HH24:MI:SS'));
   DBMS_SNAPSHOT.REFRESH(LIST =>'APPS.XXWC_OBIEE_CT__PO_PRICE_MV
                                   ,APPS.XXWC_OBIEE_CT__PRC_ADJ_MV
                                   ,APPS.XXWC_OBIEE_CT__ORD_LN_COST_MV
                                   ,APPS.XXWC_OBIEE_CT__VNDR_QUOTE_H_MV
                                   ,APPS.XXWC_OBIEE_CT__ORD_LN_MV
                                   ,APPS.XXWC_OBIEE_CT__MTL_ITEM_CAT_MV
                                   ,APPS.XXWC_OBIEE_CT__CTL_MV
                                   ,APPS.XXWC_OBIEE_CUST_TRX_HIST_MV'
                           , PUSH_DEFERRED_RPC => FALSE
                           , REFRESH_AFTER_ERRORS =>FALSE
                           , PURGE_OPTION=>0
                           , PARALLELISM =>0
                           , ATOMIC_REFRESH=>FALSE
                           , NESTED=> TRUE);

   fnd_file.put_line(fnd_file.log,'Refresh End Time '||to_char(sysdate, 'MM/DD/YYYY HH24:MI:SS'));
   fnd_file.put_line(fnd_file.log,'Historical SubMV and MV Refresh Complete');
   commit; 
   l_message :=
      'Concurrent program XXWC OBIEE Customer Transaction Historical MV Refresh Completed Successfully';
   send_email (l_message);
EXCEPTION
   WHEN OTHERS
   THEN
      g_status := 'F';
      l_error_message2 :=
            'xxwc_bko_visibility_std_prc '
         || 'Error_Stack...'
         || DBMS_UTILITY.format_error_stack ()
         || ' Error_Backtrace...'
         || DBMS_UTILITY.format_error_backtrace ();
      send_email (l_error_message2);
      xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => 'xxwc_obiee_trans_h_mv_refresh',
         p_calling             => 'xxwc_obiee_trans_h_mv_refresh',
         p_request_id          => l_conc_req_id,
         p_ora_error_msg       => l_error_message2,
         p_error_desc          => 'Error running XXWC OBIEE Customer Trx Historical MV Refresh',
         p_distribution_list   => 'HDSOracleDevelopers@hdsupply.com',
         p_module              => 'OBIEE');

      DBMS_OUTPUT.put_line (
            'Error in main xxwc_obiee_trans_h_mv_refresh. Error: '
         || l_error_message2);
      fnd_file.put_line (
         fnd_file.LOG,
            'Error in main xxwc_obiee_trans_h_mv_refresh. Error: '
         || l_error_message2);

      errbuf := l_error_message2;
      retcode := 2;
END;
/