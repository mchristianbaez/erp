   /*************************************************************************
   *   $Header XXWC_OE_ORDER_HOLDS_AL_N4.sql $
   *   Purpose : Index for purge program Performance issue 
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        18-Apr-2016 Rakesh Patel            Initial Version TMS 20160418-00008 
   * ***************************************************************************/

CREATE INDEX ONT.XXWC_OE_ORDER_HOLDS_AL_N4
   ON ONT.OE_ORDER_HOLDS_ALL (HOLD_RELEASE_ID
                              )
   TABLESPACE APPS_TS_TX_IDX
/   