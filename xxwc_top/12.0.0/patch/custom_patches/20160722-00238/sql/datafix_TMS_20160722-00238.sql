/*************************************************************************
  $Header TMS_20160722-00238 _SHIPPED_TO_CLOSE.sql $
  Module Name: TMS_20160722-00238 


  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        17-AUG-2016  Neha Saini       TMS#20160722-00238 for order number 20247372

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20160722-00238    , Before Update');

update apps.oe_order_lines_all
set 
  INVOICE_INTERFACE_STATUS_CODE='NOT_ELIGIBLE'
,open_flag='N'
,flow_status_code='CLOSED'
--,INVOICED_QUANTITY=1
where line_id =68453884
and headeR_id=41819796;

   DBMS_OUTPUT.put_line (
         'TMS: 20160722-00238  Sales order lines updated (Expected:1): '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20160722-00238    , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20160722-00238 , Errors : ' || SQLERRM);
END;
/
