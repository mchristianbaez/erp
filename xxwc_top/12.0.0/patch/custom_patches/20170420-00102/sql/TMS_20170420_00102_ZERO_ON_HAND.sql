/*************************************************************************
  $Header TMS_20170420-00102_ZERO_ON_HAND.sql $
  Module Name: TMS_20170420-00102  Data Fix script for Inventory Details - Inv OH Overview negative showing that is not neg 

  PURPOSE: Data fix script for 20170420-00102  -- Inventory Details - Inv OH Overview negative showing that is not neg 
  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        02-MAY-2017  Sundaramoorthy         TMS#20170420-00102 -- Inventory Details - Inv OH Overview negative showing that is not neg 

**************************************************************************/ 
SET SERVEROUTPUT on size 1000000 
  
  CREATE TABLE XXWC.XXWC_MTL_ONHAND_QTY_DTL_BKP
     AS
     SELECT *
     FROM mtl_onhand_quantities_detail; 
  /
  DECLARE 
  CURSOR c1 is 
  SELECT inventory_item_id item,
         organization_id org, 
         subinventory_code subinv,
		 NVL(locator_id,-999) loc, 
		 NVL(lot_number,'@@@') lot,
	     NVL(revision,'@@@') rev, 
		 NVL(cost_group_id,-999) cg,
		 SUM(primary_transaction_quantity) 
  FROM  mtl_onhand_quantities_detail 
  WHERE NVL(containerized_flag,2)=2 
  AND lpn_id IS NULL 
  GROUP BY inventory_item_id,organization_id,subinventory_code, 
  locator_id,lot_number,revision,cost_group_id 
  HAVING SUM(primary_transaction_quantity) = 0 ; 
  
  v_count number; 
 BEGIN 
 
    dbms_output.put_line('TMS: 20170420-00102   , Before Update');
 FOR c1_r IN c1 LOOP 
    
    dbms_output.put_line('item- '||c1_r.item||',subinv- '||c1_r.subinv||',loc- '||c1_r.loc||',lot number- '||c1_r.lot|| ',Cost Grp- ' ||c1_r.cg); 
  
  DELETE FROM mtl_onhand_quantities_detail 
    WHERE inventory_item_id = c1_r.item 
    AND organization_id = c1_r.org 
    AND NVL(locator_id,-999) = c1_r.loc 
    AND NVL(lot_number,'@@@') = c1_r.lot 
    AND NVL(cost_group_id,-999) = c1_r.cg 
    AND NVL(revision,'@@@') = c1_r.rev 
    AND subinventory_code = c1_r.subinv; 
  
  v_count := SQL%ROWCOUNT; 
  
  
    dbms_output.put_line('item- '||c1_r.item||',subinv- '||c1_r.subinv||',loc- '||c1_r.loc||',lot number- '||c1_r.lot|| ',Cost Grp- ' ||c1_r.cg); 
  
  dbms_output.put_line('Rows deleted :'||v_count); 
  END LOOP; 
  commit;
  dbms_output.put_line('TMS: 20170420-00102   , After Update');
 END;
 /
