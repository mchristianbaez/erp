---------------------------------------------------------------------------------------------------------------
/**************************************************************************************************************
  $Header XXEIS.eis_xxwc_cycle_acc_count_v $
  Module Name : Inventory
  PURPOSE	  : Cycle Count Accuracy
  VERSION 	DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.1     14-Feb-2016       	Siva   		 TMS#20161118-00057  
**************************************************************************************************************/
CREATE OR REPLACE FORCE VIEW xxeis.eis_xxwc_cycle_acc_count_v
(
   district
  ,region
  ,org
  ,org_code
  ,count_date
  ,cat_calss
  ,sv_class
  ,primary_uom_code
  ,item_number
  ,item_description
  ,ts
  ,avg_cost
  ,begining_count_date
  ,end_count_date
  ,prior_count_date
  ,starting_quantity
  ,prior_quantity
  ,end_quantity
  ,var_quantity
  ,varquanper
  ,var_cost
  ,total_var_cost
  ,inventory_item_id
  ,count_end_quantity
  ,count_var_quantity
  ,last_update_date
)
AS
   SELECT mp.attribute8 district
         ,mp.attribute9 region
         ,ood.organization_name org
         ,ood.organization_code org_code
         ,mcce.creation_date count_date  --Added by Mahender for TMS#40314-00007 on 09/25/2014
         --,mcce.count_date_first count_date  --Commented by Mahender for TMS#40314-00007 on 09/25/2014
         ,xxeis.eis_rs_xxwc_com_util_pkg.get_inv_cat_segments (mcce.inventory_item_id, mcce.organization_id)
             cat_calss
         ,xxeis.eis_rs_xxwc_com_util_pkg.get_inv_vel_cat_class (mcce.inventory_item_id, mcce.organization_id)
             sv_class
         ,msi.primary_uom_code primary_uom_code
         ,msi.concatenated_segments item_number
         ,msi.description item_description
         ,CASE
             WHEN msi.lot_control_code = 2 THEN 'Y'
             WHEN (msi.shelf_life_days > 0 AND msi.shelf_life_days < 10000) THEN 'Y'
             ELSE 'N'
          END
             AS ts
         ,         --APPS.CST_COST_API.GET_ITEM_COST (1,MCCE.INVENTORY_ITEM_ID,MCCE.ORGANIZATION_ID) AVG_COST,
          mcce.item_unit_cost avg_cost
         ,                                                                      -- Malli Changes on 02/18/2014
          TRUNC (mcce.count_date_first) begining_count_date
         ,TRUNC (mcce.count_date_current) end_count_date
         ,mcce.count_date_prior prior_count_date
         ,mcce.system_quantity_first starting_quantity
         ,mcce.count_quantity_prior prior_quantity
         ,mcce.count_quantity_current end_quantity
         ,NVL (mcce.count_quantity_current, 0) - NVL (mcce.system_quantity_first, 0) var_quantity
         ,                                                                      ------- diane changed 2/5/2014
            -- mcce.adjustment_quantity                                               var_quantity,
            --  nvl( mcce.adjustment_quantity ,0)*100/decode(nvl(mcce.system_quantity_first  ,0) ,0,1, mcce.system_quantity_first)  varquanper,
            (NVL (mcce.count_quantity_current, 0) - NVL (mcce.system_quantity_first, 0))
          * 100
          / DECODE (NVL (mcce.system_quantity_first, 0), 0, 1, mcce.system_quantity_first)
             varquanper
         ,                                                                      ------- diane changed 2/5/2014
            --(NVL(MCCE.COUNT_QUANTITY_CURRENT ,0) - NVL(MCCE.SYSTEM_QUANTITY_FIRST,0))* NVL(APPS.CST_COST_API.GET_ITEM_COST (1,MCCE.INVENTORY_ITEM_ID,MCCE.ORGANIZATION_ID),1) VAR_COST, ------- diane changed 2/5/2014
            (NVL (mcce.count_quantity_current, 0) - NVL (mcce.system_quantity_first, 0))
          * NVL (mcce.item_unit_cost, 1)
             var_cost
         ,                                                                     ------- Malli changed 2/18/2014
            --  MCCE.ADJUSTMENT_AMOUNT VAR_COST,
            /*NVL(MCCE.ADJUSTMENT_AMOUNT,0)*100/
            DECODE((NVL(MCCE.SYSTEM_QUANTITY_FIRST,0))* NVL(APPS.CST_COST_API.GET_ITEM_COST (1,MCCE.INVENTORY_ITEM_ID,MCCE.ORGANIZATION_ID),1),0,1,
            (NVL(MCCE.SYSTEM_QUANTITY_FIRST,0))* NVL(APPS.CST_COST_API.GET_ITEM_COST (1,MCCE.INVENTORY_ITEM_ID,MCCE.ORGANIZATION_ID),1))
            */
            -- (NVL(MCCE.SYSTEM_QUANTITY_FIRST,0)) * NVL(APPS.CST_COST_API.GET_ITEM_COST (1,MCCE.INVENTORY_ITEM_ID,MCCE.ORGANIZATION_ID),1) TOTAL_COST,
            (  (NVL (mcce.system_quantity_first, 0) * NVL (mcce.item_unit_cost, 1))
             - (NVL (mcce.count_quantity_current, 0) * NVL (mcce.item_unit_cost, 1)))
          * 100
          / DECODE ( (NVL (mcce.count_quantity_first, 0)) * NVL (mcce.item_unit_cost, 1)
                   ,0, 1
                   , (NVL (mcce.count_quantity_first, 0)) * NVL (mcce.item_unit_cost, 1))
             total_var_cost
         ,msi.inventory_item_id
         ,DECODE (SIGN (mcce.count_quantity_current),  0, 0,  1, 1,  -1, 1,  0) count_end_quantity
         ,DECODE (SIGN (NVL (mcce.count_quantity_current, 0) - NVL (mcce.system_quantity_first, 0))
                 ,0, 0
                 ,1, 1
                 ,-1, 1
                 ,0)
             count_var_quantity
         --,TRUNC (mcce.last_update_date) last_update_date  --commented for version 1.1
		 ,TRUNC (mcce.approval_date) last_update_date --added for version 1.1
     FROM mtl_cycle_count_entries_v mcce
         ,mtl_parameters mp
         ,org_organization_definitions ood
         ,mtl_system_items_b_kfv msi
    WHERE     mp.organization_id = mcce.organization_id
          AND ood.organization_id = mcce.organization_id
          AND msi.inventory_item_id = mcce.inventory_item_id
          AND mcce.entry_status_code <> 4                                          --Excluding Rejetced Cycles
          AND mcce.count_date_first IS NOT NULL   --Added by Mahender for TMS#40314-00007 on 09/25/2014
          AND mcce.count_status = 'Completed'--Added by Mahender for TMS#40314-00007 on 09/25/2014
          AND msi.organization_id = mcce.organization_id;

