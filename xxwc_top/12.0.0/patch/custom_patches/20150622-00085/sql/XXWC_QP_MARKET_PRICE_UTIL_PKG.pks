CREATE OR REPLACE PACKAGE APPS.xxwc_qp_market_price_util_pkg
as
/*************************************************************************
 Copyright (c) 2013 Lucidity Consulting Group
 All rights reserved.
**************************************************************************
  $Header xxwc_qp_market_price_util_pkg$
  Module Name: xxwc_qp_market_price_util_pkg.pks

  PURPOSE:   This package is used for the extensions in Market Pricing

  REVISIONS:
  Ver        Date        Author                     Description
  ---------  ----------  ---------------         -------------------------
  1.0        05/07/2013  Consuelo Gonzalez      Initial Version
  2.0        07/29/2013  Consuelo Gonzalez      TMS 20130729-00576: Bug fix for 
                                                Generate Ntl Price List to change 
                                                start date and precedence
  3.0        08/02/2013  Consuelo Gonzalez      TMS 20130729-00576: Added new procedure
                                                for market price list extract
  4.0        08/08/2013  Consuelo Gonzalez      TMS 20130729-00576: Additional changes
                                                to extract and to handle market category
                                                price list upload. Change to the get_market_list_price
                                                to include the new market category level
  5.0         12/2/2013  Ram Talluri            TMS #20131202-00166 -Items with Null start date are not showing up in price list extract, adding NVL condition. 
  6.0         01/07/2014 Consuelo Gonzalez      TMS 20131016-00375: added profile values to control
                                                batch sizes 
  7.0         02/24/2015 Shankar Hariharan      TMS# 20150219-00234 Added new function to get Call for Price Flag                                                                     
  8.0         08/03/2015 Gopi Damuluri          TMS# 20150622-00085 Pricing Segmentation
**************************************************************************/

G_MARKET_NTL_PRL_ID     NUMBER := fnd_profile.value ('XXWC_QP_MARKET_NATIONAL_PRL');
G_MARKET_NTL_STP_PRL_ID NUMBER := fnd_profile.value ('XXWC_QP_MARKET_NTL_SETUP_PRL');
-- 08/08/2013 CG: TMS 20130729-00576: Added for the new Market Category Price List
G_MARKET_CAT_PRL_ID     NUMBER := fnd_profile.value ('XXWC_QP_MARKET_CATEGORY_PRL');

TYPE price_list_header_type IS RECORD (
    list_header_id      NUMBER := FND_API.G_MISS_NUM
    , name              VARCHAR2(240)  := FND_API.G_MISS_CHAR
);

/*
prl_name            VARCHAR2(240)   := FND_API.G_MISS_CHAR
    , item_number       VARCHAR2(40)    := FND_API.G_MISS_CHAR
    , list_price        NUMBER          := FND_API.G_MISS_NUM
    , obsolete_item     VARCHAR2(1)     := FND_API.G_MISS_CHAR
*/

TYPE maint_tool_data_type IS RECORD (
      FILE_ID               NUMBER              := FND_API.G_MISS_NUM
    , PRL_NAME              VARCHAR2(240 BYTE)  := FND_API.G_MISS_CHAR
    , ITEM_NUMBER           VARCHAR2(40 BYTE)   := FND_API.G_MISS_CHAR
    , LIST_PRICE            NUMBER              := FND_API.G_MISS_NUM
    , OBSOLETE_ITEM         VARCHAR2(1 BYTE)    := FND_API.G_MISS_CHAR
    , PRODUCT_ATTRIBUTE     VARCHAR2(30 BYTE)   := FND_API.G_MISS_CHAR
    , PRODUCT_ATTR_VALUE    VARCHAR2(240 BYTE)  := FND_API.G_MISS_CHAR
    , PRICE_TYPE            VARCHAR2(50 BYTE)   := FND_API.G_MISS_CHAR
    , CALL_FOR_PRICE        VARCHAR2(1 BYTE)    := FND_API.G_MISS_CHAR              
    , LIST_HEADER_ID        NUMBER              := FND_API.G_MISS_NUM
    , LIST_LINE_ID          NUMBER              := FND_API.G_MISS_NUM
    , INVENTORY_ITEM_ID     NUMBER              := FND_API.G_MISS_NUM
    , PRODUCT_ATTR_ID       NUMBER              := FND_API.G_MISS_NUM
    , PRICING_FORMULA_ID    NUMBER              := FND_API.G_MISS_NUM
    , STATUS                VARCHAR2(30)        := FND_API.G_MISS_CHAR
    , ERR_MESSAGE           VARCHAR2(4000)      := FND_API.G_MISS_CHAR
    , REQUEST_ID            NUMBER              := FND_API.G_MISS_NUM
    , CREATED_BY            NUMBER              := FND_API.G_MISS_NUM
    , CREATION_DATE         DATE                := FND_API.G_MISS_DATE
    , LAST_UPDATED_BY       NUMBER              := FND_API.G_MISS_NUM
    , LAST_UPDATE_DATE      DATE                := FND_API.G_MISS_DATE
    , LAST_UPDATE_LOGIN     NUMBER              := FND_API.G_MISS_NUM
);

TYPE price_list_header_tbl_type IS TABLE OF price_list_header_type
INDEX BY BINARY_INTEGER;

TYPE maint_tool_data_tbl_type IS TABLE OF maint_tool_data_type
INDEX BY BINARY_INTEGER;

G_MISS_PRICE_LIST_HEADER_REC price_list_header_type;
G_MISS_MAINT_TOOL_DATA_REC maint_tool_data_type;
G_MISS_PRICE_LIST_HEADER_TBL  price_list_header_tbl_type;
G_MISS_MAINT_TOOL_DATA_TBL maint_tool_data_tbl_type;

-- Version# 8.0 > Start

TYPE price_segment_data_type IS RECORD ( SEGMENT                 VARCHAR2(50)      := FND_API.G_MISS_CHAR
                                    , PRODUCT_TYPE            VARCHAR2(50)      := FND_API.G_MISS_CHAR
                                    , PRODUCT                 VARCHAR2(50)      := FND_API.G_MISS_CHAR
                                    , DESCRIPTION             VARCHAR2(240)     := FND_API.G_MISS_CHAR
                                    , DISCOUNT_TYPE           VARCHAR2(50)      := FND_API.G_MISS_CHAR
                                    , TIER1                   NUMBER            := FND_API.G_MISS_NUM
                                    , TIER2                   NUMBER            := FND_API.G_MISS_NUM
                                    , TIER3                   NUMBER            := FND_API.G_MISS_NUM
                                    , TIER4                   NUMBER            := FND_API.G_MISS_NUM
                                    , TIER5                   NUMBER            := FND_API.G_MISS_NUM
                                    , TIER6                   NUMBER            := FND_API.G_MISS_NUM
                                    , TIER7                   NUMBER            := FND_API.G_MISS_NUM
                                    , DELETE_FLAG             VARCHAR2(1)       := FND_API.G_MISS_CHAR                                    
                                    , LAST_UPDATE_DATE        DATE              := FND_API.G_MISS_DATE
                                    , LAST_UPDATED_BY         NUMBER            := FND_API.G_MISS_NUM
                                    , CREATION_DATE           DATE              := FND_API.G_MISS_DATE
                                    , CREATED_BY              NUMBER            := FND_API.G_MISS_NUM
                                    , PRODUCT_ID              NUMBER            := FND_API.G_MISS_NUM
                                    , FILE_ID                 NUMBER            := FND_API.G_MISS_NUM
                                    , REQUEST_ID              NUMBER            := FND_API.G_MISS_NUM
                                    );

TYPE price_segment_tbl_type IS TABLE OF price_segment_data_type
INDEX BY BINARY_INTEGER;

G_PRICE_SEGMENT_REC price_segment_data_type;
G_PRICE_SEGMENT_TBL price_segment_tbl_type;

-- Version# 8.0 < End

FUNCTION get_item_pricing_cat (p_inventory_item_id IN NUMBER)
RETURN CHAR;

PROCEDURE xxwc_calc_wght_ntl_avg_cost (RETCODE     OUT NUMBER
                                       , ERRMSG    OUT VARCHAR2
                                       , P_LIST_HEADER_ID   IN NUMBER);

-- 05/13/2013 CG: Adding Maintenance Tool Processor
PROCEDURE xxwc_qp_maint_processor (RETCODE      OUT NUMBER
                                   , ERRMSG     OUT VARCHAR2
                                   , P_FILE_ID  IN NUMBER);

-- 05/21/2013 CG: Used by the Maintenance Tool Extract Program
FUNCTION get_eisr_vendor_num (p_inventory_item_id in number
                              , p_organization_id in number) RETURN VARCHAR2;

-- 05/21/2013 CG: Added to Generate the National Market Price list
PROCEDURE xxwc_gen_ntl_market_prl (RETCODE     OUT NUMBER
                                   , ERRMSG    OUT VARCHAR2
                                   , P_BASE_PRL_ID  IN NUMBER
                                   , P_DEST_PRL_ID  IN NUMBER);

-- 05/29/2013 CG: Added to retrieve the item number based on the product attribute value
FUNCTION get_item_number (p_product_attr_value  IN VARCHAR2
                          , p_return_type       IN VARCHAR2) RETURN VARCHAR2;

-- 05/29/2013 CG: Added to retrieve the category segments
FUNCTION get_category_desc (p_product_attr_value    IN VARCHAR2
                            , p_category_set          IN   VARCHAR2
                            , p_segment_num           IN   NUMBER) RETURN VARCHAR2;

-- 05/29/2013 CG: Adding National Price List Setup Maintenance Tool Processor
PROCEDURE XXWC_QP_NTL_PRL_MAINT_TOOL (RETCODE      OUT NUMBER
                                     , ERRMSG     OUT VARCHAR2
                                     , P_FILE_ID  IN NUMBER);

-- 06/24/2013 CG: Added function to retrieve market price list
FUNCTION get_market_list_price (p_inventory_item_id IN NUMBER
                                , p_organization_id IN NUMBER) RETURN NUMBER;
                                
-- TMS# 20150219-00234 > Start
FUNCTION get_call_for_price_flag (p_inventory_item_id IN NUMBER
                                , p_organization_id IN NUMBER) RETURN VARCHAR2;                                
-- TMS# 20150219-00234 < End

-- 07/08/2013 CG: Added new procedure for program XXWC QP Pricing For New Product Release
PROCEDURE xxwc_price_new_prod_rel (RETCODE     OUT NUMBER
                                   , ERRMSG    OUT VARCHAR2
                                   , P_INVENTORY_ITEM_ID  IN NUMBER
                                   , P_UNIT_PRICE  IN NUMBER);

-- 08/02/2013 CG: TMS 20130729-00576: New market extract procedure
PROCEDURE xxwc_gen_market_extract (RETCODE     OUT VARCHAR2
                                   , ERRMSG    OUT VARCHAR2
                                   , P_INVENTORY_CATEGORY IN VARCHAR2
                                   , P_INCLUDE_SPECIALS IN VARCHAR2);

-- Version# 8.0 > Start
PROCEDURE xxwc_pri_segment_maint_prc (RETCODE      OUT NUMBER
                                     , ERRMSG       OUT VARCHAR2
                                     , P_FILE_ID     IN NUMBER);
-- Version# 8.0 < End                                     

end xxwc_qp_market_price_util_pkg;
/
