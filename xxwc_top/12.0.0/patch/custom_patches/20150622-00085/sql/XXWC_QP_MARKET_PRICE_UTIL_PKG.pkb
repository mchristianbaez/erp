CREATE OR REPLACE PACKAGE BODY APPS.xxwc_qp_market_price_util_pkg
as
/*************************************************************************
  $Header xxwc_qp_market_price_util_pkg $
  Module Name: xxwc_qp_market_price_util_pkg.pks

  PURPOSE:   This package is used for the extensions in Market Pricing

  REVISIONS:
  Ver        Date        Author                     Description
  ---------  ----------  ---------------         -------------------------
  1.0        05/07/2013  Consuelo Gonzalez      Initial Version
  2.0        07/29/2013  Consuelo Gonzalez      TMS 20130729-00576: Bug fix for 
                                                Generate Ntl Price List to change 
                                                start date and precedence
  3.0        08/02/2013  Consuelo Gonzalez      TMS 20130729-00576: Added new procedure
                                                for market price list extract
  4.0        08/08/2013  Consuelo Gonzalez      TMS 20130729-00576: Additional changes
                                                to extract and to handle market category
                                                price list upload. Change to the get_market_list_price
                                                to include the new market category level
  5.0         12/2/2013  Ram Talluri            TMS #20131202-00166 -Items with Null start date are not showing up in price list extract, adding NVL condition. 
  6.0        01/07/2014  Consuelo Gonzalez      TMS 20131016-00375: added profile values to control
                                                batch sizes                 
 7.0        4/8/2014    Ram Talluri             TMS #20140407-00289 - Price list records are not correctly end dating   
 7.1        4/11/2014  Ram Talluri              TMS #20140411-00207  Added NVL Condition.
 7.2        02/24/2015 Shankar Hariharan        TMS# 20150219-00234 Added new function to get Call for Price Flag
  8.0       08/03/2015  Gopi Damuluri           TMS# 20150622-00085 Pricing Segmentation
**************************************************************************/

PROCEDURE   write_log   (p_message in varchar2)
IS
BEGIN
    dbms_output.put_line (p_message);
    fnd_file.put_line(fnd_file.log, p_message);
EXCEPTION
WHEN OTHERS THEN
    null;
END write_log;

PROCEDURE   write_output   (p_message in varchar2)
IS
BEGIN
    dbms_output.put_line (p_message);
    fnd_file.put_line(fnd_file.output, p_message);
EXCEPTION
WHEN OTHERS THEN
    null;
END write_output;

FUNCTION get_item_pricing_cat (p_inventory_item_id IN NUMBER)
RETURN CHAR
IS
    l_segment1   VARCHAR2 (30);
BEGIN
    l_segment1 := null;
    
    SELECT c.segment1
      INTO l_segment1
      FROM apps.MTL_CATEGORY_SETS_V b
         , apps.mtl_categories c
         , apps.mtl_item_categories d
         , apps.mtl_parameters mp
     WHERE     b.category_set_name = 'Pricing Category'
           AND b.structure_id = c.structure_id
           AND c.enabled_flag = 'Y'
           AND c.category_id = d.category_id
           AND d.inventory_item_id = p_inventory_item_id
           AND d.organization_id = mp.organization_id
           AND mp.organization_code = 'MST';

    return l_segment1;

EXCEPTION
WHEN OTHERS THEN
    return null;
END get_item_pricing_cat;

PROCEDURE xxwc_calc_wght_ntl_avg_cost (RETCODE     OUT NUMBER
                                       , ERRMSG    OUT VARCHAR2
                                       , P_LIST_HEADER_ID   IN NUMBER)
IS
    cursor cur_prl_lines is
            select  qlh.list_header_id
                    , qlh.name
                    , qlh.description
                    , qll.list_line_id
                    , qll.list_line_no
                    , qll.product_precedence
                    , qll.arithmetic_operator
                    , qll.operand
                    , qll.qualification_ind
                    , qll.pricing_phase_id
                    , qpa.pricing_attribute_id
                    , qpa.product_attribute_context
                    , qpa.product_attribute
                    , qpa.product_attr_value
                    , qpa.product_uom_code
            from    apps.qp_list_headers qlh
                    , apps.qp_list_lines qll
                    , apps.qp_pricing_attributes qpa
            where   qlh.list_header_id = p_list_header_id
            and     qlh.version_no = 1
            and     qlh.list_type_code = 'PRL'
            and     qlh.start_date_active <= trunc(sysdate)
            and     nvl(qlh.end_date_active, trunc(sysdate)) >= trunc(sysdate)
            and     qlh.list_header_id = qll.list_header_id
            and     qll.list_line_type_code = 'PLL'
            and     qll.start_date_active <= trunc(sysdate)
            and     nvl(qll.end_date_active, trunc(sysdate)) >= trunc(sysdate)
            and     qll.list_header_id = qpa.list_header_id
            and     qll.qualification_ind = qpa.qualification_ind
            and     qll.pricing_phase_id = qpa.pricing_phase_id
            and     qll.list_line_id = qpa.list_line_id; 

    cursor cur_item_list (p_category_set_id number
                          , p_category_id number
                          , p_exists varchar2
                          , p_request_id number)
    is
        select  distinct(mic.inventory_item_id) inventory_item_id
        from    apps.mtl_item_categories mic
        where   mic.organization_id = 222 -- Find a way to get master id without hardcode
        and     mic.category_set_id = p_category_set_id
        and     mic.category_id = p_category_id
        and     ( (
                    p_exists = 'N' and
                    not exists (select  'Not Loaded'
                            from    xxwc.xxwc_qp_wght_ntl_avg_cost_stg x1
                            where   x1.inventory_item_id = mic.inventory_item_id)
                  )
                  OR
                  (
                    p_exists = 'Y' and
                    exists (select  'Loaded'
                            from    xxwc.xxwc_qp_wght_ntl_avg_cost_stg x1
                            where   x1.inventory_item_id = mic.inventory_item_id)
                    and request_id != p_request_id
                  )
                );
                
    cursor cur_recalc_items (p_request_id number) is
            select  x1.rowid rid
                    , msib.segment1 item_number
                    , x1.*
            from    xxwc.xxwc_qp_wght_ntl_avg_cost_stg x1
                    , apps.mtl_system_items_b msib
            where   x1.list_header_id = p_list_header_id
            and     x1.request_id = p_request_id
            and     x1.wght_ntl_avg_cost is null
            and     x1.inventory_item_id = msib.inventory_item_id
            and     msib.organization_id = 222;

    l_msg_data                  VARCHAR2 (4000);
    l_error_message2            CLOB;
    l_count                     NUMBER;
    l_count_items               NUMBER;
    l_category_set_id           NUMBER;
    l_total_qoh_cost            NUMBER;
    l_total_qoh                 NUMBER;
    l_request_id                NUMBER := FND_GLOBAL.conc_request_id;
    l_login_id                  NUMBER := fnd_global.login_id;
    l_exists                    NUMBER;

BEGIN
    --mo_global.set_policy_context ('S', 162);
    --mo_global.init ('ONT');
    fnd_global.apps_initialize(user_id        => fnd_global.user_id,
                               resp_id        => fnd_global.resp_id,
                               resp_appl_id   => fnd_global.resp_appl_id);
    mo_global.set_policy_context ('S', fnd_global.org_id);                               

    commit;
    
    execute immediate ('truncate table xxwc.xxwc_qp_wght_ntl_avg_cost_stg');
    
    commit;
    
    Write_log ('Starting Program to Calcualte Weighted National Average Costs');
    Write_log ('*************************************************************');
    Write_log ('Processing List Header ID: '||p_list_header_id);
    Write_log ('Request ID: '||l_request_Id);
    
    Write_log (' ');
    l_count := 0;
    for c1 in cur_prl_lines
    loop
        exit when cur_prl_lines%notfound;
        
        Write_log ('Processing List Line No '||c1.list_line_no);
        
        if c1.product_attribute_context = 'ITEM' and c1.product_attribute in ('PRICING_ATTRIBUTE27', 'PRICING_ATTRIBUTE2')
        then
            l_category_set_id := null;
            begin
                select  category_set_id
                into    l_category_set_id
                from    apps.mtl_categories_b mcb
                        , apps.mtl_category_sets mcs
                where   mcb.category_id = to_number (c1.product_attr_value)
                and     mcb.structure_id = mcs.structure_id;
            exception
            when others then
                l_category_set_id := null;
                write_log ('Could not find cat set id for category '||c1.product_attr_value);
            end;
            
            if l_category_set_id is not null then
        
                Write_log ('Inserting new items associated to categories in price list');
                l_count_items := 0;
                for c2 in cur_item_list (l_category_set_id, to_number (c1.product_attr_value), 'N', l_request_id)
                loop
                    exit when cur_item_list%notfound;
                
                    begin
                        insert into xxwc.xxwc_qp_wght_ntl_avg_cost_stg (
                            inventory_item_id
                            , creation_date
                            , created_by
                            , last_update_date
                            , last_updated_by
                            , last_update_login
                            , request_id
                            , list_header_id
                        ) values (
                            c2.inventory_item_id
                            , sysdate
                            , fnd_global.user_id
                            , sysdate
                            , fnd_global.user_id
                            , l_login_id
                            , l_request_Id
                            , c1.list_header_id
                        );
                        l_count_items := l_count_items + 1;
                    exception
                    when others then
                        Write_log ('Could not insert inventory_item_id '||c2.inventory_item_id); 
                    end;
                
                end loop;
                Write_log ('Inserted '||l_count_items||' new items into the staging table');
                
                commit;
                
                Write_log ('Resetting existing items associated to categories in price list');
                l_count_items := 0;
                for c2 in cur_item_list (l_category_set_id, to_number (c1.product_attr_value), 'Y', l_request_id)
                loop
                    exit when cur_item_list%notfound;
                    
                    begin
                        update xxwc.xxwc_qp_wght_ntl_avg_cost_stg
                        set wght_ntl_avg_cost = null
                            , last_update_date = sysdate
                            , last_updated_by = fnd_global.user_id
                            , last_update_login = l_login_id
                            , request_id = l_request_Id
                            , list_header_id = c1.list_header_id
                        where   inventory_item_id = c2.inventory_item_id;
                        l_count_items := l_count_items + 1;
                    exception
                    when others then
                        Write_log ('Could not update inventory_item_id '||c2.inventory_item_id); 
                    end;
                    
                end loop;
                Write_log ('Reset '||l_count_items||' items in staging');
                
                commit;
                
            end if;
        elsif c1.product_attribute_context = 'ITEM' and c1.product_attribute = 'PRICING_ATTRIBUTE1' then
            l_exists := 0;
            begin
                select  count(*)
                into    l_exists
                from    xxwc.xxwc_qp_wght_ntl_avg_cost_stg
                where   inventory_item_id = to_number(c1.product_attr_value)
                and     list_header_id = c1.list_header_id
                and     request_id = l_request_Id;
            exception
            when others then
                l_exists := 0;
            end;    
            
            if l_exists = 0 then
                begin
                    insert into xxwc.xxwc_qp_wght_ntl_avg_cost_stg (
                        inventory_item_id
                        , creation_date
                        , created_by
                        , last_update_date
                        , last_updated_by
                        , last_update_login
                        , request_id
                        , list_header_id
                    ) values (
                        to_number(c1.product_attr_value)
                        , sysdate
                        , fnd_global.user_id
                        , sysdate
                        , fnd_global.user_id
                        , l_login_id
                        , l_request_Id
                        , c1.list_header_id
                    );
                    l_count_items := l_count_items + 1;
                exception
                when others then
                    Write_log ('Could not insert inventory_item_id '||to_number(c1.product_attr_value)); 
                end;
            else
                Write_log ('Item already in staging. inventory_item_id '||to_number(c1.product_attr_value));
            end if;
            
        else
            Write_log ('Invalid Pricing Attribute in '||c1.name||'/'||c1.list_line_no);
        end if;
     
        Write_log (' ');
    end loop;
    Write_log (' ');
    commit; 
    
    Write_log ('*************************************************************');
    Write_log ('Removing items not associated to this price list and request run');
    
    begin
        delete from xxwc.xxwc_qp_wght_ntl_avg_cost_stg
        where   list_header_id = p_list_header_id
        and     wght_ntl_avg_cost is not null
        and     request_id != l_request_Id;
    exception
    when others then
        Write_log ('Could not delete obsolete items ... ');
    end;
    Write_log (' ');
    commit;
    
    Write_log ('*************************************************************');
    Write_log ('Recalculating Weighted National Average Costs');
    l_count := 0;
    for c3 in cur_recalc_items (l_request_id)
    loop
        exit when cur_recalc_items%notfound;
        l_total_qoh_cost    := 0;
        l_total_qoh         := 0;
        
        begin
            select  SUM( xxwc_ascp_scwb_pkg.get_on_hand (msib.inventory_item_id, msib.organization_id, 'H') *
                           cst_cost_api.get_item_cost (1.0, msib.inventory_item_id, msib.organization_id)) tot_qoh_cost
                    , SUM( xxwc_ascp_scwb_pkg.get_on_hand (msib.inventory_item_id, msib.organization_id, 'H')) total_qoh
            into    l_total_qoh_cost
                    , l_total_qoh
            from    apps.mtl_system_items_b msib
            where   msib.inventory_item_id = c3.inventory_item_id
            and     organization_id != 222
            and     nvl(xxwc_ascp_scwb_pkg.get_on_hand (msib.inventory_item_id
                                                      , msib.organization_id
                                                      , 'H'),0) > 0 
            and     nvl(cst_cost_api.get_item_cost (1.0
                                                 , msib.inventory_item_id
                                                 , msib.organization_id),0) > 0                                         
            group by msib.inventory_item_id;
            
            begin
                update  xxwc.xxwc_qp_wght_ntl_avg_cost_stg
                set     wght_ntl_avg_cost = round((l_total_qoh_cost/l_total_qoh),5)
                        , last_updated_by = fnd_global.user_id
                        , last_update_date = sysdate
                        , last_update_login = l_login_id
                where   inventory_item_id = c3.inventory_item_id;
                
                l_count := l_count + 1;
            exception
            when others then
                Write_log ('Could not update weighted national avg cost for item number '||c3.item_number||'. inventory_item_id '||c3.inventory_item_id);
                Write_log ('Error: '||SQLERRM);
            end;
        exception
        when no_data_found then
            Write_log ('No data found for item '||c3.item_number||'. Could be 0 qoh or 0 avg cost. Removing from staging table');
            
            begin
                delete from xxwc.xxwc_qp_wght_ntl_avg_cost_stg
                where   inventory_item_id = c3.inventory_item_id
                and     list_header_id = c3.list_header_id
                and     request_id = l_request_Id;
            exception
            when others then
                Write_log ('Could not delete item number '||c3.item_number||'. inventory_item_id '||c3.inventory_item_id);
            end;
            
        when others then
            l_total_qoh_cost    := 0;
            l_total_qoh         := 0;
            Write_log ('Could not calculate qoh and cost for item number '||c3.item_number||'. inventory_item_id '||c3.inventory_item_id);
            Write_log ('Error: '||SQLERRM);
        end; 
    
    end loop;
    Write_log ('Recalculated Cost for '||l_count||' items');
    
    commit;
    
EXCEPTION
WHEN OTHERS THEN
    l_error_message2 := 'xxwc_calc_wght_ntl_avg_cost '
                || 'Error_Stack...'
                || DBMS_UTILITY.format_error_stack ()
                || ' Error_Backtrace...'
                || DBMS_UTILITY.format_error_backtrace ();

    xxcus_error_pkg.xxcus_error_main_api (
        p_called_from         => 'xxwc_qp_market_price_util_pkg.xxwc_calc_wght_ntl_avg_cost'
       ,p_calling             => 'xxwc_qp_market_price_util_pkg.xxwc_calc_wght_ntl_avg_cost'
       ,p_request_id          => l_request_id
       ,p_ora_error_msg       => l_error_message2
       ,p_error_desc          => 'Error running XXWC QP Recalculate Weighted National Avg Costs'
       ,p_distribution_list   => 'HDSOracleDevelopers@hdsupply.com'
       ,p_module              => 'QP');

    write_log('Error in main xxwc_calc_wght_ntl_avg_cost: '||l_error_message2);
    
    errmsg := l_error_message2;
    retcode := '2';   
   
END xxwc_calc_wght_ntl_avg_cost;

-- 05/13/2013 CG: Adding Maintenance Tool Processor
PROCEDURE xxwc_qp_maint_processor (RETCODE      OUT NUMBER
                                   , ERRMSG     OUT VARCHAR2
                                   , P_FILE_ID  IN NUMBER)
IS
    l_msg_data      VARCHAR2 (4000);
    l_error_message2    clob;
    
    -- File Processing Variables
    l_file_id       number :=  P_FILE_ID;    
    l_file          BLOB;
    l_file_name     VARCHAR2 (256);
    i               INTEGER := 1; -- Row counter
    j               INTEGER := 1; -- Column Counter
    is_file_open    INTEGER;
    l_hdr           INTEGER;
    l_line          INTEGER;

    l_comma_count   NUMBER;
    l_offset        NUMBER := 1;
    l_eol_pos       NUMBER := 32767;
    l_amount        NUMBER;
    l_file_len      NUMBER := DBMS_LOB.getlength (l_file);
    l_buffer        RAW (30000);
    
    l_price_list_header_tbl_type     xxwc_qp_market_price_util_pkg.price_list_header_tbl_type     := xxwc_qp_market_price_util_pkg.G_MISS_PRICE_LIST_HEADER_TBL; 
    l_maint_tool_data_tbl_type       xxwc_qp_market_price_util_pkg.maint_tool_data_tbl_type := xxwc_qp_market_price_util_pkg.G_MISS_MAINT_TOOL_DATA_TBL;
    l_data          VARCHAR2(240);
    l_item_number   VARCHAR2(40);
    l_is_number     VARCHAR2(1);
   
    l_start_time    number;  
    l_end_time      number;
    l_conc_req_id   number := fnd_global.conc_request_id;
    l_login_id      number := fnd_global.login_id;
    -- File Processing Variables
    
    -- QP Pricing API Variables/Cursors
    cursor  cur_price_lists ( p_file_id number
                              , p_request_id number) is
            select  distinct(prl_name) price_list_name
            from    xxwc.XXWC_QP_MAINT_TOOL_LINE_STG
            where   status = 'NEW'
            and     request_id = p_request_id
            and     file_id = p_file_id;
   
    cursor  cur_list_lines (p_prl_name varchar2
                            , p_file_id number
                            , p_request_id number) is
            select  x1.rowid rid
                    , x1.*
            from    xxwc.XXWC_QP_MAINT_TOOL_LINE_STG x1
            where   x1.prl_name = p_prl_name            
            and     x1.status = 'NEW'  
            and     request_id = p_request_id
            and     file_id = p_file_id  
            and     (list_price is not null or obsolete_item is not null)      
            order by x1.item_number;
    
    l_cnt                           NUMBER;
    l                               NUMBER; -- i
    m                               NUMBER; -- j
    n                               NUMBER; -- k       
    l_api_version_number            NUMBER         := 1.0;
    l_init_msg_list                 VARCHAR2(10)   := FND_API.G_FALSE;
    l_return_values                 VARCHAR2(10)   := FND_API.G_FALSE;
    l_commit                        VARCHAR2(10)   := FND_API.G_TRUE;
    l_x_return_status               VARCHAR2(255)  := 'S';
    l_x_msg_count                   NUMBER;
    l_x_msg_data                    VARCHAR2(255);
    g_user_id                       NUMBER;
    l_price_list_rec                QP_PRICE_LIST_PUB.PRICE_LIST_REC_TYPE          := QP_PRICE_LIST_PUB.G_MISS_PRICE_LIST_REC;
    l_price_list_val_rec            QP_PRICE_LIST_PUB.PRICE_LIST_VAL_REC_TYPE      := QP_PRICE_LIST_PUB.G_MISS_PRICE_LIST_VAL_REC;
    l_price_list_line               QP_PRICE_LIST_PUB.PRICE_LIST_LINE_REC_TYPE    := QP_PRICE_LIST_PUB.G_MISS_PRICE_LIST_LINE_REC;
    l_price_list_line_tbl           QP_PRICE_LIST_PUB.PRICE_LIST_LINE_TBL_TYPE     := QP_PRICE_LIST_PUB.G_MISS_PRICE_LIST_LINE_TBL; 
    l_price_list_line_val_tbl       QP_PRICE_LIST_PUB.PRICE_LIST_LINE_VAL_TBL_TYPE := QP_PRICE_LIST_PUB.G_MISS_PRICE_LIST_LINE_VAL_TBL;
    l_qualifiers_tbl                QP_QUALIFIER_RULES_PUB.QUALIFIERS_TBL_TYPE     := QP_PRICE_LIST_PUB.G_MISS_QUALIFIERS_TBL;
    l_qualifiers_val_tbl            QP_QUALIFIER_RULES_PUB.QUALIFIERS_VAL_TBL_TYPE := QP_PRICE_LIST_PUB.G_MISS_QUALIFIERS_VAL_TBL;
    l_pricing_att_rec               QP_PRICE_LIST_PUB.PRICING_ATTR_REC_TYPE := QP_PRICE_LIST_PUB.G_MISS_PRICING_ATTR_REC;
    l_pricing_attr_tbl              QP_PRICE_LIST_PUB.PRICING_ATTR_TBL_TYPE        := QP_PRICE_LIST_PUB.G_MISS_PRICING_ATTR_TBL;
    l_pricing_attr_val_tbl_type     QP_PRICE_LIST_PUB.PRICING_ATTR_VAL_TBL_TYPE    := QP_PRICE_LIST_PUB.G_MISS_PRICING_ATTR_VAL_TBL;
    l_x_price_list_rec              QP_PRICE_LIST_PUB.PRICE_LIST_REC_TYPE;
    l_x_price_list_val_rec          QP_PRICE_LIST_PUB.PRICE_LIST_VAL_REC_TYPE;
    l_x_price_list_line_tbl         QP_PRICE_LIST_PUB.PRICE_LIST_LINE_TBL_TYPE;
    l_x_price_list_line_val_tbl     QP_PRICE_LIST_PUB.PRICE_LIST_LINE_VAL_TBL_TYPE;
    l_x_qualifiers_tbl              QP_QUALIFIER_RULES_PUB.QUALIFIERS_TBL_TYPE;
    l_x_qualifiers_val_tbl          QP_QUALIFIER_RULES_PUB.QUALIFIERS_VAL_TBL_TYPE;
    l_x_pricing_attr_tbl            QP_PRICE_LIST_PUB.PRICING_ATTR_TBL_TYPE;
    l_x_pricing_attr_val_tbl        QP_PRICE_LIST_PUB.PRICING_ATTR_VAL_TBL_TYPE;
    
    l_error_message_list            ERROR_HANDLER.ERROR_TBL_TYPE;
    l_price_list                    VARCHAR2(3);
    l_inventory_item_id             NUMBER;
    l_primary_uom_code              VARCHAR2(3);
    l_process                       VARCHAR2(1);
    gpr_msg_data                    VARCHAR2(2000);
    l_valid                         VARCHAR2(1);
    l_warehouse_id                  VARCHAR2(30);
    l_structure_name                VARCHAR2(240);
    l_effective_start_date          DATE;
    
    x_request_id                    NUMBER;
    l_ntl_setup_prl_processed       NUMBER;
    l_commit_size                   NUMBER;
    l_submit_time                   VARCHAR2(240);
    
    -- QP Pricing API Variables/Cursors

    -- 08/02/2013 CG: TMS 20130729-00576: Added to handle upload of CFP flag for Market National PRL
    l_cfp_flag                      VARCHAR2(1);
    l_list_header_id                NUMBER;    
    
    
BEGIN
    write_output('Starting Maintenance Tool Processing for File ID: '||l_file_id);
    write_output('---------------------------------------------------------------');
    
    l_Start_time := DBMS_UTILITY.get_time;
    write_log ('l_Start_time: '||l_Start_time);
   
    BEGIN
        SELECT  file_data
                , file_name
        INTO    l_file
                , l_file_name
        FROM    fnd_lobs
        WHERE   file_id = l_file_id; 
    EXCEPTION
    WHEN OTHERS THEN
        l_file := null;
        l_file_name := null;
        write_log ('File Not Found. Error: '||SQLERRM);
        l_msg_data := substr(SQLERRM, 1, 4000);
        errmsg := l_msg_data;
        retcode := '2';  
        raise;
    END;

    is_file_open := DBMS_LOB.ISOPEN (l_file);
    IF is_file_open = 1
    THEN
        DBMS_LOB.close (l_file);
    END IF;

    BEGIN
        DBMS_LOB.open (l_file, DBMS_LOB.lob_readonly);
        is_file_open        := DBMS_LOB.ISOPEN (l_file);
    EXCEPTION
    WHEN OTHERS  THEN
        write_log ('Could not open file. Error: '||SQLERRM);
        l_msg_data := substr(SQLERRM, 1, 4000);
        errmsg := l_msg_data;
        retcode := '2';  
        raise;
    END;

    l_file_len  := DBMS_LOB.getlength (l_file);

    write_output ('File Name '||l_file_name||' and length ' ||l_file_len);
   
    write_output ('Starting File read and parsing from LOB...');
    l_price_list_header_tbl_type := xxwc_qp_market_price_util_pkg.G_MISS_PRICE_LIST_HEADER_TBL; 
    l_maint_tool_data_tbl_type := xxwc_qp_market_price_util_pkg.G_MISS_MAINT_TOOL_DATA_TBL;
    l_hdr           := 1;
    l_line          := 1;
    l_data          := null;
    WHILE (l_offset < l_file_len)
    LOOP
        -- i: Row Counter. Reset Per File
        -- j: Column Counter. Reset Per row
        
        -- Finding EOL position  for new line/line feed ascii 0A
        l_eol_pos      := DBMS_LOB.INSTR (l_file, '0A', 1, i);
        
        -- Calculating how many chars to pull in buffer based on EOL position and offset
        IF i = 1 THEN
            l_amount     := l_eol_pos - 1;
        ELSE
            l_amount     := l_eol_pos - l_offset;
        END IF;

        -- Reading first record from LOB file
        DBMS_LOB.read (l_file, l_amount, l_offset, l_buffer);
      
        -- From the first record pulled retrieve the number of columns based on comma count
        IF i = 1 THEN
            SELECT   regexp_count (REPLACE (UTL_RAW.cast_to_varchar2 (l_buffer), CHR (13), NULL), ',')
            INTO   l_comma_count
            FROM   DUAL;

            write_log ('l_comma_count: ' || l_comma_count);
            write_log (' ');            
        END IF;
        
        write_log ('Cycle #: ' || i);
        write_log ('l_offset: ' || l_offset);
        write_log ('l_eol_pos: ' || l_eol_pos);
        write_log ('l_amount: ' || l_amount);
        write_log ('Line #' || i || ': '|| REPLACE (UTL_RAW.cast_to_varchar2 (l_buffer), CHR (13), NULL));
      
        j := 1;
        FOR j IN 1..l_comma_count
        LOOP
            -- Parsing Column Data is slightly different for the first column and the remaining cols
            IF j = 1 THEN
                write_log ('Line Split#' || j || ': '|| SUBSTR ( REPLACE ( UTL_RAW.cast_to_varchar2 (l_buffer), CHR (13), NULL )
                                                                 , 1
                                                                 , INSTR ( REPLACE ( UTL_RAW.cast_to_varchar2 (l_buffer), CHR (13), NULL), ',', 1, 1) - 1)
                           );
                                       
                l_data := SUBSTR ( REPLACE ( UTL_RAW.cast_to_varchar2 (l_buffer), CHR (13), NULL )
                                 , 1
                                 , INSTR ( REPLACE ( UTL_RAW.cast_to_varchar2 (l_buffer), CHR (13), NULL), ',', 1, 1) - 1);
            ELSE
                write_log ('Line Split#' || j || ': '|| SUBSTR (REPLACE (UTL_RAW.cast_to_varchar2 (l_buffer), CHR (13), NULL)
                                                                 , INSTR (REPLACE (UTL_RAW.cast_to_varchar2 (l_buffer), CHR (13), NULL), ',', 1, j - 1)+ 1
                                                                 , (INSTR (REPLACE (UTL_RAW.cast_to_varchar2 (l_buffer), CHR (13), NULL), ',', 1, j))
                                                                   - (INSTR (REPLACE (UTL_RAW.cast_to_varchar2 (l_buffer), CHR (13), NULL), ',', 1, j - 1)+ 1)
                                                                )
                           );
                                       
                l_data := SUBSTR (REPLACE (UTL_RAW.cast_to_varchar2 (l_buffer), CHR (13), NULL)
                                 , INSTR (REPLACE (UTL_RAW.cast_to_varchar2 (l_buffer), CHR (13), NULL), ',', 1, j - 1)+ 1
                                 , (INSTR (REPLACE (UTL_RAW.cast_to_varchar2 (l_buffer), CHR (13), NULL), ',', 1, j))
                                   - (INSTR (REPLACE (UTL_RAW.cast_to_varchar2 (l_buffer), CHR (13), NULL), ',', 1, j - 1)+ 1)
                                );            
            END IF;
         
            -- i = 1 is Row1 of the file and is the column headers and price list names
            -- will ignore column 1 or header of Item
            -- 08/02/2013 CG: TMS 20130729-00576: Added to handle CFP Flag    
            -- if i = 1 and (j > 1 or l_data != 'Item Number') then
            if i = 1 and j > 2 then -- or l_data != 'Item Number') then
                -- Populate record type of price list headers with the list of price lists in row 1
                l_price_list_header_tbl_type(l_hdr).name := l_data;
                l_hdr := l_hdr + 1;
            elsif i > 1 then
                -- All other rows are item and price list data per price list column
                -- The price list column can also be used to mark an item to be end dated on a price list with a D
                
                l_is_number := null;
                SELECT  (case when regexp_like(l_data, '^-?[[:digit:],.]*$') then 'Y' else 'N' end)
                into    l_is_number
                FROM    dual;
            
                -- J = 1 is Column 1, which will always be item number. Pulling once from Col 1 and reusing for all other cols which are item/prl combos
                -- 08/01/13 CG: TMS 20130729-00576 to accomodate numeric item numbers
                if j = 1 then -- and l_is_number = 'N' then
                    l_item_number := l_data; 
                
                -- 08/02/2013 CG: TMS 20130729-00576: Added to handle CFP Flag    
                elsif  j = 2 then
                    l_cfp_flag := substr(l_data,1,1);         
                
                -- elsif j > 1 then
                elsif j > 2 then 
                    -- All other columns are Prl/Item Combos to be loaded into staging
                    -- Pull from preloaded record set with col - 1, since col 1 is item header                    
                    
                    -- 08/02/2013 CG: TMS 20130729-00576: Added to handle CFP Flag
                    -- l_maint_tool_data_tbl_type(l_line).PRL_NAME             := trim(l_price_list_header_tbl_type(j-1).name);
                    l_maint_tool_data_tbl_type(l_line).PRL_NAME             := trim(l_price_list_header_tbl_type(j-2).name);
                    l_maint_tool_data_tbl_type(l_line).ITEM_NUMBER          := trim(replace(replace(l_item_number,'?',null),'''', null)); -- l_item_number
                    
                    -- Checking to see if the value pulled is list price or the obsolete marker                    
                    if l_is_number = 'N' then
                        l_maint_tool_data_tbl_type(l_line).OBSOLETE_ITEM    := substr(l_data,1,1);
                        l_maint_tool_data_tbl_type(l_line).LIST_PRICE       := null;
                    else
                        l_maint_tool_data_tbl_type(l_line).LIST_PRICE       := to_number(l_data);
                        l_maint_tool_data_tbl_type(l_line).OBSOLETE_ITEM    := null;
                    end if;
                    
                    -- 08/02/2013 CG: TMS 20130729-00576: Added to handle CFP Flag
                    l_list_header_id := null;
                    begin                    
                        select  list_header_id
                        into    l_list_header_id
                        from    apps.qp_list_headers_tl
                        where   name = trim(l_price_list_header_tbl_type(j-2).name) -- l_maint_tool_data_tbl_type(l_line).PRL_NAME
                        and     rownum = 1;
                    exception
                    when others then
                        l_list_header_id := null;
                    end;
                    
                    /*if l_list_header_id = G_MARKET_NTL_PRL_ID then
                        l_cfp_flag := l_cfp_flag;
                    else
                        l_cfp_flag := null;
                    end if;*/
                    if l_list_header_id != G_MARKET_NTL_PRL_ID then
                        l_cfp_flag := null;
                    end if;
                    -- 08/02/2013 CG:TMS 20130729-00576
                    
                    l_maint_tool_data_tbl_type(l_line).FILE_ID              := l_file_id;
                    l_maint_tool_data_tbl_type(l_line).CALL_FOR_PRICE       := l_cfp_flag; -- null;
                    l_maint_tool_data_tbl_type(l_line).LIST_HEADER_ID       := null;
                    l_maint_tool_data_tbl_type(l_line).LIST_LINE_ID         := null;
                    l_maint_tool_data_tbl_type(l_line).INVENTORY_ITEM_ID    := null;
                    l_maint_tool_data_tbl_type(l_line).STATUS               := 'NEW';
                    l_maint_tool_data_tbl_type(l_line).ERR_MESSAGE          := null;
                    l_maint_tool_data_tbl_type(l_line).REQUEST_ID           := l_conc_req_id;       
                    l_maint_tool_data_tbl_type(l_line).CREATED_BY           := fnd_global.user_id;
                    l_maint_tool_data_tbl_type(l_line).CREATION_DATE        := sysdate;
                    l_maint_tool_data_tbl_type(l_line).LAST_UPDATED_BY      := fnd_global.user_id;
                    l_maint_tool_data_tbl_type(l_line).LAST_UPDATE_DATE     := sysdate;
                    l_maint_tool_data_tbl_type(l_line).LAST_UPDATE_LOGIN    := l_login_id;
                    l_maint_tool_data_tbl_type(l_line).PRODUCT_ATTRIBUTE    := null;
                    l_maint_tool_data_tbl_type(l_line).PRODUCT_ATTR_VALUE   := null;
                    l_maint_tool_data_tbl_type(l_line).PRICE_TYPE           := null;
                    l_maint_tool_data_tbl_type(l_line).PRODUCT_ATTR_ID      := null;
                    l_maint_tool_data_tbl_type(l_line).PRICING_FORMULA_ID   := null;
                    
                    l_line := l_line + 1; -- This will be initialized per file and increase as it reads throught the LOB
                end if;
            
            end if;
         
        END LOOP;

        write_log (' ');
        l_offset := l_offset + l_amount + 1; -- Increasing offset for read. Starting position of next read
        i        := i + 1; -- Rowcounter increase
    END LOOP;
   
    DBMS_LOB.close (l_file);
   
    write_output ('LOB file read and parsing complete. Starting bulk insert into staging...');
   
    FORALL x in l_maint_tool_data_tbl_type.first..l_maint_tool_data_tbl_type.last
    insert into xxwc.XXWC_QP_MAINT_TOOL_LINE_STG
    values l_maint_tool_data_tbl_type(x);

    commit;   
 
    write_output ('Bulk insert to staging complete.');          
    
    l_end_time := DBMS_UTILITY.get_time;
    write_log ('l_end_time : '||l_end_time );
    write_log ('File read/parsing processing time: '||to_char(l_end_time-l_start_time));
    write_output ('File read/parsing processing time: '||to_char(l_end_time-l_start_time));
   
    write_output('---------------------------------------------------------------');
    write_output('Starting staging data processing and load to EBS');
    
    l_Start_time := DBMS_UTILITY.get_time;
    write_log ('l_Start_time: '||l_Start_time);
    
    oe_msg_pub.initialize;
    
    --mo_global.init ('ONT');
    fnd_global.apps_initialize(user_id        => fnd_global.user_id,
                               resp_id        => fnd_global.resp_id,
                               resp_appl_id   => fnd_global.resp_appl_id);
                               
    mo_global.set_policy_context ('S', fnd_global.org_id);                               
    
    g_user_id := fnd_global.user_id;
    IF(g_user_id = -1) THEN
        BEGIN
            SELECT user_id
            INTO   g_user_id 
            FROM   fnd_user
            WHERE  user_name = 'SYSADMIN';
        EXCEPTION
        WHEN OTHERS THEN
            g_user_id := -1;
        END;
    END IF;
    
    l_commit_size := 0;
    begin        
        select  (case when to_number(to_char(sysdate, 'HH24MI')) between 600 and 2200
                        -- 01/07/2014 CG: TMS 20131016-00375: Updated to use batch size profiles
                        -- then 1 --'Daytime'
                        then NVL(fnd_profile.value ('XXWC_QP_MAINT_DAY_BATCH_SIZE'),1) --'Daytime'
                        -- 08/01/13 CG: TMS 20130729-00576: Reduced to 500 to account for failed batches
                        -- else 1500 ---'Evening'
                        -- else 500 ---'Evening'
                        else NVL(fnd_profile.value ('XXWC_QP_MAINT_NIGHT_BATCH_SIZE'),500) -- 'Evening'
                end) time_frame
        into    l_commit_size
        from    dual;
    exception
    when others then
        l_commit_size := 500;
    end;
    
    for c1 in cur_price_lists (l_file_id, l_conc_req_id)
    loop
        exit when cur_price_lists%notfound;
        
        write_output (' ');
        write_output ('Processing Price list: '||c1.price_list_name);
        
        l_price_list_rec := QP_PRICE_LIST_PUB.G_MISS_PRICE_LIST_REC;
        l_process := null;
        BEGIN
            SELECT list_header_id
            INTO   l_price_list_rec.list_header_id
            FROM   apps.qp_list_headers
            WHERE  name = c1.price_list_name
            -- 07/01/2013 CG: Removed to allow the National Setup PRL to validate
            --AND    active_flag = 'Y'
            AND    SYSDATE BETWEEN nvl(start_date_active, SYSDATE - 1) AND nvl(end_date_active, SYSDATE + 1);
          
            l_price_list_rec.operation := QP_GLOBALS.G_OPR_UPDATE;          
            
            l_price_list_rec.last_updated_by    := g_user_id;
            l_price_list_rec.last_update_date   := SYSDATE;
            
            /*l_price_list_rec.currency_code      := 'USD';
            l_price_list_rec.list_type_code     := 'PRL';
            l_price_list_rec.rounding_factor    := -5;
            l_price_list_rec.name               := c1.price_list_name;
            l_price_list_rec.description        := c1.description;
            l_price_list_rec.start_date_active  := trunc(sysdate);
            l_price_list_rec.active_flag        := 'Y';
            l_price_list_rec.automatic_flag     := 'Y';
            l_price_list_rec.mobile_download    := 'N';
            l_price_list_rec.context            := '162';
            l_price_list_rec.global_flag        := 'Y';
            l_price_list_rec.source_system_code := 'QP';
            l_price_list_rec.pte_code           := 'ORDFUL';
            l_price_list_rec.attribute10        := 'Price List';*/
          
            l_process := 'Y';
        EXCEPTION
          WHEN OTHERS THEN
            write_output ('Price list '||c1.price_list_name||' does not exist will not process associated records.');
            l_process := 'N';
            
            Begin
                update  xxwc.XXWC_QP_MAINT_TOOL_LINE_STG
                set     status = 'INVALID_PRL'
                        , err_message = 'Price List does not exist'
                where   prl_name = c1.price_list_name
                and     file_id = l_file_id
                and     request_id = l_conc_req_id; 
            exception
            when others then
                null;
            end;
        END;
                
        if l_process = 'Y' then
        
            -- Validating and loading lines        
            i := 1;
            l_pricing_attr_tbl       := QP_PRICE_LIST_PUB.G_MISS_PRICING_ATTR_TBL;
            l_price_list_line_tbl    := QP_PRICE_LIST_PUB.G_MISS_PRICE_LIST_LINE_TBL;
            for c2 in cur_list_lines (c1.price_list_name, l_file_id, l_conc_req_id)
            loop
                exit when cur_list_lines%notfound;
                
                l_price_list_line   := QP_PRICE_LIST_PUB.G_MISS_PRICE_LIST_LINE_REC;
                l_pricing_att_rec   := QP_PRICE_LIST_PUB.G_MISS_PRICING_ATTR_REC;
                l_valid             := null;
                
                l_price_list_line.list_header_id            := l_price_list_rec.list_header_id;
                l_price_list_line.last_updated_by           := g_user_id;
                l_price_list_line.last_update_date          := sysdate;
                
                if c2.call_for_price is not null then
                    l_price_list_line.context              := '162';
                    l_price_list_line.attribute1            := c2.call_for_price; 
                end if;
                                                
                l_pricing_att_rec.list_header_id            := l_price_list_line.list_header_id;
                l_pricing_att_rec.last_updated_by           := g_user_id;
                l_pricing_att_rec.last_update_date          := sysdate;
                l_pricing_att_rec.price_list_line_index     := i;
                
                begin
                    l_inventory_item_id     := null;
                    l_primary_uom_code      := null;
                    select  msib.inventory_item_id
                            , msib.primary_uom_code
                    into    l_inventory_item_id
                            , l_primary_uom_code
                    from    apps.mtl_system_items_b msib
                            , apps.mtl_parameters mp
                    where   msib.segment1 = c2.item_number
                    and     msib.organization_id = mp.organization_id
                    and     mp.organization_code = 'MST';
                        
                    l_valid := 'Y';
                exception
                when others then
                    write_output('Invalid item number: '||c2.item_number||'. Error: '||SQLERRM);
                    l_valid := 'N';  
                    begin                    
                        update  xxwc.XXWC_QP_MAINT_TOOL_LINE_STG
                        set     status = status||'.INVALID_ITEM'
                                , err_message = err_message||'.Invalid Item'
                        where   rowid = c2.rid;
                    exception
                    when others then
                        null;
                    end;
                end;
                
                if l_valid = 'Y' then
                
                    -- Finding list line id    
                    l_effective_start_date := null;
                    begin
                        -- 08/08/2013 CG: TMS 20130107-00832: Added to handle dates differently for National and markets
                        if l_price_list_rec.list_header_id = G_MARKET_NTL_PRL_ID then
                            select  qll.list_line_id
                                    , qpa.list_line_id
                                    , qpa.pricing_attribute_id
                                    , qll.start_date_active
                            into    l_price_list_line.list_line_id
                                    , l_pricing_att_rec.list_line_id
                                    , l_pricing_att_rec.pricing_attribute_id
                                    , l_effective_start_date
                            from    apps.qp_list_lines qll
                                    , apps.qp_pricing_attributes qpa
                            where   qll.list_header_id = l_price_list_line.list_header_id
                            and     qll.list_line_type_code = 'PLL'
                            and     qll.list_header_id = qpa.list_header_id
                            and     qll.qualification_ind = qpa.qualification_ind
                            and     qll.pricing_phase_id = qpa.pricing_phase_id
                            and     qll.list_line_id = qpa.list_line_id
                            and     qpa.product_attribute_context = 'ITEM'
                            and     qpa.product_attribute = 'PRICING_ATTRIBUTE1'
                            and     qpa.product_attribute_datatype = 'C'
                            and     qpa.product_attr_value = to_char(l_inventory_item_id);
                        else
                            select  qll.list_line_id
                                    , qpa.list_line_id
                                    , qpa.pricing_attribute_id
                                    , qll.start_date_active
                            into    l_price_list_line.list_line_id
                                    , l_pricing_att_rec.list_line_id
                                    , l_pricing_att_rec.pricing_attribute_id
                                    , l_effective_start_date
                            from    apps.qp_list_lines qll
                                    , apps.qp_pricing_attributes qpa
                            where   qll.list_header_id = l_price_list_line.list_header_id
                            and     qll.list_line_type_code = 'PLL'
                            -- 08/01/13 CG: TMS 20130729-00576: NVLd to accomodate the Market National lines that dont have start date
                            -- and     qll.start_date_active <= trunc(sysdate)                        
                            and     nvl(qll.start_date_active, trunc(sysdate-1)) <= trunc(sysdate)
                            and     nvl(qll.end_date_active, trunc(sysdate)) >= trunc(sysdate)
                            and     qll.list_header_id = qpa.list_header_id
                            and     qll.qualification_ind = qpa.qualification_ind
                            and     qll.pricing_phase_id = qpa.pricing_phase_id
                            and     qll.list_line_id = qpa.list_line_id
                            and     qpa.product_attribute_context = 'ITEM'
                            and     qpa.product_attribute = 'PRICING_ATTRIBUTE1'
                            and     qpa.product_attribute_datatype = 'C'
                            and     qpa.product_attr_value = to_char(l_inventory_item_id);
                        end if;
                        
                        if c2.obsolete_item = 'D' then                        
                            if nvl(l_effective_start_date, trunc(sysdate)) < trunc(sysdate) then
                                l_price_list_line.end_date_active       := trunc(sysdate) - 1;
                            else
                                l_price_list_line.end_date_active       := trunc(sysdate);
                            end if;
                        else
                            l_price_list_line.operand               := nvl(c2.list_price,1);
                            l_price_list_line.arithmetic_operator   := 'UNIT_PRICE';
                            l_price_list_line.end_date_active       := NULL;
                            
                        end if;
                        
                        l_pricing_att_rec.product_attr_value        := to_char(l_inventory_item_id);
                        l_pricing_att_rec.product_uom_code          := l_primary_uom_code;
                        l_pricing_att_rec.product_attribute_context := 'ITEM';
                        l_pricing_att_rec.product_attribute         := 'PRICING_ATTRIBUTE1';
                        l_pricing_att_rec.product_attribute_datatype:= 'C';
                        
                        l_price_list_line.operation                 := QP_GLOBALS.G_OPR_UPDATE;
                        l_pricing_att_rec.operation                 := QP_GLOBALS.G_OPR_UPDATE;
                        
                        l_valid := 'Y';
                    exception
                    when no_data_found then
                    
                    IF --c2.obsolete_item != 'D' THEN--Added by Ram Talluri for TMS #20140407-00289 - Price list records are not correctly end dating
                       NVL(c2.obsolete_item,'Q') <> 'D' THEN--Added by Ram Talluri for TMS #20140411-00207
                        -- 08/01/13 CG: TMS 20130729-00576: Added to handle Market National PRL upload
                        if l_price_list_rec.list_header_id = G_MARKET_NTL_PRL_ID then                        
                            l_price_list_line.start_date_active         := NULL;
                            l_price_list_line.product_precedence        := 250;                        
                        else                        
                            l_price_list_line.start_date_active         := trunc(sysdate);
                            l_price_list_line.product_precedence        := 220;                            
                        end if;
                    
                        l_price_list_line.primary_uom_flag          := 'Y';
                        l_price_list_line.created_by                := g_user_id;
                        l_price_list_line.creation_date             := sysdate;
                        l_price_list_line.list_line_type_code       := 'PLL';
                        l_price_list_line.operand                   := c2.list_price;
                        l_price_list_line.arithmetic_operator       := 'UNIT_PRICE';
                    
                        l_pricing_att_rec.product_attr_value        := to_char(l_inventory_item_id);
                        l_pricing_att_rec.product_uom_code          := l_primary_uom_code;
                        l_pricing_att_rec.product_attribute_context := 'ITEM';
                        l_pricing_att_rec.product_attribute         := 'PRICING_ATTRIBUTE1';
                        l_pricing_att_rec.product_attribute_datatype:= 'C';
                        l_pricing_att_rec.created_by                := g_user_id;
                        l_pricing_att_rec.creation_date             := sysdate;                
                        l_pricing_att_rec.pricing_phase_id          := 1;
                    
                        l_pricing_att_rec.list_line_id              := FND_API.G_MISS_NUM;
                
                        SELECT qp_pricing_attributes_s.nextval
                        INTO   l_pricing_att_rec.pricing_attribute_id
                        FROM   dual;
                    
                        l_price_list_line.operation                 := QP_GLOBALS.G_OPR_CREATE;
                        l_pricing_att_rec.operation                 := QP_GLOBALS.G_OPR_CREATE;
                        
                        l_valid := 'Y';
                        --Added by Ram Talluri for TMS #20140407-00289 - Price list records are not correctly end dating
                     --ELSIF c2.obsolete_item = 'D' THEN
                     ELSIF NVL(c2.obsolete_item,'Q') = 'D' THEN  --Added by RamTalluri for TMS 20140411-00207
                     l_valid := 'N';
                        write_output('ITEM: '||c2.item_number||'. Error: '||'Item is either already end dated or does not exist in the price list');
                        begin                    
                            update  xxwc.XXWC_QP_MAINT_TOOL_LINE_STG
                            set     status = 'ERROR'
                                    , err_message = 'Item is either already end dated or does not exist in the price list'
                            where   rowid = c2.rid;
                        exception
                        when others then
                            null;
                        end;
                     ELSE 
                     NULL;
                     END IF;
                     --end by Ram Talluri for TMS #20140407-00289 - Price list records are not correctly end dating
                    when others then
                        l_valid := 'N';
                        write_output('Invalid QP Line for item: '||c2.item_number||'. Error: '||SQLERRM);
                        begin                    
                            update  xxwc.XXWC_QP_MAINT_TOOL_LINE_STG
                            set     status = status||'.INVALID_QP_LINE'
                                    , err_message = err_message||'.Invalid QP Line'
                            where   rowid = c2.rid;
                        exception
                        when others then
                            null;
                        end;
                        
                    end;
                end if;
                                
                if l_valid = 'Y' then 
                    l_pricing_attr_tbl(i)    := l_pricing_att_rec;
                    l_price_list_line_tbl(i) := l_price_list_line;
                    i := i + 1;
                    
                    begin
                        update  xxwc.XXWC_QP_MAINT_TOOL_LINE_STG
                        set     status = 'IN_PROCESS'
                        where   rowid = c2.rid;
                    exception
                    when others then
                        null;
                    end;
                    
                end if;
                
                IF(i > l_commit_size) THEN
                    write_output ( 'Adding ' || i || ' price lines to price list');
                    
                    qp_price_list_pub.process_price_list( p_api_version_number      => l_api_version_number,
                                                          p_init_msg_list           => l_init_msg_list,
                                                          p_return_values           => l_return_values,
                                                          p_commit                  => l_commit,
                                                          x_return_status           => l_x_return_status,
                                                          x_msg_count               => l_x_msg_count,
                                                          x_msg_data                => l_x_msg_data,
                                                          p_price_list_rec          => l_price_list_rec,
                                                          p_price_list_val_rec      => l_price_list_val_rec,
                                                          p_price_list_line_tbl     => l_price_list_line_tbl,
                                                          p_price_list_line_val_tbl => l_price_list_line_val_tbl,
                                                          p_qualifiers_tbl          => l_qualifiers_tbl,
                                                          p_qualifiers_val_tbl      => l_qualifiers_val_tbl,
                                                          p_pricing_attr_tbl        => l_pricing_attr_tbl,
                                                          p_pricing_attr_val_tbl    => l_pricing_attr_val_tbl_type,
                                                          x_price_list_rec          => l_x_price_list_rec,
                                                          x_price_list_val_rec      => l_x_price_list_val_rec,
                                                          x_price_list_line_tbl     => l_x_price_list_line_tbl,
                                                          x_price_list_line_val_tbl => l_x_price_list_line_val_tbl,
                                                          x_qualifiers_tbl          => l_x_qualifiers_tbl, 
                                                          x_qualifiers_val_tbl      => l_x_qualifiers_val_tbl,
                                                          x_pricing_attr_tbl        => l_x_pricing_attr_tbl,
                                                          x_pricing_attr_val_tbl    => l_x_pricing_attr_val_tbl);
                        
                    write_output ('Return status = ' || l_x_return_status);
                      
                    IF(l_x_return_status <> 'S') THEN
                        --  Error Processing
                        write_output ('  Message Count       = ' || l_x_msg_count);
                      
                        FOR K IN 1 .. l_x_msg_count LOOP 
                          gpr_msg_data := oe_msg_pub.get(p_msg_index => k, 
                                                         p_encoded => 'F'); 
                          write_output (TO_CHAR(i)||' ERR TEXT  '|| gpr_msg_data);        
                        END LOOP;
                        
                        -- 08/01/2013 CG: TMS 20130729-00576: Changed to prevent exit of program and processing of remaining records
                        -- RETURN;
                        begin
                            update  xxwc.XXWC_QP_MAINT_TOOL_LINE_STG
                            set     status = 'ERROR'
                            where   prl_name = c1.price_list_name
                            and     status = 'IN_PROCESS'
                            and     file_id = l_file_id
                            and     request_id = l_conc_req_id;
                        exception
                        when others then
                            null;
                        end;
                    
                    ELSE
                        begin
                            update  xxwc.XXWC_QP_MAINT_TOOL_LINE_STG
                            set     status = 'LOADED'
                            where   prl_name = c1.price_list_name
                            and     status = 'IN_PROCESS'
                            and     file_id = l_file_id
                            and     request_id = l_conc_req_id;
                        exception
                        when others then
                            null;
                        end;
                    END IF;
                      
                    l_price_list_rec.operation := QP_GLOBALS.G_OPR_UPDATE;
                    COMMIT; 
                        
                    i := 1;
                    l_pricing_attr_tbl       := QP_PRICE_LIST_PUB.G_MISS_PRICING_ATTR_TBL;
                    l_price_list_line_tbl    := QP_PRICE_LIST_PUB.G_MISS_PRICE_LIST_LINE_TBL; 
                END IF;
                
            end loop;
            
            IF(i NOT IN (0, 1)) THEN
                write_output ('Adding ' || i || ' price lines to price list');
                
                qp_price_list_pub.process_price_list(  p_api_version_number      => l_api_version_number,
                                                       p_init_msg_list           => l_init_msg_list,
                                                       p_return_values           => l_return_values,
                                                       p_commit                  => l_commit,
                                                       x_return_status           => l_x_return_status,
                                                       x_msg_count               => l_x_msg_count,
                                                       x_msg_data                => l_x_msg_data,
                                                       p_price_list_rec          => l_price_list_rec,
                                                       p_price_list_val_rec      => l_price_list_val_rec,
                                                       p_price_list_line_tbl     => l_price_list_line_tbl,
                                                       p_price_list_line_val_tbl => l_price_list_line_val_tbl,
                                                       p_qualifiers_tbl          => l_qualifiers_tbl,
                                                       p_qualifiers_val_tbl      => l_qualifiers_val_tbl,
                                                       p_pricing_attr_tbl        => l_pricing_attr_tbl,
                                                       p_pricing_attr_val_tbl    => l_pricing_attr_val_tbl_type,
                                                       x_price_list_rec          => l_x_price_list_rec,
                                                       x_price_list_val_rec      => l_x_price_list_val_rec,
                                                       x_price_list_line_tbl     => l_x_price_list_line_tbl,
                                                       x_price_list_line_val_tbl => l_x_price_list_line_val_tbl,
                                                       x_qualifiers_tbl          => l_x_qualifiers_tbl, 
                                                       x_qualifiers_val_tbl      => l_x_qualifiers_val_tbl,
                                                       x_pricing_attr_tbl        => l_x_pricing_attr_tbl,
                                                       x_pricing_attr_val_tbl    => l_x_pricing_attr_val_tbl);
                  
                write_output ('Return status = ' || l_x_return_status);
                
                IF(l_x_return_status <> 'S') THEN
                    --  Error Processing
                    write_output ('  Message Count       = ' || l_x_msg_count);
              
                    FOR K IN 1 .. l_x_msg_count 
                    LOOP 
                        gpr_msg_data := oe_msg_pub.get(p_msg_index => k, p_encoded => 'F'); 
                        write_output (TO_CHAR(i)||' ERR TEXT  '|| gpr_msg_data);        
                    END LOOP;
                    
                    -- 08/01/2013 CG: TMS 20130729-00576: Changed to prevent exit of program and processing of remaining records
                    -- RETURN;
                    begin
                        update  xxwc.XXWC_QP_MAINT_TOOL_LINE_STG
                        set     status = 'ERROR'
                        where   prl_name = c1.price_list_name
                        and     status = 'IN_PROCESS'
                        and     file_id = l_file_id
                        and     request_id = l_conc_req_id;
                    exception
                    when others then
                        null;
                    end;
                
                ELSE
                    begin
                        update  xxwc.XXWC_QP_MAINT_TOOL_LINE_STG
                        set     status = 'LOADED'
                        where   prl_name = c1.price_list_name
                        and     status = 'IN_PROCESS'
                        and     file_id = l_file_id
                        and     request_id = l_conc_req_id;
                    exception
                    when others then
                        null;
                    end;
                    
                END IF;
                
                l_price_list_rec.operation := QP_GLOBALS.G_OPR_UPDATE;
                
                COMMIT;
            END IF;
            
            IF (l_x_return_status = 'S') THEN
                -- updating loading pricelist lines
                update  xxwc.XXWC_QP_MAINT_TOOL_LINE_STG
                set     status = 'LOADED'
                where   prl_name = c1.price_list_name
                and     status = 'NEW'
                and     file_id = l_file_id
                and     request_id = l_conc_req_id;
            END IF;                                
           
        END IF; -- End if for l_process flag
        
    end loop;
    
    begin
        update  XXWC.XXWC_QP_MAINT_TOOL_STG
        set     status = 'Processed'
                , last_update_date = sysdate
                , last_updated_by = fnd_global.user_id
                , last_update_login = l_login_id 
        where   file_id = l_file_id
        and     processing_request_id = l_conc_req_id;
    exception
    when others then
        null;
    end;
    
    commit;    
    
    write_output('Completed data processing and load to EBS');
    l_end_time := DBMS_UTILITY.get_time;
    write_log ('l_end_time : '||l_end_time );
    write_log ('QP Processing Time: '||to_char(l_end_time-l_start_time));
    write_output ('QP Processing Time: '||to_char(l_end_time-l_start_time));
    
    -- 08/01/13 CG: TMS 20130729-00576: Removing, dont need to submit the Generate Market National anymore
    /*commit;
    
    -- Submitting Request to push from Setup to National Market After processing
    l_ntl_setup_prl_processed   := null;
    begin
        select  count(*)
        into    l_ntl_setup_prl_processed
        from    xxwc.XXWC_QP_MAINT_TOOL_LINE_STG x1
                , qp_list_headers qlh
        where   x1.status = 'LOADED'
        and     x1.file_id = l_file_id
        and     x1.request_id = l_conc_req_id
        and     x1.prl_name = qlh.name
        and     qlh.list_header_id = G_MARKET_NTL_STP_PRL_ID;
    exception
    when others then
        l_ntl_setup_prl_processed := 0;
    end;

    l_submit_time := null;
    begin        
        select  (case when to_number(to_char(sysdate, 'HH24MI')) between 600 and 2200
                        then (to_char(sysdate+1,'DD-MON-YY')||' 01:00:00')
                        else to_char(sysdate,'DD-MON-YY HH24:MI:SS')
                 end) time_frame
        into    l_submit_time
        from    dual;
    exception
    when others then
        l_submit_time := (to_char(sysdate+1,'DD-MON-YYYY')||' 01:00:00');
    end;

    if nvl(l_ntl_setup_prl_processed,0) > 0 then
        write_output(' ');
        write_output('Submitting concurrent request for XXWC QP Generate National Market Price List');
        
        x_request_id := fnd_request.submit_request
                                   (application      => 'XXWC',
                                    program          => 'XXWC_GEN_NTL_MARKET_PRL',
                                    description      => 'XXWC QP Generate National Market Price List',
                                    start_time       => l_submit_time,
                                    sub_request      => FALSE,
                                    argument1        => G_MARKET_NTL_STP_PRL_ID,    -- From PRL
                                    argument2        => G_MARKET_NTL_PRL_ID         -- To PRL
                                   );
        
        commit;
        
        write_output('Request ID: '||x_request_id||' has been submitted and will start on '||l_submit_time);
        
    end if;*/
    -- 08/01/13 CG: TMS 20130729-00576

EXCEPTION
WHEN NO_DATA_FOUND THEN
    BEGIN
        is_file_open             := NULL;
        is_file_open             := DBMS_LOB.ISOPEN (l_file);

        IF is_file_open = 1 THEN
            DBMS_LOB.close (l_file);
            COMMIT;
        END IF;
    END;
      
    l_msg_data := substr(SQLERRM, 1, 4000);
    Write_log ('xxwc_qp_maint_processor. No Data Found Excep: '||l_msg_data);
    errmsg := l_msg_data;
    retcode := '2';  
      
WHEN OTHERS THEN
    BEGIN
        is_file_open             := NULL;
        is_file_open             := DBMS_LOB.ISOPEN (l_file);

        IF is_file_open = 1 THEN
            DBMS_LOB.close (l_file);
            COMMIT;
        END IF;
    END;
    
    l_error_message2 := 'xxwc_qp_maint_processor '
                || 'Error_Stack...'
                || DBMS_UTILITY.format_error_stack ()
                || ' Error_Backtrace...'
                || DBMS_UTILITY.format_error_backtrace ();

    xxcus_error_pkg.xxcus_error_main_api (
        p_called_from         => 'xxwc_qp_market_price_util_pkg.xxwc_qp_maint_processor'
       ,p_calling             => 'xxwc_qp_market_price_util_pkg.xxwc_qp_maint_processor'
       ,p_request_id          => l_conc_req_id
       ,p_ora_error_msg       => l_error_message2
       ,p_error_desc          => 'Error running XXWC QP Maintenance Tool Processor'
       ,p_distribution_list   => 'HDSOracleDevelopers@hdsupply.com'
       ,p_module              => 'QP');

    write_log('Error in main xxwc_qp_maint_processor: '||l_error_message2);
    
    errmsg := l_error_message2;
    retcode := '2';   
    
END xxwc_qp_maint_processor;

-- 05/21/2013 CG: Used by the Maintenance Tool Extract Program
FUNCTION get_eisr_vendor_num (p_inventory_item_id in number
                              , p_organization_id in number) RETURN VARCHAR2  IS
    l_vendor_num varchar2(240);
BEGIN
    l_vendor_num := null;
  
    select  vendor_num
    into    l_vendor_num
    from    xxeis.eis_xxwc_po_isr_tab x1
    where   inventory_item_id = p_inventory_item_id
    and     organization_id = p_organization_id
    and     rownum = 1;
    
    return l_vendor_num;
exception
    when others then
        return 'X';
END get_eisr_vendor_num;

-- 05/21/2013 CG: Added to Generate the National Market Price list
PROCEDURE xxwc_gen_ntl_market_prl (RETCODE     OUT NUMBER
                                   , ERRMSG    OUT VARCHAR2
                                   , P_BASE_PRL_ID  IN NUMBER
                                   , P_DEST_PRL_ID  IN NUMBER) 
IS
    l_msg_data          VARCHAR2 (4000);
    l_error_message2    clob;
    l_conc_req_id       number := fnd_global.conc_request_id;
    
    
    -- Variables/Cursors to delete from price list
    cursor  cur_header is
        select  list_header_id
                , list_type_code
                , name
                , description
        from    apps.qp_list_headers
        where   list_header_id = P_DEST_PRL_ID;
    
    cursor cur_lines (p_list_header_id in number) is
        select  list_line_id
                , list_line_type_code
        from    apps.qp_list_lines
        where   list_header_id = p_list_header_id;
    -- Variables/Cursors to delete from price list
    
    -- Variables/Cursors to generate data into staging table from national market setup price list
    cursor cur_base_ntl_market_prl is
            select  qlh.list_header_id
                    , qlh.name
                    , qlh.description
                    , qll.list_line_id
                    , qll.list_line_no
                    , qll.product_precedence
                    , qll.arithmetic_operator
                    , qll.operand
                    , qll.qualification_ind
                    , qll.pricing_phase_id
                    , qll.attribute1 call_for_price
                    , qpa.pricing_attribute_id
                    , qpa.product_attribute_context
                    , qpa.product_attribute
                    , qpa.product_attr_value
                    , qpa.product_uom_code
                    , (case 
                        when qpa.product_attribute = 'PRICING_ATTRIBUTE1' then 1    -- Item Number
                        when qpa.product_attribute = 'PRICING_ATTRIBUTE27' then 2   -- Pricing Category
                        when qpa.product_attribute = 'PRICING_ATTRIBUTE2' then 3    -- Item Cat Class
                        else 4
                       end) order_sequence
            from    apps.qp_list_headers qlh
                    , apps.qp_list_lines qll
                    , apps.qp_pricing_attributes qpa
            where   qlh.list_header_id = P_BASE_PRL_ID
            and     qlh.version_no = 1
            and     qlh.list_type_code = 'PRL'
            and     qlh.start_date_active <= trunc(sysdate)
            and     nvl(qlh.end_date_active, trunc(sysdate)) >= trunc(sysdate)
            and     qlh.list_header_id = qll.list_header_id
            and     qll.list_line_type_code = 'PLL'
            and     qll.start_date_active <= trunc(sysdate)
            and     nvl(qll.end_date_active, trunc(sysdate)) >= trunc(sysdate)
            and     qll.list_header_id = qpa.list_header_id
            and     qll.qualification_ind = qpa.qualification_ind
            and     qll.pricing_phase_id = qpa.pricing_phase_id
            and     qll.list_line_id = qpa.list_line_id
            order by (case 
                        when qpa.product_attribute = 'PRICING_ATTRIBUTE1' then 1
                        when qpa.product_attribute = 'PRICING_ATTRIBUTE27' then 2
                        when qpa.product_attribute = 'PRICING_ATTRIBUTE2' then 3
                        else 4
                       end );
    
    cursor cur_category_items (p_category_set_id number
                               , p_category_id number) is
    select  distinct(mic.inventory_item_id) inventory_item_id
            , msib.primary_uom_code
    from    apps.mtl_item_categories mic
            , apps.mtl_system_items_b msib
    where   mic.organization_id = 222
    and     mic.category_set_id = p_category_set_id
    and     mic.category_id = p_category_id
    and     mic.inventory_item_id = msib.inventory_item_id
    and     mic.organization_id = msib.organization_id
    and     not exists (select  'Not Loaded'
                        from    XXWC.XXWC_NTL_MARKET_PRL_UPD_STG x1
                        where   x1.inventory_item_id = mic.inventory_item_id);
    
    l_exists_in_stg                         NUMBER;
    l_primary_uom_code                      VARCHAR2(3);
    l_category_set_id                       NUMBER;
    l_wght_ntl_avg_cost                     NUMBER;
    l_new_prl_value                         NUMBER;
    -- Variables/Cursors to generate data into staging table from national market setup price list
    
    -- Variables/Cursors to update the national market price list
    cursor cur_ntl_market_stg is
        select  x1.rowid rid
                , x1.*  
        from    XXWC.XXWC_NTL_MARKET_PRL_UPD_STG x1
        where   x1.status is null;
        
    -- Variables/Cursors to update the national market price list
    
    l_api_version_number                    NUMBER         := 1.0;
    l_init_msg_list                         VARCHAR2(10)   := FND_API.G_FALSE;
    l_return_values                         VARCHAR2(10)   := FND_API.G_FALSE;
    l_commit                                VARCHAR2(10)   := FND_API.G_TRUE;
    l_x_return_status                       VARCHAR2(255)  := 'S';
    l_x_msg_count                           NUMBER;
    l_x_msg_data                            VARCHAR2(2000);
    l_price_list_rec                        QP_PRICE_LIST_PUB.Price_List_Rec_Type; 
    l_price_list_val_rec                    QP_PRICE_LIST_PUB.Price_List_Val_Rec_Type; 
    l_price_list_line_tbl                   QP_PRICE_LIST_PUB.Price_List_Line_Tbl_Type; 
    l_price_list_line_val_tbl               QP_PRICE_LIST_PUB.Price_List_Line_Val_Tbl_Type; 
    l_qualifiers_tbl                        QP_Qualifier_Rules_Pub.Qualifiers_Tbl_Type; 
    l_qualifiers_val_tbl                    QP_Qualifier_Rules_Pub.Qualifiers_Val_Tbl_Type; 
    l_pricing_attr_tbl                      QP_PRICE_LIST_PUB.Pricing_Attr_Tbl_Type; 
    l_pricing_attr_val_tbl                  QP_PRICE_LIST_PUB.Pricing_Attr_Val_Tbl_Type; 
    l_x_price_list_rec                      QP_PRICE_LIST_PUB.Price_List_Rec_Type; 
    l_x_price_list_val_rec                  QP_PRICE_LIST_PUB.Price_List_Val_Rec_Type; 
    l_x_price_list_line_tbl                 QP_PRICE_LIST_PUB.Price_List_Line_Tbl_Type; 
    l_x_price_list_line_val_tbl             QP_PRICE_LIST_PUB.Price_List_Line_Val_Tbl_Type; 
    l_x_qualifiers_tbl                      QP_Qualifier_Rules_Pub.Qualifiers_Tbl_Type; 
    l_x_qualifiers_val_tbl                  QP_Qualifier_Rules_Pub.Qualifiers_Val_Tbl_Type; 
    l_x_pricing_attr_tbl                    QP_PRICE_LIST_PUB.Pricing_Attr_Tbl_Type; 
    l_x_pricing_attr_val_tbl                QP_PRICE_LIST_PUB.Pricing_Attr_Val_Tbl_Type; 

    K number := 1; 
    j number := 1; 
    l_count number; 
    l_nums  number;   
    l_item_number varchar2(40);
    l_cfp_flag  varchar2(1);
    l_commit_size number;
    
BEGIN
    write_output('Starting Generation of National Market Price List');
    write_output('---------------------------------------------------------------');

    oe_msg_pub.initialize;
    
    --mo_global.init ('ONT');
    fnd_global.apps_initialize(user_id        => fnd_global.user_id,
                               resp_id        => fnd_global.resp_id,
                               resp_appl_id   => fnd_global.resp_appl_id);
                               
    mo_global.set_policy_context ('S', fnd_global.org_id);                               
      
    l_commit_size := 0;
    begin        
        select  (case when to_number(to_char(sysdate, 'HH24MI')) between 600 and 2200
                    -- 01/07/2014 CG: TMS 20131016-00375: Updated to use batch size profiles
                    -- then 1 --'Daytime'
                    then NVL(fnd_profile.value ('XXWC_QP_MAINT_DAY_BATCH_SIZE'),1) --'Daytime'
                    -- 08/01/13 CG: TMS 20130729-00576: Reduced to 500 in case a batch fails
                    -- else 1500 ---'Evening'
                    -- else 500 ---'Evening'
                    else NVL(fnd_profile.value ('XXWC_QP_MAINT_NIGHT_BATCH_SIZE'),500) -- 'Evening'
                end) time_frame
        into    l_commit_size
        from    dual;
    exception
    when others then
        l_commit_size := 500;
    end;
                             
    -- 08/01/13 CG: TMS 20130729-00576: Removing clearing of Market National, going to line updates instead
    /*write_output('Deleting records from National Price List');
    
    for c0 in cur_header
    loop
        exit when cur_header%notfound;
    
        l_price_list_rec.list_header_id := c0.list_header_id; 
        l_price_list_rec.name := c0.name;  
        l_price_list_rec.list_type_code := c0.list_type_code; 
        l_price_list_rec.description := c0.description; 
        l_price_list_rec.operation := QP_GLOBALS.G_OPR_UPDATE;
        
        j := 1;
        l_count := 0;
        for c1 in cur_lines (c0.list_header_id)
        loop
            exit when cur_lines%notfound;
        
            l_price_list_line_tbl(j).list_header_id := c0.list_header_id; 
            l_price_list_line_tbl(j).list_line_id := c1.list_line_id; 
            l_price_list_line_tbl(j).list_line_type_code := c1.list_line_type_code; 
            l_price_list_line_tbl(j).operation := QP_GLOBALS.G_OPR_DELETE;
            
            j:= j + 1;
            l_count := l_count + 1;
             
            IF(l_count > l_commit_size) THEN
                l_nums := j-1;
                write_output('Deleting ' || l_nums || ' price lines to price list');
            
                qp_price_list_pub.process_price_list( p_api_version_number      => l_api_version_number,
                                                      p_init_msg_list           => l_init_msg_list,
                                                      p_return_values           => l_return_values,
                                                      p_commit                  => l_commit,
                                                      x_return_status           => l_x_return_status,
                                                      x_msg_count               => l_x_msg_count,
                                                      x_msg_data                => l_x_msg_data,
                                                      p_PRICE_LIST_rec          => l_price_list_rec, 
                                                      p_PRICE_LIST_LINE_tbl     => l_price_list_line_tbl, 
                                                      p_PRICING_ATTR_tbl        => l_pricing_attr_tbl, 
                                                      x_PRICE_LIST_rec          => l_x_price_list_rec, 
                                                      x_PRICE_LIST_val_rec      => l_x_price_list_val_rec, 
                                                      x_PRICE_LIST_LINE_tbl     => l_x_price_list_line_tbl, 
                                                      x_PRICE_LIST_LINE_val_tbl => l_x_price_list_line_val_tbl, 
                                                      x_QUALIFIERS_tbl          => l_x_qualifiers_tbl, 
                                                      x_QUALIFIERS_val_tbl      => l_x_qualifiers_val_tbl,
                                                      x_PRICING_ATTR_tbl        => l_x_pricing_attr_tbl, 
                                                      x_PRICING_ATTR_val_tbl    => l_x_pricing_attr_val_tbl);
                    
                write_output('Return status = ' || l_x_return_status);
                  
                IF(l_x_return_status <> 'S') THEN
                    --  Error Processing
                    fnd_file.put_line (fnd_file.output, '  Message Count       = ' || l_x_msg_count);
                  
                    FOR K IN 1 .. l_x_msg_count LOOP 
                      l_x_msg_data := oe_msg_pub.get(p_msg_index => k, 
                                                     p_encoded => 'F'); 
                      write_output(TO_CHAR(K)||' MESSAGE TEXT  '|| l_x_msg_data);        
                    END LOOP;
                    
                    -- 08/01/2013 CG: TMS 20130729-00576: Changed to prevent exit of program and processing of remaining records
                    -- RETURN;                             
                    
                END IF;
                
                commit;  
                  
                j := 1;
                l_price_list_line_tbl    := QP_PRICE_LIST_PUB.G_MISS_PRICE_LIST_LINE_TBL; 
            END IF;
               
        end loop;
        
        IF(j NOT IN (0, 1)) THEN
            l_nums := j-1;
            write_output('Deleting ' || l_nums || ' price lines to price list');
        
            qp_price_list_pub.process_price_list( p_api_version_number      => l_api_version_number,
                                                  p_init_msg_list           => l_init_msg_list,
                                                  p_return_values           => l_return_values,
                                                  p_commit                  => l_commit,
                                                  x_return_status           => l_x_return_status,
                                                  x_msg_count               => l_x_msg_count,
                                                  x_msg_data                => l_x_msg_data,
                                                  p_PRICE_LIST_rec          => l_price_list_rec, 
                                                  p_PRICE_LIST_LINE_tbl     => l_price_list_line_tbl, 
                                                  p_PRICING_ATTR_tbl        => l_pricing_attr_tbl, 
                                                  x_PRICE_LIST_rec          => l_x_price_list_rec, 
                                                  x_PRICE_LIST_val_rec      => l_x_price_list_val_rec, 
                                                  x_PRICE_LIST_LINE_tbl     => l_x_price_list_line_tbl, 
                                                  x_PRICE_LIST_LINE_val_tbl => l_x_price_list_line_val_tbl, 
                                                  x_QUALIFIERS_tbl          => l_x_qualifiers_tbl, 
                                                  x_QUALIFIERS_val_tbl      => l_x_qualifiers_val_tbl,
                                                  x_PRICING_ATTR_tbl        => l_x_pricing_attr_tbl, 
                                                  x_PRICING_ATTR_val_tbl    => l_x_pricing_attr_val_tbl);
              
            write_output('Return status = ' || l_x_return_status);
            
            IF(l_x_return_status <> 'S') THEN
                --  Error Processing
                write_output('  Message Count       = ' || l_x_msg_count);
          
                FOR K IN 1 .. l_x_msg_count 
                LOOP 
                    l_x_msg_data := oe_msg_pub.get(p_msg_index => k, p_encoded => 'F'); 
                    write_output(TO_CHAR(K)||' MESSAGE TEXT  '|| l_x_msg_data);        
                END LOOP;
                
                -- 08/01/2013 CG: TMS 20130729-00576: Changed to prevent exit of program and processing of remaining records
                -- RETURN;            
            
            END IF;
            
            COMMIT;
        END IF;
    
    end loop;
    
    write_output('---------------------------------------------------------------');
    */
    -- 08/01/13 CG: TMS 20130729-00576
    
    write_output('Populating staging table with data from National Market Setup Price List');
    
    execute immediate 'truncate table XXWC.XXWC_NTL_MARKET_PRL_UPD_STG';
    l_count := 0;
    for c0 in cur_base_ntl_market_prl
    loop
        exit when cur_base_ntl_market_prl%notfound;
        
        if c0.product_attribute = 'PRICING_ATTRIBUTE1' then -- Item Number
        
            l_exists_in_stg := 0;            
            begin
                select  count(*)
                into    l_exists_in_stg
                from    XXWC.XXWC_NTL_MARKET_PRL_UPD_STG
                where   inventory_item_id = to_number(c0.product_attr_value);
            exception
            when others then
                l_exists_in_stg := 0;
            end;
            
            if l_exists_in_stg = 0 then
            
                l_primary_uom_code  := null;
                begin
                    select  primary_uom_code
                    into    l_primary_uom_code
                    from    apps.mtl_system_items_b msib
                            , apps.mtl_parameters mp
                    where   msib.inventory_item_id = to_number(c0.product_attr_value)
                    and     msib.organization_id = mp.organization_id
                    and     mp.organization_code = 'MST';
                exception
                when others then
                    l_primary_uom_code := null;
                end;           
            
                begin
                    insert into XXWC.XXWC_NTL_MARKET_PRL_UPD_STG (
                        base_market_prl_id
                        , market_prl_id
                        , inventory_item_id
                        , primary_uom_code
                        , price_list_value
                        , call_for_price
                        , source_prod_attribute
                        , source_prod_value
                    ) values (
                        p_base_prl_id
                        , p_dest_prl_id
                        , to_number(c0.product_attr_value)
                        , l_primary_uom_code
                        , c0.operand
                        , (case when c0.product_attribute = 'PRICING_ATTRIBUTE1' then c0.call_for_price
                                else null
                                end)
                        , c0.product_attribute
                        , c0.product_attr_value
                    );
                    l_count := l_count + 1;
                exception
                when others then
                    write_output('Could not insert inventory_item_id to stg '||to_number(c0.product_attr_value));
                end;
            
            end if;
        
        elsif c0.product_attribute in ('PRICING_ATTRIBUTE2','PRICING_ATTRIBUTE27') then -- Item Cat Class and Pricing Category
        
            l_category_set_id := null;
            begin
                select  distinct(category_set_id)
                into    l_category_set_id
                from    apps.mtl_item_categories
                where   category_id = to_number(c0.product_attr_value)
                and     rownum = 1;
            exception
            when others then
                l_category_set_id := null;
                write_output('Couldnt find category set id for category '||to_number(c0.product_attr_value));
            end;
            
            if l_category_set_id is not null then
                
                for c1 in cur_category_items (l_category_set_id, to_number(c0.product_attr_value))
                loop
                    exit when cur_category_items%notfound;
                    
                    l_wght_ntl_avg_cost := null;
                    l_new_prl_value     := null;
                    l_item_number       := null;
                    
                    begin
                        select  segment1
                        into    l_item_number
                        from    apps.mtl_system_items_b msib
                                , apps.mtl_parameters mp
                        where   msib.inventory_item_id = c1.inventory_item_id
                        and     msib.organization_id = mp.organization_id
                        and     mp.organization_code = 'MST'
                        and     rownum = 1;
                    exception
                    when others then
                        l_item_number := null;
                    end;                    
                    
                    begin
                        select  wght_ntl_avg_cost
                        into    l_wght_ntl_avg_cost
                        from    xxwc.xxwc_qp_wght_ntl_avg_cost_stg x1
                        where   x1.inventory_item_id = c1.inventory_item_id;
                    exception
                    when no_data_found then
                        l_wght_ntl_avg_cost := null;
                        write_output('No entries in the Weighted Ntl Avg Cost Stg table. Item '||nvl(l_item_number, c1.inventory_item_id)||'. Not loading into national market staging table.');
                    when too_many_rows then
                        l_wght_ntl_avg_cost := null;
                        write_output('Multiple entries for item in the Weighted Ntl Avg Cost Stg table. Item Id '||nvl(l_item_number, c1.inventory_item_id)||'. Not loading into national market staging table.');
                    when others then
                        l_wght_ntl_avg_cost := null;
                        write_output('Other error looking for item in the Weighted Ntl Avg Cost Stg table. Item Id '||nvl(l_item_number, c1.inventory_item_id)||'. Not loading into national market staging table.');
                        write_output('Error: '||SQLERRM);                                               
                    end;
                    
                    If l_wght_ntl_avg_cost is not null
                    then
                        
                        begin
                            select  ( l_wght_ntl_avg_cost / ( 1 - (c0.operand/100) ) )
                            into    l_new_prl_value
                            from    dual; 
                        exception
                        when others then
                            l_new_prl_value := null;
                            write_output('Could not calculate new price list value for item '||c1.inventory_item_id||'.  Wght Ntl Avg Cost: '||l_wght_ntl_avg_cost||
                                         '. Base Operand: '||c0.operand);
                            write_output('Error: '||SQLERRM);
                        end;
                        
                        if nvl(l_new_prl_value, 0) > 0 then
                            begin
                                insert into XXWC.XXWC_NTL_MARKET_PRL_UPD_STG (
                                    base_market_prl_id
                                    , market_prl_id
                                    , inventory_item_id
                                    , primary_uom_code
                                    , price_list_value
                                    , call_for_price
                                    , source_prod_attribute
                                    , source_prod_value
                                ) values (
                                    p_base_prl_id
                                    , p_dest_prl_id
                                    , c1.inventory_item_id
                                    , c1.primary_uom_code
                                    , l_new_prl_value
                                    , null
                                    , c0.product_attribute
                                    , c0.product_attr_value
                                );
                                l_count := l_count + 1;
                            exception
                            when others then
                                write_output('Could not insert inventory_item_id to stg '||c1.inventory_item_id||'. From category id '||c0.product_attr_value);
                            end;
                        else
                            write_output('Did not load into National Market Price List staging. New Prl value '||l_new_prl_value);
                        end if;
                    
                    end if;
                        
                end loop;
            
                commit;
            end if;
            
        else
            write_output('Invalid product attribute '||c0.product_attribute);
        end if;
        
    end loop;
    
    write_output(' ');
    write_output('Loaded '||l_count||' into the National Market Price List Staging Table');
    
    commit;
    
    write_output('---------------------------------------------------------------');
    write_output('Updating National Market Price List with data in staging');
    
    l_price_list_rec := QP_PRICE_LIST_PUB.G_MISS_PRICE_LIST_REC;
    
    l_price_list_rec.list_header_id := P_DEST_PRL_ID;
    begin
        select  name
                , list_type_code
                , description
        into    l_price_list_rec.name
                , l_price_list_rec.list_type_code
                , l_price_list_rec.description
        from    apps.qp_list_headers
        where   list_header_id = P_DEST_PRL_ID
        and     rownum = 1;                
    exception
    when others then
        null;
    end; 
    l_price_list_rec.operation := QP_GLOBALS.G_OPR_UPDATE;
    
    l_count := 1;
    j := 1;
    l_price_list_line_tbl    := QP_PRICE_LIST_PUB.G_MISS_PRICE_LIST_LINE_TBL; 
    l_pricing_attr_tbl       := QP_PRICE_LIST_PUB.G_MISS_PRICING_ATTR_TBL;
    for c0 in cur_ntl_market_stg
    loop
        exit when cur_ntl_market_stg%notfound;
        
        -- 08/01/13 CG: TMS 20130729-00576: Modified to handle updates as well as creates since we are not clearing the PRL
        begin
            select  qll.list_line_id
                    , qpa.list_line_id
                    , qpa.pricing_attribute_id
            into    l_price_list_line_tbl(j).list_line_id
                    , l_pricing_attr_tbl(j).list_line_id
                    , l_pricing_attr_tbl(j).pricing_attribute_id
            from    apps.qp_list_lines qll
                    , apps.qp_pricing_attributes qpa
            where   qll.list_header_id = P_DEST_PRL_ID
            and     qll.list_line_type_code = 'PLL'
            -- 08/01/13 CG: TMS 20130729-00576: NVLd to accomodate the Market National lines that dont have start date
            -- and     qll.start_date_active <= trunc(sysdate)                        
            and     nvl(qll.start_date_active, trunc(sysdate-1)) <= trunc(sysdate)
            and     nvl(qll.end_date_active, trunc(sysdate)) >= trunc(sysdate)
            and     qll.list_header_id = qpa.list_header_id
            and     qll.qualification_ind = qpa.qualification_ind
            and     qll.pricing_phase_id = qpa.pricing_phase_id
            and     qll.list_line_id = qpa.list_line_id
            and     qpa.product_attribute_context = 'ITEM'
            and     qpa.product_attribute = 'PRICING_ATTRIBUTE1'
            and     qpa.product_attribute_datatype = 'C'
            and     qpa.product_attr_value = to_char(c0.inventory_item_id);
             
            l_price_list_line_tbl(j).list_header_id            := P_DEST_PRL_ID;      
            -- 08/05/2013 CG: TMS 20130729-00576: Changed to include new rounding requirements
            -- l_price_list_line_tbl(j).operand                   := c0.price_list_value;
            begin            
                select  (case 
                            when c0.price_list_value <= 0.01 then round (c0.price_list_value,5)
                            when c0.price_list_value between 0.01 and 1 then round((ceil(c0.price_list_value*100)/100),2)
                            when c0.price_list_value > 1 then ((ceil(c0.price_list_value*10)/10) - 0.01) 
                         else round(c0.price_list_value,5)
                         end) new_value
                into     l_price_list_line_tbl(j).operand 
                from    dual;                
            exception
            when others then
                l_price_list_line_tbl(j).operand                   := c0.price_list_value;
            end;
            l_price_list_line_tbl(j).arithmetic_operator       := 'UNIT_PRICE';     
            
            if c0.source_prod_attribute = 'PRICING_ATTRIBUTE1' and c0.call_for_price is not null then
                l_price_list_line_tbl(j).context               := '162';
                l_price_list_line_tbl(j).attribute1            := c0.call_for_price; 
            end if;
            
            l_price_list_line_tbl(j).last_updated_by           := fnd_global.user_id;
            l_price_list_line_tbl(j).last_update_date          := sysdate;
            
            l_pricing_attr_tbl(j).product_attr_value        := to_char(c0.inventory_item_id);
            l_pricing_attr_tbl(j).product_uom_code          := c0.primary_uom_code;
            l_pricing_attr_tbl(j).product_attribute_context := 'ITEM';
            l_pricing_attr_tbl(j).product_attribute         := 'PRICING_ATTRIBUTE1';
            l_pricing_attr_tbl(j).product_attribute_datatype:= 'C';
            l_pricing_attr_tbl(j).last_updated_by           := fnd_global.user_id;
            l_pricing_attr_tbl(j).last_update_date          := sysdate; 
                        
            l_price_list_line_tbl(j).operation              := QP_GLOBALS.G_OPR_UPDATE;
            l_pricing_attr_tbl(j).operation                 := QP_GLOBALS.G_OPR_UPDATE;
                        
            begin                
                update  XXWC.XXWC_NTL_MARKET_PRL_UPD_STG
                set     status = 'IN_PROCESS'
                where   rowid = c0.rid;
            exception
            when others then
                null;
            end;
            
            j:= j + 1;
            l_count := l_count + 1;
        exception
        when no_data_found then
                
            l_price_list_line_tbl(j).list_header_id            := P_DEST_PRL_ID;
            l_price_list_line_tbl(j).primary_uom_flag          := 'Y';
            l_price_list_line_tbl(j).list_line_type_code       := 'PLL';
            -- 07/29/2013 CG: TMS 20130729-00576. Changed Product Precedence to 250
            -- l_price_list_line_tbl(j).product_precedence        := 220;
            -- 08/08/2013 CG: TMS 20130107-00832: Changed Product Precedence to 300, for new Market National Category Price List
            -- l_price_list_line_tbl(j).product_precedence        := 250;
            l_price_list_line_tbl(j).product_precedence        := 300;
            l_price_list_line_tbl(j).arithmetic_operator       := 'UNIT_PRICE';
            -- 08/05/2013 CG: TMS 20130729-00576: Changed to include new rounding requirements
            -- l_price_list_line_tbl(j).operand                   := c0.price_list_value;
            begin            
                select  (case 
                            when c0.price_list_value <= 0.01 then round (c0.price_list_value,5)
                            when c0.price_list_value between 0.01 and 1 then round((ceil(c0.price_list_value*100)/100),2)
                            when c0.price_list_value > 1 then ((ceil(c0.price_list_value*10)/10) - 0.01) 
                         else round(c0.price_list_value,5)
                         end) new_value
                into     l_price_list_line_tbl(j).operand 
                from    dual;                
            exception
            when others then
                l_price_list_line_tbl(j).operand                   := c0.price_list_value;
            end;
            
            if c0.source_prod_attribute = 'PRICING_ATTRIBUTE1' and c0.call_for_price is not null then
                l_price_list_line_tbl(j).context               := '162';
                l_price_list_line_tbl(j).attribute1            := c0.call_for_price; 
            end if;
            
            l_price_list_line_tbl(j).created_by                := fnd_global.user_id;
            l_price_list_line_tbl(j).creation_date             := sysdate;
            -- 07/29/2013 CG: TMS 20130729-00576. Changed Start Date Active to Null
            -- l_price_list_line_tbl(j).start_date_active         := trunc(sysdate);
            l_price_list_line_tbl(j).start_date_active         := NULL;
            l_price_list_line_tbl(j).last_updated_by           := fnd_global.user_id;
            l_price_list_line_tbl(j).last_update_date          := sysdate;
            l_price_list_line_tbl(j).operation                 := QP_GLOBALS.G_OPR_CREATE;
            
            l_pricing_attr_tbl(j).list_header_id            := P_DEST_PRL_ID;
            l_pricing_attr_tbl(j).product_attribute_context := 'ITEM';
            l_pricing_attr_tbl(j).product_attribute         := 'PRICING_ATTRIBUTE1';
            l_pricing_attr_tbl(j).product_attribute_datatype:= 'C';
            l_pricing_attr_tbl(j).product_attr_value        := to_char(c0.inventory_item_id);
            l_pricing_attr_tbl(j).product_uom_code          := c0.primary_uom_code;
            l_pricing_attr_tbl(j).pricing_phase_id          := 1;
            l_pricing_attr_tbl(j).price_list_line_index     := j; --l_count;
            l_pricing_attr_tbl(j).created_by                := fnd_global.user_id;
            l_pricing_attr_tbl(j).creation_date             := sysdate;
            l_pricing_attr_tbl(j).last_updated_by           := fnd_global.user_id;
            l_pricing_attr_tbl(j).last_update_date          := sysdate;        
            l_pricing_attr_tbl(j).list_line_id              := FND_API.G_MISS_NUM;
                
            SELECT qp_pricing_attributes_s.nextval
            INTO   l_pricing_attr_tbl(j).pricing_attribute_id
            FROM   dual;
                
            l_pricing_attr_tbl(j).operation                 := QP_GLOBALS.G_OPR_CREATE;
            
            begin                
                update  XXWC.XXWC_NTL_MARKET_PRL_UPD_STG
                set     status = 'IN_PROCESS'
                where   rowid = c0.rid;
            exception
            when others then
                null;
            end;
            
            j:= j + 1;
            l_count := l_count + 1;
            
        when others then
            write_output('Invalid QP Line for item: '||c0.inventory_item_id||'. Error: '||SQLERRM);
            begin                    
                update  XXWC.XXWC_NTL_MARKET_PRL_UPD_STG
                set     status = status||'.INVALID_QP_LINE'
                where   rowid = c0.rid;
            exception
            when others then
                null;
            end;
        end;
            
        IF(j > l_commit_size) THEN
            l_nums := j-1;
            write_output('Inserting ' || l_nums || ' lines to price list');
            
            qp_price_list_pub.process_price_list( p_api_version_number      => l_api_version_number,
                                                  p_init_msg_list           => l_init_msg_list,
                                                  p_return_values           => l_return_values,
                                                  p_commit                  => l_commit,
                                                  x_return_status           => l_x_return_status,
                                                  x_msg_count               => l_x_msg_count,
                                                  x_msg_data                => l_x_msg_data,
                                                  p_PRICE_LIST_rec          => l_price_list_rec, 
                                                  p_PRICE_LIST_LINE_tbl     => l_price_list_line_tbl, 
                                                  p_PRICING_ATTR_tbl        => l_pricing_attr_tbl, 
                                                  x_PRICE_LIST_rec          => l_x_price_list_rec, 
                                                  x_PRICE_LIST_val_rec      => l_x_price_list_val_rec, 
                                                  x_PRICE_LIST_LINE_tbl     => l_x_price_list_line_tbl, 
                                                  x_PRICE_LIST_LINE_val_tbl => l_x_price_list_line_val_tbl, 
                                                  x_QUALIFIERS_tbl          => l_x_qualifiers_tbl, 
                                                  x_QUALIFIERS_val_tbl      => l_x_qualifiers_val_tbl,
                                                  x_PRICING_ATTR_tbl        => l_x_pricing_attr_tbl, 
                                                  x_PRICING_ATTR_val_tbl    => l_x_pricing_attr_val_tbl);
                    
            write_output('Return status = ' || l_x_return_status);
                  
            IF(l_x_return_status <> 'S') THEN
                --  Error Processing
                fnd_file.put_line (fnd_file.output, '  Message Count       = ' || l_x_msg_count);
                  
                FOR K IN 1 .. l_x_msg_count LOOP 
                  l_x_msg_data := oe_msg_pub.get(p_msg_index => k, 
                                                 p_encoded => 'F'); 
                  write_output(TO_CHAR(K)||' MESSAGE TEXT  '|| l_x_msg_data);        
                END LOOP;
                
                -- 08/01/2013 CG: TMS 20130729-00576: Changed to prevent exit of program and processing of remaining records
                -- RETURN;   
                
                begin                
                    update  XXWC.XXWC_NTL_MARKET_PRL_UPD_STG
                    set     status = 'ERROR'
                    where   status = 'IN_PROCESS';
                exception
                when others then
                    null;
                end;                          
                    
            ELSE
                begin                
                    update  XXWC.XXWC_NTL_MARKET_PRL_UPD_STG
                    set     status = 'LOADED'
                    where   status = 'IN_PROCESS';
                exception
                when others then
                    null;
                end;
            END IF;
                
            commit;  
                  
            j := 1;
            l_price_list_line_tbl    := QP_PRICE_LIST_PUB.G_MISS_PRICE_LIST_LINE_TBL;
            l_pricing_attr_tbl       := QP_PRICE_LIST_PUB.G_MISS_PRICING_ATTR_TBL; 
        END IF;
    
    end loop;
    
    IF(j NOT IN (0, 1)) THEN
        l_nums := j-1;
        write_output('Inserting ' || l_nums || ' lines to price list');
        
        qp_price_list_pub.process_price_list( p_api_version_number      => l_api_version_number,
                                              p_init_msg_list           => l_init_msg_list,
                                              p_return_values           => l_return_values,
                                              p_commit                  => l_commit,
                                              x_return_status           => l_x_return_status,
                                              x_msg_count               => l_x_msg_count,
                                              x_msg_data                => l_x_msg_data,
                                              p_PRICE_LIST_rec          => l_price_list_rec, 
                                              p_PRICE_LIST_LINE_tbl     => l_price_list_line_tbl, 
                                              p_PRICING_ATTR_tbl        => l_pricing_attr_tbl, 
                                              x_PRICE_LIST_rec          => l_x_price_list_rec, 
                                              x_PRICE_LIST_val_rec      => l_x_price_list_val_rec, 
                                              x_PRICE_LIST_LINE_tbl     => l_x_price_list_line_tbl, 
                                              x_PRICE_LIST_LINE_val_tbl => l_x_price_list_line_val_tbl, 
                                              x_QUALIFIERS_tbl          => l_x_qualifiers_tbl, 
                                              x_QUALIFIERS_val_tbl      => l_x_qualifiers_val_tbl,
                                              x_PRICING_ATTR_tbl        => l_x_pricing_attr_tbl, 
                                              x_PRICING_ATTR_val_tbl    => l_x_pricing_attr_val_tbl);
              
        write_output('Return status = ' || l_x_return_status);
            
        IF(l_x_return_status <> 'S') THEN
            --  Error Processing
            write_output('  Message Count       = ' || l_x_msg_count);
          
            FOR K IN 1 .. l_x_msg_count 
            LOOP 
                l_x_msg_data := oe_msg_pub.get(p_msg_index => k, p_encoded => 'F'); 
                write_output(TO_CHAR(K)||' MESSAGE TEXT  '|| l_x_msg_data);        
            END LOOP;
               
            -- 08/01/2013 CG: TMS 20130729-00576: Changed to prevent exit of program and processing of remaining records 
            -- RETURN;     
            begin                
                update  XXWC.XXWC_NTL_MARKET_PRL_UPD_STG
                set     status = 'ERROR'
                where   status = 'IN_PROCESS';
            exception
            when others then
                null;
            end;       
            
        ELSE
            begin                
                update  XXWC.XXWC_NTL_MARKET_PRL_UPD_STG
                set     status = 'LOADED'
                where   status = 'IN_PROCESS';
            exception
            when others then
                null;
            end;
        END IF;
            
        COMMIT;
    END IF;
    
    write_output('Inserted a total of '||l_count-1||' lines to the National Market Price List');
    commit;
    write_output('---------------------------------------------------------------');
    
EXCEPTION
WHEN OTHERS THEN
    l_error_message2 := 'xxwc_gen_ntl_market_prl '
                || 'Error_Stack...'
                || DBMS_UTILITY.format_error_stack ()
                || ' Error_Backtrace...'
                || DBMS_UTILITY.format_error_backtrace ();

    xxcus_error_pkg.xxcus_error_main_api (
        p_called_from         => 'xxwc_qp_market_price_util_pkg.xxwc_gen_ntl_market_prl'
       ,p_calling             => 'xxwc_qp_market_price_util_pkg.xxwc_gen_ntl_market_prl'
       ,p_request_id          => l_conc_req_id
       ,p_ora_error_msg       => l_error_message2
       ,p_error_desc          => 'Error running XXWC QP Generate National Market Price List'
       ,p_distribution_list   => 'HDSOracleDevelopers@hdsupply.com'
       ,p_module              => 'QP');

    write_log('Error in main xxwc_gen_ntl_market_prl: '||l_error_message2);
    
    errmsg := l_error_message2;
    retcode := '2';   
END xxwc_gen_ntl_market_prl;

-- 05/29/2013 CG: Added to retrieve the item number based on the product attribute value
FUNCTION get_item_number (p_product_attr_value  IN VARCHAR2
                          , p_return_type       IN VARCHAR2) 
RETURN VARCHAR2 IS
    l_item_number varchar2(40);
    l_item_desc   varchar2(240);
BEGIN
    l_item_number   := null;
    l_item_desc     := null;
    
    select  msib.segment1
            , msib.description
    into    l_item_number
            , l_item_desc
    from    apps.mtl_system_items_b msib
            , apps.mtl_parameters mp
    where   msib.inventory_item_id = to_number (p_product_attr_value)
    and     msib.organization_id = mp.organization_id
    and     mp.organization_code = 'MST'
    and     rownum = 1;
    
    if p_return_type = 'ITEM_NUM' then
        return l_item_number;
    elsif p_return_type = 'ITEM_DESC' then
        return l_item_desc;
    else
        return null;
    end if;
    
EXCEPTION
WHEN OTHERS THEN
    return null;
END get_item_number;

-- 05/29/2013 CG: Added to retrieve the category segments
FUNCTION get_category_desc (p_product_attr_value    IN VARCHAR2
                            , p_category_set          IN   VARCHAR2
                            , p_segment_num           IN   NUMBER) 
RETURN VARCHAR2 IS
    l_cat_segment1      VARCHAR2 (40);
    l_cat_segment2      VARCHAR2 (40);
    l_cat_segment3      VARCHAR2 (40);
    l_cat_segment4      VARCHAR2 (40);
    l_cat_seg_concat    VARCHAR2 (163);
    l_cat_desc          VARCHAR2 (240);
    l_return_category   VARCHAR2 (240);
BEGIN

    l_cat_segment1 := NULL;
    l_cat_segment2 := NULL;
    l_cat_segment3 := NULL;
    l_cat_segment4 := NULL;
    l_cat_seg_concat := NULL;
    l_cat_desc      := NULL;
    l_return_category := NULL;

    select  mcb.segment1, mcb.segment2, mcb.segment3, mcb.segment4,
            mcb.category_concat_segs, mcb.description
    into    l_cat_segment1, l_cat_segment2, l_cat_segment3, l_cat_segment4,
            l_cat_seg_concat, l_cat_desc 
    from    apps.mtl_categories_v mcb
            , apps.mtl_category_sets mcs
    where   mcb.category_id = to_number (p_product_attr_value)
    and     mcb.structure_id = mcs.structure_id
    and     mcs.category_set_name = p_category_set
    and     rownum = 1;

    IF p_segment_num = 1 THEN
        l_return_category := l_cat_segment1;
    ELSIF p_segment_num = 2 THEN
        l_return_category := l_cat_segment2;
    ELSIF p_segment_num = 3 THEN
        l_return_category := l_cat_segment3;
    ELSIF p_segment_num = 4 THEN
        l_return_category := l_cat_segment4;
    ELSIF p_segment_num = 0 THEN 
        l_return_category := l_cat_seg_concat;
    ELSE
        l_return_category := l_cat_desc;
    END IF;

    RETURN l_return_category;

EXCEPTION
WHEN OTHERS THEN
    return null;
END get_category_desc;

-- 05/29/2013 CG: Adding National Price List Setup Maintenance Tool Processor
PROCEDURE XXWC_QP_NTL_PRL_MAINT_TOOL (RETCODE      OUT NUMBER
                                     , ERRMSG     OUT VARCHAR2
                                     , P_FILE_ID  IN NUMBER)
IS
    l_msg_data      VARCHAR2 (4000);
    l_error_message2    clob;
    
    -- File Processing Variables
    l_file_id       number :=  P_FILE_ID;    
    l_file          BLOB;
    l_file_name     VARCHAR2 (256);
    i               INTEGER := 1; -- Row counter
    j               INTEGER := 1; -- Column Counter
    is_file_open    INTEGER;
    l_hdr           INTEGER;
    l_line          INTEGER;

    l_comma_count   NUMBER;
    l_offset        NUMBER := 1;
    l_eol_pos       NUMBER := 32767;
    l_amount        NUMBER;
    l_file_len      NUMBER := DBMS_LOB.getlength (l_file);
    l_buffer        RAW (30000);
    
    l_price_list_header_tbl_type     xxwc_qp_market_price_util_pkg.price_list_header_tbl_type     := xxwc_qp_market_price_util_pkg.G_MISS_PRICE_LIST_HEADER_TBL; 
    l_maint_tool_data_tbl_type       xxwc_qp_market_price_util_pkg.maint_tool_data_tbl_type := xxwc_qp_market_price_util_pkg.G_MISS_MAINT_TOOL_DATA_TBL;
    l_data          VARCHAR2(240);
    l_item_number   VARCHAR2(40);
    l_is_number     VARCHAR2(1);
   
    l_start_time    number;  
    l_end_time      number;
    l_conc_req_id   number := fnd_global.conc_request_id;
    l_login_id      number := fnd_global.login_id;
    -- File Processing Variables
    
    -- QP Pricing API Variables/Cursors
    cursor  cur_price_lists ( p_file_id number
                              , p_request_id number) is
            select  distinct(prl_name) price_list_name
            from    xxwc.XXWC_QP_MAINT_TOOL_LINE_STG
            where   status = 'NEW'
            and     request_id = p_request_id
            and     file_id = p_file_id;
   
    cursor  cur_list_lines (p_prl_name varchar2
                            , p_file_id number
                            , p_request_id number) is
            select  x1.rowid rid
                    , x1.*
            from    xxwc.XXWC_QP_MAINT_TOOL_LINE_STG x1
            where   x1.prl_name = p_prl_name            
            and     x1.status = 'NEW'  
            and     request_id = p_request_id
            and     file_id = p_file_id         
            order by x1.item_number;
    
    l_cnt                           NUMBER;
    l                               NUMBER; -- i
    m                               NUMBER; -- j
    n                               NUMBER; -- k       
    l_api_version_number            NUMBER         := 1.0;
    l_init_msg_list                 VARCHAR2(10)   := FND_API.G_FALSE;
    l_return_values                 VARCHAR2(10)   := FND_API.G_FALSE;
    l_commit                        VARCHAR2(10)   := FND_API.G_TRUE;
    l_x_return_status               VARCHAR2(255)  := 'S';
    l_x_msg_count                   NUMBER;
    l_x_msg_data                    VARCHAR2(255);
    g_user_id                       NUMBER;
    l_price_list_rec                QP_PRICE_LIST_PUB.PRICE_LIST_REC_TYPE          := QP_PRICE_LIST_PUB.G_MISS_PRICE_LIST_REC;
    l_price_list_val_rec            QP_PRICE_LIST_PUB.PRICE_LIST_VAL_REC_TYPE      := QP_PRICE_LIST_PUB.G_MISS_PRICE_LIST_VAL_REC;
    l_price_list_line               QP_PRICE_LIST_PUB.PRICE_LIST_LINE_REC_TYPE    := QP_PRICE_LIST_PUB.G_MISS_PRICE_LIST_LINE_REC;
    l_price_list_line_tbl           QP_PRICE_LIST_PUB.PRICE_LIST_LINE_TBL_TYPE     := QP_PRICE_LIST_PUB.G_MISS_PRICE_LIST_LINE_TBL; 
    l_price_list_line_val_tbl       QP_PRICE_LIST_PUB.PRICE_LIST_LINE_VAL_TBL_TYPE := QP_PRICE_LIST_PUB.G_MISS_PRICE_LIST_LINE_VAL_TBL;
    l_qualifiers_tbl                QP_QUALIFIER_RULES_PUB.QUALIFIERS_TBL_TYPE     := QP_PRICE_LIST_PUB.G_MISS_QUALIFIERS_TBL;
    l_qualifiers_val_tbl            QP_QUALIFIER_RULES_PUB.QUALIFIERS_VAL_TBL_TYPE := QP_PRICE_LIST_PUB.G_MISS_QUALIFIERS_VAL_TBL;
    l_pricing_att_rec               QP_PRICE_LIST_PUB.PRICING_ATTR_REC_TYPE := QP_PRICE_LIST_PUB.G_MISS_PRICING_ATTR_REC;
    l_pricing_attr_tbl              QP_PRICE_LIST_PUB.PRICING_ATTR_TBL_TYPE        := QP_PRICE_LIST_PUB.G_MISS_PRICING_ATTR_TBL;
    l_pricing_attr_val_tbl_type     QP_PRICE_LIST_PUB.PRICING_ATTR_VAL_TBL_TYPE    := QP_PRICE_LIST_PUB.G_MISS_PRICING_ATTR_VAL_TBL;
    l_x_price_list_rec              QP_PRICE_LIST_PUB.PRICE_LIST_REC_TYPE;
    l_x_price_list_val_rec          QP_PRICE_LIST_PUB.PRICE_LIST_VAL_REC_TYPE;
    l_x_price_list_line_tbl         QP_PRICE_LIST_PUB.PRICE_LIST_LINE_TBL_TYPE;
    l_x_price_list_line_val_tbl     QP_PRICE_LIST_PUB.PRICE_LIST_LINE_VAL_TBL_TYPE;
    l_x_qualifiers_tbl              QP_QUALIFIER_RULES_PUB.QUALIFIERS_TBL_TYPE;
    l_x_qualifiers_val_tbl          QP_QUALIFIER_RULES_PUB.QUALIFIERS_VAL_TBL_TYPE;
    l_x_pricing_attr_tbl            QP_PRICE_LIST_PUB.PRICING_ATTR_TBL_TYPE;
    l_x_pricing_attr_val_tbl        QP_PRICE_LIST_PUB.PRICING_ATTR_VAL_TBL_TYPE;
    
    l_error_message_list            ERROR_HANDLER.ERROR_TBL_TYPE;
    l_price_list                    VARCHAR2(3);
    l_prod_attr_value_id            NUMBER;
    l_primary_uom_code              VARCHAR2(3);
    l_process                       VARCHAR2(1);
    gpr_msg_data                    VARCHAR2(2000);
    l_valid                         VARCHAR2(1);
    l_warehouse_id                  VARCHAR2(30);
    l_structure_name                VARCHAR2(240);
    l_commit_size                   NUMBER;
    
    -- QP Pricing API Variables/Cursors
    
    
BEGIN
    write_output('Starting Maintenance Tool Processing for File ID: '||l_file_id);
    write_output('---------------------------------------------------------------');
    
    l_Start_time := DBMS_UTILITY.get_time;
    write_log ('l_Start_time: '||l_Start_time);
   
    BEGIN
        SELECT  file_data
                , file_name
        INTO    l_file
                , l_file_name
        FROM    fnd_lobs
        WHERE   file_id = l_file_id; 
    EXCEPTION
    WHEN OTHERS THEN
        l_file := null;
        l_file_name := null;
        write_log ('File Not Found. Error: '||SQLERRM);
        l_msg_data := substr(SQLERRM, 1, 4000);
        errmsg := l_msg_data;
        retcode := '2';  
        raise;
    END;

    is_file_open := DBMS_LOB.ISOPEN (l_file);
    IF is_file_open = 1
    THEN
        DBMS_LOB.close (l_file);
    END IF;

    BEGIN
        DBMS_LOB.open (l_file, DBMS_LOB.lob_readonly);
        is_file_open        := DBMS_LOB.ISOPEN (l_file);
    EXCEPTION
    WHEN OTHERS  THEN
        write_log ('Could not open file. Error: '||SQLERRM);
        l_msg_data := substr(SQLERRM, 1, 4000);
        errmsg := l_msg_data;
        retcode := '2';  
        raise;
    END;

    l_file_len  := DBMS_LOB.getlength (l_file);

    write_output ('File Name '||l_file_name||' and length ' ||l_file_len);
   
    write_output ('Starting File read and parsing from LOB...');
    l_price_list_header_tbl_type := xxwc_qp_market_price_util_pkg.G_MISS_PRICE_LIST_HEADER_TBL; 
    l_maint_tool_data_tbl_type := xxwc_qp_market_price_util_pkg.G_MISS_MAINT_TOOL_DATA_TBL;
    l_hdr           := 1;
    l_line          := 1;
    l_data          := null;
    WHILE (l_offset < l_file_len)
    LOOP
        -- i: Row Counter. Reset Per File
        -- j: Column Counter. Reset Per row
        
        -- Finding EOL position  for new line/line feed ascii 0A
        l_eol_pos      := DBMS_LOB.INSTR (l_file, '0A', 1, i);
        
        -- Calculating how many chars to pull in buffer based on EOL position and offset
        IF i = 1 THEN
            l_amount     := l_eol_pos - 1;
        ELSE
            l_amount     := l_eol_pos - l_offset;
        END IF;

        -- Reading first record from LOB file
        DBMS_LOB.read (l_file, l_amount, l_offset, l_buffer);
      
        -- From the first record pulled retrieve the number of columns based on comma count
        IF i = 1 THEN
            SELECT   regexp_count (REPLACE (UTL_RAW.cast_to_varchar2 (l_buffer), CHR (13), NULL), ',')
            INTO   l_comma_count
            FROM   DUAL;

            write_log ('l_comma_count: ' || l_comma_count);
            write_log (' ');            
        END IF;
        
        write_log ('Cycle #: ' || i);
        write_log ('l_offset: ' || l_offset);
        write_log ('l_eol_pos: ' || l_eol_pos);
        write_log ('l_amount: ' || l_amount);
        write_log ('Line #' || i || ': '|| REPLACE (UTL_RAW.cast_to_varchar2 (l_buffer), CHR (13), NULL));
      
        j := 1;
        FOR j IN 1..l_comma_count
        LOOP
            -- Parsing Column Data is slightly different for the first column and the remaining cols
            IF j = 1 THEN
                l_data := SUBSTR ( REPLACE ( UTL_RAW.cast_to_varchar2 (l_buffer), CHR (13), NULL )
                                 , 1
                                 , INSTR ( REPLACE ( UTL_RAW.cast_to_varchar2 (l_buffer), CHR (13), NULL), ',', 1, 1) - 1);
                                 
                write_log ('Line Split#' || j || ': '|| l_data);
            ELSE
                l_data := SUBSTR (REPLACE (UTL_RAW.cast_to_varchar2 (l_buffer), CHR (13), NULL)
                                 , INSTR (REPLACE (UTL_RAW.cast_to_varchar2 (l_buffer), CHR (13), NULL), ',', 1, j - 1)+ 1
                                 , (INSTR (REPLACE (UTL_RAW.cast_to_varchar2 (l_buffer), CHR (13), NULL), ',', 1, j))
                                   - (INSTR (REPLACE (UTL_RAW.cast_to_varchar2 (l_buffer), CHR (13), NULL), ',', 1, j - 1)+ 1)
                                );

                write_log ('Line Split#' || j || ': '|| l_data);            
            END IF;
         
            -- i = 1 is Row1 of the file and is the column headers nothing to do with those
            if i = 1 then
                null;
            elsif i > 1 then
                -- All other rows are item and price list data per price list column
                -- The price list column can also be used to mark an item to be end dated on a price list with a D
                
                l_is_number := null;
                SELECT  (case when regexp_like(l_data, '^-?[[:digit:],.]*$') then 'Y' else 'N' end)
                into    l_is_number
                FROM    dual;
            
                -- Col number
                if j = 1 then
                    l_maint_tool_data_tbl_type(l_line).PRL_NAME             := l_data;
                elsif j = 2 then
                    l_maint_tool_data_tbl_type(l_line).PRODUCT_ATTRIBUTE    := l_data;
                elsif j = 3 then
                    l_maint_tool_data_tbl_type(l_line).PRODUCT_ATTR_VALUE   := trim(replace(replace(l_data,'?',null),'''', null));
                elsif j = 4 then
                    null; -- Product Attribute Description
                elsif j = 5 then
                    l_maint_tool_data_tbl_type(l_line).PRICE_TYPE           := l_data;                    
                elsif j = 6 then
                    -- Checking to see if the value pulled is list price or the obsolete marker                    
                    if l_is_number = 'N' then
                        l_maint_tool_data_tbl_type(l_line).OBSOLETE_ITEM    := substr(l_data,1,1);
                        l_maint_tool_data_tbl_type(l_line).LIST_PRICE       := null;
                    else
                        l_maint_tool_data_tbl_type(l_line).LIST_PRICE       := to_number(l_data);
                        l_maint_tool_data_tbl_type(l_line).OBSOLETE_ITEM    := null;
                    end if;             
                elsif j = 7 then
                    l_maint_tool_data_tbl_type(l_line).CALL_FOR_PRICE       := substr(l_data,1,1);
                else 
                    null;
                end if;
                
            end if;
         
        END LOOP;
        
        -- Row 1 is headers no need to load
        if i > 1 then
            l_maint_tool_data_tbl_type(l_line).FILE_ID              := l_file_id;
            l_maint_tool_data_tbl_type(l_line).LIST_HEADER_ID       := null;
            l_maint_tool_data_tbl_type(l_line).LIST_LINE_ID         := null;
            l_maint_tool_data_tbl_type(l_line).INVENTORY_ITEM_ID    := null;
            l_maint_tool_data_tbl_type(l_line).STATUS               := 'NEW';
            l_maint_tool_data_tbl_type(l_line).ERR_MESSAGE          := null;
            l_maint_tool_data_tbl_type(l_line).REQUEST_ID           := l_conc_req_id;       
            l_maint_tool_data_tbl_type(l_line).CREATED_BY           := fnd_global.user_id;
            l_maint_tool_data_tbl_type(l_line).CREATION_DATE        := sysdate;
            l_maint_tool_data_tbl_type(l_line).LAST_UPDATED_BY      := fnd_global.user_id;
            l_maint_tool_data_tbl_type(l_line).LAST_UPDATE_DATE     := sysdate;
            l_maint_tool_data_tbl_type(l_line).LAST_UPDATE_LOGIN    := l_login_id;
            l_maint_tool_data_tbl_type(l_line).ITEM_NUMBER          := null;
            l_maint_tool_data_tbl_type(l_line).PRODUCT_ATTR_ID      := null;
            l_maint_tool_data_tbl_type(l_line).PRICING_FORMULA_ID   := null;
            
            l_line := l_line + 1; -- This will be initialized per file and increase as it reads throught the LOB            
        end if;

        write_log (' ');
        l_offset := l_offset + l_amount + 1; -- Increasing offset for read. Starting position of next read
        i        := i + 1; -- Rowcounter increase
    END LOOP;
   
    DBMS_LOB.close (l_file);
   
    write_output ('LOB file read and parsing complete. Starting bulk insert into staging...');
   
    FORALL x in l_maint_tool_data_tbl_type.first..l_maint_tool_data_tbl_type.last
    insert into xxwc.XXWC_QP_MAINT_TOOL_LINE_STG
    values l_maint_tool_data_tbl_type(x);

    commit;   
 
    write_output ('Bulk insert to staging complete.');          
    
    l_end_time := DBMS_UTILITY.get_time;
    write_log ('l_end_time : '||l_end_time );
    write_log ('File read/parsing processing time: '||to_char(l_end_time-l_start_time));
    write_output ('File read/parsing processing time: '||to_char(l_end_time-l_start_time));
   
    write_output('---------------------------------------------------------------');
    write_output('Starting staging data processing and load to EBS');
    
    l_Start_time := DBMS_UTILITY.get_time;
    write_log ('l_Start_time: '||l_Start_time);
    
    oe_msg_pub.initialize;
    
    --mo_global.init ('ONT');
    fnd_global.apps_initialize(user_id        => fnd_global.user_id,
                               resp_id        => fnd_global.resp_id,
                               resp_appl_id   => fnd_global.resp_appl_id);
                               
    mo_global.set_policy_context ('S', fnd_global.org_id);                               
    
    l_commit_size := 0;
    begin        
        select  (case when to_number(to_char(sysdate, 'HH24MI')) between 600 and 2200
                    -- 01/07/2014 CG: TMS 20131016-00375: Updated to use batch size profiles
                    -- then 1 --'Daytime'
                    then NVL(fnd_profile.value ('XXWC_QP_MAINT_DAY_BATCH_SIZE'),1) --'Daytime'
                    -- 08/01/13 CG: TMS 20130729-00576: Reduced to 500 in case a batch fails
                    -- else 1500 ---'Evening'
                    -- else 500 ---'Evening'
                    else NVL(fnd_profile.value ('XXWC_QP_MAINT_NIGHT_BATCH_SIZE'),500) -- 'Evening'
                end) time_frame
        into    l_commit_size
        from    dual;
    exception
    when others then
        l_commit_size := 500;
    end;
    
    g_user_id := fnd_global.user_id;
    IF(g_user_id = -1) THEN
        BEGIN
            SELECT user_id
            INTO   g_user_id 
            FROM   fnd_user
            WHERE  user_name = 'SYSADMIN';
        EXCEPTION
        WHEN OTHERS THEN
            g_user_id := -1;
        END;
    END IF;
    
    for c1 in cur_price_lists (l_file_id, l_conc_req_id)
    loop
        exit when cur_price_lists%notfound;
        
        write_output (' ');
        write_output ('Processing Price list: '||c1.price_list_name);
        
        l_price_list_rec := QP_PRICE_LIST_PUB.G_MISS_PRICE_LIST_REC;
        l_process := null;
        BEGIN
            SELECT list_header_id
            INTO   l_price_list_rec.list_header_id
            FROM   apps.qp_list_headers
            WHERE  name = c1.price_list_name
            AND    SYSDATE BETWEEN nvl(start_date_active, SYSDATE - 1) AND nvl(end_date_active, SYSDATE + 1);
          
            l_price_list_rec.operation := QP_GLOBALS.G_OPR_UPDATE;          
            
            l_price_list_rec.last_updated_by    := g_user_id;
            l_price_list_rec.last_update_date   := SYSDATE;
            
            l_process := 'Y';
        EXCEPTION
          WHEN OTHERS THEN
            write_output ('Price list '||c1.price_list_name||' does not exist will not process associated records.');
            l_process := 'N';
            
            Begin
                update  xxwc.XXWC_QP_MAINT_TOOL_LINE_STG
                set     status = 'INVALID_PRL'
                        , err_message = 'Price List does not exist'
                where   prl_name = c1.price_list_name
                and     file_id = l_file_id
                and     request_id = l_conc_req_id; 
            exception
            when others then
                null;
            end;
        END;
                
        if l_process = 'Y' then
        
            -- Validating and loading lines        
            i := 1;
            l_pricing_attr_tbl       := QP_PRICE_LIST_PUB.G_MISS_PRICING_ATTR_TBL;
            l_price_list_line_tbl    := QP_PRICE_LIST_PUB.G_MISS_PRICE_LIST_LINE_TBL;
            for c2 in cur_list_lines (c1.price_list_name, l_file_id, l_conc_req_id)
            loop
                exit when cur_list_lines%notfound;
                
                l_price_list_line   := QP_PRICE_LIST_PUB.G_MISS_PRICE_LIST_LINE_REC;
                l_pricing_att_rec   := QP_PRICE_LIST_PUB.G_MISS_PRICING_ATTR_REC;
                l_valid             := null;
                l_prod_attr_value_id:= null;
                l_primary_uom_code  := null;
                
                l_price_list_line.list_header_id            := l_price_list_rec.list_header_id;
                l_price_list_line.attribute1                := c2.call_for_price;
                l_price_list_line.context                   := '162';
                l_price_list_line.last_updated_by           := g_user_id;
                l_price_list_line.last_update_date          := sysdate;
                                                
                l_pricing_att_rec.list_header_id            := l_price_list_line.list_header_id;
                l_pricing_att_rec.last_updated_by           := g_user_id;
                l_pricing_att_rec.last_update_date          := sysdate;
                l_pricing_att_rec.price_list_line_index     := i;
                
                if c2.product_attribute = 'Item Number' then
                
                    begin
                        select  msib.inventory_item_id
                                , msib.primary_uom_code
                        into    l_prod_attr_value_id
                                , l_primary_uom_code
                        from    apps.mtl_system_items_b msib
                                , apps.mtl_parameters mp
                        where   msib.segment1 = c2.product_attr_value
                        and     msib.organization_id = mp.organization_id
                        and     mp.organization_code = 'MST';
                            
                        l_valid := 'Y';
                    exception
                    when others then
                        write_output('Invalid item number: '||c2.product_attr_value||'. Error: '||SQLERRM);
                        l_valid := 'N';  
                        begin                    
                            update  xxwc.XXWC_QP_MAINT_TOOL_LINE_STG
                            set     status = status||'.INVALID_ITEM'
                                    , err_message = err_message||'.Invalid Item'
                            where   rowid = c2.rid;
                        exception
                        when others then
                            null;
                        end;
                    end;
                    
                elsif c2.product_attribute in ('Pricing Category','Item Category') then
                    begin
                        select  to_char(category_id)
                                , 'EA'
                        into    l_prod_attr_value_id
                                , l_primary_uom_code
                        from    apps.mtl_categories_v
                        where   category_concat_segs = c2.product_attr_value
                        and     enabled_flag = 'Y'
                        and     rownum = 1;
                            
                        l_valid := 'Y';
                    exception
                    when others then
                        write_output('Invalid category: '||c2.product_attr_value||'. Error: '||SQLERRM);
                        l_valid := 'N';  
                        begin                    
                            update  xxwc.XXWC_QP_MAINT_TOOL_LINE_STG
                            set     status = status||'.INVALID_CAT'
                                    , err_message = err_message||'.Invalid Category'
                            where   rowid = c2.rid;
                        exception
                        when others then
                            null;
                        end;
                    end;
                else
                    write_output('Invalid Product Attribute: '||c2.product_attribute);
                    l_valid := 'N';  
                    begin                    
                        update  xxwc.XXWC_QP_MAINT_TOOL_LINE_STG
                        set     status = status||'.INVALID_PROD_ATTR'
                                , err_message = err_message||'.Invalid Prod Attr'
                        where   rowid = c2.rid;
                    exception
                    when others then
                        null;
                    end;
                end if;
                
                if l_valid = 'Y' then
                
                    -- Finding list line id    
                    begin
                        select  qll.list_line_id
                                , qpa.list_line_id
                                , qpa.pricing_attribute_id 
                        into    l_price_list_line.list_line_id
                                , l_pricing_att_rec.list_line_id
                                , l_pricing_att_rec.pricing_attribute_id
                        from    apps.qp_list_lines qll
                                , apps.qp_pricing_attributes qpa
                        where   qll.list_header_id = l_price_list_line.list_header_id
                        and     qll.list_line_type_code = 'PLL'
                        and     qll.start_date_active <= trunc(sysdate)
                        and     nvl(qll.end_date_active, trunc(sysdate)) >= trunc(sysdate)
                        and     qll.list_header_id = qpa.list_header_id
                        and     qll.qualification_ind = qpa.qualification_ind
                        and     qll.pricing_phase_id = qpa.pricing_phase_id
                        and     qll.list_line_id = qpa.list_line_id
                        and     qpa.product_attribute_context = 'ITEM'
                        and     ( 
                                    (c2.product_attribute = 'Item Number' and qpa.product_attribute = 'PRICING_ATTRIBUTE1')
                                    OR
                                    (c2.product_attribute = 'Item Category' and qpa.product_attribute = 'PRICING_ATTRIBUTE2')
                                    OR
                                    (c2.product_attribute = 'Pricing Category' and qpa.product_attribute = 'PRICING_ATTRIBUTE27')
                                )
                        and     qpa.product_attribute_datatype = 'C'
                        and     qpa.product_attr_value = to_char(l_prod_attr_value_id);
                        
                        if c2.obsolete_item = 'D' then
                            l_price_list_line.end_date_active       := trunc(sysdate);
                        else
                            if c2.price_type = 'Cost Plus' then
                                l_price_list_line.operand               := nvl(c2.list_price,1);
                                
                                begin
                                    select  price_formula_id
                                    into    l_price_list_line.price_by_formula_id
                                    from    apps.qp_price_formulas_tl
                                    where   name = 'NATIONAL WEIGHTED COST PLUS'
                                    and     rownum = 1;
                                exception
                                when others then
                                    l_price_list_line.price_by_formula_id   := 27061;   -- 26061
                                end;
                            else
                                l_price_list_line.operand               := nvl(c2.list_price,1);
                            end if;
                            l_price_list_line.arithmetic_operator   := 'UNIT_PRICE';
                        end if;
                        
                        l_pricing_att_rec.product_attr_value        := to_char(l_prod_attr_value_id);
                        l_pricing_att_rec.product_uom_code          := l_primary_uom_code;
                        l_pricing_att_rec.product_attribute_context := 'ITEM';
                        l_pricing_att_rec.product_attribute_datatype:= 'C';
                        
                        if c2.product_attribute = 'Item Number' then
                            l_pricing_att_rec.product_attribute         := 'PRICING_ATTRIBUTE1';
                        elsif c2.product_attribute = 'Item Category' then
                            l_pricing_att_rec.product_attribute         := 'PRICING_ATTRIBUTE2';
                        elsif c2.product_attribute = 'Pricing Category' then
                            l_pricing_att_rec.product_attribute         := 'PRICING_ATTRIBUTE27';
                        end if;
                        
                        l_price_list_line.operation                 := QP_GLOBALS.G_OPR_UPDATE;
                        l_pricing_att_rec.operation                 := QP_GLOBALS.G_OPR_UPDATE;
                        
                        l_valid := 'Y';
                    exception
                    when no_data_found then
                    
                        l_price_list_line.primary_uom_flag          := 'Y';
                        l_price_list_line.created_by                := g_user_id;
                        l_price_list_line.creation_date             := sysdate;
                        l_price_list_line.start_date_active         := trunc(sysdate);
                        l_price_list_line.list_line_type_code       := 'PLL';
                        l_price_list_line.arithmetic_operator       := 'UNIT_PRICE';
                        
                        if c2.price_type = 'Cost Plus' then
                            l_price_list_line.operand               := nvl(c2.list_price,1);
                            
                            begin
                                select  price_formula_id
                                into    l_price_list_line.price_by_formula_id
                                from    apps.qp_price_formulas_tl
                                where   name = 'NATIONAL WEIGHTED COST PLUS'
                                and     rownum = 1;
                            exception
                            when others then
                                l_price_list_line.price_by_formula_id   := 27061; -- 26061
                            end;
                        else
                            l_price_list_line.operand               := nvl(c2.list_price,1);
                        end if;
                    
                        l_pricing_att_rec.product_attr_value        := to_char(l_prod_attr_value_id);
                        l_pricing_att_rec.product_uom_code          := l_primary_uom_code;
                        l_pricing_att_rec.product_attribute_context := 'ITEM';
                        l_pricing_att_rec.product_attribute_datatype:= 'C';
                        
                        if c2.product_attribute = 'Item Number' then
                            l_pricing_att_rec.product_attribute         := 'PRICING_ATTRIBUTE1';
                            l_price_list_line.product_precedence        := 220;
                        elsif c2.product_attribute = 'Item Category' then
                            l_pricing_att_rec.product_attribute         := 'PRICING_ATTRIBUTE2';
                            l_price_list_line.product_precedence        := 290;
                        elsif c2.product_attribute = 'Pricing Category' then
                            l_pricing_att_rec.product_attribute         := 'PRICING_ATTRIBUTE27';
                            l_price_list_line.product_precedence        := 250;
                        end if;
                        
                        l_pricing_att_rec.created_by                := g_user_id;
                        l_pricing_att_rec.creation_date             := sysdate;                
                        l_pricing_att_rec.pricing_phase_id          := 1;
                    
                        l_pricing_att_rec.list_line_id              := FND_API.G_MISS_NUM;
                
                        SELECT qp_pricing_attributes_s.nextval
                        INTO   l_pricing_att_rec.pricing_attribute_id
                        FROM   dual;
                    
                        l_price_list_line.operation                 := QP_GLOBALS.G_OPR_CREATE;
                        l_pricing_att_rec.operation                 := QP_GLOBALS.G_OPR_CREATE;
                        
                        l_valid := 'Y';
                    when others then
                        l_valid := 'N';
                        write_output('Invalid QP Line for item: '||c2.item_number||'. Error: '||SQLERRM);
                        begin                    
                            update  xxwc.XXWC_QP_MAINT_TOOL_LINE_STG
                            set     status = status||'.INVALID_QP_LINE'
                                    , err_message = err_message||'.Invalid QP Line'
                            where   rowid = c2.rid;
                        exception
                        when others then
                            null;
                        end;
                        
                    end;
                end if;
                                
                if l_valid = 'Y' then 
                    l_pricing_attr_tbl(i)    := l_pricing_att_rec;
                    l_price_list_line_tbl(i) := l_price_list_line;
                    i := i + 1;
                    
                    begin                    
                        update  xxwc.XXWC_QP_MAINT_TOOL_LINE_STG
                        set     status = 'IN_PROCESS'
                        where   rowid = c2.rid;
                    exception
                    when others then
                        null;
                    end;
                end if;
                
                IF(i > l_commit_size) THEN
                    write_output ( 'Adding ' || i || ' price lines to price list');
                
                    qp_price_list_pub.process_price_list( p_api_version_number      => l_api_version_number,
                                                          p_init_msg_list           => l_init_msg_list,
                                                          p_return_values           => l_return_values,
                                                          p_commit                  => l_commit,
                                                          x_return_status           => l_x_return_status,
                                                          x_msg_count               => l_x_msg_count,
                                                          x_msg_data                => l_x_msg_data,
                                                          p_price_list_rec          => l_price_list_rec,
                                                          p_price_list_val_rec      => l_price_list_val_rec,
                                                          p_price_list_line_tbl     => l_price_list_line_tbl,
                                                          p_price_list_line_val_tbl => l_price_list_line_val_tbl,
                                                          p_qualifiers_tbl          => l_qualifiers_tbl,
                                                          p_qualifiers_val_tbl      => l_qualifiers_val_tbl,
                                                          p_pricing_attr_tbl        => l_pricing_attr_tbl,
                                                          p_pricing_attr_val_tbl    => l_pricing_attr_val_tbl_type,
                                                          x_price_list_rec          => l_x_price_list_rec,
                                                          x_price_list_val_rec      => l_x_price_list_val_rec,
                                                          x_price_list_line_tbl     => l_x_price_list_line_tbl,
                                                          x_price_list_line_val_tbl => l_x_price_list_line_val_tbl,
                                                          x_qualifiers_tbl          => l_x_qualifiers_tbl, 
                                                          x_qualifiers_val_tbl      => l_x_qualifiers_val_tbl,
                                                          x_pricing_attr_tbl        => l_x_pricing_attr_tbl,
                                                          x_pricing_attr_val_tbl    => l_x_pricing_attr_val_tbl);
                        
                    write_output ('Return status = ' || l_x_return_status);
                      
                    IF(l_x_return_status <> 'S') THEN
                        --  Error Processing
                        write_output ('  Message Count       = ' || l_x_msg_count);
                      
                        FOR K IN 1 .. l_x_msg_count LOOP 
                          gpr_msg_data := oe_msg_pub.get(p_msg_index => k, 
                                                         p_encoded => 'F'); 
                          write_output (TO_CHAR(i)||' ERR TEXT  '|| gpr_msg_data);        
                        END LOOP;
                        
                        -- 08/01/2013 CG: TMS 20130729-00576: Changed to prevent exit of program and processing of remaining records
                        -- RETURN;
                        begin                    
                            update  xxwc.XXWC_QP_MAINT_TOOL_LINE_STG
                            set     status = 'ERROR'
                            where   prl_name = c1.price_list_name
                            and     status = 'IN_PROCESS'
                            and     file_id = l_file_id
                            and     request_id = l_conc_req_id;
                        exception
                        when others then
                            null;
                        end;
                    
                    ELSE
                        begin                    
                            update  xxwc.XXWC_QP_MAINT_TOOL_LINE_STG
                            set     status = 'LOADED'
                            where   prl_name = c1.price_list_name
                            and     status = 'IN_PROCESS'
                            and     file_id = l_file_id
                            and     request_id = l_conc_req_id;
                        exception
                        when others then
                            null;
                        end;   
                    END IF;
                      
                    l_price_list_rec.operation := QP_GLOBALS.G_OPR_UPDATE;
                    COMMIT; 
                        
                    i := 1;
                    l_pricing_attr_tbl       := QP_PRICE_LIST_PUB.G_MISS_PRICING_ATTR_TBL;
                    l_price_list_line_tbl    := QP_PRICE_LIST_PUB.G_MISS_PRICE_LIST_LINE_TBL; 
                END IF;
                
            end loop;
            
            IF(i NOT IN (0, 1)) THEN
                write_output ('Adding ' || i || ' price lines to price list');
            
                qp_price_list_pub.process_price_list(  p_api_version_number      => l_api_version_number,
                                                       p_init_msg_list           => l_init_msg_list,
                                                       p_return_values           => l_return_values,
                                                       p_commit                  => l_commit,
                                                       x_return_status           => l_x_return_status,
                                                       x_msg_count               => l_x_msg_count,
                                                       x_msg_data                => l_x_msg_data,
                                                       p_price_list_rec          => l_price_list_rec,
                                                       p_price_list_val_rec      => l_price_list_val_rec,
                                                       p_price_list_line_tbl     => l_price_list_line_tbl,
                                                       p_price_list_line_val_tbl => l_price_list_line_val_tbl,
                                                       p_qualifiers_tbl          => l_qualifiers_tbl,
                                                       p_qualifiers_val_tbl      => l_qualifiers_val_tbl,
                                                       p_pricing_attr_tbl        => l_pricing_attr_tbl,
                                                       p_pricing_attr_val_tbl    => l_pricing_attr_val_tbl_type,
                                                       x_price_list_rec          => l_x_price_list_rec,
                                                       x_price_list_val_rec      => l_x_price_list_val_rec,
                                                       x_price_list_line_tbl     => l_x_price_list_line_tbl,
                                                       x_price_list_line_val_tbl => l_x_price_list_line_val_tbl,
                                                       x_qualifiers_tbl          => l_x_qualifiers_tbl, 
                                                       x_qualifiers_val_tbl      => l_x_qualifiers_val_tbl,
                                                       x_pricing_attr_tbl        => l_x_pricing_attr_tbl,
                                                       x_pricing_attr_val_tbl    => l_x_pricing_attr_val_tbl);
                  
                write_output ('Return status = ' || l_x_return_status);
                
                IF(l_x_return_status <> 'S') THEN
                    --  Error Processing
                    write_output ('  Message Count       = ' || l_x_msg_count);
              
                    FOR K IN 1 .. l_x_msg_count 
                    LOOP 
                        gpr_msg_data := oe_msg_pub.get(p_msg_index => k, p_encoded => 'F'); 
                        write_output (TO_CHAR(i)||' ERR TEXT  '|| gpr_msg_data);        
                    END LOOP;
                    
                    -- 08/01/2013 CG: TMS 20130729-00576: Changed to prevent exit of program and processing of remaining records
                    -- RETURN;
                    begin                    
                        update  xxwc.XXWC_QP_MAINT_TOOL_LINE_STG
                        set     status = 'ERROR'
                        where   prl_name = c1.price_list_name
                        and     status = 'IN_PROCESS'
                        and     file_id = l_file_id
                        and     request_id = l_conc_req_id;
                    exception
                    when others then
                        null;
                    end;
                
                ELSE
                    begin                    
                        update  xxwc.XXWC_QP_MAINT_TOOL_LINE_STG
                        set     status = 'LOADED'
                        where   prl_name = c1.price_list_name
                        and     status = 'IN_PROCESS'
                        and     file_id = l_file_id
                        and     request_id = l_conc_req_id;
                    exception
                    when others then
                        null;
                    end;
                    
                END IF;
                
                l_price_list_rec.operation := QP_GLOBALS.G_OPR_UPDATE;
                
                COMMIT;
            END IF;
            
            IF (l_x_return_status = 'S') THEN
                -- updating loading pricelist lines
                update  xxwc.XXWC_QP_MAINT_TOOL_LINE_STG
                set     status = 'LOADED'
                where   prl_name = c1.price_list_name
                and     status = 'IN_PROCESS'
                and     file_id = l_file_id
                and     request_id = l_conc_req_id;
            END IF;                                
           
        END IF; -- End if for l_process flag
        
    end loop;
    
    begin
        update  XXWC.XXWC_QP_MAINT_TOOL_STG
        set     status = 'Processed'
                , last_update_date = sysdate
                , last_updated_by = fnd_global.user_id
                , last_update_login = l_login_id 
        where   file_id = l_file_id
        and     processing_request_id = l_conc_req_id;
    exception
    when others then
        null;
    end;
    
    commit;  
    
    write_output('Completed data processing and load to EBS');
    l_end_time := DBMS_UTILITY.get_time;
    write_log ('l_end_time : '||l_end_time );
    write_log ('QP Processing Time: '||to_char(l_end_time-l_start_time));
    write_output ('QP Processing Time: '||to_char(l_end_time-l_start_time));
    
    commit;

EXCEPTION
WHEN NO_DATA_FOUND THEN
    BEGIN
        is_file_open             := NULL;
        is_file_open             := DBMS_LOB.ISOPEN (l_file);

        IF is_file_open = 1 THEN
            DBMS_LOB.close (l_file);
            COMMIT;
        END IF;
    END;
      
    l_msg_data := substr(SQLERRM, 1, 4000);
    Write_log ('XXWC_QP_NTL_PRL_MAINT_TOOL. No Data Found Excep: '||l_msg_data);
    errmsg := l_msg_data;
    retcode := '2';  
      
WHEN OTHERS THEN
    BEGIN
        is_file_open             := NULL;
        is_file_open             := DBMS_LOB.ISOPEN (l_file);

        IF is_file_open = 1 THEN
            DBMS_LOB.close (l_file);
            COMMIT;
        END IF;
    END;
    
    l_error_message2 := 'XXWC_QP_NTL_PRL_MAINT_TOOL '
                || 'Error_Stack...'
                || DBMS_UTILITY.format_error_stack ()
                || ' Error_Backtrace...'
                || DBMS_UTILITY.format_error_backtrace ();

    xxcus_error_pkg.xxcus_error_main_api (
        p_called_from         => 'xxwc_qp_market_price_util_pkg.XXWC_QP_NTL_PRL_MAINT_TOOL'
       ,p_calling             => 'xxwc_qp_market_price_util_pkg.XXWC_QP_NTL_PRL_MAINT_TOOL'
       ,p_request_id          => l_conc_req_id
       ,p_ora_error_msg       => l_error_message2
       ,p_error_desc          => 'Error running XXWC QP National PRL Setup Processor'
       ,p_distribution_list   => 'HDSOracleDevelopers@hdsupply.com'
       ,p_module              => 'QP');

    write_log('Error in main XXWC_QP_NTL_PRL_MAINT_TOOL: '||l_error_message2);
    
    errmsg := l_error_message2;
    retcode := '2';  
END XXWC_QP_NTL_PRL_MAINT_TOOL;

FUNCTION get_market_list_price (p_inventory_item_id IN NUMBER
                                , p_organization_id IN NUMBER) RETURN NUMBER
IS
    l_list_price    number;
BEGIN
    l_list_price := null;

    begin     
        /*SELECT  ll.operand
        INTO    l_list_price
        FROM    qp_secu_list_headers_v lh, 
                qp_qualifiers qq,
                qp_list_lines_v ll, 
                mtl_system_items a
        WHERE   lh.list_type_code = 'PRL'
        AND     lh.attribute10 = 'Market Price List'
        AND     lh.list_header_id = qq.list_header_id
        AND     lh.list_header_id != G_MARKET_NTL_PRL_ID
        AND     qq.list_line_id = -1
        AND     qq.qualifier_context = 'ORDER'
        AND     qq.qualifier_attribute = 'QUALIFIER_ATTRIBUTE18'
        AND     qq.qualifier_attr_value = to_char(p_organization_id)
        AND     SYSDATE BETWEEN NVL (qq.start_date_active, SYSDATE - 1) AND NVL (qq.end_date_active, SYSDATE + 1)
        AND     lh.list_header_id = ll.list_header_id
        AND     SYSDATE BETWEEN NVL (ll.start_date_active, SYSDATE - 1) AND NVL (ll.end_date_active, SYSDATE + 1)
        AND     ll.product_id = a.inventory_item_id
        AND     a.organization_id = p_organization_id
        AND     a.inventory_item_id = p_inventory_item_id
        AND     ROWNUM = 1;*/
        
        -- 07/24/2013 CG: TMS 20130715-00357: Updated to help AIS Performance
        SELECT  ll.operand
        INTO    l_list_price
        FROM    apps.qp_list_headers_b lh, 
                apps.qp_qualifiers qq,
                apps.qp_list_lines ll,
                apps.qp_pricing_attributes qpa
        WHERE   lh.list_type_code = 'PRL'
        -- 08/08/2013 CG: TMS 20130729-00576: Changed to also exclude Market Category
        -- AND     lh.list_header_id != G_MARKET_NTL_PRL_ID
        AND     lh.list_header_id not in (G_MARKET_NTL_PRL_ID, G_MARKET_CAT_PRL_ID)
        AND     lh.attribute10 = 'Market Price List'
        AND     lh.list_header_id = qq.list_header_id
        AND     qq.list_line_id = -1
        AND     qq.qualifier_context = 'ORDER'
        AND     qq.qualifier_attribute = 'QUALIFIER_ATTRIBUTE18'
        AND     qq.qualifier_attr_value = to_char(p_organization_id)
        AND     SYSDATE BETWEEN NVL (qq.start_date_active, SYSDATE - 1) AND NVL (qq.end_date_active, SYSDATE + 1)
        AND     lh.list_header_id = ll.list_header_id
        AND     SYSDATE BETWEEN NVL (ll.start_date_active, SYSDATE - 1) AND NVL (ll.end_date_active, SYSDATE + 1)
        AND     ll.list_header_id  = qpa.list_header_id 
        AND     ll.qualification_ind  = qpa.qualification_ind 
        AND     ll.pricing_phase_id = qpa.pricing_phase_id 
        AND     ll.list_line_id = qpa.list_line_id 
        AND     qpa.product_attribute_context = 'ITEM'
        AND     qpa.product_attribute = 'PRICING_ATTRIBUTE1'
        AND     qpa.product_attribute_datatype = 'C'
        AND     qpa.product_attr_value = to_char(p_inventory_item_id)
        AND     ROWNUM = 1;       
    exception
    when others then
        begin
            /*SELECT  ll.operand
            INTO    l_list_price
            FROM    qp_secu_list_headers_v lh, 
                    qp_list_lines_v ll, 
                    mtl_system_items a
            WHERE   lh.list_type_code = 'PRL'
            --AND     lh.attribute10 = 'Market Price List'
            AND     lh.list_header_id = G_MARKET_NTL_PRL_ID
            AND     lh.list_header_id = ll.list_header_id
            AND     SYSDATE BETWEEN NVL (ll.start_date_active, SYSDATE - 1) AND NVL (ll.end_date_active, SYSDATE + 1)
            AND     ll.product_id = a.inventory_item_id
            AND     a.organization_id=p_organization_id
            AND     a.inventory_item_id = p_inventory_item_id
            AND     ROWNUM = 1;*/
            
            -- 07/24/2013 CG: TMS 20130715-00357: Updated to help AIS Performance
            SELECT  ll.operand
            INTO    l_list_price
            FROM    apps.qp_list_headers_b lh,
                    apps.qp_list_lines ll,
                    apps.qp_pricing_attributes qpa
            WHERE   lh.list_header_id = G_MARKET_NTL_PRL_ID
            AND     lh.list_header_id = ll.list_header_id
            AND     SYSDATE BETWEEN NVL (ll.start_date_active, SYSDATE - 1) AND NVL (ll.end_date_active, SYSDATE + 1)
            AND     ll.list_header_id  = qpa.list_header_id 
            AND     ll.qualification_ind  = qpa.qualification_ind 
            AND     ll.pricing_phase_id = qpa.pricing_phase_id 
            AND     ll.list_line_id = qpa.list_line_id 
            AND     qpa.product_attribute_context = 'ITEM'
            AND     qpa.product_attribute = 'PRICING_ATTRIBUTE1'
            AND     qpa.product_attribute_datatype = 'C'
            AND     qpa.product_attr_value = to_char(p_inventory_item_id)
            AND     ROWNUM = 1; 
        exception
        when others then
            -- 08/08/2013 CG: TMS 20130729-00576: Added to consider new level pricing for Market Category
            begin            
                SELECT  ll.operand
                INTO    l_list_price
                FROM    apps.qp_list_headers_b lh,
                        apps.qp_list_lines ll,
                        apps.qp_pricing_attributes qpa
                WHERE   lh.list_header_id = G_MARKET_CAT_PRL_ID
                AND     lh.list_header_id = ll.list_header_id
                AND     SYSDATE BETWEEN NVL (ll.start_date_active, SYSDATE - 1) AND NVL (ll.end_date_active, SYSDATE + 1)
                AND     ll.list_header_id  = qpa.list_header_id 
                AND     ll.qualification_ind  = qpa.qualification_ind 
                AND     ll.pricing_phase_id = qpa.pricing_phase_id 
                AND     ll.list_line_id = qpa.list_line_id 
                AND     qpa.product_attribute_context = 'ITEM'
                AND     qpa.product_attribute = 'PRICING_ATTRIBUTE1'
                AND     qpa.product_attribute_datatype = 'C'
                AND     qpa.product_attr_value = to_char(p_inventory_item_id)
                AND     ROWNUM = 1;     
            exception
            when others then
                l_list_price := 20000;
            end;
        end;
    end;

    return nvl(l_list_price, 0);
EXCEPTION
WHEN OTHERS THEN
    return 20000;
END get_market_list_price;

-- 07/08/2013 CG: Added new procedure for program XXWC QP Pricing For New Product Release
PROCEDURE xxwc_price_new_prod_rel (RETCODE     OUT NUMBER
                                   , ERRMSG    OUT VARCHAR2
                                   , P_INVENTORY_ITEM_ID  IN NUMBER
                                   , P_UNIT_PRICE  IN NUMBER)
IS
    l_error_message2 clob;
    l_conc_req_id   number := fnd_global.conc_request_id;
    l_start_time    number;  
    l_end_time      number;
    l_login_id      number := fnd_global.login_id;
    
    l_upload            varchar2(1);
    l_load_ntl          varchar2(1);
    l_load_ntl_setup    varchar2(1);
    
    l_list_price        number;
    l_item_cat_class    number;
    l_valid             varchar2(1);
    l_operand           number;
    l_upload_price      number;
    l_count             number;
    
    i       number;
    k       number;
    gpr_msg_data                    VARCHAR2(2000);
    l_api_version_number            NUMBER         := 1.0;
    l_init_msg_list                 VARCHAR2(10)   := FND_API.G_FALSE;
    l_return_values                 VARCHAR2(10)   := FND_API.G_FALSE;
    l_commit                        VARCHAR2(10)   := FND_API.G_TRUE;
    l_x_return_status               VARCHAR2(255)  := 'S';
    l_x_msg_count                   NUMBER;
    l_x_msg_data                    VARCHAR2(255);
    g_user_id                       NUMBER;
    l_price_list_rec                QP_PRICE_LIST_PUB.PRICE_LIST_REC_TYPE          := QP_PRICE_LIST_PUB.G_MISS_PRICE_LIST_REC;
    l_price_list_val_rec            QP_PRICE_LIST_PUB.PRICE_LIST_VAL_REC_TYPE      := QP_PRICE_LIST_PUB.G_MISS_PRICE_LIST_VAL_REC;
    l_price_list_line               QP_PRICE_LIST_PUB.PRICE_LIST_LINE_REC_TYPE    := QP_PRICE_LIST_PUB.G_MISS_PRICE_LIST_LINE_REC;
    l_price_list_line_tbl           QP_PRICE_LIST_PUB.PRICE_LIST_LINE_TBL_TYPE     := QP_PRICE_LIST_PUB.G_MISS_PRICE_LIST_LINE_TBL; 
    l_price_list_line_val_tbl       QP_PRICE_LIST_PUB.PRICE_LIST_LINE_VAL_TBL_TYPE := QP_PRICE_LIST_PUB.G_MISS_PRICE_LIST_LINE_VAL_TBL;
    l_qualifiers_tbl                QP_QUALIFIER_RULES_PUB.QUALIFIERS_TBL_TYPE     := QP_PRICE_LIST_PUB.G_MISS_QUALIFIERS_TBL;
    l_qualifiers_val_tbl            QP_QUALIFIER_RULES_PUB.QUALIFIERS_VAL_TBL_TYPE := QP_PRICE_LIST_PUB.G_MISS_QUALIFIERS_VAL_TBL;
    l_pricing_att_rec               QP_PRICE_LIST_PUB.PRICING_ATTR_REC_TYPE := QP_PRICE_LIST_PUB.G_MISS_PRICING_ATTR_REC;
    l_pricing_attr_tbl              QP_PRICE_LIST_PUB.PRICING_ATTR_TBL_TYPE        := QP_PRICE_LIST_PUB.G_MISS_PRICING_ATTR_TBL;
    l_pricing_attr_val_tbl_type     QP_PRICE_LIST_PUB.PRICING_ATTR_VAL_TBL_TYPE    := QP_PRICE_LIST_PUB.G_MISS_PRICING_ATTR_VAL_TBL;
    l_x_price_list_rec              QP_PRICE_LIST_PUB.PRICE_LIST_REC_TYPE;
    l_x_price_list_val_rec          QP_PRICE_LIST_PUB.PRICE_LIST_VAL_REC_TYPE;
    l_x_price_list_line_tbl         QP_PRICE_LIST_PUB.PRICE_LIST_LINE_TBL_TYPE;
    l_x_price_list_line_val_tbl     QP_PRICE_LIST_PUB.PRICE_LIST_LINE_VAL_TBL_TYPE;
    l_x_qualifiers_tbl              QP_QUALIFIER_RULES_PUB.QUALIFIERS_TBL_TYPE;
    l_x_qualifiers_val_tbl          QP_QUALIFIER_RULES_PUB.QUALIFIERS_VAL_TBL_TYPE;
    l_x_pricing_attr_tbl            QP_PRICE_LIST_PUB.PRICING_ATTR_TBL_TYPE;
    l_x_pricing_attr_val_tbl        QP_PRICE_LIST_PUB.PRICING_ATTR_VAL_TBL_TYPE;
    
    l_error_message_list            ERROR_HANDLER.ERROR_TBL_TYPE;
    
    no_list_price           EXCEPTION;
    no_cat_class            EXCEPTION;
    cat_class_not_on_prl    EXCEPTION;
BEGIN
    write_output('Starting XXWC QP Pricing For New Product Release');
    write_output('---------------------------------------------------------------');
    
    -- Add parameters to the output
    
    l_start_time := DBMS_UTILITY.get_time;
    write_log ('l_Start_time: '||l_start_time);
    
    l_upload := null;
    l_upload_price := null;
    if P_INVENTORY_ITEM_ID is not null and P_UNIT_PRICE is not null then
    
        write_output('Item and price passed to program');    
    
        l_load_ntl_setup := null;
        l_load_ntl := null;
        
        -- Checking if it's in NTL Setup
        begin
            select  'Y'
            into    l_load_ntl_setup
            from    apps.qp_list_lines qll
                    , apps.qp_pricing_attributes qpa
            where   qll.list_header_id = G_MARKET_NTL_STP_PRL_ID
            and     qll.list_line_type_code = 'PLL'
            and     qll.start_date_active <= trunc(sysdate)
            and     nvl(qll.end_date_active, trunc(sysdate)) >= trunc(sysdate)
            and     qll.list_header_id = qpa.list_header_id
            and     qll.qualification_ind = qpa.qualification_ind
            and     qll.pricing_phase_id = qpa.pricing_phase_id
            and     qll.list_line_id = qpa.list_line_id
            and     qpa.product_attribute_context = 'ITEM'
            and     qpa.product_attribute = 'PRICING_ATTRIBUTE1'
            and     qpa.product_attribute_datatype = 'C'
            and     qpa.product_attr_value = to_char(P_INVENTORY_ITEM_ID);
        exception
        when others then
            l_load_ntl_setup := 'N';
        end;
        
        -- Checking if it's in NTL
        begin
            select  'Y'
            into    l_load_ntl
            from    apps.qp_list_lines qll
                    , apps.qp_pricing_attributes qpa
            where   qll.list_header_id = G_MARKET_NTL_PRL_ID
            and     qll.list_line_type_code = 'PLL'
            and     qll.start_date_active <= trunc(sysdate)
            and     nvl(qll.end_date_active, trunc(sysdate)) >= trunc(sysdate)
            and     qll.list_header_id = qpa.list_header_id
            and     qll.qualification_ind = qpa.qualification_ind
            and     qll.pricing_phase_id = qpa.pricing_phase_id
            and     qll.list_line_id = qpa.list_line_id
            and     qpa.product_attribute_context = 'ITEM'
            and     qpa.product_attribute = 'PRICING_ATTRIBUTE1'
            and     qpa.product_attribute_datatype = 'C'
            and     qpa.product_attr_value = to_char(P_INVENTORY_ITEM_ID);
        exception
        when others then
            l_load_ntl := 'N';
        end;
        
        if nvl(l_load_ntl_setup,'N') = 'N' or nvl(l_load_ntl,'N') = 'N' then
            l_upload := 'Y';
            l_upload_price := P_UNIT_PRICE;
        else
            write_output('Item might already exist on the National or National Setup Price List');
        end if;
    
        write_output('l_upload: '||l_upload);
    
    else
        -- Checking second set of rules based on PO Cost
        
        -- 1. Retrieve Item List Price (Distinct and Rownum = 1)
        l_list_price := null;
        begin
            select  list_price_per_unit
            into    l_list_price
            from    apps.mtl_system_items_b
            where   inventory_item_id = p_inventory_item_id
            and     list_price_per_unit is not null
            and     rownum = 1; 
            
            l_count := null;
            
            begin
                select  count(*)
                into    l_count
                from    xxwc.xxwc_qp_wght_ntl_avg_cost_stg
                where   inventory_item_id = p_inventory_item_id;
            exception
            when others then
                l_count := -1;
            end;
            
            if nvl (l_count,-1) = 0 then
                begin
                    insert into xxwc.xxwc_qp_wght_ntl_avg_cost_stg (
                            inventory_item_id
                            , creation_date
                            , created_by
                            , last_update_date
                            , last_updated_by
                            , last_update_login
                            , request_id
                            , list_header_id
                            , wght_ntl_avg_cost
                        ) values (
                            p_inventory_item_id
                            , sysdate
                            , fnd_global.user_id
                            , sysdate
                            , fnd_global.user_id
                            , l_login_id
                            , l_conc_req_id
                            , G_MARKET_NTL_STP_PRL_ID
                            , l_list_price
                        );
                exception
                when others then
                    write_output('Could not insert into Weighted National Avg Cost Table');
                    errmsg := 'Could not insert into Weighted National Avg Cost Table';
                    retcode := '2';
                    raise;  
                end;
            end if;
            
            write_output('l_list_price: '||l_list_price);
              
        exception
        when no_data_found then
            write_output('List price not found for item');
            errmsg := 'List price not found for item';
            retcode := '2';
            raise;
        when too_many_rows then
            write_output('Multiple list price found for item');
            errmsg := 'Multiple list price found for item';
            retcode := '2';
            raise;
        when others then
            write_output('Other error pulling list price. Error: '||SQLERRM);
            errmsg := 'Other error pulling list price. Error: '||SQLERRM;
            retcode := '2';
            raise;
        end;
        
        -- 2. Find item cat class
        if l_list_price is not null then
            
            l_item_cat_class := null;
            begin
                select  mcb.category_id
                into    l_item_cat_class
                from    apps.mtl_categories_v mcb
                        , apps.mtl_category_sets mcs
                where   mcb.category_concat_segs = apps.xxwc_mv_routines_pkg.get_item_category (p_inventory_item_id, 222, 'Inventory Category', 0)
                and     mcb.structure_id = mcs.structure_id
                and     mcs.category_set_name = 'Inventory Category'
                and     rownum = 1;
            exception
            when no_data_found then
                l_item_cat_class := null;
                write_output('Item Cat Class not found for item');
                errmsg := 'Item Cat Class not found for item';
                retcode := '2';
                raise;
            when others then
                l_item_cat_class := null;
                write_output('Other error pulling item cat class. Error: '||SQLERRM);
                errmsg := 'Other error pulling item cat class. Error: '||SQLERRM;
                retcode := '2';
                raise;
            end;
        
            write_output('l_item_cat_class: '||l_item_cat_class);
        
            -- 3. Check to see if cat class is in the Ntl Setup PRL
            if l_item_cat_class is not null then
                
                l_valid := null;
                l_operand   := null;
                begin                
                    select  'Y'
                            , qll.operand
                    into    l_valid 
                            , l_operand
                    from    apps.qp_list_lines qll
                            , apps.qp_pricing_attributes qpa
                    where   qll.list_header_id = G_MARKET_NTL_STP_PRL_ID
                    and     qll.list_line_type_code = 'PLL'
                    and     qll.start_date_active <= trunc(sysdate)
                    and     nvl(qll.end_date_active, trunc(sysdate)) >= trunc(sysdate)
                    and     qll.list_header_id = qpa.list_header_id
                    and     qll.qualification_ind = qpa.qualification_ind
                    and     qll.pricing_phase_id = qpa.pricing_phase_id
                    and     qll.list_line_id = qpa.list_line_id
                    and     qpa.product_attribute_context = 'ITEM'
                    and     qpa.product_attribute = 'PRICING_ATTRIBUTE2' -- Item Category
                    and     qpa.product_attribute_datatype = 'C'
                    and     qpa.product_attr_value = to_char(l_item_cat_class);
                exception
                when no_data_found then
                    l_valid := 'N';
                    l_operand := null;
                    write_output('Item Cat Class not found on National Setup Price List');
                    errmsg := 'Item Cat Class not found on National Setup Price List';
                    retcode := 2;
                    raise;
                when others then
                    l_valid := 'N';
                    l_operand := null;
                    write_output('Other error finding item cat class on National Setup Price List. Error: '||SQLERRM);
                    errmsg := 'Other error finding item cat class on National Setup Price List. Error: '||SQLERRM;
                    retcode := 2;
                    raise;
                end;
                
                write_output('l_operand: '||l_operand);          
                      
                -- 4. If the first 3 pass calc SKU Level Price based on cat class markup on NTL Setup
                if nvl(l_valid, 'N') = 'Y' and l_operand is not null then
                
                    begin
                        select  ( l_list_price / ( 1 - (l_operand/100) ) )
                        into    l_upload_price
                        from    dual; 
                        
                        l_upload := 'Y';
                        
                        write_output('l_upload_price: '||l_upload_price); 
                        
                    exception
                    when others then
                        l_upload := 'N';
                        write_output('Could not calculate new price for item. Error: '||SQLERRM);
                        errmsg := 'Could not calculate new price for item. Error: '||SQLERRM;
                        retcode := '2';
                        raise;
                    end;    
                
                end if;
                
            end if;
            
        end if;
        
        l_load_ntl_setup := null;
        l_load_ntl := null;
        
        -- Checking if it's in NTL Setup
        begin
            select  'Y'
            into    l_load_ntl_setup
            from    apps.qp_list_lines qll
                    , apps.qp_pricing_attributes qpa
            where   qll.list_header_id = G_MARKET_NTL_STP_PRL_ID
            and     qll.list_line_type_code = 'PLL'
            and     qll.start_date_active <= trunc(sysdate)
            and     nvl(qll.end_date_active, trunc(sysdate)) >= trunc(sysdate)
            and     qll.list_header_id = qpa.list_header_id
            and     qll.qualification_ind = qpa.qualification_ind
            and     qll.pricing_phase_id = qpa.pricing_phase_id
            and     qll.list_line_id = qpa.list_line_id
            and     qpa.product_attribute_context = 'ITEM'
            and     qpa.product_attribute = 'PRICING_ATTRIBUTE1'
            and     qpa.product_attribute_datatype = 'C'
            and     qpa.product_attr_value = to_char(P_INVENTORY_ITEM_ID);
        exception
        when others then
            l_load_ntl_setup := 'N';
        end;
        
        -- Checking if it's in NTL
        begin
            select  'Y'
            into    l_load_ntl
            from    apps.qp_list_lines qll
                    , apps.qp_pricing_attributes qpa
            where   qll.list_header_id = G_MARKET_NTL_PRL_ID
            and     qll.list_line_type_code = 'PLL'
            and     qll.start_date_active <= trunc(sysdate)
            and     nvl(qll.end_date_active, trunc(sysdate)) >= trunc(sysdate)
            and     qll.list_header_id = qpa.list_header_id
            and     qll.qualification_ind = qpa.qualification_ind
            and     qll.pricing_phase_id = qpa.pricing_phase_id
            and     qll.list_line_id = qpa.list_line_id
            and     qpa.product_attribute_context = 'ITEM'
            and     qpa.product_attribute = 'PRICING_ATTRIBUTE1'
            and     qpa.product_attribute_datatype = 'C'
            and     qpa.product_attr_value = to_char(P_INVENTORY_ITEM_ID);
        exception
        when others then
            l_load_ntl := 'N';
        end;
        
    end if;
    
    write_output('l_upload: '||l_upload);
    write_output('l_load_ntl_setup: '||l_load_ntl_setup);
    write_output('l_load_ntl: '||l_load_ntl);
    
    commit;
    
    if nvl (l_upload, 'N') = 'Y' then
        
        -- Load into National Setup Price List
        if nvl(l_load_ntl_setup, 'N') = 'N' then
            write_output ('Loading into National Setup Price List...');
            
            l_price_list_rec        := QP_PRICE_LIST_PUB.G_MISS_PRICE_LIST_REC;

            l_price_list_rec.list_header_id     := G_MARKET_NTL_STP_PRL_ID;
            l_price_list_rec.operation          := QP_GLOBALS.G_OPR_UPDATE;
            l_price_list_rec.last_updated_by    := fnd_global.user_id;
            l_price_list_rec.last_update_date   := SYSDATE;

            i := 1;
            l_pricing_attr_tbl      := QP_PRICE_LIST_PUB.G_MISS_PRICING_ATTR_TBL;
            l_price_list_line_tbl   := QP_PRICE_LIST_PUB.G_MISS_PRICE_LIST_LINE_TBL;

            l_price_list_line       := QP_PRICE_LIST_PUB.G_MISS_PRICE_LIST_LINE_REC;
            l_pricing_att_rec       := QP_PRICE_LIST_PUB.G_MISS_PRICING_ATTR_REC;

            l_price_list_line.list_header_id            := l_price_list_rec.list_header_id;
            l_price_list_line.primary_uom_flag          := 'Y';
            l_price_list_line.start_date_active         := trunc(sysdate);
            l_price_list_line.list_line_type_code       := 'PLL';
            l_price_list_line.arithmetic_operator       := 'UNIT_PRICE';
            
            -- 08/08/2013 CG: TMS 20130729-00576: Changed to include new rounding requirements
            -- l_price_list_line.operand                   := nvl(l_upload_price,1);
            begin            
                select  (case 
                            when l_upload_price <= 0.01 then round (l_upload_price,5)
                            when l_upload_price between 0.01 and 1 then round((ceil(l_upload_price*100)/100),2)
                            when l_upload_price > 1 then ((ceil(l_upload_price*10)/10) - 0.01) 
                         else round(l_upload_price,5)
                         end) new_value
                into     l_price_list_line.operand 
                from    dual;                
            exception
            when others then
                l_price_list_line.operand                   := nvl(l_upload_price,1);
            end;
            
            l_price_list_line.product_precedence        := 220;
            l_price_list_line.created_by                := fnd_global.user_id;
            l_price_list_line.creation_date             := sysdate;
            l_price_list_line.last_updated_by           := fnd_global.user_id;
            l_price_list_line.last_update_date          := sysdate;
                                                
            l_pricing_att_rec.list_header_id            := l_price_list_line.list_header_id;
            l_pricing_att_rec.product_attribute_context := 'ITEM';
            l_pricing_att_rec.product_attribute         := 'PRICING_ATTRIBUTE1';
            l_pricing_att_rec.product_attribute_datatype:= 'C';
            l_pricing_att_rec.last_updated_by           := fnd_global.user_id;
            l_pricing_att_rec.last_update_date          := sysdate;
            l_pricing_att_rec.created_by                := fnd_global.user_id;
            l_pricing_att_rec.creation_date             := sysdate;                
            l_pricing_att_rec.pricing_phase_id          := 1;
            l_pricing_att_rec.price_list_line_index     := i;

            begin
                select  to_char(msib.inventory_item_id)
                        , msib.primary_uom_code
                into    l_pricing_att_rec.product_attr_value
                        , l_pricing_att_rec.product_uom_code 
                from    apps.mtl_system_items_b msib
                        , apps.mtl_parameters mp
                where   msib.inventory_item_id = p_inventory_item_id
                and     msib.organization_id = mp.organization_id
                and     mp.organization_code = 'MST';
            exception
            when others then
                null;
            end;
            
            l_pricing_att_rec.list_line_id              := FND_API.G_MISS_NUM;
            
            SELECT qp_pricing_attributes_s.nextval
            INTO   l_pricing_att_rec.pricing_attribute_id
            FROM   dual;
            
            l_price_list_line.operation                 := QP_GLOBALS.G_OPR_CREATE;
            l_pricing_att_rec.operation                 := QP_GLOBALS.G_OPR_CREATE;           
                            
            l_pricing_attr_tbl(i)    := l_pricing_att_rec;
            l_price_list_line_tbl(i) := l_price_list_line;
            i := i + 1;

            qp_price_list_pub.process_price_list( p_api_version_number      => l_api_version_number,
                                                  p_init_msg_list           => l_init_msg_list,
                                                  p_return_values           => l_return_values,
                                                  p_commit                  => l_commit,
                                                  x_return_status           => l_x_return_status,
                                                  x_msg_count               => l_x_msg_count,
                                                  x_msg_data                => l_x_msg_data,
                                                  p_price_list_rec          => l_price_list_rec,
                                                  p_price_list_val_rec      => l_price_list_val_rec,
                                                  p_price_list_line_tbl     => l_price_list_line_tbl,
                                                  p_price_list_line_val_tbl => l_price_list_line_val_tbl,
                                                  p_qualifiers_tbl          => l_qualifiers_tbl,
                                                  p_qualifiers_val_tbl      => l_qualifiers_val_tbl,
                                                  p_pricing_attr_tbl        => l_pricing_attr_tbl,
                                                  p_pricing_attr_val_tbl    => l_pricing_attr_val_tbl_type,
                                                  x_price_list_rec          => l_x_price_list_rec,
                                                  x_price_list_val_rec      => l_x_price_list_val_rec,
                                                  x_price_list_line_tbl     => l_x_price_list_line_tbl,
                                                  x_price_list_line_val_tbl => l_x_price_list_line_val_tbl,
                                                  x_qualifiers_tbl          => l_x_qualifiers_tbl, 
                                                  x_qualifiers_val_tbl      => l_x_qualifiers_val_tbl,
                                                  x_pricing_attr_tbl        => l_x_pricing_attr_tbl,
                                                  x_pricing_attr_val_tbl    => l_x_pricing_attr_val_tbl);
                        
            write_output ('Return status = ' || l_x_return_status);
                      
            IF(l_x_return_status <> 'S') THEN
                --  Error Processing
                write_output ('  Message Count       = ' || l_x_msg_count);
                      
                FOR K IN 1 .. l_x_msg_count LOOP 
                  gpr_msg_data := oe_msg_pub.get(p_msg_index => k, 
                                                 p_encoded => 'F'); 
                  write_output (TO_CHAR(i)||' ERR TEXT  '|| gpr_msg_data);        
                END LOOP;
                RETURN;
                    
            ELSE
                null;   
            END IF;
            
        end if;
        
        -- Load into National Market Price List
        if nvl(l_load_ntl, 'N') = 'N' then
            write_output ('Loading into National Market Price List...');
            
            l_price_list_rec        := QP_PRICE_LIST_PUB.G_MISS_PRICE_LIST_REC;

            l_price_list_rec.list_header_id     := G_MARKET_NTL_PRL_ID;
            l_price_list_rec.operation          := QP_GLOBALS.G_OPR_UPDATE;
            l_price_list_rec.last_updated_by    := fnd_global.user_id;
            l_price_list_rec.last_update_date   := SYSDATE;

            i := 1;
            l_pricing_attr_tbl      := QP_PRICE_LIST_PUB.G_MISS_PRICING_ATTR_TBL;
            l_price_list_line_tbl   := QP_PRICE_LIST_PUB.G_MISS_PRICE_LIST_LINE_TBL;

            l_price_list_line       := QP_PRICE_LIST_PUB.G_MISS_PRICE_LIST_LINE_REC;
            l_pricing_att_rec       := QP_PRICE_LIST_PUB.G_MISS_PRICING_ATTR_REC;

            l_price_list_line.list_header_id            := l_price_list_rec.list_header_id;
            l_price_list_line.primary_uom_flag          := 'Y';
            l_price_list_line.start_date_active         := trunc(sysdate);
            l_price_list_line.list_line_type_code       := 'PLL';
            l_price_list_line.arithmetic_operator       := 'UNIT_PRICE';
            
            -- 08/08/2013 CG: TMS 20130729-00576: Changed to include new rounding requirements
            -- l_price_list_line.operand                   := nvl(l_upload_price,1);
            begin            
                select  (case 
                            when l_upload_price <= 0.01 then round (l_upload_price,5)
                            when l_upload_price between 0.01 and 1 then round((ceil(l_upload_price*100)/100),2)
                            when l_upload_price > 1 then ((ceil(l_upload_price*10)/10) - 0.01) 
                         else round(l_upload_price,5)
                         end) new_value
                into     l_price_list_line.operand 
                from    dual;                
            exception
            when others then
                l_price_list_line.operand                   := nvl(l_upload_price,1);
            end;
            
            l_price_list_line.product_precedence        := 220;
            l_price_list_line.created_by                := fnd_global.user_id;
            l_price_list_line.creation_date             := sysdate;
            l_price_list_line.last_updated_by           := fnd_global.user_id;
            l_price_list_line.last_update_date          := sysdate;
                                                
            l_pricing_att_rec.list_header_id            := l_price_list_line.list_header_id;
            l_pricing_att_rec.product_attribute_context := 'ITEM';
            l_pricing_att_rec.product_attribute         := 'PRICING_ATTRIBUTE1';
            l_pricing_att_rec.product_attribute_datatype:= 'C';
            l_pricing_att_rec.last_updated_by           := fnd_global.user_id;
            l_pricing_att_rec.last_update_date          := sysdate;
            l_pricing_att_rec.created_by                := fnd_global.user_id;
            l_pricing_att_rec.creation_date             := sysdate;                
            l_pricing_att_rec.pricing_phase_id          := 1;
            l_pricing_att_rec.price_list_line_index     := i;

            begin
                select  to_char(msib.inventory_item_id)
                        , msib.primary_uom_code
                into    l_pricing_att_rec.product_attr_value
                        , l_pricing_att_rec.product_uom_code 
                from    apps.mtl_system_items_b msib
                        , apps.mtl_parameters mp
                where   msib.inventory_item_id = p_inventory_item_id
                and     msib.organization_id = mp.organization_id
                and     mp.organization_code = 'MST';
            exception
            when others then
                null;
            end;
            
            l_pricing_att_rec.list_line_id              := FND_API.G_MISS_NUM;
            
            SELECT qp_pricing_attributes_s.nextval
            INTO   l_pricing_att_rec.pricing_attribute_id
            FROM   dual;
            
            l_price_list_line.operation                 := QP_GLOBALS.G_OPR_CREATE;
            l_pricing_att_rec.operation                 := QP_GLOBALS.G_OPR_CREATE;
                            
            l_pricing_attr_tbl(i)    := l_pricing_att_rec;
            l_price_list_line_tbl(i) := l_price_list_line;
            i := i + 1;

            qp_price_list_pub.process_price_list( p_api_version_number      => l_api_version_number,
                                                  p_init_msg_list           => l_init_msg_list,
                                                  p_return_values           => l_return_values,
                                                  p_commit                  => l_commit,
                                                  x_return_status           => l_x_return_status,
                                                  x_msg_count               => l_x_msg_count,
                                                  x_msg_data                => l_x_msg_data,
                                                  p_price_list_rec          => l_price_list_rec,
                                                  p_price_list_val_rec      => l_price_list_val_rec,
                                                  p_price_list_line_tbl     => l_price_list_line_tbl,
                                                  p_price_list_line_val_tbl => l_price_list_line_val_tbl,
                                                  p_qualifiers_tbl          => l_qualifiers_tbl,
                                                  p_qualifiers_val_tbl      => l_qualifiers_val_tbl,
                                                  p_pricing_attr_tbl        => l_pricing_attr_tbl,
                                                  p_pricing_attr_val_tbl    => l_pricing_attr_val_tbl_type,
                                                  x_price_list_rec          => l_x_price_list_rec,
                                                  x_price_list_val_rec      => l_x_price_list_val_rec,
                                                  x_price_list_line_tbl     => l_x_price_list_line_tbl,
                                                  x_price_list_line_val_tbl => l_x_price_list_line_val_tbl,
                                                  x_qualifiers_tbl          => l_x_qualifiers_tbl, 
                                                  x_qualifiers_val_tbl      => l_x_qualifiers_val_tbl,
                                                  x_pricing_attr_tbl        => l_x_pricing_attr_tbl,
                                                  x_pricing_attr_val_tbl    => l_x_pricing_attr_val_tbl);
                        
            write_output ('Return status = ' || l_x_return_status);
                      
            IF(l_x_return_status <> 'S') THEN
                --  Error Processing
                write_output ('  Message Count       = ' || l_x_msg_count);
                      
                FOR K IN 1 .. l_x_msg_count LOOP 
                  gpr_msg_data := oe_msg_pub.get(p_msg_index => k, 
                                                 p_encoded => 'F'); 
                  write_output (TO_CHAR(i)||' ERR TEXT  '|| gpr_msg_data);        
                END LOOP;
                RETURN;
                    
            ELSE
                null;   
            END IF;
            
        end if;
    else
        write_output ('Item did not pass validation, not importing to Price Lists');
    end if;
    
    commit;
    
EXCEPTION
WHEN OTHERS THEN
    l_error_message2 := 'xxwc_price_new_prod_rel '
                || 'Error_Stack...'
                || DBMS_UTILITY.format_error_stack ()
                || ' Error_Backtrace...'
                || DBMS_UTILITY.format_error_backtrace ();

    xxcus_error_pkg.xxcus_error_main_api (
        p_called_from         => 'xxwc_qp_market_price_util_pkg.xxwc_price_new_prod_rel'
       ,p_calling             => 'xxwc_qp_market_price_util_pkg.xxwc_price_new_prod_rel'
       ,p_request_id          => l_conc_req_id
       ,p_ora_error_msg       => l_error_message2
       ,p_error_desc          => 'Error running XXWC QP Pricing For New Product Release'
       ,p_distribution_list   => 'HDSOracleDevelopers@hdsupply.com'
       ,p_module              => 'QP');

    write_log('Error in main xxwc_qp_market_price_util_pkg.xxwc_price_new_prod_rel. Error: '||l_error_message2);
    
    errmsg := l_error_message2;
    retcode := '2';
    
END xxwc_price_new_prod_rel;

-- 08/02/2013 CG: TMS 20130729-00576: New market extract procedure
--12/2/2013 Ram Talluri TMS #20131202-00166
PROCEDURE xxwc_gen_market_extract (RETCODE     OUT VARCHAR2
                                   , ERRMSG    OUT VARCHAR2
                                   , P_INVENTORY_CATEGORY IN VARCHAR2
                                   , P_INCLUDE_SPECIALS IN VARCHAR2)
IS

    l_error_message2 clob;
    l_conc_req_id   number := fnd_global.conc_request_id;
    l_start_time    number;  
    l_end_time      number;
    l_login_id      number := fnd_global.login_id;

    TYPE MarketPrlTyp  IS REF CURSOR;
    v_market_prl    MarketPrlTyp;
    prl_headers     varchar2(10000);
    prl_lines       varchar2(10000);
    v_stmt_str      VARCHAR2(30000);
    v_category_stmt VARCHAR2(1000);
    v_include_spec  VARCHAR2(1000);
    l_count         NUMBER;
      
    cursor cur_prl_sequence is
        select  lookup_code, meaning
        from    fnd_lookup_values
        where   lookup_type = 'XXWC_MARKET_EXTRACT_ORDER'
        and     enabled_flag = 'Y'
        order by to_number (lookup_code);

BEGIN
    write_log('Starting XXWC QP Generate Market Price Lists Extract');
    write_log('=====================================================');
    write_log(' ');

    -- 08/12/2013 CG: TMS 20130729-00576: Updated so that the CFP Flag would not cause duplicate records
    --v_stmt_str := 'select chr(39)||x1.item_number||'||''''||'|'||''''||'||x1.item_description||'||''''||'|'||''''||'||x1.item_category||'||''''||'|'||''''||'||x1.cfp_flag';
    v_stmt_str := 'select chr(39)||x1.item_number||'||''''||'|'||''''||'||x1.item_description||'||''''||'|'||''''||'||x1.item_category||'||''''||'|'||'''';
    v_stmt_str := v_stmt_str||'||MAX (DECODE (name, '||''''||'MARKET NATIONAL'||''''||', x1.cfp_flag, NULL))';
    -- 08/12/2013 CG: TMS 20130729-00576
    
    for c1 in cur_prl_sequence
    loop
        exit when cur_prl_sequence%notfound;
        v_stmt_str := v_stmt_str||'||'||''''||'|'||''''||'||MAX (DECODE (name, '||''''||c1.meaning||''''||', operand, NULL))';
    end loop;
    
    v_category_stmt := null;
    IF P_INVENTORY_CATEGORY IS NOT NULL THEN                
        v_category_stmt := ' and nvl(apps.xxwc_mv_routines_pkg.get_item_category (msib.inventory_item_id, msib.organization_id, '||''''||'Inventory Category'||''''||', 0), '||''''||'X'||''''||') = 
                                nvl(nvl(:p_inventory_category, apps.xxwc_mv_routines_pkg.get_item_category (msib.inventory_item_id, msib.organization_id, '||''''||'Inventory Category'||''''||', 0), '||''''||'X'||''''||')';
    END IF;    
    
    v_include_spec := null;
    IF P_INCLUDE_SPECIALS IS NOT NULL THEN
        IF P_INCLUDE_SPECIALS = 'N' THEN
            v_include_spec := ' and msib.item_type != '||''''||'SPECIAL'||''''; 
        END IF;
    END IF;
    
    v_stmt_str := v_stmt_str||chr(10)||'from (  select  qlh.name
                                                        , qlh.description
                                                        , msib.segment1 item_number
                                                        , qll.operand
                                                        , REGEXP_REPLACE (msib.description, '||''''||'[^[:alnum:]|[:blank:]|[:punct:]]+'||''''||', null) item_description
                                                        , xxwc_mv_routines_pkg.get_item_category (msib.inventory_item_id,msib.organization_id,'||''''||'Inventory Category'||''''||',null)  item_category
                                                        , (case when qlh.list_header_id = apps.fnd_profile.value ('||''''||'XXWC_QP_MARKET_NATIONAL_PRL'||''''||') then qll.attribute1 else null end) cfp_flag
                                                from    apps.qp_list_headers qlh
                                                    , apps.qp_list_lines qll
                                                    , apps.qp_pricing_attributes qpa
                                                    , apps.mtl_system_items_b msib
                                                where   qlh.list_header_id != apps.fnd_profile.value ('||''''||'XXWC_QP_MARKET_NTL_SETUP_PRL'||''''||')
                                                and     qlh.list_type_code = '||''''||'PRL'||''''||'
                                                --and     qlh.start_date_active <= trunc(sysdate)--commented on 12/2/2013 Ram Talluri TMS #20131202-00166
                                                and     NVL(qlh.start_date_active,TRUNC(SYSDATE-1)) <= trunc(sysdate)--ADDED on 12/2/2013 Ram Talluri TMS #20131202-00166
                                                and     nvl(qlh.end_date_active, trunc(sysdate+1)) > trunc(sysdate)
                                                and     (qlh.attribute10 = '||''''||'Market Price List'||''''||' or qlh.list_header_id = apps.fnd_profile.value ('||''''||'XXWC_QP_MARKET_NATIONAL_PRL'||''''||'))
                                                and     qlh.list_header_id = qll.list_header_id
                                                and     qll.list_line_type_code = '||''''||'PLL'||''''||'
                                                --and     qll.start_date_active <= trunc(sysdate)--commented on 12/2/2013 Ram Talluri TMS #20131202-00166
                                                AND     nvl(qll.start_date_active,TRUNC(SYSDATE-1)) <= trunc(sysdate)--ADDED on 12/2/2013 Ram Talluri TMS #20131202-00166
                                                and     nvl(qll.end_date_active, trunc(sysdate+1)) > trunc(sysdate)
                                                and     qll.list_header_id = qpa.list_header_id
                                                and     qll.qualification_ind = qpa.qualification_ind
                                                and     qll.pricing_phase_id = qpa.pricing_phase_id
                                                and     qll.list_line_id = qpa.list_line_id
                                                and     qpa.product_attribute_context = '||''''||'ITEM'||''''||'
                                                and     qpa.product_attribute = '||''''||'PRICING_ATTRIBUTE1'||''''||'
                                                and     to_number(qpa.product_attr_value) = msib.inventory_item_id
                                                and     msib.organization_id = 222'||v_category_stmt||v_include_spec||'
                                             )x1
                                        group by x1.item_number, x1.item_description, x1.item_category
                                        order by x1.item_number';
                                        
    write_log('Select statement: ');
    write_log(v_stmt_str); 
    
    prl_headers:= null;
    prl_headers := 'Item Number| Item Description| Item Category| CFP Flag';
    for c1 in cur_prl_sequence
    loop
        exit when cur_prl_sequence%notfound;
        prl_headers := prl_headers||'|'||c1.meaning;
    end loop;
    
    prl_headers := prl_headers||'|X';
    
    write_output(prl_headers);
    
    OPEN v_market_prl FOR v_stmt_str;

    -- Fetch rows from result set one at a time:
    l_count := 0;
    LOOP
      FETCH v_market_prl INTO prl_lines;
      EXIT WHEN v_market_prl%NOTFOUND;
      write_output(prl_lines||'|X');
      l_count := l_count + 1;
    END LOOP;

    -- Close cursor:
    CLOSE v_market_prl;
    
    write_log(' ');
    write_log('=====================================================');
    write_log(' ');
    write_log('Extracted '||l_count||' lines');
    
    commit;

EXCEPTION
WHEN OTHERS THEN
    l_error_message2 := 'xxwc_qp_market_price_util_pkg.xxwc_gen_market_extract '
                || 'Error_Stack...'
                || DBMS_UTILITY.format_error_stack ()
                || ' Error_Backtrace...'
                || DBMS_UTILITY.format_error_backtrace ();

    xxcus_error_pkg.xxcus_error_main_api (
        p_called_from         => 'xxwc_qp_market_price_util_pkg.xxwc_price_new_prod_rel'
       ,p_calling             => 'xxwc_qp_market_price_util_pkg.xxwc_price_new_prod_rel'
       ,p_request_id          => l_conc_req_id
       ,p_ora_error_msg       => l_error_message2
       ,p_error_desc          => 'Error running XXWC QP Generate Market Price Lists Extract'
       ,p_distribution_list   => 'cgonzalez@luciditycg.com'
       ,p_module              => 'QP');

    write_log('Error in main xxwc_qp_market_price_util_pkg.xxwc_gen_market_extract. Error: '||l_error_message2);
    
    errmsg := l_error_message2;
    retcode := '2';
END xxwc_gen_market_extract;


/*************************************************************************
  Function Name: get_call_for_price_flag

  PURPOSE:   This function is used in XXWC_QP_ECOMMERCE_ITEMS_MV materialized view to derive call for price flag

  REVISIONS:
  Ver        Date        Author             Description
  ---------  ----------  ---------------    -------------------------
  7.2        02/24/2015  Shankar Hariharan  TMS# 20150219-00234 Added new function to get Call for Price Flag
**************************************************************************/
FUNCTION get_call_for_price_flag (p_inventory_item_id IN NUMBER
                                , p_organization_id   IN NUMBER) RETURN VARCHAR2
IS
    l_pc_flag    VARCHAR2(1);
BEGIN
   l_pc_flag := NULL;

   SELECT ll.attribute1
     INTO l_pc_flag
     FROM apps.qp_list_headers_all         lh,
          apps.qp_list_lines               ll,
          apps.qp_pricing_attributes       qpa
    WHERE lh.list_header_id              = G_MARKET_CAT_PRL_ID
      AND lh.list_header_id              = ll.list_header_id
      AND SYSDATE BETWEEN NVL (ll.start_date_active, SYSDATE - 1) AND NVL (ll.end_date_active, SYSDATE + 1)
      AND ll.list_header_id              = qpa.list_header_id 
      AND ll.qualification_ind           = qpa.qualification_ind 
      AND ll.pricing_phase_id            = qpa.pricing_phase_id 
      AND ll.list_line_id                = qpa.list_line_id 
      AND qpa.product_attribute_context  = 'ITEM'
      AND qpa.product_attribute          = 'PRICING_ATTRIBUTE1'
      AND qpa.product_attribute_datatype = 'C'
      AND qpa.product_attr_value         = TO_CHAR(p_inventory_item_id)
      AND ll.context                     = '162'
      AND ROWNUM = 1;

    RETURN NVL(l_pc_flag, 'N');
EXCEPTION
WHEN OTHERS THEN    
    RETURN 'N';
END get_call_for_price_flag;

-- Version# 8.0 > Start
/*************************************************************************
  Function Name: xxwc_pri_segment_maint_prc

  PURPOSE:   This function is used by Pricing Segmentation Process

  REVISIONS:
  Ver        Date        Author             Description
  ---------  ----------  ---------------    -------------------------
  8.0        08/03/2015  Gopi Damuluri      TMS# 20150622-00085 Pricing Segmentation
**************************************************************************/
PROCEDURE xxwc_pri_segment_maint_prc (RETCODE      OUT NUMBER
                                     , ERRMSG       OUT VARCHAR2
                                     , P_FILE_ID     IN NUMBER)
IS
    l_msg_data      VARCHAR2 (4000);
    l_error_message2    clob;
    
    -- File Processing Variables
    l_file_id       number :=  P_FILE_ID;    
    l_file          BLOB;
    l_file_name     VARCHAR2 (256);
    i               INTEGER := 1; -- Row counter
    j               INTEGER := 1; -- Column Counter
    is_file_open    INTEGER;
    l_hdr           INTEGER;
    l_line          INTEGER;

    l_comma_count   NUMBER;
    l_offset        NUMBER := 1;
    l_eol_pos       NUMBER := 32767;
    l_amount        NUMBER;
    l_file_len      NUMBER := DBMS_LOB.getlength (l_file);
    l_buffer        RAW (30000);
    
    l_price_segment_tbl_type         xxwc_qp_market_price_util_pkg.price_segment_tbl_type := xxwc_qp_market_price_util_pkg.G_PRICE_SEGMENT_TBL;

    l_data          VARCHAR2(240);
    l_item_number   VARCHAR2(40);
    l_is_number     VARCHAR2(1);
   
    l_start_time    number;  
    l_end_time      number;
    l_conc_req_id   number := fnd_global.conc_request_id;
    l_login_id      number := fnd_global.login_id;
    -- File Processing Variables
    
    l_cnt                           NUMBER;
    l                               NUMBER; -- i
    m                               NUMBER; -- j
    n                               NUMBER; -- k       
    l_api_version_number            NUMBER         := 1.0;
    l_init_msg_list                 VARCHAR2(10)   := FND_API.G_FALSE;
    l_return_values                 VARCHAR2(10)   := FND_API.G_FALSE;
    l_commit                        VARCHAR2(10)   := FND_API.G_TRUE;
    l_x_return_status               VARCHAR2(255)  := 'S';
    l_x_msg_count                   NUMBER;
    l_x_msg_data                    VARCHAR2(255);
    g_user_id                       NUMBER;
    l_price_list_rec                QP_PRICE_LIST_PUB.PRICE_LIST_REC_TYPE          := QP_PRICE_LIST_PUB.G_MISS_PRICE_LIST_REC;
    l_price_list_val_rec            QP_PRICE_LIST_PUB.PRICE_LIST_VAL_REC_TYPE      := QP_PRICE_LIST_PUB.G_MISS_PRICE_LIST_VAL_REC;
    l_price_list_line               QP_PRICE_LIST_PUB.PRICE_LIST_LINE_REC_TYPE    := QP_PRICE_LIST_PUB.G_MISS_PRICE_LIST_LINE_REC;
    l_price_list_line_tbl           QP_PRICE_LIST_PUB.PRICE_LIST_LINE_TBL_TYPE     := QP_PRICE_LIST_PUB.G_MISS_PRICE_LIST_LINE_TBL; 
    l_price_list_line_val_tbl       QP_PRICE_LIST_PUB.PRICE_LIST_LINE_VAL_TBL_TYPE := QP_PRICE_LIST_PUB.G_MISS_PRICE_LIST_LINE_VAL_TBL;
    l_qualifiers_tbl                QP_QUALIFIER_RULES_PUB.QUALIFIERS_TBL_TYPE     := QP_PRICE_LIST_PUB.G_MISS_QUALIFIERS_TBL;
    l_qualifiers_val_tbl            QP_QUALIFIER_RULES_PUB.QUALIFIERS_VAL_TBL_TYPE := QP_PRICE_LIST_PUB.G_MISS_QUALIFIERS_VAL_TBL;
    l_pricing_att_rec               QP_PRICE_LIST_PUB.PRICING_ATTR_REC_TYPE := QP_PRICE_LIST_PUB.G_MISS_PRICING_ATTR_REC;
    l_pricing_attr_tbl              QP_PRICE_LIST_PUB.PRICING_ATTR_TBL_TYPE        := QP_PRICE_LIST_PUB.G_MISS_PRICING_ATTR_TBL;
    l_pricing_attr_val_tbl_type     QP_PRICE_LIST_PUB.PRICING_ATTR_VAL_TBL_TYPE    := QP_PRICE_LIST_PUB.G_MISS_PRICING_ATTR_VAL_TBL;
    l_x_price_list_rec              QP_PRICE_LIST_PUB.PRICE_LIST_REC_TYPE;
    l_x_price_list_val_rec          QP_PRICE_LIST_PUB.PRICE_LIST_VAL_REC_TYPE;
    l_x_price_list_line_tbl         QP_PRICE_LIST_PUB.PRICE_LIST_LINE_TBL_TYPE;
    l_x_price_list_line_val_tbl     QP_PRICE_LIST_PUB.PRICE_LIST_LINE_VAL_TBL_TYPE;
    l_x_qualifiers_tbl              QP_QUALIFIER_RULES_PUB.QUALIFIERS_TBL_TYPE;
    l_x_qualifiers_val_tbl          QP_QUALIFIER_RULES_PUB.QUALIFIERS_VAL_TBL_TYPE;
    l_x_pricing_attr_tbl            QP_PRICE_LIST_PUB.PRICING_ATTR_TBL_TYPE;
    l_x_pricing_attr_val_tbl        QP_PRICE_LIST_PUB.PRICING_ATTR_VAL_TBL_TYPE;
    
    l_error_message_list            ERROR_HANDLER.ERROR_TBL_TYPE;
    l_price_list                    VARCHAR2(3);
    l_inventory_item_id             NUMBER;
    l_primary_uom_code              VARCHAR2(3);
    l_process                       VARCHAR2(1);
    gpr_msg_data                    VARCHAR2(2000);
    l_valid                         VARCHAR2(1);
    l_warehouse_id                  VARCHAR2(30);
    l_structure_name                VARCHAR2(240);
    l_effective_start_date          DATE;
    
    x_request_id                    NUMBER;
    l_ntl_setup_prl_processed       NUMBER;
    l_commit_size                   NUMBER;
    l_submit_time                   VARCHAR2(240);
    
    -- QP Pricing API Variables/Cursors

    -- 08/02/2013 CG: TMS 20130729-00576: Added to handle upload of CFP flag for Market National PRL
    l_cfp_flag                      VARCHAR2(1);
    l_list_header_id                NUMBER;    
    l_prog_error                    EXCEPTION;
    l_sec                           VARCHAR2(200);
    
BEGIN

    write_output('Starting Maintenance Tool Processing for File ID: '||l_file_id);
    write_output('---------------------------------------------------------------');

    l_Start_time := DBMS_UTILITY.get_time;
    write_log ('l_Start_time: '||l_Start_time);

    BEGIN
       SELECT file_data
            , file_name
         INTO l_file
            , l_file_name
         FROM fnd_lobs
        WHERE file_id = l_file_id;
    EXCEPTION
    WHEN OTHERS THEN
        l_file := null;
        l_file_name := null;
        write_log ('File Not Found. Error: '||SQLERRM);
        l_msg_data := SUBSTR(SQLERRM, 1, 4000);
        errmsg := l_msg_data;
        retcode := '2';  
        RAISE;
    END;

    is_file_open := DBMS_LOB.ISOPEN (l_file);
    IF is_file_open = 1
    THEN
       DBMS_LOB.CLOSE (l_file);
    END IF;

    BEGIN
       DBMS_LOB.open (l_file, DBMS_LOB.lob_readonly);
       is_file_open        := DBMS_LOB.ISOPEN (l_file);
    EXCEPTION
    WHEN OTHERS  THEN
        write_log ('Could not open file. Error: '||SQLERRM);
        l_msg_data := substr(SQLERRM, 1, 4000);
        errmsg := l_msg_data;
        retcode := '2';  
        raise;
    END;

    l_file_len  := DBMS_LOB.getlength (l_file);
    write_output ('File Name '||l_file_name||' and length ' ||l_file_len);

    write_output ('Starting File read and parsing from LOB...');  
    l_price_segment_tbl_type     := xxwc_qp_market_price_util_pkg.G_PRICE_SEGMENT_TBL;
    
    l_hdr           := 1;
    l_line          := 1;
    l_data          := null;
    WHILE (l_offset < l_file_len)
    LOOP
        BEGIN
        -- i: Row Counter. Reset Per File
        -- j: Column Counter. Reset Per row

        -- Finding EOL position  for new line/line feed ascii 0A
        l_eol_pos      := DBMS_LOB.INSTR (l_file, '0A', 1, i);

        -- Calculating how many chars to pull in buffer based on EOL position and offset
        IF i = 1 THEN
            l_amount     := l_eol_pos - 1;
        ELSE
            l_amount     := l_eol_pos - l_offset;
        END IF;

        -- Reading first record from LOB file
        DBMS_LOB.read (l_file, l_amount, l_offset, l_buffer);

        -- From the first record pulled retrieve the number of columns based on comma count
        IF i = 1 THEN
            SELECT   regexp_count (REPLACE (UTL_RAW.cast_to_varchar2 (l_buffer), CHR (13), NULL), ',')
            INTO   l_comma_count
            FROM   DUAL;

            write_log ('l_comma_count: ' || l_comma_count);
            l_comma_count := l_comma_count + 1;
            write_log (' ');            
        END IF;

        j := 1;
        FOR j IN 1..l_comma_count
        LOOP
            -- Parsing Column Data is slightly different for the first column and the remaining cols
            IF j = 1 THEN
                write_log ('Line Split#' || j || ': '|| SUBSTR ( REPLACE ( UTL_RAW.cast_to_varchar2 (l_buffer), CHR (13), NULL )
                                                                 , 1
                                                                 , INSTR ( REPLACE ( UTL_RAW.cast_to_varchar2 (l_buffer), CHR (13), NULL), ',', 1, 1) - 1)
                           );
                                       
                l_data := SUBSTR ( REPLACE ( UTL_RAW.cast_to_varchar2 (l_buffer), CHR (13), NULL )
                                 , 1
                                 , INSTR ( REPLACE ( UTL_RAW.cast_to_varchar2 (l_buffer), CHR (13), NULL), ',', 1, 1) - 1);
            ELSIF j = l_comma_count THEN
                write_log ('Line Split#' || j || ': '|| SUBSTR (REPLACE (UTL_RAW.cast_to_varchar2 (l_buffer), CHR (13), NULL)
                                                                 , INSTR (REPLACE (UTL_RAW.cast_to_varchar2 (l_buffer), CHR (13), NULL), ',', 1, j - 1)+ 1
                                                                )
                           );
                                       
                l_data := SUBSTR (REPLACE (UTL_RAW.cast_to_varchar2 (l_buffer), CHR (13), NULL)
                                 , INSTR (REPLACE (UTL_RAW.cast_to_varchar2 (l_buffer), CHR (13), NULL), ',', 1, j - 1)+ 1
                                );            
            ELSE
                write_log ('Line Split#' || j || ': '|| SUBSTR (REPLACE (UTL_RAW.cast_to_varchar2 (l_buffer), CHR (13), NULL)
                                                                 , INSTR (REPLACE (UTL_RAW.cast_to_varchar2 (l_buffer), CHR (13), NULL), ',', 1, j - 1)+ 1
                                                                 , (INSTR (REPLACE (UTL_RAW.cast_to_varchar2 (l_buffer), CHR (13), NULL), ',', 1, j))
                                                                   - (INSTR (REPLACE (UTL_RAW.cast_to_varchar2 (l_buffer), CHR (13), NULL), ',', 1, j - 1)+ 1)
                                                                )
                           );
                                       
                l_data := SUBSTR (REPLACE (UTL_RAW.cast_to_varchar2 (l_buffer), CHR (13), NULL)
                                 , INSTR (REPLACE (UTL_RAW.cast_to_varchar2 (l_buffer), CHR (13), NULL), ',', 1, j - 1)+ 1
                                 , (INSTR (REPLACE (UTL_RAW.cast_to_varchar2 (l_buffer), CHR (13), NULL), ',', 1, j))
                                   - (INSTR (REPLACE (UTL_RAW.cast_to_varchar2 (l_buffer), CHR (13), NULL), ',', 1, j - 1)+ 1)
                                );            
            END IF;

            if i > 1 then
                IF j = 1 THEN
                   l_price_segment_tbl_type(l_line).SEGMENT            := TRIM(l_data);
                ELSIF j = 2 THEN
                   l_price_segment_tbl_type(l_line).PRODUCT_TYPE       := TRIM(l_data);
                ELSIF j = 3 THEN
                   l_price_segment_tbl_type(l_line).PRODUCT            := TRIM(l_data);

                   IF l_price_segment_tbl_type(l_line).PRODUCT_TYPE = 'Cat Class' THEN
                      BEGIN
                         SELECT category_id
                           INTO l_price_segment_tbl_type(l_line).PRODUCT_ID
                           FROM mtl_categories 
                          WHERE segment1||'.'|| segment2 = l_price_segment_tbl_type(l_line).PRODUCT;
                      EXCEPTION
                      WHEN OTHERS THEN
                         write_output ('Invalid Product: Product_Type -'||l_price_segment_tbl_type(l_line).PRODUCT_TYPE||
                                       'Product -'||l_price_segment_tbl_type(l_line).PRODUCT);
                         l_price_segment_tbl_type(l_line).PRODUCT_ID         := NULL;
                         RAISE l_prog_error;
                      END;
                   ELSIF l_price_segment_tbl_type(l_line).PRODUCT_TYPE = 'SKU' THEN
                      BEGIN
                         SELECT inventory_item_id
                           INTO l_price_segment_tbl_type(l_line).PRODUCT_ID
                           FROM mtl_system_items_b
                          WHERE segment1 = l_price_segment_tbl_type(l_line).PRODUCT
                            AND ROWNUM = 1;
                      EXCEPTION
                      WHEN OTHERS THEN
                         write_output ('Invalid Product: Product_Type -'||l_price_segment_tbl_type(l_line).PRODUCT_TYPE||
                                       'Product -'||l_price_segment_tbl_type(l_line).PRODUCT);
                         l_price_segment_tbl_type(l_line).PRODUCT_ID         := NULL;
                         RAISE l_prog_error;
                      END;
                   ELSE
                      l_price_segment_tbl_type(l_line).PRODUCT_ID         := NULL;
                      write_output ('Invalid Product Type for Record# - '||i);
                      RAISE l_prog_error;
                   END IF;
                ELSIF j = 4 THEN
                   l_price_segment_tbl_type(l_line).DESCRIPTION        := TRIM(l_data);
                ELSIF j = 5 THEN
                   l_price_segment_tbl_type(l_line).DISCOUNT_TYPE      := TRIM(l_data);
                ELSIF j = 6 THEN
                   l_price_segment_tbl_type(l_line).TIER1              := TRIM(l_data);
                ELSIF j = 7 THEN
                   l_price_segment_tbl_type(l_line).TIER2              := TRIM(l_data);
                ELSIF j = 8 THEN
                   l_price_segment_tbl_type(l_line).TIER3              := TRIM(l_data);
                ELSIF j = 9 THEN
                   l_price_segment_tbl_type(l_line).TIER4              := TRIM(l_data);
                ELSIF j = 10 THEN
                   l_price_segment_tbl_type(l_line).TIER5              := TRIM(l_data);
                ELSIF j = 11 THEN
                   l_price_segment_tbl_type(l_line).TIER6              := TRIM(l_data);
                ELSIF j = 12 THEN
                   l_price_segment_tbl_type(l_line).TIER7              := TRIM(l_data);
                ELSIF j = 13 THEN
                   l_price_segment_tbl_type(l_line).DELETE_FLAG        := TRIM(l_data);
                   
                   IF l_price_segment_tbl_type(l_line).DELETE_FLAG = 'D' THEN
                      write_log ('Delete Product: Product_Type -'||l_price_segment_tbl_type(l_line).PRODUCT_TYPE||
                                       'Product -'||l_price_segment_tbl_type(l_line).PRODUCT);
                   END IF;
                   
                ELSE
                   NULL;
                END IF;
            end if;
        END LOOP;

        l_price_segment_tbl_type(l_line).FILE_ID            := l_file_id;
        l_price_segment_tbl_type(l_line).LAST_UPDATE_DATE   := SYSDATE;
        l_price_segment_tbl_type(l_line).LAST_UPDATED_BY    := fnd_global.user_id;
        l_price_segment_tbl_type(l_line).CREATION_DATE      := SYSDATE;
        l_price_segment_tbl_type(l_line).CREATED_BY         := fnd_global.user_id;
        l_price_segment_tbl_type(l_line).REQUEST_ID         := l_conc_req_id;

        -----------------------------------------------------------------------------------------------------
        -- Validations
        -----------------------------------------------------------------------------------------------------
        IF l_price_segment_tbl_type(l_line).SEGMENT IS NULL THEN
           write_output ('SEGMENT is empty for Record# - '||i);
           RAISE l_prog_error;
        END IF;

        IF l_price_segment_tbl_type(l_line).PRODUCT IS NULL THEN
           write_output ('PRODUCT is empty for Record# - '||i);
           RAISE l_prog_error;
        END IF;
        
        IF l_price_segment_tbl_type(l_line).PRODUCT_TYPE IS NULL THEN
           write_output ('PRODUCT_TYPE is empty for Record# - '||i);
           RAISE l_prog_error;
        END IF;

        IF l_price_segment_tbl_type(l_line).DISCOUNT_TYPE IS NULL THEN
           write_output ('DISCOUNT_TYPE is empty for Record# - '||i);
           RAISE l_prog_error;
        END IF;
        
        IF l_price_segment_tbl_type(l_line).PRODUCT_TYPE = 'Cat Class' AND l_price_segment_tbl_type(l_line).DISCOUNT_TYPE != 'Percent' THEN
           write_output ('Invalid Discount Type for Record# - '||i);
           RAISE l_prog_error;
        END IF;

        IF l_price_segment_tbl_type(l_line).PRODUCT_TYPE = 'SKU' AND l_price_segment_tbl_type(l_line).DISCOUNT_TYPE NOT IN ('Percent', 'New Price') THEN
           write_output ('Invalid Discount Type for Record# - '||i);
           RAISE l_prog_error;
        END IF;

    EXCEPTION
    WHEN l_prog_error THEN
        
        l_price_segment_tbl_type(l_line).DELETE_FLAG           := 'E';
        l_price_segment_tbl_type(l_line).PRODUCT_ID            := NULL;
    END;

        l_line := l_line + 1;                   -- This will be initialized per file and increase as it reads throught the LOB
        l_offset := l_offset + l_amount + 1;    -- Increasing offset for read. Starting position of next read
        i        := i + 1;                      -- Rowcounter increase
        write_log (' ');

    END LOOP;
   
    DBMS_LOB.close (l_file);
   
    write_output ('LOB file read and parsing complete. Starting bulk insert into staging...');
   
    BEGIN
    FORALL x IN l_price_segment_tbl_type.FIRST..l_price_segment_tbl_type.LAST
    INSERT INTO xxwc.xxwc_qp_pricing_segment_tbl
    VALUES l_price_segment_tbl_type(x);
    
    EXCEPTION
    WHEN OTHERS THEN
    write_log ('Error writing into staging table...'||SQLERRM);
    write_output ('Error writing into staging table...'||SQLERRM);
    END;

    COMMIT;
    
    UPDATE xxwc.xxwc_qp_pricing_segment_tbl stg
       SET (stg.DESCRIPTION 
          , stg.DISCOUNT_TYPE 
          , stg.TIER1 
          , stg.TIER2 
          , stg.TIER3 
          , stg.TIER4 
          , stg.TIER5 
          , stg.TIER6 
          , stg.TIER7
          , stg.DELETE_FLAG
          , stg.REQUEST_ID
          , stg.LAST_UPDATE_DATE
          , stg.LAST_UPDATED_BY) = (SELECT stg2.DESCRIPTION 
                                , stg2.DISCOUNT_TYPE 
                                , NVL(stg2.TIER1 , stg.TIER1)
                                , NVL(stg2.TIER2 , stg.TIER2)
                                , NVL(stg2.TIER3 , stg.TIER3)
                                , NVL(stg2.TIER4 , stg.TIER4)
                                , NVL(stg2.TIER5 , stg.TIER5)
                                , NVL(stg2.TIER6 , stg.TIER6)
                                , NVL(stg2.TIER7 , stg.TIER7)
                                , stg2.DELETE_FLAG
                                , stg2.REQUEST_ID
                                , stg2.LAST_UPDATE_DATE
                                , stg2.LAST_UPDATED_BY
                            FROM xxwc.xxwc_qp_pricing_segment_tbl stg2
                           WHERE 1 = 1
                             AND stg2.product_id = stg.product_id
                             AND NVL(stg2.DELETE_FLAG,'N') != 'E'
                             AND stg2.segment    = stg.segment
                             AND stg2.file_id    = l_file_id)
     WHERE 1 = 1
       AND stg.product_id IS NOT NULL
       AND stg.file_id != l_file_id
       AND EXISTS (SELECT '1' 
                     FROM xxwc.xxwc_qp_pricing_segment_tbl stg3
                    WHERE 1 = 1
                      AND NVL(stg3.DELETE_FLAG,'N') != 'E'
                      AND stg3.product_id = stg.product_id
                      AND stg3.segment    = stg.segment
                      AND stg3.file_id    = l_file_id);
    
    COMMIT;
    
    DELETE FROM xxwc.xxwc_qp_pricing_segment_tbl stg 
    WHERE (stg.product_id IS NULL OR stg.product_id = 9.99E125)
      AND stg.file_id = l_file_id;

    DELETE FROM xxwc.xxwc_qp_pricing_segment_tbl stg 
    WHERE NVL(stg.DELETE_FLAG,'N') = 'E';

    COMMIT;
    
    DELETE FROM xxwc.xxwc_qp_pricing_segment_tbl stg 
          WHERE 1 = 1
            AND stg.file_id = l_file_id
            AND EXISTS (SELECT '1'
                            FROM xxwc.xxwc_qp_pricing_segment_tbl stg2
                           WHERE 1 = 1
                             AND stg2.product_id = stg.product_id
                             AND stg2.segment    = stg.segment
                             AND stg2.file_id   != l_file_id);
    
    COMMIT;
 
    write_output ('Bulk insert to staging complete.');          
    
    l_end_time := DBMS_UTILITY.get_time;
    write_log ('l_end_time : '||l_end_time );
    write_log ('File read/parsing processing time: '||to_char(l_end_time-l_start_time));
    write_output ('File read/parsing processing time: '||to_char(l_end_time-l_start_time));

    commit;

    write_output('Completed data processing and load to EBS');
    l_end_time := DBMS_UTILITY.get_time;
    write_log ('l_end_time : '||l_end_time );
    write_log ('QP Processing Time: '||to_char(l_end_time-l_start_time));
    write_output ('QP Processing Time: '||to_char(l_end_time-l_start_time));

EXCEPTION
WHEN NO_DATA_FOUND THEN
    BEGIN
        is_file_open             := NULL;
        is_file_open             := DBMS_LOB.ISOPEN (l_file);

        IF is_file_open = 1 THEN
            DBMS_LOB.close (l_file);
            COMMIT;
        END IF;
    END;
      
    l_msg_data := substr(SQLERRM, 1, 4000);
    Write_log ('xxwc_pri_segment_maint_prc. No Data Found Excep: '||l_msg_data);
    Write_log ('Line#: '||DBMS_UTILITY.format_error_backtrace ());
    errmsg := l_msg_data;
    retcode := '2';  
      
WHEN OTHERS THEN
    BEGIN
        is_file_open             := NULL;
        is_file_open             := DBMS_LOB.ISOPEN (l_file);

        IF is_file_open = 1 THEN
            DBMS_LOB.close (l_file);
            COMMIT;
        END IF;
    END;
    
    l_error_message2 := 'xxwc_pri_segment_maint_prc '
                || 'Error_Stack...'
                || DBMS_UTILITY.format_error_stack ()
                || ' Error_Backtrace...'
                || DBMS_UTILITY.format_error_backtrace ();

    xxcus_error_pkg.xxcus_error_main_api (
        p_called_from         => 'xxwc_qp_market_price_util_pkg.xxwc_pri_segment_maint_prc'
       ,p_calling             => 'xxwc_qp_market_price_util_pkg.xxwc_pri_segment_maint_prc'
       ,p_request_id          => l_conc_req_id
       ,p_ora_error_msg       => l_error_message2
       ,p_error_desc          => 'Error running XXWC QP Maintenance Tool Processor'
       ,p_distribution_list   => 'HDSOracleDevelopers@hdsupply.com'
       ,p_module              => 'QP');

    write_log('Error in main xxwc_pri_segment_maint_prc: '||l_error_message2);
    
    errmsg := l_error_message2;
    retcode := '2';   
    
END xxwc_pri_segment_maint_prc;

-- Version# 8.0 < End

end xxwc_qp_market_price_util_pkg;
/
