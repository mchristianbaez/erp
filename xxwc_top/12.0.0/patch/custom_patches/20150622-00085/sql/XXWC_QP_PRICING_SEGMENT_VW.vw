DROP VIEW APPS.XXWC_QP_PRICING_SEGMENT_VW;

/*************************************************************************
  $Header XXWC_QP_PRICING_SEGMENT_VW $
  Module Name: XXWC_QP_PRICING_SEGMENT_VW.vw

  PURPOSE:   This view is used for Pricing Segmentation form

  REVISIONS:
  Ver        Date        Author                     Description
  ---------  ----------  ---------------         -------------------------
  1.0        08/03/2015  Gopi Damuluri           TMS# 20150622-00085 Pricing Segmentation
**************************************************************************/
CREATE OR REPLACE FORCE VIEW APPS.XXWC_QP_PRICING_SEGMENT_VW
(
   SEGMENT,
   PRODUCT_TYPE,
   PRODUCT,
   PRODUCT_ID,
   DESCRIPTION,
   DISCOUNT_TYPE,
   TIER1,
   TIER2,
   TIER3,
   TIER4,
   TIER5,
   TIER6,
   TIER7,
   DELETE_FLAG
)
AS
   SELECT SEGMENT,
          PRODUCT_TYPE,
          PRODUCT,
          PRODUCT_ID,
          DESCRIPTION,
          DISCOUNT_TYPE,
          TIER1,
          TIER2,
          TIER3,
          TIER4,
          TIER5,
          TIER6,
          TIER7,
          DELETE_FLAG
     FROM XXWC.XXWC_QP_PRICING_SEGMENT_TBL STG
    WHERE 1 = 1;
/