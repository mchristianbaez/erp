DROP TABLE XXWC.XXWC_QP_PRICING_SEGMENT_TBL;
/
/*************************************************************************
  $Header XXWC_QP_PRICING_SEGMENT_TBL $
  Module Name: XXWC_QP_PRICING_SEGMENT_TBL.sql

  PURPOSE:   This table is used for Pricing Segmentation

  REVISIONS:
  Ver        Date        Author                     Description
  ---------  ----------  ---------------         -------------------------
  1.0        08/03/2015  Gopi Damuluri           TMS# 20150622-00085 Pricing Segmentation
**************************************************************************/

CREATE TABLE XXWC.XXWC_QP_PRICING_SEGMENT_TBL
(
  SEGMENT           VARCHAR2(50 BYTE),
  PRODUCT_TYPE      VARCHAR2(50 BYTE),
  PRODUCT           VARCHAR2(50 BYTE),
  DESCRIPTION       VARCHAR2(240 BYTE),
  DISCOUNT_TYPE     VARCHAR2(50 BYTE),
  TIER1             NUMBER,
  TIER2             NUMBER,
  TIER3             NUMBER,
  TIER4             NUMBER,
  TIER5             NUMBER,
  TIER6             NUMBER,
  TIER7             NUMBER,
  DELETE_FLAG       VARCHAR2(1 BYTE),
  LAST_UPDATE_DATE  DATE,
  LAST_UPDATED_BY   NUMBER,
  CREATION_DATE     DATE,
  CREATED_BY        NUMBER,
  PRODUCT_ID        NUMBER,
  FILE_ID           NUMBER,
  REQUEST_ID        NUMBER
)
/