CREATE MATERIALIZED VIEW APPS.XXWC_MD_SEARCH_PRODUCTS_MV
REFRESH COMPLETE ON DEMAND
/*************************************************************************
 Copyright (c) 2012 HD Supply
 All rights reserved.
**************************************************************************
   $Header APPS.XXWC_MD_SEARCH_PRODUCTS_MV $
  Module Name: APPS.XXWC_MD_SEARCH_PRODUCTS_MV 

  PURPOSE:Mv is created from the view so that context indexes can be placed on the mv

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     1-SEP-2015   Pahwa, Nancy                Initially Created 
TMS# 20150825-00012
  -- 1.1     01-Mar-2016  Pahwa Nancy 20160301-00328  AIS Performance Tuning
  -- 1.2     09-Mar-2016  Pahwa Nancy 20160315-00138  Uncommented out creation date
**************************************************************************/
AS 
SELECT inventory_item_id,
       organization_id,
       partnumber,
       TYPE,
       manufacturerpartnumber,
       manufacturer,
       sequence,
       currencycode,
       name,
       shortdescription,
       longdescription,
       thumbnail,
       fullimage,
       quantitymeasure,
       weightmeasure,
       weight,
       buyable,
       keyword,
       creation_date, --1.2
       item_type,
       cross_reference,
       dummy,
       primary_uom_code
  FROM APPS.XXWC_MD_SEARCH_PRODUCTS_VW;
/