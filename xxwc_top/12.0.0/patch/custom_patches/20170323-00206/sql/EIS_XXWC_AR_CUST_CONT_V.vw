/**************************************************************************************************************
  $Header XXEIS.EIS_XXWC_AR_CUST_CONT_V.vw $
  Module Name: Receivables
  PURPOSE: View for EIS Report Customer Contact Report - WC
  TMS Task Id : 20150825-00223
  REVISIONS:
  Ver        Date        Author                Description
  ---------  ----------  ------------------    ----------------
  1.0        09/18/2015  Mahender Reddy         Initial Version
  1.1		 10/18/2016	 Siva 					TMS#20160831-00146 -- Performance tuning
  1.2		 07/20/2017  Siva					TMS#20170323-00206 
**************************************************************************************************************/
CREATE OR REPLACE VIEW "XXEIS"."EIS_XXWC_AR_CUST_CONT_V" ("PARTY_ID", "ACCOUNT_NUMBER", "ACCOUNT_NAME", "FULL_NAME", "SITE_NUMBER", "SITE_NAME", "ADDRESS", "PHONE_NUMBER", "ROLE", "LOCATION_ID", "CONTACT_NUMBER", "FAX", "SALESPERSON", "STATUS", "EMAIL_ADDRESS", "ACCOUNT_STATUS", "SALES_PERSON_NTID", "BMGR", "DSMGR", "DMGR", "RSMGR", "RVP", "CREDIT_MANAGER", "LAST_INVOICE_DATE", "LAST_UPDATE_DATE") AS 
SELECT
  --start added for version 1.1
  -- DISTINCT  --Commented for version 1.2
  PARTY_ID,
  ACCOUNT_NUMBER,
  ACCOUNT_NAME,
  FULL_NAME,
  SITE_NUMBER,
  SITE_NAME,
  ADDRESS,
  PHONE_NUMBER,
  ROLE,
  LOCATION_ID,
  CONTACT_NUMBER,
  FAX,
  SALESPERSON,
  STATUS,
  EMAIL_ADDRESS
  -- End version 1.1
  --start added for version 1.2
  ,
  account_status,
  sales_person_ntid,
  bmgr,
  dsmgr,
  dmgr,
  rsmgr,
  rvp,
  credit_manager,
  last_invoice_date,
  Last_Update_Date
  -- End version 1.2
FROM
  (SELECT /*+ INDEX(hca HZ_CUST_ACCOUNTS_U1) INDEX(hcas XXWC_OBIEE_HZ_CUSTACTSTS_AL_3) INDEX(hcsu HZ_CUST_SITE_USES_N1) INDEX(sr JTF_RS_SALESREPS_U1)*/
    party.party_id party_id,
    hca.account_number account_number,
    hca.account_name account_name,
    party.party_name full_name ,
    hps.party_site_number site_number,
    hps.party_site_name site_name,
    (SELECT (hl.address1
      ||' '
      ||hl.address2
      ||' '
      ||hl.address3
      ||' '
      ||hl.address4
      ||' '
      ||hl.city
      ||' '
      ||hl.state
      ||' '
      ||hl.postal_code
      ||' '
      ||hl.country)
    FROM apps.hz_org_contacts hoc,
      hz_relationships hr ,
      hz_parties hp1,
      hz_party_sites hps1,
      hz_locations hl,
      hz_parties hp,
      hz_cust_accounts hca1
    WHERE hoc.party_relationship_id = hr.relationship_id
    AND hr.relationship_code        = 'CONTACT_OF'
    AND hr.subject_id               = hp1.party_id
    AND hr.party_id                 = hps1.party_id
    AND hps1.location_id            = hl.location_id
    AND hr.object_id                = hp.party_id
    AND hp.party_id                 = hca1.party_id
    AND hca1.cust_account_id        = hca.cust_account_id
    AND rownum                      = 1
    ) address,
    (SELECT DECODE(hcpp.phone_number,NULL,NULL, '('
      ||hcpp.phone_area_code
      ||') '
      ||hcpp.phone_number )
    FROM hz_contact_points hcpp
    WHERE hcpp.owner_table_id   = hcar.party_id --added for version 1.1
    AND hcpp.owner_table_name   = 'HZ_PARTIES'  --added for version 1.1
    AND hcpp.contact_point_type = 'PHONE'
    AND rownum                  = 1
    ) phone_number,
    -- NVL(flv.meaning,'Contact Only') Role, --Commented for version 1.1
    flv.meaning role,
    hps.location_id location_id,
    org_cont.contact_number,
    (SELECT DECODE(hcpc.phone_line_type,NULL,NULL, hcpc.phone_area_code
      ||hcpc.phone_number )
    FROM hz_contact_points hcpc
    WHERE hcpc.owner_table_id = hcar.party_id --added for version 1.1
    AND hcpc.owner_table_name = 'HZ_PARTIES'  --added for version 1.1
    AND hcpc.phone_line_type  = 'FAX'
    AND rownum                = 1
    ) fax,
    res.resource_name salesperson,
    CASE
      WHEN hcar.current_role_state = 'A'
      THEN 'ACTIVE'
      WHEN hcar.current_role_state = 'I'
      THEN 'INACTIVE'
      ELSE 'ALL'
    END status,
    (SELECT hcpe.email_address
    FROM hz_contact_points hcpe
    WHERE hcpe.owner_table_id   = hcar.party_id --added for version 1.1
    AND hcpe.owner_table_name   = 'HZ_PARTIES'  --added for version 1.1
    AND hcpe.contact_point_type = 'EMAIL'
    AND rownum                  = 1
    ) EMAIL_ADDRESS
	--start added for version 1.2
	,
    DECODE(hca.status,'A','ACTIVE','I','INACTIVE',hca.status) account_status,
    (SELECT fu.user_name FROM fnd_user fu WHERE fu.employee_id=sr.person_id
    )sales_person_ntid,
    (SELECT xssh.mgr_name
    FROM xxwc.xxwc_sr_salesrep_hierarchy xssh
    WHERE xssh.sr_id =sr.salesrep_id
    AND xssh.mgt_desc='BM'
    )bmgr,
    (SELECT xssh.mgr_name
    FROM xxwc.xxwc_sr_salesrep_hierarchy xssh
    WHERE xssh.sr_id =sr.salesrep_id
    AND xssh.mgt_desc='DSM'
    )dsmgr,
    (SELECT xssh.mgr_name
    FROM xxwc.xxwc_sr_salesrep_hierarchy xssh
    WHERE xssh.sr_id =sr.salesrep_id
    AND xssh.mgt_desc='DM'
    )dmgr,
    (SELECT xssh.mgr_name
    FROM xxwc.xxwc_sr_salesrep_hierarchy xssh
    WHERE xssh.sr_id =sr.salesrep_id
    AND xssh.mgt_desc='RSM'
    )rsmgr,
    (SELECT xssh.mgr_name
    FROM xxwc.xxwc_sr_salesrep_hierarchy xssh
    WHERE xssh.sr_id =sr.salesrep_id
    AND xssh.mgt_desc='RVP'
    )rvp,
    (SELECT ac.name
    FROM apps.hz_customer_profiles hcp,
      apps.ar_collectors ac
    WHERE hcp.cust_account_id = hca.cust_account_id
    AND hcp.site_use_id      IS NULL
    AND hcp.collector_id      = ac.collector_id
    AND rownum                =1
    ) Credit_Manager,
    (SELECT MAX(PS.GL_DATE) GL_DATE
    FROM ar_payment_schedules ps
    WHERE ps.customer_id = hca.cust_account_id
    ) last_invoice_date,
    (
     CASE
        WHEN TRUNC(party.last_update_date)>TRUNC(org_cont.last_update_date)
        THEN TRUNC(party.last_update_date)
        ELSE TRUNC(org_cont.last_update_date)
     END) last_update_date
	-- End version 1.2
  FROM hz_cust_account_roles hcar,
    hz_cust_accounts hca,
    apps.hz_relationships rel,
    apps.hz_parties party,
    hz_party_sites hps,
    hz_cust_acct_sites_all hcas,
    hz_cust_site_uses_all hcsu,
    apps.jtf_rs_salesreps sr,
    jtf_rs_resource_extns_vl res,
    hz_role_responsibility hrr,
    FND_LOOKUP_VALUES FLV,
    --  APPS.hz_contact_points hcp --Comented for version 1.1
    apps.hz_org_contacts org_cont
  WHERE hcar.cust_account_id    = hca.cust_account_id
  AND hcar.role_type            = 'CONTACT'
  AND HCAR.CUST_ACCT_SITE_ID   IS NULL
  AND hcar.party_id             = rel.party_id
  AND rel.subject_table_name    = 'HZ_PARTIES'
  AND rel.object_table_name     = 'HZ_PARTIES'
  AND rel.directional_flag      = 'F'
  AND rel.subject_id            = party.party_id
  AND party.party_id            = hps.party_id(+)
  AND hca.cust_account_id       = hcas.cust_account_id
  AND hcas.cust_acct_site_id    = hcsu.cust_acct_site_id
  AND hcsu.site_use_code        = 'BILL_TO'
  AND hcsu.primary_flag         = 'Y'
  AND hcsu.primary_salesrep_id  = sr.salesrep_id (+)
  AND hcsu.org_id               = sr.org_id(+) --added for version 1.1
  AND sr.resource_id            = res.resource_id (+)
  AND hcar.cust_account_role_id = hrr.cust_account_role_id
  AND hrr.responsibility_type   = flv.lookup_code
  AND FLV.LOOKUP_TYPE           ='SITE_USE_CODE'
    --AND hcar.party_id             = hcp.owner_table_id(+) --Comented for version 1.1
    --AND hcp.owner_table_name(+)   = 'HZ_PARTIES' --Comented for version 1.1
    --AND SUBSTR(hcsu.location,-9) = hca.account_number
    --AND hcsu.location like '%'||hca.account_number||'%'
    /*AND hcp.contact_point_type(+)        IN ('PHONE','EMAIL')
    AND (hcp.phone_line_type    = 'GEN'
    OR hcp.phone_line_type     IS NULL)*/
  AND rel.relationship_id = org_cont.party_relationship_id
  UNION ALL
  --Added below union for version 1.1 for getting 'Contact Only' Role values
  SELECT /*+ INDEX(hca HZ_CUST_ACCOUNTS_U1) INDEX(hcas XXWC_OBIEE_HZ_CUSTACTSTS_AL_3) INDEX(hcsu HZ_CUST_SITE_USES_N1) INDEX(sr JTF_RS_SALESREPS_U1)*/
    party.party_id party_id,
    hca.account_number account_number,
    hca.account_name account_name,
    party.party_name full_name ,
    hps.party_site_number site_number,
    hps.party_site_name site_name,
    (SELECT (hl.address1
      ||' '
      ||hl.address2
      ||' '
      ||hl.address3
      ||' '
      ||hl.address4
      ||' '
      ||hl.city
      ||' '
      ||hl.state
      ||' '
      ||hl.postal_code
      ||' '
      ||hl.country)
    FROM apps.hz_org_contacts hoc,
      hz_relationships hr ,
      hz_parties hp1,
      hz_party_sites hps1,
      hz_locations hl,
      hz_parties hp,
      hz_cust_accounts hca1
    WHERE hoc.party_relationship_id = hr.relationship_id
    AND hr.relationship_code        = 'CONTACT_OF'
    AND hr.subject_id               = hp1.party_id
    AND hr.party_id                 = hps1.party_id
    AND hps1.location_id            = hl.location_id
    AND hr.object_id                = hp.party_id
    AND hp.party_id                 = hca1.party_id
    AND hca1.cust_account_id        = hca.cust_account_id
    AND rownum                      = 1
    ) address,
    (SELECT DECODE(hcpp.phone_number,NULL,NULL, '('
      ||hcpp.phone_area_code
      ||') '
      ||hcpp.phone_number )
    FROM hz_contact_points hcpp
    WHERE hcpp.owner_table_id   = hcar.party_id
    AND hcpp.owner_table_name   = 'HZ_PARTIES'
    AND hcpp.contact_point_type = 'PHONE'
    AND rownum                  = 1
    ) phone_number,
    'Contact Only' role,
    hps.location_id location_id,
    org_cont.contact_number,
    (SELECT DECODE(hcpc.phone_line_type,NULL,NULL, hcpc.phone_area_code
      ||hcpc.phone_number )
    FROM hz_contact_points hcpc
    WHERE hcpc.owner_table_id = hcar.party_id
    AND hcpc.owner_table_name = 'HZ_PARTIES'
    AND hcpc.phone_line_type  = 'FAX'
    AND rownum                = 1
    ) fax,
    res.resource_name salesperson,
    CASE
      WHEN hcar.current_role_state = 'A'
      THEN 'ACTIVE'
      WHEN hcar.current_role_state = 'I'
      THEN 'INACTIVE'
      ELSE 'ALL'
    END status,
    (SELECT hcpe.email_address
    FROM hz_contact_points hcpe
    WHERE hcpe.owner_table_id   = hcar.party_id
    AND hcpe.owner_table_name   = 'HZ_PARTIES'
    AND hcpe.contact_point_type = 'EMAIL'
    AND rownum                  = 1
    ) EMAIL_ADDRESS
	--start added for version 1.2
	,
    DECODE(hca.status,'A','ACTIVE','I','INACTIVE',hca.status) account_status,
    (SELECT fu.user_name FROM fnd_user fu WHERE fu.employee_id=sr.person_id
    )sales_person_ntid,
    (SELECT xssh.mgr_name
    FROM xxwc.xxwc_sr_salesrep_hierarchy xssh
    WHERE xssh.sr_id =sr.salesrep_id
    AND xssh.mgt_desc='BM'
    )bmgr,
    (SELECT xssh.mgr_name
    FROM xxwc.xxwc_sr_salesrep_hierarchy xssh
    WHERE xssh.sr_id =sr.salesrep_id
    AND xssh.mgt_desc='DSM'
    )dsmgr,
    (SELECT xssh.mgr_name
    FROM xxwc.xxwc_sr_salesrep_hierarchy xssh
    WHERE xssh.sr_id =sr.salesrep_id
    AND xssh.mgt_desc='DM'
    )dmgr,
    (SELECT xssh.mgr_name
    FROM xxwc.xxwc_sr_salesrep_hierarchy xssh
    WHERE xssh.sr_id =sr.salesrep_id
    AND xssh.mgt_desc='RSM'
    )rsmgr,
    (SELECT xssh.mgr_name
    FROM xxwc.xxwc_sr_salesrep_hierarchy xssh
    WHERE xssh.sr_id =sr.salesrep_id
    AND xssh.mgt_desc='RVP'
    )rvp,
    (SELECT ac.name
    FROM apps.hz_customer_profiles hcp,
      apps.ar_collectors ac
    WHERE hcp.cust_account_id = hca.cust_account_id
    AND hcp.site_use_id      IS NULL
    AND hcp.collector_id      = ac.collector_id
    AND rownum                =1
    ) Credit_Manager,
    (SELECT MAX(PS.GL_DATE) GL_DATE
    FROM ar_payment_schedules ps
    WHERE ps.customer_id = hca.CUST_ACCOUNT_ID
    ) last_invoice_date,
    (
     CASE
        WHEN TRUNC(party.last_update_date)>TRUNC(org_cont.last_update_date)
        THEN TRUNC(party.last_update_date)
        ELSE TRUNC(org_cont.last_update_date)
     END) last_update_date
	-- End version 1.2
  FROM hz_cust_account_roles hcar,
    hz_cust_accounts hca,
    apps.hz_relationships rel,
    apps.hz_parties party,
    hz_party_sites hps,
    hz_cust_acct_sites_all hcas,
    hz_cust_site_uses_all hcsu,
    apps.jtf_rs_salesreps sr,
    jtf_rs_resource_extns_vl res,
    apps.hz_org_contacts org_cont
  WHERE hcar.cust_account_id   = hca.cust_account_id
  AND hcar.role_type           = 'CONTACT'
  AND HCAR.CUST_ACCT_SITE_ID  IS NULL
  AND hcar.party_id            = rel.party_id
  AND rel.subject_table_name   = 'HZ_PARTIES'
  AND rel.object_table_name    = 'HZ_PARTIES'
  AND rel.directional_flag     = 'F'
  AND rel.subject_id           = party.party_id
  AND party.party_id           = hps.party_id(+)
  AND hca.cust_account_id      = hcas.cust_account_id
  AND hcas.cust_acct_site_id   = hcsu.cust_acct_site_id
  AND hcsu.site_use_code       = 'BILL_TO'
  AND hcsu.primary_flag        = 'Y'
  AND hcsu.primary_salesrep_id = sr.salesrep_id (+)
  AND hcsu.org_id              = sr.org_id(+)
  AND sr.resource_id           = res.resource_id (+)
  AND rel.relationship_id      = org_cont.party_relationship_id
  AND NOT EXISTS
    (SELECT 1
    FROM hz_role_responsibility hrr,
      fnd_lookup_values flv
    WHERE hrr.cust_account_role_id = hcar.cust_account_role_id
    AND hrr.responsibility_type    = flv.lookup_code
    AND flv.lookup_type            ='SITE_USE_CODE'
    )
)
/