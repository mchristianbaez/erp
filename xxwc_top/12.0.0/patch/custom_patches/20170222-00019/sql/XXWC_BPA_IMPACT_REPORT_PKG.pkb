CREATE OR REPLACE PACKAGE BODY APPS.XXWC_BPA_IMPACT_REPORT_PKG AS
   /*******************************************************************************************************************************
   *    $Header XXWC_BPA_IMPACT_REPORT_PKG $                                                                                      *
   *   Module Name: XXWC_BPA_IMPACT_REPORT_PKG                                                                                    *
   *                                                                                                                              *
   *   PURPOSE:   This package is used to create the CSV version of the impact report                                             *
   *                                                                                                                              *
   *   REVISIONS:                                                                                                                 *
   *   Ver        Date        Author                     Description                                                              *
   *   ---------  ----------  ---------------         -------------------------                                                   *
   *   1.0        08/25/2014  Lee Spitzer              Initial Version 20140726-00006                                             *
   *                                                   New BPA Impact Report - deploy CSV version                                 *
   *   1.1        12/14/2014  Lee Spitzer              Updated Version for additional columns                                     *
   *                                                   Initial Version 20140726-00006                                             *
   *                                                                                                                              *
   *   1.2        08/05/2015  P.Vamshidhar             TMS#20150805-00077                                                         *   
   *                                                   Rolledback changes to text report (old report) and issue fixed for CSV     *
   *                                                   1.2 version created on 3430 Revision and remaining versions will be ignored*
   *   1.3        09/21/2015  Manjula Chellappan       TMS# 20150903-00018 - BPA- Cost Management issues                          *
   *   1.4        02/28/2017  Pattabhi Avula           TMS# 20170222-00019 - XXWC_BPA_IMPACT_REPORT_PKG Error Alert fixes         *        
   /*******************************************************************************************************************************/

G_EXCEPTION EXCEPTION;
g_err_msg         VARCHAR2 (2000);
g_err_callfrom    VARCHAR2 (175) := 'XXWC_BPA_IMPACT_REPORT_PKG';
g_err_callpoint   VARCHAR2 (175) := 'START';
--g_distro_list     VARCHAR2 (80) := 'OracleDevelopmentGroup@hdsupply.com'; Removed per code review from Manny - 9/3/2014/
g_distro_list     VARCHAR2 (80) := 'hdsoracledevelopers@hdsupply.com'; --Updated per code review to use new Distribution List per Manny - 9/3/2014
g_module          VARCHAR2 (80) := 'APPS';


   /*************************************************************************************************
   *   Procedure generate_csv                                                                       *
   *   Purpose : Procudure inserts records into temp table                                          *
   *                                                                                                *
   *                                                                                                *
   *              PROCEDURE insert_into_temp                                                        *
   *                        (p_po_number         IN VARCHAR2                                        *
   *                        ,p_mst_org_id        IN NUMBER                                          *
   *                        ,p_org_id            IN NUMBER                                          *
   *                        ,x_return            OUT NUMBER                                         *
   *                        ,x_message           OUT VARCHAR2                                       *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.0        08/25/2014  Lee Spitzer              Initial Version 20140726-00006               *
   *                                                     New BPA Impact Report - deploy CSV version *
   *                                                                                                *
   *   1.1        12/14/2014  Lee Spitzer              Updated Version for additional columns       *
   *                                                      Initial Version 20140726-00006            *
   /************************************************************************************************/

    PROCEDURE generate_csv (errbuf               OUT VARCHAR2
                           ,retcode              OUT VARCHAR2
                           ,P_PO_NUMBER          IN  VARCHAR2
                           ,P_DC_MODE            IN  VARCHAR2
                           ,P_NOTES              IN  VARCHAR2
                           ,P_MST_ORG_ID         IN  NUMBER
                           ,P_ORG_ID             IN  NUMBER) IS

      l_vendor_name       VARCHAR2(240);
      l_vendor_number     VARCHAR2(30);
      l_vendor_site_code  VARCHAR2(15);
      l_po_header_id      NUMBER;
      l_implement_date    DATE;
      l_start_date        DATE;
      l_history_units     NUMBER;
      l_history_cogs      NUMBER;
      l_total_history_cogs NUMBER;
      l_total_forecast_cogs NUMBER;
      l_total_impact_dollars NUMBER;
      l_total_percent_change NUMBER;
      --Added 12/14/2014
      l_total_history_units NUMBER;
      l_total_purchase_history NUMBER;
      l_total_purchase_cost NUMBER;
      l_total_purchase_avg NUMBER;
      l_total_forecast_spend NUMBER;
      l_total_purchase_impact NUMBER;
      l_total_purchase_change NUMBER;
      l_denominator NUMBER;
      l_district_percent_change NUMBER;
      l_district_purchase_change NUMBER;
      l_region_percent_change NUMBER;
      l_region_purchase_change NUMBER;
      l_avg_unit_costs NUMBER;
      l_avg_unit_purchase NUMBER;


      CURSOR c_impact_details (p_po_header_id IN NUMBER) IS
        SELECT
             item_number,
             primary_uom_code,
             inventory_item_id,
             description,
             vendor_id,
             vendor_site_id,
             po_header_id,
             price_zone,
             organization_code,
             organization_id,
             org_pricing_zone,
             org_type,
             district,
             region,
             organization_name,
             district_detail,
             region_detail,
             rpm_dpm,
             current_cost,
             updated_cost,
             history_units,
             history_cogs,
             forecast_cogs,
             impact_dollars,
             percent_change,
             --Columns Added 12/14/2014
             PURCHASE_HISTORY,
             PURCHASE_COST,
             FORECAST_SPEND,
             PURCHASE_IMPACT,
             PURCHASE_CHANGE
      FROM   xxwc.xxwc_bpa_impact_report_temp
      WHERE  po_header_id = p_po_header_id
      ORDER BY organization_code, item_number;

      CURSOR c_district_sum (p_po_header_id IN NUMBER, p_region IN VARCHAR2) IS
        SELECT
               region_detail,
               district_detail,
               round(sum(nvl(history_cogs,0)),2) sum_history_cogs,
               round(sum(nvl(forecast_cogs,0)),2) sum_forecast_cogs,
               round(sum(nvl(impact_dollars,0)),2) sum_impact_dollars,
               -- round(sum(nvl(percent_change,0)),2) sum_percent_change,
              --round((sum(nvl(impact_dollars,0)) / (sum(nvl(history_cogs,0)))),2) sum_percent_change,
               --Added 12/14/2014
               round(sum(nvl(purchase_history,0)),2) sum_purchase_history,
               round(sum(nvl(purchase_cost,0)),2) sum_purchase_cost,
               --round(avg(nvl(purchase_cost,0)),2) avg_purchase_cost, removed 12/16/2014 per TJ
               round(sum(nvl(forecast_spend,0)),2) sum_forecast_spend,
               round(sum(nvl(purchase_impact,0)),2) sum_purchase_impact
               --round(sum(nvl(purchase_change,0)),2) sum_purchase_change
               --round((sum(nvl(purchase_impact,0)) / (sum(nvl(purchase_history,0)))),2) sum_purchase_change
        FROM   xxwc.xxwc_bpa_impact_report_temp
        WHERE  po_header_id = p_po_header_id
        AND    region_detail = p_region
        group by region_detail, district_detail
        order by region_detail, district_detail;

      CURSOR c_region_sum (p_po_header_id IN NUMBER) IS
        SELECT
               region_detail,
               round(sum(nvl(history_cogs,0)),2) sum_history_cogs,
               round(sum(nvl(forecast_cogs,0)),2) sum_forecast_cogs,
               round(sum(nvl(impact_dollars,0)),2) sum_impact_dollars,
               --round(sum(nvl(percent_change,0)),2) sum_percent_change,
               --round((sum(nvl(impact_dollars,0)) / (sum(nvl(history_cogs,0)))),2) sum_percent_change,
               --Added 12/14/2014
               round(sum(nvl(purchase_history,0)),2) sum_purchase_history,
               round(sum(nvl(purchase_cost,0)),2) sum_purchase_cost,
               round(avg(nvl(purchase_cost,0)),2) avg_purchase_cost,
               round(sum(nvl(forecast_spend,0)),2) sum_forecast_spend,
               round(sum(nvl(purchase_impact,0)),2) sum_purchase_impact
               --round(sum(nvl(purchase_change,0)),2) sum_purchase_change
               --round((sum(nvl(purchase_impact,0)) / (sum(nvl(purchase_history,0)))),2) sum_purchase_change
        FROM   xxwc.xxwc_bpa_impact_report_temp
        WHERE  po_header_id = p_po_header_id
        group by region_detail
        order by region_detail;

    BEGIN

    g_err_callpoint := 'generate_csv';

    --Parameters
     fnd_file.put_line(fnd_file.LOG, 'P_PO_NUMBER  ' || p_po_number);
     fnd_file.put_line(fnd_file.LOG, 'P_DC_MODE   ' || p_dc_mode);
     fnd_file.put_line(fnd_file.LOG, 'P_NOTES ' || p_notes);
     fnd_file.put_line(fnd_file.LOG, 'P_MST_ORG_ID ' || p_mst_org_id);
     fnd_file.put_line(fnd_file.LOG, 'P_ORG_ID ' || p_org_id);

    --Get Vendor Header Information
      BEGIN
        SELECT asa.vendor_name,
               asa.segment1,
               assa.vendor_site_code,
               pha.po_header_id
         INTO  l_vendor_name,
               l_vendor_number,
               l_vendor_site_code,
               l_po_header_id
        FROM  ap_suppliers asa,
               ap_supplier_sites_all assa,
               po_headers_all pha
         WHERE asa.vendor_id = assa.vendor_id
         AND   pha.segment1 = p_po_number
         AND   pha.org_id = p_org_id
         AND   pha.vendor_id = asa.vendor_id
         AND   pha.vendor_site_id = assa.vendor_site_id;
      EXCEPTION
        WHEN others THEN
            --Exit and raise error
            g_err_msg := 'WHEN OTHERS getting vendor header information ' || SQLCODE || SQLERRM;
            raise g_exception;
      END;

      ---Truncate Records in Temp Table
      BEGIN
        EXECUTE IMMEDIATE 'TRUNCATE TABLE XXWC.XXWC_BPA_IMPACT_REPORT_TEMP';
      EXCEPTION
        WHEN OTHERS THEN
          g_err_msg := 'Error truncating XXWC_BPA_IMPACT_REPORT_TEMP ' || SQLCODE || SQLERRM;
          raise g_exception;
      END;

      l_start_date := xxwc_bpa_impact_report_pkg.get_history_start_date(p_mst_org_id);

      fnd_file.put_line(fnd_file.LOG, 'Start Time Insert Into Temp ' || to_char(sysdate, 'DD-MON-YYYY HH:MI:SS'));

      --Insert Records into Temp Table
      insert_into_temp
                            (p_po_number         => p_po_number
                            ,p_mst_org_id        => p_mst_org_id
                            ,p_org_id            => p_org_id
                            ,p_dc_mode           => p_dc_mode
                            ,x_return            => g_return
                            ,x_message           => g_message);

        --IF there was an error inserting records into temp table, raise g_exception
        IF g_return > 0 THEN
            g_err_msg := g_message;
            raise g_exception;
        end if;

      fnd_file.put_line(fnd_file.LOG, 'End Time Insert Into Temp ' || to_char(sysdate, 'DD-MON-YYYY HH:MI:SS'));

        BEGIN
          SELECT
                 round(sum(nvl(history_cogs,0)),2) sum_history_cogs,
                 round(sum(nvl(forecast_cogs,0)),2) sum_forecast_cogs,
                 round(sum(nvl(impact_dollars,0)),2) sum_impact_dollars,
                  --Added 12/14/2014
                 round(sum(nvl(purchase_history,0)),2) sum_purchase_history,
                 round(sum(nvl(purchase_cost,0)),2) sum_purchase_cost,
                 --round(avg(nvl(purchase_cost,0)),2) avg_purchase_cost, removed 12/16/2014 per TJ
                 round(sum(nvl(forecast_spend,0)),2) sum_forecast_spend,
                 round(sum(nvl(purchase_impact,0)),2) sum_purchase_impact,
                 round(sum(nvl(history_units,0)),2) sum_history_units
          INTO  l_total_history_cogs,
                l_total_forecast_cogs,
                l_total_impact_dollars,
                --Added 12/14/2014
                l_total_purchase_history,
                l_total_purchase_cost,
                --l_total_purchase_avg,
                l_total_forecast_spend,
                l_total_purchase_impact,
                l_total_history_units
          FROM   xxwc.xxwc_bpa_impact_report_temp
          WHERE  po_header_id = l_po_header_id;

        EXCEPTION
            WHEN OTHERS THEN
                l_total_history_cogs := 0;
                l_total_forecast_cogs := 0;
                l_total_impact_dollars := 0;
                l_total_purchase_history :=0;
                l_total_purchase_cost :=0;
                --l_total_purchase_avg :=0;
                l_total_forecast_spend :=0;
                l_total_purchase_impact :=0;
        END;

        fnd_file.put_line(fnd_file.LOG, 'End Time Getting Totals ' || to_char(sysdate, 'DD-MON-YYYY HH:MI:SS'));

          if nvl(l_total_history_cogs,0) = 0 then

                l_denominator := .01;

          else

                l_denominator := l_total_history_cogs;

          END IF;

          l_total_percent_change := round(((nvl(l_total_impact_dollars,0) / l_denominator) * 100),2);


          if nvl(l_total_purchase_cost,0) = 0 then

                l_denominator := .01;

          else

                l_denominator := l_total_purchase_cost;

          end if;

          l_total_purchase_change := round(((nvl(l_total_purchase_impact,0) / l_denominator) * 100),2);

        fnd_file.put_line(fnd_file.LOG, 'Start Time Generating FND Output ' || to_char(sysdate, 'DD-MON-YYYY HH:MI:SS'));

      -- generate report output
        fnd_file.put_line (fnd_file.output
                          ,TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI') || '||||||HD Supply BPA Impact Report');
        fnd_file.put_line (fnd_file.output, 'Parameters');
        fnd_file.put_line (fnd_file.output, 'BPA|' || p_po_number);
        fnd_file.put_line (fnd_file.output, 'Vendor Name|' || l_vendor_name);
        fnd_file.put_line (fnd_file.output, 'Vendor Number|' || l_vendor_number);
        fnd_file.put_line (fnd_file.output, 'Vendor Site Code|' || l_vendor_site_code);
        fnd_file.put_line (fnd_file.output, 'Implement Date|' || l_implement_date);
        fnd_file.put_line (fnd_file.output, 'History Start date|' || l_start_date);
        fnd_file.put_line (fnd_file.output, 'Request Id|' || fnd_global.CONC_REQUEST_ID);
        fnd_file.put_line (fnd_file.output, 'Date|' || sysdate);
        fnd_file.put_line (fnd_file.output, 'Notes|' || p_notes);

        fnd_file.put_line (fnd_file.output, ' ');
        fnd_file.put_line (fnd_file.output, 'Regional Summary');
        fnd_file.put_line (fnd_file.output, ' ');

        fnd_file.put_line (fnd_file.output, 'REGION'
                                             ||'|'||
                                             'DISTRICT'
                                             ||'|'||
                                             'PRIOR_12_MO_COGS'
                                             ||'|'||
                                             'NEW_12_MO_COGS'
                                             ||'|'||
                                             'IMPACT'
                                             ||'|'||
                                             'CHANGE_PERCENT'
                                             ||'|'||
                                             '12_MO_UNITS_PUR'
                                             ||'|'||
                                             '12_MO_$$_PUR'
                                             --||'|'|| removed 12/16/2014 per TJ
                                             --'AVG_PO$'
                                             ||'|'||
                                             'FORECAST_SPEND'
                                             ||'|'||
                                             'PUR_IMPACT_$$'
                                             ||'|'||
                                             'PUR_%_CHANGE'
                                             );

        fnd_file.put_line(fnd_file.LOG, 'Start Time Generating Region Sum ' || to_char(sysdate, 'DD-MON-YYYY HH:MI:SS'));

        FOR r_region_sum IN c_region_sum (l_po_header_id)

          loop


          for r_district_sum in c_district_sum (l_po_header_id, r_region_sum.region_detail)

            loop

              if nvl(r_district_sum.sum_history_cogs,0) = 0 then

                l_denominator := .01;

              ELSE

                 l_denominator := r_district_sum.sum_history_cogs;

              END IF;

                l_district_percent_change := round(((nvl(r_district_sum.sum_impact_dollars,0) / l_denominator) * 100),2);


              if nvl(r_district_sum.sum_purchase_cost,0) = 0 then

                    l_denominator := .01;

              else

                    l_denominator := r_district_sum.sum_purchase_cost;

              END IF;

              l_district_purchase_change := round(((nvl(r_district_sum.sum_purchase_impact,0) / l_denominator) * 100),2);

              fnd_file.put_line (fnd_file.output, r_district_sum.region_detail
                                                  ||'|'||
                                                  r_district_sum.district_detail
                                                  ||'|$'||
                                                  r_district_sum.sum_history_cogs
                                                  ||'|$'||
                                                  r_district_sum.sum_forecast_cogs
                                                  ||'|$'||
                                                  r_district_sum.sum_impact_dollars
                                                  ||'|'||
                                                  l_district_percent_change
                                                  ||'%|'||
                                                  r_district_sum.sum_purchase_history
                                                  ||'|$'||
                                                  r_district_sum.sum_purchase_cost
                                                  ||'|$'||
                                                  --r_district_sum.avg_purchase_cost removed 12/16/2014 per TJ
                                                  --||'|$'||
                                                  r_district_sum.sum_forecast_spend
                                                  ||'|$'||
                                                  r_district_sum.sum_purchase_impact
                                                  ||'|'||
                                                  l_district_purchase_change
                                                  ||'%');

            end loop;



              if nvl(r_region_sum.sum_history_cogs,0) = 0 then

                l_denominator := .01;

              ELSE

                 l_denominator := r_region_sum.sum_history_cogs;

              END IF;

                l_region_percent_change := round(((nvl(r_region_sum.sum_impact_dollars,0) / l_denominator) * 100),2);


              if nvl(r_region_sum.sum_purchase_cost,0) = 0 then

                    l_denominator := .01;

              else

                    l_denominator := r_region_sum.sum_purchase_cost;

              end if;

              l_region_purchase_change := round(((nvl(r_region_sum.sum_purchase_impact,0) / l_denominator) * 100),2);


             fnd_file.put_line (fnd_file.output, r_region_sum.region_detail
                                                  ||' Total||$'||
                                                  r_region_sum.sum_history_cogs
                                                  ||'|$'||
                                                  r_region_sum.sum_forecast_cogs
                                                  ||'|$'||
                                                  r_region_sum.sum_impact_dollars
                                                  ||'|'||
                                                  l_region_percent_change
                                                  ||'%|'||
                                                  r_region_sum.sum_purchase_history
                                                  ||'|$'||
                                                  r_region_sum.sum_purchase_cost
                                                  ||'|$'||
                                                  --r_region_sum.avg_purchase_cost removed 12/16/2014 Per TJ
                                                  --||'|$'||
                                                  r_region_sum.sum_forecast_spend
                                                  ||'|$'||
                                                  r_region_sum.sum_purchase_impact
                                                  ||'|'||
                                                  l_region_purchase_change
                                                  ||'%');

    END LOOP;

    fnd_file.put_line(fnd_file.LOG, 'End Time Generating Region Sum ' || to_char(SYSDATE, 'DD-MON-YYYY HH:MI:SS'));

        fnd_file.put_line (fnd_file.output,       'Grand Total||$'||
                                                  l_total_history_cogs
                                                  ||'|$'||
                                                  l_total_forecast_cogs
                                                  ||'|$'||
                                                  l_total_impact_dollars
                                                  ||'|'||
                                                  l_total_percent_change
                                                  ||'%|'||
                                                  l_total_purchase_history
                                                  ||'|$'||
                                                  l_total_purchase_cost
                                                  --||'|$'|| removed 12/16/2014 per TJ
                                                  --l_total_purchase_avg 12/16/2014 per TJ
                                                  ||'|$'||
                                                  l_total_forecast_spend
                                                  ||'|$'||
                                                  l_total_purchase_impact
                                                  ||'|'||
                                                  l_total_purchase_change
                                                  ||'%');


        fnd_file.put_line (fnd_file.output, ' ');
        fnd_file.put_line (fnd_file.output, 'Cost Change Data');
        fnd_file.put_line (fnd_file.output, ' ');
        fnd_file.put_line (fnd_file.output, 'ORGANIZATION_CODE'
                                            ||'|'||
                                            'WHITE_CAP_PART'
                                            ||'|'||
                                            'WHITE_CAP_DESCRIPTION'
                                            ||'|'||
                                            'UOM'
                                            ||'|'||
                                            'CURRENT_COST'
                                            ||'|'||
                                            'UPDATED_COST'
                                            ||'|'||
                                            '12_MO_UNITS'
                                            ||'|'||
                                            '12_MO_COGS'
                                            ||'|'||
                                            'FORECAST_12_COGS'
                                            ||'|'||
                                            'AVG_UNIT_COGS'
                                            ||'|'||
                                            'IMPACT_$$'
                                            ||'|'||
                                            '%_CHANGE'
                                            ||'|'||
                                            'REGION'
                                            ||'|'||
                                            'DISTRICT'
                                            ||'|'||
                                            'RPM/DPM'
                                            ||'|'||
                                            '12_MO_UNITS_PUR'
                                            ||'|'||
                                            '12_MO_$$_PUR'
                                            ||'|'||
                                            'AVG_UNIT_PO$'
                                            ||'|'||
                                            'FORECAST_SPEND'
                                            ||'|'||
                                            'PUR_IMPACT_$$'
                                            ||'|'||
                                            'PUR_%_CHANGE'
                                            );

    fnd_file.put_line(fnd_file.LOG, 'Start Time Generating Detail Sum ' || to_char(SYSDATE, 'DD-MON-YYYY HH:MI:SS'));


                     FOR r_impact_details IN c_impact_details(l_po_header_id)

                      LOOP

                          IF r_impact_details.history_units = 0 THEN

                              l_denominator := .01;

                          ELSE

                            l_denominator := r_impact_details.history_units;

                          end if;


                        l_avg_unit_costs := round((r_impact_details.history_cogs / l_denominator),2);


                          IF r_impact_details.purchase_history = 0 THEN

                            l_denominator := .01;

                          else

                            l_denominator := r_impact_details.purchase_history;

                          end if;

                        l_avg_unit_purchase := round((r_impact_details.purchase_cost / l_denominator),2);


                        fnd_file.put_line(fnd_file.output, r_impact_details.organization_code --'ORGANIZATION_CODE'
                                            ||'|'||
                                            r_impact_details.item_number --'WHITE_CAP_PART'
                                            ||'|'||
                                            r_impact_details.description --'WHITE_CAP_DESCRIPTION'
                                            ||'|'||
                                            r_impact_details.primary_uom_code --'UOM'
                                            ||'|$'||
                                            r_impact_details.current_cost --'CURRENT_COST'
                                            ||'|$'||
                                            r_impact_details.updated_cost --'UPDATED_COST'
                                            ||'|'||
                                            r_impact_details.history_units --'12_MO_UNITS'
                                            ||'|$'||
                                            r_impact_details.history_cogs --'12_MO_COGS'
                                            ||'|$'||
                                            r_impact_details.forecast_cogs --'FORECAST_12_COGS'
                                            ||'|$'||
                                            l_avg_unit_costs
                                            ||'|$'||
                                            r_impact_details.impact_dollars --'IMPACT_$$'
                                            ||'|'||
                                            r_impact_details.percent_change --'%_CHANGE'
                                            ||'%|'||
                                            r_impact_details.region_detail --'REGION'
                                            ||'|'||
                                            r_impact_details.district_detail --'DISTRICT'
                                            ||'|'||
                                            r_impact_details.rpm_dpm --'RPM/DPM'
                                            ||'|'||
                                            r_impact_details.purchase_history
                                            ||'|$'||
                                            r_impact_details.purchase_cost
                                            ||'|$'||
                                            l_avg_unit_purchase
                                            ||'|$'||
                                            r_impact_details.forecast_spend
                                            ||'|$'||
                                            r_impact_details.purchase_impact
                                            ||'|'||
                                            r_impact_details.purchase_change
                                            ||'%'
                                            );




                      END LOOP;

      fnd_file.put_line(fnd_file.LOG, 'End Time Generating Detail Sum ' || to_char(SYSDATE, 'DD-MON-YYYY HH:MI:SS'));


       fnd_file.put_line (fnd_file.output,       'Grand Total||||||'||
                                                  l_total_history_units
                                                  ||'|$'||
                                                  l_total_history_cogs
                                                  ||'|$'||
                                                  l_total_forecast_cogs
                                                  ||'|' --Average COGS
                                                  ||'|$'||
                                                  l_total_impact_dollars
                                                  ||'|'||
                                                  l_total_percent_change
                                                  ||'%|' --Region
                                                  ||'|' --District
                                                  ||'|' --RPM/DPM
                                                  ||'|'||
                                                  l_total_purchase_history
                                                  ||'|$'||
                                                  l_total_purchase_cost
                                                  ||'|' --Total Purchase Average
                                                  ||'|$'||
                                                  l_total_forecast_spend
                                                  ||'|$'||
                                                  l_total_purchase_impact
                                                  ||'|'||
                                                  l_total_purchase_change
                                                  ||'%'
                                                  );


    EXCEPTION

    WHEN G_EXCEPTION THEN

          retcode := 1;
          errbuf := g_err_msg;

          fnd_file.put_line(fnd_file.log, g_err_msg);

            xxcus_error_pkg.xxcus_error_main_api (
                p_called_from         => g_err_callfrom
               ,p_calling             => g_err_callpoint
               ,p_ora_error_msg       => g_err_msg
               ,p_error_desc          => 'WHEN G_EXCEPTION in call ' || g_err_callfrom || g_err_callpoint
               ,p_distribution_list   => g_distro_list
               ,p_module              => g_module);

      WHEN OTHERS THEN

       g_err_msg := 'Error in ' || g_err_callpoint || ' WITH  ' || SUBSTR (SQLERRM, 1, 1000);

          retcode := 1;
          errbuf := g_err_msg;

          fnd_file.put_line(fnd_file.log, g_err_msg);

            xxcus_error_pkg.xxcus_error_main_api (
                p_called_from         => g_err_callfrom
               ,p_calling             => g_err_callpoint
               ,p_ora_error_msg       => g_err_msg
               ,p_error_desc          => 'WHEN OTHERS in call ' || g_err_callfrom || g_err_callpoint
               ,p_distribution_list   => g_distro_list
               ,p_module              => g_module);

    END generate_csv;

   /*************************************************************************************************
   *   Procedure insert_into_temp                                                                   *
   *   Purpose : Procudure inserts records into temp table                                          *
   *                                                                                                *
   *                                                                                                *
   *              PROCEDURE insert_into_temp                                                        *
   *                        (p_po_number         IN VARCHAR2                                        *
   *                        ,p_mst_org_id        IN NUMBER                                          *
   *                        ,p_org_id            IN NUMBER                                          *
   *                        ,x_return            OUT NUMBER
   *                        ,x_message           OUT VARCHAR2                                                                        *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.0        08/25/2014  Lee Spitzer              Initial Version 20140726-00006               *
   *                                                     New BPA Impact Report - deploy CSV version *
   *   1.3        09/21/2015  Manjula Chellappan       TMS#20150903-00018 BPA Cost Management issues*
   *   1.4        02/28/2017  Pattabhi Avula           TMS#20170222-00019 - XXWC_BPA_IMPACT_REPORT_PKG*
   *                                                                      Error Alert fixes         *   
   *                                                                                                *
   /************************************************************************************************/

    PROCEDURE insert_into_temp
                            (p_po_number         IN VARCHAR2
                            ,p_mst_org_id        IN NUMBER
                            ,p_org_id            IN NUMBER
                            ,p_dc_mode           IN VARCHAR2
                            ,x_return            OUT NUMBER
                            ,x_message           OUT VARCHAR2)
                            IS

    l_start_date date;
    l_history_units NUMBER;
    l_history_cogs NUMBER;
    l_forecast_cogs NUMBER;
    l_impact_dollars NUMBER;
    l_percent_change NUMBER;
    l_denominator  NUMBER;
    l_history_purchase NUMBER;
    l_history_purchase_cost NUMBER;
    l_forecast_purchase NUMBER;
    l_impact_purchase NUMBER;
    l_purchase_percent_change NUMBER;
	l_impact_flag NUMBER := 0; -- Added for Ver 1.3 
	l_impact_check VARCHAR2(1000); -- Added for Ver 1.3 	

    /*cursor c_temp IS
      select rowid row_id
            ,inventory_item_id
            ,organization_id
            ,updated_cost
      from  XXWC.XXWC_BPA_IMPACT_REPORT_TEMP
      ;
      */


      cursor c_temp IS
        SELECT xbpst.item_number,
             msib.primary_uom_code,
             msib.inventory_item_id,
             msib.description,
             pha.vendor_id,
             pha.vendor_site_id,
             pha.po_header_id,
             nvl(xvpzv.price_zone,0) price_zone,
             to_char(xvpzv.organization_code) organization_code,
             xvpzv.organization_id,
             xvpzv.org_pricing_zone,
             xvpzv.org_type,
             xvpzv.district,
             xvpzv.region,
             xvpzv.organization_name,
             xvpzv.district district_detail,
             xvpzv.region region_detail,
             xxwc_bpa_impact_report_pkg.get_rpm_dpm(xvpzv.region) rvp_dpm,
             --Added CASE WHEN logic on 1/23/15 per TJ Testing If Price Zone Price is null or 0, get National Price
             CASE WHEN nvl(xxwc_bpa_impact_report_pkg.get_current_cost(pha.po_header_id, msib.inventory_item_id, nvl(xvpzv.price_zone,0)),0) = 0
                  THEN nvl(xxwc_bpa_impact_report_pkg.get_current_cost(pha.po_header_id, msib.inventory_item_id, 0),0)
                  ELSE nvl(xxwc_bpa_impact_report_pkg.get_current_cost(pha.po_header_id, msib.inventory_item_id, nvl(xvpzv.price_zone,0)),0)
                  END current_cost,
             --Added CASE WHEN logic on 1/23/15 per TJ Testing If Price Zone Price is null or 0, get National Price
             CASE WHEN nvl(xxwc_bpa_impact_report_pkg.get_updated_cost(xbpst.po_number, xbpst.item_number, nvl(xvpzv.price_zone,0)),0) = 0
                  THEN nvl(xxwc_bpa_impact_report_pkg.get_updated_cost(xbpst.po_number, xbpst.item_number, 0),0)
                  ELSE nvl(xxwc_bpa_impact_report_pkg.get_updated_cost(xbpst.po_number, xbpst.item_number, nvl(xvpzv.price_zone,0)),0)
                  END updated_cost,
             NULL history_units,
             NULL history_cogs,
             NULL forecast_cogs,
             NULL impact_dollars,
             NULL percent_change,
             --Added 12/14/2014
             NULL PURCHASE_HISTORY,
             NULL PURCHASE_COST,
             NULL FORECAST_SPEND,
             NULL PURCHASE_IMPACT,
             NULL PURCHASE_CHANGE
      from   XXWC.XXWC_BPA_PZ_STAGING_TBL xbpst,
             mtl_system_items_b msib,
             po_headers_all pha,
             XXWC_VENDOR_PRICE_ZONE_VW xvpzv
      WHERE  xbpst.item_number = msib.segment1
      and    msib.organization_id = P_MST_ORG_ID
      and    xbpst.po_number = p_po_number
      AND    xbpst.po_number = pha.segment1
      and    pha.org_id = P_ORG_ID
      AND    xvpzv.vendor_id = pha.vendor_id
      and    xvpzv.vendor_site_id = pha.vendor_site_id;

    BEGIN

      g_err_callpoint := 'insert_into_temp';

       l_start_date := xxwc_bpa_impact_report_pkg.get_history_start_date(p_mst_org_id);

       for r_temp in c_temp

        loop
		
--Added for Ver 1.3 Begin

			g_err_callpoint := 'Check if the branch part of the price Zone';

			l_impact_flag := 0;
			
			IF r_temp.price_zone = 0 THEN
			l_impact_check 	:= 	
					 'SELECT count(*) 
						 FROM xxwc.XXWC_BPA_PZ_STAGING_TBL bpa
						WHERE bpa.po_number = '||p_po_number ||
						' AND bpa.org_id ='||p_org_id ||
						' AND bpa.item_number ='''||r_temp.item_number ||
						''' AND  bpa.national_price IS NOT NULL 						  						     
						  AND rownum=1 ' ;				
            ELSE
			l_impact_check 	:= 	
					 'SELECT count(*) 
						 FROM xxwc.XXWC_BPA_PZ_STAGING_TBL bpa
						WHERE bpa.po_number = '||p_po_number ||
						' AND bpa.org_id ='||p_org_id ||
						' AND bpa.item_number ='''||r_temp.item_number ||
						''' AND  bpa.price_zone_price'||r_temp.price_zone||' IS NOT NULL						  						     
						  AND rownum=1 ' ;	
			END IF;
						  			
			
				BEGIN
					
					EXECUTE IMMEDIATE l_impact_check INTO l_impact_flag;	
											
				EXCEPTION WHEN OTHERS THEN										
                  --  g_message := 'Impact check for the branch Failed '||SQLCODE || SQLERRM; -- Ver#1.4
					g_err_msg := 'Impact check for the branch Failed for PO '||p_po_number||'for org '||p_org_id||'for the Item '||r_temp.item_number ||SQLCODE || SQLERRM;  -- Ver#1.4
					raise g_exception;

				END;
			
 
        IF l_impact_flag <> 0 THEN
		
--Added for Ver 1.3 End						  

            XXWC_BPA_IMPACT_REPORT_PKG.get_history
                            (p_inventory_item_id => r_temp.inventory_item_id
                            ,p_organization_id => r_temp.organization_id
                            ,p_dc_mode => p_dc_mode
                            ,p_start_date => l_start_date
                            ,X_HISTORY_UNITS => l_history_units
                            ,X_HISTORY_COGS  => l_history_cogs
                            ,X_PURCHASE_HISTORY => l_history_purchase
                            ,X_PURCHASE_COST => l_history_purchase_cost
                            );

            l_forecast_cogs := nvl(r_temp.updated_cost,0) * nvl(l_history_units,0);
            l_impact_dollars := nvl(l_forecast_cogs,0) - nvl(l_history_cogs,0);

            --Added 12/14/2014
            l_forecast_purchase := nvl(r_temp.updated_cost,0) * nvl(l_history_purchase,0);
            l_impact_purchase := nvl(l_forecast_purchase,0) - nvl(l_history_purchase_cost,0);


                if nvl(l_history_cogs,0) = 0 then

                l_denominator := .01;

              else

                l_denominator := l_history_cogs;

              END IF;

              l_percent_change := (nvl(l_impact_dollars,0) / l_denominator) * 100;

              --Added If 12/14/2014 for Purchase History

              if nvl(l_history_purchase_cost,0) = 0 then

                l_denominator := .01;

              ELSE

                l_denominator := l_history_purchase_cost;

              end if;

            l_purchase_percent_change := (nvl(l_impact_purchase,0) / l_denominator) * 100;

            BEGIN

              INSERT INTO XXWC.XXWC_BPA_IMPACT_REPORT_TEMP
              VALUES (
                 r_temp.item_number,
                 r_temp.primary_uom_code,
                 r_temp.inventory_item_id,
                 r_temp.description,
                 r_temp.vendor_id,
                 r_temp.vendor_site_id,
                 r_temp.po_header_id,
                 r_temp.price_zone,
                 r_temp.organization_code,
                 r_temp.organization_id,
                 r_temp.org_pricing_zone,
                 r_temp.org_type,
                 r_temp.district,
                 r_temp.region,
                 r_temp.organization_name,
                 r_temp.district_detail,
                 r_temp.region_detail,
                 r_temp.rvp_dpm,
                 r_temp.current_cost,
                 r_temp.updated_cost,
                 l_history_units,
                 l_history_cogs,
                 l_forecast_cogs,
                 l_impact_dollars,
                 l_percent_change,
                 --Added 12/14/2014
                 l_history_purchase,
                 l_history_purchase_cost,
                 l_forecast_purchase,
                 l_impact_purchase,
                 l_purchase_percent_change);

          EXCEPTION
              when others then
                    g_message := 'Unable to update XXWC_BPA_IMPACT_REPORT_TEMP ' ||SQLCODE || SQLERRM;
                    raise g_exception;
            END;

            commit;
	  END IF; --Added for Ver 1.3 
      end loop;



    g_return := 0;
    g_message := 'Success';

    EXCEPTION

      WHEN g_exception THEN

        g_return := 1;
        g_message := g_err_msg;

        xxcus_error_pkg.xxcus_error_main_api (
                p_called_from         => g_err_callfrom
               ,p_calling             => g_err_callpoint
               ,p_ora_error_msg       => g_err_msg
               ,p_error_desc          => 'WHEN OTHERS in call ' || g_err_callfrom || g_err_callpoint
               ,p_distribution_list   => g_distro_list
               ,p_module              => g_module);



      WHEN OTHERS THEN

         g_err_msg := 'Error in ' || g_err_callpoint || ' WITH  ' || SUBSTR (SQLERRM, 1, 1000);

          g_return := 1;
          g_message := g_err_msg;

            xxcus_error_pkg.xxcus_error_main_api (
                p_called_from         => g_err_callfrom
               ,p_calling             => g_err_callpoint
               ,p_ora_error_msg       => g_err_msg
               ,p_error_desc          => 'WHEN OTHERS in call ' || g_err_callfrom || g_err_callpoint
               ,p_distribution_list   => g_distro_list
               ,p_module              => g_module);

    END insert_into_temp;

  /*************************************************************************************************
   *   Function  get_rpm_dpm                                                                        *
   *   Purpose : Returns the RPM or DPM based on the region                                         *
   *                                                                                                *
   *                                                                                                *
   *              FUNCTION get_rpm_dpm                                                              *
   *                        (p_region   IN VARCHAR2)                                                *
   *                     VARCHAR2                                                                   *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.0        08/25/2014  Lee Spitzer              Initial Version 20140726-00006               *
   *                                                     New BPA Impact Report - deploy CSV version *
   *                                                                                                *
   /************************************************************************************************/

    FUNCTION  get_rpm_dpm
                           (p_region  IN VARCHAR2)
        RETURN VARCHAR2  IS

      l_region VARCHAR2(240);

    BEGIN

     g_err_callpoint := 'get_rpm_dpm';

      --Get Name from the Region Value Set Attribute4
       BEGIN
        SELECT ffv.attribute4
        INTO   l_region
        from   fnd_flex_value_sets ffvs,
               fnd_flex_values ffv
        where  ffvs.flex_value_set_name = 'XXWC_REGION'
        AND    ffv.flex_value_set_id = ffvs.flex_value_set_id
        AND    ffv.flex_value = p_region;
       EXCEPTION
        WHEN OTHERS THEN
              l_region := '';
      END;

      RETURN l_region;

    EXCEPTION

       WHEN OTHERS THEN

         g_err_msg := 'Error in ' || g_err_callpoint || ' WITH  ' || SUBSTR (SQLERRM, 1, 1000);

            xxcus_error_pkg.xxcus_error_main_api (
                p_called_from         => g_err_callfrom
               ,p_calling             => g_err_callpoint
               ,p_ora_error_msg       => g_err_msg
               ,p_error_desc          => 'WHEN OTHERS in call ' || g_err_callfrom || g_err_callpoint
               ,p_distribution_list   => g_distro_list
               ,p_module              => g_module);

    END get_rpm_dpm;

   /*************************************************************************************************
   *   Function  get_current_cost                                                                   *
   *   Purpose : Returns the current cost                                                           *
   *                                                                                                *
   *                                                                                                *
   *              FUNCTION get_current_cost                                                         *
   *                       (p_po_header_id       IN NUMBER                                          *
   *                       ,p_inventory_item_id  IN NUMBER                                          *
   *                       ,p_price_zone         IN NUMBER)                                         *
   *                     NUMBER                                                                     *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.0        08/25/2014  Lee Spitzer              Initial Version 20140726-00006               *
   *                                                     New BPA Impact Report - deploy CSV version *
   *   1.3        09/21/2015  Manjula Chellappan       TMS# 20150903-00018                          *
   *                                                      BPA- Cost Management issues               *
   /************************************************************************************************/

    FUNCTION get_current_cost
                           (p_po_header_id       IN NUMBER
                           ,p_inventory_item_id  IN NUMBER
                           ,p_price_zone         IN NUMBER)
        RETURN NUMBER IS

    l_price NUMBER;
    l_exists NUMBER;
    l_max_qty NUMBER;

    BEGIN

      g_err_callpoint := 'get_current_cost';

	  --Commented for Rev 1.3 Begin
/*	  
        --Find out if there are price breaks
        BEGIN
          SELECT count(*)
          INTO   l_exists
          FROM   XXWC.XXWC_BPA_PRICE_ZONE_BREAKS_TBL
          WHERE  po_header_id = p_po_header_id
          AND    inventory_item_id = p_inventory_item_id
          AND    price_zone = p_price_zone;
        EXCEPTION
            WHEN others THEN
                 l_exists := 0;
        END;

        --If there are no price breaks, get the zone price
        IF l_exists = 0 THEN

             l_price  :=
                     nvl(xxwc_bpa_price_zone_pkg.get_price_zone_info  (p_po_header_id => p_po_header_id,
                                           p_inventory_item_id => p_inventory_item_id,
                                           p_price_zone  => p_price_zone,
                                           p_attribute   => 'PRICE'),0);

        --If there are price breaks, get max qty break
        ELSE

            BEGIN
                SELECT nvl(MAX(price_zone_quantity),0)
                INTO   l_max_qty
                FROM   xxwc.xxwc_bpa_price_zone_breaks_tbl
                WHERE  po_header_id = p_po_header_id
                AND    inventory_item_id = p_inventory_item_id
                AND    price_zone = p_price_zone;
            EXCEPTION
                WHEN others THEN
                      l_max_qty := 0;
            END;

             IF l_max_qty > 0 THEN
                    --Get the max price break price zone price
                    l_price :=  nvl(xxwc_bpa_price_zone_pkg.get_price_zone_break_info
                                            (p_po_header_id => p_po_header_id,
                                             p_inventory_item_id => p_inventory_item_id,
                                             p_price_zone  => p_price_zone,
                                             p_quantity => l_max_qty,
                                             p_attribute   => 'QUANTITY_PRICE'),0);
              ELSE

                    --Get the price zone price without break
                    l_price  :=   nvl(xxwc_bpa_price_zone_pkg.get_price_zone_info  (p_po_header_id => p_po_header_id,
                                           p_inventory_item_id => p_inventory_item_id,
                                           p_price_zone  => p_price_zone,
                                           p_attribute   => 'PRICE'),0);


              END IF;


      END IF;
*/
--Commented for Rev 1.3 end
--Added for Rev 1.3 Begin

					l_price  :=   nvl(xxwc_bpa_price_zone_pkg.get_price_zone_info  (p_po_header_id => p_po_header_id,
                                           p_inventory_item_id => p_inventory_item_id,
                                           p_price_zone  => p_price_zone,
                                           p_attribute   => 'PRICE'),0);

--Added for Rev 1.3 End										   

      return l_price;


    EXCEPTION

     WHEN OTHERS THEN

        l_price := 0;

         g_err_msg := 'Error in ' || g_err_callpoint || ' WITH  ' || SUBSTR (SQLERRM, 1, 1000);

            xxcus_error_pkg.xxcus_error_main_api (
                p_called_from         => g_err_callfrom
               ,p_calling             => g_err_callpoint
               ,p_ora_error_msg       => g_err_msg
               ,p_error_desc          => 'WHEN OTHERS in call ' || g_err_callfrom || g_err_callpoint
               ,p_distribution_list   => g_distro_list
               ,p_module              => g_module);

          return l_price;

    END get_current_cost;

   /*************************************************************************************************
   *   Function  get_updated_cost                                                                   *
   *   Purpose : Returns the new cost                                                               *
   *                                                                                                *
   *                                                                                                *
   *              FUNCTION get_updated_cost                                                         *
   *                        (p_po_number        IN VARCHAR2                                         *
   *                        ,p_item_number      IN VARCHAR2                                         *
   *                        ,p_price_zone       IN NUMBER)                                          *
   *                RETURN NUMBER;                                                                  *
   *                                                                                                *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.0        08/25/2014  Lee Spitzer              Initial Version 20140726-00006               *
   *                                                     New BPA Impact Report - deploy CSV version *
   *   1.3        09/21/2015  Manjula Chellappan       TMS# 20150903-00018                          *
   *                                                      BPA- Cost Management issues               *             *                                                                                                *
   /************************************************************************************************/

    FUNCTION get_updated_cost
                            (p_po_number        IN VARCHAR2
                            ,p_item_number      IN VARCHAR2
                            ,p_price_zone       IN NUMBER)
        RETURN NUMBER IS

      l_price NUMBER;
      cursor c_staging_price is
        select price_zone,
               price_zone_price,
               price_zone_quantity,
               price_zone_quantity_price
        from  (
            SELECT 0 price_zone,
                   national_price price_zone_price,
                   national_quantity price_zone_quantity,
                   national_break price_zone_quantity_price
            FROM  XXWC.XXWC_BPA_PZ_STAGING_TBL
            where po_number = p_po_number
            and   item_number = p_item_number
            and   p_price_zone = 0
            union
            SELECT 1 price_zone,
                   PRICE_ZONE_PRICE1 price_zone_price,
                   price_zone_quantity1 price_zone_quantity,
                   price_zone_quantity_price1 price_zone_quantity_price
            FROM  XXWC.XXWC_BPA_PZ_STAGING_TBL
            where po_number = p_po_number
            and   item_number = p_item_number
            and   p_price_zone = 1
            UNION
            SELECT 2 price_zone,
                   PRICE_ZONE_PRICE2 price_zone_price,
                   price_zone_quantity2 price_zone_quantity,
                   price_zone_quantity_price2 price_zone_quantity_price
            FROM  XXWC.XXWC_BPA_PZ_STAGING_TBL
            where po_number = p_po_number
            and   item_number = p_item_number
            and   p_price_zone = 2
            UNION
            SELECT 3 price_zone,
                   PRICE_ZONE_PRICE3 price_zone_price,
                   price_zone_quantity3 price_zone_quantity,
                   price_zone_quantity_price3 price_zone_quantity_price
            FROM  XXWC.XXWC_BPA_PZ_STAGING_TBL
            where po_number = p_po_number
            and   item_number = p_item_number
            and   p_price_zone = 3
            UNION
            SELECT 4 price_zone,
                   PRICE_ZONE_PRICE4 price_zone_price,
                   price_zone_quantity4 price_zone_quantity,
                   price_zone_quantity_price4 price_zone_quantity_price
            FROM  XXWC.XXWC_BPA_PZ_STAGING_TBL
            where po_number = p_po_number
            and   item_number = p_item_number
            and   p_price_zone = 4
            UNION
            SELECT 5 price_zone,
                   PRICE_ZONE_PRICE5 price_zone_price,
                   price_zone_quantity5 price_zone_quantity,
                   price_zone_quantity_price5 price_zone_quantity_price
            FROM  XXWC.XXWC_BPA_PZ_STAGING_TBL
            where po_number = p_po_number
            and   item_number = p_item_number
            and   p_price_zone = 5
            UNION
            SELECT 6 price_zone,
                   PRICE_ZONE_PRICE6 price_zone_price,
                   price_zone_quantity6 price_zone_quantity,
                   price_zone_quantity_price6 price_zone_quantity_price
            FROM  XXWC.XXWC_BPA_PZ_STAGING_TBL
            where po_number = p_po_number
            and   item_number = p_item_number
            and   p_price_zone = 6
            UNION
            SELECT 7 price_zone,
                   PRICE_ZONE_PRICE7 price_zone_price,
                   price_zone_quantity7 price_zone_quantity,
                   price_zone_quantity_price7 price_zone_quantity_price
            FROM  XXWC.XXWC_BPA_PZ_STAGING_TBL
            where po_number = p_po_number
            and   item_number = p_item_number
            and   p_price_zone = 7
            UNION
            SELECT 8 price_zone,
                   PRICE_ZONE_PRICE8 price_zone_price,
                   price_zone_quantity8 price_zone_quantity,
                   price_zone_quantity_price8 price_zone_quantity_price
            FROM  XXWC.XXWC_BPA_PZ_STAGING_TBL
            where po_number = p_po_number
            and   item_number = p_item_number
            and   p_price_zone = 8
            UNION
            SELECT 9 price_zone,
                   PRICE_ZONE_PRICE9 price_zone_price,
                   price_zone_quantity9 price_zone_quantity,
                   price_zone_quantity_price9 price_zone_quantity_price
            FROM  XXWC.XXWC_BPA_PZ_STAGING_TBL
            where po_number = p_po_number
            and   item_number = p_item_number
            and   p_price_zone = 9
            UNION
            SELECT 10 price_zone,
                   PRICE_ZONE_PRICE10 price_zone_price,
                   price_zone_quantity10 price_zone_quantity,
                   price_zone_quantity_price10 price_zone_quantity_price
            FROM  XXWC.XXWC_BPA_PZ_STAGING_TBL
            where po_number = p_po_number
            and   item_number = p_item_number
            and   p_price_zone = 10
            UNION
            SELECT 11 price_zone,
                   PRICE_ZONE_PRICE11 price_zone_price,
                   price_zone_quantity11 price_zone_quantity,
                   price_zone_quantity_price11 price_zone_quantity_price
            FROM  XXWC.XXWC_BPA_PZ_STAGING_TBL
            where po_number = p_po_number
            and   item_number = p_item_number
            and   p_price_zone = 11
            UNION
            SELECT 12 price_zone,
                   PRICE_ZONE_PRICE12 price_zone_price,
                   price_zone_quantity12 price_zone_quantity,
                   price_zone_quantity_price12 price_zone_quantity_price
            FROM  XXWC.XXWC_BPA_PZ_STAGING_TBL
            where po_number = p_po_number
            and   item_number = p_item_number
            and   p_price_zone = 12
            UNION
            SELECT 13 price_zone,
                   PRICE_ZONE_PRICE13 price_zone_price,
                   price_zone_quantity13 price_zone_quantity,
                   price_zone_quantity_price13 price_zone_quantity_price
            FROM  XXWC.XXWC_BPA_PZ_STAGING_TBL
            where po_number = p_po_number
            and   item_number = p_item_number
            and   p_price_zone = 13
            UNION
            SELECT 14 price_zone,
                   PRICE_ZONE_PRICE14 price_zone_price,
                   price_zone_quantity14 price_zone_quantity,
                   price_zone_quantity_price14 price_zone_quantity_price
            FROM  XXWC.XXWC_BPA_PZ_STAGING_TBL
            where po_number = p_po_number
            and   item_number = p_item_number
            and   p_price_zone = 14
            UNION
            SELECT 15 price_zone,
                   PRICE_ZONE_PRICE15 price_zone_price,
                   price_zone_quantity15 price_zone_quantity,
                   price_zone_quantity_price15 price_zone_quantity_price
            FROM  XXWC.XXWC_BPA_PZ_STAGING_TBL
            where po_number = p_po_number
            and   item_number = p_item_number
            and   p_price_zone = 15
            UNION
            SELECT 16 price_zone,
                   PRICE_ZONE_PRICE16 price_zone_price,
                   price_zone_quantity16 price_zone_quantity,
                   price_zone_quantity_price16 price_zone_quantity_price
            FROM  XXWC.XXWC_BPA_PZ_STAGING_TBL
            where po_number = p_po_number
            and   item_number = p_item_number
            and   p_price_zone = 16
            UNION
            SELECT 17 price_zone,
                   PRICE_ZONE_PRICE17 price_zone_price,
                   price_zone_quantity17 price_zone_quantity,
                   price_zone_quantity_price17 price_zone_quantity_price
            FROM  XXWC.XXWC_BPA_PZ_STAGING_TBL
            where po_number = p_po_number
            and   item_number = p_item_number
            and   p_price_zone = 17
            UNION
            SELECT 18 price_zone,
                   PRICE_ZONE_PRICE18 price_zone_price,
                   price_zone_quantity18 price_zone_quantity,
                   price_zone_quantity_price18 price_zone_quantity_price
            FROM  XXWC.XXWC_BPA_PZ_STAGING_TBL
            where po_number = p_po_number
            and   item_number = p_item_number
            and   p_price_zone = 18
            UNION
            SELECT 19 price_zone,
                   PRICE_ZONE_PRICE19 price_zone_price,
                   price_zone_quantity19 price_zone_quantity,
                   price_zone_quantity_price19 price_zone_quantity_price
            FROM  XXWC.XXWC_BPA_PZ_STAGING_TBL
            where po_number = p_po_number
            and   item_number = p_item_number
            and   p_price_zone = 19
            UNION
            SELECT 20 price_zone,
                   PRICE_ZONE_PRICE20 price_zone_price,
                   price_zone_quantity20 price_zone_quantity,
                   price_zone_quantity_price20 price_zone_quantity_price
            FROM  XXWC.XXWC_BPA_PZ_STAGING_TBL
            where po_number = p_po_number
            and   item_number = p_item_number
            and   p_price_zone = 20
            UNION
            SELECT 21 price_zone,
                   PRICE_ZONE_PRICE21 price_zone_price,
                   price_zone_quantity21 price_zone_quantity,
                   price_zone_quantity_price21 price_zone_quantity_price
            FROM  XXWC.XXWC_BPA_PZ_STAGING_TBL
            where po_number = p_po_number
            and   item_number = p_item_number
            and   p_price_zone = 21
            UNION
            SELECT 22 price_zone,
                   PRICE_ZONE_PRICE22 price_zone_price,
                   price_zone_quantity22 price_zone_quantity,
                   price_zone_quantity_price22 price_zone_quantity_price
            FROM  XXWC.XXWC_BPA_PZ_STAGING_TBL
            where po_number = p_po_number
            and   item_number = p_item_number
            and   p_price_zone = 22
            UNION
            SELECT 23 price_zone,
                   PRICE_ZONE_PRICE23 price_zone_price,
                   price_zone_quantity23 price_zone_quantity,
                   price_zone_quantity_price23 price_zone_quantity_price
            FROM  XXWC.XXWC_BPA_PZ_STAGING_TBL
            where po_number = p_po_number
            and   item_number = p_item_number
            and   p_price_zone = 23
            UNION
            SELECT 24 price_zone,
                   PRICE_ZONE_PRICE24 price_zone_price,
                   price_zone_quantity24 price_zone_quantity,
                   price_zone_quantity_price24 price_zone_quantity_price
            FROM  XXWC.XXWC_BPA_PZ_STAGING_TBL
            where po_number = p_po_number
            and   item_number = p_item_number
            and   p_price_zone = 24
            UNION
            SELECT 25 price_zone,
                   PRICE_ZONE_PRICE25 price_zone_price,
                   price_zone_quantity25 price_zone_quantity,
                   price_zone_quantity_price25 price_zone_quantity_price
            FROM  XXWC.XXWC_BPA_PZ_STAGING_TBL
            where po_number = p_po_number
            and   item_number = p_item_number
            and   p_price_zone = 25
            UNION
            SELECT 26 price_zone,
                   PRICE_ZONE_PRICE26 price_zone_price,
                   price_zone_quantity26 price_zone_quantity,
                   price_zone_quantity_price26 price_zone_quantity_price
            FROM  XXWC.XXWC_BPA_PZ_STAGING_TBL
            where po_number = p_po_number
            and   item_number = p_item_number
            and   p_price_zone = 26
            UNION
            SELECT 27 price_zone,
                   PRICE_ZONE_PRICE27 price_zone_price,
                   price_zone_quantity27 price_zone_quantity,
                   price_zone_quantity_price27 price_zone_quantity_price
            FROM  XXWC.XXWC_BPA_PZ_STAGING_TBL
            where po_number = p_po_number
            and   item_number = p_item_number
            and   p_price_zone = 27
            UNION
            SELECT 28 price_zone,
                   PRICE_ZONE_PRICE28 price_zone_price,
                   price_zone_quantity28 price_zone_quantity,
                   price_zone_quantity_price28 price_zone_quantity_price
            FROM  XXWC.XXWC_BPA_PZ_STAGING_TBL
            where po_number = p_po_number
            and   item_number = p_item_number
            and   p_price_zone = 28
            UNION
            SELECT 29 price_zone,
                   PRICE_ZONE_PRICE29 price_zone_price,
                   price_zone_quantity29 price_zone_quantity,
                   price_zone_quantity_price29 price_zone_quantity_price
            FROM  XXWC.XXWC_BPA_PZ_STAGING_TBL
            where po_number = p_po_number
            and   item_number = p_item_number
            and   p_price_zone = 29
            UNION
            SELECT 30 price_zone,
                   PRICE_ZONE_PRICE30 price_zone_price,
                   price_zone_quantity30 price_zone_quantity,
                   price_zone_quantity_price30 price_zone_quantity_price
            FROM  XXWC.XXWC_BPA_PZ_STAGING_TBL
            where po_number = p_po_number
            and   item_number = p_item_number
            and   p_price_zone = 30)
        order by price_zone, price_zone_quantity;

    BEGIN

     g_err_callpoint := 'get_updated_cost';

    for r_staging_price in c_staging_price

      loop

--                  l_price := nvl(r_staging_price.price_zone_quantity_price, r_staging_price.price_zone_price); --commented for Rev 1.3 

                    l_price :=  r_staging_price.price_zone_price; --Added for Rev 1.3 

            end loop;
			
--Added for Rev 1.3 Begin

	IF l_price = 0 THEN 
	
		BEGIN
	
	       SELECT  national_price
             INTO  l_price		   
            FROM  XXWC.XXWC_BPA_PZ_STAGING_TBL
           WHERE  po_number = p_po_number
             AND  item_number = p_item_number
			 AND  rownum = 1;
			
		EXCEPTION WHEN OTHERS THEN
		
		l_price :=0;
						
		END;
			

	END IF;
	
--Added for Rev 1.3 End		

     return l_price;

    EXCEPTION

     WHEN OTHERS THEN

      return l_price;

         g_err_msg := 'Error in ' || g_err_callpoint || ' WITH  ' || SUBSTR (SQLERRM, 1, 1000);

            xxcus_error_pkg.xxcus_error_main_api (
                p_called_from         => g_err_callfrom
               ,p_calling             => g_err_callpoint
               ,p_ora_error_msg       => g_err_msg
               ,p_error_desc          => 'WHEN OTHERS in call ' || g_err_callfrom || g_err_callpoint
               ,p_distribution_list   => g_distro_list
               ,p_module              => g_module);

          return l_price;


    END get_updated_cost;

   /*************************************************************************************************
   *   Function: get_history_start_date                                                             *
   *   Purpose : Get history start date                                                             *
   *                                                                                                *
   *                                                                                                *
   *              FUNCTION get_history_start_date                                                   *
   *                        (p_mst_org_id      IN NUMBER)                                           *
   *               RETURN DATE;                                                                     *
   *                                                                                                *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.0        08/25/2014  Lee Spitzer              Initial Version 20140726-00006               *
   *                                                     New BPA Impact Report - deploy CSV version *
   *                                                                                                *
   /************************************************************************************************/

     FUNCTION get_history_start_date
                             (p_mst_org_id      IN NUMBER)
        RETURN DATE IS

      l_date DATE;

    BEGIN

      g_err_callpoint := 'get_history_start_date';

          begin
            Select bpst.period_start_date
            into   l_date
            From BOM_PERIOD_START_DATES bpst,
                 mtl_parameters mp
            Where bpst.calendar_code = mp.calendar_code
            and   mp.organization_id = p_mst_org_id
            AND     SYSDATE-365 between period_start_date and next_date;
          exception
              when others then
                  l_date := sysdate - 365;
          end;

        return l_date;


      EXCEPTION

         WHEN OTHERS THEN

         l_date := sysdate-365;

         g_err_msg := 'Error in ' || g_err_callpoint || ' WITH  ' || SUBSTR (SQLERRM, 1, 1000);

            xxcus_error_pkg.xxcus_error_main_api (
                p_called_from         => g_err_callfrom
               ,p_calling             => g_err_callpoint
               ,p_ora_error_msg       => g_err_msg
               ,p_error_desc          => 'WHEN OTHERS in call ' || g_err_callfrom || g_err_callpoint
               ,p_distribution_list   => g_distro_list
               ,p_module              => g_module);

          return l_date;


    END get_history_start_date;

   /*************************************************************************************************
   *   Procedure get_history                                                                        *
   *   Purpose : Get history data                                                                   *
   *                                                                                                *
   *                                                                                                *
   *              PROCEDURE get_history                                                             *
   *                        (p_inventory_item_id IN NUMBER                                          *
   *                        ,p_organization_id IN NUMBER                                            *
   *                        ,p_dc_mode IN VARCHAR2                                                  *
   *                        ,p_start_date IN DATE                                                   *
   *                        ,X_HISTORY_UNITS OUT NUMBER                                             *
   *                        ,X_HISTORY_COGS  OUT NUMBER);                                           *
   *                                                                                                *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.0        08/25/2014  Lee Spitzer              Initial Version 20140726-00006               *
   *                                                     New BPA Impact Report - deploy CSV version *
   *                                                                                                *
   /************************************************************************************************/

    PROCEDURE get_history
                            (p_inventory_item_id IN NUMBER
                            ,p_organization_id IN NUMBER
                            ,p_dc_mode IN VARCHAR2
                            ,p_start_date IN DATE
                            ,X_HISTORY_UNITS OUT NUMBER
                            ,X_HISTORY_COGS  OUT NUMBER
                            ,X_PURCHASE_HISTORY OUT NUMBER
                            ,X_PURCHASE_COST OUT NUMBER) IS

   --Sourcing from Item Master
    Cursor C_Sourced_Items Is
    select msib.organization_id,
           mp.organization_code
    from   mtl_system_items_b msib,
           mtl_parameters mp
    where  msib.source_organization_id = p_organization_id
    and    msib.source_type = 1 --Internally_Sourced
    and    msib.inventory_item_id = inventory_item_id
    AND    msib.organization_id = mp.organization_id
    AND    msib.source_organization_id != msib.organization_id; --added 2/26/2013 to prevent duplication of self source items


  l_sum_demand                         NUMBER := 0;
  l_sum_cost                           NUMBER := 0;
  l_src_demand                         NUMBER := 0;
    --l_src_orgs                           VARCHAR2(80) := NULL; --removed 4/11/2013 due to pl/sql buffer string too small
  l_src_orgs                           VARCHAR2(10000) := NULL; --added 4/11/2013 to fix pl/sql buffer string

  l_sum_sales_order_issue             NUMBER;
  l_sum_sales_order_cost              NUMBER;
  l_sum_wip_issue                     NUMBER;
  l_sum_wip_cost                      NUMBER;
  l_sum_rma_receipt                   NUMBER;
  l_sum_rma_cost                      NUMBER;

  l_sum_purchase_units                NUMBER;
  l_sum_purchase_costs                NUMBER;
  l_sum_rtv_units                     NUMBER;
  l_sum_rtv_costs                     NUMBER;

Begin

    --Select Nvl(Sum(sales_order_demand),0) --removed 2/26/2013 to include WIP usage
      BEGIN
              SELECT sum(primary_quantity), sum(actual_cost)
              into   l_sum_sales_order_issue, l_sum_sales_order_cost
                FROM (
                  SELECT mp.organization_code, msib.segment1, mmt.transaction_date, (nvl(mmt.primary_quantity,0)) primary_quantity, mta.base_transaction_value actual_cost--(nvl(mmt.actual_cost,0)) * (nvl(mmt.primary_quantity,0))actual_cost
                                FROM   mtl_material_transactions mmt,
                                       mtl_system_items_b msib,
                                       mtl_parameters mp,
                                       --Added 12/15/2014
                                       mtl_transaction_accounts mta
                                WHERE  mmt.organization_id = msib.organization_id
                                AND    mmt.inventory_item_id = msib.inventory_item_id
                                and    mp.organization_id = msib.organization_id
                                AND    mmt.transaction_date >= P_START_DATE
                                AND    mmt.transaction_date < SYSDATE
                                AND    msib.inventory_item_id = p_inventory_item_id
                                and    msib.organization_id = p_organization_id
                                AND    NOT EXISTS (SELECT *
                                                   FROM   oe_drop_ship_sources odss
                                                   WHERE  odss.line_id = mmt.source_line_id)
                                AND    EXISTS (SELECT *
                                              FROM   mtl_transaction_types mtt
                                              WHERE  mmt.transaction_type_id = mtt.transaction_type_id
                                              AND    mtt.transaction_type_name = 'Sales order issue')
                                AND    mmt.transaction_id = mta.transaction_id
                                AND    mta.accounting_line_type = 1
                                AND    mta.cost_element_id = 1
                      UNION
                      SELECT mp2.organization_code, msib.segment1, mmt.transaction_date, (nvl(mmt.primary_quantity,0)), mta.base_transaction_value actual_cost --(nvl(mmt.actual_cost,0)) * (nvl(mmt.primary_quantity,0))
                                FROM   mtl_material_transactions mmt,
                                       mtl_system_items_b msib,
                                       mtl_parameters mp,
                                       mtl_parameters mp2,
                                       --Added 12/15/2014
                                       mtl_transaction_accounts mta
                                WHERE  mmt.organization_id = msib.organization_id
                                AND    mmt.inventory_item_id = msib.inventory_item_id
                                AND    mp.organization_id = msib.source_organization_id
                                and    mp2.organization_id = msib.organization_id
                                AND    mmt.transaction_date >= P_START_DATE
                                AND    mmt.transaction_date < SYSDATE
                                AND    msib.inventory_item_id = p_inventory_item_id
                                and    msib.source_organization_id = p_organization_id
                                AND    msib.source_type = 1 --Internally_Sourced
                                AND    msib.source_organization_id != msib.organization_id --added 2/26/2013 to prevent duplication of self source items
                                AND    NOT EXISTS (SELECT *
                                                   FROM   oe_drop_ship_sources odss
                                                   where  odss.line_id = mmt.source_line_id)
                                AND    EXISTS (SELECT *
                                              FROM   mtl_transaction_types mtt
                                              WHERE  mmt.transaction_type_id = mtt.transaction_type_id
                                              AND    mtt.transaction_type_name = 'Sales order issue')
                                AND    P_DC_MODE = 'DC'
                                AND    mmt.transaction_id = mta.transaction_id
                                AND    mta.accounting_line_type = 1
                                AND    mta.cost_element_id = 1
                                )
                                group by segment1
                ;

      EXCEPTION
            WHEN others THEN
                  l_sum_sales_order_issue := 0;
                  l_sum_sales_order_cost := 0;
      END;

      BEGIN
              SELECT sum(primary_quantity), sum(actual_cost)
              into   l_sum_wip_issue, l_sum_wip_cost
                FROM (
                  SELECT mp.organization_code, msib.segment1, mmt.transaction_date, (nvl(mmt.primary_quantity,0)) primary_quantity, mta.base_transaction_value actual_cost --(nvl(mmt.actual_cost,0)) * (nvl(mmt.primary_quantity,0)) actual_cost
                                FROM   mtl_material_transactions mmt,
                                       mtl_system_items_b msib,
                                       mtl_parameters mp,
                                       --Added 12/15/2014
                                       mtl_transaction_accounts mta
                                WHERE  mmt.organization_id = msib.organization_id
                                AND    mmt.inventory_item_id = msib.inventory_item_id
                                and    mp.organization_id = msib.organization_id
                                AND    mmt.transaction_date >= P_START_DATE
                                AND    mmt.transaction_date < SYSDATE
                                AND    msib.inventory_item_id = p_inventory_item_id
                                AND    msib.organization_id = p_organization_id
                                AND    EXISTS (SELECT *
                                              FROM   mtl_transaction_types mtt
                                              WHERE  mmt.transaction_type_id = mtt.transaction_type_id
                                              AND    mtt.transaction_type_name in ('WIP Issue','WIP Return'))
                                AND    mmt.transaction_id = mta.transaction_id
                                AND    mta.accounting_line_type = 1
                                AND    mta.cost_element_id = 1

                      UNION
                      SELECT mp2.organization_code, msib.segment1, mmt.transaction_date, (nvl(mmt.primary_quantity,0)), mta.base_transaction_value actual_cost --(nvl(mmt.actual_cost,0)) * (nvl(mmt.primary_quantity,0))
                                FROM   mtl_material_transactions mmt,
                                       mtl_system_items_b msib,
                                       mtl_parameters mp,
                                       mtl_parameters mp2,
                                       --Added 12/15/2014
                                       mtl_transaction_accounts mta
                                WHERE  mmt.organization_id = msib.organization_id
                                AND    mmt.inventory_item_id = msib.inventory_item_id
                                AND    mp.organization_id = msib.source_organization_id
                                and    mp2.organization_id = msib.organization_id
                                AND    mmt.transaction_date >= P_START_DATE
                                AND    mmt.transaction_date < SYSDATE
                                AND    msib.inventory_item_id = p_inventory_item_id
                                and    msib.source_organization_id = p_organization_id
                                AND    msib.source_type = 1 --Internally_Sourced
                                AND    msib.source_organization_id != msib.organization_id --added 2/26/2013 to prevent duplication of self source items
                                AND    EXISTS (SELECT *
                                              FROM   mtl_transaction_types mtt
                                              WHERE  mmt.transaction_type_id = mtt.transaction_type_id
                                              AND    mtt.transaction_type_name in ('WIP Issue','WIP Return'))
                                AND    P_DC_MODE = 'DC'
                                AND    mmt.transaction_id = mta.transaction_id
                                AND    mta.accounting_line_type = 1
                                AND    mta.cost_element_id = 1
                                )
                                group by segment1
                ;

      EXCEPTION
            WHEN others THEN
                  l_sum_wip_issue := 0;
                  l_sum_wip_cost := 0;
      END;

  BEGIN
              SELECT sum(primary_quantity), sum(actual_cost)
              INTO   l_sum_rma_receipt, l_sum_rma_cost
                FROM (
                  SELECT mp.organization_code, msib.segment1, mmt.transaction_date, (nvl(mmt.primary_quantity,0)) primary_quantity, mta.base_transaction_value actual_cost --(nvl(mmt.actual_cost,0)) * (nvl(mmt.primary_quantity,0)) actual_cost
                                FROM   mtl_material_transactions mmt,
                                       mtl_system_items_b msib,
                                       mtl_parameters mp,
                                       --Added 12/15/2014
                                       mtl_transaction_accounts mta
                                WHERE  mmt.organization_id = msib.organization_id
                                AND    mmt.inventory_item_id = msib.inventory_item_id
                                and    mp.organization_id = msib.organization_id
                                AND    mmt.transaction_date >= P_START_DATE
                                AND    mmt.transaction_date < SYSDATE
                                AND    msib.inventory_item_id = p_inventory_item_id
                                and    msib.organization_id = p_organization_id
                                AND    EXISTS (SELECT *
                                              FROM   mtl_transaction_types mtt
                                              WHERE  mmt.transaction_type_id = mtt.transaction_type_id
                                              AND    mtt.transaction_type_name = 'RMA Receipt')
                                AND    mmt.transaction_id = mta.transaction_id
                                AND    mta.accounting_line_type = 1
                                AND    mta.cost_element_id = 1
                      UNION
                      SELECT mp2.organization_code, msib.segment1, mmt.transaction_date, (nvl(mmt.primary_quantity,0)), mta.base_transaction_value actual_cost --(nvl(mmt.actual_cost,0)) * (nvl(mmt.primary_quantity,0))
                                FROM   mtl_material_transactions mmt,
                                       mtl_system_items_b msib,
                                       mtl_parameters mp,
                                       mtl_parameters mp2,
                                       --Added 12/15/2014
                                       mtl_transaction_accounts mta
                                WHERE  mmt.organization_id = msib.organization_id
                                AND    mmt.inventory_item_id = msib.inventory_item_id
                                AND    mp.organization_id = msib.source_organization_id
                                and    mp2.organization_id = msib.organization_id
                                AND    mmt.transaction_date >= P_START_DATE
                                AND    mmt.transaction_date < SYSDATE
                                AND    msib.inventory_item_id = p_inventory_item_id
                                and    msib.source_organization_id = p_organization_id
                                AND    msib.source_type = 1 --Internally_Sourced
                                AND    msib.source_organization_id != msib.organization_id --added 2/26/2013 to prevent duplication of self source items
                                AND    EXISTS (SELECT *
                                              FROM   mtl_transaction_types mtt
                                              WHERE  mmt.transaction_type_id = mtt.transaction_type_id
                                              AND    mtt.transaction_type_name = 'RMA Receipt')
                                AND    P_DC_MODE = 'DC'
                                AND    mmt.transaction_id = mta.transaction_id
                                AND    mta.accounting_line_type = 1
                                AND    mta.cost_element_id = 1
                                )
                                group by segment1
                ;

      EXCEPTION
            WHEN others THEN
                  l_sum_rma_receipt := 0;
                  l_sum_rma_cost := 0;
      END;


      --Added 12/14/2014
      BEGIN

      /*
        SELECT sum(quantity), sum(unit_price)
        INTO   l_sum_purchase_units, l_sum_purchase_costs
        FROM  (SELECT nvl(inv_convert.inv_um_convert(pla.item_id,
                                          5,
                                          pla.quantity,
                                          NULL,
                                          NULL,
                                          pla.UNIT_MEAS_LOOKUP_CODE,
                                          msib.PRIMARY_UNIT_OF_MEASURE),0) quantity,
                      nvl(pla.unit_price,0)*nvl(inv_convert.inv_um_convert(pla.item_id,
                                          5,
                                          pla.quantity,
                                          NULL,
                                          NULL,
                                          pla.UNIT_MEAS_LOOKUP_CODE,
                                          msib.PRIMARY_UNIT_OF_MEASURE),0) unit_price
               FROM   po_lines_all pla,
                      po_line_locations_all plla,
                      mtl_system_items_b msib
               WHERE  pla.po_line_id = plla.po_line_id
               AND    pla.creation_date >= P_START_DATE
               AND    pla.creation_date < SYSDATE
               AND    nvl(pla.cancel_flag,'N') = 'N'
               AND    nvl(plla.cancel_flag,'N') = 'N'
               AND    pla.item_id = msib.inventory_item_id
               AND    plla.ship_to_organization_id = msib.organization_id
               AND    msib.inventory_item_id = p_inventory_item_id
               AND    msib.organization_id = p_organization_id
               AND    EXISTS (SELECT *
                              FROM   po_headers_all pha
                              WHERE  pha.po_header_id = pla.po_header_id
                              and    pha.type_lookup_code = 'STANDARD')
               UNION
               SELECT nvl(inv_convert.inv_um_convert(pla.item_id,
                                          5,
                                          pla.quantity,
                                          NULL,
                                          NULL,
                                          pla.UNIT_MEAS_LOOKUP_CODE,
                                          msib.PRIMARY_UNIT_OF_MEASURE),0) quantity,
                      nvl(pla.unit_price,0)*nvl(inv_convert.inv_um_convert(pla.item_id,
                                          5,
                                          pla.quantity,
                                          NULL,
                                          NULL,
                                          pla.UNIT_MEAS_LOOKUP_CODE,
                                          msib.PRIMARY_UNIT_OF_MEASURE),0) unit_price
               FROM   po_lines_all pla,
                      po_line_locations_all plla,
                      mtl_system_items_b msib
               WHERE  pla.po_line_id = plla.po_line_id
               AND    pla.creation_date >= P_START_DATE
               AND    pla.creation_date < SYSDATE
               AND    nvl(pla.cancel_flag,'N') = 'N'
               AND    nvl(plla.cancel_flag,'N') = 'N'
               AND    pla.item_id = msib.inventory_item_id
               AND    plla.ship_to_organization_id = msib.organization_id
               AND    msib.inventory_item_id = p_inventory_item_id
               AND    msib.organization_id = p_organization_id
               AND    msib.source_type = 1 --Internally_Sourced
               AND    msib.source_organization_id != msib.organization_id --added 2/26/2013 to prevent duplication of self source items
               AND    P_DC_MODE = 'DC'
               AND    EXISTS (SELECT *
                              FROM   po_headers_all pha
                              WHERE  pha.po_header_id = pla.po_header_id
                              and    pha.type_lookup_code = 'STANDARD')
              );


        */


        SELECT sum(primary_quantity), sum(actual_cost)
              INTO   l_sum_purchase_units, l_sum_purchase_costs
                FROM (
                  SELECT mp.organization_code, msib.segment1, mmt.transaction_date, (nvl(mmt.primary_quantity,0)) primary_quantity, mta.base_transaction_value actual_cost --(nvl(mmt.actual_cost,0)) * (nvl(mmt.primary_quantity,0)) actual_cost
                                FROM   mtl_material_transactions mmt,
                                       mtl_system_items_b msib,
                                       mtl_parameters mp,
                                       --Added 12/15/2014
                                       mtl_transaction_accounts mta
                                WHERE  mmt.organization_id = msib.organization_id
                                AND    mmt.inventory_item_id = msib.inventory_item_id
                                and    mp.organization_id = msib.organization_id
                                AND    mmt.transaction_date >= P_START_DATE
                                AND    mmt.transaction_date < SYSDATE
                                AND    msib.inventory_item_id = p_inventory_item_id
                                and    msib.organization_id = p_organization_id
                                AND    EXISTS (SELECT *
                                              FROM   mtl_transaction_types mtt
                                              WHERE  mmt.transaction_type_id = mtt.transaction_type_id
                                              AND    mtt.transaction_type_name IN ('PO Receipt','PO Rcpt Adjust','Return to Vendor','WC Return to Vendor'))
                                AND    mmt.transaction_id = mta.transaction_id
                                AND    mta.accounting_line_type = 1
                                AND    mta.cost_element_id = 1
                      UNION
                      SELECT mp2.organization_code, msib.segment1, mmt.transaction_date, (nvl(mmt.primary_quantity,0)), mta.base_transaction_value actual_cost --(nvl(mmt.actual_cost,0)) * (nvl(mmt.primary_quantity,0))
                                FROM   mtl_material_transactions mmt,
                                       mtl_system_items_b msib,
                                       mtl_parameters mp,
                                       mtl_parameters mp2,
                                       --Added 12/15/2014
                                       mtl_transaction_accounts mta
                                WHERE  mmt.organization_id = msib.organization_id
                                AND    mmt.inventory_item_id = msib.inventory_item_id
                                AND    mp.organization_id = msib.source_organization_id
                                and    mp2.organization_id = msib.organization_id
                                AND    mmt.transaction_date >= P_START_DATE
                                AND    mmt.transaction_date < SYSDATE
                                AND    msib.inventory_item_id = p_inventory_item_id
                                and    msib.source_organization_id = p_organization_id
                                AND    msib.source_type = 1 --Internally_Sourced
                                AND    msib.source_organization_id != msib.organization_id --added 2/26/2013 to prevent duplication of self source items
                                AND    EXISTS (SELECT *
                                              FROM   mtl_transaction_types mtt
                                              WHERE  mmt.transaction_type_id = mtt.transaction_type_id
                                              AND    mtt.transaction_type_name IN ('PO Receipt','PO Rcpt Adjust','Return to Vendor','WC Return to Vendor'))
                                AND    P_DC_MODE = 'DC'
                                AND    mmt.transaction_id = mta.transaction_id
                                AND    mta.accounting_line_type = 1
                                AND    mta.cost_element_id = 1
                                )
                                GROUP BY segment1
                ;
      EXCEPTION
        WHEN others THEN
          l_sum_purchase_units := 0;
          l_sum_purchase_costs := 0;
      END;


    X_HISTORY_UNITS := abs(nvl(l_sum_sales_order_issue,0) + nvl(l_sum_wip_issue,0) + nvl(l_sum_rma_receipt,0));
    X_HISTORY_COGS := abs(nvl(l_sum_sales_order_cost,0) + nvl(l_sum_wip_cost,0) + nvl(l_sum_rma_cost,0));

    X_PURCHASE_HISTORY := nvl(l_sum_purchase_units,0);
    X_PURCHASE_COST := nvl(l_sum_purchase_costs,0);

    END get_history;

/****************************************************************************************************
   *   Procedure generate_csv_new                                                                   *
   *   Purpose : Procudure execute the New CSV version of the impact report                         *
   *                                                                                                *
   *                                                                                                *
   * PROCEDURE generate_csv_new (P_PO_NUMBER          IN  VARCHAR2   --PO Number                    *
   *                            ,P_DC_MODE            IN  VARCHAR2   --DC or Branch                 *
   *                            ,P_NOTES              IN  VARCHAR2                                  *
   *                            ,P_MST_ORG_ID         IN  NUMNBER                                   *
   *                            ,P_ORG_ID             IN  NUMBER                                    *
   *                                                                                                *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.2       08/05/2015  P.Vamshidhar             TMS#20150805-00077                            *
   *                                                  Copied code from text report and made changes *
   *                                                                                                *
   *                                                                                                *
   /************************************************************************************************/

   PROCEDURE generate_csv_new (P_PO_NUMBER    IN VARCHAR2,
                               P_DC_MODE      IN VARCHAR2,
                               P_NOTES        IN VARCHAR2,
                               P_MST_ORG_ID   IN NUMBER,
                               P_ORG_ID       IN NUMBER)
   IS
      l_start_date                 DATE;

   BEGIN
      g_err_callpoint := 'generate_csv_new';

      --Parameters
      fnd_file.put_line (fnd_file.LOG, 'P_PO_NUMBER  ' || p_po_number);
      fnd_file.put_line (fnd_file.LOG, 'P_DC_MODE   ' || p_dc_mode);
      fnd_file.put_line (fnd_file.LOG, 'P_NOTES ' || p_notes);
      fnd_file.put_line (fnd_file.LOG, 'P_MST_ORG_ID ' || p_mst_org_id);
      fnd_file.put_line (fnd_file.LOG, 'P_ORG_ID ' || p_org_id);

      ---Truncate Records in Temp Table
      BEGIN
         EXECUTE IMMEDIATE 'TRUNCATE TABLE XXWC.XXWC_BPA_IMPACT_REPORT_TEMP';
      EXCEPTION
         WHEN OTHERS
         THEN
            g_err_msg :=
                  'Error truncating XXWC_BPA_IMPACT_REPORT_TEMP '
               || SQLCODE
               || SQLERRM;
            RAISE g_exception;
      END;

      l_start_date :=
         xxwc_bpa_impact_report_pkg.get_history_start_date (p_mst_org_id);

      fnd_file.put_line (
         fnd_file.LOG,
            'Start Time Insert Into Temp '
         || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));

      --Insert Records into Temp Table
      insert_into_temp (p_po_number    => p_po_number,
                        p_mst_org_id   => p_mst_org_id,
                        p_org_id       => p_org_id,
                        p_dc_mode      => p_dc_mode,
                        x_return       => g_return,
                        x_message      => g_message);

      --IF there was an error inserting records into temp table, raise g_exception
      IF g_return > 0
      THEN
         g_err_msg := g_message;
         RAISE g_exception;
      END IF;

   EXCEPTION
      WHEN G_EXCEPTION
      THEN
         fnd_file.put_line (fnd_file.LOG, g_err_msg);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom,
            p_calling             => g_err_callpoint,
            p_ora_error_msg       => g_err_msg,
            p_error_desc          =>    'WHEN G_EXCEPTION in call '
                                     || g_err_callfrom
                                     || g_err_callpoint,
            p_distribution_list   => g_distro_list,
            p_module              => g_module);
      WHEN OTHERS
      THEN
         g_err_msg :=
               'Error in '
            || g_err_callpoint
            || ' WITH  '
            || SUBSTR (SQLERRM, 1, 1000);

         --         retcode := 1;
         --         errbuf := g_err_msg;

         fnd_file.put_line (fnd_file.LOG, g_err_msg);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom,
            p_calling             => g_err_callpoint,
            p_ora_error_msg       => g_err_msg,
            p_error_desc          =>    'WHEN OTHERS in call '
                                     || g_err_callfrom
                                     || g_err_callpoint,
            p_distribution_list   => g_distro_list,
            p_module              => g_module);
   END;

  END XXWC_BPA_IMPACT_REPORT_PKG;
/