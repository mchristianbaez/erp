/***********************************************************************************************************************************************
   NAME:     TMS_20180409-00187_Datafix.sql
   PURPOSE:  Data fix

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ----------------------------------------------------------------------------------------------------
   1.0        04/10/2018  Rakesh Patel     TMS#20180409-00187-Print log table data
************************************************************************************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000

BEGIN
   DBMS_OUTPUT.put_line ('Before Insert');

   INSERT INTO XXWC.XXWC_PRINT_LOG_TBL SELECT * FROM XXWC.XXWC_PRINT_LOG_MISS_DATA_TBL;

   DBMS_OUTPUT.put_line ('Records Inserted -' || SQL%ROWCOUNT);

   COMMIT;

   EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Unable to insert record ' || SQLERRM);
	  ROLLBACK;
END;
/