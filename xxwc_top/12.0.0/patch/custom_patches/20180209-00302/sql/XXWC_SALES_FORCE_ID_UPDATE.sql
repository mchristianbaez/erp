/*************************************************************************************************************************
  $Header XXWC_SALES_FORCE_ID_UPDATE
  File Name: XXWC_SALES_FORCE_ID_UPDATE.sql
  PURPOSE:   
  REVISIONS:
     Ver        Date           Author         Description
     ---------  -----------  ------------ -----------------------------------------------------------------------------------
     1.0        22-Feb-2018  P.Vamshidhar  TMS# 20180209-00302 - Customer - Data Script for Sales Force ID
****************************************************************************************************************************/
 
SET SERVEROUTPUT ON

CREATE TABLE XXWC.XXWC_HZ_CUST_ACCOUNTS_160318 AS (SELECT * FROM HZ_CUST_ACCOUNTS)
/ 
CREATE TABLE XXWC.XXWC_HZ_CUST_SITES_160318 AS (SELECT * FROM HZ_CUST_ACCT_SITES_ALL)
/ 
CREATE TABLE XXWC.XXWC_EQUIFAX_SFORCE_TBL
(
  ACCOUNT_ID  VARCHAR2(100 BYTE),
  SFORCE_ID   VARCHAR2(100 BYTE),
  ACCT_NUM    VARCHAR2(100 BYTE),
  MSG         VARCHAR2(4000 BYTE)
)
/
GRANT INSERT, DELETE,SELECT ON XXWC.XXWC_EQUIFAX_SFORCE_TBL TO VP038429
/ 
CREATE TABLE XXWC.XXWC_SFORCE_UPDATE_TMP AS 
      SELECT HCA.CUST_ACCOUNT_ID,
             HCA.ACCOUNT_NUMBER,
             HCA.ATTRIBUTE6 ACCT_ATTRIBUTE6,
             SITE.ATTRIBUTE17 SITE_ATTRIBUTE17,
             SITE.SITE_ROW_ID
        FROM APPS.HZ_CUST_ACCOUNTS HCA,
             (SELECT HCSA.CUST_ACCOUNT_ID,
                     HCSA.ATTRIBUTE17,
                     HCSA.ROWID SITE_ROW_ID
                FROM APPS.HZ_CUST_ACCT_SITES_ALL HCSA,
                     APPS.HZ_CUST_SITE_USES_ALL HSUA
               WHERE     1 = 1
                     AND HCSA.CUST_ACCT_SITE_ID = HSUA.CUST_ACCT_SITE_ID
                     AND HSUA.SITE_USE_CODE = 'BILL_TO'
                     AND HSUA.PRIMARY_FLAG = 'Y') SITE
       WHERE     HCA.CUST_ACCOUNT_ID = SITE.CUST_ACCOUNT_ID
             AND LENGTH (HCA.attribute6) <> 15
             AND HCA.attribute6 NOT LIKE '001%'
             AND HCA.ATTRIBUTE6 <> SITE.ATTRIBUTE17
/
DECLARE
   CURSOR CUR_ACCT
   IS
      SELECT * FROM XXWC.XXWC_SFORCE_UPDATE_TMP;

   ln_rec_count     NUMBER := 0;
   lvc_row_locked   VARCHAR2 (1);
BEGIN
   FOR REC_ACC IN CUR_ACCT
   LOOP
      ln_rec_count := ln_rec_count + 1;

      lvc_row_locked := NULL;
      lvc_row_locked :=
         apps.xxwc_om_force_ship_pkg.is_row_locked (REC_ACC.SITE_ROW_ID,
                                                    'HZ_CUST_ACCT_SITES_ALL');

      IF lvc_row_locked <> 'Y'
      THEN
         UPDATE HZ_CUST_ACCT_SITES_ALL
            SET ATTRIBUTE17 = REC_ACC.ACCT_ATTRIBUTE6
          WHERE ROWID = REC_ACC.SITE_ROW_ID;

         IF ln_rec_count > 500
         THEN
            COMMIT;
            ln_rec_count := 0;
         END IF;
      ELSE
         DBMS_OUTPUT.PUT_LINE (
               REC_ACC.ACCOUNT_NUMBER
            || ' Account record locked, please handle manually');
      END IF;
   END LOOP;

   COMMIT;

EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.PUT_LINE ('Error Occured ' || SQLERRM);
END;
/