/*************************************************************************
  $Header TMS_20160606-00044_DELETE_DELIVERY_ID.sql $
  Module Name: TMS_20160606-00044   Data Fix script for I677948 

  PURPOSE: Data Fix script for I677948

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        06-JUN-2016  Raghav Velichetti         TMS#20160606-00044  

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20160606-00044     , Before Update');

DELETE from XXWC.XXWC_WSH_SHIPPING_STG 
where 1=1
and header_id = 42957340
and line_id=70286625 
and delivery_id=4993544;

   DBMS_OUTPUT.put_line (
         'TMS: 20160606-00044   Sales order lines updated (Expected:1): '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20160606-00044     , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20160606-00044  , Errors : ' || SQLERRM);
END;
/