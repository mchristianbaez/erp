 /*
   Ticket#                            Date         Author            Notes
   -------------------------------------------------------------------------------------------------------------------------------------------------------------------
   TMS:20170524-00141 / ESMS 561767   05/28/2017   Balaguru Seshadri Concur accrual process related routines  
 */
CREATE TABLE XXCUS.XXCUS_CONCUR_ACCRUALS_STAGING
(
  EMP_ID                   VARCHAR2(48 BYTE),
  EMP_FULL_NAME            VARCHAR2(80 BYTE),
  RPT_ENTRY_PMT_CODE_NAME  VARCHAR2(80 BYTE),
  PROGRAM_NAME             VARCHAR2(60 BYTE),
  APPROVAL_STATUS          VARCHAR2(60 BYTE),
  RPT_ID                   VARCHAR2(32 BYTE)    DEFAULT 0,
  RPT_NAME                 VARCHAR2(80 BYTE),
  DATE_FIRST_SUBMITTED     DATE                 DEFAULT NULL,
  RPT_ENTRY_VENDOR_NAME    VARCHAR2(80 BYTE),
  POSTED_DATE              DATE                 DEFAULT NULL,
  TRANS_DATE               DATE                 DEFAULT NULL,
  COUNTRY                  VARCHAR2(30 BYTE),
  REIMBURSEMENT_CURRENCY   VARCHAR2(3 BYTE),
  POSTED_AMOUNT            NUMBER,
  ACCOUNT_CODE1            VARCHAR2(30 BYTE),
  ACCOUNT_CODE2            VARCHAR2(30 BYTE),
  RPT_ORG_UNIT_1           VARCHAR2(48 BYTE),
  RPT_ORG_UNIT_2           VARCHAR2(48 BYTE),
  RPT_ORG_UNIT_3           VARCHAR2(48 BYTE),
  RPT_ORG_UNIT_4           VARCHAR2(48 BYTE),
  RPT_ORG_UNIT_6           VARCHAR2(48 BYTE),
  PERSONAL_FLAG            VARCHAR2(1 BYTE),
  RPT_ENTRY_PMT_TYPE_CODE  VARCHAR2(4 BYTE),
  RPT_CUSTOM1              VARCHAR2(48 BYTE),
  RPT_CUSTOM2              VARCHAR2(48 BYTE),
  RPT_CUSTOM3              VARCHAR2(48 BYTE),
  RPT_CUSTOM4              VARCHAR2(48 BYTE),
  RPT_CUSTOM6              VARCHAR2(48 BYTE),
  EMP_ORG_UNIT1            VARCHAR2(48 BYTE),
  EMP_ORG_UNIT2            VARCHAR2(48 BYTE),
  EMP_ORG_UNIT3            VARCHAR2(48 BYTE),
  EMP_ORG_UNIT4            VARCHAR2(48 BYTE),
  EMP_ORG_UNIT6            VARCHAR2(48 BYTE),
  EMP_CUSTOM1              VARCHAR2(48 BYTE),
  EMP_CUSTOM2              VARCHAR2(48 BYTE),
  EMP_CUSTOM3              VARCHAR2(48 BYTE),
  EMP_CUSTOM4              VARCHAR2(48 BYTE),
  EMP_CUSTOM6              VARCHAR2(48 BYTE),
  ACCRUAL_PERIOD           VARCHAR2(10 BYTE),
  ACCRUAL_FISCAL_START     DATE,
  ACCRUAL_FISCAL_END       DATE,
  REVERSAL_ACCRUAL_PERIOD  VARCHAR2(10 BYTE),
  REQUEST_ID               NUMBER,
  RUN_ID                   NUMBER,
  ACCRUAL_STATUS           VARCHAR2(60 BYTE)    DEFAULT 'NEW',
  LEDGER_ID                NUMBER               DEFAULT 0,
  ACCRUAL_ERP              VARCHAR2(48 BYTE)    DEFAULT 'NONE',
  SEGMENT1                 VARCHAR2(30 BYTE)    DEFAULT 'NONE',
  SEGMENT2                 VARCHAR2(30 BYTE)    DEFAULT 'NONE',
  SEGMENT3                 VARCHAR2(30 BYTE)    DEFAULT 'NONE',
  SEGMENT4                 VARCHAR2(30 BYTE)    DEFAULT 'NONE',
  SEGMENT5                 VARCHAR2(30 BYTE)    DEFAULT 'NONE',
  VALIDATION_FAILED        VARCHAR2(1 BYTE)     DEFAULT 'N',
  ACCRUAL_PERIOD_NUM       NUMBER               DEFAULT 0,
  OOP_CARD                 VARCHAR2(30 BYTE),
  RPT_KEY                  NUMBER               DEFAULT 0,
  EXPENSE_TYPE             VARCHAR2(64 BYTE),
  EXPENSE_CODE             VARCHAR2(64 BYTE)
)
;
--
COMMENT ON TABLE XXCUS.XXCUS_CONCUR_ACCRUALS_STAGING IS 'TMS:20170524-00141 / ESMS 561767';
--
GRANT DELETE, INSERT, SELECT ON XXCUS.XXCUS_CONCUR_ACCRUALS_STAGING TO INTERFACE_DSTAGE, BS006141, SA057257, ID020048;
--
