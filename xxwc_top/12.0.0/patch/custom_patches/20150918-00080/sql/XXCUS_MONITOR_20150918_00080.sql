/*
 TMS: 20150918-00080
 Date: 09/22/2015
*/
set serveroutput on size 1000000;
declare
 --
 l_program_1 varchar2(150) :='XXWC PO Update PO line Item Stock Status and System delivered price';
 l_program_2 varchar2(150) :='XXWC PO Update Suggested Buyer'; 
 l_conc_program_id number :=0;
 l_app_id number :=0;
 l_sc_userid number :=15985; --Supply chain generic user XXWC_INT_SUPPLYCHAIN
 l_om_userid number :=15986; --Order managenent generic user XXWC_INT_SALESFULFILLMENT
 l_fin_userid number :=1290; --WC Finance generic user XXWC_INT_FINANCE
 --
     procedure print_log(p_message in varchar2) is
     begin
      if apps.fnd_global.conc_request_id >0 then
       apps.fnd_file.put_line(fnd_file.log, p_message);
      else
       dbms_output.put_line(p_message);
      end if;
     end;
 --
begin
     --
     print_log('');     
     print_log(' Begin setup of jobs for 24 * 7 monitoring...');
     print_log('');
     --         
     begin 
      select a.concurrent_program_id, a.application_id
      into    l_conc_program_id, l_app_id
      from   apps.fnd_concurrent_programs_tl a
                  ,xxcus.xxcus_monitor_cp_b               b
      where  1 =1
           and  a.user_concurrent_program_name =l_program_1
           and  a.concurrent_program_id =b.concurrent_program_id
           and  a.application_id =b.application_id;
            --
     exception
      when no_data_found then
                --
                 begin 
                  select a.concurrent_program_id, a.application_id
                  into    l_conc_program_id, l_app_id
                  from   apps.fnd_concurrent_programs_tl a
                  where  1 =1
                       and  a.user_concurrent_program_name =l_program_1;
                        --
                         begin 
                          savepoint square2;
                            INSERT INTO XXCUS.XXCUS_MONITOR_CP_B 
                             (
                                  concurrent_program_id,
                                  email_when_warning,
                                  email_when_error,
                                  user_id,
                                  created_by,
                                  creation_date,
                                  last_updated_by,
                                  last_update_date,
                                  warning_email_address,
                                  error_email_address,
                                  owned_by,
                                  application_id
                               )
                                 VALUES 
                               (
                                 l_conc_program_id,
                                 'Y',
                                 'Y',
                                 l_sc_userid,
                                 15837,
                                 SYSDATE,
                                 15837,
                                 SYSDATE,
                                 'hdswcsupplychainalerts@hdsupply.com',
                                 'hdswcsupplychainalerts@hdsupply.com',
                                 'SUPPLY CHAIN',
                                 l_app_id
                              );       
                                --
                                commit;
                                --
                                print_log('@ insert of l_program_1 complete.');
                                --                                                 
                         exception
                          when others then
                           print_log('@ insert of l_program_1 when-others, error ='||sqlerrm);                  
                           rollback to square2;
                         end;                        
                        --
                 exception
                  when no_data_found then
                   null;        
                  when others then
                   print_log('@ l_program_1 when-others-2, error ='||sqlerrm);       
                 end;
                --
      when too_many_rows then
       print_log('@ program = '||l_program_1);
       print_log('@ l_program_1 when-too many rows, error ='||sqlerrm);          
      when others then
       print_log('@ program = '||l_program_1);
       print_log('@ l_program_1 when-others-1, error ='||sqlerrm);       
     end;
    --
    l_conc_program_id :=0;
    l_app_id :=0;
    --
     begin 
      select a.concurrent_program_id, a.application_id
      into    l_conc_program_id, l_app_id
      from   apps.fnd_concurrent_programs_tl a
                  ,xxcus.xxcus_monitor_cp_b               b
      where  1 =1
           and  a.user_concurrent_program_name =l_program_2
           and  a.concurrent_program_id =b.concurrent_program_id
           and  a.application_id =b.application_id;
            --
     exception
      when no_data_found then
                --
                 begin 
                  select a.concurrent_program_id, a.application_id
                  into    l_conc_program_id, l_app_id
                  from   apps.fnd_concurrent_programs_tl a
                  where  1 =1
                       and  a.user_concurrent_program_name =l_program_2;
                        --
                         begin 
                          savepoint square2;
                            INSERT INTO XXCUS.XXCUS_MONITOR_CP_B 
                             (
                                  concurrent_program_id,
                                  email_when_warning,
                                  email_when_error,
                                  user_id,
                                  created_by,
                                  creation_date,
                                  last_updated_by,
                                  last_update_date,
                                  warning_email_address,
                                  error_email_address,
                                  owned_by,
                                  application_id
                               )
                                 VALUES 
                               (
                                 l_conc_program_id,
                                 'Y',
                                 'Y',
                                 l_sc_userid,
                                 15837,
                                 SYSDATE,
                                 15837,
                                 SYSDATE,
                                 'hdswcsupplychainalerts@hdsupply.com',
                                 'hdswcsupplychainalerts@hdsupply.com',
                                 'SUPPLY CHAIN',
                                 l_app_id
                              );       
                                --
                                commit;
                                --
                                print_log('@ insert of l_program_2 complete.');
                                --                                                 
                         exception
                          when others then
                           print_log('@ insert of l_program_2 when-others, error ='||sqlerrm);                  
                           rollback to square2;
                         end;                        
                        --
                 exception
                  when no_data_found then
                   null;        
                  when others then
                   print_log('@ l_program_2 when-others-2, error ='||sqlerrm);       
                 end;
                --
      when too_many_rows then
       print_log('@ program = '||l_program_1);
       print_log('@ l_program_2 when-too many rows, error ='||sqlerrm);          
      when others then
       print_log('@ program = '||l_program_1);
       print_log('@ l_program_2 when-others-1, error ='||sqlerrm);       
     end;
    --    
     print_log('');     
     print_log(' End setup of jobs for 24 * 7 monitoring...');
     print_log('');
     --    
exception
 when others then
  rollback to square1;
  print_log('Outer block, message ='||sqlerrm);
end;
/