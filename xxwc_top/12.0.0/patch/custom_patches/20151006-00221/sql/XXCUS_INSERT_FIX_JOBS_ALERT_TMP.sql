/*
 TMS: 20151001-00191
 Date: 10/06/2015
*/
SET DEFINE OFF;
Insert into XXCUS.XXCUS_FIX_JOBS_ALERT_TMP
   (JOB_NAME, JOB_TYPE, REMOVE_USER, ERROR_EMAIL, WARNING_EMAIL, 
    STATUS)
 Values
   ('Accounting Program', 'SINGLE_REQUEST', 'Y', 'hds.oracleglsupport@hdsupply.com', 'hds.oracleglsupport@hdsupply.com', 
    'NEW');
Insert into XXCUS.XXCUS_FIX_JOBS_ALERT_TMP
   (JOB_NAME, JOB_TYPE, REMOVE_USER, ERROR_EMAIL, WARNING_EMAIL, 
    STATUS)
 Values
   ('Autoinvoice Master Program', 'SINGLE_REQUEST', 'N', 'wcitfinance@hdsupply.com', 'wcitfinance@hdsupply.com', 
    'NEW');
Insert into XXCUS.XXCUS_FIX_JOBS_ALERT_TMP
   (JOB_NAME, JOB_TYPE, REMOVE_USER, ERROR_EMAIL, WARNING_EMAIL, 
    STATUS)
 Values
   ('Automatic Remittances Master Program', 'SINGLE_REQUEST', 'N', 'wcitfinance@hdsupply.com', 'wcitfinance@hdsupply.com', 
    'NEW');
Insert into XXCUS.XXCUS_FIX_JOBS_ALERT_TMP
   (JOB_NAME, JOB_TYPE, REMOVE_USER, ERROR_EMAIL, WARNING_EMAIL, 
    STATUS)
 Values
   ('Collect Revenue Recognition Information', 'SINGLE_REQUEST', 'N', 'wcitfinance@hdsupply.com', 'wcitfinance@hdsupply.com', 
    'NEW');
Insert into XXCUS.XXCUS_FIX_JOBS_ALERT_TMP
   (JOB_NAME, JOB_TYPE, REMOVE_USER, ERROR_EMAIL, WARNING_EMAIL, 
    STATUS)
 Values
   ('Create Accounting', 'SINGLE_REQUEST', 'Y', 'hds.oracleglsupport@hdsupply.com', 'hds.oracleglsupport@hdsupply.com', 
    'NEW');
Insert into XXCUS.XXCUS_FIX_JOBS_ALERT_TMP
   (JOB_NAME, JOB_TYPE, REMOVE_USER, ERROR_EMAIL, WARNING_EMAIL, 
    STATUS)
 Values
   ('Create Accounting - Cost Management', 'SINGLE_REQUEST', 'Y', 'hds.oracleglsupport@hdsupply.com', 'hds.oracleglsupport@hdsupply.com', 
    'NEW');
Insert into XXCUS.XXCUS_FIX_JOBS_ALERT_TMP
   (JOB_NAME, JOB_TYPE, REMOVE_USER, ERROR_EMAIL, WARNING_EMAIL, 
    STATUS)
 Values
   ('Create Settlement Batches', 'SINGLE_REQUEST', 'N', 'wcitfinance@hdsupply.com', 'wcitfinance@hdsupply.com', 
    'NEW');
Insert into XXCUS.XXCUS_FIX_JOBS_ALERT_TMP
   (JOB_NAME, JOB_TYPE, REMOVE_USER, ERROR_EMAIL, WARNING_EMAIL, 
    STATUS)
 Values
   ('Customer text data creation and indexing', 'SINGLE_REQUEST', 'N', 'wcitfinance@hdsupply.com', 'wcitfinance@hdsupply.com', 
    'NEW');
Insert into XXCUS.XXCUS_FIX_JOBS_ALERT_TMP
   (JOB_NAME, JOB_TYPE, REMOVE_USER, ERROR_EMAIL, WARNING_EMAIL, 
    STATUS)
 Values
   ('Fetch Settlement Batch Clearing', 'SINGLE_REQUEST', 'N', 'wcitfinance@hdsupply.com', 'wcitfinance@hdsupply.com', 
    'NEW');
Insert into XXCUS.XXCUS_FIX_JOBS_ALERT_TMP
   (JOB_NAME, JOB_TYPE, REMOVE_USER, ERROR_EMAIL, WARNING_EMAIL, 
    STATUS)
 Values
   ('Gather Schema Statistics', 'SINGLE_REQUEST', 'Y', 'hds.oracleglsupport@hdsupply.com', 'hds.oracleglsupport@hdsupply.com', 
    'NEW');
Insert into XXCUS.XXCUS_FIX_JOBS_ALERT_TMP
   (JOB_NAME, JOB_TYPE, REMOVE_USER, ERROR_EMAIL, WARNING_EMAIL, 
    STATUS)
 Values
   ('Generate COGS Recognition Events', 'SINGLE_REQUEST', 'N', 'wcitfinance@hdsupply.com', 'wcitfinance@hdsupply.com', 
    'NEW');
Insert into XXCUS.XXCUS_FIX_JOBS_ALERT_TMP
   (JOB_NAME, JOB_TYPE, REMOVE_USER, ERROR_EMAIL, WARNING_EMAIL, 
    STATUS)
 Values
   ('Geography Name Referencing Program', 'SINGLE_REQUEST', 'N', 'wcitfinance@hdsupply.com', 'wcitfinance@hdsupply.com', 
    'NEW');
Insert into XXCUS.XXCUS_FIX_JOBS_ALERT_TMP
   (JOB_NAME, JOB_TYPE, REMOVE_USER, ERROR_EMAIL, WARNING_EMAIL, 
    STATUS)
 Values
   ('HDS APXCMMN OPN Refresh', 'SINGLE_REQUEST', 'N', 'hdsoraclerealestate@hdsupply.com', 'hdsoraclerealestate@hdsupply.com', 
    'NEW');
Insert into XXCUS.XXCUS_FIX_JOBS_ALERT_TMP
   (JOB_NAME, JOB_TYPE, REMOVE_USER, ERROR_EMAIL, WARNING_EMAIL, 
    STATUS)
 Values
   ('HDS AR Tax Audit Return -Report Set', 'REQUEST_SET', 'N', 'wcitfinance@hdsupply.com', 'wcitfinance@hdsupply.com', 
    'NEW');
Insert into XXCUS.XXCUS_FIX_JOBS_ALERT_TMP
   (JOB_NAME, JOB_TYPE, REMOVE_USER, ERROR_EMAIL, WARNING_EMAIL, 
    STATUS)
 Values
   ('HDS AR Taxware Audit Return Report', 'SINGLE_REQUEST', 'N', 'wcitfinance@hdsupply.com', 'wcitfinance@hdsupply.com', 
    'NEW');
Insert into XXCUS.XXCUS_FIX_JOBS_ALERT_TMP
   (JOB_NAME, JOB_TYPE, REMOVE_USER, ERROR_EMAIL, WARNING_EMAIL, 
    STATUS)
 Values
   ('HDS AR Transform Taxware Audit Tax Data', 'SINGLE_REQUEST', 'N', 'wcitfinance@hdsupply.com', 'wcitfinance@hdsupply.com', 
    'NEW');
Insert into XXCUS.XXCUS_FIX_JOBS_ALERT_TMP
   (JOB_NAME, JOB_TYPE, REMOVE_USER, ERROR_EMAIL, WARNING_EMAIL, 
    STATUS)
 Values
   ('HDS Create APPS User ID for New Employees/Disable User ID', 'SINGLE_REQUEST', 'Y', 'hds.oracleglsupport@hdsupply.com', 'hds.oracleglsupport@hdsupply.com', 
    'NEW');
Insert into XXCUS.XXCUS_FIX_JOBS_ALERT_TMP
   (JOB_NAME, JOB_TYPE, REMOVE_USER, ERROR_EMAIL, WARNING_EMAIL, 
    STATUS)
 Values
   ('HDS ERROR TABLE EMAILER', 'SINGLE_REQUEST', 'N', 'hds.oracleglsupport@hdsupply.com', 'hds.oracleglsupport@hdsupply.com', 
    'NEW');
Insert into XXCUS.XXCUS_FIX_JOBS_ALERT_TMP
   (JOB_NAME, JOB_TYPE, REMOVE_USER, ERROR_EMAIL, WARNING_EMAIL, 
    STATUS)
 Values
   ('HDS HR PEOPLESOFT MAIN PROCESS', 'SINGLE_REQUEST', 'Y', 'hds.oracleglsupport@hdsupply.com', 'hds.oracleglsupport@hdsupply.com', 
    'NEW');
Insert into XXCUS.XXCUS_FIX_JOBS_ALERT_TMP
   (JOB_NAME, JOB_TYPE, REMOVE_USER, ERROR_EMAIL, WARNING_EMAIL, 
    STATUS)
 Values
   ('HDS Journal Import - FSS ONLY', 'SINGLE_REQUEST', 'N', 'hds.oracleglsupport@hdsupply.com', 'hds.oracleglsupport@hdsupply.com', 
    'NEW');
Insert into XXCUS.XXCUS_FIX_JOBS_ALERT_TMP
   (JOB_NAME, JOB_TYPE, REMOVE_USER, ERROR_EMAIL, WARNING_EMAIL, 
    STATUS)
 Values
   ('HDS OPN ROLLING RENT REPOP', 'SINGLE_REQUEST', 'N', 'hdsoraclerealestate@hdsupply.com', 'hdsoraclerealestate@hdsupply.com', 
    'NEW');
Insert into XXCUS.XXCUS_FIX_JOBS_ALERT_TMP
   (JOB_NAME, JOB_TYPE, REMOVE_USER, ERROR_EMAIL, WARNING_EMAIL, 
    STATUS)
 Values
   ('HDS OPN: Extract Locations', 'SINGLE_REQUEST', 'N', 'hdsoraclerealestate@hdsupply.com', 'hdsoraclerealestate@hdsupply.com', 
    'NEW');
Insert into XXCUS.XXCUS_FIX_JOBS_ALERT_TMP
   (JOB_NAME, JOB_TYPE, REMOVE_USER, ERROR_EMAIL, WARNING_EMAIL, 
    STATUS)
 Values
   ('HDS OPN: PM Web Lease Extract', 'SINGLE_REQUEST', 'N', 'hdsoraclerealestate@hdsupply.com', 'hdsoraclerealestate@hdsupply.com', 
    'NEW');
Insert into XXCUS.XXCUS_FIX_JOBS_ALERT_TMP
   (JOB_NAME, JOB_TYPE, REMOVE_USER, ERROR_EMAIL, WARNING_EMAIL, 
    STATUS)
 Values
   ('HDS OPN: Submit Lease Payment Extract', 'SINGLE_REQUEST', 'N', 'hdsoraclerealestate@hdsupply.com', 'hdsoraclerealestate@hdsupply.com', 
    'NEW');
Insert into XXCUS.XXCUS_FIX_JOBS_ALERT_TMP
   (JOB_NAME, JOB_TYPE, REMOVE_USER, ERROR_EMAIL, WARNING_EMAIL, 
    STATUS)
 Values
   ('HDS Rebates: Generate TM GL Journals Extract', 'SINGLE_REQUEST', 'N', 'HDSRebateManagementSystemNotifications@hdsupply.com', 'HDSRebateManagementSystemNotifications@hdsupply.com', 
    'NEW');
Insert into XXCUS.XXCUS_FIX_JOBS_ALERT_TMP
   (JOB_NAME, JOB_TYPE, REMOVE_USER, ERROR_EMAIL, WARNING_EMAIL, 
    STATUS)
 Values
   ('HDS Rebates: Generate TM SLA Accruals Extract', 'SINGLE_REQUEST', 'N', 'HDSRebateManagementSystemNotifications@hdsupply.com', 'HDSRebateManagementSystemNotifications@hdsupply.com', 
    'NEW');
Insert into XXCUS.XXCUS_FIX_JOBS_ALERT_TMP
   (JOB_NAME, JOB_TYPE, REMOVE_USER, ERROR_EMAIL, WARNING_EMAIL, 
    STATUS)
 Values
   ('HDS Rebates: Rebuild OZF Stack', 'REQUEST_SET', 'N', 'HDSRebateManagementSystemNotifications@hdsupply.com', 'HDSRebateManagementSystemNotifications@hdsupply.com', 
    'NEW');
Insert into XXCUS.XXCUS_FIX_JOBS_ALERT_TMP
   (JOB_NAME, JOB_TYPE, REMOVE_USER, ERROR_EMAIL, WARNING_EMAIL, 
    STATUS)
 Values
   ('HDS Rebates: Submit Claims Master', 'SINGLE_REQUEST', 'N', 'HDSRebateManagementSystemNotifications@hdsupply.com', 'HDSRebateManagementSystemNotifications@hdsupply.com', 
    'NEW');
Insert into XXCUS.XXCUS_FIX_JOBS_ALERT_TMP
   (JOB_NAME, JOB_TYPE, REMOVE_USER, ERROR_EMAIL, WARNING_EMAIL, 
    STATUS)
 Values
   ('HDS Refresh Rebates Materialized Views', 'SINGLE_REQUEST', 'N', 'HDSRebateManagementSystemNotifications@hdsupply.com', 'HDSRebateManagementSystemNotifications@hdsupply.com', 
    'NEW');
Insert into XXCUS.XXCUS_FIX_JOBS_ALERT_TMP
   (JOB_NAME, JOB_TYPE, REMOVE_USER, ERROR_EMAIL, WARNING_EMAIL, 
    STATUS)
 Values
   ('Import Locations and Space Allocation From Interface Tables', 'SINGLE_REQUEST', 'N', 'hdsoraclerealestate@hdsupply.com', 'hdsoraclerealestate@hdsupply.com', 
    'NEW');
Insert into XXCUS.XXCUS_FIX_JOBS_ALERT_TMP
   (JOB_NAME, JOB_TYPE, REMOVE_USER, ERROR_EMAIL, WARNING_EMAIL, 
    STATUS)
 Values
   ('PS/HR INTERFACE (HR Interface, FND User, CB Delegate)', 'REQUEST_SET', 'Y', 'hds.oracleglsupport@hdsupply.com', 'hds.oracleglsupport@hdsupply.com', 
    'NEW');
Insert into XXCUS.XXCUS_FIX_JOBS_ALERT_TMP
   (JOB_NAME, JOB_TYPE, REMOVE_USER, ERROR_EMAIL, WARNING_EMAIL, 
    STATUS)
 Values
   ('Record Order Management Transactions', 'SINGLE_REQUEST', 'N', 'wcitfinance@hdsupply.com', 'wcitfinance@hdsupply.com', 
    'NEW');
Insert into XXCUS.XXCUS_FIX_JOBS_ALERT_TMP
   (JOB_NAME, JOB_TYPE, REMOVE_USER, ERROR_EMAIL, WARNING_EMAIL, 
    STATUS)
 Values
   ('Visa VCF 4 Transaction Loader and Validation Program', 'SINGLE_REQUEST', 'Y', 'hds.oracleglsupport@hdsupply.com', 'hds.oracleglsupport@hdsupply.com', 
    'NEW');
Insert into XXCUS.XXCUS_FIX_JOBS_ALERT_TMP
   (JOB_NAME, JOB_TYPE, REMOVE_USER, ERROR_EMAIL, WARNING_EMAIL, 
    STATUS)
 Values
   ('WC AR TAXWARE AUDIT LOAD', 'SINGLE_REQUEST', 'N', 'wcitfinance@hdsupply.com', 'wcitfinance@hdsupply.com', 
    'NEW');
Insert into XXCUS.XXCUS_FIX_JOBS_ALERT_TMP
   (JOB_NAME, JOB_TYPE, REMOVE_USER, ERROR_EMAIL, WARNING_EMAIL, 
    STATUS)
 Values
   ('XXWC Close Case Folder', 'SINGLE_REQUEST', 'N', 'wcitfinance@hdsupply.com', 'wcitfinance@hdsupply.com', 
    'NEW');
Insert into XXCUS.XXCUS_FIX_JOBS_ALERT_TMP
   (JOB_NAME, JOB_TYPE, REMOVE_USER, ERROR_EMAIL, WARNING_EMAIL, 
    STATUS)
 Values
   ('XXWC OM Progress Counter SO Lines', 'SINGLE_REQUEST', 'N', 'hds-wc-SalesAndFulfillmentAlerts-u1@hdsupply.com', 'hds-wc-SalesAndFulfillmentAlerts-u1@hdsupply.com', 
    'NEW');
Insert into XXCUS.XXCUS_FIX_JOBS_ALERT_TMP
   (JOB_NAME, JOB_TYPE, REMOVE_USER, ERROR_EMAIL, WARNING_EMAIL, 
    STATUS)
 Values
   ('XXWC OM Progress SO Lines', 'SINGLE_REQUEST', 'N', 'hds-wc-SalesAndFulfillmentAlerts-u1@hdsupply.com', 'hds-wc-SalesAndFulfillmentAlerts-u1@hdsupply.com', 
    'NEW');
COMMIT;
