/*
 TMS: 20150721-00037 
 Date: 08/01/2015
 Notes: Month End - July month end close maintainance
*/
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20150721-00037, Update 1 -Before Update');
UPDATE apps.wsh_delivery_details
SET released_status = 'D',
src_requested_quantity = 0,
requested_quantity = 0,
shipped_quantity = 0,
cycle_count_quantity = 0,
cancelled_quantity = 0,
subinventory = null,
locator_id = null,
lot_number = null,
revision = null,
inv_interfaced_flag = 'X',
oe_interfaced_flag = 'X'
WHERE delivery_detail_id =11328505;
   DBMS_OUTPUT.put_line ('TMS: 20150721-00037, update 1 -After Update, rows modified: '||sql%rowcount);
   DBMS_OUTPUT.put_line ('TMS: 20150721-00037, Update 2 -Before Update');   
   update apps.wsh_delivery_assignments
set delivery_id = null,
parent_delivery_detail_id = null
where delivery_detail_id = 11328505;
DBMS_OUTPUT.put_line ('TMS: 20150721-00037, update 2 -After Update, rows modified: '||sql%rowcount);
   COMMIT;
   DBMS_OUTPUT.put_line ('TMS: 20150721-00037, Commit Complete'||SQLERRM);
   
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20150721-00037, Script 1, Errors ='||SQLERRM);
END;
/