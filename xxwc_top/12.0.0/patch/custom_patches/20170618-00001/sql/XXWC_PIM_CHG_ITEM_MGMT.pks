CREATE OR REPLACE PACKAGE APPS.xxwc_pim_chg_item_mgmt
AS
   /*****************************************************************************************************************
   -- File Name: XXWC_PIM_CHG_ITEM_MGMT.pks
   --
   -- PROGRAM TYPE: Package Spec.
   --
   -- PURPOSE:
   -- HISTORY
   -- ===============================================================================================================
   -- ===============================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- -------------------------------------------------------------------------
   -- 1.0     01-MAR-2015   Shankar Vanga   TMS#20150126-00044  - Create Single Item Workflow Tool
   -- 2.0     01-JUN-2015   P.Vamshidhar    TMS#20150519-00091 - Added new procedure XXWC_EGO_VENDOR_INSERT
   --                                       to store Vendor Number.
   -- 3.0     12-JUN-2015   P.Vamshidhar    TMS#20150608-00031  - Single Item UI Enhancements
   --                                       declared delete_item spec level
   -- 5.0     12-NOV-2015   P.Vamshidhar    TMS#20150917-00149-PCAM add validation for item check during WF approval
   -- 5.1     21-Feb-2017   P.Vamshidhar    TMS#20160913-00168  - PCAM EHS Workflow Enhancements
                                            TMS#20170302-00044  - NPR103064-WC part is not returning any results (212125216)
   -- 5.2     15-May-2017   P.Vamshidhar    TMS#20170421-00137 - UPC Validation(Product Creation and Maintenance)											
   -- 5.3     04-Jul-2017   P.Vamshidhar    TMS#20170618-00001 - UPC validation error correction for Data Team in PCAM    
   *******************************************************************************************************************/

   PROCEDURE CREATE_CHANGE_ORDER (
      p_item_level                     VARCHAR2,
      p_new_item                       VARCHAR2 DEFAULT 'N',
      p_co_description                 VARCHAR2,
      p_revised_item                   VARCHAR2,
      P_UOM_CODE                       VARCHAR2 DEFAULT NULL,
      p_hazard_mat                     VARCHAR2 DEFAULT 'N',
      p_reson_code                     VARCHAR2,
      p_vendor_number                  VARCHAR2, -- Added by Vamshi in V2.0  TMS#20150519-00091
      x_change_id           OUT NOCOPY NUMBER,
      x_item_id             OUT NOCOPY NUMBER,
      x_change_name         OUT NOCOPY VARCHAR2,
      x_return_status       OUT NOCOPY VARCHAR2,
      x_error_msg           OUT NOCOPY VARCHAR2);

   PROCEDURE REPLACE_ITEM (p_old_item_number                 VARCHAR2,
                           p_new_item_number                 VARCHAR2,
                           p_new_uom                         VARCHAR2,
                           p_item_id                         NUMBER,
                           x_request_id           OUT NOCOPY NUMBER);

   PROCEDURE replace_item_prg (errbuf                 OUT NOCOPY VARCHAR2,
                               retcode                OUT NOCOPY VARCHAR2,
                               p_old_item_number                 VARCHAR2,
                               p_new_item_number                 VARCHAR2,
                               p_new_uom                         VARCHAR2,
                               p_item_id                         NUMBER);

   PROCEDURE xxwc_delete_constraint (p_table_name         VARCHAR2,
                                     p_constraint_name    VARCHAR2);


   -- Added by Vamshi in V2.0  TMS#20150519-00091  Introduced new procedure to store vendor info.

   PROCEDURE XXWC_EGO_VENDOR_INSERT (p_login_id        IN VARCHAR2,
                                     p_vendor_number   IN VARCHAR2,
                                     p_change_name     IN VARCHAR2,
                                     p_status          IN VARCHAR2);

   -- Added below function declaration by Vamshi in V3.0  TMS#20150608-00031
   FUNCTION delete_item (p_item_number VARCHAR2)
      RETURN NUMBER;

   -- Added below Procedure declaration by Vamshi in V5.0  TMS#20150917-00149
   PROCEDURE Item_Number_Validation (p_change_id         IN     NUMBER,
                                     p_new_item_number   IN     VARCHAR2,
                                     x_return_status        OUT VARCHAR2);
									 
   -- Added below procedure in 5.1 Rev									 
   PROCEDURE send_email (P_CHANGE_NOTICE     IN VARCHAR2,
                                        P_OLD_ITEM_NUMBER   IN VARCHAR2,
                                        P_NEW_ITEM_NUMBER   IN VARCHAR2,
                                        P_USER_NAME         IN VARCHAR2,
                                        P_OLD_UOM_CODE      IN VARCHAR2,
                                        P_NEW_UOM_CODE      IN VARCHAR2);								 

   --Added below procedures in 5.2 Rev
   PROCEDURE PCAM_UPC_VALIDATION (P_ITEM_NUMBER   IN     VARCHAR2,
                                  P_ITEM_LEVEL    IN     VARCHAR2, -- Added in Rev 5.3
                                  P_REQUESTOR_NOTES IN VARCHAR2,   -- Added in Rev 5.3                                  
                                  P_CROSS_REF     IN     VARCHAR2,
                                  X_RET_MESS         OUT VARCHAR2);


   FUNCTION UPC_VALIDATION (P_UPC_VALUE IN VARCHAR2)
      RETURN BOOLEAN;

   PROCEDURE UPC_PCAM_MESS_DISPLAY (P_ITEM_NUMBER       IN VARCHAR2,
                                    P_ITEM_LEVEL    IN     VARCHAR2, -- Added in Rev 5.3  
                                    P_REQUESTOR_NOTES IN VARCHAR2,   -- Added in Rev 5.3                                     
                                    P_CROSS_REFERENCE   IN VARCHAR2,
                                    P_MESSAGE           IN VARCHAR2);

   FUNCTION PCAM_ITEM_REQ_NOTES (p_item_number IN VARCHAR2)
      RETURN VARCHAR2;

   -- Added below procedure in Rev 5.3   
   PROCEDURE pcam_upd_item_req_notes (p_change_id IN NUMBER);      	  
END;
/
GRANT ALL ON apps.xxwc_pim_chg_item_mgmt to DLS;
/