CREATE OR REPLACE TRIGGER APPS.XXWC_MTL_SY_ITEMS_CHG_B_TRG
   BEFORE INSERT OR UPDATE
   ON XXWC.XXWC_MTL_SY_ITEMS_CHG_B
   REFERENCING NEW AS NEW OLD AS OLD
   FOR EACH ROW
/**************************************************************************************************************************
   $Header XXWC_MTL_SY_ITEMS_CHG_B_TRG $
  Module Name: XXWC_MTL_SY_ITEMS_CHG_B_TRG
  PURPOSE:  To generate sno in XXWC_MTL_SY_ITEMS_CHG_B
  -- VERSION DATE          AUTHOR(S)           DESCRIPTION
  -- ------- -----------   ------------      -----------------------------------------
  -- 1.0     04-Jul-2017   P.Vamshidhar      TMS#20170618-00001 - UPC validation error correction for Data Team in PCAM
***************************************************************************************************************************/
DECLARE
   PRAGMA AUTONOMOUS_TRANSACTION;

   CURSOR CUR_ITEM_INFO (p_item_number IN VARCHAR2)
   IS
      SELECT DISTINCT MESSAGE
        FROM XXWC.XXWC_PCAM_UPC_POPUP_MESS_TBL
       WHERE ITEM_NUMBER = p_item_number;

   lvc_notes             XXWC.XXWC_MTL_SY_ITEMS_CHG_B.LONG_DESCIPTION%TYPE;
   lvc_requestor_notes   XXWC.XXWC_MTL_SY_ITEMS_CHG_B.LONG_DESCIPTION%TYPE;
   lvc_item_level        VARCHAR2 (3);
BEGIN
   SELECT ITEM_LEVEL, REQUESTOR_NOTES
     INTO lvc_item_level, lvc_requestor_notes
     FROM XXWC.XXWC_PCAM_UPC_POPUP_MESS_TBL
    WHERE ITEM_NUMBER = :NEW.SEGMENT1 AND ROWNUM = 1;

   IF lvc_item_level = 'L3'
   THEN
      FOR REC_ITEM_INFO IN CUR_ITEM_INFO (:NEW.SEGMENT1)
      LOOP
         IF lvc_notes IS NULL
         THEN
            lvc_notes := REC_ITEM_INFO.MESSAGE;
         ELSE
            lvc_notes := REC_ITEM_INFO.MESSAGE || CHR (13) || lvc_notes;
         END IF;
      END LOOP;

      IF lvc_requestor_notes IS NOT NULL
      THEN
         lvc_notes := lvc_notes || CHR (13) || lvc_requestor_notes;
      END IF;

      IF lvc_notes IS NOT NULL
      THEN
         IF INSERTING OR UPDATING
         THEN
            :NEW.LONG_DESCIPTION := lvc_notes;
         END IF;
      END IF;
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      NULL;
END;
/