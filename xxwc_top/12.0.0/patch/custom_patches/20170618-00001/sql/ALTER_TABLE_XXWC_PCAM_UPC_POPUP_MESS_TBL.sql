/****************************************************************************************************************************
  $Header XXWC_PCAM_UPC_POPUP_MESS_TBL.sql $
  Module Name: ALTER_TABLE_XXWC_PCAM_UPC_POPUP_MESS_TBL.sql

  PURPOSE:   Adding 2 columns to XXWC_PCAM_UPC_POPUP_MESS_TBL

  REVISIONS:
  Ver        Date        Author                     Description
  ---------  ----------  ---------------         ---------------------------------------------------------------------------
  1.0        07/04/2017  P.Vamshidhar             TMS#20170618-00001 - UPC validation error correction for Data Team in PCAM
*****************************************************************************************************************************/
ALTER TABLE XXWC.XXWC_PCAM_UPC_POPUP_MESS_TBL ADD (  ITEM_LEVEL       VARCHAR2(10 BYTE), REQUESTOR_NOTES  VARCHAR2(4000 BYTE));
/


