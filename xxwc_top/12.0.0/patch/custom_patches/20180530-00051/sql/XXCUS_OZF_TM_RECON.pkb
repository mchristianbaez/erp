CREATE OR REPLACE PACKAGE BODY APPS.XXCUS_OZF_TM_RECON
-- ESMS TICKET HISTORY
-- 228687 :  Date: 18-NOV-2013
--
-- Scope: Oracle Trade Management / Rebates accruals and gl journals extract
-- Used by concurrent jobs HDS Rebates: Generate TM SLA Accruals Extract and HDS Rebates: Generate TM GL Journals Extract
--
-- Change History:
-- Ticket#    ESMS         Date         Comment
-- =======    =========    ===========  ================================================================================
-- RFC 38762               02-Dec-2013  Changes to the signature of the routines invoked
-- RFC 38897               10-Dec-2013  Include parallel clause for few of the tables in the gl extract big insert statement to improve performance.
-- RFC 39151 ESMS  235535  01-Feb-2014  Add a new routine extract_accruals_single
--
--           ESMS 265235    10/14/2014  Invoke email notification at the end of the extract_journals routine
--  RFC 42038 / ESMS 267539 10/25/2014   Bala Seshadri Remove logic to send email. This logic is moved out as a new stage
--                                                     within the custom sunday request set
-- RFC 42991  ESMS 279945    02/25/2015    New routine extract_ytd_income
-- RFC 42991  ESMS 279945   02/26/2015    Insert records for fiscal period NA. Approval in the ESMS ticket.
--                                        Invoke the extract ytd income after the TM SLA extract is over in the
--                                        routine XXCUS_OZF_TM_RECON.extract_accruals_single
-- RFC 42991  ESMS 279945   02/27/2015    Added nine new columns as mentioned in the ESMS ticket
-- RFC 43009  ESMS 280366   03/04/2015    Comment routine xxcus_ozf_tm_recon.extract_ytd_income in the extract_accruals_single
--                                       as we are going to add extract_ytd_income as a separate stage with in the
--                                       rebates tuesday, wed thru fri, saturday and the wkly request sets.
--           ESMS 281958   03/18/2015   Add a new routine extract_gl_summary
--           ESMS 283147   03/26/2015   Add fields to routine extract_ytd_income
--                                       1) oxa_org_id  2) xlae_process_status_code  3) xlae_event_status_code  4) xaeh_gl_xfer_status_code
--      TMS 20151217-00142     12/15/2015 Balaguru Seshadri Edit pop_posted_fin_loc_info_S to use resale_line_attribute12
--                             Child: TMS 20151217-00142 , Parent: TMS  20151008-00085
--     TMS 20160316-00196, 03/30/2016, Added new procedure extract_rebate_accruals and extract_rebate_journals
--    TMS  20160602-00010 ESMS 321264 05/31/2016 Add routine to extract tier zero dollar.
--TMS#20180530-00051       31/05/2018   Added new procedure by Ashwin.S
as
  --
  ln_request_id NUMBER :=0;
  --
  procedure print_log(p_message in varchar2) is
  begin
   if fnd_global.conc_request_id >0 then
    fnd_file.put_line(fnd_file.log, p_message);
   else
    dbms_output.put_line(p_message);
   end if;
  exception
   when others then
    fnd_file.put_line(fnd_file.log, 'Issue in print_log routine ='||sqlerrm);
  end print_log;
  --
  -- Begin TMS  20160602-00010 ESMS 321264
  procedure extract_tier_zero
  (
    retcode               out number
   ,errbuf                out varchar2
   ,p_period              in  varchar2
  ) is
  --
   v_sql VARCHAR2(4000) :=Null;
   N_Count NUMBER;
   b_proceed BOOLEAN;
   --
   v_del_sql varchar2(4000) :=Null;
   sql_my_trx  varchar2(20000);
   l_partition_for_delete_oper varchar2(10);
   l_partition_created boolean;
   --
   b_backup  BOOLEAN;
   v_loc     varchar2(3);
   l_del_partition_days_after number :=90;
   --
   l_period  varchar2(10) :=Null;
   l_count number :=0;
   --
   l_result boolean;
   v_merge_sql varchar2(4000) :=Null;
   --
   l_fiscal_month varchar2(3);
   l_fiscal_period varchar2(10);
   l_fiscal_partition varchar2(10);
   --
   l_created_from date;
   l_created_to date;
   --
  -- ????
  procedure setup_vol_tier_zero_partition
  (
    p_fiscal_period  in    varchar2
   ,p_result              out  boolean
  )  
  is
   --
   l_split_partition_sql  varchar2(4000) :=Null;
   l_drop_partition_sql varchar2(4000) :=Null;
--   l_proceed boolean;
   --
   l_request_id number :=fnd_global.conc_request_id;
   l_user_id number :=fnd_global.user_id;
   --
    cursor fiscal_periods  is
    select period_name                                         fiscal_period
               ,substr(upper(period_name), 1, 3) fiscal_month
               ,upper(replace(period_name, '-', '')) fiscal_partition
               ,start_date                                                 fiscal_start_date
               ,end_date                                                   fiscal_end_date
               ,(
                    select count(utp.partition_name)
                    from dba_tab_partitions  utp
                    where 1 =1
                      and utp.table_owner ='XXCUS'
                      and utp.table_name  ='XXCUS_OZF_TIER_ZERO_ACCRUALS_B'
                      and utp.partition_name =upper(replace(p_fiscal_period, '-', ''))
                ) partition_count
    from gl_periods
    where 1 =1
    and period_set_name ='4-4-QTR'
    and period_name =p_fiscal_period
    and adjustment_period_flag ='N'; 
   --
  begin
     -- 
       --
           begin 
               --
               print_log(' ');
               print_log('Checking to see if we need to remove any partitions that are older than 90 days. If so we will drop them for good.');
               print_log(' ');
               --                                      
               for rec in (select * from xxcus.xxcus_ozf_zerotier_partitions where (trunc(sysdate) -trunc(last_update_date)) >= l_del_partition_days_after ) loop
                 --
                    select 'ALTER TABLE XXCUS.XXCUS_OZF_TIER_ZERO_ACCRUALS_B DROP PARTITION '||rec.partition_name
                    into l_drop_partition_sql
                    from dual;
               --
                       print_log(' ');
                       print_log(l_drop_partition_sql);
                       print_log(' ');
                       print_log('...Over 90 days...before dropping partition '||rec.partition_name);
                       execute immediate  l_drop_partition_sql;
                       print_log('......Over 90 days...after dropping partition '||rec.partition_name);           
                 --                   
               end loop;
               l_drop_partition_sql :=Null;
                 --        
           exception
            when others then
             print_log ('Error in removal of partition over 90 days, message :'||sqlerrm);
           end;      
       --     
     --  
     for rec in fiscal_periods loop
      --
       l_partition_for_delete_oper :=rec.fiscal_partition;
       l_fiscal_month :=rec.fiscal_month;
       l_fiscal_period :=rec.fiscal_period;
       l_fiscal_partition :=rec.fiscal_partition;       
       l_created_from :=rec.fiscal_start_date;
       l_created_to :=rec.fiscal_end_date;
      --      
      if (rec.partition_count =0) then
       --
           begin 
               --
               l_split_partition_sql :=Null;
               --
                    select 'ALTER TABLE XXCUS.XXCUS_OZF_TIER_ZERO_ACCRUALS_B '||'
                           '||'SPLIT PARTITION NA VALUES ('||''''||rec.fiscal_partition||''''||') '||'
                           '||' INTO '||'
                           '||' ( '||'
                           '||' PARTITION '||rec.fiscal_partition||','||'
                           '||' PARTITION NA '||'
                           '||')' 
                    into l_split_partition_sql
                    from dual;
               --
                   print_log(' ');
                   print_log(l_split_partition_sql);
                   print_log(' ');
                   print_log('...Before creating partition '||rec.fiscal_partition);
                   execute immediate  l_split_partition_sql;
                   print_log('......After creating partition '||rec.fiscal_partition);           
               --
               begin 
                --
                savepoint square1;
                -- 
                   insert into xxcus.xxcus_ozf_zerotier_partitions
                    (
                      partition_name
                     ,creation_date
                     ,created_by
                     ,request_id                 
                    )
                   values
                    (
                      rec.fiscal_partition
                     ,trunc(sysdate)
                     ,fnd_global.user_id
                     ,fnd_global.conc_request_id
                    );
                --
               exception
                when others then
                 rollback to square1;
                 print_log('Error in insert of table xxcus.xxcus_ozf_zerotier_partitions, message : '||sqlerrm);
               end;
               --
               l_partition_created :=TRUE;
               --               
               p_result :=TRUE;
               --
           exception
            when others then
             p_result :=FALSE;
           end;
       --
      else
           --Partition exists...no need to create...move on     
               begin 
                --
                savepoint square1;
                -- 
                   update xxcus.xxcus_ozf_zerotier_partitions
                    set last_update_date =sysdate
                          ,last_updated_by =fnd_global.user_id
                    where 1 =1
                         and partition_name =rec.fiscal_partition;
                --
               exception
                when others then
                 rollback to square1;
                 print_log('Error in insert of table xxcus.xxcus_ozf_zerotier_partitions, message : '||sqlerrm);
               end;
               --            
           print_log(' ');
           print_log('TM volume tier zero accrual table [XXCUS.XXCUS_OZF_TIER_ZERO_ACCRUALS_B] partition '||rec.fiscal_partition||' already exists.');
           print_log(' ');
           p_result :=TRUE;
           l_partition_created :=FALSE;
       --
      end if;
      --
     end loop;     
     --
  exception
   when others then
    print_log( 'Issue in setup_vol_tier_zero_partition routine ='||sqlerrm);
    p_result :=FALSE;
  end setup_vol_tier_zero_partition;  
  --  
  procedure pop_accruals_mvid_info_S is
  begin
   --
    v_merge_sql :='Merge /*+ PARALLEL (M1 4) */ into xxcus.xxcus_ozf_tier_zero_accruals_b partition ('||l_fiscal_partition||') M1 '||'
    '||' using (
            select hzca.attribute2      attribute2
                  ,hzp.party_name       party_name
                  ,hzca.cust_account_id cust_account_id
            from   hz_cust_accounts hzca
                  ,hz_parties       hzp
            where  1 =1
              and  hzp.party_id =hzca.party_id
           ) s
    on (
          (M1.ofu_cust_account_id = s.cust_account_id)
       )
    when matched then update set M1.ofu_mvid =s.attribute2, M1.ofu_mvid_name =s.party_name';
    --
   print_log('M1 SQL: ');
   print_log('========');
   print_log(v_merge_sql);
   print_log(' ');
   --
    print_log ('Begin Merge 1: Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
    execute immediate 'begin ' || v_merge_sql || '; :x := sql%rowcount; end;' using OUT l_count;
    print_log ('End Merge 1: Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
    --
   print_log('Merge M1 complete:  total records :'||l_count);
   --
   commit;
   --
  exception
   when others then
    print_log ('@pop_accruals_mvid_info_S, message ='||sqlerrm);
  end pop_accruals_mvid_info_S;
  --
  procedure pop_branch_info_S is
  begin
   --
        v_merge_sql :='Merge /*+ PARALLEL (M4 8) */ into xxcus.xxcus_ozf_tier_zero_accruals_b partition ('||l_fiscal_partition||') M4 '||'
        '||' using (
                        select hzca.attribute2      attribute2
                              ,hzp.party_name       party_name
                              ,hzca.account_number  account_number
                              ,hzca.cust_account_id cust_account_id
                        from   hz_cust_accounts hzca
                              ,hz_parties       hzp
                        where  1 =1
                          and  hzp.party_id =hzca.party_id
               ) s
        on (
              (M4.billto_cust_acct_id_br = s.cust_account_id)
           )
        when matched then update set M4.billto_cust_acct_br_name =s.party_name, M4.billto_cust_acct_br_acct_name =s.account_number';
        --
           print_log('M4 SQL: ');
           print_log('========');
           print_log(v_merge_sql);
           print_log(' ');
       --
        print_log ('Begin Merge 4: Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
        execute immediate 'begin ' || v_merge_sql || '; :x := sql%rowcount; end;' using OUT l_count;
        print_log ('End Merge 4: Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
        --
       print_log('Merge M4 complete:  total records :'||l_count);
       --
       commit;
       --
  exception
   when others then
    print_log ('@pop_branch_info_S, message ='||sqlerrm);
  end pop_branch_info_S;
  --
  procedure pop_posted_fin_loc_info_S is
  begin
   --
        v_merge_sql :='Merge /*+ PARALLEL (M7 8) */ into xxcus.xxcus_ozf_tier_zero_accruals_b partition ('||l_fiscal_partition||') M7 '||'
        '||' using (
                             select * from xxcus.xxcus_ozf_override_fin_loc_b
                          ) s
        on (
                 (        
                    M7.resale_line_attribute2 =s.BU --TMS  20160602-00010 ESMS 321264
                   --M7.resale_line_attribute11 =s.product  --TMS  20160602-00010 ESMS 321264
                   --and M7.resale_line_attribute12 =s.override_fin_loc  --TMS  20160602-00010 ESMS 321264
                   and M7.coop_yes_no             =''Y''
                   and s.override_fin_loc_y_n    =''Y''
                 )
           )
        when matched then update set M7.posted_fin_loc =s.override_fin_loc, M7.override_fin_loc =s.override_fin_loc_y_n';
        --
           print_log('M7 SQL: ');
           print_log('========');
           print_log(v_merge_sql);
           print_log(' ');
       --
       print_log ('Begin Merge 7: Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
        execute immediate 'begin ' || v_merge_sql || '; :x := sql%rowcount; end;' using OUT l_count;
        print_log ('End Merge 7: Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
        --
       print_log('Merge M7 complete:  total records :'||l_count);
       --
       commit;
       --
  exception
   when others then
    rollback;
    print_log ('@pop_posted_fin_loc_info_S, message ='||sqlerrm);
  end pop_posted_fin_loc_info_S;
  --
  procedure pop_posted_bu_branch_info_S is
  begin
        --
        v_merge_sql :='Merge /*+ PARALLEL (M10 8) */ into xxcus.xxcus_ozf_tier_zero_accruals_b partition ('||l_fiscal_partition||') M10 '||'
        '||' using (
                                select /*+ RESULT_CACHE */
                                       xlcv.entrp_loc     entrp_loc
                                      ,xlcv.lob_branch    lob_branch
                                      ,xrbxt.rebates_fru  rebates_fru
                                from apps.xxcus_location_code_vw xlcv
                                    ,(
                                       select a.rebates_fru
                                       from   xxcus.xxcus_rebate_branch_xref_tbl a
                                       where  1 =1
                                         and  rowid =
                                                (
                                                  select min(rowid)
                                                  from   xxcus.xxcus_rebate_branch_xref_tbl b
                                                  where  1 =1
                                                    and  b.rebates_fru =a.rebates_fru
                                                )
                                     ) xrbxt
                                    ,fnd_lookup_values_vl lv
                                where 1 =1
                                  and xrbxt.rebates_fru = xlcv.fru
                                  and lv.lookup_type = ''XXCUS_BUSINESS_UNIT''
                                  and xlcv.business_unit = lv.lookup_code
                                order by 1, 2
                          ) s
        on (
                            M10.posted_fin_loc   =s.entrp_loc
                    and M10.coop_yes_no      =''Y''
                    and M10.override_fin_loc =''Y''
           )
        when matched then update set M10.posted_bu_branch =s.lob_branch';
        --
           print_log('M10 SQL: ');
           print_log('========');
           print_log(v_merge_sql);
           print_log(' ');
       --
       print_log ('Begin Merge 10: Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
        execute immediate 'begin ' || v_merge_sql || '; :x := sql%rowcount; end;' using OUT l_count;
        print_log ('End Merge 10: Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
        --
       print_log('Merge M10 complete:  total records :'||l_count);
       --
       commit;
       --
  exception
   when others then
    rollback;
    print_log ('@pop_posted_bu_branch_info_S, message ='||sqlerrm);
  end pop_posted_bu_branch_info_S;
  -- Main Processing begins
  begin
   --
    begin
        --
           print_log('Delete SQL: ');
           print_log('==========');
           print_log(' delete /*+ noparallel */  xxcus.xxcus_ozf_override_fin_loc_b');
           print_log(' ');
        delete /*+ noparallel */  xxcus.xxcus_ozf_override_fin_loc_b;
        --
        commit;
        --
        insert /*+ noparallel */ into  xxcus.xxcus_ozf_override_fin_loc_b
        (
         product
        ,override_fin_loc
        ,override_fin_loc_y_n
        ,bu 
        )
        (
        select distinct trim(attribute4) product, attribute5 override_fin_loc, nvl(attribute11, 'N') override_fin_loc_y_n
        ,party_name 
        from   ar.hz_parties p
        where  1 =1
          and  attribute1 = 'HDS_BU'
          and  (attribute4 is not null and attribute5 is not null)
        );
        --
        commit;
        --
    exception
     when program_error then
      print_log('PROGRAM ERROR EXCEPTION IN INSERT OF xxcus.xxcus_ozf_override_fin_loc_b, MSG = '||sqlerrm);
      raise;
     when others then
      print_log('OTHERS EXCEPTION IN INSERT OF xxcus.xxcus_ozf_override_fin_loc_b, MSG ='||sqlerrm);
      raise;
    end;
    --   
   --
   begin
    --
    setup_vol_tier_zero_partition (p_fiscal_period =>p_period, p_result =>l_result);
    --
    if (l_result) then
       b_proceed :=TRUE;
    else
       b_proceed :=FALSE;
    end if;
    --
    print_log('');
    --
    if (b_proceed) then
          --
          if (NOT l_partition_created) then --When the partition exists there is a good chance we've some data already populated, lets remove and extract again
           -- *****
              begin
                select 'DELETE /*+ PARALLEL (D1 2) */ XXCUS.XXCUS_OZF_TIER_ZERO_ACCRUALS_B partition ('  --TMS 20160316-00196
                     ||l_partition_for_delete_oper
                     ||') D1 '
                into v_del_sql
                from dual;
                --
                   print_log('Delete SQL: ');
                   print_log('===========');
                   print_log(v_del_sql);
                   print_log(' ');
                --
                print_log ('Begin Delete SQL: Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
                print_log(' ');
                execute immediate 'begin ' || v_del_sql || '; :x := sql%rowcount; end;' using OUT l_count;
                print_log ('End Delete SQL: Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
                --
                b_proceed :=TRUE;
                --
              exception
               when others then
                b_proceed :=FALSE;
              end;
              --
              print_log(' ');
              --
              if l_count =0 then
               print_log ('Action -Cleanup: No TM volume tier zero accrual entries deleted '||
                          'using partition '||l_partition_for_delete_oper||
                          ' from table xxcus.xxcus_ozf_tier_zero_accruals_b'
                         );
              else
               print_log ('Action -Cleanup: Successfully deleted '|| l_count ||
                          ' existing TM volume tier zero accrual entries ' ||
                          ' using partition '||l_partition_for_delete_oper||
                          ' from table xxcus.xxcus_ozf_tier_zero_accruals_b'
                         );
              end if;
              --
              COMMIT;
              --           
           -- *****
          else
           b_proceed  :=TRUE; --Delete was not run so lets keep the variable b_proceed true so we can proceed further
          end if;
    else
      b_proceed :=FALSE; -- leave b_proceed with FALSE value so further steps will fail to kick off
    end if;
    --
   exception
    --
    when others then
     --
     b_proceed :=FALSE;
     print_log('Error in delete of xxcus.xxcus_ozf_tier_zero_accruals_b records for period ='||p_period||', msg ='||sqlerrm);
   end;
   --
   if (NOT b_proceed) then
    --
    print_log('Either partition setup process or delete of existing partition failed, extract process will quit now, please check the concurrent log for error messages');
    --
   else
    --
    -- begin insert from a specific partition based on fiscal period
    --
    begin
     --
     sql_my_trx :=
        'INSERT INTO /*+ APPEND PARALLEL 4 */ XXCUS.XXCUS_OZF_TIER_ZERO_ACCRUALS_B
        (
            SELECT /*+ PARALLEL(ofu 2) PARALLEL(rslines 2) PARALLEL(offers 2) PARALLEL(qph 2) */
                   to_number(null)                 xla_accrual_id
                  ,ofu.request_id                     ofu_request_id
                  ,ofu.utilization_type                   utilization_type
                  ,ofu.adjustment_type                adjustment_type
                  ,ofu.adjustment_type_id             adjustment_type_id
                  ,ofu.object_id                      object_id
                  ,ofu.plan_curr_amount               rebate_amount
                  ,ofu.acctd_amount                   util_acctd_amount
                  ,to_char(null)                              event_type_code
                  ,to_char(null)                    entity_code
                  ,to_number(null)                       event_id
                  ,ofu.utilization_id                 utilization_id
                  ,ofu.org_id                         oxa_org_id
                  ,trunc(ofu.gl_date)                 ofu_gl_date
                  ,hrou.set_of_books_id               ozf_ledger_id
                  ,case
                    when hrou.set_of_books_id =2021 then ''HDS Rebates USA''
                    when hrou.set_of_books_id =2023 then ''HDS Rebates CAN''
                    else Null
                   end                                ozf_ledger_name
                  ,to_number(null)                     xlae_entity_id
                  ,to_date(null)       xlae_trxn_date
                  ,to_date(null)              xlae_event_date
                  ,to_char(null)             xlae_event_status_code
                  ,to_char(null)           xlae_process_status_code
                  ,to_number(null)                  xlae_event_number
                  ,to_char(null)                  xlae_onhold_flag
                  ,to_number(null)                xla_application_id
                  ,to_number(null)                  xaeh_ae_header_id
                  ,to_date(null)               xaeh_accounting_date
                  ,to_char(null)       xaeh_gl_xfer_status_code
                  ,to_date(null)       xaeh_gl_xfer_date
                  ,to_char(null)              xaeh_je_category
                  ,'||''''||l_fiscal_month||''''||'           fiscal_month'||'
                  ,'||''''||l_fiscal_period||''''||'        xaeh_period_name'||'
                  ,to_number(null)            accounting_error_id
                  ,to_number(null)            accounting_batch_id
                  ,to_char(null)                   encoded_msg
                  ,to_number(null)                    err_ae_line_num
                  ,to_number(null)                 message_number
                  ,to_char(null)             error_source_code
                  ,offers.offer_id                    offer_id
                  ,offers.offer_code                  offer_code
                  ,qphtl.description                  offer_name
                  ,offers.offer_type                  offer_type
                  ,offers.autopay_party_id            autopay_party_id
                  ,offers.beneficiary_account_id      beneficiary_account_id
                  ,offers.status_code                 offer_status_code
                  ,offers.custom_setup_id             offer_custom_setup_id
                  ,offers.user_status_id              offer_user_status_id
                  ,offers.owner_id                    offer_owner_id
                  ,offers.activity_media_id           activity_media_id
                  ,ams.media_name                     activity_media_name
                  ,offers.autopay_flag                offer_autopay_flag
                  ,ofu.currency_code                  ofu_currency_code
                  ,rslines.currency_code              receipts_currency
                  ,qph.attribute1                     auto_renewal
                  ,qph.attribute7                     calendar_year
                  ,qph.attribute2                     until_year
                  ,qph.attribute5                     payment_method
                  ,qph.attribute6                     payment_frequency
                  ,qph.attribute9                     guaranteed_amount
                  ,ofu.plan_id                        qp_list_header_id
                  ,qph.global_flag                    qp_global_flag
                  ,qph.active_flag                    qp_active_flag
                  ,qph.orig_org_id                    qp_orig_org_id
                  ,ofu.year_id                        ofu_year_id
                  ,ofu.fund_id                        fund_id
                  ,(
                    select /*+ RESULT_CACHE */
                           short_name
                    from   ozf_funds_all_tl
                    where  1 =1
                      and  fund_id =ofu.fund_id
                   )                                  fund_name
                  ,ofu.plan_type                      plan_type
                  ,ofu.component_id                   component_id
                  ,ofu.component_type                 component_type
                  ,ofu.object_type                    object_type
                  ,ofu.reference_type                 reference_type
                  ,ofu.reference_id                   reference_id
                  ,ofu.ams_activity_budget_id         ams_activity_budget_id
                  ,ofu.product_level_type             product_level_type
                  ,ofu.product_id                     product_id
                  ,rslines.creation_date              resale_date_created
                  ,trunc(rslines.date_invoiced)       date_invoiced
                  ,rslines.po_number                  po_number
                  ,rslines.purchase_uom_code          purchase_uom_code
                  ,rslines.uom_code                   uom_code
                  ,rslines.purchase_price             purchase_price
                  ,ofu.cust_account_id                ofu_cust_account_id
                  ,Null                               ofu_mvid
                  ,Null                               ofu_mvid_name
                  ,rslines.bill_to_cust_account_id    billto_cust_acct_id_br
                  ,Null                               billto_cust_acct_br_name
                  ,Null                               billto_cust_acct_br_acct_name
                  ,rslines.bill_to_party_id           bill_to_party_id
                  ,rslines.bill_to_party_name         bill_to_party_name_lob
                  ,trunc(rslines.date_ordered)        date_ordered
                  ,rslines.selling_price              selling_price
                  ,rslines.quantity                   quantity
                  ,rslines.inventory_item_id          inventory_item_id
                  ,rslines.item_number                item_number
                  ,rslines.line_attribute1            resale_line_attribute1 --pay_to_vendor_code
                  ,rslines.line_attribute2            resale_line_attribute2 --bu_nm
                  ,rslines.line_attribute3            resale_line_attribute3 --src_sys_nm
                  ,rslines.line_attribute5            resale_line_attribute5 --receipt_number
                  ,rslines.line_attribute6            resale_line_attribute6 --ship_to_branch_code
                  ,rslines.line_attribute11           resale_line_attribute11 --product_segment
                  ,rslines.line_attribute12           resale_line_attribute12 --location_segment
                  ,rslines.line_attribute13           resale_line_attribute13 --freight_discount
                  ,rslines.line_attribute14           resale_line_attribute14 --payment_discount
                  ,ofu.price_adjustment_id            pricing_adj_id
                  ,trunc(ofu.exchange_rate_date)      ofu_exchange_rate_date
                  ,ofu.exchange_rate                  ofu_exchange_rate
                  ,ofu.billto_cust_account_id         ofu_billto_cust_account_id_br
                  ,ofu.product_id                     ofu_product_id
                  ,rslines.sold_from_cust_account_id  sold_from_cust_account_id_mvid
                  ,rslines.sold_from_site_id          sold_from_site_id_mvid
                  ,rslines.sold_from_party_name       sold_from_party_name_mvid
                  ,rslines.line_attribute12           posted_fin_loc
                  ,ams.coop_yes_no                    coop_yes_no
                  ,rslines.line_attribute6            posted_bu_branch
                  ,''N''                                override_fin_loc
                  ,ofu.attribute10                    ofu_attribute10
                  ,trunc(ofu.creation_date)           ofu_creation_date
                  ,'||''''||l_fiscal_partition||''''||'                                                   fiscal_partition '||' 
            '||' from   ozf_funds_utilized_all_b  ofu
                  ,hr_operating_units        hrou
                  ,ozf_offers                offers
                  ,qp_list_headers_b         qph
                  ,qp_list_headers_tl        qphtl
                  ,ozf_resale_lines_all      rslines
                  ,(
                    select amstl.media_name media_name
                          ,amstl.media_id
                          ,case
                            when amstl.description IN (''COOP'', ''COOP_MIN'') then ''Y''
                            else ''N''
                           end   coop_yes_no
                    from   ams_media_tl amstl
                          ,ams_media_b  ams
                    where  1 =1
                      and  ams.media_id =amstl.media_id
                   ) ams
            where  1 =1
                  and ofu.creation_date between '||''''||to_char(l_created_from, 'DD-MON-YY')||''''||' and '||''''||to_char(l_created_to, 'DD-MON-YY')||''''||'
                  and ofu.utilization_type =''ACCRUAL''
                  and ofu.plan_type =''OFFR'' 
                  and ofu.amount =0 --For volume agreements, when the first tier is not met...accruals are created with zero dollar.
                  and not exists
                   (
                        select 1
                        from   ozf_xla_accruals
                        where 1 =1
                        and utilization_id =ofu.utilization_id
                   )            
              and  hrou.organization_id    =ofu.org_id
              and  qphtl.list_header_id         =ofu.plan_id
              and  offers.qp_list_header_id     =qphtl.list_header_id
              and  qph.list_header_id           =qphtl.list_header_id
              and  rslines.resale_line_id(+)    =ofu.object_id
              and  ams.media_id(+)              =offers.activity_media_id
        )';
     --
     print_log('');
     --
     print_log('Insert SQL for Fiscal Period: '||p_period);
     print_log('======================================');
     --
     print_log('');
     --
     print_log(sql_my_trx);
     --
     print_log(' ');
     --
     begin
      --
      print_log('Before calling dynamic insert SQL for period: '||p_period);
      --
      execute immediate sql_my_trx;
      --
      print_log(' ');
      --
      print_log('After calling dynamic insert SQL for period: '||p_period);
      --
      print_log(' ');
      --
      print_log('Total records inserted for fiscal period '||p_period||' : '||sql%rowcount);
      --
      print_log(' ');
      --
                --
                begin
                 --
                 l_period :=p_period;
                 --
                 v_loc :='103';
                 pop_accruals_mvid_info_S;
                 Commit;
                 print_log ('@ update of accrual attributes, Exit: pop_accruals_mvid_info_S');
                 v_loc :='104';
                 pop_branch_info_S;
                 Commit;
                 print_log ('@ update of accrual attributes, Exit: pop_branch_info_S');
                 v_loc :='105';
                 pop_posted_fin_loc_info_S;
                 Commit;
                 print_log ('@ update of accrual attributes, Exit: pop_posted_fin_loc_info_S');
                 v_loc :='106';
                 pop_posted_bu_branch_info_S;
                 Commit;
                 v_loc :='107';
                 print_log ('@ update of accrual attributes, Exit: pop_posted_bu_branch_info_S');
                 --
                 Commit;
                 --
                exception
                 when others then
                  print_log ('@Accruals attributes update single, last known location ='||v_loc||', Error =>'||sqlerrm);
                end;
                --      
      --
     exception
      --
      when others then
       --
       print_log('@ Period: '||p_period||' error in calling dynamic SQL, message ='||sqlerrm);
       --
     end;
     --
    exception
     when others then
      --
      print_log('@ Period: '||p_period||' error in generating dynamic SQL, message ='||sqlerrm);
      --
    end;
    --
    -- end insert from a specific partition using fiscal period
   end if;
   --
  exception
   when others then
    print_log ('Issue in extract_tier_zero routine, message ='||sqlerrm);
    rollback;
  end extract_tier_zero;
  -- End TMS  20160602-00010 ESMS 321264
  --;  
  procedure extract_ytd_income
  (
    retcode               out number
   ,errbuf                out varchar2
   ,p_period              in  varchar2
  ) is
  --
   v_sql VARCHAR2(4000) :=Null;
   N_Count NUMBER;
   b_proceed BOOLEAN;
   --
   sql_my_trx  varchar2(20000);
   --
   b_backup  BOOLEAN;
   v_loc     varchar2(3);
   l_period  varchar2(10) :=Null;
   lv_period varchar2(10) :=Null; --TMS  20160602-00010 ESMS 321264
   lv_errbuf varchar2(150) :=Null; --TMS  20160602-00010 ESMS 321264
   lv_retcode varchar2(150) :=Null; --TMS  20160602-00010 ESMS 321264   
  --
  begin
   --
   -- begin delete
   -- Begin TMS  20160602-00010 ESMS 321264
   begin
      --
      print_log('');
      --
    print_log('Begin tier zero extract process for fiscal period '||p_period);      
    --
     lv_period :=p_period;
     --
        extract_tier_zero
          (
            retcode =>lv_retcode
           ,errbuf   =>lv_errbuf
           ,p_period  =>lv_period
          );
    --
    print_log('');
    --
    print_log('End tier zero extract process for fiscal period '||p_period);
    --
    print_log('');
    --
    b_proceed :=TRUE;
    --
   exception
     --  
     when others then
     --
     b_proceed :=FALSE;
     print_log('Error in extract of tier zero data for period ='||p_period||', msg ='||sqlerrm);
   end;   
   -- End TMS  20160602-00010 ESMS 321264
        if (b_proceed) then --TMS  20160602-00010 ESMS 321264
            --
           begin
            --
            print_log('');
            --
            savepoint start_here;
            --
             delete xxcus.xxcus_ytd_income_b
             where  1 =1
               and  fiscal_period =p_period;
            --
            print_log('');
            --
            print_log('Deleted '||sql%rowcount||' records from xxcus.xxcus_ytd_income_b for fiscal period '||p_period);
            --
             delete xxcus.xxcus_ytd_income_b
             where  1 =1
               and  fiscal_period ='NA';
            --
            print_log('');
            --
            print_log('Deleted '||sql%rowcount||' records from xxcus.xxcus_ytd_income_b for fiscal period: NA');
            --
            b_proceed :=TRUE;
            --
            print_log('');
            --
           exception
            --
            when others then
             --
             b_proceed :=FALSE;
             rollback to start_here;
             print_log('Error in delete of xxcus.xxcus_ytd_income_b records for period ='||p_period||', msg ='||sqlerrm);
           end;
           -- end delete    
        --   
       end if; -- --TMS  20160602-00010 ESMS 321264
   --
   if (NOT b_proceed) then
    --
    print_log('Delete routine failed, extract process will quit now, please check the concurrent log for error messages');
    --
   else
    --
    -- begin insert from a specific partition other than NA
    --
    begin
     --
     sql_my_trx :=
        'INSERT INTO /*+ APPEND */ XXCUS.XXCUS_YTD_INCOME_B
        (
        SELECT
                    XLA.XAEH_PERIOD_NAME                 FISCAL_PERIOD,
                    XLA.OFU_MVID                         MVID,
                    XLA.OFU_CUST_ACCOUNT_ID              OFU_CUST_ACCOUNT_ID,
                    (
                     SELECT /*+ RESULT_CACHE */
                            HZP.PARTY_NAME
                     FROM   HZ_PARTIES HZP,
                            HZ_CUST_ACCOUNTS HZCA
                     WHERE  1 =1
                       AND  HZCA.CUST_ACCOUNT_ID
                           =XLA.OFU_CUST_ACCOUNT_ID
                       AND  HZP.PARTY_ID
                           =HZCA.PARTY_ID
                    )                                    VENDOR_NAME,
                    XLA.BILL_TO_PARTY_NAME_LOB           LOB,
                    XLA.BILL_TO_PARTY_ID                 LOB_ID,
                    XLA.RESALE_LINE_ATTRIBUTE2           BU,
                    XLA.RESALE_LINE_ATTRIBUTE6           BRANCH,
                    XLA.BILLTO_CUST_ACCT_BR_ACCT_NAME    CUST_ACCT_BRANCH_NAME,
                    XLA.POSTED_FIN_LOC                   POSTED_FIN_LOC,
                    XLA.POSTED_BU_BRANCH                 POSTED_BU_BRANCH,
                    XLA.OFU_ATTRIBUTE10                  OFU_ATTRIBUTE10,
                    XLA.UTILIZATION_TYPE                 UTILIZATION_TYPE,
                    XLA.ADJUSTMENT_TYPE                  ADJUSTMENT_TYPE,
                    XLA.ADJUSTMENT_TYPE_ID               ADJUSTMENT_TYPE_ID,
                    XLA.OFU_CURRENCY_CODE                CURRENCY_CODE,
                    XLA.OZF_LEDGER_NAME                  LEDGER_NAME,
                    XLA.QP_LIST_HEADER_ID                PLAN_ID,
                    (
                     SELECT /*+ RESULT_CACHE */
                            QPLH.DESCRIPTION
                     FROM   QP_LIST_HEADERS_TL QPLH
                     WHERE  1 =1
                       AND  QPLH.LIST_HEADER_ID
                           =XLA.QP_LIST_HEADER_ID
                    )                                    OFFER_NAME,
                    OO.STATUS_CODE                       STATUS,
                    CASE
                     when XLA.ACTIVITY_MEDIA_NAME IN (''Coop'', ''Coop Minimum'') then ''COOP''
                     else ''REBATE''
                    END                                  REBATE_TYPE,
                    TO_NUMBER(XLA.CALENDAR_YEAR)         AGREEMENT_YEAR,
                    SUBSTR (T.month_id, 0, 4)            CALENDAR_YEAR,
                    T.ENT_YEAR_ID                        FISCAL_YEAR,
                    GLP.PERIOD_NUM                       FISCAL_MONTH_ID,
                    T.ENT_PERIOD_ID                      PERIOD_ID,
                    SUBSTR (T.MONTH_ID, 6, 2)            CALENDAR_MONTH_ID,
                    SUM(SELLING_PRICE*QUANTITY)          PURCHASES,
                    SUM (XLA.util_acctd_amount)          ACCRUAL_AMOUNT,
                    SYSDATE                              CREATION_DATE,
                    FND_GLOBAL.USER_ID                   CREATED_BY,
                    XLA.OXA_ORG_ID                       ORG_ID,
                    XLA.XLAE_PROCESS_STATUS_CODE         PROCESS_STATUS_CODE,
                    XLA.XLAE_EVENT_STATUS_CODE           EVENT_STATUS_CODE,
                    XLA.XAEH_GL_XFER_STATUS_CODE         GL_TRANSFER_STATUS_CODE
               FROM XXCUS.XXCUS_OZF_XLA_ACCRUALS_B  PARTITION ('||UPPER(SUBSTR(p_period, 1, 3))||') XLA
                   ,APPS.OZF_TIME_DAY              T
                   ,GL_PERIODS                     GLP
                   ,OZF_OFFERS                     OO
              WHERE 1 = 1
                AND XLA.OFU_GL_DATE        =T.REPORT_DATE
                AND XLA.XAEH_PERIOD_NAME   ='||''''||p_period||''''||'
                AND GLP.PERIOD_SET_NAME    =''4-4-QTR''
                AND glp.period_name =xla.xaeh_period_name -- TMS  20160602-00010 ESMS 321264
                AND GLP.ADJUSTMENT_PERIOD_FLAG =''N''
                AND T.REPORT_DATE BETWEEN GLP.START_DATE AND GLP.END_DATE
                AND OO.OFFER_ID            =XLA.OFFER_ID
                --AND OO.STATUS_CODE         <>''DRAFT''
           GROUP BY
                    XLA.XAEH_PERIOD_NAME,                    --FISCAL_PERIOD
                    XLA.OFU_MVID,                            --MVID,
                    XLA.OFU_CUST_ACCOUNT_ID,                 --CUST_ID,
                    XLA.BILL_TO_PARTY_NAME_LOB,              --LOB,
                    XLA.BILL_TO_PARTY_ID,                    --LOB_ID,
                    XLA.RESALE_LINE_ATTRIBUTE2,              --BU,
                    XLA.RESALE_LINE_ATTRIBUTE6,              --BRANCH
                    XLA.BILLTO_CUST_ACCT_BR_ACCT_NAME,       --CUST_ACCT_BRANCH_NAME
                    XLA.POSTED_FIN_LOC,                      --POSTED_FIN_LOCATION
                    XLA.POSTED_BU_BRANCH,                    --POSTED_BU_BRANCH
                    XLA.OFU_ATTRIBUTE10,                     --OFU_ATTRIBUTE10,
                    XLA.UTILIZATION_TYPE,                    --UTILIZATION_TYPE
                    XLA.ADJUSTMENT_TYPE,                     --ADJUSTMENT_TYPE
                    XLA.ADJUSTMENT_TYPE_ID,                  --ADJUSTMENT_TYPE_ID
                    XLA.OFU_CURRENCY_CODE,                   --CURRENCY_CODE
                    XLA.OZF_LEDGER_NAME,                     --LEDGER_NAME
                    XLA.QP_LIST_HEADER_ID,                   --PLAN_ID,
                    OO.STATUS_CODE,                          --STATUS,
                    CASE
                     when XLA.ACTIVITY_MEDIA_NAME IN (''Coop'', ''Coop Minimum'') then ''COOP''
                     else ''REBATE''
                    END,
                    TO_NUMBER(XLA.CALENDAR_YEAR),            --AGREEMENT_YEAR,
                    SUBSTR (T.month_id, 0, 4),               --CALENDAR_YEAR,
                    T.ENT_YEAR_ID,                           --FISCAL_YEAR,
                    T.ENT_PERIOD_ID,                         --PERIOD_ID
                    GLP.PERIOD_NUM,                          --FISCAL_MONTH_ID
                    SUBSTR (T.MONTH_ID, 6, 2),
                    XLA.OXA_ORG_ID,
                    XLA.XLAE_PROCESS_STATUS_CODE,
                    XLA.XLAE_EVENT_STATUS_CODE,
                    XLA.XAEH_GL_XFER_STATUS_CODE
                    -- Begin TMS  20160602-00010 ESMS 321264
              UNION
        SELECT
                    XLA.XAEH_PERIOD_NAME                 FISCAL_PERIOD,
                    XLA.OFU_MVID                         MVID,
                    XLA.OFU_CUST_ACCOUNT_ID              OFU_CUST_ACCOUNT_ID,
                    (
                     SELECT /*+ RESULT_CACHE */
                            HZP.PARTY_NAME
                     FROM   HZ_PARTIES HZP,
                            HZ_CUST_ACCOUNTS HZCA
                     WHERE  1 =1
                       AND  HZCA.CUST_ACCOUNT_ID
                           =XLA.OFU_CUST_ACCOUNT_ID
                       AND  HZP.PARTY_ID
                           =HZCA.PARTY_ID
                    )                                    VENDOR_NAME,
                    XLA.BILL_TO_PARTY_NAME_LOB           LOB,
                    XLA.BILL_TO_PARTY_ID                 LOB_ID,
                    XLA.RESALE_LINE_ATTRIBUTE2           BU,
                    XLA.RESALE_LINE_ATTRIBUTE6           BRANCH,
                    XLA.BILLTO_CUST_ACCT_BR_ACCT_NAME    CUST_ACCT_BRANCH_NAME,
                    XLA.POSTED_FIN_LOC                   POSTED_FIN_LOC,
                    XLA.POSTED_BU_BRANCH                 POSTED_BU_BRANCH,
                    XLA.OFU_ATTRIBUTE10                  OFU_ATTRIBUTE10,
                    XLA.UTILIZATION_TYPE                 UTILIZATION_TYPE,
                    XLA.ADJUSTMENT_TYPE                  ADJUSTMENT_TYPE,
                    XLA.ADJUSTMENT_TYPE_ID               ADJUSTMENT_TYPE_ID,
                    XLA.OFU_CURRENCY_CODE                CURRENCY_CODE,
                    XLA.OZF_LEDGER_NAME                  LEDGER_NAME,
                    XLA.QP_LIST_HEADER_ID                PLAN_ID,
                    (
                     SELECT /*+ RESULT_CACHE */
                            QPLH.DESCRIPTION
                     FROM   QP_LIST_HEADERS_TL QPLH
                     WHERE  1 =1
                       AND  QPLH.LIST_HEADER_ID
                           =XLA.QP_LIST_HEADER_ID
                    )                                    OFFER_NAME,
                    OO.STATUS_CODE                       STATUS,
                    CASE
                     when XLA.ACTIVITY_MEDIA_NAME IN (''Coop'', ''Coop Minimum'') then ''COOP''
                     else ''REBATE''
                    END                                  REBATE_TYPE,
                    TO_NUMBER(XLA.CALENDAR_YEAR)         AGREEMENT_YEAR,
                    SUBSTR (T.month_id, 0, 4)            CALENDAR_YEAR,
                    T.ENT_YEAR_ID                        FISCAL_YEAR,
                    GLP.PERIOD_NUM                       FISCAL_MONTH_ID,
                    T.ENT_PERIOD_ID                      PERIOD_ID,
                    SUBSTR (T.MONTH_ID, 6, 2)            CALENDAR_MONTH_ID,
                    SUM(SELLING_PRICE*QUANTITY)          PURCHASES,
                    SUM (XLA.util_acctd_amount)          ACCRUAL_AMOUNT,
                    SYSDATE                              CREATION_DATE,
                    FND_GLOBAL.USER_ID                   CREATED_BY,
                    XLA.OXA_ORG_ID                       ORG_ID,
                    XLA.XLAE_PROCESS_STATUS_CODE         PROCESS_STATUS_CODE,
                    XLA.XLAE_EVENT_STATUS_CODE           EVENT_STATUS_CODE,
                    XLA.XAEH_GL_XFER_STATUS_CODE         GL_TRANSFER_STATUS_CODE
               FROM XXCUS.XXCUS_OZF_TIER_ZERO_ACCRUALS_B  PARTITION ('||upper(replace(p_period, '-', ''))||') XLA
                   ,APPS.OZF_TIME_DAY              T
                   ,GL_PERIODS                     GLP
                   ,OZF_OFFERS                     OO
              WHERE 1 = 1
                --AND XLA.OFU_GL_DATE        =T.REPORT_DATE
                AND XLA.OFU_CREATION_DATE        =T.REPORT_DATE
                AND XLA.XAEH_PERIOD_NAME   ='||''''||p_period||''''||'
                AND GLP.PERIOD_SET_NAME    =''4-4-QTR''
                AND glp.period_name =xla.xaeh_period_name
                AND GLP.ADJUSTMENT_PERIOD_FLAG =''N''
                AND T.REPORT_DATE BETWEEN GLP.START_DATE AND GLP.END_DATE
                AND OO.OFFER_ID            =XLA.OFFER_ID
                --AND OO.STATUS_CODE         <>''DRAFT''
           GROUP BY
                    XLA.XAEH_PERIOD_NAME,                    --FISCAL_PERIOD
                    XLA.OFU_MVID,                            --MVID,
                    XLA.OFU_CUST_ACCOUNT_ID,                 --CUST_ID,
                    XLA.BILL_TO_PARTY_NAME_LOB,              --LOB,
                    XLA.BILL_TO_PARTY_ID,                    --LOB_ID,
                    XLA.RESALE_LINE_ATTRIBUTE2,              --BU,
                    XLA.RESALE_LINE_ATTRIBUTE6,              --BRANCH
                    XLA.BILLTO_CUST_ACCT_BR_ACCT_NAME,       --CUST_ACCT_BRANCH_NAME
                    XLA.POSTED_FIN_LOC,                      --POSTED_FIN_LOCATION
                    XLA.POSTED_BU_BRANCH,                    --POSTED_BU_BRANCH
                    XLA.OFU_ATTRIBUTE10,                     --OFU_ATTRIBUTE10,
                    XLA.UTILIZATION_TYPE,                    --UTILIZATION_TYPE
                    XLA.ADJUSTMENT_TYPE,                     --ADJUSTMENT_TYPE
                    XLA.ADJUSTMENT_TYPE_ID,                  --ADJUSTMENT_TYPE_ID
                    XLA.OFU_CURRENCY_CODE,                   --CURRENCY_CODE
                    XLA.OZF_LEDGER_NAME,                     --LEDGER_NAME
                    XLA.QP_LIST_HEADER_ID,                   --PLAN_ID,
                    OO.STATUS_CODE,                          --STATUS,
                    CASE
                     when XLA.ACTIVITY_MEDIA_NAME IN (''Coop'', ''Coop Minimum'') then ''COOP''
                     else ''REBATE''
                    END,
                    TO_NUMBER(XLA.CALENDAR_YEAR),            --AGREEMENT_YEAR,
                    SUBSTR (T.month_id, 0, 4),               --CALENDAR_YEAR,
                    T.ENT_YEAR_ID,                           --FISCAL_YEAR,
                    T.ENT_PERIOD_ID,                         --PERIOD_ID
                    GLP.PERIOD_NUM,                          --FISCAL_MONTH_ID
                    SUBSTR (T.MONTH_ID, 6, 2),
                    XLA.OXA_ORG_ID,
                    XLA.XLAE_PROCESS_STATUS_CODE,
                    XLA.XLAE_EVENT_STATUS_CODE,
                    XLA.XAEH_GL_XFER_STATUS_CODE     
                    -- End TMS  20160602-00010 ESMS 321264                                           
        )';
     --
     print_log('');
     --
     print_log('Insert SQL for Fiscal Period: '||p_period);
     print_log('=========================================');
     --
     print_log('');
     --
     print_log(sql_my_trx);
     --
     begin
      --
      savepoint init_here;
      --
      print_log('Before calling dynamic insert SQL for period: '||p_period);
      --
      execute immediate sql_my_trx;
      --
      print_log('After calling dynamic insert SQL for period: '||p_period);
      --
      print_log('Total records inserted for fiscal period '||p_period||' ='||sql%rowcount);
      --
     exception
      --
      when others then
       --
       print_log('@ Period: '||p_period||' error in calling dynamic SQL, message ='||sqlerrm);
       --
       rollback to init_here;
     end;
     --
    exception
     when others then
      --
      print_log('@ Period: '||p_period||' error in generating dynamic SQL, message ='||sqlerrm);
      --
    end;
    --
    -- end insert from a specific partition other than NA
    --
    --
    -- begin insert from partition NA
    --
    begin
     --
     sql_my_trx :=
        'INSERT INTO /*+ APPEND */ XXCUS.XXCUS_YTD_INCOME_B
        (
        SELECT
                    XLA.XAEH_PERIOD_NAME                 FISCAL_PERIOD,
                    XLA.OFU_MVID                         MVID,
                    XLA.OFU_CUST_ACCOUNT_ID              OFU_CUST_ACCOUNT_ID,
                    (
                     SELECT /*+ RESULT_CACHE */
                            HZP.PARTY_NAME
                     FROM   HZ_PARTIES HZP,
                            HZ_CUST_ACCOUNTS HZCA
                     WHERE  1 =1
                       AND  HZCA.CUST_ACCOUNT_ID
                           =XLA.OFU_CUST_ACCOUNT_ID
                       AND  HZP.PARTY_ID
                           =HZCA.PARTY_ID
                    )                                    VENDOR_NAME,
                    XLA.BILL_TO_PARTY_NAME_LOB           LOB,
                    XLA.BILL_TO_PARTY_ID                 LOB_ID,
                    XLA.RESALE_LINE_ATTRIBUTE2           BU,
                    XLA.RESALE_LINE_ATTRIBUTE6           BRANCH,
                    XLA.BILLTO_CUST_ACCT_BR_ACCT_NAME    CUST_ACCT_BRANCH_NAME,
                    XLA.POSTED_FIN_LOC                   POSTED_FIN_LOC,
                    XLA.POSTED_BU_BRANCH                 POSTED_BU_BRANCH,
                    XLA.OFU_ATTRIBUTE10                  OFU_ATTRIBUTE10,
                    XLA.UTILIZATION_TYPE                 UTILIZATION_TYPE,
                    XLA.ADJUSTMENT_TYPE                  ADJUSTMENT_TYPE,
                    XLA.ADJUSTMENT_TYPE_ID               ADJUSTMENT_TYPE_ID,
                    XLA.OFU_CURRENCY_CODE                CURRENCY_CODE,
                    XLA.OZF_LEDGER_NAME                  LEDGER_NAME,
                    XLA.QP_LIST_HEADER_ID                PLAN_ID,
                    (
                     SELECT /*+ RESULT_CACHE */
                            QPLH.DESCRIPTION
                     FROM   QP_LIST_HEADERS_TL QPLH
                     WHERE  1 =1
                       AND  QPLH.LIST_HEADER_ID
                           =XLA.QP_LIST_HEADER_ID
                    )                                    OFFER_NAME,
                    OO.STATUS_CODE                       STATUS,
                    CASE
                     when XLA.ACTIVITY_MEDIA_NAME IN (''Coop'', ''Coop Minimum'') then ''COOP''
                     else ''REBATE''
                    END                                  REBATE_TYPE,
                    TO_NUMBER(XLA.CALENDAR_YEAR)         AGREEMENT_YEAR,
                    SUBSTR (T.month_id, 0, 4)            CALENDAR_YEAR,
                    T.ENT_YEAR_ID                        FISCAL_YEAR,
                    GLP.PERIOD_NUM                       FISCAL_MONTH_ID,
                    T.ENT_PERIOD_ID                      PERIOD_ID,
                    SUBSTR (T.MONTH_ID, 6, 2)            CALENDAR_MONTH_ID,
                    SUM(SELLING_PRICE*QUANTITY)          PURCHASES,
                    SUM (XLA.util_acctd_amount)          ACCRUAL_AMOUNT,
                    SYSDATE                              CREATION_DATE,
                    FND_GLOBAL.USER_ID                   CREATED_BY,
                    XLA.OXA_ORG_ID                       ORG_ID,
                    XLA.XLAE_PROCESS_STATUS_CODE         PROCESS_STATUS_CODE,
                    XLA.XLAE_EVENT_STATUS_CODE           EVENT_STATUS_CODE,
                    XLA.XAEH_GL_XFER_STATUS_CODE         GL_TRANSFER_STATUS_CODE
               FROM XXCUS.XXCUS_OZF_XLA_ACCRUALS_B  PARTITION (NA) XLA
                   ,APPS.OZF_TIME_DAY              T
                   ,GL_PERIODS                     GLP
                   ,OZF_OFFERS                     OO
              WHERE 1 = 1
                AND XLA.OFU_GL_DATE        =T.REPORT_DATE
                AND XLA.XAEH_PERIOD_NAME   =''NA''
                AND GLP.PERIOD_SET_NAME    =''4-4-QTR''
                AND GLP.ADJUSTMENT_PERIOD_FLAG =''N''
                AND T.REPORT_DATE BETWEEN GLP.START_DATE AND GLP.END_DATE
                AND OO.OFFER_ID            =XLA.OFFER_ID
                --AND OO.STATUS_CODE         <>''DRAFT''
           GROUP BY
                    XLA.XAEH_PERIOD_NAME,                     --FISCAL_PERIOD
                    XLA.OFU_MVID,                            --MVID,
                    XLA.OFU_CUST_ACCOUNT_ID,                 --CUST_ID,
                    XLA.BILL_TO_PARTY_NAME_LOB,              --LOB,
                    XLA.BILL_TO_PARTY_ID,                    --LOB_ID,
                    XLA.RESALE_LINE_ATTRIBUTE2,              --BU,
                    XLA.RESALE_LINE_ATTRIBUTE6,              --BRANCH
                    XLA.BILLTO_CUST_ACCT_BR_ACCT_NAME,       --CUST_ACCT_BRANCH_NAME
                    XLA.POSTED_FIN_LOC,                      --POSTED_FIN_LOCATION
                    XLA.POSTED_BU_BRANCH,                    --POSTED_BU_BRANCH
                    XLA.OFU_ATTRIBUTE10,                     --OFU_ATTRIBUTE10,
                    XLA.UTILIZATION_TYPE,                    --UTILIZATION_TYPE
                    XLA.ADJUSTMENT_TYPE,                     --ADJUSTMENT_TYPE
                    XLA.ADJUSTMENT_TYPE_ID,                  --ADJUSTMENT_TYPE_ID
                    XLA.OFU_CURRENCY_CODE,                   --CURRENCY_CODE
                    XLA.OZF_LEDGER_NAME,                     --LEDGER_NAME
                    XLA.QP_LIST_HEADER_ID,                   --PLAN_ID,
                    OO.STATUS_CODE,                          --STATUS,
                    CASE
                     when XLA.ACTIVITY_MEDIA_NAME IN (''Coop'', ''Coop Minimum'') then ''COOP''
                     else ''REBATE''
                    END,
                    TO_NUMBER(XLA.CALENDAR_YEAR),            --AGREEMENT_YEAR,
                    SUBSTR (T.month_id, 0, 4),               --CALENDAR_YEAR,
                    T.ENT_YEAR_ID,                           --FISCAL_YEAR,
                    T.ENT_PERIOD_ID,                         --PERIOD_ID
                    GLP.PERIOD_NUM,                          --FISCAL_MONTH_ID
                    SUBSTR (T.MONTH_ID, 6, 2),
                    XLA.OXA_ORG_ID,
                    XLA.XLAE_PROCESS_STATUS_CODE,
                    XLA.XLAE_EVENT_STATUS_CODE,
                    XLA.XAEH_GL_XFER_STATUS_CODE
        )';
     --
     print_log('');
     --
     print_log('Insert SQL for Period: NA');
     print_log('=========================');
     --
     print_log('');
     --
     print_log(sql_my_trx);
     --
     begin
      --
      savepoint init_here;
      --
      print_log('Before calling dynamic insert SQL @ Period: NA');
      --
      execute immediate sql_my_trx;
      --
      print_log('After calling dynamic insert SQL @ Period: NA');
      --
      print_log('Total records inserted for fiscal period NA ='||sql%rowcount);
      --
     exception
      --
      when others then
       --
       print_log('@ Period: NA, error in calling dynamic SQL, message ='||sqlerrm);
       --
       rollback to init_here;
     end;
     --
    exception
     when others then
      --
      print_log('@ Partition: NA, error in generating dynamic SQL, message ='||sqlerrm);
      --
    end;
    --
    -- end insert from partition NA
    --
   end if;
   --
  exception
   when others then
    print_log ('Issue in extract_ytd_income routine, message ='||sqlerrm);
    rollback;
  end extract_ytd_income;
  --;
  --
  procedure extract_gl_summary
  (
    retcode               out number
   ,errbuf                out varchar2
   ,p_period              in  varchar2
  ) is
  --
   v_sql VARCHAR2(4000) :=Null;
   N_Count NUMBER;
   b_proceed BOOLEAN;
   --
   sql_my_trx  varchar2(20000);
   --
   b_backup  BOOLEAN;
   v_loc     varchar2(3);
   l_period  varchar2(10) :=Null;
  --
  begin
   --
   -- begin delete
   begin
    --
    print_log('');
    --
    savepoint start_here;
    --
     delete xxcus.xxcus_ozf_gl_summary_b
     where  1 =1
       and  fiscal_period =p_period;
    --
    print_log('');
    --
    print_log('Deleted '||sql%rowcount||' records from xxcus.xxcus_ozf_gl_summary_b for fiscal period '||p_period);
    --
    b_proceed :=TRUE;
    --
    print_log('');
    --
   exception
    --
    when others then
     --
     b_proceed :=FALSE;
     rollback to start_here;
     print_log('Error in delete of xxcus.xxcus_ozf_gl_summary_b records for period ='||p_period||', msg ='||sqlerrm);
   end;
   -- end delete
   --
   if (NOT b_proceed) then
    --
    print_log('Delete routine failed, extract process will quit now, please check the concurrent log for error messages');
    --
   else
    --
    -- begin insert
    --
    begin
     --
     sql_my_trx :=
        'INSERT INTO /*+ APPEND */ XXCUS.XXCUS_OZF_GL_SUMMARY_B
        (
              SELECT OFU_MVID,
                     BILL_TO_PARTY_NAME LOB,
                     CASE
                      WHEN UPPER(ACTIVITY_MEDIA_NAME) IN (''COOP'', ''COOP MINIMUM'') THEN ''COOP''
                      ELSE ''REBATE''
                     END REBATE_TYPE,
                     QP_LIST_HEADER_ID,
                     OFFER_CODE,
                     OFFER_NAME,
                     CALENDAR_YEAR AGREEMENT_YEAR,
                     GL_LEDGER_NAME,
                     GL_SOURCE,
                     GL_CATEGORY,
                     FISCAL_MONTH,
                     GL_PERIOD,
                     (SELECT /*+ RESULT_CACHE */
                             ENT_PERIOD_ID
                        FROM OZF.OZF_TIME_ENT_PERIOD
                       WHERE NAME = GL_PERIOD
                     ) PERIOD_ID,
                     XAEH_GL_XFER_STATUS_CODE,
                     GL_PRODUCT,
                     GL_ACCOUNT,
                     GL_LOCATION,
                     SUM (AMOUNT) AMOUNT,
                     SUM (UTIL_ACCTD_AMOUNT) ACCTD_AMOUNT
        FROM XXCUS.XXCUS_OZF_GL_JOURNALS_B  PARTITION ('||UPPER(SUBSTR(p_period, 1, 3))||')
        WHERE 1 = 1
          AND FISCAL_MONTH  ='||''''||UPPER(SUBSTR(p_period, 1, 3))||''''||'
          AND GL_PERIOD     ='||''''||p_period||''''||'
        GROUP BY OFU_MVID,
                 BILL_TO_PARTY_NAME,
                 CASE
                      WHEN UPPER(ACTIVITY_MEDIA_NAME) IN (''COOP'', ''COOP MINIMUM'') THEN ''COOP''
                      ELSE ''REBATE''
                 END,
                 QP_LIST_HEADER_ID,
                 OFFER_CODE,
                 OFFER_NAME,
                 CALENDAR_YEAR,
                 GL_LEDGER_NAME,
                 GL_SOURCE,
                 GL_CATEGORY,
                 FISCAL_MONTH,
                 GL_PERIOD,
                 XAEH_GL_XFER_STATUS_CODE,
                 GL_PRODUCT,
                 GL_ACCOUNT,
                 GL_LOCATION
        )';
     --
     print_log('');
     --
     print_log('Insert SQL for Fiscal Period: '||p_period);
     print_log('=========================================');
     --
     print_log('');
     --
     print_log(sql_my_trx);
     --
     begin
      --
      savepoint init_here;
      --
      print_log('Before calling dynamic insert SQL for period: '||p_period);
      --
      execute immediate sql_my_trx;
      --
      print_log('After calling dynamic insert SQL for period: '||p_period);
      --
      print_log('Total records inserted for fiscal period '||p_period||' ='||sql%rowcount);
      --
     exception
      --
      when others then
       --
       print_log('@ Period: '||p_period||' error in calling dynamic SQL, message ='||sqlerrm);
       --
       rollback to init_here;
     end;
     --
    exception
     when others then
      --
      print_log('@ Period: '||p_period||' error in generating dynamic SQL, message ='||sqlerrm);
      --
    end;
    --
    -- end insert
    --
   end if;
   --
  exception
   when others then
    print_log ('Issue in extract_gl_summary routine, message ='||sqlerrm);
    rollback;
  end extract_gl_summary;
  --;
  --
  procedure extract_accruals_single
  (
    retcode               out number
   ,errbuf                out varchar2
   ,p_period              in  varchar2
   ,p_account             in  varchar2
  ) is
  --
   v_sql VARCHAR2(240) :=Null;
   N_Count NUMBER;
   b_proceed BOOLEAN;
   --
   v_retcode varchar2(240) :=Null;
   v_errbuf  varchar2(240) :=Null;
   --
   v_del_sql varchar2(2000) :=Null;
   --
   b_backup  BOOLEAN;
   v_loc     varchar2(3);
   l_period  varchar2(10) :=Null;
  --
   cursor rebuild_partitioned_indexes (p_table in varchar2) is
    select 'ALTER INDEX '||owner||'.'||uip.index_name||' REBUILD PARTITION '||uip.partition_name||' PARALLEL 4' idx_sql
    from dba_ind_partitions  uip
        ,dba_tab_partitions  utp
        ,dba_indexes         ui
    where 1 =1
      and uip.partition_name  =utp.partition_name
      and ui.index_name       =uip.index_name
      and ui.table_name       =utp.table_name
      and ui.owner            ='XXCUS'
      and utp.table_name      =p_table -- 'XXCUS_OZF_XLA_ACCRUALS_B'
    order by uip.index_name;
  --
  procedure pop_accruals_ledger_name_S is
  begin
   --
   savepoint square1;
   --
   merge /*+ PARALLEL (t 8) */ into xxcus.xxcus_ozf_xla_accruals_b t
    using (
            select name ledger_name
                  ,ledger_id ledger_id
            from   gl_ledgers
            where  1 =1
           ) s
    on ((t.xaeh_period_name =l_period or t.xaeh_period_name ='NA') and t.ozf_ledger_id = s.ledger_id)
    when matched then update set t.ozf_ledger_name =s.ledger_name;
   --
  exception
   when others then
    rollback to square1;
    print_log ('@pop_accruals_ledger_name_S, message ='||sqlerrm);
  end pop_accruals_ledger_name_S;
  --
  procedure pop_accruals_media_name_S is
    V_CO_OP varchar2(10) :='COOP';
  begin
   --
   savepoint square1;
   --
    merge /*+ PARALLEL (t 8) */ into xxcus.xxcus_ozf_xla_accruals_b t
    using (
            select amstl.media_name
                  ,amstl.media_id
                  ,case
                    --when ams.attribute1 =V_CO_OP then 'Y'
                    when amstl.description IN ('COOP', 'COOP_MIN') then 'Y'
                    else 'N'
                   end   coop_yes_no
            from   ams_media_tl amstl
                  ,ams_media_b  ams
            where  1 =1
              and  ams.media_id =amstl.media_id
           ) s
    on (
          ((t.xaeh_period_name =l_period or t.xaeh_period_name ='NA') and t.activity_media_id = s.media_id)
        OR
          ((NVL(xaeh_gl_xfer_status_code, 'Z') <>'Y') and t.activity_media_id = s.media_id)
       )
    when matched then update set t.activity_media_name =s.media_name, t.coop_yes_no =s.coop_yes_no;
   --
  exception
   when others then
    rollback to square1;
    print_log ('@pop_accruals_media_name_S, message ='||sqlerrm);
  end pop_accruals_media_name_S;
  --
  procedure pop_accruals_fund_name_S is
  begin
   --
   savepoint square1;
   --
    merge /*+ PARALLEL (t 8) */ into xxcus.xxcus_ozf_xla_accruals_b t
    using (
            select short_name
                  ,fund_id fund_id
            from   ozf_funds_all_tl
            where  1 =1
           ) s
    on ((t.xaeh_period_name =l_period or t.xaeh_period_name ='NA') and t.fund_id = s.fund_id)
    when matched then update set t.fund_name =s.short_name;
   --
  exception
   when others then
    rollback to square1;
    print_log ('@pop_accruals_fund_name_S, message ='||sqlerrm);
  end pop_accruals_fund_name_S;
  --
  procedure pop_accruals_mvid_info_S is
  begin
   --
   savepoint square1;
   --
    merge /*+ PARALLEL (t 8) */ into xxcus.xxcus_ozf_xla_accruals_b t
    using (
            select hzca.attribute2      attribute2
                  ,hzp.party_name       party_name
                  ,hzca.cust_account_id cust_account_id
            from   hz_cust_accounts hzca
                  ,hz_parties       hzp
            where  1 =1
              and  hzp.party_id =hzca.party_id
           ) s
    on (
          ((t.xaeh_period_name =l_period or t.xaeh_period_name ='NA') and t.ofu_cust_account_id = s.cust_account_id)
        OR
          ((NVL(t.xaeh_gl_xfer_status_code, 'Z') <>'Y') and t.ofu_cust_account_id = s.cust_account_id)
       )
    when matched then update set t.ofu_mvid =s.attribute2, t.ofu_mvid_name =s.party_name;
   --
  exception
   when others then
    rollback to square1;
    print_log ('@pop_accruals_mvid_info_S, message ='||sqlerrm);
  end pop_accruals_mvid_info_S;
  --
  procedure pop_branch_info_S is
  begin
   --
   savepoint square1;
   --
    merge /*+ PARALLEL (t 8) */ into xxcus.xxcus_ozf_xla_accruals_b t
    using (
            select hzca.attribute2      attribute2
                  ,hzp.party_name       party_name
                  ,hzca.account_number  account_number
                  ,hzca.cust_account_id cust_account_id
            from   hz_cust_accounts hzca
                  ,hz_parties       hzp
            where  1 =1
              and  hzp.party_id =hzca.party_id
           ) s
    on (
           ((t.xaeh_period_name =l_period or t.xaeh_period_name ='NA') and t.billto_cust_acct_id_br = s.cust_account_id)
        OR
          ((NVL(t.xaeh_gl_xfer_status_code, 'Z') <>'Y') and t.billto_cust_acct_id_br = s.cust_account_id)
       )
    when matched then update set t.billto_cust_acct_br_name =s.party_name, t.billto_cust_acct_br_acct_name =s.account_number;
   --
  exception
   when others then
    rollback to square1;
    print_log ('@pop_branch_info_S, message ='||sqlerrm);
  end pop_branch_info_S;
  --
  /* -- TMS 20151217-00142
  Procedure: pop_posted_fin_loc_info_S
  Called by: extract_accruals_single
  Parameters: None
  History:
  =======
  --
  Ticket                                  Date                      Author                                                              Description
  ===========                    =============  ===============================     ===================================================
                                                                                                                                                        Header Missing
  TMS 20151217-00142  12/15/2015       Balaguru Seshadri                                       Add resale_line_attribute12 to where clause in merge statement
  --
  */ -- TMS 20151217-00142
  --
  procedure pop_posted_fin_loc_info_S is
  begin
   --
   savepoint square1;
   --
    merge /*+ PARALLEL (t 8) */ into xxcus.xxcus_ozf_xla_accruals_b t
    using (
            select distinct trim(attribute4) product, attribute5 override_fin_loc, nvl(attribute11, 'N') override_fin_loc_y_n
            from   ar.hz_parties p
            where  1 =1
              and  attribute1 = 'HDS_BU'
              and  (attribute4 is not null and attribute5 is not null)
           ) s
    on (
         (
               (t.xaeh_period_name =l_period or t.xaeh_period_name ='NA')
           and t.resale_line_attribute11 =s.product
           and t.resale_line_attribute12 =s.override_fin_loc --TMS 20151217-00142
           and t.coop_yes_no             ='Y'
           and s.override_fin_loc_y_n    ='Y'
         )
        OR
          (
                 (NVL(t.xaeh_gl_xfer_status_code, 'Z') <>'Y')
             and t.resale_line_attribute11 =s.product
             and t.resale_line_attribute12 =s.override_fin_loc --TMS 20151217-00142
             and t.coop_yes_no             ='Y'
             and s.override_fin_loc_y_n    ='Y'
          )
       )
    when matched
     then update set t.posted_fin_loc =s.override_fin_loc, t.override_fin_loc =s.override_fin_loc_y_n;
   --
  exception
   when others then
    rollback to square1;
    print_log ('@pop_posted_fin_loc_info_S, message ='||sqlerrm);
  end pop_posted_fin_loc_info_S;
  --
  procedure pop_posted_bu_branch_info_S is
  begin
   --
   savepoint square1;
   --
    merge /*+ PARALLEL (t 8) */ into xxcus.xxcus_ozf_xla_accruals_b t
    using (
            select /*+ RESULT_CACHE */
                   xlcv.entrp_loc     entrp_loc
                  ,xlcv.lob_branch    lob_branch
                  ,xrbxt.rebates_fru  rebates_fru
            from apps.xxcus_location_code_vw xlcv
                ,(
                   select a.rebates_fru
                   from   xxcus.xxcus_rebate_branch_xref_tbl a
                   where  1 =1
                     and  rowid =
                            (
                              select min(rowid)
                              from   xxcus.xxcus_rebate_branch_xref_tbl b
                              where  1 =1
                                and  b.rebates_fru =a.rebates_fru
                            )
                 ) xrbxt
                ,fnd_lookup_values_vl lv
            where 1 =1
              and xrbxt.rebates_fru = xlcv.fru
              and lv.lookup_type = 'XXCUS_BUSINESS_UNIT'
              and xlcv.business_unit = lv.lookup_code
            order by 1, 2
           ) s
    on (
          (
               (t.xaeh_period_name =l_period or t.xaeh_period_name ='NA')
            and t.posted_fin_loc   =s.entrp_loc
            and t.coop_yes_no      ='Y'
            and t.override_fin_loc ='Y'
          )
        OR
          (
                 (NVL(t.xaeh_gl_xfer_status_code, 'Z') <>'Y')
            and t.posted_fin_loc   =s.entrp_loc
            and t.coop_yes_no      ='Y'
            and t.override_fin_loc ='Y'
          )
       )
    when matched
     then update set t.posted_bu_branch =s.lob_branch;
   --
  exception
   when others then
    rollback to square1;
    print_log ('@pop_posted_bu_branch_info_S, message ='||sqlerrm);
  end pop_posted_bu_branch_info_S;
  --
  begin --Main Processing
    --
    execute immediate 'ALTER SESSION FORCE PARALLEL DML';
    execute immediate 'ALTER TABLE XXCUS.XXCUS_OZF_XLA_ACCRUALS_B PARALLEL';
    execute immediate 'ALTER TABLE XXCUS.XXCUS_OZF_XLA_ACCRUALS_B NOLOGGING';

    print_log ('');
    print_log('Note: Current session and the custom table are parallel enabled.');
    print_log ('');
    print_log ('Parameter, p_period ='||p_period);
    print_log ('Parameter, p_account ='||p_account);
    print_log ('');
    --
     begin
      --
      begin
        select 'DELETE /* PARALLEL (T 8) */ XXCUS.XXCUS_OZF_XLA_ACCRUALS_B partition ('
             ||UPPER(SUBSTR(p_period, 1, 3))
             ||') T '
             ||'
        '
        ||' WHERE 1 =1 '
        ||'
        '
        ||' AND T.XAEH_PERIOD_NAME ='||''''||p_period||''''
        into v_del_sql
        from dual;
        --
        execute immediate v_del_sql;
        --
      exception
       when others then
        Null;
      end;
      --DELETE /* PARALLEL (T 8) */ XXCUS.XXCUS_OZF_XLA_ACCRUALS_B T
      --WHERE  1 =1
      --  AND  fiscal_month     =UPPER(SUBSTR(p_period, 1, 3)) --Required bcoz this is the partition name
      --  AND  xaeh_period_name =p_period;
      --
      if nvl(SQL%ROWCOUNT, 0) =0 then
       print_log ('Action -Cleanup: No TM accrual entries deleted '||
                  'using partition ='||Upper(Substr(p_period, 1, 3)) ||
                  ', period =' ||p_period ||
                  ' from table xxcus.xxcus_ozf_xla_accruals_b'
                 );
      else
       print_log ('Action -Cleanup: Successfully deleted '|| SQL%ROWCOUNT ||
                  ' existing TM accrual entries ' ||
                  ' using partition ='||Upper(Substr(p_period, 1, 3)) ||
                  ', period =' ||p_period ||
                  ' from table xxcus.xxcus_ozf_xla_accruals_b'
                 );
      end if;
      --
      COMMIT;
      --
      DELETE /* PARALLEL (T 8) */ XXCUS.XXCUS_OZF_XLA_ACCRUALS_B partition (NA) T;
      --
      if nvl(SQL%ROWCOUNT, 0) =0 then
       print_log ('Action -Cleanup: No TM accrual entries deleted '||
                  'using partition =NA'||
                  ' from table xxcus.xxcus_ozf_xla_accruals_b'
                 );
      else
       print_log ('Action -Cleanup: Successfully deleted '|| SQL%ROWCOUNT ||
                  ' existing TM accrual entries ' ||
                  ' using partition =NA'||
                  ' from table xxcus.xxcus_ozf_xla_accruals_b'
                 );
      end if;
      --
      COMMIT;
      --
      DELETE /* PARALLEL (T 8) */ XXCUS.XXCUS_OZF_XLA_ACCRUALS_B T
      WHERE  1 =1
        AND  NVL(xaeh_gl_xfer_status_code, 'Z') <>'Y';
        --AND  xaeh_gl_xfer_status_code IS NULL;
      --
      if nvl(SQL%ROWCOUNT, 0) =0 then
       print_log ('Action -Cleanup: No TM accrual entries deleted '||
                  'using partition ALL'||
                  ' from table xxcus.xxcus_ozf_xla_accruals_b AND xaeh_gl_xfer_status_code IS NULL'
                 );
      else
       print_log ('Action -Cleanup: Successfully deleted '|| SQL%ROWCOUNT ||
                  ' existing TM accrual entries ' ||
                  ' using partition ALL'||
                  ' from table xxcus.xxcus_ozf_xla_accruals_b AND xaeh_gl_xfer_status_code IS NULL'
                 );
      end if;
      --
      COMMIT;
      --
      b_proceed :=TRUE;
      --
      print_log ('Successfully removed old data removed from table xxcus.xxcus_ozf_xla_accruals_b');
      --
     exception
      when others then
       b_proceed :=FALSE;
       print_log (''||sqlerrm);
     end;
    --
    if (b_proceed) then
     --
     begin
      --
        INSERT /*+ APPEND PARALLEL(t 8) */ INTO XXCUS.XXCUS_OZF_XLA_ACCRUALS_B t
        (
            SELECT /*+ PARALLEL(oxa 8) PARALLEL(offers 8) PARALLEL(qph 8) PARALLEL(ofu 8) PARALLEL(rslines 8) */
                   oxa.xla_accrual_id                 xla_accrual_id
                  ,ofu.request_id                     ofu_request_id
                  ,utilization_type                   utilization_type
                  ,ofu.adjustment_type                adjustment_type
                  ,ofu.adjustment_type_id             adjustment_type_id
                  ,ofu.object_id                      object_id
                  ,ofu.plan_curr_amount               rebate_amount
                  ,ofu.acctd_amount                   util_acctd_amount
                  ,oxa.event_type_code                event_type_code
                  ,oxa.entity_code                    entity_code
                  ,oxa.event_id                       event_id
                  ,oxa.utilization_id                 utilization_id
                  ,oxa.org_id                         oxa_org_id
                  ,trunc(ofu.gl_date)                 ofu_gl_date
                  ,hrou.set_of_books_id               ozf_ledger_id
                  ,case
                    when hrou.set_of_books_id =2021 then 'HDS Rebates USA'
                    when hrou.set_of_books_id =2023 then 'HDS Rebates CAN'
                    else Null
                   end                                ozf_ledger_name
                  ,xlae.entity_id                     xlae_entity_id
                  ,trunc(xlae.transaction_date)       xlae_trxn_date
                  ,trunc(xlae.event_date)             xlae_event_date
                  ,xlae.event_status_code             xlae_event_status_code
                  ,xlae.process_status_code           xlae_process_status_code
                  ,xlae.event_number                  xlae_event_number
                  ,xlae.on_hold_flag                  xlae_onhold_flag
                  ,xlae.application_id                xla_application_id
                  ,xaeh.ae_header_id                  xaeh_ae_header_id
                  ,xaeh.accounting_date               xaeh_accounting_date
                  ,xaeh.gl_transfer_status_code       xaeh_gl_xfer_status_code
                  ,trunc(xaeh.gl_transfer_date)       xaeh_gl_xfer_date
                  ,xaeh.je_category_name              xaeh_je_category
                  ,nvl(SUBSTR(  UPPER(xaeh.period_name)
                           ,1
                           ,3
                         ), 'NA')                     fiscal_month
                  ,nvl(xaeh.period_name, 'NA')        xaeh_period_name
                  ,xerr.accounting_error_id           accounting_error_id
                  ,xerr.accounting_batch_id           accounting_batch_id
                  ,xerr.encoded_msg                   encoded_msg
                  ,xerr.ae_line_num                   err_ae_line_num
                  ,xerr.message_number                message_number
                  ,xerr.error_source_code             error_source_code
                  ,offers.offer_id                    offer_id
                  ,offers.offer_code                  offer_code
                  ,qphtl.description                  offer_name
                  ,offers.offer_type                  offer_type
                  ,offers.autopay_party_id            autopay_party_id
                  ,offers.beneficiary_account_id      beneficiary_account_id
                  ,offers.status_code                 offer_status_code
                  ,offers.custom_setup_id             offer_custom_setup_id
                  ,offers.user_status_id              offer_user_status_id
                  ,offers.owner_id                    offer_owner_id
                  ,offers.activity_media_id           activity_media_id
                  --,Null                               activity_media_name
                  ,ams.media_name                     activity_media_name
                  ,offers.autopay_flag                offer_autopay_flag
                  ,ofu.currency_code                  ofu_currency_code
                  ,rslines.currency_code              receipts_currency
                  ,qph.attribute1                     auto_renewal
                  ,qph.attribute7                     calendar_year
                  ,qph.attribute2                     until_year
                  ,qph.attribute5                     payment_method
                  ,qph.attribute6                     payment_frequency
                  ,qph.attribute9                     guaranteed_amount
                  ,ofu.plan_id                        qp_list_header_id
                  ,qph.global_flag                    qp_global_flag
                  ,qph.active_flag                    qp_active_flag
                  ,qph.orig_org_id                    qp_orig_org_id
                  ,ofu.year_id                        ofu_year_id
                  ,ofu.fund_id                        fund_id
                  --,Null                               fund_name
                  ,(
                    select /*+ RESULT_CACHE */
                           short_name
                    from   ozf_funds_all_tl
                    where  1 =1
                      and  fund_id =ofu.fund_id
                   )                                  fund_name
                  ,ofu.plan_type                      plan_type
                  ,ofu.component_id                   component_id
                  ,ofu.component_type                 component_type
                  ,ofu.object_type                    object_type
                  ,ofu.reference_type                 reference_type
                  ,ofu.reference_id                   reference_id
                  ,ofu.ams_activity_budget_id         ams_activity_budget_id
                  ,ofu.product_level_type             product_level_type
                  ,ofu.product_id                     product_id
                  ,rslines.creation_date              resale_date_created
                  ,trunc(rslines.date_invoiced)       date_invoiced
                  ,rslines.po_number                  po_number
                  ,rslines.purchase_uom_code          purchase_uom_code
                  ,rslines.uom_code                   uom_code
                  ,rslines.purchase_price             purchase_price
                  ,ofu.cust_account_id                ofu_cust_account_id
                  ,Null                               ofu_mvid
                  ,Null                               ofu_mvid_name
                  ,rslines.bill_to_cust_account_id    billto_cust_acct_id_br
                  ,Null                               billto_cust_acct_br_name
                  ,Null                               billto_cust_acct_br_acct_name
                  ,rslines.bill_to_party_id           bill_to_party_id
                  ,rslines.bill_to_party_name         bill_to_party_name_lob
                  ,trunc(rslines.date_ordered)        date_ordered
                  ,rslines.selling_price              selling_price
                  ,rslines.quantity                   quantity
                  ,rslines.inventory_item_id          inventory_item_id
                  ,rslines.item_number                item_number
                  ,rslines.line_attribute1            resale_line_attribute1 --pay_to_vendor_code
                  ,rslines.line_attribute2            resale_line_attribute2 --bu_nm
                  ,rslines.line_attribute3            resale_line_attribute3 --src_sys_nm
                  ,rslines.line_attribute5            resale_line_attribute5 --receipt_number
                  ,rslines.line_attribute6            resale_line_attribute6 --ship_to_branch_code
                  ,rslines.line_attribute11           resale_line_attribute11 --product_segment
                  ,rslines.line_attribute12           resale_line_attribute12 --location_segment
                  ,rslines.line_attribute13           resale_line_attribute13 --freight_discount
                  ,rslines.line_attribute14           resale_line_attribute14 --payment_discount
                  ,ofu.price_adjustment_id            pricing_adj_id
                  ,trunc(ofu.exchange_rate_date)      ofu_exchange_rate_date
                  ,ofu.exchange_rate                  ofu_exchange_rate
                  ,ofu.billto_cust_account_id         ofu_billto_cust_account_id_br
                  ,ofu.product_id                     ofu_product_id
                  ,rslines.sold_from_cust_account_id  sold_from_cust_account_id_mvid
                  ,rslines.sold_from_site_id          sold_from_site_id_mvid
                  ,rslines.sold_from_party_name       sold_from_party_name_mvid
                  ,rslines.line_attribute12           posted_fin_loc
                  --,null                               coop_yes_no
                  ,ams.coop_yes_no                    coop_yes_no
                  ,rslines.line_attribute6            posted_bu_branch
                  ,'N'                                override_fin_loc
                  ,ofu.attribute10                    ofu_attribute10
                  ,trunc(ofu.creation_date)           ofu_creation_date
            from   ozf_xla_accruals oxa
                  ,hr_operating_units        hrou
                  ,ozf_funds_utilized_all_b  ofu
                  ,xla_events                xlae
                  ,xla_ae_headers            xaeh
                  ,xla.xla_accounting_errors xerr
                  ,ozf_offers                offers
                  ,qp_list_headers_b         qph
                  ,qp_list_headers_tl        qphtl
                  ,ozf_resale_lines_all      rslines
                  ,(
                    select amstl.media_name media_name
                          ,amstl.media_id
                          ,case
                            when amstl.description IN ('COOP', 'COOP_MIN') then 'Y'
                            else 'N'
                           end   coop_yes_no
                    from   ams_media_tl amstl
                          ,ams_media_b  ams
                    where  1 =1
                      and  ams.media_id =amstl.media_id
                   ) ams
            where  1 =1
              and  hrou.organization_id    =oxa.org_id
              and  ofu.utilization_id      =oxa.utilization_id
              and  ofu.org_id              =oxa.org_id
              and  xlae.event_id           =oxa.event_id
              and  xlae.application_id     =682
              and  xaeh.event_id(+)        =xlae.event_id
              and  xaeh.application_id(+)  =xlae.application_id
              --and  ((xaeh.period_name  =p_period) OR (xaeh.period_name IS NULL) OR (xaeh.gl_transfer_status_code IS NULL))
              and  ((xaeh.period_name  =p_period) OR (xaeh.period_name IS NULL) OR NVL(xaeh.gl_transfer_status_code, 'Z') <>'Y')
              and  xerr.event_id(+)        =xlae.event_id
              and  qphtl.list_header_id         =ofu.plan_id
              and  offers.qp_list_header_id     =qphtl.list_header_id
              and  qph.list_header_id           =qphtl.list_header_id
              and  rslines.resale_line_id(+)    =ofu.object_id
              and  ams.media_id(+)              =offers.activity_media_id
        ); --End of insert statement
      --
      print_log ('Total records inserted into the table xxcus.xxcus_ozf_xla_accruals_b =>'||SQL%ROWCOUNT);
      --
      Commit;
      --
     exception
      when others then
       print_log('Error in insert of OZF XLA Accruals, Message =>'||sqlerrm);
       b_proceed :=FALSE;
     end;
     --
    else
     print_log('Issue in deleting period based data from XXCUS.XXCUS_OZF_XLA_ACCRUALS_B, message =>'||sqlerrm);
    end if;
    --
    begin
     --
     l_period :=p_period;
     --
     --pop_accruals_ledger_name_S; --Commented out bcoz the main insert takes care of this and is a lot faster.
     --Commit;
     --print_log ('@ update of accrual attributes, Exit: pop_accruals_ledger_name_single');
     --v_loc :='101';
     --pop_accruals_media_name_S; --Commented out bcoz the main insert takes care of this as we have issues with the untransferred records
     --Commit;
     --print_log ('@ update of accrual attributes, Exit: pop_accruals_media_name_single');
     --v_loc :='102';
     --pop_accruals_fund_name_S;  --Commented out bcoz the main insert takes care of this and is a lot faster.
     --Commit;
     --print_log ('@ update of accrual attributes, Exit: pop_accruals_fund_name_single');
     v_loc :='103';
     pop_accruals_mvid_info_S;
     Commit;
     print_log ('@ update of accrual attributes, Exit: pop_accruals_mvid_info_single');
     v_loc :='104';
     pop_branch_info_S;
     Commit;
     print_log ('@ update of accrual attributes, Exit: pop_branch_info_single');
     v_loc :='105';
     pop_posted_fin_loc_info_S;
     Commit;
     print_log ('@ update of accrual attributes, Exit: pop_posted_fin_loc_info_single');
     v_loc :='106';
     pop_posted_bu_branch_info_S;
     Commit;
     v_loc :='107';
     print_log ('@ update of accrual attributes, Exit: pop_posted_bu_branch_info_single');
     --
    exception
     when others then
      print_log ('@Accruals attributes update single, last known location ='||v_loc||', Error =>'||sqlerrm);
    end;
    --
    begin
     --
     print_log ('Begin rebuild of partitioned indexes...');
     --
     for rec in rebuild_partitioned_indexes (p_table =>'XXCUS_OZF_XLA_ACCRUALS_B') loop
      --
       v_sql :=rec.idx_sql;
      --
        execute immediate rec.idx_sql;
      --
       --print_log ('Rebuild SQL '||rec.idx_sql||' was executed successfully');
      --
     end loop;
     --
     print_log ('End rebuild of partitioned indexes...');
     --
    exception
     when others then
      print_log ('Issue in rebuild of partitioned index, current sql =>'||v_sql);
    end;
    --
    --execute immediate 'ALTER SESSION DISABLE PARALLEL DML';
    --execute immediate 'ALTER TABLE XXCUS.XXCUS_OZF_XLA_ACCRUALS_B NOPARALLEL';
    --
    -- call routine to extract ytd income
    --
    --print_log('');
    --print_log('Before calling xxcus_ozf_tm_recon.extract_ytd_income for period ='||l_period);
    /*
    xxcus_ozf_tm_recon.extract_ytd_income
      (
        retcode  =>v_retcode
       ,errbuf   =>v_errbuf
       ,p_period =>l_period
      );
    */
    --
    --print_log('');
    --print_log('After calling xxcus_ozf_tm_recon.extract_ytd_income for period ='||l_period);
    --
  exception
   when others then
    print_log ('Issue in extract_accruals_single routine, message ='||sqlerrm);
    rollback;
  end extract_accruals_single;
  --
  -- Begin TMS 20160316-00196
  procedure extract_rebate_accruals
  (
    retcode               out number
   ,errbuf                out varchar2
   ,p_period              in  varchar2
   ,p_account             in  varchar2
  ) is
  --
   v_sql VARCHAR2(240) :=Null;
   N_Count NUMBER;
   b_proceed BOOLEAN;
   --
   v_retcode varchar2(240) :=Null;
   v_errbuf  varchar2(240) :=Null;
   --
   v_del_sql varchar2(2000) :=Null;
   v_merge_sql varchar2(4000) :=Null;
   --
   b_backup  BOOLEAN;
   v_loc     varchar2(3);
   l_period  varchar2(10) :=Null;
   l_prev_period varchar2(10) :=Null;
   l_prev_period_gl_xfer_pending BOOLEAN :=FALSE;
   l_count NUMBER :=0;
    --
   cursor rebuild_partitioned_indexes (p_table in varchar2, p_current_period_partition in varchar2, p_prev_period_partition in varchar2) is
    select  'ALTER INDEX '||owner||'.'||uip.index_name||' REBUILD PARTITION '||uip.partition_name||' PARALLEL 4' idx_sql
    from dba_ind_partitions  uip
        ,dba_tab_partitions  utp
        ,dba_indexes         ui
    where 1 =1
      and uip.partition_name  =utp.partition_name
      and ui.index_name       =uip.index_name
      and ui.table_name       =utp.table_name
      and ui.owner            ='XXCUS'
      and utp.table_name      =p_table
      and  uip.partition_name =p_current_period_partition
    union all
    select 'ALTER INDEX '||owner||'.'||uip.index_name||' REBUILD PARTITION '||uip.partition_name||' PARALLEL 4' idx_sql
    from dba_ind_partitions  uip
        ,dba_tab_partitions  utp
        ,dba_indexes         ui
    where 1 =1
      and uip.partition_name  =utp.partition_name
      and ui.index_name       =uip.index_name
      and ui.table_name       =utp.table_name
      and ui.owner            ='XXCUS'
      and utp.table_name      =p_table
      and  uip.partition_name =p_prev_period_partition
    union all
    select  'ALTER INDEX '||owner||'.'||uip.index_name||' REBUILD PARTITION '||uip.partition_name||' PARALLEL 4' idx_sql
    from dba_ind_partitions  uip
        ,dba_tab_partitions  utp
        ,dba_indexes         ui
    where 1 =1
      and uip.partition_name  =utp.partition_name
      and ui.index_name       =uip.index_name
      and ui.table_name       =utp.table_name
      and ui.owner            ='XXCUS'
      and utp.table_name      =p_table
      and  uip.partition_name ='NA';
  --
    cursor remove_parallel_on_ozf_objects is
    select tbl.*
               ,'ALTER '||OBJECT_TYPE||' '||OBJECT_NAME||' NOPARALLEL;' sql_text
    from
    (
    select a.owner||'.'||a.index_name object_name
               ,'INDEX' object_type
               ,(
                     case
                      when a.degree ='DEFAULT' then '99'
                      else trim(degree)
                     end
               ) degree
    from all_indexes a
    where 1 =1
    and index_name like '%OZF%'
    union all
    select a.owner||'.'||a.table_name object__name
            ,'TABLE'
               ,(
                     case
                      when a.degree ='DEFAULT' then '99'
                      else trim(degree)
                     end
               ) degree
    from   all_tables a
    where 1 =1
      and table_name LIKE '%OZF%' and table_name !='XXCUS_OZF_XLA_ACCRUALS_B'
      ) tbl
    where 1 =1
    and tbl.degree <>'1'  --when not equals one it has parallellism. we need to make sure parallellism is off when index rebuilds or table dml is done
    order by 2 desc, 1 asc;
  --
  procedure pop_accruals_mvid_info_S is
  begin
   --
    v_merge_sql :='Merge /*+ PARALLEL (M1 8) */ into xxcus.xxcus_ozf_xla_accruals_b partition ('||UPPER(SUBSTR(l_period, 1, 3))||') M1 '||'
    '||' using (
            select hzca.attribute2      attribute2
                  ,hzp.party_name       party_name
                  ,hzca.cust_account_id cust_account_id
            from   hz_cust_accounts hzca
                  ,hz_parties       hzp
            where  1 =1
              and  hzp.party_id =hzca.party_id
           ) s
    on (
          (M1.xaeh_period_name ='||''''||l_period||''''||' and M1.ofu_cust_account_id = s.cust_account_id)
       )
    when matched then update set M1.ofu_mvid =s.attribute2, M1.ofu_mvid_name =s.party_name';
    --
   print_log('M1 SQL: ');
   print_log('========');
   print_log(v_merge_sql);
   print_log(' ');
   --
    print_log ('Begin Merge 1: Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
    execute immediate 'begin ' || v_merge_sql || '; :x := sql%rowcount; end;' using OUT l_count;
    print_log ('End Merge 1: Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
    --
   print_log('Merge M1 complete:  total records :'||l_count);
   --
   commit;
   --
    v_merge_sql :='Merge /*+ PARALLEL (M2 8) */ into xxcus.xxcus_ozf_xla_accruals_b partition (NA) M2 '||'
    '||' using (
            select hzca.attribute2      attribute2
                  ,hzp.party_name       party_name
                  ,hzca.cust_account_id cust_account_id
            from   hz_cust_accounts hzca
                  ,hz_parties       hzp
            where  1 =1
              and  hzp.party_id =hzca.party_id
           ) s
    on (
          (M2.xaeh_period_name ='||''''||'NA'||''''||' and M2.ofu_cust_account_id = s.cust_account_id)
       )
    when matched then update set M2.ofu_mvid =s.attribute2, M2.ofu_mvid_name =s.party_name';
    --
   print_log('M2 SQL: ');
   print_log('========');
   print_log(v_merge_sql);
   print_log(' ');
   --
    print_log ('Begin Merge 2: Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
    execute immediate 'begin ' || v_merge_sql || '; :x := sql%rowcount; end;' using OUT l_count;
    print_log ('End Merge 2: Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
    --
   print_log('Merge M2 complete:  total records :'||l_count);
   --
   commit;
   --
       if (l_prev_period_gl_xfer_pending) then
        --
            v_merge_sql :='Merge /*+ PARALLEL (M3 8) */ into xxcus.xxcus_ozf_xla_accruals_b partition ('||UPPER(SUBSTR(l_prev_period, 1, 3))||') M3 '||'
            '||' using (
                    select hzca.attribute2      attribute2
                          ,hzp.party_name       party_name
                          ,hzca.cust_account_id cust_account_id
                    from   hz_cust_accounts hzca
                          ,hz_parties       hzp
                    where  1 =1
                      and  hzp.party_id =hzca.party_id
                   ) s
            on (
                  (M3.xaeh_period_name ='||''''||l_prev_period||''''||' and M3.xaeh_gl_xfer_status_code <>''Y'' and M3.ofu_cust_account_id = s.cust_account_id)
               )
            when matched then update set M3.ofu_mvid =s.attribute2, M3.ofu_mvid_name =s.party_name';
            --
           print_log('M3 SQL: ');
           print_log('========');
           print_log(v_merge_sql);
           print_log(' ');
           --
           print_log ('Begin Merge 3: Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
            execute immediate 'begin ' || v_merge_sql || '; :x := sql%rowcount; end;' using OUT l_count;
            print_log ('End Merge 3: Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
            --
           print_log('Merge M3 complete:  total records :'||l_count);
           --
           commit;
        --
       end if;
       --
  exception
   when others then
    print_log ('@pop_accruals_mvid_info_S, message ='||sqlerrm);
  end pop_accruals_mvid_info_S;
  --
  procedure pop_branch_info_S is
  begin
   --
        v_merge_sql :='Merge /*+ PARALLEL (M4 8) */ into xxcus.xxcus_ozf_xla_accruals_b partition ('||UPPER(SUBSTR(l_period, 1, 3))||') M4 '||'
        '||' using (
                        select hzca.attribute2      attribute2
                              ,hzp.party_name       party_name
                              ,hzca.account_number  account_number
                              ,hzca.cust_account_id cust_account_id
                        from   hz_cust_accounts hzca
                              ,hz_parties       hzp
                        where  1 =1
                          and  hzp.party_id =hzca.party_id
               ) s
        on (
              (M4.xaeh_period_name ='||''''||l_period||''''||' and M4.billto_cust_acct_id_br = s.cust_account_id)
           )
        when matched then update set M4.billto_cust_acct_br_name =s.party_name, M4.billto_cust_acct_br_acct_name =s.account_number';
        --
           print_log('M4 SQL: ');
           print_log('========');
           print_log(v_merge_sql);
           print_log(' ');
       --
        print_log ('Begin Merge 4: Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
        execute immediate 'begin ' || v_merge_sql || '; :x := sql%rowcount; end;' using OUT l_count;
        print_log ('End Merge 4: Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
        --
       print_log('Merge M4 complete:  total records :'||l_count);
       --
       commit;
       --
        v_merge_sql :='Merge /*+ PARALLEL (M5 8) */ into xxcus.xxcus_ozf_xla_accruals_b partition (NA) M5 '||'
        '||' using (
                        select hzca.attribute2      attribute2
                              ,hzp.party_name       party_name
                              ,hzca.account_number  account_number
                              ,hzca.cust_account_id cust_account_id
                        from   hz_cust_accounts hzca
                              ,hz_parties       hzp
                        where  1 =1
                          and  hzp.party_id =hzca.party_id
               ) s
        on (
              (M5.xaeh_period_name ='||''''||'NA'||''''||' and M5.billto_cust_acct_id_br = s.cust_account_id)
           )
        when matched then update set M5.billto_cust_acct_br_name =s.party_name, M5.billto_cust_acct_br_acct_name =s.account_number';
        --
           print_log('M5 SQL: ');
           print_log('========');
           print_log(v_merge_sql);
           print_log(' ');
       --
        print_log ('Begin Merge 5: Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
        execute immediate 'begin ' || v_merge_sql || '; :x := sql%rowcount; end;' using OUT l_count;
        print_log ('End Merge 5: Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
        --
       print_log('Merge M5 complete:  total records :'||l_count);
       --
       if (l_prev_period_gl_xfer_pending) then
               --
                v_merge_sql :='Merge /*+ PARALLEL (M6 8) */ into xxcus.xxcus_ozf_xla_accruals_b partition ('||UPPER(SUBSTR(l_prev_period, 1, 3))||') M6 '||'
                '||' using (
                                select hzca.attribute2      attribute2
                                      ,hzp.party_name       party_name
                                      ,hzca.account_number  account_number
                                      ,hzca.cust_account_id cust_account_id
                                from   hz_cust_accounts hzca
                                      ,hz_parties       hzp
                                where  1 =1
                                  and  hzp.party_id =hzca.party_id
                       ) s
                on (
                      (M6.xaeh_period_name ='||''''||l_prev_period||''''||'  and M6.xaeh_gl_xfer_status_code <>''Y'' and M6.billto_cust_acct_id_br = s.cust_account_id)
                   )
                when matched then update set M6.billto_cust_acct_br_name =s.party_name, M6.billto_cust_acct_br_acct_name =s.account_number';
                --
           print_log('M6 SQL: ');
           print_log('========');
           print_log(v_merge_sql);
           print_log(' ');
               --
               print_log ('Begin Merge 6: Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
                execute immediate 'begin ' || v_merge_sql || '; :x := sql%rowcount; end;' using OUT l_count;
                print_log ('End Merge 6: Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
                --
               print_log('Merge M6 complete:  total records :'||l_count);
               --
               commit;
               --
       end if;
   --
  exception
   when others then
    print_log ('@pop_branch_info_S, message ='||sqlerrm);
  end pop_branch_info_S;
  --
  procedure pop_posted_fin_loc_info_S is
  begin
   --
        v_merge_sql :='Merge /*+ PARALLEL (M7 8) */ into xxcus.xxcus_ozf_xla_accruals_b partition ('||UPPER(SUBSTR(l_period, 1, 3))||') M7 '||'
        '||' using (
                             select * from xxcus.xxcus_ozf_override_fin_loc_b
                          ) s
        on (
                 (
                       M7.xaeh_period_name ='||''''||l_period||''''||'
                       and M7.resale_line_attribute2 =s.BU --TMS  20160602-00010 ESMS 321264
                   --and M7.resale_line_attribute11 =s.product --TMS  20160602-00010 ESMS 321264
                   --and M7.resale_line_attribute12 =s.override_fin_loc --TMS  20160602-00010 ESMS 321264
                   and M7.coop_yes_no             =''Y''
                   and s.override_fin_loc_y_n    =''Y''
                 )
           )
        when matched then update set M7.posted_fin_loc =s.override_fin_loc, M7.override_fin_loc =s.override_fin_loc_y_n';
        --
           print_log('M7 SQL: ');
           print_log('========');
           print_log(v_merge_sql);
           print_log(' ');
       --
       print_log ('Begin Merge 7: Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
        execute immediate 'begin ' || v_merge_sql || '; :x := sql%rowcount; end;' using OUT l_count;
        print_log ('End Merge 7: Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
        --
       print_log('Merge M7 complete:  total records :'||l_count);
       --
       commit;
       --
        v_merge_sql :='Merge /*+ PARALLEL (M8 8) */ into xxcus.xxcus_ozf_xla_accruals_b partition (NA) M8 '||'
        '||' using (
                              select * from xxcus.xxcus_ozf_override_fin_loc_b
                          ) s
              on (
                              M8.xaeh_period_name ='||''''||'NA'||''''||'
                      and M8.resale_line_attribute2 =s.BU --TMS  20160602-00010 ESMS 321264
                      --and M8.resale_line_attribute11 =s.product --TMS  20160602-00010 ESMS 321264
                      --and M8.resale_line_attribute12 =s.override_fin_loc --TMS  20160602-00010 ESMS 321264
                      and M8.coop_yes_no             =''Y''
                     and s.override_fin_loc_y_n    =''Y''
                  )
        when matched then update set M8.posted_fin_loc =s.override_fin_loc, M8.override_fin_loc =s.override_fin_loc_y_n';
        --
           print_log('M8 SQL: ');
           print_log('========');
           print_log(v_merge_sql);
           print_log(' ');
       --
       print_log ('Begin Merge 8: Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
        execute immediate 'begin ' || v_merge_sql || '; :x := sql%rowcount; end;' using OUT l_count;
        print_log ('End Merge 8: Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
        --
       print_log('Merge M8 complete:  total records :'||l_count);
       --
       commit;
       --
       if (l_prev_period_gl_xfer_pending) then
            --
            v_merge_sql :='Merge /*+ PARALLEL (M9 8) */ into xxcus.xxcus_ozf_xla_accruals_b partition ('||UPPER(SUBSTR(l_prev_period, 1, 3))||') M9 '||'
            '||' using (
                                   select * from xxcus.xxcus_ozf_override_fin_loc_b
                              ) s
            on (
                     (
                               M9.xaeh_period_name ='||''''||l_prev_period||''''||'
                       and M9.resale_line_attribute2 =s.BU --TMS  20160602-00010 ESMS 321264
                       --and M9.resale_line_attribute11 =s.product --TMS  20160602-00010 ESMS 321264
                       --and M9.resale_line_attribute12 =s.override_fin_loc --TMS  20160602-00010 ESMS 321264
                       and M9.coop_yes_no             =''Y''
                       and s.override_fin_loc_y_n    =''Y''
                       and M9.xaeh_gl_xfer_status_code <>''Y''
                     )
               )
            when matched then update set M9.posted_fin_loc =s.override_fin_loc, M9.override_fin_loc =s.override_fin_loc_y_n';
            --
           print_log('M9 SQL: ');
           print_log('========');
           print_log(v_merge_sql);
           print_log(' ');
           --
           print_log ('Begin Merge 9: Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
            execute immediate 'begin ' || v_merge_sql || '; :x := sql%rowcount; end;' using OUT l_count;
            print_log ('End Merge 9: Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
            --
           print_log('Merge M9 complete:  total records :'||l_count);
           --
           commit;
           --
       end if;
   --
  exception
   when others then
    rollback to square1;
    print_log ('@pop_posted_fin_loc_info_S, message ='||sqlerrm);
  end pop_posted_fin_loc_info_S;
  --
  procedure pop_posted_bu_branch_info_S is
  begin
        --
        v_merge_sql :='Merge /*+ PARALLEL (M10 8) */ into xxcus.xxcus_ozf_xla_accruals_b partition ('||UPPER(SUBSTR(l_period, 1, 3))||') M10 '||'
        '||' using (
                                select /*+ RESULT_CACHE */
                                       xlcv.entrp_loc     entrp_loc
                                      ,xlcv.lob_branch    lob_branch
                                      ,xrbxt.rebates_fru  rebates_fru
                                from apps.xxcus_location_code_vw xlcv
                                    ,(
                                       select a.rebates_fru
                                       from   xxcus.xxcus_rebate_branch_xref_tbl a
                                       where  1 =1
                                         and  rowid =
                                                (
                                                  select min(rowid)
                                                  from   xxcus.xxcus_rebate_branch_xref_tbl b
                                                  where  1 =1
                                                    and  b.rebates_fru =a.rebates_fru
                                                )
                                     ) xrbxt
                                    ,fnd_lookup_values_vl lv
                                where 1 =1
                                  and xrbxt.rebates_fru = xlcv.fru
                                  and lv.lookup_type = ''XXCUS_BUSINESS_UNIT''
                                  and xlcv.business_unit = lv.lookup_code
                                order by 1, 2
                          ) s
        on (
                            M10.xaeh_period_name ='||''''||l_period||''''||'
                    and M10.posted_fin_loc   =s.entrp_loc
                    and M10.coop_yes_no      =''Y''
                    and M10.override_fin_loc =''Y''
           )
        when matched then update set M10.posted_bu_branch =s.lob_branch';
        --
           print_log('M10 SQL: ');
           print_log('========');
           print_log(v_merge_sql);
           print_log(' ');
       --
       print_log ('Begin Merge 10: Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
        execute immediate 'begin ' || v_merge_sql || '; :x := sql%rowcount; end;' using OUT l_count;
        print_log ('End Merge 10: Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
        --
       print_log('Merge M10 complete:  total records :'||l_count);
       --
       commit;
       --
        v_merge_sql :='Merge /*+ PARALLEL (M11 8) */ into xxcus.xxcus_ozf_xla_accruals_b partition (NA) M11 '||'
        '||' using (
                                select /*+ RESULT_CACHE */
                                       xlcv.entrp_loc     entrp_loc
                                      ,xlcv.lob_branch    lob_branch
                                      ,xrbxt.rebates_fru  rebates_fru
                                from apps.xxcus_location_code_vw xlcv
                                    ,(
                                       select a.rebates_fru
                                       from   xxcus.xxcus_rebate_branch_xref_tbl a
                                       where  1 =1
                                         and  rowid =
                                                (
                                                  select min(rowid)
                                                  from   xxcus.xxcus_rebate_branch_xref_tbl b
                                                  where  1 =1
                                                    and  b.rebates_fru =a.rebates_fru
                                                )
                                     ) xrbxt
                                    ,fnd_lookup_values_vl lv
                                where 1 =1
                                  and xrbxt.rebates_fru = xlcv.fru
                                  and lv.lookup_type = ''XXCUS_BUSINESS_UNIT''
                                  and xlcv.business_unit = lv.lookup_code
                                order by 1, 2
                          ) s
        on (
                            M11.xaeh_period_name ='||''''||'NA'||''''||'
                    and M11.posted_fin_loc   =s.entrp_loc
                    and M11.coop_yes_no      =''Y''
                    and M11.override_fin_loc =''Y''
           )
        when matched then update set M11.posted_bu_branch =s.lob_branch';
        --
           print_log('M11 SQL: ');
           print_log('========');
           print_log(v_merge_sql);
           print_log(' ');
       --
       print_log ('Begin Merge 11: Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
        execute immediate 'begin ' || v_merge_sql || '; :x := sql%rowcount; end;' using OUT l_count;
        print_log ('End Merge 11: Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
        --
       print_log('Merge M11 complete:  total records :'||l_count);
       --
       commit;
       --
       if (l_prev_period_gl_xfer_pending) then
        --
                v_merge_sql :='Merge /*+ PARALLEL (M12 8) */ into xxcus.xxcus_ozf_xla_accruals_b partition ('||UPPER(SUBSTR(l_prev_period, 1, 3))||') M12 '||'
                '||' using (
                                        select /*+ RESULT_CACHE */
                                               xlcv.entrp_loc     entrp_loc
                                              ,xlcv.lob_branch    lob_branch
                                              ,xrbxt.rebates_fru  rebates_fru
                                        from apps.xxcus_location_code_vw xlcv
                                            ,(
                                               select a.rebates_fru
                                               from   xxcus.xxcus_rebate_branch_xref_tbl a
                                               where  1 =1
                                                 and  rowid =
                                                        (
                                                          select min(rowid)
                                                          from   xxcus.xxcus_rebate_branch_xref_tbl b
                                                          where  1 =1
                                                            and  b.rebates_fru =a.rebates_fru
                                                        )
                                             ) xrbxt
                                            ,fnd_lookup_values_vl lv
                                        where 1 =1
                                          and xrbxt.rebates_fru = xlcv.fru
                                          and lv.lookup_type = ''XXCUS_BUSINESS_UNIT''
                                          and xlcv.business_unit = lv.lookup_code
                                        order by 1, 2
                                  ) s
                on (
                                    M12.xaeh_period_name ='||''''||l_prev_period||''''||'
                            and M12.posted_fin_loc   =s.entrp_loc
                            and M12.coop_yes_no      =''Y''
                            and M12.override_fin_loc =''Y''
                            and M12.xaeh_gl_xfer_status_code <>''Y''
                   )
                when matched then update set M12.posted_bu_branch =s.lob_branch';
                --
           print_log('M12 SQL: ');
           print_log('========');
           print_log(v_merge_sql);
           print_log(' ');
               --
               print_log ('Begin Merge 12: Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
                execute immediate 'begin ' || v_merge_sql || '; :x := sql%rowcount; end;' using OUT l_count;
                print_log ('End Merge 12: Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
                --
               print_log('Merge M12 complete:  total records :'||l_count);
               --
               commit;
               --
       end if;
   --
  exception
   when others then
    rollback to square1;
    print_log ('@pop_posted_bu_branch_info_S, message ='||sqlerrm);
  end pop_posted_bu_branch_info_S;
  --
  begin --Main Processing
    --
    execute immediate 'ALTER SESSION FORCE PARALLEL DML';
    execute immediate 'ALTER TABLE XXCUS.XXCUS_OZF_XLA_ACCRUALS_B PARALLEL 8'; --03/25/2016 test this with 8 and nothing
    execute immediate 'ALTER TABLE XXCUS.XXCUS_OZF_XLA_ACCRUALS_B NOLOGGING';
    --
    -- Get previous period as we need to run thru some logic down the line to get all accruals accounted but not transferred to gl for p_period minus 1
    --
    --
    begin
        --
           print_log('Delete 1 SQL: ');
           print_log('============');
           print_log(' delete /*+ noparallel */  xxcus.xxcus_ozf_override_fin_loc_b');
           print_log(' ');
        delete /*+ noparallel */  xxcus.xxcus_ozf_override_fin_loc_b;
        --
        commit;
        --
        insert /*+ noparallel */ into  xxcus.xxcus_ozf_override_fin_loc_b
        (
         product
        ,override_fin_loc
        ,override_fin_loc_y_n
        ,bu --TMS  20160602-00010 ESMS 321264
        )
        (
        select distinct trim(attribute4) product, attribute5 override_fin_loc, nvl(attribute11, 'N') override_fin_loc_y_n
        ,party_name --TMS  20160602-00010 ESMS 321264 
        from   ar.hz_parties p
        where  1 =1
          and  attribute1 = 'HDS_BU'
          and  (attribute4 is not null and attribute5 is not null)
        );
        --
        commit;
        --
    exception
     when program_error then
      print_log('PROGRAM ERROR EXCEPTION IN INSERT OF xxcus.xxcus_ozf_override_fin_loc_b, MSG = '||sqlerrm);
      raise;
     when others then
      print_log('OTHERS EXCEPTION IN INSERT OF xxcus.xxcus_ozf_override_fin_loc_b, MSG ='||sqlerrm);
      raise;
    end;
    --
    begin
        select (
                    select period_name
                    from   gl_period_statuses
                    where 1 =1
                    and application_id =101
                    and ledger_id =2021
                    and effective_period_num = (a.effective_period_num-1)
                   ) previous_fiscal_period
         into l_prev_period
        from   gl_period_statuses  a
        where 1 =1
        and application_id =101 --owned by application GL
        and ledger_id =2021 --It's ok to use GSC US ledger as rebates shares the GSC US ledger
        and period_name =p_period;   --running period.
        --
    exception
     when no_data_found then
      l_prev_period :=Null;
     when others then
      l_prev_period :=Null;
    end;
    --
    print_log ('');
    print_log('Note: Current session and the custom table are parallel enabled.');
    print_log ('');
    print_log ('Parameter, p_period ='||p_period);
    print_log ('Parameter, p_account ='||p_account);
    print_log ('');
    print_log('Previous Fiscal Period :'||l_prev_period);
    print_log('');
    --
     begin
      --
      begin
        select 'DELETE /*+ PARALLEL (D1 8) */ XXCUS.XXCUS_OZF_XLA_ACCRUALS_B partition ('  --TMS 20160316-00196
             ||UPPER(SUBSTR(p_period, 1, 3))
             ||') D1 '
             ||'
        '
        ||' WHERE 1 =1 '
        ||'
        '
        ||' AND D1.XAEH_PERIOD_NAME ='||''''||p_period||''''
        into v_del_sql
        from dual;
        --
        --
           print_log('Delete 2 SQL: ');
           print_log('============');
           print_log(v_del_sql);
           print_log(' ');
        --
        print_log ('Begin Delete 2: Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
        execute immediate 'begin ' || v_del_sql || '; :x := sql%rowcount; end;' using OUT l_count;
        print_log ('End Delete 2: Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
        --
      exception
       when others then
        Null;
      end;
      --
      if l_count =0 then
       print_log ('Action -Cleanup: No TM accrual entries deleted '||
                  'using partition ='||Upper(Substr(p_period, 1, 3)) ||
                  ', period =' ||p_period ||
                  ' from table xxcus.xxcus_ozf_xla_accruals_b'
                 );
      else
       print_log ('Action -Cleanup: Successfully deleted '|| l_count ||
                  ' existing TM accrual entries ' ||
                  ' using partition ='||Upper(Substr(p_period, 1, 3)) ||
                  ', period =' ||p_period ||
                  ' from table xxcus.xxcus_ozf_xla_accruals_b'
                 );
      end if;
      --
      COMMIT;
      --
      print_log ('Begin Delete 3: Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
      DELETE /*+ PARALLEL (D2 2) */ XXCUS.XXCUS_OZF_XLA_ACCRUALS_B partition (NA) D2;
      print_log ('End Delete 3: Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
      --
           print_log('Delete 3 SQL: ');
           print_log('============');
           print_log('DELETE /*+ PARALLEL (D2 2) */ XXCUS.XXCUS_OZF_XLA_ACCRUALS_B partition (NA) D2');
           print_log(' ');
      --
      if nvl(SQL%ROWCOUNT, 0) =0 then
       print_log ('Action -Cleanup: No TM accrual entries deleted '||
                  'using partition =NA'||
                  ' from table xxcus.xxcus_ozf_xla_accruals_b'
                 );
      else
       print_log ('Action -Cleanup: Successfully deleted '|| SQL%ROWCOUNT ||
                  ' existing TM accrual entries ' ||
                  ' using partition =NA'||
                  ' from table xxcus.xxcus_ozf_xla_accruals_b'
                 );
      end if;
      --
      COMMIT;
      --
      begin
        select 'DELETE /*+ PARALLEL (D3 2) */ XXCUS.XXCUS_OZF_XLA_ACCRUALS_B partition ('
             ||UPPER(SUBSTR(l_prev_period, 1, 3))
             ||') D3 '
             ||'
        '
        ||' WHERE 1 =1 '
        ||'
        '
        ||' AND D3.XAEH_PERIOD_NAME ='||''''||l_prev_period||''''||'
        '||
        ' AND D3.XAEH_GL_XFER_STATUS_CODE <>''Y'''
        into v_del_sql
        from dual;
        --
        print_log ('Begin Delete 4: Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
        execute immediate 'begin ' || v_del_sql || '; :x := sql%rowcount; end;' using OUT l_count;
        print_log ('End Delete 4: Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
        --
           print_log('Delete 4 SQL: ');
           print_log('============');
           print_log(v_del_sql);
           print_log(' ');
        --
        if ( l_count >0 ) then
         --l_prev_period_gl_xfer_pending :=TRUE;
         print_log ('Action -Cleanup: Successfully deleted '|| l_count ||
                  ' existing TM accrual entries ' ||
                  ' using partition '||substr(upper(l_prev_period), 1, 3)||
                  '  from table xxcus.xxcus_ozf_xla_accruals_b AND xaeh_gl_xfer_status_code of N'
                 );
        else
         --l_prev_period_gl_xfer_pending :=FALSE;
         print_log ('Action -Cleanup: No TM accrual entries deleted '||
                  'using partition '||substr(upper(l_prev_period), 1, 3)||
                  '  from table xxcus.xxcus_ozf_xla_accruals_b AND xaeh_gl_xfer_status_code of N'
                 );
        end if;
        --
      exception
       when others then
        Null;
      end;
      --
      COMMIT;
      --
      b_proceed :=TRUE;
      --
      print_log ('Successfully removed old data removed from table xxcus.xxcus_ozf_xla_accruals_b');
      --
     exception
      when others then
       b_proceed :=FALSE;
       print_log (' Section: Delete, error: '||sqlerrm);
     end;
    --
    if (b_proceed) then
     --
     begin
      --
        print_log ('Begin Insert 1: Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
        --
        INSERT /*+ APPEND PARALLEL(t 8) */ INTO XXCUS.XXCUS_OZF_XLA_ACCRUALS_B t
        (
            SELECT /*+ PARALLEL(oxa 4) PARALLEL(offers 4) PARALLEL(qph 4) PARALLEL(ofu 4) PARALLEL(rslines 4) */
                   oxa.xla_accrual_id                 xla_accrual_id
                  ,ofu.request_id                     ofu_request_id
                  ,utilization_type                   utilization_type
                  ,ofu.adjustment_type                adjustment_type
                  ,ofu.adjustment_type_id             adjustment_type_id
                  ,ofu.object_id                      object_id
                  ,ofu.plan_curr_amount               rebate_amount
                  ,ofu.acctd_amount                   util_acctd_amount
                  ,oxa.event_type_code                event_type_code
                  ,oxa.entity_code                    entity_code
                  ,oxa.event_id                       event_id
                  ,oxa.utilization_id                 utilization_id
                  ,oxa.org_id                         oxa_org_id
                  ,trunc(ofu.gl_date)                 ofu_gl_date
                  ,hrou.set_of_books_id               ozf_ledger_id
                  ,case
                    when hrou.set_of_books_id =2021 then 'HDS Rebates USA'
                    when hrou.set_of_books_id =2023 then 'HDS Rebates CAN'
                    else Null
                   end                                ozf_ledger_name
                  ,xlae.entity_id                     xlae_entity_id
                  ,trunc(xlae.transaction_date)       xlae_trxn_date
                  ,trunc(xlae.event_date)             xlae_event_date
                  ,xlae.event_status_code             xlae_event_status_code
                  ,xlae.process_status_code           xlae_process_status_code
                  ,xlae.event_number                  xlae_event_number
                  ,xlae.on_hold_flag                  xlae_onhold_flag
                  ,xlae.application_id                xla_application_id
                  ,xaeh.ae_header_id                  xaeh_ae_header_id
                  ,xaeh.accounting_date               xaeh_accounting_date
                  ,xaeh.gl_transfer_status_code       xaeh_gl_xfer_status_code
                  ,trunc(xaeh.gl_transfer_date)       xaeh_gl_xfer_date
                  ,xaeh.je_category_name              xaeh_je_category
                  ,nvl(SUBSTR(  UPPER(xaeh.period_name)
                           ,1
                           ,3
                         ), 'NA')                     fiscal_month
                  ,nvl(xaeh.period_name, 'NA')        xaeh_period_name
                  ,xerr.accounting_error_id           accounting_error_id
                  ,xerr.accounting_batch_id           accounting_batch_id
                  ,xerr.encoded_msg                   encoded_msg
                  ,xerr.ae_line_num                   err_ae_line_num
                  ,xerr.message_number                message_number
                  ,xerr.error_source_code             error_source_code
                  ,offers.offer_id                    offer_id
                  ,offers.offer_code                  offer_code
                  ,qphtl.description                  offer_name
                  ,offers.offer_type                  offer_type
                  ,offers.autopay_party_id            autopay_party_id
                  ,offers.beneficiary_account_id      beneficiary_account_id
                  ,offers.status_code                 offer_status_code
                  ,offers.custom_setup_id             offer_custom_setup_id
                  ,offers.user_status_id              offer_user_status_id
                  ,offers.owner_id                    offer_owner_id
                  ,offers.activity_media_id           activity_media_id
                  --,Null                               activity_media_name
                  ,ams.media_name                     activity_media_name
                  ,offers.autopay_flag                offer_autopay_flag
                  ,ofu.currency_code                  ofu_currency_code
                  ,rslines.currency_code              receipts_currency
                  ,qph.attribute1                     auto_renewal
                  ,qph.attribute7                     calendar_year
                  ,qph.attribute2                     until_year
                  ,qph.attribute5                     payment_method
                  ,qph.attribute6                     payment_frequency
                  ,qph.attribute9                     guaranteed_amount
                  ,ofu.plan_id                        qp_list_header_id
                  ,qph.global_flag                    qp_global_flag
                  ,qph.active_flag                    qp_active_flag
                  ,qph.orig_org_id                    qp_orig_org_id
                  ,ofu.year_id                        ofu_year_id
                  ,ofu.fund_id                        fund_id
                  ,(
                    select /*+ RESULT_CACHE */
                           short_name
                    from   ozf_funds_all_tl
                    where  1 =1
                      and  fund_id =ofu.fund_id
                   )                                  fund_name
                  ,ofu.plan_type                      plan_type
                  ,ofu.component_id                   component_id
                  ,ofu.component_type                 component_type
                  ,ofu.object_type                    object_type
                  ,ofu.reference_type                 reference_type
                  ,ofu.reference_id                   reference_id
                  ,ofu.ams_activity_budget_id         ams_activity_budget_id
                  ,ofu.product_level_type             product_level_type
                  ,ofu.product_id                     product_id
                  ,rslines.creation_date              resale_date_created
                  ,trunc(rslines.date_invoiced)       date_invoiced
                  ,rslines.po_number                  po_number
                  ,rslines.purchase_uom_code          purchase_uom_code
                  ,rslines.uom_code                   uom_code
                  ,rslines.purchase_price             purchase_price
                  ,ofu.cust_account_id                ofu_cust_account_id
                  ,Null                               ofu_mvid
                  ,Null                               ofu_mvid_name
                  ,rslines.bill_to_cust_account_id    billto_cust_acct_id_br
                  ,Null                               billto_cust_acct_br_name
                  ,Null                               billto_cust_acct_br_acct_name
                  ,rslines.bill_to_party_id           bill_to_party_id
                  ,rslines.bill_to_party_name         bill_to_party_name_lob
                  ,trunc(rslines.date_ordered)        date_ordered
                  ,rslines.selling_price              selling_price
                  ,rslines.quantity                   quantity
                  ,rslines.inventory_item_id          inventory_item_id
                  ,rslines.item_number                item_number
                  ,rslines.line_attribute1            resale_line_attribute1 --pay_to_vendor_code
                  ,rslines.line_attribute2            resale_line_attribute2 --bu_nm
                  ,rslines.line_attribute3            resale_line_attribute3 --src_sys_nm
                  ,rslines.line_attribute5            resale_line_attribute5 --receipt_number
                  ,rslines.line_attribute6            resale_line_attribute6 --ship_to_branch_code
                  ,rslines.line_attribute11           resale_line_attribute11 --product_segment
                  ,rslines.line_attribute12           resale_line_attribute12 --location_segment
                  ,rslines.line_attribute13           resale_line_attribute13 --freight_discount
                  ,rslines.line_attribute14           resale_line_attribute14 --payment_discount
                  ,ofu.price_adjustment_id            pricing_adj_id
                  ,trunc(ofu.exchange_rate_date)      ofu_exchange_rate_date
                  ,ofu.exchange_rate                  ofu_exchange_rate
                  ,ofu.billto_cust_account_id         ofu_billto_cust_account_id_br
                  ,ofu.product_id                     ofu_product_id
                  ,rslines.sold_from_cust_account_id  sold_from_cust_account_id_mvid
                  ,rslines.sold_from_site_id          sold_from_site_id_mvid
                  ,rslines.sold_from_party_name       sold_from_party_name_mvid
                  ,rslines.line_attribute12           posted_fin_loc
                  ,ams.coop_yes_no                    coop_yes_no
                  ,rslines.line_attribute6            posted_bu_branch
                  ,'N'                                override_fin_loc
                  ,ofu.attribute10                    ofu_attribute10
                  ,trunc(ofu.creation_date)           ofu_creation_date
            from   ozf_xla_accruals oxa
                  ,hr_operating_units        hrou
                  ,ozf_funds_utilized_all_b  ofu
                  ,xla_events partition (OZF) xlae
                  ,xla_ae_headers partition (OZF) xaeh
                  ,xla.xla_accounting_errors xerr
                  ,ozf_offers                offers
                  ,qp_list_headers_b         qph
                  ,qp_list_headers_tl        qphtl
                  ,ozf_resale_lines_all      rslines
                  ,(
                    select amstl.media_name media_name
                          ,amstl.media_id
                          ,case
                            when amstl.description IN ('COOP', 'COOP_MIN') then 'Y'
                            else 'N'
                           end   coop_yes_no
                    from   ams_media_tl amstl
                          ,ams_media_b  ams
                    where  1 =1
                      and  ams.media_id =amstl.media_id
                   ) ams
            where  1 =1
              and  hrou.organization_id    =oxa.org_id
              and  ofu.utilization_id      =oxa.utilization_id
              and  ofu.org_id              =oxa.org_id
              and  xlae.event_id           =oxa.event_id
              and  xlae.application_id     =682
              and  xaeh.event_id(+)        =xlae.event_id
              and  xaeh.application_id(+)  =xlae.application_id
              and  ((xaeh.period_name  =p_period) OR (xaeh.period_name IS NULL))
              and  xerr.event_id(+)        =xlae.event_id
              and  qphtl.list_header_id         =ofu.plan_id
              and  offers.qp_list_header_id     =qphtl.list_header_id
              and  qph.list_header_id           =qphtl.list_header_id
              and  rslines.resale_line_id(+)    =ofu.object_id
              and  ams.media_id(+)              =offers.activity_media_id
        ); --End of insert statement
      --
      print_log ('Part1 Insert: Total records inserted into the table xxcus.xxcus_ozf_xla_accruals_b =>'||SQL%ROWCOUNT);
      --
      Commit;
      --
      print_log ('End Insert 1: Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
     exception
      when others then
       print_log('Error in insert of OZF XLA Accruals, Message =>'||sqlerrm);
       b_proceed :=FALSE;
     end;
     --
     begin
      --
            print_log ('Begin Insert 2: Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
            --
            INSERT /*+ APPEND PARALLEL(t 8) */ INTO XXCUS.XXCUS_OZF_XLA_ACCRUALS_B t
            (
               SELECT /*+ PARALLEL(qph 8) PARALLEL(ofu 8) PARALLEL(rslines 8) */
                       xaeh.xla_accrual_id                 xla_accrual_id
                      ,ofu.request_id                     ofu_request_id
                      ,ofu.utilization_type                   utilization_type
                      ,ofu.adjustment_type                adjustment_type
                      ,ofu.adjustment_type_id             adjustment_type_id
                      ,ofu.object_id                      object_id
                      ,ofu.plan_curr_amount               rebate_amount
                      ,ofu.acctd_amount                   util_acctd_amount
                      ,xaeh.event_type_code                event_type_code
                      ,xaeh.entity_code                    entity_code
                      ,xaeh.event_id                       event_id
                      ,xaeh.utilization_id                 utilization_id
                      ,xaeh.org_id                         oxa_org_id
                      ,trunc(ofu.gl_date)                 ofu_gl_date
                      ,hrou.set_of_books_id               ozf_ledger_id
                      ,case
                        when hrou.set_of_books_id =2021 then 'HDS Rebates USA'
                        when hrou.set_of_books_id =2023 then 'HDS Rebates CAN'
                        else Null
                       end                                ozf_ledger_name
                      ,xaeh.entity_id                     xlae_entity_id
                      ,trunc(xaeh.transaction_date)       xlae_trxn_date
                      ,trunc(xaeh.event_date)             xlae_event_date
                      ,xaeh.event_status_code             xlae_event_status_code
                      ,xaeh.process_status_code           xlae_process_status_code
                      ,xaeh.event_number                  xlae_event_number
                      ,xaeh.on_hold_flag                  xlae_onhold_flag
                      ,xaeh.application_id                xla_application_id
                      ,xaeh.ae_header_id                  xaeh_ae_header_id
                      ,xaeh.accounting_date               xaeh_accounting_date
                      ,xaeh.gl_transfer_status_code       xaeh_gl_xfer_status_code
                      ,trunc(xaeh.gl_transfer_date)       xaeh_gl_xfer_date
                      ,xaeh.je_category_name              xaeh_je_category
                      ,nvl(SUBSTR(  UPPER(xaeh.period_name)
                               ,1
                               ,3
                             ), 'NA')                     fiscal_month
                      ,nvl(xaeh.period_name, 'NA')        xaeh_period_name
                      ,xerr.accounting_error_id           accounting_error_id
                      ,xerr.accounting_batch_id           accounting_batch_id
                      ,xerr.encoded_msg                   encoded_msg
                      ,xerr.ae_line_num                   err_ae_line_num
                      ,xerr.message_number                message_number
                      ,xerr.error_source_code             error_source_code
                      ,offers.offer_id                    offer_id
                      ,offers.offer_code                  offer_code
                      ,qphtl.description                  offer_name
                      ,offers.offer_type                  offer_type
                      ,offers.autopay_party_id            autopay_party_id
                      ,offers.beneficiary_account_id      beneficiary_account_id
                      ,offers.status_code                 offer_status_code
                      ,offers.custom_setup_id             offer_custom_setup_id
                      ,offers.user_status_id              offer_user_status_id
                      ,offers.owner_id                    offer_owner_id
                      ,offers.activity_media_id           activity_media_id
                      ,ams.media_name                     activity_media_name
                      ,offers.autopay_flag                offer_autopay_flag
                      ,ofu.currency_code                  ofu_currency_code
                      ,rslines.currency_code              receipts_currency
                      ,qph.attribute1                     auto_renewal
                      ,qph.attribute7                     calendar_year
                      ,qph.attribute2                     until_year
                      ,qph.attribute5                     payment_method
                      ,qph.attribute6                     payment_frequency
                      ,qph.attribute9                     guaranteed_amount
                      ,ofu.plan_id                        qp_list_header_id
                      ,qph.global_flag                    qp_global_flag
                      ,qph.active_flag                    qp_active_flag
                      ,qph.orig_org_id                    qp_orig_org_id
                      ,ofu.year_id                        ofu_year_id
                      ,ofu.fund_id                        fund_id
                      ,(
                        select /*+ RESULT_CACHE */
                               short_name
                        from   ozf_funds_all_tl
                        where  1 =1
                          and  fund_id =ofu.fund_id
                       )                                  fund_name
                      ,ofu.plan_type                      plan_type
                      ,ofu.component_id                   component_id
                      ,ofu.component_type                 component_type
                      ,ofu.object_type                    object_type
                      ,ofu.reference_type                 reference_type
                      ,ofu.reference_id                   reference_id
                      ,ofu.ams_activity_budget_id         ams_activity_budget_id
                      ,ofu.product_level_type             product_level_type
                      ,ofu.product_id                     product_id
                      ,rslines.creation_date              resale_date_created
                      ,trunc(rslines.date_invoiced)       date_invoiced
                      ,rslines.po_number                  po_number
                      ,rslines.purchase_uom_code          purchase_uom_code
                      ,rslines.uom_code                   uom_code
                      ,rslines.purchase_price             purchase_price
                      ,ofu.cust_account_id                ofu_cust_account_id
                      ,Null                               ofu_mvid
                      ,Null                               ofu_mvid_name
                      ,rslines.bill_to_cust_account_id    billto_cust_acct_id_br
                      ,Null                               billto_cust_acct_br_name
                      ,Null                               billto_cust_acct_br_acct_name
                      ,rslines.bill_to_party_id           bill_to_party_id
                      ,rslines.bill_to_party_name         bill_to_party_name_lob
                      ,trunc(rslines.date_ordered)        date_ordered
                      ,rslines.selling_price              selling_price
                      ,rslines.quantity                   quantity
                      ,rslines.inventory_item_id          inventory_item_id
                      ,rslines.item_number                item_number
                      ,rslines.line_attribute1            resale_line_attribute1 --pay_to_vendor_code
                      ,rslines.line_attribute2            resale_line_attribute2 --bu_nm
                      ,rslines.line_attribute3            resale_line_attribute3 --src_sys_nm
                      ,rslines.line_attribute5            resale_line_attribute5 --receipt_number
                      ,rslines.line_attribute6            resale_line_attribute6 --ship_to_branch_code
                      ,rslines.line_attribute11           resale_line_attribute11 --product_segment
                      ,rslines.line_attribute12           resale_line_attribute12 --location_segment
                      ,rslines.line_attribute13           resale_line_attribute13 --freight_discount
                      ,rslines.line_attribute14           resale_line_attribute14 --payment_discount
                      ,ofu.price_adjustment_id            pricing_adj_id
                      ,trunc(ofu.exchange_rate_date)      ofu_exchange_rate_date
                      ,ofu.exchange_rate                  ofu_exchange_rate
                      ,ofu.billto_cust_account_id         ofu_billto_cust_account_id_br
                      ,ofu.product_id                     ofu_product_id
                      ,rslines.sold_from_cust_account_id  sold_from_cust_account_id_mvid
                      ,rslines.sold_from_site_id          sold_from_site_id_mvid
                      ,rslines.sold_from_party_name       sold_from_party_name_mvid
                      ,rslines.line_attribute12           posted_fin_loc
                      ,ams.coop_yes_no                    coop_yes_no
                      ,rslines.line_attribute6            posted_bu_branch
                      ,'N'                                override_fin_loc
                      ,ofu.attribute10                    ofu_attribute10
                      ,trunc(ofu.creation_date)           ofu_creation_date
                from   hr_operating_units        hrou
                      ,ozf_funds_utilized_all_b  ofu
                      ,(
                         SELECT /*+ parallel (a 8) parallel (b 8)  parallel (c 8) */
                                        a.*, b.org_id, b.utilization_id, c.on_hold_flag, c.event_number
                                       ,c.event_status_code, c.process_status_code, c.event_date, c.transaction_date
                                       ,b.entity_code, b.xla_accrual_id
                         FROM   xla_ae_headers partition (OZF) a
                                         ,ozf_xla_accruals                           b
                                         ,xla_events partition (OZF)         c
                         WHERE 1 =1
                               AND a.gl_transfer_status_code ='N'
                               AND c.event_id =a.event_id
                               AND b.event_id =c.event_id
                       ) xaeh
                      ,xla.xla_accounting_errors xerr
                      ,ozf_offers                offers
                      ,qp_list_headers_b         qph
                      ,qp_list_headers_tl        qphtl
                      ,ozf_resale_lines_all      rslines
                      ,(
                        select /*+ RESULT_CACHE */  amstl.media_name media_name
                              ,amstl.media_id
                              ,case
                                when amstl.description IN ('COOP', 'COOP_MIN') then 'Y'
                                else 'N'
                               end   coop_yes_no
                        from   ams_media_tl amstl
                              ,ams_media_b  ams
                        where  1 =1
                          and  ams.media_id =amstl.media_id
                       ) ams
                where  1 =1
                  and  ofu.utilization_id      =xaeh.utilization_id --bala
                  and  ofu.org_id              =xaeh.org_id
                  and  hrou.organization_id    =xaeh.org_id
                  and  qphtl.list_header_id         =ofu.plan_id
                  and  offers.qp_list_header_id     =qphtl.list_header_id
                  and  qph.list_header_id           =qphtl.list_header_id
                  and  rslines.resale_line_id(+)    =ofu.object_id
                  and  ams.media_id(+)              =offers.activity_media_id
                  and  xerr.event_id(+)        =xaeh.event_id
            ); --End of insert statement
      --
      print_log ('Part2 Insert: Total records inserted into the table xxcus.xxcus_ozf_xla_accruals_b =>'||SQL%ROWCOUNT);
      --
      if (SQL%ROWCOUNT >0) then
         l_prev_period_gl_xfer_pending :=TRUE;
      else
         l_prev_period_gl_xfer_pending :=FALSE;
         print_log('No accruals found for previous fiscal period '||l_prev_period||' with gl transfer status of N.');
      end if;
      --
      Commit;
      --
      print_log ('End Insert 2: Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
     exception
      when others then
       print_log('Error in insert of XXCUS_OZF XLA_ACCRUALS_B for previous period where accruals are stuck in transferring to GL, message =>'||sqlerrm);
       b_proceed :=FALSE;
     end;
     -- End TMS 20160316-00196
    else
     print_log('Issue in deleting period based data from XXCUS.XXCUS_OZF_XLA_ACCRUALS_B, message =>'||sqlerrm);
    end if;
    --
    begin
     --
     l_period :=p_period;
     --
     v_loc :='103';
     pop_accruals_mvid_info_S;
     Commit;
     print_log ('@ update of accrual attributes, Exit: pop_accruals_mvid_info_single');
     v_loc :='104';
     pop_branch_info_S;
     Commit;
     print_log ('@ update of accrual attributes, Exit: pop_branch_info_single');
     v_loc :='105';
     pop_posted_fin_loc_info_S;
     Commit;
     print_log ('@ update of accrual attributes, Exit: pop_posted_fin_loc_info_single');
     v_loc :='106';
     pop_posted_bu_branch_info_S;
     Commit;
     v_loc :='107';
     print_log ('@ update of accrual attributes, Exit: pop_posted_bu_branch_info_single');
     --
     Commit;
     --
    exception
     when others then
      print_log ('@Accruals attributes update single, last known location ='||v_loc||', Error =>'||sqlerrm);
    end;
    --
        begin
         --
         print_log ('Begin rebuild of partitioned indexes...');
             --
             for rec in rebuild_partitioned_indexes
               (
                     p_table =>'XXCUS_OZF_XLA_ACCRUALS_B'
                    ,p_current_period_partition =>upper(substr(p_period, 1, 3))
                    ,p_prev_period_partition =>upper(substr(l_prev_period, 1, 3))
               )
             loop
              --
               v_sql :=rec.idx_sql;
              --
                execute immediate rec.idx_sql;
              --
             end loop;
             --
         print_log ('End rebuild of partitioned indexes...');
         --
        exception
         when others then
          print_log ('Issue in rebuild of partitioned index, current sql =>'||v_sql);
        end;
    --
        begin
         print_log ('Begin remove_parallel_on_ozf_objects');
             --
             for rec in remove_parallel_on_ozf_objects  loop
              --
               v_sql :=rec.sql_text;
              --
                execute immediate rec.sql_text;
              --
             end loop;
             --
         print_log ('End remove_parallel_on_ozf_objects');
         --
        exception
         when no_data_found then
          print_log ('No OZF objects found with parallel on');
         when others then
          print_log ('Issue in remove_parallel_on_ozf_objects, current sql =>'||v_sql);
        end;
    --
    Commit;
    --
    execute immediate 'ALTER TABLE XXCUS.XXCUS_OZF_XLA_ACCRUALS_B NOPARALLEL';
    --
    --
  exception
   when others then
    print_log ('Issue in extract_rebate_accruals routine, message ='||sqlerrm);
    rollback;
  end extract_rebate_accruals;
  --
  -- End TMS 20160316-00196
  -- extract_accruals is wide open and will sweep every single record in the system, exercise caution before submit
  --
  procedure extract_accruals
  (
    retcode               out number
   ,errbuf                out varchar2
   ,p_ledger_id           in  number
   ,p_period              in  varchar2
   ,p_account             in  varchar2
  ) is
  --
   v_sql VARCHAR2(240) :=Null;
   N_Count NUMBER;
   b_proceed BOOLEAN;
   b_backup  BOOLEAN;
   v_loc     varchar2(3);
  --
   cursor rebuild_partitioned_indexes (p_table in varchar2) is
    select 'ALTER INDEX '||owner||'.'||uip.index_name||' REBUILD PARTITION '||uip.partition_name||' PARALLEL 4' idx_sql
    from dba_ind_partitions  uip
        ,dba_tab_partitions  utp
        ,dba_indexes         ui
    where 1 =1
      and uip.partition_name  =utp.partition_name
      and ui.index_name       =uip.index_name
      and ui.table_name       =utp.table_name
      and ui.owner            ='XXCUS'
      and utp.table_name      =p_table -- 'XXCUS_OZF_XLA_ACCRUALS_B'
    order by uip.index_name;
  --
  procedure pop_accruals_ledger_name is
  begin
   --
   savepoint square1;
   --
   merge /*+ PARALLEL (t 8) */ into xxcus.xxcus_ozf_xla_accruals_b t
    using (
            select name ledger_name
                  ,ledger_id ledger_id
            from   gl_ledgers
            where  1 =1
           ) s
    on (t.ozf_ledger_id = s.ledger_id)
    when matched then update set t.ozf_ledger_name =s.ledger_name;
   --
  exception
   when others then
    rollback to square1;
    print_log ('@pop_accruals_ledger_name, message ='||sqlerrm);
  end pop_accruals_ledger_name;
  --
  procedure pop_accruals_media_name is
   V_CO_OP varchar2(10) :='COOP';
  begin
   --
   savepoint square1;
   --
    merge /*+ PARALLEL (t 8) */ into xxcus.xxcus_ozf_xla_accruals_b t
    using (
            select amstl.media_name
                  ,amstl.media_id
                  ,case
                    --when ams.attribute1 =V_CO_OP then 'Y'
                    when amstl.description IN ('COOP', 'COOP_MIN') then 'Y'
                    else 'N'
                   end   coop_yes_no
            from   ams_media_tl amstl
                  ,ams_media_b  ams
            where  1 =1
              and  ams.media_id =amstl.media_id
           ) s
    on (t.activity_media_id = s.media_id)
    when matched then update set t.activity_media_name =s.media_name, t.coop_yes_no =s.coop_yes_no;
   --
  exception
   when others then
    rollback to square1;
    print_log ('@pop_accruals_media_name, message ='||sqlerrm);
  end pop_accruals_media_name;
  --
  procedure pop_accruals_fund_name is
  begin
   --
   savepoint square1;
   --
    merge /*+ PARALLEL (t 8) */ into xxcus.xxcus_ozf_xla_accruals_b t
    using (
            select short_name
                  ,fund_id fund_id
            from   ozf_funds_all_tl
            where  1 =1
           ) s
    on (t.fund_id = s.fund_id)
    when matched then update set t.fund_name =s.short_name;
   --
  exception
   when others then
    rollback to square1;
    print_log ('@pop_accruals_fund_name, message ='||sqlerrm);
  end pop_accruals_fund_name;
  --
  procedure pop_accruals_mvid_info is
  begin
   --
   savepoint square1;
   --
    merge /*+ PARALLEL (t 8) */ into xxcus.xxcus_ozf_xla_accruals_b t
    using (
            select hzca.attribute2      attribute2
                  ,hzp.party_name       party_name
                  ,hzca.cust_account_id cust_account_id
            from   hz_cust_accounts hzca
                  ,hz_parties       hzp
            where  1 =1
              and  hzp.party_id =hzca.party_id
           ) s
    on (t.ofu_cust_account_id = s.cust_account_id)
    when matched then update set t.ofu_mvid =s.attribute2, t.ofu_mvid_name =s.party_name;
   --
  exception
   when others then
    rollback to square1;
    print_log ('@pop_accruals_mvid_info, message ='||sqlerrm);
  end pop_accruals_mvid_info;
  --
  procedure pop_branch_info is
  begin
   --
   savepoint square1;
   --
    merge /*+ PARALLEL (t 8) */ into xxcus.xxcus_ozf_xla_accruals_b t
    using (
            select hzca.attribute2      attribute2
                  ,hzp.party_name       party_name
                  ,hzca.account_number  account_number
                  ,hzca.cust_account_id cust_account_id
            from   hz_cust_accounts hzca
                  ,hz_parties       hzp
            where  1 =1
              and  hzp.party_id =hzca.party_id
           ) s
    on (t.billto_cust_acct_id_br = s.cust_account_id)
    when matched then update set t.billto_cust_acct_br_name =s.party_name, t.billto_cust_acct_br_acct_name =s.account_number;
   --
  exception
   when others then
    rollback to square1;
    print_log ('@pop_branch_info, message ='||sqlerrm);
  end pop_branch_info;
  --
  procedure pop_posted_fin_loc_info is
  begin
   --
   savepoint square1;
   --
    merge /*+ PARALLEL (t 8) */ into xxcus.xxcus_ozf_xla_accruals_b t
    using (
            select distinct trim(attribute4) product, attribute5 override_fin_loc, nvl(attribute11, 'N') override_fin_loc_y_n
            from   ar.hz_parties p
            where  1 =1
              and  attribute1 = 'HDS_BU'
              and  (attribute4 is not null and attribute5 is not null)
           ) s
    on (
             t.resale_line_attribute11 =s.product
         and t.coop_yes_no             ='Y'
         and s.override_fin_loc_y_n    ='Y'
       )
    when matched
     then update set t.posted_fin_loc =s.override_fin_loc, t.override_fin_loc =s.override_fin_loc_y_n;
   --
  exception
   when others then
    rollback to square1;
    print_log ('@pop_posted_fin_loc_info, message ='||sqlerrm);
  end pop_posted_fin_loc_info;
  --
  procedure pop_posted_bu_branch_info is
  begin
   --
   savepoint square1;
   --
    merge /*+ PARALLEL (t 8) */ into xxcus.xxcus_ozf_xla_accruals_b t
    using (
            select /*+ RESULT_CACHE */
                   xlcv.entrp_loc     entrp_loc
                  ,xlcv.lob_branch    lob_branch
                  ,xrbxt.rebates_fru  rebates_fru
            from apps.xxcus_location_code_vw xlcv
                ,(
                   select a.rebates_fru
                   from   xxcus.xxcus_rebate_branch_xref_tbl a
                   where  1 =1
                     and  rowid =
                            (
                              select min(rowid)
                              from   xxcus.xxcus_rebate_branch_xref_tbl b
                              where  1 =1
                                and  b.rebates_fru =a.rebates_fru
                            )
                 ) xrbxt
                ,fnd_lookup_values_vl lv
            where 1 =1
              and xrbxt.rebates_fru = xlcv.fru
              and lv.lookup_type = 'XXCUS_BUSINESS_UNIT'
              and xlcv.business_unit = lv.lookup_code
            order by 1, 2
           ) s
    on (
             t.posted_fin_loc   =s.entrp_loc
         and t.coop_yes_no      ='Y'
         and t.override_fin_loc ='Y'
       )
    when matched
     then update set t.posted_bu_branch =s.lob_branch;
   --
  exception
   when others then
    rollback to square1;
    print_log ('@pop_posted_fin_loc_info, message ='||sqlerrm);
  end pop_posted_bu_branch_info;
  --
  begin --Main Processing
    --
    execute immediate 'ALTER SESSION FORCE PARALLEL DML';
    execute immediate 'ALTER TABLE XXCUS.XXCUS_OZF_XLA_ACCRUALS_B PARALLEL';
    execute immediate 'ALTER TABLE XXCUS.XXCUS_OZF_XLA_ACCRUALS_B NOLOGGING';

    print_log ('');
    print_log('Note: Current session and the custom table are parallel enabled.');
    print_log ('');
    print_log ('Parameter, p_ledger_id ='||p_ledger_id);
    print_log ('Parameter, p_period ='||p_period);
    print_log ('Parameter, p_account ='||p_account);
    print_log ('');
    --
     begin
      --
      execute immediate 'TRUNCATE TABLE XXCUS.XXCUS_OZF_XLA_ACCRUALS_B';
      --
      b_proceed :=TRUE;
      --
      print_log ('Successfully removed old data removed from table xxcus.xxcus_ozf_xla_accruals_b');
      --
     exception
      when others then
       b_proceed :=FALSE;
       print_log (''||sqlerrm);
     end;
    --
    if (b_proceed) then
     --
     begin
      --
        INSERT /*+ APPEND PARALLEL (t 8) */ INTO XXCUS.XXCUS_OZF_XLA_ACCRUALS_B t
        (
            SELECT /*+ PARALLEL(oxa 8) PARALLEL(offers 8) PARALLEL(qph 8) PARALLEL(ofu 8) PARALLEL(rslines 8) */
                   oxa.xla_accrual_id                 xla_accrual_id
                  ,ofu.request_id                     ofu_request_id
                  ,utilization_type                   utilization_type
                  ,ofu.adjustment_type                adjustment_type
                  ,ofu.adjustment_type_id             adjustment_type_id
                  ,ofu.object_id                      object_id
                  ,ofu.plan_curr_amount               rebate_amount
                  ,ofu.acctd_amount                   util_acctd_amount
                  ,oxa.event_type_code                event_type_code
                  ,oxa.entity_code                    entity_code
                  ,oxa.event_id                       event_id
                  ,oxa.utilization_id                 utilization_id
                  ,oxa.org_id                         oxa_org_id
                  ,trunc(ofu.gl_date)                 ofu_gl_date
                  ,hrou.set_of_books_id               ozf_ledger_id
                  ,case
                    when hrou.set_of_books_id =2021 then 'HDS Rebates USA'
                    when hrou.set_of_books_id =2023 then 'HDS Rebates CAN'
                    else Null
                   end                                ozf_ledger_name
                  --,xerr.error_source_code             ozf_ledger_name
                  ,xlae.entity_id                     xlae_entity_id
                  ,trunc(xlae.transaction_date)       xlae_trxn_date
                  ,trunc(xlae.event_date)             xlae_event_date
                  ,xlae.event_status_code             xlae_event_status_code
                  ,xlae.process_status_code           xlae_process_status_code
                  ,xlae.event_number                  xlae_event_number
                  ,xlae.on_hold_flag                  xlae_onhold_flag
                  ,xlae.application_id                xla_application_id
                  ,xaeh.ae_header_id                  xaeh_ae_header_id
                  ,xaeh.accounting_date               xaeh_accounting_date
                  ,xaeh.gl_transfer_status_code       xaeh_gl_xfer_status_code
                  ,trunc(xaeh.gl_transfer_date)       xaeh_gl_xfer_date
                  ,xaeh.je_category_name              xaeh_je_category
                  ,SUBSTR(  UPPER(xaeh.period_name)
                           ,1
                           ,3
                         )                            fiscal_month
                  ,nvl(xaeh.period_name, 'NA')        xaeh_period_name
                  ,xerr.accounting_error_id           accounting_error_id
                  ,xerr.accounting_batch_id           accounting_batch_id
                  ,xerr.encoded_msg                   encoded_msg
                  ,xerr.ae_line_num                   err_ae_line_num
                  ,xerr.message_number                message_number
                  ,xerr.error_source_code             error_source_code
                  ,offers.offer_id                    offer_id
                  ,offers.offer_code                  offer_code
                  ,qphtl.description                  offer_name
                  ,offers.offer_type                  offer_type
                  ,offers.autopay_party_id            autopay_party_id
                  ,offers.beneficiary_account_id      beneficiary_account_id
                  ,offers.status_code                 offer_status_code
                  ,offers.custom_setup_id             offer_custom_setup_id
                  ,offers.user_status_id              offer_user_status_id
                  ,offers.owner_id                    offer_owner_id
                  ,offers.activity_media_id           activity_media_id
                  ,Null                               activity_media_name
                  ,offers.autopay_flag                offer_autopay_flag
                  ,ofu.currency_code                  ofu_currency_code
                  ,rslines.currency_code              receipts_currency
                  ,qph.attribute1                     auto_renewal
                  ,qph.attribute7                     calendar_year
                  ,qph.attribute2                     until_year
                  ,qph.attribute5                     payment_method
                  ,qph.attribute6                     payment_frequency
                  ,qph.attribute9                     guaranteed_amount
                  ,ofu.plan_id                        qp_list_header_id
                  ,qph.global_flag                    qp_global_flag
                  ,qph.active_flag                    qp_active_flag
                  ,qph.orig_org_id                    qp_orig_org_id
                  ,ofu.year_id                        ofu_year_id
                  ,ofu.fund_id                        fund_id
                  ,Null                               fund_name
                  ,ofu.plan_type                      plan_type
                  ,ofu.component_id                   component_id
                  ,ofu.component_type                 component_type
                  ,ofu.object_type                    object_type
                  ,ofu.reference_type                 reference_type
                  ,ofu.reference_id                   reference_id
                  ,ofu.ams_activity_budget_id         ams_activity_budget_id
                  ,ofu.product_level_type             product_level_type
                  ,ofu.product_id                     product_id
                  ,rslines.creation_date              resale_date_created
                  ,trunc(rslines.date_invoiced)       date_invoiced
                  ,rslines.po_number                  po_number
                  ,rslines.purchase_uom_code          purchase_uom_code
                  ,rslines.uom_code                   uom_code
                  ,rslines.purchase_price             purchase_price
                  ,ofu.cust_account_id                ofu_cust_account_id
                  ,Null                               ofu_mvid
                  ,Null                               ofu_mvid_name
                  ,rslines.bill_to_cust_account_id    billto_cust_acct_id_br
                  ,Null                               billto_cust_acct_br_name
                  ,Null                               billto_cust_acct_br_acct_name
                  ,rslines.bill_to_party_id           bill_to_party_id
                  ,rslines.bill_to_party_name         bill_to_party_name_lob
                  ,trunc(rslines.date_ordered)        date_ordered
                  ,rslines.selling_price              selling_price
                  ,rslines.quantity                   quantity
                  ,rslines.inventory_item_id          inventory_item_id
                  ,rslines.item_number                item_number
                  ,rslines.line_attribute1            resale_line_attribute1 --pay_to_vendor_code
                  ,rslines.line_attribute2            resale_line_attribute2 --bu_nm
                  ,rslines.line_attribute3            resale_line_attribute3 --src_sys_nm
                  ,rslines.line_attribute5            resale_line_attribute5 --receipt_number
                  ,rslines.line_attribute6            resale_line_attribute6 --ship_to_branch_code
                  ,rslines.line_attribute11           resale_line_attribute11 --product_segment
                  ,rslines.line_attribute12           resale_line_attribute12 --location_segment
                  ,rslines.line_attribute13           resale_line_attribute13 --freight_discount
                  ,rslines.line_attribute14           resale_line_attribute14 --payment_discount
                  ,ofu.price_adjustment_id            pricing_adj_id
                  ,trunc(ofu.exchange_rate_date)      ofu_exchange_rate_date
                  ,ofu.exchange_rate                  ofu_exchange_rate
                  ,ofu.billto_cust_account_id         ofu_billto_cust_account_id_br
                  ,ofu.product_id                     ofu_product_id
                  ,rslines.sold_from_cust_account_id  sold_from_cust_account_id_mvid
                  ,rslines.sold_from_site_id          sold_from_site_id_mvid
                  ,rslines.sold_from_party_name       sold_from_party_name_mvid
                  ,rslines.line_attribute12           posted_fin_loc
                  ,null                               coop_yes_no
                  ,rslines.line_attribute6            posted_bu_branch
                  ,'N'                                override_fin_loc
                  ,ofu.attribute10                    ofu_attribute10
                  ,trunc(ofu.creation_date)           ofu_creation_date
            from   ozf_xla_accruals oxa
                  ,hr_operating_units        hrou
                  ,ozf_funds_utilized_all_b  ofu
                  ,xla_events                xlae
                  ,xla_ae_headers            xaeh
                  ,xla.xla_accounting_errors xerr
                  ,ozf_offers                offers
                  ,qp_list_headers_b         qph
                  ,qp_list_headers_tl        qphtl
                  ,ozf_resale_lines_all      rslines
            where  1 =1
              and  hrou.organization_id    =oxa.org_id
              and  ofu.utilization_id      =oxa.utilization_id
              and  ofu.org_id              =oxa.org_id
              and  xlae.event_id           =oxa.event_id
              and  xlae.application_id     =682
              and  xaeh.event_id(+)        =xlae.event_id
              and  xaeh.application_id(+)  =xlae.application_id
              and  xerr.event_id(+)        =xlae.event_id
              and  qphtl.list_header_id         =ofu.plan_id
              and  offers.qp_list_header_id     =qphtl.list_header_id
              and  qph.list_header_id           =qphtl.list_header_id
              and  rslines.resale_line_id(+)    =ofu.object_id
        ); --End of insert statement
      --
      print_log ('Total records inserted into the table xxcus.xxcus_ozf_xla_accruals_b =>'||SQL%ROWCOUNT);
      --
      Commit;
      --
     exception
      when others then
       print_log('Error in insert of OZF XLA Accruals, Message =>'||sqlerrm);
       b_proceed :=FALSE;
     end;
     --
    else
     print_log('Issue in truncating old data from XXCUS.XXCUS_OZF_XLA_ACCRUALS_B, message =>'||sqlerrm);
    end if;
    --
    begin
     --
     --pop_accruals_ledger_name; --Commented out bcoz the main insert takes care of this and is a lot faster.
     --Commit;
     --print_log ('@ update of accrual attributes, Exit: pop_accruals_ledger_name');
     v_loc :='101';
     pop_accruals_media_name;
     Commit;
     print_log ('@ update of accrual attributes, Exit: pop_accruals_media_name');
     v_loc :='102';
     pop_accruals_fund_name;
     Commit;
     print_log ('@ update of accrual attributes, Exit: pop_accruals_fund_name');
     v_loc :='103';
     pop_accruals_mvid_info;
     Commit;
     print_log ('@ update of accrual attributes, Exit: pop_accruals_mvid_info');
     v_loc :='104';
     pop_branch_info;
     Commit;
     print_log ('@ update of accrual attributes, Exit: pop_branch_info');
     v_loc :='105';
     pop_posted_fin_loc_info;
     Commit;
     print_log ('@ update of accrual attributes, Exit: pop_posted_fin_loc_info');
     v_loc :='106';
     pop_posted_bu_branch_info;
     Commit;
     v_loc :='107';
     print_log ('@ update of accrual attributes, Exit: pop_posted_bu_branch_info');
     --
    exception
     when others then
      print_log ('@Accruals attributes update, last known location ='||v_loc||', Error =>'||sqlerrm);
    end;
    --
    begin
     --
     print_log ('Begin rebuild of partitioned indexes...');
     --
     for rec in rebuild_partitioned_indexes (p_table =>'XXCUS_OZF_XLA_ACCRUALS_B') loop
      --
       v_sql :=rec.idx_sql;
      --
        execute immediate rec.idx_sql;
      --
       --print_log ('Rebuild SQL '||rec.idx_sql||' was executed successfully');
      --
     end loop;
     --
     print_log ('End rebuild of partitioned indexes...');
     --
    exception
     when others then
      print_log ('Issue in rebuild of partitioned index, current sql =>'||v_sql);
    end;
    --
  exception
   when others then
    print_log ('Issue in extract_accruals routine, message ='||sqlerrm);
    rollback;
  end extract_accruals;
  --
  procedure extract_journals
  (
    retcode               out number
   ,errbuf                out varchar2
   ,p_territory           in  varchar2
   ,p_period              in  varchar2
   ,p_account             in  varchar2
  ) is
   --
   v_sql              VARCHAR2(240) :=Null;
   N_Count            NUMBER;
   --
   b_proceed          BOOLEAN;
   b_backup           BOOLEAN;
   --
   v_loc              VARCHAR2(3);
   v_product          VARCHAR2(2) :=Null;
   v_ar_journals      VARCHAR2(40) :='Receivables';
   --
   v_tm_journals      VARCHAR2(40) :='Channel Management';
   v_acct_class_code  VARCHAR2(20) :='EXPENSE';
   n_ledger_id        NUMBER :=0;
   --
   cursor rebuild_partitioned_indexes (p_table in varchar2, p_partition in varchar2) is
    select 'ALTER INDEX '||owner||'.'||uip.index_name||' REBUILD PARTITION '||uip.partition_name idx_sql
    --select 'ALTER INDEX '||owner||'.'||uip.index_name||' REBUILD PARTITION '||uip.partition_name idx_sq
    from dba_ind_partitions  uip
        ,dba_tab_partitions  utp
        ,dba_indexes         ui
    where 1 =1
      and uip.partition_name  =utp.partition_name
      and ui.index_name       =uip.index_name
      and ui.table_name       =utp.table_name
      and ui.owner            ='XXCUS'
      and utp.table_name      =p_table -- 'XXCUS_OZF_GL_JOURNALS_B'
      and uip.partition_name  =p_partition
      order by uip.index_name;
  --
  procedure pop_journals_media_name (l_fiscal_month in varchar2, l_period in varchar2, l_gl_product in varchar2) is
  begin
   --
   savepoint square1;
   --
    merge /*+ PARALLEL (t 8) */ into xxcus.xxcus_ozf_gl_journals_b t
    using (
            select /*+ PARALLEL (med 8) */
                   med.media_name
                  ,med.media_id
            from   ams_media_tl med
            where  1 =1
           ) s
    on (
             t.fiscal_month      =l_fiscal_month
         and t.gl_period         =l_period
         and t.gl_product        =l_gl_product
         and t.activity_media_id =s.media_id
       )
    when matched then update set t.activity_media_name =s.media_name;
   --
  exception
   when others then
    rollback to square1;
    print_log ('@pop_journals_media_name, message ='||sqlerrm);
  end pop_journals_media_name;
  --
  procedure pop_journals_fund_name (l_fiscal_month in varchar2, l_period in varchar2, l_gl_product in varchar2) is
  begin
   --
   savepoint square1;
   --
    merge /*+ PARALLEL (t 8) */ into xxcus.xxcus_ozf_gl_journals_b t
    using (
            select /*+ PARALLEL (fun 8) */
                   fun.short_name
                  ,fun.fund_id fund_id
            from   ozf_funds_all_tl fun
            where  1 =1
           ) s
    on (
             t.fiscal_month      =l_fiscal_month
         and t.gl_period         =l_period
         and t.gl_product        =l_gl_product
         and t.fund_id           =s.fund_id
       )
    when matched then update set t.fund_name =s.short_name;
   --
  exception
   when others then
    rollback to square1;
    print_log ('@pop_journals_fund_name, message ='||sqlerrm);
  end pop_journals_fund_name;
  --
  procedure pop_journals_mvid_info (l_fiscal_month in varchar2, l_period in varchar2, l_gl_product in varchar2) is
  begin
   --
   savepoint square1;
   --
    merge /*+ PARALLEL (t 8) */ into xxcus.xxcus_ozf_gl_journals_b t
    using (
            select /*+ PARALLEL (hzca 8) PARALLEL(hz_parties 8) */
                   hzca.attribute2      attribute2
                  ,hzp.party_name       party_name
                  ,hzca.cust_account_id cust_account_id
            from   hz_cust_accounts hzca
                  ,hz_parties       hzp
            where  1 =1
              and  hzp.party_id =hzca.party_id
           ) s
    on (
             t.fiscal_month        =l_fiscal_month
         and t.gl_period           =l_period
         and t.gl_product          =l_gl_product
         and t.ofu_cust_account_id =s.cust_account_id
       )
    when matched then update set t.ofu_mvid =s.attribute2, t.ofu_mvid_name =s.party_name;
   --
  exception
   when others then
    rollback to square1;
    print_log ('@pop_journals_mvid_info, message ='||sqlerrm);
  end pop_journals_mvid_info;
  --
  procedure pop_journals_offer_info (l_fiscal_month in varchar2, l_period in varchar2, l_gl_product in varchar2) is
  begin
   --
   savepoint square1;
   --
    merge /*+ PARALLEL (t 8) */ into xxcus.xxcus_ozf_gl_journals_b t
    using (
            SELECT /*+ PARALLEL (offers 8) PARALLEL(qph 8) */
                   offers.offer_id                    offer_id
                  ,offers.offer_code                  offer_code
                  ,qph.description                    offer_name
                  ,qph.global_flag                    qp_global_flag
                  ,offers.offer_type                  offer_type
                  ,offers.autopay_party_id            autopay_party_id
                  ,offers.beneficiary_account_id      beneficiary_account_id
                  ,offers.status_code                 offer_status_code
                  ,offers.custom_setup_id             offer_custom_setup_id
                  ,offers.user_status_id              offer_user_status_id
                  ,offers.owner_id                    offer_owner_id
                  ,offers.activity_media_id           activity_media_id
                  ,offers.autopay_flag                offer_autopay_flag
                  ,qph.attribute1                     auto_renewal
                  ,qph.attribute7                     calendar_year
                  ,qph.attribute2                     until_year
                  ,qph.attribute5                     payment_method
                  ,qph.attribute6                     payment_frequency
                  ,qph.attribute9                     guaranteed_amount
                  ,qph.list_header_id                 list_header_id
            from   ozf_offers offers
                  ,qp_list_headers_vl qph
            where  1 =1
              and  offers.qp_list_header_id =qph.list_header_id
           ) s
    on (
             t.fiscal_month      =l_fiscal_month
         and t.gl_period         =l_period
         and t.gl_product        =l_gl_product
         and t.qp_list_header_id =s.list_header_id
       )
    when matched
     then update set t.offer_code =s.offer_code
                    ,t.offer_name =s.offer_name
                    ,t.qp_global_flag =s.qp_global_flag
                    ,t.offer_type =s.offer_type
                    ,t.offer_status_code =s.offer_status_code
                    ,t.activity_media_id =s.activity_media_id
                    ,t.calendar_year =s.calendar_year;
   --
  exception
   when others then
    rollback to square1;
    print_log ('@pop_journals_offer_info, message ='||sqlerrm);
  end pop_journals_offer_info;
  --
  begin --Main Processing
    --
    -- The ALTER session statements are the first ones to be invoked in the routine.
    --
    execute immediate 'ALTER SESSION FORCE PARALLEL DML';
    execute immediate 'ALTER TABLE XXCUS.XXCUS_OZF_GL_JOURNALS_B PARALLEL';
    --
    if p_territory ='US' then
     -- run for US rebates ledger
     n_ledger_id  :=2021;
     v_product    :='53';
     --
    elsif p_territory ='CA' then
     -- run for CANADA rebates ledger
     n_ledger_id  :=2023;
     v_product    :='18';
     --
    else
     n_ledger_id :=0;
     v_product :=Null;
    end if;
    --
    print_log ('');
    print_log('Note: Current session and the custom table are parallel enabled.');
    print_log('');
    print_log ('Parameter p_territory : '||p_territory);
    print_log ('Parameter p_period    : '||p_period);
    print_log ('Parameter p_account   : '||p_account);
    print_log ('Variable n_ledger_id  : '||n_ledger_id);
    print_log ('Variable v_product    : '||v_product);
    print_log ('');
    --
    begin
      --
      --Do not use NVL(p_period, gl_period) function as it will remove all other fiscal periods that belong in the same partition
      --
      DELETE /* PARALLEL (T 8) */ XXCUS.XXCUS_OZF_GL_JOURNALS_B T
      WHERE  1 =1
        AND  fiscal_month  =UPPER(SUBSTR(p_period, 1, 3)) --Required bcoz this is the partition name
        AND  gl_period     =p_period
        and  gl_product    =v_product;
      --
      if nvl(SQL%ROWCOUNT, 0) =0 then
       print_log ('Action -Cleanup: No TM GL journals deleted '||
                  'using partition ='||Upper(Substr(p_period, 1, 3)) ||
                  ', period =' ||p_period ||
                  ', product ='||v_product||
                  ' from table xxcus.xxcus_ozf_gl_journals_b'
                 );
      else
       print_log ('Action -Cleanup: Successfully deleted '|| SQL%ROWCOUNT ||
                  ' existing TM GL journals' ||
                  ' using partition ='||Upper(Substr(p_period, 1, 3)) ||
                  ', period =' ||p_period ||
                  ', product ='||v_product||
                  ' from table xxcus.xxcus_ozf_gl_journals_b'
                 );
      end if;
      print_log ('');
     --
      commit;
      --
      b_proceed :=TRUE;
     --
    exception
      when others then
       b_proceed :=FALSE;
       print_log ('@Delete, '||sqlerrm);
    end;
    --
    if (b_proceed) then
     --
     begin
      --
        INSERT /*+ APPEND PARALLEL (t 8) */ INTO XXCUS.XXCUS_OZF_GL_JOURNALS_B t
        (
            SELECT /*+ PARALLEL(jl 8) PARALLEL(ir 8) PARALLEL(gcc 8) PARALLEL(ofu 8) PARALLEL(rslines 8) */
                       jes.user_je_source_name            gl_source
                      ,jh.je_category                     gl_category
                      ,substr
                        (
                          UPPER(jh.period_name), 1, 3
                        )                                 fiscal_month
                      ,jh.period_name                     gl_period
                      ,xeh.period_name                    xla_accrual_period
                      ,xeh.gl_transfer_status_code        xaeh_gl_xfer_status_code
                      ,gcc.concatenated_segments          gl_segments
                      ,gcc.segment1                       gl_product
                      ,gcc.segment2                       gl_location
                      ,gcc.segment4                       gl_account
                      ,jl.effective_date                  jl_accounting_date
                      ,xeh.accounting_date                xaeh_accounting_date
                      ,xel.accounting_class_code          accounting_class_code
                      ,xeh.event_type_code                sla_event_type
                      ,gb.name                            gl_batch_name
                      ,jh.NAME                            gl_journal_name
                      ,jl.je_line_num                     gl_line_num
                      ,nvl
                        (
                         xdl.unrounded_accounted_dr, 0
                        )
                         -
                       nvl
                        (
                         xdl.unrounded_accounted_cr, 0
                        )                                 amount
                      ,xdl.source_distribution_type       source_distribution_type
                      ,xdl.source_distribution_id_num_1   utilization_id
                      ,gcc.code_combination_id            gl_ccid
                      ,xel.gl_sl_link_table               gl_sl_link_table
                      ,xel.gl_sl_link_id                  gl_sl_link_id
                      ,gle.name                           gl_ledger_name
                      ,gle.ledger_id                      gl_ledger_id
                      ,xeh.event_id                       event_id
                      ,xeh.entity_id                      entity_id
                      ,xeh.event_type_code                event_type_code
                      ,xel.ae_header_id                   ae_header_id
                      ,xel.ae_line_num                    ae_line_num
                      ,Null                               ofu_mvid_name
                      ,Null                               ofu_mvid
                      ,xel.gl_transfer_mode_code          xla_transfer_mode_code
                      ,ofu.adjustment_type                adjustment_type
                      ,ofu.adjustment_type_id             adjustment_type_id
                      ,ofu.object_id                      object_id
                      ,ofu.acctd_amount                   util_acctd_amount
                      ,ofu.utilization_type               utilization_type
                      ,ofu.cust_account_id                ofu_cust_account_id
                      ,ofu.plan_id                        qp_list_header_id
                      ,Null                               qp_global_flag
                      ,ofu.fund_id                        fund_id
                      ,Null                               fund_name
                      ,ofu.year_id                        ofu_year_id
                      ,ofu.reference_id                   reference_id
                      ,ofu.ams_activity_budget_id         ams_activity_budget_id
                      ,Null                               offer_code
                      ,Null                               offer_name
                      ,Null                               offer_type
                      ,Null                               offer_status_code
                      ,Null                               activity_media_id
                      ,Null                               activity_media_name
                      ,Null                               calendar_year
                      ,rslines.bill_to_party_name         bill_to_party_name
                      ,trunc(rslines.creation_date)       resale_date_created
                      ,trunc(rslines.date_invoiced)       date_invoiced
                      ,rslines.po_number                  po_number
                      ,trunc(rslines.date_ordered)        date_ordered
                      ,rslines.selling_price              selling_price
                      ,rslines.quantity                   quantity
            FROM gl_je_lines jl,
               gl_je_headers jh,
               gl_je_sources jes,
               gl_je_categories jec,
               gl_import_references ir,
               xla_ae_lines xel,
               xla_ae_headers xeh,
               xla_distribution_links xdl,
               gl_code_combinations_kfv gcc,
               gl_ledgers gle,
               gl_period_statuses gps,
               gl_je_batches gb,
               ozf_funds_utilized_all_b ofu,
               ozf_resale_lines_all rslines
            WHERE     1 = 1
               and gps.period_name     =p_period                     --GL PERIOD Any period
               and gcc.segment1        =v_product                    --GL PRODUCT 53 OR 18
               and gcc.segment4        =p_account                    --GL ACCOUNT '131100'
               AND gle.ledger_id       =n_ledger_id
               AND gps.application_id  =101
               AND jl.je_header_id     =jh.je_header_id
               AND jes.je_source_name  =jh.je_source
               AND jes.user_je_source_name =v_tm_journals            --GL SOURCE
               AND jh.je_category = jec.je_category_name
               AND ir.je_header_id = jl.je_header_id
               AND gcc.code_combination_id = jl.code_combination_id
               AND gps.ledger_id = gle.ledger_id
               and jh.ledger_id =gle.ledger_id
               AND gps.period_name = jh.period_name
               AND gle.chart_of_Accounts_id = gcc.chart_of_accounts_id
               AND jh.je_batch_id = gb.je_batch_id
               AND ir.je_line_num = jl.je_line_num
               AND xel.application_id(+) =682
               AND ir.gl_sl_link_id = xel.gl_sl_link_id(+)
               AND ir.gl_sl_link_table = xel.gl_sl_link_table(+)
               AND xdl.application_id(+) =682
               AND xel.ae_header_id = xdl.ae_header_id(+)
               AND xel.ae_line_num = xdl.ae_line_num(+)
               AND xel.accounting_class_code(+) =v_acct_class_code  --Accounting class code EXPENSE
               AND xdl.application_id =682
               AND xeh.ae_header_id(+) = xel.ae_header_id
               AND ofu.utilization_id(+) =xdl.source_distribution_id_num_1
               AND rslines.resale_line_id(+) =ofu.object_id
        ); --End of insert statement
      --
      print_log ('Source: '||v_tm_journals||', total records inserted into the table xxcus.xxcus_ozf_gl_journals_b =>'||SQL%ROWCOUNT);
      print_log ('');
      --
      Commit;
      --
      begin
         --
         print_log ('Begin update of GL Journals accrual attributes.');
         pop_journals_mvid_info
          (
            l_fiscal_month =>UPPER(SUBSTR(p_period, 1, 3)) --Required bcoz this is the partition name
           ,l_period       =>p_period
           ,l_gl_product   =>v_product
          );
         Commit;
         v_loc :='101';
         --
         pop_journals_fund_name
          (
            l_fiscal_month =>UPPER(SUBSTR(p_period, 1, 3)) --Required bcoz this is the partition name
           ,l_period       =>p_period
           ,l_gl_product   =>v_product
          );
         Commit;
         v_loc :='102';
         --
         pop_journals_offer_info --Need this executed before calling pop_accruals_media_name
          (
            l_fiscal_month =>UPPER(SUBSTR(p_period, 1, 3)) --Required bcoz this is the partition name
           ,l_period       =>p_period
           ,l_gl_product   =>v_product
          );
         Commit;
         v_loc :='103';
         --
         pop_journals_media_name
          (
            l_fiscal_month =>UPPER(SUBSTR(p_period, 1, 3)) --Required bcoz this is the partition name
           ,l_period       =>p_period
           ,l_gl_product   =>v_product
          );
         Commit;
         v_loc :='104';
         --
         print_log ('End update of GL Journals accrual attributes.');
         print_log ('');
         --
      exception
       when others then
        print_log ('@GL Journals attributes update, last known location ='||v_loc||', Error =>'||sqlerrm);
        print_log ('');
      end;
      --
      --
      INSERT /*+ APPEND PARALLEL(8) */ INTO XXCUS.XXCUS_OZF_GL_JOURNALS_B
      (
            SELECT /*+ PARALLEL(jl 8) PARALLEL(ir 8) PARALLEL(gcc 8) */
                       jes.user_je_source_name            gl_source
                      ,jh.je_category                     gl_category
                      ,substr
                        (
                          UPPER(jh.period_name), 1, 3
                        )                                 fiscal_month
                      ,jh.period_name                     gl_period
                      ,xeh.period_name                    xla_accrual_period
                      ,xeh.gl_transfer_status_code        xaeh_gl_xfer_status_code
                      ,gcc.concatenated_segments          gl_segments
                      ,gcc.segment1                       gl_product
                      ,gcc.segment2                       gl_location
                      ,gcc.segment4                       gl_account
                      ,jl.effective_date                  jl_accounting_date
                      ,xeh.accounting_date                xaeh_accounting_date
                      ,xel.accounting_class_code          accounting_class_code
                      ,xeh.event_type_code                sla_event_type
                      ,gb.name                            gl_batch_name
                      ,jh.NAME                            gl_journal_name
                      ,jl.je_line_num                     gl_line_num
                      ,nvl
                        (
                         xdl.unrounded_accounted_dr, 0
                        )
                         -
                       nvl
                        (
                         xdl.unrounded_accounted_cr, 0
                        )                                 amount
                      ,xdl.source_distribution_type       source_distribution_type
                      ,xdl.source_distribution_id_num_1   utilization_id
                      ,gcc.code_combination_id            gl_ccid
                      ,xel.gl_sl_link_table               gl_sl_link_table
                      ,xel.gl_sl_link_id                  gl_sl_link_id
                      ,gle.name                           gl_ledger_name
                      ,gle.ledger_id                      gl_ledger_id
                      ,xeh.event_id                       event_id
                      ,xeh.entity_id                      entity_id
                      ,xeh.event_type_code                event_type_code
                      ,xel.ae_header_id                   ae_header_id
                      ,xel.ae_line_num                    ae_line_num
                      ,Null                               ofu_mvid_name
                      ,Null                               ofu_mvid
                      ,xel.gl_transfer_mode_code          xla_transfer_mode_code
                      ,Null                               adjustment_type
                      ,Null                               adjustment_type_id
                      ,Null                               object_id
                      ,Null                               util_acctd_amount
                      ,Null                               utilization_type
                      ,Null                               ofu_cust_account_id
                      ,Null                               qp_list_header_id
                      ,Null                               qp_global_flag
                      ,Null                               fund_id
                      ,Null                               fund_name
                      ,Null                               ofu_year_id
                      ,Null                               reference_id
                      ,Null                               ams_activity_budget_id
                      ,Null                               offer_code
                      ,Null                               offer_name
                      ,Null                               offer_type
                      ,Null                               offer_status_code
                      ,Null                               activity_media_id
                      ,Null                               activity_media_name
                      ,Null                               calendar_year
                      ,Null                               bill_to_party_name
                      ,Null                               resale_date_created
                      ,Null                               date_invoiced
                      ,Null                               po_number
                      ,Null                               date_ordered
                      ,Null                               selling_price
                      ,Null                               quantity
            FROM gl_je_lines jl,
               gl_je_headers jh,
               gl_je_sources jes,
               gl_je_categories jec,
               gl_import_references ir,
               xla_ae_lines xel,
               xla_ae_headers xeh,
               xla_distribution_links xdl,
               gl_code_combinations_kfv gcc,
               gl_ledgers gle,
               gl_period_statuses gps,
               gl_je_batches gb
            WHERE     1 = 1
               and gps.period_name     =p_period                     --GL PERIOD Any period
               and gcc.segment1        =v_product                    --GL PRODUCT 53 OR 18
               and gcc.segment4        =p_account                    --GL ACCOUNT '131100'
               AND gle.ledger_id       =n_ledger_id
               AND gps.application_id  =101
               AND jl.je_header_id = jh.je_header_id
               AND jes.je_source_name = jh.je_source
               AND jes.user_je_source_name =v_ar_journals            --GL SOURCE
               AND jh.je_category = jec.je_category_name
               AND ir.je_header_id = jl.je_header_id
               AND gcc.code_combination_id = jl.code_combination_id
               AND gps.ledger_id = gle.ledger_id
               and jh.ledger_id =gle.ledger_id
               AND gps.period_name = jh.period_name
               AND gle.chart_of_Accounts_id = gcc.chart_of_accounts_id
               AND jh.je_batch_id = gb.je_batch_id
               AND ir.je_line_num = jl.je_line_num
               AND xel.application_id(+) =222
               AND ir.gl_sl_link_id = xel.gl_sl_link_id(+)
               AND ir.gl_sl_link_table = xel.gl_sl_link_table(+)
               AND xdl.application_id(+) =222
               AND xel.ae_header_id = xdl.ae_header_id(+)
               AND xel.ae_line_num = xdl.ae_line_num(+)
               AND xdl.application_id =222
               AND xeh.ae_header_id(+) = xel.ae_header_id
      );
      --
      print_log ('Source: '||v_ar_journals||', total records inserted into the table xxcus.xxcus_ozf_gl_journals_b =>'||SQL%ROWCOUNT);
      print_log ('');
      --
      Commit;
      --
     exception
      when others then
       print_log('Error in insert of OZF GL Journals, Message =>'||sqlerrm);
       print_log ('');
       b_proceed :=FALSE;
     end;
     --
    else
     print_log('Issue in delete of old data from XXCUS.xxcus_ozf_gl_journals_b, message =>'||sqlerrm);
     print_log ('');
    end if;
    --
    begin
     --
     print_log ('Begin rebuild of partitioned indexes...');
     --
     for rec in rebuild_partitioned_indexes (p_table =>'XXCUS_OZF_GL_JOURNALS_B', p_partition =>UPPER(SUBSTR(p_period, 1, 3))) loop
      --
       v_sql :=rec.idx_sql;
      --
        execute immediate rec.idx_sql;
      --
      print_log ('...Rebuild Partition SQL: '||rec.idx_sql||' was executed successfully');
      --
     end loop;
     --
     print_log ('End rebuild of partitioned indexes...');
     print_log ('');
     --
    exception
     when others then
      print_log ('Issue in rebuild of partitioned index, current sql =>'||v_sql);
      print_log ('');
    end;
    --
  exception
   when others then
    print_log ('Issue in extract_journals routine, message ='||sqlerrm);
    print_log ('');
    rollback;
  end extract_journals;
  --
  -- Begin TMS 20160316-00196
  procedure extract_rebate_journals
  (
    retcode               out number
   ,errbuf                out varchar2
   ,p_territory           in  varchar2
   ,p_period              in  varchar2
   ,p_account             in  varchar2
  ) is
   --
   v_sql              VARCHAR2(240) :=Null;
   N_Count            NUMBER;
   --
   b_proceed          BOOLEAN;
   b_backup           BOOLEAN;
   --
   v_loc              VARCHAR2(3);
   v_product          VARCHAR2(2) :=Null;
   v_del_sql varchar2(2000) :=Null;
   v_ar_journals      VARCHAR2(40) :='Receivables';
   --
   v_tm_journals      VARCHAR2(40) :='Channel Management';
   v_acct_class_code  VARCHAR2(20) :='EXPENSE';
   --
   n_ledger_id        NUMBER :=0;
   l_count NUMBER :=0;
   l_fiscal_mon VARCHAR2(3) :=UPPER(Substr(p_period, 1, 3));
   --
   cursor rebuild_partitioned_indexes (p_table in varchar2, p_partition in varchar2) is
    select 'ALTER INDEX '||owner||'.'||uip.index_name||' REBUILD PARTITION '||uip.partition_name idx_sql
    --select 'ALTER INDEX '||owner||'.'||uip.index_name||' REBUILD PARTITION '||uip.partition_name idx_sq
    from dba_ind_partitions  uip
        ,dba_tab_partitions  utp
        ,dba_indexes         ui
    where 1 =1
      and uip.partition_name  =utp.partition_name
      and ui.index_name       =uip.index_name
      and ui.table_name       =utp.table_name
      and ui.owner            ='XXCUS'
      and utp.table_name      =p_table -- 'XXCUS_OZF_GL_JOURNALS_B'
      and uip.partition_name  =p_partition
      order by uip.index_name;
  --
  begin --Main Processing
    --
    -- The ALTER session statements are the first ones to be invoked in the routine.
    --
    execute immediate 'ALTER SESSION FORCE PARALLEL DML';
    execute immediate 'ALTER TABLE XXCUS.XXCUS_OZF_GL_JOURNALS_B PARALLEL 8';
    --
    if p_territory ='US' then
     -- run for US rebates ledger
     n_ledger_id  :=2021;
     v_product    :='53';
     --
    elsif p_territory ='CA' then
     -- run for CANADA rebates ledger
     n_ledger_id  :=2023;
     v_product    :='18';
     --
    else
     n_ledger_id :=0;
     v_product :=Null;
    end if;
    --
    print_log ('');
    print_log('Note: Current session and the custom table are parallel enabled.');
    print_log('');
    print_log ('Parameter p_territory : '||p_territory);
    print_log ('Parameter p_period    : '||p_period);
    print_log ('Parameter p_account   : '||p_account);
    print_log ('Variable n_ledger_id  : '||n_ledger_id);
    print_log ('Variable v_product    : '||v_product);
    print_log ('');
    --
    begin
      --
      --Do not use NVL(p_period, gl_period) function as it will remove all other fiscal periods that belong in the same partition
      --
      --
      begin
        select 'DELETE /*+ PARALLEL (D1 4) */ XXCUS.XXCUS_OZF_GL_JOURNALS_B partition ('  --TMS 20160316-00196
             ||l_fiscal_mon
             ||') D1 '
             ||'
        '
        ||' WHERE 1 =1 '
        ||'
        '
        ||' AND D1.gl_period ='||''''||p_period||''''
        ||'
        '
        ||' AND D1.gl_product ='||''''||v_product||''''
        into v_del_sql
        from dual;
        --
        --
           print_log('Delete SQL: ');
           print_log('==========');
           print_log(v_del_sql);
           print_log(' ');
        --
        print_log ('Begin Delete : Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
        execute immediate 'begin ' || v_del_sql || '; :x := sql%rowcount; end;' using OUT l_count;
        print_log ('End Delete : Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
        --
      exception
       when others then
        Null;
      end;
      --
      if l_count =0 then
       print_log ('Action -Cleanup: No TM GL journals deleted '||
                  'using partition ='||Upper(Substr(p_period, 1, 3)) ||
                  ', period =' ||p_period ||
                  ', product ='||v_product||
                  ' from table xxcus.xxcus_ozf_gl_journals_b'
                 );
      else
       print_log ('Action -Cleanup: Successfully deleted '||l_count ||
                  ' existing TM GL journals' ||
                  ' using partition ='||Upper(Substr(p_period, 1, 3)) ||
                  ', period =' ||p_period ||
                  ', product ='||v_product||
                  ' from table xxcus.xxcus_ozf_gl_journals_b'
                 );
      end if;
      print_log ('');
     --
      commit;
      --
      b_proceed :=TRUE;
     --
    exception
      when others then
       b_proceed :=FALSE;
       print_log ('@Delete, '||sqlerrm);
    end;
    --
    if (b_proceed) then
     --
     begin
      --
        INSERT /*+ APPEND PARALLEL (t 8) */ INTO XXCUS.XXCUS_OZF_GL_JOURNALS_B t
        (
           SELECT /*+ PARALLEL(jl 8) PARALLEL(ir 8) PARALLEL(gcc 8) PARALLEL(ofu 8) PARALLEL(rslines 8) */
                       jes.user_je_source_name            gl_source
                      ,jh.je_category                     gl_category
                      ,substr
                        (
                          UPPER(jh.period_name), 1, 3
                        )                                 fiscal_month
                      ,jh.period_name                     gl_period
                      ,xeh.period_name                    xla_accrual_period
                      ,xeh.gl_transfer_status_code        xaeh_gl_xfer_status_code
                      ,gcc.concatenated_segments          gl_segments
                      ,gcc.segment1                       gl_product
                      ,gcc.segment2                       gl_location
                      ,gcc.segment4                       gl_account
                      ,jl.effective_date                  jl_accounting_date
                      ,xeh.accounting_date                xaeh_accounting_date
                      ,xel.accounting_class_code          accounting_class_code
                      ,xeh.event_type_code                sla_event_type
                      ,gb.name                            gl_batch_name
                      ,jh.NAME                            gl_journal_name
                      ,jl.je_line_num                     gl_line_num
                      ,nvl
                        (
                         xdl.unrounded_accounted_dr, 0
                        )
                         -
                       nvl
                        (
                         xdl.unrounded_accounted_cr, 0
                        )                                 amount
                      ,xdl.source_distribution_type       source_distribution_type
                      ,xdl.source_distribution_id_num_1   utilization_id
                      ,gcc.code_combination_id            gl_ccid
                      ,xel.gl_sl_link_table               gl_sl_link_table
                      ,xel.gl_sl_link_id                  gl_sl_link_id
                      ,gle.name                           gl_ledger_name
                      ,gle.ledger_id                      gl_ledger_id
                      ,xeh.event_id                       event_id
                      ,xeh.entity_id                      entity_id
                      ,xeh.event_type_code                event_type_code
                      ,xel.ae_header_id                   ae_header_id
                      ,xel.ae_line_num                    ae_line_num
                      ,mvid.party_name                               ofu_mvid_name
                      ,mvid.customer_attribute2                               ofu_mvid
                      ,xel.gl_transfer_mode_code          xla_transfer_mode_code
                      ,ofu.adjustment_type                adjustment_type
                      ,ofu.adjustment_type_id             adjustment_type_id
                      ,ofu.object_id                      object_id
                      ,ofu.acctd_amount                   util_acctd_amount
                      ,ofu.utilization_type               utilization_type
                      ,ofu.cust_account_id                ofu_cust_account_id
                      ,ofu.plan_id                        qp_list_header_id
                      ,qpvl.global_flag                               qp_global_flag
                      ,ofu.fund_id                        fund_id
                      ,(
                             select /*+ RESULT_CACHE */
                                   fun.short_name
                            from   ozf_funds_all_tl fun
                            where  1 =1
                                  and fun.fund_id =ofu.fund_id
                       ) fund_name
                      ,ofu.year_id                        ofu_year_id
                      ,ofu.reference_id                   reference_id
                      ,ofu.ams_activity_budget_id         ams_activity_budget_id
                      ,offers.offer_code                               offer_code
                      ,qpvl.description                                offer_name
                      ,offers.offer_type                               offer_type
                      ,offers.status_code                               offer_status_code
                      ,offers.activity_media_id activity_media_id
                      ,(
                            select /*+ RESULT_CACHE */
                                   med.media_name
                            from   ams_media_tl med
                            where  1 =1
                                  and med.media_id =offers.activity_media_id
                       )                                     activity_media_name
                      ,qpvl.attribute7                               calendar_year
                      ,rslines.bill_to_party_name         bill_to_party_name
                      ,trunc(rslines.creation_date)       resale_date_created
                      ,trunc(rslines.date_invoiced)       date_invoiced
                      ,rslines.po_number                  po_number
                      ,trunc(rslines.date_ordered)        date_ordered
                      ,rslines.selling_price              selling_price
                      ,rslines.quantity                   quantity
            FROM gl_je_lines jl,
               gl_je_headers jh,
               gl_je_sources jes,
               gl_je_categories jec,
               gl_import_references ir,
               xla_ae_lines partition (OZF) xel,
               xla_ae_headers partition (OZF) xeh,
               xla_distribution_links partition (OZF) xdl,
               gl_code_combinations_kfv gcc,
               gl_ledgers gle,
               gl_period_statuses gps,
               gl_je_batches gb,
               ozf_funds_utilized_all_b ofu,
               ozf_resale_lines_all rslines,
               ozf_offers offers,
               qp_list_headers_vl qpvl,
               xxcus.xxcus_rebate_customers mvid
            WHERE     1 = 1
               and gps.period_name  =p_period                     --GL PERIOD Any period
               and gcc.segment1        =v_product                    --GL PRODUCT 53 OR 18
               and gcc.segment4        =p_account                    --GL ACCOUNT '131100'
               AND gle.ledger_id       =n_ledger_id
               AND gps.application_id  =101
               AND jl.je_header_id     =jh.je_header_id
               AND jes.je_source_name  =jh.je_source
               AND jes.user_je_source_name =v_tm_journals            --GL SOURCE
               AND jh.je_category = jec.je_category_name
               AND ir.je_header_id = jl.je_header_id
               AND gcc.code_combination_id = jl.code_combination_id
               AND gps.ledger_id = gle.ledger_id
               and jh.ledger_id =gle.ledger_id
               AND gps.period_name = jh.period_name
               AND gle.chart_of_Accounts_id = gcc.chart_of_accounts_id
               AND jh.je_batch_id = gb.je_batch_id
               AND ir.je_line_num = jl.je_line_num
               AND xel.application_id(+) =682
               AND ir.gl_sl_link_id = xel.gl_sl_link_id(+)
               AND ir.gl_sl_link_table = xel.gl_sl_link_table(+)
               AND xdl.application_id(+) =682
               AND xel.ae_header_id = xdl.ae_header_id(+)
               AND xel.ae_line_num = xdl.ae_line_num(+)
               AND xel.accounting_class_code(+) =v_acct_class_code  --Accounting class code EXPENSE
               AND xdl.application_id =682
               AND xeh.ae_header_id(+) = xel.ae_header_id
               AND ofu.utilization_id(+) =xdl.source_distribution_id_num_1
               AND rslines.resale_line_id(+) =ofu.object_id
               AND offers.qp_list_header_id(+) =ofu.plan_id
               AND qpvl.list_header_id(+) =offers.qp_list_header_id
               AND mvid.customer_id(+) =ofu.cust_account_id
        ); --End of insert statement
      --
      print_log ('Source: '||v_tm_journals||', total records inserted into the table xxcus.xxcus_ozf_gl_journals_b =>'||SQL%ROWCOUNT);
      print_log ('');
      --
      Commit;
      --
      INSERT /*+ APPEND PARALLEL(8) */ INTO XXCUS.XXCUS_OZF_GL_JOURNALS_B
      (
            SELECT /*+ PARALLEL(jl 8) PARALLEL(ir 8) PARALLEL(gcc 8) */
                       jes.user_je_source_name            gl_source
                      ,jh.je_category                     gl_category
                      ,substr
                        (
                          UPPER(jh.period_name), 1, 3
                        )                                 fiscal_month
                      ,jh.period_name                     gl_period
                      ,xeh.period_name                    xla_accrual_period
                      ,xeh.gl_transfer_status_code        xaeh_gl_xfer_status_code
                      ,gcc.concatenated_segments          gl_segments
                      ,gcc.segment1                       gl_product
                      ,gcc.segment2                       gl_location
                      ,gcc.segment4                       gl_account
                      ,jl.effective_date                  jl_accounting_date
                      ,xeh.accounting_date                xaeh_accounting_date
                      ,xel.accounting_class_code          accounting_class_code
                      ,xeh.event_type_code                sla_event_type
                      ,gb.name                            gl_batch_name
                      ,jh.NAME                            gl_journal_name
                      ,jl.je_line_num                     gl_line_num
                      ,nvl
                        (
                         xdl.unrounded_accounted_dr, 0
                        )
                         -
                       nvl
                        (
                         xdl.unrounded_accounted_cr, 0
                        )                                 amount
                      ,xdl.source_distribution_type       source_distribution_type
                      ,xdl.source_distribution_id_num_1   utilization_id
                      ,gcc.code_combination_id            gl_ccid
                      ,xel.gl_sl_link_table               gl_sl_link_table
                      ,xel.gl_sl_link_id                  gl_sl_link_id
                      ,gle.name                           gl_ledger_name
                      ,gle.ledger_id                      gl_ledger_id
                      ,xeh.event_id                       event_id
                      ,xeh.entity_id                      entity_id
                      ,xeh.event_type_code                event_type_code
                      ,xel.ae_header_id                   ae_header_id
                      ,xel.ae_line_num                    ae_line_num
                      ,Null                               ofu_mvid_name
                      ,Null                               ofu_mvid
                      ,xel.gl_transfer_mode_code          xla_transfer_mode_code
                      ,Null                               adjustment_type
                      ,Null                               adjustment_type_id
                      ,Null                               object_id
                      ,Null                               util_acctd_amount
                      ,Null                               utilization_type
                      ,Null                               ofu_cust_account_id
                      ,Null                               qp_list_header_id
                      ,Null                               qp_global_flag
                      ,Null                               fund_id
                      ,Null                               fund_name
                      ,Null                               ofu_year_id
                      ,Null                               reference_id
                      ,Null                               ams_activity_budget_id
                      ,Null                               offer_code
                      ,Null                               offer_name
                      ,Null                               offer_type
                      ,Null                               offer_status_code
                      ,Null                               activity_media_id
                      ,Null                               activity_media_name
                      ,Null                               calendar_year
                      ,Null                               bill_to_party_name
                      ,Null                               resale_date_created
                      ,Null                               date_invoiced
                      ,Null                               po_number
                      ,Null                               date_ordered
                      ,Null                               selling_price
                      ,Null                               quantity
            FROM gl_je_lines jl,
               gl_je_headers jh,
               gl_je_sources jes,
               gl_je_categories jec,
               gl_import_references ir,
               xla_ae_lines partition (AR) xel,
               xla_ae_headers partition (AR) xeh,
               xla_distribution_links partition (AR) xdl,
               gl_code_combinations_kfv gcc,
               gl_ledgers gle,
               gl_period_statuses gps,
               gl_je_batches gb
            WHERE     1 = 1
               and gps.period_name     =p_period                     --GL PERIOD Any period
               and gcc.segment1        =v_product                    --GL PRODUCT 53 OR 18
               and gcc.segment4        =p_account                    --GL ACCOUNT '131100'
               AND gle.ledger_id       =n_ledger_id
               AND gps.application_id  =101
               AND jl.je_header_id = jh.je_header_id
               AND jes.je_source_name = jh.je_source
               AND jes.user_je_source_name =v_ar_journals            --GL SOURCE
               AND jh.je_category = jec.je_category_name
               AND ir.je_header_id = jl.je_header_id
               AND gcc.code_combination_id = jl.code_combination_id
               AND gps.ledger_id = gle.ledger_id
               and jh.ledger_id =gle.ledger_id
               AND gps.period_name = jh.period_name
               AND gle.chart_of_Accounts_id = gcc.chart_of_accounts_id
               AND jh.je_batch_id = gb.je_batch_id
               AND ir.je_line_num = jl.je_line_num
               AND xel.application_id(+) =222
               AND ir.gl_sl_link_id = xel.gl_sl_link_id(+)
               AND ir.gl_sl_link_table = xel.gl_sl_link_table(+)
               AND xdl.application_id(+) =222
               AND xel.ae_header_id = xdl.ae_header_id(+)
               AND xel.ae_line_num = xdl.ae_line_num(+)
               AND xdl.application_id =222
               AND xeh.ae_header_id(+) = xel.ae_header_id
      );
      --
      print_log ('Source: '||v_ar_journals||', total records inserted into the table xxcus.xxcus_ozf_gl_journals_b =>'||SQL%ROWCOUNT);
      print_log ('');
      --
      Commit;
      --
     exception
      when others then
       print_log('Error in insert of OZF GL Journals, Message =>'||sqlerrm);
       print_log ('');
       b_proceed :=FALSE;
     end;
     --
    else
     print_log('Issue in delete of old data from XXCUS.xxcus_ozf_gl_journals_b, message =>'||sqlerrm);
     print_log ('');
    end if;
    --
    begin
     --
     print_log ('Begin rebuild of partitioned indexes...');
     --
     for rec in rebuild_partitioned_indexes (p_table =>'XXCUS_OZF_GL_JOURNALS_B', p_partition =>UPPER(SUBSTR(p_period, 1, 3))) loop
      --
       v_sql :=rec.idx_sql;
      --
        execute immediate rec.idx_sql;
      --
      print_log ('...Rebuild Partition SQL: '||rec.idx_sql||' was executed successfully');
      --
     end loop;
     --
     print_log ('End rebuild of partitioned indexes...');
     print_log ('');
     --
    exception
     when others then
      print_log ('Issue in rebuild of partitioned index, current sql =>'||v_sql);
      print_log ('');
    end;
    --
  exception
   when others then
    print_log ('Issue in extract_rebate_journals routine, message ='||sqlerrm);
    print_log ('');
    rollback;
  end extract_rebate_journals;
  --
  -- End TMS 20160316-00196
  --
  function pivot_tm_gl
     (
       p_org_id in number
      ,p_period in varchar2
     ) return xxcus.tm_gl_recon_t pipelined is
  --
  type RefCur IS REF CURSOR;
  DySQL RefCur;
  --
  cursor get_pivot
     (
        l_gl_category in varchar2
       ,l_ledger_name in varchar2
       ,l_period      in varchar2
       ,l_org_id      in number
     ) is
    select
      ozf_accruals.xaeh_period_name          ozf_period_name
     ,ozf_accruals.ozf_ledger_name           ozf_ledger_name
     ,ozf_accruals.xaeh_gl_xfer_status_code  ozf_gl_xfer_status_code
     ,ozf_accruals.xlae_event_status_code    ozf_event_status_code
     ,ozf_accruals.xlae_process_status_code  ozf_process_status_code
     ,count(ozf_accruals.xla_accrual_id)     ozf_accrual_count
     ,sum(ozf_accruals.util_acctd_amount)    ozf_accrual_total
     ,sum(ozf_gl.journal_amount)             tm_gl_posted_amount
    from
      xxcus.xxcus_ozf_xla_accruals_b  ozf_accruals
    ,(
        select utilization_id  jl_utilization_id
              ,sum(amount)     journal_amount
        from   xxcus.xxcus_ozf_gl_journals_b
        where  1 =1
          and  gl_period      =l_period
          and  utilization_id =utilization_id
          and  gl_ledger_name =l_ledger_name -- we will derive this auto using the org_id
          and  gl_category    =l_gl_category
        group by utilization_id
      ) ozf_gl
    where  1 =1
      and ozf_accruals.xaeh_period_name =l_period
      and ozf_accruals.oxa_org_id       =l_org_id
      and ozf_gl.jl_utilization_id(+)   =ozf_accruals.utilization_id
    group by
      ozf_accruals.xaeh_period_name
     ,ozf_accruals.ozf_ledger_name
     ,ozf_accruals.xaeh_gl_xfer_status_code
     ,ozf_accruals.xlae_event_status_code
     ,ozf_accruals.xlae_process_status_code
     ,ozf_accruals.oxa_org_id --commenting this bcoz we are going for a certain org using parameter
    order by
      ozf_accruals.xaeh_period_name
    ,ozf_accruals.xlae_event_status_code
    ,ozf_accruals.xlae_process_status_code
    ,ozf_accruals.xaeh_gl_xfer_status_code;
  --
  get_pivot_row get_pivot%rowtype;
  --
  p_ledger_name       xxcus.xxcus_ozf_gl_journals_b.gl_ledger_name%type :=Null;
  b_proceed           BOOLEAN;
  --
  begin
   --
   begin
    --
    select a.name
    into   p_ledger_name
    from   gl_ledgers a
    where  1 =1
      and  exists
       (
         select 1
         from   hr_operating_units
         where  1 =1
           and  organization_id =p_org_id
           and  set_of_books_id =a.ledger_id
       );
    --
    b_proceed :=TRUE;
    --
   exception
    when no_data_found then
     b_proceed :=FALSE;
    when others then
     b_proceed :=FALSE;
   end;
   -- ledger_name is derived successfully
   if (b_proceed) then
   --
    OPEN get_pivot
      (
        l_gl_category  =>'Channel Accrual'
       ,l_ledger_name  =>p_ledger_name
       ,l_period       =>p_period
       ,l_org_id       =>p_org_id
      );
   --
    LOOP
     --
     FETCH  get_pivot
      INTO  get_pivot_row.ozf_period_name
           ,get_pivot_row.ozf_ledger_name
           ,get_pivot_row.ozf_gl_xfer_status_code
           ,get_pivot_row.ozf_event_status_code
           ,get_pivot_row.ozf_process_status_code
           ,get_pivot_row.ozf_accrual_count
           ,get_pivot_row.ozf_accrual_total
           ,get_pivot_row.tm_gl_posted_amount;
     EXIT WHEN get_pivot%NOTFOUND;
     --
      PIPE ROW (
                 xxcus.tm_gl_recon_obj
                   (
                     get_pivot_row.ozf_period_name
                    ,get_pivot_row.ozf_ledger_name
                    ,get_pivot_row.ozf_gl_xfer_status_code
                    ,get_pivot_row.ozf_event_status_code
                    ,get_pivot_row.ozf_process_status_code
                    ,get_pivot_row.ozf_accrual_count
                    ,get_pivot_row.ozf_accrual_total
                    ,get_pivot_row.tm_gl_posted_amount
                   )
                );
     --
    END LOOP;
    CLOSE get_pivot;
    RETURN;
    --
   end if;
   --
  exception
   when others then
    raise_application_error (-20010, 'Issue in tm_gl_recon_by_period_org, msg ='||sqlerrm);
   --
  end pivot_tm_gl;
  --
--Start for TMS#20180530-00051 by Ashwin.S on 31-May-2018
PROCEDURE parallel_zero
 (
    retcode  OUT NUMBER
   ,errbuf   OUT VARCHAR2
 ) IS

CURSOR cu_idx IS
SELECT 'ALTER INDEX '||owner||'.'||index_name||' NOPARALLEL' ALTER_IDX 
FROM all_indexes
WHERE 1 =1
AND owner IN ('OZF', 'APPS', 'XXCUS')
AND (TABLE_NAME LIKE 'XXCUS%' OR table_name LIKE 'OZF%') 
AND TO_NUMBER(TRIM(degree)) >1;
 
lv_query VARCHAR2(2000);
ln_count NUMBER:=0;

BEGIN
 
  fnd_file.put_line(fnd_file.log, 'Inside Start of Procedure...');
  
  fnd_file.put_line(fnd_file.log, 'Starting the LOOP...');
 
  FOR rec_idx IN cu_idx LOOP
  
    ln_count:=ln_count+1;
    fnd_file.put_line(fnd_file.log, 'Index Name...'||rec_idx.ALTER_IDX);

    EXECUTE IMMEDIATE rec_idx.ALTER_IDX;
  
  END LOOP;
  
  fnd_file.put_line(fnd_file.log, 'Index Altered Successfully...');
  fnd_file.put_line(fnd_file.log, 'Record Count...'||ln_count);
   
EXCEPTION
WHEN others THEN
    
  fnd_file.put_line(fnd_file.log, 'Issue in parallel_zero procedure ='||sqlerrm);

  --
END parallel_zero;
--End for TMS#20180530-00051 by Ashwin.S on 31-May-2018
end XXCUS_OZF_TM_RECON;
/