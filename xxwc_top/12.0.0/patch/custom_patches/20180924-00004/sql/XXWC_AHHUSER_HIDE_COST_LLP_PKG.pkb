CREATE OR REPLACE 
PACKAGE BODY XXWC_AHHUSER_HIDE_COST_LLP_PKG 
AS
/*************************************************************************
     $Header XXWC_AHHUSER_HIDE_COST_LLP_PKG $
     Module Name: XXWC_AHHUSER_HIDE_COST_LLP_PKG.pkb

     PURPOSE:   Setting the profile to AHH Users not to see the Margins for orders

     REVISIONS:
     Ver        Date        Author                  Description
     ---------  ----------  ---------------         -------------------------
     1.0        09/20/2018  Pattabhi Avula          TMS#20180924-00004 - Last price paid for North East AHH Branches
**************************************************************************/
g_err_callfrom          VARCHAR2(100):='XXWC_AHHUSER_HIDE_COST_LLP_PKG';
g_distro_list           VARCHAR2(100):='WC-ITDevelopment-U1@HDSupply.com';
g_module                VARCHAR2(10):='OM';

  -- ==============================================================================================================================
  -- Procedure: MAIN
  -- Purpose: Main procedure calling sub programs
  -- ==============================================================================================================================
  --  REVISIONS:
  --   Ver        Date        Author               Description
  --  ---------  ---------- ---------------  --------------------------------------------------------------------------------------
  --  1.0        09/20/2018  Pattabhi Avula          TMS#20180924-00004 - Last price paid for North East AHH Branches
  -- ============================================================================================================================== 

PROCEDURE MAIN(ERRBUF OUT VARCHAR2, RETCODE OUT VARCHAR2,P_USER VARCHAR2)
IS
l_errbuf     varchar2(2000);
l_retcode    varchar2(500);

l_count      NUMBER:=0;
l_username   VARCHAR2(50);
l_str        VARCHAR2(100):=P_USER;
lvc_procedure     VARCHAR2(100):='MAIN';
l_sec             VARCHAR2(240);
BEGIN
l_sec:='Checking the P_user Parameter value';
 IF P_USER IS NOT NULL THEN
  fnd_file.put_line(fnd_file.log,'START PROFILE_SET_FOR_AHH_P_USER procedure: ' ||TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));
    fnd_file.put_line(fnd_file.log,'P_USER_NAME '||P_USER);
    l_sec:='P_user Parameter value is not null';
  SELECT REGEXP_COUNT(l_str, ',') REGEXP_COUNT
    INTO l_count
    FROM DUAL; 
	l_sec:='After count chekcing for no. of user entered in parmeter';
   
   FOR I IN 1..L_COUNT+1 LOOP
     SELECT REGEXP_SUBSTR (l_str, '[^,]+', 1, I)
       INTO l_username
       FROM DUAL;
   
       fnd_file.put_line(fnd_file.log,'P_USER_NAME '||l_username);
	   l_sec:='Before calling the PROFILE_SET_FOR_AHH_P_USER procedure ';
   
       PROFILE_SET_FOR_AHH_P_USER(l_errbuf,l_retcode,l_username);
     
   END LOOP;
 fnd_file.put_line(fnd_file.log,'END PROFILE_SET_FOR_AHH_P_USER procedure: ' ||TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));
 l_sec:=' After loop closed if p_user parameter is null';
ELSE
 l_sec:=' If p_user parameter is null';
 
fnd_file.put_line(fnd_file.log,'Start PROFILE_SET_NULL_FOR_AHH_USER procedure: ' ||TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));
fnd_file.put_line(fnd_file.log,'====================================================================');
PROFILE_SET_NULL_FOR_AHH_USER(l_errbuf,l_retcode); 
l_sec:=' After PROFILE_SET_NULL_FOR_AHH_USER called, in case p_user parameter is null';
fnd_file.put_line(fnd_file.log,'END PROFILE_SET_NULL_FOR_AHH_USER procedure: ' ||TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));
fnd_file.put_line(fnd_file.log,'====================================================================');

fnd_file.put_line(fnd_file.log,'Start PROFILE_SET_FOR_AHH_USER procedure: ' ||TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));
fnd_file.put_line(fnd_file.log,'====================================================================');
PROFILE_SET_FOR_AHH_USER(l_errbuf,l_retcode);
fnd_file.put_line(fnd_file.log,'END PROFILE_SET_FOR_AHH_USER procedure: ' ||TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));
fnd_file.put_line(fnd_file.log,'====================================================================');
l_sec:=' After PROFILE_SET_FOR_AHH_USER called, in case p_user parameter is null';
END IF;
l_sec:=' Before Exception calling in Main procedure';
EXCEPTION
WHEN OTHERS THEN
fnd_file.put_line(fnd_file.log,'Uexpected error in XXWC_AHHUSER_HIDE_COST_LLP_PKG.Main PKG '||SQLERRM);
xxcus_error_pkg.xxcus_error_main_api ( p_called_from => g_err_callfrom || '.' || lvc_procedure
                                      ,p_calling => l_sec
									  ,p_ora_error_msg => SQLERRM
									  ,p_error_desc => 'Error Occured in '|| g_err_callfrom || '.' || lvc_procedure
									  ,p_distribution_list => g_distro_list
									  ,p_module => g_module);
END;

-- ==============================================================================================================================
-- Procedure: PROFILE_SET_NULL_FOR_AHH_USER
-- Purpose: This procedure will pull all user accounts and set the XXWC_OM_VIEW_GROSS_MARGINS profile value at user level as null
-- ==============================================================================================================================
--  REVISIONS:
--   Ver        Date        Author               Description
--  ---------  ---------- ---------------  --------------------------------------------------------------------------------------
--  1.0        09/20/2018  Pattabhi Avula          TMS#20180924-00004 - Last price paid for North East AHH Branches
-- ============================================================================================================================== 
PROCEDURE PROFILE_SET_NULL_FOR_AHH_USER(ERRBUF OUT VARCHAR2, RETCODE OUT VARCHAR2)
 IS
   l_Result Boolean;
   l_user_id NUMBER; 
   le_exception EXCEPTION;
   l_err_msg VARCHAR2(2000);
   l_profile_value  varchar2(5);
   l_count NUMBER := 0;
   lvc_procedure     VARCHAR2(100):='PROFILE_SET_NULL_FOR_AHH_USER';
   l_sec             VARCHAR2(240);
   
   CURSOR cur_user
   IS
    SELECT FU.USER_ID,FU.USER_NAME
      FROM fnd_profile_options fpo,
           fnd_profile_option_values fpov,
           fnd_user fu       
     WHERE 1 = 1
       AND fpo.PROFILE_OPTION_ID = fpov.PROFILE_OPTION_ID
       AND fpo.PROFILE_OPTION_NAME='XXWC_OM_VIEW_GROSS_MARGINS'
       AND FPOV.LEVEL_ID=10004
       AND FPOV.LEVEL_VALUE = FU.USER_ID
       AND (FU.END_DATE IS NULL OR FU.END_DATE > SYSDATE)
       AND FPOV.PROFILE_OPTION_VALUE IS NOT NULL
       --AND FU.USER_NAME='RV003897' --'10010564' --'PA022863' --'10011189'
       AND NOT EXISTS (SELECT 1
                         FROM APPLSYS.wf_user_role_assignments ur, 
                              apps.WF_ROLES wr,
                              apps.fnd_lookup_values FLV                        
                        WHERE  1=1  
                         -- AND ROLE_ORIG_SYSTEM IN ('FND_RESP', 'UMX') 
                          --AND UPPER(wr.name) = UPPER(ur.role_name)
                          AND wr.name = ur.role_name
                          AND UR.USER_NAME= FU.USER_NAME                      
                          AND wr.status = 'ACTIVE'
						  AND UPPER(ur.role_name) = UPPER(flv.meaning)
                          AND (ur.user_end_date IS NULL OR ur.user_end_date > SYSDATE)
                          AND (ur.assigning_role_end_date IS NULL OR ur.assigning_role_end_date > SYSDATE)
						  AND (ur.end_date IS NULL OR ur.end_date > SYSDATE)
                          and flv.lookup_type = 'XXWC_VIEW_GROSS_MARGINS_ROLE'
                          AND FLV.ENABLED_FLAG = 'Y'
                          AND SYSDATE BETWEEN NVL(flv.START_DATE_ACTIVE,SYSDATE-1) AND NVL(flv.END_DATE_ACTIVE,SYSDATE+1));
BEGIN
l_sec:=' Starting of PROFILE_SET_NULL_FOR_AHH_USER Procedure';
    fnd_file.put_line(fnd_file.log,'Start PROCEDURE PROFILE_SET_NULL_FOR_AHH_USER: ' ||TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));
    FOR rec_user IN cur_user
    LOOP
         l_Result:= Fnd_profile.SAVE('XXWC_OM_VIEW_GROSS_MARGINS',NULL,'USER',rec_user.USER_ID);
		 fnd_file.put_line(fnd_file.log,'l_Result for User: '||rec_user.USER_NAME || ' : ' ||TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));
		 l_sec:=' Inside Loop after get the profile value';
          IF l_Result
          THEN
             l_count := l_count+1;              
             fnd_file.put_line(fnd_file.log,'XXWC_OM_VIEW_GROSS_MARGINS profile is set to null for user '||rec_user.USER_NAME);
			 l_sec:=' If profile returned successful value in PROFILE_SET_NULL_FOR_AHH_USER';
             COMMIT;
          ELSE
             l_err_msg := 'XXWC_OM_VIEW_GROSS_MARGINS Profile Not Updated for user '||rec_user.USER_NAME;
            -- RAISE le_exception; 
			l_sec:=' If profile failed in returning value in PROFILE_SET_NULL_FOR_AHH_USER';
          END IF;
    END LOOP;
    fnd_file.put_line(fnd_file.log,'End Script for role changed record count: ' ||l_count|| ' : '||TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));
	l_sec:='After loop in PROFILE_SET_NULL_FOR_AHH_USER';
EXCEPTION
   WHEN OTHERS THEN
      l_err_msg := SUBSTR(SQLERRM,1,2000);
      fnd_file.put_line(fnd_file.log,'Error: '||l_err_msg);
	  xxcus_error_pkg.xxcus_error_main_api ( p_called_from => g_err_callfrom || '.' || lvc_procedure
                                            ,p_calling => l_sec
									        ,p_ora_error_msg => SQLERRM
									        ,p_error_desc => 'Error Occured in '|| g_err_callfrom || '.' || lvc_procedure
									        ,p_distribution_list => g_distro_list
									        ,p_module => g_module);
      --ROLLBACK;
END;

-- ==============================================================================================================================
-- Procedure: PROFILE_SET_FOR_AHH_USER
-- Purpose: This procedure will pull all user role accounts and set the XXWC_OM_VIEW_GROSS_MARGINS profile value at user level as N
-- ==============================================================================================================================
--  REVISIONS:
--   Ver        Date        Author               Description
--  ---------  ---------- ---------------  --------------------------------------------------------------------------------------
--  1.0        09/20/2018  Pattabhi Avula          TMS#20180924-00004 - Last price paid for North East AHH Branches
-- ============================================================================================================================== 
-- Without parameter proc
PROCEDURE PROFILE_SET_FOR_AHH_USER(ERRBUF OUT VARCHAR2, RETCODE OUT VARCHAR2)
 IS
   l_Result Boolean;
   l_user_id NUMBER; 
   le_exception EXCEPTION;
   l_err_msg VARCHAR2(2000);
   l_profile_value  varchar2(5);
   l_count NUMBER := 0;
   l_llp_count NUMBER := 0;
   lvc_procedure      VARCHAR2(100):='PROFILE_SET_FOR_AHH_USER';
   l_sec              VARCHAR2(240);
   
   CURSOR cur_role
   IS
     SELECT UR.USER_NAME, flv.meaning
       FROM   APPLSYS.wf_user_role_assignments ur, 
              apps.WF_ROLES wr,
              apps.fnd_lookup_values FLV
       WHERE  1=1 
       AND UPPER(wr.name) = UPPER(ur.role_name)
      -- AND ur.user_name ='SL069571'
       AND wr.status = 'ACTIVE'
	   AND UPPER(ur.role_name) = UPPER(flv.meaning)
       AND (ur.user_end_date IS NULL OR ur.user_end_date > SYSDATE)
	   AND (ur.end_date IS NULL OR ur.end_date > SYSDATE)
       AND (ur.assigning_role_end_date IS NULL OR ur.assigning_role_end_date > SYSDATE)
       AND flv.lookup_type='XXWC_VIEW_GROSS_MARGINS_ROLE'
       AND flv.enabled_flag = 'Y'
       AND SYSDATE BETWEEN NVL(flv.START_DATE_ACTIVE,SYSDATE-1) AND NVL(flv.END_DATE_ACTIVE,SYSDATE+1);   
BEGIN
l_sec:='Start of PROFILE_SET_FOR_AHH_USER';
    fnd_file.put_line(fnd_file.log,'Start PROCEDURE ');
    FOR rec_user_role IN cur_role
    LOOP      
          fnd_file.put_line(fnd_file.log,'Inside loop rec_user_role '||rec_user_role.USER_NAME);
		  l_sec:='Inside the loop of PROFILE_SET_FOR_AHH_USER proc and before fetching user_id'; 
          l_user_id := null;
          l_err_msg := NULL;
           BEGIN
              SELECT USER_ID 
              INTO l_user_id
              FROM FND_USER
              WHERE USER_NAME = upper(rec_user_role.USER_NAME);
           EXCEPTION
              WHEN NO_DATA_FOUND THEN
                 l_err_msg := 'User '||rec_user_role.USER_NAME||' not found';
                 fnd_file.put_line(fnd_file.log,l_err_msg);
				 l_sec:='No data found exception Error while fetching the user_id'; 
            WHEN OTHERS THEN
                 l_err_msg := 'User '||rec_user_role.USER_NAME||' not found';
                 fnd_file.put_line(fnd_file.log,l_err_msg);
				 l_sec:='When others exception Error while fetching the user_id';
           END;
           -- checking the profile option value from user level
		   l_sec:='Before profile value fetching'; 
          BEGIN
           SELECT fpov.profile_option_value
             INTO l_profile_value
             FROM fnd_profile_options fpo,
                  fnd_profile_option_values fpov                    
            WHERE 1 = 1
              AND fpo.PROFILE_OPTION_ID = fpov.PROFILE_OPTION_ID
              AND fpo.PROFILE_OPTION_NAME='XXWC_OM_VIEW_GROSS_MARGINS'
              AND fpov.level_id=10004
              AND fpov.level_value = l_user_id
              AND FPOV.PROFILE_OPTION_VALUE IS NULL;
           EXCEPTION
            WHEN NO_DATA_FOUND THEN
              l_profile_value:=NULL;
			  l_sec:='No data found exception while fetching the profile value'; 
            WHEN OTHERS THEN
              l_profile_value:=NULL;
              fnd_file.put_line(fnd_file.log,'Error while fetching the profile value for user '||rec_user_role.USER_NAME);
			  l_sec:='when Others exception while fetching the profile value'; 
           END;
           l_sec:='Before checking the profile value'; 
		   
           fnd_file.put_line(fnd_file.log,'l_profile_value '||l_profile_value ||'for user_name: '||rec_user_role.USER_NAME);

       IF l_profile_value IS NULL THEN
       
         l_Result:= Fnd_profile.SAVE('XXWC_OM_VIEW_GROSS_MARGINS','N','USER',l_user_id);
		 l_sec:='After execute Fnd_profile.SAVE function'; 
          IF l_Result
          THEN
             COMMIT;
             l_count := l_count+1;
             fnd_file.put_line(fnd_file.log,'XXWC_OM_VIEW_GROSS_MARGINS Profile is set to N for user '||rec_user_role.USER_NAME);
			 fnd_file.put_line(fnd_file.log,'Processed the record for role '||rec_user_role.meaning);
			 l_sec:='Successfully set the profile value'; 
          ELSE
             l_err_msg := 'XXWC_OM_VIEW_GROSS_MARGINS Profile Not Updated for user '||rec_user_role.USER_NAME;
			 l_sec:='Failed execute Fnd_profile.SAVE function'; 
			 fnd_file.put_line(fnd_file.log,l_err_msg);
          END IF;
       END IF;--l_profile_value
       
       --Assing value to l_profile_value XXWC_LAST_PRICE_PAID_AIS_VISIBILITY at user level.
       l_Result:= Fnd_profile.SAVE('XXWC_LAST_PRICE_PAID_AIS_VISIBILITY','AHH','USER',l_user_id);
       l_sec:='After execute Fnd_profile.SAVE function'; 
       
       IF l_Result THEN
          COMMIT;
          l_llp_count := l_llp_count+1;
          fnd_file.put_line(fnd_file.log,'XXWC_LAST_PRICE_PAID_AIS_VISIBILITY Profile is set to AHH for user '||rec_user_role.USER_NAME);
          fnd_file.put_line(fnd_file.log,'Processed the record for role '||rec_user_role.meaning);
          l_sec:='Successfully set the profile value'; 
       ELSE
           l_err_msg := 'XXWC_OM_VIEW_GROSS_MARGINS Profile Not Updated for user '||rec_user_role.USER_NAME;
		   l_sec:='Failed execute Fnd_profile.SAVE function'; 
		   fnd_file.put_line(fnd_file.log,l_err_msg);
       END IF;

    END LOOP;
    fnd_file.put_line(fnd_file.log,'XXWC_LAST_PRICE_PAID_AIS_VISIBILITY update record count: '||l_llp_count);
    fnd_file.put_line(fnd_file.log,'PROFILE_SET_FOR_AHH_USER update record count: '||l_count);
	l_sec:='Before Exception in PROFILE_SET_FOR_AHH_USER proc '; 
EXCEPTION
   WHEN OTHERS THEN
      l_err_msg := SUBSTR(SQLERRM,1,2000);
      fnd_file.put_line(fnd_file.log,'Error: '||l_err_msg);
	  xxcus_error_pkg.xxcus_error_main_api ( p_called_from => g_err_callfrom || '.' || lvc_procedure
                                      ,p_calling => l_sec
									  ,p_ora_error_msg => SQLERRM
									  ,p_error_desc => 'Error Occured in '|| g_err_callfrom || '.' || lvc_procedure
									  ,p_distribution_list => g_distro_list
									  ,p_module => g_module);
      --ROLLBACK;
END;

-- ==============================================================================================================================
-- Procedure: PROFILE_SET_FOR_AHH_P_USER
-- Purpose: This procedure will pull all user role accounts and set the XXWC_OM_VIEW_GROSS_MARGINS profile value at user level as N
--          only for passed parameter values
-- ==============================================================================================================================
--  REVISIONS:
--   Ver        Date        Author               Description
--  ---------  ---------- ---------------  --------------------------------------------------------------------------------------
--  1.0        09/20/2018  Pattabhi Avula          TMS#20180924-00004 - Last price paid for North East AHH Branches
-- ============================================================================================================================== 
-- with Parameter logic
PROCEDURE PROFILE_SET_FOR_AHH_P_USER(ERRBUF OUT VARCHAR2, RETCODE OUT VARCHAR2,P_USER IN VARCHAR2)
 IS
   l_Result Boolean;
   l_user_id NUMBER; 
   le_exception EXCEPTION;
   l_err_msg VARCHAR2(2000);
   l_profile_value  varchar2(5);
   lvc_procedure      VARCHAR2(100):='PROFILE_SET_FOR_AHH_P_USER';
   l_sec              VARCHAR2(240);
   
   CURSOR cur_role
   IS
     SELECT UR.USER_NAME,flv.meaning
       FROM   APPLSYS.wf_user_role_assignments ur, 
              apps.WF_ROLES wr,
              apps.fnd_lookup_values FLV
       WHERE  1=1 
       AND UPPER(wr.name) = UPPER(ur.role_name)
      AND ur.user_name = P_USER  --'SL069571'
       AND wr.status = 'ACTIVE'
	   AND UPPER(ur.role_name) = UPPER(flv.meaning)
       AND (ur.user_end_date IS NULL OR ur.user_end_date > SYSDATE) -- user role end date
	   AND (ur.end_date IS NULL OR ur.end_date > SYSDATE) -- user end date 
       AND (ur.assigning_role_end_date IS NULL OR ur.assigning_role_end_date > SYSDATE)
       AND flv.lookup_type='XXWC_VIEW_GROSS_MARGINS_ROLE'
       AND flv.enabled_flag = 'Y'
       AND SYSDATE BETWEEN NVL(flv.START_DATE_ACTIVE,SYSDATE-1) AND NVL(flv.END_DATE_ACTIVE,SYSDATE+1);   
BEGIN
l_sec:='Start of PROFILE_SET_FOR_AHH_P_USER proc';
    fnd_file.put_line(fnd_file.log,'Start PROCEDURE ');
    FOR rec_user_role IN cur_role
    LOOP 
    l_sec:='Before fetching user_id in PROFILE_SET_FOR_AHH_P_USER proc';	
          l_user_id := null;
          l_err_msg := NULL;
           BEGIN
              SELECT USER_ID 
              INTO l_user_id
              FROM FND_USER
              WHERE USER_NAME = upper(rec_user_role.USER_NAME);
           EXCEPTION
              WHEN NO_DATA_FOUND THEN
                 l_err_msg := 'User '||rec_user_role.USER_NAME||' not found'; 
            l_sec:='Exception user_id fetching in PROFILE_SET_FOR_AHH_P_USER proc';                 				 
           END;
		   l_sec:='checking the profile option value from user level in PROFILE_SET_FOR_AHH_P_USER proc'; 
           -- checking the profile option value from user level
          BEGIN
           SELECT fpov.profile_option_value
             INTO l_profile_value
             FROM fnd_profile_options fpo,
                  fnd_profile_option_values fpov                    
            WHERE 1 = 1
              AND fpo.PROFILE_OPTION_ID = fpov.PROFILE_OPTION_ID
              AND fpo.PROFILE_OPTION_NAME='XXWC_OM_VIEW_GROSS_MARGINS'
              AND fpov.level_id=10004
              AND fpov.level_value = l_user_id;
           EXCEPTION
            WHEN NO_DATA_FOUND THEN
              l_profile_value:=NULL;
			  l_sec:='No data found exception for profile value check in PROFILE_SET_FOR_AHH_P_USER proc';
            WHEN OTHERS THEN
            l_profile_value:=NULL;
            fnd_file.put_line(fnd_file.log,'Error while fetching the profile value for user '||rec_user_role.USER_NAME);
			l_sec:='When others exception for profile value check in PROFILE_SET_FOR_AHH_P_USER proc';
           END;
       
       IF l_profile_value IS NULL THEN
	   l_sec:='Profile value null case in PROFILE_SET_FOR_AHH_P_USER proc';
       
         l_Result:= Fnd_profile.SAVE('XXWC_OM_VIEW_GROSS_MARGINS','N','USER',l_user_id);
          IF l_Result
          THEN
             COMMIT;
             fnd_file.put_line(fnd_file.log,'XXWC_OM_VIEW_GROSS_MARGINS Profile is set to N for user '||rec_user_role.USER_NAME);
			 fnd_file.put_line(fnd_file.log,'Processed the record for role '||rec_user_role.meaning);
			 l_sec:='Profile value returned successfully in PROFILE_SET_FOR_AHH_P_USER proc';
          ELSE
             l_err_msg := 'XXWC_OM_VIEW_GROSS_MARGINS Profile Not Updated for user '||rec_user_role.USER_NAME;
			 l_sec:='Profile value returned failed case in PROFILE_SET_FOR_AHH_P_USER proc';
          END IF;
         END IF;
    END LOOP;
    fnd_file.put_line(fnd_file.log,'End Script');
	l_sec:='End of PROFILE_SET_FOR_AHH_P_USER proc';
EXCEPTION
   WHEN OTHERS THEN
      l_err_msg := SUBSTR(SQLERRM,1,2000);
      fnd_file.put_line(fnd_file.log,'Error: '||l_err_msg);
	  xxcus_error_pkg.xxcus_error_main_api ( p_called_from => g_err_callfrom || '.' || lvc_procedure
                                      ,p_calling => l_sec
									  ,p_ora_error_msg => SQLERRM
									  ,p_error_desc => 'Error Occured in '|| g_err_callfrom || '.' || lvc_procedure
									  ,p_distribution_list => g_distro_list
									  ,p_module => g_module);
      --ROLLBACK;
END;  -- parameter logic

END XXWC_AHHUSER_HIDE_COST_LLP_PKG;
/