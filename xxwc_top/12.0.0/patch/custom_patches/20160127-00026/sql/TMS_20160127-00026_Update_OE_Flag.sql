/*
 TMS: 20160127-00026      
 Date: 01/27/2016
 Notes: @SF data fix script for I647340
*/

SET SERVEROUTPUT ON SIZE 100000;
SET VERIFY OFF;


update apps.wsh_delivery_Details
set OE_INTERFACED_FLAG='Y'
where DELIVERY_DETAIL_ID=14058550
and source_header_id=38460601
and source_line_id=63033712;
		  
--1 row expected to be updated

commit
/
