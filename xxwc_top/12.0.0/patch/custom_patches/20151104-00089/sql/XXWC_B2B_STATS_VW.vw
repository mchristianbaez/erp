/********************************************************************************
   $Header XXWC_B2B_STATS_VW.VW $
   Module Name: XXWC_B2B_STATS_VW

   PURPOSE:   View to derive B2B Statistics Information.

   REVISIONS:
   Ver        Date        Author                     Description
   ---------  ----------  ---------------         -------------------------
   1.0        08/09/2015  Gopi Damuluri           TMS# 20150615-00088
                                                  Initial Version
   1.1        11/14/2015  Gopi Damuluri           TMS# 20151104-00089 
                                                  Modify B2B Metrics to provide XML file counts and amounts
********************************************************************************/

-- Version# 1.1 > Start
CREATE OR REPLACE FORCE VIEW APPS.XXWC_B2B_STATS_VW
(
   PARTY_NAME,
   PARTY_NUMBER,
   ACCOUNT_NAME,
   ACCOUNT_NUMBER,
   TP_NAME,
   ORDER_NUMBER,
   ORDER_TYPE,
   ORDER_SOURCE,
   SOA_DELIVERED,
   ASN_DELIVERED,
   ASN_DELIVERED_AMT,
   INVOICE_COUNT,
   INVOICE_AMOUNT,
   CREATION_DATE,
   SALE_AMOUNT
)
AS
   SELECT hp.party_name,
          hp.party_number,
          hca.account_name,
          hca.account_number,
          b2bc.tp_name,
          ooh.order_number,
          'STANDARD ORDER' Order_Type,
          -- TO_CHAR (ooh.creation_date, 'MM-MONTH') PERIOD,
          ooh.order_Source_id ORDER_SOURCE,
          (SELECT SUM (DECODE (b2bd_soa.soa_delivered, '1', 1, 0))
             FROM xxwc.xxwc_b2b_so_delivery_info_tbl b2bd_soa
            WHERE     1 = 1
                  AND b2bd_soa.header_id = ooh.header_id
                  AND b2bd_soa.soa_delivered = '1')
             SOA_DELIVERED,
          (SELECT COUNT (1)
             FROM xxwc.xxwc_b2b_so_delivery_info_tbl b2bd_asn
            WHERE     1 = 1
                  AND b2bd_asn.header_id = ooh.header_id
                  AND b2bd_asn.asn_delivered IN ('1', '3'))
             ASN_DELIVERED,
          NVL (
             (SELECT SUM (
                        ROUND (
                             (ool.ORDERED_QUANTITY * UNIT_SELLING_PRICE)
                           + TAX_VALUE,
                           2))
                FROM oe_order_lines_all ool
               WHERE     ool.header_id = ooh.header_id
                     AND EXISTS
                            (SELECT '1'
                               FROM xxwc.xxwc_wsh_shipping_stg stg,
                                    xxwc.xxwc_b2b_so_delivery_info_tbl b2bd_asn
                              WHERE     stg.line_id = ool.line_id
                                    AND b2bd_asn.header_id = ooh.header_id
                                    AND b2bd_asn.delivery_id =
                                           stg.delivery_id
                                    AND b2bd_asn.asn_delivered IN ('1', '3'))),
             0)
             ASN_DELIVERED_AMT,
          --DECODE(ooh.order_Source_id,1021,1,0) XML_PO_CNT,
          (SELECT COUNT (1)
             FROM ra_customer_trx_all rcta
            WHERE     rcta.interface_header_attribute1 =
                         TO_CHAR (ooh.order_number)
                  AND NVL (b2bc.deliver_invoice, 'N') = 'Y'
                  AND rcta.attribute10 IS NOT NULL)
             INVOICE_COUNT,
          NVL (
             (SELECT SUM (apsa.amount_due_original)
                FROM ra_customer_trx_all rcta, ar_payment_schedules_all apsa
               WHERE     rcta.interface_header_attribute1 =
                            TO_CHAR (ooh.order_number)
                     AND apsa.customer_trx_id = rcta.customer_trx_id
                     AND apsa.org_id = rcta.org_id
                     AND NVL (b2bc.deliver_invoice, 'N') = 'Y'
                     AND rcta.attribute10 IS NOT NULL),
             0)
             INVOICE_AMOUNT,
          ooh.creation_date,
          NVL (
             (SELECT SUM (
                        ROUND (
                             (ORDERED_QUANTITY * UNIT_SELLING_PRICE)
                           + TAX_VALUE,
                           2))
                FROM oe_order_lines_all ool
               WHERE ool.header_id = ooh.header_id),
             0)
             SALE_AMOUNT
     FROM oe_order_headers_all ooh,
          xxwc.xxwc_b2b_cust_info_tbl b2bc,
          hz_cust_accounts_all hca,
          hz_parties hp
    WHERE     1 = 1
          AND EXISTS
                 (SELECT '1'
                    FROM xxwc.xxwc_b2b_so_delivery_info_tbl b2bd
                   WHERE     ooh.header_id = b2bd.header_id
                         AND (   soa_delivered = '1'
                              OR asn_delivered IN ('1', '3')))
          AND ooh.sold_to_org_id = hca.cust_account_id
          AND hca.party_id = b2bc.party_id
          AND hca.party_id = hp.party_id
          AND ooh.order_type_id IN (1001, 1004)
          AND ooh.flow_status_code != 'CANCELLED'
          AND SYSDATE BETWEEN NVL (b2bc.start_date_active, SYSDATE - 10)
                          AND NVL (b2bc.end_date_active, SYSDATE + 10);

-- Version# 1.1 < End

/* -- Version# 1.1 > Commented Start
CREATE OR REPLACE FORCE VIEW APPS.XXWC_B2B_STATS_VW
(
   PARTY_NUMBER,
   ACCOUNT_NUMBER,
   PARTY_NAME,
   ACCOUNT_NAME,
   TP_NAME,
   ORDER_NUMBER,
   ORDER_TYPE,
   PERIOD,
   ORDER_SOURCE,
   SOA_DELIVERED,
   ASN_DELIVERED,
   CREATION_DATE,
   SALE_AMOUNT
)
AS
   SELECT hp.party_number,
          hca.account_number,
          hp.party_name,
          hca.account_name,
          b2bc.tp_name,
          ooh.order_number,
          'STANDARD ORDER' Order_Type,
          TO_CHAR (ooh.creation_date, 'MM-MONTH') PERIOD,
          'B2B LIASON'              ORDER_SOURCE,
          DECODE(SOA_DELIVERED, '1',1,0) SOA_DELIVERED,
          DECODE(ASN_DELIVERED, '1',1,'3',1,0) ASN_DELIVERED,
          ooh.creation_date,
          (SELECT SUM (
                     ROUND (
                        (ORDERED_QUANTITY * UNIT_SELLING_PRICE) + TAX_VALUE,
                        2))
             FROM oe_order_lines_all ool
            WHERE ool.header_id = ooh.header_id)
             SALE_AMOUNT
     FROM oe_order_headers_all ooh,
          xxwc.xxwc_b2b_so_delivery_info_tbl b2bd,
          xxwc.xxwc_b2b_cust_info_tbl b2bc,
          hz_cust_accounts_all hca,
          hz_parties hp
    WHERE     1 = 1
          AND ooh.header_id = b2bd.header_id
          AND ooh.sold_to_org_id = hca.cust_account_id
          AND hca.party_id = b2bc.party_id
          AND hca.party_id = hp.party_id
          AND ooh.order_type_id IN (1001, 1004)
          AND ooh.flow_status_code != 'CANCELLED'
   UNION
   SELECT hp.party_number,
          hca.account_number,
          hp.party_name,
          hca.account_name,
          b2bc.tp_name,
          ooh.order_number,
          'COUNTER ORDER' Order_Type,
          TO_CHAR (ooh.creation_date, 'MM-MONTH') PERIOD,
          'NON-B2B' ORDER_SOURCE,
          0 SOA_DELIVERED,
          0 ASN_DELIVERED,
          ooh.creation_date,
          (SELECT SUM (
                     ROUND (
                        (ORDERED_QUANTITY * UNIT_SELLING_PRICE) + TAX_VALUE,
                        2))
             FROM oe_order_lines_all ool
            WHERE ool.header_id = ooh.header_id)
             SALE_AMOUNT
     FROM oe_order_headers_all ooh,
          xxwc.xxwc_b2b_cust_info_tbl b2bc,
          hz_cust_accounts_all hca,
          hz_parties hp
    WHERE     1 = 1
          AND ooh.sold_to_org_id = hca.cust_account_id
          AND hca.party_id = b2bc.party_id
          AND hca.party_id = hp.party_id
          AND ooh.order_type_id IN (1001, 1004)
          AND NOT EXISTS (SELECT '1' FROM xxwc.xxwc_b2b_so_delivery_info_tbl b2bd WHERE b2bd.header_id = ooh.header_id)
          AND ooh.flow_status_code != 'CANCELLED';
*/ -- Version# 1.1 < Commented End
/
