CREATE OR REPLACE FORCE VIEW APPS.XXWC_MD_SEARCH_PRODUCTS_VW
/*************************************************************************
 Copyright (c) 2012 HD Supply
 All rights reserved.
**************************************************************************
   $Header APPS.XXWC_MD_SEARCH_PRODUCTS_VW $
  Module Name: APPS.XXWC_MD_SEARCH_PRODUCTS_VW

  PURPOSE:To pull in the data from MTL_SYSTEM_ITEMS_B and create a view for item search

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     1-SEP-2015   Pahwa, Nancy                Initially Created 
TMS# 20150825-00012
  -- 1.1     01-Mar-2016  Pahwa Nancy 20160301-00306 AIS Performance Tuning
  -- 1.2     09-Mar-2016  Pahwa Nancy 20160310-00219 Removed 180 days condition from specials
  -- 1.3     11-Mar-2016  Pahwa Nancy 20160314-00108 to fix UPC's removes , from listagg function
  -- 1.4     23-Jun-2016  Pahwa Nancy 20160623-00057 to allow discontinued items
**************************************************************************/
(
   DUMMY,
   INVENTORY_ITEM_ID,
   ORGANIZATION_ID,
   PARTNUMBER,
   TYPE,
   MANUFACTURERPARTNUMBER,
   MANUFACTURER,
   SEQUENCE,
   CURRENCYCODE,
   NAME,
   SHORTDESCRIPTION,
   LONGDESCRIPTION,
   THUMBNAIL,
   FULLIMAGE,
   QUANTITYMEASURE,
   WEIGHTMEASURE,
   WEIGHT,
   BUYABLE,
   KEYWORD,
   CREATION_DATE,
   ITEM_TYPE,
   CROSS_REFERENCE,
   PRIMARY_UOM_CODE
)
AS
     SELECT ' ' dummy,
            msib.inventory_item_id,
            msib.organization_id,
            msib.segment1 partNumber,
            'Product' TYPE,
            (SELECT XXWC_M_P_N_ATTR
               FROM XXWC_WHITE_C_M_AGV
              WHERE     ROWNUM = 1
                    AND organization_id = 222
                    AND inventory_item_id = msib.inventory_item_id)
               ManufacturerPartNumber,
            (SELECT XXWC_BRAND_ATTR
               FROM XXWC_WHITE_C_M_AGV
              WHERE     ROWNUM = 1
                    AND organization_id = 222
                    AND inventory_item_id = msib.inventory_item_id)
               Manufacturer,
            -- NULL ParentPartNumber,
            (SELECT XXWC_WEB_SEQUENCE_ATTR
               FROM XXWC_WHITE_C_M_AGV
              WHERE     inventory_item_id = msib.inventory_item_id
                    AND organization_id = 222
                    AND ROWNUM = 1)
               SEQUENCE,
            -- NULL ParentStoreIdentifier,
            'USD' CurrencyCode,
            (SELECT XXWC_W_S_D_ATTR
               FROM XXWC_WHITE_C_M_AGV
              WHERE     ROWNUM = 1
                    AND organization_id = 222
                    AND inventory_item_id = msib.INVENTORY_ITEM_ID)
               Name,
            msib.description ShortDescription,
            (SELECT XXWC_WEB_LONG_DESCRIPTION_ATTR
               FROM XXWC_WHITE_C_M_AGV
              WHERE     ROWNUM = 1
                    AND organization_id = 222
                    AND inventory_item_id = msib.INVENTORY_ITEM_ID)
               LongDescription,
               fnd_profile.VALUE ('XXWC_THUMBNAIL_PROD')
            || (SELECT XXWC_A_T_1_ATTR
                  FROM XXWC_WHITE_C_M_AGV
                 WHERE     ROWNUM = 1
                       AND organization_id = 222
                       AND inventory_item_id = msib.INVENTORY_ITEM_ID)
               Thumbnail,
               fnd_profile.VALUE ('XXWC_FULL_IMAGE_PROD')
            || (SELECT XXWC_A_F_1_ATTR
                  FROM XXWC_WHITE_C_M_AGV
                 WHERE     ROWNUM = 1
                       AND organization_id = 222
                       AND inventory_item_id = msib.INVENTORY_ITEM_ID)
               FullImage,
            'C62' QuantityMeasure,
            'LBS' WeightMeasure,
            unit_weight Weight,
            '1' Buyable,
            (SELECT XXWC_WEB_KEYWORDS_ATTR
               FROM XXWC_WHITE_C_M_AGV
              WHERE     inventory_item_id = msib.inventory_item_id
                    AND organization_id = 222
                    AND ROWNUM = 1)
               Keyword,
            msib.creation_date,
            msib.item_type,
--1.3 start
          /*LISTAGG (mcr.cross_reference, ',')
               WITHIN GROUP (ORDER BY mcr.inventory_item_id)
               cross_reference,*/
            LISTAGG (mcr.cross_reference, ' ')
               WITHIN GROUP (ORDER BY mcr.inventory_item_id)
               cross_reference,
--1.3 end
            msib.primary_uom_code
       FROM MTL_SYSTEM_ITEMS_B MSIB
            LEFT OUTER JOIN mtl_cross_references MCR
               ON     MSIB.Inventory_Item_Id = MCR.inventory_item_id
                  AND mcr.cross_reference_type IN ('VENDOR',
                                                   'MANUFACTURER',
                                                   'WC_XREF',
                                                   'UPC')
      WHERE     (  /*  TRUNC (msib.creation_date) > SYSDATE - 180
                 AND*/ msib.inventory_item_status_code = 'Active' and  msib.item_type = 'SPECIAL'
            AND MSIB.organization_id = 222)
   /* where msib.inventory_item_id = 2929068*/
   GROUP BY msib.inventory_item_id,
            msib.organization_id,
            msib.segment1,
            'Product',
            'USD',
            msib.description,
            'C62',
            'LBS',
            unit_weight,
            '1',
            msib.creation_date,
            msib.item_type,
            msib.primary_uom_code
   UNION
     SELECT NULL dummy,
            msib.inventory_item_id,
            msib.organization_id,
            msib.segment1 partNumber,
            'Product' TYPE,
            (SELECT XXWC_M_P_N_ATTR
               FROM XXWC_WHITE_C_M_AGV
              WHERE     ROWNUM = 1
                    AND organization_id = 222
                    AND inventory_item_id = msib.inventory_item_id)
               ManufacturerPartNumber,
            (SELECT XXWC_BRAND_ATTR
               FROM XXWC_WHITE_C_M_AGV
              WHERE     ROWNUM = 1
                    AND organization_id = 222
                    AND inventory_item_id = msib.inventory_item_id)
               Manufacturer,
            -- NULL ParentPartNumber,
            (SELECT XXWC_WEB_SEQUENCE_ATTR
               FROM XXWC_WHITE_C_M_AGV
              WHERE     inventory_item_id = msib.inventory_item_id
                    AND organization_id = 222
                    AND ROWNUM = 1)
               SEQUENCE,
            -- NULL ParentStoreIdentifier,
            'USD' CurrencyCode,
            (SELECT XXWC_W_S_D_ATTR
               FROM XXWC_WHITE_C_M_AGV
              WHERE     ROWNUM = 1
                    AND organization_id = 222
                    AND inventory_item_id = msib.INVENTORY_ITEM_ID)
               Name,
            msib.description ShortDescription,
            (SELECT XXWC_WEB_LONG_DESCRIPTION_ATTR
               FROM XXWC_WHITE_C_M_AGV
              WHERE     ROWNUM = 1
                    AND organization_id = 222
                    AND inventory_item_id = msib.INVENTORY_ITEM_ID)
               LongDescription,
               fnd_profile.VALUE ('XXWC_THUMBNAIL_PROD')
            || (SELECT XXWC_A_T_1_ATTR
                  FROM XXWC_WHITE_C_M_AGV
                 WHERE     ROWNUM = 1
                       AND organization_id = 222
                       AND inventory_item_id = msib.INVENTORY_ITEM_ID)
               Thumbnail,
               fnd_profile.VALUE ('XXWC_FULL_IMAGE_PROD')
            || (SELECT XXWC_A_F_1_ATTR
                  FROM XXWC_WHITE_C_M_AGV
                 WHERE     ROWNUM = 1
                       AND organization_id = 222
                       AND inventory_item_id = msib.INVENTORY_ITEM_ID)
               FullImage,
            'C62' QuantityMeasure,
            'LBS' WeightMeasure,
            unit_weight Weight,
            '1' Buyable,
            (SELECT XXWC_WEB_KEYWORDS_ATTR
               FROM XXWC_WHITE_C_M_AGV
              WHERE     inventory_item_id = msib.inventory_item_id
                    AND organization_id = 222
                    AND ROWNUM = 1)
               Keyword,
            msib.creation_date,
            msib.item_type,
--1.3 start
            /*LISTAGG (mcr.cross_reference, ',')
               WITHIN GROUP (ORDER BY mcr.inventory_item_id)
               cross_reference,*/
            LISTAGG (mcr.cross_reference, ' ')
               WITHIN GROUP (ORDER BY mcr.inventory_item_id)
               cross_reference,
--1.3 end
            msib.primary_uom_code
       FROM MTL_SYSTEM_ITEMS_B MSIB
            LEFT OUTER JOIN mtl_cross_references MCR
               ON     MSIB.Inventory_Item_Id = MCR.inventory_item_id
                  AND mcr.cross_reference_type IN ('VENDOR',
                                                   'MANUFACTURER',
                                                   'WC_XREF',
                                                   'UPC')
      WHERE msib.item_type NOT IN ('SPECIAL') AND MSIB.organization_id = 222 
       and msib.inventory_item_status_code in ('Active','Intangible','New Item','Repair','Discontinu') --1.4
   /* where msib.inventory_item_id = 2929068*/
   GROUP BY msib.inventory_item_id,
            msib.organization_id,
            msib.segment1,
            'Product',
            'USD',
            msib.description,
            'C62',
            'LBS',
            unit_weight,
            '1',
            msib.creation_date,
            msib.item_type,
            msib.primary_uom_code;
/