  /********************************************************************************
  FILE NAME: XXWC_PO_ORG_ATTR_VW_FUNC_OBJ.sql  
  PROGRAM TYPE: Table Type  
  PURPOSE: Table type   
  HISTORY
  ========================================================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- ------------------------------------------------------------------
  1.0     16-Mar-17     P.Vamshidhar    TMS#20170314-00295 - Org Attributes form slowness- Phase2 fix 
  ********************************************************************************************************/
CREATE OR REPLACE
TYPE XXWC.XXWC_PO_ORG_ATTR_VW_FUNC_OBJ AS OBJECT
(
  INVENTORY_ITEM_ID             NUMBER, 
  SEGMENT1                      VARCHAR2(40 BYTE),
  ORGANIZATION_ID               NUMBER,
  DESCRIPTION                   VARCHAR2(240 BYTE),
  WEIGHT_UOM_CODE               VARCHAR2(3 BYTE),
  SOURCE_TYPE                   NUMBER,
  ORG_NAME                      VARCHAR2(240 BYTE),
  SOURCE_ORGANIZATION_ID        NUMBER,
  SOURCE_ORG_NAME               VARCHAR2(240 BYTE),
  SOURCING_RULE_NAME            VARCHAR2(50 BYTE),
  VENDOR_NAME                   VARCHAR2(240 BYTE),
  VENDOR_SITE                   VARCHAR2(15 BYTE),
  SOURCE_ORG_RULE_NAME          VARCHAR2(50 BYTE),
  SOURCE_ORG_VENDOR_NAME        VARCHAR2(240 BYTE),
  SOURCE_ORG_SITE_NAME          VARCHAR2(15 BYTE),
  PREPROCESSING_LEAD_TIME       NUMBER,
  POSTPROCESSING_LEAD_TIME      NUMBER,
  FIXED_LOT_MULTIPLIER          NUMBER,
  SALES_VELOCITY                VARCHAR2(240 BYTE),
  INVENTORY_PLANNING_CODE       NUMBER,
  MIN_MINMAX_QUANTITY           NUMBER,
  MAX_MINMAX_QUANTITY           NUMBER,
  PURCHASING_ENABLED_FLAG       VARCHAR2(1 BYTE),
  PURCHASING_ITEM_FLAG          VARCHAR2(1 BYTE),
  RESERVE_STOCK                 VARCHAR2(240 BYTE),
  BUYER_ID                      NUMBER(9),
  FULL_NAME                     VARCHAR2(240 BYTE),
  PREV_PREPLT                   NUMBER,
  PREV_POSTPLT                  NUMBER,
  PREV_FLM                      NUMBER,
  PREV_SALES_VELOCITY           VARCHAR2(240 BYTE),
  PREV_INVENTORY_PLANNING_CODE  NUMBER,
  PREV_MIN                      NUMBER,
  PREV_MAX                      NUMBER,
  PREV_PURCHASE_ENABLED_FLAG    VARCHAR2(1 BYTE),
  PREV_PURCHASE_ITEM_FLAG       VARCHAR2(1 BYTE),
  PREV_RESERVE_STOCK            VARCHAR2(240 BYTE),
  PREV_BUYER                    VARCHAR2(240 BYTE),
  SOURCE_ORG_CLASSIFICATION     VARCHAR2(40 BYTE),
  SOURCING_RULE_ID              NUMBER,
  PURCHASE_FLAG                 VARCHAR2(40 BYTE),
  PREV_PURCHASE_FLAG            VARCHAR2(40 BYTE),
  PREV_SOURCE_TYPE              VARCHAR2(80 BYTE),
  CLASSIFICATION                VARCHAR2(40 BYTE),
  PREV_CLASSIFICATION           VARCHAR2(40 BYTE),
  PREV_PLANNING_CODE            VARCHAR2(80 BYTE),
  PREV_SOURCING_RULE_ID         NUMBER,
  PREV_SOURCING_RULE_NAME       VARCHAR2(50 BYTE),
  ORG_CODE                      VARCHAR2(3 BYTE),
  SOURCE_ORG_CODE               VARCHAR2(3 BYTE),
  PREVIOUS_MIN                  NUMBER,
  PREVIOUS_MAX                  NUMBER,
  PREV_SOURCE_ORG_CODE          VARCHAR2(3 BYTE),
  FULL_LEAD_TIME                NUMBER,
  PREV_FLT                      NUMBER
);
/
CREATE OR REPLACE TYPE XXWC.XXWC_PO_ORG_ATTR_VW_FUNC_T AS TABLE OF XXWC.XXWC_PO_ORG_ATTR_VW_FUNC_OBJ;
/