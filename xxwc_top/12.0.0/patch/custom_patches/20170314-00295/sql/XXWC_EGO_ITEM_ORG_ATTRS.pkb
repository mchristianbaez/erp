CREATE OR REPLACE PACKAGE BODY APPS.XXWC_EGO_ITEM_ORG_ATTRS
AS
   /**************************************************************************
    File Name:XXWC_EGO_ITEM_ORG_ATTRS
    PROGRAM TYPE: PL/SQL Package spec and body
    PURPOSE: Updates Item org attributes
    HISTORY
    -- Description   : Called from the trigger XXWC Org Attributes page
    -- Dependencies Tables        : custom table XXWC_ORG_ATTRS_DBG
    -- Dependencies Views         : None
    -- Dependencies Sequences     : XXWC_ORG_ATTRS_DBG_S
    -- Dependencies Procedures    : XXWC_ORG_ATTRS_DBG_PKG
    ================================================================
           Last Update Date : 12/12/12
    ================================================================
    ================================================================
    VERSION DATE          AUTHOR(S)       DESCRIPTION
    ------- -----------   --------------- ---------------------------------
    1.0     23-Nov-2012   Santhosh Louis   Initial creation of the package
    1.1     16-Mar-2017   P.Vamshidhar     TMS#20170314-00295 - Org Attributes form slowness- Phase2 fix     

   **************************************************************************/

   PROCEDURE SYNC_ORG_ATTRIBUTES (
      P_INVENTORY_ITEM_ID                       NUMBER,
      P_ORGANIZATION_ID                         NUMBER,
      P_USER_ID                                 NUMBER,
      P_SEGMENT1                                VARCHAR2,
      P_SOURCE_TYPE                             VARCHAR2,
      P_SOURCE_ORG                              VARCHAR2,
      P_SRC_RULE_ID                             NUMBER,
      P_PREPROCESSING_LEAD_TIME                 NUMBER,
      P_PRCESSING_LEAD_TIME                     NUMBER,
      P_FIXED_LOT_MULTIPLIER                    NUMBER,
      P_SALES_VELOCITY_CLASS                    VARCHAR2,
      P_PLANNING_METHOD                         VARCHAR2,
      P_MINMAX_MINIMUM                          NUMBER,
      P_MINMAX_MAXIMUM                          NUMBER,
      P_PURCHASE_FLAG                           VARCHAR2,
      P_RESERVE_STOCK                           VARCHAR2,
      P_DEFAULT_BUYER                           NUMBER,
      P_PREV_PURCHASE_FLAG                      VARCHAR2,
      P_PREV_SV_CLASS                           VARCHAR2,
      P_PREV_SRC_RULE_ID                        NUMBER,
      l_WARNINGS                     OUT NOCOPY VARCHAR2,
      L_RETURN_STATUS                OUT NOCOPY VARCHAR2,
      l_ERROR_MSG                    OUT NOCOPY VARCHAR2)
   AS
      /*--------------
      Declarations
      ----------------*/
      l_user_id                  NUMBER;
      L_RESP_ID                  NUMBER;
      L_APPL_ID                  NUMBER;
      l_sec_grp_id               NUMBER := 0;



      l_item_table               EGO_Item_PUB.Item_Tbl_Type;
      x_item_table               EGO_Item_PUB.Item_Tbl_Type;
      x_Inventory_Item_Id        mtl_system_items_b.inventory_item_id%TYPE;
      x_Organization_Id          mtl_system_items_b.organization_id%TYPE;
      X_RETURN_STATUS            VARCHAR2 (1);
      x_msg_count                NUMBER (10);
      X_MSG_DATA                 VARCHAR2 (1000);
      X_MESSAGE_LIST             ERROR_HANDLER.ERROR_TBL_TYPE;
      X_ERRORCODE                NUMBER;

      l_sv_Cat_Set_id            NUMBER;
      l_sv_category_id           NUMBER;
      l_sv_prev_cat_id           NUMBER;

      l_pf_Cat_Set_id            NUMBER;
      l_pf_category_id           NUMBER;
      L_PF_PREV_CAT_ID           NUMBER;

      L_SOURCE_ORG_CLASS         VARCHAR2 (100) := NULL;
      L_WARNING_MSG              VARCHAR2 (100) := NULL;
      L_SRC_ORG_ID               NUMBER;
      L_ERRO_DESC                VARCHAR2 (4000);
      l_sr_error_Desc            VARCHAR2 (4000);

      SR_RETURN_STATUS           VARCHAR2 (1) := 'S'; -- in case sourcing rule is not updated still the update to categories should work so hard coded to S
      SR_MSG_COUNT               NUMBER (10);
      SR_MSG_DATA                VARCHAR2 (1000);

      L_ASSIGNMENT_ID            NUMBER;
      L_ASSIGNMENT_SET_ID        NUMBER;
      L_ASSIGNMENT_TYPE          NUMBER;
      L_SOURCING_RULE_TYPE       NUMBER;

      /*--------------
      Record type declarations for sourcing rule
      ----------------*/
      l_assignment_set_rec       mrp_src_assignment_pub.assignment_set_rec_type;
      l_assignment_set_val_rec   mrp_src_assignment_pub.assignment_set_val_rec_type;
      l_assignment_tbl           mrp_src_assignment_pub.assignment_tbl_type;
      l_assignment_val_tbl       mrp_src_assignment_pub.assignment_val_tbl_type;
      o_assignment_set_rec       mrp_src_assignment_pub.assignment_set_rec_type;
      o_assignment_set_val_rec   mrp_src_assignment_pub.assignment_set_val_rec_type;
      O_ASSIGNMENT_TBL           MRP_SRC_ASSIGNMENT_PUB.ASSIGNMENT_TBL_TYPE;
      o_assignment_val_tbl       mrp_src_assignment_pub.assignment_val_tbl_type;
   BEGIN
      L_WARNINGS := NULL;

      -- warnings are checked .. these will not stop the transaction being saved.
      -- it will alert the user the warnings on the page
      IF P_SOURCE_ORG IS NOT NULL
      THEN
         BEGIN
            SELECT SEGMENT1
              INTO l_source_org_class
              FROM MTL_ITEM_CATEGORIES_V
             WHERE     INVENTORY_ITEM_ID = P_INVENTORY_ITEM_ID
                   AND CATEGORY_SET_NAME = 'Sales Velocity'
                   AND ORGANIZATION_ID = P_SOURCE_ORG;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_source_org_class := NULL;
         END;

         IF    L_SOURCE_ORG_CLASS IS NULL
            OR L_SOURCE_ORG_CLASS = 'N'
            OR L_SOURCE_ORG_CLASS = 'Z'
         THEN
            L_WARNINGS := L_WARNINGS || ' Source Org does not stock.';
         END IF;
      END IF;

      IF P_SOURCE_TYPE IS NOT NULL AND P_SOURCE_TYPE = '1'
      THEN
         IF P_SOURCE_ORG IS NULL
         THEN
            L_WARNINGS := L_WARNINGS || 'Source Org required.';
         END IF;
      END IF;

      /*
      Commented as per Tom , no warnings and error messages on Planning method changes
      IF P_RESERVE_STOCK IS NOT NULL AND P_RESERVE_STOCK>0 THEN
          IF P_PLANNING_METHOD ='2' OR P_PLANNING_METHOD ='6' THEN
              L_WARNINGS:=L_WARNINGS||'Check Planning Method.';
          end if;
      END IF;

      IF P_PLANNING_METHOD IS NOT NULL AND P_PLANNING_METHOD='6' THEN
          IF P_SALES_VELOCITY_CLASS ='8' OR P_SALES_VELOCITY_CLASS ='9'  OR P_SALES_VELOCITY_CLASS='B' THEN
              L_WARNINGS:=L_WARNINGS||'Check Planning Method.';
          end if;
      end if;
      */



      /* this code not required

     SELECT  responsibility_id
       INTO l_resp_id
       FROM  APPS.FND_RESPONSIBILITY_VL
      WHERE  responsibility_name ='Product Information Management Data Librarian';

     SELECT  user_id
       INTO l_user_id
       FROM  APPS.FND_USER
      WHERE  USER_NAME ='TJACKSON';

       SELECT user_id
        ,responsibility_id
        ,responsibility_application_id
        ,security_group_id
      INTO l_user_id
          ,l_resp_id
          ,l_appl_id
          ,l_sec_grp_id
      FROM apps.fnd_user_resp_groups
      WHERE user_id = l_USER_ID
      AND RESPONSIBILITY_ID = L_RESP_ID;
      */

      IF P_USER_ID IS NOT NULL
      THEN
         L_USER_ID := P_USER_ID;
      END IF;



      -- Code will not run into exception till PIM module is installed on EBS
      -- double check adding exception
      BEGIN
         SELECT responsibility_id, application_id
           INTO l_resp_id, l_appl_id
           FROM fnd_responsibility
          WHERE responsibility_key = 'EGO_PIM_DATA_LIBRARIAN';
      EXCEPTION
         WHEN OTHERS
         THEN
            NULL;
      END;


      XXWC_ORG_ATTRS_DBG_PRC ('P_INVENTORY_ITEM_ID =>', P_INVENTORY_ITEM_ID);
      XXWC_ORG_ATTRS_DBG_PRC ('P_ORGANIZATION_ID =>', P_ORGANIZATION_ID);
      XXWC_ORG_ATTRS_DBG_PRC ('P_USER_ID =>', P_USER_ID);
      XXWC_ORG_ATTRS_DBG_PRC ('P_SEGMENT1 =>', P_SEGMENT1);
      XXWC_ORG_ATTRS_DBG_PRC ('P_SOURCE_TYPE =>', P_SOURCE_TYPE);
      XXWC_ORG_ATTRS_DBG_PRC ('P_SOURCE_ORG =>', P_SOURCE_ORG);
      XXWC_ORG_ATTRS_DBG_PRC ('P_PREPROCESSING_LEAD_TIME =>',
                              P_PREPROCESSING_LEAD_TIME);
      XXWC_ORG_ATTRS_DBG_PRC ('P_PRCESSING_LEAD_TIME =>',
                              P_PRCESSING_LEAD_TIME);
      XXWC_ORG_ATTRS_DBG_PRC ('P_FIXED_LOT_MULTIPLIER =>',
                              P_FIXED_LOT_MULTIPLIER);
      XXWC_ORG_ATTRS_DBG_PRC ('P_SALES_VELOCITY_CLASS =>',
                              P_SALES_VELOCITY_CLASS);
      XXWC_ORG_ATTRS_DBG_PRC ('P_PLANNING_METHOD =>', P_PLANNING_METHOD);
      XXWC_ORG_ATTRS_DBG_PRC ('P_MINMAX_MINIMUM =>', P_MINMAX_MINIMUM);
      XXWC_ORG_ATTRS_DBG_PRC ('P_MINMAX_MAXIMUM =>', P_MINMAX_MAXIMUM);
      XXWC_ORG_ATTRS_DBG_PRC ('P_PURCHASE_FLAG =>', P_PURCHASE_FLAG);
      XXWC_ORG_ATTRS_DBG_PRC ('P_RESERVE_STOCK =>', P_RESERVE_STOCK);
      XXWC_ORG_ATTRS_DBG_PRC ('P_DEFAULT_BUYER =>', P_DEFAULT_BUYER);
      XXWC_ORG_ATTRS_DBG_PRC ('P_PREV_PURCHASE_FLAG =>',
                              P_PREV_PURCHASE_FLAG);
      XXWC_ORG_ATTRS_DBG_PRC ('P_PREV_SV_CLASS =>', P_PREV_SV_CLASS);
      XXWC_ORG_ATTRS_DBG_PRC ('L_WARNINGS =>', L_WARNINGS);
      XXWC_ORG_ATTRS_DBG_PRC ('P_PREV_SRC_RULE_ID =>', P_PREV_SRC_RULE_ID);
      XXWC_ORG_ATTRS_DBG_PRC ('P_SRC_RULE_ID =>', P_SRC_RULE_ID);

      --IF apps.FND_GLOBAl.user_id <> l_user_id THEN //for now set apps_initialize always
      apps.fnd_global.apps_initialize (l_user_id,
                                       l_resp_id,
                                       l_appl_id,
                                       l_sec_grp_id);
      --END IF;

      --Transaction type  Update for Item and the org
      L_ITEM_TABLE (1).TRANSACTION_TYPE := 'UPDATE';
      l_item_table (1).inventory_item_id := P_INVENTORY_ITEM_ID;
      L_ITEM_TABLE (1).ORGANIZATION_ID := P_ORGANIZATION_ID;

      --set parameters to update
      L_ITEM_TABLE (1).SOURCE_TYPE := P_SOURCE_TYPE;
      L_ITEM_TABLE (1).SOURCE_ORGANIZATION_ID := P_SOURCE_ORG;
      L_ITEM_TABLE (1).PREPROCESSING_LEAD_TIME := P_PREPROCESSING_LEAD_TIME;
      L_ITEM_TABLE (1).FULL_LEAD_TIME := P_PRCESSING_LEAD_TIME;
      L_ITEM_TABLE (1).FIXED_LOT_MULTIPLIER := P_FIXED_LOT_MULTIPLIER;
      -- L_ITEM_TABLE(1). := P_SALES_VELOCITY_CLASS;
      L_ITEM_TABLE (1).INVENTORY_PLANNING_CODE := P_PLANNING_METHOD;
      L_ITEM_TABLE (1).MIN_MINMAX_QUANTITY := P_MINMAX_MINIMUM;
      L_ITEM_TABLE (1).MAX_MINMAX_QUANTITY := P_MINMAX_MAXIMUM;
      -- L_ITEM_TABLE(1). := P_PURCHASE_FLAG;
      L_ITEM_TABLE (1).attribute21 := P_RESERVE_STOCK;
      L_ITEM_TABLE (1).BUYER_ID := P_DEFAULT_BUYER;
      -- L_ITEM_TABLE(1). := P_AMU ;

      DBMS_OUTPUT.PUT_LINE ('=====================================');
      DBMS_OUTPUT.PUT_LINE ('Calling EGO_ITEM_PUB.Process_Items API');

      EGO_ITEM_PUB.Process_Items (p_api_version     => 1.0,
                                  p_init_msg_list   => FND_API.g_TRUE,
                                  P_COMMIT          => FND_API.G_TRUE,
                                  p_Item_Tbl        => l_item_table,
                                  x_Item_Tbl        => x_item_table,
                                  x_return_status   => x_return_status,
                                  x_msg_count       => x_msg_count);

      DBMS_OUTPUT.PUT_LINE ('==================================');
      DBMS_OUTPUT.PUT_LINE ('Return Status ==>' || X_RETURN_STATUS);
      XXWC_ORG_ATTRS_DBG_PRC ('End of Item API Call', '');

      IF (X_RETURN_STATUS = FND_API.G_RET_STS_SUCCESS)
      THEN
         NULL;
      /* -- Commenting code since the call will always reference single item
        FOR i IN 1..x_item_table.COUNT LOOP
           DBMS_OUTPUT.PUT_LINE('Inventory Item Id :'||to_char(x_item_table(i).Inventory_Item_Id));
           DBMS_OUTPUT.PUT_LINE('Organization Id   :'||to_char(x_item_table(i).Organization_Id));
        END LOOP;
        */
      ELSE
         DBMS_OUTPUT.PUT_LINE ('Error Messages :');
         Error_Handler.GET_MESSAGE_LIST (x_message_list => x_message_list);

         FOR I IN 1 .. X_MESSAGE_LIST.COUNT
         LOOP
            l_erro_Desc := l_erro_Desc || x_message_list (i).MESSAGE_TEXT;
            DBMS_OUTPUT.PUT_LINE (x_message_list (i).MESSAGE_TEXT);
         END LOOP;
      END IF;

      COMMIT;
      DBMS_OUTPUT.PUT_LINE ('==================================');
      L_RETURN_STATUS := X_RETURN_STATUS;
      L_ERROR_MSG := L_ERRO_DESC;
      XXWC_ORG_ATTRS_DBG_PRC (L_RETURN_STATUS, L_ERROR_MSG);

      /*------------------------------------------------
      If item attribute changes have completed successfully go to update sourcing rule and other categories
      ------------------------------------------------ */
      IF (X_RETURN_STATUS = 'S')
      THEN
         --Check to see if the sourcing rule has been updated.
         --only if updated run the code to update sourcing rule
         IF P_SRC_RULE_ID IS NOT NULL
         THEN
            IF P_SRC_RULE_ID <> NVL (P_PREV_SRC_RULE_ID, -1)
            THEN
               IF P_PREV_SRC_RULE_ID IS NULL
               THEN
                  L_ASSIGNMENT_ID := NULL;
               ELSE
                  BEGIN
                     SELECT ASSIGNMENT_ID,
                            ASSIGNMENT_SET_ID,
                            ASSIGNMENT_TYPE,
                            SOURCING_RULE_TYPE
                       INTO l_assignment_id,
                            l_ASSIGNMENT_SET_ID,
                            l_ASSIGNMENT_TYPE,
                            l_SOURCING_RULE_TYPE
                       FROM MRP_SR_ASSIGNMENTS
                      WHERE     INVENTORY_ITEM_ID = P_INVENTORY_ITEM_ID
                            AND ORGANIZATION_ID = P_ORGANIZATION_ID
                            AND SOURCING_RULE_ID = P_PREV_SRC_RULE_ID;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        L_ASSIGNMENT_ID := NULL;
                        L_ASSIGNMENT_SET_ID := NULL;
                        L_ASSIGNMENT_TYPE := NULL;
                        L_SOURCING_RULE_TYPE := NULL;
                        SR_RETURN_STATUS := 'E';
                        L_SR_ERROR_DESC :=
                           'Error While updating Sourcing Rule';
                        XXWC_ORG_ATTRS_DBG_PRC (
                           'Inside sourcing rule exception',
                           '');
                  END;
               END IF;

               /*------------------------------------------------
                Sourcing rule update and create are allowed
                Deletion of sourcing rule not allowed
                Sourcing rule update type hard code to item org
               ------------------------------------------------ */
               IF L_ASSIGNMENT_ID IS NOT NULL
               THEN
                  XXWC_ORG_ATTRS_DBG_PRC (
                     'inside  IF L_ASSIGNMENT_ID IS NOT NULL ',
                     '');
                  L_ASSIGNMENT_TBL (1).ASSIGNMENT_ID := L_ASSIGNMENT_ID;
                  L_ASSIGNMENT_TBL (1).OPERATION := 'UPDATE';
               ELSE
                  L_ASSIGNMENT_TBL (1).OPERATION := 'CREATE';
               END IF;

               l_assignment_tbl (1).assignment_set_id := 1;
               L_ASSIGNMENT_TBL (1).ASSIGNMENT_TYPE := 6;
               l_assignment_tbl (1).organization_id := P_ORGANIZATION_ID;
               L_ASSIGNMENT_TBL (1).INVENTORY_ITEM_ID := P_INVENTORY_ITEM_ID;
               l_assignment_tbl (1).sourcing_rule_id := P_SRC_RULE_ID;
               L_ASSIGNMENT_TBL (1).SOURCING_RULE_TYPE := 1;

               MRP_SRC_ASSIGNMENT_PUB.PROCESS_ASSIGNMENT (
                  P_API_VERSION_NUMBER       => 1.0,
                  p_init_msg_list            => fnd_api.g_false,
                  P_RETURN_VALUES            => FND_API.G_FALSE,
                  P_COMMIT                   => FND_API.G_FALSE,
                  X_RETURN_STATUS            => SR_RETURN_STATUS,
                  X_MSG_COUNT                => SR_MSG_COUNT,
                  X_MSG_DATA                 => SR_MSG_DATA,
                  p_assignment_set_rec       => l_assignment_set_rec,
                  P_ASSIGNMENT_SET_VAL_REC   => L_ASSIGNMENT_SET_VAL_REC,
                  p_assignment_tbl           => l_assignment_tbl,
                  P_ASSIGNMENT_VAL_TBL       => L_ASSIGNMENT_VAL_TBL,
                  x_assignment_set_rec       => o_assignment_set_rec,
                  X_ASSIGNMENT_SET_VAL_REC   => O_ASSIGNMENT_SET_VAL_REC,
                  x_assignment_tbl           => o_assignment_tbl,
                  x_assignment_val_tbl       => o_assignment_val_tbl);

               IF SR_RETURN_STATUS = 'S'
               THEN
                  L_RETURN_STATUS := SR_RETURN_STATUS;
                  XXWC_ORG_ATTRS_DBG_PRC (
                     'Sourcing Rule update completed succesfully',
                     '');
               ELSE
                  L_RETURN_STATUS := SR_RETURN_STATUS;

                  IF SR_MSG_COUNT > 0
                  THEN
                     XXWC_ORG_ATTRS_DBG_PRC (
                        'Sourcing Rule completed with error',
                        '');

                     FOR l_index IN 1 .. SR_MSG_COUNT
                     LOOP
                        L_SR_ERROR_DESC :=
                              L_SR_ERROR_DESC
                           || FND_MSG_PUB.GET (
                                 P_MSG_INDEX   => L_INDEX,
                                 P_ENCODED     => FND_API.G_FALSE);
                     END LOOP;

                     L_ERROR_MSG := L_SR_ERROR_DESC;
                  END IF;
               END IF;
            END IF;
         END IF;



         XXWC_ORG_ATTRS_DBG_PRC (SR_RETURN_STATUS, L_SR_ERROR_DESC);

         /*------------------------------------------------
            If Sourcing rule updated successfull proceed to category assignment
         ------------------------------------------------ */
         IF SR_RETURN_STATUS = 'S'
         THEN
            SELECT CATEGORY_SET_ID
              INTO L_SV_CAT_SET_ID
              FROM MTL_CATEGORY_SETS_TL MCST
             WHERE MCST.CATEGORY_SET_NAME = 'Sales Velocity';

            SELECT CATEGORY_SET_ID
              INTO L_PF_CAT_SET_ID
              FROM MTL_CATEGORY_SETS_TL MCST
             WHERE MCST.CATEGORY_SET_NAME = 'Purchase Flag';

            /*------------------------------------------------
              Update categories accordingly..if made null ddelete them
              -- if changed from null to a value create
              -- if changed from one to another value update
            ------------------------------------------------ */
            IF P_PREV_PURCHASE_FLAG IS NOT NULL
            THEN
               SELECT MCb.category_id
                 INTO l_pf_prev_cat_id
                 FROM MTL_CATEGORY_SETS_B mcsb, mtl_categories_B mcb
                WHERE     MCSB.CATEGORY_SET_ID = l_pf_cat_Set_id
                      AND MCB.STRUCTURE_ID = MCSB.STRUCTURE_ID
                      AND mcb.segment1 = P_PREV_PURCHASE_FLAG;

               IF P_PURCHASE_FLAG IS NOT NULL
               THEN
                  SELECT MCb.category_id
                    INTO l_pf_category_id
                    FROM MTL_CATEGORY_SETS_B mcsb, mtl_categories_B mcb
                   WHERE     MCSB.CATEGORY_SET_ID = l_pf_cat_Set_id
                         AND MCB.STRUCTURE_ID = MCSB.STRUCTURE_ID
                         AND mcb.segment1 = P_PURCHASE_FLAG;

                  IF (P_PREV_PURCHASE_FLAG != P_PURCHASE_FLAG)
                  THEN
                     XXWC_ORG_ATTRS_DBG_PRC (
                        'update Purchase Flag category ',
                        '');
                     -- update category_assognment , if there was already a category assigned for that sales velocity then update
                     INV_ITEM_CATEGORY_PUB.UPDATE_CATEGORY_ASSIGNMENT (
                        P_API_VERSION         => 1.0,
                        P_INIT_MSG_LIST       => FND_API.G_FALSE,
                        P_COMMIT              => FND_API.G_FALSE,
                        P_CATEGORY_ID         => l_pf_category_id,
                        P_OLD_CATEGORY_ID     => l_pf_prev_cat_id,
                        P_CATEGORY_SET_ID     => l_pf_cat_Set_id,
                        P_INVENTORY_ITEM_ID   => P_INVENTORY_ITEM_ID,
                        P_ORGANIZATION_ID     => P_ORGANIZATION_ID,
                        X_RETURN_STATUS       => X_RETURN_STATUS,
                        X_ERRORCODE           => X_ERRORCODE,
                        X_MSG_COUNT           => X_MSG_COUNT,
                        X_MSG_DATA            => X_MSG_DATA);

                     IF X_RETURN_STATUS = 'S'
                     THEN
                        L_ERRO_DESC :=
                              L_ERRO_DESC
                           || ' Purchase Flag Category Update successful.';
                     ELSE
                        IF X_MSG_COUNT > 0
                        THEN
                           FOR I IN 1 .. X_MSG_COUNT
                           LOOP
                              L_ERRO_DESC :=
                                    L_ERRO_DESC
                                 || FND_MSG_PUB.GET (
                                       P_MSG_INDEX   => I,
                                       P_ENCODED     => FND_API.G_FALSE);
                           END LOOP;
                        END IF;
                     END IF;
                  END IF;
               ELSE
                  XXWC_ORG_ATTRS_DBG_PRC ('DELETE Purchase Flag category ',
                                          '');
                  INV_ITEM_CATEGORY_PUB.DELETE_CATEGORY_ASSIGNMENT (
                     p_api_version         => 1.0,
                     p_init_msg_list       => FND_API.G_TRUE,
                     p_commit              => FND_API.G_FALSE,
                     x_return_status       => X_RETURN_STATUS,
                     x_errorcode           => X_ERRORCODE,
                     x_msg_count           => X_MSG_COUNT,
                     x_msg_data            => X_MSG_DATA,
                     p_category_id         => l_pf_prev_cat_id,
                     p_category_set_id     => l_pf_cat_Set_id,
                     p_inventory_item_id   => P_INVENTORY_ITEM_ID,
                     P_ORGANIZATION_ID     => P_ORGANIZATION_ID);

                  IF X_RETURN_STATUS = 'S'
                  THEN
                     L_ERRO_DESC :=
                           L_ERRO_DESC
                        || ' Purchase Flag Category Delete successful.';
                  ELSE
                     IF X_MSG_COUNT > 0
                     THEN
                        FOR I IN 1 .. X_MSG_COUNT
                        LOOP
                           L_ERRO_DESC :=
                                 L_ERRO_DESC
                              || FND_MSG_PUB.GET (
                                    P_MSG_INDEX   => I,
                                    P_ENCODED     => FND_API.G_FALSE);
                        END LOOP;
                     END IF;
                  END IF;
               END IF;
            ELSE
               -- code for create category assignment
               -- no previous sales velocity category assigned to item for this org so update
               IF P_PURCHASE_FLAG IS NOT NULL
               THEN
                  SELECT MCb.category_id
                    INTO l_pf_category_id
                    FROM MTL_CATEGORY_SETS_B mcsb, mtl_categories_B mcb
                   WHERE     MCSB.CATEGORY_SET_ID = l_pf_cat_Set_id
                         AND MCB.STRUCTURE_ID = MCSB.STRUCTURE_ID
                         AND MCB.SEGMENT1 = P_PURCHASE_FLAG;

                  XXWC_ORG_ATTRS_DBG_PRC ('CREATE Purchase Flag category ',
                                          '');
                  INV_ITEM_CATEGORY_PUB.CREATE_CATEGORY_ASSIGNMENT (
                     p_api_version         => 1.0,
                     p_init_msg_list       => FND_API.G_TRUE,
                     p_commit              => FND_API.G_FALSE,
                     x_return_status       => X_RETURN_STATUS,
                     x_errorcode           => X_ERRORCODE,
                     x_msg_count           => X_MSG_COUNT,
                     x_msg_data            => X_MSG_DATA,
                     p_category_id         => l_pf_category_id,
                     p_category_set_id     => l_pf_cat_Set_id,
                     p_inventory_item_id   => P_INVENTORY_ITEM_ID,
                     P_ORGANIZATION_ID     => P_ORGANIZATION_ID);

                  IF X_RETURN_STATUS = 'S'
                  THEN
                     L_ERRO_DESC :=
                           L_ERRO_DESC
                        || ' Purchase Flag Category Create successful.';
                  ELSE
                     IF X_MSG_COUNT > 0
                     THEN
                        FOR I IN 1 .. X_MSG_COUNT
                        LOOP
                           L_ERRO_DESC :=
                                 L_ERRO_DESC
                              || FND_MSG_PUB.GET (
                                    P_MSG_INDEX   => I,
                                    P_ENCODED     => FND_API.G_FALSE);
                        END LOOP;
                     END IF;
                  END IF;
               END IF;
            END IF;

            IF P_PREV_SV_CLASS IS NOT NULL
            THEN
               SELECT MCb.category_id
                 INTO l_sv_prev_cat_id
                 FROM MTL_CATEGORY_SETS_B mcsb, mtl_categories_B mcb
                WHERE     MCSB.CATEGORY_SET_ID = l_sv_cat_Set_id
                      AND MCB.STRUCTURE_ID = MCSB.STRUCTURE_ID
                      AND mcb.segment1 = P_PREV_SV_CLASS;

               IF P_SALES_VELOCITY_CLASS IS NOT NULL
               THEN
                  SELECT MCb.category_id
                    INTO l_sv_category_id
                    FROM MTL_CATEGORY_SETS_B mcsb, mtl_categories_B mcb
                   WHERE     MCSB.CATEGORY_SET_ID = l_sv_cat_Set_id
                         AND MCB.STRUCTURE_ID = MCSB.STRUCTURE_ID
                         AND mcb.segment1 = P_SALES_VELOCITY_CLASS;

                  IF (P_PREV_SV_CLASS != P_SALES_VELOCITY_CLASS)
                  THEN
                     XXWC_ORG_ATTRS_DBG_PRC (
                        'update sales velocity category ',
                        '');
                     -- update category_assognment , if there was already a category assigned for that sales velocity then update
                     INV_ITEM_CATEGORY_PUB.UPDATE_CATEGORY_ASSIGNMENT (
                        P_API_VERSION         => 1.0,
                        P_INIT_MSG_LIST       => FND_API.G_FALSE,
                        P_COMMIT              => FND_API.G_FALSE,
                        P_CATEGORY_ID         => l_sv_category_id,
                        P_OLD_CATEGORY_ID     => l_sv_prev_cat_id,
                        P_CATEGORY_SET_ID     => l_sv_cat_Set_id,
                        P_INVENTORY_ITEM_ID   => P_INVENTORY_ITEM_ID,
                        P_ORGANIZATION_ID     => P_ORGANIZATION_ID,
                        X_RETURN_STATUS       => X_RETURN_STATUS,
                        X_ERRORCODE           => X_ERRORCODE,
                        X_MSG_COUNT           => X_MSG_COUNT,
                        X_MSG_DATA            => X_MSG_DATA);

                     IF X_RETURN_STATUS = 'S'
                     THEN
                        L_ERRO_DESC :=
                              L_ERRO_DESC
                           || ' Classification Category Update successful.';
                     ELSE
                        IF X_MSG_COUNT > 0
                        THEN
                           FOR I IN 1 .. X_MSG_COUNT
                           LOOP
                              L_ERRO_DESC :=
                                    L_ERRO_DESC
                                 || FND_MSG_PUB.GET (
                                       P_MSG_INDEX   => I,
                                       P_ENCODED     => FND_API.G_FALSE);
                           END LOOP;
                        END IF;
                     END IF;
                  END IF;
               ELSE
                  XXWC_ORG_ATTRS_DBG_PRC ('DELETE sales velocity category ',
                                          '');
                  INV_ITEM_CATEGORY_PUB.DELETE_CATEGORY_ASSIGNMENT (
                     p_api_version         => 1.0,
                     p_init_msg_list       => FND_API.G_TRUE,
                     p_commit              => FND_API.G_FALSE,
                     x_return_status       => X_RETURN_STATUS,
                     x_errorcode           => X_ERRORCODE,
                     x_msg_count           => X_MSG_COUNT,
                     x_msg_data            => X_MSG_DATA,
                     p_category_id         => l_sv_prev_cat_id,
                     p_category_set_id     => l_sv_cat_Set_id,
                     p_inventory_item_id   => P_INVENTORY_ITEM_ID,
                     P_ORGANIZATION_ID     => P_ORGANIZATION_ID);

                  IF X_RETURN_STATUS = 'S'
                  THEN
                     L_ERRO_DESC :=
                           L_ERRO_DESC
                        || ' Classification Category Delete successful.';
                  ELSE
                     IF X_MSG_COUNT > 0
                     THEN
                        FOR I IN 1 .. X_MSG_COUNT
                        LOOP
                           L_ERRO_DESC :=
                                 L_ERRO_DESC
                              || FND_MSG_PUB.GET (
                                    P_MSG_INDEX   => I,
                                    P_ENCODED     => FND_API.G_FALSE);
                        END LOOP;
                     END IF;
                  END IF;
               END IF;
            ELSE
               -- code for create category assignment
               -- no previous sales velocity category assigned to item for this org so update
               IF P_SALES_VELOCITY_CLASS IS NOT NULL
               THEN
                  SELECT MCb.category_id
                    INTO l_sv_category_id
                    FROM MTL_CATEGORY_SETS_B mcsb, mtl_categories_B mcb
                   WHERE     MCSB.CATEGORY_SET_ID = l_sv_cat_Set_id
                         AND MCB.STRUCTURE_ID = MCSB.STRUCTURE_ID
                         AND MCB.SEGMENT1 = P_SALES_VELOCITY_CLASS;

                  XXWC_ORG_ATTRS_DBG_PRC ('CREATE sales velocity category ',
                                          '');
                  INV_ITEM_CATEGORY_PUB.CREATE_CATEGORY_ASSIGNMENT (
                     p_api_version         => 1.0,
                     p_init_msg_list       => FND_API.G_TRUE,
                     p_commit              => FND_API.G_FALSE,
                     x_return_status       => X_RETURN_STATUS,
                     x_errorcode           => X_ERRORCODE,
                     x_msg_count           => X_MSG_COUNT,
                     x_msg_data            => X_MSG_DATA,
                     p_category_id         => l_sv_category_id,
                     p_category_set_id     => l_sv_cat_Set_id,
                     p_inventory_item_id   => P_INVENTORY_ITEM_ID,
                     P_ORGANIZATION_ID     => P_ORGANIZATION_ID);

                  IF X_RETURN_STATUS = 'S'
                  THEN
                     L_ERRO_DESC :=
                           L_ERRO_DESC
                        || 'Classification Category Create successful.';
                  ELSE
                     IF X_MSG_COUNT > 0
                     THEN
                        FOR I IN 1 .. X_MSG_COUNT
                        LOOP
                           L_ERRO_DESC :=
                                 L_ERRO_DESC
                              || FND_MSG_PUB.GET (
                                    P_MSG_INDEX   => I,
                                    P_ENCODED     => FND_API.G_FALSE);
                        END LOOP;
                     END IF;
                  END IF;
               END IF;
            END IF;

            XXWC_ORG_ATTRS_DBG_PRC ('Status= >' || X_RETURN_STATUS,
                                    'L_ERRO_DESC =>' || L_ERRO_DESC);
            /*------------------------------------------------
               Set the out params based on the info captured
            ------------------------------------------------ */
            L_RETURN_STATUS := X_RETURN_STATUS;
            L_ERROR_MSG := L_ERRO_DESC;
         END IF;                               -- end for sr_return_Status='S'
      END IF;                                   --end of if x_retun_Status='S'

      COMMIT;
   END SYNC_ORG_ATTRIBUTES;
 
 
/********************************************************************************************************************
*   Procedure Name: SETTINGS_VARIABLES
*
*   PURPOSE:   This procedure called in OAF Java file to initialize variables.
*
*   REVISIONS:
*   Ver        Date          Author               Description
*   ---------  ----------    ---------------      -------------------------------------------------------------------
*   1.1        16-Mar-17     P.Vamshidhar         TMS#20170314-00295 - Org Attributes form slowness- Phase2 fix     
********************************************************************************************************************/
   PROCEDURE SETTINGS_VARIABLES (p_inventory_item_id   IN NUMBER,
                                 p_organization_id     IN NUMBER)
   IS
   BEGIN
      g_inventory_item_id := p_inventory_item_id;
      g_organization_id := p_organization_id;      
   END;

/********************************************************************************************************************
*   Procedure Name: XXWC_PO_ORG_ATTR_VW_FUNC
*
*   PURPOSE:   This functrion to get data into view.
*
*   REVISIONS:
*   Ver        Date          Author               Description
*   ---------  ----------    ---------------      -------------------------------------------------------------------
*   1.1        16-Mar-17     P.Vamshidhar         TMS#20170314-00295 - Org Attributes form slowness- Phase2 fix     
********************************************************************************************************************/

   FUNCTION XXWC_PO_ORG_ATTR_VW_FUNC
      RETURN XXWC.XXWC_PO_ORG_ATTR_VW_FUNC_T
      PIPELINED
   IS
      TYPE l_org_attr_record IS RECORD
      (
         INVENTORY_ITEM_ID              NUMBER,
         SEGMENT1                       VARCHAR2 (40 BYTE),
         ORGANIZATION_ID                NUMBER,
         DESCRIPTION                    VARCHAR2 (240 BYTE),
         WEIGHT_UOM_CODE                VARCHAR2 (3 BYTE),
         SOURCE_TYPE                    NUMBER,
         ORG_NAME                       VARCHAR2 (240 BYTE),
         SOURCE_ORGANIZATION_ID         NUMBER,
         SOURCE_ORG_NAME                VARCHAR2 (240 BYTE),
         SOURCING_RULE_NAME             VARCHAR2 (50 BYTE),
         VENDOR_NAME                    VARCHAR2 (240 BYTE),
         VENDOR_SITE                    VARCHAR2 (15 BYTE),
         SOURCE_ORG_RULE_NAME           VARCHAR2 (50 BYTE),
         SOURCE_ORG_VENDOR_NAME         VARCHAR2 (240 BYTE),
         SOURCE_ORG_SITE_NAME           VARCHAR2 (15 BYTE),
         PREPROCESSING_LEAD_TIME        NUMBER,
         POSTPROCESSING_LEAD_TIME       NUMBER,
         FIXED_LOT_MULTIPLIER           NUMBER,
         SALES_VELOCITY                 VARCHAR2 (240 BYTE),
         INVENTORY_PLANNING_CODE        NUMBER,
         MIN_MINMAX_QUANTITY            NUMBER,
         MAX_MINMAX_QUANTITY            NUMBER,
         PURCHASING_ENABLED_FLAG        VARCHAR2 (1 BYTE),
         PURCHASING_ITEM_FLAG           VARCHAR2 (1 BYTE),
         RESERVE_STOCK                  VARCHAR2 (240 BYTE),
         BUYER_ID                       NUMBER (9),
         FULL_NAME                      VARCHAR2 (240 BYTE),
         PREV_PREPLT                    NUMBER,
         PREV_POSTPLT                   NUMBER,
         PREV_FLM                       NUMBER,
         PREV_SALES_VELOCITY            VARCHAR2 (240 BYTE),
         PREV_INVENTORY_PLANNING_CODE   NUMBER,
         PREV_MIN                       NUMBER,
         PREV_MAX                       NUMBER,
         PREV_PURCHASE_ENABLED_FLAG     VARCHAR2 (1 BYTE),
         PREV_PURCHASE_ITEM_FLAG        VARCHAR2 (1 BYTE),
         PREV_RESERVE_STOCK             VARCHAR2 (240 BYTE),
         PREV_BUYER                     VARCHAR2 (240 BYTE),
         SOURCE_ORG_CLASSIFICATION      VARCHAR2 (40 BYTE),
         SOURCING_RULE_ID               NUMBER,
         PURCHASE_FLAG                  VARCHAR2 (40 BYTE),
         PREV_PURCHASE_FLAG             VARCHAR2 (40 BYTE),
         PREV_SOURCE_TYPE               VARCHAR2 (80 BYTE),
         CLASSIFICATION                 VARCHAR2 (40 BYTE),
         PREV_CLASSIFICATION            VARCHAR2 (40 BYTE),
         PREV_PLANNING_CODE             VARCHAR2 (80 BYTE),
         PREV_SOURCING_RULE_ID          NUMBER,
         PREV_SOURCING_RULE_NAME        VARCHAR2 (50 BYTE),
         ORG_CODE                       VARCHAR2 (3 BYTE),
         SOURCE_ORG_CODE                VARCHAR2 (3 BYTE),
         PREVIOUS_MIN                   NUMBER,
         PREVIOUS_MAX                   NUMBER,
         PREV_SOURCE_ORG_CODE           VARCHAR2 (3 BYTE),
         FULL_LEAD_TIME                 NUMBER,
         PREV_FLT                       NUMBER
      );

      l_org_attr_obj     l_org_attr_record;
      get_org_att_data   l_org_attr_record;
      l_call             VARCHAR2 (1000);
      l_err_msg          VARCHAR2 (4000);
   BEGIN
      l_call :=
            'Procedure Start g_inventory_item_id'
         || g_inventory_item_id
         || ' g_organization_id: '
         || g_organization_id;

      IF g_inventory_item_id IS NOT NULL AND g_organization_id IS NOT NULL
      THEN
         l_call := 'MTL_SYSTEM_ITEMS_B Data';

         BEGIN
            SELECT MSIB.INVENTORY_ITEM_ID,
                   MSIB.SEGMENT1 SEGMENT1,
                   MSIB.ORGANIZATION_ID,
                   MSIB.DESCRIPTION,
                   MSIB.PRIMARY_UOM_CODE,
                   MSIB.SOURCE_TYPE,
                   SOURCE_ORGANIZATION_ID,
                   PREPROCESSING_LEAD_TIME,
                   POSTPROCESSING_LEAD_TIME,
                   FIXED_LOT_MULTIPLIER,
                   MSIB.ATTRIBUTE15,
                   INVENTORY_PLANNING_CODE,
                   MIN_MINMAX_QUANTITY,
                   MAX_MINMAX_QUANTITY,
                   PURCHASING_ENABLED_FLAG,
                   PURCHASING_ITEM_FLAG,
                   attribute21,
                   BUYER_ID,
                   PREPROCESSING_LEAD_TIME,
                   POSTPROCESSING_LEAD_TIME,
                   FIXED_LOT_MULTIPLIER,
                   MSIB.ATTRIBUTE15,
                   INVENTORY_PLANNING_CODE,
                   MIN_MINMAX_QUANTITY,
                   MAX_MINMAX_QUANTITY,
                   PURCHASING_ENABLED_FLAG,
                   PURCHASING_ITEM_FLAG,
                   attribute21,
                   MIN_MINMAX_QUANTITY PREVIOUS_MIN,
                   MAX_MINMAX_QUANTITY PREVIOUS_MAX,
                   FULL_LEAD_TIME,
                   FULL_LEAD_TIME PREV_FLT
              INTO GET_ORG_ATT_DATA.INVENTORY_ITEM_ID,
                   GET_ORG_ATT_DATA.SEGMENT1,
                   GET_ORG_ATT_DATA.ORGANIZATION_ID,
                   GET_ORG_ATT_DATA.DESCRIPTION,
                   GET_ORG_ATT_DATA.WEIGHT_UOM_CODE,
                   GET_ORG_ATT_DATA.SOURCE_TYPE,
                   GET_ORG_ATT_DATA.SOURCE_ORGANIZATION_ID,
                   GET_ORG_ATT_DATA.PREPROCESSING_LEAD_TIME,
                   GET_ORG_ATT_DATA.POSTPROCESSING_LEAD_TIME,
                   GET_ORG_ATT_DATA.FIXED_LOT_MULTIPLIER,
                   GET_ORG_ATT_DATA.SALES_VELOCITY,
                   GET_ORG_ATT_DATA.INVENTORY_PLANNING_CODE,
                   GET_ORG_ATT_DATA.MIN_MINMAX_QUANTITY,
                   GET_ORG_ATT_DATA.MAX_MINMAX_QUANTITY,
                   GET_ORG_ATT_DATA.PURCHASING_ENABLED_FLAG,
                   GET_ORG_ATT_DATA.PURCHASING_ITEM_FLAG,
                   GET_ORG_ATT_DATA.RESERVE_STOCK,
                   GET_ORG_ATT_DATA.BUYER_ID,
                   GET_ORG_ATT_DATA.PREV_PREPLT,
                   GET_ORG_ATT_DATA.PREV_POSTPLT,
                   GET_ORG_ATT_DATA.PREV_FLM,
                   GET_ORG_ATT_DATA.PREV_SALES_VELOCITY,
                   GET_ORG_ATT_DATA.PREV_INVENTORY_PLANNING_CODE,
                   GET_ORG_ATT_DATA.PREV_MIN,
                   GET_ORG_ATT_DATA.PREV_MAX,
                   GET_ORG_ATT_DATA.PREV_PURCHASE_ENABLED_FLAG,
                   GET_ORG_ATT_DATA.PREV_PURCHASE_ITEM_FLAG,
                   GET_ORG_ATT_DATA.PREV_RESERVE_STOCK,
                   GET_ORG_ATT_DATA.PREVIOUS_MIN,
                   GET_ORG_ATT_DATA.PREVIOUS_MAX,
                   GET_ORG_ATT_DATA.FULL_LEAD_TIME,
                   GET_ORG_ATT_DATA.PREV_FLT
              FROM APPS.MTL_SYSTEM_ITEMS_B MSIB
             WHERE     msib.inventory_item_id = g_inventory_item_id
                   AND msib.organization_id = g_organization_id;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               RETURN;
         END;

         l_call :=
               'Organization Data '
            || GET_ORG_ATT_DATA.ORGANIZATION_ID
            || ' '
            || GET_ORG_ATT_DATA.INVENTORY_ITEM_ID;

         IF GET_ORG_ATT_DATA.ORGANIZATION_ID IS NOT NULL
         THEN
            BEGIN
               SELECT ORGANIZATION_CODE, ORGANIZATION_NAME
                 INTO GET_ORG_ATT_DATA.ORG_CODE, GET_ORG_ATT_DATA.ORG_NAME
                 FROM ORG_ORGANIZATION_DEFINITIONS
                WHERE ORGANIZATION_ID = GET_ORG_ATT_DATA.ORGANIZATION_ID;
            EXCEPTION
               WHEN OTHERS
               THEN
                  GET_ORG_ATT_DATA.ORG_CODE := NULL;
                  GET_ORG_ATT_DATA.ORG_NAME := NULL;
            END;

            -- Purchaisng flag
            l_call :=
                  'Organization Purchasing Data '
               || GET_ORG_ATT_DATA.ORGANIZATION_ID
               || ' '
               || GET_ORG_ATT_DATA.INVENTORY_ITEM_ID;

            BEGIN
               SELECT SEGMENT1
                 INTO GET_ORG_ATT_DATA.PURCHASE_FLAG
                 FROM MTL_ITEM_CATEGORIES_V
                WHERE     INVENTORY_ITEM_ID =
                             GET_ORG_ATT_DATA.INVENTORY_ITEM_ID
                      AND CATEGORY_SET_NAME = 'Purchase Flag'
                      AND organization_id = GET_ORG_ATT_DATA.ORGANIZATION_ID;

               GET_ORG_ATT_DATA.PREV_PURCHASE_FLAG :=
                  GET_ORG_ATT_DATA.PURCHASE_FLAG;
            EXCEPTION
               WHEN OTHERS
               THEN
                  GET_ORG_ATT_DATA.PURCHASE_FLAG := NULL;
                  GET_ORG_ATT_DATA.PREV_PURCHASE_FLAG := NULL;
            END;

            l_call :=
                  'Organization Sales Data '
               || GET_ORG_ATT_DATA.ORGANIZATION_ID
               || ' '
               || GET_ORG_ATT_DATA.INVENTORY_ITEM_ID;

            BEGIN
               SELECT segment1
                 INTO GET_ORG_ATT_DATA.CLASSIFICATION
                 FROM MTL_ITEM_CATEGORIES_V
                WHERE     INVENTORY_ITEM_ID =
                             GET_ORG_ATT_DATA.INVENTORY_ITEM_ID
                      AND CATEGORY_SET_NAME = 'Sales Velocity'
                      AND organization_id = GET_ORG_ATT_DATA.ORGANIZATION_ID;

               GET_ORG_ATT_DATA.PREV_CLASSIFICATION :=
                  GET_ORG_ATT_DATA.CLASSIFICATION;
            EXCEPTION
               WHEN OTHERS
               THEN
                  GET_ORG_ATT_DATA.CLASSIFICATION := NULL;
                  GET_ORG_ATT_DATA.PREV_CLASSIFICATION := NULL;
            END;

            -- SOURCING RULE
            l_call :=
                  'Organization Sourcing Rule Data '
               || GET_ORG_ATT_DATA.ORGANIZATION_ID
               || ' '
               || GET_ORG_ATT_DATA.INVENTORY_ITEM_ID;

            BEGIN
               SELECT 
                      MSA.SOURCING_RULE_ID,
                      MSR.SOURCING_RULE_NAME,
                      MSOV.VENDOR_NAME,
                      MSOV.VENDOR_SITE
                 INTO GET_ORG_ATT_DATA.SOURCING_RULE_ID,
                      GET_ORG_ATT_DATA.SOURCING_RULE_NAME,
                      GET_ORG_ATT_DATA.VENDOR_NAME,
                      GET_ORG_ATT_DATA.VENDOR_SITE
                 FROM MRP_SR_ASSIGNMENTS MSA,
                      MRP_SOURCING_RULES MSR,
                      MRP_SR_RECEIPT_ORG MSRO,
                      MRP_SR_SOURCE_ORG_V MSOV
                WHERE     MSA.SOURCING_RULE_ID = MSR.SOURCING_RULE_ID
                      AND MSA.SOURCING_RULE_ID = MSRO.SOURCING_RULE_ID
                      AND MSRO.SR_RECEIPT_ID = MSOV.SR_RECEIPT_ID
                      AND MSA.ORGANIZATION_ID =
                             GET_ORG_ATT_DATA.ORGANIZATION_ID
                      AND MSA.INVENTORY_ITEM_ID =
                             GET_ORG_ATT_DATA.INVENTORY_ITEM_ID;

               GET_ORG_ATT_DATA.PREV_SOURCING_RULE_ID :=
                  GET_ORG_ATT_DATA.SOURCING_RULE_ID;
               GET_ORG_ATT_DATA.PREV_SOURCING_RULE_NAME :=
                  GET_ORG_ATT_DATA.SOURCING_RULE_NAME;
            EXCEPTION
               WHEN OTHERS
               THEN
                  GET_ORG_ATT_DATA.SOURCING_RULE_ID := NULL;
                  GET_ORG_ATT_DATA.SOURCING_RULE_NAME := NULL;
                  GET_ORG_ATT_DATA.VENDOR_NAME := NULL;
                  GET_ORG_ATT_DATA.VENDOR_SITE := NULL;
                  GET_ORG_ATT_DATA.PREV_SOURCING_RULE_ID := NULL;
                  GET_ORG_ATT_DATA.PREV_SOURCING_RULE_NAME := NULL;
            END;
         END IF;

         l_call :=
               'Source Organization Data '
            || GET_ORG_ATT_DATA.ORGANIZATION_ID
            || ' '
            || GET_ORG_ATT_DATA.INVENTORY_ITEM_ID
            || ' Source Org '
            || GET_ORG_ATT_DATA.SOURCE_ORGANIZATION_ID;

         IF GET_ORG_ATT_DATA.SOURCE_ORGANIZATION_ID IS NOT NULL
         THEN
            l_call :=
                  'Source Organization Data In  '
               || GET_ORG_ATT_DATA.ORGANIZATION_ID
               || ' '
               || GET_ORG_ATT_DATA.INVENTORY_ITEM_ID
               || ' Source Org '
               || GET_ORG_ATT_DATA.SOURCE_ORGANIZATION_ID;

            BEGIN
               SELECT ORGANIZATION_CODE, ORGANIZATION_NAME
                 INTO GET_ORG_ATT_DATA.SOURCE_ORG_CODE,
                      GET_ORG_ATT_DATA.SOURCE_ORG_NAME
                 FROM ORG_ORGANIZATION_DEFINITIONS
                WHERE ORGANIZATION_ID =
                         GET_ORG_ATT_DATA.SOURCE_ORGANIZATION_ID;

               GET_ORG_ATT_DATA.PREV_SOURCE_ORG_CODE :=
                  GET_ORG_ATT_DATA.SOURCE_ORG_CODE;
            EXCEPTION
               WHEN OTHERS
               THEN
                  GET_ORG_ATT_DATA.SOURCE_ORG_CODE := NULL;
                  GET_ORG_ATT_DATA.SOURCE_ORG_NAME := NULL;
                  GET_ORG_ATT_DATA.PREV_SOURCE_ORG_CODE := NULL;
            END;

            -- Sales Velocity

            l_call :=
                  'Source Org sales Velocity Data'
               || GET_ORG_ATT_DATA.ORGANIZATION_ID
               || ' '
               || GET_ORG_ATT_DATA.INVENTORY_ITEM_ID
               || ' Source Org '
               || GET_ORG_ATT_DATA.SOURCE_ORGANIZATION_ID;

            BEGIN
               SELECT SEGMENT1
                 INTO GET_ORG_ATT_DATA.SOURCE_ORG_CLASSIFICATION
                 FROM MTL_ITEM_CATEGORIES_V
                WHERE     INVENTORY_ITEM_ID =
                             GET_ORG_ATT_DATA.INVENTORY_ITEM_ID
                      AND CATEGORY_SET_NAME = 'Sales Velocity'
                      AND organization_id =
                             GET_ORG_ATT_DATA.SOURCE_ORGANIZATION_ID;
            EXCEPTION
               WHEN OTHERS
               THEN
                  GET_ORG_ATT_DATA.SOURCE_ORG_CLASSIFICATION := NULL;
            END;

            l_call :=
                  'Source Org Sourcing Rules Data'
               || GET_ORG_ATT_DATA.ORGANIZATION_ID
               || ' '
               || GET_ORG_ATT_DATA.INVENTORY_ITEM_ID
               || ' Source Org '
               || GET_ORG_ATT_DATA.SOURCE_ORGANIZATION_ID;

            BEGIN
               SELECT 
                      MSR.SOURCING_RULE_NAME,
                      MSOV.VENDOR_NAME,
                      MSOV.VENDOR_SITE
                 INTO GET_ORG_ATT_DATA.SOURCE_ORG_RULE_NAME,
                      GET_ORG_ATT_DATA.SOURCE_ORG_VENDOR_NAME,
                      GET_ORG_ATT_DATA.SOURCE_ORG_SITE_NAME
                 FROM MRP_SR_ASSIGNMENTS MSA,
                      MRP_SOURCING_RULES MSR,
                      MRP_SR_RECEIPT_ORG MSRO,
                      MRP_SR_SOURCE_ORG_V MSOV
                WHERE     MSA.SOURCING_RULE_ID = MSR.SOURCING_RULE_ID
                      AND MSA.SOURCING_RULE_ID = MSRO.SOURCING_RULE_ID
                      AND MSRO.SR_RECEIPT_ID = MSOV.SR_RECEIPT_ID
                      AND MSA.ORGANIZATION_ID =
                             GET_ORG_ATT_DATA.SOURCE_ORGANIZATION_ID
                      AND MSA.INVENTORY_ITEM_ID =
                             GET_ORG_ATT_DATA.INVENTORY_ITEM_ID;
            EXCEPTION
               WHEN OTHERS
               THEN
                  GET_ORG_ATT_DATA.SOURCE_ORG_RULE_NAME := NULL;
                  GET_ORG_ATT_DATA.SOURCE_ORG_VENDOR_NAME := NULL;
                  GET_ORG_ATT_DATA.SOURCE_ORG_SITE_NAME := NULL;
            END;
         END IF;

         l_call :=
               'Source Org Sourcing Rules Data'
            || GET_ORG_ATT_DATA.ORGANIZATION_ID
            || ' '
            || GET_ORG_ATT_DATA.INVENTORY_ITEM_ID
            || ' Buyer '
            || NVL (GET_ORG_ATT_DATA.BUYER_ID, 0);

         IF NVL (GET_ORG_ATT_DATA.BUYER_ID, 0) > 0
         THEN
            BEGIN
               SELECT FULL_NAME
                 INTO GET_ORG_ATT_DATA.FULL_NAME
                 FROM HR_EMPLOYEES
                WHERE EMPLOYEE_ID = GET_ORG_ATT_DATA.BUYER_ID;

               GET_ORG_ATT_DATA.PREV_BUYER := GET_ORG_ATT_DATA.FULL_NAME;
            EXCEPTION
               WHEN OTHERS
               THEN
                  GET_ORG_ATT_DATA.FULL_NAME := NULL;
                  GET_ORG_ATT_DATA.PREV_BUYER := NULL;
            END;
         END IF;

         l_call :=
               'Source Org Sourcing Rules Data'
            || GET_ORG_ATT_DATA.ORGANIZATION_ID
            || ' '
            || GET_ORG_ATT_DATA.INVENTORY_ITEM_ID
            || ' Source Type '
            || GET_ORG_ATT_DATA.SOURCE_TYPE;

         --Prev  source type
         IF GET_ORG_ATT_DATA.SOURCE_TYPE IS NOT NULL
         THEN
            BEGIN
               SELECT meaning
                 INTO GET_ORG_ATT_DATA.PREV_SOURCE_TYPE
                 FROM APPS.FND_LOOKUP_VALUES
                WHERE     LOOKUP_TYPE = 'MTL_SOURCE_TYPES'
                      AND LANGUAGE = USERENV ('LANG')
                      AND TO_NUMBER (lookup_code) =
                             GET_ORG_ATT_DATA.SOURCE_TYPE;
            EXCEPTION
               WHEN OTHERS
               THEN
                  GET_ORG_ATT_DATA.PREV_SOURCE_TYPE := NULL;
            END;
         END IF;

         l_call :=
               'Source Org Sourcing Rules Data'
            || GET_ORG_ATT_DATA.ORGANIZATION_ID
            || ' '
            || GET_ORG_ATT_DATA.INVENTORY_ITEM_ID
            || ' Inventory Planning '
            || GET_ORG_ATT_DATA.INVENTORY_PLANNING_CODE;

         IF GET_ORG_ATT_DATA.INVENTORY_PLANNING_CODE IS NOT NULL
         THEN
            BEGIN
               SELECT MEANING
                 INTO GET_ORG_ATT_DATA.PREV_PLANNING_CODE
                 FROM APPS.FND_LOOKUP_VALUES
                WHERE     LOOKUP_TYPE = 'MTL_MATERIAL_PLANNING'
                      AND LANGUAGE = USERENV ('LANG')
                      AND TO_NUMBER (LOOKUP_CODE) =
                             GET_ORG_ATT_DATA.INVENTORY_PLANNING_CODE;
            EXCEPTION
               WHEN OTHERS
               THEN
                  GET_ORG_ATT_DATA.PREV_PLANNING_CODE := NULL;
            END;
         END IF;

         l_call := ' Pipe Linned function calling';

         PIPE ROW (XXWC.XXWC_PO_ORG_ATTR_VW_FUNC_OBJ (
                      GET_ORG_ATT_DATA.INVENTORY_ITEM_ID,
                      GET_ORG_ATT_DATA.SEGMENT1,
                      GET_ORG_ATT_DATA.ORGANIZATION_ID,
                      GET_ORG_ATT_DATA.DESCRIPTION,
                      GET_ORG_ATT_DATA.WEIGHT_UOM_CODE,
                      GET_ORG_ATT_DATA.SOURCE_TYPE,
                      GET_ORG_ATT_DATA.ORG_NAME,
                      GET_ORG_ATT_DATA.SOURCE_ORGANIZATION_ID,
                      GET_ORG_ATT_DATA.SOURCE_ORG_NAME,
                      GET_ORG_ATT_DATA.SOURCING_RULE_NAME,
                      GET_ORG_ATT_DATA.VENDOR_NAME,
                      GET_ORG_ATT_DATA.VENDOR_SITE,
                      GET_ORG_ATT_DATA.SOURCE_ORG_RULE_NAME,
                      GET_ORG_ATT_DATA.SOURCE_ORG_VENDOR_NAME,
                      GET_ORG_ATT_DATA.SOURCE_ORG_SITE_NAME,
                      GET_ORG_ATT_DATA.PREPROCESSING_LEAD_TIME,
                      GET_ORG_ATT_DATA.POSTPROCESSING_LEAD_TIME,
                      GET_ORG_ATT_DATA.FIXED_LOT_MULTIPLIER,
                      GET_ORG_ATT_DATA.SALES_VELOCITY,
                      GET_ORG_ATT_DATA.INVENTORY_PLANNING_CODE,
                      GET_ORG_ATT_DATA.MIN_MINMAX_QUANTITY,
                      GET_ORG_ATT_DATA.MAX_MINMAX_QUANTITY,
                      GET_ORG_ATT_DATA.PURCHASING_ENABLED_FLAG,
                      GET_ORG_ATT_DATA.PURCHASING_ITEM_FLAG,
                      GET_ORG_ATT_DATA.RESERVE_STOCK,
                      GET_ORG_ATT_DATA.BUYER_ID,
                      GET_ORG_ATT_DATA.FULL_NAME,
                      GET_ORG_ATT_DATA.PREV_PREPLT,
                      GET_ORG_ATT_DATA.PREV_POSTPLT,
                      GET_ORG_ATT_DATA.PREV_FLM,
                      GET_ORG_ATT_DATA.PREV_SALES_VELOCITY,
                      GET_ORG_ATT_DATA.PREV_INVENTORY_PLANNING_CODE,
                      GET_ORG_ATT_DATA.PREV_MIN,
                      GET_ORG_ATT_DATA.PREV_MAX,
                      GET_ORG_ATT_DATA.PREV_PURCHASE_ENABLED_FLAG,
                      GET_ORG_ATT_DATA.PREV_PURCHASE_ITEM_FLAG,
                      GET_ORG_ATT_DATA.PREV_RESERVE_STOCK,
                      GET_ORG_ATT_DATA.PREV_BUYER,
                      GET_ORG_ATT_DATA.SOURCE_ORG_CLASSIFICATION,
                      GET_ORG_ATT_DATA.SOURCING_RULE_ID,
                      GET_ORG_ATT_DATA.PURCHASE_FLAG,
                      GET_ORG_ATT_DATA.PREV_PURCHASE_FLAG,
                      GET_ORG_ATT_DATA.PREV_SOURCE_TYPE,
                      GET_ORG_ATT_DATA.CLASSIFICATION,
                      GET_ORG_ATT_DATA.PREV_CLASSIFICATION,
                      GET_ORG_ATT_DATA.PREV_PLANNING_CODE,
                      GET_ORG_ATT_DATA.PREV_SOURCING_RULE_ID,
                      GET_ORG_ATT_DATA.PREV_SOURCING_RULE_NAME,
                      GET_ORG_ATT_DATA.ORG_CODE,
                      GET_ORG_ATT_DATA.SOURCE_ORG_CODE,
                      GET_ORG_ATT_DATA.PREVIOUS_MIN,
                      GET_ORG_ATT_DATA.PREVIOUS_MAX,
                      GET_ORG_ATT_DATA.PREV_SOURCE_ORG_CODE,
                      GET_ORG_ATT_DATA.FULL_LEAD_TIME,
                      GET_ORG_ATT_DATA.PREV_FLT));
      END IF;

      RETURN;
      
   EXCEPTION
      WHEN OTHERS
      THEN
         l_err_msg :=
               l_err_msg
            || ' ...Error_Stack...'
            || DBMS_UTILITY.format_error_stack ()
            || ' Error_Backtrace...'
            || DBMS_UTILITY.format_error_backtrace ();

         -- Calling ERROR API
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         =>    'XXWC_EGO_ITEM_ORG_ATTRS'
                                     || '.'
                                     || 'XXWC_PO_ORG_ATTR_VW_FUNC',
            p_calling             => SUBSTR (l_call, 1, 1000),
            p_ora_error_msg       => SUBSTR (l_err_msg, 1, 2000),
            p_error_desc          =>    SUBSTR (l_err_msg, 1, 240)
                                     || ' G_inv_item_id '
                                     || g_inventory_item_id
                                     || 'g_org_id '
                                     || g_organization_id,
            p_distribution_list   => 'HDSOracleDevelopers@hdsupply.com',
            p_module              => 'XXWC');
      RETURN;
   END;
END XXWC_EGO_ITEM_ORG_ATTRS;
/