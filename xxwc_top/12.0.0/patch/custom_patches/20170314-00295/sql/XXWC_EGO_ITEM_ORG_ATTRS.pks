/* Formatted on 5/25/2013 8:51:41 AM (QP5 v5.240.12305.39446) */
CREATE OR REPLACE PACKAGE APPS.XXWC_EGO_ITEM_ORG_ATTRS
AS
   /**************************************************************************
    File Name:XXWC_EGO_ITEM_ORG_ATTRS
    PROGRAM TYPE: PL/SQL Package spec and body
    PURPOSE: Updates Item org attributes
    HISTORY
    -- Description   : Called from the trigger XXWC Org Attributes page
    -- Dependencies Tables        : custom table XXWC_ORG_ATTRS_DBG
    -- Dependencies Views         : None
    -- Dependencies Sequences     : XXWC_ORG_ATTRS_DBG_S
    -- Dependencies Procedures    : XXWC_ORG_ATTRS_DBG_PKG
    ================================================================
           Last Update Date : 12/12/12
    ================================================================
    ================================================================
    VERSION DATE          AUTHOR(S)       DESCRIPTION
    ------- -----------   --------------- --------------------------------------------------------------
    1.0     23/11/12    Santhosh Louis   Initial creation of the package
	1.1     16-Mar-17   P.Vamshidhar     TMS#20170314-00295 - Org Attributes form slowness- Phase2 fix 

   *****************************************************************************************************/
   /* TODO enter package declarations (types, exceptions, methods etc) here */

--   g_inventory_item_id   NUMBER :=2928900;
--   g_organization_id     NUMBER :=244;

   g_inventory_item_id   NUMBER;
   g_organization_id     NUMBER;


   PROCEDURE SYNC_ORG_ATTRIBUTES (
      P_INVENTORY_ITEM_ID                       NUMBER,
      P_ORGANIZATION_ID                         NUMBER,
      P_USER_ID                                 NUMBER,
      P_SEGMENT1                                VARCHAR2,
      P_SOURCE_TYPE                             VARCHAR2,
      P_SOURCE_ORG                              VARCHAR2,
      P_SRC_RULE_ID                             NUMBER,
      P_PREPROCESSING_LEAD_TIME                 NUMBER,
      P_PRCESSING_LEAD_TIME                     NUMBER,
      P_FIXED_LOT_MULTIPLIER                    NUMBER,
      P_SALES_VELOCITY_CLASS                    VARCHAR2,
      P_PLANNING_METHOD                         VARCHAR2,
      P_MINMAX_MINIMUM                          NUMBER,
      P_MINMAX_MAXIMUM                          NUMBER,
      P_PURCHASE_FLAG                           VARCHAR2,
      P_RESERVE_STOCK                           VARCHAR2,
      P_DEFAULT_BUYER                           NUMBER,
      P_PREV_PURCHASE_FLAG                      VARCHAR2,
      P_PREV_SV_CLASS                           VARCHAR2,
      P_PREV_SRC_RULE_ID                        NUMBER,
      l_WARNINGS                     OUT NOCOPY VARCHAR2,
      L_RETURN_STATUS                OUT NOCOPY VARCHAR2,
      l_ERROR_MSG                    OUT NOCOPY VARCHAR2);

	  
   -- Added below code in Rev 1.1 by Vamshi - Begin	  
   FUNCTION XXWC_PO_ORG_ATTR_VW_FUNC
      RETURN XXWC.XXWC_PO_ORG_ATTR_VW_FUNC_T
      PIPELINED;
      
   PROCEDURE SETTINGS_VARIABLES (P_INVENTORY_ITEM_ID IN NUMBER, P_ORGANIZATION_ID IN NUMBER);   
   -- Added above code in Rev 1.1 by Vamshi - End
   	  
END XXWC_EGO_ITEM_ORG_ATTRS;
/