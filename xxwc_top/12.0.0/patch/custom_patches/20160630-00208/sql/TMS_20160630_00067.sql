/*
SET SERVEROUTPUT ON FORMAT WRAPPED;

SET VERIFY OFF;

SET FEEDBACK OFF;

SET TERMOUT OFF;
*/

DROP TABLE APPS.ap_undo_checks; 
/
DROP TABLE APPS.ap_undo_invoices; 
/
--
CREATE TABLE ap_undo_invoices
AS
   SELECT DISTINCT aid.invoice_id,
                   aid.accounting_event_id event_id,
                   TO_DATE (SYSDATE, 'DD-MON-YYYY') proposed_undo_date,
                   'Y' Process_flag
     FROM ap_invoice_distributions_all aid
    WHERE aid.invoice_id IN (11987789);
/

CREATE TABLE ap_undo_checks
AS
   SELECT check_id,
          accounting_event_id event_id,
          TO_DATE (SYSDATE, 'DD-MON-YYYY') proposed_undo_date,
          'Y' Process_flag
     FROM ap_payment_history_all
    WHERE check_id IN (SELECT aip1.check_id
                         FROM ap_invoice_payments_all aip1
                        WHERE     aip1.invoice_id =
                                     (SELECT DISTINCT invoice_id
                                        FROM ap_undo_invoices)
                              AND aip1.posted_flag = 'Y');
/

SPOOL /obase/ebiz/apps/apps_st/appl/xxwc/12.0.0/sql/output_AP_issue_05072016.txt;

SET SERVEROUTPUT ON SIZE 1000000;

Declare
  l_file_location     VARCHAR2(1000);
  l_bug_no         NUMBER := 12345;
  l_user_name         FND_USER.USER_NAME%TYPE := 'PA022863';
  l_resp_name         FND_RESPONSIBILITY_TL.RESPONSIBILITY_NAME%TYPE := 'HDS Payables Manager - WC';
  l_driver_table         ALL_TABLES.TABLE_NAME%TYPE := 'ap_undo_checks';
  l_calling_sequence     VARCHAR2(4000);
  l_source_type         VARCHAR2(20) := 'AP_PAYMENTS';
  l_del_adj        BOOLEAN := FALSE;
  l_org_id           NUMBER;

begin

l_calling_sequence := 'From the undo_bulk script';

  AP_ACCTG_DATA_FIX_PKG.Open_Log_Out_Files(90877,l_file_location);
  AP_ACCTG_DATA_FIX_PKG.Print('<html><body>');

  DBMS_OUTPUT.Put_Line('The file location is:'||l_file_location);

  AP_ACCTG_DATA_FIX_PKG.apps_initialize
      (l_user_name, l_resp_name, l_calling_sequence);

  AP_ACCTG_DATA_FIX_PKG.undo_acctg_entries
      (p_bug_no            => l_bug_no,
       p_driver_table      => l_driver_table,
       p_calling_sequence  => l_calling_sequence
      );

For I in (select Distinct check_id from ap_undo_checks where process_flag = 'D')
Loop

 Select org_id
into l_org_id
from ap_checks_all 
 where check_id = I.check_id;

 MO_GLOBAL.SET_POLICY_CONTEXT( 'S', l_org_id );


 if(
AP_ACCTG_DATA_FIX_PKG.delete_cascade_adjustments
(p_source_type => l_source_type,
  p_source_id   => I.check_id
  )) Then
  DBMS_OUTPUT.Put_Line('deletion of Payment adjustment done for ' || I.check_id);

  end if;
  End Loop;


  AP_ACCTG_DATA_FIX_PKG.Close_Log_Out_Files;
  COMMIT;

EXCEPTION
  WHEN OTHERS THEN
    DBMS_OUTPUT.PUT_LINE(SQLERRM);
END;
/
spool off;
/