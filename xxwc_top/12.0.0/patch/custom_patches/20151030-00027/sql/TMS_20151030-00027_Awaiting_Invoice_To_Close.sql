/*
 TMS: 20151030-00027     
 Date: 10/30/2015
 Notes: @SF data fix script for I628434 O# 18120371 SHOWS AWAITING INVOICE but is closed on back end.
*/

SET SERVEROUTPUT ON SIZE 100000;
SET VERIFY OFF;


update apps.oe_order_lines_all
set 
  INVOICE_INTERFACE_STATUS_CODE='YES'
,open_flag='N'
,flow_status_code='CLOSED'
,INVOICED_QUANTITY=1
where line_id=54797326
and headeR_id=33369640;
		  
--1 row expected to be updated

commit;

/