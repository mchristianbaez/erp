/******************************************************************************
  $Header TMS_20170412-00012_open_transfer.sql $
  Module Name:Data Fix script for TMS 20170412-00012

  PURPOSE: Data fix script for TMS 20170412-00012  Open Transfer

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    -------------------------------
  1.0        11-MAY-2017  Krishna Kumar        TMS#20170412-00012

*******************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000

BEGIN
   DBMS_OUTPUT.put_line ('Before Update');

  UPDATE  apps.oe_order_lines_all
     SET  invoice_interface_status_code ='NOT_ELIGIBLE'
          ,open_flag='N'
          ,flow_status_code='CLOSED'
  WHERE   line_id = 88499658
    AND   header_id= 54310195;

   DBMS_OUTPUT.put_line ('Records update-' || SQL%ROWCOUNT);
   COMMIT;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Unable to update record ' || SQLERRM);
END;
/