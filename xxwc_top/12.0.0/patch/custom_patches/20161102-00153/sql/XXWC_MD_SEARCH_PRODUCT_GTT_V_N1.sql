  /*******************************************************************************
  Index: XXWC_MD_SEARCH_PROD_GTT_V_N1
  Description: This index is used on XXWC_MD_SEARCH_PROD_GTT_TBL_V table 
  and is used on AIS form
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     01-Mar-2016        Pahwa Nancy   TMS# 20161102-00153   Performance Tuning
  ********************************************************************************/
  CREATE INDEX "XXWC"."XXWC_MD_SEARCH_PROD_GTT_V_N1" ON "XXWC"."XXWC_MD_SEARCH_PROD_GTT_TBL_V" ("INVENTORY_ITEM_ID") ;
/