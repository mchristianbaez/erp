/*************************************************************************
 Copyright (c) 2012 HD Supply
 All rights reserved.
**************************************************************************
   $Header APPS.XXWC_MD_PRODUCT_STORE_PREF_V $
  Module Name: APPS.XXWC_MD_PRODUCT_STORE_PREF_V 

  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     1-SEP-2015   Pahwa, Nancy                Initially Created 
TMS# 20161102-00153 

**************************************************************************/

begin   
   ctx_ddl.create_preference ('XXWC_MD_PRODUCT_STORE_PREF_V', 'multi_column_datastore');  
   ctx_ddl.set_attribute   
 	 ('XXWC_MD_PRODUCT_STORE_PREF_V',   
 	  'columns',   
 	  'partnumber,   
 	   shortdescription,
     cross_reference');  
   ctx_ddl.create_preference ('XXWC_MD_PRODUCT_STORE_LEX1_V', 'basic_lexer');  
   ctx_ddl.set_attribute ('XXWC_MD_PRODUCT_STORE_LEX1_V', 'whitespace', '/\|-_+,');  
   ctx_ddl.create_section_group ('XXWC_MD_PRODUCT_STORE_SG_V', 'basic_section_group');  
-- 1.1 start
   ctx_ddl.add_sdata_section('XXWC_MD_PRODUCT_STORE_SG_V', 'partnumber', 'partnumber', 'VARCHAR2');
-- 1.1 end 
-- ctx_ddl.add_field_section ('XXWC_MD_PRODUCT_STORE_SG_V', 'partnumber', 'partnumber', true);  
   ctx_ddl.add_field_section ('XXWC_MD_PRODUCT_STORE_SG_V', 'shortdescription', 'shortdescription', true);  
   ctx_ddl.add_field_section ('XXWC_MD_PRODUCT_STORE_SG_V', 'cross_reference', 'cross_reference', true);  
  end;  
/