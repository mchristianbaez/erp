/*************************************************************************
 Copyright (c) 2012 HD Supply
 All rights reserved.
**************************************************************************
   $Header APPS.XXWC_MD_PRODUCT_STORE_PREF1_V $
  Module Name: APPS.XXWC_MD_PRODUCT_STORE_PREF1_V

  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     1-SEP-2015   Pahwa, Nancy                Initially Created 
TMS# 20161102-00153   
**************************************************************************/

begin   
  ctx_ddl.create_preference ('XXWC_MD_PRODUCT_STORE_PREF1_V', 'multi_column_datastore');  
  ctx_ddl.set_attribute   
    ('XXWC_MD_PRODUCT_STORE_PREF1_V',   
     'columns',   
     'partnumber2,   
      shortdescription');  
  ctx_ddl.create_preference ('XXWC_MD_PRODUCT_STORE_LEX2_V', 'basic_lexer');  
  ctx_ddl.set_attribute ('XXWC_MD_PRODUCT_STORE_LEX2_V', 'whitespace', '/\|-_+,');  
  ctx_ddl.create_section_group ('XXWC_MD_PRODUCT_STORE_SG1_V', 'basic_section_group');
  ctx_ddl.add_sdata_section('XXWC_MD_PRODUCT_STORE_SG1_V', 'partnumber2', 'partnumber2', 'VARCHAR2'); 
  ctx_ddl.add_field_section ('XXWC_MD_PRODUCT_STORE_SG1_V', 'shortdescription', 'shortdescription', true); 
  end;
/