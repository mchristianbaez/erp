--Report Name            : HDS-GL_AP_Drilldown_Account_Range
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating View Data for HDS-GL_AP_Drilldown_Account_Range
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_GL_SL_180_V
xxeis.eis_rs_ins.v( 'EIS_GL_SL_180_V',101,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Gl Sl 180 V','EGS1V','','');
--Delete View Columns for EIS_GL_SL_180_V
xxeis.eis_rs_utility.delete_view_rows('EIS_GL_SL_180_V',101,FALSE);
--Inserting View Columns for EIS_GL_SL_180_V
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','BATCH_NAME',101,'Batch Name','BATCH_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Batch Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','ACC_DATE',101,'Acc Date','ACC_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Acc Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','ASSOCIATE_NUM',101,'Associate Num','ASSOCIATE_NUM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Associate Num','','','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','BATCH',101,'Batch','BATCH','','','','XXEIS_RS_ADMIN','NUMBER','','','Batch','','','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','NAME',101,'Name','NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','LINE_ENT_CR',101,'Line Ent Cr','LINE_ENT_CR','','','','XXEIS_RS_ADMIN','NUMBER','','','Line Ent Cr','','','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','CUSTOMER_OR_VENDOR',101,'Customer Or Vendor','CUSTOMER_OR_VENDOR','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Or Vendor','','','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','LINE_ENT_DR',101,'Line Ent Dr','LINE_ENT_DR','','','','XXEIS_RS_ADMIN','NUMBER','','','Line Ent Dr','','','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','LINE_DESCR',101,'Line Descr','LINE_DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Line Descr','','','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','EFFECTIVE_PERIOD_NUM',101,'Effective Period Num','EFFECTIVE_PERIOD_NUM','','','','XXEIS_RS_ADMIN','NUMBER','','','Effective Period Num','','','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','ENTRY',101,'Entry','ENTRY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Entry','','','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','GL_SL_LINK_ID',101,'Gl Sl Link Id','GL_SL_LINK_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Gl Sl Link Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','HNUMBER',101,'Hnumber','HNUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Hnumber','','','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','H_SEQ_ID',101,'H Seq Id','H_SEQ_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','H Seq Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','JE_CATEGORY',101,'Je Category','JE_CATEGORY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Je Category','','','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','JE_HEADER_ID',101,'Je Header Id','JE_HEADER_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Je Header Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','JE_LINE_NUM',101,'Je Line Num','JE_LINE_NUM','','','','XXEIS_RS_ADMIN','NUMBER','','','Je Line Num','','','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','LLINE',101,'Lline','LLINE','','','','XXEIS_RS_ADMIN','NUMBER','','','Lline','','','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','LSEQUENCE',101,'Lsequence','LSEQUENCE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Lsequence','','','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','SEQ_ID',101,'Seq Id','SEQ_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Seq Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','SEQ_NUM',101,'Seq Num','SEQ_NUM','','','','XXEIS_RS_ADMIN','NUMBER','','','Seq Num','','','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','SOURCE',101,'Source','SOURCE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Source','','','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','TRANSACTION_NUM',101,'Transaction Num','TRANSACTION_NUM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Transaction Num','','','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','TYPE',101,'Type','TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','GL_ACCOUNT_STRING',101,'Gl Account String','GL_ACCOUNT_STRING','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gl Account String','','','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','LINE_ACCTD_CR',101,'Line Acctd Cr','LINE_ACCTD_CR','','','','XXEIS_RS_ADMIN','NUMBER','','','Line Acctd Cr','','','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','LINE_ACCTD_DR',101,'Line Acctd Dr','LINE_ACCTD_DR','','','','XXEIS_RS_ADMIN','NUMBER','','','Line Acctd Dr','','','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','CURRENCY_CODE',101,'Currency Code','CURRENCY_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','Common Outputs','Common Outputs','Currency Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','PERIOD_NAME',101,'Period Name','PERIOD_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','Common Outputs','Common Outputs','Period Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','SLA_DIST_ACCOUNTED_CR',101,'Sla Dist Accounted Cr','SLA_DIST_ACCOUNTED_CR','','','','XXEIS_RS_ADMIN','NUMBER','','','Sla Dist Accounted Cr','','','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','SLA_DIST_ACCOUNTED_DR',101,'Sla Dist Accounted Dr','SLA_DIST_ACCOUNTED_DR','','','','XXEIS_RS_ADMIN','NUMBER','','','Sla Dist Accounted Dr','','','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','SLA_DIST_ENTERED_CR',101,'Sla Dist Entered Cr','SLA_DIST_ENTERED_CR','','','','XXEIS_RS_ADMIN','NUMBER','','','Sla Dist Entered Cr','','','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','SLA_DIST_ENTERED_DR',101,'Sla Dist Entered Dr','SLA_DIST_ENTERED_DR','','','','XXEIS_RS_ADMIN','NUMBER','','','Sla Dist Entered Dr','','','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','SLA_LINE_ACCOUNTED_CR',101,'Sla Line Accounted Cr','SLA_LINE_ACCOUNTED_CR','','','','XXEIS_RS_ADMIN','NUMBER','','','Sla Line Accounted Cr','','','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','SLA_LINE_ACCOUNTED_DR',101,'Sla Line Accounted Dr','SLA_LINE_ACCOUNTED_DR','','','','XXEIS_RS_ADMIN','NUMBER','','','Sla Line Accounted Dr','','','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','SLA_LINE_ENTERED_CR',101,'Sla Line Entered Cr','SLA_LINE_ENTERED_CR','','','','XXEIS_RS_ADMIN','NUMBER','','','Sla Line Entered Cr','','','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','SLA_LINE_ENTERED_DR',101,'Sla Line Entered Dr','SLA_LINE_ENTERED_DR','','','','XXEIS_RS_ADMIN','NUMBER','','','Sla Line Entered Dr','','','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','ACCOUNTED_CR',101,'Accounted Cr','ACCOUNTED_CR','','','','XXEIS_RS_ADMIN','NUMBER','','','Accounted Cr','','','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','ACCOUNTED_DR',101,'Accounted Dr','ACCOUNTED_DR','','','','XXEIS_RS_ADMIN','NUMBER','','','Accounted Dr','','','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','ENTERED_CR',101,'Entered Cr','ENTERED_CR','','','','XXEIS_RS_ADMIN','NUMBER','','','Entered Cr','','','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','ENTERED_DR',101,'Entered Dr','ENTERED_DR','','','','XXEIS_RS_ADMIN','NUMBER','','','Entered Dr','','','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','SLA_DIST_ACCOUNTED_NET',101,'Sla Dist Accounted Net','SLA_DIST_ACCOUNTED_NET','','','','XXEIS_RS_ADMIN','NUMBER','','','Sla Dist Accounted Net','','','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','SLA_DIST_ENTERED_NET',101,'Sla Dist Entered Net','SLA_DIST_ENTERED_NET','','','','XXEIS_RS_ADMIN','NUMBER','','','Sla Dist Entered Net','','','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','SLA_LINE_ACCOUNTED_NET',101,'Sla Line Accounted Net','SLA_LINE_ACCOUNTED_NET','','','','XXEIS_RS_ADMIN','NUMBER','','','Sla Line Accounted Net','','','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','SLA_LINE_ENTERED_NET',101,'Sla Line Entered Net','SLA_LINE_ENTERED_NET','','','','XXEIS_RS_ADMIN','NUMBER','','','Sla Line Entered Net','','','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','GCC#50328#PRODUCT',101,'Accounting Flexfield : Segment ''Product'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#PRODUCT','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT1','GCC#Product','50328','1014547','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','GCC#50328#LOCATION',101,'Accounting Flexfield : Segment ''Location'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT2','GCC#Location','50328','1014548','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','GCC#50328#COST_CENTER',101,'Accounting Flexfield : Segment ''Cost Center'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#COST_CENTER','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT3','GCC#Cost Center','50328','1014549','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','GCC#50328#ACCOUNT',101,'Accounting Flexfield : Segment ''Account'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#ACCOUNT','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT4','GCC#Account','50328','1014550','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','GCC#50328#ACCOUNT#DESCR',101,'Accounting Flexfield : Segment ''Account'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#ACCOUNT#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT4','GCC#Account Descr','50328','1014550','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','GCC#50328#PROJECT_CODE',101,'Accounting Flexfield : Segment ''Project Code'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#PROJECT_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT5','GCC#Project Code','50328','1014551','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','GCC#50328#FURTURE_USE',101,'Accounting Flexfield : Segment ''Furture Use'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#FURTURE_USE','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT6','GCC#Furture Use','50328','1014552','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','GCC#50328#FUTURE_USE_2',101,'Accounting Flexfield : Segment ''Future Use 2'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#FUTURE_USE_2','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT7','GCC#Future Use 2','50328','1014948','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','SEGMENT4',101,'Segment4','SEGMENT4','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Segment4','','','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','SEGMENT1',101,'Segment1','SEGMENT1','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Segment1','','','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','SEGMENT2',101,'Segment2','SEGMENT2','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Segment2','','','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','SEGMENT3',101,'Segment3','SEGMENT3','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Segment3','','','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','SEGMENT5',101,'Segment5','SEGMENT5','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Segment5','','','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','CODE_COMBINATION_ID',101,'Code Combination Id','CODE_COMBINATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Code Combination Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','GCC#50328#PRODUCT#DESCR',101,'Accounting Flexfield : Segment ''Product'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#PRODUCT#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT1','GCC#Product Descr','50328','1014547','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','GCC#50328#LOCATION#DESCR',101,'Accounting Flexfield : Segment ''Location'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#LOCATION#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT2','GCC#Location Descr','50328','1014548','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','GCC#50328#COST_CENTER#DESCR',101,'Accounting Flexfield : Segment ''Cost Center'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#COST_CENTER#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT3','GCC#Cost Center Descr','50328','1014549','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','GCC#50328#PROJECT_CODE#DESCR',101,'Accounting Flexfield : Segment ''Project Code'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#PROJECT_CODE#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT5','GCC#Project Code Descr','50328','1014551','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','GCC#50328#FURTURE_USE#DESCR',101,'Accounting Flexfield : Segment ''Furture Use'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#FURTURE_USE#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT6','GCC#Furture Use Descr','50328','1014552','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','GCC#50328#FUTURE_USE_2#DESCR',101,'Accounting Flexfield : Segment ''Future Use 2'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#FUTURE_USE_2#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT7','GCC#Future Use 2 Descr','50328','1014948','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','GCC#50368#PRODUCT',101,'Accounting Flexfield : Segment ''Product'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GCC#50368#PRODUCT','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT1','GCC#Product','50368','1014596','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','GCC#50368#PRODUCT#DESCR',101,'Accounting Flexfield : Segment ''Product'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GCC#50368#PRODUCT#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT1','GCC#Product Descr','50368','1014596','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','GCC#50368#DIVISION',101,'Accounting Flexfield : Segment ''Division'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GCC#50368#DIVISION','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT2','GCC#Division','50368','1014597','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','GCC#50368#DIVISION#DESCR',101,'Accounting Flexfield : Segment ''Division'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GCC#50368#DIVISION#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT2','GCC#Division Descr','50368','1014597','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','GCC#50368#DEPARTMENT',101,'Accounting Flexfield : Segment ''Department'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GCC#50368#DEPARTMENT','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT3','GCC#Department','50368','1014598','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','GCC#50368#DEPARTMENT#DESCR',101,'Accounting Flexfield : Segment ''Department'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GCC#50368#DEPARTMENT#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT3','GCC#Department Descr','50368','1014598','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','GCC#50368#ACCOUNT',101,'Accounting Flexfield : Segment ''Account'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GCC#50368#ACCOUNT','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT4','GCC#Account','50368','1014599','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','GCC#50368#ACCOUNT#DESCR',101,'Accounting Flexfield : Segment ''Account'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GCC#50368#ACCOUNT#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT4','GCC#Account Descr','50368','1014599','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','GCC#50368#SUBACCOUNT',101,'Accounting Flexfield : Segment ''SubAccount'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GCC#50368#SUBACCOUNT','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT5','GCC#SubAccount','50368','1014600','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','GCC#50368#SUBACCOUNT#DESCR',101,'Accounting Flexfield : Segment ''SubAccount'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GCC#50368#SUBACCOUNT#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT5','GCC#SubAccount Descr','50368','1014600','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','GCC#50368#FUTURE_USE',101,'Accounting Flexfield : Segment ''Future Use'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GCC#50368#FUTURE_USE','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT6','GCC#Future Use','50368','1014601','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','GCC#50368#FUTURE_USE#DESCR',101,'Accounting Flexfield : Segment ''Future Use'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GCC#50368#FUTURE_USE#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT6','GCC#Future Use Descr','50368','1014601','');
xxeis.eis_rs_ins.vc( 'EIS_GL_SL_180_V','GCC#BRANCH',101,'Descriptive flexfield: GL Accounts Column Name: Branch','GCC#BRANCH','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','ATTRIBUTE1','Gcc#Branch','','','');
--Inserting View Components for EIS_GL_SL_180_V
xxeis.eis_rs_ins.vcomp( 'EIS_GL_SL_180_V','GL_CODE_COMBINATIONS_KFV',101,'GL_CODE_COMBINATIONS','GCC','GCC','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','GL Journal Line Account','','','','');
--Inserting View Component Joins for EIS_GL_SL_180_V
xxeis.eis_rs_ins.vcj( 'EIS_GL_SL_180_V','GL_CODE_COMBINATIONS_KFV','GCC',101,'EGS1V.CODE_COMBINATION_ID','=','GCC.CODE_COMBINATION_ID(+)','','','','','XXEIS_RS_ADMIN','','');
END;
/
set scan on define on
prompt Creating Report LOV Data for HDS-GL_AP_Drilldown_Account_Range
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - HDS-GL_AP_Drilldown_Account_Range
xxeis.eis_rs_ins.lov( 101,'select  per.period_name , led.name ledger_name, per.period_year, per.period_num, per.start_date, per.end_date
from    gl_periods per,gl_ledgers led
where  per.period_set_name = led.period_set_name
and     GL_SECURITY_PKG.VALIDATE_ACCESS(led.ledger_ID) = ''TRUE''
and     led.accounted_period_type = per.period_type','null','EIS_GL_PERIOD_LOV','Derives GL Periods based on the corresponding setups in the sets of books','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 101,'SELECT distinct user_je_source_name FROM gl_je_sources','','EIS_GL_JE_SOURCES_LOV','LOV of all Available Journal Sources','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( '','SELECT ffv.flex_value, ffvtl.description 
			FROM 
				fnd_flex_value_sets ffvs , 
				fnd_flex_values ffv, 
				fnd_flex_values_tl ffvtl 
			WHERE   upper(ffvs.flex_value_set_name) = upper(''XXCUS_GL_PRODUCT'') 
			 and ffv.flex_value_set_id = ffvs.flex_value_set_id 
			 and ffv.FLEX_VALUE_ID = ffvtl.FLEX_VALUE_ID 
			AND ffv.enabled_flag = upper(''Y'') 
			AND ffv.summary_flag <> upper(''Y'') 
			AND ffvtl.LANGUAGE = USERENV(''LANG'') 
			order by ffv.flex_value	','','XXCUS_GL_PRODUCT','XXCUS_GL_PRODUCT','MM050208',NULL,'','','');
xxeis.eis_rs_ins.lov( '','SELECT ffv.flex_value, ffvtl.description 
			FROM 
				fnd_flex_value_sets ffvs , 
				fnd_flex_values ffv, 
				fnd_flex_values_tl ffvtl 
			WHERE   upper(ffvs.flex_value_set_name) = upper(''XXCUS_GL_LOCATION'') 
			 and ffv.flex_value_set_id = ffvs.flex_value_set_id 
			 and ffv.FLEX_VALUE_ID = ffvtl.FLEX_VALUE_ID 
			AND ffv.enabled_flag = upper(''Y'') 
			AND ffv.summary_flag <> upper(''Y'') 
			AND ffvtl.LANGUAGE = USERENV(''LANG'') 
			order by ffv.flex_value	','','XXCUS_GL_LOCATION','XXCUS_GL_LOCATION','MM050208',NULL,'','','');
xxeis.eis_rs_ins.lov( '','SELECT ffv.flex_value, ffvtl.description 
			FROM 
				fnd_flex_value_sets ffvs , 
				fnd_flex_values ffv, 
				fnd_flex_values_tl ffvtl 
			WHERE   upper(ffvs.flex_value_set_name) = upper(''XXCUS_GL_COSTCENTER'') 
			 and ffv.flex_value_set_id = ffvs.flex_value_set_id 
			 and ffv.FLEX_VALUE_ID = ffvtl.FLEX_VALUE_ID 
			AND ffv.enabled_flag = upper(''Y'') 
			AND ffv.summary_flag <> upper(''Y'') 
			AND ffvtl.LANGUAGE = USERENV(''LANG'') 
			order by ffv.flex_value	','','XXCUS_GL_COSTCENTER','XXCUS_GL_COSTCENTER','MM050208',NULL,'','','');
xxeis.eis_rs_ins.lov( '','SELECT ffv.flex_value, ffvtl.description 
			FROM 
				fnd_flex_value_sets ffvs , 
				fnd_flex_values ffv, 
				fnd_flex_values_tl ffvtl 
			WHERE   upper(ffvs.flex_value_set_name) = upper(''XXCUS_GL_ACCOUNT'') 
			 and ffv.flex_value_set_id = ffvs.flex_value_set_id 
			 and ffv.FLEX_VALUE_ID = ffvtl.FLEX_VALUE_ID 
			AND ffv.enabled_flag = upper(''Y'') 
			AND ffv.summary_flag <> upper(''Y'') 
			AND ffvtl.LANGUAGE = USERENV(''LANG'') 
			order by ffv.flex_value	','','XXCUS_GL_ACCOUNT','XXCUS_GL_ACCOUNT','MM050208',NULL,'','','');
xxeis.eis_rs_ins.lov( '','SELECT ffv.flex_value, ffvtl.description 
			FROM 
				fnd_flex_value_sets ffvs , 
				fnd_flex_values ffv, 
				fnd_flex_values_tl ffvtl 
			WHERE   upper(ffvs.flex_value_set_name) = upper(''XXCUS_GL_ACCOUNT'') 
			 and ffv.flex_value_set_id = ffvs.flex_value_set_id 
			 and ffv.FLEX_VALUE_ID = ffvtl.FLEX_VALUE_ID 
			AND ffv.enabled_flag = upper(''Y'') 
			AND ffv.summary_flag <> upper(''Y'') 
			AND ffvtl.LANGUAGE = USERENV(''LANG'') 
			order by ffv.flex_value	','','XXCUS_GL_ACCOUNT','XXCUS_GL_ACCOUNT','MM050208',NULL,'','','');
xxeis.eis_rs_ins.lov( '','SELECT ffv.flex_value, ffvtl.description 
			FROM 
				fnd_flex_value_sets ffvs , 
				fnd_flex_values ffv, 
				fnd_flex_values_tl ffvtl 
			WHERE   upper(ffvs.flex_value_set_name) = upper(''XXCUS_GL_PROJECT'') 
			 and ffv.flex_value_set_id = ffvs.flex_value_set_id 
			 and ffv.FLEX_VALUE_ID = ffvtl.FLEX_VALUE_ID 
			AND ffv.enabled_flag = upper(''Y'') 
			AND ffv.summary_flag <> upper(''Y'') 
			AND ffvtl.LANGUAGE = USERENV(''LANG'') 
			order by ffv.flex_value	','','XXCUS_GL_PROJECT','XXCUS_GL_PROJECT','MM050208',NULL,'','','');
END;
/
set scan on define on
prompt Creating Report Data for HDS-GL_AP_Drilldown_Account_Range
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - HDS-GL_AP_Drilldown_Account_Range
xxeis.eis_rs_utility.delete_report_rows( 'HDS-GL_AP_Drilldown_Account_Range' );
--Inserting Report - HDS-GL_AP_Drilldown_Account_Range
xxeis.eis_rs_ins.r( 101,'HDS-GL_AP_Drilldown_Account_Range','','USD data only','','','','XXEIS_RS_ADMIN','EIS_GL_SL_180_V','Y','','','XXEIS_RS_ADMIN','','N','HDS Standard Reports','','CSV,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - HDS-GL_AP_Drilldown_Account_Range
xxeis.eis_rs_ins.rc( 'HDS-GL_AP_Drilldown_Account_Range',101,'JE_LINE_NUM','Je Line Num','Je Line Num','','~~~','default','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS-GL_AP_Drilldown_Account_Range',101,'ACCOUNTED_CR','Total Accounted Debit','Accounted Cr','','~T~D~2','default','','23','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS-GL_AP_Drilldown_Account_Range',101,'ACCOUNTED_DR','Total Accounted Credit','Accounted Dr','','~T~D~2','default','','24','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS-GL_AP_Drilldown_Account_Range',101,'ACC_DATE','Acc Date','Acc Date','','','default','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS-GL_AP_Drilldown_Account_Range',101,'BATCH_NAME','Batch Name','Batch Name','','','default','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS-GL_AP_Drilldown_Account_Range',101,'CURRENCY_CODE','Currency Code','Currency Code','','','default','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS-GL_AP_Drilldown_Account_Range',101,'CUSTOMER_OR_VENDOR','Vendor Name','Customer Or Vendor','','','default','','17','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS-GL_AP_Drilldown_Account_Range',101,'ENTRY','Description','Entry','','','default','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS-GL_AP_Drilldown_Account_Range',101,'GCC#50328#ACCOUNT#DESCR','Account Description','Accounting Flexfield : Segment ''Account'' under chart of accounts ''XXCUS Accounting Flexfield''','','','default','','14','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS-GL_AP_Drilldown_Account_Range',101,'GL_SL_LINK_ID','Subledger Link ID','Gl Sl Link Id','','~~~','default','','18','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS-GL_AP_Drilldown_Account_Range',101,'JE_CATEGORY','Je Category','Je Category','','','default','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS-GL_AP_Drilldown_Account_Range',101,'LINE_DESCR','Line Descr','Line Descr','','','default','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS-GL_AP_Drilldown_Account_Range',101,'PERIOD_NAME','Period Name','Period Name','','','default','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS-GL_AP_Drilldown_Account_Range',101,'SOURCE','Source','Source','','','default','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS-GL_AP_Drilldown_Account_Range',101,'TRANSACTION_NUM','Invoice Number','Transaction Num','','','default','','16','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS-GL_AP_Drilldown_Account_Range',101,'TYPE','Line Type','Type','','','default','','19','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS-GL_AP_Drilldown_Account_Range',101,'SLA_DIST_ACCOUNTED_CR','Subledger Line Credit','Sla Dist Accounted Cr','','~T~D~2','default','','21','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS-GL_AP_Drilldown_Account_Range',101,'SLA_DIST_ACCOUNTED_DR','Subledger Line Debit','Sla Dist Accounted Dr','','~T~D~2','default','','20','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS-GL_AP_Drilldown_Account_Range',101,'ACCOUNTED_NET','Subledger_Net','Sla Dist Accounted Dr','NUMBER','~T~D~2','default','','22','Y','','','','','','','SLA_DIST_ACCOUNTED_NET*-1','XXEIS_RS_ADMIN','N','N','','EIS_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS-GL_AP_Drilldown_Account_Range',101,'SEGMENT4','Account','Segment4','','','default','','13','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS-GL_AP_Drilldown_Account_Range',101,'SEGMENT1','Product','Segment1','','','default','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS-GL_AP_Drilldown_Account_Range',101,'SEGMENT2','Location','Segment2','','','default','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS-GL_AP_Drilldown_Account_Range',101,'SEGMENT3','Cost Center','Segment3','','','default','','12','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS-GL_AP_Drilldown_Account_Range',101,'SEGMENT5','Project Code','Segment5','','','default','','15','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_SL_180_V','','');
--Inserting Report Parameters - HDS-GL_AP_Drilldown_Account_Range
xxeis.eis_rs_ins.rp( 'HDS-GL_AP_Drilldown_Account_Range',101,'Source','Source','SOURCE','IN','EIS_GL_JE_SOURCES_LOV','','VARCHAR2','N','Y','2','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'HDS-GL_AP_Drilldown_Account_Range',101,'Product','Product','SEGMENT1','IN','XXCUS_GL_PRODUCT','','VARCHAR2','N','Y','3','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'HDS-GL_AP_Drilldown_Account_Range',101,'Location','Location','SEGMENT2','IN','XXCUS_GL_LOCATION','','VARCHAR2','N','Y','4','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'HDS-GL_AP_Drilldown_Account_Range',101,'Cost Center','Cost Center','SEGMENT3','IN','XXCUS_GL_COSTCENTER','','VARCHAR2','N','Y','5','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'HDS-GL_AP_Drilldown_Account_Range',101,'Account From','Account From','SEGMENT4','IN','XXCUS_GL_ACCOUNT','','VARCHAR2','N','Y','6','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'HDS-GL_AP_Drilldown_Account_Range',101,'Currency Code','Currency Code','CURRENCY_CODE','IN','','','VARCHAR2','N','Y','9','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'HDS-GL_AP_Drilldown_Account_Range',101,'Project Code','Project Code','SEGMENT5','IN','XXCUS_GL_PROJECT','','VARCHAR2','N','Y','8','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'HDS-GL_AP_Drilldown_Account_Range',101,'Account To','Account To','','IN','XXCUS_GL_ACCOUNT','','VARCHAR2','N','Y','7','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'HDS-GL_AP_Drilldown_Account_Range',101,'Period Name','Period Name','PERIOD_NAME','IN','EIS_GL_PERIOD_LOV','','VARCHAR2','Y','Y','1','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
--Inserting Report Conditions - HDS-GL_AP_Drilldown_Account_Range
xxeis.eis_rs_ins.rcn( 'HDS-GL_AP_Drilldown_Account_Range',101,'PERIOD_NAME','IN',':Period Name','','','Y','1','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'HDS-GL_AP_Drilldown_Account_Range',101,'SOURCE','IN',':Source','','','Y','2','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'HDS-GL_AP_Drilldown_Account_Range',101,'SEGMENT1','IN',':Product','','','Y','3','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'HDS-GL_AP_Drilldown_Account_Range',101,'SEGMENT2','IN',':Location','','','Y','4','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'HDS-GL_AP_Drilldown_Account_Range',101,'SEGMENT3','IN',':Cost Center','','','Y','5','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'HDS-GL_AP_Drilldown_Account_Range',101,'SEGMENT4','BETWEEN',':Account From',':Account To','','Y','6','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'HDS-GL_AP_Drilldown_Account_Range',101,'SEGMENT5','IN',':Project Code','','','Y','7','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'HDS-GL_AP_Drilldown_Account_Range',101,'CURRENCY_CODE','IN',':Currency Code','','','Y','8','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'HDS-GL_AP_Drilldown_Account_Range',101,'NAME','IN','''HD Supply USD''','','','Y','9','N','XXEIS_RS_ADMIN');
--Inserting Report Sorts - HDS-GL_AP_Drilldown_Account_Range
--Inserting Report Triggers - HDS-GL_AP_Drilldown_Account_Range
--Inserting Report Templates - HDS-GL_AP_Drilldown_Account_Range
--Inserting Report Portals - HDS-GL_AP_Drilldown_Account_Range
--Inserting Report Dashboards - HDS-GL_AP_Drilldown_Account_Range
--Inserting Report Security - HDS-GL_AP_Drilldown_Account_Range
xxeis.eis_rs_ins.rsec( 'HDS-GL_AP_Drilldown_Account_Range','','JB027320','',101,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'HDS-GL_AP_Drilldown_Account_Range','','JB045764','',101,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'HDS-GL_AP_Drilldown_Account_Range','','JB052172','',101,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'HDS-GL_AP_Drilldown_Account_Range','101','','50721',101,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'HDS-GL_AP_Drilldown_Account_Range','101','','50821',101,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'HDS-GL_AP_Drilldown_Account_Range','101','','50823',101,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'HDS-GL_AP_Drilldown_Account_Range','101','','50662',101,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'HDS-GL_AP_Drilldown_Account_Range','101','','50720',101,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'HDS-GL_AP_Drilldown_Account_Range','101','','50822',101,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'HDS-GL_AP_Drilldown_Account_Range','101','','50723',101,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'HDS-GL_AP_Drilldown_Account_Range','101','','50801',101,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'HDS-GL_AP_Drilldown_Account_Range','101','','50665',101,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'HDS-GL_AP_Drilldown_Account_Range','101','','50824',101,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'HDS-GL_AP_Drilldown_Account_Range','101','','50722',101,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'HDS-GL_AP_Drilldown_Account_Range','101','','50825',101,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'HDS-GL_AP_Drilldown_Account_Range','101','','50826',101,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'HDS-GL_AP_Drilldown_Account_Range','101','','50780',101,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'HDS-GL_AP_Drilldown_Account_Range','101','','50667',101,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'HDS-GL_AP_Drilldown_Account_Range','101','','50668',101,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'HDS-GL_AP_Drilldown_Account_Range','101','','50617',101,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'HDS-GL_AP_Drilldown_Account_Range','101','','50618',101,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'HDS-GL_AP_Drilldown_Account_Range','20005','','50900',101,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'HDS-GL_AP_Drilldown_Account_Range','101','','52033',101,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'HDS-GL_AP_Drilldown_Account_Range','101','','52032',101,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'HDS-GL_AP_Drilldown_Account_Range','101','','52031',101,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'HDS-GL_AP_Drilldown_Account_Range','101','','52030',101,'XXEIS_RS_ADMIN','','');
--Inserting Report Pivots - HDS-GL_AP_Drilldown_Account_Range
END;
/
set scan on define on
