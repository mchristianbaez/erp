CREATE OR REPLACE PACKAGE APPS.XXWC_PO_INTF_CORRECTIONS_PKG
IS
   /*****************************************************************************************************************************************
   -- File Name: XXWC_PO_INTF_CORRECTIONS_PKG.pks
   --
   -- PROGRAM TYPE: PL/SQL Script   <API>
   --
   -- PURPOSE: To track and update invalid buyer information in PO Requisition interface table error records.
   --
   -- HISTORY
   -- ========================================================================================================================================
   -- ========================================================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- --------------------------------------------------------------------------------------------------
   -- 1.0     25-AUG-2015   P.vamshidhar    TMS#20140317-00206  - Purch -Requisition Import enhancement - set default buyer
   --                                       Initial Version.

   ******************************************************************************************************************************************/

   PROCEDURE PO_SUGGESTED_BUYER_UPD_PRC (x_errbuf         OUT VARCHAR2,
                                         x_retcode        OUT VARCHAR2,
                                         p_from_date   IN     VARCHAR2);


   PROCEDURE ITEM_BUYER_UPDATE_PRC (p_item_id          IN     NUMBER,
                                    p_orgaization_id   IN     NUMBER,
                                    x_process_status      OUT VARCHAR2);
                                
END XXWC_PO_INTF_CORRECTIONS_PKG;
/
