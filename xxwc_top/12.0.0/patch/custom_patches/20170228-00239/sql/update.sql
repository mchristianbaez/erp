begin
update taxware.config_param set PARAM_VALUE='Y' where PARAM_NAME='USE_TX_EXCPRULE';
dbms_output.put_line('Sucees row updated: '||sql%rowcount);
commit;
exception
  when others then
     dbms_output.put_line('Error: '||sqlerrm);
end;
/