/*******************************************************************
 -- script name: TMS_20180426_00331_DATA_FIX_MONTH_END_TRIP_STOP.sql
 -- purpose: Data fix to resolve month end issue of trip stop
 -- revision history:
 -- Version   Date          Aurthor           Description
 -- ----------------------------------------------------------------
 -- 1.0       27-APR-2018   Niraj K Ranjan    TMS#20180426-00331   Month End Interface trip stop Warning Delivery
*******************************************************************/
SET serveroutput on size 1000000;
BEGIN
   dbms_output.put_line('TMS_20180426-00331 : '||'Start script');
   
   update wsh_delivery_details
   set oe_interfaced_flag = 'Y',
       last_update_date = sysdate,
       last_updated_by = -1
   where delivery_detail_id = 28503095;

   dbms_output.put_line('TMS_20180426-00331 : '||'Total records updated for delivery_detail_id 28503095: '||SQL%ROWCOUNT);
   
   update oe_order_lines_all
   set ordered_quantity = 180,
       shipping_quantity = 180,
       shipped_quantity = 180,
       fulfilled_quantity = 180,
       last_update_date = sysdate,
       last_updated_by = -1
   where line_id=   122196356;
   dbms_output.put_line('TMS_20180426-00331 : '||'Total records updated for line_id 122196356 : '||SQL%ROWCOUNT);
   COMMIT;
   dbms_output.put_line('TMS_20180426-00331: '||'End script');
EXCEPTION
   WHEN OTHERS THEN
      dbms_output.put_line('TMS_20180426-00331 : '||'ERROR - '||SQLERRM);
	  ROLLBACK;
END;
/
