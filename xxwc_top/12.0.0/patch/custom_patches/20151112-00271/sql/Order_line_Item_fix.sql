   /*************************************************************************************************************************************************************
   -- File Name: Order_line_Item_fix.sql
   --
   -- PROGRAM TYPE: SQL Script
   --
   -- ===========================================================================================================================================================
   -- ===========================================================================================================================================================
   -- VERSION    DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- ---------------------------------------------------------------------------------------------------------------------
   -- 1.0     12-Nov-2015   P.Vamshidhar    TMS#20151112-00271

   ***************************************************************************************************************************************************************/
UPDATE OE_ORDER_LINES_ALL l
   SET l.Ordered_item =
          (SELECT ITM.Segment1
             FROM apps.mtl_system_items_B itm
            WHERE     l.inventory_item_id = itm.inventory_item_id
                  AND l.Ship_from_org_id = itm.Organization_id)
 WHERE     TRUNC (l.creation_date) IN ('08-NOV-2015', '09-NOV-2015')
       AND l.Item_identifier_type = 'INT'
       AND l.Ordered_item IS NULL
       AND l.header_id IN (SELECT HEADER_ID
                             FROM apps.oe_order_lines_all
                            WHERE     ordered_item IS NULL
                                  AND creation_date > SYSDATE - 5);
/
commit;
/
