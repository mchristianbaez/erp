 /********************************************************************************
  FILE NAME: XXWC.XXWC_AP_BRANCH_LIST_STG_TBL.sql

  PROGRAM TYPE: Table script

  PURPOSE: Conversion purpose for AH Harries

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)          DESCRIPTION
  ------- -----------   ---------------    --------------------------------------
  1.0     05/016/2018   Pattabhi Avula     TMS#20180308-00301   -- Initial Version
  2.0     07/13/2018    Naveen K  			TMS#20180709-00105  -- Re-Load
  ********************************************************************************/  
  CREATE TABLE XXWC.XXWC_AP_BRANCH_LIST_STG_TBL 
   (AHH_BRANCH_NUMBER 		VARCHAR2(50), 
	AHH_WAREHOUSE_NUMBER 	VARCHAR2(50), 
	ORACLE_BRANCH_NUMBER 	VARCHAR2(50), 
	ORACLE_BW_NUMBER 		VARCHAR2(50), 
	PROCESS_STATUS   		VARCHAR2(1), 
	CREATION_DATE 			DATE, 
	CREATED_BY 				NUMBER, 
	LAST_UPDATED_BY 		NUMBER, 
	LAST_UPDATE_DATE 		DATE, 
	LAST_UPDATE_LOGIN 		NUMBER, 
	ERROR_MESSAGE 			VARCHAR2(2000), 
	BATCH_ID 				NUMBER
   );