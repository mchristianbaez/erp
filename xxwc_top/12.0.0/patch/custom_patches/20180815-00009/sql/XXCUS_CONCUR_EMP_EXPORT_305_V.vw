set serveroutput on size unlimited;
set linesize 32767;
--//============================================================================
--//
--// Object Name         :: XXCUS_CONCUR_EMP_EXPORT_305_V
--//
--// Object Type         :: View
--//
--// Object Description  :: This view shows the Employee Details in the HRMS Application.
--//
--// Version Control
--//============================================================================
--// Vers    Author             Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Ashwin Sridhar     08/06/2018    Initial Build - Task ID: 20180227-00179--Adding Contractors to Concur Employee Feed
--// 1.1     Ashwin Sridhar     20/08/2018    TMS#20180815-00009-AHH Associate turn Expense On in Concur
--//============================================================================
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "APPS"."XXCUS_CONCUR_EMP_EXPORT_305_V" ("TRX_TYPE", "FIRST_NAME", "MIDDLE_NAME", "LAST_NAME", "EMPLOYEE_ID", "LOGIN_ID", "PASSWORD", "EMAIL_ADDRESS", "LOCLE_CODE", "CTRY_CODE", "CTRY_SUB_CODE", "LEDGER_CODE", "REIMBURSEMENT_CRN_CODE", "CASH_ADVANCE_ACCOUNT_CODE", "ACTIVE_Y_N", "ORG_UNIT_1_ERP_SYSTEM", "ORG_UNIT_2_COMPANY", "ORG_UNIT_3_BRANCH", "ORG_UNIT_4_COST_CENTER", "ORG_UNIT_5_NOT_USED", "ORG_UNIT_6_PROJECT", "CUSTOM_1_ERP_SYSTEM", "CUSTOM_2_COMPANY", "CUSTOM_3_BRANCH", "CUSTOM_4_COST_CENTER", "CUSTOM_5_NO_LONGER_BEING_USED", "CUSTOM_6_PROJECT", "CUSTOM_7", "CUSTOM_8", "CUSTOM_9", "CUSTOM_10", "CUSTOM_11", "CUSTOM_12", "CUSTOM_13", "CUSTOM_14", "CUSTOM_15", "CUSTOM_16", "CUSTOM_17", "CUSTOM_18", "CUSTOM_19", "CUSTOM_20"
  ,"CUSTOM_21_CONCUR_EXP_GRP_HIER", "EMAIL_CASH_ADV_STAT_CHANGE", "EMAIL_CASH_ADV_AWT_APRV", "EMAIL_RPT_STATUS_CHANGE", "EMAIL_RPT_AWT_APRV", "PROMPT_FOR_APRV_ON_SUBMIT_RPT", "EMAIL_TRAVEL_REQ_STAT_CHANGE", "EMAIL_TRAVEL_REQ_AWT_APRV", "PMRPT_FOR_APRV_SUB_TRAVEL_REQ", "EMAIL_WHEN_PMT_STATUS_CHANGES", "EMAIL_PMT_AWAITING_APPROVAL", "PRMPT_FOR_APRV_SUB_PMT", "PRMPT_ADD_CC_TRANS_TO_RPT", "EMAIL_WHEN_NEW_CC_TRANS_ARRIVE", "EMAIL_WHEN_FAX_RCPTS_ARRIVE", "SHOW_HELP_ON_APP_PAGES", "SHOW_IMAGING_INTRO_PAGE", "EMP_ID_OF_EXPENSE_RPT_APPROVER", "EMP_ID_OF_CASH_ADV_APPROVER", "EMP_ID_OF_REQUEST_APPROVER", "EMP_ID_OF_INVOICE_APPROVER", "EXPENSE_USER_Y_N", "EXP_ANDOR_CASH_ADV_APRVR_Y_N", "COMPANY_CARD_ADMIN", "FUTURE_USE", "RECEIPT_PROCESSOR", "FUTURE_USE_1_1"
  ,"IMPORT_EXTRACT_MONITOR", "COMPANY_INFO_ADMIN", "OFFLINE_USER", "REPORTING_CONFIG_ADMIN", "INVOICE_USER", "INVOICE_APPROVER", "INVOICE_VENDOR_MANAGER", "EXPENSE_AUDIT_REQUIRED", "BI_MANAGER_EMPLOYEE_ID", "REQUEST_USER_Y_N", "REQUEST_APPROVER_Y_N", "EXPENSE_RPT_APPROVER_EMP_ID_2", "PAYMENT_REQUEST_ASSIGNED", "FUTURE_USE_1", "FUTURE_USE_2", "TAX_ADMINISTRATOR", "FBT_ADMINISTRATOR", "TRAVEL_WIZARD_USER", "CUST_22_CONCUR_INVC_GROUP_HIER", "REQUEST_APPROVER_EMPLOYEE_ID_2", "IS_NON_EMPLOYEE_Y_N", "REIMBURSEMENT_TYPE", "ADP_EMPLOYEE_ID", "ADP_COMPANY_CODE", "ADP_DEDUCTION_CODE", "FUTURE_USE_7", "FUTURE_USE_8", "FUTURE_USE_9", "FUTURE_USE_10", "FUTURE_USE_11", "FUTURE_USE_12", "FUTURE_USE_13", "FUTURE_USE_14", "FUTURE_USE_15", "FUTURE_USE_16", "FUTURE_USE_17", "FUTURE_USE_18", "FUTURE_USE_19", "FUTURE_USE_20", "FUTURE_USE_21", "FUTURE_USE_22", "FUTURE_USE_23", "FUTURE_USE_24", "FUTURE_USE_25", "FUTURE_USE_26", "FUTURE_USE_27", "FUTURE_USE_28", "FUTURE_USE_29", "FUTURE_USE_30", "FUTURE_USE_31", "FUTURE_USE_32", "FUTURE_USE_33", "FUTURE_USE_34"
  ,"FUTURE_USE_35", "FUTURE_USE_36", "FUTURE_USE_37", "FUTURE_USE_38", "FUTURE_USE_39", "FUTURE_USE_40", "FUTURE_USE_41", "FUTURE_USE_42"
  ,"FUTURE_USE_43", "FUTURE_USE_44", "FUTURE_USE_45", "FUTURE_USE_46", "FUTURE_USE_47", "FUTURE_USE_48", "FUTURE_USE_49", "FUTURE_USE_50") AS 
  SELECT '305' AS trx_type, 
                emp.first_name,
                emp.middle_name AS middle_name,
                emp.last_name AS last_name,
                emp.emplid AS employee_id,
                REPLACE( email_addr, '''' ) AS login_id,
                NULL AS password,
                email_addr AS email_address,
                control_table.locle_code AS locle_code,
                control_table.cntry_code AS ctry_code,
                NULL AS ctry_sub_code,
                control_table.ledger_code AS ledger_code,
                control_table.currency_code AS reimbursement_crn_code,
                NULL AS cash_advance_account_code,
                CASE
                   WHEN ( termination_dt >= SYSDATE - 90 and emp.business_unit <> 'WW1US')
                     OR hr_status = 'A'
                   THEN
                      'Y'
                   ELSE
                      'N'
                END
                   AS active_y_n,
                /* These 4 columns are for US based businesses only */
                CASE WHEN control_table.cntry_code = 'US' THEN control_table.erp_name ELSE NULL END AS org_unit_1_erp_system,
                CASE
                   WHEN control_table.cntry_code = 'US' THEN DECODE( SUBSTR( et.entrp_entity, 2, 2 ), '32', '01', SUBSTR( et.entrp_entity, 2, 2 ) )
                   ELSE NULL
                END
                   AS org_unit_2_company,
                CASE
                   WHEN ( control_table.cntry_code = 'US' )
                   THEN
                      CASE WHEN emp.business_unit = 'FM1US' THEN '1000' WHEN emp.business_unit IN ( 'GC1US', 'GC2US', 'WC1US' ) THEN et.entrp_loc /* For WW, trim leading zeros from branch number */
                      WHEN emp.business_unit = 'WW1US' THEN LTRIM( et.lob_branch, '0' ) ELSE et.lob_branch END
                   ELSE
                      NULL
                END
                   AS org_unit_3_branch,
                CASE
                   WHEN ( control_table.cntry_code = 'US' )
                   THEN
                      CASE
                         WHEN emp.business_unit = 'FM1US' THEN '100' || SUBSTR( emp.native_dept, 0, 3 )
                         WHEN emp.business_unit IN ( 'GC1US', 'GC2US', 'WC1US' ) THEN '0000'
                         ELSE emp.native_dept
                      END
                   ELSE
                      NULL
                END
                   AS org_unit_4_cost_center,
                NULL AS org_unit_5_not_used,
                CASE WHEN control_table.cntry_code = 'US' THEN '00000' ELSE NULL END AS org_unit_6_project,
                /* End of US Only */
                /* These 6 columns are Canada businesses only */
                CASE WHEN control_table.cntry_code = 'CA' THEN control_table.erp_name ELSE NULL END AS custom_1_erp_system,
                CASE WHEN control_table.cntry_code = 'CA' THEN SUBSTR( et.entrp_entity, 2, 2 ) ELSE NULL END AS custom_2_company,
                CASE WHEN ( control_table.cntry_code = 'CA' ) THEN et.lob_branch ELSE NULL END AS custom_3_branch,
                CASE
                   WHEN ( control_table.cntry_code = 'CA' ) THEN CASE WHEN emp.business_unit IN ( 'BR1CA' ) THEN '0000' ELSE emp.native_dept END
                   ELSE NULL
                END
                   AS custom_4_cost_center,
                NULL AS custom_5_no_longer_being_used,
                CASE WHEN control_table.cntry_code = 'CA' THEN '00000' ELSE NULL END AS custom_6_project,
                /* End of Canada Only */
                NULL AS custom_7,
                NULL AS custom_8,
                NULL AS custom_9,
                NULL AS custom_10,
                NULL AS custom_11,
                NULL AS custom_12,
                NULL AS custom_13,
                NULL AS custom_14,
                NULL AS custom_15,
                NULL AS custom_16,
                NULL AS custom_17,
                NULL AS custom_18,
                NULL AS custom_19,
                NULL AS custom_20,
                control_table.erp_name AS custom_21_concur_exp_grp_hier,
                NULL AS email_cash_adv_stat_change,
                NULL AS email_cash_adv_awt_aprv,
                NULL AS email_rpt_status_change,
                NULL AS email_rpt_awt_aprv,
                'Y' AS prompt_for_aprv_on_submit_rpt,
                NULL AS email_travel_req_stat_change,
                NULL AS email_travel_req_awt_aprv,
                NULL AS pmrpt_for_aprv_sub_travel_req,
                NULL AS email_when_pmt_status_changes,
                NULL AS email_pmt_awaiting_approval,
                NULL AS prmpt_for_aprv_sub_pmt,
                NULL AS prmpt_add_cc_trans_to_rpt,
                NULL AS email_when_new_cc_trans_arrive,
                NULL AS email_when_fax_rcpts_arrive,
                NULL AS show_help_on_app_pages,
                NULL AS show_imaging_intro_page,
                CASE
                   WHEN empo.supervisor_id = 'NONE' THEN NULL
                   WHEN empo.expense_access_flg = 'Y' THEN COALESCE(empo.supervisor_id, emp.supervisor_id) /* Individual user overrides for enabling expense */
                   WHEN empo.expense_access_flg = 'N' THEN NULL /* Individual user overrides for disabling Expense */
                   WHEN fruo.fru IS NOT NULL THEN COALESCE(empo.supervisor_id, emp.supervisor_id) /*Overrides for FRUs with expense access before the rest of GSC frus */ 
                   WHEN control_table.exp_active = 'Y' THEN  COALESCE(empo.supervisor_id, emp.supervisor_id) /*Any employees at BUs who have expense access enabled */
                   WHEN control_table.exp_active = 'N' then  coalesce(empo.supervisor_id, emp.supervisor_id) /*Any employees at BUs who have expense access disabled */                   
                   WHEN emp.emplid IN ( SELECT supervisor_id
                                          FROM hdscmmn.hr_employee_all_vw@eaapxprd.hsi.hughessupply.com
                                         WHERE business_unit IN ( SELECT DISTINCT business_unit
                                                                    FROM hdscmmn.concur_bu_xref@eaapxprd.hsi.hughessupply.com
                                                                   WHERE exp_active = 'Y' )
                                           AND ( hr_status = 'A'
                                             OR termination_dt >= SYSDATE - 90 ) )
                   THEN
                    COALESCE(empo.supervisor_id, emp.supervisor_id) /*Any supervisors of employees who have expense access (for supervisor in other BUs, like executives reporting to Corp). */
                   ELSE
                      NULL
                END
                   AS emp_id_of_expense_rpt_approver,
                NULL emp_id_of_cash_adv_approver,
                NULL AS emp_id_of_request_approver,
                NULL AS emp_id_of_invoice_approver,
                CASE
                   WHEN control_table.erp_name= 'GSC ORACLE' THEN 'Y' --v1.1 Added by Ashwin.S for TMS#20180815-00009 on 22-Aug-2018
                   WHEN empo.expense_access_flg IS NOT NULL THEN empo.expense_access_flg
                   WHEN fruo.fru IS NOT NULL THEN 'Y' 
                   WHEN control_table.exp_active = 'Y' THEN 'Y'
                   WHEN emp.emplid IN ( SELECT supervisor_id
                                          FROM hdscmmn.hr_employee_all_vw@eaapxprd.hsi.hughessupply.com
                                         WHERE business_unit IN ( SELECT DISTINCT business_unit
                                                                    FROM hdscmmn.concur_bu_xref@eaapxprd.hsi.hughessupply.com
                                                                   WHERE exp_active = 'Y' )
                                           AND ( hr_status = 'A'
                                             OR termination_dt >= SYSDATE - 90 ) )
                   THEN
                      'Y'
                   ELSE
                      'N'
                END
                   AS expense_user_y_n,
                CASE
                  WHEN  empo.expense_approver_flg IS NOT NULL THEN empo.expense_approver_flg --Hard code expense approver flag for certain admins who do not have direct reports, but approve reports on behalf of others
                   WHEN emp.emplid IN ( SELECT supervisor_id
                                          FROM hdscmmn.hr_employee_all_vw@eaapxprd.hsi.hughessupply.com
                                         WHERE (business_unit IN  (SELECT DISTINCT business_unit FROM hdscmmn.concur_bu_xref@eaapxprd.hsi.hughessupply.com
                                                                                     WHERE exp_active = 'Y' )
                                                                   OR emp.fru IN ( SELECT distinct fru from hdscmmn.concur_fru_override@eaapxprd.hsi.hughessupply.com) ) 
                                           AND ( hr_status = 'A'
                                             OR termination_dt >= SYSDATE - 90 ) )
                   THEN
                      'Y'
                   ELSE
                      'N'
                END
                   AS exp_andor_cash_adv_aprvr_y_n,
                NULL AS company_card_admin,
                NULL AS future_use,
                NULL AS receipt_processor,
                NULL AS future_use_1_1,
                NULL AS import_extract_monitor,
                NULL AS company_info_admin,
                NULL AS offline_user,
                NULL AS reporting_config_admin,
                NULL AS invoice_user,
                NULL AS invoice_approver,
                NULL AS invoice_vendor_manager,
                NULL AS expense_audit_required,
                NULL AS bi_manager_employee_id,
                NULL AS request_user_y_n,
                NULL AS request_approver_y_n,
                NULL AS expense_rpt_approver_emp_id_2,
                NULL AS payment_request_assigned,
                NULL AS future_use_1,
                NULL AS future_use_2,
                NULL AS tax_administrator,
                NULL AS fbt_administrator,
                'Y' travel_wizard_user,
                NULL AS cust_22_concur_invc_group_hier,
                NULL AS request_approver_employee_id_2,
                NULL AS is_non_employee_y_n,
                NULL AS reimbursement_type,
                NULL AS adp_employee_id,
                NULL AS adp_company_code,
                NULL AS adp_deduction_code,
                NULL AS future_use_7,
                NULL AS future_use_8,
                NULL AS future_use_9,
                NULL AS future_use_10,
                NULL AS future_use_11,
                NULL AS future_use_12,
                NULL AS future_use_13,
                NULL AS future_use_14,
                NULL AS future_use_15,
                NULL AS future_use_16,
                NULL AS future_use_17,
                NULL AS future_use_18,
                NULL AS future_use_19,
                NULL AS future_use_20,
                NULL AS future_use_21,
                NULL AS future_use_22,
                NULL AS future_use_23,
                NULL AS future_use_24,
                NULL AS future_use_25,
                NULL AS future_use_26,
                NULL AS future_use_27,
                NULL AS future_use_28,
                NULL AS future_use_29,
                NULL AS future_use_30,
                NULL AS future_use_31,
                NULL AS future_use_32,
                NULL AS future_use_33,
                NULL AS future_use_34,
                NULL AS future_use_35,
                NULL AS future_use_36,
                NULL AS future_use_37,
                NULL AS future_use_38,
                NULL AS future_use_39,
                NULL AS future_use_40,
                NULL AS future_use_41,
                NULL AS future_use_42,
                NULL AS future_use_43,
                NULL AS future_use_44,
                NULL AS future_use_45,
                NULL AS future_use_46,
                NULL AS future_use_47,
                NULL AS future_use_48,
                NULL AS future_use_49,
                NULL AS future_use_50 
           FROM hdscmmn.hr_employee_all_vw@eaapxprd.hsi.hughessupply.com emp
                LEFT OUTER JOIN hdscmmn.et_location_code@eaapxprd.hsi.hughessupply.com et ON emp.fru = et.fru
                LEFT OUTER JOIN hdscmmn.concur_bu_xref@eaapxprd.hsi.hughessupply.com control_table ON control_table.business_unit = emp.business_unit
                LEFT OUTER JOIN hdscmmn.concur_fru_override@eaapxprd.hsi.hughessupply.com fruo ON emp.fru = fruo.fru
                LEFT OUTER JOIN hdscmmn.concur_emp_override@eaapxprd.hsi.hughessupply.com empo ON emp.emplid = empo.emplid
          WHERE 1 =1   
          AND ( hr_status = 'A' OR termination_dt >=  trunc(sysdate) - 120 )
            AND (control_table.business_unit IS NOT NULL  OR fruo.fru IS NOT NULL or empo.emplid IS NOT NULL)
            AND et.entrp_entity <> 'PPH'
            AND CONTROL_TABLE.ERP_NAME!='WW MINCRON' --v1.1 Added by Ashwin.S for TMS#20180815-00009 on 22-Aug-2018
            AND emp.HIRE_DT <=SYSDATE                --v1.1 Added by Ashwin.S for TMS#20180815-00009 on 23-Aug-2018
--v1.1 Added the below UNION query by Ashwin.S for TMS#20180815-00009 on 20-Aug-2018
/*v 1.1 Change Started*/
UNION ALL
SELECT '305' AS trx_type, 
                emp.first_name,
                emp.MIDDLE_NAME AS middle_name,
                emp.last_name AS last_name,
                to_char(to_number(regexp_replace(emp.ntid, '[^0-9]+' ))) AS employee_id,
                REPLACE( emp.EMAIL_ADDR, '''' ) AS login_id,
                NULL AS password,
                emp.EMAIL_ADDR AS email_address,
                control_table.locle_code AS locle_code,
                control_table.cntry_code AS ctry_code,
                NULL AS ctry_sub_code,
                control_table.ledger_code AS ledger_code,
                control_table.currency_code AS reimbursement_crn_code,
                NULL AS cash_advance_account_code,
                'Y' AS active_y_n,
                /* These 4 columns are for US based businesses only */
                CASE WHEN control_table.cntry_code = 'US' THEN control_table.erp_name ELSE NULL END AS org_unit_1_erp_system,
                CASE
                   WHEN control_table.cntry_code = 'US' THEN DECODE( SUBSTR( et.entrp_entity, 2, 2 ), '32', '01', SUBSTR( et.entrp_entity, 2, 2 ) )
                   ELSE NULL
                END
                   AS org_unit_2_company,
                CASE
                   WHEN ( control_table.cntry_code = 'US' )
                   THEN
                      CASE WHEN emp.PS_BUSINESS_UNIT = 'FM1US' THEN '1000' WHEN emp.PS_BUSINESS_UNIT IN ( 'GC1US', 'GC2US', 'WC1US' ) THEN et.entrp_loc
                      WHEN emp.PS_BUSINESS_UNIT = 'WW1US' THEN LTRIM( et.lob_branch, '0' ) ELSE et.lob_branch END
                   ELSE
                      NULL
                END
                   AS org_unit_3_branch,
                CASE
                   WHEN ( control_table.cntry_code = 'US' )
                   THEN
                      CASE
                         WHEN emp.PS_BUSINESS_UNIT = 'FM1US' THEN '100' || SUBSTR( emp.DEPTID, 0, 3 )
                         WHEN emp.PS_BUSINESS_UNIT IN ( 'GC1US', 'GC2US', 'WC1US' ) THEN '0000'
                         ELSE emp.DEPTID
                      END
                   ELSE
                      NULL
                END
                   AS org_unit_4_cost_center,
                NULL AS org_unit_5_not_used,
                CASE WHEN control_table.cntry_code = 'US' THEN '00000' ELSE NULL END AS org_unit_6_project,
                /* End of US Only */
                /* These 6 columns are Canada businesses only */
                CASE WHEN control_table.cntry_code = 'CA' THEN control_table.erp_name ELSE NULL END AS custom_1_erp_system,
                CASE WHEN control_table.cntry_code = 'CA' THEN SUBSTR( et.entrp_entity, 2, 2 ) ELSE NULL END AS custom_2_company,
                CASE WHEN ( control_table.cntry_code = 'CA' ) THEN et.lob_branch ELSE NULL END AS custom_3_branch,
                CASE
                   WHEN ( control_table.cntry_code = 'CA' ) THEN CASE WHEN emp.PS_BUSINESS_UNIT IN ( 'BR1CA' ) THEN '0000' ELSE emp.DEPTID END
                   ELSE NULL
                END
                   AS custom_4_cost_center,
                NULL AS custom_5_no_longer_being_used,
                CASE WHEN control_table.cntry_code = 'CA' THEN '00000' ELSE NULL END AS custom_6_project,
                /* End of Canada Only */
                NULL AS custom_7,
                NULL AS custom_8,
                NULL AS custom_9,
                NULL AS custom_10,
                NULL AS custom_11,
                NULL AS custom_12,
                NULL AS custom_13,
                NULL AS custom_14,
                NULL AS custom_15,
                NULL AS custom_16,
                NULL AS custom_17,
                NULL AS custom_18,
                NULL AS custom_19,
                NULL AS custom_20,
                control_table.erp_name AS custom_21_concur_exp_grp_hier,
                NULL AS email_cash_adv_stat_change,
                NULL AS email_cash_adv_awt_aprv,
                NULL AS email_rpt_status_change,
                NULL AS email_rpt_awt_aprv,
                'Y' AS prompt_for_aprv_on_submit_rpt,
                NULL AS email_travel_req_stat_change,
                NULL AS email_travel_req_awt_aprv,
                NULL AS pmrpt_for_aprv_sub_travel_req,
                NULL AS email_when_pmt_status_changes,
                NULL AS email_pmt_awaiting_approval,
                NULL AS prmpt_for_aprv_sub_pmt,
                NULL AS prmpt_add_cc_trans_to_rpt,
                NULL AS email_when_new_cc_trans_arrive,
                NULL AS email_when_fax_rcpts_arrive,
                NULL AS show_help_on_app_pages,
                NULL AS show_imaging_intro_page,
                CASE
                   WHEN emp.SPVR_EMPLID IS NOT NULL THEN emp.SPVR_EMPLID
                   ELSE
                      NULL
                END
                   AS emp_id_of_expense_rpt_approver,
                NULL emp_id_of_cash_adv_approver,
                NULL AS emp_id_of_request_approver,
                NULL AS emp_id_of_invoice_approver,
                CASE
                   WHEN emp.fru IS NOT NULL THEN 'Y' 
                   WHEN control_table.exp_active = 'Y' THEN 'Y'
                   ELSE
                      'N'
                END
                   AS expense_user_y_n,
                'N'
                   AS exp_andor_cash_adv_aprvr_y_n,
                NULL AS company_card_admin,
                NULL AS future_use,
                NULL AS receipt_processor,
                NULL AS future_use_1_1,
                NULL AS import_extract_monitor,
                NULL AS company_info_admin,
                NULL AS offline_user,
                NULL AS reporting_config_admin,
                NULL AS invoice_user,
                NULL AS invoice_approver,
                NULL AS invoice_vendor_manager,
                NULL AS expense_audit_required,
                NULL AS bi_manager_employee_id,
                NULL AS request_user_y_n,
                NULL AS request_approver_y_n,
                NULL AS expense_rpt_approver_emp_id_2,
                NULL AS payment_request_assigned,
                NULL AS future_use_1,
                NULL AS future_use_2,
                NULL AS tax_administrator,
                NULL AS fbt_administrator,
                'Y' travel_wizard_user,
                NULL AS cust_22_concur_invc_group_hier,
                NULL AS request_approver_employee_id_2,
                NULL AS is_non_employee_y_n,
                NULL AS reimbursement_type,
                NULL AS adp_employee_id,
                NULL AS adp_company_code,
                NULL AS adp_deduction_code,
                NULL AS future_use_7,
                NULL AS future_use_8,
                NULL AS future_use_9,
                NULL AS future_use_10,
                NULL AS future_use_11,
                NULL AS future_use_12,
                NULL AS future_use_13,
                NULL AS future_use_14,
                NULL AS future_use_15,
                NULL AS future_use_16,
                NULL AS future_use_17,
                NULL AS future_use_18,
                NULL AS future_use_19,
                NULL AS future_use_20,
                NULL AS future_use_21,
                NULL AS future_use_22,
                NULL AS future_use_23,
                NULL AS future_use_24,
                NULL AS future_use_25,
                NULL AS future_use_26,
                NULL AS future_use_27,
                NULL AS future_use_28,
                NULL AS future_use_29,
                NULL AS future_use_30,
                NULL AS future_use_31,
                NULL AS future_use_32,
                NULL AS future_use_33,
                NULL AS future_use_34,
                NULL AS future_use_35,
                NULL AS future_use_36,
                NULL AS future_use_37,
                NULL AS future_use_38,
                NULL AS future_use_39,
                NULL AS future_use_40,
                NULL AS future_use_41,
                NULL AS future_use_42,
                NULL AS future_use_43,
                NULL AS future_use_44,
                NULL AS future_use_45,
                NULL AS future_use_46,
                NULL AS future_use_47,
                NULL AS future_use_48,
                NULL AS future_use_49,
                NULL AS future_use_50 
FROM       hdscmmn.hr_ad_all_vw@eaapxprd.hsi.hughessupply.com       emp 
,          hdscmmn.concur_bu_xref@eaapxprd.hsi.hughessupply.com     control_table
,          hdscmmn.et_location_code@eaapxprd.hsi.hughessupply.com   et
WHERE 1 =1
AND USERTYPE ='Contractor'
AND EMAIL_ADDR IS NOT NULL
AND TO_NUMBER(REGEXP_REPLACE(emp.NTID, '[^0-9]+' )) is not null
AND TO_NUMBER(REGEXP_REPLACE(emp.SPVR_NTID, '[^0-9]+' )) is not null
AND (TO_NUMBER(REGEXP_REPLACE(emp.SPVR_NTID, '[^0-9]+' )) is not null and TO_NUMBER(REGEXP_REPLACE(emp.SPVR_EMPLID, '[^0-9]+' )) is not null ) 
AND control_table.business_unit(+) =emp.ps_business_unit
AND emp.fru(+) = et.fru
AND TO_NUMBER(regexp_replace(emp.NTID, '[^0-9]+' )) IN ('69273','69191','69181','69577','69232','69423','69281','69334','69489','69280'
,'69759','69197','69243','69341','69296','69455','69620','69347','69300','69274','69522','69743','69250','69357'
,'69702','69782','69665','69414','69787','69555','69237','69547','69386','69611','69424','69478','69323','69477'
,'69311','69310','69342','69270','69687','69470','69691','69189','69229','69302','69319','69351','69435','69480'
,'69585','69592','70283','70360','70703'); 
/*v 1.1 Change Ended*/
COMMENT ON TABLE "APPS"."XXCUS_CONCUR_EMP_EXPORT_305_V"  IS 'TMS: 20180227-00179';
