/*************************************************************************
  $Header TMS_20181025-00001_PQC_LOAD_TESTING_FOR_PACKSLIP.sql $
  Module Name: TMS_20181025-00001_PQC_PACKING_LOAD_TESTING

  PURPOSE: Load testing -- TMS#20181025-00001 - PQC Load Testing for Packing slilp

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        25-OCT-2018  Pattabhi Avula         TMS#20181025-00001

**************************************************************************/ 
DECLARE
   CURSOR cur_sgn
   IS
      SELECT *
        FROM xxwc.xxwc_print_log_tbl xpl
       WHERE     1 = 1          --concurrent_program_name = 'XXWC_OM_SRECEIPT'
             AND user_concurrent_program_name = 'XXWC OM Packing Slip' --'XXWC OM Pick Slip'
             AND EXISTS
                    (SELECT 1
                       FROM oe_order_headers_all a, oe_order_lines_all b
                      WHERE     a.header_id = b.header_id
                            AND a.order_number =27503469 --28295095 -- IN (29086793,29086792,29086835,29086836,29086837,29086838) --= '29168602' --enter order number here
                            AND a.header_id = xpl.argument2
                            AND b.ship_from_org_id = xpl.argument1
                            AND b.flow_status_code = 'AWAITING_SHIPPING'
							AND a.ship_from_org_id = 246)
             AND ROWNUM < 2;

   l_batch_id        NUMBER;
   l_group_id        NUMBER;
   l_return          NUMBER;
   l_template_code   VARCHAR2 (200);

   l_exception       EXCEPTION;
BEGIN
   FOR i IN 1 .. 500
   LOOP
      FOR rec_sign IN cur_sgn
      LOOP
         -- l_group_id := xxwc_print_request_groups_s.nextval;
         l_batch_id := xxwc_print_requests_s.NEXTVAL;
         l_group_id := xxwc_print_request_groups_s.NEXTVAL;

         l_template_code := 'XXWC_OM_PACK_SLIP'; --'XXWC_OM_PICK_SLIP';

         INSERT INTO XXWC.XXWC_PRINT_REQUESTS_TEMP (CREATED_BY,
                                                    CREATION_DATE,
                                                    LAST_UPDATED_BY,
                                                    LAST_UPDATE_DATE,
                                                    BATCH_ID,
                                                    PROCESS_FLAG,
                                                    GROUP_ID,
                                                    APPLICATION,
                                                    PROGRAM,
                                                    DESCRIPTION,
                                                    START_TIME,
                                                    SUB_REQUEST,
                                                    PRINTER,
                                                    STYLE,
                                                    COPIES,
                                                    SAVE_OUTPUT,
                                                    PRINT_TOGETHER,
                                                    VALIDATE_PRINTER,
                                                    TEMPLATE_APPL_NAME,
                                                    TEMPLATE_CODE,
                                                    TEMPLATE_LANGUAGE,
                                                    TEMPLATE_TERRITORY,
                                                    OUTPUT_FORMAT--,DMS_SIGN_FLAG
                                                    ,
                                                    NLS_LANGUAGE)
              VALUES (rec_sign.REQUESTED_BY                       --CREATED_BY
                                           ,
                      rec_sign.ACTUAL_COMPLETION_DATE         -- CREATION_DATE
                                                     ,
                      rec_sign.REQUESTED_BY                   --LAST_UDATED_BY
                                           ,
                      rec_sign.ACTUAL_COMPLETION_DATE      -- LAST_UPDATE_DATE
                                                     ,
                      l_batch_id                                    --BATCH_ID
                                ,
                      '1'                                       --PROCESS_FLAG
                         ,
                      l_group_id                                    --GROUP_ID
                                ,
                      'XXWC'                                     --APPLICATION
                            ,
                      rec_sign.concurrent_program_name               --PROGRAM
                                                      ,
                      rec_sign.USER_CONCURRENT_PROGRAM_NAME      --DESCRIPTION
                                                           ,
                      NULL                                        --START_TIME
                          ,
                      'FALSE'                                    --SUB_REQUEST
                             ,
                      'noprint'                                      --PRINTER
                               ,
                      NULL                                             --STYLE
                          ,
                      0                                               --COPIES
                       ,
                      'TRUE'                                     --SAVE_OUTPUT
                            ,
                      'N'                                     --PRINT_TOGETHER
                         ,
                      'RESOLVE'                             --VALIDATE_PRINTER
                               ,
                      'XXWC'                              --TEMPLATE_APPL_NAME
                            ,
                      l_template_code                          --TEMPLATE_CODE
                                     ,
                      'en'                                 --TEMPLATE_LANGUAGE
                          ,
                      'US'                                --TEMPLATE_TERRITORY
                          ,
                      'PDF'                                    --OUTPUT_FORMAT
                           --,'1'
                      ,
                      'en'                                      --NLS_LANGUAGE
                          );

         COMMIT;

         INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP
              VALUES (l_batch_id,
                      1,
                      l_group_id,
                      1,
                      rec_sign.argument1);  -- Organization_code

         INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP
              VALUES (l_batch_id,
                      1,
                      l_group_id,
                      2,
                      rec_sign.argument2);  -- Order Number

         INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP
              VALUES (l_batch_id,
                      1,
                      l_group_id,
                      3,
                      rec_sign.argument3);  -- Print Pricing Flag

         INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP
              VALUES (l_batch_id,
                      1,
                      l_group_id,
                      4,
                      rec_sign.argument4); -- Reprint

         INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP
              VALUES (l_batch_id,
                      1,
                      l_group_id,
                      5,
                      rec_sign.argument5); -- Print Kit Details

         INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP
              VALUES (l_batch_id,
                      1,
                      l_group_id,
                      6,
                      rec_sign.argument6); -- Send to Rightfax Yes or No?

         INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP
              VALUES (l_batch_id,
                      1,
                      l_group_id,
                      7,
                      rec_sign.argument7); -- Fax Number

         INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP
              VALUES (l_batch_id,
                      1,
                      l_group_id,
                      8,
                      rec_sign.argument8); -- Rightfax Comment

         INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP
              VALUES (l_batch_id,
                      1,
                      l_group_id,
                      9,
                      rec_sign.argument9);  -- Print Hazmat

         INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP
              VALUES (l_batch_id,
                      1,
                      l_group_id,
                      10,
                      rec_sign.argument10);  -- Delivery ID

         INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP
              VALUES (l_batch_id,
                      1,
                      l_group_id,
                      11,
                      rec_sign.argument11);  -- Print Allocations


         COMMIT;
		 
         l_return :=
            XXWC_ONT_ROUTINES_PKG.SUBMIT_PRINT_BATCH (l_batch_id,
                                                      l_group_id,
                                                      1,        -- ProcessFlag
                                                      30437, --28474, --rec_sign.REQUESTED_BY, Perry Njuguna
                                                      50857,
                                                      660);

         DBMS_OUTPUT.put_line (' l_return ' || l_return);
         COMMIT;
      END LOOP;
   END LOOP;
EXCEPTION
   WHEN l_exception
   THEN
      DBMS_OUTPUT.put_line (' l_Exception ');
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Error - ' || SQLERRM);
END;
/