/*******************************************************************
  script name: TMS_20180411-00265_DATA_FIX_DELETE_CSP.sql
  purpose: To delete specified csp after taking their backup
  revision history:
  Version   Date          Aurthor           Description
  ----------------------------------------------------------------
  1.0       17-APR-2017   Niraj K Ranjan    TMS#20180411-00265 CSP #501553 - RJK CONSTRUCTION - Job #10001765226   Sales Person Needs to Write Orders
*******************************************************************/
SET serveroutput on size 1000000;
CREATE TABLE xxwc.xxwc_20180411_0265_csp_hdr_bkp
   AS (SELECT * FROM xxwc_om_contract_pricing_hdr 
       WHERE agreement_id IN (501553));
       
CREATE TABLE xxwc.xxwc_20180411_0265_csp_ln_bkp
   AS (SELECT * FROM xxwc_om_contract_pricing_lines 
       WHERE agreement_id IN (501553));    
       
CREATE TABLE xxwc.xxwc_20180411_0265_csp_ntf_bkp
   AS (SELECT * FROM xxwc_om_csp_notifications_tbl 
       WHERE agreement_id IN (501553));
BEGIN
   dbms_output.put_line('TMS_20180411-00265 : '||'Start script');    
       
   DELETE FROM xxwc_om_csp_notifications_tbl
   WHERE agreement_id IN (501553);
   
   dbms_output.put_line('TMS_20180411-00265 : '||'Total records deleted from table xxwc_om_csp_notifications_tbl : '||SQL%ROWCOUNT);
   
   DELETE FROM xxwc_om_contract_pricing_lines 
   WHERE agreement_id IN (501553);
   
   dbms_output.put_line('TMS_20180411-00265 : '||'Total records deleted from table xxwc_om_contract_pricing_lines : '||SQL%ROWCOUNT);
   
   DELETE FROM xxwc_om_contract_pricing_hdr 
   WHERE agreement_id IN (501553);
   
   dbms_output.put_line('TMS_20180411-00265 : '||'Total records deleted from table xxwc_om_contract_pricing_hdr : '||SQL%ROWCOUNT);
   
   dbms_output.put_line('TMS_20180411-00265 : '||'End script');
   COMMIT;
EXCEPTION
   WHEN OTHERS THEN
      dbms_output.put_line('TMS_20180411-00265 : '||'ERROR - '||SQLERRM);
	  ROLLBACK;
END;
/