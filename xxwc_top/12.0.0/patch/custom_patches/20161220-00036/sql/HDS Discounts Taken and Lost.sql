--Report Name            : HDS Discounts Taken and Lost
--Import Instructions : 1. If RTF template needs to be uploaded to report automatically then move the RTF template to physical path of APPS_DATA_FILE_DIR oracle directory. Check dba_directories view to get the physical path of oracle directory. 
--                         (If you want to use any other oracle directory other than APPS_DATA_FILE_DIR then replace APPS_DATA_FILE_DIR with your oracle directory name below.)
--                      2. Make sure XXEIS schema has read/write permissions to above used oracle directory and give full permissions to the directory at OS level.
--                      3. Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
exec xxeis.eis_rsc_ins.g_rtf_dir := 'APPS_DATA_FILE_DIR' ;
prompt Creating Object Data EIS_XXHDS_AP_DISC_TAKEN_LOST_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXHDS_AP_DISC_TAKEN_LOST_V
xxeis.eis_rsc_ins.v( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V',200,'This view shows details of invoices with available discounts and the status of those discounts.','','','','AB063501','XXEIS','HDS EIS AP Discount Taken Lost','EXADTLV','','','VIEW','US','','EIS_AP_DISC_TAKEN_LOST_V');
--Delete Object Columns for EIS_XXHDS_AP_DISC_TAKEN_LOST_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXHDS_AP_DISC_TAKEN_LOST_V',200,FALSE);
--Inserting Object Columns for EIS_XXHDS_AP_DISC_TAKEN_LOST_V
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','TERMS_DATE',200,'Date used with payment terms to calculate scheduled payment of an invoice','TERMS_DATE','','','','AB063501','DATE','AP_INVOICES_ALL','TERMS_DATE','Terms Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','INVOICE_PAY_METHOD',200,'Name of payment method','INVOICE_PAY_METHOD','','','','AB063501','VARCHAR2','AP_INVOICES_ALL','PAYMENT_METHOD_LOOKUP_CODE','Invoice Pay Method','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','AMOUNT_APPLICABLE_TO_DISCOUNT',200,'Amount of invoice applicable to a discount','AMOUNT_APPLICABLE_TO_DISCOUNT','','','','AB063501','NUMBER','AP_INVOICES_ALL','AMOUNT_APPLICABLE_TO_DISCOUNT','Amount Applicable To Discount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','TAX_AMOUNT',200,'No Longer Used','TAX_AMOUNT','','','','AB063501','NUMBER','AP_INVOICES_ALL','TAX_AMOUNT','Tax Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','SUPPLIER_NUMBER',200,'Supplier number','SUPPLIER_NUMBER','','','','AB063501','VARCHAR2','PO_VENDORS','SEGMENT1','Supplier Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','INV_CURR_GROSS_AMOUNT',200,'Gross amount due for a scheduled payment in invoice currency','INV_CURR_GROSS_AMOUNT','','','','AB063501','NUMBER','AP_PAYMENT_SCHEDULES_ALL','INV_CURR_GROSS_AMOUNT','Inv Curr Gross Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','OPERATING_UNIT',200,'Operating Unit','OPERATING_UNIT','','','','AB063501','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','Operating Unit','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','SUPPLIER',200,'Supplier name','SUPPLIER','','','','AB063501','VARCHAR2','PO_VENDORS','VENDOR_NAME','Supplier','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','SUPPLIER_SITE',200,'Site code name','SUPPLIER_SITE','','','','AB063501','VARCHAR2','PO_VENDOR_SITES_ALL','VENDOR_SITE_CODE','Supplier Site','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','INVOICE_NUM',200,'Invoice number','INVOICE_NUM','','','','AB063501','VARCHAR2','AP_INVOICES_ALL','INVOICE_NUM','Invoice Num','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','INVOICE_DATE',200,'Invoice date','INVOICE_DATE','','','','AB063501','DATE','AP_INVOICES_ALL','INVOICE_DATE','Invoice Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','INVOICE_GL_DATE',200,'Accounting date to default to invoice distributions','INVOICE_GL_DATE','','','','AB063501','DATE','AP_INVOICES_ALL','GL_DATE','Invoice Gl Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','INVOICE_CURRENCY_CODE',200,'Currency code of invoice','INVOICE_CURRENCY_CODE','','','','AB063501','VARCHAR2','AP_INVOICES_ALL','INVOICE_CURRENCY_CODE','Invoice Currency Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','INVOICE_AMOUNT',200,'Amount paid','INVOICE_AMOUNT','','','','AB063501','NUMBER','AP_INVOICES_ALL','AMOUNT_PAID','Invoice Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','INVOICE_AMOUNT_PAID',200,'Amount paid','INVOICE_AMOUNT_PAID','','','','AB063501','NUMBER','AP_INVOICES_ALL','AMOUNT_PAID','Invoice Amount Paid','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','INVOICE_PAYMENT_STATUS',200,'Invoice Payment Status','INVOICE_PAYMENT_STATUS','','','','AB063501','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','Invoice Payment Status','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','INVOICE_BASE_AMOUNT',200,'When the Calculate User Exchange Rate option is enabled and when the exchange rate type is User, the user can enter a value for BASE_AMOUNT so the system can calculate the exchange rate.','INVOICE_BASE_AMOUNT','','','','AB063501','NUMBER','AP_INVOICES_ALL','BASE_AMOUNT','Invoice Base Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','APPROVED_AMOUNT',200,'Invoice amount approved through manual authorization for payment (used for reference purposes only).','APPROVED_AMOUNT','','','','AB063501','NUMBER','AP_INVOICES_ALL','APPROVED_AMOUNT','Approved Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','INVOICE_CANCELLED_AMOUNT',200,'Original amount of cancelled invoice','INVOICE_CANCELLED_AMOUNT','','','','AB063501','NUMBER','AP_INVOICES_ALL','CANCELLED_AMOUNT','Invoice Cancelled Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','AMOUNT_REMAINING',200,'Invoice amount remaining for payment','AMOUNT_REMAINING','','','','AB063501','NUMBER','AP_PAYMENT_SCHEDULES_ALL','AMOUNT_REMAINING','Amount Remaining','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','DISCOUNT_DATE',200,'Date first discount is available','DISCOUNT_DATE','','','','AB063501','DATE','AP_PAYMENT_SCHEDULES_ALL','DISCOUNT_DATE','Discount Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','DUE_DATE',200,'Due date','DUE_DATE','','','','AB063501','DATE','AP_PAYMENT_SCHEDULES_ALL','DUE_DATE','Due Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','DISCOUNT_AMOUNT_AVAILABLE',200,'discount amount available at first discount date','DISCOUNT_AMOUNT_AVAILABLE','','','','AB063501','NUMBER','AP_PAYMENT_SCHEDULES_ALL','DISCOUNT_AMOUNT_AVAILABLE','Discount Amount Available','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','LEDGER_NAME',200,'Ledger name','LEDGER_NAME','','','','AB063501','VARCHAR2','GL_LEDGERS','NAME','Ledger Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','BASE_CURRENCY_INVOICE_AMOUNT',200,'Invoice amount','BASE_CURRENCY_INVOICE_AMOUNT','','','','AB063501','NUMBER','AP_INVOICES_ALL','INVOICE_AMOUNT','Base Currency Invoice Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','BASE_CURR_DISCOUNT_LOST',200,'Base Curr Discount Lost','BASE_CURR_DISCOUNT_LOST','','','','AB063501','NUMBER','CALCULATION COLUMN','CALCULATION COLUMN','Base Curr Discount Lost','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','BASE_CURR_DISCOUNT_TAKEN',200,'Base Curr Discount Taken','BASE_CURR_DISCOUNT_TAKEN','','','','AB063501','NUMBER','CALCULATION COLUMN','CALCULATION COLUMN','Base Curr Discount Taken','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','CHECK_DATE',200,'Payment date','CHECK_DATE','','','','AB063501','DATE','AP_CHECKS_ALL','CHECK_DATE','Check Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','ACCOUNTING_DATE',200,'Accounting date','ACCOUNTING_DATE','','','','AB063501','DATE','AP_INVOICE_PAYMENTS_ALL','ACCOUNTING_DATE','Accounting Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','CASH_POSTED',200,'Cash Posted','CASH_POSTED','','','','AB063501','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','Cash Posted','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','PERIOD_NAME',200,'Period name','PERIOD_NAME','','','','AB063501','VARCHAR2','AP_INVOICE_PAYMENTS_ALL','PERIOD_NAME','Period Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','DISCOUNT_LOST',200,'Amount of discount lost','DISCOUNT_LOST','','','','AB063501','NUMBER','AP_INVOICE_PAYMENTS_ALL','DISCOUNT_LOST','Discount Lost','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','DISCOUNT_TAKEN',200,'Amount of discount taken','DISCOUNT_TAKEN','','','','AB063501','NUMBER','AP_INVOICE_PAYMENTS_ALL','DISCOUNT_TAKEN','Discount Taken','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','PAYMENT_BASE_AMOUNT',200,'Payment amount at payments exchange rate, only used for foreign currency invoice payments','PAYMENT_BASE_AMOUNT','','','','AB063501','NUMBER','AP_INVOICE_PAYMENTS_ALL','PAYMENT_BASE_AMOUNT','Payment Base Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','FUTURE_PAY_POSTED',200,'Future Pay Posted','FUTURE_PAY_POSTED','','','','AB063501','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','Future Pay Posted','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','JE_BATCH_ID',200,'No longer used','JE_BATCH_ID','','','','AB063501','NUMBER','AP_INVOICE_PAYMENTS_ALL','JE_BATCH_ID','Je Batch Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','ELECTRONIC_TRANSFER_ID',200,'No longer used','ELECTRONIC_TRANSFER_ID','','','','AB063501','NUMBER','AP_INVOICE_PAYMENTS_ALL','ELECTRONIC_TRANSFER_ID','Electronic Transfer Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','ASSETS_ADDITION',200,'Assets Addition','ASSETS_ADDITION','','','','AB063501','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','Assets Addition','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','INVOICE_PAYMENT_TYPE',200,'Creation method of a payment, used by Positive Pay feature (Single or Batch)','INVOICE_PAYMENT_TYPE','','','','AB063501','VARCHAR2','AP_INVOICE_PAYMENTS_ALL','INVOICE_PAYMENT_TYPE','Invoice Payment Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','OTHER_INVOICE_ID',200,'No longer used','OTHER_INVOICE_ID','','','','AB063501','NUMBER','AP_INVOICE_PAYMENTS_ALL','OTHER_INVOICE_ID','Other Invoice Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','EXTERNAL_BANK_ACCOUNT_ID',200,'External Bank Account identifier','EXTERNAL_BANK_ACCOUNT_ID','','','','AB063501','NUMBER','AP_INVOICE_PAYMENTS_ALL','EXTERNAL_BANK_ACCOUNT_ID','External Bank Account Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','REVERSAL_FLAG',200,'Reversal Flag','REVERSAL_FLAG','','','','AB063501','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','Reversal Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','REVERSAL_INV_PMT_ID',200,'Identifier for invoice payment reversed through current invoice payment','REVERSAL_INV_PMT_ID','','','','AB063501','NUMBER','AP_INVOICE_PAYMENTS_ALL','REVERSAL_INV_PMT_ID','Reversal Inv Pmt Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','ALTERNATE_NAME',200,'Alternate supplier name for kana value','ALTERNATE_NAME','','','','AB063501','VARCHAR2','PO_VENDORS','VENDOR_NAME_ALT','Alternate Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','TAX_REGISTRATION_NUMBER',200,'VAT registration number','TAX_REGISTRATION_NUMBER','','','','AB063501','VARCHAR2','PO_VENDORS','VAT_REGISTRATION_NUM','Tax Registration Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','INACTIVE_ON',200,'Key flexfield end date','INACTIVE_ON','','','','AB063501','DATE','PO_VENDORS','END_DATE_ACTIVE','Inactive On','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','CUSTOMER_NUM',200,'Customer number with the supplier','CUSTOMER_NUM','','','','AB063501','VARCHAR2','PO_VENDORS','CUSTOMER_NUM','Customer Num','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','ONE_TIME',200,'One Time','ONE_TIME','','','','AB063501','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','One Time','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','ACCOUNTING_EVENT_ID',200,'Accounting Event Identifier','ACCOUNTING_EVENT_ID','','','','AB063501','NUMBER','AP_INVOICE_PAYMENTS_ALL','ACCOUNTING_EVENT_ID','Accounting Event Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','CREDIT_STATUS_LOOKUP_CODE',200,'No longer used','CREDIT_STATUS_LOOKUP_CODE','','','','AB063501','VARCHAR2','PO_VENDORS','CREDIT_STATUS_LOOKUP_CODE','Credit Status Lookup Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','CREDIT_LIMIT',200,'Not used','CREDIT_LIMIT','','','','AB063501','NUMBER','PO_VENDORS','CREDIT_LIMIT','Credit Limit','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','VENDOR_SITE_ID',200,'Supplier site unique identifier','VENDOR_SITE_ID','','','','AB063501','NUMBER','PO_VENDOR_SITES_ALL','VENDOR_SITE_ID','Vendor Site Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','PAYMENT_STATUS',200,'Payment Status','PAYMENT_STATUS','','','','AB063501','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','Payment Status','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','PAYABLES_CC_ID',200,'Accounting Flexfield identifier for accounts payable liability account','PAYABLES_CC_ID','','','','AB063501','NUMBER','AP_INVOICE_PAYMENTS_ALL','ACCTS_PAY_CODE_COMBINATION_ID','Payables Cc Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','LOSS_CODE_COMBINATION_ID',200,'Accounting Flexfield identifier for account to which realized exchange rate losses are posted','LOSS_CODE_COMBINATION_ID','','','','AB063501','NUMBER','AP_INVOICE_PAYMENTS_ALL','LOSS_CODE_COMBINATION_ID','Loss Code Combination Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','INVOICE_PAYABLES_ACCOUNT',200,'Invoice Payables Account','INVOICE_PAYABLES_ACCOUNT','','','','AB063501','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','Invoice Payables Account','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','GAIN_CODE_COMBINATION_ID',200,'Accounting Flexfield identifier for account to which realized exchange rate gains are posted','GAIN_CODE_COMBINATION_ID','','','','AB063501','NUMBER','AP_INVOICE_PAYMENTS_ALL','GAIN_CODE_COMBINATION_ID','Gain Code Combination Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','FUTURE_PAY_CODE_COMBINATION_ID',200,'No longer used','FUTURE_PAY_CODE_COMBINATION_ID','','','','AB063501','NUMBER','AP_INVOICE_PAYMENTS_ALL','FUTURE_PAY_CODE_COMBINATION_ID','Future Pay Code Combination Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','ASSET_CODE_COMBINATION_ID',200,'Accounting Flexfield identifier for cash account','ASSET_CODE_COMBINATION_ID','','','','AB063501','NUMBER','AP_INVOICE_PAYMENTS_ALL','ASSET_CODE_COMBINATION_ID','Asset Code Combination Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','ACCTS_PAY_CODE_COMBINATION_ID',200,'Accounting Flexfield identifier for accounts payable liability account','ACCTS_PAY_CODE_COMBINATION_ID','','','','AB063501','NUMBER','AP_INVOICES_ALL','ACCTS_PAY_CODE_COMBINATION_ID','Accts Pay Code Combination Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','INVOICE_TERMS',200,'Invoice Terms','INVOICE_TERMS','','','','AB063501','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','Invoice Terms','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','INVOICE_PAY_GROUP',200,'Name of pay group','INVOICE_PAY_GROUP','','','','AB063501','VARCHAR2','AP_INVOICES_ALL','PAY_GROUP_LOOKUP_CODE','Invoice Pay Group','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','PAY_CURR_INVOICE_AMOUNT',200,'Invoice amount in the payment currency','PAY_CURR_INVOICE_AMOUNT','','','','AB063501','NUMBER','AP_INVOICES_ALL','PAY_CURR_INVOICE_AMOUNT','Pay Curr Invoice Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','PAYMENT_CROSS_RATE_DATE',200,'Cross currency payment rate date','PAYMENT_CROSS_RATE_DATE','','','','AB063501','DATE','AP_INVOICES_ALL','PAYMENT_CROSS_RATE_DATE','Payment Cross Rate Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','VENDOR_SITE_CODE',200,'Site code name','VENDOR_SITE_CODE','','','','AB063501','VARCHAR2','PO_VENDOR_SITES_ALL','VENDOR_SITE_CODE','Vendor Site Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','VENDOR_SITE_CODE_ALT',200,'Alternate supplier site code for Kana Value','VENDOR_SITE_CODE_ALT','','','','AB063501','VARCHAR2','PO_VENDOR_SITES_ALL','VENDOR_SITE_CODE_ALT','Vendor Site Code Alt','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','PURCHASING_SITE',200,'Purchasing Site','PURCHASING_SITE','','','','AB063501','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','Purchasing Site','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','RFQ_SITE',200,'Request for Quote Site','RFQ_SITE','','','','AB063501','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','Rfq Site','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','PAY_SITE',200,'Pay Site','PAY_SITE','','','','AB063501','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','Pay Site','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','TAX_REPORTING_SITE',200,'Tax Reporting Site','TAX_REPORTING_SITE','','','','AB063501','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','Tax Reporting Site','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','IBAN_NUMBER',200,'International bank account number. Used internationally to uniquely identify the account of a customer at a financial institution. During bank entry, the system validates the value to ensure that it is a valid IBAN.','IBAN_NUMBER','','','','AB063501','VARCHAR2','AP_INVOICE_PAYMENTS_ALL','IBAN_NUMBER','Iban Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','P_CARD_SITE',200,'Procurement Card Site','P_CARD_SITE','','','','AB063501','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','P Card Site','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','ADDRESS_LINE1',200,'First line of supplier address','ADDRESS_LINE1','','','','AB063501','VARCHAR2','PO_VENDOR_SITES_ALL','ADDRESS_LINE1','Address Line1','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','ADDRESS_LINE2',200,'Second line of supplier address','ADDRESS_LINE2','','','','AB063501','VARCHAR2','PO_VENDOR_SITES_ALL','ADDRESS_LINE2','Address Line2','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','ADDRESS_LINE3',200,'Third line of supplier address','ADDRESS_LINE3','','','','AB063501','VARCHAR2','PO_VENDOR_SITES_ALL','ADDRESS_LINE3','Address Line3','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','ADDRESS_LINE4',200,'Fourth line of address','ADDRESS_LINE4','','','','AB063501','VARCHAR2','PO_VENDOR_SITES_ALL','ADDRESS_LINE4','Address Line4','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','CITY',200,'City name','CITY','','','','AB063501','VARCHAR2','PO_VENDOR_SITES_ALL','CITY','City','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','STATE',200,'State name or abbreviation','STATE','','','','AB063501','VARCHAR2','PO_VENDOR_SITES_ALL','STATE','State','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','ZIP',200,'Postal code','ZIP','','','','AB063501','VARCHAR2','PO_VENDOR_SITES_ALL','ZIP','Zip','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','COUNTY',200,'Supplier site county','COUNTY','','','','AB063501','VARCHAR2','PO_VENDOR_SITES_ALL','COUNTY','County','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','PROVINCE',200,'Province','PROVINCE','','','','AB063501','VARCHAR2','PO_VENDOR_SITES_ALL','PROVINCE','Province','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','COUNTRY',200,'Country name','COUNTRY','','','','AB063501','VARCHAR2','PO_VENDOR_SITES_ALL','COUNTRY','Country','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','AREA_CODE',200,'Area code','AREA_CODE','','','','AB063501','VARCHAR2','PO_VENDOR_SITES_ALL','AREA_CODE','Area Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','PHONE',200,'Phone number','PHONE','','','','AB063501','VARCHAR2','PO_VENDOR_SITES_ALL','PHONE','Phone','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','FAX_AREA_CODE',200,'Customer site ','FAX_AREA_CODE','','','','AB063501','VARCHAR2','PO_VENDOR_SITES_ALL','FAX_AREA_CODE','Fax Area Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','FAX',200,'Customer site facsimile number','FAX','','','','AB063501','VARCHAR2','PO_VENDOR_SITES_ALL','FAX','Fax','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','SUPPLIER_NOTIF_METHOD',200,'The preferred Notification Method for the supplier','SUPPLIER_NOTIF_METHOD','','','','AB063501','VARCHAR2','PO_VENDOR_SITES_ALL','SUPPLIER_NOTIF_METHOD','Supplier Notif Method','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','THIRD_DISC_AMT_AVAILABLE',200,'Discount amount available at third discount date','THIRD_DISC_AMT_AVAILABLE','','','','AB063501','NUMBER','AP_PAYMENT_SCHEDULES_ALL','THIRD_DISC_AMT_AVAILABLE','Third Disc Amt Available','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','DISCOUNT_AMOUNT_REMAINING',200,'Discount amount available on remaining amount for payment','DISCOUNT_AMOUNT_REMAINING','','','','AB063501','NUMBER','AP_PAYMENT_SCHEDULES_ALL','DISCOUNT_AMOUNT_REMAINING','Discount Amount Remaining','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','INVOICE_ID',200,'Invoice identifier','INVOICE_ID','','','','AB063501','NUMBER','AP_INVOICES_ALL','INVOICE_ID','Invoice Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','INVOICE_SOURCE',200,'Source of invoice','INVOICE_SOURCE','','','','AB063501','VARCHAR2','AP_INVOICES_ALL','SOURCE','Invoice Source','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','INVOICE_TYPE',200,'Type of invoice','INVOICE_TYPE','','','','AB063501','VARCHAR2','AP_INVOICES_ALL','INVOICE_TYPE_LOOKUP_CODE','Invoice Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','INVOICE_DESCRIPTION',200,'Description','INVOICE_DESCRIPTION','','','','AB063501','VARCHAR2','AP_INVOICES_ALL','DESCRIPTION','Invoice Description','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','PAYMENT_CURRENCY_CODE',200,'Currency code of payment (must be same as INVOICE_CURRENCY_CODE or have a fixed rate relationship)','PAYMENT_CURRENCY_CODE','','','','AB063501','VARCHAR2','AP_INVOICES_ALL','PAYMENT_CURRENCY_CODE','Payment Currency Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','PAYMENT_CROSS_RATE',200,'Exchange rate between invoice and payment; in Release 11 the value is always 1 unless they are associated fixed-rate currencies','PAYMENT_CROSS_RATE','','','','AB063501','NUMBER','AP_INVOICES_ALL','PAYMENT_CROSS_RATE','Payment Cross Rate','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','INVOICE_EXCLUSIVE_PAYMENT',200,'Invoice Exclusive Payment','INVOICE_EXCLUSIVE_PAYMENT','','','','AB063501','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','Invoice Exclusive Payment','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','INVOICE_CANCELLED_DATE',200,'Date invoice cancelled','INVOICE_CANCELLED_DATE','','','','AB063501','DATE','AP_INVOICES_ALL','CANCELLED_DATE','Invoice Cancelled Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','INVOICE_CANCELLED_BY',200,'Invoice Cancelled By','INVOICE_CANCELLED_BY','','','','AB063501','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','Invoice Cancelled By','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','INVOICE_TEMP_CANCELLED_AMOUNT',200,'Column for recording the original amount of an invoice until cancellation completes successfully','INVOICE_TEMP_CANCELLED_AMOUNT','','','','AB063501','NUMBER','AP_INVOICES_ALL','TEMP_CANCELLED_AMOUNT','Invoice Temp Cancelled Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','FUTURE_PAY_DUE_DATE',200,'No longer used','FUTURE_PAY_DUE_DATE','','','','AB063501','DATE','AP_PAYMENT_SCHEDULES_ALL','FUTURE_PAY_DUE_DATE','Future Pay Due Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','GROSS_AMOUNT',200,'Gross amount due for a scheduled payment','GROSS_AMOUNT','','','','AB063501','NUMBER','AP_PAYMENT_SCHEDULES_ALL','GROSS_AMOUNT','Gross Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','HOLD_FLAG',200,'Flag that indicates if scheduled payment is on hold (Y or N)','HOLD_FLAG','','','','AB063501','VARCHAR2','AP_PAYMENT_SCHEDULES_ALL','HOLD_FLAG','Hold Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','PAYMENT_METHOD_LOOKUP_CODE',200,'Payment method (for example, CHECK, WIRE, EFT)','PAYMENT_METHOD_LOOKUP_CODE','','','','AB063501','VARCHAR2','AP_PAYMENT_SCHEDULES_ALL','PAYMENT_METHOD_LOOKUP_CODE','Payment Method Lookup Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','PAYMENT_PRIORITY',200,'Number representing payment priority of a scheduled payment (1 to 99)','PAYMENT_PRIORITY','','','','AB063501','NUMBER','AP_PAYMENT_SCHEDULES_ALL','PAYMENT_PRIORITY','Payment Priority','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','SECOND_DISCOUNT_DATE',200,'Date second discount is available','SECOND_DISCOUNT_DATE','','','','AB063501','DATE','AP_PAYMENT_SCHEDULES_ALL','SECOND_DISCOUNT_DATE','Second Discount Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','THIRD_DISCOUNT_DATE',200,'Date third discount is available','THIRD_DISCOUNT_DATE','','','','AB063501','DATE','AP_PAYMENT_SCHEDULES_ALL','THIRD_DISCOUNT_DATE','Third Discount Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','SECOND_DISC_AMT_AVAILABLE',200,'Discount amount available at second discount date','SECOND_DISC_AMT_AVAILABLE','','','','AB063501','NUMBER','AP_PAYMENT_SCHEDULES_ALL','SECOND_DISC_AMT_AVAILABLE','Second Disc Amt Available','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','EMAIL_ADDRESS',200,'Email Address of the supplier Contact','EMAIL_ADDRESS','','','','AB063501','VARCHAR2','PO_VENDOR_SITES_ALL','EMAIL_ADDRESS','Email Address','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','LAST_UPDATED_BY',200,'Last Updated By','LAST_UPDATED_BY','','','','AB063501','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','Last Updated By','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','LAST_UPDATE_DATE',200,'Standard Who column - date when a user last updated this row.','LAST_UPDATE_DATE','','','','AB063501','DATE','AP_INVOICE_PAYMENTS_ALL','LAST_UPDATE_DATE','Last Update Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','PAYMENT_NUM',200,'Payment number','PAYMENT_NUM','','','','AB063501','NUMBER','AP_INVOICE_PAYMENTS_ALL','PAYMENT_NUM','Payment Num','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','ACCRUAL_POSTED',200,'Accrual Posted','ACCRUAL_POSTED','','','','AB063501','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','Accrual Posted','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','AMOUNT',200,'Payment amount','AMOUNT','','','','AB063501','NUMBER','AP_INVOICE_PAYMENTS_ALL','AMOUNT','Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','CHECK_ID',200,'Payment identifier','CHECK_ID','','','','AB063501','NUMBER','AP_INVOICE_PAYMENTS_ALL','CHECK_ID','Check Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','INVOICE_PAYMENT_ID',200,'Invoice payment identifier','INVOICE_PAYMENT_ID','','','','AB063501','NUMBER','AP_INVOICE_PAYMENTS_ALL','INVOICE_PAYMENT_ID','Invoice Payment Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','INVOICE_POSTED',200,'Invoice Posted','INVOICE_POSTED','','','','AB063501','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','Invoice Posted','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','PAYABLES_ACCOUNT',200,'Payables Account','PAYABLES_ACCOUNT','','','','AB063501','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','Payables Account','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','ASSETS_ACCOUNT',200,'Assets Account','ASSETS_ACCOUNT','','','','AB063501','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','Assets Account','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','CREATED_BY',200,'Standard who column - user who created this row (foreign key to FND_USER.USER_ID).','CREATED_BY','','','','AB063501','VARCHAR2','AP_INVOICE_PAYMENTS_ALL','CREATED_BY','Created By','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','CREATION_DATE',200,'Standard who column - date when this row was created.','CREATION_DATE','','','','AB063501','DATE','AP_INVOICE_PAYMENTS_ALL','CREATION_DATE','Creation Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','LAST_UPDATED',200,'Last Updated','LAST_UPDATED','','','','AB063501','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','Last Updated','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','BANK_ACCOUNT_NUM',200,'Bank account number','BANK_ACCOUNT_NUM','','','','AB063501','VARCHAR2','AP_INVOICE_PAYMENTS_ALL','BANK_ACCOUNT_NUM','Bank Account Num','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','BANK_NUM',200,'Bank number','BANK_NUM','','','','AB063501','VARCHAR2','AP_INVOICE_PAYMENTS_ALL','BANK_NUM','Bank Num','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','INV_CURR_EXCHANGE_DATE',200,'Date for which exchange rate is obtained from daily rates table','INV_CURR_EXCHANGE_DATE','','','','AB063501','DATE','AP_INVOICE_PAYMENTS_ALL','EXCHANGE_DATE','Inv Curr Exchange Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','INVOICE_CURR_EXCHANGE_RATE',200,'Exchange rate for a foreign currency payment','INVOICE_CURR_EXCHANGE_RATE','','','','AB063501','NUMBER','AP_INVOICE_PAYMENTS_ALL','EXCHANGE_RATE','Invoice Curr Exchange Rate','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','INV_CURR_EXCHANGE_RATE_TYPE',200,'Exchange rate type for a foreign currency payment','INV_CURR_EXCHANGE_RATE_TYPE','','','','AB063501','VARCHAR2','AP_INVOICE_PAYMENTS_ALL','EXCHANGE_RATE_TYPE','Inv Curr Exchange Rate Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','GAIN_ACCOUNT',200,'Gain Account','GAIN_ACCOUNT','','','','AB063501','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','Gain Account','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','LOSS_ACCOUNT',200,'Loss Account','LOSS_ACCOUNT','','','','AB063501','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','Loss Account','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','CASH_JE_BATCH_ID',200,'No longer used','CASH_JE_BATCH_ID','','','','AB063501','NUMBER','AP_INVOICE_PAYMENTS_ALL','CASH_JE_BATCH_ID','Cash Je Batch Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','FUTURE_PAYABLES_ACCOUNT',200,'Future Payables Account','FUTURE_PAYABLES_ACCOUNT','','','','AB063501','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','Future Payables Account','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','ORGANIZATION_ID',200,'Organization Id','ORGANIZATION_ID','','','','AB063501','NUMBER','HR_ALL_ORGANIZATION_UNTIS','ORGANIZATION_ID','Organization Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','VENDOR_ID',200,'Supplier unique identifier','VENDOR_ID','','','','AB063501','NUMBER','AP_SUPPLIERS','VENDOR_ID','Vendor Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','ORG_ID',200,'Org Id','ORG_ID','','','','AB063501','NUMBER','AP_INVOICES','ORG_ID','Org Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','APS_INVOICE_ID',200,'Aps Invoice Id','APS_INVOICE_ID','','','','AB063501','NUMBER','','','Aps Invoice Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','APS_PAYMENT_NUM',200,'Aps Payment Num','APS_PAYMENT_NUM','','','','AB063501','NUMBER','','','Aps Payment Num','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','AUTO_TAX_CALC_FLAG',200,'Auto Tax Calc Flag','AUTO_TAX_CALC_FLAG','','','','AB063501','VARCHAR2','','','Auto Tax Calc Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','BANK_ACCOUNT_TYPE',200,'Bank Account Type','BANK_ACCOUNT_TYPE','','','','AB063501','VARCHAR2','','','Bank Account Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','DISCOUNT_AMOUNT_TAKEN',200,'Discount Amount Taken','DISCOUNT_AMOUNT_TAKEN','','','','AB063501','NUMBER','','','Discount Amount Taken','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','REMITTANCE_EMAIL',200,'Remittance Email','REMITTANCE_EMAIL','','','','AB063501','VARCHAR2','','','Remittance Email','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','SET_OF_BOOKS',200,'Set Of Books','SET_OF_BOOKS','','','','AB063501','VARCHAR2','','','Set Of Books','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','COPYRIGHT',200,'Copyright','COPYRIGHT','','','','AB063501','VARCHAR2','','','Copyright','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','API#162#Documentum_PO_Number',200,'Descriptive flexfield (DFF): Invoice Column Name: Documentum PO Number Context: 162','API#162#Documentum_PO_Number','','','','AB063501','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE2','Api#162#Documentum Po Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','API#163#BizTalk_ID_Frt_Bizta',200,'Descriptive flexfield (DFF): Invoice Column Name: BizTalk ID-Frt Biztalk Context: 163','API#163#BizTalk_ID_Frt_Bizta','','','','AB063501','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE1','Api#163#Biztalk Id-Frt Biztalk','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','API#163#Invoice_Type',200,'Descriptive flexfield (DFF): Invoice Column Name: Invoice Type Context: 163','API#163#Invoice_Type','','','','AB063501','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE11','Api#163#Invoice Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','API#163#DCTM_Image_Link',200,'Descriptive flexfield (DFF): Invoice Column Name: DCTM Image Link Context: 163','API#163#DCTM_Image_Link','','','','AB063501','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE14','Api#163#Dctm Image Link','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','API#163#R11_Invoice_ID',200,'Descriptive flexfield (DFF): Invoice Column Name: R11 Invoice ID Context: 163','API#163#R11_Invoice_ID','','','','AB063501','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE15','Api#163#R11 Invoice Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','API#163#Branch_Frt_Biztalk',200,'Descriptive flexfield (DFF): Invoice Column Name: Branch-Frt Biztalk Context: 163','API#163#Branch_Frt_Biztalk','','','','AB063501','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE2','Api#163#Branch-Frt Biztalk','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','API#163#SO_Number',200,'Descriptive flexfield (DFF): Invoice Column Name: SO Number Context: 163','API#163#SO_Number','','','','AB063501','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE3','Api#163#So Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','API#163#PO_Number',200,'Descriptive flexfield (DFF): Invoice Column Name: PO Number Context: 163','API#163#PO_Number','','','','AB063501','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE4','Api#163#Po Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','API#163#BOL',200,'Descriptive flexfield (DFF): Invoice Column Name: BOL Context: 163','API#163#BOL','','','','AB063501','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE6','Api#163#Bol','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','API#163#POS_System_Code_Frt_',200,'Descriptive flexfield (DFF): Invoice Column Name: POS System Code-Frt Biztalk Context: 163','API#163#POS_System_Code_Frt_','','','','AB063501','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE8','Api#163#Pos System Code-Frt Biztalk','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','API#166#Case_Identifier',200,'Descriptive flexfield (DFF): Invoice Column Name: Case Identifier Context: 166','API#166#Case_Identifier','','','','AB063501','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE1','Api#166#Case Identifier','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','API#166#R11_Invoice_ID',200,'Descriptive flexfield (DFF): Invoice Column Name: R11 Invoice ID Context: 166','API#166#R11_Invoice_ID','','','','AB063501','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE15','Api#166#R11 Invoice Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','API#166#Deduction_Code',200,'Descriptive flexfield (DFF): Invoice Column Name: Deduction Code Context: 166','API#166#Deduction_Code','','','','AB063501','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE2','Api#166#Deduction Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','API#166#Employee',200,'Descriptive flexfield (DFF): Invoice Column Name: Employee Context: 166','API#166#Employee','','','','AB063501','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE3','Api#166#Employee','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','API#166#Description',200,'Descriptive flexfield (DFF): Invoice Column Name: Description Context: 166','API#166#Description','','','','AB063501','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE4','Api#166#Description','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','API#167#SO_Number',200,'Descriptive flexfield (DFF): Invoice Column Name: SO Number Context: 167','API#167#SO_Number','','','','AB063501','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE3','Api#167#So Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','API#167#PO_Number',200,'Descriptive flexfield (DFF): Invoice Column Name: PO Number Context: 167','API#167#PO_Number','','','','AB063501','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE4','Api#167#Po Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','API#167#Canada_Tax_Type',200,'Descriptive flexfield (DFF): Invoice Column Name: Canada Tax Type Context: 167','API#167#Canada_Tax_Type','','','','AB063501','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE5','Api#167#Canada Tax Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','API#167#BOL',200,'Descriptive flexfield (DFF): Invoice Column Name: BOL Context: 167','API#167#BOL','','','','AB063501','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE6','Api#167#Bol','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','APS#Test',200,'Descriptive flexfield (DFF): Invoice Payments Column Name: Test','APS#Test','','','','AB063501','VARCHAR2','AP_PAYMENT_SCHEDULES_ALL','ATTRIBUTE1','Aps#Test','','','','US');
--Inserting Object Components for EIS_XXHDS_AP_DISC_TAKEN_LOST_V
xxeis.eis_rsc_ins.vcomp( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','AP_INVOICE_PAYMENTS',200,'AP_INVOICE_PAYMENTS_ALL','APP','APP','AB063501','AB063501','140914397','Invoice Payment Records','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','AP_CHECKS',200,'AP_CHECKS_ALL','C','C','AB063501','AB063501','140914397','Supplier Payment Data','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','HR_ORGANIZATION_UNITS',200,'HR_ALL_ORGANIZATION_UNITS','HOU','HOU','AB063501','AB063501','140914397','Organization Details','N','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','AP_PAYMENT_SCHEDULES',200,'AP_PAYMENT_SCHEDULES_ALL','APS','APS','AB063501','AB063501','140914397','Payment Schedules','','','','','APS','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','AP_INVOICES',200,'AP_INVOICES_ALL','API','API','AB063501','AB063501','140914397','Detailed Invoice Records','','','','','','',' ',' ',' ',' ',' ');
--Inserting Object Component Joins for EIS_XXHDS_AP_DISC_TAKEN_LOST_V
xxeis.eis_rsc_ins.vcj( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','AP_INVOICE_PAYMENTS','APP',200,'EXADTLV.INVOICE_PAYMENT_ID','=','APP.INVOICE_PAYMENT_ID(+)','','','','Y','AB063501');
xxeis.eis_rsc_ins.vcj( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','AP_CHECKS','C',200,'EXADTLV.CHECK_ID','=','C.CHECK_ID(+)','','','','Y','AB063501');
xxeis.eis_rsc_ins.vcj( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','HR_ORGANIZATION_UNITS','HOU',200,'EXADTLV.ORGANIZATION_ID','=','HOU.ORGANIZATION_ID(+)','','','','Y','AB063501');
xxeis.eis_rsc_ins.vcj( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','AP_PAYMENT_SCHEDULES','APS',200,'EXADTLV.INVOICE_ID','=','APS.INVOICE_ID(+)','','','','Y','AB063501');
xxeis.eis_rsc_ins.vcj( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','AP_PAYMENT_SCHEDULES','APS',200,'EXADTLV.PAYMENT_NUM','=','APS.PAYMENT_NUM(+)','','','','Y','AB063501');
xxeis.eis_rsc_ins.vcj( 'EIS_XXHDS_AP_DISC_TAKEN_LOST_V','AP_INVOICES','API',200,'EXADTLV.INVOICE_ID','=','API.INVOICE_ID(+)','','','','Y','AB063501');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
prompt Creating Report LOV Data for HDS Discounts Taken and Lost
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - HDS Discounts Taken and Lost
xxeis.eis_rsc_ins.lov( 200,'SELECT AIA.INVOICE_NUM INVOICE_NUMBER,
  HOU.NAME OPERATING_UNIT
FROM AP_INVOICES AIA,
  HR_OPERATING_UNITS HOU
WHERE AIA.ORG_ID=HOU.ORGANIZATION_ID','','AP Invoice Numbers LOV','This LOV lists Account Payables Invoice Numbers','XXEIS_RS_ADMIN',NULL,'','','','Y','','','','US');
xxeis.eis_rsc_ins.lov( 200,'SELECT hou.NAME Operating_Unit,
       TO_CHAR (date_from,
                NVL (fnd_profile.VALUE (''ICX_DATE_FORMAT_MASK''),
                     ''DD-MON-YYYY'')
               ) date_from,
       TO_CHAR (date_to,
                NVL (fnd_profile.VALUE (''ICX_DATE_FORMAT_MASK''),
                     ''DD-MON-YYYY'')
               ) date_to
  FROM hr_operating_units hou,
       mo_glob_org_access_tmp tmp
 WHERE hou.organization_id = tmp.organization_id','','AP Multi Operating Unit LOV','This LOV shows all the operating units, to which user has access.','XXEIS_RS_ADMIN',NULL,'N','','','Y','','','','US');
xxeis.eis_rsc_ins.lov( 200,'select DISTINCT vendor_name from po_vendors','','AP Vendor Names LOV','This LOV lists all Purchasing Vendor Names','XXEIS_RS_ADMIN',NULL,'','','','Y','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
prompt Creating Report Data for HDS Discounts Taken and Lost
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Deleting Report data - HDS Discounts Taken and Lost
xxeis.eis_rsc_utility.delete_report_rows( 'HDS Discounts Taken and Lost' );
--Inserting Report - HDS Discounts Taken and Lost
xxeis.eis_rsc_ins.r( 200,'HDS Discounts Taken and Lost','','This Report is copied from Discounts Taken and Lost.This Report can be used to get the details of the discounts that are taken and lost over invoice payments within invoice number range, invoice date range or supplier range.','802','','','AB063501','EIS_XXHDS_AP_DISC_TAKEN_LOST_V','Y','','','AB063501','','N','Payments','RTF,PDF,','HTML,Html Summary,CSV,XML,EXCEL,Pivot Excel,','','','','','','','N','','US','','Discounts Taken and Lost','','');
--Inserting Report Columns - HDS Discounts Taken and Lost
xxeis.eis_rsc_ins.rc( 'HDS Discounts Taken and Lost',200,'AMOUNT_APPLICABLE_TO_DISCOUNT','Amount Applicable To Discount','Amount of invoice applicable to a discount','','','','','28','N','','','','','','','','AB063501','N','N','','EIS_XXHDS_AP_DISC_TAKEN_LOST_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Discounts Taken and Lost',200,'AMOUNT_REMAINING','Amount Remaining','Invoice amount remaining for payment','','','','','30','N','','','','','','','','AB063501','N','N','','EIS_XXHDS_AP_DISC_TAKEN_LOST_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Discounts Taken and Lost',200,'APPROVED_AMOUNT','Approved Amount','Invoice amount approved through manual authorization for payment (used for reference purposes only).','','','','','26','N','','','','','','','','AB063501','N','N','','EIS_XXHDS_AP_DISC_TAKEN_LOST_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Discounts Taken and Lost',200,'CASH_POSTED','Cash Posted','Cash Posted','','','','','32','N','','','','','','','','AB063501','N','N','','EIS_XXHDS_AP_DISC_TAKEN_LOST_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Discounts Taken and Lost',200,'DISCOUNT_AMOUNT_AVAILABLE','Discount Amount Available','discount amount available at first discount date','','','','','31','N','','','','','','','','AB063501','N','N','','EIS_XXHDS_AP_DISC_TAKEN_LOST_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Discounts Taken and Lost',200,'DISCOUNT_DATE','Discount Date','Date first discount is available','','','','','11','N','','','','','','','','AB063501','N','N','','EIS_XXHDS_AP_DISC_TAKEN_LOST_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Discounts Taken and Lost',200,'DISCOUNT_LOST','Discount Lost','Amount of discount lost','','','','','14','N','','','','','','','','AB063501','N','N','','EIS_XXHDS_AP_DISC_TAKEN_LOST_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Discounts Taken and Lost',200,'DISCOUNT_TAKEN','Discount Taken','Amount of discount taken','','','','','13','N','','','','','','','','AB063501','N','N','','EIS_XXHDS_AP_DISC_TAKEN_LOST_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Discounts Taken and Lost',200,'DUE_DATE','Due Date','Due date','','','','','12','N','','','','','','','','AB063501','N','N','','EIS_XXHDS_AP_DISC_TAKEN_LOST_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Discounts Taken and Lost',200,'INVOICE_AMOUNT','Invoice Amount','Amount paid','','','','','9','N','','','','','','','','AB063501','N','N','','EIS_XXHDS_AP_DISC_TAKEN_LOST_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Discounts Taken and Lost',200,'INVOICE_AMOUNT_PAID','Invoice Amount Paid','Amount paid','','','','','29','N','','','','','','','','AB063501','N','N','','EIS_XXHDS_AP_DISC_TAKEN_LOST_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Discounts Taken and Lost',200,'INVOICE_BASE_AMOUNT','Invoice Base Amount','When the Calculate User Exchange Rate option is enabled and when the exchange rate type is User, the user can enter a value for BASE_AMOUNT so the system can calculate the exchange rate.','','','','','34','N','','','','','','','','AB063501','N','N','','EIS_XXHDS_AP_DISC_TAKEN_LOST_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Discounts Taken and Lost',200,'INVOICE_CANCELLED_AMOUNT','Invoice Cancelled Amount','Original amount of cancelled invoice','','','','','27','N','','','','','','','','AB063501','N','N','','EIS_XXHDS_AP_DISC_TAKEN_LOST_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Discounts Taken and Lost',200,'INVOICE_DATE','Invoice Date','Invoice date','','','','','8','N','','','','','','','','AB063501','N','N','','EIS_XXHDS_AP_DISC_TAKEN_LOST_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Discounts Taken and Lost',200,'INVOICE_GL_DATE','Invoice Gl Date','Accounting date to default to invoice distributions','','','','','19','N','','','','','','','','AB063501','N','N','','EIS_XXHDS_AP_DISC_TAKEN_LOST_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Discounts Taken and Lost',200,'ACCOUNTING_DATE','Accounting Date','Accounting date','','','','','33','N','','','','','','','','AB063501','N','N','','EIS_XXHDS_AP_DISC_TAKEN_LOST_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Discounts Taken and Lost',200,'LEDGER_NAME','Ledger Name','Ledger name','','','','','1','N','','','','','','','','AB063501','N','N','','EIS_XXHDS_AP_DISC_TAKEN_LOST_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Discounts Taken and Lost',200,'BASE_CURRENCY_INVOICE_AMOUNT','Total invoice amount','Invoice amount','','','','','10','N','','','','','','','','AB063501','N','N','','EIS_XXHDS_AP_DISC_TAKEN_LOST_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Discounts Taken and Lost',200,'BASE_CURR_DISCOUNT_LOST','Total discount lost','Base Curr Discount Lost','','','','','16','N','','','','','','','','AB063501','N','N','','EIS_XXHDS_AP_DISC_TAKEN_LOST_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Discounts Taken and Lost',200,'INVOICE_PAYMENT_STATUS','Invoice Payment Status','Invoice Payment Status','','','','','22','N','','','','','','','','AB063501','N','N','','EIS_XXHDS_AP_DISC_TAKEN_LOST_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Discounts Taken and Lost',200,'INVOICE_PAY_METHOD','Invoice Pay Method','Name of payment method','','','','','21','N','','','','','','','','AB063501','N','N','','EIS_XXHDS_AP_DISC_TAKEN_LOST_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Discounts Taken and Lost',200,'INV_CURR_GROSS_AMOUNT','Inv Curr Gross Amount','Gross amount due for a scheduled payment in invoice currency','','','','','23','N','','','','','','','','AB063501','N','N','','EIS_XXHDS_AP_DISC_TAKEN_LOST_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Discounts Taken and Lost',200,'PAYMENT_BASE_AMOUNT','Payment Base Amount','Payment amount at payments exchange rate, only used for foreign currency invoice payments','','','','','24','N','','','','','','','','AB063501','N','N','','EIS_XXHDS_AP_DISC_TAKEN_LOST_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Discounts Taken and Lost',200,'PERIOD_NAME','Period Name','Period name','','','','','18','N','','','','','','','','AB063501','N','N','','EIS_XXHDS_AP_DISC_TAKEN_LOST_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Discounts Taken and Lost',200,'SUPPLIER_NUMBER','Supplier Number','Supplier number','','','','','4','N','','','','','','','','AB063501','N','N','','EIS_XXHDS_AP_DISC_TAKEN_LOST_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Discounts Taken and Lost',200,'TAX_AMOUNT','Tax Amount','No Longer Used','','','','','25','N','','','','','','','','AB063501','N','N','','EIS_XXHDS_AP_DISC_TAKEN_LOST_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Discounts Taken and Lost',200,'TERMS_DATE','Terms Date','Date used with payment terms to calculate scheduled payment of an invoice','','','','','20','N','','','','','','','','AB063501','N','N','','EIS_XXHDS_AP_DISC_TAKEN_LOST_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Discounts Taken and Lost',200,'SUPPLIER','Supplier','Supplier name','','','','','3','N','','','','','','','','AB063501','N','N','','EIS_XXHDS_AP_DISC_TAKEN_LOST_V','','','','US','T');
 -- Exporting Template EIS AP Supplier Information Template
xxeis.eis_rsc_ins.qt('EIS AP Supplier Information Template','EIS AP Supplier Information Template','Y','','Y');
--Delete existing EIS AP Supplier Information Template data 
xxeis.eis_rsc_insert.delete_template_rows('EIS AP Supplier Information Template','');
 -- Exporting Template Common Params for EIS AP Supplier Information Template
xxeis.eis_rsc_ins.qtp('EIS AP Supplier Information Template','Vendor Id','');
 -- Exporting Template Details for Template => EIS AP Supplier Information Template
xxeis.eis_rsc_ins.q('EIS AP Supplier Information Query','EIS AP Supplier Information Query','select
  hou.name operating_unit,
  pov.vendor_name supplier_name,
  pov.segment1 supplier_number,
  pov.end_date_active inactive_date,
  povs.vendor_site_code supplier_site_name,
  DECODE (povs.ship_via_lookup_code,'''', pov.ship_via_lookup_code,povs.ship_via_lookup_code) ship_via,
  plc.lookup_code supplier_type,
  hrl.location_code ship_to_location,
  HRL1.LOCATION_CODE BILL_TO_LOCATION,
  (povs.address_line1 ||'', ''|| povs.city ||'' ,''||povs.state||'', ''||povs.zip ||'', ''|| povs.country) site_address,
  DECODE (povs.pay_group_lookup_code, '''', pov.pay_group_lookup_code, povs.pay_group_lookup_code ) pay_group,
  DECODE (apt1.NAME, '''', apt.NAME, apt1.NAME) payment_terms,
  DECODE (povs.invoice_currency_code, '''', pov.invoice_currency_code, povs.invoice_currency_code ) invoice_currency,
  DECODE (povs.payment_currency_code, '''', pov.payment_currency_code, povs.payment_currency_code ) payment_currency,
  DECODE (povs.payment_method_lookup_code, '''', pov.payment_method_lookup_code, povs.payment_method_lookup_code ) PAYMENT_METHOD,
  pov.vendor_id,
  povs.vendor_site_id,
  povs.org_id
from
  AP_SUPPLIERS POV,
  AP_SUPPLIER_sites povs,
  po_lookup_codes plc,
  ap_terms_vl apt,
  ap_terms_vl apt1,
  hr_locations hrl,
  hr_locations hrl1,
  hr_operating_units hou
WHERE 1                                 =1
and POV.VENDOR_ID                       = POVS.VENDOR_ID(+)
AND pov.vendor_type_lookup_code         = plc.lookup_code(+)
AND plc.lookup_type(+)                  = ''VENDOR TYPE''
AND pov.terms_id                        = apt.term_id(+)
AND povs.terms_id                       = apt1.term_id(+)
AND povs.ship_to_location_id            = hrl.location_id(+)
AND POVS.BILL_TO_LOCATION_ID            = HRL1.LOCATION_ID(+)
and povs.org_id                         = hou.organization_id
and povs.org_id = fnd_profile.value(''ORG_ID'')','Y','Y','Multiple','','');
--Delete existing EIS AP Supplier Information Query data 
xxeis.eis_rsc_insert.delete_query_rows('EIS AP Supplier Information Query','');
--Exporting Query Columns 
xxeis.eis_rsc_ins.qc('EIS AP Supplier Information Query','INVOICE_CURRENCY','Invoice Currency','','VARCHAR2','13','N','N','Y','Y','','','');
xxeis.eis_rsc_ins.qc('EIS AP Supplier Information Query','PAYMENT_CURRENCY','Payment Currency','','VARCHAR2','14','N','N','Y','Y','','','');
xxeis.eis_rsc_ins.qc('EIS AP Supplier Information Query','PAYMENT_METHOD','Payment Method','','VARCHAR2','11','N','N','Y','Y','','','');
xxeis.eis_rsc_ins.qc('EIS AP Supplier Information Query','VENDOR_ID','Vendor Id','','NUMBER','15','N','N','N','Y','~~~','N','');
xxeis.eis_rsc_ins.qc('EIS AP Supplier Information Query','VENDOR_SITE_ID','Vendor Site Id','','NUMBER','16','N','N','N','Y','~~~','N','');
xxeis.eis_rsc_ins.qc('EIS AP Supplier Information Query','ORG_ID','Org Id','','NUMBER','17','N','N','N','Y','~~~','N','');
xxeis.eis_rsc_ins.qc('EIS AP Supplier Information Query','OPERATING_UNIT','Operating Unit','','VARCHAR2','1','N','N','Y','Y','','','');
xxeis.eis_rsc_ins.qc('EIS AP Supplier Information Query','SUPPLIER_NAME','Supplier Name','','VARCHAR2','2','N','N','Y','Y','','','');
xxeis.eis_rsc_ins.qc('EIS AP Supplier Information Query','SUPPLIER_NUMBER','Supplier Number','','VARCHAR2','3','N','N','Y','Y','','','');
xxeis.eis_rsc_ins.qc('EIS AP Supplier Information Query','INACTIVE_DATE','Inactive Date','','DATE','18','N','N','N','Y','','','');
xxeis.eis_rsc_ins.qc('EIS AP Supplier Information Query','SUPPLIER_SITE_NAME','Supplier Site Name','','VARCHAR2','4','N','N','Y','Y','','','');
xxeis.eis_rsc_ins.qc('EIS AP Supplier Information Query','SHIP_VIA','Ship Via','','VARCHAR2','8','N','N','N','Y','','','');
xxeis.eis_rsc_ins.qc('EIS AP Supplier Information Query','SUPPLIER_TYPE','Supplier Type','','VARCHAR2','6','N','N','Y','Y','','','');
xxeis.eis_rsc_ins.qc('EIS AP Supplier Information Query','SHIP_TO_LOCATION','Ship To Location','','VARCHAR2','7','N','N','Y','Y','','','');
xxeis.eis_rsc_ins.qc('EIS AP Supplier Information Query','BILL_TO_LOCATION','Bill To Location','','VARCHAR2','9','N','N','Y','Y','','','');
xxeis.eis_rsc_ins.qc('EIS AP Supplier Information Query','SITE_ADDRESS','Site Address','','VARCHAR2','5','N','N','N','Y','','','');
xxeis.eis_rsc_ins.qc('EIS AP Supplier Information Query','PAY_GROUP','Pay Group','','VARCHAR2','12','N','N','N','Y','','','');
xxeis.eis_rsc_ins.qc('EIS AP Supplier Information Query','PAYMENT_TERMS','Payment Terms','','VARCHAR2','10','N','N','Y','Y','','','');
--Exporting Query Parameters 
xxeis.eis_rsc_ins.qp('EIS AP Supplier Information Query','Vendor Id','VENDOR_ID','NUMBER','=','1','Y','Y','N','');
--Exporting Query Filters 
xxeis.eis_rsc_ins.qf('EIS AP Supplier Information Query','VENDOR_ID','','','Vendor Id','','','=','','1','Y','','');
--Exporting Layout for EIS AP Supplier Information Query
xxeis.eis_rsc_ins.ql('EIS AP Supplier Information Query','Supplier Information','1','','');
--Exporting Layout Columns for EIS AP Supplier Information Query
xxeis.eis_rsc_ins.qlc('EIS AP Supplier Information Query','SUPPLIER_TYPE','0','L','5','');
xxeis.eis_rsc_ins.qlc('EIS AP Supplier Information Query','SHIP_TO_LOCATION','0','L','6','');
xxeis.eis_rsc_ins.qlc('EIS AP Supplier Information Query','BILL_TO_LOCATION','0','L','7','');
xxeis.eis_rsc_ins.qlc('EIS AP Supplier Information Query','PAYMENT_TERMS','0','L','8','');
xxeis.eis_rsc_ins.qlc('EIS AP Supplier Information Query','INVOICE_CURRENCY','0','L','9','');
xxeis.eis_rsc_ins.qlc('EIS AP Supplier Information Query','PAYMENT_CURRENCY','0','L','10','');
xxeis.eis_rsc_ins.qlc('EIS AP Supplier Information Query','PAYMENT_METHOD','0','L','11','');
xxeis.eis_rsc_ins.qlc('EIS AP Supplier Information Query','OPERATING_UNIT','0','L','1','');
xxeis.eis_rsc_ins.qlc('EIS AP Supplier Information Query','SUPPLIER_NAME','0','L','2','');
xxeis.eis_rsc_ins.qlc('EIS AP Supplier Information Query','SUPPLIER_NUMBER','0','L','3','');
xxeis.eis_rsc_ins.qlc('EIS AP Supplier Information Query','SUPPLIER_SITE_NAME','0','L','4','');
--End Exporting Query EIS AP Supplier Information Query
 
xxeis.eis_rsc_ins.qtd('EIS AP Supplier Information Template','EIS AP Supplier Information Query','1','');
xxeis.eis_rsc_ins.rdq( 'HDS Discounts Taken and Lost','SUPPLIER','EIS AP Supplier Information Template','','3','T');
xxeis.eis_rsc_ins.rdqd( 'HDS Discounts Taken and Lost','SUPPLIER','EIS AP Supplier Information Template','T','EXADTLV.VENDOR_ID',':Vendor Id');
xxeis.eis_rsc_ins.rc( 'HDS Discounts Taken and Lost',200,'INVOICE_NUM','Invoice Num','Invoice number','','','','','6','N','','','','','','','','AB063501','N','N','','EIS_XXHDS_AP_DISC_TAKEN_LOST_V','','','','US','T');
 -- Exporting Template EIS AP Invoice Information Template
xxeis.eis_rsc_ins.qt('EIS AP Invoice Information Template','EIS AP Invoice Information Template','Y','','Y');
--Delete existing EIS AP Invoice Information Template data 
xxeis.eis_rsc_insert.delete_template_rows('EIS AP Invoice Information Template','');
 -- Exporting Template Common Params for EIS AP Invoice Information Template
xxeis.eis_rsc_ins.qtp('EIS AP Invoice Information Template','Invoice Id','');
 -- Exporting Template Details for Template => EIS AP Invoice Information Template
xxeis.eis_rsc_ins.q('EIS AP Invoice Drilldown Query','EIS AP Invoice Drilldown Query','SELECT AI.INVOICE_ID,
       HOU.NAME OPERATING_UNIT,
       ASP.VENDOR_NAME SUPPLIER_NAME,
       SS.vendor_site_code supplier_site,
       AI.INVOICE_NUM INVOICE_NUMBER,
       AI.INVOICE_TYPE_LOOKUP_CODE INVOICE_TYPE,
       AI.INVOICE_DATE INVOICE_DATE,
       AI.INVOICE_CURRENCY_CODE,
       AI.INVOICE_AMOUNT INVOICE_AMOUNT,
       DECODE(ASP.MATCH_OPTION,''P'',''Purchase Order'',''R'',''Receipt'',''I'',''Invoice'',NULL) Matching_Type
  FROM AP_INVOICES ai,
       AP_SUPPLIERS ASP,
       AP_SUPPLIER_SITES SS,
       HR_OPERATING_UNITS HOU
 WHERE AI.VENDOR_ID  = ASP.VENDOR_ID
   and AI.VENDOR_SITE_ID = SS.VENDOR_SITE_ID
   AND AI.ORG_ID = HOU.ORGANIZATION_ID','Y','Y','Single','','');
--Delete existing EIS AP Invoice Drilldown Query data 
xxeis.eis_rsc_insert.delete_query_rows('EIS AP Invoice Drilldown Query','');
--Exporting Query Columns 
xxeis.eis_rsc_ins.qc('EIS AP Invoice Drilldown Query','INVOICE_ID','Invoice Id','','NUMBER','1','N','N','N','Y','~~~','N','');
xxeis.eis_rsc_ins.qc('EIS AP Invoice Drilldown Query','OPERATING_UNIT','Operating Unit','','VARCHAR2','2','N','N','Y','Y','','','');
xxeis.eis_rsc_ins.qc('EIS AP Invoice Drilldown Query','SUPPLIER_NAME','Supplier Name','','VARCHAR2','3','N','N','Y','Y','','','');
xxeis.eis_rsc_ins.qc('EIS AP Invoice Drilldown Query','SUPPLIER_SITE','Supplier Site','','VARCHAR2','4','N','N','Y','Y','','','');
xxeis.eis_rsc_ins.qc('EIS AP Invoice Drilldown Query','INVOICE_NUMBER','Invoice Number','','VARCHAR2','5','N','N','Y','Y','','','');
xxeis.eis_rsc_ins.qc('EIS AP Invoice Drilldown Query','INVOICE_TYPE','Invoice Type','','VARCHAR2','7','N','N','Y','Y','','','');
xxeis.eis_rsc_ins.qc('EIS AP Invoice Drilldown Query','INVOICE_DATE','Invoice Date','','DATE','6','N','N','Y','Y','','','');
xxeis.eis_rsc_ins.qc('EIS AP Invoice Drilldown Query','INVOICE_CURRENCY_CODE','Invoice Currency Code','','VARCHAR2','8','N','N','Y','Y','','','');
xxeis.eis_rsc_ins.qc('EIS AP Invoice Drilldown Query','INVOICE_AMOUNT','Invoice Amount','','NUMBER','9','N','N','Y','Y','~T~D~2','N','');
xxeis.eis_rsc_ins.qc('EIS AP Invoice Drilldown Query','MATCHING_TYPE','Matching Type','','VARCHAR2','10','N','N','Y','Y','','','');
--Exporting Query Parameters 
xxeis.eis_rsc_ins.qp('EIS AP Invoice Drilldown Query','Invoice Id','INVOICE_ID','NUMBER','=','1','Y','Y','N','');
--Exporting Query Filters 
xxeis.eis_rsc_ins.qf('EIS AP Invoice Drilldown Query','INVOICE_ID','','','Invoice Id','','','=','','1','Y','','');
--Exporting Layout for EIS AP Invoice Drilldown Query
xxeis.eis_rsc_ins.ql('EIS AP Invoice Drilldown Query','Invoice Information','2','','');
--Exporting Layout Columns for EIS AP Invoice Drilldown Query
xxeis.eis_rsc_ins.qlc('EIS AP Invoice Drilldown Query','SUPPLIER_SITE','2','L','3','');
xxeis.eis_rsc_ins.qlc('EIS AP Invoice Drilldown Query','INVOICE_NUMBER','1','L','2','');
xxeis.eis_rsc_ins.qlc('EIS AP Invoice Drilldown Query','INVOICE_TYPE','2','L','1','');
xxeis.eis_rsc_ins.qlc('EIS AP Invoice Drilldown Query','INVOICE_DATE','1','L','3','');
xxeis.eis_rsc_ins.qlc('EIS AP Invoice Drilldown Query','INVOICE_CURRENCY_CODE','1','L','4','');
xxeis.eis_rsc_ins.qlc('EIS AP Invoice Drilldown Query','INVOICE_AMOUNT','1','L','5','');
xxeis.eis_rsc_ins.qlc('EIS AP Invoice Drilldown Query','MATCHING_TYPE','2','L','4','');
xxeis.eis_rsc_ins.qlc('EIS AP Invoice Drilldown Query','OPERATING_UNIT','1','L','1','');
xxeis.eis_rsc_ins.qlc('EIS AP Invoice Drilldown Query','SUPPLIER_NAME','2','L','2','');
--End Exporting Query EIS AP Invoice Drilldown Query
 
xxeis.eis_rsc_ins.qtd('EIS AP Invoice Information Template','EIS AP Invoice Drilldown Query','1','');
xxeis.eis_rsc_ins.q('EIS AP Payment Drilldown Query','EIS AP Payment Drilldown Query','SELECT AI.INVOICE_ID,
       AC.CHECK_NUMBER,
       AC.CHECK_DATE CHECK_DATE,
       ATV.name PAYMENT_TERMS,
       IBY1.PAYMENT_METHOD_NAME Payment_Method,
       ac.bank_account_name bank_name,
       alc.displayed_field Payment_Status,
       ALC1.DISPLAYED_FIELD PAYMENT_DOCUMENT_TYPE,
       aip.amount payment_amount
  FROM AP_INVOICES ai,
       AP_INVOICE_PAYMENTS AIP,
       AP_CHECKS AC,
       AP_TERMS_VL ATV,
       AP_LOOKUP_CODES ALC,
       AP_LOOKUP_CODES ALC1,
       iby_payment_methods_vl iby1
 WHERE ai.INVOICE_ID = aip.INVOICE_ID
   and AIP.CHECK_ID = AC.CHECK_ID
   AND AI.TERMS_ID   = ATV.TERM_ID(+)
   and ai.payment_status_flag  = alc.lookup_code
   AND ALC.LOOKUP_TYPE =''INVOICE PAYMENT STATUS''
   and ac.payment_type_flag = alc1.lookup_code
   and ALC1.LOOKUP_TYPE=''PAYMENT TYPE''
   and AI.PAYMENT_METHOD_CODE = IBY1.PAYMENT_METHOD_CODE (+)','Y','Y','Multiple','','');
--Delete existing EIS AP Payment Drilldown Query data 
xxeis.eis_rsc_insert.delete_query_rows('EIS AP Payment Drilldown Query','');
--Exporting Query Columns 
xxeis.eis_rsc_ins.qc('EIS AP Payment Drilldown Query','PAYMENT_AMOUNT','Payment Amount','','NUMBER','9','N','N','Y','Y','~T~D~2','Y','');
xxeis.eis_rsc_ins.qc('EIS AP Payment Drilldown Query','INVOICE_ID','Invoice Id','','NUMBER','1','N','N','N','Y','~~~','N','');
xxeis.eis_rsc_ins.qc('EIS AP Payment Drilldown Query','CHECK_NUMBER','Check Number','','NUMBER','2','N','N','Y','Y','~~~','N','');
xxeis.eis_rsc_ins.qc('EIS AP Payment Drilldown Query','CHECK_DATE','Check Date','','DATE','3','N','N','Y','Y','','','');
xxeis.eis_rsc_ins.qc('EIS AP Payment Drilldown Query','PAYMENT_TERMS','Payment Terms','','VARCHAR2','6','N','N','Y','Y','','','');
xxeis.eis_rsc_ins.qc('EIS AP Payment Drilldown Query','PAYMENT_METHOD','Payment Method','','VARCHAR2','5','N','N','Y','Y','','','');
xxeis.eis_rsc_ins.qc('EIS AP Payment Drilldown Query','BANK_NAME','Bank Name','','VARCHAR2','8','N','N','Y','Y','','','');
xxeis.eis_rsc_ins.qc('EIS AP Payment Drilldown Query','PAYMENT_STATUS','Payment Status','','VARCHAR2','7','N','N','Y','Y','','','');
xxeis.eis_rsc_ins.qc('EIS AP Payment Drilldown Query','PAYMENT_DOCUMENT_TYPE','Payment Document Type','','VARCHAR2','4','N','N','Y','Y','','','');
--Exporting Query Parameters 
xxeis.eis_rsc_ins.qp('EIS AP Payment Drilldown Query','Invoice Id','INVOICE_ID','NUMBER','=','1','Y','Y','N','');
--Exporting Query Filters 
xxeis.eis_rsc_ins.qf('EIS AP Payment Drilldown Query','INVOICE_ID','','','Invoice Id','','','=','','1','Y','','');
--Exporting Layout for EIS AP Payment Drilldown Query
xxeis.eis_rsc_ins.ql('EIS AP Payment Drilldown Query','Payment Information','1','','');
--Exporting Layout Columns for EIS AP Payment Drilldown Query
xxeis.eis_rsc_ins.qlc('EIS AP Payment Drilldown Query','PAYMENT_AMOUNT','0','L','8','');
xxeis.eis_rsc_ins.qlc('EIS AP Payment Drilldown Query','PAYMENT_TERMS','0','L','3','');
xxeis.eis_rsc_ins.qlc('EIS AP Payment Drilldown Query','PAYMENT_METHOD','0','L','4','');
xxeis.eis_rsc_ins.qlc('EIS AP Payment Drilldown Query','BANK_NAME','0','L','6','');
xxeis.eis_rsc_ins.qlc('EIS AP Payment Drilldown Query','PAYMENT_STATUS','0','L','7','');
xxeis.eis_rsc_ins.qlc('EIS AP Payment Drilldown Query','PAYMENT_DOCUMENT_TYPE','0','L','5','');
xxeis.eis_rsc_ins.qlc('EIS AP Payment Drilldown Query','CHECK_NUMBER','0','L','1','');
xxeis.eis_rsc_ins.qlc('EIS AP Payment Drilldown Query','CHECK_DATE','0','L','2','');
--End Exporting Query EIS AP Payment Drilldown Query
 
xxeis.eis_rsc_ins.qtd('EIS AP Invoice Information Template','EIS AP Payment Drilldown Query','4','');
xxeis.eis_rsc_ins.q('EIS AP Distribution Drilldown Query','EIS AP Distribution Drilldown Query','SELECT AI.INVOICE_ID,
       POH.SEGMENT1 PO_NUM,
       GCCK1.CONCATENATED_SEGMENTS EXPENSE_ACCOUNT,
       GCCK2.CONCATENATED_SEGMENTS LIABILITY_ACCOUNT,
       DECODE (AID.ASSETS_TRACKING_FLAG,''Y'',''Yes'',''N'',''No'') TRACKING_ASSET_FLAG,
       ALC1.DISPLAYED_FIELD INVOICING_STATUS,
       ALC2.DISPLAYED_FIELD ACCOUNTING_STATUS,
       ADS.distribution_set_name,
       AID.INVOICE_LINE_NUMBER,
       AID.DISTRIBUTION_LINE_NUMBER,
       AID.AMOUNT
        FROM AP_INVOICES AI,
       ap_distribution_sets ads,
       AP_INVOICE_DISTRIBUTIONS AID,
       PO_DISTRIBUTIONS POD,
       PO_HEADERS POH ,
       GL_CODE_COMBINATIONS_KFV GCCK1,
       gl_code_combinations_kfv gcck2,
       AP_LOOKUP_CODES ALC1,
       AP_LOOKUP_CODES ALC2
 WHERE AI.DISTRIBUTION_SET_ID = ADS.DISTRIBUTION_SET_ID(+)
   and AI.INVOICE_ID=AID.INVOICE_ID
   and AID.PO_DISTRIBUTION_ID=POD.PO_DISTRIBUTION_ID(+)
   and POD.PO_HEADER_ID =POH.PO_HEADER_ID(+)
   and AI.ACCTS_PAY_CODE_COMBINATION_ID = GCCK2.CODE_COMBINATION_ID(+)
   and AID.DIST_CODE_COMBINATION_ID =GCCK1.CODE_COMBINATION_ID
   and ALC1.LOOKUP_TYPE (+) = ''NLS TRANSLATION''
   and ALC1.LOOKUP_CODE (+) = DECODE(AID.MATCH_STATUS_FLAG , null,''NEVER APPROVED'', ''N'', ''NEVER APPROVED'', ''T'', ''NEEDS REAPPROVAL'', ''A'', ''APPROVED'', ''S'', ''NEVER APPROVED'')
   and ALC2.LOOKUP_TYPE (+) = ''POSTING STATUS''
   and ALC2.LOOKUP_CODE (+) = AID.POSTED_FLAG
order by AID.INVOICE_LINE_NUMBER,aid.distribution_line_number','Y','Y','Multiple','','');
--Delete existing EIS AP Distribution Drilldown Query data 
xxeis.eis_rsc_insert.delete_query_rows('EIS AP Distribution Drilldown Query','');
--Exporting Query Columns 
xxeis.eis_rsc_ins.qc('EIS AP Distribution Drilldown Query','INVOICE_LINE_NUMBER','Invoice Line Number','','NUMBER','9','N','N','Y','Y','~~~','N','');
xxeis.eis_rsc_ins.qc('EIS AP Distribution Drilldown Query','AMOUNT','Amount','','NUMBER','10','N','N','Y','Y','~T~D~2','Y','');
xxeis.eis_rsc_ins.qc('EIS AP Distribution Drilldown Query','DISTRIBUTION_LINE_NUMBER','Distribution Line Number','','NUMBER','11','N','N','Y','Y','~~~','N','');
xxeis.eis_rsc_ins.qc('EIS AP Distribution Drilldown Query','INVOICE_ID','Invoice Id','','NUMBER','1','N','N','N','Y','~~~','N','');
xxeis.eis_rsc_ins.qc('EIS AP Distribution Drilldown Query','PO_NUM','PO Number','','VARCHAR2','2','N','N','Y','Y','','','');
xxeis.eis_rsc_ins.qc('EIS AP Distribution Drilldown Query','EXPENSE_ACCOUNT','Expense Account','','VARCHAR2','3','N','N','Y','Y','','','');
xxeis.eis_rsc_ins.qc('EIS AP Distribution Drilldown Query','LIABILITY_ACCOUNT','Liability Account','','VARCHAR2','4','N','N','Y','Y','','','');
xxeis.eis_rsc_ins.qc('EIS AP Distribution Drilldown Query','TRACKING_ASSET_FLAG','Tracking Asset Flag','','VARCHAR2','7','N','N','Y','Y','','','');
xxeis.eis_rsc_ins.qc('EIS AP Distribution Drilldown Query','INVOICING_STATUS','Invoicing Status','','VARCHAR2','5','N','N','Y','Y','','','');
xxeis.eis_rsc_ins.qc('EIS AP Distribution Drilldown Query','ACCOUNTING_STATUS','Accounting Status','','VARCHAR2','6','N','N','Y','Y','','','');
xxeis.eis_rsc_ins.qc('EIS AP Distribution Drilldown Query','DISTRIBUTION_SET_NAME','Distribution Set Name','','VARCHAR2','8','N','N','Y','Y','','','');
--Exporting Query Parameters 
xxeis.eis_rsc_ins.qp('EIS AP Distribution Drilldown Query','Invoice Id','INVOICE_ID','NUMBER','=','1','Y','Y','N','');
--Exporting Query Filters 
xxeis.eis_rsc_ins.qf('EIS AP Distribution Drilldown Query','INVOICE_ID','','','Invoice Id','','','=','','1','Y','','');
--Exporting Layout for EIS AP Distribution Drilldown Query
xxeis.eis_rsc_ins.ql('EIS AP Distribution Drilldown Query','Distribution Information','1','','');
--Exporting Layout Columns for EIS AP Distribution Drilldown Query
xxeis.eis_rsc_ins.qlc('EIS AP Distribution Drilldown Query','DISTRIBUTION_LINE_NUMBER','0','L','2','');
xxeis.eis_rsc_ins.qlc('EIS AP Distribution Drilldown Query','PO_NUM','0','L','3','');
xxeis.eis_rsc_ins.qlc('EIS AP Distribution Drilldown Query','EXPENSE_ACCOUNT','0','L','4','');
xxeis.eis_rsc_ins.qlc('EIS AP Distribution Drilldown Query','LIABILITY_ACCOUNT','0','L','5','');
xxeis.eis_rsc_ins.qlc('EIS AP Distribution Drilldown Query','TRACKING_ASSET_FLAG','0','L','6','');
xxeis.eis_rsc_ins.qlc('EIS AP Distribution Drilldown Query','INVOICING_STATUS','0','L','7','');
xxeis.eis_rsc_ins.qlc('EIS AP Distribution Drilldown Query','ACCOUNTING_STATUS','0','L','8','');
xxeis.eis_rsc_ins.qlc('EIS AP Distribution Drilldown Query','DISTRIBUTION_SET_NAME','0','L','9','');
xxeis.eis_rsc_ins.qlc('EIS AP Distribution Drilldown Query','INVOICE_LINE_NUMBER','0','L','1','');
xxeis.eis_rsc_ins.qlc('EIS AP Distribution Drilldown Query','AMOUNT','0','L','10','');
--End Exporting Query EIS AP Distribution Drilldown Query
 
xxeis.eis_rsc_ins.qtd('EIS AP Invoice Information Template','EIS AP Distribution Drilldown Query','3','');
xxeis.eis_rsc_ins.q('EIS AP Line Drilldown Query','EIS AP Line Drilldown Query','SELECT AI.INVOICE_ID,
 AIL.LINE_TYPE_LOOKUP_CODE,
  ail.DESCRIPTION,
  ai.gl_date GL_DATE,
  AIL.QUANTITY_INVOICED QUANTITY_INVOICED,
  AIL.AMOUNT LINE_AMOUNT,
  abs(TAX.AMOUNT) WITHHOLDING_TAX,
  ail.line_number
FROM AP_INVOICES AI,
  ap_invoice_lines ail,
  (SELECT apid.invoice_id,
    APID.AMOUNT,
    apid.invoice_line_number,
    APID.LINE_TYPE_LOOKUP_CODE
  FROM ap_invoice_distributions apid,
    ap_tax_codes aptc,
    ap_awt_tax_rates apt
  WHERE apid.withholding_tax_code_id = aptc.tax_id(+)
  AND apid.awt_tax_rate_id           = apt.tax_rate_id(+)
  AND apid.line_type_lookup_code(+)  = ''AWT''
  ) tax
WHERE AI.INVOICE_ID                                          =AIL.INVOICE_ID(+)
AND ail.invoice_id                                           = tax.invoice_id(+)
AND ail.line_number                                          = tax.invoice_line_number(+)
AND ail.line_type_lookup_code                                = tax.line_type_lookup_code(+)
order by ail.line_number','Y','Y','Multiple','','');
--Delete existing EIS AP Line Drilldown Query data 
xxeis.eis_rsc_insert.delete_query_rows('EIS AP Line Drilldown Query','');
--Exporting Query Columns 
xxeis.eis_rsc_ins.qc('EIS AP Line Drilldown Query','LINE_NUMBER','Line Number','','NUMBER','8','N','N','Y','Y','~~~','N','');
xxeis.eis_rsc_ins.qc('EIS AP Line Drilldown Query','INVOICE_ID','Invoice Id','','NUMBER','1','N','N','N','Y','~~~','N','');
xxeis.eis_rsc_ins.qc('EIS AP Line Drilldown Query','LINE_TYPE_LOOKUP_CODE','Line Type','','VARCHAR2','2','N','N','Y','Y','','','');
xxeis.eis_rsc_ins.qc('EIS AP Line Drilldown Query','DESCRIPTION','Description','','VARCHAR2','3','N','N','Y','Y','','','');
xxeis.eis_rsc_ins.qc('EIS AP Line Drilldown Query','GL_DATE','GL Date','','DATE','4','N','N','Y','Y','','','');
xxeis.eis_rsc_ins.qc('EIS AP Line Drilldown Query','QUANTITY_INVOICED','Quantity Invoiced','','NUMBER','6','N','N','Y','Y','~T~~','N','');
xxeis.eis_rsc_ins.qc('EIS AP Line Drilldown Query','LINE_AMOUNT','Line Amount','','NUMBER','5','N','N','Y','Y','~T~D~2','Y','');
xxeis.eis_rsc_ins.qc('EIS AP Line Drilldown Query','WITHHOLDING_TAX','Withholding Tax','','NUMBER','7','N','N','Y','Y','~T~D~2','N','');
--Exporting Query Parameters 
xxeis.eis_rsc_ins.qp('EIS AP Line Drilldown Query','Invoice Id','INVOICE_ID','NUMBER','=','1','Y','Y','N','');
--Exporting Query Filters 
xxeis.eis_rsc_ins.qf('EIS AP Line Drilldown Query','INVOICE_ID','','','Invoice Id','','','=','','1','Y','','');
--Exporting Layout for EIS AP Line Drilldown Query
xxeis.eis_rsc_ins.ql('EIS AP Line Drilldown Query','Line Information','1','','');
--Exporting Layout Columns for EIS AP Line Drilldown Query
xxeis.eis_rsc_ins.qlc('EIS AP Line Drilldown Query','LINE_NUMBER','0','L','1','');
xxeis.eis_rsc_ins.qlc('EIS AP Line Drilldown Query','LINE_TYPE_LOOKUP_CODE','0','L','2','');
xxeis.eis_rsc_ins.qlc('EIS AP Line Drilldown Query','DESCRIPTION','0','L','3','');
xxeis.eis_rsc_ins.qlc('EIS AP Line Drilldown Query','GL_DATE','0','L','4','');
xxeis.eis_rsc_ins.qlc('EIS AP Line Drilldown Query','WITHHOLDING_TAX','0','L','7','');
xxeis.eis_rsc_ins.qlc('EIS AP Line Drilldown Query','QUANTITY_INVOICED','0','L','5','');
xxeis.eis_rsc_ins.qlc('EIS AP Line Drilldown Query','LINE_AMOUNT','0','L','6','');
--End Exporting Query EIS AP Line Drilldown Query
 
xxeis.eis_rsc_ins.qtd('EIS AP Invoice Information Template','EIS AP Line Drilldown Query','2','');
xxeis.eis_rsc_ins.rdq( 'HDS Discounts Taken and Lost','INVOICE_NUM','EIS AP Invoice Information Template','','6','T');
xxeis.eis_rsc_ins.rdqd( 'HDS Discounts Taken and Lost','INVOICE_NUM','EIS AP Invoice Information Template','T','EXADTLV.INVOICE_ID',':Invoice Id');
xxeis.eis_rsc_ins.rc( 'HDS Discounts Taken and Lost',200,'BASE_CURR_DISCOUNT_TAKEN','Total discount taken','Base Curr Discount Taken','','','','','15','N','','','','','','','','AB063501','N','N','','EIS_XXHDS_AP_DISC_TAKEN_LOST_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Discounts Taken and Lost',200,'CHECK_DATE','Check Date','Payment date','','','','','17','N','','','','','','','','AB063501','N','N','','EIS_XXHDS_AP_DISC_TAKEN_LOST_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Discounts Taken and Lost',200,'INVOICE_CURRENCY_CODE','Invoice Currency Code','Currency code of invoice','','','','','7','N','','','','','','','','AB063501','N','N','','EIS_XXHDS_AP_DISC_TAKEN_LOST_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Discounts Taken and Lost',200,'OPERATING_UNIT','Operating Unit','Operating Unit','','','','','2','N','','','','','','','','AB063501','N','N','','EIS_XXHDS_AP_DISC_TAKEN_LOST_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Discounts Taken and Lost',200,'SUPPLIER_SITE','Supplier Site','Site code name','','','','','5','N','','','','','','','','AB063501','N','N','','EIS_XXHDS_AP_DISC_TAKEN_LOST_V','','','','US','');
--Inserting Report Parameters - HDS Discounts Taken and Lost
xxeis.eis_rsc_ins.rp( 'HDS Discounts Taken and Lost',200,'Supplier','Supplier Name','SUPPLIER','IN','AP Vendor Names LOV','','VARCHAR2','N','Y','4','Y','Y','CONSTANT','AB063501','Y','N','','','','EIS_XXHDS_AP_DISC_TAKEN_LOST_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS Discounts Taken and Lost',200,'Invoice Number From','Invoice Number From','INVOICE_NUM','>=','AP Invoice Numbers LOV','','VARCHAR2','N','Y','5','N','Y','CONSTANT','AB063501','Y','N','','','','EIS_XXHDS_AP_DISC_TAKEN_LOST_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS Discounts Taken and Lost',200,'Invoice Number To','Invoice Number To','INVOICE_NUM','<=','AP Invoice Numbers LOV','','VARCHAR2','N','Y','6','N','Y','CONSTANT','AB063501','Y','N','','','','EIS_XXHDS_AP_DISC_TAKEN_LOST_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS Discounts Taken and Lost',200,'Payment Start Date','Payment Start Date','CHECK_DATE','>=','','','DATE','N','Y','2','N','Y','CONSTANT','AB063501','Y','N','','Start Date','','EIS_XXHDS_AP_DISC_TAKEN_LOST_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS Discounts Taken and Lost',200,'Payment End Date','Payment End Date','CHECK_DATE','<=','','','DATE','N','Y','3','N','Y','CONSTANT','AB063501','Y','N','','End Date','','EIS_XXHDS_AP_DISC_TAKEN_LOST_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS Discounts Taken and Lost',200,'Operating Unit','Operating Unit','OPERATING_UNIT','IN','AP Multi Operating Unit LOV','','VARCHAR2','N','Y','1','Y','Y','CONSTANT','AB063501','Y','N','','','','EIS_XXHDS_AP_DISC_TAKEN_LOST_V','','','US','');
--Inserting Dependent Parameters - HDS Discounts Taken and Lost
--Inserting Report Conditions - HDS Discounts Taken and Lost
xxeis.eis_rsc_ins.rcnh( 'HDS Discounts Taken and Lost',200,'INVOICE_NUM >= :Invoice Number From ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','INVOICE_NUM','','Invoice Number From','','','','','EIS_XXHDS_AP_DISC_TAKEN_LOST_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',200,'HDS Discounts Taken and Lost','INVOICE_NUM >= :Invoice Number From ');
xxeis.eis_rsc_ins.rcnh( 'HDS Discounts Taken and Lost',200,'OPERATING_UNIT IN :Operating Unit ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','OPERATING_UNIT','','Operating Unit','','','','','EIS_XXHDS_AP_DISC_TAKEN_LOST_V','','','','','','IN','Y','Y','','','','','1',200,'HDS Discounts Taken and Lost','OPERATING_UNIT IN :Operating Unit ');
xxeis.eis_rsc_ins.rcnh( 'HDS Discounts Taken and Lost',200,'SUPPLIER IN :Supplier ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','SUPPLIER','','Supplier','','','','','EIS_XXHDS_AP_DISC_TAKEN_LOST_V','','','','','','IN','Y','Y','','','','','1',200,'HDS Discounts Taken and Lost','SUPPLIER IN :Supplier ');
xxeis.eis_rsc_ins.rcnh( 'HDS Discounts Taken and Lost',200,'EADTLV.CHECK_DATE >= Payment Start Date','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CHECK_DATE','','Payment Start Date','','','','','EIS_XXHDS_AP_DISC_TAKEN_LOST_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',200,'HDS Discounts Taken and Lost','EADTLV.CHECK_DATE >= Payment Start Date');
xxeis.eis_rsc_ins.rcnh( 'HDS Discounts Taken and Lost',200,'EADTLV.CHECK_DATE <= Payment End Date','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CHECK_DATE','','Payment End Date','','','','','EIS_XXHDS_AP_DISC_TAKEN_LOST_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',200,'HDS Discounts Taken and Lost','EADTLV.CHECK_DATE <= Payment End Date');
xxeis.eis_rsc_ins.rcnh( 'HDS Discounts Taken and Lost',200,'EADTLV.INVOICE_NUM <= Invoice Number To','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','INVOICE_NUM','','Invoice Number To','','','','','EIS_XXHDS_AP_DISC_TAKEN_LOST_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',200,'HDS Discounts Taken and Lost','EADTLV.INVOICE_NUM <= Invoice Number To');
--Inserting Report Sorts - HDS Discounts Taken and Lost
xxeis.eis_rsc_ins.rs( 'HDS Discounts Taken and Lost',200,'SUPPLIER','ASC','AB063501','2','');
xxeis.eis_rsc_ins.rs( 'HDS Discounts Taken and Lost',200,'INVOICE_NUM','ASC','AB063501','1','');
--Inserting Report Triggers - HDS Discounts Taken and Lost
--inserting report templates - HDS Discounts Taken and Lost
xxeis.eis_rsc_ins.r_tem( 'HDS Discounts Taken and Lost','AP Discounts Taken and Lost Report','Seeded template for AP Discounts Taken and Lost Report','','','','','','','','','','','Discounts Taken and Lost.rtf','AB063501','X','','','Y','Y','','');
xxeis.eis_rsc_ins.r_tem( 'HDS Discounts Taken and Lost','Discounts Taken and Lost','Seeded template for Discounts Taken and Lost','','','','','','','','','','','Discounts Taken and Lost.rtf','AB063501','X','','','Y','Y',' ',' ');
--Inserting Report Portals - HDS Discounts Taken and Lost
--inserting report dashboards - HDS Discounts Taken and Lost
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'HDS Discounts Taken and Lost','200','EIS_XXHDS_AP_DISC_TAKEN_LOST_V','EIS_XXHDS_AP_DISC_TAKEN_LOST_V','N','');
--inserting report security - HDS Discounts Taken and Lost
xxeis.eis_rsc_ins.rsec( 'HDS Discounts Taken and Lost','200','','HDS_AP_ADMIN_US_IWO',200,'AB063501','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Discounts Taken and Lost','200','','HDS_AP_ADMIN_US_GSCIWO',200,'AB063501','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Discounts Taken and Lost','200','','HDS_AP_ADMIN_US_GSC',200,'AB063501','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Discounts Taken and Lost','200','','XXWC_PAY_DISBURSE',200,'AB063501','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Discounts Taken and Lost','200','','XXWC_PAY_W_CALENDAR',200,'AB063501','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Discounts Taken and Lost','200','','XXWC_PAY_NO_CALENDAR',200,'AB063501','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Discounts Taken and Lost','200','','HDS_AP_DISBUREMTS_US_IWO',200,'AB063501','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Discounts Taken and Lost','200','','HDS_AP_DISBURSEMTS_US_GSC',200,'AB063501','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Discounts Taken and Lost','200','','XXWC_PAY_VENDOR_MSTR_INQUIRY',200,'AB063501','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Discounts Taken and Lost','200','','XXWC_PAYABLES_INQUIRY',200,'AB063501','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Discounts Taken and Lost','200','','HDS_AP_INQUIRY_US_IWO',200,'AB063501','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Discounts Taken and Lost','200','','HDS_AP_INQUIRY_US_GSC',200,'AB063501','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Discounts Taken and Lost','200','','XXWC_PAY_MANAGER',200,'AB063501','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Discounts Taken and Lost','200','','HDS_AP_MGR_NOSUP_US_IWO',200,'AB063501','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Discounts Taken and Lost','200','','HDS_AP_MGR_NOSUP_US_GSC',200,'AB063501','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Discounts Taken and Lost','200','','HDS_PYBLS_MNGR',200,'AB063501','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Discounts Taken and Lost','200','','HDS_PYABLS_MNGR_CAN',200,'AB063501','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Discounts Taken and Lost','200','','HDS_AP_MANAGER',200,'AB063501','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Discounts Taken and Lost','200','','HDS_AP_SUPPLIER_MAINT_US_IWO',200,'AB063501','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Discounts Taken and Lost','200','','HDS_AP_SUPPLIER_MAINT_US_GSC',200,'AB063501','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Discounts Taken and Lost','200','','HDS_AP_TRNS_ENTRY_US_IWO',200,'AB063501','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Discounts Taken and Lost','200','','HDS_AP_TRNS_ENTRY_US_GSC',200,'AB063501','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Discounts Taken and Lost','200','','XXWC_PAY_VENDOR_MSTR',200,'AB063501','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Discounts Taken and Lost','200','','XXWC_PAY_VENDOR_BANK_DETAILS',200,'AB063501','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Discounts Taken and Lost','20005','','XXWC_VIEW_ALL_EIS_REPORTS',200,'AB063501','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Discounts Taken and Lost','200','','PAYABLES_MANAGER',200,'AB063501','','','');
--Inserting Report Pivots - HDS Discounts Taken and Lost
xxeis.eis_rsc_ins.rpivot( 'HDS Discounts Taken and Lost',200,'Pivot','1','1,0|1,2,1','1,1,0,0||2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS Discounts Taken and Lost',200,'Pivot','CHECK_DATE','PAGE_FIELD','','','6','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS Discounts Taken and Lost',200,'Pivot','OPERATING_UNIT','ROW_FIELD','','','1','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS Discounts Taken and Lost',200,'Pivot','INVOICE_AMOUNT_PAID','PAGE_FIELD','','','4','','');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS Discounts Taken and Lost',200,'Pivot','INVOICE_CURRENCY_CODE','PAGE_FIELD','','','5','','');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS Discounts Taken and Lost',200,'Pivot','DISCOUNT_AMOUNT_AVAILABLE','DATA_FIELD','SUM','','2','','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS Discounts Taken and Lost',200,'Pivot','DISCOUNT_LOST','DATA_FIELD','SUM','','4','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS Discounts Taken and Lost',200,'Pivot','DISCOUNT_TAKEN','DATA_FIELD','SUM','','3','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS Discounts Taken and Lost',200,'Pivot','DUE_DATE','PAGE_FIELD','','','2','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS Discounts Taken and Lost',200,'Pivot','INVOICE_AMOUNT','DATA_FIELD','SUM','','1','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS Discounts Taken and Lost',200,'Pivot','SUPPLIER','ROW_FIELD','','','2','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS Discounts Taken and Lost',200,'Pivot','INVOICE_NUM','PAGE_FIELD','','','1','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS Discounts Taken and Lost',200,'Pivot','AMOUNT_REMAINING','PAGE_FIELD','','','3','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS Discounts Taken and Lost',200,'Pivot','BASE_CURRENCY_INVOICE_AMOUNT','DATA_FIELD','SUM','','5','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS Discounts Taken and Lost',200,'Pivot','BASE_CURR_DISCOUNT_LOST','DATA_FIELD','SUM','','7','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS Discounts Taken and Lost',200,'Pivot','BASE_CURR_DISCOUNT_TAKEN','DATA_FIELD','SUM','','6','1','xlNormal');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
xxeis.eis_rsc_ins.rpivot( 'HDS Discounts Taken and Lost',200,'Discounts Taken By Supplier','2','1,0|1,2,1','1,1,0,0||2');
--Inserting Report Pivot Details For Pivot - Discounts Taken By Supplier
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS Discounts Taken and Lost',200,'Discounts Taken By Supplier','DISCOUNT_AMOUNT_AVAILABLE','DATA_FIELD','SUM','','1','','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS Discounts Taken and Lost',200,'Discounts Taken By Supplier','DISCOUNT_TAKEN','DATA_FIELD','SUM','','2','','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS Discounts Taken and Lost',200,'Discounts Taken By Supplier','SUPPLIER','ROW_FIELD','','','1','1','');
--Inserting Report Summary Calculation Columns For Pivot- Discounts Taken By Supplier
xxeis.eis_rsc_ins.rpivot( 'HDS Discounts Taken and Lost',200,'Discounts Lost by Supplier','3','1,0|1,2,1','1,1,0,0||2');
--Inserting Report Pivot Details For Pivot - Discounts Lost by Supplier
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS Discounts Taken and Lost',200,'Discounts Lost by Supplier','DISCOUNT_AMOUNT_AVAILABLE','DATA_FIELD','SUM','','1','','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS Discounts Taken and Lost',200,'Discounts Lost by Supplier','DISCOUNT_LOST','DATA_FIELD','SUM','','2','','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS Discounts Taken and Lost',200,'Discounts Lost by Supplier','SUPPLIER','ROW_FIELD','','','1','1','');
--Inserting Report Summary Calculation Columns For Pivot- Discounts Lost by Supplier
--Inserting Report   Version details- HDS Discounts Taken and Lost
xxeis.eis_rsc_ins.rv( 'HDS Discounts Taken and Lost','802','','AB063501');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
