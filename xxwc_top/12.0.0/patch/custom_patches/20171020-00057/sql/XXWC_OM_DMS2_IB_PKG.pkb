CREATE OR REPLACE PACKAGE BODY APPS.XXWC_OM_DMS2_IB_PKG AS
/******************************************************************************
     $Header XXWC_OM_DMS2_IB_PKG $
     Module Name: XXWC_OM_DMS2_IB_PKG.pkb

     PURPOSE:   DESCARTES Project

     REVISIONS:
     Ver        Date        Author                Description
     ---------  ----------  ---------------       -----------------------------
     1.0        12/19/2017  Rakesh Patel          TMS#20170901-00010 DMS Phase-2.0 POD Integration with Oracle deploy web service - Pre Go Live
*******************************************************************************/

   --Email Defaults
   g_dflt_email                fnd_user.email_address%TYPE    := 'WC-ITDEVALERTS-U1@HDSupply.com';
   g_debug                     BOOLEAN := FALSE;
   g_request_id                NUMBER;
   g_retcode                   NUMBER;  
   g_err_msg                   VARCHAR2(3000);
   g_sqlcode                   NUMBER;   
   g_sqlerrm                   VARCHAR2(2000);
   g_message                   VARCHAR2(2000);
   g_package                   VARCHAR2(200) :=   'XXWC_OM_DMS2_IB_PKG';
   g_call_from                 VARCHAR2(200);
   g_module                    VARCHAR2 (80) := 'OM';
   g_err_callfrom              VARCHAR2(75) DEFAULT 'XXWC_OM_DMS2_IB_PKG';
   g_distro_list               VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
   g_host                      VARCHAR2(256) := 'mailoutrelay.hdsupply.net';
   g_hostport                  VARCHAR2(20) := '25';
  
   TYPE request_ids_tbl IS TABLE OF NUMBER INDEX BY PLS_INTEGER;
   g_request_ids_tbl   request_ids_tbl;
   g_request_id_count  NUMBER:= 0;
     
   PROCEDURE debug_on AS
   BEGIN
      g_debug := TRUE;
   END;

   PROCEDURE debug_off AS
   BEGIN
     g_debug := FALSE;
   END;

  /*******************************************************************************
   Procedure Name  :   DEBUG_LOG
   Description     :   This function will be used for log messages 
   ******************************************************************************/
   PROCEDURE DEBUG_LOG (p_xxwc_log_rec IN OUT xxwc_log_rec )
   IS
       PRAGMA AUTONOMOUS_TRANSACTION;
       l_order_number XXWC.XXWC_dms2_LOG_TBL.order_number%TYPE;
       l_update BOOLEAN := FALSE;
       
       l_sec           VARCHAR2 (2000);
   BEGIN
      l_sec := 'Start DEBUG_LOG';
      g_call_from := 'DEBUG_LOG';
      
      BEGIN
        SELECT order_number
          INTO l_order_number 
          FROM xxwc.xxwc_dms2_log_tbl
         WHERE interface_type   = 'INBOUND'
           AND service_name	    = p_xxwc_log_rec.service_name
		   AND sequence_id      = p_xxwc_log_rec.sequence_id;
         
         l_update := TRUE;
		 
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
        l_update := FALSE;
      WHEN OTHERS THEN
        l_update := FALSE;
      END;   
        
      IF l_update = TRUE THEN
        UPDATE XXWC.XXWC_DMS2_LOG_TBL
           SET SERVICE_NAME            = p_xxwc_log_rec.service_name
              ,REQUEST_PAYLOAD         = p_xxwc_log_rec.request_payload
              ,RESPONSE_PAYLOAD        = p_xxwc_log_rec.response_payload
              ,WS_RESPONSECODE         = p_xxwc_log_rec.ws_responsecode
              ,WS_RESPONSEDESCRIPTION  = p_xxwc_log_rec.ws_responsedescription
              ,HTTP_STATUS_CODE        = p_xxwc_log_rec.http_status_code
              ,HTTP_REASON_PHRASE      = p_xxwc_log_rec.http_reason_phrase
              ,LAST_UPDATE_DATE        = SYSDATE
              ,LAST_UPDATED_BY         = FND_GLOBAL.user_id
              ,CONCURRENT_REQUEST_ID   = Fnd_global.conc_request_id 
			  ,ORDER_NUMBER            = p_xxwc_log_rec.order_number
			  ,DELIVERY_ID             = p_xxwc_log_rec.delivery_id
			  ,ORDER_TYPE              = p_xxwc_log_rec.order_type
			  ,cmd					   = p_xxwc_log_rec.cmd
			  ,stype				   = p_xxwc_log_rec.stype
              ,lcode                   = p_xxwc_log_rec.lcode
         WHERE interface_type          = 'INBOUND'		   
           AND service_name	           = p_xxwc_log_rec.service_name
		   AND sequence_id             = p_xxwc_log_rec.sequence_id;
      ELSE
         INSERT INTO XXWC.XXWC_DMS2_LOG_TBL
            (
             SERVICE_NAME            ,
             INTERFACE_TYPE          ,
             ORDER_NUMBER            ,  
             ORDER_HEADER_ID         ,
             LINE_ID                 ,
             DELIVERY_ID             , 
             ORDER_TYPE              ,
             SCHEDULE_DELIVERY_DATE  ,
             REQUEST_PAYLOAD         ,
             RESPONSE_PAYLOAD        ,
             WS_RESPONSECODE         ,
             WS_RESPONSEDESCRIPTION  ,
             HTTP_STATUS_CODE        ,
             HTTP_REASON_PHRASE      ,
             CONCURRENT_REQUEST_ID   ,
			 SEQUENCE_ID             ,
             CREATION_DATE           ,
             CREATED_BY              ,
             LAST_UPDATE_DATE        ,
             LAST_UPDATED_BY         ,
             ATTRIBUTE1              ,
             ATTRIBUTE2              ,
             ATTRIBUTE3              ,
             ATTRIBUTE4              ,
             ATTRIBUTE5              ,
			 REQUEST_PAYLOAD_XMLTYPE
             )
         VALUES
             (
              p_xxwc_log_rec.service_name              ,
              'INBOUND'                                , 
              p_xxwc_log_rec.order_number              ,
              p_xxwc_log_rec.header_id                 ,
              p_xxwc_log_rec.line_id                   ,
              p_xxwc_log_rec.delivery_id               ,
              p_xxwc_log_rec.order_type                ,
              p_xxwc_log_rec.schedule_delivery_date    ,
              p_xxwc_log_rec.request_payload           ,
              p_xxwc_log_rec.response_payload          ,
              p_xxwc_log_rec.ws_responsecode           ,
              p_xxwc_log_rec.ws_responsedescription    ,
              p_xxwc_log_rec.http_status_code          ,
              p_xxwc_log_rec.http_reason_phrase        ,
              Fnd_global.conc_request_id               ,
			  p_xxwc_log_rec.sequence_id               ,
              SYSDATE                                  ,
              FND_GLOBAL.user_id                       ,
              SYSDATE                                  ,
              FND_GLOBAL.user_id                       ,
              NULL                                     ,   
              NULL                                     ,
              NULL                                     ,
              NULL                                     ,
			  NULL                                     ,
			  p_xxwc_log_rec.REQUEST_PAYLOAD_XMLTYPE                                                 
             ) ;  
      END IF;
      COMMIT;
	  
      l_sec := 'End DEBUG_LOG';
	  
EXCEPTION
WHEN others THEN
	    
  g_sqlcode := SQLCODE;
  g_sqlerrm := SQLERRM;
  g_message := 'Error in generating debug log ';
    
  xxcus_error_pkg.xxcus_error_main_api(p_called_from         => g_package||'.'||g_call_from
                                     , p_calling             => l_sec
                                     , p_ora_error_msg       => SQLERRM
                                     , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                     , p_distribution_list   => g_dflt_email
                                     , p_module              => g_module);

   
END DEBUG_LOG;

/*************************************************************************
     $Header XXWC_OM_DMS2_IB_PKG $
     Module Name: INSERT_SHIPMENT_DATA
**************************************************************************/
PROCEDURE insert_shipment_data(p_sequence_id   IN  NUMBER
                              ,p_xxwc_log_rec IN OUT xxwc_log_rec
                              ,p_order_number  OUT NUMBER
							  ,p_order_type_id OUT NUMBER
							  ,p_delivery_id   OUT NUMBER
							  ,p_cmd           OUT NUMBER
							  ,p_stype         OUT NUMBER
							  ,p_route_id      OUT VARCHAR2
							  ,p_stop_id       OUT NUMBER
							  ,p_lcode         OUT NUMBER
							  ,p_err_msg       OUT VARCHAR2) IS
    l_err_msg             CLOB;				
    l_procedure           VARCHAR2(50) := 'SHIP_CONFIRM';		
    l_sec                 VARCHAR2(2000) DEFAULT 'START';	  
BEGIN
  NULL;--do nothing
EXCEPTION
WHEN OTHERS THEN
   l_err_msg := 'Error_Stack...' || dbms_utility.format_error_stack() ||
                ' Error_Backtrace...' ||
                dbms_utility.format_error_backtrace();
   fnd_file.put_line(fnd_file.log, l_err_msg);
   dbms_output.put_line(l_err_msg);
   -- Calling ERROR API
   xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom || '.' ||
                                                               l_procedure
                                       ,p_calling           => l_sec
                                       ,p_request_id        => fnd_global.conc_request_id
                                       ,p_ora_error_msg     => substr(l_err_msg
                                                                     ,1
                                                                     ,2000)
                                       ,p_error_desc        => substr(l_err_msg
                                                                     ,1
                                                                     ,240)
                                       ,p_distribution_list => g_distro_list
                                       ,p_module            => 'ONT'); 
								  
END insert_shipment_data;
 
END XXWC_OM_DMS2_IB_PKG;
/