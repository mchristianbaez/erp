  /********************************************************************************
  FILE NAME: XXWC_DMS2_LOG_TBL.sql
  
  PROGRAM TYPE: Log table for DESCARTES requests
  
  PURPOSE: Log table for DESCARTES requests
  
  HISTORY
  ==========================================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- ----------------------------------------------------
  1.0     09/19/2017    Rakesh P.        TMS#20171020-00057 DMS Phase-2.0 POD Integration with Oracle deploy web service - Pre Go Live
  *******************************************************************************************/
  CREATE TABLE XXWC.XXWC_DMS2_LOG_TBL
            (
            SEQUENCE_ID              NUMBER,
            SERVICE_NAME             VARCHAR2(150 BYTE),
            INTERFACE_TYPE           VARCHAR2(150 BYTE),
            ORDER_NUMBER             NUMBER,
            ORDER_HEADER_ID          NUMBER,
            LINE_ID                  NUMBER,
            DELIVERY_ID              NUMBER,
            ORDER_TYPE               VARCHAR2(150 BYTE),
            PO_NUMBER                NUMBER,
            PO_HEADER_ID             NUMBER,
            SCHEDULE_DELIVERY_DATE   DATE,
			RPT_CON_REQUEST_ID       NUMBER,
            WS_RESPONSECODE          VARCHAR2(500 BYTE),
            WS_RESPONSEDESCRIPTION   VARCHAR2(4000 BYTE),
            HTTP_STATUS_CODE         VARCHAR2(500 BYTE),
            HTTP_REASON_PHRASE       VARCHAR2(4000 BYTE),
            REQUEST_PAYLOAD          CLOB,
            REQUEST_PAYLOAD_XMLTYPE  SYS.XMLTYPE,
            CMD                      VARCHAR2(150 BYTE),
            STYPE                    VARCHAR2(150 BYTE),
            RESPONSE_PAYLOAD         CLOB,
            CONCURRENT_REQUEST_ID    NUMBER,
            CREATION_DATE            DATE                 NOT NULL,
            CREATED_BY               NUMBER(15)           NOT NULL,
            LAST_UPDATE_DATE         DATE                 NOT NULL,
            LAST_UPDATED_BY          NUMBER(15)           NOT NULL,
            ATTRIBUTE1               VARCHAR2(150 BYTE),
            ATTRIBUTE2               VARCHAR2(150 BYTE),
            ATTRIBUTE3               VARCHAR2(150 BYTE),
            ATTRIBUTE4               VARCHAR2(150 BYTE),
            ATTRIBUTE5               VARCHAR2(150 BYTE),
            LCODE                    VARCHAR2(150 BYTE)
);

ALTER TABLE XXWC.XXWC_DMS2_LOG_TBL ADD (RPT_CON_REQUEST_ID   NUMBER);

CREATE PUBLIC SYNONYM XXWC_DMS2_LOG_TBL FOR xxwc.XXWC_DMS2_LOG_TBL;

CREATE OR REPLACE SYNONYM APPS.XXWC_DMS2_LOG_TBL FOR XXWC.XXWC_DMS2_LOG_TBL;

CREATE INDEX XXWC.XXWC_DMS2_LOG_TBL_N1 ON XXWC.XXWC_DMS2_LOG_TBL
(SEQUENCE_ID);   

CREATE INDEX XXWC.XXWC_DMS2_LOG_TBL_N2 ON XXWC.XXWC_DMS2_LOG_TBL
(INTERFACE_TYPE, SERVICE_NAME, ORDER_NUMBER);      

CREATE INDEX XXWC.XXWC_DMS2_LOG_TBL_N3 ON XXWC.XXWC_DMS2_LOG_TBL
(INTERFACE_TYPE, SERVICE_NAME, ORDER_NUMBER, DELIVERY_ID);  

CREATE INDEX XXWC.XXWC_DMS2_LOG_TBL_N4 ON XXWC.XXWC_DMS2_LOG_TBL
(INTERFACE_TYPE, SERVICE_NAME, ORDER_NUMBER, WS_RESPONSECODE);

CREATE INDEX XXWC.XXWC_DMS2_LOG_TBL_N5 ON XXWC.XXWC_DMS2_LOG_TBL
(RPT_CON_REQUEST_ID);