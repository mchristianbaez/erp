CREATE OR REPLACE PACKAGE APPS.XXWC_OM_DMS2_IB_PKG IS
 /*************************************************************************
     $Header XXWC_OM_DMS2_IB_PKG $
     Module Name: XXWC_OM_DMS2_IB_PKG.pks

     PURPOSE:   DESCARTES Project    
                       
     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        01/23/2017  Rakesh Patel            TMS#20171020-00057 DMS Phase-2.0 POD Integration with Oracle deploy web service - Pre Go Live
**************************************************************************/
  
   TYPE xxwc_log_rec IS RECORD (
      SERVICE_NAME            VARCHAR2(150)
     ,ORDER_NUMBER            NUMBER
     ,HEADER_ID               NUMBER
     ,LINE_ID                 NUMBER
     ,DELIVERY_ID             NUMBER
     ,ORDER_TYPE              VARCHAR2(150)  
     ,PO_NUMBER               NUMBER
     ,PO_HEADER_ID            NUMBER
     ,SCHEDULE_DELIVERY_DATE  DATE             
     ,WS_RESPONSECODE         VARCHAR2(500)
     ,WS_RESPONSEDESCRIPTION  VARCHAR2(4000)
     ,HTTP_STATUS_CODE        VARCHAR2(500)
     ,HTTP_REASON_PHRASE      VARCHAR2(4000)
     ,SEQUENCE_ID             NUMBER
	 ,CMD                     NUMBER
	 ,STYPE                   NUMBER
	 ,LCODE                   NUMBER
     ,REQUEST_PAYLOAD         CLOB
     ,RESPONSE_PAYLOAD        CLOB 
     ,REQUEST_PAYLOAD_XMLTYPE XMLTYPE
   );
   
PROCEDURE DEBUG_LOG (p_xxwc_log_rec IN OUT xxwc_log_rec);
PROCEDURE debug_on;
PROCEDURE debug_off;

PROCEDURE insert_shipment_data(p_sequence_id   IN NUMBER
                              ,p_xxwc_log_rec IN OUT xxwc_log_rec
                              ,p_order_number  OUT NUMBER
                              ,p_order_type_id OUT NUMBER
                              ,p_delivery_id   OUT NUMBER
                              ,p_cmd           OUT NUMBER
                              ,p_stype         OUT NUMBER
                              ,p_route_id      OUT VARCHAR2
                              ,p_stop_id       OUT NUMBER
							  ,p_lcode         OUT NUMBER
							  ,p_err_msg       OUT VARCHAR2
                              );  
  
END XXWC_OM_DMS2_IB_PKG;
/