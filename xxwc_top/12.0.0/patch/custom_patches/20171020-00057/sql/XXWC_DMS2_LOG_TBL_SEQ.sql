/*************************************************************************
     $Header XXWC_DMS2_LOG_TBL_SEQ $
     Module Name: XXWC_DMS2_LOG_TBL_SEQ.pkb

     PURPOSE:   DESCARTES Project

     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        11/11/2017  Rakesh Patel            TMS#20171020-00057 DMS Phase-2.0 POD Integration with Oracle deploy web service - Pre Go Live
**************************************************************************/
-- Create sequence 
create sequence XXWC.XXWC_DMS2_LOG_TBL_SEQ
minvalue 1
maxvalue 999999999999999999999999999
start with 1
increment by 1
nocache;
