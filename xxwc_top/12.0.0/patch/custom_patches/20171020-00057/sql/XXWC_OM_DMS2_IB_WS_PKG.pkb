CREATE OR REPLACE PACKAGE BODY APPS.XXWC_OM_DMS2_IB_WS_PKG AS
/******************************************************************************
     $Header XXWC_OM_DMS2_IB_PKG $
     Module Name: XXWC_OM_DMS2_IB_PKG.pkb

     PURPOSE:   DESCARTES Project

     REVISIONS:
     Ver        Date        Author                Description
     ---------  ----------  ---------------       -----------------------------
     1.0        01/23/2017  Rakesh Patel          TMS#20171020-00057 DMS Phase-2.0 POD Integration with Oracle deploy web service - Pre Go Live
*******************************************************************************/

PROCEDURE RECEIVE_SHIPMENT(
 MLW                IN  CLOB
,X_RETURN_STATUS    OUT NOCOPY VARCHAR2
,X_MSG_DATA         OUT NOCOPY VARCHAR2
) IS

lv_xml_data            XMLTYPE;
l_xxwc_log_rec         XXWC_OM_DMS2_IB_PKG.xxwc_log_rec;
le_exception           EXCEPTION;
ln_order_type_id       NUMBER;
ln_order_number        NUMBER;
ln_org_id              NUMBER := 162;
lv_err_msg             VARCHAR2(4000);
lv_response_code       VARCHAR2(500);
lv_timestamp           VARCHAR2(40);
lv_start_timestamp     VARCHAR2(40);
lv_order_type          VARCHAR2(30);
lv_route_id            VARCHAR2(30);
ln_log_seq             NUMBER;
ln_delivery_id         NUMBER;
ln_cmd                 NUMBER;
ln_stype               NUMBER;
ln_stop_id             NUMBER;
ln_time_elapsed        NUMBER;
ln_lcode               NUMBER;

BEGIN
  IF FND_PROFILE.VALUE ('XXWC_OM_DESCARTES_DEBUG_FLAG') = 'Y' THEN
     XXWC_OM_DMS2_IB_PKG.DEBUG_ON;
  END IF;
     
  SELECT TO_CHAR(SYSTIMESTAMP,'DD-MON-YYYY HH24:MI:SS')
  INTO lv_start_timestamp
  FROM dual;
  
  ln_log_seq:=XXWC.XXWC_DMS2_LOG_TBL_SEQ.NEXTVAL;  

  l_xxwc_log_rec.service_name    := 'SHIP_CONFIRM';
  l_xxwc_log_rec.request_payload := MLW;
  l_xxwc_log_rec.sequence_id     := ln_log_seq;

  --Converting the CLOB data into XMLTYPE...
  lv_xml_data := XMLTYPE.createXML(MLW);
  
  l_xxwc_log_rec.REQUEST_PAYLOAD_XMLTYPE :=lv_xml_data;
      
  --Calling the debug_log to INSERT the payload details for the first time...
  XXWC_OM_DMS2_IB_PKG.DEBUG_LOG (p_xxwc_log_rec => l_xxwc_log_rec);
  
  --Calling the DMS IB package to INSERT the shipping data into the shipment table.
  XXWC_OM_DMS2_IB_PKG.insert_shipment_data(ln_log_seq
                                          ,l_xxwc_log_rec
                                          ,ln_order_number  
					                      ,ln_order_type_id
										  ,ln_delivery_id
										  ,ln_cmd
										  ,ln_stype
										  ,lv_route_id
										  ,ln_stop_id
										  ,ln_lcode
										  ,lv_err_msg
					                       );
		  
  SELECT TO_CHAR(SYSTIMESTAMP,'DD-MON-YYYY HH24:MI:SS')
  INTO lv_timestamp
  FROM dual;
    
  SELECT (TO_DATE(lv_start_timestamp,'DD-MON-YYYY HH24:MI:SS') - TO_DATE(lv_timestamp,'DD-MON-YYYY HH24:MI:SS')) * (24 * 60 * 60 * 1000)  
  INTO ln_time_elapsed
  FROM DUAL;
  
  IF lv_err_msg IS NOT NULL THEN
    RAISE le_exception;
  END IF;
  
  lv_response_code:=
  '<Command><Execute><Status>
     <MLWResponse>
       <TransactionID>'||ln_log_seq||'</TransactionID>
       <RouteId>'||lv_route_id||'</RouteId>
       <StartTStamp>'||lv_start_timestamp||'</StartTStamp>
       <TStamp>'||lv_timestamp||'</TStamp>
       <Elapsed>'||ln_time_elapsed||'</Elapsed>
       <StopId>'||ln_stop_id||'</StopId>
       <Message>Data inserted successfully</Message>
       <Result>Success</Result>
     </MLWResponse>
     </Status></Execute></Command>';

  l_xxwc_log_rec.WS_RESPONSECODE        := lv_response_code;
  l_xxwc_log_rec.WS_RESPONSEDESCRIPTION := 'Data inserted successfully';
  l_xxwc_log_rec.order_number           := ln_order_number;
  l_xxwc_log_rec.delivery_id            := ln_delivery_id;
  l_xxwc_log_rec.cmd                    := ln_cmd;
  l_xxwc_log_rec.stype                  := ln_stype;
  l_xxwc_log_rec.lcode                  := ln_lcode;

  X_RETURN_STATUS := l_xxwc_log_rec.WS_RESPONSECODE;
  X_MSG_DATA      := l_xxwc_log_rec.WS_RESPONSEDESCRIPTION ;
  
  --Calling the debug_log to UPDATE the other details for the given payload...
  XXWC_OM_DMS2_IB_PKG.DEBUG_LOG (p_xxwc_log_rec => l_xxwc_log_rec);
  		
EXCEPTION
WHEN le_exception THEN

  l_xxwc_log_rec.WS_RESPONSEDESCRIPTION := 'Error in Data Insertion '||lv_err_msg;
  X_MSG_DATA := l_xxwc_log_rec.WS_RESPONSEDESCRIPTION ;
  
  lv_response_code:='<?xml version="1.0"?>
  <Command><Execute><Status>
  <MLWResponse>
  <TransactionID>'||ln_order_number||'</TransactionID>
  <RouteId>'||lv_route_id||'</RouteId>
  <StartTStamp>'||lv_start_timestamp||'</StartTStamp>
  <TStamp>'||lv_timestamp||'</TStamp>
  <Elapsed>'||ln_time_elapsed||'</Elapsed>
  <StopId>'||ln_stop_id||'</StopId>
  <Message>'||X_MSG_DATA||'</Message>
  <Result>FAILURE</Result>
  </MLWResponse></Status></Execute></Command>';

  l_xxwc_log_rec.WS_RESPONSECODE := lv_response_code;
  X_RETURN_STATUS                := l_xxwc_log_rec.WS_RESPONSECODE;

WHEN OTHERS THEN

  l_xxwc_log_rec.WS_RESPONSEDESCRIPTION := 'Error in Data Insertion '||SQLERRM;
  X_MSG_DATA := l_xxwc_log_rec.WS_RESPONSEDESCRIPTION ;

  lv_response_code:='<?xml version="1.0"?>
  <Command><Execute><Status>
  <MLWResponse>
  <TransactionID>'||ln_order_number||'</TransactionID>
  <RouteId>'||lv_route_id||'</RouteId>
  <StartTStamp>'||lv_start_timestamp||'</StartTStamp>
  <TStamp>'||lv_timestamp||'</TStamp>
  <Elapsed>'||ln_time_elapsed||'</Elapsed>
  <StopId>'||ln_stop_id||'</StopId>
  <Message>'||X_MSG_DATA||'</Message>
  <Result>FAILURE</Result>
  </MLWResponse></Status></Execute></Command>';

  l_xxwc_log_rec.WS_RESPONSECODE := lv_response_code;
  X_RETURN_STATUS                := l_xxwc_log_rec.WS_RESPONSECODE;
  
END;

END XXWC_OM_DMS2_IB_WS_PKG;
/