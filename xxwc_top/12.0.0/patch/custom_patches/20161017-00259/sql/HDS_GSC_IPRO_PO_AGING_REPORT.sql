/*
--Report Name            : HDS GSC iPro PO Aging Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating View Data for HDS GSC iPro PO Aging Report
set scan off define off
History
===========
Ticket                           Version   Author                               Comment
===============================  ======    ==================================== =====================================================
TMS 20161017-00259 / ESMS 306425 1.0       Balaguru Seshadri                    HDS GSC iPro Purchase Orders with Requisition Details)
*/
DECLARE
BEGIN 
--Inserting View XXCUS_GSC_PO_DETAIL_WITH_REQ_V
xxeis.eis_rs_ins.v( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V',201,'','','','','ID020048','XXEIS','Xxcus Gsc Po Detail With Req V','XGPDWRV','','');
--Delete View Columns for XXCUS_GSC_PO_DETAIL_WITH_REQ_V
xxeis.eis_rs_utility.delete_view_rows('XXCUS_GSC_PO_DETAIL_WITH_REQ_V',201,FALSE);
--Inserting View Columns for XXCUS_GSC_PO_DETAIL_WITH_REQ_V
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_AMOUNT_BILLED',201,'Po Amount Billed','PO_AMOUNT_BILLED','','','','ID020048','NUMBER','','','Po Amount Billed','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_AMOUNT_DUE',201,'Po Amount Due','PO_AMOUNT_DUE','','','','ID020048','NUMBER','','','Po Amount Due','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_AMOUNT_RECEIVED',201,'Po Amount Received','PO_AMOUNT_RECEIVED','','','','ID020048','NUMBER','','','Po Amount Received','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_AMOUNT_CANCELLED',201,'Po Amount Cancelled','PO_AMOUNT_CANCELLED','','','','ID020048','NUMBER','','','Po Amount Cancelled','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_AMOUNT_ORDERED',201,'Po Amount Ordered','PO_AMOUNT_ORDERED','','','','ID020048','NUMBER','','','Po Amount Ordered','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_QUANTITY_BILLED',201,'Po Quantity Billed','PO_QUANTITY_BILLED','','','','ID020048','NUMBER','','','Po Quantity Billed','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_QUANTITY_DUE',201,'Po Quantity Due','PO_QUANTITY_DUE','','','','ID020048','NUMBER','','','Po Quantity Due','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_QUANTITY_RECEIVED',201,'Po Quantity Received','PO_QUANTITY_RECEIVED','','','','ID020048','NUMBER','','','Po Quantity Received','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_QUANTITY_CANCELLED',201,'Po Quantity Cancelled','PO_QUANTITY_CANCELLED','','','','ID020048','NUMBER','','','Po Quantity Cancelled','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_QUANTITY_ORDERED',201,'Po Quantity Ordered','PO_QUANTITY_ORDERED','','','','ID020048','NUMBER','','','Po Quantity Ordered','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_UNIT_PRICE',201,'Po Unit Price','PO_UNIT_PRICE','','','','ID020048','NUMBER','','','Po Unit Price','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_ITEM_DESCRIPTION',201,'Po Item Description','PO_ITEM_DESCRIPTION','','','','ID020048','VARCHAR2','','','Po Item Description','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_ITEM_CATEGORY',201,'Po Item Category','PO_ITEM_CATEGORY','','','','ID020048','VARCHAR2','','','Po Item Category','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_ITEM_NUMBER',201,'Po Item Number','PO_ITEM_NUMBER','','','','ID020048','VARCHAR2','','','Po Item Number','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_LINE_NUM',201,'Po Line Num','PO_LINE_NUM','','','','ID020048','NUMBER','','','Po Line Num','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_LINE_TYPE',201,'Po Line Type','PO_LINE_TYPE','','','','ID020048','VARCHAR2','','','Po Line Type','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','POL_CREATED_BY_NAME',201,'Pol Created By Name','POL_CREATED_BY_NAME','','','','ID020048','VARCHAR2','','','Pol Created By Name','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','POL_CREATED_BY',201,'Pol Created By','POL_CREATED_BY','','','','ID020048','NUMBER','','','Pol Created By','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','POL_CREATION_DATE',201,'Pol Creation Date','POL_CREATION_DATE','','','','ID020048','DATE','','','Pol Creation Date','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','POL_LAST_UPDATED_BY',201,'Pol Last Updated By','POL_LAST_UPDATED_BY','','','','ID020048','NUMBER','','','Pol Last Updated By','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','POL_LAST_UPDATE_DATE',201,'Pol Last Update Date','POL_LAST_UPDATE_DATE','','','','ID020048','DATE','','','Pol Last Update Date','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_LINE_ID',201,'Po Line Id','PO_LINE_ID','','','','ID020048','NUMBER','','','Po Line Id','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_CLOSED_DATE',201,'Po Closed Date','PO_CLOSED_DATE','','','','ID020048','DATE','','','Po Closed Date','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_CLOSED_CODE',201,'Po Closed Code','PO_CLOSED_CODE','','','','ID020048','VARCHAR2','','','Po Closed Code','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_COMMENTS',201,'Po Comments','PO_COMMENTS','','','','ID020048','VARCHAR2','','','Po Comments','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_NOTE_TO_RECEIVER',201,'Po Note To Receiver','PO_NOTE_TO_RECEIVER','','','','ID020048','VARCHAR2','','','Po Note To Receiver','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_NOTE_TO_SUPPLIER',201,'Po Note To Supplier','PO_NOTE_TO_SUPPLIER','','','','ID020048','VARCHAR2','','','Po Note To Supplier','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_NOTE_TO_AUTHORIZER',201,'Po Note To Authorizer','PO_NOTE_TO_AUTHORIZER','','','','ID020048','VARCHAR2','','','Po Note To Authorizer','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_ACCEPTANCE_REQUIRED',201,'Po Acceptance Required','PO_ACCEPTANCE_REQUIRED','','','','ID020048','VARCHAR2','','','Po Acceptance Required','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_ON_HOLD',201,'Po On Hold','PO_ON_HOLD','','','','ID020048','VARCHAR2','','','Po On Hold','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_ACCEPTANCE_DUE_DATE',201,'Po Acceptance Due Date','PO_ACCEPTANCE_DUE_DATE','','','','ID020048','DATE','','','Po Acceptance Due Date','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_CURRENCY_CODE',201,'Po Currency Code','PO_CURRENCY_CODE','','','','ID020048','VARCHAR2','','','Po Currency Code','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_APPROVED_DATE',201,'Po Approved Date','PO_APPROVED_DATE','','','','ID020048','DATE','','','Po Approved Date','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_PRINTED_DATE',201,'Po Printed Date','PO_PRINTED_DATE','','','','ID020048','DATE','','','Po Printed Date','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_REVISED_DATE',201,'Po Revised Date','PO_REVISED_DATE','','','','ID020048','DATE','','','Po Revised Date','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_REVISION_NUM',201,'Po Revision Num','PO_REVISION_NUM','','','','ID020048','NUMBER','','','Po Revision Num','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_STATUS',201,'Po Status','PO_STATUS','','','','ID020048','VARCHAR2','','','Po Status','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_FREIGHT_TERMS',201,'Po Freight Terms','PO_FREIGHT_TERMS','','','','ID020048','VARCHAR2','','','Po Freight Terms','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_FOB',201,'Po Fob','PO_FOB','','','','ID020048','VARCHAR2','','','Po Fob','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_SHIP_VIA',201,'Po Ship Via','PO_SHIP_VIA','','','','ID020048','VARCHAR2','','','Po Ship Via','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_TERMS',201,'Po Terms','PO_TERMS','','','','ID020048','VARCHAR2','','','Po Terms','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_SHIP_TO_LOCATION',201,'Po Ship To Location','PO_SHIP_TO_LOCATION','','','','ID020048','VARCHAR2','','','Po Ship To Location','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_END_DATE',201,'Po End Date','PO_END_DATE','','','','ID020048','DATE','','','Po End Date','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_START_DATE',201,'Po Start Date','PO_START_DATE','','','','ID020048','DATE','','','Po Start Date','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_ENABLED',201,'Po Enabled','PO_ENABLED','','','','ID020048','VARCHAR2','','','Po Enabled','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_SUMMARY',201,'Po Summary','PO_SUMMARY','','','','ID020048','VARCHAR2','','','Po Summary','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','SUPPLIER_CONTACT_PHONE',201,'Supplier Contact Phone','SUPPLIER_CONTACT_PHONE','','','','ID020048','VARCHAR2','','','Supplier Contact Phone','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','SUPPLIER_CONTACT_AREA_CODE',201,'Supplier Contact Area Code','SUPPLIER_CONTACT_AREA_CODE','','','','ID020048','VARCHAR2','','','Supplier Contact Area Code','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','SUPPLIER_CONTACT_LAST_NAME',201,'Supplier Contact Last Name','SUPPLIER_CONTACT_LAST_NAME','','','','ID020048','VARCHAR2','','','Supplier Contact Last Name','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','SUPPLIER_CONTACT_FIRST_NAME',201,'Supplier Contact First Name','SUPPLIER_CONTACT_FIRST_NAME','','','','ID020048','VARCHAR2','','','Supplier Contact First Name','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_SUPPLIER_SITE_CODE',201,'Po Supplier Site Code','PO_SUPPLIER_SITE_CODE','','','','ID020048','VARCHAR2','','','Po Supplier Site Code','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_SUPPLIER_NAME',201,'Po Supplier Name','PO_SUPPLIER_NAME','','','','ID020048','VARCHAR2','','','Po Supplier Name','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_TYPE',201,'Po Type','PO_TYPE','','','','ID020048','VARCHAR2','','','Po Type','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_NUMBER',201,'Po Number','PO_NUMBER','','','','ID020048','VARCHAR2','','','Po Number','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_BUYER_NAME',201,'Po Buyer Name','PO_BUYER_NAME','','','','ID020048','VARCHAR2','','','Po Buyer Name','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','REQ_PREPARER_NAME',201,'Req Preparer Name','REQ_PREPARER_NAME','','','','ID020048','VARCHAR2','','','Req Preparer Name','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','REQ_PREPARER_ID',201,'Req Preparer Id','REQ_PREPARER_ID','','','','ID020048','NUMBER','','','Req Preparer Id','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','REQ_CREATED_BY_NAME',201,'Req Created By Name','REQ_CREATED_BY_NAME','','','','ID020048','VARCHAR2','','','Req Created By Name','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','REQ_CREATED_BY',201,'Req Created By','REQ_CREATED_BY','','','','ID020048','NUMBER','','','Req Created By','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_CREATED_BY_NAME',201,'Po Created By Name','PO_CREATED_BY_NAME','','','','ID020048','VARCHAR2','','','Po Created By Name','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_CREATED_BY',201,'Po Created By','PO_CREATED_BY','','','','ID020048','NUMBER','','','Po Created By','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_CREATION_DATE',201,'Po Creation Date','PO_CREATION_DATE','','','','ID020048','DATE','','','Po Creation Date','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_LAST_UPDATED_BY_NAME',201,'Po Last Updated By Name','PO_LAST_UPDATED_BY_NAME','','','','ID020048','VARCHAR2','','','Po Last Updated By Name','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_LAST_UPDATED_BY',201,'Po Last Updated By','PO_LAST_UPDATED_BY','','','','ID020048','NUMBER','','','Po Last Updated By','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_LAST_UPDATE_DATE',201,'Po Last Update Date','PO_LAST_UPDATE_DATE','','','','ID020048','DATE','','','Po Last Update Date','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_HEADER_ID',201,'Po Header Id','PO_HEADER_ID','','','','ID020048','NUMBER','','','Po Header Id','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','MSI#WC#AVERAGE_UNITS',201,'Msi#Wc#Average Units','MSI#WC#AVERAGE_UNITS','','','','ID020048','VARCHAR2','','','Msi#Wc#Average Units','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','MSI#WC#TAXWARE_CODE',201,'Msi#Wc#Taxware Code','MSI#WC#TAXWARE_CODE','','','','ID020048','VARCHAR2','','','Msi#Wc#Taxware Code','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','MSI#WC#RESERVE_STOCK',201,'Msi#Wc#Reserve Stock','MSI#WC#RESERVE_STOCK','','','','ID020048','VARCHAR2','','','Msi#Wc#Reserve Stock','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','MSI#WC#AMU',201,'Msi#Wc#Amu','MSI#WC#AMU','','','','ID020048','VARCHAR2','','','Msi#Wc#Amu','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','MSI#WC#LAST_LEAD_TIME',201,'Msi#Wc#Last Lead Time','MSI#WC#LAST_LEAD_TIME','','','','ID020048','VARCHAR2','','','Msi#Wc#Last Lead Time','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','MSI#WC#GTP_INDICATOR',201,'Msi#Wc#Gtp Indicator','MSI#WC#GTP_INDICATOR','','','','ID020048','VARCHAR2','','','Msi#Wc#Gtp Indicator','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','MSI#WC#HAZMAT_CONTAINER',201,'Msi#Wc#Hazmat Container','MSI#WC#HAZMAT_CONTAINER','','','','ID020048','VARCHAR2','','','Msi#Wc#Hazmat Container','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','MSI#WC#HAZMAT_DESCRIPTION',201,'Msi#Wc#Hazmat Description','MSI#WC#HAZMAT_DESCRIPTION','','','','ID020048','VARCHAR2','','','Msi#Wc#Hazmat Description','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','MSI#WC#PRISM_PART_NUMBER',201,'Msi#Wc#Prism Part Number','MSI#WC#PRISM_PART_NUMBER','','','','ID020048','VARCHAR2','','','Msi#Wc#Prism Part Number','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','MSI#WC#YEARLY_DC_VELOCITY',201,'Msi#Wc#Yearly Dc Velocity','MSI#WC#YEARLY_DC_VELOCITY','','','','ID020048','VARCHAR2','','','Msi#Wc#Yearly Dc Velocity','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','MSI#WC#YEARLY_STORE_VELOCITY',201,'Msi#Wc#Yearly Store Velocity','MSI#WC#YEARLY_STORE_VELOCITY','','','','ID020048','VARCHAR2','','','Msi#Wc#Yearly Store Velocity','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','MSI#WC#DC_VELOCITY',201,'Msi#Wc#Dc Velocity','MSI#WC#DC_VELOCITY','','','','ID020048','VARCHAR2','','','Msi#Wc#Dc Velocity','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','MSI#WC#STORE_VELOCITY',201,'Msi#Wc#Store Velocity','MSI#WC#STORE_VELOCITY','','','','ID020048','VARCHAR2','','','Msi#Wc#Store Velocity','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','MSI#WC#ORM_D_FLAG',201,'Msi#Wc#Orm D Flag','MSI#WC#ORM_D_FLAG','','','','ID020048','VARCHAR2','','','Msi#Wc#Orm D Flag','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','MSI#WC#COUNTRY_OF_ORIGIN',201,'Msi#Wc#Country Of Origin','MSI#WC#COUNTRY_OF_ORIGIN','','','','ID020048','VARCHAR2','','','Msi#Wc#Country Of Origin','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','MSI#WC#CA_PROP_65',201,'Msi#Wc#Ca Prop 65','MSI#WC#CA_PROP_65','','','','ID020048','VARCHAR2','','','Msi#Wc#Ca Prop 65','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','MSI#HDS#SKU_DESCRIPTION',201,'Msi#Hds#Sku Description','MSI#HDS#SKU_DESCRIPTION','','','','ID020048','VARCHAR2','','','Msi#Hds#Sku Description','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','MSI#HDS#UPC_PRIMARY',201,'Msi#Hds#Upc Primary','MSI#HDS#UPC_PRIMARY','','','','ID020048','VARCHAR2','','','Msi#Hds#Upc Primary','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','MSI#HDS#UNSPSC_CODE',201,'Msi#Hds#Unspsc Code','MSI#HDS#UNSPSC_CODE','','','','ID020048','VARCHAR2','','','Msi#Hds#Unspsc Code','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','MSI#HDS#VENDOR_PART_NUMBER',201,'Msi#Hds#Vendor Part Number','MSI#HDS#VENDOR_PART_NUMBER','','','','ID020048','VARCHAR2','','','Msi#Hds#Vendor Part Number','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','MSI#HDS#PRODUCT_ID',201,'Msi#Hds#Product Id','MSI#HDS#PRODUCT_ID','','','','ID020048','VARCHAR2','','','Msi#Hds#Product Id','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','MSI#HDS#INVOICE_UOM',201,'Msi#Hds#Invoice Uom','MSI#HDS#INVOICE_UOM','','','','ID020048','VARCHAR2','','','Msi#Hds#Invoice Uom','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','MSI#HDS#DROP_SHIPMENT_ELIGAB',201,'Msi#Hds#Drop Shipment Eligab','MSI#HDS#DROP_SHIPMENT_ELIGAB','','','','ID020048','VARCHAR2','','','Msi#Hds#Drop Shipment Eligab','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','MSI#HDS#LOB',201,'Msi#Hds#Lob','MSI#HDS#LOB','','','','ID020048','VARCHAR2','','','Msi#Hds#Lob','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','GCC#BRANCH',201,'Gcc#Branch','GCC#BRANCH','','','','ID020048','VARCHAR2','','','Gcc#Branch','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PROJECT_ID',201,'Project Id','PROJECT_ID','','','','ID020048','NUMBER','','','Project Id','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PROJECT_NAME',201,'Project Name','PROJECT_NAME','','','','ID020048','VARCHAR2','','','Project Name','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PROJECT_NUMBER',201,'Project Number','PROJECT_NUMBER','','','','ID020048','VARCHAR2','','','Project Number','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','CODE_COMBINATION_ID',201,'Code Combination Id','CODE_COMBINATION_ID','','','','ID020048','NUMBER','','','Code Combination Id','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','AGENT_ID',201,'Agent Id','AGENT_ID','','','','ID020048','NUMBER','','','Agent Id','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','LOCATION_ID',201,'Location Id','LOCATION_ID','','','','ID020048','NUMBER','','','Location Id','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','INVENTORY_ITEM_ID',201,'Inventory Item Id','INVENTORY_ITEM_ID','','','','ID020048','NUMBER','','','Inventory Item Id','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','VENDOR_ID',201,'Vendor Id','VENDOR_ID','','','','ID020048','NUMBER','','','Vendor Id','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','ORG_ID',201,'Org Id','ORG_ID','','','','ID020048','NUMBER','','','Org Id','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','OPERATING_UNIT',201,'Operating Unit','OPERATING_UNIT','','','','ID020048','VARCHAR2','','','Operating Unit','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','REQ_LINE_QTY',201,'Req Line Qty','REQ_LINE_QTY','','','','ID020048','NUMBER','','','Req Line Qty','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','REQ_LINE_UNIT_PRICE',201,'Req Line Unit Price','REQ_LINE_UNIT_PRICE','','','','ID020048','NUMBER','','','Req Line Unit Price','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','REQ_LINE_ITEM_DESCRIPTION',201,'Req Line Item Description','REQ_LINE_ITEM_DESCRIPTION','','','','ID020048','VARCHAR2','','','Req Line Item Description','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','REQ_LINE_NUM',201,'Req Line Num','REQ_LINE_NUM','','','','ID020048','NUMBER','','','Req Line Num','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','REQ_TYPE',201,'Req Type','REQ_TYPE','','','','ID020048','VARCHAR2','','','Req Type','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','REQ_STATUS',201,'Req Status','REQ_STATUS','','','','ID020048','VARCHAR2','','','Req Status','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','REQ_DESCRIPTION',201,'Req Description','REQ_DESCRIPTION','','','','ID020048','VARCHAR2','','','Req Description','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','REQ_NUMBER',201,'Req Number','REQ_NUMBER','','','','ID020048','VARCHAR2','','','Req Number','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','REQUISITION_HEADER_ID',201,'Requisition Header Id','REQUISITION_HEADER_ID','','','','ID020048','NUMBER','','','Requisition Header Id','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','REQUISITION_LINE_ID',201,'Requisition Line Id','REQUISITION_LINE_ID','','','','ID020048','NUMBER','','','Requisition Line Id','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','DISTRIBUTION_ID',201,'Distribution Id','DISTRIBUTION_ID','','','','ID020048','NUMBER','','','Distribution Id','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_VARIANCE_ACCOUNT',201,'Po Variance Account','PO_VARIANCE_ACCOUNT','','','','ID020048','VARCHAR2','','','Po Variance Account','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_VARIANCE_CCID',201,'Po Variance Ccid','PO_VARIANCE_CCID','','','','ID020048','NUMBER','','','Po Variance Ccid','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_ACCRUAL_ACCOUNT',201,'Po Accrual Account','PO_ACCRUAL_ACCOUNT','','','','ID020048','VARCHAR2','','','Po Accrual Account','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_ACCRUAL_CCID',201,'Po Accrual Ccid','PO_ACCRUAL_CCID','','','','ID020048','NUMBER','','','Po Accrual Ccid','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_CHARGE_ACCOUNT',201,'Po Charge Account','PO_CHARGE_ACCOUNT','','','','ID020048','VARCHAR2','','','Po Charge Account','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_CHARGE_CCID',201,'Po Charge Ccid','PO_CHARGE_CCID','','','','ID020048','NUMBER','','','Po Charge Ccid','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_SET_OF_BOOKS',201,'Po Set Of Books','PO_SET_OF_BOOKS','','','','ID020048','VARCHAR2','','','Po Set Of Books','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','SHIPMENT_RECEIPT_DATE',201,'Shipment Receipt Date','SHIPMENT_RECEIPT_DATE','','','','ID020048','DATE','','','Shipment Receipt Date','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_DISTRIBUTION_NUM',201,'Po Distribution Num','PO_DISTRIBUTION_NUM','','','','ID020048','NUMBER','','','Po Distribution Num','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_DISTRIBUTION_ID',201,'Po Distribution Id','PO_DISTRIBUTION_ID','','','','ID020048','NUMBER','','','Po Distribution Id','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_ACCRUE_ON_RECEIPT',201,'Po Accrue On Receipt','PO_ACCRUE_ON_RECEIPT','','','','ID020048','VARCHAR2','','','Po Accrue On Receipt','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_RECEIVE_CLOSE_TOLERANCE',201,'Po Receive Close Tolerance','PO_RECEIVE_CLOSE_TOLERANCE','','','','ID020048','NUMBER','','','Po Receive Close Tolerance','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_INVOICE_CLOSE_TOLERANCE',201,'Po Invoice Close Tolerance','PO_INVOICE_CLOSE_TOLERANCE','','','','ID020048','NUMBER','','','Po Invoice Close Tolerance','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_DAYS_LATE_RECEIPT_ALLOWED',201,'Po Days Late Receipt Allowed','PO_DAYS_LATE_RECEIPT_ALLOWED','','','','ID020048','NUMBER','','','Po Days Late Receipt Allowed','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_DAYS_EARLY_RECEIPT_ALLOWED',201,'Po Days Early Receipt Allowed','PO_DAYS_EARLY_RECEIPT_ALLOWED','','','','ID020048','NUMBER','','','Po Days Early Receipt Allowed','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_QTY_RCV_TOLERANCE',201,'Po Qty Rcv Tolerance','PO_QTY_RCV_TOLERANCE','','','','ID020048','NUMBER','','','Po Qty Rcv Tolerance','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_3_WAY_MATCH',201,'Po 3 Way Match','PO_3_WAY_MATCH','','','','ID020048','VARCHAR2','','','Po 3 Way Match','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_4_WAY_MATCH',201,'Po 4 Way Match','PO_4_WAY_MATCH','','','','ID020048','VARCHAR2','','','Po 4 Way Match','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_LAST_ACCEPT_DATE',201,'Po Last Accept Date','PO_LAST_ACCEPT_DATE','','','','ID020048','DATE','','','Po Last Accept Date','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_PROMISED_DATE',201,'Po Promised Date','PO_PROMISED_DATE','','','','ID020048','DATE','','','Po Promised Date','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_NEED_BY_DATE',201,'Po Need By Date','PO_NEED_BY_DATE','','','','ID020048','DATE','','','Po Need By Date','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_SHIPMENT_TYPE',201,'Po Shipment Type','PO_SHIPMENT_TYPE','','','','ID020048','VARCHAR2','','','Po Shipment Type','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_SHIPMENT_NUM',201,'Po Shipment Num','PO_SHIPMENT_NUM','','','','ID020048','NUMBER','','','Po Shipment Num','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_LINE_LOCATION_ID',201,'Po Line Location Id','PO_LINE_LOCATION_ID','','','','ID020048','NUMBER','','','Po Line Location Id','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','POL_CLOSED_REASON',201,'Pol Closed Reason','POL_CLOSED_REASON','','','','ID020048','VARCHAR2','','','Pol Closed Reason','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','POL_CLOSED_DATE',201,'Pol Closed Date','POL_CLOSED_DATE','','','','ID020048','DATE','','','Pol Closed Date','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','POL_CLOSED_CODE',201,'Pol Closed Code','POL_CLOSED_CODE','','','','ID020048','VARCHAR2','','','Pol Closed Code','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_TYPE_1099',201,'Po Type 1099','PO_TYPE_1099','','','','ID020048','VARCHAR2','','','Po Type 1099','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_TAX_NAME',201,'Po Tax Name','PO_TAX_NAME','','','','ID020048','VARCHAR2','','','Po Tax Name','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_TAXABLE_FLAG',201,'Po Taxable Flag','PO_TAXABLE_FLAG','','','','ID020048','VARCHAR2','','','Po Taxable Flag','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','POL_CANCEL_REASON',201,'Pol Cancel Reason','POL_CANCEL_REASON','','','','ID020048','VARCHAR2','','','Pol Cancel Reason','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','POL_CANCEL_DATE',201,'Pol Cancel Date','POL_CANCEL_DATE','','','','ID020048','DATE','','','Pol Cancel Date','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','POL_CANCEL',201,'Pol Cancel','POL_CANCEL','','','','ID020048','VARCHAR2','','','Pol Cancel','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_MARKET_PRICE',201,'Po Market Price','PO_MARKET_PRICE','','','','ID020048','NUMBER','','','Po Market Price','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_LIST_PRICE',201,'Po List Price','PO_LIST_PRICE','','','','ID020048','NUMBER','','','Po List Price','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_UOM',201,'Po Uom','PO_UOM','','','','ID020048','VARCHAR2','','','Po Uom','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_ITEM_REVISION',201,'Po Item Revision','PO_ITEM_REVISION','','','','ID020048','VARCHAR2','','','Po Item Revision','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','REQUESTOR_EMAIL',201,'Requestor Email','REQUESTOR_EMAIL','','','','ID020048','VARCHAR2','','','Requestor Email','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','GCC#50368#SUBACCOUNT#DESCR',201,'Gcc#50368#Subaccount#Descr','GCC#50368#SUBACCOUNT#DESCR','','','','ID020048','VARCHAR2','','','Gcc#50368#Subaccount#Descr','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','GCC#50368#SUBACCOUNT',201,'Gcc#50368#Subaccount','GCC#50368#SUBACCOUNT','','','','ID020048','VARCHAR2','','','Gcc#50368#Subaccount','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','GCC#50368#PRODUCT#DESCR',201,'Gcc#50368#Product#Descr','GCC#50368#PRODUCT#DESCR','','','','ID020048','VARCHAR2','','','Gcc#50368#Product#Descr','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','GCC#50368#PRODUCT',201,'Gcc#50368#Product','GCC#50368#PRODUCT','','','','ID020048','VARCHAR2','','','Gcc#50368#Product','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','GCC#50368#FUTURE_USE#DESCR',201,'Gcc#50368#Future Use#Descr','GCC#50368#FUTURE_USE#DESCR','','','','ID020048','VARCHAR2','','','Gcc#50368#Future Use#Descr','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','GCC#50368#FUTURE_USE',201,'Gcc#50368#Future Use','GCC#50368#FUTURE_USE','','','','ID020048','VARCHAR2','','','Gcc#50368#Future Use','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','GCC#50368#DIVISION#DESCR',201,'Gcc#50368#Division#Descr','GCC#50368#DIVISION#DESCR','','','','ID020048','VARCHAR2','','','Gcc#50368#Division#Descr','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','GCC#50368#DIVISION',201,'Gcc#50368#Division','GCC#50368#DIVISION','','','','ID020048','VARCHAR2','','','Gcc#50368#Division','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','GCC#50368#DEPARTMENT#DESCR',201,'Gcc#50368#Department#Descr','GCC#50368#DEPARTMENT#DESCR','','','','ID020048','VARCHAR2','','','Gcc#50368#Department#Descr','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','GCC#50368#DEPARTMENT',201,'Gcc#50368#Department','GCC#50368#DEPARTMENT','','','','ID020048','VARCHAR2','','','Gcc#50368#Department','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','GCC#50368#ACCOUNT#DESCR',201,'Gcc#50368#Account#Descr','GCC#50368#ACCOUNT#DESCR','','','','ID020048','VARCHAR2','','','Gcc#50368#Account#Descr','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','GCC#50368#ACCOUNT',201,'Gcc#50368#Account','GCC#50368#ACCOUNT','','','','ID020048','VARCHAR2','','','Gcc#50368#Account','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','GCC#50328#FUTURE_USE_2#DESCR',201,'Gcc#50328#Future Use 2#Descr','GCC#50328#FUTURE_USE_2#DESCR','','','','ID020048','VARCHAR2','','','Gcc#50328#Future Use 2#Descr','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','GCC#50328#FUTURE_USE_2',201,'Gcc#50328#Future Use 2','GCC#50328#FUTURE_USE_2','','','','ID020048','VARCHAR2','','','Gcc#50328#Future Use 2','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','GCC#50328#FURTURE_USE#DESCR',201,'Gcc#50328#Furture Use#Descr','GCC#50328#FURTURE_USE#DESCR','','','','ID020048','VARCHAR2','','','Gcc#50328#Furture Use#Descr','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','GCC#50328#FURTURE_USE',201,'Gcc#50328#Furture Use','GCC#50328#FURTURE_USE','','','','ID020048','VARCHAR2','','','Gcc#50328#Furture Use','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','GCC#50328#PROJECT_CODE#DESCR',201,'Gcc#50328#Project Code#Descr','GCC#50328#PROJECT_CODE#DESCR','','','','ID020048','VARCHAR2','','','Gcc#50328#Project Code#Descr','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','GCC#50328#PROJECT_CODE',201,'Gcc#50328#Project Code','GCC#50328#PROJECT_CODE','','','','ID020048','VARCHAR2','','','Gcc#50328#Project Code','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','GCC#50328#ACCOUNT#DESCR',201,'Gcc#50328#Account#Descr','GCC#50328#ACCOUNT#DESCR','','','','ID020048','VARCHAR2','','','Gcc#50328#Account#Descr','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','GCC#50328#ACCOUNT',201,'Gcc#50328#Account','GCC#50328#ACCOUNT','','','','ID020048','VARCHAR2','','','Gcc#50328#Account','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','GCC#50328#COST_CENTER#DESCR',201,'Gcc#50328#Cost Center#Descr','GCC#50328#COST_CENTER#DESCR','','','','ID020048','VARCHAR2','','','Gcc#50328#Cost Center#Descr','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','GCC#50328#COST_CENTER',201,'Gcc#50328#Cost Center','GCC#50328#COST_CENTER','','','','ID020048','VARCHAR2','','','Gcc#50328#Cost Center','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','GCC#50328#LOCATION#DESCR',201,'Gcc#50328#Location#Descr','GCC#50328#LOCATION#DESCR','','','','ID020048','VARCHAR2','','','Gcc#50328#Location#Descr','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','GCC#50328#LOCATION',201,'Gcc#50328#Location','GCC#50328#LOCATION','','','','ID020048','VARCHAR2','','','Gcc#50328#Location','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','GCC#50328#PRODUCT#DESCR',201,'Gcc#50328#Product#Descr','GCC#50328#PRODUCT#DESCR','','','','ID020048','VARCHAR2','','','Gcc#50328#Product#Descr','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','GCC#50328#PRODUCT',201,'Gcc#50328#Product','GCC#50328#PRODUCT','','','','ID020048','VARCHAR2','','','Gcc#50328#Product','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','POH#STANDARDP#FOB_TERMS_TAB',201,'Poh#Standardp#Fob Terms Tab','POH#STANDARDP#FOB_TERMS_TAB','','','','ID020048','VARCHAR2','','','Poh#Standardp#Fob Terms Tab','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','POH#STANDARDP#CARRIER_TERMS_',201,'Poh#Standardp#Carrier Terms ','POH#STANDARDP#CARRIER_TERMS_','','','','ID020048','VARCHAR2','','','Poh#Standardp#Carrier Terms ','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','POH#STANDARDP#FREIGHT_TERMS_',201,'Poh#Standardp#Freight Terms ','POH#STANDARDP#FREIGHT_TERMS_','','','','ID020048','VARCHAR2','','','Poh#Standardp#Freight Terms ','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','POH#STANDARDP#NEED_BY_DATE',201,'Poh#Standardp#Need By Date','POH#STANDARDP#NEED_BY_DATE','','','','ID020048','VARCHAR2','','','Poh#Standardp#Need By Date','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','MSI#WC#HAZMAT_PACKAGING_GROU',201,'Msi#Wc#Hazmat Packaging Grou','MSI#WC#HAZMAT_PACKAGING_GROU','','','','ID020048','VARCHAR2','','','Msi#Wc#Hazmat Packaging Grou','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','MSI#WC#MSDS_#',201,'Msi#Wc#Msds #','MSI#WC#MSDS_#','','','','ID020048','VARCHAR2','','','Msi#Wc#Msds #','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','MSI#WC#VOC_SUB_CATEGORY',201,'Msi#Wc#Voc Sub Category','MSI#WC#VOC_SUB_CATEGORY','','','','ID020048','VARCHAR2','','','Msi#Wc#Voc Sub Category','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','MSI#WC#VOC_CATEGORY',201,'Msi#Wc#Voc Category','MSI#WC#VOC_CATEGORY','','','','ID020048','VARCHAR2','','','Msi#Wc#Voc Category','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','MSI#WC#PESTICIDE_FLAG_STATE',201,'Msi#Wc#Pesticide Flag State','MSI#WC#PESTICIDE_FLAG_STATE','','','','ID020048','VARCHAR2','','','Msi#Wc#Pesticide Flag State','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','MSI#WC#VOC_GL',201,'Msi#Wc#Voc Gl','MSI#WC#VOC_GL','','','','ID020048','VARCHAR2','','','Msi#Wc#Voc Gl','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','MSI#WC#CALC_LEAD_TIME',201,'Msi#Wc#Calc Lead Time','MSI#WC#CALC_LEAD_TIME','','','','ID020048','VARCHAR2','','','Msi#Wc#Calc Lead Time','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','MSI#WC#PESTICIDE_FLAG',201,'Msi#Wc#Pesticide Flag','MSI#WC#PESTICIDE_FLAG','','','','ID020048','VARCHAR2','','','Msi#Wc#Pesticide Flag','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','MSI#WC#KEEP_ITEM_ACTIVE',201,'Msi#Wc#Keep Item Active','MSI#WC#KEEP_ITEM_ACTIVE','','','','ID020048','VARCHAR2','','','Msi#Wc#Keep Item Active','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','MSI#WC#IMPORT_DUTY_',201,'Msi#Wc#Import Duty ','MSI#WC#IMPORT_DUTY_','','','','ID020048','VARCHAR2','','','Msi#Wc#Import Duty ','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','MSI#WC#PRODUCT_CODE',201,'Msi#Wc#Product Code','MSI#WC#PRODUCT_CODE','','','','ID020048','VARCHAR2','','','Msi#Wc#Product Code','','','');
--Inserting View Components for XXCUS_GSC_PO_DETAIL_WITH_REQ_V
--Inserting View Component Joins for XXCUS_GSC_PO_DETAIL_WITH_REQ_V
END;
/
/*set scan on define on
prompt Creating Report LOV Data for HDS GSC iPro PO Aging Report
set scan off define off*/
DECLARE
BEGIN 
--Inserting Report LOVs - HDS GSC iPro PO Aging Report
xxeis.eis_rs_ins.lov( 201,'select agent_name buyer_name from po_agents_v','','EIS_PO_BUYER_LOV','List of Values for Buyer','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 201,'select poh.segment1 PO_NUMBER,HOU.name OPERATING_UNIT FROM PO_HEADERS_ALL POH,HR_OPERATING_UNITS HOU
where POH.ORG_ID=HOU.ORGANIZATION_ID
and exists(select 1 from XXEIS.EIS_MO_ORG_TMP_V
            WHERE ORG_ID=POH.ORG_ID)','','EIS_PO_PURCHASE_ORDER_NUM_LOV','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'select poh.segment1 PO_NUMBER,HOU.name OPERATING_UNIT FROM PO_HEADERS_ALL POH,HR_OPERATING_UNITS HOU
where POH.ORG_ID=HOU.ORGANIZATION_ID
and exists(select 1 from XXEIS.EIS_MO_ORG_TMP_V
            WHERE ORG_ID=POH.ORG_ID)','','EIS_PO_PURCHASE_ORDER_NUM_LOV','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'select PRH.SEGMENT1 Requisition_number,HOU.name OPERATING_UNIT from PO_REQUISITION_HEADERS_ALL PRH,HR_OPERATING_UNITS HOU
where PRH.ORG_ID=HOU.ORGANIZATION_ID
and exists(select 1 from XXEIS.EIS_MO_ORG_TMP_V
            WHERE ORG_ID=PRH.ORG_ID)
order by PRH.SEGMENT1','','EIS_PO_REQ_LOV','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'select PRH.SEGMENT1 Requisition_number,HOU.name OPERATING_UNIT from PO_REQUISITION_HEADERS_ALL PRH,HR_OPERATING_UNITS HOU
where PRH.ORG_ID=HOU.ORGANIZATION_ID
and exists(select 1 from XXEIS.EIS_MO_ORG_TMP_V
            WHERE ORG_ID=PRH.ORG_ID)
order by PRH.SEGMENT1','','EIS_PO_REQ_LOV','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'select vendor_name from po_vendors','','EIS_PO_SUPPLIER_LOV','','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 201,'select vendor_name from po_vendors','','EIS_PO_SUPPLIER_LOV','','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 201,'SELECT lookup_code, meaning FROM apps.mfg_lookups
WHERE LOOKUP_TYPE = ''INV_SRS_PRECISION''
and enabled_flag = ''Y''
order by LOOKUP_CODE','','EIS_PO_NUMBER_PRECISION_LOV','','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 201,'SELECT hou.NAME organization_name,
       TO_CHAR (date_from,
                NVL (fnd_profile.VALUE (''ICX_DATE_FORMAT_MASK''),
                     ''DD-MON-YYYY'')
               ) date_from,
       TO_CHAR (date_to,
                NVL (fnd_profile.VALUE (''ICX_DATE_FORMAT_MASK''),
                     ''DD-MON-YYYY'')
               ) date_to
  FROM hr_operating_units hou,
       mo_glob_org_access_tmp tmp
 WHERE hou.organization_id = tmp.organization_id','','EIS_MULTI_OPERATING_UNIT_LOV','This LOV shows all the operating units, to which user has access.','XXEIS_RS_ADMIN',NULL,'N','','');
END;
/
/*set scan on define on
prompt Creating Report Data for HDS GSC iPro PO Aging Report
set scan off define off*/
DECLARE
BEGIN 
--Deleting Report data - HDS GSC iPro PO Aging Report
xxeis.eis_rs_utility.delete_report_rows( 'HDS GSC iPro PO Aging Report' );
--Inserting Report - HDS GSC iPro PO Aging Report
xxeis.eis_rs_ins.r( 201,'HDS GSC iPro PO Aging Report','','This report lists purchase order, line, shipment and distribution details along with purchase order requisition, line and distribution details created within a specified date range, supplier range, buyer(s), purchase requisition or purchase order range. This report can be used to track the purchase orders back to the requisitions thereby monitoring that all purchase orders have been created against proper requisitions. This also includes the Receipt Date and Requestor Created By.

Per Project: 24066

','','','','ID020048','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','Y','','','ID020048','','N','iProcurement','','CSV,Pivot Excel,EXCEL,','');
--Inserting Report Columns - HDS GSC iPro PO Aging Report
xxeis.eis_rs_ins.rc( 'HDS GSC iPro PO Aging Report',201,'GCC#50328#PROJECT_CODE','Gcc#50328#Project Code','Gcc#50328#Project Code','','','','','33','N','','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','');
xxeis.eis_rs_ins.rc( 'HDS GSC iPro PO Aging Report',201,'GCC#50328#PROJECT_CODE#DESCR','Gcc#50328#Project Code#Descr','Gcc#50328#Project Code#Descr','','','','','34','N','','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','');
xxeis.eis_rs_ins.rc( 'HDS GSC iPro PO Aging Report',201,'POL_CREATED_BY','Pol Created By','Pol Created By','','','','','37','N','','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','');
xxeis.eis_rs_ins.rc( 'HDS GSC iPro PO Aging Report',201,'PO_BUYER_NAME','Po Buyer Name','Po Buyer Name','','','','','38','N','','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','');
xxeis.eis_rs_ins.rc( 'HDS GSC iPro PO Aging Report',201,'PO_CLOSED_CODE','Po Closed Code','Po Closed Code','','','','','27','N','','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','');
xxeis.eis_rs_ins.rc( 'HDS GSC iPro PO Aging Report',201,'PO_CREATED_BY','Po Created By','Po Created By','','','','','39','N','','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','');
xxeis.eis_rs_ins.rc( 'HDS GSC iPro PO Aging Report',201,'PO_LAST_ACCEPT_DATE','Po Last Accept Date','Po Last Accept Date','','','','','31','N','','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','');
xxeis.eis_rs_ins.rc( 'HDS GSC iPro PO Aging Report',201,'PO_LAST_UPDATED_BY','Po Last Updated By','Po Last Updated By','','','','','40','N','','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','');
xxeis.eis_rs_ins.rc( 'HDS GSC iPro PO Aging Report',201,'PO_NEED_BY_DATE','Po Need By Date','Po Need By Date','','','','','29','N','','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','');
xxeis.eis_rs_ins.rc( 'HDS GSC iPro PO Aging Report',201,'PO_PROMISED_DATE','Po Promised Date','Po Promised Date','','','','','30','N','','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','');
xxeis.eis_rs_ins.rc( 'HDS GSC iPro PO Aging Report',201,'PO_SHIP_TO_LOCATION','Po Ship To Location','Po Ship To Location','','','','','36','N','','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','');
xxeis.eis_rs_ins.rc( 'HDS GSC iPro PO Aging Report',201,'PO_SUPPLIER_SITE_CODE','Po Supplier Site Code','Po Supplier Site Code','','','','','32','N','','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','');
xxeis.eis_rs_ins.rc( 'HDS GSC iPro PO Aging Report',201,'REQUESTOR_EMAIL','Requestor Email','Requestor Email','','','','','35','N','','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','');
xxeis.eis_rs_ins.rc( 'HDS GSC iPro PO Aging Report',201,'REQ_PREPARER_NAME','Req Preparer Name','Req Preparer Name','','','','','41','N','','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','');
xxeis.eis_rs_ins.rc( 'HDS GSC iPro PO Aging Report',201,'POL_CLOSED_CODE','Pol Closed Code','Pol Closed Code','','','','','28','N','','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','');
xxeis.eis_rs_ins.rc( 'HDS GSC iPro PO Aging Report',201,'OPERATING_UNIT','Operating Unit','Operating Unit','','','','','1','N','','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','');
xxeis.eis_rs_ins.rc( 'HDS GSC iPro PO Aging Report',201,'PO_DISTRIBUTION_NUM','Po Distribution Num','Po Distribution Num','','','','','7','N','','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','');
xxeis.eis_rs_ins.rc( 'HDS GSC iPro PO Aging Report',201,'PO_ITEM_CATEGORY','Po Item Category','Po Item Category','','','','','10','N','','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','');
xxeis.eis_rs_ins.rc( 'HDS GSC iPro PO Aging Report',201,'PO_LINE_NUM','Po Line Num','Po Line Num','','','','','6','N','','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','');
xxeis.eis_rs_ins.rc( 'HDS GSC iPro PO Aging Report',201,'PO_NUMBER','Po Number','Po Number','','','','','2','N','','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','');
xxeis.eis_rs_ins.rc( 'HDS GSC iPro PO Aging Report',201,'PO_SUPPLIER_NAME','Po Supplier Name','Po Supplier Name','','','','','5','N','','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','');
xxeis.eis_rs_ins.rc( 'HDS GSC iPro PO Aging Report',201,'REQ_DESCRIPTION','Req Description','Req Description','','','','','4','N','','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','');
xxeis.eis_rs_ins.rc( 'HDS GSC iPro PO Aging Report',201,'REQ_LINE_NUM','Req Line Num','Req Line Num','','','','','8','N','','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','');
xxeis.eis_rs_ins.rc( 'HDS GSC iPro PO Aging Report',201,'REQ_LINE_QTY','Req Line Qty','Req Line Qty','','','','','9','N','','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','');
xxeis.eis_rs_ins.rc( 'HDS GSC iPro PO Aging Report',201,'REQ_NUMBER','Req Number','Req Number','','','','','3','N','','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','');
xxeis.eis_rs_ins.rc( 'HDS GSC iPro PO Aging Report',201,'PO_CREATION_DATE','Po Creation Date','Po Creation Date','','','','','13','N','','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','');
xxeis.eis_rs_ins.rc( 'HDS GSC iPro PO Aging Report',201,'PO_ITEM_DESCRIPTION','Po Item Description','Po Item Description','','','','','16','N','','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','');
xxeis.eis_rs_ins.rc( 'HDS GSC iPro PO Aging Report',201,'PO_SHIPMENT_NUM','Po Shipment Num','Po Shipment Num','','','','','12','N','','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','');
xxeis.eis_rs_ins.rc( 'HDS GSC iPro PO Aging Report',201,'PO_STATUS','Po Status','Po Status','','','','','14','N','','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','');
xxeis.eis_rs_ins.rc( 'HDS GSC iPro PO Aging Report',201,'PO_TYPE','Po Type','Po Type','','','','','15','N','','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','');
xxeis.eis_rs_ins.rc( 'HDS GSC iPro PO Aging Report',201,'PO_UNIT_PRICE','Po Unit Price','Po Unit Price','','','','','18','N','','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','');
xxeis.eis_rs_ins.rc( 'HDS GSC iPro PO Aging Report',201,'PO_UOM','Po Uom','Po Uom','','','','','17','N','','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','');
xxeis.eis_rs_ins.rc( 'HDS GSC iPro PO Aging Report',201,'REQ_LINE_ITEM_DESCRIPTION','Req Line Item Description','Req Line Item Description','','','','','11','N','','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','');
xxeis.eis_rs_ins.rc( 'HDS GSC iPro PO Aging Report',201,'PO_ACCRUAL_ACCOUNT','Po Accrual Account','Po Accrual Account','','','','','25','N','','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','');
xxeis.eis_rs_ins.rc( 'HDS GSC iPro PO Aging Report',201,'PO_AMOUNT_BILLED','Po Amount Billed','Po Amount Billed','','','','','23','N','','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','');
xxeis.eis_rs_ins.rc( 'HDS GSC iPro PO Aging Report',201,'PO_AMOUNT_CANCELLED','Po Amount Cancelled','Po Amount Cancelled','','','','','20','N','','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','');
xxeis.eis_rs_ins.rc( 'HDS GSC iPro PO Aging Report',201,'PO_AMOUNT_DUE','Po Amount Due','Po Amount Due','','','','','22','N','','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','');
xxeis.eis_rs_ins.rc( 'HDS GSC iPro PO Aging Report',201,'PO_AMOUNT_ORDERED','Po Amount Ordered','Po Amount Ordered','','','','','19','N','','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','');
xxeis.eis_rs_ins.rc( 'HDS GSC iPro PO Aging Report',201,'PO_AMOUNT_RECEIVED','Po Amount Received','Po Amount Received','','','','','21','N','','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','');
xxeis.eis_rs_ins.rc( 'HDS GSC iPro PO Aging Report',201,'PO_CHARGE_ACCOUNT','Po Charge Account','Po Charge Account','','','','','24','N','','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','');
xxeis.eis_rs_ins.rc( 'HDS GSC iPro PO Aging Report',201,'PO_VARIANCE_ACCOUNT','Po Variance Account','Po Variance Account','','','','','26','N','','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','');
xxeis.eis_rs_ins.rc( 'HDS GSC iPro PO Aging Report',201,'REQ_CREATED_BY_NAME','Req Created By Name','Req Created By Name','','','','','42','','Y','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','');
xxeis.eis_rs_ins.rc( 'HDS GSC iPro PO Aging Report',201,'SHIPMENT_RECEIPT_DATE','Shipment Receipt Date','Shipment Receipt Date','','','','','43','','Y','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','');
--Inserting Report Parameters - HDS GSC iPro PO Aging Report
xxeis.eis_rs_ins.rp( 'HDS GSC iPro PO Aging Report',201,'OPerating unit','OPerating unit','OPERATING_UNIT','IN','EIS_MULTI_OPERATING_UNIT_LOV','''HD Supply Corp USD - Org''','VARCHAR2','N','Y','1','','Y','CONSTANT','ID020048','Y','N','','','');
xxeis.eis_rs_ins.rp( 'HDS GSC iPro PO Aging Report',201,'PO Date From','From Date (Purchase Order Date)','','IN','','','DATE','N','Y','2','','N','CONSTANT','ID020048','Y','N','','','');
xxeis.eis_rs_ins.rp( 'HDS GSC iPro PO Aging Report',201,'PO Date To','To Date (Purchase Order Date)','','IN','','','DATE','N','Y','3','','N','CONSTANT','ID020048','Y','N','','','');
xxeis.eis_rs_ins.rp( 'HDS GSC iPro PO Aging Report',201,'Supplier From','Supplier From','PO_SUPPLIER_NAME','IN','EIS_PO_SUPPLIER_LOV','','VARCHAR2','N','Y','4','','N','CONSTANT','ID020048','Y','N','','','');
xxeis.eis_rs_ins.rp( 'HDS GSC iPro PO Aging Report',201,'Supplier To','Supplier To','PO_SUPPLIER_NAME','IN','EIS_PO_SUPPLIER_LOV','','VARCHAR2','N','Y','5','','N','CONSTANT','ID020048','Y','N','','','');
xxeis.eis_rs_ins.rp( 'HDS GSC iPro PO Aging Report',201,'Buyer','Buyer','PO_BUYER_NAME','IN','EIS_PO_BUYER_LOV','','VARCHAR2','N','Y','6','','Y','CONSTANT','ID020048','Y','N','','','');
xxeis.eis_rs_ins.rp( 'HDS GSC iPro PO Aging Report',201,'PO Req From','PO Requisition From','REQ_NUMBER','IN','EIS_PO_REQ_LOV','','VARCHAR2','N','Y','7','','N','CONSTANT','ID020048','Y','N','','','');
xxeis.eis_rs_ins.rp( 'HDS GSC iPro PO Aging Report',201,'PO Req To','PO Requisition To','REQ_NUMBER','IN','EIS_PO_REQ_LOV','','VARCHAR2','N','Y','8','','N','CONSTANT','ID020048','Y','N','','','');
xxeis.eis_rs_ins.rp( 'HDS GSC iPro PO Aging Report',201,'PO From','Purchase Order From','PO_NUMBER','IN','EIS_PO_PURCHASE_ORDER_NUM_LOV','','VARCHAR2','N','Y','9','','N','CONSTANT','ID020048','Y','N','','','');
xxeis.eis_rs_ins.rp( 'HDS GSC iPro PO Aging Report',201,'PO To','Purchase Order To','PO_NUMBER','IN','EIS_PO_PURCHASE_ORDER_NUM_LOV','','VARCHAR2','N','Y','10','','N','CONSTANT','ID020048','Y','N','','','');
xxeis.eis_rs_ins.rp( 'HDS GSC iPro PO Aging Report',201,'Dynamic Precision Option','','','IN','EIS_PO_NUMBER_PRECISION_LOV','SELECT nvl(fnd_profile.VALUE(''REPORT_QUANTITY_PRECISION''),2) FROM dual','NUMERIC','N','Y','11','','N','SQL','ID020048','Y','N','','','');
xxeis.eis_rs_ins.rp( 'HDS GSC iPro PO Aging Report',201,'PaTT Project Code','Accounting Flexfield : Segment ''Project Code'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#PROJECT_CODE','IN','','','VARCHAR2','N','Y','12','','Y','CONSTANT','ID020048','Y','N','','','');
--Inserting Report Conditions - HDS GSC iPro PO Aging Report
xxeis.eis_rs_ins.rcn( 'HDS GSC iPro PO Aging Report',201,'OPERATING_UNIT','IN',':OPerating unit','','','Y','1','Y','ID020048');
xxeis.eis_rs_ins.rcn( 'HDS GSC iPro PO Aging Report',201,'PO_BUYER_NAME','IN',':Buyer','','','Y','6','Y','ID020048');
xxeis.eis_rs_ins.rcn( 'HDS GSC iPro PO Aging Report',201,'GCC#50328#PROJECT_CODE','IN',':PaTT Project Code','','','Y','12','Y','ID020048');
xxeis.eis_rs_ins.rcn( 'HDS GSC iPro PO Aging Report',201,'TRUNC(PO_CREATION)','>=',':PO Date From','','','Y','2','Y','ID020048');
xxeis.eis_rs_ins.rcn( 'HDS GSC iPro PO Aging Report',201,'TRUNC(PO_CREATION)','<=',':PO Date To','','','Y','3','Y','ID020048');
xxeis.eis_rs_ins.rcn( 'HDS GSC iPro PO Aging Report',201,'PO_SUPPLIER_NAME','>=',':Supplier From','','','Y','4','Y','ID020048');
xxeis.eis_rs_ins.rcn( 'HDS GSC iPro PO Aging Report',201,'PO_SUPPLIER_NAME','<=',':Supplier To','','','Y','5','Y','ID020048');
xxeis.eis_rs_ins.rcn( 'HDS GSC iPro PO Aging Report',201,'REQ_NUMBER','>=',':PO Req From','','','Y','6','Y','ID020048');
xxeis.eis_rs_ins.rcn( 'HDS GSC iPro PO Aging Report',201,'REQ_NUMBER','<=',':PO Req To','','','Y','7','Y','ID020048');
xxeis.eis_rs_ins.rcn( 'HDS GSC iPro PO Aging Report',201,'PO_NUMBER','>=',':PO From','','','Y','9','Y','ID020048');
xxeis.eis_rs_ins.rcn( 'HDS GSC iPro PO Aging Report',201,'PO_NUMBER','<=',':PO To','','','Y','10','Y','ID020048');
xxeis.eis_rs_ins.rcn( 'HDS GSC iPro PO Aging Report',201,'GCC#50328#PROJECT_CODE','IN',':GCC#50328#PROJECT_CODE','','','Y','12','Y','ID020048');
--Inserting Report Sorts - HDS GSC iPro PO Aging Report
--Inserting Report Triggers - HDS GSC iPro PO Aging Report
--Inserting Report Templates - HDS GSC iPro PO Aging Report
--Inserting Report Portals - HDS GSC iPro PO Aging Report
--Inserting Report Dashboards - HDS GSC iPro PO Aging Report
--Inserting Report Security - HDS GSC iPro PO Aging Report
xxeis.eis_rs_ins.rsec( 'HDS GSC iPro PO Aging Report','178','','21584',201,'ID020048','','');
xxeis.eis_rs_ins.rsec( 'HDS GSC iPro PO Aging Report','178','','52090',201,'ID020048','','');
xxeis.eis_rs_ins.rsec( 'HDS GSC iPro PO Aging Report','178','','51731',201,'ID020048','','');
xxeis.eis_rs_ins.rsec( 'HDS GSC iPro PO Aging Report','178','','51729',201,'ID020048','','');
xxeis.eis_rs_ins.rsec( 'HDS GSC iPro PO Aging Report','101','','50723',201,'ID020048','','');
xxeis.eis_rs_ins.rsec( 'HDS GSC iPro PO Aging Report','101','','50720',201,'ID020048','','');
xxeis.eis_rs_ins.rsec( 'HDS GSC iPro PO Aging Report','','JS016768','',201,'ID020048','','');
--Inserting Report Pivots - HDS GSC iPro PO Aging Report
END;
/
/*set scan on define on*/
