/*************************************************************************
  $Header TMS_20180801-00068_UPDATE_INVOICE_DFF.sql $
  Module Name: TMS_20180801-00068_UPDATE_INVOICE_DFF.sql

  PURPOSE:   Update the invoice DFF

  REVISIONS:
  Ver        Date        Author               Description
  ---------  ----------  ---------------      -------------------------
  1.0        08/01/2018  Pattabhi Avula       TMS#20180801-00068
  **************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000;
DECLARE
BEGIN
 DBMS_OUTPUT.put_line ('TMS: Datafix script    , Before Update');
 -- Updating the headers table
 
  UPDATE apps.ra_customer_trx_all
     SET attribute15 = NULL
   WHERE batch_source_id IN (1012, 1013, 1014);
   
   COMMIT;

   DBMS_OUTPUT.put_line ('Records updated - ' || SQL%ROWCOUNT);
	  DBMS_OUTPUT.put_line ('TMS: 20180801-00068  , End Update');
EXCEPTION
 WHEN OTHERS THEN
 DBMS_OUTPUT.put_line ('TMS: 20180801-00068 , Errors : ' || SQLERRM);
END;
/