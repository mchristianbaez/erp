/*************************************************************************
  $Header TMS_20170719-00111_STOCK_LOCATOR_DELETE.sql $
  Module Name: 20170719-00111  Stock Locator delete

  PURPOSE: Data fix  

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        17-AUG-2017  Pattabhi Avula        TMS#20170719-00111 

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
l_return_status   VARCHAR2(10);
l_msg_count       NUMBER;
L_MSG_DATA        VARCHAR2(240);
l_locator_id      NUMBER;  --:= LOCATOR_ID;
l_org_id          NUMBER;  --:= org_id;
l_user_id         NUMBER:= fnd_global.user_id;
l_resp_id         NUMBER:= fnd_global.resp_id;  
l_resp_appl_id    NUMBER:= fnd_global.resp_appl_id; 
l_subinv_code     VARCHAR2(10); 

BEGIN
/* APPS_INITIALIZE required because indirectly use profile options*/ 
fnd_global.apps_initialize (l_user_id, l_resp_id, l_resp_appl_id);

 BEGIN
   SELECT inventory_location_id, 
          organization_id,
		  subinventory_code
     INTO l_locator_id
         ,l_org_id
         ,l_subinv_code		 
     FROM apps.mtl_item_locations
    WHERE segment1 = '1-T040305'
      AND organization_id = 234;
 EXCEPTION
  WHEN OTHERS THEN
   dbms_output.put_line ('Unexpected error while fetching the locator id and org_id '||SQLERRM);
    l_locator_id:=NULL;
    l_org_id:=NULL;
 END;	

dbms_output.put_line ('Before API Callilng');

inv_loc_wms_pub.delete_locator(
                               x_return_status => l_return_status
                               , x_msg_count => l_msg_count
                               , X_MSG_DATA => L_MSG_DATA
                               , p_inventory_location_id => l_locator_id
                               , P_CONCATENATED_SEGMENTS => NULL
                               , p_organization_id => l_org_id
                               , p_organization_code => l_subinv_code
                               , p_validation_req_flag => 'N'
                              );
							  
	dbms_output.put_line(' API l_return_status Return status is :'||l_return_status);
    IF l_return_status <> 'S' THEN
       dbms_output.put_line('Failure ');
       dbms_output.put_line('Reason ' || l_msg_data);
	   ROLLBACK;
    ELSE
       dbms_output.put_line('Success ');
	   COMMIT;
    END IF;
	
EXCEPTION
WHEN OTHERS THEN
dbms_output.put_line('Error executing script '||SQLERRM);
ROLLBACK;
END;
/