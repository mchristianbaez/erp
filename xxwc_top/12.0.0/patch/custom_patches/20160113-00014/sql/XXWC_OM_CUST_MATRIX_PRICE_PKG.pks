CREATE OR REPLACE PACKAGE APPS.XXWC_OM_CUST_MATRIX_PRICE_PKG
IS
   /**********************************************************************************************************************************************************
        $Header XXWC_OM_CUST_MATRIX_PRICE_PKG $
        Module Name: XXWC_OM_CUST_MATRIX_PRICE_PKG.pks

        PURPOSE:   This package is to populate Custom Matrix price eligibility info into custom table.

        REVISIONS:
        Ver        Date        Author                  Description
        ---------  ----------  ---------------         -------------------------
        1.0        04/17/2017  P.Vamshidhar            Initial Version
                                                       TMS# 20160526-00017 - Ability to inactive Matrix modifiers in Bulk
                                                       TMS# 20160113-00014 -  Display Matrix Customer Flag on SOE banner, and this flag will be used in OBIEE
    **********************************************************************************************************************************************************/

   PROCEDURE populate_data (x_errbuf             OUT VARCHAR2,
                            x_retcode            OUT VARCHAR2,
                            p_list_header_id     IN  NUMBER);

   PROCEDURE xxwc_qp_ntl_prl_maint_tool (X_ERRBUF       OUT VARCHAR2,
                                         X_RETCODE      OUT VARCHAR2,
                                         P_FILE_ID   IN     NUMBER);

   PROCEDURE xxwc_qualifiers_update (x_errbuf       OUT VARCHAR2,
                                     x_retcode      OUT VARCHAR2,
                                     p_file_id   IN     NUMBER);


   PROCEDURE write_log (p_message IN VARCHAR2);

   PROCEDURE write_output (p_message IN VARCHAR2);

   END XXWC_OM_CUST_MATRIX_PRICE_PKG;
/
