 /****************************************************************************************************
  Table: XXWC_CUST_MTRX_PRICE_ELIG_TBL.sql     
  Description: Displaying banner info on Order screen.
  HISTORY
  =====================================================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- ----------------------------------------------------------
  1.0     26-Apr-2017        P.Vamshidhar    Initial version TMS#20160113-00014 
                                             Display Matrix Customer Flag on SOE banner, and this flag will be used in OBIEE
  *****************************************************************************************************/

CREATE TABLE XXWC.XXWC_CUST_MTRX_PRICE_ELIG_TBL
(
  CUST_ACCOUNT_ID   NUMBER PRIMARY KEY,
  QUALIFIER_TYPE    VARCHAR2(100 BYTE),
  CREATION_DATE     DATE,
  CREATED_BY        NUMBER,
  LAST_UPDATE_DATE  DATE,
  LAST_UPDATED_BY   NUMBER
)
/

