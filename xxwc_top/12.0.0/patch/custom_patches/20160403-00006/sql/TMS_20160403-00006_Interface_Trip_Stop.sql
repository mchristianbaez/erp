
/*************************************************************************
  $Header TMS_20160403-00006_Interface_Trip_Stop.sql $
  Module Name: TMS_20160403-00006  Update the Unprocesed Shipping Transactions
                for sales order # 20126828

  PURPOSE: Data fix to update the Delivery Details

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        03-APR-2016  ANITHA MANI         TMS#20160403-00006 

**************************************************************************/ 


SET SERVEROUTPUT ON SIZE 100000;
SET VERIFY OFF;

DECLARE
BEGIN

DBMS_OUTPUT.put_line ('TMS: 20160403-00006    , Before Update');

UPDATE apps.wsh_delivery_details
SET released_status = 'D',
src_requested_quantity = 0,
requested_quantity = 0,
shipped_quantity = 0,
cycle_count_quantity = 0,
cancelled_quantity = 0,
subinventory = null,
locator_id = null,
lot_number = null,
revision = null,
inv_interfaced_flag = 'X',
oe_interfaced_flag = 'X'
WHERE delivery_detail_id =15168725;--Pass the delivery detail ID


DBMS_OUTPUT.put_line (
         'TMS: 20160403-00006 - number of records updated: '
      || SQL%ROWCOUNT);


update apps.wsh_delivery_assignments
set delivery_id = null,
parent_delivery_detail_id = null
where delivery_detail_id = 15168725;--Pass the delivery detail ID

COMMIT;

DBMS_OUTPUT.put_line (
         'TMS: 20160403-00006 - number of records updated: '
      || SQL%ROWCOUNT);

   DBMS_OUTPUT.put_line ('TMS: 20160403-00006   , End Update');
   
END; 
/ 