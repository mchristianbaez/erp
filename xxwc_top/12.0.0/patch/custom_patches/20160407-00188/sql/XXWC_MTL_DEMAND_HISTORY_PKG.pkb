create or replace PACKAGE BODY      XXWC_MTL_DEMAND_HISTORY_PKG
/*************************************************************************
  $Header XXWC_MTL_DEMAND_HISTORY_PKG.pkb $
  Module Name: XXWC_MTL_DEMAND_HISTORY_PKG

  PURPOSE: To update mtl_demand_histories for specific transactions defined

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    ----------------
  1.0        30-Apr-2015  Manjula Chellappan    Initial Version TMS # 20150302-00057
  1.1        07-Apr-16    Gopi Damuluri         TMS# 20160407-00188 - Remove Parallelism
**************************************************************************/
IS
   g_start        NUMBER;
   g_errbuf       CLOB;
   g_retcode      NUMBER := 0;
   g_dflt_email   fnd_user.email_address%TYPE
                     := 'HDSOracleDevelopers@hdsupply.com';
   g_line_open    VARCHAR2 (100)
      := '/*************************************************************************';
   g_line_close   VARCHAR2 (100)
      := '**************************************************************************/';
   g_sec          VARCHAR2 (200);


   PROCEDURE write_log (p_log_msg VARCHAR2)
   /*************************************************************************
     $Header XXWC_MTL_DEMAND_HISTORY_PKG.write_log $
     PURPOSE:     Procedure to write Log messages

     REVISIONS:
     Ver    Date         Author                Description
     ------ -----------  ------------------    ----------------
     1.0    30-Apr-15    Manjula Chellappan    Initial Version
     **************************************************************************/
   IS
      l_log_msg   VARCHAR2 (2000);
   BEGIN
      IF p_log_msg = g_line_open OR p_log_msg = g_line_close
      THEN
         l_log_msg := p_log_msg;
      ELSE
         l_log_msg :=
            p_log_msg || ' : ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS');
         xxwc_common_tunning_helpers.elapsed_time ('DEMAND_HISTORY',
                                                   p_log_msg,
                                                   g_start);
      END IF;

      fnd_file.put_line (fnd_file.LOG, l_log_msg);
   END write_log;


   PROCEDURE drop_temp_table (p_owner VARCHAR2, p_table_name VARCHAR2)
   /*************************************************************************
     $Header XXWC_MTL_DEMAND_HISTORY_PKG.drop_temp_table $
     PURPOSE:     Local Procedure to drop the temp tables

     REVISIONS:
     Ver    Date         Author                Description
     ------ -----------  ------------------    ----------------
     1.0    30-Apr-15    Manjula Chellappan    Initial Version
     **************************************************************************/
   IS
      l_table_count       NUMBER := 0;
      l_drop_stmt         VARCHAR2 (2000);
      l_table_full_name   VARCHAR2 (60) := p_owner || '.' || p_table_name;
      l_success_msg       VARCHAR2 (200)
                             := l_table_full_name || ' Table Dropped ';
      l_error_msg         VARCHAR (200)
                             := l_table_full_name || ' Table Drop failed ';
   BEGIN
      g_sec := 'DROP TABLE ' || p_owner || '.' || p_table_name;


      SELECT COUNT (*)
        INTO l_table_count
        FROM all_tables
       WHERE     table_name = UPPER (p_table_name)
             AND owner = UPPER (p_owner)
             AND UPPER (p_owner) LIKE 'XX%';

      IF l_table_count > 0
      THEN
         l_drop_stmt := 'DROP TABLE ' || p_owner || '.' || p_table_name;

         EXECUTE IMMEDIATE l_drop_stmt;

         write_log (l_success_msg);
      ELSE
         write_log (l_table_full_name || ' Do not exist ');
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         g_errbuf := l_error_msg || CHR (10) || SQLERRM;
         write_log (g_errbuf);
   END;



   PROCEDURE Load_Ou (p_retcode OUT VARCHAR2)
   /*************************************************************************
     $Header XXWC_MTL_DEMAND_HISTORY_PKG.Load_Ou $
     PURPOSE:     1. Procedure to Load Operating Units list for ISR Datamart Refresh

     REVISIONS:
     Ver    Date         Author                Description
     ------ -----------  ------------------    ----------------
     1.0    30-Apr-2015  Manjula Chellappan    Initial Version
     **************************************************************************/
   IS
      l_retcode             NUMBER := 0;
      l_owner               VARCHAR2 (10) := 'XXWC';
      l_table_name          VARCHAR2 (40) := 'XXWC_MTL_DEMAND_OU##';
      l_table_full_name     VARCHAR2 (60) := l_owner || '.' || l_table_name;
      l_index_name          VARCHAR2 (60) := l_table_full_name || '_N1';
      l_table_success_msg   VARCHAR2 (200)
         := l_table_full_name || ' Table Load completed ';
      l_table_error_msg     VARCHAR2 (200)
                               := l_table_full_name || ' Table Load failed ';
      l_success_msg         VARCHAR2 (200) := l_table_success_msg;
      l_error_msg           VARCHAR2 (200) := l_table_error_msg;
      l_index_success_msg   VARCHAR2 (200)
         := l_index_name || ' Index Creation completed ';
      l_index_error_msg     VARCHAR2 (200)
                               := l_index_name || ' Index Creation failed ';
      l_create_table_stmt   VARCHAR2 (200)
         := 'CREATE TABLE ' || l_table_full_name || ' AS ';
      l_select_stmt         VARCHAR2 (4000)
         := '  SELECT organization_id org_id, name operating_unit
                 FROM hr_operating_units
                WHERE short_code IN (SELECT lookup_code
                                       FROM fnd_lookup_values
                                      WHERE lookup_type = ''XXWC_REPORTING_OU_LIST'')';

      l_Load_stmt           VARCHAR2 (4000)
                               := l_create_table_stmt || l_select_stmt;

      l_index_stmt          VARCHAR2 (200)
         :=    'CREATE INDEX '
            || l_index_name
            || ' ON '
            || l_table_full_name
            || ' ( org_id)';
   BEGIN
      g_sec := 'Process 1 : Load Operating Units ';
      write_log (g_line_open);
      write_log (g_sec);

      drop_temp_table (l_owner, l_table_name);

      EXECUTE IMMEDIATE l_Load_stmt;

      write_log (l_success_msg);

      l_success_msg := l_index_success_msg;
      l_error_msg := l_index_error_msg;

      EXECUTE IMMEDIATE l_index_stmt;

      write_log (l_success_msg);
      write_log (g_line_close);
   EXCEPTION
      WHEN OTHERS
      THEN
         l_retcode := 2;
         p_retcode := l_retcode;
         g_errbuf := l_error_msg || CHR (10) || SQLERRM;
         write_log (g_errbuf);
         write_log (g_line_close);
   END Load_Ou;


   PROCEDURE Load_Inv_Orgs (p_retcode              OUT VARCHAR2,
                            p_organization_id   IN     NUMBER,
                            p_last_run_date     IN     VARCHAR2)
   /*************************************************************************
     $Header XXWC_MTL_DEMAND_HISTORY_PKG.Load_Inv_Orgs $
     PURPOSE:     2. Procedure to Load inventory Organizations for ISR Datamart Refresh

     REVISIONS:
     Ver    Date         Author                Description
     ------ -----------  ------------------    ----------------
     1.0    30-Apr-15    Manjula Chellappan    Initial Version
     **************************************************************************/
   IS
      l_retcode           NUMBER := 0;
      l_organization_id   NUMBER := p_organization_id;
      l_last_run_date     DATE
         := TO_DATE (p_last_run_date, 'YYYY/MM/DD HH24:MI:SS');
      l_success_msg       VARCHAR2 (200);
      l_error_msg         VARCHAR2 (200);
   BEGIN
      g_sec := 'Process 2 : Load Inventory Organizations ';
      write_log (g_line_open);
      write_log (g_sec);


      l_success_msg := ' XXWC_MTL_DEMAND_HISTORIES_ORG table load completed ';
      l_error_msg := ' XXWC_MTL_DEMAND_HISTORIES_ORG table load Failed ';

      EXECUTE IMMEDIATE
         'INSERT INTO XXWC.XXWC_MTL_DEMAND_HISTORIES_ORG
         (SELECT m.organization_id,
                 m.organization_code,
                 m.calendar_code,
                 m.calendar_exception_set_id,
                 NVL (:1, d.prior_date-1) last_run_date,
                 TO_NUMBER (
                    TO_CHAR (NVL (:2, d.prior_date-1), ''J''))
                    last_run_j_date
            FROM org_organization_definitions o,
                 mtl_parameters m,
                 xxwc.xxwc_mtl_demand_ou## x,
                 bom_period_start_dates d
           WHERE     o.operating_unit = x.org_id
                 AND o.organization_id = m.organization_id
                 AND m.calendar_code = d.calendar_code
                 AND m.calendar_exception_set_id = d.exception_set_id
                 AND TRUNC (SYSDATE) BETWEEN d.period_start_date
                                         AND d.next_date-1
                 AND m.organization_id =
                        NVL (:3, m.organization_id)
                 AND NOT EXISTS
                        (SELECT ''X''
                           FROM XXWC.XXWC_MTL_DEMAND_HISTORIES_ORG
                          WHERE organization_id = m.organization_id)) '
         USING l_last_run_date, l_last_run_date, l_organization_id;

      write_log (l_success_msg);

      write_log (g_line_close);
   EXCEPTION
      WHEN OTHERS
      THEN
         l_retcode := 2;
         p_retcode := l_retcode;
         g_errbuf := l_error_msg || CHR (10) || SQLERRM;
         write_log (g_errbuf);
         write_log (g_line_close);
   END Load_Inv_Orgs;



   PROCEDURE Load_Periods (p_retcode OUT VARCHAR2)
   /*************************************************************************
   $Header XXWC_MTL_DEMAND_HISTORY_PKG.Load_Periods $
   PURPOSE:     3. Procedure to Load periods list for Demand History Correction

   REVISIONS:
   Ver    Date         Author                Description
   ------ -----------  ------------------    ----------------
   1.0    30-Apr-15    Manjula Chellappan    Initial Version
   **************************************************************************/

   IS
      l_retcode             NUMBER := 0;
      l_owner               VARCHAR2 (10) := 'XXWC';
      l_table_name          VARCHAR2 (40) := 'XXWC_MTL_DEMAND_PERIODS##';
      l_table_full_name     VARCHAR2 (60) := l_owner || '.' || l_table_name;
      l_index_name          VARCHAR2 (60) := l_table_full_name || '_N1';
      l_table_success_msg   VARCHAR2 (200)
         := l_table_full_name || ' Table Load completed ';
      l_table_error_msg     VARCHAR2 (200)
                               := l_table_full_name || ' Table Load failed ';
      l_success_msg         VARCHAR2 (200) := l_table_success_msg;
      l_error_msg           VARCHAR2 (200) := l_table_error_msg;
      l_index_success_msg   VARCHAR2 (200)
         := l_index_name || ' Index Creation completed ';
      l_index_error_msg     VARCHAR2 (200)
                               := l_index_name || ' Index Creation failed ';
      l_create_table_stmt   VARCHAR2 (200)
         := 'CREATE TABLE ' || l_table_full_name || ' AS ';
      l_select_stmt         VARCHAR2 (4000)
         := '     SELECT organization_id,
                        period_start_date,
                        next_date - 1 period_end_date,
                        TO_NUMBER (TO_CHAR (period_start_date, ''J'')) start_j_date,
                        TO_NUMBER (TO_CHAR (next_date - 1, ''J'')) end_j_date
                   FROM bom_period_start_dates d, xxwc.xxwc_mtl_demand_histories_org p
                  WHERE     p.calendar_code = d.calendar_code
                        AND d.exception_set_id = p.calendar_exception_set_id
                        AND d.period_start_date <= SYSDATE
               ORDER BY 1 ';

      l_Load_stmt           VARCHAR2 (4000)
                               := l_create_table_stmt || l_select_stmt;

      l_index_stmt          VARCHAR2 (200)
         :=    'CREATE INDEX '
            || l_index_name
            || ' ON '
            || l_table_full_name
            || ' ( organization_id,period_start_date,period_end_date)';
   BEGIN
      g_sec := 'Process 3 : Load Periods ';
      write_log (g_line_open);
      write_log (g_sec);

      drop_temp_table (l_owner, l_table_name);

      EXECUTE IMMEDIATE l_Load_stmt;

      write_log (l_success_msg);

      l_success_msg := l_index_success_msg;
      l_error_msg := l_index_error_msg;

      EXECUTE IMMEDIATE l_index_stmt;

      write_log (l_success_msg);
      write_log (g_line_close);
   EXCEPTION
      WHEN OTHERS
      THEN
         l_retcode := 2;
         p_retcode := l_retcode;
         g_errbuf := l_error_msg || CHR (10) || SQLERRM;
         write_log (g_errbuf);
         write_log (g_line_close);
   END Load_Periods;


   PROCEDURE Load_Transactions (p_retcode              OUT VARCHAR2,
                                p_organization_id   IN     NUMBER)
   /*************************************************************************
     $Header XXWC_MTL_DEMAND_HISTORY_PKG.Load_Transactions $
     PURPOSE:     4. Procedure to Get Transactions list for Demand History Correction

     REVISIONS:
     Ver    Date         Author                Description
     ------ -----------  ------------------    ----------------
     1.0    30-Apr-15    Manjula Chellappan    Initial Version
     **************************************************************************/
   IS
      l_retcode              NUMBER := 0;
      l_organization_id      NUMBER := p_organization_id;
      l_owner                VARCHAR2 (10) := 'XXWC';
      l_table_name           VARCHAR2 (40) := 'XXWC_MTL_DEMAND_TXN_ALL##';
      l_table_full_name      VARCHAR2 (60) := l_owner || '.' || l_table_name;
      l_index_name           VARCHAR2 (60) := l_table_full_name || '_N1';
      l_table_success_msg    VARCHAR2 (200)
         := l_table_full_name || ' Table Load completed ';
      l_table_error_msg      VARCHAR2 (200)
                                := l_table_full_name || ' Table Load failed ';
      l_success_msg          VARCHAR2 (200) := l_table_success_msg;
      l_error_msg            VARCHAR2 (200) := l_table_error_msg;
      l_index_success_msg    VARCHAR2 (200)
         := l_index_name || ' Index Creation completed ';
      l_index_error_msg      VARCHAR2 (200)
                                := l_index_name || ' Index Creation failed ';
      l_create_table_stmt    VARCHAR2 (200)
         := 'CREATE TABLE ' || l_table_full_name || ' AS ';


      l_select_stmt          VARCHAR2 (4000)
         := ' SELECT txn.transaction_id,
                      TO_NUMBER (TO_CHAR (txn.transaction_date, ''J'')) transaction_j_date,
                      txn.transaction_date,
                      txn.inventory_item_id,
                      txn.transaction_source_type_id,
                      txn.transaction_action_id,
                      txn.transaction_type_id,
                      txn.primary_quantity,
                      txn.source_line_id,
                      txn.trx_source_line_id,
                      txn.organization_id
                 FROM mtl_material_transactions txn, xxwc.XXWC_MTL_DEMAND_HISTORIES_ORG org 
                WHERE 1=1 
                      AND txn.organization_id = org.organization_id
                      AND transaction_date >= last_run_date+1
                      AND txn.transaction_type_id IN ( (SELECT tag
                                                          FROM fnd_lookup_values
                                                         WHERE     lookup_type LIKE
                                                                      ''XXWC_MTL_DEMAND_TXN_TYPE''
                                                               AND enabled_flag = ''Y''
                                                               AND TRUNC (SYSDATE) BETWEEN TRUNC (
                                                                                              NVL (
                                                                                                 start_date_active,
                                                                                                 SYSDATE))
                                                                                       AND TRUNC (
                                                                                              NVL (
                                                                                                 end_date_active,
                                                                                                 SYSDATE)))) ';

      l_Load_stmt            VARCHAR2 (4000)
                                := l_create_table_stmt || l_select_stmt;

      l_index_stmt           VARCHAR2 (200)
         :=    'CREATE INDEX '
            || l_index_name
            || ' ON '
            || l_table_full_name
            || ' ( transaction_type_id)';

      l_table_name1          VARCHAR2 (40) := 'XXWC_MTL_DEMAND_TXN##';
      l_table_full_name1     VARCHAR2 (60) := l_owner || '.' || l_table_name1;
      l_index_name1          VARCHAR2 (60) := l_table_full_name1 || '_N1';
      l_table_success_msg1   VARCHAR2 (200)
         := l_table_full_name1 || ' Table Load completed ';
      l_table_error_msg1     VARCHAR2 (200)
         := l_table_full_name1 || ' Table Load failed ';
      l_index_success_msg1   VARCHAR2 (200)
         := l_index_name1 || ' Index Creation completed ';
      l_index_error_msg1     VARCHAR2 (200)
                                := l_index_name1 || ' Index Creation failed ';
      l_create_table_stmt1   VARCHAR2 (200)
         := 'CREATE TABLE ' || l_table_full_name1 || ' AS ';

      l_select_stmt1         CLOB
         := ' SELECT txn.transaction_id,
                      txn_type.transaction_type_code,
                      txn_type.transaction_type,
                      txn.transaction_type_id,    
                      txn.transaction_source_type_id,
                      txn.source_line_id,     
                      txn.trx_source_line_id,                      
                      txn.transaction_action_id,                           
                      txn_type.include_exclude_flag,
                      txn_type.bucket_to_update,
                      TO_NUMBER (TO_CHAR (txn.transaction_date, ''J'')) transaction_j_date,
                      txn.transaction_date,
                      txn.inventory_item_id,
                      txn.organization_id,                        
                      txn.primary_quantity              
         FROM XXWC.XXWC_MTL_DEMAND_TXN_ALL## txn,
              (SELECT tag transaction_type_id,
                      lookup_code transaction_type_code,
                      meaning transaction_type,
                      description,
                      enabled_flag,
                      start_date_active,
                      end_date_active,
                      attribute_category,
                      attribute1 include_exclude_flag,
                      attribute2 bucket_to_update,
                      creation_date,
                      last_update_date
                 FROM fnd_lookup_values
                WHERE     lookup_type LIKE ''XXWC_MTL_DEMAND_TXN_TYPE''
                      AND enabled_flag = ''Y''
                      AND TRUNC (SYSDATE) BETWEEN TRUNC (
                                                     NVL (start_date_active,
                                                          SYSDATE))
                                              AND TRUNC (
                                                     NVL (end_date_active,
                                                          SYSDATE))) txn_type
        WHERE     1 = 1
              AND txn_type.transaction_type_id = txn.transaction_type_id
              AND (   txn_type.transaction_type_code IN (''RMA RECEIPT'',
                                                         ''WIP RETURN'')
                   OR EXISTS
                         (SELECT ''X''
                            FROM oe_drop_ship_sources
                           WHERE line_id = txn.trx_source_line_id))
              AND EXISTS
                     (SELECT ''X''
                        FROM mtl_item_categories i, mtl_category_sets s
                       WHERE     inventory_item_id = txn.inventory_item_id
                             AND organization_id = txn.organization_id                             
                             AND i.category_set_id = s.category_set_id
                             AND s.category_set_name = ''Inventory Category'')          
             AND 1=2             ';
      l_insert_stmt1         CLOB;

      l_index_stmt1          VARCHAR2 (200)
         :=    'CREATE INDEX '
            || l_index_name1
            || ' ON '
            || l_table_full_name1
            || ' ( transaction_id)';
   BEGIN
      g_sec := 'Process 4 : Load Transactions ';
      write_log (g_line_open);
      write_log (g_sec);

      drop_temp_table (l_owner, l_table_name);

      EXECUTE IMMEDIATE l_Load_stmt;

      write_log (l_success_msg);

      l_success_msg := l_index_success_msg;
      l_error_msg := l_index_error_msg;

      EXECUTE IMMEDIATE l_index_stmt;

      write_log (l_success_msg);

      l_insert_stmt1 :=
         '  INSERT /*+ append */ INTO XXWC.XXWC_MTL_DEMAND_TXN## 
         ( SELECT txn.transaction_id,
                  txn_type.transaction_type_code,
                  txn_type.transaction_type,
                  txn.transaction_type_id,    
                  txn.transaction_source_type_id,
                  txn.source_line_id,  
                  txn.trx_source_line_id,
                  txn.transaction_action_id,                           
                  txn_type.include_exclude_flag,
                  txn_type.bucket_to_update,
                  TO_NUMBER (TO_CHAR (txn.transaction_date, ''J'')) transaction_j_date,
                  txn.transaction_date,
                  txn.inventory_item_id,
                  txn.organization_id,                        
                  txn.primary_quantity
         FROM XXWC.XXWC_MTL_DEMAND_TXN_ALL## txn,
              (SELECT tag transaction_type_id,
                      lookup_code transaction_type_code,
                      meaning transaction_type,
                      description,
                      enabled_flag,
                      start_date_active,
                      end_date_active,
                      attribute_category,
                      attribute1 include_exclude_flag,
                      attribute2 bucket_to_update,
                      creation_date,
                      last_update_date
                 FROM fnd_lookup_values
                WHERE     lookup_type LIKE ''XXWC_MTL_DEMAND_TXN_TYPE''
                      AND enabled_flag = ''Y''
                      AND TRUNC (SYSDATE) BETWEEN TRUNC (
                                                     NVL (start_date_active,
                                                          SYSDATE))
                                              AND TRUNC (
                                                     NVL (end_date_active,
                                                          SYSDATE))) txn_type
        WHERE     1 = 1
              AND txn_type.transaction_type_id = txn.transaction_type_id
              AND (   txn_type.transaction_type_code IN (''RMA RECEIPT'',
                                                         ''WIP RETURN'')
                   OR EXISTS
                         (SELECT ''X''
                            FROM oe_drop_ship_sources
                           WHERE line_id = txn.trx_source_line_id
                           AND txn.primary_quantity < 0))
              AND EXISTS
                     (SELECT ''X''
                        FROM mtl_item_categories i, mtl_category_sets s
                       WHERE     inventory_item_id = txn.inventory_item_id
                             AND organization_id = txn.organization_id                             
                             AND i.category_set_id = s.category_set_id
                             AND s.category_set_name = ''Inventory Category'') 
             AND txn.organization_id = NVL(:l_organization_id, txn.organization_id)) ';


      l_success_msg := l_table_success_msg1;
      l_error_msg := l_table_error_msg1;

      drop_temp_table (l_owner, l_table_name1);

      EXECUTE IMMEDIATE l_create_table_stmt1 || l_select_stmt1;

      EXECUTE IMMEDIATE l_insert_stmt1 USING l_organization_id;


      write_log (l_success_msg);

      l_success_msg := l_index_success_msg1;
      l_error_msg := l_index_error_msg1;

      EXECUTE IMMEDIATE l_index_stmt1;

      write_log (l_success_msg);

      write_log (g_line_close);
   EXCEPTION
      WHEN OTHERS
      THEN
         l_retcode := 2;
         p_retcode := l_retcode;
         g_errbuf := l_error_msg || CHR (10) || SQLERRM;
         write_log (g_errbuf);
         write_log (g_line_close);
   END Load_Transactions;



   PROCEDURE Gen_Demand_Update (p_retcode OUT VARCHAR2)
   /*************************************************************************
     $Header XXWC_MTL_DEMAND_HISTORY_PKG.Gen_Demand_Update $
     PURPOSE:     5. Procedure to Get Transactions list for Demand History Correction

     REVISIONS:
     Ver    Date         Author                Description
     ------ -----------  ------------------    ----------------
     1.0    30-Apr-15    Manjula Chellappan    Initial Version
     **************************************************************************/
   IS
      l_retcode             NUMBER := 0;
      l_owner               VARCHAR2 (10) := 'XXWC';
      l_table_name          VARCHAR2 (40) := 'XXWC_MTL_DEMAND_HISTORIES##';
      l_table_full_name     VARCHAR2 (60) := l_owner || '.' || l_table_name;
      l_index_name          VARCHAR2 (60) := l_table_full_name || '_N1';
      l_table_success_msg   VARCHAR2 (200)
         := l_table_full_name || ' Table Load completed ';
      l_table_error_msg     VARCHAR2 (200)
                               := l_table_full_name || ' Table Load failed ';
      l_success_msg         VARCHAR2 (200) := l_table_success_msg;
      l_error_msg           VARCHAR2 (200) := l_table_error_msg;
      l_index_success_msg   VARCHAR2 (200)
         := l_index_name || ' Index Creation completed ';
      l_index_error_msg     VARCHAR2 (200)
                               := l_index_name || ' Index Creation failed ';
      l_create_table_stmt   VARCHAR2 (200)
         := 'CREATE TABLE ' || l_table_full_name || ' AS ';
      l_select_stmt         VARCHAR2 (4000)
         := '   SELECT txn.*,
                       period.period_start_date,
                       period.period_end_date,
                       dem.ROWID row_id, 
                       dem.std_wip_usage,
                       dem.sales_order_demand,
                       dem.miscellaneous_issue,
                       dem.interorg_issue,
                          ''UPDATE MTL_DEMAND_HISTORIES SET ''
                       || txn.bucket_to_update
                       || '' = ''
                       || txn.bucket_to_update
                       || DECODE (txn.include_exclude_flag, ''INCLUDE'', '' - '', '' + '')
                       || ABS(txn.primary_quantity)
                       || '' WHERE ROWID = ''''''
                       || dem.ROWID||''''''''
                       demand_restore_stmt, 
                          ''UPDATE MTL_DEMAND_HISTORIES SET ''
                       || txn.bucket_to_update
                       || '' = ''
                       || txn.bucket_to_update
                       || DECODE (txn.include_exclude_flag, ''INCLUDE'', '' + '', '' - '')
                       || ABS(txn.primary_quantity)
                       || '' WHERE ROWID = ''''''
                       || dem.ROWID||''''''''
                       demand_update_stmt,
                       sysdate update_date                       
                  FROM xxwc.XXWC_MTL_DEMAND_TXN## txn,
                       xxwc.XXWC_MTL_DEMAND_PERIODS## period,
                       mtl_demand_histories dem
                 WHERE     txn.transaction_j_date BETWEEN period.start_j_date
                                                      AND period.end_j_date
                       AND txn.organization_id = period.organization_id
                       AND dem.period_start_date = period.period_start_date
                       AND txn.inventory_item_id = dem.inventory_item_id
                       AND txn.organization_id = dem.organization_id 
                       AND NOT EXISTS 
                           ( SELECT ''X'' FROM XXWC.XXWC_MTL_DEMAND_HISTORIES_ARCH
                              WHERE transaction_id = txn.transaction_id)  ';


      l_Load_stmt           VARCHAR2 (4000)
                               := l_create_table_stmt || l_select_stmt;

      l_index_stmt          VARCHAR2 (200)
         :=    'CREATE INDEX '
            || l_index_name
            || ' ON '
            || l_table_full_name
            || ' ( transaction_id)';
   BEGIN
      g_sec :=
         'Process 5 : Generate Update Statements for Demand History Update ';
      write_log (g_line_open);
      write_log (g_sec);

      drop_temp_table (l_owner, l_table_name);

      EXECUTE IMMEDIATE l_Load_stmt;

      write_log (l_success_msg);

      l_success_msg := l_index_success_msg;
      l_error_msg := l_index_error_msg;

      EXECUTE IMMEDIATE l_index_stmt;

      write_log (l_success_msg);

      write_log (g_line_close);
   EXCEPTION
      WHEN OTHERS
      THEN
         l_retcode := 2;
         p_retcode := l_retcode;
         g_errbuf := l_error_msg || CHR (10) || SQLERRM;
         write_log (g_errbuf);
         write_log (g_line_close);
   END Gen_Demand_Update;


   PROCEDURE Demand_Update (p_retcode              OUT VARCHAR2,
                            p_organization_id   IN     NUMBER)
   /*************************************************************************
     $Header XXWC_MTL_DEMAND_HISTORY_PKG.Demand_Update $
     PURPOSE:     6. Update Demand History and Load into Archive Table

     REVISIONS:
     Ver    Date         Author                Description
     ------ -----------  ------------------    ----------------
     1.0    30-Apr-15    Manjula Chellappan    Initial Version
     **************************************************************************/
   IS
      l_success_msg       VARCHAR2 (200);
      l_error_msg         VARCHAR2 (200);
      l_organization_id   NUMBER := p_organization_id;
   BEGIN
      g_sec := 'Process 6 : Update Demand History and Load into Archive Table XXWC_MTL_DEMAND_HISTORIES_ARCH';
      write_log (g_line_open);
      write_log (g_sec);

      EXECUTE IMMEDIATE
         ' BEGIN
           FOR r_org_rec
              IN (SELECT DISTINCT organization_id
                    FROM xxwc.xxwc_mtl_demand_histories_org 
                   WHERE organization_id = NVL(:l_organization_id, organization_id)
                   ORDER BY 1)
           LOOP              
             
              FOR r_txn_rec IN (SELECT demand_update_stmt
                                  FROM xxwc.xxwc_mtl_demand_histories##
                                 WHERE organization_id = r_org_rec.organization_id)
              LOOP                            
                 
                 EXECUTE IMMEDIATE r_txn_rec.demand_update_stmt;                                  
                 
              END LOOP;
             
                 INSERT /*+ append */ INTO xxwc.xxwc_mtl_demand_histories_arch
                    (  TRANSACTION_ID,
                       TRANSACTION_TYPE_CODE,
                       TRANSACTION_TYPE,
                       TRANSACTION_TYPE_ID,
                       TRANSACTION_SOURCE_TYPE_ID,
                       SOURCE_LINE_ID,
                       TRX_SOURCE_LINE_ID,
                       TRANSACTION_ACTION_ID,
                       INCLUDE_EXCLUDE_FLAG,
                       BUCKET_TO_UPDATE,
                       TRANSACTION_J_DATE,
                       TRANSACTION_DATE,
                       INVENTORY_ITEM_ID,
                       ORGANIZATION_ID,
                       PRIMARY_QUANTITY,
                       PERIOD_START_DATE,
                       PERIOD_END_DATE,
                       ROW_ID,
                       STD_WIP_USAGE,
                       SALES_ORDER_DEMAND,
                       MISCELLANEOUS_ISSUE,
                       INTERORG_ISSUE,
                       DEMAND_RESTORE_STMT,
                       DEMAND_UPDATE_STMT,
                       UPDATE_DATE  )
                  (SELECT    TRANSACTION_ID,
                   TRANSACTION_TYPE_CODE,
                   TRANSACTION_TYPE,
                   TRANSACTION_TYPE_ID,
                   TRANSACTION_SOURCE_TYPE_ID,
                   SOURCE_LINE_ID,
                   TRX_SOURCE_LINE_ID,
                   TRANSACTION_ACTION_ID,
                   INCLUDE_EXCLUDE_FLAG,
                   BUCKET_TO_UPDATE,
                   TRANSACTION_J_DATE,
                   TRANSACTION_DATE,
                   INVENTORY_ITEM_ID,
                   ORGANIZATION_ID,
                   PRIMARY_QUANTITY,
                   PERIOD_START_DATE,
                   PERIOD_END_DATE,
                   ROW_ID,
                   STD_WIP_USAGE,
                   SALES_ORDER_DEMAND,
                   MISCELLANEOUS_ISSUE,
                   INTERORG_ISSUE,
                   DEMAND_RESTORE_STMT,
                   DEMAND_UPDATE_STMT,
                   UPDATE_DATE
                      FROM xxwc.xxwc_mtl_demand_histories##
                     WHERE organization_id = r_org_rec.organization_id) ;
                     
                UPDATE xxwc.xxwc_mtl_demand_histories_org
                   SET last_run_date =
                          (SELECT prior_date - 1
                             FROM bom_period_start_dates period,
                                  xxwc.xxwc_mtl_demand_histories_org org
                            WHERE     SYSDATE BETWEEN period_start_date AND next_date
                                  AND period.calendar_code = org.calendar_code
                                  AND period.exception_set_id = org.calendar_exception_set_id
                                  AND org.organization_id = r_org_rec.organization_id),
                       last_run_j_date =
                          TO_NUMBER (
                             TO_CHAR (
                                (SELECT prior_date - 1
                                   FROM bom_period_start_dates period,
                                        xxwc.xxwc_mtl_demand_histories_org org
                                  WHERE     SYSDATE BETWEEN period_start_date AND next_date
                                        AND period.calendar_code = org.calendar_code
                                        AND period.exception_set_id =
                                               org.calendar_exception_set_id
                                        AND org.organization_id = r_org_rec.organization_id),
                                ''J''))
                 WHERE organization_id = r_org_rec.organization_id   ;              
 
			  
			     UPDATE mtl_demand_histories mdh
					SET closed_flag = ''Y''
					WHERE EXISTS
					  (SELECT ''x''
					  FROM org_acct_periods
					  WHERE period_start_date = mdh.period_start_date
					  AND organization_id     = mdh.organization_id
					  AND open_flag =''N''
					  )
					AND NVL(closed_flag,''N'') = ''N''
					AND organization_id = r_org_rec.organization_id ;

              COMMIT;
			  
           END LOOP  ;
     END ; '
         USING l_organization_id;

      write_log ('Demand History Correction Completed');
      write_log (g_line_close);
   EXCEPTION
      WHEN OTHERS
      THEN
         p_retcode := 2;
         g_errbuf := 'Demand History Update Failed ' || CHR (10) || SQLERRM;
         write_log (g_errbuf);
         write_log (g_line_close);
   END Demand_Update;


   PROCEDURE Demand_Update_Main (errbuf                 OUT VARCHAR2,
                                 retcode                OUT NUMBER,
                                 p_organization_id   IN     NUMBER,
                                 p_last_run_date     IN     VARCHAR2)
   /*************************************************************************
     $Header XXWC_MTL_DEMAND_HISTORY_PKG.Demand_Update_Main $
     PURPOSE:     7. Main procedure to invoke all the subprocedures in sequence

     REVISIONS:
     Ver    Date         Author                Description
     ------ -----------  ------------------    ----------------
     1.0    30-Apr-15    Manjula Chellappan    Initial Version
     1.1    07-Apr-16    Gopi Damuluri         TMS# 20160407-00188 - Remove Parallelism
     **************************************************************************/
   IS
      l_retcode           NUMBER := 0;
      l_organization_id   NUMBER := p_organization_id;
      l_last_run_date     VARCHAR2 (30) := p_last_run_date;
   BEGIN
      g_sec := 'Process Main : Update Demand main ';
      write_log (g_line_open);
      write_log (g_sec);
      write_log (g_line_close);
      
      apps.xxwc_mv_routines_add_pkg.enable_parallelism; -- Version# 1.1

      Load_Ou (l_retcode);

      IF l_retcode = 2
      THEN
         GOTO laststep;
      END IF;

      Load_Inv_Orgs (l_retcode, l_organization_id, l_last_run_date);

      IF l_retcode = 2
      THEN
         GOTO laststep;
      END IF;

      Load_Periods (l_retcode);

      IF l_retcode = 2
      THEN
         GOTO laststep;
      END IF;

      Load_Transactions (l_retcode, l_organization_id);

      IF l_retcode = 2
      THEN
         GOTO laststep;
      END IF;

      Gen_Demand_Update (l_retcode);

      IF l_retcode = 2
      THEN
         GOTO laststep;
      END IF;

      Demand_Update (l_retcode, l_organization_id);

      IF l_retcode = 2
      THEN
         GOTO laststep;
      END IF;

     <<laststep>>
      IF l_retcode = 2
      THEN
         retcode := 2;
         errbuf := g_errbuf;

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_MTL_DEMAND_HISTORY_PKG.Demand_Update_Main',
            p_calling             => g_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (g_errbuf, 1, 240),
            p_distribution_list   => g_dflt_email,
            p_module              => 'PO');
      END IF;
      
      apps.xxwc_mv_routines_add_pkg.disable_parallelism;  -- Version# 1.1
   END Demand_Update_Main;
END XXWC_MTL_DEMAND_HISTORY_PKG;
/
