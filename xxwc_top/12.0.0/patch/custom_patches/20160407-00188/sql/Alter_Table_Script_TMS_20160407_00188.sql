/******************************************************************************
   NAME   : Alter_Table_Script_TMS_20160407_00188.sql
   PURPOSE: Script to remove parallelism 
 
   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        04/07/2016  Gopi Damuluri    TMS# 20160407-00188 Script to remove parallelism
******************************************************************************/
ALTER TABLE XXWC.XXWC_MTL_DEMAND_HISTORIES NOPARALLEL;
/