CREATE OR REPLACE PACKAGE BODY apps.xxwc_ar_salesreps_pkg AS
  /******************************************************************************
     NAME:       xxwc.xxwc_ar_salesreps_pkg
     PURPOSE:    Package related to SR Hierarchy enhancement
  
     REVISIONS:
     Ver        Date        Author           Description
     ---------  ----------  ---------------  ------------------------------------
     1.0        04/02/2014      shariharan    Initial Version
     1.1        11/15/2014      shariharan    20141110-00027 bug fixes
     1.2        04/15/2015      Pahwa Nancy   ESMS Service-286819 bug fixes
     1.3        05/18/2015      Pahwa Nancy   ESMS Service-286821 To pull EEID added another source table
  ******************************************************************************/
  g_dflt_email VARCHAR2(50) := 'HDSOracleDevelopers@hdsupply.com';

  PROCEDURE send_email(l_string in varchar2) IS
    /*************************************************************************
    *   Procedure : send_email
    *
    *   PURPOSE:   This procedure sends email to SC group for any market/district/region creation.
    *  Author: Shankar Hariharan
    * Creation_date 04/02/2014
    * Last update Date 04/02/2014
    *   Parameter:
    *          IN  l_string
    * Version   Date         Name             TMS
    * 1.0      04/02/2014  Shankar Hariharan  TMS 20131021-00499      
    *
    * ************************************************************************/
    l_email_dist    varchar2(100) := 'HDSWCSupplyChainAlerts@hdsupply.com';
    l_db_name       varchar2(100);
    l_err_callfrom  VARCHAR2(50);
    l_err_callpoint VARCHAR2(50);
    l_error_code    NUMBER;
    l_error_message VARCHAR2(240);
  
  BEGIN
    l_err_callfrom  := 'XXWC_AR_SALESREPS_PKG.SEND_EMAIL';
    l_err_callpoint := 'Send Mail';
  
    select name into l_db_name from v$database;
  
    utl_mail.send(sender     => 'HDSWCSupplyChainAlerts@hdsupply.com',
                  recipients => l_email_dist,
                  cc         => NULL,
                  bcc        => NULL,
                  subject    => 'Notification from ' || l_db_name ||
                                ' instance ' || l_string ||
                                ' by SR Hierarchy Sync Process',
                  message    => 'Notification from ' || l_db_name ||
                                ' instance ' || l_string ||
                                ' by SR Hierarchy Sync Process',
                  mime_type  => 'text/plain; charset=us-ascii',
                  priority   => 3,
                  replyto    => NULL);
  EXCEPTION
    WHEN OTHERS THEN
      l_error_code    := SQLCODE;
      l_error_message := SUBSTR(SQLERRM, 1, 200);
      XXCUS_error_pkg.XXCUS_error_main_api(p_called_from       => l_err_callfrom,
                                           p_calling           => l_err_callpoint,
                                           p_request_id        => -1,
                                           p_ora_error_msg     => SQLERRM,
                                           p_error_desc        => l_error_message,
                                           p_distribution_list => g_dflt_email,
                                           p_module            => 'APPS');
    
  END;

  PROCEDURE sync_seed_data(errbuf OUT VARCHAR2, retcode OUT VARCHAR2) IS
    /*************************************************************************
    *   Procedure : sync_seed_data
    *
    *   PURPOSE:   This procedure syncs the market/district/region valueset with longview hierarchy data.
    *  Author: Shankar Hariharan
    * Creation_date 04/02/2014
    * Last update Date 04/02/2014
    *   Parameter:
    *          OUT  errbuf
    *          OUT retcode
    * Version   Date         Name             TMS
    * 1.0      04/02/2014  Shankar Hariharan  TMS 20131021-00499      
    * 2.0      04/09/2018  Nancy Pahwa        Task ID: 20180406-00157 - to handle AHH district,market/region
    * ************************************************************************/
    l_user_id        NUMBER := fnd_global.user_id;
    l_user_name      VARCHAR2(30) := NVL(fnd_global.user_name, 'AUTO');
    l_login_id       NUMBER := fnd_global.login_id;
    l_rowid          VARCHAR2(100);
    l_district_vs    VARCHAR2(30) := 'XXWC_DISTRICT';
    l_market_vs      VARCHAR2(30) := 'XXWC_MARKET';
    l_region_vs      VARCHAR2(30) := 'XXWC_REGION';
    l_district_vs_id NUMBER;
    l_region_vs_id   NUMBER;
    l_market_vs_id   NUMBER;
    l_view_app_id    NUMBER := 660;
    l_start_date     DATE;
    l_lookup_code    VARCHAR2(80);
    l_meaning        VARCHAR2(80);
    l_flex_value     VARCHAR2(100);
    l_err_callfrom   VARCHAR2(50);
    l_err_callpoint  VARCHAR2(50);
    l_error_code     NUMBER;
    l_error_message  VARCHAR2(240);
  --version 2.0 start
    CURSOR cur_district IS
 SELECT DISTINCT sym_nm6, sym_desc_e6, sym_nm5
   FROM XXWC_LONGVIEW_BRANCH_HIERARCHY a, mtl_parameters b
  WHERE TRIM(leaf_fru) IS NOT NULL
    AND TRIM(sym_nm6) IS NOT NULL
    AND b.attribute10 = a.leaf_fru
 union
 SELECT DISTINCT sym_nm6, sym_desc_e6, sym_nm5
   FROM xxwc.XXWC_LONGVIEW_BRANCH_HIERARCHY a
  WHERE (a.SYM_DESC_E7 like '%(AHH)%' OR a.SYM_DESC_E7 like '%(KCP)%' OR
        a.SYM_DESC_E7 like '%(HM)%')
    and TRIM(a.leaf_fru) IS NOT NULL
    AND TRIM(a.sym_nm6) IS NOT NULL
    AND NOT EXISTS
  (SELECT '1'
           FROM apps.org_organization_definitions c
          WHERE SUBSTR(SYM_DESC_E7, LENGTH(SYM_DESC_E7) - 2) =
                c.organization_code
            and NVL(disable_date, sysdate) >= sysdate);
  
    CURSOR cur_market IS
      SELECT DISTINCT sym_nm5, sym_desc_e5, sym_nm4
        FROM xxwc.XXWC_LONGVIEW_BRANCH_HIERARCHY a, apps.mtl_parameters b
       WHERE TRIM(leaf_fru) IS NOT NULL
         AND TRIM(sym_nm5) IS NOT NULL
         AND b.attribute10 = a.leaf_fru
        union
        SELECT DISTINCT sym_nm5, sym_desc_e5, sym_nm4
        FROM xxwc.XXWC_LONGVIEW_BRANCH_HIERARCHY a
 WHERE (a.SYM_DESC_E7 like '%(AHH)%' OR a.SYM_DESC_E7 like '%(KCP)%' OR
       a.SYM_DESC_E7 like '%(HM)%')
   and TRIM(a.leaf_fru) IS NOT NULL
   AND TRIM(a.sym_nm5) IS NOT NULL
   AND NOT EXISTS
 (SELECT '1'
          FROM apps.org_organization_definitions c
         WHERE SUBSTR(SYM_DESC_E7, LENGTH(SYM_DESC_E7) - 2) =
               c.organization_code
           and NVL(disable_date, sysdate) >= sysdate);
  
    CURSOR cur_region IS
      SELECT DISTINCT sym_nm4, sym_desc_e4
        FROM xxwc.XXWC_LONGVIEW_BRANCH_HIERARCHY a, apps.mtl_parameters b
       WHERE TRIM(leaf_fru) IS NOT NULL
         AND TRIM(sym_nm4) IS NOT NULL
         AND b.attribute10 = a.leaf_fru
         union
         SELECT DISTINCT sym_nm4, sym_desc_e4
        FROM xxwc.XXWC_LONGVIEW_BRANCH_HIERARCHY a
 WHERE (a.SYM_DESC_E7 like '%(AHH)%' OR a.SYM_DESC_E7 like '%(KCP)%' OR
       a.SYM_DESC_E7 like '%(HM)%')
   and TRIM(a.leaf_fru) IS NOT NULL
   AND TRIM(a.sym_nm4) IS NOT NULL
   AND NOT EXISTS
 (SELECT '1'
          FROM apps.org_organization_definitions c
         WHERE SUBSTR(SYM_DESC_E7, LENGTH(SYM_DESC_E7) - 2) =
               c.organization_code
           and NVL(disable_date, sysdate) >= sysdate);
    --version 2.0 end
    CURSOR cur_fru IS
      SELECT DISTINCT a.leaf_fru, a.SYM_desc_e6, a.SYM_desc_e4
        FROM XXWC_LONGVIEW_BRANCH_HIERARCHY a, mtl_parameters b
       WHERE a.leaf_fru = b.attribute10
         AND TRIM(a.leaf_fru) IS NOT NULL
         AND TRIM(sym_desc_e6) IS NOT NULL
         AND TRIM(sym_desc_e4) IS NOT NULL
         AND (a.sym_desc_e6 <> b.attribute8 OR
             a.sym_desc_e4 <> b.attribute9);
  BEGIN
    l_err_callfrom  := 'XXWC_AR_SALESREPS_PKG.SYNC_SEED_DATA';
    l_err_callpoint := 'Update Mtl Parameters with District Information';
  
    --===========================================================================
    -- Sync Branch Parameters
    --===========================================================================
    FND_FILE.Put_Line(FND_FILE.Output,
                      '             Sync Branch Parameters        ');
    FND_FILE.Put_Line(FND_FILE.Output,
                      '==================================');
  
    FOR cur_fru_rec IN cur_fru LOOP
      UPDATE mtl_parameters
         SET attribute8       = cur_fru_rec.sym_desc_e6,
             attribute9       = cur_fru_rec.sym_desc_e4,
             last_update_date = sysdate,
             last_updated_by  = l_user_id
       WHERE attribute10 = cur_fru_rec.leaf_fru;
    END LOOP;
  
    COMMIT;
    l_err_callpoint := 'Select Value Set information';
    SELECT flex_value_set_id
      INTO l_district_vs_id
      FROM Fnd_flex_value_sets
     WHERE flex_value_set_name = l_district_vs;
  
    SELECT flex_value_set_id
      INTO l_market_vs_id
      FROM Fnd_flex_value_sets
     WHERE flex_value_set_name = l_market_vs;
  
    SELECT flex_value_set_id
      INTO l_region_vs_id
      FROM Fnd_flex_value_sets
     WHERE flex_value_set_name = l_region_vs;
  
    --===========================================================================
    -- Sync District
    --===========================================================================
    FND_FILE.Put_Line(FND_FILE.Output,
                      '             Sync District        ');
    FND_FILE.Put_Line(FND_FILE.Output,
                      '==================================');
    l_err_callpoint := 'Sync District Value Set';
    FOR cur_district_rec IN cur_district LOOP
      --Sync Value Sets
      BEGIN
        SELECT b.flex_value
          INTO l_flex_value
          FROM Fnd_flex_value_sets a, Fnd_flex_values_vl b
         WHERE a.flex_value_set_name = l_district_vs
           AND a.flex_value_set_id = b.flex_value_set_id
           AND b.flex_value = cur_district_rec.sym_desc_e6;
      EXCEPTION
        WHEN OTHERS THEN
          Fnd_flex_values_pkg.insert_row(x_rowid                      => l_rowid,
                                         x_flex_value_id              => Fnd_flex_values_s.NEXTVAL,
                                         x_attribute_sort_order       => NULL,
                                         x_flex_value_set_id          => l_district_vs_id,
                                         x_flex_value                 => cur_district_rec.sym_desc_e6,
                                         x_enabled_flag               => 'Y',
                                         x_summary_flag               => 'N',
                                         x_start_date_active          => NULL,
                                         x_end_date_active            => NULL,
                                         x_parent_flex_value_low      => NULL,
                                         x_parent_flex_value_high     => NULL,
                                         x_structured_hierarchy_level => NULL,
                                         x_hierarchy_level            => NULL,
                                         x_compiled_value_attributes  => NULL,
                                         x_value_category             => 'XXWC_DISTRICT_DFF',
                                         x_attribute1                 => NULL,
                                         x_attribute2                 => cur_district_rec.sym_nm5,
                                         x_attribute3                 => NULL,
                                         x_attribute4                 => NULL,
                                         x_attribute5                 => NULL,
                                         x_attribute6                 => NULL,
                                         x_attribute7                 => NULL,
                                         x_attribute8                 => NULL,
                                         x_attribute9                 => NULL,
                                         x_attribute10                => NULL,
                                         x_attribute11                => NULL,
                                         x_attribute12                => NULL,
                                         x_attribute13                => NULL,
                                         x_attribute14                => NULL,
                                         x_attribute15                => NULL,
                                         x_attribute16                => NULL,
                                         x_attribute17                => NULL,
                                         x_attribute18                => NULL,
                                         x_attribute19                => NULL,
                                         x_attribute20                => NULL,
                                         x_attribute21                => NULL,
                                         x_attribute22                => NULL,
                                         x_attribute23                => NULL,
                                         x_attribute24                => NULL,
                                         x_attribute25                => NULL,
                                         x_attribute26                => NULL,
                                         x_attribute27                => NULL,
                                         x_attribute28                => NULL,
                                         x_attribute29                => NULL,
                                         x_attribute30                => NULL,
                                         x_attribute31                => NULL,
                                         x_attribute32                => NULL,
                                         x_attribute33                => NULL,
                                         x_attribute34                => NULL,
                                         x_attribute35                => NULL,
                                         x_attribute36                => NULL,
                                         x_attribute37                => NULL,
                                         x_attribute38                => NULL,
                                         x_attribute39                => NULL,
                                         x_attribute40                => NULL,
                                         x_attribute41                => NULL,
                                         x_attribute42                => NULL,
                                         x_attribute43                => NULL,
                                         x_attribute44                => NULL,
                                         x_attribute45                => NULL,
                                         x_attribute46                => NULL,
                                         x_attribute47                => NULL,
                                         x_attribute48                => NULL,
                                         x_attribute49                => NULL,
                                         x_attribute50                => NULL,
                                         x_flex_value_meaning         => cur_district_rec.sym_desc_e6,
                                         x_description                => cur_district_rec.sym_desc_e6,
                                         x_creation_date              => SYSDATE,
                                         x_created_by                 => l_user_id,
                                         x_last_update_date           => SYSDATE,
                                         x_last_updated_by            => l_user_id,
                                         x_last_update_login          => l_login_id);
          send_email('New District with code ' ||
                     cur_district_rec.sym_desc_e6 || ' and description ' ||
                     cur_district_rec.sym_desc_e6 || ' added.');
      END;
    
      --Sync Value Sets
      BEGIN
        SELECT b.flex_value
          INTO l_flex_value
          FROM Fnd_flex_value_sets a, Fnd_flex_values_vl b
         WHERE a.flex_value_set_name = l_district_vs
           AND a.flex_value_set_id = b.flex_value_set_id
           AND b.flex_value = cur_district_rec.sym_nm6;
      EXCEPTION
        WHEN OTHERS THEN
          Fnd_flex_values_pkg.insert_row(x_rowid                      => l_rowid,
                                         x_flex_value_id              => Fnd_flex_values_s.NEXTVAL,
                                         x_attribute_sort_order       => NULL,
                                         x_flex_value_set_id          => l_district_vs_id,
                                         x_flex_value                 => cur_district_rec.sym_nm6,
                                         x_enabled_flag               => 'Y',
                                         x_summary_flag               => 'N',
                                         x_start_date_active          => NULL,
                                         x_end_date_active            => NULL,
                                         x_parent_flex_value_low      => NULL,
                                         x_parent_flex_value_high     => NULL,
                                         x_structured_hierarchy_level => NULL,
                                         x_hierarchy_level            => NULL,
                                         x_compiled_value_attributes  => NULL,
                                         x_value_category             => 'XXWC_DISTRICT_DFF',
                                         x_attribute1                 => NULL,
                                         x_attribute2                 => cur_district_rec.sym_nm5,
                                         x_attribute3                 => NULL,
                                         x_attribute4                 => NULL,
                                         x_attribute5                 => NULL,
                                         x_attribute6                 => NULL,
                                         x_attribute7                 => NULL,
                                         x_attribute8                 => NULL,
                                         x_attribute9                 => NULL,
                                         x_attribute10                => NULL,
                                         x_attribute11                => NULL,
                                         x_attribute12                => NULL,
                                         x_attribute13                => NULL,
                                         x_attribute14                => NULL,
                                         x_attribute15                => NULL,
                                         x_attribute16                => NULL,
                                         x_attribute17                => NULL,
                                         x_attribute18                => NULL,
                                         x_attribute19                => NULL,
                                         x_attribute20                => NULL,
                                         x_attribute21                => NULL,
                                         x_attribute22                => NULL,
                                         x_attribute23                => NULL,
                                         x_attribute24                => NULL,
                                         x_attribute25                => NULL,
                                         x_attribute26                => NULL,
                                         x_attribute27                => NULL,
                                         x_attribute28                => NULL,
                                         x_attribute29                => NULL,
                                         x_attribute30                => NULL,
                                         x_attribute31                => NULL,
                                         x_attribute32                => NULL,
                                         x_attribute33                => NULL,
                                         x_attribute34                => NULL,
                                         x_attribute35                => NULL,
                                         x_attribute36                => NULL,
                                         x_attribute37                => NULL,
                                         x_attribute38                => NULL,
                                         x_attribute39                => NULL,
                                         x_attribute40                => NULL,
                                         x_attribute41                => NULL,
                                         x_attribute42                => NULL,
                                         x_attribute43                => NULL,
                                         x_attribute44                => NULL,
                                         x_attribute45                => NULL,
                                         x_attribute46                => NULL,
                                         x_attribute47                => NULL,
                                         x_attribute48                => NULL,
                                         x_attribute49                => NULL,
                                         x_attribute50                => NULL,
                                         x_flex_value_meaning         => cur_district_rec.sym_desc_e6,
                                         x_description                => cur_district_rec.sym_desc_e6,
                                         x_creation_date              => SYSDATE,
                                         x_created_by                 => l_user_id,
                                         x_last_update_date           => SYSDATE,
                                         x_last_updated_by            => l_user_id,
                                         x_last_update_login          => l_login_id);
          send_email('New District with code ' || cur_district_rec.sym_nm6 ||
                     ' and description ' || cur_district_rec.sym_desc_e6 ||
                     ' added.');
      END;
    END LOOP;
  
    COMMIT;
    --===========================================================================
    -- Sync District Table
    --===========================================================================
    FND_FILE.Put_Line(FND_FILE.Output,
                      '             Sync District Table       ');
    FND_FILE.Put_Line(FND_FILE.Output,
                      '======================================');
    l_err_callpoint := 'Sync District Custom Table';
    FOR cur_district_rec IN cur_district LOOP
      BEGIN
        SELECT dist_code, dist_name
          INTO l_lookup_code, l_meaning
          FROM xxwc_br_districts
         WHERE dist_code = cur_district_rec.sym_nm6;
      
        IF l_meaning <> cur_district_rec.sym_desc_e6 THEN
          UPDATE xxwc_br_districts
             SET dist_name = cur_district_rec.sym_desc_e6
           WHERE dist_code = cur_district_rec.sym_nm6;
        
          FND_FILE.Put_Line(FND_FILE.Output,
                            'Updated District Meaning for ' ||
                            cur_district_rec.sym_nm6 || ' from ' ||
                            l_meaning || ' to ' ||
                            cur_district_rec.sym_desc_e6);
        END IF;
      EXCEPTION
        WHEN OTHERS THEN
          /*
          SELECT start_date
            INTO l_start_date
            FROM xxwc.xxwc_br_districts
           WHERE ROWNUM = 1;*/
        
          INSERT INTO xxwc_br_districts
            (dist_code,
             dist_name,
             start_date,
             end_date,
             last_update_date,
             last_update_user)
          VALUES
            (cur_district_rec.sym_nm6,
             cur_district_rec.sym_desc_e6,
             l_start_date,
             NULL,
             SYSDATE,
             l_user_name);
        
          FND_FILE.Put_Line(FND_FILE.Output,
                            'Created District ' || cur_district_rec.sym_nm6 ||
                            ' - ' || cur_district_rec.sym_desc_e6);
      END;
    END LOOP;
  
    COMMIT;
  
    --===========================================================================
    -- Sync Market
    --===========================================================================
    FND_FILE.Put_Line(FND_FILE.Output, '             Sync Market        ');
    FND_FILE.Put_Line(FND_FILE.Output,
                      '==================================');
    l_err_callpoint := 'Sync Market Value Set';
    FOR cur_market_rec IN cur_market LOOP
      /*
      --Sync Value Sets
      BEGIN
         SELECT b.flex_value
           INTO l_flex_value
           FROM Fnd_flex_value_sets a, Fnd_flex_values b
          WHERE     a.flex_value_set_name = l_market_vs
                AND a.flex_value_set_id = b.flex_value_set_id
                AND b.flex_value = c_market_rec.sym_desc_e5;
      EXCEPTION
         WHEN OTHERS
         THEN
            Fnd_flex_values_pkg.insert_row (
               x_rowid                        => l_rowid,
               x_flex_value_id                => Fnd_flex_values_s.NEXTVAL,
               x_attribute_sort_order         => NULL,
               x_flex_value_set_id            => l_market_vs_id,
               x_flex_value                   => c_market_rec.sym_desc_e5,
               x_enabled_flag                 => 'Y',
               x_summary_flag                 => 'N',
               x_start_date_active            => NULL,
               x_end_date_active              => NULL,
               x_parent_flex_value_low        => NULL,
               x_parent_flex_value_high       => NULL,
               x_structured_hierarchy_level   => NULL,
               x_hierarchy_level              => NULL,
               x_compiled_value_attributes    => NULL,
               x_value_category               => 'XXWC_MARKET_DFF',
               x_attribute1                   => NULL,
               x_attribute2                   => c_market_rec.sym_nm4,
               x_attribute3                   => NULL,
               x_attribute4                   => NULL,
               x_attribute5                   => NULL,
               x_attribute6                   => NULL,
               x_attribute7                   => NULL,
               x_attribute8                   => NULL,
               x_attribute9                   => NULL,
               x_attribute10                  => NULL,
               x_attribute11                  => NULL,
               x_attribute12                  => NULL,
               x_attribute13                  => NULL,
               x_attribute14                  => NULL,
               x_attribute15                  => NULL,
               x_attribute16                  => NULL,
               x_attribute17                  => NULL,
               x_attribute18                  => NULL,
               x_attribute19                  => NULL,
               x_attribute20                  => NULL,
               x_attribute21                  => NULL,
               x_attribute22                  => NULL,
               x_attribute23                  => NULL,
               x_attribute24                  => NULL,
               x_attribute25                  => NULL,
               x_attribute26                  => NULL,
               x_attribute27                  => NULL,
               x_attribute28                  => NULL,
               x_attribute29                  => NULL,
               x_attribute30                  => NULL,
               x_attribute31                  => NULL,
               x_attribute32                  => NULL,
               x_attribute33                  => NULL,
               x_attribute34                  => NULL,
               x_attribute35                  => NULL,
               x_attribute36                  => NULL,
               x_attribute37                  => NULL,
               x_attribute38                  => NULL,
               x_attribute39                  => NULL,
               x_attribute40                  => NULL,
               x_attribute41                  => NULL,
               x_attribute42                  => NULL,
               x_attribute43                  => NULL,
               x_attribute44                  => NULL,
               x_attribute45                  => NULL,
               x_attribute46                  => NULL,
               x_attribute47                  => NULL,
               x_attribute48                  => NULL,
               x_attribute49                  => NULL,
               x_attribute50                  => NULL,
               x_flex_value_meaning           => c_market_rec.sym_desc_e5,
               x_description                  => c_market_rec.sym_desc_e5,
               x_creation_date                => SYSDATE,
               x_created_by                   => l_user_id,
               x_last_update_date             => SYSDATE,
               x_last_updated_by              => l_user_id,
               x_last_update_login            => l_login_id);
      END;
      */
      --Sync Value Sets
      BEGIN
        SELECT b.flex_value
          INTO l_flex_value
          FROM Fnd_flex_value_sets a, Fnd_flex_values b
         WHERE a.flex_value_set_name = l_market_vs
           AND a.flex_value_set_id = b.flex_value_set_id
           AND b.flex_value = cur_market_rec.sym_nm5;
      EXCEPTION
        WHEN OTHERS THEN
          Fnd_flex_values_pkg.insert_row(x_rowid                      => l_rowid,
                                         x_flex_value_id              => Fnd_flex_values_s.NEXTVAL,
                                         x_attribute_sort_order       => NULL,
                                         x_flex_value_set_id          => l_market_vs_id,
                                         x_flex_value                 => cur_market_rec.sym_nm5,
                                         x_enabled_flag               => 'Y',
                                         x_summary_flag               => 'N',
                                         x_start_date_active          => NULL,
                                         x_end_date_active            => NULL,
                                         x_parent_flex_value_low      => NULL,
                                         x_parent_flex_value_high     => NULL,
                                         x_structured_hierarchy_level => NULL,
                                         x_hierarchy_level            => NULL,
                                         x_compiled_value_attributes  => NULL,
                                         x_value_category             => 'XXWC_MARKET_DFF',
                                         x_attribute1                 => NULL,
                                         x_attribute2                 => cur_market_rec.sym_nm4,
                                         x_attribute3                 => NULL,
                                         x_attribute4                 => NULL,
                                         x_attribute5                 => NULL,
                                         x_attribute6                 => NULL,
                                         x_attribute7                 => NULL,
                                         x_attribute8                 => NULL,
                                         x_attribute9                 => NULL,
                                         x_attribute10                => NULL,
                                         x_attribute11                => NULL,
                                         x_attribute12                => NULL,
                                         x_attribute13                => NULL,
                                         x_attribute14                => NULL,
                                         x_attribute15                => NULL,
                                         x_attribute16                => NULL,
                                         x_attribute17                => NULL,
                                         x_attribute18                => NULL,
                                         x_attribute19                => NULL,
                                         x_attribute20                => NULL,
                                         x_attribute21                => NULL,
                                         x_attribute22                => NULL,
                                         x_attribute23                => NULL,
                                         x_attribute24                => NULL,
                                         x_attribute25                => NULL,
                                         x_attribute26                => NULL,
                                         x_attribute27                => NULL,
                                         x_attribute28                => NULL,
                                         x_attribute29                => NULL,
                                         x_attribute30                => NULL,
                                         x_attribute31                => NULL,
                                         x_attribute32                => NULL,
                                         x_attribute33                => NULL,
                                         x_attribute34                => NULL,
                                         x_attribute35                => NULL,
                                         x_attribute36                => NULL,
                                         x_attribute37                => NULL,
                                         x_attribute38                => NULL,
                                         x_attribute39                => NULL,
                                         x_attribute40                => NULL,
                                         x_attribute41                => NULL,
                                         x_attribute42                => NULL,
                                         x_attribute43                => NULL,
                                         x_attribute44                => NULL,
                                         x_attribute45                => NULL,
                                         x_attribute46                => NULL,
                                         x_attribute47                => NULL,
                                         x_attribute48                => NULL,
                                         x_attribute49                => NULL,
                                         x_attribute50                => NULL,
                                         x_flex_value_meaning         => cur_market_rec.sym_desc_e5,
                                         x_description                => cur_market_rec.sym_desc_e5,
                                         x_creation_date              => SYSDATE,
                                         x_created_by                 => l_user_id,
                                         x_last_update_date           => SYSDATE,
                                         x_last_updated_by            => l_user_id,
                                         x_last_update_login          => l_login_id);
          send_email('New Market with code ' || cur_market_rec.sym_nm5 ||
                     ' and description ' || cur_market_rec.sym_desc_e5 ||
                     ' added.');
      END;
    
    END LOOP;
  
    COMMIT;
    --===========================================================================
    -- Sync Region
    --===========================================================================
    FND_FILE.Put_Line(FND_FILE.Output, '             Sync Region        ');
    FND_FILE.Put_Line(FND_FILE.Output,
                      '==================================');
    l_err_callpoint := 'Sync Region Value Set';
    FOR cur_region_rec IN cur_region LOOP
      --Sync Value Sets
      BEGIN
        SELECT b.flex_value
          INTO l_flex_value
          FROM Fnd_flex_value_sets a, Fnd_flex_values b
         WHERE a.flex_value_set_name = l_region_vs
           AND a.flex_value_set_id = b.flex_value_set_id
           AND b.flex_value = cur_region_rec.sym_desc_e4;
      EXCEPTION
        WHEN OTHERS THEN
          Fnd_flex_values_pkg.insert_row(x_rowid                      => l_rowid,
                                         x_flex_value_id              => Fnd_flex_values_s.NEXTVAL,
                                         x_attribute_sort_order       => NULL,
                                         x_flex_value_set_id          => l_region_vs_id,
                                         x_flex_value                 => cur_region_rec.sym_desc_e4,
                                         x_enabled_flag               => 'Y',
                                         x_summary_flag               => 'N',
                                         x_start_date_active          => NULL,
                                         x_end_date_active            => NULL,
                                         x_parent_flex_value_low      => NULL,
                                         x_parent_flex_value_high     => NULL,
                                         x_structured_hierarchy_level => NULL,
                                         x_hierarchy_level            => NULL,
                                         x_compiled_value_attributes  => NULL,
                                         x_value_category             => NULL,
                                         x_attribute1                 => NULL,
                                         x_attribute2                 => NULL,
                                         x_attribute3                 => NULL,
                                         x_attribute4                 => NULL,
                                         x_attribute5                 => NULL,
                                         x_attribute6                 => NULL,
                                         x_attribute7                 => NULL,
                                         x_attribute8                 => NULL,
                                         x_attribute9                 => NULL,
                                         x_attribute10                => NULL,
                                         x_attribute11                => NULL,
                                         x_attribute12                => NULL,
                                         x_attribute13                => NULL,
                                         x_attribute14                => NULL,
                                         x_attribute15                => NULL,
                                         x_attribute16                => NULL,
                                         x_attribute17                => NULL,
                                         x_attribute18                => NULL,
                                         x_attribute19                => NULL,
                                         x_attribute20                => NULL,
                                         x_attribute21                => NULL,
                                         x_attribute22                => NULL,
                                         x_attribute23                => NULL,
                                         x_attribute24                => NULL,
                                         x_attribute25                => NULL,
                                         x_attribute26                => NULL,
                                         x_attribute27                => NULL,
                                         x_attribute28                => NULL,
                                         x_attribute29                => NULL,
                                         x_attribute30                => NULL,
                                         x_attribute31                => NULL,
                                         x_attribute32                => NULL,
                                         x_attribute33                => NULL,
                                         x_attribute34                => NULL,
                                         x_attribute35                => NULL,
                                         x_attribute36                => NULL,
                                         x_attribute37                => NULL,
                                         x_attribute38                => NULL,
                                         x_attribute39                => NULL,
                                         x_attribute40                => NULL,
                                         x_attribute41                => NULL,
                                         x_attribute42                => NULL,
                                         x_attribute43                => NULL,
                                         x_attribute44                => NULL,
                                         x_attribute45                => NULL,
                                         x_attribute46                => NULL,
                                         x_attribute47                => NULL,
                                         x_attribute48                => NULL,
                                         x_attribute49                => NULL,
                                         x_attribute50                => NULL,
                                         x_flex_value_meaning         => cur_region_rec.sym_desc_e4,
                                         x_description                => cur_region_rec.sym_desc_e4,
                                         x_creation_date              => SYSDATE,
                                         x_created_by                 => l_user_id,
                                         x_last_update_date           => SYSDATE,
                                         x_last_updated_by            => l_user_id,
                                         x_last_update_login          => l_login_id);
          send_email('New Region with code ' || cur_region_rec.sym_desc_e4 ||
                     ' and description ' || cur_region_rec.sym_desc_e4 ||
                     ' added.');
      END;
    
      --Sync Value Sets
      BEGIN
        SELECT b.flex_value
          INTO l_flex_value
          FROM Fnd_flex_value_sets a, Fnd_flex_values b
         WHERE a.flex_value_set_name = l_region_vs
           AND a.flex_value_set_id = b.flex_value_set_id
           AND b.flex_value = cur_region_rec.sym_nm4;
      EXCEPTION
        WHEN OTHERS THEN
          Fnd_flex_values_pkg.insert_row(x_rowid                      => l_rowid,
                                         x_flex_value_id              => Fnd_flex_values_s.NEXTVAL,
                                         x_attribute_sort_order       => NULL,
                                         x_flex_value_set_id          => l_region_vs_id,
                                         x_flex_value                 => cur_region_rec.sym_nm4,
                                         x_enabled_flag               => 'Y',
                                         x_summary_flag               => 'N',
                                         x_start_date_active          => NULL,
                                         x_end_date_active            => NULL,
                                         x_parent_flex_value_low      => NULL,
                                         x_parent_flex_value_high     => NULL,
                                         x_structured_hierarchy_level => NULL,
                                         x_hierarchy_level            => NULL,
                                         x_compiled_value_attributes  => NULL,
                                         x_value_category             => NULL,
                                         x_attribute1                 => NULL,
                                         x_attribute2                 => NULL,
                                         x_attribute3                 => NULL,
                                         x_attribute4                 => NULL,
                                         x_attribute5                 => NULL,
                                         x_attribute6                 => NULL,
                                         x_attribute7                 => NULL,
                                         x_attribute8                 => NULL,
                                         x_attribute9                 => NULL,
                                         x_attribute10                => NULL,
                                         x_attribute11                => NULL,
                                         x_attribute12                => NULL,
                                         x_attribute13                => NULL,
                                         x_attribute14                => NULL,
                                         x_attribute15                => NULL,
                                         x_attribute16                => NULL,
                                         x_attribute17                => NULL,
                                         x_attribute18                => NULL,
                                         x_attribute19                => NULL,
                                         x_attribute20                => NULL,
                                         x_attribute21                => NULL,
                                         x_attribute22                => NULL,
                                         x_attribute23                => NULL,
                                         x_attribute24                => NULL,
                                         x_attribute25                => NULL,
                                         x_attribute26                => NULL,
                                         x_attribute27                => NULL,
                                         x_attribute28                => NULL,
                                         x_attribute29                => NULL,
                                         x_attribute30                => NULL,
                                         x_attribute31                => NULL,
                                         x_attribute32                => NULL,
                                         x_attribute33                => NULL,
                                         x_attribute34                => NULL,
                                         x_attribute35                => NULL,
                                         x_attribute36                => NULL,
                                         x_attribute37                => NULL,
                                         x_attribute38                => NULL,
                                         x_attribute39                => NULL,
                                         x_attribute40                => NULL,
                                         x_attribute41                => NULL,
                                         x_attribute42                => NULL,
                                         x_attribute43                => NULL,
                                         x_attribute44                => NULL,
                                         x_attribute45                => NULL,
                                         x_attribute46                => NULL,
                                         x_attribute47                => NULL,
                                         x_attribute48                => NULL,
                                         x_attribute49                => NULL,
                                         x_attribute50                => NULL,
                                         x_flex_value_meaning         => cur_region_rec.sym_desc_e4,
                                         x_description                => cur_region_rec.sym_desc_e4,
                                         x_creation_date              => SYSDATE,
                                         x_created_by                 => l_user_id,
                                         x_last_update_date           => SYSDATE,
                                         x_last_updated_by            => l_user_id,
                                         x_last_update_login          => l_login_id);
          send_email('New Region with code ' || cur_region_rec.sym_nm4 ||
                     ' and description ' || cur_region_rec.sym_desc_e4 ||
                     ' added.');
      END;
    END LOOP;
  EXCEPTION
    WHEN OTHERS THEN
      l_error_code    := SQLCODE;
      l_error_message := SUBSTR(SQLERRM, 1, 200);
      XXCUS_error_pkg.XXCUS_error_main_api(p_called_from       => l_err_callfrom,
                                           p_calling           => l_err_callpoint,
                                           p_request_id        => -1,
                                           p_ora_error_msg     => SQLERRM,
                                           p_error_desc        => l_error_message,
                                           p_distribution_list => g_dflt_email,
                                           p_module            => 'APPS');
    
  END sync_seed_data;

  PROCEDURE sync_sr_hierarchy(i_dst_code IN VARCHAR2, i_sr_id IN NUMBER) IS
    /*************************************************************************
    *   Procedure : sync_sr_hierarchy
    *
    *   PURPOSE:   This procedure syncs the SR Hierarchy for a give district / SR
    *  Author: Shankar Hariharan
    * Creation_date 04/02/2014
    * Last update Date 04/02/2014
    *   Parameter:
    *          IN   I_DST_CODE
    *          IN  i_SR_ID
    * Version   Date         Name             TMS
    * 1.0      04/02/2014  Shankar Hariharan  TMS 20131021-00499      
    *
    * ************************************************************************/
    l_err_callfrom  VARCHAR2(50);
    l_err_callpoint VARCHAR2(50);
    l_error_code    NUMBER;
    l_error_message VARCHAR2(240);
  
    CURSOR cur_sr IS
      SELECT salesrep_id, home_district_code
        FROM xxwc_sr_salesreps a
       WHERE home_district_code = NVL(i_dst_code, home_district_code)
         AND salesrep_id = NVL(i_sr_id, salesrep_id)
         AND auto_update = 'Y';
  
    CURSOR cur_dist_rup(i_home_district_code IN VARCHAR2) IS
      SELECT *
        FROM XXWC_BR_DISTRICT_ROLLUP
       WHERE dist_code = i_home_district_code
         AND SYSDATE BETWEEN NVL(start_date, SYSDATE - 1) AND
             NVL(end_date, SYSDATE + 1);
  BEGIN
    l_err_callfrom := 'XXWC_AR_SALESREPS_PKG.SYNC_SR_HIERARCHY';
  
    FOR cur_sr_rec IN cur_sr LOOP
      DELETE FROM XXWC_SR_SALESREP_HIERARCHY
       WHERE sr_id = cur_sr_rec.salesrep_id;
    
      FOR cur_dist_rup_rec IN cur_dist_rup(cur_sr_rec.home_district_code) LOOP
        l_err_callpoint := 'Sync SR Hierarchy for ' ||
                           cur_sr_rec.home_district_code || ' ' ||
                           cur_sr_rec.salesrep_id;
        INSERT INTO XXWC_SR_SALESREP_HIERARCHY
          (SR_HIER_ID,
           SR_ID,
           MGT_LVL,
           MGT_DESC,
           MGR_EEID,
           MGR_NAME,
           START_DATE,
           END_DATE,
           LAST_UPDATE_DATE,
           LAST_UPDATE_USER)
        VALUES
          (XXWC_SR_SALESREP_HIERARCHY_S.NEXTVAL,
           cur_sr_rec.salesrep_id,
           cur_dist_rup_rec.mgt_lvl,
           cur_dist_rup_rec.mgt_desc,
           cur_dist_rup_rec.MGR_EEID,
           cur_dist_rup_rec.MGR_NAME,
           cur_dist_rup_rec.START_DATE,
           cur_dist_rup_rec.END_DATE,
           SYSDATE,
           Fnd_global.user_name);
      END LOOP;
    END LOOP;
  EXCEPTION
    WHEN OTHERS THEN
      l_error_code    := SQLCODE;
      l_error_message := SUBSTR(SQLERRM, 1, 200);
      XXCUS_error_pkg.XXCUS_error_main_api(p_called_from       => l_err_callfrom,
                                           p_calling           => l_err_callpoint,
                                           p_request_id        => -1,
                                           p_ora_error_msg     => SQLERRM,
                                           p_error_desc        => l_error_message,
                                           p_distribution_list => g_dflt_email,
                                           p_module            => 'APPS');
    
  END sync_sr_hierarchy;

  PROCEDURE update_snapshot(errbuf        OUT VARCHAR2,
                            retcode       OUT VARCHAR2,
                            i_period_name IN VARCHAR2) IS
    /*************************************************************************
    *   Procedure : update_snapshot
    *
    *   PURPOSE:   This procedure updates the snapshot table at the end of each fiscal period
    *  Author: Shankar Hariharan
    * Creation_date 04/02/2014
    * Last update Date 04/02/2014
    *   Parameter:
    *          OUT  errbuf
    *          OUT retcode
    *          IN   i_period_name
    * Version   Date         Name             TMS
    * 1.0      04/02/2014  Shankar Hariharan  TMS 20131021-00499      
    *
    * ************************************************************************/
    l_start_date    DATE;
    l_pr_start_date DATE;
    l_pr_end_date   DATE;
    l_err_callfrom  VARCHAR2(50);
    l_err_callpoint VARCHAR2(50);
    l_error_code    NUMBER;
    l_error_message VARCHAR2(240);
  
  BEGIN
    l_err_callfrom  := 'XXWC_AR_SALESREPS_PKG.UPDATE_SNAPSHOT';
    l_err_callpoint := 'Select End Date for the period';
    SELECT start_date
      INTO l_start_date
      FROM gl_periods
     WHERE period_set_name = '4-4-QTR'
       AND period_name = i_period_name
       AND adjustment_period_flag = 'N';
  
    SELECT start_date, end_date
      INTO l_pr_start_date, l_pr_end_date
      FROM gl_periods
     WHERE period_set_name = '4-4-QTR'
       AND end_date = l_start_date - 1
       AND adjustment_period_flag = 'N';
  
    l_err_callpoint := 'Before Br Mgt Levels SS Insert';
  
    INSERT INTO XXWC_BR_MGMT_LEVELS_SS
      (MGT_ID,
       SS_DATE,
       MGT_LVL,
       MGT_DESC,
       START_DATE,
       END_DATE,
       LAST_UPDATE_DATE,
       LAST_UPDATE_USER)
      SELECT MGT_ID,
             SYSDATE,
             MGT_LVL,
             MGT_DESC,
             l_pr_START_DATE,
             NVL(end_date, l_pr_END_DATE),
             LAST_UPDATE_DATE,
             LAST_UPDATE_USER
        FROM XXWC_BR_MGMT_LEVELS a
       WHERE NOT EXISTS (SELECT 1
                FROM XXWC_BR_MGMT_LEVELS_SS b
               WHERE b.mgt_id = a.mgt_id
                 AND b.start_date = l_pr_start_date
                 AND b.end_date = l_pr_end_date);
  
    l_err_callpoint := 'Before Br Mgt Levels SS Update';
  
    UPDATE XXWC_BR_MGMT_LEVELS SET start_date = l_start_date;
  
    --WHERE end_date IS NULL;
  
    --DELETE FROM xxwc.XXWC_BR_MGMT_LEVELS
    --    WHERE end_date IS NOT NULL;
    l_err_callpoint := 'Before Br Districts SS Insert';
  
    INSERT INTO XXWC_BR_DISTRICTS_SS
      (DIST_CODE,
       DIST_NAME,
       SS_DATE,
       START_DATE,
       END_DATE,
       LAST_UPDATE_DATE,
       LAST_UPDATE_USER)
      SELECT DIST_CODE,
             DIST_NAME,
             SYSDATE,
             l_pr_START_DATE,
             NVL(end_date, l_pr_END_DATE),
             LAST_UPDATE_DATE,
             LAST_UPDATE_USER
        FROM XXWC_BR_DISTRICTS a
       WHERE NOT EXISTS (SELECT 1
                FROM XXWC_BR_DISTRICTS_SS b
               WHERE b.dist_code = a.dist_code
                 AND b.start_date = l_pr_start_date
                 AND b.end_date = l_pr_end_date);
  
    l_err_callpoint := 'Before Br Districts SS Update';
  
    UPDATE XXWC_BR_DISTRICTS SET start_date = l_start_date;
  
    --       WHERE end_date IS NULL;
  
    --    DELETE FROM xxwc.XXWC_BR_DISTRICTS
    --        WHERE end_date IS NOT NULL;
  
    l_err_callpoint := 'Before Br District Rollup SS Insert';
  
    INSERT INTO XXWC_BR_DISTRICT_ROLLUP_SS
      (DIST_HIER_ID,
       SS_DATE,
       DIST_CODE,
       MGT_LVL,
       MGT_DESC,
       MGR_EEID,
       MGR_NAME,
       START_DATE,
       END_DATE,
       LAST_UPDATE_DATE,
       LAST_UPDATE_USER)
      SELECT DIST_HIER_ID,
             SYSDATE,
             DIST_CODE,
             MGT_LVL,
             MGT_DESC,
             MGR_EEID,
             MGR_NAME,
             L_PR_START_DATE,
             NVL(end_date, l_pr_END_DATE),
             LAST_UPDATE_DATE,
             LAST_UPDATE_USER
        FROM XXWC_BR_DISTRICT_ROLLUP a
       WHERE NOT EXISTS (SELECT 1
                FROM XXWC_BR_DISTRICT_ROLLUP_SS b
               WHERE b.dist_hier_id = a.dist_hier_id
                 AND b.start_date = l_pr_start_date
                 AND b.end_date = l_pr_end_date);
  
    l_err_callpoint := 'Before Br District Rollup SS Update';
  
    UPDATE XXWC_BR_DISTRICT_ROLLUP SET start_date = l_start_date;
  
    --WHERE end_date IS NULL;
  
    -- DELETE FROM xxwc.XXWC_BR_DISTRICT_ROLLUP
    --     WHERE end_date IS NOT NULL;
  
    l_err_callpoint := 'Before Br Salesreps SS Insert';
  
    INSERT INTO xxwc_sr_salesreps_ss
      (SALESREP_ID,
       SS_DATE,
       RESOURCE_ID,
       EEID,
       NTID,
       PRISM_SR_NUMBER,
       SR_NAME,
       SR_TYPE,
       AUTO_UPDATE,
       SR_TITLE,
       HOME_BR,
       HOME_MARKET_CODE,
       HOME_MARKET,
       HOME_DISTRICT_CODE,
       HOME_DISTRICT,
       HOME_REGION_CODE,
       HOME_REGION,
       START_DATE,
       END_DATE,
       LAST_UPDATE_DATE,
       LAST_UPDATE_USER)
      SELECT SALESREP_ID,
             SYSDATE,
             RESOURCE_ID,
             EEID,
             NTID,
             PRISM_SR_NUMBER,
             SR_NAME,
             SR_TYPE,
             AUTO_UPDATE,
             SR_TITLE,
             HOME_BR,
             HOME_MARKET_CODE,
             HOME_MARKET,
             HOME_DISTRICT_CODE,
             HOME_DISTRICT,
             HOME_REGION_CODE,
             HOME_REGION,
             L_PR_START_DATE,
             NVL(end_date, L_PR_END_DATE),
             LAST_UPDATE_DATE,
             LAST_UPDATE_USER
        FROM xxwc_sr_salesreps a
       WHERE NOT EXISTS (SELECT 1
                FROM xxwc_sr_salesreps_ss b
               WHERE a.salesrep_id = b.salesrep_id
                 AND b.start_date = l_pr_start_date
                 AND b.end_date = l_pr_end_date);
  
    l_err_callpoint := 'Before Br Salesreps SS Update';
  
    UPDATE xxwc_sr_salesreps SET start_date = l_start_date;
  
    --WHERE end_date IS NULL;
  
    --      DELETE FROM xxwc.xxwc_sr_salesreps
    --          WHERE end_date IS NOT NULL;
  
    l_err_callpoint := 'Before Salesrep Hierarchy SS Insert';
  
    INSERT INTO XXWC_SR_SALESREP_HIERARCHY_SS
      (SR_HIER_ID,
       SS_DATE,
       SR_ID,
       MGT_LVL,
       MGT_DESC,
       MGR_EEID,
       MGR_NAME,
       START_DATE,
       END_DATE,
       LAST_UPDATE_DATE,
       LAST_UPDATE_USER)
      SELECT SR_HIER_ID,
             SYSDATE,
             SR_ID,
             MGT_LVL,
             MGT_DESC,
             MGR_EEID,
             MGR_NAME,
             L_PR_START_DATE,
             NVL(end_date, L_PR_END_DATE),
             LAST_UPDATE_DATE,
             LAST_UPDATE_USER
        FROM XXWC_SR_SALESREP_HIERARCHY a
       WHERE NOT EXISTS (SELECT 1
                FROM XXWC_SR_SALESREP_HIERARCHY_SS b
               WHERE a.sr_hier_id = b.sr_hier_id
                 AND b.start_date = l_pr_start_date
                 AND b.end_date = l_pr_end_date);
  
    l_err_callpoint := 'Before Salesrep Hierarchy SS Update';
  
    UPDATE XXWC_SR_SALESREP_HIERARCHY SET start_date = l_start_date;
    --      WHERE end_date IS NULL;
  
    --   DELETE FROM xxwc.XXWC_SR_SALESREP_HIERARCHY
    --       WHERE end_date IS NOT NULL;
  EXCEPTION
    WHEN OTHERS THEN
      l_error_code    := SQLCODE;
      l_error_message := SUBSTR(SQLERRM, 1, 200);
      XXCUS_error_pkg.XXCUS_error_main_api(p_called_from       => l_err_callfrom,
                                           p_calling           => l_err_callpoint,
                                           p_request_id        => -1,
                                           p_ora_error_msg     => SQLERRM,
                                           p_error_desc        => l_error_message,
                                           p_distribution_list => g_dflt_email,
                                           p_module            => 'APPS');
  END update_snapshot;

  PROCEDURE sync_salesreps(errbuf OUT VARCHAR2, retcode OUT VARCHAR2) IS
    /*************************************************************************
    *   Procedure : sync_salesreps
    *
    *   PURPOSE:   This procedure syncs the custom SR table with updates in the standard jtf table.
    *  Author: Shankar Hariharan
    * Creation_date 04/02/2014
    * Last update Date 04/02/2014
    *   Parameter:
    *          OUT  errbuf
    *          OUT retcode
    * Version   Date         Name             TMS
    * 1.0      04/02/2014  Shankar Hariharan  TMS 20131021-00499      
    * 1.1      11/11/2014  Shankar Hariharan  TMS 20141110-00027 
    * 1.2      04/15/2015  Pahwa Nancy        ESMS Service-286819
    * 1.3      05/18/2015  Pahwa Nancy        ESMS Service-286821
    * ************************************************************************/
    l_sr_name       VARCHAR2(100);
    l_sr_type       VARCHAR2(100);
    l_end_date      DATE;
    l_active_status VARCHAR2(1);
    l_job_title     VARCHAR2(100);
    l_err_callfrom  VARCHAR2(50);
    l_err_callpoint VARCHAR2(50);
    l_error_code    NUMBER;
    l_error_message VARCHAR2(240);
    l_ntid          VARCHAR2(240);
    l_eeid          NUMBER; --1.3v
   

    CURSOR cur_sr IS
      --1.3 V Start
      select srp.salesrep_id,
             srp.resource_id,
             per.EMPLOYEE_NUMBER source_number,
             res.USER_NAME,
             res.RESOURCE_NAME,
             res.category,
             srp.status,
             res.start_date_active,
             res.end_date_active,
             (SELECT job_descr
                FROM xxcus.xxcushr_ps_emp_all_tbl
               WHERE employee_number = per.EMPLOYEE_NUMBER
                 AND ROWNUM = 1) source_job_title
        from apps.jtf_rs_defresources_srp_v srp
       inner join JTF_RS_DEFRESOURCES_V res
          on res.RESOURCE_ID = srp.RESOURCE_ID
       inner join PER_ALL_PEOPLE_F per
          on per.PERSON_ID = srp.PERSON_ID
         and current_date > per.EFFECTIVE_START_DATE
         and current_date < per.EFFECTIVE_END_DATE
       where salesrep_id <> -3
         AND org_id = xxwc_ascp_scwb_pkg.get_wc_org_id
      union --1.3v end
      SELECT a.salesrep_id,
             a.resource_id,
             b.source_number,
             b.user_name,
             b.resource_name,
             b.category,
             a.status,
             a.start_date_active,
             a.end_date_active,
             (SELECT job_descr
                FROM xxcus.xxcushr_ps_emp_all_tbl
               WHERE employee_number = b.source_number
                 AND ROWNUM = 1) source_job_title
        FROM JTF_RS_SALESREPS a, JTF_RS_DEFRESOURCES_V b
       WHERE a.resource_id = b.resource_id
         AND a.salesrep_id <> -3 --AND b.category = 'EMPLOYEE'
         AND org_id = xxwc_ascp_scwb_pkg.get_wc_org_id
         AND b.category = 'OTHER'; --1.3v additional where clause
  BEGIN
    l_err_callfrom := 'XXWC_AR_SALESREPS_PKG.SYNC_SALESREPS';
    FOR cur_sr_rec IN cur_sr LOOP
      l_err_callpoint := 'Before Salesrep Select for SR ' ||
                         cur_sr_rec.salesrep_id;
      BEGIN
        SELECT sr_name, end_date, active_status, sr_title, ntid, eeid --1.3v
          INTO l_sr_name,
               l_end_date,
               l_active_status,
               l_job_title,
               l_ntid,
               l_eeid --1.3 v
          FROM xxwc_sr_salesreps
         WHERE salesrep_id = cur_sr_rec.salesrep_id;
      
        IF cur_sr_rec.category = 'OTHER' and
           cur_sr_rec.source_number is not null then
          l_sr_type   := 'House';
          l_job_title := 'House';
          -- Version # 1.2 > Start
          /*  ELSE
          l_sr_type :=cur_sr_rec.category;
          l_job_title := cur_sr_rec.source_job_title;*/
          -- Version # 1.2 > end
        END IF;
      
        IF l_sr_name <> cur_sr_rec.resource_name OR
           NVL(l_end_date, TO_CHAR(SYSDATE, 'DD-MON-YYYY')) <>
           NVL(cur_sr_rec.end_date_active, TO_CHAR(SYSDATE, 'DD-MON-YYYY')) OR
           l_active_status <> cur_sr_rec.status OR
           NVL(l_job_title, 'X') <>
           NVL(cur_sr_rec.source_job_title, NVL(l_job_title, 'X')) OR
           NVL(l_ntid, '.') <> nvl(cur_sr_rec.user_name, '.') OR
           l_eeid <> cur_sr_rec.source_number THEN
          --1.3v
          l_err_callpoint := 'Before Salesrep Update for SR ' ||
                             cur_sr_rec.salesrep_id;
          UPDATE xxwc_sr_salesreps
             SET sr_name       = cur_sr_rec.resource_name,
                 end_date      = l_end_date,
                 active_status = cur_sr_rec.status,
                 sr_title      = cur_sr_rec.source_job_title, -- Version # 1.2 
                 -- Version # 1.2 > Start /*sr_title = l_job_title, --cur_sr_rec.source_job_title,*/
                 -- Version # 1.2 > end
                 last_update_date = SYSDATE,
                 last_update_user = Fnd_global.user_name,
                 ntid             = cur_sr_rec.user_name,
                 eeid             = cur_sr_rec.source_number --1.3v
           WHERE salesrep_id = cur_sr_rec.salesrep_id;
        END IF;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          l_err_callpoint := 'Before Salesrep Insert for SR ' ||
                             cur_sr_rec.salesrep_id;
          BEGIN
          
            IF cur_sr_rec.category = 'OTHER' and
               cur_sr_rec.source_number is not null then
              l_sr_type   := 'House';
              l_job_title := 'House';
            ELSE
              l_sr_type   := cur_sr_rec.category;
              l_job_title := cur_sr_rec.source_job_title;
            END IF;
            INSERT INTO xxwc_sr_salesreps
              (salesrep_id,
               resource_id,
               eeid,
               ntid,
               sr_name,
               sr_type,
               SR_TITLE,
               start_date,
               end_date,
               AUTO_UPDATE,
               active_status,
               last_update_date,
               last_update_user)
            VALUES
              (cur_sr_rec.salesrep_id,
               cur_sr_rec.resource_id,
               cur_sr_rec.source_number,
               cur_sr_rec.user_name,
               cur_sr_rec.resource_name,
               l_sr_type, --cur_sr_rec.category,
               l_job_title, --cur_sr_rec.source_job_title,
               cur_sr_rec.start_date_active,
               cur_sr_rec.end_date_active,
               'Y',
               cur_sr_rec.status,
               SYSDATE,
               'AUTO');
          EXCEPTION
            WHEN OTHERS THEN
              NULL;
          END;
      END;
    END LOOP;
  EXCEPTION
    WHEN OTHERS THEN
      l_error_code    := SQLCODE;
      l_error_message := SUBSTR(SQLERRM, 1, 200);
      XXCUS_error_pkg.XXCUS_error_main_api(p_called_from       => l_err_callfrom,
                                           p_calling           => l_err_callpoint,
                                           p_request_id        => -1,
                                           p_ora_error_msg     => SQLERRM,
                                           p_error_desc        => l_error_message,
                                           p_distribution_list => g_dflt_email,
                                           p_module            => 'APPS');
  END sync_salesreps;
END xxwc_ar_salesreps_pkg;
/