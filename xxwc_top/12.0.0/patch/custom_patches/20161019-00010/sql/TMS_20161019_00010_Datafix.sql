REM $Header: ontd0018.sql 120.0.12010000.2 2013/01/31 07:25:56 srpasuma noship $
REM +=======================================================================+
REM |    Copyright (c) 2003 Oracle Corporation, Redwood Shores, CA, USA     |
REM |                         All rights reserved.                          |
REM +=======================================================================+
REM | FILENAME                                                              |
REM |     ontd0018.sql                                                      |
REM |                                                                       |
REM | DESCRIPTION                                                           |
REM |     This script  generates a list of  delivered RMA lines stuck at    |
REM |     Wait for Receiving activity.                                      |
REM |                                                                       |
REM |     The script also takes  line id as input and  progresses Return    |
REM |     Lines affected by the problem.                                    |
REM |                                                                       |
REM |     Inputs : Generate Report?(Y/N) -- Y is Default                    |
REM |              Line Id (Required)                                       |
REM |                                                                       |
REM |     Output : Report is printed to an O/S file named ontd0018.lst      |
REM |              Messages for each Line Id processed by the script are    |
REM |              put in the OM debug file.                                |
REM |                                                                       |
REM | NOTE                                                                  |
REM |     This script should be tested in TEST Instance just on one line    |
REM |     first. If results  are  satisfactory in TEST, then only use in    |
REM |     PRODUCTION Instance.                                              |
REM |                                                                       |
REM | HISTORY                                                               |
REM |     06-MAY-2003              Tarun Bharti                 Created     |
REM |     31-JAN-2013              Srinivas Pasumarthi          Updated     |
REM +=======================================================================+

REM dbdrv:none
SET VERIFY OFF;
WHENEVER OSERROR EXIT FAILURE ROLLBACK;
WHENEVER SQLERROR EXIT FAILURE ROLLBACK;

clear buffer;
set serveroutput on size 200000
set ver off
set feed off
set pagesize 1000
set underline '='

--Prompt
--accept l_id num prompt 'Enter Line_Id of RMA line to process : '
--Prompt

spool 69638324.txt 

declare

  l_line_id       number := 69638324;
  l_ordered_qty   number;
  l_received_qty  rcv_transactions.quantity%type;

  cursor line_info
  is
  select line_id
        ,ordered_quantity
        ,ool.org_id
    ,rcv.quantity received_quantity --to pass in oe_rma_receiving.push_receiving_info in place of ordered qty
                                    --if the line is partially shipped and not split due to corruption
  from  oe_order_lines_all ool
       ,mtl_material_transactions mmt
       ,rcv_transactions rcv
  where ool.line_id                  = l_line_id
  and   ool.flow_status_code         = 'AWAITING_RETURN'
  and   mmt.trx_source_line_id = ool.line_id
  and   mmt.transaction_type_id= 15
  and   rcv.oe_order_line_id   = ool.line_id
  and   mmt.rcv_transaction_id = rcv.transaction_id
  and   rcv.transaction_type = 'DELIVER'
  for update nowait;

  l_user_id       number;
  l_resp_id       number;
  l_appl_id       number;
  x_return_status varchar2(10);
  x_msg_count     number;
  x_msg_data      varchar2(2000);
  l_org_id        NUMBER;

begin

  if nvl(l_line_id, 0) > 0 then
    open line_info;
    fetch line_info into l_line_id, l_ordered_qty,l_org_id, l_received_qty;
    if line_info%notfound then
      close line_info;
      dbms_output.put_line('Error: Invalid Line Id, Re-enter.');
      return;
    end if;
    close line_info;
  else
    return;
  end if;

  Begin
    select number_value
    into   l_user_id
    from   wf_item_attribute_values
    where  item_type = 'OEOL'
    and    item_key  = l_line_id
    and    name      = 'USER_ID';

    select number_value
    into   l_resp_id
    from   wf_item_attribute_values
    where  item_type = 'OEOL'
    and    item_key  = l_line_id
    and    name      = 'RESPONSIBILITY_ID';

    select number_value
    into   l_appl_id
    from   wf_item_attribute_values
    where  item_type = 'OEOL'
    and    item_key  = l_line_id
    and    name      = 'APPLICATION_ID';

    Exception
      When No_Data_Found Then
        dbms_output.put_line('Error: Line flow does not exist.');
        return;
  End;

  mo_global.init('ONT');
  mo_global.set_policy_context('S',l_org_id);
  fnd_global.apps_initialize(l_user_id, l_resp_id, l_appl_id);

  update oe_order_lines
  set    fulfilled_quantity = null
  ,      shipped_quantity   = null
  ,      last_updated_by    = -99999999
  ,      last_update_date   = sysdate
  where  line_id            = l_line_id;

  begin
    oe_rma_receiving.push_receiving_info( l_line_id
                                        , l_received_qty --removed l_ordered_qty to consider partially shipped qty if the line is not split already
                                        , 'NO PARENT'
                                        , 'RECEIVE'
                                        , 'N'
                                        ,  x_return_status
                                        ,  x_msg_count
                                        ,  x_msg_data
                                        );

    if x_return_status = 'S' then

      oe_rma_receiving.push_receiving_info( l_line_id
                                          , l_received_qty --removed l_ordered_qty to consider partially shipped qty if the line is not split already
                                          , 'RECEIVE'
                                          , 'DELIVER'
                                          , 'N'
                                          ,  x_return_status
                                          ,  x_msg_count
                                          ,  x_msg_data
                                          );
    end if;

    oe_debug_pub.add('no. of OE messages :'||x_msg_count,1);
    dbms_output.put_line('no. of OE messages :'||x_msg_count);

    for k in 1 .. x_msg_count loop

      x_msg_data := oe_msg_pub.get( p_msg_index => k,
                                    p_encoded => 'F');

      oe_debug_pub.add(substr(x_msg_data,1,255));
      oe_debug_pub.add(substr(x_msg_data,255,length(x_msg_data)));
      dbms_output.put_line('Error msg: '||substr(x_msg_data,1,200));

    end loop;

    fnd_msg_pub.count_and_get( p_encoded    => 'F'
                             , p_count      => x_msg_count
                            , p_data        => x_msg_data);

    oe_debug_pub.add('no. of FND messages :'||x_msg_count,1);
    dbms_output.put_line('no. of FND messages :'||x_msg_count);

    for k in 1 .. x_msg_count loop

      x_msg_data := fnd_msg_pub.get( p_msg_index => k,
                                     p_encoded => 'F');

      dbms_output.put_line('Error msg: '||substr(x_msg_data,1,200));
      oe_debug_pub.add(substr(x_msg_data,1,255));

    end loop;

    if x_return_status <> 'S' then
      oe_debug_pub.add('Error occurred, rolling back changes.',1);
      dbms_output.put_line('Error occurred, please fix the errors and retry.');
      rollback;
    else
      commit;
    end if;
  end;

  dbms_output.put_line('For details, see OM Debug File: '||OE_DEBUG_PUB.G_DIR||'/'||OE_DEBUG_PUB.G_FILE);

end;
/

spool off
