CREATE OR REPLACE FORCE VIEW XXEIS.EIS_XXWC_INV_UNINVODR_DTL_V (PERIOD_NAME, LOCATION, SEGMENT_NUMBER, CUSTOMER_NUMBER, CUSTOMER_NAME, PART_NUMBER, DESCRIPTION, LAST_INVOICE_NUM, LAST_ORDER_NUM, SALESREP_ID, ORDER_TYPE, ORDERED_QUANTITY, NEW_SELLING_PRICE, LAST_BILLED_DATE, TOTAL_DAYS, UNBILLED_RENTAL_DAYS, UNBILLED_RENTAL_AMOUNT, CAT_CLASS, CATCLASS_DESC)
  /**************************************************************************************************************
  $Header XXEIS.EIS_XXWC_INV_UNINVODR_DTL_V.vw $
  Module Name: Inventory
  PURPOSE: View for EIS Report Unbilled Rental Revenue
  TMS Task Id : NA
  REVISIONS:
  Ver        Date        Author                Description
  ---------  ----------  ------------------    ----------------
  1.0        NA          NA                    Initial Version
  1.1       21-Aug-2014  Mahesh                TMS#20140815-00178
  1.2       28-Sep-2015  Mahender Reddy        TMS#20150924-00151
  **************************************************************************************************************/
AS
  SELECT gps.period_name ,
    ship_from_org.organization_code location ,
    gcc.segment2 segment_number ,
    cust_acct.account_number customer_number ,
    NVL (cust_acct.account_name, party.party_name) customer_name ,
    msi.segment1 part_number ,
    msi.description description ,
    xxeis.eis_rs_xxwc_com_util_pkg.get_last_invoice_num (ol.inventory_item_id ,ol.sold_to_org_id ,ol.header_id) last_invoice_num ,
    oh.order_number last_order_num , --XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_LAST_ORDER_NUM(OL.INVENTORY_ITEM_ID,OL.SOLD_TO_ORG_ID) LAST_ORDER_NUM,
    jsr.name salesrep_id ,
    oh.attribute1 order_type
    --,ol1.shipped_quantity ordered_quantity                   -- Commented by Mahesh 21-Aug-2014, TMS#20140815-00178
    ,
    ol.ordered_quantity ordered_quantity -- Added by Mahesh 21-Aug-2014, TMS#20140815-00178
    --,ol1.unit_list_price new_selling_price                   -- Commented by Mahesh 21-Aug-2014, TMS#20140815-00178
    ,
    NVL(ol1.attribute4,ol1.unit_list_price) new_selling_price -- Added by Mahesh 21-Aug-2014, TMS#20140815-00178
    ,
    xxeis.eis_rs_xxwc_com_util_pkg.get_last_billed_date (ol.inventory_item_id ,ol.sold_to_org_id ,ol.header_id) last_billed_date ,
    ( ( TRUNC (gps.end_date) - TRUNC (NVL (TO_DATE (ol1.attribute12, 'YYYY/MM/DD HH24:MI:SS'), ol1.actual_shipment_date))) + 1) total_days ,
    xxeis.eis_rs_xxwc_com_util_pkg.get_rental_long_bill_days ( NVL (TO_DATE (ol1.attribute12, 'YYYY/MM/DD HH24:MI:SS'), ol1.actual_shipment_date) ,gps.end_date) unbilled_rental_days ,
    /*CASE
    WHEN ol.attribute4 IS NULL
    THEN
    (  (ol1.shipped_quantity)
    * xxeis.eis_rs_xxwc_com_util_pkg.get_rental_long_bill_days (
    NVL (TO_DATE (ol1.attribute12, 'YYYY/MM/DD HH24:MI:SS'), ol1.actual_shipment_date)
    ,gps.end_date)
    * (ol1.unit_list_price / 28))
    ELSE
    (  ol1.shipped_quantity
    * (ol.attribute4 / 28)
    * xxeis.eis_rs_xxwc_com_util_pkg.get_rental_long_bill_days (
    NVL (TO_DATE (ol1.attribute12, 'YYYY/MM/DD HH24:MI:SS'), ol1.actual_shipment_date)
    ,gps.end_date))
    END*/
    -- Commented by Mahesh 21-Aug-2014, TMS#20140815-00178
    ((ol.ordered_quantity*NVL(ol1.attribute4,ol1.unit_list_price))/28)* xxeis.eis_rs_xxwc_com_util_pkg.get_rental_long_bill_days ( NVL (TO_DATE (ol1.attribute12, 'YYYY/MM/DD HH24:MI:SS'), ol1.actual_shipment_date) ,gps.end_date) -- Added by Mahesh 21-Aug-2014, TMS#20140815-00178
    UNBILLED_RENTAL_AMOUNT ,
    MCB.SEGMENT2 CAT_CLASS -- Added for Ver 1.2
    ,
    MCB.DESCRIPTION CATCLASS_DESC -- Added for Ver 1.2
    --descr#flexfield#start
    --descr#flexfield#end
    --gl#accountff#start
    --gl#accountff#end
  FROM mtl_parameters ship_from_org ,
    hz_parties party ,
    hz_cust_accounts cust_acct ,
    oe_order_headers oh ,
    oe_order_lines ol ,
    oe_transaction_types_vl otl ,
    oe_order_lines ol1 ,
    hr_operating_units hou ,
    mtl_system_items_kfv msi ,
    gl_code_combinations_kfv gcc ,
    gl_period_statuses gps , --    OE_ORDER_TYPES_V OT,
    /*
    QP_LIST_HEADERS PH,*/
    ra_salesreps jsr ,
    (SELECT MCB1.SEGMENT2,
      MCB1.DESCRIPTION,
      MIC.INVENTORY_ITEM_ID,
      mic.organization_id
    FROM MTL_ITEM_CATEGORIES MIC,
      MTL_CATEGORIES MCB1
    WHERE 1              =1
    AND MCB1.CATEGORY_ID = MIC.CATEGORY_ID
    AND MCB1.SEGMENT2   IS NOT NULL
    ) MCB -- Added for Ver 1.2
    -- QP_LIST_LINES PL
    --          QP_LIST_LINES_V QLLV
  WHERE 1                             = 1
  AND ol.sold_to_org_id               = cust_acct.cust_account_id
  AND cust_acct.party_id              = party.party_id
  AND ol.ship_from_org_id             = ship_from_org.organization_id
  AND ol.header_id                    = oh.header_id
  AND ol.line_type_id                 = otl.transaction_type_id
  AND ol.org_id                       = otl.org_id
  AND ol.link_to_line_id              = ol1.line_id
  AND ol.flow_status_code             = 'AWAITING_RETURN'
  AND otl.name                        = 'RETURN WITH RECEIPT'
  AND oh.attribute1                   = 'Long Term'
  AND oh.flow_status_code            <> 'CLOSED'
  AND NVL (ol1.attribute2, 'NOT IN') <> '28 Day ReRental Billing'
    --  and OH.ORDER_TYPE_ID = OT.ORDER_TYPE_ID
    --  and ot.name ='WC LONG TERM RENTAL'
  AND ol.inventory_item_id  = msi.inventory_item_id
  AND msi.organization_id   = ol.ship_from_org_id
  AND oh.org_id             = hou.organization_id
  AND gcc.code_combination_id = ship_from_org.cost_of_sales_account
  AND gps.application_id      = 101
  AND gps.set_of_books_id     = hou.set_of_books_id
  AND gps.end_date            > TRUNC (NVL (TO_DATE (ol1.attribute12, 'YYYY/MM/DD HH24:MI:SS'), ol1.actual_shipment_date))
  AND ( TRUNC (gps.end_date) - TRUNC (NVL (TO_DATE (ol1.attribute12, 'YYYY/MM/DD HH24:MI:SS'), ol1.actual_shipment_date))) > 28
  AND jsr.salesrep_id(+)      = oh.salesrep_id
  AND jsr.org_id(+)           = oh.org_id
  AND msi.organization_id     = MCB.organization_id   -- Added for Ver 1.2
  AND MSI.INVENTORY_ITEM_ID   = MCB.INVENTORY_ITEM_ID -- Added for Ver 1.2
UNION ALL
  SELECT gps.period_name ,
    ship_from_org.organization_code location ,
    gcc.segment2 segment_number ,
    cust_acct.account_number customer_number ,
    NVL (cust_acct.account_name, party.party_name) customer_name ,
    msi.segment1 part_number ,
    msi.description description ,
    xxeis.eis_rs_xxwc_com_util_pkg.get_last_invoice_num (ol.inventory_item_id ,ol.sold_to_org_id ,ol.header_id) last_invoice_num ,
    oh.order_number last_order_num , --XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_LAST_ORDER_NUM(OL.INVENTORY_ITEM_ID,OL.SOLD_TO_ORG_ID) LAST_ORDER_NUM,
    jsr.name salesrep_id ,
    oh.attribute1 order_type , -- SUM(OL.ORDERED_QUANTITY) ORDERED_QUANTITY,
    --ol1.shipped_quantity ordered_quantity                   -- Commented by Mahesh 21-Aug-2014, TMS#20140815-00178
    ol.ordered_quantity ordered_quantity -- Added by Mahesh 21-Aug-2014, TMS#20140815-00178
    --,ol1.unit_list_price new_selling_price                   -- Commented by Mahesh 21-Aug-2014, TMS#20140815-00178
    ,
    NVL(ol1.attribute4,ol1.unit_list_price) new_selling_price -- Added by Mahesh 21-Aug-2014, TMS#20140815-00178
    ,                                                         --xxeis.eis_rs_xxwc_com_util_pkg.get_new_selling_price(ol.inventory_item_id,ol.ship_from_org_id,ol.sold_to_org_id) new_selling_price,
    xxeis.eis_rs_xxwc_com_util_pkg.get_last_billed_date (ol.inventory_item_id ,ol.sold_to_org_id ,ol.header_id) last_billed_date ,
    ( (TRUNC (gps.end_date) - TRUNC (ol1.actual_shipment_date)) + 1) total_days ,
    (xxeis.eis_rs_xxwc_com_util_pkg.get_rental_bill_days (ol1.actual_shipment_date ,gps.start_date ,gps.end_date)) unbilled_rental_days ,
    /*( ol1.shipped_quantity
    * xxeis.eis_rs_xxwc_com_util_pkg.get_accrued_amt (
    xxeis.eis_rs_xxwc_com_util_pkg.get_rental_bill_days (ol1.actual_shipment_date
    ,gps.start_date
    ,gps.end_date)
    ,ol1.attribute4
    ,ol1.unit_list_price))*/
    -- Commented by Mahesh 21-Aug-2014, TMS#20140815-00178
    (ol.ordered_quantity*NVL(ol1.attribute4,ol1.unit_list_price))/28* (xxeis.eis_rs_xxwc_com_util_pkg.get_rental_bill_days (ol1.actual_shipment_date ,gps.start_date ,gps.end_date)) UNBILLED_RENTAL_AMOUNT -- Added by Mahesh 21-Aug-2014, TMS#20140815-00178
    ,
    MCB.SEGMENT2 CAT_CLASS -- Added for Ver 1.2
    ,
    MCB.DESCRIPTION CATCLASS_DESC -- Added for Ver 1.2
    --descr#flexfield#start
    --descr#flexfield#end
    --gl#accountff#start
    --gl#accountff#end
  FROM mtl_parameters ship_from_org ,
    hz_parties party ,
    hz_cust_accounts cust_acct ,
    oe_order_headers oh ,
    oe_order_lines ol ,
    oe_transaction_types_vl otl ,
    oe_order_lines ol1 ,
    hr_operating_units hou ,
    mtl_system_items_kfv msi ,
    gl_code_combinations_kfv gcc ,
    gl_period_statuses gps , --    OE_ORDER_TYPES_V OT,
    -- QP_LIST_HEADERS PH,
    ra_salesreps jsr
    --   QP_LIST_LINES PL
    ,
    (SELECT MCB1.SEGMENT2,
      MCB1.DESCRIPTION,
      MIC.INVENTORY_ITEM_ID,
      mic.organization_id
    FROM MTL_ITEM_CATEGORIES MIC,
      MTL_CATEGORIES MCB1
    WHERE 1              =1
    AND MCB1.CATEGORY_ID = MIC.CATEGORY_ID
    AND MCB1.SEGMENT2   IS NOT NULL
    ) MCB -- Added for Ver 1.2
  WHERE 1                  = 1
  AND ol.sold_to_org_id    = cust_acct.cust_account_id
  AND cust_acct.party_id   = party.party_id
  AND ol.ship_from_org_id  = ship_from_org.organization_id
  AND ol.header_id         = oh.header_id
  AND ol.line_type_id      = otl.transaction_type_id
  AND ol.org_id            = otl.org_id
  AND ol.link_to_line_id   = ol1.line_id
  AND ol.flow_status_code  = 'AWAITING_RETURN'
  AND otl.name             = 'RETURN WITH RECEIPT'
  AND oh.attribute1        = 'Short Term'
  AND oh.flow_status_code <> 'CLOSED'
    --  and OH.ORDER_TYPE_ID = OT.ORDER_TYPE_ID
    --  and ot.name ='WC SHORT TERM RENTAL'
  AND ol.inventory_item_id    = msi.inventory_item_id(+)
  AND msi.organization_id     = ol.ship_from_org_id
  AND oh.org_id               = hou.organization_id
  AND gcc.code_combination_id = ship_from_org.cost_of_sales_account
  AND gps.application_id      = 101
  AND gps.set_of_books_id     = hou.set_of_books_id
  AND gps.end_date            > TRUNC (ol1.actual_shipment_date)
  AND jsr.salesrep_id(+)      = oh.salesrep_id
  AND JSR.ORG_ID(+)           = OH.ORG_ID
  AND msi.organization_id     = MCB.organization_id   -- Added for Ver 1.2
  AND MSI.INVENTORY_ITEM_ID   = MCB.INVENTORY_ITEM_ID -- Added for Ver 1.2
/