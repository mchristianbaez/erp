/*************************************************************************
  $Header TMS_20160711-00230_Stuck_in_shipped_status.sql $
  Module Name: TMS#20160711-00230 - Order 21409241  -- Data fix Script 

  PURPOSE: Data fix to update the order status

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        10-OCT-2016 Pattabhi Avula         TMS#20160711-00230 

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 100000;
SET VERIFY OFF;
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20160711-00230    , Before Update');
   
UPDATE  apps.oe_order_lines_all
SET    INVOICE_INTERFACE_STATUS_CODE='NOT_ELIGIBLE'
      ,open_flag='N'
      ,flow_status_code='CLOSED'
WHERE line_id =25953968
AND    headeR_id=15542855;

 DBMS_OUTPUT.put_line (
         'TMS: 20160711-00230  Sales order lines updated (Expected:1): '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20160711-00230    , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20160711-00230 , Errors : ' || SQLERRM);
END;
/