/******************************************************************************
   NAME:       TMS_20180717_00004_DATA_FIX_APPROVAL_ISSUE_CSP.sql
   PURPOSE:    CSPs are not able to get approved imported through quote to CSP.
   
   REVISIONS:
   Ver        Date        Author               Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        17/07/2018  Niraj K Ranjan   TMS#20180717-00004   orders pending on all seven CSP
******************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20180717-00004   , Before Update');
UPDATE xxwc_om_contract_pricing_hdr
      SET AGREEMENT_STATUS = 'AWAITING_APPROVAL'
      WHERE AGREEMENT_ID IN (717825,717824,717817,715674,703384,684568);
   DBMS_OUTPUT.put_line ('TMS: 20180717-00004    , Total records update1: '|| SQL%ROWCOUNT);
UPDATE xxwc_om_contract_pricing_lines
	  SET line_status = 'AWAITING_APPROVAL'
	  WHERE agreement_id in (717825,717824,717817,715674,703384,684568)
      AND   line_status = 'DRAFT';
   DBMS_OUTPUT.put_line ('TMS: 20180717-00004    , Total records update2: '|| SQL%ROWCOUNT);
UPDATE xxwc_om_csp_notifications_tbl
      SET AGREEMENT_STATUS = 'APPROVED'
	  WHERE agreement_id = 703384
	  AND REVISION_NUMBER = 0;
   DBMS_OUTPUT.put_line ('TMS: 20180717-00004    , Total records update3: '|| SQL%ROWCOUNT);
UPDATE xxwc_om_csp_notifications_tbl
      SET AGREEMENT_STATUS = 'AWAITING_APPROVAL'
	  WHERE agreement_id = 703384
	  AND REVISION_NUMBER = 1;	  
   DBMS_OUTPUT.put_line ('TMS: 20180717-00004    , Total records update4: '|| SQL%ROWCOUNT);
UPDATE xxwc_om_csp_notifications_tbl
      SET AGREEMENT_STATUS = 'AWAITING_APPROVAL'
	  WHERE agreement_id = 684568
	  AND REVISION_NUMBER = 1;	  
   DBMS_OUTPUT.put_line ('TMS: 20180717-00004    , Total records update5: '|| SQL%ROWCOUNT);
UPDATE xxwc_om_csp_notifications_tbl
      SET AGREEMENT_STATUS = 'AWAITING_APPROVAL'
	  WHERE agreement_id in (717825,717824,717817,715674)
	  AND REVISION_NUMBER = 0;	
   DBMS_OUTPUT.put_line ('TMS: 20180717-00004    , Total records update6: '|| SQL%ROWCOUNT);	  
COMMIT;
   DBMS_OUTPUT.put_line ('TMS: 20180717-00004    , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20180717-00004 , Errors : ' || SQLERRM);
END;
/   	  