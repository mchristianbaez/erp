/*************************************************************************
Copyright (c) 2012 HD Supply
All rights reserved.
**************************************************************************
   $Header XXWC.XXWC_BR_CAGE_DETAILS_TBL$
  Module Name: XXWC.XXWC_BR_CAGE_DETAILS_TBL
  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
   1.0     30-Jun-2017        Pahwa Nancy   TMS# 20170607-00052 
**************************************************************************/
grant select, insert, update, delete on XXWC.XXWC_BR_CAGE_DETAILS_TBL to EA_APEX;
