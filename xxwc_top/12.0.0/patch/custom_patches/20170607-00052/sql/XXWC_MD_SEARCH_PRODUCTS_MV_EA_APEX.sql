/*************************************************************************
Copyright (c) 2012 HD Supply
All rights reserved.
**************************************************************************
   $Header XXWC_MD_SEARCH_PRODUCTS_MV$
  Module Name: XXWC_MD_SEARCH_PRODUCTS_MV
  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
   1.0     30-Jun-2017        Pahwa Nancy   TMS# 20170607-00052 
**************************************************************************/
  GRANT SELECT ON "APPS"."XXWC_MD_SEARCH_PRODUCTS_MV" TO "EA_APEX";
