  /*******************************************************************************
  Table: "XXWC"."XXWC_BR_CAGE_DETAILS_TBL" 
  Description: "XXWC"."XXWC_BR_CAGE_DETAILS_TBL" 
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     30-Jun-2017        Pahwa Nancy   TMS# 20170607-00052 
  ********************************************************************************/
-- Create table
create table XXWC.XXWC_BR_CAGE_DETAILS_TBL
(
  id             NUMBER not null,
  serial_number  VARCHAR2(50),
  check_in_date  DATE,
  check_out_date DATE,
  checked_in_by  VARCHAR2(150),
  order_number   VARCHAR2(150),
  checked_out_by VARCHAR2(150),
  partnumber     VARCHAR2(40),
  description    VARCHAR2(240),
  reason         VARCHAR2(250)
)
/