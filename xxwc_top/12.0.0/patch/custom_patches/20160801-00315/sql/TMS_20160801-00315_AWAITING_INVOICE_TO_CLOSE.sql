/*************************************************************************
  $Header TMS_20160801-00315_AWAITING_INVOICE_TO_CLOSE.sql $
  Module Name: TMS_20160801-00315  Data Fix script for 20160801-00315

  PURPOSE: Data fix script for 20160801-00315--No permanent fix in process (But patch available per oracle)

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        30-AUG-2016  Raghav Velichetti         TMS#20160801-00315 

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000;
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20160801-00315    , Before Update');

update apps.oe_order_lines_all
set FLOW_STATUS_CODE='CANCELLED', cancelled_flag='Y'
where line_id =38022055;


   DBMS_OUTPUT.put_line (
         'TMS: 20160801-00315  Sales order lines updated (Expected:1): '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20160801-00315    , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20160801-00315 , Errors : ' || SQLERRM);
END;
/