/*************************************************************************
 $Header TMS_20180914-00004_UPDATE_ORDER_CLOSE.sql $
 Module Name: TMS_20180914-00004_UPDATE_ORDER_CLOSE.sql

 PURPOSE: Update the issue order line to close

 REVISIONS:
 Ver        Date       Author           Description
 --------- ----------  ---------------  -------------------------
 1.0       09/14/2018  Pattabhi Avula   TMS#20180914-00004 -  Line Picked status 
                                        for  ISO 29832851 for item number 339PSP3448 in org112
 **************************************************************************/
  SET SERVEROUTPUT ON SIZE 1000000;
  DECLARE
  
  l_count        NUMBER:=0;
BEGIN
  DBMS_OUTPUT.put_line ('TMS: Datafix script    , Before Update');
  
-- Updating the headers table
 
UPDATE apps.oe_order_lines_all
   SET FLOW_STATUS_CODE='CLOSED'
      ,open_Flag='N'
 WHERE header_id = 77443248
   AND line_id = 133056089;
l_count:=SQL%ROWCOUNT;
DBMS_OUTPUT.put_line ('Records updated - ' || l_count);		   
COMMIT;

	  DBMS_OUTPUT.put_line ('TMS: 20180914-00004  , End Update');
EXCEPTION
 WHEN OTHERS THEN
 DBMS_OUTPUT.put_line ('TMS: 20180914-00004 , Errors : ' || SQLERRM);
END;
/