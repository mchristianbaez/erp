/*
 Ticket: ESMS 322411 / TMS 20161026-00104 
 Date: 10/24/2016
 MODULE: CASH MANAGEMENT
 AUTHOR: BALAGURU SESHADRI 
 Notes:  HDS TREASURY: PROCESS CURRENT DAY BANK BAI FILES
*/
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
 --
 n_loc number :=0;
 n_count number :=0;
 l_err_msg varchar2(2000) :=Null;
 b_go boolean;
 --
BEGIN --Main Processing...
   --
   b_go :=TRUE;
   --
   n_loc :=101;
   --
  if (b_go) then 
   --
   n_loc :=102;
   --
   begin
    --
    savepoint start1;
    --
	delete xxcus.xxcus_banks_bai_codes_mapping;
	--
            insert into xxcus.xxcus_banks_bai_codes_mapping
            (
            select
                     d.bank_account_id
                    ,d.bank_account_name        
                    ,d.bank_account_num
                    ,a.code bai_code
                    ,a.required_flag --R -Required, N - No, O -Optional
                    ,15837 created_by        
                    ,sysdate creation_date
                    ,15837 last_updated_by
                    ,sysdate last_update_date        
            from xxcus.xxcus_banks_bai_lookup a
                     ,apps.ce_bank_accounts  d
                     ,apps.ce_bank_branches_v e
            where 1 =1
                 and d.attribute_category='Cash Positioning'
                 and d.attribute2 ='yes'     
                 and e.bank_party_id =d.bank_id
                 and e.branch_party_id =d.bank_branch_id
                 and a.acct =d.bank_account_num          
            );
    --
    n_loc :=104;
    --
     dbms_output.put_line('@ n_loc ='||n_loc||', rows inserted into xxcus.xxcus_banks_bai_codes_mapping ='||sql%rowcount);
    --
    commit;
    --
   exception
    when others then
     --
     n_loc :=105;
     --
     dbms_output.put_line('@ n_loc ='||n_loc||', message ='||sqlerrm);
     --
     rollback to start1;
     --     
   end;
   --
  else
   --
   n_loc :=103;
   --
   dbms_output.put_line('@b_go =FALSE, n_loc ='||n_loc);
   --
  end if;  --  if (b_go) then 
 --
 commit;
 --
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('Outer Block..., Errors =' || SQLERRM);
END;
/