CREATE OR REPLACE PACKAGE APPS.xxcus_concur_pkg is 
  --
 /*
   Ticket#                                                            Date                                 Author                                                      Notes
   -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   TMS  20170407-00204 / ESMS 554216                        02/17/2017                     Balaguru Seshadri                               Concur process related routines   
 */
  --
    g_limit number :=2500;
    g_rpt_crt_btch_id number :=0;
    g_rpt_concur_batch_id number :=0;
    g_rpt_run_id number :=0;
  --
    procedure process_new_batch --need here declared because we need this from the concurrent program
    (
        retcode out varchar2
       ,errbuf out varchar2
       ,p_non_prod_email in varchar2
       ,p_push_to_sharepoint in varchar2
    )    
    ;
    --
    procedure set_sae_hdr_status (p_status in varchar2); --need here declared because we need this from the BI publisher report 
    --
    function beforeReport return boolean;
    --
    function afterreport return boolean;
    --
    procedure DELETE_REQUEST_SET_LOG;
    --
    procedure INSERT_REQUEST_SET_LOG (p_set_req_id IN NUMBER, p_wrapper_req_id IN NUMBER);
    --
      procedure submit_request_set
    (
        retcode out varchar2
       ,errbuf  out varchar2 
       ,p_non_prod_email in varchar2
       ,p_push_to_sharepoint in varchar2 
    );
    --
end xxcus_concur_pkg;
/