/*
TMS: 20171031-00425 - Account Realignment SE Region
*/
set serverout on
   DECLARE
      v_last_updated_by        NUMBER := 1296; --fnd_profile.VALUE ('USER_ID');
      v_last_update_date       DATE := SYSDATE;
      v_responsibility_id      NUMBER := 50992; --fnd_profile.VALUE ('RESP_ID');
      v_org_id                 NUMBER := 162; --fnd_profile.VALUE ('ORG_ID');
      v_last_update_login      NUMBER; -- := fnd_profile.VALUE ('LOGIN_ID');
      v_message                VARCHAR2 (564);
      v_records_count_before   NUMBER;
      v_records_count_after    NUMBER;
      v_error_count            NUMBER := 0;
      v_all_count              NUMBER := 0;
      v_status                 VARCHAR2 (1) := NULL;
      l_run_id                 NUMBER;
      v_new_sales_rep_id       NUMBER := 100000616;
      --v_active_sites_only      VARCHAR2(1) := 'N';
      
      v_cntr                   NUMBER := 0;
     
   BEGIN
   
      mo_global.set_policy_context('S',162);

      fnd_global.APPS_INITIALIZE(user_id=>1296, 
                                 resp_id=>50992, 
                                 resp_appl_id=>1);

      FOR r IN (SELECT hcsu.rowid                       site_rowid
                  FROM xxwc.xxwc_cust_acct_salesrep_upd stg
                     , apps.hz_cust_accounts_all        hca
                     , apps.hz_cust_acct_sites_all      hcas
                     , apps.hz_cust_site_uses_all       hcsu
                 WHERE hca.account_number             = stg.account_number
                   AND hcas.cust_account_id           = hca.cust_account_id
                   AND hcsu.cust_acct_site_id         = hcas.cust_acct_site_id
                   --AND hca.status                     = 'A'
                   --AND hcas.status                    = 'A'
                   --AND hcsu.status                    = 'A'
				   )
      LOOP
         xxwc_ar_customer_attr_update.xxwc_update_cust_site_uses (
            p_site_rowid          => r.site_rowid,
            p_salesrep_id         => v_new_sales_rep_id,
            p_freight_term        => NULL,
            p_last_updated_by     => v_last_updated_by,
            p_last_update_date    => v_last_update_date,
            p_last_update_login   => v_last_update_login,
            p_responsibility_id   => v_responsibility_id,
            p_org_id              => v_org_id);
            
         v_cntr := v_cntr + 1;
      END LOOP;

COMMIT;
dbms_output.put_line('Total Number of records - '||v_Cntr);

EXCEPTION
WHEN OTHERS THEN
   dbms_output.put_line('Error - '||SQLERRM);
END;
/