/*
 TMS: 20160602-00112 
 Date: 02/16/2016
 Scope: @GSC ESMS 318276 -- CC Unposted Report issue because of duplicate lines in ap credit card transactions.
 Notes:  Delete from table ap.ap_credit_card_trxns_all where trx_id in ('2720814','2720815')
               All related lines are backed up in an excel spreadsheet and attached to the ESMS ticket.
*/
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
 --
 b_go boolean :=Null;
 n_loc number :=Null;
 --
BEGIN --Main Processing...
   --
   b_go :=TRUE;
   --
   n_loc :=101;
   --
  if (b_go) then 
   --
   n_loc :=102;
   --
   begin
    --
     dbms_output.put_line('Before delete from table ap.ap_credit_card_trxns_all where trx_id in (2720814,2720815)');
     dbms_output.put_line(' ');
    delete ap.ap_credit_card_trxns_all where trx_id in (2720814,2720815);
     dbms_output.put_line('After delete from table ap.ap_credit_card_trxns_all where trx_id in (2720814,2720815)'); 
     dbms_output.put_line(' ');    
     dbms_output.put_line('Total rows deleted : '||sql%rowcount);
     dbms_output.put_line(' ');        
    --
    n_loc :=104;
    --
   exception
    when others then
     --
     n_loc :=105;
     --
     dbms_output.put_line('@ n_loc ='||n_loc||', message ='||sqlerrm);
     --   
   end;
   --
  end if;
   --
  Commit;
  --
EXCEPTION
   WHEN OTHERS THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20160602-00112, Errors =' || SQLERRM);
END;
/