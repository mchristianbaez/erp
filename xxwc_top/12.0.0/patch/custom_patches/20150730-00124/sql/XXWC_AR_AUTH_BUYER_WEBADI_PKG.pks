CREATE OR REPLACE PACKAGE APPS.XXWC_AR_AUTH_BUYER_WEBADI_PKG
/********************************************************************************
FILE NAME: APPS.XXWC_AR_AUTH_BUYER_WEBADI_PKG.pkg

PROGRAM TYPE: PL/SQL Package

PURPOSE: API to create contacts and auth buyers in Oracle.

HISTORY
===============================================================================
VERSION DATE          AUTHOR(S)              DESCRIPTION
------- -----------   --------------- -----------------------------------------
1.0     10/12/2015    Neha Saini    TMS#20150730-00124 Initial version
********************************************************************************/
AS
   PROCEDURE IMPORT (p_account_number   IN VARCHAR2,
                     p_role             IN VARCHAR2,
                     p_first_name       IN VARCHAR2,
                     p_last_name        IN VARCHAR2,
                     p_contact          IN VARCHAR2,
                     p_email            IN VARCHAR2);
END XXWC_AR_AUTH_BUYER_WEBADI_PKG;
/