CREATE OR REPLACE PACKAGE BODY APPS.XXWC_AR_AUTH_BUYER_WEBADI_PKG
/********************************************************************************
FILE NAME: APPS.XXWC_AR_AUTH_BUYER_WEBADI_PKG.pkg

PROGRAM TYPE: PL/SQL Package Body

PURPOSE: API to load contacts and auth buyer into Oracle.

HISTORY
===============================================================================
VERSION DATE          AUTHOR(S)                DESCRIPTION
------- -----------   --------------- -----------------------------------------
1.0     10/12/2015    Neha Saini   TMS#20150730-00124 Initial version
********************************************************************************/
AS


   PROCEDURE IMPORT (p_account_number   IN VARCHAR2,
                     p_role             IN VARCHAR2,
                     p_first_name       IN VARCHAR2,
                     p_last_name        IN VARCHAR2,
                     p_contact          IN VARCHAR2,
                     p_email            IN VARCHAR2)
   IS
      -- Contact variables
      l_create_person_rec              hz_party_v2pub.person_rec_type;
      l_contact_party_id               NUMBER;
      l_contact_party_number           VARCHAR2 (2000);
      l_contact_profile_id             NUMBER;
      -- contact org
      l_org_contact_rec                hz_party_contact_v2pub.org_contact_rec_type;
      l_org_contact_id                 NUMBER;
      l_party_rel_id                   NUMBER;
      l_rel_party_id                   NUMBER;
      l_rel_party_number               VARCHAR2 (2000);
      -- contact acct
      l_cr_cust_acc_role_rec           hz_cust_account_role_v2pub.cust_account_role_rec_type;
      l_cust_account_role_id           NUMBER;
      -- Role resp
      l_ROLE_RESPONSIBILITY_REC_TYPE   HZ_CUST_ACCOUNT_ROLE_V2PUB.role_responsibility_rec_type;
      l_resp_id                        NUMBER;
      l_profile_id                     NUMBER;

      --location variable
      l_location_rec                   hz_location_v2pub.location_rec_type;
      l_location_id                    hz_locations.location_id%TYPE;
      --site variables
      l_party_site_rec                 hz_party_site_v2pub.party_site_rec_type;
      l_party_site_id                  hz_party_sites.party_site_id%TYPE;
      l_party_site_no                  hz_party_sites.party_site_number%TYPE;
      --site account avriables
      l_cust_acct_site_rec             hz_cust_account_site_v2pub.cust_acct_site_rec_type;
      --site use variables
      l_cust_acct_site_use_rec         hz_cust_account_site_v2pub.cust_site_use_rec_type;
      l_customer_profile               hz_customer_profile_v2pub.customer_profile_rec_type;

      --contact variables
      l_contact_point_rec              hz_contact_point_v2pub.contact_point_rec_type;
      l_phone_rec                      hz_contact_point_v2pub.phone_rec_type;
      l_email_rec                      hz_contact_point_v2pub.email_rec_type;
      l_telex_rec                      hz_contact_point_v2pub.TELEX_REC_TYPE;
      l_web_rec                        hz_contact_point_v2pub.WEB_REC_TYPE;
      l_edi_rec                        hz_contact_point_v2pub.EDI_REC_TYPE;
      l_contact_point_id               NUMBER;
      l_object_version_number          NUMBER;
      l_party_id                       NUMBER;
      l_cust_account_id                NUMBER;
      l_api_name                       VARCHAR2 (30) := 'ONT_UI_ADD_CUSTOMER';

      l_status                         VARCHAR2 (1);
      l_msg_count                      NUMBER;
      l_msg_data                       VARCHAR2 (2000);
      l_error_message                  VARCHAR2 (5000);

      l_program_error                  EXCEPTION;
      l_count                          NUMBER := 0;
      l_msgname                        VARCHAR2 (30);
      l_msgapp                         VARCHAR2 (50);
      l_msgencoded                     VARCHAR2 (32100);
      l_msgencodedlen                  NUMBER (6);
      l_msgnameloc                     NUMBER (6);
      l_msgtextloc                     NUMBER (6);
   BEGIN
      MO_GLOBAL.SET_POLICY_CONTEXT ('S', 162);
      MO_GLOBAL.INIT ('AR');
      DBMS_OUTPUT.PUT_LINE('Started Import');
      l_count :=  0;
      IF p_account_number IS NOT NULL THEN
         --------------------------------------------------------
         -- Derive party id
         --------------------------------------------------------
         BEGIN
         
            SELECT party_id, cust_account_id
              INTO l_party_id, l_cust_account_id
              FROM hz_cust_accounts
             WHERE account_number = p_account_number;
             
         EXCEPTION
         
            WHEN OTHERS THEN
               l_error_message := 'Invalid Account Number- ' || SQLERRM;
               RAISE l_program_error;
               
         END;
         
         DBMS_OUTPUT.PUT_LINE('Party Id '||l_party_id || ' cust_account_id '|| l_cust_account_id);
         --------------------------------------------------------
         --Create person API mapping
         --------------------------------------------------------

         l_create_person_rec                   := NULL;
         l_contact_party_id                    := NULL;
         l_contact_party_number                := NULL;
         l_contact_profile_id                  := NULL;
         l_create_person_rec.person_first_name := p_first_name;
         l_create_person_rec.person_last_name  := p_last_name;
         l_create_person_rec.created_by_module := l_api_name;

         --------------------------------------------------------
         --Create person API
         --------------------------------------------------------

         hz_party_v2pub.create_person ('T',
                                       l_create_person_rec,
                                       l_contact_party_id,
                                       l_contact_party_number,
                                       l_contact_profile_id,
                                       l_status,
                                       l_msg_count,
                                       l_msg_data);


         IF l_msg_count >= 1
         THEN
            FOR I IN 1 .. l_msg_count
            LOOP
               l_error_message :=
                     l_error_message
                  || ' . '
                  || SUBSTR (FND_MSG_PUB.Get (p_encoded => FND_API.G_FALSE),
                             1,
                             255);
               dbms_output.put_line(I||'. '||SubStr(FND_MSG_PUB.Get(p_encoded => FND_API.G_FALSE ), 1, 255));
            END LOOP;
            l_error_message:= l_error_message || l_msg_data;
            RAISE l_program_error;
         END IF;

         DBMS_OUTPUT.PUT_LINE('contact_profile_id '||l_contact_profile_id );

         --====================================================================
         -- org contact
         --====================================================================
         l_org_contact_rec                                  := NULL;
         l_org_contact_rec.created_by_module                := l_api_name;
         l_org_contact_rec.party_rel_rec.subject_id         := l_contact_party_id;
         l_org_contact_rec.party_rel_rec.subject_type       := 'PERSON';
         l_org_contact_rec.party_rel_rec.subject_table_name := 'HZ_PARTIES';
         l_org_contact_rec.party_rel_rec.object_id          := l_party_id;
         l_org_contact_rec.party_rel_rec.object_type        := 'ORGANIZATION';
         l_org_contact_rec.party_rel_rec.object_table_name  := 'HZ_PARTIES';
         l_org_contact_rec.party_rel_rec.relationship_code  := 'CONTACT_OF';
         l_org_contact_rec.party_rel_rec.relationship_type  := 'CONTACT';
         l_org_contact_rec.party_rel_rec.start_date         := SYSDATE;

         hz_party_contact_v2pub.create_org_contact ('T',
                                                    l_org_contact_rec,
                                                    l_org_contact_id,
                                                    l_party_rel_id,
                                                    l_rel_party_id,
                                                    l_rel_party_number,
                                                    l_status,
                                                    l_msg_count,
                                                    l_msg_data);

         IF l_msg_count >= 1
         THEN
            FOR I IN 1 .. l_msg_count
            LOOP
               l_error_message :=
                     l_error_message
                  || ' . '
                  || SUBSTR (FND_MSG_PUB.Get (p_encoded => FND_API.G_FALSE),
                             1,
                             255);
               dbms_output.put_line('error. '||SubStr(FND_MSG_PUB.Get(p_encoded => FND_API.G_FALSE ), 1, 255));
            END LOOP;
            l_error_message:= l_error_message || l_msg_data;
            RAISE l_program_error;
         END IF;
         DBMS_OUTPUT.PUT_LINE('l_rel_party_id '||l_rel_party_id || ' l_rel_party_number '|| l_rel_party_number);
         ------------------------------------------------------------------------------------------------------------
         -- Create a Phone Contact Point using party_id
         ------------------------------------------------------------------------------------------------------------
         IF p_contact IS NOT NULL
         THEN
            -- Initializing the Mandatory API parameters
            l_contact_point_rec.contact_point_type        := 'PHONE';
            l_contact_point_rec.owner_table_name          := 'HZ_PARTIES';
            l_contact_point_rec.owner_table_id            := l_rel_party_id;
            l_contact_point_rec.primary_flag              := 'Y';
            l_contact_point_rec.contact_point_purpose     := 'BUSINESS';
            l_contact_point_rec.created_by_module         := l_api_name;
            l_phone_rec.phone_area_code                   := NULL;
            l_phone_rec.phone_country_code                := '1';
            l_phone_rec.phone_number                      := p_contact;
            l_phone_rec.phone_line_type                   := 'GEN';


            HZ_CONTACT_POINT_V2PUB.CREATE_CONTACT_POINT (
               p_init_msg_list       => FND_API.G_TRUE,
               p_contact_point_rec   => l_contact_point_rec,
               p_edi_rec             => l_edi_rec,
               p_email_rec           => l_email_rec,
               p_phone_rec           => l_phone_rec,
               p_telex_rec           => l_telex_rec,
               p_web_rec             => l_web_rec,
               x_contact_point_id    => l_contact_point_id,
               x_return_status       => l_status,
               x_msg_count           => l_msg_count,
               x_msg_data            => l_msg_data);


            IF l_msg_count >= 1
            THEN
               FOR I IN 1 .. l_msg_count
               LOOP
                  l_error_message :=
                        l_error_message
                     || ' . '
                     || SUBSTR (
                           FND_MSG_PUB.Get (p_encoded => FND_API.G_FALSE),
                           1,
                           255);
                  dbms_output.put_line(I||'error. '||SubStr(FND_MSG_PUB.Get(p_encoded => FND_API.G_FALSE ), 1, 255));
               END LOOP;
               l_error_message:= l_error_message || l_msg_data;
               RAISE l_program_error;
            END IF;
         END IF;

         DBMS_OUTPUT.PUT_LINE('creating contact point l_contact_point_id '||l_contact_point_id );
         ------------------------------------------------------------------------------------------------------------
         -- Create a Email Contact Point using party_id
         ------------------------------------------------------------------------------------------------------------
         IF p_email IS NOT NULL
         THEN
            -- Initializing the Mandatory API parameters
            l_contact_point_rec.contact_point_type    := 'EMAIL';
            l_contact_point_rec.owner_table_name      := 'HZ_PARTIES';
            l_contact_point_rec.owner_table_id        := l_rel_party_id;
            l_contact_point_rec.primary_flag          := 'Y';
            l_contact_point_rec.contact_point_purpose := 'BUSINESS';
            l_contact_point_rec.created_by_module     := 'BO_API';
            l_email_rec.email_format                  := 'MAILHTML';
            l_email_rec.email_address                 := p_email;

            HZ_CONTACT_POINT_V2PUB.CREATE_CONTACT_POINT (
               p_init_msg_list       => FND_API.G_TRUE,
               p_contact_point_rec   => l_contact_point_rec,
               p_edi_rec             => l_edi_rec,
               p_email_rec           => l_email_rec,
               p_phone_rec           => l_phone_rec,
               p_telex_rec           => l_telex_rec,
               p_web_rec             => l_web_rec,
               x_contact_point_id    => l_contact_point_id,
               x_return_status       => l_status,
               x_msg_count           => l_msg_count,
               x_msg_data            => l_msg_data);

            IF l_msg_count >= 1
            THEN
               FOR I IN 1 .. l_msg_count
               LOOP
                  l_error_message :=
                        l_error_message
                     || ' . '
                     || SUBSTR (
                           FND_MSG_PUB.Get (p_encoded => FND_API.G_FALSE),
                           1,
                           255);
                    dbms_output.put_line(I||'error. '||SubStr(FND_MSG_PUB.Get(p_encoded => FND_API.G_FALSE ), 1, 255));
               END LOOP;
               l_error_message:= l_error_message || l_msg_data;
               RAISE l_program_error;
            END IF;
         END IF;
         DBMS_OUTPUT.PUT_LINE(' crating contact point l_contact_point_id '||l_contact_point_id );
         --====================================================================
         -- create location for contact
         --====================================================================
         l_location_rec             := NULL;
         l_party_site_rec           := NULL;
         l_cust_acct_site_rec       := NULL;
         l_cust_acct_site_use_rec   := NULL;
         l_customer_profile         := NULL;
         l_location_id              := NULL;
         l_status                   := NULL;

         l_location_rec.country     := 'US';
         l_location_rec.postal_code := '32805';
         l_location_rec.address1    := '501 W CHURCH ST';

         l_location_rec.state       := 'FL';
         l_location_rec.city        := 'ORLANDO';
         l_location_rec.county      := 'ORANGE';
         l_location_rec.created_by_module := l_api_name;


         hz_location_v2pub.create_location (
            p_init_msg_list   => 'T',
            p_location_rec    => l_location_rec,
            x_location_id     => l_location_id,
            x_return_status   => l_status,
            x_msg_count       => l_msg_count,
            x_msg_data        => l_msg_data);


         IF l_msg_count >= 1
         THEN
            FOR I IN 1 .. l_msg_count
            LOOP
               l_error_message :=
                     l_error_message
                  || ' . '
                  || SUBSTR (FND_MSG_PUB.Get (p_encoded => FND_API.G_FALSE),
                             1,
                             255);
               dbms_output.put_line(I||'. '||SubStr(FND_MSG_PUB.Get(p_encoded => FND_API.G_FALSE ), 1, 255));
            END LOOP;
            l_error_message:= l_error_message || l_msg_data;
            RAISE l_program_error;
         END IF;

         --====================================================================
         -- Create Party Site
         --====================================================================

         l_party_site_id := NULL;
         l_party_site_no := NULL;
         l_profile_id    := NULL;
         l_party_site_rec.created_by_module := l_api_name;

         -- create a party site now
         l_party_site_rec.party_id    := l_rel_party_id;
         l_party_site_rec.location_id := l_location_id;
         l_party_site_rec.identifying_address_flag := 'Y';

         hz_party_site_v2pub.create_party_site (
            p_init_msg_list       => 'T',
            p_party_site_rec      => l_party_site_rec,
            x_party_site_id       => l_party_site_id,
            x_party_site_number   => l_party_site_no,
            x_return_status       => l_status,
            x_msg_count           => l_msg_count,
            x_msg_data            => l_msg_data);

         IF l_msg_count >= 1
         THEN
            FOR I IN 1 .. l_msg_count
            LOOP
               l_error_message :=
                     l_error_message
                  || ' . '
                  || SUBSTR (FND_MSG_PUB.Get (p_encoded => FND_API.G_FALSE),
                             1,
                             255);
               dbms_output.put_line(I||'. '||SubStr(FND_MSG_PUB.Get(p_encoded => FND_API.G_FALSE ), 1, 255));
            END LOOP;
            l_error_message:= l_error_message || l_msg_data;
            RAISE l_program_error;
         END IF;
         DBMS_OUTPUT.PUT_LINE(' l_party_site_id '||l_party_site_id || ' l_party_site_no '||l_party_site_no);
         --====================================================================
         -- create account role
         --====================================================================

         l_cr_cust_acc_role_rec                   := NULL;
         l_cr_cust_acc_role_rec.party_id          := l_rel_party_id;
         l_cr_cust_acc_role_rec.cust_account_id   := l_cust_account_id;
         l_cr_cust_acc_role_rec.primary_flag      := 'N';
         l_cr_cust_acc_role_rec.role_type         := 'CONTACT';
         l_cr_cust_acc_role_rec.created_by_module := l_api_name;

         hz_cust_account_role_v2pub.create_cust_account_role (
                                    p_init_msg_list         =>  'T',
                                    p_cust_account_role_rec => l_cr_cust_acc_role_rec,
                                    x_cust_account_role_id  => l_cust_account_role_id,
                                    x_return_status         => l_status,
                                    x_msg_count             => l_msg_count,
                                    x_msg_data              => l_msg_data);

         IF l_msg_count >= 1
         THEN
            FOR I IN 1 .. l_msg_count
            LOOP
               l_error_message :=
                     l_error_message
                  || ' . '
                  || SUBSTR (FND_MSG_PUB.Get (p_encoded => FND_API.G_FALSE),
                             1,
                             255);
               dbms_output.put_line(I||'. '||SubStr(FND_MSG_PUB.Get(p_encoded => FND_API.G_FALSE ), 1, 255));
            END LOOP;
            l_error_message:= l_error_message || l_msg_data;
            RAISE l_program_error;
         END IF;
        DBMS_OUTPUT.PUT_LINE(' l_cust_account_role_id ' ||l_cust_account_role_id); 
         --====================================================================
         -- Assign Authorized Buyer role
         --====================================================================
         IF p_role IS NOT NULL
         THEN
            L_ROLE_RESPONSIBILITY_REC_TYPE := NULL;
            L_ROLE_RESPONSIBILITY_REC_TYPE.cust_account_role_id := l_cust_account_role_id;

            CASE WHEN  p_role = 'Authorized Buyer' THEN
               L_ROLE_RESPONSIBILITY_REC_TYPE.responsibility_type :='AUTH_BUYER';
               L_ROLE_RESPONSIBILITY_REC_TYPE.primary_flag := 'Y';
               l_count := 1;
            WHEN p_role = 'General Contractor'  THEN
               L_ROLE_RESPONSIBILITY_REC_TYPE.responsibility_type := 'GENERAL';
            WHEN p_role = 'Bonding Company' THEN
               L_ROLE_RESPONSIBILITY_REC_TYPE.responsibility_type := 'BONDING';
            WHEN p_role = 'Accounts Payable'  THEN
               L_ROLE_RESPONSIBILITY_REC_TYPE.responsibility_type := 'ACC_PAY';
            WHEN p_role = 'Credit Contact'  THEN
               L_ROLE_RESPONSIBILITY_REC_TYPE.responsibility_type := 'CREDIT_CONTACT';
            WHEN p_role = 'Additional NTO Contact' THEN
               L_ROLE_RESPONSIBILITY_REC_TYPE.responsibility_type := 'ADD_NTO';
            WHEN p_role = 'Legal' THEN
               L_ROLE_RESPONSIBILITY_REC_TYPE.responsibility_type := 'LEGAL';
            WHEN p_role = 'Financial Institution' THEN
               L_ROLE_RESPONSIBILITY_REC_TYPE.responsibility_type := 'FINANCIAL';
            WHEN p_role = 'Owner' THEN
               L_ROLE_RESPONSIBILITY_REC_TYPE.responsibility_type := 'OWNER';
            END CASE;

            L_ROLE_RESPONSIBILITY_REC_TYPE.created_by_module := l_api_name;

            hz_cust_account_role_v2pub.create_role_responsibility (
               p_init_msg_list             => 'T',
               p_role_responsibility_rec   => L_ROLE_RESPONSIBILITY_REC_TYPE,
               x_responsibility_id         => l_resp_id,
               x_return_status             => l_status,
               x_msg_count                 => l_msg_count,
               x_msg_data                  => l_msg_data);

            IF l_msg_count >= 1
            THEN
               FOR I IN 1 .. l_msg_count
               LOOP
                  l_error_message :=
                        l_error_message
                     || ' . '
                     || SUBSTR (
                           FND_MSG_PUB.Get (p_encoded => FND_API.G_FALSE),
                           1,
                           255);
                 dbms_output.put_line(I||'. '||SubStr(FND_MSG_PUB.Get(p_encoded => FND_API.G_FALSE ), 1, 255));
               END LOOP;
               l_error_message:= l_error_message || l_msg_data;
               RAISE l_program_error;
            END IF;
         END IF;
         DBMS_OUTPUT.PUT_LINE(' l_resp_id ' ||l_resp_id); 
         -----------------------------------------------------------
        -- Remove all previous Authorize Buyer roles for this Account
         -------------------------------------------------------------
        IF l_count = 1 THEN 
        FOR rec in (
        SELECT hcar.cust_account_id,
               hcar.cust_account_role_id,
               hrr.RESPONSIBILITY_TYPE,
               hrr.RESPONSIBILITY_id,
               hrr.creation_date,
               hcar.party_id,
               hcar.status,
               hcar.object_version_number 
          FROM apps.hz_cust_account_roles hcar, 
               apps.hz_role_responsibility hrr
         WHERE     hcar.cust_account_role_id = hrr.cust_account_role_id
               AND cust_account_id           = l_cust_account_id
               AND TRUNC (hrr.creation_date) < TRUNC (SYSDATE)
               AND hrr.RESPONSIBILITY_TYPE   = 'AUTH_BUYER')
               
               LOOP
                   DBMS_OUTPUT.PUT_LINE(' making inactive all previous Authorize Buyer ' ||rec.cust_account_role_id);
                   DBMS_OUTPUT.PUT_LINE('  for cust account id '||rec.cust_account_id);
                   DBMS_OUTPUT.PUT_LINE('  object_version_number '||rec.object_version_number);
                   
                   IF rec.object_version_number is NULL THEN 
                      l_object_version_number                     := 1;
                   ELSE
                      l_object_version_number                     := rec.object_version_number;
                   END IF;
                   
                   l_cr_cust_acc_role_rec                      := NULL;
                   --l_cr_cust_acc_role_rec.party_id             := l_rel_party_id;
                   l_cr_cust_acc_role_rec.cust_account_role_id := rec.cust_account_role_id;
                   l_cr_cust_acc_role_rec.cust_account_id      := rec.cust_account_id;
                   l_cr_cust_acc_role_rec.status               := 'I';
                   
                   
                   HZ_CUST_ACCOUNT_ROLE_V2PUB.update_cust_account_role ('T',
                                                                         l_cr_cust_acc_role_rec,
                                                                         l_object_version_number,
                                                                         l_status,
                                                                         l_msg_count,
                                                                         l_msg_data);
                   IF l_msg_count >= 1 THEN
                       FOR I IN 1 .. l_msg_count
                    LOOP
                      l_error_message :=
                            l_error_message
                         || ' . '
                         || SUBSTR (
                               FND_MSG_PUB.Get (p_encoded => FND_API.G_FALSE),
                               1,
                               255);
                     dbms_output.put_line(I||'. '||SubStr(FND_MSG_PUB.Get(p_encoded => FND_API.G_FALSE ), 1, 255));
                    END LOOP;
                    l_error_message:= l_error_message || l_msg_data;
                    RAISE l_program_error;
                    END IF;
            
               END LOOP;
               
          END IF; 
             
         IF l_count =  1 THEN 
          DBMS_OUTPUT.PUT_LINE('  Buyers set up exists previously so updating Buyer required DFF ');
         --  Buyers set up exists previously so updating Buyer required DFF
           UPDATE apps.HZ_CUST_ACCOUNTS_ALL 
           SET    ATTRIBUTE7 = 'Y'
           WHERE  cust_account_id= l_cust_account_id;
           COMMIT;
         ELSE
           DBMS_OUTPUT.PUT_LINE(' No Buyers set up previously so updating Buyer required DFF ');
           -- No Buyers set up previously so updating Buyer required DFF
           UPDATE apps.HZ_CUST_ACCOUNTS_ALL 
           SET    ATTRIBUTE7 = 'N'
           WHERE  cust_account_id= l_cust_account_id;
           COMMIT;
           
         END IF;
         
         l_msgencoded := fnd_message.get_encoded ();
         l_msgencodedlen := LENGTH (l_msgencoded);
         l_msgnameloc := INSTR (l_msgencoded, CHR (0));
         l_msgapp := SUBSTR (l_msgencoded, 1, l_msgnameloc - 1);
         l_msgencoded :=SUBSTR (l_msgencoded, l_msgnameloc + 1, l_msgencodedlen);
         l_msgencodedlen := LENGTH (l_msgencoded);
         l_msgtextloc := INSTR (l_msgencoded, CHR (0));
         l_msgname := SUBSTR (l_msgencoded, 1, l_msgtextloc - 1);

         IF (l_msgname <> 'CONC-SINGLE PENDING REQUEST') THEN
            fnd_message.set_name (l_msgapp, l_msgname);
         END IF;
         
      END IF;
      
   EXCEPTION
      WHEN l_program_error THEN
      
         FND_MESSAGE.SET_NAME ('XXWC', 'XXWC_ADI_ERROR_MSG');
         FND_MESSAGE.SET_TOKEN ('ERROR_MESSAGE', l_error_message);
         DBMS_OUTPUT.PUT_LINE('Error: '||l_error_message);
         
      WHEN OTHERS THEN
      
         l_error_message := 'Unidentified error - ' || SQLERRM;
         FND_MESSAGE.SET_NAME ('XXWC', 'XXWC_ADI_ERROR_MSG');
         FND_MESSAGE.SET_TOKEN ('ERROR_MESSAGE', l_error_message);
         DBMS_OUTPUT.PUT_LINE('Error: '||l_error_message);
         
   END IMPORT;
END XXWC_AR_AUTH_BUYER_WEBADI_PKG;
/