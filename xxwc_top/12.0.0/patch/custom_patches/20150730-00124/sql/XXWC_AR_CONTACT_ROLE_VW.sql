/* 
-- ************************************************************************
-- $Header XXWC_AR_CONTACT_ROLE_VW $
-- Module Name: XXWC 

-- REVISIONS:
-- Ver        Date        Author             Description
-- ---------  ----------  ----------         ----------------   
-- 1.1       12/22/2015 Neha Saini          TMS#20150730-00124 Initial Version.                                   
-- **************************************************************************/
CREATE OR REPLACE FORCE VIEW APPS.XXWC_AR_CONTACT_ROLE_VW
(
   DESCRIPTION
)
AS
   SELECT NULL description FROM DUAL
   UNION
   SELECT ffvv.DESCRIPTION
     FROM FND_FLEX_VALUES_VL ffvv, FND_FLEX_VALUE_SETS ffvs
    WHERE     ffvv.flex_value_set_id = ffvs.flex_value_set_id
          AND ffvs.flex_value_set_name = 'XXWC_CONTACT_ROLE';
/
