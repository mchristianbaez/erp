/*************************************************************************
  $Header TMS_20160503-00034_AWAITING_INVOICE_TO_CLOSE.sql $
  Module Name: TMS_20160503-00034 Data Fix script for I672142

  PURPOSE: Data Fix script for I672142

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        03-MAY-2016  Raghav Velichetti         TMS#20160503-00034

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000;
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20160503-00034   , Before Update');

update apps.oe_order_lines_all
set 
  INVOICE_INTERFACE_STATUS_CODE='YES'
,open_flag='N'
,flow_status_code='CLOSED'
,INVOICED_QUANTITY=35
where line_id=67143573
and headeR_id=41011427;

   DBMS_OUTPUT.put_line (
         'TMS: 20160503-00034 Sales order lines updated (Expected:1): '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20160503-00034   , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20160503-00034, Errors : ' || SQLERRM);
END;
/