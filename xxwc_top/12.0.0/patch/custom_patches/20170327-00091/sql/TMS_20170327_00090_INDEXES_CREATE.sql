/*****************************************************************************************************************************************************
  $Header TMS_20170327-00090 Dropping INDEXES

  REVISIONS:
  Ver        Date        Author                     Description
  ---------  ----------  ---------------         -------------------------
  1.0        27-Mar-2017 P.Vamshidhar            TMS#20170327-00090 - Drop indexes for forecast upload to prevent “Index lock contention” issue.
*****************************************************************************************************************************************************/
Set serverout on
 BEGIN
 DBMS_OUTPUT.PUT_LINE('MRP.MRP_FORECAST_DATES_U1 Index Creation Begin');
 Execute Immediate 'CREATE UNIQUE INDEX MRP.MRP_FORECAST_DATES_U1 ON MRP.MRP_FORECAST_DATES (TRANSACTION_ID)';
 DBMS_OUTPUT.PUT_LINE('MRP.MRP_FORECAST_DATES_U1 Index Creation End');
  DBMS_OUTPUT.PUT_LINE('MRP.MRP_FORECAST_DATES_N4 Index Creation Begin');
 Execute Immediate 'CREATE INDEX MRP.MRP_FORECAST_DATES_N4 ON MRP.MRP_FORECAST_DATES (ORIGINATION_TYPE)'; 
 DBMS_OUTPUT.PUT_LINE('MRP.MRP_FORECAST_DATES_N4 Index Creation End');
 END;
/
