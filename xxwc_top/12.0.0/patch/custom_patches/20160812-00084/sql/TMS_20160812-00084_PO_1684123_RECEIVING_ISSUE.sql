/*************************************************************************
  $Header TMS_20160812-00084_PO_1684123_RECEIVING_ISSUE.sql $
  Module Name: TMS_20160812-00084  Data Fix script  

  PURPOSE: Data Fix script 

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        06-SEP-2016  Pattabhi Avula        TMS#20160812-00084

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000;


BEGIN
   DBMS_OUTPUT.put_line ('Before update');

UPDATE rcv_transactions_interface
   SET request_id = NULL,
       processing_request_id = NULL,
       processing_status_code = 'PENDING',
       transaction_status_code = 'PENDING',
       processing_mode_code = 'BATCH',
       last_update_date = SYSDATE,
       Last_updated_by = 16991,
       transaction_date = SYSDATE
 WHERE po_header_id = 3022485;


   DBMS_OUTPUT.put_line ('Records update-' || SQL%ROWCOUNT);
   COMMIT;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Unable to update ' || SQLERRM);
END;
/