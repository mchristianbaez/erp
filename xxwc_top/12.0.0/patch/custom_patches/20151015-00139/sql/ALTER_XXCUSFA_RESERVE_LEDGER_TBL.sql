/************************************************************************** 
-- ************************************************************************
-- $Header XXCUSFA_RESERVE_LEDGER_TBL $
-- Module Name: XXCUS 

-- REVISIONS:
-- Ver        Date        Author             Description
-- ---------  ----------  ----------         ----------------   
-- 1.0       07/1/2016    Neha Saini         initial creation  for TMS#20151015-00139                                
-- **************************************************************************/
ALTER TABLE XXCUS.XXCUSFA_RESERVE_LEDGER_TBL
ADD ORIGINAL_COST NUMBER;
ALTER TABLE XXCUS.XXCUSFA_RESERVE_LEDGER_TBL
ADD ORIGINAL_UNITS NUMBER;
