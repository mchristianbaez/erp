CREATE OR REPLACE PACKAGE BODY APPS."XXCUSFA_DETAIL_EXTRACT_PKG" AS
  /**************************************************************************
   File Name: XXCUSFA_DETAIL_EXTRACT_PKG
  
   PROGRAM TYPE: PL/SQL Package
  
   PURPOSE: package declaration for extracting data from Oracle Assets.
  
   HISTORY
  =============================================================================
    VERSION DATE          AUTHOR(S)             DESCRIPTION
  --------- -----------   -------------------- -----------------------------------
    1.0     03-Aug-2011   Mani Kumar            Created this package.
    1.1     08/29/2012    Luong Vu              Add no history parameters 
                                                Incident 133808.  And logic
    1.2     09/25/2012    Luong Vu              Remove the fields in 1.1 and move 
                                                expense field after cost center.
    1.3     07-Jan-2016   Neha Saini            look changes for TMS#20151015-00139      
  **************************************************************************/
  PROCEDURE fa_detail_extract(p_errbuf            OUT VARCHAR2
                             ,p_retcode           OUT VARCHAR2
                             ,p_book              IN VARCHAR2
                             ,p_setofbookcurrency IN VARCHAR2
                             ,p_period            IN VARCHAR2
                             ,p_history           IN VARCHAR2) IS
    -- Variable declaration
  
    v_dist_book      VARCHAR2(15);
    v_ucd            DATE;
    v_upc            NUMBER;
    v_tod            DATE;
    v_tpc            NUMBER;
    v_reporting_flag VARCHAR2(1);
    v_sql_delete     VARCHAR2(100);
    l_location       VARCHAR2(200);
    l_procedure_name VARCHAR2(75) := 'XXCUSFA_DETAIL_EXTRACT_PKG.FA_DETAIL_EXTRACT';
    l_distro_list    VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
    l_req_id         NUMBER := fnd_global.conc_request_id;
  
  BEGIN
  
    -- Deleting Data from Staging table
    l_location   := 'Truncating data from XXCUS.XXCUSFA_RESERVE_LEDGER_TBL';
    v_sql_delete := 'TRUNCATE TABLE XXCUS.XXCUSFA_RESERVE_LEDGER_TBL';
    EXECUTE IMMEDIATE v_sql_delete;
  
    l_location := 'Get MRC Related Information';
  
    BEGIN
      SELECT sob.mrc_sob_type_code
        INTO v_reporting_flag
        FROM apps.fa_book_controls fbc
            ,apps.gl_sets_of_books sob
       WHERE fbc.book_type_code = p_book
            --AND sob.CURRENCY_CODE  = p_setofbookcurrency
         AND fbc.set_of_books_id = sob.set_of_books_id;
    EXCEPTION
      WHEN OTHERS THEN
        v_reporting_flag := 'P';
    END;
  
    l_location := 'Get Depreciation Period Related Information';
  
    IF (v_reporting_flag = 'R') THEN
      SELECT bc.distribution_source_book dbk
            ,nvl(dp.period_close_date, SYSDATE) v_ucd
            ,dp.period_counter v_upc
            ,MIN(dp_fy.period_open_date) v_tod
            ,MIN(dp_fy.period_counter) v_tpc
        INTO v_dist_book
            ,v_ucd
            ,v_upc
            ,v_tod
            ,v_tpc
        FROM fa_deprn_periods_mrc_v dp
            ,fa_deprn_periods_mrc_v dp_fy
            ,fa_book_controls_mrc_v bc
       WHERE dp.book_type_code = p_book
         AND dp.period_name = p_period
         AND dp_fy.book_type_code = p_book
         AND dp_fy.fiscal_year = dp.fiscal_year
         AND bc.book_type_code = p_book
       GROUP BY bc.distribution_source_book
               ,dp.period_close_date
               ,dp.period_counter;
    ELSE
      SELECT bc.distribution_source_book dbk
            ,nvl(dp.period_close_date, SYSDATE) v_ucd
            ,dp.period_counter v_upc
            ,MIN(dp_fy.period_open_date) v_tod
            ,MIN(dp_fy.period_counter) v_tpc
        INTO v_dist_book
            ,v_ucd
            ,v_upc
            ,v_tod
            ,v_tpc
        FROM fa_deprn_periods dp
            ,fa_deprn_periods dp_fy
            ,fa_book_controls bc
       WHERE dp.book_type_code = p_book
         AND dp.period_name = p_period
         AND dp_fy.book_type_code = p_book
         AND dp_fy.fiscal_year = dp.fiscal_year
         AND bc.book_type_code = p_book
       GROUP BY bc.distribution_source_book
               ,dp.period_close_date
               ,dp.period_counter;
    END IF;
  
    IF (nvl(fnd_profile.value('CRL-FA ENABLED'), 'N') = 'N') THEN
      IF (v_reporting_flag = 'R') THEN
        -- Begin if CRL not installed and v_reporting_flag = R
        l_location := 'Getting into the loop - CRL not installed and v_reporting_flag = R';
        INSERT INTO xxcus.xxcusfa_reserve_ledger_tbl
          (asset_id
          ,dh_ccid
          ,deprn_reserve_acct
          ,date_placed_in_service
          ,method_code
          ,life
          ,rate
          ,capacity
          ,cost
          ,deprn_amount
          ,ytd_deprn
          ,deprn_reserve
          ,percent
          ,transaction_type
          ,period_counter
          ,date_effective
          ,reserve_acct
          ,units_assigned
          ,location_id
          ,remaining_life1
          ,adjustment_amount
          ,original_cost  --added for TMS#20151015-00139 change for ver 1.3
          ,original_units)--added for TMS#20151015-00139 change for ver 1.3
          SELECT dh.asset_id asset_id
                ,dh.code_combination_id dh_ccid
                ,cb.deprn_reserve_acct rsv_account
                ,books.date_placed_in_service start_date
                ,books.deprn_method_code method
                ,books.life_in_months life
                ,books.adjusted_rate rate
                ,books.production_capacity capacity
                ,dd_bonus.cost cost
                ,decode(dd_bonus.period_counter, v_upc,
                        dd_bonus.deprn_amount - dd_bonus.bonus_deprn_amount,
                        0) deprn_amount
                ,decode(sign(v_tpc - dd_bonus.period_counter), 1, 0,
                        dd_bonus.ytd_deprn - dd_bonus.bonus_ytd_deprn) ytd_deprn
                ,dd_bonus.deprn_reserve - dd_bonus.bonus_deprn_reserve deprn_reserve
                ,decode(th.transaction_type_code, NULL,
                        dh.units_assigned / ah.units * 100) percent
                ,decode(th.transaction_type_code, NULL, decode(th_rt.transaction_type_code, 'FULL RETIREMENT', 'FUL',decode(books.depreciate_flag, 'NO', 'N')),
                      ----TMS#20151015-00139 change for ver 1.3 starts
                      --  'TRANSFER', 'T', 
                      --   'TRANSFER OUT', 'P', 
                      --   'RECLASS', 'R'
                        'ADDITION'           ,'ADD',
                        'ADJUSTMENT'         ,'ADJ',
                        'UNIT ADJUSTMENT'    ,'ADJ',
                        'PARTIAL RETIREMENT' ,'PAR',
                        'TRANSFER'           ,'TRA',
                        'TRANSFER IN'        ,'TRA',
                        'TRANSFER OUT'       ,'TRA',
                        'REINSTATEMENT'      ,'REI',
                        'RECLASS'            ,'REC') t_type
                        ----TMS#20151015-00139 change for ver 1.3 ends
                ,dd_bonus.period_counter
                ,nvl(th.date_effective, v_ucd)
                ,''
                ,dh.units_assigned
                ,dh.location_id
                ,books.remaining_life1
                ,dd_bonus.deprn_adjustment_amount adjusted_amount
                ,books.original_cost --TMS#20151015-00139 change for ver 1.3
                , (SELECT fah1.units
                    FROM fa_asset_history fah1
                   WHERE fah1.asset_id = ah.asset_id
                     AND fah1.DATE_EFFECTIVE = (SELECT MIN (fah.DATE_EFFECTIVE)
                                               FROM fa_asset_history fah
                                              WHERE fah.asset_id = ah.asset_id)) original_units --TMS#20151015-00139 change for ver 1.3
            FROM fa_deprn_detail_mrc_v   dd_bonus
                ,fa_asset_history        ah
                ,fa_transaction_headers  th
                ,fa_transaction_headers  th_rt
                ,fa_books_mrc_v          books
                ,fa_distribution_history dh
                ,fa_category_books       cb
           WHERE cb.book_type_code = p_book
             AND cb.category_id = ah.category_id
             AND ah.asset_id = dh.asset_id
             AND ah.date_effective < nvl(th.date_effective, v_ucd)
             AND nvl(ah.date_ineffective, SYSDATE) >=
                 nvl(th.date_effective, v_ucd)
             AND ah.asset_type = 'CAPITALIZED'
             AND dd_bonus.book_type_code = p_book
             AND dd_bonus.distribution_id = dh.distribution_id
             AND dd_bonus.period_counter =
                 (SELECT MAX(dd_sub.period_counter)
                    FROM fa_deprn_detail_mrc_v dd_sub
                   WHERE dd_sub.book_type_code = p_book
                     AND dd_sub.asset_id = dh.asset_id
                     AND dd_sub.distribution_id = dh.distribution_id
                     AND dd_sub.period_counter <= v_upc)
             AND th_rt.book_type_code = p_book
             AND th_rt.transaction_header_id =
                 books.transaction_header_id_in
             AND books.book_type_code = p_book
             AND books.asset_id = dh.asset_id
             AND nvl(books.period_counter_fully_retired, v_upc) >= v_tpc
             AND books.date_effective <= nvl(th.date_effective, v_ucd)
             AND nvl(books.date_ineffective, SYSDATE + 1) >
                 nvl(th.date_effective, v_ucd)
             AND th.book_type_code(+) = v_dist_book
             AND th.transaction_header_id(+) = dh.transaction_header_id_out
             AND th.date_effective(+) BETWEEN v_tod AND v_ucd
             AND dh.book_type_code = v_dist_book
             AND dh.date_effective <= v_ucd
             AND nvl(dh.date_ineffective, SYSDATE) > v_tod
          UNION ALL
          SELECT dh.asset_id asset_id
                ,dh.code_combination_id dh_ccid
                ,cb.bonus_deprn_reserve_acct rsv_account
                ,books.date_placed_in_service start_date
                ,books.deprn_method_code method
                ,books.life_in_months life
                ,books.adjusted_rate rate
                ,books.production_capacity capacity
                ,0 cost
                ,decode(dd.period_counter, v_upc, dd.bonus_deprn_amount, 0) deprn_amount
                ,decode(sign(v_tpc - dd.period_counter), 1, 0,
                        dd.bonus_ytd_deprn) ytd_deprn
                ,dd.bonus_deprn_reserve deprn_reserve
                ,0 percent
                ,'B' t_type
                ,dd.period_counter
                ,nvl(th.date_effective, v_ucd)
                ,cb.bonus_deprn_expense_acct
                ,dh.units_assigned
                ,dh.location_id
                ,books.remaining_life1 remaining_life
                ,dd.deprn_adjustment_amount adjusted_amount
                ,books.original_cost --TMS#20151015-00139 change for ver 1.3
                , (SELECT fah1.units
                    FROM fa_asset_history fah1
                   WHERE fah1.asset_id = ah.asset_id
                     AND fah1.DATE_EFFECTIVE = (SELECT MIN (fah.DATE_EFFECTIVE)
                                               FROM fa_asset_history fah
                                              WHERE fah.asset_id = ah.asset_id)) original_units--TMS#20151015-00139 change for ver 1.3
            FROM fa_deprn_detail_mrc_v   dd
                ,fa_asset_history        ah
                ,fa_transaction_headers  th
                ,fa_transaction_headers  th_rt
                ,fa_books_mrc_v          books
                ,fa_distribution_history dh
                ,fa_category_books       cb
           WHERE cb.book_type_code = p_book
             AND cb.category_id = ah.category_id
             AND ah.asset_id = dh.asset_id
             AND ah.date_effective < nvl(th.date_effective, v_ucd)
             AND nvl(ah.date_ineffective, SYSDATE) >=
                 nvl(th.date_effective, v_ucd)
             AND ah.asset_type = 'CAPITALIZED'
             AND dd.book_type_code = p_book
             AND dd.distribution_id = dh.distribution_id
             AND dd.period_counter =
                 (SELECT MAX(dd_sub.period_counter)
                    FROM fa_deprn_detail_mrc_v dd_sub
                   WHERE dd_sub.book_type_code = p_book
                     AND dd_sub.asset_id = dh.asset_id
                     AND dd_sub.distribution_id = dh.distribution_id
                     AND dd_sub.period_counter <= v_upc)
             AND th_rt.book_type_code = p_book
             AND th_rt.transaction_header_id =
                 books.transaction_header_id_in
             AND books.book_type_code = p_book
             AND books.asset_id = dh.asset_id
             AND nvl(books.period_counter_fully_retired, v_upc) >= v_tpc
             AND books.date_effective <= nvl(th.date_effective, v_ucd)
             AND nvl(books.date_ineffective, SYSDATE + 1) >
                 nvl(th.date_effective, v_ucd)
             AND books.bonus_rule IS NOT NULL
             AND th.book_type_code(+) = v_dist_book
             AND th.transaction_header_id(+) = dh.transaction_header_id_out
             AND th.date_effective(+) BETWEEN v_tod AND v_ucd
             AND dh.book_type_code = v_dist_book
             AND dh.date_effective <= v_ucd
             AND nvl(dh.date_ineffective, SYSDATE) > v_tod;
        -- End  if CRL not installed and v_reporting_flag = R
      ELSE
        -- Begin if CRL not installed and v_reporting_flag Is NOT R
        l_location := 'Getting into the loop - CRL not installed and v_reporting_flag Is NOT R';
      
        INSERT INTO xxcus.xxcusfa_reserve_ledger_tbl
          (asset_id
          ,dh_ccid
          ,deprn_reserve_acct
          ,date_placed_in_service
          ,method_code
          ,life
          ,rate
          ,capacity
          ,cost
          ,deprn_amount
          ,ytd_deprn
          ,deprn_reserve
          ,percent
          ,transaction_type
          ,period_counter
          ,date_effective
          ,reserve_acct
          ,units_assigned
          ,location_id
          ,remaining_life1
          ,adjustment_amount
          ,original_cost--TMS#20151015-00139 change for ver 1.3
          ,original_units)--TMS#20151015-00139 change for ver 1.3
          SELECT dh.asset_id                  asset_id
                ,dh.code_combination_id       dh_ccid
                ,cb.deprn_reserve_acct        rsv_account
                ,books.date_placed_in_service start_date
                ,books.deprn_method_code      method
                ,books.life_in_months         life
                ,books.adjusted_rate          rate
                ,books.production_capacity    capacity
                ,dd_bonus.cost                cost
                ,
                 
                 decode(dd_bonus.period_counter, v_upc,
                        dd_bonus.deprn_amount - dd_bonus.bonus_deprn_amount,
                        0) deprn_amount
                ,
                 
                 decode(sign(v_tpc - dd_bonus.period_counter), 1, 0,
                        dd_bonus.ytd_deprn - dd_bonus.bonus_ytd_deprn) ytd_deprn
                ,
                 
                 dd_bonus.deprn_reserve - dd_bonus.bonus_deprn_reserve deprn_reserve
                ,decode(th.transaction_type_code, NULL,
                        dh.units_assigned / ah.units * 100) percent
                ,decode(th.transaction_type_code, NULL, decode(th_rt.transaction_type_code, 'FULL RETIREMENT', 'FUL',decode(books.depreciate_flag, 'NO', 'N')),
                      ----TMS#20151015-00139 change for ver 1.3 starts
                      --  'TRANSFER', 'T', 
                      --   'TRANSFER OUT', 'P', 
                      --   'RECLASS', 'R'
                        'ADDITION'           ,'ADD',
                        'ADJUSTMENT'         ,'ADJ',
                        'UNIT ADJUSTMENT'    ,'ADJ',
                        'PARTIAL RETIREMENT' ,'PAR',
                        'TRANSFER'           ,'TRA',
                        'TRANSFER IN'        ,'TRA',
                        'TRANSFER OUT'       ,'TRA',
                        'REINSTATEMENT'      ,'REI',
                        'RECLASS'            ,'REC') t_type
                        ----TMS#20151015-00139 change for ver 1.3 ends
                ,dd_bonus.period_counter
                ,nvl(th.date_effective, v_ucd)
                ,''
                ,dh.units_assigned
                ,dh.location_id
                ,books.remaining_life1
                ,dd_bonus.deprn_adjustment_amount adjusted_amount
                ,books.original_cost--TMS#20151015-00139 change for ver 1.3
                , (SELECT fah1.units
                    FROM fa_asset_history fah1
                   WHERE fah1.asset_id = ah.asset_id
                     AND fah1.DATE_EFFECTIVE = (SELECT MIN (fah.DATE_EFFECTIVE)
                                               FROM fa_asset_history fah
                                              WHERE fah.asset_id = ah.asset_id)) original_units--TMS#20151015-00139 change for ver 1.3
            FROM fa_deprn_detail         dd_bonus
                ,fa_asset_history        ah
                ,fa_transaction_headers  th
                ,fa_transaction_headers  th_rt
                ,fa_books                books
                ,fa_distribution_history dh
                ,fa_category_books       cb
           WHERE cb.book_type_code = p_book
             AND cb.category_id = ah.category_id
             AND ah.asset_id = dh.asset_id
             AND ah.date_effective < nvl(th.date_effective, v_ucd)
             AND nvl(ah.date_ineffective, SYSDATE) >=
                 nvl(th.date_effective, v_ucd)
             AND ah.asset_type = 'CAPITALIZED'
             AND dd_bonus.book_type_code = p_book
             AND dd_bonus.distribution_id = dh.distribution_id
             AND dd_bonus.period_counter =
                 (SELECT MAX(dd_sub.period_counter)
                    FROM fa_deprn_detail dd_sub
                   WHERE dd_sub.book_type_code = p_book
                     AND dd_sub.asset_id = dh.asset_id
                     AND dd_sub.distribution_id = dh.distribution_id
                     AND dd_sub.period_counter <= v_upc)
             AND th_rt.book_type_code = p_book
             AND th_rt.transaction_header_id =
                 books.transaction_header_id_in
             AND books.book_type_code = p_book
             AND books.asset_id = dh.asset_id
             AND nvl(books.period_counter_fully_retired, v_upc) >= v_tpc
             AND books.date_effective <= nvl(th.date_effective, v_ucd)
             AND nvl(books.date_ineffective, SYSDATE + 1) >
                 nvl(th.date_effective, v_ucd)
             AND th.book_type_code(+) = v_dist_book
             AND th.transaction_header_id(+) = dh.transaction_header_id_out
             AND th.date_effective(+) BETWEEN v_tod AND v_ucd
             AND dh.book_type_code = v_dist_book
             AND dh.date_effective <= v_ucd
             AND nvl(dh.date_ineffective, SYSDATE) > v_tod
          UNION ALL
          SELECT dh.asset_id asset_id
                ,dh.code_combination_id dh_ccid
                ,cb.bonus_deprn_reserve_acct rsv_account
                ,books.date_placed_in_service start_date
                ,books.deprn_method_code method
                ,books.life_in_months life
                ,books.adjusted_rate rate
                ,books.production_capacity capacity
                ,0 cost
                ,decode(dd.period_counter, v_upc, dd.bonus_deprn_amount, 0) deprn_amount
                ,decode(sign(v_tpc - dd.period_counter), 1, 0,
                        dd.bonus_ytd_deprn) ytd_deprn
                ,dd.bonus_deprn_reserve deprn_reserve
                ,0 percent
                ,'B' t_type
                ,dd.period_counter
                ,nvl(th.date_effective, v_ucd)
                ,cb.bonus_deprn_expense_acct
                ,dh.units_assigned
                ,dh.location_id
                ,books.remaining_life1
                ,NULL adjusted_amount
                ,books.original_cost--TMS#20151015-00139 change for ver 1.3
                , (SELECT fah1.units
                    FROM fa_asset_history fah1
                   WHERE fah1.asset_id = ah.asset_id
                     AND fah1.DATE_EFFECTIVE = (SELECT MIN (fah.DATE_EFFECTIVE)
                                               FROM fa_asset_history fah
                                              WHERE fah.asset_id = ah.asset_id)) original_units--TMS#20151015-00139 change for ver 1.3
            FROM fa_deprn_detail         dd
                ,fa_asset_history        ah
                ,fa_transaction_headers  th
                ,fa_transaction_headers  th_rt
                ,fa_books                books
                ,fa_distribution_history dh
                ,fa_category_books       cb
           WHERE cb.book_type_code = p_book
             AND cb.category_id = ah.category_id
             AND ah.asset_id = dh.asset_id
             AND ah.date_effective < nvl(th.date_effective, v_ucd)
             AND nvl(ah.date_ineffective, SYSDATE) >=
                 nvl(th.date_effective, v_ucd)
             AND ah.asset_type = 'CAPITALIZED'
             AND dd.book_type_code = p_book
             AND dd.distribution_id = dh.distribution_id
             AND dd.period_counter =
                 (SELECT MAX(dd_sub.period_counter)
                    FROM fa_deprn_detail dd_sub
                   WHERE dd_sub.book_type_code = p_book
                     AND dd_sub.asset_id = dh.asset_id
                     AND dd_sub.distribution_id = dh.distribution_id
                     AND dd_sub.period_counter <= v_upc)
             AND th_rt.book_type_code = p_book
             AND th_rt.transaction_header_id =
                 books.transaction_header_id_in
             AND books.book_type_code = p_book
             AND books.asset_id = dh.asset_id
             AND nvl(books.period_counter_fully_retired, v_upc) >= v_tpc
             AND books.date_effective <= nvl(th.date_effective, v_ucd)
             AND nvl(books.date_ineffective, SYSDATE + 1) >
                 nvl(th.date_effective, v_ucd)
             AND books.bonus_rule IS NOT NULL
             AND th.book_type_code(+) = v_dist_book
             AND th.transaction_header_id(+) = dh.transaction_header_id_out
             AND th.date_effective(+) BETWEEN v_tod AND v_ucd
             AND dh.book_type_code = v_dist_book
             AND dh.date_effective <= v_ucd
             AND nvl(dh.date_ineffective, SYSDATE) > v_tod;
        -- End if CRL not installed and v_reporting_flag Is NOT R
      END IF;
    
    ELSIF (nvl(fnd_profile.value('CRL-FA ENABLED'), 'N') = 'Y') THEN
      -- Insert Non-Group Details
      l_location := 'Inserting Non-Group Details';
    
      IF (v_reporting_flag = 'R') THEN
        INSERT INTO xxcus.xxcusfa_reserve_ledger_tbl
          (asset_id
          ,dh_ccid
          ,deprn_reserve_acct
          ,date_placed_in_service
          ,method_code
          ,life
          ,rate
          ,capacity
          ,cost
          ,deprn_amount
          ,ytd_deprn
          ,deprn_reserve
          ,percent
          ,transaction_type
          ,period_counter
          ,date_effective
          ,units_assigned
          ,location_id
          ,remaining_life1
          ,adjustment_amount
          ,original_cost--TMS#20151015-00139 change for ver 1.3
          ,original_units)--TMS#20151015-00139 change for ver 1.3
          SELECT dh.asset_id asset_id
                ,dh.code_combination_id dh_ccid
                ,cb.deprn_reserve_acct rsv_account
                ,books.date_placed_in_service start_date
                ,books.deprn_method_code method
                ,books.life_in_months life
                ,books.adjusted_rate rate
                ,books.production_capacity capacity
                ,dd.cost cost
                ,decode(dd.period_counter, v_upc, dd.deprn_amount, 0) deprn_amount
                ,decode(sign(v_tpc - dd.period_counter), 1, 0, dd.ytd_deprn) ytd_deprn
                ,dd.deprn_reserve deprn_reserve
                ,decode(th.transaction_type_code, NULL,
                        dh.units_assigned / ah.units * 100) percent
                ,decode(th.transaction_type_code, NULL, decode(th_rt.transaction_type_code, 'FULL RETIREMENT', 'FUL',decode(books.depreciate_flag, 'NO', 'N')),
                      ----TMS#20151015-00139 change for ver 1.3 starts
                      --  'TRANSFER', 'T', 
                      --   'TRANSFER OUT', 'P', 
                      --   'RECLASS', 'R'
                        'ADDITION'           ,'ADD',
                        'ADJUSTMENT'         ,'ADJ',
                        'UNIT ADJUSTMENT'    ,'ADJ',
                        'PARTIAL RETIREMENT' ,'PAR',
                        'TRANSFER'           ,'TRA',
                        'TRANSFER IN'        ,'TRA',
                        'TRANSFER OUT'       ,'TRA',
                        'REINSTATEMENT'      ,'REI',
                        'RECLASS'            ,'REC') t_type
                        ----TMS#20151015-00139 change for ver 1.3 ends
                ,dd.period_counter
                ,nvl(th.date_effective, v_ucd)
                ,dh.units_assigned
                ,dh.location_id
                ,books.remaining_life1
                ,dd.deprn_adjustment_amount adjusted_amount
                ,books.original_cost--TMS#20151015-00139 change for ver 1.3
                , (SELECT fah1.units
                    FROM fa_asset_history fah1
                   WHERE fah1.asset_id = ah.asset_id
                     AND fah1.DATE_EFFECTIVE = (SELECT MIN (fah.DATE_EFFECTIVE)
                                               FROM fa_asset_history fah
                                              WHERE fah.asset_id = ah.asset_id)) original_units--TMS#20151015-00139 change for ver 1.3
            FROM fa_deprn_detail_mrc_v   dd
                ,fa_asset_history        ah
                ,fa_transaction_headers  th
                ,fa_transaction_headers  th_rt
                ,fa_books_mrc_v          books
                ,fa_distribution_history dh
                ,fa_category_books       cb
           WHERE
          -- start cua  - exclude the group Assets
           books.group_asset_id IS NULL
           AND -- end cua
           cb.book_type_code = p_book
           AND cb.category_id = ah.category_id
           AND ah.asset_id = dh.asset_id
           AND ah.date_effective < nvl(th.date_effective, v_ucd)
           AND nvl(ah.date_ineffective, SYSDATE) >=
           nvl(th.date_effective, v_ucd)
           AND ah.asset_type = 'CAPITALIZED'
           AND dd.book_type_code = p_book
           AND dd.distribution_id = dh.distribution_id
           AND dd.period_counter =
           (SELECT MAX(dd_sub.period_counter)
              FROM fa_deprn_detail_mrc_v dd_sub
             WHERE dd_sub.book_type_code = p_book
               AND dd_sub.asset_id = dh.asset_id
               AND dd_sub.distribution_id = dh.distribution_id
               AND dd_sub.period_counter <= v_upc)
           AND th_rt.book_type_code = p_book
           AND th_rt.transaction_header_id = books.transaction_header_id_in
           AND books.book_type_code = p_book
           AND books.asset_id = dh.asset_id
           AND nvl(books.period_counter_fully_retired, v_upc) >= v_tpc
           AND books.date_effective <= nvl(th.date_effective, v_ucd)
           AND nvl(books.date_ineffective, SYSDATE + 1) >
           nvl(th.date_effective, v_ucd)
           AND th.book_type_code(+) = v_dist_book
           AND th.transaction_header_id(+) = dh.transaction_header_id_out
           AND th.date_effective(+) BETWEEN v_tod AND v_ucd
           AND dh.book_type_code = v_dist_book
           AND dh.date_effective <= v_ucd
           AND nvl(dh.date_ineffective, SYSDATE) > v_tod
           AND
          -- start cua  - exclude the group Assets
           books.group_asset_id IS NULL;
      ELSE
        INSERT INTO xxcus.xxcusfa_reserve_ledger_tbl
          (asset_id
          ,dh_ccid
          ,deprn_reserve_acct
          ,date_placed_in_service
          ,method_code
          ,life
          ,rate
          ,capacity
          ,cost
          ,deprn_amount
          ,ytd_deprn
          ,deprn_reserve
          ,percent
          ,transaction_type
          ,period_counter
          ,date_effective
          ,units_assigned
          ,location_id
          ,remaining_life1
          ,adjustment_amount
          ,original_cost--TMS#20151015-00139 change for ver 1.3
          ,original_units)--TMS#20151015-00139 change for ver 1.3
          SELECT dh.asset_id asset_id
                ,dh.code_combination_id dh_ccid
                ,cb.deprn_reserve_acct rsv_account
                ,books.date_placed_in_service start_date
                ,books.deprn_method_code method
                ,books.life_in_months life
                ,books.adjusted_rate rate
                ,books.production_capacity capacity
                ,dd.cost cost
                ,decode(dd.period_counter, v_upc, dd.deprn_amount, 0) deprn_amount
                ,decode(sign(v_tpc - dd.period_counter), 1, 0, dd.ytd_deprn) ytd_deprn
                ,dd.deprn_reserve deprn_reserve
                ,decode(th.transaction_type_code, NULL,
                        dh.units_assigned / ah.units * 100) percent
                ,decode(th.transaction_type_code, NULL, decode(th_rt.transaction_type_code, 'FULL RETIREMENT', 'FUL',decode(books.depreciate_flag, 'NO', 'N')),
                      ----TMS#20151015-00139 change for ver 1.3 starts
                      --  'TRANSFER', 'T', 
                      --   'TRANSFER OUT', 'P', 
                      --   'RECLASS', 'R'
                        'ADDITION'           ,'ADD',
                        'ADJUSTMENT'         ,'ADJ',
                        'UNIT ADJUSTMENT'    ,'ADJ',
                        'PARTIAL RETIREMENT' ,'PAR',
                        'TRANSFER'           ,'TRA',
                        'TRANSFER IN'        ,'TRA',
                        'TRANSFER OUT'       ,'TRA',
                        'REINSTATEMENT'      ,'REI',
                        'RECLASS'            ,'REC') t_type
                        ----TMS#20151015-00139 change for ver 1.3 ends
                ,dd.period_counter
                ,nvl(th.date_effective, v_ucd)
                ,dh.units_assigned
                ,dh.location_id
                ,books.remaining_life1
                ,dd.deprn_adjustment_amount adjusted_amount
                ,books.original_cost--TMS#20151015-00139 change for ver 1.3
                , (SELECT fah1.units
                    FROM fa_asset_history fah1
                   WHERE fah1.asset_id = ah.asset_id
                     AND fah1.DATE_EFFECTIVE = (SELECT MIN (fah.DATE_EFFECTIVE)
                                               FROM fa_asset_history fah
                                              WHERE fah.asset_id = ah.asset_id)) original_units--TMS#20151015-00139 change for ver 1.3
            FROM fa_deprn_detail         dd
                ,fa_asset_history        ah
                ,fa_transaction_headers  th
                ,fa_transaction_headers  th_rt
                ,fa_books                books
                ,fa_distribution_history dh
                ,fa_category_books       cb
           WHERE
          -- start cua  - exclude the group Assets
           books.group_asset_id IS NULL
           AND -- end cua
           cb.book_type_code = p_book
           AND cb.category_id = ah.category_id
           AND ah.asset_id = dh.asset_id
           AND ah.date_effective < nvl(th.date_effective, v_ucd)
           AND nvl(ah.date_ineffective, SYSDATE) >=
           nvl(th.date_effective, v_ucd)
           AND ah.asset_type = 'CAPITALIZED'
           AND dd.book_type_code = p_book
           AND dd.distribution_id = dh.distribution_id
           AND dd.period_counter =
           (SELECT MAX(dd_sub.period_counter)
              FROM fa_deprn_detail dd_sub
             WHERE dd_sub.book_type_code = p_book
               AND dd_sub.asset_id = dh.asset_id
               AND dd_sub.distribution_id = dh.distribution_id
               AND dd_sub.period_counter <= v_upc)
           AND th_rt.book_type_code = p_book
           AND th_rt.transaction_header_id = books.transaction_header_id_in
           AND books.book_type_code = p_book
           AND books.asset_id = dh.asset_id
           AND nvl(books.period_counter_fully_retired, v_upc) >= v_tpc
           AND books.date_effective <= nvl(th.date_effective, v_ucd)
           AND nvl(books.date_ineffective, SYSDATE + 1) >
           nvl(th.date_effective, v_ucd)
           AND th.book_type_code(+) = v_dist_book
           AND th.transaction_header_id(+) = dh.transaction_header_id_out
           AND th.date_effective(+) BETWEEN v_tod AND v_ucd
           AND dh.book_type_code = v_dist_book
           AND dh.date_effective <= v_ucd
           AND nvl(dh.date_ineffective, SYSDATE) > v_tod
           AND
          -- start cua  - exclude the group Assets
           books.group_asset_id IS NULL;
      END IF; -- IF (v_reporting_flag = 'R') THEN
    END IF; -- IF (nvl(fnd_profile.value('CRL-FA ENABLED'), 'N') = 'N' ) THEN
  
    -- Insert the Group Depreciation Details
    l_location := 'Inserting the Group Depreciation Details';
  
    IF (v_reporting_flag = 'R') THEN
      INSERT INTO xxcus.xxcusfa_reserve_ledger_tbl
        (asset_id
        ,dh_ccid
        ,deprn_reserve_acct
        ,date_placed_in_service
        ,method_code
        ,life
        ,rate
        ,capacity
        ,cost
        ,deprn_amount
        ,ytd_deprn
        ,deprn_reserve
        ,percent
        ,transaction_type
        ,period_counter
        ,date_effective
        ,units_assigned
        ,location_id
        ,remaining_life1
        ,adjustment_amount
        ,original_cost--TMS#20151015-00139 change for ver 1.3
        ,original_units)--TMS#20151015-00139 change for ver 1.3
        SELECT gar.group_asset_id asset_id
              ,gad.deprn_expense_acct_ccid ch_ccid
              ,gad.deprn_reserve_acct_ccid rsv_account
              ,gar.deprn_start_date start_date
              ,gar.deprn_method_code method
              ,gar.life_in_months life
              ,gar.adjusted_rate rate
              ,gar.production_capacity capacity
              ,dd.adjusted_cost cost
              ,decode(dd.period_counter, v_upc, dd.deprn_amount, 0) deprn_amount
              ,decode(sign(v_tpc - dd.period_counter), 1, 0, dd.ytd_deprn) ytd_deprn
              ,dd.deprn_reserve deprn_reserve
              ,100 percent
              ,'G' t_type
              ,dd.period_counter
              ,v_ucd
              ,NULL
              ,NULL
              ,NULL
              ,dd.deprn_adjustment_amount adjusted_amount
              ,NULL--TMS#20151015-00139 change for ver 1.3
              ,NULL--TMS#20151015-00139 change for ver 1.3
          FROM fa_deprn_summary_mrc_v dd
              ,fa_group_asset_rules   gar
              ,fa_group_asset_default gad
              ,fa_deprn_periods_mrc_v dp
         WHERE dd.book_type_code = p_book
           AND dd.asset_id = gar.group_asset_id
           AND gad.super_group_id IS NULL
           AND -- MPOWELL
               gar.book_type_code = dd.book_type_code
           AND gad.book_type_code = gar.book_type_code
           AND gad.group_asset_id = gar.group_asset_id
           AND dd.period_counter =
               (SELECT MAX(dd_sub.period_counter)
                  FROM fa_deprn_detail_mrc_v dd_sub
                 WHERE dd_sub.book_type_code = p_book
                   AND dd_sub.asset_id = gar.group_asset_id
                   AND dd_sub.period_counter <= v_upc)
           AND dd.period_counter = dp.period_counter
           AND dd.book_type_code = dp.book_type_code
           AND gar.date_effective <= dp.calendar_period_close_date
           AND -- mwoodwar
               nvl(gar.date_ineffective, (dp.calendar_period_close_date + 1)) >
               dp.calendar_period_close_date; -- mwoodwar
    ELSE
      -- IF (v_reporting_flag = 'R') THEN
      INSERT INTO xxcus.xxcusfa_reserve_ledger_tbl
        (asset_id
        ,dh_ccid
        ,deprn_reserve_acct
        ,date_placed_in_service
        ,method_code
        ,life
        ,rate
        ,capacity
        ,cost
        ,deprn_amount
        ,ytd_deprn
        ,deprn_reserve
        ,percent
        ,transaction_type
        ,period_counter
        ,date_effective
        ,units_assigned
        ,location_id
        ,remaining_life1
        ,adjustment_amount
        ,original_cost--TMS#20151015-00139 change for ver 1.3
        ,original_units)--TMS#20151015-00139 change for ver 1.3
        SELECT gar.group_asset_id asset_id
              ,gad.deprn_expense_acct_ccid ch_ccid
              ,gad.deprn_reserve_acct_ccid rsv_account
              ,gar.deprn_start_date start_date
              ,gar.deprn_method_code method
              ,gar.life_in_months life
              ,gar.adjusted_rate rate
              ,gar.production_capacity capacity
              ,dd.adjusted_cost cost
              ,decode(dd.period_counter, v_upc, dd.deprn_amount, 0) deprn_amount
              ,decode(sign(v_tpc - dd.period_counter), 1, 0, dd.ytd_deprn) ytd_deprn
              ,dd.deprn_reserve deprn_reserve
              ,100 percent
              ,'G' t_type
              ,dd.period_counter
              ,v_ucd
              ,NULL
              ,NULL
              ,NULL
              ,dd.deprn_adjustment_amount adjusted_amount
              ,NULL--TMS#20151015-00139 change for ver 1.3
              ,NULL--TMS#20151015-00139 change for ver 1.3
          FROM fa_deprn_summary       dd
              ,fa_group_asset_rules   gar
              ,fa_group_asset_default gad
              ,fa_deprn_periods       dp
         WHERE dd.book_type_code = p_book
           AND dd.asset_id = gar.group_asset_id
           AND gad.super_group_id IS NULL
           AND -- MPOWELL
               gar.book_type_code = dd.book_type_code
           AND gad.book_type_code = gar.book_type_code
           AND gad.group_asset_id = gar.group_asset_id
           AND dd.period_counter =
               (SELECT MAX(dd_sub.period_counter)
                  FROM fa_deprn_detail dd_sub
                 WHERE dd_sub.book_type_code = p_book
                   AND dd_sub.asset_id = gar.group_asset_id
                   AND dd_sub.period_counter <= v_upc)
           AND dd.period_counter = dp.period_counter
           AND dd.book_type_code = dp.book_type_code
           AND gar.date_effective <= dp.calendar_period_close_date
           AND -- mwoodwar
               nvl(gar.date_ineffective, (dp.calendar_period_close_date + 1)) >
               dp.calendar_period_close_date; -- mwoodwar
    
    END IF; -- IF (v_reporting_flag = 'R') THEN
  
    -- Insert the SuperGroup Depreciation Details    MPOWELL
    l_location := 'Inserting the SuperGroup Depreciation Details';
  
    IF (v_reporting_flag = 'R') THEN
      INSERT INTO xxcus.xxcusfa_reserve_ledger_tbl
        (asset_id
        ,dh_ccid
        ,deprn_reserve_acct
        ,date_placed_in_service
        ,method_code
        ,life
        ,rate
        ,capacity
        ,cost
        ,deprn_amount
        ,ytd_deprn
        ,deprn_reserve
        ,percent
        ,transaction_type
        ,period_counter
        ,date_effective
        ,units_assigned
        ,location_id
        ,remaining_life1
        ,adjustment_amount
        ,original_cost--TMS#20151015-00139 change for ver 1.3
        ,original_units)--TMS#20151015-00139 change for ver 1.3
        SELECT gar.group_asset_id asset_id
              ,gad.deprn_expense_acct_ccid dh_ccid
              ,gad.deprn_reserve_acct_ccid rsv_account
              ,gar.deprn_start_date start_date
              ,sgr.deprn_method_code method
              , -- MPOWELL
               gar.life_in_months life
              ,sgr.adjusted_rate rate
              , -- MPOWELL
               gar.production_capacity capacity
              ,dd.adjusted_cost cost
              ,decode(dd.period_counter, v_upc, dd.deprn_amount, 0) deprn_amount
              ,decode(sign(v_tpc - dd.period_counter), 1, 0, dd.ytd_deprn) ytd_deprn
              ,dd.deprn_reserve deprn_reserve
              ,100 percent
              ,'G' t_type
              ,dd.period_counter
              ,v_ucd
              ,NULL
              ,NULL
              ,NULL
              ,dd.deprn_adjustment_amount adjusted_amount
              ,NULL--TMS#20151015-00139 change for ver 1.3
              ,NULL--TMS#20151015-00139 change for ver 1.3
          FROM fa_deprn_summary_mrc_v dd
              ,fa_group_asset_rules   gar
              ,fa_group_asset_default gad
              ,fa_super_group_rules   sgr
              ,fa_deprn_periods_mrc_v dp
         WHERE dd.book_type_code = p_book
           AND dd.asset_id = gar.group_asset_id
           AND gar.book_type_code = dd.book_type_code
           AND gad.super_group_id = sgr.super_group_id
           AND -- MPOWELL
               gad.book_type_code = sgr.book_type_code
           AND -- MPOWELL
               gad.book_type_code = gar.book_type_code
           AND gad.group_asset_id = gar.group_asset_id
           AND dd.period_counter =
               (SELECT MAX(dd_sub.period_counter)
                  FROM fa_deprn_detail_mrc_v dd_sub
                 WHERE dd_sub.book_type_code = p_book
                   AND dd_sub.asset_id = gar.group_asset_id
                   AND dd_sub.period_counter <= v_upc)
           AND dd.period_counter = dp.period_counter
           AND dd.book_type_code = dp.book_type_code
           AND gar.date_effective <= dp.calendar_period_close_date
           AND nvl(gar.date_ineffective, (dp.calendar_period_close_date + 1)) >
               dp.calendar_period_close_date
           AND sgr.date_effective <= dp.calendar_period_close_date
           AND nvl(sgr.date_ineffective, (dp.calendar_period_close_date + 1)) >
               dp.calendar_period_close_date;
    ELSE
      INSERT INTO xxcus.xxcusfa_reserve_ledger_tbl
        (asset_id
        ,dh_ccid
        ,deprn_reserve_acct
        ,date_placed_in_service
        ,method_code
        ,life
        ,rate
        ,capacity
        ,cost
        ,deprn_amount
        ,ytd_deprn
        ,deprn_reserve
        ,percent
        ,transaction_type
        ,period_counter
        ,date_effective
        ,units_assigned
        ,location_id
        ,remaining_life1
        ,adjustment_amount
        ,original_cost --TMS#20151015-00139 change for ver 1.3
        ,original_units)--TMS#20151015-00139 change for ver 1.3
        SELECT gar.group_asset_id asset_id
              ,gad.deprn_expense_acct_ccid dh_ccid
              ,gad.deprn_reserve_acct_ccid rsv_account
              ,gar.deprn_start_date start_date
              ,sgr.deprn_method_code method
              , -- MPOWELL
               gar.life_in_months life
              ,sgr.adjusted_rate rate
              , -- MPOWELL
               gar.production_capacity capacity
              ,dd.adjusted_cost cost
              ,decode(dd.period_counter, v_upc, dd.deprn_amount, 0) deprn_amount
              ,decode(sign(v_tpc - dd.period_counter), 1, 0, dd.ytd_deprn) ytd_deprn
              ,dd.deprn_reserve deprn_reserve
              ,100 percent
              ,'G' t_type
              ,dd.period_counter
              ,v_ucd
              ,NULL
              ,NULL
              ,NULL
              ,dd.deprn_adjustment_amount adjusted_amount
              ,NULL--TMS#20151015-00139 change for ver 1.3
              ,NULL --TMS#20151015-00139 change for ver 1.3
          FROM fa_deprn_summary       dd
              ,fa_group_asset_rules   gar
              ,fa_group_asset_default gad
              ,fa_super_group_rules   sgr
              ,fa_deprn_periods       dp
         WHERE dd.book_type_code = p_book
           AND dd.asset_id = gar.group_asset_id
           AND gar.book_type_code = dd.book_type_code
           AND gad.super_group_id = sgr.super_group_id
           AND -- MPOWELL
               gad.book_type_code = sgr.book_type_code
           AND -- MPOWELL
               gad.book_type_code = gar.book_type_code
           AND gad.group_asset_id = gar.group_asset_id
           AND dd.period_counter =
               (SELECT MAX(dd_sub.period_counter)
                  FROM fa_deprn_detail dd_sub
                 WHERE dd_sub.book_type_code = p_book
                   AND dd_sub.asset_id = gar.group_asset_id
                   AND dd_sub.period_counter <= v_upc)
           AND dd.period_counter = dp.period_counter
           AND dd.book_type_code = dp.book_type_code
           AND gar.date_effective <= dp.calendar_period_close_date
           AND nvl(gar.date_ineffective, (dp.calendar_period_close_date + 1)) >
               dp.calendar_period_close_date
           AND sgr.date_effective <= dp.calendar_period_close_date
           AND nvl(sgr.date_ineffective, (dp.calendar_period_close_date + 1)) >
               dp.calendar_period_close_date;
    END IF;
  
    -- Selecting the required set of data from Oracle Assets  DFF's
    l_location := 'Selecting the required set of data from Oracle Assets  DFFs';
    IF p_book LIKE 'HDS LITEMOR%' THEN
      fa_detail_select_litemor(p_book, p_history,p_period);--TMS#20151015-00139 change for ver 1.3
    ELSE
      fa_detail_select(p_book, p_history,p_period);--TMS#20151015-00139 change for ver 1.3
    END IF;
    l_location := 'End of Select';
  EXCEPTION
    WHEN OTHERS THEN
      p_retcode := 900021;
      p_errbuf  := 'XXCUSFA: ' || p_retcode || ': Error During extract in ' ||
                   l_procedure_name || ' at ' || l_location || ' - ' ||
                   substr(SQLERRM, 1, 1900);
      fnd_file.put_line(fnd_file.log, p_errbuf);
      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_procedure_name,
                                           p_calling => 'Exception for XXCUSFA_DETAIL_EXTRACT_PKG',
                                           p_request_id => l_req_id,
                                           p_ora_error_msg => p_retcode,
                                           p_error_desc => p_errbuf,
                                           p_distribution_list => l_distro_list,
                                           p_module => 'XXUCS');
    
  END fa_detail_extract;

  /*------------------------< PROCEDURE fa_detail_select >-----------------------------------------
  
        Version      Date         Developer          Description
  ================================================================================================= 
  --    1.1          06/11/2011   Luong Vu           SR 133808 - Add Adjusted Amount and Remaining 
  --                                                             Life. add the filter base on the 
  --                                                             TRANSACTION_TYPE field
  --    1.2          09/25/2012   Luong Vu                     - Remove the fields in 1.1 and move 
  --                                                             expense field after cost center.
  --    1.3          07/01/2016  Neha Saini            look changes for TMS#20151015-00139  
   ------------------------< PROCEDURE fa_detail_select >----------------------------------------*/

  PROCEDURE fa_detail_select(p_book    IN VARCHAR2
                            ,p_history IN VARCHAR2
                            ,p_period  IN VARCHAR2) IS --TMS#20151015-00139 change for ver 1.3
  
    CURSOR fa_detail_cur IS
      SELECT dp.book_type_code                 "BOOK"
             ,dp.fiscal_year                    "FISCAL_YEAR"
             ,dp.period_name                    "PERIOD"
             ,dp.period_counter                 "PERIOD_COUNTER"
             ,rsv_ldg_gt.units_assigned         "UNITS_ASSIGNED"
             ,fa.asset_number                   "ASSET_NUMBER"
             ,fa.asset_id                       "ASSET_ID"
             ,loc.segment1                      "HOME_BRANCH"
             ,loc.segment2                      "CUSTODIAN_BRANCH"
             ,loc.segment3                      "LEGAL_ENTITY"
             ,loc.segment4                      "STATE"
             ,loc.segment5                      "CITY"
             ,cc.segment1                       "PROD"
             ,cc.segment2                       "LOCATION"
             ,cc.segment3                       "COST_CENTER"
             ,cc.segment5                       "PROJECT"
             ,cc.segment6                       "FUTURE_USE_1"
             ,cc.segment7                       "FUTURE_USE_2"
             ,fcb.deprn_expense_acct            "EXPENSE_ACCOUNT"
             ,rsv_ldg_gt.deprn_reserve_acct     "RESERVE_ACCOUNT"
             ,fa.description                    "FA_DESCRIPTION"
             ,cb.segment1                       "MAJOR_CATEGORY"
             ,cb.segment2                       "MINOR_CATEGORY"
             ,fa.inventorial                    "PHYSICAL_INV"
             ,fa.current_units                  "CURRENT_UNITS" --TMS#20151015-00139 change for ver 1.3
             ,rsv_ldg_gt.original_units         "ORIGINAL_UNITS" --TMS#20151015-00139 change for ver 1.3
             ,fa.serial_number                  "SERIAL_NUMBER"
             ,fa.tag_number                     "TAG_NUMBER"
             ,rsv_ldg_gt.date_placed_in_service "DPS"
             ,rsv_ldg_gt.method_code            "DEPR_METHOD"
             ,rsv_ldg_gt.life                   "LIFE"
             ,rsv_ldg_gt.remaining_life1        "REMAINING_LIFE"
             ,rsv_ldg_gt.cost                   "CURRENT_COST"--TMS#20151015-00139 change for ver 1.3
             ,rsv_ldg_gt.original_cost          "ORIGINAL_COST"--TMS#20151015-00139 change for ver 1.3
             ,rsv_ldg_gt.deprn_amount           "DEPRN_AMOUNT"
             ,rsv_ldg_gt.ytd_deprn              "YTD_DEPRN"
             ,rsv_ldg_gt.deprn_reserve          "DEPRN_RESERVE"
             ,rsv_ldg_gt.adjustment_amount      "ADJUSTED_AMOUNT"
             ,fcb.asset_cost_acct               "COST_ACCOUNT"
             ,rsv_ldg_gt.transaction_type       "STATUS"--TMS#20151015-00139 change for ver 1.3
             ,fa.attribute1
             ,fa.attribute2
             ,fa.attribute3
             ,fa.attribute4
             ,fa.attribute5
             ,fa.attribute6
             ,fa.attribute7
             ,fa.attribute8
        FROM xxcus.xxcusfa_reserve_ledger_tbl rsv_ldg_gt
            ,apps.fa_additions                fa
            ,gl_code_combinations             cc
            ,fa_locations                     loc
            ,fa.fa_deprn_periods              dp
            ,fa.fa_category_books             fcb
            ,fa.fa_categories_b               cb
      --FA.FA_ASSET_INVOICES             FAI
       WHERE rsv_ldg_gt.asset_id = fa.asset_id
            --AND RSV_LDG_GT.ASSET_ID = FAI.ASSET_ID
         AND rsv_ldg_gt.dh_ccid = cc.code_combination_id
         AND rsv_ldg_gt.location_id = loc.location_id
         AND rsv_ldg_gt.period_counter = dp.period_counter
         AND rsv_ldg_gt.deprn_reserve_acct = fcb.deprn_reserve_acct
         AND cb.category_id = fa.asset_category_id
         AND dp.book_type_code = p_book
         AND dp.period_name = p_period--TMS#20151015-00139 change for ver 1.3
         AND fcb.book_type_code = dp.book_type_code
         AND cb.category_id = fcb.category_id
       ORDER BY cc.segment2
               ,fa.asset_number;
  
    CURSOR fa_detail_cur_nohist IS
      SELECT dp.book_type_code                 "BOOK"
             ,dp.fiscal_year                    "FISCAL_YEAR"
             ,dp.period_name                    "PERIOD"
             ,dp.period_counter                 "PERIOD_COUNTER"
             ,rsv_ldg_gt.units_assigned         "UNITS_ASSIGNED"
             ,fa.asset_number                   "ASSET_NUMBER"
             ,fa.asset_id                       "ASSET_ID"
             ,loc.segment1                      "HOME_BRANCH"
             ,loc.segment2                      "CUSTODIAN_BRANCH"
             ,loc.segment3                      "LEGAL_ENTITY"
             ,loc.segment4                      "STATE"
             ,loc.segment5                      "CITY"
             ,cc.segment1                       "PROD"
             ,cc.segment2                       "LOCATION"
             ,cc.segment3                       "COST_CENTER"
             ,cc.segment5                       "PROJECT"
             ,cc.segment6                       "FUTURE_USE_1"
             ,cc.segment7                       "FUTURE_USE_2"
             ,fcb.deprn_expense_acct            "EXPENSE_ACCOUNT"
             ,rsv_ldg_gt.deprn_reserve_acct     "RESERVE_ACCOUNT"
             ,fa.description                    "FA_DESCRIPTION"
             ,cb.segment1                       "MAJOR_CATEGORY"
             ,cb.segment2                       "MINOR_CATEGORY"
             ,fa.inventorial                    "PHYSICAL_INV"
             ,fa.current_units                  "CURRENT_UNITS"--TMS#20151015-00139 change for ver 1.3
             ,rsv_ldg_gt.original_units         "ORIGINAL_UNITS" --TMS#20151015-00139 change for ver 1.3             
             ,fa.serial_number                  "SERIAL_NUMBER"
             ,fa.tag_number                     "TAG_NUMBER"
             ,rsv_ldg_gt.date_placed_in_service "DPS"
             ,rsv_ldg_gt.method_code            "DEPR_METHOD"
             ,rsv_ldg_gt.life                   "LIFE"
             ,rsv_ldg_gt.remaining_life1        "REMAINING_LIFE"
             ,rsv_ldg_gt.cost                   "CURRENT_COST"--TMS#20151015-00139 change for ver 1.3
             ,rsv_ldg_gt.original_cost          "ORIGINAL_COST"--TMS#20151015-00139 change for ver 1.3
             ,rsv_ldg_gt.deprn_amount           "DEPRN_AMOUNT"
             ,rsv_ldg_gt.ytd_deprn              "YTD_DEPRN"
             ,rsv_ldg_gt.deprn_reserve          "DEPRN_RESERVE"
             ,rsv_ldg_gt.adjustment_amount      "ADJUSTED_AMOUNT"
             ,fcb.asset_cost_acct               "COST_ACCOUNT"
             ,rsv_ldg_gt.transaction_type       "STATUS"--TMS#20151015-00139 change for ver 1.3
             ,fa.attribute1
             ,fa.attribute2
             ,fa.attribute3
             ,fa.attribute4
             ,fa.attribute5
             ,fa.attribute6
             ,fa.attribute7
             ,fa.attribute8
        FROM xxcus.xxcusfa_reserve_ledger_tbl rsv_ldg_gt
            ,apps.fa_additions                fa
            ,gl_code_combinations             cc
            ,fa_locations                     loc
            ,fa.fa_deprn_periods              dp
            ,fa.fa_category_books             fcb
            ,fa.fa_categories_b               cb
      --FA.FA_ASSET_INVOICES             FAI
       WHERE rsv_ldg_gt.asset_id = fa.asset_id
            --AND RSV_LDG_GT.ASSET_ID = FAI.ASSET_ID
         AND rsv_ldg_gt.dh_ccid = cc.code_combination_id
         AND rsv_ldg_gt.location_id = loc.location_id
         AND rsv_ldg_gt.period_counter = dp.period_counter
         AND rsv_ldg_gt.deprn_reserve_acct = fcb.deprn_reserve_acct
         AND cb.category_id = fa.asset_category_id
         AND dp.book_type_code = p_book
         AND dp.period_name = p_period--TMS#20151015-00139 change for ver 1.3
         AND fcb.book_type_code = dp.book_type_code
         AND cb.category_id = fcb.category_id
         AND (rsv_ldg_gt.transaction_type IS NULL OR
             rsv_ldg_gt.transaction_type = 'N')
       ORDER BY cc.segment2
               ,fa.asset_number;
  
    -- Variable declaration
  
    fa_detail_rec    fa_detail_cur%ROWTYPE;
    v_output         VARCHAR2(2000);
    l_location       VARCHAR2(200);
    l_procedure_name VARCHAR2(75) := 'XXCUSFA_DETAIL_EXTRACT_PKG.FA_DETAIL_SELECT';
    l_distro_list    VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
    l_req_id         NUMBER := fnd_global.conc_request_id;
    l_retcode        NUMBER;
    l_errbuf         VARCHAR2(2000);
    l_inv_info       VARCHAR2(3000);
  
  BEGIN
    v_output := 'BOOK|FISCAL YEAR|PERIOD|PERIOD_COUNTER|UNITS ASSIGNED|ASSET NUMBER|HOME BRANCH|CUSTODIAN BRANCH|LEGAL ENTITY|STATE|CITY|PROD|LOCATION|COST CENTER|EXPENSE ACCOUNT|PROJECT|FUTURE USE 1|FUTURE USE 2|RESERVE ACCOUNT|FA DESCRIPTION|MAJOR CATEGORY|MINOR CATEGORY|PHYSICAL INV|CURRENT UNITS|ORIGINAL UNITS|SERIAL NUMBER|TAG NUMBER|DPS|DEPR METHOD|Life Yr. Mo|Current Cost|Original Cost|DEPRN AMOUNT|YTD DEPRN|DEPRN RESERVE|ATTRIBUTE1|ATTRIBUTE2|ATTRIBUTE3|ATTRIBUTE4|ATTRIBUTE5|ATTRIBUTE6|ATTRIBUTE7|ATTRIBUTE8|COST ACCOUNT|STATUS|PO-INVOICE NUMBER(S)';--TMS#20151015-00139 change for ver 1.3
    fnd_file.put_line(fnd_file.output, v_output);
  
    IF p_history = 'No' THEN
      FOR fa_detail_rec IN fa_detail_cur_nohist
      LOOP
        l_inv_info := fa_ast_inv_info(fa_detail_rec.asset_id);
        v_output   := fa_detail_rec.book || '|' ||
                      fa_detail_rec.fiscal_year || '|' ||
                      fa_detail_rec.period || '|';
        v_output   := v_output || fa_detail_rec.period_counter || '|' ||
                      fa_detail_rec.units_assigned || '|' ||
                      fa_detail_rec.asset_number || '|';
        v_output   := v_output || fa_detail_rec.home_branch || '|' ||
                      fa_detail_rec.custodian_branch || '|' ||
                      fa_detail_rec.legal_entity || '|';
        v_output   := v_output || fa_detail_rec.state || '|' ||
                      fa_detail_rec.city || '|' || fa_detail_rec.prod || '|';
        v_output   := v_output || fa_detail_rec.location || '|' ||
                      fa_detail_rec.cost_center || '|' ||
                      fa_detail_rec.expense_account || '|' ||
                      fa_detail_rec.project || '|' ||
                      fa_detail_rec.future_use_1 || '|' ||
                      fa_detail_rec.future_use_2 || '|';
        v_output   := v_output || fa_detail_rec.reserve_account || '|' ||
                      fa_detail_rec.fa_description || '|' ||
                      fa_detail_rec.major_category || '|';
        v_output   := v_output || fa_detail_rec.minor_category || '|' ||
                      fa_detail_rec.physical_inv || '|' ||
                      fa_detail_rec.current_units || '|'|| fa_detail_rec.original_units ||'|';--TMS#20151015-00139 change for ver 1.3
        v_output   := v_output || fa_detail_rec.serial_number || '|' ||
                      fa_detail_rec.tag_number || '|' || fa_detail_rec.dps || '|';
        v_output   := v_output || fa_detail_rec.depr_method || '|' ||
                      fa_detail_rec.life || '|' ||
                     --fa_detail_rec.remaining_life || '|' ||     --v1.2
                      fa_detail_rec.current_cost || '|'||fa_detail_rec.original_cost||'|';--TMS#20151015-00139 change for ver 1.3
        v_output   := v_output || fa_detail_rec.deprn_amount || '|' ||
                      fa_detail_rec.ytd_deprn || '|' ||
                      fa_detail_rec.deprn_reserve || '|'
        --fa_detail_rec.adjusted_amount || '|'        --v1.2
         ;
        v_output   := v_output || fa_detail_rec.attribute1 || '|' ||
                      fa_detail_rec.attribute2 || '|' ||
                      fa_detail_rec.attribute3 || '|' ||
                      fa_detail_rec.attribute4 || '|' ||
                      fa_detail_rec.attribute5 || '|';
        v_output   := v_output || fa_detail_rec.attribute6 || '|' ||
                      fa_detail_rec.attribute7 || '|' ||
                      fa_detail_rec.attribute8 || '|' ||
                      fa_detail_rec.cost_account || '|' ||fa_detail_rec.status || '|'|| l_inv_info;--TMS#20151015-00139 change for ver 1.3
        fnd_file.put_line(fnd_file.output, v_output);
      END LOOP;
    
    ELSE
    
      FOR fa_detail_rec IN fa_detail_cur
      LOOP
        l_inv_info := fa_ast_inv_info(fa_detail_rec.asset_id);
        v_output   := fa_detail_rec.book || '|' ||
                      fa_detail_rec.fiscal_year || '|' ||
                      fa_detail_rec.period || '|';
        v_output   := v_output || fa_detail_rec.period_counter || '|' ||
                      fa_detail_rec.units_assigned || '|' ||
                      fa_detail_rec.asset_number || '|';
        v_output   := v_output || fa_detail_rec.home_branch || '|' ||
                      fa_detail_rec.custodian_branch || '|' ||
                      fa_detail_rec.legal_entity || '|';
        v_output   := v_output || fa_detail_rec.state || '|' ||
                      fa_detail_rec.city || '|' || fa_detail_rec.prod || '|';
        v_output   := v_output || fa_detail_rec.location || '|' ||
                      fa_detail_rec.cost_center || '|' ||
                      fa_detail_rec.expense_account || '|' ||
                      fa_detail_rec.project || '|' ||
                      fa_detail_rec.future_use_1 || '|' ||
                      fa_detail_rec.future_use_2 || '|';
        v_output   := v_output || fa_detail_rec.reserve_account || '|' ||
                      fa_detail_rec.fa_description || '|' ||
                      fa_detail_rec.major_category || '|';
        v_output   := v_output || fa_detail_rec.minor_category || '|' ||
                      fa_detail_rec.physical_inv || '|' ||
                      fa_detail_rec.current_units || '|'||fa_detail_rec.original_units || '|';--TMS#20151015-00139 change for ver 1.3
        v_output   := v_output || fa_detail_rec.serial_number || '|' ||
                      fa_detail_rec.tag_number || '|' || fa_detail_rec.dps || '|';
        v_output   := v_output || fa_detail_rec.depr_method || '|' ||
                      fa_detail_rec.life || '|' ||
                     --fa_detail_rec.remaining_life || '|' ||
                      fa_detail_rec.current_cost || '|'||fa_detail_rec.original_cost || '|';--TMS#20151015-00139 change for ver 1.3
        v_output   := v_output || fa_detail_rec.deprn_amount || '|' ||
                      fa_detail_rec.ytd_deprn || '|' ||
                      fa_detail_rec.deprn_reserve || '|'
        --fa_detail_rec.adjusted_amount || '|'
         ;
        v_output   := v_output || fa_detail_rec.attribute1 || '|' ||
                      fa_detail_rec.attribute2 || '|' ||
                      fa_detail_rec.attribute3 || '|' ||
                      fa_detail_rec.attribute4 || '|' ||
                      fa_detail_rec.attribute5 || '|';
        v_output   := v_output || fa_detail_rec.attribute6 || '|' ||
                      fa_detail_rec.attribute7 || '|' ||
                      fa_detail_rec.attribute8 || '|' ||
                      fa_detail_rec.cost_account || '|' ||fa_detail_rec.status || '|'|| l_inv_info;--TMS#20151015-00139 change for ver 1.3
        fnd_file.put_line(fnd_file.output, v_output);
      END LOOP;
    END IF;
  
  EXCEPTION
    WHEN OTHERS THEN
      l_retcode := 900022;
      l_errbuf  := 'XXCUSFA: ' || l_retcode || ': Error During extract in ' ||
                   l_procedure_name || ' at ' || l_location || ' - ' ||
                   substr(SQLERRM, 1, 1900);
      fnd_file.put_line(fnd_file.log, l_errbuf);
      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_procedure_name,
                                           p_calling => 'Exception for XXCUSFA_DETAIL_EXTRACT_PKG',
                                           p_request_id => l_req_id,
                                           p_ora_error_msg => l_retcode,
                                           p_error_desc => l_errbuf,
                                           p_distribution_list => l_distro_list,
                                           p_module => 'XXUCS');
  END fa_detail_select;


  FUNCTION fa_ast_inv_info(p_asset_id IN NUMBER) RETURN VARCHAR2 IS
    l_location       VARCHAR2(200);
    l_procedure_name VARCHAR2(75) := 'XXCUSFA_DETAIL_EXTRACT_PKG.FA_AST_INV_INFO';
    l_distro_list    VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
    l_req_id         NUMBER := fnd_global.conc_request_id;
    l_retcode        NUMBER;
    l_errbuf         VARCHAR2(2000);
    l_inv_info       VARCHAR2(3000) := NULL;
  
    CURSOR ast_inv_info IS
      SELECT DISTINCT fai.invoice_number
                     ,fai.po_number
        FROM fa.fa_asset_invoices fai
       WHERE fai.asset_id = p_asset_id
       ORDER BY fai.invoice_number
               ,fai.po_number;
  BEGIN
    l_inv_info := NULL;
    FOR i IN ast_inv_info
    LOOP
      l_inv_info := l_inv_info || '|' || i.po_number || '-' ||
                    i.invoice_number;
    END LOOP;
    RETURN rtrim(ltrim(l_inv_info, '|'), '-');
  EXCEPTION
    WHEN OTHERS THEN
      l_retcode := 900023;
      l_errbuf  := 'XXCUSFA: ' || l_retcode || ': Error During extract in ' ||
                   l_procedure_name || ' at ' || l_location || ' - ' ||
                   substr(SQLERRM, 1, 1900);
      fnd_file.put_line(fnd_file.log, l_errbuf);
      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_procedure_name,
                                           p_calling => 'Exception for XXCUSFA_DETAIL_EXTRACT_PKG',
                                           p_request_id => l_req_id,
                                           p_ora_error_msg => l_retcode,
                                           p_error_desc => l_errbuf,
                                           p_distribution_list => l_distro_list,
                                           p_module => 'XXUCS');
  END fa_ast_inv_info;

  /*  ------------------------< PROCEDURE fa_detail_select_litemor >--------------------------------
    Purpose: Create FA Extract for Canada Litemor
  ==================================================================================================
    Version       Date          Developer         Description
  ==================================================================================================
    1.0           06/04/2012    Luong Vu          Canada Litemor FA Extract - SR 141582
    1.1           08/29/2012    Luong Vu          Add no history parameters 
                                                  Incident 133808.  And logic
    1.3           07/01/2016   Neha Saini            look changes for TMS#20151015-00139                                              
  ------------------------------------------------------------------------------------------------*/
  PROCEDURE fa_detail_select_litemor(p_book    IN VARCHAR2
                                    ,p_history IN VARCHAR2
                                    ,p_period  IN VARCHAR2) IS --TMS#20151015-00139 change for ver 1.3
  
    CURSOR fa_detail_cur IS
      SELECT dp.book_type_code         "BOOK"
             ,dp.fiscal_year            "FISCAL_YEAR"
             ,dp.period_name            "PERIOD"
             ,dp.period_counter         "PERIOD_COUNTER"
             ,rsv_ldg_gt.units_assigned "UNITS_ASSIGNED"
             ,fa.asset_number           "ASSET_NUMBER"
             ,fa.asset_id               "ASSET_ID"
             ,loc.segment1              "HOME_BRANCH"
             ,loc.segment2              "CUSTODIAN_BRANCH"
             ,loc.segment3              "LEGAL_ENTITY"
             ,loc.segment4              "STATE"
             ,loc.segment5              "CITY"
             ,cc.segment1               "PROD"
             ,cc.segment2               "DIVISION"
             ,cc.segment3               "DEPARTMENT"
             ,cc.segment4               "ACCOUNT"
             ,cc.segment5               "SUBACCOUNT"
             ,cc.segment6               "FUTURE_USE"
             -- ,fcb.deprn_expense_acct            "EXPENSE_ACCOUNT"
             ,rsv_ldg_gt.deprn_reserve_acct     "RESERVE_ACCOUNT"
             ,fa.description                    "FA_DESCRIPTION"
             ,cb.segment1                       "MAJOR_CATEGORY"
             ,cb.segment2                       "MINOR_CATEGORY"
             ,fa.inventorial                    "PHYSICAL_INV"
             ,fa.current_units                  "CURRENT_UNITS"--TMS#20151015-00139 change for ver 1.3
             ,rsv_ldg_gt.original_units         "ORIGINAL_UNITS" --TMS#20151015-00139 change for ver 1.3               
             ,fa.serial_number                  "SERIAL_NUMBER"
             ,fa.tag_number                     "TAG_NUMBER"
             ,rsv_ldg_gt.date_placed_in_service "DPS"
             ,rsv_ldg_gt.method_code            "DEPR_METHOD"
             ,rsv_ldg_gt.life                   "LIFE"
             ,rsv_ldg_gt.remaining_life1        "REMAINING_LIFE"
             ,rsv_ldg_gt.cost                   "CURRENT_COST"--TMS#20151015-00139 change for ver 1.3
             ,rsv_ldg_gt.original_cost          "ORIGINAL_COST"--TMS#20151015-00139 change for ver 1.3
             ,rsv_ldg_gt.deprn_amount           "DEPRN_AMOUNT"
             ,rsv_ldg_gt.ytd_deprn              "YTD_DEPRN"
             ,rsv_ldg_gt.deprn_reserve          "DEPRN_RESERVE"
             ,rsv_ldg_gt.adjustment_amount      "ADJUSTED_AMOUNT"
             ,fcb.asset_cost_acct               "COST_ACCOUNT"
             ,rsv_ldg_gt.transaction_type       "STATUS"--TMS#20151015-00139 change for ver 1.3
             ,fa.attribute1
             ,fa.attribute2
             ,fa.attribute3
             ,fa.attribute4
             ,fa.attribute5
             ,fa.attribute6
             ,fa.attribute7
             ,fa.attribute8
        FROM xxcus.xxcusfa_reserve_ledger_tbl rsv_ldg_gt
            ,apps.fa_additions                fa
            ,gl_code_combinations             cc
            ,fa_locations                     loc
            ,fa.fa_deprn_periods              dp
            ,fa.fa_category_books             fcb
            ,fa.fa_categories_b               cb
       WHERE rsv_ldg_gt.asset_id = fa.asset_id
         AND rsv_ldg_gt.dh_ccid = cc.code_combination_id
         AND rsv_ldg_gt.location_id = loc.location_id
         AND rsv_ldg_gt.period_counter = dp.period_counter
         AND rsv_ldg_gt.deprn_reserve_acct = fcb.deprn_reserve_acct
         AND cb.category_id = fa.asset_category_id
         AND dp.book_type_code = p_book
         AND dp.period_name = p_period--TMS#20151015-00139 change for ver 1.3
         AND fcb.book_type_code = dp.book_type_code
         AND cb.category_id = fcb.category_id
       ORDER BY cc.segment2
               ,fa.asset_number;
  
    CURSOR fa_detail_cur_nohist IS
      SELECT dp.book_type_code         "BOOK"
             ,dp.fiscal_year            "FISCAL_YEAR"
             ,dp.period_name            "PERIOD"
             ,dp.period_counter         "PERIOD_COUNTER"
             ,rsv_ldg_gt.units_assigned "UNITS_ASSIGNED"
             ,fa.asset_number           "ASSET_NUMBER"
             ,fa.asset_id               "ASSET_ID"
             ,loc.segment1              "HOME_BRANCH"
             ,loc.segment2              "CUSTODIAN_BRANCH"
             ,loc.segment3              "LEGAL_ENTITY"
             ,loc.segment4              "STATE"
             ,loc.segment5              "CITY"
             ,cc.segment1               "PROD"
             ,cc.segment2               "DIVISION"
             ,cc.segment3               "DEPARTMENT"
             ,cc.segment4               "ACCOUNT"
             ,cc.segment5               "SUBACCOUNT"
             ,cc.segment6               "FUTURE_USE"
             -- ,fcb.deprn_expense_acct            "EXPENSE_ACCOUNT"
             ,rsv_ldg_gt.deprn_reserve_acct     "RESERVE_ACCOUNT"
             ,fa.description                    "FA_DESCRIPTION"
             ,cb.segment1                       "MAJOR_CATEGORY"
             ,cb.segment2                       "MINOR_CATEGORY"
             ,fa.inventorial                    "PHYSICAL_INV"
             ,fa.current_units                  "CURRENT_UNITS"--TMS#20151015-00139 change for ver 1.3
             ,rsv_ldg_gt.original_units         "ORIGINAL_UNITS" --TMS#20151015-00139 change for ver 1.3               
             ,fa.serial_number                  "SERIAL_NUMBER"
             ,fa.tag_number                     "TAG_NUMBER"
             ,rsv_ldg_gt.date_placed_in_service "DPS"
             ,rsv_ldg_gt.method_code            "DEPR_METHOD"
             ,rsv_ldg_gt.life                   "LIFE"
             ,rsv_ldg_gt.remaining_life1        "REMAINING_LIFE"
             ,rsv_ldg_gt.cost                   "CURRENT_COST"--TMS#20151015-00139 change for ver 1.3
             ,rsv_ldg_gt.original_cost          "ORIGINAL_COST"--TMS#20151015-00139 change for ver 1.3
             ,rsv_ldg_gt.deprn_amount           "DEPRN_AMOUNT"
             ,rsv_ldg_gt.ytd_deprn              "YTD_DEPRN"
             ,rsv_ldg_gt.deprn_reserve          "DEPRN_RESERVE"
             ,rsv_ldg_gt.adjustment_amount      "ADJUSTED_AMOUNT"
             ,fcb.asset_cost_acct               "COST_ACCOUNT"
             ,rsv_ldg_gt.transaction_type       "STATUS"--TMS#20151015-00139 change for ver 1.3
             ,fa.attribute1
             ,fa.attribute2
             ,fa.attribute3
             ,fa.attribute4
             ,fa.attribute5
             ,fa.attribute6
             ,fa.attribute7
             ,fa.attribute8
        FROM xxcus.xxcusfa_reserve_ledger_tbl rsv_ldg_gt
            ,apps.fa_additions                fa
            ,gl_code_combinations             cc
            ,fa_locations                     loc
            ,fa.fa_deprn_periods              dp
            ,fa.fa_category_books             fcb
            ,fa.fa_categories_b               cb
       WHERE rsv_ldg_gt.asset_id = fa.asset_id
         AND rsv_ldg_gt.dh_ccid = cc.code_combination_id
         AND rsv_ldg_gt.location_id = loc.location_id
         AND rsv_ldg_gt.period_counter = dp.period_counter
         AND rsv_ldg_gt.deprn_reserve_acct = fcb.deprn_reserve_acct
         AND cb.category_id = fa.asset_category_id
         AND dp.book_type_code = p_book
         AND dp.period_name = p_period--TMS#20151015-00139 change for ver 1.3
         AND fcb.book_type_code = dp.book_type_code
         AND cb.category_id = fcb.category_id
         AND (rsv_ldg_gt.transaction_type IS NULL OR
             rsv_ldg_gt.transaction_type = 'N')
       ORDER BY cc.segment2
               ,fa.asset_number;
  
    -- Variable declaration
  
    fa_detail_rec    fa_detail_cur%ROWTYPE;
    v_output         VARCHAR2(2000);
    l_location       VARCHAR2(200);
    l_procedure_name VARCHAR2(75) := 'XXCUSFA_DETAIL_EXTRACT_PKG.FA_DETAIL_SELECT';
    l_distro_list    VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
    l_req_id         NUMBER := fnd_global.conc_request_id;
    l_retcode        NUMBER;
    l_errbuf         VARCHAR2(2000);
    l_inv_info       VARCHAR2(3000);
  
  BEGIN
    v_output := 'BOOK|FISCAL YEAR|PERIOD|PERIOD_COUNTER|UNITS ASSIGNED|ASSET NUMBER|HOME BRANCH|CUSTODIAN BRANCH|LEGAL ENTITY|STATE|CITY|PROD|DIVISION|DEPARTMENT|ACCOUNT|SUBACCOUNT|FUTURE USE|RESERVE ACCOUNT|FA DESCRIPTION|MAJOR CATEGORY|MINOR CATEGORY|PHYSICAL INV|CURRENT UNITS|ORIGINAL UNITS|SERIAL NUMBER|TAG NUMBER|DPS|DEPR METHOD|Life Yr. Mo|Current Cost|ORIGINAL COST|DEPRN AMOUNT|YTD DEPRN|DEPRN RESERVE|ATTRIBUTE1|ATTRIBUTE2|ATTRIBUTE3|ATTRIBUTE4|ATTRIBUTE5|ATTRIBUTE6|ATTRIBUTE7|ATTRIBUTE8|COST ACCOUNT|STATUS|PO-INVOICE NUMBER(S)';--TMS#20151015-00139 change for ver 1.3
    fnd_file.put_line(fnd_file.output, v_output);
  
    IF p_history = 'No' THEN
      FOR fa_detail_rec IN fa_detail_cur_nohist
      LOOP
        l_inv_info := fa_ast_inv_info(fa_detail_rec.asset_id);
        v_output   := fa_detail_rec.book || '|' ||
                      fa_detail_rec.fiscal_year || '|' ||
                      fa_detail_rec.period || '|';
        v_output   := v_output || fa_detail_rec.period_counter || '|' ||
                      fa_detail_rec.units_assigned || '|' ||
                      fa_detail_rec.asset_number || '|';
        v_output   := v_output || fa_detail_rec.home_branch || '|' ||
                      fa_detail_rec.custodian_branch || '|' ||
                      fa_detail_rec.legal_entity || '|';
        v_output   := v_output || fa_detail_rec.state || '|' ||
                      fa_detail_rec.city || '|' || fa_detail_rec.prod || '|';
        v_output   := v_output || fa_detail_rec.division || '|' ||
                      fa_detail_rec.department || '|' ||
                      fa_detail_rec.account || '|' ||
                      fa_detail_rec.subaccount || '|' ||
                      fa_detail_rec.future_use || '|' ||
                     --fa_detail_rec.expense_account || 
                      '|';
        v_output   := v_output || fa_detail_rec.reserve_account || '|' ||
                      fa_detail_rec.fa_description || '|' ||
                      fa_detail_rec.major_category || '|';
        v_output   := v_output || fa_detail_rec.minor_category || '|' ||
                      fa_detail_rec.physical_inv || '|' ||
                      fa_detail_rec.current_units || '|'||fa_detail_rec.original_units || '|';--TMS#20151015-00139 change for ver 1.3
        v_output   := v_output || fa_detail_rec.serial_number || '|' ||
                      fa_detail_rec.tag_number || '|' || fa_detail_rec.dps || '|';
        v_output   := v_output || fa_detail_rec.depr_method || '|' ||
                      fa_detail_rec.life || '|' ||
                     --fa_detail_rec.remaining_life || '|' ||
                      fa_detail_rec.current_cost || '|'||fa_detail_rec.original_cost || '|';--TMS#20151015-00139 change for ver 1.3
        v_output   := v_output || fa_detail_rec.deprn_amount || '|' ||
                      fa_detail_rec.ytd_deprn || '|' ||
                      fa_detail_rec.deprn_reserve || '|'
        --fa_detail_rec.adjusted_amount || '|'
         ;
        v_output   := v_output || fa_detail_rec.attribute1 || '|' ||
                      fa_detail_rec.attribute2 || '|' ||
                      fa_detail_rec.attribute3 || '|' ||
                      fa_detail_rec.attribute4 || '|' ||
                      fa_detail_rec.attribute5 || '|';
        v_output   := v_output || fa_detail_rec.attribute6 || '|' ||
                      fa_detail_rec.attribute7 || '|' ||
                      fa_detail_rec.attribute8 || '|' ||
                      fa_detail_rec.cost_account || '|' ||fa_detail_rec.status || '|'|| l_inv_info;--TMS#20151015-00139 change for ver 1.3
        fnd_file.put_line(fnd_file.output, v_output);
      END LOOP;
    
    ELSE
    
      FOR fa_detail_rec IN fa_detail_cur
      LOOP
        l_inv_info := fa_ast_inv_info(fa_detail_rec.asset_id);
        v_output   := fa_detail_rec.book || '|' ||
                      fa_detail_rec.fiscal_year || '|' ||
                      fa_detail_rec.period || '|';
        v_output   := v_output || fa_detail_rec.period_counter || '|' ||
                      fa_detail_rec.units_assigned || '|' ||
                      fa_detail_rec.asset_number || '|';
        v_output   := v_output || fa_detail_rec.home_branch || '|' ||
                      fa_detail_rec.custodian_branch || '|' ||
                      fa_detail_rec.legal_entity || '|';
        v_output   := v_output || fa_detail_rec.state || '|' ||
                      fa_detail_rec.city || '|' || fa_detail_rec.prod || '|';
        v_output   := v_output || fa_detail_rec.division || '|' ||
                      fa_detail_rec.department || '|' ||
                      fa_detail_rec.account || '|' ||
                      fa_detail_rec.subaccount || '|' ||
                      fa_detail_rec.future_use || '|' ||
                     --fa_detail_rec.expense_account || 
                      '|';
        v_output   := v_output || fa_detail_rec.reserve_account || '|' ||
                      fa_detail_rec.fa_description || '|' ||
                      fa_detail_rec.major_category || '|';
        v_output   := v_output || fa_detail_rec.minor_category || '|' ||
                      fa_detail_rec.physical_inv || '|' ||
                      fa_detail_rec.current_units || '|'||fa_detail_rec.original_units || '|';--TMS#20151015-00139 change for ver 1.3
        v_output   := v_output || fa_detail_rec.serial_number || '|' ||
                      fa_detail_rec.tag_number || '|' || fa_detail_rec.dps || '|';
        v_output   := v_output || fa_detail_rec.depr_method || '|' ||
                      fa_detail_rec.life || '|' ||
                     --fa_detail_rec.remaining_life || '|' ||
                      fa_detail_rec.current_cost || '|'||fa_detail_rec.original_cost || '|';--TMS#20151015-00139 change for ver 1.3
        v_output   := v_output || fa_detail_rec.deprn_amount || '|' ||
                      fa_detail_rec.ytd_deprn || '|' ||
                      fa_detail_rec.deprn_reserve || '|'
        --fa_detail_rec.adjusted_amount || '|'
         ;
        v_output   := v_output || fa_detail_rec.attribute1 || '|' ||
                      fa_detail_rec.attribute2 || '|' ||
                      fa_detail_rec.attribute3 || '|' ||
                      fa_detail_rec.attribute4 || '|' ||
                      fa_detail_rec.attribute5 || '|';
        v_output   := v_output || fa_detail_rec.attribute6 || '|' ||
                      fa_detail_rec.attribute7 || '|' ||
                      fa_detail_rec.attribute8 || '|' ||
                      fa_detail_rec.cost_account || '|' ||fa_detail_rec.status || '|'|| l_inv_info;--TMS#20151015-00139 change for ver 1.3
        fnd_file.put_line(fnd_file.output, v_output);
      END LOOP;
    END IF;
  
  EXCEPTION
    WHEN OTHERS THEN
      l_retcode := 900022;
      l_errbuf  := 'XXCUSFA: ' || l_retcode || ': Error During extract in ' ||
                   l_procedure_name || ' at ' || l_location || ' - ' ||
                   substr(SQLERRM, 1, 1900);
      fnd_file.put_line(fnd_file.log, l_errbuf);
      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_procedure_name,
                                           p_calling => 'Exception for XXCUSFA_DETAIL_EXTRACT_PKG',
                                           p_request_id => l_req_id,
                                           p_ora_error_msg => l_retcode,
                                           p_error_desc => l_errbuf,
                                           p_distribution_list => l_distro_list,
                                           p_module => 'FA');
  END fa_detail_select_litemor;
END xxcusfa_detail_extract_pkg;
/