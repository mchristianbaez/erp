/*************************************************************************
  $Header TMS_20170111-00137_Data_Fix_AP_Inv_and_pymts_wrong_validts.sql $
  Module Name: TMS_20170111-00137  Data Fix script 

  PURPOSE: Data Fix script 

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        13-JAN-2017  Pattabhi Avula        TMS#20170111-00137 

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000;
DECLARE
l_sql1      VARCHAR2 (32000);
l_sql2      VARCHAR2 (32000);
l_sql3      VARCHAR2 (32000);
l_cnt       NUMBER;
l_cnt1      NUMBER;
l_tab_cnt   NUMBER:=0;
l_tab_cnt1  NUMBER:=0;

-- API Local Variables

l_file_location        VARCHAR2(1000);
l_bug_no               NUMBER :=312922705991;
l_user_name            FND_USER.USER_NAME%TYPE := '10012196';
l_resp_name            FND_RESPONSIBILITY_TL.RESPONSIBILITY_NAME%TYPE := 'HDS Payables Analyst - WC';
l_driver_table         ALL_TABLES.TABLE_NAME%TYPE := 'ap_undo_checks';
l_calling_sequence     VARCHAR2(4000);
l_source_type          VARCHAR2(20) := 'AP_PAYMENTS';
l_del_adj              BOOLEAN := FALSE;
l_org_id               NUMBER;

BEGIN
 
    BEGIN	  
      EXECUTE IMMEDIATE 'DROP TABLE AP_UNDO_INVOICES';
      DBMS_OUTPUT.put_line ('TMS: 20170111-00137    , AP_UNDO_INVOICES Table Dropped');
    EXCEPTION
     WHEN OTHERS
    THEN
      DBMS_OUTPUT.PUT_LINE ('TMS: 20170111-00137 , Errors while Droping the tables, Error is : ' || SQLERRM);  
    END;
	
	BEGIN      
      EXECUTE IMMEDIATE 'DROP TABLE AP_UNDO_CHECKS';
      DBMS_OUTPUT.put_line ('TMS: 20170111-00137    , AP_UNDO_CHECKS Table Dropped');
    EXCEPTION
     WHEN OTHERS
    THEN
      DBMS_OUTPUT.PUT_LINE ('TMS: 20170111-00137 , Errors while Droping the AP_UNDO_CHECKS, Error is : ' || SQLERRM);  
    END;
    
     BEGIN
	      DBMS_OUTPUT.put_line ('TMS: 20170111-00137 , Before Creating the AP_UNDO_INVOICES table');
	  l_sql1:='CREATE TABLE ap_undo_invoices AS 
                         SELECT DISTINCT aid.invoice_id, 
						       aid.accounting_event_id event_id, 
                               TO_DATE(SYSDATE, ''DD-MON-YYYY'') proposed_undo_date, 
                               ''Y'' Process_flag 
                         FROM   ap_invoice_distributions_all aid 
                         WHERE  aid.invoice_id in(
                          12783273,
                           12128929,
                           11934447,
                           11934448,
                           11934449,
                           13108857,
                           12082166,
                           12082167,
                           10964250,
                           12908036,
                           12849874,
                           12331722,
                           12416550,
                           12752315,
                           13125194,
                           11941341,
                           13121320,
                           12526719,
                           12696224,
                           12770617,
                           12047718,
                           12921591,
                           10457326,
                           12470233,
                           7616511,
                           12077909,
                           12309978,
                           10390534,
                           12737848,
                           12061465,
                           12876096,
                           12597460,
                           12159435,
                           10629641,
                           12037485,
                           13147146,
                           12001607,
                           8977302,
                           10629394,
                           12378770,
                           11981187,
                           12003191,
                           12680172,
                           12378160,
                           12314747,
                           12660368,
                           12956824,
                           12750549,
                           12726364,
                           10389665,
                           12409872,
                           12596206,
                           7616511,
                           11714763,
                           12804449,
                           12213548,
                           9273273,
                           10817163,
                           12606036,
                           13688892,
                           13583314,
                           13643960,
                           13599047,
                           13599046,
                           13599045,
                           13644469,
                           13609342,
                           13488299,
                           13615333,
                           13641083,
                           13623732,
                           13623622,
                           13612077,
                           13612036,
                           13583520,
                           13592399,
                           13362014,
                           13393878,
                           13589148,
                           13465949,
                           13553137,
                           13609952,
                           13408226,
                           13531983,
                           13406040,
                           13477961,
                           13420396,
                           13420394,
                           13518088,
                           13518084,
                           13508976,
                           13408509,
                           13366963,
                           13355228,
                           13436347,
                           13451085,
                           13450108,
                           13442029,
                           13423054,
                           13442069,
                           13406687,
                           13436314,
                           13420484,
                           13449873,
                           13407985,
                           13413597,
                           13412439,
                           13406598,
                           13378049,
                           13393248,
                           13363175,
                           13305272,
                           13390532,
                           13387773,
                           13393293,
                           13346761,
                           13404897,
                           13381088,
                           13387774,
                           13412342,
                           13291395,
                           13391481,
                           13366298,
                           13333926,
                           13374138,
                           13320590,
                           13290846,
                           13277405,
                           13357611,
                           13211405,
                           13323664,
                           13338722,
                           13295927,
                           13309139,
                           13322718,
                           13306163,
                           13197157,
                           13221467,
                           13158128,
                           13246748,
                           13172342,
                           13292415,
                           13267347,
                           13270024,
                           13238710,
                           13257781,
                           13257679,
                           13257678,
                           13257540,
                           13173633,
                           13232054,
                           13174530,
                           13173661,
                           13185779,
                           13175473,
                           13069184,
                           13175474,
                           13175472,
                           13198859,
                           13194600,
                           13066936,
                           13139891,
                           13089106,
                           13173663,
                           13181177,
                           13181178,
                           13073825,
                           13094285,
                           13157894,
                           13067035,
                           13030363,
                           13141893,
                           13145983,
                           13136447,
                           13063014,
                           13094384,
                           13066991,
                           13047035,
                           13003046,
                           13060194,
                           13011301,
                           12952608,
                           12997102,
                           12988813,
                           12988690,
                           12996835,
                           12955359,
                           12980134,
                           12988689,
                           12982738,
                           12945088,
                           12972926,
                           12846932,
                           12908387,
                           12905197,
                           12919469,
                           12913077,
                           12833184,
                           12890993,
                           12845647,
                           12801987,
                           12739584,
                           12812055,
                           12811233,
                           12694078,
                           12795863,
                           12671861,
                           12788878,
                           12771891,
                           12761623,
                           12775384,
                           12752591,
                           12740842,
                           12711043,
                           12710909,
                           12727677,
                           12754529,
                           12680618,
                           12724943,
                           12696273,
                           12552777,
                           12593818,
                           12587476,
                           12675870,
                           12657103,
                           12650132,
                           12637842,
                           12522528,
                           12526768,
                           12580320,
                           12599859,
                           12618427,
                           12599780,
                           12307375,
                           12595803,
                           12437427,
                           12550842,
                           12534159,
                           12530297,
                           12552624,
                           12531270,
                           12482475,
                           12481413,
                           12505575,
                           12450936,
                           12497671,
                           12497703,
                           12492779,
                           12403853,
                           12461943,
                           12350167,
                           12212141,
                           12376149,
                           12242505,
                           12352205,
                           12328137,
                           12244649,
                           12294925,
                           12258246,
                           12173591,
                           12226779,
                           12276511,
                           12130446,
                           12185146,
                           12144044,
                           12159668,
                           12103851,
                           12173863,
                           12137335,
                           12118622,
                           12105170,
                           11936549,
                           12064011,
                           11987789,
                           12010136,
                           11982941,
                           11981633,
                           11461279,
                           11462642,
                           11462132,
                           11193331,
                           10714237,
                           10497629,
                           10629338,
                           11032367,
                           10491458,
                           10558507,
                           8819467,
                           9150580,
                           9041007,
                           8794877,
                           8677123)';
                           EXECUTE IMMEDIATE l_sql1;
	   COMMIT;
    DBMS_OUTPUT.put_line ('TMS: 20170111-00137 , Created the AP_UNDO_INVOICES table');
	EXCEPTION
	WHEN OTHERS THEN
  -- ROLLBACK;
	DBMS_OUTPUT.PUT_LINE ('TMS: 20170111-00137 , Error while AP_UNDO_INVOICES Table creating');
	END;
  
  BEGIN
          DBMS_OUTPUT.put_line ('TMS: 20170111-00137 , Before Creating the AP_UNDO_CHECKS table');	
	  l_sql2:= 'CREATE TABLE ap_undo_checks AS 
                         SELECT CHECK_ID, 
						        ACCOUNTING_EVENT_ID EVENT_ID, 
                                TO_DATE( SYSDATE, ''DD-MON-YYYY'' ) PROPOSED_UNDO_DATE, 
                                ''Y'' Process_flag 
                         FROM   ap_payment_history_all 
                         WHERE   check_id IN ( 
                                    SELECT  aip1.check_id 
                                    FROM    ap_invoice_payments_all aip1 
                                    WHERE   aip1.invoice_id  in 
                                            (SELECT DISTINCT invoice_id 
                                             FROM   AP_UNDO_INVOICES) 
                                    AND aip1.posted_flag = ''Y'')';	
		EXECUTE IMMEDIATE l_sql2;
       COMMIT;		
    DBMS_OUTPUT.put_line ('TMS: 20170111-00137 , Created the AP_UNDO_CHECKS table');
  EXCEPTION
    WHEN OTHERS
    THEN
     -- ROLLBACK;
      DBMS_OUTPUT.PUT_LINE ('TMS: 20170111-00137 , Errors while creating the AP_UNDO_CHECKS table, Error is : ' || SQLERRM);  
  END;
 
 -- api EXECUTIONS STARTS
 
 BEGIN
 
    l_calling_sequence := 'From the undo_bulk script';

    AP_ACCTG_DATA_FIX_PKG.Open_Log_Out_Files(90877,l_file_location);
    AP_ACCTG_DATA_FIX_PKG.Print('<html><body>');

    DBMS_OUTPUT.Put_Line('The file location is:'||l_file_location);

    AP_ACCTG_DATA_FIX_PKG.apps_initialize
      (l_user_name, l_resp_name, l_calling_sequence);
 
    AP_ACCTG_DATA_FIX_PKG.undo_acctg_entries
      (p_bug_no            => l_bug_no,
       p_driver_table      => l_driver_table,
       p_calling_sequence  => l_calling_sequence
      );

    FOR I IN (SELECT DISTINCT check_id FROM ap_undo_checks WHERE process_flag = 'D')
    LOOP
 
    Select org_id
    into l_org_id
    from ap_checks_all 
    where check_id = I.check_id;
    
    MO_GLOBAL.SET_POLICY_CONTEXT( 'S', l_org_id );
    
    
    if(
    AP_ACCTG_DATA_FIX_PKG.delete_cascade_adjustments
    (p_source_type => l_source_type,
     p_source_id   => I.check_id
     )) Then
     DBMS_OUTPUT.Put_Line('deletion of Payment adjustment done for ' || I.check_id);
    
     end if;
     End Loop;


     AP_ACCTG_DATA_FIX_PKG.Close_Log_Out_Files;
    COMMIT;

    EXCEPTION
      WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE('API Error Details as :'||SQLERRM);
    END;     
    
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20170111-00137 , Errors : ' || SQLERRM);
END;
/