---------------------------------------------------------------------------------------------------------------
/**************************************************************************************************************
  $Header XXEIS.EIS_XXWC_AR_AGING_CUST_MONTH_V $
  Module Name : Receivables
  PURPOSE	  : AR Aging Summary by Customer - MonthEnd - WC
  TMS Task Id : 20161020-00069  
  REVISIONS   :
  VERSION 		DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     	   02-Nov-2016        	Siva   		 Initial version--TMS#20161020-00069
**************************************************************************************************************/
DROP VIEW XXEIS.EIS_XXWC_AR_AGING_CUST_MONTH_V;

CREATE OR REPLACE VIEW XXEIS.EIS_XXWC_AR_AGING_CUST_MONTH_V (BILL_TO_CUSTOMER_NAME, BILL_TO_CUSTOMER_NUMBER, OUTSTANDING_AMOUNT, BUCKET_CURRENT, BUCKET_1_TO_30, BUCKET_31_TO_60, BUCKET_61_TO_90, BUCKET_91_TO_180, BUCKET_181_TO_360, BUCKET_361_DAYS_AND_ABOVE, PROFILE_CLASS, CREDIT_HOLD, CUSTOMER_ACCOUNT_STATUS, COLLECTOR, CREDIT_ANALYST_NAME, SALESREP_NUMBER, ACCOUNT_MANAGER)
AS
  SELECT customer_name bill_to_customer_name,
    customer_account_number bill_to_customer_number,
    SUM(transaction_remaining_balance) outstanding_amount,
    SUM(current_balance) bucket_current,
    SUM(thirty_days_bal) bucket_1_to_30,
    SUM(sixty_days_bal) bucket_31_to_60,
    SUM(ninety_days_bal) bucket_61_to_90,
    SUM(one_eighty_days_bal) bucket_91_to_180,
    SUM(three_sixty_days_bal) bucket_181_to_360,
    SUM(over_three_sixty_days_bal) bucket_361_days_and_above,
    customer_profile_class profile_class,
    account_credit_hold credit_hold,
    customer_account_status,
    collector_name collector,
    credit_analyst credit_analyst_name,
    salesrep_number salesrep_number,
    account_manager account_manager
  FROM xxwc.xxwc_ar_cust_bal_mv_monthly
  GROUP BY customer_name,
    customer_account_number,
    customer_profile_class,
    account_credit_hold,
    customer_account_status,
    collector_name,
    credit_analyst,
    salesrep_number,
    account_manager
/

