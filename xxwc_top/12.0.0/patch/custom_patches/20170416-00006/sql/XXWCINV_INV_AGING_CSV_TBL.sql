/***************************************************************************
    $Header XXWC.XXWCINV_INV_AGING_CSV_TBL $
    Module Name: XXWCINV_INV_AGING_CSV_TBL
    PURPOSE: Table used for XXWC Inventory Aging Report - CSV Program
 
    REVISIONS:
    Ver        Date         Author           Description
   ---------  -----------  ---------------  ------------------------------------
   1.0        04-JUL-2016  P.Vamshidhar      TMS #20170416-00006 - XXWC Inventory Aging Report - CSV Output
***************************************************************************/
CREATE TABLE  XXWC.XXWCINV_INV_AGING_CSV_TBL (VENDOR_ID          NUMBER
                                             ,ITEM_ID            NUMBER 
                                             ,TO_ORGANIZATION_ID NUMBER)
/