-------------------------------------------------------------------------------------------
/*****************************************************************************************
  $Header XXEIS.EIS_XXWC_PO_RCV_DETAILS_V.vw $
  Module Name: Purchasing
  PURPOSE: Receipt Details-WC
  REVISIONS:
  Ver          Date        Author                Description
  ---------  ----------  ------------------    ----------------
  1.1		 4-Apr-2018      Siva			  TMS#20180327-00082
******************************************************************************************/
---------------------------------------------------------------------------------------------
  CREATE OR REPLACE  VIEW "XXEIS"."EIS_XXWC_PO_RCV_DETAILS_V" ("TX_ID", "ORG", "ORGANIZATION_CODE", "SRC_TYPE", "SRC", "ITEM", "CATEGORY", "REV", "DES", "RECEIPT_NUM", "DOC_NUM", "TX_DATE", "TX_TYPE", "UOM", "QTY", "PRICE", "DOC_TYPE", "BUYER_PREPARER", "RECEIVER", "PCKING_SLIP", "BILL_OF_LADING", "FREIGHT_CARRIER", "CREATION_DATE", "Q_CODE", "EXCEPTION", "PAR_TX_TYPE", "DESTINATION_TYPE", "DELIVER_TO_PERSON", "DELIVER_TO_LOCATION", "DESTINATION_SUBINVENTORY", "LOCATOR", "TX_REASON", "LS_ITEM_ID", "LS_TRANSACTION_ID", "LS_SHIPMENT_LINE_ID", "LS_ORGANIZATION_ID", "RCV_SHIPMENT_NUM", "RCV_SL_QUANTITY_SHIPPED", "RCV_SL_QUANTITY_RECEIVED", "RCV_SL_UNIT_OF_MEASURE", "COMPANY", "PROJETC_NUMBER", "PROJECT_NAME", "PROJECT_ID", "UNIT_WEIGHT", "ON_HAND_QTY") AS 
  SELECT 
  rct.transaction_id tx_id,
  HRU.NAME ORG,
  MP.ORGANIZATION_CODE ORGANIZATION_CODE,
 flv1.meaning src_type,
  pov.vendor_name src,
  msi.concatenated_segments item,
  mca.concatenated_segments CATEGORY,
  rsl.item_revision rev,
  rsl.item_description des,
  rsh.receipt_num receipt_num,
  poh.segment1
  || DECODE (por.release_num, NULL, NULL, '-'
  || por.release_num)
  || DECODE (pol.line_num, NULL, NULL, '-'
  || pol.line_num)
  || DECODE (pll.shipment_num, NULL, NULL, '-'
  || PLL.SHIPMENT_NUM) DOC_NUM,
  trunc(RCT.TRANSACTION_DATE) TX_DATE,
  flv2.meaning tx_type,
  rct.unit_of_measure uom,
  ROUND (rct.quantity, xxeis.eis_rs_po_fin_com_util_pkg.get_qty_precision ) qty,
  pll.price_override price,
  PDT.TYPE_NAME DOC_TYPE,
  xxeis.eis_rs_xxwc_com_util_pkg.get_person_name(poh.agent_id,poh.creation_date) BUYER_PREPARER,
  xxeis.eis_rs_xxwc_com_util_pkg.get_person_name(rct.employee_id,rct.creation_date) receiver,
  rsh.packing_slip pcking_slip,
  rsh.bill_of_lading, --added for version 1.1
  rsh.freight_carrier_code freight_carrier, --added for version 1.1
  rct.creation_date creation_date,
  rct.inspection_quality_code q_code,
  DECODE (rct.receipt_exception_flag, 'Y', 'Yes', 'N', 'No', NULL) EXCEPTION,
  FLV3.meaning PAR_TX_TYPE,
  flv.meaning DESTINATION_TYPE,
  xxeis.eis_rs_xxwc_com_util_pkg.get_person_name(rct.deliver_to_person_id,rct.creation_date) DELIVER_TO_PERSON,
  XHR.LOCATION_CODE  deliver_to_location,
  rct.subinventory destination_subinventory,
  msl.concatenated_segments LOCATOR,
  mtr.reason_name tx_reason,
  rsl.item_id ls_item_id,
  rct.transaction_id ls_transaction_id,
  rct.shipment_line_id ls_shipment_line_id,
  rct.organization_id ls_organization_id,
  rsh.shipment_num rcv_shipment_num,
  rsl.quantity_shipped rcv_sl_quantity_shipped,
  rsl.quantity_received rcv_sl_quantity_received,
  rsl.unit_of_measure rcv_sl_unit_of_measure,
  COMP.COMPANY COMPANY ,
  null projetc_number,
 null project_name,
  null project_id,
  msi.unit_weight,
  xxeis.eis_rs_xxwc_com_util_pkg.GET_ONHAND_INV(msi.inventory_item_id,msi.organization_id) on_hand_qty
  FROM
  FND_LOOKUP_VALUES FLV,
  FND_LOOKUP_VALUES FLV1,
  FND_LOOKUP_VALUES FLV2,
  FND_LOOKUP_VALUES FLV3,
  po_releases por,
  po_document_types pdt,
  PO_DISTRIBUTIONS POD,
  hr_locations hrl,
  mtl_transaction_reasons mtr,
  po_vendors pov,
  hr_organization_units hru,
  mtl_system_items_kfv msi,
  mtl_categories_kfv mca,
  mtl_item_locations_kfv msl,
  po_headers poh,
  po_lines pol,
  po_line_locations pll,
  rcv_transactions par,
  rcv_shipment_lines rsl,
  RCV_SHIPMENT_HEADERS RSH,
  rcv_transactions rct,
  (SELECT gsb.NAME company,
    fsp.inventory_organization_id organization_id,
    gsb.currency_code gl_currency
  FROM gl_sets_of_books gsb,
    financials_system_parameters fsp,
    fnd_currencies fc,
    mtl_default_sets_view mdv
  WHERE gsb.set_of_books_id  = fsp.set_of_books_id
  AND MDV.FUNCTIONAL_AREA_ID = 2
  AND FC.CURRENCY_CODE       = GSB.CURRENCY_CODE
  ) COMP,
 Mtl_parameters MP,
  XXEIS.XXWC_HR_LOCATIONS_V XHR
WHERE msi.inventory_item_id(+)                      = rsl.item_id
AND NVL (msi.organization_id, comp.organization_id) = comp.organization_id
AND mca.category_id                                 = rsl.category_id
AND msl.inventory_location_id(+)                    = rct.locator_id
AND NVL (msl.organization_id, rct.organization_id)  = rct.organization_id
AND rct.source_document_code                        = 'PO'
AND rsh.receipt_source_code
  || ''                      = 'VENDOR'
AND rct.shipment_line_id     = rsl.shipment_line_id
AND rct.shipment_header_id   = rsh.shipment_header_id
AND rct.po_header_id         = poh.po_header_id
AND rct.po_line_id           = pol.po_line_id
AND rct.po_line_location_id  = pll.line_location_id
AND rct.po_distribution_id   = pod.po_distribution_id(+)
AND rct.po_release_id        = por.po_release_id(+)
AND ( ( poh.type_lookup_code = 'STANDARD'
AND pdt.document_type_code   = 'PO'
AND pdt.document_subtype     = 'STANDARD' )
OR ( pdt.document_type_code  = 'RELEASE'
AND pdt.document_subtype     = NVL (por.release_type, '~') ) )
AND FLV1.LOOKUP_CODE = RSH.RECEIPT_SOURCE_CODE
  || ''
AND flv1.lookup_type      = 'SHIPMENT SOURCE TYPE'
AND rsh.vendor_id         = pov.vendor_id
AND par.transaction_id(+) = rct.parent_transaction_id
AND HRL.LOCATION_ID(+)    = RCT.DELIVER_TO_LOCATION_ID
AND flv.LOOKUP_CODE = RCT.DESTINATION_TYPE_CODE
AND FLV.LOOKUP_TYPE = 'RCV DESTINATION TYPE'
AND flv2.lookup_type = 'RCV TRANSACTION TYPE'
AND FLV2.LOOKUP_CODE = RCT.TRANSACTION_TYPE
AND FLV3.lookup_type(+)   = 'RCV TRANSACTION TYPE'
AND FLV3.lookup_code(+)   = par.transaction_type
AND hru.organization_id   = rct.organization_id
AND mtr.reason_id(+)      = rct.reason_id
and MP.ORGANIZATION_ID(+)=HRU.ORGANIZATION_ID
AND XHR.LOCATION_ID(+)=HRL.LOCATION_ID
AND FLV2.MEANING='Receive'
and not exists(select 'Y' 
               FROM MTL_SYSTEM_ITEMS_B_KFV RMSI
               WHERE  RMSI.INVENTORY_ITEM_ID=MSI.INVENTORY_ITEM_ID
               AND RMSI.ORGANIZATION_ID=MSI.ORGANIZATION_ID
               and RMSI.ITEM_TYPE='RENTAL'
               )
UNION
SELECT 
  rct.transaction_id tx_id,
  HRU.NAME ORG,
  MP.ORGANIZATION_CODE ORGANIZATION_CODE,
  flv1.meaning src_type,
  pov.vendor_name src,
  msi.concatenated_segments item,
  mca.concatenated_segments CATEGORY,
  rsl.item_revision rev,
  rsl.item_description des,
  rsh.receipt_num receipt_num,
  poh.segment1
  || DECODE (por.release_num, NULL, NULL, '-'
  || por.release_num)
  || DECODE (pol.line_num, NULL, NULL, '-'
  || pol.line_num)
  || DECODE (pll.shipment_num, NULL, NULL, '-'
  || PLL.SHIPMENT_NUM) DOC_NUM,
  trunc(RCT.TRANSACTION_DATE) TX_DATE,
  flv2.meaning tx_type,
  rct.unit_of_measure uom,
  ROUND (rct.quantity, xxeis.eis_rs_po_fin_com_util_pkg.get_qty_precision ) qty,
  pll.price_override price,
  PDT.TYPE_NAME DOC_TYPE,
  xxeis.eis_rs_xxwc_com_util_pkg.get_person_name(poh.agent_id,poh.creation_date) BUYER_PREPARER,
  xxeis.eis_rs_xxwc_com_util_pkg.get_person_name(rct.employee_id,rct.creation_date) receiver,
  rsh.packing_slip pcking_slip,
  rsh.bill_of_lading,    --added for version 1.1
  rsh.freight_carrier_code freight_carrier,   --added for version 1.1
  rct.creation_date creation_date,
  rct.inspection_quality_code q_code,
  DECODE (rct.receipt_exception_flag, 'Y', 'Yes', 'N', 'No', NULL) EXCEPTION,
  FLV3.meaning PAR_TX_TYPE,
  flv.meaning DESTINATION_TYPE,
  xxeis.eis_rs_xxwc_com_util_pkg.get_person_name(rct.deliver_to_person_id,rct.creation_date) DELIVER_TO_PERSON,
  XHR.LOCATION_CODE  deliver_to_location,
  rct.subinventory destination_subinventory,
  msl.concatenated_segments LOCATOR,
  mtr.reason_name tx_reason,
  rsl.item_id ls_item_id,
  rct.transaction_id ls_transaction_id,
  rct.shipment_line_id ls_shipment_line_id,
  rct.organization_id ls_organization_id,
  rsh.shipment_num rcv_shipment_num,
  rsl.quantity_shipped rcv_sl_quantity_shipped,
  rsl.quantity_received rcv_sl_quantity_received,
  rsl.unit_of_measure rcv_sl_unit_of_measure,
  COMP.COMPANY COMPANY ,
  null projetc_number,
 null project_name,
  null project_id,
  msi.unit_weight,
  xxeis.eis_rs_xxwc_com_util_pkg.GET_ONHAND_INV(msi.inventory_item_id,msi.organization_id) on_hand_qty
  FROM
  FND_LOOKUP_VALUES FLV,
  FND_LOOKUP_VALUES FLV1,
  FND_LOOKUP_VALUES FLV2,
  FND_LOOKUP_VALUES FLV3,
  po_releases por,
  po_document_types pdt,
  PO_DISTRIBUTIONS POD,
  hr_locations hrl,
  mtl_transaction_reasons mtr,
  po_vendors pov,
  hr_organization_units hru,
  mtl_system_items_kfv msi,
  mtl_categories_kfv mca,
  mtl_item_locations_kfv msl,
  po_headers poh,
  po_lines pol,
  po_line_locations pll,
  rcv_transactions par,
  rcv_shipment_lines rsl,
  RCV_SHIPMENT_HEADERS RSH,
  rcv_transactions rct,
  (SELECT gsb.NAME company,
    fsp.inventory_organization_id organization_id,
    gsb.currency_code gl_currency
  FROM gl_sets_of_books gsb,
    financials_system_parameters fsp,
    fnd_currencies fc,
    mtl_default_sets_view mdv
  WHERE gsb.set_of_books_id  = fsp.set_of_books_id
  AND MDV.FUNCTIONAL_AREA_ID = 2
  AND FC.CURRENCY_CODE       = GSB.CURRENCY_CODE
  ) COMP,
 Mtl_parameters MP,
  XXEIS.XXWC_HR_LOCATIONS_V XHR
WHERE msi.inventory_item_id(+)                      = rsl.item_id
AND NVL (msi.organization_id, comp.organization_id) = comp.organization_id
AND mca.category_id                                 = rsl.category_id
AND msl.inventory_location_id(+)                    = rct.locator_id
AND NVL (msl.organization_id, rct.organization_id)  = rct.organization_id
AND rct.source_document_code                        = 'PO'
AND rsh.receipt_source_code
  || ''                      = 'VENDOR'
AND rct.shipment_line_id     = rsl.shipment_line_id
AND rct.shipment_header_id   = rsh.shipment_header_id
AND rct.po_header_id         = poh.po_header_id
AND rct.po_line_id           = pol.po_line_id
AND rct.po_line_location_id  = pll.line_location_id
AND rct.po_distribution_id   = pod.po_distribution_id(+)
AND rct.po_release_id        = por.po_release_id(+)
AND ( ( poh.type_lookup_code = 'STANDARD'
AND pdt.document_type_code   = 'PO'
AND pdt.document_subtype     = 'STANDARD' )
OR ( pdt.document_type_code  = 'RELEASE'
AND pdt.document_subtype     = NVL (por.release_type, '~') ) )
AND FLV1.LOOKUP_CODE = RSH.RECEIPT_SOURCE_CODE
  || ''
AND flv1.lookup_type      = 'SHIPMENT SOURCE TYPE'
AND rsh.vendor_id         = pov.vendor_id
AND par.transaction_id(+) = rct.parent_transaction_id
AND HRL.LOCATION_ID(+)    = RCT.DELIVER_TO_LOCATION_ID
AND flv.LOOKUP_CODE = RCT.DESTINATION_TYPE_CODE
AND FLV.LOOKUP_TYPE = 'RCV DESTINATION TYPE'
AND flv2.lookup_type = 'RCV TRANSACTION TYPE'
AND FLV2.LOOKUP_CODE = RCT.TRANSACTION_TYPE
AND FLV3.lookup_type(+)   = 'RCV TRANSACTION TYPE'
AND FLV3.lookup_code(+)   = par.transaction_type
AND hru.organization_id   = rct.organization_id
AND mtr.reason_id(+)      = rct.reason_id
and MP.ORGANIZATION_ID(+)=HRU.ORGANIZATION_ID
AND XHR.LOCATION_ID(+)=HRL.LOCATION_ID
AND FLV2.MEANING='Correct'
and (FLV3.MEANING is null or FLV3.MEANING='Receive')
and not exists(select 'Y' 
               FROM MTL_SYSTEM_ITEMS_B_KFV RMSI
               WHERE  RMSI.INVENTORY_ITEM_ID=MSI.INVENTORY_ITEM_ID
               AND RMSI.ORGANIZATION_ID=MSI.ORGANIZATION_ID
               and RMSI.ITEM_TYPE='RENTAL'
               )
union
SELECT 
rct.transaction_id tx_id,
  HRU.NAME ORG,
  mp.ORGANIZATION_CODE ORGANIZATION_CODE,
  FLV1.MEANING SRC_TYPE,
  hru1.NAME
  || prl.source_subinventory src,
  msi.concatenated_segments item,
  mca.concatenated_segments CATEGORY,
  rsl.item_revision rev,
  rsl.item_description des,
  rsh.receipt_num receipt_num,
  PRH.SEGMENT1 DOC_NUM,
  trunc(RCT.TRANSACTION_DATE) TX_DATE,
  flv2.meaning tx_type,
  rct.unit_of_measure uom,
  ROUND (rct.quantity, xxeis.eis_rs_po_fin_com_util_pkg.get_qty_precision ) qty,
  prl.unit_price price,
  PDT.TYPE_NAME DOC_TYPE,
  xxeis.eis_rs_xxwc_com_util_pkg.get_person_name(prh.preparer_id,prh.last_update_date) BUYER_PREPARER,
  xxeis.eis_rs_xxwc_com_util_pkg.get_person_name(rct.employee_id,rct.creation_date) receiver,
  rsh.packing_slip pcking_slip,
  rsh.bill_of_lading,  --added for version 1.1
  rsh.freight_carrier_code freight_carrier,  --added for version 1.1
  rct.creation_date creation_date,
  rct.inspection_quality_code q_code,
  DECODE (rct.receipt_exception_flag, 'Y', 'Yes', 'N', 'No', NULL) EXCEPTION,
  flv3.meaning PAR_TX_TYPE,
  FLV.MEANING DESTINATION_TYPE,
  xxeis.eis_rs_xxwc_com_util_pkg.get_person_name(prl.to_person_id,prl.last_update_date) DELIVER_TO_PERSON,
  XHR.LOCATION_CODE  deliver_to_location,
  rct.subinventory destination_subinventory,
  msl.concatenated_segments LOCATOR,
  mtr.reason_name tx_reason,
  rsl.item_id ls_item_id,
  rct.transaction_id ls_transaction_id,
  rct.shipment_line_id ls_shipment_line_id,
  rct.organization_id ls_organization_id,
  rsh.shipment_num rcv_shipment_num,
  rsl.quantity_shipped rcv_sl_quantity_shipped,
  rsl.quantity_received rcv_sl_quantity_received,
  rsl.unit_of_measure rcv_sl_unit_of_measure,
  comp.company company ,
  NULL projetc_number,
  NULL project_name,
  NULL project_id,
  msi.unit_weight,
  xxeis.eis_rs_xxwc_com_util_pkg.GET_ONHAND_INV(msi.inventory_item_id,msi.organization_id) on_hand_qty
  FROM 
RCV_SHIPMENT_HEADERS RSH,
RCV_TRANSACTIONS RCT,
RCV_SHIPMENT_LINES RSL,
RCV_TRANSACTIONS PAR,
MTL_SYSTEM_ITEMS_KFV MSI,
PO_REQUISITION_HEADERS PRH,
PO_REQUISITION_LINES PRL,
MTL_CATEGORIES_KFV MCA,
MTL_ITEM_LOCATIONS_KFV MSL,
MTL_TRANSACTION_REASONS MTR,
FND_LOOKUP_VALUES FLV1,
FND_LOOKUP_VALUES FLV,
FND_LOOKUP_VALUES FLV2,
FND_LOOKUP_VALUES FLV3,
PO_DOCUMENT_TYPES PDT,
HR_ORGANIZATION_UNITS HRU,
HR_ORGANIZATION_UNITS HRU1,
HR_LOCATIONS HRL,
XXEIS.XXWC_HR_LOCATIONS_V XHR,
    (SELECT gsb.NAME company,
    fsp.inventory_organization_id organization_id,
    gsb.currency_code gl_currency
  FROM gl_sets_of_books gsb,
    financials_system_parameters fsp,
    fnd_currencies fc,
    mtl_default_sets_view mdv
  WHERE gsb.set_of_books_id  = fsp.set_of_books_id
  AND MDV.FUNCTIONAL_AREA_ID = 2
  AND FC.CURRENCY_CODE       = GSB.CURRENCY_CODE
  ) COMP,
  mtl_parameters mp
WHERE 1=1
AND RCT.SHIPMENT_HEADER_ID                          = RSH.SHIPMENT_HEADER_ID
AND RCT.SHIPMENT_LINE_ID                            = RSL.SHIPMENT_LINE_ID
AND RSH.SHIPMENT_HEADER_ID = RSL.SHIPMENT_HEADER_ID
AND PAR.TRANSACTION_ID(+) = RCT.PARENT_TRANSACTION_ID
AND MSI.INVENTORY_ITEM_ID(+)                      = RSL.ITEM_ID
AND NVL (MSI.ORGANIZATION_ID, COMP.ORGANIZATION_ID) = COMP.ORGANIZATION_ID
AND RCT.REQUISITION_LINE_ID                         = PRL.REQUISITION_LINE_ID
AND PRL.REQUISITION_HEADER_ID                       = PRH.REQUISITION_HEADER_ID
AND RCT.SOURCE_DOCUMENT_CODE                        = 'REQ'
AND RSH.RECEIPT_SOURCE_CODE   || ''     = 'INTERNAL ORDER'
AND MCA.CATEGORY_ID                                 = RSL.CATEGORY_ID
AND MSL.INVENTORY_LOCATION_ID(+)                    = RCT.LOCATOR_ID
AND NVL (MSL.ORGANIZATION_ID, RCT.ORGANIZATION_ID)  = RCT.ORGANIZATION_ID
AND MTR.REASON_ID(+)      = RCT.REASON_ID
 AND FLV1.LOOKUP_CODE = RSH.RECEIPT_SOURCE_CODE
  || ''
AND FLV1.LOOKUP_TYPE      = 'SHIPMENT SOURCE TYPE'
AND FLV.LOOKUP_CODE = PRL.DESTINATION_TYPE_CODE
AND FLV.LOOKUP_TYPE = 'RCV DESTINATION TYPE'
AND FLV2.LOOKUP_TYPE = 'RCV TRANSACTION TYPE'
AND FLV2.LOOKUP_CODE = RCT.TRANSACTION_TYPE
AND FLV3.LOOKUP_TYPE(+)   = 'RCV TRANSACTION TYPE'
AND FLV3.LOOKUP_CODE(+)   = PAR.TRANSACTION_TYPE
AND pdt.document_subtype                            = prh.type_lookup_code
AND PDT.DOCUMENT_TYPE_CODE                          = 'REQUISITION'
AND RCT.ORGANIZATION_ID                             = HRU.ORGANIZATION_ID
AND HRU1.ORGANIZATION_ID  =  RSH.ORGANIZATION_ID
AND HRL.LOCATION_ID(+) = PRL.DELIVER_TO_LOCATION_ID
AND MP.ORGANIZATION_ID= HRU.ORGANIZATION_ID 
AND XHR.LOCATION_ID(+)=HRL.LOCATION_ID
and FLV2.MEANING='Receive'
and not exists(select 'Y' 
               FROM MTL_SYSTEM_ITEMS_B_KFV RMSI
               WHERE  RMSI.INVENTORY_ITEM_ID=MSI.INVENTORY_ITEM_ID
               AND RMSI.ORGANIZATION_ID=MSI.ORGANIZATION_ID
               and RMSI.ITEM_TYPE='RENTAL'
               )
union
SELECT 
rct.transaction_id tx_id,
  HRU.NAME ORG,
  mp.ORGANIZATION_CODE ORGANIZATION_CODE,
  FLV1.MEANING SRC_TYPE,
  hru1.NAME
  || prl.source_subinventory src,
  msi.concatenated_segments item,
  mca.concatenated_segments CATEGORY,
  rsl.item_revision rev,
  rsl.item_description des,
  rsh.receipt_num receipt_num,
  PRH.SEGMENT1 DOC_NUM,
  trunc(RCT.TRANSACTION_DATE) TX_DATE,
  flv2.meaning tx_type,
  rct.unit_of_measure uom,
  ROUND (rct.quantity, xxeis.eis_rs_po_fin_com_util_pkg.get_qty_precision ) qty,
  prl.unit_price price,
  PDT.TYPE_NAME DOC_TYPE,
  xxeis.eis_rs_xxwc_com_util_pkg.get_person_name(prh.preparer_id,prh.last_update_date) BUYER_PREPARER,
  xxeis.eis_rs_xxwc_com_util_pkg.get_person_name(rct.employee_id,rct.creation_date) receiver,
  rsh.packing_slip pcking_slip,
  rsh.bill_of_lading,  --added for version 1.1
  rsh.freight_carrier_code freight_carrier,  --added for version 1.1
  rct.creation_date creation_date,
  rct.inspection_quality_code q_code,
  DECODE (rct.receipt_exception_flag, 'Y', 'Yes', 'N', 'No', NULL) EXCEPTION,
  flv3.meaning PAR_TX_TYPE,
  FLV.MEANING DESTINATION_TYPE,
  xxeis.eis_rs_xxwc_com_util_pkg.get_person_name(prl.to_person_id,prl.last_update_date) DELIVER_TO_PERSON,
  XHR.LOCATION_CODE  deliver_to_location,
  rct.subinventory destination_subinventory,
  msl.concatenated_segments LOCATOR,
  mtr.reason_name tx_reason,
  rsl.item_id ls_item_id,
  rct.transaction_id ls_transaction_id,
  rct.shipment_line_id ls_shipment_line_id,
  rct.organization_id ls_organization_id,
  rsh.shipment_num rcv_shipment_num,
  rsl.quantity_shipped rcv_sl_quantity_shipped,
  rsl.quantity_received rcv_sl_quantity_received,
  rsl.unit_of_measure rcv_sl_unit_of_measure,
  comp.company company ,
  NULL projetc_number,
  NULL project_name,
  NULL project_id,
  msi.unit_weight,
  xxeis.eis_rs_xxwc_com_util_pkg.GET_ONHAND_INV(msi.inventory_item_id,msi.organization_id) on_hand_qty
  FROM 
RCV_SHIPMENT_HEADERS RSH,
RCV_TRANSACTIONS RCT,
RCV_SHIPMENT_LINES RSL,
RCV_TRANSACTIONS PAR,
MTL_SYSTEM_ITEMS_KFV MSI,
PO_REQUISITION_HEADERS PRH,
PO_REQUISITION_LINES PRL,
MTL_CATEGORIES_KFV MCA,
MTL_ITEM_LOCATIONS_KFV MSL,
MTL_TRANSACTION_REASONS MTR,
FND_LOOKUP_VALUES FLV1,
FND_LOOKUP_VALUES FLV,
FND_LOOKUP_VALUES FLV2,
FND_LOOKUP_VALUES FLV3,
PO_DOCUMENT_TYPES PDT,
HR_ORGANIZATION_UNITS HRU,
HR_ORGANIZATION_UNITS HRU1,
HR_LOCATIONS HRL,
XXEIS.XXWC_HR_LOCATIONS_V XHR,
    (SELECT gsb.NAME company,
    fsp.inventory_organization_id organization_id,
    gsb.currency_code gl_currency
  FROM gl_sets_of_books gsb,
    financials_system_parameters fsp,
    fnd_currencies fc,
    mtl_default_sets_view mdv
  WHERE gsb.set_of_books_id  = fsp.set_of_books_id
  AND MDV.FUNCTIONAL_AREA_ID = 2
  AND FC.CURRENCY_CODE       = GSB.CURRENCY_CODE
  ) COMP,
  mtl_parameters mp
WHERE 1=1
AND RCT.SHIPMENT_HEADER_ID                          = RSH.SHIPMENT_HEADER_ID
AND RCT.SHIPMENT_LINE_ID                            = RSL.SHIPMENT_LINE_ID
AND RSH.SHIPMENT_HEADER_ID = RSL.SHIPMENT_HEADER_ID
AND PAR.TRANSACTION_ID(+) = RCT.PARENT_TRANSACTION_ID
AND MSI.INVENTORY_ITEM_ID(+)                      = RSL.ITEM_ID
AND NVL (MSI.ORGANIZATION_ID, COMP.ORGANIZATION_ID) = COMP.ORGANIZATION_ID
AND RCT.REQUISITION_LINE_ID                         = PRL.REQUISITION_LINE_ID
AND PRL.REQUISITION_HEADER_ID                       = PRH.REQUISITION_HEADER_ID
AND RCT.SOURCE_DOCUMENT_CODE                        = 'REQ'
AND RSH.RECEIPT_SOURCE_CODE   || ''     = 'INTERNAL ORDER'
AND MCA.CATEGORY_ID                                 = RSL.CATEGORY_ID
AND MSL.INVENTORY_LOCATION_ID(+)                    = RCT.LOCATOR_ID
AND NVL (MSL.ORGANIZATION_ID, RCT.ORGANIZATION_ID)  = RCT.ORGANIZATION_ID
AND MTR.REASON_ID(+)      = RCT.REASON_ID
 AND FLV1.LOOKUP_CODE = RSH.RECEIPT_SOURCE_CODE
  || ''
AND FLV1.LOOKUP_TYPE      = 'SHIPMENT SOURCE TYPE'
AND FLV.LOOKUP_CODE = PRL.DESTINATION_TYPE_CODE
AND FLV.LOOKUP_TYPE = 'RCV DESTINATION TYPE'
AND FLV2.LOOKUP_TYPE = 'RCV TRANSACTION TYPE'
AND FLV2.LOOKUP_CODE = RCT.TRANSACTION_TYPE
AND FLV3.LOOKUP_TYPE(+)   = 'RCV TRANSACTION TYPE'
AND FLV3.LOOKUP_CODE(+)   = PAR.TRANSACTION_TYPE
AND pdt.document_subtype                            = prh.type_lookup_code
AND PDT.DOCUMENT_TYPE_CODE                          = 'REQUISITION'
AND RCT.ORGANIZATION_ID                             = HRU.ORGANIZATION_ID
AND HRU1.ORGANIZATION_ID  =  RSH.ORGANIZATION_ID
AND HRL.LOCATION_ID(+) = PRL.DELIVER_TO_LOCATION_ID
AND MP.ORGANIZATION_ID= HRU.ORGANIZATION_ID 
AND XHR.LOCATION_ID(+)=HRL.LOCATION_ID
AND FLV2.MEANING='Correct'
and (FLV3.MEANING is null or FLV3.MEANING='Receive')
and not exists(select 'Y' 
               FROM MTL_SYSTEM_ITEMS_B_KFV RMSI
               WHERE  RMSI.INVENTORY_ITEM_ID=MSI.INVENTORY_ITEM_ID
               AND RMSI.ORGANIZATION_ID=MSI.ORGANIZATION_ID
               and RMSI.ITEM_TYPE='RENTAL'
               )
union
SELECT 
  rct.transaction_id tx_id,
  HRU.NAME ORG,
  mp.ORGANIZATION_CODE ORGANIZATION_CODE,
  FLV1.MEANING SRC_TYPE,
  HRU1.NAME SRC,
  msi.concatenated_segments item,
  mca.concatenated_segments CATEGORY,
  rsl.item_revision rev,
  rsl.item_description des,
  rsh.receipt_num receipt_num,
  RSH.SHIPMENT_NUM DOC_NUM,
  trunc(RCT.TRANSACTION_DATE) TX_DATE,
  flv2.meaning tx_type,
  rct.unit_of_measure uom,
  ROUND (rct.quantity, xxeis.eis_rs_po_fin_com_util_pkg.get_qty_precision ) qty,
  RSL.SHIPMENT_UNIT_PRICE PRICE,
  flv4.meaning doc_type,
  '' BUYER_PREPARER,
  xxeis.eis_rs_xxwc_com_util_pkg.get_person_name(rct.employee_id,rct.creation_date) receiver,
  rsh.packing_slip pcking_slip,
  rsh.bill_of_lading,   --added for version 1.1
  rsh.freight_carrier_code freight_carrier,  --added for version 1.1
  rct.creation_date creation_date,
  rct.inspection_quality_code q_code,
  DECODE (rct.receipt_exception_flag, 'Y', 'Yes', 'N', 'No', NULL) EXCEPTION,
  flv3.meaning par_tx_type,
  FLV.meaning DESTINATION_TYPE,
  xxeis.eis_rs_xxwc_com_util_pkg.get_person_name(rct.deliver_to_person_id,rct.creation_date) DELIVER_TO_PERSON,
  XHR.LOCATION_CODE  deliver_to_location,
  rct.subinventory destination_subinventory,
  msl.concatenated_segments LOCATOR,
  mtr.reason_name tx_reason,
  rsl.item_id ls_item_id,
  rct.transaction_id ls_transaction_id,
  rct.shipment_line_id ls_shipment_line_id,
  rct.organization_id ls_organization_id,
  rsh.shipment_num rcv_shipment_num,
  rsl.quantity_shipped rcv_sl_quantity_shipped,
  rsl.quantity_received rcv_sl_quantity_received,
  rsl.unit_of_measure rcv_sl_unit_of_measure,
  comp.company company ,
  NULL projetc_number,
  NULL project_name,
  NULL project_id,
  msi.unit_weight,
  xxeis.eis_rs_xxwc_com_util_pkg.GET_ONHAND_INV(msi.inventory_item_id,msi.organization_id) on_hand_qty
  FROM 
  FND_LOOKUP_VALUES FLV,
  FND_LOOKUP_VALUES FLV1,
  FND_LOOKUP_VALUES FLV2,
  FND_LOOKUP_VALUES FLV3,
  FND_LOOKUP_VALUES FLV4,
  hr_organization_units hru,
  hr_locations hrl,
  mtl_transaction_reasons mtr,
  hr_organization_units hru1,
  mtl_system_items_kfv msi,
  mtl_categories_kfv mca,
  mtl_item_locations_kfv msl,
  rcv_transactions par,
  rcv_shipment_lines rsl,
  RCV_SHIPMENT_HEADERS RSH,
  rcv_transactions rct,
  (SELECT gsb.NAME company,
    fsp.inventory_organization_id organization_id,
    gsb.currency_code gl_currency
  FROM gl_sets_of_books gsb,
    financials_system_parameters fsp,
    fnd_currencies fc,
    mtl_default_sets_view mdv
  WHERE gsb.set_of_books_id  = fsp.set_of_books_id
  AND MDV.FUNCTIONAL_AREA_ID = 2
  AND FC.CURRENCY_CODE       = GSB.CURRENCY_CODE
  ) COMP,
  mtl_parameters mp,
   XXEIS.XXWC_HR_LOCATIONS_V XHR
WHERE msi.inventory_item_id(+)                      = rsl.item_id
AND NVL (msi.organization_id, comp.organization_id) = comp.organization_id
AND mca.category_id                                 = rsl.category_id
AND msl.inventory_location_id(+)                    = rct.locator_id
AND NVL (msl.organization_id, rct.organization_id)  = rct.organization_id
AND rct.shipment_line_id                            = rsl.shipment_line_id
AND rct.shipment_header_id                          = rsh.shipment_header_id
AND rsh.receipt_source_code|| '' = 'INVENTORY'
AND FLV4.LOOKUP_CODE = RCT.SOURCE_DOCUMENT_CODE
AND FLV4.LOOKUP_TYPE = 'SHIPMENT SOURCE DOCUMENT TYPE'
AND flv1.lookup_code = rsh.receipt_source_code|| ''
AND FLV1.LOOKUP_TYPE = 'SHIPMENT SOURCE TYPE'
AND rsh.organization_id   = hru1.organization_id
AND par.transaction_id(+) = rct.parent_transaction_id
AND HRL.LOCATION_ID(+)    = RCT.DELIVER_TO_LOCATION_ID
AND flv.lookup_code = rsl.destination_type_code
AND flv.lookup_type = 'RCV DESTINATION TYPE'
AND flv2.lookup_type = 'RCV TRANSACTION TYPE'
AND flv2.lookup_code = rct.transaction_type
AND flv3.lookup_type(+)  = 'RCV TRANSACTION TYPE'
AND flv3.lookup_code(+)  = par.transaction_type
AND HRU.ORGANIZATION_ID	= RCT.ORGANIZATION_ID
AND MTR.REASON_ID(+)     = RCT.REASON_ID
AND MP.ORGANIZATION_ID= HRU.ORGANIZATION_ID 
AND XHR.LOCATION_ID(+)=HRL.LOCATION_ID
and FLV2.MEANING='Receive'
and not exists(select 'Y' 
               FROM MTL_SYSTEM_ITEMS_B_KFV RMSI
               WHERE  RMSI.INVENTORY_ITEM_ID=MSI.INVENTORY_ITEM_ID
               AND RMSI.ORGANIZATION_ID=MSI.ORGANIZATION_ID
               and RMSI.ITEM_TYPE='RENTAL'
               )
union
SELECT 
  rct.transaction_id tx_id,
  HRU.NAME ORG,
  mp.ORGANIZATION_CODE ORGANIZATION_CODE,
  FLV1.MEANING SRC_TYPE,
  DECODE(RCT.SOURCE_DOCUMENT_CODE, 'PO', POV.VENDOR_NAME, OEV.NAME ) SRC,
  MSI.CONCATENATED_SEGMENTS ITEM,
  MCA.CONCATENATED_SEGMENTS CATEGORY,  
  rsl.item_revision rev,
  rsl.item_description des,
  rsh.receipt_num receipt_num,
  '' DOC_NUM,
  trunc(RCT.TRANSACTION_DATE) TX_DATE,
  flv2.meaning tx_type,
  rct.unit_of_measure uom,
  ROUND (rct.quantity, xxeis.eis_rs_po_fin_com_util_pkg.get_qty_precision ) qty  ,
  TO_NUMBER('') price,
  '' doc_type,
  '' BUYER_PREPARER,
  xxeis.eis_rs_xxwc_com_util_pkg.get_person_name(rct.employee_id,rct.creation_date) receiver,
  rsh.packing_slip pcking_slip,
  rsh.bill_of_lading,  --added for version 1.1
  rsh.freight_carrier_code freight_carrier, --added for version 1.1 
  rct.creation_date creation_date,
  RCT.INSPECTION_QUALITY_CODE Q_CODE,
  DECODE (RCT.RECEIPT_EXCEPTION_FLAG, 'Y', 'Yes', 'N', 'No', NULL) EXCEPTION,
  FLV3.MEANING PAR_TX_TYPE,
  FLV.MEANING DESTINATION_TYPE,  
  XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_PERSON_NAME(RCT.DELIVER_TO_PERSON_ID,RCT.CREATION_DATE) DELIVER_TO_PERSON,
  XHR.LOCATION_CODE  deliver_to_location,
  '' DESTINATION_SUBINVENTORY,
  msl.concatenated_segments LOCATOR,
  mtr.reason_name tx_reason,
  rsl.item_id ls_item_id,
  rct.transaction_id ls_transaction_id,
  rct.shipment_line_id ls_shipment_line_id,
  RCT.ORGANIZATION_ID LS_ORGANIZATION_ID,
  rsh.shipment_num rcv_shipment_num,
  rsl.quantity_shipped rcv_sl_quantity_shipped,
  rsl.quantity_received rcv_sl_quantity_received,
  rsl.unit_of_measure rcv_sl_unit_of_measure,
  comp.company company ,
  NULL projetc_number,
  NULL project_name,
  NULL project_id,
  MSI.UNIT_WEIGHT,
  xxeis.eis_rs_xxwc_com_util_pkg.GET_ONHAND_INV(msi.inventory_item_id,msi.organization_id) on_hand_qty  
FROM 
  FND_LOOKUP_VALUES FLV,
  FND_LOOKUP_VALUES FLV1,
  FND_LOOKUP_VALUES FLV2,
  FND_LOOKUP_VALUES FLV3,  
  HR_LOCATIONS lot,
  mtl_transaction_reasons mtr,
  po_vendors pov,
  hr_organization_units hru,
  mtl_system_items_kfv msi,
  mtl_categories_kfv mca,
  mtl_item_locations_kfv msl,
  rcv_transactions par,
  rcv_shipment_lines rsl,
  RCV_SHIPMENT_HEADERS RSH,
  RCV_TRANSACTIONS RCT,
  oe_sold_to_orgs_v oev,
  (SELECT gsb.NAME company,
    fsp.inventory_organization_id organization_id,
    gsb.currency_code gl_currency
  FROM gl_sets_of_books gsb,
    financials_system_parameters fsp,
    fnd_currencies fc,
    mtl_default_sets_view mdv
  WHERE gsb.set_of_books_id  = fsp.set_of_books_id
  AND MDV.FUNCTIONAL_AREA_ID = 2
  AND FC.CURRENCY_CODE       = GSB.CURRENCY_CODE
  ) COMP,
  mtl_parameters mp,
  XXEIS.XXWC_HR_LOCATIONS_V XHR
WHERE MSI.INVENTORY_ITEM_ID(+)   = RSL.ITEM_ID
AND NVL(msi.organization_id,comp.organization_id)= comp.organization_id
AND mca.category_id              = rsl.category_id
AND msl.inventory_location_id(+) = rct.locator_id
AND msl.organization_id(+)       = rct.organization_id
AND rct.source_document_code    IN('PO', 'RMA')
AND rsh.receipt_source_code     IN('VENDOR', 'CUSTOMER')
AND rct.shipment_line_id        = rsl.shipment_line_id
AND RCT.SHIPMENT_HEADER_ID      = RSH.SHIPMENT_HEADER_ID
AND FLV1.LOOKUP_CODE            = RSH.RECEIPT_SOURCE_CODE|| ''
AND FLV1.LOOKUP_TYPE            = 'SHIPMENT SOURCE TYPE'
AND RSH.VENDOR_ID               = POV.VENDOR_ID(+)
AND par.transaction_id(+)       = rct.parent_transaction_id
AND LOT.LOCATION_ID(+)          = RCT.DELIVER_TO_LOCATION_ID
AND FLV.LOOKUP_CODE             = RCT.DESTINATION_TYPE_CODE
AND FLV.LOOKUP_TYPE             = 'RCV DESTINATION TYPE'
AND FLV2.LOOKUP_TYPE            = 'RCV TRANSACTION TYPE'
AND FLV2.LOOKUP_CODE            = RCT.TRANSACTION_TYPE
AND FLV3.LOOKUP_TYPE(+)         = 'RCV TRANSACTION TYPE'
AND FLV3.LOOKUP_CODE(+)         = PAR.TRANSACTION_TYPE
AND hru.organization_id         = rct.organization_id
AND MTR.REASON_ID(+)            = RCT.REASON_ID
AND RCT.TRANSACTION_TYPE        = 'UNORDERED'
AND MP.ORGANIZATION_ID(+)       = HRU.ORGANIZATION_ID
AND OEV.CUSTOMER_ID(+)          = RSH.CUSTOMER_ID
AND FLV2.MEANING                = 'Receive'
and XHR.LOCATION_ID(+)          = LOT.LOCATION_ID
and not exists(select 'Y' 
               FROM MTL_SYSTEM_ITEMS_B_KFV RMSI
               WHERE  RMSI.INVENTORY_ITEM_ID=MSI.INVENTORY_ITEM_ID
               AND RMSI.ORGANIZATION_ID=MSI.ORGANIZATION_ID
               and RMSI.ITEM_TYPE='RENTAL'
               )
union
SELECT 
  rct.transaction_id TX_ID ,
  HRU.NAME ORG ,
  mp.ORGANIZATION_CODE ORGANIZATION_CODE,
  FLV1.MEANING SRC_TYPE,
  OEV.NAME SRC ,
  MSI.CONCATENATED_SEGMENTS ITEM,
  MCA.CONCATENATED_SEGMENTS CATEGORY,  
  rsl.item_revision REV ,
  rsl.item_description DES ,
  rsh.receipt_num receipt_num ,
  TO_CHAR(oeh.order_number)
  || '-'
  || TO_CHAR(oel.line_number) doc_num ,
trunc(RCT.TRANSACTION_DATE) TX_DATE,
  flv2.meaning tx_type,
  RCT.UNIT_OF_MEASURE UOM,
  RCT.QUANTITY QTY,
  rsl.shipment_unit_price PRICE ,
  'RMA' DOC_TYPE ,
  TO_CHAR(NULL) BUYER_PREPARER ,
  xxeis.eis_rs_xxwc_com_util_pkg.get_person_name(rct.employee_id,rct.creation_date) receiver,
  RSH.PACKING_SLIP PCKING_SLIP ,
  rsh.bill_of_lading,   --added for version 1.1
  rsh.freight_carrier_code freight_carrier,  --added for version 1.1
  RCT.CREATION_DATE CREATION_DATE ,
  RCT.INSPECTION_QUALITY_CODE Q_CODE ,
  DECODE (RCT.RECEIPT_EXCEPTION_FLAG, 'Y', 'Yes', 'N', 'No', NULL) EXCEPTION,
  FLV3.MEANING PAR_TX_TYPE,
  FLV.MEANING DESTINATION_TYPE,  
  XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_PERSON_NAME(RCT.DELIVER_TO_PERSON_ID,RCT.CREATION_DATE) DELIVER_TO_PERSON,
  XHR.LOCATION_CODE  deliver_to_location,
  RCT.SUBINVENTORY DESTINATION_SUBINVENTORY ,
  msl.concatenated_segments LOCATOR,
  MTR.REASON_NAME TX_REASON ,
  RSL.ITEM_ID LS_ITEM_ID ,
  RCT.TRANSACTION_ID LS_TRANSACTION_ID ,
  RCT.SHIPMENT_LINE_ID LS_SHIPMENT_LINE_ID ,
  RCT.ORGANIZATION_ID LS_ORGANIZATION_ID,
  rsh.shipment_num rcv_shipment_num,
  rsl.quantity_shipped rcv_sl_quantity_shipped,
  rsl.quantity_received rcv_sl_quantity_received,
  RSL.UNIT_OF_MEASURE RCV_SL_UNIT_OF_MEASURE,
  null company ,
  NULL projetc_number,
  NULL project_name,
  NULL project_id,
  MSI.UNIT_WEIGHT,
  xxeis.eis_rs_xxwc_com_util_pkg.GET_ONHAND_INV(msi.inventory_item_id,msi.organization_id) on_hand_qty  
FROM RCV_transactions rct ,
  RCV_transactions par ,
  hr_organization_units hru ,
  RCV_SHIPMENT_HEADERS RSH ,
  FND_LOOKUP_VALUES FLV1,
  FND_LOOKUP_VALUES FLV2,  
  oe_sold_to_orgs_v oev ,
  rcv_shipment_lines rsl ,
  mtl_system_items_kfv MSI ,
  MTL_CATEGORIES_KFV MCA ,
  FND_LOOKUP_VALUES FLV3,
  FND_LOOKUP_VALUES FLV,  
  HR_LOCATIONS LOT ,
  MTL_TRANSACTION_REASONS MTR ,
  mtl_item_locations_kfv MSL ,
  OE_ORDER_HEADERS OEH ,
  OE_ORDER_LINES OEL,
  mtl_parameters mp,
  XXEIS.XXWC_HR_LOCATIONS_V XHR,
   (
    SELECT gsb.NAME company,
      fsp.inventory_organization_id organization_id,
      gsb.currency_code gl_currency
    FROM gl_sets_of_books gsb,
      financials_system_parameters fsp,
      fnd_currencies fc,
      mtl_default_sets_view mdv
    WHERE gsb.set_of_books_id  = fsp.set_of_books_id
    AND MDV.FUNCTIONAL_AREA_ID = 2
    and FC.CURRENCY_CODE       = GSB.CURRENCY_CODE
    ) COMP
WHERE RCT.SOURCE_DOCUMENT_CODE = 'RMA'
AND RSH.RECEIPT_SOURCE_CODE
  || ''                    = 'CUSTOMER'
AND PAR.TRANSACTION_ID (+) = RCT.PARENT_TRANSACTION_ID
AND hru.organization_id    = rct.organization_id
AND FLV1.LOOKUP_CODE = RSH.RECEIPT_SOURCE_CODE
  || ''
AND FLV1.LOOKUP_TYPE                             = 'SHIPMENT SOURCE TYPE'
AND FLV.LOOKUP_CODE                              = RCT.DESTINATION_TYPE_CODE
and FLV.LOOKUP_TYPE                              = 'RCV DESTINATION TYPE'
AND oev.customer_id (+)                          = rsh.customer_id
AND RCT.SHIPMENT_LINE_ID                         = RSL.SHIPMENT_LINE_ID
AND RCT.SHIPMENT_HEADER_ID                       = RSH.SHIPMENT_HEADER_ID
AND RSH.shipment_header_id                       = rsl.shipment_header_id
AND MCA.CATEGORY_ID (+)                          = RSL.CATEGORY_ID -- OUTER JOIN ???
and MSI.INVENTORY_ITEM_ID (+)                    = RSL.ITEM_ID
AND NVL(msi.organization_id,comp.organization_id)= comp.organization_id--Added By SK
AND MSL.INVENTORY_LOCATION_ID (+)                = RCT.LOCATOR_ID
AND NVL(MSL.ORGANIZATION_ID,RCT.ORGANIZATION_ID) = RCT.ORGANIZATION_ID
AND FLV2.LOOKUP_TYPE                             = 'RCV TRANSACTION TYPE'
AND FLV2.LOOKUP_CODE                             = RCT.TRANSACTION_TYPE
AND FLV3.LOOKUP_TYPE (+) = 'RCV TRANSACTION TYPE'
AND FLV3.LOOKUP_CODE (+) = PAR.TRANSACTION_TYPE
and LOT.LOCATION_ID(+) = RCT.DELIVER_TO_LOCATION_ID
AND MTR.REASON_ID(+)   = RCT.REASON_ID
AND OEH.header_id      = OEL.header_id
AND OEL.LINE_ID        = RCT.OE_ORDER_LINE_ID
and mp.ORGANIZATION_ID(+)=HRU.ORGANIZATION_ID
AND XHR.LOCATION_ID(+)=LOT.LOCATION_ID
and FLV2.MEANING='Receive'
and not exists(select 'Y' 
               FROM MTL_SYSTEM_ITEMS_B_KFV RMSI
               WHERE  RMSI.INVENTORY_ITEM_ID=MSI.INVENTORY_ITEM_ID
               AND RMSI.ORGANIZATION_ID=MSI.ORGANIZATION_ID
               and RMSI.ITEM_TYPE='RENTAL'
               );
