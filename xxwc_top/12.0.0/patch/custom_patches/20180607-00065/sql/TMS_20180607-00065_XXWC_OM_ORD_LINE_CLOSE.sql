/*************************************************************************
  $Header TMS_20180607-00065_XXWC_OM_ORD_LINE_CLOSE.sql $
  Module Name: TMS_20180607-00065_XXWC_OM_ORD_LINE_CLOSE.sql

  PURPOSE:   Created to process the order headers stucked in booked status
             and line  is in Pending PreBill.

  REVISIONS:
  Ver        Date        Author               Description
  ---------  ----------  ---------------      -------------------------
  1.0        07/27/2018  Pattabhi Avula       TMS#20180607-00065
  **************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000;
DECLARE
BEGIN
 DBMS_OUTPUT.put_line ('TMS: Datafix script    , Before Update');
 -- Updating the headers table
 
UPDATE apps.oe_order_lines_all
   SET FLOW_STATUS_CODE='CLOSED'
      ,open_Flag='N'
 WHERE header_id =72186019
    AND line_id =122476556;

DBMS_OUTPUT.put_line ('Records updated  -' || SQL%ROWCOUNT);
		   	 
COMMIT;

	  DBMS_OUTPUT.put_line ('TMS: 20180607-00065  , End Update');
EXCEPTION
 WHEN OTHERS THEN
 DBMS_OUTPUT.put_line ('TMS: 20180607-00065 , Errors : ' || SQLERRM);
END;
/