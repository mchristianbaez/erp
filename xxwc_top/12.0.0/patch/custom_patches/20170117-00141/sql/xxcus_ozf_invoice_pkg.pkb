CREATE OR REPLACE PACKAGE BODY APPS.xxcus_ozf_invoice_pkg AS
  --    REVISIONS:
  --    Ver        Date        Author           Description
  --    ---------  ----------  ---------------  -------------------------------
  --    2.0       03/19/2014  Balaguru Seshadri 2. Modify Start_Autopay routine to include calendar year as a parameter
  --    1.1       12/19/2014  Balaguru Seshadri 3. ESMS 263605.
  --    3.0       08/08/2016  Balaguru Seshadri TMS  20160706-00218 / ESMS 786370
  --    4.0      01/17/2017   Balaguru Seshadri TMS 20170117-00141   -- Add routine wrapper.  
  -- -----------------------------------------------------------------------------

  g_pkg_name              CONSTANT VARCHAR2(30) := 'OZF_AUTOPAY_PVT';
  g_update_event          CONSTANT VARCHAR2(30) := 'UPDATE';
  g_daily                 CONSTANT VARCHAR2(30) := 'DAYS';
  g_weekly                CONSTANT VARCHAR2(30) := 'WEEKS';
  g_monthly               CONSTANT VARCHAR2(30) := 'MONTHS';
  g_quarterly             CONSTANT VARCHAR2(30) := 'QUARTERS';
  g_annual                CONSTANT VARCHAR2(30) := 'YEAR';
  g_offer_type            CONSTANT VARCHAR2(30) := 'OFFR';
  g_campaign_type         CONSTANT VARCHAR2(30) := 'CAMP';
  g_autopay_flag_off      CONSTANT VARCHAR2(40) := 'Autopay flag is not turned on.';
  g_autopay_period_miss   CONSTANT VARCHAR2(40) := 'Autopay period information missing.';
  g_autopay_plan_type_err CONSTANT VARCHAR2(40) := 'Can not hanlde this plan type.';
  g_claim_setup_id        CONSTANT NUMBER := 2001;
  g_claim_status          CONSTANT VARCHAR2(30) := 'OZF_CLAIM_STATUS';
  g_open_status           CONSTANT VARCHAR2(30) := 'OPEN';
  g_closed_status         CONSTANT VARCHAR2(30) := 'CLOSED';

  -- object_type
  g_claim_object_type CONSTANT VARCHAR2(30) := 'CLAM';

  ozf_debug_high_on BOOLEAN := fnd_msg_pub.check_msg_level(fnd_msg_pub.g_msg_lvl_debug_high);
  ozf_debug_low_on  BOOLEAN := fnd_msg_pub.check_msg_level(fnd_msg_pub.g_msg_lvl_debug_low);

  ---------------------------------------------------------------------
  -- Definitions of some packagewise cursors.
  ---------------------------------------------------------------------
  CURSOR g_site_info_csr(p_id IN NUMBER) IS
    SELECT trade_profile_id
          ,cust_account_id
          ,site_use_id
          ,payment_method
          ,vendor_id
          ,vendor_site_id
          ,last_paid_date
          ,autopay_periodicity
          ,autopay_periodicity_type
          ,autopay_flag
          ,claim_threshold
          ,claim_currency
          ,org_id
      FROM apps.ozf_cust_trd_prfls_all
     WHERE site_use_id = p_id;
  /*
     CURSOR g_customer_info_csr(p_id in number) IS
        SELECT trade_profile_id,
               cust_account_id,
               site_use_id,
               payment_method,
               vendor_id,
               vendor_site_id,
               last_paid_date,
               autopay_periodicity,
               autopay_periodicity_type,
               autopay_flag,
               claim_threshold,
               claim_currency,
               org_id
          FROM apps.ozf_cust_trd_prfls
         WHERE cust_account_id = p_id;
  --         AND site_use_id IS NULL;
  */
  CURSOR g_customer_info_csr(p_id IN NUMBER) IS
    SELECT trade_profile_id
          ,cust_account_id
          ,site_use_id
          ,payment_method
          ,vendor_id
          ,vendor_site_id
          ,last_paid_date
          ,autopay_periodicity
          ,autopay_periodicity_type
          ,autopay_flag
          ,claim_threshold
          ,claim_currency
          ,org_id
      FROM apps.ozf_cust_trd_prfls
     WHERE cust_account_id = p_id;

  CURSOR g_party_trade_info_csr(p_id IN NUMBER) IS
    SELECT trade_profile_id
          ,cust_account_id
          ,site_use_id
          ,payment_method
          ,vendor_id
          ,vendor_site_id
          ,last_paid_date
          ,autopay_periodicity
          ,autopay_periodicity_type
          ,autopay_flag
          ,claim_threshold
          ,claim_currency
          ,org_id
      FROM apps.ozf_cust_trd_prfls
     WHERE party_id = p_id
       AND cust_account_id IS NULL;

  TYPE funds_rem_rec_type IS RECORD(
    utilization_id      NUMBER,
    amount_remaining    NUMBER,
    scan_unit_remaining NUMBER);
  TYPE funds_rem_tbl_type IS TABLE OF funds_rem_rec_type INDEX BY BINARY_INTEGER;

  TYPE line_util_rec_type IS RECORD(
    claim_line_util_id    NUMBER,
    object_version_number NUMBER,
    last_update_date      DATE,
    last_updated_by       NUMBER,
    creation_date         DATE,
    created_by            NUMBER,
    last_update_login     NUMBER,
    claim_line_id         NUMBER,
    utilization_id        NUMBER,
    amount                NUMBER,
    currency_code         VARCHAR2(15),
    exchange_rate_type    VARCHAR2(30),
    exchange_rate_date    DATE,
    exchange_rate         NUMBER,
    acctd_amount          NUMBER,
    util_curr_amount      NUMBER,
    plan_curr_amount      NUMBER,
    scan_unit             NUMBER,
    activity_product_id   NUMBER,
    uom_code              VARCHAR2(3),
    quantity              NUMBER,
    org_id                NUMBER,
    univ_curr_amount      NUMBER,
    fxgl_acctd_amount     NUMBER,
    utilized_acctd_amount NUMBER,
    update_from_tbl_flag  VARCHAR2(1) := fnd_api.g_false);

  TYPE line_util_tbl_type IS TABLE OF line_util_rec_type INDEX BY BINARY_INTEGER;

  TYPE funds_util_flt_type IS RECORD(
    claim_line_id             NUMBER,
    fund_id                   NUMBER,
    activity_type             VARCHAR2(30),
    activity_id               NUMBER,
    activity_product_id       NUMBER,
    schedule_id               NUMBER,
    offer_type                VARCHAR2(30),
    document_class            VARCHAR2(15),
    document_id               NUMBER,
    product_level_type        VARCHAR2(30),
    product_id                NUMBER,
    reference_type            VARCHAR2(30),
    reference_id              NUMBER,
    utilization_type          VARCHAR2(30),
    total_amount              NUMBER,
    old_total_amount          NUMBER,
    pay_over_all_flag         BOOLEAN, -- Bugfix 5154157
    total_units               NUMBER,
    old_total_units           NUMBER,
    quantity                  NUMBER,
    uom_code                  VARCHAR2(3),
    cust_account_id           NUMBER,
    relationship_type         VARCHAR2(30),
    related_cust_account_id   NUMBER,
    buy_group_cust_account_id NUMBER,
    buy_group_party_id        NUMBER,
    select_cust_children_flag VARCHAR2(1) := 'N',
    pay_to_customer           VARCHAR2(30),
    prorate_earnings_flag     VARCHAR2(1),
    adjustment_type_id        NUMBER := NULL,
    end_date                  DATE,
    run_mode                  VARCHAR2(30),
    check_sales_rep_flag      VARCHAR2(1),
    group_by_offer            VARCHAR2(1),
    offer_payment_method      VARCHAR2(30), -- internal use; do not populate
    utiz_currency_code        VARCHAR2(15),
    bill_to_site_use_id       NUMBER,
    utilization_id            NUMBER, -- Added For Bug 8402328
    autopay_check             VARCHAR2(15), -- Added for Claims-Mulitcurrency ER
    year_name                 VARCHAR2(15) -- Added for Bug 12657908
    );

  --//Added for Claims-Mulitcurrency ER
  TYPE currency_rec_type IS RECORD(
    functional_currency_code  VARCHAR2(15),
    offer_currency_code       VARCHAR2(15),
    universal_currency_code   VARCHAR2(15),
    claim_currency_code       VARCHAR2(15),
    transaction_currency_code VARCHAR2(15),
    association_currency_code VARCHAR2(15));

  PROCEDURE printlog(p_message IN VARCHAR2) IS
  BEGIN
    apps.fnd_file.put_line(apps.fnd_file.log, p_message);
    dbms_output.put_line(p_message);
  END printlog;
  --
  PROCEDURE printoutput(p_message IN VARCHAR2) IS
  BEGIN
    apps.fnd_file.put_line(apps.fnd_file.output, p_message);
    dbms_output.put_line(p_message);
  END printoutput;

  ---------------------------------------------------------------------
  -- PROCEDURE
  --    Init_Line_Util_Rec
  --
  -- HISTORY
  --    05/10/2001  mchang  Create.
  ---------------------------------------------------------------------
  PROCEDURE init_line_util_rec(x_line_util_rec OUT NOCOPY line_util_rec_type) IS
  BEGIN

    RETURN;
  END init_line_util_rec;

  PROCEDURE create_line_util(p_api_version      IN NUMBER
                            ,p_init_msg_list    IN VARCHAR2 := fnd_api.g_false
                            ,p_commit           IN VARCHAR2 := fnd_api.g_false
                            ,p_validation_level IN NUMBER := fnd_api.g_valid_level_full

                            ,x_return_status OUT NOCOPY VARCHAR2
                            ,x_msg_count     OUT NOCOPY NUMBER
                            ,x_msg_data      OUT NOCOPY VARCHAR2

                            ,p_line_util_rec IN line_util_rec_type
                            ,p_currency_rec  IN currency_rec_type
                            ,p_mode          IN VARCHAR2 := ozf_claim_utility_pvt.g_auto_mode
                            ,x_line_util_id  OUT NOCOPY NUMBER);

  ---------------------------------------------------------------------
  -- FUNCTION
  --    Calculate_FXGL_Amount
  --
  -- PURPOSE
  --    Returns FXGL amount of the claim line util
  --
  -- PARAMETERS
  --
  --
  -- NOTES
  -- created by azahmed For FXGL ER:
  -- 16-04-2009  psomyaju  Modified for Multicurrency ER.
  ---------------------------------------------------------------------
  FUNCTION calculate_fxgl_amount(p_line_util_rec IN line_util_rec_type
                                ,p_currency_rec  IN currency_rec_type)
    RETURN NUMBER IS

    CURSOR csr_funds_util_details(cv_utilization_id IN NUMBER) IS
      SELECT exchange_rate_type, exchange_rate_date, exchange_rate
        FROM ozf_funds_utilized_all_b
       WHERE utilization_id = cv_utilization_id;

    l_fu_plan_currency_code VARCHAR2(15);
    l_fu_exc_rate           NUMBER;
    l_fu_exc_date           DATE;
    l_fu_exc_type           VARCHAR2(30);
    l_return_status         VARCHAR2(1);
    l_utilized_amount       NUMBER := 0;
    l_fxgl_acctd_amount     NUMBER := 0;
    l_fu_plan_amount        NUMBER := 0;

  BEGIN

    IF p_currency_rec.claim_currency_code =
       p_currency_rec.transaction_currency_code AND
       p_currency_rec.claim_currency_code <>
       p_currency_rec.functional_currency_code
    THEN

      OPEN csr_funds_util_details(p_line_util_rec.utilization_id);
      FETCH csr_funds_util_details
        INTO l_fu_exc_type, l_fu_exc_date, l_fu_exc_rate;
      CLOSE csr_funds_util_details;

      IF p_line_util_rec.exchange_rate <> l_fu_exc_rate AND
         l_fu_exc_rate IS NOT NULL
      THEN
        ozf_utility_pvt.convert_currency(p_from_currency => p_currency_rec.transaction_currency_code
                                        ,p_to_currency   => p_currency_rec.functional_currency_code
                                        ,p_conv_type     => l_fu_exc_type
                                        ,p_conv_date     => l_fu_exc_date
                                        ,p_from_amount   => p_line_util_rec.amount
                                        ,x_return_status => l_return_status
                                        ,x_to_amount     => l_utilized_amount
                                        ,x_rate          => l_fu_exc_rate);
        IF l_return_status = fnd_api.g_ret_sts_error
        THEN
          RAISE fnd_api.g_exc_error;
        ELSIF l_return_status = fnd_api.g_ret_sts_unexp_error
        THEN
          RAISE fnd_api.g_exc_unexpected_error;
        END IF;
        l_fxgl_acctd_amount := p_line_util_rec.acctd_amount -
                               l_utilized_amount;

      END IF;
    ELSE
      l_fxgl_acctd_amount := 0;
    END IF;

    RETURN l_fxgl_acctd_amount;
  END calculate_fxgl_amount;

  ---------------------------------------------------------------------
  -- PROCEDURE
  --   Update_Fund_utils
  --   22-Oct-2005    Created     Sahana
  --   08-Aug-06      azahmed     Modified for FXGL Er
  --   21-Jan-08      psomyaju    Modified for Ship - Debit Claims
  --   19-Apr-09      psomyaju    Re-organized code for R12 multicurrency ER.
  --   24-Jun-09      BKUNJAN     Added parameter px_currency_rec.
  ---------------------------------------------------------------------
  PROCEDURE update_fund_utils(p_line_util_rec IN OUT NOCOPY line_util_rec_type
                             ,p_asso_amount   IN NUMBER
                             ,p_mode          IN VARCHAR2 := 'CALCULATE'
                             ,px_currency_rec IN OUT NOCOPY currency_rec_type
                             ,x_return_status OUT NOCOPY VARCHAR2
                             ,x_msg_count     OUT NOCOPY NUMBER
                             ,x_msg_data      OUT NOCOPY VARCHAR2) IS
    l_api_version CONSTANT NUMBER := 1.0;
    l_api_name    CONSTANT VARCHAR2(30) := ' Update_Fund_Utils';
    l_full_name   CONSTANT VARCHAR2(60) := g_pkg_name || '.' || l_api_name;
    l_return_status       VARCHAR2(1);
    l_line_util_rec       line_util_rec_type := p_line_util_rec;
    l_fu_req_currency     VARCHAR2(15);
    l_fu_fund_currency    VARCHAR2(15);
    l_claim_currency      VARCHAR2(15);
    l_fu_exc_date         DATE;
    l_fu_exc_type         VARCHAR2(30);
    l_fu_amt_rem          NUMBER := 0;
    l_fu_acctd_amt_rem    NUMBER := 0;
    l_fu_plan_amt_rem     NUMBER := 0;
    l_fu_univ_amt_rem     NUMBER := 0;
    l_fu_req_amt_rem      NUMBER := 0;
    l_rate                NUMBER := 0;
    l_reference_id        NUMBER := 0;
    l_source_object_class VARCHAR2(15);
    l_fu_plan_id          NUMBER;
    l_fu_plan_type        VARCHAR2(15);

    l_currency_rec currency_rec_type := px_currency_rec;

    CURSOR csr_fu_amt_rem(cv_utilization_id IN NUMBER) IS
      SELECT currency_code
            ,fund_request_currency_code
            ,exchange_rate_date
            ,exchange_rate_type
            ,nvl(acctd_amount_remaining, 0)
            ,nvl(plan_curr_amount_remaining, 0)
            ,reference_id
            ,plan_id
            ,plan_type
            ,plan_currency_code
        FROM ozf_funds_utilized_all_b
       WHERE utilization_id = cv_utilization_id;

    --csr_object_class added for Ship - Debit claims / Pranay
    --Bug# 8513457 fixed by ateotia (+)
    /*
    CURSOR  csr_object_class (cv_request_id IN NUMBER) IS
    SELECT  cla.source_object_class
    FROM    ozf_claims cla
          , ozf_claim_lines line
    WHERE   cla.claim_id = line.claim_id
      AND   line.activity_id = cv_request_id;
    */
    CURSOR csr_object_class(cv_claim_line_id IN NUMBER) IS
      SELECT cla.source_object_class
        FROM ozf_claims_all cla, ozf_claim_lines_all line
       WHERE cla.claim_id = line.claim_id
         AND line.claim_line_id = cv_claim_line_id;
    --Bug# 8513457 fixed by ateotia (-)

    CURSOR csr_claim_currency(cv_claim_line_id NUMBER) IS
      SELECT currency_code
        FROM ozf_claim_lines
       WHERE claim_line_id = cv_claim_line_id;
  BEGIN
    --------------------- initialize -----------------------
    SAVEPOINT update_fund_utils;

    IF ozf_debug_high_on
    THEN
      ozf_utility_pvt.debug_message(l_full_name || ': start');
    END IF;

    x_return_status := fnd_api.g_ret_sts_success;

    OPEN csr_fu_amt_rem(l_line_util_rec.utilization_id);
    FETCH csr_fu_amt_rem
      INTO l_fu_fund_currency
          ,l_fu_req_currency
          ,l_fu_exc_date
          ,l_fu_exc_type
          ,l_fu_acctd_amt_rem
          ,l_fu_plan_amt_rem
          ,l_reference_id
          ,l_fu_plan_id
          ,l_fu_plan_type
          ,l_currency_rec.transaction_currency_code;
    CLOSE csr_fu_amt_rem;

    --Bug# 8513457 fixed by ateotia (+)
    --OPEN csr_object_class(l_reference_id);
    OPEN csr_object_class(l_line_util_rec.claim_line_id);
    FETCH csr_object_class
      INTO l_source_object_class;
    CLOSE csr_object_class;

    IF ozf_debug_high_on
    THEN
      ozf_utility_pvt.debug_message('l_line_util_rec.claim_line_id: ' ||
                                    l_line_util_rec.claim_line_id);
      ozf_utility_pvt.debug_message('l_source_object_class: ' ||
                                    l_source_object_class);
    END IF;
    --Bug# 8513457 fixed by ateotia (-)

    --association_currency_code set in Update_Group_Line_Util program unit. In some processes, like
    --public API which use other route for association this global variable will not set.
    --Below logic will handle those scenarios.

    IF l_currency_rec.association_currency_code IS NULL
    THEN
      OPEN csr_claim_currency(l_line_util_rec.claim_line_id);
      FETCH csr_claim_currency
        INTO l_currency_rec.claim_currency_code;
      CLOSE csr_claim_currency;

      IF l_currency_rec.claim_currency_code =
         l_currency_rec.transaction_currency_code
      THEN
        l_currency_rec.association_currency_code := l_currency_rec.transaction_currency_code;
      ELSE
        l_currency_rec.association_currency_code := l_currency_rec.functional_currency_code;
      END IF;
    END IF;

    --Set UNIVERSAL currency from profile.
    l_currency_rec.universal_currency_code := fnd_profile.value('OZF_TP_COMMON_CURRENCY');

    IF ozf_debug_high_on
    THEN
      ozf_utility_pvt.debug_message('Currencies at Update_Fund_Utils API...');
      ozf_utility_pvt.debug_message('p_asso_amount = ' || p_asso_amount);
      ozf_utility_pvt.debug_message('l_line_util_rec.amount = ' ||
                                    l_line_util_rec.amount);
      ozf_utility_pvt.debug_message('l_line_util_rec.claim_line_id = ' ||
                                    l_line_util_rec.claim_line_id);
      ozf_utility_pvt.debug_message('l_currency_rec.association_currency_code = ' ||
                                    l_currency_rec.association_currency_code);
      ozf_utility_pvt.debug_message('l_currency_rec.claim_currency_code = ' ||
                                    l_currency_rec.claim_currency_code);
      ozf_utility_pvt.debug_message('l_currency_rec.transaction_currency_code = ' ||
                                    l_currency_rec.transaction_currency_code);
      ozf_utility_pvt.debug_message('l_currency_rec.functional_currency_code = ' ||
                                    l_currency_rec.functional_currency_code);
      ozf_utility_pvt.debug_message('l_currency_rec.universal_currency_code  = ' ||
                                    l_currency_rec.universal_currency_code);
    END IF;
    --p_asso_amount (earning/adjustment amount) passed in TRANSACTIONAL or FUNCTIONAL
    --currency. Need to calculate amount remaining to be reduced accordingly.
    IF l_currency_rec.association_currency_code =
       l_currency_rec.transaction_currency_code
    THEN
      --l_fu_plan_amt_rem :=  l_fu_plan_amt_rem + NVL(p_asso_amount,0);
      l_fu_plan_amt_rem := -nvl(p_asso_amount, 0);
      IF l_fu_plan_amt_rem IS NOT NULL AND l_fu_plan_amt_rem <> 0
      THEN
        ozf_utility_pvt.convert_currency(p_from_currency => l_currency_rec.transaction_currency_code
                                        ,p_to_currency   => l_currency_rec.functional_currency_code
                                        ,p_conv_date     => l_fu_exc_date
                                        ,p_conv_type     => l_fu_exc_type
                                        ,p_from_amount   => l_fu_plan_amt_rem
                                        ,x_return_status => l_return_status
                                        ,x_to_amount     => l_fu_acctd_amt_rem
                                        ,x_rate          => l_rate);
        IF l_return_status = fnd_api.g_ret_sts_error
        THEN
          RAISE fnd_api.g_exc_error;
        ELSIF l_return_status = fnd_api.g_ret_sts_unexp_error
        THEN
          RAISE fnd_api.g_exc_unexpected_error;
        END IF;
        IF l_fu_acctd_amt_rem IS NOT NULL
        THEN
          l_fu_acctd_amt_rem := ozf_utility_pvt.currround(l_fu_acctd_amt_rem
                                                         ,l_currency_rec.functional_currency_code);
        END IF;
      ELSE
        l_fu_acctd_amt_rem := 0;
      END IF;
    ELSE
      --l_fu_acctd_amt_rem := l_fu_acctd_amt_rem + NVL(p_asso_amount,0);
      l_fu_acctd_amt_rem := -nvl(p_asso_amount, 0);
      IF l_fu_acctd_amt_rem IS NOT NULL AND l_fu_acctd_amt_rem <> 0
      THEN
        ozf_utility_pvt.convert_currency(p_from_currency => l_currency_rec.functional_currency_code
                                        ,p_to_currency   => l_currency_rec.transaction_currency_code
                                        ,p_conv_date     => l_fu_exc_date
                                        ,p_conv_type     => l_fu_exc_type
                                        ,p_from_amount   => l_fu_acctd_amt_rem
                                        ,x_return_status => l_return_status
                                        ,x_to_amount     => l_fu_plan_amt_rem
                                        ,x_rate          => l_rate);
        IF l_return_status = fnd_api.g_ret_sts_error
        THEN
          RAISE fnd_api.g_exc_error;
        ELSIF l_return_status = fnd_api.g_ret_sts_unexp_error
        THEN
          RAISE fnd_api.g_exc_unexpected_error;
        END IF;
        IF l_fu_plan_amt_rem IS NOT NULL
        THEN
          l_fu_plan_amt_rem := ozf_utility_pvt.currround(l_fu_plan_amt_rem
                                                        ,l_currency_rec.transaction_currency_code);
        END IF;
      ELSE
        l_fu_plan_amt_rem := 0;
      END IF;
    END IF;

    IF l_fu_plan_amt_rem <> 0 OR l_fu_acctd_amt_rem <> 0
    THEN
      IF l_fu_fund_currency = l_currency_rec.transaction_currency_code
      THEN
        l_fu_amt_rem := l_fu_plan_amt_rem;
      ELSIF l_fu_fund_currency = l_currency_rec.functional_currency_code
      THEN
        l_fu_amt_rem := l_fu_acctd_amt_rem;
      ELSE
        IF l_fu_plan_amt_rem IS NOT NULL AND l_fu_plan_amt_rem <> 0
        THEN
          ozf_utility_pvt.convert_currency(p_from_currency => l_currency_rec.transaction_currency_code
                                          ,p_to_currency   => l_fu_fund_currency
                                          ,p_conv_date     => l_fu_exc_date
                                          ,p_conv_type     => l_fu_exc_type
                                          ,p_from_amount   => l_fu_plan_amt_rem
                                          ,x_return_status => l_return_status
                                          ,x_to_amount     => l_fu_amt_rem
                                          ,x_rate          => l_rate);
          IF l_return_status = fnd_api.g_ret_sts_error
          THEN
            RAISE fnd_api.g_exc_error;
          ELSIF l_return_status = fnd_api.g_ret_sts_unexp_error
          THEN
            RAISE fnd_api.g_exc_unexpected_error;
          END IF;
          IF l_fu_amt_rem IS NOT NULL
          THEN
            l_fu_amt_rem := ozf_utility_pvt.currround(l_fu_amt_rem
                                                     ,l_fu_fund_currency);
          END IF;
        ELSE
          l_fu_amt_rem := 0;
        END IF;
      END IF;

      IF l_currency_rec.universal_currency_code =
         l_currency_rec.transaction_currency_code
      THEN
        l_fu_univ_amt_rem := l_fu_plan_amt_rem;
      ELSIF l_currency_rec.universal_currency_code =
            l_currency_rec.functional_currency_code
      THEN
        l_fu_univ_amt_rem := l_fu_acctd_amt_rem;
      ELSIF l_currency_rec.universal_currency_code = l_fu_fund_currency
      THEN
        l_fu_univ_amt_rem := l_fu_amt_rem;
      ELSE
        IF l_fu_plan_amt_rem IS NOT NULL AND l_fu_plan_amt_rem <> 0
        THEN
          ozf_utility_pvt.convert_currency(p_from_currency => l_currency_rec.transaction_currency_code
                                          ,p_to_currency   => l_currency_rec.universal_currency_code
                                          ,p_conv_date     => l_fu_exc_date
                                          ,p_conv_type     => l_fu_exc_type
                                          ,p_from_amount   => l_fu_plan_amt_rem
                                          ,x_return_status => l_return_status
                                          ,x_to_amount     => l_fu_univ_amt_rem
                                          ,x_rate          => l_rate);
          IF l_return_status = fnd_api.g_ret_sts_error
          THEN
            RAISE fnd_api.g_exc_error;
          ELSIF l_return_status = fnd_api.g_ret_sts_unexp_error
          THEN
            RAISE fnd_api.g_exc_unexpected_error;
          END IF;
          IF l_fu_univ_amt_rem IS NOT NULL
          THEN
            l_fu_univ_amt_rem := ozf_utility_pvt.currround(l_fu_univ_amt_rem
                                                          ,l_currency_rec.universal_currency_code);
          END IF;
        ELSE
          l_fu_univ_amt_rem := 0;
        END IF;
      END IF;

      IF l_fu_req_currency = l_currency_rec.transaction_currency_code
      THEN
        l_fu_req_amt_rem := l_fu_plan_amt_rem;
      ELSIF l_fu_req_currency = l_currency_rec.functional_currency_code
      THEN
        l_fu_req_amt_rem := l_fu_acctd_amt_rem;
      ELSIF l_fu_req_currency = l_fu_fund_currency
      THEN
        l_fu_req_amt_rem := l_fu_amt_rem;
      ELSIF l_fu_req_currency = l_currency_rec.universal_currency_code
      THEN
        l_fu_req_amt_rem := l_fu_univ_amt_rem;
      ELSE
        IF l_fu_plan_amt_rem IS NOT NULL AND l_fu_plan_amt_rem <> 0
        THEN
          ozf_utility_pvt.convert_currency(p_from_currency => l_currency_rec.transaction_currency_code
                                          ,p_to_currency   => l_fu_req_currency
                                          ,p_conv_date     => l_fu_exc_date
                                          ,p_conv_type     => l_fu_exc_type
                                          ,p_from_amount   => l_fu_plan_amt_rem
                                          ,x_return_status => l_return_status
                                          ,x_to_amount     => l_fu_req_amt_rem
                                          ,x_rate          => l_rate);
          IF l_return_status = fnd_api.g_ret_sts_error
          THEN
            RAISE fnd_api.g_exc_error;
          ELSIF l_return_status = fnd_api.g_ret_sts_unexp_error
          THEN
            RAISE fnd_api.g_exc_unexpected_error;
          END IF;
          IF l_fu_req_amt_rem IS NOT NULL
          THEN
            l_fu_req_amt_rem := ozf_utility_pvt.currround(l_fu_req_amt_rem
                                                         ,l_fu_req_currency);
          END IF;
        ELSE
          l_fu_req_amt_rem := 0;
        END IF;
      END IF;

    ELSE
      l_fu_amt_rem      := 0;
      l_fu_univ_amt_rem := 0;
    END IF;

    IF ozf_debug_high_on
    THEN
      ozf_utility_pvt.debug_message('l_fu_amt_rem  : ' || l_fu_amt_rem);
      ozf_utility_pvt.debug_message('l_fu_acctd_amt_rem  : ' ||
                                    l_fu_acctd_amt_rem);
      ozf_utility_pvt.debug_message('l_fu_plan_amt_rem  : ' ||
                                    l_fu_plan_amt_rem);
      ozf_utility_pvt.debug_message('l_fu_univ_amt_rem  : ' ||
                                    l_fu_univ_amt_rem);
      ozf_utility_pvt.debug_message('l_fu_req_amt_rem  : ' ||
                                    l_fu_req_amt_rem);
      ozf_utility_pvt.debug_message('l_line_util_rec.utilization_id  : ' ||
                                    l_line_util_rec.utilization_id);
    END IF;
    --Reduce utilization amount remaining columns of respective currencies.
    -- Fix for Bug 9776744
    IF nvl(l_source_object_class, 'X') <> 'SD_SUPPLIER'
    THEN
      UPDATE ozf_funds_utilized_all_b
         SET amount_remaining              = amount_remaining - l_fu_amt_rem
            ,acctd_amount_remaining        = acctd_amount_remaining -
                                             l_fu_acctd_amt_rem
            ,plan_curr_amount_remaining    = plan_curr_amount_remaining -
                                             l_fu_plan_amt_rem
            ,univ_curr_amount_remaining    = univ_curr_amount_remaining -
                                             l_fu_univ_amt_rem
            ,fund_request_amount_remaining = fund_request_amount_remaining -
                                             l_fu_req_amt_rem
       WHERE utilization_id = l_line_util_rec.utilization_id;

    END IF; --SD_SUPPLIER check

    -- Calculate FXGL for association amount.
    IF p_mode = 'CALCULATE'
    THEN

      l_line_util_rec.fxgl_acctd_amount     := calculate_fxgl_amount(l_line_util_rec
                                                                    ,l_currency_rec);
      l_line_util_rec.utilized_acctd_amount := l_line_util_rec.acctd_amount -
                                               l_line_util_rec.fxgl_acctd_amount;

      IF ozf_debug_high_on
      THEN
        ozf_utility_pvt.debug_message('l_line_util_rec.fxgl_acctd_amount  : ' ||
                                      l_line_util_rec.fxgl_acctd_amount);
        ozf_utility_pvt.debug_message('l_line_util_rec.utilized_acctd_amount : ' ||
                                      l_line_util_rec.utilized_acctd_amount);
      END IF;
      --nepanda : fix for bug # 9508390  - issue # 1
      l_line_util_rec.util_curr_amount := nvl(l_line_util_rec.util_curr_amount
                                             ,0) + l_fu_amt_rem;
      l_line_util_rec.plan_curr_amount := nvl(l_line_util_rec.plan_curr_amount
                                             ,0) + l_fu_req_amt_rem;
      l_line_util_rec.univ_curr_amount := nvl(l_line_util_rec.univ_curr_amount
                                             ,0) + l_fu_univ_amt_rem;

      IF ozf_debug_high_on
      THEN
        ozf_utility_pvt.debug_message('l_line_util_rec.util_curr_amount  : ' ||
                                      l_line_util_rec.util_curr_amount);
        ozf_utility_pvt.debug_message('l_line_util_rec.plan_curr_amount : ' ||
                                      l_line_util_rec.plan_curr_amount);
        ozf_utility_pvt.debug_message('l_line_util_rec.univ_curr_amount : ' ||
                                      l_line_util_rec.univ_curr_amount);
      END IF;

    END IF; --'CALCULATE' check

    p_line_util_rec := l_line_util_rec;

    fnd_msg_pub.count_and_get(p_encoded => fnd_api.g_false
                             ,p_count   => x_msg_count
                             ,p_data    => x_msg_data);

    IF ozf_debug_high_on
    THEN
      ozf_utility_pvt.debug_message(l_full_name || ': end');
    END IF;

  EXCEPTION
    WHEN fnd_api.g_exc_error THEN
      ROLLBACK TO update_fund_utils;
      x_return_status := fnd_api.g_ret_sts_error;
      fnd_msg_pub.count_and_get(p_encoded => fnd_api.g_false
                               ,p_count   => x_msg_count
                               ,p_data    => x_msg_data);

    WHEN fnd_api.g_exc_unexpected_error THEN
      ROLLBACK TO update_fund_utils;
      x_return_status := fnd_api.g_ret_sts_unexp_error;
      fnd_msg_pub.count_and_get(p_encoded => fnd_api.g_false
                               ,p_count   => x_msg_count
                               ,p_data    => x_msg_data);

    WHEN OTHERS THEN
      ROLLBACK TO update_fund_utils;
      x_return_status := fnd_api.g_ret_sts_unexp_error;
      IF fnd_msg_pub.check_msg_level(fnd_msg_pub.g_msg_lvl_unexp_error)
      THEN
        fnd_msg_pub.add_exc_msg(g_pkg_name, l_api_name);
      END IF;
      fnd_msg_pub.count_and_get(p_encoded => fnd_api.g_false
                               ,p_count   => x_msg_count
                               ,p_data    => x_msg_data);

  END update_fund_utils;

  ---------------------------------------------------------------------
  -- PROCEDURE
  --    Create_Line_Util_Tbl
  --
  -- HISTORY
  --    05/11/2001  mchang  Create.
  --    05/08/2006  azahmed  Modified for FXGL ER
  --    01/09/2009  kpatro   Modified for Bug 7658894
  ---------------------------------------------------------------------
  PROCEDURE create_line_util_tbl(p_api_version      IN NUMBER
                                ,p_init_msg_list    IN VARCHAR2 := fnd_api.g_false
                                ,p_commit           IN VARCHAR2 := fnd_api.g_false
                                ,p_validation_level IN NUMBER := fnd_api.g_valid_level_full

                                ,x_return_status OUT NOCOPY VARCHAR2
                                ,x_msg_data      OUT NOCOPY VARCHAR2
                                ,x_msg_count     OUT NOCOPY NUMBER

                                ,p_line_util_tbl IN line_util_tbl_type
                                ,p_currency_rec  IN currency_rec_type
                                ,p_mode          IN VARCHAR2 := ozf_claim_utility_pvt.g_auto_mode
                                ,x_error_index   OUT NOCOPY NUMBER) IS
    l_api_version CONSTANT NUMBER := 1.0;
    l_api_name    CONSTANT VARCHAR2(30) := 'Create_Line_Util_Tbl';
    l_full_name   CONSTANT VARCHAR2(60) := g_pkg_name || '.' || l_api_name;
    l_return_status VARCHAR2(1);

    l_claim_id            NUMBER;
    l_access              VARCHAR2(1) := 'N';
    l_line_util_amount    NUMBER;
    l_claim_currency_code VARCHAR2(15);
    l_claim_exc_rate      NUMBER;
    l_claim_exc_date      DATE;
    l_claim_exc_type      VARCHAR2(30);
    l_org_id              NUMBER;
    l_line_util_rec       line_util_rec_type;
    l_line_util_id        NUMBER;
    l_currency_rec        currency_rec_type := p_currency_rec;

    CURSOR csr_claim_exc(cv_claim_line_id IN NUMBER) IS
      SELECT cla.currency_code
            ,cla.exchange_rate_type
            ,cla.exchange_rate_date
            ,cla.exchange_rate
        FROM ozf_claims cla, ozf_claim_lines ln
       WHERE ln.claim_id = cla.claim_id
         AND ln.claim_line_id = cv_claim_line_id;

    CURSOR csr_claim_id(cv_claim_line_id IN NUMBER) IS
      SELECT claim_id
        FROM ozf_claim_lines
       WHERE claim_line_id = cv_claim_line_id;

    -- Bug fix 7463302
    CURSOR csr_claim_line_util_amount(cv_claim_line_util_id IN NUMBER) IS
      SELECT nvl(amount, 0)
        FROM ozf_claim_lines_util
       WHERE claim_line_util_id = cv_claim_line_util_id;

    CURSOR csr_function_currency(cv_org_id NUMBER) IS
      SELECT gs.currency_code
        FROM gl_sets_of_books gs, ozf_sys_parameters org
       WHERE org.set_of_books_id = gs.set_of_books_id
         AND org.org_id = cv_org_id;

    --nepanda Fix for Bug 9075792 - this code is moved to java layer to ClaimAssoVO
    /* CURSOR csr_line_util_amount(cv_claim_line_id      IN NUMBER,
                                 cv_claim_line_util_id IN NUMBER) IS
      SELECT SUM(amount)
      FROM  ozf_claim_lines_util
      WHERE claim_line_id = cv_claim_line_id
      AND   claim_line_util_id <> cv_claim_line_util_id;

    CURSOR csr_total_util_lines (cv_claim_line_id      IN NUMBER)IS
       SELECT COUNT(1),
              MAX(claim_line_util_id)
       FROM  ozf_claim_lines_util_all
       WHERE claim_line_id = cv_claim_line_id; */

    l_sum_amount          NUMBER := 0;
    l_last_record_num     NUMBER := 0;
    l_entered_diff_amount NUMBER := 0;
    l_line_util_tot_amt   NUMBER := 0;
    l_last_line_util_id   NUMBER;
    l_line_util_count     NUMBER := 0;

  BEGIN
    --------------------- initialize -----------------------
    SAVEPOINT create_line_util_tbl;

    IF ozf_debug_high_on
    THEN
      ozf_utility_pvt.debug_message(l_full_name || ': start');
    END IF;

    IF fnd_api.to_boolean(p_init_msg_list)
    THEN
      fnd_msg_pub.initialize;
    END IF;

    IF NOT fnd_api.compatible_api_call(l_api_version
                                      ,p_api_version
                                      ,l_api_name
                                      ,g_pkg_name)
    THEN
      RAISE fnd_api.g_exc_unexpected_error;
    END IF;

    x_return_status := fnd_api.g_ret_sts_success;

    --Claim Access Check: Abort process, if current user doesnt have access on claim.
    IF p_mode = ozf_claim_utility_pvt.g_manu_mode AND
       p_line_util_tbl.count > 0
    THEN
      FOR j IN p_line_util_tbl.first .. p_line_util_tbl.last
      LOOP
        IF p_line_util_tbl.exists(j)
        THEN

          OPEN csr_claim_id(p_line_util_tbl(j).claim_line_id);
          FETCH csr_claim_id
            INTO l_claim_id;
          CLOSE csr_claim_id;

          ozf_claim_utility_pvt.check_claim_access(p_api_version_number => 1.0
                                                  ,p_init_msg_list      => fnd_api.g_false
                                                  ,p_validation_level   => fnd_api.g_valid_level_full
                                                  ,p_commit             => fnd_api.g_false
                                                  ,p_object_id          => l_claim_id
                                                  ,p_object_type        => g_claim_object_type
                                                  ,p_user_id            => ozf_utility_pvt.get_resource_id(nvl(fnd_global.user_id
                                                                                                              ,-1))
                                                  ,x_return_status      => l_return_status
                                                  ,x_msg_count          => x_msg_count
                                                  ,x_msg_data           => x_msg_data
                                                  ,x_access             => l_access);

          IF l_access = 'N'
          THEN
            IF fnd_msg_pub.check_msg_level(fnd_msg_pub.g_msg_lvl_error)
            THEN
              fnd_message.set_name('OZF', 'OZF_CLAIM_NO_ACCESS');
              fnd_msg_pub.add;
            END IF;
            RAISE fnd_api.g_exc_error;
          END IF;
          EXIT;
        END IF;
      END LOOP;
    END IF;

    --------------------- Create Line Util Table -----------------------
    IF p_line_util_tbl.first IS NOT NULL
    THEN

      --Get currency and exchange rate details of claim line.
      OPEN csr_claim_exc(p_line_util_tbl(p_line_util_tbl.first)
                         .claim_line_id);
      FETCH csr_claim_exc
        INTO l_claim_currency_code
            ,l_claim_exc_type
            ,l_claim_exc_date
            ,l_claim_exc_rate;
      CLOSE csr_claim_exc;

      IF l_currency_rec.claim_currency_code IS NULL
      THEN
        l_currency_rec.claim_currency_code := l_claim_currency_code;
      END IF;

      --Get FUNCTIONAL currency from system parameters
      IF l_currency_rec.functional_currency_code IS NULL
      THEN
        OPEN csr_function_currency(mo_global.get_current_org_id());
        FETCH csr_function_currency
          INTO l_currency_rec.functional_currency_code;
        CLOSE csr_function_currency;
      END IF;

      --Set UNIVERSAL currency from profile.
      IF l_currency_rec.universal_currency_code IS NULL
      THEN
        l_currency_rec.universal_currency_code := fnd_profile.value('OZF_TP_COMMON_CURRENCY');
      END IF;
      IF l_currency_rec.transaction_currency_code IS NULL
      THEN
        l_currency_rec.transaction_currency_code := l_currency_rec.claim_currency_code;
      END IF;
      --Association process started for all earnings/adjustments of current
      --claim line.
      FOR i IN p_line_util_tbl.first .. p_line_util_tbl.last
      LOOP
        IF p_line_util_tbl.exists(i)
        THEN
          l_line_util_rec                    := p_line_util_tbl(i);
          l_line_util_rec.currency_code      := l_claim_currency_code;
          l_line_util_rec.exchange_rate_type := l_claim_exc_type;
          l_line_util_rec.exchange_rate_date := l_claim_exc_date;
          l_line_util_rec.exchange_rate      := l_claim_exc_rate;
          l_line_util_rec.org_id             := mo_global.get_current_org_id();

          l_line_util_rec.object_version_number := 1;
          l_line_util_rec.last_updated_by       := nvl(fnd_global.user_id
                                                      ,-1);
          l_line_util_rec.created_by            := nvl(fnd_global.user_id
                                                      ,-1);
          l_line_util_rec.last_update_login     := nvl(fnd_global.conc_login_id
                                                      ,-1);

          create_line_util(p_api_version      => 1.0
                          ,p_init_msg_list    => fnd_api.g_false
                          ,p_commit           => fnd_api.g_false
                          ,p_validation_level => p_validation_level
                          ,x_return_status    => l_return_status
                          ,x_msg_count        => x_msg_count
                          ,x_msg_data         => x_msg_data
                          ,p_line_util_rec    => l_line_util_rec
                          ,p_currency_rec     => l_currency_rec
                          ,p_mode             => ozf_claim_utility_pvt.g_auto_mode
                          ,x_line_util_id     => l_line_util_id);

          --l_last_line_util_id := l_line_util_id;
          --OZF_Utility_PVT.debug_message('l_last_line_util_id :'|| l_last_line_util_id );

          IF l_return_status = fnd_api.g_ret_sts_error
          THEN
            x_error_index := i;
            RAISE fnd_api.g_exc_error;
          ELSIF l_return_status = fnd_api.g_ret_sts_unexp_error
          THEN
            x_error_index := i;
            RAISE fnd_api.g_exc_unexpected_error;
          END IF;

        END IF;
      END LOOP;

      --nepanda Fix for Bug 9075792 - this code is moved to java layer to ClaimAssoVO
      --//BKUNJAN 7463302 - Rounding Issue : Modified the Code
      /*   OPEN  csr_total_util_lines(l_line_util_rec.claim_line_id);
        FETCH csr_total_util_lines INTO l_line_util_count,l_last_line_util_id;
        CLOSE csr_total_util_lines;

        IF OZF_DEBUG_HIGH_ON THEN
           OZF_Utility_PVT.debug_message('l_line_util_count :'|| l_line_util_count );
           OZF_Utility_PVT.debug_message('l_last_line_util_id :'|| l_last_line_util_id );
           OZF_Utility_PVT.debug_message('G_ENTERED_AMOUNT :'|| G_ENTERED_AMOUNT );
        END IF;


        IF l_line_util_count = 1 THEN
           l_line_util_amount := G_ENTERED_AMOUNT;

        ELSIF l_line_util_count > 1 THEN
           OPEN  csr_line_util_amount(l_line_util_rec.claim_line_id,l_last_line_util_id);
           FETCH csr_line_util_amount INTO l_line_util_tot_amt;
           CLOSE csr_line_util_amount;

           l_line_util_amount := G_ENTERED_AMOUNT - l_line_util_tot_amt;
        END IF;

        IF OZF_DEBUG_HIGH_ON THEN
           OZF_Utility_PVT.debug_message('l_line_util_tot_amt :'|| l_line_util_tot_amt );
           OZF_Utility_PVT.debug_message('l_line_util_amount :'|| l_line_util_amount );
        END IF;

        -- Update Last record of Claim line Util
        UPDATE ozf_claim_lines_util_all
        SET amount                = l_line_util_amount
        WHERE claim_line_util_id = l_last_line_util_id;
      */

      -- Update Claim Line: set earnings_associated_flag to TRUE
      UPDATE ozf_claim_lines_all
         SET earnings_associated_flag = 'T'
       WHERE claim_line_id = l_line_util_rec.claim_line_id;
    END IF;

    ------------------------- finish -------------------------------
    IF fnd_api.to_boolean(p_commit)
    THEN
      COMMIT;
    END IF;

    fnd_msg_pub.count_and_get(p_encoded => fnd_api.g_false
                             ,p_count   => x_msg_count
                             ,p_data    => x_msg_data);

    IF ozf_debug_high_on
    THEN
      ozf_utility_pvt.debug_message(l_full_name || ': end');
    END IF;

  EXCEPTION
    WHEN fnd_api.g_exc_error THEN
      ROLLBACK TO create_line_util_tbl;
      x_return_status := fnd_api.g_ret_sts_error;
      fnd_msg_pub.count_and_get(p_encoded => fnd_api.g_false
                               ,p_count   => x_msg_count
                               ,p_data    => x_msg_data);

    WHEN fnd_api.g_exc_unexpected_error THEN
      ROLLBACK TO create_line_util_tbl;
      x_return_status := fnd_api.g_ret_sts_unexp_error;
      fnd_msg_pub.count_and_get(p_encoded => fnd_api.g_false
                               ,p_count   => x_msg_count
                               ,p_data    => x_msg_data);

    WHEN OTHERS THEN
      ROLLBACK TO create_line_util_tbl;
      x_return_status := fnd_api.g_ret_sts_unexp_error;
      IF fnd_msg_pub.check_msg_level(fnd_msg_pub.g_msg_lvl_unexp_error)
      THEN
        fnd_msg_pub.add_exc_msg(g_pkg_name, l_api_name);
      END IF;
      fnd_msg_pub.count_and_get(p_encoded => fnd_api.g_false
                               ,p_count   => x_msg_count
                               ,p_data    => x_msg_data);

  END create_line_util_tbl;

  ---------------------------------------------------------------------
  -- PROCEDURE
  --    Create_Line_Util
  --
  -- HISTORY
  --    05/10/2001  mchang  Create.
  --    07/22/2002  yizhang add p_mode for security check
  --    05/08/2006  azahmed Modified for FXGL ER
  ---------------------------------------------------------------------
  PROCEDURE create_line_util(p_api_version      IN NUMBER
                            ,p_init_msg_list    IN VARCHAR2 := fnd_api.g_false
                            ,p_commit           IN VARCHAR2 := fnd_api.g_false
                            ,p_validation_level IN NUMBER := fnd_api.g_valid_level_full

                            ,x_return_status OUT NOCOPY VARCHAR2
                            ,x_msg_count     OUT NOCOPY NUMBER
                            ,x_msg_data      OUT NOCOPY VARCHAR2

                            ,p_line_util_rec IN line_util_rec_type
                            ,p_currency_rec  IN currency_rec_type
                            ,p_mode          IN VARCHAR2 := ozf_claim_utility_pvt.g_auto_mode
                            ,x_line_util_id  OUT NOCOPY NUMBER) IS
    l_api_version CONSTANT NUMBER := 1.0;
    l_api_name    CONSTANT VARCHAR2(30) := 'Create_Line_Util';
    l_full_name   CONSTANT VARCHAR2(60) := g_pkg_name || '.' || l_api_name;
    l_return_status VARCHAR2(1);

    l_line_util_rec     line_util_rec_type := p_line_util_rec;
    l_currency_rec      currency_rec_type := p_currency_rec;
    l_line_util_count   NUMBER;
    l_line_acctd_amount NUMBER;

    l_line_util_amount      NUMBER;
    l_exchange_rate_type    VARCHAR2(30);
    l_exchange_rate_date    DATE;
    l_exchange_rate         NUMBER;
    l_convert_exchange_rate NUMBER;

    CURSOR csr_line_util_seq IS
      SELECT ozf_claim_lines_util_all_s.nextval FROM dual;

    CURSOR csr_line_util_count(cv_line_util_id IN NUMBER) IS
      SELECT COUNT(claim_line_util_id)
        FROM ozf_claim_lines_util
       WHERE claim_line_util_id = cv_line_util_id;

  BEGIN
    --------------------- initialize -----------------------
    SAVEPOINT create_line_util;

    IF ozf_debug_high_on
    THEN
      ozf_utility_pvt.debug_message(l_full_name || ': start');
    END IF;

    IF fnd_api.to_boolean(p_init_msg_list)
    THEN
      fnd_msg_pub.initialize;
    END IF;

    IF NOT fnd_api.compatible_api_call(l_api_version
                                      ,p_api_version
                                      ,l_api_name
                                      ,g_pkg_name)
    THEN
      RAISE fnd_api.g_exc_unexpected_error;
    END IF;

    x_return_status := fnd_api.g_ret_sts_success;

    IF p_line_util_rec.amount IS NOT NULL
    THEN

      IF l_line_util_rec.utilization_id > -1
      THEN
        IF l_currency_rec.association_currency_code =
           l_currency_rec.transaction_currency_code
        THEN
          IF l_line_util_rec.amount IS NOT NULL AND
             l_line_util_rec.amount <> 0
          THEN
            -- Bugfix 5528210
            ozf_utility_pvt.convert_currency(p_from_currency => l_currency_rec.association_currency_code
                                            ,p_to_currency   => l_currency_rec.functional_currency_code
                                            ,p_conv_type     => l_line_util_rec.exchange_rate_type
                                            ,p_conv_date     => l_line_util_rec.exchange_rate_date
                                            ,p_from_amount   => l_line_util_rec.amount
                                            ,x_return_status => l_return_status
                                            ,x_to_amount     => l_line_util_rec.acctd_amount
                                            ,x_rate          => l_convert_exchange_rate);
            IF l_return_status = fnd_api.g_ret_sts_error
            THEN
              RAISE fnd_api.g_exc_error;
            ELSIF l_return_status = fnd_api.g_ret_sts_unexp_error
            THEN
              RAISE fnd_api.g_exc_unexpected_error;
            END IF;
          ELSE
            l_line_util_rec.acctd_amount := 0;
          END IF;
        ELSE
          l_line_util_rec.acctd_amount := l_line_util_rec.amount;
        END IF;
      END IF;

      l_line_util_rec.acctd_amount := ozf_utility_pvt.currround(l_line_util_rec.acctd_amount
                                                               ,l_currency_rec.functional_currency_code);
      l_line_util_rec.amount       := ozf_utility_pvt.currround(l_line_util_rec.amount
                                                               ,l_currency_rec.association_currency_code);

      IF ozf_debug_high_on
      THEN
        ozf_utility_pvt.debug_message('l_line_util_rec.acctd_amount = ' ||
                                      l_line_util_rec.acctd_amount);
        ozf_utility_pvt.debug_message('l_line_util_rec.utilization_id = ' ||
                                      l_line_util_rec.utilization_id);
        ozf_utility_pvt.debug_message('l_line_util_rec.amount - Before Update_Fund_Utils() : ' ||
                                      l_line_util_rec.amount);
      END IF;

      update_fund_utils(p_line_util_rec => l_line_util_rec
                       ,p_asso_amount   => -nvl(l_line_util_rec.amount, 0)
                       ,p_mode          => 'CALCULATE'
                       ,px_currency_rec => l_currency_rec
                       ,x_return_status => l_return_status
                       ,x_msg_count     => x_msg_count
                       ,x_msg_data      => x_msg_data);

      IF l_return_status = fnd_api.g_ret_sts_error
      THEN
        RAISE fnd_api.g_exc_error;
      ELSIF l_return_status = fnd_api.g_ret_sts_unexp_error
      THEN
        RAISE fnd_api.g_exc_unexpected_error;
      END IF;

      IF ozf_debug_high_on
      THEN
        ozf_utility_pvt.debug_message('l_line_util_rec.amount - After Update_Fund_Utils() : ' ||
                                      l_line_util_rec.amount);
      END IF;

    END IF; --l_utilization_id > -1 check

    --Since, amount column of OZF_CLAIM_LINES_UTIL table expect amount in CLAIM currency. Hence, need to convert
    --l_line_util_rec.amount into CLAIM currency, before creation of association record.
    IF l_line_util_rec.amount IS NOT NULL AND l_line_util_rec.amount <> 0
    THEN
      IF l_currency_rec.association_currency_code <>
         l_currency_rec.claim_currency_code
      THEN
        ozf_utility_pvt.convert_currency(p_from_currency => l_currency_rec.association_currency_code
                                        ,p_to_currency   => l_currency_rec.claim_currency_code
                                        ,p_conv_type     => l_line_util_rec.exchange_rate_type
                                         --, p_conv_rate     => l_line_util_rec.exchange_rate
                                        ,p_conv_date     => l_line_util_rec.exchange_rate_date
                                        ,p_from_amount   => l_line_util_rec.amount
                                        ,x_return_status => l_return_status
                                        ,x_to_amount     => l_line_util_amount
                                        ,x_rate          => l_convert_exchange_rate);

        IF l_return_status = fnd_api.g_ret_sts_error
        THEN
          RAISE fnd_api.g_exc_error;
        ELSIF l_return_status = fnd_api.g_ret_sts_unexp_error
        THEN
          RAISE fnd_api.g_exc_unexpected_error;
        END IF;

        IF ozf_debug_high_on
        THEN
          ozf_utility_pvt.debug_message('p_from_currency : ' ||
                                        l_currency_rec.association_currency_code);
          ozf_utility_pvt.debug_message('p_to_currency   : ' ||
                                        l_currency_rec.claim_currency_code);
          ozf_utility_pvt.debug_message('p_conv_type     : ' ||
                                        l_line_util_rec.exchange_rate_type);
          ozf_utility_pvt.debug_message('p_conv_rate     : ' ||
                                        l_line_util_rec.exchange_rate);
          ozf_utility_pvt.debug_message('p_conv_date     : ' ||
                                        l_line_util_rec.exchange_rate_date);
          ozf_utility_pvt.debug_message('p_from_amount   : ' ||
                                        l_line_util_rec.amount);
          ozf_utility_pvt.debug_message('x_to_amount     : ' ||
                                        l_line_util_amount);
        END IF;

        IF l_line_util_amount IS NOT NULL AND l_line_util_amount <> 0
        THEN
          l_line_util_rec.amount := ozf_utility_pvt.currround(l_line_util_amount
                                                             ,l_currency_rec.claim_currency_code);
        END IF;
      END IF;
    ELSE
      l_line_util_rec.amount := 0;
    END IF;
    IF ozf_debug_high_on
    THEN
      ozf_utility_pvt.debug_message('Inserting association record...');
      ozf_utility_pvt.debug_message('claim_line_id : ' ||
                                    l_line_util_rec.claim_line_id);
      ozf_utility_pvt.debug_message('utilization_id : ' ||
                                    l_line_util_rec.utilization_id);
      ozf_utility_pvt.debug_message('amount         : ' ||
                                    l_line_util_rec.amount);
      ozf_utility_pvt.debug_message('acctd amount         : ' ||
                                    l_line_util_rec.acctd_amount);
    END IF;

    IF l_line_util_rec.claim_line_util_id IS NULL
    THEN
      LOOP
        OPEN csr_line_util_seq;
        FETCH csr_line_util_seq
          INTO l_line_util_rec.claim_line_util_id;
        CLOSE csr_line_util_seq;
        -- Check the uniqueness of the identifier
        OPEN csr_line_util_count(l_line_util_rec.claim_line_util_id);
        FETCH csr_line_util_count
          INTO l_line_util_count;
        CLOSE csr_line_util_count;
        -- Exit when the identifier uniqueness is established
        EXIT WHEN l_line_util_count = 0;
      END LOOP;
    END IF;

    INSERT INTO ozf_claim_lines_util_all
      (claim_line_util_id
      ,object_version_number
      ,last_update_date
      ,last_updated_by
      ,creation_date
      ,created_by
      ,last_update_login
      ,claim_line_id
      ,utilization_id
      ,amount
      ,currency_code
      ,exchange_rate_type
      ,exchange_rate_date
      ,exchange_rate
      ,acctd_amount
      ,util_curr_amount
      ,plan_curr_amount
      ,univ_curr_amount
      ,scan_unit
      ,activity_product_id
      ,uom_code
      ,quantity
      ,org_id
      ,fxgl_acctd_amount
      ,utilized_acctd_amount)
    VALUES
      (l_line_util_rec.claim_line_util_id
      ,l_line_util_rec.object_version_number
      ,SYSDATE
      ,l_line_util_rec.last_updated_by
      ,SYSDATE
      ,l_line_util_rec.created_by
      ,l_line_util_rec.last_update_login
      ,l_line_util_rec.claim_line_id
      ,l_line_util_rec.utilization_id
      ,l_line_util_rec.amount
      ,l_line_util_rec.currency_code
      ,l_line_util_rec.exchange_rate_type
      ,l_line_util_rec.exchange_rate_date
      ,l_line_util_rec.exchange_rate
      ,l_line_util_rec.acctd_amount
      ,l_line_util_rec.util_curr_amount
      ,l_line_util_rec.plan_curr_amount
      ,l_line_util_rec.univ_curr_amount
      ,l_line_util_rec.scan_unit
      ,l_line_util_rec.activity_product_id
      ,l_line_util_rec.uom_code
      ,l_line_util_rec.quantity
      ,l_line_util_rec.org_id
      ,l_line_util_rec.fxgl_acctd_amount
      ,l_line_util_rec.utilized_acctd_amount

       );

    ------------------------- finish -------------------------------
    x_line_util_id := l_line_util_rec.claim_line_util_id;

    -- Check for commit
    IF fnd_api.to_boolean(p_commit)
    THEN
      COMMIT;
    END IF;

    fnd_msg_pub.count_and_get(p_encoded => fnd_api.g_false
                             ,p_count   => x_msg_count
                             ,p_data    => x_msg_data);

    IF ozf_debug_high_on
    THEN
      ozf_utility_pvt.debug_message(l_full_name || ': end');
    END IF;

  EXCEPTION
    WHEN fnd_api.g_exc_error THEN
      ROLLBACK TO create_line_util;
      x_return_status := fnd_api.g_ret_sts_error;
      fnd_msg_pub.count_and_get(p_encoded => fnd_api.g_false
                               ,p_count   => x_msg_count
                               ,p_data    => x_msg_data);

    WHEN fnd_api.g_exc_unexpected_error THEN
      ROLLBACK TO create_line_util;
      x_return_status := fnd_api.g_ret_sts_unexp_error;
      fnd_msg_pub.count_and_get(p_encoded => fnd_api.g_false
                               ,p_count   => x_msg_count
                               ,p_data    => x_msg_data);

    WHEN OTHERS THEN
      ROLLBACK TO create_line_util;
      x_return_status := fnd_api.g_ret_sts_unexp_error;
      IF fnd_msg_pub.check_msg_level(fnd_msg_pub.g_msg_lvl_unexp_error)
      THEN
        fnd_msg_pub.add_exc_msg(g_pkg_name, l_api_name);
      END IF;
      fnd_msg_pub.count_and_get(p_encoded => fnd_api.g_false
                               ,p_count   => x_msg_count
                               ,p_data    => x_msg_data);

  END create_line_util;

  ---------------------------------------------------------------------
  -- PROCEDURE
  --    Validate_Customer_Info
  --
  -- PURPOSE
  --    This procedure validates customer info
  --
  -- PARAMETERS
  --    p_cust_account : custome account id
  --    x_days_due     : days due
  --
  -- NOTES
  ---------------------------------------------------------------------
  PROCEDURE validate_customer_info(p_customer_info IN g_customer_info_csr%ROWTYPE
                                  ,x_return_status OUT NOCOPY VARCHAR2) IS
    CURSOR csr_cust_name(cv_cust_account_id IN NUMBER) IS
      SELECT concat(concat(party.party_name, ' (')
                   ,concat(ca.account_number, ') '))
        FROM hz_cust_accounts ca, hz_parties party
       WHERE ca.party_id = party.party_id
         AND ca.cust_account_id = cv_cust_account_id;

    l_cust_account_id NUMBER := p_customer_info.cust_account_id;
    l_cust_name_num   VARCHAR2(70);

  BEGIN
    -- Initialize API return status to sucess
    x_return_status := fnd_api.g_ret_sts_success;

    IF p_customer_info.claim_currency IS NULL
    THEN
      IF fnd_msg_pub.check_msg_level(fnd_msg_pub.g_msg_lvl_error)
      THEN
        OPEN csr_cust_name(l_cust_account_id);
        FETCH csr_cust_name
          INTO l_cust_name_num;
        CLOSE csr_cust_name;

        fnd_message.set_name('OZF', 'OZF_CLAIM_ATPY_CURRENCY_MISS');
        fnd_message.set_token('ID', l_cust_name_num);
        fnd_msg_pub.add;
      END IF;
      x_return_status := fnd_api.g_ret_sts_unexp_error;
    END IF;

    IF p_customer_info.payment_method IS NULL
    THEN
      IF fnd_msg_pub.check_msg_level(fnd_msg_pub.g_msg_lvl_error)
      THEN
        OPEN csr_cust_name(l_cust_account_id);
        FETCH csr_cust_name
          INTO l_cust_name_num;
        CLOSE csr_cust_name;

        fnd_message.set_name('OZF', 'OZF_CLAIM_ATPY_PYMTHD_MISS');
        fnd_message.set_token('ID', l_cust_name_num);
        fnd_msg_pub.add;
      END IF;
      x_return_status := fnd_api.g_ret_sts_unexp_error;
    END IF;

    IF p_customer_info.payment_method = 'CHECK'
    THEN
      IF p_customer_info.vendor_id IS NULL OR
         p_customer_info.vendor_site_id IS NULL
      THEN
        IF fnd_msg_pub.check_msg_level(fnd_msg_pub.g_msg_lvl_error)
        THEN
          OPEN csr_cust_name(l_cust_account_id);
          FETCH csr_cust_name
            INTO l_cust_name_num;
          CLOSE csr_cust_name;

          fnd_message.set_name('OZF', 'OZF_CLAIM_ATPY_VENDOR_MISS');
          fnd_message.set_token('ID', l_cust_name_num);
          fnd_msg_pub.add;
        END IF;
        x_return_status := fnd_api.g_ret_sts_unexp_error;
      END IF;
      --nepanda : fix for bug # 9539273 - issue #2
      /* ELSIF p_customer_info.payment_method = 'CREDIT_MEMO' THEN
      IF p_customer_info.site_use_id is NULL THEN
         IF FND_MSG_PUB.Check_Msg_Level (FND_MSG_PUB.G_MSG_LVL_ERROR) THEN
            OPEN csr_cust_name(l_cust_account_id);
            FETCH csr_cust_name INTO l_cust_name_num;
            CLOSE csr_cust_name;

            FND_MESSAGE.Set_Name('OZF', 'OZF_CLAIM_ATPY_SITEID_MISS');
            FND_MESSAGE.Set_Token('ID',l_cust_name_num);
            FND_MSG_PUB.ADD;
         END IF;
         x_return_status := FND_API.G_RET_STS_UNEXP_ERROR;
      END IF;*/
    END IF;

    /*
    IF p_customer_info.org_id is null THEN
       IF FND_MSG_PUB.Check_Msg_Level (FND_MSG_PUB.G_MSG_LVL_ERROR) THEN
          OPEN csr_cust_name(l_cust_account_id);
          FETCH csr_cust_name INTO l_cust_name_num;
          CLOSE csr_cust_name;

          FND_MESSAGE.Set_Name('OZF', 'OZF_CLAIM_ATPY_ORG_ID_MISS');
          FND_MESSAGE.Set_Token('ID',l_cust_name_num);
          FND_MSG_PUB.add;
       END IF;FND_MSG_PUB.ADD;
       x_return_status := FND_API.G_RET_STS_UNEXP_ERROR;
    END IF;
    */

  EXCEPTION
    WHEN OTHERS THEN
      x_return_status := fnd_api.g_ret_sts_unexp_error;
      IF fnd_msg_pub.check_msg_level(fnd_msg_pub.g_msg_lvl_unexp_error)
      THEN
        fnd_message.set_name('OZF', 'OZF_CLAIM_ATPY_CUSTOMER_ERR');
        fnd_msg_pub.add;
      END IF;
  END validate_customer_info;

  ---------------------------------------------------------------------
  -- PROCEDURE
  --    Get_Prorate_Earnings_Flag
  --
  -- HISTORY
  --    01/21/2003  yizhang  Create.
  ---------------------------------------------------------------------
  PROCEDURE get_prorate_earnings_flag(p_funds_util_flt        IN funds_util_flt_type
                                     ,x_prorate_earnings_flag OUT NOCOPY VARCHAR2) IS
    l_api_name  CONSTANT VARCHAR2(30) := 'Get_Prorate_Earnings_Flag';
    l_full_name CONSTANT VARCHAR2(60) := g_pkg_name || '.' || l_api_name;

    l_prorate_earnings_flag VARCHAR2(1);

    CURSOR csr_line_flag(cv_claim_line_id IN NUMBER) IS
      SELECT prorate_earnings_flag
        FROM apps.ozf_claim_lines
       WHERE claim_line_id = cv_claim_line_id;

    -- fix for bug 5042046
    CURSOR csr_system_flag IS
      SELECT nvl(prorate_earnings_flag, 'F')
        FROM apps.ozf_sys_parameters
       WHERE org_id = mo_global.get_current_org_id();

  BEGIN
    IF ozf_debug_high_on
    THEN
      ozf_utility_pvt.debug_message(l_full_name || ': start');
    END IF;

    IF p_funds_util_flt.prorate_earnings_flag IS NOT NULL
    THEN
      l_prorate_earnings_flag := p_funds_util_flt.prorate_earnings_flag;
    ELSE
      OPEN csr_line_flag(p_funds_util_flt.claim_line_id);
      FETCH csr_line_flag
        INTO l_prorate_earnings_flag;
      CLOSE csr_line_flag;

      IF l_prorate_earnings_flag IS NULL
      THEN
        OPEN csr_system_flag;
        FETCH csr_system_flag
          INTO l_prorate_earnings_flag;
        CLOSE csr_system_flag;
      END IF;
    END IF;
    x_prorate_earnings_flag := l_prorate_earnings_flag;

    IF ozf_debug_high_on
    THEN
      ozf_utility_pvt.debug_message(l_full_name || ': end');
    END IF;
  END get_prorate_earnings_flag;

  ---------------------------------------------------------------------
  -- PROCEDURE
  --    Get_End_Date
  --
  -- PURPOSE
  --    This procedure computes the date a payment has to be made based on last_paid_date and periodicity.
  --
  -- PARAMETERS
  --    p_frequency: type of peroidicity
  --    x_end_date:  date a payment should be made based on the periodicity
  --    x_return_status
  --
  -- NOTES
  ---------------------------------------------------------------------
  PROCEDURE get_end_date(p_frequency     IN VARCHAR2
                        ,x_end_date      OUT NOCOPY DATE
                        ,x_return_status OUT NOCOPY VARCHAR2) IS
    l_return_date DATE;
  BEGIN
    -- Initialize API return status to sucess
    x_return_status := fnd_api.g_ret_sts_success;

    IF p_frequency = 'MONTH'
    THEN
      SELECT otep.start_date - 1
        INTO l_return_date
        FROM apps.ozf_time_ent_period otep
       WHERE SYSDATE BETWEEN otep.start_date AND otep.end_date;
    ELSIF p_frequency = 'QTR'
    THEN
      SELECT oteq.start_date - 1
        INTO l_return_date
        FROM apps.ozf_time_ent_qtr oteq
       WHERE SYSDATE BETWEEN oteq.start_date AND oteq.end_date;
    ELSIF p_frequency = 'YEAR'
    THEN
      SELECT otey.start_date - 1
        INTO l_return_date
        FROM apps.ozf_time_ent_year otey
       WHERE SYSDATE BETWEEN otey.start_date AND otey.end_date;
    ELSE
      l_return_date := trunc(SYSDATE);
    END IF;
    x_end_date := l_return_date;
  EXCEPTION
    WHEN fnd_api.g_exc_error THEN
      x_return_status := fnd_api.g_ret_sts_error;
  END get_end_date;

  ---------------------------------------------------------------------
  -- PROCEDURE
  --   Get_Customer_For_Earnings
  --    Helper procedure called by Get_Utiz_Sql_Stmt only.
  --
  -- PARAMETERS
  --    p_cust_account_id   : Claiming customer account id
  --    p_relationship_type : Relationship_type
  --    p_related_cust_account_id : Related customer account id
  --    p_buy_group_party_id : Buying group/member party id
  --    p_select_cust_children_flag : Include all member earnings
  --
  -- HISTORY
  --   14-FEB-2003  yizhang  Create.
  --   05-MAY-2003  yizhang  Use FND_DSQL for dynamic sql and bind vars
  --   28-feb-06   azahmed   modified for bugfix 4958714
  ---------------------------------------------------------------------
  PROCEDURE get_customer_for_earnings(p_cust_account_id           IN NUMBER
                                     ,p_relationship_type         IN VARCHAR2
                                     ,p_related_cust_account_id   IN NUMBER
                                     ,p_buy_group_party_id        IN NUMBER
                                     ,p_select_cust_children_flag IN VARCHAR2) IS

    l_bg_is_parent_of_cust    NUMBER := 0;
    l_bg_is_parent_of_relcust NUMBER := 0;

    CURSOR csr_buy_group(cv_bg_party_id     IN NUMBER
                        ,cv_cust_account_id IN NUMBER) IS
      SELECT COUNT(seg.party_id)
        FROM apps.ams_party_market_segments seg, apps.hz_cust_accounts hca2
       WHERE seg.market_qualifier_reference = cv_bg_party_id
         AND hca2.party_id = seg.party_id
         AND seg.market_qualifier_type = 'BG'
         AND hca2.cust_account_id = cv_cust_account_id
         AND seg.market_qualifier_reference <> seg.party_id;

  BEGIN
    IF p_select_cust_children_flag IS NULL OR
       p_select_cust_children_flag = 'F' OR
       p_select_cust_children_flag = 'N'
    THEN
      -- not to include member earnings
      IF p_buy_group_party_id IS NOT NULL
      THEN
        fnd_dsql.add_text(' IN (SELECT c.cust_account_id FROM hz_cust_accounts c');
        fnd_dsql.add_text(' WHERE c.party_id = ');
        fnd_dsql.add_bind(p_buy_group_party_id);
        fnd_dsql.add_text(')');
      ELSIF p_relationship_type IS NOT NULL AND
            p_related_cust_account_id IS NULL
      THEN
        fnd_dsql.add_text(' IN (SELECT related_cust_account_id FROM hz_cust_acct_relate');
        fnd_dsql.add_text(' WHERE cust_account_id = ');
        fnd_dsql.add_bind(p_cust_account_id);
        fnd_dsql.add_text(' AND relationship_type = ');
        fnd_dsql.add_bind(p_relationship_type);
        fnd_dsql.add_text(')');
      ELSE
        fnd_dsql.add_text(' = ');
        IF p_related_cust_account_id IS NOT NULL
        THEN
          fnd_dsql.add_bind(p_related_cust_account_id);
        ELSE
          fnd_dsql.add_bind(p_cust_account_id);
        END IF;
      END IF;

    ELSIF p_select_cust_children_flag = 'T' OR
          p_select_cust_children_flag = 'Y'
    THEN
      -- to include member earnings
      IF p_buy_group_party_id IS NOT NULL
      THEN
        -- if buying group is parent of customer, do not include members
        OPEN csr_buy_group(p_buy_group_party_id, p_cust_account_id);
        FETCH csr_buy_group
          INTO l_bg_is_parent_of_cust;
        CLOSE csr_buy_group;

        IF l_bg_is_parent_of_cust <> 1
        THEN
          -- if buying group is parent of related customer, do not include members
          IF p_related_cust_account_id IS NOT NULL
          THEN
            OPEN csr_buy_group(p_buy_group_party_id
                              ,p_related_cust_account_id);
            FETCH csr_buy_group
              INTO l_bg_is_parent_of_relcust;
            CLOSE csr_buy_group;
          END IF;
        END IF;
      END IF;

      IF l_bg_is_parent_of_cust = 1 OR l_bg_is_parent_of_relcust = 1
      THEN
        fnd_dsql.add_text(' IN (SELECT c.cust_account_id ');
        fnd_dsql.add_text(' FROM hz_cust_accounts c ');
        fnd_dsql.add_text(' WHERE c.party_id = ');
        fnd_dsql.add_bind(p_buy_group_party_id);
        fnd_dsql.add_text('  OR  c.cust_account_id = ');
        IF p_related_cust_account_id IS NOT NULL
        THEN
          fnd_dsql.add_bind(p_related_cust_account_id);
        ELSE
          fnd_dsql.add_bind(p_cust_account_id);
        END IF;
        fnd_dsql.add_text(')');

      ELSE
        IF p_buy_group_party_id IS NOT NULL
        THEN
          fnd_dsql.add_text(' IN (SELECT c2.cust_account_id ');
          fnd_dsql.add_text(' FROM ams_party_market_segments sg, hz_cust_accounts c2 ');
          fnd_dsql.add_text(' WHERE sg.market_qualifier_type = ''BG'' ');
          fnd_dsql.add_text(' AND sg.party_id = c2.party_id ');
          fnd_dsql.add_text(' AND sg.market_qualifier_reference = ');
          fnd_dsql.add_bind(p_buy_group_party_id);
          fnd_dsql.add_text(')');
        ELSE
          -- Modified for Bugfix 5346249
          fnd_dsql.add_text(' IN (SELECT cust2.cust_account_id FROM  hz_cust_accounts cust2  ');
          fnd_dsql.add_text(' WHERE cust2.party_id IN (SELECT seg.party_id from ');
          fnd_dsql.add_text(' ams_party_market_segments seg ,hz_cust_accounts cust1 ');
          fnd_dsql.add_text(' where seg.market_qualifier_type = ''BG'' ');
          fnd_dsql.add_text(' and seg.market_qualifier_reference =  cust1.party_id ');
          fnd_dsql.add_text(' and cust1.cust_account_id = ');
          IF p_related_cust_account_id IS NOT NULL
          THEN
            fnd_dsql.add_bind(p_related_cust_account_id);
          ELSE
            fnd_dsql.add_bind(p_cust_account_id);
          END IF;
          fnd_dsql.add_text(')');
          fnd_dsql.add_text(')');
        END IF;
      END IF;
    END IF;

  END get_customer_for_earnings;

  ---------------------------------------------------------------------
  -- PROCEDURE
  --   Copy_Util_Flt
  --    Helper procedure called by Get_Utiz_Sql_Stmt only.
  --
  -- PARAMETERS
  --
  -- HISTORY
  --   16-FEB-2004  yizhang  Create.
  ---------------------------------------------------------------------
  PROCEDURE copy_util_flt(px_funds_util_flt IN OUT NOCOPY funds_util_flt_type) IS

    l_line_util_flt  funds_util_flt_type;
    l_offer_id       NUMBER;
    l_offer_type     VARCHAR2(30);
    l_reference_type VARCHAR2(30);
    l_reference_id   NUMBER;

    CURSOR csr_line_flt(cv_claim_line_id IN NUMBER) IS
      SELECT ln.activity_type
            ,ln.activity_id
            ,ln.offer_type
            ,ln.source_object_class
            ,ln.source_object_id
            ,ln.item_type
            ,ln.item_id
            ,ln.relationship_type
            ,ln.related_cust_account_id
            ,ln.buy_group_party_id
            ,ln.select_cust_children_flag
            ,cla.cust_account_id
            ,ln.earnings_end_date
            ,ln.claim_currency_amount
            ,cla.source_object_class
            ,cla.source_object_id
        FROM apps.ozf_claim_lines ln, apps.ozf_claims cla
       WHERE ln.claim_id = cla.claim_id
         AND ln.claim_line_id = cv_claim_line_id;

    CURSOR csr_offer_id(cv_request_id IN NUMBER) IS
      SELECT offer_id
        FROM apps.ozf_request_headers_all_b
       WHERE request_header_id = cv_request_id;

    CURSOR csr_offer_type(cv_offer_id IN NUMBER) IS
      SELECT offer_type
        FROM apps.ozf_offers
       WHERE qp_list_header_id = cv_offer_id;

    --Ship - Debit Enhancements / Added by Pranay
    CURSOR csr_sd_offer_id(cv_request_id IN NUMBER) IS
      SELECT offer_id
        FROM apps.ozf_sd_request_headers_all_b
       WHERE request_header_id = cv_request_id;
    --Bug# 8632964 fixed by anuj  muthsubr (+)
    CURSOR sysparam_accrual_flag_csr(p_resale_batch_id IN NUMBER) IS
      SELECT nvl(ospa.ship_debit_accrual_flag, 'F')
        FROM apps.ozf_sys_parameters_all ospa
            ,apps.ozf_resale_batches_all orba
       WHERE ospa.org_id = orba.org_id
         AND orba.resale_batch_id = p_resale_batch_id;

    l_accrual_flag VARCHAR2(1);
    --Bug# 8632964 fixed by anuj  muthsubr (-)

  BEGIN
    OPEN csr_line_flt(px_funds_util_flt.claim_line_id);
    FETCH csr_line_flt
      INTO l_line_util_flt.activity_type
          ,l_line_util_flt.activity_id
          ,l_line_util_flt.offer_type
          ,l_line_util_flt.document_class
          ,l_line_util_flt.document_id
          ,l_line_util_flt.product_level_type
          ,l_line_util_flt.product_id
          ,l_line_util_flt.relationship_type
          ,l_line_util_flt.related_cust_account_id
          ,l_line_util_flt.buy_group_party_id
          ,l_line_util_flt.select_cust_children_flag
          ,l_line_util_flt.cust_account_id
          ,l_line_util_flt.end_date
          ,l_line_util_flt.total_amount
          ,l_reference_type
          ,l_reference_id;
    CLOSE csr_line_flt;

    IF px_funds_util_flt.activity_type IS NULL
    THEN
      px_funds_util_flt.activity_type := l_line_util_flt.activity_type;
    END IF;
    IF px_funds_util_flt.activity_id IS NULL
    THEN
      px_funds_util_flt.activity_id := l_line_util_flt.activity_id;
    END IF;
    IF px_funds_util_flt.offer_type IS NULL
    THEN
      px_funds_util_flt.offer_type := l_line_util_flt.offer_type;
    END IF;
    IF px_funds_util_flt.document_class IS NULL
    THEN
      px_funds_util_flt.document_class := l_line_util_flt.document_class;
    END IF;
    IF px_funds_util_flt.document_id IS NULL
    THEN
      px_funds_util_flt.document_id := l_line_util_flt.document_id;
    END IF;
    IF px_funds_util_flt.product_level_type IS NULL
    THEN
      px_funds_util_flt.product_level_type := l_line_util_flt.product_level_type;
    END IF;
    IF px_funds_util_flt.product_id IS NULL
    THEN
      px_funds_util_flt.product_id := l_line_util_flt.product_id;
    END IF;
    IF px_funds_util_flt.relationship_type IS NULL
    THEN
      px_funds_util_flt.relationship_type := l_line_util_flt.relationship_type;
    END IF;
    IF px_funds_util_flt.related_cust_account_id IS NULL
    THEN
      px_funds_util_flt.related_cust_account_id := l_line_util_flt.related_cust_account_id;
    END IF;
    IF px_funds_util_flt.buy_group_party_id IS NULL
    THEN
      px_funds_util_flt.buy_group_party_id := l_line_util_flt.buy_group_party_id;
    END IF;
    IF px_funds_util_flt.select_cust_children_flag IS NULL
    THEN
      px_funds_util_flt.select_cust_children_flag := l_line_util_flt.select_cust_children_flag;
    END IF;
    IF px_funds_util_flt.cust_account_id IS NULL
    THEN
      px_funds_util_flt.cust_account_id := l_line_util_flt.cust_account_id;
    END IF;
    IF px_funds_util_flt.end_date IS NULL
    THEN
      px_funds_util_flt.end_date := l_line_util_flt.end_date;
    END IF;
    IF px_funds_util_flt.total_amount IS NULL
    THEN
      --      px_funds_util_flt.total_amount := l_line_util_flt.total_amount;
      NULL; -- If null, then leave as null implies line is to be deleted. Bugfix 5101106

    END IF;

    IF px_funds_util_flt.reference_type IS NULL
    THEN
      IF l_reference_type = 'REFERRAL'
      THEN
        px_funds_util_flt.reference_type := 'LEAD_REFERRAL';
        px_funds_util_flt.reference_id   := l_reference_id;
      ELSIF l_reference_type = 'BATCH'
      THEN
        --Bug# 8632964 fixed by anuj  muthsubr (+)
        OPEN sysparam_accrual_flag_csr(l_reference_id);
        FETCH sysparam_accrual_flag_csr
          INTO l_accrual_flag;
        CLOSE sysparam_accrual_flag_csr;
        IF l_accrual_flag = 'T'
        THEN
          -- Added for Bug 4997509
          px_funds_util_flt.reference_type := l_reference_type;
          px_funds_util_flt.reference_id   := l_reference_id;
        END IF;
        --Bug# 8632964 fixed by anuj  muthsubr (-)
      END IF;
    END IF;

    -- for special price request and soft fund, search by offer
    IF px_funds_util_flt.activity_type IN ('SPECIAL_PRICE', 'SOFT_FUND')
    THEN
      px_funds_util_flt.activity_type := 'OFFR';

      IF px_funds_util_flt.activity_id IS NOT NULL
      THEN
        OPEN csr_offer_id(px_funds_util_flt.activity_id);
        FETCH csr_offer_id
          INTO l_offer_id;
        CLOSE csr_offer_id;

        px_funds_util_flt.activity_id := l_offer_id;
      END IF;
    END IF;

    --Ship - Debit Enhancements / Added by Pranay
    IF px_funds_util_flt.activity_type = 'SD_REQUEST'
    THEN
      px_funds_util_flt.activity_type := 'OFFR';

      IF px_funds_util_flt.activity_id IS NOT NULL
      THEN
        OPEN csr_sd_offer_id(px_funds_util_flt.activity_id);
        FETCH csr_sd_offer_id
          INTO l_offer_id;
        CLOSE csr_sd_offer_id;
        px_funds_util_flt.activity_id := l_offer_id;
      END IF;
    END IF;

    -- set offer_type
    IF px_funds_util_flt.offer_type IS NULL AND
       px_funds_util_flt.activity_type = 'OFFR' AND
       px_funds_util_flt.activity_id IS NOT NULL
    THEN
      OPEN csr_offer_type(px_funds_util_flt.activity_id);
      FETCH csr_offer_type
        INTO l_offer_type;
      CLOSE csr_offer_type;

      px_funds_util_flt.offer_type := l_offer_type;
    END IF;

  END copy_util_flt;

  ---------------------------------------------------------------------
  -- PROCEDURE
  --   Get_Utiz_Sql_Stmt_Where_Clause
  --
  -- PARAMETERS
  --
  -- NOTE
  --
  -- HISTORY
  --   17-FEB-2004  yizhang  Create.
  --   08-AUg-2006  azahmed Modified for FXGL ER: Added condition fu.currency_code = claim_curr
  --   26-Jun-2009  kpatro  Corrected the GSCC Error.
  ---------------------------------------------------------------------
  PROCEDURE get_utiz_sql_stmt_where_clause(p_summary_view   IN VARCHAR2 := NULL
                                          ,p_funds_util_flt IN funds_util_flt_type
                                          ,px_currency_rec  IN OUT NOCOPY currency_rec_type) IS
    l_api_name  CONSTANT VARCHAR2(30) := 'Get_Utiz_Sql_Stmt_Where_Clause';
    l_full_name CONSTANT VARCHAR2(60) := g_pkg_name || '.' || l_api_name;
    l_return_status VARCHAR2(1);

    l_funds_util_flt  funds_util_flt_type := NULL;
    l_cust_account_id NUMBER := NULL;
    l_scan_data_flag  VARCHAR2(1) := 'N';
    l_org_id          NUMBER;
    l_currency_rec    currency_rec_type := px_currency_rec;

    CURSOR csr_claim_currency(cv_claim_line_id IN NUMBER) IS
      SELECT currency_code
        FROM apps.ozf_claim_lines
       WHERE claim_line_id = cv_claim_line_id;

  BEGIN
    --------------------- start -----------------------
    l_org_id := fnd_profile.value('QP_ORGANIZATION_ID');

    l_funds_util_flt := p_funds_util_flt;

    -- get claim currency
    OPEN csr_claim_currency(l_funds_util_flt.claim_line_id);
    FETCH csr_claim_currency
      INTO l_currency_rec.claim_currency_code;
    CLOSE csr_claim_currency;
    -- bug fix 4338584
    -- when a fund line utilization is updated from the UI the account id should be picked up from the record
    l_cust_account_id := l_funds_util_flt.cust_account_id;

    IF l_funds_util_flt.offer_type = 'SCAN_DATA'
    THEN
      l_scan_data_flag := 'Y';
    END IF;

    -- Added Debug For Multi Currency - kpatro
    IF ozf_debug_high_on
    THEN
      ozf_utility_pvt.debug_message('----- Get_Utiz_Sql_Stmt_Where_Clause:Start -----');
      ozf_utility_pvt.debug_message('l_currency_rec.claim_currency_code        : ' ||
                                    l_currency_rec.claim_currency_code);
      ozf_utility_pvt.debug_message('l_currency_rec.functional_currency_code   : ' ||
                                    l_currency_rec.functional_currency_code);
      ozf_utility_pvt.debug_message('l_currency_rec.transaction_currency_code  : ' ||
                                    l_currency_rec.transaction_currency_code);
      ozf_utility_pvt.debug_message('p_summary_view         : ' ||
                                    p_summary_view);
      ozf_utility_pvt.debug_message('l_funds_util_flt.utiz_currency_code         : ' ||
                                    l_funds_util_flt.utiz_currency_code);
      ozf_utility_pvt.debug_message('Process_By         : ' ||
                                    l_funds_util_flt.autopay_check);
      ozf_utility_pvt.debug_message('----------------------------Get_Utiz_Sql_Stmt_Where_Clause:End ------------');
    END IF;
    -- Bug5059770: Allow pay over for offers with zero available amount
    --Bug 5154157 : Reverted change made for bug 4927201 as this will not be called if
    --  total amount is to be overpaid
    -- Modified for FXGL ER: only accruals in claim currency must be returned
    -- Modified conditions for R12 Multicurrency / Pranay
    IF p_summary_view IS NULL OR p_summary_view <> 'DEL_GRP_LINE_UTIL'
    THEN

      -- Added For Multi Currency - kpatro
      IF (p_summary_view IN ('AUTOPAY', 'AUTOPAY_LINE') AND
         l_funds_util_flt.autopay_check = 'AUTOPAY')
      THEN
        fnd_dsql.add_text(' AND fu.plan_curr_amount_remaining <> 0 ');
      ELSE
        fnd_dsql.add_text(' AND (DECODE(NVL(''' ||
                          l_currency_rec.claim_currency_code ||
                          ''',fu.plan_currency_code), fu.plan_currency_code, fu.plan_curr_amount_remaining, fu.acctd_amount_remaining))<> 0 ');
      END IF;

    END IF;

    IF l_currency_rec.transaction_currency_code IS NOT NULL
    THEN
      --restrict for public API
      fnd_dsql.add_text(' AND fu.plan_currency_code = ''' ||
                        l_currency_rec.transaction_currency_code || ''''); --kdass
    END IF;

    -- for lead referral accruals, set utilization_type as LEAD_ACCRUAL
    -- Fixed for Bug4576309
    -- Modified for Bug4997509 to match ClaimAssoVO.getCommonWhereClause
    IF l_funds_util_flt.utilization_type IS NULL OR
       l_funds_util_flt.utilization_type = 'ACCRUAL'
    THEN
      IF l_funds_util_flt.reference_type = 'LEAD_REFERRAL'
      THEN
        fnd_dsql.add_text(' AND fu.utilization_type IN (''LEAD_ACCRUAL'', ''LEAD_ADJUSTMENT'') ');
      ELSIF l_funds_util_flt.reference_type = 'BATCH'
      THEN
        fnd_dsql.add_text(' AND fu.utilization_type = ''CHARGEBACK'' ');
      ELSE
        fnd_dsql.add_text('  AND fu.utilization_type IN (''ACCRUAL'', ''ADJUSTMENT'') ');
      END IF;
    ELSE
      fnd_dsql.add_text(' AND fu.utilization_type = ');
      fnd_dsql.add_bind(l_funds_util_flt.utilization_type);
    END IF;

    IF l_funds_util_flt.utilization_type = 'ADJUSTMENT'
    THEN
      fnd_dsql.add_text(' AND fu.cust_account_id IS NULL ');
    ELSE
      -- bug fix 4338584
      IF l_funds_util_flt.cust_account_id IS NOT NULL AND
         l_scan_data_flag = 'N' AND l_funds_util_flt.run_mode IS NOT NULL
      THEN
        fnd_dsql.add_text(' AND fu.cust_account_id');

        get_customer_for_earnings(p_cust_account_id           => l_funds_util_flt.cust_account_id
                                 ,p_relationship_type         => l_funds_util_flt.relationship_type
                                 ,p_related_cust_account_id   => l_funds_util_flt.related_cust_account_id
                                 ,p_buy_group_party_id        => l_funds_util_flt.buy_group_party_id
                                 ,p_select_cust_children_flag => l_funds_util_flt.select_cust_children_flag);

      ELSIF l_cust_account_id IS NOT NULL AND l_scan_data_flag = 'N'
      THEN
        fnd_dsql.add_text(' AND fu.cust_account_id = ');
        fnd_dsql.add_bind(l_cust_account_id);
      END IF;
    END IF;

    -- Add fund_id as search filter for claim autopay program.
    IF l_funds_util_flt.fund_id IS NOT NULL
    THEN
      fnd_dsql.add_text(' AND fu.fund_id = ');
      fnd_dsql.add_bind(l_funds_util_flt.fund_id);
    END IF;

    IF l_funds_util_flt.activity_id IS NOT NULL
    THEN
      fnd_dsql.add_text(' AND fu.plan_id = ');
      fnd_dsql.add_bind(l_funds_util_flt.activity_id);
    END IF;

    IF l_funds_util_flt.reference_type IS NOT NULL
    THEN
      fnd_dsql.add_text(' AND fu.reference_type = ');
      fnd_dsql.add_bind(l_funds_util_flt.reference_type);
    END IF;

    IF l_funds_util_flt.reference_id IS NOT NULL
    THEN
      fnd_dsql.add_text(' AND fu.reference_id = ');
      fnd_dsql.add_bind(l_funds_util_flt.reference_id);
    END IF;

    IF l_funds_util_flt.activity_product_id IS NOT NULL
    THEN
      fnd_dsql.add_text(' AND fu.activity_product_id = ');
      fnd_dsql.add_bind(l_funds_util_flt.activity_product_id);
    END IF;

    IF l_funds_util_flt.schedule_id IS NOT NULL AND l_scan_data_flag = 'N'
    THEN
      fnd_dsql.add_text(' AND fu.component_type = ''CSCH'' ');
      fnd_dsql.add_text(' AND fu.component_id = ');
      fnd_dsql.add_bind(l_funds_util_flt.schedule_id);
    END IF;

    -- fix for 4308165
    -- modified for bugfix 4990767
    IF l_funds_util_flt.document_class IS NOT NULL AND
       l_scan_data_flag = 'N'
    THEN
      IF l_funds_util_flt.document_class IN ('ORDER', 'TP_ORDER')
      THEN
        fnd_dsql.add_text(' AND fu.object_type = ');
        fnd_dsql.add_bind(l_funds_util_flt.document_class);
      END IF;
    END IF;

    IF l_funds_util_flt.document_id IS NOT NULL AND l_scan_data_flag = 'N'
    THEN
      IF l_funds_util_flt.document_class = 'TP_ORDER'
      THEN
        fnd_dsql.add_text(' AND fu.object_id IN (SELECT chargeback_line_id FROM ozf_chargeback_lines WHERE chargeback_header_id = ');
        fnd_dsql.add_bind(l_funds_util_flt.document_id);
        fnd_dsql.add_text(') ');
      ELSE
        IF l_funds_util_flt.document_class = 'ORDER'
        THEN
          fnd_dsql.add_text(' AND fu.object_id = ');
          fnd_dsql.add_bind(l_funds_util_flt.document_id);
        END IF;
      END IF;
    END IF;

    IF (l_funds_util_flt.product_level_type = 'PRODUCT' OR
       l_funds_util_flt.product_level_type IS NULL) AND
       l_funds_util_flt.product_id IS NOT NULL
    THEN
      fnd_dsql.add_text(' AND ((fu.product_level_type = ''PRODUCT'' ');
      fnd_dsql.add_text(' AND fu.product_id = ');
      fnd_dsql.add_bind(l_funds_util_flt.product_id);
      fnd_dsql.add_text(' ) OR (fu.product_level_type = ''FAMILY'' ');
      fnd_dsql.add_text(' AND fu.product_id IN (select category_id from mtl_item_categories ');
      fnd_dsql.add_text(' where inventory_item_id = ');
      fnd_dsql.add_bind(l_funds_util_flt.product_id);
      fnd_dsql.add_text(' and organization_id = ');
      fnd_dsql.add_bind(l_org_id);
      fnd_dsql.add_text(' ))) ');
    ELSIF l_funds_util_flt.product_level_type = 'FAMILY' AND
          l_funds_util_flt.product_id IS NOT NULL
    THEN
      fnd_dsql.add_text(' AND ((fu.product_level_type = ''FAMILY'' ');
      fnd_dsql.add_text(' AND fu.product_id = ');
      fnd_dsql.add_bind(l_funds_util_flt.product_id);
      fnd_dsql.add_text(' ) OR (fu.product_level_type = ''PRODUCT'' ');
      fnd_dsql.add_text(' AND fu.product_id IN (select b.inventory_item_id from eni_denorm_hierarchies a, mtl_item_categories b  ');
      fnd_dsql.add_text(' where a.parent_id = ');
      fnd_dsql.add_text(l_funds_util_flt.product_id);
      fnd_dsql.add_text(' and b.organization_id = ');
      fnd_dsql.add_bind(l_org_id);
      fnd_dsql.add_text(' and a.object_type = ''CATEGORY_SET'' and b.category_id = a.child_id ');
      fnd_dsql.add_text(' ))) ');
    ELSIF l_funds_util_flt.product_level_type = 'MEDIA'
    THEN
      fnd_dsql.add_text(' AND fu.product_level_type = ''MEDIA'' ');
      IF l_funds_util_flt.product_id IS NOT NULL
      THEN
        fnd_dsql.add_text(' AND fu.product_id = ');
        fnd_dsql.add_bind(l_funds_util_flt.product_id);
      END IF;
    END IF;

    IF l_funds_util_flt.end_date IS NOT NULL
    THEN
      fnd_dsql.add_text(' AND trunc(fu.creation_date) <= ');
      fnd_dsql.add_bind(l_funds_util_flt.end_date);
    END IF;

    -- Fix for Bug 8402328
    IF (l_funds_util_flt.utilization_id IS NOT NULL)
    THEN
      fnd_dsql.add_text(' AND fu.utilization_id = ');
      fnd_dsql.add_bind(l_funds_util_flt.utilization_id);
    END IF;

    fnd_dsql.add_text(' AND fu.gl_posted_flag = ''Y'' ');

  END get_utiz_sql_stmt_where_clause;

  ---------------------------------------------------------------------
  -- PROCEDURE
  --   Get_Utiz_Sql_Stmt_From_Clause
  --
  -- PARAMETERS
  --
  -- NOTE
  --
  -- HISTORY
  --   17-FEB-2004  yizhang  Create.
  ---------------------------------------------------------------------
  PROCEDURE get_utiz_sql_stmt_from_clause(p_summary_view   IN VARCHAR2
                                         ,p_funds_util_flt IN funds_util_flt_type
                                         ,p_currency_rec   IN currency_rec_type) IS
    l_api_name  CONSTANT VARCHAR2(30) := 'Get_Utiz_Sql_Stmt_From_Clause';
    l_full_name CONSTANT VARCHAR2(60) := g_pkg_name || '.' || l_api_name;

    l_funds_util_flt  funds_util_flt_type := NULL;
    l_cust_account_id NUMBER := NULL;
    l_scan_data_flag  VARCHAR2(1) := 'N';
    l_offer_flag      VARCHAR2(1) := 'Y';
    l_price_list_flag VARCHAR2(1) := 'Y';
    l_resource_id     NUMBER;
    l_sales_rep       VARCHAR2(1) := fnd_api.g_false;
    l_is_admin        BOOLEAN := FALSE;
    l_orgid           NUMBER;
    l_claim_line_id   NUMBER;
    l_currency_rec    currency_rec_type := p_currency_rec;
  BEGIN
    --------------------- initialize -----------------------
    IF ozf_debug_high_on
    THEN
      ozf_utility_pvt.debug_message(l_full_name || ': start');
    END IF;

    --------------------- start -----------------------
    l_funds_util_flt := p_funds_util_flt;

    IF l_funds_util_flt.offer_type = 'SCAN_DATA'
    THEN
      l_scan_data_flag := 'Y';
    END IF;

    IF l_funds_util_flt.activity_type IS NOT NULL
    THEN
      IF l_funds_util_flt.activity_type = 'OFFR'
      THEN
        l_price_list_flag := 'N';
      ELSE
        l_offer_flag := 'N';
      END IF;
    END IF;

    IF l_funds_util_flt.run_mode IN ('OFFER_AUTOPAY', 'OFFER_NO_AUTOPAY')
    THEN
      l_price_list_flag := 'N';
    END IF;

    IF l_funds_util_flt.offer_type IS NOT NULL
    THEN
      l_price_list_flag := 'N';
    END IF;

    l_resource_id := ozf_utility_pvt.get_resource_id(fnd_global.user_id);
    l_is_admin    := ams_access_pvt.check_admin_access(l_resource_id);
    l_orgid       := mo_global.get_current_org_id();

    -- Added Debug For Multi Currency - kpatro
    IF ozf_debug_high_on
    THEN
      ozf_utility_pvt.debug_message('----- Get_Utiz_Sql_Stmt_From_Clause:Start -----');
      ozf_utility_pvt.debug_message('l_currency_rec.claim_currency_code        : ' ||
                                    l_currency_rec.claim_currency_code);
      ozf_utility_pvt.debug_message('l_currency_rec.functional_currency_code   : ' ||
                                    l_currency_rec.functional_currency_code);
      ozf_utility_pvt.debug_message('l_currency_rec.transaction_currency_code  : ' ||
                                    l_currency_rec.transaction_currency_code);
      ozf_utility_pvt.debug_message('p_summary_view         : ' ||
                                    p_summary_view);
      ozf_utility_pvt.debug_message('Process_By         : ' ||
                                    l_funds_util_flt.autopay_check);
      ozf_utility_pvt.debug_message('----------------------------Get_Utiz_Sql_Stmt_From_Clause:End ------------');
    END IF;

    IF l_offer_flag = 'Y'
    THEN
      -- restrict offer access if user is sales rep
      IF l_funds_util_flt.reference_type = 'LEAD_REFERRAL'
      THEN
        l_sales_rep := fnd_api.g_false;
      ELSE
        l_resource_id := ozf_utility_pvt.get_resource_id(fnd_global.user_id);
        l_sales_rep   := ozf_utility_pvt.has_sales_rep_role(l_resource_id);
      END IF;

      fnd_dsql.add_text('SELECT fu.utilization_id, fu.cust_account_id ' ||
                        ', fu.plan_type, fu.plan_id, o.offer_type, o.autopay_method ' ||
                        ', fu.product_level_type, fu.product_id, fu.acctd_amount_remaining, ');

      -- HUMOZF Add PlanID
      fnd_dsql.add_text('orl.attribute1 rx_plan_id,  fu.fund_id ');

      -- Added For Multi Currency - kpatro
      IF (p_summary_view IN ('AUTOPAY', 'AUTOPAY_LINE') AND
         l_funds_util_flt.autopay_check = 'AUTOPAY')
      THEN
        fnd_dsql.add_text('fu.plan_curr_amount_remaining amount_remaining, ');
      ELSE
        fnd_dsql.add_text('DECODE(NVL(''' ||
                          l_currency_rec.claim_currency_code ||
                          ''',fu.plan_currency_code), fu.plan_currency_code, fu.plan_curr_amount_remaining, fu.acctd_amount_remaining) amount_remaining, ');
      END IF;

      fnd_dsql.add_text('fu.scan_unit_remaining , fu.creation_date, ');

      -- Added For Multi Currency - kpatro
      IF (p_summary_view IN ('AUTOPAY', 'AUTOPAY_LINE') AND
         l_funds_util_flt.autopay_check = 'AUTOPAY')
      THEN
        fnd_dsql.add_text('fu.PLAN_CURRENCY_CODE currency_code, ');
      ELSE
        fnd_dsql.add_text('DECODE(NVL(''' ||
                          l_currency_rec.claim_currency_code ||
                          ''',fu.PLAN_CURRENCY_CODE), fu.PLAN_CURRENCY_CODE, fu.PLAN_CURRENCY_CODE, ''' ||
                          l_currency_rec.functional_currency_code ||
                          ''') currency_code, ');
      END IF;

      fnd_dsql.add_text('fu.bill_to_site_use_id ' ||
                        'FROM ozf_funds_utilized_all_b fu, ozf_offers o, ozf_resale_lines_all orl ');

      --Modified for Bugfix 5346249
      fnd_dsql.add_text('WHERE fu.plan_type = ''OFFR'' ' ||
                        'AND fu.plan_id = o.qp_list_header_id ' ||
                        -- HUMOZF Join to resale line
                        'AND fu.object_type = ''TP_ORDER'' ' ||
                        'AND fu.object_id = orl.resale_line_id ' ||
                        'AND fu.cust_account_id = orl.bill_to_cust_account_id ' ||
                        --
                        'AND fu.org_id = ');
      fnd_dsql.add_bind(l_orgid);

      -- Rx Plan ID Reference Type
      IF l_funds_util_flt.reference_type IS NOT NULL
      THEN
        fnd_dsql.add_text(' AND orl.attribute1 = ');
        fnd_dsql.add_bind(l_funds_util_flt.reference_type);
      END IF;

      IF l_funds_util_flt.offer_type IS NOT NULL
      THEN
        fnd_dsql.add_text(' AND o.offer_type = ');
        fnd_dsql.add_bind(l_funds_util_flt.offer_type);
      ELSE
        fnd_dsql.add_text(' AND o.offer_type <> ''SCAN_DATA'' ');
      END IF;

      IF l_funds_util_flt.run_mode = 'OFFER_AUTOPAY'
      THEN
        fnd_dsql.add_text(' AND o.autopay_flag = ''Y'' ');
      ELSIF l_funds_util_flt.run_mode = 'OFFER_NO_AUTOPAY'
      THEN
        fnd_dsql.add_text(' AND (o.autopay_flag IS NULL OR o.autopay_flag = ''N'') ');
      END IF;

      IF l_funds_util_flt.offer_payment_method IS NOT NULL
      THEN
        IF l_funds_util_flt.offer_payment_method = 'NULL'
        THEN
          fnd_dsql.add_text(' AND o.autopay_method IS NULL ');
        ELSE
          fnd_dsql.add_text(' AND o.autopay_method = ');
          fnd_dsql.add_bind(l_funds_util_flt.offer_payment_method);
        END IF;
      END IF;

      IF (l_sales_rep = fnd_api.g_true AND NOT l_is_admin)
      THEN
        fnd_dsql.add_text(' AND (o.confidential_flag =''N'' OR ');
        fnd_dsql.add_text(' o.confidential_flag IS NULL OR ');
        fnd_dsql.add_text(' ( NVL(o.budget_offer_yn, ''N'') = ''N'' AND ');
        fnd_dsql.add_text(' EXISTS ( SELECT 1 FROM    ams_act_access_denorm act ');
        fnd_dsql.add_text(' WHERE act.object_id = o.qp_list_header_id ');
        fnd_dsql.add_text(' AND  act.object_type = ''OFFR'' ');
        fnd_dsql.add_text(' AND   act.resource_id= ');
        fnd_dsql.add_bind(l_resource_id);
        fnd_dsql.add_text('))');
        fnd_dsql.add_text(' OR ( NVL(o.budget_offer_yn, ''N'') = ''Y'' ');
        fnd_dsql.add_text(' AND EXISTS ( SELECT 1 FROM ams_act_access_denorm act ');
        fnd_dsql.add_text(' WHERE act.object_id = fu.fund_id ');
        fnd_dsql.add_text(' AND   act.object_type = ''FUND'' ');
        fnd_dsql.add_text(' AND   act.resource_id= ');
        fnd_dsql.add_bind(l_resource_id);
        fnd_dsql.add_text(')))');
      END IF;

      get_utiz_sql_stmt_where_clause(p_summary_view   => p_summary_view
                                    ,p_funds_util_flt => l_funds_util_flt
                                    ,px_currency_rec  => l_currency_rec);

    END IF;

    IF l_offer_flag = 'Y' AND l_price_list_flag = 'Y'
    THEN
      fnd_dsql.add_text('UNION ALL ');
    END IF;

    IF l_price_list_flag = 'Y'
    THEN
      fnd_dsql.add_text('SELECT fu.utilization_id, fu.cust_account_id ' ||
                        ', fu.plan_type, fu.plan_id, null, null ' ||
                        ', fu.product_level_type, fu.product_id ' ||
                        ', fu.acctd_amount_remaining,');

      -- Added For Multii Currency - kpatro
      IF (p_summary_view IN ('AUTOPAY', 'AUTOPAY_LINE') AND
         l_funds_util_flt.autopay_check = 'AUTOPAY')
      THEN
        fnd_dsql.add_text('fu.plan_curr_amount_remaining amount_remaining, ');
      ELSE
        fnd_dsql.add_text('DECODE(NVL(''' ||
                          l_currency_rec.claim_currency_code ||
                          ''',fu.plan_currency_code), fu.plan_currency_code, fu.plan_curr_amount_remaining, fu.acctd_amount_remaining) amount_remaining, ');
      END IF;

      fnd_dsql.add_text('fu.scan_unit_remaining , fu.creation_date, ');

      -- Added For Multii Currency - kpatro
      IF (p_summary_view IN ('AUTOPAY', 'AUTOPAY_LINE') AND
         l_funds_util_flt.autopay_check = 'AUTOPAY')
      THEN
        fnd_dsql.add_text('fu.PLAN_CURRENCY_CODE currency_code, ');
      ELSE
        fnd_dsql.add_text('DECODE(NVL(''' ||
                          l_currency_rec.claim_currency_code ||
                          ''',fu.PLAN_CURRENCY_CODE), fu.PLAN_CURRENCY_CODE, fu.PLAN_CURRENCY_CODE, ''' ||
                          l_currency_rec.functional_currency_code ||
                          ''') currency_code, ');
      END IF;

      fnd_dsql.add_text('fu.bill_to_site_use_id ' ||
                        'FROM ozf_funds_utilized_all_b fu ' ||
                        'WHERE fu.plan_type = ''PRIC'' ' ||
                        'AND fu.org_id =');
      fnd_dsql.add_bind(l_orgid);
      fnd_dsql.add_text(' AND fu.cust_account_id = ');
      fnd_dsql.add_bind(l_funds_util_flt.cust_account_id);

      get_utiz_sql_stmt_where_clause(p_summary_view   => p_summary_view
                                    ,p_funds_util_flt => l_funds_util_flt
                                    ,px_currency_rec  => l_currency_rec);
    END IF;

  END get_utiz_sql_stmt_from_clause;

  ---------------------------------------------------------------------
  -- PROCEDURE
  --   Get_Utiz_Sql_Stmt
  --
  -- PARAMETERS
  --    p_summary_view     : Available values
  --                          1. OZF_AUTOPAY_PVT -- 'AUTOPAY'
  --                          2. OZF_CLAIM_LINE_PVT --'ACTIVITY', 'PRODUCT', 'SCHEDULE'
  --    p_funds_util_flt   :
  --    p_cust_account_id  : Only be used for OZF_AUTOPAY_PVT
  --    x_utiz_sql_stmt    : Return datatype is VARCHAR2(500)
  --
  -- NOTE
  --   1. This statement will be used for both OZF_AUTOPAY_PVT and OZF_CLAIM_LINE_PVT
  --      to get funds_utilized SQL statement by giving in search criteria.
  --
  -- HISTORY
  --   25-JUN-2002  mchang  Create.
  ---------------------------------------------------------------------
  PROCEDURE get_utiz_sql_stmt(p_api_version      IN NUMBER
                             ,p_init_msg_list    IN VARCHAR2 := fnd_api.g_false
                             ,p_commit           IN VARCHAR2 := fnd_api.g_false
                             ,p_validation_level IN NUMBER := fnd_api.g_valid_level_full
                             ,

                              x_return_status OUT NOCOPY VARCHAR2
                             ,x_msg_count     OUT NOCOPY NUMBER
                             ,x_msg_data      OUT NOCOPY VARCHAR2
                             ,

                              p_summary_view    IN VARCHAR2 := NULL
                             ,p_funds_util_flt  IN funds_util_flt_type
                             ,px_currency_rec   IN OUT NOCOPY currency_rec_type
                             ,p_cust_account_id IN NUMBER := NULL
                             ,

                              x_utiz_sql_stmt OUT NOCOPY VARCHAR2) IS
    l_api_version CONSTANT NUMBER := 1.0;
    l_api_name    CONSTANT VARCHAR2(30) := 'Get_Utiz_Sql_Stmt';
    l_full_name   CONSTANT VARCHAR2(60) := g_pkg_name || '.' || l_api_name;
    l_return_status VARCHAR2(1);

    l_utiz_sql             VARCHAR2(4000) := NULL;
    l_utiz_sql_from_clause VARCHAR2(2000) := NULL;
    l_funds_util_flt       funds_util_flt_type := NULL;
    l_line_util_flt        funds_util_flt_type := NULL;
    l_cust_account_id      NUMBER := NULL;
    l_scan_data_flag       VARCHAR2(1) := 'N';
    l_org_id               NUMBER;
    l_currency_rec         currency_rec_type := px_currency_rec;

    CURSOR csr_request_offer(cv_request_id IN NUMBER) IS
      SELECT o.qp_list_header_id, o.offer_type
        FROM apps.ozf_offers o, apps.ozf_request_headers_all_b r
       WHERE o.qp_list_header_id = r.offer_id
         AND r.request_header_id = cv_request_id;
    -- Added For Multi Currency - kpatro (As For Public API this is the starting point)
    CURSOR csr_function_currency IS
      SELECT gs.currency_code
        FROM apps.gl_sets_of_books gs, apps.ozf_sys_parameters org
       WHERE org.set_of_books_id = gs.set_of_books_id
         AND org.org_id = mo_global.get_current_org_id();

  BEGIN
    --------------------- initialize -----------------------
    IF ozf_debug_high_on
    THEN
      ozf_utility_pvt.debug_message(l_full_name || ': start');
    END IF;

    IF fnd_api.to_boolean(p_init_msg_list)
    THEN
      fnd_msg_pub.initialize;
    END IF;

    IF NOT fnd_api.compatible_api_call(l_api_version
                                      ,p_api_version
                                      ,l_api_name
                                      ,g_pkg_name)
    THEN
      RAISE fnd_api.g_exc_unexpected_error;
    END IF;
    x_return_status := fnd_api.g_ret_sts_success;
    -- Added For Multi Currency - kpatro
    OPEN csr_function_currency;
    FETCH csr_function_currency
      INTO l_currency_rec.functional_currency_code;
    CLOSE csr_function_currency;

    l_org_id := fnd_profile.value('QP_ORGANIZATION_ID');

    --------------------- start -----------------------
    l_funds_util_flt := p_funds_util_flt;

    -- default filter parameters based on claim line properties
    IF l_funds_util_flt.claim_line_id IS NOT NULL
    THEN
      IF p_summary_view IS NULL OR p_summary_view <> 'DEL_GRP_LINE_UTIL'
      THEN
        copy_util_flt(px_funds_util_flt => l_funds_util_flt);
      END IF;
    END IF;

    -- for special pricing requests, set offer id
    IF l_funds_util_flt.reference_type = 'SPECIAL_PRICE' AND
       l_funds_util_flt.reference_id IS NOT NULL AND
       l_funds_util_flt.activity_id IS NULL
    THEN
      l_funds_util_flt.activity_type := 'OFFR';
      OPEN csr_request_offer(l_funds_util_flt.reference_id);
      FETCH csr_request_offer
        INTO l_funds_util_flt.activity_id, l_funds_util_flt.offer_type;
      CLOSE csr_request_offer;
    END IF;

    IF ozf_debug_high_on
    THEN
      ozf_utility_pvt.debug_message('----- p_funds_util_flt -----');
      ozf_utility_pvt.debug_message('cust_account_id    : ' ||
                                    l_funds_util_flt.cust_account_id);
      ozf_utility_pvt.debug_message('activity_type      : ' ||
                                    l_funds_util_flt.activity_type);
      ozf_utility_pvt.debug_message('activity_id        : ' ||
                                    l_funds_util_flt.activity_id);
      ozf_utility_pvt.debug_message('offer_type         : ' ||
                                    l_funds_util_flt.offer_type);
      ozf_utility_pvt.debug_message('product_level_type : ' ||
                                    l_funds_util_flt.product_level_type);
      ozf_utility_pvt.debug_message('product_id         : ' ||
                                    l_funds_util_flt.product_id);
      ozf_utility_pvt.debug_message('Process_By         : ' ||
                                    l_funds_util_flt.autopay_check); -- Added For Multi Currency - kpatro
      ozf_utility_pvt.debug_message('l_currency_rec.universal_currency_code :' ||
                                    l_currency_rec.universal_currency_code);
      ozf_utility_pvt.debug_message('l_currency_rec.claim_currency_code :' ||
                                    l_currency_rec.claim_currency_code);
      ozf_utility_pvt.debug_message('l_currency_rec.functional_currency_code :' ||
                                    l_currency_rec.functional_currency_code);
      ozf_utility_pvt.debug_message('l_currency_rec.transaction_currency_code :' ||
                                    l_currency_rec.transaction_currency_code);
      ozf_utility_pvt.debug_message('l_currency_rec.association_currency_code :' ||
                                    l_currency_rec.association_currency_code);
      ozf_utility_pvt.debug_message('----------------------------');
    END IF;

    -- use FND_DSQL package to handle dynamic sql and bind variables
    fnd_dsql.init;

    IF p_funds_util_flt.offer_type = 'SCAN_DATA'
    THEN
      l_scan_data_flag := 'Y';
    END IF;

    --
    IF p_summary_view = 'AUTOPAY'
    THEN
      -- if cust account is specified in autopay parameter, do
      -- not group by cust_account_id in query
      -- R12 Group By Offer Enhancement
      -- Modified for FXGL Enhancement, Need to derive amount_remaining instead of acctd_amount_remaining
      -- Also need to group by currency as amount of different currencies cannot be added
      -- Modified for R12.1 enhancements, Need to select the value of bill_to_site_use_id.

      IF p_cust_account_id IS NOT NULL AND
         l_funds_util_flt.group_by_offer = 'N'
      THEN
        fnd_dsql.add_text('SELECT autopay_method, sum(amount_remaining), currency_code, bill_to_site_use_id, fund_id  ');
      ELSIF p_cust_account_id IS NULL AND
            l_funds_util_flt.group_by_offer = 'N'
      THEN
        fnd_dsql.add_text('SELECT autopay_method, sum(amount_remaining), currency_code , bill_to_site_use_id , cust_account_id, fund_id ');
      ELSIF p_cust_account_id IS NOT NULL AND
            l_funds_util_flt.group_by_offer = 'Y'
      THEN
        fnd_dsql.add_text('SELECT autopay_method, sum(amount_remaining), currency_code , bill_to_site_use_id , plan_id ');
      ELSIF p_cust_account_id IS NULL AND
            l_funds_util_flt.group_by_offer = 'Y'
      THEN
        fnd_dsql.add_text('SELECT autopay_method, sum(amount_remaining), currency_code , bill_to_site_use_id , cust_account_id, plan_id ');
      END IF;
      fnd_dsql.add_text('FROM (');
      --
    ELSIF p_summary_view = 'AUTOPAY_LINE'
    THEN
      --         FND_DSQL.add_text('SELECT cust_account_id, plan_type, plan_id, bill_to_site_use_id ' ||
      fnd_dsql.add_text('SELECT cust_account_id, bill_to_site_use_id ' ||
                        ', product_level_type, product_id ' ||
                        ', sum(amount_remaining), currency_code ' ||
                        ', rx_plan_id ' || 'FROM (' ||
                        'SELECT cust_account_id ' || --, plan_type, plan_id ' ||
                        ', decode(product_id, null, null, product_level_type) product_level_type ' ||
                        ', product_id product_id ' ||
                        ', acctd_amount_remaining , amount_remaining, currency_code, bill_to_site_use_id  ' ||
                        ', orl.attribute1 ' || 'FROM (');
      --
    ELSIF p_summary_view = 'DEL_GRP_LINE_UTIL'
    THEN
      -- Modified for FXGL ER(amount selected)
      fnd_dsql.add_text('SELECT lu.claim_line_util_id, lu.utilization_id, lu.amount, lu.scan_unit, lu.currency_code  ' ||
                        'FROM (');
      --
    ELSE
      -- Modified for FXGL ER
      -- R12 Multicurrency Enhancements: Amount Remaining changed from BUDGET to TRANSACTIONAL currency
      fnd_dsql.add_text('SELECT utilization_id, amount_remaining, scan_unit_remaining, currency_code ' ||
                        'FROM (');
    END IF;

    get_utiz_sql_stmt_from_clause(p_summary_view   => p_summary_view
                                 ,p_funds_util_flt => l_funds_util_flt
                                 ,p_currency_rec   => l_currency_rec);

    -- R12.1 autopay enhancement,  Need to select and group by bill_to_site_use_id.
    -- for p_summary_view = AUTOPAY and AUTOPAY_LINE.

    IF p_summary_view = 'AUTOPAY'
    THEN
      fnd_dsql.add_text(') utiz ');
      -- R12 Enhancements: Group By Offer for Autopay.
      IF p_cust_account_id IS NOT NULL AND
         l_funds_util_flt.group_by_offer = 'N'
      THEN
        fnd_dsql.add_text('GROUP BY utiz.autopay_method, utiz.currency_code, utiz.bill_to_site_use_id, utiz.fund_id ');
      ELSIF p_cust_account_id IS NULL AND
            l_funds_util_flt.group_by_offer = 'N'
      THEN
        fnd_dsql.add_text('GROUP BY utiz.cust_account_id, utiz.autopay_method, utiz.currency_code, utiz.bill_to_site_use_id, utiz.fund_id ');
      ELSIF p_cust_account_id IS NOT NULL AND
            l_funds_util_flt.group_by_offer = 'Y'
      THEN
        fnd_dsql.add_text('GROUP BY utiz.plan_id, utiz.autopay_method , utiz.currency_code, utiz.bill_to_site_use_id ');
      ELSIF p_cust_account_id IS NULL AND
            l_funds_util_flt.group_by_offer = 'Y'
      THEN
        fnd_dsql.add_text('GROUP BY utiz.cust_account_id,utiz.plan_id, utiz.autopay_method , utiz.currency_code, utiz.bill_to_site_use_id ');
      END IF;
    ELSIF p_summary_view = 'AUTOPAY_LINE'
    THEN
      fnd_dsql.add_text(') utiz ) ' ||
                        --'GROUP BY cust_account_id, plan_type, plan_id, bill_to_site_use_id, product_level_type, product_id, currency_code ' ||
                        'GROUP BY cust_account_id, bill_to_site_use_id, product_level_type, product_id, currency_code, rx_plan_id ' ||
                        --'ORDER BY cust_account_id, plan_type, plan_id, bill_to_site_use_id, product_level_type, product_id ');
                        'ORDER BY cust_account_id, bill_to_site_use_id, product_level_type, product_id, rx_plan_id ');
    ELSIF p_summary_view = 'DEL_GRP_LINE_UTIL'
    THEN
      fnd_dsql.add_text(') utiz, ozf_claim_lines_util lu ' ||
                        'WHERE lu.utilization_id = utiz.utilization_id ' ||
                        'AND lu.claim_line_id = ');
      fnd_dsql.add_bind(l_funds_util_flt.claim_line_id);
      fnd_dsql.add_text(' ORDER BY utiz.creation_date desc ');
    ELSE
      fnd_dsql.add_text(') utiz ');
      IF l_funds_util_flt.total_amount IS NOT NULL
      THEN
        IF l_funds_util_flt.total_amount >= 0
        THEN
          fnd_dsql.add_text(' ORDER BY sign(utiz.amount_remaining) asc, utiz.creation_date asc');
        ELSE
          fnd_dsql.add_text(' ORDER BY sign(utiz.amount_remaining) desc, utiz.creation_date asc');
        END IF;
      ELSE
        fnd_dsql.add_text(' ORDER BY utiz.creation_date asc');
      END IF;
    END IF;

    x_utiz_sql_stmt := fnd_dsql.get_text(FALSE);

    IF ozf_debug_high_on
    THEN
      --l_utiz_sql := FND_DSQL.get_text(TRUE);
      l_utiz_sql := substr(fnd_dsql.get_text(TRUE), 1, 4000);
      ozf_utility_pvt.debug_message('----- UTIZ SQL -----');
      ozf_utility_pvt.debug_message(substr(l_utiz_sql, 1, 250));
      ozf_utility_pvt.debug_message(substr(l_utiz_sql, 251, 250));
      ozf_utility_pvt.debug_message(substr(l_utiz_sql, 501, 250));
      ozf_utility_pvt.debug_message(substr(l_utiz_sql, 751, 250));
      ozf_utility_pvt.debug_message(substr(l_utiz_sql, 1001, 250));
      ozf_utility_pvt.debug_message(substr(l_utiz_sql, 1251, 250));
      ozf_utility_pvt.debug_message(substr(l_utiz_sql, 1501, 250));
      ozf_utility_pvt.debug_message(substr(l_utiz_sql, 1751, 250));
      ozf_utility_pvt.debug_message(substr(l_utiz_sql, 2001, 250));
      ozf_utility_pvt.debug_message(substr(l_utiz_sql, 2251, 250));
      ozf_utility_pvt.debug_message(substr(l_utiz_sql, 2751, 250));
      ozf_utility_pvt.debug_message(substr(l_utiz_sql, 3001, 250));
      ozf_utility_pvt.debug_message(substr(l_utiz_sql, 3251, 250));
      ozf_utility_pvt.debug_message(substr(l_utiz_sql, 3501, 250));
      ozf_utility_pvt.debug_message(substr(l_utiz_sql, 3751, 250));
      ozf_utility_pvt.debug_message('--------------------');
      ozf_utility_pvt.debug_message(l_full_name || ': end');
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      x_return_status := fnd_api.g_ret_sts_unexp_error;
      IF fnd_msg_pub.check_msg_level(fnd_msg_pub.g_msg_lvl_unexp_error)
      THEN
        fnd_message.set_name('OZF', 'OZF_CLAIM_ATPY_UTIZ_STMT_ERR');
        fnd_msg_pub.add;
      END IF;
  END get_utiz_sql_stmt;

  ---------------------------------------------------------------------
  -- PROCEDURE
  --    Settle_Claim
  --
  -- PURPOSE
  --    Close a claim
  --
  -- PARAMETERS
  --    p_claim_id: claim id
  --
  -- NOTES
  ---------------------------------------------------------------------
  PROCEDURE settle_claim(p_claim_id      IN NUMBER
                        ,x_return_status OUT NOCOPY VARCHAR2
                        ,x_msg_count     OUT NOCOPY NUMBER
                        ,x_msg_data      OUT NOCOPY VARCHAR2) IS
    l_api_version CONSTANT NUMBER := 1.0;
    l_api_name    CONSTANT VARCHAR2(30) := 'Settle_Claim';
    l_return_status VARCHAR2(1);

    l_claim_id              NUMBER := p_claim_id;
    l_claim_rec             ozf_claim_pvt.claim_rec_type;
    l_object_version_number NUMBER;
    l_sales_rep_id          NUMBER;
    l_salesrep_req_flag     VARCHAR2(1);

    CURSOR csr_claim_info(cv_claim_id IN NUMBER) IS
      SELECT object_version_number, sales_rep_id
        FROM apps.ozf_claims_all
       WHERE claim_id = cv_claim_id;

    CURSOR csr_ar_system_options IS
      SELECT salesrep_required_flag FROM apps.ar_system_parameters;

  BEGIN
    --------------------- initialize -----------------------
    SAVEPOINT settle_claim;

    x_return_status := fnd_api.g_ret_sts_success;

    ----------------- start ----------------
    OPEN csr_claim_info(l_claim_id);
    FETCH csr_claim_info
      INTO l_object_version_number, l_sales_rep_id;
    CLOSE csr_claim_info;

    l_claim_rec.claim_id              := l_claim_id;
    l_claim_rec.object_version_number := l_object_version_number;
    l_claim_rec.user_status_id        := to_number(ozf_utility_pvt.get_default_user_status(p_status_type => 'OZF_CLAIM_STATUS'
                                                                                          ,p_status_code => 'CLOSED'));

    ------------------------------------------------------
    -- Sales Credit
    --   Bug 2950241 fixing: default Sales Rep in Claims
    --   if "Requires Salesperson" in AR system options.
    ------------------------------------------------------
    IF l_sales_rep_id IS NULL
    THEN
      OPEN csr_ar_system_options;
      FETCH csr_ar_system_options
        INTO l_salesrep_req_flag;
      CLOSE csr_ar_system_options;

      IF l_salesrep_req_flag = 'Y'
      THEN
        l_claim_rec.sales_rep_id := -3; -- No Sales Credit
      END IF;
    END IF;

    ozf_claim_pvt.update_claim(p_api_version           => l_api_version
                              ,p_init_msg_list         => fnd_api.g_false
                              ,p_commit                => fnd_api.g_false
                              ,p_validation_level      => fnd_api.g_valid_level_full
                              ,x_return_status         => l_return_status
                              ,x_msg_count             => x_msg_count
                              ,x_msg_data              => x_msg_data
                              ,p_claim                 => l_claim_rec
                              ,p_event                 => 'UPDATE'
                              ,p_mode                  => ozf_claim_utility_pvt.g_auto_mode
                              ,x_object_version_number => l_object_version_number);
    IF l_return_status = fnd_api.g_ret_sts_error
    THEN
      RAISE fnd_api.g_exc_error;
    ELSIF l_return_status = fnd_api.g_ret_sts_unexp_error
    THEN
      RAISE fnd_api.g_exc_unexpected_error;
    END IF;

  EXCEPTION
    WHEN fnd_api.g_exc_error THEN
      ROLLBACK TO settle_claim;
      x_return_status := fnd_api.g_ret_sts_error;
    WHEN fnd_api.g_exc_unexpected_error THEN
      ROLLBACK TO settle_claim;
      x_return_status := fnd_api.g_ret_sts_unexp_error;
    WHEN OTHERS THEN
      ROLLBACK TO settle_claim;
      x_return_status := fnd_api.g_ret_sts_unexp_error;
      IF fnd_msg_pub.check_msg_level(fnd_msg_pub.g_msg_lvl_unexp_error)
      THEN
        fnd_msg_pub.add_exc_msg(g_pkg_name, l_api_name);
      END IF;
  END settle_claim;

  ---------------------------------------------------------------------
  -- PROCEDURE
  --    Delete_Group_Line_Util
  --
  -- HISTORY
  ---------------------------------------------------------------------
  PROCEDURE delete_group_line_util(p_api_version      IN NUMBER
                                  ,p_init_msg_list    IN VARCHAR2 := fnd_api.g_false
                                  ,p_commit           IN VARCHAR2 := fnd_api.g_false
                                  ,p_validation_level IN NUMBER := fnd_api.g_valid_level_full

                                  ,x_return_status OUT NOCOPY VARCHAR2
                                  ,x_msg_count     OUT NOCOPY NUMBER
                                  ,x_msg_data      OUT NOCOPY VARCHAR2

                                  ,p_funds_util_flt IN funds_util_flt_type
                                  ,p_mode           IN VARCHAR2 := ozf_claim_utility_pvt.g_auto_mode) IS
    l_api_version CONSTANT NUMBER := 1.0;
    l_api_name    CONSTANT VARCHAR2(30) := 'Delete_Group_Line_Util';
    l_full_name   CONSTANT VARCHAR2(60) := g_pkg_name || '.' || l_api_name;
    l_return_status VARCHAR2(1);

    l_funds_util_flt funds_util_flt_type := p_funds_util_flt;

    TYPE fundsutilcsrtyp IS REF CURSOR;
    l_funds_util_csr        NUMBER; --FundsUtilCsrTyp;
    l_funds_util_sql        VARCHAR2(3000);
    l_line_util_tbl         line_util_tbl_type;
    l_upd_line_util_rec     line_util_rec_type;
    l_lu_line_util_id       NUMBER;
    l_lu_utilization_id     NUMBER;
    l_lu_amt                NUMBER;
    l_lu_scan_unit          NUMBER;
    l_del_total_amount      NUMBER;
    l_del_total_units       NUMBER;
    l_counter               PLS_INTEGER := 1;
    i                       PLS_INTEGER;
    l_funds_util_end        VARCHAR2(1) := 'N';
    l_final_lu_amt          NUMBER;
    l_object_version_number NUMBER;
    l_ignore                NUMBER;
    l_offer_uom_code        VARCHAR2(3);
    l_offer_quantity        NUMBER;
    l_scan_value            NUMBER;
    l_product_id            NUMBER;
    l_lu_acctd_amt          NUMBER;
    l_utiz_currency         VARCHAR2(15);
    l_utiz_amount           NUMBER;
    l_lu_currency_code      VARCHAR2(15);

    l_currency_rec currency_rec_type;

    CURSOR csr_final_lu_amt(cv_claim_line_id IN NUMBER) IS
      SELECT SUM(amount)
        FROM apps.ozf_claim_lines_util
       WHERE claim_line_id = cv_claim_line_id;

    -- fix for bug 5042046
    CURSOR csr_function_currency IS
      SELECT gs.currency_code
        FROM apps.gl_sets_of_books gs, apps.ozf_sys_parameters org
       WHERE org.set_of_books_id = gs.set_of_books_id
         AND org.org_id = mo_global.get_current_org_id();

    CURSOR csr_claim_currency(cv_claim_line_id IN NUMBER) IS
      SELECT currency_code
        FROM apps.ozf_claim_lines
       WHERE claim_line_id = cv_claim_line_id;

    CURSOR csr_offer_currency(cv_plan_id IN NUMBER) IS
      SELECT transaction_currency_code
        FROM apps.ozf_offers
       WHERE qp_list_header_id = cv_plan_id;

    CURSOR csr_over_util(cv_claim_line_id  IN NUMBER
                        ,cv_act_product_id IN NUMBER) IS
      SELECT claim_line_util_id, acctd_amount, scan_unit
        FROM apps.ozf_claim_lines_util
       WHERE claim_line_id = cv_claim_line_id
         AND activity_product_id = cv_act_product_id
         AND utilization_id = -1;

    CURSOR csr_acc_over_util(cv_claim_line_id IN NUMBER
                            ,cv_offer_id      IN NUMBER) IS
      SELECT claim_line_util_id, acctd_amount
        FROM apps.ozf_claim_lines_util util
       WHERE claim_line_id = cv_claim_line_id
         AND activity_product_id = cv_offer_id
         AND utilization_id = -2;

    -- Bugfix 5101106: Recalculate qty
    CURSOR csr_offer_profile(cv_activity_product_id IN NUMBER) IS
      SELECT uom_code, quantity, scan_value, inventory_item_id
        FROM apps.ams_act_products
       WHERE activity_product_id = cv_activity_product_id;

    --Changed cursor csr_utiz_amount currency for Multicurrency ER.
    /*
    CURSOR csr_utiz_amount(cv_line_util_id IN NUMBER) IS
    SELECT util_curr_amount
    FROM ozf_claim_lines_util
    WHERE claim_line_util_id = cv_line_util_id;
    */

    CURSOR csr_utiz_amount(cv_line_util_id IN NUMBER) IS
      SELECT decode(ln.currency_code
                   ,fu.plan_currency_code
                   ,ln.plan_curr_amount
                   ,ln.acctd_amount)
        FROM apps.ozf_claim_lines_util ln, apps.ozf_funds_utilized_all_b fu
       WHERE claim_line_util_id = cv_line_util_id
         AND fu.utilization_id = ln.utilization_id;

  BEGIN
    --------------------- initialize -----------------------
    SAVEPOINT delete_group_line_util;

    IF ozf_debug_high_on
    THEN
      ozf_utility_pvt.debug_message(l_full_name || ': start');
    END IF;

    IF fnd_api.to_boolean(p_init_msg_list)
    THEN
      fnd_msg_pub.initialize;
    END IF;

    IF NOT fnd_api.compatible_api_call(l_api_version
                                      ,p_api_version
                                      ,l_api_name
                                      ,g_pkg_name)
    THEN
      RAISE fnd_api.g_exc_unexpected_error;
    END IF;

    x_return_status := fnd_api.g_ret_sts_success;

    OPEN csr_function_currency;
    FETCH csr_function_currency
      INTO l_currency_rec.functional_currency_code;
    CLOSE csr_function_currency;

    init_line_util_rec(x_line_util_rec => l_upd_line_util_rec);

    ----------------- copy line info to filter ---------------

    IF p_funds_util_flt.claim_line_id IS NOT NULL
    THEN
      copy_util_flt(px_funds_util_flt => l_funds_util_flt);
    END IF;

    -- derive claim currency
    OPEN csr_claim_currency(l_funds_util_flt.claim_line_id);
    FETCH csr_claim_currency
      INTO l_currency_rec.claim_currency_code;
    CLOSE csr_claim_currency;

    --kdass
    l_currency_rec.transaction_currency_code := l_funds_util_flt.utiz_currency_code;

    IF l_currency_rec.claim_currency_code =
       l_currency_rec.transaction_currency_code
    THEN
      l_currency_rec.association_currency_code := l_currency_rec.transaction_currency_code;
    ELSE
      l_currency_rec.association_currency_code := l_currency_rec.functional_currency_code;
    END IF;

    IF ozf_debug_high_on
    THEN
      ozf_utility_pvt.debug_message('l_currency_rec.claim_currency_code       :' ||
                                    l_currency_rec.claim_currency_code);
      ozf_utility_pvt.debug_message('l_currency_rec.transaction_currency_code :' ||
                                    l_currency_rec.transaction_currency_code);
      ozf_utility_pvt.debug_message('l_currency_rec.association_currency_code :' ||
                                    l_currency_rec.association_currency_code);
      ozf_utility_pvt.debug_message('l_currency_rec.functional_currency_code  :' ||
                                    l_currency_rec.functional_currency_code);
    END IF;
    -- Modified for FXGL ER
    -- deletion also to take place in amount and not acctd_amount

    --------------------- start -----------------------
    l_del_total_amount := nvl(l_funds_util_flt.old_total_amount, 0) -
                          nvl(l_funds_util_flt.total_amount, 0);
    l_del_total_amount := ozf_utility_pvt.currround(l_del_total_amount
                                                   ,l_currency_rec.claim_currency_code);
    l_del_total_units  := nvl(l_funds_util_flt.old_total_units, 0) -
                          nvl(l_funds_util_flt.total_units, 0);

    ------------ reduce the over-utilization first ---------
    IF p_funds_util_flt.offer_type = 'SCAN_DATA'
    THEN
      OPEN csr_over_util(l_funds_util_flt.claim_line_id
                        ,l_funds_util_flt.activity_product_id);
      LOOP
        FETCH csr_over_util
          INTO l_lu_line_util_id, l_lu_amt, l_lu_scan_unit;
        EXIT WHEN csr_over_util%NOTFOUND;

        IF l_funds_util_flt.total_amount IS NULL
        THEN
          l_line_util_tbl(l_counter).claim_line_util_id := l_lu_line_util_id;
          l_line_util_tbl(l_counter).utilization_id := -1;
          l_line_util_tbl(l_counter).amount := l_lu_amt;
          l_line_util_tbl(l_counter).scan_unit := l_lu_scan_unit;
          l_line_util_tbl(l_counter).claim_line_id := l_funds_util_flt.claim_line_id;
        ELSIF l_del_total_amount >= l_lu_acctd_amt
        THEN
          l_line_util_tbl(l_counter).claim_line_util_id := l_lu_line_util_id;
          l_line_util_tbl(l_counter).utilization_id := -1;
          l_line_util_tbl(l_counter).amount := l_lu_amt;
          l_line_util_tbl(l_counter).scan_unit := l_lu_scan_unit;
          l_line_util_tbl(l_counter).claim_line_id := l_funds_util_flt.claim_line_id;

          l_del_total_amount := l_del_total_amount - l_lu_amt;
          l_del_total_units  := l_del_total_units - l_lu_scan_unit;
        ELSE
          l_upd_line_util_rec.claim_line_util_id := l_lu_line_util_id;
          l_upd_line_util_rec.amount             := l_lu_amt -
                                                    l_del_total_amount;
          l_upd_line_util_rec.scan_unit          := l_lu_scan_unit -
                                                    l_del_total_units;

          l_del_total_amount := 0;
          l_del_total_units  := 0;
        END IF;

        l_counter := l_counter + 1;

        EXIT WHEN l_del_total_amount = 0;
      END LOOP;
      CLOSE csr_over_util;
    ELSIF p_funds_util_flt.activity_type = 'OFFR' AND
          p_funds_util_flt.activity_id IS NOT NULL
    THEN
      NULL;
      /* -- We do not create -2 utilizations for non scan offers currently
      OPEN csr_acc_over_util(l_funds_util_flt.claim_line_id, l_funds_util_flt.activity_id);
      LOOP
        FETCH csr_acc_over_util INTO l_lu_line_util_id, l_lu_acctd_amt;
        EXIT WHEN csr_acc_over_util%NOTFOUND;

        IF p_funds_util_flt.total_amount IS NULL THEN
          l_line_util_tbl(l_counter).claim_line_util_id := l_lu_line_util_id;
          l_line_util_tbl(l_counter).utilization_id := l_lu_line_util_id;
          l_line_util_tbl(l_counter).acctd_amount := l_lu_acctd_amt;
          l_line_util_tbl(l_counter).claim_line_id := l_funds_util_flt.claim_line_id;
        ELSIF l_del_total_amount >= l_lu_acctd_amt THEN
          l_line_util_tbl(l_counter).claim_line_util_id := l_lu_line_util_id;
          l_line_util_tbl(l_counter).utilization_id := l_lu_line_util_id;
          l_line_util_tbl(l_counter).acctd_amount := l_lu_acctd_amt;
          l_line_util_tbl(l_counter).claim_line_id := l_funds_util_flt.claim_line_id;

          l_del_total_amount := l_del_total_amount - l_lu_acctd_amt;
        ELSE
          l_upd_line_util_rec.claim_line_util_id := l_lu_line_util_id;
          l_upd_line_util_rec.acctd_amount := l_lu_acctd_amt - l_del_total_amount;

          l_del_total_amount := 0;
        END IF;

        l_counter := l_counter + 1;

        EXIT WHEN l_del_total_amount = 0;
      END LOOP;
      CLOSE csr_acc_over_util;
      */
    END IF;

    IF l_del_total_amount <> 0
    THEN
      -- added for bugfix 4448859
      get_utiz_sql_stmt(p_api_version      => l_api_version
                       ,p_init_msg_list    => fnd_api.g_false
                       ,p_commit           => fnd_api.g_false
                       ,p_validation_level => fnd_api.g_valid_level_full
                       ,x_return_status    => l_return_status
                       ,x_msg_count        => x_msg_count
                       ,x_msg_data         => x_msg_data
                       ,p_summary_view     => 'DEL_GRP_LINE_UTIL'
                       ,p_funds_util_flt   => l_funds_util_flt
                       ,px_currency_rec    => l_currency_rec
                       ,p_cust_account_id  => l_funds_util_flt.cust_account_id
                       ,x_utiz_sql_stmt    => l_funds_util_sql);
      IF l_return_status = fnd_api.g_ret_sts_error
      THEN
        RAISE fnd_api.g_exc_error;
      ELSIF l_return_status = fnd_api.g_ret_sts_unexp_error
      THEN
        RAISE fnd_api.g_exc_unexpected_error;
      END IF;

      -- use FND_DSQL package for dynamic sql and bind variables
      l_funds_util_csr := dbms_sql.open_cursor;
      fnd_dsql.set_cursor(l_funds_util_csr);
      dbms_sql.parse(l_funds_util_csr, l_funds_util_sql, dbms_sql.native);
      dbms_sql.define_column(l_funds_util_csr, 1, l_lu_line_util_id);
      dbms_sql.define_column(l_funds_util_csr, 2, l_lu_utilization_id);
      dbms_sql.define_column(l_funds_util_csr, 3, l_lu_amt);
      dbms_sql.define_column(l_funds_util_csr, 4, l_lu_scan_unit);
      dbms_sql.define_column(l_funds_util_csr, 5, l_lu_currency_code, 15);
      --    DBMS_SQL.define_column(l_funds_util_csr, 5, l_utiz_amount);
      fnd_dsql.do_binds;

      l_ignore := dbms_sql.execute(l_funds_util_csr);
      --OPEN l_funds_util_csr FOR l_funds_util_sql;
      LOOP
        /*
          FETCH l_funds_util_csr INTO l_lu_line_util_id
                                  , l_lu_utilization_id
                                  , l_lu_acctd_amt
                                  , l_lu_scan_unit;
        EXIT WHEN l_funds_util_csr%NOTFOUND;
        */

        IF dbms_sql.fetch_rows(l_funds_util_csr) > 0
        THEN
          dbms_sql.column_value(l_funds_util_csr, 1, l_lu_line_util_id);
          dbms_sql.column_value(l_funds_util_csr, 2, l_lu_utilization_id);
          dbms_sql.column_value(l_funds_util_csr, 3, l_lu_amt);
          dbms_sql.column_value(l_funds_util_csr, 4, l_lu_scan_unit);
          dbms_sql.column_value(l_funds_util_csr, 5, l_lu_currency_code);
          --        DBMS_SQL.define_column(l_funds_util_csr, 5, l_utiz_amount);

          --If CLAIM and TRANSACTIONAL currencies are same, then l_utiz_amount will be
          --in TRANSACTIONAL currency otherwise it will be in FUNCTIONAL currency.
          --Changed for Claims Multicurrency ER.
          OPEN csr_utiz_amount(l_lu_line_util_id);
          FETCH csr_utiz_amount
            INTO l_utiz_amount;
          CLOSE csr_utiz_amount;

          IF ozf_debug_low_on
          THEN
            ozf_utility_pvt.debug_message('l_lu_line_util_id. : ' ||
                                          l_lu_line_util_id);
            ozf_utility_pvt.debug_message('l_lu_utilization_id. : ' ||
                                          l_lu_utilization_id);
            ozf_utility_pvt.debug_message('l_lu_amt. : ' || l_lu_amt);
            ozf_utility_pvt.debug_message('l_utiz_amount. : ' ||
                                          l_utiz_amount);
            ozf_utility_pvt.debug_message('l_del_total_amount. : ' ||
                                          l_del_total_amount);
          END IF;

          IF p_funds_util_flt.total_amount IS NULL
          THEN
            l_line_util_tbl(l_counter).claim_line_util_id := l_lu_line_util_id;
            l_line_util_tbl(l_counter).utilization_id := l_lu_utilization_id;
            l_line_util_tbl(l_counter).amount := l_utiz_amount;
            IF p_funds_util_flt.offer_type = 'SCAN_DATA'
            THEN
              l_line_util_tbl(l_counter).scan_unit := l_lu_scan_unit;
              l_line_util_tbl(l_counter).uom_code := l_funds_util_flt.uom_code;
              l_line_util_tbl(l_counter).quantity := l_funds_util_flt.quantity;
            END IF;
            l_line_util_tbl(l_counter).claim_line_id := l_funds_util_flt.claim_line_id;
            l_counter := l_counter + 1;
          ELSE
            IF abs(l_del_total_amount) >= abs(l_lu_amt)
            THEN
              l_line_util_tbl(l_counter).claim_line_util_id := l_lu_line_util_id;
              l_line_util_tbl(l_counter).utilization_id := l_lu_utilization_id;
              l_line_util_tbl(l_counter).amount := l_utiz_amount;
              IF p_funds_util_flt.offer_type = 'SCAN_DATA'
              THEN
                l_line_util_tbl(l_counter).scan_unit := l_lu_scan_unit;
                l_line_util_tbl(l_counter).uom_code := l_funds_util_flt.uom_code;
                l_line_util_tbl(l_counter).quantity := l_funds_util_flt.quantity;
                l_del_total_units := l_del_total_units - l_lu_scan_unit;
              END IF;
              l_line_util_tbl(l_counter).claim_line_id := l_funds_util_flt.claim_line_id;

              l_del_total_amount := l_del_total_amount - l_lu_amt;

            ELSE
              l_upd_line_util_rec.claim_line_util_id := l_lu_line_util_id;
              l_upd_line_util_rec.amount             := l_lu_amt -
                                                        l_del_total_amount;
              IF p_funds_util_flt.offer_type = 'SCAN_DATA'
              THEN
                l_upd_line_util_rec.scan_unit := l_lu_scan_unit -
                                                 l_del_total_units;
                l_upd_line_util_rec.uom_code  := l_funds_util_flt.uom_code;
                l_upd_line_util_rec.quantity  := l_funds_util_flt.quantity;
              END IF;

              l_funds_util_end := 'Y';
            END IF;

            l_counter := l_counter + 1;

            EXIT WHEN l_del_total_amount = 0;
            EXIT WHEN l_funds_util_end = 'Y';
          END IF;
        ELSE
          EXIT;
        END IF;
      END LOOP;
      --CLOSE l_funds_util_csr;
      dbms_sql.close_cursor(l_funds_util_csr);
    END IF; -- l_del_total_amount <> 0

    --------------------- 1. delete -----------------------
    i := l_line_util_tbl.first;
    IF i IS NOT NULL
    THEN
      IF p_funds_util_flt.activity_type = 'OFFR' AND
         p_funds_util_flt.activity_id IS NOT NULL
      THEN
        OPEN csr_offer_currency(p_funds_util_flt.activity_id);
        FETCH csr_offer_currency
          INTO l_currency_rec.offer_currency_code; -- BKUNJAN Need to check where it is used ?
        CLOSE csr_offer_currency;
      END IF;

      LOOP
        IF l_line_util_tbl(i).claim_line_util_id IS NOT NULL
        THEN
          ozf_utility_pvt.debug_message('kd: calling Del_Line_Util_By_Group');
          -- modified call for HUMOZF
          -- HUMOZF TO DO
          /*
          OZF_CLAIM_ACCRUAL_PVT.Del_Line_Util_By_Group(x_return_status => l_return_status,
                                 x_msg_count     => x_msg_count,
                                 x_msg_data      => x_msg_data,
                                 p_line_util_rec => l_line_util_tbl(i),
                                 px_currency_rec => l_currency_rec);
          */
          IF l_return_status = fnd_api.g_ret_sts_error
          THEN
            RAISE fnd_api.g_exc_error;
          ELSIF l_return_status = fnd_api.g_ret_sts_unexp_error
          THEN
            RAISE fnd_api.g_exc_unexpected_error;
          END IF;
        END IF;
        EXIT WHEN i = l_line_util_tbl.last;
        i := l_line_util_tbl.next(i);
      END LOOP;
    END IF;

    --------------------- 2. update -----------------------
    IF (l_upd_line_util_rec.claim_line_util_id IS NOT NULL AND
       l_upd_line_util_rec.claim_line_util_id <> fnd_api.g_miss_num)
    THEN
      -- Bugfix 5101106: Recalculate qty
      IF p_funds_util_flt.offer_type = 'SCAN_DATA'
      THEN
        OPEN csr_offer_profile(l_funds_util_flt.activity_product_id);
        FETCH csr_offer_profile
          INTO l_offer_uom_code
              ,l_offer_quantity
              ,l_scan_value
              ,l_product_id;
        CLOSE csr_offer_profile;
        IF l_funds_util_flt.uom_code <> l_offer_uom_code
        THEN
          l_upd_line_util_rec.quantity := inv_convert.inv_um_convert(item_id       => l_product_id
                                                                    ,PRECISION     => 2
                                                                    ,from_quantity => l_upd_line_util_rec.scan_unit *
                                                                                      l_offer_quantity
                                                                    ,from_unit     => l_offer_uom_code
                                                                    ,to_unit       => l_funds_util_flt.uom_code
                                                                    ,from_name     => NULL
                                                                    ,to_name       => NULL);
        ELSE
          l_upd_line_util_rec.quantity := l_upd_line_util_rec.scan_unit *
                                          l_offer_quantity;
        END IF;
      END IF;

      -- modified call for HUMOZF
      /*
      OZF_CLAIM_ACCRUAL_PVT.Update_Line_Util(p_api_version      => l_api_version,
                                             p_init_msg_list    => FND_API.g_false,
                                             p_commit           => FND_API.g_false,
                                             p_validation_level => FND_API.g_valid_level_full,
                                             x_return_status    => l_return_status,
                                             x_msg_count        => x_msg_count,
                                             x_msg_data         => x_msg_data,
                                             p_line_util_rec    => l_upd_line_util_rec,
                                             x_object_version   => l_object_version_number);*/
      IF l_return_status = fnd_api.g_ret_sts_error
      THEN
        RAISE fnd_api.g_exc_error;
      ELSIF l_return_status = fnd_api.g_ret_sts_unexp_error
      THEN
        RAISE fnd_api.g_exc_unexpected_error;
      END IF;
      l_upd_line_util_rec.object_version_number := l_object_version_number;
    END IF;

    -- 3. update claim line earnings_associated_flag -------
    -- if there is no more earnings associated.
    OPEN csr_final_lu_amt(p_funds_util_flt.claim_line_id);
    FETCH csr_final_lu_amt
      INTO l_final_lu_amt;
    CLOSE csr_final_lu_amt;

    IF l_final_lu_amt = 0 OR l_final_lu_amt IS NULL
    THEN
      UPDATE apps.ozf_claim_lines_all
         SET earnings_associated_flag = 'F'
       WHERE claim_line_id = p_funds_util_flt.claim_line_id;
    END IF;

    ------------------------- finish -------------------------------
    -- Check for commit
    IF fnd_api.to_boolean(p_commit)
    THEN
      COMMIT;
    END IF;

    fnd_msg_pub.count_and_get(p_encoded => fnd_api.g_false
                             ,p_count   => x_msg_count
                             ,p_data    => x_msg_data);

    IF ozf_debug_high_on
    THEN
      ozf_utility_pvt.debug_message(l_full_name || ': end');
    END IF;

  EXCEPTION
    WHEN fnd_api.g_exc_error THEN
      ROLLBACK TO delete_group_line_util;
      x_return_status := fnd_api.g_ret_sts_error;
      fnd_msg_pub.count_and_get(p_encoded => fnd_api.g_false
                               ,p_count   => x_msg_count
                               ,p_data    => x_msg_data);

    WHEN fnd_api.g_exc_unexpected_error THEN
      ROLLBACK TO delete_group_line_util;
      x_return_status := fnd_api.g_ret_sts_unexp_error;
      fnd_msg_pub.count_and_get(p_encoded => fnd_api.g_false
                               ,p_count   => x_msg_count
                               ,p_data    => x_msg_data);

    WHEN OTHERS THEN
      ROLLBACK TO delete_group_line_util;
      x_return_status := fnd_api.g_ret_sts_unexp_error;
      IF fnd_msg_pub.check_msg_level(fnd_msg_pub.g_msg_lvl_unexp_error)
      THEN
        fnd_msg_pub.add_exc_msg(g_pkg_name, l_api_name);
      END IF;
      fnd_msg_pub.count_and_get(p_encoded => fnd_api.g_false
                               ,p_count   => x_msg_count
                               ,p_data    => x_msg_data);

  END delete_group_line_util;

  ---------------------------------------------------------------------
  -- PROCEDURE
  --    Delete_All_Line_Utils
  --
  -- HISTORY
  --    04-Jul-2005  Sahana  Created for Bug4348163
  --    08-Aug-06    azahmed Modified for FXGL ER
  ---------------------------------------------------------------------
  PROCEDURE delete_all_line_util(p_api_version      IN NUMBER
                                ,p_init_msg_list    IN VARCHAR2 := fnd_api.g_false
                                ,p_commit           IN VARCHAR2 := fnd_api.g_false
                                ,p_validation_level IN NUMBER := fnd_api.g_valid_level_full

                                ,x_return_status OUT NOCOPY VARCHAR2
                                ,x_msg_count     OUT NOCOPY NUMBER
                                ,x_msg_data      OUT NOCOPY VARCHAR2

                                ,p_funds_util_flt IN funds_util_flt_type
                                ,p_currency_rec   IN currency_rec_type
                                ,p_mode           IN VARCHAR2 := ozf_claim_utility_pvt.g_auto_mode) IS
    l_api_version CONSTANT NUMBER := 1.0;
    l_api_name    CONSTANT VARCHAR2(30) := 'Delete_All_Line_Util';
    l_full_name   CONSTANT VARCHAR2(60) := g_pkg_name || '.' || l_api_name;
    l_return_status VARCHAR2(1);

    TYPE fundsutilcsrtyp IS REF CURSOR;
    l_funds_util_csr        NUMBER; --FundsUtilCsrTyp;
    l_funds_util_sql        VARCHAR2(3000);
    l_line_util_tbl         ozf_claim_accrual_pvt.line_util_tbl_type;
    l_lu_line_util_id       NUMBER;
    l_lu_utilization_id     NUMBER;
    l_lu_amt                NUMBER;
    l_lu_scan_unit          NUMBER;
    l_counter               PLS_INTEGER := 1;
    l_object_version_number NUMBER;
    l_ignore                NUMBER;
    l_utiz_amount           NUMBER;
    l_lu_currency_code      VARCHAR2(15);
    l_currency_rec          currency_rec_type := p_currency_rec;

    CURSOR csr_util_obj_ver(cv_line_util_id IN NUMBER) IS
      SELECT object_version_number
        FROM apps.ozf_claim_lines_util_all
       WHERE claim_line_util_id = cv_line_util_id;

  BEGIN
    --------------------- initialize -----------------------
    SAVEPOINT delete_all_line_util;

    IF ozf_debug_high_on
    THEN
      ozf_utility_pvt.debug_message(l_full_name || ': start');
    END IF;

    IF fnd_api.to_boolean(p_init_msg_list)
    THEN
      fnd_msg_pub.initialize;
    END IF;

    IF NOT fnd_api.compatible_api_call(l_api_version
                                      ,p_api_version
                                      ,l_api_name
                                      ,g_pkg_name)
    THEN
      RAISE fnd_api.g_exc_unexpected_error;
    END IF;

    x_return_status := fnd_api.g_ret_sts_success;

    --------------------- start -----------------------
    get_utiz_sql_stmt(p_api_version      => l_api_version
                     ,p_init_msg_list    => fnd_api.g_false
                     ,p_commit           => fnd_api.g_false
                     ,p_validation_level => fnd_api.g_valid_level_full
                     ,x_return_status    => l_return_status
                     ,x_msg_count        => x_msg_count
                     ,x_msg_data         => x_msg_data
                     ,p_summary_view     => 'DEL_GRP_LINE_UTIL'
                     ,p_funds_util_flt   => p_funds_util_flt
                     ,px_currency_rec    => l_currency_rec
                     ,p_cust_account_id  => p_funds_util_flt.cust_account_id
                     ,x_utiz_sql_stmt    => l_funds_util_sql);
    IF l_return_status = fnd_api.g_ret_sts_error
    THEN
      RAISE fnd_api.g_exc_error;
    ELSIF l_return_status = fnd_api.g_ret_sts_unexp_error
    THEN
      RAISE fnd_api.g_exc_unexpected_error;
    END IF;

    -- use FND_DSQL package for dynamic sql and bind variables
    l_funds_util_csr := dbms_sql.open_cursor;
    fnd_dsql.set_cursor(l_funds_util_csr);
    dbms_sql.parse(l_funds_util_csr, l_funds_util_sql, dbms_sql.native);
    dbms_sql.define_column(l_funds_util_csr, 1, l_lu_line_util_id);
    dbms_sql.define_column(l_funds_util_csr, 2, l_lu_utilization_id);
    dbms_sql.define_column(l_funds_util_csr, 3, l_lu_amt);
    dbms_sql.define_column(l_funds_util_csr, 4, l_lu_scan_unit);
    dbms_sql.define_column(l_funds_util_csr, 5, l_lu_currency_code, 15);
    --  DBMS_SQL.define_column(l_funds_util_csr, 5, l_utiz_amount);
    fnd_dsql.do_binds;

    l_ignore := dbms_sql.execute(l_funds_util_csr);
    LOOP
      IF dbms_sql.fetch_rows(l_funds_util_csr) > 0
      THEN
        dbms_sql.column_value(l_funds_util_csr, 1, l_lu_line_util_id);
        dbms_sql.column_value(l_funds_util_csr, 2, l_lu_utilization_id);
        dbms_sql.column_value(l_funds_util_csr, 3, l_lu_amt);
        dbms_sql.column_value(l_funds_util_csr, 4, l_lu_scan_unit);
        dbms_sql.column_value(l_funds_util_csr, 5, l_lu_currency_code);
        --    DBMS_SQL.define_column(l_funds_util_csr, 5, l_utiz_amount);

        OPEN csr_util_obj_ver(l_lu_line_util_id);
        FETCH csr_util_obj_ver
          INTO l_object_version_number;
        CLOSE csr_util_obj_ver;

        -- modified call for HUMOZF
        /*
        OZF_CLAIM_ACCRUAL_PVT.Delete_Line_Util(p_api_version    => l_api_version,
                                               p_init_msg_list  => FND_API.g_false,
                                               p_commit         => FND_API.g_false,
                                               x_return_status  => l_return_status,
                                               x_msg_data       => x_msg_data,
                                               x_msg_count      => x_msg_count,
                                               p_line_util_id   => l_lu_line_util_id,
                                               p_object_version => l_object_version_number,
                                               p_mode           => OZF_CLAIM_UTILITY_PVT.g_auto_mode);*/
        IF l_return_status = fnd_api.g_ret_sts_error
        THEN
          RAISE fnd_api.g_exc_error;
        ELSIF l_return_status = fnd_api.g_ret_sts_unexp_error
        THEN
          RAISE fnd_api.g_exc_unexpected_error;
        END IF;
      ELSE
        EXIT;
      END IF;
    END LOOP;
    dbms_sql.close_cursor(l_funds_util_csr);

    -- Check for commit
    IF fnd_api.to_boolean(p_commit)
    THEN
      COMMIT;
    END IF;

    fnd_msg_pub.count_and_get(p_encoded => fnd_api.g_false
                             ,p_count   => x_msg_count
                             ,p_data    => x_msg_data);

    IF ozf_debug_high_on
    THEN
      ozf_utility_pvt.debug_message(l_full_name || ': end');
    END IF;

  EXCEPTION
    WHEN fnd_api.g_exc_error THEN
      ROLLBACK TO delete_all_line_util;
      x_return_status := fnd_api.g_ret_sts_error;
      fnd_msg_pub.count_and_get(p_encoded => fnd_api.g_false
                               ,p_count   => x_msg_count
                               ,p_data    => x_msg_data);

    WHEN fnd_api.g_exc_unexpected_error THEN
      ROLLBACK TO delete_all_line_util;
      x_return_status := fnd_api.g_ret_sts_unexp_error;
      fnd_msg_pub.count_and_get(p_encoded => fnd_api.g_false
                               ,p_count   => x_msg_count
                               ,p_data    => x_msg_data);

    WHEN OTHERS THEN
      ROLLBACK TO delete_all_line_util;
      x_return_status := fnd_api.g_ret_sts_unexp_error;
      IF fnd_msg_pub.check_msg_level(fnd_msg_pub.g_msg_lvl_unexp_error)
      THEN
        fnd_msg_pub.add_exc_msg(g_pkg_name, l_api_name);
      END IF;
      fnd_msg_pub.count_and_get(p_encoded => fnd_api.g_false
                               ,p_count   => x_msg_count
                               ,p_data    => x_msg_data);

  END delete_all_line_util;

  ---------------------------------------------------------------------
  -- PROCEDURE
  --    Populate_Line_Util_Tbl
  --
  -- HISTORY
  --    11-Mar-2009  psomyaju  Create.
  --    10-Jun-2009  kpatro   Fix for Bug 8583847
  -- -----------------------------------------------------------------------------
  --   VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- -------   -----------   --------------- ---------------------------------
  --    1.0                                   Created
  --    1.1    12-Jul-2013   Kathy Poling     SR 208563 - Issue #308 TM Claims are
  --                                          being duplicated.
  --------------------------------------------------------------------------------
  PROCEDURE populate_line_util_tbl(p_funds_util_flt        funds_util_flt_type
                                  ,p_source_object_class   IN VARCHAR2
                                  ,p_source_object_id      IN NUMBER
                                  ,p_request_header_id     IN NUMBER
                                  ,p_batch_line_id         IN NUMBER
                                  ,p_batch_type            IN VARCHAR2
                                  ,p_summary_view          IN VARCHAR2
                                  ,p_cre_util_amount       IN NUMBER
                                  ,p_prorate_earnings_flag IN VARCHAR2
                                  ,p_pmt_frequency         IN VARCHAR2
                                  ,p_pmt_method            IN VARCHAR2
                                  ,p_invoice_type          IN VARCHAR2
                                  ,p_currency_rec          IN currency_rec_type
                                  ,x_funds_rem_tbl         OUT NOCOPY funds_rem_tbl_type
                                  ,x_tot_accrual_amt       OUT NOCOPY NUMBER
                                  ,x_line_amount           OUT NOCOPY NUMBER
                                  ,x_line_util_tbl         OUT NOCOPY line_util_tbl_type
                                  ,x_return_status         OUT NOCOPY VARCHAR2
                                  ,x_msg_data              OUT NOCOPY VARCHAR2
                                  ,x_msg_count             OUT NOCOPY NUMBER) IS

    l_funds_util_csr       NUMBER;
    l_funds_util_sql       VARCHAR2(3000);
    l_total_pay_over_flag  BOOLEAN := FALSE;
    l_util_id              NUMBER := 0;
    l_fu_amt_rem           NUMBER := 0;
    l_total_amt_rem        NUMBER := 0;
    l_tot_accrual_amt      NUMBER := 0;
    l_total_prorate_amount NUMBER := 0;
    l_prorate_amount       NUMBER := 0;
    l_fu_scan_unit_rem     NUMBER := 0;
    l_total_scan_unit_rem  NUMBER := 0;
    l_fu_currency_code     VARCHAR2(15);
    l_util_uom_code        VARCHAR2(15);
    l_util_quantity        NUMBER := 0;
    l_ignore               NUMBER := 0;
    l_funds_used_units     NUMBER := 0;
    j                      NUMBER := 0;
    l_exit                 BOOLEAN := FALSE;
    l_line_rem_amount      NUMBER := 0;
    l_fu_exchange_rate     NUMBER := 0;
    l_fu_exc_rate_type     VARCHAR2(30);
    l_fu_exc_rate_date     DATE;
    l_conv_exchange_rate   NUMBER := 0;
    l_batch_curr_claim_amt NUMBER := 0;

    l_api_version CONSTANT NUMBER := 1.0;
    l_api_name    CONSTANT VARCHAR2(30) := 'Populate_Line_Util_Tbl';
    l_full_name   CONSTANT VARCHAR2(60) := g_pkg_name || '.' || l_api_name;
    l_return_status VARCHAR2(15);
    l_msg_count     NUMBER;
    l_msg_data      VARCHAR2(15);

    l_line_util_tbl ozf_claim_accrual_pvt.line_util_tbl_type;
    l_funds_rem_tbl funds_rem_tbl_type;
    l_currency_rec  currency_rec_type := p_currency_rec;

    TYPE claim_line_util1 IS REF CURSOR;
    clm_line_util claim_line_util1;

    -- Bugfix 4926327
    -- Bugfix 5101106
    CURSOR csr_funds_used_units(cv_activity_product_id IN NUMBER) IS
      SELECT nvl(SUM(scan_unit * amp.quantity), 0)
        FROM apps.ozf_claim_lines_util lu
            ,apps.ozf_claims           c
            ,apps.ozf_claim_lines      cl
            ,apps.ams_act_products     amp
       WHERE lu.activity_product_id = cv_activity_product_id
         AND cl.claim_line_id = lu.claim_line_id
         AND cl.claim_id = c.claim_id
         AND c.status_code <> 'CLOSED'
         AND lu.activity_product_id = amp.activity_product_id
         AND lu.utilization_id <> -1;

    CURSOR csr_sd_accruals(cv_batch_id          NUMBER
                          ,cv_product_id        NUMBER
                          ,cv_request_header_id NUMBER
                          ,cv_batch_line_id     NUMBER) IS
      SELECT util.utilization_id
            ,util.exchange_rate
            ,util.exchange_rate_date
            ,util.exchange_rate_type
            ,line.batch_curr_claim_amount
        FROM apps.ozf_funds_utilized_all_b util
            ,apps.ozf_sd_batch_lines_all   line
       WHERE util.utilization_id = line.utilization_id
         AND line.status_code = 'APPROVED' --//Bugfix: 9794989
         AND line.purge_flag = 'N'
         AND line.batch_id = cv_batch_id
         AND line.item_id = cv_product_id
         AND util.reference_id = cv_request_header_id
         AND line.batch_line_id = nvl(cv_batch_line_id, batch_line_id)
         AND util.reference_type = 'SD_REQUEST'
         AND line.batch_curr_claim_amount <> 0;

    CURSOR csr_line_utils(v_fund_id         IN NUMBER
                         ,v_plan_id         IN NUMBER
                         ,v_cust_account_id IN NUMBER
                         ,v_pmt_frequency   IN VARCHAR2
                         ,v_pmt_method      IN VARCHAR2) IS
--Version 1.1  7/12/2013
/*      SELECT fu.utilization_id
            ,fu.amount_remaining
            ,fu.currency_code
            ,fu.scan_unit_remaining

        FROM apps.ozf_funds_utilized_all_b fu
            ,apps.qp_list_headers_b        qpb
            ,apps.ozf_offers               oof
            ,apps.qp_qualifiers            qpq
            ,apps.ozf_resale_lines_all     orl
       WHERE fu.plan_type = 'OFFR'
         AND fu.plan_id = qpb.list_header_id
         AND fu.object_id = orl.resale_line_id
         AND fu.cust_account_id = orl.sold_from_cust_account_id
         AND qpb.list_header_id = qpq.list_header_id
         AND oof.qp_list_header_id = qpb.list_header_id
         AND qpb.attribute5 = nvl(v_pmt_method, qpb.attribute5)
         AND qpb.attribute6 = nvl(v_pmt_frequency, qpb.attribute6)
         AND nvl(oof.autopay_flag, 'Y') = 'Y'
            --AND oof.autopay_flag = 'Y'
         AND fu.fund_id = nvl(v_fund_id, fu.fund_id)
         AND fu.plan_curr_amount_remaining <> 0
         AND fu.utilization_type IN ('ACCRUAL', 'ADJUSTMENT')
         AND fu.cust_account_id =
             nvl(v_cust_account_id, fu.cust_account_id)
         AND fu.plan_id = nvl(v_plan_id, fu.plan_id)
         AND qualifier_attribute = 'QUALIFIER_ATTRIBUTE2'
         AND qualifier_context = 'SOLD_BY'
         AND fu.gl_posted_flag = 'Y';
*/
      SELECT fu.utilization_id
            ,fu.amount_remaining
            ,fu.currency_code
            ,fu.scan_unit_remaining
        FROM apps.ozf_funds_utilized_all_b fu
            ,apps.qp_list_headers_b        qpb
            ,apps.ozf_offers               oof
            ,(SELECT DISTINCT attribute5
                                     ,attribute6
                                     ,list_header_id
                        FROM apps.qp_qualifiers
                       WHERE qualifier_attribute = 'QUALIFIER_ATTRIBUTE2'
                         AND qualifier_context = 'SOLD_BY') qpq
            ,apps.ozf_resale_lines_all     orl
       WHERE fu.plan_type = 'OFFR'
         AND fu.plan_id = qpb.list_header_id
         AND fu.object_id = orl.resale_line_id
         AND fu.cust_account_id = orl.sold_from_cust_account_id
         AND qpb.list_header_id = qpq.list_header_id
         AND oof.qp_list_header_id = qpb.list_header_id
         AND qpb.attribute5 = nvl(v_pmt_method, qpb.attribute5)
         AND qpb.attribute6 = nvl(v_pmt_frequency, qpb.attribute6)
         AND nvl(oof.autopay_flag, 'Y') = 'Y'
         AND fu.fund_id = nvl(v_fund_id, fu.fund_id)
         AND fu.plan_curr_amount_remaining <> 0
         AND fu.utilization_type IN ('ACCRUAL', 'ADJUSTMENT')
         AND fu.cust_account_id =
             nvl(v_cust_account_id, fu.cust_account_id)
         AND fu.plan_id = nvl(v_plan_id, fu.plan_id)
         AND fu.gl_posted_flag = 'Y';
--7/12/2013

  BEGIN

    IF ozf_debug_high_on
    THEN
      ozf_utility_pvt.debug_message(l_full_name || ': start');
    END IF;

    -- Fix for Bug 8583847
    l_line_rem_amount := p_cre_util_amount;

    SAVEPOINT populate_line_util_tbl;

    --For Supplier Ship Debit claims, since we already know the utilizations during batch creation.
    --Hence, with batch lines, we can get the qualified utilizations directly.
    IF nvl(p_source_object_class, 'X') = 'SD_SUPPLIER'
    THEN
      --//ER 9226258
      OPEN csr_sd_accruals(p_source_object_id
                          ,p_funds_util_flt.product_id
                          ,p_request_header_id
                          ,p_batch_line_id);
      j := 0;
      LOOP
        FETCH csr_sd_accruals
          INTO l_util_id
              ,l_fu_exchange_rate
              ,l_fu_exc_rate_date
              ,l_fu_exc_rate_type
              ,l_batch_curr_claim_amt;
        EXIT WHEN csr_sd_accruals%NOTFOUND;

        x_line_util_tbl(j).utilization_id := l_util_id;
        x_line_util_tbl(j).claim_line_id := p_funds_util_flt.claim_line_id;
        --x_line_util_tbl(j).activity_product_id := p_funds_util_flt.activity_product_id;
        --x_line_util_tbl(j).uom_code := l_util_uom_code;
        --x_line_util_tbl(j).update_from_tbl_flag := FND_API.g_true;

        IF (l_currency_rec.transaction_currency_code =
           l_currency_rec.claim_currency_code)
        THEN
          x_line_util_tbl(j).amount := l_batch_curr_claim_amt;
        ELSE
          IF l_currency_rec.claim_currency_code <>
             l_currency_rec.functional_currency_code
          THEN
            ozf_utility_pvt.convert_currency(p_from_currency => l_currency_rec.claim_currency_code
                                            ,p_to_currency   => l_currency_rec.functional_currency_code
                                            ,p_conv_type     => l_fu_exc_rate_type
                                            ,p_conv_rate     => l_fu_exchange_rate
                                            ,p_conv_date     => SYSDATE
                                            ,p_from_amount   => l_batch_curr_claim_amt
                                            ,x_return_status => l_return_status
                                            ,x_to_amount     => x_line_util_tbl(j)
                                                                .amount
                                            ,x_rate          => l_conv_exchange_rate);

            IF l_return_status = fnd_api.g_ret_sts_error
            THEN
              RAISE fnd_api.g_exc_error;
            ELSIF l_return_status = fnd_api.g_ret_sts_unexp_error
            THEN
              RAISE fnd_api.g_exc_unexpected_error;
            END IF;

            x_line_util_tbl(j).amount := ozf_utility_pvt.currround(x_line_util_tbl(j)
                                                                   .amount
                                                                  ,l_currency_rec.functional_currency_code);

          ELSE
            x_line_util_tbl(j).amount := l_batch_curr_claim_amt;
          END IF;
        END IF;
        j := j + 1;
      END LOOP;
      CLOSE csr_sd_accruals;

    ELSE

      IF p_funds_util_flt.pay_over_all_flag IS NULL
      THEN
        l_total_pay_over_flag := FALSE;
      ELSE
        l_total_pay_over_flag := p_funds_util_flt.pay_over_all_flag;
      END IF;

      --Get all the qualified utilizations for current claim line association on the basis of
      --customer, offer and product. If pay over earnings is allowed, then no need to identify
      --utilizations. We will create adjustments for entire pay over earnings.
      --Bugfix 5144750
      IF NOT (l_total_pay_over_flag)
      THEN

        /*
        Get_Utiz_Sql_Stmt(p_api_version      => 1.0,
                          p_init_msg_list    => FND_API.g_false,
                          p_commit           => FND_API.g_false,
                          p_validation_level => FND_API.g_valid_level_full,
                          x_return_status    => l_return_status,
                          x_msg_count        => l_msg_count,
                          x_msg_data         => l_msg_data,
                          p_summary_view     => p_summary_view,
                          p_funds_util_flt   => p_funds_util_flt,
                          px_currency_rec    => l_currency_rec,
                          p_cust_account_id  => p_funds_util_flt.cust_account_id,
                          x_utiz_sql_stmt    => l_funds_util_sql);
        IF l_return_status = FND_API.g_ret_sts_error THEN
           RAISE FND_API.g_exc_error;
        ELSIF l_return_status = FND_API.g_ret_sts_unexp_error THEN
           RAISE FND_API.g_exc_error;
        END IF;
        */

        fnd_file.put_line(fnd_file.log
                         ,'---------------------------------');
        fnd_file.put_line(fnd_file.log
                         ,'Customer ID         :  ' ||
                          p_funds_util_flt.cust_account_id);
        fnd_file.put_line(fnd_file.log
                         ,'Fund ID         :  ' ||
                          p_funds_util_flt.fund_id);
        fnd_file.put_line(fnd_file.log
                         ,'Plan ID           :  ' ||
                          p_funds_util_flt.activity_id);
        fnd_file.put_line(fnd_file.log
                         ,'Product Id        :  ' ||
                          p_funds_util_flt.product_id);
        fnd_file.put_line(fnd_file.log
                         ,'Rx Plan Id        :  ' ||
                          p_funds_util_flt.reference_type);

        --
        j := 0;

        --
        IF p_invoice_type = 'INV'
        THEN

          OPEN clm_line_util FOR
--Version 1.1
/*            SELECT fu.utilization_id
                  ,fu.amount_remaining
                  ,fu.currency_code
                  ,fu.scan_unit_remaining
              FROM apps.ozf_funds_utilized_all_b fu
                  ,apps.qp_list_headers_b        qpb
                  ,apps.ozf_offers               oof
                  ,apps.qp_qualifiers            qpq
                  ,apps.ozf_resale_lines_all     orl
             WHERE fu.plan_type = 'OFFR'
               AND fu.plan_id = qpb.list_header_id
               AND fu.object_id = orl.resale_line_id
               AND fu.cust_account_id = orl.sold_from_cust_account_id
               AND qpb.list_header_id = qpq.list_header_id
               AND oof.qp_list_header_id = qpb.list_header_id
               AND qpb.attribute5 = nvl(p_pmt_method, qpb.attribute5)
               AND qpb.attribute6 = nvl(p_pmt_frequency, qpb.attribute6)
               AND nvl(oof.autopay_flag, 'Y') = 'Y'
                  --AND oof.autopay_flag = 'Y'
               AND fu.fund_id = nvl(p_funds_util_flt.fund_id, fu.fund_id)
               AND fu.plan_curr_amount_remaining <> 0
               AND fu.utilization_type IN ('ACCRUAL', 'ADJUSTMENT')
               AND fu.cust_account_id =
                   nvl(p_funds_util_flt.cust_account_id, fu.cust_account_id)
               AND fu.plan_id =
                   nvl(p_funds_util_flt.activity_id, fu.plan_id)
               AND qualifier_attribute = 'QUALIFIER_ATTRIBUTE2'
               AND qualifier_context = 'SOLD_BY'
               AND fu.gl_posted_flag = 'Y';
*/
            SELECT fu.utilization_id
                  ,fu.amount_remaining
                  ,fu.currency_code
                  ,fu.scan_unit_remaining
              FROM apps.ozf_funds_utilized_all_b fu
                  ,apps.qp_list_headers_b        qpb
                  ,apps.ozf_offers               oof
                  ,(SELECT DISTINCT attribute5
                                     ,attribute6
                                     ,list_header_id
                        FROM apps.qp_qualifiers
                       WHERE qualifier_attribute = 'QUALIFIER_ATTRIBUTE2'
                         AND qualifier_context = 'SOLD_BY') qpq
                  ,apps.ozf_resale_lines_all     orl
             WHERE fu.plan_type = 'OFFR'
               AND fu.plan_id = qpb.list_header_id
               AND fu.object_id = orl.resale_line_id
               AND fu.cust_account_id = orl.sold_from_cust_account_id
               AND qpb.list_header_id = qpq.list_header_id
               AND oof.qp_list_header_id = qpb.list_header_id
               AND qpb.attribute5 = nvl(p_pmt_method, qpb.attribute5)
               AND qpb.attribute6 = nvl(p_pmt_frequency, qpb.attribute6)
               AND nvl(oof.autopay_flag, 'Y') = 'Y'
               AND fu.fund_id = nvl(p_funds_util_flt.fund_id, fu.fund_id)
               AND fu.plan_curr_amount_remaining <> 0
               AND fu.utilization_type IN ('ACCRUAL', 'ADJUSTMENT')
               AND fu.cust_account_id =
                   nvl(p_funds_util_flt.cust_account_id, fu.cust_account_id)
               AND fu.plan_id =
                   nvl(p_funds_util_flt.activity_id, fu.plan_id)
               AND fu.gl_posted_flag = 'Y';
--7/12/2013
        ELSE
          OPEN clm_line_util FOR
--Version 1.1
/*            SELECT fu.utilization_id
                  ,fu.amount_remaining
                  ,fu.currency_code
                  ,fu.scan_unit_remaining
              FROM apps.ozf_funds_utilized_all_b fu
                  ,apps.qp_list_headers_b        qpb
                  ,apps.ozf_offers               oof
                  ,apps.qp_qualifiers            qpq
                  ,apps.ozf_resale_lines_all     orl
             WHERE fu.plan_type = 'OFFR'
               AND fu.plan_id = qpb.list_header_id
               AND fu.object_id = orl.resale_line_id
               AND fu.cust_account_id = orl.sold_from_cust_account_id
               AND qpb.list_header_id = qpq.list_header_id
               AND oof.qp_list_header_id = qpb.list_header_id
               AND qpb.attribute5 = nvl(p_pmt_method, qpb.attribute5)
               AND qpb.attribute6 = nvl(p_pmt_frequency, qpb.attribute6)
               AND nvl(oof.autopay_flag, 'Y') = 'Y'
                  --AND oof.autopay_flag = 'Y'
               AND fu.fund_id = nvl(p_funds_util_flt.fund_id, fu.fund_id)
               AND fu.plan_curr_amount_remaining <> 0
               AND fu.attribute10 = 'REIMBURSEMENT'
               AND fu.utilization_type IN ('ACCRUAL', 'ADJUSTMENT')
               AND fu.cust_account_id =
                   nvl(p_funds_util_flt.cust_account_id, fu.cust_account_id)
               AND fu.plan_id =
                   nvl(p_funds_util_flt.activity_id, fu.plan_id)
               AND qualifier_attribute = 'QUALIFIER_ATTRIBUTE2'
               AND qualifier_context = 'SOLD_BY'
               AND fu.gl_posted_flag = 'Y';
*/
           SELECT fu.utilization_id
                  ,fu.amount_remaining
                  ,fu.currency_code
                  ,fu.scan_unit_remaining
              FROM apps.ozf_funds_utilized_all_b fu
                  ,apps.qp_list_headers_b        qpb
                  ,apps.ozf_offers               oof
                  ,(SELECT DISTINCT attribute5
                                     ,attribute6
                                     ,list_header_id
                        FROM apps.qp_qualifiers
                       WHERE qualifier_attribute = 'QUALIFIER_ATTRIBUTE2'
                         AND qualifier_context = 'SOLD_BY') qpq
                  ,apps.ozf_resale_lines_all     orl
             WHERE fu.plan_type = 'OFFR'
               AND fu.plan_id = qpb.list_header_id
               AND fu.object_id = orl.resale_line_id
               AND fu.cust_account_id = orl.sold_from_cust_account_id
               AND qpb.list_header_id = qpq.list_header_id
               AND oof.qp_list_header_id = qpb.list_header_id
               AND qpb.attribute5 = nvl(p_pmt_method, qpb.attribute5)
               AND qpb.attribute6 = nvl(p_pmt_frequency, qpb.attribute6)
               AND nvl(oof.autopay_flag, 'Y') = 'Y'
               AND fu.fund_id = nvl(p_funds_util_flt.fund_id, fu.fund_id)
               AND fu.plan_curr_amount_remaining <> 0
               AND fu.attribute10 --= 'REIMBURSEMENT'
                        IN ( SELECT LOOKUP_CODE
                             from apps.fnd_lookup_values_vl
                             WHERE     1 = 1
                             and lookup_type = 'XXCUS_OZF_NEG_INVOICE_TYPES')
               AND fu.utilization_type IN ('ACCRUAL', 'ADJUSTMENT')
               AND fu.cust_account_id =
                   nvl(p_funds_util_flt.cust_account_id, fu.cust_account_id)
               AND fu.plan_id =
                   nvl(p_funds_util_flt.activity_id, fu.plan_id)
               AND fu.gl_posted_flag = 'Y';
--7/12/2013
        END IF;

        LOOP
          FETCH clm_line_util
            INTO l_util_id
                ,l_fu_amt_rem
                ,l_fu_currency_code
                ,l_fu_scan_unit_rem;

          EXIT WHEN clm_line_util%NOTFOUND;

          /*
          OPEN csr_funds_used_units(p_funds_util_flt.activity_product_id);
          FETCH csr_funds_used_units
             INTO l_funds_used_units;
          CLOSE csr_funds_used_units;
          */

          l_funds_rem_tbl(j).utilization_id := l_util_id;
          l_funds_rem_tbl(j).amount_remaining := l_fu_amt_rem;
          /*l_funds_rem_tbl(j).scan_unit_remaining := l_fu_scan_unit_rem -
          l_funds_used_units;*/
          l_total_amt_rem := l_total_amt_rem + l_fu_amt_rem;
          --l_total_scan_unit_rem := l_total_scan_unit_rem + l_funds_rem_tbl(j).scan_unit_remaining;

          j := j + 1;

        END LOOP;
        CLOSE clm_line_util;

        --If total amount remaining for all the qualified adjustments is zero, then raise warning.
        --Raise error, if negative assocation is taking place for positive amount remaining.
        IF l_total_amt_rem < 0 AND
          --IF l_total_amt_rem = 0 AND
           nvl(p_funds_util_flt.offer_type, 'X') NOT IN
           ('ACCRUAL'
           ,'DEAL'
           ,'LUMPSUM'
           ,'NET_ACCRUAL'
           ,'VOLUME_OFFER'
           ,'SCAN_DATA')
        THEN
          /* IF FND_MSG_PUB.check_msg_level(FND_MSG_PUB.g_msg_lvl_error) THEN
             FND_MESSAGE.set_name('OZF', 'OZF_EARN_AVAIL_AMT_ZERO');
             FND_MSG_PUB.add;
          END IF;
          RAISE FND_API.g_exc_error; */
          NULL; --  Defect # 225 -- Negative and Zero Lines can be created ... No longer need this message
          -- Bugfix 8198443
        ELSIF (p_cre_util_amount < 0 AND l_total_amt_rem > 0) AND
              p_batch_type <> 'BATCH'
        THEN
          IF fnd_msg_pub.check_msg_level(fnd_msg_pub.g_msg_lvl_error)
          THEN
            fnd_message.set_name('OZF', 'OZF_ASSO_NEG_AMT');
            fnd_msg_pub.add;
          END IF;
          RAISE fnd_api.g_exc_error;
        END IF;

        /* Earnings Calculation:
        If prorate earnigns flag is set, then calculate earnings on prorate basis i.e. distribute association amount uniformly
        across qualified utilizations based on amount remaining of utilizations.
        Otherwise, earnings should be calculated in FIFO basis, i.e. in set of qualified utilizations, first utilization will
        consume association amount for its entire amount remaining value. Remaining amount will be used by next utilization in
        similar fashion and so on until either association amount exhaust or all utilizations processed. In this case, if association
        amount left after all utilizations process, this amount will be considered as pay over earnings and adjustment need to be
        created for this amount.
        */

        IF ozf_debug_high_on
        THEN
          ozf_utility_pvt.debug_message('l_funds_rem_tbl.COUNT = ' ||
                                        l_funds_rem_tbl.count);
          ozf_utility_pvt.debug_message('p_cre_util_amount = ' ||
                                        p_cre_util_amount);
          ozf_utility_pvt.debug_message('l_total_amt_rem = ' ||
                                        l_total_amt_rem);
          ozf_utility_pvt.debug_message('p_prorate_earnings_flag = ' ||
                                        p_prorate_earnings_flag);
        END IF;
        -- PrintLog('util count'||l_funds_rem_tbl.COUNT);
        -- PrintLog('Prorate Earnings'||p_prorate_earnings_flag);
        -- PrintLog('cre util amount'||p_cre_util_amount);
        -- PrintLog('TOtal Remain amount'||l_total_amt_rem);
        -- Bugfix 6042226
        IF l_funds_rem_tbl.count > 0
        THEN

          -- Commented this IF Condition for the Defect # 225 by CHandra Gadge 08/18/2011
          -- IF l_funds_rem_tbl.COUNT > 1 AND
          -- p_prorate_earnings_flag = 'F' AND
          -- ABS(p_cre_util_amount) <= ABS(l_total_amt_rem) THEN
          --PrintLog('In IF of Util');
          FOR i IN l_funds_rem_tbl.first .. l_funds_rem_tbl.last
          LOOP

            x_line_util_tbl(i).amount := l_funds_rem_tbl(i).amount_remaining;
            x_line_util_tbl(i).utilization_id := l_funds_rem_tbl(i)
                                                 .utilization_id;
            x_line_util_tbl(i).claim_line_id := p_funds_util_flt.claim_line_id;
            --x_line_util_tbl(i).activity_product_id := p_funds_util_flt.activity_product_id;--NS
          --x_line_util_tbl(i).uom_code            := p_funds_util_flt.uom_code;--NS
          ----x_line_util_tbl(i).quantity            := p_funds_util_flt.quantity;--NS
          --x_line_util_tbl(i).update_from_tbl_flag := FND_API.g_true;
          END LOOP;
          -- END IF;

          /*  IF l_funds_rem_tbl.COUNT > 1 AND
                   p_prorate_earnings_flag = 'T' AND
                   ABS(p_cre_util_amount) <= ABS(l_total_amt_rem) THEN

                   FOR i IN l_funds_rem_tbl.FIRST .. l_funds_rem_tbl.LAST LOOP

                      IF i = l_funds_rem_tbl.LAST THEN
                         -- Modified the logic for multiple lines - kpatro

                         l_prorate_amount := l_funds_rem_tbl(i).amount_remaining *
                                              (p_cre_util_amount /
                                               l_total_amt_rem);
                         l_prorate_amount := OZF_UTILITY_PVT.CurrRound(l_prorate_amount,
                                                                       l_currency_rec.association_currency_code);

                         -- modified the condition to <> to account for either less or greater condition

                         IF (l_total_prorate_amount + l_prorate_amount) <>
                            p_cre_util_amount THEN
                            l_prorate_amount := p_cre_util_amount -
                                                l_total_prorate_amount;
                         END IF;
                         x_line_util_tbl(i).amount := l_prorate_amount;
                      ELSE
                         l_prorate_amount := l_funds_rem_tbl(i)
                                             .amount_remaining *
                                              (p_cre_util_amount /
                                               l_total_amt_rem);
                         x_line_util_tbl(i).amount := OZF_UTILITY_PVT.CurrRound(l_prorate_amount,
                                                                                l_currency_rec.association_currency_code);
                      END IF;

                      l_total_prorate_amount := l_total_prorate_amount + x_line_util_tbl(i)
                                               .amount;

          FND_FILE.PUT_LINE(FND_FILE.LOG, '  l_total_prorate_amount       :  ' || l_total_prorate_amount);

                      -- commented the below code as it is calcualted above --by kpatro
                      --x_line_util_tbl(i).amount := l_prorate_amount;
                      x_line_util_tbl(i).utilization_id := l_funds_rem_tbl(i)
                                                           .utilization_id;
                      x_line_util_tbl(i).claim_line_id := p_funds_util_flt.claim_line_id;
                      --x_line_util_tbl(i).activity_product_id := p_funds_util_flt.activity_product_id;
                      --x_line_util_tbl(i).uom_code := p_funds_util_flt.uom_code;
                      --x_line_util_tbl(i).quantity := p_funds_util_flt.quantity;
                      x_line_util_tbl(i).update_from_tbl_flag := FND_API.g_true;
                   END LOOP;
                ELSE
                   -- Fix for Bug 8583847
                   --l_line_rem_amount := p_cre_util_amount;

                   FOR i IN l_funds_rem_tbl.FIRST .. l_funds_rem_tbl.LAST LOOP
                      l_fu_amt_rem := l_funds_rem_tbl(i).amount_remaining;

                      IF ((SIGN(p_cre_util_amount) = -1 AND
                         SIGN(l_total_amt_rem) = -1) AND
                         (p_cre_util_amount > l_total_amt_rem)) OR
                         ((SIGN(p_cre_util_amount) = 1 AND
                         SIGN(l_total_amt_rem) = 1) AND
                         (p_cre_util_amount < l_total_amt_rem)) THEN
                         IF l_line_rem_amount >= l_fu_amt_rem THEN

                            x_line_util_tbl(i).amount := l_fu_amt_rem;
                         ELSE
                            x_line_util_tbl(i).amount := l_line_rem_amount;
                         END IF;
                      ELSE
                         -- Bugfix 5404951
                         IF (p_batch_type = 'BATCH' AND
                            p_source_object_class = 'SPECIAL_PRICE' AND
                            p_funds_util_flt.offer_type = 'SCAN_DATA') THEN
                            x_line_util_tbl(i).amount := p_cre_util_amount;
                            l_exit := TRUE;
                         END IF;
                         IF SIGN(p_cre_util_amount) = SIGN(l_fu_amt_rem) THEN
                            l_tot_accrual_amt := NVL(l_tot_accrual_amt, 0) +
                                                 l_fu_amt_rem;
                         END IF;
                      END IF;

                      --Last utilization amount rounding
                      -- Fix for non prorate condition -- kpatro
                      --IF ABS(l_line_rem_amount) >= ABS(l_fu_amt_rem) THEN
                      IF l_line_rem_amount >= l_fu_amt_rem THEN
                         --nepanda : fix for bug # 9508390  - issue # 5
                         x_line_util_tbl(i).amount := l_fu_amt_rem;
                      ELSE
                         x_line_util_tbl(i).amount := l_line_rem_amount;
                      END IF;


                     -- IF p_funds_util_flt.offer_type = 'SCAN_DATA' THEN
                       --  x_line_util_tbl(i).scan_unit := x_line_util_tbl(i)
                      --                                   .amount /
                      --                                    G_SCAN_VALUE;
                     --    x_line_util_tbl(i).quantity := p_funds_util_flt.quantity;
                    --  END IF;


                      x_line_util_tbl(i).utilization_id := l_funds_rem_tbl(i).utilization_id;
                      x_line_util_tbl(i).claim_line_id := p_funds_util_flt.claim_line_id;
                      --x_line_util_tbl(i).activity_product_id := p_funds_util_flt.activity_product_id;
                      --x_line_util_tbl(i).uom_code := p_funds_util_flt.uom_code;
                      --x_line_util_tbl(i).update_from_tbl_flag := FND_API.g_true;

                      l_line_rem_amount := l_line_rem_amount - l_fu_amt_rem;
                      IF l_line_rem_amount <= 0 THEN
                         EXIT;
                      END IF;
                   END LOOP;
                END IF; */ --l_prorate_earnings_flag = 'T'
          --NULL;
        END IF; --l_funds_rem_tbl.COUNT > 0

        x_tot_accrual_amt := l_total_amt_rem;

      END IF; --l_total_pay_over_flag = FALSE
    END IF; --SD_SUPPLIER Check

    IF ozf_debug_high_on
    THEN
      IF x_line_util_tbl.count > 0
      THEN
        FOR i IN x_line_util_tbl.first .. x_line_util_tbl.last
        LOOP
          ozf_utility_pvt.debug_message('x_line_util_tbl(' || i ||
                                        ').utilization_id = ' || x_line_util_tbl(i)
                                        .utilization_id);
          ozf_utility_pvt.debug_message('x_line_util_tbl(' || i ||
                                        ').claim_line_id = ' || x_line_util_tbl(i)
                                        .claim_line_id);
          ozf_utility_pvt.debug_message('x_line_util_tbl(' || i ||
                                        ').activity_product_id = ' || x_line_util_tbl(i)
                                        .activity_product_id);
          ozf_utility_pvt.debug_message('x_line_util_tbl(' || i ||
                                        ').uom_code = ' || x_line_util_tbl(i)
                                        .uom_code);
          ozf_utility_pvt.debug_message('x_line_util_tbl(' || i ||
                                        ').amount = ' || x_line_util_tbl(i)
                                        .amount);
          ozf_utility_pvt.debug_message('x_line_util_tbl(' || i ||
                                        ').quantity = ' || x_line_util_tbl(i)
                                        .quantity);
          ozf_utility_pvt.debug_message('x_line_util_tbl(' || i ||
                                        ').scan_unit = ' || x_line_util_tbl(i)
                                        .scan_unit);
        END LOOP;
      END IF;
    END IF;

    -- Fix for Bug 8583847
    --x_line_amount := l_line_rem_amount;
    x_line_amount := l_total_amt_rem; -- Chandra Gadge 08/11/2011

    IF ozf_debug_high_on
    THEN
      ozf_utility_pvt.debug_message(l_full_name || ': end');
      ozf_utility_pvt.debug_message('x_line_amount :' || x_line_amount);
    END IF;

    x_funds_rem_tbl := l_funds_rem_tbl;

  EXCEPTION
    WHEN fnd_api.g_exc_error THEN
      ROLLBACK TO populate_line_util_tbl;
      x_return_status := fnd_api.g_ret_sts_error;
      fnd_msg_pub.count_and_get(p_encoded => fnd_api.g_false
                               ,p_count   => x_msg_count
                               ,p_data    => x_msg_data);

    WHEN fnd_api.g_exc_unexpected_error THEN
      ROLLBACK TO populate_line_util_tbl;
      x_return_status := fnd_api.g_ret_sts_unexp_error;
      fnd_msg_pub.count_and_get(p_encoded => fnd_api.g_false
                               ,p_count   => x_msg_count
                               ,p_data    => x_msg_data);

    WHEN OTHERS THEN
      ROLLBACK TO populate_line_util_tbl;
      x_return_status := fnd_api.g_ret_sts_unexp_error;
      IF fnd_msg_pub.check_msg_level(fnd_msg_pub.g_msg_lvl_unexp_error)
      THEN
        fnd_msg_pub.add_exc_msg(g_pkg_name, l_api_name);
      END IF;
      fnd_msg_pub.count_and_get(p_encoded => fnd_api.g_false
                               ,p_count   => x_msg_count
                               ,p_data    => x_msg_data);

  END populate_line_util_tbl;

  ---------------------------------------------------------------------
  -- PROCEDURE
  --    Update_Group_Line_Util
  --
  -- HISTORY
  --    10/05/2001  mchang  Create.
  --    11/03/2009  psomyaju  Re-organized code for R12 multicurrency ER
  ---------------------------------------------------------------------
  PROCEDURE update_group_line_util(p_api_version      IN NUMBER
                                  ,p_init_msg_list    IN VARCHAR2 := fnd_api.g_false
                                  ,p_commit           IN VARCHAR2 := fnd_api.g_false
                                  ,p_validation_level IN NUMBER := fnd_api.g_valid_level_full
                                  ,

                                   x_return_status OUT NOCOPY VARCHAR2
                                  ,x_msg_count     OUT NOCOPY NUMBER
                                  ,x_msg_data      OUT NOCOPY VARCHAR2
                                  ,

                                   p_summary_view   IN VARCHAR2 := NULL
                                  ,p_pmt_frequency  IN VARCHAR2
                                  ,p_pmt_method     IN VARCHAR2
                                  ,p_invoice_type   IN VARCHAR2
                                  ,p_funds_util_flt IN funds_util_flt_type
                                  ,p_mode           IN VARCHAR2 := ozf_claim_utility_pvt.g_auto_mode) IS

    l_api_version CONSTANT NUMBER := 1.0;
    l_api_name    CONSTANT VARCHAR2(30) := 'Update_Group_Line_Util';
    l_full_name   CONSTANT VARCHAR2(60) := g_pkg_name || '.' || l_api_name;
    l_return_status VARCHAR2(1);
    l_msg_count     NUMBER;
    l_msg_data      VARCHAR2(30);

    l_funds_util_flt funds_util_flt_type := p_funds_util_flt;
    l_line_util_tbl  line_util_tbl_type;
    l_funds_rem_tbl  funds_rem_tbl_type;
    l_currency_rec   currency_rec_type;

    l_asso_amount        NUMBER;
    l_exists_asso_amount NUMBER := 0;
    --l_exists_asso_amount      NUMBER;
    l_net_asso_amount       NUMBER;
    l_cre_util_amount       NUMBER;
    l_create_util           BOOLEAN := TRUE;
    l_claim_status          VARCHAR2(15);
    l_source_object_class   VARCHAR2(1024);
    l_batch_type            VARCHAR2(1024);
    l_claim_date            DATE;
    l_claim_exc_type        VARCHAR2(1024);
    l_claim_exc_date        DATE;
    l_claim_exc_rate        NUMBER;
    l_source_object_id      NUMBER;
    l_request_header_id     NUMBER; --Bugfix : 7717638
    l_batch_line_id         NUMBER; --Bugfix : 7811671
    l_convert_exchange_rate NUMBER;
    l_prorate_earnings_flag VARCHAR2(15);
    l_prorate_req_flag      BOOLEAN := FALSE;
    l_pay_over_amount       NUMBER;
    l_tot_accrual_amt       NUMBER := 0;
    l_error_index           NUMBER;
    l_asso_total_units      NUMBER;
    l_asso_uom_code         VARCHAR2(15);
    l_asso_total_quantity   NUMBER;
    l_org_id                NUMBER;
    l_claim_amt             NUMBER := 0;
    l_offer_currency        VARCHAR2(15);
    l_offer_status          VARCHAR2(30);
    l_pay_over_flag         BOOLEAN := FALSE;
    l_created_from          VARCHAR2(30);

    l_new_line_amount NUMBER := 0;

    CURSOR csr_function_currency(cv_org_id NUMBER) IS
      SELECT gs.currency_code
        FROM apps.gl_sets_of_books gs, apps.ozf_sys_parameters org
       WHERE org.set_of_books_id = gs.set_of_books_id
         AND org.org_id = cv_org_id;

    -- Bugfix 5404951
    CURSOR csr_claim_status(cv_claim_line_id IN NUMBER) IS
      SELECT cla.status_code
            ,cla.source_object_class
            ,cla.batch_type
            ,cla.currency_code
            ,cla.creation_date
            ,cla.exchange_rate_type
            ,cla.exchange_rate_date
            ,cla.exchange_rate
            ,cla.source_object_id
            ,cln.activity_id
            ,cln.batch_line_id
            ,cla.created_from
            ,cla.org_id
        FROM apps.ozf_claims cla, apps.ozf_claim_lines cln
       WHERE cla.claim_id = cln.claim_id
         AND cln.claim_line_id = cv_claim_line_id;

  BEGIN
    --------------------- initialize -----------------------
    SAVEPOINT update_group_line_util;
    x_return_status := fnd_api.g_ret_sts_success;

    --Set to handle rounding issue at Update_Fund_Utils
    --Commented in HUMOZF
    --G_ENTERED_AMOUNT := l_funds_util_flt.total_amount;

    IF ozf_debug_high_on
    THEN
      ozf_utility_pvt.debug_message(l_full_name || ': start');
    END IF;

    IF fnd_api.to_boolean(p_init_msg_list)
    THEN
      fnd_msg_pub.initialize;
    END IF;

    IF NOT fnd_api.compatible_api_call(l_api_version
                                      ,p_api_version
                                      ,l_api_name
                                      ,g_pkg_name)
    THEN
      RAISE fnd_api.g_exc_unexpected_error;
    END IF;

    --Get claim details of claim for which association is taking place.
    OPEN csr_claim_status(l_funds_util_flt.claim_line_id);
    FETCH csr_claim_status
      INTO l_claim_status
          ,l_source_object_class
          ,l_batch_type
          ,l_currency_rec.claim_currency_code
          ,l_claim_date
          ,l_claim_exc_type
          ,l_claim_exc_date
          ,l_claim_exc_rate
          ,l_source_object_id
          ,l_request_header_id
          ,l_batch_line_id
          ,l_created_from
          ,l_org_id;
    CLOSE csr_claim_status;

    --Get FUNCTIONAL currency from system parameters
    OPEN csr_function_currency(l_org_id);
    FETCH csr_function_currency
      INTO l_currency_rec.functional_currency_code;
    CLOSE csr_function_currency;

    --Set UNIVERSAL currency from profile.
    l_currency_rec.universal_currency_code := fnd_profile.value('OZF_TP_COMMON_CURRENCY');
    IF ozf_debug_high_on
    THEN
      ozf_utility_pvt.debug_message('l_claim_status = ' || l_claim_status);
      ozf_utility_pvt.debug_message('l_claim_date = ' || l_claim_date);
      ozf_utility_pvt.debug_message('l_claim_exc_type = ' ||
                                    l_claim_exc_type);
      ozf_utility_pvt.debug_message('l_claim_exc_date = ' ||
                                    l_claim_exc_date);
      ozf_utility_pvt.debug_message('l_claim_exc_rate = ' ||
                                    l_claim_exc_rate);
      ozf_utility_pvt.debug_message('l_source_object_id = ' ||
                                    l_source_object_id);
      ozf_utility_pvt.debug_message('l_source_object_class = ' ||
                                    l_source_object_class);
      ozf_utility_pvt.debug_message('l_request_header_id = ' ||
                                    l_request_header_id);
      ozf_utility_pvt.debug_message('l_batch_line_id = ' ||
                                    l_batch_line_id);
      ozf_utility_pvt.debug_message('l_batch_type = ' || l_batch_type);
      ozf_utility_pvt.debug_message('l_created_from = ' || l_created_from);
      ozf_utility_pvt.debug_message('p_summary_view = ' || p_summary_view);
      ozf_utility_pvt.debug_message('l_org_id = ' || l_org_id);
      ozf_utility_pvt.debug_message('Passed Association Amount = ' ||
                                    l_funds_util_flt.total_amount);
      ozf_utility_pvt.debug_message('Existing Association Amount = ' ||
                                    l_funds_util_flt.old_total_amount);
    END IF;

    --Raise error, if claim is not OPEN status and associate earnings is happening.
    IF l_claim_status <> 'OPEN'
    THEN
      IF fnd_msg_pub.check_msg_level(fnd_msg_pub.g_msg_lvl_error)
      THEN
        fnd_message.set_name('OZF', 'OZF_CLAIM_ASSO_NOT_OPEN');
        fnd_msg_pub.add;
      END IF;
      RAISE fnd_api.g_exc_error;
    END IF;

    ----------------- copy line info to filter ---------------
    IF l_funds_util_flt.claim_line_id IS NOT NULL
    THEN
      copy_util_flt(px_funds_util_flt => l_funds_util_flt);
    END IF;

    --Association processing depends on prorate condition. Get the details of prorate check
    --at system parameter level and claim lines level.

    printlog('Before Get_Prorate_Earnings_Flag');

    get_prorate_earnings_flag(p_funds_util_flt        => l_funds_util_flt
                             ,x_prorate_earnings_flag => l_prorate_earnings_flag);

    printlog('End Get_Prorate_Earnings_Flag');

    --For SCAN DATA offers prorate is not required. Also, if claim line is not associated with any
    --earnings then we need not require to do earnings on prorate basis.
    IF l_funds_util_flt.offer_type = 'SCAN_DATA' OR
       nvl(l_prorate_earnings_flag, 'F') = 'F' OR
       nvl(l_funds_util_flt.old_total_amount, 0) = 0
    THEN
      l_prorate_req_flag := FALSE;
    ELSE
      l_prorate_req_flag := TRUE;
    END IF;

    IF ozf_debug_high_on
    THEN
      ozf_utility_pvt.debug_message('l_prorate_earnings_flag = ' ||
                                    l_prorate_earnings_flag);
      ozf_utility_pvt.debug_message('l_funds_util_flt.offer_type = ' ||
                                    l_funds_util_flt.offer_type);
      ozf_utility_pvt.debug_message('l_funds_util_flt.old_total_amount = ' ||
                                    l_funds_util_flt.old_total_amount);
    END IF;

    --Transactional currency (OFFER or ORDER currency) supposed to be passed to this program unit.
    IF l_funds_util_flt.utiz_currency_code IS NOT NULL
    THEN
      l_currency_rec.transaction_currency_code := l_funds_util_flt.utiz_currency_code;
    END IF;
    IF ozf_debug_high_on
    THEN
      ozf_utility_pvt.debug_message('l_funds_util_flt.utiz_currency_code  = ' ||
                                    l_funds_util_flt.utiz_currency_code);
      ozf_utility_pvt.debug_message('l_currency_rec.transaction_currency_code = ' ||
                                    l_currency_rec.transaction_currency_code);
    END IF;

    IF ozf_debug_high_on
    THEN
      ozf_utility_pvt.debug_message('l_funds_util_flt.total_units = ' ||
                                    l_funds_util_flt.total_units);
      ozf_utility_pvt.debug_message('l_funds_util_flt.quantity  = ' ||
                                    l_funds_util_flt.quantity);
      ozf_utility_pvt.debug_message('l_funds_util_flt.total_amount = ' ||
                                    l_funds_util_flt.total_amount);
      ozf_utility_pvt.debug_message('l_currency_rec.transaction_currency_code = ' ||
                                    l_currency_rec.transaction_currency_code);
      ozf_utility_pvt.debug_message('l_currency_rec.claim_currency_code =  ' ||
                                    l_currency_rec.claim_currency_code);
      ozf_utility_pvt.debug_message('l_currency_rec.functional_currency_code = ' ||
                                    l_currency_rec.functional_currency_code);
    END IF;

    --Association can be done in either TRANSACTIONAL currency or FUNCTIONAL currency. We will set GLOBAL variable
    --G_ASSO_CURRENCY accordingly, so that entire association will be in single known currency and we need NOT to
    --convert them on case by case basis.

    IF l_currency_rec.claim_currency_code =
       l_currency_rec.transaction_currency_code
    THEN
      printlog('When Currency are equal');
      l_currency_rec.association_currency_code := l_currency_rec.transaction_currency_code;
      l_asso_amount                            := l_funds_util_flt.total_amount;
      --l_exists_asso_amount                     := l_funds_util_flt.old_total_amount;--- NS
      l_net_asso_amount := nvl(abs(l_asso_amount), 0) -
                           nvl(abs(l_exists_asso_amount), 0);
    ELSE
      IF l_funds_util_flt.total_amount IS NOT NULL AND
         l_funds_util_flt.total_amount <> 0
      THEN
        l_currency_rec.association_currency_code := l_currency_rec.functional_currency_code;

        IF l_currency_rec.claim_currency_code <>
           l_currency_rec.functional_currency_code
        THEN
          ozf_utility_pvt.convert_currency(p_from_currency => l_currency_rec.claim_currency_code
                                          ,p_to_currency   => l_currency_rec.functional_currency_code
                                          ,p_conv_type     => l_claim_exc_type
                                          ,p_conv_rate     => l_claim_exc_rate
                                          ,p_conv_date     => l_claim_exc_date
                                          ,p_from_amount   => l_funds_util_flt.total_amount
                                          ,x_return_status => l_return_status
                                          ,x_to_amount     => l_asso_amount
                                          ,x_rate          => l_convert_exchange_rate);

          IF l_return_status = fnd_api.g_ret_sts_error
          THEN
            RAISE fnd_api.g_exc_error;
          ELSIF l_return_status = fnd_api.g_ret_sts_unexp_error
          THEN
            RAISE fnd_api.g_exc_error;
          END IF;
          IF l_asso_amount IS NOT NULL
          THEN
            l_asso_amount := ozf_utility_pvt.currround(l_asso_amount
                                                      ,l_currency_rec.association_currency_code);
          END IF;
        ELSE
          l_asso_amount := l_funds_util_flt.total_amount;
        END IF;
      ELSE
        l_asso_amount := 0;
      END IF;

      IF l_funds_util_flt.old_total_amount IS NOT NULL AND
         l_funds_util_flt.old_total_amount <> 0
      THEN
        IF l_currency_rec.claim_currency_code <>
           l_currency_rec.association_currency_code
        THEN
          ozf_utility_pvt.convert_currency(p_from_currency => l_currency_rec.claim_currency_code
                                          ,p_to_currency   => l_currency_rec.functional_currency_code
                                          ,p_conv_type     => l_claim_exc_type
                                          ,p_conv_rate     => l_claim_exc_rate
                                          ,p_conv_date     => l_claim_exc_date
                                          ,p_from_amount   => l_funds_util_flt.old_total_amount
                                          ,x_return_status => l_return_status
                                          ,x_to_amount     => l_exists_asso_amount
                                          ,x_rate          => l_convert_exchange_rate);

          IF l_return_status = fnd_api.g_ret_sts_error
          THEN
            RAISE fnd_api.g_exc_error;
          ELSIF l_return_status = fnd_api.g_ret_sts_unexp_error
          THEN
            RAISE fnd_api.g_exc_error;
          END IF;
          IF l_exists_asso_amount IS NOT NULL
          THEN
            l_exists_asso_amount := ozf_utility_pvt.currround(l_exists_asso_amount
                                                             ,l_currency_rec.association_currency_code);
          END IF;
        ELSE
          l_exists_asso_amount := l_funds_util_flt.old_total_amount;
        END IF;
      ELSE
        l_exists_asso_amount := 0;
      END IF;

      l_net_asso_amount := nvl(abs(l_asso_amount), 0) -
                           nvl(abs(l_exists_asso_amount), 0);
    END IF;

    IF l_net_asso_amount IS NOT NULL
    THEN
      l_net_asso_amount := ozf_utility_pvt.currround(l_net_asso_amount
                                                    ,l_currency_rec.association_currency_code);
    END IF;

    IF ozf_debug_high_on
    THEN
      ozf_utility_pvt.debug_message('l_currency_rec.association_currency_code = ' ||
                                    l_currency_rec.association_currency_code);
      ozf_utility_pvt.debug_message('l_asso_amount  = ' || l_asso_amount);
      ozf_utility_pvt.debug_message('l_exists_asso_amount = ' ||
                                    l_exists_asso_amount);
      ozf_utility_pvt.debug_message('l_net_asso_amount = ' ||
                                    l_net_asso_amount);
    END IF;

    /* Prorate Condition:
    If prorate flag is set, then identify all the qualified utilizations for current association
    and reduce amount remaining with current association amount. Remove existing associations with
    current claim line and calculate fresh association earnings for claim line..
    Otherwise, if prorate flag is not set, and association amount is smaller than already associated
    amount with current claim line then reduce amount remaining of qualified utilizations as well as
    already associated amounts. Re-calculate FXGL for reduced associated amounts. No fresh association
    should be done.
    If prorate is not set and association amount is larger than already associated amount, then do association
    with supplied association amount.
    */

    IF l_prorate_req_flag
    THEN
      printlog('Call to Delete_All_Line_Util');

      delete_all_line_util(p_api_version      => l_api_version
                          ,p_init_msg_list    => fnd_api.g_false
                          ,p_commit           => fnd_api.g_false
                          ,p_validation_level => fnd_api.g_valid_level_full
                          ,x_return_status    => l_return_status
                          ,x_msg_data         => x_msg_data
                          ,x_msg_count        => x_msg_count
                          ,p_currency_rec     => l_currency_rec
                          ,p_funds_util_flt   => l_funds_util_flt);

      printlog('End Call to Delete_All_Line_Util');

      IF l_return_status = fnd_api.g_ret_sts_error
      THEN
        RAISE fnd_api.g_exc_error;
      ELSIF l_return_status = fnd_api.g_ret_sts_unexp_error
      THEN
        RAISE fnd_api.g_exc_error;
      END IF;
      l_cre_util_amount := nvl(l_asso_amount, 0); --sum(remaining_amount)
    ELSE
      IF l_net_asso_amount < 0
      THEN

        printlog('Call to Delete_All_Group_Util');

        delete_group_line_util(p_api_version      => l_api_version
                              ,p_init_msg_list    => fnd_api.g_false
                              ,p_commit           => fnd_api.g_false
                              ,p_validation_level => fnd_api.g_valid_level_full
                              ,x_return_status    => l_return_status
                              ,x_msg_data         => x_msg_data
                              ,x_msg_count        => x_msg_count
                              ,p_funds_util_flt   => l_funds_util_flt);

        printlog('End Call to Delete_All_Group_Util');

        IF l_return_status = fnd_api.g_ret_sts_error
        THEN
          RAISE fnd_api.g_exc_error;
        ELSIF l_return_status = fnd_api.g_ret_sts_unexp_error
        THEN
          RAISE fnd_api.g_exc_error;
        END IF;
        l_create_util := FALSE;
      ELSE
        l_cre_util_amount := nvl(l_asso_amount, 0) -
                             nvl(l_exists_asso_amount, 0);
        l_cre_util_amount := ozf_utility_pvt.currround(l_cre_util_amount
                                                      ,l_currency_rec.association_currency_code);
      END IF;
    END IF;

    IF ozf_debug_high_on
    THEN
      ozf_utility_pvt.debug_message('l_cre_util_amount  = ' ||
                                    l_cre_util_amount);
      IF l_create_util
      THEN
        ozf_utility_pvt.debug_message('l_create_util = TRUE');
      ELSE
        ozf_utility_pvt.debug_message('l_create_util = FALSE');
      END IF;
    END IF;

    /* Associate Earnings:
    Identify all the utilizations qualifies for current association based on customer, offer
    and product combinations. If prorate is checked, then calculate association amount based
    on prorate basis.
    */

    IF l_create_util
    THEN

      printlog('PROCEDURE : Populate_Line_Util_Tbl');

      populate_line_util_tbl(p_funds_util_flt        => l_funds_util_flt
                            ,p_source_object_class   => l_source_object_class
                            ,p_source_object_id      => l_source_object_id
                            ,p_request_header_id     => l_request_header_id
                            ,p_batch_line_id         => l_batch_line_id
                            ,p_batch_type            => l_batch_type
                            ,p_summary_view          => p_summary_view
                            ,p_cre_util_amount       => l_cre_util_amount
                            ,p_prorate_earnings_flag => l_prorate_earnings_flag
                            ,p_pmt_frequency         => p_pmt_frequency
                            ,p_pmt_method            => p_pmt_method
                            ,p_invoice_type          => p_invoice_type
                            ,p_currency_rec          => l_currency_rec
                            ,x_funds_rem_tbl         => l_funds_rem_tbl
                            ,x_tot_accrual_amt       => l_tot_accrual_amt
                            ,x_line_amount           => l_new_line_amount
                            ,x_line_util_tbl         => l_line_util_tbl
                            ,x_msg_data              => x_msg_data
                            ,x_msg_count             => x_msg_count
                            ,x_return_status         => l_return_status);

      printlog('PROCEDURE : Populate_Line_Util_Tbl');

      IF l_return_status = fnd_api.g_ret_sts_error
      THEN
        RAISE fnd_api.g_exc_error;
      ELSIF l_return_status = fnd_api.g_ret_sts_unexp_error
      THEN
        RAISE fnd_api.g_exc_error;
      END IF;

      --For Supplier Ship Debit claims, claim line amount will always be total association
      --amount and never fall into pay over earnings category. Hence, skip pay over earnings
      --these type of claims.

      IF nvl(l_source_object_class, 'X') = 'SD_SUPPLIER'
      THEN
        GOTO create_line_util;
      END IF;

      l_claim_amt := l_asso_amount - nvl(l_exists_asso_amount, 0);

      /* Validate Over Utilization:
      Pay over earnings will take place only for Normal claims created from Trade Management
      UI. Since, utilizations amount remaining in BUDGET currency, while association amount
      is in CLAIM currency. Hence, need to validate over utilizations w.r.t. association amount
      in CLAIM currency.
      */

      /*
      IF ((NVL(l_created_from, 'NONE') <> 'AUTOPAY') AND
         (NVL(l_batch_type, 'NONE') <> 'BATCH' AND
         NVL(l_source_object_class, 'NONE') NOT IN
         ('BATCH', 'SPECIAL_PRICE') --//Bug fix : 9751679
         AND l_funds_util_flt.offer_type IS NOT NULL)) THEN
         Get_Pay_Over_Amount(p_util            => l_funds_rem_tbl,
                             p_claim_amt       => l_cre_util_amount,
                             p_claim_exc_rate  => l_claim_exc_rate,
                             p_claim_exc_date  => l_claim_exc_date,
                             p_claim_exc_type  => l_claim_exc_type,
                             p_currency_rec    => l_currency_rec,
                             x_pay_over_flag   => l_pay_over_flag,
                             x_pay_over_amount => l_pay_over_amount,
                             x_return_status   => l_return_status,
                             x_msg_data        => x_msg_data,
                             x_msg_count       => x_msg_count);
      END IF;
      */

      IF ozf_debug_high_on
      THEN
        IF l_pay_over_flag
        THEN
          ozf_utility_pvt.debug_message('l_pay_over_flag = TRUE');
        ELSE
          ozf_utility_pvt.debug_message('l_pay_over_flag = FALSE');
        END IF;
        ozf_utility_pvt.debug_message('l_claim_amt = ' || l_claim_amt);
        ozf_utility_pvt.debug_message('l_cre_util_amount = ' ||
                                      l_cre_util_amount);
        ozf_utility_pvt.debug_message('l_pay_over_amount = ' ||
                                      l_pay_over_amount);
        ozf_utility_pvt.debug_message('l_tot_accrual_amt = ' ||
                                      nvl(l_tot_accrual_amt, 0));
        ozf_utility_pvt.debug_message('l_new_line_amount = ' ||
                                      nvl(l_new_line_amount, 0));
        ozf_utility_pvt.debug_message('l_funds_rem_tbl.count = ' ||
                                      l_funds_rem_tbl.count);
      END IF;

      /*
      IF l_pay_over_flag THEN
         Validate_Over_Utilization(p_api_version      => l_api_version,
                                   p_init_msg_list    => FND_API.g_false,
                                   p_validation_level => p_validation_level,
                                   x_return_status    => l_return_status,
                                   x_msg_count        => x_msg_count,
                                   x_msg_data         => x_msg_data,
                                   p_currency_rec     => l_currency_rec,
                                   p_funds_util_flt   => l_funds_util_flt,
                                   p_over_paid_amount => l_pay_over_amount --nepanda : fix for bug # 9508390  - issue # 3
                                   );
         IF l_return_status = fnd_api.g_ret_sts_error THEN
            RAISE FND_API.g_exc_error;
         ELSIF l_return_status = fnd_api.g_ret_sts_unexp_error THEN
            RAISE FND_API.g_exc_unexpected_error;
         END IF;

      END IF;

      --If pay over earnings is allowed, then create adjustments for over paid earnings w.r.t.
      --association amount.
      -- Changed the code to create the adjustment for multiple and single product lines -- kpatro
      IF (NVL(l_new_line_amount, 0) <> 0 AND l_pay_over_amount > 0) THEN
         Create_Pay_Over_Adjustments(px_line_util_tbl        => l_line_util_tbl,
                                     p_funds_util_flt        => l_funds_util_flt,
                                     p_tot_accrual_amt       => NVL(l_tot_accrual_amt,
                                                                    0),
                                     p_pay_over_amount       => l_pay_over_amount,
                                     p_prorate_earnings_flag => l_prorate_earnings_flag,
                                     p_new_total_amount      => l_new_line_amount,
                                     p_currency_rec          => l_currency_rec,
                                     x_return_status         => l_return_status,
                                     x_msg_data              => x_msg_data,
                                     x_msg_count             => x_msg_count

                                     );
         IF l_return_status = FND_API.g_ret_sts_error THEN
            RAISE FND_API.g_exc_error;
         ELSIF l_return_status = FND_API.g_ret_sts_unexp_error THEN
            RAISE FND_API.g_exc_error;
         END IF;
      END IF;
      */
    END IF; -- l_create_util TRUE

    <<create_line_util>>

    --Do associate earnings for qualified utilizations with current claim line. Calculate
    --FXGL. Reduce amount remaining of utilizations w.r.t. corresponding association amounts.

    IF l_line_util_tbl.count > 0
    THEN
      -- modified call for HUMZOF

      create_line_util_tbl(p_api_version      => l_api_version
                          ,p_init_msg_list    => fnd_api.g_false
                          ,p_commit           => fnd_api.g_false
                          ,p_validation_level => p_validation_level
                          ,x_return_status    => l_return_status
                          ,x_msg_data         => x_msg_data
                          ,x_msg_count        => x_msg_count
                          ,p_currency_rec     => l_currency_rec
                          ,p_line_util_tbl    => l_line_util_tbl
                          ,x_error_index      => l_error_index);

      IF l_return_status = fnd_api.g_ret_sts_error
      THEN
        RAISE fnd_api.g_exc_error;
      ELSIF l_return_status = fnd_api.g_ret_sts_unexp_error
      THEN
        RAISE fnd_api.g_exc_unexpected_error;
      END IF;
    END IF;

    ------------------------- finish -------------------------------
    -- Check for commit
    IF fnd_api.to_boolean(p_commit)
    THEN
      COMMIT;
    END IF;

    fnd_msg_pub.count_and_get(p_encoded => fnd_api.g_false
                             ,p_count   => x_msg_count
                             ,p_data    => x_msg_data);

    IF ozf_debug_high_on
    THEN
      ozf_utility_pvt.debug_message(l_full_name || ': end');
    END IF;

  EXCEPTION
    WHEN fnd_api.g_exc_error THEN
      ROLLBACK TO update_group_line_util;
      x_return_status := fnd_api.g_ret_sts_error;
      fnd_msg_pub.count_and_get(p_encoded => fnd_api.g_false
                               ,p_count   => x_msg_count
                               ,p_data    => x_msg_data);

    WHEN fnd_api.g_exc_unexpected_error THEN
      ROLLBACK TO update_group_line_util;
      x_return_status := fnd_api.g_ret_sts_unexp_error;
      fnd_msg_pub.count_and_get(p_encoded => fnd_api.g_false
                               ,p_count   => x_msg_count
                               ,p_data    => x_msg_data);

    WHEN OTHERS THEN
      ROLLBACK TO update_group_line_util;
      x_return_status := fnd_api.g_ret_sts_unexp_error;
      IF fnd_msg_pub.check_msg_level(fnd_msg_pub.g_msg_lvl_unexp_error)
      THEN
        fnd_msg_pub.add_exc_msg(g_pkg_name, l_api_name);
      END IF;
      fnd_msg_pub.count_and_get(p_encoded => fnd_api.g_false
                               ,p_count   => x_msg_count
                               ,p_data    => x_msg_data);

  END update_group_line_util;

  ---------------------------------------------------------------------
  -- PROCEDURE
  --    Create_Claim_For_Accruals
  --
  -- PURPOSE
  --    Create a claim and associate earnings based on search filters.
  --
  -- PARAMETERS
  --    p_claim_rec: claim record
  --    p_funds_util_flt: search filter for earnings
  --
  -- NOTES
  -- -----------------------------------------------------------------------------
  --   VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- -------   -----------   --------------- ---------------------------------
  --    1.0                                   Created
  --    1.1    12-Jul-2013   Kathy Poling     SR 208563 - Issue #308 TM Claims are
  --                                          being duplicated.
  -- -----------------------------------------------------------------------------
  PROCEDURE create_claim_for_accruals(p_api_version      IN NUMBER
                                     ,p_org_id           IN NUMBER
                                     ,p_init_msg_list    IN VARCHAR2 := fnd_api.g_false
                                     ,p_commit           IN VARCHAR2 := fnd_api.g_false
                                     ,p_validation_level IN NUMBER := fnd_api.g_valid_level_full
                                     ,

                                      x_return_status OUT NOCOPY VARCHAR2
                                     ,x_msg_count     OUT NOCOPY NUMBER
                                     ,x_msg_data      OUT NOCOPY VARCHAR2
                                     ,

                                      p_pmt_frequency  IN VARCHAR2
                                     ,p_pmt_method     IN VARCHAR2
                                     ,p_invoice_type   IN VARCHAR2
                                     ,p_claim_rec      IN ozf_claim_pvt.claim_rec_type
                                     ,p_funds_util_flt IN funds_util_flt_type
                                     ,

                                      x_claim_id OUT NOCOPY NUMBER) IS

    l_api_version CONSTANT NUMBER := 1.0;
    l_api_name    CONSTANT VARCHAR2(30) := 'Create_Claim_For_Accruals';
    l_full_name   CONSTANT VARCHAR2(60) := g_pkg_name || '.' || l_api_name;
    l_return_status VARCHAR2(1);

    l_funds_util_flt         funds_util_flt_type;
    l_line_tbl               ozf_claim_line_pvt.claim_line_tbl_type;
    l_claim_rec              ozf_claim_pvt.claim_rec_type := p_claim_rec;
    l_claim_id               NUMBER;
    l_cust_account_id        NUMBER;
    l_plan_type              VARCHAR2(30);
    l_plan_id                NUMBER;
    l_product_level_type     VARCHAR2(30);
    l_product_id             NUMBER;
    l_rx_plan_id             VARCHAR2(30); -- HUMOZF add
    l_fund_id                NUMBER;
    l_amount                 NUMBER;
    l_total_acctd_amount_rem NUMBER;
    l_performance_flag       VARCHAR2(1) := fnd_api.g_true;

    l_emp_csr         NUMBER;
    l_stmt            VARCHAR2(3000);
    l_ignore          NUMBER;
    l_counter         PLS_INTEGER := 1;
    l_error_index     NUMBER;
    l_ignore_text     VARCHAR2(240);
    l_dummy           NUMBER;
    l_offer_perf_tbl  ozf_claim_accrual_pvt.offer_performance_tbl_type;
    l_currency_code   VARCHAR2(15);
    l_amount_ut_curr  NUMBER; -- amount in utilization currency (source budget currency)
    l_bill_to_site_id NUMBER;

    l_currency_rec currency_rec_type;
    l_currorgid    NUMBER := mo_global.get_current_org_id();

    l_quantity    NUMBER;
    l_unit_rebate NUMBER;

    l_period_type  VARCHAR2(30);
    l_period_name  VARCHAR2(30);
    l_period_id    NUMBER;
    l_period_start DATE;
    l_period_end   DATE;

    TYPE claim_line_csr1 IS REF CURSOR;
    clm_line_csr claim_line_csr1;

    --Change the amount for multi currency - kpatro - 5/22/2009
    CURSOR csr_claim_line(cv_claim_id IN NUMBER) IS
      SELECT claim_line_id
            ,activity_type
            ,activity_id
            ,claim_currency_amount

        FROM apps.ozf_claim_lines
       WHERE claim_id = cv_claim_id;

    CURSOR csr_uom_code(cv_item_id IN NUMBER) IS
      SELECT primary_uom_code
        FROM apps.mtl_system_items
       WHERE inventory_item_id = cv_item_id
         AND organization_id = fnd_profile.value('QP_ORGANIZATION_ID');

    CURSOR csr_offer_perf(cv_list_header_id IN NUMBER) IS
      SELECT 1
        FROM apps.ozf_offer_performances
       WHERE list_header_id = cv_list_header_id;

    -- period names
    CURSOR c_get_period_names(v_inv_frequency IN VARCHAR2
                             ,v_end_date      IN DATE) IS
      SELECT period_type, period_name, period_id, start_date, end_date
        FROM (SELECT 'MONTH' period_type
                    ,otep.name period_name
                    ,otep.ent_period_id period_id
                    ,otep.start_date start_date
                    ,otep.end_date end_date
                FROM apps.ozf_time_ent_period otep
              UNION ALL
              SELECT DISTINCT 'QTR' period_type
                             ,oteq.name period_name
                             ,oteq.ent_qtr_id period_id
                             ,oteq.start_date start_date
                             ,oteq.end_date end_date
                FROM apps.ozf_time_ent_qtr oteq
              UNION ALL
              SELECT 'YEAR' period_type
                    ,oteq.name period_name
                    ,oteq.ent_year_id period_id
                    ,oteq.start_date start_date
                    ,oteq.end_date end_date
                FROM apps.ozf_time_ent_year oteq) etp
       WHERE etp.period_type = v_inv_frequency --'MONTH'
         AND v_end_date BETWEEN etp.start_date AND etp.end_date;

    --Added for bug 7030415
    CURSOR c_get_conversion_type IS
      SELECT exchange_rate_type
        FROM apps.ozf_sys_parameters_all
       WHERE org_id = l_currorgid;

    CURSOR csr_function_currency IS
      SELECT gs.currency_code
        FROM apps.gl_sets_of_books gs, apps.ozf_sys_parameters org
       WHERE org.set_of_books_id = gs.set_of_books_id
         AND org.org_id = l_currorgid;

    ---Get the Max Expense----
    /*CURSOR max_expense_csr(v_plan_id IN NUMBER,
                         v_cust_account_id IN NUMBER) IS
    select a.expense,a.end_cust_party_name,a.end_cust_party_id
        from(select sum(orl.quantity * orl.selling_price) expense,
                        orl.end_cust_party_name,
                        orl.end_cust_party_id

        from   ozf_resale_lines_all orl,
              ozf_funds_utilized_all_b ofu
        where orl.resale_line_id = ofu.object_id
        AND ofu.cust_account_id = orl.sold_from_cust_account_id
        AND ofu.plan_id = nvl(v_plan_id,ofu.plan_id) --'20478'
        AND ofu.cust_account_id = nvl(v_cust_account_id,ofu.cust_account_id) --'2006'
        GROUP BY orl.end_cust_party_name,
                 orl.end_cust_party_id
                 ) a
    WHERE  a.expense in (select max(expense)
              from (select sum(orl.quantity * orl.selling_price) expense,
                           orl.end_cust_party_id
                    from ozf_resale_lines_all orl,
                         ozf_funds_utilized_all_b ofu
                    where orl.resale_line_id = ofu.object_id
                    AND ofu.cust_account_id = orl.sold_from_cust_account_id
                    AND ofu.plan_id = nvl(v_plan_id,ofu.plan_id) --'20478's
                    AND ofu.cust_account_id = nvl(v_cust_account_id,ofu.cust_account_id) --'2006'
                    AND ofu.cust_account_id = orl.sold_from_cust_account_id
                    GROUP BY orl.end_cust_party_id));

     CURSOR csr_get_site_use_id(cv_cust_account_id IN NUMBER,
                                cv_lob_id          IN NUMBER) IS
     SELECT sold_from_site_id
     FROM  ozf_resale_lines_all orl
     WHERE orl.end_cust_party_id = cv_lob_id
     AND   orl.sold_from_cust_account_id = nvl(cv_cust_account_id,orl.sold_from_cust_account_id)
     AND rownum = 1;

     CURSOR csr_get_payto_siteid(cv_soldfrom_siteid IN NUMBER) IS
     SELECT hcas.cust_acct_site_id
     FROM hz_cust_acct_sites_all hcas
     WHERE hcas.party_site_id = cv_soldfrom_siteid;*/

    l_party_id           NUMBER;
    l_expense            NUMBER;
    l_party_name         VARCHAR2(50);
    l_party_site_id      NUMBER;
    l_billto_site_use_id NUMBER;
    ----Get the Offer Type---
    CURSOR get_offr_type_csr(v_plan_id IN NUMBER) IS
      SELECT --amv.media_name
       amv.description
        FROM apps.ams_media_vl amv, ozf_offers oof
       WHERE amv.media_id = oof.activity_media_id
         AND amv.media_type_code = 'DEAL'
         AND oof.qp_list_header_id = v_plan_id;

    ---Get the Offer Name----
    CURSOR get_offr_name(v_plan_id IN NUMBER) IS
      SELECT qlh.description
        FROM qp_list_headers_vl qlh
       WHERE list_header_id = v_plan_id;

    -- Get the Legal Entity ID
    CURSOR get_legal_entity(p_org_id IN NUMBER) IS
      SELECT default_legal_context_id
        FROM hr_operating_units
       WHERE organization_id = p_org_id;

    CURSOR claim_line_csr(v_fund_id         IN NUMBER
                         ,v_plan_id         IN NUMBER
                         ,v_cust_account_id IN NUMBER
                         ,v_pmt_frequency   IN VARCHAR2
                         ,v_pmt_method      IN VARCHAR2) IS
--Version 1.1  7/12/13
/*      SELECT SUM(fu.amount_remaining) amount
            ,fu.currency_code
            ,orl.sold_from_cust_account_id
            ,fu.plan_id
            ,qpb.attribute5

        FROM apps.ozf_funds_utilized_all_b fu
            ,apps.qp_list_headers_b        qpb
            ,apps.qp_qualifiers            qpq
            ,apps.ozf_offers               oof
            ,apps.ozf_resale_lines_all     orl
       WHERE fu.plan_type = 'OFFR'
         AND fu.plan_id = qpb.list_header_id
         AND fu.object_id = orl.resale_line_id
         AND fu.cust_account_id = orl.sold_from_cust_account_id
         AND qpb.list_header_id = qpq.list_header_id
         AND qpb.attribute5 = nvl(v_pmt_method, qpb.attribute5)
         AND qpb.attribute6 = nvl(v_pmt_frequency, qpb.attribute6)
         AND oof.qp_list_header_id = qpb.list_header_id
         AND fu.fund_id = nvl(v_fund_id, fu.fund_id)
         AND fu.plan_curr_amount_remaining <> 0
         AND fu.utilization_type IN ('ACCRUAL', 'ADJUSTMENT')
         AND fu.cust_account_id =
             nvl(v_cust_account_id, fu.cust_account_id)
         AND nvl(oof.autopay_flag, 'Y') = 'Y'
         AND fu.plan_id = nvl(v_plan_id, fu.plan_id)
            -- AND oof.autopay_flag = 'Y'
         AND fu.gl_posted_flag = 'Y'
         AND qualifier_attribute = 'QUALIFIER_ATTRIBUTE2'
         AND qualifier_context = 'SOLD_BY'
       GROUP BY fu.currency_code
               ,orl.sold_from_cust_account_id
               ,fu.plan_id
               ,qpb.attribute5;
*/
SELECT SUM(fu.amount_remaining) amount
                    ,fu.currency_code
                    ,orl.sold_from_cust_account_id
                    ,fu.plan_id
                    ,qpb.attribute5
                FROM apps.ozf_funds_utilized_all_b fu
                    ,apps.qp_list_headers_b qpb
                    ,apps.ozf_offers oof
                    ,(SELECT DISTINCT attribute5
                                     ,attribute6
                                     ,list_header_id
                        FROM apps.qp_qualifiers
                       WHERE qualifier_attribute = 'QUALIFIER_ATTRIBUTE2'
                         AND qualifier_context = 'SOLD_BY') qpq
                    ,apps.ozf_resale_lines_all orl
              WHERE fu.plan_type = 'OFFR'
         AND fu.plan_id = qpb.list_header_id
         AND fu.object_id = orl.resale_line_id
         AND fu.cust_account_id = orl.sold_from_cust_account_id
         AND qpb.list_header_id = qpq.list_header_id
         AND qpb.attribute5 = nvl(v_pmt_method, qpb.attribute5)
         AND qpb.attribute6 = nvl(v_pmt_frequency, qpb.attribute6)
         AND oof.qp_list_header_id = qpb.list_header_id
         AND fu.fund_id = nvl(v_fund_id, fu.fund_id)
         AND fu.plan_curr_amount_remaining <> 0
         AND fu.utilization_type IN ('ACCRUAL', 'ADJUSTMENT')
         AND fu.cust_account_id =
             nvl(v_cust_account_id, fu.cust_account_id)
         AND nvl(oof.autopay_flag, 'Y') = 'Y'
         AND fu.plan_id =v_plan_id --Ver 3.0
         AND fu.gl_posted_flag = 'Y'
       GROUP BY fu.currency_code
               ,orl.sold_from_cust_account_id
               ,fu.plan_id
               ,qpb.attribute5;
--7/12/2013

    l_exchange_rate_type VARCHAR2(30) := fnd_api.g_miss_char;
    l_rate               NUMBER;
    l_offer_type         VARCHAR2(50);
    l_offr_name          VARCHAR2(240);
    l_pmt_frequency      VARCHAR2(50);
    l_pmt_method         VARCHAR2(50);
    l_legal_entity_id    NUMBER;
  BEGIN
    --------------------- initialize -----------------------

    fnd_file.put_line(fnd_file.log
                     ,'Procedure : Create_Claim_For_Accruals');

    SAVEPOINT create_claim_for_accruals;

    IF ozf_debug_high_on
    THEN
      ozf_utility_pvt.debug_message(l_full_name || ': start');
      ozf_utility_pvt.debug_message('l_claim_rec.claim_number :  ' ||
                                    l_claim_rec.claim_number);
    END IF;

    IF fnd_api.to_boolean(p_init_msg_list)
    THEN
      fnd_msg_pub.initialize;
    END IF;

    IF NOT fnd_api.compatible_api_call(l_api_version
                                      ,p_api_version
                                      ,l_api_name
                                      ,g_pkg_name)
    THEN
      RAISE fnd_api.g_exc_unexpected_error;
    END IF;

    x_return_status := fnd_api.g_ret_sts_success;

    -------------------- start -------------------
    l_funds_util_flt         := p_funds_util_flt;
    l_total_acctd_amount_rem := 0;

    IF ozf_debug_high_on
    THEN
      ozf_utility_pvt.debug_message('Claim Currency Code in Create Claim For Accruals :' ||
                                    l_claim_rec.currency_code);
    END IF;

    -- Added For Multi Currency - kpatro
    IF (l_claim_rec.currency_code IS NOT NULL)
    THEN
      l_currency_rec.claim_currency_code := l_claim_rec.currency_code;
    END IF;

    fnd_file.put_line(fnd_file.log
                     ,'l_currency_rec.claim_currency_code' ||
                      l_currency_rec.claim_currency_code);
    fnd_file.put_line(fnd_file.log
                     ,'l_claim_rec.currency_code' ||
                      l_claim_rec.currency_code);

    l_currency_rec.transaction_currency_code := l_funds_util_flt.utiz_currency_code;

    fnd_file.put_line(fnd_file.log
                     ,'l_currency_rec.transaction_currency_code' ||
                      l_currency_rec.transaction_currency_code);
    fnd_file.put_line(fnd_file.log
                     ,'l_funds_util_flt.utiz_currency_code' ||
                      l_funds_util_flt.utiz_currency_code);

    OPEN csr_function_currency;
    FETCH csr_function_currency
      INTO l_currency_rec.functional_currency_code;
    CLOSE csr_function_currency;

    fnd_file.put_line(fnd_file.log
                     ,'l_currency_rec.functional_currency_code' ||
                      l_currency_rec.functional_currency_code);

    fnd_file.put_line(fnd_file.log
                     ,'l_currency_rec.claim_currency_code' ||
                      l_currency_rec.claim_currency_code);
    fnd_file.put_line(fnd_file.log
                     ,'l_currency_rec.transaction_currency_code' ||
                      l_currency_rec.transaction_currency_code);

    IF l_currency_rec.claim_currency_code =
       l_currency_rec.transaction_currency_code
    THEN

      fnd_file.put_line(fnd_file.log
                       ,'Assign association currency code to transaction currency code');

      l_currency_rec.association_currency_code := l_currency_rec.transaction_currency_code;

    ELSE

      fnd_file.put_line(fnd_file.log
                       ,',Assign association currency code to Functional currency code');

      l_currency_rec.association_currency_code := l_currency_rec.functional_currency_code;

    END IF;

    --
    fnd_file.put_line(fnd_file.log, 'Fund ID ' || l_funds_util_flt.fund_id);
    fnd_file.put_line(fnd_file.log
                     ,'Rebate ID ' || l_funds_util_flt.activity_id);
    fnd_file.put_line(fnd_file.log
                     ,'Customer ID ' || l_funds_util_flt.cust_account_id);
    fnd_file.put_line(fnd_file.log
                     ,'Payment Frequency ' || p_pmt_frequency);
    fnd_file.put_line(fnd_file.log, 'Payment Method ' || p_pmt_method);
    --FND_FILE.PUT_LINE(FND_FILE.LOG, 'End Date ' ||l_funds_util_flt.end_date);

    OPEN get_offr_type_csr(l_funds_util_flt.activity_id);
    FETCH get_offr_type_csr
      INTO l_offer_type;
    CLOSE get_offr_type_csr;

    fnd_file.put_line(fnd_file.log, 'l_offer_type ' || l_offer_type);

    OPEN get_offr_name(l_funds_util_flt.activity_id);
    FETCH get_offr_name
      INTO l_offr_name;
    CLOSE get_offr_name;

    fnd_file.put_line(fnd_file.log, 'l_offr_name ' || l_offr_name);

    IF p_invoice_type = 'INV'
    THEN

      --    clm_line_csr
      --
      fnd_file.put_line(fnd_file.log, 'Error1');
      OPEN clm_line_csr FOR
--Version 1.1 7/12/2013
/*        SELECT SUM(fu.amount_remaining) amount
              ,fu.currency_code
              ,orl.sold_from_cust_account_id
              ,fu.plan_id
              ,qpb.attribute5

          FROM apps.ozf_funds_utilized_all_b fu
              ,apps.qp_list_headers_b        qpb
              ,apps.ozf_offers               oof
              ,apps.qp_qualifiers            qpq
              ,apps.ozf_resale_lines_all     orl
         WHERE fu.plan_type = 'OFFR'
           AND fu.plan_id = qpb.list_header_id
           AND fu.object_id = orl.resale_line_id
           AND fu.cust_account_id = orl.sold_from_cust_account_id
           AND qpb.list_header_id = qpq.list_header_id
           AND oof.qp_list_header_id = qpb.list_header_id
           AND qpb.attribute5 = nvl(p_pmt_method, qpb.attribute5)
           AND qpb.attribute6 = nvl(p_pmt_frequency, qpb.attribute6)
           AND nvl(oof.autopay_flag, 'Y') = 'Y'
              -- AND fu.fund_id = nvl(v_fund_id, fu.fund_id)
           AND fu.plan_curr_amount_remaining <> 0
           AND qualifier_attribute = 'QUALIFIER_ATTRIBUTE2'
           AND qualifier_context = 'SOLD_BY'
           AND fu.utilization_type IN ('ACCRUAL', 'ADJUSTMENT')
           AND fu.cust_account_id =
               nvl(l_funds_util_flt.cust_account_id, fu.cust_account_id)
              --AND fu.org_id= p_org_id
           AND fu.plan_id = nvl(l_funds_util_flt.activity_id, fu.plan_id)
           AND fu.gl_posted_flag = 'Y'
         GROUP BY fu.currency_code
                 ,orl.sold_from_cust_account_id
                 ,fu.plan_id
                 ,qpb.attribute5;
*/
SELECT SUM(fu.amount_remaining) amount
              ,fu.currency_code
              ,orl.sold_from_cust_account_id
              ,fu.plan_id
              ,qpb.attribute5
          FROM apps.ozf_funds_utilized_all_b fu
              ,apps.qp_list_headers_b        qpb
              ,apps.ozf_offers               oof
              ,(SELECT DISTINCT attribute5
                                     ,attribute6
                                     ,list_header_id
                        FROM apps.qp_qualifiers
                       WHERE qualifier_attribute = 'QUALIFIER_ATTRIBUTE2'
                         AND qualifier_context = 'SOLD_BY') qpq
              ,apps.ozf_resale_lines_all     orl
         WHERE fu.plan_type = 'OFFR'
           AND fu.plan_id = qpb.list_header_id
           AND fu.object_id = orl.resale_line_id
           AND fu.cust_account_id = orl.sold_from_cust_account_id
           AND qpb.list_header_id = qpq.list_header_id
           AND oof.qp_list_header_id = qpb.list_header_id
           AND qpb.attribute5 = nvl(p_pmt_method, qpb.attribute5)
           AND qpb.attribute6 = nvl(p_pmt_frequency, qpb.attribute6)
           AND nvl(oof.autopay_flag, 'Y') = 'Y'
           AND fu.plan_curr_amount_remaining <> 0
           AND fu.utilization_type IN ('ACCRUAL', 'ADJUSTMENT')
           AND fu.cust_account_id =
               nvl(l_funds_util_flt.cust_account_id, fu.cust_account_id)
           AND fu.plan_id =l_funds_util_flt.activity_id --Ver 3.0
           AND fu.gl_posted_flag = 'Y'
         GROUP BY fu.currency_code
                 ,orl.sold_from_cust_account_id
                 ,fu.plan_id
                 ,qpb.attribute5;
--7/12/2013

    ELSE
      fnd_file.put_line(fnd_file.log, 'Error2 - testing performance issue 07/14/2016');
      OPEN clm_line_csr FOR
--Version 1.1 7/12/2013
/*        SELECT SUM(fu.amount_remaining) amount
              ,fu.currency_code
              ,orl.sold_from_cust_account_id
              ,fu.plan_id
              ,qpb.attribute5 --,
        -- qpb.attribute6

          FROM apps.ozf_funds_utilized_all_b fu
              ,apps.qp_list_headers_b        qpb
              ,apps.qp_qualifiers            qpq
              ,apps.ozf_offers               oof
              ,apps.ozf_resale_lines_all     orl
         WHERE fu.plan_type = 'OFFR'
           AND fu.plan_id = qpb.list_header_id
           AND fu.object_id = orl.resale_line_id
           AND fu.cust_account_id = orl.sold_from_cust_account_id
           AND qpb.list_header_id = qpq.list_header_id
           AND qpb.attribute5 = nvl(p_pmt_method, qpb.attribute5)
           AND qpb.attribute6 = nvl(p_pmt_frequency, qpb.attribute6)
           AND fu.attribute10 = 'REIMBURSEMENT'
           AND oof.qp_list_header_id = qpb.list_header_id
           AND fu.fund_id = nvl(l_funds_util_flt.fund_id, fu.fund_id)
           AND fu.plan_curr_amount_remaining <> 0
           AND fu.utilization_type IN ('ACCRUAL', 'ADJUSTMENT')
           AND fu.cust_account_id =
               nvl(l_funds_util_flt.cust_account_id, fu.cust_account_id)
           AND nvl(oof.autopay_flag, 'Y') = 'Y'
           AND fu.plan_id = nvl(l_funds_util_flt.activity_id, fu.plan_id)
              --AND oof.autopay_flag = 'Y'
           AND fu.gl_posted_flag = 'Y'
           AND qualifier_attribute = 'QUALIFIER_ATTRIBUTE2'
           AND qualifier_context = 'SOLD_BY'
         GROUP BY fu.currency_code
                 ,orl.sold_from_cust_account_id
                 ,fu.plan_id
                 ,qpb.attribute5; --,
      --qpb.attribute6;
*/
SELECT SUM(fu.amount_remaining) amount
              ,fu.currency_code
              ,orl.sold_from_cust_account_id
              ,fu.plan_id
              ,qpb.attribute5
          FROM apps.ozf_funds_utilized_all_b fu
              ,apps.qp_list_headers_b        qpb
              ,(SELECT DISTINCT attribute5
                                     ,attribute6
                                     ,list_header_id
                        FROM apps.qp_qualifiers
                       WHERE qualifier_attribute = 'QUALIFIER_ATTRIBUTE2'
                         AND qualifier_context = 'SOLD_BY') qpq
              ,apps.ozf_offers               oof
              ,apps.ozf_resale_lines_all     orl
         WHERE fu.plan_type = 'OFFR'
           AND fu.plan_id = qpb.list_header_id
           AND fu.object_id = orl.resale_line_id
           AND fu.cust_account_id = orl.sold_from_cust_account_id
           AND qpb.list_header_id = qpq.list_header_id
           AND qpb.attribute5 = nvl(p_pmt_method, qpb.attribute5)
           AND QPB.ATTRIBUTE6 = NVL(P_PMT_FREQUENCY, QPB.ATTRIBUTE6)
           AND FU.ATTRIBUTE10 --= 'REIMBURSEMENT'
                        IN ( SELECT LOOKUP_CODE
                             from apps.fnd_lookup_values_vl
                             WHERE     1 = 1
                             and lookup_type = 'XXCUS_OZF_NEG_INVOICE_TYPES')
           AND oof.qp_list_header_id = qpb.list_header_id
           AND fu.fund_id = nvl(l_funds_util_flt.fund_id, fu.fund_id)
           AND fu.plan_curr_amount_remaining <> 0
           AND fu.utilization_type IN ('ACCRUAL', 'ADJUSTMENT')
           AND fu.cust_account_id =
               nvl(l_funds_util_flt.cust_account_id, fu.cust_account_id)
           AND nvl(oof.autopay_flag, 'Y') = 'Y'
           AND fu.plan_id =l_funds_util_flt.activity_id --Ver 3.0
           AND fu.gl_posted_flag = 'Y'
            GROUP BY fu.currency_code
                 ,orl.sold_from_cust_account_id
                 ,fu.plan_id
                 ,qpb.attribute5;
--7/12/2013
    END IF;

    LOOP
      FETCH clm_line_csr
        INTO l_amount
            ,l_currency_code
            ,l_cust_account_id
            ,l_plan_id
            ,l_pmt_method;

      EXIT WHEN clm_line_csr%NOTFOUND;

      fnd_file.put_line(fnd_file.log, 'Amount ' || l_amount);
      fnd_file.put_line(fnd_file.log, 'Currency Code ' || l_currency_code);
      fnd_file.put_line(fnd_file.log, 'Customer ID ' || l_cust_account_id);
      fnd_file.put_line(fnd_file.log, 'Plan ID ' || l_plan_id);

      --
      fnd_file.put_line(fnd_file.log
                       ,'l_claim_rec.currency_code' ||
                        l_claim_rec.currency_code);
      IF l_currency_code <> l_claim_rec.currency_code
      THEN
        l_amount_ut_curr := l_amount;

        fnd_file.put_line(fnd_file.log
                         ,'l_amount_ut_curr' || l_amount_ut_curr);

        --Added for bug 7030415, get exchange_rate type
        OPEN c_get_conversion_type;
        FETCH c_get_conversion_type
          INTO l_exchange_rate_type;
        CLOSE c_get_conversion_type;
        --end
        fnd_file.put_line(fnd_file.log, 'Convert Currency');

        ozf_utility_pvt.convert_currency(p_from_currency => l_currency_code
                                        ,p_to_currency   => l_claim_rec.currency_code
                                        ,p_conv_type     => l_exchange_rate_type
                                        ,p_conv_date     => SYSDATE
                                        ,p_from_amount   => l_amount_ut_curr
                                        , -- amount in utilization currency (func currency)
                                         x_return_status => l_return_status
                                        ,x_to_amount     => l_amount
                                        , -- amount in claim currency
                                         x_rate          => l_rate);

        fnd_file.put_line(fnd_file.log, 'Converted  Currency');

        IF l_return_status = fnd_api.g_ret_sts_error
        THEN
          RAISE fnd_api.g_exc_error;
        ELSIF l_return_status = fnd_api.g_ret_sts_unexp_error
        THEN
          RAISE fnd_api.g_exc_unexpected_error;
        END IF;
      END IF;

      -- IF l_amount IS NOT NULL AND l_amount <> 0 THEN
      IF ozf_debug_high_on
      THEN
        ozf_utility_pvt.debug_message('---------------------------------');
        ozf_utility_pvt.debug_message('Line ' || l_counter || ': Amount=' ||
                                      l_amount);
        --OZF_Utility_PVT.debug_message('Product Level Type:  ' || l_product_level_type);
        --OZF_Utility_PVT.debug_message('Product Id        :  ' || l_product_id);
        --OZF_Utility_PVT.debug_message('Rx Plan Id           :  ' || l_rx_plan_id);

        -- Added For Bug 8402328
        --OZF_Utility_PVT.debug_message('l_funds_util_flt.utilization_id        :  ' || l_funds_util_flt.utilization_id);
      END IF;

      --IF p_claim_rec.created_from = 'AUTOPAY' THEN
      fnd_file.put_line(fnd_file.log, '---------------------------------');
      fnd_file.put_line(fnd_file.log
                       ,'Line ' || l_counter || ': Amount=' || l_amount);
      fnd_file.put_line(fnd_file.log
                       ,'Customer ID         :  ' ||
                        l_funds_util_flt.cust_account_id);
      fnd_file.put_line(fnd_file.log
                       ,'Fund ID         :  ' || l_funds_util_flt.fund_id);
      --FND_FILE.PUT_LINE(FND_FILE.LOG, 'Plan Type         :  ' || l_offer_type);
      fnd_file.put_line(fnd_file.log, 'Plan ID           :  ' || l_plan_id);
      /* FND_FILE.PUT_LINE(FND_FILE.LOG, 'Product Level Type:  ' || l_product_level_type);
      FND_FILE.PUT_LINE(FND_FILE.LOG, 'Product Id        :  ' || l_product_id);
      FND_FILE.PUT_LINE(FND_FILE.LOG, 'Rx Plan Id        :  ' || l_rx_plan_id);*/
      --END IF;

      --Added Debug For Multi Currency - kpatro
      IF ozf_debug_high_on
      THEN
        ozf_utility_pvt.debug_message('l_bill_to_site_id         :  ' ||
                                      l_bill_to_site_id);
        ozf_utility_pvt.debug_message('l_funds_util_flt.bill_to_site_use_id           :  ' ||
                                      l_funds_util_flt.bill_to_site_use_id);
        ozf_utility_pvt.debug_message('l_funds_util_flt.utiz_currency_code           :  ' ||
                                      l_funds_util_flt.utiz_currency_code);
        ozf_utility_pvt.debug_message('l_currency_code           :  ' ||
                                      l_currency_code);
      END IF;
      --R12.1 enhancements. Added condition so that earnings accrued only against
      --the respective bill_to_site_id should be added.

      IF ozf_debug_high_on
      THEN
        ozf_utility_pvt.debug_message('l_bill_to_site_id         :  ' ||
                                      l_bill_to_site_id);
        ozf_utility_pvt.debug_message('l_funds_util_flt.bill_to_site_use_id           :  ' ||
                                      l_funds_util_flt.bill_to_site_use_id);
        ozf_utility_pvt.debug_message('l_funds_util_flt.utiz_currency_code           :  ' ||
                                      l_funds_util_flt.utiz_currency_code);
      END IF;

      -- The Claim Created From Autopay should be grouped based on Currency Code Also - kpatro

      fnd_file.put_line(fnd_file.log
                       ,'l_bill_to_site_id ' || l_bill_to_site_id);
      fnd_file.put_line(fnd_file.log
                       ,'l_funds_util_flt.bill_to_site_use_id ' ||
                        l_funds_util_flt.bill_to_site_use_id);

      l_bill_to_site_id := l_funds_util_flt.bill_to_site_use_id;

      IF l_bill_to_site_id IS NOT NULL AND
         l_bill_to_site_id = l_funds_util_flt.bill_to_site_use_id AND
         l_currency_code = l_funds_util_flt.utiz_currency_code -- Added the check for Multi Currency - kpatro

      THEN

        -- assume single currency for now; add multi-curr later
        l_line_tbl(l_counter).claim_currency_amount := l_amount;
        l_line_tbl(l_counter).activity_type := 'OFFR';
        l_line_tbl(l_counter).activity_id := l_plan_id;
        l_line_tbl(l_counter).attribute1 := l_offer_type;
        l_line_tbl(l_counter).attribute2 := l_offr_name;
        l_line_tbl(l_counter).attribute8 := l_pmt_method;
        --l_line_tbl(l_counter).item_type := l_product_level_type;
        --l_line_tbl(l_counter).item_id := l_product_id;
        --l_line_tbl(l_counter).relationship_type := l_funds_util_flt.relationship_type;
        --l_line_tbl(l_counter).related_cust_account_id := l_funds_util_flt.related_cust_account_id;
        --l_line_tbl(l_counter).buy_group_party_id := l_funds_util_flt.buy_group_party_id;
        --l_line_tbl(l_counter).select_cust_children_flag := l_funds_util_flt.select_cust_children_flag;
        --HUMOZF
        --l_line_tbl(l_counter).attribute1 := l_rx_plan_id;
        --l_line_tbl(l_counter).attribute2 := l_quantity; -- qty
        --l_line_tbl(l_counter).attribute3 := l_unit_rebate; -- rate

        -- Added For Bug 8402328
        --l_line_tbl(l_counter).utilization_id := l_funds_util_flt.utilization_id;

        l_total_acctd_amount_rem := l_total_acctd_amount_rem + l_amount;

        /* IF l_product_level_type = 'PRODUCT' AND
           l_product_id IS NOT NULL THEN
           OPEN csr_uom_code(l_line_tbl(l_counter).item_id);
           FETCH csr_uom_code
              INTO l_line_tbl(l_counter).quantity_uom;
           IF csr_uom_code%NOTFOUND THEN
              CLOSE csr_uom_code;
              IF FND_MSG_PUB.Check_Msg_Level(FND_MSG_PUB.G_MSG_LVL_ERROR) THEN
                 FND_MESSAGE.Set_Name('OZF',
                                      'OZF_PRODUCT_UOM_MISSING');
                 FND_MESSAGE.Set_Token('ITEM_ID',
                                       l_line_tbl(l_counter)
                                       .item_id);
                 FND_MSG_PUB.ADD;
              END IF;
              RAISE FND_API.g_exc_unexpected_error;
           END IF;
           CLOSE csr_uom_code;
        END IF; */

        fnd_file.put_line(fnd_file.log
                         ,'l_total_acctd_amount_rem' ||
                          l_total_acctd_amount_rem);
        fnd_file.put_line(fnd_file.log, 'l_amount' || l_amount);

        IF l_plan_type = 'OFFR' AND l_plan_id IS NOT NULL
        THEN
          OPEN csr_offer_perf(l_plan_id);
          FETCH csr_offer_perf
            INTO l_dummy;
          CLOSE csr_offer_perf;

          IF l_dummy = 1
          THEN
            l_line_tbl(l_counter).performance_attached_flag := fnd_api.g_true;
            l_line_tbl(l_counter).performance_complete_flag := fnd_api.g_true;
          END IF;
        END IF;
        fnd_file.put_line(fnd_file.log, 'Counter ' || l_counter);
        l_counter := l_counter + 1;

        -- Fix for Bug 8501176
      ELSIF l_funds_util_flt.bill_to_site_use_id IS NULL
      THEN
        --AND
        --l_currency_code = l_funds_util_flt.utiz_currency_code THEN
        fnd_file.put_line(fnd_file.log, 'Bill to Site is null');

        l_line_tbl(l_counter).claim_currency_amount := l_amount;
        l_line_tbl(l_counter).activity_type := 'OFFR';
        l_line_tbl(l_counter).activity_id := l_plan_id;
        l_line_tbl(l_counter).attribute1 := l_offer_type;
        l_line_tbl(l_counter).attribute2 := l_offr_name;
        --l_line_tbl(l_counter).item_type := l_product_level_type;
        --l_line_tbl(l_counter).item_id := l_product_id;
        --l_line_tbl(l_counter).relationship_type := l_funds_util_flt.relationship_type;
        --l_line_tbl(l_counter).related_cust_account_id := l_funds_util_flt.related_cust_account_id;
        --l_line_tbl(l_counter).buy_group_party_id := l_funds_util_flt.buy_group_party_id;
        --l_line_tbl(l_counter).select_cust_children_flag := l_funds_util_flt.select_cust_children_flag;
        --l_line_tbl(l_counter).attribute1 := l_rx_plan_id;
        -- Added For Bug 8402328
        --l_line_tbl(l_counter).utilization_id := l_funds_util_flt.utilization_id;

        l_total_acctd_amount_rem := l_total_acctd_amount_rem + l_amount;

        fnd_file.put_line(fnd_file.log
                         ,'NP l_total_acctd_amount_rem        :  ' ||
                          l_total_acctd_amount_rem);
        fnd_file.put_line(fnd_file.log
                         ,'NP l_amount        :  ' || l_amount);

        /*IF l_product_level_type = 'PRODUCT' AND
           l_product_id IS NOT NULL THEN
           OPEN csr_uom_code(l_line_tbl(l_counter).item_id);
           FETCH csr_uom_code
              INTO l_line_tbl(l_counter).quantity_uom;
           IF csr_uom_code%NOTFOUND THEN
              CLOSE csr_uom_code;
              IF FND_MSG_PUB.Check_Msg_Level(FND_MSG_PUB.G_MSG_LVL_ERROR) THEN
                 FND_MESSAGE.Set_Name('OZF',
                                      'OZF_PRODUCT_UOM_MISSING');
                 FND_MESSAGE.Set_Token('ITEM_ID',
                                       l_line_tbl(l_counter)
                                       .item_id);
                 FND_MSG_PUB.ADD;
              END IF;
              RAISE FND_API.g_exc_unexpected_error;
           END IF;
           CLOSE csr_uom_code;
        END IF; */

        fnd_file.put_line(fnd_file.log
                         ,'NP l_total_acctd_amount_rem        :  ' ||
                          l_total_acctd_amount_rem);
        fnd_file.put_line(fnd_file.log
                         ,'NP l_amount        :  ' || l_amount);

        fnd_file.put_line(fnd_file.log
                         ,'l_total_acctd_amount_rem' ||
                          l_total_acctd_amount_rem);

        IF l_plan_type = 'OFFR' AND l_plan_id IS NOT NULL
        THEN
          OPEN csr_offer_perf(l_plan_id);
          FETCH csr_offer_perf
            INTO l_dummy;
          CLOSE csr_offer_perf;

          IF l_dummy = 1
          THEN
            l_line_tbl(l_counter).performance_attached_flag := fnd_api.g_true;
            l_line_tbl(l_counter).performance_complete_flag := fnd_api.g_true;
          END IF;
        END IF;
        --
        l_counter := l_counter + 1;

      END IF;

      fnd_file.put_line(fnd_file.log, 'l_counter' || l_counter);
      --
      -- END IF;
    END LOOP;
    CLOSE clm_line_csr;

    --
    IF p_pmt_frequency = 'MONTHLY'
    THEN

      OPEN c_get_period_names('MONTH', l_funds_util_flt.end_date);
      FETCH c_get_period_names
        INTO l_period_type
            ,l_period_name
            ,l_period_id
            ,l_period_start
            ,l_period_end;
      CLOSE c_get_period_names;

    END IF;

    IF p_pmt_frequency = 'QTRLY' OR p_pmt_frequency = 'SEMIANNUAL'
    THEN

      OPEN c_get_period_names('QTR', l_funds_util_flt.end_date);
      FETCH c_get_period_names
        INTO l_period_type
            ,l_period_name
            ,l_period_id
            ,l_period_start
            ,l_period_end;
    END IF;

    IF p_pmt_frequency = 'ANNUAL'
    THEN
      OPEN c_get_period_names('ANNUAL', l_funds_util_flt.end_date);
      FETCH c_get_period_names
        INTO l_period_type
            ,l_period_name
            ,l_period_id
            ,l_period_start
            ,l_period_end;
    END IF;

    fnd_file.put_line(fnd_file.output, 'Period Name ' || l_period_name);
    fnd_file.put_line(fnd_file.output, 'Period ID ' || l_period_id);

    fnd_file.put_line(fnd_file.log
                     ,'Acctd Amt Remaining ' || l_total_acctd_amount_rem);

    fnd_file.put_line(fnd_file.output, 'Expense ' || l_expense);
    fnd_file.put_line(fnd_file.output, 'Party ID ' || l_party_id);
    fnd_file.put_line(fnd_file.output, 'Party Name' || l_party_name);

    -- Flow initiated from promotional payment
    IF (l_claim_rec.created_from = 'PROMO_CLAIM')
    THEN
      l_claim_rec.amount := l_total_acctd_amount_rem;
    END IF;

    -- Fix for Bug 8501176
    IF l_total_acctd_amount_rem <> 0
    THEN
      l_claim_rec.amount := l_total_acctd_amount_rem;

      -- create claim in OPEN status, ignoring the status passed in
      l_claim_rec.status_code    := 'OPEN';
      l_claim_rec.user_status_id := to_number(ozf_utility_pvt.get_default_user_status(p_status_type => 'OZF_CLAIM_STATUS'
                                                                                     ,p_status_code => l_claim_rec.status_code));
      -- l_claim_rec.attribute1 :=   l_party_id;
      -- l_claim_rec.attribute2 :=   l_party_name;
      l_claim_rec.attribute3 := p_pmt_frequency;
      --l_claim_rec.cust_billto_acct_site_id := l_funds_util_flt.bill_to_site_use_id;
      --l_claim_rec.attribute5 :=   p_pmt_method;
      --l_claim_rec.attribute6 :=   l_period_end;
      --l_claim_rec.attribute7 :=   l_funds_util_flt.fund_id;
      --l_claim_rec.attribute8 :=   l_funds_util_flt.activity_id;
      -- l_claim_rec.attribute_category := p_org_id;
      --l_claim_rec.attribute12 :=  p_org_id;
      --l_claim_rec.attribute9 :=   l_party_id;

      l_claim_rec.legal_entity_id := NULL;

      OPEN get_legal_entity(p_org_id);
      FETCH get_legal_entity
        INTO l_claim_rec.legal_entity_id;
      CLOSE get_legal_entity;

      fnd_file.put_line(fnd_file.log
                       ,'l_claim_rec.cust_billto_acct_site_id' ||
                        l_claim_rec.cust_billto_acct_site_id);

      -- Create a Claim
      fnd_file.put_line(fnd_file.output, 'Create Claim');

      ozf_claim_pvt.create_claim(p_api_version   => l_api_version
                                ,x_return_status => l_return_status
                                ,x_msg_data      => x_msg_data
                                ,x_msg_count     => x_msg_count
                                ,p_claim         => l_claim_rec
                                ,x_claim_id      => l_claim_id);

      fnd_file.put_line(fnd_file.output, 'End Create Claim');

      IF l_return_status = fnd_api.g_ret_sts_error
      THEN
        RAISE fnd_api.g_exc_unexpected_error;
      ELSIF l_return_status = fnd_api.g_ret_sts_unexp_error
      THEN
        RAISE fnd_api.g_exc_unexpected_error;
      END IF;

      IF ozf_debug_high_on
      THEN
        ozf_utility_pvt.debug_message('Claim created id: ' || l_claim_id);
      END IF;

      FOR i IN l_line_tbl.first .. l_line_tbl.last
      LOOP
        IF l_line_tbl.exists(i) IS NOT NULL
        THEN
          l_line_tbl(i).claim_id := l_claim_id;
        END IF;
      END LOOP;

      fnd_file.put_line(fnd_file.output
                       ,'PROCEDURE : Create_Claim_Line_Tbl');

      ozf_claim_line_pvt.create_claim_line_tbl(p_api_version      => 1.0
                                              ,p_init_msg_list    => fnd_api.g_false
                                              ,p_commit           => fnd_api.g_false
                                              ,p_validation_level => fnd_api.g_valid_level_full
                                              ,x_return_status    => l_return_status
                                              ,x_msg_count        => x_msg_count
                                              ,x_msg_data         => x_msg_data
                                              ,p_claim_line_tbl   => l_line_tbl
                                              ,x_error_index      => l_error_index);

      fnd_file.put_line(fnd_file.output
                       ,'End PROCEDURE : Create_Claim_Line_Tbl');

      IF l_return_status = fnd_api.g_ret_sts_error
      THEN
        RAISE fnd_api.g_exc_error;
      ELSIF l_return_status = fnd_api.g_ret_sts_unexp_error
      THEN
        RAISE fnd_api.g_exc_error;
      END IF;

      IF ozf_debug_high_on
      THEN
        ozf_utility_pvt.debug_message('Claim lines created for claim_id=' ||
                                      l_claim_id);
      END IF;

      OPEN csr_claim_line(l_claim_id);
      LOOP
        FETCH csr_claim_line
          INTO l_funds_util_flt.claim_line_id
              ,l_funds_util_flt.activity_type
              ,l_funds_util_flt.activity_id
              ,l_funds_util_flt.total_amount;
        --l_funds_util_flt.product_level_type,
        --l_funds_util_flt.product_id,

        --l_funds_util_flt.reference_type;
        EXIT WHEN csr_claim_line%NOTFOUND;

        fnd_file.put_line(fnd_file.output
                         ,'total_amount' || l_funds_util_flt.total_amount);

        fnd_file.put_line(fnd_file.output
                         ,'Procedure : Update_Group_Line_Util');

        update_group_line_util(p_api_version      => 1.0
                              ,p_init_msg_list    => fnd_api.g_false
                              ,p_commit           => fnd_api.g_false
                              ,p_validation_level => fnd_api.g_valid_level_full
                              ,x_return_status    => l_return_status
                              ,x_msg_count        => x_msg_count
                              ,x_msg_data         => x_msg_data
                              ,p_summary_view     => 'ACTIVITY'
                              ,p_pmt_frequency    => p_pmt_frequency
                              ,p_pmt_method       => p_pmt_method
                              ,p_invoice_type     => p_invoice_type
                              ,p_funds_util_flt   => l_funds_util_flt);

        fnd_file.put_line(fnd_file.output
                         ,'Procedure :End Update_Group_Line_Util');

        IF l_return_status = fnd_api.g_ret_sts_error
        THEN
          RAISE fnd_api.g_exc_error;
        ELSIF l_return_status = fnd_api.g_ret_sts_unexp_error
        THEN
          RAISE fnd_api.g_exc_error;
        END IF;
      END LOOP;
      CLOSE csr_claim_line;

    ELSIF l_claim_rec.created_from = 'PROMO_CLAIM' AND
          l_claim_rec.amount <> 0
    THEN
      IF ozf_debug_high_on
      THEN
        ozf_utility_pvt.debug_message('Created Form: ' ||
                                      l_claim_rec.created_from);
        ozf_utility_pvt.debug_message('Amount: ' || l_claim_rec.amount);
      END IF;

      IF fnd_msg_pub.check_msg_level(fnd_msg_pub.g_msg_lvl_error)
      THEN
        fnd_message.set_name('OZF', 'OZF_ACCRUAL_SCAN_DATA_ERROR');
        fnd_msg_pub.add;
      END IF;

      RAISE fnd_api.g_exc_unexpected_error;

    ELSIF l_claim_rec.created_from <> 'PROMO_CLAIM'
    THEN
      IF fnd_msg_pub.check_msg_level(fnd_msg_pub.g_msg_lvl_error)
      THEN
        fnd_message.set_name('OZF', 'OZF_ACCRUAL_REM_AMOUNT_LT_ZERO');
        fnd_msg_pub.add;
      END IF;
      RAISE fnd_api.g_exc_unexpected_error;

    END IF;

    x_claim_id := l_claim_id;

    ------------------------- finish -------------------------------
    -- Check for commit
    IF fnd_api.to_boolean(p_commit)
    THEN
      COMMIT;
    END IF;

    fnd_msg_pub.count_and_get(p_encoded => fnd_api.g_false
                             ,p_count   => x_msg_count
                             ,p_data    => x_msg_data);

    IF ozf_debug_high_on
    THEN
      ozf_utility_pvt.debug_message(l_full_name || ': end');
    END IF;

  EXCEPTION
    WHEN fnd_api.g_exc_error THEN
      ROLLBACK TO create_claim_for_accruals;
      x_return_status := fnd_api.g_ret_sts_error;
      fnd_msg_pub.count_and_get(p_encoded => fnd_api.g_false
                               ,p_count   => x_msg_count
                               ,p_data    => x_msg_data);

    WHEN fnd_api.g_exc_unexpected_error THEN
      ROLLBACK TO create_claim_for_accruals;
      x_return_status := fnd_api.g_ret_sts_unexp_error;
      fnd_msg_pub.count_and_get(p_encoded => fnd_api.g_false
                               ,p_count   => x_msg_count
                               ,p_data    => x_msg_data);

    WHEN OTHERS THEN
      ROLLBACK TO create_claim_for_accruals;
      x_return_status := fnd_api.g_ret_sts_unexp_error;
      IF fnd_msg_pub.check_msg_level(fnd_msg_pub.g_msg_lvl_unexp_error)
      THEN
        fnd_msg_pub.add_exc_msg(g_pkg_name, l_api_name);
      END IF;
      fnd_msg_pub.count_and_get(p_encoded => fnd_api.g_false
                               ,p_count   => x_msg_count
                               ,p_data    => x_msg_data);

  END create_claim_for_accruals;
  --
  function get_lob_name (p_lob_id in varchar2) return varchar as
   --
   v_lob_name varchar2(150);
   --
  begin
     select /*+ RESULT_CACHE */ hp2.party_name lob_name
     into   v_lob_name
     /*
        hp1.party_name bu_name,
        hp1.party_number bu_number,
        hp2.party_name lob_name,
        hp2.party_number lob_number
       ,hp1.status bu_party_status
       ,hp2.status lob_party_status
       ,rel.status relationship_status
     */
     from hz_parties hp1,
          hz_parties hp2,
          hz_relationships rel
     where 1 =1
       and hp1.party_id              =p_lob_id
       and hp1.attribute1(+)         ='HDS_BU'
       and hp1.party_id              =rel.subject_id(+)
       and hp2.party_id(+)           =rel.object_id
       and hp2.attribute1(+)         ='HDS_LOB'
       and rel.subject_table_name(+) ='HZ_PARTIES'
       and rel.object_table_name(+)  ='HZ_PARTIES'
       and rel.relationship_code(+)  ='DIVISION_OF'
       and rel.directional_flag(+) in ('F', 'B');
   return v_lob_name;
  exception
   when others then
    printlog('@get_LOB_name, p_lob_id ='||p_lob_id||', error_msg ='||sqlerrm);
    return null;
  end get_lob_name;
  --
  ---------------------------------------------------------------------
  -- PROCEDURE
  --    Create_Claim_For_Cust
  --
  -- PURPOSE
  --    This procedure creates a claim and its lines for a customer based on the utilization table.
  --    It will then settle it based on different payment method.
  --
  -- PARAMETERS
  --    p_customer_info IN g_customer_info_csr%rowtype
  --    p_amount IN number,
  --    p_mode IN varchar2
  --    p_auto_reason_code_id IN number
  --    p_auto_claim_type_id  IN number
  --    p_autopay_periodicity IN number
  --    p_autopay_periodicity_type IN VARCHAR2
  --    p_offer_payment_method IN VARCHAR2
  --    p_funds_util_flt      IN OZF_Claim_Accrual_PVT.funds_util_flt_type
  --    x_return_status       OUT VARCHAR2
  --
  -- NOTES
  ---------------------------------------------------------------------
  PROCEDURE create_claim_for_cust(p_customer_info       IN g_customer_info_csr%ROWTYPE
                                 ,p_org_id              IN NUMBER
                                 ,p_amount              IN NUMBER
                                 ,p_mode                IN VARCHAR2
                                 ,p_auto_reason_code_id IN NUMBER
                                 ,p_auto_claim_type_id  IN NUMBER
                                 ,p_pmt_frequency       IN VARCHAR2
                                 ,p_pmt_method          IN VARCHAR2
                                 ,p_invoice_type        IN VARCHAR2
                                 ,p_funds_util_flt      IN funds_util_flt_type
                                 ,x_return_status       OUT NOCOPY VARCHAR2) IS

    l_amount           NUMBER := p_amount;
    l_cust_account_id  NUMBER;
    l_last_pay_date    DATE;
    l_claim_id         NUMBER;
    l_claim_rec        ozf_claim_pvt.claim_rec_type;
    l_claim_settle_rec ozf_claim_pvt.claim_rec_type;
    l_funds_util_flt   funds_util_flt_type := p_funds_util_flt;
    l_plan_type        VARCHAR2(30);

    CURSOR csr_cust_name(cv_cust_account_id IN NUMBER) IS
      SELECT concat(concat(party.party_name, ' (')
                   ,concat(ca.account_number, ') '))
        FROM apps.hz_cust_accounts ca, apps.hz_parties party
       WHERE ca.party_id = party.party_id
         AND ca.cust_account_id = cv_cust_account_id;

    l_cust_name_num VARCHAR2(70);

    CURSOR get_offr_name(v_plan_id IN NUMBER) IS
      SELECT qlh.description
            ,qlh.name offer_code --ESMS 240298
        FROM qp_list_headers_vl qlh
       WHERE list_header_id = v_plan_id;

    CURSOR claim_info_csr(p_claim_id IN NUMBER) IS
      SELECT object_version_number, sales_rep_id
        FROM apps.ozf_claims_all
       WHERE claim_id = p_claim_id;

    l_object_version_number NUMBER;
    l_return_status         VARCHAR2(1);
    l_msg_data              VARCHAR2(2000);
    l_msg_count             NUMBER;

    l_autopay_periodicity      NUMBER;
    l_autopay_periodicity_type VARCHAR2(30);

    CURSOR csr_ar_system_options IS
      SELECT salesrep_required_flag FROM apps.ar_system_parameters;
    l_salesrep_req_flag VARCHAR2(1);
    l_sales_rep_id      NUMBER;

    CURSOR csr_claim_num(cv_claim_id IN NUMBER) IS
      SELECT claim_number, amount, cust_billto_acct_site_id
        FROM apps.ozf_claims
       WHERE claim_id = cv_claim_id;

    ---Get the Max Expense----
    CURSOR max_expense_csr(v_plan_id         IN NUMBER
                          ,v_cust_account_id IN NUMBER) IS
      SELECT a.expense, a.end_cust_party_name, a.end_cust_party_id
        FROM (SELECT SUM(orl.quantity * orl.selling_price) expense
                    ,orl.end_cust_party_name
                    ,orl.end_cust_party_id

                FROM ozf_resale_lines_all orl, ozf_funds_utilized_all_b ofu
               WHERE orl.resale_line_id = ofu.object_id
                 AND ofu.cust_account_id = orl.sold_from_cust_account_id
                 AND ofu.plan_id =v_plan_id --'20478' --Ver 3.0
                 AND ofu.plan_curr_amount_remaining <> 0
                 AND ofu.cust_account_id =
                     nvl(v_cust_account_id, ofu.cust_account_id) --'2006'
               GROUP BY orl.end_cust_party_name, orl.end_cust_party_id

              ) a
       WHERE a.expense IN
             (SELECT MAX(expense)
                FROM (SELECT SUM(orl.quantity * orl.selling_price) expense
                            ,orl.end_cust_party_id

                        FROM ozf_resale_lines_all     orl
                            ,ozf_funds_utilized_all_b ofu
                       WHERE orl.resale_line_id = ofu.object_id
                         AND ofu.cust_account_id =
                             orl.sold_from_cust_account_id
                         AND ofu.plan_id =v_plan_id --'20478's --Ver 3.0
                         AND ofu.plan_curr_amount_remaining <> 0
                         AND ofu.cust_account_id =
                             nvl(v_cust_account_id, ofu.cust_account_id) --'2006'
                         AND ofu.cust_account_id =
                             orl.sold_from_cust_account_id
                       GROUP BY orl.end_cust_party_id));

    CURSOR csr_get_site_use_id(cv_cust_account_id IN NUMBER
                              ,cv_lob_id          IN NUMBER) IS
      SELECT sold_from_site_id
        FROM ozf_resale_lines_all orl
       WHERE orl.end_cust_party_id = cv_lob_id
         AND orl.sold_from_cust_account_id =
             nvl(cv_cust_account_id, orl.sold_from_cust_account_id)
         AND orl.org_id = p_org_id
         AND rownum = 1;

    CURSOR csr_get_cust_site_id(cv_soldfrom_siteid IN NUMBER) IS
      SELECT hcas.cust_acct_site_id
        FROM hz_cust_acct_sites_all hcas
       WHERE hcas.party_site_id = cv_soldfrom_siteid
         AND hcas.org_id = p_org_id;

    CURSOR csr_get_payto_siteid(cv_cust_id IN NUMBER) IS
      SELECT hcus.site_use_id
        FROM hz_cust_site_uses_all hcus
       WHERE hcus.cust_acct_site_id = cv_cust_id
         AND hcus.site_use_code = 'BILL_TO';

    l_party_id           NUMBER;
    l_expense            NUMBER;
    l_party_name         VARCHAR2(50);
    l_party_site_id      NUMBER;
    l_billto_site_use_id NUMBER;

    l_claim_num                VARCHAR2(30);
    l_claim_amt                NUMBER;
    l_cust_billto_acct_site_id NUMBER;

    l_offr_name          VARCHAR2(240);
    l_offer_code         qp_list_headers_tl.name%type;
    l_eligible_flag      VARCHAR2(1);
    l_bill_to_party_name VARCHAR2(30);
    l_billto_party_id    NUMBER;
    l_pay_to_site_id     NUMBER;
    l_cust_site_id       NUMBER;

  BEGIN

    IF ozf_debug_high_on
    THEN
      ozf_utility_pvt.debug_message('create_claim_for_cust START');
    END IF;

    -- Initialize API return status to sucess
    x_return_status := fnd_api.g_ret_sts_success;

    ---Get the Offer Name-------
    OPEN get_offr_name(l_funds_util_flt.activity_id);
    FETCH get_offr_name
      INTO l_offr_name, l_offer_code;
    CLOSE get_offr_name;

    ---Get the Party Site Use ID----
    OPEN max_expense_csr(l_funds_util_flt.activity_id
                        ,l_funds_util_flt.cust_account_id);
    FETCH max_expense_csr
      INTO l_expense, l_party_name, l_party_id;

    CLOSE max_expense_csr;
    fnd_file.put_line(fnd_file.log
                     ,'l_billto_party_id' || l_billto_party_id);
    fnd_file.put_line(fnd_file.log, 'l_expense' || l_expense);
    fnd_file.put_line(fnd_file.log, 'l_party_name' || l_party_name);
    fnd_file.put_line(fnd_file.log, 'l_party_id' || l_party_id);
    -- For this customer: check whether there is a need to create a claim
    -- check sum of acctd_amount from utiliztion only create claims with positvit amount
    -- IF l_amount is NOT NULL AND l_amount > 0 THEN
    IF l_amount IS NOT NULL AND l_amount <> 0
    THEN
      l_eligible_flag := fnd_api.g_true;

      IF l_eligible_flag = fnd_api.g_true THEN
      --
        IF l_amount > 0 THEN
          l_claim_rec.claim_class    := 'CLAIM';
          l_claim_rec.payment_method := 'HDS_REB_INV';
        ELSE
          l_claim_rec.claim_class    := 'CHARGE';
          l_claim_rec.payment_method := 'HDS_REB_INV';
        END IF;
        --
        l_claim_rec.claim_type_id  :=p_auto_claim_type_id;
        l_claim_rec.reason_code_id :=p_auto_reason_code_id;
        --
        -- Modified for FXGL Enhancement
        -- The claim currency will be the accrual currency and not the customer
        -- currency. As no cross currency associtaion is supported
        -- For a particular accrual in X currerncy to be associated
        -- claim must also be in X currency
        --
        l_claim_rec.currency_code   := l_funds_util_flt.utiz_currency_code;
        l_claim_rec.cust_account_id := l_funds_util_flt.cust_account_id; --p_customer_info.cust_account_id;
        --
        l_claim_rec.cust_billto_acct_site_id :=l_funds_util_flt.bill_to_site_use_id;
        l_claim_rec.vendor_id                :=p_customer_info.vendor_id;
        l_claim_rec.vendor_site_id           :=p_customer_info.vendor_site_id;
        --
        l_claim_rec.attribute_category       :=p_org_id;
        l_claim_rec.attribute1               :=l_party_id; --BU ID
        l_claim_rec.attribute2               :=l_party_name; --BU NAME
        --
        l_claim_rec.attribute4               :=xxcus_ozf_invoice_pkg.g_cal_year;   --ESMS 240298, add calendar year for reporting purpose.--8
        l_claim_rec.attribute6              :=l_offer_code; --ESMS 240298, add offer code --9
        l_claim_rec.attribute7              :=get_lob_name(l_claim_rec.attribute1); --ESMS 240298, Pass the bu party_id and get the lob name. --10
        l_claim_rec.cust_billto_acct_site_id :=l_funds_util_flt.bill_to_site_use_id;
        /*
        fnd_file.put_line(fnd_file.log
                         ,'l_claim_rec.attribute_category' ||
                          l_claim_rec.attribute_category);
        fnd_file.put_line(fnd_file.log
                         ,'l_claim_rec.attribute1' ||
                          l_claim_rec.attribute1);
        fnd_file.put_line(fnd_file.log
                         ,'l_claim_rec.attribute2' ||
                          l_claim_rec.attribute2);

        fnd_file.put_line(fnd_file.log
                         ,'l_claim_rec.currency_code' ||
                          l_claim_rec.currency_code);
        fnd_file.put_line(fnd_file.log
                         ,'l_claim_rec.cust_account_id' ||
                          l_claim_rec.cust_account_id);
        fnd_file.put_line(fnd_file.log
                         ,'l_claim_rec.vendor_id' || l_claim_rec.vendor_id);
        fnd_file.put_line(fnd_file.log
                         ,'l_claim_rec.vendor_site_id' ||
                          l_claim_rec.vendor_site_id);
        */
        l_claim_rec.created_from := 'AUTOPAY';
        --
        l_funds_util_flt.offer_payment_method := p_pmt_method; --NULL ;
        --
        fnd_file.put_line(fnd_file.log ,'Before Call to Create_Claim_For_Accruals');
        --
        create_claim_for_accruals(p_api_version      => 1.0
                                 ,p_org_id           => p_org_id
                                 ,p_init_msg_list    => fnd_api.g_false
                                 ,p_commit           => fnd_api.g_false
                                 ,p_validation_level => fnd_api.g_valid_level_full
                                 ,x_return_status    => l_return_status
                                 ,x_msg_count        => l_msg_count
                                 ,x_msg_data         => l_msg_data
                                 ,p_pmt_frequency    => p_pmt_frequency
                                 ,p_pmt_method       => p_pmt_method
                                 ,p_invoice_type     => p_invoice_type
                                 ,p_claim_rec        => l_claim_rec
                                 ,p_funds_util_flt   => l_funds_util_flt
                                 ,x_claim_id         => l_claim_id);
        --
        fnd_file.put_line(fnd_file.log,'After Call to Create_Claim_For_Accruals');
        --
        IF l_return_status <> fnd_api.g_ret_sts_success THEN
          x_return_status := fnd_api.g_ret_sts_error;
          RETURN;
        END IF;
        --
        IF l_claim_id IS NOT NULL THEN
          -- update to settle the claim
          OPEN claim_info_csr(l_claim_id);
          FETCH claim_info_csr
            INTO l_object_version_number, l_sales_rep_id;
          CLOSE claim_info_csr;

          l_claim_settle_rec.claim_id              := l_claim_id;
          l_claim_settle_rec.object_version_number := l_object_version_number;
          l_claim_settle_rec.user_status_id        := to_number(ozf_utility_pvt.get_default_user_status(p_status_type => g_claim_status
                                                                                                       ,p_status_code => g_closed_status));
          ------------------------------------------------------
          -- Sales Credit
          --   Bug 2950241 fixing: default Sales Rep in Claims
          --   if "Requires Salesperson" in AR system options.
          ------------------------------------------------------
          IF l_sales_rep_id IS NULL
          THEN
            OPEN csr_ar_system_options;
            FETCH csr_ar_system_options
              INTO l_salesrep_req_flag;
            CLOSE csr_ar_system_options;

            IF l_salesrep_req_flag = 'Y'
            THEN
              l_claim_settle_rec.sales_rep_id := -3; -- No Sales Credit
            END IF;
          END IF;

          ozf_claim_pvt.update_claim(p_api_version           => 1.0
                                    ,p_init_msg_list         => fnd_api.g_false
                                    ,p_commit                => fnd_api.g_false
                                    ,p_validation_level      => fnd_api.g_valid_level_full
                                    ,x_return_status         => l_return_status
                                    ,x_msg_count             => l_msg_count
                                    ,x_msg_data              => l_msg_data
                                    ,p_claim                 => l_claim_settle_rec
                                    ,p_event                 => 'UPDATE'
                                    ,p_mode                  => ozf_claim_utility_pvt.g_auto_mode
                                    ,x_object_version_number => l_object_version_number);

          IF l_return_status <> fnd_api.g_ret_sts_success
          THEN
            x_return_status := fnd_api.g_ret_sts_error;
            RETURN;
          END IF;
        END IF;

      ELSE
        IF fnd_msg_pub.check_msg_level(fnd_msg_pub.g_msg_lvl_error)
        THEN
          OPEN csr_cust_name(l_funds_util_flt.cust_account_id); --p_customer_info.cust_account_id);
          FETCH csr_cust_name
            INTO l_cust_name_num;
          CLOSE csr_cust_name;

          fnd_message.set_name('OZF', 'OZF_CLAIM_ATPY_CUST_INELIG');
          fnd_message.set_token('ID', l_cust_name_num);
          fnd_msg_pub.add;
        END IF;
        RAISE fnd_api.g_exc_unexpected_error;
      END IF;
    ELSE
      IF fnd_msg_pub.check_msg_level(fnd_msg_pub.g_msg_lvl_error)
      THEN
        OPEN csr_cust_name(l_funds_util_flt.cust_account_id); --p_customer_info.cust_account_id);
        FETCH csr_cust_name
          INTO l_cust_name_num;
        CLOSE csr_cust_name;

        fnd_message.set_name('OZF', 'OZF_CLAIM_ATPY_AMT_SMALL');
        fnd_message.set_token('ID', l_cust_name_num);
        fnd_msg_pub.add;
      END IF;
      RAISE fnd_api.g_exc_unexpected_error;
    END IF;

    OPEN csr_claim_num(l_claim_id);
    FETCH csr_claim_num
      INTO l_claim_num, l_claim_amt, l_cust_billto_acct_site_id;
    CLOSE csr_claim_num;

    fnd_file.put_line(fnd_file.log, 'Claim Number: ' || l_claim_num);
    fnd_file.put_line(fnd_file.log, 'Claim Amount: ' || l_claim_amt);

    IF l_cust_billto_acct_site_id IS NOT NULL OR
       l_cust_billto_acct_site_id <> 0
    THEN

      fnd_file.put_line(fnd_file.log
                       ,'The claim is created for bill_to site: ' ||
                        l_cust_billto_acct_site_id);
      fnd_file.put_line(fnd_file.output
                       ,'The claim is created for bill_to site: ' ||
                        l_cust_billto_acct_site_id);

    END IF;

    fnd_file.put_line(fnd_file.output
                     ,'Claim Number               : ' || l_claim_num);
    fnd_file.put_line(fnd_file.output
                     ,'Claim Amount               : ' || l_claim_amt);

  EXCEPTION
    WHEN OTHERS THEN
      x_return_status := fnd_api.g_ret_sts_error;
  END create_claim_for_cust;

  ---------------------------------------------------------------------
  -- PROCEDURE
  --    Create_Claim_for_BD_Offer
  --
  -- PURPOSE
  --    Create a claim for a backdated offer.
  --
  -- PARAMETERS
  --    p_offer_tbl : list of offers info that a claim will be created on.
  --
  ---------------------------------------------------------------------
  /* PROCEDURE Create_Claim_for_BD_Offer(p_api_version      IN NUMBER,
                                      p_init_msg_list    IN VARCHAR2 := FND_API.G_FALSE,
                                      p_commit           IN VARCHAR2 := FND_API.G_FALSE,
                                      p_validation_level IN NUMBER := FND_API.G_VALID_LEVEL_FULL,
                                      x_return_status    OUT NOCOPY VARCHAR2,
                                      x_msg_data         OUT NOCOPY VARCHAR2,
                                      x_msg_count        OUT NOCOPY NUMBER,
                                      p_offer_tbl        IN offer_tbl_type) IS
     l_return_status varchar2(1);
     l_msg_data      varchar2(2000);
     l_msg_count     number;

     l_api_name    CONSTANT VARCHAR2(30) := 'Create_Claim_for_BD_Offer';
     l_api_version CONSTANT NUMBER := 1.0;
     l_full_name   CONSTANT VARCHAR2(60) := G_PKG_NAME || '.' ||
                                            l_api_name;

     CURSOR sys_parameter_info_csr IS
        SELECT autopay_flag,
               autopay_reason_code_id,
               autopay_claim_type_id,
               autopay_periodicity,
               autopay_periodicity_type
          FROM apps.ozf_sys_parameters;

     l_autopay_flag             varchar2(1);
     l_auto_reason_code_id      number;
     l_auto_claim_type_id       number;
     l_autopay_periodicity      number;
     l_autopay_periodicity_type VARCHAR2(30);
     l_cust_account_id          number;
     l_amount                   number;

     l_inv_frequency            varchar2(30);
     l_inv_category             varchar2(30);

     CURSOR settlement_method_CSR(p_id in number) is
        select settlement_code
          from apps.ozf_offer_adjustments_b
         where list_header_id = p_id;

     l_customer_info  g_customer_info_csr%rowtype;
     l_funds_util_flt OZF_CLAIM_ACCRUAL_PVT.funds_util_flt_type := NULL;

  BEGIN

     SAVEPOINT BDOffer;

     -- Debug Message
     IF OZF_DEBUG_LOW_ON THEN
        FND_MESSAGE.Set_Name('OZF', 'OZF_API_DEBUG_MESSAGE');
        FND_MESSAGE.Set_Token('TEXT', l_full_name || ': Start');
        FND_MSG_PUB.Add;
     END IF;

     -- get autopay_flag, reason_code_id
     OPEN sys_parameter_info_csr;
     FETCH sys_parameter_info_csr
        INTO l_autopay_flag,
             l_auto_reason_code_id,
             l_auto_claim_type_id,
             l_autopay_periodicity,
             l_autopay_periodicity_type;
     CLOSE sys_parameter_info_csr;

     -- check reason_code and claim_type from sys_parameters.
     IF l_auto_reason_code_id is NULL THEN
        IF FND_MSG_PUB.check_msg_level(FND_MSG_PUB.g_msg_lvl_error) THEN
           FND_MESSAGE.set_name('OZF', 'OZF_CLAIM_REASON_CD_MISSING');
           FND_MSG_PUB.add;
        END IF;
        RAISE FND_API.g_exc_unexpected_error;
     END IF;

     IF l_auto_claim_type_id is NULL THEN
        IF FND_MSG_PUB.check_msg_level(FND_MSG_PUB.g_msg_lvl_error) THEN
           FND_MESSAGE.set_name('OZF', 'OZF_CLAIM_CLAIM_TYPE_MISSING');
           FND_MSG_PUB.add;
        END IF;
        RAISE FND_API.g_exc_unexpected_error;
     END IF;

     -- Loop through p_offer table
     For i in p_offer_tbl.FIRST .. p_offer_tbl.COUNT LOOP
        IF l_cust_account_id is not null THEN
           -- Get customer information
           OPEN g_customer_info_csr(l_cust_account_id);
           FETCH g_customer_info_csr
              into l_customer_info;
           CLOSE g_customer_info_csr;

           Validate_Customer_Info(p_customer_info => l_customer_info,
                                  x_return_status => l_return_status);

           -- skip this customer if we can not get all the info.
           IF l_return_status = FND_API.g_ret_sts_error or
              l_return_status = FND_API.g_ret_sts_unexp_error THEN
              RAISE FND_API.g_exc_unexpected_error;
              --goto end_loop;
           END IF;

           -- But, we need to overwrite the payment method for the customer;
           OPEN settlement_method_CSR(p_offer_tbl(i).offer_id);
           FETCH settlement_method_CSR
              into l_customer_info.payment_method;
           CLOSE settlement_method_CSR;

           l_funds_util_flt                    := null;
           l_funds_util_flt.activity_id        := p_offer_tbl(i).offer_id;
           l_funds_util_flt.activity_type      := G_OFFER_TYPE;
           l_funds_util_flt.adjustment_type_id := p_offer_tbl(i)
                                                  .adjustment_type_id;

           Create_Claim_for_Cust(p_customer_info            => l_customer_info,
                                 p_amount                   => p_offer_tbl(i).amount,
                                 p_mode                     => 'B',
                                 p_auto_reason_code_id      => l_auto_reason_code_id,
                                 p_auto_claim_type_id       => l_auto_claim_type_id,
                                 p_inv_frequency            => l_inv_frequency,
                                 p_inv_category             => l_inv_category,
                                 p_offer_payment_method     => null,
                                 p_funds_util_flt           => l_funds_util_flt,
                                 x_return_status            => l_return_status);
           IF l_return_status = FND_API.g_ret_sts_error THEN
              RAISE FND_API.g_exc_error;
           ELSIF l_return_status = FND_API.g_ret_sts_unexp_error THEN
              RAISE FND_API.g_exc_unexpected_error;
           END IF;
        END IF;
     END LOOP;

     -- Debug Message
     IF OZF_DEBUG_LOW_ON THEN
        FND_MESSAGE.Set_Name('OZF', 'OZF_API_DEBUG_MESSAGE');
        FND_MESSAGE.Set_Token('TEXT', l_full_name || ': End');
        FND_MSG_PUB.Add;
     END IF;

  EXCEPTION
     WHEN FND_API.g_exc_error THEN
        ROLLBACK TO BDOffer;
        FND_MSG_PUB.count_and_get(p_encoded => FND_API.g_false,
                                  p_count   => l_msg_count,
                                  p_data    => l_msg_data);
     WHEN FND_API.g_exc_unexpected_error THEN
        ROLLBACK TO BDOffer;
        FND_MSG_PUB.count_and_get(p_encoded => FND_API.g_false,
                                  p_count   => l_msg_count,
                                  p_data    => l_msg_data);
     WHEN OTHERS THEN
        ROLLBACK TO BDOffer;
        FND_MSG_PUB.count_and_get(p_encoded => FND_API.g_false,
                                  p_count   => l_msg_count,
                                  p_data    => l_msg_data);
  END Create_claim_for_bd_offer; */

  --------------------------------------------------------------------------------
  --    API name   : Start_Autopay
  --    Type       : Public
  --    Pre-reqs   : None
  --    Function   :
  --    Parameters :
  --
  --    IN         : p_customer_id                    IN NUMBER    Optional
  --               : p_fund_id                        IN NUMBER    Optional
  --               : p_plan_id                        IN NUMBER    Optional
  --               : p_end_date                       IN VARCHAR2  Optional
  --               : p_org_id                         IN NUMBER    Optional
  --

  --
  --   Note: This program automatically creates a claim for a set of customers
  --   The customer set is selected based on the input paramter. Also, we will pay a cusomter:
  --       if a customer utiliztion amount summation is greater than his threshold_amount
  --   or if the current date passes last_paid_date + threshold period.
  --   End of Comments
  -- -----------------------------------------------------------------------------
  --   VERSION DATE          AUTHOR(S)          DESCRIPTION
  -- -------   -----------   ---------------    ---------------------------------
  --    1.0                                     Created
  --    1.1    20-Jun-2013   Kathy Poling       SR 208563 - Issue #308 TM Claims are
  --                                            being duplicated.
  --   1.2    03/20/2014    Balaguru Seshadri  Add p_cal_year parameter to the routine start_autopay
  --                                           and to the ref cursor query claim_csr
  --------------------------------------------------------------------------------
  PROCEDURE start_autopay(
                          errbuf          OUT NOCOPY VARCHAR2
                         ,retcode         OUT NOCOPY NUMBER
                         ,p_org_id        IN NUMBER DEFAULT NULL
                         ,p_customer_id   IN NUMBER := NULL
                         ,p_plan_id       IN NUMBER
                         ,p_pmt_frequency IN VARCHAR2 := NULL
                         ,p_pmt_method    IN VARCHAR2 := NULL
                         ,p_threshold_val IN NUMBER
                         ,p_invoice_type  IN VARCHAR2
                         ,p_cal_year      IN VARCHAR2 --ESMS 240298
                         ) IS
    l_return_status VARCHAR2(1);
    l_msg_data      VARCHAR2(2000);
    l_msg_count     NUMBER;

    l_api_name    CONSTANT VARCHAR2(30) := 'Start_Autopay';
    l_api_version CONSTANT NUMBER := 1.0;
    l_full_name   CONSTANT VARCHAR2(60) := g_pkg_name || '.' || l_api_name;

    l_autopay_flag             VARCHAR2(1);
    l_auto_reason_code_id      NUMBER;
    l_auto_claim_type_id       NUMBER;
    l_autopay_periodicity      NUMBER;
    l_autopay_periodicity_type VARCHAR2(30);
    l_run_mode                 VARCHAR2(80);
    l_pay_to                   VARCHAR2(80);
    l_rel_cust_name            VARCHAR2(70);
    l_cust_name                VARCHAR2(70);
    l_rlship                   VARCHAR2(80);
    l_fund_name                VARCHAR2(240);
    l_plan_type_name           VARCHAR2(240);
    l_plan_name                VARCHAR2(240);
    l_prod_cat_name            VARCHAR2(240);
    l_prod_name                VARCHAR2(240);
    l_buy_gp_name              VARCHAR2(70);
    l_members_flag             VARCHAR2(80);
    l_offer_type_name          VARCHAR2(80);
    l_invoice_type             VARCHAR2(80) := p_invoice_type;

    l_bill_to_site_use_id NUMBER;
    l_prev_site_use_id    NUMBER;

    TYPE claim_amt_csr1 IS REF CURSOR;
    claim_csr claim_amt_csr1;

    CURSOR csr_meaning(lkup_type IN VARCHAR2, lkup_code IN VARCHAR2) IS
      SELECT meaning
        FROM ozf_lookups
       WHERE lookup_type = lkup_type
         AND lookup_code = lkup_code;

    CURSOR csr_fund_name(p_fund_id IN NUMBER) IS
      SELECT f.short_name
        FROM apps.ozf_funds_vl f
       WHERE f.fund_id = p_fund_id;

    CURSOR csr_offer_name(off_id IN NUMBER) IS
      SELECT qp.description
        FROM apps.qp_list_headers_vl qp
       WHERE 1 =1
         AND qp.list_header_id =off_id;
         --AND qp.attribute7     =p_cal_year; --ESMS 240298

    CURSOR csr_cust_name(cv_cust_account_id IN NUMBER) IS
      SELECT concat(concat(party.party_name, ' (')
                   ,concat(ca.account_number, ') '))
        FROM apps.hz_cust_accounts ca, apps.hz_parties party
       WHERE ca.party_id = party.party_id
         AND ca.cust_account_id = cv_cust_account_id;

    CURSOR csr_get_party_id(cv_cust_account_id IN NUMBER) IS
      SELECT party_id
        FROM apps.hz_cust_accounts
       WHERE cust_account_id = cv_cust_account_id;

    CURSOR csr_party_name(cv_party_id IN NUMBER) IS
      SELECT party_name FROM apps.hz_parties WHERE party_id = cv_party_id;

    CURSOR csr_offer_pay_name(cv_payment_method IN VARCHAR2) IS
      SELECT meaning
        FROM apps.ozf_lookups
       WHERE lookup_type = 'OZF_AUTOPAY_METHOD'
         AND lookup_code = cv_payment_method;

    CURSOR csr_get_autopay_flag(cv_offer_id IN NUMBER) IS
      SELECT qpb.attribute3
        FROM apps.qp_list_headers_b qpb
       WHERE 1 =1
         AND qpb.list_header_id =cv_offer_id;
         --AND qpb.attribute7     =p_cal_year; --ESMS 240298;

    --Multiorg Changes
    CURSOR operating_unit_csr IS
      SELECT ou.organization_id org_id
        FROM apps.hr_operating_units ou
       WHERE mo_global.check_access(ou.organization_id) = 'Y';

    CURSOR sys_parameter_info_csr(cv_org_id IN NUMBER) IS
      SELECT reason_code_id
        FROM apps.ozf_sys_parameters_all -- Changed by NS from apps.ozf_sys_parameters
       WHERE org_id = cv_org_id;

    CURSOR get_claim_type_id_csr(cv_payment_method IN VARCHAR2
                                ,cv_org_id         IN NUMBER) IS
      SELECT ctb.claim_type_id
        FROM ozf_claim_types_all_vl ctb
       WHERE ctb.org_id = cv_org_id
         AND ctb.attribute1 = cv_payment_method;

    CURSOR csr_bill_site_use_id(p_cust_account_id IN NUMBER
                               ,p_org_id          IN NUMBER) IS
      SELECT hcsu.site_use_id
        FROM hz_cust_site_uses_all  hcsu
            ,hz_cust_acct_sites_all hcas
            ,hz_cust_accounts       hca
       WHERE hca.cust_account_id = p_cust_account_id
         AND hcsu.org_id = p_org_id
         AND hca.cust_account_id = hcas.cust_account_id
         AND hcas.cust_acct_site_id = hcsu.cust_acct_site_id
         AND hcsu.status = 'A'
         AND hcsu.site_use_code = 'BILL_TO'
         AND hcsu.primary_flag = 'Y'
         AND rownum = 1;

    l_address1      VARCHAR2(50);
    l_party_site_id NUMBER;
    l_site_use_id   NUMBER;
    --
    TYPE empcurtype IS REF CURSOR;
    l_emp_csr NUMBER; --EmpCurType;

    l_stmt           VARCHAR2(3000);
    l_funds_util_flt funds_util_flt_type := NULL;

    l_cust_account_id      NUMBER;
    l_amount               NUMBER;
    l_customer_info        g_customer_info_csr%ROWTYPE;
    l_cust_name_num        VARCHAR2(70);
    l_offer_pay_method     VARCHAR2(30);
    l_offer_pay_name       VARCHAR2(80);
    l_party_id             NUMBER;
    l_trade_prf_exist      BOOLEAN := FALSE;
    l_ignore               NUMBER;
    --
    l_accrual_class        VARCHAR2(30);
    v_message              VARCHAR2(250);
    l_message              VARCHAR2(150);
    l_err_msg              VARCHAR2(3000);
    l_err_code             NUMBER;
    l_sec                  VARCHAR2(500);
    l_cust_info_invalid    BOOLEAN := FALSE;
    l_prev_cust_account_id NUMBER;
    l_utiz_currency        VARCHAR2(15);
    l_err_callfrom         VARCHAR2(75) DEFAULT 'XXCUSOZF_INVOICE_PKG';
    l_err_callpoint        VARCHAR2(75) DEFAULT 'START';
    l_distro_list          VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
    l_req_id               NUMBER NULL;
    --
    TYPE trd_prf_tbl_type IS TABLE OF NUMBER INDEX BY BINARY_INTEGER;
    l_trd_prof_tbl trd_prf_tbl_type;
    i              BINARY_INTEGER := 1;
    --
    l_plan_id NUMBER;
    l_fund_id NUMBER; -- HUMOZF add
    --Multiorg Changes
    m        NUMBER := 0;
    l_org_id ozf_utility_pvt.operating_units_tbl;
    --
    l_end_date       DATE;
    l_payment_method VARCHAR2(30);
    --
  BEGIN

    SAVEPOINT autopay;

    l_req_id := fnd_global.conc_request_id;

    fnd_file.put_line(fnd_file.output
                     ,'*-------------------- XXCUS: Rebate Invoice Generation Execution Report --------------------*');
    fnd_file.put_line(fnd_file.output
                     ,'Execution Starts On: ' ||
                      to_char(SYSDATE, 'MM-DD-YYYY HH24:MI:SS'));
    fnd_file.put_line(fnd_file.output
                     ,'*---------------------------------------------------------------------------------------------*');
    v_message := 'Running Customer Interface';
    l_message := v_message;
    -- Debug Message
    IF ozf_debug_low_on
    THEN
      fnd_message.set_name('OZF', 'OZF_API_DEBUG_MESSAGE');
      fnd_message.set_token('TEXT', l_full_name || ': Start');
      fnd_msg_pub.add;
    END IF;

    --Multiorg Changes
    mo_global.init('OZF');

    IF p_org_id IS NULL
    THEN
      mo_global.set_policy_context('M', NULL);
      OPEN operating_unit_csr;
      LOOP
        FETCH operating_unit_csr
          INTO l_org_id(m);
        m := m + 1;
        EXIT WHEN operating_unit_csr%NOTFOUND;
      END LOOP;
      CLOSE operating_unit_csr;
    ELSE
      l_org_id(m) := p_org_id;
    END IF;

    --Multiorg Changes
    IF (l_org_id.count > 0)
    THEN
      FOR m IN l_org_id.first .. l_org_id.last
      LOOP
        BEGIN
          mo_global.set_policy_context('S', l_org_id(m));
          -- Write OU info to OUT file
          fnd_file.put_line(fnd_file.output
                           ,'Operating Unit: ' ||
                            mo_global.get_ou_name(l_org_id(m)));
          fnd_file.put_line(fnd_file.output
                           ,'-----------------------------------------------------');
          -- Write OU info to LOG file
          fnd_file.put_line(fnd_file.log
                           ,'Operating Unit: ' ||
                            mo_global.get_ou_name(l_org_id(m)));
          fnd_file.put_line(fnd_file.log
                           ,'-----------------------------------------------------');

          IF p_customer_id IS NOT NULL
          THEN
            OPEN csr_cust_name(p_customer_id);
            FETCH csr_cust_name
              INTO l_cust_name;
            CLOSE csr_cust_name;
          END IF;

          /* IF p_fund_id IS NOT NULL THEN
             OPEN csr_fund_name(p_fund_id);
             FETCH csr_fund_name
                INTO l_fund_name;
             CLOSE csr_fund_name;
          END IF;*/

          IF p_plan_id IS NOT NULL
          THEN
            OPEN csr_offer_name(p_plan_id);
            FETCH csr_offer_name
              INTO l_plan_name;
            CLOSE csr_offer_name;
          END IF;

          --FND_FILE.PUT_LINE(FND_FILE.OUTPUT, rpad('Run Mode', 40, ' ') || ': ' || l_run_mode);
          fnd_file.put_line(fnd_file.output
                           ,rpad('Customer  Name', 40, ' ') || ': ' ||
                            l_cust_name);
          fnd_file.put_line(fnd_file.output
                           ,rpad('Contract  Name', 40, ' ') || ': ' ||
                            l_fund_name);
          fnd_file.put_line(fnd_file.output
                           ,rpad('Agreement Name', 40, ' ') || ': ' ||
                            l_plan_name);
          fnd_file.put_line(fnd_file.output
                           ,rpad('Payment    Frequency', 40, ' ') || ': ' ||
                            p_pmt_frequency);
          fnd_file.put_line(fnd_file.output
                           ,rpad('Payment Method', 40, ' ') || ': ' ||
                            p_pmt_method);
          --FND_FILE.PUT_LINE(FND_FILE.OUTPUT, rpad('Group By Offer', 40, ' ') || ': ' || p_group_by_offer);
          fnd_file.put_line(fnd_file.output
                           ,'*---------------------------------------------------------------------------------------------*');

          /*------------ Autopay starts ---------------*/

          ------------Get the Autopay Flag----------

          OPEN sys_parameter_info_csr(p_org_id);
          FETCH sys_parameter_info_csr
            INTO l_auto_reason_code_id;
          CLOSE sys_parameter_info_csr;

          -- check reason_code and claim_type from sys_parameters.
          IF l_auto_reason_code_id IS NULL
          THEN
            IF fnd_msg_pub.check_msg_level(fnd_msg_pub.g_msg_lvl_error)
            THEN
              fnd_message.set_name('OZF', 'OZF_CLAIM_REASON_CD_MISSING');
              fnd_msg_pub.add;
            END IF;
            RAISE fnd_api.g_exc_unexpected_error;
          END IF;
          ------------Get the Autopay Flag----------
          OPEN csr_get_autopay_flag(p_plan_id);
          FETCH csr_get_autopay_flag
            INTO l_autopay_flag;
          CLOSE csr_get_autopay_flag;

          ------------Get the Claim Type ID------

          /* IF l_auto_claim_type_id is NULL THEN
             IF FND_MSG_PUB.check_msg_level(FND_MSG_PUB.g_msg_lvl_error) THEN
                FND_MESSAGE.set_name('OZF', 'OZF_CLAIM_CLAIM_TYPE_MISSING');
                FND_MSG_PUB.add;
             END IF;
             RAISE FND_API.g_exc_unexpected_error;
          END IF; */

          -- get end date
          fnd_file.put_line(fnd_file.output
                           ,'Call to the Get End Date Function');

          IF p_pmt_frequency = 'MONTHLY'
          THEN
            get_end_date(p_frequency     => 'MONTH'
                        ,x_end_date      => l_end_date
                        ,x_return_status => l_return_status);
          END IF;

          IF p_pmt_frequency = 'QTRLY' OR p_pmt_frequency = 'SEMIANNUAL'
          THEN
            get_end_date(p_frequency     => 'QTR'
                        ,x_end_date      => l_end_date
                        ,x_return_status => l_return_status);
          END IF;

          IF p_pmt_frequency = 'ANNUAL'
          THEN
            get_end_date(p_frequency     => 'YEAR'
                        ,x_end_date      => l_end_date
                        ,x_return_status => l_return_status);
          END IF;

          printlog('p_pmt_frequency' || p_pmt_frequency);
          printlog('l_end_date' || l_end_date);
          printlog('l_autopay_flag' || l_autopay_flag); --ESMS 240298
          printlog('l_plan_name' || l_plan_name); --ESMS 240298
          printlog('End Call to the Get End Date Function');

          fnd_file.put_line(fnd_file.output
                           ,'End Call to the Get End Date Function');
          fnd_file.put_line(fnd_file.output, l_end_date);

          -- construct the following sql based on the inputs
          l_funds_util_flt.run_mode           := NULL; --p_run_mode;
          l_funds_util_flt.utilization_type   := NULL;
          l_funds_util_flt.offer_type         := NULL; --p_offer_type;
          l_funds_util_flt.activity_type      := 'OFFR'; --Added by NS --
          l_funds_util_flt.activity_id        := p_plan_id;
          l_funds_util_flt.fund_id            := NULL; --p_fund_id;
          l_funds_util_flt.adjustment_type_id := NULL;

          l_funds_util_flt.product_level_type := NULL;
          l_funds_util_flt.product_id         := NULL;

          -- additional filter conditions
          l_funds_util_flt.cust_account_id           := p_customer_id;
          l_funds_util_flt.relationship_type         := NULL; --p_relationship_type;
          l_funds_util_flt.related_cust_account_id   := NULL; --p_related_cust_account_id;
          l_funds_util_flt.buy_group_party_id        := NULL; --p_buy_group_party_id;
          l_funds_util_flt.select_cust_children_flag := NULL; --p_select_cust_children_flag;
          l_funds_util_flt.pay_to_customer           := NULL; --p_pay_to_customer;
          l_funds_util_flt.end_date                  := l_end_date; --FND_DATE.CANONICAL_TO_DATE(l_end_date);
          l_funds_util_flt.group_by_offer            := 'N'; --NVL(p_group_by_offer, 'N'); --R12

          fnd_file.put_line(fnd_file.output, l_funds_util_flt.end_date);

          l_funds_util_flt.autopay_check := 'AUTOPAY'; --R12

          -- log query for debugging
          IF ozf_debug_low_on
          THEN
            ozf_utility_pvt.write_conc_log;
          END IF;

          --l_accrual_class := 'ACCRUAL';

          IF p_invoice_type = 'INV'
          THEN

            printlog('Invoice');

            OPEN claim_csr FOR
              SELECT SUM(fu.amount_remaining) amount
                    ,fu.currency_code
                    ,orl.sold_from_cust_account_id
                    ,fu.plan_id
                    ,qpb.attribute5
                FROM apps.ozf_funds_utilized_all_b fu
                    ,apps.qp_list_headers_b qpb
                    ,apps.ozf_offers oof
                    ,(SELECT DISTINCT attribute5
                                     ,attribute6
                                     ,list_header_id
                        FROM apps.qp_qualifiers
                       WHERE qualifier_attribute = 'QUALIFIER_ATTRIBUTE2'
                         AND qualifier_context = 'SOLD_BY') qpq
                    ,apps.ozf_resale_lines_all orl
               WHERE fu.plan_type = 'OFFR'
                 AND fu.plan_id = qpb.list_header_id
                 AND fu.object_id = orl.resale_line_id
                 AND fu.cust_account_id = orl.sold_from_cust_account_id
                 AND qpb.list_header_id = qpq.list_header_id
                 AND oof.qp_list_header_id = qpb.list_header_id
                 AND qpb.attribute5 = nvl(p_pmt_method, qpb.attribute5)
                 AND qpb.attribute6 = nvl(p_pmt_frequency, qpb.attribute6)
                 AND qpb.attribute7 = nvl(p_cal_year, qpb.attribute7) --ESMS 240298
                 AND nvl(oof.autopay_flag, 'Y') = 'Y'
                 AND fu.plan_curr_amount_remaining <> 0
                 AND fu.utilization_type IN ('ACCRUAL', 'ADJUSTMENT')
                 AND fu.cust_account_id =
                     nvl(p_customer_id, fu.cust_account_id)
                 AND fu.plan_id = nvl(p_plan_id, fu.plan_id)
                 AND fu.gl_posted_flag = 'Y'
               GROUP BY fu.currency_code
                       ,orl.sold_from_cust_account_id
                       ,fu.plan_id
                       ,qpb.attribute5;

          ELSE
            printlog('Reimbursement');

            OPEN claim_csr FOR
              SELECT SUM(fu.amount_remaining) amount
                    ,fu.currency_code
                    ,orl.sold_from_cust_account_id
                    ,fu.plan_id
                    ,qpb.attribute5
                FROM apps.ozf_funds_utilized_all_b fu
                    ,apps.qp_list_headers_b qpb
                    ,apps.ozf_offers oof
                    ,(SELECT DISTINCT attribute5
                                     ,attribute6
                                     ,list_header_id
                        FROM apps.qp_qualifiers
                       WHERE qualifier_attribute = 'QUALIFIER_ATTRIBUTE2'
                         AND qualifier_context = 'SOLD_BY') qpq
                    ,apps.ozf_resale_lines_all orl
               WHERE fu.plan_type = 'OFFR'
                 AND fu.plan_id = qpb.list_header_id
                 AND fu.object_id = orl.resale_line_id
                 AND fu.cust_account_id = orl.sold_from_cust_account_id
                 AND qpb.list_header_id = qpq.list_header_id
                 AND oof.qp_list_header_id = qpb.list_header_id
                 AND qpb.attribute5 = nvl(p_pmt_method, qpb.attribute5)
                 AND qpb.attribute6 = nvl(p_pmt_frequency, qpb.attribute6)
                 --AND qpb.attribute7 = nvl(p_cal_year, qpb.attribute7) --ESMS 240298 --uncomment later when requested
                 AND nvl(oof.autopay_flag, 'Y') = 'Y'
                 AND fu.attribute10 --= 'REIMBURSEMENT'
                        IN ( SELECT LOOKUP_CODE
                             from apps.fnd_lookup_values_vl
                             WHERE     1 = 1
                             and lookup_type = 'XXCUS_OZF_NEG_INVOICE_TYPES')
                 AND fu.plan_curr_amount_remaining <> 0
                 AND fu.utilization_type IN ('ACCRUAL', 'ADJUSTMENT')
                 AND fu.cust_account_id =
                     nvl(p_customer_id, fu.cust_account_id)
                 AND fu.plan_id = nvl(p_plan_id, fu.plan_id)
                 AND fu.gl_posted_flag = 'Y'
               GROUP BY fu.currency_code
                       ,orl.sold_from_cust_account_id
                       ,fu.plan_id
                       ,qpb.attribute5;

          END IF;

          LOOP
            FETCH claim_csr
              INTO l_amount
                  ,l_utiz_currency
                  ,l_cust_account_id
                  ,l_plan_id
                  ,l_payment_method;

            EXIT WHEN claim_csr%NOTFOUND;

            fnd_file.put_line(fnd_file.log
                             ,rpad('Payment Method', 40, ' ') || ': ' ||
                              l_payment_method);
            --

            IF p_pmt_method IS NOT NULL
            THEN
              OPEN get_claim_type_id_csr(p_pmt_method, p_org_id);
              FETCH get_claim_type_id_csr
                INTO l_auto_claim_type_id;
              CLOSE get_claim_type_id_csr;
            END IF;

            IF p_pmt_method IS NULL THEN
              OPEN get_claim_type_id_csr(l_payment_method, p_org_id);
              FETCH get_claim_type_id_csr
                INTO l_auto_claim_type_id;
              CLOSE get_claim_type_id_csr;
            END IF;

            fnd_file.put_line(fnd_file.log
                             ,rpad('l_auto_claim_type_id', 40, ' ') || ': ' ||
                              l_auto_claim_type_id);

            fnd_file.put_line(fnd_file.log
                             ,rpad('l_amount', 40, ' ') || ': ' ||
                              l_amount);
            fnd_file.put_line(fnd_file.log
                             ,rpad('l_plan_id', 40, ' ') || ': ' ||
                              l_plan_id);
            fnd_file.put_line(fnd_file.log
                             ,rpad('l_utiz_currency', 40, ' ') || ': ' ||
                              l_utiz_currency);
            fnd_file.put_line(fnd_file.log
                             ,rpad('l_cust_account_id', 40, ' ') || ': ' ||
                              l_cust_account_id);
            fnd_msg_pub.initialize;

            l_bill_to_site_use_id := NULL;

            OPEN csr_bill_site_use_id(l_cust_account_id, p_org_id);
            FETCH csr_bill_site_use_id
              INTO l_bill_to_site_use_id;
            CLOSE csr_bill_site_use_id;

            l_funds_util_flt.activity_id        := l_plan_id;
            l_funds_util_flt.fund_id            := l_fund_id;
            l_funds_util_flt.cust_account_id    := l_cust_account_id;
            l_funds_util_flt.utiz_currency_code := l_utiz_currency;
            BEGIN
              SAVEPOINT autopay_cust;
              --IF (p_threshold_val < l_amount AND p_pmt_method NOT IN ('DEDUCTION')) THEN
              IF ((p_threshold_val > l_amount AND
                 nvl(p_pmt_method, l_payment_method) NOT IN ('DEDUCTION')) OR
                 (p_threshold_val < l_amount))
              THEN
                IF l_cust_account_id IS NOT NULL AND l_amount > 0
                THEN
                  IF l_prev_site_use_id IS NULL OR
                     l_bill_to_site_use_id <> l_prev_site_use_id
                  THEN
                    l_cust_name_num := NULL;
                    l_customer_info := NULL;
                    l_party_id      := NULL;

                    -- Get customer information for log message purpose
                    OPEN csr_cust_name(l_cust_account_id);
                    FETCH csr_cust_name
                      INTO l_cust_name_num;
                    CLOSE csr_cust_name;
                    fnd_file.put_line(fnd_file.log
                                     ,rpad('l_cust_account_id', 40, ' ') || ': ' ||
                                      l_cust_account_id);
                    fnd_file.put_line(fnd_file.log
                                     ,rpad('l_bill_to_site_use_id'
                                          ,40
                                          ,' ') || ': ' ||
                                      l_bill_to_site_use_id);
                    fnd_file.put_line(fnd_file.log
                                     ,rpad('l_fund_id', 40, ' ') || ': ' ||
                                      l_fund_id);
                    fnd_file.put_line(fnd_file.log
                                     ,rpad('l_plan_id', 40, ' ') || ': ' ||
                                      l_plan_id);
                    fnd_file.put_line(fnd_file.log
                                     ,rpad('l_amount', 40, ' ') || ': ' ||
                                      l_amount);

                  END IF;

                  OPEN g_customer_info_csr(l_cust_account_id);
                  FETCH g_customer_info_csr
                    INTO l_customer_info;
                  CLOSE g_customer_info_csr;
                  l_prev_site_use_id := l_bill_to_site_use_id;

                  IF p_customer_id IS NULL
                  THEN
                    fnd_file.put_line(fnd_file.output
                                     ,'Customer Name : ' ||
                                      l_cust_name_num);
                    fnd_file.put_line(fnd_file.output
                                     ,'Customer Name is null: ');
                  END IF;

                  l_funds_util_flt.bill_to_site_use_id := l_bill_to_site_use_id;
                  --FND_FILE.PUT_LINE(FND_FILE.LOG, 'l_funds_util_flt.bill_to_site_use_id' || l_funds_util_flt.bill_to_site_use_id);

                  --R12.1 enhancements

                  fnd_file.put_line(fnd_file.log
                                   ,'--- Start AUTOPAY for customer ' ||
                                    l_cust_name_num || ' ---');

                  --
                  fnd_file.put_line(fnd_file.log
                                   ,'Before Call to Create_Claim_for_Cust');
                  --
                  xxcus_ozf_invoice_pkg.g_cal_year :=p_cal_year; --ESMS 240298
                  --
                  create_claim_for_cust(p_customer_info       => l_customer_info
                                       ,p_org_id              => p_org_id
                                       ,p_amount              => l_amount
                                       ,p_mode                => 'N'
                                       ,p_auto_reason_code_id => l_auto_reason_code_id
                                       ,p_auto_claim_type_id  => l_auto_claim_type_id
                                       ,p_pmt_frequency       => p_pmt_frequency
                                       ,p_pmt_method          => p_pmt_method
                                       ,p_invoice_type        => p_invoice_type
                                       ,p_funds_util_flt      => l_funds_util_flt
                                       ,x_return_status       => l_return_status);

                  fnd_file.put_line(fnd_file.log
                                   ,'After Call to Create_Claim_for_Cust');
                  fnd_file.put_line(fnd_file.log
                                   ,'Return Status' || l_return_status);

                  IF l_return_status = fnd_api.g_ret_sts_error
                  THEN
                    RAISE fnd_api.g_exc_error;
                  ELSIF l_return_status = fnd_api.g_ret_sts_unexp_error
                  THEN
                    RAISE fnd_api.g_exc_unexpected_error;
                  END IF;

                  --FND_FILE.PUT_LINE(FND_FILE.LOG, 'Claim Amount = '||l_amount);
                  fnd_file.put_line(fnd_file.log, '===> Success.');
                  fnd_file.put_line(fnd_file.log, '--- End ---');
                  fnd_file.put_line(fnd_file.log, ' ');
                  fnd_file.put_line(fnd_file.output
                                   ,'Claim Amount               : ' ||
                                    l_amount);
                  fnd_file.put_line(fnd_file.output
                                   ,'Status : Auto Pay Success. ');
                  fnd_file.put_line(fnd_file.output, '');
                  ozf_utility_pvt.write_conc_log;
                  --

                END IF; -- end of if l_cust_account_id is not null

              END IF;

            EXCEPTION
              WHEN fnd_api.g_exc_error THEN

                l_err_code := 2;
                l_err_msg  := l_sec;
                l_err_msg  := l_err_msg || ' ERROR ' ||
                              substr(SQLERRM, 1, 2000);

                fnd_file.put_line(fnd_file.log, l_err_msg);
                fnd_file.put_line(fnd_file.output, l_err_msg);
                retcode         := l_err_code;
                errbuf          := l_err_msg;
                l_err_callpoint := l_message;

                xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                                    ,p_calling           => l_err_callpoint
                                                    ,p_request_id        => l_req_id
                                                    ,p_ora_error_msg     => SQLERRM
                                                    ,p_error_desc        => 'Error running XXCUSOZF_INVOICE_PKG.Start_Autopay package with PROGRAM ERROR'
                                                    ,p_distribution_list => l_distro_list
                                                    ,p_module            => 'TM');

                ROLLBACK TO autopay_cust;
                fnd_file.put_line(fnd_file.log, '===> Failed11.');
                ozf_utility_pvt.write_conc_log;
                fnd_file.put_line(fnd_file.log, '--- End ---');
                fnd_file.put_line(fnd_file.log, ' ');
                fnd_file.put_line(fnd_file.output
                                 ,'Status : Auto Pay Failed. ');
                fnd_file.put_line(fnd_file.output
                                 ,'Error : ' ||
                                  fnd_msg_pub.get(fnd_msg_pub.count_msg
                                                 ,fnd_api.g_false));
                fnd_file.put_line(fnd_file.output, '');
              WHEN fnd_api.g_exc_unexpected_error THEN
                ROLLBACK TO autopay_cust;
                fnd_file.put_line(fnd_file.log, '===> Failed.');
                ozf_utility_pvt.write_conc_log;
                fnd_file.put_line(fnd_file.log, '--- End ---');
                fnd_file.put_line(fnd_file.log, ' ');
                fnd_file.put_line(fnd_file.output
                                 ,'Status : Auto Pay Failed. ');
                fnd_file.put_line(fnd_file.output
                                 ,'Error : ' ||
                                  fnd_msg_pub.get(fnd_msg_pub.count_msg
                                                 ,fnd_api.g_false));
                fnd_file.put_line(fnd_file.output, '');

              WHEN OTHERS THEN
                l_err_code := 2;
                l_err_msg  := l_sec;
                l_err_msg  := l_err_msg || ' ERROR ' ||
                              substr(SQLERRM, 1, 2000);

                fnd_file.put_line(fnd_file.log, l_err_msg);
                fnd_file.put_line(fnd_file.output, l_err_msg);
                retcode         := l_err_code;
                errbuf          := l_err_msg;
                l_err_callpoint := l_message;

                xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                                    ,p_calling           => l_err_callpoint
                                                    ,p_request_id        => l_req_id
                                                    ,p_ora_error_msg     => SQLERRM
                                                    ,p_error_desc        => 'Error running XXCUSOZF_INVOICE_PKG.Start_Autopay package with PROGRAM ERROR'
                                                    ,p_distribution_list => l_distro_list
                                                    ,p_module            => 'TM');
                ROLLBACK TO autopay_cust;
                fnd_file.put_line(fnd_file.log, '===> Failed.');
                IF ozf_debug_low_on
                THEN
                  fnd_message.set_name('OZF', 'OZF_API_DEBUG_MESSAGE');
                  fnd_message.set_token('TEXT', SQLERRM);
                  fnd_msg_pub.add;
                END IF;
                ozf_utility_pvt.write_conc_log;
                fnd_file.put_line(fnd_file.log, '--- End ---');
                fnd_file.put_line(fnd_file.log, ' ');
                fnd_file.put_line(fnd_file.output
                                 ,'Status : Auto Pay Failed. ');
                fnd_file.put_line(fnd_file.output
                                 ,'Error : ' || SQLCODE || SQLERRM);
                fnd_file.put_line(fnd_file.output, '');
            END;
            --
          END LOOP;
          CLOSE claim_csr;

          -- Debug Message
          IF ozf_debug_low_on
          THEN
            fnd_message.set_name('OZF', 'OZF_API_DEBUG_MESSAGE');
            fnd_message.set_token('TEXT', l_full_name || ': End');
            fnd_msg_pub.add;
          END IF;

          -- Write all messages to a log
          ozf_utility_pvt.write_conc_log;

          fnd_file.put_line(fnd_file.output
                           ,'*---------------------------------------------------------------------------------------------*');
          fnd_file.put_line(fnd_file.output
                           ,'Execution Status: Successful');
          fnd_file.put_line(fnd_file.output
                           ,'Execution Ends On: ' ||
                            to_char(SYSDATE, 'MM-DD-YYYY HH24:MI:SS'));
          fnd_file.put_line(fnd_file.output
                           ,'*---------------------------------------------------------------------------------------------*');

        EXCEPTION
          WHEN fnd_api.g_exc_error THEN
            l_err_code := 2;
            l_err_msg  := l_sec;
            l_err_msg  := l_err_msg || ' ERROR ' ||
                          substr(SQLERRM, 1, 2000);

            fnd_file.put_line(fnd_file.log, l_err_msg);
            fnd_file.put_line(fnd_file.output, l_err_msg);
            retcode         := l_err_code;
            errbuf          := l_err_msg;
            l_err_callpoint := l_message;

            xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                                ,p_calling           => l_err_callpoint
                                                ,p_request_id        => l_req_id
                                                ,p_ora_error_msg     => SQLERRM
                                                ,p_error_desc        => 'Error running XXCUSOZF_INVOICE_PKG.Start_Autopay package with PROGRAM ERROR'
                                                ,p_distribution_list => l_distro_list
                                                ,p_module            => 'TM');
            ROLLBACK TO autopay;

            ozf_utility_pvt.write_conc_log;
            errbuf  := l_msg_data;
            retcode := 2;
            fnd_file.put_line(fnd_file.output
                             ,'Execution Status: Failure (Error:' ||
                              fnd_msg_pub.get(1, fnd_api.g_false) || ')');
            fnd_file.put_line(fnd_file.output
                             ,'Execution Ends On: ' ||
                              to_char(SYSDATE, 'MM-DD-YYYY HH24:MI:SS'));
            fnd_file.put_line(fnd_file.output
                             ,'*---------------------------------------------------------------------------------------------*');

          WHEN fnd_api.g_exc_unexpected_error THEN
            l_err_code := 2;
            l_err_msg  := l_sec;
            l_err_msg  := l_err_msg || ' ERROR ' ||
                          substr(SQLERRM, 1, 2000);

            fnd_file.put_line(fnd_file.log, l_err_msg);
            fnd_file.put_line(fnd_file.output, l_err_msg);
            retcode         := l_err_code;
            errbuf          := l_err_msg;
            l_err_callpoint := l_message;

            xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                                ,p_calling           => l_err_callpoint
                                                ,p_request_id        => l_req_id
                                                ,p_ora_error_msg     => SQLERRM
                                                ,p_error_desc        => 'Error running XXCUSOZF_INVOICE_PKG.Start_Autopay package with PROGRAM ERROR'
                                                ,p_distribution_list => l_distro_list
                                                ,p_module            => 'TM');
            ROLLBACK TO autopay;

            ozf_utility_pvt.write_conc_log;
            errbuf  := l_msg_data;
            retcode := 1; -- show status as warning if claim type/reason is missing,  Fix for 5158782
            fnd_file.put_line(fnd_file.output
                             ,'Execution Status: ( Warning:' ||
                              fnd_msg_pub.get(1, fnd_api.g_false) || ')');
            fnd_file.put_line(fnd_file.output
                             ,'Execution Ends On: ' ||
                              to_char(SYSDATE, 'MM-DD-YYYY HH24:MI:SS'));
            fnd_file.put_line(fnd_file.output
                             ,'*---------------------------------------------------------------------------------------------*');

          WHEN OTHERS THEN
            l_err_code := 2;
            l_err_msg  := l_sec;
            l_err_msg  := l_err_msg || ' ERROR ' ||
                          substr(SQLERRM, 1, 2000);

            fnd_file.put_line(fnd_file.log, l_err_msg);
            fnd_file.put_line(fnd_file.output, l_err_msg);
            retcode         := l_err_code;
            errbuf          := l_err_msg;
            l_err_callpoint := l_message;

            xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                                ,p_calling           => l_err_callpoint
                                                ,p_request_id        => l_req_id
                                                ,p_ora_error_msg     => SQLERRM
                                                ,p_error_desc        => 'Error running XXCUSOZF_INVOICE_PKG.Start_Autopay package with PROGRAM ERROR'
                                                ,p_distribution_list => l_distro_list
                                                ,p_module            => 'TM');
            ROLLBACK TO autopay;

            ozf_utility_pvt.write_conc_log;
            errbuf  := l_msg_data;
            retcode := 2;
            fnd_file.put_line(fnd_file.output
                             ,'Execution Status: Failure (Error:' ||
                              SQLCODE || SQLERRM || ')');
            fnd_file.put_line(fnd_file.output
                             ,'Execution Ends On: ' ||
                              to_char(SYSDATE, 'MM-DD-YYYY HH24:MI:SS'));
            fnd_file.put_line(fnd_file.output
                             ,'*---------------------------------------------------------------------------------------------*');

        END;
      END LOOP;
    END IF;
  END start_autopay;

  -- -----------------------------------------------------------------------------
  --   VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- -------   -----------   --------------- ---------------------------------
  --    1.0                                   Created
  --    1.1    12-Jul-2013   Kathy Poling     SR 208563 - Issue #308 TM Claims are
  --                                          being duplicated.
  --------------------------------------------------------------------------------

  PROCEDURE start_remburse(errbuf          OUT NOCOPY VARCHAR2
                          ,retcode         OUT NOCOPY NUMBER
                          ,p_org_id        IN NUMBER DEFAULT NULL
                          ,p_customer_id   IN NUMBER := NULL
                          ,p_plan_id       IN NUMBER
                          ,p_fund_id       IN NUMBER
                          ,p_pmt_frequency IN VARCHAR2
                          ,p_pmt_method    IN VARCHAR2
                          ,p_invoice_type  IN VARCHAR2) IS

    l_return_status VARCHAR2(1);
    l_msg_data      VARCHAR2(2000);
    l_msg_count     NUMBER;

    l_api_name    CONSTANT VARCHAR2(30) := 'Start_Autopay';
    l_api_version CONSTANT NUMBER := 1.0;
    l_full_name   CONSTANT VARCHAR2(60) := g_pkg_name || '.' || l_api_name;

    l_autopay_flag             VARCHAR2(1);
    l_auto_reason_code_id      NUMBER;
    l_auto_claim_type_id       NUMBER;
    l_autopay_periodicity      NUMBER;
    l_autopay_periodicity_type VARCHAR2(30);
    l_run_mode                 VARCHAR2(80);
    l_pay_to                   VARCHAR2(80);
    l_rel_cust_name            VARCHAR2(70);
    l_cust_name                VARCHAR2(70);
    l_rlship                   VARCHAR2(80);
    l_fund_name                VARCHAR2(240);
    l_plan_type_name           VARCHAR2(240);
    l_plan_name                VARCHAR2(240);
    l_prod_cat_name            VARCHAR2(240);
    l_prod_name                VARCHAR2(240);
    l_buy_gp_name              VARCHAR2(70);
    l_members_flag             VARCHAR2(80);
    l_offer_type_name          VARCHAR2(80);
    l_invoice_type             VARCHAR2(80) := p_invoice_type;

    l_bill_to_site_use_id NUMBER;
    l_prev_site_use_id    NUMBER;
    l_payment_freq        VARCHAR2(50);

    TYPE claim_amt_csr1 IS REF CURSOR;
    claim_csr claim_amt_csr1;

    CURSOR csr_meaning(lkup_type IN VARCHAR2, lkup_code IN VARCHAR2) IS
      SELECT meaning
        FROM ozf_lookups
       WHERE lookup_type = lkup_type
         AND lookup_code = lkup_code;

    CURSOR csr_fund_name(p_fund_id IN NUMBER) IS
      SELECT f.short_name
        FROM apps.ozf_funds_vl f
       WHERE f.fund_id = p_fund_id;

    CURSOR csr_offer_name(off_id IN NUMBER) IS
      SELECT qp.description
        FROM apps.qp_list_headers_vl qp
       WHERE qp.list_header_id = off_id;

    CURSOR csr_cust_name(cv_cust_account_id IN NUMBER) IS
      SELECT concat(concat(party.party_name, ' (')
                   ,concat(ca.account_number, ') '))
        FROM apps.hz_cust_accounts ca, apps.hz_parties party
       WHERE ca.party_id = party.party_id
         AND ca.cust_account_id = cv_cust_account_id;

    CURSOR csr_get_party_id(cv_cust_account_id IN NUMBER) IS
      SELECT party_id
        FROM apps.hz_cust_accounts
       WHERE cust_account_id = cv_cust_account_id;

    CURSOR csr_party_name(cv_party_id IN NUMBER) IS
      SELECT party_name FROM apps.hz_parties WHERE party_id = cv_party_id;

    CURSOR csr_offer_pay_name(cv_payment_method IN VARCHAR2) IS
      SELECT meaning
        FROM apps.ozf_lookups
       WHERE lookup_type = 'OZF_AUTOPAY_METHOD'
         AND lookup_code = cv_payment_method;

    CURSOR csr_get_autopay_flag(cv_offer_id IN NUMBER) IS
      SELECT qpb.attribute3
        FROM apps.qp_list_headers_b qpb
       WHERE qpb.list_header_id = cv_offer_id;

    --Multiorg Changes
    CURSOR operating_unit_csr IS
      SELECT ou.organization_id org_id
        FROM apps.hr_operating_units ou
       WHERE mo_global.check_access(ou.organization_id) = 'Y';

    CURSOR sys_parameter_info_csr(cv_org_id IN NUMBER) IS
      SELECT reason_code_id
        FROM apps.ozf_sys_parameters_all -- Changed by NS from apps.ozf_sys_parameters
       WHERE org_id = cv_org_id;

    CURSOR get_claim_type_id_csr(cv_payment_method IN VARCHAR2
                                ,cv_org_id         IN NUMBER) IS
      SELECT ctb.claim_type_id
        FROM ozf_claim_types_all_vl ctb
       WHERE ctb.org_id = cv_org_id
         AND ctb.attribute1 = cv_payment_method;

    CURSOR csr_bill_site_use_id(p_cust_account_id IN NUMBER
                               ,p_org_id          IN NUMBER) IS
      SELECT hcsu.site_use_id
        FROM hz_cust_site_uses_all  hcsu
            ,hz_cust_acct_sites_all hcas
            ,hz_cust_accounts       hca
       WHERE hca.cust_account_id = p_cust_account_id
         AND hcsu.org_id = p_org_id
         AND hca.cust_account_id = hcas.cust_account_id
         AND hcas.cust_acct_site_id = hcsu.cust_acct_site_id
         AND hcsu.status = 'A'
         AND hcsu.site_use_code = 'BILL_TO'
            --AND hcsu.primary_flag <>'Y'
         AND hcsu.primary_flag = 'Y'
         AND rownum = 1;

    l_address1      VARCHAR2(50);
    l_party_site_id NUMBER;
    l_site_use_id   NUMBER;

    /*SELECT hcsu.site_use_id
      FROM hz_cust_site_uses_all hcsu,
           hz_cust_acct_sites_all hcas,
           hz_cust_accounts hca
     WHERE hca.cust_account_id=p_cust_account_id
       AND hca.cust_account_id=hcas.cust_account_id
       AND hcas.cust_acct_site_id = hcsu.cust_acct_site_id
       AND hcsu.status = 'A'
       AND hcsu.site_use_code = 'BILL_TO'
       AND hcsu.primary_flag = 'Y';
    -- AND hcsu.attribute4 = p_inv_category;
    */

    TYPE empcurtype IS REF CURSOR;
    l_emp_csr NUMBER; --EmpCurType;

    l_stmt           VARCHAR2(3000);
    l_funds_util_flt funds_util_flt_type := NULL;

    l_cust_account_id      NUMBER;
    l_amount               NUMBER;
    l_customer_info        g_customer_info_csr%ROWTYPE;
    l_cust_name_num        VARCHAR2(70);
    l_offer_pay_method     VARCHAR2(30);
    l_offer_pay_name       VARCHAR2(80);
    l_party_id             NUMBER;
    l_trade_prf_exist      BOOLEAN := FALSE;
    l_ignore               NUMBER;
    l_accrual_class        VARCHAR2(30);
    v_message              VARCHAR2(250);
    l_message              VARCHAR2(150);
    l_err_msg              VARCHAR2(3000);
    l_err_code             NUMBER;
    l_sec                  VARCHAR2(500);
    l_cust_info_invalid    BOOLEAN := FALSE;
    l_prev_cust_account_id NUMBER;
    l_utiz_currency        VARCHAR2(15);
    l_err_callfrom         VARCHAR2(75) DEFAULT 'XXCUSOZF_INVOICE_PKG';
    l_err_callpoint        VARCHAR2(75) DEFAULT 'START';
    l_distro_list          VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
    l_req_id               NUMBER NULL;

    TYPE trd_prf_tbl_type IS TABLE OF NUMBER INDEX BY BINARY_INTEGER;
    l_trd_prof_tbl trd_prf_tbl_type;
    i              BINARY_INTEGER := 1;

    l_plan_id NUMBER;
    l_fund_id NUMBER; -- HUMOZF add

    --Multiorg Changes
    m        NUMBER := 0;
    l_org_id ozf_utility_pvt.operating_units_tbl;

    l_end_date       DATE;
    l_payment_method VARCHAR2(30);

  BEGIN

    SAVEPOINT autopay;

    l_req_id := fnd_global.conc_request_id;

    fnd_file.put_line(fnd_file.output
                     ,'*-------------------- XXCUS: Rebate Invoice Generation Execution Report --------------------*');
    fnd_file.put_line(fnd_file.output
                     ,'Execution Starts On: ' ||
                      to_char(SYSDATE, 'MM-DD-YYYY HH24:MI:SS'));
    fnd_file.put_line(fnd_file.output
                     ,'*---------------------------------------------------------------------------------------------*');
    v_message := 'Running Customer Interface';
    l_message := v_message;
    -- Debug Message
    IF ozf_debug_low_on
    THEN
      fnd_message.set_name('OZF', 'OZF_API_DEBUG_MESSAGE');
      fnd_message.set_token('TEXT', l_full_name || ': Start');
      fnd_msg_pub.add;
    END IF;

    --Multiorg Changes
    mo_global.init('OZF');

    IF p_org_id IS NULL
    THEN
      mo_global.set_policy_context('M', NULL);
      OPEN operating_unit_csr;
      LOOP
        FETCH operating_unit_csr
          INTO l_org_id(m);
        m := m + 1;
        EXIT WHEN operating_unit_csr%NOTFOUND;
      END LOOP;
      CLOSE operating_unit_csr;
    ELSE
      l_org_id(m) := p_org_id;
    END IF;

    --Multiorg Changes
    IF (l_org_id.count > 0)
    THEN
      FOR m IN l_org_id.first .. l_org_id.last
      LOOP
        BEGIN
          mo_global.set_policy_context('S', l_org_id(m));
          -- Write OU info to OUT file
          fnd_file.put_line(fnd_file.output
                           ,'Operating Unit: ' ||
                            mo_global.get_ou_name(l_org_id(m)));
          fnd_file.put_line(fnd_file.output
                           ,'-----------------------------------------------------');
          -- Write OU info to LOG file
          fnd_file.put_line(fnd_file.log
                           ,'Operating Unit: ' ||
                            mo_global.get_ou_name(l_org_id(m)));
          fnd_file.put_line(fnd_file.log
                           ,'-----------------------------------------------------');

          IF p_customer_id IS NOT NULL
          THEN
            OPEN csr_cust_name(p_customer_id);
            FETCH csr_cust_name
              INTO l_cust_name;
            CLOSE csr_cust_name;
          END IF;

          /* IF p_fund_id IS NOT NULL THEN
             OPEN csr_fund_name(p_fund_id);
             FETCH csr_fund_name
                INTO l_fund_name;
             CLOSE csr_fund_name;
          END IF;*/

          IF p_plan_id IS NOT NULL
          THEN
            OPEN csr_offer_name(p_plan_id);
            FETCH csr_offer_name
              INTO l_plan_name;
            CLOSE csr_offer_name;
          END IF;

          --FND_FILE.PUT_LINE(FND_FILE.OUTPUT, rpad('Run Mode', 40, ' ') || ': ' || l_run_mode);
          fnd_file.put_line(fnd_file.output
                           ,rpad('Customer  Name', 40, ' ') || ': ' ||
                            l_cust_name);
          fnd_file.put_line(fnd_file.output
                           ,rpad('Contract  Name', 40, ' ') || ': ' ||
                            l_fund_name);
          fnd_file.put_line(fnd_file.output
                           ,rpad('Agreement Name', 40, ' ') || ': ' ||
                            l_plan_name);
          fnd_file.put_line(fnd_file.output
                           ,rpad('Payment    Frequency', 40, ' ') || ': ' ||
                            p_pmt_frequency);
          fnd_file.put_line(fnd_file.output
                           ,rpad('Payment Method', 40, ' ') || ': ' ||
                            p_pmt_method);
          --FND_FILE.PUT_LINE(FND_FILE.OUTPUT, rpad('Group By Offer', 40, ' ') || ': ' || p_group_by_offer);
          fnd_file.put_line(fnd_file.output
                           ,'*---------------------------------------------------------------------------------------------*');

          /*------------ Autopay starts ---------------*/

          ------------Get the Autopay Flag----------

          OPEN sys_parameter_info_csr(p_org_id);
          FETCH sys_parameter_info_csr
            INTO l_auto_reason_code_id;
          CLOSE sys_parameter_info_csr;

          -- check reason_code and claim_type from sys_parameters.
          IF l_auto_reason_code_id IS NULL
          THEN
            IF fnd_msg_pub.check_msg_level(fnd_msg_pub.g_msg_lvl_error)
            THEN
              fnd_message.set_name('OZF', 'OZF_CLAIM_REASON_CD_MISSING');
              fnd_msg_pub.add;
            END IF;
            RAISE fnd_api.g_exc_unexpected_error;
          END IF;
          ------------Get the Autopay Flag----------
          OPEN csr_get_autopay_flag(p_plan_id);
          FETCH csr_get_autopay_flag
            INTO l_autopay_flag;
          CLOSE csr_get_autopay_flag;

          ------------Get the Claim Type ID------

          /* IF l_auto_claim_type_id is NULL THEN
             IF FND_MSG_PUB.check_msg_level(FND_MSG_PUB.g_msg_lvl_error) THEN
                FND_MESSAGE.set_name('OZF', 'OZF_CLAIM_CLAIM_TYPE_MISSING');
                FND_MSG_PUB.add;
             END IF;
             RAISE FND_API.g_exc_unexpected_error;
          END IF; */

          -- get end date
          fnd_file.put_line(fnd_file.output
                           ,'Call to the Get End Date Function');

          IF p_pmt_frequency = 'MONTHLY'
          THEN
            get_end_date(p_frequency     => 'MONTH'
                        ,x_end_date      => l_end_date
                        ,x_return_status => l_return_status);
          END IF;

          IF p_pmt_frequency = 'QTRLY' OR p_pmt_frequency = 'SEMIANNUAL'
          THEN
            get_end_date(p_frequency     => 'QTR'
                        ,x_end_date      => l_end_date
                        ,x_return_status => l_return_status);
          END IF;

          IF p_pmt_frequency = 'ANNUAL'
          THEN
            get_end_date(p_frequency     => 'YEAR'
                        ,x_end_date      => l_end_date
                        ,x_return_status => l_return_status);
          END IF;

          printlog('p_pmt_frequency' || p_pmt_frequency);
          printlog('l_end_date' || l_end_date);

          printlog('End Call to the Get End Date Function');

          fnd_file.put_line(fnd_file.output
                           ,'End Call to the Get End Date Function');
          fnd_file.put_line(fnd_file.output, l_end_date);

          -- construct the following sql based on the inputs
          l_funds_util_flt.run_mode           := NULL; --p_run_mode;
          l_funds_util_flt.utilization_type   := NULL;
          l_funds_util_flt.offer_type         := NULL; --p_offer_type;
          l_funds_util_flt.activity_type      := 'OFFR'; --Added by NS --
          l_funds_util_flt.activity_id        := p_plan_id;
          l_funds_util_flt.fund_id            := NULL; --p_fund_id;
          l_funds_util_flt.adjustment_type_id := NULL;

          l_funds_util_flt.product_level_type := NULL;
          l_funds_util_flt.product_id         := NULL;

          -- additional filter conditions
          l_funds_util_flt.cust_account_id           := p_customer_id;
          l_funds_util_flt.relationship_type         := NULL; --p_relationship_type;
          l_funds_util_flt.related_cust_account_id   := NULL; --p_related_cust_account_id;
          l_funds_util_flt.buy_group_party_id        := NULL; --p_buy_group_party_id;
          l_funds_util_flt.select_cust_children_flag := NULL; --p_select_cust_children_flag;
          l_funds_util_flt.pay_to_customer           := NULL; --p_pay_to_customer;
          l_funds_util_flt.end_date                  := l_end_date; --FND_DATE.CANONICAL_TO_DATE(l_end_date);
          l_funds_util_flt.group_by_offer            := 'N'; --NVL(p_group_by_offer, 'N'); --R12

          fnd_file.put_line(fnd_file.output, l_funds_util_flt.end_date);

          l_funds_util_flt.autopay_check := 'AUTOPAY'; --R12

          -- log query for debugging
          IF ozf_debug_low_on
          THEN
            ozf_utility_pvt.write_conc_log;
          END IF;

          --l_accrual_class := 'ACCRUAL';

          IF p_invoice_type = 'INV'
          THEN

            printlog('Invoice');

            OPEN claim_csr FOR
--Version 1.1 7/12/13
/*              SELECT SUM(fu.amount_remaining) amount
                    ,fu.currency_code
                    ,orl.sold_from_cust_account_id
                    ,fu.plan_id
                    ,qpb.attribute5
                FROM apps.ozf_funds_utilized_all_b fu
                    ,apps.qp_list_headers_b        qpb
                    ,apps.ozf_offers               oof
                    ,apps.qp_qualifiers            qpq
                    ,apps.ozf_resale_lines_all     orl
               WHERE fu.plan_type = 'OFFR'
                 AND fu.plan_id = qpb.list_header_id
                 AND fu.object_id = orl.resale_line_id
                 AND fu.cust_account_id = orl.sold_from_cust_account_id
                 AND qpb.list_header_id = qpq.list_header_id
                 AND oof.qp_list_header_id = qpb.list_header_id
                 AND qpb.attribute5 = nvl(p_pmt_method, qpb.attribute5)
                 AND qpb.attribute6 = nvl(p_pmt_frequency, qpb.attribute6)
                 AND nvl(oof.autopay_flag, 'Y') = 'Y'
                    -- AND fu.fund_id = nvl(v_fund_id, fu.fund_id)
                 AND fu.plan_curr_amount_remaining <> 0
                 AND qualifier_attribute = 'QUALIFIER_ATTRIBUTE2'
                 AND qualifier_context = 'SOLD_BY'
                 AND fu.utilization_type IN ('ACCRUAL', 'ADJUSTMENT')
                 AND fu.cust_account_id =
                     nvl(p_customer_id, fu.cust_account_id)
                    --AND fu.org_id= p_org_id
                 AND fu.plan_id = nvl(p_plan_id, fu.plan_id)
                 AND fu.gl_posted_flag = 'Y'
               GROUP BY fu.currency_code
                       ,orl.sold_from_cust_account_id
                       ,fu.plan_id
                       ,qpb.attribute5;
*/
SELECT SUM(fu.amount_remaining) amount
                    ,fu.currency_code
                    ,orl.sold_from_cust_account_id
                    ,fu.plan_id
                    ,qpb.attribute5
                FROM apps.ozf_funds_utilized_all_b fu
                    ,apps.qp_list_headers_b        qpb
                    ,apps.ozf_offers               oof
                    ,(SELECT DISTINCT attribute5
                                     ,attribute6
                                     ,list_header_id
                        FROM apps.qp_qualifiers
                       WHERE qualifier_attribute = 'QUALIFIER_ATTRIBUTE2'
                         AND qualifier_context = 'SOLD_BY') qpq
                    ,apps.ozf_resale_lines_all     orl
               WHERE fu.plan_type = 'OFFR'
                 AND fu.plan_id = qpb.list_header_id
                 AND fu.object_id = orl.resale_line_id
                 AND fu.cust_account_id = orl.sold_from_cust_account_id
                 AND qpb.list_header_id = qpq.list_header_id
                 AND oof.qp_list_header_id = qpb.list_header_id
                 AND qpb.attribute5 = nvl(p_pmt_method, qpb.attribute5)
                 AND qpb.attribute6 = nvl(p_pmt_frequency, qpb.attribute6)
                 AND nvl(oof.autopay_flag, 'Y') = 'Y'
                    -- AND fu.fund_id = nvl(v_fund_id, fu.fund_id)
                 AND fu.plan_curr_amount_remaining <> 0
                 AND fu.utilization_type IN ('ACCRUAL', 'ADJUSTMENT')
                 AND fu.cust_account_id =
                     nvl(p_customer_id, fu.cust_account_id)
                    --AND fu.org_id= p_org_id
                 AND fu.plan_id = nvl(p_plan_id, fu.plan_id)
                 AND fu.gl_posted_flag = 'Y'
               GROUP BY fu.currency_code
                       ,orl.sold_from_cust_account_id
                       ,fu.plan_id
                       ,qpb.attribute5;
          ELSE
            printlog('Remb');

            OPEN claim_csr FOR
--Version 1.1
/*              SELECT SUM(fu.amount_remaining) amount
                    ,fu.currency_code
                    ,orl.sold_from_cust_account_id
                    ,fu.plan_id
                    ,qpb.attribute5
                    ,qpb.attribute6
                FROM apps.ozf_funds_utilized_all_b fu
                    ,apps.qp_list_headers_b        qpb
                    ,apps.ozf_offers               oof
                    ,apps.qp_qualifiers            qpq
                    ,apps.ozf_resale_lines_all     orl
               WHERE fu.plan_type = 'OFFR'
                 AND fu.plan_id = qpb.list_header_id
                 AND fu.object_id = orl.resale_line_id
                 AND fu.cust_account_id = orl.sold_from_cust_account_id
                 AND qpb.list_header_id = qpq.list_header_id
                 AND oof.qp_list_header_id = qpb.list_header_id
                 AND qpb.attribute5 = nvl(p_pmt_method, qpb.attribute5)
                 AND qpb.attribute6 = nvl(p_pmt_frequency, qpb.attribute6)
                 AND nvl(oof.autopay_flag, 'Y') = 'Y'
                    --AND oof.autopay_flag = 'Y'
                    --AND fu.fund_id = nvl(v_fund_id, fu.fund_id)
                 AND fu.attribute10 = 'REIMBURSEMENT'
                 AND fu.plan_curr_amount_remaining <> 0
                 AND qualifier_attribute = 'QUALIFIER_ATTRIBUTE2'
                 AND qualifier_context = 'SOLD_BY'
                 AND fu.utilization_type IN ('ACCRUAL', 'ADJUSTMENT')
                 AND fu.cust_account_id =
                     nvl(p_customer_id, fu.cust_account_id)
                 AND fu.plan_id = nvl(p_plan_id, fu.plan_id)
                 AND fu.gl_posted_flag = 'Y'
               GROUP BY fu.currency_code
                       ,orl.sold_from_cust_account_id
                       ,fu.plan_id
                       ,qpb.attribute5
                       ,qpb.attribute6;
*/
SELECT SUM(fu.amount_remaining) amount
                    ,fu.currency_code
                    ,orl.sold_from_cust_account_id
                    ,fu.plan_id
                    ,qpb.attribute5
                    ,qpb.attribute6
                FROM apps.ozf_funds_utilized_all_b fu
                    ,apps.qp_list_headers_b        qpb
                    ,apps.ozf_offers               oof
                    ,(SELECT DISTINCT attribute5
                                     ,attribute6
                                     ,list_header_id
                        FROM apps.qp_qualifiers
                       WHERE qualifier_attribute = 'QUALIFIER_ATTRIBUTE2'
                         AND qualifier_context = 'SOLD_BY') qpq
                    ,apps.ozf_resale_lines_all     orl
               WHERE fu.plan_type = 'OFFR'
                 AND fu.plan_id = qpb.list_header_id
                 AND fu.object_id = orl.resale_line_id
                 AND fu.cust_account_id = orl.sold_from_cust_account_id
                 AND qpb.list_header_id = qpq.list_header_id
                 AND oof.qp_list_header_id = qpb.list_header_id
                 AND qpb.attribute5 = nvl(p_pmt_method, qpb.attribute5)
                 AND qpb.attribute6 = nvl(p_pmt_frequency, qpb.attribute6)
                 AND nvl(oof.autopay_flag, 'Y') = 'Y'
                    --AND oof.autopay_flag = 'Y'
                    --AND fu.fund_id = nvl(v_fund_id, fu.fund_id)
                 AND fu.attribute10 --= 'REIMBURSEMENT'
                        IN ( SELECT LOOKUP_CODE
                             from apps.fnd_lookup_values_vl
                             WHERE     1 = 1
                             and lookup_type = 'XXCUS_OZF_NEG_INVOICE_TYPES')
                 AND fu.plan_curr_amount_remaining <> 0
                 AND fu.utilization_type IN ('ACCRUAL', 'ADJUSTMENT')
                 AND fu.cust_account_id =
                     nvl(p_customer_id, fu.cust_account_id)
                 AND fu.plan_id = p_plan_id --Ver 3.0
                 AND fu.gl_posted_flag = 'Y'
               GROUP BY fu.currency_code
                       ,orl.sold_from_cust_account_id
                       ,fu.plan_id
                       ,qpb.attribute5
                       ,qpb.attribute6;
          END IF;

          LOOP
            FETCH claim_csr
              INTO l_amount
                  ,l_utiz_currency
                  ,l_cust_account_id
                  ,l_plan_id
                  ,l_payment_method
                  ,l_payment_freq;

            EXIT WHEN claim_csr%NOTFOUND;

            fnd_file.put_line(fnd_file.log
                             ,rpad('Payment Method', 40, ' ') || ': ' ||
                              l_payment_method);
            --

            IF p_pmt_method IS NOT NULL
            THEN
              OPEN get_claim_type_id_csr(p_pmt_method, p_org_id);
              FETCH get_claim_type_id_csr
                INTO l_auto_claim_type_id;
              CLOSE get_claim_type_id_csr;
            END IF;

            IF p_pmt_method IS NULL
            THEN
              OPEN get_claim_type_id_csr(l_payment_method, p_org_id);
              FETCH get_claim_type_id_csr
                INTO l_auto_claim_type_id;
              CLOSE get_claim_type_id_csr;
            END IF;

            fnd_file.put_line(fnd_file.log
                             ,rpad('l_auto_claim_type_id', 40, ' ') || ': ' ||
                              l_auto_claim_type_id);

            fnd_file.put_line(fnd_file.log
                             ,rpad('l_amount', 40, ' ') || ': ' ||
                              l_amount);
            fnd_file.put_line(fnd_file.log
                             ,rpad('l_plan_id', 40, ' ') || ': ' ||
                              l_plan_id);
            fnd_file.put_line(fnd_file.log
                             ,rpad('l_utiz_currency', 40, ' ') || ': ' ||
                              l_utiz_currency);
            fnd_file.put_line(fnd_file.log
                             ,rpad('l_cust_account_id', 40, ' ') || ': ' ||
                              l_cust_account_id);
            fnd_msg_pub.initialize;

            --PrintLog('Check0');

            l_bill_to_site_use_id := NULL;

            OPEN csr_bill_site_use_id(l_cust_account_id, p_org_id);
            FETCH csr_bill_site_use_id
              INTO l_bill_to_site_use_id;
            CLOSE csr_bill_site_use_id;

            --PrintLog('Check0.1');

            l_funds_util_flt.activity_id        := l_plan_id;
            l_funds_util_flt.fund_id            := l_fund_id;
            l_funds_util_flt.cust_account_id    := l_cust_account_id;
            l_funds_util_flt.utiz_currency_code := l_utiz_currency;

            BEGIN
              SAVEPOINT autopay_cust;
              --  PrintLog('Check0.2');
              --IF (p_threshold_val < l_amount AND p_pmt_method NOT IN ('DEDUCTION')) THEN
              -- IF ((p_threshold_val > l_amount AND p_pmt_method NOT IN ('DEDUCTION')) OR (p_threshold_val<l_amount))  THEN
              IF l_cust_account_id IS NOT NULL AND l_amount <> 0 --Dragan
              THEN
                --  PrintLog('Check0.3');
                IF l_prev_site_use_id IS NULL OR

                   l_bill_to_site_use_id <> l_prev_site_use_id
                THEN
                  --    PrintLog('Check0.4');
                  l_cust_name_num := NULL;
                  l_customer_info := NULL;
                  l_party_id      := NULL;

                  -- Get customer information for log message purpose
                  OPEN csr_cust_name(l_cust_account_id);
                  FETCH csr_cust_name
                    INTO l_cust_name_num;
                  CLOSE csr_cust_name;

                  fnd_file.put_line(fnd_file.log
                                   ,rpad('l_cust_account_id', 40, ' ') || ': ' ||
                                    l_cust_account_id);
                  fnd_file.put_line(fnd_file.log
                                   ,rpad('l_bill_to_site_use_id', 40, ' ') || ': ' ||
                                    l_bill_to_site_use_id);
                  fnd_file.put_line(fnd_file.log
                                   ,rpad('l_fund_id', 40, ' ') || ': ' ||
                                    l_fund_id);
                  fnd_file.put_line(fnd_file.log
                                   ,rpad('l_plan_id', 40, ' ') || ': ' ||
                                    l_plan_id);
                  fnd_file.put_line(fnd_file.log
                                   ,rpad('l_amount', 40, ' ') || ': ' ||
                                    l_amount);

                END IF;
                -- PrintLog('Check0.5');
                OPEN g_customer_info_csr(l_cust_account_id);
                FETCH g_customer_info_csr
                  INTO l_customer_info;
                CLOSE g_customer_info_csr;
                printlog('Check0.6');
                l_prev_site_use_id := l_bill_to_site_use_id;

                IF p_customer_id IS NULL
                THEN
                  fnd_file.put_line(fnd_file.output
                                   ,'Customer Name : ' || l_cust_name_num);
                  fnd_file.put_line(fnd_file.output
                                   ,'Customer Name is null: ');
                END IF;
                -- PrintLog('Check0.7');
                l_funds_util_flt.bill_to_site_use_id := l_bill_to_site_use_id;
                -- PrintLog('Check0.8');

                --FND_FILE.PUT_LINE(FND_FILE.LOG, 'l_funds_util_flt.bill_to_site_use_id' || l_funds_util_flt.bill_to_site_use_id);

                --R12.1 enhancements

                -- PrintLog('Check1');
                fnd_file.put_line(fnd_file.log
                                 ,'--- Start AUTOPAY for customer ' ||
                                  l_cust_name_num || ' ---');

                --
                fnd_file.put_line(fnd_file.log
                                 ,'Before Call to Create_Claim_for_Cust');

                create_claim_for_cust(p_customer_info       => l_customer_info
                                     ,p_org_id              => p_org_id
                                     ,p_amount              => l_amount
                                     ,p_mode                => 'N'
                                     ,p_auto_reason_code_id => l_auto_reason_code_id
                                     ,p_auto_claim_type_id  => l_auto_claim_type_id
                                     ,p_pmt_frequency       => l_payment_freq
                                     ,p_pmt_method          => p_pmt_method
                                     ,p_invoice_type        => p_invoice_type
                                     ,p_funds_util_flt      => l_funds_util_flt
                                     ,x_return_status       => l_return_status);

                fnd_file.put_line(fnd_file.log
                                 ,'After Call to Create_Claim_for_Cust');
                fnd_file.put_line(fnd_file.log
                                 ,'Return Status' || l_return_status);

                IF l_return_status = fnd_api.g_ret_sts_error
                THEN
                  RAISE fnd_api.g_exc_error;
                ELSIF l_return_status = fnd_api.g_ret_sts_unexp_error
                THEN
                  RAISE fnd_api.g_exc_unexpected_error;
                END IF;

                --FND_FILE.PUT_LINE(FND_FILE.LOG, 'Claim Amount = '||l_amount);
                fnd_file.put_line(fnd_file.log, '===> Success.');
                fnd_file.put_line(fnd_file.log, '--- End ---');
                fnd_file.put_line(fnd_file.log, ' ');
                fnd_file.put_line(fnd_file.output
                                 ,'Claim Amount               : ' ||
                                  l_amount);
                fnd_file.put_line(fnd_file.output
                                 ,'Status : Auto Pay Success. ');
                fnd_file.put_line(fnd_file.output, '');
                ozf_utility_pvt.write_conc_log;
                --

              END IF; -- end of if l_cust_account_id is not null

              --END IF;

            EXCEPTION
              WHEN fnd_api.g_exc_error THEN

                l_err_code := 2;
                l_err_msg  := l_sec;
                l_err_msg  := l_err_msg || ' ERROR ' ||
                              substr(SQLERRM, 1, 2000);

                fnd_file.put_line(fnd_file.log, l_err_msg);
                fnd_file.put_line(fnd_file.output, l_err_msg);
                retcode         := l_err_code;
                errbuf          := l_err_msg;
                l_err_callpoint := l_message;

                xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                                    ,p_calling           => l_err_callpoint
                                                    ,p_request_id        => l_req_id
                                                    ,p_ora_error_msg     => SQLERRM
                                                    ,p_error_desc        => 'Error running XXCUSOZF_INVOICE_PKG.Start_Autopay package with PROGRAM ERROR'
                                                    ,p_distribution_list => l_distro_list
                                                    ,p_module            => 'TM');

                ROLLBACK TO autopay_cust;
                fnd_file.put_line(fnd_file.log, '===> Failed11.');
                ozf_utility_pvt.write_conc_log;
                fnd_file.put_line(fnd_file.log, '--- End ---');
                fnd_file.put_line(fnd_file.log, ' ');
                fnd_file.put_line(fnd_file.output
                                 ,'Status : Auto Pay Failed. ');
                fnd_file.put_line(fnd_file.output
                                 ,'Error : ' ||
                                  fnd_msg_pub.get(fnd_msg_pub.count_msg
                                                 ,fnd_api.g_false));
                fnd_file.put_line(fnd_file.output, '');
              WHEN fnd_api.g_exc_unexpected_error THEN
                ROLLBACK TO autopay_cust;
                fnd_file.put_line(fnd_file.log, '===> Failed.');
                ozf_utility_pvt.write_conc_log;
                fnd_file.put_line(fnd_file.log, '--- End ---');
                fnd_file.put_line(fnd_file.log, ' ');
                fnd_file.put_line(fnd_file.output
                                 ,'Status : Auto Pay Failed. ');
                fnd_file.put_line(fnd_file.output
                                 ,'Error : ' ||
                                  fnd_msg_pub.get(fnd_msg_pub.count_msg
                                                 ,fnd_api.g_false));
                fnd_file.put_line(fnd_file.output, '');

              WHEN OTHERS THEN
                l_err_code := 2;
                l_err_msg  := l_sec;
                l_err_msg  := l_err_msg || ' ERROR ' ||
                              substr(SQLERRM, 1, 2000);

                fnd_file.put_line(fnd_file.log, l_err_msg);
                fnd_file.put_line(fnd_file.output, l_err_msg);
                retcode         := l_err_code;
                errbuf          := l_err_msg;
                l_err_callpoint := l_message;

                xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                                    ,p_calling           => l_err_callpoint
                                                    ,p_request_id        => l_req_id
                                                    ,p_ora_error_msg     => SQLERRM
                                                    ,p_error_desc        => 'Error running XXCUSOZF_INVOICE_PKG.Start_Autopay package with PROGRAM ERROR'
                                                    ,p_distribution_list => l_distro_list
                                                    ,p_module            => 'TM');
                ROLLBACK TO autopay_cust;
                fnd_file.put_line(fnd_file.log, '===> Failed.');
                IF ozf_debug_low_on
                THEN
                  fnd_message.set_name('OZF', 'OZF_API_DEBUG_MESSAGE');
                  fnd_message.set_token('TEXT', SQLERRM);
                  fnd_msg_pub.add;
                END IF;
                ozf_utility_pvt.write_conc_log;
                fnd_file.put_line(fnd_file.log, '--- End ---');
                fnd_file.put_line(fnd_file.log, ' ');
                fnd_file.put_line(fnd_file.output
                                 ,'Status : Auto Pay Failed. ');
                fnd_file.put_line(fnd_file.output
                                 ,'Error : ' || SQLCODE || SQLERRM);
                fnd_file.put_line(fnd_file.output, '');
            END;
            --
          END LOOP;
          CLOSE claim_csr;

          -- Debug Message
          IF ozf_debug_low_on
          THEN
            fnd_message.set_name('OZF', 'OZF_API_DEBUG_MESSAGE');
            fnd_message.set_token('TEXT', l_full_name || ': End');
            fnd_msg_pub.add;
          END IF;

          -- Write all messages to a log
          ozf_utility_pvt.write_conc_log;

          fnd_file.put_line(fnd_file.output
                           ,'*---------------------------------------------------------------------------------------------*');
          fnd_file.put_line(fnd_file.output
                           ,'Execution Status: Successful');
          fnd_file.put_line(fnd_file.output
                           ,'Execution Ends On: ' ||
                            to_char(SYSDATE, 'MM-DD-YYYY HH24:MI:SS'));
          fnd_file.put_line(fnd_file.output
                           ,'*---------------------------------------------------------------------------------------------*');

        EXCEPTION
          WHEN fnd_api.g_exc_error THEN
            l_err_code := 2;
            l_err_msg  := l_sec;
            l_err_msg  := l_err_msg || ' ERROR ' ||
                          substr(SQLERRM, 1, 2000);

            fnd_file.put_line(fnd_file.log, l_err_msg);
            fnd_file.put_line(fnd_file.output, l_err_msg);
            retcode         := l_err_code;
            errbuf          := l_err_msg;
            l_err_callpoint := l_message;

            xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                                ,p_calling           => l_err_callpoint
                                                ,p_request_id        => l_req_id
                                                ,p_ora_error_msg     => SQLERRM
                                                ,p_error_desc        => 'Error running XXCUSOZF_INVOICE_PKG.Start_Autopay package with PROGRAM ERROR'
                                                ,p_distribution_list => l_distro_list
                                                ,p_module            => 'TM');
            ROLLBACK TO autopay;

            ozf_utility_pvt.write_conc_log;
            errbuf  := l_msg_data;
            retcode := 2;
            fnd_file.put_line(fnd_file.output
                             ,'Execution Status: Failure (Error:' ||
                              fnd_msg_pub.get(1, fnd_api.g_false) || ')');
            fnd_file.put_line(fnd_file.output
                             ,'Execution Ends On: ' ||
                              to_char(SYSDATE, 'MM-DD-YYYY HH24:MI:SS'));
            fnd_file.put_line(fnd_file.output
                             ,'*---------------------------------------------------------------------------------------------*');

          WHEN fnd_api.g_exc_unexpected_error THEN
            l_err_code := 2;
            l_err_msg  := l_sec;
            l_err_msg  := l_err_msg || ' ERROR ' ||
                          substr(SQLERRM, 1, 2000);

            fnd_file.put_line(fnd_file.log, l_err_msg);
            fnd_file.put_line(fnd_file.output, l_err_msg);
            retcode         := l_err_code;
            errbuf          := l_err_msg;
            l_err_callpoint := l_message;

            xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                                ,p_calling           => l_err_callpoint
                                                ,p_request_id        => l_req_id
                                                ,p_ora_error_msg     => SQLERRM
                                                ,p_error_desc        => 'Error running XXCUSOZF_INVOICE_PKG.Start_Autopay package with PROGRAM ERROR'
                                                ,p_distribution_list => l_distro_list
                                                ,p_module            => 'TM');
            ROLLBACK TO autopay;

            ozf_utility_pvt.write_conc_log;
            errbuf  := l_msg_data;
            retcode := 1; -- show status as warning if claim type/reason is missing,  Fix for 5158782
            fnd_file.put_line(fnd_file.output
                             ,'Execution Status: ( Warning:' ||
                              fnd_msg_pub.get(1, fnd_api.g_false) || ')');
            fnd_file.put_line(fnd_file.output
                             ,'Execution Ends On: ' ||
                              to_char(SYSDATE, 'MM-DD-YYYY HH24:MI:SS'));
            fnd_file.put_line(fnd_file.output
                             ,'*---------------------------------------------------------------------------------------------*');

          WHEN OTHERS THEN
            l_err_code := 2;
            l_err_msg  := l_sec;
            l_err_msg  := l_err_msg || ' ERROR ' ||
                          substr(SQLERRM, 1, 2000);

            fnd_file.put_line(fnd_file.log, l_err_msg);
            fnd_file.put_line(fnd_file.output, l_err_msg);
            retcode         := l_err_code;
            errbuf          := l_err_msg;
            l_err_callpoint := l_message;

            xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                                ,p_calling           => l_err_callpoint
                                                ,p_request_id        => l_req_id
                                                ,p_ora_error_msg     => SQLERRM
                                                ,p_error_desc        => 'Error running XXCUSOZF_INVOICE_PKG.Start_Autopay package with PROGRAM ERROR'
                                                ,p_distribution_list => l_distro_list
                                                ,p_module            => 'TM');
            ROLLBACK TO autopay;

            ozf_utility_pvt.write_conc_log;
            errbuf  := l_msg_data;
            retcode := 2;
            fnd_file.put_line(fnd_file.output
                             ,'Execution Status: Failure (Error:' ||
                              SQLCODE || SQLERRM || ')');
            fnd_file.put_line(fnd_file.output
                             ,'Execution Ends On: ' ||
                              to_char(SYSDATE, 'MM-DD-YYYY HH24:MI:SS'));
            fnd_file.put_line(fnd_file.output
                             ,'*---------------------------------------------------------------------------------------------*');

        END;
      END LOOP;
    END IF;
  END start_remburse;
--   Begin 4.0 --TMS 20170117-00141
  PROCEDURE reimbursement_wrapper
   (
    retcode               out number
   ,errbuf                out varchar2
   ,p_cal_year      in varchar2
   ,p_customer_id   in number  DEFAULT NULL
   ,p_plan_id       in number  DEFAULT NULL
   ,p_pmt_frequency in varchar2  DEFAULT NULL   
   ,p_pmt_method in varchar2  DEFAULT NULL
   ,p_invoice_type IN VARCHAR2   
  ) IS
   --
   CURSOR all_reimbursements IS
SELECT /*+ parallel (orl 2) parallel (fu 2) parallel (qpb 2) parallel (oof 2) */ 
                     fu.currency_code currency
                    ,orl.sold_from_cust_account_id cust_account_id
                    ,fu.plan_id qp_list_header_id 
                    ,qpb.attribute5 --payment method
                    ,qpb.attribute6 --payment frequency
                    ,qpb.attribute7 --agreement year
                    --SUM(fu.amount_remaining) amount
                FROM apps.ozf_funds_utilized_all_b fu
                    ,apps.qp_list_headers_b        qpb
                    ,apps.ozf_offers               oof
                    ,(SELECT /*+ parallel (a 2) */ DISTINCT attribute5
                                     ,attribute6
                                     ,list_header_id
                        FROM apps.qp_qualifiers a
                       WHERE qualifier_attribute = 'QUALIFIER_ATTRIBUTE2'
                         AND qualifier_context = 'SOLD_BY') qpq
                    ,apps.ozf_resale_lines_all     orl
               WHERE 1 =1
                  AND fu.plan_type = 'OFFR'
                 AND fu.plan_id = qpb.list_header_id
                 AND fu.object_id = orl.resale_line_id
                 and (
                              (p_customer_id is not null and fu.cust_account_id =p_customer_id)
                              or
                              (p_customer_id is null and 2 =2)
                           )                 
                 AND fu.cust_account_id = orl.sold_from_cust_account_id
                 AND qpb.list_header_id = qpq.list_header_id
                 AND oof.qp_list_header_id = qpb.list_header_id
                 AND qpb.attribute5 = nvl(p_pmt_method, qpb.attribute5)
                 AND qpb.attribute6 = nvl(p_pmt_frequency, qpb.attribute6)
                 AND qpb.attribute7 = p_cal_year                
                 AND nvl(oof.autopay_flag, 'Y') = 'Y'
                    --AND oof.autopay_flag = 'Y'
                    --AND fu.fund_id = nvl(v_fund_id, fu.fund_id)
                 AND fu.attribute10 --= 'REIMBURSEMENT'
                        IN ( SELECT LOOKUP_CODE
                             from apps.fnd_lookup_values_vl
                             WHERE     1 = 1
                             and lookup_type = 'XXCUS_OZF_NEG_INVOICE_TYPES'
                             and attribute_category ='XXCUS_REIMBURSEMENT_WRAPPER'
                             and attribute1 ='Y'
                             )
                 AND fu.plan_curr_amount_remaining <> 0
                 AND fu.utilization_type IN ('ACCRUAL', 'ADJUSTMENT')
                 --AND fu.cust_account_id =                     nvl(:p_customer_id, fu.cust_account_id)
                 --AND fu.plan_id =:p_plan_id --Ver 3.0
                 and (
                              (p_plan_id is not null and fu.plan_id =p_plan_id)
                              or
                              (p_plan_id is null and 2 =2)
                           ) 
                 AND fu.gl_posted_flag = 'Y'
               GROUP BY fu.currency_code
                       ,orl.sold_from_cust_account_id
                       ,fu.plan_id
                       ,qpb.attribute5
                       ,qpb.attribute6
                       ,qpb.attribute7;
  --
    TYPE reimb_tbl IS TABLE OF all_reimbursements%ROWTYPE INDEX BY BINARY_INTEGER;
    reimb_rec reimb_tbl; 
  --
   v_location       VARCHAR2(2000);
   lc_request_data  VARCHAR2(20) :='';
   ln_request_id    NUMBER :=0;
   --
   n_req_id         NUMBER :=0;
   l_org_id         NUMBER :=0; 
   v_cp_name        fnd_concurrent_programs_tl.user_concurrent_program_name%type :=Null;  
  --    
  -- =========================================================================================
  l_module        VARCHAR2(24) :='HDS Rebates';
  l_err_msg       CLOB;
  l_sec           VARCHAR2(255);
  l_err_callfrom  VARCHAR2(75) DEFAULT 'apps.xxcus_ozf_invoice_pkg.reimbursement_wrapper';
  l_err_callpoint VARCHAR2(75) DEFAULT 'START';
  l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';    
  --     
  begin 
  --
   n_req_id        :=fnd_global.conc_request_id;
   l_org_id        :=mo_global.get_current_org_id;
   lc_request_data :=fnd_conc_global.request_data;
  --
  printlog('l_org_id :'||l_org_id);
  --
   If lc_request_data is null then
    --   
     lc_request_data :='1';
    --
     Begin 
      open all_reimbursements;
      fetch all_reimbursements bulk collect into reimb_rec;
      close all_reimbursements;
     Exception
      When program_error then
       v_location :='103';
     End;
    -- 
     if reimb_rec.count >0 then
      --
      begin 
        select cp_tl.user_concurrent_program_name
        into   v_cp_name
        from   fnd_concurrent_programs     cp
              ,fnd_concurrent_programs_tl  cp_tl
        where  1 =1
          and  cp.concurrent_program_name   ='XXCUSOZF_REIMBURSE'
          and  cp_tl.concurrent_program_id  =cp.concurrent_program_id;      
      exception
       when others then
        v_cp_name :='NA';
      end;
      --
       for idx in reimb_rec.first .. reimb_rec.last loop
        -- 
        fnd_request.set_org_id(l_org_id);
        --
        ln_request_id :=fnd_request.submit_request --102, , 3731217, , , , REM
              (
                application      =>'XXCUS'
               ,program          =>'XXCUSOZF_REIMBURSE'
               ,description      =>''
               ,start_time       =>''
               ,sub_request      =>TRUE 
               ,argument1        =>l_org_id
               ,argument2        =>''
               ,argument3        =>reimb_rec(idx).qp_list_header_id
               ,argument4        =>''
               ,argument5        =>''
               ,argument6        =>''
               ,argument7        =>p_invoice_type
              );
        --
          if ln_request_id >0 then 
            fnd_file.put_line(fnd_file.log, 'Submitted '||v_cp_name||' for plan_id  ='
                                             ||reimb_rec(idx).qp_list_header_id
                                             ||', Request Id ='
                                             ||ln_request_id
                                            );
          else
            fnd_file.put_line(fnd_file.log, 'Failed to submit '||v_cp_name||' for plan_id ='
                                             ||reimb_rec(idx).qp_list_header_id                                                   
                                            );
          end if;     
                   
         fnd_conc_global.set_req_globals(conc_status =>'PAUSED' ,request_data =>lc_request_data);
           
         lc_request_data :=to_char(to_number(lc_request_data)+1);        
        --
       end loop; --idx in reimb_rec.first .. reimb_rec.last 
      --
     end if; --reimb_rec >0
    --
   Else
    --
      retcode :=0;
      errbuf :='Child request completed. Exit HDS Rebates: Kickoff Reimbursement - Master';
      fnd_file.put_line(fnd_file.log,errbuf);
    --
   End If;  
  --
  exception
  --
   when program_error then
    fnd_file.put_line(fnd_file.log,'Location =>'||v_location||', program error, message ='||sqlerrm);
  --  
   when others then   
    fnd_file.put_line(fnd_file.log,'Location =>'||v_location||', outer block other errors');   
    fnd_file.put_line(fnd_file.log,'Outer block, other errors in calling wrapper, message ='||sqlerrm);
    rollback;
    l_err_msg :='Location =>'||v_location||', outer block other errors';
        l_err_msg := l_err_msg || ' ...Error_Stack...' ||
                     dbms_utility.format_error_stack() || ' Error_Backtrace...' ||
                     dbms_utility.format_error_backtrace();
        fnd_file.put_line(fnd_file.log, l_err_msg);
        l_err_callpoint := l_sec;      
        xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                            ,p_calling           => l_err_callpoint
                                            ,p_request_id        => fnd_global.conc_request_id
                                            ,p_ora_error_msg     => substr(' Error_Stack...' ||
                                                                           dbms_utility.format_error_stack() ||
                                                                           ' Error_Backtrace...' ||
                                                                           dbms_utility.format_error_backtrace()
                                                                          ,1
                                                                          ,2000)
                                            ,p_error_desc        => substr(l_err_msg
                                                                          ,1
                                                                          ,240)
                                            ,p_distribution_list => l_distro_list
                                            ,p_module            => l_module);    
  --    
  end reimbursement_wrapper;  
 -- End 4.0 --TMS 20170117-00141
--
END xxcus_ozf_invoice_pkg;
/