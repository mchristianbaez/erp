CREATE OR REPLACE PACKAGE BODY XXEIS.eis_po_xxwc_isr_pkg
AS
  /*************************************************************************
  $Header EIS_PO_XXWC_ISR_PKG.pkb $
  Module Name: EIS_PO_XXWC_ISR_PKG
  PURPOSE: ISR Datamart Package
  ESMS Task Id :
  REVISIONS:
  Ver        Date        Author                Description
  ---------  ----------  ------------------    ----------------
  1.1        28-Jul-2014  Manjula Chellappan   Modified TMS # 20140728-00186 : ISR Package Execution Error
  1.2        05-Aug-2014  Krishna              Modified TMS#20130709-01006: UC4 cannot handle huge logs. 
                                               Commented the Common Tuning FTP Help Write Log.
  1.3        05-Oct-2015  Manjula Chellappan   Modified for TMS # 20151005-00003 - Performance Improvement    
  1.4        09-May-2016  P.Vamshidhar         Modified for TMS # 20160509-00068  - Reduce parallelism on EIS Reports to 4                                               
  **************************************************************************/
  g_distribution_list fnd_user.email_address%TYPE := 'HDSOracleDevelopers@hdsupply.com';
PROCEDURE write_log(
    p_text IN VARCHAR2 )
IS
BEGIN
  fnd_file.put_line(fnd_file.LOG,p_text);
  dbms_output.put_line(p_text);
END;
PROCEDURE get_vendor_info(
    p_inventory_item_id IN NUMBER,
    p_organization_id   IN NUMBER,
    p_vendor_name OUT VARCHAR2,
    p_vendor_number OUT VARCHAR2,
    p_vendor_site OUT VARCHAR2)
IS
  l_vendor_name   VARCHAR2(240) := NULL;
  l_vendor_number VARCHAR2(240) := NULL;
  l_vendor_site   VARCHAR2(240) := NULL;
  l_vendor_id     NUMBER;
BEGIN
  BEGIN
    SELECT pov.vendor_name,
      pov.segment1,
      pvs.vendor_site_code
    INTO l_vendor_name,
      l_vendor_number,
      l_vendor_site
    FROM mrp_sr_assignments ass,
      mrp_sr_receipt_org rco,
      mrp_sr_source_org sso,
      po_vendors pov,
      po_vendor_sites_all pvs,
      mtl_system_items_b msi
    WHERE 1                   =1
    AND msi.inventory_item_id = ass.inventory_item_id
    AND msi.organization_id   = ass.organization_id
    AND ass.inventory_item_id = p_inventory_item_id
    AND ass.organization_id   = p_organization_id
    AND msi.source_type       = 2
    AND rco.sourcing_rule_id  = ass.sourcing_rule_id
    AND sso.sr_receipt_id     = rco.sr_receipt_id
    AND SSO.SOURCE_TYPE       = 3
    AND pov.vendor_id         = sso.vendor_id
    AND pvs.vendor_site_id    = sso.vendor_site_id;
    p_vendor_name            := l_vendor_name;
    p_vendor_number          := l_vendor_number;
    p_vendor_site            := l_vendor_site;
  EXCEPTION
  WHEN OTHERS THEN
    l_vendor_name   := NULL;
    p_vendor_name   := NULL;
    p_vendor_number := NULL;
    p_vendor_site   := NULL;
  END;
  IF l_vendor_name IS NULL THEN
    SELECT vendor_id
    INTO l_vendor_id
    FROM
      (SELECT COUNT(sso.vendor_id) cnt,
        sso.vendor_id
      FROM mrp_sr_assignments ass,
        mrp_sr_receipt_org rco,
        mrp_sr_source_org sso,
        --                 po_vendors pov,
        mtl_system_items_b msi
      WHERE 1                   =1
      AND msi.inventory_item_id = ass.inventory_item_id
      AND msi.organization_id   = ass.organization_id
      AND ass.inventory_item_id = p_inventory_item_id
        --and ass.organization_id   = p_organization_id
      AND msi.source_type      = 2
      AND rco.sourcing_rule_id = ass.sourcing_rule_id
      AND sso.sr_receipt_id    = rco.sr_receipt_id
      AND SSO.SOURCE_TYPE      = 3
        --            and pov.vendor_id         = sso.vendor_id
      GROUP BY sso.vendor_id
      ORDER BY 1 DESC
      )a
    WHERE rownum <=1 ;
    SELECT segment1,
      vendor_name
    INTO l_vendor_number,
      l_vendor_name
    FROM po_vendors
    WHERE vendor_id = l_vendor_id;
  END IF;
  p_vendor_name   := l_vendor_name;
  p_vendor_number := l_vendor_number;
  p_vendor_site   := l_vendor_site;
EXCEPTION
WHEN OTHERS THEN
  p_vendor_name   := NULL;
  p_vendor_number := NULL;
  p_vendor_site   := NULL;
END;

  /************************************************************************************************************************
  PURPOSE: Main Procedure
  ESMS Task Id :
  REVISIONS:
  Ver        Date        Author                Description
  ---------  ----------  ------------------    ----------------
  1.4        09-May-2016  P.Vamshidhar         Modified for TMS # 20160509-00068  - Reduce parallelism on EIS Reports to 4                                               
  *************************************************************************************************************************/

PROCEDURE MAIN
IS
  CURSOR c_item_cur
  IS
    ( SELECT * FROM xxeis.eis_xxwc_po_isr_tab_v
    );
  CURSOR c_item_cur1
  IS
    ( SELECT * FROM xxeis.eis_xxwc_po_isr_mst_tab_v
    );
type period_tab
IS
  TABLE OF VARCHAR2(200) INDEX BY binary_integer;
type period_tab2
IS
  TABLE OF VARCHAR2(200) INDEX BY binary_integer;
  period_name_tab period_tab;
  ordered_period_name_tab period_tab2;
  l_yes_no                       VARCHAR2(10);
  l_current_period_year          VARCHAR2(20);
  l_current_effective_period_num VARCHAR2(240);
  l_period_name                  VARCHAR2(100);
  l_store_period_column_name     VARCHAR2(240);
  l_other_period_column_name     VARCHAR2(240);
  l_update_str                   VARCHAR2(10000);
  lv_program_location            VARCHAR2(4000);
  l_period_count                 NUMBER :=0;
  l_item_cost                    NUMBER;
  l_avail_d                      NUMBER;
  l_on_ord                       NUMBER;
  l_avail2                       NUMBER;
  l_vendor_name                  VARCHAR2(240);
  l_vendor_number                VARCHAR2(50);
  l_vendor_site                  VARCHAR2(100);
  l_organization_id              NUMBER;
  l_count                        NUMBER;
  L_REF_CURSOR2 CURSOR_TYPE2;
  L_REF_CURSOR4 CURSOR_TYPE4;
  L_REF_CURSOR5 CURSOR_TYPE5;
  l_main_sql           VARCHAR2(32000);
  l_period_sales       VARCHAR2(32000);
  l_period_other_sales VARCHAR2(32000);
  l_hits_sql           VARCHAR2(32000);
  l_sales_sql          VARCHAR2(32000);
BEGIN
  lv_program_location := '1, Program Start ';
  write_log('Truncate Start '|| TO_CHAR(sysdate,'DD-MON-YYYY HH:MI:SS') );
  EXECUTE IMMEDIATE 'truncate table xxeis.eis_xxwc_po_isr_tab';
  -- Drop the indexes
  BEGIN
    EXECUTE IMMEDIATE 'drop index XXEIS.XXWC_PO_ISR_HIT_N1';
    EXECUTE IMMEDIATE 'drop index XXEIS.XXWC_PO_ISR_OHITS_N1';
    EXECUTE IMMEDIATE 'drop index XXEIS.XXWC_PO_ISR_POSALES_TAB_N1';
    EXECUTE IMMEDIATE 'drop index XXEIS.XXWC_PO_ISR_OSALES_TAB_N1';
    EXECUTE IMMEDIATE 'drop index XXEIS.XXWC_PO_ISR_PSALES_TAB_N1';
    EXECUTE IMMEDIATE 'drop index XXEIS.XXWC_PO_ISR_SALES_TAB_N1';
    EXECUTE IMMEDIATE 'drop index XXEIS.XXWC_PO_ISR_TAB_TMP_N1';
    EXECUTE IMMEDIATE 'drop index XXEIS.XXWC_PO_ISR_TAB_N1';
  EXCEPTION
  WHEN OTHERS THEN
    write_log('Error on dropping indexes '|| SQLERRM);
  END;
  l_main_sql := ' SELECT *                     
from xxeis.eis_xxwc_po_isr_tab_v ';
  write_log('Bulk collect Main query start '|| TO_CHAR(sysdate,'DD-MON-YYYY HH:MI:SS') );
  lv_program_location := '2, Main query Start ';
  OPEN l_ref_cursor2 FOR l_main_sql;
  LOOP
    FETCH L_REF_CURSOR2 BULK COLLECT INTO G_VIEW_RECORD2 LIMIT 10000;
    l_count             := 0;
    lv_program_location := '3, After Main query Fetch';
    FOR j IN 1..G_VIEW_RECORD2.count
    LOOP
      lv_program_location := '4, Populate Main query into table record ';
      -- get values for isr changes
      --write_log('Org :' ||G_VIEW_RECORD2(j).organization_id || 'Item Id '||G_VIEW_RECORD2(j).inventory_item_id );
      eis_po_xxwc_isr_util_pkg.get_isr_open_req_qty(G_VIEW_RECORD2(j).organization_id, G_VIEW_RECORD2(j).inventory_item_id, G_VIEW_RECORD2(j).on_ord, G_VIEW_RECORD2(j).open_req, G_VIEW_RECORD2(j).int_req, G_VIEW_RECORD2(j).dir_req);
      -- changes end
      l_item_cost            := NVL(G_VIEW_RECORD2(j).bpa_cost,G_VIEW_RECORD2(j).item_cost);
      l_avail_d              := G_VIEW_RECORD2(j).avail  * G_VIEW_RECORD2(j).aver_cost;
      l_on_ord               := G_VIEW_RECORD2(j).supply - NVL(G_VIEW_RECORD2(j).open_req,0);
      l_avail2               := G_VIEW_RECORD2(j).qoh    - G_VIEW_RECORD2(j).demand;
      l_vendor_name          := G_VIEW_RECORD2(j).vendor_name;
      l_vendor_number        := G_VIEW_RECORD2(j).vendor_number;
      l_vendor_site          := G_VIEW_RECORD2(j).vendor_site;
      IF G_VIEW_RECORD2(j).st ='I' OR G_VIEW_RECORD2(j).vendor_number IS NULL THEN
        l_organization_id    :=
        CASE
        WHEN G_VIEW_RECORD2(j).st = 'I' THEN
          G_VIEW_RECORD2(j).source_organization_id
        ELSE
          G_VIEW_RECORD2(j).organization_id
        END;
        get_vendor_info(G_VIEW_RECORD2(j).inventory_item_id, l_organization_id, l_vendor_name,l_vendor_number,l_vendor_site);
      END IF;
      G_VIEW_TAB2(j).org                   := G_VIEW_RECORD2(j).org;
      G_VIEW_TAB2(j).pre                   := G_VIEW_RECORD2(j).pre;
      G_VIEW_TAB2(j).item_number           := G_VIEW_RECORD2(j).item_number;
      G_VIEW_TAB2(j).vendor_num            := l_vendor_number;
      G_VIEW_TAB2(j).vendor_name           := l_vendor_Name;
      G_VIEW_TAB2(j).source                := G_VIEW_RECORD2(j).source;
      G_VIEW_TAB2(j).st                    := G_VIEW_RECORD2(j).st;
      G_VIEW_TAB2(j).description           := G_VIEW_RECORD2(j).description;
      G_VIEW_TAB2(j).cat                   := G_VIEW_RECORD2(j).cat_Class;
      G_VIEW_TAB2(j).pplt                  := G_VIEW_RECORD2(j).pplt;
      G_VIEW_TAB2(j).plt                   := G_VIEW_RECORD2(j).plt;
      G_VIEW_TAB2(j).uom                   := G_VIEW_RECORD2(j).uom;
      G_VIEW_TAB2(j).cl                    := G_VIEW_RECORD2(j).cl;
      G_VIEW_TAB2(j).stk_flag              := G_VIEW_RECORD2(j).stk;
      G_VIEW_TAB2(j).pm                    := G_VIEW_RECORD2(j).pm;
      G_VIEW_TAB2(j).minn                  := G_VIEW_RECORD2(j).min;
      G_VIEW_TAB2(j).maxn                  := G_VIEW_RECORD2(j).max;
      G_VIEW_TAB2(j).amu                   := G_VIEW_RECORD2(j).amu;
      G_VIEW_TAB2(j).mf_flag               := G_VIEW_RECORD2(j).mf_flag;
      G_VIEW_TAB2(j).hit6_store_Sales      := NULL;
      G_VIEW_TAB2(j).hit6_other_inv_sales  := NULL;
      G_VIEW_TAB2(j).aver_cost             := G_VIEW_RECORD2(j).aver_cost;
      G_VIEW_TAB2(j).item_cost             := l_item_cost;
      G_VIEW_TAB2(j).bpa_cost              := G_VIEW_RECORD2(j).bpa_cost;
      G_VIEW_TAB2(j).bpa                   := G_VIEW_RECORD2(j).bpa;
      G_VIEW_TAB2(j).QOH                   := G_VIEW_RECORD2(j).QOH;
      G_VIEW_TAB2(j).on_ord                := l_on_ord;
      G_VIEW_TAB2(j).available             := G_VIEW_RECORD2(j).avail;
      G_VIEW_TAB2(j).availabledollar       := l_avail_d;
      G_VIEW_TAB2(j).one_store_sale        := NULL;
      G_VIEW_TAB2(j).six_store_sale        := NULL;
      G_VIEW_TAB2(j).twelve_store_Sale     := NULL;
      G_VIEW_TAB2(j).one_other_inv_sale    := NULL;
      G_VIEW_TAB2(j).six_other_inv_sale    := NULL;
      G_VIEW_TAB2(j).twelve_other_inv_Sale := NULL;
      G_VIEW_TAB2(j).bin_loc               := G_VIEW_RECORD2(j).bin_loc;
      G_VIEW_TAB2(j).mc                    := G_VIEW_RECORD2(j).mc;
      G_VIEW_TAB2(j).fi_flag               := G_VIEW_RECORD2(j).fi;
      G_VIEW_TAB2(j).freeze_date           := G_VIEW_RECORD2(j).freeze_date;
      G_VIEW_TAB2(j).res                   := G_VIEW_RECORD2(j).res;
      G_VIEW_TAB2(j).thirteen_wk_avg_inv   := G_VIEW_RECORD2(j).thirteen_wk_avg_inv;
      G_VIEW_TAB2(j).thirteen_wk_an_cogs   := G_VIEW_RECORD2(j).thirteen_wk_an_cogs;
      G_VIEW_TAB2(j).turns                 := G_VIEW_RECORD2(j).turns;
      G_VIEW_TAB2(j).buyer                 := G_VIEW_RECORD2(j).buyer;
      G_VIEW_TAB2(j).ts                    := G_VIEW_RECORD2(j).ts;
      G_VIEW_TAB2(j).jan_store_sale        := NULL;
      G_VIEW_TAB2(j).feb_store_sale        := NULL;
      G_VIEW_TAB2(j).mar_store_sale        := NULL;
      G_VIEW_TAB2(j).apr_store_sale        := NULL;
      G_VIEW_TAB2(j).may_store_sale        := NULL;
      G_VIEW_TAB2(j).jun_store_sale        := NULL;
      G_VIEW_TAB2(j).jul_store_sale        := NULL;
      G_VIEW_TAB2(j).aug_store_sale        := NULL;
      G_VIEW_TAB2(j).sep_store_sale        := NULL;
      G_VIEW_TAB2(j).oct_store_sale        := NULL;
      G_VIEW_TAB2(j).nov_store_sale        := NULL;
      G_VIEW_TAB2(j).dec_store_sale        := NULL;
      G_VIEW_TAB2(j).jan_other_inv_sale    := NULL;
      G_VIEW_TAB2(j).feb_other_inv_sale    := NULL;
      G_VIEW_TAB2(j).mar_other_inv_sale    := NULL;
      G_VIEW_TAB2(j).apr_other_inv_sale    := NULL;
      G_VIEW_TAB2(j).may_other_inv_sale    := NULL;
      G_VIEW_TAB2(j).jun_other_inv_sale    := NULL;
      G_VIEW_TAB2(j).jul_other_inv_sale    := NULL;
      G_VIEW_TAB2(j).aug_other_inv_sale    := NULL;
      G_VIEW_TAB2(j).sep_other_inv_sale    := NULL;
      G_VIEW_TAB2(j).oct_other_inv_sale    := NULL;
      G_VIEW_TAB2(j).nov_other_inv_sale    := NULL;
      G_VIEW_TAB2(j).dec_other_inv_sale    := NULL;
      G_VIEW_TAB2(j).hit4_store_Sales      := NULL;
      G_VIEW_TAB2(j).hit4_other_inv_Sales  := NULL;
      G_VIEW_TAB2(j).so                    := G_VIEW_RECORD2(j).so;
      G_VIEW_TAB2(j).inventory_item_id     := G_VIEW_RECORD2(j).inventory_item_id;
      G_VIEW_TAB2(j).organization_id       := G_VIEW_RECORD2(j).organization_id;
      G_VIEW_TAB2(j).set_of_books_id       := G_VIEW_RECORD2(j).set_of_books_id;
      G_VIEW_TAB2(j).org_name              := G_VIEW_RECORD2(j).organization_name;
      G_VIEW_TAB2(j).district              := G_VIEW_RECORD2(j).district;
      G_VIEW_TAB2(j).region                := G_VIEW_RECORD2(j).region;
      G_VIEW_TAB2(j).inv_Cat_Seg1          := G_VIEW_RECORD2(j).inv_cat_seg1;
      G_VIEW_TAB2(j).wt                    := G_VIEW_RECORD2(j).wt;
      G_VIEW_TAB2(j).ss                    := G_VIEW_RECORD2(j).ss;
      G_VIEW_TAB2(j).fml                   := G_VIEW_RECORD2(j).fml;
      G_VIEW_TAB2(j).open_req              := G_VIEW_RECORD2(j).open_req;
      G_VIEW_TAB2(j).sourcing_rule         := G_VIEW_RECORD2(j).sourcing_rule;
      G_VIEW_TAB2(j).clt                   := G_VIEW_RECORD2(j).clt;
      G_VIEW_TAB2(j).avail2                := l_avail2;
      G_VIEW_TAB2(j).int_req               := G_VIEW_RECORD2(j).int_req;
      G_VIEW_TAB2(j).dir_req               := G_VIEW_RECORD2(j).dir_req;
      G_VIEW_TAB2(j).demand                := G_VIEW_RECORD2(j).demand;
      G_VIEW_TAB2(j).ITEM_STATUS_CODE      := G_VIEW_RECORD2(j).ITEM_STATUS_CODE;
      G_VIEW_TAB2(j).SITE_VENDOR_NUM       := G_VIEW_RECORD2(j).vendor_number;
      G_VIEW_TAB2(j).vendor_site           := G_VIEW_RECORD2(j).vendor_site;
      l_count                              := l_count + 1;
    END LOOP;
    IF l_count            >= 1 THEN
      lv_program_location := '5, Insert into eis_xxwc_po_isr_tab ';
      forall j IN 1 .. G_VIEW_TAB2.count
      INSERT
      INTO xxeis.eis_xxwc_po_isr_tab_tmp VALUES g_view_tab2
        (
          j
        );
      COMMIT;
    END IF;
    lv_program_location := '6, Delete tab ';
    G_VIEW_TAB2.delete;
    G_VIEW_RECORD2.delete;
    IF l_ref_cursor2%NOTFOUND THEN
      CLOSE l_ref_cursor2;
      EXIT;
    END IF;
  END LOOP;
  write_log(' Bulk Collect Main query End '|| TO_CHAR(sysdate,'DD-MON-YYYY HH:MI:SS') );
  -- Create index
  BEGIN
    EXECUTE IMMEDIATE 'CREATE INDEX XXEIS.XXWC_PO_ISR_TAB_TMP_N1 ON XXEIS.EIS_XXWC_PO_ISR_TAB_TMP              
(                            
INVENTORY_ITEM_ID,                
ORGANIZATION_ID              
) TABLESPACE XXEIS_IDX ';
  EXCEPTION
  WHEN OTHERS THEN
    write_log(' Error on creating the index XXWC_PO_ISR_TAB_TMP_N1 '|| SQLERRM );
  END;
  -- Derive last 12 periods based on sysdate
  FOR c_distinct_sob_id IN
  (SELECT 2061 set_of_books_id FROM dual
  )
  LOOP
    period_name_tab.DELETE;
    --get current period and year
    lv_program_location :='7, Getting current period and year';
    SELECT period_name,
      period_year,
      effective_period_num
    INTO l_period_name,
      l_current_period_year,
      l_current_effective_period_num
    FROM gl_period_statuses
    WHERE application_id=101
    AND TRUNC(sysdate) BETWEEN start_date AND end_date
    AND adjustment_period_flag <>'Y'
    AND set_of_books_id         = c_distinct_sob_id.set_of_books_id;
    --period_name_tab(l_period_count):=l_period_name;
    lv_program_location :='8, current period and year is='||l_period_name||','||l_current_period_year;
    write_log(lv_program_location);
    --store next period names for existing year in array list
    LOOP
      l_period_count                :=l_period_count                +1;
      l_current_effective_period_num:=l_current_effective_period_num-1;
      BEGIN
        SELECT period_name
        INTO period_name_tab(l_period_count)
        FROM gl_period_statuses
        WHERE application_id        =101
        AND effective_period_num    = l_current_effective_period_num
        AND period_year             = l_current_period_year
        AND adjustment_period_flag <>'Y'
        AND set_of_books_id         = c_distinct_sob_id.set_of_books_id;
        write_log('period_name_tab(l_period_count)='||period_name_tab(l_period_count));
        write_log('l_current_effective_period_num is:'||l_current_effective_period_num);
        write_log('l_current_period_year is:'||l_current_period_year);
        lv_program_location :=' Next period and year is='||period_name_tab(l_period_count)||','||l_current_period_year;
        --write_log(lv_program_location);
      EXCEPTION
      WHEN OTHERS THEN
        l_period_count:=l_period_count-1;
        write_log('Inside Exception ='||l_period_count);
        EXIT;
      END;
      EXIT
    WHEN l_period_count=12;
    END LOOP;
    lv_program_location :='9, Before change in year';
    write_log('l_period_count before change in year ='||l_period_count);
    --when there is change in year then store next period names for prevous year in array list
    FOR c_period_name IN
    (SELECT period_name
    FROM
      (SELECT period_name
      FROM gl_period_statuses
      WHERE application_id        = 101
      AND period_year             = (l_current_period_year-1)
      AND set_of_books_id         = c_distinct_sob_id.set_of_books_id
      AND adjustment_period_flag <>'Y'
      ORDER BY effective_period_num DESC
      )
    WHERE ROWNUM<=(12-l_period_count)
    )
    LOOP
      write_log('l_period_count='||l_period_count);
      l_period_count                 :=l_period_count+1;
      period_name_tab(l_period_count):=c_period_name.period_name;
      lv_program_location            :=' Next period and year is='||period_name_tab(l_period_count)||','||(l_current_period_year-1);
      write_log(lv_program_location);
    END LOOP;
  END LOOP;
  write_log('After Period tab'|| TO_CHAR(sysdate,'DD-MON-YYYY HH:MI:SS') );
  lv_program_location :='10, For each period';
  FOR period_name_cnt IN 1..12
  LOOP
    lv_program_location :=' getting update period name for ='||period_name_tab(period_name_cnt);
    write_log(lv_program_location);
    IF upper(period_name_tab(period_name_cnt)) LIKE '%JAN%' THEN
      ordered_period_name_tab(1) := period_name_tab(period_name_cnt);
    elsif upper(period_name_tab(period_name_cnt)) LIKE '%FEB%' THEN
      ordered_period_name_tab(2) := period_name_tab(period_name_cnt);
    elsif upper(period_name_tab(period_name_cnt)) LIKE '%MAR%' THEN
      ordered_period_name_tab(3) := period_name_tab(period_name_cnt);
    elsif upper(period_name_tab(period_name_cnt)) LIKE '%APR%' THEN
      ordered_period_name_tab(4) := period_name_tab(period_name_cnt);
    elsif upper(period_name_tab(period_name_cnt)) LIKE '%MAY%' THEN
      ordered_period_name_tab(5) := period_name_tab(period_name_cnt);
    elsif upper(period_name_tab(period_name_cnt)) LIKE '%JUN%' THEN
      ordered_period_name_tab(6) := period_name_tab(period_name_cnt);
    elsif upper(period_name_tab(period_name_cnt)) LIKE '%JUL%' THEN
      ordered_period_name_tab(7) := period_name_tab(period_name_cnt);
    elsif upper(period_name_tab(period_name_cnt)) LIKE '%AUG%' THEN
      ordered_period_name_tab(8) := period_name_tab(period_name_cnt);
    elsif upper(period_name_tab(period_name_cnt)) LIKE '%SEP%' THEN
      ordered_period_name_tab(9) := period_name_tab(period_name_cnt);
    elsif upper(period_name_tab(period_name_cnt)) LIKE '%OCT%' THEN
      ordered_period_name_tab(10) := period_name_tab(period_name_cnt);
    elsif upper(period_name_tab(period_name_cnt)) LIKE '%NOV%' THEN
      ordered_period_name_tab(11) := period_name_tab(period_name_cnt);
    elsif upper(period_name_tab(period_name_cnt)) LIKE '%DEC%' THEN
      ordered_period_name_tab(12) := period_name_tab(period_name_cnt);
    END IF;
  END LOOP;
  lv_program_location :='12, Populate 12 buckets';
  write_log('populate 12 bucket store sales Start :'|| TO_CHAR(sysdate,'DD-MON-YYYY HH:MI:SS') );
  l_period_sales :=
  'select TAB.inventory_item_id  inventory_item_id                           
,tab.organization_id  organization_id                           
,sum(tab.jan1)        jan_store_sales                            
,sum(tab.feb1)        feb_store_sales                           
,sum(tab.mar1)        mar_store_sales                           
,sum(tab.apr1)        apr_store_sales                           
,sum(tab.May1)        may_store_sales                           
,sum(tab.june1)       jun_store_sales                           
,sum(tab.july1)       jul_store_sales                           
,sum(tab.aug1)        aug_store_sales                           
,sum(tab.sept1)       sep_store_sales                           
,sum(tab.oct1)        oct_store_sales                           
,sum(tab.nov1)        nov_store_sales                           
,sum(tab.dec1)        dec_store_Sales                      
from                               
(                           
select tab1.inventory_item_id                                 
,tab1.organization_id,                             
case when  gps.period_name = '''
  ||ordered_period_name_tab(1)||''' then                                 
mdh.sales_order_demand + NVL(std_wip_usage,0)                             
end jan1,                               
case when gps.period_name = '''||ordered_period_name_tab(2)||''' then                                 
mdh.sales_order_demand + NVL(std_wip_usage,0)                             
end Feb1,                               
case when  gps.period_name = '''||ordered_period_name_tab(3)||''' then                                 
mdh.sales_order_demand + NVL(std_wip_usage,0)                             
end Mar1,                               
case when  gps.period_name = '''||ordered_period_name_tab(4)||''' then                                 
mdh.sales_order_demand + NVL(std_wip_usage,0)                             
end Apr1,                               
case when  gps.period_name = '''||ordered_period_name_tab(5)||
  ''' then                                 
mdh.sales_order_demand + NVL(std_wip_usage,0)                             
end May1,                               
case when  gps.period_name = '''||ordered_period_name_tab(6)||''' then                                 
mdh.sales_order_demand + NVL(std_wip_usage,0)                             
end june1,                               
case when   gps.period_name = '''||ordered_period_name_tab(7)||''' then                                 
mdh.sales_order_demand + NVL(std_wip_usage,0)                             
end july1,                               
case when  gps.period_name = '''||ordered_period_name_tab(8)||''' then                                 
mdh.sales_order_demand + NVL(std_wip_usage,0)                             
end Aug1,                               
case when gps.period_name = '''||ordered_period_name_tab(9)||
  ''' then                                 
mdh.sales_order_demand + NVL(std_wip_usage,0)                             
end Sept1,                               
case when  gps.period_name = '''||ordered_period_name_tab(10)||''' then                                 
mdh.sales_order_demand + NVL(std_wip_usage,0)                             
end Oct1,                               
case when  gps.period_name = '''||ordered_period_name_tab(11)||''' then                                 
mdh.sales_order_demand + NVL(std_wip_usage,0)                             
end Nov1,                               
case when  gps.period_name = '''||ordered_period_name_tab(12)||
  ''' then                                 
mdh.sales_order_demand + NVL(std_wip_usage,0)                             
end Dec1                         
from  mtl_demand_histories mdh,                             
gl_period_statuses    gps,                             
xxeis.eis_xxwc_po_isr_tab_tmp tab1                       
where 1=1                         
and mdh.inventory_item_id         = tab1.inventory_item_id                         
and mdh.organization_id           = tab1.organization_id                         
and gps.application_id            = 101                         
and mdh.period_type               = 3                         
and gps.set_of_books_id           = 2061                         
and mdh.period_start_date between gps.start_date and gps.end_date                        
) TAB                    
group by tab.inventory_item_id                            
,tab.organization_id';
  write_log(' Period Sales SQL  '|| l_period_sales );
  write_log(' Period Sales SQL start '|| TO_CHAR(sysdate,'DD-MON-YYYY HH:MI:SS') );
  lv_program_location :='13, Period Sales';
  OPEN l_ref_cursor4 FOR l_period_sales;
  LOOP
    FETCH L_REF_CURSOR4 BULK COLLECT INTO G_VIEW_RECORD4 LIMIT 10000;
    l_count := 0;
    FOR j IN 1..G_VIEW_RECORD4.count
    LOOP
      lv_program_location              :='14, Period Sales for each record';
      G_VIEW_TAB4(j).inventory_item_id := G_VIEW_RECORD4(j).inventory_item_id;
      G_VIEW_TAB4(j).organization_id   := G_VIEW_RECORD4(j).organization_id;
      G_VIEW_TAB4(j).jan_store_sale    := G_VIEW_RECORD4(j).jan_store_sale;
      G_VIEW_TAB4(j).feb_store_sale    := G_VIEW_RECORD4(j).feb_store_sale;
      G_VIEW_TAB4(j).mar_store_sale    := G_VIEW_RECORD4(j).mar_store_sale;
      G_VIEW_TAB4(j).apr_store_sale    := G_VIEW_RECORD4(j).apr_store_sale;
      G_VIEW_TAB4(j).may_store_sale    := G_VIEW_RECORD4(j).may_store_sale;
      G_VIEW_TAB4(j).jun_store_sale    := G_VIEW_RECORD4(j).jun_store_sale;
      G_VIEW_TAB4(j).jul_store_sale    := G_VIEW_RECORD4(j).jul_store_sale;
      G_VIEW_TAB4(j).aug_store_sale    := G_VIEW_RECORD4(j).aug_store_sale;
      G_VIEW_TAB4(j).sep_store_sale    := G_VIEW_RECORD4(j).sep_store_sale;
      G_VIEW_TAB4(j).oct_store_sale    := G_VIEW_RECORD4(j).oct_store_sale;
      G_VIEW_TAB4(j).nov_store_sale    := G_VIEW_RECORD4(j).nov_store_sale;
      G_VIEW_TAB4(j).dec_store_sale    := G_VIEW_RECORD4(j).dec_store_sale;
      l_count                          := l_count+1;
    END LOOP;
    IF l_count >= 1 THEN
      forall j IN 1 .. G_VIEW_TAB4.count
      INSERT INTO xxeis.eis_xxwc_po_isr_psales_tab VALUES g_view_tab4
        (j
        );
      COMMIT;
    END IF;
    G_VIEW_TAB4.delete;
    G_VIEW_RECORD4.delete;
    IF l_ref_cursor4%NOTFOUND THEN
      CLOSE l_ref_cursor4;
      EXIT;
    END IF;
  END LOOP;
  BEGIN
    EXECUTE IMMEDIATE 'CREATE INDEX XXEIS.XXWC_PO_ISR_PSALES_TAB_N1 ON XXEIS.EIS_XXWC_PO_ISR_PSALES_TAB              
(                            
INVENTORY_ITEM_ID,                
ORGANIZATION_ID              
) TABLESPACE XXEIS_IDX ';
  EXCEPTION
  WHEN OTHERS THEN
    write_log(' Error on creating the index XXWC_PO_ISR_PSALES_TAB_N1 '|| SQLERRM );
  END;
  write_log('populate 12 bucket Other store sales Start :'|| TO_CHAR(sysdate,'DD-MON-YYYY HH:MI:SS') );
  lv_program_location  :='13, 12 buckets other sales ';
  l_period_other_sales :=
  ' select TAB1.inventory_item_id  inventory_item_id                           
,tab1.organization_id  organization_id                           
,sum(tab.jan_store_sale)        jan_store_other_sales                            
,sum(tab.feb_store_sale)        feb_store_other_sales                           
,sum(tab.mar_store_sale)        mar_store_other_sales                           
,sum(tab.apr_store_sale)        apr_store_other_sales                           
,sum(tab.may_store_sale)        may_store_other_sales                           
,sum(tab.jun_store_sale)       jun_store_other_sales                           
,sum(tab.jul_store_sale)       jul_store_other_sales                           
,sum(tab.aug_store_sale)        aug_store_other_sales                           
,sum(tab.sep_store_sale)       sep_store_other_sales                           
,sum(tab.oct_store_sale)        oct_store_other_sales                           
,sum(tab.nov_store_sale)        nov_store_other_sales                           
,sum(tab.dec_store_sale)        dec_store_other_Sales                      
from  mtl_system_items_kfv msi,                            
xxeis.eis_xxwc_po_isr_tab_tmp tab1,                            
xxeis.eis_xxwc_po_isr_psales_tab tab                       
where 1=1                         
and msi.inventory_item_id         = tab1.inventory_item_id                         
and msi.source_organization_id    = tab1.organization_id                         
and msi.inventory_item_id         = tab.inventory_item_id                         
and msi.organization_id           = tab.organization_id                         
AND msi.source_organization_id <> msi.organization_id                    
group by tab1.inventory_item_id                            
,tab1.organization_id '
  ;
  write_log(' Period Sales SQL  '|| l_period_other_sales );
  write_log(' Period Other Sales SQL  Start '|| TO_CHAR(sysdate,'DD-MON-YYYY HH:MI:SS') );
  lv_program_location :='13, 12 buckets other sales start ';
  OPEN l_ref_cursor5 FOR l_period_other_sales;
  LOOP
    FETCH L_REF_CURSOR5 BULK COLLECT INTO G_VIEW_RECORD5 LIMIT 10000;
    l_count := 0;
    FOR j IN 1..G_VIEW_RECORD5.count
    LOOP
      lv_program_location               :='15, 12 buckets other sales each record ';
      G_VIEW_TAB5(j).inventory_item_id  := G_VIEW_RECORD5(j).inventory_item_id;
      G_VIEW_TAB5(j).organization_id    := G_VIEW_RECORD5(j).organization_id;
      G_VIEW_TAB5(j).jan_other_inv_sale := G_VIEW_RECORD5(j).jan_other_inv_sale;
      G_VIEW_TAB5(j).feb_other_inv_sale := G_VIEW_RECORD5(j).feb_other_inv_sale;
      G_VIEW_TAB5(j).mar_other_inv_sale := G_VIEW_RECORD5(j).mar_other_inv_sale;
      G_VIEW_TAB5(j).apr_other_inv_sale := G_VIEW_RECORD5(j).apr_other_inv_sale;
      G_VIEW_TAB5(j).may_other_inv_sale := G_VIEW_RECORD5(j).may_other_inv_sale;
      G_VIEW_TAB5(j).jun_other_inv_sale := G_VIEW_RECORD5(j).jun_other_inv_sale;
      G_VIEW_TAB5(j).jul_other_inv_sale := G_VIEW_RECORD5(j).jul_other_inv_sale;
      G_VIEW_TAB5(j).aug_other_inv_sale := G_VIEW_RECORD5(j).aug_other_inv_sale;
      G_VIEW_TAB5(j).sep_other_inv_sale := G_VIEW_RECORD5(j).sep_other_inv_sale;
      G_VIEW_TAB5(j).oct_other_inv_sale := G_VIEW_RECORD5(j).oct_other_inv_sale;
      G_VIEW_TAB5(j).nov_other_inv_sale := G_VIEW_RECORD5(j).nov_other_inv_sale;
      G_VIEW_TAB5(j).dec_other_inv_sale := G_VIEW_RECORD5(j).dec_other_inv_sale;
      l_count                           := l_count+1;
    END LOOP;
    IF l_count >= 1 THEN
      forall j IN 1 .. G_VIEW_TAB5.count
      INSERT INTO xxeis.eis_xxwc_po_isr_posales_tab VALUES g_view_tab5
        (j
        );
      COMMIT;
    END IF;
    G_VIEW_TAB5.delete;
    G_VIEW_RECORD5.delete;
    IF l_ref_cursor5%NOTFOUND THEN
      CLOSE l_ref_cursor5;
      EXIT;
    END IF;
  END LOOP;
  BEGIN
    EXECUTE IMMEDIATE 'CREATE INDEX XXEIS.XXWC_PO_ISR_POSALES_TAB_N1 ON XXEIS.EIS_XXWC_PO_ISR_POSALES_TAB              
(                            
INVENTORY_ITEM_ID,                
ORGANIZATION_ID              
) TABLESPACE XXEIS_IDX ';
  EXCEPTION
  WHEN OTHERS THEN
    write_log(' Error on creating the index XXWC_PO_ISR_POSALES_TAB_N1 '|| SQLERRM );
  END;
  write_log('After 12 buckts population :'|| TO_CHAR(sysdate,'DD-MON-YYYY HH:MI:SS') );
  lv_program_location :='16, hit sales ';
  l_hits_sql          :=
  'select organization_id, inventory_item_id, count(distinct hit4) HIT4_STORE_SALES,count(distinct hit6) HIT6_STORE_SALES                  
from (                         
select  oel.ship_from_org_id organization_id, oel.inventory_item_id inventory_item_id,                          
CASE WHEN ((ott.name != ''COUNTER LINE'' and Trunc(oel.Actual_Shipment_Date) >  (Trunc(sysdate-(182))))                                          
OR                             
(ott.name = ''COUNTER LINE'' and Trunc(oel.fulfillment_Date) >  (Trunc(sysdate-(182))))) THEN                              
oeh.header_id                         
END  hit6 ,                          
CASE WHEN ((ott.name != ''COUNTER LINE'' and Trunc(oel.Actual_Shipment_Date) >  (Trunc(sysdate-(112))))                                          
OR                             
(ott.name = ''COUNTER LINE'' and Trunc(oel.fulfillment_Date) >  (Trunc(sysdate-(112))))) THEN                             
oeh.header_id                         
END  hit4                        
from   oe_order_headers_all      oeh,                               
oe_order_lines_all        oel,                               
oe_transaction_types_tl ott,                               
xxeis.eis_xxwc_po_isr_tab_tmp tab                        
where 1=1                        
and oel.inventory_item_id       = tab.inventory_item_id                        
and oel.ship_from_org_id        = tab.organization_id                        
and oel.header_id               = oeh.header_id                        
AND oeh.order_number             is not null                        
AND oel.line_type_id             = ott.transaction_type_id                        
And Not Exists ( Select 1                                              
From Mtl_Parameters Mp                                              
Where  mp.Organization_ID = oel.Ship_to_Org_ID )                        
AND ((ott.name != ''COUNTER LINE'' and Trunc(oel.Actual_Shipment_Date) >  (Trunc(sysdate-(182))))                                          
OR                             
(ott.name = ''COUNTER LINE'' and Trunc(oel.fulfillment_Date) >  (Trunc(sysdate-(182)))))                                                             
and  oel.Flow_Status_Code       = ''CLOSED''                        
and oel.line_category_code      <> ''RETURN''                        
AND  oel.invoice_interface_status_code = ''YES''                                    
and not exists (select 1 from dual where (DECODE(oel.line_category_code,''RETURN'',(oel.ordered_quantity*-1),oel.ordered_quantity)                                                                                                                                                                                     

) =                                                                  
(select -1* sum(DECODE(olr.line_category_code,''RETURN'',(olr.ordered_quantity*-1),olr.ordered_quantity))                                                                    
from oe_order_lines_all olr                                                                                                                         
where olr.reference_header_id = oeh.header_id                                                                      
AND olr.reference_line_id   = oel.line_id                                                                                                                        
AND olr.line_category_code  = ''RETURN''                                                                     
and olr.return_context      = ''ORDER''                                                                                                                 
)                                        
)                   
) group by organization_id , inventory_item_id'
  ;
  write_log(' Hit Sales SQL  '|| l_hits_sql );
  write_log(' Hit Sales Start '|| TO_CHAR(sysdate,'DD-MON-YYYY HH:MI:SS') );
  OPEN l_ref_cursor5 FOR l_hits_sql;
  LOOP
    FETCH L_REF_CURSOR5 BULK COLLECT INTO G_VIEW_RECORD6 LIMIT 10000;
    l_count := 0;
    FOR j IN 1..G_VIEW_RECORD6.count
    LOOP
      lv_program_location              :='17, hit 4 sales populate to record ';
      G_VIEW_TAB6(j).organization_id   := G_VIEW_RECORD6(j).organization_id;
      G_VIEW_TAB6(j).inventory_item_id := G_VIEW_RECORD6(j).inventory_item_id;
      G_VIEW_TAB6(j).HIT4_STORE_SALES  := G_VIEW_RECORD6(j).HIT4_STORE_SALES;
      G_VIEW_TAB6(j).HIT6_STORE_SALES  := G_VIEW_RECORD6(j).HIT6_STORE_SALES;
      l_count                          := l_count+1;
    END LOOP;
    IF l_count >= 1 THEN
      forall j IN 1 .. G_VIEW_TAB6.count
      INSERT INTO xxeis.eis_xxwc_po_isr_hits_tab VALUES g_view_tab6
        (j
        );
      COMMIT;
    END IF;
    G_VIEW_TAB6.delete;
    G_VIEW_RECORD6.delete;
    IF l_ref_cursor5%NOTFOUND THEN
      CLOSE l_ref_cursor5;
      EXIT;
    END IF;
  END LOOP;
  BEGIN
    EXECUTE IMMEDIATE 'CREATE INDEX XXEIS.XXWC_PO_ISR_HIT_N1 ON XXEIS.EIS_XXWC_PO_ISR_HITS_TAB              
(                            
INVENTORY_ITEM_ID,                
ORGANIZATION_ID              
) TABLESPACE XXEIS_IDX ';
  EXCEPTION
  WHEN OTHERS THEN
    write_log(' Error on creating the index XXWC_PO_ISR_HIT_N1 '|| SQLERRM );
  END;
  lv_program_location :='18, hit sales ';
  l_hits_sql          :=
  ' select    msi.source_organization_id  organization_id                           
,TAB1.inventory_item_id  inventory_item_id                           
,sum(tab.HIT4_STORE_SALES)        HIT4_OTHER_INV_SALES                            
,sum(tab.HIT6_STORE_SALES)        HIT6_OTHER_INV_SALES                         
from  mtl_system_items_kfv msi,                            
xxeis.eis_xxwc_po_isr_tab_tmp tab1,                            
xxeis.eis_xxwc_po_isr_hits_tab tab                       
where 1=1                         
and msi.inventory_item_id         = tab1.inventory_item_id                         
and msi.source_organization_id    = tab1.organization_id                         
and msi.inventory_item_id         = tab.inventory_item_id                         
and msi.organization_id           = tab.organization_id                         
AND msi.source_organization_id <> msi.organization_id                    
group by tab1.inventory_item_id                            
,msi.source_organization_id '
  ;
  write_log(' Hit Other Sales SQL  '|| l_hits_sql );
  write_log(' Hit other Sales Start '|| TO_CHAR(sysdate,'DD-MON-YYYY HH:MI:SS') );
  OPEN l_ref_cursor5 FOR l_hits_sql;
  LOOP
    FETCH L_REF_CURSOR5 BULK COLLECT INTO G_VIEW_RECORD7 LIMIT 10000;
    l_count := 0;
    FOR j IN 1..G_VIEW_RECORD7.count
    LOOP
      lv_program_location                 :='19, hit4 other sales ';
      G_VIEW_TAB7(j).organization_id      := G_VIEW_RECORD7(j).organization_id;
      G_VIEW_TAB7(j).inventory_item_id    := G_VIEW_RECORD7(j).inventory_item_id;
      G_VIEW_TAB7(j).HIT4_OTHER_INV_SALES := G_VIEW_RECORD7(j).HIT4_OTHER_INV_SALES;
      G_VIEW_TAB7(j).HIT6_OTHER_INV_SALES := G_VIEW_RECORD7(j).HIT6_OTHER_INV_SALES;
      l_count                             := l_count+1;
    END LOOP;
    IF l_count >= 1 THEN
      forall j IN 1 .. G_VIEW_TAB7.count
      INSERT INTO xxeis.eis_xxwc_po_isr_ohits_tab VALUES g_view_tab7
        (j
        );
      COMMIT;
    END IF;
    G_VIEW_TAB7.delete;
    G_VIEW_RECORD7.delete;
    IF l_ref_cursor5%NOTFOUND THEN
      CLOSE l_ref_cursor5;
      EXIT;
    END IF;
  END LOOP;
  BEGIN
    EXECUTE IMMEDIATE 'CREATE INDEX XXEIS.XXWC_PO_ISR_OHITS_N1 ON XXEIS.EIS_XXWC_PO_ISR_OHITS_TAB              
(                            
INVENTORY_ITEM_ID,                
ORGANIZATION_ID              
) TABLESPACE XXEIS_IDX ';
  EXCEPTION
  WHEN OTHERS THEN
    write_log(' Error on creating the index XXWC_PO_ISR_OHITS_N1 '|| SQLERRM );
  END;
  write_log('After 12 buckts population :'|| TO_CHAR(sysdate,'DD-MON-YYYY HH:MI:SS') );
  lv_program_location :='20,  sales 1,6,12 bucket sql';
  write_log(' 1,6,12,Sales buckets :'|| TO_CHAR(sysdate,'DD-MON-YYYY HH:MI:SS') );
  l_sales_sql :=
  'select organization_id, inventory_item_id,                  
sum(twelve_store_sale) twelve_store_sale,                  
sum(six_store_sale)    six_store_sale,                  
sum(one_store_sale)    one_store_sale            
FROM (                    
SELECT ol.ship_from_org_id organization_id , ol.inventory_item_id inventory_item_id,                           
case when ((ott.name != ''COUNTER LINE'' and Trunc(ol.Actual_Shipment_Date) >  (Trunc(sysdate-(365))))                                      
OR                           
(ott.name = ''COUNTER LINE'' and Trunc(ol.fulfillment_Date) >  (Trunc(sysdate-(365))))) THEN                             
DECODE(ol.line_category_code,''RETURN'',(ol.ordered_quantity*-1),ol.ordered_quantity)                             
END twelve_store_sale,                           
case when ((ott.name != ''COUNTER LINE'' and Trunc(ol.Actual_Shipment_Date) >  (Trunc(sysdate-(182))))                                      
OR                           
(ott.name = ''COUNTER LINE'' and Trunc(ol.fulfillment_Date) >  (Trunc(sysdate-(182))))) THEN                             
DECODE(ol.line_category_code,''RETURN'',(ol.ordered_quantity*-1),ol.ordered_quantity)                             
END six_store_sale,                          
case when ((ott.name != ''COUNTER LINE'' and Trunc(ol.Actual_Shipment_Date) >  (Trunc(sysdate-(30))))                                      
OR                           
(ott.name = ''COUNTER LINE'' and Trunc(ol.fulfillment_Date) >  (Trunc(sysdate-(30))))) THEN                             
DECODE(ol.line_category_code,''RETURN'',(ol.ordered_quantity*-1),ol.ordered_quantity)                             
END one_store_sale                     
from oe_order_lines_all ol,                          
oe_order_headers_all oh,                          
oe_transaction_types_tl ott,                          
xxeis.eis_xxwc_po_isr_tab_tmp tab                    
where oh.header_id          = ol.header_id                      
and ol.inventory_item_id       = tab.inventory_item_id                      
and ol.ship_from_org_id     = tab.organization_id                      
and ol.flow_status_code   = ''CLOSED''                      
and ol.invoice_interface_status_code = ''YES''                       
AND oh.order_number             is not null                      
AND ol.line_type_id             = ott.transaction_type_id                      
And Not Exists ( Select 1                                      
From Mtl_Parameters Mp                                      
Where  mp.Organization_ID = ol.Ship_to_Org_ID )                      
AND ((ott.name != ''COUNTER LINE'' and Trunc(ol.Actual_Shipment_Date) >  (Trunc(sysdate-(365))))                                       
OR                           
(ott.name = ''COUNTER LINE'' and Trunc(ol.fulfillment_Date) >  (Trunc(sysdate-(365)))))                 
) group by organization_id, inventory_item_id '
  ;
  write_log(' 1,6,12 Buckets Sales SQL  '|| l_sales_sql );
  write_log(' 1,6,12 buckets Sales Start '|| TO_CHAR(sysdate,'DD-MON-YYYY HH:MI:SS') );
  OPEN l_ref_cursor5 FOR l_sales_sql;
  LOOP
    FETCH L_REF_CURSOR5 BULK COLLECT INTO G_VIEW_RECORD8 LIMIT 10000;
    l_count := 0;
    FOR j IN 1..G_VIEW_RECORD8.count
    LOOP
      lv_program_location              :='21, sales 1,6,12 bucket population ';
      G_VIEW_TAB8(j).organization_id   := G_VIEW_RECORD8(j).organization_id;
      G_VIEW_TAB8(j).inventory_item_id := G_VIEW_RECORD8(j).inventory_item_id;
      G_VIEW_TAB8(j).twelve_store_sale := G_VIEW_RECORD8(j).twelve_store_sale;
      G_VIEW_TAB8(j).six_store_sale    := G_VIEW_RECORD8(j).six_store_sale;
      G_VIEW_TAB8(j).one_store_sale    := G_VIEW_RECORD8(j).one_store_sale;
      l_count                          := l_count+1;
    END LOOP;
    IF l_count >= 1 THEN
      forall j IN 1 .. G_VIEW_TAB8.count
      INSERT INTO xxeis.eis_xxwc_po_isr_sales_tab VALUES g_view_tab8
        (j
        );
      COMMIT;
    END IF;
    G_VIEW_TAB8.delete;
    G_VIEW_RECORD8.delete;
    IF l_ref_cursor5%NOTFOUND THEN
      CLOSE l_ref_cursor5;
      EXIT;
    END IF;
  END LOOP;
  BEGIN
    EXECUTE IMMEDIATE 'CREATE INDEX XXEIS.XXWC_PO_ISR_SALES_TAB_N1 ON XXEIS.EIS_XXWC_PO_ISR_SALES_TAB              
(                            
INVENTORY_ITEM_ID,                
ORGANIZATION_ID              
) TABLESPACE XXEIS_IDX ';
  EXCEPTION
  WHEN OTHERS THEN
    write_log(' Error on creating the index XXWC_PO_ISR_SALES_TAB_N1 '|| SQLERRM );
  END;
  write_log(' 1,6,12,Sales buckets :'|| TO_CHAR(sysdate,'DD-MON-YYYY HH:MI:SS') );
  lv_program_location :='22, Other sales ';
  l_sales_sql         :=
  'select    msi.source_organization_id  organization_id                           
,TAB1.inventory_item_id  inventory_item_id               
,sum(tab.twelve_store_sale)     twelve_other_inv_sale                            
,sum(tab.six_store_sale)        six_other_inv_sale                            
,sum(tab.one_STORE_SALE)        one_other_inv_sale                       
from  mtl_system_items_kfv msi,                            
xxeis.eis_xxwc_po_isr_tab_tmp tab1,                            
xxeis.eis_xxwc_po_isr_sales_tab tab                                                   
where 1=1                         
and msi.inventory_item_id         = tab1.inventory_item_id                         
and msi.source_organization_id    = tab1.organization_id                         
and msi.inventory_item_id         = tab.inventory_item_id                         
and msi.organization_id           = tab.organization_id                                                  
AND msi.source_organization_id <> msi.organization_id                    
group by tab1.inventory_item_id                            
,msi.source_organization_id '
  ;
  write_log(' Other Sales 1,6,12 SQL  '|| l_hits_sql );
  write_log(' Other Sales 1,6,12 Start '|| TO_CHAR(sysdate,'DD-MON-YYYY HH:MI:SS') );
  OPEN l_ref_cursor5 FOR l_sales_sql;
  LOOP
    FETCH L_REF_CURSOR5 BULK COLLECT INTO G_VIEW_RECORD9 LIMIT 10000;
    l_count := 0;
    FOR j IN 1..G_VIEW_RECORD9.count
    LOOP
      lv_program_location                  :='23, Other Sales 1,6,12 population ';
      G_VIEW_TAB9(j).organization_id       := G_VIEW_RECORD9(j).organization_id;
      G_VIEW_TAB9(j).inventory_item_id     := G_VIEW_RECORD9(j).inventory_item_id;
      G_VIEW_TAB9(j).twelve_other_inv_sale := G_VIEW_RECORD9(j).twelve_other_inv_sale;
      G_VIEW_TAB9(j).six_other_inv_sale    := G_VIEW_RECORD9(j).six_other_inv_sale;
      G_VIEW_TAB9(j).one_other_inv_sale    := G_VIEW_RECORD9(j).one_other_inv_sale;
      l_count                              := l_count+1;
    END LOOP;
    IF l_count >= 1 THEN
      forall j IN 1 .. G_VIEW_TAB9.count
      INSERT INTO xxeis.eis_xxwc_po_isr_osales_tab VALUES g_view_tab9
        (j
        );
      COMMIT;
    END IF;
    G_VIEW_TAB9.delete;
    G_VIEW_RECORD9.delete;
    IF l_ref_cursor5%NOTFOUND THEN
      CLOSE l_ref_cursor5;
      EXIT;
    END IF;
  END LOOP;
  BEGIN
    EXECUTE IMMEDIATE 'CREATE INDEX XXEIS.XXWC_PO_ISR_OSALES_TAB_N1 ON XXEIS.EIS_XXWC_PO_ISR_OSALES_TAB              
(                            
INVENTORY_ITEM_ID,                
ORGANIZATION_ID              
) TABLESPACE XXEIS_IDX ';
  EXCEPTION
  WHEN OTHERS THEN
    write_log(' Error on creating the index XXWC_PO_ISR_OSALES_TAB_N1 '|| SQLERRM );
  END;
  write_log('Bulk collect Main query MST start '|| TO_CHAR(sysdate,'DD-MON-YYYY HH:MI:SS') );
  lv_program_location := '25, Main query MST Start ';
  l_main_sql          := ' SELECT *                     
from xxeis.EIS_XXWC_PO_ISR_MST_TAB_V ';
  OPEN l_ref_cursor2 FOR l_main_sql;
  LOOP
    FETCH L_REF_CURSOR2 BULK COLLECT INTO G_VIEW_RECORD2 LIMIT 10000;
    l_count             := 0;
    lv_program_location := '26, After Main query MST Fetch';
    FOR j IN 1..G_VIEW_RECORD2.count
    LOOP
      lv_program_location    := '27, Populate Main query into table record ';
      l_item_cost            := NVL(G_VIEW_RECORD2(j).bpa_cost,G_VIEW_RECORD2(j).item_cost);
      l_avail_d              := G_VIEW_RECORD2(j).avail  * G_VIEW_RECORD2(j).aver_cost;
      l_on_ord               := G_VIEW_RECORD2(j).supply - NVL(G_VIEW_RECORD2(j).open_req,0);
      l_avail2               := G_VIEW_RECORD2(j).qoh    - G_VIEW_RECORD2(j).demand;
      l_vendor_name          := G_VIEW_RECORD2(j).vendor_name;
      l_vendor_number        := G_VIEW_RECORD2(j).vendor_number;
      l_vendor_site          := G_VIEW_RECORD2(j).vendor_site;
      IF G_VIEW_RECORD2(j).st ='I' OR G_VIEW_RECORD2(j).vendor_number IS NULL THEN
        l_organization_id    :=
        CASE
        WHEN G_VIEW_RECORD2(j).st = 'I' THEN
          G_VIEW_RECORD2(j).source_organization_id
        ELSE
          G_VIEW_RECORD2(j).organization_id
        END;
        get_vendor_info(G_VIEW_RECORD2(j).inventory_item_id, l_organization_id, l_vendor_name,l_vendor_number,l_vendor_site);
      END IF;
      G_VIEW_TAB2(j).org                   := G_VIEW_RECORD2(j).org;
      G_VIEW_TAB2(j).pre                   := G_VIEW_RECORD2(j).pre;
      G_VIEW_TAB2(j).item_number           := G_VIEW_RECORD2(j).item_number;
      G_VIEW_TAB2(j).vendor_num            := l_vendor_number;
      G_VIEW_TAB2(j).vendor_name           := l_vendor_Name;
      G_VIEW_TAB2(j).source                := G_VIEW_RECORD2(j).source;
      G_VIEW_TAB2(j).st                    := G_VIEW_RECORD2(j).st;
      G_VIEW_TAB2(j).description           := G_VIEW_RECORD2(j).description;
      G_VIEW_TAB2(j).cat                   := G_VIEW_RECORD2(j).cat_Class;
      G_VIEW_TAB2(j).pplt                  := G_VIEW_RECORD2(j).pplt;
      G_VIEW_TAB2(j).plt                   := G_VIEW_RECORD2(j).plt;
      G_VIEW_TAB2(j).uom                   := G_VIEW_RECORD2(j).uom;
      G_VIEW_TAB2(j).cl                    := G_VIEW_RECORD2(j).cl;
      G_VIEW_TAB2(j).stk_flag              := G_VIEW_RECORD2(j).stk;
      G_VIEW_TAB2(j).pm                    := G_VIEW_RECORD2(j).pm;
      G_VIEW_TAB2(j).minn                  := G_VIEW_RECORD2(j).min;
      G_VIEW_TAB2(j).maxn                  := G_VIEW_RECORD2(j).max;
      G_VIEW_TAB2(j).amu                   := G_VIEW_RECORD2(j).amu;
      G_VIEW_TAB2(j).mf_flag               := G_VIEW_RECORD2(j).mf_flag;
      G_VIEW_TAB2(j).hit6_store_Sales      := NULL;
      G_VIEW_TAB2(j).hit6_other_inv_sales  := NULL;
      G_VIEW_TAB2(j).aver_cost             := G_VIEW_RECORD2(j).aver_cost;
      G_VIEW_TAB2(j).item_cost             := l_item_cost;
      G_VIEW_TAB2(j).bpa_cost              := G_VIEW_RECORD2(j).bpa_cost;
      G_VIEW_TAB2(j).bpa                   := G_VIEW_RECORD2(j).bpa;
      G_VIEW_TAB2(j).QOH                   := G_VIEW_RECORD2(j).QOH;
      G_VIEW_TAB2(j).on_ord                := l_on_ord;
      G_VIEW_TAB2(j).available             := G_VIEW_RECORD2(j).avail;
      G_VIEW_TAB2(j).availabledollar       := l_avail_d;
      G_VIEW_TAB2(j).one_store_sale        := NULL;
      G_VIEW_TAB2(j).six_store_sale        := NULL;
      G_VIEW_TAB2(j).twelve_store_Sale     := NULL;
      G_VIEW_TAB2(j).one_other_inv_sale    := NULL;
      G_VIEW_TAB2(j).six_other_inv_sale    := NULL;
      G_VIEW_TAB2(j).twelve_other_inv_Sale := NULL;
      G_VIEW_TAB2(j).bin_loc               := G_VIEW_RECORD2(j).bin_loc;
      G_VIEW_TAB2(j).mc                    := G_VIEW_RECORD2(j).mc;
      G_VIEW_TAB2(j).fi_flag               := G_VIEW_RECORD2(j).fi;
      G_VIEW_TAB2(j).freeze_date           := G_VIEW_RECORD2(j).freeze_date;
      G_VIEW_TAB2(j).res                   := G_VIEW_RECORD2(j).res;
      G_VIEW_TAB2(j).thirteen_wk_avg_inv   := G_VIEW_RECORD2(j).thirteen_wk_avg_inv;
      G_VIEW_TAB2(j).thirteen_wk_an_cogs   := G_VIEW_RECORD2(j).thirteen_wk_an_cogs;
      G_VIEW_TAB2(j).turns                 := G_VIEW_RECORD2(j).turns;
      G_VIEW_TAB2(j).buyer                 := G_VIEW_RECORD2(j).buyer;
      G_VIEW_TAB2(j).ts                    := G_VIEW_RECORD2(j).ts;
      G_VIEW_TAB2(j).jan_store_sale        := NULL;
      G_VIEW_TAB2(j).feb_store_sale        := NULL;
      G_VIEW_TAB2(j).mar_store_sale        := NULL;
      G_VIEW_TAB2(j).apr_store_sale        := NULL;
      G_VIEW_TAB2(j).may_store_sale        := NULL;
      G_VIEW_TAB2(j).jun_store_sale        := NULL;
      G_VIEW_TAB2(j).jul_store_sale        := NULL;
      G_VIEW_TAB2(j).aug_store_sale        := NULL;
      G_VIEW_TAB2(j).sep_store_sale        := NULL;
      G_VIEW_TAB2(j).oct_store_sale        := NULL;
      G_VIEW_TAB2(j).nov_store_sale        := NULL;
      G_VIEW_TAB2(j).dec_store_sale        := NULL;
      G_VIEW_TAB2(j).jan_other_inv_sale    := NULL;
      G_VIEW_TAB2(j).feb_other_inv_sale    := NULL;
      G_VIEW_TAB2(j).mar_other_inv_sale    := NULL;
      G_VIEW_TAB2(j).apr_other_inv_sale    := NULL;
      G_VIEW_TAB2(j).may_other_inv_sale    := NULL;
      G_VIEW_TAB2(j).jun_other_inv_sale    := NULL;
      G_VIEW_TAB2(j).jul_other_inv_sale    := NULL;
      G_VIEW_TAB2(j).aug_other_inv_sale    := NULL;
      G_VIEW_TAB2(j).sep_other_inv_sale    := NULL;
      G_VIEW_TAB2(j).oct_other_inv_sale    := NULL;
      G_VIEW_TAB2(j).nov_other_inv_sale    := NULL;
      G_VIEW_TAB2(j).dec_other_inv_sale    := NULL;
      G_VIEW_TAB2(j).hit4_store_Sales      := NULL;
      G_VIEW_TAB2(j).hit4_other_inv_Sales  := NULL;
      G_VIEW_TAB2(j).so                    := G_VIEW_RECORD2(j).so;
      G_VIEW_TAB2(j).inventory_item_id     := G_VIEW_RECORD2(j).inventory_item_id;
      G_VIEW_TAB2(j).organization_id       := G_VIEW_RECORD2(j).organization_id;
      G_VIEW_TAB2(j).set_of_books_id       := G_VIEW_RECORD2(j).set_of_books_id;
      G_VIEW_TAB2(j).org_name              := G_VIEW_RECORD2(j).organization_name;
      G_VIEW_TAB2(j).district              := G_VIEW_RECORD2(j).district;
      G_VIEW_TAB2(j).region                := G_VIEW_RECORD2(j).region;
      G_VIEW_TAB2(j).inv_Cat_Seg1          := G_VIEW_RECORD2(j).inv_cat_seg1;
      G_VIEW_TAB2(j).wt                    := G_VIEW_RECORD2(j).wt;
      G_VIEW_TAB2(j).ss                    := G_VIEW_RECORD2(j).ss;
      G_VIEW_TAB2(j).fml                   := G_VIEW_RECORD2(j).fml;
      G_VIEW_TAB2(j).open_req              := G_VIEW_RECORD2(j).open_req;
      G_VIEW_TAB2(j).sourcing_rule         := G_VIEW_RECORD2(j).sourcing_rule;
      G_VIEW_TAB2(j).clt                   := G_VIEW_RECORD2(j).clt;
      G_VIEW_TAB2(j).avail2                := l_avail2;
      G_VIEW_TAB2(j).int_req               := G_VIEW_RECORD2(j).int_req;
      G_VIEW_TAB2(j).dir_req               := G_VIEW_RECORD2(j).dir_req;
      G_VIEW_TAB2(j).demand                := G_VIEW_RECORD2(j).demand;
      G_VIEW_TAB2(j).ITEM_STATUS_CODE      := G_VIEW_RECORD2(j).ITEM_STATUS_CODE;
      G_VIEW_TAB2(j).SITE_VENDOR_NUM       := G_VIEW_RECORD2(j).vendor_number;
      G_VIEW_TAB2(j).vendor_site           := G_VIEW_RECORD2(j).vendor_site;
      l_count                              := l_count + 1;
    END LOOP;
    IF l_count            >= 1 THEN
      lv_program_location := '5, Insert into eis_xxwc_po_isr_tab ';
      forall j IN 1 .. G_VIEW_TAB2.count
      INSERT INTO xxeis.eis_xxwc_po_isr_tab VALUES g_view_tab2
        (j
        );
      COMMIT;
    END IF;
    lv_program_location := '6, Delete tab ';
    G_VIEW_TAB2.delete;
    G_VIEW_RECORD2.delete;
    IF l_ref_cursor2%NOTFOUND THEN
      CLOSE l_ref_cursor2;
      EXIT;
    END IF;
  END LOOP;
  write_log(' Bulk Collect Main MST query End '|| TO_CHAR(sysdate,'DD-MON-YYYY HH:MI:SS') );
  COMMIT;
  EXECUTE IMMEDIATE 'alter table xxeis.eis_xxwc_po_isr_tab NOLOGGING';
  EXECUTE IMMEDIATE 'alter table xxeis.eis_xxwc_po_isr_tab PARALLEL 4';   -- Added in 1.4 Revision
  --EXECUTE IMMEDIATE 'alter table xxeis.eis_xxwc_po_isr_tab PARALLEL 8';   -- Commented in 1.4 Revision
  write_log(' Merge Insert Start '|| TO_CHAR(sysdate,'DD-MON-YYYY HH:MI:SS') );
  INSERT /*+ append */
  INTO xxeis.eis_xxwc_po_isr_tab
  SELECT * FROM XXEIS.EIS_XXWC_PO_ISR_TAB_TMP_V;
  COMMIT;
  write_log(' Merge Insert Start '|| TO_CHAR(sysdate,'DD-MON-YYYY HH:MI:SS') );
  -- Truncate the temp tables
  EXECUTE IMMEDIATE 'truncate table xxeis.eis_xxwc_po_isr_tab_tmp';
  EXECUTE IMMEDIATE 'truncate table xxeis.eis_xxwc_po_isr_psales_tab';
  EXECUTE IMMEDIATE 'truncate table xxeis.eis_xxwc_po_isr_posales_tab';
  EXECUTE IMMEDIATE 'truncate table xxeis.eis_xxwc_po_isr_hits_tab';
  EXECUTE IMMEDIATE 'truncate table xxeis.eis_xxwc_po_isr_ohits_tab';
  EXECUTE IMMEDIATE 'truncate table xxeis.eis_xxwc_po_isr_sales_tab';
  EXECUTE IMMEDIATE 'truncate table xxeis.eis_xxwc_po_isr_osales_tab';
  EXECUTE IMMEDIATE 'alter table xxeis.eis_xxwc_po_isr_tab LOGGING';
  EXECUTE IMMEDIATE 'alter table xxeis.eis_xxwc_po_isr_tab NOPARALLEL';
  BEGIN
    EXECUTE IMMEDIATE 'CREATE INDEX XXEIS.XXWC_PO_ISR_TAB_N1 ON XXEIS.EIS_XXWC_PO_ISR_TAB              
(          
INVENTORY_ITEM_ID,                
ORGANIZATION_ID              
) TABLESPACE XXEIS_IDX';
  EXCEPTION
  WHEN OTHERS THEN
    NULL;
  END;
END ;--MAIN
PROCEDURE isr_rpt_proc(
    p_process_id          IN NUMBER ,
    p_region              IN VARCHAR2 ,
    p_district            IN VARCHAR2 ,
    p_location            IN VARCHAR2 ,
    p_dc_mode             IN VARCHAR2 ,
    p_tool_repair         IN VARCHAR2 ,
    p_time_sensitive      IN VARCHAR2 ,
    p_stk_items_with_hit4 IN VARCHAR2 ,
    p_report_condition    IN VARCHAR2 ,
    p_report_criteria     IN VARCHAR2 ,
    p_report_criteria_val IN VARCHAR2 ,
    p_start_bin_loc       IN VARCHAR2 ,
    p_end_bin_loc         IN VARCHAR2 ,
    p_vendor              IN VARCHAR2 ,
    p_item                IN VARCHAR2 ,
    p_cat_class           IN VARCHAR2 ,
    p_org_list            IN VARCHAR2 ,
    p_item_list           IN VARCHAR2 ,
    p_supplier_list       IN VARCHAR2 ,
    p_cat_class_list      IN VARCHAR2 ,
    p_source_list         IN VARCHAR2 ,
    p_intangibles         IN VARCHAR2)
  /*************************************************************************
  $Header EIS_PO_XXWC_ISR_PKG.ISR_RPT_PROC $
  PURPOSE: Procedure to Populate the Data for Pricing Team
  REVISIONS:
  Ver        Date         Author                Description
  -----  -----------  ------------------    ----------------
  1.0    28-Jul-2014  Manjula Chellappan TMS # 20140728-00186 : ISR Package Execution Error
  1.3    05-Oct-2015  Manjula Chellappan TMS # 20151005-00003 : Performance Improvement  
  **************************************************************************/
IS
  l_query             VARCHAR2 (32000);
  l_condition_str     VARCHAR2 (32000);
  lv_program_location VARCHAR2 (2000);
  l_ref_cursor cursor_type;
  l_insert_recd_cnt NUMBER;
  l_supplier_exists VARCHAR2 (32767);
  l_org_exists      VARCHAR2 (32000);
  l_item_exists     VARCHAR2 (32000);
  l_catclass_exists VARCHAR2 (32000);
  l_source_exists   VARCHAR2 (32000);
  l_sec VARCHAR2(4000);
  -- Added the cursor by Manjula on 28-Jul-14 for TMS # 20140728-00186
  CURSOR cur_org
  IS
    SELECT DISTINCT organization_id FROM xxeis.EIS_XXWC_PO_ISR_V;
  -- Added the cursor by Manjula on 28-Jul-14 for TMS # 20140728-00186
BEGIN
  l_sec             := 'Set Conditions Based on Parameters';
  IF p_region       IS NOT NULL THEN
    l_condition_str := l_condition_str || ' and REGION in (' || xxeis.eis_rs_utility.get_param_values (p_region) || ' )';
  END IF;
  IF p_district     IS NOT NULL THEN
    l_condition_str := l_condition_str || ' and DISTRICT in (' || xxeis.eis_rs_utility.get_param_values (p_district) || ' )';
  END IF;
  IF p_location     IS NOT NULL THEN
    l_condition_str := l_condition_str || ' and ORG in (' || xxeis.eis_rs_utility.get_param_values (p_location) || ' )';
    /*  l_location_exists:='And exists (select 1 from apps.XXWC_PARAM_LIST
    where list_name=''ORG''
    and list_values like '''||'%'||p_location||'%'||'''
    )';
    ELSE
    l_location_exists:='and 1=1';*/
  END IF;
  IF p_vendor       IS NOT NULL THEN
    l_condition_str := l_condition_str || ' and VENDOR_NUM in (' || xxeis.eis_rs_utility.get_param_values (p_vendor) || ' )';
    /* l_vendor_exists :='And exists (select 1 from apps.XXWC_PARAM_LIST
    where list_name=''VENDOR''
    and list_values like '''||'%'||p_vendor||'%'||'''
    )';
    FND_FILE.PUT_LINE(FND_FILE.LOG,'l_vendor_exists is '||L_VENDOR_EXISTS);
    ELSE
    l_vendor_exists:='and 1=1';*/
  END IF;
  IF p_item         IS NOT NULL THEN
--  l_condition_str := l_condition_str || ' and msi.concatenated_segments in (' || xxeis.eis_rs_utility.get_param_values (p_item) || ' )'; -- Commented for Ver 1.3
    l_condition_str := l_condition_str || ' and msi.segment1 in (' || xxeis.eis_rs_utility.get_param_values (p_item) || ' )'; --Added for Ver 1.3 
    /* L_ITEM_EXISTS:='And exists (select 1 from apps.XXWC_PARAM_LIST
    where list_name=''ITEM''
    and list_values like '''||'%'||p_item||'%'||'''
    )';
    ELSE
    L_ITEM_EXISTS:='and 1=1';*/
  END IF;
  IF p_cat_class    IS NOT NULL THEN
    l_condition_str := l_condition_str || ' and CAT in (' || xxeis.eis_rs_utility.get_param_values (p_cat_class) || ' )';
  END IF;
  /*  If P_Dc_Mode Is Not Null
  l_condition_str:= l_condition_str:=||'and ORG_NAME ='P_Location;
  Then
  End If;*/
  IF (p_intangibles  = 'Exclude') THEN
    l_condition_str := l_condition_str || ' and nvl(ITEM_STATUS_CODE,''Active'') <> ''Intangible''';
  END IF;
  IF (p_tool_repair  = 'Tool Repair Only' OR p_tool_repair LIKE 'Tool%') THEN
    l_condition_str := l_condition_str || ' and CAT = ''TH01''';
  END IF;
  IF p_tool_repair   = 'Exclude' THEN
    l_condition_str := l_condition_str || ' and CAT <> ''TH01''';
  END IF;
  IF p_report_condition = 'All items' THEN
    l_condition_str    := l_condition_str || ' and 1=1 ';
  END IF;
  IF p_report_condition = 'All items on hand' THEN
    l_condition_str    := l_condition_str || ' and QOH > 0 ';
  END IF;
  IF (p_report_condition = 'Non-stock on hand' OR p_report_condition LIKE 'Non-%') THEN
    l_condition_str     := l_condition_str || ' and STK_FLAG  =''N''' || ' and QOH > 0 ';
  END IF;
  IF ( p_report_condition = 'Non stock only' OR (p_report_condition LIKE 'Non%' AND p_report_condition NOT LIKE 'Non-%')) THEN
    l_condition_str      := l_condition_str || ' and STK_FLAG =''N'' ';
  END IF;
  IF p_report_condition = 'Active large' THEN
    l_condition_str    := l_condition_str || ' and STK_FLAG in (''N'',''Y'') ' || ' and (QOH > 0 or twelve_sales > 0 or on_ord > 0 or Available > 0 or Available < 0)';
  END IF;
  IF p_report_condition = 'Active small' THEN
    l_condition_str    := l_condition_str || ' AND (( STK_FLAG =''Y'' OR (STK_FLAG =''N'' and (QOH > 0 or on_ord > 0 or open_req > 0 or  int_req > 0 or demand > 0 or dir_req > 0 or Available > 0 )))' || ' AND ((NOT EXISTS (select 1 from dual where REGEXP_LIKE(substr(item_number,1,1),''[a-zA-Z]''))) or item_number like ''SP%''))';
    ---l_condition_str:= l_condition_str||' and STK_FLAG in (''N'',''Y'') '||' and (QOH > 0 or on_ord > 0 or Available > 0 or Available < 0)';
  END IF;
  IF p_report_condition = 'Stock items only' THEN
    l_condition_str    := l_condition_str || ' and STK_FLAG =''Y''';
  END IF;
  IF p_report_condition = 'Stock items with 0/0 min/max' THEN
    l_condition_str    := l_condition_str || ' and STK_FLAG =''Y''' || ' and MINN=0 and MAXN=0';
  END IF;
  IF (p_time_sensitive = 'Time Sensitive Only' OR p_time_sensitive LIKE 'Time%') THEN
    l_condition_str   := l_condition_str || ' and TS > 0 and TS < 4000';
  END IF;
  /* Removing and STK_FLAG =''Y''' || ' because this no longer filters for stock parts- 20130904-01108 */
  IF p_stk_items_with_hit4 IS NOT NULL THEN
    l_condition_str        := l_condition_str || ' and hit4_sales >= ' || p_stk_items_with_hit4;
  END IF;
  l_sec         := 'Validate Org List';
  IF p_org_list IS NOT NULL THEN
    --insert into xxeis.log values ('Start P_ORG_LIST'); commit;
    xxeis.eis_rs_xxwc_com_util_pkg.parse_param_list (p_process_id, p_org_list, 'Org');
    l_org_exists := ' AND EXISTS (SELECT 1                                        
FROM xxeis.EIS_XXWC_PARAM_PARSE_LIST x                                       
WHERE x.LIST_NAME  = ''' || REPLACE (p_org_list, '''', '') || '''                                         
AND x.LIST_TYPE     =''Org''                                         
AND x.process_id    = ' || p_process_id || '                                         
AND TAB.ORG = X.list_value) ';
  ELSE
    l_org_exists := ' and 1=1 ';
  END IF;
  l_sec              := 'Validate Supplier List';
  IF p_supplier_list IS NOT NULL THEN
    xxeis.eis_rs_xxwc_com_util_pkg.parse_param_list (p_process_id, p_supplier_list, 'Supplier');
    l_supplier_exists := ' AND EXISTS (SELECT 1                                        
FROM xxeis.EIS_XXWC_PARAM_PARSE_LIST x                                       
WHERE x.LIST_NAME    =''' || REPLACE (p_supplier_list, '''', '') || '''                                         
AND x.LIST_TYPE    =''Supplier''                                         
AND x.process_id   = ' || p_process_id || '                                         
AND TAB.vendor_num = X.list_value) ';
  ELSE
    l_supplier_exists := ' and 1=1 ';
  END IF;
  l_sec          := 'Validate Item List';
  IF p_item_list IS NOT NULL THEN
    xxeis.eis_rs_xxwc_com_util_pkg.parse_param_list (p_process_id, p_item_list, 'Item');
    l_item_exists := ' AND EXISTS (SELECT 1                                        
FROM xxeis.EIS_XXWC_PARAM_PARSE_LIST x                                       
WHERE x.LIST_NAME    =''' || REPLACE (p_item_list, '''', '') || '''                                         
AND x.LIST_TYPE    =''Item''                                         
AND x.process_id   = ' || p_process_id || '                                         
AND TAB.item_number = X.list_value ) ';
  ELSE
    l_item_exists := ' and 1=1 ';
  END IF;
  l_sec               := 'Validate Category Class List';
  IF p_cat_class_list IS NOT NULL THEN
    xxeis.eis_rs_xxwc_com_util_pkg.parse_param_list (p_process_id, p_cat_class_list, 'Cat Class');
    l_item_exists := ' AND EXISTS (SELECT 1                                        
FROM xxeis.EIS_XXWC_PARAM_PARSE_LIST x                                       
WHERE x.LIST_NAME    =''' || REPLACE (p_cat_class_list, '''', '') || '''                                         
AND x.LIST_TYPE    =''Cat Class''                                         
AND x.process_id   = ' || p_process_id || '                                         
AND TAB.cat        = X.list_value) ';
  ELSE
    l_catclass_exists := ' and 1=1 ';
  END IF;
  l_sec            := 'Validate Source List';
  IF p_source_list IS NOT NULL THEN
    xxeis.eis_rs_xxwc_com_util_pkg.parse_param_list (p_process_id, p_source_list, 'Source');
    l_item_exists := ' AND EXISTS (SELECT 1                                        
FROM xxeis.EIS_XXWC_PARAM_PARSE_LIST x                                       
WHERE x.LIST_NAME    =''' || REPLACE (p_source_list, '''', '') || '''                                         
AND x.LIST_TYPE    =''Source''                                         
AND x.process_id   = ' || p_process_id || '                                         
AND TAB.source     = X.list_value) ';
  ELSE
    l_source_exists := ' and 1=1 ';
  END IF;
  l_sec                  := 'Validate Report Criteria';
  IF p_report_criteria   IS NOT NULL AND p_report_criteria_val IS NOT NULL THEN
    IF (p_report_criteria = 'Vendor Number(%)' OR p_report_criteria LIKE 'Vendor%') THEN
      l_condition_str    := l_condition_str || ' and upper(VENDOR_NUM) like ''' || UPPER (p_report_criteria_val) || '''';
      fnd_file.put_line (fnd_file.LOG, l_condition_str);
    END IF;
    IF (p_report_criteria = '3 Digit Prefix' OR p_report_criteria LIKE '3%') THEN
      l_condition_str    := l_condition_str || ' and upper(PRE) = ''' || UPPER (p_report_criteria_val) || '''';
    END IF;
    IF (p_report_criteria = 'Item number' OR p_report_criteria LIKE 'Item%') THEN
      l_condition_str    := l_condition_str || ' and upper(ITEM_NUMBER) = ''' || UPPER (p_report_criteria_val) || '''';
    END IF;
    IF p_report_criteria = 'Source' AND p_report_criteria_val IS NOT NULL THEN
      l_condition_str   := l_condition_str || ' and upper(SOURCE) = ''' || UPPER (p_report_criteria_val) || '''';
      NULL;
    END IF;
    IF p_report_criteria = 'External source vendor' THEN
      -- L_Condition_Str:= L_Condition_Str||' and ITEM_NUMBER = '''||P_Report_Criteria_Val||'';
      NULL;
    END IF;
    IF (p_report_criteria = '2 Digit Cat' OR p_report_criteria LIKE '2%') THEN
      l_condition_str    := l_condition_str || ' and upper(inv_cat_seg1) = ''' || UPPER (p_report_criteria_val) || '''';
      NULL;
    END IF;
    IF (p_report_criteria = '4 Digit Cat Class' OR p_report_criteria LIKE '4%') THEN
      l_condition_str    := l_condition_str || ' and upper(CAT) = ''' || UPPER (p_report_criteria_val) || '''';
    END IF;
    IF (p_report_criteria = 'Default Buyer(%)' OR p_report_criteria LIKE 'Default%') THEN
      l_condition_str    := l_condition_str || ' and upper(Buyer) like ''' || UPPER (p_report_criteria_val) || ''''; --commented by santosh
      -- l_condition_str:= l_condition_str||' and upper(Buyer) like '''||upper(replace(p_report_criteria_val,''''))||''''; --commented by santosh
      --             L_CONDITION_STR:= L_CONDITION_STR||' and upper(Buyer) in ('''||UPPER(P_REPORT_CRITERIA_VAL)||''')'; -- added by santosh
      --       L_CONDITION_STR:= L_CONDITION_STR||' and upper(Buyer) in ('''||UPPER(replace(replace(P_REPORT_CRITERIA_VAL,'''',''),',',', '))||''')'; -- added by santosh
      --l_condition_str:= l_condition_str||' and upper(Buyer) like '''||upper(XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(p_report_criteria_val))||'''';
    END IF;
  END IF;
  IF p_start_bin_loc IS NOT NULL THEN
    l_condition_str  := l_condition_str || ' and upper(Bin_Loc) >= ''' || UPPER (p_start_bin_loc) || '''';
  END IF;
  IF p_end_bin_loc  IS NOT NULL THEN
    l_condition_str := l_condition_str || ' and upper(Bin_Loc) <= ''' || UPPER (p_end_bin_loc) || '''';
  END IF;
  /*   IF p_all_items ='Yes'   THEN
  l_condition_str:= ' and 1=1';
  END IF;
  */
  -- Added the cursor by Manjula on 28-Jul-14 for TMS # 20140728-00186
  l_sec := 'Entering the Load cursor';
  FOR rec_org IN cur_org
  LOOP
    l_sec               := 'Cursor for Organization_id : ' || rec_org.organization_id;
--Commented for Ver 1.3 Begin
/*    
    l_query             := 'select tab.*             
from             
xxeis.EIS_XXWC_PO_ISR_V tab,             
mtl_system_items_kfv msi             
where 1=1             
and msi.inventory_item_id = tab.inventory_item_id             
and msi.organization_id   = tab.organization_id              
and tab.organization_id = ' || rec_org.organization_id || l_condition_str || l_org_exists || l_supplier_exists || l_item_exists || l_catclass_exists || l_source_exists;
*/
--Commented for Ver 1.3 End

--Added for Ver 1.3 Begin>>
    l_query             := 
 'SELECT tab.*            
  FROM             
      xxeis.eis_xxwc_po_isr_v tab,             
      mtl_system_items_b msi      
  WHERE 1=1             
    AND msi.inventory_item_id = tab.inventory_item_id             
    AND msi.organization_id   = tab.organization_id 
    AND msi.item_type                             !=''Special''
    AND msi.inventory_item_status_code            !=''Inactive''
    AND tab.organization_id = ' || rec_org.organization_id || l_condition_str || l_org_exists || l_supplier_exists || l_item_exists || l_catclass_exists || l_source_exists;
--Added for Ver 1.3 End>>

    lv_program_location := 'The main query is ' || l_query;
    --apps.xxwc_common_tunning_helpers.write_log (lv_program_location); Commented for TMS#20130709-01006 by Krishna 05-Aug-2014
    l_insert_recd_cnt := 1;
    OPEN l_ref_cursor FOR l_query;
    LOOP
      FETCH l_ref_cursor INTO g_view_record;
      EXIT
    WHEN l_ref_cursor%NOTFOUND;
      g_view_tab (l_insert_recd_cnt).org                 := g_view_record.org;
      g_view_tab (l_insert_recd_cnt).pre                 := g_view_record.pre;
      g_view_tab (l_insert_recd_cnt).item_number         := g_view_record.item_number;
      g_view_tab (l_insert_recd_cnt).vendor_num          := g_view_record.vendor_num;
      g_view_tab (l_insert_recd_cnt).vendor_name         := g_view_record.vendor_name;
      g_view_tab (l_insert_recd_cnt).source              := g_view_record.source;
      g_view_tab (l_insert_recd_cnt).st                  := g_view_record.st;
      g_view_tab (l_insert_recd_cnt).description         := g_view_record.description;
      g_view_tab (l_insert_recd_cnt).cat                 := g_view_record.cat;
      g_view_tab (l_insert_recd_cnt).pplt                := g_view_record.pplt;
      g_view_tab (l_insert_recd_cnt).plt                 := g_view_record.plt;
      g_view_tab (l_insert_recd_cnt).uom                 := g_view_record.uom;
      g_view_tab (l_insert_recd_cnt).cl                  := g_view_record.cl;
      g_view_tab (l_insert_recd_cnt).stk_flag            := g_view_record.stk_flag;
      g_view_tab (l_insert_recd_cnt).pm                  := g_view_record.pm;
      g_view_tab (l_insert_recd_cnt).minn                := g_view_record.minn;
      g_view_tab (l_insert_recd_cnt).maxn                := g_view_record.maxn;
      g_view_tab (l_insert_recd_cnt).amu                 := g_view_record.amu;
      g_view_tab (l_insert_recd_cnt).mf_flag             := g_view_record.mf_flag;
      g_view_tab (l_insert_recd_cnt).hit6_sales          := g_view_record.hit6_sales;
      g_view_tab (l_insert_recd_cnt).aver_cost           := g_view_record.aver_cost;
      g_view_tab (l_insert_recd_cnt).item_cost           := g_view_record.item_cost;
      g_view_tab (l_insert_recd_cnt).bpa                 := g_view_record.bpa;
      g_view_tab (l_insert_recd_cnt).bpa_cost            := g_view_record.bpa_cost;
      g_view_tab (l_insert_recd_cnt).qoh                 := g_view_record.qoh;
      g_view_tab (l_insert_recd_cnt).available           := g_view_record.available;
      g_view_tab (l_insert_recd_cnt).availabledollar     := g_view_record.availabledollar;
      g_view_tab (l_insert_recd_cnt).jan_sales           := g_view_record.jan_sales;
      g_view_tab (l_insert_recd_cnt).feb_sales           := g_view_record.feb_sales;
      g_view_tab (l_insert_recd_cnt).mar_sales           := g_view_record.mar_sales;
      g_view_tab (l_insert_recd_cnt).apr_sales           := g_view_record.apr_sales;
      g_view_tab (l_insert_recd_cnt).may_sales           := g_view_record.may_sales;
      g_view_tab (l_insert_recd_cnt).june_sales          := g_view_record.june_sales;
      g_view_tab (l_insert_recd_cnt).jul_sales           := g_view_record.jul_sales;
      g_view_tab (l_insert_recd_cnt).aug_sales           := g_view_record.aug_sales;
      g_view_tab (l_insert_recd_cnt).sep_sales           := g_view_record.sep_sales;
      g_view_tab (l_insert_recd_cnt).oct_sales           := g_view_record.oct_sales;
      g_view_tab (l_insert_recd_cnt).nov_sales           := g_view_record.nov_sales;
      g_view_tab (l_insert_recd_cnt).dec_sales           := g_view_record.dec_sales;
      g_view_tab (l_insert_recd_cnt).hit4_sales          := g_view_record.hit4_sales;
      g_view_tab (l_insert_recd_cnt).one_sales           := g_view_record.one_sales;
      g_view_tab (l_insert_recd_cnt).six_sales           := g_view_record.six_sales;
      g_view_tab (l_insert_recd_cnt).twelve_sales        := g_view_record.twelve_sales;
      g_view_tab (l_insert_recd_cnt).bin_loc             := g_view_record.bin_loc;
      g_view_tab (l_insert_recd_cnt).mc                  := g_view_record.mc;
      g_view_tab (l_insert_recd_cnt).fi_flag             := g_view_record.fi_flag;
      g_view_tab (l_insert_recd_cnt).freeze_date         := g_view_record.freeze_date;
      g_view_tab (l_insert_recd_cnt).res                 := g_view_record.res;
      g_view_tab (l_insert_recd_cnt).thirteen_wk_avg_inv := g_view_record.thirteen_wk_avg_inv;
      g_view_tab (l_insert_recd_cnt).thirteen_wk_an_cogs := g_view_record.thirteen_wk_an_cogs;
      g_view_tab (l_insert_recd_cnt).turns               := g_view_record.turns;
      g_view_tab (l_insert_recd_cnt).buyer               := g_view_record.buyer;
      g_view_tab (l_insert_recd_cnt).ts                  := g_view_record.ts;
      g_view_tab (l_insert_recd_cnt).so                  := g_view_record.so;
      g_view_tab (l_insert_recd_cnt).sourcing_rule       := g_view_record.sourcing_rule;
      g_view_tab (l_insert_recd_cnt).clt                 := g_view_record.clt;
      g_view_tab (l_insert_recd_cnt).inventory_item_id   := g_view_record.inventory_item_id;
      g_view_tab (l_insert_recd_cnt).organization_id     := g_view_record.organization_id;
      g_view_tab (l_insert_recd_cnt).set_of_books_id     := g_view_record.set_of_books_id;
      g_view_tab (l_insert_recd_cnt).on_ord              := g_view_record.on_ord;
      g_view_tab (l_insert_recd_cnt).org_name            := g_view_record.org_name;
      g_view_tab (l_insert_recd_cnt).district            := g_view_record.district;
      g_view_tab (l_insert_recd_cnt).region              := g_view_record.region;
      g_view_tab (l_insert_recd_cnt).wt                  := g_view_record.wt;
      g_view_tab (l_insert_recd_cnt).ss                  := g_view_record.ss;
      g_view_tab (l_insert_recd_cnt).fml                 := g_view_record.fml;
      g_view_tab (l_insert_recd_cnt).open_req            := g_view_record.open_req;
      g_view_tab (l_insert_recd_cnt).common_output_id    := xxeis.eis_rs_common_outputs_s.NEXTVAL;
      g_view_tab (l_insert_recd_cnt).process_id          := p_process_id;
      g_view_tab (l_insert_recd_cnt).avail2              := g_view_record.avail2;
      g_view_tab (l_insert_recd_cnt).int_req             := g_view_record.int_req;
      g_view_tab (l_insert_recd_cnt).dir_req             := g_view_record.dir_req;
      g_view_tab (l_insert_recd_cnt).demand              := g_view_record.demand;
      g_view_tab (l_insert_recd_cnt).site_vendor_num     := g_view_record.site_vendor_num;
      g_view_tab (l_insert_recd_cnt).vendor_site         := g_view_record.vendor_site;
      l_insert_recd_cnt                                  := l_insert_recd_cnt + 1;
    END LOOP;
    CLOSE l_ref_cursor;
    BEGIN
      fnd_file.put_line (fnd_file.LOG, 'l_insert_recd_cnt' || l_insert_recd_cnt);
      fnd_file.put_line (fnd_file.LOG, 'first common_output_id' || g_view_tab (1).common_output_id);
      --    Fnd_File.Put_Line(Fnd_File.Log,'last common_output_id' ||G_View_Tab(L_Insert_Recd_Cnt).Common_Output_Id);
      IF l_insert_recd_cnt >= 1 THEN
        FORALL i IN g_view_tab.FIRST .. g_view_tab.LAST
        INSERT INTO xxeis.eis_xxwc_po_isr_rpt_v VALUES g_view_tab
          (i
          );
      END IF;
      COMMIT;
      g_view_tab.delete;
    EXCEPTION
    WHEN OTHERS THEN
      fnd_file.put_line (fnd_file.LOG, 'Inside Exception==>' || SUBSTR (SQLERRM, 1, 240));
    END;
  END LOOP; -- Added the cursor by Manjula on 28-Jul-14 for TMS # 20140728-00186
EXCEPTION
WHEN OTHERS THEN
  APPS.xxcus_error_pkg.xxcus_error_main_api ( p_called_from => 'eis_po_xxwc_isr_pkg.isr_rpt_proc', p_calling => l_sec, p_request_id => NULL, p_ora_error_msg => SUBSTR ( ' Error_Stack...' || DBMS_UTILITY.format_error_stack () || ' Error_Backtrace...' || DBMS_UTILITY.format_error_backtrace (), 1, 2000), p_error_desc => SUBSTR (lv_program_location, 1, 240), p_distribution_list => g_distribution_list, p_module => 'INV');
END;
PROCEDURE ISR_LIVE_RPT_PROC
  (
    p_process_id          IN NUMBER,
    p_region              IN VARCHAR2,
    p_district            IN VARCHAR2,
    p_location            IN VARCHAR2,
    p_dc_mode             IN VARCHAR2,
    p_tool_repair         IN VARCHAR2,
    p_time_sensitive      IN VARCHAR2,
    p_stk_items_with_hit4 IN VARCHAR2,
    p_Report_Condition    IN VARCHAR2,
    p_report_criteria     IN VARCHAR2,
    p_report_criteria_val IN VARCHAR2,
    p_start_bin_loc       IN VARCHAR2,
    p_end_bin_loc         IN VARCHAR2,
    p_vendor              IN VARCHAR2,
    p_item                IN VARCHAR2,
    p_cat_class           IN VARCHAR2 ,
    P_ORG_LIST            IN VARCHAR2,
    P_ITEM_LIST           IN VARCHAR2,
    p_supplier_list       IN VARCHAR2,
    P_CAT_CLASS_LIST      IN VARCHAR2,
    P_SOURCE_LIST         IN VARCHAR2
  )
IS
  l_query             VARCHAR2(32000);
  L_CONDITION_STR     VARCHAR2(32000);
  LV_PROGRAM_LOCATION VARCHAR2(2000);
  L_REF_CURSOR CURSOR_TYPE;
  L_INSERT_RECD_CNT NUMBER;
  L_ORG_LIST_VLAUES VARCHAR2(32000);
  L_SUPPLIER_EXISTS VARCHAR2(32767);
  L_ORG_EXISTS      VARCHAR2(32000);
  l_item_exists     VARCHAR2(32000);
  L_CATCLASS_EXISTS VARCHAR2(32000);
  L_source_exists   VARCHAR2(32000);
  L_LLEGTH          NUMBER;
BEGIN
  IF p_region      IS NOT NULL THEN
    l_condition_str:= l_condition_str||' and REGION in ('||xxeis.eis_rs_utility.get_param_values(p_region)||' )';
  END IF;
  IF p_district    IS NOT NULL THEN
    l_condition_str:= l_condition_str||' and DISTRICT in ('||xxeis.eis_rs_utility.get_param_values(p_district)||' )';
  END IF;
  IF p_location    IS NOT NULL THEN
    L_CONDITION_STR:= L_CONDITION_STR||' and ORG in ('||XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(P_LOCATION)||' )';
    /*  l_location_exists:='And exists (select 1 from apps.XXWC_PARAM_LIST
    where list_name=''ORG''
    and list_values like '''||'%'||p_location||'%'||'''
    )';
    ELSE
    l_location_exists:='and 1=1';*/
  END IF;
  IF p_vendor      IS NOT NULL THEN
    L_CONDITION_STR:= L_CONDITION_STR||' and VENDOR_NAME in ('||XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(P_VENDOR)||' )';
    /* l_vendor_exists :='And exists (select 1 from apps.XXWC_PARAM_LIST
    where list_name=''VENDOR''
    and list_values like '''||'%'||p_vendor||'%'||'''
    )';
    FND_FILE.PUT_LINE(FND_FILE.LOG,'l_vendor_exists is '||L_VENDOR_EXISTS);
    ELSE
    l_vendor_exists:='and 1=1';*/
  END IF;
  IF p_item        IS NOT NULL THEN
    L_CONDITION_STR:= L_CONDITION_STR||' and msi.concatenated_segments in ('||XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(P_ITEM)||' )';
    /* L_ITEM_EXISTS:='And exists (select 1 from apps.XXWC_PARAM_LIST
    where list_name=''ITEM''
    and list_values like '''||'%'||p_item||'%'||'''
    )';
    ELSE
    L_ITEM_EXISTS:='and 1=1';*/
  END IF;
  IF p_cat_class   IS NOT NULL THEN
    l_condition_str:= l_condition_str||' and cat_class in ('||xxeis.eis_rs_utility.get_param_values(p_cat_class)||' )';
  END IF;
  /*  If P_Dc_Mode Is Not Null
  l_condition_str:= l_condition_str:=||'and ORG_NAME ='P_Location;
  Then
  End If;*/
  IF (P_TOOL_REPAIR ='Tool Repair Only' OR P_TOOL_REPAIR LIKE 'Tool%') THEN
    l_condition_str:= l_condition_str||' and cat_class = ''TH01''';
  END IF;
  IF p_tool_repair  ='Exclude' THEN
    l_condition_str:= l_condition_str||' and cat_class <> ''TH01''';
  END IF;
  IF p_report_condition='All items' THEN
    l_condition_str   := l_condition_str || ' and 1=1 ';
  END IF;
  IF p_report_condition='All items on hand' THEN
    l_condition_str   := l_condition_str||' and QOH > 0 ';
  END IF;
  IF (p_report_condition='Non-stock on hand' OR p_report_condition LIKE 'Non-%') THEN
    l_condition_str    := l_condition_str||' and STK  =''N'''||' and QOH > 0 ';
  END IF;
  IF (p_report_condition='Non stock only' OR (p_report_condition LIKE 'Non%' AND p_report_condition NOT LIKE 'Non-%')) THEN
    l_condition_str    := l_condition_str||' and STK =''N'' ';
  END IF;
  IF p_report_condition='Active large' THEN
    L_CONDITION_STR   := L_CONDITION_STR||' and STK in (''N'',''Y'') '||' and (QOH > 0 or twelve_sales > 0 or on_ord > 0 or Available > 0 or Available < 0)';
  END IF;
  IF p_report_condition='Active small' THEN
    l_condition_str   := l_condition_str||' AND (( STK =''Y'' OR (STK =''N'' and (QOH > 0 or on_ord > 0 or open_req > 0 or  int_req > 0 or demand > 0 or dir_req > 0 or Available > 0 )))' || ' AND ((NOT EXISTS (select 1 from dual where REGEXP_LIKE(substr(item_number,1,1),''[a-zA-Z]''))) or item_number like ''SP%''))';
    ---l_condition_str:= l_condition_str||' and STK_FLAG in (''N'',''Y'') '||' and (QOH > 0 or on_ord > 0 or Available > 0 or Available < 0)';
  END IF;
  IF p_report_condition='Stock items only' THEN
    l_condition_str   := l_condition_str||' and STK =''Y''';
  END IF;
  IF p_report_condition='Stock items with 0/0 min/max' THEN
    l_condition_str   := l_condition_str||' and STK =''Y'''||' and MINN=0 and MAXN=0';
  END IF;
  IF (p_time_sensitive ='Time Sensitive Only' OR p_time_sensitive LIKE 'Time%') THEN
    l_condition_str   := l_condition_str||' and TS > 0 and TS < 4000';
  END IF;
  IF p_stk_items_with_hit4 IS NOT NULL THEN
    l_condition_str        := l_condition_str||' and STK =''Y'''|| ' and hit4_sales >'||p_stk_items_with_hit4;
  END IF;
  IF P_ORG_LIST IS NOT NULL THEN
    --insert into xxeis.log values ('Start P_ORG_LIST'); commit;
    XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.PARSE_PARAM_LIST(P_PROCESS_ID,P_ORG_LIST,'Org');
    L_ORG_EXISTS:= ' AND EXISTS (SELECT 1                                          
FROM xxeis.EIS_XXWC_PARAM_PARSE_LIST x                                       
WHERE x.LIST_NAME  = '''||REPLACE(P_ORG_LIST,'''','')||'''                                         
AND x.LIST_TYPE     =''Org''                                         
AND x.process_id    = '||P_PROCESS_ID||'                                         
AND TAB.ORG = X.list_value) ';
  ELSE
    L_ORG_EXISTS :=' and 1=1 ';
  END IF;
  IF P_SUPPLIER_LIST IS NOT NULL THEN
    XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.PARSE_PARAM_LIST(P_PROCESS_ID,P_SUPPLIER_LIST,'Supplier');
    L_SUPPLIER_EXISTS :=' AND EXISTS (SELECT 1                                          
FROM xxeis.EIS_XXWC_PARAM_PARSE_LIST x                                       
WHERE x.LIST_NAME    ='''||REPLACE(P_SUPPLIER_LIST,'''','')||'''                                         
AND x.LIST_TYPE    =''Supplier''                                         
AND x.process_id   = '||P_PROCESS_ID||'                                         
AND TAB.vendor_number = X.list_value) ';
  ELSE
    L_SUPPLIER_EXISTS :=' and 1=1 ';
  END IF;
  IF P_ITEM_LIST IS NOT NULL THEN
    XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.PARSE_PARAM_LIST(P_PROCESS_ID,P_ITEM_LIST,'Item');
    L_ITEM_EXISTS:=' AND EXISTS (SELECT 1                                          
FROM xxeis.EIS_XXWC_PARAM_PARSE_LIST x                                       
WHERE x.LIST_NAME    ='''||REPLACE(P_ITEM_LIST,'''','')||'''                                         
AND x.LIST_TYPE    =''Item''                                         
AND x.process_id   = '||P_PROCESS_ID||'                                         
AND TAB.item_number = X.list_value ) ';
  ELSE
    L_ITEM_EXISTS :=' and 1=1 ';
  END IF;
  IF P_CAT_CLASS_LIST IS NOT NULL THEN
    XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.PARSE_PARAM_LIST(P_PROCESS_ID,P_CAT_CLASS_LIST,'Cat Class');
    L_ITEM_EXISTS:=' AND EXISTS (SELECT 1                                          
FROM xxeis.EIS_XXWC_PARAM_PARSE_LIST x                                       
WHERE x.LIST_NAME    ='''||REPLACE(P_CAT_CLASS_LIST,'''','')||'''                                         
AND x.LIST_TYPE    =''Cat Class''                                         
AND x.process_id   = '||P_PROCESS_ID||'                                         
AND tab.cat_class  = X.list_value) ';
  ELSE
    L_CATCLASS_EXISTS :=' and 1=1 ';
  END IF;
  IF P_SOURCE_LIST IS NOT NULL THEN
    XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.PARSE_PARAM_LIST(P_PROCESS_ID,P_SOURCE_LIST,'Source');
    L_ITEM_EXISTS:=' AND EXISTS (SELECT 1                                          
FROM xxeis.EIS_XXWC_PARAM_PARSE_LIST x                                       
WHERE x.LIST_NAME    ='''||REPLACE(P_SOURCE_LIST,'''','')||'''                                         
AND x.LIST_TYPE    =''Source''                                         
AND x.process_id   = '||P_PROCESS_ID||'                                         
AND TAB.source     = X.list_value) ';
  ELSE
    L_source_exists :=' and 1=1 ';
  END IF;
  IF p_report_criteria  IS NOT NULL AND p_report_criteria_val IS NOT NULL THEN
    IF (p_report_criteria='Vendor Number(%)' OR p_report_criteria LIKE 'Vendor%') THEN
      l_condition_str   := l_condition_str||' and upper(VENDOR_NUMBER) like '''||upper(p_report_criteria_val)||'''';
      Fnd_File.Put_Line(Fnd_File.Log,l_condition_str);
    END IF;
    IF (p_report_criteria='3 Digit Prefix' OR p_report_criteria LIKE '3%') THEN
      l_condition_str   := l_condition_str||' and upper(PRE) = '''||upper(p_report_criteria_val)||'''';
    END IF;
    IF (p_report_criteria='Item number' OR p_report_criteria LIKE 'Item%') THEN
      l_condition_str   := l_condition_str||' and upper(ITEM_NUMBER) = '''||upper(p_report_criteria_val)||'''';
    END IF;
    IF p_report_criteria='Source' AND p_report_criteria_val IS NOT NULL THEN
      L_Condition_Str  := L_Condition_Str||' and upper(SOURCE) = '''||upper(p_report_criteria_val)||'''';
      NULL;
    END IF;
    IF p_report_criteria='External source vendor' THEN
      -- L_Condition_Str:= L_Condition_Str||' and ITEM_NUMBER = '''||P_Report_Criteria_Val||'';
      NULL;
    END IF;
    IF (p_report_criteria='2 Digit Cat' OR p_report_criteria LIKE '2%') THEN
      l_condition_str   := l_condition_str||' and upper(inv_cat_seg1) = '''||upper(p_report_criteria_val)||'''';
      NULL;
    END IF;
    IF (p_report_criteria='4 Digit Cat Class' OR p_report_criteria LIKE '4%') THEN
      l_condition_str   := l_condition_str||' and upper(cat_class) = '''||upper(p_report_criteria_val)||'''';
    END IF;
    IF (P_REPORT_CRITERIA='Default Buyer(%)' OR P_REPORT_CRITERIA LIKE 'Default%') THEN
      l_condition_str   := l_condition_str||' and upper(Buyer) like '''||upper(p_report_criteria_val)||''''; --commented by santosh
      -- l_condition_str:= l_condition_str||' and upper(Buyer) like '''||upper(replace(p_report_criteria_val,''''))||''''; --commented by santosh
      --             L_CONDITION_STR:= L_CONDITION_STR||' and upper(Buyer) in ('''||UPPER(P_REPORT_CRITERIA_VAL)||''')'; -- added by santosh
      --       L_CONDITION_STR:= L_CONDITION_STR||' and upper(Buyer) in ('''||UPPER(replace(replace(P_REPORT_CRITERIA_VAL,'''',''),',',', '))||''')'; -- added by santosh
      --l_condition_str:= l_condition_str||' and upper(Buyer) like '''||upper(XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(p_report_criteria_val))||'''';
    END IF;
  END IF;
  IF p_start_bin_loc IS NOT NULL THEN
    l_condition_str  := l_condition_str||' and upper(Bin_Loc) >= '''||upper(p_start_bin_loc)||'''';
  END IF;
  IF p_end_bin_loc IS NOT NULL THEN
    l_condition_str:= l_condition_str||' and upper(Bin_Loc) <= '''||upper(p_end_bin_loc)||'''';
  END IF;
  /*   IF p_all_items ='Yes'   THEN
  l_condition_str:= ' and 1=1';
  END IF;
  */
  l_query            := 'select tab.*              
from              
xxeis.EIS_XXWC_PO_ISR_LIVE_V tab,             
mtl_system_items_kfv msi             
where 1=1              
and msi.inventory_item_id = tab.inventory_item_id             
and msi.organization_id   = tab.organization_id  '|| L_CONDITION_STR||L_ORG_EXISTS||L_SUPPLIER_EXISTS||L_ITEM_EXISTS||L_CATCLASS_EXISTS||L_SOURCE_EXISTS;
  lv_program_location:='The main query is ' ||l_query;
  write_log(lv_program_location);
  l_insert_recd_cnt:=1;
  OPEN l_ref_cursor FOR l_query;
  LOOP
    FETCH l_ref_cursor INTO G_View_Record10;
    EXIT
  WHEN L_Ref_Cursor%Notfound;
    G_View_Tab(L_Insert_Recd_Cnt).Org                 := G_View_Record10.Org ;
    G_View_Tab(L_Insert_Recd_Cnt).Pre                 := G_View_Record10.Pre ;
    G_View_Tab(L_Insert_Recd_Cnt).ITEM_NUMBER         := g_view_record10.ITEM_NUMBER ;
    G_View_Tab(L_Insert_Recd_Cnt).Vendor_Num          := G_View_Record10.vendor_number ;
    G_View_Tab(L_Insert_Recd_Cnt ).VENDOR_NAME        := g_view_record10.VENDOR_NAME ;
    G_View_Tab(L_Insert_Recd_Cnt ).Source             := G_View_Record10.Source ;
    G_View_Tab(L_Insert_Recd_Cnt ).St                 := G_View_Record10.St ;
    G_View_Tab(L_Insert_Recd_Cnt ).Description        := G_View_Record10.Description ;
    G_View_Tab(L_Insert_Recd_Cnt ).Cat                := G_View_Record10.cat_class ;
    G_View_Tab(L_Insert_Recd_Cnt ).Pplt               := G_View_Record10.Pplt ;
    G_View_Tab(L_Insert_Recd_Cnt ).Plt                := G_View_Record10.Plt ;
    G_View_Tab(L_Insert_Recd_Cnt ).Uom                := G_View_Record10.Uom ;
    G_View_Tab(L_Insert_Recd_Cnt ).Cl                 := G_View_Record10.Cl ;
    G_View_Tab(L_Insert_Recd_Cnt ).Stk_Flag           := G_View_Record10.Stk ;
    G_View_Tab(L_Insert_Recd_Cnt ).Pm                 := G_View_Record10.Pm ;
    G_View_Tab(L_Insert_Recd_Cnt ).Minn               := G_View_Record10.Minn ;
    G_View_Tab(L_Insert_Recd_Cnt ).Maxn               := G_View_Record10.Maxn ;
    G_View_Tab(L_Insert_Recd_Cnt ).Amu                := G_View_Record10.Amu ;
    G_View_Tab(L_Insert_Recd_Cnt ).Mf_Flag            := G_View_Record10.Mf_Flag ;
    G_View_Tab(L_Insert_Recd_Cnt ).Hit6_Sales         := G_View_Record10.Hit6_Sales ;
    G_View_Tab(L_Insert_Recd_Cnt ).Aver_Cost          := G_View_Record10.Aver_Cost ;
    G_View_Tab(L_Insert_Recd_Cnt ).Item_Cost          := G_View_Record10.Item_Cost;
    G_VIEW_TAB(L_INSERT_RECD_CNT ).BPA                := G_VIEW_RECORD10.BPA;
    G_View_Tab(L_Insert_Recd_Cnt ).Bpa_cost           := G_View_Record10.Bpa_cost;
    G_View_Tab(L_Insert_Recd_Cnt ).Qoh                := G_View_Record10.Qoh;
    G_View_Tab(L_Insert_Recd_Cnt ).Available          := G_View_Record10.Available;
    G_View_Tab(L_Insert_Recd_Cnt ).Availabledollar    := G_View_Record10.Availabledollar;
    G_View_Tab(L_Insert_Recd_Cnt ).Jan_Sales          := G_View_Record10.Jan_Sales;
    G_View_Tab(L_Insert_Recd_Cnt ).Feb_Sales          := G_View_Record10.Feb_Sales;
    G_View_Tab(L_Insert_Recd_Cnt ).Mar_Sales          := G_View_Record10.Mar_Sales;
    G_View_Tab(L_Insert_Recd_Cnt ).Apr_Sales          := G_View_Record10.Apr_Sales;
    G_View_Tab(L_Insert_Recd_Cnt ).May_Sales          := G_View_Record10.May_Sales;
    G_View_Tab(L_Insert_Recd_Cnt ).June_Sales         := G_View_Record10.June_Sales;
    G_View_Tab(L_Insert_Recd_Cnt ).Jul_Sales          := G_View_Record10.Jul_Sales;
    G_View_Tab(L_Insert_Recd_Cnt ).Aug_Sales          := G_View_Record10.Aug_Sales;
    G_View_Tab(L_Insert_Recd_Cnt ).Sep_Sales          := G_View_Record10.Sep_Sales;
    G_View_Tab(L_Insert_Recd_Cnt ).Oct_Sales          := G_View_Record10.Oct_Sales;
    G_View_Tab(L_Insert_Recd_Cnt ).Nov_Sales          := G_View_Record10.Nov_Sales;
    G_View_Tab(L_Insert_Recd_Cnt ).Dec_Sales          := G_View_Record10.Dec_Sales;
    G_View_Tab(L_Insert_Recd_Cnt ).Hit4_Sales         := G_View_Record10.Hit4_Sales;
    G_View_Tab(L_Insert_Recd_Cnt ).One_Sales          := G_View_Record10.One_Sales;
    G_View_Tab(L_Insert_Recd_Cnt ).Six_Sales          := G_View_Record10.Six_Sales;
    G_View_Tab(L_Insert_Recd_Cnt ).Twelve_Sales       := G_View_Record10.Twelve_Sales;
    G_View_Tab(L_Insert_Recd_Cnt ).Bin_Loc            := G_View_Record10.Bin_Loc;
    G_View_Tab(L_Insert_Recd_Cnt ).Mc                 := G_View_Record10.Mc;
    G_View_Tab(L_Insert_Recd_Cnt ).Fi_Flag            := G_View_Record10.Fi;
    G_View_Tab(L_Insert_Recd_Cnt ).Freeze_Date        := G_View_Record10.Freeze_Date;
    G_View_Tab(L_Insert_Recd_Cnt ).Res                := G_View_Record10.Res;
    G_View_Tab(L_Insert_Recd_Cnt ).Thirteen_Wk_Avg_Inv:= G_View_Record10.Thirteen_Wk_Avg_Inv ;
    G_View_Tab(L_Insert_Recd_Cnt ).THIRTEEN_WK_AN_COGS:= g_view_record10.THIRTEEN_WK_AN_COGS ;
    G_View_Tab(L_Insert_Recd_Cnt ).Turns              := G_View_Record10.Turns ;
    G_View_Tab(L_Insert_Recd_Cnt ).Buyer              := G_View_Record10.Buyer ;
    G_VIEW_TAB(L_INSERT_RECD_CNT ).TS                 := G_VIEW_RECORD10.TS ;
    G_VIEW_TAB(L_INSERT_RECD_CNT ).SO                 := G_VIEW_RECORD10.SO ;
    g_view_tab(l_insert_recd_cnt ).sourcing_rule      := g_view_record10.sourcing_rule ;
    g_View_Tab(L_Insert_Recd_Cnt ).clt                := g_View_Record10.clt ;
    G_View_Tab(L_Insert_Recd_Cnt ).INVENTORY_ITEM_ID  := g_view_record10.INVENTORY_ITEM_ID ;
    G_View_Tab(L_Insert_Recd_Cnt ).Organization_Id    := G_View_Record10.Organization_Id ;
    g_view_tab(l_insert_recd_cnt ).set_of_books_id    := g_view_record10.set_of_books_id ;
    G_View_Tab(L_Insert_Recd_Cnt ).on_ord             := g_view_record10.on_ord ;
    G_View_Tab(L_Insert_Recd_Cnt ).ORG_NAME           := g_view_record10.organization_name ;
    G_View_Tab(L_Insert_Recd_Cnt ).District           := G_View_Record10.District ;
    g_view_tab(l_insert_recd_cnt ).region             := g_view_record10.region ;
    G_View_Tab(L_Insert_Recd_Cnt ).wt                 := g_view_record10.wt ;
    g_view_tab(l_insert_recd_cnt ).ss                 := g_view_record10.ss ;
    g_view_tab(l_insert_recd_cnt ).fml                := g_view_record10.fml ;
    g_view_tab(l_insert_recd_cnt ).open_req           := g_view_record10.open_req ;
    g_view_tab(l_insert_recd_cnt ).common_output_id   :=xxeis.eis_rs_common_outputs_s.nextval;
    g_view_tab(l_insert_recd_cnt ).process_id         :=p_process_id;
    g_view_tab(l_insert_recd_cnt ).avail2             :=G_View_Record10.avail2;
    g_view_tab(l_insert_recd_cnt ).int_req            :=G_View_Record10.int_req;
    g_view_tab(l_insert_recd_cnt ).dir_req            :=G_View_Record10.dir_req;
    g_view_tab(l_insert_recd_cnt ).demand             :=G_View_Record10.demand;
    g_view_tab(l_insert_recd_cnt ).site_vendor_num    :=G_View_Record10.vendor_number;
    g_view_tab(l_insert_recd_cnt ).vendor_site        :=G_View_Record10.vendor_site;
    l_insert_recd_cnt                                 :=l_insert_recd_cnt+1;
  END LOOP;
  CLOSE l_ref_cursor;
  BEGIN
    fnd_file.put_line(fnd_file.log,'l_insert_recd_cnt' ||l_insert_recd_cnt);
    fnd_file.put_line(fnd_file.log,'first common_output_id' ||g_view_tab(1).common_output_id);
    --    Fnd_File.Put_Line(Fnd_File.Log,'last common_output_id' ||G_View_Tab(L_Insert_Recd_Cnt).Common_Output_Id);
    IF l_insert_recd_cnt >= 1 THEN
      forall i IN g_view_tab.FIRST .. g_view_tab.LAST
      INSERT INTO xxeis.eis_xxwc_po_isr_rpt_v VALUES g_view_tab
        (i
        );
    END IF;
    COMMIT;
    g_view_tab.DELETE;
  EXCEPTION
  WHEN OTHERS THEN
    fnd_file.put_line(fnd_file.log,'Inside Exception==>'||SUBSTR(sqlerrm,1,240));
  END;
END;
END;
/