create or replace procedure apps.XXCUS_OZF_MASS_COPY_FIX_PRC (retcode out varchar2, errbuf out varchar2, p_cal_year in varchar2) as
/*
 -- Author: Balaguru Seshadri
 -- Parameters: None
 -- Modification History
 -- ESMS                TMS                           Date                    Version   Comments
 -- =========== ===============  ==========       =======  ========================
 -- 311553          20151217-00158  22-DEC-2015   1.0           Created.
 */ 
--
    b_proceed1 BOOLEAN :=Null;
    b_proceed2 BOOLEAN :=Null;
    b_proceed3 BOOLEAN :=Null;
 --
     procedure print_log(p_message in varchar2) is
     begin
      if fnd_global.conc_request_id >0 then
       fnd_file.put_line(fnd_file.log, p_message);
       fnd_file.put_line(fnd_file.output, p_message);       
      else
       dbms_output.put_line(p_message);  
      end if;
     end print_log;
     --    
BEGIN
 --
 print_log('');
 print_log('Parameters:'); 
 print_log('=========='); 
 print_log('   Calendar Year :'||p_cal_year); 
 print_log(''); 
 --
 print_log('Begin data fix of mass copy');
 print_log('');  
 --
---------------------------------
----1 *** ----CAD updates
---------------------------------
 begin 
    --
    update ozf.ozf_funds_all_b
    set org_id =null
    where 1 =1
    and org_id is not null
    and fund_id in (select fund_id 
                    from  ozf.ozf_funds_all_tl
                    where 1=1
                    and short_name like '%FY%'||p_cal_year||'-CAD%');
     --
     print_log('@ CAD, Action: set org_id to blank value, Table: ozf.ozf_funds_all_b, rows updated: '||sql%rowcount);
     print_log('');    
     --
     b_proceed1 :=TRUE;
     --
 exception
   --
   when others then
    --
    print_log('@ CAD, Action 1: error in setting org_id to blank value, Table: ozf.ozf_funds_all_b, message:  '||sqlerrm);
   --
   b_proceed1 :=FALSE;
   --
 end;
 --
 -- Update ORG_ID in OZF.OZF_FUNDS_ALL_tl 
 if (b_proceed1) then 
  --
     begin 
        --
        update ozf.ozf_funds_all_tl
        set org_id =null
        where 1=1
        and short_name like '%FY%'||p_cal_year||'-CAD-%'
        and org_id is not null;
         --
         print_log('@ CAD, Action 2: set org_id to blank value, Table: ozf.ozf_funds_all_tl, rows updated: '||sql%rowcount);
         print_log('');    
         --
         b_proceed2 :=TRUE;
         --     
      exception
       --
       when others then
        --
        print_log('@ CAD, Action 2: error in setting org_id to blank value, Table: ozf.ozf_funds_all_tl, message:  '||sqlerrm);
        --
         b_proceed2 :=FALSE;
         --          
     end;
     --   
  --
 end if; 
 --
-- USA UPDATES, Set currency to USD
 if (b_proceed2) then 
  --
     begin 
        --
        update QP_LIST_HEADERS_B
        set currency_code='USD'
        where 1=1
        and attribute7=p_cal_year
        and ORIG_ORG_ID =101
        and currency_code is null
        and global_flag ='N';
         --
         print_log('@ USD, Action 1: set currency_code to USD, Table: QP_LIST_HEADERS_B, rows updated: '||sql%rowcount);
         print_log('');    
         --
         b_proceed3 :=TRUE;
         --     
      exception
       --
       when others then
        --
        print_log('@ USD, Action 1: error in setting currency_code to USD, Table: QP_LIST_HEADERS_B, message:  '||sqlerrm);
        --
         b_proceed3 :=FALSE;
         --          
     end;
     --   
  --
 end if; 
 --
 print_log('End data fix for mass copy');
 --
 if ( (NOT b_proceed1) OR (NOT b_proceed2) OR  (NOT b_proceed3) ) then
  --
  rollback;
  --
  raise program_error;
  --
 end if;
 --
EXCEPTION
 WHEN PROGRAM_ERROR THEN
  print_log ('One of the updates failed, Please check the log file for details');
 WHEN OTHERS THEN
  ROLLBACK;
  print_log ('Main Block, Error =>'||SQLERRM);
END;
/