/*****************************************************************************
  $Header TMS_20170423-00017_credit_rebill.sql $    
  Module Name: TMS_20170423-00017_credit_rebill.sql

  PURPOSE:   Credit and Rebill Program - Performance Issue

  REVISIONS:
  Ver    Date          Author            Description
  -----  ----------    ---------------   -----------------------------
  1.0    18-May-2017   Saini Neha        Initial Version - TMS#20170423-00017 
******************************************************************************/
CREATE INDEX ZX.XX_ZX_RATES_B_N1 ON ZX.ZX_RATES_B (TAX_JURISDICTION_CODE, TAX_REGIME_CODE, CONTENT_OWNER_ID, TAX_RATE_CODE);
/
BEGIN
fnd_stats.gather_table_stats ('ZX', 'ZX_RATES_B', dbms_stats.auto_sample_size, CASCADE=>TRUE); 
END;
/