/********************************************************************************
   $Header XXWC_OE_AIS_LPP_TBL_N1.sql $
   Module Name: XXWC_OE_AIS_LPP_TBL_N1

   PURPOSE:   This index to improve performance when quering the data from XXWC_OE_AIS_LPP_TBL table

   REVISIONS:
   Ver        Date        Author                     Description
   ---------  ----------  ---------------         -------------------------
   1.0        05/17/2016  Rakesh Patel            TMS# 20160516-00102 -LPP Performance Improvement
********************************************************************************/
CREATE INDEX XXWC.XXWC_OE_AIS_LPP_TBL_N1 ON XXWC.XXWC_OE_AIS_LPP_TBL (INVENTORY_ITEM_ID, SHIP_TO_ORG_ID);