/*************************************************************************
*   $Header XXWC_PRELOAD_OE_AIS_LPP_TBL.sql $
*
*   PURPOSE:   On time script to load last price paid in the XXWC_OE_AIS_LPP_TBL table.
*
*   REVISIONS:
*   Ver        Date         Author                Description
*   ---------  ----------   ---------------       -------------------------
*   1.0        17-May-2015  Rakesh Patel          Initial Version
*                                                 TMS#20160516-00102 -LPP Performance Improvement
* ***************************************************************************/
  
SET SERVEROUTPUT ON SIZE 1000000

DECLARE
BEGIN
   	DBMS_OUTPUT.put_line ('TMS: 20160516-00102 , Script 1 -Before Insert');

	DELETE FROM XXWC.XXWC_OE_AIS_LPP_TBL;
		
	DBMS_OUTPUT.put_line ('TMS: 20160516-00102, After Delete, records Deleted : '||sql%rowcount);
	
    DBMS_OUTPUT.put_line ('TMS: 20160516-00102, Profile values for XXWC_LAST_PRICE_PAID : '||FND_PROFILE.VALUE ('XXWC_LAST_PRICE_PAID'));
	DBMS_OUTPUT.put_line ('TMS: 20160516-00102, Profile values for XXWC_LAST_PRICE_PAID_EXCL: '|| FND_PROFILE.VALUE ('XXWC_LAST_PRICE_PAID_EXCL'));
   
	INSERT
	INTO XXWC.XXWC_OE_AIS_LPP_TBL
	  (
	    inventory_item_id ,
		ship_to_org_id,
	    last_price_paid ,
		line_creation_date,
		last_price_paid_excl_flag,
	    created_by ,
	    last_updated_by ,
	    creation_date ,
	    last_update_date ,
	    last_update_login
	  )
	(SELECT ool_lpp.inventory_item_id ,
	  ool_lpp.ship_to_org_id ,
	  ool_lpp.last_price_paid ,
	  MAX(ool_lpp.creation_date),
	  'Y',
	  fnd_global.user_id ,
	  fnd_global.user_id ,
	  sysdate ,
	  sysdate ,
	  fnd_global.login_id
	FROM (SELECT  ool.inventory_item_id
	             ,ool.ship_to_org_id
				 ,ool.ordered_item
				 ,ool.creation_date
                 ,FIRST_VALUE(ool.unit_selling_price) IGNORE NULLS 
                  OVER (PARTITION BY ool.inventory_item_id, ool.ship_to_org_id ORDER BY ool.creation_date desc) AS last_price_paid
            FROM oe_order_lines_all ool
           WHERE 1=1
           --  AND ool.inventory_item_id =  :p_inventory_item_id 
           --  AND ool.ship_to_org_id = :l_site_use_id
             AND ool.flow_status_code = 'CLOSED'
             AND ool.line_type_id IN (1002, 1005)
             AND TRUNC(ool.creation_date) >= TRUNC (SYSDATE - FND_PROFILE.VALUE ('XXWC_LAST_PRICE_PAID'))
             AND NOT EXISTS ( SELECT 1
                                FROM mtl_category_sets_v B
                                   , mtl_item_categories_v a
                               WHERE B.category_set_name = 'Inventory Category'
                                 AND a.category_set_id   = b.category_set_id
                                 AND a.organization_id   = 222
                                 AND a.inventory_item_id = ool.inventory_item_id
                          AND EXISTS (SELECT 1
                                        FROM fnd_lookup_values  flv
                                       WHERE flv.lookup_type  = 'XXWC_LAST_PRICE_PAID_EXCLUSION'
                                         AND flv.meaning      = a.category_id
                                         AND flv.enabled_flag = 'Y'
                                         AND TRUNC(SYSDATE) BETWEEN TRUNC(flv.start_date_active) AND TRUNC(NVL(flv.end_date_active,SYSDATE))
                                      )
                             )              
         ) ool_lpp
	GROUP BY ool_lpp.inventory_item_id, ool_lpp.ship_to_org_id, ool_lpp.last_price_paid);
	DBMS_OUTPUT.put_line ('TMS: 20160516-00102, Script 1 -After Insert, records Inserted : '||sql%rowcount);
	
	INSERT
	INTO XXWC.XXWC_OE_AIS_LPP_TBL
	  (
	    inventory_item_id ,
		ship_to_org_id,
		last_price_paid ,
		line_creation_date,
		last_price_paid_excl_flag,
	    created_by ,
	    last_updated_by ,
	    creation_date ,
	    last_update_date ,
	    last_update_login
	  )
	(SELECT ool_lpp.inventory_item_id ,
	  ool_lpp.ship_to_org_id ,
	  ool_lpp.last_price_paid ,
	  MAX(ool_lpp.creation_date),
	  'N',
	  fnd_global.user_id ,
	  fnd_global.user_id ,
	  sysdate ,
	  sysdate ,
	  fnd_global.login_id
	FROM (SELECT  ool.inventory_item_id
	             ,ool.ship_to_org_id
				 ,ool.ordered_item
				 ,ool.creation_date
                 ,FIRST_VALUE(ool.unit_selling_price) IGNORE NULLS 
                  OVER (PARTITION BY ool.inventory_item_id, ool.ship_to_org_id ORDER BY ool.creation_date desc) AS last_price_paid
            FROM oe_order_lines_all ool
           WHERE 1=1
           --  AND ool.inventory_item_id = :p_inventory_item_id
           --  AND ool.ship_to_org_id = :l_site_use_id
             AND ool.flow_status_code = 'CLOSED'
             AND ool.line_type_id IN (1002, 1005)
             AND TRUNC(ool.creation_date) >= TRUNC (SYSDATE - FND_PROFILE.VALUE ('XXWC_LAST_PRICE_PAID_EXCL'))
             AND EXISTS ( SELECT 1
                                FROM mtl_category_sets_v B
                                   , mtl_item_categories_v a
                               WHERE B.category_set_name = 'Inventory Category'
                                 AND a.category_set_id   = b.category_set_id
                                 AND a.organization_id   = 222
                                 AND a.inventory_item_id = ool.inventory_item_id
                          AND EXISTS (SELECT 1
                                        FROM fnd_lookup_values  flv
                                       WHERE flv.lookup_type  = 'XXWC_LAST_PRICE_PAID_EXCLUSION'
                                         AND flv.meaning      = a.category_id
                                         AND flv.enabled_flag = 'Y'
                                         AND TRUNC(SYSDATE) BETWEEN TRUNC(flv.start_date_active) AND TRUNC(NVL(flv.end_date_active,SYSDATE))
                                      )
                             )              
         ) ool_lpp
	GROUP BY ool_lpp.inventory_item_id, ool_lpp.ship_to_org_id, ool_lpp.last_price_paid);
	
	DBMS_OUTPUT.put_line ('TMS: 20160516-00102, Script 2 -After Insert, records Inserted : '||sql%rowcount);
	
	COMMIT;
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20160516-00102, Errors ='||SQLERRM);
END;
/