CREATE OR REPLACE PACKAGE APPS.XXWC_OE_AIS_LPP_UPDATE_PKG AS
/****************************************************************************************************************************************************
*   $Header XXWC_OE_AIS_LPP_UPDATE_PKG $
*   Module Name: XXWC_OE_AIS_LPP_UPDATE_PKG
*
*   PURPOSE:   Package to update XXWC_OE_AIS_LPP_TBL table
*
*   REVISIONS:
*   Ver        Date        Author                     Description
*   ---------  ----------  ---------------         -------------------------
*   1.0        05/16/2016  Rakesh Patel            Initial Version
******************************************************************************************************************************************************/
  PROCEDURE main(errbuf          OUT VARCHAR2
                ,retcode         OUT NUMBER
                ,p_line_cr_date   IN VARCHAR2);

END XXWC_OE_AIS_LPP_UPDATE_PKG;
/