CREATE OR REPLACE PACKAGE BODY APPS.xxwc_inv_ais_pkg
IS
   /*************************************************************************
     $Header: xxwc_inv_ais_pkg $
     Module Name: xxwc_inv_ais_pkg.pkb

     PURPOSE:   This package is used AIS 

     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        11/05/2012  Shankar Hariharan       Initial Version
     2.0        01/23/2013  Shankar Hariharan       UOM Code fix TMS 20130124-00974
     3.0        02/18/2013  Shankar Hariharan       Closed Codeto include "Closed for Invoice" 
                                                   status. TMS 20130206-00890   
     4.0        08/22/2013  Consuelo Gonzalez       TMS 20130821-00995: Update to 
                                                    exclude drop ships in the
                                                    get_onorder_qty function
     5.0        10/29/2014  Vijaysrinivasan        TMS#20141002-00042 Multi org changes 
     6.0        01/13/2015  Shankar Hariharan      Included line_location_id in where condition (bug fix) Task ID: 20141215-00214 
     7.0        03/16/2015  Shankar Hariharan     Performance issues Task ID:  20140919-00308
     8.0        02/19/2016  Pattabhi Avula         TMS#20160208-00179  -- 1) Added lookup condition and excluded the category_id
                                                    from the query. 2) Reduced the time from 90 days to 45 days by using the profile
     8.1        03/09/2016  Gopi Damuluri           TMS# 20160309-00148 - Resolve LastPricePaid issue for new AIS form
	 9.0        05/17/2016  Rakesh Patel            TMS-20160516-00102 -LPP Performance Improvement
   **************************************************************************/
   FUNCTION get_onorder_qty (i_organization_id     IN NUMBER
                           , i_inventory_item_id   IN NUMBER)
      RETURN NUMBER
   IS
      l_po_qty    NUMBER;
      l_req_qty   NUMBER;
   BEGIN
      SELECT NVL (
                SUM (
                   (y.quantity - y.quantity_received-y.quantity_cancelled) --TMS 20130206-00890
                   * po_uom_s.po_uom_convert (x.ordered_uom
                                            , x.primary_uom
                                            , x.item_id))
              , 0)
        INTO l_po_qty
        --Modified the below line for TMS ## 20141002-00042 by VijaySrinivasan  on 10/29/2014 
        FROM rcv_enter_receipts_po_v x, po_line_locations y  
       WHERE     x.to_organization_id = i_organization_id
             AND x.item_id = i_inventory_item_id
             AND x.po_line_id = y.po_line_id
             -- 08/22/2013 CG: TMS 20130821-00995: Excluding Drop Ship POs
             AND nvl(y.drop_ship_flag, 'N') = 'N'
             AND x.po_line_location_id=y.line_location_id -- Added by Shankar 13/Jan/15 Task ID: 20141215-00214 
             AND x.closed_code in ('OPEN','CLOSED FOR INVOICE'); --TMS 20130206-00890

      SELECT NVL (
                SUM (
                   (  b.quantity
                    - NVL (b.quantity_received, 0)
                    - NVL (b.quantity_cancelled, 0))
                   * po_uom_s.po_uom_convert (d.unit_of_measure
                                            , c.primary_unit_of_measure
                                            , b.item_id))
              , 0)  -- Shankar TMS 20130124-00974 23-Jan-2013
        INTO l_req_qty
        --Modified the below 2 lines for TMS ## 20141002-00042 by VijaySrinivasan  on 10/29/2014 
        FROM po_requisition_lines b      
           , po_requisition_headers a    
           , mtl_system_items c
           , mtl_units_of_measure d
       WHERE     b.source_type_code = 'INVENTORY'
             AND NVL (b.cancel_flag, 'N') = 'N'
             AND a.type_lookup_code = 'INTERNAL'
             AND a.authorization_status = 'APPROVED'
             AND a.requisition_header_id = b.requisition_header_id
             AND b.item_id = c.inventory_item_id
             AND b.unit_meas_lookup_code = d.unit_of_measure
             AND b.destination_organization_id = c.organization_id
             AND b.destination_organization_id = i_organization_id
             AND b.item_id = i_inventory_item_id
             -- 08/22/2013 CG: TMS 20130821-00995: Excluding Drop Ship POs
             AND nvl(b.drop_ship_flag, 'N') = 'N';

      RETURN (l_po_qty + l_req_qty);
   END get_onorder_qty;
/*************************************************************************
     $Header: get_last_price_paid $
     Module Name: get_last_price_paid

     PURPOSE:   This function is used AIS Lite Form  

     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        11/05/2012  Shankar Hariharan       Initial Version
        8.0        02/19/2016  Pattabhi Avula         TMS#20160208-00179  -- 1) Added lookup condition and excluded the category_id
                                                    from the query. 2) Reduced the time from 90 days to 45 days by using the profile
**************************************************************************/
   FUNCTION get_last_price_paid (p_header_id IN NUMBER, p_quote_number IN NUMBER, p_inventory_item_id IN NUMBER)
   RETURN NUMBER
   IS
    l_site_use_id       number;
    l_last_price_paid   number;
    l_category_cnt      number;  -- Vers#8.0
   BEGIN
    l_site_use_id := null;
    l_last_price_paid := null;
    -- <START>  -- Vers#8.0
     BEGIN
       select count(*)
         into l_category_cnt
          from MTL_CATEGORY_SETS_V B, 
              MTL_ITEM_CATEGORIES_V a
         where  B.CATEGORY_SET_NAME = 'Inventory Category'
          and  a.category_set_id=b.category_set_id
           and a.ORGANIZATION_ID=222
           and a.inventory_item_id=p_inventory_item_id
           and exists ( select 1 
                        from FND_LOOKUP_VALUES  
                    where LOOKUP_TYPE='XXWC_LAST_PRICE_PAID_EXCLUSION'
                    and meaning=a.category_id);
        exception
         when others then
            l_category_cnt:=NULL;
         END;
    -- <END>  -- Vers#8.0
   
    if p_header_id is not null and p_quote_number is null then
        begin
            select  ship_to_org_id
            into    l_site_use_id
        --Modified the below line for TMS ## 20141002-00042 by VijaySrinivasan  on 10/29/2014 
            from    oe_order_headers     
            where   header_id = p_header_id;
            
            -- <START>  -- Vers#8.0
            if l_category_cnt > 0 then
            l_last_price_paid:=null;
            else 
           -- <END>  -- Vers#8.0            
            begin
                select  x1.unit_selling_price
                into    l_last_price_paid
                from    (
                            select  unit_selling_price
                                    , creation_date
        --Modified the below line for TMS ## 20141002-00042 by VijaySrinivasan  on 10/29/2014 
                            from    oe_order_lines   
                            where   inventory_item_id = p_inventory_item_id
                            and     ship_to_org_id = l_site_use_id
                            /*
                            and     nvl(cancelled_flag, 'N') = 'N'
                            and     nvl(booked_flag, 'N') = 'Y' */
                            and     flow_status_code = 'CLOSED'  -- Commented and added by Shankar 20140919-00308
                            and     line_type_id in (1002,1005)
                            -- 09/09/2013 CG: TMS 20130909-00564: Last Price Paid Function Update to only pull last 90 days
                            --and     creation_date >= trunc(sysdate - 90)  -- Added for Vers#8.0
                            and     creation_date >= trunc(sysdate - FND_PROFILE.VALUE('XXWC_LAST_PRICE_PAID'))  -- Added for Vers#8.0
                            order by 2 desc
                        ) x1
                where   rownum = 1;
            exception
            when others then
                l_last_price_paid := null;
            end;
          end if;   -- Added for Vers#8.0
        exception
        when others then
            l_last_price_paid := null;
        end;
    elsif p_header_id is null and p_quote_number is not null then
        begin
            select  site_use_id
            into    l_site_use_id
        --Modified the below line for TMS ## 20141002-00042 by VijaySrinivasan  on 10/29/2014 
            from    xxwc.xxwc_om_quote_headers       
            where   quote_number = p_quote_number;
            
            -- <START>  -- Vers#8.0            
            if l_category_cnt > 0 then
            l_last_price_paid:=null;
            else 
            -- <END>  -- Vers#8.0            
            begin
                select  x1.unit_selling_price
                into    l_last_price_paid
                from    (
                            select  unit_selling_price
                                    , creation_date
        --Modified the below line for TMS ## 20141002-00042 by VijaySrinivasan  on 10/29/2014 
                            from    oe_order_lines       
                            where   inventory_item_id = p_inventory_item_id
                            and     ship_to_org_id = l_site_use_id
                            /*
                            and     nvl(cancelled_flag, 'N') = 'N'
                            and     nvl(booked_flag, 'N') = 'Y'    */
                            and     flow_status_code = 'CLOSED' -- Commented and added by Shankar 20140919-00308
                            and     line_type_id in (1002,1005)
                            -- 09/09/2013 CG: TMS 20130909-00564: Last Price Paid Function Update to only pull last 90 days
                            --and     trunc(creation_date) >= trunc(sysdate - 90)
                            and     creation_date >= trunc(sysdate - FND_PROFILE.VALUE('XXWC_LAST_PRICE_PAID'))   -- Added for Vers#8.0
                            order by 2 desc
                        ) x1
                where   rownum = 1;
            exception
            when others then
                l_last_price_paid := null;
            end;
           end if;    -- Added for Vers#8.0
        exception
        when others then
            l_last_price_paid := null;
        end;
    else
        l_last_price_paid := null;
    end if;
   
    return l_last_price_paid;
   EXCEPTION
   WHEN OTHERS THEN
    return null;
   END get_last_price_paid;

/*************************************************************************
     Module Name: shipto_last_price_paid

     PURPOSE:   This function is used AIS Lite Form  

     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     8.1        03/09/2016  Gopi Damuluri           TMS# 20160309-00148 - Resolve LastPricePaid issue for new AIS form
	 9.0        05/17/2016  Rakesh Patel            TMS-20160516-00102 -LPP Performance Improvement
**************************************************************************/

FUNCTION shipto_last_price_paid (p_site_use_id         IN NUMBER,
                                 p_inventory_item_id   IN NUMBER)
   RETURN NUMBER
IS
   l_last_price_paid   NUMBER;
   l_category_cnt      NUMBER;
BEGIN
   -- Version# 9.0 > Start
   l_last_price_paid := NULL;
   
   IF p_site_use_id IS NOT NULL THEN
      BEGIN
         SELECT last_price_paid
           INTO l_last_price_paid
           FROM XXWC.XXWC_OE_AIS_LPP_TBL
		  WHERE inventory_item_id = p_inventory_item_id
            AND ship_to_org_id    = p_site_use_id
            AND ROWNUM = 1;
      EXCEPTION
      WHEN OTHERS THEN
         l_last_price_paid := NULL;
      END;
   END IF;
   
   RETURN l_last_price_paid;
   -- Version# 9.0 < End
   
   -- Commented for ver 9.0 > Start 
   /**
   IF p_site_use_id IS NOT NULL THEN
   l_last_price_paid := NULL;

   BEGIN
      SELECT COUNT (1)
        INTO l_category_cnt
        FROM mtl_category_sets_v B
           , mtl_item_categories_v a
       WHERE B.category_set_name = 'Inventory Category'
         AND a.category_set_id   = b.category_set_id
         AND a.organization_id   = 222
         AND a.inventory_item_id = p_inventory_item_id
         AND EXISTS (SELECT 1
                       FROM fnd_lookup_values  flv
                      WHERE flv.lookup_type  = 'XXWC_LAST_PRICE_PAID_EXCLUSION'
                        AND flv.meaning      = a.category_id
                        AND flv.enabled_flag = 'Y'
                        AND TRUNC(SYSDATE) BETWEEN TRUNC(flv.start_date_active) AND TRUNC(NVL(flv.end_date_active,SYSDATE))
                        );
   EXCEPTION
      WHEN OTHERS
      THEN
         l_category_cnt := 0;
   END;

   IF l_category_cnt > 0
   THEN
      l_last_price_paid := NULL;
   ELSE
      BEGIN
         SELECT x1.unit_selling_price
           INTO l_last_price_paid
           FROM (  SELECT unit_selling_price, creation_date
                     FROM oe_order_lines
                    WHERE inventory_item_id = p_inventory_item_id
                      AND ship_to_org_id    = p_site_use_id
                      AND flow_status_code  = 'CLOSED'
                      AND line_type_id     IN (1002, 1005)
                      AND creation_date    >= TRUNC (SYSDATE  - FND_PROFILE.VALUE ('XXWC_LAST_PRICE_PAID'))
                 ORDER BY 2 DESC) x1
          WHERE ROWNUM = 1;
      EXCEPTION
      WHEN OTHERS THEN
         l_last_price_paid := NULL;
      END;
   END IF;
   END IF; -- IF p_site_use_id IS NOT NULL THEN 
   
   RETURN l_last_price_paid; **/ -- Commented for ver 9.0 < End

EXCEPTION
WHEN OTHERS THEN
   RETURN NULL;
END shipto_last_price_paid;

END xxwc_inv_ais_pkg;
/