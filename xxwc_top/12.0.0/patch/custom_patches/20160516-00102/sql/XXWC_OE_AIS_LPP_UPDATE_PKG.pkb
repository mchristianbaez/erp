CREATE OR REPLACE PACKAGE BODY apps.XXWC_OE_AIS_LPP_UPDATE_PKG IS
/****************************************************************************************************************************************************
*   $Header XXWC_OE_AIS_LPP_UPDATE_PKG $
*   Module Name: XXWC_OE_AIS_LPP_UPDATE_PKG
*
*   PURPOSE:   Package used to update XXWC_OE_AIS_LPP_TBL table
*
*   REVISIONS:
*   Ver        Date        Author                     Description
*   ---------  ----------  ---------------         -------------------------
*   1.0        05/09/2016  Rakesh Patel            Initial Version
******************************************************************************************************************************************************/

  ----------------------------------------------------------------------------------
  -- GLOBAL VARIABLES
  ----------------------------------------------------------------------------------
  g_request_id               NUMBER;
  g_retcode                  NUMBER;  
  g_err_msg                  VARCHAR2(3000);

  --Email Defaults
  g_dflt_email fnd_user.email_address%TYPE := 'HDSOracleDevelopers@hdsupply.com';


  /********************************************************************************
  ProcedureName : update_last_price_paid_tbl
  Purpose       : to update XXWC_OE_AIS_LPP_TBL table
  
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     05/16/2016    Rakesh Patel    Initial version.
  ********************************************************************************/
  PROCEDURE update_last_price_paid_tbl(p_line_cr_date IN DATE) AS
 
    l_message         VARCHAR2(2000);
    l_error_code      NUMBER;
    p_error_msg       VARCHAR2(2000);
    l_batch_source_id NUMBER;
	l_sec             VARCHAR2 (100);
	l_update_rec_cnt  NUMBER :=0;
	l_insert_rec_cnt  NUMBER :=0;
	
	cursor cur_closed_order_lines (p_line_cr_date IN DATE) is
	    SELECT ool.inventory_item_id
	              ,ool.ship_to_org_id
                      ,ool.ordered_item
                      ,ool.creation_date
                      ,ool.unit_selling_price
                      ,'Y' last_price_paid_excl_flag
              FROM oe_order_lines_all ool
             WHERE 1=1
           --  AND ool.inventory_item_id = 2943507 :p_inventory_item_id 
           --  AND ool.ship_to_org_id =1523349 --:l_site_use_id
               AND ool.flow_status_code = 'CLOSED'
               AND ool.line_type_id IN (1002, 1005)
               AND TRUNC(ool.creation_date) >= TRUNC (SYSDATE - FND_PROFILE.VALUE ('XXWC_LAST_PRICE_PAID'))
               AND TRUNC(ool.last_update_date) = TRUNC (p_line_cr_date)
	       AND NOT EXISTS ( SELECT 1
                                  FROM mtl_category_sets_v B
                                     , mtl_item_categories_v a
                                 WHERE B.category_set_name = 'Inventory Category'
                                   AND a.category_set_id   = b.category_set_id
                                   AND a.organization_id   = 222
                                   AND a.inventory_item_id = ool.inventory_item_id
                            AND EXISTS (SELECT 1
                                          FROM fnd_lookup_values  flv
                                         WHERE flv.lookup_type  = 'XXWC_LAST_PRICE_PAID_EXCLUSION'
                                           AND flv.meaning      = a.category_id
                                           AND flv.enabled_flag = 'Y'
                                           AND TRUNC(SYSDATE) BETWEEN TRUNC(flv.start_date_active) AND TRUNC(NVL(flv.end_date_active,SYSDATE))
                                       )
                              )
         UNION              
            SELECT ool.inventory_item_id
                  ,ool.ship_to_org_id
                  ,ool.ordered_item
                  ,ool.creation_date
                  ,ool.unit_selling_price
                  ,'N' last_price_paid_excl_flag
              FROM oe_order_lines_all ool
             WHERE 1=1
           --  AND ool.inventory_item_id = :p_inventory_item_id 
           --  AND ool.ship_to_org_id    =:l_site_use_id
               AND ool.flow_status_code = 'CLOSED'
               AND ool.line_type_id IN (1002, 1005)
               AND TRUNC(ool.creation_date) >= TRUNC (SYSDATE - FND_PROFILE.VALUE ('XXWC_LAST_PRICE_PAID_EXCL'))
               AND TRUNC(ool.last_update_date) = TRUNC (p_line_cr_date)
	           AND EXISTS ( SELECT 1
                                  FROM mtl_category_sets_v B
                                     , mtl_item_categories_v a
                                 WHERE B.category_set_name = 'Inventory Category'
                                   AND a.category_set_id   = b.category_set_id
                                   AND a.organization_id   = 222
                                   AND a.inventory_item_id = ool.inventory_item_id
                            AND EXISTS (SELECT 1
                                          FROM fnd_lookup_values  flv
                                         WHERE flv.lookup_type  = 'XXWC_LAST_PRICE_PAID_EXCLUSION'
                                           AND flv.meaning      = a.category_id
                                           AND flv.enabled_flag = 'Y'
                                           AND TRUNC(SYSDATE) BETWEEN TRUNC(flv.start_date_active) AND TRUNC(NVL(flv.end_date_active,SYSDATE))
                                       )
                              );   
  BEGIN
    l_sec := 'Start update_last_price_paid_tbl';
	
    fnd_file.put_line(fnd_file.log, '----------------------------------------------------------------------------------');
    fnd_file.put_line(fnd_file.log, 'Start of Procedure : update_last_price_paid_tbl');
    fnd_file.put_line(fnd_file.log, '----------------------------------------------------------------------------------');
    
	l_sec := 'Delete records from update_last_price_paid_tbl';
	
	DELETE from XXWC.XXWC_OE_AIS_LPP_TBL
	 WHERE last_price_paid_excl_flag = 'Y'
	   AND line_creation_date        < TRUNC (SYSDATE - FND_PROFILE.VALUE ('XXWC_LAST_PRICE_PAID'));
	   
	fnd_file.put_line(fnd_file.log, 'Records deleted from XXWC_OE_LAST_PRICE_PAID_TBL older than 45 days: '||sql%rowcount);
	
	DELETE from XXWC.XXWC_OE_AIS_LPP_TBL
	 WHERE last_price_paid_excl_flag = 'N'
	   AND line_creation_date        < TRUNC (SYSDATE - FND_PROFILE.VALUE ('XXWC_LAST_PRICE_PAID_EXCL'));
	
	fnd_file.put_line(fnd_file.log, 'Records deleted from XXWC_OE_AIS_LPP_TBL older than 14 days: '||sql%rowcount);
	
	l_sec := 'update records in update_last_price_paid_tbl table';
	FOR c1 in cur_closed_order_lines ( p_line_cr_date => p_line_cr_date )
    LOOP
       l_update_rec_cnt := l_update_rec_cnt+1;
       
	   UPDATE XXWC.XXWC_OE_AIS_LPP_TBL
              SET last_price_paid   = c1.unit_selling_price,
                  last_update_Date  = sysdate,
                  last_updated_by   = fnd_global.user_id,
                  last_update_login = fnd_global.login_id
        WHERE inventory_item_id = c1.inventory_item_id
          AND ship_to_org_id    = c1.ship_to_org_id;

                IF SQL%ROWCOUNT = 0 THEN
		         BEGIN
		            l_insert_rec_cnt := l_insert_rec_cnt+1;
                            INSERT
				INTO XXWC.XXWC_OE_AIS_LPP_TBL
                                  (
                                   inventory_item_id ,
                                   ship_to_org_id,
                                   last_price_paid ,
                                   line_creation_date,
                                   last_price_paid_excl_flag,
                                   created_by ,
                                   last_updated_by ,
                                   creation_date ,
                                   last_update_date ,
                                   last_update_login
				  )
                           VALUES(
                                   c1.inventory_item_id ,
                                   c1.ship_to_org_id ,
                                   c1.unit_selling_price,
                                   c1.creation_date,
                                   c1.last_price_paid_excl_flag,
                                   fnd_global.user_id ,
                                   fnd_global.user_id ,
                                   sysdate ,
                                   sysdate ,
                                   fnd_global.login_id
				  );
			 EXCEPTION WHEN OTHERS THEN
                              p_error_msg := 'Unable to insert record in the XXWC_OE_AIS_LPP_TBL procedure'||SQLERRM;
                              xxcus_error_pkg.xxcus_error_main_api (
                                              p_called_from         => 'XXWC_LPP_UPDATE_PKG.update_last_price_paid_tbl',
                                              p_calling             => l_sec,
                                              p_request_id          => fnd_global.conc_request_id,
                                              p_ora_error_msg       => SUBSTR (
                                                                                 ' Error_Stack...'
                                                                                || DBMS_UTILITY.format_error_stack ()
                                                                                || ' Error_Backtrace...'
                                                                                || DBMS_UTILITY.format_error_backtrace (),
                                                                                1,
                                                                                2000),
                                              p_error_desc          => SUBSTR (p_error_msg, 1, 240),
                                              p_distribution_list   => g_dflt_email,
                                              p_module              => 'OM');			 
			 END;
                END IF; --SQL%ROWCOUNT
	END LOOP;
	
	COMMIT;
	fnd_file.put_line(fnd_file.log, 'Records updated in XXWC_OE_AIS_LPP_TBL: '||l_update_rec_cnt);
	fnd_file.put_line(fnd_file.log, 'Records inserted into XXWC_OE_AIS_LPP_TBL: '||l_insert_rec_cnt);
	
    fnd_file.put_line(fnd_file.log, '----------------------------------------------------------------------------------');
    fnd_file.put_line(fnd_file.log, 'End of Procedure : update_last_price_paid_tbl');
    fnd_file.put_line(fnd_file.log, '----------------------------------------------------------------------------------');
  
  EXCEPTION
    WHEN OTHERS THEN
      l_error_code    := SQLCODE;
      p_error_msg := 'Error in the update_last_price_paid_tbl procedure'||SQLERRM;
      
	  fnd_file.put_line(fnd_file.log
                       ,'Exception Block of Standard Program Call Procedure');
      fnd_file.put_line(fnd_file.log
                       ,'The Error Code Traced is : ' || l_error_code);
      fnd_file.put_line(fnd_file.log
                       ,'The Error Message Traced is : ' ||
                        p_error_msg);
    
      g_retcode := 2;
      g_err_msg := p_error_msg;
	 
      xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_LPP_UPDATE_PKG.update_last_price_paid_tbl',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (p_error_msg, 1, 240),
            p_distribution_list   => g_dflt_email,
            p_module              => 'OM');	  		
  END update_last_price_paid_tbl;

  /********************************************************************************
  ProcedureName : MAIN
  Purpose       : This is mail program and will be called by concurrent program to 
                  update, insert or detele records from XXWC_OE_AIS_LPP_TBL
				  on daily basis.
  
  HISTORY
   ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     05/16/2016    Rakesh Patel    Initial version.
  ********************************************************************************/
  ----------------------------------------------------------------------------------
  -- Main Procedure
  ----------------------------------------------------------------------------------
  PROCEDURE main(errbuf          OUT VARCHAR2
                ,retcode         OUT NUMBER
                ,p_line_cr_date   IN VARCHAR2) AS
    
    l_error_code    NUMBER;
    p_error_msg     VARCHAR2(2000);
	l_sec           VARCHAR2 (100);
	l_line_cr_date  DATE;
  
  BEGIN
    l_sec := 'Start main';
  
    fnd_file.put_line(fnd_file.log, '----------------------------------------------------------------------------------');
    fnd_file.put_line(fnd_file.log, 'Start of Procedure : MAIN');
    fnd_file.put_line(fnd_file.log, '----------------------------------------------------------------------------------');
	
    IF p_line_cr_date IS NOT NULL THEN
       l_line_cr_date := TO_DATE (p_line_cr_date, 'YYYY/MM/DD HH24:MI:SS');
    ELSE
       l_line_cr_date := SYSDATE;
    END IF;   
    fnd_file.put_line(fnd_file.log, 'Input Parameters     :');
    fnd_file.put_line(fnd_file.log,'l_line_cr_date        :' || l_line_cr_date);
    fnd_file.put_line(fnd_file.log,'----------------------------------------------------------------------------------');
    
	l_sec := 'call last_price_paid_tbl';
    ----------------------------------------------------------------------------------
    -- Calling the last_price_paid_tbl procedure to update XXWC_OE_AIS_LPP_TBL table
    ----------------------------------------------------------------------------------
    update_last_price_paid_tbl(l_line_cr_date);
    
    l_sec := 'After call to last_price_paid_tbl';
    
	retcode := g_retcode;
    errbuf  := g_err_msg;
  
    fnd_file.put_line(fnd_file.log,'----------------------------------------------------------------------------------');
    fnd_file.put_line(fnd_file.log, 'End of Procedure : MAIN');
    fnd_file.put_line(fnd_file.log,'----------------------------------------------------------------------------------');
  
  EXCEPTION
    WHEN OTHERS THEN
      l_error_code    := SQLCODE;
      p_error_msg := 'Error in the main procedure'||SQLERRM;
    
      retcode := 2;
      errbuf  := p_error_msg;
    
      fnd_file.put_line(fnd_file.log, 'Exception Block of Main Procedure');
      fnd_file.put_line(fnd_file.log
                       ,'The Error Code Traced is : ' || l_error_code);
      fnd_file.put_line(fnd_file.log
                       ,'The Error Message Traced is : ' ||
                        p_error_msg);
      xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_LPP_UPDATE_PKG.main',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (p_error_msg, 1, 240),
            p_distribution_list   => g_dflt_email,
            p_module              => 'OM');	  						
  END main;

END XXWC_OE_AIS_LPP_UPDATE_PKG;
/