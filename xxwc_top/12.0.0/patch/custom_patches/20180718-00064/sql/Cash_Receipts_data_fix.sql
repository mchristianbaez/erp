
SET SERVEROUT ON;


BEGIN
   DBMS_OUTPUT.PUT_LINE ('BEFORE DELETE');

   DELETE FROM xxwc.xxwcar_cash_rcpts_tbl
         WHERE status = 'NEW';

   DBMS_OUTPUT.PUT_LINE ('Rows deleted' || SQL%ROWCOUNT);

   IF SQL%ROWCOUNT = 622
   THEN
      COMMIT;
   ELSE
      ROLLBACK;
   END IF;

   DBMS_OUTPUT.PUT_LINE ('AFTER DELETE');
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.PUT_LINE ('ERROR: ' || SQLERRM);
	  ROLLBACK;
END;
/




  