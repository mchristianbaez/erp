CREATE OR REPLACE PACKAGE BODY APPS.XXWC_MD_SEARCH_REFRESH_PKG
AS
   /*************************************************************************
    Copyright (c) 2012 Lucidity Consulting Group
    All rights reserved.
   **************************************************************************
     $Header XXWC_MD_SEARCH_REFRESH_PKG $
     Module Name: XXWC_MD_SEARCH_REFRESH_PKG.pkb

     PURPOSE:   This package is used by AIS process to refresh MVs , Indexes and create Synonym
          MV      : XXWC_MD_SEARCH_PRODUCTS_MV
          MV2     : XXWC_MD_SEARCH_PRODUCTS_MV2
          SYNONYM : XXWC_MD_SEARCH_PRODUCTS_MV_S

     REVISIONS:
     Ver        Date        Author                  Description
     ---------  ----------  ---------------         -------------------------
     1.0        06/28/2016   Gopi Damuluri           Initial Version TMS# 20160628-00001
     2.0        11/21/2016   Nancy Pahwa             TMS 20161121-00176
     3.0        11/22/2016   Nancy Pahwa             TMS 20161121-00176 Index 8 to use pref1 under XXWC schema
     3.1        02/13/2016   Niraj K Ranjan          TMS 20170202-00084 AIS MV refresh process - Resolve Invalid objects issue
	 5.1        02/02/2018   Ashwin Sridhar          TMS#20180127-00004-WC UC4 Error - WC.EBS.AIS.REFRESH_AIS_MV_WF  WC.EBS.AIS.REFRESH_AIS_MV
	 6.0        08/17/2018   Rakesh Patel            TMS#20180808-00096-Fix AIS MV refresh UC4 flow to aviod sending notification
	 6.1        10/17/2018   P.Vamshidhar            TMS#20181010-00001- XXWC AIS MV Refresh Process Completed in Error
     **************************************************************************/
   /*************************************************************************
     Function : is_object_exists

     PURPOSE:   This function is used to verify if db object exists or not

     REVISIONS:
     Ver        Date        Author                  Description
     ---------  ----------  ---------------         -------------------------
     3.1       02/13/2016  Niraj K Ranjan          TMS 20170202-00084 AIS MV refresh process - Resolve Invalid objects issue
   ************************************************************************/
   FUNCTION is_object_exists (p_object_name    dba_objects.object_name%TYPE,
                              p_object_type    dba_objects.object_type%TYPE)
      RETURN NUMBER
   IS
      l_sec     VARCHAR2 (100);
      l_count   NUMBER;
   BEGIN
      l_sec := 'inside function is_object_exists';

      SELECT COUNT (1)
        INTO l_count
        FROM dba_objects
       WHERE     owner = 'APPS'
             AND object_type = p_object_type
             AND OBJECT_NAME = p_object_name;

      RETURN l_count;
   EXCEPTION
      WHEN OTHERS
      THEN
         l_count := 0;
         RETURN l_count;
   END is_object_exists;

   /*************************************************************************
        Procedure : refresh_ais_mv

        PURPOSE:   This procedure is to refresh AIS XXWC_MD_SEARCH_PRODUCTS_MV and its Indexes

        REVISIONS:
        Ver        Date        Author                  Description
        ---------  ----------  ---------------         -------------------------
        1.0        06/28/2016  Gopi Damuluri           Initial Version TMS# 20160628-00001
        2.0        11/21/2016  Nancy Pahwa             TMS 20161121-00176
        3.1        02/13/2016  Niraj K Ranjan          TMS 20170202-00084 AIS MV refresh process - Resolve Invalid objects issue
        4.1        01/18/2018  Nancy Pahwa             Task ID: 20180118-00337 work around for MV refresh bug
		5.1        02/02/2018  Ashwin Sridhar          TMS#20180127-00004-WC UC4 Error - WC.EBS.AIS.REFRESH_AIS_MV_WF  WC.EBS.AIS.REFRESH_AIS_MV
	    6.1        10/17/2018   P.Vamshidhar            TMS#20181010-00001- XXWC AIS MV Refresh Process Completed in Error		
   ************************************************************************/
   PROCEDURE refresh_ais_mv (p_err_buf       OUT VARCHAR2,
                             p_return_code   OUT NUMBER)
   IS

      
--Ver 5.1 Start

CURSOR cu_wait_session IS		  
SELECT w1.sid blocked_session
,  h1.sid holding_session
,  w.kgllktype lock_or_pin
,  w.kgllkhdl  address
,  DECODE(h.kgllkmod, 0, 'None', 1, 'Null', 2, 'Share', 3, 'Exclusive', 'Unknown') mode_held
,  DECODE(w.kgllkreq, 0, 'None', 1, 'Null', 2, 'Share', 3, 'Exclusive', 'Unknown') mode_requested
FROM dba_kgllock w
,    dba_kgllock h
,    gv$session w1
,    gv$session h1
WHERE (((h.kgllkmod != 0)
AND (h.kgllkmod     != 1)
AND ((h.kgllkreq     = 0)
OR (h.kgllkreq       = 1)))
AND (((w.kgllkmod    = 0)
OR (w.kgllkmod       = 1))
AND ((w.kgllkreq    != 0)
AND (w.kgllkreq     != 1))))
AND w.kgllktype      = h.kgllktype
AND w.kgllkhdl       = h.kgllkhdl
AND w.kgllkuse       = w1.saddr
AND H.KGLLKUSE       = H1.SADDR;

--Cursor for Blocked or Holding Session...
CURSOR cu_blocked_session(cp_blk_session IN VARCHAR2) IS
SELECT DISTINCT KGLNAOBJ
FROM X$KGLLK
WHERE kgllkuse IN
(SELECT saddr FROM gv$session 
 WHERE sid = cp_blk_session);

--Ver 5.1 End 

      l_sec          VARCHAR2 (100);
      l_count        NUMBER;  
	  lv_object_name VARCHAR2(100); --Added for Ver 5.1
	  lv_timestamp   VARCHAR2(40);  --Added for Ver 5.1
	  ln_request_id  NUMBER:=fnd_global.user_id;  --Added for Ver 5.1
	  ln_user_id     NUMBER:=fnd_global.conc_request_id;  --Added for Ver 5.1
			 
	  --3.1
   BEGIN
      
      -- 3.1 Start
      IF is_object_exists ('XXWC_MD_SEARCH_PRODUCTS_MV', 'MATERIALIZED VIEW') >
            0
      THEN
         --ver 3.1
         l_sec := 'Droping APPS.XXWC_MD_SEARCH_PRODUCTS_MV';
         fnd_file.put_line (
            fnd_file.LOG,
            l_sec || ' - ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));

	     EXECUTE IMMEDIATE 'ALTER SESSION SET DDL_LOCK_TIMEOUT=900'; -- Added in Rev 6.1			
         EXECUTE IMMEDIATE
            'drop MATERIALIZED VIEW APPS.XXWC_MD_SEARCH_PRODUCTS_MV';
	     EXECUTE IMMEDIATE 'ALTER SESSION SET DDL_LOCK_TIMEOUT=0'; -- Added in Rev 6.1						
         l_sec := 'XXWC_MD_SEARCH_PRODUCTS_MV drop completed.'; -- Added in Rev 6.1						
         fnd_file.put_line (
            fnd_file.LOG,
            l_sec || ' - ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS')); -- Added in Rev 6.1						
 
      END IF;

      -- 3.1 end
 --ver 4.1 start
      IF is_object_exists ('XXWC_MD_SEARCH_PRODUCTS_MV', 'MATERIALIZED VIEW') =
            0 and is_object_exists ('XXWC_MD_SEARCH_PRODUCTS_MV', 'TABLE') = 1 then
      EXECUTE IMMEDIATE
            'drop table APPS.XXWC_MD_SEARCH_PRODUCTS_MV';
      end if;
      --ver 4.1 end
      IF is_object_exists ('XXWC_MD_SEARCH_PRODUCTS_MV', 'MATERIALIZED VIEW') =
            0 and is_object_exists ('XXWC_MD_SEARCH_PRODUCTS_MV', 'TABLE') =  0 --4.1

      THEN
         --ver 3.1
         --3.1

         l_sec := 'Create APPS.XXWC_MD_SEARCH_PRODUCTS_MV';
         fnd_file.put_line (
            fnd_file.LOG,
            l_sec || ' - ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));

         --2.0 Start
         EXECUTE IMMEDIATE
            '   CREATE MATERIALIZED VIEW APPS.XXWC_MD_SEARCH_PRODUCTS_MV REFRESH COMPLETE ON DEMAND

            AS
              select inventory_item_id,
                     organization_id,
                     partnumber,
                     type,
                     manufacturerpartnumber,
                     manufacturer,
                     sequence,
                     currencycode,
                     name,
                     shortdescription,
                     longdescription,
                     thumbnail,
                     fullimage,
                     quantitymeasure,
                     weightmeasure,
                     weight,
                     buyable,
                     keyword,
                     creation_date,
                     item_type,
                     cross_reference,
                     dummy,
                     primary_uom_code,
                       dummy2, partnumber2
                from APPS.XXWC_MD_SEARCH_PRODUCTS_VW';
      -- 2.0 end
      --Start comment for ver 3.1
      /*l_sec := 'Create index APPS.XXWC_MD_SEARCH_MV_N1';
        fnd_file.put_line(fnd_file.LOG,
                          l_sec || ' - ' ||
                          TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));
        EXECUTE IMMEDIATE 'create index APPS.XXWC_MD_SEARCH_MV_N1 on XXWC_MD_SEARCH_PRODUCTS_MV(partnumber) indextype is ctxsys.context parameters(''
                                  wordlist XXWC_MD_PRODUCT_SEARCH_PREF '')';

        l_sec := 'Create index APPS.XXWC_MD_SEARCH_MV_N2';
        fnd_file.put_line(fnd_file.LOG,
                          l_sec || ' - ' ||
                          TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));
        EXECUTE IMMEDIATE 'create index APPS.XXWC_MD_SEARCH_MV_N2 on XXWC_MD_SEARCH_PRODUCTS_MV(shortdescription) indextype is ctxsys.context parameters(''
                                  wordlist XXWC_MD_PRODUCT_SEARCH_PREF '')';

        l_sec := 'Create index APPS.XXWC_MD_SEARCH_MV_N3';
        fnd_file.put_line(fnd_file.LOG,
                          l_sec || ' - ' ||
                          TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));
        EXECUTE IMMEDIATE 'create index APPS.XXWC_MD_SEARCH_MV_N3 on XXWC_MD_SEARCH_PRODUCTS_MV(cross_reference) indextype is ctxsys.context parameters(''
                                  wordlist XXWC_MD_PRODUCT_SEARCH_PREF '')';

        l_sec := 'Create index APPS.XXWC_MD_SEARCH_MV_N6';
        fnd_file.put_line(fnd_file.LOG,
                          l_sec || ' - ' ||
                          TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));
        EXECUTE IMMEDIATE 'create index APPS.XXWC_MD_SEARCH_MV_N6 on XXWC_MD_SEARCH_PRODUCTS_MV (inventory_item_id)'; --3.1

        l_sec := 'Create index APPS.XXWC_MD_SEARCH_MV_N5';
        fnd_file.put_line(fnd_file.LOG,
                          l_sec || ' - ' ||
                          TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));
        EXECUTE IMMEDIATE 'create index APPS.XXWC_MD_SEARCH_MV_N5 on XXWC_MD_SEARCH_PRODUCTS_MV(dummy) indextype is ctxsys.context parameters(''
                                  DATASTORE XXWC_MD_PRODUCT_STORE_PREF lexer
                                  XXWC_MD_PRODUCT_STORE_LEX1 section group
                                  XXWC_MD_PRODUCT_STORE_SG '')';
        --2.0 start
        l_sec := 'Create index APPS.XXWC_MD_SEARCH_MV_N7';
        fnd_file.put_line(fnd_file.LOG,
                          l_sec || ' - ' ||
                          TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));
        EXECUTE IMMEDIATE 'CREATE INDEX "APPS"."XXWC_MD_SEARCH_MV_N7" ON "APPS"."XXWC_MD_SEARCH_PRODUCTS_MV" ("PARTNUMBER2")
      INDEXTYPE IS "CTXSYS"."CONTEXT"  PARAMETERS (''wordlist XXWC_MD_PRODUCT_SEARCH_PREF'')';

        l_sec := 'Create index APPS.XXWC_MD_SEARCH_MV_N8';
        fnd_file.put_line(fnd_file.LOG,
                          l_sec || ' - ' ||
                          TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));
        EXECUTE IMMEDIATE 'CREATE INDEX APPS.XXWC_MD_SEARCH_MV_N8 ON XXWC_MD_SEARCH_PRODUCTS_MV (DUMMY2) INDEXTYPE IS CTXSYS.CONTEXT PARAMETERS (''
                                DATASTORE XXWC_MD_PRODUCT_STORE_PREF1 lexer
                                XXWC_MD_PRODUCT_STORE_LEX2 section group
                                XXWC_MD_PRODUCT_STORE_SG1'')'; -- 2.0 end
        fnd_file.put_line(fnd_file.LOG,
                          'End REFRESH_AIS_MV ' || ' - ' ||
                          TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));*/
      --End comment for ver 3.1
      END IF;                                                           -- 3.1
   EXCEPTION
   WHEN OTHERS THEN
	  
	  --Ver 5.1 Start
	  IF INSTR(SQLERRM,'ORA-04021')>0 THEN

	    SELECT systimestamp
		INTO  lv_timestamp FROM dual;
		
		fnd_file.put_line (fnd_file.LOG, 'Session Start Time '||lv_timestamp);
	    
	    BEGIN
		
		  FOR rec_wait_session IN cu_wait_session LOOP
		
		    BEGIN
			
			  SELECT to_name 
			  INTO   lv_object_name
              FROM   V$OBJECT_DEPENDENCY 
              WHERE  to_address = rec_wait_session.address;
			
			EXCEPTION
			WHEN others THEN
			
			  lv_object_name:=NULL;
			
			END;
		
		    fnd_file.put_line (fnd_file.LOG, 'ORA-04021 timeout occurred while waiting to lock object '||lv_object_name);
			
			INSERT INTO XXWC.XXWC_AIS_SEARCH_LOG_TBL
			(SEGMENT1                
            ,CREATED_BY              
            ,CREATION_DATE           
            ,LAST_UPDATE_DATE       
            ,MESSAGE 
            ) 
            VALUES
            (ln_request_id
			,ln_user_id
			,SYSDATE
			,SYSDATE
			,'ORA-04021 timeout occurred while waiting to lock object '||lv_object_name
			);			

			--Cursor for blocked session...
			FOR rec_blocked_session IN cu_blocked_session(rec_wait_session.blocked_session) LOOP
			
              fnd_file.put_line (fnd_file.LOG, 'Blocked Objects details '||rec_blocked_session.KGLNAOBJ);
			  
			  INSERT INTO XXWC.XXWC_AIS_SEARCH_LOG_TBL
			  (SEGMENT1                
              ,CREATED_BY              
              ,CREATION_DATE           
              ,LAST_UPDATE_DATE       
              ,MESSAGE 
              ) 
              VALUES
              (ln_request_id
			  ,ln_user_id
			  ,SYSDATE
			  ,SYSDATE
			  ,'Blocked Objects details '||rec_blocked_session.KGLNAOBJ
			  );
		
		    END LOOP;
			
			--Cursor for blocking or holding session...
			FOR rec_blocked_session IN cu_blocked_session(rec_wait_session.holding_session) LOOP
			
              fnd_file.put_line (fnd_file.LOG, 'Blocking Objects details '||rec_blocked_session.KGLNAOBJ);
			  
			  INSERT INTO XXWC.XXWC_AIS_SEARCH_LOG_TBL
			  (SEGMENT1                
              ,CREATED_BY              
              ,CREATION_DATE           
              ,LAST_UPDATE_DATE       
              ,MESSAGE 
              ) 
              VALUES
              (ln_request_id
			  ,ln_user_id
			  ,SYSDATE
			  ,SYSDATE
			  ,'Blocking Objects details '||rec_blocked_session.KGLNAOBJ
			  );
		
		    END LOOP;
		   
		  END LOOP;
		
		END;    

        COMMIT;		
	 
	 	SELECT systimestamp
		INTO  lv_timestamp FROM dual;
		
		fnd_file.put_line (fnd_file.LOG, 'Session End Time '||lv_timestamp);
	 
	 END IF;
	 --Ver 5.1 End
	 
         p_err_buf := SQLERRM || l_sec;
         p_return_code := 2;
		 
   END refresh_ais_mv;

   /*************************************************************************
     Procedure : create_index_for_ais_mv

     PURPOSE:   This procedure is to create indexes on  XXWC_MD_SEARCH_PRODUCTS_MV

     REVISIONS:
     Ver        Date        Author                  Description
     ---------  ----------  ---------------         -------------------------
     3.1       02/13/2016  Niraj K Ranjan          TMS 20170202-00084 AIS MV refresh process - Resolve Invalid objects issue
   ************************************************************************/
   PROCEDURE create_index_for_ais_mv (p_err_buf       OUT VARCHAR2,
                                      p_return_code   OUT NUMBER)
   IS
      l_sec     VARCHAR2 (100);
      l_count   NUMBER;
   BEGIN
      IF is_object_exists ('XXWC_MD_SEARCH_PRODUCTS_MV', 'MATERIALIZED VIEW') >
            0
      THEN
         IF is_object_exists ('XXWC_MD_SEARCH_MV_N1', 'INDEX') > 0
         THEN
            l_sec := 'Droping APPS.XXWC_MD_SEARCH_MV_N1';
            fnd_file.put_line (
               fnd_file.LOG,
               l_sec || ' - ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));

            EXECUTE IMMEDIATE 'DROP INDEX APPS.XXWC_MD_SEARCH_MV_N1';
         END IF;

         IF is_object_exists ('XXWC_MD_SEARCH_MV_N1', 'INDEX') = 0
         THEN
            l_sec := 'Create index APPS.XXWC_MD_SEARCH_MV_N1';
            fnd_file.put_line (
               fnd_file.LOG,
               l_sec || ' - ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));

            EXECUTE IMMEDIATE
               'create index APPS.XXWC_MD_SEARCH_MV_N1 on XXWC_MD_SEARCH_PRODUCTS_MV(partnumber) indextype is ctxsys.context parameters(''
                                    wordlist XXWC_MD_PRODUCT_SEARCH_PREF '')';
         END IF;

         IF is_object_exists ('XXWC_MD_SEARCH_MV_N2', 'INDEX') > 0
         THEN
            l_sec := 'Droping APPS.XXWC_MD_SEARCH_MV_N2';
            fnd_file.put_line (
               fnd_file.LOG,
               l_sec || ' - ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));

            EXECUTE IMMEDIATE 'DROP INDEX APPS.XXWC_MD_SEARCH_MV_N2';
         END IF;

         IF is_object_exists ('XXWC_MD_SEARCH_MV_N2', 'INDEX') = 0
         THEN
            l_sec := 'Create index APPS.XXWC_MD_SEARCH_MV_N2';
            fnd_file.put_line (
               fnd_file.LOG,
               l_sec || ' - ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));

            EXECUTE IMMEDIATE
               'create index APPS.XXWC_MD_SEARCH_MV_N2 on XXWC_MD_SEARCH_PRODUCTS_MV(shortdescription) indextype is ctxsys.context parameters(''
                                 wordlist XXWC_MD_PRODUCT_SEARCH_PREF '')';
         END IF;

         IF is_object_exists ('XXWC_MD_SEARCH_MV_N3', 'INDEX') > 0
         THEN
            l_sec := 'Droping APPS.XXWC_MD_SEARCH_MV_N3';
            fnd_file.put_line (
               fnd_file.LOG,
               l_sec || ' - ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));

            EXECUTE IMMEDIATE 'DROP INDEX APPS.XXWC_MD_SEARCH_MV_N3';
         END IF;

         IF is_object_exists ('XXWC_MD_SEARCH_MV_N3', 'INDEX') = 0
         THEN
            l_sec := 'Create index APPS.XXWC_MD_SEARCH_MV_N3';
            fnd_file.put_line (
               fnd_file.LOG,
               l_sec || ' - ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));

            EXECUTE IMMEDIATE
               'create index APPS.XXWC_MD_SEARCH_MV_N3 on XXWC_MD_SEARCH_PRODUCTS_MV(cross_reference) indextype is ctxsys.context parameters(''
                                    wordlist XXWC_MD_PRODUCT_SEARCH_PREF '')';
         END IF;

         IF is_object_exists ('XXWC_MD_SEARCH_MV_N6', 'INDEX') > 0
         THEN
            l_sec := 'Droping APPS.XXWC_MD_SEARCH_MV_N6';
            fnd_file.put_line (
               fnd_file.LOG,
               l_sec || ' - ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));

            EXECUTE IMMEDIATE 'DROP INDEX APPS.XXWC_MD_SEARCH_MV_N6';
         END IF;

         IF is_object_exists ('XXWC_MD_SEARCH_MV_N6', 'INDEX') = 0
         THEN
            l_sec := 'Create index APPS.XXWC_MD_SEARCH_MV_N6';
            fnd_file.put_line (
               fnd_file.LOG,
               l_sec || ' - ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));

            EXECUTE IMMEDIATE
               'create index APPS.XXWC_MD_SEARCH_MV_N6 on XXWC_MD_SEARCH_PRODUCTS_MV (inventory_item_id)';
         END IF;

         IF is_object_exists ('XXWC_MD_SEARCH_MV_N5', 'INDEX') > 0
         THEN
            l_sec := 'Droping APPS.XXWC_MD_SEARCH_MV_N5';
            fnd_file.put_line (
               fnd_file.LOG,
               l_sec || ' - ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));

            EXECUTE IMMEDIATE 'DROP INDEX APPS.XXWC_MD_SEARCH_MV_N5';
         END IF;

         IF is_object_exists ('XXWC_MD_SEARCH_MV_N5', 'INDEX') = 0
         THEN
            l_sec := 'Create index APPS.XXWC_MD_SEARCH_MV_N5';
            fnd_file.put_line (
               fnd_file.LOG,
               l_sec || ' - ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));

            EXECUTE IMMEDIATE
               'create index APPS.XXWC_MD_SEARCH_MV_N5 on XXWC_MD_SEARCH_PRODUCTS_MV(dummy) indextype is ctxsys.context parameters(''
                                    DATASTORE XXWC_MD_PRODUCT_STORE_PREF lexer
                                    XXWC_MD_PRODUCT_STORE_LEX1 section group
                                    XXWC_MD_PRODUCT_STORE_SG '')';
         END IF;

         IF is_object_exists ('XXWC_MD_SEARCH_MV_N7', 'INDEX') > 0
         THEN
            l_sec := 'Droping APPS.XXWC_MD_SEARCH_MV_N7';
            fnd_file.put_line (
               fnd_file.LOG,
               l_sec || ' - ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));

            EXECUTE IMMEDIATE 'DROP INDEX APPS.XXWC_MD_SEARCH_MV_N7';
         END IF;

         IF is_object_exists ('XXWC_MD_SEARCH_MV_N7', 'INDEX') = 0
         THEN
            --2.0 start
            l_sec := 'Create index APPS.XXWC_MD_SEARCH_MV_N7';
            fnd_file.put_line (
               fnd_file.LOG,
               l_sec || ' - ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));

            EXECUTE IMMEDIATE
               'CREATE INDEX "APPS"."XXWC_MD_SEARCH_MV_N7" ON "APPS"."XXWC_MD_SEARCH_PRODUCTS_MV" ("PARTNUMBER2")
          INDEXTYPE IS "CTXSYS"."CONTEXT"  PARAMETERS (''wordlist XXWC_MD_PRODUCT_SEARCH_PREF'')';
         END IF;

         IF is_object_exists ('XXWC_MD_SEARCH_MV_N8', 'INDEX') > 0
         THEN
            l_sec := 'Droping APPS.XXWC_MD_SEARCH_MV_N8';
            fnd_file.put_line (
               fnd_file.LOG,
               l_sec || ' - ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));

            EXECUTE IMMEDIATE 'DROP INDEX APPS.XXWC_MD_SEARCH_MV_N8';
         END IF;

         IF is_object_exists ('XXWC_MD_SEARCH_MV_N8', 'INDEX') = 0
         THEN
            l_sec := 'Create index APPS.XXWC_MD_SEARCH_MV_N8';
            fnd_file.put_line (
               fnd_file.LOG,
               l_sec || ' - ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));

            EXECUTE IMMEDIATE
               'CREATE INDEX APPS.XXWC_MD_SEARCH_MV_N8 ON XXWC_MD_SEARCH_PRODUCTS_MV (DUMMY2) INDEXTYPE IS CTXSYS.CONTEXT PARAMETERS (''
                                  DATASTORE XXWC_MD_PRODUCT_STORE_PREF1 lexer
                                  XXWC_MD_PRODUCT_STORE_LEX2 section group
                                  XXWC_MD_PRODUCT_STORE_SG1'')'; -- 2.0 end
         END IF;

         fnd_file.put_line (
            fnd_file.LOG,
               'End CREATE_INDEX_FOR_AIS_MV '
            || ' - '
            || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         p_err_buf := SQLERRM || l_sec;
         p_return_code := 2;
   END create_index_for_ais_mv;

   /*************************************************************************
     Procedure : create_syn_for_ais_mv

     PURPOSE:   This procedure is create synonym XXWC_MD_SEARCH_PRODUCTS_MV_S pointing to XXWC_MD_SEARCH_PRODUCTS_MV

     REVISIONS:
     Ver        Date        Author                  Description
     ---------  ----------  ---------------         -------------------------
     1.0        06/28/2016  Gopi Damuluri           Initial Version TMS# 20160628-00001
     3.1       02/13/2016  Niraj K Ranjan          TMS 20170202-00084 AIS MV refresh process - Resolve Invalid objects issue
   ************************************************************************/
   PROCEDURE create_syn_for_ais_mv (p_err_buf       OUT VARCHAR2,
                                    p_return_code   OUT NUMBER)
   IS
      l_sec          VARCHAR2 (100);
      l_mv_rec_cnt   NUMBER;
      l_stmt         VARCHAR2 (500);
   BEGIN
      BEGIN
         --start ver 3.1
         l_stmt := 'SELECT COUNT(1) FROM APPS.XXWC_MD_SEARCH_PRODUCTS_MV';

         EXECUTE IMMEDIATE l_stmt INTO l_mv_rec_cnt;
      --end ver 3.1
      EXCEPTION
         WHEN OTHERS
         THEN
            l_mv_rec_cnt := 0;
      END;

      l_sec := 'l_mv_rec_cnt - ' || l_mv_rec_cnt;
      fnd_file.put_line (
         fnd_file.LOG,
         l_sec || ' - ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));

      IF l_mv_rec_cnt > 0
      THEN
         l_sec :=
            'Crate Synonym APPS.XXWC_MD_SEARCH_PRODUCTS_MV_S for XXWC_MD_SEARCH_PRODUCTS_MV';
         fnd_file.put_line (
            fnd_file.LOG,
            l_sec || ' - ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));

         EXECUTE IMMEDIATE
            'CREATE OR REPLACE SYNONYM APPS.XXWC_MD_SEARCH_PRODUCTS_MV_S FOR APPS.XXWC_MD_SEARCH_PRODUCTS_MV';
      ELSE
         create_syn_for_ais_bkp (p_err_buf, p_return_code);
      END IF;

      fnd_file.put_line (
         fnd_file.LOG,
            'End CREATE_SYN_FOR_AIS_MV '
         || ' - '
         || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));
   EXCEPTION
      WHEN OTHERS
      THEN
         p_err_buf := SQLERRM || l_sec;
         p_return_code := 2;
   END create_syn_for_ais_mv;

   /*************************************************************************
    Procedure : backup_ais_mv

    PURPOSE:   This procedure is to refresh AIS XXWC_MD_SEARCH_PRODUCTS_MV2 and its Indexes

    REVISIONS:
    Ver        Date        Author                  Description
    ---------  ----------  ---------------         -------------------------
    1.0        06/28/2016  Gopi Damuluri           Initial Version TMS# 20160628-00001
    2.0       11/21/2016   Nancy Pahwa             TMS 20161121-00176
    3.0       11/22/2016   Nancy Pahwa             TMS 20161121-00176 Index 8 to use pref1 under XXWC schema

   ************************************************************************/
   PROCEDURE backup_ais_mv (p_err_buf OUT VARCHAR2, p_return_code OUT NUMBER)
   IS
      l_sec   VARCHAR2 (100);
   BEGIN
      BEGIN
         l_sec := 'Droping table XXWC.XXWC_MD_SEARCH_PRODUCTS_TBL';
         fnd_file.put_line (
            fnd_file.LOG,
            l_sec || ' - ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));

         EXECUTE IMMEDIATE 'DROP TABLE XXWC.XXWC_MD_SEARCH_PRODUCTS_TBL';
      EXCEPTION
         WHEN OTHERS
         THEN
            fnd_file.put_line (fnd_file.LOG, SQLERRM);
      END;

      l_sec := 'Create Table XXWC.XXWC_MD_SEARCH_PRODUCTS_TBL';
      fnd_file.put_line (
         fnd_file.LOG,
         l_sec || ' - ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));

      EXECUTE IMMEDIATE 'CREATE TABLE XXWC.XXWC_MD_SEARCH_PRODUCTS_TBL
            AS select *
                from APPS.XXWC_MD_SEARCH_PRODUCTS_MV';

      l_sec := 'Create index XXWC.XXWC_MD_SEARCH_PRODUCT_N1';
      fnd_file.put_line (
         fnd_file.LOG,
         l_sec || ' - ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));

      EXECUTE IMMEDIATE
         'create index XXWC.XXWC_MD_SEARCH_PRODUCT_N1 on XXWC.XXWC_MD_SEARCH_PRODUCTS_TBL(partnumber) indextype is ctxsys.context parameters(''
                                wordlist XXWC.XXWC_MD_PRODUCT_SEARCH_PREF '')';

      l_sec := 'Create index XXWC.XXWC_MD_SEARCH_PRODUCT_N2';
      fnd_file.put_line (
         fnd_file.LOG,
         l_sec || ' - ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));

      EXECUTE IMMEDIATE
         'create index XXWC.XXWC_MD_SEARCH_PRODUCT_N2 on XXWC.XXWC_MD_SEARCH_PRODUCTS_TBL(shortdescription) indextype is ctxsys.context parameters(''
                                wordlist XXWC.XXWC_MD_PRODUCT_SEARCH_PREF '')';

      l_sec := 'Create index XXWC.XXWC_MD_SEARCH_PRODUCT_N3';
      fnd_file.put_line (
         fnd_file.LOG,
         l_sec || ' - ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));

      EXECUTE IMMEDIATE
         'create index XXWC.XXWC_MD_SEARCH_PRODUCT_N3 on XXWC.XXWC_MD_SEARCH_PRODUCTS_TBL(cross_reference) indextype is ctxsys.context parameters(''
                                wordlist XXWC.XXWC_MD_PRODUCT_SEARCH_PREF '')';

      l_sec := 'Create index XXWC.XXWC_MD_SEARCH_PRODUCT_N6';
      fnd_file.put_line (
         fnd_file.LOG,
         l_sec || ' - ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));

      EXECUTE IMMEDIATE
         'create index XXWC.XXWC_MD_SEARCH_PRODUCT_N6 on XXWC.XXWC_MD_SEARCH_PRODUCTS_TBL (inventory_item_id)';

      l_sec := 'Create index XXWC.XXWC_MD_SEARCH_PRODUCT_N5';
      fnd_file.put_line (
         fnd_file.LOG,
         l_sec || ' - ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));

      EXECUTE IMMEDIATE
         'create index XXWC.XXWC_MD_SEARCH_PRODUCT_N5 on XXWC.XXWC_MD_SEARCH_PRODUCTS_TBL(dummy) indextype is ctxsys.context parameters(''
                                DATASTORE XXWC.XXWC_MD_PRODUCT_STORE_PREF lexer
                                XXWC_MD_PRODUCT_STORE_LEX1 section group
                                XXWC_MD_PRODUCT_STORE_SG '')';

      --2.0 start
      l_sec := 'Create index XXWC.XXWC_MD_SEARCH_PRODUCT_N7';
      fnd_file.put_line (
         fnd_file.LOG,
         l_sec || ' - ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));

      EXECUTE IMMEDIATE
         'create index XXWC.XXWC_MD_SEARCH_PRODUCT_N7 on XXWC.XXWC_MD_SEARCH_PRODUCTS_TBL(partnumber2) indextype is ctxsys.context parameters(''
                                wordlist XXWC.XXWC_MD_PRODUCT_SEARCH_PREF '')';

      l_sec := 'Create index XXWC.XXWC_MD_SEARCH_PRODUCT_N8';
      fnd_file.put_line (
         fnd_file.LOG,
         l_sec || ' - ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));

      EXECUTE IMMEDIATE
         'CREATE INDEX XXWC.XXWC_MD_SEARCH_PRODUCT_N8 ON XXWC.XXWC_MD_SEARCH_PRODUCTS_TBL(DUMMY2) INDEXTYPE IS CTXSYS.CONTEXT PARAMETERS (''
                              DATASTORE XXWC.XXWC_MD_PRODUCT_STORE_PREF1 lexer
                              XXWC_MD_PRODUCT_STORE_LEX2 section group
                              XXWC_MD_PRODUCT_STORE_SG1'')';

      -- 2.0 end
      fnd_file.put_line (
         fnd_file.LOG,
            'End BACKUP_AIS_MV '
         || ' - '
         || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));
   EXCEPTION
      WHEN OTHERS
      THEN
         p_err_buf := SQLERRM || l_sec;
         p_return_code := 2;
   END backup_ais_mv;

   /*************************************************************************
     Procedure : create_syn_for_ais_bkp

     PURPOSE:   This procedure is create synonym XXWC_MD_SEARCH_PRODUCTS_MV_S pointing to XXWC_MD_SEARCH_PRODUCTS_MV2

     REVISIONS:
     Ver        Date        Author                  Description
     ---------  ----------  ---------------         -------------------------
     1.0        06/28/2016  Gopi Damuluri           Initial Version TMS# 20160628-00001
     3.1       02/13/2016  Niraj K Ranjan          TMS 20170202-00084 AIS MV refresh process - Resolve Invalid objects issue
   ************************************************************************/
   PROCEDURE create_syn_for_ais_bkp (p_err_buf       OUT VARCHAR2,
                                     p_return_code   OUT NUMBER)
   IS
      l_sec           VARCHAR2 (100);
      l_bkp_rec_cnt   NUMBER;
      l_sql_stmt      VARCHAR2 (500);
   BEGIN
      BEGIN
         --start ver 3.1
         l_sql_stmt := 'SELECT COUNT(1) FROM XXWC.XXWC_MD_SEARCH_PRODUCTS_TBL';

         EXECUTE IMMEDIATE l_sql_stmt INTO l_bkp_rec_cnt;
      --end ver 3.1
      EXCEPTION
         WHEN OTHERS
         THEN
            l_bkp_rec_cnt := 0;
      END;

      fnd_file.put_line (fnd_file.LOG, 'l_bkp_rec_cnt ' || l_bkp_rec_cnt);

      IF l_bkp_rec_cnt > 0
      THEN
         l_sec :=
            'Crate Synonym APPS.XXWC_MD_SEARCH_PRODUCTS_MV_S for XXWC.XXWC_MD_SEARCH_PRODUCTS_TBL';
         fnd_file.put_line (
            fnd_file.LOG,
            l_sec || ' - ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));

         EXECUTE IMMEDIATE
            'CREATE OR REPLACE SYNONYM APPS.XXWC_MD_SEARCH_PRODUCTS_MV_S FOR XXWC.XXWC_MD_SEARCH_PRODUCTS_TBL';
      END IF;

      fnd_file.put_line (
         fnd_file.LOG,
            'End CREATE_SYN_FOR_AIS_BKP '
         || ' - '
         || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));
   EXCEPTION
      WHEN OTHERS
      THEN
         p_err_buf := SQLERRM || l_sec;
         p_return_code := 2;
   END create_syn_for_ais_bkp;

   /*************************************************************************
     Procedure : main

     PURPOSE:   This procedure is the main procedure for creating synonym XXWC_MD_SEARCH_PRODUCTS_MV_S
                pointing to XXWC_MD_SEARCH_PRODUCTS_MV or XXWC_MD_SEARCH_PRODUCTS_MV2

     REVISIONS:
     Ver        Date        Author                  Description
     ---------  ----------  ---------------         -------------------------
     1.0        06/28/2016  Gopi Damuluri           Initial Version TMS# 20160628-00001
     3.1       02/13/2016  Niraj K Ranjan          TMS 20170202-00084 AIS MV refresh process - Resolve Invalid objects issue
   ************************************************************************/
   PROCEDURE main (p_err_buf           OUT VARCHAR2,
                   p_return_code       OUT NUMBER,
                   p_process_step   IN     VARCHAR2,
                   p_send_alert   IN VARCHAR2 DEFAULT 'Y' --Rev#6.0
                   )
   IS
      l_exception     EXCEPTION;
      l_err_buf       VARCHAR2 (2000);
      l_return_code   NUMBER;
   BEGIN
      fnd_file.put_line (fnd_file.LOG, 'p_process_step - ' || p_process_step);

      IF p_process_step = 'REFRESH_AIS_MV'
      THEN
         refresh_ais_mv (l_err_buf, l_return_code);
      ELSIF p_process_step = 'CREATE_SYN_FOR_AIS_MV'
      THEN
         create_syn_for_ais_mv (l_err_buf, l_return_code);
      ELSIF p_process_step = 'BACKUP_AIS_MV'
      THEN
         backup_ais_mv (l_err_buf, l_return_code);
      ELSIF p_process_step = 'CREATE_SYN_FOR_AIS_BKP'
      THEN
         create_syn_for_ais_bkp (l_err_buf, l_return_code);
      ELSIF p_process_step = 'CREATE_INDEX_FOR_AIS_MV'
      THEN
         --This condition added for ver 3.1
         create_index_for_ais_mv (l_err_buf, l_return_code);
      END IF;
      
      --Rev# 6.0 <Start -- if any error occur and the p_send_alert parameter valve is set to 'N', in this case the 
      --concurrent program will complete in warning, this allow BSA team to add the cocurrent program in OpsGenie in critical alert
      --and the critical alert will not be sent to the team if the p_send_alert is set to 'N'.
      IF p_process_step IN ('REFRESH_AIS_MV', 'CREATE_INDEX_FOR_AIS_MV') AND l_return_code = 2 AND p_send_alert = 'N' 
      THEN 
         p_return_code := 1; 
         p_err_buf     := l_err_buf; 
      ELSIF l_return_code = 2 --Rev# 6.0 >End
      THEN
         RAISE l_exception;
      END IF;

      fnd_file.put_line (
         fnd_file.LOG,
         'End MAIN ' || ' - ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));
   EXCEPTION
      WHEN l_exception
      THEN
         p_err_buf := l_err_buf;
         p_return_code := 2;
      WHEN OTHERS
      THEN
         p_err_buf := SQLERRM;
         p_return_code := 2;
   END main;
END XXWC_MD_SEARCH_REFRESH_PKG;
/