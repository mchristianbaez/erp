CREATE OR REPLACE TRIGGER APPS.XXWC_PO_LINES_ARCH_AIR_TRG
/**************************************************************************
     $Header XXWC_PO_LINES_ARCH_AIR_TRG $
     Module Name:XXWC_PO_LINES_ARCH_AIR_TRG
     PURPOSE:   This Trigger is used to raise the business event to sync
              the cost on the so order line for the drop ship orders.
     REVISIONS:
     Ver        Date         Author                  Description
     ---------  -----------  ---------------         -------------------------
     1.0        25-Oct-2017  Nancy Pahwa              PO Outbound
     **************************************************************************/
AFTER UPDATE ON APPS.PO_LINES_ARCHIVE_ALL
FOR EACH ROW
DECLARE
      l_error_msg   VARCHAR2 (240);
      l_sec         VARCHAR2 (100);
      l_dflt_email  fnd_user.email_address%TYPE  := 'HDSOracleDevelopers@hdsupply.com';
BEGIN
 if :new.cancel_flag = 'Y' then
  XXWC_OM_DMS2_OB_PKG.raise_po_Send_To_DMS2(:new.po_header_id,l_error_msg);
  end if;
  IF l_error_msg IS NOT NULL THEN
       l_sec := 'po_line_id'||:new.po_line_id|| l_error_msg;

       xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_PO_LINES_ARCH_AIR_TRG',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => NULL,
            p_error_desc          => SUBSTR (SQLERRM, 1, 240),
            p_distribution_list   => l_dflt_email,
            p_module              => 'PO');
    END IF;

EXCEPTION
      WHEN OTHERS
      THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_PO_LINES_ARCH_AIR_TRG',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (SQLERRM, 1, 240),
            p_distribution_list   => l_dflt_email,
            p_module              => 'PO');
END;
/