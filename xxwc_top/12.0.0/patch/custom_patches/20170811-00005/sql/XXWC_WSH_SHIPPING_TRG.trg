CREATE OR REPLACE TRIGGER APPS.XXWC_WSH_SHIPPING_TRG
AFTER UPDATE or DELETE on xxwc.xxwc_wsh_shipping_stg
  REFERENCING NEW AS NEW OLD AS OLD
  FOR EACH ROW
  
DECLARE
  /******************************************************************************
   NAME:     XXWC_WSH_SHIPPING_TRG
  
   PURPOSE:  To capture the order lines that get deleted that will allow them to be
             sent to MyLogistics for that delivery
  
  
   REVISIONS:
   Ver        Date        Author           Description
   ---------  -----------  ---------------  ------------------------------------
   1.0        09-Jul-2014  Kathy Poling     Created this trigger.
                                            TMS 20140606-00082
   1.1        04-Nov-2014  Kathy Poling     Added call point for header_id and 
                                            exception on org_id  
   1.2        11-Nov-2014  Kathy Poling     Corrected org_id needs to be old.header_id for 
                                            deleted deliveries TMS 20141112-00097  
   1.3        12-Nov-2014  Kathy Poling     Added change status for cancelled orders
                                            TMS 20141112-00097
   1.4        20-Feb-2015  Gopi Damuluri    TMS# 20150128-00043 XXWC DMS File creation modification
   1.5        10-Oct-2017  Rakesh P.        TMS#20170811-00005 DMS Phase-2.0 Outbound Extract(Standard and Internal orders)
  ******************************************************************************/

  -- Error DEBUG
  l_err_callfrom     VARCHAR2(110) DEFAULT 'XXWC_WSH_SHIPPING_TRG';
  l_err_callpoint    VARCHAR2(110) DEFAULT 'START';
  l_distro_list      VARCHAR2(240) DEFAULT 'hdsoracledevelopers@hdsupply.com';
  l_last_update_date DATE := SYSDATE;
  l_error_msg        VARCHAR2 (4000); --Version 1.5 

  l_org_id NUMBER;

BEGIN
  --Version 1.1
  l_err_callpoint := 'Get org_id record xxwc_om_dms_change_tbl for header_id: ' ||
                     :old.header_id;

  BEGIN
    SELECT org_id
      INTO l_org_id
      FROM oe_order_headers_all
    --WHERE header_id = :new.header_id;   --Version 1.2
     WHERE header_id = :old.header_id; --Version 1.2
  EXCEPTION
    --Version 1.1
    WHEN no_data_found THEN
      l_org_id := NULL;
  END;

  IF deleting
  THEN
  
    l_err_callpoint := 'Insert record xxwc_om_dms_change_tbl for line_id being deleted: ' ||
                       :old.line_id;
  
    INSERT INTO xxwc.xxwc_om_dms_change_tbl
      (header_id
      ,line_id
      ,delivery_id
      ,created_by
      ,creation_date
      ,last_updated_by
      ,last_update_date
      ,change_type
      ,org_id
      ,status) --Version 1.3
    VALUES
      (:old.header_id
      ,:old.line_id
      ,:old.delivery_id
      ,:old.created_by
      ,:old.creation_date
      ,nvl(fnd_global.user_id(), :old.last_updated_by)
      ,l_last_update_date
      ,'D'
      ,l_org_id
      ,:old.status); --Version 1.3
  
    --delete from on demand staging if exists
    DELETE FROM xxwc.xxwc_om_dms_brnch_file_tbl
     WHERE header_id = :old.header_id
       AND delivery_id = :old.delivery_id
       AND creation_date < l_last_update_date;

    --delete from on DMS archive staging if exists -- Version# 1.4
    DELETE FROM xxwc_om_dms_change_archive_tbl
     WHERE header_id = :old.header_id
       AND delivery_id = :old.delivery_id;
  
  END IF;

  IF updating
  THEN
  
    l_err_callpoint := 'Insert record xxwc_om_dms_change_tbl for line_id being updated: ' ||
                       :old.line_id;
    
	--Version 1.5
	IF :new.status = 'CANCELLED' THEN

	   XXWC_OM_DMS2_OB_PKG.raise_so_cancel_line(:old.header_id,:old.line_id,l_error_msg);

	   IF l_error_msg IS NOT NULL THEN
	      raise_application_error (
					 -20000, 'Header_id : '||:old.header_id||', Line_id : '||:old.line_id||' - '|| l_error_msg);
	   END IF;

    END IF;
    --Version 1.5 
	
    INSERT INTO xxwc.xxwc_om_dms_change_tbl
      (header_id
      ,line_id
      ,delivery_id
      ,created_by
      ,creation_date
      ,last_updated_by
      ,last_update_date
      ,change_type
      ,org_id
      ,status) --Version 1.3
    VALUES
      (:new.header_id
      ,:new.line_id
      ,:new.delivery_id
      ,:old.created_by
      ,:old.creation_date
      ,nvl(fnd_global.user_id(), :new.last_updated_by)
      ,l_last_update_date
      ,CASE WHEN :new.status = 'CANCELLED' THEN 'C' ELSE 'U' END
      ,l_org_id
      ,:new.status); --Version 1.3
      
    --delete from on DMS archive staging if exists -- Version# 1.4
    DELETE FROM xxwc_om_dms_change_archive_tbl
     WHERE header_id = :old.header_id
       AND delivery_id = :old.delivery_id;
  
  END IF;

EXCEPTION
  WHEN OTHERS THEN
    xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                        ,p_calling           => l_err_callpoint
                                        ,p_ora_error_msg     => substr(SQLERRM ||
                                                                       regexp_replace(l_err_callpoint
                                                                                     ,'[[:cntrl:]]'
                                                                                     ,NULL)
                                                                      ,1
                                                                      ,2000)
                                        ,p_error_desc        => 'Order header_id=' ||
                                                                :new.header_id ||
                                                                ' Order line_id=' ||
                                                                :old.line_id --Version 1.1
                                        ,p_distribution_list => l_distro_list
                                        ,p_module            => 'OM');
  
END xxwc_wsh_shipping_trg;
/
