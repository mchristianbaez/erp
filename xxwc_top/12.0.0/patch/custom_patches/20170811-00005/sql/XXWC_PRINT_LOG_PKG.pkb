CREATE OR REPLACE PACKAGE BODY  XXWC_PRINT_LOG_PKG AS 
/******************************************************************************
   NAME:       XXWC_PRINT_LOG_PKG

   PURPOSE:    Package to insert concurrent request records into XXWC.XXWC_PRINT_LOG_TBL used for the Print Log Form
   

   REVISIONS:   
   Ver        Date         Author           Description
   ---------  -----------  ---------------  ------------------------------------
   1.0        27-FEB-2013  Lee Spitzer       1. Create the PL/SQL Package
   2.0        20-OCT-2014  Gopi Damuluri     TMS# 20140606-00082 Added a new field SIGNATURE_NAME to Print Log Form
   2.1        20-FEB-2015  Gopi Damuluri     TMS# 20150220-00107 - Move DMS Functionality to APEX on EBS
   3.0        09-Jan-2018  Rakesh Patel      TMS#20170811-00005-DMS Phase-2.0 Outbound Extract(Standard and Internal orders)
******************************************************************************/

  /*************************************************************************
 *   Procedure : Write_Log
 *
 *   PURPOSE:   This procedure logs error message
 *   Parameter:
 *          IN
 *              p_debug_msg      -- Debug Message
 * ************************************************************************/
   -- add debug message to log table and concurrent log file
   PROCEDURE Write_Log (p_debug_level   IN NUMBER,
                        p_mod_name      IN VARCHAR2,
                        p_debug_msg     IN VARCHAR2)
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      --IF (p_debug_level >= FND_LOG.G_CURRENT_RUNTIME_LEVEL)
      --THEN
      -- Call Oracle FND_LOG API to insert the debug message
      FND_LOG.STRING (p_debug_level, p_mod_name, P_DEBUG_MSG);
      -- END IF;
      fnd_file.put_line (fnd_file.LOG, P_DEBUG_MSG);
      --DBMS_OUTPUT.put_line (P_DEBUG_MSG);
      COMMIT;
   END Write_Log;

  /*************************************************************************
 *   Procedure : Write_Error
 *
 *   PURPOSE:   This procedure logs error message
 *   Parameter:
 *          IN
 *              p_debug_msg      -- Debug Message
 * ************************************************************************/

   --add message to concurrent output file
   PROCEDURE Write_Error (p_debug_msg IN VARCHAR2)
   IS
      l_req_id          NUMBER := fnd_global.conc_request_id;
      l_err_callfrom    VARCHAR2 (75) DEFAULT G_PACKAGE_NAME ;
      l_err_callpoint   VARCHAR2 (75) DEFAULT 'START' ;
      l_distro_list VARCHAR2 (75)
            DEFAULT 'HDSOracleDevelopers@hdsupply.com'  ;
   BEGIN
      xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => l_err_callfrom,
         p_calling             => l_err_callpoint,
         p_request_id          => l_req_id,
         p_ora_error_msg       => SUBSTR (p_debug_msg, 1, 2000),
         p_error_desc          => 'Error running '|| G_PACKAGE_NAME ||' with PROGRAM ERROR',
         p_distribution_list   => l_distro_list,
         p_module              => 'INV'
      );
   END Write_Error;

   /*************************************************************************
    *   Procedure : Write_output
    *
    *   PURPOSE:   This procedure logs message into concurrent output
    *   Parameter:
    *          IN
    *              p_debug_msg      -- Debug Message
    * ************************************************************************/
   PROCEDURE Write_output (p_debug_msg IN VARCHAR2)
   IS
   BEGIN
      fnd_file.put_line (fnd_file.output, p_debug_msg);
   END Write_output;

   /*************************************************************************
    *   Procedure : Write_log_output
    *
    *   PURPOSE:   This procedure logs message into concurrent logfile
    *   Parameter:
    *          IN
    *              p_debug_msg      -- Debug Message
    * ************************************************************************/

   PROCEDURE Write_log_output (p_debug_msg IN VARCHAR2)
   IS
   BEGIN
      fnd_file.put_line (fnd_file.LOG, p_debug_msg);
   END write_log_output;
   
   /*************************************************************************
    *   Procedure : INSERT_CCR
    *
    *   PURPOSE: This procedure inserts Concurrent Request details 
    *   REVISIONS:
    *   Ver        Date         Author           Description
    *   ---------  -----------  ---------------  ------------------------------------
    *   2.0        20-OCT-2014  Gopi Damuluri     TMS# 20140606-00082 Added a new field SIGNATURE_NAME to Print Log Form
    *   2.1        20-FEB-2015  Gopi Damuluri     TMS# 20150220-00107 - Move DMS Functionality to APEX on EBS
	*   3.0        09-Jan-2018  Rakesh Patel      TMS#20170811-00005-DMS Phase-2.0 Outbound Extract(Standard and Internal orders)
    * ************************************************************************/
PROCEDURE  INSERT_CCR
              (retbuf                         OUT VARCHAR2
              ,retcode                        OUT NUMBER
              ) IS


  l_count NUMBER DEFAULT 0;
 
BEGIN

      G_NAME := 'INSERT_CCR';
      G_PROGRAM_NAME := G_PACKAGE_NAME||'.'||G_NAME;
  
      write_log_output('******************************************************************');
      write_log_output('Starting Program '||G_PROGRAM_NAME);
      write_log_output('******************************************************************');


      --Get count of records that are not in custom table but are in FND Concurrent Requests
      
      BEGIN
      
          SELECT count(*)
          INTO   l_count
          FROM   fnd_concurrent_requests fcr,
                 xxwc.xxwc_print_log_ccp_tbl x1
          WHERE  NOT EXISTS (SELECT *
                             FROM   XXWC.XXWC_PRINT_LOG_TBL X
                             WHERE  X.request_id = fcr.request_id)
          AND  fcr.CONCURRENT_PROGRAM_ID = x1.concurrent_program_id
          AND  fcr.program_application_id = x1.application_id;                

      EXCEPTION
      
          WHEN others THEN
          
              write_log_output('When others message getting count and setting count value to 0');
              l_count := 0;
      
      END;
        
    write_log_output('Count of request ids not in XXWC.XXWC_PRINT_LOG_TBL = ' || l_count);
    
    
    BEGIN
      
      INSERT INTO XXWC.XXWC_PRINT_LOG_TBL
        (REQUEST_ID
        ,CONCURRENT_PROGRAM_ID
        ,PROGRAM_APPLICATION_ID
        ,CONCURRENT_PROGRAM_NAME
        ,USER_CONCURRENT_PROGRAM_NAME
        ,PRINTER
        ,PRINTER_DESCRIPTION
        ,REQUESTED_BY
        ,USER_NAME
        ,USER_DESCRIPTION
        ,HEADER_ID
        ,ORGANIZATION_ID
        ,TABLE_NAME
        ,ACTUAL_COMPLETION_DATE
        ,PHASE_CODE
        ,STATUS_CODE
        ,NUMBER_OF_COPIES
        ,PHASE
        ,STATUS
        ,ARGUMENT1
        ,ARGUMENT2
        ,ARGUMENT3
        ,ARGUMENT4
        ,ARGUMENT5
        ,ARGUMENT6
        ,ARGUMENT7
        ,ARGUMENT8
        ,ARGUMENT9
        ,ARGUMENT10
        ,ARGUMENT11
        ,ARGUMENT12
        ,ARGUMENT13
        ,ARGUMENT14
        ,ARGUMENT15
        ,ARGUMENT16
        ,ARGUMENT17
        ,ARGUMENT18
        ,ARGUMENT19
        ,ARGUMENT20
        ,ARGUMENT21
        ,ARGUMENT22
        ,ARGUMENT23
        ,ARGUMENT24
        ,ARGUMENT25
        , SIGNATURE_NAME -- Version# 2.0
		,SCHEDULE_DELIVERY_DATE	 -- Version# 3.0
        )
        select fcr.request_id 
                       ,fcr.concurrent_program_id 
                       ,fcr.program_application_id 
                       ,fcp.concurrent_program_name
                       ,fcpt.user_concurrent_program_name
                       ,fcr.printer
                       ,fpt.description printer_description 
                       ,fcr.requested_by
                       ,fu.user_name
                       ,fu.description user_description
                                ,(case   when x1.header_id = 1 then fcr.argument1
                                when x1.header_id = 2 then fcr.argument2
                                when x1.header_id = 3 then fcr.argument3
                                when x1.header_id = 4 then fcr.argument4
                                when x1.header_id = 5 then fcr.argument5
                                when x1.header_id = 6 then fcr.argument6
                                when x1.header_id = 7 then fcr.argument7
                                when x1.header_id = 8 then fcr.argument8
                                when x1.header_id = 9 then fcr.argument9
                                WHEN x1.header_id = 10 THEN fcr.argument10
                                WHEN x1.header_id = 11 THEN fcr.argument11
                                WHEN x1.header_id = 12 THEN fcr.argument12
                                when x1.header_id = 13 then fcr.argument13
                                WHEN x1.header_id = 14 THEN fcr.argument14
                                WHEN x1.header_id = 15 THEN fcr.argument15
                                WHEN x1.header_id = 16 THEN fcr.argument16
                                WHEN x1.header_id = 17 THEN fcr.argument17
                                WHEN x1.header_id = 18 THEN fcr.argument18
                                WHEN x1.header_id = 19 THEN fcr.argument19
                                WHEN x1.header_id = 20 THEN fcr.argument20
                                WHEN x1.header_id = 21 THEN fcr.argument21
                                WHEN x1.header_id = 22 THEN fcr.argument22
                                WHEN x1.header_id = 23 THEN fcr.argument23
                                WHEN x1.header_id = 24 THEN fcr.argument24
                                when x1.header_id = 25 then fcr.argument25
                          else null
                          end) header_id
                       , (case  when x1.organization_id = 1 then fcr.argument1
                                when x1.organization_id = 2 then fcr.argument2
                                when x1.organization_id = 3 then fcr.argument3
                                when x1.organization_id = 4 then fcr.argument4
                                when x1.organization_id = 5 then fcr.argument5
                                when x1.organization_id = 6 then fcr.argument6
                                when x1.organization_id = 7 then fcr.argument7
                                when x1.organization_id = 8 then fcr.argument8
                                when x1.organization_id = 9 then fcr.argument9
                                WHEN x1.organization_id = 10 THEN fcr.argument10
                                WHEN x1.organization_id = 11 THEN fcr.argument11
                                WHEN x1.organization_id = 12 THEN fcr.argument12
                                WHEN x1.organization_id = 13 THEN fcr.argument13
                                WHEN x1.organization_id = 14 THEN fcr.argument14
                                when x1.organization_id = 15 then fcr.argument15
                                WHEN x1.organization_id = 16 THEN fcr.argument16
                                WHEN x1.organization_id = 17 THEN fcr.argument17
                                WHEN x1.organization_id = 18 THEN fcr.argument18
                                WHEN x1.organization_id = 19 THEN fcr.argument19
                                WHEN x1.organization_id = 20 THEN fcr.argument20
                                WHEN x1.organization_id = 21 THEN fcr.argument21
                                WHEN x1.organization_id = 22 THEN fcr.argument22
                                WHEN x1.organization_id = 23 THEN fcr.argument23
                                WHEN x1.organization_id = 24 THEN fcr.argument24
                                when x1.organization_id = 25 then fcr.argument25
                          else null
                          END) organization_id,
                       x1.TABLE_NAME,
                       fcr.actual_completion_date,
                       fcr.phase_code,
                       fcr.status_code,
                       fcr.number_of_copies,
                       trim(ccr_phase.meaning) phase,
                       trim(ccr_status.meaning) status,
                       fcr.ARGUMENT1, 
                       fcr.ARGUMENT2,
                       fcr.ARGUMENT3, 
                       fcr.ARGUMENT4, 
                       fcr.ARGUMENT5, 
                       fcr.ARGUMENT6, 
                       fcr.ARGUMENT7, 
                       fcr.ARGUMENT8, 
                       fcr.ARGUMENT9,
                       fcr.ARGUMENT10, 
                       fcr.ARGUMENT11, 
                       fcr.ARGUMENT12, 
                       fcr.ARGUMENT13, 
                       fcr.ARGUMENT14, 
                       fcr.ARGUMENT15, 
                       fcr.ARGUMENT16, 
                       fcr.ARGUMENT17, 
                       fcr.ARGUMENT18, 
                       fcr.ARGUMENT19, 
                       fcr.ARGUMENT20, 
                       fcr.ARGUMENT21, 
                       fcr.ARGUMENT22, 
                       fcr.ARGUMENT23, 
                       fcr.ARGUMENT24, 
                       fcr.ARGUMENT25
               , (SELECT signature_name
                    FROM xxwc.xxwc_signature_capture_tbl
                   WHERE request_id = fcr.request_id
                     AND NVL(signature_name,'*&*^&*') != '***CANCEL***'
                     AND rownum = 1) SIGNATURE_NAME --  TMS# 20140606-00082
			  ,	(SELECT SCHEDULE_DELIVERY_DATE
                    FROM XXWC.XXWC_DMS2_LOG_TBL
                   WHERE RPT_CON_REQUEST_ID = fcr.request_id
                     AND rownum = 1) SCHEDULE_DELIVERY_DATE -- Version# 3.0	 
                from   fnd_concurrent_requests fcr,
                       fnd_printer_tl fpt,
                       fnd_user fu,
                       fnd_concurrent_programs fcp,
                       fnd_concurrent_programs_tl fcpt,
                       xxwc.xxwc_print_log_ccp_tbl x1,
                       fnd_lookup_values ccr_phase,
                       fnd_lookup_values ccr_status
                where  fcr.CONCURRENT_PROGRAM_ID = x1.concurrent_program_id
                and   fcr.program_application_id = x1.application_id
                and   fcr.printer = fpt.PRINTER_NAME
                and   fpt.LANGUAGE = 'US'
                and   fcpt.LANGUAGE = 'US'
                and   fu.user_id = fcr.REQUESTED_BY 
                and   fcr.CONCURRENT_PROGRAM_ID = fcp.CONCURRENT_PROGRAM_ID
                and   fcr.PROGRAM_APPLICATION_ID = fcp.APPLICATION_ID
                and   fcpt.APPLICATION_ID = fcp.application_id
                and   fcpt.CONCURRENT_PROGRAM_ID = fcp.CONCURRENT_PROGRAM_ID
                and   ccr_phase.view_application_id = 0 --FND
                and   ccr_status.view_application_id = 0 --FND
                and   ccr_phase.lookup_type = 'CP_PHASE_CODE'
                and   ccr_status.lookup_type = 'CP_STATUS_CODE'
                and   ccr_phase.lookup_code = fcr.PHASE_CODE
                AND   ccr_status.lookup_code = fcr.STATUS_CODE
                AND   NOT EXISTS (SELECT *
                                  FROM   XXWC.XXWC_PRINT_LOG_TBL X
                                  WHERE  X.request_id = fcr.request_id);
    
    
        
        EXCEPTION
        
            WHEN OTHERS THEN
                  ROLLBACK;
                  g_message := 'WHEN OTHERS ' || SQLCODE || SQLERRM;
                  raise g_exception;
        
        END;
     
     write_log_output('Inserted '|| SQL%ROWCOUNT ||' into XXWC.XXWC_PRINT_LOG_TBL');
        
    -- Version# 2.1 > Start
    -----------------------------------------------------------------------
    -- Archiving XXWC_SIGNATURE_CAPTURE_TBL
    -----------------------------------------------------------------------
    BEGIN
    INSERT INTO xxwc.xxwc_signature_capture_arc_tbl arc
    SELECT * FROM xxwc.xxwc_signature_capture_tbl stg
    WHERE NOT EXISTS (SELECT '1' FROM xxwc.xxwc_signature_capture_arc_tbl arc2 WHERE 1 = 1 AND stg.request_id = arc2.request_id)
      AND EXISTS (SELECT '1' FROM XXWC.XXWC_PRINT_LOG_TBL pri WHERE 1 = 1 AND stg.request_id = pri.request_id);
    EXCEPTION
    WHEN OTHERS THEN
      g_message := 'Error archiving XXWC_SIGNATURE_CAPTURE_TBL - '||SQLERRM;
      RAISE g_exception;
    END;
    
    -----------------------------------------------------------------------
    -- Deleteing archived records from XXWC_SIGNATURE_CAPTURE_TBL
    -----------------------------------------------------------------------
    BEGIN
    DELETE FROM xxwc.xxwc_signature_capture_tbl stg
    WHERE EXISTS (SELECT '1' FROM xxwc.xxwc_signature_capture_arc_tbl arc2 WHERE 1 = 1 AND stg.request_id = arc2.request_id)
      AND EXISTS (SELECT '1' FROM XXWC.XXWC_PRINT_LOG_TBL pri WHERE 1 = 1 AND stg.request_id = pri.request_id);
    EXCEPTION
    WHEN OTHERS THEN
      g_message := 'Error deleting archived records from XXWC_SIGNATURE_CAPTURE_TBL - '||SQLERRM;
      RAISE g_exception;
    END;
    -- Version# 2.1 < End
    
    commit;
    
    
    retcode := 0;
     
EXCEPTION

  WHEN G_EXCEPTION THEN
  
      write_log_output(g_message);
      retcode := 2;
  
      
     
END INSERT_CCR;


PROCEDURE  DELETE_CCR
              (retbuf                         OUT VARCHAR2
              ,retcode                        OUT NUMBER
              ,p_date                         IN VARCHAR2
              ) IS
              
              
  l_date DATE;
  l_count NUMBER;         
BEGIN

    /*We are going to copy records from XXWC.XXWC_PRINT_LOG_TBL to XXWC.XXWC_PRINT_LOG_BK_TBL
      Then truncate XXWC.XXWC_PRINT_LOG_TBL
      Insert records back to XXWC.XXWC_PRINT_LOG_TBL
      Then truncate XXWC.XXWC_PRINT_LOG_BK_TBL;
    */
  
      G_NAME := 'DELETE_CCR';
      G_PROGRAM_NAME := G_PACKAGE_NAME||'.'||G_NAME;
  
      write_log_output('******************************************************************');
      write_log_output('Starting Program '||G_PROGRAM_NAME);
      write_log_output('******************************************************************');


      l_date := FND_DATE.CANONICAL_TO_DATE(p_date);   --Change the p_date parameter from a varchar2 to a date 
         
    
    BEGIN
    
        select count(*)
        INTO  l_count
        FROM  XXWC.XXWC_PRINT_LOG_BK_TBL;
    EXCEPTION
        WHEN OTHERS THEN
            g_message := 'Could not count records on XXWC.XXWC_PRINT_LOG_BK_TBL ' || SQLCODE || SQLERRM;
            raise g_exception;
    END;
        
    
    --if there are records in the backup table, then truncate those records
    IF l_count > 0 THEN
        
      write_log_output('Truncating table XXWC.XXWC_PRINT_LOG_BK_TBL');
        
        BEGIN      
            EXECUTE IMMEDIATE 'TRUNCATE TABLE XXWC.XXWC_PRINT_LOG_BK_TBL';
        EXCEPTION
            WHEN OTHERS THEN
              g_message := 'Could not truncate XXWC.XXWC_PRINT_LOG_BK_TBL ' || SQLCODE || SQLERRM;
              raise g_exception;
        END;
        
      write_log_output('Truncated table XXWC.XXWC_PRINT_LOG_BK_TBL');
    
    END IF;
  
    
    write_log_output('Inserting into XXWC.XXWC_PRINT_LOG_BK_TBL');
   
    BEGIN
    
      INSERT INTO XXWC.XXWC_PRINT_LOG_BK_TBL
      SELECT * FROM XXWC.XXWC_PRINT_LOG_TBL WHERE ACTUAL_COMPLETION_DATE > l_date;
    
    EXCEPTION
      WHEN OTHERS THEN
          g_message := 'Could not insert into XXWC.XXWC_PRINT_LOG_BK_TBL ' || SQLCODE || SQLERRM;
          raise g_exception;
    
    END;
    
      COMMIT;
      
    write_log_output('Insert complete');
    
    write_log_output('Truncating table XXWC.XXWC_PRINT_LOG_TBL');
      
    BEGIN      
        EXECUTE IMMEDIATE 'TRUNCATE TABLE XXWC.XXWC_PRINT_LOG_TBL';
    EXCEPTION
        WHEN OTHERS THEN
          g_message := 'Could not truncate XXWC.XXWC_PRINT_LOG_TBL ' || SQLCODE || SQLERRM;
          raise g_exception;
    END;
    
    write_log_output('Truncated table XXWC.XXWC_PRINT_LOG_TBL');
    
    write_log_output('Inserting records back to XXWC.XXWC_PRINT_LOG_TBL');
    
  
    BEGIN
    
      INSERT INTO XXWC.XXWC_PRINT_LOG_TBL
      SELECT * FROM XXWC.XXWC_PRINT_LOG_BK_TBL;
    
    EXCEPTION
      WHEN OTHERS THEN
          g_message := 'Could not insert into XXWC.XXWC_PRINT_LOG_TBL ' || SQLCODE || SQLERRM;
          raise g_exception;
    
    END;
    
      COMMIT;
    
   write_log_output('Records inserted back to XXWC.XXWC_PRINT_LOG_TBL');
    
   write_log_output('Truncating XXWC.XXWC_PRINT_LOG_BK_TBL to clear out existing records');
    
    BEGIN      
        EXECUTE IMMEDIATE 'TRUNCATE TABLE XXWC.XXWC_PRINT_LOG_BK_TBL';
    EXCEPTION
        WHEN OTHERS THEN
          g_message := 'Could not truncate XXWC.XXWC_PRINT_LOG_BK_TBL ' || SQLCODE || SQLERRM;
          raise g_exception;
    END;
    
    write_log_output('Truncated table XXWC.XXWC_PRINT_LOG_BK_TBL');
     

  retcode :=0 ;
  
  
  
EXCEPTION

    WHEN g_exception THEN
    
        write_log_output(g_message);
        retcode :=2;
  
END DELETE_CCR;
              
END XXWC_PRINT_LOG_PKG;
/