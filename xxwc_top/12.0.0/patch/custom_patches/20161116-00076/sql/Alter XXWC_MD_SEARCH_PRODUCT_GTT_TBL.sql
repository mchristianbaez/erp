/*************************************************************************
Copyright (c) 2012 HD Supply
All rights reserved.
**************************************************************************
   $Header Alter XXWC_MD_SEARCH_PRODUCT_GTT_TBL$
  Module Name: XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL
  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     10-Oct-2016   Pahwa, Nancy                Initially Created TMS# 20160801-00182  
  -- 2.0     17-Nov-2016   Pahwa, Nancy		       Revert the change 20161116-00076
**************************************************************************/

alter table xxwc.XXWC_MD_SEARCH_PRODUCT_GTT_TBL drop column reservable;
alter table xxwc.XXWC_MD_SEARCH_PRODUCT_GTT_TBL drop column list_price_2;