/*************************************************************************
 Copyright (c) 2012 HD Supply
 All rights reserved.
**************************************************************************
   $Header APPS.XXWC_MD_PRODUCT_STORE_PREF $
  Module Name: APPS.XXWC_MD_PRODUCT_STORE_PREF 

  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     1-SEP-2015   Pahwa, Nancy                Initially Created 
TMS# 20150825-00012
  -- 2.0     17-Nov-2016   Pahwa, Nancy		       Revert the change 20161116-00076
**************************************************************************/

begin  
  ctx_ddl.drop_preference ('XXWC_MD_PRODUCT_STORE_PREF');
  ctx_ddl.drop_preference ('XXWC_MD_PRODUCT_STORE_LEX1');
  ctx_ddl.drop_section_group ('XXWC_MD_PRODUCT_STORE_SG'); 
  ctx_ddl.create_preference ('XXWC_MD_PRODUCT_STORE_PREF', 'multi_column_datastore');  
  ctx_ddl.set_attribute   
 	 ('XXWC_MD_PRODUCT_STORE_PREF',   
 	  'columns',   
 	  'partnumber,   
 	   shortdescription,
     	   cross_reference');  
  ctx_ddl.create_preference ('XXWC_MD_PRODUCT_STORE_LEX1', 'basic_lexer');  
  ctx_ddl.set_attribute ('XXWC_MD_PRODUCT_STORE_LEX1', 'whitespace', '/\|-_+,');  
  ctx_ddl.create_section_group ('XXWC_MD_PRODUCT_STORE_SG', 'basic_section_group');  
  ctx_ddl.add_field_section ('XXWC_MD_PRODUCT_STORE_SG', 'partnumber', 'partnumber', true);  
  ctx_ddl.add_field_section ('XXWC_MD_PRODUCT_STORE_SG', 'shortdescription', 'shortdescription', true);  
  ctx_ddl.add_field_section ('XXWC_MD_PRODUCT_STORE_SG', 'cross_reference', 'cross_reference', true);  
  end;  
/