--Report Name            : WC-Inventory AVP Taxware Code Audit Table
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View XXEIS_451485_PNHQKN_V
prompt Table type Objects cannot be imported
set scan off define off
prompt Creating Object APPS.XXEIS_451485_PNHQKN_V
set scan off define off
DECLARE
mod_exist varchar2(1);
stmt CLOB;
v_objlength INTEGER := 0;
v_offset INTEGER := 1;
v_sql_long DBMS_SQL.varchar2s;
v_sql_long_count INTEGER := 1;
c NUMBER;
r NUMBER;
l_stmt varchar2(32000);
l_view_cur pls_integer;
l_view_rows NUMBER;
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(222);
IF mod_exist = 'Y' THEN 
dbms_lob.createtemporary(stmt,TRUE);
dbms_lob.write( lob_loc   => stmt,amount    => length('create or replace view APPS.XXEIS_451485_PNHQKN_V
 ("UPDATED_ROWID","ORGANIZATION_ID","ITEM_ID","ITEM_NUMBER","OLD_TAXWARE_CODE","NEW_TAXWARE_CODE","OS_USER","HOST","IP_ADDRESS","TERMINAL","SESSIONID","SESSION_USER","SID","GLOBAL_UID","CREATED_BY","CREATED_USER","CREATION_DATE","TRUNC_CREATION_DATE","LAST_UPDATED_BY","LAST_UPDATE_DATE","UPDATED_BY"
) as '),offset    => 1, BUFFER    => 'create or replace view APPS.XXEIS_451485_PNHQKN_V
 ("UPDATED_ROWID","ORGANIZATION_ID","ITEM_ID","ITEM_NUMBER","OLD_TAXWARE_CODE","NEW_TAXWARE_CODE","OS_USER","HOST","IP_ADDRESS","TERMINAL","SESSIONID","SESSION_USER","SID","GLOBAL_UID","CREATED_BY","CREATED_USER","CREATION_DATE","TRUNC_CREATION_DATE","LAST_UPDATED_BY","LAST_UPDATE_DATE","UPDATED_BY"
) as ');
l_stmt :=  'SELECT UPDATED_ROWID,
  ORGANIZATION_ID,
  ITEM_ID,
  ITEM_NUMBER,
  OLD_TAXWARE_CODE,
  NEW_TAXWARE_CODE,
  OS_USER,
  HOST,
  IP_ADDRESS,
  TERMINAL,
  SESSIONID,
  SESSION_USER,
  SID,
  GLOBAL_UID,
  CREATED_BY ,
  (SELECT a.user_name
  FROM fnd_user a
  WHERE a.user_id=b.created_by
  AND rownum     =1
  )created_user,
  creation_date,
  TRUNC(creation_date) trunc_creation_date,
  LAST_UPDATED_BY,
  TRUNC(LAST_UPDATE_DATE)last_update_date,
    (SELECT a.user_name
  FROM fnd_user a
  WHERE a.user_id=b.last_updated_by
  AND ROWNUM     =1
  )updated_by
FROM xxcus.xxcus_mtl_items_audit_b b
WHERE 1              =1
AND old_taxware_code<>new_taxware_code

';
dbms_lob.APPEND( dest_lob  => stmt,      src_lob   => l_stmt );
v_objlength := DBMS_LOB.getlength (stmt);
IF v_objlength <= 32000 THEN 
EXECUTE IMMEDIATE to_char(stmt);
ELSE
v_offset := 1; 
WHILE v_offset <= v_objlength
LOOP
-- each record is 256 bytes from the LOB
v_sql_long (v_sql_long_count) := DBMS_LOB.SUBSTR (stmt, 256, v_offset);
-- increment the record count
v_sql_long_count := v_sql_long_count + 1;
-- figure out the new offset
v_offset := v_offset + 256;
END LOOP;
BEGIN
  -- open cursor
 c := DBMS_SQL.open_cursor;
 -- parse VARCHAR2S table
 DBMS_SQL.parse (c, v_sql_long, 1, v_sql_long_count - 1, FALSE, 1);
 -- Execute the cursor
r := DBMS_SQL.EXECUTE (c);
-- Close the cursor
 DBMS_SQL.close_cursor (c);
 END;
END IF;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 222');
END IF;
END;
/
prompt Creating Object Data XXEIS_451485_PNHQKN_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(222);
IF mod_exist = 'Y' THEN 
--Inserting Object XXEIS_451485_PNHQKN_V
xxeis.eis_rsc_ins.v( 'XXEIS_451485_PNHQKN_V',222,'Paste SQL View for WC-Inventory AVP Taxware Code Audit Table','','','','MM027735','APPS','WC-Inventory AVP Taxware Code Audit Table View','X4PV','','Y','VIEW','US','','','');
--Delete Object Columns for XXEIS_451485_PNHQKN_V
xxeis.eis_rsc_utility.delete_view_rows('XXEIS_451485_PNHQKN_V',222,FALSE);
--Inserting Object Columns for XXEIS_451485_PNHQKN_V
xxeis.eis_rsc_ins.vc( 'XXEIS_451485_PNHQKN_V','ITEM_NUMBER',222,'','','','','','MM027735','VARCHAR2','','','Item Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_451485_PNHQKN_V','OLD_TAXWARE_CODE',222,'','','','','','MM027735','VARCHAR2','','','Old Taxware Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_451485_PNHQKN_V','NEW_TAXWARE_CODE',222,'','','','','','MM027735','VARCHAR2','','','New Taxware Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_451485_PNHQKN_V','OS_USER',222,'','','','','','MM027735','VARCHAR2','','','Os User','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_451485_PNHQKN_V','HOST',222,'','','','','','MM027735','VARCHAR2','','','Host','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_451485_PNHQKN_V','IP_ADDRESS',222,'','','','','','MM027735','VARCHAR2','','','Ip Address','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_451485_PNHQKN_V','TERMINAL',222,'','','','','','MM027735','VARCHAR2','','','Terminal','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_451485_PNHQKN_V','SESSIONID',222,'','','','','','MM027735','VARCHAR2','','','Sessionid','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_451485_PNHQKN_V','SESSION_USER',222,'','','','','','MM027735','VARCHAR2','','','Session User','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_451485_PNHQKN_V','SID',222,'','','','','','MM027735','VARCHAR2','','','Sid','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_451485_PNHQKN_V','GLOBAL_UID',222,'','','','','','MM027735','VARCHAR2','','','Global Uid','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_451485_PNHQKN_V','CREATED_BY',222,'','','','','','MM027735','NUMBER','','','Created By','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_451485_PNHQKN_V','CREATION_DATE',222,'','','','','','MM027735','DATE','','','Creation Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_451485_PNHQKN_V','LAST_UPDATED_BY',222,'','','','','','MM027735','NUMBER','','','Last Updated By','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_451485_PNHQKN_V','LAST_UPDATE_DATE',222,'','','','','','MM027735','DATE','','','Last Update Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_451485_PNHQKN_V','UPDATED_ROWID',222,'','','','','','MM027735','VARCHAR2','','','Updated Rowid','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_451485_PNHQKN_V','ORGANIZATION_ID',222,'','','','','','MM027735','NUMBER','','','Organization Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_451485_PNHQKN_V','ITEM_ID',222,'','','','','','MM027735','NUMBER','','','Item Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_451485_PNHQKN_V','CREATED_USER',222,'Created User','CREATED_USER','','','','MM027735','VARCHAR2','','','Created User','','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_451485_PNHQKN_V','TRUNC_CREATION_DATE',222,'Trunc Creation Date','','','','','MM027735','DATE','','','Trunc Creation Date','','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_451485_PNHQKN_V','UPDATED_BY',222,'Updated By','UPDATED_BY','','','','MM027735','VARCHAR2','','','Updated By','','','','','');
--Inserting Object Components for XXEIS_451485_PNHQKN_V
--Inserting Object Component Joins for XXEIS_451485_PNHQKN_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 222');
END IF;
END;
/
prompt Creating Report LOV Data for WC-Inventory AVP Taxware Code Audit Table
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(222);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - WC-Inventory AVP Taxware Code Audit Table
xxeis.eis_rsc_ins.lov( 222,'select distinct old_taxware_code FROM xxcus.xxcus_mtl_items_audit_b order by 1','','EIS XXWC Old Taxware Code','To Fetch Old Taxware Code','PS066716',NULL,'N','','','N','N','','','US');
xxeis.eis_rsc_ins.lov( 222,'select distinct  new_taxware_code FROM xxcus.xxcus_mtl_items_audit_b order by 1','','EIS XXWC New Taxware Code','To fetch new taxware code','PS066716',NULL,'N','','','N','N','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 222');
END IF;
END;
/
prompt Creating Report Data for WC-Inventory AVP Taxware Code Audit Table
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(222);
IF mod_exist = 'Y' THEN 
--Deleting Report data - WC-Inventory AVP Taxware Code Audit Table
xxeis.eis_rsc_utility.delete_report_rows( 'WC-Inventory AVP Taxware Code Audit Table',222 );
--Inserting Report - WC-Inventory AVP Taxware Code Audit Table
xxeis.eis_rsc_ins.r( 222,'WC-Inventory AVP Taxware Code Audit Table','','This report is used to report changes to the Taxware Code (commonly called AVP Code) at the material master level.  This report shows changes to attribute22 of the material master item to highlight the changes made to the AVP/Taxware Code on a product for trouble shooting tax issues.  This is a select * from xxcus_mtl_items_audit_b.','','','','MM027735','XXEIS_451485_PNHQKN_V','Y','','SELECT UPDATED_ROWID,
  ORGANIZATION_ID,
  ITEM_ID,
  ITEM_NUMBER,
  OLD_TAXWARE_CODE,
  NEW_TAXWARE_CODE,
  OS_USER,
  HOST,
  IP_ADDRESS,
  TERMINAL,
  SESSIONID,
  SESSION_USER,
  SID,
  GLOBAL_UID,
  CREATED_BY ,
  (SELECT a.user_name
  FROM fnd_user a
  WHERE a.user_id=b.created_by
  AND rownum     =1
  )created_user,
  creation_date,
  TRUNC(creation_date) trunc_creation_date,
  LAST_UPDATED_BY,
  TRUNC(LAST_UPDATE_DATE)last_update_date,
    (SELECT a.user_name
  FROM fnd_user a
  WHERE a.user_id=b.last_updated_by
  AND ROWNUM     =1
  )updated_by
FROM xxcus.xxcus_mtl_items_audit_b b
WHERE 1              =1
AND old_taxware_code<>new_taxware_code
','MM027735','','N','White Cap Tax Reporting','','CSV,EXCEL,','N','','','','','','N','APPS','US','','','','','','','','','','','','','','','','');
--Inserting Report Columns - WC-Inventory AVP Taxware Code Audit Table
xxeis.eis_rsc_ins.rc( 'WC-Inventory AVP Taxware Code Audit Table',222,'ITEM_NUMBER','Item Number','','','','default','','1','N','','','','','','','','MM027735','N','N','','XXEIS_451485_PNHQKN_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'WC-Inventory AVP Taxware Code Audit Table',222,'OLD_TAXWARE_CODE','Old Taxware Code','','','','default','','2','N','','','','','','','','MM027735','N','N','','XXEIS_451485_PNHQKN_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'WC-Inventory AVP Taxware Code Audit Table',222,'NEW_TAXWARE_CODE','New Taxware Code','','','','default','','3','N','','','','','','','','MM027735','N','N','','XXEIS_451485_PNHQKN_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'WC-Inventory AVP Taxware Code Audit Table',222,'OS_USER','Os User','','','','default','','4','N','','','','','','','','MM027735','N','N','','XXEIS_451485_PNHQKN_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'WC-Inventory AVP Taxware Code Audit Table',222,'SESSION_USER','Session User','','','','default','','10','N','','','','','','','','MM027735','N','N','','XXEIS_451485_PNHQKN_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'WC-Inventory AVP Taxware Code Audit Table',222,'SID','Sid','','','','default','','8','N','','','','','','','','MM027735','N','N','','XXEIS_451485_PNHQKN_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'WC-Inventory AVP Taxware Code Audit Table',222,'CREATION_DATE','Creation Date','','','','default','','5','N','','','','','','','','MM027735','N','N','','XXEIS_451485_PNHQKN_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'WC-Inventory AVP Taxware Code Audit Table',222,'LAST_UPDATE_DATE','Last Update Date','','','','default','','6','N','','','','','','','','MM027735','N','N','','XXEIS_451485_PNHQKN_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'WC-Inventory AVP Taxware Code Audit Table',222,'ORGANIZATION_ID','Organization Id','','','~T~D~0','default','','9','N','','','','','','','','MM027735','N','N','','XXEIS_451485_PNHQKN_V','','','SUM','US','','');
xxeis.eis_rsc_ins.rc( 'WC-Inventory AVP Taxware Code Audit Table',222,'ITEM_ID','Item Id','','','~T~D~0','default','','7','N','','','','','','','','MM027735','N','N','','XXEIS_451485_PNHQKN_V','','','SUM','US','','');
xxeis.eis_rsc_ins.rc( 'WC-Inventory AVP Taxware Code Audit Table',222,'CREATED_USER','Created User','Created User','','','','','11','','Y','','','','','','','MM027735','N','N','','XXEIS_451485_PNHQKN_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'WC-Inventory AVP Taxware Code Audit Table',222,'TRUNC_CREATION_DATE','Trunc Creation Date','Trunc Creation Date','DATE','','','','1','','Y','','','','','','','MM027735','','','','XXEIS_451485_PNHQKN_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'WC-Inventory AVP Taxware Code Audit Table',222,'UPDATED_BY','Updated By','Updated By','','','','','13','','Y','','','','','','','MM027735','N','N','','XXEIS_451485_PNHQKN_V','','','','US','','');
--Inserting Report Parameters - WC-Inventory AVP Taxware Code Audit Table
xxeis.eis_rsc_ins.rp( 'WC-Inventory AVP Taxware Code Audit Table',222,'Creation Date (M/DD/YYYY)','','TRUNC_CREATION_DATE','IN','','','DATE','N','Y','1','Y','Y','CONSTANT','MM027735','Y','N','','Start Date','','XXEIS_451485_PNHQKN_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC-Inventory AVP Taxware Code Audit Table',222,'Item Number','','ITEM_NUMBER','IN','','','VARCHAR2','N','Y','2','Y','Y','CONSTANT','MM027735','Y','N','','','','XXEIS_451485_PNHQKN_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC-Inventory AVP Taxware Code Audit Table',222,'Old Taxware Code','Old Taxware Code','OLD_TAXWARE_CODE','IN','EIS XXWC Old Taxware Code','','VARCHAR2','N','Y','3','Y','Y','CONSTANT','MM027735','Y','N','','','','XXEIS_451485_PNHQKN_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC-Inventory AVP Taxware Code Audit Table',222,'New Taxware Code','New Taxware Code','NEW_TAXWARE_CODE','IN','EIS XXWC New Taxware Code','','VARCHAR2','N','Y','4','Y','Y','CONSTANT','MM027735','Y','N','','','','XXEIS_451485_PNHQKN_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC-Inventory AVP Taxware Code Audit Table',222,'Last Update Date','Last Update Date','LAST_UPDATE_DATE','IN','','','DATE','N','Y','5','Y','Y','CONSTANT','MM027735','Y','N','','','','XXEIS_451485_PNHQKN_V','','','US','');
--Inserting Dependent Parameters - WC-Inventory AVP Taxware Code Audit Table
--Inserting Report Conditions - WC-Inventory AVP Taxware Code Audit Table
xxeis.eis_rsc_ins.rcnh( 'WC-Inventory AVP Taxware Code Audit Table',222,'X4PV.TRUNC_CREATION_DATE IN Creation Date (M/DD/YYYY)','SIMPLE','','','Y','','1');
xxeis.eis_rsc_ins.rcnd( '','','TRUNC_CREATION_DATE','','Creation Date (M/DD/YYYY)','','','','','XXEIS_451485_PNHQKN_V','','','','','','IN','Y','Y','','','','','1',222,'WC-Inventory AVP Taxware Code Audit Table','X4PV.TRUNC_CREATION_DATE IN Creation Date (M/DD/YYYY)');
xxeis.eis_rsc_ins.rcnh( 'WC-Inventory AVP Taxware Code Audit Table',222,'X4PV.ITEM_NUMBER IN Item Number','SIMPLE','','','Y','','2');
xxeis.eis_rsc_ins.rcnd( '','','ITEM_NUMBER','','Item Number','','','','','XXEIS_451485_PNHQKN_V','','','','','','IN','Y','Y','','','','','1',222,'WC-Inventory AVP Taxware Code Audit Table','X4PV.ITEM_NUMBER IN Item Number');
xxeis.eis_rsc_ins.rcnh( 'WC-Inventory AVP Taxware Code Audit Table',222,'X4PV.OLD_TAXWARE_CODE IN Old Taxware Code','SIMPLE','','','Y','','3');
xxeis.eis_rsc_ins.rcnd( '','','OLD_TAXWARE_CODE','','Old Taxware Code','','','','','XXEIS_451485_PNHQKN_V','','','','','','IN','Y','Y','','','','','1',222,'WC-Inventory AVP Taxware Code Audit Table','X4PV.OLD_TAXWARE_CODE IN Old Taxware Code');
xxeis.eis_rsc_ins.rcnh( 'WC-Inventory AVP Taxware Code Audit Table',222,'X4PV.NEW_TAXWARE_CODE IN New Taxware Code','SIMPLE','','','Y','','4');
xxeis.eis_rsc_ins.rcnd( '','','NEW_TAXWARE_CODE','','New Taxware Code','','','','','XXEIS_451485_PNHQKN_V','','','','','','IN','Y','Y','','','','','1',222,'WC-Inventory AVP Taxware Code Audit Table','X4PV.NEW_TAXWARE_CODE IN New Taxware Code');
xxeis.eis_rsc_ins.rcnh( 'WC-Inventory AVP Taxware Code Audit Table',222,'X4PV.LAST_UPDATE_DATE IN Last Update Date','SIMPLE','','','Y','','5');
xxeis.eis_rsc_ins.rcnd( '','','LAST_UPDATE_DATE','','Last Update Date','','','','','XXEIS_451485_PNHQKN_V','','','','','','IN','Y','Y','','','','','1',222,'WC-Inventory AVP Taxware Code Audit Table','X4PV.LAST_UPDATE_DATE IN Last Update Date');
--Inserting Report Sorts - WC-Inventory AVP Taxware Code Audit Table
xxeis.eis_rsc_ins.rs( 'WC-Inventory AVP Taxware Code Audit Table',222,'CREATION_DATE','ASC','MM027735','1','');
--Inserting Report Triggers - WC-Inventory AVP Taxware Code Audit Table
--inserting report templates - WC-Inventory AVP Taxware Code Audit Table
--Inserting Report Portals - WC-Inventory AVP Taxware Code Audit Table
--inserting report dashboards - WC-Inventory AVP Taxware Code Audit Table
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'WC-Inventory AVP Taxware Code Audit Table','222','XXEIS_451485_PNHQKN_V','XXEIS_451485_PNHQKN_V','N','');
--inserting report security - WC-Inventory AVP Taxware Code Audit Table
xxeis.eis_rsc_ins.rsec( 'WC-Inventory AVP Taxware Code Audit Table','','DM027741','',222,'MM027735','','','');
xxeis.eis_rsc_ins.rsec( 'WC-Inventory AVP Taxware Code Audit Table','222','','HDS_RCVBLS_MNGR_WC',222,'MM027735','','','');
xxeis.eis_rsc_ins.rsec( 'WC-Inventory AVP Taxware Code Audit Table','222','','XXWC_RECEIVABLES_INQUIRY_WC',222,'MM027735','','','');
xxeis.eis_rsc_ins.rsec( 'WC-Inventory AVP Taxware Code Audit Table','222','','XXWC_CRE_CREDIT_COLL_MGR',222,'MM027735','','','');
xxeis.eis_rsc_ins.rsec( 'WC-Inventory AVP Taxware Code Audit Table','','LA023190','',222,'MM027735','','','');
--Inserting Report Pivots - WC-Inventory AVP Taxware Code Audit Table
--Inserting Report Distribution Details 
--Inserting Report Distribution Details 
--Inserting Report SubTotals Details 
--Inserting Report aggregations 
--Inserting Report   Version details- WC-Inventory AVP Taxware Code Audit Table
xxeis.eis_rsc_ins.rv( 'WC-Inventory AVP Taxware Code Audit Table','','WC-Inventory AVP Taxware Code Audit Table','SA059956','08-FEB-2018');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 222');
END IF;
END;
/
