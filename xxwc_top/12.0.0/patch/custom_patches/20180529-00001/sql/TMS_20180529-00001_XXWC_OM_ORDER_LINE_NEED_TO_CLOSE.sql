/*************************************************************************
  $Header TMS_20180529-00001_XXWC_OM_ORDER_LINE_NEED_TO_CLOSE.sql $
  Module Name: TMS_XXWC_OM_ORDER_NEED_TO_CLOSE.sql

  PURPOSE:   Created to process the order headers stucked in booked status
             and line  is in Fulfilled status.

  REVISIONS:
  Ver        Date        Author               Description
  ---------  ----------  ---------------      -------------------------
  1.0        07/27/2018  Pattabhi Avula       TMS#20180529-00001
  **************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000;
DECLARE
BEGIN
 DBMS_OUTPUT.put_line ('TMS: Datafix script    , Before Update');
 -- Updating the headers table
 
UPDATE apps.oe_order_lines_all 
   SET flow_status_Code='CLOSED',
       open_flag='N' 
 WHERE line_id=121609113;

		   	 
	 COMMIT;
		   
DBMS_OUTPUT.put_line ('Records updated-' || SQL%ROWCOUNT);

	  DBMS_OUTPUT.put_line ('TMS: 20180529-00001  , End Update');
EXCEPTION
 WHEN OTHERS THEN
 DBMS_OUTPUT.put_line ('TMS: 20180529-00001 , Errors : ' || SQLERRM);
END;
/