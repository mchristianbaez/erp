:: This batch file will export all RDF reports in the
:: current directory to XML file, overwriting existing
:: XML files. It should only be run when necessary. 
:: Normally you must export your XML files manually before
:: committing.
:: Created 4/17/2013, Andre Rivas

REM WINDOWS Export forms to fmt
@ECHO OFF
:: Not exporting to fmt.
::compile_forms.bat
::cls
::Echo Exporting Forms....
::for %%f IN (*.fmb) do frmcmp module=%%f userid=apps/qafapps@ebizfqa batch=yes module_type=FORM script=YES window_state=minimize
::ECHO FINISHED Exporting Forms

REM WINDOWS Export Reports to XML
@ECHO OFF
cls
Echo Exporting Reports to XML ....
for %%f IN (*.rdf) do rwconverter apps/qafapps@ebizfqa stype=rdffile source=%%f dtype=xmlfile batch=yes overwrite=yes
ECHO FINISHED Exporting


