/* TMS 20151104-00152  -Disable and Enable a trigger that prevents personalization changes.
This is needed for Parent task 20150611-00128  PO - Fix issue with Need by date personalization.
Date created: 11/4/2015
Date last updated: 11/4/2015 */

SET SERVEROUTPUT ON SIZE 100000;

DECLARE
   l_status   VARCHAR2 (10);
   sql_stmt   VARCHAR2 (200);
BEGIN
   SELECT STATUS
     INTO l_status
     FROM all_triggers
    WHERE     owner = 'AM_AGENT'
          AND trigger_name = 'FND_FORM_CUSTOM_AC2370_7165_AM';

   DBMS_OUTPUT.put_line ('Trigger status before change is ' || l_status);

   IF l_status = 'ENABLED'
   THEN
      sql_stmt :=
         'alter trigger am_agent.fnd_form_custom_AC2370_7165_am disable';

      EXECUTE IMMEDIATE sql_stmt;
   ELSE
      sql_stmt :=
         'alter trigger am_agent.fnd_form_custom_AC2370_7165_am enable';

      EXECUTE IMMEDIATE sql_stmt;
   END IF;

   SELECT STATUS
     INTO l_status
     FROM all_triggers
    WHERE     owner = 'AM_AGENT'
          AND trigger_name = 'FND_FORM_CUSTOM_AC2370_7165_AM';

   DBMS_OUTPUT.put_line ('Trigger status after change is ' || l_status);
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Error is ' || SQLERRM);
END;
/