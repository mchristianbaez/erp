spool OM21549733;

SET serveroutput ON;

DECLARE
	l_line_id      NUMBER :=42043527 ;
	
	l_result       VARCHAR2(1000);
	l_file_name    VARCHAR2(1000);
	l_db_name      VARCHAR2(1000);
	
BEGIN
	-- Setup debugging
	oe_debug_pub.debug_on;
	oe_debug_pub.initialize;
	oe_debug_pub.setdebuglevel(5);
	
	l_file_name := oe_debug_pub.set_debug_mode('FILE');
	dbms_output.put_line('Debug log is located at: ' || Oe_debug_pub.g_dir || '/' ||Oe_debug_pub.g_file);
	
	SELECT name 
	INTO l_db_name
	FROM V$DATABASE;
	oe_debug_pub.add('Running for databse: ' || l_db_name);
	
	
	oe_debug_pub.add('process line: '||l_line_id||' to cancelled');
	dbms_output.put_line('process line: '||l_line_id||' to cancelled');
	
	update oe_order_lines_all set FLOW_STATUS_CODE = 'CANCELLED', open_flag = 'N', LAST_UPDATE_DATE = sysdate, LAST_UPDATED_BY = 21549733 where line_id = l_line_id;
	
	oe_debug_pub.add('skip close_activity savepoint');
	
	Oe_standard_wf.OEOL_Selector(p_itemtype => 'OEOL'
							 ,p_itemkey => to_char(l_line_id)
							 ,p_actid => 12345
							 ,p_funcmode => 'SET_CTX'
							 ,p_result => l_result
							 );
							 
	oe_debug_pub.add('Result: '||l_result );

	oe_debug_pub.add('Calling handleerror for line...');
	
	wf_engine.handleerror( itemtype    =>    'OEOL'
						  ,itemkey     =>    To_Char(l_line_id)
						  ,activity    =>    'CLOSE_LINE'
						  ,command     =>    'SKIP'
						  ,result      =>    'COMPLETE'
						);
	
	oe_debug_pub.add('CLOSE_LINE Skiped...');

    
	-- Finishing script with success
	oe_debug_pub.add('Script successfully executed.');
	dbms_output.put_line('Script successfully executed.');
	oe_debug_pub.debug_off;
	
EXCEPTION
	WHEN Others THEN
		oe_debug_pub.ADD('Error: ...'||SQLERRM);
        Dbms_Output.Put_Line('Error: ...'||SQLERRM);
        ROLLBACK;
	
END;
/
COMMIT;

spool OFF;
/