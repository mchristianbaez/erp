/*************************************************************************
   *   $Header XXWC_OM_PRISM_RMA_REPORT.sql $
   *   Module Name: xxwc OM Prism Return Report
   *
   *   PURPOSE:   Report displays all RMA orders without a return reference for a
   *              given date range and branch 
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        01/08/2013  Shankar Hariharan        Initial Version
   *   1.1        02/07/2013  Shankar Hariharan        TMS Task 20130206-00639
   * ***************************************************************************/

SELECT    'BRANCH'
       || '|'
       || 'ORDER_NUMBER'
       || '|'
       || 'ORDERED_DATE'
       || '|'
       || 'ORDER_TYPE'
       || '|'
       || 'ORDER_TOTAL'
       || '|'
       || 'CUST_PO'
       || '|'
       || 'CUSTOMER_NUM'
       || '|'
       || 'CUSTOMER_NAME'
       || '|'
       || 'PRISM_RETURNS'
  FROM DUAL
UNION ALL
SELECT data
  FROM (  SELECT (   a.ship_from
                  || '|'
                  || a.order_number
                  || '|'
                  || a.ordered_date
                  || '|'
                  || a.order_type
                  || '|'
                  || TO_CHAR (
                        OE_TOTALS_GRP.GET_ORDER_TOTAL (a.HEADER_ID
                                                     , NULL
                                                     , 'ALL')
                      , FND_CURRENCY.SAFE_GET_FORMAT_MASK (
                           a.TRANSACTIONAL_CURR_CODE
                         , 30))
                  || '|'
                  || a.cust_po_number
                  || '|'
                  || a.customer_number
                  || '|'
                  || a.sold_to
                  || '|'
                  || attribute8)
                    data
            FROM oe_order_headers_v a
           WHERE a.order_type LIKE '%RETURN%'
                 AND a.flow_status_code NOT IN ('ENTERED', 'CANCELLED')
                 AND EXISTS
                        (SELECT 1
                           FROM oe_order_lines_all b
                          WHERE a.header_id = b.header_id
                                AND b.flow_status_code NOT IN
                                       ('ENTERED', 'CANCELLED')
                                AND b.return_context IS NULL
                                AND b.line_category_code = 'RETURN')
                 AND a.ship_from_org_id = NVL ('&1', a.ship_from_org_id)
                 --and trunc(a.ordered_date) between  fnd_date.canonical_to_date('&2') and  fnd_date.canonical_to_date('&3')
                 AND TRUNC (a.booked_date) BETWEEN fnd_date.canonical_to_date (
                                                      '&2')
                                               AND fnd_date.canonical_to_date (
                                                      '&3')
                 -- Added by Shankar TMS 20130206-00639                                      
                 AND not exists (select 1 from hz_cust_accounts c
                                  where c.cust_account_id=a.sold_to_org_id
                                    and c.customer_type='I')
                 AND exists ( select 1 
                                From hz_customer_profiles d, ra_terms e
                               where d.site_use_id is null
                                 and d.cust_account_id=a.sold_to_org_id
                                 and d.standard_terms=e.term_id
                                 and (e.prepayment_flag='Y' or e.name='IMMEDIATE'))  
                 -- Added by Shankar TMS 20130206-00639                                                                                       
        ORDER BY a.ship_from, a.ordered_date)
/