/*
 TMS: 20150922-00093   
 Date: 09/21/2015
 Notes: data fix script to process month end transactions
*/

SET SERVEROUTPUT ON SIZE 100000;
SET VERIFY OFF;



UPDATE apps.wsh_delivery_details
SET released_status = 'D',
src_requested_quantity = 0,
requested_quantity = 0,
shipped_quantity = 0,
cycle_count_quantity = 0,
cancelled_quantity = 0,
subinventory = null,
locator_id = null,
lot_number = null,
revision = null,
inv_interfaced_flag = 'X',
oe_interfaced_flag = 'X'
WHERE delivery_detail_id =11994234;

commit;

/