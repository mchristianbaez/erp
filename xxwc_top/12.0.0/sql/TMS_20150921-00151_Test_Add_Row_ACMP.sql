/*
	$Header: TMS_20150921-00151_Test_Add_Row_ACMP.sql 12.2 2015/10/23 09:38 arivas ship $

	This is a test SQL Insert 20150921-00151. 
	
	This will test whether ACMP can choose the correct revision of a file. Will commit
	a couple revisions of this one.
	
*/
SET VERIFY OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK;
WHENEVER OSERROR  EXIT FAILURE ROLLBACK;

INSERT INTO
  XXWC.TEST_DELETE_ME(COLUMN_1)
VALUES
  ('Test one. Version 12.2');

commit;
exit;