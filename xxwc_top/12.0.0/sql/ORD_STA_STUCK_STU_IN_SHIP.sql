/*
 TMS: 20150812-00171   
 Date: 08/12/2015
 Notes: Order status is stuck in Shipped.
*/

SET SERVEROUTPUT ON SIZE 100000;



update apps.oe_order_lines_all
set 
  INVOICE_INTERFACE_STATUS_CODE='YES'
,open_flag='N'
,flow_status_code='CLOSED'
,INVOICED_QUANTITY=1
where line_id=52634124
and headeR_id=32033229;

commit;

/



