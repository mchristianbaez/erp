CREATE SEQUENCE XXWC.XXWC_OM_CONTRACT_PRICING_HDR_S
  START WITH 1
  MAXVALUE 9999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  NOCACHE 
  NOORDER
/
CREATE SEQUENCE XXWC.XXWC_OM_CONTRACT_PRICING_LIN_S
  START WITH 1
  MAXVALUE 9999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  NOCACHE 
  NOORDER
/
grant all on XXWC.XXWC_OM_CONTRACT_PRICING_HDR_S to apps
/

CREATE TABLE xxwc.xxwc_om_contract_pricing_hdr
(
   agreement_id            NUMBER NOT NULL PRIMARY KEY
 , price_type              VARCHAR2 (30) NOT NULL
 , customer_id             NUMBER
 , customer_site_id        NUMBER
 , agreement_status        VARCHAR2 (30) NOT NULL
 , revision_number         NUMBER
 , organization_id         NUMBER
 , gross_margin            NUMBER
 , incompatability_group   VARCHAR2 (30) NOT NULL
 , attribute_category      VARCHAR2 (30)
 , attribute1              VARCHAR2 (150)
 , attribute2              VARCHAR2 (150)
 , attribute3              VARCHAR2 (150)
 , attribute4              VARCHAR2 (150)
 , attribute5              VARCHAR2 (150)
 , attribute6              VARCHAR2 (150)
 , attribute7              VARCHAR2 (150)
 , attribute8              VARCHAR2 (150)
 , attribute9              VARCHAR2 (150)
 , attribute10             VARCHAR2 (150)
 , attribute11             VARCHAR2 (150)
 , attribute12             VARCHAR2 (150)
 , attribute13             VARCHAR2 (150)
 , attribute14             VARCHAR2 (150)
 , attribute15             VARCHAR2 (150)
 , creation_date           DATE NOT NULL
 , created_by              NUMBER NOT NULL
 , last_update_date        DATE NOT NULL
 , last_updated_by         NUMBER NOT NULL
 , last_update_login       NUMBER NOT NULL
)
/

grant all on xxwc.xxwc_om_contract_pricing_hdr to apps
/

CREATE TABLE xxwc.xxwc_om_contract_pricing_lines
(
   agreement_line_id       NUMBER NOT NULL PRIMARY KEY
 , agreement_id            NUMBER NOT NULL
 , agreement_type          VARCHAR2 (30) NOT NULL
 , vendor_quote_number     VARCHAR2 (30)
 , product_attribute       VARCHAR2 (30) NOT null
 , product_value           VARCHAR2 (50) NOT null
 , product_Description     VARCHAR2 (240)
 , list_price              number
 , start_date              DATE
 , end_date                DATE
 , modifier_type           VARCHAR2 (30) NOT NULL
 , modifier_value          NUMBER
 , selling_price           NUMBER
 , special_cost            NUMBER
 , vendor_id               NUMBER
 , max_quantity_terms      number
 , average_Cost            number
 , gross_margin            NUMBER
 , line_status        VARCHAR2 (30) NOT NULL
 , interfaced_flag         VARCHAR2 (1)
 , revision_number         NUMBER
 , latest_rec_flag         VARCHAR2(1)
 , attribute_category      VARCHAR2 (30)
 , attribute1              VARCHAR2 (150)
 , attribute2              VARCHAR2 (150)
 , attribute3              VARCHAR2 (150)
 , attribute4              VARCHAR2 (150)
 , attribute5              VARCHAR2 (150)
 , attribute6              VARCHAR2 (150)
 , attribute7              VARCHAR2 (150)
 , attribute8              VARCHAR2 (150)
 , attribute9              VARCHAR2 (150)
 , attribute10             VARCHAR2 (150)
 , attribute11             VARCHAR2 (150)
 , attribute12             VARCHAR2 (150)
 , attribute13             VARCHAR2 (150)
 , attribute14             VARCHAR2 (150)
 , attribute15             VARCHAR2 (150)
 , creation_date           DATE NOT NULL
 , created_by              NUMBER NOT NULL
 , last_update_date        DATE NOT NULL
 , last_updated_by         NUMBER NOT NULL
 , last_update_login       NUMBER NOT NULL
)
/

grant all on xxwc.xxwc_om_contract_pricing_lines to apps
/

create synonym apps.XXWC_OM_CONTRACT_PRICING_HDR_S for xxwc.XXWC_OM_CONTRACT_PRICING_HDR_S
/
create synonym apps.XXWC_OM_CONTRACT_PRICING_LIN_S for xxwc.XXWC_OM_CONTRACT_PRICING_LIN_S
/
create synonym apps.xxwc_om_contract_pricing_hdr for xxwc.xxwc_om_contract_pricing_hdr
/
create synonym apps.xxwc_om_contract_pricing_lines for xxwc.xxwc_om_contract_pricing_lines
/

create or replace view apps.xxwc_ar_customers_v
as
select customer_number||' - '||customer_name customer_num_name, customer_id
  from ar_customers
/  

create or replace view apps.xxwc_inv_organization_v
as
select a.organization_code||' - '||a.organization_name org_code_name, a.organization_id
  from org_organization_definitions a, mtl_parameters b
 where a.organization_id=b.organization_id
   and b.organization_id <> b.master_organization_id
/   

create or replace view apps.xxwc_om_contract_pricing_hdr_v
as
select 
   a.agreement_id           
 , a.price_type              
 , a.customer_id            
 , a.customer_site_id       
 , a.agreement_status       
 , a.revision_number        
 , a.organization_id        
 , a.gross_margin           
 , a.incompatability_group  
 , (SELECT account_number || ' - ' || party_name
      FROM hz_cust_accounts acc, hz_parties hzp
     where acc.party_id=hzp.party_id
       and acc.cust_account_id=a.customer_id
       and a.price_type<>'NATIONAL'
    union
    select description
      from fnd_lookup_values 
     where lookup_type = 'NATIONAL_ACCOUNTS' 
       and lookup_code = to_char(a.customer_id)
       and a.price_type='NATIONAL'
     ) customer_info
  , (select location
       from hz_cust_site_uses_all hsu
      where hsu.site_Use_id=a.customer_site_id
    ) site_info
  , (select org_code_name
       from xxwc_inv_organization_v inv
      where inv.organization_id=a.organization_id
    ) branch_info  
  from apps.xxwc_om_contract_pricing_hdr a
/  
 
 create table xxwc.XXWC_CSP_HDR_INTERFACE
 ( reference_id       varchar2(240)
 , price_type     varchar2(30)
 , CUSTOMER_NUMBER    varchar2(30)
 , job_location       varchar2(100)
 , incompatability    varchar2(30)
 , branch             varchar2(30)
 , process_flag       varchar2(1)
 , error_message      varchar2(240)
)
/

create index xxwc.XXWC_CSP_HDR_INTERFACE_n1
on xxwc.XXWC_CSP_HDR_INTERFACE (reference_id)
/

 create table xxwc.XXWC_CSP_LINES_INTERFACE
 ( reference_id       varchar2(240)
 , agreement_type     varchar2(30)
 , start_date         date
 , end_date           date
 , product_attribute  varchar2(30)
 , product_value       varchar2(100)
 , app_method    varchar2(30)
 , modifier_value             number
 , vq_number      varchar2(100)
 , special_cost   number
 , vendor_number  varchar2(30)
 , process_flag       varchar2(1)
 , error_message      varchar2(240)
)
/

create index xxwc.XXWC_CSP_LINES_INTERFACE_n1
on xxwc.XXWC_CSP_LINES_INTERFACE (reference_id)
/
 
create synonym  apps.XXWC_CSP_HDR_INTERFACE for xxwc.XXWC_CSP_HDR_INTERFACE
/

create synonym apps.XXWC_CSP_LINES_INTERFACE for xxwc.XXWC_CSP_LINES_INTERFACE
/