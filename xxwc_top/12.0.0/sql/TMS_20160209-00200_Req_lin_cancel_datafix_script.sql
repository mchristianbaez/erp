  /****************************************************************************************************************************************
  *   PROCEDURE: cancel_requisition_lines                                                                                                 *
  *                                   																									  *
  *   PURPOSE: Procedure will pick Internal and purchasing requisition which is in approved status and older than 7 days and canclled     *
  *                                   																									  *
  *   HISTORY                                																							  *
  *   ===========================================================================               									      *
  *   ===========================================================================              											  *
  *   VERSION DATE          AUTHOR(S)               DESCRIPTION                   														  *
  *   ------- -----------   ---------------         -------------------------------------             									  *
  *    1.0    01-MAR-2016   Pattabhi Avula          Initial Version TMS# 20160209-00200                                                   *
  ****************************************************************************************************************************************/
SET SERVEROUTPUT ON SIZE 100000;
SET VERIFY OFF;
DECLARE
  -- Approved Internal Requisitions
  l_return_status VARCHAR2 (1000);
  l_msg_count     NUMBER;
  l_msg_data      VARCHAR2 (1000);
  ln_header_id po_tbl_number;
  ln_line_id po_tbl_number;
  m               NUMBER := NULL;
  l_msg_dummy     VARCHAR2 (2000);
  l_output        VARCHAR2 (2000);
  ln_user_id      NUMBER;
  ln_resp_id      NUMBER;
  ln_resp_appl_id NUMBER;
  ln_org_id       NUMBER := 162;

  
  CURSOR cur_req_intnpur
  IS
    SELECT prh.requisition_header_id, prl.requisition_line_id         
    FROM po_requisition_headers_all prh, 
	     po_requisition_lines_all prl
   WHERE    1=1
         and prh.requisition_header_id = prl.requisition_header_id
         and prh.type_lookup_code='INTERNAL'
         AND prh.AUTHORIZATION_STATUS = 'APPROVED'
         AND NVL(prh.cancel_flag,'N')='N'
         AND NVL(prl.cancel_flag,'N')='N'
         AND NVL(prh.CLOSED_CODE,'OPEN') NOT IN ('FINALLY CLOSED','CLOSED')
         AND NVL(prl.CLOSED_CODE,'OPEN') NOT IN ('FINALLY CLOSED','CLOSED')
         and PRH.CREATION_DATE <TRUNC(sysdate-7)
        -- and PRH.REQUISITION_HEADER_ID=489623
         and not exists (select 1 from OE_ORDER_LINES_ALL OLA where PRL.REQUISITION_LINE_ID = OLA.SOURCE_DOCUMENT_LINE_ID and LINE_TYPE_ID=1012)
union all
  SELECT prh.requisition_header_id, prl.REQUISITION_LINE_ID         
    FROM po_requisition_headers_all prh, po_requisition_lines_all prl,po_req_distributions_all prd
   WHERE    1=1
         and prh.requisition_header_id = prl.requisition_header_id
         and prl.requisition_line_id=prd.requisition_line_id
         and prh.type_lookup_code='PURCHASE'
         AND prh.AUTHORIZATION_STATUS = 'APPROVED'
         AND NVL(prh.cancel_flag,'N')='N'
         AND NVL(prl.cancel_flag,'N')='N'
         AND NVL(prh.CLOSED_CODE,'OPEN') NOT IN ('FINALLY CLOSED','CLOSED')
         AND NVL(prl.CLOSED_CODE,'OPEN') NOT IN ('FINALLY CLOSED','CLOSED')
        and PRH.CREATION_DATE <TRUNC(sysdate-7)
       --- and PRH.REQUISITION_HEADER_ID=10686498
         and not exists (select 1 from PO_DISTRIBUTIONS_ALL PDA where REQ_DISTRIBUTION_ID=PRD.DISTRIBUTION_ID)
        order by 1;

BEGIN
  
  SELECT user_id
  INTO   ln_user_id
  FROM   fnd_user
  WHERE  user_name = 'XXWC_INT_SUPPLYCHAIN';
  
  
  SELECT responsibility_id,
         application_id
  INTO   ln_resp_id,
         ln_resp_appl_id
  FROM   fnd_responsibility_vl
  WHERE responsibility_name = 'HDS Purchasing Super User';
  
  fnd_global.apps_initialize (user_id => ln_user_id, resp_id => ln_resp_id, --'HDS Purchasing Super User'
  resp_appl_id => ln_resp_appl_id);
  mo_global.init ('PO');
  mo_global.set_policy_context ('S', ln_org_id);
  fnd_request.set_org_id (fnd_global.org_id);
  
  -- Approved Internal and Purchase Requisitions
  FOR I    IN cur_req_intnpur
  LOOP
    m            := 1;
    ln_header_id := po_tbl_number (I.requisition_header_id);
    ln_line_id   := po_tbl_number (I.requisition_line_id);
    po_req_document_cancel_grp.cancel_requisition (p_api_version => 1.0, 
	                                               p_req_header_id => ln_header_id, 
												   p_req_line_id => ln_line_id, 
												   p_cancel_date => SYSDATE, 
												   p_cancel_reason => 'Cancelled Requisition', 
												   p_source => 'REQUISITION', 
												   x_return_status => l_return_status, 
												   x_msg_count => l_msg_count, 
												   x_msg_data => l_msg_data
												  );
    COMMIT;
   DBMS_OUTPUT.PUT_LINE('Requisition line id'||' '||I.requisition_line_id||' '||'status is :'||' '||l_return_status);
   
    IF l_return_status <> 'S' THEN
      fnd_msg_pub.get (m, fnd_api.g_false, l_msg_data, l_msg_dummy);
      l_output   := (TO_CHAR (m) || ': ' || l_msg_data);
      DBMS_OUTPUT.PUT_LINE('Failed requisition id :'||I.requisition_line_id||' '||'and details is: '||l_output);
    END IF;
  END LOOP;
 END;
 /