-- 20180815-00037 (Prop 65 Production for EHS WebADI)
set serveroutput ON
DECLARE
	l_error_code  VARCHAR2(1) := 'S';
	l_err_message VARCHAR2(4000);
	l_sec VARCHAR2(10);

BEGIN
  dbms_output.put_line('... Started');
	UPDATE INV.MTL_SYSTEM_ITEMS_B
	SET ATTRIBUTE1 = DECODE(SUBSTR(UPPER(ATTRIBUTE1),1,1),'Y','Y','N')
	WHERE ATTRIBUTE1 IS NOT NULL
	AND ATTRIBUTE1 <> 'Y';
  dbms_output.put_line('...Update of MTL_SYSTEM_ITEMS_B Attribute1 CA Prop65  - Done, rows '||sql%rowcount);
  COMMIT;
EXCEPTION
  WHEN OTHERS THEN
      dbms_output.put_line('... Error Occurred: '||sqlerrm);

END;
/

