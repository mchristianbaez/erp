/*
 TMS: 20151029-00068    
 Date: 10/20/2015
 Notes: SF Data fix script for I628342
*/

SET SERVEROUTPUT ON SIZE 100000;
SET VERIFY OFF;


update apps.oe_order_lines_all
set 
  INVOICE_INTERFACE_STATUS_CODE='NOT_ELIGIBLE'
,open_flag='N'
,flow_status_code='CLOSED'
--,INVOICED_QUANTITY=1
where line_id =58635869
and headeR_id=35729111;
		  
--1 row expected to be updated

commit;

/