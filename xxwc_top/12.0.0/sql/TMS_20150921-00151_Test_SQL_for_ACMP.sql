/*
	$Header: TMS_20150921-00151_Test_SQL_for_ACMP.sql 12.3 2015/10/15 09:38 arivas ship $

	This is a test PL/SQL file which does a test DDL. 20150921-00151. 
	
	Now I have updated the header version number and date.
	
	This should create a table in XXWC. Updating file version.
	
	Removing the trailing forward-slash after the create table statement. It
	would try to execute it a second time. Only PL/SQL requires a slash, I've learned.
	
*/
SET VERIFY OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK;
WHENEVER OSERROR  EXIT FAILURE ROLLBACK;

CREATE TABLE XXWC.TEST_DELETE_ME
(COLUMN_1 VARCHAR2(30));

commit;
exit;