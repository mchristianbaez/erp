SET serveroutput ON
SET verify OFF

SPOOL om_datafix.lst;

DECLARE
  l_result		VARCHAR2(30);
  l_line_id             NUMBER;
  l_file_name           VARCHAR2(255);


CURSOR lines IS
SELECT oel.line_id
FROM oe_order_lines_all oel
WHERE oel.booked_flag = 'Y'
AND oel.open_flag = 'Y'
AND oel.shipping_interfaced_flag = 'Y'
AND oel.shippable_flag = 'Y'
AND oel.shipped_quantity = oel.ordered_quantity
AND oel.shipping_quantity IS NOT NULL
AND oel.flow_status_code = 'AWAITING_SHIPPING'
AND oel.ato_line_id IS NULL
AND Nvl(oel.invoiced_quantity,0)  = 0
AND Nvl(oel.invoice_interface_status_code,'N') ='N'
AND oel.line_id  = 43720188
AND EXISTS (SELECT 1 FROM wsh_delivery_details wdd
            WHERE oel.line_id = wdd.source_line_id
            AND wdd.released_status = 'C'
            AND wdd.source_code = 'OE'
            AND wdd.oe_interfaced_flag IN ('Y')
            AND NVL(wdd.shipped_quantity, 0) > 0)
AND NOT EXISTS(SELECT 1
                 FROM wsh_delivery_details wdd
                WHERE wdd.source_line_id = oel.line_id
                  AND wdd.source_code = 'OE'
                  AND wdd.source_code = 'C'
                  AND NVL(wdd.oe_interfaced_flag,'N') = 'N')
AND    EXISTS ( SELECT 1 FROM wf_process_activities p, wf_item_activity_statuses s
		WHERE p.instance_id	= s.process_activity
		AND   item_type		= 'OEOL'
		AND   item_key		= to_char(oel.line_id)
		AND   p.activity_name	IN( 'SHIP_CONFIRM' )
		AND   s.activity_status  in('NOTIFIED', 'ERROR')
                AND   s.end_date IS NULL);


BEGIN

  Oe_debug_pub.debug_ON;
  Oe_debug_pub.initialize;
  Oe_debug_pub.setdebuglevel(5);
  l_file_name := Oe_debug_pub.set_debug_mode('FILE');
  Dbms_Output.put_line('DEBUG FILE IS located AT :' ||  l_file_name);


    oe_debug_pub.add('Processing line info ....');

     FOR c IN lines LOOP

     oe_debug_pub.add('Seting Context for line ' || c.line_id);

      Oe_standard_wf.OEOL_Selector(p_itemtype => 'OEOL',
                                   p_itemkey => to_char(c.line_id),
                                   p_actid => 12345,
                                   p_funcmode => 'SET_CTX',
                                   p_result => l_result);

      oe_debug_pub.add('Result: '||l_result );
      oe_debug_pub.add('Calling handleerror for Line...');

      wf_engine.HandleError('OEOL', to_char(c.line_id), 'SHIP_LINE', 'SKIP', 'SHIP_CONFIRM');
      END LOOP;

    Oe_debug_pub.debug_OFF;


Exception
  When Others Then
    dbms_output.put_line(' Error in the base script : '||sqlerrm);
    oe_debug_pub.add('### Error in the base script : '||sqlerrm);
    Oe_debug_pub.debug_OFF;
End;
/
spool OFF;
COMMIT;
/