-- 20180716-00107 AH Harris Items Conversions - POST CHECKS
-- **** SVN check in � Nancy Pahwa � 10/8/2018 / 11.06am TMS 20181008-00001 *****
-- FD Ticket : 13482 https://hdsupply.freshdesk.com/a/tickets/13482
-- TMS : http://gwcint1awps.hsi.hughessupply.com:8001/api/Tasks/Index?taskId=76492
--set serveroutput on unlock stats
DECLARE
  l_schema        VARCHAR2(30);
  l_schema_status VARCHAR2(1);
  l_industry      VARCHAR2(1);

BEGIN

  dbms_stats.unlock_table_stats('EGO'
                               ,'EGO_ITM_USR_ATTR_INTRFC');
  dbms_stats.unlock_table_stats('INV'
                               ,'MTL_SYSTEM_ITEMS_INTERFACE');
  dbms_stats.unlock_table_stats('INV'
                               ,'MTL_ITEM_REVISIONS_INTERFACE');
  dbms_stats.unlock_table_stats('INV'
                               ,'MTL_ITEM_CATEGORIES_INTERFACE');

  dbms_output.put_line('... Unlock Stats on Interface tables');

  dbms_output.put_line('... <Ended');
EXCEPTION
  WHEN OTHERS THEN
    dbms_output.put_line('... Error Occurred: ' || SQLERRM);
  
END;
/