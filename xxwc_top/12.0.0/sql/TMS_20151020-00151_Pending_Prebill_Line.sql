/*
 TMS: 20151020-00151     
 Date: 10/20/2015
 Notes: data fix script to process Pending Pre-bill line
*/

SET SERVEROUTPUT ON SIZE 100000;
SET VERIFY OFF;


update apps.oe_order_lines_all
set flow_status_Code='CLOSED',
INVOICE_INTERFACE_STATUS_CODE='YES',
INVOICED_QUANTITY='1',
OPEN_FLAG='N'
where LINE_ID=21769836
and header_id=13126616;
		  
--1 row expected to be updated

commit;

/