/*
 TMS: Task ID: 20180529-00093 Edison NJ - GL Hitting Omaha 101
 */
SET SERVEROUTPUT ON SIZE 1000000


BEGIN
  dbms_output.put_line('Before update');

  UPDATE mtl_parameters
     SET average_cost_var_account = 1781163 --0W.BW109.0000.501010.00000.00000.00000
   WHERE organization_id = 1570;

  dbms_output.put_line('Records Mtl_Parameters Update-' || SQL%ROWCOUNT);

  UPDATE cst_cost_group_accounts
     SET average_cost_var_account = 1781163
   WHERE cost_group_id = 83026
     AND organization_id = 1570; --for branch 109

  dbms_output.put_line('Records Cst_Cost_Group_Accounts Update-' || SQL%ROWCOUNT);

  COMMIT;
EXCEPTION
  WHEN OTHERS THEN
    dbms_output.put_line('Unable to Update... ' || SQLERRM);
END;
/
