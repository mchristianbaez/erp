-- 20180716-00106 (AH Harris Items Conversions - PRE CHECKS)
set serveroutput ON
DECLARE
  l_schema VARCHAR2(30);
  l_schema_status VARCHAR2(1);
  l_industry VARCHAR2(1);
  l_backupsuffix VARCHAR2(10) := to_char(SYSDATE,'ddhh24miss');

BEGIN
  dbms_output.put_line('... Started');
  
  EXECUTE IMMEDIATE 'GRANT SELECT, INSERT, UPDATE, DELETE ON INV.MTL_SYSTEM_ITEMS_INTERFACE TO GP050872';
  EXECUTE IMMEDIATE 'GRANT SELECT, INSERT, UPDATE, DELETE ON INV.MTL_INTERFACE_ERRORS TO GP050872';
  EXECUTE IMMEDIATE 'GRANT SELECT, INSERT, UPDATE, DELETE ON INV.MTL_ITEM_REVISIONS_INTERFACE TO GP050872';
  EXECUTE IMMEDIATE 'GRANT SELECT, INSERT, UPDATE, DELETE ON INV.MTL_ITEM_CATEGORIES_INTERFACE TO GP050872';
  EXECUTE IMMEDIATE 'GRANT SELECT, INSERT, UPDATE, DELETE ON EGO.EGO_ITM_USR_ATTR_INTRFC TO GP050872';
  dbms_output.put_line('... Grant Priveleges from Interface Tables - Done');
  
  EXECUTE IMMEDIATE 'GRANT SELECT, INSERT, UPDATE, DELETE ON XXWC.XXWC_INVITEM_XREF_REF TO GP050872';
  EXECUTE IMMEDIATE 'GRANT SELECT, INSERT, UPDATE, DELETE ON XXWC.XXWC_INVITEM_XREF_STG TO GP050872';
  EXECUTE IMMEDIATE 'GRANT SELECT, INSERT, UPDATE, DELETE ON XXWC.XXWC_INVITEM_STG TO GP050872';
  EXECUTE IMMEDIATE 'GRANT SELECT, INSERT, UPDATE, DELETE ON XXWC.XXWC_ITEM_MASTER_REF TO GP050872';
  EXECUTE IMMEDIATE 'GRANT SELECT, INSERT, UPDATE, DELETE ON XXWC.XXWC_ORG_ITEM_REF TO GP050872';
  EXECUTE IMMEDIATE 'GRANT SELECT, INSERT, UPDATE, DELETE ON XXWC.XXWC_ONHAND_BAL_CNV TO GP050872';
  EXECUTE IMMEDIATE 'GRANT SELECT, INSERT, UPDATE, DELETE ON XXWC.XXWC_AHH_ITEMS_CONV_STG TO GP050872';
  EXECUTE IMMEDIATE 'GRANT SELECT, INSERT, UPDATE, DELETE ON XXWC.XXWC_AHH_ITEMS_ORG_CONV_STG TO GP050872';
  EXECUTE IMMEDIATE 'GRANT SELECT, INSERT, UPDATE, DELETE ON XXWC.XXWC_AHH_ITEMS_LOC_CONV_STG TO GP050872';
  EXECUTE IMMEDIATE 'GRANT SELECT, INSERT, UPDATE, DELETE ON XXWC.XXWC_AHH_ITEMS_CONV_STGARC TO GP050872';
  EXECUTE IMMEDIATE 'GRANT SELECT, INSERT, UPDATE, DELETE ON XXWC.XXWC_AHH_ITEMS_ORG_CONV_STGARC TO GP050872';
  EXECUTE IMMEDIATE 'GRANT SELECT, INSERT, UPDATE, DELETE ON XXWC.XXWC_MSII_ARCHIVE_ALL TO GP050872';
  EXECUTE IMMEDIATE 'GRANT SELECT, INSERT, UPDATE, DELETE ON XXWC.XXWC_MICI_ARCHIVE_ALL TO GP050872';
  EXECUTE IMMEDIATE 'GRANT SELECT, INSERT, UPDATE, DELETE ON XXWC.XXWC_MIE_ARCHIVE_ALL TO GP050872';
  EXECUTE IMMEDIATE 'GRANT SELECT, INSERT, UPDATE, DELETE ON XXWC.XXWC_MIRI_ARCHIVE_ALL TO GP050872';
  dbms_output.put_line('... Grant Priveleges from Custom Staging Tables - Done');

  EXECUTE IMMEDIATE 'GRANT SELECT, INSERT, UPDATE, DELETE ON INV.MTL_SYSTEM_ITEMS_INTERFACE TO RP058050';
  EXECUTE IMMEDIATE 'GRANT SELECT, INSERT, UPDATE, DELETE ON INV.MTL_INTERFACE_ERRORS TO RP058050';
  EXECUTE IMMEDIATE 'GRANT SELECT, INSERT, UPDATE, DELETE ON INV.MTL_ITEM_REVISIONS_INTERFACE TO RP058050';
  EXECUTE IMMEDIATE 'GRANT SELECT, INSERT, UPDATE, DELETE ON INV.MTL_ITEM_CATEGORIES_INTERFACE TO RP058050';
  EXECUTE IMMEDIATE 'GRANT SELECT, INSERT, UPDATE, DELETE ON EGO.EGO_ITM_USR_ATTR_INTRFC TO RP058050';
  dbms_output.put_line('... Grant Priveleges from Interface Tables - Done');
  
  EXECUTE IMMEDIATE 'GRANT SELECT, INSERT, UPDATE, DELETE ON XXWC.XXWC_INVITEM_XREF_REF TO RP058050';
  EXECUTE IMMEDIATE 'GRANT SELECT, INSERT, UPDATE, DELETE ON XXWC.XXWC_INVITEM_XREF_STG TO RP058050';
  EXECUTE IMMEDIATE 'GRANT SELECT, INSERT, UPDATE, DELETE ON XXWC.XXWC_INVITEM_STG TO RP058050';
  EXECUTE IMMEDIATE 'GRANT SELECT, INSERT, UPDATE, DELETE ON XXWC.XXWC_ITEM_MASTER_REF TO RP058050';
  EXECUTE IMMEDIATE 'GRANT SELECT, INSERT, UPDATE, DELETE ON XXWC.XXWC_ORG_ITEM_REF TO RP058050';
  EXECUTE IMMEDIATE 'GRANT SELECT, INSERT, UPDATE, DELETE ON XXWC.XXWC_ONHAND_BAL_CNV TO RP058050';
  EXECUTE IMMEDIATE 'GRANT SELECT, INSERT, UPDATE, DELETE ON XXWC.XXWC_AHH_ITEMS_CONV_STG TO RP058050';
  EXECUTE IMMEDIATE 'GRANT SELECT, INSERT, UPDATE, DELETE ON XXWC.XXWC_AHH_ITEMS_ORG_CONV_STG TO RP058050';
  EXECUTE IMMEDIATE 'GRANT SELECT, INSERT, UPDATE, DELETE ON XXWC.XXWC_AHH_ITEMS_LOC_CONV_STG TO RP058050';
  EXECUTE IMMEDIATE 'GRANT SELECT, INSERT, UPDATE, DELETE ON XXWC.XXWC_AHH_ITEMS_CONV_STGARC TO RP058050';
  EXECUTE IMMEDIATE 'GRANT SELECT, INSERT, UPDATE, DELETE ON XXWC.XXWC_AHH_ITEMS_ORG_CONV_STGARC TO RP058050';
  EXECUTE IMMEDIATE 'GRANT SELECT, INSERT, UPDATE, DELETE ON XXWC.XXWC_MSII_ARCHIVE_ALL TO RP058050';
  EXECUTE IMMEDIATE 'GRANT SELECT, INSERT, UPDATE, DELETE ON XXWC.XXWC_MICI_ARCHIVE_ALL TO RP058050';
  EXECUTE IMMEDIATE 'GRANT SELECT, INSERT, UPDATE, DELETE ON XXWC.XXWC_MIE_ARCHIVE_ALL TO RP058050';
  EXECUTE IMMEDIATE 'GRANT SELECT, INSERT, UPDATE, DELETE ON XXWC.XXWC_MIRI_ARCHIVE_ALL TO RP058050';
  dbms_output.put_line('... Grant Priveleges from Custom Staging Tables - Done');

EXECUTE IMMEDIATE 'GRANT SELECT, INSERT, UPDATE, DELETE ON INV.MTL_SYSTEM_ITEMS_INTERFACE TO VP038429';
  EXECUTE IMMEDIATE 'GRANT SELECT, INSERT, UPDATE, DELETE ON INV.MTL_INTERFACE_ERRORS TO VP038429';
  EXECUTE IMMEDIATE 'GRANT SELECT, INSERT, UPDATE, DELETE ON INV.MTL_ITEM_REVISIONS_INTERFACE TO VP038429';
  EXECUTE IMMEDIATE 'GRANT SELECT, INSERT, UPDATE, DELETE ON INV.MTL_ITEM_CATEGORIES_INTERFACE TO VP038429';
  EXECUTE IMMEDIATE 'GRANT SELECT, INSERT, UPDATE, DELETE ON EGO.EGO_ITM_USR_ATTR_INTRFC TO VP038429';
  dbms_output.put_line('... Grant Priveleges from Interface Tables - Done');
  
  EXECUTE IMMEDIATE 'GRANT SELECT, INSERT, UPDATE, DELETE ON XXWC.XXWC_INVITEM_XREF_REF TO VP038429';
  EXECUTE IMMEDIATE 'GRANT SELECT, INSERT, UPDATE, DELETE ON XXWC.XXWC_INVITEM_XREF_STG TO VP038429';
  EXECUTE IMMEDIATE 'GRANT SELECT, INSERT, UPDATE, DELETE ON XXWC.XXWC_INVITEM_STG TO VP038429';
  EXECUTE IMMEDIATE 'GRANT SELECT, INSERT, UPDATE, DELETE ON XXWC.XXWC_ITEM_MASTER_REF TO VP038429';
  EXECUTE IMMEDIATE 'GRANT SELECT, INSERT, UPDATE, DELETE ON XXWC.XXWC_ORG_ITEM_REF TO VP038429';
  EXECUTE IMMEDIATE 'GRANT SELECT, INSERT, UPDATE, DELETE ON XXWC.XXWC_ONHAND_BAL_CNV TO VP038429';
  EXECUTE IMMEDIATE 'GRANT SELECT, INSERT, UPDATE, DELETE ON XXWC.XXWC_AHH_ITEMS_CONV_STG TO VP038429';
  EXECUTE IMMEDIATE 'GRANT SELECT, INSERT, UPDATE, DELETE ON XXWC.XXWC_AHH_ITEMS_ORG_CONV_STG TO VP038429';
  EXECUTE IMMEDIATE 'GRANT SELECT, INSERT, UPDATE, DELETE ON XXWC.XXWC_AHH_ITEMS_LOC_CONV_STG TO VP038429';
  EXECUTE IMMEDIATE 'GRANT SELECT, INSERT, UPDATE, DELETE ON XXWC.XXWC_AHH_ITEMS_CONV_STGARC TO VP038429';
  EXECUTE IMMEDIATE 'GRANT SELECT, INSERT, UPDATE, DELETE ON XXWC.XXWC_AHH_ITEMS_ORG_CONV_STGARC TO VP038429';
  EXECUTE IMMEDIATE 'GRANT SELECT, INSERT, UPDATE, DELETE ON XXWC.XXWC_MSII_ARCHIVE_ALL TO VP038429';
  EXECUTE IMMEDIATE 'GRANT SELECT, INSERT, UPDATE, DELETE ON XXWC.XXWC_MICI_ARCHIVE_ALL TO VP038429';
  EXECUTE IMMEDIATE 'GRANT SELECT, INSERT, UPDATE, DELETE ON XXWC.XXWC_MIE_ARCHIVE_ALL TO VP038429';
  EXECUTE IMMEDIATE 'GRANT SELECT, INSERT, UPDATE, DELETE ON XXWC.XXWC_MIRI_ARCHIVE_ALL TO VP038429';
  dbms_output.put_line('... Grant Priveleges from Custom Staging Tables - Done');
  dbms_output.put_line('... <Ended');
EXCEPTION
  WHEN OTHERS THEN
      dbms_output.put_line('... Error Occurred: '||sqlerrm);

END;
/