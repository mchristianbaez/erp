/*
 TMS: Task ID: 20180503-00021 Process stuck receiving transactions.
 */
SET SERVEROUTPUT ON SIZE 1000000


BEGIN
   DBMS_OUTPUT.put_line ('Before update');

   UPDATE rcv_transactions_interface
      SET request_id = NULL,
          processing_request_id = NULL,
          processing_status_code = 'PENDING',
          transaction_status_code = 'PENDING',
          processing_mode_code = 'BATCH',
          TRANSACTION_DATE = SYSDATE,
          last_update_date = SYSDATE,
          Last_updated_by = 16991
    WHERE     processing_status_code = 'PENDING'
          AND transaction_status_code = 'PENDING'
          AND processing_mode_code = 'ONLINE'
          AND po_header_id=4262690;


   DBMS_OUTPUT.put_line ('Records update-' || SQL%ROWCOUNT);
   COMMIT;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Unable to update ' || SQLERRM);
END;
/