/********************************************************************************
FILE NAME: AHH_INVITEM_CONV_ONDEMAND.sql

PROGRAM TYPE: DataFix or Gather Statistics Script

PURPOSE: Item Master Reference table

HISTORY
==========================================================================================
VERSION DATE          AUTHOR(S)        DESCRIPTION
------- -----------   ---------------- ----------------------------------------------------
1.0     07/18/2018    Naveen Kalidindi Initial Version, TMS 20180716-00110
*******************************************************************************************/
set serveroutput ON
WHEN SQLERROR CONTINUE

PROMPT ... Started
ALTER TRIGGER XXCUS_MTL_ITEMS_AUDIT_TRG DISABLE;
PROMPT ... Alter XXCUS_MTL_ITEMS_AUDIT_TRG Trigger - DONE

--table stats
PROMPT ... Gathering Stats Interface tables

exec fnd_stats.gather_table_stats('EGO','EGO_MTL_SY_ITEMS_EXT_B',30)
/
exec fnd_stats.gather_table_stats('EGO','EGO_MTL_SY_ITEMS_EXT_TL',30)
/
exec fnd_stats.gather_table_stats('EGO','EGO_ITM_USR_ATTR_INTRFC',30)
/
exec fnd_stats.gather_table_stats('INV','MTL_SYSTEM_ITEMS_INTERFACE',30);
/
exec fnd_stats.gather_table_stats('INV','MTL_ITEM_CATEGORIES_INTERFACE',30)
/
exec fnd_stats.gather_table_stats('INV','MTL_ITEM_REVISIONS_INTERFACE',30)
/

PROMPT ... Gather Stats Interface tables DONE 

--index stats
PROMPT ... Gathering Stats Interface table indexes 
EXEC fnd_stats.gather_index_stats('XXWC','XXWC_MTL_SYSTEM_ITEMS_INTF_N6',30)
/
EXEC fnd_stats.gather_index_stats('XXWC','XXWC_MTL_SYSTEM_ITEMS_INTF_N5',30)
/
EXEC fnd_stats.gather_index_stats('XXWC','XXWC_MTL_SYSTEM_ITEMS_INTF_N2',30)
/
EXEC fnd_stats.gather_index_stats('XXWC','XXWC_MTL_SYSTEM_ITEMS_INTF_N1',30)
/
EXEC fnd_stats.gather_index_stats('INV','MTL_SYSTEM_ITEMS_INTERFACE_N3',30)
/
EXEC fnd_stats.gather_index_stats('INV','MTL_SYSTEM_ITEMS_INTERFACE_U2',30)
/
EXEC fnd_stats.gather_index_stats('INV','MTL_SYSTEM_ITEMS_INTERFACE_N5',30)
/
EXEC fnd_stats.gather_index_stats('INV','MTL_SYSTEM_ITEMS_INTERFACE_N4',30)
/
EXEC fnd_stats.gather_index_stats('INV','MTL_SYSTEM_ITEMS_INTERFACE_N6',30)
/
EXEC fnd_stats.gather_index_stats('INV','MTL_SYSTEM_ITEMS_INTERFACE_N1',30)
/
EXEC fnd_stats.gather_index_stats('INV','MTL_SYSTEM_ITEMS_INTERFACE_N2',30)
/

PROMPT ... Gather Stats Interface table indexes - DONE

PROMPT ... <Ended
