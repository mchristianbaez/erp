-- 20181008-00002 Purchasing mass upload performance  Fix
-- **** SVN check in � Nancy Pahwa � 10/8/2018 / 4.14pm TMS 20181008-00002 *****
-- FD Ticket : 13547 https://hdsupply.freshdesk.com/a/tickets/13547� 
-- TMS : http://gwcint1awps.hsi.hughessupply.com:8001/api/Tasks/Index?taskId=76493
SET SERVEROUTPUT ON SIZE 1000000


BEGIN
   DBMS_OUTPUT.put_line ('Before truncate');

   TRUNCATE TABLE XXWC.XXWC_PURC_MASS_UPLOAD_T;
TRUNCATE TABLE XXWC.XXWC_PURC_MASS_UPLOAD_P;


   DBMS_OUTPUT.put_line ('Truncate Complete');

EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Unable to truncate ' || SQLERRM);
END;
/