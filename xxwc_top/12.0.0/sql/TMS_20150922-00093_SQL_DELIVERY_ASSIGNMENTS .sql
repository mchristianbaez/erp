/*
 TMS: 20150922-00093   
 Date: 09/21/2015
 Notes: data fix script to process month end transactions
*/

SET SERVEROUTPUT ON SIZE 100000;
SET VERIFY OFF;



update apps.wsh_delivery_assignments
set delivery_id = null,
parent_delivery_detail_id = null
where delivery_detail_id = 11994234;

--1 row expected to be updated

commit;

/