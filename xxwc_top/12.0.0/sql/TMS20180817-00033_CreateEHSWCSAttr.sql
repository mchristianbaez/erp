-- TMS# 20180817-00033 Create EHS WCS PDH Attribute for custom PDH table entries
set serveroutput on size 1000000
DECLARE
  l_row_identifier        NUMBER;
  l_attr_row_index        NUMBER := 1;
  l_attr_data_index       NUMBER := 1;
  l_attributes_row_table  ego_user_attr_row_table := ego_user_attr_row_table();
  l_attributes_data_table ego_user_attr_data_table := ego_user_attr_data_table();


  l_error_message_list error_handler.error_tbl_type;

  x_failed_row_id_list VARCHAR2(1000);
  x_return_status      VARCHAR2(100);
  x_msg_count          NUMBER;
  x_msg_data           VARCHAR2(10000);
  x_error_code         NUMBER;

  l_error_code  VARCHAR2(1) := 'S';
  l_err_msg     VARCHAR2(2000);
  l_err_message VARCHAR2(4000);

  l_success_count NUMBER := 0;
  l_err_count     NUMBER := 0;
  l_total_count   NUMBER := 0;
  --
  l_attr_group_name   VARCHAR2(100) := 'XXWC_EHS_AG';
  l_attr_group_type   VARCHAR2(100) := 'EGO_ITEMMGMT_GROUP';
  l_attr_group_app_id NUMBER := 431;
  l_data_level        VARCHAR2(100) := 'ITEM_LEVEL';

  l_mst_organization_id NUMBER := 222;

  l_attr_name1     VARCHAR2(100) := 'XXWC_PROP_65_ATTR'; --  WCS Attribute 
  l_attr_value1    VARCHAR2(100) := 'Y'; -- := 'Dummy';
  l_user_id        NUMBER := -1;
  l_resp_id        NUMBER := -1;
  l_application_id NUMBER := -1;
  l_rowcnt         NUMBER := 1;

  l_user_name VARCHAR2(20) := 'XXWC_INT_SUPPLYCHAIN';
  l_resp_name VARCHAR2(40) := 'HDS Product Data Hub Admin - WC';

  CURSOR c_select_items IS
    SELECT *
      FROM xxwc.xxwc_item_ehs_attributes;

  l_sec VARCHAR2(10);

BEGIN
  l_sec := '10';
  -- Get the user_id
  SELECT user_id
    INTO l_user_id
    FROM fnd_user
   WHERE user_name = l_user_name;

  l_sec := '20';
  -- Get the application_id and responsibility_id
  SELECT application_id
        ,responsibility_id
    INTO l_application_id
        ,l_resp_id
    FROM fnd_responsibility_vl
   WHERE responsibility_name = l_resp_name;

  l_sec := '30';
  fnd_global.apps_initialize(l_user_id
                            ,l_resp_id
                            ,l_application_id); -- MGRPLM / Development Manager / EGO
  dbms_output.put_line('Initialized applications context: ' || l_user_id || ' ' || l_resp_id || ' ' ||
                       l_application_id);

  l_sec := '40';
  FOR rec IN c_select_items LOOP
    BEGIN
      l_sec                   := '50';
      l_total_count           := l_total_count + 1;
      l_attr_row_index        := 0;
      l_attr_data_index       := 0;
      l_attributes_data_table := ego_user_attr_data_table();
      l_attributes_row_table  := ego_user_attr_row_table();
    
      -- Assign EHS DFF attributes --
      -- Add in the attribute groups here, multirow attribute groups should have the same attr_group_id
      -- transaction_type can be CREATE | UPDATE | SYNC
    
      l_sec := '60';
      l_attributes_row_table.extend;
      l_attr_row_index       := l_attributes_row_table.count;
      l_row_identifier       := l_attr_row_index;
      l_attributes_row_table := ego_user_attr_row_table(ego_user_attr_row_obj(l_row_identifier -- ROW_IDENTIFIER
                                                                             ,NULL -- ATTR_GROUP_ID from EGO_ATTR_GROUPS_V 
                                                                             ,l_attr_group_app_id -- ATTR_GROUP_APP_ID
                                                                             ,l_attr_group_type -- ATTR_GROUP_TYPE
                                                                             ,l_attr_group_name -- ATTR_GROUP_NAME
                                                                             ,l_data_level -- NDATA_LEVEL
                                                                             ,NULL -- DATA_LEVEL_1       (Required if attribute groups are at revision level)
                                                                             ,NULL -- DATA_LEVEL_2
                                                                             ,NULL -- DATA_LEVEL_3
                                                                             ,NULL -- DATA_LEVEL_4
                                                                             ,NULL -- DATA_LEVEL_5
                                                                             ,ego_user_attrs_data_pvt.g_sync_mode -- TRANSACTION_TYPE
                                                                              ));
    
      -- Add the attribute - attribute values here, for the attr groups above
      -- row_identifier is the foriegn key from the attribute group defined above
      -- user_row_identifier is used for error handling
    
      l_sec := '70';
      l_attributes_data_table.extend;
      l_attr_data_index := l_attributes_data_table.count;
      --
      l_sec := '80';
      l_attributes_data_table(l_attr_data_index) := ego_user_attr_data_obj(l_row_identifier -- ROW_IDENTIFIER from above
                                                                          ,l_attr_name1 -- ATTR_NAME
                                                                          ,NULL -- ATTR_VALUE_STR
                                                                          ,NULL -- ATTR_VALUE_NUM
                                                                          ,NULL -- ATTR_VALUE_DATE
                                                                          ,l_attr_value1 -- ATTR_DISP_VALUE
                                                                          ,NULL -- ATTR_UNIT_OF_MEASURE
                                                                          ,l_row_identifier -- USER_ROW_IDENTIFIER
                                                                           );
      l_sec := '90';
      --
      ego_item_pub.process_user_attrs_for_item(p_api_version           => '1.0'
                                              ,p_inventory_item_id     => rec.inventory_item_id
                                              ,p_organization_id       => l_mst_organization_id
                                              ,p_attributes_row_table  => l_attributes_row_table
                                              ,p_attributes_data_table => l_attributes_data_table
                                              ,x_failed_row_id_list    => x_failed_row_id_list
                                              ,x_return_status         => x_return_status
                                              ,x_errorcode             => x_error_code
                                              ,x_msg_count             => x_msg_count
                                              ,x_msg_data              => x_msg_data);
      --  
    
      IF x_return_status <> 'S' THEN
        l_sec := '100';
        error_handler.get_message_list(l_error_message_list);
        l_error_code := 'E';
      
        FOR i IN 1 .. l_error_message_list.count LOOP
          l_sec         := '110';
          l_err_msg     := NULL;
          l_err_msg     := l_error_message_list(i).message_text;
          l_err_message := l_err_message || l_err_msg;
        END LOOP;
        dbms_output.put_line(' * Item, API Error: ' || rec.item_number || ', sec: ' || l_sec ||
                             ' - ' || l_err_message);
        l_sec       := '120';
        l_err_count := l_err_count + 1;
      ELSE
        l_success_count := l_success_count + 1;
      END IF; --IF x_return_status <> 'S' THEN
    
      l_sec := '130';
      COMMIT;
    EXCEPTION
      WHEN OTHERS THEN
        ROLLBACK;
        l_err_count := l_err_count + 1;
        dbms_output.put_line(' * Item, Error: ' || rec.item_number || ', sec: ' || l_sec || ' - ' ||
                             substr(SQLERRM
                                   ,1
                                   ,100));
    END;
  
  END LOOP;
  --
  dbms_output.put_line(' Total Recs: ' || l_total_count);
  dbms_output.put_line(' Success Count: ' || l_success_count);
  dbms_output.put_line(' Error Count: ' || l_err_count);
  dbms_output.put_line(' Program Successfully completed');
  --
EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
    dbms_output.put_line('Unknown Error : ' || ', sec: ' || l_sec || ' - ' ||
                         substr(SQLERRM
                               ,1
                               ,100));
END;
/
