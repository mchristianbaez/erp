DECLARE
  l_line_id     NUMBER := 53809982;
  l_ordered_qty NUMBER;
  l_received_qty rcv_transactions.quantity%TYPE;
  CURSOR line_info
  IS
    SELECT line_id ,
      ordered_quantity ,
      ool.org_id ,
      rcv.quantity received_quantity --to pass in oe_rma_receiving.push_receiving_info in place of ordered qty
      --if the line is partially shipped and not split due to corruption
    FROM oe_order_lines_all ool ,
      mtl_material_transactions mmt ,
      rcv_transactions rcv
    WHERE ool.line_id          = l_line_id
    AND ool.flow_status_code   = 'AWAITING_RETURN'
    AND mmt.trx_source_line_id = ool.line_id
    AND mmt.transaction_type_id= 15
    AND rcv.oe_order_line_id   = ool.line_id
    AND mmt.rcv_transaction_id = rcv.transaction_id
    AND rcv.transaction_type   = 'DELIVER' FOR UPDATE NOWAIT;
  
  l_user_id       NUMBER;
  l_resp_id       NUMBER;
  l_appl_id       NUMBER;
  x_return_status VARCHAR2(10);
  x_msg_count     NUMBER;
  x_msg_data      VARCHAR2(2000);
  l_org_id        NUMBER;
BEGIN
  IF NVL(l_line_id, 0) > 0 THEN
    OPEN line_info;
    FETCH line_info INTO l_line_id, l_ordered_qty,l_org_id, l_received_qty;
    IF line_info%notfound THEN
      CLOSE line_info;
      dbms_output.put_line('Error: Invalid Line Id, Re-enter.');
      RETURN;
    END IF;
    CLOSE line_info;
  ELSE
    RETURN;
  END IF;

    BEGIN
      SELECT number_value
      INTO l_user_id
      FROM wf_item_attribute_values
      WHERE item_type = 'OEOL'
      AND item_key    = l_line_id
      AND name        = 'USER_ID';
    EXCEPTION
    WHEN OTHERS THEN
      dbms_output.put_line('Error: Unable to fetch the USER_ID l_line_id not exist.'||l_line_id);
	 END;
      BEGIN
        SELECT number_value
        INTO l_resp_id
        FROM wf_item_attribute_values
        WHERE item_type = 'OEOL'
        AND item_key    = l_line_id
        AND name        = 'RESPONSIBILITY_ID';
      EXCEPTION
      WHEN OTHERS THEN
        dbms_output.put_line('Error: Unable to fetch the RESPONSIBILITY_ID l_line_id not exist.'||l_line_id);
      END;
      BEGIN
        SELECT number_value
        INTO l_appl_id
        FROM wf_item_attribute_values
        WHERE item_type = 'OEOL'
        AND item_key    = l_line_id
        AND name        = 'APPLICATION_ID';
      EXCEPTION
      WHEN No_Data_Found THEN
        dbms_output.put_line('Error: Line flow does not exist.'||l_line_id);
        RETURN;
      END;
      mo_global.init('ONT');
      mo_global.set_policy_context('S',l_org_id);
      fnd_global.apps_initialize(l_user_id, l_resp_id, l_appl_id);
      BEGIN
        UPDATE oe_order_lines
        SET fulfilled_quantity = NULL ,
          shipped_quantity     = NULL ,
          last_updated_by      = -99999999 ,
          last_update_date     = sysdate
        WHERE line_id          = l_line_id;
      EXCEPTION
      WHEN OTHERS THEN
        dbms_output.put_line('Error: Unable to update the line id l_line_id not exist.'||l_line_id);
      END;
      BEGIN
        oe_rma_receiving.push_receiving_info( l_line_id , l_received_qty --removed l_ordered_qty to consider partially shipped qty if the line is not split already
        , 'NO PARENT' , 'RECEIVE' , 'N' , x_return_status , x_msg_count , x_msg_data );
        IF x_return_status = 'S' THEN
          oe_rma_receiving.push_receiving_info( l_line_id , l_received_qty --removed l_ordered_qty to consider partially shipped qty if the line is not split already
          , 'RECEIVE' , 'DELIVER' , 'N' , x_return_status , x_msg_count , x_msg_data );
        END IF;
        oe_debug_pub.add('no. of OE messages :'||x_msg_count,1);
        dbms_output.put_line('no. of OE messages :'||x_msg_count);
        FOR k IN 1 .. x_msg_count
        LOOP
          x_msg_data := oe_msg_pub.get( p_msg_index => k, p_encoded => 'F');
          oe_debug_pub.add(SUBSTR(x_msg_data,1,255));
          oe_debug_pub.add(SUBSTR(x_msg_data,255,LENGTH(x_msg_data)));
          dbms_output.put_line('Error msg: '||SUBSTR(x_msg_data,1,200));
        END LOOP;
        fnd_msg_pub.count_and_get( p_encoded => 'F' , p_count => x_msg_count , p_data => x_msg_data);
        oe_debug_pub.add('no. of FND messages :'||x_msg_count,1);
        dbms_output.put_line('no. of FND messages :'||x_msg_count);
        FOR k IN 1 .. x_msg_count
        LOOP
          x_msg_data := fnd_msg_pub.get( p_msg_index => k, p_encoded => 'F');
          dbms_output.put_line('Error msg: '||SUBSTR(x_msg_data,1,200));
          oe_debug_pub.add(SUBSTR(x_msg_data,1,255));
        END LOOP;
        IF x_return_status <> 'S' THEN
          oe_debug_pub.add('Error occurred, rolling back changes.',1);
          dbms_output.put_line('Error occurred, please fix the errors and retry.');
          ROLLBACK;
        ELSE
          COMMIT;
        END IF;
      END;
      dbms_output.put_line('For details, see OM Debug File: '||OE_DEBUG_PUB.G_DIR||'/'||OE_DEBUG_PUB.G_FILE);
    END;
    /