CREATE TABLE xxwc.xxwc_ar_new_acct_tbl
(  record_id               NUMBER
 , new_party_flag          VARCHAR2 (1)
 , party_id                NUMBER
 , party_name              VARCHAR2 (240)
 , customer_name           VARCHAR2 (240)
 , account_number          VARCHAR2 (30)
 , telephone_number        VARCHAR2 (30)
 , fax_number              VARCHAR2 (30)
 , salesrep_id             NUMBER
 , customer_since          DATE
 , classification          VARCHAR2 (100)
 , customer_source         VARCHAR2 (30)
 , prism_account_number    VARCHAR2 (30)
 , profile_class_id        NUMBER
 , credit_manager_id       NUMBER
 , collector_id            NUMBER
 , credit_classification   VARCHAR2 (30)
 , review_cycle            VARCHAR2 (30)
 , account_status          VARCHAR2 (30)
 , inv_remit_to            VARCHAR2 (10)
 , statement_by_job        VARCHAR2 (3)
 , mand_po_number          VARCHAR2 (3)
 , bill_to_address1        VARCHAR2 (100)
 , bill_to_address2        VARCHAR2 (100)
 , bill_to_address3        VARCHAR2 (100)
 , bill_to_address4        VARCHAR2 (100)
 , bill_to_city            VARCHAR2 (100)
 , bill_to_county          VARCHAR2 (100)
 , bill_to_state           VARCHAR2 (100)
 , bill_to_postal_code     VARCHAR2 (100)
 , bill_to_geocode         VARCHAR2 (100)
 , ship_to_same_flag       VARCHAR2 (1)
 , ship_to_address1        VARCHAR2 (100)
 , ship_to_address2        VARCHAR2 (100)
 , ship_to_address3        VARCHAR2 (100)
 , ship_to_address4        VARCHAR2 (100)
 , ship_to_city            VARCHAR2 (100)
 , ship_to_county          VARCHAR2 (100)
 , ship_to_state           VARCHAR2 (100)
 , ship_to_postal_code     VARCHAR2 (100)
 , ship_to_geocode         VARCHAR2 (100)
 , prism_yard_site_num     VARCHAR2 (30)
 , yard_credit_limit       NUMBER
 , process_flag            varchar2(1)
 , process_message         varchar2(2000)
 , creation_date           DATE
 , created_by              NUMBER
 , last_update_date        DATE
 , last_updated_by         NUMBER
 , last_update_login       NUMBER
)
/

create unique index xxwc.xxwc_ar_new_acct_u1
on xxwc.xxwc_ar_new_acct_tbl(record_id)
/

create or replace synonym xxwc_ar_new_acct_tbl for xxwc.xxwc_ar_new_acct_tbl
/

CREATE TABLE xxwc.xxwc_ar_new_acct_authbuyer_tbl
(  record_id               NUMBER
 , first_name              VARCHAR2(100)
 , last_name               VARCHAR2(100)
 , process_flag            varchar2(1)
 , process_message         varchar2(2000)
 , creation_date           DATE
 , created_by              NUMBER
 , last_update_date        DATE
 , last_updated_by         NUMBER
 , last_update_login       NUMBER
)
/

create index xxwc.xxwc_ar_new_acct_authbuyer_n1
on xxwc.xxwc_ar_new_acct_authbuyer_tbl(record_id)
/

create or replace synonym xxwc_ar_new_acct_authbuyer_tbl for xxwc.xxwc_ar_new_acct_authbuyer_tbl
/

create sequence xxwc.xxwc_ar_new_acct_s
/

create or replace synonym xxwc_ar_new_acct_s for xxwc.xxwc_ar_new_acct_s
/


CREATE TABLE xxwc.xxwc_ar_cm_remitcode_tbl
(  area_code               VARCHAR2(10)
 , credit_analyst_id       NUMBER
 , INV_REMIT_TO_CODE       varchar2(10)
 , creation_date           DATE
 , created_by              NUMBER
 , last_update_date        DATE
 , last_updated_by         NUMBER
 , last_update_login       NUMBER
)
/

create or replace synonym xxwc_ar_cm_remitcode_tbl for xxwc.xxwc_ar_cm_remitcode_tbl
/
