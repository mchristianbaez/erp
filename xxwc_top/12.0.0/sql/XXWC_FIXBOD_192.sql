-- TMS#  20180813-00081 (Data fix to resolve incorrect transfer BOD issue for AHH pilot branches)
set serveroutput on size 1000000
DECLARE
  -- 
  CURSOR c IS
    SELECT msib.segment1
          ,(SELECT organization_code
              FROM inv.mtl_parameters
             WHERE organization_id = t.organization_id) org_code
          ,t.*
      FROM inv.mtl_material_transactions t
          ,inv.mtl_system_items_b        msib
     WHERE t.organization_id = (SELECT organization_id
                                  FROM inv.mtl_parameters
                                 WHERE organization_code = '192')
          --AND msib.segment1 = '12115337'
          --AND msib.inventory_item_id = 12163056
       AND msib.organization_id = t.organization_id
       AND msib.inventory_item_id = t.inventory_item_id
       AND t.creation_date BETWEEN to_date('21-JUN-2018 04:00:00'
                                          ,'DD-MON-YYYY HH24:MI:SS') AND
           to_date('30-JUL-2018 21:30:00'
                  ,'DD-MON-YYYY HH24:MI:SS')
       AND t.transaction_type_id = 62
       AND EXISTS (SELECT 1
              FROM inv.mtl_material_transactions t2
             WHERE t2.organization_id = t.organization_id
               AND t2.inventory_item_id = t.inventory_item_id
               AND t2.source_code = 'CONVERSION'
               AND t2.transaction_type_id = 41)
     ORDER BY t.organization_id
             ,t.inventory_item_id
             ,t.creation_date ASC;

  CURSOR get_src(p_organization_id NUMBER
                ,p_inv_item        NUMBER) IS
    SELECT organization_id
          ,inventory_item_id
          ,transaction_id
          ,transaction_quantity total_qty
          ,to_date(attribute15
                  ,'DD-MON-RR') orig_rcv_date
      FROM inv.mtl_material_transactions t2
     WHERE t2.organization_id = p_organization_id
       AND t2.inventory_item_id = p_inv_item
       AND t2.source_code = 'CONVERSION'
       AND t2.transaction_type_id = 41
     ORDER BY organization_id
             ,inventory_item_id
             ,total_qty         ASC
             ,orig_rcv_date     ASC;

  rec_src get_src%ROWTYPE;
  TYPE tbl_src IS TABLE OF get_src%ROWTYPE;
  lt_src tbl_src;

  TYPE rec_onhand_list IS RECORD(
     create_transaction_id  NUMBER
    ,date_received          DATE
    ,orig_inventory_item_id NUMBER
    ,orig_organization_id   NUMBER);

  TYPE tbl_onhand_list IS TABLE OF rec_onhand_list;

  lt_oh_list             tbl_onhand_list;
  l_item_switch          VARCHAR2(100) := 'XXXX';
  lcreate_transaction_id NUMBER;
  lcreateor_id           NUMBER;
  lxfer_transaction_id   NUMBER;
  l_check_ttl            NUMBER := 0;
  l_assgn                VARCHAR2(1) := 'N';
  l_orig_rcv_date        DATE;
  lohqty                 NUMBER := 0;
  l_one_to_one           VARCHAR2(1) := '1';
  lcreateor_code         VARCHAR2(10);
BEGIN

  -- take backup
  EXECUTE IMMEDIATE 'CREATE TABLE xxwc.xxwc_mtl_oh_qty_dtl_1921 AS SELECT * FROM inv.mtl_onhand_quantities_detail';

  lt_src     := tbl_src();
  lt_oh_list := tbl_onhand_list();
  -- Check the qty received from.
  -- Date asc, Qty asc
  dbms_output.put_line('organization_id' || ',' || 'organization_code' || ',' || 'segment1' || ',' ||
                       'inventory_item_id' || ',' || 'xfer_from_transaction_id' || ',' ||
                       'xfer_from_transaction_quantity' || ',' || 'xfer_to__transaction_id' || ',' ||
                       'xfer_to_orgid' || ',' || 'xfer_to_orgcode' || ',' || 'Conv_Transaction' || ',' ||
                       'Conv_Transaction_Qty' || ',' || 'Received_Date' || ',' || 'OnhandQty,' ||
                       'Category');

  FOR rec IN c LOOP
    l_check_ttl := 0;
    l_assgn     := 'N';
    --lt_oh_list.delete;
    l_one_to_one := '1';
    IF l_item_switch <> rec.organization_id || '-' || rec.inventory_item_id THEN
    
      l_item_switch := rec.organization_id || '-' || rec.inventory_item_id;
    
      lt_src.delete;
    
      OPEN get_src(rec.organization_id
                  ,rec.inventory_item_id);
      FETCH get_src BULK COLLECT
        INTO lt_src;
      CLOSE get_src;
    
      --      dbms_output.put_line('----- ' || lt_src.count);
    
    END IF;
    --
    --    dbms_output.put_line(' trx Qty : ' || rec.transaction_quantity);
    --  
    FOR i IN 1 .. lt_src.count LOOP
      --      dbms_output.put_line(' = trx_id,initial qty : ' || lt_src(i).transaction_id || ',' || lt_src(i).total_qty);
      --
      IF lt_src.exists(i) THEN
        --
        IF abs(rec.transaction_quantity) = abs(lt_src(i).total_qty) THEN
          l_one_to_one := '2';
          --
          BEGIN
            lcreate_transaction_id := NULL;
            --
            SELECT transaction_id
                  ,organization_id
                  ,(SELECT organization_code
                     FROM mtl_parameters
                    WHERE organization_id = mmt.organization_id)
              INTO lcreate_transaction_id
                  ,lcreateor_id
                  ,lcreateor_code
              FROM inv.mtl_material_transactions mmt
             WHERE transfer_transaction_id = rec.transaction_id
               AND transaction_type_id = 61;
            --
            -- dbms_output.put_line(' = lcreate_transaction_id : ' || lcreate_transaction_id);
          EXCEPTION
            WHEN no_data_found THEN
              lcreate_transaction_id := NULL;
              --              dbms_output.put_line(' -* No 61');
          END;
          --
          BEGIN
            lxfer_transaction_id := NULL;
            --
            SELECT transaction_id
              INTO lxfer_transaction_id
              FROM xxwc.xxwcinv_interco_transfers xfer
             WHERE xfer.transaction_id = rec.transaction_id;
            --
            --            dbms_output.put_line(' = lxfer_transaction_id : ' || lxfer_transaction_id);
          EXCEPTION
            WHEN no_data_found THEN
              lxfer_transaction_id := NULL;
              --              dbms_output.put_line(' -* = Custom Table NOT Exists');
          END;
        
          -- 
          IF lcreate_transaction_id IS NOT NULL THEN
            --
            lt_oh_list.extend;
            lt_oh_list(lt_oh_list.count).create_transaction_id := lcreate_transaction_id;
            lt_oh_list(lt_oh_list.count).date_received := lt_src(i).orig_rcv_date;
            lt_oh_list(lt_oh_list.count).orig_inventory_item_id := lt_src(i).inventory_item_id;
            lt_oh_list(lt_oh_list.count).orig_organization_id := lt_src(i).organization_id;
            --
            --            dbms_output.put_line(' = assigned qty : ' || lt_src(i).total_qty);
            --
            SELECT COUNT(*)
              INTO lohqty
              FROM inv.mtl_onhand_quantities_detail
             WHERE create_transaction_id = lcreate_transaction_id;
          
            dbms_output.put_line(rec.organization_id || ',' || rec.org_code || ',' || rec.segment1 || ',' ||
                                 rec.inventory_item_id || ',' || rec.transaction_id || ',' ||
                                 rec.transaction_quantity || ',' || lcreate_transaction_id || ',' ||
                                 lcreateor_id || ',' || lcreateor_code || ',' || lt_src(i)
                                 .transaction_id || ',' || lt_src(i).total_qty || ',' || lt_src(i)
                                 .orig_rcv_date || ',' || lohqty || ',' || 'One-to-One');
            lt_src.delete(i);
            --
            l_assgn := 'Y';
            EXIT;
            --
          END IF;
        END IF;
      END IF;
    END LOOP;
  
    IF l_one_to_one = '1' THEN
      FOR i IN 1 .. lt_src.count LOOP
        --      dbms_output.put_line(' < trx_id,initial qty : ' || lt_src(i).transaction_id || ',' || lt_src(i)                           .total_qty);
        --
        IF lt_src.exists(i) THEN
          --
          IF abs(rec.transaction_quantity) < abs(lt_src(i).total_qty) THEN
            --
            BEGIN
              lcreate_transaction_id := NULL;
              --
              SELECT transaction_id
                    ,organization_id
                    ,(SELECT organization_code
                       FROM mtl_parameters
                      WHERE organization_id = mmt.organization_id)
                INTO lcreate_transaction_id
                    ,lcreateor_id
                    ,lcreateor_code
                FROM inv.mtl_material_transactions mmt
               WHERE transfer_transaction_id = rec.transaction_id
                 AND transaction_type_id = 61;
              --
              --            dbms_output.put_line(' < create_transaction_id,org_id : ' || lcreate_transaction_id || ',' ||                                 lcreateor_id);
            EXCEPTION
              WHEN no_data_found THEN
                lcreate_transaction_id := NULL;
                --              dbms_output.put_line(' -* No 61');
            END;
            --
            BEGIN
              lxfer_transaction_id := NULL;
              --
              SELECT transaction_id
                INTO lxfer_transaction_id
                FROM xxwc.xxwcinv_interco_transfers xfer
               WHERE xfer.transaction_id = rec.transaction_id;
              --
              --            dbms_output.put_line(' < lxfer_transaction_id : ' || lxfer_transaction_id);
            EXCEPTION
              WHEN no_data_found THEN
                lxfer_transaction_id := NULL;
                --              dbms_output.put_line(' -*< Custom Table NOT Exists');
            END;
          
            -- 
            IF lcreate_transaction_id IS NOT NULL THEN
              --
              lt_oh_list.extend;
              lt_oh_list(lt_oh_list.count).create_transaction_id := lcreate_transaction_id;
              lt_oh_list(lt_oh_list.count).date_received := lt_src(i).orig_rcv_date;
              lt_oh_list(lt_oh_list.count).orig_inventory_item_id := lt_src(i).inventory_item_id;
              lt_oh_list(lt_oh_list.count).orig_organization_id := lt_src(i).organization_id;
            
              SELECT COUNT(*)
                INTO lohqty
                FROM inv.mtl_onhand_quantities_detail
               WHERE create_transaction_id = lcreate_transaction_id;
              --
              dbms_output.put_line(rec.organization_id || ',' || rec.org_code || ',' ||
                                   rec.segment1 || ',' || rec.inventory_item_id || ',' ||
                                   rec.transaction_id || ',' || rec.transaction_quantity || ',' ||
                                   lcreate_transaction_id || ',' || lcreateor_id || ',' ||
                                   lcreateor_code || ',' || lt_src(i).transaction_id || ',' || lt_src(i)
                                   .total_qty || ',' || lt_src(i).orig_rcv_date || ',' || lohqty || ',' ||
                                   'One-to-Less');
              --
              lt_src(i).total_qty := abs(lt_src(i).total_qty) - abs(rec.transaction_quantity);
              --            dbms_output.put_line(' <assigned qty remaining: ' || lt_src(i).total_qty); --
              l_assgn := 'Y';
              EXIT;
              --
            END IF;
          
          END IF;
          --
        END IF;
        --
      END LOOP;
    
    END IF;
    --
    IF l_assgn = 'NXXXX' THEN
      l_check_ttl     := 0;
      l_orig_rcv_date := NULL;
      FOR i IN 1 .. lt_src.count LOOP
        IF lt_src.exists(i) THEN
          l_check_ttl := l_check_ttl + lt_src(i).total_qty;
          --          dbms_output.put_line(' l_check_ttl : ' || l_check_ttl);
          IF nvl(l_orig_rcv_date
                ,SYSDATE - 700) < lt_src(i).orig_rcv_date THEN
            l_orig_rcv_date := lt_src(i).orig_rcv_date;
            --            dbms_output.put_line(' l_orig_rcv_date : ' || l_orig_rcv_date);
          END IF;
        END IF;
      
      END LOOP;
    
      IF abs(rec.transaction_quantity) = l_check_ttl THEN
        --
        BEGIN
          lcreate_transaction_id := NULL;
          --
          SELECT transaction_id
                ,organization_id
                ,(SELECT organization_code
                   FROM mtl_parameters
                  WHERE organization_id = mmt.organization_id)
            INTO lcreate_transaction_id
                ,lcreateor_id
                ,lcreateor_code
            FROM inv.mtl_material_transactions mmt
           WHERE transfer_transaction_id = rec.transaction_id
             AND transaction_type_id = 61;
          --
          --          dbms_output.put_line(' <Total create_transaction_id,org_id : ' || lcreate_transaction_id || ',' ||                               lcreateor_id);
        EXCEPTION
          WHEN no_data_found THEN
            lcreate_transaction_id := NULL;
            --            dbms_output.put_line(' -* No 61');
        END;
        -- 
        IF lcreate_transaction_id IS NOT NULL
           AND l_orig_rcv_date IS NOT NULL THEN
          --
          lt_oh_list.extend;
          lt_oh_list(lt_oh_list.count).create_transaction_id := lcreate_transaction_id;
          lt_oh_list(lt_oh_list.count).date_received := l_orig_rcv_date;
          lt_oh_list(lt_oh_list.count).orig_inventory_item_id := rec.inventory_item_id;
          lt_oh_list(lt_oh_list.count).orig_organization_id := rec.organization_id;
        
          --
          SELECT COUNT(*)
            INTO lohqty
            FROM inv.mtl_onhand_quantities_detail
           WHERE create_transaction_id = lcreate_transaction_id;
          --
          dbms_output.put_line(rec.organization_id || ',' || rec.org_code || ',' || rec.segment1 || ',' ||
                               rec.inventory_item_id || ',' || rec.transaction_id || ',' ||
                               rec.transaction_quantity || ',' || lcreate_transaction_id || ',' ||
                               lcreateor_id || ',' || lcreateor_code || ',' || 'Greater than 1-' ||
                               lt_src.count || ',' || l_check_ttl || ',' || l_orig_rcv_date || ',' ||
                               lohqty || ',' || 'Club Match');
          --
          --          dbms_output.put_line(' <assigned Total qty remaining: ' || l_check_ttl);
          --
          --
        END IF;
      
      ELSE
        --        dbms_output.put_line(' No Available transaction id');
        dbms_output.put_line(rec.organization_id || ',' || rec.org_code || ',' || rec.segment1 || ',' ||
                             rec.inventory_item_id || ',' || rec.transaction_id || ',' ||
                             rec.transaction_quantity || ',' || NULL || ',' || NULL || ',' ||
                             'Greater than 1-' || lt_src.count || ',' || NULL || ',' || NULL || ',' || '0' || ',' ||
                             'Exception');
        --
      END IF;
    END IF;
  END LOOP;
  dbms_output.put_line('**********************************');

  --  dbms_output.put_line(lt_oh_list.count);
  DECLARE
    gg                NUMBER;
    dest_org_id       NUMBER;
    dest_inventory_id NUMBER;
  BEGIN
    dbms_output.put_line('orig_organization_id' || ',' || 'orig_inventory_item_id' || ',' ||
                         'create_transaction_id' || ',Exists,' || 'date_received' || ',' ||
                         'trxQty' || ',' || 'dest_org_id' || ',' || 'dest_inventory_id');
    FOR q IN 1 .. lt_oh_list.count LOOP
      gg                := NULL;
      dest_org_id       := NULL;
      dest_inventory_id := NULL;
      BEGIN
        --
        SELECT transaction_quantity
              ,organization_id
              ,inventory_item_id
          INTO gg
              ,dest_org_id
              ,dest_inventory_id
          FROM inv.mtl_onhand_quantities_detail
         WHERE create_transaction_id = lt_oh_list(q).create_transaction_id;
      
        UPDATE inv.mtl_onhand_quantities_detail
          SET orig_date_received = lt_oh_list(q).date_received
        WHERE create_transaction_id = lt_oh_list(q).create_transaction_id;
      
        dbms_output.put_line(lt_oh_list(q)
                             .orig_organization_id || ',' || lt_oh_list(q).orig_inventory_item_id || ',' || lt_oh_list(q)
                             .create_transaction_id || ',Exists,' || lt_oh_list(q).date_received || ',' || gg || ',' ||
                              dest_org_id || ',' || dest_inventory_id);
        --
      EXCEPTION
        WHEN too_many_rows THEN
          --          dbms_output.put_line('     --> Multiple Records Exist...');
        
          -- Find the transaction to transaction
          DECLARE
            CURSOR c_onhand_list IS
              SELECT ROWID onhand_quantities_id
                    ,transaction_quantity
                FROM inv.mtl_onhand_quantities_detail
               WHERE create_transaction_id = lt_oh_list(q).create_transaction_id;
          
            l_src_oh_list      tbl_src;
            lgg                NUMBER;
            ldest_org_id       NUMBER;
            ldest_inventory_id NUMBER;
            l_rowexist         NUMBER := 0;
          BEGIN
          
            OPEN get_src(lt_oh_list(q).orig_organization_id
                        ,lt_oh_list(q).orig_inventory_item_id);
          
            FETCH get_src BULK COLLECT
              INTO l_src_oh_list;
            CLOSE get_src;
            --            dbms_output.put_line('     --> 41 Src Recs Cnt: ' || l_src_oh_list.count);
            FOR r_oh IN c_onhand_list LOOP
            
              FOR i_oh IN l_src_oh_list.first .. l_src_oh_list.last LOOP
                --dbms_output.put_line('1');
                IF l_src_oh_list.exists(i_oh) THEN
                  --                  dbms_output.put_line('2');
                  IF l_src_oh_list(i_oh).total_qty = r_oh.transaction_quantity THEN
                    --                    dbms_output.put_line('3');
                    lgg                := NULL;
                    ldest_org_id       := NULL;
                    ldest_inventory_id := NULL;
                    SELECT transaction_quantity
                          ,organization_id
                          ,inventory_item_id
                      INTO lgg
                          ,ldest_org_id
                          ,ldest_inventory_id
                      FROM inv.mtl_onhand_quantities_detail
                     WHERE ROWID = r_oh.onhand_quantities_id
                       AND rownum < 2;
                  
                    UPDATE inv.mtl_onhand_quantities_detail
                      SET orig_date_received = lt_oh_list(q).date_received
                    WHERE ROWID = r_oh.onhand_quantities_id;
                  
                    l_rowexist := l_rowexist + 1;
                    --                    dbms_output.put_line('4');
                    --                    dbms_output.put_line('     --> Exists: r_oh.onhand_quantities_id| l_src_oh_list(i_oh)                            .transaction_id | orig_rcv_date' || i_oh || '-' ||                                         r_oh.onhand_quantities_id || '|' || l_src_oh_list(i_oh)                                         .transaction_id || '|' || l_src_oh_list(i_oh)                                         .orig_rcv_date || '|' || r_oh.transaction_quantity);
                    dbms_output.put_line(lt_oh_list(q).orig_organization_id || ',' || lt_oh_list(q)
                                         .orig_inventory_item_id || ',' || lt_oh_list(q)
                                         .create_transaction_id || ',MExists,' || lt_oh_list(q)
                                         .date_received || ',' || lgg || ',' ||
                                          ldest_org_id || ',' || ldest_inventory_id);
                  
                    l_src_oh_list.delete(i_oh);
                    --                    dbms_output.put_line('5');
                  
                    --                    dbms_output.put_line('9');
                    EXIT;
                    --                    dbms_output.put_line('7');
                  END IF; --l_src_oh_list(i_oh).total_qty
                  --                  dbms_output.put_line('8');
                END IF; --l_src_oh_list(i_oh).exists
              --                dbms_output.put_line('10');
              END LOOP; --i_oh IN 1 .. l_src_oh_list
            --              dbms_output.put_line('11');
            END LOOP; --c_onhand_list
            --            dbms_output.put_line('12');
          
            IF l_rowexist = 0 THEN
              dbms_output.put_line(lt_oh_list(q)
                                   .orig_organization_id || ',' || lt_oh_list(q)
                                   .orig_inventory_item_id || ',' || lt_oh_list(q)
                                   .create_transaction_id || ',MExistsNoMatch,' || lt_oh_list(q)
                                   .date_received || ',' || '' || ',' || '' || ',' || '');
            END IF;
          END;
        
        WHEN OTHERS THEN
          dbms_output.put_line(lt_oh_list(q)
                               .orig_organization_id || ',' || lt_oh_list(q).orig_inventory_item_id || ',' || lt_oh_list(q)
                               .create_transaction_id || ',NOT Exists,' || lt_oh_list(q)
                               .date_received || ',' || '' || ',' || '' || ',' || 'Error : ' ||
                                SQLERRM);
          --dbms_output.put_line('     --> Error : ' || SQLERRM);
      END;
    END LOOP;
  END;
  dbms_output.put_line('**********************************');

  --
  COMMIT;
END;
/