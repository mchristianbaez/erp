SELECT e.party_name customer_name,
       d.account_number customer_number,
       NULL party_site_number,
       NULL location,
       jtf.resource_name salesrep,
       h.salesrep_id,
       a.price_type price_type,
       c.organization_code branch,
       a.gross_margin order_gross_margin,
       a.incompatability_group incompatability,
       b.agreement_id agreement_id,
       b.agreement_line_id,
       DECODE (b.agreement_type,
               'CSP', 'Contract Pricing',
               'VQN', 'Vendor Quote')
          Agreement_Type,
       b.vendor_quote_number vq_number,
       b.product_attribute item_attribute,
       b.product_value item_number,
       b.product_description item_description,
       b.start_date start_date,
       b.end_date end_date,
       b.list_price list_price,
       b.modifier_type modifier_type,
       b.modifier_value modifier_value,
       b.selling_price selling_price,
       b.special_cost special_cost,
       (SELECT segment1
          FROM apps.ap_suppliers
         WHERE vendor_id = b.vendor_id)
          vendor_number,
       b.average_cost average_cost,
       b.gross_margin gross_margin,
       i.description last_updated_by,
       b.last_update_date,
       b.latest_rec_flag LATEST_REC_FLAG,
       --qlh.name Modifier_number,
       qlh.description Modifier_name,
       (  (SELECT NVL (
                     SUM (
                        (  ool.UNIT_SELLING_PRICE
                         * NVL (ool.SHIPPED_QUANTITY, ool.ORDERED_QUANTITY))),
                     0)
             FROM apps.oe_order_lines_all ool,
                  apps.oe_price_adjustments opa,
                  apps.qp_modifier_summary_v qll
            WHERE     1 = 1
                  AND opa.line_id = ool.line_id
                  AND qll.list_line_id = opa.list_line_id
                  AND ool.line_type_id NOT IN (1007, 1008)
                  AND qll.list_header_id = opa.list_header_id
                  AND NVL (ool.actual_shipment_date, ool.request_date) >=
                         SYSDATE - 180
                  AND qll.list_header_id = qlh.list_header_id
                  AND qll.attribute2 = b.agreement_line_id)
        -                                          --sale_amount_last_6months,
          (SELECT NVL (
                     SUM (
                        (  ool.UNIT_SELLING_PRICE
                         * NVL (ool.SHIPPED_QUANTITY, ool.ORDERED_QUANTITY))),
                     0)
             FROM apps.oe_order_lines_all ool,
                  apps.oe_price_adjustments opa,
                  apps.qp_modifier_summary_v qll
            WHERE     1 = 1
                  AND opa.line_id = ool.line_id
                  AND qll.list_line_id = opa.list_line_id
                  AND ool.line_type_id IN (1007, 1008)
                  AND qll.list_header_id = opa.list_header_id
                  AND NVL (ool.actual_shipment_date, ool.request_date) >=
                         SYSDATE - 180
                  AND qll.list_header_id = qlh.list_header_id
                  AND qll.attribute2 = b.agreement_line_id))
          sale_amount_last_6months,
       (  (SELECT NVL (
                     SUM (NVL (ool.SHIPPED_QUANTITY, ool.ORDERED_QUANTITY)),
                     0)
             FROM apps.oe_order_lines_all ool,
                  apps.oe_price_adjustments opa,
                  apps.qp_modifier_summary_v qll
            WHERE     1 = 1
                  AND opa.line_id = ool.line_id
                  AND ool.line_type_id NOT IN (1007, 1008, 1003)
                  AND qll.list_line_id = opa.list_line_id
                  AND qll.list_header_id = opa.list_header_id
                  AND NVL (ool.actual_shipment_date, ool.request_date) >=
                         SYSDATE - 180
                  AND qll.list_header_id = qlh.list_header_id
                  AND qll.attribute2 = b.agreement_line_id)
        -                                        --units_sold_in_last_6months,
          (SELECT NVL (
                     SUM (NVL (ool.SHIPPED_QUANTITY, ool.ORDERED_QUANTITY)),
                     0)
             FROM apps.oe_order_lines_all ool,
                  apps.oe_price_adjustments opa,
                  apps.qp_modifier_summary_v qll
            WHERE     1 = 1
                  AND opa.line_id = ool.line_id
                  AND ool.line_type_id = 1007
                  AND qll.list_line_id = opa.list_line_id
                  AND qll.list_header_id = opa.list_header_id
                  AND NVL (ool.actual_shipment_date, ool.request_date) >=
                         SYSDATE - 180
                  AND qll.list_header_id = qlh.list_header_id
                  AND qll.attribute2 = b.agreement_line_id))
          Units_sold_in_last_6months,
       (  (SELECT NVL (
                     SUM (
                        (  ool.unit_cost
                         * NVL (ool.SHIPPED_QUANTITY, ool.ORDERED_QUANTITY))),
                     0)
             FROM apps.oe_order_lines_all ool,
                  apps.oe_price_adjustments opa,
                  apps.qp_modifier_summary_v qll
            WHERE     1 = 1
                  AND opa.line_id = ool.line_id
                  AND qll.list_line_id = opa.list_line_id
                  AND ool.line_type_id NOT IN (1007, 1008)
                  AND qll.list_header_id = opa.list_header_id
                  AND NVL (ool.actual_shipment_date, ool.request_date) >=
                         SYSDATE - 180
                  AND qll.list_header_id = qlh.list_header_id
                  AND qll.attribute2 = b.agreement_line_id)
        -                                          --sale_amount_last_6months,
          (SELECT NVL (
                     SUM (
                        (  ool.unit_cost
                         * NVL (ool.SHIPPED_QUANTITY, ool.ORDERED_QUANTITY))),
                     0)
             FROM apps.oe_order_lines_all ool,
                  apps.oe_price_adjustments opa,
                  apps.qp_modifier_summary_v qll
            WHERE     1 = 1
                  AND opa.line_id = ool.line_id
                  AND qll.list_line_id = opa.list_line_id
                  AND ool.line_type_id IN (1007, 1008)
                  AND qll.list_header_id = opa.list_header_id
                  AND NVL (ool.actual_shipment_date, ool.request_date) >=
                         SYSDATE - 180
                  AND qll.list_header_id = qlh.list_header_id
                  AND qll.attribute2 = b.agreement_line_id))
          cost_amount_last_6months,
       b.creation_date,
       b.REVISION_NUMBER
  FROM xxwc.xxwc_om_contract_pricing_hdr a,
       xxwc.xxwc_om_contract_pricing_lines b,
       apps.mtl_parameters c,
       apps.hz_cust_accounts d,
       apps.hz_parties e,
       apps.hz_cust_acct_sites_all f,
       apps.hz_cust_site_uses_all g,
       apps.ra_salesreps_all h,
       apps.fnd_user i,
       jtf.jtf_rs_resource_extns_tl jtf,
       apps.qp_list_headers qlh
 WHERE     a.agreement_id = b.agreement_id
       AND a.organization_id = c.organization_id
       AND b.last_updated_by = i.user_id
       AND d.party_id = e.party_id
       AND a.customer_id = d.cust_account_id
       AND f.cust_acct_site_id = g.cust_acct_site_id
       AND f.status = 'A'
       AND f.org_id = 162
       AND f.cust_account_id = a.customer_id
       AND g.site_use_code = 'BILL_TO'
       AND g.primary_flag = 'Y'
       AND g.status = 'A'
       AND g.org_id = 162
       AND g.primary_salesrep_id = h.salesrep_id
       AND h.resource_id = jtf.resource_id
       AND jtf.language = 'US'
       AND NVL (h.org_id, 162) = 162
       AND a.price_type = 'MASTER'
       AND a.agreement_id = qlh.attribute14
       --AND NVL (TRUNC (b.END_DATE), TRUNC (SYSDATE)) >= TRUNC (SYSDATE)
       AND b.REVISION_NUMBER > 0
       AND NOT (                             --b.CREATION_DATE < SYSDATE - 180
                NVL (TRUNC (b.END_DATE), TRUNC (SYSDATE)) <
                       TRUNC (SYSDATE)
                AND (  (SELECT NVL (
                                  SUM (
                                     (  ool.UNIT_SELLING_PRICE
                                      * NVL (ool.SHIPPED_QUANTITY,
                                             ool.ORDERED_QUANTITY))),
                                  0)
                          FROM apps.oe_order_lines_all ool,
                               apps.oe_price_adjustments opa,
                               apps.qp_modifier_summary_v qll
                         WHERE     1 = 1
                               AND opa.line_id = ool.line_id
                               AND qll.list_line_id = opa.list_line_id
                               AND ool.line_type_id NOT IN (1007, 1008)
                               AND qll.list_header_id = opa.list_header_id
                               AND NVL (ool.actual_shipment_date,
                                        ool.request_date) >= SYSDATE - 180
                               AND qll.list_header_id = qlh.list_header_id
                               AND qll.attribute2 = b.agreement_line_id)
                     -                             --sale_amount_last_6months,
                       (SELECT NVL (
                                  SUM (
                                     (  ool.UNIT_SELLING_PRICE
                                      * NVL (ool.SHIPPED_QUANTITY,
                                             ool.ORDERED_QUANTITY))),
                                  0)
                          FROM apps.oe_order_lines_all ool,
                               apps.oe_price_adjustments opa,
                               apps.qp_modifier_summary_v qll
                         WHERE     1 = 1
                               AND opa.line_id = ool.line_id
                               AND qll.list_line_id = opa.list_line_id
                               AND ool.line_type_id IN (1007, 1008)
                               AND qll.list_header_id = opa.list_header_id
                               AND NVL (ool.actual_shipment_date,
                                        ool.request_date) >= SYSDATE - 180
                               AND qll.list_header_id = qlh.list_header_id
                               AND qll.attribute2 = b.agreement_line_id)) = 0)
UNION
SELECT e.party_name customer_name,
       d.account_number customer_number,
       j.party_site_number,
       --g1.location,
       g.location,
       jtf.resource_name salesrep,
       h.salesrep_id,
       a.price_type price_type,
       c.organization_code branch,
       a.gross_margin order_gross_margin,
       a.incompatability_group incompatability,
       b.agreement_id agreement_id,
       b.agreement_line_id,
       DECODE (b.agreement_type,
               'CSP', 'Contract Pricing',
               'VQN', 'Vendor Quote')
          Agreement_Type,
       b.vendor_quote_number vq_number,
       b.product_attribute item_attribute,
       b.product_value item_number,
       b.product_description item_description,
       b.start_date start_date,
       b.end_date end_date,
       b.list_price list_price,
       b.modifier_type modifier_type,
       b.modifier_value modifier_value,
       b.selling_price selling_price,
       b.special_cost special_cost,
       (SELECT segment1
          FROM apps.ap_suppliers
         WHERE vendor_id = b.vendor_id)
          vendor_number,
       b.average_cost average_cost,
       b.gross_margin gross_margin,
       i.description last_updated_by,
       b.last_update_date,
       b.latest_rec_flag LATEST_REC_FLAG,
       --qlh.name Modifier_number,
       qlh.description Modifier_name,
       (  (SELECT NVL (
                     SUM (
                        (  ool.UNIT_SELLING_PRICE
                         * NVL (ool.SHIPPED_QUANTITY, ool.ORDERED_QUANTITY))),
                     0)
             FROM apps.oe_order_lines_all ool,
                  apps.oe_price_adjustments opa,
                  apps.qp_modifier_summary_v qll
            WHERE     1 = 1
                  AND opa.line_id = ool.line_id
                  AND qll.list_line_id = opa.list_line_id
                  AND ool.line_type_id NOT IN (1007, 1008)
                  AND qll.list_header_id = opa.list_header_id
                  AND NVL (ool.actual_shipment_date, ool.request_date) >=
                         SYSDATE - 180
                  AND qll.list_header_id = qlh.list_header_id
                  AND qll.attribute2 = b.agreement_line_id)
        -                                          --sale_amount_last_6months,
          (SELECT NVL (
                     SUM (
                        (  ool.UNIT_SELLING_PRICE
                         * NVL (ool.SHIPPED_QUANTITY, ool.ORDERED_QUANTITY))),
                     0)
             FROM apps.oe_order_lines_all ool,
                  apps.oe_price_adjustments opa,
                  apps.qp_modifier_summary_v qll
            WHERE     1 = 1
                  AND opa.line_id = ool.line_id
                  AND qll.list_line_id = opa.list_line_id
                  AND ool.line_type_id IN (1007, 1008)
                  AND qll.list_header_id = opa.list_header_id
                  AND NVL (ool.actual_shipment_date, ool.request_date) >=
                         SYSDATE - 180
                  AND qll.list_header_id = qlh.list_header_id
                  AND qll.attribute2 = b.agreement_line_id))
          sale_amount_last_6months,
       (  (SELECT NVL (
                     SUM (NVL (ool.SHIPPED_QUANTITY, ool.ORDERED_QUANTITY)),
                     0)
             FROM apps.oe_order_lines_all ool,
                  apps.oe_price_adjustments opa,
                  apps.qp_modifier_summary_v qll
            WHERE     1 = 1
                  AND opa.line_id = ool.line_id
                  AND ool.line_type_id NOT IN (1007, 1008, 1003)
                  AND qll.list_line_id = opa.list_line_id
                  AND qll.list_header_id = opa.list_header_id
                  AND NVL (ool.actual_shipment_date, ool.request_date) >=
                         SYSDATE - 180
                  AND qll.list_header_id = qlh.list_header_id
                  AND qll.attribute2 = b.agreement_line_id)
        -                                        --units_sold_in_last_6months,
          (SELECT NVL (
                     SUM (NVL (ool.SHIPPED_QUANTITY, ool.ORDERED_QUANTITY)),
                     0)
             FROM apps.oe_order_lines_all ool,
                  apps.oe_price_adjustments opa,
                  apps.qp_modifier_summary_v qll
            WHERE     1 = 1
                  AND opa.line_id = ool.line_id
                  AND ool.line_type_id = 1007
                  AND qll.list_line_id = opa.list_line_id
                  AND qll.list_header_id = opa.list_header_id
                  AND NVL (ool.actual_shipment_date, ool.request_date) >=
                         SYSDATE - 180
                  AND qll.list_header_id = qlh.list_header_id
                  AND qll.attribute2 = b.agreement_line_id))
          Units_sold_in_last_6months,
       (  (SELECT NVL (
                     SUM (
                        (  ool.unit_cost
                         * NVL (ool.SHIPPED_QUANTITY, ool.ORDERED_QUANTITY))),
                     0)
             FROM apps.oe_order_lines_all ool,
                  apps.oe_price_adjustments opa,
                  apps.qp_modifier_summary_v qll
            WHERE     1 = 1
                  AND opa.line_id = ool.line_id
                  AND qll.list_line_id = opa.list_line_id
                  AND ool.line_type_id NOT IN (1007, 1008)
                  AND qll.list_header_id = opa.list_header_id
                  AND NVL (ool.actual_shipment_date, ool.request_date) >=
                         SYSDATE - 180
                  AND qll.list_header_id = qlh.list_header_id
                  AND qll.attribute2 = b.agreement_line_id)
        -                                          --sale_amount_last_6months,
          (SELECT NVL (
                     SUM (
                        (  ool.unit_cost
                         * NVL (ool.SHIPPED_QUANTITY, ool.ORDERED_QUANTITY))),
                     0)
             FROM apps.oe_order_lines_all ool,
                  apps.oe_price_adjustments opa,
                  apps.qp_modifier_summary_v qll
            WHERE     1 = 1
                  AND opa.line_id = ool.line_id
                  AND qll.list_line_id = opa.list_line_id
                  AND ool.line_type_id IN (1007, 1008)
                  AND qll.list_header_id = opa.list_header_id
                  AND NVL (ool.actual_shipment_date, ool.request_date) >=
                         SYSDATE - 180
                  AND qll.list_header_id = qlh.list_header_id
                  AND qll.attribute2 = b.agreement_line_id))
          cost_amount_last_6months,
       b.creation_date,
       b.REVISION_NUMBER
  FROM xxwc.xxwc_om_contract_pricing_hdr a,
       xxwc.xxwc_om_contract_pricing_lines b,
       apps.mtl_parameters c,
       apps.hz_cust_accounts d,
       apps.hz_parties e,
       apps.hz_cust_acct_sites_all f,
       -- apps.hz_cust_acct_sites_all f1,
       apps.hz_cust_site_uses_all g,
       --apps.hz_cust_site_uses_all g1,
       apps.ra_salesreps_all h,
       apps.fnd_user i,
       jtf.jtf_rs_resource_extns_tl jtf,
       apps.hz_party_sites j,
       apps.qp_list_headers qlh
 WHERE     a.agreement_id = b.agreement_id
       AND a.organization_id = c.organization_id
       AND b.last_updated_by = i.user_id
       AND d.party_id = e.party_id
       AND a.customer_id = d.cust_account_id
       AND f.cust_acct_site_id = g.cust_acct_site_id
       AND f.cust_account_id = a.customer_id
       AND f.status = 'A'
       AND f.org_id = 162
       AND g.site_use_code = 'SHIP_TO'
       AND g.site_use_id = a.CUSTOMER_SITE_ID
       -- AND g.primary_flag = 'Y'
       AND g.status = 'A'
       AND g.org_id = 162
       AND g.primary_salesrep_id = h.salesrep_id
       AND h.resource_id = jtf.resource_id
       AND jtf.language = 'US'
       AND NVL (h.org_id, 162) = 162
       --AND g1.site_use_id = a.customer_site_id
       --AND f1.cust_acct_site_id = g1.cust_acct_site_id
       --AND f1.party_site_id = j.party_site_id
       AND f.party_site_id = j.party_site_id
       AND a.price_type = 'SHIPTO'
       AND a.agreement_id = qlh.attribute14
       --AND NVL (TRUNC (b.END_DATE), TRUNC (SYSDATE)) >= TRUNC (SYSDATE)
       AND b.REVISION_NUMBER > 0
       AND NOT (                             --b.CREATION_DATE < SYSDATE - 180
                NVL (TRUNC (b.END_DATE), TRUNC (SYSDATE)) <
                       TRUNC (SYSDATE)
                AND (  (SELECT NVL (
                                  SUM (
                                     (  ool.UNIT_SELLING_PRICE
                                      * NVL (ool.SHIPPED_QUANTITY,
                                             ool.ORDERED_QUANTITY))),
                                  0)
                          FROM apps.oe_order_lines_all ool,
                               apps.oe_price_adjustments opa,
                               apps.qp_modifier_summary_v qll
                         WHERE     1 = 1
                               AND opa.line_id = ool.line_id
                               AND qll.list_line_id = opa.list_line_id
                               AND ool.line_type_id NOT IN (1007, 1008)
                               AND qll.list_header_id = opa.list_header_id
                               AND NVL (ool.actual_shipment_date,
                                        ool.request_date) >= SYSDATE - 180
                               AND qll.list_header_id = qlh.list_header_id
                               AND qll.attribute2 = b.agreement_line_id)
                     -                             --sale_amount_last_6months,
                       (SELECT NVL (
                                  SUM (
                                     (  ool.UNIT_SELLING_PRICE
                                      * NVL (ool.SHIPPED_QUANTITY,
                                             ool.ORDERED_QUANTITY))),
                                  0)
                          FROM apps.oe_order_lines_all ool,
                               apps.oe_price_adjustments opa,
                               apps.qp_modifier_summary_v qll
                         WHERE     1 = 1
                               AND opa.line_id = ool.line_id
                               AND qll.list_line_id = opa.list_line_id
                               AND ool.line_type_id IN (1007, 1008)
                               AND qll.list_header_id = opa.list_header_id
                               AND NVL (ool.actual_shipment_date,
                                        ool.request_date) >= SYSDATE - 180
                               AND qll.list_header_id = qlh.list_header_id
                               AND qll.attribute2 = b.agreement_line_id)) = 0)
UNION
SELECT e.party_name customer_name,
       d.account_number customer_number,
       NULL party_site_number,
       NULL location,
       jtf.resource_name salesrep,
       h.salesrep_id,
       a.price_type price_type,
       c.organization_code branch,
       a.gross_margin order_gross_margin,
       a.incompatability_group incompatability,
       b.agreement_id agreement_id,
       b.agreement_line_id,
       DECODE (b.agreement_type,
               'CSP', 'Contract Pricing',
               'VQN', 'Vendor Quote')
          Agreement_Type,
       b.vendor_quote_number vq_number,
       b.product_attribute item_attribute,
       b.product_value item_number,
       b.product_description item_description,
       b.start_date start_date,
       b.end_date end_date,
       b.list_price list_price,
       b.modifier_type modifier_type,
       b.modifier_value modifier_value,
       b.selling_price selling_price,
       b.special_cost special_cost,
       (SELECT segment1
          FROM apps.ap_suppliers
         WHERE vendor_id = b.vendor_id)
          vendor_number,
       b.average_cost average_cost,
       b.gross_margin gross_margin,
       i.description last_updated_by,
       b.last_update_date,
       b.latest_rec_flag LATEST_REC_FLAG,
       --qlh.name Modifier_number,
       qlh.description Modifier_name,
       (  (SELECT NVL (
                     SUM (
                          (  ool.UNIT_SELLING_PRICE
                           * NVL (ool.SHIPPED_QUANTITY, ool.ORDERED_QUANTITY))
                        + TAX_VALUE),
                     0)
             FROM apps.oe_order_lines_all ool,
                  apps.oe_price_adjustments opa,
                  apps.qp_modifier_summary_v qll
            WHERE     1 = 1
                  AND opa.line_id = ool.line_id
                  AND qll.list_line_id = opa.list_line_id
                  AND ool.line_type_id NOT IN (1007, 1008)
                  AND qll.list_header_id = opa.list_header_id
                  AND NVL (ool.actual_shipment_date, ool.request_date) >=
                         SYSDATE - 180
                  AND qll.list_header_id = qlh.list_header_id
                  AND qll.attribute2 = b.agreement_line_id)
        -                                          --sale_amount_last_6months,
          (SELECT NVL (
                     SUM (
                          (  ool.UNIT_SELLING_PRICE
                           * NVL (ool.SHIPPED_QUANTITY, ool.ORDERED_QUANTITY))
                        + TAX_VALUE),
                     0)
             FROM apps.oe_order_lines_all ool,
                  apps.oe_price_adjustments opa,
                  apps.qp_modifier_summary_v qll
            WHERE     1 = 1
                  AND opa.line_id = ool.line_id
                  AND qll.list_line_id = opa.list_line_id
                  AND ool.line_type_id IN (1007, 1008)
                  AND qll.list_header_id = opa.list_header_id
                  AND NVL (ool.actual_shipment_date, ool.request_date) >=
                         SYSDATE - 180
                  AND qll.list_header_id = qlh.list_header_id
                  AND qll.attribute2 = b.agreement_line_id))
          sale_amount_last_6months,
       (  (SELECT NVL (
                     SUM (NVL (ool.SHIPPED_QUANTITY, ool.ORDERED_QUANTITY)),
                     0)
             FROM apps.oe_order_lines_all ool,
                  apps.oe_price_adjustments opa,
                  apps.qp_modifier_summary_v qll
            WHERE     1 = 1
                  AND opa.line_id = ool.line_id
                  AND ool.line_type_id NOT IN (1007, 1008, 1003)
                  AND qll.list_line_id = opa.list_line_id
                  AND qll.list_header_id = opa.list_header_id
                  AND NVL (ool.actual_shipment_date, ool.request_date) >=
                         SYSDATE - 180
                  AND qll.list_header_id = qlh.list_header_id
                  AND qll.attribute2 = b.agreement_line_id)
        -                                        --units_sold_in_last_6months,
          (SELECT NVL (
                     SUM (NVL (ool.SHIPPED_QUANTITY, ool.ORDERED_QUANTITY)),
                     0)
             FROM apps.oe_order_lines_all ool,
                  apps.oe_price_adjustments opa,
                  apps.qp_modifier_summary_v qll
            WHERE     1 = 1
                  AND opa.line_id = ool.line_id
                  AND ool.line_type_id = 1007
                  AND qll.list_line_id = opa.list_line_id
                  AND qll.list_header_id = opa.list_header_id
                  AND NVL (ool.actual_shipment_date, ool.request_date) >=
                         SYSDATE - 180
                  AND qll.list_header_id = qlh.list_header_id
                  AND qll.attribute2 = b.agreement_line_id))
          Units_sold_in_last_6months,
       (  (SELECT NVL (
                     SUM (
                        (  ool.unit_cost
                         * NVL (ool.SHIPPED_QUANTITY, ool.ORDERED_QUANTITY))),
                     0)
             FROM apps.oe_order_lines_all ool,
                  apps.oe_price_adjustments opa,
                  apps.qp_modifier_summary_v qll
            WHERE     1 = 1
                  AND opa.line_id = ool.line_id
                  AND qll.list_line_id = opa.list_line_id
                  AND ool.line_type_id NOT IN (1007, 1008)
                  AND qll.list_header_id = opa.list_header_id
                  AND NVL (ool.actual_shipment_date, ool.request_date) >=
                         SYSDATE - 180
                  AND qll.list_header_id = qlh.list_header_id
                  AND qll.attribute2 = b.agreement_line_id)
        -                                          --sale_amount_last_6months,
          (SELECT NVL (
                     SUM (
                        (  ool.unit_cost
                         * NVL (ool.SHIPPED_QUANTITY, ool.ORDERED_QUANTITY))),
                     0)
             FROM apps.oe_order_lines_all ool,
                  apps.oe_price_adjustments opa,
                  apps.qp_modifier_summary_v qll
            WHERE     1 = 1
                  AND opa.line_id = ool.line_id
                  AND qll.list_line_id = opa.list_line_id
                  AND ool.line_type_id IN (1007, 1008)
                  AND qll.list_header_id = opa.list_header_id
                  AND NVL (ool.actual_shipment_date, ool.request_date) >=
                         SYSDATE - 180
                  AND qll.list_header_id = qlh.list_header_id
                  AND qll.attribute2 = b.agreement_line_id))
          cost_amount_last_6months,
       b.creation_date,
       b.REVISION_NUMBER
  FROM xxwc.xxwc_om_contract_pricing_hdr a,
       xxwc.xxwc_om_contract_pricing_lines b,
       apps.mtl_parameters c,
       apps.hz_cust_accounts d,
       apps.hz_parties e,
       apps.hz_cust_acct_sites_all f,
       apps.hz_cust_site_uses_all g,
       apps.ra_salesreps_all h,
       apps.fnd_user i,
       jtf.jtf_rs_resource_extns_tl jtf,
       apps.qp_list_headers qlh,
       apps.qp_qualifiers_v qqv
 WHERE     a.agreement_id = b.agreement_id
       AND a.organization_id = c.organization_id
       AND b.last_updated_by = i.user_id
       AND d.party_id = e.party_id
       AND f.cust_acct_site_id = g.cust_acct_site_id
       AND f.status = 'A'
       AND f.org_id = 162
       AND g.site_use_code = 'BILL_TO'
       AND g.primary_flag = 'Y'
       AND g.status = 'A'
       AND g.org_id = 162
       AND g.primary_salesrep_id = h.salesrep_id
       AND h.resource_id = jtf.resource_id
       AND jtf.language = 'US'
       AND NVL (h.org_id, 162) = 162
       AND a.price_type = 'NATIONAL'
       AND a.agreement_id = qlh.attribute14
       AND qlh.list_header_id = qqv.list_header_id
       AND qqv.QUALIFIER_ATTR_VALUE = TO_CHAR (d.cust_account_id)
       AND qqv.QUALIFIER_ATTR_VALUE = TO_CHAR (f.cust_account_id)
       --AND NVL (TRUNC (b.END_DATE), TRUNC (SYSDATE)) >= TRUNC (SYSDATE)
       AND b.REVISION_NUMBER > 0
       AND NOT (                             --b.CREATION_DATE < SYSDATE - 180
                NVL (TRUNC (b.END_DATE), TRUNC (SYSDATE)) <
                       TRUNC (SYSDATE)
                AND (  (SELECT NVL (
                                  SUM (
                                     (  ool.UNIT_SELLING_PRICE
                                      * NVL (ool.SHIPPED_QUANTITY,
                                             ool.ORDERED_QUANTITY))),
                                  0)
                          FROM apps.oe_order_lines_all ool,
                               apps.oe_price_adjustments opa,
                               apps.qp_modifier_summary_v qll
                         WHERE     1 = 1
                               AND opa.line_id = ool.line_id
                               AND qll.list_line_id = opa.list_line_id
                               AND ool.line_type_id NOT IN (1007, 1008)
                               AND qll.list_header_id = opa.list_header_id
                               AND NVL (ool.actual_shipment_date,
                                        ool.request_date) >= SYSDATE - 180
                               AND qll.list_header_id = qlh.list_header_id
                               AND qll.attribute2 = b.agreement_line_id)
                     -                             --sale_amount_last_6months,
                       (SELECT NVL (
                                  SUM (
                                     (  ool.UNIT_SELLING_PRICE
                                      * NVL (ool.SHIPPED_QUANTITY,
                                             ool.ORDERED_QUANTITY))),
                                  0)
                          FROM apps.oe_order_lines_all ool,
                               apps.oe_price_adjustments opa,
                               apps.qp_modifier_summary_v qll
                         WHERE     1 = 1
                               AND opa.line_id = ool.line_id
                               AND qll.list_line_id = opa.list_line_id
                               AND ool.line_type_id IN (1007, 1008)
                               AND qll.list_header_id = opa.list_header_id
                               AND NVL (ool.actual_shipment_date,
                                        ool.request_date) >= SYSDATE - 180
                               AND qll.list_header_id = qlh.list_header_id
                               AND qll.attribute2 = b.agreement_line_id)) = 0)