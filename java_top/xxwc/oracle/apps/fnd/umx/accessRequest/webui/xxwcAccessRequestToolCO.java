/*===========================================================================+
 |   Copyright 2012 HD Supply                          |
 +===========================================================================+
 |  HISTORY         
 |  10/18/2012 - Initial Version, Andre
 |
 |  Description
 |  This page serves as the User Interface for collecting additional info
 |  for a User Access Request Tool request and workflow. This page simply
 |  gathers some required information and performs basic validation. There
 |  is (will be) a back end process that subscribes to the workflow approval
 |  event. That process will take the information collected here and apply
 |  it to the various modules, tables, APIs to grant the user all the access
 |  if the request was approved.
 
    This page should remain simple and light since it will be used by all
    end users. It should provide enough instructions and information on the
    screen so that any user can understand what is being asked for.
 +===========================================================================*/
package xxwc.oracle.apps.fnd.umx.accessRequest.webui;

import java.io.Serializable;

import java.util.Date;

import oracle.apps.fnd.common.VersionInfo;
import oracle.apps.fnd.framework.OAApplicationModule;
import oracle.apps.fnd.framework.OAException;
import oracle.apps.fnd.framework.webui.OAControllerImpl;
import oracle.apps.fnd.framework.webui.OAPageContext;
import oracle.apps.fnd.framework.webui.beans.OAWebBean;
import oracle.apps.fnd.framework.webui.beans.message.OAMessageDateFieldBean;
import oracle.apps.fnd.umx.registration.RegistrationAdmin;
import oracle.apps.fnd.umx.registration.RegistrationBean;


/**
 * Controller for xxwcAccessRequestTool.PageLayoutRN
 */
public class xxwcAccessRequestToolCO extends OAControllerImpl
{
  public static final String RCS_ID="$Header$";
  public static final boolean RCS_ID_RECORDED =
        VersionInfo.recordClassVersion(RCS_ID, "%packagename%");

  /**
   * Layout and page setup logic for a region.
   * @param pageContext the current OA page context
   * @param webBean the web bean corresponding to the region
   */
  public void processRequest(OAPageContext pageContext, OAWebBean webBean)
  {
    super.processRequest(pageContext, webBean);
    
      ///// BEGIN ORACLE CODE
      // Read value of the registration parameter
      String umxRegParams = pageContext.getParameter("UMXRegParams");
      // Call method in AM to instantiate RegistrationBean
      // Combine any other instantiation logic that
      // you have in this method
      Serializable[] parameters = { umxRegParams };
      OAApplicationModule am = pageContext.getApplicationModule(webBean);
      am.invokeMethod("initiateRegProcess", parameters);
      ///// END ORACLE CODE
      
      // Set Role Start Date to default to today.
      OAMessageDateFieldBean  dateBean = 
        (OAMessageDateFieldBean)webBean.findChildRecursive("roleStartDate"); 
      dateBean.setValue(new Date());

  }

  /**
   * Procedure to handle form submissions for form elements in
   * a region.
   * @param pageContext the current OA page context
   * @param webBean the web bean corresponding to the region
   */
  public void processFormRequest(OAPageContext pageContext, OAWebBean webBean)  {
    super.processFormRequest(pageContext, webBean);
    
      if (pageContext.getParameter("Submit") != null) {
        
        // Get a handle to the Application Module
        OAApplicationModule am = pageContext.getApplicationModule(webBean);
        
        // The Oracle developer guide says to use an Entity object to 
        // instantiate and populate the Registration bean. However that is
        // overkill because we are not doing a multi-page registration flow.
        // This is a single page so the Controller can handle this.
        RegistrationBean regBean;
        
        // The error handling here must be re-examined because I don't think it
        // will roll back the OADB transaction in case of a real exception
        // since I wrapped it in a try/catch block. See the UMX Developer's 
        // guide for information on how to do this correctly.
        try {
            
            regBean = RegistrationAdmin.getRegistrationBean(am.getOADBTransaction());
            
            // This is just for demonstration purposes until reviewed more closely.
            // It is not good practice to throw items from getParameter directly
            // into an object like this without some validation. However it should
            // be safe for fields that perform their own validation such as the LOVs.
            // It is also safe for things like checkboxes or free-text fields.
            regBean.setCustomAttribute("XXWC_ART_ROLE_NAME", pageContext.getParameter("roleNameFromLOV")); 
            regBean.setCustomAttribute("XXWC_ART_ROLE_DISPLAY_NAME", pageContext.getParameter("roleLOVMsgRN"));
            // TODO: This date is NOT in the right format.
            regBean.setCustomAttribute("XXWC_ART_ROLE_START_DATE", pageContext.getParameter("roleStartDate")); 
            // TODO: Add profile options and other values as per the MD50 design
            regBean.setCustomAttribute("XXWC_ART_SET_AS_BUYER", pageContext.getParameter("setAsBuyer"));
            regBean.setCustomAttribute("XXWC_ART_SHIPPING_GRANTS", pageContext.getParameter("shippingGrants"));
            regBean.setCustomAttribute("XXWC_ART_WO_APPROVAL_LIMITS", pageContext.getParameter("writeOffApprovalLimits"));
            regBean.setCustomAttribute("XXWC_ART_JUSTIFICATION", pageContext.getParameter("justification")); 
            
            am.getOADBTransaction().commit();
            
            // Get the registration Id and status code.
            String message = new String();
            message += "Request ID: " + regBean.getRequestRegid();
            message += " Status Code: " + regBean.getStatusCode();
            throw new OAException(message, OAException.INFORMATION);
        }
        catch (OAException e) {
            // This is just to catch the exception thrown intentionally to display
            // the confirmation number.
            throw new OAException(e.getMessage(), e.getMessageType());
        }
        catch (Exception e){
            // This is for all otehr exceptions.
            throw new OAException(e.getMessage(), OAException.ERROR);
        }
    }
  }

}
