package xxwc.oracle.apps.ego.item.eu.util;

public class ItemUtil {
    public ItemUtil() {
    }

    public static boolean isNull(String text) {
        return ((text == null) || (text != null && text.trim().equals("")));
    }
}
