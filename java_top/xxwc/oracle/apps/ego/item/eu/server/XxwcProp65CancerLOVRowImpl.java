package xxwc.oracle.apps.ego.item.eu.server;

import oracle.apps.fnd.framework.server.OAViewRowImpl;

import oracle.jbo.server.AttributeDefImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class XxwcProp65CancerLOVRowImpl extends OAViewRowImpl {
    public static final int FLEXVALUE = 0;
    public static final int VALUEDESCRIPTION = 1;

    /**This is the default constructor (do not remove)
     */
    public XxwcProp65CancerLOVRowImpl() {
    }

    /**Gets the attribute value for the calculated attribute FlexValue
     */
    public String getFlexValue() {
        return (String) getAttributeInternal(FLEXVALUE);
    }

    /**Sets <code>value</code> as the attribute value for the calculated attribute FlexValue
     */
    public void setFlexValue(String value) {
        setAttributeInternal(FLEXVALUE, value);
    }

    /**Gets the attribute value for the calculated attribute ValueDescription
     */
    public String getValueDescription() {
        return (String) getAttributeInternal(VALUEDESCRIPTION);
    }

    /**Sets <code>value</code> as the attribute value for the calculated attribute ValueDescription
     */
    public void setValueDescription(String value) {
        setAttributeInternal(VALUEDESCRIPTION, value);
    }

    /**getAttrInvokeAccessor: generated method. Do not modify.
     */
    protected Object getAttrInvokeAccessor(int index, 
                                           AttributeDefImpl attrDef) throws Exception {
        switch (index) {
        case FLEXVALUE:
            return getFlexValue();
        case VALUEDESCRIPTION:
            return getValueDescription();
        default:
            return super.getAttrInvokeAccessor(index, attrDef);
        }
    }

    /**setAttrInvokeAccessor: generated method. Do not modify.
     */
    protected void setAttrInvokeAccessor(int index, Object value, 
                                         AttributeDefImpl attrDef) throws Exception {
        switch (index) {
        default:
            super.setAttrInvokeAccessor(index, value, attrDef);
            return;
        }
    }
}
