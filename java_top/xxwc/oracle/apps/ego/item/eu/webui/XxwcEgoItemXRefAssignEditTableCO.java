package xxwc.oracle.apps.ego.item.eu.webui;

import com.sun.java.util.collections.ArrayList;

import java.sql.SQLException;
import java.sql.Types;

import oracle.apps.ego.item.eu.webui.EgoItemXRefAssignEditTableCO;
import oracle.apps.fnd.framework.OAApplicationModule;
import oracle.apps.fnd.framework.OAException;
import oracle.apps.fnd.framework.OAViewObject;
import oracle.apps.fnd.framework.webui.OAPageContext;
import oracle.apps.fnd.framework.webui.beans.OAWebBean;

import oracle.jbo.Row;
import oracle.jbo.domain.Number;

import oracle.jdbc.OracleCallableStatement;

// Created this controllder Java file in TMS20170421-00196 - Vamshi
public class XxwcEgoItemXRefAssignEditTableCO extends EgoItemXRefAssignEditTableCO {

    public void processRequest(OAPageContext pageContext, OAWebBean webBean) {
        pageContext.writeDiagnostics(this, "BEFOREPR", 6);
        super.processRequest(pageContext, webBean);
        pageContext.writeDiagnostics(this, "AFTERPR", 6);
    }

    public void processFormRequest(OAPageContext pageContext, 
                                   OAWebBean webBean) {
        pageContext.writeDiagnostics(this, "Stage122", 6);
        String str = pageContext.getParameter("_FORM_SUBMIT_BUTTON");
        pageContext.writeDiagnostics(this, "str = " + str, 6);
        OAApplicationModule am = 
            (OAApplicationModule)pageContext.getApplicationModule(webBean);
        pageContext.writeDiagnostics(this, "Stage1221" + am, 6);
        OAViewObject vo = 
            (OAViewObject)am.findViewObject("EgoItemXRefPrivilegeVO");
        pageContext.writeDiagnostics(this, 
                                     "vo = " + vo + " ,rowcount = " + vo.getRowCount(), 
                                     6);
        String crossReference = null, crossReferenceType = null, xretun_msg = 
            null;
        boolean result = true;
        Number itemId = null;
        ArrayList xerrmsg = new ArrayList();
        String s = 
            "begin xxwc_pim_chg_item_mgmt.pdh_upc_validation_prc(" + "p_inventory_item_id  => :1" + 
            ", p_cross_reference => :2" + ", x_error_msg       => :3); end; ";

        pageContext.writeDiagnostics(this, "Stage123", 6);

        if (pageContext.getParameter("EgoApplyButton") != null || 
            "applyButton".equals(str)) {

            pageContext.writeDiagnostics(this, "Stage1", 6);

            if (vo != null) {
                pageContext.writeDiagnostics(this, "Stage2", 6);
                //vo.getAllRowsInRange();
                //pageContext.writeDiagnostics(this,"vo1 = "+vo+" ,rowcount = "+vo.getRowCount(),6);   
                Row localRow3 = vo.first();
                pageContext.writeDiagnostics(this, "localRow3 = " + localRow3, 
                                             6);
                while (localRow3 != null) {
                    crossReference = 
                            (String)localRow3.getAttribute("CrossReference");
                    crossReferenceType = 
                            (String)localRow3.getAttribute("CrossReferenceType");
                    itemId = (Number)localRow3.getAttribute("InventoryItemId");
                    pageContext.writeDiagnostics(this, 
                                                 "Stage3 itemId " + itemId, 6);
                    pageContext.writeDiagnostics(this, 
                                                 "Stage3 crossReference " + 
                                                 crossReference, 6);
                    pageContext.writeDiagnostics(this, 
                                                 "Stage3 crossReferenceType " + 
                                                 crossReferenceType, 6);
                    if ("UPC".equals(crossReferenceType) || 
                        "EAN".equals(crossReferenceType) || 
                        "PENDING".equals(crossReferenceType) || 
                        "GTIN".equals(crossReferenceType)) {

                        pageContext.writeDiagnostics(this, 
                                                     "Stage4 inside crossReferenceType ", 
                                                     6);

                        try {
                            OracleCallableStatement callableStmt = 
                                (OracleCallableStatement)am.getOADBTransaction().createCallableStatement(s, 
                                                                                                         -1);

                            xretun_msg = null;
                            callableStmt.setNUMBER(1, itemId);
                            callableStmt.setString(2, crossReference);
                            callableStmt.registerOutParameter(3, 
                                                              Types.VARCHAR);
                            callableStmt.execute();
                            xretun_msg = callableStmt.getString(3);
                            pageContext.writeDiagnostics(this, 
                                                         "Stage5 xretun_msg " + 
                                                         xretun_msg, 6);
                            if (xretun_msg != null) {
                                xerrmsg.add(new OAException(xretun_msg));
                                result = false;
                            }
                        } catch (SQLException sqlexception) {
                            throw OAException.wrapperException(sqlexception);
                        } catch (Exception exception) {
                            throw OAException.wrapperException(exception);
                        }
                    }
                    localRow3 = vo.next();

                }
                if (!result) {
                    pageContext.writeDiagnostics(this, "Stage6 ", 6);
                    OAException.raiseBundledOAException(xerrmsg);
                }
            }
        }
        super.processFormRequest(pageContext, webBean);
    }
}
