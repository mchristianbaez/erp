/*===========================================================================+
 |   Copyright (c) 2001, 2005 Oracle Corporation, Redwood Shores, CA, USA    |
 |                         All rights reserved.                              |
 +===========================================================================+
 |  HISTORY                                                                  |
 +===========================================================================*/
package xxwc.oracle.apps.ego.item.admin.lov.webui;

import java.io.Serializable;

import java.util.Dictionary;

import oracle.apps.fnd.common.VersionInfo;
import oracle.apps.fnd.framework.OAApplicationModule;
import oracle.apps.fnd.framework.OAFwkConstants;
import oracle.apps.fnd.framework.webui.OAControllerImpl;
import oracle.apps.fnd.framework.webui.OAPageContext;
import oracle.apps.fnd.framework.webui.beans.OAWebBean;

/**
 * Controller for ...
 */
public class XXWCSourceOrgLovRNCO extends OAControllerImpl
{
  public static final String RCS_ID="$Header$";
  public static final boolean RCS_ID_RECORDED =
        VersionInfo.recordClassVersion(RCS_ID, "%packagename%");

  /**
   * Layout and page setup logic for a region.
   * @param pageContext the current OA page context
   * @param webBean the web bean corresponding to the region
   */
  public void processRequest(OAPageContext pageContext, OAWebBean webBean)
  {
    super.processRequest(pageContext, webBean);

          OAApplicationModule am = pageContext.getApplicationModule(webBean);
          Dictionary  critItems = pageContext.getLovCriteriaItems();
          
          String mainOrgId = critItems.get("OrganizationId") == null ? "" : critItems.get("OrganizationId").toString();
            System.out.println("mainOrgId ====>"+mainOrgId);
            
            pageContext.writeDiagnostics("XXWCSourceOrgLovCO", "mainOrgId ====>"+mainOrgId, OAFwkConstants.STATEMENT);

          Serializable[] param = {mainOrgId};
          am.invokeMethod("executeSourceLovVO", param);
    
  }

  /**
   * Procedure to handle form submissions for form elements in
   * a region.
   * @param pageContext the current OA page context
   * @param webBean the web bean corresponding to the region
   */
  public void processFormRequest(OAPageContext pageContext, OAWebBean webBean)
  {
    super.processFormRequest(pageContext, webBean);
  }

}
