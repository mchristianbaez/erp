/*===========================================================================+
 |   Copyright (c) 2001, 2005 Oracle Corporation, Redwood Shores, CA, USA    |
 |                         All rights reserved.                              |
 +===========================================================================+
 |  HISTORY             
 |  File Name                       XXWCOrgAttributesCO.java 
 |  Created by                      Santhosh Louis          
 |  Last modified on                12/12/12
 |  File Details                    Controller file for Org Attributes Page.
 |  Controller file has the code for form Validations and rules requested.
 |  
 +===========================================================================*/
package xxwc.oracle.apps.ego.item.admin.webui;


import java.io.Serializable;

import java.util.HashMap;

import oracle.apps.fnd.common.VersionInfo;
import oracle.apps.fnd.framework.OAApplicationModule;
import oracle.apps.fnd.framework.OAFwkConstants;
import oracle.apps.fnd.framework.server.OADBTransaction;
import oracle.apps.fnd.framework.webui.OAControllerImpl;
import oracle.apps.fnd.framework.webui.OADataBoundValueViewObject;
import oracle.apps.fnd.framework.webui.OAPageContext;
import oracle.apps.fnd.framework.webui.OAWebBeanConstants;
import oracle.apps.fnd.framework.webui.beans.OAWebBean;
import oracle.apps.fnd.framework.webui.beans.message.OAMessageChoiceBean;
import oracle.apps.fnd.framework.webui.beans.message.OAMessageTextInputBean;
import oracle.apps.fnd.framework.OAException;
import oracle.apps.fnd.framework.webui.OADialogPage;
import oracle.apps.fnd.framework.webui.beans.message.OAMessageLovInputBean;


/**
 * Controller for ...
 */
public class XXWCOrgAttributesCO extends OAControllerImpl
{
  public static final String RCS_ID="$Header$";
  public static final boolean RCS_ID_RECORDED =
        VersionInfo.recordClassVersion(RCS_ID, "%packagename%");

  /**
   * Layout and page setup logic for a region.
   * @param pageContext the current OA page context
   * @param webBean the web bean corresponding to the region
   */
  public void processRequest(OAPageContext pageContext, OAWebBean webBean)
  {
    super.processRequest(pageContext, webBean);
    
      OAApplicationModule am=pageContext.getApplicationModule(webBean);
    //calling the program to show blank values at page entry
          am.invokeMethod("getOrgLevelAttributesData", null);
          
    // On hover feature to set the previous values on their respective fields
     OAMessageChoiceBean  SourceTypeBean = (OAMessageChoiceBean)webBean.findChildRecursive("SourceType");
      if(SourceTypeBean != null){
          SourceTypeBean.setAttributeValue(SHORT_DESC_ATTR, new OADataBoundValueViewObject(SourceTypeBean, "PrevSourceType", "XXWCOrgLevelAttributesMainVO1"));
      }    
      OAMessageLovInputBean srcOrgBean = (OAMessageLovInputBean)webBean.findChildRecursive("SourceOrgLov");
      if(srcOrgBean != null){
          srcOrgBean.setAttributeValue(SHORT_DESC_ATTR, new OADataBoundValueViewObject(srcOrgBean, "PrevSourceOrgCode", "XXWCOrgLevelAttributesMainVO1"));
      }              
      OAMessageLovInputBean srcRuleBean = (OAMessageLovInputBean)webBean.findChildRecursive("SourcingRuleLov");
      if(srcRuleBean != null){
          srcRuleBean.setAttributeValue(SHORT_DESC_ATTR, new OADataBoundValueViewObject(srcRuleBean, "PrevSourcingRuleName", "XXWCOrgLevelAttributesMainVO1"));
      }           
     OAMessageTextInputBean PpltItemBean = (OAMessageTextInputBean)webBean.findChildRecursive("PpltItem");
     if(PpltItemBean != null){
         PpltItemBean.setAttributeValue(SHORT_DESC_ATTR, new OADataBoundValueViewObject(PpltItemBean, "PrevPreplt", "XXWCOrgLevelAttributesMainVO1"));
     }
      OAMessageTextInputBean PltItemBean = (OAMessageTextInputBean)webBean.findChildRecursive("PltItem");
      if(PltItemBean != null){
          PltItemBean.setAttributeValue(SHORT_DESC_ATTR, new OADataBoundValueViewObject(PltItemBean, "PrevFlt", "XXWCOrgLevelAttributesMainVO1"));
      }
          OAMessageTextInputBean FlmItemBean = (OAMessageTextInputBean)webBean.findChildRecursive("FlmItem");
      if(FlmItemBean != null){
          FlmItemBean.setAttributeValue(SHORT_DESC_ATTR, new OADataBoundValueViewObject(FlmItemBean, "PrevFlm", "XXWCOrgLevelAttributesMainVO1"));
      }
      OAMessageChoiceBean  ClassificationItemBean = (OAMessageChoiceBean )webBean.findChildRecursive("ClassificationItem");
      if(ClassificationItemBean != null){
          ClassificationItemBean.setAttributeValue(SHORT_DESC_ATTR, new OADataBoundValueViewObject(ClassificationItemBean, "PrevClassification", "XXWCOrgLevelAttributesMainVO1"));
      }
      OAMessageChoiceBean  PlanningItemBean = (OAMessageChoiceBean)webBean.findChildRecursive("PlanningItem");
      if(PlanningItemBean != null){
          PlanningItemBean.setAttributeValue(SHORT_DESC_ATTR, new OADataBoundValueViewObject(PlanningItemBean, "PrevPlanningCode", "XXWCOrgLevelAttributesMainVO1"));
      }
      OAMessageTextInputBean reserveFlagItemBean = (OAMessageTextInputBean)webBean.findChildRecursive("reserveFlagItem");
      if(reserveFlagItemBean != null){
          reserveFlagItemBean.setAttributeValue(SHORT_DESC_ATTR, new OADataBoundValueViewObject(reserveFlagItemBean, "PrevReserveStock", "XXWCOrgLevelAttributesMainVO1"));
      }
      OAMessageTextInputBean MinItemBean = (OAMessageTextInputBean)webBean.findChildRecursive("MinItem");
      if(MinItemBean != null){
          MinItemBean.setAttributeValue(SHORT_DESC_ATTR, new OADataBoundValueViewObject(MinItemBean, "PrevMin", "XXWCOrgLevelAttributesMainVO1"));
      }
      OAMessageTextInputBean MaxItemBean = (OAMessageTextInputBean)webBean.findChildRecursive("MaxItem");
      if(MaxItemBean != null){
          MaxItemBean.setAttributeValue(SHORT_DESC_ATTR, new OADataBoundValueViewObject(MaxItemBean, "PrevMax", "XXWCOrgLevelAttributesMainVO1"));
      }
      OAMessageChoiceBean  PflagItemBean = (OAMessageChoiceBean)webBean.findChildRecursive("PflagItem");
      if(PflagItemBean != null){
          PflagItemBean.setAttributeValue(SHORT_DESC_ATTR, new OADataBoundValueViewObject(PflagItemBean, "PrevPurchaseFlag", "XXWCOrgLevelAttributesMainVO1"));
      } 
      
      OAMessageLovInputBean BuyerLovBean = (OAMessageLovInputBean)webBean.findChildRecursive("BuyerLov");
      if(BuyerLovBean != null){
          BuyerLovBean.setAttributeValue(SHORT_DESC_ATTR, new OADataBoundValueViewObject(BuyerLovBean, "FullName", "XXWCOrgLevelAttributesMainVO1"));
      }      
  }

  /**
   * Procedure to handle form submissions for form elements in
   * a region.
   * @param pageContext the current OA page context
   * @param webBean the web bean corresponding to the region
   */
  public void processFormRequest(OAPageContext pageContext, OAWebBean webBean)
  {
    super.processFormRequest(pageContext, webBean);
    
      OAApplicationModule am=pageContext.getApplicationModule(webBean);
            OADBTransaction txn = am.getOADBTransaction();
         
    // page parameters and declarations   
       String itemId= (String) pageContext.getParameter("InventoryItemId");
       String itemNo= (String) pageContext.getParameter("ItemNumLov");
       String orgName=pageContext.getParameter("OrgLov");
       String orgId=pageContext.getParameter("OrganizationId");
       String srcTypeId = pageContext.getParameter("SourceType");
       String srcValue = pageContext.getParameter("SourcingRuleLov");
       String ClassificationId = pageContext.getParameter("ClassificationItem");
       String minValue = pageContext.getParameter("MinItem");
       String maxValue = pageContext.getParameter("MaxItem");       
       String ppltValue = pageContext.getParameter("PpltItem");
       String pltValue = pageContext.getParameter("PltItem");
      String flmValue = pageContext.getParameter("FlmItem");
      String pflagValue = pageContext.getParameter("PflagItem");      
      String reserveItemValue = pageContext.getParameter("reserveFlagItem");
      String buyerId = pageContext.getParameter("BuyerId");       
       String planningItemValue = pageContext.getParameter("PlanningItem");
       String srcOrgClassificationVal = pageContext.getParameter("SourceOrgClassification");
       String SourceOrgLovVal = pageContext.getParameter("srcOrgId");
      String previousPurchaseFlag = pageContext.getParameter("previousPurchaseFlag");
      String previousClassification = pageContext.getParameter("previousClassification");      
      String prevSrcRuleId = pageContext.getParameter("prevSrcRuleId");
      String srcRuleId = pageContext.getParameter("SrcRuleId");     
      String prevMinValue = pageContext.getParameter("prevMinItem");
      String prevMaxValue = pageContext.getParameter("prevMaxItem");       
      String userId = Integer.toString(pageContext.getUserId());
      String a="-1";
        double prevMaxInt = Double.parseDouble(a);
        double prevMinInt =Double.parseDouble(a);
       
       String flag="N";    //setting flag to N initially ,if any rule violated it will be set to Y
       
      txn.writeDiagnostics("XXWCOrgLevelAttrsCO", "pflagValue out====>"+pflagValue, OAFwkConstants.STATEMENT);    
          if (pageContext.isLovEvent()){  
             String sourceInputId = pageContext.getLovInputSourceId();
             
             // code check for Lov changes and Value population
             if ("OrgLov".equals(sourceInputId) || "ItemNumLov".equals(sourceInputId)){
             
                // If ORganization drop down is modified save the org values in session
                 if ("OrgLov".equals(sourceInputId)){
                     pageContext.putSessionValue("sessionOrgId",orgId);
                     pageContext.putSessionValue("sessionOrgName",orgName);
                     txn.writeDiagnostics("XXWCOrgLevelAttrsCO", "set session org id ====>"+orgId, OAFwkConstants.STATEMENT);
                     txn.writeDiagnostics("XXWCOrgLevelAttrsCO", "set session org Name ====>"+orgName, OAFwkConstants.STATEMENT);
                 }
                // If Item Lov is modified pick values based on the item selected
                 if ("ItemNumLov".equals(sourceInputId)){

                        String prevOrgId   = (String)pageContext.getSessionValue("sessionOrgId");
                        String prevOrgName = (String)pageContext.getSessionValue("sessionOrgName");
                     txn.writeDiagnostics("XXWCOrgLevelAttrsCO", "getting session org id ====>"+prevOrgId, OAFwkConstants.STATEMENT);
                     txn.writeDiagnostics("XXWCOrgLevelAttrsCO", "getting session org Name ====>"+prevOrgName, OAFwkConstants.STATEMENT);     
                     

                     // If the item selected also has the previous organization then get from session and get values
                        if(prevOrgId != null && !"".equals(prevOrgId)){
                            txn.writeDiagnostics("XXWCOrgLevelAttrsCO", "inside if ord not null", OAFwkConstants.STATEMENT);
                                Serializable[] orgParams = {itemId, prevOrgId,prevOrgName};
                                
                                String checkFlag = (String)am.invokeMethod("checkItemOrgExists", orgParams);
                            txn.writeDiagnostics("XXWCOrgLevelAttrsCO", "checkFlag==>"+checkFlag, OAFwkConstants.STATEMENT);
                                if("N".equals(checkFlag)){
                                
                                    txn.writeDiagnostics("XXWCOrgLevelAttrsCO", "inside checkflag N", OAFwkConstants.STATEMENT);
                                        pageContext.removeSessionValue("sessionOrgId");
                                        pageContext.removeSessionValue("sessionOrgName");
                                }
                                else{
                                txn.writeDiagnostics("XXWCOrgLevelAttrsCO", "inside checkflag Y", OAFwkConstants.STATEMENT);
                                        orgId= prevOrgId;
                                        orgName = prevOrgName;
                                }
                                
                        }
                    
                 }           
                 txn.writeDiagnostics("XXWCOrgLevelAttrsCO", "itemId ====>"+itemId, OAFwkConstants.STATEMENT);

                 txn.writeDiagnostics("XXWCOrgLevelAttrsCO", "itemNo ====>"+itemNo, OAFwkConstants.STATEMENT);

                 txn.writeDiagnostics("XXWCOrgLevelAttrsCO", "orgName ====>"+orgName, OAFwkConstants.STATEMENT);
                txn.writeDiagnostics("XXWCOrgLevelAttrsCO", "orgId ====>"+orgId, OAFwkConstants.STATEMENT);

                // if both org and ITem parameters are set execute AM method to retrieve attribute values
                 if(itemId != null && orgId != null){
                     Serializable[] param = {itemId,itemNo,orgName,orgId};
                     am.invokeMethod("getOrgAttributesData",param);
                 }
                 
                // throw new OAException("Exception - Select orgname",OAException.ERROR);
                 
                 
             }             
            
              
          }
          
         if (pageContext.getParameter("Apply") != null )
         {
             txn.writeDiagnostics("XXWCOrgLevelAttrsCO", "pflagValue ====>"+pflagValue, OAFwkConstants.STATEMENT);         
         /*
             if (!("".equals(SourceOrgLovVal))) {
             if ( ("".equals(srcOrgClassificationVal)||"N".equals(srcOrgClassificationVal)||"Z".equals(srcOrgClassificationVal)) ) {
               
               /OAException message = new OAException("Source Org does not stock.Proceed?",OAException.WARNING);
                OADialogPage dialogPage = new OADialogPage(OAException.WARNING, message, null, "", "");  
                 dialogPage.setOkButtonItemName("ProceedOkButton");

                  dialogPage.setOkButtonToPost(true); 
                  dialogPage.setNoButtonToPost(true); 
                  dialogPage.setPostToCallingPage(true); 

                  dialogPage.setOkButtonLabel("Yes"); 
                  dialogPage.setNoButtonLabel("No");        
                 java.util.Hashtable formParams = new java.util.Hashtable(1);   
                 dialogPage.setFormParameters(formParams); 

                 pageContext.redirectToDialogPage(dialogPage);                   
               // pageContext.putDialogMessage(message);    
                 flag="Y";
                 
             }    
              } 
             */
            // Put your validations here .. 
            // Hard stops will have popup error messages
            //Soft stops will have the ok button 
             // To Diaplay the Exception on a Dialog page
             
             //Source Type set to S but no new Sourcing rule set and existing Sourcing Rule = blank

             if ("2".equals(srcTypeId) && "".equals(srcValue)) {

                 OAException message = new OAException("Sourcing Rule needs to be supplied to proceed.",OAException.ERROR);
                 pageContext.putDialogMessage(message);
                    
                    flag="Y";

                
             }
             if ((!("".equals(orgId))) && orgId !=null){
                    if ((!("".equals(SourceOrgLovVal))) && SourceOrgLovVal!=null ) {
                        if ((orgId).equals(SourceOrgLovVal)){
                             OAException message = new OAException("Cannot Source to Same Org",OAException.ERROR);
                             pageContext.putDialogMessage(message);
                             flag="Y";
                        }
                    }
             }             
             /*
            if (previousClassification!=null && (!("".equals(previousClassification))))     
            {
             if (previousClassification.equals(ClassificationId))
             {      ;
            }
            else{
             if (!("8".equals(ClassificationId) ||"9".equals(ClassificationId) ||"B".equals(ClassificationId) ||"N".equals(ClassificationId) ||"Z".equals(ClassificationId) )) {
                 OAException message = new OAException("Invalid Classification selected.Valid selections: 8, 9, B, N, Z.",OAException.WARNING);
                 pageContext.putDialogMessage(message);    
                 flag="Y";                 
             }
             }
            }
             */
             /*
               --     Tom asked to remove the validations         
              if ((!("".equals(prevMinValue))) && (!("".equals(prevMaxValue))))
              {
                  try{
                     prevMinInt = Double.parseDouble(prevMinValue);
                     prevMaxInt= Double.parseDouble(prevMaxValue);   
                  }
                  
                 catch (Exception e) 
                 {
                    throw new OAException(" Min and Max Values are number fields. ",  OAException.ERROR);
                 }
              }
              */
              // Min value greater than Max value check
             if ((!("".equals(minValue))) && (!("".equals(maxValue))))
             {
             try{
                 double minIntValue = Double.parseDouble(minValue);
                 double maxIntValue = Double.parseDouble(maxValue);

                   /* 
                    * --     Tom asked to remove the validations     
                      if (prevMinInt ==0 && prevMaxInt ==0 ) {
                          if ((minIntValue > 0 || maxIntValue > 0 )&& (!("2".equals(planningItemValue)))){
                              OAException message = new OAException("Planning method not set properly.",OAException.ERROR);
                              pageContext.putDialogMessage(message);    
                              flag="Y";
                          }
                      }                    
                    */
   
                  
                  if ( minIntValue > maxIntValue) {
                      OAException message = new OAException("Min Value greater than Max Value.",OAException.ERROR);
                      pageContext.putDialogMessage(message);    
                      flag="Y";                      
                  }

                  if (minIntValue > 0 && maxIntValue > 0 && (("N".equals(ClassificationId)) || ("Z".equals(ClassificationId)))){
                      OAException message = new OAException("Non-Stock Item.",OAException.ERROR);
                      pageContext.putDialogMessage(message);    
                      flag="Y";
                  }       
                  
              }
              catch (Exception e) 
              {
                 throw new OAException(" Min and Max Values are number fields. ",  OAException.ERROR);
              }
             }

             if (!("".equals(reserveItemValue))){
             try{
                 double reserveIntValue = Double.parseDouble(reserveItemValue);
                /* if (reserveIntValue > 0 && (("2".equals(planningItemValue)) || ("6".equals(planningItemValue)))){
                     OAException message = new OAException("Check Planning Method.",OAException.ERROR);
                     pageContext.putDialogMessage(message);  
                     flag="Y";
                     
                 }   
                 */
             }
                 catch (Exception e) 
                 {
                    throw new OAException(" Reserve Flag value should be a number. ",  OAException.ERROR);
                 }
             }             
             //Classification set to stock but new or existing planning method = blank
             
             if (("8".equals(ClassificationId)||"9".equals(ClassificationId)||"B".equals(ClassificationId)) && ("".equals(planningItemValue))) {
                 OAException message = new OAException("Change Planning Method.",OAException.ERROR);
                 pageContext.putDialogMessage(message);    
                 flag="Y";
             }     
             /*
             if (("8".equals(ClassificationId)||"9".equals(ClassificationId)||"B".equals(ClassificationId)) && ("6".equals(planningItemValue))) {
                 OAException message = new OAException("Check Planning Method.",OAException.ERROR);
                 pageContext.putDialogMessage(message); 
                 flag="Y";
             }     
             */
             //Planning method "ROP" selected but new or existing classification = non-stock
             if ( ("1".equals(planningItemValue)) && ("".equals(ClassificationId)||"N".equals(ClassificationId)||"Z".equals(ClassificationId)) ) {
                 OAException message = new OAException("Check Planning Method.",OAException.ERROR);
                 pageContext.putDialogMessage(message);   
                 flag="Y";
             }    
             //sourcing rule  cant be set to Null
             if (prevSrcRuleId != null && (!("".equals(prevSrcRuleId)))) {
                 if (!(srcRuleId != null && (!("".equals(srcRuleId))))){
                     OAException message = new OAException("Cannot change sourcing rule to Null.",OAException.ERROR);
                     pageContext.putDialogMessage(message);   
                     flag="Y";
                 }
             }
             /*
             if (srcRuleId != null && (!("".equals(srcRuleId)))) {
                 if (!(prevSrcRuleId != null && (!("".equals(prevSrcRuleId))))){
                     OAException message = new OAException("Cannot Map a new Sourcing Rule.",OAException.ERROR);
                     pageContext.putDialogMessage(message);   
                     flag="Y";
                 }
             }          
             */
             txn.writeDiagnostics("XXWCOrgLevelAttrsCO", "orgId ====>"+orgId, OAFwkConstants.STATEMENT);
             // if no validations fail then invoke the database transaction with the parameters
             if ("N".equals(flag)){
                 if(itemId != null && orgId != null){
                     Serializable[] param = {itemId,itemNo,orgName,orgId,userId,srcTypeId,SourceOrgLovVal,ppltValue,pltValue,flmValue,ClassificationId,planningItemValue,minValue,maxValue,pflagValue,reserveItemValue,buyerId,previousPurchaseFlag,previousClassification,srcRuleId,prevSrcRuleId};
                     HashMap param1 =(HashMap)am.invokeMethod("applyButtonSubmit",param);
                 }
                 
             }
                            


             
              /*String userContent = pageContext.getParameter("HelloName");
               String message1 = "Hello, " + userContent + "!";
               throw new OAException(message1, OAException.INFORMATION);
             */


                          
         }
         // If source type made as supplier blank out certain fields
       else if ("setSourceOrg".equals(pageContext.getParameter(OAWebBeanConstants.EVENT_PARAM))){
                String srcType = pageContext.getParameter("SourceType");
                  if ("2".equals(srcType)){
                  am.invokeMethod("setSourceOrgParams");
                  }
              }         
         // If classification is made reset certain values
        else if ("SetParams".equals(pageContext.getParameter(OAWebBeanConstants.EVENT_PARAM))){
                   String pflagValue1 = pageContext.getParameter("PflagItem");   
                if("N".equals(ClassificationId)||"Z".equals(ClassificationId)){
                    if (("3".equals(pflagValue1))||("4".equals(pflagValue1)))
                    {am.invokeMethod("resetFields");}
                    else{am.invokeMethod("resetFieldsnPflag");
                    }
                }
               }

          
             else if ("SourceTypeEvent".equals(pageContext.getParameter(OAWebBeanConstants.EVENT_PARAM))){
                 
             }
             
             else if (pageContext.getParameter("Apply") != null){
                 txn.writeDiagnostics("XXWCOrgLevelAttrsCO", "Inside Apply", OAFwkConstants.STATEMENT);
             }
             // Click on cancel call am with null params and show bllank values
              else if (pageContext.getParameter("Cancel") != null){
                  txn.writeDiagnostics("XXWCOrgLevelAttrsCO", "Inside Apply", OAFwkConstants.STATEMENT);
                  pageContext.removeSessionValue("sessionOrgId");
                  pageContext.removeSessionValue("sessionOrgName");
                  am.invokeMethod("getOrgLevelAttributesData", null);
              }
  }

}
