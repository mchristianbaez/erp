package xxwc.oracle.apps.ar.creditmgt.analysis.webui;

import oracle.apps.ar.creditmgt.analysis.webui.ArCMAnalysisMainSearchPageCO;
import oracle.apps.fnd.framework.webui.OAPageContext;
import oracle.apps.fnd.framework.webui.beans.OAWebBean;
import oracle.apps.fnd.framework.webui.beans.message.OAMessageTextInputBean;
import oracle.apps.ar.creditmgt.util.webui.CMControllerImpl;



public class XXExtArCMAnalysisMainSearchPageCO extends ArCMAnalysisMainSearchPageCO {
    public XXExtArCMAnalysisMainSearchPageCO() {
    }
    
    public void processRequest(OAPageContext pageContext, OAWebBean webBean){
    
    pageContext.writeDiagnostics(this,"*** Start of Custom Code ****",1);
    OAMessageTextInputBean txtInpbn=(OAMessageTextInputBean)pageContext.getRootWebBean().findChildRecursive("OCMSerVal");
        pageContext.writeDiagnostics(this,"*** Bean existing or not  ****"+txtInpbn,1);
    String valFromForm =null;
    pageContext.writeDiagnostics(this,"***  OCMSerVal value from form ****"+pageContext.getParameter("Demo"),1);
    if(pageContext.getParameter("Demo") != null){
    valFromForm = pageContext.getParameter("Demo");  
    
    if(txtInpbn != null){       
    txtInpbn.setValue(pageContext,valFromForm);         
    }
    }
        pageContext.writeDiagnostics(this,"*** End of Custom Code ****",1);
        
        super.processRequest(pageContext, webBean);
    }
    
    
}

