package xxwc.oracle.apps.ar.cusstd.acctSite.webui;

import java.sql.*;
import oracle.apps.ar.cusstd.acctSite.webui.ArAcctSiteOverviewCO;
import oracle.apps.ar.hz.components.account.site.server.HzPuiCustSiteUsesVORowImpl;
import oracle.apps.fnd.framework.OAApplicationModule;
import oracle.apps.fnd.framework.OAException;
import oracle.apps.fnd.framework.server.OADBTransaction;
import oracle.apps.fnd.framework.webui.OAPageContext;
import oracle.apps.fnd.framework.webui.beans.OAWebBean;
import oracle.jbo.ViewObject;
import oracle.jbo.domain.Number;

public class XXWCArAcctSiteOverviewCO extends ArAcctSiteOverviewCO
{

    public XXWCArAcctSiteOverviewCO()
    {
    }

    public void processRequest(OAPageContext oapagecontext, OAWebBean oawebbean)
    {
        super.processRequest(oapagecontext, oawebbean);
    }

    public void processFormRequest(OAPageContext oapagecontext, OAWebBean oawebbean)
    {
        OAApplicationModule oaapplicationmodule = oapagecontext.getApplicationModule(oawebbean);
        if(oapagecontext.getParameter("Save") != null || oapagecontext.getParameter("Apply") != null)
        {
            String scodestr = null;
            Number SiteUseId = null;
            String SiteUseIdStr = null;
            String billToValid = "Y";
            String shipToValid = "Y";
            OAApplicationModule nestedAM = (OAApplicationModule)oaapplicationmodule.findApplicationModule("HzPuiAccountSiteAM");
            if(nestedAM != null)
            {
                ViewObject siteUseVO = nestedAM.findViewObject("HzPuiCustSiteUsesVO");
                HzPuiCustSiteUsesVORowImpl row = null;
                if(siteUseVO.getRowCount() > 0)
                {
                    Connection conn = oaapplicationmodule.getOADBTransaction().getJdbcConnection();
                    for(int x = 0; x < siteUseVO.getRowCount(); x++)
                    {
                        row = (HzPuiCustSiteUsesVORowImpl)siteUseVO.getRowAtRangeIndex(x);
                        if(row != null)
                        {
                            scodestr = (String)row.getAttribute("SiteUseCode");
                            SiteUseId = (Number)row.getAttribute("SiteUseId");
                            SiteUseIdStr = SiteUseId.toString();
                            if("BILL_TO".equals(scodestr))
                            {
                                billToValid = checkSiteValid(SiteUseIdStr, "BILL_TO", conn);
                            }
                            if("SHIP_TO".equals(scodestr))
                            {
                                shipToValid = checkSiteValid(SiteUseIdStr, "SHIP_TO", conn);
                            }
                        }
                    }

                }
            }
            if("N".equals(billToValid) || "N".equals(shipToValid))
            {
                throw new OAException("Please enter Sales Rep before saving record.");
            }
        }
        super.processFormRequest(oapagecontext, oawebbean);
    }

    public String checkSiteValid(String siteUseId, String siteUseCode, Connection conn)
    {
        String result = "N";
        String Query = "SELECT 'Y' isvalid FROM hz_cust_site_uses_all WHERE site_use_id = :1 AND site_us" +
"e_code = :2 AND primary_salesrep_id IS NOT NULL"
;
        try
        {
            PreparedStatement stmt = conn.prepareStatement(Query);
            stmt.setString(1, siteUseId);
            stmt.setString(2, siteUseCode);
            ResultSet resultset = stmt.executeQuery();
            if(resultset.next())
            {
                result = resultset.getString(1);
            }
        }
        catch(Exception ex)
        {
            String s = "N";
            return s;
        }
        return result;
    }
}
