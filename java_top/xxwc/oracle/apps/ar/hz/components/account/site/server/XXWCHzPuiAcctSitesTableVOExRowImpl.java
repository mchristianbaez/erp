package xxwc.oracle.apps.ar.hz.components.account.site.server;

import com.sun.java.util.collections.HashMap;

import java.io.Serializable;

import oracle.apps.ar.hz.components.account.customer.server.HzCustAccountSitesEOImpl;
import oracle.apps.ar.hz.components.account.site.server.HzCustAccountSitesEOExImpl;
import oracle.apps.ar.hz.components.account.site.server.HzPuiAcctSitesTableVORowImpl;
import oracle.apps.fnd.framework.server.OAApplicationModuleImpl;

import oracle.jbo.domain.ClobDomain;
import oracle.jbo.domain.Date;
import oracle.jbo.domain.Number;
import oracle.jbo.server.AttributeDefImpl;


public class XXWCHzPuiAcctSitesTableVOExRowImpl extends HzPuiAcctSitesTableVORowImpl {

    public static final int MAXATTRCONST = oracle.jbo.server.ViewDefImpl.getMaxAttrConst("oracle.apps.ar.hz.components.account.site.server.HzPuiAcctSitesTableVO");
    public static final int PRIMARY_BT = MAXATTRCONST;
    public static final int PRELIM = MAXATTRCONST + 1;
    public static final int PRISM_SITE_NUMBER = MAXATTRCONST + 2;
    public static final int CR_HOLD = MAXATTRCONST + 3;
    public static final int DUMMY = MAXATTRCONST + 4;
    public static final int CUST_SITE_NAME = MAXATTRCONST + 5;


    public XXWCHzPuiAcctSitesTableVOExRowImpl()
    {
    }

    public Number getCustAcctSiteId()
    {
        return (Number)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.CUSTACCTSITEID);
    }

    public void setCustAcctSiteId(Number number)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.CUSTACCTSITEID, number);
    }

    public Number getCustAccountId()
    {
        return (Number)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.CUSTACCOUNTID);
    }

    public void setCustAccountId(Number number)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.CUSTACCOUNTID, number);
    }

    public Number getPartySiteId()
    {
        return (Number)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.PARTYSITEID);
    }

    public void setPartySiteId(Number number)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.PARTYSITEID, number);
    }

    public Date getLastUpdateDate()
    {
        return (Date)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.LASTUPDATEDATE);
    }

    public void setLastUpdateDate(Date date)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.LASTUPDATEDATE, date);
    }

    public Number getLastUpdatedBy()
    {
        return (Number)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.LASTUPDATEDBY);
    }

    public void setLastUpdatedBy(Number number)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.LASTUPDATEDBY, number);
    }

    public Date getCreationDate()
    {
        return (Date)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.CREATIONDATE);
    }

    public void setCreationDate(Date date)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.CREATIONDATE, date);
    }

    public Number getCreatedBy()
    {
        return (Number)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.CREATEDBY);
    }

    public void setCreatedBy(Number number)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.CREATEDBY, number);
    }

    public Number getLastUpdateLogin()
    {
        return (Number)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.LASTUPDATELOGIN);
    }

    public void setLastUpdateLogin(Number number)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.LASTUPDATELOGIN, number);
    }

    public Number getRequestId()
    {
        return (Number)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.REQUESTID);
    }

    public void setRequestId(Number number)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.REQUESTID, number);
    }

    public Number getProgramApplicationId()
    {
        return (Number)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.PROGRAMAPPLICATIONID);
    }

    public void setProgramApplicationId(Number number)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.PROGRAMAPPLICATIONID, number);
    }

    public Number getProgramId()
    {
        return (Number)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.PROGRAMID);
    }

    public void setProgramId(Number number)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.PROGRAMID, number);
    }

    public Date getProgramUpdateDate()
    {
        return (Date)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.PROGRAMUPDATEDATE);
    }

    public void setProgramUpdateDate(Date date)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.PROGRAMUPDATEDATE, date);
    }

    public Date getWhUpdateDate()
    {
        return (Date)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.WHUPDATEDATE);
    }

    public void setWhUpdateDate(Date date)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.WHUPDATEDATE, date);
    }

    public String getAttributeCategory()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.ATTRIBUTECATEGORY);
    }

    public void setAttributeCategory(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.ATTRIBUTECATEGORY, s);
    }

    public String getAttribute1()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.ATTRIBUTE1);
    }

    public void setAttribute1(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.ATTRIBUTE1, s);
    }

    public String getAttribute2()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.ATTRIBUTE2);
    }

    public void setAttribute2(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.ATTRIBUTE2, s);
    }

    public String getAttribute3()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.ATTRIBUTE3);
    }

    public void setAttribute3(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.ATTRIBUTE3, s);
    }

    public String getAttribute4()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.ATTRIBUTE4);
    }

    public void setAttribute4(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.ATTRIBUTE4, s);
    }

    public String getAttribute5()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.ATTRIBUTE5);
    }

    public void setAttribute5(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.ATTRIBUTE5, s);
    }

    public String getAttribute6()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.ATTRIBUTE6);
    }

    public void setAttribute6(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.ATTRIBUTE6, s);
    }

    public String getAttribute7()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.ATTRIBUTE7);
    }

    public void setAttribute7(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.ATTRIBUTE7, s);
    }

    public String getAttribute8()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.ATTRIBUTE8);
    }

    public void setAttribute8(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.ATTRIBUTE8, s);
    }

    public String getAttribute9()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.ATTRIBUTE9);
    }

    public void setAttribute9(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.ATTRIBUTE9, s);
    }

    public String getAttribute10()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.ATTRIBUTE10);
    }

    public void setAttribute10(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.ATTRIBUTE10, s);
    }

    public String getAttribute11()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.ATTRIBUTE11);
    }

    public void setAttribute11(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.ATTRIBUTE11, s);
    }

    public String getAttribute12()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.ATTRIBUTE12);
    }

    public void setAttribute12(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.ATTRIBUTE12, s);
    }

    public String getAttribute13()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.ATTRIBUTE13);
    }

    public void setAttribute13(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.ATTRIBUTE13, s);
    }

    public String getAttribute14()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.ATTRIBUTE14);
    }

    public void setAttribute14(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.ATTRIBUTE14, s);
    }

    public String getAttribute15()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.ATTRIBUTE15);
    }

    public void setAttribute15(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.ATTRIBUTE15, s);
    }

    public String getAttribute16()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.ATTRIBUTE16);
    }

    public void setAttribute16(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.ATTRIBUTE16, s);
    }

    public String getAttribute17()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.ATTRIBUTE17);
    }

    public void setAttribute17(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.ATTRIBUTE17, s);
    }

    public String getAttribute18()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.ATTRIBUTE18);
    }

    public void setAttribute18(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.ATTRIBUTE18, s);
    }

    public String getAttribute19()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.ATTRIBUTE19);
    }

    public void setAttribute19(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.ATTRIBUTE19, s);
    }

    public String getAttribute20()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.ATTRIBUTE20);
    }

    public void setAttribute20(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.ATTRIBUTE20, s);
    }

    public String getGlobalAttributeCategory()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.GLOBALATTRIBUTECATEGORY);
    }

    public void setGlobalAttributeCategory(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.GLOBALATTRIBUTECATEGORY, s);
    }

    public String getGlobalAttribute1()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.GLOBALATTRIBUTE1);
    }

    public void setGlobalAttribute1(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.GLOBALATTRIBUTE1, s);
    }

    public String getGlobalAttribute2()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.GLOBALATTRIBUTE2);
    }

    public void setGlobalAttribute2(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.GLOBALATTRIBUTE2, s);
    }

    public String getGlobalAttribute3()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.GLOBALATTRIBUTE3);
    }

    public void setGlobalAttribute3(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.GLOBALATTRIBUTE3, s);
    }

    public String getGlobalAttribute4()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.GLOBALATTRIBUTE4);
    }

    public void setGlobalAttribute4(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.GLOBALATTRIBUTE4, s);
    }

    public String getGlobalAttribute5()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.GLOBALATTRIBUTE5);
    }

    public void setGlobalAttribute5(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.GLOBALATTRIBUTE5, s);
    }

    public String getGlobalAttribute6()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.GLOBALATTRIBUTE6);
    }

    public void setGlobalAttribute6(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.GLOBALATTRIBUTE6, s);
    }

    public String getGlobalAttribute7()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.GLOBALATTRIBUTE7);
    }

    public void setGlobalAttribute7(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.GLOBALATTRIBUTE7, s);
    }

    public String getGlobalAttribute8()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.GLOBALATTRIBUTE8);
    }

    public void setGlobalAttribute8(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.GLOBALATTRIBUTE8, s);
    }

    public String getGlobalAttribute9()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.GLOBALATTRIBUTE9);
    }

    public void setGlobalAttribute9(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.GLOBALATTRIBUTE9, s);
    }

    public String getGlobalAttribute10()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.GLOBALATTRIBUTE10);
    }

    public void setGlobalAttribute10(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.GLOBALATTRIBUTE10, s);
    }

    public String getGlobalAttribute11()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.GLOBALATTRIBUTE11);
    }

    public void setGlobalAttribute11(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.GLOBALATTRIBUTE11, s);
    }

    public String getGlobalAttribute12()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.GLOBALATTRIBUTE12);
    }

    public void setGlobalAttribute12(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.GLOBALATTRIBUTE12, s);
    }

    public String getGlobalAttribute13()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.GLOBALATTRIBUTE13);
    }

    public void setGlobalAttribute13(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.GLOBALATTRIBUTE13, s);
    }

    public String getGlobalAttribute14()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.GLOBALATTRIBUTE14);
    }

    public void setGlobalAttribute14(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.GLOBALATTRIBUTE14, s);
    }

    public String getGlobalAttribute15()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.GLOBALATTRIBUTE15);
    }

    public void setGlobalAttribute15(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.GLOBALATTRIBUTE15, s);
    }

    public String getGlobalAttribute16()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.GLOBALATTRIBUTE16);
    }

    public void setGlobalAttribute16(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.GLOBALATTRIBUTE16, s);
    }

    public String getGlobalAttribute17()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.GLOBALATTRIBUTE17);
    }

    public void setGlobalAttribute17(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.GLOBALATTRIBUTE17, s);
    }

    public String getGlobalAttribute18()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.GLOBALATTRIBUTE18);
    }

    public void setGlobalAttribute18(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.GLOBALATTRIBUTE18, s);
    }

    public String getGlobalAttribute19()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.GLOBALATTRIBUTE19);
    }

    public void setGlobalAttribute19(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.GLOBALATTRIBUTE19, s);
    }

    public String getGlobalAttribute20()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.GLOBALATTRIBUTE20);
    }

    public void setGlobalAttribute20(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.GLOBALATTRIBUTE20, s);
    }

    public String getOrigSystemReference()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.ORIGSYSTEMREFERENCE);
    }

    public void setOrigSystemReference(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.ORIGSYSTEMREFERENCE, s);
    }

    public String getStatus()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.STATUS);
    }

    public void setStatus(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.STATUS, s);
    }

    public Number getOrgId()
    {
        return (Number)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.ORGID);
    }

    public void setOrgId(Number number)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.ORGID, number);
    }

    public String getBillToFlag()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.BILLTOFLAG);
    }

    public void setBillToFlag(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.BILLTOFLAG, s);
    }

    public String getMarketFlag()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.MARKETFLAG);
    }

    public void setMarketFlag(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.MARKETFLAG, s);
    }

    public String getShipToFlag()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.SHIPTOFLAG);
    }

    public void setShipToFlag(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.SHIPTOFLAG, s);
    }

    public String getCustomerCategoryCode()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.CUSTOMERCATEGORYCODE);
    }

    public void setCustomerCategoryCode(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.CUSTOMERCATEGORYCODE, s);
    }

    public String getLanguage()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.LANGUAGE);
    }

    public void setLanguage(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.LANGUAGE, s);
    }

    public String getKeyAccountFlag()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.KEYACCOUNTFLAG);
    }

    public void setKeyAccountFlag(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.KEYACCOUNTFLAG, s);
    }

    public Number getTpHeaderId()
    {
        return (Number)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.TPHEADERID);
    }

    public void setTpHeaderId(Number number)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.TPHEADERID, number);
    }

    public String getEceTpLocationCode()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.ECETPLOCATIONCODE);
    }

    public void setEceTpLocationCode(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.ECETPLOCATIONCODE, s);
    }

    public Number getServiceTerritoryId()
    {
        return (Number)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.SERVICETERRITORYID);
    }

    public void setServiceTerritoryId(Number number)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.SERVICETERRITORYID, number);
    }

    public Number getPrimarySpecialistId()
    {
        return (Number)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.PRIMARYSPECIALISTID);
    }

    public void setPrimarySpecialistId(Number number)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.PRIMARYSPECIALISTID, number);
    }

    public Number getSecondarySpecialistId()
    {
        return (Number)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.SECONDARYSPECIALISTID);
    }

    public void setSecondarySpecialistId(Number number)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.SECONDARYSPECIALISTID, number);
    }

    public Number getTerritoryId()
    {
        return (Number)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.TERRITORYID);
    }

    public void setTerritoryId(Number number)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.TERRITORYID, number);
    }

    public ClobDomain getAddressText()
    {
        return (ClobDomain)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.ADDRESSTEXT);
    }

    public void setAddressText(ClobDomain clobdomain)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.ADDRESSTEXT, clobdomain);
    }

    public String getTerritory()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.TERRITORY);
    }

    public void setTerritory(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.TERRITORY, s);
    }

    public String getTranslatedCustomerName()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.TRANSLATEDCUSTOMERNAME);
    }

    public void setTranslatedCustomerName(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.TRANSLATEDCUSTOMERNAME, s);
    }

    public Number getObjectVersionNumber()
    {
        return (Number)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.OBJECTVERSIONNUMBER);
    }

    public void setObjectVersionNumber(Number number)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.OBJECTVERSIONNUMBER, number);
    }

    public String getCreatedByModule()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.CREATEDBYMODULE);
    }

    public void setCreatedByModule(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.CREATEDBYMODULE, s);
    }

    public Number getApplicationId()
    {
        return (Number)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.APPLICATIONID);
    }

    public void setApplicationId(Number number)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.APPLICATIONID, number);
    }

    public String getOrigSystem()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.ORIGSYSTEM);
    }

    public void setOrigSystem(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.ORIGSYSTEM, s);
    }

    public String getIsPrivilegeChecked()
    {
        return super.getIsPrivilegeChecked(null);
    }

    public void setIsPrivilegeChecked(String s)
    {
        super.setIsPrivilegeChecked(s);
    }

    public String getUpdateImage()
    {
        return super.getUpdateImage();
    }

    public void setUpdateImage(String s)
    {
        super.setUpdateImage(s);
    }

    public String getDeleteImage()
    {
        if(getSiteStatusMeaning() != null && getSiteStatusMeaning().equals("Inactive"))
        {
            return "DeleteDisabled";
        } else
        {
            return super.getDeleteImage();
        }
    }

    public void setDeleteImage(String s)
    {
        super.setDeleteImage(s);
    }

    public String getRestoreImage()
    {
        if(getSiteStatusMeaning() != null && getSiteStatusMeaning().equals("Active"))
        {
            return "RestoreDisabled";
        } else
        {
            return super.getRestoreImage();
        }
    }

    public void setRestoreImage(String s)
    {
        super.setRestoreImage(s);
    }

    public String getViewImage()
    {
        return "ViewEnabled";
    }

    public void setViewImage(String s1)
    {
    }

    public String getIsFormatted()
    {
        HashMap hashmap = new HashMap(10);
        Number number = getCustAcctSiteId();
        if(number != null)
        {
            hashmap.put("CustAcctSiteId", number.toString());
        }
        return super.getIsFormatted(hashmap);
    }

    public void setIsFormatted(String s)
    {
        super.setIsFormatted(s);
    }

    public String getFormattedPhone()
    {
        return super.getFormattedPhone();
    }

    public void setFormattedPhone(String s)
    {
        super.setFormattedPhone(s);
    }

    public String getFormattedAddress()
    {
        return super.getFormattedAddress();
    }

    public void setFormattedAddress(String s)
    {
        super.setFormattedAddress(s);
    }

    public String getOrgContactRoles()
    {
        return super.getOrgContactRoles();
    }

    public void setOrgContactRoles(String s)
    {
        super.setOrgContactRoles(s);
    }

    public String getSelectFlag()
    {
        return super.getSelectFlag();
    }

    public void setSelectFlag(String s)
    {
        super.setSelectFlag(s);
    }

    public String getMailstop()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.MAILSTOP);
    }

    public void setMailstop(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.MAILSTOP, s);
    }

    public String getCountry()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.COUNTRY);
    }

    public void setCountry(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.COUNTRY, s);
    }

    public String getPartySiteName()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.PARTYSITENAME);
    }

    public void setPartySiteName(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.PARTYSITENAME, s);
    }

    public String getAddressLinesPhonetic()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.ADDRESSLINESPHONETIC);
    }

    public void setAddressLinesPhonetic(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.ADDRESSLINESPHONETIC, s);
    }

    public String getIdentifyingAddressFlag()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.IDENTIFYINGADDRESSFLAG);
    }

    public void setIdentifyingAddressFlag(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.IDENTIFYINGADDRESSFLAG, s);
    }

    public String getPurposes()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.PURPOSES);
    }

    public void setPurposes(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.PURPOSES, s);
    }

    public Number getLocationId()
    {
        return (Number)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.LOCATIONID);
    }

    public void setLocationId(Number number)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.LOCATIONID, number);
    }

    public String getCountryName()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.COUNTRYNAME);
    }

    public void setCountryName(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.COUNTRYNAME, s);
    }

    public String getMapURL()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.MAPURL);
    }

    public void setMapURL(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.MAPURL, s);
    }

    public String getOperatingUnit()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.OPERATINGUNIT);
    }

    public void setOperatingUnit(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.OPERATINGUNIT, s);
    }

    public String getSiteSourceContactName()
    {
        OAApplicationModuleImpl oaapplicationmoduleimpl = (OAApplicationModuleImpl)getApplicationModule();
        String s = null;
        Number number = getCustAccountId();
        if(number != null)
        {
            Serializable aserializable[] = {
                number
            };
            Class aclass[] = {
                number.getClass()
            };
            s = (String)oaapplicationmoduleimpl.invokeMethod("getSiteSourceContactName", aserializable, aclass);
        }
        return s;
    }

    public void setSiteSourceContactName(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.SITESOURCECONTACTNAME, s);
    }

    public String getPartySiteNumber()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.PARTYSITENUMBER);
    }

    public void setPartySiteNumber(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.PARTYSITENUMBER, s);
    }

    public String getSiteStatusMeaning()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.SITESTATUSMEANING);
    }

    public void setSiteStatusMeaning(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.SITESTATUSMEANING, s);
    }

    public String getAddress1()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.ADDRESS1);
    }

    public void setAddress1(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.ADDRESS1, s);
    }

    public String getAddress2()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.ADDRESS2);
    }

    public void setAddress2(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.ADDRESS2, s);
    }

    public String getAddress3()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.ADDRESS3);
    }

    public void setAddress3(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.ADDRESS3, s);
    }

    public String getAddress4()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.ADDRESS4);
    }

    public void setAddress4(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.ADDRESS4, s);
    }

    public String getCity()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.CITY);
    }

    public void setCity(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.CITY, s);
    }

    public String getState()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.STATE);
    }

    public void setState(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.STATE, s);
    }

    public String getPostalCode()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.POSTALCODE);
    }

    public void setPostalCode(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.POSTALCODE, s);
    }

    public String getCounty()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.COUNTY);
    }

    public void setCounty(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.COUNTY, s);
    }

    public String getProvince()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.PROVINCE);
    }

    public void setProvince(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.PROVINCE, s);
    }

    public String getAddress()
    {
        return (String)getAttributeInternal(HzPuiAcctSitesTableVORowImpl.ADDRESS);
    }

    public void setAddress(String s)
    {
        setAttributeInternal(HzPuiAcctSitesTableVORowImpl.ADDRESS, s);
    }

    protected Object getAttrInvokeAccessor(int index, AttributeDefImpl attrDef)
        throws Exception
    {
        if (index == PRIMARY_BT) {
            return getPRIMARY_BT();
        }
        if (index == PRELIM) {
            return getPRELIM();
        }
        if (index == PRISM_SITE_NUMBER) {
            return getPRISM_SITE_NUMBER();
        }
        if (index == CR_HOLD) {
            return getCR_HOLD();
        }
        if (index == DUMMY) {
            return getDUMMY();
        }
        if (index == CUST_SITE_NAME) {
            return getCUST_SITE_NAME();
        }
        return super.getAttrInvokeAccessor(index, attrDef);
    }

    protected void setAttrInvokeAccessor(int index, Object value, AttributeDefImpl attrDef)
        throws Exception
    {
        if (index == PRIMARY_BT) {
            setPRIMARY_BT((String)value);
            return;
        }
        if (index == PRELIM) {
            setPRELIM((String)value);
            return;
        }
        if (index == PRISM_SITE_NUMBER) {
            setPRISM_SITE_NUMBER((String)value);
            return;
        }
        if (index == CR_HOLD) {
            setCR_HOLD((String)value);
            return;
        }
        if (index == DUMMY) {
            setDUMMY((String)value);
            return;
        }
        if (index == CUST_SITE_NAME) {
            setCUST_SITE_NAME((String)value);
            return;
        }
        super.setAttrInvokeAccessor(index, value, attrDef);
        return;
    }

    public HzCustAccountSitesEOImpl getHzCustAccountSitesEOEx()
    {
        return (HzCustAccountSitesEOExImpl)getEntity(0);
    }

    public String getActContactRoles()
    {
        return (String)getAttributeInternal("ActContactRoles");
    }

    public void setActContactRoles(String value)
    {
        setAttributeInternal("ActContactRoles", value);
    }

    public String getDoNotCallIndicator()
    {
        return (String)getAttributeInternal("DoNotCallIndicator");
    }

    public void setDoNotCallIndicator(String value)
    {
        setAttributeInternal("DoNotCallIndicator", value);
    }

    public String getIsRoleInRelationshipGroup()
    {
        return (String)getAttributeInternal("IsRoleInRelationshipGroup");
    }

    public void setIsRoleInRelationshipGroup(String value)
    {
        setAttributeInternal("IsRoleInRelationshipGroup", value);
    }

    public String getBillTo_primary()
    {
        return (String)getAttributeInternal("BillTo_primary");
    }

    public void setBillTo_primary(String value)
    {
        setAttributeInternal("BillTo_primary", value);
    }

    public String getPRIMARY_BT()
    {
        return (String)getAttributeInternal(PRIMARY_BT);
    }

    public void setPRIMARY_BT(String value)
    {
        setAttributeInternal(PRIMARY_BT, value);
    }

    public String getPRELIM()
    {
        return (String)getAttributeInternal(PRELIM);
    }

    public void setPRELIM(String value)
    {
        setAttributeInternal(PRELIM, value);
    }

    public String getPRISM_SITE_NUMBER()
    {
        return (String)getAttributeInternal(PRISM_SITE_NUMBER);
    }

    public void setPRISM_SITE_NUMBER(String value)
    {
        setAttributeInternal(PRISM_SITE_NUMBER, value);
    }

    public String getCR_HOLD()
    {
        return (String)getAttributeInternal(CR_HOLD);
    }

    public void setCR_HOLD(String value)
    {
        setAttributeInternal(CR_HOLD, value);
    }

    public String getDUMMY()
    {
        return (String)getAttributeInternal(DUMMY);
    }

    public void setDUMMY(String value)
    {
        setAttributeInternal(DUMMY, value);
    }

    public String getCUST_SITE_NAME()
    {
        return (String)getAttributeInternal(CUST_SITE_NAME);
    }

    public void setCUST_SITE_NAME(String value)
    {
        setAttributeInternal(CUST_SITE_NAME, value);
    }


}
