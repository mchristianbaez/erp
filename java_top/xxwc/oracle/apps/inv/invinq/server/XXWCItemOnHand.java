/*************************************************************************
   *   $Header XXWCItemOnHand.java $
   *   Module Name: XXWCItemOnHandFListener
   *   
   *   Package xxwc.oracle.apps.inv.invinq.server;
   *   
   *   Imports Classes
   *   
   *   import oracle.apps.fnd.common.VersionInfo;
   *  
   *   PURPOSE:   XXWCItemOnHand returns values from query
   *   
   *    
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
        1.0       02-MAR-2015   Lee Spitzer             Initial Version - 
                                                           TMS Ticket 20150302-00175 - RF - Inventory Inquiry   
**************************************************************************/
package xxwc.oracle.apps.inv.invinq.server;

import oracle.apps.fnd.common.VersionInfo;

/*************************************************************************
 *   NAME: public class XXWCItemOnHand
 *
 *   PURPOSE:   Main class for XXWCItemOnHand 
 *
 *   REVISIONS:
 *   Ver        Date        Author                     Description
 *   ---------  ----------  ---------------         -------------------------
        1.0       02-MAR-2015   Lee Spitzer             Initial Version -
                                                         TMS Ticket 20150302-00175 - RF - Inventory Inquiry   
 **************************************************************************/

public class XXWCItemOnHand {
    public static final String RCS_ID = "$Header: ItemOnhand.java 120.1 2006/03/22 03:16:04 rsagar noship $";
    public static final boolean RCS_ID_RECORDED =
        VersionInfo.recordClassVersion("$Header: ItemOnhand.java 120.1 2006/03/22 03:16:04 rsagar noship $",
                                       "oracle.apps.inv.invinq.server");

    String mSubinventoryCode;
    String mLocatorId;
    String mLocatorName;
    String mLotNumber;
    String mAvailable;
    String mOnHand;
    String mOnOrder;

    /*************************************************************************
     *   NAME: XXWCItemOnHand
     *
     *   PURPOSE:   public method XXWCItemOnHand if no values passed
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       02-MAR-2015   Lee Spitzer             Initial Version -
     **************************************************************************/

    public XXWCItemOnHand() {
        this("", "", "", "", "", "", "");
    }


     /*************************************************************************
     *   NAME: XXWCItemOnHand(String x_item, Long x_item_id, String x_description, String x_subinventory_code,
                                    String x_bin_locator, Long x_locator_id, Long x_default_type, Long x_max_qty, String x_default_description)
     *
     *   PURPOSE:   public method XXWCItemOnHand if values passed
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       02-MAR-2015   Lee Spitzer             Initial Version -
     **************************************************************************/
    
     public XXWCItemOnHand(String x_subinventory_code, String x_locator_id, String x_locator_name, String x_lot_number, String x_available, String x_on_hand, String x_on_order) {
        mSubinventoryCode = x_subinventory_code;
        mLocatorId = x_locator_id;
        mLocatorName = x_locator_name;
        mLotNumber = x_lot_number;
        mAvailable = x_available;
        mOnHand = x_on_hand;
        mOnOrder = x_on_order;
    }

    /*************************************************************************
    *   NAME: public String getmSubinventoryCode()
    *
    *   PURPOSE:   returns the subinventory
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       18-FEB-2015   Lee Spitzer             Initial Version -
    **************************************************************************/
          
    public String getmSubinventoryCode() {
       return mSubinventoryCode;
    }
    
    /*************************************************************************
    *   NAME: public String getmLocatorId()
    *
    *   PURPOSE:   returns the mLocatorId
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       18-FEB-2015   Lee Spitzer             Initial Version -
    **************************************************************************/
          
    public String getmLocatorId() {
       return mLocatorId;
    }
    
    /*************************************************************************
     *   NAME: public String getmLocatorName() 
     *
     *   PURPOSE:   returns the mLocatorName
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/

    public String getmLocatorName() {
        return mLocatorName;
    }

    /*************************************************************************
    *   NAME: public String getmLotNumber()
    *
    *   PURPOSE:   returns the mLotNumber
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       18-FEB-2015   Lee Spitzer             Initial Version -
    **************************************************************************/
          
    public String getmLotNumber() {
       return mLotNumber;
    }

    /*************************************************************************
    *   NAME: public String getmAvailable()
    *
    *   PURPOSE:   returns the mAvailable
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       18-FEB-2015   Lee Spitzer             Initial Version -
    **************************************************************************/
          
    public String getmAvailable() {
       return mAvailable;
    }


    /*************************************************************************
    *   NAME: public String getmOnHand()
    *
    *   PURPOSE:   returns the Asset Type
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       18-FEB-2015   Lee Spitzer             Initial Version -
    **************************************************************************/
          
    public String getmOnHand() {
       return mOnHand;
    }

    /*************************************************************************
    *   NAME: public String getmOnOrder()
    *
    *   PURPOSE:   returns the mOnOrder
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       18-FEB-2015   Lee Spitzer             Initial Version -
    **************************************************************************/
          
    public String getmOnOrder() {
       return mOnOrder;
    }

}
