/*************************************************************************
   *   $Header XXWCItemOnHandIterator.java $
   *   Module Name: XXWCItemOnHandIterator
   *   
   *   Package xxwc.oracle.apps.inv.invinq.server;
   *   
   *   Imports Classes
   *   
   *   import oracle.apps.fnd.common.VersionInfo;
   *   import oracle.apps.inv.invinq.server.ItemOnhand;
   *   import oracle.apps.inv.invinq.server.NextRecordException;
   *   import oracle.apps.inv.invinq.server.PreviousRecordException;
   *   import oracle.apps.inv.invinq.server.UnknownMaxLeft;
   *   import oracle.apps.inv.utilities.server.UtilFns;
   *   import oracle.apps.mwa.container.FileLogger;
   *  
   *   PURPOSE:   XXWCPrimaryBinAssignmentIterator controls the Next, Previous, and Current Position of the query results
   *   
   *    
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
        1.0       02-MAR-2015   Lee Spitzer             Initial Version - 
                                                           TMS Ticket 20150302-00175 - RF - Inventory Inquiry   
**************************************************************************/
package xxwc.oracle.apps.inv.invinq.server;

import java.util.Vector;

import oracle.apps.fnd.common.VersionInfo;
import oracle.apps.inv.invinq.server.ItemOnhand;
import oracle.apps.inv.invinq.server.NextRecordException;
import oracle.apps.inv.invinq.server.PreviousRecordException;
import oracle.apps.inv.invinq.server.UnknownMaxLeft;
import oracle.apps.inv.utilities.server.UtilFns;
import oracle.apps.mwa.container.FileLogger;

/*************************************************************************
 *   NAME: public class XXWCItemOnHandIterator
 *
 *   PURPOSE:   Main class for XXWCItemOnHandIterator 
 *
 *   REVISIONS:
 *   Ver        Date        Author                     Description
 *   ---------  ----------  ---------------         -------------------------
        1.0       02-MAR-2015   Lee Spitzer             Initial Version -
                                                         TMS Ticket 20150302-00175 - RF - Inventory Inquiry   
 **************************************************************************/

public class XXWCItemOnHandIterator {
    public static final String RCS_ID = "$Header: XXWCItemOnHandIterator.java 120.0 2005/05/25 05:18:03 appldev noship $";
    public static final boolean RCS_ID_RECORDED =
        VersionInfo.recordClassVersion("$Header: XXWCItemOnHandIterator.java 120.0 2005/05/25 05:18:03 appldev noship $",
                                       "xxwc.oracle.apps.inv.invinq.server");
    private Vector mItemOnHandSeq;
    private int mCurrentPos;

    /*************************************************************************
     *   NAME: private static String gPackage = "xxwc.oracle.apps.inv.invinq.server";
     *
     *   PURPOSE:   private method to default gPackage value for debugging
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       02-MAR-2015   Lee Spitzer             Initial Version -
     **************************************************************************/

    private static String gPackage = "xxwc.oracle.apps.inv.invinq.server.XXWCItemOnHandIterator";


    /*************************************************************************
     *   NAME: private static String gCallFrom = "XXWCPrimaryBinAssignmentIterator";
     *
     *   PURPOSE:   private method to pass gCallFrom from methods to return class name CustomListener
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       02-MAR-2015   Lee Spitzer             Initial Version -
     **************************************************************************/

    private static String gCallFrom = "XXWCItemOnHandIterator";
    
    
    /*************************************************************************
     *   NAME: public XXWCPrimaryBinAssignmentIterator(Vector vector)
     *
     *   PURPOSE:   publice method to set mXXWCPrimaryBinAssignmentSeq in the vector and mCurrentPos
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       02-MAR-2015   Lee Spitzer             Initial Version -
     **************************************************************************/
    
    public XXWCItemOnHandIterator(Vector vector) {
        String gMethod = "XXWCItemOnHandIterator";
        String gCallPoint = "Start";
        if (vector == null) {
            gCallPoint = "vector == null";
            mItemOnHandSeq = new Vector();
        } else {
            gCallPoint = "vector != null";
            mItemOnHandSeq = vector;
        }
        FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
        mCurrentPos = 0;
    }

    /*************************************************************************
     *   NAME: int max_left() throws UnknownMaxLeft
     *
     *   PURPOSE:   publice method get the max_left value
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       02-MAR-2015   Lee Spitzer             Initial Version -
     **************************************************************************/

    public int max_left() throws UnknownMaxLeft {
        String gMethod = "max_left";
        String gCallPoint = "Start";
        if (mItemOnHandSeq == null) {
            gCallPoint = "mItemOnHandSeq == null";
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
            throw new UnknownMaxLeft();
        } else {
            gCallPoint = "mItemOnHandSeq != null";
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " mItemOnHandSeq.size() " + mItemOnHandSeq.size());
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " mCurrentPos " +mCurrentPos);
            return mItemOnHandSeq.size() - mCurrentPos;
        }
    }


    /*************************************************************************
     *   NAME: public XXWCPrimaryBinAssignment next() throws NextRecordException
     *
     *   PURPOSE:   publice method to the next sequence in the vector
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       02-MAR-2015   Lee Spitzer             Initial Version -
     **************************************************************************/

    public XXWCItemOnHand next() throws NextRecordException {
        boolean flag;
        try {
            int i = max_left();
            flag = i > 0;
        } catch (UnknownMaxLeft unknownmaxleft) {
            UtilFns.log((new StringBuilder()).append("Error in XXWCItemOnHand.next ").append(unknownmaxleft).toString());
            throw new NextRecordException();
        }
        XXWCItemOnHand XXWCItemOnHand;
        if (flag && mCurrentPos <= mItemOnHandSeq.size() - 1) {
            XXWCItemOnHand =
                    (XXWCItemOnHand)mItemOnHandSeq.elementAt(mCurrentPos++);
        } else {
            throw new NextRecordException();
        }
        return XXWCItemOnHand;
    }

    /*************************************************************************
     *   NAME: public XXWCPrimaryBinAssignment previous() throws NextRecordException
     *
     *   PURPOSE:   publice method to the previous sequence in the vector
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       02-MAR-2015   Lee Spitzer             Initial Version -
     **************************************************************************/
    
    public XXWCItemOnHand previous() throws PreviousRecordException {
        int i = mItemOnHandSeq.size();
        if (mCurrentPos == 0) {
            throw new PreviousRecordException();
        } else {
            mCurrentPos = mCurrentPos - 1;
            XXWCItemOnHand XXWCItemOnHand = (XXWCItemOnHand)mItemOnHandSeq.elementAt(mCurrentPos - 1);
            return XXWCItemOnHand;
        }
    }

    /*************************************************************************
     *   NAME: public XXWCItemOnHand getCurrentRecord() throws NextRecordException
     *
     *   PURPOSE:   publice method to the current record sequence in the vector
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       02-MAR-2015   Lee Spitzer             Initial Version -
     **************************************************************************/

    public XXWCItemOnHand getCurrentRecord() {
        return (XXWCItemOnHand)mItemOnHandSeq.elementAt(mCurrentPos - 1);
    }

    /*************************************************************************
     *   NAME: public public void destroy() throws NextRecordException
     *
     *   PURPOSE:   public method to destroy the mXXWCPrimaryBinAssignmentSeq to null
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       02-MAR-2015   Lee Spitzer             Initial Version -
     **************************************************************************/
    
    public void destroy() {
        mItemOnHandSeq = null;
        UtilFns.log("OfferIterator is destroyed");
    }

    /*************************************************************************
     *   NAME: public int getCurrentPos()
     *
     *   PURPOSE:   return the mCurrentPos;
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       02-MAR-2015   Lee Spitzer             Initial Version -
     **************************************************************************/
    
    public int getCurrentPos() {
        return mCurrentPos;
    }

    /*************************************************************************
     *   NAME: public int getSize()
     *
     *   PURPOSE:   return the mXXWCPrimaryBinAssignmentSeq size of the vector;
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       02-MAR-2015   Lee Spitzer             Initial Version -
     **************************************************************************/

    public int getSize() {
        return mItemOnHandSeq.size();
    }

}
