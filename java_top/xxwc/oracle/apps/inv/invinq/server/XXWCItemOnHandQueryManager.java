/*************************************************************************
   *   $Header XXWCItemOnHandQueryManager.java $
   *   Module Name: XXWCItemOnHandQueryManager
   *   
   *   Package: package xxwc.oracle.apps.inv.invinq.server;
   *   
   *   Imports Classes:
   *   
   *   import java.sql.*;
   *   import java.util.Hashtable;
   *   import java.util.Vector;
   *   import oracle.apps.fnd.common.VersionInfo;
   *   import oracle.apps.inv.invinq.server.ItemOnhand;
   *   import oracle.apps.inv.utilities.server.UtilFns;
   *   import oracle.apps.mwa.container.FileLogger;
   *   import oracle.jdbc.OracleCallableStatement;
   *   import oracle.jdbc.OraclePreparedStatement;

   *
   *   PURPOSE:   Java Class for XXWCItemOnHandQueryManager.  This calls XXWC_INV_PRIMARY_BIN_PKG.GET_MILD_INQUIRY and controls parameters and result values
   *    
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
        1.0       02-MAR-2015   Lee Spitzer             Initial Version - 
                                                            TMS Ticket 20150302-00175 - RF - Inventory Inquiry   
**************************************************************************/

package xxwc.oracle.apps.inv.invinq.server;

import java.sql.*;
import java.util.Hashtable;
import java.util.Vector;
import oracle.apps.fnd.common.VersionInfo;
import oracle.apps.inv.invinq.server.ItemOnhand;
import oracle.apps.inv.utilities.server.UtilFns;
import oracle.apps.mwa.container.FileLogger;
import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OraclePreparedStatement;

import xxwc.oracle.apps.inv.bins.server.XXWCBinPutAway;


/*************************************************************************
 *   NAME: XXWCItemOnHandQueryManager
 *
 *   PURPOSE:   Main class for XXWCItemOnHandQueryManager
 *
 *   REVISIONS:
 *   Ver        Date        Author                     Description
 *   ---------  ----------  ---------------         -------------------------
        1.0       02-MAR-2015   Lee Spitzer             Initial Version -
                                                            TMS Ticket 20150302-00175 - RF - Inventory Inquiry   
 **************************************************************************/

public class XXWCItemOnHandQueryManager {

    public static final String RCS_ID =
        "$Header: XXWCItemOnHandQueryManager.java 120.5 2007/12/19 15:33:21 vssrivat ship $";
    public static final boolean RCS_ID_RECORDED =
        VersionInfo.recordClassVersion("$Header: XXWCItemOnHandQueryManager.java 120.5 2007/12/19 15:33:21 vssrivat ship $",
                                       "xxwc.oracle.apps.inv.invinq.server");
    XXWCItemOnHand currentRecord;
    Connection con;

    /*************************************************************************
     *   NAME: private static String gPackage = "xxwc.oracle.apps.inv.invinq.server";
     *
     *   PURPOSE:   private method to default gPackage value for debugging
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       02-MAR-2015   Lee Spitzer             Initial Version -
     **************************************************************************/

    private static String gPackage = "xxwc.oracle.apps.inv.invinq.server.XXWCItemOnHandQueryManager";


    /*************************************************************************
     *   NAME: private static String gCallFrom = "XXWCItemOnHandQueryManager";
     *
     *   PURPOSE:   private method to pass gCallFrom from methods to return class name CustomListener
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       02-MAR-2015   Lee Spitzer             Initial Version -
     **************************************************************************/

    private static String gCallFrom = "XXWCItemOnHandQueryManager";

     /*************************************************************************
     *   NAME: public static String mWCOnHand
     *
     *   PURPOSE:   public method to call XXWC_MWA_ROUTINES_PKG.GET_WC_OH
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       02-MAR-2015   Lee Spitzer             Initial Version -
     **************************************************************************/

    public static String mWCOnHand =
        "BEGIN   XXWC_MWA_ROUTINES_PKG.GET_WC_OH(:1, :2, :3, :4, :5, :6, :7, :8); END; ";

    /*************************************************************************
    *   NAME: XXWCItemOnHandQueryManager(Connection connection)
    *
    *   PURPOSE:   public method to set the connection
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
             1.0       02-MAR-2015   Lee Spitzer             Initial Version -
    **************************************************************************/

    public XXWCItemOnHandQueryManager(Connection connection) {
        con = connection;
    }

    /*************************************************************************
    *   NAME:  public Vector getmWCBinLoc(String p_organization_id, String p_inventory_item_id,
                                              String p_subinventory_code, String p_locator_id, String p_default_type)
    *
    *   PURPOSE:   public method to input the parameters for the mtlItemLocatorDefaultStr and retrieve the resultset
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
             1.0       02-MAR-2015   Lee Spitzer             Initial Version -
    **************************************************************************/


    public Vector getmWCOnHand(String p_organization_id, String p_inventory_item_id, String p_subinventory_code,
                                               String p_locator_id, String p_lot_number, String p_project_id, String p_task_id) {
        String gMethod = "getmWCOnHand";
        String gCallPoint = "Start";
        FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
        Vector vector;
        OracleCallableStatement oraclecallablestatement;
        ResultSet resultset;
        vector = new Vector();
        oraclecallablestatement = null;
        resultset = null;
        try {
            gCallPoint = "Trying mWCOnHand";
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
            gCallPoint = "Getting resultset";
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " p_organization_id " +
                                               p_organization_id);
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " p_inventory_item_id " +
                                               p_inventory_item_id);
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " p_subinventory_code " +
                                               p_subinventory_code);
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " p_locator_id " +
                                               p_locator_id);
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " p_lot_number " +
                                               p_lot_number);
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " p_project_id " +
                                               p_project_id);
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " p_task_id " +
                                               p_task_id);
            oraclecallablestatement = (OracleCallableStatement)con.prepareCall(mWCOnHand);
            oraclecallablestatement.registerOutParameter(1, -10);
            oraclecallablestatement.setString(2, p_organization_id);
            oraclecallablestatement.setString(3, p_inventory_item_id);
            oraclecallablestatement.setString(4, p_subinventory_code);
            oraclecallablestatement.setString(5, p_locator_id);
            oraclecallablestatement.setString(6, p_lot_number);
            oraclecallablestatement.setString(7, p_project_id);
            oraclecallablestatement.setString(8, p_task_id);
            oraclecallablestatement.execute();
            resultset = (ResultSet)oraclecallablestatement.getObject(1);
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " resultset " +
                                               resultset);

            do {
                gCallPoint = "do";
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);

                if (!resultset.next()) {
                    gCallPoint = "!resultset.next";
                    FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
                    break;
                }
                gCallPoint = "Getting resultset";
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
                String x_subinventory_code = resultset.getString(1);
                String x_locator_id = resultset.getString(2);
                String x_locator_name = resultset.getString(3);
                String x_lot_number = resultset.getString(4);
                String x_available = resultset.getString(5);
                String x_on_hand = resultset.getString(6);
                String x_on_order = resultset.getString(7);
                
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " x_subinventory_code " + x_subinventory_code);
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " x_locator_id " + x_locator_id);
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " x_locator_name " + x_locator_name);
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " x_lot_number " + x_lot_number);
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " x_available " + x_available);
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " x_on_hand " + x_on_hand);
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " x_on_order " + x_on_order);
                currentRecord =
                        new XXWCItemOnHand(x_subinventory_code, x_locator_id, x_locator_name, x_lot_number, x_available, x_on_hand, x_on_order);
                vector.addElement(currentRecord);
            } while (true);
            gCallPoint = "while";
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
            if (resultset != null) {
                gCallPoint = "resultset.close";
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
                resultset.close();
            }
            if (oraclecallablestatement != null) {
                gCallPoint = "oraclecallablestatement is not null";
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint +
                                                   " oraclecallablestatement " + oraclecallablestatement);
                oraclecallablestatement.close();
            }
        } catch (Exception e) {
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " " + e);
        }
        gCallPoint = "Vector";
        FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " " + vector);
        return vector;
    }


}
