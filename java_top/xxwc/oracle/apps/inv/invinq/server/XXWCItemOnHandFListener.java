/*************************************************************************
   *   $Header XXWCItemOnHandFListener.java $
   *   Module Name: XXWCItemOnHandFListener
   *   
   *   Package: package xxwc.oracle.apps.inv.invinq.server;
   *   
   *   Imports Classes:
   *   
   *   import java.sql.*;
   *   import oracle.apps.inv.utilities.server.UtilFns;
   *   import oracle.apps.mwa.beans.*;
   *   import oracle.apps.mwa.container.MWALib;
   *   import oracle.apps.mwa.container.Session;
   *   import oracle.apps.mwa.eventmodel.*;
   *   import oracle.apps.mwa.container.FileLogger;
   *   import oracle.apps.fnd.flexj.FlexException;
   *   import oracle.apps.inv.lov.server.ItemLOV;
   *   import oracle.apps.inv.lov.server.LocatorKFF;
   *   import oracle.apps.inv.lov.server.SubinventoryLOV;
   *   import xxwc.oracle.apps.inv.lov.server.*;
   *
   *   PURPOSE:   Java Class for XXWCItemOnHandPage Page Layout.
   *    
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
        1.0       02-MAR-2015   Lee Spitzer             Initial Version - 
                                                          TMS Ticket 20150302-00175 - RF - Inventory Inquiry   
**************************************************************************/
package xxwc.oracle.apps.inv.invinq.server;

import java.sql.*;
import java.util.Hashtable;
import java.util.Vector;
import oracle.apps.fnd.common.VersionInfo;
import oracle.apps.inv.lov.server.*;
import oracle.apps.inv.utilities.server.*;
import oracle.apps.mwa.beans.*;
import oracle.apps.mwa.container.FileLogger;
import oracle.apps.mwa.container.Session;
import oracle.apps.mwa.eventmodel.*;
import oracle.apps.mwa.presentation.telnet.TelnetSession;
import oracle.jdbc.OraclePreparedStatement;
import oracle.sql.NUMBER;
import oracle.apps.mwa.container.MWALib;
import oracle.apps.fnd.flexj.KeyFlexfield;
import oracle.apps.inv.invinq.server.ItemOnhandIterator;
import oracle.apps.inv.invinq.server.ItemOnhandQueryManager;

import oracle.jdbc.OracleCallableStatement;

import xxwc.oracle.apps.inv.bins.server.XXWCBinInquiryPage;
import xxwc.oracle.apps.inv.bins.server.XXWCBinPutAway;
import xxwc.oracle.apps.inv.bins.server.XXWCBinPutAwayIterator;
import xxwc.oracle.apps.inv.bins.server.XXWCBinPutAwayQueryManager;
import xxwc.oracle.apps.inv.lov.server.XXWCBinsLOV;
import xxwc.oracle.apps.inv.lov.server.XXWCItemLOV;

/*************************************************************************
 *   NAME: XXWCItemOnHandFListener implements MWAFieldListener
 *
 *   PURPOSE:   Main class for XXWCItemOnHandFListener
 *
 *   REVISIONS:
 *   Ver        Date        Author                     Description
 *   ---------  ----------  ---------------         -------------------------
        1.0       02-MAR-2015   Lee Spitzer             Initial Version -
                                                          TMS Ticket 20150302-00175 - RF - Inventory Inquiry   
 **************************************************************************/


public class XXWCItemOnHandFListener implements MWAFieldListener{
    XXWCItemOnHandPage pg;
    Session ses;
    String dialogPageButtons[];

    /*************************************************************************
     *   NAME: private static String gPackage = "xxwc.oracle.apps.inv.invinq.server";
     *
     *   PURPOSE:   private method to default gPackage value for debugging
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       02-MAR-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00175 - RF - Inventory Inquiry   
     **************************************************************************/

    private static String gPackage = "xxwc.oracle.apps.inv.invinq.server";


    /*************************************************************************
     *   NAME: private static String gCallFrom = "XXWCItemOnHandFListener";
     *
     *   PURPOSE:   private method to pass gCallFrom from methods to return class
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       02-MAR-2015   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20150302-00175 - RF - Inventory Inquiry   
     **************************************************************************/

    private static String gCallFrom = "XXWCItemOnHandFListener";

    /*************************************************************************
     *   NAME: public XXWCItemOnHandFListener()
     *
     *   PURPOSE:   public method XXWCItemOnHandFListener
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       02-MAR-2015   Lee Spitzer             Initial Version - 
                                                               TMS Ticket 20150302-00175 - RF - Inventory Inquiry   
     **************************************************************************/

    public XXWCItemOnHandFListener() {
        $init$();
    }

    /*************************************************************************
     *   NAME:  public void fieldEntered(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                       DefaultOnlyHandlerException
     *
     *   PURPOSE:   field entered for XXWCItemOnHandFListener
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       02-MAR-2015   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20150302-00175 - RF - Inventory Inquiry   
     **************************************************************************/
    
    public void fieldEntered(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                       DefaultOnlyHandlerException {
        ses = mwaevent.getSession();
        pg = (XXWCItemOnHandPage)ses.getCurrentPage();
        String s = UtilFns.fieldEnterSource(ses);
        /*Item*/
        if (s.equals("INV.ITEM")) {
            enteredItem(mwaevent);
            return;
        }
        if (s.equals("XXWC.UPC")) {
            enteredUPC(mwaevent);
            return;
        }
        /*Sub*/
        if (s.equals("INV.SUB")) {
            enteredSub(mwaevent);
            return;
        }        
        /*Locator*/
        if (s.equals("INV.LOC")) {
            enteredLocator(mwaevent);
            return;
        }        
    }

    /*************************************************************************
     *   NAME:      public void fieldExited(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException
     *
     *   PURPOSE:   field exited for XXWCItemOnHandFListener
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       02-MAR-2015   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20150302-00175 - RF - Inventory Inquiry   
     **************************************************************************/

    public void fieldExited(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException {
        String s = ((FieldBean)mwaevent.getSource()).getName();
        String vField = mwaevent.getAction();
        boolean flag = false;
        boolean flag1 = false;
        boolean flag3 = false;
        if (mwaevent.getAction().equals("MWA_SUBMIT")) {
            flag = true;
        } else if (mwaevent.getAction().equals("MWA_PREVIOUSFIELD")) {
            flag1 = true;
        } else if (mwaevent.getAction().equals("MWA_NEXTFIELD")) {
            flag3 = true;
        }
        /*Item*/
        if (s.equals("INV.ITEM")) {
            exitedItem(mwaevent, ses);
            return;
        }
        if (s.equals("XXWC.UPC")) {
            exitedUPC(mwaevent, ses);
            return;
        }
        /*Sub*/
        if (s.equals("INV.SUB")) {
            exitedSub(mwaevent, ses);
            return;
        }
       /*Cancel*/
        if (flag && s.equals("XXWC.NEXT_ITEM")) {
            exitedNextItem(mwaevent, ses);
            return;
        }
        /*SaveNext*/
        if (flag && s.equals("XXWC.NEXT")) {
            exitedNext(mwaevent, ses);
            //clearFields(mwaevent, ses);
            return;
        }
        /*SaveNext*/
        if (flag && s.equals("XXWC.PREVIOUS")) {
            exitedPrevious(mwaevent, ses);
            //exitedNext(mwaevent, ses);
            //clearFields(mwaevent, ses);
            return;
        }
        /*SaveNext*/
        if (flag && s.equals("XXWC.BINS")) {
            exitedBins(mwaevent, ses);
            //clearFields(mwaevent, ses);
            return;
        }
        /*Done*/
        if (flag && s.equals("INV.CANCEL")) {
            exitedCancel(mwaevent, ses);
            return;
        }
        else {
            return;
        }
    }

    /*************************************************************************
     *   NAME:      private void $init$()
     *
     *   PURPOSE:   $init$ sets the diaglogButtons button for user prompt messages to OK
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
         1.0       02-MAR-2015   Lee Spitzer             Initial Version - 
                                                             TMS Ticket 20150302-00175 - RF - Inventory Inquiry   
     **************************************************************************/

    private void $init$() {
        dialogPageButtons = (new String[] { "OK" });
    }


    /*************************************************************************
     *   NAME: public void enteredItem(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException
     *
     *   PURPOSE:  Entered Item Field Listener Process to set the List of Values for the Item Field
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       02-MAR-2015   Lee Spitzer             Initial Version - 
                                                                  TMS Ticket 20150302-00175 - RF - Inventory Inquiry   
     **************************************************************************/


    public void enteredItem(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException {

        try {
            XXWCItemLOV itemlov = pg.getmItemFld();
            itemlov.setValidateFromLOV(true);
            itemlov.setlovStatement("XXWC_MWA_ROUTINES_PKG.GET_WC_ITEM_LOV"); //updated 5/11/2015 to GET_WC_ITEM_LOV query
            String paramType[] = { "C", "N", "S" };
            String parameters[] =
            { " ", "ORGID", "xxwc.oracle.apps.inv.invinq.server.XXWCItemOnHandPage.INV.ITEM" };
            itemlov.setInputParameterTypes(paramType);
            itemlov.setInputParameters(parameters);
        } catch (Exception e) {
            UtilFns.error("Error in calling enteredItem" + e);
        }        
    }

    /*************************************************************************
     *   NAME: public void exitedItem(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException
     *
     *   PURPOSE:  Exited Item Field Listener to populate the Item Description Field
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       02-MAR-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00175 - RF - Inventory Inquiry   
     **************************************************************************/

    public void exitedItem(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                  DefaultOnlyHandlerException {
        
        String gMethod = "exitedItem";
        String gCallPoint = "Start";
        FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
        
        String l_item = "";
        l_item = pg.getmItemFld().getValue();
        FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " l_item " + l_item);
        if (l_item.equals(null) || l_item.equals("")) {
            gCallPoint = "Item Fld is null";
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
            //pg.getmItemFld().setEditable(false); //removed 5/11/2015
            // pg.getmUPCFld().setEditable(true); //removed 5/11/2015
            
            /* removed 5/11/2015
            
            //added 5/11/2015
            TelnetSession telnetsessionX = (TelnetSession)ses;
            int k = telnetsessionX.showPromptPage("Error!", "You must enter an item to search. " , dialogPageButtons);
                
            //(new String[] { "OK" });
                if (k == 0) {
                    ses.setNextFieldName("INV.ITEM");
                    pg.getmItemFld().setValue("");
                    pg.getmItemDescFld().setValue("");
                    return;
                }
                else if (k == 1) {
                    ses.setStatusMessage(pg.WMS_TXN_CANCEL);
                    ses.clearAllApplicationScopeObjects();
                    ses.setStatusMessage(XXWCBinInquiryPage.WMS_TXN_CANCEL);
                    pg.getmItemFld().setNextPageName("|END_OF_TRANSACTION|");
                }
                else {
                    return;
                }
                */
        }
        else {
            gCallPoint = "Item Fld is not null";
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
            try {
                pg.getmItemFld().getValue().toUpperCase();
                pg.getmItemDescFld().setValue(pg.getmItemFld().getmItemDescription());
                pg.getmUOMFld().setValue(pg.getmItemFld().getmPrimaryUOMCode());
                onHandQuery(mwaevent, ses);
            } catch (Exception e) {
                UtilFns.error("Error in calling exitedItem" + e);
            }
        }
    }


    /*************************************************************************
     *   NAME: public void exitedItem(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException
     *
     *   PURPOSE:  Exited Item Field Listener to populate the Item Description Field
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       02-MAR-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00175 - RF - Inventory Inquiry   
     **************************************************************************/

    public void exitedUPC(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                  DefaultOnlyHandlerException {
        
        String gMethod = "exitedUPC";
        String gCallPoint = "Start";
        FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
        
        String l_item = "";
        l_item = pg.getmUPCFld().getValue();
        FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " l_item " + l_item);
        if (l_item.equals(null) || l_item.equals("")) {
            gCallPoint = "Item Fld is null";
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
            
            if (mwaevent.getAction().equals("MWA_PREVIOUSFIELD")) {
                
                pg.getmItemFld().setEditable(true);
                pg.getmUPCFld().setEditable(false);
                exitedNextItem(mwaevent, ses);

            }
            else {
            
             TelnetSession telnetsessionX = (TelnetSession)ses;
             int k = telnetsessionX.showPromptPage("Error!", "You must enter an item or UPC number to search. " , dialogPageButtons);
                 
             //(new String[] { "OK" });
                 if (k == 0) {
                     ses.setNextFieldName("INV.ITEM");
                     pg.getmItemFld().setValue("");
                     pg.getmUPCFld().setValue("");
                     pg.getmItemFld().setEditable(true);
                     pg.getmUPCFld().setEditable(false);
                     return;
                 }
                 else if (k == 1) {
                     ses.setStatusMessage(pg.WMS_TXN_CANCEL);
                     ses.clearAllApplicationScopeObjects();
                     ses.setStatusMessage(XXWCBinInquiryPage.WMS_TXN_CANCEL);
                     pg.getmItemFld().setNextPageName("|END_OF_TRANSACTION|");
                 }
                 else {
                     return;
                 }
            }        
        }
        else {
            gCallPoint = "UPC Fld is not null";
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
            try {
                pg.getmUPCFld().getValue().toUpperCase();
                pg.getmItemDescFld().setValue(pg.getmUPCFld().getmItemDescription());
                pg.getmUOMFld().setValue(pg.getmUPCFld().getmPrimaryUOMCode());
                pg.getmItemFld().setmInventoryItemId(pg.getmUPCFld().getmInventoryItemId());
                onHandQuery(mwaevent, ses);
            } catch (Exception e) {
                UtilFns.error("Error in calling exitedItem" + e);
            }
        }
    }

    /*************************************************************************
     *   NAME: public void enteredItem(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException
     *
     *   PURPOSE:  Entered Item Field Listener Process to set the List of Values for the Item Field
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       02-MAR-2015   Lee Spitzer             Initial Version - 
                                                                  TMS Ticket 20150302-00175 - RF - Inventory Inquiry   
     **************************************************************************/


    public void enteredUPC(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException {

        try {
            XXWCItemLOV itemlov = pg.getmUPCFld();
            itemlov.setValidateFromLOV(true);
            itemlov.setlovStatement("XXWC_MWA_ROUTINES_PKG.GET_UPC_ONLY_LOV");
            String paramType[] = { "C", "N", "S" };
            String parameters[] =
            { " ", "ORGID", "xxwc.oracle.apps.inv.invinq.server.XXWCItemOnHandPage.XXWC.UPC" };
            itemlov.setInputParameterTypes(paramType);
            itemlov.setInputParameters(parameters);
        } catch (Exception e) {
            UtilFns.error("Error in calling enteredItem" + e);
        }
    }
    
    
    /*************************************************************************
    *   NAME: public void enteredSub(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                     DefaultOnlyHandlerException
    *
    *   PURPOSE:  Entered Subinventory Field Listener Process to set the List of Values for the Subinventory Field
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
             1.0       02-MAR-2015   Lee Spitzer             Initial Version - 
                                                                  TMS Ticket 20150302-00175 - RF - Inventory Inquiry   
    **************************************************************************/

    public void enteredSub(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException {

       try {
           SubinventoryLOV sublov = pg.getmSubFld();
           sublov.setValidateFromLOV(true);
           sublov.setlovStatement("INV_UI_ITEM_SUB_LOC_LOVS.GET_VALID_SUBS");
           String paramType[] = { "C", "N", "S" };
           String parameters[] =
           { " ", "ORGID", "xxwc.oracle.apps.inv.invinq.server.XXWCItemOnHandPage.INV.SUB" };
           sublov.setInputParameterTypes(paramType);
           sublov.setInputParameters(parameters);
       } catch (Exception e) {
           UtilFns.error("Error in calling enteredSub" + e);
       }
    }

    /*************************************************************************
    *   NAME: public void exitedSub(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException
    *
    *   PURPOSE:  Exited Subinventory Field Listener Process to determine if the Locator Field should be displayed
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       02-MAR-2015   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20150302-00175 - RF - Inventory Inquiry   
    **************************************************************************/

    public void exitedSub(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                DefaultOnlyHandlerException {
        try {
            Integer i = getLocatorControl(mwaevent, ses);
            FileLogger.getSystemLogger().trace("Locator Control returned from getLocatorControl " + i);
            if (i == 1) {
                FileLogger.getSystemLogger().trace("Locator control is not enabled");
                pg.getmLocFld().setHidden(true);
                pg.getmLocFld().clear();
                ses.setNextFieldName("XXWC.NEXT");
            } else {
                FileLogger.getSystemLogger().trace("Locator control is enabled");
                pg.getmLocFld().setHidden(false);
                pg.getmLocFld().setRequired(false);
                ses.setNextFieldName("INV.LOC");
            }
        } catch (Exception e) {
            FileLogger.getSystemLogger().trace("Error in calling exitedSub " + e);
            UtilFns.error("Error in calling exitedSub" + e);
        }
    }

    /*************************************************************************
    *   NAME: public void getLocatorControl(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                     DefaultOnlyHandlerException
    *
    *   PURPOSE:  Determines if Subinventory has Locator Control turned on or off
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
             1.0       21-OCT-2014   Lee Spitzer             Initial Version -
    **************************************************************************/

    private int getLocatorControl(MWAEvent mwavevent, Session session) throws AbortHandlerException,
                                                                              InterruptedHandlerException,
                                                                              DefaultOnlyHandlerException {
        int i = 1;
        int j = 1;
        int k = 1;
        try {
            Long orgid = Long.parseLong((String)session.getObject("ORGID"));
            FileLogger.getSystemLogger().trace("orgid  " + orgid);
            FileLogger.getSystemLogger().trace("i  " + i);
            OrgParameters orgparameters = OrgParameters.getOrgParameters(session.getConnection(), orgid);
            i = orgparameters.getLocatorControlCode();
            FileLogger.getSystemLogger().trace("i  " + i);
            FileLogger.getSystemLogger().trace("j " + j);
            SubinventoryLOV sublov = pg.getmSubFld();
            if (pg.getmSubFld().getValue() != null | pg.getmSubFld().getValue() != "") {
                j = Integer.parseInt(sublov.getLocatorType());
            }
            FileLogger.getSystemLogger().trace("j " + j);
            XXWCItemLOV itemlov = pg.getmItemFld();
            FileLogger.getSystemLogger().trace("k " + k);
            if (pg.getmItemFld().getValue() != null | pg.getmItemFld().getValue() != "") {
                k = Integer.parseInt(pg.getmItemFld().getmLotControlCode());
            }
            FileLogger.getSystemLogger().trace("k " + k);
            if (UtilFns.isTraceOn) {
                UtilFns.trace((new StringBuilder()).append("RCV: getLocatorControl orgLocControl:").append(i).append(":subLocControl:").append(j).append(":itemLocControl:").append(k).toString());
            }
        } catch (Exception e) {
            FileLogger.getSystemLogger().trace("error getLocatorControl " + e);
        }
        return pg.getmLocFld().getLocatorControl(i, j, k);
    }

    /*************************************************************************
    *   NAME: public void enteredLocator(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                     DefaultOnlyHandlerException
    *
    *   PURPOSE:  Entered Locator Field Listener Process to set the List of Values for the Locator Field
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
             1.0       21-OCT-2014   Lee Spitzer             Initial Version -
    **************************************************************************/

    public void enteredLocator(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                         DefaultOnlyHandlerException {

        try {
            Session session = mwaevent.getSession();
            String s = "";
            String s1 = "";
            int i = -1;
            //LocatorKFF locatorlov = pg.getmLocFld();
            //ItemLOV itemlov = pg.getmItemFld();
            if ((String)session.getObject("PROJECT_ID") != null &&
                !((String)session.getObject("PROJECT_ID")).equals("-9999")) {
                s = (String)session.getObject("PROJECT_ID");
                s1 = (String)session.getObject("TASK_ID");
            } else {
                s = "";
                s1 = "";
            }
            FileLogger.getSystemLogger().trace("PROJECT_ID " + s);
            FileLogger.getSystemLogger().trace("TASK_ID " + s1);

            if (pg.getmSubFld() != null && !pg.getmSubFld().getValue().equals("")) {
                i = getLocatorControl(mwaevent, session);
            }
            if (i == 3) {
                //locatorlov.setValidateFromLOV(false);
                pg.getmLocFld().setValidateFromLOV(true);
            } else {
                //locatorlov.setValidateFromLOV(true);
                pg.getmLocFld().setValidateFromLOV(true);
            }

            FileLogger.getSystemLogger().trace("i " + i);

            String s2 = "FALSE";
            if (session.getObject("wms_purchased") != null &&
                ((String)session.getObject("wms_purchased")).equals("I")) {
                s2 = "TRUE";
            }

            FileLogger.getSystemLogger().trace("WMS Enabled " + s2);
            //locatorlov.setSubMapper(pg.getmSubFld());
            pg.getmLocFld().setSubMapper(pg.getmSubFld());
            //locatorlov.setlovStatement("INV_UI_ITEM_SUB_LOC_LOVS.GET_INQ_LOC_LOV");
            pg.getmLocFld().setlovStatement("INV_UI_ITEM_SUB_LOC_LOVS.GET_INQ_LOC_LOV");
            //                      0    1    2    3    4    5    6    7
            String paramType[] = { "C", "N", "S", "N", "N", "S", "S", "S" };
            //                      0         1                                                                        2                                      3                             4                                                                         5   6  7
            //String parameters[] = {" ", "ORGID", "xxwc.oracle.apps.inv.bins.server.XXWCPrimaryBinAssignmentPage.INV.SUB", itemlov.getRestrictLocCode(), itemlov.getItemID(), "xxwc.oracle.apps.inv.bins.server.XXWCPrimaryBinAssignmentPage.INV.ITEM", s, s1};
            String parameters[] =
            { " ", "ORGID", "xxwc.oracle.apps.inv.invinq.server.XXWCItemOnHandPage.INV.SUB",
              pg.getmItemFld().getmLotControlCode(), pg.getmItemFld().getmInventoryItemId(),
              "xxwc.oracle.apps.inv.invinq.server.XXWCItemOnHandPage.INV.LOC", s, s1 };
            //locatorlov.setInputParameterTypes(paramType);
            pg.getmLocFld().setInputParameterTypes(paramType);
            //locatorlov.setInputParameters(paramType);
            pg.getmLocFld().setInputParameters(parameters);
        } catch (Exception e) {
            UtilFns.error("Error in calling enteredLoc" + e);
        }
    }

    /*************************************************************************
    *   NAME: public void exitedClose(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                     DefaultOnlyHandlerException
    *
    *   PURPOSE:  Exited Close For Field Listener Process to proces the Close Button
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       02-MAR-2015   Lee Spitzer             Initial Version - 
                                                               TMS Ticket 20150302-00175 - RF - Inventory Inquiry   
    **************************************************************************/

    public void exitedCancel(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                   DefaultOnlyHandlerException {
        try {
            XXWCItemOnHandPage _tmp = pg;
            ses.setStatusMessage(pg.WMS_TXN_CANCEL);
            ses.clearAllApplicationScopeObjects();
            ses.setStatusMessage(XXWCItemOnHandPage.WMS_TXN_CANCEL);
            pg.getmCancel().setNextPageName("|END_OF_TRANSACTION|");
        } catch (Exception e) {
            UtilFns.error("Error in calling exitedCancel " + e);
        }

    }
   


    /*************************************************************************
    *   NAME:     public void clearFields(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                                 InterruptedHandlerException,
                                                                                 DefaultOnlyHandlerException
    *
    *   PURPOSE:  Clears the values in the form
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
        1.0       02-MAR-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00175 - RF - Inventory Inquiry   
    **************************************************************************/
    
    public void clearFields(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                   InterruptedHandlerException,
                                                                   DefaultOnlyHandlerException {
        pg.mItemFld.setValue(null);
        pg.mItemDescFld.setValue(null);
        pg.mSubFld.setValue(null);
        pg.mNext.setHidden(false);
        pg.mNext.setPrompt("Next");
        pg.mCancel.setHidden(false);
        ses.setNextFieldName("INV.ITEM");
    }
    
    
    /*************************************************************************
    *   NAME:     public void exitedDone(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                                 InterruptedHandlerException,
                                                                                 DefaultOnlyHandlerException
    *
    *   PURPOSE:  Clears the values in the form
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
        1.0       02-MAR-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00175 - RF - Inventory Inquiry   

    **************************************************************************/
    
    protected void exitedDone(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                        DefaultOnlyHandlerException {
        try {
                XXWCItemOnHandPage _tmp = pg;
                ses.setStatusMessage(pg.WMS_TXN_SUCCESS);
                ses.clearAllApplicationScopeObjects();
                //pg.getmDone().setNextPageName("|END_OF_TRANSACTION|");
            } 
        catch (Exception e) {
            UtilFns.error("Error in calling exitedDone " + e);
        }
    
    }
    
    
    /*************************************************************************
    *   NAME: public void exitedNextItem(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                     DefaultOnlyHandlerException
    *
    *   PURPOSE:  Exited Clear For Field Listener Process to proces the Clear Button
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
             1.0       21-OCT-2014   Lee Spitzer             Initial Version -
    **************************************************************************/

    public void exitedNextItem(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                   DefaultOnlyHandlerException {
        try {
            pg.getmItemFld().clear();
            pg.getmUPCFld().clear();
            pg.getmItemDescFld().setValue("");
            pg.getmSubFld().clear();
            pg.getmUOMFld().setValue("");
            pg.getmLocFld().clear();
            pg.getmLotFld().setValue("");
            pg.getmOnHandFld().setValue("");
            pg.getmAvailableFld().setValue("");
            pg.getmOnOrderFld().setValue("");
            ses.setNextFieldName("INV.ITEM");
            pg.getmItemFld().setEditable(true);
            pg.getmUPCFld().setEditable(false);
            pg.getmUPCFld().setHidden(true); //Added 5/11/2015
        } catch (Exception e) {
            UtilFns.error("Error in calling exitedNextItem " + e);
        }

    }


    /*************************************************************************
    *   NAME:     public void exitedBins(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                                 InterruptedHandlerException,
                                                                                 DefaultOnlyHandlerException
    *
    *   PURPOSE:  Execute the procedure XXWC_INV_PRIMARY_BIN_PKG.PROCESS_ITEM_LOCATOR_TIE
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
         1.0       02-MAR-2015   Lee Spitzer             Initial Version - 
                                                              TMS Ticket 20150302-00175 - RF - Inventory Inquiry   
    **************************************************************************/
    
    public void exitedBins(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                             InterruptedHandlerException,
                                                                             DefaultOnlyHandlerException {
        String gMethod = "exitedBins";
        String gCallPoint = "Start";
        FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
        
        
            try {
                CallableStatement cstmt = null;
                Connection con = ses.getConnection();
                String p_organization_id = (String)ses.getObject("ORGID");
                String p_inventory_item_id = (String)pg.getmItemFld().getmInventoryItemId();
                cstmt = con.prepareCall("{? = call XXWC_INV_BIN_MAINTENANCE_PKG.BIN_INQUIRY(?,?)}");
                cstmt.setString(2, p_organization_id);
                cstmt.setString(3, p_inventory_item_id);
                cstmt.registerOutParameter(1, Types.VARCHAR);
                cstmt.execute();
                String x_locators = cstmt.getString(1);
                cstmt.close();
                con.close(); //added 5/12/2015
                FileLogger.getSystemLogger().trace("x_locators " + x_locators);
                
                if (x_locators.equals("No bins associated for item")){
                    x_locators = x_locators + " " + pg.getmItemFld().getmItemNumber().toString();
                }
                
                TelnetSession telnetsessionX = (TelnetSession)ses;
                int k = telnetsessionX.showPromptPage("Bin Assignments for " + pg.getmItemFld().getmItemNumber(), 
                    x_locators.replace("|",System.getProperty("line.separator"))
                                                      , new String [] {"Continue", "Menu"} );
                    
                //(new String[] { "OK" });
                    if (k == 0) {
                        ses.setNextFieldName("XXWC.BINS");
                        return;
                    }
                    else if (k == 1) {
                        ses.setStatusMessage(pg.WMS_TXN_CANCEL);
                        ses.clearAllApplicationScopeObjects();
                        ses.setStatusMessage(XXWCBinInquiryPage.WMS_TXN_CANCEL);
                        pg.getmBins().setNextPageName("|END_OF_TRANSACTION|");
                    }
                    else {
                        return;
                    }
            } catch (Exception e) {
                UtilFns.error("Error in calling exitedBins" + e);
            }
    
    }



    /*************************************************************************
    *   NAME:     public void exitedNext(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                                 InterruptedHandlerException,
                                                                                 DefaultOnlyHandlerException
    *
    *   PURPOSE:  Execute the procedure XXWC_INV_PRIMARY_BIN_PKG.PROCESS_ITEM_LOCATOR_TIE
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
         1.0       02-MAR-2015   Lee Spitzer             Initial Version - 
                                                              TMS Ticket 20150302-00175 - RF - Inventory Inquiry   
    **************************************************************************/
    
    public void exitedNext(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                             InterruptedHandlerException,
                                                                             DefaultOnlyHandlerException {
        String gMethod = "exitedNext";
        String gCallPoint = "Start";
        FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
            
            XXWCItemOnHandIterator XXWCItemOnHandIterator = (XXWCItemOnHandIterator)ses.getObject("XXWCITEMONHANDSEQ");
            
            int i = XXWCItemOnHandIterator.getCurrentPos();
            int l = -1;
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " " + XXWCItemOnHandIterator);
            //FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " " + XXWCBinPutAway);
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " i " + i);
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " l " + l);
            Connection connection = ses.getConnection();
            XXWCItemOnHandQueryManager XXWCItemOnHandQueryManager =
                new XXWCItemOnHandQueryManager(connection);
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " XXWCItemOnHandQueryManager " + XXWCItemOnHandQueryManager);
            try {
                XXWCItemOnHand XXWCItemOnHand1 = XXWCItemOnHandIterator.next();
                l = XXWCItemOnHandIterator.max_left();
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " XXWCItemOnHand1 " + XXWCItemOnHand1);
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " l " + l);
                if (XXWCItemOnHand1 != null) {
                    FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " XXWCItemOnHand1 != null ");
                    pg.setCurrentXXWCItemOnHand(XXWCItemOnHand1);
                }
                if (l == 0) {
                    FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " l == 0");
                    pg.getmNext().setHidden(true);
                    pg.getmPrevious().setHidden(true);
                    ses.setRefreshScreen(true);
                    ses.setNextFieldName("XXWC.NEXT_ITEM");
                } else {
                    FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " l != 0");
                    pg.getmNext().setHidden(false);
                    ses.setNextFieldName("XXWC.NEXT");
                    ses.setRefreshScreen(true);
                }
                pg.getmPrevious().setHidden(false);
                ses.setRefreshScreen(true);
                int j = XXWCItemOnHandIterator.getCurrentPos();
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " j " + j);
                pg.mSubFld.setValue(XXWCItemOnHand1.getmSubinventoryCode());
                pg.mLocFld.setValue(XXWCItemOnHand1.getmLocatorName());
                pg.mLotFld.setValue(XXWCItemOnHand1.getmLotNumber());
                pg.mAvailableFld.setValue(XXWCItemOnHand1.getmAvailable());
                pg.mOnHandFld.setValue(XXWCItemOnHand1.getmOnHand());
                pg.mOnOrderFld.setValue(XXWCItemOnHand1.getmOnOrder());
                
                gCallPoint = " getmLocatorName ";
                if (pg.getmLocFld().getValue().equals(null) || pg.getmLocFld().getValue().equals("")) {
                    FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " pg.getmLocFld().setHidden(true) ");
                    pg.getmLocFld().setHidden(true);     
                }
                else {
                    FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " pg.getmLocFld().setHidden(false)");
                    pg.getmLocFld().setHidden(false);
                }
                gCallPoint = " getmLotNumber ";
                
                if (pg.getmLotFld().getValue().equals(null) || pg.getmLotFld().getValue().equals("") ) {
                    FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " pg.getmLotFld().setHidden(true); ");
                    pg.getmLotFld().setHidden(true);
                }
                else {
                    FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " pg.getmLotFld().setHidden(false); ");
                    pg.getmLotFld().setHidden(false);
                }
                
                
            } catch (Exception exception1) {
                UtilFns.log((new StringBuilder()).append(" ").append(exception1).toString());
            }
            
            //ses.setNextFieldName("XXWC.NEXT_ITEM");
        }



    /*************************************************************************
    *   NAME:     public void onHandQuery(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                                 InterruptedHandlerException,
                                                                                 DefaultOnlyHandlerException
    *
    *   PURPOSE:  Execute the procedure XXWC_INV_PRIMARY_BIN_PKG.PROCESS_ITEM_LOCATOR_TIE
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
         1.0       02-MAR-2015   Lee Spitzer             Initial Version - 
                                                              TMS Ticket 20150302-00175 - RF - Inventory Inquiry   
    **************************************************************************/
    
    public void onHandQuery(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                             InterruptedHandlerException,
                                                                             DefaultOnlyHandlerException {
        String gMethod = "onHandQuery";
        String gCallPoint = "Start";
        FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
                        
        try{
            gCallPoint = "Entering Try";
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
            //Call the QueryManager to execute the query
            java.sql.Connection con = ses.getConnection();
            XXWCItemOnHandQueryManager XXWCItemOnHandQueryManager = new XXWCItemOnHandQueryManager(con);
            
            String p_organization_id = (String)ses.getObject("ORGID");
            String p_inventory_item_id = (String)pg.getmItemFld().getmInventoryItemId();
            String p_subinventory_code = "";
            String p_locator_id = "";
            String p_lot_number = "";
            String p_project_id = "";
            String p_task_id = "";
            Vector vector1;
            gCallPoint = "about to call vector 1";
            //Execute the getmtlItemLocatorDefaultStr query and set the vector
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
            vector1 =
                    XXWCItemOnHandQueryManager.getmWCOnHand(p_organization_id, p_inventory_item_id, p_subinventory_code, p_locator_id, p_lot_number, p_project_id, p_task_id);
        
            gCallPoint = "results after vector 1";
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " vector size " + vector1.size() );
            
            if (vector1.size() == 0) {
                gCallPoint = "vector1 is 0";
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
                TelnetSession telnetsessionX = (TelnetSession)ses;
                Integer k =
                    telnetsessionX.showPromptPage("Error!", "No on-hand for item " + pg.getmItemFld().getValue(), dialogPageButtons);
                //showSubError = true;
                if (k == 0) {
                    pg.getmNext().setHidden(true);
                    pg.getmPrevious().setHidden(true);
                    return;
                    
                } 
        }
            else {
                //If the vector has results then set session values, retun page
                gCallPoint = "vector1 found";
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
                XXWCItemOnHandIterator XXWCItemOnHandIterator = new XXWCItemOnHandIterator(vector1);
                ses.putObject("XXWCITEMONHANDSEQ", XXWCItemOnHandIterator);
                XXWCItemOnHandIterator mXXWCItemOnHandSeq = (XXWCItemOnHandIterator)ses.getObject("XXWCITEMONHANDSEQ");
                int i = mXXWCItemOnHandSeq.getCurrentPos();
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " mXXWCItemOnHandSeq " + mXXWCItemOnHandSeq);
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " i " + i);
                if (i == 0){
                    gCallPoint = "i == 0";
                    FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " mXXWCItemOnHandSeq == null");
                    try {
                         pg.mCurrentItemOnHand = mXXWCItemOnHandSeq.next();
                         FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " pg.mCurrentItemOnHand " + pg.mCurrentItemOnHand);
                         pg.getmSubFld().setValue(pg.getmCurrentXXWCItemOnHand().getmSubinventoryCode());
                         FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " pg.getmSubFld().getValue " + pg.getmSubFld().getValue());
                         pg.getmLocFld().setValue(pg.getmCurrentXXWCItemOnHand().getmLocatorName());
                         FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " pg.getmLocFld().getValue " + pg.getmLocFld().getValue());
                         pg.getmLotFld().setValue((pg.getmCurrentXXWCItemOnHand().getmLotNumber()));
                         FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " pg.getmLotFld().getValue() " + pg.getmLotFld().getValue());
                         pg.getmAvailableFld().setValue(pg.getmCurrentXXWCItemOnHand().getmAvailable());
                         FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " pg.getmAvailableFld().getValue " + pg.getmAvailableFld().getValue());
                         pg.getmOnHandFld().setValue(pg.getmCurrentXXWCItemOnHand().getmOnHand());
                         FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " pg.getmOnHandFld().getValue " + pg.getmOnHandFld().getValue());
                         pg.getmOnOrderFld().setValue(pg.getmCurrentXXWCItemOnHand().getmOnOrder());
                         FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " pg.getmOnOrderFld().getValue " + pg.getmOnOrderFld().getValue());
                        
                        
                        gCallPoint = " getmLocatorName ";
                        if (pg.getmLocFld().getValue().equals(null) || pg.getmLocFld().getValue().equals("")) {
                            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " pg.getmLocFld().setHidden(true) ");
                            pg.getmLocFld().setHidden(true);     
                        }
                        else {
                            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " pg.getmLocFld().setHidden(false)");
                            pg.getmLocFld().setHidden(false);
                        }
                        gCallPoint = " getmLotNumber ";
                        
                        if (pg.getmLotFld().getValue().equals(null) || pg.getmLotFld().getValue().equals("") ) {
                            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " pg.getmLotFld().setHidden(true); ");
                            pg.getmLotFld().setHidden(true);
                        }
                        else {
                            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " pg.getmLotFld().setHidden(false); ");
                            pg.getmLotFld().setHidden(false);
                        }
                        
                        if (vector1.size()==1){
                            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " Vector Size is 1");
                            pg.getmNext().setHidden(true);
                            pg.getmPrevious().setHidden(true);
                         }
                         if (vector1.size()>1) {
                             FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " Vector Size > 1 ");
                             pg.getmNext().setHidden(false);
                             pg.getmPrevious().setHidden(true);
                         }
                        
                        
                    } catch (Exception e) {
                        FileLogger.getSystemLogger().trace("Error calling " + gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " " + e);
                    }
                }
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " " + pg.getmCurrentXXWCItemOnHand());
                
            }
        } catch (Exception e) {
            ses.setStatusMessage((new StringBuilder()).append(pg.WMS_TXN_ERROR).append(" (").append(e).append(")").toString());
            UtilFns.error((new StringBuilder()).append("Error in onHandQuery: error - ").append(e).toString());
        }
    }

    /*************************************************************************
    *   NAME:     public void exitedPrevious(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                                 InterruptedHandlerException,
                                                                                 DefaultOnlyHandlerException
    *
    *   PURPOSE:  Execute the procedure XXWC_INV_PRIMARY_BIN_PKG.PROCESS_ITEM_LOCATOR_TIE
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
         1.0       02-MAR-2015   Lee Spitzer             Initial Version - 
                                                              TMS Ticket 20150302-00175 - RF - Inventory Inquiry   
    **************************************************************************/
    
    public void exitedPrevious(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                             InterruptedHandlerException,
                                                                             DefaultOnlyHandlerException {
        String gMethod = "exitedPrevious";
        String gCallPoint = "Start";
        FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
            
            XXWCItemOnHandIterator XXWCItemOnHandIterator = (XXWCItemOnHandIterator)ses.getObject("XXWCITEMONHANDSEQ");
            
            int i = XXWCItemOnHandIterator.getCurrentPos();
            int l = -1;
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " " + XXWCItemOnHandIterator);
            //FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " " + XXWCBinPutAway);
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " i " + i);
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " l " + l);
            Connection connection = ses.getConnection();
            XXWCItemOnHandQueryManager XXWCItemOnHandQueryManager =
                new XXWCItemOnHandQueryManager(connection);
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " XXWCItemOnHandQueryManager " + XXWCItemOnHandQueryManager);
            try {
                XXWCItemOnHand XXWCItemOnHand1 = XXWCItemOnHandIterator.previous();
                l = XXWCItemOnHandIterator.max_left();
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " XXWCItemOnHand1 " + XXWCItemOnHand1);
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " l " + l);
                if (XXWCItemOnHand1 != null) {
                    FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " XXWCItemOnHand1 != null ");
                    pg.setCurrentXXWCItemOnHand(XXWCItemOnHand1);
                }
                if (l == 0) {
                    FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " l == 0");
                    pg.getmNext().setHidden(true);
                    pg.getmPrevious().setHidden(true);
                    ses.setRefreshScreen(true);
                    ses.setNextFieldName("XXWC.NEXT_ITEM");
                } else {
                    FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " l != 0");
                    pg.getmNext().setHidden(false);
                    pg.getmPrevious().setHidden(false);
                    ses.setRefreshScreen(true);
                    ses.setNextFieldName("XXWC.PREVIOUS");
                }
                //pg.getmPrevious().setHidden(false);
                ses.setRefreshScreen(true);
                int j = XXWCItemOnHandIterator.getCurrentPos();
                if (j == 1 || j == 0) {
                    pg.getmPrevious().setHidden(true);
                    ses.setNextFieldName("XXWC.NEXT");
                }
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " j " + j);
                pg.mSubFld.setValue(XXWCItemOnHand1.getmSubinventoryCode());
                pg.mLocFld.setValue(XXWCItemOnHand1.getmLocatorName());
                pg.mLotFld.setValue(XXWCItemOnHand1.getmLotNumber());
                pg.mAvailableFld.setValue(XXWCItemOnHand1.getmAvailable());
                pg.mOnHandFld.setValue(XXWCItemOnHand1.getmOnHand());
                pg.mOnOrderFld.setValue(XXWCItemOnHand1.getmOnOrder());
                
                gCallPoint = " getmLocatorName ";
                if (pg.getmLocFld().getValue().equals(null) || pg.getmLocFld().getValue().equals("")) {
                    FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " pg.getmLocFld().setHidden(true) ");
                    pg.getmLocFld().setHidden(true);     
                }
                else {
                    FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " pg.getmLocFld().setHidden(false)");
                    pg.getmLocFld().setHidden(false);
                }
                gCallPoint = " getmLotNumber ";
                
                if (pg.getmLotFld().getValue().equals(null) || pg.getmLotFld().getValue().equals("") ) {
                    FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " pg.getmLotFld().setHidden(true); ");
                    pg.getmLotFld().setHidden(true);
                }
                else {
                    FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " pg.getmLotFld().setHidden(false); ");
                    pg.getmLotFld().setHidden(false);
                }
                
            } catch (Exception exception1) {
                UtilFns.log((new StringBuilder()).append(" ").append(exception1).toString());
            }
            
            //ses.setNextFieldName("XXWC.NEXT_ITEM");
        }

}