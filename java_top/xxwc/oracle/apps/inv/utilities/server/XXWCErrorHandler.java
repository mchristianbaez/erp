/*************************************************************************
   *   $Header XXWCErrorHandler.java $
   *   Module Name: XXWCErrorHandler
   *   
   *   Package: package xxwc.oracle.apps.inv.utilities.server;
   *   
   *   Imports Classes:
   *   
   *   import java.sql.*;
   *   import java.util.Hashtable;
   *   import java.util.Vector;
   *   import oracle.apps.fnd.common.VersionInfo;
   *   import oracle.apps.inv.invinq.server.ItemOnhand;
   *   import oracle.apps.inv.utilities.server.UtilFns;
   *   import oracle.apps.mwa.container.FileLogger;
   *   import oracle.jdbc.OracleCallableStatement;
   *   import oracle.jdbc.OraclePreparedStatement;

   *
   *   PURPOSE:   Java Class for XXWCErrorHandler.  This calls XXWC_INV_PRIMARY_BIN_PKG.GET_MILD_INQUIRY and controls parameters and result values
   *    
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
        1.0       02-MAR-2015   Lee Spitzer             Initial Version - 
                                                            TMS Ticket 20150302-00175 - RF - Inventory Inquiry   
**************************************************************************/

package xxwc.oracle.apps.inv.utilities.server;


import java.sql.*;
import java.util.Hashtable;
import java.util.Vector;
import oracle.apps.fnd.common.VersionInfo;
import oracle.apps.inv.invinq.server.ItemOnhand;
import oracle.apps.inv.utilities.server.UtilFns;
import oracle.apps.mwa.container.FileLogger;
import oracle.apps.mwa.container.Session;

import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OraclePreparedStatement;

import xxwc.oracle.apps.inv.invinq.server.XXWCItemOnHandQueryManager;
import xxwc.oracle.apps.inv.rcv.server.XXWCRcvGenFListener;

/*************************************************************************
 *   NAME: XXWCErrorHandler
 *
 *   PURPOSE:   Main class for XXWCErrorHandler
 *
 *   REVISIONS:
 *   Ver        Date        Author                     Description
 *   ---------  ----------  ---------------         -------------------------
        1.0       02-MAR-2015   Lee Spitzer             Initial Version -
                                                            TMS Ticket 20150302-00175 - RF - Inventory Inquiry
 **************************************************************************/

public class XXWCErrorHandler {
    
    //Session ses;

    /*************************************************************************
    *   NAME: private static String mXXWCErrorMainAP
    *
    *   PURPOSE:  Status string to call the custom error handlier package
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20150302-00152 RF - Receiving
    **************************************************************************/

    private static String mXXWCErrorMainAPI =
        "BEGIN   xxcus_error_pkg.xxcus_error_main_api(:1, :2, :3, :4, :5, :6); END; ";


    /*************************************************************************
    *   NAME: private static String p_distribution_list = "hdsoracledevelopers@hdsupply.com";
    *
    *   PURPOSE:  Static string to set distribution list to hdsoracledevelopers@hdsupply.com
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20150302-00152 RF - Receiving
    **************************************************************************/


    private static String p_distribution_list = "hdsoracledevelopers@hdsupply.com";

    /*************************************************************************
    *   NAME: private static String p_module = "mwa";
    *
    *   PURPOSE:  Static string set module to mwa
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20150302-00152 RF - Receiving
    **************************************************************************/

    private static String p_module = "mwa";
    
    /*************************************************************************
    *   NAME: public void XXWCErrorHandler (String p_called_from, String p_calling, String p_ora_error_msg, String p_error_desc)
    *
    *   PURPOSE:  Public call to use the custom error handler package
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20150302-00152 RF - Receiving
    **************************************************************************/
  
    public XXWCErrorHandler() {
    }

    public void XXWCCallAPI(String p_called_from, String p_calling, String p_ora_error_msg, String p_error_desc, Session ses) {
        
        String p_user_id = "";
        p_user_id = (String)ses.getObject("USERID");
        
        try {
              CallableStatement cstmt = null;
              Connection con = ses.getConnection();
              //                                                                  1 2 3 4 5 6 7 8 9 0 1 2 3
              cstmt = con.prepareCall("{call xxcus_error_pkg.xxcus_error_main_api(?,?,?,?,?,?,?,?,?,?,?,?,?)}");
              cstmt.setString(1, p_called_from);//p_called_from.substring(0, 109));
              cstmt.setString(2, p_calling);//p_calling.substring(0, 109));
              cstmt.setString(3, "");
              cstmt.setString(4, p_ora_error_msg);//p_ora_error_msg.substring(0,1999));
              cstmt.setString(5, p_error_desc);//p_error_desc.substring(0, 239));
              cstmt.setString(6, p_distribution_list);
              cstmt.setString(7, "");
              cstmt.setString(8, "");
              cstmt.setString(9, "");
              cstmt.setString(10, "");
              cstmt.setString(11, p_user_id); //p_user_id
              cstmt.setString(12, "");
              cstmt.setString(13, p_module);
              cstmt.execute();
              con.commit();
              cstmt.close();
              con.close();
        }
        catch (Exception e) {
            FileLogger.getSystemLogger().trace("p_called_from " +  p_called_from + 
                                               " p_calling " + p_calling +
                                               " p_ora_error_msg " + p_ora_error_msg +
                                               " p_error_desc " + p_error_desc +
                                               " p_distribution_list " + p_distribution_list +
                                               " p_module " + p_module +
                                               " error is " + e
                                               );
            
        }
    }
}