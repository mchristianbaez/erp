/*************************************************************************
   *   $Header XXWCItemLabelPage.java $
   *   Module Name: XXWCItemLabelPage
   *   
   *   Package: package xxwc.oracle.apps.inv.labels.server;
   *   
   *   Imports Classes:
   *   
   *   import java.sql.*;
   *   import oracle.apps.inv.utilities.server.UtilFns;
   *   import oracle.apps.mwa.beans.*;
   *   import oracle.apps.mwa.container.MWALib;
   *   import oracle.apps.mwa.container.Session;
   *   import oracle.apps.mwa.eventmodel.*;
   *   import oracle.apps.mwa.container.FileLogger;
   *   import oracle.apps.fnd.flexj.FlexException;
   *   import oracle.apps.inv.lov.server.ItemLOV;
   *   import oracle.apps.inv.lov.server.LocatorKFF;
   *   import oracle.apps.inv.lov.server.SubinventoryLOV;
   *   import xxwc.oracle.apps.inv.lov.server.*;
   *
   *   PURPOSE:   Java Class for XXWCItemLabelPage Page Layout.
   *    
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
        1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                          TMS Ticket 20150302-00152 - RF Receiving
**************************************************************************/

package xxwc.oracle.apps.inv.labels.server;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import oracle.apps.mwa.beans.*;
import oracle.apps.mwa.container.MWALib;
import oracle.apps.mwa.container.Session;
import oracle.apps.mwa.eventmodel.*;
import oracle.apps.mwa.container.FileLogger;
import oracle.apps.fnd.flexj.FlexException;
import oracle.apps.fnd.flexj.KeyFlexfield;
import oracle.apps.inv.invinq.server.ItemOnhandIterator;
import oracle.apps.inv.utilities.server.*;
import xxwc.oracle.apps.inv.lov.server.*;

/*************************************************************************
 *   NAME: XXWCItemLabelPage extends PageBean
 *
 *   PURPOSE:   Main class for XXWCItemLabelPage
 *
 *   REVISIONS:
 *   Ver        Date        Author                     Description
 *   ---------  ----------  ---------------         -------------------------
        1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                          TMS Ticket 20150302-00152 - RF Receiving
 **************************************************************************/


public class XXWCItemLabelPage extends PageBean{
    /*Define objects in the class*/
    public static String WMS_TXN_SUCCESS = "Txn Successful";
    public static String WMS_TXN_ERROR = "Txn Failed. Please check log";
    public static String WMS_TXN_CANCEL = "Txn Cancelled";
    public static String WMS_LOT_INS_FAIL = "Lot Insert Failed";
    public static String WMS_SER_INS_FAIL = "Serial Insert Failed";
    public static String savenextPrompt = "";
    public static String donePrompt = "";
    public static String cancelPrompt = "";
    private boolean areThePromptsSet;
    protected XXWCItemLOV mItemFld;
    protected XXWCBinsLOV mBinFld;
    protected XXWCYesNoLOV mPriceFld;
    protected XXWCItemLabelStatusFlagLOV mItemLabelStatusFlag;
    protected XXWCYesNoLOV mPrintLocationFld;
    protected XXWCYesNoLOV mShelfPricingFld;
    protected TextFieldBean mCopies;
    protected ButtonFieldBean mCancel;
    protected ButtonFieldBean mSaveNext;
    protected ButtonFieldBean mDone;
    protected String m_userid;


    /*************************************************************************
     *   NAME: private static String gPackage = "xxwc.oracle.apps.inv.labels.server.XXWCItemLabelPage";
     *
     *   PURPOSE:   private method to default gPackage value for debugging
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00152 - RF Receiving
     **************************************************************************/

    private static String gPackage = "xxwc.oracle.apps.inv.labels.server";


    /*************************************************************************
     *   NAME: private static String gCallFrom = "XXWCItemLabelPage";
     *
     *   PURPOSE:   private method to pass gCallFrom from methods to return class name CustomListener
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00152 - RF Receiving
     **************************************************************************/

    private static String gCallFrom = "XXWCItemLabelPage";



    /*************************************************************************
     *   NAME: public XXWCItemLabelPage(Session session)
     *
     *   PURPOSE:   Inherits session from XXWCPrimaryBinAssignmentPage
     *
     *   Parameters: Session session
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                               TMS Ticket 20150302-00152 - RF Receiving
     **************************************************************************/

    public XXWCItemLabelPage(Session session) {
        initLayout(session);
    }

    
    /*************************************************************************
     *   NAME: private void initLayout(Session session)
     *
     *   PURPOSE:   Sets the initial layout of the page
     *
     *   Parameters: Session session
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
          1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                             TMS Ticket 20150302-00152 - RF Receiving
     **************************************************************************/

    private void initLayout(Session session) {
        m_userid = "";
        WMS_TXN_SUCCESS = UtilFns.getMessage(session, "WMS", "WMS_TXN_SUCCESS");
        WMS_TXN_ERROR = UtilFns.getMessage(session, "WMS", "WMS_TXN_ERROR");
        WMS_TXN_CANCEL = UtilFns.getMessage(session, "WMS", "WMS_TXN_CANCEL");
        WMS_LOT_INS_FAIL = UtilFns.getMessage(session, "WMS", "WMS_LOT_INS_FAIL");
        WMS_SER_INS_FAIL = UtilFns.getMessage(session, "WMS", "WMS_SER_INS_FAIL");
        /*Item Life*/
        mItemFld = new XXWCItemLOV("ALL");
        mItemFld.setName("XXWC.ITEM");
        mItemFld.setRequired(true);
        /*Bin*/
        mBinFld = new XXWCBinsLOV();
        mBinFld.setName("XXWC.BIN");
        mBinFld.setRequired(false);
        /*Price*/
        mPriceFld = new XXWCYesNoLOV ();
        mPriceFld.setName("XXWC.PRICE");
        mPriceFld.setRequired(false);
        mPriceFld.setValue("No");
        mPriceFld.setHidden(true);
        /*Item Label Status*/
        mItemLabelStatusFlag = new XXWCItemLabelStatusFlagLOV();
        mItemLabelStatusFlag.setName("XXWC.ITEM_STATUS");
        mItemLabelStatusFlag.setRequired(true);
        //mItemLabelStatusFlag.setValue("T");
        /*Print Location Fld*/
        mPrintLocationFld = new XXWCYesNoLOV();
        mPrintLocationFld.setName("XXWC.PRINT_LOCATION");
        mPrintLocationFld.setRequired(false);
        /*Print Location*/
        mShelfPricingFld = new XXWCYesNoLOV();
        mShelfPricingFld.setName("XXWC.SHELF_PRICE");
        mShelfPricingFld.setRequired(false);
        mShelfPricingFld.setHidden(true);
        /*Copies*/
        mCopies = new TextFieldBean();
        mCopies.setName("XXWC.COPIES");
        mCopies.setRequired(true);
        mCopies.setValue("1");
        /*Save Properties*/
        mSaveNext = new ButtonFieldBean();
        mSaveNext.setName("INV.SAVENEXT");
        mSaveNext.setPrompt("Save/Next");
        mSaveNext.setEnableAcceleratorKey(true);
        mSaveNext.setHidden(true);
        /*Save Properties*/
        mDone = new ButtonFieldBean();
        mDone.setName("INV.DONE");
        mDone.setPrompt("Done");
        mDone.setEnableAcceleratorKey(true);
        mDone.setHidden(false);
        /*Cancel Properties*/
        mCancel = new ButtonFieldBean();
        mCancel.setName("INV.CANCEL");
        mCancel.setPrompt("Cancel");
        mCancel.setEnableAcceleratorKey(true);
        mCancel.setHidden(false);
        /*set the properties for each to the addFieldBean*/
        addFieldBean(mItemFld);
        addFieldBean(mBinFld);
        addFieldBean(mPriceFld);
        addFieldBean(mItemLabelStatusFlag);
        addFieldBean(mPrintLocationFld);
        addFieldBean(mShelfPricingFld);
        addFieldBean(mCopies);
        addFieldBean(mSaveNext);
        addFieldBean(mDone);
        addFieldBean(mCancel);
        XXWCItemLabelFListener fieldListener = new XXWCItemLabelFListener();
        /*Add Listener to each property*/
        mItemFld.addListener(fieldListener);
        mBinFld.addListener(fieldListener);
        mPriceFld.addListener(fieldListener);
        mItemLabelStatusFlag.addListener(fieldListener);
        mPrintLocationFld.addListener(fieldListener);
        mShelfPricingFld.addListener(fieldListener);
        mCopies.addListener(fieldListener);
        mSaveNext.addListener(fieldListener);
        mDone.addListener(fieldListener);
        mCancel.addListener(fieldListener);
        try {
            setPrompts(session);
        } catch (Exception exception) {
            session.setStatusMessage(UtilFns.getMessage(session, "WMS", "WMS_PROMPT_NOTSET" + exception));
        }
    }
    
    /*************************************************************************
     *   NAME: private void initPrompts(Session session) throws SQLException
     *
     *   PURPOSE:   Sets the initial prompts of the session
     *
     *   Parameters: Session session
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
          1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                             TMS Ticket 20150302-00152 - RF Receiving
     **************************************************************************/

    private void initPrompts(Session session) throws SQLException {
        try {
            String gMethod = "initPrompts";
            String gCallPoint = "Start";
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint);
            String s = (String)session.getObject("ORGCODE");
            setPrompt((new StringBuilder()).append("XXWC Item Labels (").append(s).append(")").toString());
            mItemFld.setPrompt("Item");
            mBinFld.setPrompt("Bin");
            mPriceFld.setPrompt("Price?");
            mItemLabelStatusFlag.setPrompt("Label Size");
            mPrintLocationFld.setPrompt("Location?");
            mShelfPricingFld.setPrompt("Shelf Price?");
            mCopies.setPrompt("Copies");
            savenextPrompt = MWALib.getAKPrompt(session, "oracle.apps.inv.utilities.InvResourceTable", "INV_SAVE_NEXT_PROMPT");
            donePrompt = MWALib.getAKPrompt(session, "oracle.apps.inv.utilities.InvResourceTable", "INV_DONE_PROMPT");
            cancelPrompt = MWALib.getAKPrompt(session, "oracle.apps.inv.utilities.InvResourceTable", "INV_CANCEL_PROMPT");
            areThePromptsSet = true;
            gCallPoint = "End";
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint);
        } catch (SQLException sqlexception) {
            session.setStatusMessage(UtilFns.getMessage(session, "WMS", "WMS_TXN_ERROR"));
        }
    }

    /*************************************************************************
     *   NAME: private void setPrompts(Session session)
     *
     *   PURPOSE:   Sets the set prompts of the session
     *
     *   Parameters: Session session
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
          1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                             TMS Ticket 20150302-00152 - RF Receiving
     **************************************************************************/

    private void setPrompts(Session session) {
            String gMethod = "setPrompts";
            String gCallPoint = "Start";
            if (!areThePromptsSet) {
                try {
                    initPrompts(session);
                } catch (SQLException sqlexception) {
                }
            }
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint);
            String s = (String)session.getObject("ORGCODE");
            setPrompt((new StringBuilder()).append("XXWC Item Labels (").append(s).append(")").toString());

            mSaveNext.setPrompt(savenextPrompt);
            mDone.setPrompt("Print Label");
            mCancel.setPrompt(cancelPrompt);
            mSaveNext.retrieveAttributes("INVE_SAVE_NEXT_PROMPT");
            mDone.retrieveAttributes("INV_DONE_PROMPT");
            mCancel.retrieveAttributes("INV_CANCEL_PROMPT");
            //mCancel.setPrompt("Menu");
            gCallPoint = "End Prompts";
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint);
            
            setFieldValues(session);
    }


     /*************************************************************************
     *   NAME: public XXWCItemLOV mItemFld(
     *
     *   PURPOSE:   return the value of getmShelfLife
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00152 - RF Receiving
     **************************************************************************/

    public XXWCItemLOV getmItemFld() {
        return mItemFld;
    }

    /*************************************************************************
    *   NAME: public TextFieldBean getmBinFld()
    *
    *   PURPOSE:   return the value of getmUBDDate
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
           1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                               TMS Ticket 20150302-00152 - RF Receiving
    **************************************************************************/

    public XXWCBinsLOV getmBinFld() {
        return mBinFld;
    }
    

    /*************************************************************************
    *   NAME: public XXWCYesNoLOV getmPriceFld()
    *
    *   PURPOSE:   return the value of getmUBDDate
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
           1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                               TMS Ticket 20150302-00152 - RF Receiving
    **************************************************************************/

    public XXWCYesNoLOV getmPriceFld() {
        return mPriceFld;
    }
    /*************************************************************************
     *   NAME: public ButtonFieldBean getmCancel
     *
     *   PURPOSE:   return the value of mCancel
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00152 - RF Receiving
     **************************************************************************/

     public XXWCItemLabelStatusFlagLOV getmItemLabelStatusFlag() {
         return mItemLabelStatusFlag;
     }
    
    
    /*************************************************************************
    *   NAME: public XXWCYesNoLOV mPrintLocationFld()
    *
    *   PURPOSE:   return the value of getmUBDDate
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
           1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                               TMS Ticket 20150302-00152 - RF Receiving
    **************************************************************************/

    public XXWCYesNoLOV getmPrintLocationFld() {
        return mPrintLocationFld;
    }
    
    
    /*************************************************************************
    *   NAME: public XXWCYesNoLOV get()
    *
    *   PURPOSE:   return the value of getmUBDDate
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
           1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                               TMS Ticket 20150302-00152 - RF Receiving
    **************************************************************************/

    public XXWCYesNoLOV getmShelfPricingFld() {
        return mShelfPricingFld;
    }
    
    /*************************************************************************
    *   NAME: public TextFieldBean getmCopies()
    *
    *   PURPOSE:   return the value of getmUBDDate
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
           1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                               TMS Ticket 20150302-00152 - RF Receiving
    **************************************************************************/

    public TextFieldBean getmCopies() {
        return mCopies;
    }
     /*************************************************************************
      *   NAME: public ButtonFieldBean getmCancel
      *
      *   PURPOSE:   return the value of mCancel
      *
      *   REVISIONS:
      *   Ver        Date        Author                     Description
      *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20150302-00152 - RF Receiving
      **************************************************************************/

    public ButtonFieldBean getmCancel() {
        return mCancel;
    }

    /*************************************************************************
     *   NAME: public ButtonFieldBean getmSaveNext()
     *
     *   PURPOSE:   return the value of mSaveNext
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00152 - RF Receiving
     **************************************************************************/

    public ButtonFieldBean getmSaveNext() {
        return mSaveNext;
    }

    /*************************************************************************
     *   NAME: public ButtonFieldBean getmDone()
     *
     *   PURPOSE:   return the value of mDone
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00152 - RF Receiving
     **************************************************************************/

    public ButtonFieldBean getmDone() {
        return mDone;
    }


    /*************************************************************************
     *   NAME: public String getUserId()
     *
     *   PURPOSE:   return the value of m_userid
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00152 - RF Receiving
     **************************************************************************/

    public String getUserId() {
        return m_userid;
    }

    /*************************************************************************
     *   NAME: void pageEntered(MWAEvent e) throws AbortHandlerException, InterruptedHandlerException,
                                               DefaultOnlyHandlerException {
     *
     *   PURPOSE:   pageEntered method
     *
     *   Parameters: MWAEvent mwaevent
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00152 - RF Receiving
     **************************************************************************/

    public void pageEntered(MWAEvent e) throws AbortHandlerException, InterruptedHandlerException,
                                               DefaultOnlyHandlerException {
        Session session = e.getSession();
        
    }

    /*************************************************************************
     *   NAME: void pageExited(MWAEvent e) throws AbortHandlerException, InterruptedHandlerException,
                                               DefaultOnlyHandlerException {
     *
     *   PURPOSE:   pageExited method
     *
     *
     *   Parameters: MWAEvent   mwaevent
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00152 - RF Receiving

     **************************************************************************/

    public void pageExited(MWAEvent e) throws AbortHandlerException, InterruptedHandlerException,
                                              DefaultOnlyHandlerException {
        Session session = e.getSession();
    }
   
    /*************************************************************************
     *   NAME: void setFieldValues(Session session) throws AbortHandlerException, InterruptedHandlerException,
                                               DefaultOnlyHandlerException {
     *
     *   PURPOSE:   pageExited method
     *
     *
     *   Parameters: MWAEvent   mwaevent
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00152 - RF Receiving

     **************************************************************************/
 
    public void setFieldValues(Session session){
        String gMethod = "setFieldValues";
        String gCallPoint = "Start";
        session.setRefreshScreen(true);
        String sessPreviousPage = "";
        
            try {
                sessPreviousPage = (String)session.getObject("sessPreviousPage").toString();
            }
            catch (Exception e) {
                sessPreviousPage = "";
                session.putSessionObject("sessPreviousField", "");
                session.putSessionObject("sessXXWC.ITEM", "");
                session.putSessionObject("sessXXWC.BIN", "");
                session.putSessionObject("sessXXWC.LOCATION_ID", "");
                session.putSessionObject("sessXXWC.PRICE", "");
                session.putSessionObject("sessXXWC.ITEM_STATUS", "");
                session.putSessionObject("sessXXWC.PRINT_LOCATION", "");
                session.putSessionObject("sessXXWC.SHELF_PRICE", "");
                session.putSessionObject("sessXXWC.COPIES", "");
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " " + e);
                //mCancel.setPrompt("Menu"); //added 5/12/2015
                session.setRefreshScreen(true);//added 5/12/2015
            }
        
        FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " sessPreviousPage " + session.getObject("sessPreviousPage"));
        
        if (!sessPreviousPage.equals(null) || !sessPreviousPage.equals("")){
            try{
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " sessPreviousField " + session.getObject("sessPreviousField"));
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " sessXXWC.ITEM " + session.getObject("sessXXWC.ITEM"));
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " sessInventoryItemId " + session.getObject("sessInventoryItemId"));
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " sessXXWC.BIN " + session.getObject("sessXXWC.BIN"));
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " sessInventoryLocationId " + session.getObject("sessInventoryLocationId"));
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " sessXXWC.PRICE " + session.getObject("sessXXWC.PRICE"));
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " sessXXWC.ITEM_STATUS " + session.getObject("sessXXWC.ITEM_STATUS"));
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " sessXXWC.PRINT_LOCATION " + session.getObject("sessXXWC.PRINT_LOCATION"));
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " sessXXWC.SHELF_PRICE " + session.getObject("sessXXWC.SHELF_PRICE"));
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " sessXXWC.COPIES " + session.getObject("sessXXWC.COPIES"));
                
                mItemFld.setValue(session.getObject("sessXXWC.ITEM").toString());
                mBinFld.setValue(session.getObject("sessXXWC.BIN").toString());
                mPriceFld.setValue(session.getObject("sessXXWC.PRICE").toString());
                mItemLabelStatusFlag.setValue(session.getObject("sessXXWC.ITEM_STATUS").toString());
                mPrintLocationFld.setValue(session.getObject("sessXXWC.PRINT_LOCATION").toString());
                mShelfPricingFld.setValue(session.getObject("sessXXWC.SHELF_PRICE").toString());
                mCopies.setValue(session.getObject("sessXXWC.COPIES").toString());
                mItemFld.setmItemNumber(session.getObject("sessXXWC.ITEM").toString());
                mItemFld.setmInventoryItemId(session.getObject("sessInventoryItemId").toString());
                mBinFld.setmConcatedSegments(session.getObject("sessXXWC.BIN").toString());
                mBinFld.setmLocationId(session.getObject("sessXXWC.LOCATION_ID").toString());
                //mCancel.setPrompt("Cancel"); //added 5/12/2015
                session.setRefreshScreen(true);
            }
            catch (Exception e){
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " " + e);
            }
        }
        else {
            mItemFld.setValue("");
            mBinFld.setValue("");
            mPriceFld.setValue("");
            mItemLabelStatusFlag.setValue("T");
            mPrintLocationFld.setValue("");
            mShelfPricingFld.setValue("");
            mCopies.setValue("");
            //mCancel.setPrompt("Menu"); //added 5/12/2015
            session.setRefreshScreen(true);//added 5/12/2015
        }
    }
}
