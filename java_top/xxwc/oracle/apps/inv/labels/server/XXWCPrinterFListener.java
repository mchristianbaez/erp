/*************************************************************************
   *   $Header XXWCPrinterFListener.java $
   *   Module Name: XXWCPrinterFListener
   *   
   *   Package: package xxwc.oracle.apps.inv.labels.server;
   *   
   *   Imports Classes:
   *   
   *   import java.sql.*;
   *   import oracle.apps.inv.utilities.server.UtilFns;
   *   import oracle.apps.mwa.beans.*;
   *   import oracle.apps.mwa.container.MWALib;
   *   import oracle.apps.mwa.container.Session;
   *   import oracle.apps.mwa.eventmodel.*;
   *   import oracle.apps.mwa.container.FileLogger;
   *   import oracle.apps.fnd.flexj.FlexException;
   *   import oracle.apps.inv.lov.server.ItemLOV;
   *   import oracle.apps.inv.lov.server.LocatorKFF;
   *   import oracle.apps.inv.lov.server.SubinventoryLOV;
   *   import xxwc.oracle.apps.inv.lov.server.*;
   *
   *   PURPOSE:   Java Class for XXWCPrinterFListener Page Layout.
   *    
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
        1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                          TMS Ticket 20150302-00152 - RF Receiving
   *    1.1       11-FEB-2016   Lee Spitzer             TMS#20151103-00188 - RF Enhancements
**************************************************************************/
 package xxwc.oracle.apps.inv.labels.server;

import java.sql.*;
import java.util.Hashtable;
import java.util.Vector;
import oracle.apps.fnd.common.VersionInfo;
import oracle.apps.inv.lov.server.*;
import oracle.apps.inv.utilities.server.*;
import oracle.apps.mwa.beans.*;
import oracle.apps.mwa.container.FileLogger;
import oracle.apps.mwa.container.Session;
import oracle.apps.mwa.eventmodel.*;
import oracle.apps.mwa.presentation.telnet.TelnetSession;
import oracle.jdbc.OraclePreparedStatement;
import oracle.sql.NUMBER;
import oracle.apps.mwa.container.MWALib;
import oracle.apps.fnd.flexj.KeyFlexfield;
import xxwc.oracle.apps.inv.lov.server.*;


/*************************************************************************
 *   NAME: XXWCPrinterFListener implements MWAFieldListener
 *
 *   PURPOSE:   Main class for XXWCPrinterFListener
 *
 *   REVISIONS:
 *   Ver        Date        Author                     Description
 *   ---------  ----------  ---------------         -------------------------
        1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                          TMS Ticket 20150302-00152 - RF Receiving
 **************************************************************************/


public class XXWCPrinterFListener implements MWAFieldListener{
    XXWCPrinterPage pg;
    Session ses;
    String dialogPageButtons[];
    Integer g_return = -1;
    String g_message = "";
    
    /*************************************************************************
     *   NAME: private static String gPackage = "xxwc.oracle.apps.inv.labels.server";
     *
     *   PURPOSE:   private method to default gPackage value for debugging
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00152 - RF Receiving
     **************************************************************************/

    private static String gPackage = "xxwc.oracle.apps.inv.labels.server";


    /*************************************************************************
     *   NAME: private static String gCallFrom = "XXWCPrinterFListener";
     *
     *   PURPOSE:   private method to pass gCallFrom from methods to return class
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20150302-00152 - RF Receiving
     **************************************************************************/

    private static String gCallFrom = "XXWCPrinterFListener";

    /*************************************************************************
     *   NAME: public XXWCPrinterFListener()
     *
     *   PURPOSE:   public method XXWCPrinterFListener
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                               TMS Ticket 20150302-00152 - RF Receiving
     **************************************************************************/

    public XXWCPrinterFListener() {
        $init$();
    }

    /*************************************************************************
     *   NAME:  public void fieldEntered(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                       DefaultOnlyHandlerException
     *
     *   PURPOSE:   field entered for XXWCPrinterFListener
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20150302-00152 - RF Receiving
     *      1.1       11-FEB-2016   Lee Spitzer             TMS#20151103-00188 - RF Enhancements

     **************************************************************************/
    
    public void fieldEntered(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                       DefaultOnlyHandlerException {
        ses = mwaevent.getSession();
        pg = (XXWCPrinterPage)ses.getCurrentPage();
        String s = UtilFns.fieldEnterSource(ses);
        /*Sub*/
        if (s.equals("XXWC.PRINTER")) {
            enteredPrinter(mwaevent);
            return;
        }
        /*Loc*/
        if (s.equals("XXWC.LABEL_PRINTER")) {
            enteredLabelPrinter(mwaevent);
            return;
        }
        /*Added TMS#20151103-00188 XXWC.ITEM_STATUS and XXWC.PRINT_LOCATION*/
        /*Lable Size*/
        if (s.equals("XXWC.ITEM_STATUS")) {
            enteredItemStatus(mwaevent);
            return;
        } 
        /*UBD Status*/
        if (s.equals("XXWC.PRINT_LOCATION")) {
            enteredPrintLocation(mwaevent);
            return;
        }
        /*End Added TMS#20151103-00188*/
    }

    /*************************************************************************
     *   NAME:      public void fieldExited(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException
     *
     *   PURPOSE:   field exited for XXWCPrinterFListener
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20150302-00152 - RF Receiving
     *      1.1       11-FEB-2016   Lee Spitzer             TMS#20151103-00188 - RF Enhancements

     **************************************************************************/

    public void fieldExited(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException {
        String s = ((FieldBean)mwaevent.getSource()).getName();
        String vField = mwaevent.getAction();
        boolean flag = false;
        boolean flag1 = false;
        boolean flag3 = false;
        if (mwaevent.getAction().equals("MWA_SUBMIT")) {
            flag = true;
        } else if (mwaevent.getAction().equals("MWA_PREVIOUSFIELD")) {
            flag1 = true;
        } else if (mwaevent.getAction().equals("MWA_NEXTFIELD")) {
            flag3 = true;
        }
        /*Sub*/
        if (s.equals("XXWC.PRINTER")) {
            exitedPrinter(mwaevent, ses);
            return;
        }
        /*Loc*/
        if (s.equals("XXWC.LABEL_PRINTER")) {
            exitedLabelPrinter(mwaevent, ses);
            return;
        }
        /*Added TMS#20151103-00188 XXWC.ITEM_STATUS, XXWC.PRINT_LOCATION, and XXWC.COPIES*/
       /*Label Size*/
        if (s.equals("XXWC.ITEM_STATUS")) {
            exitedItemStatus(mwaevent, ses);
            return;
        }   
        
        /*Print Location*/
        if (s.equals("XXWC.PRINT_LOCATION")) {
            exitedPrintLocation(mwaevent, ses);
            return;
        }
        if (s.equals("XXWC.COPIES")) {
            exitedQty(mwaevent, ses);
            return;
        }   
        /*End TMS#20151103-00188 XXWC.ITEM_STATUS, XXWC.PRINT_LOCATION, and XXWC.COPIES*/
        /*Cancel*/
        if (flag && s.equals("INV.CANCEL")) {
            exitedCancel(mwaevent, ses);
            return;
        }
        /*SaveNext*/
        if (flag && s.equals("INV.SAVENEXT")) {
            setPrinter(mwaevent, ses);
            setLabelPrinter(mwaevent, ses);
            setItemLabelSize(mwaevent,ses);/*Added TMS#20151103-00188*/
            setItemLabelLocation(mwaevent,ses);/*Added TMS#20151103-00188*/
            setItemLabelCopies(mwaevent,ses);/*Added TMS#20151103-00188*/
            
            ses.setNextFieldName("XXWC.PRINTER");
            ses.putObject("sessEnterPage", "Y");/*Added TMS#20151103-00188*/
            return;
        }
        /*Done*/
        if (flag && s.equals("INV.DONE")) {
            setPrinter(mwaevent, ses);
            setLabelPrinter(mwaevent, ses);
            setItemLabelSize(mwaevent,ses);/*Added TMS#20151103-00188*/
            setItemLabelLocation(mwaevent,ses);/*Added TMS#20151103-00188*/
            setItemLabelCopies(mwaevent,ses);/*Added TMS#20151103-00188*/
            exitedDone(mwaevent, ses);
            return;
        }
        else {
            return;
        }
    }

    /*************************************************************************
     *   NAME:      private void $init$()
     *
     *   PURPOSE:   $init$ sets the diaglogButtons button for user prompt messages to OK
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
         1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                             TMS Ticket 20150302-00152 - RF Receiving
     **************************************************************************/

    private void $init$() {
        dialogPageButtons = (new String[] { "OK" });
    }

    /*************************************************************************
    *   NAME: public void enteredPrinter(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                     DefaultOnlyHandlerException
    *
    *   PURPOSE:  Entered Subinventory Field Listener Process to set the List of Values for the Subinventory Field
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
     *      1.1       11-FEB-2016   Lee Spitzer             TMS#20151103-00188 - RF Enhancements
    **************************************************************************/

    public void enteredItemStatus(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException {

         
       try {
           XXWCItemLabelStatusFlagLOV itemStatusLOV = pg.getmItemLabelStatusFlag();
           itemStatusLOV.setValidateFromLOV(true);
           itemStatusLOV.setlovStatement("XXWC_LABEL_PRINTERS_PKG.GET_ITEM_LBL_STATUS_LOV");
           String paramType[] = { "C", "S" };
           String parameters[] =
           { " ", "xxwc.oracle.apps.inv.labels.server.XXWCPrinterPage.XXWC.ITEM_STATUS" };
           itemStatusLOV.setInputParameterTypes(paramType);
           itemStatusLOV.setInputParameters(parameters);
       } catch (Exception e) {
           UtilFns.error("Error in calling enteredPrinter" + e);
       }
    }

    /*************************************************************************
    *   NAME: public void enteredPrintLocation(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                     DefaultOnlyHandlerException
    *
    *   PURPOSE:  Entered Item Field Listener Process to set the List of Values for the Subinventory Field
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
     *      1.1       11-FEB-2016   Lee Spitzer             TMS#20151103-00188 - RF Enhancements
         **************************************************************************/

    public void enteredPrintLocation(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException {

        
       try {
           XXWCYesNoLOV yesNoLOV = pg.getmPrintLocationFld();
           yesNoLOV.setValidateFromLOV(true);
           yesNoLOV.setlovStatement("XXWC_LABEL_PRINTERS_PKG.GET_YES_NO_LOV");
           
           String paramType[] = { "C", "S"};
           String prompts[] = { "LOOKUP_CODE" , "MEANING", "DESCRIPTION"};
           boolean flag[] = {false, false, true};
           String parameters[] = { " ", "xxwc.oracle.apps.inv.labels.server.XXWCPrinterPage.XXWC.PRINT_LOCATION"};
           yesNoLOV.setInputParameters(parameters);
           yesNoLOV.setInputParameterTypes(paramType);
           yesNoLOV.setSubfieldPrompts(prompts);
           yesNoLOV.setSubfieldDisplays(flag);      

       } catch (Exception e) {
           UtilFns.error("Error in calling enteredPrintLocation " + e);
       }
    }



    /*************************************************************************
    *   NAME: public void exitedItemStatus(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException
    *
    *   PURPOSE:  Exited ItemStatus Field Listener
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
    *      1.1       11-FEB-2016   Lee Spitzer             TMS#20151103-00188 - RF Enhancements
    **************************************************************************/

     public void exitedItemStatus(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                 DefaultOnlyHandlerException {
         try {
             if (pg.getmItemLabelStatusFlag().getValue().equals("") || pg.getmItemLabelStatusFlag().getValue().equals(null)) {
                 return;
             }
             else {
                 pg.getmItemLabelStatusFlag().getValue().toUpperCase();
             }
              
         }
         catch (Exception e) {
             UtilFns.error("Error in calling exitedItemStatus" + e);         
         }
    }

    /*************************************************************************
    *   NAME: public void exitedPrintLocation(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException
    *
    *   PURPOSE:  Exited Printer Location Field Listener
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
    *      1.1       11-FEB-2016   Lee Spitzer             TMS#20151103-00188 - RF Enhancements
    **************************************************************************/

     public void exitedPrintLocation(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                 DefaultOnlyHandlerException {
         try {
             if (pg.getmPrintLocationFld().getValue().equals("") || pg.getmPrintLocationFld().getValue().equals(null)) {
                 return;
             }
             else {
                 return;
             }
              
         }
         catch (Exception e) {
             UtilFns.error("Error in calling exitedItemStatus" + e);         
         }
    }

    
    /*************************************************************************
    *   NAME: public void enteredPrinter(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                     DefaultOnlyHandlerException
    *
    *   PURPOSE:  Entered Subinventory Field Listener Process to set the List of Values for the Subinventory Field
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                  TMS Ticket 20150302-00152 - RF Receiving
    *        1.1       11-FEB-2016   Lee Spitzer             TMS#20151103-00188 - RF Enhancements

    **************************************************************************/

    public void enteredPrinter(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException {

        String gMethod = "enteredPrinter";
        String gCallPoint = "Start";
        
        String x_printer = "";
        String x_label_printer = "";
        String x_label_item_status = ""; /*Added TMS#20151103-00188*/
        String x_label_item_location = ""; /*Added TMS#20151103-00188*/
        String x_label_item_copies = ""; /*Added TMS#20151103-00188*/
        
        String x_label_item_status_desc = ""; /*Added TMS#20151103-00188*/
        String x_label_item_location_mean = ""; /*Added TMS#20151103-00188*/
        
        //Added TMS#20151103-00188 logic when user first enters the field to get the defaults 
        if (ses.getObject("sessEnterPage").equals("Y")) {
        
        
        
       //Set Default Printers for the page based on user profile options
        x_printer = getDefaultPrinter("PRINTER");
        x_label_printer = getDefaultPrinter("XXWC_LABEL_PRINTER");
        x_label_item_status = getDefaultPrinter("XXWC_LABEL_ITEM_SIZE"); /*Added TMS#20151103-00188*/
        x_label_item_location = getDefaultPrinter("XXWC_LABEL_ITEM_LOCATION");/*Added TMS#20151103-00188*/
        x_label_item_copies = getDefaultPrinter("XXWC_LABEL_ITEM_COPIES");/*Added TMS#20151103-00188*/
        
        FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " x_printer " + x_printer);
        FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " x_label_printer " + x_label_printer);
        FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " x_label_item_status " + x_label_item_status);
        FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " x_label_item_location " + x_label_item_location);
        FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " x_label_item_copies " + x_label_item_copies);
        
        pg.getmPrinterFld().setValue(x_printer);
        pg.getmLabelPrinterFld().setValue(x_label_printer);
        pg.getmItemLabelStatusFlag().setmFlexValue(x_label_item_status);/*Added TMS#20151103-00188*/
        pg.getmPrintLocationFld().setmLookupCode(x_label_item_location);/*Added TMS#20151103-00188*/
        
        gCallPoint = "Get Item Label Description";
        try{
            CallableStatement cstmt = null;
            Connection con = ses.getConnection();
            cstmt = con.prepareCall("{? = call XXWC_LABEL_PRINTERS_PKG.GET_ITEM_LABEL_DESC(?)}");
            cstmt.registerOutParameter(1, Types.VARCHAR);
            cstmt.setString(2, x_label_item_status);
            cstmt.execute();
            x_label_item_status_desc = cstmt.getString(1);
            cstmt.close();
            if (x_label_item_status_desc.equals(null) || x_label_item_status_desc.equals("")) {
                x_label_item_status_desc = "";
            }
        }
        catch(Exception e){
            UtilFns.error("Error in calling XXWC_LABEL_PRINTERS_PKG.GET_ITEM_LABEL_DESC " + e);
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " " + e);
            
        }
        
        FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " x_label_item_status_desc " + x_label_item_status_desc);
        
        gCallPoint = "Get Yes No Mean";
        
        try{
            CallableStatement cstmt = null;
            Connection con = ses.getConnection();
            cstmt = con.prepareCall("{? = call XXWC_LABEL_PRINTERS_PKG.GET_YES_NO_MEAN(?)}");
            cstmt.registerOutParameter(1, Types.VARCHAR);
            cstmt.setString(2, x_label_item_location);
            cstmt.execute();
            x_label_item_location_mean = cstmt.getString(1);
            cstmt.close();
            if (x_label_item_location_mean.equals(null) || x_label_item_location_mean.equals("")) {
                x_label_item_location_mean = "";
            }
        }
        catch(Exception e){
            UtilFns.error("Error in calling XXWC_LABEL_PRINTERS_PKG.GET_YES_NO_MEAN " + e);
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " " + e);
        }
        
        FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " x_label_item_location_mean " + x_label_item_location_mean);
        
        pg.getmItemLabelStatusFlag().setValue(x_label_item_status_desc);
        pg.getmPrintLocationFld().setValue(x_label_item_location_mean);
        
        pg.getmCopies().setValue(x_label_item_copies);/*Added TMS#20151103-00188*/
        
        ses.putObject("sessEnterPage", "N");
        
        } 
        else {
            ses.putObject("sessEnterPage", "N");
        }/*End Added TMS#20151103-00188 End If Statement on entering page*/
        
       try {
           XXWCPrintersLOV printerLOV = pg.getmPrinterFld();
           printerLOV.setValidateFromLOV(true);
           printerLOV.setlovStatement("XXWC_LABEL_PRINTERS_PKG.GET_PRINTERS_LOV");
           String paramType[] = { "C", "S" };
           String parameters[] =
           { " ", "xxwc.oracle.apps.inv.labels.server.XXWCPrinterPage.XXWC.PRINTER" };
           printerLOV.setInputParameterTypes(paramType);
           printerLOV.setInputParameters(parameters);
       } catch (Exception e) {
           UtilFns.error("Error in calling enteredPrinter" + e);
       }
    }

    /*************************************************************************
    *   NAME: public void exitedPrinter(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException
    *
    *   PURPOSE:  Exited Subinventory Field Listener Process to determine if the Locator Field should be displayed
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20150302-00152 - RF Receiving
    **************************************************************************/

    public void exitedPrinter(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                DefaultOnlyHandlerException {
       try {
           return;
       } catch (Exception e) {
           UtilFns.error("Error in calling exitedSub" + e);
       }
    }

 
    /*************************************************************************
    *   NAME: public void enteredLabelPrinter(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException
    *
    *   PURPOSE:  Entered Locator Field Listener Process to set the List of Values for the Locator Field
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
         1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00152 - RF Receiving
    **************************************************************************/

    public void enteredLabelPrinter(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                        DefaultOnlyHandlerException {

        try {
            XXWCPrintersLOV labelPrinterLOV = pg.getmLabelPrinterFld();
            labelPrinterLOV.setValidateFromLOV(true);
            labelPrinterLOV.setlovStatement("XXWC_LABEL_PRINTERS_PKG.GET_PRINTERS_LOV");
            String paramType[] = { "C", "S" };
            String parameters[] =
            { " ", "xxwc.oracle.apps.inv.labels.server.XXWCPrinterPage.XXWC.LABEL_PRINTER" };
            labelPrinterLOV.setInputParameterTypes(paramType);
            labelPrinterLOV.setInputParameters(parameters);
        } catch (Exception e) {
            UtilFns.error("Error in calling enteredLabelPrinter" + e);
        }
    }

    /*************************************************************************
    *   NAME: public void exitedLabelPrinter(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException
    *
    *   PURPOSE:  Exited Subinventory Field Listener Process to determine if the Locator Field should be displayed
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20150302-00152 - RF Receiving
    **************************************************************************/

    public void exitedLabelPrinter(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                DefaultOnlyHandlerException {
       try {
           return;
        }
           
       catch (Exception e) {
           UtilFns.error("Error in calling exitedLocator" + e);
       }
    }


    /*************************************************************************
    *   NAME: public void exitedQty(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException
    *
    *   PURPOSE:  Exited Subinventory Field Listener Process to determine if the Locator Field should be displayed
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
        1.0       11-FEB-2016   Lee Spitzer             TMS#20151103-00188 - RF Enhancements
                                                                
    **************************************************************************/

    public void exitedQty(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                DefaultOnlyHandlerException {
        try {
            CallableStatement cstmt = null;
            Connection con = ses.getConnection();
            cstmt = con.prepareCall("{call XXWC_LABEL_PRINTERS_PKG.validate_lbl_qty_val(?,?,?)}");
            cstmt.setString(1, pg.getmCopies().getValue());
            cstmt.registerOutParameter(2, Types.INTEGER); //x_return
            cstmt.registerOutParameter(3, Types.VARCHAR); //x_message
            cstmt.execute();
            Integer x_return = cstmt.getInt(2);
            String  x_message = cstmt.getString(3);
            cstmt.close();
            boolean showSubError = true;
            if (x_return > 0) {
                TelnetSession telnetsessionX = (TelnetSession)ses;
                Integer k =
                    telnetsessionX.showPromptPage("Error!", x_message,
                                                  dialogPageButtons);
                if (k == 0) {
                    pg.getmCopies().setValue(null);
                    ses.setNextFieldName("XXWC.COPIES");
                } else {
                    return;
                }
            }
            
            Integer l_copies = Integer.parseInt(pg.getmCopies().getValue());
            Integer l_max_copies = 25;
            
            if (l_copies > l_max_copies){
                TelnetSession telnetsessionX = (TelnetSession)ses;
                Integer k =
                    telnetsessionX.showPromptPage("Error!", l_copies + " number of copies is greater than " + l_max_copies,
                                                  dialogPageButtons);
                if (k == 0) {
                    pg.getmCopies().setValue(null);
                    ses.setNextFieldName("XXWC.COPIES");
                } else {
                    return;
                }
            }
            //if (x_return == 0) {
            //    ses.setNextFieldName("XXWC.UBD_STATUS");
            //}
        
       } catch (Exception e) {
           UtilFns.error("Error in calling exitedCopies" + e);
       }
    }


    /*************************************************************************
    *   NAME: public void exitedCancel(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                     DefaultOnlyHandlerException
    *
    *   PURPOSE:  Exited Close For Field Listener Process to proces the Close Button
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                               TMS Ticket 20150302-00152 - RF Receiving
    **************************************************************************/

    public void exitedCancel(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                   DefaultOnlyHandlerException {
        try {
            XXWCPrinterPage _tmp = pg;
            ses.setStatusMessage(pg.WMS_TXN_CANCEL);
            ses.clearAllApplicationScopeObjects();
            ses.setStatusMessage(XXWCPrinterPage.WMS_TXN_CANCEL);
            pg.getmCancel().setNextPageName("|END_OF_TRANSACTION|");
        } catch (Exception e) {
            UtilFns.error("Error in calling exitedCancel " + e);
        }

    }
    
    /*************************************************************************
    *   NAME:     public void setPrinter(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                                 InterruptedHandlerException,
                                                                                 DefaultOnlyHandlerException
    *
    *   PURPOSE:  Execute the procedure XXWC_LABEL_PRINTERS_PKG.SET_PRINTER
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
         1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                              TMS Ticket 20150302-00152 - RF Receiving
    **************************************************************************/
    
    public void setPrinter(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                             InterruptedHandlerException,
                                                                             DefaultOnlyHandlerException {
        String p_printer = (String)pg.getmPrinterFld().getValue();
        executeProfileUpdate(p_printer, "PRINTER", ses);
        
        if (!g_return.equals(0)) {
            TelnetSession telnetsessionX = (TelnetSession)ses;
            int k = telnetsessionX.showPromptPage("Error!", g_message, dialogPageButtons);
            if (k == 0) {
                ses.setRefreshScreen(true);
            } else {
                return;
            }
        }
    }


    /*************************************************************************
    *   NAME:     public void setLabelPrinter(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                                 InterruptedHandlerException,
                                                                                 DefaultOnlyHandlerException
    *
    *   PURPOSE:  Execute the procedure XXWC_INV_PRIMARY_BIN_PKG.PROCESS_ITEM_LOCATOR_TIE
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
         1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                              TMS Ticket 20150302-00152 - RF Receiving
    **************************************************************************/
    
    public void setLabelPrinter(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                             InterruptedHandlerException,
                                                                             DefaultOnlyHandlerException {
        String p_label_printer = (String)pg.getmLabelPrinterFld().getValue();
        executeProfileUpdate(p_label_printer, "XXWC_LABEL_PRINTER", ses);
        
        if (!g_return.equals(0)) {
            TelnetSession telnetsessionX = (TelnetSession)ses;
            int k = telnetsessionX.showPromptPage("Error!", g_message, dialogPageButtons);
            if (k == 0) {
                ses.setRefreshScreen(true);
            } else {
                return;
            }
        }
    }
    

    /*************************************************************************
    *   NAME:     public void setItemLabelSize(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                                 InterruptedHandlerException,
                                                                                 DefaultOnlyHandlerException
    *
    *   PURPOSE:  Execute the procedure XXWC_INV_PRIMARY_BIN_PKG.PROCESS_ITEM_LOCATOR_TIE
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
         1.0       11-FEB-2016   Lee Spitzer             TMS#20151103-00188 - RF Enhancements  
    **************************************************************************/
    
    public void setItemLabelSize(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                             InterruptedHandlerException,
                                                                             DefaultOnlyHandlerException {
        String p_item_label_size = (String)pg.getmItemLabelStatusFlag().getmFlexValue();
        executeProfileUpdate(p_item_label_size, "XXWC_LABEL_ITEM_SIZE", ses);
        
        if (!g_return.equals(0)) {
            TelnetSession telnetsessionX = (TelnetSession)ses;
            int k = telnetsessionX.showPromptPage("Error!", g_message, dialogPageButtons);
            if (k == 0) {
                ses.setRefreshScreen(true);
            } else {
                return;
            }
        }
    }


    /*************************************************************************
    *   NAME:     public void setItemLabelLocation(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                                 InterruptedHandlerException,
                                                                                 DefaultOnlyHandlerException
    *
    *   PURPOSE:  Execute the procedure XXWC_INV_PRIMARY_BIN_PKG.PROCESS_ITEM_LOCATOR_TIE
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
         1.0       11-FEB-2016   Lee Spitzer             TMS#20151103-00188 - RF Enhancements  
    **************************************************************************/
    
    public void setItemLabelLocation(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                             InterruptedHandlerException,
                                                                             DefaultOnlyHandlerException {
        String p_item_label_location = (String)pg.getmPrintLocationFld().getmLookupCode();
        executeProfileUpdate(p_item_label_location, "XXWC_LABEL_ITEM_LOCATION", ses);
        
        if (!g_return.equals(0)) {
            TelnetSession telnetsessionX = (TelnetSession)ses;
            int k = telnetsessionX.showPromptPage("Error!", g_message, dialogPageButtons);
            if (k == 0) {
                ses.setRefreshScreen(true);
            } else {
                return;
            }
        }
    }


    /*************************************************************************
    *   NAME:     public void setItemLabelCopies(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                                 InterruptedHandlerException,
                                                                                 DefaultOnlyHandlerException
    *
    *   PURPOSE:  Execute the procedure XXWC_INV_PRIMARY_BIN_PKG.PROCESS_ITEM_LOCATOR_TIE
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
         1.0       11-FEB-2016   Lee Spitzer             TMS#20151103-00188 - RF Enhancements  
    **************************************************************************/
    
    public void setItemLabelCopies(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                             InterruptedHandlerException,
                                                                             DefaultOnlyHandlerException {
        String p_item_label_copies = (String)pg.getmCopies().getValue();
        executeProfileUpdate(p_item_label_copies, "XXWC_LABEL_ITEM_COPIES", ses);
        
        if (!g_return.equals(0)) {
            TelnetSession telnetsessionX = (TelnetSession)ses;
            int k = telnetsessionX.showPromptPage("Error!", g_message, dialogPageButtons);
            if (k == 0) {
                ses.setRefreshScreen(true);
            } else {
                return;
            }
        }
    }

    /*************************************************************************
    *   NAME:     public String executeProfileUpdate (String p_printer, String p_profile_option_name) 
    *
    *   PURPOSE:  Execute the procedure XXWC_LABEL_PRINTERS_PKG.SET_PRINTER
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
         1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                              TMS Ticket 20150302-00152 - RF Receiving
         1.0       11-FEB-2016   Lee Spitzer             TMS#20151103-00188 - RF Enhancements
    **************************************************************************/

    public void executeProfileUpdate (String p_printer, String p_profile_option_name, Session ses) {
    
    Integer x_return = -1;
    String x_message = "";
    
        try {
            //                                                                  1 2 3 4
            CallableStatement cstmt = null;
            Connection con = ses.getConnection();
            cstmt = con.prepareCall("{call XXWC_LABEL_PRINTERS_PKG.set_printers(?,?,?,?)}");
            cstmt.setString(1, p_printer);
            cstmt.setString(2, p_profile_option_name);
            cstmt.registerOutParameter(3, Types.NUMERIC);
            cstmt.registerOutParameter(4, Types.VARCHAR);
            cstmt.execute();
            x_return = cstmt.getInt(3);
            x_message = cstmt.getString(4);
            cstmt.close();
        } catch (Exception e) {
            ses.setStatusMessage((new StringBuilder()).append(pg.WMS_TXN_ERROR).append(" (").append(e).append(")").toString());
            UtilFns.error((new StringBuilder()).append("Error in executeProfileUpdate: error - ").append(e).toString());
        }
    
        g_return = x_return;
        g_message = "Error updating profile " + p_profile_option_name + " with value " + p_printer + " "+  x_message; //added TMS#20151103-00188
    }
 
    
    /*************************************************************************
    *   NAME:     public void exitedDone(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                                 InterruptedHandlerException,
                                                                                 DefaultOnlyHandlerException
    *
    *   PURPOSE:  Clears the values in the form
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
        1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00152 - RF Receiving

    **************************************************************************/
    
    protected void exitedDone(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                        DefaultOnlyHandlerException {
        try {
                XXWCPrinterPage _tmp = pg;
                ses.setStatusMessage(pg.WMS_TXN_SUCCESS);
                ses.clearAllApplicationScopeObjects();
                pg.getmPrinterFld().setValue("");
                pg.getmLabelPrinterFld().setValue("");                   
                pg.getmItemLabelStatusFlag().setValue("");    
                pg.getmDone().setNextPageName("|END_OF_TRANSACTION|");
            } 
        catch (Exception e) {
            UtilFns.error("Error in calling exitedDone " + e);
        }
    
    }
    

    /*************************************************************************
    *   NAME:     public void getDefaultPrinter(MWAEvent mwaevent) throws AbortHandlerException,
                                                                                 InterruptedHandlerException,
                                                                                 DefaultOnlyHandlerException
    *
    *   PURPOSE:  Gets the default printer value for a user
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
        1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00152 - RF Receiving

    **************************************************************************/
    
    public String getDefaultPrinter(String p_profile_option_name) {
        String x_printer = "";
        try{
            CallableStatement cstmt = null;
            Connection con = ses.getConnection();
            cstmt = con.prepareCall("{? = call XXWC_LABEL_PRINTERS_PKG.get_default_printer(?)}");
            cstmt.registerOutParameter(1, Types.VARCHAR);
            cstmt.setString(2, p_profile_option_name);
            cstmt.execute();
            x_printer = cstmt.getString(1);
            cstmt.close();
            if (x_printer.equals(null) || x_printer.equals("")) {
                x_printer = "";
            }
        }
        catch (Exception e){
            UtilFns.error("Error in calling getDefaultPrinter " + e);
        }
        return x_printer;
    }
        
}
