/*************************************************************************
   *   $Header XXWCUBDPage.java $
   *   Module Name: XXWCUBDPage
   *   
   *   Package: package xxwc.oracle.apps.inv.labels.server;
   *   
   *   Imports Classes:
   *   
   *   import java.sql.*;
   *   import oracle.apps.inv.utilities.server.UtilFns;
   *   import oracle.apps.mwa.beans.*;
   *   import oracle.apps.mwa.container.MWALib;
   *   import oracle.apps.mwa.container.Session;
   *   import oracle.apps.mwa.eventmodel.*;
   *   import oracle.apps.mwa.container.FileLogger;
   *   import oracle.apps.fnd.flexj.FlexException;
   *   import oracle.apps.inv.lov.server.ItemLOV;
   *   import oracle.apps.inv.lov.server.LocatorKFF;
   *   import oracle.apps.inv.lov.server.SubinventoryLOV;
   *   import xxwc.oracle.apps.inv.lov.server.*;
   *
   *   PURPOSE:   Java Class for XXWCUBDPage Page Layout.
   *    
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
        1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                          TMS Ticket 20150302-00152 - RF Receiving
**************************************************************************/

package xxwc.oracle.apps.inv.labels.server;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import oracle.apps.mwa.beans.*;
import oracle.apps.mwa.container.MWALib;
import oracle.apps.mwa.container.Session;
import oracle.apps.mwa.eventmodel.*;
import oracle.apps.mwa.container.FileLogger;
import oracle.apps.fnd.flexj.FlexException;
import oracle.apps.fnd.flexj.KeyFlexfield;
import oracle.apps.inv.invinq.server.ItemOnhandIterator;
import oracle.apps.inv.utilities.server.*;
import xxwc.oracle.apps.inv.lov.server.*;

/*************************************************************************
 *   NAME: XXWCUBDPage extends PageBean
 *
 *   PURPOSE:   Main class for XXWCUBDPage
 *
 *   REVISIONS:
 *   Ver        Date        Author                     Description
 *   ---------  ----------  ---------------         -------------------------
        1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                          TMS Ticket 20150302-00152 - RF Receiving
 **************************************************************************/


public class XXWCUBDPage extends PageBean{
    /*Define objects in the class*/
    public static String WMS_TXN_SUCCESS = "Txn Successful";
    public static String WMS_TXN_ERROR = "Txn Failed. Please check log";
    public static String WMS_TXN_CANCEL = "Txn Cancelled";
    public static String WMS_LOT_INS_FAIL = "Lot Insert Failed";
    public static String WMS_SER_INS_FAIL = "Serial Insert Failed";
    public static String savenextPrompt = "";
    public static String donePrompt = "";
    public static String cancelPrompt = "";
    private boolean areThePromptsSet;
    protected TextFieldBean mShelfLife;
    protected TextFieldBean mUBDDate;
    protected TextFieldBean mCopies;
    protected XXWCUBDStatusFlagLOV mUBDStatusFlag;
    protected ButtonFieldBean mCancel;
    protected ButtonFieldBean mSaveNext;
    protected ButtonFieldBean mDone;
    protected String m_userid;


    /*************************************************************************
     *   NAME: private static String gPackage = "xxwc.oracle.apps.inv.labels.server";
     *
     *   PURPOSE:   private method to default gPackage value for debugging
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00152 - RF Receiving
     **************************************************************************/

    private static String gPackage = "xxwc.oracle.apps.inv.labels.server";


    /*************************************************************************
     *   NAME: private static String gCallFrom = "XXWCUBDPage";
     *
     *   PURPOSE:   private method to pass gCallFrom from methods to return class name CustomListener
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00152 - RF Receiving
     **************************************************************************/

    private static String gCallFrom = "XXWCUBDPage";



    /*************************************************************************
     *   NAME: public XXWCUBDPage(Session session)
     *
     *   PURPOSE:   Inherits session from XXWCPrimaryBinAssignmentPage
     *
     *   Parameters: Session session
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                               TMS Ticket 20150302-00152 - RF Receiving
     **************************************************************************/

    public XXWCUBDPage(Session session) {
        initLayout(session);
    }

    
    /*************************************************************************
     *   NAME: private void initLayout(Session session)
     *
     *   PURPOSE:   Sets the initial layout of the page
     *
     *   Parameters: Session session
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
          1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                             TMS Ticket 20150302-00152 - RF Receiving
     **************************************************************************/

    private void initLayout(Session session) {
        m_userid = "";
        WMS_TXN_SUCCESS = UtilFns.getMessage(session, "WMS", "WMS_TXN_SUCCESS");
        WMS_TXN_ERROR = UtilFns.getMessage(session, "WMS", "WMS_TXN_ERROR");
        WMS_TXN_CANCEL = UtilFns.getMessage(session, "WMS", "WMS_TXN_CANCEL");
        WMS_LOT_INS_FAIL = UtilFns.getMessage(session, "WMS", "WMS_LOT_INS_FAIL");
        WMS_SER_INS_FAIL = UtilFns.getMessage(session, "WMS", "WMS_SER_INS_FAIL");
        /*Shelf Life*/
        mShelfLife = new TextFieldBean();
        mShelfLife.setName("XXWC.SHELF_LIFE");
        mShelfLife.setEditable(false);
        mShelfLife.setRequired(true);
        /*UBD*/
        mUBDDate = new TextFieldBean ();
        mUBDDate.setName("XXWC.UBD");
        mUBDDate.setRequired(true);
        /*Copies*/
        mCopies = new TextFieldBean ();
        mCopies.setName("XXWC.COPIES");
        mCopies.setRequired(true);
        /*Copies*/
        mUBDStatusFlag = new XXWCUBDStatusFlagLOV();
        mUBDStatusFlag.setName("XXWC.UBD_STATUS");
        mUBDStatusFlag.setRequired(true);
        mUBDStatusFlag.setValue("4X1");
        /*Save Properties*/
        //mSaveNext = new ButtonFieldBean();
        //mSaveNext.setName("INV.SAVENEXT");
        //mSaveNext.setPrompt("Save/Next");
        //mSaveNext.setEnableAcceleratorKey(true);
        //mSaveNext.setHidden(true);
        /*Save Properties*/
        mDone = new ButtonFieldBean();
        mDone.setName("XXWC.UBD_PRINT");
        mDone.setPrompt("Done");
        mDone.setEnableAcceleratorKey(true);
        mDone.setHidden(false);
        /*Cancel Properties*/
        mCancel = new ButtonFieldBean();
        mCancel.setName("INV.CANCEL");
        mCancel.setPrompt("Menu"); //updated 5/11/2015 from Cancel to Menu
        mCancel.setEnableAcceleratorKey(true);
        mCancel.setHidden(false);
        /*set the properties for each to the addFieldBean*/
        addFieldBean(mShelfLife);
        addFieldBean(mUBDDate);
        addFieldBean(mCopies);
        addFieldBean(mUBDStatusFlag);
        //addFieldBean(mSaveNext);
        addFieldBean(mDone);
        addFieldBean(mCancel);
        XXWCUBDFListener fieldListener = new XXWCUBDFListener();
        /*Add Listener to each property*/
        mShelfLife.addListener(fieldListener);
        mUBDDate.addListener(fieldListener);
        mCopies.addListener(fieldListener);
        mUBDStatusFlag.addListener(fieldListener);
        //mSaveNext.addListener(fieldListener);
        mDone.addListener(fieldListener);
        mCancel.addListener(fieldListener);
        try {
            setPrompts(session);
        } catch (Exception exception) {
            session.setStatusMessage(UtilFns.getMessage(session, "WMS", "WMS_PROMPT_NOTSET" + exception));
        }
    }
    
    /*************************************************************************
     *   NAME: private void initPrompts(Session session) throws SQLException
     *
     *   PURPOSE:   Sets the initial prompts of the session
     *
     *   Parameters: Session session
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
          1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                             TMS Ticket 20150302-00152 - RF Receiving
     **************************************************************************/

    private void initPrompts(Session session) throws SQLException {
        try {
            String gMethod = "initPrompts";
            String gCallPoint = "Start";
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
            String s = (String)session.getObject("ORGCODE");
            setPrompt((new StringBuilder()).append("XXWC UBD Labels (").append(s).append(")").toString());

            mShelfLife.setPrompt("Shelf Life");
            mUBDDate.setPrompt("UBD");
            mCopies.setPrompt("Label Qty");
            mUBDStatusFlag.setPrompt("Label Size");
            savenextPrompt = MWALib.getAKPrompt(session, "oracle.apps.inv.utilities.InvResourceTable", "INV_SAVE_NEXT_PROMPT");
            donePrompt = MWALib.getAKPrompt(session, "oracle.apps.inv.utilities.InvResourceTable", "INV_DONE_PROMPT");
            cancelPrompt = MWALib.getAKPrompt(session, "oracle.apps.inv.utilities.InvResourceTable", "INV_CANCEL_PROMPT");
            areThePromptsSet = true;
            gCallPoint = "End";
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
        } catch (SQLException sqlexception) {
            session.setStatusMessage(UtilFns.getMessage(session, "WMS", "WMS_TXN_ERROR"));
        }
    }

    /*************************************************************************
     *   NAME: private void setPrompts(Session session)
     *
     *   PURPOSE:   Sets the set prompts of the session
     *
     *   Parameters: Session session
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
          1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                             TMS Ticket 20150302-00152 - RF Receiving
     **************************************************************************/

    private void setPrompts(Session session) {
            String gMethod = "setPrompts";
            String gCallPoint = "Start";
            if (!areThePromptsSet) {
                try {
                    initPrompts(session);
                } catch (SQLException sqlexception) {
                }
            }
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
            String s = (String)session.getObject("ORGCODE");
            setPrompt((new StringBuilder()).append("XXWC UBD Labels (").append(s).append(")").toString());

            //mSaveNext.setPrompt(savenextPrompt);
            mDone.setPrompt("Print Label");
            mCancel.setPrompt(cancelPrompt);
            //mSaveNext.retrieveAttributes("INVE_SAVE_NEXT_PROMPT");
            mDone.retrieveAttributes("INV_DONE_PROMPT");
            mCancel.retrieveAttributes("INV_CANCEL_PROMPT");
            //mCancel.setPrompt("Menu");
            gCallPoint = "End Prompts";
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
            
            setFieldValues(session);
    }


     /*************************************************************************
     *   NAME: public XXWCPrintersLOV getmShelfLife(
     *
     *   PURPOSE:   return the value of getmShelfLife
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00152 - RF Receiving
     **************************************************************************/

    public TextFieldBean getmShelfLife() {
        return mShelfLife;
    }

    /*************************************************************************
    *   NAME: public XXWCPrintersLOV getmUBDDate()
    *
    *   PURPOSE:   return the value of getmUBDDate
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
           1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                               TMS Ticket 20150302-00152 - RF Receiving
    **************************************************************************/

    public TextFieldBean getmUBDDate() {
        return mUBDDate;
    }
    

    /*************************************************************************
    *   NAME: public XXWCPrintersLOV getmUBDDate()
    *
    *   PURPOSE:   return the value of getmUBDDate
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
           1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                               TMS Ticket 20150302-00152 - RF Receiving
    **************************************************************************/

    public TextFieldBean getmCopies() {
        return mCopies;
    }
    /*************************************************************************
     *   NAME: public ButtonFieldBean getmCancel
     *
     *   PURPOSE:   return the value of mCancel
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00152 - RF Receiving
     **************************************************************************/

     public XXWCUBDStatusFlagLOV getmUBDStatusFlag() {
         return mUBDStatusFlag;
     }
    
     /*************************************************************************
      *   NAME: public ButtonFieldBean getmCancel
      *
      *   PURPOSE:   return the value of mCancel
      *
      *   REVISIONS:
      *   Ver        Date        Author                     Description
      *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20150302-00152 - RF Receiving
      **************************************************************************/

    public ButtonFieldBean getmCancel() {
        return mCancel;
    }

    /*************************************************************************
     *   NAME: public ButtonFieldBean getmSaveNext()
     *
     *   PURPOSE:   return the value of mSaveNext
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00152 - RF Receiving
     **************************************************************************/

    //public ButtonFieldBean getmSaveNext() {
    //    return mSaveNext;
    //}

    /*************************************************************************
     *   NAME: public ButtonFieldBean getmDone()
     *
     *   PURPOSE:   return the value of mDone
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00152 - RF Receiving
     **************************************************************************/

    public ButtonFieldBean getmDone() {
        return mDone;
    }


    /*************************************************************************
     *   NAME: public String getUserId()
     *
     *   PURPOSE:   return the value of m_userid
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00152 - RF Receiving
     **************************************************************************/

    public String getUserId() {
        return m_userid;
    }

    /*************************************************************************
     *   NAME: void pageEntered(MWAEvent e) throws AbortHandlerException, InterruptedHandlerException,
                                               DefaultOnlyHandlerException {
     *
     *   PURPOSE:   pageEntered method
     *
     *   Parameters: MWAEvent mwaevent
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00152 - RF Receiving
     **************************************************************************/

    public void pageEntered(MWAEvent e) throws AbortHandlerException, InterruptedHandlerException,
                                               DefaultOnlyHandlerException {
        Session session = e.getSession();
        
    }

    /*************************************************************************
     *   NAME: void pageExited(MWAEvent e) throws AbortHandlerException, InterruptedHandlerException,
                                               DefaultOnlyHandlerException {
     *
     *   PURPOSE:   pageExited method
     *
     *
     *   Parameters: MWAEvent   mwaevent
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00152 - RF Receiving

     **************************************************************************/

    public void pageExited(MWAEvent e) throws AbortHandlerException, InterruptedHandlerException,
                                              DefaultOnlyHandlerException {
        Session session = e.getSession();

        session.putSessionObject("sessPreviousField", "");
        session.putSessionObject("sessXXWC.SHELF_LIFE", "");
        session.putSessionObject("sessXXWC.COPIES", "");
        session.putSessionObject("sessXXWC.UBD_STATUS", "");

    }
   

    /*************************************************************************
     *   NAME: void setFieldValues(Session session) throws AbortHandlerException, InterruptedHandlerException,
                                               DefaultOnlyHandlerException {
     *
     *   PURPOSE:   pageExited method
     *
     *
     *   Parameters: MWAEvent   mwaevent
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00152 - RF Receiving

     **************************************************************************/
   
    public void setFieldValues(Session session){
        String gMethod = "sesFieldValues";
        String gCallPoint = "Start";
        session.setRefreshScreen(true);
        String sessPreviousPage = "";
            try {
                sessPreviousPage = (String)session.getObject("sessPreviousPage").toString();
            }
            catch (Exception e) {
                sessPreviousPage = "";
                session.putSessionObject("sessPreviousField", "");
                session.putSessionObject("sessXXWC.SHELF_LIFE", "");
                session.putSessionObject("sessXXWC.COPIES", "");
                session.putSessionObject("sessXXWC.UBD_STATUS", "");
                //mCancel.setPrompt("Menu"); //added 5/12/2015
                session.setRefreshScreen(true);//added 5/12/2015
                
            }
        
        if (!sessPreviousPage.equals(null) || !sessPreviousPage.equals("")){
            mShelfLife.setValue(session.getObject("sessXXWC.SHELF_LIFE").toString());
            mCopies.setValue(session.getObject("sessXXWC.COPIES").toString());
            mUBDStatusFlag.setValue(session.getObject("sessXXWC.UBD_STATUS").toString());
            //mCancel.setPrompt("Cancel"); //added 5/12/2015
            session.setRefreshScreen(true);//added 5/12/2015
        }
        else {
            mShelfLife.setValue("");
            mCopies.setValue("");
            mUBDStatusFlag.setValue("F");
            //mCancel.setPrompt("Menu"); //added 5/12/2015
            session.setRefreshScreen(true); //added 5/12/2015
        }
    }
}
