/*************************************************************************
   *   $Header XXWCPrinterFunction.java $
   *   Module Name: XXWCPrinterFunction
   *   
   *   Package: package xxwc.oracle.apps.inv.labels;
   *   
   *   Imports Classes:
   *   
   *   import oracle.apps.fnd.common.VersionInfo;
   *   import oracle.apps.inv.utilities.server.OrgFunction;
   *   import oracle.apps.inv.utilities.server.UtilFns;
   *   import oracle.apps.mwa.eventmodel.*;
   *   
   *   PURPOSE:   Java Class for Printer Sign on Function
   *    
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
        1.0       17-FEB-2015   Lee Spitzer             Initial Version - 
                                                            TMS Ticket 20150302-00152 - RF Receiving
**************************************************************************/

package xxwc.oracle.apps.inv.labels.server;

import oracle.apps.fnd.common.VersionInfo;
import oracle.apps.inv.utilities.server.OrgFunction;
import oracle.apps.inv.utilities.server.UtilFns;
import oracle.apps.mwa.eventmodel.*;


/*************************************************************************
 *   NAME: public class XXWCPrinterFunction extends OrgFunction implements MWAAppListener
 *
 *   PURPOSE:   Main class for XXWCPrimaryBinAssignmentFunction
 *
 *   REVISIONS:
 *   Ver        Date        Author                     Description
 *   ---------  ----------  ---------------         -------------------------
        1.0       17-FEB-2015   Lee Spitzer             Initial Version -
                                                         TMS Ticket 20150302-00152 - RF Receiving
 **************************************************************************/


public class XXWCPrinterFunction extends OrgFunction implements MWAAppListener {

    /*************************************************************************
     *   NAME: public static final String RCS_ID
     *
     *   PURPOSE:   Returns Header File Name
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       17-FEB-2015   Lee Spitzer             Initial Version -
                                                              TMS Ticket 20150302-00152 - RF Receiving
     **************************************************************************/
    
    public static final String RCS_ID =
        "$Header: XXWCPrinterFunction.java 120.0 2015/02/09 05:18:34 appldev noship $";

    /*************************************************************************
     *   NAME: public static final boolean RCS_ID_RECORDED
     *
     *   PURPOSE:   Returns Header File Name
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       17-FEB-2015   Lee Spitzer             Initial Version -
                                                            TMS Ticket 20150302-00152 - RF Receiving
     **************************************************************************/

    public static final boolean RCS_ID_RECORDED =
        VersionInfo.recordClassVersion("$Header: XXWCPrinterFunction.java 120.0 2014/10/21 05:18:34 appldev noship $",
                                       "xxwc.oracle.apps.inv.labels.server");



    /*************************************************************************
     *   NAME: public XXWCPrinterFunction()
     *
     *   PURPOSE:   Menu function that points the mobile to the mobile page layout
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       17-FEB-2015   Lee Spitzer             Initial Version -
                                                             TMS Ticket 20150302-00152 - RF Receiving
     **************************************************************************/

    public XXWCPrinterFunction() {
        setFirstPageName("xxwc.oracle.apps.inv.labels.server.XXWCPrinterPage"); /*This should point to the file name XXWCRcptGenFunction.java in the local directory*/
        addListener(this);
        UtilFns.log("constructing XXWCPrinterPage function");
        
    }
    
    /*************************************************************************
     *   NAME: public void appEntered(MWAEvent mwaevent)
     *
     *   PURPOSE:   Standard appEntered Method and inherits
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       17-FEB-2015   Lee Spitzer             Initial Version -
                                                              TMS Ticket 20150302-00152 - RF Receiving
     **************************************************************************/

    public void appEntered(MWAEvent mwaevent) {
        UtilFns.log("App entered : complete");
        try {
            super.appEntered(mwaevent);
        } catch (Exception exception) {
            UtilFns.log("Error calling parent");
        }
    }


    /*************************************************************************
     *   NAME: appExited(MWAEvent mwaevent)
     *
     *   PURPOSE:   Standard appExited Method
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       17-FEB-2015   Lee Spitzer             Initial Version -
                                                              TMS Ticket 20150302-00152 - RF Receiving
     **************************************************************************/

    public void appExited(MWAEvent mwaevent) {
        UtilFns.log("App Exited : complete");
        try {
            super.appExited(mwaevent);
        } catch (Exception exception) {
            UtilFns.log("Error calling parent");
        }
    }
       
}
