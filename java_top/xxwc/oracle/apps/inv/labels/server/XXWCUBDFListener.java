/*************************************************************************
   *   $Header XXWCUBDFListener.java $
   *   Module Name: XXWCUBDFListener
   *   
   *   Package: package xxwc.oracle.apps.inv.labels.server;
   *   
   *   Imports Classes:
   *   
   *   import java.sql.*;
   *   import oracle.apps.inv.utilities.server.UtilFns;
   *   import oracle.apps.mwa.beans.*;
   *   import oracle.apps.mwa.container.MWALib;
   *   import oracle.apps.mwa.container.Session;
   *   import oracle.apps.mwa.eventmodel.*;
   *   import oracle.apps.mwa.container.FileLogger;
   *   import oracle.apps.fnd.flexj.FlexException;
   *   import oracle.apps.inv.lov.server.ItemLOV;
   *   import oracle.apps.inv.lov.server.LocatorKFF;
   *   import oracle.apps.inv.lov.server.SubinventoryLOV;
   *   import xxwc.oracle.apps.inv.lov.server.*;
   *
   *   PURPOSE:   Java Class for XXWCUBDFListener Page Layout.
   *    
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
        1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                          TMS Ticket 20150302-00152 - RF Receiving
**************************************************************************/
 package xxwc.oracle.apps.inv.labels.server;

import java.sql.*;
import java.util.Hashtable;
import java.util.Vector;
import oracle.apps.fnd.common.VersionInfo;
import oracle.apps.inv.lov.server.*;
import oracle.apps.inv.utilities.server.*;
import oracle.apps.mwa.beans.*;
import oracle.apps.mwa.container.FileLogger;
import oracle.apps.mwa.container.Session;
import oracle.apps.mwa.eventmodel.*;
import oracle.apps.mwa.presentation.telnet.TelnetSession;
import oracle.jdbc.OraclePreparedStatement;
import oracle.sql.NUMBER;
import oracle.apps.mwa.container.MWALib;
import oracle.apps.fnd.flexj.KeyFlexfield;
import oracle.apps.inv.rcv.server.RcptGenPage;
import oracle.apps.inv.rcv.server.RcvTxnPage;

import xxwc.oracle.apps.inv.lov.server.*;


/*************************************************************************
 *   NAME: XXWCUBDFListener implements MWAFieldListener
 *
 *   PURPOSE:   Main class for XXWCUBDFListener
 *
 *   REVISIONS:
 *   Ver        Date        Author                     Description
 *   ---------  ----------  ---------------         -------------------------
        1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                          TMS Ticket 20150302-00152 - RF Receiving
 **************************************************************************/


public class XXWCUBDFListener implements MWAFieldListener{
    XXWCUBDPage pg;
    Session ses;
    String dialogPageButtons[];
    Integer g_return = -1;
    String g_message = "";

    /*************************************************************************
     *   NAME: private static String gPackage = "xxwc.oracle.apps.inv.labels.server";
     *
     *   PURPOSE:   private method to default gPackage value for debugging
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00152 - RF Receiving
     **************************************************************************/

    private static String gPackage = "xxwc.oracle.apps.inv.labels.server";


    /*************************************************************************
     *   NAME: private static String gCallFrom = "XXWCUBDFListener";
     *
     *   PURPOSE:   private method to pass gCallFrom from methods to return class
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20150302-00152 - RF Receiving
     **************************************************************************/

    private static String gCallFrom = "XXWCUBDFListener";

    /*************************************************************************
     *   NAME: public XXWCUBDFListener()
     *
     *   PURPOSE:   public method XXWCUBDFListener
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                               TMS Ticket 20150302-00152 - RF Receiving
     **************************************************************************/

    public XXWCUBDFListener() {
        $init$();
    }

    /*************************************************************************
     *   NAME:  public void fieldEntered(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                       DefaultOnlyHandlerException
     *
     *   PURPOSE:   field entered for XXWCUBDFListener
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20150302-00152 - RF Receiving
     **************************************************************************/
    
    public void fieldEntered(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                       DefaultOnlyHandlerException {
        ses = mwaevent.getSession();
        pg = (XXWCUBDPage)ses.getCurrentPage();
        String s = UtilFns.fieldEnterSource(ses);
        /*Shelf Life*/
        if (s.equals("XXWC.SHELF_LIFE")) {
            return;
        }
        /*UBD*/
        if (s.equals("XXWC.UBD")) {
            enteredUBD(mwaevent);
            return;
        }
        /*Copies*/
        if (s.equals("XXWC.COPIES")) {
            return;
        }
        /*UBD Status*/
        if (s.equals("XXWC.UBD_STATUS")) {
            enteredUBDStatus(mwaevent);
            return;
        }
            
        
    }

    /*************************************************************************
     *   NAME:      public void fieldExited(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException
     *
     *   PURPOSE:   field exited for XXWCUBDFListener
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20150302-00152 - RF Receiving
     **************************************************************************/

    public void fieldExited(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException {
        String s = ((FieldBean)mwaevent.getSource()).getName();
        String vField = mwaevent.getAction();
        boolean flag = false;
        boolean flag1 = false;
        boolean flag3 = false;
        if (mwaevent.getAction().equals("MWA_SUBMIT")) {
            flag = true;
        } else if (mwaevent.getAction().equals("MWA_PREVIOUSFIELD")) {
            flag1 = true;
        } else if (mwaevent.getAction().equals("MWA_NEXTFIELD")) {
            flag3 = true;
        }
        /*Shelf Life*/
        if (s.equals("XXWC.SHELF_LIFE")) {
            return;
        }
        /*UBD*/
        if (s.equals("XXWC.UBD")) {
            exitedUBD(mwaevent, ses);
            return;
        }
        /*Shelf Life*/
        if (s.equals("XXWC.COPIES")) {
            exitedQty(mwaevent, ses);
            return;
        }
        /*UBD*/
        if (s.equals("XXWC.UBD_STATUS")) {
            exitedUBDStatus(mwaevent, ses);
            return;
        }
        /*Cancel*/
        if (flag && s.equals("INV.CANCEL")) {
            exitedCancel(mwaevent, ses);
            return;
        }
        /*SaveNext*/
        /*if (flag && s.equals("INV.SAVENEXT")) {
            execPrintTSLabel(mwaevent, ses);
            ses.setNextFieldName("XXWC.UBD");
            return;
        }
        */
        /*Done*/
        if (flag && s.equals("XXWC.UBD_PRINT")) {
            execPrintTSLabel(mwaevent, ses);
            exitedDone(mwaevent, ses);
            return;
        }
        else {
            return;
        }
    }

    /*************************************************************************
     *   NAME:      private void $init$()
     *
     *   PURPOSE:   $init$ sets the diaglogButtons button for user prompt messages to OK
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
         1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                             TMS Ticket 20150302-00152 - RF Receiving
     **************************************************************************/

    private void $init$() {
        dialogPageButtons = (new String[] { "OK" });
    }


        /*************************************************************************
        *   NAME: public void enteredUBD(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                         DefaultOnlyHandlerException
        *
        *   PURPOSE:  Entered Subinventory Field Listener Process to set the List of Values for the Subinventory Field
        *
        *   REVISIONS:
        *   Ver        Date        Author                     Description
        *   ---------  ----------  ---------------         -------------------------
                 1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                      TMS Ticket 20150302-00152 - RF Receiving
        **************************************************************************/

        public void enteredUBD(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                        DefaultOnlyHandlerException {


            ses.setStatusMessage("Date format is DD-MON-YYYY");
        
        }
    
    
    /*************************************************************************
    *   NAME: public void exitedUBD(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException
    *
    *   PURPOSE:  Exited Subinventory Field Listener Process to determine if the Locator Field should be displayed
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20150302-00152 - RF Receiving
    **************************************************************************/

    public void exitedUBD(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                DefaultOnlyHandlerException {
        String gMethod = "exitedUBD";
        String gCallPoint = "Start";
        FileLogger.getSystemLogger().trace(gPackage + " " + gCallFrom + " " + gCallPoint);
        pg.getmUBDDate().getValue().toUpperCase();
        /*
        try {
            if (pg.getmUBDDate().getValue().equals("") || pg.getmUBDDate().getValue().equals(null)) {
            }
            else {
                pg.getmUBDDate().getValue().toUpperCase();
            }
        }
        catch (Exception e) {
            UtilFns.error("Error in calling exitedItemStatus" + e);         
        }
        */
        try {
            gCallPoint = "validate UBD Date";
            FileLogger.getSystemLogger().trace(gPackage + " " + gCallFrom + " " + gCallPoint);
            CallableStatement cstmt = null;
            Connection con = ses.getConnection();
            cstmt = con.prepareCall("{call XXWC_LABEL_PRINTERS_PKG.VAL_UBD_DATE(?,?,?,?)}");
            cstmt.setString(1, pg.getmUBDDate().getValue());
            cstmt.registerOutParameter(2, Types.INTEGER); //x_return
            cstmt.registerOutParameter(3, Types.VARCHAR); //x_message
            cstmt.registerOutParameter(4, Types.VARCHAR); //x_date
            cstmt.execute();
            Integer x_return = cstmt.getInt(2);
            String  x_message = cstmt.getString(3);
            String  x_date = cstmt.getString(4);
            FileLogger.getSystemLogger().trace(gPackage + " " + gCallFrom + " " + gCallPoint + " x_return " + x_return );
            FileLogger.getSystemLogger().trace(gPackage + " " + gCallFrom + " " + gCallPoint + " x_message " + x_message);
            FileLogger.getSystemLogger().trace(gPackage + " " + gCallFrom + " " + gCallPoint + " x_date " + x_date);
            cstmt.close();
            boolean showSubError = true;
                if (x_return > 0) {
                    TelnetSession telnetsessionX = (TelnetSession)ses;
                    Integer k =
                        telnetsessionX.showPromptPage("Error!", x_message,
                                                      dialogPageButtons);
                    if (k == 0) {
                        pg.getmUBDDate().setValue(null);
                        ses.setNextFieldName("XXWC.UBD");
                    } else {
                        return;
                    }
                }
            pg.getmUBDDate().setValue(x_date);
            pg.getmUBDDate().getValue().toUpperCase();
        }
        catch (Exception e){
                UtilFns.error("Error in calling exitedUBD" + e);  
        }
        try {
            gCallPoint = "validate shelf life";
            FileLogger.getSystemLogger().trace(gPackage + " " + gCallFrom + " " + gCallPoint);
            CallableStatement cstmt = null;
            Connection con = ses.getConnection();
            cstmt = con.prepareCall("{call XXWC_LABEL_PRINTERS_PKG.VAL_SHELF_AND_UBD(?,?,?,?)}");
            cstmt.setString(1, pg.getmShelfLife().getValue());
            cstmt.setString(2, pg.getmUBDDate().getValue());
            cstmt.registerOutParameter(3, Types.INTEGER); //x_return
            cstmt.registerOutParameter(4, Types.VARCHAR); //x_message
            cstmt.execute();
            Integer x_return = cstmt.getInt(3);
            String  x_message = cstmt.getString(4);
            FileLogger.getSystemLogger().trace(gPackage + " " + gCallFrom + " " + gCallPoint + " x_return " + x_return);
            FileLogger.getSystemLogger().trace(gPackage + " " + gCallFrom + " " + gCallPoint + " x_message " + x_message);
            cstmt.close();
            boolean showSubError = true;
                if (x_return > 0) {
                    TelnetSession telnetsessionX = (TelnetSession)ses;
                    Integer k =
                        telnetsessionX.showPromptPage("Error!", x_message,
                                                      dialogPageButtons);
                    if (k == 0) {
                        return;
                    } else {
                        return;
                    }
                }
        }
        catch (Exception e){
            FileLogger.getSystemLogger().trace(gPackage + " " + gCallFrom + " " + gCallPoint + " exception " + e);
            UtilFns.error("Error in calling exitedUBD" + e);  
        }           
    }
    
    /*************************************************************************
    *   NAME: public void enteredPrinter(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                     DefaultOnlyHandlerException
    *
    *   PURPOSE:  Entered Subinventory Field Listener Process to set the List of Values for the Subinventory Field
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                  TMS Ticket 20150302-00152 - RF Receiving
    **************************************************************************/

    public void enteredUBDStatus(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException {

         
       try {
           XXWCUBDStatusFlagLOV ubdStatusLOV = pg.getmUBDStatusFlag();
           ubdStatusLOV.setValidateFromLOV(true);
           ubdStatusLOV.setlovStatement("XXWC_LABEL_PRINTERS_PKG.GET_UBD_STATUS_LOV");
           String paramType[] = { "C", "S" };
           String parameters[] =
           { " ", "xxwc.oracle.apps.inv.labels.server.XXWCUBDPage.XXWC.UBD_STATUS" };
           ubdStatusLOV.setInputParameterTypes(paramType);
           ubdStatusLOV.setInputParameters(parameters);
       } catch (Exception e) {
           UtilFns.error("Error in calling enteredPrinter" + e);
       }
    }

    /*************************************************************************
    *   NAME: public void exitedQty(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException
    *
    *   PURPOSE:  Exited Subinventory Field Listener Process to determine if the Locator Field should be displayed
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20150302-00152 - RF Receiving
    **************************************************************************/

    public void exitedQty(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                DefaultOnlyHandlerException {
        try {
            CallableStatement cstmt = null;
            Connection con = ses.getConnection();
            cstmt = con.prepareCall("{call XXWC_LABEL_PRINTERS_PKG.validate_lbl_qty_val(?,?,?)}");
            cstmt.setString(1, pg.getmCopies().getValue());
            cstmt.registerOutParameter(2, Types.INTEGER); //x_return
            cstmt.registerOutParameter(3, Types.VARCHAR); //x_message
            cstmt.execute();
            Integer x_return = cstmt.getInt(2);
            String  x_message = cstmt.getString(3);
            cstmt.close();
            boolean showSubError = true;
            if (x_return > 0) {
                TelnetSession telnetsessionX = (TelnetSession)ses;
                Integer k =
                    telnetsessionX.showPromptPage("Error!", x_message,
                                                  dialogPageButtons);
                if (k == 0) {
                    pg.getmCopies().setValue(null);
                    ses.setNextFieldName("XXWC.COPIES");
                } else {
                    return;
                }
            }
            
            Integer l_copies = Integer.parseInt(pg.getmCopies().getValue());
            Integer l_max_copies = 25;
            
            if (l_copies > l_max_copies){
                TelnetSession telnetsessionX = (TelnetSession)ses;
                Integer k =
                    telnetsessionX.showPromptPage("Error!", l_copies + " number of copies is greater than " + l_max_copies,
                                                  dialogPageButtons);
                if (k == 0) {
                    pg.getmCopies().setValue(null);
                    ses.setNextFieldName("XXWC.COPIES");
                } else {
                    return;
                }
            }
            //if (x_return == 0) {
            //    ses.setNextFieldName("XXWC.UBD_STATUS");
            //}
        
       } catch (Exception e) {
           UtilFns.error("Error in calling exitedSub" + e);
       }
    }

 

    /*************************************************************************
    *   NAME: public void exitedCancel(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                     DefaultOnlyHandlerException
    *
    *   PURPOSE:  Exited Close For Field Listener Process to proces the Close Button
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                               TMS Ticket 20150302-00152 - RF Receiving
    **************************************************************************/

    public void exitedCancel(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                   DefaultOnlyHandlerException {
        try {
            XXWCUBDPage _tmp = pg;
            String sessPreviousPage = "";
            String sessPreviousField = "";
            try {
                    sessPreviousPage = (String)ses.getObject("sessPreviousPage").toString();
                    sessPreviousField = (String)ses.getObject("sessPreviousField").toString();
            }
            catch (Exception e) {
                    sessPreviousPage = "";
                    sessPreviousField = "";
            }
            
            if (sessPreviousPage.equals(null) || sessPreviousPage.equals("")){
                ses.setStatusMessage(pg.WMS_TXN_CANCEL);
                ses.clearAllApplicationScopeObjects();
                ses.setStatusMessage(XXWCItemLabelPage.WMS_TXN_CANCEL);
                pg.getmCancel().setNextPageName("|END_OF_TRANSACTION|");
            }
            else {
                ses.setStatusMessage(pg.WMS_TXN_CANCEL);
                ses.clearAllApplicationScopeObjects();
                ses.setStatusMessage(XXWCItemLabelPage.WMS_TXN_CANCEL);
                pg.getmCancel().setNextPageName(sessPreviousPage);
                ses.setNextFieldName(sessPreviousField); 
                ses.putSessionObject("sessPreviousPage", "");
                ses.putSessionObject("sessPreviousField", "");
                ses.putSessionObject("sessXXWC.SHELF_LIFE", "");
                ses.putSessionObject("sessXXWC.COPIES", "");
                ses.putSessionObject("sessXXWC.UBD_STATUS", "");
            }
            
        } catch (Exception e) {
            UtilFns.error("Error in calling exitedCancel " + e);
        }

    }
    
    
    /*************************************************************************
    *   NAME:     public void exitedDone(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                                 InterruptedHandlerException,
                                                                                 DefaultOnlyHandlerException
    *
    *   PURPOSE:  Clears the values in the form
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
        1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00152 - RF Receiving

    **************************************************************************/
    
    protected void exitedDone(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                        DefaultOnlyHandlerException {
        try {
                XXWCUBDPage _tmp = pg;
                ses.setStatusMessage(pg.WMS_TXN_SUCCESS);
                //ses.clearAllApplicationScopeObjects();
                pg.getmShelfLife().setValue("");
                pg.getmUBDDate().setValue("");
                pg.getmCopies().setValue("");
                pg.getmUBDStatusFlag().setValue("");
                
                String sessPreviousPage = "";
                String sessPreviousField = "";
                try {
                        sessPreviousPage = (String)ses.getObject("sessPreviousPage").toString();
                        sessPreviousField = (String)ses.getObject("sessPreviousField").toString();
                }
                catch (Exception e) {
                        sessPreviousPage = "";
                        sessPreviousField = "";
                }
                
                if (!sessPreviousPage.equals(null) || !sessPreviousPage.equals("")){
                    pg.getmDone().setNextPageName(sessPreviousPage);
                    ses.setNextFieldName(sessPreviousField);
                    sessPreviousPage = "";
                    ses.putSessionObject("sessPreviousPage", "");
                    ses.putSessionObject("sessPreviousField", "");
                    ses.putSessionObject("sessXXWC.SHELF_LIFE", "");
                    ses.putSessionObject("sessXXWC.COPIES", "");
                    ses.putSessionObject("sessXXWC.UBD_STATUS", "");
                }
                else {
                    pg.getmDone().setNextPageName("|END_OF_TRANSACTION|");
                    
                }
            } 
        catch (Exception e) {
            UtilFns.error("Error in calling exitedDone " + e);
        }
    
    }
    

    /*************************************************************************
         *   NAME: public void execPrintTSLabel(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                      InterruptedHandlerException,
                                                                      DefaultOnlyHandlerException 
    
         *   PURPOSE: Retrieves page values and executes the print material label API, xxwc_mwa_routines_pkg.material_label
         *    
         *   In: MWAEvent mwaevent
         *    
         *    
         *   REVISIONS:
         *   Ver        Date        Author                     Description
         *   ---------  ----------  ---------------         -------------------------
              1.0       06-FEB-2015   Lee Spitzer             Initial Version - 
    **************************************************************************/
      
    public String execPrintTSLabel(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                      InterruptedHandlerException,
                                                                      DefaultOnlyHandlerException 
    { 
        String gMethod = "execPrintTSLabel";
        String gCallPoint = "Start";
        FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint );
        String p_date = "";
        String p_label_copies = "";
        String p_status_flag = "";
        Integer x_return = null;
        String x_message = "";
                
        gCallPoint = "Getting values from page";   
        
        p_date = pg.getmUBDDate().getValue();
        p_label_copies = pg.getmCopies().getValue();
        p_status_flag = pg.getmUBDStatusFlag().getmFlexValue();
        
        try {
            gCallPoint = "Calling XXWC_LABEL_PRINTERS_PKG.PROCESS_UBD_CCR";
            CallableStatement cstmt = null;
            Connection con = ses.getConnection();
            //                                                                     1 2 3 4 5         
            cstmt = con.prepareCall("{call XXWC_LABEL_PRINTERS_PKG.PROCESS_UBD_CCR(?,?,?,?,?)}");
            cstmt.setString(1, p_date); //p_date
            cstmt.setString(2, p_label_copies); //p_label_copies
            cstmt.setString(3, p_status_flag); //p_status_flag
            cstmt.registerOutParameter(4, Types.INTEGER); //x_result
            cstmt.registerOutParameter(5, Types.VARCHAR); //x_message 
            cstmt.execute();
            x_return = cstmt.getInt(4);
            x_message = cstmt.getString(5);
            cstmt.close();
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " x_return " + x_return);
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " x_message " + x_message);
            boolean showSubError = true;
                if (x_return == 0) {
                        ses.setStatusMessage(x_message);
                }
                if (x_return > 0) {
                    dialogPageButtons = new String[] { "OK" };
                    TelnetSession telnetsessionX = (TelnetSession)ses;
                    int k = telnetsessionX.showPromptPage("Error!", x_message, dialogPageButtons);
                    if (k == 0) {
                        ses.setRefreshScreen(true);
                     } 
                }
        }
        catch (Exception e){
            UtilFns.error("Error in calling default execPrintTSLabel " + e);
        }
    
        return x_message;
    }
        
    /*************************************************************************
    *   NAME: public void exitedUBDStatus(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException
    *
    *   PURPOSE:  Exited Subinventory Field Listener Process to determine if the Locator Field should be displayed
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20150302-00152 - RF Receiving
    **************************************************************************/

     public void exitedUBDStatus(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                 DefaultOnlyHandlerException {
         try {
             if (pg.getmUBDStatusFlag().getValue().equals("") || pg.getmUBDStatusFlag().getValue().equals(null)) {
                 return;
             }
             else {
                 pg.getmUBDStatusFlag().getValue().toUpperCase();
             }
              
         }
         catch (Exception e) {
             UtilFns.error("Error in calling exitedItemStatus" + e);         
         }
    }

}
