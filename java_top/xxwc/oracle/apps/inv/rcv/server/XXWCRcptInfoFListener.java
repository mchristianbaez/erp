/*************************************************************************
   *   $Header XXWCRcptInfoFListener.java $
   *   Module Name: XXWCRcptInfoFListener
   *   
   *   Package: package xxwc.oracle.apps.inv.rcv.server;
   *   
   *   Imports Classes:
   *   
   *   import java.sql.*;
   *   import oracle.apps.inv.utilities.server.UtilFns;
   *   import oracle.apps.mwa.beans.*;
   *   import oracle.apps.mwa.container.MWALib;
   *   import oracle.apps.mwa.container.Session;
   *   import oracle.apps.mwa.eventmodel.*;
   *   import oracle.apps.mwa.container.FileLogger;
   *   import oracle.apps.fnd.flexj.FlexException;
   *   import oracle.apps.inv.lov.server.ItemLOV;
   *   import oracle.apps.inv.lov.server.LocatorKFF;
   *   import oracle.apps.inv.lov.server.SubinventoryLOV;
   *   import xxwc.oracle.apps.inv.lov.server.*;
   *
   *   PURPOSE:   Java Class for XXWCRcptInfoFListener Page Layout.
   *    
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
        1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                          TMS Ticket 20150302-00152 RF - Receiving
**************************************************************************/
 package xxwc.oracle.apps.inv.rcv.server;

import java.sql.*;
import java.util.Hashtable;
import java.util.Vector;
import oracle.apps.fnd.common.VersionInfo;
import oracle.apps.inv.lov.server.*;
import oracle.apps.inv.utilities.server.*;
import oracle.apps.mwa.beans.*;
import oracle.apps.mwa.container.FileLogger;
import oracle.apps.mwa.container.Session;
import oracle.apps.mwa.eventmodel.*;
import oracle.apps.mwa.presentation.telnet.TelnetSession;
import oracle.jdbc.OraclePreparedStatement;
import oracle.sql.NUMBER;
import oracle.apps.mwa.container.MWALib;
import oracle.apps.fnd.flexj.KeyFlexfield;

import xxwc.oracle.apps.inv.labels.server.XXWCUBDFListener;
import xxwc.oracle.apps.inv.labels.server.XXWCUBDPage;
import xxwc.oracle.apps.inv.lov.server.*;


/*************************************************************************
 *   NAME: XXWCRcptInfoFListener implements MWAFieldListener
 *
 *   PURPOSE:   Main class for XXWCRcptInfoFListener
 *
 *   REVISIONS:
 *   Ver        Date        Author                     Description
 *   ---------  ----------  ---------------         -------------------------
        1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                          TMS Ticket 20150302-00152 RF - Receiving
 **************************************************************************/


public class XXWCRcptInfoFListener implements MWAFieldListener{
    XXWCRcptInfoPage pg;
    Session ses;
    String dialogPageButtons[];
    Integer g_return = -1;
    String g_message = "";

    /*************************************************************************
     *   NAME: private static String gPackage = "xxwc.oracle.apps.inv.rcv.server";
     *
     *   PURPOSE:   private method to default gPackage value for debugging
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00152 RF - Receiving
     **************************************************************************/

    private static String gPackage = "xxwc.oracle.apps.inv.rcv.server";


    /*************************************************************************
     *   NAME: private static String gCallFrom = "XXWCRcptInfoFListener";
     *
     *   PURPOSE:   private method to pass gCallFrom from methods to return class
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20150302-00152 RF - Receiving
     **************************************************************************/

    private static String gCallFrom = "XXWCRcptInfoFListener";

    /*************************************************************************
     *   NAME: public XXWCRcptInfoFListener()
     *
     *   PURPOSE:   public method XXWCRcptInfoFListener
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                               TMS Ticket 20150302-00152 RF - Receiving
     **************************************************************************/

    public XXWCRcptInfoFListener() {
        $init$();
    }

    /*************************************************************************
     *   NAME:  public void fieldEntered(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                       DefaultOnlyHandlerException
     *
     *   PURPOSE:   field entered for XXWCRcptInfoFListener
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20150302-00152 RF - Receiving
     **************************************************************************/
    
    public void fieldEntered(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                       DefaultOnlyHandlerException {
        ses = mwaevent.getSession();
        pg = (XXWCRcptInfoPage)ses.getCurrentPage();
        String s = UtilFns.fieldEnterSource(ses);
        /*PO*/
        if (s.equals("XXWC.ACCEPT_DEFER")) {
            enteredAcceptanceDefer(mwaevent);
            return;
        }
        if (s.equals("XXWC.CARRIER")) {
            enteredCarrier(mwaevent);
            return;
        }
    }

    /*************************************************************************
     *   NAME:      public void fieldExited(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException
     *
     *   PURPOSE:   field exited for XXWCRcptInfoFListener
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20150302-00152 RF - Receiving
     **************************************************************************/

    public void fieldExited(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException {
        String s = ((FieldBean)mwaevent.getSource()).getName();
        String vField = mwaevent.getAction();
        boolean flag = false;
        boolean flag1 = false;
        boolean flag3 = false;
        if (mwaevent.getAction().equals("MWA_SUBMIT")) {
            flag = true;
        } else if (mwaevent.getAction().equals("MWA_PREVIOUSFIELD")) {
            flag1 = true;
        } else if (mwaevent.getAction().equals("MWA_NEXTFIELD")) {
            flag3 = true;
        }
        /*Cancel*/
        if (s.equals("XXWC.BILL_OF_LADING")) {
            exitedBOL(mwaevent, ses);
            return;
        }
        /*Cancel*/
        if (flag && s.equals("XXWC.RECEIPT_CANCEL")) {
            execDeleteGroupId(mwaevent, ses);
            clearSessionValues(mwaevent, ses);
            exitedCancel(mwaevent, ses);
            return;
        }
        /*Done*/
        if (flag && s.equals("XXWC.RECEIPT_DONE")) {
            execUpdateRcvInterface(mwaevent, ses);
            if (g_return.equals(0)) {
                execRcvManager(mwaevent, ses);
            }
            exitedDone(mwaevent, ses);
            clearSessionValues(mwaevent, ses);
            return;
        }
        else {
            return;
        }
    }

    /*************************************************************************
     *   NAME:      private void $init$()
     *
     *   PURPOSE:   $init$ sets the diaglogButtons button for user prompt messages to OK
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
         1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                             TMS Ticket 20150302-00152 RF - Receiving
     **************************************************************************/

    private void $init$() {
        dialogPageButtons = (new String[] { "OK" });
    }


    /*************************************************************************
    *   NAME: public void enteredAcceptanceDefer(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                     DefaultOnlyHandlerException
    *
    *   PURPOSE:  Entered Item Field Listener Process to set the List of Values for the Subinventory Field
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                  TMS Ticket 20150302-00152 RF - Receiving
    **************************************************************************/

    public void enteredAcceptanceDefer(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException {

     try{     
           XXWCPOAcceptanceDeferReasonsLOV  poAcceptDeferLOV = pg.getmPOAcceptanceDeferReasonsFld();
           poAcceptDeferLOV.setValidateFromLOV(true);
           poAcceptDeferLOV.setlovStatement("XXWC_RCV_MOB_PKG.GET_PO_DEFER_ACCEPTANCE_LOV");
           String paramType[] = { "C", "S" };
           String parameters[] =
           { " ", "xxwc.oracle.apps.inv.rcv.server.XXWCRcptInfoPage.XXWC.ACCEPT_DEFER" };
           poAcceptDeferLOV.setInputParameterTypes(paramType);
           poAcceptDeferLOV.setInputParameters(parameters);
       } catch (Exception e) {
           UtilFns.error("Error in calling enteredCarrier " + e);
       }
    }

    

    /*************************************************************************
    *   NAME: public void enteredCarrier(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                     DefaultOnlyHandlerException
    *
    *   PURPOSE:  Entered Item Field Listener Process to set the List of Values for the Subinventory Field
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                  TMS Ticket 20150302-00152 RF - Receiving
    **************************************************************************/

    public void enteredCarrier(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException {

     try{     
           FreightCarrierLOV carrierLOV = pg.getmCarrierFld();
           carrierLOV.setValidateFromLOV(true);
           carrierLOV.setlovStatement("INV_UI_RCV_LOVS.get_freight_carrier_LOV");
           String paramType[] = { "C", "N", "S" };
           String parameters[] =
           { " ", "ORGID", "xxwc.oracle.apps.inv.rcv.server.XXWCRcptInfoPage.XXWC.CARRIER" };
           carrierLOV.setInputParameterTypes(paramType);
           carrierLOV.setInputParameters(parameters);
       } catch (Exception e) {
           UtilFns.error("Error in calling enteredCarrier " + e);
       }
    }
  

    /*************************************************************************
    *   NAME: public void exitedBOL(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                     DefaultOnlyHandlerException
    *
    *   PURPOSE:  Exited Close For Field Listener Process to proces the Close Button
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                               TMS Ticket 20150302-00152 RF - Receiving
    **************************************************************************/

    public void exitedBOL(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                   DefaultOnlyHandlerException {
        try {
            if(!pg.getmBillOfLadingFld().getValue().equals(null) | !pg.getmBillOfLadingFld().getValue().equals("")) {
                pg.getmDone().setHidden(false);
            }
            else {
                pg.getmDone().setHidden(true);
            }
        } catch (Exception e) {
            UtilFns.error("Error in calling exitedCancel " + e);
        }

    }


    /*************************************************************************
    *   NAME: public void exitedCancel(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                     DefaultOnlyHandlerException
    *
    *   PURPOSE:  Exited Close For Field Listener Process to proces the Close Button
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                               TMS Ticket 20150302-00152 RF - Receiving
    **************************************************************************/

    public void exitedCancel(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                   DefaultOnlyHandlerException {
        try {
            CallableStatement cstmt = null;
            Connection con = ses.getConnection();
            cstmt = con.prepareCall("{call XXWC_RCV_MOB_PKG.EXECUTE_ROLLBACK}");
            cstmt.execute();
            cstmt.close();
            
            XXWCRcptInfoPage _tmp = pg;
            ses.setStatusMessage(pg.WMS_TXN_CANCEL);
            ses.clearAllApplicationScopeObjects();
            ses.setStatusMessage(XXWCRcptInfoPage.WMS_TXN_CANCEL);
            pg.getmCancel().setNextPageName("|END_OF_TRANSACTION|");
        } catch (Exception e) {
            UtilFns.error("Error in calling exitedCancel " + e);
        }

    }
    
    
    /*************************************************************************
    *   NAME:     public void exitedDone(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                                 InterruptedHandlerException,
                                                                                 DefaultOnlyHandlerException
    *
    *   PURPOSE:  Clears the values in the form
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
        1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00152 RF - Receiving

    **************************************************************************/
    
    protected void exitedDone(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                        DefaultOnlyHandlerException {
        try {
                XXWCRcptInfoPage _tmp = pg;
                ses.setStatusMessage(pg.WMS_TXN_SUCCESS);
                ses.clearAllApplicationScopeObjects();
                pg.getmDone().setNextPageName("|END_OF_TRANSACTION|");
            } 
        catch (Exception e) {
            UtilFns.error("Error in calling exitedDone " + e);
        }
    
    }

  
    /*************************************************************************
    *   NAME: public void execUpdateRcvInterface(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException
    *
    *   PURPOSE:  Exited Subinventory Field Listener Process to determine if the Locator Field should be displayed
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20150302-00152 RF - Receiving
    **************************************************************************/

    public void execUpdateRcvInterface(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                    DefaultOnlyHandlerException {
        
        String gMethod = "execUpdateRcvInterface";
        String gCallPoint = "Start";
        String p_group_id;
        String p_organization_id;
        String p_transaction_type;
        String p_doctype;
        String p_freight_carrier_code;
        String p_packing_slip;
        String p_bill_of_lading;
        String p_attribute2;
        String p_user_id;
        Integer x_return;
        String x_message;
        
        p_group_id = pg.getmGroupId().getValue();
        p_organization_id = (String)ses.getObject("ORGID");
        p_transaction_type = pg.getmTransactionType().getValue();
        p_doctype = pg.getmDocType().getValue();
        p_user_id = (String)ses.getObject("USERID");
        p_freight_carrier_code = pg.getmCarrierFld().getCarrier();
        p_packing_slip = pg.getmPackSlipFld().getValue();
        p_bill_of_lading = pg.getmBillOfLadingFld().getValue();
        p_attribute2 = pg.getmPOAcceptanceDeferReasonsFld().getmLookupCode();
        x_return = -1;
        x_message = "";
        
          try {
                CallableStatement cstmt = null;
                Connection con = ses.getConnection();
                //                                                                   1 2 3 4 5 6 7 8 9 0 1
                cstmt = con.prepareCall("{call XXWC_RCV_MOB_PKG.UPDATE_RCV_INTERFACE(?,?,?,?,?,?,?,?,?,?,?)}");
                cstmt.setString(1, p_group_id);
                cstmt.setString(2, p_organization_id);
                cstmt.setString(3, p_transaction_type);
                cstmt.setString(4, p_doctype);
                cstmt.setString(5, p_freight_carrier_code);
                cstmt.setString(6, p_packing_slip);
                cstmt.setString(7, p_bill_of_lading);
                cstmt.setString(8, p_user_id);
                cstmt.setString(9, p_attribute2);
                cstmt.registerOutParameter(10, Types.INTEGER); //x_return
                cstmt.registerOutParameter(11, Types.VARCHAR); //x_message
                cstmt.execute();
                x_return = cstmt.getInt(10);
                x_message = cstmt.getString(11);
                cstmt.close();
                boolean showSubError = true;
                if (x_return > 0) {
                    TelnetSession telnetsessionX = (TelnetSession)ses;
                    Integer k =
                        telnetsessionX.showPromptPage("Error!", x_message,
                                                      dialogPageButtons);
                    if (k == 0) {
                        ses.setNextFieldName("XXWC.RECEIPT_DONE");
                    } else {
                        return;
                    }
                }
                if (x_return == 0) {
                    
                    ses.setNextFieldName("XXWC.RECEIPT_DONE");
                }
            }
            catch (Exception e) {
                UtilFns.error("Error in validating execUpdateRcvInterface" + e);
            }
            
          g_return = x_return;
          g_message = x_message;
        }
        
        
    /*************************************************************************
    *   NAME: public void execRcvManager(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException
    *
    *   PURPOSE:  Exited Subinventory Field Listener Process to determine if the Locator Field should be displayed
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20150302-00152 RF - Receiving
    **************************************************************************/

    public void execRcvManager(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                    DefaultOnlyHandlerException {
        
        String gMethod = "execRcvManager";
        String gCallPoint = "Start";
        String p_group_id;
        
        Integer x_return;
        String x_message;
        
        p_group_id = pg.getmGroupId().getValue();
        
        x_return = -1;
        x_message = "";
        
          try {
                CallableStatement cstmt = null;
                Connection con = ses.getConnection();
                //                                                              1 2 3
                cstmt = con.prepareCall("{call XXWC_RCV_MOB_PKG.PROCESS_RCV_MGR(?,?,?)}");
                cstmt.setString(1, p_group_id);
                cstmt.registerOutParameter(2, Types.INTEGER); //x_return
                cstmt.registerOutParameter(3, Types.VARCHAR); //x_message
                cstmt.execute();
                x_return = cstmt.getInt(2);
                x_message = cstmt.getString(3);
                cstmt.close();
                boolean showSubError = true;
                if (x_return > 0) {
                    TelnetSession telnetsessionX = (TelnetSession)ses;
                    Integer k =
                        telnetsessionX.showPromptPage("Error!", x_message,
                                                      dialogPageButtons);
                    if (k == 0) {
                        ses.setNextFieldName("XXWC.RECEIPT_DONE");
                    } else {
                        return;
                    }
                }
                if (x_return == 0) {
                    ses.setNextFieldName("XXWC.RECEIPT_DONE");
                }
            }
            catch (Exception e) {
                UtilFns.error("Error in validating execRcvManager" + e);
            }
            
          g_return = x_return;
          g_message = x_message;
        }
                
        /*************************************************************************
        *   NAME: public void clearSessionValues(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                        DefaultOnlyHandlerException
        *
        *   PURPOSE:  Exited Subinventory Field Listener Process to determine if the Locator Field should be displayed
        *
        *   REVISIONS:
        *   Ver        Date        Author                     Description
        *   ---------  ----------  ---------------         -------------------------
                1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                     TMS Ticket 20150302-00152 RF - Receiving
        **************************************************************************/

        public void clearSessionValues(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                        DefaultOnlyHandlerException {
            ses.putSessionObject("sessXXWC.CARRIER", "");
            ses.putSessionObject("sessXXWC.GROUP_ID", "");
            ses.putSessionObject("sessXXWC.DOC_TYPE","");
            ses.putSessionObject("sessXXWC.TRANSACTION_TYPE","");
            ses.putSessionObject("sessXXWC.ACCEPT_DEFER",""); 
            ses.putSessionObject("sessPreviousPage", "");
            ses.putSessionObject("sessPreviousField", "");
            ses.putSessionObject("sessXXWC.SHELF_LIFE", "");
            ses.putSessionObject("sessXXWC.COPIES","");
            ses.putSessionObject("sessXXWC.UBD_STATUS", "");
            ses.putSessionObject("sessPOHeaderId", "");  
            ses.putSessionObject("sessReqHeaderId", "");
            ses.putSessionObject("sessShipmentHeaderId", "");
            ses.putSessionObject("sessRMAHeaderId", "");
            ses.putSessionObject("sessShipFromOrgId", "");
    }     
        

    /*************************************************************************
    *   NAME:   public execDeleteGroupId(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                                             InterruptedHandlerException,
                                                                                             DefaultOnlyHandlerException 
    
    *   PURPOSE: Retrieves page values and executes the print material label API, xxwc_mwa_routines_pkg.material_label
    *    
    *   In: MWAEvent mwaevent
    *    
    *    
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
           1.0       06-FEB-2015   Lee Spitzer             Initial Version - 
    **************************************************************************/
      
    public void execDeleteGroupId(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                      InterruptedHandlerException,
                                                                      DefaultOnlyHandlerException 
    { 
        String gMethod = "execDeleteGroupId";
        String gCallPoint = "Start";
        FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint );
        
        String p_group_id = "";
        Integer x_return = null;
        String x_message = "";
                    
        gCallPoint = "Getting values from page";   
            
        p_group_id = pg.getmGroupId().getValue();
            
        FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " p_group_id " + p_group_id);
            
            
            try {
                gCallPoint = "Calling XXWC_RCV_MOB_PKG.DELETE_RCV_IFACE";
                CallableStatement cstmt = null;
                Connection con = ses.getConnection();
                //                                                               1 2 3
                cstmt = con.prepareCall("{call XXWC_RCV_MOB_PKG.DELETE_RCV_IFACE(?,?,?)}");
                cstmt.setString(1, p_group_id); //p_group_id
                cstmt.registerOutParameter(2, Types.INTEGER); //x_result
                cstmt.registerOutParameter(3, Types.VARCHAR); //x_message 
                cstmt.execute();
                x_return = cstmt.getInt(2);
                x_message = cstmt.getString(3);
                cstmt.close();
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " x_return " + x_return);
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " x_message " + x_message);
                boolean showSubError = true;
                    if (x_return > 0) {
                        dialogPageButtons = new String[] { "OK" };
                        TelnetSession telnetsessionX = (TelnetSession)ses;
                        int k = telnetsessionX.showPromptPage("Error!", x_message, dialogPageButtons);
                        if (k == 0) {
                         } 
                    }
            }
            catch (Exception e){
                UtilFns.error("Error in calling default execDeleteGroupId " + e);
            }
            
    }
          
}
