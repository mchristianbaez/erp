/*************************************************************************
   *   $Header XXWCRcptInfoPage.java $
   *   Module Name: XXWCRcvGenPage
   *   
   *   Package: package xxwc.oracle.apps.inv.rcv.server;
   *   
   *   Imports Classes:
   *   
   *   import java.sql.*;
   *   import oracle.apps.inv.utilities.server.UtilFns;
   *   import oracle.apps.mwa.beans.*;
   *   import oracle.apps.mwa.container.MWALib;
   *   import oracle.apps.mwa.container.Session;
   *   import oracle.apps.mwa.eventmodel.*;
   *   import oracle.apps.mwa.container.FileLogger;
   *   import oracle.apps.fnd.flexj.FlexException;
   *   import oracle.apps.inv.lov.server.ItemLOV;
   *   import oracle.apps.inv.lov.server.LocatorKFF;
   *   import oracle.apps.inv.lov.server.SubinventoryLOV;
   *   import xxwc.oracle.apps.inv.lov.server.*;
   *
   *   PURPOSE:   Java Class for XXWCRcvGenPage Page Layout.
   *    
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
        1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                          TMS Ticket 20150302-00152 RF - Receiving
        1.1       03-MAY-2016   Lee Spitzer             TMS Ticket 20160502-00389 Receipt Head Menu to Cancel

**************************************************************************/

package xxwc.oracle.apps.inv.rcv.server;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import oracle.apps.mwa.beans.*;
import oracle.apps.mwa.container.MWALib;
import oracle.apps.mwa.container.Session;
import oracle.apps.mwa.eventmodel.*;
import oracle.apps.mwa.container.FileLogger;
import oracle.apps.fnd.flexj.FlexException;
import oracle.apps.fnd.flexj.KeyFlexfield;
import oracle.apps.inv.lov.server.FreightCarrierLOV;
import oracle.apps.inv.utilities.server.*;
import xxwc.oracle.apps.inv.lov.server.*;

/*************************************************************************
 *   NAME: XXWCRcvGenPage extends PageBean
 *
 *   PURPOSE:   Main class for XXWCRcvGenPage
 *
 *   REVISIONS:
 *   Ver        Date        Author                     Description
 *   ---------  ----------  ---------------         -------------------------
        1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                          TMS Ticket 20150302-00152 RF - Receiving
 **************************************************************************/


public class XXWCRcptInfoPage extends PageBean{
    /*Define objects in the class*/
    public static String WMS_TXN_SUCCESS = "Txn Successful";
    public static String WMS_TXN_ERROR = "Txn Failed. Please check log";
    public static String WMS_TXN_CANCEL = "Txn Cancelled";
    public static String WMS_LOT_INS_FAIL = "Lot Insert Failed";
    public static String WMS_SER_INS_FAIL = "Serial Insert Failed";
    public static String savenextPrompt = "";
    public static String donePrompt = "";
    public static String cancelPrompt = "";
    private boolean areThePromptsSet;
    //protected XXWCPOLOV mPOFld;
    protected TextFieldBean mGroupId;
    protected TextFieldBean mDocType;
    protected TextFieldBean mTransactionType;
    protected XXWCPOAcceptanceDeferReasonsLOV mPOAcceptanceDeferReasonsFld;
    protected FreightCarrierLOV mCarrierFld;
    protected TextFieldBean mPackSlipFld;
    protected TextFieldBean mBillOfLadingFld;
    protected ButtonFieldBean mCancel;
    //protected ButtonFieldBean mSaveNext;
    protected ButtonFieldBean mDone;
    protected String m_userid;


    /*************************************************************************
     *   NAME: private static String gPackage = "xxwc.oracle.apps.inv.rcv.server";
     *
     *   PURPOSE:   private method to default gPackage value for debugging
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00152 RF - Receiving
     **************************************************************************/

    private static String gPackage = "xxwc.oracle.apps.inv.rcv.server";


    /*************************************************************************
     *   NAME: private static String gCallFrom = "XXWCRcvGenPage";
     *
     *   PURPOSE:   private method to pass gCallFrom from methods to return class name CustomListener
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00152 RF - Receiving
     **************************************************************************/

    private static String gCallFrom = "XXWCRcvGenPage";



    /*************************************************************************
     *   NAME: public XXWCRcvGenPage(Session session)
     *
     *   PURPOSE:   Inherits session from XXWCPrimaryBinAssignmentPage
     *
     *   Parameters: Session session
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                               TMS Ticket 20150302-00152 RF - Receiving
     **************************************************************************/

    public XXWCRcptInfoPage(Session session) {
        initLayout(session);
    }

    
    /*************************************************************************
     *   NAME: private void initLayout(Session session)
     *
     *   PURPOSE:   Sets the initial layout of the page
     *
     *   Parameters: Session session
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
          1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                             TMS Ticket 20150302-00152 RF - Receiving
     **************************************************************************/

    private void initLayout(Session session) {
        String gMethod = "setPrompts";
        String gCallPoint = "Start";
        m_userid = "";
        WMS_TXN_SUCCESS = UtilFns.getMessage(session, "WMS", "WMS_TXN_SUCCESS");
        WMS_TXN_ERROR = UtilFns.getMessage(session, "WMS", "WMS_TXN_ERROR");
        WMS_TXN_CANCEL = UtilFns.getMessage(session, "WMS", "WMS_TXN_CANCEL");
        WMS_LOT_INS_FAIL = UtilFns.getMessage(session, "WMS", "WMS_LOT_INS_FAIL");
        WMS_SER_INS_FAIL = UtilFns.getMessage(session, "WMS", "WMS_SER_INS_FAIL");
        /*Doc Type*/
        String l_doc_type;
        String l_transaction_type;
               
        //mPOFld = new XXWCPOLOV ();
        //mPOFld.setName("XXWC.PO");
        //mPOFld.setEditable(false);
        
        mGroupId = new TextFieldBean();
        mGroupId.setName("XXWC.GROUP_ID");
        mGroupId.setEditable(false);
        mGroupId.setHidden(true);
        
        mDocType = new TextFieldBean();
        mDocType.setName("XXWC.DOC_TYPE");
        mDocType.setEditable(false);
        mDocType.setHidden(true);

        mTransactionType = new TextFieldBean();
        mTransactionType.setName("XXWC.TRANSACTION_TYPE");
        mTransactionType.setEditable(false);
        mTransactionType.setHidden(true);
        
        
        mPOAcceptanceDeferReasonsFld = new XXWCPOAcceptanceDeferReasonsLOV();
        mPOAcceptanceDeferReasonsFld.setName("XXWC.ACCEPT_DEFER");
        mPOAcceptanceDeferReasonsFld.setRequired(true);
         
        mCarrierFld = new FreightCarrierLOV ();
        mCarrierFld.setName("XXWC.CARRIER");
        mCarrierFld.setRequired(true);
        
        mPackSlipFld = new TextFieldBean();
        mPackSlipFld.setName("XXWC.PACK_SLIP");
        mPackSlipFld.setRequired(true);
        
        mBillOfLadingFld = new TextFieldBean();
        mBillOfLadingFld.setName("XXWC.BILL_OF_LADING");
        mBillOfLadingFld.setRequired(true);
        
        /*Save Properties*/
        //mSaveNext = new ButtonFieldBean();
        //mSaveNext.setName("INV.SAVENEXT");
        //mSaveNext.setPrompt("Save/Next");
        //mSaveNext.setEnableAcceleratorKey(true);
        //mSaveNext.setHidden(true);
        /*Save Properties*/
        mDone = new ButtonFieldBean();
        mDone.setName("XXWC.RECEIPT_DONE");
        mDone.setPrompt("Done");
        mDone.setEnableAcceleratorKey(true);
        mDone.setHidden(true);
        /*Cancel Properties*/
        mCancel = new ButtonFieldBean();
        mCancel.setName("XXWC.RECEIPT_CANCEL");
        mCancel.setPrompt("Cancel");
        mCancel.setEnableAcceleratorKey(true);
        mCancel.setHidden(false);
        /*set the properties for each to the addFieldBean*/
        //addFieldBean(mPOFld);
        addFieldBean(mGroupId);
        addFieldBean(mDocType);
        addFieldBean(mTransactionType);
        addFieldBean(mPOAcceptanceDeferReasonsFld);
        addFieldBean(mCarrierFld);
        addFieldBean(mPackSlipFld);
        addFieldBean(mBillOfLadingFld);
        //addFieldBean(mSaveNext);
        addFieldBean(mDone);
        addFieldBean(mCancel);
        XXWCRcptInfoFListener fieldListener = new XXWCRcptInfoFListener();
        /*Add Listener to each property*/
        //mPOFld.addListener(fieldListener);
        mGroupId.addListener(fieldListener);
        mPOAcceptanceDeferReasonsFld.addListener(fieldListener);
        mCarrierFld.addListener(fieldListener);
        mPackSlipFld.addListener(fieldListener);
        mBillOfLadingFld.addListener(fieldListener);
        mDocType.addListener(fieldListener);
        mTransactionType.addListener(fieldListener);
        //mSaveNext.addListener(fieldListener);
        mDone.addListener(fieldListener);
        mCancel.addListener(fieldListener);
        try {
            setPrompts(session);
        } catch (Exception exception) {
            session.setStatusMessage(UtilFns.getMessage(session, "WMS", "WMS_PROMPT_NOTSET"));
        }
    }
    
    /*************************************************************************
     *   NAME: private void initPrompts(Session session) throws SQLException
     *
     *   PURPOSE:   Sets the initial prompts of the session
     *
     *   Parameters: Session session
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
          1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                             TMS Ticket 20150302-00152 RF - Receiving
     **************************************************************************/

    private void initPrompts(Session session) throws SQLException {
        try {
            String gMethod = "initPrompts";
            String gCallPoint = "Start";
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
            String s = (String)session.getObject("ORGCODE");
            setPrompt((new StringBuilder()).append("XXWC Receipts (").append(s).append(")").toString());
            //mPOFld.setPrompt("PO Num");
            mGroupId.setPrompt("GROUP_ID");
            mDocType.setPrompt("DOCTYPE");
            mTransactionType.setPrompt("TRANSACTION");
            mPOAcceptanceDeferReasonsFld.setPrompt("Accpt Overide");
            mCarrierFld.setPrompt("Carrier");
            mPackSlipFld.setPrompt("Pack Slip");
            mBillOfLadingFld.setPrompt("BOL");
            savenextPrompt = MWALib.getAKPrompt(session, "oracle.apps.inv.utilities.InvResourceTable", "INV_SAVE_NEXT_PROMPT");
            donePrompt = MWALib.getAKPrompt(session, "oracle.apps.inv.utilities.InvResourceTable", "INV_DONE_PROMPT");
            cancelPrompt = MWALib.getAKPrompt(session, "oracle.apps.inv.utilities.InvResourceTable", "INV_CANCEL_PROMPT");
            areThePromptsSet = true;
            gCallPoint = "End";
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
        } catch (SQLException sqlexception) {
            session.setStatusMessage(UtilFns.getMessage(session, "WMS", "WMS_TXN_ERROR"));
        }
    }

    /*************************************************************************
     *   NAME: private void setPrompts(Session session)
     *
     *   PURPOSE:   Sets the set prompts of the session
     *
     *   Parameters: Session session
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
          1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                             TMS Ticket 20150302-00152 RF - Receiving
          1.1       03-MAY-2016   Lee Spitzer             TMS Ticket 20160502-00389 Receipt Head Menu to Cancel
     **************************************************************************/

    private void setPrompts(Session session) {
            String gMethod = "setPrompts";
            String gCallPoint = "Start";
            if (!areThePromptsSet) {
                try {
                    initPrompts(session);
                } catch (SQLException sqlexception) {
                }
            }
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
            String s = (String)session.getObject("ORGCODE");
            setPrompt((new StringBuilder()).append("XXWC Receipt Header (").append(s).append(")").toString());

            //mSaveNext.setPrompt(savenextPrompt);
            mDone.setPrompt(donePrompt);
            mCancel.setPrompt(cancelPrompt);
            //mSaveNext.retrieveAttributes("INVE_SAVE_NEXT_PROMPT");
            mDone.retrieveAttributes("INV_DONE_PROMPT");
            mCancel.retrieveAttributes("INV_CANCEL_PROMPT");
            //mCancel.setPrompt("Menu"); // Removed 05-03-2016 20160502-00389 Receipt Head Menu to Cancel
            gCallPoint = "End Prompts";
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
            
            setFieldValues(session);
    }


    /*************************************************************************
    *   NAME: public XXWCPOLOV getmPOFld(
    *
    *   PURPOSE:   return the value of getmPrinterFld
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
           1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                               TMS Ticket 20150302-00152 RF - Receiving
    **************************************************************************/

    //public XXWCPOLOV getmPOFld() {
    //   return mPOFld;
    //}


    /*************************************************************************
    *   NAME: public TextFieldBean getmVendorName(
    *
    *   PURPOSE:   return the value of getmPrinterFld
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
           1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                               TMS Ticket 20150302-00152 RF - Receiving
    **************************************************************************/

    public FreightCarrierLOV getmCarrierFld() {
       return mCarrierFld;
    }

    /*************************************************************************
    *   NAME: public TextFieldBean getmNoteToReceiver(
    *
    *   PURPOSE:   return the value of getmPrinterFld
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
           1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                               TMS Ticket 20150302-00152 RF - Receiving
    **************************************************************************/

    public TextFieldBean getmPackSlipFld() {
       return mPackSlipFld;
    }

     /*************************************************************************
     *   NAME: public XXWCItemLOV getmItemFld(
     *
     *   PURPOSE:   return the value of getmPrinterFld
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00152 RF - Receiving
     **************************************************************************/

    public TextFieldBean getmBillOfLadingFld() {
        return mBillOfLadingFld;
    }
     
    public TextFieldBean getmDocType() {
        return mDocType;
    }
    
    public TextFieldBean getmTransactionType() {
        return mTransactionType;
    }


    public XXWCPOAcceptanceDeferReasonsLOV  getmPOAcceptanceDeferReasonsFld() {
        return mPOAcceptanceDeferReasonsFld;
    }


    public TextFieldBean getmGroupId() {
        return mGroupId;
    }

    /*************************************************************************
     *   NAME: public ButtonFieldBean getmCancel
     *
     *   PURPOSE:   return the value of mCancel
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00152 RF - Receiving
     **************************************************************************/

    public ButtonFieldBean getmCancel() {
        return mCancel;
    }


    /*************************************************************************
     *   NAME: public ButtonFieldBean getmDone()
     *
     *   PURPOSE:   return the value of mDone
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00152 RF - Receiving
     **************************************************************************/

    public ButtonFieldBean getmDone() {
        return mDone;
    }


    /*************************************************************************
     *   NAME: public String getUserId()
     *
     *   PURPOSE:   return the value of m_userid
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00152 RF - Receiving
     **************************************************************************/

    public String getUserId() {
        return m_userid;
    }

    /*************************************************************************
     *   NAME: void pageEntered(MWAEvent e) throws AbortHandlerException, InterruptedHandlerException,
                                               DefaultOnlyHandlerException {
     *
     *   PURPOSE:   pageEntered method
     *
     *   Parameters: MWAEvent mwaevent
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00152 RF - Receiving
     **************************************************************************/

    public void pageEntered(MWAEvent e) throws AbortHandlerException, InterruptedHandlerException,
                                               DefaultOnlyHandlerException {
        Session session = e.getSession();
    }

    /*************************************************************************
     *   NAME: void pageExited(MWAEvent e) throws AbortHandlerException, InterruptedHandlerException,
                                               DefaultOnlyHandlerException {
     *
     *   PURPOSE:   pageExited method
     *
     *
     *   Parameters: MWAEvent   mwaevent
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00152 RF - Receiving

     **************************************************************************/

    public void pageExited(MWAEvent e) throws AbortHandlerException, InterruptedHandlerException,
                                              DefaultOnlyHandlerException {
        Session session = e.getSession();
    }
    
    
    
    /*************************************************************************
     *   NAME: void setFieldValues((Session session) throws AbortHandlerException, InterruptedHandlerException,
                                               DefaultOnlyHandlerException {
     *
     *   PURPOSE:   pageExited method
     *
     *
     *   Parameters: MWAEvent   mwaevent
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00152 RF - Receiving

     **************************************************************************/

    
    public void setFieldValues(Session session){
        String gMethod = "sesFieldValues";
        String gCallPoint = "Start";
        session.setRefreshScreen(true);
        String l_po_defer_acceptance = "";
        try{
            mCarrierFld.setValue(session.getObject("sessXXWC.CARRIER").toString());
            mGroupId.setValue(session.getObject("sessXXWC.GROUP_ID").toString());
            mDocType.setValue(session.getObject("sessXXWC.DOC_TYPE").toString());
            mTransactionType.setValue(session.getObject("sessXXWC.TRANSACTION_TYPE").toString());
        }
        catch (Exception e){
            mCarrierFld.setValue("");
            mGroupId.setValue("");
            mDocType.setValue("");
            mTransactionType.setValue("");
        }
        try {
            l_po_defer_acceptance = session.getObject("sessXXWC.ACCEPT_DEFER").toString();    
        }
        catch (Exception e) {
            l_po_defer_acceptance = "";
        }
        if (l_po_defer_acceptance.equals("")) {
            mPOAcceptanceDeferReasonsFld.setHidden(true);
            mPOAcceptanceDeferReasonsFld.setRequired(false);
            mPOAcceptanceDeferReasonsFld.setEditable(false);
            mPOAcceptanceDeferReasonsFld.setValue("");
            mPOAcceptanceDeferReasonsFld.setmLookupCode("");
        }
        else {
            mPOAcceptanceDeferReasonsFld.setHidden(true);
            mPOAcceptanceDeferReasonsFld.setRequired(true);
            mPOAcceptanceDeferReasonsFld.setEditable(false);
            mPOAcceptanceDeferReasonsFld.setValue(l_po_defer_acceptance);
            mPOAcceptanceDeferReasonsFld.setmLookupCode(l_po_defer_acceptance);
            
        }
        
        
        
    }
   
}
