/*************************************************************************
   *   $Header XXWCAccountAliasFunction.java $
   *   Module Name: XXWCAccountAliasFunction
   *   
   *   Package xxwc.oracle.apps.inv.invtxn.server;
   *   
   *   
   *   Imports Classes:
   *   
   *   import oracle.apps.fnd.common.VersionInfo;
   *   import oracle.apps.inv.utilities.server.OrgFunction;
   *   import oracle.apps.inv.utilities.server.UtilFns;
   *   import oracle.apps.mwa.eventmodel.*;
   *   
   *   PURPOSE:   Java Class for XXWC Cycle Count Entry Function
   *    
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
      1.0       20-JUN-2015   Lee Spitzer             Initial Version - 
                                                          TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
 **************************************************************************/

 package xxwc.oracle.apps.inv.invtxn.server;

 import oracle.apps.fnd.common.VersionInfo;
 import oracle.apps.inv.utilities.server.OrgFunction;
 import oracle.apps.inv.utilities.server.UtilFns;
 import oracle.apps.mwa.eventmodel.*;

 /*************************************************************************
 *   NAME: public class XXWCAccountAliasFunction extends OrgFunction implements MWAAppListener
 *
 *   PURPOSE:   Main class for XXWCAccountAliasFunction
 *
 *   REVISIONS:
 *   Ver        Date        Author                     Description
 *   ---------  ----------  ---------------         -------------------------
      1.0       20-JUN-2015   Lee Spitzer             Initial Version - 
                                                      TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
  **************************************************************************/


 public class XXWCAccountAliasFunction extends OrgFunction implements MWAAppListener {

  /*************************************************************************
   *   NAME: public static final String RCS_ID
   *
   *   PURPOSE:   Returns Header File Name
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
        1.0       20-JUN-2015   Lee Spitzer             Initial Version - 
                                                        TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
    **************************************************************************/

  public static final String RCS_ID =
      "$Header: XXWCAccountAliasFunction.java 120.0 2015/20/06 05:18:34 appldev noship $";

  /*************************************************************************
   *   NAME: public static final boolean RCS_ID_RECORDED
   *
   *   PURPOSE:   Returns Header File Name
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
        1.0       20-JUN-2015   Lee Spitzer             Initial Version - 
                                                        TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
    **************************************************************************/


  public static final boolean RCS_ID_RECORDED =
      VersionInfo.recordClassVersion("$Header: XXWCAccountAliasFunction.java 120.0 2014/11/06 05:18:34 appldev noship $",
                                     "xxwc.oracle.apps.inv.invtxn.server");


  /*************************************************************************
   *   NAME: public XXWCAccountAliasFunction()
   *
   *   PURPOSE:   Menu function that points the mobile to the mobile page layout
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
        1.0       20-JUN-2015   Lee Spitzer             Initial Version - 
                                                        TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
    **************************************************************************/


  public XXWCAccountAliasFunction() {
      setFirstPageName("xxwc.oracle.apps.inv.invtxn.server.XXWCAccountAliasPage"); /*This should point to the file name XXWCRcptGenFunction.java in the local directory*/
      addListener(this);
      UtilFns.log("constructing XXWCAccountAliasFunction function");
  }


  /*************************************************************************
   *   NAME: public void appEntered(MWAEvent mwaevent)
   *
   *   PURPOSE:   Standard appEntered Method and inherits
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
        1.0       20-JUN-2015   Lee Spitzer             Initial Version - 
                                                        TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
    **************************************************************************/

  public void appEntered(MWAEvent mwaevent) {
      UtilFns.log("App entered : complete");
      try {
          super.appEntered(mwaevent);
      } catch (Exception exception) {
          UtilFns.log("Error calling parent");
      }
  }


  /*************************************************************************
   *   NAME: appExited(MWAEvent mwaevent)
   *
   *   PURPOSE:   Standard appExited Method
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
        1.0       20-JUN-2015   Lee Spitzer             Initial Version - 
                                                        TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
    **************************************************************************/

  public void appExited(MWAEvent mwaevent) {
      UtilFns.log("App Exited : complete");
      try {
          super.appExited(mwaevent);
      } catch (Exception exception) {
          UtilFns.log("Error calling parent");
      }
  }

 }
