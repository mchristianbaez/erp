/*************************************************************************
   *   $Header XXWCAddUPCFListener.java $
   *   Module Name: XXWCAddUPCPage
   *   
   *   Package: package xxwc.oracle.apps.inv.invtxn.server;
   *   
   *   Imports Classes:
   *   
   *   import java.sql.*;
   *   import oracle.apps.inv.utilities.server.UtilFns;
   *   import oracle.apps.mwa.beans.*;
   *   import oracle.apps.mwa.container.MWALib;
   *   import oracle.apps.mwa.container.Session;
   *   import oracle.apps.mwa.eventmodel.*;
   *   import oracle.apps.mwa.container.FileLogger;
   *   import oracle.apps.fnd.flexj.FlexException;
   *   import oracle.apps.inv.lov.server.ItemLOV;
   *   import oracle.apps.inv.lov.server.LocatorKFF;
   *   import oracle.apps.inv.lov.server.SubinventoryLOV;
   *   import xxwc.oracle.apps.inv.lov.server.*;
   *
   *   PURPOSE:   Java Class for XXWC Cycle Count Page Layout.
   *    
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
        1.0       20-JUN-2015   Lee Spitzer             Initial Version - 
                                                            TMS Ticket Number 20150622-00004 RF - UPC Update
**************************************************************************/
package xxwc.oracle.apps.inv.invtxn.server;

import java.sql.*;
import java.util.Hashtable;
import java.util.Vector;
import oracle.apps.fnd.common.VersionInfo;
import oracle.apps.inv.lov.server.*;
import oracle.apps.inv.utilities.server.*;
import oracle.apps.mwa.beans.*;
import oracle.apps.mwa.container.FileLogger;
import oracle.apps.mwa.container.Session;
import oracle.apps.mwa.eventmodel.*;
import oracle.apps.mwa.presentation.telnet.TelnetSession;
import oracle.jdbc.OraclePreparedStatement;
import oracle.sql.NUMBER;
import oracle.apps.mwa.container.MWALib;
import oracle.apps.fnd.flexj.KeyFlexfield;
import xxwc.oracle.apps.inv.lov.server.*;

/*************************************************************************
 *   NAME: XXWCAddUPCFListener implements MWAFieldListener
 *
 *   PURPOSE:   Main class for XXWCAddUPCFListener
 *
 *   REVISIONS:
 *   Ver        Date        Author                     Description
 *   ---------  ----------  ---------------         -------------------------
        1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                            TMS Ticket Number 20150622-00004 RF - UPC Update
 **************************************************************************/


public class XXWCAddUPCFListener implements MWAFieldListener {
    XXWCAddUPCPage pg;
    Session ses;
    String dialogPageButtons[];
    
    /*************************************************************************
     *   NAME: private static String gPackage = "xxwc.oracle.apps.inv.invtxn.server";
     *
     *   PURPOSE:   private method to pass gPackage from methods to return class
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150622-00004 RF - UPC Update
     **************************************************************************/
    
    private static String gPackage = "xxwc.oracle.apps.inv.invtxn.server";


    /*************************************************************************
     *   NAME: private static String gCallFrom = "XXWCAddUPCFListener";
     *
     *   PURPOSE:   private method to pass gCallFrom from methods to return class
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                 TMS Ticket Number 20150622-00004 RF - UPC Update

     **************************************************************************/

    private static String gCallFrom = "XXWCAddUPCFListener";

    /*************************************************************************
     *   NAME: public XXWCCycleCounttFListener()
     *
     *   PURPOSE:   public method XXWCAddUPCFListener
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                 TMS Ticket Number 20150622-00004 RF - UPC Update

     **************************************************************************/

    public XXWCAddUPCFListener() {
        $init$();
    }

    /*************************************************************************
     *   NAME:  public void fieldEntered(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                       DefaultOnlyHandlerException
     *
     *   PURPOSE:   field entered for XXWCPrimaryBinAssignmentPage
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                 TMS Ticket Number 20150622-00004 RF - UPC Update

     **************************************************************************/
    
    public void fieldEntered(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                       DefaultOnlyHandlerException {
        ses = mwaevent.getSession();
        pg = (XXWCAddUPCPage)ses.getCurrentPage();
        String s = UtilFns.fieldEnterSource(ses);
        
        /*Item*/
        if (s.equals("XXWC.ITEM")) {
            enteredItem(mwaevent);
            return;
        }
        /*UPC*/
        if (s.equals("XXWC.UPC")) {
            return;
        }
        /*Vendor*/
        if (s.equals("XXWC.VENDOR")) {
            enteredVendor(mwaevent);
            return;
        }

    }

    /*************************************************************************
     *   NAME:      public void fieldExited(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException
     *
     *   PURPOSE:   field exited for XXWCPrimaryBinAssignmentPage
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                 TMS Ticket Number 20150622-00004 RF - UPC Update

     **************************************************************************/

    public void fieldExited(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException {
        String s = ((FieldBean)mwaevent.getSource()).getName();
        String vField = mwaevent.getAction();
        boolean flag = false;
        boolean flag1 = false;
        boolean flag3 = false;
        if (mwaevent.getAction().equals("MWA_SUBMIT")) {
            flag = true;
        } else if (mwaevent.getAction().equals("MWA_PREVIOUSFIELD")) {
            flag1 = true;
        } else if (mwaevent.getAction().equals("MWA_NEXTFIELD")) {
            flag3 = true;
        }
         if ((flag || flag1 || flag3) && s.equals("XXWC.ITEM")) {
            exitedItem(mwaevent, ses);
            return;
        }
        if ((flag || flag1 || flag3) && s.equals("XXWC.UPC")) {
            exitedUPC(mwaevent, ses);
            return;
        }
        if ((flag || flag1 || flag3) && s.equals("XXWC.VENDOR")) {
            exitedVendor(mwaevent, ses);
            return;
        }
         if (flag && s.equals("XXWC.SAVENEXT")) {
            processXRef(mwaevent, ses);
            exitedSaveNext(mwaevent, ses);
            return;
        }
        if (flag && s.equals("XXWC.DONE")) {
            processXRef(mwaevent, ses);
            exitedDone(mwaevent, ses);
            return;
        }
        if (flag && s.equals("XXWC.MENU")) {
            exitedMenu(mwaevent, ses);
            return;
        }        
        /*Done*/
         else {
            return;
        }

    }

    /*************************************************************************
     *   NAME:      private void $init$()
     *
     *   PURPOSE:   $init$ sets the diaglogButtons button for user prompt messages to OK
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150622-00004 RF - UPC Update
     **************************************************************************/

    private void $init$() {
        dialogPageButtons = (new String[] { "OK" });
    }


    /*************************************************************************
     *   NAME:      public void enteredItem(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException
     *
     *   PURPOSE:   Item List of values for cycle count page
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150622-00004 RF - UPC Update
     **************************************************************************/



    public void enteredItem(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException {

        
        try {
            XXWCItemLOV itemLOV = pg.getmItemFld();
            itemLOV.setValidateFromLOV(true);
            itemLOV.setlovStatement("XXWC_MWA_ROUTINES_PKG.GET_WC_ITEM_LOV");
            //                      0    1    2    
            String paramType[] = { "C", "N", "S"};
            //                                     0       1       2      3              4                   5                      6                  7 
            String prompts[] = { "INVENTORY_ITEM_ID" , "ITEM", "UOM", "DESCRIPTION", "LOT_CONTROL_CODE", "SERIAL_CONTROL_CODE", "SHELF_LIFE_DAYS", "QTY"};
            //                    0     1     2     3     4     5       6    7     
            boolean flag[] = {false, true, true, true, false, false, true, true};
            //                        0        1                                                              2 
            String parameters[] = { " ", "ORGID", "xxwc.oracle.apps.inv.invtxn.server.XXWCAddUPCPage.XXWC.ITEM"};
            itemLOV.setInputParameterTypes(paramType);
            itemLOV.setInputParameters(parameters);
            itemLOV.setSubfieldPrompts(prompts);
            itemLOV.setSubfieldDisplays(flag);

        } catch (Exception e) {
            UtilFns.error("Error in calling enteredItem" + e);
        }
    }
    


    /*************************************************************************
     *   NAME: public void exitedItem(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException
     *
     *   PURPOSE:  Exited Item Field Listener to populate the Item Description Field
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150622-00004 RF - UPC Update
     **************************************************************************/

    public void exitedItem(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                  DefaultOnlyHandlerException {
        try {
            
            if (pg.getmItemFld().getValue().equals(null) || pg.getmItemFld().getValue().equals("") ) {
                
                pg.getmItemDescFld().setHidden(true);
               
            }
            else {
                
                pg.getmItemDescFld().setValue(pg.getmItemFld().getmItemDescription());
                pg.getmItemDescFld().setHidden(false);
                pg.getmUPCFld().setHidden(false);
                
            }
        }
        catch (Exception e) {
                UtilFns.error("Error in calling exitedItem" + e);
        }
    }

    /*************************************************************************
     *   NAME: public void exitedUPC(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException
     *
     *   PURPOSE:  Exited Item Field Listener to populate the Item Description Field
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150622-00004 RF - UPC Update
     **************************************************************************/

    public void exitedUPC(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                  DefaultOnlyHandlerException {
        try {
            
            if (pg.getmUPCFld().getValue().equals(null) || pg.getmUPCFld().getValue().equals("") ) {
                pg.getmItemDescFld().setHidden(true);
                pg.getmUPCFld().setHidden(true);
                pg.getmVendorFld().setHidden(true);
                pg.getmSaveNext().setHidden(true);
                pg.getmDone().setHidden(true);
                ses.setNextFieldName("XXWC.ITEM");
            }
            else {
                pg.getmVendorFld().setHidden(false);
                pg.getmSaveNext().setHidden(false);
                pg.getmDone().setHidden(false);
                ses.setNextFieldName("XXWC.VENDOR");
            }
        } 
        catch (Exception e) {
                UtilFns.error("Error in calling exitedUPC" + e);
        }
    }


    /*************************************************************************
     *   NAME:      public void enteredVendor(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException
     *
     *   PURPOSE:   Item List of values for cycle count page
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150622-00004 RF - UPC Update
     **************************************************************************/



    public void enteredVendor(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException {

        
        try {
            XXWCVendorLOV vendorLOV = pg.getmVendorFld();
            vendorLOV.setValidateFromLOV(true);
            vendorLOV.setlovStatement("XXWC_MWA_ROUTINES_PKG.GET_VENDOR_LOV");
            String paramType[] = { "C", "S"};
            String prompts[] = { "VENDOR_ID","VENDOR_NAME", "VENDOR_NUMBER"};
            boolean flag[] = {false, true, true};
            String parameters[] = { " ", "xxwc.oracle.apps.inv.invtxn.server.XXWCAddUPCPage.XXWC.VENDOR"};
            vendorLOV.setInputParameterTypes(paramType);
            vendorLOV.setInputParameters(parameters);
            vendorLOV.setSubfieldPrompts(prompts);
            vendorLOV.setSubfieldDisplays(flag);

        } catch (Exception e) {
            UtilFns.error("Error in calling enteredItem" + e);
        }
    }


    /*************************************************************************
     *   NAME: public void exitedVendor(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException
     *
     *   PURPOSE:  Exited Item Field Listener to populate the Item Description Field
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150622-00004 RF - UPC Update
     **************************************************************************/

    public void exitedVendor(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                  DefaultOnlyHandlerException {
        try {
            
            if (pg.getmVendorFld().getValue().equals(null) || pg.getmVendorFld().getValue().equals("") ) {
                
                //pg.getmVendorFld().setHidden(true);
                //pg.getmSaveNext().setHidden(true);
                //pg.getmDone().setHidden(true);
                return;
            }
            else {
                    pg.getmVendorFld().getValue().toUpperCase(); //Added per testing feedback on 21-JUN-2015
                //    pg.getmSaveNext().setHidden(false);
                //    pg.getmDone().setHidden(false);
                
                }
                
            }
        catch (Exception e) {
                UtilFns.error("Error in calling exitedVendor" + e);
        }
    }




    /*************************************************************************
    *   NAME: public void exitedMenu(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                     DefaultOnlyHandlerException
    *
    *   PURPOSE:  Exited Close For Field Listener Process to proces the Close Button
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
         1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                            TMS Ticket Number 20150622-00004 RF - UPC Update
    **************************************************************************/

    public void exitedMenu(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                   DefaultOnlyHandlerException {
        String gMethod = "exitedMenu";
        String gCallPoint = "Start";
        XXWCAddUPCPage _tmp = pg;
        ses.setStatusMessage(pg.WMS_TXN_CANCEL);
        ses.clearAllApplicationScopeObjects();
        ses.setStatusMessage(pg.WMS_TXN_CANCEL);
        pg.getmMenu().setNextPageName("|END_OF_TRANSACTION|");
    }
    
    
    /*************************************************************************
    *   NAME: public void exitedSaveNext(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                     DefaultOnlyHandlerException
    *
    *   PURPOSE:  Exited Close For Field Listener Process to proces the Close Button
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
         1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                            TMS Ticket Number 20150622-00004 RF - UPC Update
    **************************************************************************/

    public void exitedSaveNext(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                   DefaultOnlyHandlerException {
        String gMethod = "exitedSaveNext";
        String gCallPoint = "Start";
        XXWCAddUPCPage _tmp = pg;
        ses.setStatusMessage(pg.WMS_TXN_SUCCESS);
        
        pg.getmItemDescFld().setHidden(true);
        pg.getmUPCFld().setHidden(true);
        pg.getmVendorFld().setHidden(true);
        pg.getmSaveNext().setHidden(true);
        pg.getmDone().setHidden(true);
    
        pg.getmItemFld().clear();
        pg.getmItemDescFld().setValue("");
        pg.getmUPCFld().setValue("");
        pg.getmVendorFld().clear();
        
        ses.setNextFieldName("XXWC.ITEM");
    }
    
    
    /*************************************************************************
    *   NAME: public void exitedSaveNext(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                     DefaultOnlyHandlerException
    *
    *   PURPOSE:  Exited Close For Field Listener Process to proces the Close Button
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
         1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                            TMS Ticket Number 20150622-00004 RF - UPC Update
    **************************************************************************/

    public void exitedDone(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                   DefaultOnlyHandlerException {
        String gMethod = "exitedDone";
        String gCallPoint = "Start";
        XXWCAddUPCPage _tmp = pg;
        ses.setStatusMessage(pg.WMS_TXN_SUCCESS);
        ses.clearAllApplicationScopeObjects();
        ses.setStatusMessage(pg.WMS_TXN_SUCCESS);
        pg.getmDone().setNextPageName("|END_OF_TRANSACTION|");
    }
    
    
        /*************************************************************************
        *   NAME: public void processXRef(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                         DefaultOnlyHandlerException
        *
        *   PURPOSE:  Exited Close For Field Listener Process to proces the Close Button
        *
        *   REVISIONS:
        *   Ver        Date        Author                     Description
        *   ---------  ----------  ---------------         -------------------------
             1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150622-00004 RF - UPC Update
        **************************************************************************/

        public void processXRef(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                       DefaultOnlyHandlerException {


        
        
            String gMethod = "processXRef";
            String gCallPoint = "start";
            String p_organization_id;
            String p_inventory_item_id;
            String p_cross_reference;
            String p_description; 
            String p_user_id; 
            Integer x_return;
            String x_message;
            
            
            p_organization_id = ""; // Setting Value to Null to grant to all orgs (String)ses.getObject("ORGID");
            p_inventory_item_id = pg.getmItemFld().getmInventoryItemId();
            p_cross_reference = pg.getmUPCFld().getValue();
            p_user_id = (String)ses.getObject("USERID");
            x_return = 0;
            x_message = "";
            
            if (pg.getmVendorFld().getValue().equals("") || pg.getmVendorFld().getValue().equals(null)){
                p_description = "";
            }
            else {
                p_description =  pg.getmVendorFld().getmVendorNumber(); // pg.getmVendorFld().getmVendorNumber() + " - " + pg.getmVendorFld().getmVendorName(); --Updated 06/29/2015 per Testing Feedback
            }
            
            
            try {
                  CallableStatement cstmt = null;
                  Connection con = ses.getConnection();
                  //                                                                1 2 3 4 5 6 7
                  cstmt = con.prepareCall("{call XXWC_MWA_ROUTINES_PKG.PROCESS_XREF(?,?,?,?,?,?,?)");
                  cstmt.setString(1, p_organization_id);
                  cstmt.setString(2, p_inventory_item_id);
                  cstmt.setString(3, p_cross_reference);
                  cstmt.setString(4, p_description);
                  cstmt.setString(5, p_user_id);
                  cstmt.registerOutParameter(6, Types.INTEGER);
                  cstmt.registerOutParameter(7, Types.VARCHAR);
                  cstmt.execute();
                  x_return = cstmt.getInt(6);
                  x_message = cstmt.getString(7);
                  cstmt.close();
                  boolean showSubError = true;
                  if (x_return > 0) {
                    TelnetSession telnetsessionX = (TelnetSession)ses;
                    Integer k =
                    telnetsessionX.showPromptPage("Error!", x_message,
                                                            dialogPageButtons);
                        if (k == 0) {
                            ses.setNextFieldName("XXWC.UPC");
                        }
                  }
                }
            catch (Exception e) {
                UtilFns.error("Error in processXRef " + gMethod + " " + e);
            }
         
        }


}