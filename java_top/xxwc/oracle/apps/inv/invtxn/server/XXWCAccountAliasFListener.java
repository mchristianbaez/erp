/*************************************************************************
   *   $Header XXWCAccountAliasFListener.java $
   *   Module Name: XXWCAccountAliasPage
   *   
   *   Package: package xxwc.oracle.apps.inv.invtxn.server;
   *   
   *   Imports Classes:
   *   
   *   import java.sql.*;
   *   import oracle.apps.inv.utilities.server.UtilFns;
   *   import oracle.apps.mwa.beans.*;
   *   import oracle.apps.mwa.container.MWALib;
   *   import oracle.apps.mwa.container.Session;
   *   import oracle.apps.mwa.eventmodel.*;
   *   import oracle.apps.mwa.container.FileLogger;
   *   import oracle.apps.fnd.flexj.FlexException;
   *   import oracle.apps.inv.lov.server.ItemLOV;
   *   import oracle.apps.inv.lov.server.LocatorKFF;
   *   import oracle.apps.inv.lov.server.SubinventoryLOV;
   *   import xxwc.oracle.apps.inv.lov.server.*;
   *
   *   PURPOSE:   Java Class for XXWC Cycle Count Page Layout.
   *    
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
        1.0       20-JUN-2015   Lee Spitzer             Initial Version - 
                                                            TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
**************************************************************************/
package xxwc.oracle.apps.inv.invtxn.server;

import java.sql.*;
import java.util.Hashtable;
import java.util.Vector;
import oracle.apps.fnd.common.VersionInfo;
import oracle.apps.inv.lov.server.*;
import oracle.apps.inv.utilities.server.*;
import oracle.apps.mwa.beans.*;
import oracle.apps.mwa.container.FileLogger;
import oracle.apps.mwa.container.Session;
import oracle.apps.mwa.eventmodel.*;
import oracle.apps.mwa.presentation.telnet.TelnetSession;
import oracle.jdbc.OraclePreparedStatement;
import oracle.sql.NUMBER;
import oracle.apps.mwa.container.MWALib;
import oracle.apps.fnd.flexj.KeyFlexfield;
import xxwc.oracle.apps.inv.lov.server.*;

/*************************************************************************
 *   NAME: XXWCAccountAliasFListener implements MWAFieldListener
 *
 *   PURPOSE:   Main class for XXWCAccountAliasFListener
 *
 *   REVISIONS:
 *   Ver        Date        Author                     Description
 *   ---------  ----------  ---------------         -------------------------
        1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                            TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
 **************************************************************************/


public class XXWCAccountAliasFListener implements MWAFieldListener {
    XXWCAccountAliasPage pg;
    Session ses;
    String dialogPageButtons[];
    
    /*************************************************************************
     *   NAME: private static String gPackage = "xxwc.oracle.apps.inv.invtxn.server";
     *
     *   PURPOSE:   private method to pass gPackage from methods to return class
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
     **************************************************************************/
    
    private static String gPackage = "xxwc.oracle.apps.inv.invtxn.server";


    /*************************************************************************
     *   NAME: private static String gCallFrom = "XXWCAccountAliasFListener";
     *
     *   PURPOSE:   private method to pass gCallFrom from methods to return class
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                 TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions

     **************************************************************************/

    private static String gCallFrom = "XXWCAccountAliasFListener";

    /*************************************************************************
     *   NAME: public XXWCCycleCounttFListener()
     *
     *   PURPOSE:   public method XXWCAccountAliasFListener
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                 TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions

     **************************************************************************/

    public XXWCAccountAliasFListener() {
        $init$();
    }

    /*************************************************************************
     *   NAME:  public void fieldEntered(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                       DefaultOnlyHandlerException
     *
     *   PURPOSE:   field entered for XXWCPrimaryBinAssignmentPage
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                 TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions

     **************************************************************************/
    
    public void fieldEntered(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                       DefaultOnlyHandlerException {
        ses = mwaevent.getSession();
        pg = (XXWCAccountAliasPage)ses.getCurrentPage();
        String s = UtilFns.fieldEnterSource(ses);
        
        /*Transaction Type*/
        if (s.equals("XXWC.TRANSACTION_TYPE")) {
            enteredTransactionType(mwaevent);
            return;
        }
        /*Account Alias*/
        if (s.equals("XXWC.ACCOUNT_ALIAS")) {
            enteredAccountAlias(mwaevent);
            return;
        }
        /*Item*/
        if (s.equals("XXWC.ITEM")) {
            enteredItem(mwaevent);
            return;
        }
        /*Subinventory*/
        if (s.equals("XXWC.SUB")) {
            enteredSub(mwaevent);
            return;
        }
        /*Locator*/
        if (s.equals("XXWC.LOC")) {
            enteredLocator(mwaevent);
            return;
        }
        /*Quantity*/
        if (s.equals("XXWC.QTY")) {
            return;
        }

    }

    /*************************************************************************
     *   NAME:      public void fieldExited(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException
     *
     *   PURPOSE:   field exited for XXWCPrimaryBinAssignmentPage
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                 TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions

     **************************************************************************/

    public void fieldExited(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException {
        String s = ((FieldBean)mwaevent.getSource()).getName();
        String vField = mwaevent.getAction();
        boolean flag = false;
        boolean flag1 = false;
        boolean flag3 = false;
        if (mwaevent.getAction().equals("MWA_SUBMIT")) {
            flag = true;
        } else if (mwaevent.getAction().equals("MWA_PREVIOUSFIELD")) {
            flag1 = true;
        } else if (mwaevent.getAction().equals("MWA_NEXTFIELD")) {
            flag3 = true;
        }
        /*Transaction Type*/
        if ((flag || flag1 || flag3) && s.equals("XXWC.TRANSACTION_TYPE")) {
            exitedTransactionType(mwaevent, ses);
            return;
        }
        /*Account Alias*/
        if ((flag || flag1 || flag3) && s.equals("XXWC.ACCOUNT_ALIAS")) {
            exitedAccountAlias(mwaevent, ses);
            return;
        }
        if ((flag || flag1 || flag3) && s.equals("XXWC.ITEM")) {
            exitedItem(mwaevent, ses);
            return;
        }
        if ((flag || flag1 || flag3) && s.equals("XXWC.SUB")) {
            exitedSub(mwaevent, ses);
            return;
        }
        if ((flag || flag1 || flag3) && s.equals("XXWC.LOC")) {
            exitedLocator(mwaevent, ses);
            return;
        }
        if ((flag || flag1 || flag3) && s.equals("XXWC.QTY")) {
            exitedQty(mwaevent, ses);
            return;
        }
        if (flag && s.equals("XXWC.SAVENEXT")) {
            processAliasTxn(mwaevent, ses);
            exitedSaveNext(mwaevent, ses);
            return;
        }
        if (flag && s.equals("XXWC.DONE")) {
            processAliasTxn(mwaevent, ses);
            exitedDone(mwaevent, ses);
            return;
        }
        if (flag && s.equals("XXWC.MENU")) {
            exitedMenu(mwaevent, ses);
            return;
        }        
        /*Done*/
         else {
            return;
        }

    }

    /*************************************************************************
     *   NAME:      private void $init$()
     *
     *   PURPOSE:   $init$ sets the diaglogButtons button for user prompt messages to OK
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
     **************************************************************************/

    private void $init$() {
        dialogPageButtons = (new String[] { "OK" });
    }


    /*************************************************************************
     *   NAME:      public void enteredTransactionType(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException
     *
     *   PURPOSE:   Item List of values for cycle count page
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
     **************************************************************************/



    public void enteredTransactionType(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException {

        
        try {
            XXWCTransactionTypeLOV transactionTypeLOV = pg.getmTranasctionTypeFld();
            transactionTypeLOV.setValidateFromLOV(true);
            transactionTypeLOV.setlovStatement("XXWC_MWA_ROUTINES_PKG.GET_TRANSACTION_TYPE_LOV");
            //                      0    1    
            String paramType[] = { "C", "S"};
            //                                      1                        2              3                       4                               5                   6                       7                         8
            String prompts[] = { "TRANSACTION_TYPE_ID", "TRANSACTION_TYPE_NAME", "DESCRIPTION", "TRANSACTION_ACTION_ID", "TRANSACTION_SOURCE_TYPE_ID", "USER_DEFINED_FLAG", "STATUS_CONTROL_FLAG", "LOCATION_REQUIRED_FIELD"};
            //                    1     2    3      4      5      6        7     8
            boolean flag[] = {false, true, true, false, false, false, false, false};
            String parameters[] = { " ", "xxwc.oracle.apps.inv.invtxn.server.XXWCAccountAliasPage.XXWC.TRANSACTION_TYPE"};
            transactionTypeLOV.setInputParameterTypes(paramType);
            transactionTypeLOV.setInputParameters(parameters);
            transactionTypeLOV.setSubfieldPrompts(prompts);
            transactionTypeLOV.setSubfieldDisplays(flag);

        } catch (Exception e) {
            UtilFns.error("Error in calling enteredTransactionType" + e);
        }
    }


    /*************************************************************************
     *   NAME:      public void enteredAccountAlias(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException
     *
     *   PURPOSE:   Item List of values for cycle count page
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
     **************************************************************************/



    public void enteredAccountAlias(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException {

        
        try {
            XXWCAccountAliasLOV accountAliasLOV = pg.getmAccountAliasFld();
            accountAliasLOV.setValidateFromLOV(true);
            accountAliasLOV.setlovStatement("XXWC_MWA_ROUTINES_PKG.GET_ACCOUNT_ALIAS_LOV");
            //                      0    1    2    
            String paramType[] = { "C", "N", "S"};
            //                                     0       1       2      3              4                   5                      6                  7 
            String prompts[] = { "DISPOSITION_ID","CONCATENATED_SEGMENTS", "DESCRIPTION"};
            boolean flag[] = {false, true, true};
                       //                        0        1                                                              2 
            String parameters[] = { " ", "ORGID", "xxwc.oracle.apps.inv.invtxn.server.XXWCAccountAliasPage.XXWC.ACCOUNT_ALIAS"};
            accountAliasLOV.setInputParameterTypes(paramType);
            accountAliasLOV.setInputParameters(parameters);
            accountAliasLOV.setSubfieldPrompts(prompts);
            accountAliasLOV.setSubfieldDisplays(flag);

        } catch (Exception e) {
            UtilFns.error("Error in calling enteredItem" + e);
        }
    }

    /*************************************************************************
     *   NAME:      public void enteredItem(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException
     *
     *   PURPOSE:   Item List of values for cycle count page
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
     **************************************************************************/



    public void enteredItem(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException {

        
        try {
            XXWCItemLOV itemLOV = pg.getmItemFld();
            itemLOV.setValidateFromLOV(true);
            itemLOV.setlovStatement("XXWC_MWA_ROUTINES_PKG.GET_WC_ITEM_LOV");
            //                      0    1    2    
            String paramType[] = { "C", "N", "S"};
            //                                     0       1       2      3              4                   5                      6                  7 
            String prompts[] = { "INVENTORY_ITEM_ID" , "ITEM", "UOM", "DESCRIPTION", "LOT_CONTROL_CODE", "SERIAL_CONTROL_CODE", "SHELF_LIFE_DAYS", "QTY"};
            //                    0     1     2     3     4     5       6    7     
            boolean flag[] = {false, true, true, true, false, false, true, true};
            //                        0        1                                                              2 
            String parameters[] = { " ", "ORGID", "xxwc.oracle.apps.inv.invtxn.server.XXWCAccountAliasPage.XXWC.ITEM"};
            itemLOV.setInputParameterTypes(paramType);
            itemLOV.setInputParameters(parameters);
            itemLOV.setSubfieldPrompts(prompts);
            itemLOV.setSubfieldDisplays(flag);

        } catch (Exception e) {
            UtilFns.error("Error in calling enteredItem" + e);
        }
    }
    


    /*************************************************************************
     *   NAME:      public void enteredSub(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException
     *
     *   PURPOSE:   Item List of values for cycle count page
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
     **************************************************************************/



    public void enteredSub(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException {

        
        try {
            XXWCSubLOV subLOV = pg.getmSubFld();
            subLOV.setValidateFromLOV(true);
            subLOV.setlovStatement("XXWC_MWA_ROUTINES_PKG.GET_WC_SUB_LOV");
            String paramType[] = { "C", "N", "S", "S"};
            String prompts[] = { "SUBINVENTORY_CODE" , "DESCRIPTION", "LOCATOR_TYPE", "ASSET_INVENTORY"};
            boolean flag[] = {true, true, false, false};
            String parameters[] = { " ", "ORGID", "xxwc.oracle.apps.inv.invtxn.server.XXWCAccountAliasPage.XXWC.SUB", ""};
            subLOV.setInputParameterTypes(paramType);
            subLOV.setInputParameters(parameters);
            subLOV.setSubfieldPrompts(prompts);
            subLOV.setSubfieldDisplays(flag);

        } catch (Exception e) {
            UtilFns.error("Error in calling enteredSub" + e);
        }
    }

    /*************************************************************************
     *   NAME: public void exitedTransactionType(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException
     *
     *   PURPOSE:  Exited Item Field Listener to populate the Item Description Field
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
     **************************************************************************/

    public void exitedTransactionType(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                  DefaultOnlyHandlerException {
        try {
            
            if (pg.getmTranasctionTypeFld().getValue().equals(null) || pg.getmTranasctionTypeFld().getValue().equals("") ) {
                return;
            }
            else {
                
                pg.getmAccountAliasFld().setHidden(false);
            }
        }
        catch (Exception e) {
                UtilFns.error("Error in calling exitedTransactionType" + e);
        }
    }
    
    /*************************************************************************
     *   NAME: public void exitedAccountAlias(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException
     *
     *   PURPOSE:  Exited Item Field Listener to populate the Item Description Field
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
     **************************************************************************/

    public void exitedAccountAlias(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                  DefaultOnlyHandlerException {
        try {
            
            if (pg.getmAccountAliasFld().getValue().equals(null) || pg.getmAccountAliasFld().getValue().equals("") ) {
                pg.getmAccountAliasFld().setHidden(true);
                return;
            }
            else {
                
                pg.getmItemFld().setHidden(false);
                
                pg.getmTranasctionTypeFld().setEditable(false);
                pg.getmAccountAliasFld().setEditable(false);
            }
        }
        catch (Exception e) {
                UtilFns.error("Error in calling exitedAccountAlias" + e);
        }
    }


    /*************************************************************************
     *   NAME: public void exitedItem(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException
     *
     *   PURPOSE:  Exited Item Field Listener to populate the Item Description Field
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
     **************************************************************************/

    public void exitedItem(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                  DefaultOnlyHandlerException {
        try {
            
            if (pg.getmItemFld().getValue().equals(null) || pg.getmItemFld().getValue().equals("") ) {
                
                pg.getmItemFld().setHidden(true);
                pg.getmItemDescFld().setHidden(true);
                pg.getmAccountAliasFld().setEditable(true);
                pg.getmTranasctionTypeFld().setEditable(true);
                
                ses.setNextFieldName("XXWC.ACCOUNT_ALIAS");
               
            }
            else {
                
                pg.getmItemDescFld().setValue(pg.getmItemFld().getmItemDescription());
                pg.getmUOMFld().setValue(pg.getmItemFld().getmPrimaryUOMCode());
                pg.getmItemDescFld().setHidden(false);
                pg.getmSubFld().setHidden(false);
            }
        }
        catch (Exception e) {
                UtilFns.error("Error in calling exitedItem" + e);
        }
    }

    /*************************************************************************
     *   NAME: public void exitedSub(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException
     *
     *   PURPOSE:  Exited Item Field Listener to populate the Item Description Field
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
     **************************************************************************/

    public void exitedSub(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                  DefaultOnlyHandlerException {
        try {
            
            if (pg.getmSubFld().getValue().equals(null) || pg.getmSubFld().getValue().equals("") ) {
                pg.getmSubFld().setHidden(true);
                pg.getmOnHandFld().setHidden(true);
                pg.getmAvailableFld().setHidden(true);
                pg.getmItemDescFld().setHidden(true);
                ses.setNextFieldName("XXWC.ITEM");
            }
            else {
                
                try{
                    Integer i = getLocatorControl(mwaevent, ses);
                    FileLogger.getSystemLogger().trace("Locator Control returned from getLocatorControl " + i);
                    if (i == 1) {
                        FileLogger.getSystemLogger().trace("Locator control is not enabled");
                
                            getOnHand(mwaevent, ses);
                            getAvailable(mwaevent,ses);
                            
                            pg.getmOnHandFld().setHidden(false);
                            pg.getmAvailableFld().setHidden(false);
                            pg.getmUOMFld().setHidden(false);
                            pg.getmQtyFld().setHidden(false);
                    } else {
                        FileLogger.getSystemLogger().trace("Locator control is enabled");
                        pg.getmLocFld().setHidden(false);
                        pg.getmLocFld().setRequired(true);
                        pg.getmLocFld().setEditable(true);
                        ses.setNextFieldName("XXWC.LOC");
                    }
                }
            catch (Exception e) {
                FileLogger.getSystemLogger().trace("Error in calling exitedSub " + e);
                UtilFns.error("Error in calling exitedSub" + e);
            }
            }
        }
        catch (Exception e) {
                UtilFns.error("Error in calling exitedSub" + e);
        }
    }


    /*************************************************************************
     *   NAME: public void exitedQty(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException
     *
     *   PURPOSE:  Exited Item Field Listener to populate the Item Description Field
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
     **************************************************************************/

 
    public void exitedQty(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException, DefaultOnlyHandlerException
        {
            String gMethod = "exitedQty";
            String gCallPoint = "start";
            
            if (pg.getmQtyFld().getValue().equals(null) || pg.getmQtyFld().getValue().equals("") ) {
            
                Integer i = getLocatorControl(mwaevent, ses);
                FileLogger.getSystemLogger().trace("Locator Control returned from getLocatorControl " + i);
                if (i == 1) {
                    ses.setNextFieldName("XXWC.SUB");
                }
                else {
                    ses.setNextFieldName("XXWC.LOC");
                }
                
                pg.getmOnHandFld().setHidden(true);
                pg.getmAvailableFld().setHidden(true);
                pg.getmUOMFld().setHidden(true);
                pg.getmQtyFld().setHidden(true);
                pg.getmSaveNext().setHidden(true);
                pg.getmDone().setHidden(true);
            }
            
            else {
            
            
                Double d;
                Double ad;
                String l_quantity = pg.getmQtyFld().getValue();
                FileLogger.getSystemLogger().trace(gMethod + " " + gCallPoint + " " + "l_quantity " + l_quantity);
                String exception_message = "";
                if (!l_quantity.isEmpty()) {
                        try {
                            d = Double.valueOf(pg.getmQtyFld().getValue());
                            ad =  Double.valueOf(pg.getmAvailableFld().getValue());
                                
                                if (d < 0) {
                                            exception_message = "Number is not positive";
                                               throw new AbortHandlerException(exception_message);
                                    }
                                else if (d == 0) {
                                                exception_message = "Number can not be 0";
                                                   throw new AbortHandlerException(exception_message);
                                }
                        
                                else if (d > 0) {
                                    
                                    if (d > ad && pg.getmTranasctionTypeFld().getmTransactionTypeId().equals("31")) {
                                        TelnetSession telnetsessionX = (TelnetSession)ses;
                                        Integer k =
                                            telnetsessionX.showPromptPage("Warning!", pg.getmQtyFld().getValue() + " will drive inventory negative.  Only " + pg.getmAvailableFld().getValue() + " available. Do you want to continue?",
                                                                          new String [] {"Yes", "No"});
                                            if (k == 0) {
                                                pg.getmSaveNext().setHidden(false);
                                                pg.getmDone().setHidden(false);
                                            }
                                            else if(k == 1) {
                                                pg.getmQtyFld().setValue("");
                                                ses.setNextFieldName("XXWC.QTY");
                                            }
                                    }
                                    else {
                                        pg.getmSaveNext().setHidden(false);
                                        pg.getmDone().setHidden(false);
                                    }            
                                 }
                            }
                        
                         catch (Exception exception) {
                            gCallPoint = "Number value check";
                            UtilFns.error(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " " + exception);
                            if (exception_message == "") {
                                ses.setStatusMessage(UtilFns.getMessage(ses, "INV", "INV_MWA_NUMBER_EXPECTED"));
                            }
                            else  {
                                ses.setStatusMessage(exception_message);
                            }    
                            throw new AbortHandlerException((new StringBuilder()).append(gMethod + " "  + gCallPoint).append(exception).toString());
                        }
                }
                else {
                    return;
                }
            }   
        }
    
        
    /*************************************************************************
    *   NAME: public void exitedMenu(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                     DefaultOnlyHandlerException
    *
    *   PURPOSE:  Exited Close For Field Listener Process to proces the Close Button
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
         1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                            TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
    **************************************************************************/

    public void exitedMenu(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                   DefaultOnlyHandlerException {
        String gMethod = "exitedMenu";
        String gCallPoint = "Start";
        XXWCAccountAliasPage _tmp = pg;
        ses.setStatusMessage(pg.WMS_TXN_CANCEL);
        ses.clearAllApplicationScopeObjects();
        ses.setStatusMessage(pg.WMS_TXN_CANCEL);
        pg.getmMenu().setNextPageName("|END_OF_TRANSACTION|");
    }
    
    
    /*************************************************************************
    *   NAME: public void exitedSaveNext(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                     DefaultOnlyHandlerException
    *
    *   PURPOSE:  Exited Close For Field Listener Process to proces the Close Button
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
         1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                            TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
    **************************************************************************/

    public void exitedSaveNext(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                   DefaultOnlyHandlerException {
        String gMethod = "exitedSaveNext";
        String gCallPoint = "Start";
        XXWCAccountAliasPage _tmp = pg;
        ses.setStatusMessage(pg.WMS_TXN_SUCCESS);
        
        pg.getmItemDescFld().setHidden(true);
        pg.getmSubFld().setHidden(true);
        pg.getmLocFld().setHidden(true);
        pg.getmOnHandFld().setHidden(true);
        pg.getmAvailableFld().setHidden(true);
        pg.getmUOMFld().setHidden(true);
        pg.getmQtyFld().setHidden(true);
        pg.getmSaveNext().setHidden(true);
        pg.getmDone().setHidden(true);
    
        pg.getmItemFld().clear();
        pg.getmItemDescFld().setValue("");
        pg.getmSubFld().clear();
        pg.getmLocFld().clear();
        pg.getmOnHandFld().setValue("");
        pg.getmAvailableFld().setValue("");
        pg.getmUOMFld().setValue("");
        pg.getmQtyFld().setValue("");
        
        ses.setNextFieldName("XXWC.ITEM");
    }
    
    
    /*************************************************************************
    *   NAME: public void exitedSaveNext(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                     DefaultOnlyHandlerException
    *
    *   PURPOSE:  Exited Close For Field Listener Process to proces the Close Button
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
         1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                            TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
    **************************************************************************/

    public void exitedDone(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                   DefaultOnlyHandlerException {
        String gMethod = "exitedDone";
        String gCallPoint = "Start";
        XXWCAccountAliasPage _tmp = pg;
        ses.setStatusMessage(pg.WMS_TXN_SUCCESS);
        ses.clearAllApplicationScopeObjects();
        ses.setStatusMessage(pg.WMS_TXN_SUCCESS);
        pg.getmDone().setNextPageName("|END_OF_TRANSACTION|");
    }
    
    
    
    /*************************************************************************
    *   NAME: public void getOnHand(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                     DefaultOnlyHandlerException
    *
    *   PURPOSE:  Exited Close For Field Listener Process to proces the Close Button
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
         1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                            TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
    **************************************************************************/

    public void getOnHand(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                   DefaultOnlyHandlerException {


    
    
        String gMethod = "getOnHand";
        String gCallPoint = "start";
        String p_inventory_item_id;
        String p_organization_id;
        String p_subinventory;
        String p_locator_id; 
        String p_return_type; 
        String p_lot_number; 
        Integer x_return;
        
        p_inventory_item_id = pg.getmItemFld().getmInventoryItemId();
        p_organization_id = (String)ses.getObject("ORGID");
        p_subinventory = pg.getmSubFld().getValue();
        p_locator_id = "";
        p_return_type = "QOH";
        p_lot_number = "";
        x_return = 0;
        
        try {
              CallableStatement cstmt = null;
              Connection con = ses.getConnection();
              //                        1                                         2 3 4 5 6 7
              cstmt = con.prepareCall("{? = call XXWC_MWA_ROUTINES_PKG.GET_ONHAND(?,?,?,?,?,?)");
              cstmt.registerOutParameter(1, Types.INTEGER);
              cstmt.setString(2, p_inventory_item_id);
              cstmt.setString(3, p_organization_id);
              cstmt.setString(4, p_subinventory);
              cstmt.setString(5, p_locator_id);
              cstmt.setString(6, p_return_type);
              cstmt.setString(7, p_lot_number);
              cstmt.execute();
              x_return = cstmt.getInt(1);
              cstmt.close();
              /*boolean showSubError = true;
              if (x_return > 1) {
                TelnetSession telnetsessionX = (TelnetSession)ses;
                Integer k =
                telnetsessionX.showPromptPage("Error!", x_message,
                                                        dialogPageButtons);
                    if (k == 0) {
                        pg.getmCountQtyFld().setValue(null);
                    }
              }
             */
        }
        catch (Exception e) {
            UtilFns.error("Error in getOnHand " + gMethod + " " + e);
        }
        
        pg.getmOnHandFld().setValue(x_return.toString());
    }
    
    
    /*************************************************************************
    *   NAME: public void getAvailable(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                     DefaultOnlyHandlerException
    *
    *   PURPOSE:  Exited Close For Field Listener Process to proces the Close Button
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
         1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                            TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
    **************************************************************************/

    public void getAvailable(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                   DefaultOnlyHandlerException {


    
    
        String gMethod = "getAvailable";
        String gCallPoint = "start";
        String p_inventory_item_id;
        String p_organization_id;
        String p_subinventory;
        String p_locator_id; 
        String p_return_type; 
        String p_lot_number; 
        Integer x_return;
        
        p_inventory_item_id = pg.getmItemFld().getmInventoryItemId();
        p_organization_id = (String)ses.getObject("ORGID");
        p_subinventory = pg.getmSubFld().getValue();
        p_locator_id = "";
        p_return_type = "ATT";
        p_lot_number = "";
        x_return = 0;
        
        try {
              CallableStatement cstmt = null;
              Connection con = ses.getConnection();
              //                        1                                         2 3 4 5 6 7
              cstmt = con.prepareCall("{? = call XXWC_MWA_ROUTINES_PKG.GET_ONHAND(?,?,?,?,?,?)");
              cstmt.registerOutParameter(1, Types.INTEGER);
              cstmt.setString(2, p_inventory_item_id);
              cstmt.setString(3, p_organization_id);
              cstmt.setString(4, p_subinventory);
              cstmt.setString(5, p_locator_id);
              cstmt.setString(6, p_return_type);
              cstmt.setString(7, p_lot_number);
              cstmt.execute();
              x_return = cstmt.getInt(1);
              cstmt.close();
              /*boolean showSubError = true;
              if (x_return > 1) {
                TelnetSession telnetsessionX = (TelnetSession)ses;
                Integer k =
                telnetsessionX.showPromptPage("Error!", x_message,
                                                        dialogPageButtons);
                    if (k == 0) {
                        pg.getmCountQtyFld().setValue(null);
                    }
              }
             */
        }
        catch (Exception e) {
            UtilFns.error("Error in getAvailable " + gMethod + " " + e);
        }
        
        pg.getmAvailableFld().setValue(x_return.toString());
    }



        /*************************************************************************
        *   NAME: public void processAliasTxn(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                         DefaultOnlyHandlerException
        *
        *   PURPOSE:  Exited Close For Field Listener Process to proces the Close Button
        *
        *   REVISIONS:
        *   Ver        Date        Author                     Description
        *   ---------  ----------  ---------------         -------------------------
             1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
        **************************************************************************/

        public void processAliasTxn(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                       DefaultOnlyHandlerException {


        
        
            String gMethod = "processAliasTxn";
            String gCallPoint = "start";
            String p_inventory_item_id;
            String p_organization_id;
            String p_subinventory;
            String p_locator_id; 
            String p_lot_number; 
            String p_uom;
            String p_qty;
            String p_disposition_id;
            String p_transaction_type_id;
            String p_transaction_action_id;
            String p_transaction_source_type_id;                            
            String p_user_id;
            Integer x_return;
            String x_message;
            String l_quantity = "0";
            
            
            p_inventory_item_id = pg.getmItemFld().getmInventoryItemId();
            p_organization_id = (String)ses.getObject("ORGID");
            p_subinventory = pg.getmSubFld().getValue();
            p_locator_id = pg.getmLocFld().getmLocationId();
            p_lot_number = "";
            p_uom = pg.getmUOMFld().getValue();
            p_qty = pg.getmQtyFld().getValue();
            p_disposition_id = pg.getmAccountAliasFld().getmDispositionId();
            p_transaction_type_id = pg.getmTranasctionTypeFld().getmTransactionTypeId();
            p_transaction_action_id = pg.getmTranasctionTypeFld().getmTransactionActionId();
            p_transaction_source_type_id = pg.getmTranasctionTypeFld().getmTransactionSourceTypeId();                         
            p_user_id = (String)ses.getObject("USERID");
            x_return = 0;
            x_message = "";
            
            //If Account Alias Issue 31 - then add a - sign with p_qty
            if (pg.getmTranasctionTypeFld().getmTransactionTypeId().equals("31")) {
                l_quantity = "-" + p_qty;
            }
            //If Account Alias Receipt 41 - then p_qty
            else if (pg.getmTranasctionTypeFld().getmTransactionTypeId().equals("41")) {
                l_quantity = p_qty;
            }
            //Else p_qty
            else {
                l_quantity = p_qty;
            }
            
            try {
                  CallableStatement cstmt = null;
                  Connection con = ses.getConnection();
                  //                        1                                            1 2 3 4 5 6 7 8 9 0 1 2 3 4
                  cstmt = con.prepareCall("{call XXWC_MWA_ROUTINES_PKG.PROCESS_ALIAS_TXN(?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
                  cstmt.setString(1, p_organization_id);
                  cstmt.setString(2, p_inventory_item_id);
                  cstmt.setString(3, p_subinventory);
                  cstmt.setString(4, p_locator_id);
                  cstmt.setString(5, p_lot_number);
                  cstmt.setString(6, p_uom);
                  cstmt.setString(7, l_quantity);
                  cstmt.setString(8, p_disposition_id);
                  cstmt.setString(9, p_transaction_type_id);
                  cstmt.setString(10, p_transaction_action_id);
                  cstmt.setString(11, p_transaction_source_type_id);
                  cstmt.setString(12, p_user_id);
                  cstmt.registerOutParameter(13, Types.INTEGER);
                  cstmt.registerOutParameter(14, Types.VARCHAR);
                  cstmt.execute();
                  x_return = cstmt.getInt(13);
                  x_message = cstmt.getString(14);
                  cstmt.close();
                  boolean showSubError = true;
                  if (x_return > 0) {
                    TelnetSession telnetsessionX = (TelnetSession)ses;
                    Integer k =
                    telnetsessionX.showPromptPage("Error!", x_message,
                                                            dialogPageButtons);
                        if (k == 0) {
                            pg.getmQtyFld().setValue(null);
                        }
                  }
                }
            catch (Exception e) {
                UtilFns.error("Error in getAvailable " + gMethod + " " + e);
            }
            
        }

    /*************************************************************************
    *   NAME: public void getLocatorControl(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                     DefaultOnlyHandlerException
    *
    *   PURPOSE:  Determines if Subinventory has Locator Control turned on or off
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
         1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                            TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
    **************************************************************************/

    private int getLocatorControl(MWAEvent mwavevent, Session session) throws AbortHandlerException,
                                                                              InterruptedHandlerException,
                                                                              DefaultOnlyHandlerException {
        int i = 1;
        int j = 1;
        int k = 1;
        try {
            Long orgid = Long.parseLong((String)session.getObject("ORGID"));
            FileLogger.getSystemLogger().trace("orgid  " + orgid);
            FileLogger.getSystemLogger().trace("i  " + i);
            OrgParameters orgparameters = OrgParameters.getOrgParameters(session.getConnection(), orgid);
            i = orgparameters.getLocatorControlCode();
            FileLogger.getSystemLogger().trace("i  " + i);
            FileLogger.getSystemLogger().trace("j " + j);
            XXWCSubLOV sublov = pg.getmSubFld();
            if (pg.getmSubFld().getValue() != null | pg.getmSubFld().getValue() != "") {
                j = Integer.parseInt(sublov.getmLocatorType());
            }
            FileLogger.getSystemLogger().trace("j " + j);
            XXWCItemLOV itemlov = pg.getmItemFld();
            FileLogger.getSystemLogger().trace("k " + k);
            if (pg.getmItemFld().getValue() != null | pg.getmItemFld().getValue() != "") {
                k = Integer.parseInt(pg.getmItemFld().getmLotControlCode());
            }
            FileLogger.getSystemLogger().trace("k " + k);
            if (UtilFns.isTraceOn) {
                UtilFns.trace((new StringBuilder()).append("RCV: getLocatorControl orgLocControl:").append(i).append(":subLocControl:").append(j).append(":itemLocControl:").append(k).toString());
            }
        } catch (Exception e) {
            FileLogger.getSystemLogger().trace("error getLocatorControl " + e);
        }
      //  return pg.getmLocFld().getLocatorControl(i, j, k);
      //if Subinventory Control is Locator Contorl or Item Numberis Locator control then return 1 else return 0
      if (j > 1 || k > 1) {
          return 0;
      }
      else {
          return 1;
      }
    }

    /*************************************************************************
    *   NAME: public void enteredLocator(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                     DefaultOnlyHandlerException
    *
    *   PURPOSE:  Entered Locator Field Listener Process to set the List of Values for the Locator Field
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
         1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                            TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions

    **************************************************************************/

    public void enteredLocator(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                         DefaultOnlyHandlerException {

        /*try {
            Session session = mwaevent.getSession();
            String s = "";
            String s1 = "";
            int i = -1;
            //LocatorKFF locatorlov = pg.getmLocFld();
            //ItemLOV itemlov = pg.getmItemFld();
            if ((String)session.getObject("PROJECT_ID") != null &&
                !((String)session.getObject("PROJECT_ID")).equals("-9999")) {
                s = (String)session.getObject("PROJECT_ID");
                s1 = (String)session.getObject("TASK_ID");
            } else {
                s = "";
                s1 = "";
            }
            FileLogger.getSystemLogger().trace("PROJECT_ID " + s);
            FileLogger.getSystemLogger().trace("TASK_ID " + s1);

            if (pg.getmSubFld() != null && !pg.getmSubFld().getValue().equals("")) {
                i = getLocatorControl(mwaevent, session);
            }
            if (i == 3) {
                //locatorlov.setValidateFromLOV(false);
                pg.getmLocFld().setValidateFromLOV(true);
            } else {
                //locatorlov.setValidateFromLOV(true);
                pg.getmLocFld().setValidateFromLOV(true);
            }

            FileLogger.getSystemLogger().trace("i " + i);

            String s2 = "FALSE";
            if (session.getObject("wms_purchased") != null &&
                ((String)session.getObject("wms_purchased")).equals("I")) {
                s2 = "TRUE";
            }

            FileLogger.getSystemLogger().trace("WMS Enabled " + s2);
            //locatorlov.setSubMapper(pg.getmSubFld());
            pg.getmLocFld().setSubMapper(pg.getmSubFld());
            //locatorlov.setlovStatement("INV_UI_ITEM_SUB_LOC_LOVS.GET_INQ_LOC_LOV");
            pg.getmLocFld().setlovStatement("INV_UI_ITEM_SUB_LOC_LOVS.GET_INQ_LOC_LOV");
            //                      0    1    2    3    4    5    6    7
            String paramType[] = { "C", "N", "S", "N", "N", "S", "S", "S" };
            //                      0         1                                                                        2                                      3                             4                                                                         5   6  7
            //String parameters[] = {" ", "ORGID", "xxwc.oracle.apps.inv.bins.server.XXWCPrimaryBinAssignmentPage.INV.SUB", itemlov.getRestrictLocCode(), itemlov.getItemID(), "xxwc.oracle.apps.inv.bins.server.XXWCPrimaryBinAssignmentPage.INV.ITEM", s, s1};
            String parameters[] =
            { " ", "ORGID", "xxwc.oracle.apps.inv.invtxn.server.XXWCSubXferPage.XXWC.SUB",
              pg.getmItemFld().getmLotControlCode(), pg.getmItemFld().getmInventoryItemId(),
              "xxwc.oracle.apps.inv.invtxn.server.XXWCSubXferPage.XXWC.LOC", s, s1 };
            //locatorlov.setInputParameterTypes(paramType);
            pg.getmLocFld().setInputParameterTypes(paramType);
            //locatorlov.setInputParameters(paramType);
            pg.getmLocFld().setInputParameters(parameters);
        } catch (Exception e) {
            UtilFns.error("Error in calling enteredLoc" + e);
        }
    */
    
        try {
            ses.putSessionObject("sessInventoryItemId", pg.getmItemFld().getmInventoryItemId());
            ses.putSessionObject("sessRestrictLocator", 2);
            ses.putSessionObject("sessHidePrefix","N");
            XXWCBinsLOV binsLOV = pg.getmLocFld();
            //                      0    1    2    3    4    5    6    7    8
            String paramType[] = { "C", "N", "S", "N", "N", "S", "S", "S", "S" };
            String prompts[] = { "WC_LOCATOR", "PREFIX", "LOCATOR_ID", "LOCATOR", "DESCRIPTION", "SUBINVENTORY" };
            boolean flag[] = {false, false, false, true, false, false};
            //                        0        1                                                              2                      3                      4                                                                         5   6   7                8 
            String parameters[] = { " ", "ORGID", "xxwc.oracle.apps.inv.invtxn.server.XXWCAccountAliasPage.XXWC.SUB" ,"sessRestrictLocator", "sessInventoryItemId" , "xxwc.oracle.apps.inv.invtxn.server.XXWCAccountAliasPage.XXWC.LOC", "", "", "sessHidePrefix" };
            //String parameters[] = { " ", "ORGID", "" ,"", "sessInventoryItemId" , "xxwc.oracle.apps.inv.lables.server.XXWCItemLabelPage.XXWC.BIN", "", "", "N" };
            binsLOV.setInputParameters(parameters);
            binsLOV.setlovStatement("XXWC_LABEL_PRINTERS_PKG.GET_BIN_LOC");
            binsLOV.setInputParameterTypes(paramType);
            binsLOV.setSubfieldPrompts(prompts);
            binsLOV.setSubfieldDisplays(flag);  
            
        } catch (Exception e) {
            UtilFns.error("Error in calling enteredItem " + e);
        }
        
    }


    /*************************************************************************
     *   NAME: public void exitedLocator(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException
     *
     *   PURPOSE:  Exited Item Field Listener to populate the Item Description Field
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
     **************************************************************************/

    public void exitedLocator(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                  DefaultOnlyHandlerException {
        try {
            
            if (pg.getmLocFld().getValue().equals(null) || pg.getmLocFld().getValue().equals("") ) {
                pg.getmLocFld().setHidden(true);
                pg.getmOnHandFld().setHidden(true);
                pg.getmAvailableFld().setHidden(true);
                pg.getmUOMFld().setHidden(true);
                pg.getmQtyFld().setHidden(true);
                
            }
            else {
                    
                    getOnHand(mwaevent, ses);
                    getAvailable(mwaevent,ses);
                        
                    pg.getmOnHandFld().setHidden(false);
                    pg.getmAvailableFld().setHidden(false);
                    pg.getmUOMFld().setHidden(false);
                    pg.getmQtyFld().setHidden(false);
                
                    
                    ses.setNextFieldName("XXWC.QTY");
                }
                
            }
        catch (Exception e) {
                UtilFns.error("Error in calling exitedLocator" + e);
        }
    }






}