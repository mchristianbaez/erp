/*************************************************************************
   *   $Header XXWCCycleCount.java $
   *   Module Name: XXWCCycleCount
   *   
   *   Package xxwc.oracle.apps.inv.count.server;
   *   
   *   
   *   Imports Classes:
   *   
   *   import oracle.apps.fnd.common.VersionInfo;
   *   import oracle.apps.inv.utilities.server.OrgFunction;
   *   import oracle.apps.inv.utilities.server.UtilFns;
   *   import oracle.apps.mwa.eventmodel.*;
   *   
   *   PURPOSE:   Java Class for XXWC Cycle Count Entry Function
   *    
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
      1.0       01-JUN-2015   Lee Spitzer             Initial Version - 
                                                          TMS Ticket Number 20150302-00173 - RF - Cyclecount
 **************************************************************************/

package xxwc.oracle.apps.inv.count.server;

import oracle.apps.fnd.common.VersionInfo;

/*************************************************************************
 *   NAME: public class XXWCCycleCount
 *
 *   PURPOSE:   Main class for XXWCCycleCount
 *
 *   REVISIONS:
 *   Ver        Date        Author                     Description
 *   ---------  ----------  ---------------         -------------------------
        1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                            TMS Ticket Number 20150302-00173 - RF - Cyclecount
 **************************************************************************/

public class XXWCCycleCount {
    public static final String RCS_ID = "$Header: XXWCCycleCount.java 120.1 2006/03/22 03:16:04 rsagar noship $";
    public static final boolean RCS_ID_RECORDED =
        VersionInfo.recordClassVersion("$Header: XXWCCycleCount.java 120.1 2006/03/22 03:16:04 rsagar noship $",
                                       "oracle.apps.inv.invinq.server");

    private String mInventoryItemId;
    private String mItemNumber;
    private String mItemDescription;
    private String mLotControlCode;
    private String mSerialControlCode;
    private String mPrimaryUOMCode;
    private String mShelfLifeDays;
    private String mMinQty;
    private String mMaxQty;
    private String mSalesVelocity;
    private String mWCCountListSequence;
    private String mSubinventory;
    private String mBin;
    private String mWCBin;
    private String mInventoryLocationId;
    private String mCycleCountEntryId;
    private String mLotNumber;
    private String mCountListSequence;
    private String mAdjustmentAmount;
    private String mCostGroupId;
    private String mCountQuantity;
    private String mDueDate;
    private String mPriorCountQuantity;
    private String mSnapShotQuantity;

    /*************************************************************************
     *   NAME: public class XXWCCycleCount
     *
     *   PURPOSE:   Main class for XXWCCycleCount
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150302-00173 - RF - Cyclecount
     **************************************************************************/

    public XXWCCycleCount() {
        //    1   2   3   4   5   6   7   8   9   0   1   2   3   4   5   6   7   8   9   0   1   2   3   4
        this("", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
        
    }
    
    
    /*************************************************************************
     *   NAME: public class XXWCCycleCount
     *
     *   PURPOSE:   Main class for XXWCCycleCount
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150302-00173 - RF - Cyclecount
     **************************************************************************/

    //                                              1                          2                             3                           4                     5                    6                            7               8                     9                    0               1               2               3               4              5               6                    7             
    public XXWCCycleCount(String        p_inventory_item_id	,
                          String	p_concatenated_segments	,
                          String        p_primary_uom_code      ,
                          String	p_description	,
                          String	p_lot_control_code	,
                          String	p_serial_number_control_code	,
                          String	p_shelf_life_days	,
                          String	p_min_minmap_quantity	,
                          String	p_map_minmap_quantity	,
                          String	p_sv	,
                          String	p_wc_count_list_sequence	,
                          String	p_subinventory	,
                          String	p_stock_locator	,
                          String	p_wc_stock_locator	,
                          String	p_inventory_location_id	,
                          String	p_cycle_count_entry_id	,
                          String	p_lot_number	,
                          String	p_count_list_sequence	,
                          String	p_adjustment_amount	,
                          String	p_cost_group_id	,
                          String	p_count_quantity,
                          String        p_due_date,
                          String        p_prior_count_quantity,
                          String        p_snap_shot_quantity)
    {
        
        mInventoryItemId = p_inventory_item_id;
        mItemNumber = p_concatenated_segments;
        mItemDescription = p_description;
        mLotControlCode = p_lot_control_code;
        mSerialControlCode = p_serial_number_control_code;
        mPrimaryUOMCode = p_primary_uom_code;
        mShelfLifeDays = p_shelf_life_days;
        mMinQty = p_min_minmap_quantity;
        mMaxQty = p_map_minmap_quantity;
        mSalesVelocity = p_sv;
        mWCCountListSequence = p_wc_count_list_sequence;
        mSubinventory = p_subinventory;
        mBin = p_stock_locator;
        mWCBin = p_wc_stock_locator;
        mInventoryLocationId = p_inventory_location_id;
        mCycleCountEntryId = p_cycle_count_entry_id;
        mLotNumber = p_lot_number;
        mCountListSequence = p_count_list_sequence;
        mAdjustmentAmount = p_adjustment_amount;
        mCostGroupId = p_cost_group_id;
        mCountQuantity = p_count_quantity;
        mDueDate = p_due_date;
        mPriorCountQuantity = p_prior_count_quantity;
        mSnapShotQuantity = p_snap_shot_quantity;
        
    }

    /*************************************************************************
     *   NAME: public String getmInventoryItemId() 
     *
     *   PURPOSE:   returns the Printer Name 
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       01-JUN-2015   Lee Spitzer             Initial Version -
     **************************************************************************/

    public String getmInventoryItemId() {
        return mInventoryItemId;
    }
      
     /*************************************************************************
     *   NAME: public String getmItemNumber()
     *
     *   PURPOSE:   returns the Printer Description
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       01-JUN-2015   Lee Spitzer             Initial Version -
     **************************************************************************/
           
    public String getmItemNumber() {
        return mItemNumber;
    }

    /*************************************************************************
    *   NAME: public String getmItemDescription()
    *
    *   PURPOSE:   returns the Printer Description
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       01-JUN-2015   Lee Spitzer             Initial Version -
    **************************************************************************/
          
    public String getmItemDescription() {
       return mItemDescription;
    }


    /*************************************************************************
     *   NAME: public String getmLotControlCode() 
     *
     *   PURPOSE:   returns the Printer Name 
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       01-JUN-2015   Lee Spitzer             Initial Version -
     **************************************************************************/

    public String getmLotControlCode() {
        return mLotControlCode;
    }

    /*************************************************************************
     *   NAME: public String getmSerialControlCode() 
     *
     *   PURPOSE:   returns the Printer Name 
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       01-JUN-2015   Lee Spitzer             Initial Version -
     **************************************************************************/

    public String getmSerialControlCode() {
        return mSerialControlCode;
    }
    
    /*************************************************************************
     *   NAME: public String getmPrimaryUOMCode() 
     *
     *   PURPOSE:   returns the Printer Name 
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       01-JUN-2015   Lee Spitzer             Initial Version -
     **************************************************************************/

    public String getmPrimaryUOMCode() {
        return mPrimaryUOMCode;
    }


    /*************************************************************************
     *   NAME: public String getmShelfLifeDays() 
     *
     *   PURPOSE:   returns the mShelfLifeDays
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       01-JUN-2015   Lee Spitzer             Initial Version -
     **************************************************************************/

    public String getmShelfLifeDays () {
        return mShelfLifeDays;
    }

    /*************************************************************************
     *   NAME: public String getmMinQty() 
     *
     *   PURPOSE:   returns the Printer Name 
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       01-JUN-2015   Lee Spitzer             Initial Version -
     **************************************************************************/

    public String getmMinQty() {
        return mMinQty;
    }

    /*************************************************************************
     *   NAME: public String getmMinQty() 
     *
     *   PURPOSE:   returns the Printer Name 
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       01-JUN-2015   Lee Spitzer             Initial Version -
     **************************************************************************/

    public String getmMaxQty() {
        return mMaxQty;
    }
    
    /*************************************************************************
     *   NAME: public String getmSalesVelocity() 
     *
     *   PURPOSE:   returns the Printer Name 
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       01-JUN-2015   Lee Spitzer             Initial Version -
     **************************************************************************/

    public String getmSalesVelocity() {
        return mSalesVelocity;
    }
    
    /*************************************************************************
    *   NAME: public getmWCCountListSequence
    *
    *   PURPOSE:  gets the mWCCountListSequence
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       01-JUN-2015   Lee Spitzer             Initial Version -
    **************************************************************************/
    
     public String getmWCCountListSequence() {
         return mWCCountListSequence;
     }

    /*************************************************************************
    *   NAME: public getmSubinventory
    *
    *   PURPOSE:  gets the mSubinventory
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       01-JUN-2015   Lee Spitzer             Initial Version -
    **************************************************************************/
    
     public String getmSubinventory() {
         return mSubinventory;
     }
    
    /*************************************************************************
    *   NAME: public getmBin
    *
    *   PURPOSE:  gets the mBin
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       01-JUN-2015   Lee Spitzer             Initial Version -
    **************************************************************************/
    
     public String getmBin() {
         return mBin;
     }
    
    /*************************************************************************
    *   NAME: public getmWCBin
    *
    *   PURPOSE:  gets the mBin
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       01-JUN-2015   Lee Spitzer             Initial Version -
    **************************************************************************/
    
     public String getmWCBin() {
         return mWCBin;
     }

    /*************************************************************************
    *   NAME: public getmInventoryLocationId
    *
    *   PURPOSE:  gets the mInventoryLocationId
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       01-JUN-2015   Lee Spitzer             Initial Version -
    **************************************************************************/
    
     public String getmInventoryLocationId() {
         return mInventoryLocationId;
     }    

    /*************************************************************************
    *   NAME: public getmCycleCountEntryId
    *
    *   PURPOSE:  gets the mCycleCountEntryId
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       01-JUN-2015   Lee Spitzer             Initial Version -
    **************************************************************************/
    
     public String getmCycleCountEntryId() {
         return mCycleCountEntryId;
     }   
    

    /*************************************************************************
    *   NAME: public getmLotNumber
    *
    *   PURPOSE:  gets the mLotNumber
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       01-JUN-2015   Lee Spitzer             Initial Version -
    **************************************************************************/
    
     public String getmLotNumber() {
         return mLotNumber;
     }   
    
    /*************************************************************************
    *   NAME: public getmCountListSequence
    *
    *   PURPOSE:  gets the mCountListSequence
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       01-JUN-2015   Lee Spitzer             Initial Version -
    **************************************************************************/
    
     public String getmCountListSequence() {
         return mCountListSequence;
     }

    
    /*************************************************************************
    *   NAME: public getmCostGroupId()
    *
    *   PURPOSE:  gets the mCostGroupId
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       01-JUN-2015   Lee Spitzer             Initial Version -
    **************************************************************************/
    
     public String getmCostGroupId() {
         return mCostGroupId;
     } 
    
    /*************************************************************************
    *   NAME: public getmAdjustmentAmount
    *
    *   PURPOSE:  gets the mAdjustmentAmount
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       01-JUN-2015   Lee Spitzer             Initial Version -
    **************************************************************************/
    
     public String getmAdjustmentAmount() {
         return mAdjustmentAmount;
     } 
    

    /*************************************************************************
    *   NAME: public getmCountQuantity
    *
    *   PURPOSE:  gets the mCountQuantity
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       01-JUN-2015   Lee Spitzer             Initial Version -
    **************************************************************************/
    
    public String getmCountQuantity() {
        return mCountQuantity;
    }


    /*************************************************************************
    *   NAME: public getmDueDate
    *
    *   PURPOSE:  gets the mDueDate
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       01-JUN-2015   Lee Spitzer             Initial Version -
    **************************************************************************/
    
    public String getmDueDate() {
        return mDueDate;
    }

    /*************************************************************************
    *   NAME: public getmPriorCountQuantity
    *
    *   PURPOSE:  gets the mPriorCountQuantity
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       01-JUN-2015   Lee Spitzer             Initial Version -
    **************************************************************************/
    
    public String getmPriorCountQuantity() {
        return mPriorCountQuantity;
    }

    /*************************************************************************
    *   NAME: public getmSnapShotQuantity
    *
    *   PURPOSE:  gets the mSnapShotQuantity
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       01-JUN-2015   Lee Spitzer             Initial Version -
    **************************************************************************/
    
    public String getmSnapShotQuantity() {
        return mSnapShotQuantity;
    }
    
}
