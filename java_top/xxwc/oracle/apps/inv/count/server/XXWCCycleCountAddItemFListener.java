/*************************************************************************
   *   $Header XXWCCycleCountAddItemFListener.java $
   *   Module Name: XXWCCycleCountAddItemFListener
   *   
   *   Package: package xxwc.oracle.apps.inv.count.server;
   *   
   *   Imports Classes:
   *   
   *   import java.sql.*;
   *   import oracle.apps.inv.utilities.server.UtilFns;
   *   import oracle.apps.mwa.beans.*;
   *   import oracle.apps.mwa.container.MWALib;
   *   import oracle.apps.mwa.container.Session;
   *   import oracle.apps.mwa.eventmodel.*;
   *   import oracle.apps.mwa.container.FileLogger;
   *   import oracle.apps.fnd.flexj.FlexException;
   *   import oracle.apps.inv.lov.server.ItemLOV;
   *   import oracle.apps.inv.lov.server.LocatorKFF;
   *   import oracle.apps.inv.lov.server.SubinventoryLOV;
   *   import xxwc.oracle.apps.inv.lov.server.*;
   *
   *   PURPOSE:   Java Class for XXWC Cycle Count Page Layout.
   *    
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
        1.0       01-JUN-2015   Lee Spitzer             Initial Version - 
                                                            TMS Ticket Number 20150302-00173 - RF - Cyclecount
**************************************************************************/
package xxwc.oracle.apps.inv.count.server;

import java.sql.*;
import java.util.Hashtable;
import java.util.Vector;
import oracle.apps.fnd.common.VersionInfo;
import oracle.apps.inv.lov.server.*;
import oracle.apps.inv.utilities.server.*;
import oracle.apps.mwa.beans.*;
import oracle.apps.mwa.container.FileLogger;
import oracle.apps.mwa.container.Session;
import oracle.apps.mwa.eventmodel.*;
import oracle.apps.mwa.presentation.telnet.TelnetSession;
import oracle.jdbc.OraclePreparedStatement;
import oracle.sql.NUMBER;
import oracle.apps.mwa.container.MWALib;
import oracle.apps.fnd.flexj.KeyFlexfield;

import xxwc.oracle.apps.inv.count.server.XXWCCycleCount;
import xxwc.oracle.apps.inv.count.server.XXWCCycleCountIterator;
import xxwc.oracle.apps.inv.count.server.XXWCCycleCountQueryManager;
import xxwc.oracle.apps.inv.lov.server.*;
import xxwc.oracle.apps.inv.rcv.server.XXWCRcvGenPage;

/*************************************************************************
 *   NAME: XXWCCycleCountFListener implements MWAFieldListener
 *
 *   PURPOSE:   Main class for XXWCCycleCountFListener
 *
 *   REVISIONS:
 *   Ver        Date        Author                     Description
 *   ---------  ----------  ---------------         -------------------------
        1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                            TMS Ticket Number 20150302-00173 - RF - Cyclecount
 **************************************************************************/


public class XXWCCycleCountAddItemFListener implements MWAFieldListener {
    XXWCCycleCountAddItemPage pg;
    Session ses;
    String dialogPageButtons[];
    String g_exit_page = "N";
    String g_add_item = "N";
    
    /*************************************************************************
     *   NAME: private static String gPackage = "xxwc.oracle.apps.inv.count.server";
     *
     *   PURPOSE:   private method to pass gPackage from methods to return class
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150302-00173 - RF - Cyclecount
     **************************************************************************/
    
    private static String gPackage = "xxwc.oracle.apps.inv.count.server";


    /*************************************************************************
     *   NAME: private static String gCallFrom = "XXWCCycleCountAddItemFListener";
     *
     *   PURPOSE:   private method to pass gCallFrom from methods to return class
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                 TMS Ticket Number 20150302-00173 - RF - Cyclecount

     **************************************************************************/

    private static String gCallFrom = "XXWCCycleCountAddItemFListener";

    /*************************************************************************
     *   NAME: public XXWCCycleCounttFListener()
     *
     *   PURPOSE:   public method XXWCCycleCountFListener
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                 TMS Ticket Number 20150302-00173 - RF - Cyclecount

     **************************************************************************/

    public XXWCCycleCountAddItemFListener() {
        $init$();
    }

    /*************************************************************************
     *   NAME:  public void fieldEntered(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                       DefaultOnlyHandlerException
     *
     *   PURPOSE:   field entered for XXWCPrimaryBinAssignmentPage
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                 TMS Ticket Number 20150302-00173 - RF - Cyclecount

     **************************************************************************/
    
    public void fieldEntered(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                       DefaultOnlyHandlerException {
        ses = mwaevent.getSession();
        pg = (XXWCCycleCountAddItemPage)ses.getCurrentPage();
        String s = UtilFns.fieldEnterSource(ses);
        

        /*Item*/
        if (s.equals("XXWC.ADD_ITEM")) {
            enteredItem(mwaevent);
            return;
        }
        if (s.equals("XXWC.ADD_LOC")) {
            enteredLocator(mwaevent);
        }
        /*Sub*/
           /*Default For*/
        if (s.equals("XXWC.ADD_SUB")) {
            enteredSub(mwaevent);
        }
        if (s.equals("XXWC.ADD_QTY")) {
            return;
        }

    }

    /*************************************************************************
     *   NAME:      public void fieldExited(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException
     *
     *   PURPOSE:   field exited for XXWCPrimaryBinAssignmentPage
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                 TMS Ticket Number 20150302-00173 - RF - Cyclecount

     **************************************************************************/

    public void fieldExited(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException {
        String s = ((FieldBean)mwaevent.getSource()).getName();
        String vField = mwaevent.getAction();
        boolean flag = false;
        boolean flag1 = false;
        boolean flag3 = false;
        if (mwaevent.getAction().equals("MWA_SUBMIT")) {
            flag = true;
        } else if (mwaevent.getAction().equals("MWA_PREVIOUSFIELD")) {
            flag1 = true;
        } else if (mwaevent.getAction().equals("MWA_NEXTFIELD")) {
            flag3 = true;
        }
        if (s.equals("XXWC.ADD_ITEM")) {
            exitedItem(mwaevent, ses);
            return;
        }
        if (s.equals("XXWC.ADD_SUB")) {
            exitedSub(mwaevent, ses);
            return;
        }
        if (s.equals("XXWC.ADD_LOC")) {
            exitedLocator(mwaevent, ses);
        }
        if (s.equals("XXWC.ADD_QTY")) {
            exitedQty(mwaevent, ses);
            return;
        }
        if (flag && s.equals("XXWC.ADD_BACK")) {
            exitedMenu(mwaevent, ses);
            return;
        }
        if (flag && s.equals("XXWC.ADD_SAVE")) {
            processCCEntriesTable(mwaevent, ses);
            exitedSave(mwaevent, ses);
            return;
        }
        /*Done*/
         else {
            return;
        }

    }

    /*************************************************************************
     *   NAME:      private void $init$()
     *
     *   PURPOSE:   $init$ sets the diaglogButtons button for user prompt messages to OK
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150302-00173 - RF - Cyclecount
     **************************************************************************/

    private void $init$() {
        dialogPageButtons = (new String[] { "OK" });
    }


    /*************************************************************************
     *   NAME:      public void enteredItem(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException
     *
     *   PURPOSE:   Item List of values for cycle count page
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150302-00173 - RF - Cyclecount
     **************************************************************************/



    public void enteredItem(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException {

        
        pg.getmCycleCountHeaderFld().setHidden(true);
        ses.setRefreshScreen(true);
        
        try {
            XXWCItemLOV itemlov = pg.getmItemFld();
            itemlov.setValidateFromLOV(true);
            itemlov.setlovStatement("XXWC_MWA_ROUTINES_PKG.GET_WC_ITEM_LOV");
            //                      0    1    2    
            String paramType[] = { "C", "N", "S"};
            //                                     0       1       2      3              4                   5                      6                  7 
            String prompts[] = { "INVENTORY_ITEM_ID" , "ITEM", "UOM", "DESCRIPTION", "LOT_CONTROL_CODE", "SERIAL_CONTROL_CODE", "SHELF_LIFE_DAYS", "QTY"};
            //                    0     1     2     3     4     5       6    7     
            boolean flag[] = {false, true, true, true, false, false, true, true};
            //                        0        1                                                              2 
            String parameters[] = { " ", "ORGID", "xxwc.oracle.apps.inv.count.server.XXWCCycleCountAddItemPage.XXWC.ADD_ITEM"};
            itemlov.setInputParameterTypes(paramType);
            itemlov.setInputParameters(parameters);
        } catch (Exception e) {
            UtilFns.error("Error in calling enteredItem" + e);
        }
    }

    /*************************************************************************
     *   NAME: public void exitedItem(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException
     *
     *   PURPOSE:  Exited Item Field Listener to populate the Item Description Field
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150302-00173 - RF - Cyclecount
     **************************************************************************/

    public void exitedItem(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                  DefaultOnlyHandlerException {
        try {
            
            if (pg.getmItemFld().getValue().equals(null) || pg.getmItemFld().getValue().equals("") ) {
                //pg.getmCountQtyFld().setEditable(false);
                pg.getmItemDescFld().setHidden(true);
                pg.getmSubFld().setHidden(true);
                pg.getmUOMFld().setHidden(true);
            }
            else {
                
                Integer l_item_open_count = getItemOpenCount(mwaevent, ses);
                
                if (l_item_open_count > 0) {
                  TelnetSession telnetsessionX1 = (TelnetSession)ses;
                  Integer k1 =
                  telnetsessionX1.showPromptPage("Error!", "Item " + pg.getmItemFld().getValue() + " is already on the count.  Please select another item to add to the count.",
                                                          dialogPageButtons);
                      if (k1 == 0) {
                          pg.getmItemFld().clear();
                          ses.setNextFieldName("XXWC.ADD_ITEM");
                      }
                }
                else {                
                    pg.getmItemFld().getValue().toUpperCase().trim();
                    pg.getmItemDescFld().setValue(pg.getmItemFld().getmItemDescription());
                    pg.getmUOMFld().setValue(pg.getmItemFld().getmPrimaryUOMCode());
                    pg.getmItemDescFld().setHidden(false);
                    pg.getmSubFld().setHidden(false);
                    pg.getmUOMFld().setHidden(false);
                    
                    if (pg.getmSubFld().getValue().equals(null) || pg.getmSubFld().getValue().equals("") ) {
                        pg.getmSubFld().setValue("General");
                    }
                    
                    ses.setNextFieldName("XXWC.ADD_SUB");
                }
            }
    
    }
    catch (Exception e) {
            UtilFns.error("Error in calling exitedItem" + e);
        }
    }



    /*************************************************************************
    *   NAME: public void enteredBins(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                     DefaultOnlyHandlerException
    *
    *   PURPOSE:  Exited Clear For Field Listener Process to proces the Clear Button
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
             1.0       01-JUN-2015   Lee Spitzer             Initial Version -
    **************************************************************************/


    public void enteredBins(MWAEvent mwaevent)
         throws AbortHandlerException, InterruptedHandlerException, DefaultOnlyHandlerException
     {
         pg.getmCycleCountHeaderFld().setHidden(true);
         ses.setRefreshScreen(true);
         
         try
         {
             //ses.putSessionObject("sessInventoryItemId", "");
             ses.putSessionObject("sessRestrictLocator", Integer.valueOf(1));
             ses.putSessionObject("sessHidePrefix", "Y");
             XXWCBinsLOV binsLOV = pg.getmLocFld();
             String paramType[] = {
                 "C", "N", "S", "N", "N", "S", "S", "S", "S", "S", "S"
             };
             String prompts[] = {
                 "WC_LOCATOR", "PREFIX", "LOCATOR_ID", "LOCATOR", "DESCRIPTION", "SUBINVENTORY"
             };
             boolean flag[] = {
                 true, false, false, false, false, false
             };
             String parameters[] = {
                 //0        1   2                      3   4                                                                          5   6   7                 8                                                                                     9                          0
                 " ", "ORGID", "", "sessRestrictLocator", "", "xxwc.oracle.apps.inv.count.server.XXWCCycleCountAddItemPage.XXWC.ADD_LOC", "", "", "sessHidePrefix", "xxwc.oracle.apps.inv.count.server.XXWCCycleCountAddItemPage.XXWC.ADD_CYCLECOUNT_HEADER", "TXN.ENTRY_STATUS_CODE"
             };
             binsLOV.setInputParameters(parameters);
             binsLOV.setlovStatement("XXWC_CC_MOBILE_PKG.GET_BIN_LOC");
             binsLOV.setInputParameterTypes(paramType);
             binsLOV.setSubfieldPrompts(prompts);
             binsLOV.setSubfieldDisplays(flag);
         }
         catch(Exception e)
         {
             UtilFns.error((new StringBuilder()).append("Error in calling enteredItem ").append(e).toString());
         }
     }
    

    /*************************************************************************
    *   NAME: public void enteredSub(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                     DefaultOnlyHandlerException
    *
    *   PURPOSE:  Exited Clear For Field Listener Process to proces the Clear Button
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
             1.0       01-JUN-2015   Lee Spitzer             Initial Version -
    **************************************************************************/


    public void enteredSub(MWAEvent mwaevent)
         throws AbortHandlerException, InterruptedHandlerException, DefaultOnlyHandlerException
     {
         try {
             XXWCSubLOV subLOV = pg.getmSubFld();
             subLOV.setValidateFromLOV(true);
             subLOV.setlovStatement("XXWC_MWA_ROUTINES_PKG.GET_WC_SUB_LOV");
             String paramType[] = { "C", "N", "S", "S"};
             String prompts[] = { "SUBINVENTORY_CODE" , "DESCRIPTION", "LOCATOR_TYPE", "ASSET_INVENTORY"};
             boolean flag[] = {true, true, false, false};
             String parameters[] = { " ", "ORGID", "xxwc.oracle.apps.inv.count.server.XXWCCycleCountAddItemPage.XXWC.ADD_SUB", ""};
             subLOV.setInputParameterTypes(paramType);
             subLOV.setInputParameters(parameters);
             subLOV.setSubfieldPrompts(prompts);
             subLOV.setSubfieldDisplays(flag);

         } catch (Exception e) {
             UtilFns.error("Error in calling enteredSub" + e);
         }   
     }


    /*************************************************************************
     *   NAME: public void exitedSub(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException
     *
     *   PURPOSE:  Exited Item Field Listener to populate the Item Description Field
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
     **************************************************************************/

    public void exitedSub(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                  DefaultOnlyHandlerException {
        try {
            
            if (pg.getmSubFld().getValue().equals(null) || pg.getmSubFld().getValue().equals("") ) {
                pg.getmSubFld().setHidden(true);
                pg.getmLocFld().setHidden(true);
                pg.getmItemDescFld().setHidden(true);
                pg.getmUOMFld().setHidden(true);
                pg.getmCountQtyFld().setHidden(true);
                ses.setNextFieldName("XXWC.ADD_ITEM");
            }
            else {
                
                try{
                    Integer i = getLocatorControl(mwaevent, ses);
                    FileLogger.getSystemLogger().trace("Locator Control returned from getLocatorControl " + i);
                    if (i == 1) {
                        FileLogger.getSystemLogger().trace("Locator control is not enabled");
                
                            pg.getmUOMFld().setHidden(false);
                            pg.getmCountQtyFld().setHidden(false);
                            ses.setNextFieldName("XXWC.ADD_QTY");
                    } else {
                        FileLogger.getSystemLogger().trace("Locator control is enabled");
                        pg.getmLocFld().setHidden(false);
                        pg.getmLocFld().setRequired(true);
                        pg.getmLocFld().setEditable(true);
                        ses.setNextFieldName("XXWC.ADD_LOC");
                    }
                }
            catch (Exception e) {
                FileLogger.getSystemLogger().trace("Error in calling exitedSub " + e);
                UtilFns.error("Error in calling exitedSub" + e);
            }
            }
        }
        catch (Exception e) {
                UtilFns.error("Error in calling exitedSub" + e);
        }
    }


    /*************************************************************************
    *   NAME: public void getLocatorControl(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                     DefaultOnlyHandlerException
    *
    *   PURPOSE:  Determines if Subinventory has Locator Control turned on or off
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
         1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                            TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
    **************************************************************************/

    private int getLocatorControl(MWAEvent mwavevent, Session session) throws AbortHandlerException,
                                                                              InterruptedHandlerException,
                                                                              DefaultOnlyHandlerException {
        int i = 1;
        int j = 1;
        int k = 1;
        try {
            Long orgid = Long.parseLong((String)session.getObject("ORGID"));
            FileLogger.getSystemLogger().trace("orgid  " + orgid);
            FileLogger.getSystemLogger().trace("i  " + i);
            OrgParameters orgparameters = OrgParameters.getOrgParameters(session.getConnection(), orgid);
            i = orgparameters.getLocatorControlCode();
            FileLogger.getSystemLogger().trace("i  " + i);
            FileLogger.getSystemLogger().trace("j " + j);
            XXWCSubLOV sublov = pg.getmSubFld();
            if (pg.getmSubFld().getValue() != null | pg.getmSubFld().getValue() != "") {
                j = Integer.parseInt(sublov.getmLocatorType());
            }
            FileLogger.getSystemLogger().trace("j " + j);
            XXWCItemLOV itemlov = pg.getmItemFld();
            FileLogger.getSystemLogger().trace("k " + k);
            if (pg.getmItemFld().getValue() != null | pg.getmItemFld().getValue() != "") {
                k = Integer.parseInt(pg.getmItemFld().getmLotControlCode());
            }
            FileLogger.getSystemLogger().trace("k " + k);
            if (UtilFns.isTraceOn) {
                UtilFns.trace((new StringBuilder()).append("RCV: getLocatorControl orgLocControl:").append(i).append(":subLocControl:").append(j).append(":itemLocControl:").append(k).toString());
            }
        } catch (Exception e) {
            FileLogger.getSystemLogger().trace("error getLocatorControl " + e);
        }
      //  return pg.getmLocFld().getLocatorControl(i, j, k);
      //if Subinventory Control is Locator Contorl or Item Numberis Locator control then return 1 else return 0
      if (j > 1 || k > 1) {
          return 0;
      }
      else {
          return 1;
      }
    }
    
    
    /*************************************************************************
     *   NAME: public void exitedQty(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException
     *
     *   PURPOSE:  Exited Item Field Listener to populate the Item Description Field
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150302-00173 - RF - Cyclecount
     **************************************************************************/

    
    public void exitedQty(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException, DefaultOnlyHandlerException
        {
            String gMethod = "exitedQty";
            String gCallPoint = "INV.QTY start";
            
            if (pg.getmCountQtyFld().getValue().equals(null) || pg.getmCountQtyFld().getValue().equals("") ) {
            
                Integer i = getLocatorControl(mwaevent, ses);
                FileLogger.getSystemLogger().trace("Locator Control returned from getLocatorControl " + i);
                if (i == 1) {
                    ses.setNextFieldName("XXWC.ADD_SUB");
                }
                else {
                    ses.setNextFieldName("XXWC.ADD_LOC");
                }
                
                pg.getmUOMFld().setHidden(true);
                pg.getmCountQtyFld().setHidden(true);
                pg.getmSave().setHidden(true);
             }
            
            else {
                Double d;
                String l_quantity = pg.getmCountQtyFld().getValue();
                FileLogger.getSystemLogger().trace(gMethod + " " + gCallPoint + " " + "l_quantity " + l_quantity);
                String exception_message = "";
                if (!l_quantity.isEmpty()) {
                        try {
                            d = Double.valueOf(pg.getmCountQtyFld().getValue());
                            if (d < 0) {
                                        exception_message = "Number is not positive";
                                           throw new AbortHandlerException(exception_message);
                                }
                            }
                         catch (Exception exception) {
                            gCallPoint = "Number value check";
                            UtilFns.error(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " " + exception);
                            pg.mSave.setHidden(true);
                            //pg.mDone.setHidden(true);
                            if (exception_message == "") {
                                ses.setStatusMessage(UtilFns.getMessage(ses, "INV", "INV_MWA_NUMBER_EXPECTED"));
                            }
                            else  {
                                ses.setStatusMessage(exception_message);
                            }    
                            throw new AbortHandlerException((new StringBuilder()).append(gMethod + " "  + gCallPoint).append(exception).toString());
                        }
                        pg.mSave.setHidden(false);
                        //pg.mDone.setHidden(false);
                }
                else {
                    pg.mSave.setHidden(true);
                    //pg.mDone.setHidden(true);
                }
                
            }

        }

    
    /*************************************************************************
     *   NAME: public void processCCEntriesTable(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                 DefaultOnlyHandlerException
     *
     *   PURPOSE:  Exited Item Field Listener to populate the Item Description Field
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150302-00173 - RF - Cyclecount
     **************************************************************************/

    
    public void processCCEntriesTable(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException, DefaultOnlyHandlerException
        {
            String gMethod = "processCCEntriesTable";
            String gCallPoint = "start";
            String p_cycle_count_header_id;
            String p_cycle_count_entry_id;
            String p_wc_count_list_sequence;
            String p_count_list_sequence; 
            String p_organization_id; 
            String p_inventory_item_id; 
            String p_revision; 
            String p_quantity;
            String p_primary_uom_code;
            String p_subinventory; 
            String p_locator_id;
            String p_lot_number;
            String p_cost_group_id;
            String p_user_id;
            Integer x_return;
            String x_message;
            
            Integer l_cost_group_id = getCostGroupId(mwaevent, ses);
            
            p_cycle_count_header_id = pg.getmCycleCountHeaderFld().getValue();
            p_cycle_count_entry_id = ""; //pg.getmCycleCountEntryId().getValue();
            p_wc_count_list_sequence = ""; // l_cycle_count_sequence.toString();//pg.getmWCCycleCountSequence().getValue();
            p_count_list_sequence = ""; //l_cycle_count_sequence.toString(); //pg.getmCycleCountSequence().getValue();
            p_organization_id = (String)ses.getObject("ORGID");
            p_inventory_item_id = pg.getmItemFld().getmInventoryItemId();
            p_revision = "";
            p_quantity = pg.getmCountQtyFld().getValue();
            p_primary_uom_code = pg.getmUOMFld().getValue();
            p_subinventory = pg.getmSubFld().getValue();
            p_locator_id = pg.getmLocFld().getValue();
            p_lot_number = "";//pg.getm .getValue();
            p_cost_group_id = l_cost_group_id.toString() ;//pg.getmCostGroupId().getValue();
            p_user_id = (String)ses.getObject("USERID");
            x_return = -1;
            x_message = "";
            
            try {
                  CallableStatement cstmt = null;
                  Connection con = ses.getConnection();
                  //                                                                       1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6    
                  cstmt = con.prepareCall("{call XXWC_CC_MOBILE_PKG.PROCESS_CC_ENTRIES_TBL(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
                  cstmt.setString(1, p_cycle_count_header_id);
                  cstmt.setString(2, p_cycle_count_entry_id);
                  cstmt.setString(3, p_wc_count_list_sequence);
                  cstmt.setString(4, p_count_list_sequence);
                  cstmt.setString(5, p_organization_id);
                  cstmt.setString(6, p_inventory_item_id);
                  cstmt.setString(7, p_revision);
                  cstmt.setString(8, p_quantity);
                  cstmt.setString(9, p_primary_uom_code);
                  cstmt.setString(10, p_subinventory);
                  cstmt.setString(11, p_locator_id);
                  cstmt.setString(12, p_lot_number);
                  cstmt.setString(13, p_cost_group_id);
                  cstmt.setString(14, p_user_id);
                  cstmt.registerOutParameter(15, Types.INTEGER); //x_return
                  cstmt.registerOutParameter(16, Types.VARCHAR); //x_message
                  cstmt.execute();
                  x_return = cstmt.getInt(15);
                  x_message = cstmt.getString(16);
                  cstmt.close();
                  boolean showSubError = true;
                  if (x_return > 1) {
                    TelnetSession telnetsessionX = (TelnetSession)ses;
                    Integer k =
                    telnetsessionX.showPromptPage("Error!", x_message,
                                                            dialogPageButtons);
                        if (k == 0) {
                            pg.getmCountQtyFld().setValue(null);
                        }
                  }
            }
            catch (Exception e) {
                UtilFns.error("Error in validating " + gMethod + " " + e);
            }
                                            
        }
    
    /*************************************************************************
    *   NAME: public void enteredLocator(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                     DefaultOnlyHandlerException
    *
    *   PURPOSE:  Entered Locator Field Listener Process to set the List of Values for the Locator Field
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
         1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                            TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions

    **************************************************************************/

    public void enteredLocator(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                         DefaultOnlyHandlerException {

            
        try {
            ses.putSessionObject("sessInventoryItemId", pg.getmItemFld().getmInventoryItemId());
            ses.putSessionObject("sessRestrictLocator", 2);
            ses.putSessionObject("sessHidePrefix","N");
            XXWCBinsLOV binsLOV = pg.getmLocFld();
            //                      0    1    2    3    4    5    6    7    8
            String paramType[] = { "C", "N", "S", "N", "N", "S", "S", "S", "S" };
            String prompts[] = { "WC_LOCATOR", "PREFIX", "LOCATOR_ID", "LOCATOR", "DESCRIPTION", "SUBINVENTORY" };
            boolean flag[] = {false, false, false, true, false, false};
            //                        0        1                                                              2                      3                      4                                                                         5   6   7                8 
            String parameters[] = { " ", "ORGID", "xxwc.oracle.apps.inv.count.server.XXWCCycleCountAddItemPage.XXWC.ADD_SUB" ,"sessRestrictLocator", "sessInventoryItemId" , "xxwc.oracle.apps.inv.count.server.XXWCCycleCountAddItemPage.XXWC.ADD_LOC", "", "", "sessHidePrefix" };
            //String parameters[] = { " ", "ORGID", "" ,"", "sessInventoryItemId" , "xxwc.oracle.apps.inv.lables.server.XXWCItemLabelPage.XXWC.BIN", "", "", "N" };
            binsLOV.setInputParameters(parameters);
            binsLOV.setlovStatement("XXWC_LABEL_PRINTERS_PKG.GET_BIN_LOC");
            binsLOV.setInputParameterTypes(paramType);
            binsLOV.setSubfieldPrompts(prompts);
            binsLOV.setSubfieldDisplays(flag);  
            
        } catch (Exception e) {
            UtilFns.error("Error in calling enteredItem " + e);
        }
        
    }


    /*************************************************************************
     *   NAME: public void exitedLocator(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException
     *
     *   PURPOSE:  Exited Item Field Listener to populate the Item Description Field
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
     **************************************************************************/

    public void exitedLocator(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                  DefaultOnlyHandlerException {
        try {
            
            if (pg.getmLocFld().getValue().equals(null) || pg.getmLocFld().getValue().equals("") ) {
                pg.getmLocFld().setHidden(true);
                pg.getmUOMFld().setHidden(true);
                pg.getmCountQtyFld().setHidden(true);
                
            }
            else {
                    
                        
                    pg.getmUOMFld().setHidden(false);
                    pg.getmCountQtyFld().setHidden(false);
                    ses.setNextFieldName("XXWC.ADD_QTY");
                }
                
            }
        catch (Exception e) {
                UtilFns.error("Error in calling exitedLocator" + e);
        }
    }

    /*************************************************************************
    *   NAME:     public void getOpenCountQty(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                                 InterruptedHandlerException,
                                                                                 DefaultOnlyHandlerException
    *
    *   PURPOSE:  Clears the values in the form
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
         1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                            TMS Ticket Number 20150302-00173 - RF - Cyclecount

    **************************************************************************/
    
    protected Integer getCostGroupId(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                        DefaultOnlyHandlerException {
            String gMethod = "getCostGroupId";
            String gCallPoint = "Start";
            int x_return = 0;
            
            try
            {
                CallableStatement cstmt = null;
                Connection con = ses.getConnection();
                cstmt = con.prepareCall("{? = call XXWC_CC_MOBILE_PKG.GET_COST_GROUP_ID(?)");
                cstmt.registerOutParameter(1, Types.NUMERIC);
                cstmt.setString(2, (String)ses.getObject("ORGID"));
                cstmt.execute();
                x_return = cstmt.getInt(1);
                cstmt.close();
            }
            catch(Exception e)
            {
                UtilFns.error((new StringBuilder()).append("Error in validating getCostGroupId").append(e).toString());

            }
            
            return x_return;
        }

    /*************************************************************************
    *   NAME:     public void getOpenCountQty(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                                 InterruptedHandlerException,
                                                                                 DefaultOnlyHandlerException
    *
    *   PURPOSE:  Clears the values in the form
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
         1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                            TMS Ticket Number 20150302-00173 - RF - Cyclecount

    **************************************************************************/
    
    protected Integer getItemOpenCount(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                        DefaultOnlyHandlerException {
            String gMethod = "getItemOpenCount";
            String gCallPoint = "Start";
            int x_return = 0;
            
            try
            {
                CallableStatement cstmt = null;
                Connection con = ses.getConnection();
                cstmt = con.prepareCall("{? = call XXWC_CC_MOBILE_PKG.GET_ITEM_OPEN_COUNT_QTY(?,?,?)");
                cstmt.registerOutParameter(1, Types.NUMERIC);
                cstmt.setString(2, pg.getmCycleCountHeaderFld().getValue());
                cstmt.setString(3, (String)ses.getObject("ORGID"));
                cstmt.setString(4, pg.getmItemFld().getmInventoryItemId());
                cstmt.execute();
                x_return = cstmt.getInt(1);
                cstmt.close();
            }
            catch(Exception e)
            {
                UtilFns.error((new StringBuilder()).append("Error in validating getItemOpenCount").append(e).toString());

            }
            
            return x_return;
        }
    
    /*************************************************************************
    *   NAME: public void exitedSave(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                     DefaultOnlyHandlerException
    *
    *   PURPOSE:  Exited Close For Field Listener Process to proces the Close Button
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
         1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                            TMS Ticket Number 20150302-00173 - RF - Cyclecount
    **************************************************************************/

    public void exitedSave(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                   DefaultOnlyHandlerException {
        String gMethod = "exitedSave";
        String gCallPoint = "Start";
        XXWCCycleCountAddItemPage _tmp = pg;
   
        try{
            ses.setStatusMessage("Item Added to the Count");
            pg.getmSave().setNextPageName("xxwc.oracle.apps.inv.count.server.XXWCCycleCountPage");
        }
        catch (Exception exception){
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " exception " + exception);
        }
    }

    /*************************************************************************
    *   NAME: public void exitedMenu(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                     DefaultOnlyHandlerException
    *
    *   PURPOSE:  Exited Close For Field Listener Process to proces the Close Button
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
         1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                            TMS Ticket Number 20150302-00173 - RF - Cyclecount
    **************************************************************************/

    public void exitedMenu(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                   DefaultOnlyHandlerException {
        String gMethod = "exitedMenu";
        String gCallPoint = "Start";
        XXWCCycleCountAddItemPage _tmp = pg;
    
        try{
            ses.setStatusMessage(pg.WMS_TXN_CANCEL);
            pg.getmBack().setNextPageName("xxwc.oracle.apps.inv.count.server.XXWCCycleCountPage");
        }
        catch (Exception exception){
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " exception " + exception);
        }
    }
}
