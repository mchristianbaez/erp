/*************************************************************************
   *   $Header XXWCCycleCountIterator.java $
   *   Module Name: XXWCCycleCountIterator
   *   
   *   Package: package xxwc.oracle.apps.inv.count.server;
   *   
   *   Imports Classes:
   *   
   *   import java.sql.*;
   *   import oracle.apps.inv.utilities.server.UtilFns;
   *   import oracle.apps.mwa.beans.*;
   *   import oracle.apps.mwa.container.MWALib;
   *   import oracle.apps.mwa.container.Session;
   *   import oracle.apps.mwa.eventmodel.*;
   *   import oracle.apps.mwa.container.FileLogger;
   *   import oracle.apps.fnd.flexj.FlexException;
   *   import oracle.apps.inv.lov.server.ItemLOV;
   *   import oracle.apps.inv.lov.server.LocatorKFF;
   *   import oracle.apps.inv.lov.server.SubinventoryLOV;
   *   import xxwc.oracle.apps.inv.lov.server.*;
   *
   *   PURPOSE:   Java Class for XXWC Cycle Count Page Layout.
   *    
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
        1.0       01-JUN-2015   Lee Spitzer             Initial Version - 
                                                            TMS Ticket Number 20150302-00173 - RF - Cyclecount
**************************************************************************/
package xxwc.oracle.apps.inv.count.server;

import java.util.Vector;

import oracle.apps.fnd.common.VersionInfo;
import oracle.apps.inv.invinq.server.ItemOnhand;
import oracle.apps.inv.invinq.server.NextRecordException;
import oracle.apps.inv.invinq.server.PreviousRecordException;
import oracle.apps.inv.invinq.server.UnknownMaxLeft;
import oracle.apps.inv.utilities.server.UtilFns;
import oracle.apps.mwa.container.FileLogger;

import xxwc.oracle.apps.inv.invinq.server.XXWCItemOnHand;

/*************************************************************************
 *   NAME: public class XXWCCycleCountIterator
 *
 *   PURPOSE:   Main class for XXWCCycleCountIterator
 *
 *   REVISIONS:
 *   Ver        Date        Author                     Description
 *   ---------  ----------  ---------------         -------------------------
        1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                            TMS Ticket Number 20150302-00173 - RF - Cyclecount
 **************************************************************************/

public class XXWCCycleCountIterator {
    
    public static final String RCS_ID = "$Header: XXWCCycleCountIterator.java 120.0 2005/05/25 05:18:03 appldev noship $";
    public static final boolean RCS_ID_RECORDED =
        VersionInfo.recordClassVersion("$Header: XXWCCycleCountIterator.java 120.0 2005/05/25 05:18:03 appldev noship $",
                                       "xxwc.oracle.apps.inv.invinq.server");
    private Vector mWCCountListSeq;
    private int mCurrentPos;

    /*************************************************************************
     *   NAME: public class gPackage
     *
     *   PURPOSE:   private method to default gPackage value for debugging
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150302-00173 - RF - Cyclecount
     **************************************************************************/


     private static String gPackage = "xxwc.oracle.apps.inv.count.server";

    /*************************************************************************
     *   NAME: public class gCallFrom
     *
     *   PURPOSE:   private method to default gCallFrom value for debugging
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150302-00173 - RF - Cyclecount
     **************************************************************************/


     private static String gCallFrom = "XXWCCycleCountIterator";


    /*************************************************************************
     *   NAME: public XXWCCycleCountIterator(Vector vector)
     *
     *   PURPOSE:   private method to default gCallFrom value for debugging
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150302-00173 - RF - Cyclecount
     **************************************************************************/

    public XXWCCycleCountIterator(Vector vector) {
        String gMethod = "XXWCCycleCountIterator";
        String gCallPoint = "Start";
        if (vector == null) {
            gCallPoint = "vector == null";
            mWCCountListSeq = new Vector();
        } else {
            gCallPoint = "vector != null";
            mWCCountListSeq = vector;
        }
        FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
        mCurrentPos = 0;
    }

    /*************************************************************************
     *   NAME: public XXWCCycleCountIterator(Vector vector)
     *
     *   PURPOSE:   private method to default gCallFrom value for debugging
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150302-00173 - RF - Cyclecount
     **************************************************************************/

    public int max_left() throws UnknownMaxLeft {
        String gMethod = "max_left";
        String gCallPoint = "Start";
        if (mWCCountListSeq == null) {
            gCallPoint = "mWCCountListSeq == null";
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
            throw new UnknownMaxLeft();
        } else {
            gCallPoint = "mWCCountListSeq != null";
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " mWCCountListSeq.size() " + mWCCountListSeq.size());
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " mCurrentPos " +mCurrentPos);
            return mWCCountListSeq.size() - mCurrentPos;
        }
    }

    /*************************************************************************
     *   NAME: public XXWCItemOnHand next() throws NextRecordException
     *
     *   PURPOSE:   private method to default gCallFrom value for debugging
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150302-00173 - RF - Cyclecount
     **************************************************************************/

    public XXWCCycleCount next() throws NextRecordException {
        boolean flag;
        try {
            int i = max_left();
            flag = i > 0;
        } catch (UnknownMaxLeft unknownmaxleft) {
            UtilFns.log((new StringBuilder()).append("Error in mWCCountListSeq.next ").append(unknownmaxleft).toString());
            throw new NextRecordException();
        }
        XXWCCycleCount XXWCCycleCount;
        if (flag && mCurrentPos <= mWCCountListSeq.size() - 1) {
            XXWCCycleCount =
                    (XXWCCycleCount)mWCCountListSeq.elementAt(mCurrentPos++);
        } else {
            throw new NextRecordException();
        }
        return XXWCCycleCount;
    }

    /*************************************************************************
     *   NAME: public XXWCCycleCount previous() throws PreviousRecordException
     *
     *   PURPOSE:   private method to default gCallFrom value for debugging
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150302-00173 - RF - Cyclecount
     **************************************************************************/

    public XXWCCycleCount previous() throws PreviousRecordException {
        int i = mWCCountListSeq.size();
        if (mCurrentPos == 0) {
            throw new PreviousRecordException();
        } else {
            mCurrentPos = mCurrentPos - 1;
            XXWCCycleCount XXWCCycleCount = (XXWCCycleCount)mWCCountListSeq.elementAt(mCurrentPos - 1);
            return XXWCCycleCount;
        }
    }

    /*************************************************************************
     *   NAME: public XXWCCycleCount getCurrentRecord()
     *
     *   PURPOSE:   private method to default gCallFrom value for debugging
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150302-00173 - RF - Cyclecount
     **************************************************************************/

    public XXWCCycleCount getCurrentRecord() {
        return (XXWCCycleCount)mWCCountListSeq.elementAt(mCurrentPos - 1);
    }

    /*************************************************************************
     *   NAME: public void destroy()
     *
     *   PURPOSE:   private method to default gCallFrom value for debugging
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150302-00173 - RF - Cyclecount
     **************************************************************************/

    public void destroy() {
        mWCCountListSeq = null;
        UtilFns.log("OfferIterator is destroyed");
    }

    /*************************************************************************
     *   NAME: public int getCurrentPos()
     *
     *   PURPOSE:   private method to default gCallFrom value for debugging
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150302-00173 - RF - Cyclecount
     **************************************************************************/

    public int getCurrentPos() {
        return mCurrentPos;
    }

    /*************************************************************************
     *   NAME: public int getCurrentPos()
     *
     *   PURPOSE:   private method to default gCallFrom value for debugging
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150302-00173 - RF - Cyclecount
     **************************************************************************/

    public int getSize() {
        return mWCCountListSeq.size();
    }
    
}
