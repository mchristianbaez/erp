/*************************************************************************
   *   $Header XXWCCycleCountFunction.java $
   *   Module Name: XXWCCycleCountFunction
   *   
   *   Package xxwc.oracle.apps.inv.count.server;
   *   
   *   
   *   Imports Classes:
   *   
   *   import oracle.apps.fnd.common.VersionInfo;
   *   import oracle.apps.inv.utilities.server.OrgFunction;
   *   import oracle.apps.inv.utilities.server.UtilFns;
   *   import oracle.apps.mwa.eventmodel.*;
   *   
   *   PURPOSE:   Java Class for XXWC Cycle Count Entry Function
   *    
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
      1.0       01-JUN-2015   Lee Spitzer             Initial Version - 
                                                          TMS Ticket Number 20150302-00173 - RF - Cyclecount
 **************************************************************************/

 package xxwc.oracle.apps.inv.count.server;

 import oracle.apps.fnd.common.VersionInfo;
 import oracle.apps.inv.utilities.server.OrgFunction;
 import oracle.apps.inv.utilities.server.UtilFns;
 import oracle.apps.mwa.eventmodel.*;

 /*************************************************************************
 *   NAME: public class XXWCCycleCountFunction extends OrgFunction implements MWAAppListener
 *
 *   PURPOSE:   Main class for XXWCPrimaryBinAssignmentFunction
 *
 *   REVISIONS:
 *   Ver        Date        Author                     Description
 *   ---------  ----------  ---------------         -------------------------
      1.0       01-JUN-2015   Lee Spitzer             Initial Version - 
                                                      TMS Ticket Number 20150302-00173 - RF - Cyclecount
  **************************************************************************/


 public class XXWCCycleCountFunction extends OrgFunction implements MWAAppListener {

  /*************************************************************************
   *   NAME: public static final String RCS_ID
   *
   *   PURPOSE:   Returns Header File Name
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
        1.0       01-JUN-2015   Lee Spitzer             Initial Version - 
                                                        TMS Ticket Number 20150302-00173 - RF - Cyclecount
    **************************************************************************/

  public static final String RCS_ID =
      "$Header: XXWCCycleCountFunction.java 120.0 2014/11/06 05:18:34 appldev noship $";

  /*************************************************************************
   *   NAME: public static final boolean RCS_ID_RECORDED
   *
   *   PURPOSE:   Returns Header File Name
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
        1.0       01-JUN-2015   Lee Spitzer             Initial Version - 
                                                        TMS Ticket Number 20150302-00173 - RF - Cyclecount
    **************************************************************************/


  public static final boolean RCS_ID_RECORDED =
      VersionInfo.recordClassVersion("$Header: XXWCCycleCountFunction.java 120.0 2014/11/06 05:18:34 appldev noship $",
                                     "xxwc.oracle.apps.inv.bins.server");


  /*************************************************************************
   *   NAME: public XXWCCycleCountFunction()
   *
   *   PURPOSE:   Menu function that points the mobile to the mobile page layout
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
        1.0       01-JUN-2015   Lee Spitzer             Initial Version - 
                                                        TMS Ticket Number 20150302-00173 - RF - Cyclecount
    **************************************************************************/


  public XXWCCycleCountFunction() {
      setFirstPageName("xxwc.oracle.apps.inv.count.server.XXWCCycleCountPage"); /*This should point to the file name XXWCRcptGenFunction.java in the local directory*/
      addListener(this);
      UtilFns.log("constructing XXWCCycleCountFunction function");
  }


  /*************************************************************************
   *   NAME: public void appEntered(MWAEvent mwaevent)
   *
   *   PURPOSE:   Standard appEntered Method and inherits
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
        1.0       01-JUN-2015   Lee Spitzer             Initial Version - 
                                                        TMS Ticket Number 20150302-00173 - RF - Cyclecount
    **************************************************************************/

  public void appEntered(MWAEvent mwaevent) {
      UtilFns.log("App entered : complete");
      try {
          super.appEntered(mwaevent);
      } catch (Exception exception) {
          UtilFns.log("Error calling parent");
      }
  }


  /*************************************************************************
   *   NAME: appExited(MWAEvent mwaevent)
   *
   *   PURPOSE:   Standard appExited Method
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
        1.0       01-JUN-2015   Lee Spitzer             Initial Version - 
                                                        TMS Ticket Number 20150302-00173 - RF - Cyclecount
    **************************************************************************/

  public void appExited(MWAEvent mwaevent) {
      UtilFns.log("App Exited : complete");
      try {
          super.appExited(mwaevent);
      } catch (Exception exception) {
          UtilFns.log("Error calling parent");
      }
  }

 }
