/*************************************************************************
   *   $Header XXWCCycleCountAddItemPage.java $
   *   Module Name: XXWCCycleCountAddItemPage
   *   
   *   Package: package xxwc.oracle.apps.inv.count.server;
   *   
   *   Imports Classes:
   *   
   *   import java.sql.*;
   *   import oracle.apps.inv.utilities.server.UtilFns;
   *   import oracle.apps.mwa.beans.*;
   *   import oracle.apps.mwa.container.MWALib;
   *   import oracle.apps.mwa.container.Session;
   *   import oracle.apps.mwa.eventmodel.*;
   *   import oracle.apps.mwa.container.FileLogger;
   *   import oracle.apps.fnd.flexj.FlexException;
   *   import oracle.apps.inv.lov.server.ItemLOV;
   *   import oracle.apps.inv.lov.server.LocatorKFF;
   *   import oracle.apps.inv.lov.server.SubinventoryLOV;
   *   import xxwc.oracle.apps.inv.lov.server.*;
   *
   *   PURPOSE:   Java Class for XXWC Cycle Count Page Layout.
   *    
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
        1.0       01-JUN-2015   Lee Spitzer             Initial Version - 
                                                            TMS Ticket Number 20150302-00173 - RF - Cyclecount
**************************************************************************/
package xxwc.oracle.apps.inv.count.server;

import java.sql.*;

import oracle.apps.mwa.beans.*;
import oracle.apps.mwa.container.MWALib;
import oracle.apps.mwa.container.Session;
import oracle.apps.mwa.eventmodel.*;
import oracle.apps.mwa.container.FileLogger;
import oracle.apps.fnd.flexj.FlexException;
import oracle.apps.fnd.flexj.KeyFlexfield;
import oracle.apps.inv.invinq.server.ItemOnhandIterator;
import oracle.apps.inv.lov.server.CycleCountLOV;
import oracle.apps.inv.utilities.server.*;
import oracle.apps.inv.lov.server.ItemLOV;
import oracle.apps.inv.lov.server.LocatorKFF;
import oracle.apps.inv.lov.server.SubinventoryLOV;

import oracle.apps.mwa.presentation.telnet.TelnetSession;

import xxwc.oracle.apps.inv.invinq.server.XXWCItemOnHand;
import xxwc.oracle.apps.inv.invinq.server.XXWCItemOnHandIterator;
import xxwc.oracle.apps.inv.lov.server.*;
import xxwc.oracle.apps.inv.rcv.server.XXWCRcvGenPage;

/*************************************************************************
 *   NAME: public class XXWCCycleCountAddItemPage extends PageBean
 *
 *   PURPOSE:   Main class for XXWCCycleCountPageAddItemPage
 *
 *   REVISIONS:
 *   Ver        Date        Author                     Description
 *   ---------  ----------  ---------------         -------------------------
      1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                      TMS Ticket Number 20150302-00173 - RF - Cyclecount
 **************************************************************************/

public class XXWCCycleCountAddItemPage extends PageBean{
    /*Define objects in the class*/
    public static String WMS_TXN_SUCCESS = "Txn Successful";
    public static String WMS_TXN_ERROR = "Txn Failed. Please check log";
    public static String WMS_TXN_CANCEL = "Txn Cancelled";
    public static String WMS_LOT_INS_FAIL = "Lot Insert Failed";
    public static String WMS_SER_INS_FAIL = "Serial Insert Failed";
    public static String saveNextPrompt = "";
    public static String donePrompt = "";
    public static String cancelPrompt = "";
    private boolean areThePromptsSet;
    TextFieldBean mCycleCountHeaderFld;
    protected XXWCBinsLOV mLocFld;
    protected XXWCItemLOV mItemFld;
    TextFieldBean mItemDescFld;
    protected XXWCSubLOV mSubFld;
    TextFieldBean mUOMFld;
    TextFieldBean mCountQtyFld;
    TextFieldBean mTotalCountQuantityFld;
    protected ButtonFieldBean mBack;
    protected ButtonFieldBean mSave;
    protected String m_userid;
    
    /*************************************************************************
     *   NAME: private static String gPackage = "xxwc.oracle.apps.inv.rcv.server";
     *
     *   PURPOSE:   private method to default gPackage value for debugging
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       01-JUN-2015   Lee Spitzer             Initial Version - 
                                                            TMS Ticket Number 20150302-00173 - RF - Cyclecount
     **************************************************************************/

    private static String gPackage = "xxwc.oracle.apps.inv.count.server";


    /*************************************************************************
     *   NAME: private static String gCallFrom = "XXWCCycleCountAddItemPage";
     *
     *   PURPOSE:   private method to pass gCallFrom from methods to return class name CustomListener
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       01-JUN-2015   Lee Spitzer             Initial Version - 
                                                            TMS Ticket Number 20150302-00173 - RF - Cyclecount
     **************************************************************************/

    private static String gCallFrom = "XXWCCycleCountAddItemPage";



    
    /*************************************************************************
    *   NAME: public XXWCCycleCountPage(Session session)
    *
    *   PURPOSE:   Main method and calls initLayout Method
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
         1.0       01-JUN-2015   Lee Spitzer             Initial Version - 
                                                         TMS Ticket Number 20150302-00173 - RF - Cyclecount
     **************************************************************************/

    public XXWCCycleCountAddItemPage(Session session) {
        initLayout(session);
    }
    
    /*************************************************************************
    *   NAME: private void initLayout(Session session)
    *
    *   PURPOSE:   Sets the initial layout of the paged
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
         1.0       01-JUN-2015   Lee Spitzer             Initial Version - 
                                                         TMS Ticket Number 20150302-00173 - RF - Cyclecount
     **************************************************************************/

    private void initLayout(Session session) {
        String gMethod = "setPrompts";
        String gCallPoint = "Start";
        m_userid = "";
        WMS_TXN_SUCCESS = UtilFns.getMessage(session, "WMS", "WMS_TXN_SUCCESS");
        WMS_TXN_ERROR = UtilFns.getMessage(session, "WMS", "WMS_TXN_ERROR");
        WMS_TXN_CANCEL = UtilFns.getMessage(session, "WMS", "WMS_TXN_CANCEL");
        WMS_LOT_INS_FAIL = UtilFns.getMessage(session, "WMS", "WMS_LOT_INS_FAIL");
        WMS_SER_INS_FAIL = UtilFns.getMessage(session, "WMS", "WMS_SER_INS_FAIL");
    
        /*Cycle Count Header*/
        mCycleCountHeaderFld = new TextFieldBean();
        mCycleCountHeaderFld.setName("XXWC.ADD_CYCLECOUNT_HEADER");
        mCycleCountHeaderFld.setPrompt("CycleHdrId");
        mCycleCountHeaderFld.setRequired(false);
        mCycleCountHeaderFld.setHidden(true);
        mCycleCountHeaderFld.setEditable(true);
        /*Item Description*/
        mItemDescFld = new TextFieldBean();
        mItemDescFld.setName("XXWC.ADD_DESC");
        mItemDescFld.setPrompt("Item Desc");
        mItemDescFld.setEditable(false);
        mItemDescFld.setHidden(true);
        /*Item*/        
        mItemFld = new XXWCItemLOV("ALL");
        mItemFld.setName("XXWC.ADD_ITEM");
        mItemFld.setPrompt("Item");
        mItemFld.setRequired(false);
        mItemFld.setEditable(true);
        /*Subinventory*/
        mSubFld = new XXWCSubLOV();
        mSubFld.setName("XXWC.ADD_SUB");
        mSubFld.setPrompt("Sub");
        mSubFld.setRequired(false);
        mSubFld.setEditable(true);
        mSubFld.setHidden(true);
        /*Bin*/
        mLocFld = new XXWCBinsLOV();
        mLocFld.setName("XXWC.ADD_LOC");
        mLocFld.setPrompt("Bin");
        mLocFld.setRequired(false);
        mLocFld.setEditable(false);
        mLocFld.setHidden(true);
        /*UOM Properties*/
        mUOMFld = new TextFieldBean();
        mUOMFld.setName("XXWC.ADD_UOM");
        mUOMFld.setPrompt("UOM");
        mUOMFld.setEditable(false);
        mUOMFld.setHidden(true);
        /*Count Qty*/
        mCountQtyFld = new TextFieldBean();
        mCountQtyFld.setName("XXWC.ADD_QTY");
        mCountQtyFld.setPrompt("Qty");
        mCountQtyFld.setEditable(true);
        mCountQtyFld.setRequired(false);
        mCountQtyFld.setHidden(true);
        /*Total Qty*/
        mTotalCountQuantityFld = new TextFieldBean();
        mTotalCountQuantityFld.setName("XXWC.ADD_TOTAL_QTY");
        mTotalCountQuantityFld.setPrompt("Total Qty");
        mTotalCountQuantityFld.setEditable(false);
        mTotalCountQuantityFld.setRequired(false);
        mTotalCountQuantityFld.setHidden(true);
        /*Create Bin Properties*/
        mBack = new ButtonFieldBean();
        mBack.setName("XXWC.ADD_BACK");
        mBack.setPrompt("Back");
        mBack.setEnableAcceleratorKey(true);
        mBack.setHidden(false);
        /*Add Item Properties*/
        mSave = new ButtonFieldBean();
        mSave.setName("XXWC.ADD_SAVE");
        mSave.setPrompt("Add");
        mSave.setEnableAcceleratorKey(true);
        mSave.setHidden(true);
        /*set the properties for each to the addFieldBean*/
        addFieldBean(mCycleCountHeaderFld);
        addFieldBean(mItemFld);
        addFieldBean(mItemDescFld);
        addFieldBean(mSubFld);
        addFieldBean(mLocFld);
        addFieldBean(mUOMFld);
        addFieldBean(mTotalCountQuantityFld);
        addFieldBean(mCountQtyFld);
        addFieldBean(mSave);
        addFieldBean(mBack);
        XXWCCycleCountAddItemFListener fieldListener = new XXWCCycleCountAddItemFListener();
        /*Add Listener to each property*/
        mCycleCountHeaderFld.addListener(fieldListener);
        mItemFld.addListener(fieldListener);
        mItemDescFld.addListener(fieldListener);
        mSubFld.addListener(fieldListener);
        mLocFld.addListener(fieldListener);
        mUOMFld.addListener(fieldListener);
        mTotalCountQuantityFld.addListener(fieldListener);
        mCountQtyFld.addListener(fieldListener);
        mSave.addListener(fieldListener);
        mBack.addListener(fieldListener);
        try {
            setPrompts(session);
        } catch (Exception exception) {
            session.setStatusMessage(UtilFns.getMessage(session, "WMS", "WMS_PROMPT_NOTSET"));
        }

    }
    
        /*************************************************************************
        *   NAME: private void setPrompts(Session session)
        *
        *   PURPOSE:   Sets Prompts for the Page
        *
        *   REVISIONS:
        *   Ver        Date        Author                     Description
        *   ---------  ----------  ---------------         -------------------------
             1.0       01-JUN-2015   Lee Spitzer             Initial Version - 
                                                             TMS Ticket Number 20150302-00173 - RF - Cyclecount
         **************************************************************************/

        private void setPrompts(Session session) {
                String gMethod = "setPrompts";
                String gCallPoint = "Start";
                if (!areThePromptsSet) {
                try {
                    initPrompts(session);
                } catch (SQLException sqlexception) {
                    session.setStatusMessage(UtilFns.getMessage(session, "WMS", "WMS_TXN_ERROR"));
                }
            }
            //mClose.setPrompt(saveNextPrompt);
            //mProcess.setPrompt(donePrompt);
            mSave.retrieveAttributes("INV_SAVE_NEXT_PROMPT");
            mBack.retrieveAttributes("INV_CANCEL_PROMPT");
            gCallPoint = "End Prompts";
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
            
            setFieldValues(session);
        }
        
        /*************************************************************************
        *   NAME: private void initPrompts(Session session) throws SQLException
        *
        *   PURPOSE:   Sets the initial prompts for the page
        *
        *   REVISIONS:
        *   Ver        Date        Author                     Description
        *   ---------  ----------  ---------------         -------------------------
             1.0       01-JUN-2015   Lee Spitzer             Initial Version - 
                                                             TMS Ticket Number 20150302-00173 - RF - Cyclecount
         **************************************************************************/
        
        private void initPrompts(Session session) throws SQLException {
            try {
                String s = (String)session.getObject("ORGCODE");
                setPrompt((new StringBuilder()).append("XXWC Cycle Count Add Item (").append(s).append(")").toString());
                saveNextPrompt =
                        MWALib.getAKPrompt(session, "oracle.apps.inv.utilities.InvResourceTable", "INV_SAVE_NEXT_PROMPT");
                donePrompt = MWALib.getAKPrompt(session, "oracle.apps.inv.utilities.InvResourceTable", "INV_DONE_PROMPT");
                areThePromptsSet = true;
            } catch (SQLException sqlexception) {
                session.setStatusMessage(UtilFns.getMessage(session, "WMS", "WMS_TXN_ERROR"));
            }
        }
        
        /*************************************************************************
         *   NAME: public TextFieldBean  getmCycleCount()
         *
         *   PURPOSE:   return the value of mCycleCountHeaderFld
         *
         *   REVISIONS:
         *   Ver        Date        Author                     Description
         *   ---------  ----------  ---------------         -------------------------
            1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                    TMS Ticket Number 20150302-00173 - RF - Cyclecount
         **************************************************************************/

        public TextFieldBean getmCycleCountHeaderFld() 
        {
            return mCycleCountHeaderFld;    
        }

        /*************************************************************************
         *   NAME: public XXWCBinsLOV  getmLocFld()
         *
         *   PURPOSE:   return the value of mLocFld
         *
         *   REVISIONS:
         *   Ver        Date        Author                     Description
         *   ---------  ----------  ---------------         -------------------------
            1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                    TMS Ticket Number 20150302-00173 - RF - Cyclecount
         **************************************************************************/
        


        public XXWCBinsLOV getmLocFld()
        {
           return mLocFld;
        }


        /*************************************************************************
         *   NAME: public TextFieldBean  getmItemDescFld()
         *
         *   PURPOSE:   return the value of mItemDescFld
         *
         *   REVISIONS:
         *   Ver        Date        Author                     Description
         *   ---------  ----------  ---------------         -------------------------
            1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                    TMS Ticket Number 20150302-00173 - RF - Cyclecount
         **************************************************************************/

        public TextFieldBean getmItemDescFld() 
        {
            return mItemDescFld;    
        }
        

        /*************************************************************************
         *   NAME: public XXWCItemLOV  getmItemFld()
         *
         *   PURPOSE:   return the value of mItemFld
         *
         *   REVISIONS:
         *   Ver        Date        Author                     Description
         *   ---------  ----------  ---------------         -------------------------
            1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                    TMS Ticket Number 20150302-00173 - RF - Cyclecount
         **************************************************************************/

        public XXWCItemLOV getmItemFld() 
        {
            return mItemFld;    
        }

    /*************************************************************************
     *   NAME: public TextFieldBean  getmSubFld()
     *
     *   PURPOSE:   return the value of mSubFld
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
        1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150302-00173 - RF - Cyclecount
     **************************************************************************/

    public XXWCSubLOV getmSubFld()
    {
        return mSubFld;
    }
    


        /*************************************************************************
         *   NAME: public TextFieldBean getmUOMFld()
         *
         *   PURPOSE:   return the value of mUOMFld
         *
         *   REVISIONS:
         *   Ver        Date        Author                     Description
         *   ---------  ----------  ---------------         -------------------------
            1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                    TMS Ticket Number 20150302-00173 - RF - Cyclecount
         **************************************************************************/

        public TextFieldBean getmUOMFld() {
            return mUOMFld;
        }

        /*************************************************************************
         *   NAME: public TextFieldBean getmTotalCountQuantityFld()
         *
         *   PURPOSE:   return the value of mTotalCountQuantityFld
         *
         *   REVISIONS:
         *   Ver        Date        Author                     Description
         *   ---------  ----------  ---------------         -------------------------
            1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                    TMS Ticket Number 20150302-00173 - RF - Cyclecount
         **************************************************************************/

        public TextFieldBean getmTotalCountQuantityFld() {
            return mTotalCountQuantityFld;
        }
        
        /*************************************************************************
         *   NAME: public TextFieldBean mCountQtyFld()
         *
         *   PURPOSE:   return the value of mCountQtyFld
         *
         *   REVISIONS:
         *   Ver        Date        Author                     Description
         *   ---------  ----------  ---------------         -------------------------
            1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                    TMS Ticket Number 20150302-00173 - RF - Cyclecount
         **************************************************************************/

        public TextFieldBean getmCountQtyFld() {
            return mCountQtyFld;
        }


        /*************************************************************************
        *   NAME: public ButtonFieldBean getmBack()
        *
        *   PURPOSE:   return the value of mBack
        *
        *   REVISIONS:
        *   Ver        Date        Author                     Description
        *   ---------  ----------  ---------------         -------------------------
            1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                        TMS Ticket Number 20150302-00173 - RF - Cyclecount
        **************************************************************************/

        public ButtonFieldBean getmBack() {
            return mBack;
        }

        /*************************************************************************
        *   NAME: public ButtonFieldBean getmSave()
        *
        *   PURPOSE:   return the value of mSave
        *
        *   REVISIONS:
        *   Ver        Date        Author                     Description
        *   ---------  ----------  ---------------         -------------------------
            1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                        TMS Ticket Number 20150302-00173 - RF - Cyclecount
        **************************************************************************/

        public ButtonFieldBean getmSave() {
            return mSave;
        }


        /*************************************************************************
         *   NAME: public String getUserId() getUserId()
         *
         *   PURPOSE:   return the value of m_userid
         *
         *   REVISIONS:
         *   Ver        Date        Author                     Description
         *   ---------  ----------  ---------------         -------------------------
            1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                    TMS Ticket Number 20150302-00173 - RF - Cyclecount
         **************************************************************************/

        public String getUserId() {
            return m_userid;
        }

        /*************************************************************************
        *   NAME: void pageEntered(MWAEvent e) throws AbortHandlerException, InterruptedHandlerException,
                                                       DefaultOnlyHandlerException {
        *
        *   PURPOSE:   pageEntered method
        *
        *   Parameters: MWAEvent mwaevent
        *
        *   REVISIONS:
        *   Ver        Date        Author                     Description
        *   ---------  ----------  ---------------         -------------------------
            1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                        TMS Ticket Number 20150302-00173 - RF - Cyclecount
        **************************************************************************/

        public void pageEntered(MWAEvent e) throws AbortHandlerException, InterruptedHandlerException,
                                                   DefaultOnlyHandlerException {
            
            Session session = e.getSession();
            
        }

        /*************************************************************************
        *   NAME: void pageEntered(MWAEvent e) throws AbortHandlerException, InterruptedHandlerException,
                                                           DefaultOnlyHandlerException {
        *
        *   PURPOSE:   pageEntered method
        *
        *   Parameters: MWAEvent mwaevent
        *
        *   REVISIONS:
        *   Ver        Date        Author                     Description
        *   ---------  ----------  ---------------         -------------------------
            1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                            TMS Ticket Number 20150302-00173 - RF - Cyclecount
        **************************************************************************/

        public void pageExited(MWAEvent e) throws AbortHandlerException, InterruptedHandlerException,
                                                   DefaultOnlyHandlerException {
            Session session = e.getSession();
        
        }
    /*************************************************************************
     *   NAME: void setFieldValues((Session session) throws AbortHandlerException, InterruptedHandlerException,
                                               DefaultOnlyHandlerException {
     *
     *   PURPOSE:   pageExited method
     *
     *
     *   Parameters: MWAEvent   mwaevent
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                     TMS Ticket Number 20150302-00173 - RF - Cyclecount

     **************************************************************************/

    
    public void setFieldValues(Session session){
        String gMethod = "sesFieldValues";
        String gCallPoint = "Start";
        session.setRefreshScreen(true);
        try{
            mCycleCountHeaderFld.setValue(session.getObject("sessXXWC.CYCLECOUNT_HEADER").toString());
          }
            catch (Exception e){
              mCycleCountHeaderFld.setValue("");
              
          }
        
        }

}
    