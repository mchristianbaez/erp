/*************************************************************************
   *   $Header XXWCBinsLOV.java $
   *   Module Name: XXWCBinsLOV
   *   
   *   Package package xxwc.oracle.apps.inv.lov.server;
   *   
   *   Imports Classes
   *   
   *   import java.sql.SQLException;
   *   import java.util.StringTokenizer;
   *   import java.util.Vector;
   *   import oracle.apps.fnd.common.*;
   *   import oracle.apps.inv.lov.server.*;
   *   import oracle.apps.inv.utilities.server.UtilFns;
   *   import oracle.apps.mwa.beans.*;
   *   import oracle.apps.mwa.container.MWALib;
   *   import oracle.apps.mwa.container.Session;
   *   import oracle.apps.mwa.eventmodel.*;
   *   import xxwc.oracle.apps.inv.bins.server.XXWCPrimaryBinAssignment;
   *   
   *   PURPOSE:   XXWCBinsLOV List of Values for MTL_SYSTM_ITEMS_KFV and MTL_CROSS_REFERENCES for Item and UPC Scanning
   *   
   *    
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
        1.0       18-FEB-2015   Lee Spitzer             Initial Version - 
                                                           TMS Ticket 20150302-00152 - RF - Receiving
**************************************************************************/

package xxwc.oracle.apps.inv.lov.server;

import java.sql.SQLException;
import java.util.StringTokenizer;
import java.util.Vector;
import oracle.apps.fnd.common.*;
import oracle.apps.inv.lov.server.*;
import oracle.apps.inv.utilities.server.UtilFns;
import oracle.apps.mwa.beans.*;
import oracle.apps.mwa.container.MWALib;
import oracle.apps.mwa.container.Session;
import oracle.apps.mwa.eventmodel.*;

/*************************************************************************
 *   NAME: XXWCBinsLOV extends InvLOVFieldBean implements MWAFieldListener
 *
 *   PURPOSE:   Main class for XXWCBinsLOV 
 *
 *   REVISIONS:
 *   Ver        Date        Author                     Description
 *   ---------  ----------  ---------------         -------------------------
        1.0       18-FEB-2015   Lee Spitzer             Initial Version -
                                                         TMS Ticket 20150302-00152 - RF - Receiving
 **************************************************************************/

public class XXWCBinsLOV extends InvLOVFieldBean implements MWAFieldListener{
    public static final String RCS_ID = "$Header: XXWCBinsLOV.java 120.6 2014/10/21 15:24:59 aambulka ship $";
    public static final boolean RCS_ID_RECORDED = VersionInfo.recordClassVersion("$Header: XXWCBinsLOV.java 120.6 2014/10/21 15:24:59 aambulka ship $", "xxwc.oracle.apps.inv.lov.server");
    private Session ses;
    private String mWCLocator;
    private String mWCPreFix;
    private String mLocationId;
    private String mConcatedSegments;
    private String mDescription;
    private String mSubinventory;
    private boolean areThePromptsSet;
      
    /*************************************************************************
     *   NAME: public XXWCBinsLOV
     *
     *   PURPOSE:   main method XXWCBinsLOV
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/
    
    public XXWCBinsLOV() {
        areThePromptsSet = false;
        addListener(this);
        clear();
    
    }
    /*************************************************************************
     *   NAME: private void initDefaultType()
     *
     *   PURPOSE:   sets the paramType, prompts, lovstatement, subfield and flags 
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/
    
    private void initDefaultType() {
        //                      0    1    2    3    4    5    6    7    8
        String paramType[] = { "C", "N", "S", "N", "N", "S", "S", "S", "S" };
        String prompts[] = { "WC_LOCATOR", "PREFIX", "LOCATOR_ID", "LOCATOR", "DESCRIPTION", "SUBINVENTORY_CODE" };
        boolean flag[] = {true, true, false, true, false, false};
        setlovStatement("XXWC_INV_BIN_MAINTENANCE_PKG.GET_WC_BIN_LOC");
        setInputParameterTypes(paramType);
        setSubfieldPrompts(prompts);
        setSubfieldDisplays(flag);  
    }
    
     /*************************************************************************
     *   NAME: public void fieldEntered(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException, DefaultOnlyHandlerException
     *
     *   PURPOSE:   fieldEntered method sets the connection and subPrompts
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/
      
    public void fieldEntered(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException, DefaultOnlyHandlerException{
        ses = mwaevent.getSession();
        java.sql.Connection connection = ses.getConnection();
        setSubPrompts(mwaevent.getSession());
       }
    
    /*************************************************************************
     *   NAME: public void fieldExited(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException, DefaultOnlyHandlerException
     *
     *   PURPOSE:   fieldExited method calls the setReturnValues from the LOV Query
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/
    
    
    public void fieldExited(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException, DefaultOnlyHandlerException {
        ses = mwaevent.getSession();
        try {
                if(mwaevent.getAction().equals("MWA_SUBMIT") && isPopulated())
                {
                    setReturnValues();
                }
            }
        catch(Exception e){
            UtilFns.error("Error in calling fieldExited " + e);
            
        }
        
        if(getValue().equals(""))
                {
                    clear();
                }
    }
    
    /*************************************************************************
     *   NAME: private void setReturnValues()
     *
     *   PURPOSE:   sets the return values from the list of values query
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/
    
    private void setReturnValues()
        { 
            mWCLocator = "";
            mWCPreFix = "";
            mLocationId = "";
            mConcatedSegments = "";
            mDescription = "";
            mSubinventory = "";
            
            
            Vector vector = getSelectedValues();
            if(vector != null) {
                int i = vector.size();
                mWCLocator = (String)vector.elementAt(0);
                mWCPreFix = (String)vector.elementAt(1);
                mLocationId = (String)vector.elementAt(2);
                mConcatedSegments = (String)vector.elementAt(3);
                mDescription = (String)vector.elementAt(4);
                mSubinventory = (String)vector.elementAt(5);
            }
        }
    
    /*************************************************************************
     *   NAME: public void clear
     *
     *   PURPOSE:   clears the returned list of values
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/
    
    public void clear()
        {
            setValue("");
            mWCLocator = "";
            mWCPreFix = "";
            mLocationId = "";
            mConcatedSegments = "";
            mDescription = "";
            mSubinventory = "";
        }

     
    /*************************************************************************
    *   NAME: public String getmWCLocator()
    *
    *   PURPOSE:   returns the subinventory
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       18-FEB-2015   Lee Spitzer             Initial Version -
    **************************************************************************/
          
    public String getmWCLocator() {
       return mWCLocator;
    }
    
    /*************************************************************************
    *   NAME: public String getmWCPreFix()
    *
    *   PURPOSE:   returns the subinventory
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       18-FEB-2015   Lee Spitzer             Initial Version -
    **************************************************************************/
          
    public String getmWCPreFix() {
       return mWCPreFix;
    }
    
    /*************************************************************************
     *   NAME: public String getmLocationId() 
     *
     *   PURPOSE:   returns the Description
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/

    public String getmLocationId() {
        return mLocationId;
    }

    /*************************************************************************
    *   NAME: public String getmConcatedSegments()
    *
    *   PURPOSE:   returns the Locator Type
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       18-FEB-2015   Lee Spitzer             Initial Version -
    **************************************************************************/
          
    public String getmConcatedSegments() {
       return mConcatedSegments;
    }

    /*************************************************************************
    *   NAME: public String getmDescription()
    *
    *   PURPOSE:   returns the Asset Type
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       18-FEB-2015   Lee Spitzer             Initial Version -
    **************************************************************************/
          
    public String getmDescription() {
       return mDescription;
    }

    /*************************************************************************
    *   NAME: public String getmSubinventory()
    *
    *   PURPOSE:   returns the Asset Type
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       18-FEB-2015   Lee Spitzer             Initial Version -
    **************************************************************************/

    public String getmSubinventory() {
        return mSubinventory;
    }
     
    /*************************************************************************
    *   NAME: public void setmItemNumber(String setmItemNumber)
    *
    *   PURPOSE:  sets the mPrinterName
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       18-FEB-2015   Lee Spitzer             Initial Version -
    **************************************************************************/
    
    public void setmWCLocator (String setmWCLocator) {
            mWCLocator = mWCLocator;
    }

    /*************************************************************************
    *   NAME: public void setmItemNumber(String setmItemNumber)
    *
    *   PURPOSE:  sets the mPrinterName
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       18-FEB-2015   Lee Spitzer             Initial Version -
    **************************************************************************/
    
    public void setmWCPreFix (String setmWCPreFix) {
            mWCPreFix = setmWCPreFix;
    }

    /*************************************************************************
    *   NAME: public void setmLocationId(String setmLocationId)
    *
    *   PURPOSE:  sets the mLocationId
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       18-FEB-2015   Lee Spitzer             Initial Version -
    **************************************************************************/

    public void setmLocationId (String setmLocationId) {
            mLocationId = setmLocationId;
    }

    /*************************************************************************
    *   NAME: public void setmConcatedSegments(String setmConcatedSegments)
    *
    *   PURPOSE:  sets the mConcatedSegments
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       18-FEB-2015   Lee Spitzer             Initial Version -
    **************************************************************************/

    public void setmConcatedSegments (String setmConcatedSegments) {
            mConcatedSegments = setmConcatedSegments;
    }
    
    

    /*************************************************************************
    *   NAME: public void setmDescription(String setmDescription)
    *
    *   PURPOSE:  sets the mDescription
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       18-FEB-2015   Lee Spitzer             Initial Version -
    **************************************************************************/

    public void setmDescription (String setmDescription) {
            mDescription = setmDescription;
    }
    
    /*************************************************************************
    *   NAME: public void setmDescription(String setmDescription)
    *
    *   PURPOSE:  sets the mDescription
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       18-FEB-2015   Lee Spitzer             Initial Version -
    **************************************************************************/

    public void setmSubinventory (String setmSubinventory) {
            mSubinventory = setmSubinventory;
    }
   
    /*************************************************************************
    *   NAME: public void setSubPrompts(Session session)
    *
    *   PURPOSE:  sets the query header values displayed in the list of values
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       18-FEB-2015   Lee Spitzer             Initial Version -
    **************************************************************************/
    
    public void setSubPrompts(Session session) {
        String prompts[] = { "WC_LOCATOR", "PREFIX", "LOCATOR_ID", "LOCATOR", "DESCRIPTION", "SUBINVENTORY" };
    }
}
