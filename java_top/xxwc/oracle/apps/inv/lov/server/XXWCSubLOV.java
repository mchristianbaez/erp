/*************************************************************************
   *   $Header XXWCSubLOV.java $
   *   Module Name: XXWCSubLOV
   *   
   *   Package package xxwc.oracle.apps.inv.lov.server;
   *   
   *   Imports Classes
   *   
   *   import java.sql.SQLException;
   *   import java.util.StringTokenizer;
   *   import java.util.Vector;
   *   import oracle.apps.fnd.common.*;
   *   import oracle.apps.inv.lov.server.*;
   *   import oracle.apps.inv.utilities.server.UtilFns;
   *   import oracle.apps.mwa.beans.*;
   *   import oracle.apps.mwa.container.MWALib;
   *   import oracle.apps.mwa.container.Session;
   *   import oracle.apps.mwa.eventmodel.*;
   *   import xxwc.oracle.apps.inv.bins.server.XXWCPrimaryBinAssignment;
   *   
   *   PURPOSE:   XXWCSubLOV List of Values for MTL_SYSTM_ITEMS_KFV and MTL_CROSS_REFERENCES for Item and UPC Scanning
   *   
   *    
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
        1.0       18-FEB-2015   Lee Spitzer             Initial Version - 
                                                           TMS Ticket 20150302-00152 - RF - Receiving
**************************************************************************/

package xxwc.oracle.apps.inv.lov.server;

import java.sql.SQLException;
import java.util.StringTokenizer;
import java.util.Vector;
import oracle.apps.fnd.common.*;
import oracle.apps.inv.lov.server.*;
import oracle.apps.inv.utilities.server.UtilFns;
import oracle.apps.mwa.beans.*;
import oracle.apps.mwa.container.MWALib;
import oracle.apps.mwa.container.Session;
import oracle.apps.mwa.eventmodel.*;

/*************************************************************************
 *   NAME: XXWCSubLOV extends InvLOVFieldBean implements MWAFieldListener
 *
 *   PURPOSE:   Main class for XXWCSubLOV 
 *
 *   REVISIONS:
 *   Ver        Date        Author                     Description
 *   ---------  ----------  ---------------         -------------------------
        1.0       18-FEB-2015   Lee Spitzer             Initial Version -
                                                         TMS Ticket 20150302-00152 - RF - Receiving
 **************************************************************************/

public class XXWCSubLOV extends InvLOVFieldBean implements MWAFieldListener{
    public static final String RCS_ID = "$Header: XXWCSubLOV.java 120.6 2014/10/21 15:24:59 aambulka ship $";
    public static final boolean RCS_ID_RECORDED = VersionInfo.recordClassVersion("$Header: XXWCSubLOV.java 120.6 2014/10/21 15:24:59 aambulka ship $", "xxwc.oracle.apps.inv.lov.server");
    private Session ses;
    private String mSubinventoryCode;
    private String mDescription;
    private String mLocatorType;
    private String mAssetType;
    private boolean areThePromptsSet;
      
    /*************************************************************************
     *   NAME: public XXWCSubLOV
     *
     *   PURPOSE:   main method XXWCSubLOV
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/
    
    public XXWCSubLOV() {
        areThePromptsSet = false;
        addListener(this);
        clear();
        initDefaultType();
    
    }
    /*************************************************************************
     *   NAME: private void initDefaultType()
     *
     *   PURPOSE:   sets the paramType, prompts, lovstatement, subfield and flags 
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/
    
    private void initDefaultType() {
        String paramType[] = { "C", "N", "S", "S"};
        String prompts[] = { "SUBINVENTORY_CODE" , "DESCRIPTION", "LOCATOR_TYPE", "ASSET_INVENTORY"};
        boolean flag[] = {true, true, false, false};
        setlovStatement("XXWC_MWA_ROUTINES_PKG.GET_WC_SUB_LOV");
        setInputParameterTypes(paramType);
        setSubfieldPrompts(prompts);
        setSubfieldDisplays(flag);      
    }
    
     /*************************************************************************
     *   NAME: public void fieldEntered(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException, DefaultOnlyHandlerException
     *
     *   PURPOSE:   fieldEntered method sets the connection and subPrompts
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/
      
    public void fieldEntered(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException, DefaultOnlyHandlerException{
        ses = mwaevent.getSession();
        java.sql.Connection connection = ses.getConnection();
        setSubPrompts(mwaevent.getSession());
       }
    
    /*************************************************************************
     *   NAME: public void fieldExited(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException, DefaultOnlyHandlerException
     *
     *   PURPOSE:   fieldExited method calls the setReturnValues from the LOV Query
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/
    
    
    public void fieldExited(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException, DefaultOnlyHandlerException {
        ses = mwaevent.getSession();
        try {
                if(mwaevent.getAction().equals("MWA_SUBMIT") && isPopulated())
                {
                    setReturnValues();
                }
            }
        catch(Exception e){
            UtilFns.error("Error in calling fieldExited " + e);
            
        }
        
        if(getValue().equals(""))
                {
                    clear();
                }
    }
    
    /*************************************************************************
     *   NAME: private void setReturnValues()
     *
     *   PURPOSE:   sets the return values from the list of values query
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/
    
    private void setReturnValues()
        { 
            mSubinventoryCode = "";
            mDescription = "";
            mLocatorType = "";
            mAssetType = "";
            
            Vector vector = getSelectedValues();
            if(vector != null) {
                int i = vector.size();
                mSubinventoryCode = (String)vector.elementAt(0);
                mDescription = (String)vector.elementAt(1);
                mLocatorType = (String)vector.elementAt(2);
                mAssetType = (String)vector.elementAt(3);
            }
        }
    
    /*************************************************************************
     *   NAME: public void clear
     *
     *   PURPOSE:   clears the returned list of values
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/
    
    public void clear()
        {
            setValue("");
            mSubinventoryCode = "";
            mDescription = "";
            mLocatorType = "";
            mAssetType = "";
            
        }

     
    /*************************************************************************
    *   NAME: public String getmSubinventoryCode()
    *
    *   PURPOSE:   returns the subinventory
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       18-FEB-2015   Lee Spitzer             Initial Version -
    **************************************************************************/
          
    public String getmSubinventoryCode() {
       return mSubinventoryCode;
    }
    
    
    /*************************************************************************
     *   NAME: public String getmDescription() 
     *
     *   PURPOSE:   returns the Description
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/

    public String getmDescription() {
        return mDescription;
    }

    /*************************************************************************
    *   NAME: public String getmLocatorType()
    *
    *   PURPOSE:   returns the Locator Type
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       18-FEB-2015   Lee Spitzer             Initial Version -
    **************************************************************************/
          
    public String getmLocatorType() {
       return mLocatorType;
    }

    /*************************************************************************
    *   NAME: public String getmAssetType()
    *
    *   PURPOSE:   returns the Asset Type
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       18-FEB-2015   Lee Spitzer             Initial Version -
    **************************************************************************/
          
    public String getmAssetType() {
       return mAssetType;
    }


     
    /*************************************************************************
    *   NAME: public void setmItemNumber(String setmItemNumber)
    *
    *   PURPOSE:  sets the mPrinterName
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       18-FEB-2015   Lee Spitzer             Initial Version -
    **************************************************************************/
    
    public void setmSubinventoryCode(String setmSubinventoryCoe) {
        mSubinventoryCode = setmSubinventoryCoe;
    }
    
    /*************************************************************************
    *   NAME: public void setSubPrompts(Session session)
    *
    *   PURPOSE:  sets the query header values displayed in the list of values
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       18-FEB-2015   Lee Spitzer             Initial Version -
    **************************************************************************/
    
    public void setSubPrompts(Session session) {
        String as1[] = { "SUBINVENTORY_CODE" , "DESCRIPTION", "LOCATOR_TYPE", "ASSET_INVENTORY"};
    }
}
