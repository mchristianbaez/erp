/*************************************************************************
   *   $Header XXWCREQLOV.java $
   *   Module Name: XXWCREQLOV 
   *   
   *   Package package xxwc.oracle.apps.inv.lov.server;
   *   
   *   Imports Classes
   *   
   *   import java.sql.SQLException;
   *   import java.util.StringTokenizer;
   *   import java.util.Vector;
   *   import oracle.apps.fnd.common.*;
   *   import oracle.apps.inv.lov.server.*;
   *   import oracle.apps.inv.utilities.server.UtilFns;
   *   import oracle.apps.mwa.beans.*;
   *   import oracle.apps.mwa.container.MWALib;
   *   import oracle.apps.mwa.container.Session;
   *   import oracle.apps.mwa.eventmodel.*;
   *   import xxwc.oracle.apps.inv.bins.server.XXWCPrimaryBinAssignment;
   *   
   *   PURPOSE:   XXWCREQLOV  List of Values for MTL_SYSTM_ITEMS_KFV and MTL_CROSS_REFERENCES for Item and UPC Scanning
   *   
   *    
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
        1.0       18-FEB-2015   Lee Spitzer             Initial Version - 
                                                           TMS Ticket 20150302-00152 - RF - Receiving
**************************************************************************/

package xxwc.oracle.apps.inv.lov.server;

import java.sql.SQLException;
import java.util.StringTokenizer;
import java.util.Vector;
import oracle.apps.fnd.common.*;
import oracle.apps.inv.lov.server.*;
import oracle.apps.inv.utilities.server.UtilFns;
import oracle.apps.mwa.beans.*;
import oracle.apps.mwa.container.MWALib;
import oracle.apps.mwa.container.Session;
import oracle.apps.mwa.eventmodel.*;

/*************************************************************************
 *   NAME: XXWCREQLOV  extends InvLOVFieldBean implements MWAFieldListener
 *
 *   PURPOSE:   Main class for XXWCREQLOV  
 *
 *   REVISIONS:
 *   Ver        Date        Author                     Description
 *   ---------  ----------  ---------------         -------------------------
        1.0       18-FEB-2015   Lee Spitzer             Initial Version -
                                                         TMS Ticket 20150302-00152 - RF - Receiving
 **************************************************************************/

public class XXWCREQLOV extends InvLOVFieldBean implements MWAFieldListener{
    public static final String RCS_ID = "$Header: XXWCREQLOV .java 120.6 2014/10/21 15:24:59 aambulka ship $";
    public static final boolean RCS_ID_RECORDED = VersionInfo.recordClassVersion("$Header: XXWCREQLOV .java 120.6 2014/10/21 15:24:59 aambulka ship $", "xxwc.oracle.apps.inv.lov.server");
    private Session ses;
    private String mREQHeaderId;
    private String mREQNumber;
    private String mNeedByDate;
    private String mShipFromOrg;
    private String mShipFromOrgId;
    private String mShipmentNumber;
    private String mShipmentHeaderId;
    private String mOrderHeaderId;
    private String mOrderNumber;
    private boolean areThePromptsSet;
      
    /*************************************************************************
     *   NAME: public XXWCREQLOV 
     *
     *   PURPOSE:   main method XXWCREQLOV 
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/
    
    public XXWCREQLOV () {
        areThePromptsSet = false;
        addListener(this);
        clear();
        initDefaultType();
          
    }
    
    /*************************************************************************
     *   NAME: private void initDefaultType()
     *
     *   PURPOSE:   sets the paramType, prompts, lovstatement, subfield and flags 
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/
    
    private void initDefaultType() {
        String paramType[] = { "C", "N", "S"};
        String prompts[] = { "REQ_HEADER_ID", "REQ_NUMBER", "SHIPPED_DATE", "SHIP_FROM_ORG", "SHIP_FROM_ORG_ID", "SHIPMENT_NUM", "SHIPMENT_HEADER_ID", "OE_HEADER_ID", "ISO_NUMBER"};
        boolean flag[] = {false, true, true, true, false, true, false, false, true};
        setlovStatement("XXWC_RCV_MOB_PKG.GET_REQ_LOV");
        setInputParameterTypes(paramType);
        setSubfieldPrompts(prompts);
        setSubfieldDisplays(flag);      
    }
    
    /*************************************************************************
     *   NAME: public void fieldEntered(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException, DefaultOnlyHandlerException
     *
     *   PURPOSE:   fieldEntered method sets the connection and subPrompts
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/
      
    public void fieldEntered(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException, DefaultOnlyHandlerException{
        ses = mwaevent.getSession();
        java.sql.Connection connection = ses.getConnection();
        setSubPrompts(mwaevent.getSession());
       }
    
    /*************************************************************************
     *   NAME: public void fieldExited(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException, DefaultOnlyHandlerException
     *
     *   PURPOSE:   fieldExited method calls the setReturnValues from the LOV Query
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/
    
    
    public void fieldExited(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException, DefaultOnlyHandlerException {
        ses = mwaevent.getSession();
        try {
                if(mwaevent.getAction().equals("MWA_SUBMIT") && isPopulated())
                {
                    setReturnValues();
                }
            }
        catch(Exception e){
            UtilFns.error("Error in calling fieldExited " + e);
            
        }
        
        if(getValue().equals(""))
                {
                    clear();
                }
    }
    
    /*************************************************************************
     *   NAME: private void setReturnValues()
     *
     *   PURPOSE:   sets the return values from the list of values query
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/
    
    private void setReturnValues()
        { 
            mREQHeaderId = "";
            mREQNumber = "";
            mNeedByDate = "";
            mShipFromOrg = "";
            mShipFromOrgId = "";
            mShipmentNumber = "";
            mShipmentHeaderId = "";
            mOrderHeaderId = "";
            mOrderNumber = "";
            
            Vector vector = getSelectedValues();
            if(vector != null) {
                int i = vector.size();
                mREQHeaderId = (String)vector.elementAt(0);
                mREQNumber = (String)vector.elementAt(1);
                mNeedByDate = (String)vector.elementAt(2);
                mShipFromOrg = (String)vector.elementAt(3);
                mShipFromOrgId = (String)vector.elementAt(4);
                mShipmentNumber = (String)vector.elementAt(5);
                mShipmentHeaderId = (String)vector.elementAt(6);
                mOrderHeaderId = (String)vector.elementAt(7);
                mOrderNumber = (String)vector.elementAt(8);
            }
        }
    
    /*************************************************************************
     *   NAME: public void clear
     *
     *   PURPOSE:   clears the returned list of values
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/
    
    public void clear()
        {
            setValue("");
            mREQHeaderId = "";
            mREQNumber = "";
            mNeedByDate = "";
            mShipFromOrg = "";
            mShipFromOrgId = "";
            mShipmentNumber = "";
            mShipmentHeaderId = "";
            mOrderHeaderId = "";
            mOrderNumber = "";
        }

    /*************************************************************************
     *   NAME: public String getmREQHeaderId() 
     *
     *   PURPOSE:   returns the Printer Name 
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/

    public String getmREQHeaderId() {
        return mREQHeaderId;
    }
      
     /*************************************************************************
     *   NAME: public String getmREQNumber()
     *
     *   PURPOSE:   returns the Printer Description
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/
           
    public String getmREQNumber() {
        return mREQNumber;
    }

    /*************************************************************************
    *   NAME: public String getmNoteToReceiver()
    *
    *   PURPOSE:   returns the Printer Description
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       18-FEB-2015   Lee Spitzer             Initial Version -
    **************************************************************************/
          
    public String getmShipFromOrg() {
       return mShipFromOrg;
    }

    /*************************************************************************
    *   NAME: public String getmNeedByDate()
    *
    *   PURPOSE:   returns the Printer Description
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       18-FEB-2015   Lee Spitzer             Initial Version -
    **************************************************************************/
          
    public String getmNeedByDate() {
       return mNeedByDate;
    }


    /*************************************************************************
     *   NAME: public String getmVendorName() 
     *
     *   PURPOSE:   returns the Printer Name 
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/

    public String getmShipFromOrgId() {
        return mShipFromOrgId;
    }


    /*************************************************************************
     *   NAME: public String getmVendorNumber() 
     *
     *   PURPOSE:   returns the Printer Name 
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/

    public String getmShipmentNumber() {
        return mShipmentNumber;
    }

    /*************************************************************************
     *   NAME: public String getmVendorId() 
     *
     *   PURPOSE:   returns the Printer Name 
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/

    public String getmShipmentHeaderId() {
        return mShipmentHeaderId;
    }
    
    /*************************************************************************
     *   NAME: public String getmOrderHeaderId() 
     *
     *   PURPOSE:   returns the Printer Name 
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/

    public String getmOrderHeaderId() {
        return mOrderHeaderId;
    }

    /*************************************************************************
     *   NAME: public String getmOrderNumber() 
     *
     *   PURPOSE:   returns the Printer Name 
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/

    public String getmOrderNumber() {
        return mOrderNumber;
    }    
    
    /*************************************************************************
    *   NAME: public void setmREQNumber(String setmREQNumber)
    *
    *   PURPOSE:  sets the mPrinterName
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       18-FEB-2015   Lee Spitzer             Initial Version -
    **************************************************************************/
    
    public void setmREQNumber(String setmREQNumber) {
        mREQNumber = setmREQNumber;
    }
    
    /*************************************************************************
    *   NAME: public void setSubPrompts(Session session)
    *
    *   PURPOSE:  sets the query header values displayed in the list of values
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       18-FEB-2015   Lee Spitzer             Initial Version -
    **************************************************************************/
    
    public void setSubPrompts(Session session) {
        String as1[] = { "REQ_HEADER_ID", "REQ_NUMBER", "NEED_BY_DATE", "SHIP_FROM_ORG", "SHIP_FROM_ORG_ID", "SHIPMENT_NUM", "SHIPMENT_HEADER_ID", "OE_HEADER_ID", "ISO_NUMBER"};
        
    }
}
