/*************************************************************************
   *   $Header XXWCStatusFlagLOV.java $
   *   Module Name: XXWCStatusFlagLOV
   *   
   *   Package package xxwc.oracle.apps.inv.lov.server;
   *   
   *   Imports Classes
   *   
   *   import java.sql.SQLException;
   *   import java.util.StringTokenizer;
   *   import java.util.Vector;
   *   import oracle.apps.fnd.common.*;
   *   import oracle.apps.inv.lov.server.*;
   *   import oracle.apps.inv.utilities.server.UtilFns;
   *   import oracle.apps.mwa.beans.*;
   *   import oracle.apps.mwa.container.MWALib;
   *   import oracle.apps.mwa.container.Session;
   *   import oracle.apps.mwa.eventmodel.*;
   *   import xxwc.oracle.apps.inv.bins.server.XXWCPrimaryBinAssignment;
   *   
   *   PURPOSE:   XXWCStatusFlagLOV List of Values for Fnd Flex Values where Flex Value Set is XXWC_HOLD_OR_CLOSE
   *   
   *    
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
        1.0       06-FEB-2015   Lee Spitzer             Initial Version - 
                                                           TMS Ticket 20150302-00152 - RF - Receiving
**************************************************************************/
package xxwc.oracle.apps.inv.lov.server;

import java.sql.SQLException;
import java.util.StringTokenizer;
import java.util.Vector;
import oracle.apps.fnd.common.*;
import oracle.apps.inv.lov.server.*;
import oracle.apps.inv.utilities.server.UtilFns;
import oracle.apps.mwa.beans.*;
import oracle.apps.mwa.container.MWALib;
import oracle.apps.mwa.container.Session;
import oracle.apps.mwa.eventmodel.*;

    /*************************************************************************
     *   NAME: XXWCStatusFlagLOV extends InvLOVFieldBean implements MWAFieldListener
     *
     *   PURPOSE:   Main class for XXWCStatusFlag 
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       06-FEB-2015   Lee Spitzer             Initial Version -
                                                             TMS Ticket 20150302-00152 - RF - Receiving
     **************************************************************************/

public class XXWCStatusFlagLOV extends InvLOVFieldBean implements MWAFieldListener {
 
        public static final String RCS_ID = "$Header: XXWCStatusFlagLOV.java 120.6 2014/10/21 15:24:59 aambulka ship $";
        public static final boolean RCS_ID_RECORDED = VersionInfo.recordClassVersion("$Header: XXWCStatusFlagLOV.java 120.6 2014/10/21 15:24:59 aambulka ship $", "xxwc.oracle.apps.inv.lov.server");
        private Session ses;
        private String mFlexValue;
        private String mFlexValueMeaning;
        private String mFlexValueDescription;
        private boolean areThePromptsSet;
          
        /*************************************************************************
         *   NAME: public XXWCStatusFlagLOV
         *
         *   PURPOSE:   main method XXWCStatusFlagLOV
         *
         *   REVISIONS:
         *   Ver        Date        Author                     Description
         *   ---------  ----------  ---------------         -------------------------
                 1.0       06-FEB-2015   Lee Spitzer             Initial Version -
         **************************************************************************/
        
        public XXWCStatusFlagLOV() {
            areThePromptsSet = false;
            addListener(this);
            clear();
            initDefaultType();
              
        }
        
        /*************************************************************************
         *   NAME: private void initDefaultType()
         *
         *   PURPOSE:   sets the paramType, prompts, lovstatement, subfield and flags 
         *
         *   REVISIONS:
         *   Ver        Date        Author                     Description
         *   ---------  ----------  ---------------         -------------------------
                 1.0       06-FEB-2015   Lee Spitzer             Initial Version -
         **************************************************************************/
        
        private void initDefaultType() {
            String paramType[] = { "C", "S"};
            //String paramType[] = { "C" };
            String prompts[] = { "VALUE" , "MEANING", "DESCRIPTION"};
            boolean flag[] = {true, false, true};
            setlovStatement("XXWC_MWA_ROUTINES_PKG.GET_TS_LABEL_LOV");
            setInputParameterTypes(paramType);
            setSubfieldPrompts(prompts);
            setSubfieldDisplays(flag);      
        }
        
        /*************************************************************************
         *   NAME: public void fieldEntered(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException, DefaultOnlyHandlerException
         *
         *   PURPOSE:   fieldEntered method sets the connection and subPrompts
         *
         *   REVISIONS:
         *   Ver        Date        Author                     Description
         *   ---------  ----------  ---------------         -------------------------
                 1.0       06-FEB-2015   Lee Spitzer             Initial Version -
         **************************************************************************/
          
        public void fieldEntered(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException, DefaultOnlyHandlerException{
            ses = mwaevent.getSession();
            java.sql.Connection connection = ses.getConnection();
            setSubPrompts(mwaevent.getSession());
           }
        
        /*************************************************************************
         *   NAME: public void fieldExited(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException, DefaultOnlyHandlerException
         *
         *   PURPOSE:   fieldExited method calls the setReturnValues from the LOV Query
         *
         *   REVISIONS:
         *   Ver        Date        Author                     Description
         *   ---------  ----------  ---------------         -------------------------
                 1.0       06-FEB-2015   Lee Spitzer             Initial Version -
         **************************************************************************/
        
        
        public void fieldExited(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException, DefaultOnlyHandlerException {
            ses = mwaevent.getSession();
            try {
                    if(mwaevent.getAction().equals("MWA_SUBMIT") && isPopulated())
                    {
                        setReturnValues();
                    }
                }
            catch(Exception e){
                UtilFns.error("Error in calling fieldExited " + e);
                
            }
            
            if(getValue().equals(""))
                    {
                        clear();
                    }
        }
        
        /*************************************************************************
         *   NAME: private void setReturnValues()
         *
         *   PURPOSE:   sets the return values from the list of values query
         *
         *   REVISIONS:
         *   Ver        Date        Author                     Description
         *   ---------  ----------  ---------------         -------------------------
                 1.0       06-FEB-2015   Lee Spitzer             Initial Version -
         **************************************************************************/
        
        private void setReturnValues()
            { 
                mFlexValue = "";
                mFlexValueMeaning = "";
                mFlexValueDescription = "";
                Vector vector = getSelectedValues();
                if(vector != null) {
                    int i = vector.size();
                    mFlexValue = (String)vector.elementAt(0);
                    mFlexValueMeaning = (String)vector.elementAt(1);
                    mFlexValueDescription = (String)vector.elementAt(2);
                }
            }
        
        /*************************************************************************
         *   NAME: public void clear
         *
         *   PURPOSE:   clears the returned list of values
         *
         *   REVISIONS:
         *   Ver        Date        Author                     Description
         *   ---------  ----------  ---------------         -------------------------
                 1.0       06-FEB-2015   Lee Spitzer             Initial Version -
         **************************************************************************/
        
        public void clear()
            {
                setValue("");
                mFlexValue = "";
                mFlexValueMeaning = "";
                mFlexValueDescription = "";
            }

        /*************************************************************************
         *   NAME: public String getmFlexValue() 
         *
         *   PURPOSE:   returns the Flex Value 
         *
         *   REVISIONS:
         *   Ver        Date        Author                     Description
         *   ---------  ----------  ---------------         -------------------------
                 1.0       06-FEB-2015   Lee Spitzer             Initial Version -
         **************************************************************************/

        public String getmFlexValue() {
            return mFlexValue;
        }
          
         /*************************************************************************
         *   NAME: public String getmFlexValueMeaning()
         *
         *   PURPOSE:   returns the Flex Value meaning value 
         *
         *   REVISIONS:
         *   Ver        Date        Author                     Description
         *   ---------  ----------  ---------------         -------------------------
                 1.0       06-FEB-2015   Lee Spitzer             Initial Version -
         **************************************************************************/
               
        public String getmFlexValueMeaning() {
            return mFlexValueMeaning;
        }
        
        /*************************************************************************
        *   NAME: public String getmFlexValueDescription()
        *
        *   PURPOSE:   returns the Flex Value Description
        *
        *   REVISIONS:
        *   Ver        Date        Author                     Description
        *   ---------  ----------  ---------------         -------------------------
                1.0       06-FEB-2015   Lee Spitzer             Initial Version -
        **************************************************************************/
        
        public String getmFlexValueDescription() {
            return mFlexValueDescription;
        }
        
        /*************************************************************************
        *   NAME: public void setmFlexValue(String setmFlexValue)
        *
        *   PURPOSE:  sets the mfg_lookup lookup_code value
        *
        *   REVISIONS:
        *   Ver        Date        Author                     Description
        *   ---------  ----------  ---------------         -------------------------
                1.0       06-FEB-2015   Lee Spitzer             Initial Version -
        **************************************************************************/
        
        public void setmFlexValue(String setmFlexValue) {
            mFlexValue = setmFlexValue;
        }
        
        /*************************************************************************
        *   NAME: public void setSubPrompts(Session session)
        *
        *   PURPOSE:  sets the query header values displayed in the list of values
        *
        *   REVISIONS:
        *   Ver        Date        Author                     Description
        *   ---------  ----------  ---------------         -------------------------
                1.0       06-FEB-2015   Lee Spitzer             Initial Version -
        **************************************************************************/
        
        public void setSubPrompts(Session session) {
            String as1[] = { "VALUE" , "MEANING", "DESCRIPTION"};
        }
    }
