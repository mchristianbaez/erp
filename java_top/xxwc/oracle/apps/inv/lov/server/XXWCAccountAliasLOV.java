/*************************************************************************
   *   $Header XXWCAccountAliasLOV.java $
   *   Module Name: XXWCAccountAliasLOV
   *   
   *   Package package xxwc.oracle.apps.inv.lov.server;
   *   
   *   Imports Classes
   *   
   *   import java.sql.SQLException;
   *   import java.util.StringTokenizer;
   *   import java.util.Vector;
   *   import oracle.apps.fnd.common.*;
   *   import oracle.apps.inv.lov.server.*;
   *   import oracle.apps.inv.utilities.server.UtilFns;
   *   import oracle.apps.mwa.beans.*;
   *   import oracle.apps.mwa.container.MWALib;
   *   import oracle.apps.mwa.container.Session;
   *   import oracle.apps.mwa.eventmodel.*;
   *   import xxwc.oracle.apps.inv.bins.server.XXWCPrimaryBinAssignment;
   *   
   *   PURPOSE:   XXWCAccountAliasLOV List of Values for Account Alias RF Scanning
   *   
   *    
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
        1.0       20-JUN-2015   Lee Spitzer             Initial Version - 
                                                           TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
**************************************************************************/

package xxwc.oracle.apps.inv.lov.server;

import java.sql.SQLException;
import java.util.StringTokenizer;
import java.util.Vector;
import oracle.apps.fnd.common.*;
import oracle.apps.inv.lov.server.*;
import oracle.apps.inv.utilities.server.UtilFns;
import oracle.apps.mwa.beans.*;
import oracle.apps.mwa.container.MWALib;
import oracle.apps.mwa.container.Session;
import oracle.apps.mwa.eventmodel.*;

/*************************************************************************
 *   NAME: XXWCAccountAliasLOV extends InvLOVFieldBean implements MWAFieldListener
 *
 *   PURPOSE:   Main class for XXWCAccountAliasLOV 
 *
 *   REVISIONS:
 *   Ver        Date        Author                     Description
 *   ---------  ----------  ---------------         -------------------------
        1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                         TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
 **************************************************************************/

public class XXWCAccountAliasLOV extends InvLOVFieldBean implements MWAFieldListener{
    public static final String RCS_ID = "$Header: XXWCAccountAliasLOV.java 120.6 2014/10/21 15:24:59 aambulka ship $";
    public static final boolean RCS_ID_RECORDED = VersionInfo.recordClassVersion("$Header: XXWCAccountAliasLOV.java 120.6 2014/10/21 15:24:59 aambulka ship $", "xxwc.oracle.apps.inv.lov.server");
    private Session ses;
    private String mDispositionId;
    private String mDescription;
    private String mConcatenatedSegments;
    private boolean areThePromptsSet;
      
    /*************************************************************************
     *   NAME: public XXWCAccountAliasLOV
     *
     *   PURPOSE:   main method XXWCAccountAliasLOV
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
     **************************************************************************/
    
    public XXWCAccountAliasLOV() {
        areThePromptsSet = false;
        addListener(this);
        clear();
        initDefaultType();
    
    }
    /*************************************************************************
     *   NAME: private void initDefaultType()
     *
     *   PURPOSE:   sets the paramType, prompts, lovstatement, subfield and flags 
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                 TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
     **************************************************************************/
    
    private void initDefaultType() {
        String paramType[] = { "C", "N", "S"};
        String prompts[] = { "DISPOSITION_ID","CONCATENATED_SEGMENTS", "DESCRIPTION"};
        boolean flag[] = {false, true, true};
        setlovStatement("XXWC_MWA_ROUTINES_PKG.GET_ACCOUNT_ALIAS_LOV");
        setInputParameterTypes(paramType);
        setSubfieldPrompts(prompts);
        setSubfieldDisplays(flag);      
    }
    
     /*************************************************************************
     *   NAME: public void fieldEntered(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException, DefaultOnlyHandlerException
     *
     *   PURPOSE:   fieldEntered method sets the connection and subPrompts
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
     **************************************************************************/
      
    public void fieldEntered(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException, DefaultOnlyHandlerException{
        ses = mwaevent.getSession();
        java.sql.Connection connection = ses.getConnection();
        setSubPrompts(mwaevent.getSession());
       }
    
    /*************************************************************************
     *   NAME: public void fieldExited(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException, DefaultOnlyHandlerException
     *
     *   PURPOSE:   fieldExited method calls the setReturnValues from the LOV Query
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
     **************************************************************************/
    
    
    public void fieldExited(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException, DefaultOnlyHandlerException {
        ses = mwaevent.getSession();
        try {
                if(mwaevent.getAction().equals("MWA_SUBMIT") && isPopulated())
                {
                    setReturnValues();
                }
            }
        catch(Exception e){
            UtilFns.error("Error in calling fieldExited " + e);
            
        }
        
        if(getValue().equals(""))
                {
                    clear();
                }
    }
    
    /*************************************************************************
     *   NAME: private void setReturnValues()
     *
     *   PURPOSE:   sets the return values from the list of values query
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
     **************************************************************************/
    
    private void setReturnValues()
        { 
            mDispositionId = "";
            mConcatenatedSegments = "";
            mDescription = "";
            
            Vector vector = getSelectedValues();
            if(vector != null) {
                int i = vector.size();
                mDispositionId = (String)vector.elementAt(0);
                mConcatenatedSegments = (String)vector.elementAt(1);
                mDescription = (String)vector.elementAt(2);
            }
        }
    
    /*************************************************************************
     *   NAME: public void clear
     *
     *   PURPOSE:   clears the returned list of values
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
     **************************************************************************/
    
    public void clear()
        {
            setValue("");
            mDispositionId = "";
            mConcatenatedSegments = "";
            mDescription = "";
            
        }

     
    /*************************************************************************
    *   NAME: public String getmDispositionId()
    *
    *   PURPOSE:   returns the mDispositionId
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
    **************************************************************************/
          
    public String getmDispositionId() {
       return mDispositionId;
    }

    /*************************************************************************
    *   NAME: public String getmConcatenatedSegments()
    *
    *   PURPOSE:   returns the mConcatenatedSegments
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
    **************************************************************************/
          
    public String getmConcatenatedSegments() {
       return mConcatenatedSegments;
    }
    
    
    /*************************************************************************
     *   NAME: public String getmDescription() 
     *
     *   PURPOSE:   returns the Description
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
     **************************************************************************/

    public String getmDescription() {
        return mDescription;
    }
     
    /*************************************************************************
    *   NAME: public void setmDispositionId(String setmDispositionId)
    *
    *   PURPOSE:  sets the mDispositionId
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
    **************************************************************************/
    
    public void setmDispositionId(String setmDispositionId) {
        mDispositionId = setmDispositionId;
    }
    
    /*************************************************************************
    *   NAME: public void setSubPrompts(Session session)
    *
    *   PURPOSE:  sets the query header values displayed in the list of values
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
    **************************************************************************/
    
    public void setSubPrompts(Session session) {
        String as1[] = { "DISPOSITION_ID","CONCATENATED_SEGMENTS", "DESCRIPTION"};
    }
}
