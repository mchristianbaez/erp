/*************************************************************************
   *   $Header XXWCPrinterLOV.java $
   *   Module Name: XXWCPrinterLOV
   *   
   *   Package package xxwc.oracle.apps.inv.lov.server;
   *   
   *   Imports Classes
   *   
   *   import java.sql.SQLException;
   *   import java.util.StringTokenizer;
   *   import java.util.Vector;
   *   import oracle.apps.fnd.common.*;
   *   import oracle.apps.inv.lov.server.*;
   *   import oracle.apps.inv.utilities.server.UtilFns;
   *   import oracle.apps.mwa.beans.*;
   *   import oracle.apps.mwa.container.MWALib;
   *   import oracle.apps.mwa.container.Session;
   *   import oracle.apps.mwa.eventmodel.*;
   *   import xxwc.oracle.apps.inv.bins.server.XXWCPrimaryBinAssignment;
   *   
   *   PURPOSE:   XXWCPrinterLOV List of Values for MFG_LOOKUPS where lookupt code is MTL_DEFAULT_LOCATORS
   *   
   *    
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
        1.0       21-OCT-2014   Lee Spitzer             Initial Version - 
                                                           TMS Ticket 20150302-00152 - RF - Receiving
**************************************************************************/

package xxwc.oracle.apps.inv.lov.server;

import java.sql.SQLException;
import java.util.StringTokenizer;
import java.util.Vector;
import oracle.apps.fnd.common.*;
import oracle.apps.inv.lov.server.*;
import oracle.apps.inv.utilities.server.UtilFns;
import oracle.apps.mwa.beans.*;
import oracle.apps.mwa.container.MWALib;
import oracle.apps.mwa.container.Session;
import oracle.apps.mwa.eventmodel.*;

/*************************************************************************
 *   NAME: XXWCPrinterLOV extends InvLOVFieldBean implements MWAFieldListener
 *
 *   PURPOSE:   Main class for XXWCPrinterLOV 
 *
 *   REVISIONS:
 *   Ver        Date        Author                     Description
 *   ---------  ----------  ---------------         -------------------------
        1.0       21-OCT-2014   Lee Spitzer             Initial Version -
                                                         TMS Ticket 20150302-00152 - RF - Receiving
 **************************************************************************/

public class XXWCPrintersLOV extends InvLOVFieldBean implements MWAFieldListener{
    public static final String RCS_ID = "$Header: XXWCPrinterLOV.java 120.6 2014/10/21 15:24:59 aambulka ship $";
    public static final boolean RCS_ID_RECORDED = VersionInfo.recordClassVersion("$Header: XXWCPrinterLOV.java 120.6 2014/10/21 15:24:59 aambulka ship $", "xxwc.oracle.apps.inv.lov.server");
    private Session ses;
    private String mPrinterName;
    private String mPrinterDescription;
    private boolean areThePromptsSet;
      
    /*************************************************************************
     *   NAME: public XXWCPrintersLOV
     *
     *   PURPOSE:   main method XXWCPrintersLOV
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       21-OCT-2014   Lee Spitzer             Initial Version -
     **************************************************************************/
    
    public XXWCPrintersLOV() {
        areThePromptsSet = false;
        addListener(this);
        clear();
        initDefaultType();
          
    }
    
    /*************************************************************************
     *   NAME: private void initDefaultType()
     *
     *   PURPOSE:   sets the paramType, prompts, lovstatement, subfield and flags 
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       21-OCT-2014   Lee Spitzer             Initial Version -
     **************************************************************************/
    
    private void initDefaultType() {
        String paramType[] = { "C", "S"};
        String prompts[] = { "PRINTER_NAME" , "DESCRIPTION"};
        boolean flag[] = {true, true};
        setlovStatement("XXWC_LABEL_PRINTERS_PKG.GET_PRINTERS_LOV");
        setInputParameterTypes(paramType);
        setSubfieldPrompts(prompts);
        setSubfieldDisplays(flag);      
    }
    
    /*************************************************************************
     *   NAME: public void fieldEntered(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException, DefaultOnlyHandlerException
     *
     *   PURPOSE:   fieldEntered method sets the connection and subPrompts
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       21-OCT-2014   Lee Spitzer             Initial Version -
     **************************************************************************/
      
    public void fieldEntered(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException, DefaultOnlyHandlerException{
        ses = mwaevent.getSession();
        java.sql.Connection connection = ses.getConnection();
        setSubPrompts(mwaevent.getSession());
       }
    
    /*************************************************************************
     *   NAME: public void fieldExited(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException, DefaultOnlyHandlerException
     *
     *   PURPOSE:   fieldExited method calls the setReturnValues from the LOV Query
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       21-OCT-2014   Lee Spitzer             Initial Version -
     **************************************************************************/
    
    
    public void fieldExited(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException, DefaultOnlyHandlerException {
        ses = mwaevent.getSession();
        try {
                if(mwaevent.getAction().equals("MWA_SUBMIT") && isPopulated())
                {
                    setReturnValues();
                }
            }
        catch(Exception e){
            UtilFns.error("Error in calling fieldExited " + e);
            
        }
        
        if(getValue().equals(""))
                {
                    clear();
                }
    }
    
    /*************************************************************************
     *   NAME: private void setReturnValues()
     *
     *   PURPOSE:   sets the return values from the list of values query
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       21-OCT-2014   Lee Spitzer             Initial Version -
     **************************************************************************/
    
    private void setReturnValues()
        { 
            mPrinterName = "";
            mPrinterDescription = "";
            Vector vector = getSelectedValues();
            if(vector != null) {
                int i = vector.size();
                mPrinterName = (String)vector.elementAt(0);
                mPrinterDescription = (String)vector.elementAt(1);
            }
        }
    
    /*************************************************************************
     *   NAME: public void clear
     *
     *   PURPOSE:   clears the returned list of values
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       21-OCT-2014   Lee Spitzer             Initial Version -
     **************************************************************************/
    
    public void clear()
        {
            setValue("");
            mPrinterName = "";
            mPrinterDescription = "";
        }

    /*************************************************************************
     *   NAME: public String getmPrinterName() 
     *
     *   PURPOSE:   returns the Printer Name 
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       21-OCT-2014   Lee Spitzer             Initial Version -
     **************************************************************************/

    public String getmPrinterName() {
        return mPrinterName;
    }
      
     /*************************************************************************
     *   NAME: public String getmPrinterDescription()
     *
     *   PURPOSE:   returns the Printer Description
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       21-OCT-2014   Lee Spitzer             Initial Version -
     **************************************************************************/
           
    public String getmPrinterDescription() {
        return mPrinterDescription;
    }
    
    /*************************************************************************
    *   NAME: public void setmPrinterName(String setmPrinterName)
    *
    *   PURPOSE:  sets the mPrinterName
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       21-OCT-2014   Lee Spitzer             Initial Version -
    **************************************************************************/
    
    public void setmPrinterName(String setmPrinterName) {
        mPrinterName = setmPrinterName;
    }
    
    /*************************************************************************
    *   NAME: public void setSubPrompts(Session session)
    *
    *   PURPOSE:  sets the query header values displayed in the list of values
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       21-OCT-2014   Lee Spitzer             Initial Version -
    **************************************************************************/
    
    public void setSubPrompts(Session session) {
        String as1[] = {"PRINTER_NAME", "DESCRIPTION"};
    }
}
