/*************************************************************************
   *   $Header XXWCRMALOV.java $
   *   Module Name: XXWCRMALOV 
   *   
   *   Package package xxwc.oracle.apps.inv.lov.server;
   *   
   *   Imports Classes
   *   
   *   import java.sql.SQLException;
   *   import java.util.StringTokenizer;
   *   import java.util.Vector;
   *   import oracle.apps.fnd.common.*;
   *   import oracle.apps.inv.lov.server.*;
   *   import oracle.apps.inv.utilities.server.UtilFns;
   *   import oracle.apps.mwa.beans.*;
   *   import oracle.apps.mwa.container.MWALib;
   *   import oracle.apps.mwa.container.Session;
   *   import oracle.apps.mwa.eventmodel.*;
   *   import xxwc.oracle.apps.inv.bins.server.XXWCPrimaryBinAssignment;
   *   
   *   PURPOSE:   XXWCRMALOV  List of Values for MTL_SYSTM_ITEMS_KFV and MTL_CROSS_REFERENCES for Item and UPC Scanning
   *   
   *    
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
        1.0       18-FEB-2015   Lee Spitzer             Initial Version - 
                                                           TMS Ticket 20150302-00152 - RF - Receiving
**************************************************************************/

package xxwc.oracle.apps.inv.lov.server;

import java.sql.SQLException;
import java.util.StringTokenizer;
import java.util.Vector;
import oracle.apps.fnd.common.*;
import oracle.apps.inv.lov.server.*;
import oracle.apps.inv.utilities.server.UtilFns;
import oracle.apps.mwa.beans.*;
import oracle.apps.mwa.container.MWALib;
import oracle.apps.mwa.container.Session;
import oracle.apps.mwa.eventmodel.*;

/*************************************************************************
 *   NAME: XXWCRMALOV  extends InvLOVFieldBean implements MWAFieldListener
 *
 *   PURPOSE:   Main class for XXWCRMALOV  
 *
 *   REVISIONS:
 *   Ver        Date        Author                     Description
 *   ---------  ----------  ---------------         -------------------------
        1.0       18-FEB-2015   Lee Spitzer             Initial Version -
                                                         TMS Ticket 20150302-00152 - RF - Receiving
 **************************************************************************/

public class XXWCRMALOV extends InvLOVFieldBean implements MWAFieldListener{
    public static final String RCS_ID = "$Header: XXWCRMALOV .java 120.6 2014/10/21 15:24:59 aambulka ship $";
    public static final boolean RCS_ID_RECORDED = VersionInfo.recordClassVersion("$Header: XXWCRMALOV .java 120.6 2014/10/21 15:24:59 aambulka ship $", "xxwc.oracle.apps.inv.lov.server");
    private Session ses;
    private String mRMAHeaderId;
    private String mRMANumber;
    private String mRMACreationDate;
    private String mCustAccountId;
    private String mPartyId;
    private String mAccountNumber;
    private String mPartyName;
    private boolean areThePromptsSet;
      
    /*************************************************************************
     *   NAME: public XXWCRMALOV 
     *
     *   PURPOSE:   main method XXWCRMALOV 
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/
    
    public XXWCRMALOV () {
        areThePromptsSet = false;
        addListener(this);
        clear();
        initDefaultType();
          
    }
    
    /*************************************************************************
     *   NAME: private void initDefaultType()
     *
     *   PURPOSE:   sets the paramType, prompts, lovstatement, subfield and flags 
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/
    
    private void initDefaultType() {
        String paramType[] = { "C", "N", "S"};
        String prompts[] = { "HEADER_ID", "RMA_NUMBER", "CREATION_DATE", "CUST_ACCOUNT_ID", "PARTY_ID", "ACCOUNT_NUMBER", "CUSTOMER"};
        boolean flag[] = {false, true, true, false, false, true, true};
        setlovStatement("XXWC_RCV_MOB_PKG.GET_RMA_LOV");
        setInputParameterTypes(paramType);
        setSubfieldPrompts(prompts);
        setSubfieldDisplays(flag);      
    }
    
    /*************************************************************************
     *   NAME: public void fieldEntered(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException, DefaultOnlyHandlerException
     *
     *   PURPOSE:   fieldEntered method sets the connection and subPrompts
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/
      
    public void fieldEntered(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException, DefaultOnlyHandlerException{
        ses = mwaevent.getSession();
        java.sql.Connection connection = ses.getConnection();
        setSubPrompts(mwaevent.getSession());
       }
    
    /*************************************************************************
     *   NAME: public void fieldExited(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException, DefaultOnlyHandlerException
     *
     *   PURPOSE:   fieldExited method calls the setReturnValues from the LOV Query
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/
    
    
    public void fieldExited(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException, DefaultOnlyHandlerException {
        ses = mwaevent.getSession();
        try {
                if(mwaevent.getAction().equals("MWA_SUBMIT") && isPopulated())
                {
                    setReturnValues();
                }
            }
        catch(Exception e){
            UtilFns.error("Error in calling fieldExited " + e);
            
        }
        
        if(getValue().equals(""))
                {
                    clear();
                }
    }
    
    /*************************************************************************
     *   NAME: private void setReturnValues()
     *
     *   PURPOSE:   sets the return values from the list of values query
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/
    
    private void setReturnValues()
        { 
            mRMAHeaderId = "";
            mRMANumber = "";
            mRMACreationDate = "";
            mCustAccountId = "";
            mPartyId = "";
            mAccountNumber = "";
            mPartyName = "";
            
            Vector vector = getSelectedValues();
            if(vector != null) {
                int i = vector.size();
                mRMAHeaderId = (String)vector.elementAt(0);
                mRMANumber = (String)vector.elementAt(1);
                mRMACreationDate = (String)vector.elementAt(2);
                mCustAccountId = (String)vector.elementAt(3);
                mPartyId = (String)vector.elementAt(4);
                mAccountNumber = (String)vector.elementAt(5);
                mPartyName = (String)vector.elementAt(6);
            }
        }
    
    /*************************************************************************
     *   NAME: public void clear
     *
     *   PURPOSE:   clears the returned list of values
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/
    
    public void clear()
        {
            setValue("");
            mRMAHeaderId = "";
            mRMANumber = "";
            mRMACreationDate = "";
            mCustAccountId = "";
            mPartyId = "";
            mAccountNumber = "";
            mPartyName = "";
        }

    /*************************************************************************
     *   NAME: public String getmREQHeaderId() 
     *
     *   PURPOSE:   returns the Printer Name 
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/

    public String getmRMAHeaderId() {
        return mRMAHeaderId;
    }
      
     /*************************************************************************
     *   NAME: public String getmREQNumber()
     *
     *   PURPOSE:   returns the Printer Description
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/
           
    public String getmRMANumber() {
        return mRMANumber;
    }

    /*************************************************************************
    *   NAME: public String getmRMACreationDate()
    *
    *   PURPOSE:   returns the Printer Description
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       18-FEB-2015   Lee Spitzer             Initial Version -
    **************************************************************************/
          
    public String getmRMACreationDate() {
       return mRMACreationDate;
    }

    /*************************************************************************
    *   NAME: public String mCustAccountId()
    *
    *   PURPOSE:   returns the Printer Description
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       18-FEB-2015   Lee Spitzer             Initial Version -
    **************************************************************************/
          
    public String getmCustAccountId() {
       return mCustAccountId;
    }


    /*************************************************************************
     *   NAME: public String getmVendorName() 
     *
     *   PURPOSE:   returns the Printer Name 
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/

    public String getmPartyId() {
        return mPartyId;
    }


    /*************************************************************************
     *   NAME: public String getmAccountNumber() 
     *
     *   PURPOSE:   returns the Printer Name 
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/

    public String getmAccountNumber() {
        return mAccountNumber;
    }

    /*************************************************************************
     *   NAME: public String getmVendorId() 
     *
     *   PURPOSE:   returns the Printer Name 
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/

    public String getmPartyName() {
        return mPartyName;
    }
    

    /*************************************************************************
    *   NAME: public void setmREQNumber(String setmREQNumber)
    *
    *   PURPOSE:  sets the mPrinterName
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       18-FEB-2015   Lee Spitzer             Initial Version -
    **************************************************************************/
    
    public void setmRMANumber(String setmRMANumber) {
        mRMANumber = setmRMANumber;
    }
    
    /*************************************************************************
    *   NAME: public void setSubPrompts(Session session)
    *
    *   PURPOSE:  sets the query header values displayed in the list of values
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       18-FEB-2015   Lee Spitzer             Initial Version -
    **************************************************************************/
    
    public void setSubPrompts(Session session) {
        String as1[] = { "HEADER_ID", "RMA_NUMBER", "CREATION_DATE", "CUST_ACCOUNT_ID", "PARTY_ID", "ACCOUNT_NUMBER", "CUSTOMER"};
        
    }
}
