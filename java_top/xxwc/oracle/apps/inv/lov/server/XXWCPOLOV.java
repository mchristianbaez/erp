/*************************************************************************
   *   $Header XXWCPOLOV.java $
   *   Module Name: XXWCPOLOV 
   *   
   *   Package package xxwc.oracle.apps.inv.lov.server;
   *   
   *   Imports Classes
   *   
   *   import java.sql.SQLException;
   *   import java.util.StringTokenizer;
   *   import java.util.Vector;
   *   import oracle.apps.fnd.common.*;
   *   import oracle.apps.inv.lov.server.*;
   *   import oracle.apps.inv.utilities.server.UtilFns;
   *   import oracle.apps.mwa.beans.*;
   *   import oracle.apps.mwa.container.MWALib;
   *   import oracle.apps.mwa.container.Session;
   *   import oracle.apps.mwa.eventmodel.*;
   *   import xxwc.oracle.apps.inv.bins.server.XXWCPrimaryBinAssignment;
   *   
   *   PURPOSE:   XXWCPOLOV  List of Values for MTL_SYSTM_ITEMS_KFV and MTL_CROSS_REFERENCES for Item and UPC Scanning
   *   
   *    
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
        1.0       18-FEB-2015   Lee Spitzer             Initial Version - 
                                                           TMS Ticket 20150302-00152 - RF - Receiving
**************************************************************************/

package xxwc.oracle.apps.inv.lov.server;

import java.sql.SQLException;
import java.util.StringTokenizer;
import java.util.Vector;
import oracle.apps.fnd.common.*;
import oracle.apps.inv.lov.server.*;
import oracle.apps.inv.utilities.server.UtilFns;
import oracle.apps.mwa.beans.*;
import oracle.apps.mwa.container.MWALib;
import oracle.apps.mwa.container.Session;
import oracle.apps.mwa.eventmodel.*;

/*************************************************************************
 *   NAME: XXWCPOLOV  extends InvLOVFieldBean implements MWAFieldListener
 *
 *   PURPOSE:   Main class for XXWCPOLOV  
 *
 *   REVISIONS:
 *   Ver        Date        Author                     Description
 *   ---------  ----------  ---------------         -------------------------
        1.0       18-FEB-2015   Lee Spitzer             Initial Version -
                                                         TMS Ticket 20150302-00152 - RF - Receiving
 **************************************************************************/

public class XXWCPOLOV extends InvLOVFieldBean implements MWAFieldListener{
    public static final String RCS_ID = "$Header: XXWCPOLOV .java 120.6 2014/10/21 15:24:59 aambulka ship $";
    public static final boolean RCS_ID_RECORDED = VersionInfo.recordClassVersion("$Header: XXWCPOLOV .java 120.6 2014/10/21 15:24:59 aambulka ship $", "xxwc.oracle.apps.inv.lov.server");
    private Session ses;
    private String mPOHeaderId;
    private String mPONumber;
    private String mNoteToReceiver;
    private String mNeedByDate;
    private String mVendorNumber;
    private String mVendorName;
    private String mVendorId;
    private String mVendorSiteId;
    private boolean areThePromptsSet;
      
    /*************************************************************************
     *   NAME: public XXWCPOLOV 
     *
     *   PURPOSE:   main method XXWCPOLOV 
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/
    
    public XXWCPOLOV () {
        areThePromptsSet = false;
        addListener(this);
        clear();
        initDefaultType();
          
    }
    
    /*************************************************************************
     *   NAME: private void initDefaultType()
     *
     *   PURPOSE:   sets the paramType, prompts, lovstatement, subfield and flags 
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/
    
    private void initDefaultType() {
        String paramType[] = { "C", "N", "S"};
        String prompts[] = { "PO_HEADER_ID", "PO_NUMBER", "NOTE_TO_RECEIVER", "NEED_BY_DATE", "VENDOR_NUMBER", "VENDOR_NAME", "VENDOR_ID", "VENDOR_SITE_ID"};
        boolean flag[] = {false, true, true, true, true, true, false, false};
        setlovStatement("XXWC_RCV_MOB_PKG.GET_PO_LOV");
        setInputParameterTypes(paramType);
        setSubfieldPrompts(prompts);
        setSubfieldDisplays(flag);      
    }
    
    /*************************************************************************
     *   NAME: public void fieldEntered(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException, DefaultOnlyHandlerException
     *
     *   PURPOSE:   fieldEntered method sets the connection and subPrompts
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/
      
    public void fieldEntered(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException, DefaultOnlyHandlerException{
        ses = mwaevent.getSession();
        java.sql.Connection connection = ses.getConnection();
        setSubPrompts(mwaevent.getSession());
       }
    
    /*************************************************************************
     *   NAME: public void fieldExited(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException, DefaultOnlyHandlerException
     *
     *   PURPOSE:   fieldExited method calls the setReturnValues from the LOV Query
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/
    
    
    public void fieldExited(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException, DefaultOnlyHandlerException {
        ses = mwaevent.getSession();
        try {
                if(mwaevent.getAction().equals("MWA_SUBMIT") && isPopulated())
                {
                    setReturnValues();
                }
            }
        catch(Exception e){
            UtilFns.error("Error in calling fieldExited " + e);
            
        }
        
        if(getValue().equals(""))
                {
                    clear();
                }
    }
    
    /*************************************************************************
     *   NAME: private void setReturnValues()
     *
     *   PURPOSE:   sets the return values from the list of values query
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/
    
    private void setReturnValues()
        { 
            mPOHeaderId = "";
            mPONumber = "";
            mNoteToReceiver = "";
            mNeedByDate = "";
            mVendorNumber = "";
            mVendorName = "";
            mVendorId = "";
            mVendorSiteId = "";
 
            Vector vector = getSelectedValues();
            if(vector != null) {
                int i = vector.size();
                mPOHeaderId = (String)vector.elementAt(0);
                mPONumber = (String)vector.elementAt(1);
                mNoteToReceiver = (String)vector.elementAt(2);
                mNeedByDate = (String)vector.elementAt(3);
                mVendorNumber = (String)vector.elementAt(4);
                mVendorName = (String)vector.elementAt(5);
                mVendorId = (String)vector.elementAt(6);
                mVendorSiteId = (String)vector.elementAt(7);
            }
        }
    
    /*************************************************************************
     *   NAME: public void clear
     *
     *   PURPOSE:   clears the returned list of values
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/
    
    public void clear()
        {
            setValue("");
            mPOHeaderId = "";
            mPONumber = "";
            mNoteToReceiver = "";
            mNeedByDate = "";
            mVendorNumber = "";
            mVendorName = "";
            mVendorId = "";
            mVendorSiteId = "";
        }

    /*************************************************************************
     *   NAME: public String getmPOHeaderId() 
     *
     *   PURPOSE:   returns the Printer Name 
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/

    public String getmPOHeaderId() {
        return mPOHeaderId;
    }
      
     /*************************************************************************
     *   NAME: public String getmPONumber()
     *
     *   PURPOSE:   returns the Printer Description
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/
           
    public String getmPONumber() {
        return mPONumber;
    }

    /*************************************************************************
    *   NAME: public String getmNoteToReceiver()
    *
    *   PURPOSE:   returns the Printer Description
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       18-FEB-2015   Lee Spitzer             Initial Version -
    **************************************************************************/
          
    public String getmNoteToReceiver() {
       return mNoteToReceiver;
    }

    /*************************************************************************
    *   NAME: public String getmNeedByDate()
    *
    *   PURPOSE:   returns the Printer Description
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       18-FEB-2015   Lee Spitzer             Initial Version -
    **************************************************************************/
          
    public String getmNeedByDate() {
       return mNeedByDate;
    }


    /*************************************************************************
     *   NAME: public String getmVendorName() 
     *
     *   PURPOSE:   returns the Printer Name 
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/

    public String getmVendorName() {
        return mVendorName;
    }


    /*************************************************************************
     *   NAME: public String getmVendorNumber() 
     *
     *   PURPOSE:   returns the Printer Name 
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/

    public String getmVendorNumber() {
        return mVendorNumber;
    }

    /*************************************************************************
     *   NAME: public String getmVendorId() 
     *
     *   PURPOSE:   returns the Printer Name 
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/

    public String getmVendorId() {
        return mVendorId;
    }
    
    /*************************************************************************
     *   NAME: public String getmVendorSiteId() 
     *
     *   PURPOSE:   returns the Printer Name 
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/

    public String getmVendorSiteId() {
        return mVendorSiteId;
    }


    /*************************************************************************
    *   NAME: public void setmPONumber(String setmPONumber)
    *
    *   PURPOSE:  sets the mPrinterName
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       18-FEB-2015   Lee Spitzer             Initial Version -
    **************************************************************************/
    
    public void setmPONumber(String setmPONumber) {
        mPONumber = setmPONumber;
    }
    
    /*************************************************************************
    *   NAME: public void setSubPrompts(Session session)
    *
    *   PURPOSE:  sets the query header values displayed in the list of values
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       18-FEB-2015   Lee Spitzer             Initial Version -
    **************************************************************************/
    
    public void setSubPrompts(Session session) {
        String as1[] = { "PO_HEADER_ID", "PO_NUMBER", "NOTE_TO_RECEIVER", "NEED_BY_DATE", "VENDOR_NUMBER", "VENDOR_NAME", "VENDOR_ID", "VENDOR_SITE_ID"};
    }
}
