/*************************************************************************
   *   $Header XXWCVendorLOV.java $
   *   Module Name: XXWCVendorLOV
   *   
   *   Package package xxwc.oracle.apps.inv.lov.server;
   *   
   *   Imports Classes
   *   
   *   import java.sql.SQLException;
   *   import java.util.StringTokenizer;
   *   import java.util.Vector;
   *   import oracle.apps.fnd.common.*;
   *   import oracle.apps.inv.lov.server.*;
   *   import oracle.apps.inv.utilities.server.UtilFns;
   *   import oracle.apps.mwa.beans.*;
   *   import oracle.apps.mwa.container.MWALib;
   *   import oracle.apps.mwa.container.Session;
   *   import oracle.apps.mwa.eventmodel.*;
   *   import xxwc.oracle.apps.inv.bins.server.XXWCPrimaryBinAssignment;
   *   
   *   PURPOSE:   XXWCVendorLOV List of Values for MTL_SYSTM_ITEMS_KFV and MTL_CROSS_REFERENCES for Item and UPC Scanning
   *   
   *    
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
        1.0       20-JUN-2015   Lee Spitzer             Initial Version - 
                                                           TMS Ticket Number 20150622-00004 RF - UPC Update
**************************************************************************/

package xxwc.oracle.apps.inv.lov.server;

import java.sql.SQLException;
import java.util.StringTokenizer;
import java.util.Vector;
import oracle.apps.fnd.common.*;
import oracle.apps.inv.lov.server.*;
import oracle.apps.inv.utilities.server.UtilFns;
import oracle.apps.mwa.beans.*;
import oracle.apps.mwa.container.MWALib;
import oracle.apps.mwa.container.Session;
import oracle.apps.mwa.eventmodel.*;

/*************************************************************************
 *   NAME: XXWCVendorLOV extends InvLOVFieldBean implements MWAFieldListener
 *
 *   PURPOSE:   Main class for XXWCVendorLOV 
 *
 *   REVISIONS:
 *   Ver        Date        Author                     Description
 *   ---------  ----------  ---------------         -------------------------
        1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                         TMS Ticket Number 20150622-00004 RF - UPC Update
 **************************************************************************/

public class XXWCVendorLOV extends InvLOVFieldBean implements MWAFieldListener{
    public static final String RCS_ID = "$Header: XXWCVendorLOV.java 120.6 2014/10/21 15:24:59 aambulka ship $";
    public static final boolean RCS_ID_RECORDED = VersionInfo.recordClassVersion("$Header: XXWCVendorLOV.java 120.6 2014/10/21 15:24:59 aambulka ship $", "xxwc.oracle.apps.inv.lov.server");
    private Session ses;
    private String mVendorId;
    private String mVendorName;
    private String mVendorNumber;
    private boolean areThePromptsSet;
      
    /*************************************************************************
     *   NAME: public XXWCVendorLOV
     *
     *   PURPOSE:   main method XXWCVendorLOV
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                 TMS Ticket Number 20150622-00004 RF - UPC Update
     **************************************************************************/
    
    public XXWCVendorLOV() {
        areThePromptsSet = false;
        addListener(this);
        clear();
        initDefaultType();
    
    }
    /*************************************************************************
     *   NAME: private void initDefaultType()
     *
     *   PURPOSE:   sets the paramType, prompts, lovstatement, subfield and flags 
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                 TMS Ticket Number 20150622-00004 RF - UPC Update
     **************************************************************************/
    
    private void initDefaultType() {
        String paramType[] = { "C", "S"};
        String prompts[] = { "VENDOR_ID","VENDOR_NAME", "VENDOR_NUMBER"};
        boolean flag[] = {false, true, true};
        setlovStatement("XXWC_MWA_ROUTINES_PKG.GET_VENDOR_LOV");
        setInputParameterTypes(paramType);
        setSubfieldPrompts(prompts);
        setSubfieldDisplays(flag);      
    }
    
     /*************************************************************************
     *   NAME: public void fieldEntered(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException, DefaultOnlyHandlerException
     *
     *   PURPOSE:   fieldEntered method sets the connection and subPrompts
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                  TMS Ticket Number 20150622-00004 RF - UPC Update
     **************************************************************************/
      
    public void fieldEntered(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException, DefaultOnlyHandlerException{
        ses = mwaevent.getSession();
        java.sql.Connection connection = ses.getConnection();
        setSubPrompts(mwaevent.getSession());
       }
    
    /*************************************************************************
     *   NAME: public void fieldExited(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException, DefaultOnlyHandlerException
     *
     *   PURPOSE:   fieldExited method calls the setReturnValues from the LOV Query
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                 TMS Ticket Number 20150622-00004 RF - UPC Update
     **************************************************************************/
    
    
    public void fieldExited(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException, DefaultOnlyHandlerException {
        ses = mwaevent.getSession();
        try {
                if(mwaevent.getAction().equals("MWA_SUBMIT") && isPopulated())
                {
                    setReturnValues();
                }
            }
        catch(Exception e){
            UtilFns.error("Error in calling fieldExited " + e);
            
        }
        
        if(getValue().equals(""))
                {
                    clear();
                }
    }
    
    /*************************************************************************
     *   NAME: private void setReturnValues()
     *
     *   PURPOSE:   sets the return values from the list of values query
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                 TMS Ticket Number 20150622-00004 RF - UPC Update
     **************************************************************************/
    
    private void setReturnValues()
        { 
            mVendorId = "";
            mVendorName = "";
            mVendorNumber = "";
            
            Vector vector = getSelectedValues();
            if(vector != null) {
                int i = vector.size();
                mVendorId = (String)vector.elementAt(0);
                mVendorName = (String)vector.elementAt(1);
                mVendorNumber = (String)vector.elementAt(2);
            }
        }
    
    /*************************************************************************
     *   NAME: public void clear
     *
     *   PURPOSE:   clears the returned list of values
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                 TMS Ticket Number 20150622-00004 RF - UPC Update
     **************************************************************************/
    
    public void clear()
        {
            setValue("");
            mVendorId = "";
            mVendorName = "";
            mVendorNumber = "";
            
        }

     
    /*************************************************************************
    *   NAME: public String getmVendorId()
    *
    *   PURPOSE:   returns the mVendorId
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                 TMS Ticket Number 20150622-00004 RF - UPC Update
    **************************************************************************/
          
    public String getmVendorId() {
       return mVendorId;
    }

    /*************************************************************************
    *   NAME: public String getmVendorName()
    *
    *   PURPOSE:   returns the mVendorName
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                 TMS Ticket Number 20150622-00004 RF - UPC Update
    **************************************************************************/
          
    public String getmVendorName() {
       return mVendorName;
    }
    
    
    /*************************************************************************
     *   NAME: public String getmVendorNumber() 
     *
     *   PURPOSE:   returns the mVendorNumber
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150622-00004 RF - UPC Update
    **************************************************************************/

    public String getmVendorNumber() {
        return mVendorNumber;
    }
     
    /*************************************************************************
    *   NAME: public void setmVendorId(String setmVendorId)
    *
    *   PURPOSE:  sets the mVendorId
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                 TMS Ticket Number 20150622-00004 RF - UPC Update
    **************************************************************************/
    
    public void setmVendorId(String setmVendorId) {
        mVendorId = setmVendorId;
    }
    
    /*************************************************************************
    *   NAME: public void setSubPrompts(Session session)
    *
    *   PURPOSE:  sets the query header values displayed in the list of values
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                 TMS Ticket Number 20150622-00004 RF - UPC Update
    **************************************************************************/
    
    public void setSubPrompts(Session session) {
        String as1[] = { "VENDOR_ID","VENDOR_NAME", "VENDOR_NUMBER"};
    }
}
