/*************************************************************************
   *   $Header XXWCPOAcceptanceDeferReasonsLOV.java $
   *   Module Name: XXWCPOAcceptanceDeferReasonsLOV
   *   
   *   Package package xxwc.oracle.apps.inv.lov.server;
   *   
   *   Imports Classes
   *   
   *   import java.sql.SQLException;
   *   import java.util.StringTokenizer;
   *   import java.util.Vector;
   *   import oracle.apps.fnd.common.*;
   *   import oracle.apps.inv.lov.server.*;
   *   import oracle.apps.inv.utilities.server.UtilFns;
   *   import oracle.apps.mwa.beans.*;
   *   import oracle.apps.mwa.container.MWALib;
   *   import oracle.apps.mwa.container.Session;
   *   import oracle.apps.mwa.eventmodel.*;
   *   import xxwc.oracle.apps.inv.bins.server.XXWCPrimaryBinAssignment;
   *   
   *   PURPOSE:   XXWCPOAcceptanceDeferReasonsLOV List of Values for MFG_LOOKUPS where lookupt code is MTL_DEFAULT_LOCATORS
   *   
   *    
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
        1.0       21-OCT-2014   Lee Spitzer             Initial Version - 
                                                           TMS Ticket 20150302-00152 - RF - Receiving
**************************************************************************/

package xxwc.oracle.apps.inv.lov.server;

import java.sql.SQLException;
import java.util.StringTokenizer;
import java.util.Vector;
import oracle.apps.fnd.common.*;
import oracle.apps.inv.lov.server.*;
import oracle.apps.inv.utilities.server.UtilFns;
import oracle.apps.mwa.beans.*;
import oracle.apps.mwa.container.MWALib;
import oracle.apps.mwa.container.Session;
import oracle.apps.mwa.eventmodel.*;

/*************************************************************************
 *   NAME: XXWCPOAcceptanceDeferReasonsLOV extends InvLOVFieldBean implements MWAFieldListener
 *
 *   PURPOSE:   Main class for XXWCDefaultLocatorTypesLOV 
 *
 *   REVISIONS:
 *   Ver        Date        Author                     Description
 *   ---------  ----------  ---------------         -------------------------
        1.0       21-OCT-2014   Lee Spitzer             Initial Version -
                                                         TMS Ticket 20150302-00152 - RF - Receiving
 **************************************************************************/

public class XXWCPOAcceptanceDeferReasonsLOV extends InvLOVFieldBean implements MWAFieldListener{
    public static final String RCS_ID = "$Header: XXWCPOAcceptanceDeferReasonsLOV.java 120.6 2014/10/21 15:24:59 aambulka ship $";
    public static final boolean RCS_ID_RECORDED = VersionInfo.recordClassVersion("$Header: XXWCDefaultLocatorTypesLOV.java 120.6 2014/10/21 15:24:59 aambulka ship $", "xxwc.oracle.apps.inv.lov.server");
    private Session ses;
    private String mLookupCode;
    private String mMeaning;
    private String mDescription;
    private boolean areThePromptsSet;
      
    /*************************************************************************
     *   NAME: public XXWCDefaultLocatorTypesLOV
     *
     *   PURPOSE:   main method XXWCDefaultLocatorTypesLOV
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       21-OCT-2014   Lee Spitzer             Initial Version -
     **************************************************************************/
    
    public XXWCPOAcceptanceDeferReasonsLOV() {
        areThePromptsSet = false;
        addListener(this);
        clear();
        initDefaultType();
          
    }
    
    /*************************************************************************
     *   NAME: private void initDefaultType()
     *
     *   PURPOSE:   sets the paramType, prompts, lovstatement, subfield and flags 
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       21-OCT-2014   Lee Spitzer             Initial Version -
     **************************************************************************/
    
    private void initDefaultType() {
        String paramType[] = { "C", "S"};
        String prompts[] = { "LOOKUP_CODE" , "MEANING", "DESCRIPTION"};
        boolean flag[] = {false, true, true};
        setlovStatement("XXWC_RCV_MOB_PKG.GET_PO_DEFER_ACCEPTANCE_LOV");
        setInputParameterTypes(paramType);
        setSubfieldPrompts(prompts);
        setSubfieldDisplays(flag);      
    }
    
    /*************************************************************************
     *   NAME: public void fieldEntered(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException, DefaultOnlyHandlerException
     *
     *   PURPOSE:   fieldEntered method sets the connection and subPrompts
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       21-OCT-2014   Lee Spitzer             Initial Version -
     **************************************************************************/
      
    public void fieldEntered(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException, DefaultOnlyHandlerException{
        ses = mwaevent.getSession();
        java.sql.Connection connection = ses.getConnection();
        setSubPrompts(mwaevent.getSession());
       }
    
    /*************************************************************************
     *   NAME: public void fieldExited(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException, DefaultOnlyHandlerException
     *
     *   PURPOSE:   fieldExited method calls the setReturnValues from the LOV Query
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       21-OCT-2014   Lee Spitzer             Initial Version -
     **************************************************************************/
    
    
    public void fieldExited(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException, DefaultOnlyHandlerException {
        ses = mwaevent.getSession();
        try {
                if(mwaevent.getAction().equals("MWA_SUBMIT") && isPopulated())
                {
                    setReturnValues();
                }
            }
        catch(Exception e){
            UtilFns.error("Error in calling fieldExited " + e);
            
        }
        
        if(getValue().equals(""))
                {
                    clear();
                }
    }
    
    /*************************************************************************
     *   NAME: private void setReturnValues()
     *
     *   PURPOSE:   sets the return values from the list of values query
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       21-OCT-2014   Lee Spitzer             Initial Version -
     **************************************************************************/
    
    private void setReturnValues()
        { 
            mLookupCode = "";
            mMeaning = "";
            mDescription = "";
            Vector vector = getSelectedValues();
            if(vector != null) {
                int i = vector.size();
                mLookupCode = (String)vector.elementAt(0);
                mMeaning = (String)vector.elementAt(1);
                mDescription = (String)vector.elementAt(2);
            }
        }
    
    /*************************************************************************
     *   NAME: public void clear
     *
     *   PURPOSE:   clears the returned list of values
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       21-OCT-2014   Lee Spitzer             Initial Version -
     **************************************************************************/
    
    public void clear()
        {
            setValue("");
            mLookupCode = "";
            mMeaning = "";
            mDescription = "";
        }

    /*************************************************************************
     *   NAME: public String getmLookupCode() 
     *
     *   PURPOSE:   returns the mfg_lookup lookup_code value 
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       21-OCT-2014   Lee Spitzer             Initial Version -
     **************************************************************************/

    public String getmLookupCode() {
        return mLookupCode;
    }
      
     /*************************************************************************
     *   NAME: public String getmMeaning()
     *
     *   PURPOSE:   returns the mfg_lookup meaning value 
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       21-OCT-2014   Lee Spitzer             Initial Version -
     **************************************************************************/
           
    public String getmMeaning() {
        return mMeaning;
    }
    
    /*************************************************************************
    *   NAME: public String getmDescription()
    *
    *   PURPOSE:   returns the mfg_lookup description value
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       21-OCT-2014   Lee Spitzer             Initial Version -
    **************************************************************************/
    
    public String getmDescription() {
        return mDescription;
    }
    
    /*************************************************************************
    *   NAME: public void setmLookupCode(String setmLookupCode)
    *
    *   PURPOSE:  sets the mfg_lookup lookup_code value
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       21-OCT-2014   Lee Spitzer             Initial Version -
    **************************************************************************/
    
    public void setmLookupCode(String setmLookupCode) {
        mLookupCode = setmLookupCode;
    }
    
    /*************************************************************************
    *   NAME: public void setSubPrompts(Session session)
    *
    *   PURPOSE:  sets the query header values displayed in the list of values
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       21-OCT-2014   Lee Spitzer             Initial Version -
    **************************************************************************/
    
    public void setSubPrompts(Session session) {
        String as1[] = {"LOOKUP_CODE", "MEANING", "DESCRIPTION"};
    }
}
