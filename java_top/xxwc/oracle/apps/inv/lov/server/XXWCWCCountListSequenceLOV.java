/*************************************************************************
   *   $Header XXWCWCCountListSequenceLOV.java $
   *   Module Name: XXWCWCCountListSequenceLOV
   *   
   *   Package package xxwc.oracle.apps.inv.lov.server;
   *   
   *   Imports Classes
   *   
   *   import java.sql.SQLException;
   *   import java.util.StringTokenizer;
   *   import java.util.Vector;
   *   import oracle.apps.fnd.common.*;
   *   import oracle.apps.inv.lov.server.*;
   *   import oracle.apps.inv.utilities.server.UtilFns;
   *   import oracle.apps.mwa.beans.*;
   *   import oracle.apps.mwa.container.MWALib;
   *   import oracle.apps.mwa.container.Session;
   *   import oracle.apps.mwa.eventmodel.*;
   *   import xxwc.oracle.apps.inv.bins.server.XXWCPrimaryBinAssignment;
   *   
   *   PURPOSE:   XXWCWCCountListSequenceLOV List of Values for xxwc_mtl_cycle_count_entries_v
   *   
   *    
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
        1.0       21-OCT-2014   Lee Spitzer             Initial Version - 
                                                           TMS Ticket 20150302-00173 - RF - Cyclecount
**************************************************************************/

package xxwc.oracle.apps.inv.lov.server;

import java.sql.SQLException;
import java.util.StringTokenizer;
import java.util.Vector;
import oracle.apps.fnd.common.*;
import oracle.apps.inv.lov.server.*;
import oracle.apps.inv.utilities.server.UtilFns;
import oracle.apps.mwa.beans.*;
import oracle.apps.mwa.container.MWALib;
import oracle.apps.mwa.container.Session;
import oracle.apps.mwa.eventmodel.*;

/*************************************************************************
 *   NAME: XXWCWCCountListSequenceLOV extends InvLOVFieldBean implements MWAFieldListener
 *
 *   PURPOSE:   Main class for XXWCWCCountListSequenceLOV 
 *
 *   REVISIONS:
 *   Ver        Date        Author                     Description
 *   ---------  ----------  ---------------         -------------------------
        1.0       21-OCT-2014   Lee Spitzer             Initial Version -
                                                         TMS Ticket 20150302-00173 - RF - Cyclecount   
 **************************************************************************/

public class XXWCWCCountListSequenceLOV extends InvLOVFieldBean implements MWAFieldListener{
    public static final String RCS_ID = "$Header: XXWCDefaultLocatorTypesLOV.java 120.6 2014/10/21 15:24:59 aambulka ship $";
    public static final boolean RCS_ID_RECORDED = VersionInfo.recordClassVersion("$Header: XXWCDefaultLocatorTypesLOV.java 120.6 2014/10/21 15:24:59 aambulka ship $", "xxwc.oracle.apps.inv.lov.server");
    private Session ses;
    private String mWCCountListSequence;
    private String mItemNumber;
    private String mItemDescription;
    private String mItemId;
    private String mSubinventoryCode;
    private String mLocator;
    private String mLocatorId;
    private String mItemRevision;
    private String mPrimaryUOMCode;
    private String mCycleCountEntryId;
    private String mCountListSequence;
    private String mCostGroupId;
    private String mEntryStatusCode;
    private String mRecount;
    private boolean areThePromptsSet;
      
    /*************************************************************************
     *   NAME: public XXWCWCCountListSequenceLOV
     *
     *   PURPOSE:   main method XXWCDefaultLocatorTypesLOV
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       21-OCT-2014   Lee Spitzer             Initial Version -
     **************************************************************************/
    
    public XXWCWCCountListSequenceLOV() {
        areThePromptsSet = false;
        addListener(this);
        clear();
        initDefaultType();
          
    }
    
    /*************************************************************************
     *   NAME: private void initDefaultType()
     *
     *   PURPOSE:   sets the paramType, prompts, lovstatement, subfield and flags 
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       21-OCT-2014   Lee Spitzer             Initial Version -
     **************************************************************************/
    
    private void initDefaultType() {
        String paramType[] = { "C", "S", "S", "S", "S", "S", "S"};
        String prompts[] = { "WC_COUNT_SEQ" , "ITEM", "DESCRIPTION", "ITEM_ID", "SUBINVENTORY", "LOCATOR", "LOCATOR_ID", "PRIMARY_UOM", "CYCLE_COUNT_ENTRY_ID", "ITEM_REVISION", "COUNT_LIST_SEQUENCE", "COST_GROUP_ID", "ENTRY_STATUS_CODE", "RECOUNT"};
        boolean flag[] = {true, true, true, false, true, true, false, false, false, false, false, false, false, true};
        setlovStatement("XXWC_CC_MOBILE_PKG.GET_WC_COUNT_LIST_SEQUENCE_LOV");
        setInputParameterTypes(paramType);
        setSubfieldPrompts(prompts);
        setSubfieldDisplays(flag);      
    }
    
    /*************************************************************************
     *   NAME: public void fieldEntered(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException, DefaultOnlyHandlerException
     *
     *   PURPOSE:   fieldEntered method sets the connection and subPrompts
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       21-OCT-2014   Lee Spitzer             Initial Version -
     **************************************************************************/
      
    public void fieldEntered(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException, DefaultOnlyHandlerException{
        ses = mwaevent.getSession();
        java.sql.Connection connection = ses.getConnection();
        setSubPrompts(mwaevent.getSession());
       }
    
    /*************************************************************************
     *   NAME: public void fieldExited(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException, DefaultOnlyHandlerException
     *
     *   PURPOSE:   fieldExited method calls the setReturnValues from the LOV Query
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       21-OCT-2014   Lee Spitzer             Initial Version -
     **************************************************************************/
    
    
    public void fieldExited(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException, DefaultOnlyHandlerException {
        ses = mwaevent.getSession();
        try {
                if(mwaevent.getAction().equals("MWA_SUBMIT") && isPopulated())
                {
                    setReturnValues();
                }
            }
        catch(Exception e){
            UtilFns.error("Error in calling fieldExited " + e);
            
        }
        
        if(getValue().equals(""))
                {
                    clear();
                }
    }
    
    /*************************************************************************
     *   NAME: private void setReturnValues()
     *
     *   PURPOSE:   sets the return values from the list of values query
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       21-OCT-2014   Lee Spitzer             Initial Version -
     **************************************************************************/
    
    private void setReturnValues()
        { 
            mWCCountListSequence = "";
            mItemNumber = "";
            mItemDescription = "";
            mItemId = "";
            mSubinventoryCode = "";
            mLocator = "";
            mLocatorId = "";
            mItemRevision = "";
            mPrimaryUOMCode = "";
            mCycleCountEntryId = "";
            mCountListSequence = "";
            mCostGroupId = "";
            mEntryStatusCode = "";
            mRecount = "";
            
            
            Vector vector = getSelectedValues();
            if(vector != null) {
                int i = vector.size();
                mWCCountListSequence = (String)vector.elementAt(0);
                mItemNumber = (String)vector.elementAt(1);
                mItemDescription = (String)vector.elementAt(2);
                mItemId = (String)vector.elementAt(3);
                mSubinventoryCode = (String)vector.elementAt(4);
                mLocator = (String)vector.elementAt(5);
                mLocatorId = (String)vector.elementAt(6);
                mItemRevision = (String)vector.elementAt(7);
                mPrimaryUOMCode = (String)vector.elementAt(8);
                mCycleCountEntryId = (String)vector.elementAt(9);
                mCountListSequence = (String)vector.elementAt(10);
                mCostGroupId = (String)vector.elementAt(11);
                mEntryStatusCode = (String)vector.elementAt(12);
                mRecount = (String)vector.elementAt(13);
            }
        }
    
    /*************************************************************************
     *   NAME: public void clear
     *
     *   PURPOSE:   clears the returned list of values
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       21-OCT-2014   Lee Spitzer             Initial Version -
     **************************************************************************/
    
    public void clear()
        {
            setValue("");
            mWCCountListSequence = "";
            mItemNumber = "";
            mItemDescription = "";
            mItemId = "";
            mSubinventoryCode = "";
            mLocator = "";
            mLocatorId = "";
            mItemRevision = "";
            mPrimaryUOMCode = "";
            mCycleCountEntryId = "";
            mCountListSequence = "";
            mCostGroupId = "";
            mEntryStatusCode = "";
            mRecount = "";            
        }

    /*************************************************************************
     *   NAME: public String getmWCCountListSequence() 
     *
     *   PURPOSE:   returns the mWCCountListSequence value 
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       21-OCT-2014   Lee Spitzer             Initial Version -
     **************************************************************************/

    public String getmWCCountListSequence() {
        return mWCCountListSequence;
    }
      
     /*************************************************************************
     *   NAME: public String getmItemNumber()
     *
     *   PURPOSE:   returns the mItemNumber value 
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       21-OCT-2014   Lee Spitzer             Initial Version -
     **************************************************************************/
           
    public String getmItemNumber() {
        return mItemNumber;
    }
    
    /*************************************************************************
    *   NAME: public String getmItemDescription()
    *
    *   PURPOSE:   returns the item description value
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       21-OCT-2014   Lee Spitzer             Initial Version -
    **************************************************************************/
    
    public String getmItemDescription() {
        return mItemDescription;
    }

    /*************************************************************************
     *   NAME: public String getmItemId() 
     *
     *   PURPOSE:   returns the inventory item id value 
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       21-OCT-2014   Lee Spitzer             Initial Version -
     **************************************************************************/

    public String getmItemId() {
        return mItemId;
    }
    
    /*************************************************************************
     *   NAME: public String getmSubinventoryCode() 
     *
     *   PURPOSE:   returns the subinventory code value 
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       21-OCT-2014   Lee Spitzer             Initial Version -
     **************************************************************************/

    public String getmSubinventoryCode() {
        return mSubinventoryCode;
    }
    
    /*************************************************************************
     *   NAME: public String getmLocator() 
     *
     *   PURPOSE:   returns the Locator value 
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       21-OCT-2014   Lee Spitzer             Initial Version -
     **************************************************************************/

    public String getmLocator() {
        return mLocator;
    }
    
    /*************************************************************************
     *   NAME: public String getmLocatorId() 
     *
     *   PURPOSE:   returns the locator id value
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       21-OCT-2014   Lee Spitzer             Initial Version -
     **************************************************************************/

    public String getmLocatorId() {
        return mLocatorId;
    }
    
    /*************************************************************************
     *   NAME: public String getmItemRevision() 
     *
     *   PURPOSE:   returns the item revision value
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       21-OCT-2014   Lee Spitzer             Initial Version -
     **************************************************************************/

    public String getmItemRevision() {
        return mItemRevision;
    }
    
    /*************************************************************************
     *   NAME: public String getmPrimaryUOMCode() 
     *
     *   PURPOSE:   returns the primary uom code value
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       21-OCT-2014   Lee Spitzer             Initial Version -
     **************************************************************************/

    public String getmPrimaryUOMCode() {
        return mPrimaryUOMCode;
    }
    
    /*************************************************************************
     *   NAME: public String getmCycleCountEntryId() 
     *
     *   PURPOSE:   returns the cycle count entry id
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       21-OCT-2014   Lee Spitzer             Initial Version -
     **************************************************************************/

    public String getmCycleCountEntryId() {
        return mCycleCountEntryId;
    }

    /*************************************************************************
     *   NAME: public String getmCountListSequence() 
     *
     *   PURPOSE:   returns the count list sequence number
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       21-OCT-2014   Lee Spitzer             Initial Version -
     **************************************************************************/
    
    public String getmCountListSequence() {
        return mCountListSequence;
    }

    /*************************************************************************
     *   NAME: public String getmCostGroupId() 
     *
     *   PURPOSE:   returns the cost group id
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       21-OCT-2014   Lee Spitzer             Initial Version -
     **************************************************************************/
    
    public String getmCostGroupId() {
        return mCostGroupId;
    }
 
    /*************************************************************************
     *   NAME: public void setSubPrompts(Session session)
     *
     *   PURPOSE:   setSubsPrompts
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       21-OCT-2014   Lee Spitzer             Initial Version -
     **************************************************************************/
   
     public String getmEntryStatusCode() {
         return mEntryStatusCode;
     }
     
     /*************************************************************************
      *   NAME: public void setSubPrompts(Session session)
      *
      *   PURPOSE:   setSubsPrompts
      *
      *   REVISIONS:
      *   Ver        Date        Author                     Description
      *   ---------  ----------  ---------------         -------------------------
              1.0       21-OCT-2014   Lee Spitzer             Initial Version -
      **************************************************************************/
      public String getmRecount() {
          return mRecount;
      }
      
      /*************************************************************************
       *   NAME: public void setSubPrompts(Session session)
       *
       *   PURPOSE:   setSubsPrompts
       *
       *   REVISIONS:
       *   Ver        Date        Author                     Description
       *   ---------  ----------  ---------------         -------------------------
               1.0       21-OCT-2014   Lee Spitzer             Initial Version -
       **************************************************************************/
      
    public void setSubPrompts(Session session) {
        String prompts[] = { "WC_COUNT_SEQ" , "ITEM", "DESCRIPTION", "ITEM_ID", "SUBINVENTORY", "LOCATOR", "LOCATOR_ID", "PRIMARY_UOM", "CYCLE_COUNT_ENTRY_ID", "ITEM_REVISION", "COUNT_LIST_SEQUENCE", "COST_GROUP_ID", "ENTRY_STATUS_CODE", "RECOUNT"};
    }
}
