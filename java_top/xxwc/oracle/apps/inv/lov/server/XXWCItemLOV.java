/*************************************************************************
   *   $Header XXWCItemLOV.java $
   *   Module Name: XXWCItemLOV
   *   
   *   Package package xxwc.oracle.apps.inv.lov.server;
   *   
   *   Imports Classes
   *   
   *   import java.sql.SQLException;
   *   import java.util.StringTokenizer;
   *   import java.util.Vector;
   *   import oracle.apps.fnd.common.*;
   *   import oracle.apps.inv.lov.server.*;
   *   import oracle.apps.inv.utilities.server.UtilFns;
   *   import oracle.apps.mwa.beans.*;
   *   import oracle.apps.mwa.container.MWALib;
   *   import oracle.apps.mwa.container.Session;
   *   import oracle.apps.mwa.eventmodel.*;
   *   import xxwc.oracle.apps.inv.bins.server.XXWCPrimaryBinAssignment;
   *   
   *   PURPOSE:   XXWCItemLOV List of Values for MTL_SYSTM_ITEMS_KFV and MTL_CROSS_REFERENCES for Item and UPC Scanning
   *   
   *    
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
        1.0       18-FEB-2015   Lee Spitzer             Initial Version - 
                                                           TMS Ticket 20150302-00152 - RF - Receiving
**************************************************************************/

package xxwc.oracle.apps.inv.lov.server;

import java.sql.SQLException;
import java.util.StringTokenizer;
import java.util.Vector;
import oracle.apps.fnd.common.*;
import oracle.apps.inv.lov.server.*;
import oracle.apps.inv.utilities.server.UtilFns;
import oracle.apps.mwa.beans.*;
import oracle.apps.mwa.container.MWALib;
import oracle.apps.mwa.container.Session;
import oracle.apps.mwa.eventmodel.*;

/*************************************************************************
 *   NAME: XXWCItemLOV extends InvLOVFieldBean implements MWAFieldListener
 *
 *   PURPOSE:   Main class for XXWCItemLOV 
 *
 *   REVISIONS:
 *   Ver        Date        Author                     Description
 *   ---------  ----------  ---------------         -------------------------
        1.0       18-FEB-2015   Lee Spitzer             Initial Version -
                                                         TMS Ticket 20150302-00152 - RF - Receiving
 **************************************************************************/

public class XXWCItemLOV extends InvLOVFieldBean implements MWAFieldListener{
    public static final String RCS_ID = "$Header: XXWCItemLOV.java 120.6 2014/10/21 15:24:59 aambulka ship $";
    public static final boolean RCS_ID_RECORDED = VersionInfo.recordClassVersion("$Header: XXWCItemLOV.java 120.6 2014/10/21 15:24:59 aambulka ship $", "xxwc.oracle.apps.inv.lov.server");
    private Session ses;
    private String mInventoryItemId;
    private String mItemNumber;
    private String mItemDescription;
    private String mLotControlCode;
    private String mSerialControlCode;
    private String mPrimaryUOMCode;
    private String mShelfLifeDays;
    private String mQuantityOpen;
    private String mRunningQty;
    private String mLovType;
    private String mMinQty;
    private String mMaxQty;
    private String mSalesVelocity;
    private String mWCCountListSequence;
    private String mSubinventory;
    private String mBin;
    private String mWCBin;
    private String mInventoryLocationId;
    private String mCycleCountEntryId;
    private String mLotNumber;
    private String mCountListSequence;
    private String mAdjustmentAmount;
    private String mCostGroupId;
    private String mCountQuantity;
    private String mDueDate;
    private String mPriorCountQuantity;
    private String mSnapShotQuantity;
    private boolean areThePromptsSet;
      
    /*************************************************************************
     *   NAME: public XXWCItemLOV
     *
     *   PURPOSE:   main method XXWCItemLOV
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/
    
    public XXWCItemLOV(String LovType) {
        areThePromptsSet = false;
        addListener(this);
        clear();
        setmLovType(LovType);
        
        if (LovType.equals("ALL")) {
            allItems();
        }
        
        if (LovType.equals("RCV")) {
            rcvItems();
        }
        if (LovType.equals("ITEM")) {
            itemsOnly();
        }
        
        if (LovType.equals("UPC")) {
            upcOnly();
        }
        if (LovType.equals("CYCLE")) {
            cycleItems();
        }
    
    }
    /*************************************************************************
     *   NAME: private void allItems()
     *
     *   PURPOSE:   sets the paramType, prompts, lovstatement, subfield and flags 
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/
    
    private void allItems() {
        String paramType[] = { "C", "N", "S"};
        String prompts[] = { "INVENTORY_ITEM_ID" , "ITEM", "UOM", "DESCRIPTION", "LOT_CONTROL_CODE", "SERIAL_CONTROL_CODE", "SHELF_LIFE_DAYS", "QTY", "RUNNING_QTY"};
        boolean flag[] = {false, true, true, true, false, false, true, false, false};
        setlovStatement("XXWC_MWA_ROUTINES_PKG.GET_WC_ITEM_LOV");
        setInputParameterTypes(paramType);
        setSubfieldPrompts(prompts);
        setSubfieldDisplays(flag);      
    }
    
    /*************************************************************************
     *   NAME: private void rcvItems()
     *
     *   PURPOSE:   sets the paramType, prompts, lovstatement, subfield and flags 
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/
    
    private void rcvItems() {
        String paramType[] = { "C", "N", "S", "S", "S", "S", "S", "S", "S"};
        //String prompts[] = { "INVENTORY_ITEM_ID" , "UPC", "ITEM", "UOM", "DESCRIPTION", "LOT_CONTROL_CODE", "SERIAL_CONTROL_CODE", "SHELF_LIFE_DAYS", "QTY"};
        //boolean flag[] = {false, true, true, true, true, false, false, true, true};
        //                                     0       1       2              3     4              5                    6                      7                  8
        String prompts[] = { "INVENTORY_ITEM_ID" , "ITEM", "QTY", "RUNNING_QTY", "UOM", "DESCRIPTION", "LOT_CONTROL_CODE", "SERIAL_CONTROL_CODE", "SHELF_LIFE_DAYS" };
        //                    0     1     2     3     4     5     6       7      8
        boolean flag[] = {false, true, true, true, true, true, false, false, false};
        
        setlovStatement("XXWC_RCV_MOB_PKG.GET_ITEM_LOV");
        setInputParameterTypes(paramType);
        setSubfieldPrompts(prompts);
        setSubfieldDisplays(flag);      
    }

    /*************************************************************************
     *   NAME: private void itemsOnly()
     *
     *   PURPOSE:   sets the paramType, prompts, lovstatement, subfield and flags 
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/
    
    private void itemsOnly() {
        String paramType[] = { "C", "N", "S"};
        String prompts[] = { "INVENTORY_ITEM_ID" , "ITEM", "UOM", "DESCRIPTION", "LOT_CONTROL_CODE", "SERIAL_CONTROL_CODE", "SHELF_LIFE_DAYS", "QTY", "RUNNING_QTY"};
        boolean flag[] = {false, true, true, true, false, false, true, false, false};
        setlovStatement("XXWC_MWA_ROUTINES_PKG.GET_ITEM_ONLY_LOV");
        setInputParameterTypes(paramType);
        setSubfieldPrompts(prompts);
        setSubfieldDisplays(flag);      
    }

    /*************************************************************************
     *   NAME: private void upcOnly()
     *
     *   PURPOSE:   sets the paramType, prompts, lovstatement, subfield and flags 
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/
    
    private void upcOnly() {
        String paramType[] = { "C", "N", "S"};
        String prompts[] = { "INVENTORY_ITEM_ID" , "ITEM", "UOM", "DESCRIPTION", "LOT_CONTROL_CODE", "SERIAL_CONTROL_CODE", "SHELF_LIFE_DAYS", "QTY", "RUNNING_QTY"};
        boolean flag[] = {false, true, true, true, false, false, true, false, false};
        setlovStatement("XXWC_MWA_ROUTINES_PKG.GET_UPC_ONLY_LOV");
        setInputParameterTypes(paramType);
        setSubfieldPrompts(prompts);
        setSubfieldDisplays(flag);      
    }
    

    /*************************************************************************
     *   NAME: private void cycleItems()
     *
     *   PURPOSE:   sets the paramType, prompts, lovstatement, subfield and flags 
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/
    
    private void cycleItems() {
        String paramType[] = { "C", "N", "S", "S", "S", "S"};
        //                                     1        2      3              4                   5                      6                 7       8      9     0        1       2      3        4                        5                       6             7                      8                    9               0            1            2                  3                    4
        String prompts[] = { "INVENTORY_ITEM_ID" , "ITEM", "UOM", "DESCRIPTION", "LOT_CONTROL_CODE", "SERIAL_CONTROL_CODE", "SHELF_LIFE_DAYS", "MIN", "MAX", "SV", "WC_SEQ", "SUB", "BIN", "WCBIN", "INVENTORY_LOCATION_ID", "CYCLE_COUNT_ENTRY_ID", "LOT_NUMBER", "COUNT_LIST_SEQUENCE", "ADJUSTMENT_AMOUNT", "COST_GROUP_ID", "COUNT_QTY", "DUE_DATE", "PRIOR_COUNT_QTY", "SNAP_SHOT_QUANTITY"};
        //                     1    2     3     4      5      6     7     8     9        0     1     2     3    4      5       6     7       8      9      0      1      2      3      4
        boolean flag[] = {false, true, true, true, false, false, false, false, false, false, true, true, true, true, false, false, true, false, false, false, false, false, false, false};
        setlovStatement("XXWC_CC_MOBILE_PKG.GET_WC_ITEM_LOV");
        setInputParameterTypes(paramType);
        setSubfieldPrompts(prompts);
        setSubfieldDisplays(flag);      
    }

    
    /*************************************************************************
     *   NAME: public void fieldEntered(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException, DefaultOnlyHandlerException
     *
     *   PURPOSE:   fieldEntered method sets the connection and subPrompts
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/
      
    public void fieldEntered(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException, DefaultOnlyHandlerException{
        ses = mwaevent.getSession();
        java.sql.Connection connection = ses.getConnection();
        setSubPrompts(mwaevent.getSession());
       }
    
    /*************************************************************************
     *   NAME: public void fieldExited(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException, DefaultOnlyHandlerException
     *
     *   PURPOSE:   fieldExited method calls the setReturnValues from the LOV Query
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/
    
    
    public void fieldExited(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException, DefaultOnlyHandlerException {
        ses = mwaevent.getSession();
        try {
                if(mwaevent.getAction().equals("MWA_SUBMIT") && isPopulated())
                {
                    setReturnValues();
                }
            }
        catch(Exception e){
            UtilFns.error("Error in calling fieldExited " + e);
            
        }
        
        if(getValue().equals(""))
                {
                    clear();
                }
    }
    
    /*************************************************************************
     *   NAME: private void setReturnValues()
     *
     *   PURPOSE:   sets the return values from the list of values query
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/
    
    private void setReturnValues()
        { 
            mInventoryItemId = "";
            mItemNumber = "";
            mPrimaryUOMCode = "";
            mItemDescription = "";
            mLotControlCode = "";
            mSerialControlCode = "";
            mShelfLifeDays = "";
            mQuantityOpen = "";
            mRunningQty = "";
            mMinQty = "";
            mMaxQty = "";
            mSalesVelocity = "";
            mWCCountListSequence = "";
            mSubinventory = "";
            mBin = "";
            mWCBin = "";
            mInventoryLocationId = "";
            mCycleCountEntryId = "";
            mLotNumber = "";
            mCountListSequence = "";
            mAdjustmentAmount = "";
            mCostGroupId = "";
            mCountQuantity = "";
            mDueDate = "";
            mPriorCountQuantity = "";
            mSnapShotQuantity = "";
            
            
            Vector vector = getSelectedValues();
            
            if (mLovType.equals("RCV")) {

                if(vector != null) {
                    int i = vector.size();
                    mInventoryItemId = (String)vector.elementAt(0);
                    mItemNumber = (String)vector.elementAt(1);
                    mQuantityOpen = (String)vector.elementAt(2);
                    mRunningQty = (String)vector.elementAt(3);
                    mPrimaryUOMCode = (String)vector.elementAt(4);
                    mItemDescription = (String)vector.elementAt(5);
                    mLotControlCode = (String)vector.elementAt(6);
                    mSerialControlCode = (String)vector.elementAt(7);
                    mShelfLifeDays = (String)vector.elementAt(8);
                }
                            
            }
            
            else if (mLovType.equals("CYCLE")) {
                if(vector != null) {
                    int i = vector.size();
                    mInventoryItemId = (String)vector.elementAt(0);
                    mItemNumber = (String)vector.elementAt(1);
                    mPrimaryUOMCode = (String)vector.elementAt(2);
                    mItemDescription = (String)vector.elementAt(3);
                    mLotControlCode = (String)vector.elementAt(4);
                    mSerialControlCode = (String)vector.elementAt(5);
                    mShelfLifeDays = (String)vector.elementAt(6);
                    mMinQty = (String)vector.elementAt(7);
                    mMaxQty = (String)vector.elementAt(8);
                    mSalesVelocity = (String)vector.elementAt(9);
                    mWCCountListSequence = (String)vector.elementAt(10);
                    mSubinventory = (String)vector.elementAt(11);
                    mBin = (String)vector.elementAt(12);
                    mWCBin = (String)vector.elementAt(13);
                    mInventoryLocationId = (String)vector.elementAt(14);
                    mCycleCountEntryId = (String)vector.elementAt(15);
                    mLotNumber = (String)vector.elementAt(16);
                    mCountListSequence = (String)vector.elementAt(17);
                    mAdjustmentAmount = (String)vector.elementAt(18);
                    mCostGroupId = (String)vector.elementAt(19);
                    mCountQuantity = (String)vector.elementAt(20);
                    mDueDate = (String)vector.elementAt(21);
                    mPriorCountQuantity = (String)vector.elementAt(22);
                    mSnapShotQuantity = (String)vector.elementAt(23);
                }

            
            }
            
            else {
                
            
                if(vector != null) {
                    int i = vector.size();
                    mInventoryItemId = (String)vector.elementAt(0);
                    mItemNumber = (String)vector.elementAt(1);
                    mPrimaryUOMCode = (String)vector.elementAt(2);
                    mItemDescription = (String)vector.elementAt(3);
                    mLotControlCode = (String)vector.elementAt(4);
                    mSerialControlCode = (String)vector.elementAt(5);
                    mShelfLifeDays = (String)vector.elementAt(6);
                    mQuantityOpen = (String)vector.elementAt(7);
                    mRunningQty = (String)vector.elementAt(8);
                }
            }
        }
    
    /*************************************************************************
     *   NAME: public void clear
     *
     *   PURPOSE:   clears the returned list of values
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/
    
    public void clear()
        {
            setValue("");
            mInventoryItemId = "";
            mItemNumber = "";
            mPrimaryUOMCode = "";
            mItemDescription = "";
            mLotControlCode = "";
            mSerialControlCode = "";
            mShelfLifeDays = "";
            mQuantityOpen = "";
            mRunningQty = "";
            mMinQty = "";
            mMaxQty = "";
            mSalesVelocity = "";
            mWCCountListSequence = "";
            mSubinventory = "";
            mBin = "";
            mInventoryLocationId = "";
            mCycleCountEntryId = "";
            mLotNumber = "";
            mCountListSequence = "";
            mAdjustmentAmount = "";
            mCostGroupId = "";
            mCountQuantity = "";
            mDueDate = "";
            mPriorCountQuantity = "";
            mSnapShotQuantity = "";
    }

    /*************************************************************************
     *   NAME: public String getmInventoryItemId() 
     *
     *   PURPOSE:   returns the Printer Name 
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/

    public String getmInventoryItemId() {
        return mInventoryItemId;
    }
      
     /*************************************************************************
     *   NAME: public String getmItemNumber()
     *
     *   PURPOSE:   returns the Printer Description
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/
           
    public String getmItemNumber() {
        return mItemNumber;
    }

    /*************************************************************************
    *   NAME: public String getmItemDescription()
    *
    *   PURPOSE:   returns the Printer Description
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       18-FEB-2015   Lee Spitzer             Initial Version -
    **************************************************************************/
          
    public String getmItemDescription() {
       return mItemDescription;
    }


    /*************************************************************************
     *   NAME: public String getmLotControlCode() 
     *
     *   PURPOSE:   returns the Printer Name 
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/

    public String getmLotControlCode() {
        return mLotControlCode;
    }

    /*************************************************************************
     *   NAME: public String getmSerialControlCode() 
     *
     *   PURPOSE:   returns the Printer Name 
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/

    public String getmSerialControlCode() {
        return mSerialControlCode;
    }
    
    /*************************************************************************
     *   NAME: public String getmPrimaryUOMCode() 
     *
     *   PURPOSE:   returns the Printer Name 
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/

    public String getmPrimaryUOMCode() {
        return mPrimaryUOMCode;
    }


    /*************************************************************************
     *   NAME: public String getmShelfLifeDays() 
     *
     *   PURPOSE:   returns the mShelfLifeDays
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/

    public String getmShelfLifeDays () {
        return mShelfLifeDays;
    }
    /*************************************************************************
     *   NAME: public String getmQuantityOpen() 
     *
     *   PURPOSE:   returns the Printer Name 
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/

    public String getmQuantityOpen() {
        return mQuantityOpen;
    }

    /*************************************************************************
     *   NAME: public String getmQuantityOpen() 
     *
     *   PURPOSE:   returns the Printer Name 
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/

    public String getmRunningQty() {
        return mRunningQty;
    }
    

    /*************************************************************************
     *   NAME: public String getmMinQty() 
     *
     *   PURPOSE:   returns the Printer Name 
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/

    public String getmMinQty() {
        return mMinQty;
    }

    /*************************************************************************
     *   NAME: public String getmMinQty() 
     *
     *   PURPOSE:   returns the Printer Name 
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/

    public String getmMaxQty() {
        return mMaxQty;
    }
    
    /*************************************************************************
     *   NAME: public String getmSalesVelocity() 
     *
     *   PURPOSE:   returns the Printer Name 
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/

    public String getmSalesVelocity() {
        return mSalesVelocity;
    }
    
    /*************************************************************************
    *   NAME: public getmWCCountListSequence
    *
    *   PURPOSE:  gets the mWCCountListSequence
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       18-FEB-2015   Lee Spitzer             Initial Version -
    **************************************************************************/
    
     public String getmWCCountListSequence() {
         return mWCCountListSequence;
     }

    /*************************************************************************
    *   NAME: public getmSubinventory
    *
    *   PURPOSE:  gets the mSubinventory
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       18-FEB-2015   Lee Spitzer             Initial Version -
    **************************************************************************/
    
     public String getmSubinventory() {
         return mSubinventory;
     }
    
    /*************************************************************************
    *   NAME: public getmBin
    *
    *   PURPOSE:  gets the mBin
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       18-FEB-2015   Lee Spitzer             Initial Version -
    **************************************************************************/
    
     public String getmBin() {
         return mBin;
     }
    
    /*************************************************************************
    *   NAME: public getmWCBin
    *
    *   PURPOSE:  gets the mBin
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       18-FEB-2015   Lee Spitzer             Initial Version -
    **************************************************************************/
    
     public String getmWCBin() {
         return mWCBin;
     }

    /*************************************************************************
    *   NAME: public getmInventoryLocationId
    *
    *   PURPOSE:  gets the mInventoryLocationId
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       18-FEB-2015   Lee Spitzer             Initial Version -
    **************************************************************************/
    
     public String getmInventoryLocationId() {
         return mInventoryLocationId;
     }    

    /*************************************************************************
    *   NAME: public getmCycleCountEntryId
    *
    *   PURPOSE:  gets the mCycleCountEntryId
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       18-FEB-2015   Lee Spitzer             Initial Version -
    **************************************************************************/
    
     public String getmCycleCountEntryId() {
         return mCycleCountEntryId;
     }   
    

    /*************************************************************************
    *   NAME: public getmLotNumber
    *
    *   PURPOSE:  gets the mLotNumber
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       18-FEB-2015   Lee Spitzer             Initial Version -
    **************************************************************************/
    
     public String getmLotNumber() {
         return mLotNumber;
     }   
    
    /*************************************************************************
    *   NAME: public getmCountListSequence
    *
    *   PURPOSE:  gets the mCountListSequence
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       18-FEB-2015   Lee Spitzer             Initial Version -
    **************************************************************************/
    
     public String getmCountListSequence() {
         return mCountListSequence;
     }

    
    /*************************************************************************
    *   NAME: public getmCostGroupId()
    *
    *   PURPOSE:  gets the mCostGroupId
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       18-FEB-2015   Lee Spitzer             Initial Version -
    **************************************************************************/
    
     public String getmCostGroupId() {
         return mCostGroupId;
     } 
    
    /*************************************************************************
    *   NAME: public getmAdjustmentAmount
    *
    *   PURPOSE:  gets the mAdjustmentAmount
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       18-FEB-2015   Lee Spitzer             Initial Version -
    **************************************************************************/
    
     public String getmAdjustmentAmount() {
         return mAdjustmentAmount;
     } 
    

    /*************************************************************************
    *   NAME: public getmCountQuantity
    *
    *   PURPOSE:  gets the mCountQuantity
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       18-FEB-2015   Lee Spitzer             Initial Version -
    **************************************************************************/
    
    public String getmCountQuantity() {
        return mCountQuantity;
    }
    

    /*************************************************************************
    *   NAME: public getmDueDate
    *
    *   PURPOSE:  gets the mDueDate
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       18-FEB-2015   Lee Spitzer             Initial Version -
    **************************************************************************/
    
    public String getmDueDate() {
        return mDueDate;
    }
    
    /*************************************************************************
    *   NAME: public getmPriorCountQuantity
    *
    *   PURPOSE:  gets the mPriorCountQuantity
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       18-FEB-2015   Lee Spitzer             Initial Version -
    **************************************************************************/
    
    public String getmPriorCountQuantity() {
        return mPriorCountQuantity;
    }

    /*************************************************************************
    *   NAME: public getmSnapShotQuantity
    *
    *   PURPOSE:  gets the mSnapShotQuantity
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       18-FEB-2015   Lee Spitzer             Initial Version -
    **************************************************************************/
        
    public String getmSnapShotQuantity() {
        return mSnapShotQuantity;
    }
    
     /*************************************************************************
     *   NAME: public void setmItemNumber(String setmItemNumber)
     *
     *   PURPOSE:  sets the mPrinterName
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/


    public void setmItemNumber(String setmItemNumber) {
        mItemNumber = setmItemNumber;
    }
    
    /*************************************************************************
    *   NAME: public void setmItemNumber(String setmItemNumber)
    *
    *   PURPOSE:  sets the mPrinterName
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       18-FEB-2015   Lee Spitzer             Initial Version -
    **************************************************************************/
    
    public void setmInventoryItemId(String setmInventoryItemId) {
        mInventoryItemId =  setmInventoryItemId;
    }



    /*************************************************************************
    *   NAME: public void setmItemNumber(String setmItemNumber)
    *
    *   PURPOSE:  sets the mPrinterName
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       18-FEB-2015   Lee Spitzer             Initial Version -
    **************************************************************************/
    
    public void setmLovType(String setmLovType) {
        mLovType =  setmLovType;
    }
    /*************************************************************************
    *   NAME: public void getmLovType(String setmItemNumber)
    *
    *   PURPOSE:  sets the mPrinterName
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       18-FEB-2015   Lee Spitzer             Initial Version -
    **************************************************************************/
    
    public String getmLovType() {
        return mLovType;
    }
    
    /*************************************************************************
    *   NAME: public void setSubPrompts(Session session)
    *
    *   PURPOSE:  sets the query header values displayed in the list of values
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       18-FEB-2015   Lee Spitzer             Initial Version -
    **************************************************************************/
    
    public void setSubPrompts(Session session) {
        String as1[] = { "INVENTORY_ITEM_ID" , "ITEM", "UOM", "DESCRIPTION", "LOT_CONTROL_CODE", "SERIAL_CONTROL_CODE", "SHELF_LIFE_DAYS"};
    }
}
