/*************************************************************************
   *   $Header XXWCBinItemAssignmentLOV.java $
   *   Module Name: XXWCBinItemAssignmentLOV
   *   
   *   Package package xxwc.oracle.apps.inv.lov.server;
   *   
   *   Imports Classes
   *   
   *   import java.sql.SQLException;
   *   import java.util.StringTokenizer;
   *   import java.util.Vector;
   *   import oracle.apps.fnd.common.*;
   *   import oracle.apps.inv.lov.server.*;
   *   import oracle.apps.inv.utilities.server.UtilFns;
   *   import oracle.apps.mwa.beans.*;
   *   import oracle.apps.mwa.container.MWALib;
   *   import oracle.apps.mwa.container.Session;
   *   import oracle.apps.mwa.eventmodel.*;
   *   import xxwc.oracle.apps.inv.bins.server.XXWCPrimaryBinAssignment;
   *   
   *   PURPOSE:   XXWCCycleCountSortLOV List of Values for xxwc_mtl_cycle_count_entries_v
   *   
   *    
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
        1.0       21-OCT-2014   Lee Spitzer             Initial Version - 
                                                           TMS Ticket 20150302-00152 - RF - Receiving
**************************************************************************/

package xxwc.oracle.apps.inv.lov.server;

import java.sql.SQLException;
import java.util.StringTokenizer;
import java.util.Vector;
import oracle.apps.fnd.common.*;
import oracle.apps.inv.lov.server.*;
import oracle.apps.inv.utilities.server.UtilFns;
import oracle.apps.mwa.beans.*;
import oracle.apps.mwa.container.MWALib;
import oracle.apps.mwa.container.Session;
import oracle.apps.mwa.eventmodel.*;
import oracle.apps.inv.lov.server.LocatorKFF;

/*************************************************************************
 *   NAME: XXWCBinItemAssignmentLOV extends InvLOVFieldBean implements MWAFieldListener
 *
 *   PURPOSE:   Main class for XXWCWCCountListSequenceLOV 
 *
 *   REVISIONS:
 *   Ver        Date        Author                     Description
 *   ---------  ----------  ---------------         -------------------------
        1.0       21-OCT-2014   Lee Spitzer             Initial Version -
                                                         TMS Ticket 20150302-00152 - RF - Receiving
 **************************************************************************/


public class XXWCBinItemAssignmentLOV extends InvLOVFieldBean implements MWAFieldListener {
    public static final String RCS_ID = "$Header: XXWCBinItemAssignmentLOV.java 120.6 2014/10/21 15:24:59 aambulka ship $";
    public static final boolean RCS_ID_RECORDED = VersionInfo.recordClassVersion("$Header: XXWCBinItemAssignmentLOV.java 120.6 2014/10/21 15:24:59 aambulka ship $", "xxwc.oracle.apps.inv.lov.server");
    private Session ses;
    private String mLocatorId;
    private String mLocatorName;
    private String mLocatorDescription;
    private String mWCLocatorPrefix;
    private String mWCLocator;
    private boolean areThePromptsSet;
    private LocatorKFF mLocatorFld;
      
    /*************************************************************************
     *   NAME: public XXWCBinItemAssignmentLOV
     *
     *   PURPOSE:   main method XXWCDefaultLocatorTypesLOV
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       21-OCT-2014   Lee Spitzer             Initial Version -
     **************************************************************************/
    
     public XXWCBinItemAssignmentLOV(){
        areThePromptsSet = false;
        addListener(this);
        clear();
        initDefaultType();
          
    }
    
    /*************************************************************************
     *   NAME: private void initDefaultType()
     *
     *   PURPOSE:   sets the paramType, prompts, lovstatement, subfield and flags 
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       21-OCT-2014   Lee Spitzer             Initial Version -
     **************************************************************************/
    
    private void initDefaultType() {
        String paramType[] = { "C", "N", "S", "N", "N", "S", "S", "S" };
        String prompts[] = { "WC_LOCATOR", "PREFIX", "LOCATOR_ID", "LOCATOR", "DESCRIPTION" };
        boolean flag[] = {true, true, false, true, false };
        setlovStatement("XXWC_INV_BIN_MAINTENANCE_PKG.GET_WC_BIN_ITEM_LOC");
        setInputParameterTypes(paramType);
        setSubfieldPrompts(prompts);
        setSubfieldDisplays(flag);      
    }
    
    /*************************************************************************
     *   NAME: public void fieldEntered(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException, DefaultOnlyHandlerException
     *
     *   PURPOSE:   fieldEntered method sets the connection and subPrompts
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       21-OCT-2014   Lee Spitzer             Initial Version -
     **************************************************************************/
      
    public void fieldEntered(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException, DefaultOnlyHandlerException{
        ses = mwaevent.getSession();
        java.sql.Connection connection = ses.getConnection();
        setSubPrompts(mwaevent.getSession());
       }
    
    /*************************************************************************
     *   NAME: public void fieldExited(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException, DefaultOnlyHandlerException
     *
     *   PURPOSE:   fieldExited method calls the setReturnValues from the LOV Query
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       21-OCT-2014   Lee Spitzer             Initial Version -
     **************************************************************************/
    
    
    public void fieldExited(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException, DefaultOnlyHandlerException {
        ses = mwaevent.getSession();
        try {
                if(mwaevent.getAction().equals("MWA_SUBMIT") && isPopulated())
                {
                    setReturnValues();
                }
            }
        catch(Exception e){
            UtilFns.error("Error in calling fieldExited " + e);
            
        }
        
        if(getValue().equals(""))
                {
                    clear();
                }
    }
    
    /*************************************************************************
     *   NAME: private void setReturnValues()
     *
     *   PURPOSE:   sets the return values from the list of values query
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       21-OCT-2014   Lee Spitzer             Initial Version -
     **************************************************************************/
    
    private void setReturnValues()
        { 
            mWCLocator = "";
            mWCLocatorPrefix = "";
            mLocatorId = "";
            mLocatorName = "";
            mLocatorDescription = "";
            
            
            Vector vector = getSelectedValues();
            if(vector != null) {
                int i = vector.size();
                mWCLocator = (String)vector.elementAt(0);
                mWCLocatorPrefix = (String)vector.elementAt(1);
                mLocatorId = (String)vector.elementAt(2);
                mLocatorName = (String)vector.elementAt(3);
                mLocatorDescription = (String)vector.elementAt(4);
                ;
                
            }
        }
    
    /*************************************************************************
     *   NAME: public void clear
     *
     *   PURPOSE:   clears the returned list of values
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       21-OCT-2014   Lee Spitzer             Initial Version -
     **************************************************************************/
    
    public void clear()
        {
            setValue("");
            mWCLocator = "";
            mWCLocatorPrefix = "";
            mLocatorId = "";
            mLocatorName = "";
            mLocatorDescription = "";
            
            
        }

    /*************************************************************************
     *   NAME: public String getmWCCountListSequence() 
     *
     *   PURPOSE:   returns the mLocatorId value 
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       21-OCT-2014   Lee Spitzer             Initial Version -
     **************************************************************************/

    public String getmLocatorId() {
        return mLocatorId;
    }
      
     /*************************************************************************
     *   NAME: public String getmLocatorName()
     *
     *   PURPOSE:   returns the mLocatorName value 
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       21-OCT-2014   Lee Spitzer             Initial Version -
     **************************************************************************/
           
    public String getmLocatorName() {
        return mLocatorName;
    }
    
    /*************************************************************************
    *   NAME: public String getmLocatorDescription()
    *
    *   PURPOSE:   returns the mLocatorDescription value 
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       21-OCT-2014   Lee Spitzer             Initial Version -
    **************************************************************************/
          
    public String getmLocatorDescription() {
       return mLocatorDescription;
    }
    
    /*************************************************************************
    *   NAME: public String getmWCLocatorPrefix()
    *
    *   PURPOSE:   returns the mWCLocatorPrefix value 
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       21-OCT-2014   Lee Spitzer             Initial Version -
    **************************************************************************/
          
    public String getmWCLocatorPrefix() {
       return mWCLocatorPrefix;
    }
    
    /*************************************************************************
    *   NAME: public String getmWCLocator()
    *
    *   PURPOSE:   returns the mWCLocator value 
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       21-OCT-2014   Lee Spitzer             Initial Version -
    **************************************************************************/
          
    public String getmWCLocator() {
       return mWCLocator;
    }
    
    
    /*************************************************************************
     *   NAME: public void setSubPrompts(Session session)
     *
     *   PURPOSE:   setSubsPrompts
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       21-OCT-2014   Lee Spitzer             Initial Version -
     **************************************************************************/
   
    public void setSubPrompts(Session session) {
        String as1[] = { "WC_LOCATOR", "PREFIX", "LOCATOR_ID", "LOCATOR", "DESCRIPTION" };
    }
    
    /*************************************************************************
     *   NAME: public void getLocatorControl((int paramInt1, int paramInt2, int paramInt3) throws AbortHandlerException, InterruptedHandlerException, DefaultOnlyHandlerException
     *
     *   PURPOSE:   getLocatorControl
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       21-OCT-2014   Lee Spitzer             Initial Version -
     **************************************************************************/


    public int getLocatorControl(int paramInt1, int paramInt2, int paramInt3)
        throws AbortHandlerException, InterruptedHandlerException, DefaultOnlyHandlerException
      {
       
       int i = mLocatorFld.getLocatorControl(paramInt1, paramInt2, paramInt3); 
       return i;
       
      }
    
    
    /*************************************************************************
     *   NAME: public void setSubMapper(SubinventoryLOV paramSubinventoryLOV)
     *
     *   PURPOSE:   setSubMapper
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       21-OCT-2014   Lee Spitzer             Initial Version -
     **************************************************************************/

    public void setSubMapper(SubinventoryLOV paramSubinventoryLOV)
      {
        mLocatorFld.setSubMapper(paramSubinventoryLOV);
      }
}