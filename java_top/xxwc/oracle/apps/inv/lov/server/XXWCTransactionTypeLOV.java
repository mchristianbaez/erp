/*************************************************************************
   *   $Header XXWCTransactionTypeLOV.java $
   *   Module Name: XXWCTransactionTypeLOV
   *   
   *   Package package xxwc.oracle.apps.inv.lov.server;
   *   
   *   Imports Classes
   *   
   *   import java.sql.SQLException;
   *   import java.util.StringTokenizer;
   *   import java.util.Vector;
   *   import oracle.apps.fnd.common.*;
   *   import oracle.apps.inv.lov.server.*;
   *   import oracle.apps.inv.utilities.server.UtilFns;
   *   import oracle.apps.mwa.beans.*;
   *   import oracle.apps.mwa.container.MWALib;
   *   import oracle.apps.mwa.container.Session;
   *   import oracle.apps.mwa.eventmodel.*;
   *   import xxwc.oracle.apps.inv.bins.server.XXWCPrimaryBinAssignment;
   *   
   *   PURPOSE:   XXWCTransactionTypeLOV List of Values for Transaction Type List Of Values for RF Scanning
   *   
   *    
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
        1.0       20-JUN-2015   Lee Spitzer             Initial Version - 
                                                           TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
**************************************************************************/

package xxwc.oracle.apps.inv.lov.server;

import java.sql.SQLException;
import java.util.StringTokenizer;
import java.util.Vector;
import oracle.apps.fnd.common.*;
import oracle.apps.inv.lov.server.*;
import oracle.apps.inv.utilities.server.UtilFns;
import oracle.apps.mwa.beans.*;
import oracle.apps.mwa.container.MWALib;
import oracle.apps.mwa.container.Session;
import oracle.apps.mwa.eventmodel.*;

/*************************************************************************
 *   NAME: XXWCTransactionTypeLOV extends InvLOVFieldBean implements MWAFieldListener
 *
 *   PURPOSE:   Main class for XXWCTransactionTypeLOV 
 *
 *   REVISIONS:
 *   Ver        Date        Author                     Description
 *   ---------  ----------  ---------------         -------------------------
        1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                         TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
 **************************************************************************/

public class XXWCTransactionTypeLOV extends InvLOVFieldBean implements MWAFieldListener{
    public static final String RCS_ID = "$Header: XXWCTransactionTypeLOV.java 120.6 2014/10/21 15:24:59 aambulka ship $";
    public static final boolean RCS_ID_RECORDED = VersionInfo.recordClassVersion("$Header: XXWCTransactionTypeLOV.java 120.6 2014/10/21 15:24:59 aambulka ship $", "xxwc.oracle.apps.inv.lov.server");
    private Session ses;
    private String mTransactionTypeId;
    private String mTransactionTypeName;
    private String mDescription;
    private String mTransactionActionId;
    private String mTransactionSourceTypeId;
    private String mUserDefinedFlag;
    private String mStatusControlFlag;
    private String mLocationRequiredFlag;
    private boolean areThePromptsSet;
    
    /*************************************************************************
     *   NAME: public XXWCTransactionTypeLOV
     *
     *   PURPOSE:   main method XXWCTransactionTypeLOV
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                 TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
     **************************************************************************/
    
    public XXWCTransactionTypeLOV() {
        areThePromptsSet = false;
        addListener(this);
        clear();
        initDefaultType();
    
    }
    /*************************************************************************
     *   NAME: private void initDefaultType()
     *
     *   PURPOSE:   sets the paramType, prompts, lovstatement, subfield and flags 
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
     **************************************************************************/
    
    private void initDefaultType() {
        String paramType[] = { "C", "S"};
        //                                      1                        2              3                       4                               5                   6                       7                         8
         String prompts[] = { "TRANSACTION_TYPE_ID", "TRANSACTION_TYPE_NAME", "DESCRIPTION", "TRANSACTION_ACTION_ID", "TRANSACTION_SOURCE_TYPE_ID", "USER_DEFINED_FLAG", "STATUS_CONTROL_FLAG", "LOCATION_REQUIRED_FIELD"};
        //                    1     2    3      4      5      6        7     8
        boolean flag[] = {false, true, true, false, false, false, false, false};
        setlovStatement("XXWC_MWA_ROUTINES_PKG.GET_TRANSACTION_TYPE_LOV");
        setInputParameterTypes(paramType);
        setSubfieldPrompts(prompts);
        setSubfieldDisplays(flag);      
    }
    
     /*************************************************************************
     *   NAME: public void fieldEntered(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException, DefaultOnlyHandlerException
     *
     *   PURPOSE:   fieldEntered method sets the connection and subPrompts
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
     **************************************************************************/
      
    public void fieldEntered(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException, DefaultOnlyHandlerException{
        ses = mwaevent.getSession();
        java.sql.Connection connection = ses.getConnection();
        setSubPrompts(mwaevent.getSession());
       }
    
    /*************************************************************************
     *   NAME: public void fieldExited(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException, DefaultOnlyHandlerException
     *
     *   PURPOSE:   fieldExited method calls the setReturnValues from the LOV Query
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                 TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
     **************************************************************************/
    
    
    public void fieldExited(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException, DefaultOnlyHandlerException {
        ses = mwaevent.getSession();
        try {
                if(mwaevent.getAction().equals("MWA_SUBMIT") && isPopulated())
                {
                    setReturnValues();
                }
            }
        catch(Exception e){
            UtilFns.error("Error in calling fieldExited " + e);
            
        }
        
        if(getValue().equals(""))
                {
                    clear();
                }
    }
    
    /*************************************************************************
     *   NAME: private void setReturnValues()
     *
     *   PURPOSE:   sets the return values from the list of values query
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
     **************************************************************************/
    
    private void setReturnValues()
        { 
            mTransactionTypeId = "";
            mTransactionTypeName = "";
            mDescription = "";
            mTransactionActionId = "";
            mTransactionSourceTypeId = "";
            mUserDefinedFlag = "";
            mStatusControlFlag = "";
            mLocationRequiredFlag = "";
            
            Vector vector = getSelectedValues();
            if(vector != null) {
                int i = vector.size();
                mTransactionTypeId = (String)vector.elementAt(0);
                mTransactionTypeName = (String)vector.elementAt(1);
                mDescription = (String)vector.elementAt(2);
                mTransactionActionId = (String)vector.elementAt(3);
                mTransactionSourceTypeId = (String)vector.elementAt(4);
                mUserDefinedFlag = (String)vector.elementAt(5);
                mStatusControlFlag = (String)vector.elementAt(6);
                mLocationRequiredFlag = (String)vector.elementAt(7);
            }
        }
    
    /*************************************************************************
     *   NAME: public void clear
     *
     *   PURPOSE:   clears the returned list of values
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
     **************************************************************************/
    
    public void clear()
        {
            setValue("");
            mTransactionTypeId = "";
            mTransactionTypeName = "";
            mDescription = "";
            mTransactionActionId = "";
            mTransactionSourceTypeId = "";
            mUserDefinedFlag = "";
            mStatusControlFlag = "";
            mLocationRequiredFlag = "";
        }

     
    /*************************************************************************
    *   NAME: public String getmTransactionTypeId()
    *
    *   PURPOSE:   returns the mTransactionTypeId
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                 TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
    **************************************************************************/
          
    public String getmTransactionTypeId() {
       return mTransactionTypeId;
    }

    /*************************************************************************
    *   NAME: public String getmTransactionTypeName()
    *
    *   PURPOSE:   returns the mTransactionTypeName
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                 TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
    **************************************************************************/
          
    public String getmTransactionTypeName() {
       return mTransactionTypeName;
    }
    
    
    /*************************************************************************
     *   NAME: public String getmDescription() 
     *
     *   PURPOSE:   returns the Description
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                 TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
     **************************************************************************/

    public String getmDescription() {
        return mDescription;
    }
    
    /*************************************************************************
     *   NAME: public String getmTransactionActionId() 
     *
     *   PURPOSE:   returns the mTransactionActionId
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                 TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
     **************************************************************************/

    public String getmTransactionActionId() {
        return mTransactionActionId;
    }
     
     /*************************************************************************
     *   NAME: public String getmTransactionSourceTypeId() 
     *
     *   PURPOSE:   returns the mTransactionSourceTypeId
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                  TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
     **************************************************************************/

    public String getmTransactionSourceTypeId() {
        return mTransactionSourceTypeId;
    }
     
     /*************************************************************************
     *   NAME: public String getmUserDefinedFlag() 
     *
     *   PURPOSE:   returns the mUserDefinedFlag
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                  TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
     **************************************************************************/

    public String getmUserDefinedFlag() {
        return mUserDefinedFlag;
    }
     
     /*************************************************************************
     *   NAME: public String getmStatusControlFlag() 
     *
     *   PURPOSE:   returns the mStatusControlFlag
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                  TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
     **************************************************************************/

    public String getmStatusControlFlag() {
        return mStatusControlFlag;
    }
     
     /*************************************************************************
     *   NAME: public String getmLocationRequiredFlag() 
     *
     *   PURPOSE:   returns the mLocationRequiredFlag
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                  TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
     **************************************************************************/

    public String getmLocationRequiredFlag() {
        return mLocationRequiredFlag;
    }
     
    /*************************************************************************
    *   NAME: public void setmTransactionTypeId(String setmTransactionTypeId)
    *
    *   PURPOSE:  sets the mTransactionTypeId
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                 TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
    **************************************************************************/
    
    public void setmTransactionTypeId(String setmTransactionTypeId) {
        mTransactionTypeId = setmTransactionTypeId;
    }
    
    /*************************************************************************
    *   NAME: public void setSubPrompts(Session session)
    *
    *   PURPOSE:  sets the query header values displayed in the list of values
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                 TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
    **************************************************************************/
    
    public void setSubPrompts(Session session) {
        String as1[] = { "TRANSACTION_TYPE_ID", "TRANSACTION_TYPE_NAME", "DESCRIPTION", "TRANSACTION_ACTION_ID", "TRANSACTION_SOURCE_TYPE_ID", "USER_DEFINED_FLAG", "STATUS_CONTROL_FLAG", "LOCATION_REQUIRED_FIELD"};
        
    }
}
