
/*************************************************************************
   *   $Header XXWCBinDeleteAssignmentFListener.java $
   *   Module Name: XXWCBinDeleteAssignmentFListener
   *   
   *   Package: package xxwc.oracle.apps.inv.bins.server;
   *   
   *   Imports Classes:
   *   
   *   import java.sql.*;
   *   import oracle.apps.inv.utilities.server.UtilFns;
   *   import oracle.apps.mwa.beans.*;
   *   import oracle.apps.mwa.container.MWALib;
   *   import oracle.apps.mwa.container.Session;
   *   import oracle.apps.mwa.eventmodel.*;
   *   import oracle.apps.mwa.container.FileLogger;
   *   import oracle.apps.fnd.flexj.FlexException;
   *   import oracle.apps.inv.lov.server.ItemLOV;
   *   import oracle.apps.inv.lov.server.LocatorKFF;
   *   import oracle.apps.inv.lov.server.SubinventoryLOV;
   *   import xxwc.oracle.apps.inv.lov.server.*;
   *
   *   PURPOSE:   Java Class for XXWCBinDeleteAssignmentPage Page Layout.
   *    
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
        1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                          TMS Ticket 20150302-00164 - RF - Bin maintenance form   
**************************************************************************/
package xxwc.oracle.apps.inv.bins.server;

import java.sql.*;
import java.util.Hashtable;
import java.util.Vector;
import oracle.apps.fnd.common.VersionInfo;
import oracle.apps.inv.lov.server.*;
import oracle.apps.inv.utilities.server.*;
import oracle.apps.mwa.beans.*;
import oracle.apps.mwa.container.FileLogger;
import oracle.apps.mwa.container.Session;
import oracle.apps.mwa.eventmodel.*;
import oracle.apps.mwa.presentation.telnet.TelnetSession;
import oracle.jdbc.OraclePreparedStatement;
import oracle.sql.NUMBER;
import oracle.apps.mwa.container.MWALib;
import oracle.apps.fnd.flexj.KeyFlexfield;
import oracle.apps.inv.invinq.server.ItemOnhandIterator;
import oracle.apps.inv.invinq.server.ItemOnhandQueryManager;

import xxwc.oracle.apps.inv.lov.server.XXWCBinsLOV;
import xxwc.oracle.apps.inv.lov.server.XXWCItemLOV;

/*************************************************************************
 *   NAME: XXWCBinDeleteAssignmentFListener implements MWAFieldListener
 *
 *   PURPOSE:   Main class for XXWCBinDeleteAssignmentFListener
 *
 *   REVISIONS:
 *   Ver        Date        Author                     Description
 *   ---------  ----------  ---------------         -------------------------
        1.0       09-FEB-2015   Lee Spitzer             Initial Version -
                                                          TMS Ticket 20150302-00164 - RF - Bin maintenance form   
 **************************************************************************/


public class XXWCBinDeleteAssignmentFListener implements MWAFieldListener{
    XXWCBinDeleteAssignmentPage pg;
    Session ses;
    String dialogPageButtons[];

    /*************************************************************************
     *   NAME: private static String gPackage = "xxwc.oracle.apps.inv.bins.server";
     *
     *   PURPOSE:   private method to default gPackage value for debugging
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00164 - RF - Bin maintenance form   
     **************************************************************************/

    private static String gPackage = "xxwc.oracle.apps.inv.bins.server";


    /*************************************************************************
     *   NAME: private static String gCallFrom = "XXWCBinDeleteAssignmentFListener";
     *
     *   PURPOSE:   private method to pass gCallFrom from methods to return class
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20150302-00164 - RF - Bin maintenance form   
     **************************************************************************/

    private static String gCallFrom = "XXWCBinDeleteAssignmentFListener";

    /*************************************************************************
     *   NAME: public XXWCBinDeleteAssignmentFListener()
     *
     *   PURPOSE:   public method XXWCBinDeleteAssignmentFListener
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                               TMS Ticket 20150302-00164 - RF - Bin maintenance form   
     **************************************************************************/

    public XXWCBinDeleteAssignmentFListener() {
        $init$();
    }

    /*************************************************************************
     *   NAME:  public void fieldEntered(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                       DefaultOnlyHandlerException
     *
     *   PURPOSE:   field entered for XXWCBinDeleteAssignmentFListener
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20150302-00164 - RF - Bin maintenance form   
     **************************************************************************/
    
    public void fieldEntered(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                       DefaultOnlyHandlerException {
        ses = mwaevent.getSession();
        pg = (XXWCBinDeleteAssignmentPage)ses.getCurrentPage();
        String s = UtilFns.fieldEnterSource(ses);
        /*Item*/
        if (s.equals("INV.ITEM")) {
            enteredItem(mwaevent);
            return;
        }
        /*Sub*/
        if (s.equals("INV.SUB")) {
            enteredSub(mwaevent);
            return;
        }
        /*Loc*/
        if (s.equals("INV.LOC")) {
            enteredLocator(mwaevent);
            return;
        }
        
    }

    /*************************************************************************
     *   NAME:      public void fieldExited(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException
     *
     *   PURPOSE:   field exited for XXWCBinDeleteAssignmentFListener
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20150302-00164 - RF - Bin maintenance form   
     **************************************************************************/

    public void fieldExited(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException {
        String s = ((FieldBean)mwaevent.getSource()).getName();
        String vField = mwaevent.getAction();
        boolean flag = false;
        boolean flag1 = false;
        boolean flag3 = false;
        if (mwaevent.getAction().equals("MWA_SUBMIT")) {
            flag = true;
        } else if (mwaevent.getAction().equals("MWA_PREVIOUSFIELD")) {
            flag1 = true;
        } else if (mwaevent.getAction().equals("MWA_NEXTFIELD")) {
            flag3 = true;
        }
        /*Item*/
        if (s.equals("INV.ITEM")) {
            exitedItem(mwaevent, ses);
            return;
        }
        /*Sub*/
        if (s.equals("INV.SUB")) {
            exitedSub(mwaevent, ses);
            return;
        }
        /*Loc*/
        if (s.equals("INV.LOC")) {
            exitedLocator(mwaevent, ses);
            return;
        }
      
        /*Cancel*/
        if (flag && s.equals("INV.CANCEL")) {
            exitedCancel(mwaevent, ses);
            return;
        }
        if (flag && s.equals("XXWC.NEXT_BIN")) {
            exitedNextBin(mwaevent, ses);
            //clearFields(mwaevent, ses);
            return;
        }
        /*SaveNext*/
        if (flag && s.equals("INV.SAVENEXT")) {
            deleteItemLocatorTie(mwaevent, ses);
            clearFields(mwaevent, ses);
            return;
        }
        /*Done*/
        if (flag && s.equals("INV.DONE")) {
            deleteItemLocatorTie(mwaevent, ses);
            exitedDone(mwaevent, ses);
            return;
        }
        else {
            return;
        }
    }

    /*************************************************************************
     *   NAME:      private void $init$()
     *
     *   PURPOSE:   $init$ sets the diaglogButtons button for user prompt messages to OK
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
         1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                             TMS Ticket 20150302-00164 - RF - Bin maintenance form   
     **************************************************************************/

    private void $init$() {
        dialogPageButtons = (new String[] { "OK" });
    }


    /*************************************************************************
     *   NAME: public void enteredItem(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException
     *
     *   PURPOSE:  Entered Item Field Listener Process to set the List of Values for the Item Field
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                  TMS Ticket 20150302-00164 - RF - Bin maintenance form   
     **************************************************************************/


    public void enteredItem(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException {

        try {
            XXWCItemLOV itemlov = pg.getmItemFld();
            itemlov.setValidateFromLOV(true);
            itemlov.setlovStatement("XXWC_MWA_ROUTINES_PKG.GET_WC_ITEM_LOV");
            String paramType[] = { "C", "N", "S" };
            String parameters[] =
            { " ", "ORGID", "xxwc.oracle.apps.inv.bins.server.XXWCBinDeleteAssignmentPage.INV.ITEM" };
            itemlov.setInputParameterTypes(paramType);
            itemlov.setInputParameters(parameters);
        } catch (Exception e) {
            UtilFns.error("Error in calling enteredItem" + e);
        }
        }

    /*************************************************************************
     *   NAME: public void exitedItem(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException
     *
     *   PURPOSE:  Exited Item Field Listener to populate the Item Description Field
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00164 - RF - Bin maintenance form   
     **************************************************************************/

    public void exitedItem(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                  DefaultOnlyHandlerException {
        String l_item = "";
        pg.getmItemFld().getValue().toUpperCase();
        try {
            l_item = pg.getmItemFld().getValue();
            if (l_item.equals(null) || l_item.equals("")) {
                pg.getmItemDescFld().setHidden(true);
                pg.getmSubFld().setHidden(true);
                pg.getmLocFld().setHidden(true);
                pg.getmLocFld().setRequired(false);
                pg.getmLocFldPreFix().setHidden(false);
                pg.getmLocFld().setEditable(false);
                pg.getmLocFldPreFix().setEditable(false);
                pg.getmNextBin().setHidden(true);
                pg.getmSaveNext().setHidden(true);
                pg.getmDone().setHidden(true);
            }
            else {
                pg.getmItemDescFld().setValue(pg.getmItemFld().getmItemDescription());
                pg.getmItemDescFld().setHidden(false);
                binQuery(mwaevent, ses);
                pg.getmSubFld().setEditable(false);
                pg.getmSubFld().setHidden(false);
                pg.getmLocFld().setHidden(false);
                pg.getmLocFld().setRequired(false);
                pg.getmLocFldPreFix().setHidden(false);
                pg.getmLocFld().setEditable(false);
                pg.getmLocFldPreFix().setEditable(false);
                pg.getmNextBin().setHidden(false);
                pg.getmSaveNext().setHidden(false);
                pg.getmDone().setHidden(false);
            }
        } catch (Exception e) {
            UtilFns.error("Error in calling exitedItem" + e);
        }
    }

    /*************************************************************************
    *   NAME: public void enteredSub(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                     DefaultOnlyHandlerException
    *
    *   PURPOSE:  Entered Subinventory Field Listener Process to set the List of Values for the Subinventory Field
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                  TMS Ticket 20150302-00164 - RF - Bin maintenance form   
    **************************************************************************/

    public void enteredSub(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException {
    pg.getmSubFld().setValue("General");
    
       try {
           SubinventoryLOV sublov = pg.getmSubFld();
           sublov.setValidateFromLOV(true);
           sublov.setlovStatement("INV_UI_ITEM_SUB_LOC_LOVS.GET_VALID_SUBS");
           String paramType[] = { "C", "N", "S" };
           String parameters[] =
           { " ", "ORGID", "xxwc.oracle.apps.inv.bins.server.XXWCBinDeleteAssignmentPage.INV.SUB" };
           sublov.setInputParameterTypes(paramType);
           sublov.setInputParameters(parameters);
       } catch (Exception e) {
           UtilFns.error("Error in calling enteredSub" + e);
       }
    }

    /*************************************************************************
    *   NAME: public void exitedSub(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException
    *
    *   PURPOSE:  Exited Subinventory Field Listener Process to determine if the Locator Field should be displayed
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20150302-00164 - RF - Bin maintenance form   
    **************************************************************************/

    public void exitedSub(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                DefaultOnlyHandlerException {
       try {
           Integer i = getLocatorControl(mwaevent, ses);
           if (i == 1) {
               pg.getmLocFld().setHidden(true);
               pg.getmLocFld().clear();
                  
           } else {
               pg.getmLocFld().setHidden(false);
               pg.getmLocFld().setRequired(false);
               pg.getmLocFldPreFix().setHidden(false);
               pg.getmLocFld().setEditable(false);
               pg.getmLocFldPreFix().setEditable(false);
               pg.getmNextBin().setHidden(false);
           }
       } catch (Exception e) {
           UtilFns.error("Error in calling exitedSub" + e);
       }
    }

    /*************************************************************************
    *   NAME: public void getLocatorControl(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException
    *
    *   PURPOSE:  Determines if Subinventory has Locator Control turned on or off
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
          1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                               TMS Ticket 20150302-00164 - RF - Bin maintenance form   
    **************************************************************************/

    private int getLocatorControl(MWAEvent mwavevent, Session session) throws AbortHandlerException,
                                                                             InterruptedHandlerException,
                                                                             DefaultOnlyHandlerException {
       int i = 1;
       int j = 1;
       int k = 1;
       int x_count = 0;
       try {
           Long orgid = Long.parseLong((String)session.getObject("ORGID"));
           FileLogger.getSystemLogger().trace("orgid  " + orgid);
           FileLogger.getSystemLogger().trace("i  " + i);
           OrgParameters orgparameters = OrgParameters.getOrgParameters(session.getConnection(), orgid);
           i = orgparameters.getLocatorControlCode();
           FileLogger.getSystemLogger().trace("i  " + i);
           FileLogger.getSystemLogger().trace("j " + j);
           SubinventoryLOV sublov = pg.getmSubFld();
           if (pg.getmSubFld().getValue() != null | pg.getmSubFld().getValue() != "") {
               j = Integer.parseInt(sublov.getLocatorType());
           }
           FileLogger.getSystemLogger().trace("j " + j);
           XXWCItemLOV itemlov = pg.getmItemFld();
           FileLogger.getSystemLogger().trace("k " + k);
           if (pg.getmItemFld().getValue() != null | pg.getmItemFld().getValue() != "") {
               k = Integer.parseInt("2");

           }
           FileLogger.getSystemLogger().trace("k " + k);
           if (UtilFns.isTraceOn) {
               UtilFns.trace((new StringBuilder()).append("RCV: getLocatorControl orgLocControl:").append(i).append(":subLocControl:").append(j).append(":itemLocControl:").append(k).toString());
           }
           
       } catch (Exception e) {
           FileLogger.getSystemLogger().trace("error getLocatorControl " + e);
       }
        //Added for WC Branches
        try {
            CallableStatement cstmt = null;
            Connection con = ses.getConnection();
            String p_organization_id = (String)ses.getObject("ORGID");
            String p_subinventory_code = (String)pg.getmSubFld().getValue();
            cstmt = con.prepareCall("{? = call XXWC_INV_BIN_MAINTENANCE_PKG.WC_BRANCH_LOCATOR_CONTROL(?,?)}");
            cstmt.setString(2, p_organization_id);
            cstmt.setString(3, p_subinventory_code);
            cstmt.registerOutParameter(1, Types.NUMERIC);
            cstmt.execute();
            x_count = cstmt.getInt(1);
            cstmt.close();
            FileLogger.getSystemLogger().trace("x_count " + x_count);
        }
        catch (Exception e) {
            UtilFns.error("Error in calling getLocatorControl" + e);
        }
        
        if (x_count > 0) {
            return 0;
        }
        else {
            return 1;
            
        }
    }

    /*************************************************************************
    *   NAME: public void enteredLocator(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException
    *
    *   PURPOSE:  Entered Locator Field Listener Process to set the List of Values for the Locator Field
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
         1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00164 - RF - Bin maintenance form   
    **************************************************************************/

    public void enteredLocator(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                        DefaultOnlyHandlerException {

       try {
           /*String gMethod = "enteredLocator";
           String gCallPoint = "Starting";
           FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
           Session session = mwaevent.getSession();
           String s = "";
           String s1 = "";
           int i = -1;
           String p_inventory_item_id = pg.getmItemFld().getItemID();
           UtilFns.trace("p_inventory_item_id " + p_inventory_item_id);
           ses.putSessionObject("sessInventoryItemId", Integer.valueOf(p_inventory_item_id));
           //LocatorKFF locatorlov = pg.getmLocFld();
           //ItemLOV itemlov = pg.getmItemFld();
           if ((String)session.getObject("PROJECT_ID") != null &&
               !((String)session.getObject("PROJECT_ID")).equals("-9999")) {
               s = (String)session.getObject("PROJECT_ID");
               s1 = (String)session.getObject("TASK_ID");
           } else {
               s = "";
               s1 = "";
           }
           FileLogger.getSystemLogger().trace("PROJECT_ID " + s);
           FileLogger.getSystemLogger().trace("TASK_ID " + s1);

           if (pg.getmSubFld() != null && !pg.getmSubFld().getValue().equals("")) {
               i = getLocatorControl(mwaevent, session);
           }
           if (i == 3) {
               //locatorlov.setValidateFromLOV(false);
               pg.getmLocFld().setValidateFromLOV(true);
           } else {
               //locatorlov.setValidateFromLOV(true);
               pg.getmLocFld().setValidateFromLOV(true);
           }

           FileLogger.getSystemLogger().trace("i " + i);

           String s2 = "FALSE";
           if (session.getObject("wms_purchased") != null &&
               ((String)session.getObject("wms_purchased")).equals("I")) {
               s2 = "TRUE";
           }
           FileLogger.getSystemLogger().trace("WMS Enabled " + s2);
           //locatorlov.setSubMapper(pg.getmSubFld());
           gCallPoint = "setting lov statement";
           UtilFns.trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
           //pg.getmLocFld().setSubMapper(pg.getmSubFld());
           //locatorlov.setlovStatement("INV_UI_ITEM_SUB_LOC_LOVS.GET_INQ_LOC_LOV");
           //pg.getmLocFld().setlovStatement("INV_UI_ITEM_SUB_LOC_LOVS.GET_INQ_LOC_LOV");
           gCallPoint = "XXWC_INV_BIN_MAINTENANCE_PKG.GET_WC_BIN_ITEM_LOC";
           pg.getmLocFld().setlovStatement("XXWC_INV_BIN_MAINTENANCE_PKG.GET_WC_BIN_ITEM_LOC");
           UtilFns.trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
           //                      0    1    2    3    4    5    6    7
           String paramType[] = { "C", "N", "S", "N", "N", "S", "S", "S" };
           // 0         1                                                                        2                                      3                             4                                                                    5   6  7
           String parameters[] =
           { " ", "ORGID", "xxwc.oracle.apps.inv.bins.server.XXWCBinDeleteAssignmentPage.INV.SUB" ,pg.getmItemFld().getRestrictLocCode(), "sessInventoryItemId" , "xxwc.oracle.apps.inv.bins.server.XXWCBinDeleteAssignmentPage.INV.LOC", s, s1 };
           //{ " ", "ORGID", "xxwc.oracle.apps.inv.bins.server.XXWCBinDeleteAssignmentPage.INV.SUB",pg.getmItemFld().getRestrictLocCode(), pg.getmItemFld().getItemID() , "xxwc.oracle.apps.inv.bins.server.XXWCBinDeleteAssignmentPage.INV.LOC", s, s1 };
           UtilFns.trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " paramType" + paramType );
           UtilFns.trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " parameters " + parameters );
           String prompts[] = { "WC_LOCATOR", "PREFIX", "LOCATOR_ID", "LOCATOR", "DESCRIPTION" };
           boolean flag[] = {true, true, false, true, false};
           pg.getmLocFld().setInputParameterTypes(paramType);
           pg.getmLocFld().setInputParameters(parameters);
           pg.getmLocFld().setSubfieldPrompts(prompts);
           pg.getmLocFld().setSubfieldDisplays(flag);
           */
           
           
               ses.putSessionObject("sessInventoryItemId", pg.getmItemFld().getmInventoryItemId());
               ses.putSessionObject("sessRestrictLocator", 1);
               ses.putSessionObject("sessHidePrefix","N");
               XXWCBinsLOV binsLOV = pg.getmLocFld();
               //                      0    1    2    3    4    5    6    7    8
               String paramType[] = { "C", "N", "S", "N", "N", "S", "S", "S", "S" };
               String prompts[] = { "WC_LOCATOR", "PREFIX", "LOCATOR_ID", "LOCATOR", "DESCRIPTION", "SUBINVENTORY_CODE" };
               boolean flag[] = {true, true, false, true, false, false};
               //                        0        1                                                              2 
               String parameters[] = { " ", "ORGID", "" ,"sessRestrictLocator", "sessInventoryItemId" , "xxwc.oracle.apps.inv.bins.server.XXWCBinDeleteAssignmentPage.XXWC.BIN", "", "", "sessHidePrefix" };
               //String parameters[] = { " ", "ORGID", "" ,"", "sessInventoryItemId" , "xxwc.oracle.apps.inv.lables.server.XXWCItemLabelPage.XXWC.BIN", "", "", "N" };
               binsLOV.setInputParameters(parameters);
               binsLOV.setlovStatement("XXWC_INV_BIN_MAINTENANCE_PKG.GET_WC_BIN_LOC");
               binsLOV.setInputParameterTypes(paramType);
               binsLOV.setSubfieldPrompts(prompts);
               binsLOV.setSubfieldDisplays(flag);  
       } catch (Exception e) {
           UtilFns.trace("Error in calling enteredLoc" + e);
       }
       
    }

    /*************************************************************************
    *   NAME: public void exitedLocator(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException
    *
    *   PURPOSE:  Exited Subinventory Field Listener Process to determine if the Locator Field should be displayed
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20150302-00164 - RF - Bin maintenance form   
    **************************************************************************/

    public void exitedLocator(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                DefaultOnlyHandlerException {
       try {
           if (pg.getmLocFld().getValue().equals(null)){
               pg.mDone.setHidden(true);
               pg.mSaveNext.setHidden(true);
               pg.getmLocFldPreFix().setValue("");
           } else {
               pg.mDone.setHidden(false);
               pg.mSaveNext.setHidden(false);
               pg.getmLocFldPreFix().setValue(pg.getmLocFld().getmWCPreFix());
               ses.setNextFieldName("INV.SAVENEXT");
           }
       } catch (Exception e) {
           UtilFns.error("Error in calling exitedLocator" + e);
       }
    }


    /*************************************************************************
    *   NAME: public void exitedClose(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                     DefaultOnlyHandlerException
    *
    *   PURPOSE:  Exited Close For Field Listener Process to proces the Close Button
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                               TMS Ticket 20150302-00164 - RF - Bin maintenance form   
    **************************************************************************/

    public void exitedCancel(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                   DefaultOnlyHandlerException {
        try {
            XXWCBinDeleteAssignmentPage _tmp = pg;
            ses.setStatusMessage(pg.WMS_TXN_CANCEL);
            ses.clearAllApplicationScopeObjects();
            ses.setStatusMessage(XXWCBinDeleteAssignmentPage.WMS_TXN_CANCEL);
            pg.getmCancel().setNextPageName("|END_OF_TRANSACTION|");
        } catch (Exception e) {
            UtilFns.error("Error in calling exitedCancel " + e);
        }

    }
    
    /*************************************************************************
    *   NAME:     public void deleteItemLocatorTie(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                                 InterruptedHandlerException,
                                                                                 DefaultOnlyHandlerException
    *
    *   PURPOSE:  Execute the procedure XXWC_INV_PRIMARY_BIN_PKG.PROCESS_ITEM_LOCATOR_TIE
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
         1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                              TMS Ticket 20150302-00164 - RF - Bin maintenance form   
    **************************************************************************/
    
    public void deleteItemLocatorTie(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                             InterruptedHandlerException,
                                                                             DefaultOnlyHandlerException {
        TelnetSession telnetsession = (TelnetSession)ses;
        Integer l =
            telnetsession.showPromptPage("Warning!", "Are you sure you want to delete bin assignment Item " + pg.getmItemFld().getValue() + " subinventory " + pg.getmSubFld().getValue() + " Locator " + pg.getmLocFld().getmConcatedSegments() + "?", (new String[] { "Delete Bin Assignment", "Back"}));

        
        if (l == 0) {
         
            CallableStatement cstmt = null;
            Connection con = ses.getConnection();
            String p_organization_id = (String)ses.getObject("ORGID");
            String p_inventory_item_id = (String)pg.getmItemFld().getmInventoryItemId();
            String p_subinventory_code = (String)pg.getmSubFld().getValue();
            String p_locator_id = (String)pg.getmLocFld().getmLocationId();
            Integer x_return;
            String x_message;
            try {
                //                                                                                  1 2 3 4 5 6
                cstmt = con.prepareCall("{call XXWC_INV_BIN_MAINTENANCE_PKG.DELETE_ITEM_LOCATOR_TIE(?,?,?,?,?,?)}");
                cstmt.setString(1, p_organization_id);
                cstmt.setString(2, p_inventory_item_id);
                cstmt.setString(3, p_subinventory_code);
                cstmt.setString(4, p_locator_id);
                cstmt.registerOutParameter(5, Types.NUMERIC);
                cstmt.registerOutParameter(6, Types.VARCHAR);
                cstmt.execute();
                x_return = cstmt.getInt(5);
                x_message = cstmt.getString(6);
                cstmt.close();
                if (x_return == 0) {
                    ses.setStatusMessage((new StringBuilder()).append(pg.WMS_TXN_SUCCESS).append(" (").append(pg.getmItemFld().getValue() + " | " + pg.getmLocFld().getmConcatedSegments() + " " + x_message).append(")").toString());
                    return;
                }
                if (x_return != 0) {
                    TelnetSession telnetsessionX = (TelnetSession)ses;
                    int k = telnetsessionX.showPromptPage("Error!", x_message, dialogPageButtons);
                    if (k == 0) {
                        ses.setRefreshScreen(true);
                    } else {
                        return;
                    }
                }
            } catch (Exception e) {
                ses.setStatusMessage((new StringBuilder()).append(pg.WMS_TXN_ERROR).append(" (").append(e).append(")").toString());
                UtilFns.error((new StringBuilder()).append("Error in deleteItemLocatorTie: error - ").append(e).toString());
            }
        }
        else if (l == 1) {
            clearFields(mwaevent, ses);
        }
        else {
        
            return;
        }
  }


    /*************************************************************************
    *   NAME:     public void clearFields(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                                 InterruptedHandlerException,
                                                                                 DefaultOnlyHandlerException
    *
    *   PURPOSE:  Clears the values in the form
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
        1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00164 - RF - Bin maintenance form   
    **************************************************************************/
    
    public void clearFields(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                   InterruptedHandlerException,
                                                                   DefaultOnlyHandlerException {
        pg.mItemFld.setValue(null);
        pg.mItemDescFld.setHidden(true);
        pg.mItemDescFld.setValue(null);
        pg.mSubFld.setHidden(true);
        pg.mSubFld.setValue(null);
        pg.mLocFld.setValue(null);
        pg.mLocFld.setHidden(true);
        pg.mLocFldPreFix.setHidden(true);
        pg.mLocFldPreFix.setValue(null);
        pg.mNextBin.setHidden(true);
        pg.mSaveNext.setHidden(true);
        pg.mDone.setHidden(true);
        ses.setNextFieldName("INV.ITEM");
        pg.getmLocFld().clear();
        pg.getmLocFldPreFix().clear();
    }
    
    
    /*************************************************************************
    *   NAME:     public void exitedDone(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                                 InterruptedHandlerException,
                                                                                 DefaultOnlyHandlerException
    *
    *   PURPOSE:  Clears the values in the form
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
        1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00164 - RF - Bin maintenance form   

    **************************************************************************/
    
    protected void exitedDone(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                        DefaultOnlyHandlerException {
        try {
                XXWCBinDeleteAssignmentPage _tmp = pg;
                ses.clearAllApplicationScopeObjects();
                pg.getmDone().setNextPageName("|END_OF_TRANSACTION|");
            } 
        catch (Exception e) {
            UtilFns.error("Error in calling exitedDone " + e);
        }
    
    }
    
    /*************************************************************************
    *   NAME:     public void exitedNextBin(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                                 InterruptedHandlerException,
                                                                                 DefaultOnlyHandlerException
    *
    *   PURPOSE:  Execute the procedure XXWC_INV_PRIMARY_BIN_PKG.PROCESS_ITEM_LOCATOR_TIE
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
         1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                              TMS Ticket 20150302-00164 - RF - Bin maintenance form   
    **************************************************************************/
    
    public void exitedNextBin(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                             InterruptedHandlerException,
                                                                             DefaultOnlyHandlerException {
        String gMethod = "exitedNextBin";
        String gCallPoint = "Start";
        FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint);
        
        if (pg.getmNextBin().getPrompt().equals("Create New Bin Loc")) {
            String l_current_page = ses.getCurrentPageName();
            ses.putSessionObject("sessPreviousPage", l_current_page);
            ses.putSessionObject("sessPreviousField", "INV.ITEM");
            ses.putSessionObject("sessXXWC.ITEM", pg.getmItemFld().getValue().toString());
            ses.putSessionObject("sessInventoryItemId", pg.getmItemFld().getmInventoryItemId().toString());
            ses.putSessionObject("sessXXWC.DESCRPTION", pg.getmItemFld().getmItemDescription().toString());
            ses.putSessionObject("sessXXWC.SUBINVENTORY", pg.getmSubFld().getValue().toString());
            pg.getmNextBin().setNextPageName("xxwc.oracle.apps.inv.bins.server.XXWCBinDeleteAssignmentPage"); 
            ses.setNextFieldName("XXWC.LOC");
        }
        else {
        
            //XXWCBinPutAwayIterator XXWCBinPutAwayIterator = pg.getmXXWCBinPutAwaySeq();
            //XXWCBinPutAway XXWCBinPutAway = pg.getmCurrentXXWCBinPutAway();
            
            XXWCBinPutAwayIterator XXWCBinPutAwayIterator = (XXWCBinPutAwayIterator)ses.getObject("XXWCBINPUTAWAYSEQ");
            
            int i = XXWCBinPutAwayIterator.getCurrentPos();
            int l = -1;
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " " + XXWCBinPutAwayIterator);
            //FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " " + XXWCBinPutAway);
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " i " + i);
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " l " + l);
            Connection connection = ses.getConnection();
            XXWCBinPutAwayQueryManager XXWCBinPutAwayQueryManager =
                new XXWCBinPutAwayQueryManager(connection);
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " XXWCBinPutAwayQueryManager " + XXWCBinPutAwayQueryManager);
            try {
                XXWCBinPutAway XXWCBinPutAway1 = XXWCBinPutAwayIterator.next();
                l = XXWCBinPutAwayIterator.max_left();
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " XXWCBinPutAway1 " + XXWCBinPutAway1);
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " l " + l);
                if (XXWCBinPutAway1 != null) {
                    FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " XXWCBinPutAway1 != null ");
                    pg.setCurrentXXWCBinPutAway(XXWCBinPutAway1);
                }
                if (l == 0) {
                    FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " l == 0");
                    pg.mNextBin.setPrompt("Create New Bin Loc");
                    ses.setRefreshScreen(true);
                } else {
                    FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " l != 0");
                    pg.mNextBin.setPrompt("Next Bin Loc");
                    ses.setRefreshScreen(true);
                }
                ses.setRefreshScreen(true);
                int j = XXWCBinPutAwayIterator.getCurrentPos();
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " j " + j);
                //pg.mItemFld.setValue(XXWCBinPutAway1.getmItemNumber());
                //pg.mItemResultFld.setItemID(Long.toString(XXWCBinPutAway1.getmInventoryItemId()));
                //pg.mItemDescResultFld.setValue(XXWCBinPutAway1.getmItemDescription());
                //pg.mSubResultFld.setValue(XXWCBinPutAway1.getmSubinventoryCode());
                pg.getmLocFld().setValue(XXWCBinPutAway1.getmWCLocator());
                pg.getmLocFldPreFix().setValue(XXWCBinPutAway1.getmWCPreFix());
                
                pg.getmLocFld().setmWCLocator(XXWCBinPutAway1.getmWCLocator());
                pg.getmLocFld().setmWCPreFix(XXWCBinPutAway1.getmWCPreFix());
                pg.getmLocFld().setmConcatedSegments(XXWCBinPutAway1.getmConcatedSegments());
                pg.getmLocFld().setmLocationId(XXWCBinPutAway1.getmLocationId());
                pg.getmLocFldPreFix().setmWCPreFix(XXWCBinPutAway1.getmWCPreFix());
                pg.getmSubFld().setValue((XXWCBinPutAway1.getmSubinventory()));
                
                
            } catch (Exception exception1) {
                UtilFns.log((new StringBuilder()).append(" ").append(exception1).toString());
            }
            
            ses.setNextFieldName("XXWC.NEXT_BIN");
        }
        }


    /*************************************************************************
    *   NAME:     public void binQuery(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                                 InterruptedHandlerException,
                                                                                 DefaultOnlyHandlerException
    *
    *   PURPOSE:  Execute the procedure XXWC_INV_PRIMARY_BIN_PKG.PROCESS_ITEM_LOCATOR_TIE
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
         1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                              TMS Ticket 20150302-00164 - RF - Bin maintenance form   
    **************************************************************************/
    
    public void binQuery(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                             InterruptedHandlerException,
                                                                             DefaultOnlyHandlerException {
        String gMethod = "binQuery";
        String gCallPoint = "Start";
        FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint);
                        
        try{
            gCallPoint = "Entering Try";
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint);
            //Call the QueryManager to execute the query
            java.sql.Connection con = ses.getConnection();
            XXWCBinPutAwayQueryManager XXWCBinPutAwayQueryManager = new XXWCBinPutAwayQueryManager(con);
            //{ " ", "ORGID", "xxwc.oracle.apps.inv.bins.server.XXWCBinPutAwayPage.INV.SUB" ,"1", "sessInventoryItemId" , "xxwc.oracle.apps.inv.bins.server.XXWCBinPutAwayPage.INV.LOC", s, s1 };
            
            String p_organization_id = (String)ses.getObject("ORGID");
            String p_subinventory_code = "";
            String p_restrict_locators_code = "1";
            String p_inventory_item_id = (String)pg.getmItemFld().getmInventoryItemId();
            String p_concatenated_segments = "";
            String p_project_id = "";
            String p_task_id = "";
            String p_hide_prefix = "N";
            Vector vector1;
            gCallPoint = "about to call vector 1";
            //Execute the getmtlItemLocatorDefaultStr query and set the vector
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint);
            vector1 =
                    XXWCBinPutAwayQueryManager.getmWCBinLoc(p_organization_id, p_subinventory_code, p_restrict_locators_code, p_inventory_item_id, p_concatenated_segments, p_project_id, p_task_id, p_hide_prefix);
        
        
            gCallPoint = "results after vector 1";
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " vector size " + vector1.size() );
            
            if (vector1.size() == 0) {
                gCallPoint = "vector1 is 0";
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint);
                TelnetSession telnetsessionX = (TelnetSession)ses;
                Integer k =
                    telnetsessionX.showPromptPage("Error!", "No bin assignments for item " + pg.getmItemFld().getValue(), (new String[] { "Create Bin Assignment", "Next Item"}));
                //showSubError = true;
                if (k == 0) {
                    String l_current_page = ses.getCurrentPageName();
                    ses.putSessionObject("sessPreviousPage", l_current_page);
                    ses.putSessionObject("sessPreviousField", "INV.ITEM");
                    ses.putSessionObject("sessXXWC.ITEM", pg.getmItemFld().getValue());
                    ses.putSessionObject("sessInventoryItemId", pg.getmItemFld().getmInventoryItemId().toString());
                    ses.putSessionObject("sessXXWC.DESCRPTION", pg.getmItemFld().getmItemDescription().toString());
                    ses.putSessionObject("sessXXWC.SUBINVENTORY", pg.getmSubFld().toString());
                    pg.getmItemFld().setNextPageName("xxwc.oracle.apps.inv.bins.server.XXWCBinAssignmentPage");   
                    ses.setNextFieldName("XXWC.LOC");
                    pg.getmSaveNext().setHidden(false);
                    return;
                }
                if (k == 1) {
                    pg.getmItemFld().setNextPageName("xxwc.oracle.apps.inv.bins.server.XXWCBinDeleteAssignmentPage"); 
                    return;
                } 
        }
            else {
                //If the vector has results then set session values, retun page
                gCallPoint = "vector1 found";
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint);
                XXWCBinPutAwayIterator XXWCBinPutAwayIterator = new XXWCBinPutAwayIterator(vector1);
                ses.putObject("XXWCBINPUTAWAYSEQ", XXWCBinPutAwayIterator);
                XXWCBinPutAwayIterator mXXWCBinPutAwaySeq = (XXWCBinPutAwayIterator)ses.getObject("XXWCBINPUTAWAYSEQ");
                int i = mXXWCBinPutAwaySeq.getCurrentPos();
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " mXXWCBinPutAwaySeq " + mXXWCBinPutAwaySeq);
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " i " + i);
                if (i == 0){
                    gCallPoint = "i == 0";
                    FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " mXXWCBinPutAwaySeq == null");
                    try {
                         pg.mCurrentBinPutAway = mXXWCBinPutAwaySeq.next();
                         FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " mXXWCBinPutAwaySeq == null");
                         pg.getmLocFld().setValue(pg.getmCurrentXXWCBinPutAway().getmWCLocator());
                         pg.getmLocFldPreFix().setValue((pg.getmCurrentXXWCBinPutAway().getmWCPreFix()));
                         pg.getmLocFld().setmConcatedSegments((pg.getmCurrentXXWCBinPutAway().getmConcatedSegments()));
                         pg.getmLocFld().setmLocationId(pg.getmCurrentXXWCBinPutAway().getmLocationId());
                         pg.getmSubFld().setValue(pg.getmCurrentXXWCBinPutAway().getmSubinventory());

                        
                        if (vector1.size()==1){
                            pg.getmNextBin().setPrompt("Create New Bin Loc");
                        }
                        else {
                            pg.getmNextBin().setPrompt("Next Bin Loc");
                        }
                        
                    } catch (Exception e) {
                        FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " " + e);
                    }
                }
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " " + pg.getmCurrentXXWCBinPutAway());
                
            }
        } catch (Exception e) {
            ses.setStatusMessage((new StringBuilder()).append(pg.WMS_TXN_ERROR).append(" (").append(e).append(")").toString());
            UtilFns.error((new StringBuilder()).append("Error in binQuery: error - ").append(e).toString());
        }
    }
        
}
