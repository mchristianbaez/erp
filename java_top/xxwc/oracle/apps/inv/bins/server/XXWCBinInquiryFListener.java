/*************************************************************************
   *   $Header XXWCBinInquiryFListener.java $
   *   Module Name: XXWCBinInquiryFListener
   *   
   *   Package: package xxwc.oracle.apps.inv.bins.server;
   *   
   *   Imports Classes:
   *   
   *   import java.sql.*;
   *   import oracle.apps.inv.utilities.server.UtilFns;
   *   import oracle.apps.mwa.beans.*;
   *   import oracle.apps.mwa.container.MWALib;
   *   import oracle.apps.mwa.container.Session;
   *   import oracle.apps.mwa.eventmodel.*;
   *   import oracle.apps.mwa.container.FileLogger;
   *   import oracle.apps.fnd.flexj.FlexException;
   *   import oracle.apps.inv.lov.server.ItemLOV;
   *   import oracle.apps.inv.lov.server.LocatorKFF;
   *   import oracle.apps.inv.lov.server.SubinventoryLOV;
   *   import xxwc.oracle.apps.inv.lov.server.*;
   *
   *   PURPOSE:   Java Class for XXWCBinInquiryPage Page Layout.
   *    
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
        1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                          TMS Ticket 20150302-00164 - RF - Bin maintenance form   
**************************************************************************/
package xxwc.oracle.apps.inv.bins.server;

import java.sql.*;
import java.util.Hashtable;
import java.util.Vector;
import oracle.apps.fnd.common.VersionInfo;
import oracle.apps.inv.lov.server.*;
import oracle.apps.inv.utilities.server.*;
import oracle.apps.mwa.beans.*;
import oracle.apps.mwa.container.FileLogger;
import oracle.apps.mwa.container.Session;
import oracle.apps.mwa.eventmodel.*;
import oracle.apps.mwa.presentation.telnet.TelnetSession;
import oracle.jdbc.OraclePreparedStatement;
import oracle.sql.NUMBER;
import oracle.apps.mwa.container.MWALib;
import oracle.apps.fnd.flexj.KeyFlexfield;
import oracle.apps.inv.invinq.server.ItemOnhandIterator;
import oracle.apps.inv.invinq.server.ItemOnhandQueryManager;

import xxwc.oracle.apps.inv.lov.server.XXWCBinsLOV;
import xxwc.oracle.apps.inv.lov.server.XXWCItemLOV;

/*************************************************************************
 *   NAME: XXWCBinInquiryFListener implements MWAFieldListener
 *
 *   PURPOSE:   Main class for XXWCBinInquiryFListener
 *
 *   REVISIONS:
 *   Ver        Date        Author                     Description
 *   ---------  ----------  ---------------         -------------------------
        1.0       09-FEB-2015   Lee Spitzer             Initial Version -
                                                          TMS Ticket 20150302-00164 - RF - Bin maintenance form   
 **************************************************************************/


public class XXWCBinInquiryFListener implements MWAFieldListener{
    XXWCBinInquiryPage pg;
    Session ses;
    String dialogPageButtons[];

    /*************************************************************************
     *   NAME: private static String gPackage = "xxwc.oracle.apps.inv.bins.server";
     *
     *   PURPOSE:   private method to default gPackage value for debugging
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00164 - RF - Bin maintenance form   
     **************************************************************************/

    private static String gPackage = "xxwc.oracle.apps.inv.bins.server";


    /*************************************************************************
     *   NAME: private static String gCallFrom = "XXWCBinInquiryFListener";
     *
     *   PURPOSE:   private method to pass gCallFrom from methods to return class
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20150302-00164 - RF - Bin maintenance form   
     **************************************************************************/

    private static String gCallFrom = "XXWCBinInquiryFListener";

    /*************************************************************************
     *   NAME: public XXWCBinInquiryFListener()
     *
     *   PURPOSE:   public method XXWCBinInquiryFListener
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                               TMS Ticket 20150302-00164 - RF - Bin maintenance form   
     **************************************************************************/

    public XXWCBinInquiryFListener() {
        $init$();
    }

    /*************************************************************************
     *   NAME:  public void fieldEntered(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                       DefaultOnlyHandlerException
     *
     *   PURPOSE:   field entered for XXWCBinInquiryFListener
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20150302-00164 - RF - Bin maintenance form   
     **************************************************************************/
    
    public void fieldEntered(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                       DefaultOnlyHandlerException {
        ses = mwaevent.getSession();
        pg = (XXWCBinInquiryPage)ses.getCurrentPage();
        String s = UtilFns.fieldEnterSource(ses);
        /*Item*/
        if (s.equals("XXWC.ITEM")) {
            enteredItem(mwaevent);
            return;
        }
        /*Loc*/
        if (s.equals("XXWC.BIN_LIST")) {
            enteredLocator(mwaevent);
            return;
        }
    }
    
    /*************************************************************************
     *   NAME:      public void fieldExited(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException
     *
     *   PURPOSE:   field exited for XXWCBinInquiryFListener
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20150302-00164 - RF - Bin maintenance form   
     **************************************************************************/

    public void fieldExited(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException {
        String s = ((FieldBean)mwaevent.getSource()).getName();
        String vField = mwaevent.getAction();
        boolean flag = false;
        boolean flag1 = false;
        boolean flag3 = false;
        if (mwaevent.getAction().equals("MWA_SUBMIT")) {
            flag = true;
        } else if (mwaevent.getAction().equals("MWA_PREVIOUSFIELD")) {
            flag1 = true;
        } else if (mwaevent.getAction().equals("MWA_NEXTFIELD")) {
            flag3 = true;
        }
        /*Item*/
        if (s.equals("XXWC.ITEM")) {
            exitedItem(mwaevent, ses);
            return;
        }
        /*Loc*/
        if (s.equals("XXWC.BIN_LIST")) {
            exitedLocator(mwaevent, ses);
            return;
        }
        /*Cancel*/
        if (flag && s.equals("XXWC.CANCEL")) {
            exitedCancel(mwaevent, ses);
            return;
        }
        /*Done*/
        if (flag && s.equals("XXWC.DONE")) {
            exitedDone(mwaevent, ses);
            return;
        }
        else {
            return;
        }
    }

    /*************************************************************************
     *   NAME:      private void $init$()
     *
     *   PURPOSE:   $init$ sets the diaglogButtons button for user prompt messages to OK
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
         1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                             TMS Ticket 20150302-00164 - RF - Bin maintenance form   
     **************************************************************************/

    private void $init$() {
        dialogPageButtons = (new String[] { "OK" });
    }


    /*************************************************************************
     *   NAME: public void enteredItem(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException
     *
     *   PURPOSE:  Entered Item Field Listener Process to set the List of Values for the Item Field
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                  TMS Ticket 20150302-00164 - RF - Bin maintenance form   
     **************************************************************************/


    public void enteredItem(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException {

        try {
            XXWCItemLOV itemlov = pg.getmItemFld();
            itemlov.setValidateFromLOV(true);
            itemlov.setlovStatement("XXWC_MWA_ROUTINES_PKG.GET_WC_ITEM_LOV");
            String paramType[] = { "C", "N", "S" };
            String parameters[] =
            { " ", "ORGID", "xxwc.oracle.apps.inv.bins.server.XXWCBinInquiryPage.XXWC.ITEM" };
            itemlov.setInputParameterTypes(paramType);
            itemlov.setInputParameters(parameters);
        } catch (Exception e) {
            UtilFns.error("Error in calling enteredItem" + e);
        }
    }

    /*************************************************************************
     *   NAME: public void exitedItem(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException
     *
     *   PURPOSE:  Exited Item Field Listener to populate the Item Description Field
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00164 - RF - Bin maintenance form   
     **************************************************************************/

    public void exitedItem(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                  DefaultOnlyHandlerException {
        String x_locators = "No bins associated for item";
        if (pg.getmItemFld().getValue().equals(null) || pg.getmItemFld().getValue().equals("")) {
            return;
        }
        else {
            try {
                pg.getmItemFld().getValue().toUpperCase();
                pg.getmItemDescFld().setValue(pg.getmItemFld().getmItemDescription());
                
                
                CallableStatement cstmt = null;
                Connection con = ses.getConnection();
                String p_organization_id = (String)ses.getObject("ORGID");
                String p_inventory_item_id = (String)pg.getmItemFld().getmInventoryItemId();
                cstmt = con.prepareCall("{? = call XXWC_INV_BIN_MAINTENANCE_PKG.BIN_INQUIRY(?,?)}");
                cstmt.setString(2, p_organization_id);
                cstmt.setString(3, p_inventory_item_id);
                cstmt.registerOutParameter(1, Types.VARCHAR);
                cstmt.execute();
                x_locators = cstmt.getString(1);
                cstmt.close();
                FileLogger.getSystemLogger().trace("x_locators " + x_locators);
                
                if (x_locators.equals("No bins associated for item")){
                    x_locators = x_locators + " " + pg.getmItemFld().getmItemNumber().toString();
                }
                
                TelnetSession telnetsessionX = (TelnetSession)ses;
                int k = telnetsessionX.showPromptPage("Bin Assignments for " + pg.getmItemFld().getmItemNumber(), 
                    x_locators.replace("|",System.getProperty("line.separator"))
                                                      , new String [] {"Continue", "Menu"} );
                    
                //(new String[] { "OK" });
                    if (k == 0) {
                        ses.setRefreshScreen(true);
                        pg.getmItemFld().clear();
                        ses.setNextFieldName("XXWC.ITEM");
                    }
                    else if (k == 1) {
                        ses.setStatusMessage(pg.WMS_TXN_CANCEL);
                        ses.clearAllApplicationScopeObjects();
                        ses.setStatusMessage(XXWCBinInquiryPage.WMS_TXN_CANCEL);
                        pg.getmItemFld().setNextPageName("|END_OF_TRANSACTION|");
                    }
                    else {
                        return;
                    }
            } catch (Exception e) {
                UtilFns.error("Error in calling exitedItem" + e);
            }
        }
    }


    /*************************************************************************
    *   NAME: public void enteredLocator(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException
    *
    *   PURPOSE:  Entered Locator Field Listener Process to set the List of Values for the Locator Field
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
         1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00164 - RF - Bin maintenance form   
    **************************************************************************/

    public void enteredLocator(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                        DefaultOnlyHandlerException {

        try {
            ses.putSessionObject("sessInventoryItemId", pg.getmItemFld().getmInventoryItemId().toString());
            ses.putSessionObject("sessRestrictLocator", 3);
            ses.putSessionObject("sessHidePrefix","N");
        } catch (Exception e) {
            UtilFns.error("Error in calling enteredItem " + e);
        }
    }

    /*************************************************************************
    *   NAME: public void exitedLocator(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException
    *
    *   PURPOSE:  Exited Subinventory Field Listener Process to determine if the Locator Field should be displayed
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20150302-00164 - RF - Bin maintenance form   
    **************************************************************************/

    public void exitedLocator(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                DefaultOnlyHandlerException {
       try {
            return;
           }
       catch (Exception e) {
           UtilFns.error("Error in calling exitedLocator" + e);
       }
    }

    /*************************************************************************
    *   NAME: public void exitedClose(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                     DefaultOnlyHandlerException
    *
    *   PURPOSE:  Exited Close For Field Listener Process to proces the Close Button
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                               TMS Ticket 20150302-00164 - RF - Bin maintenance form   
    **************************************************************************/

    public void exitedCancel(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                   DefaultOnlyHandlerException {
        try {
            XXWCBinInquiryPage _tmp = pg;
            ses.setStatusMessage(pg.WMS_TXN_CANCEL);
            ses.clearAllApplicationScopeObjects();
            ses.setStatusMessage(XXWCBinInquiryPage.WMS_TXN_CANCEL);
            pg.getmCancel().setNextPageName("|END_OF_TRANSACTION|");
        } catch (Exception e) {
            UtilFns.error("Error in calling exitedCancel " + e);
        }

    }
    

    /*************************************************************************
    *   NAME:     public void clearFields(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                                 InterruptedHandlerException,
                                                                                 DefaultOnlyHandlerException
    *
    *   PURPOSE:  Clears the values in the form
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
        1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00164 - RF - Bin maintenance form   
    **************************************************************************/
    
    public void clearFields(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                   InterruptedHandlerException,
                                                                   DefaultOnlyHandlerException {
        pg.mItemFld.setValue(null);
        pg.mItemDescFld.setValue(null);
        pg.mBinFld.setValue(null);
        pg.mDone.setHidden(true);
        ses.setNextFieldName("XXWC.ITEM");
    }
    
    /*************************************************************************
    *   NAME:     public void exitedDone(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                                 InterruptedHandlerException,
                                                                                 DefaultOnlyHandlerException
    *
    *   PURPOSE:  Clears the values in the form
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
        1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00164 - RF - Bin maintenance form   

    **************************************************************************/
    
    protected void exitedDone(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                        DefaultOnlyHandlerException {
        try {
                XXWCBinInquiryPage _tmp = pg;
                ses.setStatusMessage(pg.WMS_TXN_SUCCESS);
                ses.clearAllApplicationScopeObjects();
                pg.getmDone().setNextPageName("|END_OF_TRANSACTION|");
            } 
        catch (Exception e) {
            UtilFns.error("Error in calling exitedDone " + e);
        }
    
    }
        
}