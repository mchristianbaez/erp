/*************************************************************************
   *   $Header XXWCBinMaintenancePage.java $
   *   Module Name: XXWCBinMaintenancePage
   *   
   *   Package: package xxwc.oracle.apps.inv.bins.server;
   *   
   *   Imports Classes:
   *   
   *   import java.sql.*;
   *   import oracle.apps.inv.utilities.server.UtilFns;
   *   import oracle.apps.mwa.beans.*;
   *   import oracle.apps.mwa.container.MWALib;
   *   import oracle.apps.mwa.container.Session;
   *   import oracle.apps.mwa.eventmodel.*;
   *   import oracle.apps.mwa.container.FileLogger;
   *   import oracle.apps.fnd.flexj.FlexException;
   *   import oracle.apps.inv.lov.server.ItemLOV;
   *   import oracle.apps.inv.lov.server.LocatorKFF;
   *   import oracle.apps.inv.lov.server.SubinventoryLOV;
   *   import xxwc.oracle.apps.inv.lov.server.*;
   *
   *   PURPOSE:   Java Class for XXWCBinMaintenancePage Page Layout.
   *    
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
        1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                          TMS Ticket 20150302-00164 - RF - Bin maintenance form
        1.1       01-JUN-2015   Lee Spitzer             TMS Ticket 20150604-00154 RF Bundle 1
                                                            Need Next Button for Bin Maintenance
**************************************************************************/

package xxwc.oracle.apps.inv.bins.server;

import java.sql.SQLException;

import oracle.apps.mwa.beans.*;
import oracle.apps.mwa.container.MWALib;
import oracle.apps.mwa.container.Session;
import oracle.apps.mwa.eventmodel.*;
import oracle.apps.mwa.container.FileLogger;
import oracle.apps.fnd.flexj.FlexException;
import oracle.apps.fnd.flexj.KeyFlexfield;
import oracle.apps.inv.invinq.server.ItemOnhandIterator;
import oracle.apps.inv.utilities.server.*;
import oracle.apps.inv.lov.server.ItemLOV;
import oracle.apps.inv.lov.server.LocatorKFF;
import oracle.apps.inv.lov.server.SubinventoryLOV;
import xxwc.oracle.apps.inv.lov.server.*;

/*************************************************************************
 *   NAME: XXWCBinMaintenancePage extends PageBean
 *
 *   PURPOSE:   Main class for XXWCBinMaintenancePage
 *
 *   REVISIONS:
 *   Ver        Date        Author                     Description
 *   ---------  ----------  ---------------         -------------------------
        1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                          TMS Ticket 20150302-00164 - RF - Bin maintenance form
        1.1       01-JUN-2015   Lee Spitzer             TMS Ticket 20150604-00154 RF Bundle 1
                                                         Need Next Button for Bin Maintenance
 **************************************************************************/


public class XXWCBinMaintenancePage extends PageBean{
    /*Define objects in the class*/
    public static String WMS_TXN_SUCCESS = "Txn Successful";
    public static String WMS_TXN_ERROR = "Txn Failed. Please check log";
    public static String WMS_TXN_CANCEL = "Txn Cancelled";
    public static String WMS_LOT_INS_FAIL = "Lot Insert Failed";
    public static String WMS_SER_INS_FAIL = "Serial Insert Failed";
    public static String savenextPrompt = "";
    public static String donePrompt = "";
    public static String cancelPrompt = "";
    private boolean areThePromptsSet;
    protected XXWCItemLOV mItemFld;
    TextFieldBean mItemDescFld;
    protected XXWCBinsLOV mBinFld;
    protected ButtonFieldBean mPrintItemLabel;
    protected ButtonFieldBean mDone;
    protected ButtonFieldBean mDelete;
    protected ButtonFieldBean mMenu;
    protected ButtonFieldBean mNext; //Added TMS Ticket 20150604-00154 01-JUN-2015
    protected String m_userid;


    /*************************************************************************
     *   NAME: private static String gPackage = "xxwc.oracle.apps.inv.bins.server";
     *
     *   PURPOSE:   private method to default gPackage value for debugging
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00164 - RF - Bin maintenance form   
     **************************************************************************/

    private static String gPackage = "xxwc.oracle.apps.inv.bins.server";


    /*************************************************************************
     *   NAME: private static String gCallFrom = "XXWCBinMaintenancePage";
     *
     *   PURPOSE:   private method to pass gCallFrom from methods to return class name CustomListener
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00164 - RF - Bin maintenance form   
     **************************************************************************/

    private static String gCallFrom = "XXWCBinMaintenancePage";



    /*************************************************************************
     *   NAME: public XXWCBinMaintenancePage(Session session)
     *
     *   PURPOSE:   Inherits session from XXWCPrimaryBinAssignmentPage
     *
     *   Parameters: Session session
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                               TMS Ticket 20150302-00164 - RF - Bin maintenance form   
     **************************************************************************/

    public XXWCBinMaintenancePage(Session session) {
        initLayout(session);
    }

    
    /*************************************************************************
     *   NAME: private void initLayout(Session session)
     *
     *   PURPOSE:   Sets the initial layout of the page
     *
     *   Parameters: Session session
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
          1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                             TMS Ticket 20150302-00164 - RF - Bin maintenance form 
          1.1       01-JUN-2015   Lee Spitzer             TMS Ticket 20150604-00154 RF Bundle 1
                                                            Need Next Button for Bin Maintenance     
     **************************************************************************/

    private void initLayout(Session session) {
        m_userid = "";
            WMS_TXN_SUCCESS = UtilFns.getMessage(session, "WMS", "WMS_TXN_SUCCESS");
            WMS_TXN_ERROR = UtilFns.getMessage(session, "WMS", "WMS_TXN_ERROR");
            WMS_TXN_CANCEL = UtilFns.getMessage(session, "WMS", "WMS_TXN_CANCEL");
            WMS_LOT_INS_FAIL = UtilFns.getMessage(session, "WMS", "WMS_LOT_INS_FAIL");
            WMS_SER_INS_FAIL = UtilFns.getMessage(session, "WMS", "WMS_SER_INS_FAIL");
            mItemFld = new XXWCItemLOV("ALL");
            mItemFld.setName("INV.ITEM");
            mItemFld.setRequired(true);
            mItemDescFld = new TextFieldBean();
            mItemDescFld.setName("INV.DESC");
            mItemDescFld.setPrompt("Item Desc");
            mItemDescFld.setEditable(false);
            mBinFld = new XXWCBinsLOV();
            mBinFld.setName("XXWC.BIN");
            mBinFld.setRequired(true);
            mBinFld.setHidden(true);
            mDone = new ButtonFieldBean();
            mDone.setName("INV.DONE");
            mDone.setPrompt("Assign/Create");
            mDone.setEnableAcceleratorKey(true);
            mDone.setHidden(true);
            mDelete = new ButtonFieldBean();
            mDelete.setName("XXWC.DELETE");
            mDelete.setPrompt("Delete Bin");
            mDelete.setEnableAcceleratorKey(true);
            mDelete.setHidden(true);
            mPrintItemLabel = new ButtonFieldBean();
            mPrintItemLabel.setName("XXWC.PRINT_ITEM_LABEL");
            mPrintItemLabel.setHidden(true);
            mMenu = new ButtonFieldBean();
            mMenu.setName("XXWC.MENU");
            mMenu.setPrompt("Menu");
            mMenu.setHidden(false);
            mMenu.setEnableAcceleratorKey(true);
            /*Added TMS Ticket 20150604-00154 01-JUN-2015 mNext field*/
            mNext = new ButtonFieldBean();
            mNext.setName("XXWC.NEXT"); 
            mNext.setPrompt("Next");
            mNext.setHidden(true);
            mNext.setEnableAcceleratorKey(true);
            /*End TMS Ticket 20150604-00154 added mNext*/
            addFieldBean(mItemFld);
            addFieldBean(mItemDescFld);
            addFieldBean(mBinFld);
            addFieldBean(mDone);
            addFieldBean(mNext); //Added TMS Ticket 20150604-00154 01-JUN-2015 mNext
            addFieldBean(mDelete);
            addFieldBean(mPrintItemLabel);
            addFieldBean(mMenu);
            XXWCBinMaintenanceFListener fieldListener = new XXWCBinMaintenanceFListener();
            mItemFld.addListener(fieldListener);
            mItemDescFld.addListener(fieldListener);
            mBinFld.addListener(fieldListener);
            mDone.addListener(fieldListener);
            mNext.addListener(fieldListener); //Added TMS Ticket 20150604-00154 01-JUN-2015 mNext
            mDelete.addListener(fieldListener);
            mPrintItemLabel.addListener(fieldListener);
            mMenu.addListener(fieldListener);
            try
            {
                setPrompts(session);
            }
            catch(Exception exception)
            {
                session.setStatusMessage(UtilFns.getMessage(session, "WMS", "WMS_PROMPT_NOTSET"));
            }
        }
    
    /*************************************************************************
     *   NAME: private void initPrompts(Session session) throws SQLException
     *
     *   PURPOSE:   Sets the initial prompts of the session
     *
     *   Parameters: Session session
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
          1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                             TMS Ticket 20150302-00164 - RF - Bin maintenance form   
     **************************************************************************/

    private void initPrompts(Session session) throws SQLException {
        try
              {
                  String gMethod = "initPrompts";
                  String gCallPoint = "Start";
                  FileLogger.getSystemLogger().trace((new StringBuilder()).append(gPackage).append(".").append(gCallFrom).append(" ").append(gMethod).append(" ").append(gCallPoint).toString());
                  String s = (String)session.getObject("ORGCODE");
                  setPrompt((new StringBuilder()).append("XXWC Bin Putaway (").append(s).append(")").toString());
                  mItemFld.setPrompt(MWALib.getAKPrompt(session, "oracle.apps.inv.utilities.InvResourceTable", "INV_ITEM_PROMPT"));
                  mItemFld.retrieveAttributes("INV_ITEM_PROMPT");
                  mItemDescFld.setPrompt(MWALib.getAKPrompt(session, "oracle.apps.inv.utilities.InvResourceTable", "INV_DESCRIPTION_PROMPT"));
                  mItemDescFld.retrieveAttributes("INV_DESCRIPTION_PROMPT");
                  mBinFld.setPrompt(MWALib.getAKPrompt(session, "oracle.apps.inv.utilities.InvResourceTable", "INV_LOCATOR_PROMPT"));
                  mBinFld.retrieveAttributes("INV_LOCATOR_PROMPT");
                  savenextPrompt = MWALib.getAKPrompt(session, "oracle.apps.inv.utilities.InvResourceTable", "INV_SAVE_NEXT_PROMPT");
                  cancelPrompt = MWALib.getAKPrompt(session, "oracle.apps.inv.utilities.InvResourceTable", "INV_CANCEL_PROMPT");
                  donePrompt = MWALib.getAKPrompt(session, "oracle.apps.inv.utilities.InvResourceTable", "INV_DONE_PROMPT");
                  mPrintItemLabel.setPrompt("Item Label");
                  areThePromptsSet = true;
                  gCallPoint = "End";
                  FileLogger.getSystemLogger().trace((new StringBuilder()).append(gPackage).append(".").append(gCallFrom).append(" ").append(gMethod).append(" ").append(gCallPoint).toString());
              }
              catch(SQLException sqlexception)
              {
                  session.setStatusMessage(UtilFns.getMessage(session, "WMS", "WMS_TXN_ERROR"));
              }
    }

    /*************************************************************************
     *   NAME: private void setPrompts(Session session)
     *
     *   PURPOSE:   Sets the set prompts of the session
     *
     *   Parameters: Session session
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
          1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                             TMS Ticket 20150302-00164 - RF - Bin maintenance form   
          1.1       01-JUN-2015   Lee Spitzer             TMS Ticket 20150604-00154 RF Bundle 1
                                                             Need Next Button for Bin Maintenance
     **************************************************************************/

    private void setPrompts(Session session) {
        String gMethod = "setPrompts";
             String gCallPoint = "Start";
             if(!areThePromptsSet)
             {
                 try
                 {
                     initPrompts(session);
                 }
                 catch(SQLException sqlexception) { }
             }
             FileLogger.getSystemLogger().trace((new StringBuilder()).append(gPackage).append(".").append(gCallFrom).append(" ").append(gMethod).append(" ").append(gCallPoint).toString());
             String s = (String)session.getObject("ORGCODE");
             setPrompt((new StringBuilder()).append("XXWC Bin Maintenance (").append(s).append(")").toString());
             mDone.setPrompt("Assign/Create");
             mNext.setPrompt("Next"); //Added TMS Ticket 20150604-00154 01-JUN-2015
             mDelete.setPrompt("Delete");
             mPrintItemLabel.setPrompt("Item Label");
             mDone.retrieveAttributes("INV_DONE_PROMPT");
             mMenu.setPrompt(cancelPrompt);
             mMenu.retrieveAttributes("INV_CANCEL_PROMPT");
             mMenu.setPrompt("Menu");
             gCallPoint = "End Prompts";
             FileLogger.getSystemLogger().trace((new StringBuilder()).append(gPackage).append(".").append(gCallFrom).append(" ").append(gMethod).append(" ").append(gCallPoint).toString());
    }


    /*************************************************************************
     *   NAME: public ItemLOV  getmItemFld()
     *
     *   PURPOSE:   return the value of mItemFld
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00164 - RF - Bin maintenance form   
      **************************************************************************/

    public XXWCItemLOV getmItemFld()
        //public LOVFieldBean getmItemFld()
    {
        return mItemFld;
    }

    /*************************************************************************
     *   NAME: public TextFieldBean getmItemDescFld()
     *
     *   PURPOSE:   return the value of mItemDescFld
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00164 - RF - Bin maintenance form   
     **************************************************************************/

    public TextFieldBean getmItemDescFld() {
        return mItemDescFld;
    }

    /*************************************************************************
     *   NAME: public MultiListFieldBean getmBinFld()
     *
     *   PURPOSE:   return the value of mBinFld
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket 20150302-00164 - RF - Bin maintenance form   
     **************************************************************************/

    public XXWCBinsLOV getmBinFld() {
        return mBinFld;
    }

    /*************************************************************************
     *   NAME: public ButtonFieldBean getmDone()
     *
     *   PURPOSE:   return the value of mDone
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00164 - RF - Bin maintenance form   
     **************************************************************************/

    public ButtonFieldBean getmDone() {
        return mDone;
    }

    /*************************************************************************
     *   NAME: public ButtonFieldBean getmDone()
     *
     *   PURPOSE:   return the value of mDone
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00164 - RF - Bin maintenance form   
             1.1       01-JUN-2015   Lee Spitzer             TMS Ticket 20150604-00154 RF Bundle 1
                                                               Need Next Button for Bin Maintenance
     **************************************************************************/
    /*Added 01-JUN-2015 TMS Ticket 20150604-00154 mNext Field*/
    public ButtonFieldBean getmNext() {
        return mNext;
    }
    
    
    /*************************************************************************
     *   NAME: public ButtonFieldBean getmPrintItemLabel()
     *
     *   PURPOSE:   return the value of mPrintItemLabel
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00164 - RF - Bin maintenance form   
     **************************************************************************/

    public ButtonFieldBean getmPrintItemLabel()
        {
            return mPrintItemLabel;
        }

    /*************************************************************************
     *   NAME: public ButtonFieldBean getmDelete()
     *
     *   PURPOSE:   return the value of mDelete
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00164 - RF - Bin maintenance form   
     **************************************************************************/

        public ButtonFieldBean getmDelete()
        {
            return mDelete;
        }

    /*************************************************************************
     *   NAME: public ButtonFieldBean getmMenu()
     *
     *   PURPOSE:   return the value of mMenu
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00164 - RF - Bin maintenance form   
     **************************************************************************/

        public ButtonFieldBean getmMenu()
        {
            return mMenu;
        }

    /*************************************************************************
     *   NAME: public String getUserId()
     *
     *   PURPOSE:   return the value of m_userid
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00164 - RF - Bin maintenance form   
     **************************************************************************/

    public String getUserId() {
        return m_userid;
    }

    /*************************************************************************
     *   NAME: void pageEntered(MWAEvent e) throws AbortHandlerException, InterruptedHandlerException,
                                               DefaultOnlyHandlerException {
     *
     *   PURPOSE:   pageEntered method
     *
     *   Parameters: MWAEvent mwaevent
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00164 - RF - Bin maintenance form   
     **************************************************************************/

    public void pageEntered(MWAEvent e) throws AbortHandlerException, InterruptedHandlerException,
                                               DefaultOnlyHandlerException {
        Session session = e.getSession();
    }

    /*************************************************************************
     *   NAME: void pageExited(MWAEvent e) throws AbortHandlerException, InterruptedHandlerException,
                                               DefaultOnlyHandlerException {
     *
     *   PURPOSE:   pageExited method
     *
     *
     *   Parameters: MWAEvent   mwaevent
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00164 - RF - Bin maintenance form   

     **************************************************************************/

    public void pageExited(MWAEvent e) throws AbortHandlerException, InterruptedHandlerException,
                                              DefaultOnlyHandlerException {
        Session session = e.getSession();
    }
}
