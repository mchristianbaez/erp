/*************************************************************************
   *   $Header XXWCBinPutAwayFListener.java $
   *   Module Name: XXWCBinPutAwayFListener
   *   
   *   Package: package xxwc.oracle.apps.inv.bins.server;
   *   
   *   Imports Classes:
   *   
   *   import java.sql.*;
   *   import oracle.apps.inv.utilities.server.UtilFns;
   *   import oracle.apps.mwa.beans.*;
   *   import oracle.apps.mwa.container.MWALib;
   *   import oracle.apps.mwa.container.Session;
   *   import oracle.apps.mwa.eventmodel.*;
   *   import oracle.apps.mwa.container.FileLogger;
   *   import oracle.apps.fnd.flexj.FlexException;
   *   import oracle.apps.inv.lov.server.ItemLOV;
   *   import oracle.apps.inv.lov.server.LocatorKFF;
   *   import oracle.apps.inv.lov.server.SubinventoryLOV;
   *   import xxwc.oracle.apps.inv.lov.server.*;
   *
   *   PURPOSE:   Java Class for XXWCBinPutAwayPage Page Layout.
   *    
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
        1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                          TMS Ticket 20150302-00158 - RF - Put-Away
        1.1       01-JUN-2015   Lee Spitzer             TMS Ticket 20150604-00154 RF Bundle 1
                                                           Add Delete Button
**************************************************************************/
package xxwc.oracle.apps.inv.bins.server;

import java.sql.*;
import java.util.Hashtable;
import java.util.Vector;
import oracle.apps.fnd.common.VersionInfo;
import oracle.apps.inv.lov.server.*;
import oracle.apps.inv.utilities.server.*;
import oracle.apps.mwa.beans.*;
import oracle.apps.mwa.container.FileLogger;
import oracle.apps.mwa.container.Session;
import oracle.apps.mwa.eventmodel.*;
import oracle.apps.mwa.presentation.telnet.TelnetSession;
import oracle.jdbc.OraclePreparedStatement;
import oracle.sql.NUMBER;
import oracle.apps.mwa.container.MWALib;
import oracle.apps.fnd.flexj.KeyFlexfield;
import oracle.apps.inv.invinq.server.ItemOnhandIterator;
import oracle.apps.inv.invinq.server.ItemOnhandQueryManager;

import oracle.jdbc.OracleCallableStatement;

import xxwc.oracle.apps.inv.lov.server.XXWCBinsLOV;
import xxwc.oracle.apps.inv.lov.server.XXWCItemLOV;

/*************************************************************************
 *   NAME: XXWCBinPutAwayFListener implements MWAFieldListener
 *
 *   PURPOSE:   Main class for XXWCBinPutAwayFListener
 *
 *   REVISIONS:
 *   Ver        Date        Author                     Description
 *   ---------  ----------  ---------------         -------------------------
        1.0       09-FEB-2015   Lee Spitzer             Initial Version -
                                                          TMS Ticket 20150302-00158 - RF - Put-Away
        1.1       01-JUN-2015   Lee Spitzer             TMS Ticket 20150604-00154 RF Bundle 1
                                                          Add Delete Button
 **************************************************************************/


public class XXWCBinPutAwayFListener implements MWAFieldListener {
    XXWCBinPutAwayPage pg;
    Session ses;
    String dialogPageButtons[];

    /*************************************************************************
     *   NAME: private static String gPackage = "xxwc.oracle.apps.inv.bins.server";
     *
     *   PURPOSE:   private method to default gPackage value for debugging
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00158 - RF - Put-Away
     **************************************************************************/

    private static String gPackage = "xxwc.oracle.apps.inv.bins.server";


    /*************************************************************************
     *   NAME: private static String gCallFrom = "XXWCBinPutAwayFListener";
     *
     *   PURPOSE:   private method to pass gCallFrom from methods to return class
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20150302-00158 - RF - Put-Away
     **************************************************************************/

    private static String gCallFrom = "XXWCBinPutAwayFListener";

    /*************************************************************************
     *   NAME: public XXWCBinPutAwayFListener()
     *
     *   PURPOSE:   public method XXWCBinPutAwayFListener
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                               TMS Ticket 20150302-00158 - RF - Put-Away
     **************************************************************************/

    public XXWCBinPutAwayFListener() {
        $init$();
    }

    /*************************************************************************
     *   NAME:  public void fieldEntered(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                       DefaultOnlyHandlerException
     *
     *   PURPOSE:   field entered for XXWCBinPutAwayFListener
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20150302-00158 - RF - Put-Away
     **************************************************************************/
    
    public void fieldEntered(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                       DefaultOnlyHandlerException {
        ses = mwaevent.getSession();
        pg = (XXWCBinPutAwayPage)ses.getCurrentPage();
        String s = UtilFns.fieldEnterSource(ses);
        if(s.equals("INV.ITEM"))
        {
            enteredItem(mwaevent);
            return;
        }
        if(s.equals("XXWC.BIN"))
        {
            enteredBins(mwaevent);
            return;
        } 
        else
        {
            return;
        }
    }

    /*************************************************************************
     *   NAME:      public void fieldExited(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException
     *
     *   PURPOSE:   field exited for XXWCBinPutAwayFListener
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20150302-00158 - RF - Put-Away
            1.1       01-JUN-2015   Lee Spitzer             TMS Ticket 20150604-00154 RF Bundle 1
                                                                 Add Delete Button

     **************************************************************************/

    public void fieldExited(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException {
               String s = ((FieldBean)mwaevent.getSource()).getName();
               String vField = mwaevent.getAction();
               boolean flag = false;
               boolean flag1 = false;
               boolean flag3 = false;
               if(mwaevent.getAction().equals("MWA_SUBMIT"))
               {
                   flag = true;
               } else
               if(mwaevent.getAction().equals("MWA_PREVIOUSFIELD"))
               {
                   flag1 = true;
               } else
               if(mwaevent.getAction().equals("MWA_NEXTFIELD"))
               {
                   flag3 = true;
               }
               if(s.equals("INV.ITEM"))
               {
                   if (pg.getmItemFld().getValue().equals(null) || pg.getmItemFld().getValue().equals("")) {
                    
                   }
                   else {
                    exitedItem(mwaevent, ses);
                   }
                   
                return;
               
               }
               if(s.equals("XXWC.BIN"))
               {
                   exitedLocator(mwaevent, ses);
                   return;
               }
               /*Added TMS Ticket 20150604-00154 01-JUN-2015*/
               if(flag && s.equals("XXWC.DELETE"))
               {
                   deleteItemLocatorTie(mwaevent, ses);
                   clearFields(mwaevent, ses);
                   return;
               }
               if(flag && s.equals("XXWC.PRINT_ITEM_LABEL"))
               {
                   exitedPrintItemLabel(mwaevent, ses);
                   return;
               }
               if(flag && s.equals("INV.DONE"))
               {
                   exitedDone(mwaevent, ses);
                   return;
               }
               if (flag && s.equals("XXWC.CANCEL")) 
                   {
                       exitedCancel(mwaevent, ses);
                       return;
               }
               else
               {
                   return;
               }
    }

    /*************************************************************************
     *   NAME:      private void $init$()
     *
     *   PURPOSE:   $init$ sets the diaglogButtons button for user prompt messages to OK
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
         1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                             TMS Ticket 20150302-00158 - RF - Put-Away
     **************************************************************************/

    private void $init$() {
        dialogPageButtons = (new String[] { "OK" });
    }


    /*************************************************************************
     *   NAME: public void enteredItem(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException
     *
     *   PURPOSE:  Entered Item Field Listener Process to set the List of Values for the Item Field
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                  TMS Ticket 20150302-00158 - RF - Put-Away
     **************************************************************************/


    public void enteredItem(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException {

        try {
                XXWCItemLOV itemlov = pg.getmItemFld();
                itemlov.setValidateFromLOV(true);
                itemlov.setlovStatement("XXWC_MWA_ROUTINES_PKG.GET_WC_ITEM_LOV");
                String paramType[] = {
                    "C", "N", "S"
                };
                String parameters[] = {
                    " ", "ORGID", "xxwc.oracle.apps.inv.bins.server.XXWCBinPutAwayPage.INV.ITEM"
                };
                itemlov.setInputParameterTypes(paramType);
                itemlov.setInputParameters(parameters);
            }
            catch(Exception e)
            {
                UtilFns.error((new StringBuilder()).append("Error in calling enteredItem").append(e).toString());
            }
    }

    /*************************************************************************
     *   NAME: public void exitedItem(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException
     *
     *   PURPOSE:  Exited Item Field Listener to populate the Item Description Field
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00158 - RF - Put-Away
            1.1       01-JUN-2015   Lee Spitzer             TMS Ticket 20150604-00154 RF Bundle 1
                                                                     Add Delete Button
                                                            TMS Ticket 20150601-00250
                                                                    RF Break fix: Unable to assign a bin location if item has no bin locations assigned
     **************************************************************************/

    public void exitedItem(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                  DefaultOnlyHandlerException {
        try
            {
                pg.getmItemFld().getValue().toUpperCase();
                pg.getmItemDescFld().setValue(pg.getmItemFld().getmItemDescription());
                ses.setNextFieldName("XXWC.BIN");
                if(pg.getmBinFld().getValue().isEmpty()){
                    if (pg.getmItemFld().getValue().equals("") || pg.getmItemFld().getValue().equals(null)) {
                       return;
                    }
                    else {
                    
                        //Added TMS Ticket 20150601-00250 01-JUN-2015
                    
                    
                        CallableStatement cstmt = null;
                        Connection con = ses.getConnection();
                        String p_organization_id = (String)ses.getObject("ORGID");
                        String p_inventory_item_id = (String)pg.getmItemFld().getmInventoryItemId();
                        String p_subinventory_code = (String)pg.getmBinFld().getmSubinventory();
                        Integer x_return;
                        x_return = 0;
                        try {
                            //                        1                                                 2 3 4
                            cstmt = con.prepareCall("{? = call XXWC_LABEL_PRINTERS_PKG.GET_WC_BIN_COUNT(?,?,?)}");
                            cstmt.registerOutParameter(1, Types.NUMERIC);
                            cstmt.setString(2, p_organization_id);
                            cstmt.setString(3, p_subinventory_code);
                            cstmt.setString(4, p_inventory_item_id);
                            cstmt.execute();
                            x_return = cstmt.getInt(1);
                            cstmt.close();
                        }
                        catch (Exception exception) {
                            UtilFns.error((new StringBuilder()).append("Error in calling exitedItem").append(exception).toString());  
                        }
                        
                        
                        
                        if (x_return == 0) {
                            pg.getmBinFld().setHidden(false);
                            ses.setNextFieldName("XXWC.BIN");
                            pg.getmBinFld().setRequired(false);
                            pg.getmDone().setHidden(false);
                            pg.getmDelete().setHidden(true); //Added TMS Ticket 20150302-00158
                            pg.getmPrintItemLabel().setHidden(false);                                   
                        }
                        else {
                            pg.getmBinFld().setHidden(false);
                            ses.setNextFieldName("XXWC.BIN");
                            pg.getmBinFld().setRequired(true);
                            ses.putObject("MWA_AUTO_ENTER", Boolean.TRUE);
                        }
                        }
                    ///End Added TMS Ticket 20150601-00250 01-JUN-2015 
                     }
                }
            catch(Exception e)
            {
                UtilFns.error((new StringBuilder()).append("Error in calling exitedItem").append(e).toString());
            }
        }

 
    /*************************************************************************
    *   NAME: public void enteredLocator(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException
    *
    *   PURPOSE:  Entered Locator Field Listener Process to set the List of Values for the Locator Field
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
         1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00158 - RF - Put-Away
    **************************************************************************/

    public void enteredLocator(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                        DefaultOnlyHandlerException {

    try
    {
        ses.putSessionObject("sessInventoryItemId", pg.getmItemFld().getmInventoryItemId());
        ses.putSessionObject("sessRestrictLocator", Integer.valueOf(1));
        ses.putSessionObject("sessHidePrefix", "N");
        XXWCBinsLOV binsLOV = pg.getmBinFld();
        String paramType[] = {
            "C", "N", "S", "N", "N", "S", "S", "S", "S"
        };
        String prompts[] = {
            "WC_LOCATOR", "PREFIX", "LOCATOR_ID", "LOCATOR", "DESCRIPTION", "SUBINVENTORY_CODE"
        };
        boolean flag[] = {
            true, true, false, true, false, false
        };
        String parameters[] = {
            " ", "ORGID", "", "sessRestrictLocator", "sessInventoryItemId", "xxwc.oracle.apps.inv.bins.server.XXWCBinPutAwayPage.XXWC.BIN", "", "", "sessHidePrefix"
        };
        binsLOV.setInputParameters(parameters);
        binsLOV.setlovStatement("XXWC_INV_BIN_MAINTENANCE_PKG.GET_WC_BIN_LOC");
        binsLOV.setInputParameterTypes(paramType);
        binsLOV.setSubfieldPrompts(prompts);
        binsLOV.setSubfieldDisplays(flag);
    }
    catch(Exception e)
    {
        UtilFns.error((new StringBuilder()).append("Error in calling enteredItem ").append(e).toString());
    }
    }

    /*************************************************************************
    *   NAME: public void exitedLocator(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException
    *
    *   PURPOSE:  Exited Subinventory Field Listener Process to determine if the Locator Field should be displayed
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20150302-00158 - RF - Put-Away
            1.1       01-JUN-2015   Lee Spitzer             TMS Ticket 20150604-00154 RF Bundle 1
                                                                 Add Delete Button
    **************************************************************************/

    public void exitedLocator(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                DefaultOnlyHandlerException {
        try
            {
                if(pg.getmBinFld().getValue().equals(null) || pg.getmBinFld().getValue().equals("")) //Added TMS Ticket 20150604-00154 01-JUN-2015
                {
                    pg.getmDone().setHidden(false);
                    pg.getmPrintItemLabel().setHidden(false);
                    pg.getmDelete().setHidden(true); //Added TMS Ticket 20150604-00154 01-JUN-2015
                } else {
                    pg.getmDone().setHidden(false);
                    pg.getmPrintItemLabel().setHidden(false);
                    pg.getmDelete().setHidden(false); //Added TMS Ticket 20150604-00154 01-JUN-2015
                    
                }
            }
            catch(Exception e)
            {
                UtilFns.error((new StringBuilder()).append("Error in calling exitedLocator").append(e).toString());
            }
    }



    /*************************************************************************
    *   NAME:     public void clearFields(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                                 InterruptedHandlerException,
                                                                                 DefaultOnlyHandlerException
    *
    *   PURPOSE:  Clears the values in the form
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
        1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00158 - RF - Put-Away
        1.1       01-JUN-2015   Lee Spitzer             TMS Ticket 20150604-00154 RF Bundle 1
                                                                 Add Delete Button
    **************************************************************************/
    
    public void clearFields(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                   InterruptedHandlerException,
                                                                   DefaultOnlyHandlerException {
        pg.getmItemFld().setValue(null);
        pg.getmItemDescFld().setValue(null);
        pg.getmBinFld().setValue(null);
        pg.getmDone().setHidden(true); //Added TMS Ticket 20150604-00154 01-JUN-2015
        pg.getmBinFld().setHidden(true); //Added TMS Ticket 20150604-00154 01-JUN-2015
        pg.getmPrintItemLabel().setHidden(true); //Added TMS Ticket 20150604-00154 01-JUN-2015
        ses.setNextFieldName("INV.ITEM");
    }
        
    /*************************************************************************
    *   NAME:     public void exitedDone(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                                 InterruptedHandlerException,
                                                                                 DefaultOnlyHandlerException
    *
    *   PURPOSE:  Clears the values in the form
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
        1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00158 - RF - Put-Away

    **************************************************************************/
    
    protected void exitedDone(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                        DefaultOnlyHandlerException {
        /*try {
                XXWCBinPutAwayPage _tmp = pg;
                ses.setStatusMessage(pg.WMS_TXN_SUCCESS);
                ses.clearAllApplicationScopeObjects();
                pg.getmDone().setNextPageName("|END_OF_TRANSACTION|");
            } 
        catch (Exception e) {
            UtilFns.error("Error in calling exitedDone " + e);
        }
    */
        try
                {
                    String gMethod = "exitedDone";
                    String gCallPoint = "Start";
                    String l_current_page = ses.getCurrentPageName();
                    String l_item_description = "";
                    String l_subinventory = "";
                    l_item_description = pg.getmItemFld().getmItemDescription(); //pg.getmItemDescFld().getValue();
                    l_subinventory = pg.getmBinFld().getmSubinventory();
                    if (l_subinventory.equals(null) || l_subinventory.equals("")) {
                        //Setting the subinventory value to "" 
                        l_subinventory = "General";
                    }
                    FileLogger.getSystemLogger().trace((new StringBuilder()).append(gPackage).append(".").append(gCallPoint).append(" ").append(gMethod).append(" ").append(gCallPoint).append(" l_item_description ").append(l_item_description).toString());
                    FileLogger.getSystemLogger().trace((new StringBuilder()).append(gPackage).append(".").append(gCallPoint).append(" ").append(gMethod).append(" ").append(gCallPoint).append(" l_subinventory ").append(l_subinventory).toString());
                    ses.putSessionObject("sessPreviousPage", l_current_page);
                    ses.putSessionObject("sessPreviousField", "XXWC.BIN");
                    ses.putSessionObject("sessXXWC.ITEM", pg.getmItemFld().getValue());
                    ses.putSessionObject("sessInventoryItemId", pg.getmItemFld().getmInventoryItemId().toString());
                    ses.putSessionObject("sessXXWC.DESCRIPTION", pg.getmItemFld().getmItemDescription().toString());
                    ses.putSessionObject("sessXXWC.SUBINVENTORY", l_subinventory);
                    ses.putSessionObject("sessXXWC.LOC", "");
                    ses.putSessionObject("sessXXWC.WC_PREFIX", "");
                    FileLogger.getSystemLogger().trace((new StringBuilder()).append(gPackage).append(".").append(gCallPoint).append(" ").append(gMethod).append(" ").append(gCallPoint).append(" sessPreviousPage ").append(ses.getObject("sessPreviousPage")).toString());
                    FileLogger.getSystemLogger().trace((new StringBuilder()).append(gPackage).append(".").append(gCallPoint).append(" ").append(gMethod).append(" ").append(gCallPoint).append(" sessPreviousField ").append(ses.getObject("sessPreviousField")).toString());
                    FileLogger.getSystemLogger().trace((new StringBuilder()).append(gPackage).append(".").append(gCallPoint).append(" ").append(gMethod).append(" ").append(gCallPoint).append(" sessXXWC.ITEM ").append(ses.getObject("sessXXWC.ITEM")).toString());
                    FileLogger.getSystemLogger().trace((new StringBuilder()).append(gPackage).append(".").append(gCallPoint).append(" ").append(gMethod).append(" ").append(gCallPoint).append(" sessInventoryItemId ").append(ses.getObject("sessInventoryItemId")).toString());
                    FileLogger.getSystemLogger().trace((new StringBuilder()).append(gPackage).append(".").append(gCallPoint).append(" ").append(gMethod).append(" ").append(gCallPoint).append(" sessXXWC.DESCRIPTION ").append(ses.getObject("sessXXWC.DESCRIPTION")).toString());
                    FileLogger.getSystemLogger().trace((new StringBuilder()).append(gPackage).append(".").append(gCallPoint).append(" ").append(gMethod).append(" ").append(gCallPoint).append(" sessXXWC.SUBINVENTORY ").append(ses.getObject("sessXXWC.SUBINVENTORY")).toString());
                    FileLogger.getSystemLogger().trace((new StringBuilder()).append(gPackage).append(".").append(gCallPoint).append(" ").append(gMethod).append(" ").append(gCallPoint).append(" sessXXWC.LOC ").append("").toString());
                    FileLogger.getSystemLogger().trace((new StringBuilder()).append(gPackage).append(".").append(gCallPoint).append(" ").append(gMethod).append(" ").append(gCallPoint).append(" sessXXWC.WC_PREFIX ").append("").toString());
                    pg.getmDone().setNextPageName("xxwc.oracle.apps.inv.bins.server.XXWCBinAssignmentPage");
                    ses.setNextFieldName("XXWC.LOC");
                    
                }
                catch(Exception e)
                {
                    UtilFns.error((new StringBuilder()).append("Error in validating exitedDone").append(e).toString());
                    ses.putSessionObject("sessPreviousPage", "");
                    ses.putSessionObject("sessPreviousField", "");
                    ses.putSessionObject("sessXXWC.ITEM", "");
                    ses.putSessionObject("sessInventoryItemId", "");
                    ses.putSessionObject("sessXXWC.SUBINVENTORY", "");
                    ses.putSessionObject("sessXXWC.LOC", "");
                    ses.putSessionObject("sessXXWC.WC_PREFIX", "");
                }
    }
    
    /*************************************************************************
    *   NAME: public void exitedPrintItemLabel(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException
    *
    *   PURPOSE:  Exited Subinventory Field Listener Process to determine if the Locator Field should be displayed
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20150302-00158 - RF - Put-Away
    **************************************************************************/

    public void exitedPrintItemLabel(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                    DefaultOnlyHandlerException {
        
        try{
            String gMethod = "exitedPrintItemLabel";
            String gCallPoint = "Start";
            String l_current_page = ses.getCurrentPageName();
            ses.putSessionObject("sessPreviousPage", l_current_page);
            ses.putSessionObject("sessPreviousField", "XXWC.NEXT_BIN");
            ses.putSessionObject("sessXXWC.ITEM", pg.getmItemFld().getValue());
            ses.putSessionObject("sessInventoryItemId", pg.getmItemFld().getmInventoryItemId().toString());
            ses.putSessionObject("sessXXWC.BIN", pg.getmBinFld().getmConcatedSegments());
            ses.putSessionObject("sessXXWC.LOCATION_ID", pg.getmBinFld().getmLocationId());
            ses.putSessionObject("sessXXWC.PRICE", "No");
            ses.putSessionObject("sessXXWC.ITEM_STATUS","4X2");
            ses.putSessionObject("sessXXWC.PRINT_LOCATION", "Yes");
            ses.putSessionObject("sessXXWC.SHELF_PRICE","No");
            ses.putSessionObject("sessXXWC.COPIES", "1");
            
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " sessPreviousPage " + ses.getObject("sessPreviousPage"));
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " sessPreviousField " + ses.getObject("sessPreviousField"));
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " sessXXWC.ITEM " + ses.getObject("sessXXWC.ITEM"));
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " sessInventoryItemId " + ses.getObject("sessInventoryItemId"));
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " sessXXWC.BIN " + ses.getObject("sessXXWC.BIN"));
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " sessInventoryLocationId " + ses.getObject("sessInventoryLocationId"));
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " sessXXWC.PRICE " + ses.getObject("sessXXWC.PRICE"));
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " sessXXWC.ITEM_STATUS " + ses.getObject("sessXXWC.ITEM_STATUS"));
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " sessXXWC.PRINT_LOCATION " + ses.getObject("sessXXWC.PRINT_LOCATION"));
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " sessXXWC.SHELF_PRICE " + ses.getObject("sessXXWC.SHELF_PRICE"));
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " sessXXWC.COPIES " + ses.getObject("sessXXWC.COPIES"));

            pg.getmPrintItemLabel().setNextPageName("xxwc.oracle.apps.inv.labels.server.XXWCItemLabelPage");
            ses.setNextFieldName("XXWC.ITEM");
            }
            catch (Exception e) {
                UtilFns.error("Error in validating Quantity" + e);
                     ses.putSessionObject("sessXXWC.ITEM", "");
                     ses.putSessionObject("sessXXWC.BIN", "");
                     ses.putSessionObject("sessXXWC.LOCATION_ID", "");
                     ses.putSessionObject("sessXXWC.PRICE", "");
                     ses.putSessionObject("sessXXWC.ITEM_STATUS","");
                     ses.putSessionObject("sessXXWC.PRINT_LOCATION", "");
                     ses.putSessionObject("sessXXWC.SHELF_PRICE","");
                     ses.putSessionObject("sessXXWC.COPIES", "");
            }
        }
    
    /*************************************************************************
    *   NAME: public void exitedNextItem(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                     DefaultOnlyHandlerException
    *
    *   PURPOSE:  Exited Clear For Field Listener Process to proces the Clear Button
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
             1.0       21-OCT-2014   Lee Spitzer             Initial Version -
    **************************************************************************/

    public void exitedNextItem(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                   DefaultOnlyHandlerException {
        try {
                pg.getmItemFld().clear();
                pg.getmItemDescFld().setValue("");
                pg.getmBinFld().clear();
                ses.setNextFieldName("INV.ITEM");
        } catch (Exception e) {
            UtilFns.error("Error in calling exitedNextItem " + e);
        }

    }

    /*************************************************************************
    *   NAME: public void enteredBins(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                     DefaultOnlyHandlerException
    *
    *   PURPOSE:  Exited Clear For Field Listener Process to proces the Clear Button
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
             1.0       21-OCT-2014   Lee Spitzer             Initial Version -
    **************************************************************************/


    public void enteredBins(MWAEvent mwaevent)
         throws AbortHandlerException, InterruptedHandlerException, DefaultOnlyHandlerException
     {
         try
         {
             ses.putSessionObject("sessInventoryItemId", pg.getmItemFld().getmInventoryItemId());
             ses.putSessionObject("sessRestrictLocator", Integer.valueOf(1));
             ses.putSessionObject("sessHidePrefix", "N");
             XXWCBinsLOV binsLOV = pg.getmBinFld();
             String paramType[] = {
                 "C", "N", "S", "N", "N", "S", "S", "S", "S"
             };
             String prompts[] = {
                 "WC_LOCATOR", "PREFIX", "LOCATOR_ID", "LOCATOR", "DESCRIPTION", "DUBINVENTORY"
             };
             boolean flag[] = {
                 false, false, false, true, false, false
             };
             String parameters[] = {
                 " ", "ORGID", "", "sessRestrictLocator", "sessInventoryItemId", "xxwc.oracle.apps.inv.labels.server.XXWCItemLabelPage.XXWC.BIN", "", "", "sessHidePrefix"
             };
             binsLOV.setInputParameters(parameters);
             binsLOV.setlovStatement("XXWC_LABEL_PRINTERS_PKG.GET_BIN_LOC");
             binsLOV.setInputParameterTypes(paramType);
             binsLOV.setSubfieldPrompts(prompts);
             binsLOV.setSubfieldDisplays(flag);
         }
         catch(Exception e)
         {
             UtilFns.error((new StringBuilder()).append("Error in calling enteredItem ").append(e).toString());
         }
     }
    
    /*************************************************************************
    *   NAME: public void exitedCancel(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                     DefaultOnlyHandlerException
    *
    *   PURPOSE:  Exited Close For Field Listener Process to proces the Close Button
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                               TMS Ticket 20150302-00164 - RF - Bin maintenance form   
    **************************************************************************/

    public void exitedCancel(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                   DefaultOnlyHandlerException {
        try {
            XXWCBinPutAwayPage _tmp = pg;
            ses.setStatusMessage(pg.WMS_TXN_CANCEL);
            ses.clearAllApplicationScopeObjects();
            ses.setStatusMessage(XXWCBinPutAwayPage.WMS_TXN_CANCEL);
            pg.getmCancel().setNextPageName("|END_OF_TRANSACTION|");
        } catch (Exception e) {
            UtilFns.error("Error in calling exitedCancel " + e);
        }

    }
    
    /*************************************************************************
    *   NAME: public void deleteItemLocatorTie(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException
    *
    *   PURPOSE:  Exited Subinventory Field Listener Process to determine if the Locator Field should be displayed
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20150302-00164 - RF - Bin maintenance form 
            1.1       01-JUN-2015   Lee Spitzer             TMS Ticket 20150604-00154 RF Bundle 1
                                                                  Add Delete Button
    **************************************************************************/
    /*Added TMS Ticket - 20150604-00154 01-JUN-2015*/
    public void deleteItemLocatorTie(MWAEvent mwaevent, Session ses)
        throws AbortHandlerException, InterruptedHandlerException, DefaultOnlyHandlerException
    {
    TelnetSession telnetsession = (TelnetSession)ses;
    Integer l =
        telnetsession.showPromptPage("Warning!", "Are you sure you want to delete bin assignment Item " + pg.getmItemFld().getValue() + " subinventory " + pg.getmBinFld().getmSubinventory() + " Locator " + pg.getmBinFld().getmConcatedSegments() + "?", (new String[] { "Delete Bin Assignment", "Back"}));

    
    if (l == 0) {
     
        CallableStatement cstmt = null;
        Connection con = ses.getConnection();
        String p_organization_id = (String)ses.getObject("ORGID");
        String p_inventory_item_id = (String)pg.getmItemFld().getmInventoryItemId();
        String p_subinventory_code = (String)pg.getmBinFld().getmSubinventory();
        String p_locator_id = (String)pg.getmBinFld().getmLocationId();
        Integer x_return;
        String x_message;
        try {
            //                                                                                  1 2 3 4 5 6
            cstmt = con.prepareCall("{call XXWC_INV_BIN_MAINTENANCE_PKG.DELETE_ITEM_LOCATOR_TIE(?,?,?,?,?,?)}");
            cstmt.setString(1, p_organization_id);
            cstmt.setString(2, p_inventory_item_id);
            cstmt.setString(3, p_subinventory_code);
            cstmt.setString(4, p_locator_id);
            cstmt.registerOutParameter(5, Types.NUMERIC);
            cstmt.registerOutParameter(6, Types.VARCHAR);
            cstmt.execute();
            x_return = cstmt.getInt(5);
            x_message = cstmt.getString(6);
            cstmt.close();
            if (x_return == 0) {
                ses.setStatusMessage((new StringBuilder()).append(pg.WMS_TXN_SUCCESS).append(" (").append(pg.getmItemFld().getValue() + " | " + pg.getmBinFld().getmConcatedSegments() + " " + x_message).append(")").toString());
                return;
            }
            if (x_return != 0) {
                TelnetSession telnetsessionX = (TelnetSession)ses;
                int k = telnetsessionX.showPromptPage("Error!", x_message, dialogPageButtons);
                if (k == 0) {
                    ses.setRefreshScreen(true);
                } else {
                    return;
                }
            }
        } catch (Exception e) {
            ses.setStatusMessage((new StringBuilder()).append(pg.WMS_TXN_ERROR).append(" (").append(e).append(")").toString());
            UtilFns.error((new StringBuilder()).append("Error in deleteItemLocatorTie: error - ").append(e).toString());
        }
    }
    else if (l == 1) {
        clearFields(mwaevent, ses);
        
    }
    else {
    
        return;
    }
    
    }

   
}
