/*************************************************************************
   *   $Header XXWCBinPutAwayQueryManager.java $
   *   Module Name: XXWCBinPutAwayQueryManager
   *   
   *   Package: package xxwc.oracle.apps.inv.bins.server;
   *   
   *   Imports Classes:
   *   
   *   import java.sql.*;
   *   import java.util.Hashtable;
   *   import java.util.Vector;
   *   import oracle.apps.fnd.common.VersionInfo;
   *   import oracle.apps.inv.invinq.server.ItemOnhand;
   *   import oracle.apps.inv.utilities.server.UtilFns;
   *   import oracle.apps.mwa.container.FileLogger;
   *   import oracle.jdbc.OracleCallableStatement;
   *   import oracle.jdbc.OraclePreparedStatement;

   *
   *   PURPOSE:   Java Class for XXWCBinPutAwayQueryManager.  This calls XXWC_INV_PRIMARY_BIN_PKG.GET_MILD_INQUIRY and controls parameters and result values
   *    
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
        1.0       21-OCT-2014   Lee Spitzer             Initial Version - 
                                                            TMS Ticket 20150302-00158 - RF - Put-Away
**************************************************************************/

package xxwc.oracle.apps.inv.bins.server;

import java.sql.*;
import java.util.Hashtable;
import java.util.Vector;
import oracle.apps.fnd.common.VersionInfo;
import oracle.apps.inv.invinq.server.ItemOnhand;
import oracle.apps.inv.utilities.server.UtilFns;
import oracle.apps.mwa.container.FileLogger;
import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OraclePreparedStatement;


/*************************************************************************
 *   NAME: XXWCBinPutAwayQueryManager
 *
 *   PURPOSE:   Main class for XXWCBinPutAwayQueryManager
 *
 *   REVISIONS:
 *   Ver        Date        Author                     Description
 *   ---------  ----------  ---------------         -------------------------
        1.0       21-OCT-2014   Lee Spitzer             Initial Version -
                                                            TMS Ticket 20150302-00158 - RF - Put-Away
 **************************************************************************/

public class XXWCBinPutAwayQueryManager {

    public static final String RCS_ID =
        "$Header: XXWCBinPutAwayQueryManager.java 120.5 2007/12/19 15:33:21 vssrivat ship $";
    public static final boolean RCS_ID_RECORDED =
        VersionInfo.recordClassVersion("$Header: XXWCBinPutAwayQueryManager.java 120.5 2007/12/19 15:33:21 vssrivat ship $",
                                       "xxwc.oracle.apps.inv.bins.server");
    XXWCBinPutAway currentRecord;
    Connection con;

    /*************************************************************************
     *   NAME: private static String gPackage = "xxwc.oracle.apps.inv.bins.server";
     *
     *   PURPOSE:   private method to default gPackage value for debugging
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       21-OCT-2014   Lee Spitzer             Initial Version -
     **************************************************************************/

    private static String gPackage = "xxwc.oracle.apps.inv.bins.server.XXWCBinPutAwayQueryManager";


    /*************************************************************************
     *   NAME: private static String gCallFrom = "XXWCBinPutAwayQueryManager";
     *
     *   PURPOSE:   private method to pass gCallFrom from methods to return class name CustomListener
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       21-OCT-2014   Lee Spitzer             Initial Version -
     **************************************************************************/

    private static String gCallFrom = "XXWCBinPutAwayQueryManager";

     /*************************************************************************
     *   NAME: public static String GET_WC_BIN_LOC
     *
     *   PURPOSE:   public method to call XXWC_INV_PRIMARY_BIN_PKG.GET_MILD_INQUIRY
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       21-OCT-2014   Lee Spitzer             Initial Version -
     **************************************************************************/

    public static String mWCBinLoc =
        "BEGIN   XXWC_INV_BIN_MAINTENANCE_PKG.GET_WC_BIN_LOC(:1, :2, :3, :4, :5, :6, :7, :8, :9); END; ";

    /*************************************************************************
    *   NAME: XXWCBinPutAwayQueryManager(Connection connection)
    *
    *   PURPOSE:   public method to set the connection
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
             1.0       21-OCT-2014   Lee Spitzer             Initial Version -
    **************************************************************************/

    public XXWCBinPutAwayQueryManager(Connection connection) {
        con = connection;
    }

    /*************************************************************************
    *   NAME:  public Vector getmWCBinLoc(String p_organization_id, String p_inventory_item_id,
                                              String p_subinventory_code, String p_locator_id, String p_default_type)
    *
    *   PURPOSE:   public method to input the parameters for the mtlItemLocatorDefaultStr and retrieve the resultset
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
             1.0       21-OCT-2014   Lee Spitzer             Initial Version -
    **************************************************************************/


    public Vector getmWCBinLoc(String p_organization_id, String p_subinventory_code,
                                              String p_restrict_locators_code, String p_inventory_item_id, String p_concatenated_segments, 
                                              String p_project_id, String p_task_id, String p_hide_prefix) {
        String gMethod = "getmWCBinLoc";
        String gCallPoint = "Start";
        FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod  + " " + gCallPoint);
        Vector vector;
        OracleCallableStatement oraclecallablestatement;
        ResultSet resultset;
        vector = new Vector();
        oraclecallablestatement = null;
        resultset = null;
        try {
            gCallPoint = "Trying mWCBinLoc";
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod  + " " + gCallPoint);
            gCallPoint = "Getting resultset";
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod  + " " + gCallPoint + " p_organization_id " +
                                               p_organization_id);
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod  + " " + gCallPoint + " p_subinventory_code " +
                                               p_subinventory_code);
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod  + " " + gCallPoint + " p_restrict_locators_code " +
                                               p_restrict_locators_code);
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod  + " " + gCallPoint + " p_inventory_item_id " +
                                               p_inventory_item_id);
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod  + " " + gCallPoint + " p_concatenated_segments " +
                                               p_concatenated_segments);
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod  + " " + gCallPoint + " p_project_id " +
                                               p_project_id);
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod  + " " + gCallPoint + " p_task_id " +
                                               p_task_id);
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod  + " " + gCallPoint + " p_hide_prefix " +
                                               p_hide_prefix);
            oraclecallablestatement = (OracleCallableStatement)con.prepareCall(mWCBinLoc);
            oraclecallablestatement.registerOutParameter(1, -10);
            oraclecallablestatement.setString(2, p_organization_id);
            oraclecallablestatement.setString(3, p_subinventory_code);
            oraclecallablestatement.setString(4, p_restrict_locators_code);
            oraclecallablestatement.setString(5, p_inventory_item_id);
            oraclecallablestatement.setString(6, p_concatenated_segments);
            oraclecallablestatement.setString(7, p_project_id);
            oraclecallablestatement.setString(8, p_task_id);
            oraclecallablestatement.setString(9, p_hide_prefix);
            oraclecallablestatement.execute();
            resultset = (ResultSet)oraclecallablestatement.getObject(1);
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod  + " " + gCallPoint + " resultset " +
                                               resultset);

            do {
                gCallPoint = "do";
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod  + " " + gCallPoint);

                if (!resultset.next()) {
                    gCallPoint = "!resultset.next";
                    FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod  + " " + gCallPoint);
                    break;
                }
                gCallPoint = "Getting resultset";
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod  + " " + gCallPoint);
                String x_wc_locator = resultset.getString(1);
                String x_wc_pre_fix = resultset.getString(2);
                String x_inventory_location_id = resultset.getString(3);
                String x_concatenated_segments = resultset.getString(4);
                String x_description = resultset.getString(5);
                String x_subinventory = resultset.getString(6);
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod  + " " + gCallPoint + " x_wc_locator " + x_wc_locator);
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod  + " " + gCallPoint + " x_wc_pre_fix " + x_wc_pre_fix);
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod  + " " + gCallPoint + " x_inventory_location_id " + x_inventory_location_id);
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod  + " " + gCallPoint + " x_concatenated_segments " + x_concatenated_segments);
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod  + " " + gCallPoint + " x_description " + x_description);
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod  + " " + gCallPoint + " x_subinventory " + x_subinventory);
                currentRecord =
                        new XXWCBinPutAway(x_wc_locator, x_wc_pre_fix, x_inventory_location_id, x_concatenated_segments, x_description, x_subinventory);
                vector.addElement(currentRecord);
            } while (true);
            gCallPoint = "while";
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod  + " " + gCallPoint);
            if (resultset != null) {
                gCallPoint = "resultset.close";
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod  + " " + gCallPoint);
                resultset.close();
            }
            if (oraclecallablestatement != null) {
                gCallPoint = "oraclecallablestatement is not null";
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod  + " " + gCallPoint +
                                                   " oraclecallablestatement " + oraclecallablestatement);
                oraclecallablestatement.close();
                con.close(); //added 5/12/2015
            }
        } catch (Exception e) {
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod  + " " + gCallPoint + " " + e);
        }
        gCallPoint = "Vector";
        FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod  + " " + gCallPoint + " " + vector);
        return vector;
    }


}
