/*************************************************************************
   *   $Header XXWCBinPutAway.java $
   *   Module Name: XXWCBinPutAwayFListener
   *   
   *   Package xxwc.oracle.apps.inv.bins.server;
   *   
   *   Imports Classes
   *   
   *   import oracle.apps.fnd.common.VersionInfo;
   *  
   *   PURPOSE:   XXWCBinPutAway returns values from query
   *   
   *    
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
        1.0       21-OCT-2014   Lee Spitzer             Initial Version - 
                                                           TMS Ticket 20150302-00158 - RF - Put-Away
**************************************************************************/
package xxwc.oracle.apps.inv.bins.server;

import oracle.apps.fnd.common.VersionInfo;

/*************************************************************************
 *   NAME: public class XXWCBinPutAway
 *
 *   PURPOSE:   Main class for XXWCBinPutAway 
 *
 *   REVISIONS:
 *   Ver        Date        Author                     Description
 *   ---------  ----------  ---------------         -------------------------
        1.0       21-OCT-2014   Lee Spitzer             Initial Version -
                                                         TMS Ticket 20150302-00158 - RF - Put-Away
 **************************************************************************/

public class XXWCBinPutAway {
    public static final String RCS_ID = "$Header: ItemOnhand.java 120.1 2006/03/22 03:16:04 rsagar noship $";
    public static final boolean RCS_ID_RECORDED =
        VersionInfo.recordClassVersion("$Header: ItemOnhand.java 120.1 2006/03/22 03:16:04 rsagar noship $",
                                       "oracle.apps.inv.invinq.server");

    String mWCLocator;
    String mWCPreFix;
    String mLocationId;
    String mConcatedSegments;
    String mDescription;
    String mDefaultDescription;
    String mSubinventory;

    /*************************************************************************
     *   NAME: XXWCBinPutAway
     *
     *   PURPOSE:   public method XXWCBinPutAway if no values passed
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       21-OCT-2014   Lee Spitzer             Initial Version -
     **************************************************************************/

    public XXWCBinPutAway() {
        this("", "", "", "", "", "");
    }


     /*************************************************************************
     *   NAME: XXWCBinPutAway(String x_item, Long x_item_id, String x_description, String x_subinventory_code,
                                    String x_bin_locator, Long x_locator_id, Long x_default_type, Long x_max_qty, String x_default_description)
     *
     *   PURPOSE:   public method XXWCBinPutAway if values passed
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       21-OCT-2014   Lee Spitzer             Initial Version -
     **************************************************************************/
    
     public XXWCBinPutAway(String x_wc_locator, String x_wc_pre_fix, String x_inventory_location_id, String x_concatenated_segments, String x_description, String x_subinventory) {
        mWCLocator = x_wc_locator;
        mWCPreFix = x_wc_pre_fix;
        mLocationId = x_inventory_location_id;
        mConcatedSegments = x_concatenated_segments;
        mDescription = x_description;
        mSubinventory = x_subinventory;
    }

    /*************************************************************************
    *   NAME: public String getmWCLocator()
    *
    *   PURPOSE:   returns the subinventory
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       18-FEB-2015   Lee Spitzer             Initial Version -
    **************************************************************************/
          
    public String getmWCLocator() {
       return mWCLocator;
    }
    
    /*************************************************************************
    *   NAME: public String getmWCPreFix()
    *
    *   PURPOSE:   returns the subinventory
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       18-FEB-2015   Lee Spitzer             Initial Version -
    **************************************************************************/
          
    public String getmWCPreFix() {
       return mWCPreFix;
    }
    
    /*************************************************************************
     *   NAME: public String getmLocationId() 
     *
     *   PURPOSE:   returns the Description
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       18-FEB-2015   Lee Spitzer             Initial Version -
     **************************************************************************/

    public String getmLocationId() {
        return mLocationId;
    }

    /*************************************************************************
    *   NAME: public String getmConcatedSegments()
    *
    *   PURPOSE:   returns the Locator Type
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       18-FEB-2015   Lee Spitzer             Initial Version -
    **************************************************************************/
          
    public String getmConcatedSegments() {
       return mConcatedSegments;
    }

    /*************************************************************************
    *   NAME: public String getmDescription()
    *
    *   PURPOSE:   returns the Asset Type
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       18-FEB-2015   Lee Spitzer             Initial Version -
    **************************************************************************/
          
    public String getmDescription() {
       return mDescription;
    }


    /*************************************************************************
    *   NAME: public String getmSubinventory()
    *
    *   PURPOSE:   returns the Asset Type
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       18-FEB-2015   Lee Spitzer             Initial Version -
    **************************************************************************/
          
    public String getmSubinventory() {
       return mSubinventory;
    }
}
