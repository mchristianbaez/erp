/*************************************************************************
   *   $Header XXWCBinPutAwayIterator.java $
   *   Module Name: XXWCBinPutAwayIterator
   *   
   *   Package xxwc.oracle.apps.inv.bins.server;
   *   
   *   Imports Classes
   *   
   *   import oracle.apps.fnd.common.VersionInfo;
   *   import oracle.apps.inv.invinq.server.ItemOnhand;
   *   import oracle.apps.inv.invinq.server.NextRecordException;
   *   import oracle.apps.inv.invinq.server.PreviousRecordException;
   *   import oracle.apps.inv.invinq.server.UnknownMaxLeft;
   *   import oracle.apps.inv.utilities.server.UtilFns;
   *   import oracle.apps.mwa.container.FileLogger;
   *  
   *   PURPOSE:   XXWCPrimaryBinAssignmentIterator controls the Next, Previous, and Current Position of the query results
   *   
   *    
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
        1.0       21-OCT-2014   Lee Spitzer             Initial Version - 
                                                           TMS Ticket 20150302-00158 - RF - Put-Away
**************************************************************************/
package xxwc.oracle.apps.inv.bins.server;

import java.util.Vector;

import oracle.apps.fnd.common.VersionInfo;
import oracle.apps.inv.invinq.server.ItemOnhand;
import oracle.apps.inv.invinq.server.NextRecordException;
import oracle.apps.inv.invinq.server.PreviousRecordException;
import oracle.apps.inv.invinq.server.UnknownMaxLeft;
import oracle.apps.inv.utilities.server.UtilFns;
import oracle.apps.mwa.container.FileLogger;

/*************************************************************************
 *   NAME: public class XXWCBinPutAwayIterator
 *
 *   PURPOSE:   Main class for XXWCBinPutAwayIterator 
 *
 *   REVISIONS:
 *   Ver        Date        Author                     Description
 *   ---------  ----------  ---------------         -------------------------
        1.0       21-OCT-2014   Lee Spitzer             Initial Version -
                                                         TMS Ticket 20150302-00158 - RF - Put-Away
 **************************************************************************/

public class XXWCBinPutAwayIterator {
    public static final String RCS_ID = "$Header: XXWCBinPutAwayIterator.java 120.0 2005/05/25 05:18:03 appldev noship $";
    public static final boolean RCS_ID_RECORDED =
        VersionInfo.recordClassVersion("$Header: XXWCBinPutAwayIterator.java 120.0 2005/05/25 05:18:03 appldev noship $",
                                       "xxwc.oracle.apps.inv.bins.server");
    private Vector mXXWCBinPutAwaySeq;
    private int mCurrentPos;

    /*************************************************************************
     *   NAME: private static String gPackage = "xxwc.oracle.apps.inv.bins.server";
     *
     *   PURPOSE:   private method to default gPackage value for debugging
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       21-OCT-2014   Lee Spitzer             Initial Version -
     **************************************************************************/

    private static String gPackage = "xxwc.oracle.apps.inv.bins.server.XXWCBinPutAwayIterator";


    /*************************************************************************
     *   NAME: private static String gCallFrom = "XXWCPrimaryBinAssignmentIterator";
     *
     *   PURPOSE:   private method to pass gCallFrom from methods to return class name CustomListener
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       21-OCT-2014   Lee Spitzer             Initial Version -
     **************************************************************************/

    private static String gCallFrom = "XXWCBinPutAwayIterator";
    
    
    /*************************************************************************
     *   NAME: public XXWCPrimaryBinAssignmentIterator(Vector vector)
     *
     *   PURPOSE:   publice method to set mXXWCPrimaryBinAssignmentSeq in the vector and mCurrentPos
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       21-OCT-2014   Lee Spitzer             Initial Version -
     **************************************************************************/
    
    public XXWCBinPutAwayIterator(Vector vector) {
        String gMethod = "XXWCBinPutAwayIterator";
        String gCallPoint = "Start";
        if (vector == null) {
            gCallPoint = "vector == null";
            mXXWCBinPutAwaySeq = new Vector();
        } else {
            gCallPoint = "vector != null";
            mXXWCBinPutAwaySeq = vector;
        }
        FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod  + " " + gCallPoint);
        mCurrentPos = 0;
    }

    /*************************************************************************
     *   NAME: int max_left() throws UnknownMaxLeft
     *
     *   PURPOSE:   publice method get the max_left value
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       21-OCT-2014   Lee Spitzer             Initial Version -
     **************************************************************************/

    public int max_left() throws UnknownMaxLeft {
        String gMethod = "max_left";
        String gCallPoint = "Start";
        if (mXXWCBinPutAwaySeq == null) {
            gCallPoint = "mXXWCBinPutAwaySeq == null";
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod  + " " + gCallPoint);
            throw new UnknownMaxLeft();
        } else {
            gCallPoint = "mXXWCBinPutAwaySeq != null";
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod  + " " + gCallPoint + " mXXWCBinPutAwaySeq.size() " + mXXWCBinPutAwaySeq.size());
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod  + " " + gCallPoint + " mCurrentPos " +mCurrentPos);
            return mXXWCBinPutAwaySeq.size() - mCurrentPos;
        }
    }


    /*************************************************************************
     *   NAME: public XXWCPrimaryBinAssignment next() throws NextRecordException
     *
     *   PURPOSE:   publice method to the next sequence in the vector
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       21-OCT-2014   Lee Spitzer             Initial Version -
     **************************************************************************/

    public XXWCBinPutAway next() throws NextRecordException {
        boolean flag;
        try {
            int i = max_left();
            flag = i > 0;
        } catch (UnknownMaxLeft unknownmaxleft) {
            UtilFns.log((new StringBuilder()).append("Error in XXWCBinPutAway.next ").append(unknownmaxleft).toString());
            throw new NextRecordException();
        }
        XXWCBinPutAway XXWCBinPutAway;
        if (flag && mCurrentPos <= mXXWCBinPutAwaySeq.size() - 1) {
            XXWCBinPutAway =
                    (XXWCBinPutAway)mXXWCBinPutAwaySeq.elementAt(mCurrentPos++);
        } else {
            throw new NextRecordException();
        }
        return XXWCBinPutAway;
    }

    /*************************************************************************
     *   NAME: public XXWCPrimaryBinAssignment previous() throws NextRecordException
     *
     *   PURPOSE:   publice method to the previous sequence in the vector
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       21-OCT-2014   Lee Spitzer             Initial Version -
     **************************************************************************/
    
    public XXWCBinPutAway previous() throws PreviousRecordException {
        int i = mXXWCBinPutAwaySeq.size();
        if (mCurrentPos == 0) {
            throw new PreviousRecordException();
        } else {
            mCurrentPos = mCurrentPos - 1;
            XXWCBinPutAway XXWCBinPutAway = (XXWCBinPutAway)mXXWCBinPutAwaySeq.elementAt(mCurrentPos - 1);
            return XXWCBinPutAway;
        }
    }

    /*************************************************************************
     *   NAME: public XXWCBinPutAway getCurrentRecord() throws NextRecordException
     *
     *   PURPOSE:   publice method to the current record sequence in the vector
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       21-OCT-2014   Lee Spitzer             Initial Version -
     **************************************************************************/

    public XXWCBinPutAway getCurrentRecord() {
        return (XXWCBinPutAway)mXXWCBinPutAwaySeq.elementAt(mCurrentPos - 1);
    }

    /*************************************************************************
     *   NAME: public public void destroy() throws NextRecordException
     *
     *   PURPOSE:   public method to destroy the mXXWCPrimaryBinAssignmentSeq to null
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       21-OCT-2014   Lee Spitzer             Initial Version -
     **************************************************************************/
    
    public void destroy() {
        mXXWCBinPutAwaySeq = null;
        UtilFns.log("OfferIterator is destroyed");
    }

    /*************************************************************************
     *   NAME: public int getCurrentPos()
     *
     *   PURPOSE:   return the mCurrentPos;
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       21-OCT-2014   Lee Spitzer             Initial Version -
     **************************************************************************/
    
    public int getCurrentPos() {
        return mCurrentPos;
    }

    /*************************************************************************
     *   NAME: public int getSize()
     *
     *   PURPOSE:   return the mXXWCPrimaryBinAssignmentSeq size of the vector;
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       21-OCT-2014   Lee Spitzer             Initial Version -
     **************************************************************************/

    public int getSize() {
        return mXXWCBinPutAwaySeq.size();
    }

}
