
/*************************************************************************
   *   $Header XXWCBinAssignmentFListener.java $
   *   Module Name: XXWCBinAssignmentFListener
   *   
   *   Package: package xxwc.oracle.apps.inv.bins.server;
   *   
   *   Imports Classes:
   *   
   *   import java.sql.*;
   *   import oracle.apps.inv.utilities.server.UtilFns;
   *   import oracle.apps.mwa.beans.*;
   *   import oracle.apps.mwa.container.MWALib;
   *   import oracle.apps.mwa.container.Session;
   *   import oracle.apps.mwa.eventmodel.*;
   *   import oracle.apps.mwa.container.FileLogger;
   *   import oracle.apps.fnd.flexj.FlexException;
   *   import oracle.apps.inv.lov.server.ItemLOV;
   *   import oracle.apps.inv.lov.server.LocatorKFF;
   *   import oracle.apps.inv.lov.server.SubinventoryLOV;
   *   import xxwc.oracle.apps.inv.lov.server.*;
   *
   *   PURPOSE:   Java Class for XXWCBinAssignmentPage Page Layout.
   *    
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
        1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                          TMS Ticket 20150302-00164 - RF - Bin maintenance form   
**************************************************************************/
package xxwc.oracle.apps.inv.bins.server;

import java.sql.*;
import java.util.Hashtable;
import java.util.Vector;
import oracle.apps.fnd.common.VersionInfo;
import oracle.apps.inv.lov.server.*;
import oracle.apps.inv.utilities.server.*;
import oracle.apps.mwa.beans.*;
import oracle.apps.mwa.container.FileLogger;
import oracle.apps.mwa.container.Session;
import oracle.apps.mwa.eventmodel.*;
import oracle.apps.mwa.presentation.telnet.TelnetSession;
import oracle.jdbc.OraclePreparedStatement;
import oracle.sql.NUMBER;
import oracle.apps.mwa.container.MWALib;
import oracle.apps.fnd.flexj.KeyFlexfield;
import oracle.apps.inv.invinq.server.ItemOnhandIterator;
import oracle.apps.inv.invinq.server.ItemOnhandQueryManager;

import xxwc.oracle.apps.inv.lov.server.XXWCBinsLOV;
import xxwc.oracle.apps.inv.lov.server.XXWCItemLOV;

/*************************************************************************
 *   NAME: XXWCBinAssignmentFListener implements MWAFieldListener
 *
 *   PURPOSE:   Main class for XXWCBinAssignmentFListener
 *
 *   REVISIONS:
 *   Ver        Date        Author                     Description
 *   ---------  ----------  ---------------         -------------------------
        1.0       09-FEB-2015   Lee Spitzer             Initial Version -
                                                          TMS Ticket 20150302-00164 - RF - Bin maintenance form   
 **************************************************************************/


public class XXWCBinAssignmentFListener implements MWAFieldListener{
    XXWCBinAssignmentPage pg;
    Session ses;
    String dialogPageButtons[];

    /*************************************************************************
     *   NAME: private static String gPackage = "xxwc.oracle.apps.inv.bins.server";
     *
     *   PURPOSE:   private method to default gPackage value for debugging
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00164 - RF - Bin maintenance form   
     **************************************************************************/

    private static String gPackage = "xxwc.oracle.apps.inv.bins.server";


    /*************************************************************************
     *   NAME: private static String gCallFrom = "XXWCBinAssignmentFListener";
     *
     *   PURPOSE:   private method to pass gCallFrom from methods to return class
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20150302-00164 - RF - Bin maintenance form   
     **************************************************************************/

    private static String gCallFrom = "XXWCBinAssignmentFListener";

    /*************************************************************************
     *   NAME: public XXWCBinAssignmentFListener()
     *
     *   PURPOSE:   public method XXWCBinAssignmentFListener
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                               TMS Ticket 20150302-00164 - RF - Bin maintenance form   
     **************************************************************************/

    public XXWCBinAssignmentFListener() {
        $init$();
    }

    /*************************************************************************
     *   NAME:  public void fieldEntered(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                       DefaultOnlyHandlerException
     *
     *   PURPOSE:   field entered for XXWCBinAssignmentFListener
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20150302-00164 - RF - Bin maintenance form   
     **************************************************************************/
    
    public void fieldEntered(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                       DefaultOnlyHandlerException {
        ses = mwaevent.getSession();
        pg = (XXWCBinAssignmentPage)ses.getCurrentPage();
        String s = UtilFns.fieldEnterSource(ses);
        /*Item*/
        if (s.equals("XXWC.ITEM")) {
            enteredItem(mwaevent);
            return;
        }
        /*Sub*/
        if (s.equals("XXWC.SUB")) {
            enteredSub(mwaevent);
            return;
        }
        /*Loc*/
        if (s.equals("XXWC.LOC")) {
            enteredLocator(mwaevent);
            return;
        }
        /*Loc*/
        if (s.equals("XXWC.WC_PREFIX")) {
            enteredLocatorPrefix(mwaevent);
            return;
        }
        
    }

    /*************************************************************************
     *   NAME:      public void fieldExited(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException
     *
     *   PURPOSE:   field exited for XXWCBinAssignmentFListener
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20150302-00164 - RF - Bin maintenance form   
     **************************************************************************/

    public void fieldExited(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException {
        String s = ((FieldBean)mwaevent.getSource()).getName();
        String vField = mwaevent.getAction();
        boolean flag = false;
        boolean flag1 = false;
        boolean flag3 = false;
        if (mwaevent.getAction().equals("MWA_SUBMIT")) {
            flag = true;
        } else if (mwaevent.getAction().equals("MWA_PREVIOUSFIELD")) {
            flag1 = true;
        } else if (mwaevent.getAction().equals("MWA_NEXTFIELD")) {
            flag3 = true;
        }
        
        /*Item*/
        if (s.equals("XXWC.ITEM")) {
            exitedItem(mwaevent, ses);
            return;
        }
        /*Sub*/
        if (s.equals("XXWC.SUB")) {
            exitedSub(mwaevent, ses);
            return;
        }
        /*Loc*/
        if (s.equals("XXWC.LOC")) {
            exitedLocator(mwaevent, ses);
            return;
        }
        if (s.equals("XXWC.WC_PREFIX")) {
            exitedLocatorPrefix(mwaevent, ses);
            return;
        }
        /*Cancel*/
        if (flag && s.equals("XXWC.CANCEL")) {
            exitedCancel(mwaevent, ses);
            return;
        }
        /*SaveNext*/
        if (flag && s.equals("XXWC.SAVENEXT")) {
            processItemLocatorTie(mwaevent, ses);
            clearFields(mwaevent, ses);
            return;
        }
        /*Done*/
        if (flag && s.equals("XXWC.DONE")) {
            processItemLocatorTie(mwaevent, ses);
            exitedDone(mwaevent, ses);
            return;
        }
        else {
            return;
        }
    }

    /*************************************************************************
     *   NAME:      private void $init$()
     *
     *   PURPOSE:   $init$ sets the diaglogButtons button for user prompt messages to OK
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
         1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                             TMS Ticket 20150302-00164 - RF - Bin maintenance form   
     **************************************************************************/

    private void $init$() {
        dialogPageButtons = (new String[] { "OK" });
    }


    /*************************************************************************
     *   NAME: public void enteredItem(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException
     *
     *   PURPOSE:  Entered Item Field Listener Process to set the List of Values for the Item Field
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                  TMS Ticket 20150302-00164 - RF - Bin maintenance form   
     **************************************************************************/


    public void enteredItem(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException {

        try {
            XXWCItemLOV itemlov = pg.getmItemFld();
            itemlov.setValidateFromLOV(true);
            itemlov.setlovStatement("XXWC_MWA_ROUTINES_PKG.GET_WC_ITEM_LOV");
            String paramType[] = { "C", "N", "S" };
            String parameters[] =
            { " ", "ORGID", "xxwc.oracle.apps.inv.bins.server.XXWCBinAssignmentPage.XXWC.ITEM" };
            itemlov.setInputParameterTypes(paramType);
            itemlov.setInputParameters(parameters);
        } catch (Exception e) {
            UtilFns.error("Error in calling enteredItem" + e);
        }
    }

    /*************************************************************************
     *   NAME: public void exitedItem(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException
     *
     *   PURPOSE:  Exited Item Field Listener to populate the Item Description Field
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00164 - RF - Bin maintenance form   
     **************************************************************************/

    public void exitedItem(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                  DefaultOnlyHandlerException {
        try {
            pg.getmItemFld().getValue().toUpperCase();
            pg.getmItemDescFld().setValue(pg.getmItemFld().getmItemDescription());
        } catch (Exception e) {
            UtilFns.error("Error in calling exitedItem" + e);
        }
    }

    /*************************************************************************
    *   NAME: public void enteredSub(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                     DefaultOnlyHandlerException
    *
    *   PURPOSE:  Entered Subinventory Field Listener Process to set the List of Values for the Subinventory Field
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                  TMS Ticket 20150302-00164 - RF - Bin maintenance form   
    **************************************************************************/

    public void enteredSub(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException {

      pg.getmSubFld().setValue("General");
       
       try {
           SubinventoryLOV sublov = pg.getmSubFld();
           sublov.setValidateFromLOV(true);
           sublov.setlovStatement("INV_UI_ITEM_SUB_LOC_LOVS.GET_VALID_SUBS");
           String paramType[] = { "C", "N", "S" };
           String parameters[] =
           { " ", "ORGID", "xxwc.oracle.apps.inv.bins.server.XXWCBinAssignmentPage.XXWC.SUB" };
           sublov.setInputParameterTypes(paramType);
           sublov.setInputParameters(parameters);
       } catch (Exception e) {
           UtilFns.error("Error in calling enteredSub" + e);
       }
    }

    /*************************************************************************
    *   NAME: public void exitedSub(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException
    *
    *   PURPOSE:  Exited Subinventory Field Listener Process to determine if the Locator Field should be displayed
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20150302-00164 - RF - Bin maintenance form   
    **************************************************************************/

    public void exitedSub(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                DefaultOnlyHandlerException {
       try {
           Integer i = getLocatorControl(mwaevent, ses);
           if (i == 1) {
               pg.getmLocFld().setHidden(true);
               pg.getmLocFld().clear();
               pg.getmLocFld().setRequired(false);
               pg.getmLocFldPreFix().setHidden(true);
               pg.getmLocFldPreFix().clear();
               pg.getmLocFldPreFix().setRequired(false);
                  
           } else {
               pg.getmLocFld().setHidden(false);
               pg.getmLocFld().setRequired(true);
               pg.getmLocFldPreFix().setHidden(false);
               pg.getmLocFldPreFix().setRequired(false);
           }
       } catch (Exception e) {
           UtilFns.error("Error in calling exitedSub" + e);
       }
    }

    /*************************************************************************
    *   NAME: public void getLocatorControl(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException
    *
    *   PURPOSE:  Determines if Subinventory has Locator Control turned on or off
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
          1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                               TMS Ticket 20150302-00164 - RF - Bin maintenance form   
    **************************************************************************/

    private int getLocatorControl(MWAEvent mwavevent, Session session) throws AbortHandlerException,
                                                                             InterruptedHandlerException,
                                                                             DefaultOnlyHandlerException {
       int i = 1;
       int j = 1;
       int k = 1;
       int x_count = 0;
       try {
           Long orgid = Long.parseLong((String)session.getObject("ORGID"));
           FileLogger.getSystemLogger().trace("orgid  " + orgid);
           FileLogger.getSystemLogger().trace("i  " + i);
           OrgParameters orgparameters = OrgParameters.getOrgParameters(session.getConnection(), orgid);
           i = orgparameters.getLocatorControlCode();
           FileLogger.getSystemLogger().trace("i  " + i);
           FileLogger.getSystemLogger().trace("j " + j);
           SubinventoryLOV sublov = pg.getmSubFld();
           if (pg.getmSubFld().getValue() != null | pg.getmSubFld().getValue() != "") {
               j = Integer.parseInt(sublov.getLocatorType());
           }
           FileLogger.getSystemLogger().trace("j " + j);
           XXWCItemLOV itemlov = pg.getmItemFld();
           FileLogger.getSystemLogger().trace("k " + k);
           if (pg.getmItemFld().getValue() != null | pg.getmItemFld().getValue() != "") {
               k = Integer.parseInt("2");
           }
           FileLogger.getSystemLogger().trace("k " + k);
           if (UtilFns.isTraceOn) {
               UtilFns.trace((new StringBuilder()).append("RCV: getLocatorControl orgLocControl:").append(i).append(":subLocControl:").append(j).append(":itemLocControl:").append(k).toString());
           }
           
       } catch (Exception e) {
           FileLogger.getSystemLogger().trace("error getLocatorControl " + e);
       }
        //Added for WC Branches
        try {
            CallableStatement cstmt = null;
            Connection con = ses.getConnection();
            String p_organization_id = (String)ses.getObject("ORGID");
            String p_subinventory_code = (String)pg.getmSubFld().getValue();
            cstmt = con.prepareCall("{? = call XXWC_INV_BIN_MAINTENANCE_PKG.WC_BRANCH_LOCATOR_CONTROL(?,?)}");
            cstmt.setString(2, p_organization_id);
            cstmt.setString(3, p_subinventory_code);
            cstmt.registerOutParameter(1, Types.NUMERIC);
            cstmt.execute();
            x_count = cstmt.getInt(1);
            cstmt.close();
            FileLogger.getSystemLogger().trace("x_count " + x_count);
        }
        catch (Exception e) {
            UtilFns.error("Error in calling getLocatorControl" + e);
        }
        
        if (x_count > 0) {
            return 0;
        }
        else {
            return 1;
        }
    }

    /*************************************************************************
    *   NAME: public void enteredLocator(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException
    *
    *   PURPOSE:  Entered Locator Field Listener Process to set the List of Values for the Locator Field
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
         1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00164 - RF - Bin maintenance form   
    **************************************************************************/

    public void enteredLocator(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                        DefaultOnlyHandlerException {

        try {
            ses.putSessionObject("sessInventoryItemId", pg.getmItemFld().getmInventoryItemId().toString());
            ses.putSessionObject("sessRestrictLocator", 3);
            ses.putSessionObject("sessHidePrefix","N");
            XXWCBinsLOV binsLOV = pg.getmLocFld();
            //                      0    1    2    3    4    5    6    7    8
            String paramType[] = { "C", "N", "S", "N", "N", "S", "S", "S", "S" };
            String prompts[] = { "WC_LOCATOR", "PREFIX", "LOCATOR_ID", "LOCATOR", "DESCRIPTION", "SUBINVENTORY_CODE" };
            boolean flag[] = {true, true, false, true, false, false};
            //                        0        1                                                              2 
            String parameters[] = { " ", "ORGID", "xxwc.oracle.apps.inv.bins.server.XXWCBinAssignmentPage.XXWC.SUB" ,"sessRestrictLocator", "sessInventoryItemId" , "xxwc.oracle.apps.inv.bins.server.XXWCBinAssignmentPage.XXWC.LOC", "", "", "sessHidePrefix" };
            //String parameters[] = { " ", "ORGID", "" ,"", "sessInventoryItemId" , "xxwc.oracle.apps.inv.lables.server.XXWCItemLabelPage.XXWC.BIN", "", "", "N" };
            binsLOV.setInputParameters(parameters);
            binsLOV.setlovStatement("XXWC_INV_BIN_MAINTENANCE_PKG.GET_WC_BIN_LOC");
            binsLOV.setInputParameterTypes(paramType);
            binsLOV.setSubfieldPrompts(prompts);
            binsLOV.setSubfieldDisplays(flag);
            binsLOV.setValidateFromLOV(false);
            
        } catch (Exception e) {
            UtilFns.error("Error in calling enteredItem " + e);
        }
    }

    /*************************************************************************
    *   NAME: public void exitedLocator(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException
    *
    *   PURPOSE:  Exited Subinventory Field Listener Process to determine if the Locator Field should be displayed
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20150302-00164 - RF - Bin maintenance form   
    **************************************************************************/

    public void exitedLocator(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                DefaultOnlyHandlerException {
       
       
        String gMethod = "exitedLocator";
        String gCallPoint = "Start";
        FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint);
        gCallPoint = "Upper and Trim Locator Field";
        pg.getmLocFld().setValue(pg.getmLocFld().getValue().toUpperCase().trim());
        FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " locator field after triming and upper is " + pg.getmLocFld().getValue().toUpperCase().trim());
          
       try {
           //If locator is null or blank then hide done and save next
           if ((pg.getmLocFld().getValue().equals(null)) || pg.getmLocFld().getValue().equals("")){
               gCallPoint = "Locator is null";
               FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint);
               ses.setNextFieldName("XXWC.WC_PREFIX");
               pg.mDone.setHidden(true);
               pg.mSaveNext.setHidden(true);
           } 
           else {   CallableStatement cstmt = null;
                    Connection con = ses.getConnection();
                    //Need to validate the locator_id exists, if it was populated by the LOV it exists
                    String p_organization_id = (String)ses.getObject("ORGID");
                    String p_inventory_item_id = (String)pg.getmItemFld().getmInventoryItemId();
                    String p_subinventory_code = (String)pg.getmSubFld().getValue();
                    String p_locator  = (String)pg.getmLocFld().getValue();
                    String p_locator_id = (String)pg.getmLocFld().getmLocationId();
                    String p_locator_prefix = pg.getmLocFldPreFix().getValue();
                    Integer x_return;
                    String x_message;
                    //Check if locator is already assigned to item
                    try {
                        //                                                                                    1 2 3 4 5 6
                        cstmt = con.prepareCall("{call XXWC_INV_BIN_MAINTENANCE_PKG.CHECK_FOR_DUP_ASSIGNMENTS(?,?,?,?,?,?)}");
                        cstmt.setString(1, p_organization_id);
                        cstmt.setString(2, p_inventory_item_id);
                        cstmt.setString(3, p_subinventory_code);
                        cstmt.setString(4, p_locator);
                        cstmt.registerOutParameter(5, Types.NUMERIC);
                        cstmt.registerOutParameter(6, Types.VARCHAR);
                        cstmt.execute();
                        x_return = cstmt.getInt(5);
                        x_message = cstmt.getString(6);
                        cstmt.close();
                        //if the x_return is not 0, that means the locator is already assigned to the item
                        if (x_return != 0) {
                            TelnetSession telnetsessionX = (TelnetSession)ses;
                            int k = telnetsessionX.showPromptPage("Error!", x_message.replace("|",System.getProperty("line.separator")), dialogPageButtons);
                            if (k == 0) {
                                ses.setRefreshScreen(true);
                                pg.getmLocFld().setValue("");
                                pg.getmLocFldPreFix().setValue("");
                                pg.getmDone().setHidden(true);
                                pg.getmSaveNext().setHidden(true);
                                ses.setNextFieldName("XXWC.LOC");
                                return;
                            } else {
                                return;
                            }
                        }
                    } catch (Exception e) {
                        ses.setStatusMessage((new StringBuilder()).append(pg.WMS_TXN_ERROR).append(" (").append(e).append(")").toString());
                        UtilFns.error((new StringBuilder()).append("Error in processItemLocatorTie: CHECK_FOR_DUP_ASSIGNMENTS error - ").append(e).toString());
                    }

               if (pg.getmLocFld().getmLocationId().equals(null) || pg.getmLocFld().getmLocationId().equals("")){
               
               //if locator is populated then validate locator is valid since lov validation is set to false
               gCallPoint = "Locator is not null";
               FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint);
               p_organization_id = (String)ses.getObject("ORGID");
               p_locator = (String)pg.getmLocFld().getValue();
               FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " p_organization_id " + p_organization_id );
               FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " p_locator " + p_locator);
               FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " prefix " + (String)pg.getmLocFld().getmWCPreFix());
               FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " location_id  " + (String)pg.getmLocFld().getmLocationId());
               x_return = 0;
               //Validate locator exists as a 1-, 2-, 3-
               gCallPoint = "About to call XXWC_INV_BIN_MAINTENANCE_PKG.VAL_LOC";
               try {
                   FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint);
                   //                                                                      1 2 
                   con = ses.getConnection(); //added 5/12/2015
                   cstmt = con.prepareCall("{? = call XXWC_INV_BIN_MAINTENANCE_PKG.VAL_BIN(?,?)}");
                   cstmt.registerOutParameter(1, Types.NUMERIC);
                   cstmt.setString(2, p_organization_id);
                   cstmt.setString(3, p_locator);
                   cstmt.execute();
                   x_return = cstmt.getInt(1);
                   cstmt.close();
                   FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " x_return " + x_return);
                   //if there isn't a count of a valid locator then raise message the locator entered doesn't exists
                   String l_previous_page =  "";
                   l_previous_page = (String)ses.getObject("sessPreviousPage");
                   if (x_return == 0) {
                       //Allow if coming from Bin Maintenance Form to add and create locators
                       if (l_previous_page.equals("xxwc.oracle.apps.inv.bins.server.XXWCBinMaintenancePage")) {
                           gCallPoint = "x_return is 0 and Bin Maintenance" ;
                           FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint);
                           TelnetSession telnetsessionX = (TelnetSession)ses;
                           int k = telnetsessionX.showPromptPage("Error!", "Locator " + pg.getmLocFld().getValue() + " does not exist", new String [] {"Create", "Back"});
                           if (k == 0) {
                               createLocator(mwaevent, ses);
                               ses.setRefreshScreen(true);
                               pg.getmDone().setHidden(false);
                               pg.getmSaveNext().setHidden(true); //Updated 5/13/2015 to always hide field
                           }
                           if (k == 1) {
                               ses.setRefreshScreen(true);
                               pg.getmLocFld().setValue("");
                               pg.getmLocFldPreFix().setValue("");
                               pg.getmSaveNext().setHidden(true);
                               pg.getmDone().setHidden(true);
                               ses.setNextFieldName("XXWC.LOC");
                           } else {
                               return;
                           }
                       }
                       else {
                           gCallPoint = "x_return is 0 and no Bin Maintenance" ;
                           FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint);
                           TelnetSession telnetsessionX = (TelnetSession)ses;
                           int k = telnetsessionX.showPromptPage("Error!", "Locator " + pg.getmLocFld().getValue() + " does not exist", dialogPageButtons );
                           if (k == 0) {
                               ses.setRefreshScreen(true);
                               pg.getmLocFld().setValue("");
                               pg.getmLocFldPreFix().setValue("");
                               pg.mDone.setHidden(true);
                               pg.mSaveNext.setHidden(true);
                               ses.setNextFieldName("XXWC.LOC");
                           } else {
                               return;
                           }
                       }
                   }
                   else {
                       //The locator exists now get the next prefix
                       gCallPoint = "x_return is > 0";
                       p_inventory_item_id = pg.getmItemFld().getmInventoryItemId();
                       String x_next_bin = "0";
                       FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint);
                       con = ses.getConnection(); //added 5/12/2015
                       cstmt = con.prepareCall("{? = call XXWC_INV_BIN_MAINTENANCE_PKG.NEXT_BIN_PREFIX(?,?)}");
                       cstmt.registerOutParameter(1, Types.VARCHAR);
                       cstmt.setString(2, p_organization_id);
                       cstmt.setString(3, p_inventory_item_id);
                       cstmt.execute();
                       x_next_bin = cstmt.getString(1);
                       cstmt.close();
                       FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " x_next_bin " + x_next_bin);
                       if (x_next_bin.equals(null) || x_next_bin.equals("")){
                           FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " next bin is null");
                           pg.getmLocFldPreFix().setValue("");
                           pg.mDone.setHidden(true);
                           pg.mSaveNext.setHidden(true);
                           ses.setNextFieldName("XXWC.LOC");
                       }
                       else {
                           FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " next bin is not null");
                           pg.getmLocFldPreFix().setValue(x_next_bin);
                           pg.mDone.setHidden(false);
                           pg.mSaveNext.setHidden(true); //Updated 5/13/2015
                           ses.setNextFieldName("XXWC.DONE"); //Update 5/13/2015 to DONE
                       }
                   }
               }
                catch (Exception e) {
                      FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " " + e);
                      UtilFns.error("Error in calling XXWC_INV_BIN_MAINTENANCE_PKG.VAL_LOC " + e);
                }
               
               }
               else {
                   gCallPoint = "Locator id populated from LOV";
                   FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint);
                   pg.getmLocFldPreFix().setValue(pg.getmLocFld().getmWCPreFix());
                   pg.mDone.setHidden(false);
                   pg.mSaveNext.setHidden(true); //Updated 5/13/2015
                   ses.setNextFieldName("XXWC.DONE"); //Updated 5/13/2015 to XXWC.DONE
               }
        }
       }
        catch (Exception e) {
           UtilFns.error("Error in calling exitedLocator" + e);
       }
    }

    /*************************************************************************
    *   NAME: public void enteredLocatorPrefix(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException
    *
    *   PURPOSE:  Entered Locator Field Listener Process to set the List of Values for the Locator Field
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
         1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00164 - RF - Bin maintenance form   
    **************************************************************************/

    public void enteredLocatorPrefix(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                        DefaultOnlyHandlerException {

       try {
           Session session = mwaevent.getSession();
           String s = "";
           String s1 = "";
           int i = -1;
           //LocatorKFF locatorlov = pg.getmLocFld();
           //ItemLOV itemlov = pg.getmItemFld();
           String p_inventory_item_id = pg.getmItemFld().getmInventoryItemId();
           UtilFns.trace("p_inventory_item_id " + p_inventory_item_id);
           ses.putSessionObject("sessInventoryItemId", Integer.valueOf(p_inventory_item_id));
           if ((String)session.getObject("PROJECT_ID") != null &&
               !((String)session.getObject("PROJECT_ID")).equals("-9999")) {
               s = (String)session.getObject("PROJECT_ID");
               s1 = (String)session.getObject("TASK_ID");
           } else {
               s = "";
               s1 = "";
           }
           FileLogger.getSystemLogger().trace("PROJECT_ID " + s);
           FileLogger.getSystemLogger().trace("TASK_ID " + s1);

           if (pg.getmSubFld() != null && !pg.getmSubFld().getValue().equals("")) {
               i = getLocatorControl(mwaevent, session);
           }
           if (i == 3) {
               //locatorlov.setValidateFromLOV(false);
               pg.getmLocFldPreFix().setValidateFromLOV(true);
           } else {
               //locatorlov.setValidateFromLOV(true);
               pg.getmLocFldPreFix().setValidateFromLOV(true);
           }

           FileLogger.getSystemLogger().trace("i " + i);

           String s2 = "FALSE";
           if (session.getObject("wms_purchased") != null &&
               ((String)session.getObject("wms_purchased")).equals("I")) {
               s2 = "TRUE";
           }

           FileLogger.getSystemLogger().trace("WMS Enabled " + s2);
           //locatorlov.setSubMapper(pg.getmSubFld());
           //pg.getmLocFld().setSubMapper(pg.getmSubFld());
           //locatorlov.setlovStatement("INV_UI_ITEM_SUB_LOC_LOVS.GET_INQ_LOC_LOV");
           pg.getmLocFldPreFix().setlovStatement("XXWC_INV_BIN_MAINTENANCE_PKG.GET_WC_BIN_LOC");
           //                      0    1    2    3    4    5    6    7
           String paramType[] = { "C", "N", "S", "N", "N", "S", "S", "S" };
           // 0         1                                                                        2                                      3                             4                                                                    5   6  7
           String parameters[] =
           { " ", "ORGID", "xxwc.oracle.apps.inv.bins.server.XXWCBinAssignmentPage.XXWC.SUB" ,"2", "sessInventoryItemId" , "xxwc.oracle.apps.inv.bins.server.XXWCBinAssignmentPage.XXWC.LOC", s, s1 };
           //{ " ", "ORGID", "xxwc.oracle.apps.inv.bins.server.XXWCBinDeleteAssignmentPage.INV.SUB",pg.getmItemFld().getRestrictLocCode(), pg.getmItemFld().getItemID() , "xxwc.oracle.apps.inv.bins.server.XXWCBinDeleteAssignmentPage.INV.LOC", s, s1 };
           String prompts[] = { "WC_LOCATOR", "PREFIX", "LOCATOR_ID", "LOCATOR", "DESCRIPTION" };
           boolean flag[] = {false, true, false, false, false};
           pg.getmLocFldPreFix().setInputParameterTypes(paramType);
           pg.getmLocFldPreFix().setInputParameters(parameters);
           pg.getmLocFldPreFix().setSubfieldPrompts(prompts);
           pg.getmLocFldPreFix().setSubfieldDisplays(flag);
                 
       } catch (Exception e) {
           UtilFns.error("Error in calling enteredLoc" + e);
       }
    }

    /*************************************************************************
    *   NAME: public void exitedLocatorPrefix(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException
    *
    *   PURPOSE:  Exited Subinventory Field Listener Process to determine if the Locator Field should be displayed
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20150302-00164 - RF - Bin maintenance form   
    **************************************************************************/

    public void exitedLocatorPrefix(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                DefaultOnlyHandlerException {
       try {
           if (pg.getmLocFldPreFix().getValue().equals(null)){
               pg.mDone.setHidden(true);
               pg.mSaveNext.setHidden(true);
           } else {
               pg.mDone.setHidden(false);
               pg.mSaveNext.setHidden(true); //Updated 5/13/2015 to true
               
           }
       } catch (Exception e) {
           UtilFns.error("Error in calling exitedLocator" + e);
       }
    }

    /*************************************************************************
    *   NAME: public void exitedClose(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                     DefaultOnlyHandlerException
    *
    *   PURPOSE:  Exited Close For Field Listener Process to proces the Close Button
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                               TMS Ticket 20150302-00164 - RF - Bin maintenance form   
    **************************************************************************/

    public void exitedCancel(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                   DefaultOnlyHandlerException {
        try {
            XXWCBinAssignmentPage _tmp = pg;
            ses.setStatusMessage(pg.WMS_TXN_CANCEL);
            ses.clearAllApplicationScopeObjects();
            ses.setStatusMessage(XXWCBinAssignmentPage.WMS_TXN_CANCEL);
            
            String l_previous_page = "";
            l_previous_page = (String)ses.getObject("sessPreviousPage");
            if (l_previous_page.equals("") || l_previous_page.equals(null)) {
                pg.getmCancel().setNextPageName(l_previous_page);
                ses.setNextFieldName("sessPreviousField");
            }
            else {
                pg.getmCancel().setNextPageName("|END_OF_TRANSACTION|");
            }
            ses.putObject("sessPreviousPage", "");
            ses.putSessionObject("sessXXWC.ITEM", "");
            ses.putSessionObject("sessInventoryItemId", "");
            ses.putSessionObject("sessXXWC.DESCRPTION", "");
            ses.putSessionObject("sessXXWC.SUBINVENTORY", "");
        } catch (Exception e) {
            UtilFns.error("Error in calling exitedCancel " + e);
        }

    }
    
    /*************************************************************************
    *   NAME:     public void processItemLocatorTie(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                                 InterruptedHandlerException,
                                                                                 DefaultOnlyHandlerException
    *
    *   PURPOSE:  Execute the procedure XXWC_INV_PRIMARY_BIN_PKG.PROCESS_ITEM_LOCATOR_TIE
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
         1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                              TMS Ticket 20150302-00164 - RF - Bin maintenance form   
    **************************************************************************/
    
    public void processItemLocatorTie(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                             InterruptedHandlerException,
                                                                             DefaultOnlyHandlerException {
        String gMethod = "processItemLocatorTie";
        String gCallPoint = "Start";
        CallableStatement cstmt = null;
        Connection con = ses.getConnection();
        //Need to validate the locator_id exists, if it was populated by the LOV it exists
        String p_organization_id = (String)ses.getObject("ORGID");
        String p_inventory_item_id = (String)pg.getmItemFld().getmInventoryItemId();
        String p_subinventory_code = (String)pg.getmSubFld().getValue();
        String p_locator  = (String)pg.getmLocFld().getValue();
        String p_locator_prefix = pg.getmLocFldPreFix().getValue();
        Integer x_return;
        String x_message; 
        if (pg.getmLocFld().getmLocationId().equals(null) || pg.getmLocFld().getmLocationId().equals("")){
            //Need to get the locator id from the Locator Field and Locator Prefix; if that doesn't exist then need to pop up a message to create a new location
            try {
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint);
                //                                                                              1 2 3
                Integer x_locator_id = -1;
                cstmt = con.prepareCall("{? = call XXWC_INV_BIN_MAINTENANCE_PKG.GET_BIN_VAL_LOC(?,?,?)}");
                cstmt.registerOutParameter(1, Types.NUMERIC);
                cstmt.setString(2, p_organization_id);
                cstmt.setString(3, p_locator);
                cstmt.setString(4, p_locator_prefix);
                cstmt.execute();
                x_locator_id = cstmt.getInt(1);
                cstmt.close();
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " x_locator_id " + x_locator_id);
                //if there isn't a count of a valid locator then raise message the locator entered doesn't exists
                if (x_locator_id == -1) {
                    gCallPoint = "x_return is -1";
                    FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint);
                    TelnetSession telnetsessionX = (TelnetSession)ses;
                    int k = telnetsessionX.showPromptPage("Warning", "Locator " + pg.getmLocFldPreFix().getValue() + "-" + pg.getmLocFld().getValue() + " does not exist.  Would you like to create it and assign it to item " + pg.getmItemFld().getValue() + "?", new String [] {"Yes", "No"});
                    if (k == 0) {
                        gCallPoint = "Starting XXWC_INV_BIN_MAINTENANCE_PKG.CREATE_NEW_LOCATOR";
                        try {
                            con = ses.getConnection(); //added 5/12/2015
                            cstmt = con.prepareCall("{call XXWC_INV_BIN_MAINTENANCE_PKG.CREATE_NEW_LOCATOR(?,?,?,?,?,?)}");
                            cstmt.setString(1, p_organization_id);
                            cstmt.setString(2, p_subinventory_code);
                            cstmt.setString(3, p_locator);
                            cstmt.setString(4, p_locator_prefix);
                            cstmt.registerOutParameter(5, Types.NUMERIC);
                            cstmt.registerOutParameter(6, Types.VARCHAR);
                            cstmt.execute();
                            x_locator_id = cstmt.getInt(5);
                            x_message = cstmt.getString(6);
                            cstmt.close();
                            if (x_locator_id == -1) {
                                TelnetSession telnetsessionY = (TelnetSession)ses;
                                int l = telnetsessionY.showPromptPage("Error!", "Error creating locator " + p_locator_prefix + "-" + p_locator, dialogPageButtons);
                                if (l == 0) {
                                    clearFields(mwaevent, ses);
                                    return;
                                }
                            }
                            else {
                                pg.getmLocFld().setmLocationId(Integer.toString(x_locator_id));
                                pg.getmLocFld().setmWCPreFix(p_locator_prefix);
                                pg.getmLocFld().setmWCLocator(p_locator);
                                pg.getmLocFld().setmConcatedSegments(p_locator_prefix + "-" + p_locator);
                            }
                            
                        }
                        catch (Exception e) {
                            FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " error getting XXWC_INV_BIN_MAINTENANCE_PKG.CREATE_NEW_LOCATOR " + e);
                            
                        }
                    } 
                    else if (k == 1){
                        clearFields(mwaevent, ses);
                        return;
                    }
                    else {
                        return;
                    }
                }
        
                pg.getmLocFld().setmLocationId(Integer.toString(x_locator_id));
                pg.getmLocFld().setmWCPreFix(p_locator_prefix);
                pg.getmLocFld().setmWCLocator(p_locator);
                pg.getmLocFld().setmConcatedSegments(p_locator_prefix + "-" + p_locator);
            }
            catch (Exception e) {
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " error " + e);
            }
        }
        
        //Resetting x_return and x_message values;
        x_return = 0;
        x_message = "";
        String p_locator_id = (String)pg.getmLocFld().getmLocationId();
                
        //if all test validate, then create item locator tie
        try {
            con = ses.getConnection(); //added 5/12/2015
            //                                                                                   1 2 3 4 5 6
            cstmt = con.prepareCall("{call XXWC_INV_BIN_MAINTENANCE_PKG.PROCESS_ITEM_LOCATOR_TIE(?,?,?,?,?,?)}");
            cstmt.setString(1, p_organization_id);
            cstmt.setString(2, p_inventory_item_id);
            cstmt.setString(3, p_subinventory_code);
            cstmt.setString(4, p_locator_id);
            cstmt.registerOutParameter(5, Types.NUMERIC);
            cstmt.registerOutParameter(6, Types.VARCHAR);
            cstmt.execute();
            x_return = cstmt.getInt(5);
            x_message = cstmt.getString(6);
            cstmt.close();
            if (x_return == 0) {
                ses.setStatusMessage((new StringBuilder()).append(pg.WMS_TXN_SUCCESS).append(" (").append(pg.getmItemFld().getValue() + " | " + pg.getmLocFld().getmConcatedSegments() + " " + x_message).append(")").toString());
                return;
            }
            if (x_return != 0) {
                TelnetSession telnetsessionX = (TelnetSession)ses;
                int k = telnetsessionX.showPromptPage("Error!", x_message, dialogPageButtons);
                if (k == 0) {
                    ses.setRefreshScreen(true);
                } else {
                    return;
                }
            }
        } catch (Exception e) {
            ses.setStatusMessage((new StringBuilder()).append(pg.WMS_TXN_ERROR).append(" (").append(e).append(")").toString());
            UtilFns.error((new StringBuilder()).append("Error in processItemLocatorTie: error PROCESS_ITEM_LOCATOR_TIE - ").append(e).toString());
        }
    }


    /*************************************************************************
    *   NAME:     public void clearFields(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                                 InterruptedHandlerException,
                                                                                 DefaultOnlyHandlerException
    *
    *   PURPOSE:  Clears the values in the form
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
        1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00164 - RF - Bin maintenance form   
    **************************************************************************/
    
    public void clearFields(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                   InterruptedHandlerException,
                                                                   DefaultOnlyHandlerException {
        pg.mItemFld.setValue(null);
        pg.mItemDescFld.setValue(null);
        pg.mSubFld.setValue(null);
        pg.mLocFld.setValue(null);
        pg.mLocFld.setHidden(true);
        pg.mLocFldPreFix.setValue(null);
        pg.mLocFldPreFix.setHidden(true);
        pg.mSaveNext.setHidden(true);
        pg.mDone.setHidden(true);
        ses.setNextFieldName("XXWC.ITEM");
        pg.getmLocFld().clear();
        pg.getmLocFldPreFix().clear();
    }
    
    
    /*************************************************************************
    *   NAME:     public void exitedDone(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                                 InterruptedHandlerException,
                                                                                 DefaultOnlyHandlerException
    *
    *   PURPOSE:  Clears the values in the form
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
        1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00164 - RF - Bin maintenance form   

    **************************************************************************/
    
    protected void exitedDone(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                        DefaultOnlyHandlerException {
        try {
                XXWCBinAssignmentPage _tmp = pg;
                //ses.clearAllApplicationScopeObjects(); removed to prevent values from getting lost on previous pages
                String l_previous_page = "";
                l_previous_page = (String)ses.getObject("sessPreviousPage");
                if (l_previous_page.equals("") || l_previous_page.equals(null)) {
                    pg.getmDone().setNextPageName("|END_OF_TRANSACTION|");
                    ses.clearAllApplicationScopeObjects(); //01-JUN-2015 removed to prevent values 
                }
                else {
                    pg.getmDone().setNextPageName(l_previous_page);
                    ses.setNextFieldName("sessPreviousField");
                    ses.putObject("sessPreviousPage", "");
                    ses.putSessionObject("sessXXWC.ITEM", "");
                    ses.putSessionObject("sessInventoryItemId", "");
                    ses.putSessionObject("sessXXWC.DESCRPTION", "");
                    ses.putSessionObject("sessXXWC.SUBINVENTORY", "");
                }
            } 
        catch (Exception e) {
            UtilFns.error("Error in calling exitedDone " + e);
        }
    }
    
    
    /*************************************************************************
    *   NAME:     public void createLocator(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                                 InterruptedHandlerException,
                                                                                 DefaultOnlyHandlerException
    *
    *   PURPOSE:  Execute the procedure XXWC_INV_PRIMARY_BIN_PKG.PROCESS_ITEM_LOCATOR_TIE
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
         1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                              TMS Ticket 20150302-00164 - RF - Bin maintenance form   
    **************************************************************************/
    
    public void createLocator(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                             InterruptedHandlerException,
                                                                             DefaultOnlyHandlerException {
        String gMethod = "createLocator";
        String gCallPoint = "Start";
        CallableStatement cstmt = null;
        Connection con = ses.getConnection();
        String p_organization_id = (String)ses.getObject("ORGID");
        String p_subinventory_code = (String)pg.getmSubFld().getValue();
        String p_locator = (String)pg.getmLocFld().getValue();
        Integer x_return;
        String x_message;
        try {
            //                                                                            1 2 3 4 5
            cstmt = con.prepareCall("{call XXWC_INV_BIN_MAINTENANCE_PKG.CREATE_WC_LOCATOR(?,?,?,?,?)}");
            cstmt.setString(1, p_organization_id);
            cstmt.setString(2, p_subinventory_code);
            cstmt.setString(3, p_locator);
            cstmt.registerOutParameter(4, Types.NUMERIC);
            cstmt.registerOutParameter(5, Types.VARCHAR);
            cstmt.execute();
            x_return = cstmt.getInt(4);
            x_message = cstmt.getString(5);
            cstmt.close();
            if (x_return == 0) {
                ses.setStatusMessage((new StringBuilder()).append(pg.WMS_TXN_SUCCESS).append(" (").append(pg.getmSubFld().getValue() + " | " + pg.getmLocFld().getValue() + " " + x_message).append(")").toString());
                //The locator exists now get the next prefix
                gCallPoint = "x_return is > 0";
                String p_inventory_item_id = "";
                p_inventory_item_id = pg.getmItemFld().getmInventoryItemId();
                String x_next_bin = "0";
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint);
                con = ses.getConnection();
                cstmt = con.prepareCall("{? = call XXWC_INV_BIN_MAINTENANCE_PKG.NEXT_BIN_PREFIX(?,?)}");
                cstmt.registerOutParameter(1, Types.VARCHAR);
                cstmt.setString(2, p_organization_id);
                cstmt.setString(3, p_inventory_item_id);
                cstmt.execute();
                x_next_bin = cstmt.getString(1);
                cstmt.close();
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " x_next_bin " + x_next_bin);
                if (x_next_bin.equals(null) || x_next_bin.equals("")){
                    FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " next bin is null");
                    pg.getmLocFldPreFix().setValue("");
                }
                else {
                    FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " next bin is not null");
                    pg.getmLocFldPreFix().setValue(x_next_bin);
                }
            }
            if (x_return != 0) {
                TelnetSession telnetsessionX = (TelnetSession)ses;
                int k = telnetsessionX.showPromptPage("Error!", x_message, dialogPageButtons);
                if (k == 0) {
                    ses.setRefreshScreen(true);
                } else {
                    return;
                }
            }
        } catch (Exception e) {
            ses.setStatusMessage((new StringBuilder()).append(pg.WMS_TXN_ERROR).append(" (").append(e).append(")").toString());
            UtilFns.error((new StringBuilder()).append("Error in createLocator: error - ").append(e).toString());
        }
    }
   
}
