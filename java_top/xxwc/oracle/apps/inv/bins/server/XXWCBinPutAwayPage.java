/*************************************************************************
   *   $Header XXWCBinPutAwayPage.java $
   *   Module Name: XXWCBinPutAwayPage
   *   
   *   Package: package xxwc.oracle.apps.inv.bins.server;
   *   
   *   Imports Classes:
   *   
   *   import java.sql.*;
   *   import oracle.apps.inv.utilities.server.UtilFns;
   *   import oracle.apps.mwa.beans.*;
   *   import oracle.apps.mwa.container.MWALib;
   *   import oracle.apps.mwa.container.Session;
   *   import oracle.apps.mwa.eventmodel.*;
   *   import oracle.apps.mwa.container.FileLogger;
   *   import oracle.apps.fnd.flexj.FlexException;
   *   import oracle.apps.inv.lov.server.ItemLOV;
   *   import oracle.apps.inv.lov.server.LocatorKFF;
   *   import oracle.apps.inv.lov.server.SubinventoryLOV;
   *   import xxwc.oracle.apps.inv.lov.server.*;
   *
   *   PURPOSE:   Java Class for XXWCBinPutAwayPage Page Layout.
   *    
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
        1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                          TMS Ticket 20150302-00158 - RF - Put-Away
        1.1       01-JUN-2015   Lee Spitzer             TMS Ticket 20150604-00154 RF Bundle 1
                                                          Add Delete Button
        1.2       29-MAR-2016   Lee Spitzer             TMS Ticket Number 20151103-00182 - RF - UI Improvements to reduce number of clicks
**************************************************************************/

package xxwc.oracle.apps.inv.bins.server;

import java.sql.SQLException;

import oracle.apps.mwa.beans.*;
import oracle.apps.mwa.container.MWALib;
import oracle.apps.mwa.container.Session;
import oracle.apps.mwa.eventmodel.*;
import oracle.apps.mwa.container.FileLogger;
import oracle.apps.fnd.flexj.FlexException;
import oracle.apps.fnd.flexj.KeyFlexfield;
import oracle.apps.inv.invinq.server.ItemOnhandIterator;
import oracle.apps.inv.utilities.server.*;
import oracle.apps.inv.lov.server.ItemLOV;
import oracle.apps.inv.lov.server.LocatorKFF;
import oracle.apps.inv.lov.server.SubinventoryLOV;
import xxwc.oracle.apps.inv.lov.server.*;

/*************************************************************************
 *   NAME: XXWCBinPutAwayPage extends PageBean
 *
 *   PURPOSE:   Main class for XXWCBinPutAwayPage
 *
 *   REVISIONS:
 *   Ver        Date        Author                     Description
 *   ---------  ----------  ---------------         -------------------------
        1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                          TMS Ticket 20150302-00158 - RF - Put-Away
        1.1       01-JUN-2015   Lee Spitzer             TMS Ticket 20150604-00154 RF Bundle 1
                                                           Add Delete Button
 **************************************************************************/


public class XXWCBinPutAwayPage extends PageBean{
    /*Define objects in the class*/
    public static String WMS_TXN_SUCCESS = "Txn Successful";
    public static String WMS_TXN_ERROR = "Txn Failed. Please check log";
    public static String WMS_TXN_CANCEL = "Txn Cancelled";
    public static String WMS_LOT_INS_FAIL = "Lot Insert Failed";
    public static String WMS_SER_INS_FAIL = "Serial Insert Failed";
    public static String savenextPrompt = "";
    public static String donePrompt = "";
    public static String cancelPrompt = "";
    private boolean areThePromptsSet;
    protected XXWCItemLOV mItemFld;
    TextFieldBean mItemDescFld;
    protected XXWCBinsLOV mBinFld;
    protected ButtonFieldBean mPrintItemLabel;
    protected ButtonFieldBean mDone;
    protected ButtonFieldBean mCancel;
    protected ButtonFieldBean mDelete; //Added TMS Ticket 20150604-00154 01-JUN-2015
    protected String m_userid;

    /*************************************************************************
     *   NAME: private static String gPackage = "xxwc.oracle.apps.inv.bins.server";
     *
     *   PURPOSE:   private method to default gPackage value for debugging
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00158 - RF - Put-Away
     **************************************************************************/

    private static String gPackage = "xxwc.oracle.apps.inv.bins.server.XXWCBinPutAwayPage";


    /*************************************************************************
     *   NAME: private static String gCallFrom = "XXWCBinPutAwayPage";
     *
     *   PURPOSE:   private method to pass gCallFrom from methods to return class name CustomListener
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00158 - RF - Put-Away
     **************************************************************************/

    private static String gCallFrom = "XXWCBinPutAwayPage";



    /*************************************************************************
     *   NAME: public XXWCBinPutAwayPage(Session session)
     *
     *   PURPOSE:   Inherits session from XXWCPrimaryBinAssignmentPage
     *
     *   Parameters: Session session
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                               TMS Ticket 20150302-00158 - RF - Put-Away
     **************************************************************************/

    public XXWCBinPutAwayPage(Session session) {
        initLayout(session);
    }

    
    /*************************************************************************
     *   NAME: private void initLayout(Session session)
     *
     *   PURPOSE:   Sets the initial layout of the page
     *
     *   Parameters: Session session
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
          1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                             TMS Ticket 20150302-00158 - RF - Put-Away
          1.1       01-JUN-2015   Lee Spitzer             TMS Ticket 20150604-00154 RF Bundle 1
                                                             Add Delete Button
          1.2       29-MAR-2016   Lee Spitzer             TMS Ticket Number 20151103-00182 - RF - UI Improvements to reduce number of clicks
     **************************************************************************/

    private void initLayout(Session session) {
        m_userid = "";
        WMS_TXN_SUCCESS = UtilFns.getMessage(session, "WMS", "WMS_TXN_SUCCESS");
        WMS_TXN_ERROR = UtilFns.getMessage(session, "WMS", "WMS_TXN_ERROR");
        WMS_TXN_CANCEL = UtilFns.getMessage(session, "WMS", "WMS_TXN_CANCEL");
        WMS_LOT_INS_FAIL = UtilFns.getMessage(session, "WMS", "WMS_LOT_INS_FAIL");
        WMS_SER_INS_FAIL = UtilFns.getMessage(session, "WMS", "WMS_SER_INS_FAIL");
        /*Item Field*/
        // mItemFld = new LOVFieldBean();
        mItemFld = new XXWCItemLOV("ALL");
        mItemFld.setName("INV.ITEM");
        mItemFld.setRequired(true);
        /*Item Description*/
        mItemDescFld = new TextFieldBean();
        mItemDescFld.setName("INV.DESC");
        mItemDescFld.setPrompt("Item Desc");
        mItemDescFld.setEditable(false);
        //Bins//    
        mBinFld = new XXWCBinsLOV();
        mBinFld.setName("XXWC.BIN");
        mBinFld.setRequired(true);
        mBinFld.setHidden(true);
        //Print Labels//   
        mPrintItemLabel = new ButtonFieldBean();
        mPrintItemLabel.setName("XXWC.PRINT_ITEM_LABEL");
        mPrintItemLabel.setHidden(true);
        /*Save Properties*/
        mDone = new ButtonFieldBean();
        mDone.setName("INV.DONE");
        mDone.setPrompt("Assign");
        mDone.setEnableAcceleratorKey(true);
        mDone.setHidden(true);
        /*Menu Properties*/
        mCancel = new ButtonFieldBean();
        mCancel.setName("XXWC.CANCEL");
        mCancel.setPrompt("Cancel");
        mCancel.setEnableAcceleratorKey(true);
        mCancel.setHidden(false);
        /*Delete properties added 01-JUN-2015*/
        mDelete = new ButtonFieldBean();
        mDelete.setName("XXWC.DELETE");
        //mDelete.setPrompt("Delete"); Removed 04/13/2016 TMS Ticket Number 20151103-00182
        mDelete.setPrompt("Delete Bin"); //Added 04/13/2016 TMS Ticket Number 20151103-00182
        mDelete.setEnableAcceleratorKey(true);
        mDelete.setHidden(true);
        /*set the properties for each to the addFieldBean*/
        addFieldBean(mItemFld);
        addFieldBean(mItemDescFld);
        addFieldBean(mBinFld);
        addFieldBean(mDone);
        addFieldBean(mDelete); //Added 01-JUN-2015
        addFieldBean(mPrintItemLabel);
        addFieldBean(mCancel);
        /*Point Page to the XXWCPrimaryBinAssignmentFListener Listener*/
        XXWCBinPutAwayFListener fieldListener = new XXWCBinPutAwayFListener();
        /*Add Listener to each property*/
        mItemFld.addListener(fieldListener);
        mItemDescFld.addListener(fieldListener);
        mBinFld.addListener(fieldListener);
        mDone.addListener(fieldListener);
        mDelete.addListener(fieldListener); //Added 01-JUN-2015
        mPrintItemLabel.addListener(fieldListener);
        mCancel.addListener(fieldListener);
        try {
            setPrompts(session);
        } catch (Exception exception) {
            session.setStatusMessage(UtilFns.getMessage(session, "WMS", "WMS_PROMPT_NOTSET"));
        }
    }
    
    /*************************************************************************
     *   NAME: private void initPrompts(Session session) throws SQLException
     *
     *   PURPOSE:   Sets the initial prompts of the session
     *
     *   Parameters: Session session
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
          1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                             TMS Ticket 20150302-00158 - RF - Put-Away
          1.1       01-JUN-2015   Lee Spitzer             TMS Ticket 20150604-00154 RF Bundle 1
                                                             Add Delete Button
          1.2       29-MAR-2016   Lee Spitzer             TMS Ticket Number 20151103-00182 - RF - UI Improvements to reduce number of clicks
     **************************************************************************/

    private void initPrompts(Session session) throws SQLException {
        try {
            String gMethod = "initPrompts";
            String gCallPoint = "Start";
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod  + " " + gCallPoint);
            String s = (String)session.getObject("ORGCODE");
            setPrompt((new StringBuilder()).append("XXWC Bin Putaway (").append(s).append(")").toString());

            mItemFld.setPrompt(MWALib.getAKPrompt(session, "oracle.apps.inv.utilities.InvResourceTable",
                                                  "INV_ITEM_PROMPT"));
            mItemFld.retrieveAttributes("INV_ITEM_PROMPT");
            mItemDescFld.setPrompt(MWALib.getAKPrompt(session, "oracle.apps.inv.utilities.InvResourceTable",
                                                      "INV_DESCRIPTION_PROMPT"));
            mItemDescFld.retrieveAttributes("INV_DESCRIPTION_PROMPT");
            mBinFld.setPrompt(MWALib.getAKPrompt(session, "oracle.apps.inv.utilities.InvResourceTable",
                                                 "INV_LOCATOR_PROMPT"));
            mBinFld.retrieveAttributes("INV_LOCATOR_PROMPT");
            //mLocFldPreFix.setPrompt("Loc PreFix");
            savenextPrompt = MWALib.getAKPrompt(session, "oracle.apps.inv.utilities.InvResourceTable", "INV_SAVE_NEXT_PROMPT");
            cancelPrompt = MWALib.getAKPrompt(session, "oracle.apps.inv.utilities.InvResourceTable", "INV_CANCEL_PROMPT");
            donePrompt = MWALib.getAKPrompt(session, "oracle.apps.inv.utilities.InvResourceTable", "INV_DONE_PROMPT");
            //mDelete.setPrompt("Delete"); //Added TMS Ticket 20150604-00154 01-JUN-2015 //Removed 04/13/2016 TMS Ticket Number 20151103-0018
            mDelete.setPrompt("Delete Bin"); //Added 04/13/2016 TMS Ticket Number 20151103-00182
            mPrintItemLabel.setPrompt("Item Label");
            areThePromptsSet = true;
            gCallPoint = "End";
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod  + " " + gCallPoint);
        } catch (SQLException sqlexception) {
            session.setStatusMessage(UtilFns.getMessage(session, "WMS", "WMS_TXN_ERROR"));
        }
    }

    /*************************************************************************
     *   NAME: private void setPrompts(Session session)
     *
     *   PURPOSE:   Sets the set prompts of the session
     *
     *   Parameters: Session session
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
          1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                             TMS Ticket 20150302-00158 - RF - Put-Away
          1.1       01-JUN-2015   Lee Spitzer             TMS Ticket 20150604-00154 RF Bundle 1
                                                             Add Delete Button
          1.2       29-MAR-2016   Lee Spitzer             TMS Ticket Number 20151103-00182 - RF - UI Improvements to reduce number of clicks
     **************************************************************************/

    private void setPrompts(Session session) {
            String gMethod = "setPrompts";
            String gCallPoint = "Start";
            if (!areThePromptsSet) {
                try {
                    initPrompts(session);
                } catch (SQLException sqlexception) {
                }
            }
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod  + " " + gCallPoint);
            String s = (String)session.getObject("ORGCODE");
            setPrompt((new StringBuilder()).append("XXWC Bin Putaway (").append(s).append(")").toString());

            //mNextBin.setPrompt("Next Bin Loc");
            //mNextItem.setPrompt("Next Item");
            
            mCancel.setPrompt(cancelPrompt);
            mDone.setPrompt(cancelPrompt);
            mDone.retrieveAttributes("INV_DONE_PROMPT");
            mCancel.retrieveAttributes("INV_CANCEL_PROMPT");
            mDone.setPrompt("Assign");
            //mDelete.setPrompt("Delete"); //Added TMS Ticket 20150604-00154 01-JUN-2015 //Removed 04/13/2016 TMS Ticket Number 20151103-0018
            mDelete.setPrompt("Delete Bin"); //Added 04/13/2016 TMS Ticket Number 20151103-00182
            mPrintItemLabel.setPrompt("Item Label");
            //mCancel.setPrompt("Menu"); Removed 03/29/2016 TMS Ticket 20151103-00182
            //mNextBin.retrieveAttributes("INVE_SAVE_NEXT_PROMPT");
            //mNextItem.retrieveAttributes("INV_CANCEL_PROMPT");
            gCallPoint = "End Prompts";
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod  + " " + gCallPoint);
    }


    /*************************************************************************
     *   NAME: public XXWCItemLOV  getmItemFld()
     *
     *   PURPOSE:   return the value of mItemFld
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00158 - RF - Put-Away
      **************************************************************************/

    public XXWCItemLOV getmItemFld()
        //public LOVFieldBean getmItemFld()
    {
        return mItemFld;
    }

    /*************************************************************************
     *   NAME: public TextFieldBean getmItemDescFld()
     *
     *   PURPOSE:   return the value of mItemDescFld
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00158 - RF - Put-Away
     **************************************************************************/

    public TextFieldBean getmItemDescFld() {
        return mItemDescFld;
    }


    /*************************************************************************
     *   NAME: public XXWCBinsLOV getmLocFld()
     *
     *   PURPOSE:   return the value of mLocFld
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket 20150302-00158 - RF - Put-Away
     **************************************************************************/

    public XXWCBinsLOV getmBinFld() {
        return mBinFld;
    }


    /*************************************************************************
     *   NAME: public ButtonFieldBean getmDelete()
     *
     *   PURPOSE:   return the value of mDelete
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00158 - RF - Put-Away
             1.1       01-JUN-2015   Lee Spitzer             TMS Ticket 20150604-00154 RF Bundle 1
                                                                Add Delete Button
     **************************************************************************/
    /*Added TMS Ticket 20150604-00154 - 01-JUN-2015 */
    public ButtonFieldBean getmDelete() {
        return mDelete;
    }


    /*************************************************************************
     *   NAME: public ButtonFieldBean getmDone()
     *
     *   PURPOSE:   return the value of mDone
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00158 - RF - Put-Away
     **************************************************************************/

    public ButtonFieldBean getmDone() {
        return mDone;
    }

    /*************************************************************************
     *   NAME: public ButtonFieldBean getmPrintItemLabel()
     *
     *   PURPOSE:   return the value of mDone
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00158 - RF - Put-Away
     **************************************************************************/

    public ButtonFieldBean getmPrintItemLabel() {
        return mPrintItemLabel;
    }

    /*************************************************************************
     *   NAME: public ButtonFieldBean getmCancel()
     *
     *   PURPOSE:   return the value of mCancel
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00164 - RF - Bin maintenance form   
     **************************************************************************/

        public ButtonFieldBean getmCancel()
        {
            return mCancel;
        }

    /*************************************************************************
     *   NAME: public String getUserId()
     *
     *   PURPOSE:   return the value of m_userid
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00158 - RF - Put-Away
     **************************************************************************/

    public String getUserId() {
        return m_userid;
    }

    /*************************************************************************
     *   NAME: void pageEntered(MWAEvent e) throws AbortHandlerException, InterruptedHandlerException,
                                               DefaultOnlyHandlerException {
     *
     *   PURPOSE:   pageEntered method
     *
     *   Parameters: MWAEvent mwaevent
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00158 - RF - Put-Away
     **************************************************************************/

    public void pageEntered(MWAEvent e) throws AbortHandlerException, InterruptedHandlerException,
                                               DefaultOnlyHandlerException {
        Session session = e.getSession();
    }

    /*************************************************************************
     *   NAME: void pageExited(MWAEvent e) throws AbortHandlerException, InterruptedHandlerException,
                                               DefaultOnlyHandlerException {
     *
     *   PURPOSE:   pageExited method
     *
     *
     *   Parameters: MWAEvent   mwaevent
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00158 - RF - Put-Away

     **************************************************************************/

    public void pageExited(MWAEvent e) throws AbortHandlerException, InterruptedHandlerException,
                                              DefaultOnlyHandlerException {
        Session session = e.getSession();
    }
   
}
