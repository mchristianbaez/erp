/*************************************************************************
   *   $Header SupplierSiteNameValidator.java $
   *   Module Name: SupplierSiteNameValidator
   *
   *   PURPOSE:   Java Class used in Web ADI Download to validate supplier sites
   *    
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
        1.0       17-AUG-2014   Lee Spitzer             Initial Version - 20140726-00008 8/13/2014 
                                                            Update Vendor min Web ADI to download/ update default contact @SC Update Vendor min Web ADI to download/ update default contact
**************************************************************************/
package xxwc.oracle.apps.bne.lov.suppliersites.validator;

import java.util.Hashtable;

import oracle.apps.bne.exception.BneException;
import oracle.apps.bne.exception.BneFatalException;
import oracle.apps.bne.exception.BneMissingParameterException;
import oracle.apps.bne.framework.BneWebAppsContext;
import oracle.apps.bne.integrator.validators.BneUploadValidator;
import oracle.apps.bne.utilities.sql.BneCompositeSQLCriteria;
import oracle.apps.bne.utilities.sql.BneResultSet;
import oracle.apps.bne.utilities.sql.BneSQLStatement;

import xxwc.oracle.apps.bne.lov.suppliersites.sql.SupplierSiteNameSQL;

public class SupplierSiteNameValidator extends BneUploadValidator {

    public String[] getDomainParameters() {
        return new String[] { "I_VENDOR_ID" };
    }

    public BneResultSet getDomainValues(BneWebAppsContext paramBneWebAppsContext, 
                                        Hashtable paramHashtable, 
                                        BneCompositeSQLCriteria paramBneCompositeSQLCriteria) throws BneException {

        SupplierSiteNameSQL SupplierSiteNameSQL = null;
        BneResultSet bneResultSet = null;
        BneSQLStatement bneSQLStatement1 = new BneSQLStatement();

        if (paramBneCompositeSQLCriteria != null) {
            bneSQLStatement1 = 
                    paramBneCompositeSQLCriteria.evaluate(bneSQLStatement1);
        }

        String str1 = (String)paramHashtable.get("I_VENDOR_ID");

        if (str1 == null) {
            throw new BneMissingParameterException("Supplier Field Error");
        }


        try {
            SupplierSiteNameSQL = 
                    new SupplierSiteNameSQL(paramBneWebAppsContext, 
                                                 bneSQLStatement1.getStatement());
            BneSQLStatement bneSQLStatement2 = 
                new BneSQLStatement(SupplierSiteNameSQL.getQuery(), 
                                    new Object[] { str1 });

            bneSQLStatement2.append("", bneSQLStatement1.getBindValues());
            bneResultSet = 
                    SupplierSiteNameSQL.getBneResultSet(bneSQLStatement2.getBindValuesAsArray());
        } catch (Exception exception) {
            throw new BneFatalException(exception.toString());
        }


        return bneResultSet;
    }

}
