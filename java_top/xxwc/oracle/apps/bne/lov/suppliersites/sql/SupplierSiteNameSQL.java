/*************************************************************************
   *   $Header SupplierSiteNameSQL.java $
   *   Module Name: SupplierSiteNameSQL
   *
   *   PURPOSE:   Java Class used in Web ADI Download to validate supplier sites
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
        1.0       17-AUG-2014   Lee Spitzer             Initial Version - 20140726-00008 8/13/2014 
                                                            Update Vendor min Web ADI to download/ update default contact @SC Update Vendor min Web ADI to download/ update default contact
**************************************************************************/
package xxwc.oracle.apps.bne.lov.suppliersites.sql;

import java.sql.Connection;
import java.sql.SQLException;

import oracle.apps.bne.exception.BneException;
import oracle.apps.bne.framework.BneWebAppsContext;
import oracle.apps.bne.utilities.sql.BneBaseSQL;

public class SupplierSiteNameSQL extends BneBaseSQL {
    public SupplierSiteNameSQL(BneWebAppsContext paramBneWebAppsContext, String paramString) throws SQLException,
                                                                                                    BneException {
        Connection connection = paramBneWebAppsContext.getJDBCConnection();
        StringBuffer stringBuffer = new StringBuffer();

        stringBuffer.append("SELECT ss.vendor_site_id, ss.vendor_site_code FROM ap_suppliers s,ap_supplier_sites_all ss WHERE ss.vendor_id = s.vendor_id and s.segment1= :1");

        if ((paramString != null) && (!paramString.trim().equals(""))) {
            stringBuffer.append("AND " + paramString);
        }

        //stringBuffer.append(" ORDER BY VENDOR_SITE_CODE, ADDRESS ");

        setQuery(connection, stringBuffer.toString());
    }
}
