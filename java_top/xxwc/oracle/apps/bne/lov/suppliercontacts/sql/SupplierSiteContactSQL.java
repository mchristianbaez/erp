/*************************************************************************
   *   $Header SupplierSiteContactSQL.java $
   *   Module Name: SupplierSiteContactSQL
   *
   *   PURPOSE:   Java Class used in Web ADI Download to validate supplier sites
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
        1.0       17-AUG-2014   Lee Spitzer             Initial Version - 20140726-00008 8/13/2014 
                                                            Update Vendor min Web ADI to download/ update default contact @SC Update Vendor min Web ADI to download/ update default contact
**************************************************************************/
package xxwc.oracle.apps.bne.lov.suppliercontacts.sql;

import java.sql.Connection;
import java.sql.SQLException;

import oracle.apps.bne.exception.BneException;
import oracle.apps.bne.framework.BneWebAppsContext;
import oracle.apps.bne.utilities.sql.BneBaseSQL;

public class SupplierSiteContactSQL extends BneBaseSQL {
    public SupplierSiteContactSQL(BneWebAppsContext paramBneWebAppsContext, String paramString) throws SQLException,
                                                                                                    BneException {
        Connection connection = paramBneWebAppsContext.getJDBCConnection();
        StringBuffer stringBuffer = new StringBuffer();

        stringBuffer.append("SELECT pvc.vendor_contact_id, pvc.last_name FROM ap_suppliers s,ap_supplier_sites_all ss, po_vendor_contacts pvc WHERE ss.vendor_id = s.vendor_id and s.segment1= :1 and ss.vendor_site_code = :2 and ss.vendor_site_id = pvc.vendor_site_id and ss.vendor_id = pvc.vendor_id order by pvc.last_name");

        if ((paramString != null) && (!paramString.trim().equals(""))) {
            stringBuffer.append("AND " + paramString);
        }

        //stringBuffer.append(" ORDER BY VENDOR_SITE_CODE, ADDRESS ");

        setQuery(connection, stringBuffer.toString());
    }
}
