/*===========================================================================================================================+
 |   Company: HD SUPPLY                                                                                                      |
 |   All rights reserved.                                                                                                    |
 |===========================================================================================================================+
 |  HISTORY                                                                                                                  |
 |  =======                                                                                                                  |
 |                                                                                                                           |
 |  TICKET#              Date         Author                   Comments                                                      |
 |  =======              ==========   ======================== ============================================================= |
 |  TMS 20151201-00153   16-FEB-2016  Pankaj Yadav (L&T)       OPN SuperSearch fixes                                         |
 |                                                                                                                           | 
 +===========================================================================================================================*/
package xxcus.oracle.apps.pn.leaseinquiry.server;

import oracle.apps.fnd.framework.server.OAViewRowImpl;
import oracle.jbo.domain.Date;
import oracle.jbo.domain.Number;
import oracle.jbo.server.AttributeDefImpl;

public class LeaseFinancialVORowImpl
  extends OAViewRowImpl {
    public static final int PAYMENTPURPOSE = 0;
    public static final int PAYMENTTERMTYPE = 1;
    public static final int FREQUENCYTYPE = 2;
    public static final int VENDORNAME = 3;
    public static final int VENDORNUMBER = 4;
    public static final int VENDORSITE = 5;
    public static final int LEASEID = 6;
    public static final int STARTDATE = 7;
    public static final int ENDDATE = 8;
    public static final int LOCATION = 9;
    public static final int ACTUALAMOUNT = 10;
    public static final int CURRENCYCODE = 11;
    public static final int ESTIMATEDAMOUNT = 12;
    public static final int TERMCOMMENTS = 13;
    public static final int FIXEDVAR = 14;
    public static final int PAYSTAX = 15;

    /**This is the default constructor (do not remove)
     */
    public LeaseFinancialVORowImpl() {
    }

    public String getPaymentPurpose()
  {
    return (String)getAttributeInternal(0);
  }
  
  public void setPaymentPurpose(String value)
  {
    setAttributeInternal(0, value);
  }
  
  public String getPaymentTermType()
  {
    return (String)getAttributeInternal(1);
  }
  
  public void setPaymentTermType(String value)
  {
    setAttributeInternal(1, value);
  }
  
  public String getFrequencyType()
  {
    return (String)getAttributeInternal(2);
  }
  
  public void setFrequencyType(String value)
  {
    setAttributeInternal(2, value);
  }
  
  public String getVendorName()
  {
    return (String)getAttributeInternal(3);
  }
  
  public void setVendorName(String value)
  {
    setAttributeInternal(3, value);
  }
  
  public String getVendorNumber()
  {
    return (String)getAttributeInternal(4);
  }
  
  public void setVendorNumber(String value)
  {
    setAttributeInternal(4, value);
  }
  
  public String getVendorSite()
  {
    return (String)getAttributeInternal(5);
  }
  
  public void setVendorSite(String value)
  {
    setAttributeInternal(5, value);
  }
  
  public Number getLeaseId()
  {
    return (Number)getAttributeInternal(6);
  }
  
  public void setLeaseId(Number value)
  {
    setAttributeInternal(6, value);
  }
  
  public Date getStartDate()
  {
    return (Date)getAttributeInternal(7);
  }
  
  public void setStartDate(Date value)
  {
    setAttributeInternal(7, value);
  }
  
  public Date getEndDate()
  {
    return (Date)getAttributeInternal(8);
  }
  
  public void setEndDate(Date value)
  {
    setAttributeInternal(8, value);
  }
  
  public String getLocation()
  {
    return (String)getAttributeInternal(9);
  }
  
  public void setLocation(String value)
  {
    setAttributeInternal(9, value);
  }
  
  protected Object getAttrInvokeAccessor(int index, AttributeDefImpl attrDef)
    throws Exception
  {
        switch (index) {
        case PAYMENTPURPOSE:
            return getPaymentPurpose();
        case PAYMENTTERMTYPE:
            return getPaymentTermType();
        case FREQUENCYTYPE:
            return getFrequencyType();
        case VENDORNAME:
            return getVendorName();
        case VENDORNUMBER:
            return getVendorNumber();
        case VENDORSITE:
            return getVendorSite();
        case LEASEID:
            return getLeaseId();
        case STARTDATE:
            return getStartDate();
        case ENDDATE:
            return getEndDate();
        case LOCATION:
            return getLocation();
        case ACTUALAMOUNT:
            return getActualAmount();
        case CURRENCYCODE:
            return getCurrencyCode();
        case ESTIMATEDAMOUNT:
            return getEstimatedAmount();
        case TERMCOMMENTS:
            return getTermComments();
        case FIXEDVAR:
            return getFixedVar();
        case PAYSTAX:
            return getPaysTax();
        default:
            return super.getAttrInvokeAccessor(index, attrDef);
        }
    }
  
  protected void setAttrInvokeAccessor(int index, Object value, AttributeDefImpl attrDef)
    throws Exception
  {
        switch (index) {
        case PAYMENTPURPOSE:
            setPaymentPurpose((String)value);
            return;
        case PAYMENTTERMTYPE:
            setPaymentTermType((String)value);
            return;
        case FREQUENCYTYPE:
            setFrequencyType((String)value);
            return;
        case VENDORNAME:
            setVendorName((String)value);
            return;
        case VENDORNUMBER:
            setVendorNumber((String)value);
            return;
        case VENDORSITE:
            setVendorSite((String)value);
            return;
        case LEASEID:
            setLeaseId((Number)value);
            return;
        case STARTDATE:
            setStartDate((Date)value);
            return;
        case ENDDATE:
            setEndDate((Date)value);
            return;
        case LOCATION:
            setLocation((String)value);
            return;
        case ACTUALAMOUNT:
            setActualAmount((String)value);
            return;
        case CURRENCYCODE:
            setCurrencyCode((String)value);
            return;
        case ESTIMATEDAMOUNT:
            setEstimatedAmount((String)value);
            return;
        case TERMCOMMENTS:
            setTermComments((String)value);
            return;
        case FIXEDVAR:
            setFixedVar((String)value);
            return;
        case PAYSTAX:
            setPaysTax((String)value);
            return;
        default:
            super.setAttrInvokeAccessor(index, value, attrDef);
            return;
        }
    }

    /**Gets the attribute value for the calculated attribute ActualAmount
     */
    public String getActualAmount() {
        return (String) getAttributeInternal(ACTUALAMOUNT);
    }

    /**Sets <code>value</code> as the attribute value for the calculated attribute ActualAmount
     */
    public void setActualAmount(String value) {
        setAttributeInternal(ACTUALAMOUNT, value);
    }

    /**Gets the attribute value for the calculated attribute CurrencyCode
     */
    public String getCurrencyCode() {
        return (String) getAttributeInternal(CURRENCYCODE);
    }

    /**Sets <code>value</code> as the attribute value for the calculated attribute CurrencyCode
     */
    public void setCurrencyCode(String value) {
        setAttributeInternal(CURRENCYCODE, value);
    }

    /**Gets the attribute value for the calculated attribute EstimatedAmount
     */
    public String getEstimatedAmount() {
        return (String) getAttributeInternal(ESTIMATEDAMOUNT);
    }

    /**Sets <code>value</code> as the attribute value for the calculated attribute EstimatedAmount
     */
    public void setEstimatedAmount(String value) {
        setAttributeInternal(ESTIMATEDAMOUNT, value);
    }

    /**Gets the attribute value for the calculated attribute TermComments
     */
    public String getTermComments() {
        return (String) getAttributeInternal(TERMCOMMENTS);
    }

    /**Sets <code>value</code> as the attribute value for the calculated attribute TermComments
     */
    public void setTermComments(String value) {
        setAttributeInternal(TERMCOMMENTS, value);
    }

    /**Gets the attribute value for the calculated attribute FixedVar
     */
    public String getFixedVar() {
        return (String) getAttributeInternal(FIXEDVAR);
    }

    /**Sets <code>value</code> as the attribute value for the calculated attribute FixedVar
     */
    public void setFixedVar(String value) {
        setAttributeInternal(FIXEDVAR, value);
    }

    /**Gets the attribute value for the calculated attribute PaysTax
     */
    public String getPaysTax() {
        return (String) getAttributeInternal(PAYSTAX);
    }

    /**Sets <code>value</code> as the attribute value for the calculated attribute PaysTax
     */
    public void setPaysTax(String value) {
        setAttributeInternal(PAYSTAX, value);
    }
}
