package xxcus.oracle.apps.pn.leaseinquiry;

import oracle.jbo.ApplicationModule;

public interface LeaseInquiryAM
    extends ApplicationModule
{

    public abstract void initLobRollups();

    public abstract void initLeaseSummary(String s);
}
