package xxcus.oracle.apps.pn.leaseinquiry;

import oracle.apps.fnd.common.VersionInfo;
import oracle.apps.fnd.framework.OAApplicationModule;
import oracle.apps.fnd.framework.webui.*;
import oracle.apps.fnd.framework.webui.beans.OAImageBean;
import oracle.apps.fnd.framework.webui.beans.OAWebBean;
import oracle.apps.fnd.framework.webui.beans.table.OATableBean;
import oracle.cabo.ui.UIConstants;

public class LeaseInquiryCO extends OAControllerImpl
{

    public LeaseInquiryCO()
    {
    }

    public void processRequest(OAPageContext pageContext, OAWebBean webBean)
    {
        super.processRequest(pageContext, webBean);
        OAApplicationModule am = pageContext.getApplicationModule(webBean);
        am.invokeMethod("initLobRollups");
        OATableBean tableBean = (OATableBean)webBean.findChildRecursive("tableLeaseSummary");
        OAImageBean m = (OAImageBean)tableBean.findChildRecursive("RerDocs");
        OAImageBean n = (OAImageBean)tableBean.findChildRecursive("LocDocs");
        OADataBoundValueViewObject tip1 = new OADataBoundValueViewObject(m, "RerDocs");
        OADataBoundValueViewObject tip2 = new OADataBoundValueViewObject(n, "LocDocs");
        m.setAttributeValue(UIConstants.DESTINATION_ATTR, tip1);
        n.setAttributeValue(UIConstants.DESTINATION_ATTR, tip2);
    }

    public void processFormRequest(OAPageContext pageContext, OAWebBean webBean)
    {
        super.processFormRequest(pageContext, webBean);
    }

    public static final String RCS_ID = "$Header$";
    public static final boolean RCS_ID_RECORDED = VersionInfo.recordClassVersion("$Header$", "%packagename%");

}
