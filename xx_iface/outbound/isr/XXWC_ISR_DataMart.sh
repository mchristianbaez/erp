# ------------------------------------------------------------------------
# ------------------------------------------------------------------------
#!/bin/sh
# ------------------------------------------------------------------------
# copyright 2012, All Rights Reserved, 
# Version 1.0
#
# Name        : ISR Datamart.sh 
# Date Written: 
# Author      : Govardhan
#
# Description : send mail to user after ISR datamart completes
#
# Usasage     : ./XXWC_ISR_DataMart.sh
#
# Modification History:
#
# When         Who        Did what
# --------------------------------------------------------------------------
# 29-MAR-2016  Govardhan    Created
# --------------------------------------------------------------------------

dir=/xx_iface/ebsqa/outbound/isr/
echo 'Hi All,
	
          The ISR refresh has successfully completed this morning. Please run your ISR reports now.' | mutt -s ' ISR Refresh - Success' HDSWCOracleBatchSupport@hdsupply.com wc-itoperations-u1@hdsupply.com govardhan.reddy@hdsupply.com krishnakumar.govindarajulu@hdsupply.com sundaramoorthy.r@hdsupply.com
#mutt -s "Test Email" govardhan.reddy@hdsupply.com