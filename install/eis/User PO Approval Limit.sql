--Report Name            : User PO Approval Limit
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View EIS_PO_APPROVAL_LIMIT_V
set scan off define off
prompt Creating View XXEIS.EIS_PO_APPROVAL_LIMIT_V
Create or replace View XXEIS.EIS_PO_APPROVAL_LIMIT_V
(USER_NAME,FULL_NAME,EMAIL_ADDRESS,FAX,EMPLOYEE_NUMBER,IS_BUYER,BUYER_POSITION,BUYER_JOB,DOCUMENT_TYPE,GROUP_NAME,AMOUNT_LIMIT) AS 
SELECT xxeis_943_roaztl_v."USER_NAME",xxeis_943_roaztl_v."FULL_NAME",xxeis_943_roaztl_v."EMAIL_ADDRESS",xxeis_943_roaztl_v."FAX",xxeis_943_roaztl_v."EMPLOYEE_NUMBER",xxeis_943_roaztl_v."IS_BUYER",xxeis_943_roaztl_v."BUYER_POSITION",xxeis_943_roaztl_v."BUYER_JOB"

      ,document_type

      ,group_name

      ,amount_limit

  FROM apps.xxeis_943_roaztl_v

       LEFT OUTER JOIN eis_po_approval_assignment_v ON (buyer_position = position_name)

      INNER JOIN eis_po_approval_groups_v ON (approval_group_name = group_name)

WHERE object = 'Document Total' AND approval_group_enabled_flag = 'Y' AND group_enabled_flag = 'Y'/
set scan on define on
prompt Creating View Data for User PO Approval Limit
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_PO_APPROVAL_LIMIT_V
xxeis.eis_rs_ins.v( 'EIS_PO_APPROVAL_LIMIT_V',85000,'','','','','HT038687','XXEIS','Eis Po Approval Limit V','EPALV','','');
--Delete View Columns for EIS_PO_APPROVAL_LIMIT_V
xxeis.eis_rs_utility.delete_view_rows('EIS_PO_APPROVAL_LIMIT_V',85000,FALSE);
--Inserting View Columns for EIS_PO_APPROVAL_LIMIT_V
xxeis.eis_rs_ins.vc( 'EIS_PO_APPROVAL_LIMIT_V','AMOUNT_LIMIT',85000,'Amount Limit','AMOUNT_LIMIT','','','','HT038687','NUMBER','','','Amount Limit','','','');
xxeis.eis_rs_ins.vc( 'EIS_PO_APPROVAL_LIMIT_V','GROUP_NAME',85000,'Group Name','GROUP_NAME','','','','HT038687','VARCHAR2','','','Group Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_PO_APPROVAL_LIMIT_V','DOCUMENT_TYPE',85000,'Document Type','DOCUMENT_TYPE','','','','HT038687','VARCHAR2','','','Document Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_PO_APPROVAL_LIMIT_V','BUYER_JOB',85000,'Buyer Job','BUYER_JOB','','','','HT038687','VARCHAR2','','','Buyer Job','','','');
xxeis.eis_rs_ins.vc( 'EIS_PO_APPROVAL_LIMIT_V','BUYER_POSITION',85000,'Buyer Position','BUYER_POSITION','','','','HT038687','VARCHAR2','','','Buyer Position','','','');
xxeis.eis_rs_ins.vc( 'EIS_PO_APPROVAL_LIMIT_V','IS_BUYER',85000,'Is Buyer','IS_BUYER','','','','HT038687','VARCHAR2','','','Is Buyer','','','');
xxeis.eis_rs_ins.vc( 'EIS_PO_APPROVAL_LIMIT_V','EMPLOYEE_NUMBER',85000,'Employee Number','EMPLOYEE_NUMBER','','','','HT038687','VARCHAR2','','','Employee Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_PO_APPROVAL_LIMIT_V','FAX',85000,'Fax','FAX','','','','HT038687','VARCHAR2','','','Fax','','','');
xxeis.eis_rs_ins.vc( 'EIS_PO_APPROVAL_LIMIT_V','EMAIL_ADDRESS',85000,'Email Address','EMAIL_ADDRESS','','','','HT038687','VARCHAR2','','','Email Address','','','');
xxeis.eis_rs_ins.vc( 'EIS_PO_APPROVAL_LIMIT_V','FULL_NAME',85000,'Full Name','FULL_NAME','','','','HT038687','VARCHAR2','','','Full Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_PO_APPROVAL_LIMIT_V','USER_NAME',85000,'User Name','USER_NAME','','','','HT038687','VARCHAR2','','','User Name','','','');
--Inserting View Components for EIS_PO_APPROVAL_LIMIT_V
--Inserting View Component Joins for EIS_PO_APPROVAL_LIMIT_V
END;
/
set scan on define on
prompt Creating Report Data for User PO Approval Limit
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - User PO Approval Limit
xxeis.eis_rs_utility.delete_report_rows( 'User PO Approval Limit' );
--Inserting Report - User PO Approval Limit
xxeis.eis_rs_ins.r( 85000,'User PO Approval Limit','','Provide a way the support team may view the PO approval limit associated with Buyer Positions and individuals','','','','HT038687','EIS_PO_APPROVAL_LIMIT_V','Y','','','HT038687','','N','Audit Reports','','CSV,EXCEL,','N');
--Inserting Report Columns - User PO Approval Limit
xxeis.eis_rs_ins.rc( 'User PO Approval Limit',85000,'AMOUNT_LIMIT','Amount Limit','Amount Limit','','~~~','default','','11','N','','','','','','','','HT038687','N','N','','EIS_PO_APPROVAL_LIMIT_V','','');
xxeis.eis_rs_ins.rc( 'User PO Approval Limit',85000,'BUYER_JOB','Buyer Job','Buyer Job','','','default','','8','N','','','','','','','','HT038687','N','N','','EIS_PO_APPROVAL_LIMIT_V','','');
xxeis.eis_rs_ins.rc( 'User PO Approval Limit',85000,'BUYER_POSITION','Buyer Position','Buyer Position','','','default','','7','N','','','','','','','','HT038687','N','N','','EIS_PO_APPROVAL_LIMIT_V','','');
xxeis.eis_rs_ins.rc( 'User PO Approval Limit',85000,'DOCUMENT_TYPE','Document Type','Document Type','','','default','','9','N','','','','','','','','HT038687','N','N','','EIS_PO_APPROVAL_LIMIT_V','','');
xxeis.eis_rs_ins.rc( 'User PO Approval Limit',85000,'EMAIL_ADDRESS','Email Address','Email Address','','','default','','3','N','','','','','','','','HT038687','N','N','','EIS_PO_APPROVAL_LIMIT_V','','');
xxeis.eis_rs_ins.rc( 'User PO Approval Limit',85000,'EMPLOYEE_NUMBER','Employee Number','Employee Number','','','default','','5','N','','','','','','','','HT038687','N','N','','EIS_PO_APPROVAL_LIMIT_V','','');
xxeis.eis_rs_ins.rc( 'User PO Approval Limit',85000,'FAX','Fax','Fax','','','default','','4','N','','','','','','','','HT038687','N','N','','EIS_PO_APPROVAL_LIMIT_V','','');
xxeis.eis_rs_ins.rc( 'User PO Approval Limit',85000,'FULL_NAME','Full Name','Full Name','','','default','','2','N','','','','','','','','HT038687','N','N','','EIS_PO_APPROVAL_LIMIT_V','','');
xxeis.eis_rs_ins.rc( 'User PO Approval Limit',85000,'GROUP_NAME','Group Name','Group Name','','','default','','10','N','','','','','','','','HT038687','N','N','','EIS_PO_APPROVAL_LIMIT_V','','');
xxeis.eis_rs_ins.rc( 'User PO Approval Limit',85000,'IS_BUYER','Is Buyer','Is Buyer','','','default','','6','N','','','','','','','','HT038687','N','N','','EIS_PO_APPROVAL_LIMIT_V','','');
xxeis.eis_rs_ins.rc( 'User PO Approval Limit',85000,'USER_NAME','User Name','User Name','','','default','','1','N','','','','','','','','HT038687','N','N','','EIS_PO_APPROVAL_LIMIT_V','','');
--Inserting Report Parameters - User PO Approval Limit
xxeis.eis_rs_ins.rp( 'User PO Approval Limit',85000,'User Name (NT ID)','User Name (NT ID)','USER_NAME','LIKE','','%','VARCHAR2','Y','Y','1','','Y','CONSTANT','HT038687','Y','','','','');
xxeis.eis_rs_ins.rp( 'User PO Approval Limit',85000,'Full Name (Last, First Name)','Full Name (Last, First Name)','FULL_NAME','LIKE','','%','VARCHAR2','N','Y','2','','Y','CONSTANT','HT038687','Y','','','','');
--Inserting Report Conditions - User PO Approval Limit
xxeis.eis_rs_ins.rcn( 'User PO Approval Limit',85000,'USER_NAME','LIKE',':User Name (NT ID)','','','Y','1','Y','HT038687');
xxeis.eis_rs_ins.rcn( 'User PO Approval Limit',85000,'FULL_NAME','LIKE',':Full Name (Last, First Name)','','','Y','2','Y','HT038687');
--Inserting Report Sorts - User PO Approval Limit
--Inserting Report Triggers - User PO Approval Limit
--Inserting Report Templates - User PO Approval Limit
--Inserting Report Portals - User PO Approval Limit
--Inserting Report Dashboards - User PO Approval Limit
--Inserting Report Security - User PO Approval Limit
xxeis.eis_rs_ins.rsec( 'User PO Approval Limit','20005','','50843',85000,'HT038687','','');
xxeis.eis_rs_ins.rsec( 'User PO Approval Limit','20005','','51207',85000,'HT038687','','');
xxeis.eis_rs_ins.rsec( 'User PO Approval Limit','20005','','50861',85000,'HT038687','','');
--Inserting Report Pivots - User PO Approval Limit
END;
/
set scan on define on
