--Report Name            : WC All User List
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data XXEIS_943_ROAZTL_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(85000);
IF mod_exist = 'Y' THEN 
--Inserting Object XXEIS_943_ROAZTL_V
xxeis.eis_rsc_ins.v( 'XXEIS_943_ROAZTL_V',85000,'Paste SQL View for WC All User List','1.0','','','10011289','APPS','WC All User List View','X9RV','','','VIEW','US','','');
--Delete Object Columns for XXEIS_943_ROAZTL_V
xxeis.eis_rsc_utility.delete_view_rows('XXEIS_943_ROAZTL_V',85000,FALSE);
--Inserting Object Columns for XXEIS_943_ROAZTL_V
xxeis.eis_rsc_ins.vc( 'XXEIS_943_ROAZTL_V','USER_NAME',85000,'','','','','','10011289','VARCHAR2','','','User Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_943_ROAZTL_V','FULL_NAME',85000,'','','','','','10011289','VARCHAR2','','','Full Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_943_ROAZTL_V','EMAIL_ADDRESS',85000,'','','','','','10011289','VARCHAR2','','','Email Address','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_943_ROAZTL_V','FAX',85000,'','','','','','10011289','VARCHAR2','','','Fax','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_943_ROAZTL_V','EMPLOYEE_NUMBER',85000,'','','','','','10011289','VARCHAR2','','','Employee Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_943_ROAZTL_V','BUYER_JOB',85000,'','','','','','10011289','VARCHAR2','','','Buyer Job','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_943_ROAZTL_V','BUYER_POSITION',85000,'','','','','','10011289','VARCHAR2','','','Buyer Position','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_943_ROAZTL_V','IS_BUYER',85000,'','','','','','10011289','VARCHAR2','','','Is Buyer','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_943_ROAZTL_V','REGION',85000,'','','','','','10011289','VARCHAR2','','','Region','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_943_ROAZTL_V','DATE_LAST_LOGGED_IN',85000,'','','','','','10011289','DATE','','','Date Last Logged In','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_943_ROAZTL_V','FRU',85000,'','','','','','10011289','VARCHAR2','','','Fru','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_943_ROAZTL_V','FRU_DESCRIPTION',85000,'','','','','','10011289','VARCHAR2','','','Fru Description','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_943_ROAZTL_V','JOB_DESCRIPTION',85000,'','','','','','10011289','VARCHAR2','','','Job Description','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_943_ROAZTL_V','LOB',85000,'','','','','','10011289','VARCHAR2','','','Lob','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_943_ROAZTL_V','LOB_BRANCH',85000,'','','','','','10011289','VARCHAR2','','','Lob Branch','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_943_ROAZTL_V','SCOPE',85000,'','','','','','10011289','VARCHAR2','','','Scope','','','','US');
--Inserting Object Components for XXEIS_943_ROAZTL_V
--Inserting Object Component Joins for XXEIS_943_ROAZTL_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 85000');
END IF;
END;
/
--There are no LOV/LOV TL entries for the report WC All User List
prompt Creating Report Data for WC All User List
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(85000);
IF mod_exist = 'Y' THEN 
--Deleting Report data - WC All User List
xxeis.eis_rsc_utility.delete_report_rows( 'WC All User List' );
--Inserting Report - WC All User List
xxeis.eis_rsc_ins.r( 85000,'WC All User List','','Lists all White Cap users based on the default GL code, or assignment of any XXWC role or responsibility.','','','','10011289','XXEIS_943_ROAZTL_V','Y','','SELECT
  DISTINCT UPPER(u.user_name) user_name
          ,u.description AS full_name
          ,u.email_address
          ,u.fax
          ,PER.EMPLOYEE_NUMBER
          ,CASE WHEN buyers.buyer_employee_id IS NULL THEN '''' ELSE ''Y'' END is_buyer
          ,POS.NAME BUYER_POSITION
          ,POS.JOB_NAME BUYER_JOB
          ,PS.JOB_DESCR AS JOB_DESCRIPTION
          ,PS.ATTRIBUTE1 AS FRU
          ,LC.FRU_DESCR AS FRU_DESCRIPTION
          ,LC.LOB_BRANCH
          ,MP.ATTRIBUTE9 AS REGION
          ,LC.BUSINESS_UNIT AS LOB
          ,U.LAST_LOGON_DATE AS DATE_LAST_LOGGED_IN
          ,CASE WHEN scope.user_name IS NULL THEN ''GSC'' ELSE ''WC'' END scope
FROM
  apps.per_people_f per
  INNER JOIN apps.per_all_assignments_f ass
    ON ass.person_id = per.person_id
  INNER JOIN apps.gl_code_combinations gc
    ON gc.code_combination_id = ass.default_code_comb_id
  INNER JOIN apps.fnd_user u
    ON u.employee_id = per.person_id
  LEFT JOIN (SELECT
               DISTINCT user_name
             FROM
               (SELECT
                  DISTINCT u.user_name
                FROM
                  apps.per_people_f per
                  INNER JOIN apps.per_all_assignments_f ass
                    ON ass.person_id = per.person_id
                  INNER JOIN apps.gl_code_combinations gc
                    ON gc.code_combination_id = ass.default_code_comb_id
                  INNER JOIN apps.fnd_user u
                    ON u.employee_id = per.person_id
                WHERE /* Only include White Cap associates, segment1=''0W'' */
                  gc.segment1 = ''0W'' AND
                  NVL(per.effective_end_date, SYSDATE + 1) > SYSDATE AND
                  NVL(u.end_date, SYSDATE + 1) > SYSDATE
                UNION
                SELECT
                  DISTINCT a.user_name
                FROM
                  applsys.wf_user_role_assignments a
                WHERE
                  a.role_name LIKE ''%XXWC%'' AND
                  a.effective_start_date <= SYSDATE AND
                  a.effective_end_date >= SYSDATE)) scope
    ON scope.user_name = u.user_name
  LEFT JOIN apps.per_positions_v pos
    ON POS.POSITION_ID = ASS.POSITION_ID
  LEFT JOIN xxeis.eis_po_buyer_listing_v buyers
    ON PER.PERSON_ID = BUYERS.BUYER_EMPLOYEE_ID
  LEFT JOIN XXCUS.XXCUSHR_PS_EMP_ALL_TBL PS
    ON PS.EMPLOYEE_NUMBER = PER.EMPLOYEE_NUMBER
  LEFT JOIN XXCUS.XXCUS_LOCATION_CODE_TBL LC
    ON PS.ATTRIBUTE1 = LC.FRU
  LEFT JOIN APPS.MTL_PARAMETERS MP
    ON MP.ATTRIBUTE10 = LC.FRU AND
       MP.ATTRIBUTE11 = LC.LOB_BRANCH
WHERE /* Only include White Cap associates, segment1=''0W'' */
  (gc.segment1 = ''0W'' OR
   u.user_name IN (SELECT
                     a2.user_name
                   FROM
                     applsys.wf_user_role_assignments a2
                   WHERE
                     a2.user_name = u.user_name AND
                     a2.effective_start_date <= SYSDATE AND
                     a2.effective_end_date >= SYSDATE)) AND
  NVL(per.effective_end_date, SYSDATE + 1) > SYSDATE AND
  NVL(U.END_DATE, SYSDATE + 1) > SYSDATE
','10011289','','N','WC Audit Reports','','HTML,EXCEL,','N','','','','','','','APPS','US','','','','');
--Inserting Report Columns - WC All User List
xxeis.eis_rsc_ins.rc( 'WC All User List',85000,'USER_NAME','User Name (NT ID)','','','','','','1','N','','','','','','','','10011289','N','N','','XXEIS_943_ROAZTL_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC All User List',85000,'FULL_NAME','Full Name','','','','','','2','N','','','','','','','','10011289','N','N','','XXEIS_943_ROAZTL_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC All User List',85000,'EMAIL_ADDRESS','Email Address','','','','','','3','N','','','','','','','','10011289','N','N','','XXEIS_943_ROAZTL_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC All User List',85000,'FAX','Fax','','','','','','4','N','','','','','','','','10011289','N','N','','XXEIS_943_ROAZTL_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC All User List',85000,'EMPLOYEE_NUMBER','Employee Number','','','','','','5','N','','','','','','','','10011289','N','N','','XXEIS_943_ROAZTL_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC All User List',85000,'BUYER_JOB','Buyer Job','','','','','','6','N','','','','','','','','10011289','N','N','','XXEIS_943_ROAZTL_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC All User List',85000,'BUYER_POSITION','Buyer Position','','','','','','7','N','','','','','','','','10011289','N','N','','XXEIS_943_ROAZTL_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC All User List',85000,'IS_BUYER','Is Buyer','','','','','','8','N','','','','','','','','10011289','N','N','','XXEIS_943_ROAZTL_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC All User List',85000,'REGION','Region','','','','','','13','N','','','','','','','','10011289','N','N','','XXEIS_943_ROAZTL_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC All User List',85000,'DATE_LAST_LOGGED_IN','Date Last Logged In','','','','','','15','N','','','','','','','','10011289','N','N','','XXEIS_943_ROAZTL_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC All User List',85000,'FRU','Fru','','','','','','10','N','','','','','','','','10011289','N','N','','XXEIS_943_ROAZTL_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC All User List',85000,'FRU_DESCRIPTION','Fru Description','','','','','','11','N','','','','','','','','10011289','N','N','','XXEIS_943_ROAZTL_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC All User List',85000,'JOB_DESCRIPTION','Job Description','','','','','','9','N','','','','','','','','10011289','N','N','','XXEIS_943_ROAZTL_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC All User List',85000,'LOB','Lob','','','','','','14','N','','','','','','','','10011289','N','N','','XXEIS_943_ROAZTL_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC All User List',85000,'LOB_BRANCH','Lob Branch','','','','','','12','N','','','','','','','','10011289','N','N','','XXEIS_943_ROAZTL_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC All User List',85000,'SCOPE','Scope','','','','','','16','N','','','','','','','','10011289','N','N','','XXEIS_943_ROAZTL_V','','','GROUP_BY','US','');
--Inserting Report Parameters - WC All User List
xxeis.eis_rsc_ins.rp( 'WC All User List',85000,'User Name (NT ID)','User Name (NT ID)','USER_NAME','LIKE','','%','VARCHAR2','Y','Y','1','','Y','CONSTANT','10011289','Y','','','','','XXEIS_943_ROAZTL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC All User List',85000,'Full Name (Last, First Middle)','Full Name (Last, First Middle)','FULL_NAME','LIKE','','%','VARCHAR2','N','Y','2','','Y','CONSTANT','10011289','Y','','','','','XXEIS_943_ROAZTL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC All User List',85000,'Scope','Scope','SCOPE','LIKE','','WC','VARCHAR2','N','Y','3','','Y','CONSTANT','10011289','Y','','','','','XXEIS_943_ROAZTL_V','','','US','');
--Inserting Dependent Parameters - WC All User List
--Inserting Report Conditions - WC All User List
xxeis.eis_rsc_ins.rcnh( 'WC All User List',85000,'FULL_NAME LIKE :Full Name (Last, First Middle) ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','FULL_NAME','','Full Name (Last, First Middle)','','','','','XXEIS_943_ROAZTL_V','','','','','','LIKE','Y','Y','','','','','1',85000,'WC All User List','FULL_NAME LIKE :Full Name (Last, First Middle) ');
xxeis.eis_rsc_ins.rcnh( 'WC All User List',85000,'SCOPE LIKE :Scope ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','SCOPE','','Scope','','','','','XXEIS_943_ROAZTL_V','','','','','','LIKE','Y','Y','','','','','1',85000,'WC All User List','SCOPE LIKE :Scope ');
xxeis.eis_rsc_ins.rcnh( 'WC All User List',85000,'USER_NAME LIKE :User Name (NT ID) ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','USER_NAME','','User Name (NT ID)','','','','','XXEIS_943_ROAZTL_V','','','','','','LIKE','Y','Y','','','','','1',85000,'WC All User List','USER_NAME LIKE :User Name (NT ID) ');
--Inserting Report Sorts - WC All User List
xxeis.eis_rsc_ins.rs( 'WC All User List',85000,'FULL_NAME','ASC','10011289','1','');
--Inserting Report Triggers - WC All User List
--inserting report templates - WC All User List
--Inserting Report Portals - WC All User List
--inserting report dashboards - WC All User List
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'WC All User List','85000','XXEIS_943_ROAZTL_V','XXEIS_943_ROAZTL_V','N','');
--inserting report security - WC All User List
xxeis.eis_rsc_ins.rsec( 'WC All User List','20005','','XXWC_IT_FUNC_CONFIGURATOR',85000,'10011289','','','');
xxeis.eis_rsc_ins.rsec( 'WC All User List','20005','','XXWC_IT_SECURITY_ADMINISTRATOR',85000,'10011289','','','');
xxeis.eis_rsc_ins.rsec( 'WC All User List','660','','XXWC_ORDER_MGMT_PRICING_FULL',85000,'10011289','','','');
xxeis.eis_rsc_ins.rsec( 'WC All User List','20005','','XXWC_VIEW_ALL_EIS_REPORTS',85000,'10011289','','','');
xxeis.eis_rsc_ins.rsec( 'WC All User List','1','','HDS_SYSTEM_MAINTENANCE',85000,'10011289','','','');
xxeis.eis_rsc_ins.rsec( 'WC All User List','1','','',85000,'10011289','','','');
--Inserting Report Pivots - WC All User List
--Inserting Report   Version details- WC All User List
xxeis.eis_rsc_ins.rv( 'WC All User List','','WC All User List','AB063501');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 85000');
END IF;
END;
/
