--Report Name            : Account Analysis - (180 Char)
--Import Instructions : 1. If RTF template needs to be uploaded to report automatically then move the RTF template to physical path of APPS_DATA_FILE_DIR oracle directory. Check dba_directories view to get the physical path of oracle directory. 
--                         (If you want to use any other oracle directory other than APPS_DATA_FILE_DIR then replace APPS_DATA_FILE_DIR with your oracle directory name below.)
--                      2. Make sure XXEIS schema has read/write permissions to above used oracle directory and give full permissions to the directory at OS level.
--                      3. Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
exec xxeis.eis_rsc_ins.g_rtf_dir := 'APPS_DATA_FILE_DIR' ;
prompt Creating Object Data EIS_GL_ACCOUNT_ANALYSIS_180_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(101);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_GL_ACCOUNT_ANALYSIS_180_V
xxeis.eis_rsc_ins.v( 'EIS_GL_ACCOUNT_ANALYSIS_180_V',101,'This view shows details of accounts with entry, line and source information.','','','','XXEIS_RS_ADMIN','XXEIS','EIS GL Account Analysis','EGAA1V','','','VIEW','US','Y','','');
--Delete Object Columns for EIS_GL_ACCOUNT_ANALYSIS_180_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_GL_ACCOUNT_ANALYSIS_180_V',101,FALSE);
--Inserting Object Columns for EIS_GL_ACCOUNT_ANALYSIS_180_V
xxeis.eis_rsc_ins.vc( 'EIS_GL_ACCOUNT_ANALYSIS_180_V','GL_ACCOUNT_STRING',101,'Gl Account String','GL_ACCOUNT_STRING','','','GL Accounts LOV','XXEIS_RS_ADMIN','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','Gl Account String','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_ACCOUNT_ANALYSIS_180_V','LINE_DESCRIPTION',101,'Journal entry line description','LINE_DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_LINES','DESCRIPTION','Line Description','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_ACCOUNT_ANALYSIS_180_V','LINE_ITEM_REFERENCE',101,'Journal entry line reference column','LINE_ITEM_REFERENCE','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_LINES','REFERENCE_1','Line Item Reference','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_ACCOUNT_ANALYSIS_180_V','HEADER_ITEM_REFERENCE',101,'Extra reference column','HEADER_ITEM_REFERENCE','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_HEADERS','EXTERNAL_REFERENCE','Header Item Reference','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_ACCOUNT_ANALYSIS_180_V','REFERENCE_4',101,'Journal entry line reference column','REFERENCE_4','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_LINES','REFERENCE_4','Reference 4','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_ACCOUNT_ANALYSIS_180_V','ACCOUNTED_DEBITS',101,'Journal entry line debit amount in base currency','ACCOUNTED_DEBITS','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','GL_JE_LINES','ACCOUNTED_DR','Accounted Debits','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_ACCOUNT_ANALYSIS_180_V','ACCOUNTED_CREDITS',101,'Journal entry line credit amount in base currency','ACCOUNTED_CREDITS','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','GL_JE_LINES','ACCOUNTED_CR','Accounted Credits','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_ACCOUNT_ANALYSIS_180_V','ENTERED_DEBITS',101,'Journal entry line debit amount in entered currency','ENTERED_DEBITS','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','GL_JE_LINES','ENTERED_DR','Entered Debits','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_ACCOUNT_ANALYSIS_180_V','ENTERED_CREDITS',101,'Journal entry line credit amount in entered currency','ENTERED_CREDITS','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','GL_JE_LINES','ENTERED_CR','Entered Credits','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_ACCOUNT_ANALYSIS_180_V','STAT_DEBIT',101,'Stat Debit','STAT_DEBIT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','CALCULATION COLUMN','CALCULATION COLUMN','Stat Debit','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_ACCOUNT_ANALYSIS_180_V','STAT_CREDIT',101,'Stat Credit','STAT_CREDIT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','CALCULATION COLUMN','CALCULATION COLUMN','Stat Credit','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_ACCOUNT_ANALYSIS_180_V','BEGINING_BALANCE',101,'Begining Balance','BEGINING_BALANCE','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','CALCULATION COLUMN','CALCULATION COLUMN','Begining Balance','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_ACCOUNT_ANALYSIS_180_V','ENDING_BALANCE',101,'Ending Balance','ENDING_BALANCE','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','CALCULATION COLUMN','CALCULATION COLUMN','Ending Balance','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_ACCOUNT_ANALYSIS_180_V','JE_HEADER_ID',101,'Journal entry header defining column','JE_HEADER_ID','','','','XXEIS_RS_ADMIN','NUMBER','GL_JE_HEADERS','JE_HEADER_ID','Je Header Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_ACCOUNT_ANALYSIS_180_V','LEDGER_NAME',101,'Ledger name','LEDGER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_LEDGERS','NAME','Ledger Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_ACCOUNT_ANALYSIS_180_V','JOURNAL_CURRENCY_CODE',101,'Currency','JOURNAL_CURRENCY_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_HEADERS','CURRENCY_CODE','Journal Currency Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_ACCOUNT_ANALYSIS_180_V','JOURNAL_SOURCE',101,'Journal entry source user defined name','JOURNAL_SOURCE','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_SOURCES_TL','USER_JE_SOURCE_NAME','Journal Source','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_ACCOUNT_ANALYSIS_180_V','JOURNAL_CATEGORY',101,'Journal entry category user defined name','JOURNAL_CATEGORY','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_CATEGORIES_TL','USER_JE_CATEGORY_NAME','Journal Category','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_ACCOUNT_ANALYSIS_180_V','PERIOD_NAME',101,'Accounting period','PERIOD_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_LINES','PERIOD_NAME','Period Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_ACCOUNT_ANALYSIS_180_V','EFFECTIVE_DATE',101,'Journal entry line effective date','EFFECTIVE_DATE','','','','XXEIS_RS_ADMIN','DATE','GL_JE_LINES','EFFECTIVE_DATE','Effective Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_ACCOUNT_ANALYSIS_180_V','BATCH_NAME',101,'Name of journal entry batch','BATCH_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_BATCHES','NAME','Batch Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_ACCOUNT_ANALYSIS_180_V','JOURNAL_NAME',101,'Journal entry header name','JOURNAL_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_HEADERS','NAME','Journal Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_ACCOUNT_ANALYSIS_180_V','CODE_COMBINATION_ID',101,'Key flexfield combination defining column','CODE_COMBINATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','GL_CODE_COMBINATIONS','CODE_COMBINATION_ID','Code Combination Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_ACCOUNT_ANALYSIS_180_V','EFFECTIVE_PERIOD_NUM',101,'Denormalized period number (period_year*10000 + period_num)','EFFECTIVE_PERIOD_NUM','','','','XXEIS_RS_ADMIN','NUMBER','GL_PERIOD_STATUSES','EFFECTIVE_PERIOD_NUM','Effective Period Num','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_ACCOUNT_ANALYSIS_180_V','BALANCE_TYPE',101,'Balance type (Actual, Budget, or Encumbrance)','BALANCE_TYPE','','','GL Balance Types LOV','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_HEADERS','ACTUAL_FLAG','Balance Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_ACCOUNT_ANALYSIS_180_V','CURRENCY_CODE',101,'Currency','CURRENCY_CODE','','','GL BASE and STAT Currency Codes LOV','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_HEADERS','CURRENCY_CODE','Currency Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_ACCOUNT_ANALYSIS_180_V','JOURNAL_LINE_NUM',101,'Journal entry line number','JOURNAL_LINE_NUM','','','','XXEIS_RS_ADMIN','NUMBER','GL_JE_LINES','JE_LINE_NUM','Journal Line Num','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_ACCOUNT_ANALYSIS_180_V','BATCH_ID',101,'Journal entry batch defining column','BATCH_ID','','','','XXEIS_RS_ADMIN','NUMBER','GL_JE_HEADERS','JE_BATCH_ID','Batch Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_ACCOUNT_ANALYSIS_180_V','SET_OF_BOOKS_ID',101,'Accounting books defining column','SET_OF_BOOKS_ID~SET_OF_BOOKS_ID','','','','XXEIS_RS_ADMIN','NUMBER','GL_PERIOD_STATUSES','SET_OF_BOOKS_ID','Set Of Books Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_ACCOUNT_ANALYSIS_180_V','APPLICATION_ID',101,'Application defining column','APPLICATION_ID~APPLICATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','GL_PERIOD_STATUSES','APPLICATION_ID','Application Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_ACCOUNT_ANALYSIS_180_V','JE_BATCH_ID',101,'Journal entry batch defining column','JE_BATCH_ID~JE_BATCH_ID','','','','XXEIS_RS_ADMIN','NUMBER','GL_JE_BATCHES','JE_BATCH_ID','Je Batch Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_ACCOUNT_ANALYSIS_180_V','JE_LINE_NUM',101,'Journal entry line number','JE_LINE_NUM~JE_LINE_NUM','','','','XXEIS_RS_ADMIN','NUMBER','GL_JE_LINES','JE_LINE_NUM','Je Line Num','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_ACCOUNT_ANALYSIS_180_V','SET_OF_BOOKS_NAME',101,'Set Of Books Name','SET_OF_BOOKS_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Set Of Books Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_ACCOUNT_ANALYSIS_180_V','COPYRIGHT',101,'Copyright','COPYRIGHT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Copyright','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_ACCOUNT_ANALYSIS_180_V','GCC#50328#ACCOUNT',101,'Accounting Flexfield (KFF): Segment ''Account'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#ACCOUNT','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT4','GCC#Account','50328','1014550','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_ACCOUNT_ANALYSIS_180_V','GCC#50328#ACCOUNT#DESCR',101,'Accounting Flexfield (KFF) : Segment ''Account'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#ACCOUNT#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT4','GCC#Account Descr','50328','1014550','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_ACCOUNT_ANALYSIS_180_V','GCC#50328#COST_CENTER',101,'Accounting Flexfield (KFF): Segment ''Cost Center'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#COST_CENTER','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT3','GCC#Cost Center','50328','1014549','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_ACCOUNT_ANALYSIS_180_V','GCC#50328#COST_CENTER#DESCR',101,'Accounting Flexfield (KFF) : Segment ''Cost Center'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#COST_CENTER#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT3','GCC#Cost Center Descr','50328','1014549','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_ACCOUNT_ANALYSIS_180_V','GCC#50328#FUTURE_USE',101,'Accounting Flexfield (KFF): Segment ''Future Use'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#FUTURE_USE','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT6','GCC#Future Use','50328','1014552','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_ACCOUNT_ANALYSIS_180_V','GCC#50328#FUTURE_USE#DESCR',101,'Accounting Flexfield (KFF) : Segment ''Future Use'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#FUTURE_USE#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT6','GCC#Future Use Descr','50328','1014552','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_ACCOUNT_ANALYSIS_180_V','GCC#50328#FUTURE_USE_2',101,'Accounting Flexfield (KFF): Segment ''Future Use 2'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#FUTURE_USE_2','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT7','GCC#Future Use 2','50328','1014948','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_ACCOUNT_ANALYSIS_180_V','GCC#50328#FUTURE_USE_2#DESCR',101,'Accounting Flexfield (KFF) : Segment ''Future Use 2'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#FUTURE_USE_2#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT7','GCC#Future Use 2 Descr','50328','1014948','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_ACCOUNT_ANALYSIS_180_V','GCC#50328#LOCATION',101,'Accounting Flexfield (KFF): Segment ''Location'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT2','GCC#Location','50328','1014548','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_ACCOUNT_ANALYSIS_180_V','GCC#50328#LOCATION#DESCR',101,'Accounting Flexfield (KFF) : Segment ''Location'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#LOCATION#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT2','GCC#Location Descr','50328','1014548','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_ACCOUNT_ANALYSIS_180_V','GCC#50328#PRODUCT',101,'Accounting Flexfield (KFF): Segment ''Product'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#PRODUCT','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT1','GCC#Product','50328','1014547','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_ACCOUNT_ANALYSIS_180_V','GCC#50328#PRODUCT#DESCR',101,'Accounting Flexfield (KFF) : Segment ''Product'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#PRODUCT#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT1','GCC#Product Descr','50328','1014547','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_ACCOUNT_ANALYSIS_180_V','GCC#50328#PROJECT_CODE',101,'Accounting Flexfield (KFF): Segment ''Project Code'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#PROJECT_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT5','GCC#Project Code','50328','1014551','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_ACCOUNT_ANALYSIS_180_V','GCC#50328#PROJECT_CODE#DESCR',101,'Accounting Flexfield (KFF) : Segment ''Project Code'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#PROJECT_CODE#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT5','GCC#Project Code Descr','50328','1014551','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_ACCOUNT_ANALYSIS_180_V','GCC#Branch',101,'Descriptive flexfield (DFF): GL Accounts Column Name: Branch','GCC#Branch','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','ATTRIBUTE1','Gcc#Branch','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_ACCOUNT_ANALYSIS_180_V','JEL#Account',101,'Descriptive flexfield (DFF): Enter Journals: Lines Column Name: Account','JEL#Account','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_LINES','ATTRIBUTE3','Jel#Account','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_ACCOUNT_ANALYSIS_180_V','JEL#Branch',101,'Descriptive flexfield (DFF): Enter Journals: Lines Column Name: Branch','JEL#Branch','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_LINES','ATTRIBUTE1','Jel#Branch','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_ACCOUNT_ANALYSIS_180_V','JEL#Dept',101,'Descriptive flexfield (DFF): Enter Journals: Lines Column Name: Dept','JEL#Dept','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_LINES','ATTRIBUTE2','Jel#Dept','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_ACCOUNT_ANALYSIS_180_V','JEL#POS_Branch',101,'Descriptive flexfield (DFF): Enter Journals: Lines Column Name: POS Branch','JEL#POS_Branch','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_LINES','ATTRIBUTE5','Jel#Pos Branch','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_ACCOUNT_ANALYSIS_180_V','JEL#Sub_Acct',101,'Descriptive flexfield (DFF): Enter Journals: Lines Column Name: Sub Acct','JEL#Sub_Acct','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_LINES','ATTRIBUTE4','Jel#Sub Acct','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_ACCOUNT_ANALYSIS_180_V','JEL#WC_Formula',101,'Descriptive flexfield (DFF): Enter Journals: Lines Column Name: WC Formula','JEL#WC_Formula','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_LINES','ATTRIBUTE6','Jel#Wc Formula','','','','US','');
--Inserting Object Components for EIS_GL_ACCOUNT_ANALYSIS_180_V
xxeis.eis_rsc_ins.vcomp( 'EIS_GL_ACCOUNT_ANALYSIS_180_V','GL_JE_BATCHES',101,'GL_JE_BATCHES','JEB','JEB','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Journal Entry Batches','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_GL_ACCOUNT_ANALYSIS_180_V','GL_LEDGERS',101,'GL_LEDGERS','GLE','GLE','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Ledgers','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_GL_ACCOUNT_ANALYSIS_180_V','GL_PERIOD_STATUSES',101,'GL_PERIOD_STATUSES','GPS','GPS','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Calendar Period Statuses','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_GL_ACCOUNT_ANALYSIS_180_V','GL_JE_LINES',101,'GL_JE_LINES','JEL','JEL','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Journal Entry Lines','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_GL_ACCOUNT_ANALYSIS_180_V','GL_JE_HEADERS',101,'GL_JE_HEADERS','JEH','JEH','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Journal Entry Headers','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_GL_ACCOUNT_ANALYSIS_180_V','GL_CODE_COMBINATIONS_KFV',101,'GL_CODE_COMBINATIONS','GCC','GCC','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','GL Code Combinations','','','','','','Y',' ',' ',' ',' ',' ');
--Inserting Object Component Joins for EIS_GL_ACCOUNT_ANALYSIS_180_V
xxeis.eis_rsc_ins.vcj( 'EIS_GL_ACCOUNT_ANALYSIS_180_V','GL_JE_HEADERS','JEH',101,'EGAA1V.JE_HEADER_ID','=','JEH.JE_HEADER_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_GL_ACCOUNT_ANALYSIS_180_V','GL_JE_BATCHES','JEB',101,'EGAA1V.JE_BATCH_ID','=','JEB.JE_BATCH_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_GL_ACCOUNT_ANALYSIS_180_V','GL_LEDGERS','GLE',101,'EGAA1V.LEDGER_NAME','=','GLE.NAME(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_GL_ACCOUNT_ANALYSIS_180_V','GL_PERIOD_STATUSES','GPS',101,'EGAA1V.APPLICATION_ID','=','GPS.APPLICATION_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_GL_ACCOUNT_ANALYSIS_180_V','GL_PERIOD_STATUSES','GPS',101,'EGAA1V.SET_OF_BOOKS_ID','=','GPS.SET_OF_BOOKS_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_GL_ACCOUNT_ANALYSIS_180_V','GL_PERIOD_STATUSES','GPS',101,'EGAA1V.PERIOD_NAME','=','GPS.PERIOD_NAME(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_GL_ACCOUNT_ANALYSIS_180_V','GL_JE_LINES','JEL',101,'EGAA1V.JE_HEADER_ID','=','JEL.JE_HEADER_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_GL_ACCOUNT_ANALYSIS_180_V','GL_JE_LINES','JEL',101,'EGAA1V.JE_LINE_NUM','=','JEL.JE_LINE_NUM(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_GL_ACCOUNT_ANALYSIS_180_V','GL_CODE_COMBINATIONS_KFV','GCC',101,'EGAA1V.CODE_COMBINATION_ID','=','GCC.CODE_COMBINATION_ID(+)','','','','','XXEIS_RS_ADMIN');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 101');
END IF;
END;
/
prompt Creating Report LOV Data for Account Analysis - (180 Char)
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(101);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - Account Analysis - (180 Char)
xxeis.eis_rsc_ins.lov( 101,'select  per.period_name , led.name ledger_name, per.period_year, per.period_num, per.start_date, per.end_date
from    gl_periods per,gl_ledgers led
where  per.period_set_name = led.period_set_name
and     GL_SECURITY_PKG.VALIDATE_ACCESS(led.ledger_ID) = ''TRUE''
and     led.accounted_period_type = per.period_type','null','EIS_GL_PERIOD_LOV','Derives GL Periods based on the corresponding setups in the sets of books','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 101,'SELECT CURR.CURRENCY_CODE
FROM FND_CURRENCIES CURR, GL_LEDGERS LED
WHERE CURR.ENABLED_FLAG IN (''Y'',''N'')
AND   GL_SECURITY_PKG.VALIDATE_ACCESS(LED.LEDGER_ID) = ''TRUE''
AND (CURR.CURRENCY_CODE = ''STAT'' OR
CURR.CURRENCY_CODE = LED.CURRENCY_CODE)
ORDER BY CURR.CURRENCY_CODE','','GL_CURR_STAT_AND_BASE_LOV','Currency Code - Only BASE and STAT','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 101,'SELECT DESCRIPTION
FROM GL_LOOKUPS
WHERE LOOKUP_TYPE = ''BATCH_TYPE''
ORDER BY UPPER(MEANING)','','EIS_GL_BALANCE_TYPE_LOV','LOV containing Balance Types of Actual/Budget/Encumbrance','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 101,'SELECT  CONCATENATED_SEGMENTS
FROM   gl_code_combinations_kfv glcc
WHERE  chart_of_accounts_id IN (
SELECT led.chart_of_accounts_id
FROM gl_ledgers led
WHERE GL_SECURITY_PKG.VALIDATE_ACCESS(LED.LEDGER_ID) = ''TRUE'')
and nvl(summary_flag,''N'') = upper(''N'')
order by CONCATENATED_SEGMENTS','','EIS_GLACCOUNT_CONCAT_LOV','EIS_GLACCOUNT_CONCAT_LOV','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 101');
END IF;
END;
/
prompt Creating Report Data for Account Analysis - (180 Char)
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(101);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Account Analysis - (180 Char)
xxeis.eis_rsc_utility.delete_report_rows( 'Account Analysis - (180 Char)',101 );
--Inserting Report - Account Analysis - (180 Char)
xxeis.eis_rsc_ins.r( 101,'Account Analysis - (180 Char)','','Enables review of source, category and reference information to trace your functional currency or
STAT transactions back to their original source. This report can be run with entry, line or source item reference information to help identify the origin of journals created by Journal Import.
This report prints the journal entry lines and beginning and ending balances of the accounts you request. For each journal entry line, the report prints the source, category, batch name, journal entry name, account, description, entry/line/source item reference information, and the debit or credit amount.','','','','XXEIS_RS_ADMIN','EIS_GL_ACCOUNT_ANALYSIS_180_V','Y','','','XXEIS_RS_ADMIN','','N','Balances and Account Inquiry','','CSV,Pivot Excel,EXCEL,','N','','','','','','','','US','','','','','','','','','','','','','','','','');
--Inserting Report Columns - Account Analysis - (180 Char)
xxeis.eis_rsc_ins.rc( 'Account Analysis - (180 Char)',101,'CURRENCY_CODE','Currency Code','Currency','','','','','27','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_ACCOUNT_ANALYSIS_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Account Analysis - (180 Char)',101,'LEDGER_NAME','Ledger Name','Ledger name','','','','','28','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_ACCOUNT_ANALYSIS_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Account Analysis - (180 Char)',101,'ACCOUNTED_CREDITS','Accounted Credits','Journal entry line credit amount in base currency','','~T~D~2','default','','1','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_ACCOUNT_ANALYSIS_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Account Analysis - (180 Char)',101,'ACCOUNTED_DEBITS','Accounted Debits','Journal entry line debit amount in base currency','','~T~D~2','default','','2','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_ACCOUNT_ANALYSIS_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Account Analysis - (180 Char)',101,'BALANCE_TYPE','Balance Type','Balance type (Actual, Budget, or Encumbrance)','','','default','','3','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_ACCOUNT_ANALYSIS_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Account Analysis - (180 Char)',101,'BATCH_ID','Batch Id','Journal entry batch defining column','','~~~','default','','4','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_ACCOUNT_ANALYSIS_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Account Analysis - (180 Char)',101,'BATCH_NAME','Batch Name','Name of journal entry batch','','','default','','5','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_ACCOUNT_ANALYSIS_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Account Analysis - (180 Char)',101,'CODE_COMBINATION_ID','Code Combination Id','Key flexfield combination defining column','','~~~','default','','6','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_ACCOUNT_ANALYSIS_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Account Analysis - (180 Char)',101,'EFFECTIVE_PERIOD_NUM','Effective Period Num','Denormalized period number (period_year*10000 + period_num)','','~~~','default','','7','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_ACCOUNT_ANALYSIS_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Account Analysis - (180 Char)',101,'ENTERED_CREDITS','Entered Credits','Journal entry line credit amount in entered currency','','~T~D~2','default','','8','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_ACCOUNT_ANALYSIS_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Account Analysis - (180 Char)',101,'ENTERED_DEBITS','Entered Debits','Journal entry line debit amount in entered currency','','~T~D~2','default','','9','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_ACCOUNT_ANALYSIS_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Account Analysis - (180 Char)',101,'GL_ACCOUNT_STRING','Gl Account String','Gl Account String','','','default','','10','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_ACCOUNT_ANALYSIS_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Account Analysis - (180 Char)',101,'HEADER_ITEM_REFERENCE','Header Item Reference','Extra reference column','','','','','11','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_ACCOUNT_ANALYSIS_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Account Analysis - (180 Char)',101,'JE_HEADER_ID','Je Header Id','Journal entry header defining column','','~~~','','','12','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_ACCOUNT_ANALYSIS_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Account Analysis - (180 Char)',101,'JOURNAL_CATEGORY','Journal Category','Journal entry category user defined name','','','','','13','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_ACCOUNT_ANALYSIS_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Account Analysis - (180 Char)',101,'JOURNAL_CURRENCY_CODE','Journal Currency Code','Currency','','','','','14','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_ACCOUNT_ANALYSIS_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Account Analysis - (180 Char)',101,'JOURNAL_LINE_NUM','Journal Line Num','Journal entry line number','','~~~','','','15','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_ACCOUNT_ANALYSIS_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Account Analysis - (180 Char)',101,'JOURNAL_NAME','Journal Name','Journal entry header name','','','','','16','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_ACCOUNT_ANALYSIS_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Account Analysis - (180 Char)',101,'JOURNAL_SOURCE','Journal Source','Journal entry source user defined name','','','','','17','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_ACCOUNT_ANALYSIS_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Account Analysis - (180 Char)',101,'LINE_DESCRIPTION','Line Description','Journal entry line description','','','','','18','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_ACCOUNT_ANALYSIS_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Account Analysis - (180 Char)',101,'LINE_ITEM_REFERENCE','Line Item Reference','Journal entry line reference column','','','','','19','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_ACCOUNT_ANALYSIS_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Account Analysis - (180 Char)',101,'PERIOD_NAME','Period Name','Accounting period','','','','','20','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_ACCOUNT_ANALYSIS_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Account Analysis - (180 Char)',101,'REFERENCE_4','Reference 4','Journal entry line reference column','','','','','21','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_ACCOUNT_ANALYSIS_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Account Analysis - (180 Char)',101,'STAT_CREDIT','Stat Credit','Stat Credit','','~T~D~2','','','22','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_ACCOUNT_ANALYSIS_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Account Analysis - (180 Char)',101,'STAT_DEBIT','Stat Debit','Stat Debit','','~T~D~2','','','23','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_ACCOUNT_ANALYSIS_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Account Analysis - (180 Char)',101,'EFFECTIVE_DATE','Effective Date','Journal entry line effective date','','','','','24','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_ACCOUNT_ANALYSIS_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Account Analysis - (180 Char)',101,'BEGINING_BALANCE','Begining Balance','Begining Balance','','~T~D~2','','','25','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_ACCOUNT_ANALYSIS_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Account Analysis - (180 Char)',101,'ENDING_BALANCE','Ending Balance','Ending Balance','','~T~D~2','','','26','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_ACCOUNT_ANALYSIS_180_V','','','','US','','');
--Inserting Report Parameters - Account Analysis - (180 Char)
xxeis.eis_rsc_ins.rp( 'Account Analysis - (180 Char)',101,'Currency','Currency','CURRENCY_CODE','IN','GL_CURR_STAT_AND_BASE_LOV','select currency_code from   GL_SETS_OF_BOOKS where  SET_OF_BOOKS_ID         = FND_PROFILE.VALUE(''GL_SET_OF_BKS_ID'')','VARCHAR2','Y','Y','2','Y','Y','SQL','XXEIS_RS_ADMIN','Y','N','','','','EIS_GL_ACCOUNT_ANALYSIS_180_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Account Analysis - (180 Char)',101,'Starting Period','Starting Period','','IN','EIS_GL_PERIOD_LOV','','VARCHAR2','Y','Y','4','N','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_GL_ACCOUNT_ANALYSIS_180_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Account Analysis - (180 Char)',101,'Ending Period','Ending Period','','IN','EIS_GL_PERIOD_LOV','','VARCHAR2','Y','Y','5','N','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_GL_ACCOUNT_ANALYSIS_180_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Account Analysis - (180 Char)',101,'GL Account String','GL Account String','GL_ACCOUNT_STRING','IN','EIS_GLACCOUNT_CONCAT_LOV','','VARCHAR2','N','Y','6','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','Y','GL#SEGMENT','','','EIS_GL_ACCOUNT_ANALYSIS_180_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Account Analysis - (180 Char)',101,'Balance Type','Balance Type','BALANCE_TYPE','IN','EIS_GL_BALANCE_TYPE_LOV','','VARCHAR2','Y','Y','3','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_GL_ACCOUNT_ANALYSIS_180_V','','','US','');
--Inserting Dependent Parameters - Account Analysis - (180 Char)
--Inserting Report Conditions - Account Analysis - (180 Char)
xxeis.eis_rsc_ins.rcnh( 'Account Analysis - (180 Char)',101,'BALANCE_TYPE IN :Balance Type ','SIMPLE','','','Y','','1');
xxeis.eis_rsc_ins.rcnd( '','','BALANCE_TYPE','','Balance Type','','','','','EIS_GL_ACCOUNT_ANALYSIS_180_V','','','','','','IN','Y','Y','','','','','1',101,'Account Analysis - (180 Char)','BALANCE_TYPE IN :Balance Type ');
xxeis.eis_rsc_ins.rcnh( 'Account Analysis - (180 Char)',101,'CURRENCY_CODE IN :Currency ','SIMPLE','','','Y','','2');
xxeis.eis_rsc_ins.rcnd( '','','CURRENCY_CODE','','Currency','','','','','EIS_GL_ACCOUNT_ANALYSIS_180_V','','','','','','IN','Y','Y','','','','','1',101,'Account Analysis - (180 Char)','CURRENCY_CODE IN :Currency ');
xxeis.eis_rsc_ins.rcnh( 'Account Analysis - (180 Char)',101,'EFFECTIVE_PERIOD_NUM <= xxeis.EIS_RS_GL_FIN_COM_UTIL_PKG.get_period_num(:Ending Period) ','ADVANCED','','1#$#','Y','','3');
xxeis.eis_rsc_ins.rcnd( '','','EFFECTIVE_PERIOD_NUM','','','','','','','EIS_GL_ACCOUNT_ANALYSIS_180_V','','','','','','LESS_THAN_EQUALS','Y','Y','','xxeis.EIS_RS_GL_FIN_COM_UTIL_PKG.get_period_num(:Ending Period)','','','1',101,'Account Analysis - (180 Char)','EFFECTIVE_PERIOD_NUM <= xxeis.EIS_RS_GL_FIN_COM_UTIL_PKG.get_period_num(:Ending Period) ');
xxeis.eis_rsc_ins.rcnh( 'Account Analysis - (180 Char)',101,'EFFECTIVE_PERIOD_NUM >= xxeis.EIS_RS_GL_FIN_COM_UTIL_PKG.get_period_num(:Starting Period) ','ADVANCED','','1#$#','Y','','4');
xxeis.eis_rsc_ins.rcnd( '','','EFFECTIVE_PERIOD_NUM','','','','','','','EIS_GL_ACCOUNT_ANALYSIS_180_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','xxeis.EIS_RS_GL_FIN_COM_UTIL_PKG.get_period_num(:Starting Period)','','','1',101,'Account Analysis - (180 Char)','EFFECTIVE_PERIOD_NUM >= xxeis.EIS_RS_GL_FIN_COM_UTIL_PKG.get_period_num(:Starting Period) ');
xxeis.eis_rsc_ins.rcnh( 'Account Analysis - (180 Char)',101,'GL_ACCOUNT_STRING IN :GL Account String ','SIMPLE','','','Y','','5');
xxeis.eis_rsc_ins.rcnd( '','','GL_ACCOUNT_STRING','','GL Account String','','','','','EIS_GL_ACCOUNT_ANALYSIS_180_V','','','','','','IN','Y','Y','','','','','1',101,'Account Analysis - (180 Char)','GL_ACCOUNT_STRING IN :GL Account String ');
--Inserting Report Sorts - Account Analysis - (180 Char)
--Inserting Report Triggers - Account Analysis - (180 Char)
--inserting report templates - Account Analysis - (180 Char)
xxeis.eis_rsc_ins.r_tem( 'Account Analysis - (180 Char)','Account Analysis - (180 Char)','Seeded template for Account Analysis - (180 Char)','','','','','','','','','','','Account Analysis - (180 Char).rtf','XXEIS_RS_ADMIN','X','','','Y','Y','','');
--Inserting Report Portals - Account Analysis - (180 Char)
--inserting report dashboards - Account Analysis - (180 Char)
xxeis.eis_rsc_ins.R_dash( 'Account Analysis - (180 Char)','Account Analysis - (180 Char) Report','Account Analysis - (180 Char) Report','pie','large','Source Name','Source Name','','Credits','Sum','XXEIS_RS_ADMIN');
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Account Analysis - (180 Char)','101','EIS_GL_ACCOUNT_ANALYSIS_180_V','EIS_GL_ACCOUNT_ANALYSIS_180_V','N','');
--inserting report security - Account Analysis - (180 Char)
xxeis.eis_rsc_ins.rsec( 'Account Analysis - (180 Char)','20005','','XXWC_VIEW_ALL_EIS_REPORTS',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Account Analysis - (180 Char)','101','','XXCUS_GL_MANAGER_PS',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Account Analysis - (180 Char)','101','','XXCUS_GL_INQUIRY_PS',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Account Analysis - (180 Char)','101','','XXCUS_GL_ACCOUNTANT_USD_PS',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Account Analysis - (180 Char)','101','','XXCUS_GL_ACCOUNTANT_CAD_PS',101,'XXEIS_RS_ADMIN','','','');
--Inserting Report Pivots - Account Analysis - (180 Char)
--Inserting Report Distribution Details 
--Inserting Report Distribution Details 
--Inserting Report SubTotals Details 
--Inserting Report aggregations 
--Inserting Report   Version details- Account Analysis - (180 Char)
xxeis.eis_rsc_ins.rv( 'Account Analysis - (180 Char)','','Account Analysis - (180 Char)','AB065961','27-JUN-2017');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 101');
END IF;
END;
/
