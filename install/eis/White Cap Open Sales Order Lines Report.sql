--Report Name            : White Cap Open Sales Order Lines Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View XXWC_OPEN_ORDER_LINES_V
set scan off define off
prompt Creating View XXEIS.XXWC_OPEN_ORDER_LINES_V
Create or replace View XXEIS.XXWC_OPEN_ORDER_LINES_V
 AS 
SELECT DISTINCT mp.attribute9 AS Region ,
    mp.attribute8               AS District ,
    ood.organization_code       AS Branch_Number ,
    ood.organization_name       AS Branch_Name ,
    h.ordered_date              AS Header_Order_Date ,
    l.creation_date             AS Line_Create_Date ,
    l.request_date              AS Line_Request_Date ,
    fu.user_name                AS Created_By_NTID ,
    fu.description              AS Created_By_Name ,
    hca.account_number          AS Customer_Acct_Number ,
    hca.account_name            AS Customer_Acct_Name ,
    typ.name                    AS Order_Type ,
    h.order_number              AS Order_Number ,
    h.flow_status_code          AS Header_Status ,
    l.line_number               AS Line_Number ,
    l.flow_status_code          AS Line_Status ,
    l.user_item_description     AS User_Item_Description ,
    (CASE
      WHEN (l.flow_status_code = 'AWAITING_SHIPPING' and ( l.user_item_description = 'DELIVERED' OR  l.user_item_description = 'OUT_FOR_DELIVERY'))
        THEN  'OUT_FOR_DELIVERY or DELIVERED'
      WHEN (l.flow_status_code = 'AWAITING_SHIPPING' and ( l.user_item_description <> 'DELIVERED' OR  l.user_item_description <> 'OUT_FOR_DELIVERY' OR l.user_item_description is null))
        THEN  'NOT_SHIPPED'
      ELSE  '--'
    END) Awaiting_Shipping_Group,
    (SELECT wcsmv.ship_method_code_meaning
    FROM apps.wsh_carrier_ship_methods_v wcsmv
    WHERE wcsmv.ship_method_code = l.shipping_method_code
    AND ROWNUM                   = 1
    )                                                                                                                                             AS Ship_Via ,
    wss.creation_date                                                                                                                             AS Delivery_Create_Date ,
    l.ordered_item                                                                                                                                AS SKU ,
    DECODE(typ.name,'RETURN ORDER',(l.ordered_quantity *-1), l.ordered_quantity)                                                                  AS Order_Qty ,
    l.pricing_quantity_uom                                                                                                                        AS UOM ,
    DECODE(typ.name,'RETURN ORDER',(l.unit_selling_price     *-1), l.unit_selling_price)                                                          AS Unit_Sell_Price ,
    DECODE(typ.name,'RETURN ORDER',ROUND((l.ordered_quantity * l.unit_selling_price),2)*-1, ROUND((l.ordered_quantity * l.unit_selling_price),2)) AS Ext_Sales_Amt ,
    DECODE(typ.name,'RETURN ORDER', (l.unit_cost             * -1), l.unit_cost)                                                                  AS Unit_Cost ,
    DECODE(typ.name,'RETURN ORDER',ROUND((l.ordered_quantity * l.unit_cost),2)*-1, ROUND((l.ordered_quantity * l.unit_cost),2))                   AS Ext_Cost
  FROM ONT.oe_order_lines_all l ,
    ont.oe_order_headers_all h ,
    apps.org_organization_definitions ood ,
    apps.oe_order_types_v typ ,
    apps.mtl_parameters mp ,
    AR.HZ_CUST_ACCOUNTS hca ,
    applsys.fnd_user fu ,
    xxwc.xxwc_wsh_shipping_stg wss
  WHERE h.ordered_date       IS NOT NULL
  AND h.header_id             = l.header_id
  AND wss.header_id(+)        = l.header_id
  AND wss.line_id(+)          = l.line_id
  AND l.created_by            = fu.user_id
  AND hca.cust_account_id     = h.sold_to_org_id
  AND mp.organization_id      = l.ship_from_org_id
  AND l.ship_from_org_id      = ood.organization_id
  AND h.order_type_id         = typ.order_type_id
  AND h.flow_status_code NOT IN ('CANCELLED', 'ENTERED', 'CLOSED')
  AND l.flow_status_code NOT IN ('CANCELLED', 'ENTERED','CLOSED')
    --AND mp.attribute9 = 'NORTHWEST REGION'
  ORDER BY h.ordered_date/
set scan on define on
prompt Creating View Data for White Cap Open Sales Order Lines Report
set scan off define off
DECLARE
BEGIN 
--Inserting View XXWC_OPEN_ORDER_LINES_V
xxeis.eis_rs_ins.v( 'XXWC_OPEN_ORDER_LINES_V',660,'','','','','HT038687','XXEIS','Xxwc Open Order Lines V','XOOLV','','');
--Delete View Columns for XXWC_OPEN_ORDER_LINES_V
xxeis.eis_rs_utility.delete_view_rows('XXWC_OPEN_ORDER_LINES_V',660,FALSE);
--Inserting View Columns for XXWC_OPEN_ORDER_LINES_V
xxeis.eis_rs_ins.vc( 'XXWC_OPEN_ORDER_LINES_V','EXT_COST',660,'Ext Cost','EXT_COST','','','','HT038687','NUMBER','','','Ext Cost','','','');
xxeis.eis_rs_ins.vc( 'XXWC_OPEN_ORDER_LINES_V','UNIT_COST',660,'Unit Cost','UNIT_COST','','','','HT038687','NUMBER','','','Unit Cost','','','');
xxeis.eis_rs_ins.vc( 'XXWC_OPEN_ORDER_LINES_V','EXT_SALES_AMT',660,'Ext Sales Amt','EXT_SALES_AMT','','','','HT038687','NUMBER','','','Ext Sales Amt','','','');
xxeis.eis_rs_ins.vc( 'XXWC_OPEN_ORDER_LINES_V','UNIT_SELL_PRICE',660,'Unit Sell Price','UNIT_SELL_PRICE','','','','HT038687','NUMBER','','','Unit Sell Price','','','');
xxeis.eis_rs_ins.vc( 'XXWC_OPEN_ORDER_LINES_V','UOM',660,'Uom','UOM','','','','HT038687','VARCHAR2','','','Uom','','','');
xxeis.eis_rs_ins.vc( 'XXWC_OPEN_ORDER_LINES_V','ORDER_QTY',660,'Order Qty','ORDER_QTY','','','','HT038687','NUMBER','','','Order Qty','','','');
xxeis.eis_rs_ins.vc( 'XXWC_OPEN_ORDER_LINES_V','SKU',660,'Sku','SKU','','','','HT038687','VARCHAR2','','','Sku','','','');
xxeis.eis_rs_ins.vc( 'XXWC_OPEN_ORDER_LINES_V','DELIVERY_CREATE_DATE',660,'Delivery Create Date','DELIVERY_CREATE_DATE','','','','HT038687','DATE','','','Delivery Create Date','','','');
xxeis.eis_rs_ins.vc( 'XXWC_OPEN_ORDER_LINES_V','SHIP_VIA',660,'Ship Via','SHIP_VIA','','','','HT038687','VARCHAR2','','','Ship Via','','','');
xxeis.eis_rs_ins.vc( 'XXWC_OPEN_ORDER_LINES_V','AWAITING_SHIPPING_GROUP',660,'Awaiting Shipping Group','AWAITING_SHIPPING_GROUP','','','','HT038687','VARCHAR2','','','Awaiting Shipping Group','','','');
xxeis.eis_rs_ins.vc( 'XXWC_OPEN_ORDER_LINES_V','USER_ITEM_DESCRIPTION',660,'User Item Description','USER_ITEM_DESCRIPTION','','','','HT038687','VARCHAR2','','','User Item Description','','','');
xxeis.eis_rs_ins.vc( 'XXWC_OPEN_ORDER_LINES_V','LINE_STATUS',660,'Line Status','LINE_STATUS','','','','HT038687','VARCHAR2','','','Line Status','','','');
xxeis.eis_rs_ins.vc( 'XXWC_OPEN_ORDER_LINES_V','LINE_NUMBER',660,'Line Number','LINE_NUMBER','','','','HT038687','NUMBER','','','Line Number','','','');
xxeis.eis_rs_ins.vc( 'XXWC_OPEN_ORDER_LINES_V','HEADER_STATUS',660,'Header Status','HEADER_STATUS','','','','HT038687','VARCHAR2','','','Header Status','','','');
xxeis.eis_rs_ins.vc( 'XXWC_OPEN_ORDER_LINES_V','ORDER_NUMBER',660,'Order Number','ORDER_NUMBER','','','','HT038687','NUMBER','','','Order Number','','','');
xxeis.eis_rs_ins.vc( 'XXWC_OPEN_ORDER_LINES_V','ORDER_TYPE',660,'Order Type','ORDER_TYPE','','','','HT038687','VARCHAR2','','','Order Type','','','');
xxeis.eis_rs_ins.vc( 'XXWC_OPEN_ORDER_LINES_V','CUSTOMER_ACCT_NUMBER',660,'Customer Acct Number','CUSTOMER_ACCT_NUMBER','','','','HT038687','VARCHAR2','','','Customer Acct Number','','','');
xxeis.eis_rs_ins.vc( 'XXWC_OPEN_ORDER_LINES_V','CREATED_BY_NAME',660,'Created By Name','CREATED_BY_NAME','','','','HT038687','VARCHAR2','','','Created By Name','','','');
xxeis.eis_rs_ins.vc( 'XXWC_OPEN_ORDER_LINES_V','CREATED_BY_NTID',660,'Created By Ntid','CREATED_BY_NTID','','','','HT038687','VARCHAR2','','','Created By Ntid','','','');
xxeis.eis_rs_ins.vc( 'XXWC_OPEN_ORDER_LINES_V','LINE_REQUEST_DATE',660,'Line Request Date','LINE_REQUEST_DATE','','','','HT038687','DATE','','','Line Request Date','','','');
xxeis.eis_rs_ins.vc( 'XXWC_OPEN_ORDER_LINES_V','LINE_CREATE_DATE',660,'Line Create Date','LINE_CREATE_DATE','','','','HT038687','DATE','','','Line Create Date','','','');
xxeis.eis_rs_ins.vc( 'XXWC_OPEN_ORDER_LINES_V','HEADER_ORDER_DATE',660,'Header Order Date','HEADER_ORDER_DATE','','','','HT038687','DATE','','','Header Order Date','','','');
xxeis.eis_rs_ins.vc( 'XXWC_OPEN_ORDER_LINES_V','BRANCH_NAME',660,'Branch Name','BRANCH_NAME','','','','HT038687','VARCHAR2','','','Branch Name','','','');
xxeis.eis_rs_ins.vc( 'XXWC_OPEN_ORDER_LINES_V','BRANCH_NUMBER',660,'Branch Number','BRANCH_NUMBER','','','','HT038687','VARCHAR2','','','Branch Number','','','');
xxeis.eis_rs_ins.vc( 'XXWC_OPEN_ORDER_LINES_V','REGION',660,'Region','REGION','','','','HT038687','VARCHAR2','','','Region','','','');
xxeis.eis_rs_ins.vc( 'XXWC_OPEN_ORDER_LINES_V','DISTRICT',660,'District','DISTRICT','','','','HT038687','VARCHAR2','','','District','','','');
xxeis.eis_rs_ins.vc( 'XXWC_OPEN_ORDER_LINES_V','CUSTOMER_ACCT_NAME',660,'Customer Acct Name','CUSTOMER_ACCT_NAME','','','','HT038687','VARCHAR2','','','Customer Acct Name','','','');
xxeis.eis_rs_ins.vc( 'XXWC_OPEN_ORDER_LINES_V','CUSOTMER_ACCT_NAME',660,'Cusotmer Acct Name','CUSOTMER_ACCT_NAME','','','','HT038687','VARCHAR2','','','Cusotmer Acct Name','','','');
xxeis.eis_rs_ins.vc( 'XXWC_OPEN_ORDER_LINES_V','DESTRICT',660,'Destrict','DESTRICT','','','','HT038687','VARCHAR2','','','Destrict','','','');
--Inserting View Components for XXWC_OPEN_ORDER_LINES_V
--Inserting View Component Joins for XXWC_OPEN_ORDER_LINES_V
END;
/
set scan on define on
prompt Creating Report LOV Data for White Cap Open Sales Order Lines Report
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - White Cap Open Sales Order Lines Report
xxeis.eis_rs_ins.lov( 660,'select distinct ATTRIBUTE9 region from mtl_parameters','','Region Lov','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'select distinct organization_code org_code,organization_name org_name from org_organization_definitions','','Branch Lov','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'select distinct ATTRIBUTE8 District from mtl_parameters','','District Lov','','ANONYMOUS',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for White Cap Open Sales Order Lines Report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - White Cap Open Sales Order Lines Report
xxeis.eis_rs_utility.delete_report_rows( 'White Cap Open Sales Order Lines Report' );
--Inserting Report - White Cap Open Sales Order Lines Report
xxeis.eis_rs_ins.r( 660,'White Cap Open Sales Order Lines Report','','The purpose of this report is to list and to summarize all non-cancelled, open Sales Order Lines by Line Status and User Item Description for each Region / District / Branch as to provide visibility into any Sales Order Lines that can be auctioned and/or may require follow-up in order to progress the line to Closed status.


','','','','HT038687','XXWC_OPEN_ORDER_LINES_V','Y','','','HT038687','','N','White Cap Reports','','CSV,HTML,Html Summary,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - White Cap Open Sales Order Lines Report
xxeis.eis_rs_ins.rc( 'White Cap Open Sales Order Lines Report',660,'BRANCH_NAME','Branch Name','Branch Name','','','default','','4','N','','','','','','','','HT038687','N','N','','XXWC_OPEN_ORDER_LINES_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Open Sales Order Lines Report',660,'BRANCH_NUMBER','Branch Number','Branch Number','','','default','','3','N','','','','','','','','HT038687','N','N','','XXWC_OPEN_ORDER_LINES_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Open Sales Order Lines Report',660,'CREATED_BY_NAME','Created By Name','Created By Name','','','default','','9','N','','','','','','','','HT038687','N','N','','XXWC_OPEN_ORDER_LINES_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Open Sales Order Lines Report',660,'CREATED_BY_NTID','Created By NTID','Created By Ntid','','','default','','8','N','','','','','','','','HT038687','N','N','','XXWC_OPEN_ORDER_LINES_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Open Sales Order Lines Report',660,'CUSTOMER_ACCT_NUMBER','Customer Acct Number','Customer Acct Number','','','default','','10','N','','','','','','','','HT038687','N','N','','XXWC_OPEN_ORDER_LINES_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Open Sales Order Lines Report',660,'DELIVERY_CREATE_DATE','Delivery Create Date','Delivery Create Date','','','default','','20','N','','','','','','','','HT038687','N','N','','XXWC_OPEN_ORDER_LINES_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Open Sales Order Lines Report',660,'EXT_COST','Extended Cost','Ext Cost','','~~~','default','','27','N','','','','','','','','HT038687','N','N','','XXWC_OPEN_ORDER_LINES_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Open Sales Order Lines Report',660,'EXT_SALES_AMT','Extended Sales Amount','Ext Sales Amt','','~,~.~','default','','25','N','','','','','','','','HT038687','N','N','','XXWC_OPEN_ORDER_LINES_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Open Sales Order Lines Report',660,'HEADER_ORDER_DATE','Header Order Date','Header Order Date','','','default','','5','N','','','','','','','','HT038687','N','N','','XXWC_OPEN_ORDER_LINES_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Open Sales Order Lines Report',660,'HEADER_STATUS','Header Status','Header Status','','','default','','14','N','','','','','','','','HT038687','N','N','','XXWC_OPEN_ORDER_LINES_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Open Sales Order Lines Report',660,'LINE_CREATE_DATE','Line Create Date','Line Create Date','','','default','','6','N','','','','','','','','HT038687','N','N','','XXWC_OPEN_ORDER_LINES_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Open Sales Order Lines Report',660,'LINE_NUMBER','Line Number','Line Number','','~,~.~','default','','15','N','','','','','','','','HT038687','N','N','','XXWC_OPEN_ORDER_LINES_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Open Sales Order Lines Report',660,'LINE_REQUEST_DATE','Line Request Date','Line Request Date','','','default','','7','N','','','','','','','','HT038687','N','N','','XXWC_OPEN_ORDER_LINES_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Open Sales Order Lines Report',660,'LINE_STATUS','Line Status','Line Status','','','default','','16','N','','','','','','','','HT038687','N','N','','XXWC_OPEN_ORDER_LINES_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Open Sales Order Lines Report',660,'ORDER_NUMBER','Order Number','Order Number','','~~~','default','','13','N','','','','','','','','HT038687','N','N','','XXWC_OPEN_ORDER_LINES_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Open Sales Order Lines Report',660,'ORDER_QTY','Order Qty','Order Qty','','~~~','default','','22','N','','','','','','','','HT038687','N','N','','XXWC_OPEN_ORDER_LINES_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Open Sales Order Lines Report',660,'ORDER_TYPE','Order Type','Order Type','','','default','','12','N','','','','','','','','HT038687','N','N','','XXWC_OPEN_ORDER_LINES_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Open Sales Order Lines Report',660,'REGION','Region','Region','','','default','','1','N','','','','','','','','HT038687','N','N','','XXWC_OPEN_ORDER_LINES_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Open Sales Order Lines Report',660,'SHIP_VIA','Ship Via','Ship Via','','','default','','19','N','','','','','','','','HT038687','N','N','','XXWC_OPEN_ORDER_LINES_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Open Sales Order Lines Report',660,'SKU','SKU','Sku','','','default','','21','N','','','','','','','','HT038687','N','N','','XXWC_OPEN_ORDER_LINES_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Open Sales Order Lines Report',660,'UNIT_COST','Unit Cost','Unit Cost','','~~~','default','','26','N','','','','','','','','HT038687','N','N','','XXWC_OPEN_ORDER_LINES_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Open Sales Order Lines Report',660,'UNIT_SELL_PRICE','Unit Sell Price','Unit Sell Price','','~~~','default','','24','N','','','','','','','','HT038687','N','N','','XXWC_OPEN_ORDER_LINES_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Open Sales Order Lines Report',660,'UOM','UOM','Uom','','','default','','23','N','','','','','','','','HT038687','N','N','','XXWC_OPEN_ORDER_LINES_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Open Sales Order Lines Report',660,'USER_ITEM_DESCRIPTION','User Item Description','User Item Description','','','default','','17','N','','','','','','','','HT038687','N','N','','XXWC_OPEN_ORDER_LINES_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Open Sales Order Lines Report',660,'DISTRICT','District','District','','','default','','2','N','','','','','','','','HT038687','N','N','','XXWC_OPEN_ORDER_LINES_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Open Sales Order Lines Report',660,'AWAITING_SHIPPING_GROUP','Awaiting Shipping Group','Awaiting Shipping Group','','','default','','18','N','','','','','','','','HT038687','N','N','','XXWC_OPEN_ORDER_LINES_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Open Sales Order Lines Report',660,'CUSTOMER_ACCT_NAME','Customer Acct Name','Customer Acct Name','','','','','11','N','','','','','','','','HT038687','N','N','','XXWC_OPEN_ORDER_LINES_V','','');
--Inserting Report Parameters - White Cap Open Sales Order Lines Report
xxeis.eis_rs_ins.rp( 'White Cap Open Sales Order Lines Report',660,'Branch Name','Branch Name','BRANCH_NAME','IN','Branch Lov','','VARCHAR2','N','Y','3','','Y','CONSTANT','HT038687','Y','N','','','');
xxeis.eis_rs_ins.rp( 'White Cap Open Sales Order Lines Report',660,'District','District','DISTRICT','IN','District Lov','','VARCHAR2','N','Y','2','','Y','CONSTANT','HT038687','Y','N','','','');
xxeis.eis_rs_ins.rp( 'White Cap Open Sales Order Lines Report',660,'Region','Region','REGION','IN','Region Lov','','VARCHAR2','N','Y','1','','Y','CONSTANT','HT038687','Y','N','','','');
--Inserting Report Conditions - White Cap Open Sales Order Lines Report
xxeis.eis_rs_ins.rcn( 'White Cap Open Sales Order Lines Report',660,'REGION','IN',':Region','','','Y','1','Y','HT038687');
xxeis.eis_rs_ins.rcn( 'White Cap Open Sales Order Lines Report',660,'DISTRICT','IN',':District','','','Y','2','Y','HT038687');
xxeis.eis_rs_ins.rcn( 'White Cap Open Sales Order Lines Report',660,'BRANCH_NAME','IN',':Branch Name','','','Y','3','Y','HT038687');
--Inserting Report Sorts - White Cap Open Sales Order Lines Report
xxeis.eis_rs_ins.rs( 'White Cap Open Sales Order Lines Report',660,'HEADER_ORDER_DATE','ASC','HT038687','','1');
--Inserting Report Triggers - White Cap Open Sales Order Lines Report
--Inserting Report Templates - White Cap Open Sales Order Lines Report
--Inserting Report Portals - White Cap Open Sales Order Lines Report
--Inserting Report Dashboards - White Cap Open Sales Order Lines Report
--Inserting Report Security - White Cap Open Sales Order Lines Report
xxeis.eis_rs_ins.rsec( 'White Cap Open Sales Order Lines Report','660','','50869',660,'HT038687','','');
xxeis.eis_rs_ins.rsec( 'White Cap Open Sales Order Lines Report','20005','','50843',660,'HT038687','','');
xxeis.eis_rs_ins.rsec( 'White Cap Open Sales Order Lines Report','20005','','51207',660,'HT038687','','');
xxeis.eis_rs_ins.rsec( 'White Cap Open Sales Order Lines Report','20005','','50861',660,'HT038687','','');
xxeis.eis_rs_ins.rsec( 'White Cap Open Sales Order Lines Report','660','','51045',660,'HT038687','','');
xxeis.eis_rs_ins.rsec( 'White Cap Open Sales Order Lines Report','660','','50860',660,'HT038687','','');
xxeis.eis_rs_ins.rsec( 'White Cap Open Sales Order Lines Report','660','','50886',660,'HT038687','','');
xxeis.eis_rs_ins.rsec( 'White Cap Open Sales Order Lines Report','660','','50859',660,'HT038687','','');
xxeis.eis_rs_ins.rsec( 'White Cap Open Sales Order Lines Report','660','','50858',660,'HT038687','','');
xxeis.eis_rs_ins.rsec( 'White Cap Open Sales Order Lines Report','660','','50857',660,'HT038687','','');
xxeis.eis_rs_ins.rsec( 'White Cap Open Sales Order Lines Report','101','','50723',660,'HT038687','','');
xxeis.eis_rs_ins.rsec( 'White Cap Open Sales Order Lines Report','20005','','50900',660,'HT038687','','');
--Inserting Report Pivots - White Cap Open Sales Order Lines Report
xxeis.eis_rs_ins.rpivot( 'White Cap Open Sales Order Lines Report',660,'Pivot','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rs_ins.rpivot_dtls( 'White Cap Open Sales Order Lines Report',660,'Pivot','REGION','ROW_FIELD','','','1','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'White Cap Open Sales Order Lines Report',660,'Pivot','DISTRICT','ROW_FIELD','','','2','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'White Cap Open Sales Order Lines Report',660,'Pivot','BRANCH_NAME','ROW_FIELD','','','3','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'White Cap Open Sales Order Lines Report',660,'Pivot','LINE_NUMBER','DATA_FIELD','COUNT','','1','','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'White Cap Open Sales Order Lines Report',660,'Pivot','EXT_SALES_AMT','DATA_FIELD','SUM','','2','','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'White Cap Open Sales Order Lines Report',660,'Pivot','LINE_STATUS','COLUMN_FIELD','','','2','','');
xxeis.eis_rs_ins.rpivot_dtls( 'White Cap Open Sales Order Lines Report',660,'Pivot','AWAITING_SHIPPING_GROUP','COLUMN_FIELD','','','1','','');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
END;
/
set scan on define on
