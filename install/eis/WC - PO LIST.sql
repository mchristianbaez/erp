--Report Name            : WC - PO LIST
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data XXEIS_1091488_BDRWVZ_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Inserting Object XXEIS_1091488_BDRWVZ_V
xxeis.eis_rsc_ins.v( 'XXEIS_1091488_BDRWVZ_V',200,'Paste SQL View for TEST - PO LIST','1.0','','','10012196','APPS','TEST - PO LIST View','X1BV123','','','VIEW','US','','');
--Delete Object Columns for XXEIS_1091488_BDRWVZ_V
xxeis.eis_rsc_utility.delete_view_rows('XXEIS_1091488_BDRWVZ_V',200,FALSE);
--Inserting Object Columns for XXEIS_1091488_BDRWVZ_V
xxeis.eis_rsc_ins.vc( 'XXEIS_1091488_BDRWVZ_V','PO_NUM',200,'','','','','','10012196','VARCHAR2','','','Po Num','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1091488_BDRWVZ_V','CREATE_DATE',200,'','','','','','10012196','DATE','','','Create Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1091488_BDRWVZ_V','BUYER',200,'','','','','','10012196','VARCHAR2','','','Buyer','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1091488_BDRWVZ_V','BRANCH',200,'','','','','','10012196','VARCHAR2','','','Branch','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1091488_BDRWVZ_V','VENDOR_NUM',200,'','','','','','10012196','VARCHAR2','','','Vendor Num','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1091488_BDRWVZ_V','VENDOR_NAME',200,'','','','','','10012196','VARCHAR2','','','Vendor Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1091488_BDRWVZ_V','VENDOR_SITE_CODE',200,'','','','','','10012196','VARCHAR2','','','Vendor Site Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1091488_BDRWVZ_V','PO_TYPE',200,'','','','','','10012196','VARCHAR2','','','Po Type','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1091488_BDRWVZ_V','CLOSED_CODE',200,'','','','','','10012196','VARCHAR2','','','Closed Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1091488_BDRWVZ_V','CANCEL_FLAG',200,'','','','','','10012196','VARCHAR2','','','Cancel Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1091488_BDRWVZ_V','LINE_NUM',200,'Line Num','Line Num','','','','10012196','','','','Line Num','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1091488_BDRWVZ_V','QUANTITY',200,'Quantity','Quantity','','','','10012196','','','','Quantity','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1091488_BDRWVZ_V','UNIT_PRICE',200,'Unit Price','Unit Price','','','','10012196','','','','Unit Price','','','','US');
--Inserting Object Components for XXEIS_1091488_BDRWVZ_V
--Inserting Object Component Joins for XXEIS_1091488_BDRWVZ_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
--There are no LOV/LOV TL entries for the report WC - PO LIST
prompt Creating Report Data for WC - PO LIST
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Deleting Report data - WC - PO LIST
xxeis.eis_rsc_utility.delete_report_rows( 'WC - PO LIST' );
--Inserting Report - WC - PO LIST
xxeis.eis_rsc_ins.r( 200,'WC - PO LIST','','','','','','10012196','XXEIS_1091488_BDRWVZ_V','Y','','SELECT pha.segment1 po_num
,pha.attribute2 po_type
,pha.closed_code
,pha.cancel_flag
,pha.creation_date create_date
,papf.full_name buyer
,hla.location_code branch
,ass.segment1 vendor_num
,ass.vendor_name
,assa.vendor_site_code
FROM po.po_headers_all pha
,ap.ap_suppliers ass
,ap.ap_supplier_sites_all assa
,hr.hr_locations_all hla
,hr.per_all_people_f papf
WHERE pha.agent_id = papf.person_id
AND pha.vendor_id = ass.vendor_id
AND pha.vendor_site_id = assa.vendor_site_id
AND pha.ship_to_location_id = hla.location_id
AND pha.org_id = 162
','10012196','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N','','','','','','','APPS','US','','','','');
--Inserting Report Columns - WC - PO LIST
xxeis.eis_rsc_ins.rc( 'WC - PO LIST',200,'CREATE_DATE','Create Date','','','','default','','7','N','Y','','','','','','','10012196','N','N','','XXEIS_1091488_BDRWVZ_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'WC - PO LIST',200,'PO_NUM','Po Num','','','','default','','1','N','Y','','','','','','','10012196','N','N','','XXEIS_1091488_BDRWVZ_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'WC - PO LIST',200,'VENDOR_NUM','Vendor Num','','','','default','','4','N','Y','','','','','','','10012196','N','N','','XXEIS_1091488_BDRWVZ_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'WC - PO LIST',200,'BUYER','Buyer','','','','default','','3','N','Y','','','','','','','10012196','N','N','','XXEIS_1091488_BDRWVZ_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'WC - PO LIST',200,'VENDOR_NAME','Vendor Name','','','','default','','5','N','Y','','','','','','','10012196','N','N','','XXEIS_1091488_BDRWVZ_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'WC - PO LIST',200,'BRANCH','Branch','','','','default','','2','N','Y','','','','','','','10012196','N','N','','XXEIS_1091488_BDRWVZ_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'WC - PO LIST',200,'VENDOR_SITE_CODE','Vendor Site Code','','','','default','','6','N','Y','','','','','','','10012196','N','N','','XXEIS_1091488_BDRWVZ_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'WC - PO LIST',200,'PO_TYPE','Po Type','','','','default','','8','N','Y','','','','','','','10012196','N','N','','XXEIS_1091488_BDRWVZ_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'WC - PO LIST',200,'CLOSED_CODE','Closed Code','','','','default','','9','','Y','','','','','','','10012196','N','N','','XXEIS_1091488_BDRWVZ_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'WC - PO LIST',200,'CANCEL_FLAG','Cancel Flag','','','','default','','10','','Y','','','','','','','10012196','N','N','','XXEIS_1091488_BDRWVZ_V','','','','US','');
--Inserting Report Parameters - WC - PO LIST
xxeis.eis_rsc_ins.rp( 'WC - PO LIST',200,'Create Date From','','CREATE_DATE','>=','','','DATE','N','Y','1','','Y','CONSTANT','10012196','Y','N','','','','XXEIS_1091488_BDRWVZ_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC - PO LIST',200,'Vendor Num','','VENDOR_NUM','IN','','','VARCHAR2','N','Y','3','Y','Y','CONSTANT','10012196','Y','N','','','','XXEIS_1091488_BDRWVZ_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC - PO LIST',200,'Create Date To','','CREATE_DATE','<=','','','DATE','N','Y','2','','Y','CONSTANT','10012196','Y','N','','','','XXEIS_1091488_BDRWVZ_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC - PO LIST',200,'Po Num','','PO_NUM','>=','','','NUMERIC','N','Y','4','','Y','CONSTANT','10012196','Y','N','','','','XXEIS_1091488_BDRWVZ_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC - PO LIST',200,'Cancel Flag','','CANCEL_FLAG','IN','','','VARCHAR2','N','Y','5','Y','Y','CONSTANT','10012196','Y','N','','','','XXEIS_1091488_BDRWVZ_V','','','US','');
--Inserting Dependent Parameters - WC - PO LIST
--Inserting Report Conditions - WC - PO LIST
xxeis.eis_rsc_ins.rcnh( 'WC - PO LIST',200,'CANCEL_FLAG IN :Cancel Flag ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CANCEL_FLAG','','Cancel Flag','','','','','XXEIS_1091488_BDRWVZ_V','','','','','','IN','Y','Y','','','','','1',200,'WC - PO LIST','CANCEL_FLAG IN :Cancel Flag ');
xxeis.eis_rsc_ins.rcnh( 'WC - PO LIST',200,'CREATE_DATE >= :Create Date From ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CREATE_DATE','','Create Date From','','','','','XXEIS_1091488_BDRWVZ_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',200,'WC - PO LIST','CREATE_DATE >= :Create Date From ');
xxeis.eis_rsc_ins.rcnh( 'WC - PO LIST',200,'CREATE_DATE <= :Create Date To ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CREATE_DATE','','Create Date To','','','','','XXEIS_1091488_BDRWVZ_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',200,'WC - PO LIST','CREATE_DATE <= :Create Date To ');
xxeis.eis_rsc_ins.rcnh( 'WC - PO LIST',200,'PO_NUM >= :Po Num ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','PO_NUM','','Po Num','','','','','XXEIS_1091488_BDRWVZ_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',200,'WC - PO LIST','PO_NUM >= :Po Num ');
xxeis.eis_rsc_ins.rcnh( 'WC - PO LIST',200,'VENDOR_NUM IN :Vendor Num ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','VENDOR_NUM','','Vendor Num','','','','','XXEIS_1091488_BDRWVZ_V','','','','','','IN','Y','Y','','','','','1',200,'WC - PO LIST','VENDOR_NUM IN :Vendor Num ');
--Inserting Report Sorts - WC - PO LIST
--Inserting Report Triggers - WC - PO LIST
--inserting report templates - WC - PO LIST
--Inserting Report Portals - WC - PO LIST
--inserting report dashboards - WC - PO LIST
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'WC - PO LIST','200','XXEIS_1091488_BDRWVZ_V','XXEIS_1091488_BDRWVZ_V','N','');
--inserting report security - WC - PO LIST
xxeis.eis_rsc_ins.rsec( 'WC - PO LIST','200','','XXWC_PAY_VENDOR_MSTR_INQUIRY',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - PO LIST','200','','XXWC_PAY_VENDOR_MSTR',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - PO LIST','200','','XXWC_PAY_VENDOR_BANK_DETAILS',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - PO LIST','200','','XXWC_PAY_MANAGER',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - PO LIST','200','','XXWC_PAYABLES_INQUIRY',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - PO LIST','200','','XXWC_PAY_DISBURSE',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - PO LIST','200','','XXWC_PAY_W_CALENDAR',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - PO LIST','200','','XXWC_PAY_NO_CALENDAR',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - PO LIST','','10011203','',200,'10012196','','','');
--Inserting Report Pivots - WC - PO LIST
--Inserting Report   Version details- WC - PO LIST
xxeis.eis_rsc_ins.rv( 'WC - PO LIST','','WC - PO LIST','AB063501');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
