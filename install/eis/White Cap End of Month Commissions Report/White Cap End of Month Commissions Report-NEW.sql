--Report Name            : White Cap End of Month Commissions Report-NEW
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View EIS_XXWC_AR_TBD_OUTBND_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_AR_TBD_OUTBND_V
Create or replace View XXEIS.EIS_XXWC_AR_TBD_OUTBND_V
 AS 
SELECT
    gd.gl_date businessdate,
    CASE
      WHEN msi.segment1 = 'CONTBILL'
      THEN 'FabRebar'
      WHEN ( mic.category_concat_segs IN ('99.99RT', '99.99RR'))
        --OR upper(MSI.segment1) LIKE 'R%')
      THEN 'Rental'
      WHEN mic.segment1 IN ('BB', 'BS', 'DC', 'EC', 'EP', 'FA', 'FC', 'JS', 'ME', 'ML', ' MR', 'NS', 'PS', 'RE', 'RL', 'RP', 'SA', 'SC', 'SH', 'TH')
      THEN 'Intangibles'
      WHEN mc.segment1 IN ('GC', 'MD', 'PC', 'PD', 'PR', 'RR', 'RV', 'ST', 'XX')
      THEN 'Non Comm.'
      WHEN (mic.category_concat_segs IN ('FB.OSPI','FB.MODL', 'FB.SUBA', 'FB.OPTC', 'RF.OSPI', 'RF.SERV'))
      OR mic.category_concat_segs    IN ('60.6021', '60.6022', '60.6035')
      THEN 'FabRebar'
      WHEN ( (ol.attribute3 = 'Y')
      OR (ol.attribute15    = 'Y' ))
      THEN 'Blow and Expired'
      ELSE 'Product'
    END report_type,
    oh.order_number order_number,
    ct.trx_number invoicenumber,
    ctl.line_number lineno,
    DECODE(msi.segment1 ,'Rental Charge',DECODE(ol.line_category_code,'RETURN',xxeis.eis_rs_xxwc_com_util_pkg.get_rental_item_desc(ol.reference_line_id), xxeis.eis_rs_xxwc_com_util_pkg.get_rental_item_desc(ol.link_to_line_id)) ,ctl.description) description,
    -- NVL(CTL.QUANTITY_ORDERED,0) QTY,
    -- NVL(ctl.unit_selling_price,0) unitprice,
    -- DECODE(NVL(ctl.unit_selling_price,0),0,0,(NVL(CTL.QUANTITY_ORDERED,0)*(NVL(ctl.unit_selling_price,0)+ xxeis.eis_rs_xxwc_com_util_pkg.Get_MfgAdjust(ol.header_id,ol.line_id)))) ExtSale,
    --  NVL(CTL.QUANTITY_ORDERED,0) QTY,
    NVL (ctl.quantity_invoiced, ctl.quantity_credited) qty,
    --DECODE(CTL.interface_line_attribute2,'RETURN ORDER',CTL.quantity_credited,NVL(quantity_invoiced,NVL(QUANTITY_ORDERED,0)))QTY,
    NVL (ctl.unit_selling_price, 0) unitprice,
    --  DECODE(NVL(ctl.unit_selling_price,0),0,0,(NVL(CTL.QUANTITY_ORDERED,0)*(NVL(ctl.unit_selling_price,0)+ xxeis.eis_rs_xxwc_com_util_pkg.Get_MfgAdjust(ol.header_id,ol.line_id)))) ExtSale,
    NVL ( ctl.extended_amount, DECODE ( NVL (ctl.unit_selling_price, 0), 0, 0, ( NVL (ctl.quantity_invoiced, ctl.quantity_credited) * ( NVL (ctl.unit_selling_price, 0) + xxeis.eis_rs_xxwc_com_util_pkg.get_mfgadjust ( ol.header_id, ol.line_id))))) extsale,
    hca.account_number masternumber,
    hca.account_name mastername,
    hps.party_site_number jobnumber,
    hcsu.location customername,
    jrs.salesrep_number salesrepnumber,
    NVL(jrse.source_name,jrse.resource_name) salesrepname,
    --RA.SALESREP_NUMBER SALESREPNUMBER,
    --RA.NAME SALESREPNAME,
    mp.organization_code loc,
    mp.organization_id mp_organization_id,
    mp.attribute9 regionname,
    DECODE(msi.segment1,'Rental Charge',DECODE(ol.line_category_code,'RETURN',xxeis.eis_rs_xxwc_com_util_pkg.get_rental_item(ol.reference_line_id), xxeis.eis_rs_xxwc_com_util_pkg.get_rental_item(ol.link_to_line_id)) ,msi.segment1) partno,
    MC.SEGMENT2 CATCLASS,
    bs.name invoice_source,
    NVL (ol.shipped_quantity, 0) shipped_quantity,
    DECODE (ol.source_type_code, 'INTERNAL', 0, 'EXTERNAL', 1, ol.source_type_code) directflag,
    --NVL(APPS.CST_COST_API.GET_ITEM_COST(1,MSI.INVENTORY_ITEM_ID,MSI.ORGANIZATION_ID),0) AVERAGECOST,
    /*  CASE
    WHEN Mic.Segment1      IN ('BB', 'BS', 'DC', 'EC', 'EP', 'FA', 'FC', 'JS', 'ME',' ML',' MR', 'NS', 'PS', 'RE', 'RL', 'RP', 'SA','SC','SH','TH')
    AND Upper(Mic.Segment2) ='EC10'
    THEN to_number(Ol.Attribute8)
    WHEN NVL(ctl.unit_selling_price,0) = 0
    THEN 0
    ELSE NVL(Apps.Cst_Cost_Api.Get_Item_Cost(1,Msi.Inventory_Item_Id,Msi.Organization_Id),0)
    end averagecost,*/
    CASE
      WHEN ol.line_type_id = 1008
      THEN NVL(apps.xxwc_mv_routines_pkg.get_order_line_cost (ol.reference_line_id), 0)
      WHEN ol.line_type_id = 1003
      THEN NVL(xxeis.eis_rs_xxwc_com_util_pkg.get_bill_line_cost (oh.header_id,ol.inventory_item_id), 0)
      ELSE NVL(apps.xxwc_mv_routines_pkg.get_order_line_cost (ol.line_id), 0)
    END averagecost,
    DECODE ( xxeis.eis_rs_xxwc_com_util_pkg.get_trader (ol.header_id, ol.line_id), 0, '-', 'Trader') trader,
    NVL ( ROUND ( xxeis.eis_rs_xxwc_com_util_pkg.get_special_cost ( ol.header_id, ol.line_id, ol.inventory_item_id), 2), 0) special_cost,
    hou.name operating_unit,
    msi.list_price_per_unit list_price,
    hou.organization_id org_id,
    msi.inventory_item_id,
    msi.organization_id,
    ct.customer_trx_id,
    HP.PARTY_ID,
  --  xxeis.eis_rs_xxwc_com_util_pkg.get_period_name (gd.set_of_books_id, gd.gl_date) period_name,
    gp.period_name period_name,
    XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_MFGADJUST (OL.HEADER_ID, OL.LINE_ID) MFGADJUST,
    DECODE(JRSE.CATEGORY,'EMPLOYEE','Employee','OTHER','House Acct','House Acct') SALESREP_TYPE,
    JRSE.category SALES_TYPE,
    ctl.INTERFACE_LINE_ATTRIBUTE2,
    fu.description insidesalesrep,
    fu.employee_id insideSRnum,
    srep.prism_num PRISMSRnum,   ---- diane added
    OTT.NAME LINETYPE,      -----diane added
    NVL((select EMP.full_name
      from apps.per_all_people_f emp,
      apps.FND_USER  FU
      where ol.created_by = fu.user_id
      and rownum = 1
      and fu.employee_id  = EMP.person_id), '') HRinsideSRname, ------ diane added
    NVL((select EMP.employee_number
      from apps.per_all_people_f emp,
      apps.FND_USER  FU
      where ol.created_by = fu.user_id
      and rownum = 1
      and fu.employee_id  = EMP.person_id), '') HRinsideSRnum  -------- diane added
    --descr#flexfield#start
    --descr#flexfield#end
    --gl#accountff#start
    --gl#accountff#end
  FROM
    RA_CUSTOMER_TRX           CT,
    ra_customer_trx_lines     ctl,
    RA_CUST_TRX_LINE_GL_DIST  GD,
    HZ_PARTIES                HP,
    HZ_CUST_ACCOUNTS          HCA,
    HZ_PARTY_SITES            HPS,
    HZ_CUST_ACCT_SITES        HCAS,
    HZ_CUST_SITE_USES         HCSU,
    JTF_RS_SALESREPS          JRS,
    jtf_rs_resource_extns_vl  jrse,
       -- RA_SALESREPS_ALL RA,
    mtl_parameters            mp,
    MTL_SYSTEM_ITEMS_KFV      MSI,
    MTL_ITEM_CATEGORIES_V     MIC,
   -- MTL_CATEGORIES_V mc,
    MTL_CATEGORIES_B_KFV      MC,
    OE_ORDER_LINES            OL,
    OE_ORDER_HEADERS          OH,
    HR_OPERATING_UNITS        HOU,
    MTL_CATEGORY_SETS         MDCS,
    RA_BATCH_SOURCES          BS,
    OE_TRANSACTION_TYPES_VL   OEL,
    OE_TRANSACTION_TYPES_VL   OEH,
    FND_USER                  FU,
    GL_PERIODS                GP,
    gl_ledgers                gl,
    xxeis.eis_xxwc_ar_prism_salesrep srep,  ------ diane added
    ont.oe_transaction_types_tl OTT   ---- DIANE ADDED
  WHERE 1                       = 1
  and srep.employee_num(+) = jrs.salesrep_number -------   diane added
  and OL.Line_Type_Id = OTT.TRANSACTION_TYPE_ID     --------DIANE ADDED
  AND GD.CUSTOMER_TRX_ID        = CT.CUSTOMER_TRX_ID
  AND GD.ACCOUNT_CLASS          = 'REC'
  AND CTL.CUSTOMER_TRX_ID        = CT.CUSTOMER_TRX_ID
  AND CT.ORG_ID                 = HOU.ORGANIZATION_ID
  AND GD.ORG_ID                 = HOU.ORGANIZATION_ID
  AND CT.BILL_TO_CUSTOMER_ID    = HCA.CUST_ACCOUNT_ID
  AND HCA.PARTY_ID              = HP.PARTY_ID(+)
  AND HP.PARTY_ID               = HPS.PARTY_ID
  AND HCAS.PARTY_SITE_ID        = HPS.PARTY_SITE_ID
  AND HCSU.CUST_ACCT_SITE_ID    = HCAS.CUST_ACCT_SITE_ID
  AND HCSU.SITE_USE_ID          = CT.BILL_TO_SITE_USE_ID
  AND HCA.CUST_ACCOUNT_ID       = HCAS.CUST_ACCOUNT_ID
  AND ol.salesrep_id            = jrs.salesrep_id
  AND hca.account_number    <> '70200000'
    -- AND msi.segment1 NOT      IN('CONTOFFSET','CONTBILL')
    --AND ct.primary_salesrep_id     = jrs.salesrep_id
  AND OH.ORG_ID                   = JRS.ORG_ID
  AND JRS.RESOURCE_ID             = JRSE.RESOURCE_ID
  --AND CT.INTERFACE_HEADER_CONTEXT = 'ORDER ENTRY'
  AND TO_CHAR (OH.ORDER_NUMBER)   = CT.INTERFACE_HEADER_ATTRIBUTE1
  AND CTL.INTERFACE_LINE_CONTEXT  = 'ORDER ENTRY'
  AND TO_CHAR (OL.LINE_ID)        = CTL.INTERFACE_LINE_ATTRIBUTE6
  --AND OH.ORDER_NUMBER             = CTL.SALES_ORDER
  AND OH.HEADER_ID                = OL.HEADER_ID
  AND msi.inventory_item_id       = ol.inventory_item_id
  AND MSI.ORGANIZATION_ID         = OL.SHIP_FROM_ORG_ID
  AND mic.category_id             = mc.category_id
  AND MSI.ORGANIZATION_ID         = MIC.ORGANIZATION_ID
  AND MSI.INVENTORY_ITEM_ID       = MIC.INVENTORY_ITEM_ID
  --AND CT.ORG_ID                   = HOU.ORGANIZATION_ID
  AND msi.organization_id         = mp.organization_id
  AND MIC.CATEGORY_SET_ID         = MDCS.CATEGORY_SET_ID
  AND ctl.extended_amount       <> 0
  AND MDCS.CATEGORY_SET_NAME      = 'Inventory Category'
  AND CT.BATCH_SOURCE_ID          = BS.BATCH_SOURCE_ID
  AND CT.ORG_ID                   = BS.ORG_ID
 -- AND mc.structure_name          = 'Item Categories'
  AND MC.STRUCTURE_ID             = 101
  AND OEL.TRANSACTION_TYPE_ID     = OL.LINE_TYPE_ID
  AND oeh.transaction_type_id     = oh.order_type_id
  AND OEH.ORG_ID                  = OH.ORG_ID
  AND OEL.ORG_ID                  = OL.ORG_ID
  AND OH.HEADER_ID                = OL.HEADER_ID
  AND OL.CREATED_BY               = FU.USER_ID
 -- and gd.cust_trx_line_gl_dist_id = 45676898
  AND GP.PERIOD_SET_NAME          = GL.PERIOD_SET_NAME
  AND GL.LEDGER_ID                = GD.SET_OF_BOOKS_ID
  AND GL.PERIOD_SET_NAME= '4-4-QTR'
  AND TRUNC(GD.GL_DATE) BETWEEN TRUNC(GP.START_DATE) AND TRUNC(GP.END_DATE)
/
set scan on define on
prompt Creating View Data for White Cap End of Month Commissions Report-NEW
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_AR_TBD_OUTBND_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_AR_TBD_OUTBND_V',222,'','','','','TD002849','XXEIS','Eis Ar Tbd Outbound V','EXATOV','','');
--Delete View Columns for EIS_XXWC_AR_TBD_OUTBND_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_AR_TBD_OUTBND_V',222,FALSE);
--Inserting View Columns for EIS_XXWC_AR_TBD_OUTBND_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','OPERATING_UNIT',222,'Operating Unit','OPERATING_UNIT','','','','TD002849','VARCHAR2','','','Operating Unit','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','AVERAGECOST',222,'Averagecost','AVERAGECOST','','~T~D~2','','TD002849','NUMBER','','','Averagecost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','DIRECTFLAG',222,'Directflag','DIRECTFLAG','','','','TD002849','NUMBER','','','Directflag','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','CATCLASS',222,'Catclass','CATCLASS','','','','TD002849','VARCHAR2','','','Catclass','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','PARTNO',222,'Partno','PARTNO','','','','TD002849','VARCHAR2','','','Partno','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','REGIONNAME',222,'Regionname','REGIONNAME','','','','TD002849','VARCHAR2','','','Regionname','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','LOC',222,'Loc','LOC','','','','TD002849','VARCHAR2','','','Loc','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','SALESREPNAME',222,'Salesrepname','SALESREPNAME','','','','TD002849','VARCHAR2','','','Salesrepname','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','SALESREPNUMBER',222,'Salesrepnumber','SALESREPNUMBER','','','','TD002849','VARCHAR2','','','Salesrepnumber','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','CUSTOMERNAME',222,'Customername','CUSTOMERNAME','','','','TD002849','VARCHAR2','','','Customername','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','JOBNUMBER',222,'Jobnumber','JOBNUMBER','','','','TD002849','VARCHAR2','','','Jobnumber','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','MASTERNAME',222,'Mastername','MASTERNAME','','','','TD002849','VARCHAR2','','','Mastername','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','MASTERNUMBER',222,'Masternumber','MASTERNUMBER','','','','TD002849','VARCHAR2','','','Masternumber','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','EXTSALE',222,'Extsale','EXTSALE','','','','TD002849','NUMBER','','','Extsale','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','UNITPRICE',222,'Unitprice','UNITPRICE','','','','TD002849','NUMBER','','','Unitprice','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','QTY',222,'Qty','QTY','','','','TD002849','NUMBER','','','Qty','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','DESCRIPTION',222,'Description','DESCRIPTION','','','','TD002849','VARCHAR2','','','Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','LINENO',222,'Lineno','LINENO','','','','TD002849','NUMBER','','','Lineno','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','INVOICENUMBER',222,'Invoicenumber','INVOICENUMBER','','','','TD002849','VARCHAR2','','','Invoicenumber','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','BUSINESSDATE',222,'Businessdate','BUSINESSDATE','','','','TD002849','DATE','','','Businessdate','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','TRADER',222,'Trader','TRADER','','','','TD002849','VARCHAR2','','','Trader','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','INVOICE_SOURCE',222,'Invoice Source','INVOICE_SOURCE','','','','TD002849','VARCHAR2','','','Invoice Source','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','REPORT_TYPE',222,'Report Type','REPORT_TYPE','','','','TD002849','VARCHAR2','','','Report Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','PERIOD_NAME',222,'Period Name','PERIOD_NAME','','','','TD002849','VARCHAR2','','','Period Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','SHIPPED_QUANTITY',222,'Shipped Quantity','SHIPPED_QUANTITY','','','','TD002849','NUMBER','','','Shipped Quantity','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','ORDER_NUMBER',222,'Order Number','ORDER_NUMBER','','','','TD002849','NUMBER','','','Order Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','MFGADJUST',222,'Mfgadjust','MFGADJUST','','','','TD002849','NUMBER','','','Mfgadjust','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','CUSTOMER_TRX_ID',222,'Customer Trx Id','CUSTOMER_TRX_ID','','','','TD002849','NUMBER','','','Customer Trx Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','ORG_ID',222,'Org Id','ORG_ID','','','','TD002849','NUMBER','','','Org Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','PARTY_ID',222,'Party Id','PARTY_ID','','','','TD002849','NUMBER','','','Party Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','SALESREP_TYPE',222,'Salesrep Type','SALESREP_TYPE','','','','TD002849','VARCHAR2','','','Salesrep Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','INTERFACE_LINE_ATTRIBUTE2',222,'Interface Line Attribute2','INTERFACE_LINE_ATTRIBUTE2','','','','TD002849','VARCHAR2','','','Interface Line Attribute2','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','INSIDESALESREP',222,'Insidesalesrep','INSIDESALESREP','','','','TD002849','VARCHAR2','','','Insidesalesrep','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','ATTRIBUTE7',222,'Attribute7','ATTRIBUTE7','','','','TD002849','VARCHAR2','','','Attribute7','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','PRISMSRNUM',222,'Prismsrnum','PRISMSRNUM','','','','TD002849','VARCHAR2','','','Prismsrnum','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','LINETYPE',222,'Linetype','LINETYPE','','','','TD002849','VARCHAR2','','','Linetype','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','ORGANIZATION_ID',222,'Organization Id','ORGANIZATION_ID','','','','TD002849','NUMBER','','','Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','INVENTORY_ITEM_ID',222,'Inventory Item Id','INVENTORY_ITEM_ID','','','','TD002849','NUMBER','','','Inventory Item Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','MP_ORGANIZATION_ID',222,'Mp Organization Id','MP_ORGANIZATION_ID','','','','TD002849','NUMBER','','','Mp Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','SPECIAL_COST',222,'Special Cost','SPECIAL_COST','','~T~D~2','','TD002849','NUMBER','','','Special Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','SALES_TYPE',222,'Sales Type','SALES_TYPE','','','','TD002849','VARCHAR2','','','Sales Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','LIST_PRICE',222,'List Price','LIST_PRICE','','','','TD002849','NUMBER','','','List Price','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','INSIDESRNUM',222,'Insidesrnum','INSIDESRNUM','','','','TD002849','NUMBER','','','Insidesrnum','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','HRINSIDESRNAME',222,'Hrinsidesrname','HRINSIDESRNAME','','','','TD002849','VARCHAR2','','','Hrinsidesrname','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','HRINSIDESRNUM',222,'Hrinsidesrnum','HRINSIDESRNUM','','','','TD002849','VARCHAR2','','','Hrinsidesrnum','','','');
--Inserting View Components for EIS_XXWC_AR_TBD_OUTBND_V
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_AR_TBD_OUTBND_V','RA_CUSTOMER_TRX',222,'RA_CUSTOMER_TRX_ALL','CT','CT','TD002849','TD002849','-1','Header-Level Information About Invoices, Debit Mem','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_AR_TBD_OUTBND_V','MTL_SYSTEM_ITEMS_KFV',222,'MTL_SYSTEM_ITEMS_B','MSI','MSI','TD002849','TD002849','-1','Inventory Item Definitions','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_AR_TBD_OUTBND_V','HZ_PARTIES',222,'HZ_PARTIES','HP','HP','TD002849','TD002849','-1','Party Information','','','','');
--Inserting View Component Joins for EIS_XXWC_AR_TBD_OUTBND_V
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_AR_TBD_OUTBND_V','RA_CUSTOMER_TRX','CT',222,'EXATOV.CUSTOMER_TRX_ID','=','CT.CUSTOMER_TRX_ID(+)','','','','Y','TD002849','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_AR_TBD_OUTBND_V','MTL_SYSTEM_ITEMS_KFV','MSI',222,'EXATOV.INVENTORY_ITEM_ID','=','MSI.INVENTORY_ITEM_ID(+)','','','','','TD002849','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_AR_TBD_OUTBND_V','MTL_SYSTEM_ITEMS_KFV','MSI',222,'EXATOV.ORGANIZATION_ID','=','MSI.ORGANIZATION_ID(+)','','','','','TD002849','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_AR_TBD_OUTBND_V','HZ_PARTIES','HP',222,'EXATOV.PARTY_ID','=','HP.PARTY_ID(+)','','','','','TD002849','','');
END;
/
set scan on define on
prompt Creating Report LOV Data for White Cap End of Month Commissions Report-NEW
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - White Cap End of Month Commissions Report-NEW
xxeis.eis_rs_ins.lov( 222,'select  per.period_name , led.name ledger_name, per.period_year, per.period_num, per.start_date, per.end_date
from    gl_periods per,gl_ledgers led
where  per.period_set_name = led.period_set_name
and     GL_SECURITY_PKG.VALIDATE_ACCESS(led.ledger_ID) = ''TRUE''
and     led.accounted_period_type = per.period_type','','AR_PERIOD_NAMES','AR_PERIOD_NAMES','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 222,'SELECT hou.NAME organization_name,
       TO_CHAR (date_from,
                NVL (fnd_profile.VALUE (''ICX_DATE_FORMAT_MASK''),
                     ''DD-MON-YYYY'')
               ) date_from,
       TO_CHAR (date_to,
                NVL (fnd_profile.VALUE (''ICX_DATE_FORMAT_MASK''),
                     ''DD-MON-YYYY'')
               ) date_to
  FROM hr_operating_units hou,
       mo_glob_org_access_tmp tmp
 WHERE hou.organization_id = tmp.organization_id','','EIS_MULTI_OPERATING_UNIT_LOV','This LOV shows all the operating units, to which user has access.','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 222,'select DISTINCT NAME from RA_BATCH_SOURCES_ALL','','AR Batch Source Name LOV','Displays Batch Sources information','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 222,'select distinct DECODE(B.category,''EMPLOYEE'',''Employee'',''OTHER'',''House Acct'',''House Acct'') type , B.category 
from  JTF.JTF_RS_Resource_Extns_TL b','','SalesRepType','','ANONYMOUS',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for White Cap End of Month Commissions Report-NEW
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - White Cap End of Month Commissions Report-NEW
xxeis.eis_rs_utility.delete_report_rows( 'White Cap End of Month Commissions Report-NEW' );
--Inserting Report - White Cap End of Month Commissions Report-NEW
xxeis.eis_rs_ins.r( 222,'White Cap End of Month Commissions Report-NEW','','The purpose of this report is to extract the necessary information on posted sales to enable the accurate calculation of commissions to White Cap Sales Representatives for a defined period of time (e.g. one month). Note:  The parameter �GL Period to Report� correlates to the range of dates that will be reported (e.g. select APR-20yy) will report all transactions within the respective GL period. 

','','','','TD002849','EIS_XXWC_AR_TBD_OUTBND_V','Y','','','TD002849','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - White Cap End of Month Commissions Report-NEW
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report-NEW',222,'PRISMSRNUM','Prismsrnum','Prismsrnum','','','','','35','N','','','','','','','','TD002849','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report-NEW',222,'BUSINESSDATE','BusinessDate','Businessdate','','','default','','2','N','','','','','','','','TD002849','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report-NEW',222,'CATCLASS','CatClass','Catclass','','','default','','11','N','','','','','','','','TD002849','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report-NEW',222,'CUSTOMERNAME','CustomerName','Customername','','','default','','29','N','','','','','','','','TD002849','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report-NEW',222,'DESCRIPTION','Description','Description','','','default','','14','N','','','','','','','','TD002849','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report-NEW',222,'DIRECTFLAG','DirectFlag','Directflag','','~~~','default','','28','N','','','','','','','','TD002849','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report-NEW',222,'INVOICENUMBER','InvoiceNumber','Invoicenumber','','','default','','5','N','','','','','','','','TD002849','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report-NEW',222,'JOBNUMBER','JobNumber','Jobnumber','','','default','','4','N','','','','','','','','TD002849','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report-NEW',222,'LINENO','LineNo','Lineno','','~~~','default','','12','N','','','','','','','','TD002849','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report-NEW',222,'LOC','Loc','Loc','','','default','','9','N','','','','','','','','TD002849','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report-NEW',222,'MASTERNAME','MasterName','Mastername','','','default','','30','N','','','','','','','','TD002849','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report-NEW',222,'MASTERNUMBER','MasterNumber','Masternumber','','','default','','3','N','','','','','','','','TD002849','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report-NEW',222,'PARTNO','PartNo','Partno','','','default','','13','N','','','','','','','','TD002849','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report-NEW',222,'QTY','QTY','Qty','','~~~','default','','15','N','','','','','','','','TD002849','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report-NEW',222,'REGIONNAME','RegionName','Regionname','','','default','','10','N','','','','','','','','TD002849','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report-NEW',222,'SALESREPNAME','SalesRepName','Salesrepname','','','default','','8','N','','','','','','','','TD002849','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report-NEW',222,'SALESREPNUMBER','SalesRepNumber','Salesrepnumber','','','default','','7','N','','','','','','','','TD002849','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report-NEW',222,'UNITPRICE','UnitPrice','Unitprice','','~T~D~2','default','','16','N','','','','','','','','TD002849','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report-NEW',222,'TRADER','Trader','Trader','','','default','','27','N','','','','','','','','TD002849','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report-NEW',222,'REPORT_TYPE','ReportTypeName','Report Type','','','default','','1','N','','','','','','','','TD002849','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report-NEW',222,'EXT_AVG_COST','ExtAvgCost','Report Type','NUMBER','~T~D~2','default','','21','Y','','','','','','','((CASE WHEN EXATOV.AVERAGECOST = 0 AND EXATOV.REPORT_TYPE =''Product'' THEN nvl(EXATOV.list_price,0) ELSE EXATOV.AVERAGECOST END)   * EXATOV.QTY)','TD002849','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report-NEW',222,'GM_AVG_COST','GMAvgCost','Report Type','NUMBER','~T~D~2','default','','23','Y','','','','','','','decode(EXATOV.ExtSale ,0,0,((EXATOV.ExtSale - ((CASE WHEN EXATOV.AVERAGECOST = 0 AND EXATOV.REPORT_TYPE =''Product'' THEN nvl(EXATOV.list_price,0) ELSE EXATOV.AVERAGECOST END) * EXATOV.QTY)) / EXATOV.ExtSale))','TD002849','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report-NEW',222,'EXT_SPECIAL_COST','ExtSpecialCost','Report Type','NUMBER','~T~D~2','default','','24','Y','','','','','','','(EXATOV.SPECIAL_COST * EXATOV.QTY)','TD002849','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report-NEW',222,'GPS_SPECIAL_COST','GPSpecialCost','Report Type','NUMBER','~T~D~2','default','','25','Y','','','','','','','DECODE((EXATOV.SPECIAL_COST * EXATOV.QTY),0,0,(EXATOV.ExtSale - (EXATOV.SPECIAL_COST * EXATOV.QTY)))','TD002849','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report-NEW',222,'GMS_SPECIAL_COST','GMSpecialCost','Report Type','NUMBER','~T~D~2','default','','26','Y','','','','','','','DECODE((EXATOV.SPECIAL_COST * EXATOV.QTY),0,0,((EXATOV.ExtSale - (EXATOV.SPECIAL_COST * EXATOV.QTY))/(EXATOV.ExtSale)))','TD002849','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report-NEW',222,'GP_AVG_COST','GPAvgCost','Report Type','NUMBER','~T~D~2','default','','22','Y','','','','','','','(EXATOV.ExtSale - ((CASE WHEN EXATOV.AVERAGECOST = 0 AND EXATOV.REPORT_TYPE =''Product'' THEN nvl(EXATOV.list_price,0) ELSE EXATOV.AVERAGECOST END) * EXATOV.QTY))','TD002849','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report-NEW',222,'MFGADJUST','Mfgadjust','Mfgadjust','','~T~D~2','default','','17','N','','','','','','','','TD002849','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report-NEW',222,'EXTSALE','Extsale','Extsale','','~T~D~2','default','','18','N','','','','','','','','TD002849','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report-NEW',222,'ORDER_NUMBER','Order Number','Order Number','','~~~','default','','31','N','','','','','','','','TD002849','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report-NEW',222,'SALESREP_TYPE','SalesRep Type','Salesrep Type','','','default','','6','N','','','','','','','','TD002849','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report-NEW',222,'AVERAGECOST','AverageCost','Averagecost','NUMBER','~T~D~2','default','','20','Y','','','','','','','(CASE WHEN EXATOV.AVERAGECOST = 0 AND EXATOV.REPORT_TYPE =''Product'' THEN nvl(EXATOV.list_price,0) ELSE EXATOV.AVERAGECOST END)','TD002849','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report-NEW',222,'INTERFACE_LINE_ATTRIBUTE2','SalesOrder Type','Interface Line Attribute2','','','default','','32','N','','','','','','','','TD002849','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report-NEW',222,'INSIDESALESREP','InsideSalesRep','Insidesalesrep','','','default','','33','N','','','','','','','','TD002849','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report-NEW',222,'LINETYPE','Linetype','Linetype','','','','','34','N','','','','','','','','TD002849','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report-NEW',222,'HRINSIDESRNAME','Hrinsidesrname','Hrinsidesrname','','','','','36','N','','','','','','','','TD002849','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report-NEW',222,'HRINSIDESRNUM','Hrinsidesrnum','Hrinsidesrnum','','','','','37','N','','','','','','','','TD002849','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
--Inserting Report Parameters - White Cap End of Month Commissions Report-NEW
xxeis.eis_rs_ins.rp( 'White Cap End of Month Commissions Report-NEW',222,'GL Period to Report','GL Period to Report','PERIOD_NAME','IN','AR_PERIOD_NAMES','select gps.period_name from gl_period_statuses gps where gps.application_id = 101  and gps.set_of_books_id = fnd_profile.value(''GL_SET_OF_BKS_ID'') and trunc(sysdate) between gps.start_date and gps.end_date','VARCHAR2','Y','Y','3','','Y','SQL','TD002849','Y','N','','','');
xxeis.eis_rs_ins.rp( 'White Cap End of Month Commissions Report-NEW',222,'Order Line Quantity Shipped','Order Line Quantity Shipped','SHIPPED_QUANTITY','IN','','','NUMERIC','N','N','5','','Y','CONSTANT','TD002849','N','N','','','');
xxeis.eis_rs_ins.rp( 'White Cap End of Month Commissions Report-NEW',222,'Operating Unit','Operating Unit','OPERATING_UNIT','IN','EIS_MULTI_OPERATING_UNIT_LOV','select name from hr_operating_units where organization_id=fnd_profile.value(''ORG_ID'')','VARCHAR2','Y','Y','1','','Y','SQL','TD002849','Y','N','','','');
xxeis.eis_rs_ins.rp( 'White Cap End of Month Commissions Report-NEW',222,'Order Line Extended Amount','Order Line Extended Amount','EXTSALE','IN','','','NUMERIC','N','N','6','','Y','CONSTANT','TD002849','N','N','','','');
xxeis.eis_rs_ins.rp( 'White Cap End of Month Commissions Report-NEW',222,'Order Line Extended Cost','Order Line Extended Cost','AVERAGECOST','IN','','','NUMERIC','N','N','7','','Y','CONSTANT','TD002849','N','N','','','');
xxeis.eis_rs_ins.rp( 'White Cap End of Month Commissions Report-NEW',222,'Invoice Source','Invoice Source','INVOICE_SOURCE','IN','AR Batch Source Name LOV','''ORDER MANAGEMENT'',''STANDARD OM SOURCE'',''REPAIR OM SOURCE'',''WC MANUAL''','VARCHAR2','Y','Y','2','','Y','CONSTANT','TD002849','Y','N','','','');
xxeis.eis_rs_ins.rp( 'White Cap End of Month Commissions Report-NEW',222,'SalesRepType','SalesRepType','SALESREP_TYPE','IN','SalesRepType','','VARCHAR2','N','Y','4','','Y','CONSTANT','TD002849','Y','N','','','');
--Inserting Report Conditions - White Cap End of Month Commissions Report-NEW
xxeis.eis_rs_ins.rcn( 'White Cap End of Month Commissions Report-NEW',222,'SHIPPED_QUANTITY','IN',':Order Line Quantity Shipped','','','Y','5','Y','TD002849');
xxeis.eis_rs_ins.rcn( 'White Cap End of Month Commissions Report-NEW',222,'EXTSALE','IN',':Order Line Extended Amount','','','Y','6','Y','TD002849');
xxeis.eis_rs_ins.rcn( 'White Cap End of Month Commissions Report-NEW',222,'AVERAGECOST','IN',':Order Line Extended Cost','','','Y','7','Y','TD002849');
xxeis.eis_rs_ins.rcn( 'White Cap End of Month Commissions Report-NEW',222,'INVOICE_SOURCE','IN',':Invoice Source','','','Y','2','Y','TD002849');
xxeis.eis_rs_ins.rcn( 'White Cap End of Month Commissions Report-NEW',222,'OPERATING_UNIT','IN',':Operating Unit','','','Y','1','Y','TD002849');
xxeis.eis_rs_ins.rcn( 'White Cap End of Month Commissions Report-NEW',222,'PERIOD_NAME','IN',':GL Period to Report','','','Y','3','Y','TD002849');
xxeis.eis_rs_ins.rcn( 'White Cap End of Month Commissions Report-NEW',222,'SALESREP_TYPE','IN',':SalesRepType','','','Y','4','Y','TD002849');
--Inserting Report Sorts - White Cap End of Month Commissions Report-NEW
xxeis.eis_rs_ins.rs( 'White Cap End of Month Commissions Report-NEW',222,'SALESREPNUMBER','ASC','TD002849','','');
xxeis.eis_rs_ins.rs( 'White Cap End of Month Commissions Report-NEW',222,'REPORT_TYPE','ASC','TD002849','','');
--Inserting Report Triggers - White Cap End of Month Commissions Report-NEW
--Inserting Report Templates - White Cap End of Month Commissions Report-NEW
--Inserting Report Portals - White Cap End of Month Commissions Report-NEW
--Inserting Report Dashboards - White Cap End of Month Commissions Report-NEW
--Inserting Report Security - White Cap End of Month Commissions Report-NEW
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report-NEW','222','','50894',222,'TD002849','','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report-NEW','','SE012733','',222,'TD002849','','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report-NEW','','RB054040','',222,'TD002849','','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report-NEW','','DM027741','',222,'TD002849','','');
--Inserting Report Pivots - White Cap End of Month Commissions Report-NEW
xxeis.eis_rs_ins.rpivot( 'White Cap End of Month Commissions Report-NEW',222,'Pivot','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rs_ins.rpivot_dtls( 'White Cap End of Month Commissions Report-NEW',222,'Pivot','LOC','COL_FIELD','','Branch Id','','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'White Cap End of Month Commissions Report-NEW',222,'Pivot','SALESREPNUMBER','ROW_FIELD','','','','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'White Cap End of Month Commissions Report-NEW',222,'Pivot','EXTSALE','DATA_FIELD','SUM','','','1','xlNormal');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
xxeis.eis_rs_ins.rpivot( 'White Cap End of Month Commissions Report-NEW',222,'Total Sales by SalesRep and Bra','2','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Total Sales by SalesRep and Bra
xxeis.eis_rs_ins.rpivot_dtls( 'White Cap End of Month Commissions Report-NEW',222,'Total Sales by SalesRep and Bra','LOC','ROW_FIELD','','','1','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'White Cap End of Month Commissions Report-NEW',222,'Total Sales by SalesRep and Bra','SALESREPNUMBER','ROW_FIELD','','','2','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'White Cap End of Month Commissions Report-NEW',222,'Total Sales by SalesRep and Bra','EXTSALE','DATA_FIELD','SUM','','1','','');
--Inserting Report Summary Calculation Columns For Pivot- Total Sales by SalesRep and Bra
END;
/
set scan on define on
