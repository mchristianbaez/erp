--Report Name            : HDS AP Accounting lines with image link - Vendor
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data XXEIS_VENDOR_IMAGE_LINK
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Inserting Object XXEIS_VENDOR_IMAGE_LINK
xxeis.eis_rsc_ins.v( 'XXEIS_VENDOR_IMAGE_LINK',200,'Paste SQL View for VENDOR IMAGE LINK','1.0','','','XXEIS_RS_ADMIN','XXEIS','XXEIS_VENDOR_IMAGE_LINK','XVIL','','','VIEW','US','','');
--Delete Object Columns for XXEIS_VENDOR_IMAGE_LINK
xxeis.eis_rsc_utility.delete_view_rows('XXEIS_VENDOR_IMAGE_LINK',200,FALSE);
--Inserting Object Columns for XXEIS_VENDOR_IMAGE_LINK
xxeis.eis_rsc_ins.vc( 'XXEIS_VENDOR_IMAGE_LINK','ACCOUNT',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Account','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_VENDOR_IMAGE_LINK','CHECK_DATE',200,'','','','','','XXEIS_RS_ADMIN','DATE','','','Check Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_VENDOR_IMAGE_LINK','CHECK_NUMBER',200,'','','','','','XXEIS_RS_ADMIN','NUMBER','','','Check Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_VENDOR_IMAGE_LINK','GL_DATE',200,'','','','','','XXEIS_RS_ADMIN','DATE','','','Gl Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_VENDOR_IMAGE_LINK','IMAGE_LINK',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Image Link','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_VENDOR_IMAGE_LINK','INVOICE_DATE',200,'','','','','','XXEIS_RS_ADMIN','DATE','','','Invoice Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_VENDOR_IMAGE_LINK','INVOICE_NUM',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Invoice Num','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_VENDOR_IMAGE_LINK','LOCATION',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Location','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_VENDOR_IMAGE_LINK','VENDOR_NAME',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_VENDOR_IMAGE_LINK','VENDOR_NUMBER',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_VENDOR_IMAGE_LINK','CHECK_AMOUNT',200,'','','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Check Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_VENDOR_IMAGE_LINK','DISTRIBUTION_AMOUNT',200,'','','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Distribution Amount','','','','US');
--Inserting Object Components for XXEIS_VENDOR_IMAGE_LINK
--Inserting Object Component Joins for XXEIS_VENDOR_IMAGE_LINK
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
--There are no LOV/LOV TL entries for the report HDS AP Accounting lines with image link - Vendor
prompt Creating Report Data for HDS AP Accounting lines with image link - Vendor
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Deleting Report data - HDS AP Accounting lines with image link - Vendor
xxeis.eis_rsc_utility.delete_report_rows( 'HDS AP Accounting lines with image link - Vendor' );
--Inserting Report - HDS AP Accounting lines with image link - Vendor
xxeis.eis_rsc_ins.r( 200,'HDS AP Accounting lines with image link - Vendor','','HDS AP Accounting lines with image link - Vendor','','','','XXEIS_RS_ADMIN','XXEIS_VENDOR_IMAGE_LINK','Y','','','XXEIS_RS_ADMIN','','N','HDS Standard Reports','','CSV,EXCEL,Pivot Excel,','Y','','','','','','N','','US','','','','');
--Inserting Report Columns - HDS AP Accounting lines with image link - Vendor
xxeis.eis_rsc_ins.rc( 'HDS AP Accounting lines with image link - Vendor',200,'ACCOUNT','Account','','','','default','','5','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_VENDOR_IMAGE_LINK','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP Accounting lines with image link - Vendor',200,'CHECK_AMOUNT','Check Amount','','','~T~D~2','default','','12','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_VENDOR_IMAGE_LINK','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP Accounting lines with image link - Vendor',200,'CHECK_DATE','Check Date','','','','default','','11','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_VENDOR_IMAGE_LINK','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP Accounting lines with image link - Vendor',200,'CHECK_NUMBER','Check Number','','','~~~','default','','10','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_VENDOR_IMAGE_LINK','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP Accounting lines with image link - Vendor',200,'GL_DATE','Gl Date','','','','default','','3','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_VENDOR_IMAGE_LINK','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP Accounting lines with image link - Vendor',200,'IMAGE_LINK','Image Link','','','','default','','13','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_VENDOR_IMAGE_LINK','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP Accounting lines with image link - Vendor',200,'INVOICE_DATE','Invoice Date','','','','default','','7','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_VENDOR_IMAGE_LINK','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP Accounting lines with image link - Vendor',200,'INVOICE_NUM','Invoice Num','','','','default','','6','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_VENDOR_IMAGE_LINK','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP Accounting lines with image link - Vendor',200,'LOCATION','Location','','','','default','','4','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_VENDOR_IMAGE_LINK','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP Accounting lines with image link - Vendor',200,'VENDOR_NAME','Vendor Name','','','','default','','1','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_VENDOR_IMAGE_LINK','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP Accounting lines with image link - Vendor',200,'VENDOR_NUMBER','Vendor Number','','','','default','','2','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_VENDOR_IMAGE_LINK','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP Accounting lines with image link - Vendor',200,'DISTRIBUTION_AMOUNT','Distribution_Amount','','NUMBER','~T~D~2','default','','9','Y','Y','','','','','','SUM(XVIL.DISTRIBUTION_AMOUNT)','XXEIS_RS_ADMIN','N','N','','XXEIS_VENDOR_IMAGE_LINK','','','','US','');
--Inserting Report Parameters - HDS AP Accounting lines with image link - Vendor
xxeis.eis_rsc_ins.rp( 'HDS AP Accounting lines with image link - Vendor',200,'Vendor Name','','VENDOR_NAME','IN','','','VARCHAR2','N','Y','1','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','XXEIS_VENDOR_IMAGE_LINK','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS AP Accounting lines with image link - Vendor',200,'Start Date','','GL_DATE','>=','','','DATE','N','Y','2','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','Start Date','','XXEIS_VENDOR_IMAGE_LINK','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS AP Accounting lines with image link - Vendor',200,'End Date','','GL_DATE','<=','','','DATE','N','Y','3','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','End Date','','XXEIS_VENDOR_IMAGE_LINK','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS AP Accounting lines with image link - Vendor',200,'Location','','LOCATION','IN','','','VARCHAR2','N','Y','4','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','XXEIS_VENDOR_IMAGE_LINK','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS AP Accounting lines with image link - Vendor',200,'Account','','ACCOUNT','IN','','','VARCHAR2','N','Y','5','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','XXEIS_VENDOR_IMAGE_LINK','','','US','');
--Inserting Dependent Parameters - HDS AP Accounting lines with image link - Vendor
--Inserting Report Conditions - HDS AP Accounting lines with image link - Vendor
xxeis.eis_rsc_ins.rcnh( 'HDS AP Accounting lines with image link - Vendor',200,'(UPPER(VENDOR_NAME)) IN :Vendor Name ','ADVANCED','','1#$#','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','Vendor Name','','','','','','','','','','','IN','Y','Y','(UPPER(VENDOR_NAME))','','','','1',200,'HDS AP Accounting lines with image link - Vendor','(UPPER(VENDOR_NAME)) IN :Vendor Name ');
xxeis.eis_rsc_ins.rcnh( 'HDS AP Accounting lines with image link - Vendor',200,'ACCOUNT IN :Account ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','ACCOUNT','','Account','','','','','XXEIS_VENDOR_IMAGE_LINK','','','','','','IN','Y','Y','','','','','1',200,'HDS AP Accounting lines with image link - Vendor','ACCOUNT IN :Account ');
xxeis.eis_rsc_ins.rcnh( 'HDS AP Accounting lines with image link - Vendor',200,'GL_DATE BETWEEN :Start Date  :End Date','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','GL_DATE','','Start Date','','','','','XXEIS_VENDOR_IMAGE_LINK','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',200,'HDS AP Accounting lines with image link - Vendor','GL_DATE BETWEEN :Start Date  :End Date');
xxeis.eis_rsc_ins.rcnh( 'HDS AP Accounting lines with image link - Vendor',200,'LOCATION IN :Location ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','LOCATION','','Location','','','','','XXEIS_VENDOR_IMAGE_LINK','','','','','','IN','Y','Y','','','','','1',200,'HDS AP Accounting lines with image link - Vendor','LOCATION IN :Location ');
xxeis.eis_rsc_ins.rcnh( 'HDS AP Accounting lines with image link - Vendor',200,'Free Text ','FREE_TEXT','','','N','');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','','Y','','','','','GROUP BY XVIL.VENDOR_NAME
,XVIL.VENDOR_NUMBER
,XVIL.GL_DATE
,XVIL.LOCATION
,XVIL.ACCOUNT
,XVIL.INVOICE_NUM
,XVIL.INVOICE_DATE
,XVIL.CHECK_AMOUNT
,XVIL.CHECK_NUMBER
,XVIL.CHECK_DATE
,XVIL.IMAGE_LINK      ','1',200,'HDS AP Accounting lines with image link - Vendor','Free Text ');
xxeis.eis_rsc_ins.rcnh( 'HDS AP Accounting lines with image link - Vendor',200,'XVIL.VENDOR_NAME IN Vendor Name','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','VENDOR_NAME','','Vendor Name','','','','','XXEIS_VENDOR_IMAGE_LINK','','','','','','IN','Y','Y','','','','','1',200,'HDS AP Accounting lines with image link - Vendor','XVIL.VENDOR_NAME IN Vendor Name');
xxeis.eis_rsc_ins.rcnh( 'HDS AP Accounting lines with image link - Vendor',200,'XVIL.GL_DATE <= End Date','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','GL_DATE','','End Date','','','','','XXEIS_VENDOR_IMAGE_LINK','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',200,'HDS AP Accounting lines with image link - Vendor','XVIL.GL_DATE <= End Date');
--Inserting Report Sorts - HDS AP Accounting lines with image link - Vendor
--Inserting Report Triggers - HDS AP Accounting lines with image link - Vendor
--inserting report templates - HDS AP Accounting lines with image link - Vendor
--Inserting Report Portals - HDS AP Accounting lines with image link - Vendor
--inserting report dashboards - HDS AP Accounting lines with image link - Vendor
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'HDS AP Accounting lines with image link - Vendor','200','XXEIS_VENDOR_IMAGE_LINK','XXEIS_VENDOR_IMAGE_LINK','N','');
--inserting report security - HDS AP Accounting lines with image link - Vendor
xxeis.eis_rsc_ins.rsec( 'HDS AP Accounting lines with image link - Vendor','200','','HDS_AP_ADMIN_US_GSCIWO',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Accounting lines with image link - Vendor','200','','HDS_AP_ADMIN_US_IWO',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Accounting lines with image link - Vendor','200','','HDS_AP_DISBUREMTS_US_IWO',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Accounting lines with image link - Vendor','200','','HDS_AP_INQUIRY_US_IWO',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Accounting lines with image link - Vendor','200','','HDS_AP_TRNS_ENTRY_US_IWO',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Accounting lines with image link - Vendor','200','','HDS_AP_MGR_NOSUP_US_IWO',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Accounting lines with image link - Vendor','200','','HDS_AP_SUPPLIER_MAINT_US_IWO',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Accounting lines with image link - Vendor','200','','PAYABLES_MANAGER',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Accounting lines with image link - Vendor','200','','HDS_AP_ADMIN_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Accounting lines with image link - Vendor','200','','HDS_AP_DISBURSEMTS_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Accounting lines with image link - Vendor','200','','HDS_AP_INQUIRY_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Accounting lines with image link - Vendor','200','','HDS_AP_MGR_NOSUP_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Accounting lines with image link - Vendor','200','','HDS_AP_SUPPLIER_MAINT_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Accounting lines with image link - Vendor','200','','HDS_AP_TRNS_ENTRY_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Accounting lines with image link - Vendor','200','','HDS_OIE_USER',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Accounting lines with image link - Vendor','200','','PAYABLES_INQUIRY',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Accounting lines with image link - Vendor','200','','XXWC_PAY_NO_CALENDAR',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Accounting lines with image link - Vendor','200','','XXWC_PAY_W_CALENDAR',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Accounting lines with image link - Vendor','200','','XXWC_PAY_DISBURSE',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Accounting lines with image link - Vendor','200','','XXWC_PAYABLES_INQUIRY',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Accounting lines with image link - Vendor','200','','XXWC_PAY_MANAGER',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Accounting lines with image link - Vendor','200','','XXWC_PAY_VENDOR_BANK_DETAILS',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Accounting lines with image link - Vendor','200','','XXWC_PAY_VENDOR_MSTR',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Accounting lines with image link - Vendor','200','','XXWC_PAY_VENDOR_MSTR_INQUIRY',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Accounting lines with image link - Vendor','200','','HDS_AP_MANAGER',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Accounting lines with image link - Vendor','200','','HDS_PYABLS_MNGR_CAN',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Accounting lines with image link - Vendor','200','','HDS_PYBLS_MNGR',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Accounting lines with image link - Vendor','20005','','XXWC_VIEW_ALL_EIS_REPORTS',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Accounting lines with image link - Vendor','200','','HDS_AP_TRNS_ENTRY_CAD_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Accounting lines with image link - Vendor','200','','HDS_AP_SUPPLIER_MAINT_CAD_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Accounting lines with image link - Vendor','200','','HDS_AP_MGR_NOSUP_CAD_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Accounting lines with image link - Vendor','200','','HDS_AP_DISBURSEMTS_CAD_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Accounting lines with image link - Vendor','200','','HDS_AP_ADMIN_CAD_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Accounting lines with image link - Vendor','200','','HDS_PAYABLES_CLOSE_GSC_GLBL',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Accounting lines with image link - Vendor','200','','HDS_PAYABLES_CLOSE_GLBL',200,'XXEIS_RS_ADMIN','','','');
--Inserting Report Pivots - HDS AP Accounting lines with image link - Vendor
--Inserting Report   Version details- HDS AP Accounting lines with image link - Vendor
xxeis.eis_rsc_ins.rv( 'HDS AP Accounting lines with image link - Vendor','','HDS AP Accounting lines with image link - Vendor','AB063501');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
