#!/bin/ksh
#Script Name: eis_install.sh
#Purpose    : Installs the patch for EIS eXpress Reporting Products
#Instructions : Run this script from command line. 
#               Make sure that module list has all modules that you like to install
#MODIFICATION HISTORY
# Person      Date          Version        Comments
# ---------   ------        --------       --------------------------------------
# KP          20-Aug-2005   5.0.0          Created
# KP          04-Mar-2007   5.0.0          Modified to show Not Installed if ORA-00942 table or view not exists
# Sunil       28-May-2008   6.3.0          Modified to add new iRecriutment Module
# Sunil       28-Jul-2008   6.3.0          Modified the select statement where comma was missing
# AG	      05-Aug-2008   6.3.1	   Included the installation of Eis Financial Statement Generator.
# Sunil       30-Mar-2009   6.3.2          Modified for single line command parameter
# Venkat      21-Aug-2009   6.3.2.8        Modified
# KP          05-May-2010   7.0.0          Modified
# Ramana      19-Jul-2010   7.0.0          Added COMPILE_INVALID to compile invalid database objects after complete installation
# Venkat      10-Aug-2010   7.0.0          Done changes for eis_java_top and 11i,12 installations
# KP          10-Aug-2010   7.0.0          Modified to install the patching tables if they do not exist already
# Ramana      24-Aug-2010   7.0.0          Modified the condition for force_apply.
# KP          02-Sep-2010   7.0.0          Modified the install to move the ldt log files to log ditectory
# Ramana      23-Sep-2010   7.0.0.         Moving of ldt log files is giving error if files does not exist.. so modified to run that mv only if files exists
# Ramana      07-Oct-2010   7.0.0.         Modified the file to reset connect_user to apps during the module install start in database install
# Ramana      13-Oct-2010   7.0.0          Modified module exists sql for PA module
# Ramana      20-Oct-2010   7.0.0          Removed test as itis erroring in some shells.  Also added UPDATE_PATCH to update last_update_date in eis_rs_patches.
# Ramana      20-Oct-2010   7.0.0          Added directory exists condition for copy r12 specific java and oaf files
# Ramana      27-Oct-2010   7.0.0          Added the pre_req_patch logic.
# Ramana      28-Oct-2010   7.0.0          Changed apply_date to database server date instead of application server date.
# Ramana      29-Oct-2010   7.0.0          Commented echo for apps version, fixed file_name initialization.
# Ramana      07-Dec-2010   7.0.0          Updated logic for patch status.
# Ramana      09-Dec-2010   7.0.0          Fixed the ldt file logic giving invalid operand message.  Modified the logic to check for fndload in file entry first
#                                          and then getting the file name and verifying file exists or not.
# Ramana      23-Dec-2010   7.0.0          Modified the logic to call patch_ddl.sql file based on patch status column exists or not.

patch_number="HDS_PARAM_LISTS"
patch_description="HDS Parameter Lists Configuration"
build_number="1"
product_code=""     #RS XLC DASHB PWB TE PP
release_date="24-MAY-2012"      #DD-MON-YYYY
one_off_patch="Y"
force_apply="Y"
#apply_date=`date +"%d-%m-%Y %T"`
pre_req_patch=""    #mention the pre-req patch if any exists


#Modules to be installed. Copy and edit the lines to change the module list.
#Even though all modules are listed. This will install only the modules that have been purchased or modules under database folder
modules="hds"
# be dm adm hr pay ben otl irec cwb gl ap ar fa po pa ce gms fsg inv om cst bom wip opm ascp pwb dashb xlc pp"
#Module Help
# "be"  - "Reporting Base Engine"

#HRMS Modules
# "hr"  - "Human Resources"
# "ben" - "Benefits - OAB"
# "dm"  - "EIS Payroll Datamart" -- Pre-Requisite for Payroll Module
# "pay" - "Payroll"
# "otl" - "Oracle Time and Labour"
# "pp"  - "EIS Payroll Processor"
# "te"  - "EIS Time Entry"
# "irec"- "iRecruitment"

#Financial Modules
# "gl"  - "General Engine"
# "ap"  - "Accounts Payables"
# "ar"  - "Accounts Receivables"
# "po"  - "Purchasing"
# "fa"  - "Fixed Assets"
# "pa"  - "Projects"
# "ce"  - "Cash Management"
# "gms" - "Grants Accounting"
# "fsg" - "Financial Statement Generator"

#Supply Chain Modules
# "inv" - "Inventory"
# "om"  - "Order Management"
# "cst" - "Cost Management"
# "bom" - "Bills of Material"
# "wip" - "Work in Progress"
# "opm" - "Oracle Process Manufacturing"
# "ascp"- "Advanced Supply Chain Planning"

################ Start Internal Procedures########################################

execute_query() {
  typeset stmt=$1
  typeset login=$2

  echo "
    set feedback off verify off heading off pagesize 0
    $stmt;
    exit 
  " |  sqlplus -s  $login
}

CHKLOGIN(){
       if sqlplus -s /nolog <<! >/dev/null 2>&1
          WHENEVER SQLERROR EXIT 1;
          CONNECT $1 ;
          EXIT;
!
    then
        echo OK
    else
        echo NOK
    fi
}
#End CHKLOGIN

#Validate the parameters
VALIDATE_PARAMETERS(){

	if [ `CHKLOGIN "apps/$apps_password@$sid" "DUAL"` = "NOK" ]
	then
	echo "APPS Password or SID entered is not CORRECT" 
	exit 1; #KP#
	fi

	#if [ `CHKLOGIN "xxeis/$xxeis_password@$sid" "DUAL"` = "NOK" ]
	#then
	#echo "XXEIS Password entered is not CORRECT" 
	#exit  1; #KP#
	#fi
	
	if [ "$system_password" != "" ]
	then
		if [ `CHKLOGIN "system/$system_password@$sid" "DUAL"` = "NOK" ]
		then
		echo "SYSTEM Password entered is not CORRECT" 
		exit 1; #KP#
		fi
	fi

        #Verify XXEISD tablespace
        echo set heading off feedback off verify off > $INSTALL_DIR/log/index_temp.sql
        echo "select count(*) from dba_tablespaces where tablespace_name = upper('$tab_tablespace');" >> $INSTALL_DIR/log/index_temp.sql
        echo exit>> $INSTALL_DIR/log/index_temp.sql

        outvar=`sqlplus -s apps/$apps_password@$sid @$INSTALL_DIR/log/index_temp.sql`
        outvar=`echo $outvar | cut -c1-1`

        if [ "$outvar" = "0"  ]
        then
                echo "Tablespace " $tab_tablespace " is invalid"
                exit 1; #KP#
        fi

        #Verify XXEISX tablespace
        echo set heading off feedback off verify off > $INSTALL_DIR/log/index_temp.sql
        echo "select count(*) from dba_tablespaces where tablespace_name = upper('$index_tablespace');" >> $INSTALL_DIR/log/index_temp.sql
        echo exit>> $INSTALL_DIR/log/index_temp.sql

        outvar=`sqlplus -s apps/$apps_password@$sid @$INSTALL_DIR/log/index_temp.sql`
        outvar=`echo $outvar | cut -c1-1`

        if [ "$outvar" = "0"  ]
        then
                echo "Index Tablespace " $index_tablespace " is invalid"
                exit 1; #KP#
        fi

        #Verify if System Password needs to passed
	echo set feedback on > $INSTALL_DIR/log/index_temp.sql
	echo "grant all on fnd_sessions to xxeis;" >> $INSTALL_DIR/log/index_temp.sql
	echo exit>> $INSTALL_DIR/log/index_temp.sql

        outvar=`sqlplus -s apps/$apps_password@$sid @$INSTALL_DIR/log/index_temp.sql`
        outvar=`echo $outvar | cut -c1-15`

        if [ "$outvar" != "Grant succeeded"  ]
        then
		if [ "$system_password" = "" ]
		then
			echo "System Password is needed for this installation. Please pass the it as last parameter to this shell script"
			exit 1; #KP#
		fi
        fi

        rm $INSTALL_DIR/log/index_temp.sql

	# Verify Host name and Port Number
	#Create Dummy XML File which will be deleted later
	#echo "<test> </test>" > eistest.xml
	
       #JAVALOG=`$java_exec oracle.jrad.tools.xml.importer.XMLImporter ./eistest.xml  -rootdir ./ -username APPS -password $apps_password -dbconnection "(description = (load_balance=yes)(failover=yes) (address_list=(address=(protocol=tcp)(host =$host_name)(port = $database_port)))(connect_data=(service_name=$sid)))"`

	#if [ `echo $JAVALOG | grep  -c "Import completed"` -eq 0 ];then
	#	echo "Host Name or Port Number is invalid or java executable might be in wrong path"
	#	exit;
	#fi
	#rm -rf eistest.xml

      #KP#
      #Verify if patching tables exists
      #echo set heading off feedback off verify off > $INSTALL_DIR/log/index_temp.sql
      #echo "select count(*) from all_tables where owner = 'XXEIS' and table_name = 'EIS_PATCHES';" >> $INSTALL_DIR/log/index_temp.sql
      #echo exit>> $INSTALL_DIR/log/index_temp.sql
    
      #outvar=`sqlplus -s apps/$apps_password@$sid @$INSTALL_DIR/log/index_temp.sql`
      #outvar=`echo $outvar | cut -c1-1`

      #outvar=`execute_query "select count(*) from all_tab_columns where owner = 'XXEIS' and table_name = 'EIS_PATCHES' and  column_name = 'PATCH_STATUS'" apps/$apps_password@$sid`

      #if [ "$outvar" = "0"  ]
      #then
		#if patching tables don't exists then create them or patch_status column is not there
	#      sqlplus -s xxeis/$xxeis_password@$sid @$INSTALL_DIR/eis_patch_ddl.sql $tab_tablespace $index_tablespace 
      #fi
      #KP#	

}
#End VALIDATE_PARAMETERS

#Get Oracle Application version
GET_ORACLE_APPS_VERSION(){

	oracle_apps_version=`execute_query "SELECT substr(max(release_name),1,2) FROM fnd_product_groups" apps/$apps_password@$sid`
   
	if [ "$oracle_apps_version" = "12" ];then
		oracle_apps_version="r12"
	else
		oracle_apps_version="11i"
	fi

	# echo "Installed Oracle Apps Version $oracle_apps_version"

}

#Insert Patch
INSERT_PATCH(){

        echo set heading off feedback off verify off > $INSTALL_DIR/log/patch_count.sql
        echo "SELECT count(*) FROM xxeis.eis_patches where patch_number = '$patch_number' and patch_status = 'C';" >> $INSTALL_DIR/log/patch_count.sql
        echo "exit;">> $INSTALL_DIR/log/patch_count.sql

	patch_count=`sqlplus -s apps/$apps_password@$sid @$INSTALL_DIR/log/patch_count.sql`
        patch_count=`echo $patch_count | cut -c1-1`
        rm -rf $INSTALL_DIR/log/patch_count.sql

        if [ "$patch_count" != "0" -a $force_apply != "Y" ]
        then
		
		echo "Patch" $patch_number "is already installed." 
		echo "Patch" $patch_number "is already installed." >> $INSTALL_DIR/log/"$patch_number".log
		exit;

	fi

	echo "begin xxeis.eis_product_install_pkg.insert_patches('$patch_number','$product_code','$build_number','$patch_description',to_date('$release_date','DD-MON-YYYY'),to_date('$apply_date','DD-MM-YYYY HH24:MI:SS'),'','$patch_status'); end;" > $INSTALL_DIR/log/index_temp.sql
        echo "/">> $INSTALL_DIR/log/index_temp.sql
        echo exit>> $INSTALL_DIR/log/index_temp.sql

        outvar=`sqlplus -s xxeis/$xxeis_password@$sid @$INSTALL_DIR/log/index_temp.sql`
}
#End INSERT_PATCH

#Update Patch
UPDATE_PATCH(){

	echo "begin xxeis.eis_product_install_pkg.insert_patches('$patch_number','$product_code','$build_number','$patch_description',to_date('$release_date','DD-MON-YYYY'),to_date('$apply_date','DD-MM-YYYY HH24:MI:SS'),'','$patch_status'); end;" > $INSTALL_DIR/log/update_patch.sql
        echo "/">> $INSTALL_DIR/log/update_patch.sql
        echo exit>> $INSTALL_DIR/log/update_patch.sql

        outvar=`sqlplus -s xxeis/$xxeis_password@$sid @$INSTALL_DIR/log/update_patch.sql`

}
#End UPDATE_PATCH

#compile invalid
COMPILE_INVALID(){

	echo "begin xxeis.eis_rs_install_uninstall_pkg.compile_invalid_objects; end;" > $INSTALL_DIR/log/compile_invalid.sql
        echo "/">> $INSTALL_DIR/log/compile_invalid.sql
        echo exit>> $INSTALL_DIR/log/compile_invalid.sql

        outvar=`sqlplus -s xxeis/$xxeis_password@$sid @$INSTALL_DIR/log/compile_invalid.sql`
}
#End COMPILE_INVALID

LOAD_VERSION_NUMBERS(){

	echo "prompt " > $INSTALL_DIR/log/version_number.sql
	echo "set define off scan off " >> $INSTALL_DIR/log/version_number.sql
	echo "begin " >> $INSTALL_DIR/log/version_number.sql
        l_counter=0
	for fileline in `cat $version_control_file`
	do
                l_counter=`echo $l_counter+1 | bc`
		if [ "$l_counter" = "500" ]; then
			echo "end;" >> $INSTALL_DIR/log/version_number.sql
			echo "/" >> $INSTALL_DIR/log/version_number.sql
			echo "begin " >> $INSTALL_DIR/log/version_number.sql
			l_counter=0
		fi
		filename=`echo $fileline | cut -d ':' -f1`
		version_number=`echo $fileline | cut -d ':' -f2`
  		echo "xxeis.eis_product_install_pkg.insert_patch_details('$patch_number','$filename','$version_number','Y');" >> $INSTALL_DIR/log/version_number.sql

	done 
	echo "null;" >> $INSTALL_DIR/log/version_number.sql
	echo "end;" >> $INSTALL_DIR/log/version_number.sql
	echo "/" >> $INSTALL_DIR/log/version_number.sql
	echo "exit;" >> $INSTALL_DIR/log/version_number.sql
        sqlplus -s xxeis/$xxeis_password@$sid @$INSTALL_DIR/log/version_number.sql

}

VERIFY_LOG_FILES(){

	touch $INSTALL_DIR/log/1_install.log

	log_message="notshown"

	for filename in `grep -l ORA- $INSTALL_DIR/log/*_install.log`
	do
		if [ "$log_message" = "notshown" ]
		then
		echo "Errors occurred while installing the patch. Please find the following log files for reference."
		log_message="shown"
		patch_status="E"
		echo $filename
		else
		echo $filename
		fi
	done

	if [ "$log_message" = "notshown" ]
	then
		echo 
		echo "Patch installation completed successfully. Log files are in $INSTALL_DIR/log"
		echo
		patch_status="C"
	fi

	rm $INSTALL_DIR/log/1_install.log

	#Move the sql files to $INSTALL_DIR/log/sql
	cd $INSTALL_DIR/log

	if [ ! -d $INSTALL_DIR/log/sql ];then
		mkdir $INSTALL_DIR/log/sql
	fi

	mv -f *.sql $INSTALL_DIR/log/sql

	cd $INSTALL_DIR	
         }

DATABASE_INSTALL(){

        if [ -f $INSTALL_DIR/database/be/sql/eis_admin_user_creation.sql ];then
	
	
	   sqlplus -s apps/$apps_password@$sid @$INSTALL_DIR/database/be/sql/eis_admin_user_creation.sql $default_admin_user $default_admin_pwd

           #Verify ADMIN User
           echo set heading off feedback off verify off > $INSTALL_DIR/log/user_test.sql
           echo "select count(*) from fnd_user where user_name = upper('$default_admin_user');" >> $INSTALL_DIR/log/user_test.sql
           echo exit>> $INSTALL_DIR/log/user_test.sql

           user_exists=`sqlplus -s apps/$apps_password@$sid @$INSTALL_DIR/log/user_test.sql`
           user_exists=`echo $user_exists | cut -c1-1`
           rm -rf @$INSTALL_DIR/log/user_test.sql

           if [ "$user_exists" = "0"  ]
           then
                echo "EiS Admin user not created"
                exit 1 ; #KP#
           fi           
	fi
 echo "Installing database"
	cd $INSTALL_DIR/database

        for module in `echo $modules`
        do
	    echo "Installing database"
	    module_exists="Y"
	    connect_user="apps"

    	    if [ -d $INSTALL_DIR/database/$module -a "$module_exists" = "Y" ];then
                install_file=$INSTALL_DIR/log/"$module"_install.sql
		cd $INSTALL_DIR/database/$module

		#KP#
		#Load ldt files if any
		for fileline in `cat $INSTALL_DIR/database/$module/file_order.txt`
		do
		if [ `echo $fileline | cut -c1-7` = "fndload" ];then

			filename=`echo $fileline | cut -d ':' -f3`
			if [ -f $INSTALL_DIR/database/$module/$filename ];then
			
				lctfilename=`echo $fileline | cut -d ':' -f2`		
				ldtfilename="$INSTALL_DIR/database/$module/"`echo $fileline | cut -d ':' -f3`		

				echo Loading ldt $ldtfilename
				echo FNDLOAD apps@$sid 0 Y UPLOAD $FND_TOP/patch/115/import/$lctfilename $ldtfilename   >> $INSTALL_DIR/log/"$module"_install.log
				FNDLOAD apps/$apps_password@$sid 0 Y UPLOAD $FND_TOP/patch/115/import/$lctfilename $ldtfilename   >> $INSTALL_DIR/log/"$module"_install.log

			fi
		fi
		done		

                file_cnt=`ls *.log 2> /dev/null | wc -l`
                if [ "$file_cnt" != "0" ]; then
                   mv -f *.log $INSTALL_DIR/log
                fi

                file_cnt=`ls *.out 2> /dev/null | wc -l`
                if [ "$file_cnt" != "0" ]; then
                   mv -f *.out $INSTALL_DIR/log
                fi

		#KP#
		
		echo "SET serveroutput ON SIZE 1000000" > $install_file
		echo "set define '\`'" >> $install_file

		for fileline in `cat $INSTALL_DIR/database/$module/file_order.txt`
		do

		filename=`echo $fileline | cut -d ':' -f3`
		if [ "$filename" != "" ];then

			if [ `echo $fileline | cut -c1-12` = "sqlplusspool" ];then

				if [ `echo $fileline | cut -d ':' -f2` != $connect_user ];then
					connect_user=`echo $fileline | cut -d ':' -f2`
					echo  >> $install_file
					echo "PROMPT ***********Connecting to `echo $fileline | cut -d ':' -f2`*********" >> $install_file
					if [ $connect_user  = "apps" ];then
						echo "connect apps/\`4@\`6" >> $install_file					
					fi 
					if [ $connect_user  = "xxeis" ];then
						echo "connect xxeis/\`3@\`6" >> $install_file					
					fi 
				fi 

				
				echo "PROMPT executing $filename" >> $install_file
				echo @$INSTALL_DIR/database/$module/$filename >> $install_file
				echo "show errors" >> $install_file
				echo >> $install_file

			fi
		fi
		done

		echo spool "$module"_install.log >> $install_file
		echo "PROMPT **************** EIS eXpress Reporting - $module Module Install**********" >> $install_file
		echo PROMPT >> $install_file

		install_module="N"
		for fileline in `cat $INSTALL_DIR/database/$module/file_order.txt`
		do

		filename=`echo $fileline | cut -d ':' -f3`
		file_apps_version=`echo $fileline | cut -d ':' -f4`

		if [ "$file_apps_version" != "" ];then
		   if [ "$oracle_apps_version" != "$file_apps_version" ];then
			filename="skip_this_file"
		   fi
		fi

		if [ "$filename" != "" -a "$filename" != "skip_this_file" ];then

			if [ "$filename" != "pre_install.sql" -a "$filename" != "post_install.sql" ];then
				install_module="Y"
			fi

			if [ `echo $fileline | cut -c1-8` = "sqlplus:" ];then

				if [ `echo $fileline | cut -d ':' -f2` != "$connect_user" ];then
					connect_user=`echo $fileline | cut -d ':' -f2`
					echo  >> $install_file
					
					if [ $connect_user  = "system" -a "$system_password" != "" ];then
						echo "PROMPT ***********Connecting to `echo $fileline | cut -d ':' -f2`*********" >> $install_file
						echo "connect system/\`15@\`6" >> $install_file					
					fi 
					if [ $connect_user  = "apps" ];then
						echo "PROMPT ***********Connecting to `echo $fileline | cut -d ':' -f2`*********" >> $install_file 
						echo "connect apps/\`4@\`6" >> $install_file					
					fi 
					if [ $connect_user  = "xxeis" ];then
						echo "PROMPT ***********Connecting to `echo $fileline | cut -d ':' -f2`*********" >> $install_file
						echo "connect xxeis/\`3@\`6" >> $install_file		
						if [ "$module" != "be" -a "$one_off_patch" != "Y" ]; then
						   echo "exec xxeis.eis_rs_ins.g_load_view_comp_vc := 'false'" >> $install_file	
						fi
					fi 
				fi 

				
				if [ `echo $fileline | grep -c "*"` = 1 ];then
					for in_file in `ls $filename`
					do
						echo "PROMPT executing $in_file" >> $install_file
						echo @$INSTALL_DIR/database/$module/$in_file >> $install_file
						echo "show errors" >> $install_file
						echo >> $install_file
					done
				else
				    # if system password is passed then only run the file
				    if [ $connect_user  = "system" -a "$system_password" != "" ];then
					echo "PROMPT executing $filename" >> $install_file
					echo @$INSTALL_DIR/database/$module/$filename >> $install_file
					echo "show errors" >> $install_file
					echo >> $install_file
				    else
				       if [ $connect_user  != "system" ]; then
					  echo "PROMPT executing $filename" >> $install_file
  					  echo @$INSTALL_DIR/database/$module/$filename >> $install_file
					  echo "show errors" >> $install_file
					  echo >> $install_file
				       fi
				    fi
				fi

			fi
		fi
		done

		echo "spool off" >> $install_file
		echo "exit" >> $install_file

                if [ -f $install_file ];then

		    if [ "$install_module" = "Y" ];then
		        sqlplus -s apps/$apps_password@$sid @$install_file $tab_tablespace $index_tablespace $xxeis_password $apps_password apps $sid "./$module/" $data_group $default_admin_user $patch_number $build_number $release_date "$patch_description" $product_code $system_password 
			if [ -f "$module"_install.log ];then
				mv "$module"_install.log $INSTALL_DIR/log/
			fi
                    fi
                fi

		#Insert patch details
		if [ "$install_module" = "Y" ];then
			echo Loading version numbers for database/$module
			version_control_file=$INSTALL_DIR/database/$module/version_control.txt
			#LOAD_VERSION_NUMBERS;
                fi
            fi
        done

	cd $INSTALL_DIR

	if [ -f L*.log ];then

		mv  L*.log $INSTALL_DIR/log/
	fi

	cd $INSTALL_DIR

	rm -f *.lst

        #echo Compiling Invalid Objects
        #COMPILE_INVALID;

         }

APPLICATION_INSTALL(){

	LOGFILE="$INSTALL_DIR/log/apps_server_install.log"

        

        if [ -d $INSTALL_DIR/oaf ];then
		cp -rf $INSTALL_DIR/oaf/common/hds $INSTALL_DIR
		if [ "$oracle_apps_version" = "r12" ];then
		   if [ -d $INSTALL_DIR/oaf/r12/hds ]; then
		      cp -rf $INSTALL_DIR/oaf/r12/hds $INSTALL_DIR
		   fi
		fi
		#cp $INSTALL_DIR/oaf/version_control.txt $INSTALL_DIR/eis
        fi

        if [ -d $INSTALL_DIR/hds ];then

		echo
		echo "Copying hds class files to JAVA_TOP"
		echo "Copying class files to JAVA_TOP"				>> $LOGFILE
		cp -rf $INSTALL_DIR/hds   $EIS_JAVA_TOP			>> $LOGFILE

		chmod -R +x $EIS_JAVA_TOP/hds
		#rm -rf $EIS_JAVA_TOP/hds/version_number.txt
	fi

       
	#OAF Files Import

	echo

        if [ -d $INSTALL_DIR/hds ];then
		cd $INSTALL_DIR
		echo "Loading XML files into MDS Repository....This may take few minutes...."
		for dir_name in `find ./hds -name webui`
		do
		   #if test -f $dir_name/*.xml ;then
			file_cnt=`ls $dir_name/*.xml 2> /dev/null | wc -l` 
			if [ "$file_cnt" != "0" ]; then
			   for filename in `ls $dir_name/*.xml`
			   do
				$java_exec oracle.jrad.tools.xml.importer.XMLImporter $EIS_JAVA_TOP/`echo $filename | cut -c3-` -rootdir $EIS_JAVA_TOP -username APPS -password $apps_password -dbconnection "(description = (load_balance=yes)(failover=yes) (address_list=(address=(protocol=tcp)(host =$host_name)(port = $database_port)))(connect_data=(sid=$sid)))" >> $LOGFILE
			   done
			fi
		    #fi
		done
        fi

       

	#Load Version numbers for eis
	version_control_file=$INSTALL_DIR/hds/version_control.txt
        if [ -f $INSTALL_DIR/hds/version_control.txt ];then
	     echo Loading Version numbers for eis
	     LOAD_VERSION_NUMBERS;
        fi

	#Load Version numbers for OAF pages
	version_control_file=$INSTALL_DIR/eisrs/version_control.txt

	if [ -f $INSTALL_DIR/eisrs/version_control.txt ];then
	     echo Loading Version numbers for OAF pages
	     LOAD_VERSION_NUMBERS;
        fi

	echo Application Server Installation Completed.

}
#End APPLICATION_INSTALL
################ End Internal Procedures########################################

################ Shell Script Execution Starts Here ############################

if [ $# -le 7 ]
then
    echo "Error in $0 - Invalid Argument Count"
    echo "Usage: $0 <sid> <database_host> <database_port> <apps_password> <table_tablespace> <index_tablespace> <data_group> <java_executable> <system_password(optional)>"
    echo "Example: $0 PROD prod.eistech.com 1521 appspwd XXEISD XXEISX Standard java system_pwd "

    exit 1 ; #KP#
fi

if [ "$patch_number" = "" -o "$build_number" = "" -o "$release_date" = "" ];then
    echo "Either patch_Number or build_number or release_date is null"
    exit 1 ; #KP#
fi

sid=$1                                  #SID of the instance
host_name=$2                            #Database Server Name
database_port=$3                        #Database Port number
apps_password=$4                        #APPS Schema Password.
xxeis_password=""                       #XXEIS Schema Password.
tab_tablespace=$5                       #tablespace for XXEIS Tables
index_tablespace=$6                     #tablespace for XXEIS Indexes
data_group=$7                           #datagroup ( default is Standard )
java_exec=$8                            #java or jre executable optionally with full path
system_password=${9}                   #System Password
default_admin_user="XXEIS_RS_ADMIN"     #Default Admin user for eis reporting.  Not recommended to change this.
default_admin_pwd="welcome#123"         #Default Admin user password.
EIS_JAVA_TOP=$JAVA_TOP              #Custom java top for eis oaf files.

INSTALL_DIR=`'pwd'`

if [ ! -d $INSTALL_DIR/log ];then
	mkdir $INSTALL_DIR/log
fi

VALIDATE_PARAMETERS;

GET_ORACLE_APPS_VERSION;

apply_date=`execute_query "SELECT to_char(sysdate,'DD-MM-YYYY HH24:MI:SS') FROM dual" apps/$apps_password@$sid`

	echo -------------------------------------------------
if [ "$one_off_patch" = "Y" ];then
	install_choice=3
	echo "Installing Patch : " $patch_number
else
	install_choice=0
	echo "     Installing Patch  "
fi
	echo -------------------------------------------------

if [ "$pre_req_patch" != "" ];then

   patch_count=`execute_query "SELECT count(*) from xxeis.eis_patches where patch_number = '$pre_req_patch'" xxeis/$xxeis_password@$sid`
   
   if [ $patch_count = 0 ];then

      echo " Patch $pre_req_patch is pre-requisite Patch for this patch $patch_number.  But $pre_req_patch is not installed.  "
      echo " Please install the pre-requisite Patch $pre_req_patch."
      exit;

   fi
fi

while [ "$install_choice" != 1 -a "$install_choice" != 2 -a "$install_choice" != 3 -a "$install_choice" != 4 ]
do
    if [ "$install_choice" = 0 ];then
        echo "1. Install Application Server Components"
	echo "2. Install Database Components"
        echo "3. Install both Application Server and Database Components"
	echo "4. Exit Installation"
        echo -n "Select your Choice [1,2,3,4]? "

        read install_choice  #install_choice = install choice
    else
        echo "Installation Choice is not correct. Please enter valid choice:" 
        read install_choice  #install_choice = install choice
    fi
done

patch_status="U"

#INSERT_PATCH;

if [ $install_choice -eq 1 ]; then

     APPLICATION_INSTALL;

elif [ $install_choice -eq 2 ];  then

     DATABASE_INSTALL;

     VERIFY_LOG_FILES;

elif [ $install_choice -eq 3 ];  then
     
     DATABASE_INSTALL;

     APPLICATION_INSTALL;

     VERIFY_LOG_FILES;

elif [ $install_choice -eq 4 ];  then
     
     exit;

fi

#UPDATE_PATCH;

#KP#

if [ "$log_message" = "shown" ];then
	exit 1 ;
else
	exit 0 ;
fi
#KP#
