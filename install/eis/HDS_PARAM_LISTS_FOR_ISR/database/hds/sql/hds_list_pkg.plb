CREATE OR REPLACE PACKAGE BODY HDS_LIST_PKG
AS
   PROCEDURE INS_UPD_LISTS (
      P_LIST_ID      IN OUT NUMBER,
      P_LIST_NAME    IN VARCHAR2,
      P_LIST_VALUES  IN CLOB,
      P_FLAG         IN VARCHAR2 ,
      P_USER_ID      IN NUMBER DEFAULT NULL,  
      p_list_type    in varchar2,
      P_MESSAGE      OUT VARCHAR2
     )
   IS
      l_list_id   NUMBER;
      p_message_list_id number;
   BEGIN
      IF P_LIST_ID IS NULL
          THEN
             SELECT XXWC_PARAM_LIST_S.NEXTVAL
               INTO l_list_id
               FROM DUAL;
    
             P_LIST_ID := l_list_id;
    
          INSERT INTO XXWC_PARAM_LIST (LIST_ID,
               LIST_NAME,
               LIST_VALUES,
               public_FLAG,
               USER_ID,
               list_type)
          VALUES (P_LIST_ID,
                  P_LIST_NAME,
                  P_LIST_VALUES,
                  P_FLAG,
                  P_USER_ID,
                  p_list_type);
     ELSE
         p_message_list_id:=P_LIST_ID;
         UPDATE XXWC_PARAM_LIST
            SET LIST_NAME = P_LIST_NAME,
                LIST_VALUES = P_LIST_VALUES,
                public_FLAG = P_FLAG,
                USER_ID = P_USER_ID
          WHERE LIST_ID = P_LIST_ID;
    END IF;
    
      IF p_message_list_id IS NOT NULL THEN
         P_MESSAGE := P_LIST_NAME || ' saved successfully';
      ELSE 
         P_MESSAGE := P_LIST_NAME || ' Created Successfully';
      END IF; 
   EXCEPTION
      WHEN OTHERS
      THEN
         RAISE;
   END INS_UPD_LISTS;
   
   PROCEDURE DELETE_ROW                                 
          ( P_LIST_ID    IN  NUMBER,
            P_MESSAGE    OUT VARCHAR2
          )
   IS
     l_list_name VARCHAR2(100);
     
   BEGIN    
        IF P_LIST_ID IS NOT NULL THEN
          SELECT LIST_NAME INTO l_list_name FROM XXWC_PARAM_LIST WHERE LIST_ID = P_LIST_ID;
        END IF;  
        DELETE FROM XXWC_PARAM_LIST WHERE LIST_ID = P_LIST_ID;  
        P_MESSAGE :=  l_list_name || ' deleted successfully. ';    
   EXCEPTION
      WHEN OTHERS
      THEN
         RAISE ;
   END DELETE_ROW;
   
END HDS_LIST_PKG;

/
