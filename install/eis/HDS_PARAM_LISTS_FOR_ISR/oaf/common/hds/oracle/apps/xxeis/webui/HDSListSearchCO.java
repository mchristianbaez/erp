/*===========================================================================+
 |   Copyright (c) 2001, 2005 Oracle Corporation, Redwood Shores, CA, USA    |
 |                         All rights reserved.                              |
 +===========================================================================+
 |  HISTORY                                                                  |
 +===========================================================================*/
package hds.oracle.apps.xxeis.webui;

import java.io.Serializable;

import java.util.Hashtable;

import oracle.apps.fnd.common.MessageToken;
import oracle.apps.fnd.common.VersionInfo;
import oracle.apps.fnd.framework.OAApplicationModule;
import oracle.apps.fnd.framework.OAException;
import oracle.apps.fnd.framework.webui.OABoundValueEnterOnKeyPress;
import oracle.apps.fnd.framework.webui.OAControllerImpl;
import oracle.apps.fnd.framework.webui.OADialogPage;
import oracle.apps.fnd.framework.webui.OAPageContext;
import oracle.apps.fnd.framework.webui.OAWebBeanConstants;
import oracle.apps.fnd.framework.webui.beans.OAWebBean;
import oracle.apps.fnd.framework.webui.beans.form.OAFormValueBean;
import oracle.apps.fnd.framework.webui.beans.message.OAMessageLovInputBean;
import oracle.apps.fnd.framework.webui.beans.OAFormattedTextBean;
import oracle.apps.fnd.framework.webui.beans.layout.OAPageLayoutBean;


/**
 * Controller for ...
 */
public class HDSListSearchCO extends OAControllerImpl
{
  public static final String RCS_ID="$Header$";
  public static final boolean RCS_ID_RECORDED =
        VersionInfo.recordClassVersion(RCS_ID, "%packagename%");

  /**
   * Layout and page setup logic for a region.
   * @param pageContext the current OA page context
   * @param webBean the web bean corresponding to the region
   */
  public void processRequest(OAPageContext pageContext, OAWebBean webBean)
  {
    super.processRequest(pageContext, webBean);
    OAApplicationModule am = pageContext.getApplicationModule(webBean);
    String listFlag = pageContext.getParameter("listType");
    MessageToken[] tTokens = {new MessageToken("LTYPE",listFlag)};
    String msg = pageContext.getMessage("XXEIS", "HDS_LIST_SEARCH", tTokens);
    pageContext.getPageLayoutBean().setWindowTitle(msg);
    pageContext.getPageLayoutBean().setTitle(msg);
    String message = null;


    
    if(pageContext.isLoggingEnabled(1))
    {
       pageContext.writeDiagnostics(this," processRequest()::: listFlag = " + listFlag,1);
    } 
    if(pageContext.getParameter("status")!=null && pageContext.getParameter("status").length()>0)
    {
      message = pageContext.getParameter("status");
      OAException messages = new OAException(message,OAException.CONFIRMATION);
      pageContext.putDialogMessage(messages);
    }
    if(pageContext.getParameter("delMessage")!=null && pageContext.getParameter("delMessage").length()>0)
    {
       message = pageContext.getParameter("delMessage");
       OAException messages = new OAException(message,OAException.CONFIRMATION);
       pageContext.putDialogMessage(messages);
    }

    Hashtable paramsOnEnter = new Hashtable(1);
    paramsOnEnter.put("SEARCH_EVENT", "SEARCH_EVENT");
    OABoundValueEnterOnKeyPress onEnterAction = new OABoundValueEnterOnKeyPress("DefaultFormName", paramsOnEnter,  true, true);
    OAMessageLovInputBean listNameBean        = (OAMessageLovInputBean) webBean.findChildRecursive("listNameLV");
    if(listNameBean!=null)
     listNameBean.setAttributeValue(OAWebBeanConstants.ON_KEY_PRESS_ATTR,onEnterAction);

    String loginUserId = String.valueOf(pageContext.getUserId()); 
    Serializable[] userParams = {loginUserId,listFlag};
    am.invokeMethod("listDetails",userParams);
  }

  /**
   * Procedure to handle form submissions for form elements in
   * a region.
   * @param pageContext the current OA page context
   * @param webBean the web bean corresponding to the region
   */
  public void processFormRequest(OAPageContext pageContext, OAWebBean webBean)
  {
    super.processFormRequest(pageContext, webBean);
    String listFlag = pageContext.getParameter("listType");
    if(pageContext.isLoggingEnabled(1))
    {
       pageContext.writeDiagnostics(this," processFormRequest()::: listFlag = " + listFlag,1);
    } 
    OAApplicationModule am = pageContext.getApplicationModule(webBean);
    String listId = pageContext.getParameter("listId");
    String listName = pageContext.getParameter("listName");
    String loginUserId = String.valueOf(pageContext.getUserId()); 

    if("NEW_LIST".equals(pageContext.getParameter(OAWebBeanConstants.EVENT_PARAM)))
    {
      com.sun.java.util.collections.HashMap newParams = 
         new com.sun.java.util.collections.HashMap();
       newParams.put("listFlag",listFlag);
      pageContext.forwardImmediately("HDS_LIST_CREATE",
                                       OAWebBeanConstants.KEEP_MENU_CONTEXT,
                                       null,
                                       newParams,
                                       true,
                                       OAWebBeanConstants.ADD_BREAD_CRUMB_NO);
    }
    if("EDIT_LIST".equals(pageContext.getParameter(OAWebBeanConstants.EVENT_PARAM)))
    {
       com.sun.java.util.collections.HashMap editParams = 
         new com.sun.java.util.collections.HashMap();
       editParams.put("listId",listId);
       editParams.put("listName",listName);
       editParams.put("listFlag",listFlag);

       pageContext.forwardImmediately("HDS_LIST_CREATE",
                                       OAWebBeanConstants.KEEP_MENU_CONTEXT,
                                       null,
                                       editParams,
                                       true,
                                       OAWebBeanConstants.ADD_BREAD_CRUMB_NO);
    } 

    if("DELETE_LIST".equals(pageContext.getParameter(OAWebBeanConstants.EVENT_PARAM)))
    {
       String delListName = pageContext.getParameter("delListName");
       String delListId   = pageContext.getParameter("delListId");
       OAException lException  = null;
       MessageToken[] tokens= {new MessageToken("LNAME",delListName)}; 
       lException   = new OAException ("XXEIS", "HDS_LIST_DELETE", tokens);
       OADialogPage lDialogPG   = new OADialogPage(OAException.WARNING, lException, null, "", "");
       lDialogPG.setOkButtonItemName("submitYesBt");
       lDialogPG.setOkButtonToPost(true);
       lDialogPG.setNoButtonToPost(true);
       lDialogPG.setPostToCallingPage(true);               
       String yesLabel  = pageContext.getMessage("FND", "YES", null);
       String noLabel  = pageContext.getMessage("FND", "NO", null);
       if(yesLabel==null || yesLabel.equals(""))
          yesLabel  = "Yes";
       if(noLabel==null || noLabel.equals(""))
          noLabel  = "No";          
       lDialogPG.setOkButtonLabel(yesLabel);
       lDialogPG.setNoButtonLabel(noLabel);        
       lDialogPG.setReusePageLayout(true);           
       Hashtable param = new Hashtable();
       param.put("deleteListId",delListId);
       lDialogPG.setFormParameters(param);
       pageContext.redirectToDialogPage(lDialogPG);
    }
    else if(pageContext.getParameter("submitYesBt") != null)
    {
       String delId = pageContext.getParameter("deleteListId");
       Serializable[] delParams = {delId};
       String message = (String)am.invokeMethod("deleteLists",delParams);
       pageContext.putParameter("delMessage",message);
       pageContext.forwardImmediatelyToCurrentPage(null,false,null);         
    }

    if("SEARCH_EVENT".equals(pageContext.getParameter("SEARCH_EVENT")) || "SEARCH".equals(pageContext.getParameter(OAWebBeanConstants.EVENT_PARAM)))
    {
      String lsName = pageContext.getParameter("listNameLV");
//      OAFormValueBean listFormBean = (OAFormValueBean)pageContext.getPageLayoutBean().findChildRecursive("listId");
//      String searchListId = null; 
//      if(listFormBean!=null)
//       searchListId = (String)listFormBean.getValue(pageContext);
//      Serializable searchParams[]  = {searchListId}; 
      Serializable searchParams[] = { lsName,loginUserId,listFlag};
      am.invokeMethod("searchLists",searchParams);
    }
  }

}
