/*===========================================================================+
 |   Copyright (c) 2001, 2005 Oracle Corporation, Redwood Shores, CA, USA    |
 |                         All rights reserved.                              |
 +===========================================================================+
 |  HISTORY                                                                  |
 +===========================================================================*/
package hds.oracle.apps.xxeis.webui;

import java.io.Serializable;

import oracle.apps.fnd.common.MessageToken;
import oracle.apps.fnd.common.VersionInfo;
import oracle.apps.fnd.framework.OAApplicationModule;
import oracle.apps.fnd.framework.OAException;
import oracle.apps.fnd.framework.webui.OAControllerImpl;
import oracle.apps.fnd.framework.webui.OAPageContext;
import oracle.apps.fnd.framework.webui.OAWebBeanConstants;
import oracle.apps.fnd.framework.webui.beans.OAWebBean;
import oracle.apps.fnd.framework.webui.beans.message.OAMessageCheckBoxBean;
import oracle.apps.fnd.framework.webui.beans.OAFormattedTextBean;
import oracle.apps.fnd.framework.webui.beans.layout.OAPageLayoutBean;

/**
 * Controller for ...
 */
public class HDSListCreateCO extends OAControllerImpl
{
  public static final String RCS_ID="$Header$";
  public static final boolean RCS_ID_RECORDED =
        VersionInfo.recordClassVersion(RCS_ID, "%packagename%");

  /**
   * Layout and page setup logic for a region.
   * @param pageContext the current OA page context
   * @param webBean the web bean corresponding to the region
   */
  public void processRequest(OAPageContext pageContext, OAWebBean webBean)
  {
    super.processRequest(pageContext, webBean);
    OAApplicationModule am = pageContext.getApplicationModule(webBean);
    String listId =pageContext.getParameter("listId");
    String listName = pageContext.getParameter("listName");
    String loginUserId = String.valueOf(pageContext.getUserId());
    String listFlag = pageContext.getParameter("listFlag");

    
    if(pageContext.isLoggingEnabled(1))
    {
       pageContext.writeDiagnostics(this," processRequest()::: listFlag Create = " + listFlag,1);
    } 
    if(listId!=null && listId.length()>0)
    {
      if(listName!=null && listName.length()>0)
      {
          MessageToken[] tokens ={ new MessageToken("LNAME", listName),new MessageToken("LTYPE",listFlag) };
          String msg = pageContext.getMessage("XXEIS", "HDS_LIST_EDIT", tokens);
          pageContext.getPageLayoutBean().setWindowTitle(msg);
          pageContext.getPageLayoutBean().setTitle(msg);
      }
      /*
          OAMessageCheckBoxBean flagBean = (OAMessageCheckBoxBean)pageContext.getPageLayoutBean().findChildRecursive("flag");
          flagBean.setReadOnly(true);
      */    
    }
    else
    {     MessageToken[] tTokens = {new MessageToken("LTYPE",listFlag)};
          String msg = pageContext.getMessage("XXEIS", "HDS_LIST_CREATE", tTokens);
          pageContext.getPageLayoutBean().setWindowTitle(msg);
          pageContext.getPageLayoutBean().setTitle(msg);
    }
    Serializable[] listParams = {listId,loginUserId,listFlag}; 
    am.invokeMethod("initLists",listParams);
  }

  /**
   * Procedure to handle form submissions for form elements in
   * a region.
   * @param pageContext the current OA page context
   * @param webBean the web bean corresponding to the region
   */
  public void processFormRequest(OAPageContext pageContext, OAWebBean webBean)
  {
    super.processFormRequest(pageContext, webBean);
    OAApplicationModule am = pageContext.getApplicationModule(webBean);
    String listFlag = pageContext.getParameter("listFlag");
    if(pageContext.isLoggingEnabled(1))
    {
       pageContext.writeDiagnostics(this," processFromRequest()::: listFlag Create = " + listFlag,1);
    } 
    String listId = "";
    if(pageContext.getParameter("listId")!=null && pageContext.getParameter("listId").length()>0)
      listId = pageContext.getParameter("listId");
    String userId = String.valueOf(pageContext.getUserId());
    if("SAVE_LIST".equalsIgnoreCase(pageContext.getParameter(OAWebBeanConstants.EVENT_PARAM)))
    {
       String listName = pageContext.getParameter("listName");
       if(listId == null || listId.length() == 0)
       {
         Serializable[] checkParams = {listName};
         String isExists = (String)am.invokeMethod("checkDuplicateListName",checkParams);
         if(isExists.equalsIgnoreCase("yes"))
         {
              OAException message = new OAException("XXEIS", 
              "HDS_LIST_EXISTS", null , OAException.ERROR, null);
              throw OAException.wrapperException(message);
         }
       }
       String flag = pageContext.getParameter("flag");
       Serializable[] params = {userId,listId,listFlag};
       String message = (String)am.invokeMethod("createList",params);
       pageContext.putParameter("status",message);
       
       if(listFlag!=null && !listFlag.equals("Cat Class"))
       {
         pageContext.forwardImmediately("OA.jsp?page=/hds/oracle/apps/xxeis/webui/HDSListSearchPG&listType="+listFlag,
                                       null,
                                      OAWebBeanConstants.KEEP_MENU_CONTEXT,
                                      null,
                                      null,
                                      false, // retain AM
                                      OAWebBeanConstants.ADD_BREAD_CRUMB_NO);
       }     
       else
       {
          pageContext.forwardImmediately("HDS_LIST_TYPE_CAT",
                                       OAWebBeanConstants.KEEP_MENU_CONTEXT,
                                       null,
                                       null,
                                       false,
                                       OAWebBeanConstants.ADD_BREAD_CRUMB_NO);
       }
                                      
//       pageContext.forwardImmediately("HDS_LIST_SEARCH",
//                                        OAWebBeanConstants.KEEP_MENU_CONTEXT,
//                                        null,
//                                        null,
//                                        false, 
//                                        OAWebBeanConstants.ADD_BREAD_CRUMB_NO);
    }

    if("CANCEL".equalsIgnoreCase(pageContext.getParameter(OAWebBeanConstants.EVENT_PARAM)))
    {
       if(listFlag!=null && !listFlag.equals("Cat Class"))
       {
         pageContext.forwardImmediately("OA.jsp?page=/hds/oracle/apps/xxeis/webui/HDSListSearchPG&listType="+listFlag,
                                       null,
                                      OAWebBeanConstants.KEEP_MENU_CONTEXT,
                                      null,
                                      null,
                                      false, // retain AM
                                      OAWebBeanConstants.ADD_BREAD_CRUMB_NO);
       }     
       else
       {
          pageContext.forwardImmediately("HDS_LIST_TYPE_CAT",
                                       OAWebBeanConstants.KEEP_MENU_CONTEXT,
                                       null,
                                       null,
                                       false,
                                       OAWebBeanConstants.ADD_BREAD_CRUMB_NO);
       }
//       pageContext.forwardImmediately("HDS_LIST_SEARCH",
//                                        OAWebBeanConstants.KEEP_MENU_CONTEXT,
//                                        null,
//                                        null,
//                                        false, 
//                                        OAWebBeanConstants.ADD_BREAD_CRUMB_NO);
    }
  }

}
