CREATE OR REPLACE PACKAGE XXEIS.EIS_OM_XXWC_CONT_PRIC_PKG AS 

   Type CURSOR_TYPE is ref cursor;
   Type View_Tab Is Table Of XXEIS.eis_xxwc_cont_modi_pricing%Rowtype Index By Binary_Integer;
   G_View_Tab      View_Tab ;
   G_VIEW_RECORD View_Tab;
  -- G_VIEW_RECORD  XXEIS.EIS_XXWC_CONT_MODI_PRICING%ROWTYPE ;
   
   Type CURSOR_TYPE3 is ref cursor;
   Type View_Tab3 Is Table Of Xxeis.EIS_XXWC_ITEM_DATA%Rowtype Index By Binary_Integer;
   G_View_Tab3                   View_Tab3 ;
   G_VIEW_RECORD3                View_Tab3;
   
   Type CURSOR_TYPE2 is ref cursor;
   Type View_Tab2 Is Table Of Xxeis.EIS_XXWC_QUALIFIER_TMP%Rowtype Index By Binary_Integer;
   G_View_Tab2                    View_Tab2 ;
   G_VIEW_RECORD2                 View_Tab2;
   
   Type CURSOR_TYPE4 is ref cursor;
   TYPE View_tab4 IS TABLE OF XXEIS.eis_xxwc_cont_modi_datamart_v1%Rowtype Index By Binary_Integer;
   G_View_Tab4                    View_Tab4 ;
   G_VIEW_RECORD4                 View_Tab4;
   
   Type CURSOR_TYPE5 is ref cursor;
   TYPE View_tab5 IS TABLE OF XXEIS.EIS_XXWC_LIST_PRICE%Rowtype Index By Binary_Integer;
   G_View_Tab5                    View_Tab5 ;
   G_VIEW_RECORD5                 VIEW_TAB5;
   
    TYPE CURSOR_TYPE8 IS REF CURSOR;
   TYPE VIEW_TAB8 IS TABLE OF XXEIS.EIS_XXWC_LIST_PRICE%ROWTYPE INDEX BY BINARY_INTEGER;
   G_VIEW_TAB8                    VIEW_TAB8 ;
   G_VIEW_RECORD8                 View_Tab8;

   Type CURSOR_TYPE6 is ref cursor;
   TYPE View_tab6 IS TABLE OF XXEIS.EIS_XXWC_LIST_PRICE%Rowtype Index By Binary_Integer;
   G_View_Tab6                    View_Tab6 ;
   G_VIEW_RECORD6                 View_Tab6;
   
   Type CURSOR_TYPE7 is ref cursor;
   TYPE View_tab7 IS TABLE OF xxeis.EIS_XXWC_UNIT_SALES_DATA_GT%Rowtype Index By Binary_Integer;
   G_View_Tab7                    View_Tab7;
   G_VIEW_RECORD7                 View_Tab7;

procedure CONTRACT_PRICING_MODI_DATAMART (P_PROCESS_ID number DEFAULT 0);
                                      
PROCEDURE CLEAN_UP_PROCESS (P_PROCESS_ID NUMBER);

procedure CONTRACT_PRICING_MODIFIER  (         P_PROCESS_ID                     number,
                                               P_START_DATE                   in date default null,
                                               P_END_DATE                     IN DATE DEFAULT NULL,
                                              -- P_CURRENT_COST_LOC              varchar2,
                                               P_CATCLASS                     in varchar2 default null,
                                               P_CAT_PRICED_ITEMS             in varchar2  default null,
                                               P_RVP                          in varchar2  default null,
                                               P_DM                           in varchar2  default null,
                                               P_SALESPERSON                  in varchar2  default null,
                                               P_JOB_ACCOUNT                  varchar2,
                                               P_MASTER_ACCOUNT               varchar2,
                                               P_CUSTOMER_NAME                varchar2     default null,
                                               P_CUSTOMER_NUMBER              VARCHAR2     default null,
                                               P_MODIFIER_NAME                VARCHAR2     default null
                                               );
END EIS_OM_XXWC_CONT_PRIC_PKG;
/