CREATE OR REPLACE FORCE VIEW xxeis.eis_xxwc_cont_modi_pricing_v1
(
   modifier_name
  ,customer_name
  ,qualifier_attribute
  ,product
  ,description
  ,cat_class
  ,gm
  ,list_price
  ,app
  ,value1
  ,contract_price
  ,units_sold
  ,actual_sales
  ,actual_gross_margin
  ,current_location_cost
  ,new_price_or_new_gm_disc
  ,inventory_item_id
  ,list_header_id
  ,product_attribute_type
  ,category_id
  ,cust_account_id
  ,TYPE
  ,process_id
  ,arithmetic_operator
  ,operand
  ,unit_cost
  ,cat
  ,cat_desc
  ,price_by_cat
  ,contract_type
  ,formula_name
  ,salesrep
  ,cat_class_desc
  ,transaction_code
  ,creation_date
  ,person_id
  ,account_number
  ,common_output_id
  ,location
  ,party_site_number
  ,version_no
  ,price_type
)
AS
   SELECT varchar2_col1 modifier_name
         ,varchar2_col2 customer_name
         ,varchar2_col3 qualifier_attribute
         ,varchar2_col4 product
         ,varchar2_col5 description
         ,varchar2_col6 cat_class
         ,number_col1 gm
         ,number_col2 list_price
         ,varchar2_col7 app
         ,number_col3 value1
         ,number_col4 contract_price
         ,number_col5 units_sold
         ,number_col6 actual_sales
         ,number_col7 actual_gross_margin
         ,number_col8 current_location_cost
         ,number_col9 new_price_or_new_gm_disc
         ,number_col10 inventory_item_id
         ,number_col11 list_header_id
         ,varchar2_col8 product_attribute_type
         ,number_col13 category_id
         ,number_col14 cust_account_id
         ,varchar2_col9 TYPE
         ,process_id process_id
         ,varchar2_col10 arithmetic_operator
         ,number_col15 operand
         ,number_col16 unit_cost
         ,varchar2_col11 cat
         ,varchar2_col12 cat_desc
         ,varchar2_col13 price_by_cat
         ,varchar2_col14 contract_type
         ,varchar2_col15 formula_name
         ,varchar2_col16 salesrep
         ,varchar2_col17 cat_class_desc
         ,varchar2_col18 transaction_code
         ,varchar2_col19 creation_date
         ,number_col17 person_id
         ,varchar2_col20 account_number
         ,common_output_id common_output_id
         ,varchar2_col21 location
         ,varchar2_col22 party_site_number
         ,varchar2_col23 version_no
         ,varchar2_col24 price_type
     FROM xxeis.eis_rs_common_outputs;