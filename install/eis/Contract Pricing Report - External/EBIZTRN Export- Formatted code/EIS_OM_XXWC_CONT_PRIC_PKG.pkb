CREATE OR REPLACE PACKAGE BODY XXEIS.EIS_OM_XXWC_CONT_PRIC_PKG as

procedure CONTRACT_PRICING_MODI_DATAMART (P_PROCESS_ID number DEFAULT 0)  IS
  
  L_JOB_ACCOUNT_STR             VARCHAR2(32000);
  L_MASTER_ACCOUNT_STR          VARCHAR2(32000);
  L_BOTH_ACCOUNTS               varchar2(32000);
  L_INSERT_RECD_CNT             NUMBER;
  L_ERROR_COUNT                 NUMBER;
  L_SALESREP_STR                varchar2(32000);   
  l_count                       NUMBER;
  L_MAIN_SQL                    LONG;
  L_ITEM_NUMBER_BILL_SQL        LONG;
  L_ITEM_NUMBER_SHIP_SQL        LONG;
  L_ITEM_CAT_SUMM_SHIP_SQL      LONG;
  L_ITEM_CAT_SUMM_BILL_SQL      LONG;
  L_ITEM_CAT_SHIP_SQL           LONG;
  L_ITEM_CAT_BILL_SQL           LONG;
  L_UPDATE_ITEM_NUM             long;
  L_UPDATE_ITEM_CAT             long;
  L_UPDATE_ITEM_CAT1            long;
  L_UPDATE_ITEM_CAT2            long;
  L_UPDATE_ITEM_CAT3            long;
  L_UNIT_SALES_DATA             long;
  L_START_DATE                  varchar2(32000);
  l_END_DATE                    varchar2(32000);
  l_CUSTOMER_NAME               varchar2(32000);
  l_CUSTOMER_NUMBER             varchar2(32000);
  l_modifier_data               varchar2(32000);
  l_bill_to_modifier_data       varchar2(32000);
  l_ship_to_modifier_data       varchar2(32000);
  L_ITEM_DATA                   VARCHAR2(32000);
  L_LIST_PRICE_SQL              LONG; 
  l_list_price_sql1              long; 
  l_max_item_data               Xxeis.EIS_XXWC_ITEM_DATA%Rowtype; 
  l_REF_CURSOR7                 CURSOR_TYPE7;  
  L_REF_CURSOR5                 CURSOR_TYPE5;
  L_REF_CURSOR8                 CURSOR_TYPE8;
  L_REF_CURSOR3                 CURSOR_TYPE3;
  L_REF_CURSOR2                 CURSOR_TYPE2;
  L_REF_CURSOR                  CURSOR_TYPE;
  l_list_price                  number;
  l_max_list_price              number;
  l_max_line_id                 number;
  l_contract_price              number;
 
  CURSOR xxcur_max_item_data(p_item_id IN NUMBER) IS
         SELECT *
           FROM Xxeis.EIS_XXWC_ITEM_DATA xxid
          WHERE xxid.inventory_item_id = p_item_id ; 
          
  CURSOR xxcur_list_price(p_inventory_item_id IN NUMBER, p_product_attribute_type IN VARCHAR2) IS
         SELECT distinct excmp.list_price
           from XXEIS.eis_xxwc_cont_modi_pricing excmp
          WHERE excmp.inventory_item_id = p_inventory_item_id
            AND excmp.product_attribute_type = p_product_attribute_type;
            
 /* CURSOR xxcur_distinct_item IS
        SELECT distinct excmp.inventory_item_id,xlpt.list_price
           FROM XXEIS.eis_xxwc_cont_modi_pricing excmp
               ,XXEIS.EIS_XXWC_LIST_PRICE xlpt
          WHERE excmp.inventory_item_id !=0
            AND xlpt.inventory_item_id = excmp.inventory_item_id;*

  CURSOR xxcur_list_price_update(p_inventory_item_id IN NUMBER) IS
         SELECT excmp.operand
               ,excmp.arithmetic_operator
               ,excmp.app
               ,excmp.common_output_id
           FROM XXEIS.eis_xxwc_cont_modi_pricing excmp
          WHERE excmp.inventory_item_id = p_inventory_item_id;*/
    
begin
  fnd_file.put_line(fnd_file.log,'Truncate Start'|| to_char(sysdate,'DD-MON-YYYY HH:MI:SS') );
  EXECUTE IMMEDIATE 'truncate table  Xxeis.EIS_XXWC_ITEM_DATA';
  EXECUTE IMMEDIATE 'truncate table  Xxeis.EIS_XXWC_QUALIFIER_TMP';
  EXECUTE IMMEDIATE 'truncate table  XXEIS.eis_xxwc_cont_modi_pricing';
  EXECUTE IMMEDIATE 'truncate table  XXEIS.EIS_XXWC_LIST_PRICE';
  EXECUTE IMMEDIATE 'truncate table  xxeis.EIS_XXWC_UNIT_SALES_DATA';
  fnd_file.put_line(fnd_file.log,'Truncate End null insert'|| to_char(sysdate,'DD-MON-YYYY HH:MI:SS') );
  
  L_SALESREP_STR:='And 1=1';
  L_INSERT_RECD_CNT:=1;
  L_START_DATE:=' and 1=1';
  L_END_DATE:=' and 1=1';
  l_customer_name:=' and 1=1';
  l_ship_to_modifier_data  := 'SELECT QH.name MODIFIER_NAME
                            ,HP.PARTY_NAME CUSTOMER_NAME
                            ,QUAL.QUALIFIER_ATTRIBUTE QUALIFIER_ATTRIBUTE
                            ,qh.list_header_id
                            ,HCA.CUST_ACCOUNT_ID
                            ,REP.name SalesRep
                            ,Rep.person_id 
                            ,hca.account_number
                            ,hcsu.location
                            ,hps.party_site_number
                            ,qh.version_no
                            ,case WHEN (select count(*)
                                         from apps.xxwc_om_contract_pricing_hdr 
                                         Where agreement_id = case when instr(qh.version_no,''.'',1) > 0 
                                                              THEN to_number(substr(qh.version_no,1,instr(qh.version_no,''.'',1)-1))                                         
                                                              else 0
                                                              END                                         
                                           AND price_type =''NATIONAL'')   > 0 THEN
                              ''NATIONAL''
                             ELSE ''SHIPTO'' 
                             end  price_type
                        FROM QP_LIST_HEADERS_VL      QH
                            ,QP_QUALIFIERS           QUAL
                            ,HZ_PARTIES              HP
                            ,HZ_CUST_ACCOUNTS        HCA
                            ,HZ_CUST_ACCT_SITES_all          HCAS
                            ,HZ_CUST_SITE_USES_all           HCSU
                            ,hz_party_sites hps
                            ,Ra_Salesreps                REP
                       WHERE QH.attribute10                    = ''Contract Pricing''
                         AND HP.PARTY_ID                       = HCA.PARTY_ID 
                         AND HCA.CUST_ACCOUNT_ID               = HCAS.CUST_ACCOUNT_ID              
                         AND QH.LIST_HEADER_ID                 = QUAL.LIST_HEADER_ID 
                         AND trunc(NVL(qh.end_date_active,sysdate))   >= trunc(sysdate)
                         AND trunc(NVL(QUAL.end_date_active,sysdate))   >= trunc(sysdate)
                         AND HCA.STATUS                        = ''A''
                         AND QUAL.QUALIFIER_ATTRIBUTE          = ''QUALIFIER_ATTRIBUTE11''
                         AND QUAL.QUALIFIER_CONTEXT            = ''CUSTOMER''
                         AND QUAL.comparison_operator_code     = ''=''    
                         AND QUAL.QUALIFIER_ATTR_VALUE         = HCSU.SITE_USE_ID   
                         AND QH.ACTIVE_FLAG                    = ''Y''
                         AND HCAS.CUST_ACCT_SITE_ID            = HCSU.CUST_ACCT_SITE_ID  
                         AND HCSU.org_id                       = rep.org_id(+)
                         AND hcas.party_site_id                = hps.party_site_id
                         AND HCSU.PRIMARY_SALESREP_ID          = REP.SALESREP_ID(+) ';

  l_bill_to_modifier_data  := 'SELECT  QH.name MODIFIER_NAME
                            ,HP.PARTY_NAME CUSTOMER_NAME
                            ,QUAL.QUALIFIER_ATTRIBUTE QUALIFIER_ATTRIBUTE
                            ,qh.list_header_id
                            ,HCA.CUST_ACCOUNT_ID
                            ,REP.name SalesRep
                            ,Rep.person_id 
                            ,hca.account_number
                            ,null location
                            ,null party_site_number
                            ,qh.version_no
                            ,case WHEN (select count(*)
                                         from apps.xxwc_om_contract_pricing_hdr 
                                         Where agreement_id = case when instr(qh.version_no,''.'',1) > 0 
                                                              THEN to_number(substr(qh.version_no,1,instr(qh.version_no,''.'',1)-1))                                         
                                                              else 0
                                                              END                                         
                                           AND price_type =''NATIONAL'')   > 0 THEN
                              ''NATIONAL''
                             ELSE ''MASTER'' 
                             end  price_type
                      FROM  QP_LIST_HEADERS_VL      QH
                           ,QP_QUALIFIERS           QUAL
                           ,HZ_PARTIES              HP
                           ,HZ_CUST_ACCOUNTS        HCA
                           ,HZ_CUST_ACCT_SITES_all  HCAS
                           ,HZ_CUST_SITE_USES_all   HCSU
                           ,Ra_Salesreps                REP
                      WHERE QH.attribute10                   = ''Contract Pricing''
                        AND QH.LIST_HEADER_ID                 = QUAL.LIST_HEADER_ID 
                        AND trunc(NVL(qh.end_date_active,sysdate))   >= trunc(sysdate)
                        AND trunc(NVL(QUAL.end_date_active,sysdate))   >= trunc(sysdate)
                        AND QUAL.QUALIFIER_CONTEXT            = ''CUSTOMER''
                        AND QUAL.comparison_operator_code     = ''=''
                        AND HP.PARTY_ID                       = HCA.PARTY_ID
                        AND HCA.STATUS                        = ''A''
                        AND (QUAL.QUALIFIER_ATTRIBUTE         = ''QUALIFIER_ATTRIBUTE32'' 
                        OR  QUAL.QUALIFIER_ATTRIBUTE          = ''QUALIFIER_ATTRIBUTE14'')
                        AND QUAL.QUALIFIER_ATTR_VALUE         = HCA.CUST_ACCOUNT_ID               
                        AND HCA.CUST_ACCOUNT_ID               = HCAS.CUST_ACCOUNT_ID              
                        AND HCAS.CUST_ACCT_SITE_ID            = HCSU.CUST_ACCT_SITE_ID 
                        AND HCSU.SITE_USE_CODE                = ''BILL_TO''
                        AND HCSU.primary_flag                 = ''Y''
                        AND QH.ACTIVE_FLAG                    = ''Y''
                        AND HCSU.org_ID                       = REP.org_id(+)
                        AND HCSU.PRIMARY_SALESREP_ID          = REP.SALESREP_ID(+) ';


  l_item_data := ' SELECT max(msi.segment1) item
                             ,max(msi.DESCRIPTION) item_desc
                             ,max(mcv.segment1) cat
                             ,max(ffv.Description) cat_desc
                             ,max(mcv.CONCATENATED_SEGMENTS) cat_class
                             ,max(ffv.description ||''.''||ffv.description) cat_class_desc
                             ,max(mcv.category_id) category_id
                             ,msi.inventory_item_id
                             ,min(msi.organization_id) organization_id
                        from  Mtl_Categories_Kfv Mcv
                             ,Mtl_Item_Categories Mic
                             ,mtl_system_items_b msi
                             ,fnd_flex_values_vl ffv
                             ,fnd_flex_value_sets ffvs
                             ,fnd_flex_values_vl ffv1
                             ,fnd_flex_value_sets ffvs1
                             ,mtl_parameters mp
                        Where mcv.Structure_Id           = 101
                          AND mic.category_set_id        = 1100000062
                          And Mic.Organization_Id        = msi.organization_id
                          AND mic.inventory_item_id      = msi.inventory_item_id
                          And Mic.Category_Id            = Mcv.Category_Id
                          And ffv.flex_value             = mcv.Segment1
                          AND ffvs.Flex_Value_Set_Id     = ffv.Flex_Value_Set_Id
                          And ffv1.flex_value            = mcv.Segment2
                          AND ffvs1.Flex_Value_Set_Id    = ffv1.Flex_Value_Set_Id
                          AND msi.organization_id        = mp.organization_id
                          And ffvs.Flex_Value_Set_Name   = ''XXWC_CATEGORY_NUMBER''
                          And ffvs1.Flex_Value_Set_Name  = ''XXWC_CATEGORY_CLASS''
                          AND mp.organization_code   NOT IN(''CAN'',''HDS'',''US1'',''CN1'',''MST'',''WCC'')
                  group by msi.inventory_item_id  ';

  fnd_file.put_line(fnd_file.log,'The sql query ITEM Data is'||l_item_data );
  fnd_file.put_line(fnd_file.log,'Item Data Start'|| to_char(sysdate,'DD-MON-YYYY HH:MI:SS') );

  OPEN l_ref_cursor3 for l_item_data;
  LOOP
    FETCH L_REF_CURSOR3 BULK COLLECT INTO G_VIEW_RECORD3 LIMIT 10000;
      l_count := 0;
      FOR j in 1..G_VIEW_RECORD3.count 
      Loop
        G_VIEW_TAB3(j).ITEM              :=  G_VIEW_RECORD3(j).ITEM; 
        G_VIEW_TAB3(j).ITEM_DESC         :=  G_VIEW_RECORD3(j).ITEM_DESC; 
        G_VIEW_TAB3(j).CAT               :=  G_VIEW_RECORD3(j).CAT; 
        G_VIEW_TAB3(j).CAT_DESC          :=  G_VIEW_RECORD3(j).CAT_DESC ; 
        G_VIEW_TAB3(j).CAT_CLASS         :=  G_VIEW_RECORD3(j).CAT_CLASS; 
        G_VIEW_TAB3(j).CAT_CLASS_DESC    :=  G_VIEW_RECORD3(j).CAT_CLASS_DESC ; 
        G_VIEW_TAB3(j).CATEGORY_ID       :=  G_VIEW_RECORD3(j).CATEGORY_ID ; 
        G_VIEW_TAB3(j).INVENTORY_ITEM_ID :=  G_VIEW_RECORD3(j).INVENTORY_ITEM_ID; 
        G_VIEW_TAB3(j).ORGANIZATION_ID   :=  G_VIEW_RECORD3(j).ORGANIZATION_ID ; 
            
        l_count := l_count+1;
      END LOOP;   
      IF l_count >= 1  then
        forall j in 1 .. G_VIEW_TAB3.count 
           INSERT INTO xxeis.EIS_XXWC_ITEM_DATA
                values g_view_tab3 (j);    
           COMMIT;   
      end if;  
      G_VIEW_TAB3.delete;
      G_VIEW_RECORD3.delete;
      IF l_ref_cursor3%NOTFOUND Then
        CLOSE l_ref_cursor3;
        EXIT;
      End if;
  END LOOP;  
  fnd_file.put_line(fnd_file.log,'Item Data End'|| to_char(sysdate,'DD-MON-YYYY HH:MI:SS') );
  
  L_ITEM_NUMBER_SHIP_SQL:=' SELECT  qtt.MODIFIER_NAME MODIFIER_NAME,
                                    qtt.CUSTOMER_NAME CUSTOMER_NAME,
                                    DECODE(qtt.QUALIFIER_ATTRIBUTE,''QUALIFIER_ATTRIBUTE11'',''Ship To'')QUALIFIER_ATTRIBUTE, 
                                    TO_CHAR(null)                PRODUCT,
                                    TO_CHAR(null)             DESCRIPTION,
                                    TO_CHAR(null)            CAT_CLASS,        
                                    0 GM ,
                                    0 LIST_PRICE,
                                    decode(price_by_formula_id, null,apps.qp_qp_form_pricing_attr.get_meaning(ql.arithmetic_operator, ''ARITHMETIC_OPERATOR''), ''Cost Plus'') app,
                                    QL.OPERAND VALUE,
                                    0 CONTRACT_PRICE,
                                    0 units_sold,
                                    0 actual_sales,
                                    0 ACTUAL_GROSS_MARGIN,
                                    ROUND(APPS.CST_COST_API.GET_ITEM_COST(1,QPA.PRODUCT_ATTR_VALUE,XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_COST_LOCATION),2) CURRENT_LOCATION_COST,
                                    TO_CHAR(null) NEW_PRICE_OR_NEW_GM_DISC,
                                    to_number(QPA.PRODUCT_ATTR_VALUE) inventory_item_id,
                                    qtt.list_header_id ,
                                    QPA.PRODUCT_ATTRIBUTE PRODUCT_ATTRIBUTE_TYPE,
                                    0 CATEGORY_ID,
                                    qtt.CUST_ACCOUNT_ID,
                                    ''Item_Number'' TYPE,
                                    '||P_PROCESS_ID||' PROCESS_ID,
                                    ql.ARITHMETIC_OPERATOR ,
                                    ql.OPERAND ,
                                    0 unit_cost,
                                    TO_CHAR(null)  CAT,
                                    TO_CHAR(null) cat_desc,
                                    ''No'' PRICE_BY_CAT,
                                    ''NULL'' FORMULA_NAME,
                                    QL.attribute5 contract_type,
                                    qtt.SalesRep SalesRep,
                                    TO_CHAR(null) cat_class_desc,
                                    ''1'' Transaction_Code,
                                    ''NULL'' CREATION_DATE,
                                    qtt.person_id,
                                    qtt.account_number,
                                    0    common_output_id,
                                    qtt.location,
                                    qtt.party_site_number,
                                    qtt.version_no,
                                    qtt.price_type
                               FROM QP_LIST_LINES           QL,
                                    QP_pricing_attributeS   QPA,
                                    xxeis.EIS_XXWC_QUALIFIER_TMP qtt
                             WHERE QL.LIST_HEADER_ID               = qtt.list_header_id
                               AND QL.PRICING_PHASE_ID               > 1
                               AND QL.QUALIFICATION_IND          IN (4,6,20,22,24)
                               AND (ql.end_date_active is null or 
                                    ql.end_date_active >= sysdate)                               
                               AND QL.LIST_LINE_ID                   = QPA.LIST_LINE_ID(+)
                               AND QL.MODIFIER_LEVEL_CODE            = ''LINE''
                               AND QPA.PRODUCT_ATTRIBUTE_CONTEXT     = ''ITEM''
                               AND QPA.PRODUCT_ATTRIBUTE             = ''PRICING_ATTRIBUTE1''
                               AND QTT.QUALIFIER_ATTRIBUTE           = ''QUALIFIER_ATTRIBUTE11''';

  L_ITEM_NUMBER_BILL_SQL:='
    SELECT   qtt.MODIFIER_NAME MODIFIER_NAME,
        qtt.CUSTOMER_NAME CUSTOMER_NAME,
        DECODE(qtt.QUALIFIER_ATTRIBUTE,''QUALIFIER_ATTRIBUTE32'',''Bill To Customer'',''QUALIFIER_ATTRIBUTE14'',''Bill To'')QUALIFIER_ATTRIBUTE, 
        TO_CHAR(null)               PRODUCT,
        TO_CHAR(null)            DESCRIPTION,
        TO_CHAR(null)   CAT_CLASS,        
        0 GM ,
        0 LIST_PRICE,
        decode(price_by_formula_id, null,apps.qp_qp_form_pricing_attr.get_meaning(ql.arithmetic_operator, ''ARITHMETIC_OPERATOR''), ''Cost Plus'') app,
        QL.OPERAND VALUE,
        0 CONTRACT_PRICE,
        0 units_sold,
        0 actual_sales,
        0 ACTUAL_GROSS_MARGIN,
        ROUND(APPS.CST_COST_API.GET_ITEM_COST(1,QPA.PRODUCT_ATTR_VALUE ,XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_COST_LOCATION),2) CURRENT_LOCATION_COST,
        TO_CHAR(null) NEW_PRICE_OR_NEW_GM_DISC,
        to_number(QPA.PRODUCT_ATTR_VALUE) inventory_item_id,
        qtt.list_header_id,
        QPA.PRODUCT_ATTRIBUTE PRODUCT_ATTRIBUTE_TYPE,
        0 CATEGORY_ID,
        qtt.CUST_ACCOUNT_ID,
        ''Item_Number'' TYPE,
        '||P_PROCESS_ID||' PROCESS_ID,
        ql.ARITHMETIC_OPERATOR ,
        ql.OPERAND ,
        0 unit_cost,
        TO_CHAR(null)  CAT,
        TO_CHAR(null) cat_desc,
        ''No'' PRICE_BY_CAT,
        ''NULL'' FORMULA_NAME,
        QL.attribute5 contract_type,
        qtt.SalesRep SalesRep,
        TO_CHAR(null) cat_class_desc,
        ''2'' Transaction_Code,
        ''NULL'' CREATION_DATE,
        qtt.person_id,
        qtt.account_number,
        0    common_output_id,
        qtt.location,
        qtt.party_site_number,
        qtt.version_no,
        qtt.price_type
  FROM  QP_LIST_LINES           QL,
        QP_pricing_attributeS   QPA,
        XXEIS.EIS_XXWC_QUALIFIER_TMP qtt
  WHERE QL.LIST_HEADER_ID                = qtt.LIST_HEADER_ID               
    AND QL.PRICING_PHASE_ID               > 1
    AND QL.QUALIFICATION_IND          IN (4,6,20,22,24)
    AND QL.LIST_LINE_ID                   = QPA.LIST_LINE_ID(+)
    AND (ql.end_date_active is null or 
         ql.end_date_active >= sysdate) 
    AND QL.MODIFIER_LEVEL_CODE            = ''LINE''
    AND QPA.PRODUCT_ATTRIBUTE_CONTEXT     = ''ITEM''
    AND QPA.PRODUCT_ATTRIBUTE             = ''PRICING_ATTRIBUTE1''
    AND (QTT.QUALIFIER_ATTRIBUTE           = ''QUALIFIER_ATTRIBUTE14''
     OR QTT.QUALIFIER_ATTRIBUTE           = ''QUALIFIER_ATTRIBUTE32'')';

  L_ITEM_CAT_SUMM_SHIP_SQL:='
    SELECT  
      qtt.MODIFIER_NAME  MODIFIER_NAME,
      qtt.CUSTOMER_NAME CUSTOMER_NAME,
      DECODE(qtt.QUALIFIER_ATTRIBUTE,''QUALIFIER_ATTRIBUTE11'',''Ship To'')QUALIFIER_ATTRIBUTE, 
      TO_CHAR(null)  PRODUCT,
      TO_CHAR(NULL)  DESCRIPTION,
      MCK.CONCATENATED_SEGMENTS  CAT_CLASS,
      XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_INV_GM(NULL,XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_COST_LOCATION, to_number(QPA.PRODUCT_ATTR_VALUE))GM ,
      0  LIST_PRICE,
      decode(price_by_formula_id, null,apps.qp_qp_form_pricing_attr.get_meaning(ql.arithmetic_operator, ''ARITHMETIC_OPERATOR''), ''Cost Plus'') app,
      QL.operand value,
      0 CONTRACT_PRICE,
      0 units_sold,
      0 actual_sales,
      0 ACTUAL_GROSS_MARGIN,
      0 CURRENT_LOCATION_COST,
      to_char(null) NEW_PRICE_OR_NEW_GM_DISC,
      to_number(0) inventory_item_id,
      qtt.list_header_id,
      QPA.PRODUCT_ATTRIBUTE PRODUCT_ATTRIBUTE_TYPE,
      to_number(QPA.PRODUCT_ATTR_VALUE) CATEGORY_ID,
      qtt.CUST_ACCOUNT_ID CUST_ACCOUNT_ID,
     ''Item_Summary'' TYPE,
     '||P_PROCESS_ID||' PROCESS_ID,
      ql.ARITHMETIC_OPERATOR ,
      ql.OPERAND ,
      0 unit_cost,
      MCK.SEGMENT1          CAT,
      Flex_Val.Description cat_desc,
      ''No'' PRICE_BY_CAT,
      '''' FORMULA_NAME,
      QL.attribute5 contract_type,
      qtt.SalesRep SalesRep,
      Flex_Val.Description || ''.'' ||Ffv1.Description cat_class_desc,
      ''3'' Transaction_Code,
      ''NULL'' CREATION_DATE,
        qtt.person_id,
        qtt.account_number,
        0    common_output_id,
        qtt.location,
        qtt.party_site_number,
        qtt.version_no,
        qtt.price_type 
  FROM  QP_LIST_LINES               QL,
        QP_PRICING_ATTRIBUTES       QPA,
        MTL_CATEGORIES_KFV          MCK,
        fnd_flex_values_vl          flex_val,
        fnd_flex_value_sets         flex_name,
        fnd_flex_values_vl          ffv1,
        fnd_flex_value_sets         ffvs1,
        XXEIS.EIS_XXWC_QUALIFIER_TMP qtt
  WHERE qtt.LIST_HEADER_ID                     = QL.LIST_HEADER_ID
    AND QL.PRICING_PHASE_ID               > 1
    AND QL.QUALIFICATION_IND          IN (0,2,4,6,8,10,12,14,16,18,20,22,24,26, 28, 30, 32)
    AND QL.LIST_LINE_ID                       = QPA.LIST_LINE_ID(+)
    AND (ql.end_date_active is null or 
                                    ql.end_date_active >= sysdate) 
    AND QL.MODIFIER_LEVEL_CODE                = ''LINE''
    AND QPA.PRODUCT_ATTRIBUTE_CONTEXT         = ''ITEM''
    AND QPA.PRODUCT_ATTRIBUTE                 = ''PRICING_ATTRIBUTE2''
    AND (QPA.PRICING_ATTRIBUTE_CONTEXT        = ''VOLUME'' OR QPA.PRICING_ATTRIBUTE_CONTEXT  IS NULL)
    AND MCK.CATEGORY_ID                       = QPA.PRODUCT_ATTR_VALUE
    And flex_val.Flex_Value                   = MCK.Segment1
    AND Flex_Name.Flex_Value_Set_Id           = Flex_Val.Flex_Value_Set_Id
    And Flex_Name.Flex_Value_Set_Name         = ''XXWC_CATEGORY_NUMBER'' 
    And ffv1.flex_value                       = mck.Segment2
    AND ffvs1.Flex_Value_Set_Id               = ffv1.Flex_Value_Set_Id
    And ffvs1.Flex_Value_Set_Name             = ''XXWC_CATEGORY_CLASS''
    AND QTT.QUALIFIER_ATTRIBUTE           = ''QUALIFIER_ATTRIBUTE11''';

L_ITEM_CAT_SUMM_BILL_SQL:='
SELECT  
      qtt.MODIFIER_NAME MODIFIER_NAME,
      qtt.CUSTOMER_NAME CUSTOMER_NAME,
      DECODE(qtt.QUALIFIER_ATTRIBUTE,''QUALIFIER_ATTRIBUTE32'',''Bill To Customer'',''QUALIFIER_ATTRIBUTE14'',''Bill To'')QUALIFIER_ATTRIBUTE, 
      TO_CHAR(null)  PRODUCT,
      TO_CHAR(NULL)  DESCRIPTION,
      MCK.CONCATENATED_SEGMENTS  CAT_CLASS,
      XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_INV_GM(NULL,XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_COST_LOCATION, to_number(QPA.PRODUCT_ATTR_VALUE))GM ,
      0  LIST_PRICE,
      decode(price_by_formula_id, null,apps.qp_qp_form_pricing_attr.get_meaning(ql.arithmetic_operator, ''ARITHMETIC_OPERATOR''), ''Cost Plus'') app,
      QL.operand value,
      0 CONTRACT_PRICE,
      0 units_sold,
      0 actual_sales,
      0 ACTUAL_GROSS_MARGIN,
      0 CURRENT_LOCATION_COST,
      to_char(null) NEW_PRICE_OR_NEW_GM_DISC,
      to_number(0)inventory_item_id,
      qtt.list_header_id,
      QPA.PRODUCT_ATTRIBUTE PRODUCT_ATTRIBUTE_TYPE,
      to_number(QPA.PRODUCT_ATTR_VALUE) CATEGORY_ID,
      qtt.CUST_ACCOUNT_ID CUST_ACCOUNT_ID,
     ''Item_Summary'' TYPE,
      '||P_PROCESS_ID||' PROCESS_ID,
      ql.ARITHMETIC_OPERATOR ,
      ql.OPERAND ,
      0 unit_cost,
      MCK.SEGMENT1          CAT,
      Flex_Val.Description cat_desc,      
      ''No'' PRICE_BY_CAT,
      '''' FORMULA_NAME,
      QL.attribute5 contract_type,
      qtt.SalesRep SalesRep,
      Flex_Val.Description || ''.'' ||Ffv1.Description cat_class_desc,
      ''4'' Transaction_Code,
      ''NULL'' CREATION_DATE,
        qtt.person_id,
        qtt.account_number,
        0    common_output_id,
       qtt.location,
       qtt.party_site_number,
       qtt.version_no,
       qtt.price_type 
FROM 
      QP_LIST_LINES           QL,
      QP_PRICING_ATTRIBUTES   QPA,
      MTL_CATEGORIES_KFV      MCK,
      fnd_flex_values_vl flex_val,
      fnd_flex_value_sets flex_name,
      fnd_flex_values_vl          ffv1,
      fnd_flex_value_sets         ffvs1,
      xxeis.EIS_XXWC_QUALIFIER_TMP qtt
WHERE qtt.LIST_HEADER_ID                     = QL.LIST_HEADER_ID
  AND QL.PRICING_PHASE_ID               > 1
  AND QL.QUALIFICATION_IND          IN (0,2,4,6,8,10,12,14,16,18,20,22,24,26, 28, 30, 32)
  AND QL.LIST_LINE_ID                       = QPA.LIST_LINE_ID(+)
  AND (ql.end_date_active is null or 
                                    ql.end_date_active >= sysdate) 
  AND QL.MODIFIER_LEVEL_CODE                = ''LINE''
  AND QPA.PRODUCT_ATTRIBUTE_CONTEXT         = ''ITEM''
  AND QPA.PRODUCT_ATTRIBUTE                 = ''PRICING_ATTRIBUTE2'' 
  AND MCK.CATEGORY_ID                      = QPA.PRODUCT_ATTR_VALUE
  And flex_val.Flex_Value                   = MCK.Segment1
  AND Flex_Name.Flex_Value_Set_Id           = Flex_Val.Flex_Value_Set_Id
  And Flex_Name.Flex_Value_Set_Name         = ''XXWC_CATEGORY_NUMBER''
  And ffv1.flex_value                       = mck.Segment2
  AND ffvs1.Flex_Value_Set_Id               = ffv1.Flex_Value_Set_Id
  And ffvs1.Flex_Value_Set_Name             = ''XXWC_CATEGORY_CLASS''
  AND (QTT.QUALIFIER_ATTRIBUTE           = ''QUALIFIER_ATTRIBUTE14''
     OR QTT.QUALIFIER_ATTRIBUTE           = ''QUALIFIER_ATTRIBUTE32'')
   ';

L_ITEM_CAT_SHIP_SQL :='
 select 
      MODIFIER_NAME,
      CUSTOMER_NAME,
      QUALIFIER_ATTRIBUTE,
      PRODUCT,
      DESCRIPTION,      
      CAT_CLASS,
      max(GM) GM,
      max(LIST_PRICE) LIST_PRICE,
      APP,
      max(value) value,
      max(CONTRACT_PRICE) CONTRACT_PRICE,
      max(UNITS_SOLD)UNITS_SOLD,
      max(ACTUAL_SALES) ACTUAL_SALES,
      max(to_number(ACTUAL_GROSS_MARGIN)) ACTUAL_GROSS_MARGIN,
      max(CURRENT_LOCATION_COST) CURRENT_LOCATION_COST,
      max(NEW_PRICE_OR_NEW_GM_DISC),
      max(to_number(INVENTORY_ITEM_ID)) INVENTORY_ITEM_ID,
      max(list_header_id) list_header_id,
      max(PRODUCT_ATTRIBUTE_TYPE)PRODUCT_ATTRIBUTE_TYPE,
      max(CATEGORY_ID) CATEGORY_ID,
      max(CUST_ACCOUNT_ID) CUST_ACCOUNT_ID,
      MAX(TYPE) TYPE,
      MAX(PROCESS_ID) PROCESS_ID,
      MAX(ARITHMETIC_OPERATOR) ARITHMETIC_OPERATOR,
      MAX(OPERAND) OPERAND,
      MAX(unit_cost) unit_cost,
      CAT,
      CAT_DESC,
      PRICE_BY_CAT,
      FORMULA_NAME,
      contract_type,
      SalesRep, 
      cat_class_desc,
      ''3'' Transaction_Code,
      ''NULL'' CREATION_DATE,
      person_id,
      account_number,
      common_output_id,
      location,
      party_site_number,
      version_no,
      price_type
FROM(
SELECT  
      qtt.MODIFIER_NAME  MODIFIER_NAME,
      qtt.CUSTOMER_NAME CUSTOMER_NAME,
      DECODE(qtt.QUALIFIER_ATTRIBUTE,''QUALIFIER_ATTRIBUTE11'',''Ship To'')QUALIFIER_ATTRIBUTE, 
      TO_CHAR(null)  PRODUCT,
      TO_CHAR(NULL)  DESCRIPTION,
      TO_CHAR(NULL) CAT_CLASS,
      to_number(0)  GM ,
      0  LIST_PRICE,
      decode(price_by_formula_id, null,apps.qp_qp_form_pricing_attr.get_meaning(ql.arithmetic_operator, ''ARITHMETIC_OPERATOR''), ''Cost Plus'') app,
      QL.operand value,
      0 CONTRACT_PRICE,
      0 units_sold,
      0 actual_sales,
      0 ACTUAL_GROSS_MARGIN,
      0 CURRENT_LOCATION_COST,
      to_char(null) NEW_PRICE_OR_NEW_GM_DISC,
      to_number(0) inventory_item_id,
      qtt.list_header_id,
      QPA.PRODUCT_ATTRIBUTE PRODUCT_ATTRIBUTE_TYPE,
      to_number(0) CATEGORY_ID,
      qtt.CUST_ACCOUNT_ID CUST_ACCOUNT_ID,
     ''Item_Summary'' TYPE,
     '||P_PROCESS_ID||' PROCESS_ID,
      ql.ARITHMETIC_OPERATOR ,
      ql.OPERAND ,
      0 unit_cost,
      MCK.SEGMENT1          CAT,
      Flex_Val.Description cat_desc,
      ''Yes'' PRICE_BY_CAT,
      '''' FORMULA_NAME,
      QL.attribute5 contract_type,
      qtt.SalesRep SalesRep,
      to_char(NULL) cat_class_desc,
      ''3'' Transaction_Code,
      ''NULL'' CREATION_DATE,
        qtt.person_id,
        qtt.account_number,
        0    common_output_id,
        qtt.location,
        qtt.party_site_number,
        qtt.version_no,
        qtt.price_type
  FROM 
      QP_LIST_LINES               QL,
      QP_PRICING_ATTRIBUTES       QPA,
      MTL_CATEGORIES_KFV          MCK,
      fnd_flex_values_vl          flex_val,
      fnd_flex_value_sets         flex_name,
      XXEIS.EIS_XXWC_QUALIFIER_TMP qtt
WHERE qtt.LIST_HEADER_ID                     = QL.LIST_HEADER_ID
  AND QL.PRICING_PHASE_ID               > 1
  AND QL.QUALIFICATION_IND          IN (0,2,4,6,8,10,12,14,16,18,20,22,24,26, 28, 30, 32)
  --AND QL.QUALIFICATION_IND          IN (4,6,20,22,24)
  AND QL.LIST_LINE_ID                       = QPA.LIST_LINE_ID(+)
  AND (ql.end_date_active is null or 
                                    ql.end_date_active >= sysdate) 
  AND QL.MODIFIER_LEVEL_CODE                = ''LINE''
  AND QPA.PRODUCT_ATTRIBUTE_CONTEXT         = ''ITEM''
  AND QPA.PRODUCT_ATTRIBUTE                 = ''PRICING_ATTRIBUTE26''
  AND (QPA.PRICING_ATTRIBUTE_CONTEXT        = ''VOLUME'' OR QPA.PRICING_ATTRIBUTE_CONTEXT  IS NULL)
  AND MCK.segment1                          = QPA.PRODUCT_ATTR_VALUE
  And flex_val.Flex_Value                   = MCK.Segment1
  AND Flex_Name.Flex_Value_Set_Id           = Flex_Val.Flex_Value_Set_Id
  And Flex_Name.Flex_Value_Set_Name         = ''XXWC_CATEGORY_NUMBER'' 
  AND QTT.QUALIFIER_ATTRIBUTE               = ''QUALIFIER_ATTRIBUTE11''
  ) 
  GROUP BY MODIFIER_NAME, CUSTOMER_NAME, QUALIFIER_ATTRIBUTE, PRODUCT, DESCRIPTION, CAT_CLASS,CAT, CAT_DESC,PRICE_BY_CAT,FORMULA_NAME, APP,
   contract_type,SalesRep, cat_class_desc,person_id,account_number,common_output_id,location,party_site_number,version_no,price_type  
 ';

L_ITEM_CAT_BILL_SQL :='
 select 
      MODIFIER_NAME,
      CUSTOMER_NAME,
      QUALIFIER_ATTRIBUTE,
      PRODUCT,
      DESCRIPTION,      
      CAT_CLASS,
      max(GM) GM,
      max(LIST_PRICE) LIST_PRICE,
      APP,
      max(value) value,
      max(CONTRACT_PRICE) CONTRACT_PRICE,
      max(UNITS_SOLD)UNITS_SOLD,
      max(ACTUAL_SALES) ACTUAL_SALES,
      max(to_number(ACTUAL_GROSS_MARGIN)) ACTUAL_GROSS_MARGIN,
      max(CURRENT_LOCATION_COST) CURRENT_LOCATION_COST,
      max(NEW_PRICE_OR_NEW_GM_DISC),
      max(to_number(INVENTORY_ITEM_ID)) INVENTORY_ITEM_ID,
      max(list_header_id) list_header_id,
      max(PRODUCT_ATTRIBUTE_TYPE)PRODUCT_ATTRIBUTE_TYPE,
      max(CATEGORY_ID) CATEGORY_ID,
      max(CUST_ACCOUNT_ID) CUST_ACCOUNT_ID,
      MAX(TYPE) TYPE,
      MAX(PROCESS_ID) PROCESS_ID,
      MAX(ARITHMETIC_OPERATOR) ARITHMETIC_OPERATOR,
      MAX(OPERAND) OPERAND,
      MAX(unit_cost) unit_cost,
      CAT,
      CAT_DESC,
      PRICE_BY_CAT,
      FORMULA_NAME,
      contract_type,
      SalesRep, 
      cat_class_desc,
      ''4'' Transaction_Code,
      ''NULL'' CREATION_DATE,
      person_id,
      account_number,
      common_output_id,
      location,
      party_site_number,
      version_no,
      price_type
FROM(
SELECT  
      qtt.MODIFIER_NAME MODIFIER_NAME,
      qtt.CUSTOMER_NAME CUSTOMER_NAME,
      DECODE(qtt.QUALIFIER_ATTRIBUTE,''QUALIFIER_ATTRIBUTE32'',''Bill To Customer'',''QUALIFIER_ATTRIBUTE14'',''Bill To'')QUALIFIER_ATTRIBUTE, 
      TO_CHAR(null)  PRODUCT,
      TO_CHAR(NULL)  DESCRIPTION,
      TO_CHAR(NULL) CAT_CLASS,
      TO_NUMBER(0) GM ,
      0  LIST_PRICE,
      decode(price_by_formula_id, null,apps.qp_qp_form_pricing_attr.get_meaning(ql.arithmetic_operator, ''ARITHMETIC_OPERATOR''), ''Cost Plus'') app,
      QL.operand value,
      0 CONTRACT_PRICE,
      0 units_sold,
      0 actual_sales,
      0 ACTUAL_GROSS_MARGIN,
      0 CURRENT_LOCATION_COST,
      to_char(null) NEW_PRICE_OR_NEW_GM_DISC,
      to_number(0)inventory_item_id,
      qtt.list_header_id,
      QPA.PRODUCT_ATTRIBUTE PRODUCT_ATTRIBUTE_TYPE,
      to_number(0) CATEGORY_ID,
      qtt.CUST_ACCOUNT_ID CUST_ACCOUNT_ID,
     ''Item_Summary'' TYPE,
      '||P_PROCESS_ID||' PROCESS_ID,
      ql.ARITHMETIC_OPERATOR ,
      ql.OPERAND ,
      0 unit_cost,
      MCK.SEGMENT1          CAT,
      Flex_Val.Description cat_desc,      
      ''Yes'' PRICE_BY_CAT,
      '''' FORMULA_NAME,
      QL.attribute5 contract_type,
      qtt.SalesRep SalesRep,
      TO_CHAR(NULL) cat_class_desc,
      ''4'' Transaction_Code,
      ''NULL'' CREATION_DATE,
        qtt.person_id,
        qtt.account_number,
        0    common_output_id,
        qtt.location,
        qtt.party_site_number,
        qtt.version_no,
        qtt.price_type 
FROM 
      QP_LIST_LINES           QL,
      QP_PRICING_ATTRIBUTES   QPA,
      MTL_CATEGORIES_KFV      MCK,
      fnd_flex_values_vl flex_val,
      fnd_flex_value_sets flex_name,
      xxeis.EIS_XXWC_QUALIFIER_TMP qtt
WHERE qtt.LIST_HEADER_ID                     = QL.LIST_HEADER_ID
  AND QL.PRICING_PHASE_ID               > 1
  AND QL.QUALIFICATION_IND          IN (0,2,4,6,8,10,12,14,16,18,20,22,24,26, 28, 30, 32)
  AND QL.LIST_LINE_ID                       = QPA.LIST_LINE_ID(+)
  AND (ql.end_date_active is null or 
                                    ql.end_date_active >= sysdate) 
  AND QL.MODIFIER_LEVEL_CODE                = ''LINE''
  AND QPA.PRODUCT_ATTRIBUTE_CONTEXT         = ''ITEM''
  AND QPA.PRODUCT_ATTRIBUTE                 = ''PRICING_ATTRIBUTE26'' 
  AND MCK.SEGMENT1                          = QPA.PRODUCT_ATTR_VALUE
  And flex_val.Flex_Value                   = MCK.Segment1
  AND Flex_Name.Flex_Value_Set_Id           = Flex_Val.Flex_Value_Set_Id
  And Flex_Name.Flex_Value_Set_Name         = ''XXWC_CATEGORY_NUMBER''
  AND (QTT.QUALIFIER_ATTRIBUTE           = ''QUALIFIER_ATTRIBUTE14''
     OR QTT.QUALIFIER_ATTRIBUTE           = ''QUALIFIER_ATTRIBUTE32'')
  )
  GROUP BY MODIFIER_NAME, CUSTOMER_NAME, QUALIFIER_ATTRIBUTE, PRODUCT, DESCRIPTION, CAT_CLASS,CAT, CAT_DESC,PRICE_BY_CAT,FORMULA_NAME, APP,
   contract_type,SalesRep, cat_class_desc,person_id,account_number,common_output_id, location, party_site_number,version_no,price_type 
  
  ';

  l_modifier_data := l_ship_to_modifier_data||' '||'Union all'||' '||l_bill_to_modifier_data  ;
  L_MAIN_SQL:=L_ITEM_NUMBER_SHIP_SQL||' '||'Union all'||' '||L_ITEM_NUMBER_BILL_SQL||' '||'Union all'||' '||
              L_ITEM_CAT_SUMM_BILL_SQL||' '||'Union all'||' '||L_ITEM_CAT_SUMM_SHIP_SQL||' '||'Union all'||' '||
              L_ITEM_CAT_SHIP_SQL||' '||'Union all'||' '||L_ITEM_CAT_BILL_SQL;
  fnd_file.put_line(fnd_file.log,'The sql query modifier Data is'||l_modifier_data );  
  fnd_file.put_line(fnd_file.log,'Modifer Data Start'|| to_char(sysdate,'DD-MON-YYYY HH:MI:SS') );
  OPEN l_ref_cursor2 for l_modifier_data;
  LOOP
  FETCH L_REF_CURSOR2 BULK COLLECT INTO G_VIEW_RECORD2 LIMIT 10000;
    l_count := 0;
    FOR j in 1..G_VIEW_RECORD2.count 
    Loop
      G_VIEW_TAB2(j).QUALIFIER_ATTRIBUTE   :=  G_VIEW_RECORD2(j).QUALIFIER_ATTRIBUTE; 
      G_VIEW_TAB2(j).CUSTOMER_NAME         :=  G_VIEW_RECORD2(j).CUSTOMER_NAME; 
      G_VIEW_TAB2(j).MODIFIER_NAME         :=  G_VIEW_RECORD2(j).MODIFIER_NAME; 
      G_VIEW_TAB2(j).list_header_id        :=  G_VIEW_RECORD2(j).list_header_id ; 
      G_VIEW_TAB2(j).CUST_ACCOUNT_ID       :=  G_VIEW_RECORD2(j).CUST_ACCOUNT_ID; 
      G_VIEW_TAB2(j).SalesRep              :=  G_VIEW_RECORD2(j).SalesRep ; 
      G_VIEW_TAB2(j).person_id             :=  G_VIEW_RECORD2(j).person_id ;
      G_VIEW_TAB2(j).account_number        :=  G_VIEW_RECORD2(j).account_number ; 
      G_VIEW_TAB2(j).location              :=  G_VIEW_RECORD2(j).location ; 
      G_VIEW_TAB2(j).party_site_number     :=  G_VIEW_RECORD2(j).party_site_number ; 
      G_VIEW_TAB2(j).version_no            :=  G_VIEW_RECORD2(j).version_no ; 
      G_VIEW_TAB2(j).price_type            :=  G_VIEW_RECORD2(j).price_type ; 
      l_count := l_count+1;
    END LOOP;   
    
    IF l_count >= 1  then
      forall j in 1 .. G_VIEW_TAB2.count 
        INSERT INTO xxeis.EIS_XXWC_QUALIFIER_TMP
             values g_view_tab2 (j);    
           COMMIT;
    end if;  
    G_VIEW_TAB2.delete;
    G_VIEW_RECORD2.delete;
    IF l_ref_cursor2%NOTFOUND Then
      CLOSE l_ref_cursor2;
      EXIT;
    End if;
  END LOOP;  
  
  fnd_file.put_line(fnd_file.log,'Modifer Data End'|| to_char(sysdate,'DD-MON-YYYY HH:MI:SS') );
  fnd_file.put_line(fnd_file.log,'The sql query is'||L_MAIN_SQL );
  fnd_file.put_line(fnd_file.log,'Bill to and Ship to Start load'|| to_char(sysdate,'DD-MON-YYYY HH:MI:SS') );

  OPEN L_REF_CURSOR for  L_MAIN_SQL ;
  LOOP
  FETCH L_REF_CURSOR BULK COLLECT INTO G_VIEW_RECORD LIMIT 10000;
    L_INSERT_RECD_CNT := 0;
      FOR j in 1..G_VIEW_RECORD.count Loop
      l_max_item_data := null;
      IF G_VIEW_RECORD(j).cat_desc is null THEN
        OPEN xxcur_max_item_data(G_VIEW_RECORD(j).INVENTORY_ITEM_ID);
        FETCH xxcur_max_item_data INTO l_max_item_data;
        CLOSE XXCUR_MAX_ITEM_DATA;
        l_list_price := 0;
        G_VIEW_RECORD(j).CONTRACT_PRICE := 0;
        
        G_VIEW_TAB (j).MODIFIER_NAME                  := G_VIEW_RECORD(j).MODIFIER_NAME;
        G_VIEW_TAB (j).CUSTOMER_NAME              := G_VIEW_RECORD(j).CUSTOMER_NAME;
        G_VIEW_TAB (j).QUALIFIER_ATTRIBUTE        := G_VIEW_RECORD(j).QUALIFIER_ATTRIBUTE;
        G_VIEW_TAB (j).PRODUCT                    := l_max_item_data.item;
        G_VIEW_TAB (j).DESCRIPTION                := l_max_item_data.Item_desc;    
        G_VIEW_TAB (j).CAT_CLASS                  :=  l_max_item_data.CAT_CLASS;
        G_VIEW_TAB (j).GM                         :=  XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_INV_GM(l_max_item_data.INVENTORY_ITEM_ID,XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_COST_LOCATION, l_max_item_data.category_id);
        G_VIEW_TAB (j).LIST_PRICE                 := l_list_price;
        G_VIEW_TAB (j).APP                        := G_VIEW_RECORD(j).APP;
        G_VIEW_TAB (j).VALUE1                     := G_VIEW_RECORD(j).VALUE1;
        G_VIEW_TAB (j).CONTRACT_PRICE             := G_VIEW_RECORD(j).CONTRACT_PRICE;
        G_VIEW_TAB (j).UNITS_SOLD                 := G_VIEW_RECORD(j).UNITS_SOLD;
        G_VIEW_TAB (j).ACTUAL_SALES               := G_VIEW_RECORD(j).ACTUAL_SALES;
        G_VIEW_TAB (j).ACTUAL_GROSS_MARGIN        := G_VIEW_RECORD(j).ACTUAL_GROSS_MARGIN;
        G_VIEW_TAB (j).CURRENT_LOCATION_COST      := G_VIEW_RECORD(j).CURRENT_LOCATION_COST;
        G_VIEW_TAB (j).NEW_PRICE_OR_NEW_GM_DISC   := G_VIEW_RECORD(j).NEW_PRICE_OR_NEW_GM_DISC;
        G_VIEW_TAB (j).INVENTORY_ITEM_ID          := l_max_item_data.INVENTORY_ITEM_ID;
        G_VIEW_TAB (j).LIST_HEADER_ID             := G_VIEW_RECORD(j).LIST_HEADER_ID;
        G_VIEW_TAB (j).PRODUCT_ATTRIBUTE_TYPE     := G_VIEW_RECORD(j).PRODUCT_ATTRIBUTE_TYPE;
        G_VIEW_TAB (j).CATEGORY_ID                := l_max_item_data.CATEGORY_ID;
        G_VIEW_TAB (j).CUST_ACCOUNT_ID            := G_VIEW_RECORD(j).CUST_ACCOUNT_ID;
        G_VIEW_TAB (j).type                       := G_VIEW_RECORD(j).type;
        G_VIEW_TAB (j).PROCESS_ID                 := G_VIEW_RECORD(j).PROCESS_ID;
        G_VIEW_TAB (j).ARITHMETIC_OPERATOR        := G_VIEW_RECORD(j).ARITHMETIC_OPERATOR;
        G_VIEW_TAB (j).OPERAND                    := G_VIEW_RECORD(j).OPERAND;
        G_VIEW_TAB (j).unit_cost                  := G_VIEW_RECORD(j).unit_cost;
        G_VIEW_TAB (j).CAT                        := l_max_item_data.CAT;
        G_VIEW_TAB (j).CAT_DESC                   := l_max_item_data.CAT_DESC;
        G_VIEW_TAB (j).PRICE_BY_CAT               := G_VIEW_RECORD(j).PRICE_BY_CAT;
        G_VIEW_TAB (j).FORMULA_NAME               := G_VIEW_RECORD(j).FORMULA_NAME;
        G_VIEW_TAB (j).contract_type              := G_VIEW_RECORD(j).contract_type;
        G_VIEW_TAB (j).salesrep                   := G_VIEW_RECORD(j).salesrep;
        G_VIEW_TAB (j).cat_class_desc             := l_max_item_data.cat_class_desc;
        G_VIEW_TAB (j).Transaction_code           := G_VIEW_RECORD(j).Transaction_code;
        G_VIEW_TAB (j).person_id                  := G_VIEW_RECORD(j).person_id;
        G_VIEW_TAB (j).account_number             := G_VIEW_RECORD(j).account_number;
        G_VIEW_TAB (j).common_output_id           := xxeis.eis_rs_common_outputs_s.nextval;
        G_VIEW_TAB (j).location                   := G_VIEW_RECORD(j).location ; 
        G_VIEW_TAB (j).party_site_number          := G_VIEW_RECORD(j).party_site_number ; 
        G_VIEW_TAB (j).version_no                 := G_VIEW_RECORD(j).version_no ; 
        G_VIEW_TAB (j).price_type                 := G_VIEW_RECORD(j).price_type ;         
      ELSE
        G_VIEW_TAB (j).MODIFIER_NAME                  := G_VIEW_RECORD(j).MODIFIER_NAME;
        G_VIEW_TAB (j).CUSTOMER_NAME              := G_VIEW_RECORD(j).CUSTOMER_NAME;
        G_VIEW_TAB (j).QUALIFIER_ATTRIBUTE        := G_VIEW_RECORD(j).QUALIFIER_ATTRIBUTE;
        G_VIEW_TAB (j).PRODUCT                    := G_VIEW_RECORD(j).PRODUCT;
        G_VIEW_TAB (j).DESCRIPTION                := G_VIEW_RECORD(j).DESCRIPTION;    
        G_VIEW_TAB (j).CAT_CLASS                  := G_VIEW_RECORD(j).CAT_CLASS;
        G_VIEW_TAB (j).GM                         := G_VIEW_RECORD(j).GM;
        G_VIEW_TAB (j).LIST_PRICE                 := G_VIEW_RECORD(j).LIST_PRICE;
        G_VIEW_TAB (j).APP                        := G_VIEW_RECORD(j).APP;
        G_VIEW_TAB (j).VALUE1                     := G_VIEW_RECORD(j).VALUE1;
        G_VIEW_TAB (j).CONTRACT_PRICE             := G_VIEW_RECORD(j).CONTRACT_PRICE;
        G_VIEW_TAB (j).UNITS_SOLD                 := G_VIEW_RECORD(j).UNITS_SOLD;
        G_VIEW_TAB (j).ACTUAL_SALES               := G_VIEW_RECORD(j).ACTUAL_SALES;
        G_VIEW_TAB (j).ACTUAL_GROSS_MARGIN        := G_VIEW_RECORD(j).ACTUAL_GROSS_MARGIN;
        G_VIEW_TAB (j).CURRENT_LOCATION_COST      := G_VIEW_RECORD(j).CURRENT_LOCATION_COST;
        G_VIEW_TAB (j).NEW_PRICE_OR_NEW_GM_DISC   := G_VIEW_RECORD(j).NEW_PRICE_OR_NEW_GM_DISC;
        G_VIEW_TAB (j).INVENTORY_ITEM_ID          := G_VIEW_RECORD(j).INVENTORY_ITEM_ID;
        G_VIEW_TAB (j).LIST_HEADER_ID             := G_VIEW_RECORD(j).LIST_HEADER_ID;
        G_VIEW_TAB (j).PRODUCT_ATTRIBUTE_TYPE     := G_VIEW_RECORD(j).PRODUCT_ATTRIBUTE_TYPE;
        G_VIEW_TAB (j).CATEGORY_ID                := G_VIEW_RECORD(j).CATEGORY_ID;
        G_VIEW_TAB (j).CUST_ACCOUNT_ID            := G_VIEW_RECORD(j).CUST_ACCOUNT_ID;
        G_VIEW_TAB (j).type                       := G_VIEW_RECORD(j).type;
        G_VIEW_TAB (j).PROCESS_ID                 := P_PROCESS_ID;
        G_VIEW_TAB (j).ARITHMETIC_OPERATOR        := G_VIEW_RECORD(j).ARITHMETIC_OPERATOR;
        G_VIEW_TAB (j).OPERAND                    := G_VIEW_RECORD(j).OPERAND;
        G_VIEW_TAB (j).unit_cost                  := G_VIEW_RECORD(j).unit_cost;
        G_VIEW_TAB (j).CAT                        := G_VIEW_RECORD(j).CAT;
        G_VIEW_TAB (j).CAT_DESC                   := G_VIEW_RECORD(j).CAT_DESC;
        G_VIEW_TAB (j).PRICE_BY_CAT               := G_VIEW_RECORD(j).PRICE_BY_CAT;
        G_VIEW_TAB (j).FORMULA_NAME               := G_VIEW_RECORD(j).FORMULA_NAME;
        G_VIEW_TAB (j).contract_type              := G_VIEW_RECORD(j).contract_type;
        G_VIEW_TAB (j).salesrep                   := G_VIEW_RECORD(j).salesrep;
        G_VIEW_TAB (j).cat_class_desc             := G_VIEW_RECORD(j).cat_class_desc;
        G_VIEW_TAB (j).Transaction_code           := G_VIEW_RECORD(j).Transaction_code;
        G_VIEW_TAB (j).person_id                  := G_VIEW_RECORD(j).person_id;
        G_VIEW_TAB (j).account_number             := G_VIEW_RECORD(j).account_number;
        G_VIEW_TAB (j).common_output_id           := xxeis.eis_rs_common_outputs_s.nextval;
        G_VIEW_TAB (j).location                   := G_VIEW_RECORD(j).location ; 
        G_VIEW_TAB (j).party_site_number          := G_VIEW_RECORD(j).party_site_number ; 
        G_VIEW_TAB (j).version_no                 := G_VIEW_RECORD(j).version_no ; 
        G_VIEW_TAB (j).price_type                 := G_VIEW_RECORD(j).price_type ;      
      END IF; 
   
      L_INSERT_RECD_CNT := L_INSERT_RECD_CNT + 1;
    END LOOP;
    IF l_insert_recd_cnt >= 1  then
       forall j in 1 .. G_VIEW_TAB.count
         INSERT INTO XXEIS.eis_xxwc_cont_modi_pricing
             values g_view_tab (j);    
          commit;
    end if;   
    G_VIEW_TAB.delete;
    G_VIEW_RECORD.delete;

    IF l_ref_cursor%NOTFOUND Then
       CLOSE l_ref_cursor;
       EXIT;
    End if;
  END LOOP;
  fnd_file.put_line(fnd_file.log,'Bill to and Ship to end'|| to_char(sysdate,'DD-MON-YYYY HH:MI:SS') );   
  FND_FILE.PUT_LINE(FND_FILE.log,'Updatation End Time'|| TO_CHAR(sysdate,'DD-MON-YYYY HH:MI:SS') );
        
 /* l_list_price_sql := 'select excmpi.inventory_item_id
                            ,qllv.operand
                        from qp_list_lines qllv
                            ,(SELECT excmp.inventory_item_id inventory_item_id
                                    ,max(qpa.list_line_id) list_line_id
                                FROM qp_pricing_attributes qpa
                                     ,qp_list_headers       qslv
                                              ,XXEIS.eis_xxwc_cont_modi_pricing excmp
                                        where  1 = 1
                                        and qslv.list_header_id           = qpa.list_header_id
                                        And qslv.list_type_code           = ''PRL''
                                         --and upper(qslv.name)           like ''CATEGORY%''
                                        AND Qpa.PRODUCT_ATTRIBUTE_CONTEXT = ''ITEM''
                                        AND Qpa.PRODUCT_attr_value        = to_char(excmp.inventory_item_id)
                                        AND excmp.inventory_item_id != 0
                                        group by excmp.inventory_item_id) excmpi
                                        where excmpi.list_line_id = qllv.list_line_id';*/
    L_LIST_PRICE_SQL:='
                          SELECT    to_number(QPA.PRODUCT_ATTR_VALUE)inventory_item_id,
                                    QLLV.OPERAND list_price,
                                    to_number(QP.QUALIFIER_ATTR_VALUE) QUALIFIER_ATTR_VALUE
                          FROM
                            QP_LIST_HEADERS         QSLV,
                            QP_LIST_LINES           QLLV,
                            QP_PRICING_ATTRIBUTES   QPA,
                            QP_QUALIFIERS           QP
                          WHERE 1=1
                          AND QSLV.LIST_HEADER_ID             = QLLV.LIST_HEADER_ID
                          AND QSLV.LIST_TYPE_CODE             = ''PRL''
                          AND QP.LIST_HEADER_ID               = QSLV.LIST_HEADER_ID
                          AND QPA.LIST_HEADER_ID              = QSLV.LIST_HEADER_ID
                          and QPA.list_line_id                = QLLV.list_line_id
                          AND QPA.PRODUCT_ATTRIBUTE_CONTEXT   = ''ITEM''
                          AND QP.QUALIFIER_ATTRIBUTE          = ''QUALIFIER_ATTRIBUTE18''
                          AND QPA.PRODUCT_ATTRIBUTE           = ''PRICING_ATTRIBUTE1''
                          AND QP.QUALIFIER_CONTEXT            = ''ORDER''
                          AND QSLV.ATTRIBUTE10                = ''Market Price List''
                          AND sysdate          <= NVL(QSLV.end_date_active,sysdate)
                          AND sysdate          <= NVL(QLLV.end_date_active,sysdate)
                          ';
              
  fnd_file.put_line(fnd_file.log,'List Price Start'|| to_char(sysdate,'DD-MON-YYYY HH:MI:SS') );                                         
  OPEN l_ref_cursor5 for l_list_price_sql;
  LOOP
  FETCH L_REF_CURSOR5 BULK COLLECT INTO G_VIEW_RECORD5 LIMIT 10000;
    l_count := 0;
    FOR j in 1..G_VIEW_RECORD5.count 
    Loop
      G_VIEW_TAB5(j).inventory_item_id   :=  G_VIEW_RECORD5(j).inventory_item_id; 
      G_VIEW_TAB5(J).LIST_PRICE         :=  G_VIEW_RECORD5(J).LIST_PRICE; 
      G_VIEW_TAB5(J).QUALIFIER_ATTR_VALUE   :=  G_VIEW_RECORD5(J).QUALIFIER_ATTR_VALUE; 
      l_count := l_count+1;
    END LOOP;   
    
    IF l_count >= 1  THEN
      FORALL J IN 1 .. G_VIEW_TAB5.COUNT 
         INSERT INTO xxeis.EIS_XXWC_LIST_PRICE
              values g_view_tab5(j);    
    END IF;
    G_VIEW_TAB5.delete;
    G_VIEW_RECORD5.delete;
    IF l_ref_cursor5%NOTFOUND Then
       CLOSE l_ref_cursor5;
       EXIT;
    End if;
  END LOOP; 
  
  fnd_file.put_line(fnd_file.log,'List Price End'|| to_char(sysdate,'DD-MON-YYYY HH:MI:SS') );   
  
      L_LIST_PRICE_SQL1:='  SELECT   to_number(QPA.PRODUCT_ATTR_VALUE) inventory_item_id ,
                                     QLLV.OPERAND list_price,
                                     NULL QUALIFIER_ATTR_VALUE 
                            
                          FROM
                            QP_LIST_HEADERS         QSLV,
                            QP_LIST_LINES           QLLV,
                            QP_PRICING_ATTRIBUTES   QPA
                          WHERE 1=1
                          AND QSLV.LIST_HEADER_ID             = QLLV.LIST_HEADER_ID
                          AND QSLV.LIST_TYPE_CODE             = ''PRL''
                          AND QPA.LIST_HEADER_ID              = QSLV.LIST_HEADER_ID
                          AND QPA.LIST_LINE_ID                = QLLV.LIST_LINE_ID
                          AND QPA.PRODUCT_ATTRIBUTE_CONTEXT   = ''ITEM''
                          AND QSLV.NAME =''MARKET NATIONAL''
                          /*AND QSLV.NAME                      IN (select lookup_code from fnd_lookup_values_VL where 
                                                                  lookup_type =''XXWC_NATIONAL_MARKET_PL''
                                                                  AND sysdate between start_date_active and nvl(end_date_active,sysdate))*/                          
                          AND sysdate          <= NVL(QSLV.end_date_active,sysdate)
                          AND sysdate          <= NVL(QLLV.end_date_active,sysdate)
                          ';
    FND_FILE.PUT_LINE(FND_FILE.LOG,'List Price Nationa1'|| L_LIST_PRICE_SQL1); 
    FND_FILE.PUT_LINE(FND_FILE.LOG,'List Price Nationa1'|| TO_CHAR(SYSDATE,'DD-MON-YYYY HH:MI:SS') ); 
    
  OPEN l_ref_cursor8 for L_LIST_PRICE_SQL1;
  LOOP
  FETCH L_REF_CURSOR8 BULK COLLECT INTO G_VIEW_RECORD8 LIMIT 10000;
    L_COUNT := 0;
    FOR j in 1..G_VIEW_RECORD8.count 
    LOOP
      G_VIEW_TAB8(J).INVENTORY_ITEM_ID        :=  G_VIEW_RECORD8(J).INVENTORY_ITEM_ID; 
      G_VIEW_TAB8(J).LIST_PRICE               :=  G_VIEW_RECORD8(J).LIST_PRICE; 
      G_VIEW_TAB8(J).QUALIFIER_ATTR_VALUE     :=  G_VIEW_RECORD8(J).QUALIFIER_ATTR_VALUE; 
      l_count := l_count+1;
    END LOOP;   
    
    IF L_COUNT >= 1  THEN
      FORALL J IN 1 .. G_VIEW_TAB8.COUNT 
         INSERT INTO XXEIS.EIS_XXWC_LIST_PRICE
              values g_view_tab8(j);    
    END IF;
    G_VIEW_TAB8.delete;
    G_VIEW_RECORD8.DELETE;
    IF L_REF_CURSOR8%NOTFOUND THEN
       CLOSE l_ref_cursor8;
       EXIT;
    END IF;
  END LOOP; 
    commit;
    FND_FILE.PUT_LINE(FND_FILE.LOG,'List Price National End1'|| TO_CHAR(SYSDATE,'DD-MON-YYYY HH:MI:SS') );  
 /* FOR distinct_item IN xxcur_distinct_item
  LOOP
    l_max_line_id := 0;
    l_max_list_price := distinct_item.list_price;
    FOR list_price_update IN xxcur_list_price_update(distinct_item.inventory_item_id)
    LOOP
      l_contract_price := 0;
      IF list_price_update.ARITHMETIC_OPERATOR ='AMT' THEN
        l_contract_price := (l_max_list_price - list_price_update.OPERAND);
      ELSIF  list_price_update.ARITHMETIC_OPERATOR = '%' THEN  
        l_contract_price :=((l_max_list_price )-((list_price_update.OPERAND*l_max_list_price)/100));
      ELSIF list_price_update.ARITHMETIC_OPERATOR = 'NEWPRICE'  THEN
        l_contract_price := list_price_update.OPERAND;      
      ELSIF list_price_update.APP is not null THEN   
        l_contract_price :=((l_max_list_price)/(1-0.15));           
      END IF; 
  
      UPDATE XXEIS.eis_xxwc_cont_modi_pricing excmp
         SET  list_price = l_max_list_price
                   ,contract_price = l_contract_price
       WHERE excmp.inventory_item_id = distinct_item.inventory_item_id
         AND excmp.common_output_id = list_price_update.common_output_id;
    END LOOP;
  END LOOP;*/

  fnd_file.put_line(fnd_file.log,'Contract Price End'|| to_char(sysdate,'DD-MON-YYYY HH:MI:SS') ); 
END;
   
   
procedure CLEAN_UP_PROCESS (P_PROCESS_ID number) is
l_sql varchar2(32000);
begin

L_SQL:= 'delete from  xxeis.EIS_XXWC_CONT_MODI_PRICING where process_id ='||P_PROCESS_ID||'';
fnd_file.put_line(fnd_file.log,'The Clean up Sql is '||L_SQL);
execute immediate l_sql;
END;


PROCEDURE get_curr_sold_sales (p_item_id          IN     NUMBER
                                 ,p_cat_id           IN     NUMBER
                                 ,p_cat              IN     VARCHAR2
                                 ,p_cust_acct_id     IN     NUMBER
                                 ,p_list_header_id   IN     NUMBER
                                 ,P_START_DATE       IN     DATE
                                 ,P_END_DATE         IN     DATE
                                 ,p_units_sold          OUT NUMBER
                                 ,p_actual_sales        OUT NUMBER
                                 ,p_unit_cost           OUT NUMBER
                                 )
   IS
      l_start_date   VARCHAR2 (32000);
      L_END_DATE     varchar2 (32000); 
      L_SQL VARCHAR2 (32000);
   begin
   L_SQL:=NULL;      
       IF p_start_date is not null or p_end_date is not null THEN
       IF p_start_date is not null THEN
          l_start_date :=  ' AND ((ol.line_type_id != 1005 AND  Trunc(ol.Actual_Shipment_Date) >= '''||P_START_DATE||''')
                    OR (ol.line_type_id = 1005 and  Trunc(ol.fulfillment_Date) >= '''||P_START_DATE||'''))';
          
       ELSE 
          l_start_date := ' AND 1 = 1 ';
       END IF;
       IF p_end_date is not null THEN
         l_end_date := ' AND ((ol.line_type_id != 1005 AND  Trunc(ol.Actual_Shipment_Date) <= '''||p_end_date||''')
                    OR (ol.line_type_id = 1005 and  Trunc(ol.fulfillment_Date) <= '''||p_end_date||'''))';       
       ELSE
          l_end_date := ' AND 1 =1 ';
       end if;
       
     DBMS_OUTPUT.PUT_LINE('l_start_date '||L_START_DATE);
      DBMS_OUTPUT.PUT_LINE('l_end_date '||l_end_date);
     
      IF p_item_id != 0
      THEN
         begin
          DBMS_OUTPUT.PUT_LINE('FIRST');
            --FND_FILE.PUT_LINE(FND_FILE.log,' Start and End Date  and item is not zero' || P_START_DATE || ' ' || P_END_DATE );
            --fnd_file.put_line(fnd_file.log,' Customer Id ' || p_cust_acct_id || '  Item Id ' || p_item_id||' list '||P_LIST_HEADER_ID );
                                             
     L_SQL:=   ' Select  NVL(SUM(DECODE(LINE_CATEGORY_CODE, ''RETURN'',NVL(OL.SHIPPED_QUANTITY,NVL(OL.FULFILLED_QUANTITY,OL.ORDERED_QUANTITY))*-1,NVL(OL.SHIPPED_QUANTITY,NVL(OL.FULFILLED_QUANTITY,OL.ORDERED_QUANTITY)))),0) UNITS_SOLD,
                                      NVL(SUM((NVL(DECODE (LINE_CATEGORY_CODE,''RETURN'',NVL(OL.SHIPPED_QUANTITY,NVL(OL.FULFILLED_QUANTITY,OL.ORDERED_QUANTITY))*-1,NVL(OL.SHIPPED_QUANTITY,NVL(OL.FULFILLED_QUANTITY,OL.ORDERED_QUANTITY))),0)*OL.UNIT_SELLING_PRICE)),0) ACTUAL_SALES,
                                      AVG(NVL(ol.unit_cost,0)) unit_cost 
                                 from OE_ORDER_LINES_all              OL,
                                      OE_ORDER_HEADERS_all            OH,
                                      oe_price_adjustments            OP,
                                      qp_list_headers                 qh,
                                      xxeis.EIS_XXWC_ITEM_DATA            exid
                                where 1=1
                                   and OL.FLOW_STATUS_CODE       = ''CLOSED''
                                            AND ol.inventory_item_id      =  exid.inventory_item_id                                           
                                  AND oh.header_id              = ol.header_id 
                                  AND ol.line_id                = op.line_id
                                  and OP.HEADER_ID              = OH.HEADER_ID 
                                  AND QH.attribute10            = ''Contract Pricing'' 
                                  and QH.LIST_HEADER_ID         = OP.LIST_HEADER_ID'
                                  ||L_START_DATE || L_END_DATE
                                                  ||' AND ol.inventory_item_id ='|| P_ITEM_ID 
                                        ||' AND ol.sold_to_org_id ='|| P_CUST_ACCT_ID 
                                        ||' and QH.LIST_HEADER_ID = '||P_LIST_HEADER_ID                                              
                                 ||' group by ol.sold_to_org_id, ol.inventory_item_id,op.list_header_id';
                 
--                if P_ITEM_ID = 2924933 then
--        FND_FILE.PUT_LINE(FND_FILE.log,L_SQL );
--        end if;
        execute immediate L_SQL into P_UNITS_SOLD, P_ACTUAL_SALES, P_UNIT_COST;
         EXCEPTION
            WHEN OTHERS
            then
             --   fnd_file.put_line(fnd_file.log,' In Exception  item id not zero :' || substr(sqlerrm,1,250) );
               p_units_sold := 0;
               p_actual_sales := 0;
               P_UNIT_COST := 0;
               DBMS_OUTPUT.PUT_LINE(SQLERRM);
         END;
      ELSIF p_cat_id = 0
      THEN
         begin
            DBMS_OUTPUT.PUT_LINE('SECOND');
              --fnd_file.put_line(fnd_file.log,' Start and End Date  and item is not zero' || p_start_date || ' ' || p_end_date );
              --fnd_file.put_line(fnd_file.log,' Customer Id ' || p_cust_acct_id || '  Item Id ' || p_item_id );
       L_SQL:='SELECT SUM (exusd.units_sold), SUM (exusd.actual_sales), AVG (0)                
            from(
           Select  NVL(SUM(DECODE(LINE_CATEGORY_CODE, ''RETURN'',NVL(OL.SHIPPED_QUANTITY,NVL(OL.FULFILLED_QUANTITY,OL.ORDERED_QUANTITY))*-1,NVL(OL.SHIPPED_QUANTITY,NVL(OL.FULFILLED_QUANTITY,OL.ORDERED_QUANTITY)))),0) UNITS_SOLD,
                                      NVL(SUM((NVL(DECODE (LINE_CATEGORY_CODE, ''RETURN'',NVL(OL.SHIPPED_QUANTITY,NVL(OL.FULFILLED_QUANTITY,OL.ORDERED_QUANTITY))*-1,NVL(OL.SHIPPED_QUANTITY,NVL(OL.FULFILLED_QUANTITY,OL.ORDERED_QUANTITY))),0)*OL.UNIT_SELLING_PRICE)),0) ACTUAL_SALES,
                                      AVG(NVL(ol.unit_cost,0)) unit_cost,
                                       ol.inventory_item_id inventory_item_id,
                                      ol.sold_to_org_id cust_account_id,
                                      max(0),
                                      OP.LIST_HEADER_ID LIST_HEADER_ID,
                                    min(EXID.CAT) CAT,
                                    min(EXID.CATEGORY_ID) CATEGORY_ID
                                 from OE_ORDER_LINES_all              OL,
                                      OE_ORDER_HEADERS_all            OH,
                                      oe_price_adjustments            OP,
                                      qp_list_headers                 qh,
                                      xxeis.EIS_XXWC_ITEM_DATA            exid
                                where 1=1
                                   and OL.FLOW_STATUS_CODE       = ''CLOSED''
                                            AND ol.inventory_item_id      =  exid.inventory_item_id                                           
                                  AND oh.header_id              = ol.header_id 
                                  AND ol.line_id                = op.line_id
                                  AND op.header_id              = oh.header_id 
                                  AND QH.attribute10            = ''Contract Pricing'' 
                                  AND qh.list_header_id         = op.list_header_id'
                                  ||L_START_DATE||L_END_DATE 
                                 ||' group by ol.sold_to_org_id, ol.inventory_item_id,op.list_header_id) exusd'
            ||' WHERE exusd.cat ='|| P_CAT 
               ||' AND exusd.cust_account_id ='|| P_CUST_ACCT_ID 
               ||' and EXUSD.LIST_HEADER_ID ='|| P_LIST_HEADER_ID 
              -- AND exusd.process_id = p_process_id
            ||'GROUP BY exusd.cust_account_id
                    ,exusd.list_header_id
                    ,exusd.cat';
            EXECUTE IMMEDIATE L_SQL INTO p_units_sold, p_actual_sales, p_unit_cost;
         EXCEPTION
            WHEN OTHERS
            then
            --   fnd_file.put_line(fnd_file.log,' In Exception  Cat :' || substr(sqlerrm,1,250) );
               p_units_sold := 0;
               p_actual_sales := 0;
               p_unit_cost := 0;
         END;
      ELSE
         begin
          DBMS_OUTPUT.PUT_LINE('THIRD');
             -- fnd_file.put_line(fnd_file.log,' Start and End Date  and item is not zero' || p_start_date || ' ' || p_end_date );
             -- fnd_file.put_line(fnd_file.log,' Customer Id ' || p_cust_acct_id || '  Item Id ' || p_item_id );
       L_SQL:='SELECT SUM (exusd.units_sold), SUM (exusd.actual_sales), AVG (0)
            from(
           Select  NVL(SUM(DECODE(LINE_CATEGORY_CODE, ''RETURN'',NVL(OL.SHIPPED_QUANTITY,NVL(OL.FULFILLED_QUANTITY,OL.ORDERED_QUANTITY))*-1,NVL(OL.SHIPPED_QUANTITY,NVL(OL.FULFILLED_QUANTITY,OL.ORDERED_QUANTITY)))),0) UNITS_SOLD,
                                      NVL(SUM((NVL(DECODE (LINE_CATEGORY_CODE, ''RETURN'',NVL(OL.SHIPPED_QUANTITY,NVL(OL.FULFILLED_QUANTITY,OL.ORDERED_QUANTITY))*-1,NVL(OL.SHIPPED_QUANTITY,NVL(OL.FULFILLED_QUANTITY,OL.ORDERED_QUANTITY))),0)*OL.UNIT_SELLING_PRICE)),0) ACTUAL_SALES,
                                      AVG(NVL(ol.unit_cost,0)) unit_cost,
                                       ol.inventory_item_id inventory_item_id,
                                      ol.sold_to_org_id cust_account_id,
                                      max(0),
                                      OP.LIST_HEADER_ID LIST_HEADER_ID,
                                    min(EXID.CAT) CAT,
                                    MIN(exid.category_id) category_id
                                 from OE_ORDER_LINES_all              OL,
                                      OE_ORDER_HEADERS_all            OH,
                                      oe_price_adjustments            OP,
                                      qp_list_headers                 qh,
                                      xxeis.EIS_XXWC_ITEM_DATA            exid
                                where 1=1
                                   and OL.FLOW_STATUS_CODE       = ''CLOSED''
                                            AND ol.inventory_item_id      =  exid.inventory_item_id                                           
                                  AND oh.header_id              = ol.header_id 
                                  AND ol.line_id                = op.line_id
                                  AND op.header_id              = oh.header_id 
                                  AND QH.attribute10            = ''Contract Pricing'' 
                                  AND qh.list_header_id         = op.list_header_id'
                                  ||L_START_DATE||L_END_DATE 
                                 ||' group by ol.sold_to_org_id, ol.inventory_item_id,op.list_header_id) exusd'
            ||' WHERE exusd.category_id ='|| P_CAT_ID 
             ||' AND exusd.cust_account_id ='|| P_CUST_ACCT_ID 
             ||' and EXUSD.LIST_HEADER_ID ='|| P_LIST_HEADER_ID 
       ||' GROUP BY exusd.cust_account_id
                    ,EXUSD.LIST_HEADER_ID
                    ,exusd.category_id';
                    
    --  DBMS_OUTPUT.PUT_LINE(L_SQL);
            -- AND exusd.process_id = p_process_id

      execute immediate L_SQL into P_UNITS_SOLD, P_ACTUAL_SALES, P_UNIT_COST;
    --  fnd_file.put_line(fnd_file.log,'inv id '||P_UNITS_SOLD||''||P_ACTUAL_SALES||''||P_UNIT_COST );
         EXCEPTION
            WHEN OTHERS
            then
              -- DBMS_OUTPUT.PUT_LINE(L_SQL);
             --  fnd_file.put_line(fnd_file.log,' In Exception category_id  :' || substr(sqlerrm,1,250) );
               p_units_sold := 0;
               p_actual_sales := 0;
               p_unit_cost := 0;
         END;
      end if;
    END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         p_units_sold := 0;
         p_actual_sales := 0;
         P_UNIT_COST := 0;
   END get_curr_sold_sales;  

procedure  CONTRACT_PRICING_MODIFIER (           P_PROCESS_ID                   in number,
                                               P_START_DATE                   in date default null,
                                               P_END_DATE                     IN DATE DEFAULT NULL,
                                              -- P_CURRENT_COST_LOC              varchar2,
                                               P_CATCLASS                     in varchar2 default null,
                                               P_CAT_PRICED_ITEMS             in varchar2  default null,
                                               P_RVP                          in varchar2  default null,
                                               P_DM                           in varchar2  default null,
                                               P_SALESPERSON                  in varchar2  default null,
                                               P_JOB_ACCOUNT                  varchar2,
                                               P_MASTER_ACCOUNT               varchar2,
                                               P_CUSTOMER_NAME                varchar2     default null,
                                               P_CUSTOMER_NUMBER              VARCHAR2     default null,
                                               P_MODIFIER_NAME                VARCHAR2     default null
                                               ) IS
    L_SALESREP_STR                varchar2(32000);
    l_customer_name               varchar2(32000);
    l_main_sql                    varchar2(32000);
    l_transaction_type            varchar2(32000);
    l_customer_number             varchar2(32000);
    l_modifier_name               varchar2(32000);
    l_count                       NUMBER;
    l_start_date                  varchar2(32000);
    l_end_date                    varchar2(32000);
    l_ref_cursor4                 CURSOR_TYPE4;
    l_units_sold                  NUMBER;
    l_actual_sales                NUMBER;
    l_unit_cost                   NUMBER;
    l_REF_CURSOR7                 CURSOR_TYPE7;  
    L_UNIT_SALES_DATA             VARCHAR2(32000);   
    l_list_price                  number;
    l_max_list_price              number;
    l_max_line_id                 number;
    L_CONTRACT_PRICE              NUMBER;
    l_inventory_item_id           NUMBER;
    L_ORG_ID                      NUMBER;  
    --Added--
    l_list_header_id              number;
    l_cust_account_id             number;
    l_category_id                 number;
    --End Added-----

    
    CURSOR xxcur_distinct_item  IS
        SELECT distinct excmp.inventory_item_id,xlpt.list_price
           FROM XXEIS.eis_xxwc_cont_modi_pricing excmp
               ,XXEIS.EIS_XXWC_LIST_PRICE XLPT
               ,ORG_ORGANIZATION_DEFINITIONS ODD
          WHERE EXCMP.INVENTORY_ITEM_ID !=0
            AND XLPT.INVENTORY_ITEM_ID  = EXCMP.INVENTORY_ITEM_ID
            AND TO_CHAR(ODD.ORGANIZATION_ID)     = (XLPT.QUALIFIER_ATTR_VALUE)
            and odd.organization_code=XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_COST_LOCATION;

    CURSOR xxcur_list_price_update(p_inventory_item_id IN NUMBER) IS
         SELECT excmp.operand
               ,excmp.arithmetic_operator
               ,excmp.app
               ,excmp.common_output_id
           FROM XXEIS.eis_xxwc_cont_modi_pricing excmp
          WHERE excmp.inventory_item_id = p_inventory_item_id;
  
    
BEGIN
     L_SALESREP_STR:='And 1=1';
    
   BEGIN
  -- FND_FILE.PUT_LINE(FND_FILE.LOG,'Start Begin');
   --     FND_FILE.PUT_LINE(FND_FILE.LOG,'Param Value'|| XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_COST_LOCATION);
     Select ORGANIZATION_ID
       INTO l_org_id
       from ORG_ORGANIZATION_DEFINITIONS ODD
      WHERE ODD.ORGANIZATION_ID = XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_COST_LOCATION;
     --    FND_FILE.PUT_LINE(fnd_file.log,'Set org id  is '||l_org_id);
   EXCEPTION 
     WHEN OTHERS THEN
      -- FND_FILE.PUT_LINE(FND_FILE.LOG,'Start Begin First Exception');
      l_org_id := null;
   END;

   if P_SALESPERSON is not null then
          L_SALESREP_STR:=L_SALESREP_STR||''||' AND EOXC.SALESREP IN ('||XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(P_SALESPERSON)||')';
    else
          L_SALESREP_STR:=L_SALESREP_STR||''||' AND 1=1';
    END IF;
  
    IF P_RVP IS  NOT NULL THEN 
        L_SALESREP_STR :=L_SALESREP_STR||''||' AND EOXC.person_id IN (SELECT distinct papf.person_id
                                                                       FROM per_all_assignments_f paaf, PER_ALL_PEOPLE_F PAPF
                                                                       WHERE paaf.person_id = papf.person_id   
                                                                         and ASSIGNMENT_TYPE=''E''
                                                                  CONNECT BY paaf.supervisor_id = PRIOR papf.person_id
                                                START WITH TRIM(papf.full_name) IN ('||XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(P_RVP)||'))'; 
      else
        L_SALESREP_STR:=L_SALESREP_STR||''||' AND 1=1';
    END IF;
    
    if P_DM is not null then
    L_SALESREP_STR :=L_SALESREP_STR||''|| '  AND EOXC.person_id IN (SELECT distinct papf.person_id
                                                                       FROM per_all_assignments_f paaf, PER_ALL_PEOPLE_F PAPF
                                                                       WHERE paaf.person_id = papf.person_id   
                                                                         and ASSIGNMENT_TYPE=''E''
                                                                  CONNECT BY paaf.supervisor_id = PRIOR papf.person_id
                                                START WITH TRIM(papf.full_name) IN ('||XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(P_DM)||'))'; 
    else
    L_SALESREP_STR:=L_SALESREP_STR||''|| ' AND 1=1';
    end if;
    
  /*    
    IF P_START_DATE IS NOT NULL THEN
         L_START_DATE:=  'AND (DECODE(OTT.NAME,''STANDARD ORDER'' ,TRUNC(OL.ACTUAL_SHIPMENT_DATE),
                                          ''COUNTER ORDER'' , TRUNC(OL.SCHEDULE_SHIP_DATE),
                                          ''RETURN ORDER''  , NVL(TRUNC(OL.ACTUAL_SHIPMENT_DATE),TRUNC(OL.SCHEDULE_SHIP_DATE)),
                                          ''REPAIR ORDER''  , TRUNC(OL.SCHEDULE_SHIP_DATE),
                                          ''INTERNAL ORDER'', TRUNC(OL.SCHEDULE_SHIP_DATE),
                                          ''WC LONG TERM RENTAL'',NVL(TRUNC(OL.ACTUAL_SHIPMENT_DATE),TRUNC(OL.SCHEDULE_SHIP_DATE))))>='''||P_START_DATE||'''';
                           
    FND_FILE.PUT_LINE(FND_FILE.LOG, ' The Start Cond is  '||L_START_DATE);
    else
        L_START_DATE:=' and 1=1';
    end if;
    
    IF P_END_DATE IS NOT NULL THEN
      L_END_DATE :='AND EOXC.CREATION_DATE <='''||P_END_DATE||'''';
                           
 
    FND_FILE.PUT_LINE(FND_FILE.LOG, ' The End Cond is  '||L_END_DATE);
    else
        L_END_DATE:=' and 1=1';
    end if;
  */
    IF P_CUSTOMER_NAME IS NOT NULL THEN               
           l_customer_name := ' and EOXC.CUSTOMER_NAME in ('||xxeis.eis_rs_utility.get_param_values(p_customer_name)||' )';          
       
    else
        l_customer_name:=' and 1=1';
    end if;
    
    IF P_CUSTOMER_NUMBER IS NOT NULL THEN               
           l_customer_number := ' and EOXC.account_number in ('||xxeis.eis_rs_utility.get_param_values(p_customer_number)||' )';          
       
    else
        l_customer_number:=' and 1=1';
    end if;
    
    IF P_MODIFIER_NAME IS NOT NULL THEN               
        l_modifier_name := ' and EOXC.MODIFIER_NAME in ('||xxeis.eis_rs_utility.get_param_values(P_MODIFIER_NAME)||' )';          
       
    else
        l_modifier_name:=' and 1=1';
    end if;
    
    IF  P_CAT_PRICED_ITEMS = 'Include' THEN 
     IF (P_JOB_ACCOUNT ='Include' AND P_MASTER_ACCOUNT ='Exclude') THEN 
        l_transaction_type := ' AND (EOXC.TRANSACTION_CODE = 1 OR EOXC.TRANSACTION_CODE = 3) ';
     ELSIF (P_JOB_ACCOUNT ='Include' AND P_MASTER_ACCOUNT ='Include') THEN 
        l_transaction_type := ' AND 1=1';
     ELSE     
        l_transaction_type := ' AND (EOXC.TRANSACTION_CODE = 2 OR EOXC.TRANSACTION_CODE = 4) ';
     END IF;
    ELSE
     IF (P_JOB_ACCOUNT ='Include' AND P_MASTER_ACCOUNT ='Exclude') THEN
        l_transaction_type := ' AND EOXC.TRANSACTION_CODE = 1'; 
     ELSIF (P_JOB_ACCOUNT ='Include' AND P_MASTER_ACCOUNT ='Include')  THEN  
        l_transaction_type := ' AND (EOXC.TRANSACTION_CODE = 1 OR EOXC.TRANSACTION_CODE = 2) ';
     ELSE     
        l_transaction_type := ' AND EOXC.TRANSACTION_CODE = 2';
     END IF;    
    END IF; 
   
   /* IF p_start_date is not null or p_end_date is not null THEN
       IF p_start_date is not null THEN
          l_start_date :=  ' AND ((ol.line_type_id != 1005 AND  Trunc(ol.Actual_Shipment_Date) >= '''||P_START_DATE||''')
                    OR (ol.line_type_id = 1005 and  Trunc(ol.fulfillment_Date) >= '''||P_START_DATE||'''))';
          
       ELSE 
          l_start_date := ' AND 1 = 1 ';
       END IF;
       IF p_end_date is not null THEN
         l_end_date := ' AND ((ol.line_type_id != 1005 AND  Trunc(ol.Actual_Shipment_Date) <= '''||p_end_date||''')
                    OR (ol.line_type_id = 1005 and  Trunc(ol.fulfillment_Date) <= '''||p_end_date||'''))';       
       ELSE
          l_end_date := ' AND 1 =1 ';
       END IF;
       L_UNIT_SALES_DATA := ' Select  NVL(SUM(DECODE(LINE_CATEGORY_CODE, ''RETURN'',NVL(OL.SHIPPED_QUANTITY,NVL(OL.FULFILLED_QUANTITY,OL.ORDERED_QUANTITY))*-1,NVL(OL.SHIPPED_QUANTITY,NVL(OL.FULFILLED_QUANTITY,OL.ORDERED_QUANTITY)))),0) UNITS_SOLD,
                                      NVL(SUM((NVL(DECODE (LINE_CATEGORY_CODE, ''RETURN'',NVL(OL.SHIPPED_QUANTITY,NVL(OL.FULFILLED_QUANTITY,OL.ORDERED_QUANTITY))*-1,NVL(OL.SHIPPED_QUANTITY,NVL(OL.FULFILLED_QUANTITY,OL.ORDERED_QUANTITY))),0)*OL.UNIT_SELLING_PRICE)),0) ACTUAL_SALES,
                                      AVG(NVL(ol.unit_cost,0)) unit_cost ,
                                      ol.inventory_item_id inventory_item_id,
                                      ol.sold_to_org_id cust_account_id,
                                      max(0),
                                      op.list_header_id list_header_id,
                                      MIN(exid.cat) cat,
                                      MIN(exid.category_id) category_id
                                 from OE_ORDER_LINES_all              OL,
                                      OE_ORDER_HEADERS_all            OH,
                                      oe_price_adjustments            OP,
                                      qp_list_headers                 qh,
                                      xxeis.EIS_XXWC_ITEM_DATA            exid
                                where OL.FLOW_STATUS_CODE       = ''CLOSED''
                                            AND ol.inventory_item_id      =  exid.inventory_item_id                                           
                                  AND oh.header_id              = ol.header_id 
                                  AND ol.line_id                = op.line_id
                                  AND op.header_id              = oh.header_id 
                                  AND QH.attribute10            = ''Contract Pricing'' 
                                  AND qh.list_header_id         = op.list_header_id'
                                  ||l_start_date || l_end_date 
                                  ||' group by ol.sold_to_org_id, ol.inventory_item_id,op.list_header_id';
       
       fnd_file.put_line(fnd_file.log,'The sql unit sales update query is'||L_UNIT_SALES_DATA );
       OPEN l_ref_cursor7 for L_UNIT_SALES_DATA;
       LOOP
       FETCH L_REF_CURSOR7 BULK COLLECT INTO G_VIEW_RECORD7 LIMIT 10000;
              
          l_count := 0;
          FOR j in 1..G_VIEW_RECORD7.count 
          Loop
                G_VIEW_TAB7(j).unit_sold                :=  G_VIEW_RECORD7(j).unit_sold; 
                G_VIEW_TAB7(j).actual_sales             :=  G_VIEW_RECORD7(j).actual_sales; 
                G_VIEW_TAB7(j).unit_cost                :=  G_VIEW_RECORD7(j).unit_cost; 
                G_VIEW_TAB7(j).inventory_item_id        :=  G_VIEW_RECORD7(j).inventory_item_id; 
                G_VIEW_TAB7(j).cust_account_id          :=  G_VIEW_RECORD7(j).cust_account_id ; 
                G_VIEW_TAB7(j).process_id               :=  P_PROCESS_ID; 
                G_VIEW_TAB7(j).LIST_HEADER_ID           :=  G_VIEW_RECORD7(j).LIST_HEADER_ID ;
                G_VIEW_TAB7(j).cat                      :=  G_VIEW_RECORD7(j).cat ;
                G_VIEW_TAB7(j).category_id              :=  G_VIEW_RECORD7(j).category_id ;
                 l_count := l_count+1;
         END LOOP;   
           
           
         
         IF l_count >= 1  then
                  forall j in 1 .. G_VIEW_TAB7.count 
                 --forall i IN g_view_tab.FIRST .. g_view_tab.LAST 
                     INSERT INTO xxeis.EIS_XXWC_UNIT_SALES_DATA_GT
                          values g_view_tab7 (j);    
                     COMMIT;
         end if;  
         
         G_VIEW_TAB7.delete;
         G_VIEW_RECORD7.delete;
         IF l_ref_cursor7%NOTFOUND Then
               CLOSE l_ref_cursor7;
               EXIT;
         End if;
       END LOOP;    
     END IF; */
  
  --COMMIT;
  
  l_main_sql := 'SELECT EOXC.MODIFIER_NAME           MODIFIER_NAME         
                         ,EOXC.CUSTOMER_NAME           CUSTOMER_NAME          
                         ,EOXC.QUALIFIER_ATTRIBUTE     QUALIFIER_ATTRIBUTE  
                         ,EOXC.PRODUCT                 PRODUCT
                         ,EOXC.DESCRIPTION             DESCRIPTION              
                         ,EOXC.CAT_CLASS               CAT_CLASS     
                         ,XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_INV_GM(decode(EOXC.INVENTORY_ITEM_ID,0,NULL,EOXC.INVENTORY_ITEM_ID),XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_COST_LOCATION, EOXC.CATEGORY_ID)                      GM 
                         ,EOXC.LIST_PRICE             LIST_PRICE
                         ,EOXC.APP                     APP 
                         ,EOXC.VALUE1                  VALUE1
                         ,EOXC.CONTRACT_PRICE          CONTRACT_PRICE  
                         ,EOXC.UNITS_SOLD              UNITS_SOLD  
                         ,EOXC.ACTUAL_SALES            ACTUAL_SALES  
                         ,EOXC.ACTUAL_GROSS_MARGIN     ACTUAL_GROSS_MARGIN  
                         ,CASE WHEN EOXC.INVENTORY_ITEM_ID = 0 THEN
                           0
                        ELSE  ROUND(APPS.CST_COST_API.GET_ITEM_COST(1,EOXC.INVENTORY_ITEM_ID,XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_COST_LOCATION),2)  
                        END CURRENT_LOCATION_COST
                         ,EOXC.NEW_PRICE_OR_NEW_GM_DISC  NEW_PRICE_OR_NEW_GM_DISC
                         ,EOXC.INVENTORY_ITEM_ID         INVENTORY_ITEM_ID
                         ,EOXC.LIST_HEADER_ID            LIST_HEADER_ID
                         ,EOXC.PRODUCT_ATTRIBUTE_TYPE    PRODUCT_ATTRIBUTE_TYPE
                         ,EOXC.CATEGORY_ID               CATEGORY_ID
                         ,EOXC.CUST_ACCOUNT_ID           CUST_ACCOUNT_ID
                         ,EOXC.type                      type
                         ,'||P_PROCESS_ID||'             PROCESS_ID
                       ,EOXC.ARITHMETIC_OPERATOR       ARITHMETIC_OPERATOR
                       ,EOXC.OPERAND                   OPERAND
                       ,EOXC.unit_cost                 unit_cost
                       ,EOXC.CAT                       CAT
                         ,EOXC.CAT_DESC                  CAT_DESC
                       ,EOXC.PRICE_BY_CAT              PRICE_BY_CAT
                       ,EOXC.contract_type             contract_type
                       ,EOXC.FORMULA_NAME              FORMULA_NAME
                       ,EOXC.salesrep                  salesrep
                       ,EOXC.cat_class_desc            cat_class_desc
                       ,EOXC.Transaction_code          Transaction_code
                       ,EOXC.creation_Date             creation_Date
                       ,EOXC.person_id                 person_id
                       ,EOXC.account_number            account_number
                       ,EOXC.common_output_id          common_output_id
                       ,EOXC.location                  location
                       ,EOXC.party_site_number         party_site_number
                       ,EOXC.version_no                version_no
                       ,EOXC.price_type                price_type                       
                   FROM XXEIS.eis_xxwc_cont_modi_pricing EOXC
                   WHERE 1=1
                 -- and EOXC.inventory_item_id in (2930124,2924219,2923045)
                  '||L_SALESREP_STR||'
                  '||l_transaction_type||'
                  '||l_customer_name||'
                  '||l_modifier_name||'
                  '||l_customer_number||'';
  fnd_file.put_line(fnd_file.log,'The sql query is'||L_MAIN_SQL );                
  OPEN l_ref_cursor4  FOR l_main_sql;
  LOOP
   FETCH L_REF_CURSOR4 BULK COLLECT INTO G_VIEW_RECORD4 LIMIT 10000;
               
                 l_count := 0;
              FOR j in 1..G_VIEW_RECORD4.count 
                   Loop
                      l_units_sold := 0;
                      l_actual_sales := 0;
                      l_unit_cost := 0;
                      l_inventory_item_id := g_view_record4(j).inventory_item_id;
                      l_list_header_id     := g_view_record4(j).list_header_id;      
                      l_cust_account_id    := g_view_record4(j).cust_account_id ;       
                      l_category_id         := g_view_record4(j).category_id ;      
                      SELECT xxeis.eis_rs_common_outputs_s.NEXTVAL
                        INTO G_VIEW_TAB4(j).common_output_id
                        FROM DUAL;
                        
                  /*   BEGIN
                   --  FND_FILE.PUT_LINE(FND_FILE.LOG,'l_inventory_item_id is '||L_INVENTORY_ITEM_ID);
                   --  FND_FILE.PUT_LINE(fnd_file.log,'l_org_id is '||l_org_id);
                         SELECT max(xlpt.list_price)
                          INTO L_MAX_LIST_PRICE
                           FROM XXEIS.EIS_XXWC_LIST_PRICE XLPT                             
                          WHERE QUALIFIER_ATTR_VALUE IS NOT NULL
                            AND inventory_item_id = l_inventory_item_id
                            AND QUALIFIER_ATTR_VALUE = L_ORG_ID;
                     --  FND_FILE.PUT_LINE(fnd_file.log,'L_MAX_LIST_PRICE is '||L_MAX_LIST_PRICE);     
                     EXCEPTION 
                       WHEN OTHERS THEN
                         BEGIN
                          SELECT MAX(xlpt.list_price)
                           INTO  L_MAX_LIST_PRICE
                           FROM XXEIS.EIS_XXWC_LIST_PRICE XLPT                             
                          WHERE QUALIFIER_ATTR_VALUE IS NULL
                            AND INVENTORY_ITEM_ID = L_INVENTORY_ITEM_ID;
                          --   FND_FILE.PUT_LINE(fnd_file.log,'L_MAX_LIST_PRICE when others is '||L_MAX_LIST_PRICE);  
                         EXCEPTION 
                           WHEN OTHERS THEN
                              L_MAX_LIST_PRICE := 20000;
                             --  FND_FILE.PUT_LINE(fnd_file.log,'L_MAX_LIST_PRICE In Expection is '||L_MAX_LIST_PRICE);  
                         END;
                     
                     END;*/
                     
                    begin
                    if( (L_INVENTORY_ITEM_ID is not null) and (L_ORG_ID is not null))then
                        select max(XLPT.LIST_PRICE)
                          INTO L_MAX_LIST_PRICE
                           FROM XXEIS.EIS_XXWC_LIST_PRICE XLPT                             
                          where QUALIFIER_ATTR_VALUE is not null
                            and INVENTORY_ITEM_ID = L_INVENTORY_ITEM_ID
                            and QUALIFIER_ATTR_VALUE = L_ORG_ID;
                   -- FND_FILE.PUT_LINE(FND_FILE.log,'Org Exists'||L_MAX_LIST_PRICE||L_INVENTORY_ITEM_ID||L_ORG_ID);
                    end if;
                     if ((L_MAX_LIST_PRICE is null)) then
                     
                          select max(XLPT.LIST_PRICE)
                           INTO  L_MAX_LIST_PRICE
                           FROM XXEIS.EIS_XXWC_LIST_PRICE XLPT                             
                          where QUALIFIER_ATTR_VALUE is null
                            and INVENTORY_ITEM_ID =L_INVENTORY_ITEM_ID ;
                    --       FND_FILE.PUT_LINE(FND_FILE.log,'Org Not Exists'||L_MAX_LIST_PRICE||L_INVENTORY_ITEM_ID||L_ORG_ID);
                   end if; 
                   if L_MAX_LIST_PRICE is null then 
                              L_MAX_LIST_PRICE := 20000;
                      --     fnd_file.put_line(fnd_file.log,'Org else Exists'||L_MAX_LIST_PRICE||L_INVENTORY_ITEM_ID||L_ORG_ID);       
                    end if;
                    
                        EXCEPTION when OTHERS then
                          L_MAX_LIST_PRICE := 20000;
                     --         fnd_file.put_line(fnd_file.log,'Nothing'||L_MAX_LIST_PRICE||L_INVENTORY_ITEM_ID||L_ORG_ID);
                        end;
                                              
                      l_contract_price := 0;
                      IF G_VIEW_RECORD4(j).ARITHMETIC_OPERATOR ='AMT' THEN
                        l_contract_price := (l_max_list_price - G_VIEW_RECORD4(j).OPERAND);
                      ELSIF  G_VIEW_RECORD4(j).ARITHMETIC_OPERATOR = '%' THEN  
                        l_contract_price :=((l_max_list_price )-((G_VIEW_RECORD4(j).OPERAND*l_max_list_price)/100));
                      ELSIF G_VIEW_RECORD4(j).ARITHMETIC_OPERATOR = 'NEWPRICE'  THEN
                        l_contract_price := G_VIEW_RECORD4(j).OPERAND;      
                      ELSIF G_VIEW_RECORD4(j).APP is not null THEN   
                        l_contract_price :=((l_max_list_price)/(1-0.15));           
                      end if; 
                      
                      EIS_OM_XXWC_CONT_PRIC_PKG.get_curr_sold_sales(G_VIEW_RECORD4(J).INVENTORY_ITEM_ID,G_VIEW_RECORD4(J).CATEGORY_ID,G_VIEW_RECORD4(J).CAT,G_VIEW_RECORD4(J).CUST_ACCOUNT_ID,G_VIEW_RECORD4(J).LIST_HEADER_ID,P_START_DATE,P_END_DATE,L_UNITS_SOLD,L_ACTUAL_SALES,L_UNIT_COST);

                      --XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CURR_SOLD_SALES(G_VIEW_RECORD4(J).INVENTORY_ITEM_ID,G_VIEW_RECORD4(J).CATEGORY_ID,G_VIEW_RECORD4(J).CAT,G_VIEW_RECORD4(J).CUST_ACCOUNT_ID,G_VIEW_RECORD4(J).LIST_HEADER_ID,L_UNITS_SOLD,L_ACTUAL_SALES,L_UNIT_COST,P_PROCESS_ID);
                    --  fnd_file.put_line(fnd_file.log,' Not Null Customer:  ' || G_VIEW_RECORD4(j).CUSTOMER_NAME );
                    --  fnd_file.put_line(fnd_file.log,'The before Insert'||l_max_list_price);
                      G_VIEW_TAB4(j).MODIFIER_NAME            := G_VIEW_RECORD4(j).MODIFIER_NAME;
                        G_VIEW_TAB4(j).CUSTOMER_NAME              := G_VIEW_RECORD4(j).CUSTOMER_NAME;
                        G_VIEW_TAB4(j).QUALIFIER_ATTRIBUTE        := G_VIEW_RECORD4(j).QUALIFIER_ATTRIBUTE;
                        G_VIEW_TAB4(j).PRODUCT                    := G_VIEW_RECORD4(j).PRODUCT;
                        G_VIEW_TAB4(j).DESCRIPTION                := G_VIEW_RECORD4(j).DESCRIPTION;    
                        G_VIEW_TAB4(j).CAT_CLASS                  := G_VIEW_RECORD4(j).CAT_CLASS;
                        G_VIEW_TAB4(J).GM                         := G_VIEW_RECORD4(J).GM;
                        G_VIEW_TAB4(j).LIST_PRICE                 := l_max_list_price;
                        G_VIEW_TAB4(j).APP                        := G_VIEW_RECORD4(j).APP;
                        G_VIEW_TAB4(j).VALUE1                     := G_VIEW_RECORD4(j).VALUE1;
                        G_VIEW_TAB4(j).CONTRACT_PRICE             := l_contract_price;
                        G_VIEW_TAB4(j).UNITS_SOLD                 := l_units_sold;
                        G_VIEW_TAB4(j).ACTUAL_SALES               := l_actual_sales;
                        G_VIEW_TAB4(j).ACTUAL_GROSS_MARGIN        := G_VIEW_RECORD4(j).ACTUAL_GROSS_MARGIN;
                        G_VIEW_TAB4(j).CURRENT_LOCATION_COST      := G_VIEW_RECORD4(j).CURRENT_LOCATION_COST;
                        G_VIEW_TAB4(j).NEW_PRICE_OR_NEW_GM_DISC   := G_VIEW_RECORD4(j).NEW_PRICE_OR_NEW_GM_DISC;
                        G_VIEW_TAB4(j).INVENTORY_ITEM_ID          := G_VIEW_RECORD4(j).INVENTORY_ITEM_ID;
                        G_VIEW_TAB4(j).LIST_HEADER_ID             := G_VIEW_RECORD4(j).LIST_HEADER_ID;
                        G_VIEW_TAB4(j).PRODUCT_ATTRIBUTE_TYPE     := G_VIEW_RECORD4(j).PRODUCT_ATTRIBUTE_TYPE;
                        G_VIEW_TAB4(j).CATEGORY_ID                := G_VIEW_RECORD4(j).CATEGORY_ID;
                        G_VIEW_TAB4(j).CUST_ACCOUNT_ID            := G_VIEW_RECORD4(j).CUST_ACCOUNT_ID;
                        G_VIEW_TAB4(j).type                       := G_VIEW_RECORD4(j).type;
                        G_VIEW_TAB4(j).PROCESS_ID                 := G_VIEW_RECORD4(j).PROCESS_ID;
                      G_VIEW_TAB4(j).ARITHMETIC_OPERATOR        := G_VIEW_RECORD4(j).ARITHMETIC_OPERATOR;
                      G_VIEW_TAB4(j).OPERAND                    := G_VIEW_RECORD4(j).OPERAND;
                      G_VIEW_TAB4(j).unit_cost                  := l_unit_cost;
                      G_VIEW_TAB4(j).CAT                        := G_VIEW_RECORD4(j).CAT;
                        G_VIEW_TAB4(j).CAT_DESC                   := G_VIEW_RECORD4(j).CAT_DESC;
                      G_VIEW_TAB4(j).PRICE_BY_CAT               := G_VIEW_RECORD4(j).PRICE_BY_CAT;
                      G_VIEW_TAB4(j).contract_type              := G_VIEW_RECORD4(j).contract_type;
                      G_VIEW_TAB4(j).FORMULA_NAME               := G_VIEW_RECORD4(j).FORMULA_NAME;
                      G_VIEW_TAB4(j).salesrep                   := G_VIEW_RECORD4(j).salesrep;
                      G_VIEW_TAB4(j).cat_class_desc             := G_VIEW_RECORD4(j).cat_class_desc;
                      G_VIEW_TAB4(j).Transaction_code           := G_VIEW_RECORD4(j).Transaction_code;
                      G_VIEW_TAB4(j).person_id                  := G_VIEW_RECORD4(j).person_id;
                      G_VIEW_TAB4(j).account_number             := G_VIEW_RECORD4(j).account_number;
                      --G_VIEW_TAB4(j).common_output_id           := G_VIEW_RECORD4(j).common_output_id;
                      G_VIEW_TAB4(j).location                   := G_VIEW_RECORD4(j).location;
                      G_VIEW_TAB4(j).party_site_number          := G_VIEW_RECORD4(j).party_site_number;
                      G_VIEW_TAB4(j).version_no                 := G_VIEW_RECORD4(j).version_no;
                      G_VIEW_TAB4(j).price_type                 := G_VIEW_RECORD4(j).price_type;
                 l_count := l_count+1;
              END LOOP;   
 
         
               IF l_count >= 1  then
                  forall j in 1 .. G_VIEW_TAB4.count 
                    --forall i IN g_view_tab.FIRST .. g_view_tab.LAST 
                     INSERT INTO XXEIS.EIS_XXWC_CONT_MODI_PRICING_V1
                          values g_view_tab4 (j);    
                   COMMIT;       
               end if;  
         
               G_VIEW_TAB4.delete;
               G_VIEW_RECORD4.delete;
               IF l_ref_cursor4%NOTFOUND Then
               CLOSE l_ref_cursor4;
               EXIT;
               End if;
           END LOOP; 
 --delete from xxeis.EIS_XXWC_UNIT_SALES_DATA_GT   where process_id = p_process_id;
 commit;
                  
END CONTRACT_PRICING_MODIFIER;

END EIS_OM_XXWC_CONT_PRIC_PKG;
/