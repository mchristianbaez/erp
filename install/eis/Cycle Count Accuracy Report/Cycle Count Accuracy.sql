--Report Name            : Cycle Count Accuracy
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View EIS_XXWC_CYCLE_ACC_COUNT_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_CYCLE_ACC_COUNT_V
Create or replace View XXEIS.EIS_XXWC_CYCLE_ACC_COUNT_V
 AS 
SELECT MP.ATTRIBUTE8 DISTRICT,
    mp.attribute9 region,
    OOD.ORGANIZATION_NAME ORG,
    OOD.ORGANIZATION_CODE ORG_CODE,
    mcce.count_date_first count_date,
    xxeis.EIS_RS_XXWC_COM_UTIL_PKG.GET_INV_CAT_SEGMENTS(MCCE.INVENTORY_ITEM_ID,MCCE.ORGANIZATION_ID) CAT_CALSS,
    XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_INV_VEL_CAT_CLASS(MCCE.INVENTORY_ITEM_ID,MCCE.ORGANIZATION_ID) SV_CLASS,
    MSI.PRIMARY_UOM_CODE PRIMARY_UOM_CODE,
    MSI.CONCATENATED_SEGMENTS ITEM_NUMBER ,
    msi.description item_description,
    CASE
      WHEN msi.lot_control_code=2
      THEN 'Y'
      WHEN (msi.shelf_life_days >0
      AND msi.shelf_life_days   <10000)
      THEN 'Y'
      ELSE 'N'
    END AS ts,
    --APPS.CST_COST_API.GET_ITEM_COST (1,MCCE.INVENTORY_ITEM_ID,MCCE.ORGANIZATION_ID) AVG_COST,
    MCCE.ITEM_UNIT_COST AVG_COST, -- Malli Changes on 02/18/2014
    TRUNC(MCCE.COUNT_DATE_FIRST) BEGINING_COUNT_DATE,
    TRUNC(MCCE.COUNT_DATE_CURRENT) END_COUNT_DATE,
    MCCE.COUNT_DATE_PRIOR PRIOR_COUNT_DATE,
    MCCE.SYSTEM_QUANTITY_FIRST STARTING_QUANTITY,
    MCCE.COUNT_QUANTITY_PRIOR PRIOR_QUANTITY,
    mcce.count_quantity_current end_quantity,
    NVL(mcce.count_quantity_current ,0)- NVL(mcce.system_quantity_first,0) var_quantity, ------- diane changed 2/5/2014
    -- mcce.adjustment_quantity                                               var_quantity,
    --  nvl( mcce.adjustment_quantity ,0)*100/decode(nvl(mcce.system_quantity_first  ,0) ,0,1, mcce.system_quantity_first)  varquanper,
    (NVL(mcce.count_quantity_current ,0) - NVL(mcce.system_quantity_first,0))*100/DECODE(NVL(mcce.system_quantity_first,0) ,0,1,mcce.system_quantity_first) varquanper,         ------- diane changed 2/5/2014
    --(NVL(MCCE.COUNT_QUANTITY_CURRENT ,0) - NVL(MCCE.SYSTEM_QUANTITY_FIRST,0))* NVL(APPS.CST_COST_API.GET_ITEM_COST (1,MCCE.INVENTORY_ITEM_ID,MCCE.ORGANIZATION_ID),1) VAR_COST, ------- diane changed 2/5/2014
    (NVL(MCCE.COUNT_QUANTITY_CURRENT ,0) - NVL(MCCE.SYSTEM_QUANTITY_FIRST,0))* NVL(MCCE.ITEM_UNIT_COST,1) VAR_COST, ------- Malli changed 2/18/2014
    --  MCCE.ADJUSTMENT_AMOUNT VAR_COST,
    /*NVL(MCCE.ADJUSTMENT_AMOUNT,0)*100/
    DECODE((NVL(MCCE.SYSTEM_QUANTITY_FIRST,0))* NVL(APPS.CST_COST_API.GET_ITEM_COST (1,MCCE.INVENTORY_ITEM_ID,MCCE.ORGANIZATION_ID),1),0,1,
    (NVL(MCCE.SYSTEM_QUANTITY_FIRST,0))* NVL(APPS.CST_COST_API.GET_ITEM_COST (1,MCCE.INVENTORY_ITEM_ID,MCCE.ORGANIZATION_ID),1))
    */
    -- (NVL(MCCE.SYSTEM_QUANTITY_FIRST,0)) * NVL(APPS.CST_COST_API.GET_ITEM_COST (1,MCCE.INVENTORY_ITEM_ID,MCCE.ORGANIZATION_ID),1) TOTAL_COST,
    ((NVL(MCCE.SYSTEM_QUANTITY_FIRST,0)*NVL(MCCE.ITEM_UNIT_COST,1)) -(NVL(MCCE.COUNT_QUANTITY_CURRENT ,0)* NVL(MCCE.ITEM_UNIT_COST,1) ))*100 /DECODE((NVL(MCCE.COUNT_QUANTITY_FIRST,0))* NVL(MCCE.ITEM_UNIT_COST,1),0,1, (NVL(MCCE.COUNT_QUANTITY_FIRST,0))* NVL(MCCE.ITEM_UNIT_COST,1)) TOTAL_VAR_COST ,
    msi.inventory_item_id,
    DECODE(SIGN(MCCE.COUNT_QUANTITY_CURRENT),0,0,1,1,-1,1,0) count_END_QUANTITY,
    DECODE(SIGN( NVL(mcce.count_quantity_current ,0) -NVL(mcce.system_quantity_first,0)),0,0, 1,1,-1,1,0) count_var_quantity
  FROM MTL_CYCLE_COUNT_ENTRIES_V MCCE,
    MTL_PARAMETERS MP,
    ORG_ORGANIZATION_DEFINITIONS OOD,
    MTL_SYSTEM_ITEMS_B_KFV MSI
  WHERE MP.ORGANIZATION_ID    = MCCE.ORGANIZATION_ID
  AND ood.organization_id     = mcce.organization_id
  AND MSI.INVENTORY_ITEM_ID   = MCCE.INVENTORY_ITEM_ID
  AND MCCE.ENTRY_STATUS_CODE <> 4--Excluding Rejetced Cycles
  AND msi.organization_id     = mcce.organization_id
/
set scan on define on
prompt Creating View Data for Cycle Count Accuracy
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_CYCLE_ACC_COUNT_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_CYCLE_ACC_COUNT_V',401,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Xxwc Cycle Acc Count V','EXCACV','','');
--Delete View Columns for EIS_XXWC_CYCLE_ACC_COUNT_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_CYCLE_ACC_COUNT_V',401,FALSE);
--Inserting View Columns for EIS_XXWC_CYCLE_ACC_COUNT_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CYCLE_ACC_COUNT_V','VARQUANPER',401,'Varquanper','VARQUANPER','','','','XXEIS_RS_ADMIN','NUMBER','','','Varquanper','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CYCLE_ACC_COUNT_V','END_QUANTITY',401,'End Quantity','END_QUANTITY','','','','XXEIS_RS_ADMIN','NUMBER','','','End Quantity','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CYCLE_ACC_COUNT_V','STARTING_QUANTITY',401,'Starting Quantity','STARTING_QUANTITY','','','','XXEIS_RS_ADMIN','NUMBER','','','Starting Quantity','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CYCLE_ACC_COUNT_V','END_COUNT_DATE',401,'End Count Date','END_COUNT_DATE','','','','XXEIS_RS_ADMIN','DATE','','','End Count Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CYCLE_ACC_COUNT_V','BEGINING_COUNT_DATE',401,'Begining Count Date','BEGINING_COUNT_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Begining Count Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CYCLE_ACC_COUNT_V','AVG_COST',401,'Avg Cost','AVG_COST','','','','XXEIS_RS_ADMIN','NUMBER','','','Avg Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CYCLE_ACC_COUNT_V','TS',401,'Ts','TS','','','','XXEIS_RS_ADMIN','CHAR','','','Ts','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CYCLE_ACC_COUNT_V','ITEM_DESCRIPTION',401,'Item Description','ITEM_DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Item Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CYCLE_ACC_COUNT_V','ITEM_NUMBER',401,'Item Number','ITEM_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Item Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CYCLE_ACC_COUNT_V','PRIMARY_UOM_CODE',401,'Primary Uom Code','PRIMARY_UOM_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Primary Uom Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CYCLE_ACC_COUNT_V','SV_CLASS',401,'Sv Class','SV_CLASS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Sv Class','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CYCLE_ACC_COUNT_V','CAT_CALSS',401,'Cat Calss','CAT_CALSS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cat Calss','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CYCLE_ACC_COUNT_V','COUNT_DATE',401,'Count Date','COUNT_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Count Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CYCLE_ACC_COUNT_V','ORG_CODE',401,'Org Code','ORG_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Org Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CYCLE_ACC_COUNT_V','REGION',401,'Region','REGION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Region','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CYCLE_ACC_COUNT_V','DISTRICT',401,'District','DISTRICT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','District','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CYCLE_ACC_COUNT_V','VAR_QUANTITY',401,'Var Quantity','VAR_QUANTITY','','','','XXEIS_RS_ADMIN','NUMBER','','','Var Quantity','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CYCLE_ACC_COUNT_V','COUNT_END_QUANTITY',401,'Count End Quantity','COUNT_END_QUANTITY','','','','XXEIS_RS_ADMIN','NUMBER','','','Count End Quantity','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CYCLE_ACC_COUNT_V','COUNT_VAR_QUANTITY',401,'Count Var Quantity','COUNT_VAR_QUANTITY','','','','XXEIS_RS_ADMIN','NUMBER','','','Count Var Quantity','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CYCLE_ACC_COUNT_V','TOTAL_VAR_COST',401,'Total Var Cost','TOTAL_VAR_COST','','','','XXEIS_RS_ADMIN','NUMBER','','','Total Var Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CYCLE_ACC_COUNT_V','VAR_COST',401,'Var Cost','VAR_COST','','','','XXEIS_RS_ADMIN','NUMBER','','','Var Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CYCLE_ACC_COUNT_V','ORG',401,'Org','ORG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Org','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CYCLE_ACC_COUNT_V','INVENTORY_ITEM_ID',401,'Inventory Item Id','INVENTORY_ITEM_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Inventory Item Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CYCLE_ACC_COUNT_V','PRIOR_COUNT_DATE',401,'Prior Count Date','PRIOR_COUNT_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Prior Count Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CYCLE_ACC_COUNT_V','PRIOR_QUANTITY',401,'Prior Quantity','PRIOR_QUANTITY','','','','XXEIS_RS_ADMIN','NUMBER','','','Prior Quantity','','','');
--Inserting View Components for EIS_XXWC_CYCLE_ACC_COUNT_V
--Inserting View Component Joins for EIS_XXWC_CYCLE_ACC_COUNT_V
END;
/
set scan on define on
prompt Creating Report LOV Data for Cycle Count Accuracy
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Cycle Count Accuracy
xxeis.eis_rs_ins.lov( 401,'SELECT DISTINCT concatenated_segments item, description FROM mtl_system_items_kfv msi,
                org_organization_definitions ood
          WHERE msi.organization_id = ood.organization_id
            AND ood.operating_unit = fnd_profile.VALUE (''ORG_ID'')
       ORDER BY concatenated_segments','','EIS_INV_ITEM_LOV','List of all inventory items.','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'select list_name from XXWC_PARAM_LIST where list_type = ''Org'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Org List','','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'select distinct ATTRIBUTE9 Region from mtl_parameters','','Region Lov','','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'select list_name from XXWC_PARAM_LIST where list_type = ''Item'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Item List','','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'SELECT organization_code code,organization_name name
FROM ORG_ORGANIZATION_DEFINITIONS OOD
WHERE EXISTS
  (SELECT 1
  FROM XXEIS.EIS_ORG_ACCESS_V
  WHERE organization_id = ood.organization_id
  )
ORDER BY organization_code','','XXWC INV ORGANIZATIONS LOV','List of All Inventory Orgs under a given operating unit.','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'select distinct ATTRIBUTE8 District from mtl_parameters','','District Lov','District Lov','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'select list_name from XXWC_PARAM_LIST where list_type = ''Cat Class''  and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Cat Class List','','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'select distinct  MCV.SEGMENT1
        from  MTL_CATEGORIES_KFV    MCV,
                 MTL_CATEGORY_SETS     MCS,
                 Mtl_Item_Categories         Mic
          Where   Mcs.Category_Set_Name       =  ''Inventory Category''
          And Mcs.Structure_Id                         =  Mcv.Structure_Id
          and MIC.CATEGORY_SET_ID               =  MCS.CATEGORY_SET_ID
          And Mic.Category_Id                           =  Mcv.Category_Id','','XXWC Cat Class Lov','','ANONYMOUS',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Cycle Count Accuracy
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Cycle Count Accuracy
xxeis.eis_rs_utility.delete_report_rows( 'Cycle Count Accuracy' );
--Inserting Report - Cycle Count Accuracy
xxeis.eis_rs_ins.r( 401,'Cycle Count Accuracy','','','','','','XXEIS_RS_ADMIN','EIS_XXWC_CYCLE_ACC_COUNT_V','Y','','','XXEIS_RS_ADMIN','','N','White Cap Reports','RTF,PDF,','CSV,HTML,Html Summary,XML,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - Cycle Count Accuracy
xxeis.eis_rs_ins.rc( 'Cycle Count Accuracy',401,'AVG_COST','Avg Cost','Avg Cost','','~T~D~2','default','','17','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CYCLE_ACC_COUNT_V','','');
xxeis.eis_rs_ins.rc( 'Cycle Count Accuracy',401,'CAT_CALSS','Cat Class','Cat Calss','','','default','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CYCLE_ACC_COUNT_V','','');
xxeis.eis_rs_ins.rc( 'Cycle Count Accuracy',401,'COUNT_DATE','Count Date','Count Date','','','default','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CYCLE_ACC_COUNT_V','','');
xxeis.eis_rs_ins.rc( 'Cycle Count Accuracy',401,'DISTRICT','District','District','','','default','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CYCLE_ACC_COUNT_V','','');
xxeis.eis_rs_ins.rc( 'Cycle Count Accuracy',401,'END_QUANTITY','Counted Qty','End Quantity','','~~~','default','','12','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CYCLE_ACC_COUNT_V','','');
xxeis.eis_rs_ins.rc( 'Cycle Count Accuracy',401,'ITEM_DESCRIPTION','Item Description','Item Description','','','default','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CYCLE_ACC_COUNT_V','','');
xxeis.eis_rs_ins.rc( 'Cycle Count Accuracy',401,'ITEM_NUMBER','Item','Item Number','','','default','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CYCLE_ACC_COUNT_V','','');
xxeis.eis_rs_ins.rc( 'Cycle Count Accuracy',401,'PRIMARY_UOM_CODE','UOM','Primary Uom Code','','','default','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CYCLE_ACC_COUNT_V','','');
xxeis.eis_rs_ins.rc( 'Cycle Count Accuracy',401,'REGION','Region','Region','','','default','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CYCLE_ACC_COUNT_V','','');
xxeis.eis_rs_ins.rc( 'Cycle Count Accuracy',401,'STARTING_QUANTITY','Starting Quantity','Starting Quantity','','~~~','default','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CYCLE_ACC_COUNT_V','','');
xxeis.eis_rs_ins.rc( 'Cycle Count Accuracy',401,'SV_CLASS','SV Class','Sv Class','','','default','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CYCLE_ACC_COUNT_V','','');
xxeis.eis_rs_ins.rc( 'Cycle Count Accuracy',401,'TS','TS','Ts','','','default','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CYCLE_ACC_COUNT_V','','');
xxeis.eis_rs_ins.rc( 'Cycle Count Accuracy',401,'VARQUANPER','Variance%','Varquanper','','~T~D~2','default','','15','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CYCLE_ACC_COUNT_V','','');
xxeis.eis_rs_ins.rc( 'Cycle Count Accuracy',401,'VAR_QUANTITY','Variance Qty','Var Quantity','','~~~','default','','13','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CYCLE_ACC_COUNT_V','','');
xxeis.eis_rs_ins.rc( 'Cycle Count Accuracy',401,'COUNT_END_QUANTITY','Lines Counted','Count End Quantity','','~~~','default','','20','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CYCLE_ACC_COUNT_V','','');
xxeis.eis_rs_ins.rc( 'Cycle Count Accuracy',401,'COUNT_VAR_QUANTITY','Variance Line','Count Var Quantity','','~~~','default','','21','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CYCLE_ACC_COUNT_V','','');
xxeis.eis_rs_ins.rc( 'Cycle Count Accuracy',401,'ORG_CODE','Organization','Org Code','','','default','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CYCLE_ACC_COUNT_V','','');
xxeis.eis_rs_ins.rc( 'Cycle Count Accuracy',401,'TOTAL','Total $$ Variance','Org Code','NUMBER','~T~D~2','default','','18','Y','','','','','','','EXCACV.VAR_QUANTITY*EXCACV.AVG_COST','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CYCLE_ACC_COUNT_V','','');
xxeis.eis_rs_ins.rc( 'Cycle Count Accuracy',401,'TOTAL COST','Cost Variance %','Org Code','NUMBER','~T~D~2','default','','19','Y','','','','','','','100*(EXCACV.VAR_QUANTITY*EXCACV.AVG_COST)/decode(EXCACV.STARTING_QUANTITY*EXCACV.AVG_COST,0,1,EXCACV.STARTING_QUANTITY*EXCACV.AVG_COST)','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CYCLE_ACC_COUNT_V','','');
--Inserting Report Parameters - Cycle Count Accuracy
xxeis.eis_rs_ins.rp( 'Cycle Count Accuracy',401,'Region','Region','REGION','IN','Region Lov','','VARCHAR2','N','Y','1','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Cycle Count Accuracy',401,'District','District','DISTRICT','IN','District Lov','','VARCHAR2','N','Y','2','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Cycle Count Accuracy',401,'Org','Org','ORG_CODE','IN','XXWC INV ORGANIZATIONS LOV','','VARCHAR2','N','Y','3','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Cycle Count Accuracy',401,'OrgList','Org List','','IN','XXWC Org List','','VARCHAR2','N','Y','4','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Cycle Count Accuracy',401,'Count Date Beginning','Count Date Beginning','BEGINING_COUNT_DATE','>=','','','DATE','N','Y','5','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Cycle Count Accuracy',401,'Count date ending','Count date ending','END_COUNT_DATE','<=','','','DATE','N','Y','6','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Cycle Count Accuracy',401,'Cat Class','Cat Class','CAT_CALSS','IN','XXWC Cat Class Lov','','VARCHAR2','N','Y','7','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Cycle Count Accuracy',401,'Item','Item','ITEM_NUMBER','IN','EIS_INV_ITEM_LOV','','VARCHAR2','N','Y','8','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Cycle Count Accuracy',401,'CatList','Cat Class List','','IN','XXWC Cat Class List','','VARCHAR2','N','Y','9','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Cycle Count Accuracy',401,'ItemList','Item List','','IN','XXWC Item List','','VARCHAR2','N','Y','10','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
--Inserting Report Conditions - Cycle Count Accuracy
xxeis.eis_rs_ins.rcn( 'Cycle Count Accuracy',401,'','','','',' AND (:OrgList is NULL  OR
          EXISTS  (SELECT 1
  from xxeis.eis_xxwc_param_parse_list x
  where x.list_name     =:OrgList
  and x.list_type     =''Org''
 and x.process_id    = :system.process_id
and  EXCACV.ORG_CODE =regexp_replace(x.list_value,''[^[a-z,A-Z,0-9]]*'')))
AND ( :ItemList is NULL  OR
          EXISTS  (SELECT 1
  from xxeis.eis_xxwc_param_parse_list x
  where x.list_name     =:ItemList
  and x.list_type     =''Item''
 and x.process_id    = :system.process_id
and  EXCACV.ITEM_NUMBER =regexp_replace(x.list_value,''[^[a-z,A-Z,0-9]]*'')))
AND ( :CatList is NULL  OR
          EXISTS  (SELECT 1
  from xxeis.eis_xxwc_param_parse_list x
  where x.list_name     =:CatList
  and x.list_type     =''Cat Class''
 and x.process_id    = :system.process_id
and  EXCACV.CAT_CALSS =regexp_replace(x.list_value,''[^[a-z,A-Z,0-9]]*'')))','Y','0','','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Cycle Count Accuracy',401,'REGION','IN',':Region','','','Y','1','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Cycle Count Accuracy',401,'DISTRICT','IN',':District','','','Y','2','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Cycle Count Accuracy',401,'ORG_CODE','IN',':Org','','','Y','3','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Cycle Count Accuracy',401,'BEGINING_COUNT_DATE','>=',':Count Date Beginning','','','Y','5','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Cycle Count Accuracy',401,'END_COUNT_DATE','<=',':Count date ending','','','Y','6','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Cycle Count Accuracy',401,'CAT_CALSS','IN',':Cat Class','','','Y','7','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Cycle Count Accuracy',401,'ITEM_NUMBER','IN',':Item','','','Y','8','Y','XXEIS_RS_ADMIN');
--Inserting Report Sorts - Cycle Count Accuracy
--Inserting Report Triggers - Cycle Count Accuracy
xxeis.eis_rs_ins.rt( 'Cycle Count Accuracy',401,'Declare 
L_TEMP         varchar2(240);
L_ITEM_LIST     varchar2(240);
L_CAT_LIST     varchar2(240);
begin
L_TEMP         :=:OrgList ;
L_ITEM_LIST    :=:ItemList;
L_CAT_LIST    :=:CatList;
if l_temp is not null then 
XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.PARSE_PARAM_LIST(P_PROCESS_ID => :SYSTEM.PROCESS_ID,
                        P_LIST_NAME => L_TEMP ,
                        P_LIST_TYPE => ''Org''
                        );
xxeis.EIS_RS_XXWC_COM_UTIL_PKG.g_flag_value:=''Y'';
else
xxeis.EIS_RS_XXWC_COM_UTIL_PKG.g_flag_value:=''N'';
end if;
if L_ITEM_LIST is not null then 
XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.PARSE_PARAM_LIST(P_PROCESS_ID    => :SYSTEM.PROCESS_ID,
                        P_LIST_NAME    => L_ITEM_LIST,
                        P_LIST_TYPE    => ''Item''
                        );    
xxeis.EIS_RS_XXWC_COM_UTIL_PKG.G_ITEM_FLAG:=''Y'';
else
xxeis.EIS_RS_XXWC_COM_UTIL_PKG.G_ITEM_FLAG:=''N'';
end if; 
if L_CAT_LIST is not null then 
XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.PARSE_PARAM_LIST(P_PROCESS_ID    => :SYSTEM.PROCESS_ID,
                        P_LIST_NAME    => L_CAT_LIST,
                        P_LIST_TYPE    => ''Cat Class'');  
xxeis.EIS_RS_XXWC_COM_UTIL_PKG.g_cat_flag:=''Y'';
else
XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.g_cat_flag:=''N'';
end if; 
end;','B','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rt( 'Cycle Count Accuracy',401,'BEGIN
xxeis.EIS_RS_XXWC_COM_UTIL_PKG.parse_cleanup_table(P_PROCESS_ID    => :SYSTEM.PROCESS_ID);
END;','A','Y','XXEIS_RS_ADMIN');
--Inserting Report Templates - Cycle Count Accuracy
--Inserting Report Portals - Cycle Count Accuracy
--Inserting Report Dashboards - Cycle Count Accuracy
--Inserting Report Security - Cycle Count Accuracy
xxeis.eis_rs_ins.rsec( 'Cycle Count Accuracy','401','','50884',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cycle Count Accuracy','401','','50855',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cycle Count Accuracy','401','','50981',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cycle Count Accuracy','401','','50882',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cycle Count Accuracy','401','','50883',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cycle Count Accuracy','401','','51004',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cycle Count Accuracy','401','','50619',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cycle Count Accuracy','201','','50892',401,'XXEIS_RS_ADMIN','','');
--Inserting Report Pivots - Cycle Count Accuracy
xxeis.eis_rs_ins.rpivot( 'Cycle Count Accuracy',401,'Summary By Line','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Summary By Line
xxeis.eis_rs_ins.rpivot_dtls( 'Cycle Count Accuracy',401,'Summary By Line','COUNT_END_QUANTITY','DATA_FIELD','SUM','Lines Counted','1','','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Cycle Count Accuracy',401,'Summary By Line','COUNT_VAR_QUANTITY','DATA_FIELD','SUM','Variance Line','2','','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Cycle Count Accuracy',401,'Summary By Line','REGION','ROW_FIELD','','','2','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'Cycle Count Accuracy',401,'Summary By Line','DISTRICT','ROW_FIELD','','','3','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'Cycle Count Accuracy',401,'Summary By Line','ORG_CODE','ROW_FIELD','','','1','','');
--Inserting Report Summary Calculation Columns For Pivot- Summary By Line
xxeis.eis_rs_ins.rpivot_sum_cal( 'Cycle Count Accuracy',401,'Summary By Line','100*<Variance Line>/<Lines Counted>','Variance','1');
xxeis.eis_rs_ins.rpivot( 'Cycle Count Accuracy',401,'Summary By Region, by District','2','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Summary By Region, by District
xxeis.eis_rs_ins.rpivot_dtls( 'Cycle Count Accuracy',401,'Summary By Region, by District','VAR_QUANTITY','DATA_FIELD','SUM','Variance Count','2','','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Cycle Count Accuracy',401,'Summary By Region, by District','DISTRICT','ROW_FIELD','','','2','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'Cycle Count Accuracy',401,'Summary By Region, by District','REGION','ROW_FIELD','','','1','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'Cycle Count Accuracy',401,'Summary By Region, by District','STARTING_QUANTITY','DATA_FIELD','SUM','Total Item Count','1','','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Cycle Count Accuracy',401,'Summary By Region, by District','ORG_CODE','ROW_FIELD','','','3','','');
--Inserting Report Summary Calculation Columns For Pivot- Summary By Region, by District
xxeis.eis_rs_ins.rpivot_sum_cal( 'Cycle Count Accuracy',401,'Summary By Region, by District','100*<Variance Count>/<Total Item Count>','Varaiance%','1');
END;
/
set scan on define on
