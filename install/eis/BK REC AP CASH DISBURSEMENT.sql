--Report Name            : BK REC AP CASH DISBURSEMENT
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data XXEIS_CASH_DISBURSMENT
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Inserting Object XXEIS_CASH_DISBURSMENT
xxeis.eis_rsc_ins.v( 'XXEIS_CASH_DISBURSMENT',200,'Paste SQL View for HDS AP CASH DISBURSEMENT','1.0','','','XXEIS_RS_ADMIN','XXEIS','XXEIS_CASH_DISBURSMENT','X4TV','','','VIEW','US','','');
--Delete Object Columns for XXEIS_CASH_DISBURSMENT
xxeis.eis_rsc_utility.delete_view_rows('XXEIS_CASH_DISBURSMENT',200,FALSE);
--Inserting Object Columns for XXEIS_CASH_DISBURSMENT
xxeis.eis_rsc_ins.vc( 'XXEIS_CASH_DISBURSMENT','BANK_ACCOUNT_NAME',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Bank Account Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_CASH_DISBURSMENT','CHECK_DATE',200,'','','','','','XXEIS_RS_ADMIN','DATE','','','Check Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_CASH_DISBURSMENT','CHECK_NUMBER',200,'','','','','','XXEIS_RS_ADMIN','NUMBER','','','Check Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_CASH_DISBURSMENT','PAYMENT_STATUS_DESCR',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Payment Status Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_CASH_DISBURSMENT','VOID_DATE',200,'','','','','','XXEIS_RS_ADMIN','DATE','','','Void Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_CASH_DISBURSMENT','STOP_PAYMENT_RECORDED_DATE',200,'','','','','','XXEIS_RS_ADMIN','DATE','','','Stop Payment Recorded Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_CASH_DISBURSMENT','VENDOR_NAME',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_CASH_DISBURSMENT','ADDRESS_LINE1',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Address Line1','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_CASH_DISBURSMENT','ADDRESS_LINE2',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Address Line2','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_CASH_DISBURSMENT','ADDRESS_LINE3',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Address Line3','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_CASH_DISBURSMENT','VENDOR_CITY',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor City','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_CASH_DISBURSMENT','VENDOR_POSTAL_CODE',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Postal Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_CASH_DISBURSMENT','VENDOR_STATE',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor State','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_CASH_DISBURSMENT','VENDOR_SITE_CODE',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Site Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_CASH_DISBURSMENT','PAYMENT_TYPE',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Payment Type','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_CASH_DISBURSMENT','PAY_GROUP',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Pay Group','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_CASH_DISBURSMENT','SUPPLIER_NUMBER',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Supplier Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_CASH_DISBURSMENT','CLEARED_DATE',200,'','','','','','XXEIS_RS_ADMIN','DATE','','','Cleared Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_CASH_DISBURSMENT','AMOUNT',200,'','','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_CASH_DISBURSMENT','PAYMENT_METHOD_CODE',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Payment Method Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_CASH_DISBURSMENT','PAYMENT_PRIORITY',200,'','','','','','XXEIS_RS_ADMIN','NUMBER','','','Payment Priority','','','','US');
--Inserting Object Components for XXEIS_CASH_DISBURSMENT
--Inserting Object Component Joins for XXEIS_CASH_DISBURSMENT
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
--There are no LOV/LOV TL entries for the report BK REC AP CASH DISBURSEMENT
prompt Creating Report Data for BK REC AP CASH DISBURSEMENT
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Deleting Report data - BK REC AP CASH DISBURSEMENT
xxeis.eis_rsc_utility.delete_report_rows( 'BK REC AP CASH DISBURSEMENT' );
--Inserting Report - BK REC AP CASH DISBURSEMENT
xxeis.eis_rsc_ins.r( 200,'BK REC AP CASH DISBURSEMENT','','','','','','AO014801','XXEIS_CASH_DISBURSMENT','Y','','','AO014801','','N','HDS Standard Reports','','CSV,Pivot Excel,EXCEL,','N','','','','','','','','US','','','','');
--Inserting Report Columns - BK REC AP CASH DISBURSEMENT
xxeis.eis_rsc_ins.rc( 'BK REC AP CASH DISBURSEMENT',200,'ADDRESS_LINE1','Address Line1','','','','','','9','N','','','','','','','','AO014801','N','N','','XXEIS_CASH_DISBURSMENT','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'BK REC AP CASH DISBURSEMENT',200,'ADDRESS_LINE2','Address Line2','','','','','','10','N','','','','','','','','AO014801','N','N','','XXEIS_CASH_DISBURSMENT','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'BK REC AP CASH DISBURSEMENT',200,'ADDRESS_LINE3','Address Line3','','','','','','11','N','','','','','','','','AO014801','N','N','','XXEIS_CASH_DISBURSMENT','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'BK REC AP CASH DISBURSEMENT',200,'AMOUNT','Amount','','','','','','4','N','','','','','','','','AO014801','N','N','','XXEIS_CASH_DISBURSMENT','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'BK REC AP CASH DISBURSEMENT',200,'BANK_ACCOUNT_NAME','Bank Account Name','','','','','','1','N','','','','','','','','AO014801','N','N','','XXEIS_CASH_DISBURSMENT','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'BK REC AP CASH DISBURSEMENT',200,'CHECK_DATE','Check Date','','','','','','2','N','','','','','','','','AO014801','N','N','','XXEIS_CASH_DISBURSMENT','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'BK REC AP CASH DISBURSEMENT',200,'CHECK_NUMBER','Check Number','','','','','','3','N','','','','','','','','AO014801','N','N','','XXEIS_CASH_DISBURSMENT','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'BK REC AP CASH DISBURSEMENT',200,'PAYMENT_STATUS_DESCR','Payment Status Descr','','','','','','5','N','','','','','','','','AO014801','N','N','','XXEIS_CASH_DISBURSMENT','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'BK REC AP CASH DISBURSEMENT',200,'PAYMENT_TYPE','Payment Type','','','','','','16','N','','','','','','','','AO014801','N','N','','XXEIS_CASH_DISBURSMENT','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'BK REC AP CASH DISBURSEMENT',200,'PAY_GROUP','Pay Group','','','','','','17','N','','','','','','','','AO014801','N','N','','XXEIS_CASH_DISBURSMENT','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'BK REC AP CASH DISBURSEMENT',200,'STOP_PAYMENT_RECORDED_DATE','Stop Payment Recorded Date','','','','','','7','N','','','','','','','','AO014801','N','N','','XXEIS_CASH_DISBURSMENT','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'BK REC AP CASH DISBURSEMENT',200,'SUPPLIER_NUMBER','Supplier Number','','','','','','18','N','','','','','','','','AO014801','N','N','','XXEIS_CASH_DISBURSMENT','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'BK REC AP CASH DISBURSEMENT',200,'VENDOR_CITY','Vendor City','','','','','','12','N','','','','','','','','AO014801','N','N','','XXEIS_CASH_DISBURSMENT','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'BK REC AP CASH DISBURSEMENT',200,'VENDOR_NAME','Vendor Name','','','','','','8','N','','','','','','','','AO014801','N','N','','XXEIS_CASH_DISBURSMENT','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'BK REC AP CASH DISBURSEMENT',200,'VENDOR_POSTAL_CODE','Vendor Postal Code','','','','','','13','N','','','','','','','','AO014801','N','N','','XXEIS_CASH_DISBURSMENT','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'BK REC AP CASH DISBURSEMENT',200,'VENDOR_SITE_CODE','Vendor Site Code','','','','','','15','N','','','','','','','','AO014801','N','N','','XXEIS_CASH_DISBURSMENT','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'BK REC AP CASH DISBURSEMENT',200,'VENDOR_STATE','Vendor State','','','','','','14','N','','','','','','','','AO014801','N','N','','XXEIS_CASH_DISBURSMENT','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'BK REC AP CASH DISBURSEMENT',200,'VOID_DATE','Void Date','','','','','','6','N','','','','','','','','AO014801','N','N','','XXEIS_CASH_DISBURSMENT','','','GROUP_BY','US','');
--Inserting Report Parameters - BK REC AP CASH DISBURSEMENT
xxeis.eis_rsc_ins.rp( 'BK REC AP CASH DISBURSEMENT',200,'Check Date From','Check Date From','CHECK_DATE','IN','','01-JAN-2005','DATE','N','Y','1','Y','Y','CONSTANT','AO014801','Y','N','','Start Date','','XXEIS_CASH_DISBURSMENT','','','US','');
xxeis.eis_rsc_ins.rp( 'BK REC AP CASH DISBURSEMENT',200,'Check Date To','Check Date To','CHECK_DATE','IN','','','DATE','N','Y','2','Y','Y','CURRENT_DATE','AO014801','Y','N','','End Date','','XXEIS_CASH_DISBURSMENT','','','US','');
xxeis.eis_rsc_ins.rp( 'BK REC AP CASH DISBURSEMENT',200,'Void Date','Void Date','VOID_DATE','>','','','DATE','N','Y','3','Y','Y','CURRENT_DATE','AO014801','Y','N','','','','XXEIS_CASH_DISBURSMENT','','','US','');
xxeis.eis_rsc_ins.rp( 'BK REC AP CASH DISBURSEMENT',200,'Cleared Date','Cleared Date','CLEARED_DATE','>','','','DATE','N','Y','4','Y','Y','CURRENT_DATE','AO014801','Y','N','','','','XXEIS_CASH_DISBURSMENT','','','US','');
--Inserting Dependent Parameters - BK REC AP CASH DISBURSEMENT
--Inserting Report Conditions - BK REC AP CASH DISBURSEMENT
xxeis.eis_rsc_ins.rcnh( 'BK REC AP CASH DISBURSEMENT',200,'BANK_ACCOUNT_NAME = ''Suntrust Check Disbursements'' ','ADVANCED','','1#$#','Y','');
xxeis.eis_rsc_ins.rcnd( '','','BANK_ACCOUNT_NAME','','','','','','','XXEIS_CASH_DISBURSMENT','','','','','','EQUALS','Y','N','','''Suntrust Check Disbursements''','','','1',200,'BK REC AP CASH DISBURSEMENT','BANK_ACCOUNT_NAME = ''Suntrust Check Disbursements'' ');
xxeis.eis_rsc_ins.rcnh( 'BK REC AP CASH DISBURSEMENT',200,'Free Text ','FREE_TEXT','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','','Y','','','','','and    (((X4TV.void_date IS NULL OR X4TV.void_date > :Void Date)
AND    X4TV.CHECK_DATE BETWEEN :Check Date From AND :Check Date To
AND    (X4TV.CLEARED_DATE > :Cleared Date OR X4TV.CLEARED_DATE IS NULL)))','1',200,'BK REC AP CASH DISBURSEMENT','Free Text ');
xxeis.eis_rsc_ins.rcnh( 'BK REC AP CASH DISBURSEMENT',200,'X4TV.CHECK_DATE IN Check Date From','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CHECK_DATE','','Check Date From','','','','','XXEIS_CASH_DISBURSMENT','','','','','','IN','Y','Y','','','','','1',200,'BK REC AP CASH DISBURSEMENT','X4TV.CHECK_DATE IN Check Date From');
xxeis.eis_rsc_ins.rcnh( 'BK REC AP CASH DISBURSEMENT',200,'X4TV.CHECK_DATE IN Check Date To','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CHECK_DATE','','Check Date To','','','','','XXEIS_CASH_DISBURSMENT','','','','','','IN','Y','Y','','','','','1',200,'BK REC AP CASH DISBURSEMENT','X4TV.CHECK_DATE IN Check Date To');
xxeis.eis_rsc_ins.rcnh( 'BK REC AP CASH DISBURSEMENT',200,'X4TV.VOID_DATE > Void Date','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','VOID_DATE','','Void Date','','','','','XXEIS_CASH_DISBURSMENT','','','','','','GREATER_THAN','Y','Y','','','','','1',200,'BK REC AP CASH DISBURSEMENT','X4TV.VOID_DATE > Void Date');
xxeis.eis_rsc_ins.rcnh( 'BK REC AP CASH DISBURSEMENT',200,'X4TV.CLEARED_DATE > Cleared Date','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CLEARED_DATE','','Cleared Date','','','','','XXEIS_CASH_DISBURSMENT','','','','','','GREATER_THAN','Y','Y','','','','','1',200,'BK REC AP CASH DISBURSEMENT','X4TV.CLEARED_DATE > Cleared Date');
--Inserting Report Sorts - BK REC AP CASH DISBURSEMENT
--Inserting Report Triggers - BK REC AP CASH DISBURSEMENT
--inserting report templates - BK REC AP CASH DISBURSEMENT
--Inserting Report Portals - BK REC AP CASH DISBURSEMENT
--inserting report dashboards - BK REC AP CASH DISBURSEMENT
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'BK REC AP CASH DISBURSEMENT','200','XXEIS_CASH_DISBURSMENT','XXEIS_CASH_DISBURSMENT','N','');
--inserting report security - BK REC AP CASH DISBURSEMENT
xxeis.eis_rsc_ins.rsec( 'BK REC AP CASH DISBURSEMENT','20005','','XXWC_VIEW_ALL_EIS_REPORTS',200,'AO014801','','','');
--Inserting Report Pivots - BK REC AP CASH DISBURSEMENT
--Inserting Report   Version details- BK REC AP CASH DISBURSEMENT
xxeis.eis_rsc_ins.rv( 'BK REC AP CASH DISBURSEMENT','','BK REC AP CASH DISBURSEMENT','AB063501');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
