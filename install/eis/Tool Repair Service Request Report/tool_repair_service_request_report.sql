--Report Name            : Tool Repair Service Request Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View EIS_XXWC_OM_TOOLS_REPAIR_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_OM_TOOLS_REPAIR_V
Create or replace View XXEIS.EIS_XXWC_OM_TOOLS_REPAIR_V
 AS 
SELECT mp.organization_code organization_code,
    sr.incident_number request_number,
    sr.incident_date date_opened,
    item.description Item_description,
    dra.quantity qty,
    dra.repair_number repair_number,
    item.segment1 item,
    rtype.name repair_type,
    hca.account_number,
    NVL(hca.account_name,hzp.party_name) customer_name,
    citb.name request_type,
    ppf.full_name employee_name,
    crf.flow_status_meaning status,
    jrre.source_name owner,
    jrre1.source_name technician,
    hzsu.location customer_job_name,
    --Primary Keys
    sr.incident_id,
    hzp.party_id,
    hca.cust_account_id,
    hzsu.site_use_id,
    ppf.person_id,
    ppf.effective_start_date,
    ppf.effective_end_date,
    rtype.repair_type_id,
    item.inventory_item_id,
    item.organization_id,
    citb.incident_type_id,
    dra.repair_line_id,
    jrre.resource_id
    --descr#flexfield#start
    --descr#flexfield#end
    --gl#accountff#start
    --gl#accountff#end
  FROM CSD_REPAIRS dra,
    CS_INCIDENTS_ALL_B sr,
    CSD_REPAIR_TYPES_VL rtype,
    HZ_CUST_ACCOUNTS hca,
    HZ_PARTIES hzp,
    MTL_SYSTEM_ITEMS_B item,
    MTL_PARAMETERS mp,
    CSD_REPAIR_FLOW_STATUSES_V crf,
    per_all_people_f ppf,
    cs_incident_types_vl citb,
    JTF_RS_RESOURCE_EXTNS jrre,
    JTF_RS_RESOURCE_EXTNS jrre1,
    hz_cust_site_uses hzsu,
    hz_party_sites hps,
    hz_cust_acct_sites hzas
    --HZ_PARTY_SITES hzps
  WHERE dra.incident_id     = sr.incident_id
  AND dra.repair_type_id    = rtype.repair_type_id
  AND sr.customer_id        = hzp.party_id
 -- AND sr.incident_number    = 37524
  AND sr.account_id         = hca.cust_account_id (+)
  AND sr.ship_to_site_id    = hps.party_site_id(+) 
  AND hps.party_site_id     = hzas.party_site_id(+)
  AND hzas.cust_acct_site_id = hzsu.cust_acct_site_id(+)
  AND hzsu.primary_flag(+)   ='Y' 
  AND hzsu.site_use_code(+)  = 'SHIP_TO'
  AND dra.inventory_item_id = item.inventory_item_id
  AND item.organization_id  = dra.inventory_org_id
  AND mp.organization_id    = item.organization_id
  AND dra.flow_status_id    = crf.flow_status_id(+)
    --AND dra.status            = fnd2.lookup_code(+)
  AND sr.EMPLOYEE_ID = ppf.PERSON_ID(+)
  AND sr.incident_date BETWEEN ppf.effective_start_date(+) AND ppf.effective_end_Date(+)
    --AND fnd2.lookup_type(+)   = 'CSD_REPAIR_FLOW_STATUS'
  AND citb.incident_type_id = sr.incident_type_id
    -- and dra.repair_line_id     = 11007
  AND sr.incident_owner_id   = jrre.resource_id(+)
  AND DRA.RESOURCE_ID        = JRRE1.RESOURCE_ID(+)
/
set scan on define on
prompt Creating View Data for Tool Repair Service Request Report
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_OM_TOOLS_REPAIR_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_OM_TOOLS_REPAIR_V',660,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Xxwc Om Tools Repair V','EXOTRV','','');
--Delete View Columns for EIS_XXWC_OM_TOOLS_REPAIR_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_OM_TOOLS_REPAIR_V',660,FALSE);
--Inserting View Columns for EIS_XXWC_OM_TOOLS_REPAIR_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_TOOLS_REPAIR_V','CUSTOMER_JOB_NAME',660,'Customer Job Name','CUSTOMER_JOB_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Job Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_TOOLS_REPAIR_V','TECHNICIAN',660,'Technician','TECHNICIAN','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Technician','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_TOOLS_REPAIR_V','OWNER',660,'Owner','OWNER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Owner','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_TOOLS_REPAIR_V','STATUS',660,'Status','STATUS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Status','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_TOOLS_REPAIR_V','REQUEST_TYPE',660,'Request Type','REQUEST_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Request Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_TOOLS_REPAIR_V','CUSTOMER_NAME',660,'Customer Name','CUSTOMER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_TOOLS_REPAIR_V','ACCOUNT_NUMBER',660,'Account Number','ACCOUNT_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Account Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_TOOLS_REPAIR_V','REPAIR_NUMBER',660,'Repair Number','REPAIR_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Repair Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_TOOLS_REPAIR_V','ITEM_DESCRIPTION',660,'Item Description','ITEM_DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Item Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_TOOLS_REPAIR_V','DATE_OPENED',660,'Date Opened','DATE_OPENED','','','','XXEIS_RS_ADMIN','DATE','','','Date Opened','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_TOOLS_REPAIR_V','REQUEST_NUMBER',660,'Request Number','REQUEST_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Request Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_TOOLS_REPAIR_V','ORGANIZATION_CODE',660,'Organization Code','ORGANIZATION_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Organization Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_TOOLS_REPAIR_V','RESOURCE_ID',660,'Resource Id','RESOURCE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Resource Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_TOOLS_REPAIR_V','REPAIR_LINE_ID',660,'Repair Line Id','REPAIR_LINE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Repair Line Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_TOOLS_REPAIR_V','INCIDENT_TYPE_ID',660,'Incident Type Id','INCIDENT_TYPE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Incident Type Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_TOOLS_REPAIR_V','ORGANIZATION_ID',660,'Organization Id','ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_TOOLS_REPAIR_V','INVENTORY_ITEM_ID',660,'Inventory Item Id','INVENTORY_ITEM_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Inventory Item Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_TOOLS_REPAIR_V','REPAIR_TYPE_ID',660,'Repair Type Id','REPAIR_TYPE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Repair Type Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_TOOLS_REPAIR_V','EFFECTIVE_END_DATE',660,'Effective End Date','EFFECTIVE_END_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Effective End Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_TOOLS_REPAIR_V','EFFECTIVE_START_DATE',660,'Effective Start Date','EFFECTIVE_START_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Effective Start Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_TOOLS_REPAIR_V','PERSON_ID',660,'Person Id','PERSON_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Person Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_TOOLS_REPAIR_V','SITE_USE_ID',660,'Site Use Id','SITE_USE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Site Use Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_TOOLS_REPAIR_V','CUST_ACCOUNT_ID',660,'Cust Account Id','CUST_ACCOUNT_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Cust Account Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_TOOLS_REPAIR_V','PARTY_ID',660,'Party Id','PARTY_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Party Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_TOOLS_REPAIR_V','INCIDENT_ID',660,'Incident Id','INCIDENT_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Incident Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_TOOLS_REPAIR_V','EMPLOYEE_NAME',660,'Employee Name','EMPLOYEE_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Employee Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_TOOLS_REPAIR_V','REPAIR_TYPE',660,'Repair Type','REPAIR_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Repair Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_TOOLS_REPAIR_V','ITEM',660,'Item','ITEM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Item','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_TOOLS_REPAIR_V','QTY',660,'Qty','QTY','','','','XXEIS_RS_ADMIN','NUMBER','','','Qty','','','');
--Inserting View Components for EIS_XXWC_OM_TOOLS_REPAIR_V
--Inserting View Component Joins for EIS_XXWC_OM_TOOLS_REPAIR_V
END;
/
set scan on define on
prompt Creating Report LOV Data for Tool Repair Service Request Report
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Tool Repair Service Request Report
xxeis.eis_rs_ins.lov( 660,'SELECT DISTINCT source_name Owner
FROM JTF_RS_RESOURCE_EXTNS jrre,
  CS_INCIDENTS_ALL_B sr
WHERE sr.incident_owner_id = jrre.resource_id','','XXWC CSD OWNER','CSD Owners','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'SELECT DISTINCT source_name Technician
FROM JTF_RS_RESOURCE_EXTNS jrre,
      CSD_REPAIRS csd
WHERE csd.resource_id = jrre.resource_id','','XXWC CSD Technician','XXWC CSD Technician','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'SELECT flow_status_meaning Repir_Status
  FROM CSD_REPAIR_FLOW_STATUSES_V','','XXWC CSD STATUS','','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'SELECT DISTINCT NAME FROM CSD_REPAIR_TYPES_VL
WHERE  NAME NOT in (''Advance Exchange'',''Credit Only'',''Exchange'',''Loaner'',''Loaner, Repair and Return'',''Reburbishment'',''Repair and Return'',''Replacement'',''Standard'',''Third Party Repair and Return'',''Walk-In Repair'',''Walk-In Repair with Loaner'')
UNION  SELECT ''All'' from Dual','','XXWC Repair Type','This Lov Provides list of Repair types.','ANONYMOUS',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Tool Repair Service Request Report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Tool Repair Service Request Report
xxeis.eis_rs_utility.delete_report_rows( 'Tool Repair Service Request Report' );
--Inserting Report - Tool Repair Service Request Report
xxeis.eis_rs_ins.r( 660,'Tool Repair Service Request Report','','Tool Repair Service Request Report','','','','XXEIS_RS_ADMIN','EIS_XXWC_OM_TOOLS_REPAIR_V','Y','','','XXEIS_RS_ADMIN','','N','White Cap Reports','RTF,PDF,','CSV,HTML,Html Summary,XML,Pivot Excel,EXCEL,','');
--Inserting Report Columns - Tool Repair Service Request Report
xxeis.eis_rs_ins.rc( 'Tool Repair Service Request Report',660,'ACCOUNT_NUMBER','Customer Number','Account Number','','','default','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_TOOLS_REPAIR_V','','');
xxeis.eis_rs_ins.rc( 'Tool Repair Service Request Report',660,'CUSTOMER_JOB_NAME','Customer Job Name','Customer Job Name','','','default','','12','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_TOOLS_REPAIR_V','','');
xxeis.eis_rs_ins.rc( 'Tool Repair Service Request Report',660,'CUSTOMER_NAME','Customer Name','Customer Name','','','default','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_TOOLS_REPAIR_V','','');
xxeis.eis_rs_ins.rc( 'Tool Repair Service Request Report',660,'DATE_OPENED','Date Opened','Date Opened','','','default','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_TOOLS_REPAIR_V','','');
xxeis.eis_rs_ins.rc( 'Tool Repair Service Request Report',660,'ITEM_DESCRIPTION','Item Description','Item Description','','','default','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_TOOLS_REPAIR_V','','');
xxeis.eis_rs_ins.rc( 'Tool Repair Service Request Report',660,'OWNER','Owner','Owner','','','default','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_TOOLS_REPAIR_V','','');
xxeis.eis_rs_ins.rc( 'Tool Repair Service Request Report',660,'REPAIR_NUMBER','Repair Number','Repair Number','','','default','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_TOOLS_REPAIR_V','','');
xxeis.eis_rs_ins.rc( 'Tool Repair Service Request Report',660,'REQUEST_NUMBER','Request Num','Request Number','','','default','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_TOOLS_REPAIR_V','','');
xxeis.eis_rs_ins.rc( 'Tool Repair Service Request Report',660,'REQUEST_TYPE','Order Type','Request Type','','','default','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_TOOLS_REPAIR_V','','');
xxeis.eis_rs_ins.rc( 'Tool Repair Service Request Report',660,'STATUS','Status','Status','','','default','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_TOOLS_REPAIR_V','','');
xxeis.eis_rs_ins.rc( 'Tool Repair Service Request Report',660,'TECHNICIAN','Technician','Technician','','','default','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_TOOLS_REPAIR_V','','');
xxeis.eis_rs_ins.rc( 'Tool Repair Service Request Report',660,'REPAIR_TYPE','Repair Type','Repair Type','','','default','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_TOOLS_REPAIR_V','','');
--Inserting Report Parameters - Tool Repair Service Request Report
xxeis.eis_rs_ins.rp( 'Tool Repair Service Request Report',660,'Repair Type','','REPAIR_TYPE','IN','XXWC Repair Type','','VARCHAR2','Y','Y','1','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Tool Repair Service Request Report',660,'Owner','','OWNER','IN','XXWC CSD OWNER','','VARCHAR2','N','Y','2','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Tool Repair Service Request Report',660,'Technician','','TECHNICIAN','IN','XXWC CSD Technician','','VARCHAR2','N','Y','3','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Tool Repair Service Request Report',660,'Status','','STATUS','IN','XXWC CSD STATUS','','VARCHAR2','Y','Y','4','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Tool Repair Service Request Report',660,'Start Date','','DATE_OPENED','>=','','','DATE','N','Y','5','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','Start Date','');
xxeis.eis_rs_ins.rp( 'Tool Repair Service Request Report',660,'End Date','','DATE_OPENED','<=','','','DATE','N','Y','6','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','End Date','');
--Inserting Report Conditions - Tool Repair Service Request Report
xxeis.eis_rs_ins.rcn( 'Tool Repair Service Request Report',660,'DATE_OPENED','>=',':Start Date','','','Y','5','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Tool Repair Service Request Report',660,'DATE_OPENED','<=',':End Date','','','Y','6','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Tool Repair Service Request Report',660,'OWNER','IN',':Owner','','','Y','2','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Tool Repair Service Request Report',660,'TECHNICIAN','IN',':Technician','','','Y','3','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Tool Repair Service Request Report',660,'STATUS','IN',':Status','','','Y','4','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Tool Repair Service Request Report',660,'','','','','AND ( ''All'' IN (:Repair Type) OR (REPAIR_TYPE IN (:Repair Type)))
','Y','1','','XXEIS_RS_ADMIN');
--Inserting Report Sorts - Tool Repair Service Request Report
xxeis.eis_rs_ins.rs( 'Tool Repair Service Request Report',660,'REQUEST_NUMBER','ASC','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rs( 'Tool Repair Service Request Report',660,'CUSTOMER_NAME','ASC','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rs( 'Tool Repair Service Request Report',660,'REPAIR_NUMBER','ASC','XXEIS_RS_ADMIN','','');
--Inserting Report Triggers - Tool Repair Service Request Report
--Inserting Report Templates - Tool Repair Service Request Report
--Inserting Report Portals - Tool Repair Service Request Report
--Inserting Report Dashboards - Tool Repair Service Request Report
--Inserting Report Security - Tool Repair Service Request Report
xxeis.eis_rs_ins.rsec( 'Tool Repair Service Request Report','660','','51044',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Tool Repair Service Request Report','660','','51065',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Tool Repair Service Request Report','660','','50901',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Tool Repair Service Request Report','660','','51025',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Tool Repair Service Request Report','660','','50860',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Tool Repair Service Request Report','660','','50886',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Tool Repair Service Request Report','660','','50859',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Tool Repair Service Request Report','660','','50858',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Tool Repair Service Request Report','660','','50857',660,'XXEIS_RS_ADMIN','','');
--Inserting Report Pivots - Tool Repair Service Request Report
END;
/
set scan on define on
