grant select on  cs_incident_types_vl to xxeis;
grant select on  CSD_REPAIRS to xxeis;
grant select on  CS_INCIDENTS_ALL_B to xxeis;
grant select on  CSD_REPAIR_TYPES_VL to xxeis;
grant select on CSD_REPAIR_FLOW_STATUSES_V to xxeis;
create or replace synonym xxeis.cs_incident_types_vl for apps.cs_incident_types_vl;
create or replace synonym xxeis.CS_INCIDENTS_ALL_B for apps.CS_INCIDENTS_ALL_B;
create or replace synonym  xxeis.CSD_REPAIR_TYPES_VL for apps.CSD_REPAIR_TYPES_VL;
create or replace synonym xxeis.CSD_REPAIR_FLOW_STATUSES_V for apps.CSD_REPAIR_FLOW_STATUSES_V;

