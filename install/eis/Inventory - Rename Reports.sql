--Run this file in XXEIS user
/**************************************************************************************************************
  Module Name : Inventory
  REVISIONS   :
  VERSION 	DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.1     28-Sep-2016        	Siva   		 TMS#20160824-00056 --Changed the report name for 'Inventory - COS, Shipped Prior Period' report
																	and 'Inventory - Shipped, No COS' reports		
**************************************************************************************************************/
BEGIN
  XXEIS.EIS_RS_INSTALL_UNINSTALL_PKG.RENAME_REPORT('Inventory � COS, Shipped Prior Period','Inventory - COS, Shipped Prior Period', 401);
  XXEIS.EIS_RS_INSTALL_UNINSTALL_PKG.RENAME_REPORT('Inventory � Shipped, No COS','Inventory - Shipped, No COS', 401);
END;
/
