--Report Name            : WC - Payment Remittance
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
CREATE OR REPLACE VIEW APPS.XXEIS_586485_BIMSVE_V (SEGMENT1, VENDOR_NAME, CHECK_NUMBER, CHECK_DATE, INVOICE_NUM, INVOICE_DATE, INVOICE_AMOUNT, DISCOUNT_TAKEN, AMOUNT_PAID, DESCRIPTION, INVOICE_ID, STATUS_LOOKUP_CODE, DISCOUNT_DATE, DUE_DATE, NAME, CREATION_DATE)
AS
  SELECT ap.ap_suppliers.segment1,
    ap.ap_suppliers.vendor_name,
    ap.ap_checks_all.check_number ,
    ap.ap_checks_all.check_date,
    ap.ap_invoices_all.invoice_num,
    ap.ap_invoices_all.invoice_date ,
    ap.ap_invoices_all.invoice_amount,
    ap.ap_invoice_payments_all.discount_taken ,
    ap.ap_invoices_all.amount_paid,
    ap.ap_invoices_all.description,
    ap.ap_invoices_all.invoice_id,
    ap.ap_checks_all.status_lookup_code,
    APSA.DISCOUNT_DATE,
    APSA.DUE_DATE ,
    apt.name ,
    ap.ap_invoices_all.creation_date
  FROM ap.ap_invoices_all,
    ap.ap_invoice_payments_all,
    ap.ap_checks_all,
    ap.ap_suppliers,
    AP.AP_PAYMENT_SCHEDULES_ALL APSA,
    ap.ap_terms_tl apt
  WHERE ap.ap_invoices_all.invoice_id     = ap.ap_invoice_payments_all.invoice_id
  AND ap.ap_invoice_payments_all.check_id = ap.ap_checks_all.check_id
  AND ap.ap_invoices_all.vendor_id        = ap.ap_suppliers.vendor_id
  AND ap.ap_invoices_all.invoice_id       = APSA.INVOICE_ID
  AND apt.term_id                         = ap.ap_invoices_all.terms_id
  AND ap.ap_invoices_all.org_id           = 162 
/
prompt Creating Object Data XXEIS_586485_BIMSVE_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Inserting Object XXEIS_586485_BIMSVE_V
xxeis.eis_rsc_ins.v( 'XXEIS_586485_BIMSVE_V',200,'Paste SQL View for TEST - REMITTANCE AND TERMS','1.0','','','10012196','APPS','TEST - REMITTANCE AND TERMS View','X5BV','','','VIEW','US','','');
--Delete Object Columns for XXEIS_586485_BIMSVE_V
xxeis.eis_rsc_utility.delete_view_rows('XXEIS_586485_BIMSVE_V',200,FALSE);
--Inserting Object Columns for XXEIS_586485_BIMSVE_V
xxeis.eis_rsc_ins.vc( 'XXEIS_586485_BIMSVE_V','SEGMENT1',200,'','','','','','10012196','VARCHAR2','','','Segment1','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_586485_BIMSVE_V','VENDOR_NAME',200,'','','','','','10012196','VARCHAR2','','','Vendor Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_586485_BIMSVE_V','CHECK_NUMBER',200,'','','','','','10012196','NUMBER','','','Check Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_586485_BIMSVE_V','CHECK_DATE',200,'','','','','','10012196','DATE','','','Check Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_586485_BIMSVE_V','INVOICE_NUM',200,'','','','','','10012196','VARCHAR2','','','Invoice Num','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_586485_BIMSVE_V','INVOICE_DATE',200,'','','','','','10012196','DATE','','','Invoice Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_586485_BIMSVE_V','INVOICE_AMOUNT',200,'','','','~T~D~2','','10012196','NUMBER','','','Invoice Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_586485_BIMSVE_V','DISCOUNT_TAKEN',200,'','','','~T~D~2','','10012196','NUMBER','','','Discount Taken','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_586485_BIMSVE_V','AMOUNT_PAID',200,'','','','~T~D~2','','10012196','NUMBER','','','Amount Paid','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_586485_BIMSVE_V','DESCRIPTION',200,'','','','','','10012196','VARCHAR2','','','Description','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_586485_BIMSVE_V','INVOICE_ID',200,'','','','','','10012196','NUMBER','','','Invoice Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_586485_BIMSVE_V','STATUS_LOOKUP_CODE',200,'','','','','','10012196','VARCHAR2','','','Status Lookup Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_586485_BIMSVE_V','DISCOUNT_DATE',200,'','','','','','10012196','DATE','','','Discount Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_586485_BIMSVE_V','DUE_DATE',200,'','','','','','10012196','DATE','','','Due Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_586485_BIMSVE_V','NAME',200,'','','','','','10012196','VARCHAR2','','','Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_586485_BIMSVE_V','CREATION_DATE',200,'','','','','','10012196','DATE','','','Creation Date','','','','US');
--Inserting Object Components for XXEIS_586485_BIMSVE_V
--Inserting Object Component Joins for XXEIS_586485_BIMSVE_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
--There are no LOV/LOV TL entries for the report WC - Payment Remittance
prompt Creating Report Data for WC - Payment Remittance
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Deleting Report data - WC - Payment Remittance
xxeis.eis_rsc_utility.delete_report_rows( 'WC - Payment Remittance' );
--Inserting Report - WC - Payment Remittance
xxeis.eis_rsc_ins.r( 200,'WC - Payment Remittance','','','','','','10012196','XXEIS_586485_BIMSVE_V','Y','','SELECT ap.ap_suppliers.segment1, ap.ap_suppliers.vendor_name, ap.ap_checks_all.check_number
, ap.ap_checks_all.check_date, ap.ap_invoices_all.invoice_num, ap.ap_invoices_all.invoice_date
, ap.ap_invoices_all.invoice_amount, ap.ap_invoice_payments_all.discount_taken
, ap.ap_invoices_all.amount_paid, ap.ap_invoices_all.description, ap.ap_invoices_all.invoice_id, ap.ap_checks_all.status_lookup_code, APSA.DISCOUNT_DATE, APSA.DUE_DATE
, apt.name
, ap.ap_invoices_all.creation_date

FROM ap.ap_invoices_all, ap.ap_invoice_payments_all, ap.ap_checks_all, ap.ap_suppliers, AP.AP_PAYMENT_SCHEDULES_ALL APSA, ap.ap_terms_tl apt

WHERE ap.ap_invoices_all.invoice_id = ap.ap_invoice_payments_all.invoice_id
 AND ap.ap_invoice_payments_all.check_id = ap.ap_checks_all.check_id
 AND ap.ap_invoices_all.vendor_id = ap.ap_suppliers.vendor_id
 AND ap.ap_invoices_all.invoice_id = APSA.INVOICE_ID
 AND apt.term_id = ap.ap_invoices_all.terms_id
 AND ap.ap_invoices_all.org_id = 162
','10012196','','N','Payments','','CSV,Pivot Excel,EXCEL,','N','','','','','','','APPS','US','','','','');
--Inserting Report Columns - WC - Payment Remittance
xxeis.eis_rsc_ins.rc( 'WC - Payment Remittance',200,'STATUS_LOOKUP_CODE','Status Lookup Code','','','','default','','11','N','','','','','','','','10012196','N','N','','XXEIS_586485_BIMSVE_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Payment Remittance',200,'INVOICE_AMOUNT','Invoice Amount','','','~,~.~','default','','7','N','','','','','','','','10012196','N','N','','XXEIS_586485_BIMSVE_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - Payment Remittance',200,'DISCOUNT_DATE','Discount Date','','','','default','','12','N','','','','','','','','10012196','N','N','','XXEIS_586485_BIMSVE_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Payment Remittance',200,'DESCRIPTION','Description','','','','default','','10','N','','','','','','','','10012196','N','N','','XXEIS_586485_BIMSVE_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Payment Remittance',200,'SEGMENT1','Supplier Number','','','','default','','1','N','','','','','','','','10012196','N','N','','XXEIS_586485_BIMSVE_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Payment Remittance',200,'INVOICE_NUM','Invoice Num','','','','default','','5','N','','','','','','','','10012196','N','N','','XXEIS_586485_BIMSVE_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Payment Remittance',200,'DUE_DATE','Due Date','','','','default','','13','N','','','','','','','','10012196','N','N','','XXEIS_586485_BIMSVE_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Payment Remittance',200,'AMOUNT_PAID','Amount Paid','','','~,~.~','default','','9','N','','','','','','','','10012196','N','N','','XXEIS_586485_BIMSVE_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - Payment Remittance',200,'VENDOR_NAME','Vendor Name','','','','default','','2','N','','','','','','','','10012196','N','N','','XXEIS_586485_BIMSVE_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Payment Remittance',200,'INVOICE_DATE','Invoice Date','','','','default','','6','N','','','','','','','','10012196','N','N','','XXEIS_586485_BIMSVE_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Payment Remittance',200,'CHECK_DATE','Check Date','','','','default','','4','N','','','','','','','','10012196','N','N','','XXEIS_586485_BIMSVE_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Payment Remittance',200,'NAME','Terms Name','','','','default','','14','N','','','','','','','','10012196','N','N','','XXEIS_586485_BIMSVE_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Payment Remittance',200,'CHECK_NUMBER','Check Number','','','~T~D~0','default','','3','N','','','','','','','','10012196','N','N','','XXEIS_586485_BIMSVE_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - Payment Remittance',200,'INVOICE_ID','Invoice Id','','','~T~D~0','default','','15','N','','','','','','','','10012196','N','N','','XXEIS_586485_BIMSVE_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - Payment Remittance',200,'DISCOUNT_TAKEN','Discount Taken','','','~,~.~','default','','8','N','','','','','','','','10012196','N','N','','XXEIS_586485_BIMSVE_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - Payment Remittance',200,'CREATION_DATE','Creation Date','','','','','','16','','Y','','','','','','','10012196','N','N','','XXEIS_586485_BIMSVE_V','','','GROUP_BY','US','');
--Inserting Report Parameters - WC - Payment Remittance
xxeis.eis_rsc_ins.rp( 'WC - Payment Remittance',200,'Check Date From','','CHECK_DATE','>=','','','DATE','N','Y','1','','Y','CONSTANT','10012196','Y','N','','','','XXEIS_586485_BIMSVE_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC - Payment Remittance',200,'Supplier Number','','SEGMENT1','IN','','','VARCHAR2','N','Y','3','Y','Y','CONSTANT','10012196','Y','N','','','','XXEIS_586485_BIMSVE_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC - Payment Remittance',200,'Check Date To','','CHECK_DATE','<=','','','DATE','N','Y','2','','Y','CONSTANT','10012196','Y','N','','','','XXEIS_586485_BIMSVE_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC - Payment Remittance',200,'Check Number','','CHECK_NUMBER','IN','','','NUMBER','N','Y','4','Y','Y','','10012196','Y','N','','','','XXEIS_586485_BIMSVE_V','','','US','');
--Inserting Dependent Parameters - WC - Payment Remittance
--Inserting Report Conditions - WC - Payment Remittance
xxeis.eis_rsc_ins.rcnh( 'WC - Payment Remittance',200,'CHECK_DATE >= :Check Date From ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CHECK_DATE','','Check Date From','','','','','XXEIS_586485_BIMSVE_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',200,'WC - Payment Remittance','CHECK_DATE >= :Check Date From ');
xxeis.eis_rsc_ins.rcnh( 'WC - Payment Remittance',200,'CHECK_DATE <= :Check Date To ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CHECK_DATE','','Check Date To','','','','','XXEIS_586485_BIMSVE_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',200,'WC - Payment Remittance','CHECK_DATE <= :Check Date To ');
xxeis.eis_rsc_ins.rcnh( 'WC - Payment Remittance',200,'CHECK_NUMBER IN :Check Number ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CHECK_NUMBER','','Check Number','','','','','XXEIS_586485_BIMSVE_V','','','','','','IN','Y','Y','','','','','1',200,'WC - Payment Remittance','CHECK_NUMBER IN :Check Number ');
xxeis.eis_rsc_ins.rcnh( 'WC - Payment Remittance',200,'SEGMENT1 IN :Supplier Number ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','SEGMENT1','','Supplier Number','','','','','XXEIS_586485_BIMSVE_V','','','','','','IN','Y','Y','','','','','1',200,'WC - Payment Remittance','SEGMENT1 IN :Supplier Number ');
--Inserting Report Sorts - WC - Payment Remittance
--Inserting Report Triggers - WC - Payment Remittance
--inserting report templates - WC - Payment Remittance
--Inserting Report Portals - WC - Payment Remittance
--inserting report dashboards - WC - Payment Remittance
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'WC - Payment Remittance','200','XXEIS_586485_BIMSVE_V','XXEIS_586485_BIMSVE_V','N','');
--inserting report security - WC - Payment Remittance
xxeis.eis_rsc_ins.rsec( 'WC - Payment Remittance','200','','XXWC_PAY_VENDOR_MSTR_INQUIRY',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Payment Remittance','200','','XXWC_PAY_VENDOR_MSTR',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Payment Remittance','200','','XXWC_PAY_VENDOR_BANK_DETAILS',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Payment Remittance','200','','XXWC_PAY_MANAGER',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Payment Remittance','200','','XXWC_PAYABLES_INQUIRY',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Payment Remittance','200','','XXWC_PAY_DISBURSE',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Payment Remittance','200','','XXWC_PAY_W_CALENDAR',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Payment Remittance','200','','XXWC_PAY_NO_CALENDAR',200,'10012196','','','');
--Inserting Report Pivots - WC - Payment Remittance
--Inserting Report   Version details- WC - Payment Remittance
xxeis.eis_rsc_ins.rv( 'WC - Payment Remittance','','WC - Payment Remittance','AB063501');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
