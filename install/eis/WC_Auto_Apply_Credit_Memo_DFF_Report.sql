--TMS#20150123-00197
--Report Name            : XXWC Auto Apply Credit Memo DFF Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View EIS_AR_AUTO_APPLY_CM
set scan off define off
prompt Creating View XXEIS.EIS_AR_AUTO_APPLY_CM
Create or replace View XXEIS.EIS_AR_AUTO_APPLY_CM
 AS 
SELECT   cust.account_number
         ,cust.account_name
         ,DECODE(cust.attribute11,'Y','ON','N','OFF',NULL)
 auto_apply         ,prof.credit_hold
         ,NVL (cust.status,'A')
 account_status        ,pc.name profile_class
         ,coll.name collector
         ,cust.attribute4 source_code
         ,MAX((SELECT SUM(amount_due_remaining) 
           FROM ar_payment_schedules_all 
           WHERE customer_id = cust.cust_account_id
            AND status = 'OP'))Total_ar_balance
         ,SUM(amount_due_remaining) credit_memo_remaining_bal
        , COUNT(rcta.trx_number) credit_memo_transaction_count      
    FROM AR.hz_cust_accounts cust,
         AR.hz_customer_profiles prof,
         AR.hz_cust_profile_classes pc,    
         AR.ar_collectors coll,
         AR.ar_payment_schedules_all aps,
         AR.ra_customer_trx_all rcta
   WHERE cust.cust_account_id   =   prof.cust_account_id    
    AND prof.profile_class_id   = pc.profile_class_id
    AND prof.collector_id       = coll.collector_id(+)
    AND aps.customer_id = cust.cust_account_id
    AND rcta.sold_to_customer_id = cust.cust_account_id
    AND rcta.cust_trx_type_id = 2
    AND aps.customer_trx_id = rcta.customer_trx_id
    AND prof.site_use_id IS NULL
    AND aps.status = 'OP'
    GROUP BY cust.account_number
            ,cust.account_name
            ,DECODE(cust.attribute11,'Y','ON','N','OFF',NULL)
	    ,prof.credit_hold
            ,NVL (cust.status,'A')
            ,pc.name 
            ,coll.name
            ,cust.attribute4/
set scan on define on
prompt Creating View Data for XXWC Auto Apply Credit Memo DFF Report
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_AR_AUTO_APPLY_CM
xxeis.eis_rs_ins.v( 'EIS_AR_AUTO_APPLY_CM',222,'','','','','LA023190','XXEIS','Eis Ar Auto Apply Cm','EAAAC','','');
--Delete View Columns for EIS_AR_AUTO_APPLY_CM
xxeis.eis_rs_utility.delete_view_rows('EIS_AR_AUTO_APPLY_CM',222,FALSE);
--Inserting View Columns for EIS_AR_AUTO_APPLY_CM
xxeis.eis_rs_ins.vc( 'EIS_AR_AUTO_APPLY_CM','CREDIT_MEMO_TRANSACTION_COUNT',222,'Credit Memo Transaction Count','CREDIT_MEMO_TRANSACTION_COUNT','','','','LA023190','NUMBER','','','Credit Memo Transaction Count','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_AUTO_APPLY_CM','CREDIT_MEMO_REMAINING_BAL',222,'Credit Memo Remaining Bal','CREDIT_MEMO_REMAINING_BAL','','','','LA023190','NUMBER','','','Credit Memo Remaining Bal','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_AUTO_APPLY_CM','TOTAL_AR_BALANCE',222,'Total Ar Balance','TOTAL_AR_BALANCE','','','','LA023190','NUMBER','','','Total Ar Balance','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_AUTO_APPLY_CM','SOURCE_CODE',222,'Source Code','SOURCE_CODE','','','','LA023190','VARCHAR2','','','Source Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_AUTO_APPLY_CM','COLLECTOR',222,'Collector','COLLECTOR','','','','LA023190','VARCHAR2','','','Collector','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_AUTO_APPLY_CM','PROFILE_CLASS',222,'Profile Class','PROFILE_CLASS','','','','LA023190','VARCHAR2','','','Profile Class','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_AUTO_APPLY_CM','ACCOUNT_STATUS',222,'Account Status','ACCOUNT_STATUS','','','','LA023190','VARCHAR2','','','Account Status','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_AUTO_APPLY_CM','CREDIT_HOLD',222,'Credit Hold','CREDIT_HOLD','','','','LA023190','VARCHAR2','','','Credit Hold','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_AUTO_APPLY_CM','AUTO_APPLY',222,'Auto Apply','AUTO_APPLY','','','','LA023190','VARCHAR2','','','Auto Apply','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_AUTO_APPLY_CM','CUSTOMER_NAME',222,'Customer Name','CUSTOMER_NAME','','','','LA023190','VARCHAR2','','','Customer Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_AUTO_APPLY_CM','CUSTOMER_NUMBER',222,'Customer Number','CUSTOMER_NUMBER','','','','LA023190','VARCHAR2','','','Customer Number','','','');
--Inserting View Components for EIS_AR_AUTO_APPLY_CM
--Inserting View Component Joins for EIS_AR_AUTO_APPLY_CM
END;
/
set scan on define on
prompt Creating Report LOV Data for XXWC Auto Apply Credit Memo DFF Report
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - XXWC Auto Apply Credit Memo DFF Report
xxeis.eis_rs_ins.lov( 222,'select account_number from hz_cust_accounts','null','Customer Number','Displays List of Values for Customer Number','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 222,'select name from ar_collectors','null','Collector','Displays list of values for Collector','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 222,'select distinct name from hz_cust_profile_classes','null','PROFILE CLASS','This LOV lists all the profile classes of the customers','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 222,'select account_name,account_number from hz_cust_accounts','','AR Customer Name LOV','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 222,'select  customer_status.meaning Status
from  fnd_lookup_values_vl customer_status
where customer_status.lookup_type= ''ACCOUNT_STATUS''
 and customer_status.view_application_id=222','','XXWC Customer Account Status','','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( '','select distinct attribute4 Source_Code from hz_cust_accounts where attribute4 not in ''-''','','XXWC AR Source Code','To display Source code','ANONYMOUS',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for XXWC Auto Apply Credit Memo DFF Report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - XXWC Auto Apply Credit Memo DFF Report
xxeis.eis_rs_utility.delete_report_rows( 'XXWC Auto Apply Credit Memo DFF Report' );
--Inserting Report - XXWC Auto Apply Credit Memo DFF Report
xxeis.eis_rs_ins.r( 222,'XXWC Auto Apply Credit Memo DFF Report','','','','','','LA023190','EIS_AR_AUTO_APPLY_CM','Y','','','LA023190','','N','White Cap Reports','','CSV,EXCEL,','');
--Inserting Report Columns - XXWC Auto Apply Credit Memo DFF Report
xxeis.eis_rs_ins.rc( 'XXWC Auto Apply Credit Memo DFF Report',222,'ACCOUNT_STATUS','Account Status','Account Status','','','default','','5','N','','','','','','','','LA023190','N','N','','EIS_AR_AUTO_APPLY_CM','','');
xxeis.eis_rs_ins.rc( 'XXWC Auto Apply Credit Memo DFF Report',222,'AUTO_APPLY','Auto Apply DFF','Auto Apply','','','default','','3','N','','','','','','','','LA023190','N','N','','EIS_AR_AUTO_APPLY_CM','','');
xxeis.eis_rs_ins.rc( 'XXWC Auto Apply Credit Memo DFF Report',222,'COLLECTOR','Collector','Collector','','','default','','7','N','','','','','','','','LA023190','N','N','','EIS_AR_AUTO_APPLY_CM','','');
xxeis.eis_rs_ins.rc( 'XXWC Auto Apply Credit Memo DFF Report',222,'CREDIT_HOLD','Credit Hold','Credit Hold','','','default','','4','N','','','','','','','','LA023190','N','N','','EIS_AR_AUTO_APPLY_CM','','');
xxeis.eis_rs_ins.rc( 'XXWC Auto Apply Credit Memo DFF Report',222,'CREDIT_MEMO_REMAINING_BAL','Credit Memo Remaining Bal','Credit Memo Remaining Bal','','$~,~.~2','default','','10','N','','','','','','','','LA023190','N','N','','EIS_AR_AUTO_APPLY_CM','','');
xxeis.eis_rs_ins.rc( 'XXWC Auto Apply Credit Memo DFF Report',222,'CREDIT_MEMO_TRANSACTION_COUNT','Credit Memo Transaction Count','Credit Memo Transaction Count','','~,~.~','default','','11','N','','','','','','','','LA023190','N','N','','EIS_AR_AUTO_APPLY_CM','','');
xxeis.eis_rs_ins.rc( 'XXWC Auto Apply Credit Memo DFF Report',222,'CUSTOMER_NAME','Customer Name','Customer Name','','','default','','2','N','','','','','','','','LA023190','N','N','','EIS_AR_AUTO_APPLY_CM','','');
xxeis.eis_rs_ins.rc( 'XXWC Auto Apply Credit Memo DFF Report',222,'CUSTOMER_NUMBER','Customer Number','Customer Number','','','default','','1','N','','','','','','','','LA023190','N','N','','EIS_AR_AUTO_APPLY_CM','','');
xxeis.eis_rs_ins.rc( 'XXWC Auto Apply Credit Memo DFF Report',222,'PROFILE_CLASS','Profile Class','Profile Class','','','default','','6','N','','','','','','','','LA023190','N','N','','EIS_AR_AUTO_APPLY_CM','','');
xxeis.eis_rs_ins.rc( 'XXWC Auto Apply Credit Memo DFF Report',222,'SOURCE_CODE','Source Code','Source Code','','','default','','8','N','','','','','','','','LA023190','N','N','','EIS_AR_AUTO_APPLY_CM','','');
xxeis.eis_rs_ins.rc( 'XXWC Auto Apply Credit Memo DFF Report',222,'TOTAL_AR_BALANCE','Total AR Balance','Total Ar Balance','','$~,~.~2','default','','9','N','','','','','','','','LA023190','N','N','','EIS_AR_AUTO_APPLY_CM','','');
--Inserting Report Parameters - XXWC Auto Apply Credit Memo DFF Report
xxeis.eis_rs_ins.rp( 'XXWC Auto Apply Credit Memo DFF Report',222,'Account Status','Account Status','ACCOUNT_STATUS','IN','XXWC Customer Account Status','','VARCHAR2','N','Y','6','','Y','CONSTANT','LA023190','Y','N','','','');
xxeis.eis_rs_ins.rp( 'XXWC Auto Apply Credit Memo DFF Report',222,'Auto Apply','Auto Apply','AUTO_APPLY','IN','','OFF','VARCHAR2','N','Y','4','','Y','CONSTANT','LA023190','Y','N','','','');
xxeis.eis_rs_ins.rp( 'XXWC Auto Apply Credit Memo DFF Report',222,'Collector','Collector','COLLECTOR','IN','Collector','','VARCHAR2','N','Y','3','','Y','CONSTANT','LA023190','Y','N','','','');
xxeis.eis_rs_ins.rp( 'XXWC Auto Apply Credit Memo DFF Report',222,'Customer Name','Customer Name','CUSTOMER_NAME','IN','AR Customer Name LOV','','VARCHAR2','N','Y','2','','Y','CONSTANT','LA023190','Y','N','','','');
xxeis.eis_rs_ins.rp( 'XXWC Auto Apply Credit Memo DFF Report',222,'Customer Number','Customer Number','CUSTOMER_NUMBER','IN','Customer Number','','VARCHAR2','N','Y','1','','Y','CONSTANT','LA023190','Y','N','','','');
xxeis.eis_rs_ins.rp( 'XXWC Auto Apply Credit Memo DFF Report',222,'Profile Class','Profile Class','PROFILE_CLASS','IN','PROFILE CLASS','','VARCHAR2','N','Y','5','','Y','CONSTANT','LA023190','Y','N','','','');
xxeis.eis_rs_ins.rp( 'XXWC Auto Apply Credit Memo DFF Report',222,'Source Code','Source Code','SOURCE_CODE','IN','XXWC AR Source Code','','VARCHAR2','N','Y','7','','Y','CONSTANT','LA023190','Y','N','','','');
--Inserting Report Conditions - XXWC Auto Apply Credit Memo DFF Report
xxeis.eis_rs_ins.rcn( 'XXWC Auto Apply Credit Memo DFF Report',222,'ACCOUNT_STATUS','IN',':Account Status','','','Y','6','Y','LA023190');
xxeis.eis_rs_ins.rcn( 'XXWC Auto Apply Credit Memo DFF Report',222,'AUTO_APPLY','IN',':Auto Apply','','','Y','4','Y','LA023190');
xxeis.eis_rs_ins.rcn( 'XXWC Auto Apply Credit Memo DFF Report',222,'COLLECTOR','IN',':Collector','','','Y','3','Y','LA023190');
xxeis.eis_rs_ins.rcn( 'XXWC Auto Apply Credit Memo DFF Report',222,'CUSTOMER_NAME','IN',':Customer Name','','','Y','2','Y','LA023190');
xxeis.eis_rs_ins.rcn( 'XXWC Auto Apply Credit Memo DFF Report',222,'CUSTOMER_NUMBER','IN',':Customer Number','','','Y','1','Y','LA023190');
xxeis.eis_rs_ins.rcn( 'XXWC Auto Apply Credit Memo DFF Report',222,'PROFILE_CLASS','IN',':Profile Class','','','Y','5','Y','LA023190');
xxeis.eis_rs_ins.rcn( 'XXWC Auto Apply Credit Memo DFF Report',222,'SOURCE_CODE','IN',':Source Code','','','Y','7','Y','LA023190');
--Inserting Report Sorts - XXWC Auto Apply Credit Memo DFF Report
--Inserting Report Triggers - XXWC Auto Apply Credit Memo DFF Report
--Inserting Report Templates - XXWC Auto Apply Credit Memo DFF Report
--Inserting Report Portals - XXWC Auto Apply Credit Memo DFF Report
--Inserting Report Dashboards - XXWC Auto Apply Credit Memo DFF Report
--Inserting Report Security - XXWC Auto Apply Credit Memo DFF Report
--Inserting Report Pivots - XXWC Auto Apply Credit Memo DFF Report
END;
/
set scan on define on
