--Report Name            : HDS-Manual Entries
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data EIS_GL_JOURNAL_DETAILS_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(101);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_GL_JOURNAL_DETAILS_V
xxeis.eis_rsc_ins.v( 'EIS_GL_JOURNAL_DETAILS_V',101,'This view shows details of the general ledger journal detail information.','','','','XXEIS_RS_ADMIN','XXEIS','EIS GL Journal Details','EGJDV','','','VIEW','US','Y','','');
--Delete Object Columns for EIS_GL_JOURNAL_DETAILS_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_GL_JOURNAL_DETAILS_V',101,FALSE);
--Inserting Object Columns for EIS_GL_JOURNAL_DETAILS_V
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JE_DESCRIPTION',101,'Journal entry description','JE_DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_HEADERS','DESCRIPTION','Je Description','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JE_CURRENCY',101,'Currency','JE_CURRENCY','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_HEADERS','CURRENCY_CODE','Je Currency','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JE_STATUS',101,'Journal entry header status lookup code','JE_STATUS','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_HEADERS','STATUS','Je Status','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JE_TYPE',101,'Balance type (Actual, Budget, or Encumbrance)','JE_TYPE','','','GL Journal Entry Types LOV','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_HEADERS','ACTUAL_FLAG','Je Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','BATCH_NAME',101,'Name of journal entry batch','BATCH_NAME','','','GL Batches LOV','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_BATCHES','NAME','Batch Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JE_POSTED_DATE',101,'Date journal entry header was posted','JE_POSTED_DATE','','','','XXEIS_RS_ADMIN','DATE','GL_JE_HEADERS','POSTED_DATE','Je Posted Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JE_CREATION_DATE',101,'Standard Who column','JE_CREATION_DATE','','','','XXEIS_RS_ADMIN','DATE','GL_JE_HEADERS','CREATION_DATE','Je Creation Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JE_CREATED_BY',101,'Standard Who column','JE_CREATED_BY','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_HEADERS','CREATED_BY','Je Created By','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JE_BALANCED',101,'Balanced journal entry flag','JE_BALANCED','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_HEADERS','BALANCED_JE_FLAG','Je Balanced','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','ACCRUAL_REVERSAL',101,'Reversed journal entry flag','ACCRUAL_REVERSAL','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_HEADERS','ACCRUAL_REV_FLAG','Accrual Reversal','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JE_MULTI_BAL_SEG',101,'Multiple balancing segment flag','JE_MULTI_BAL_SEG','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_HEADERS','MULTI_BAL_SEG_FLAG','Je Multi Bal Seg','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','ACCRUAL_REV_EFFECTIVE_DATE',101,'Reversed journal entry effective date','ACCRUAL_REV_EFFECTIVE_DATE','','','','XXEIS_RS_ADMIN','DATE','GL_JE_HEADERS','ACCRUAL_REV_EFFECTIVE_DATE','Accrual Rev Effective Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','ACCRUAL_REV_PERIOD_NAME',101,'Reversed journal entry reversal period','ACCRUAL_REV_PERIOD_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_HEADERS','ACCRUAL_REV_PERIOD_NAME','Accrual Rev Period Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','ACCRUAL_REV_STATUS',101,'Reversed journal entry status','ACCRUAL_REV_STATUS','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_HEADERS','ACCRUAL_REV_STATUS','Accrual Rev Status','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','ACCRUAL_REV_CHANGE_SIGN',101,'Type of reversal (Change Sign or Switch Dr/Cr)','ACCRUAL_REV_CHANGE_SIGN','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_HEADERS','ACCRUAL_REV_CHANGE_SIGN_FLAG','Accrual Rev Change Sign','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JE_CURR_CONV_RATE',101,'Currency exchange rate','JE_CURR_CONV_RATE','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','GL_JE_HEADERS','CURRENCY_CONVERSION_RATE','Je Curr Conv Rate','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JE_CURR_CONV_TYPE',101,'Type of currency exchange rate','JE_CURR_CONV_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_HEADERS','CURRENCY_CONVERSION_TYPE','Je Curr Conv Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JE_CURR_CONV_DATE',101,'Currency conversion date','JE_CURR_CONV_DATE','','','','XXEIS_RS_ADMIN','DATE','GL_JE_HEADERS','CURRENCY_CONVERSION_DATE','Je Curr Conv Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JE_ACCOUNTED_DR',101,'Journal entry line debit amount in base currency','JE_ACCOUNTED_DR','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','GL_JE_LINES','ACCOUNTED_DR','Je Accounted Dr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JE_ACCOUNTED_CR',101,'Journal entry line credit amount in base currency','JE_ACCOUNTED_CR','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','GL_JE_LINES','ACCOUNTED_CR','Je Accounted Cr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JE_LINE_DESCRIPTION',101,'Journal entry line description','JE_LINE_DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_LINES','DESCRIPTION','Je Line Description','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GL_ACCOUNT_STRING',101,'GL Account String','GL_ACCOUNT_STRING','','','GL Accounts LOV','XXEIS_RS_ADMIN','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','Gl Account String','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','BUDGET_NAME',101,'Budget Name','BUDGET_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_BUDGET_VERSIONS','BUDGET_NAME','Budget Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','BUDGET_TYPE',101,'Budget type(only STANDARD is used)','BUDGET_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_BUDGET_VERSIONS','BUDGET_TYPE','Budget Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','BUDGET_STATUS',101,'Version status lookup code','BUDGET_STATUS','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_BUDGET_VERSIONS','STATUS','Budget Status','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','LEDGER_NAME',101,'Ledger name','LEDGER_NAME~LEDGER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_LEDGERS','NAME','Ledger Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JE_NAME',101,'Journal entry header name','JE_NAME','','','GL Journal Names LOV','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_HEADERS','NAME','Je Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','BUDGET_DESCRIPTION',101,'Budget version description','BUDGET_DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_BUDGET_VERSIONS','DESCRIPTION','Budget Description','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','ENCUMBRANCE_TYPE',101,'Encumbrance type name','ENCUMBRANCE_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_ENCUMBRANCE_TYPES','ENCUMBRANCE_TYPE','Encumbrance Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JE_EXTERNAL_REF',101,'Extra reference column','JE_EXTERNAL_REF','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_HEADERS','EXTERNAL_REFERENCE','Je External Ref','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JE_LINE_NUM',101,'Journal entry line number','JE_LINE_NUM','','','','XXEIS_RS_ADMIN','NUMBER','GL_JE_LINES','JE_LINE_NUM','Je Line Num','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JE_EFFECTIVE_DATE',101,'Journal entry line effective date','JE_EFFECTIVE_DATE','','','','XXEIS_RS_ADMIN','DATE','GL_JE_LINES','EFFECTIVE_DATE','Je Effective Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JE_ENTERED_DR',101,'Journal entry line debit amount in entered currency','JE_ENTERED_DR','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','GL_JE_LINES','ENTERED_DR','Je Entered Dr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JE_ENTERED_CR',101,'Journal entry line credit amount in entered currency','JE_ENTERED_CR','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','GL_JE_LINES','ENTERED_CR','Je Entered Cr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JE_BATCH_ID',101,'Journal entry batch defining column','JE_BATCH_ID','','','','XXEIS_RS_ADMIN','NUMBER','GL_JE_BATCHES','JE_BATCH_ID','Je Batch Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','BATCH_DESCRIPTION',101,'Journal entry batch description','BATCH_DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_BATCHES','DESCRIPTION','Batch Description','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JE_SOURCE',101,'Journal entry source user defined name','JE_SOURCE','','','GL Journal Sources LOV','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_SOURCES_TL','USER_JE_SOURCE_NAME','Je Source','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JE_CATEGORY',101,'JE Category','JE_CATEGORY','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_CATEGORIES','USER_JE_CATEGORY_NAME','Je Category','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JE_PERIOD',101,'Accounting period','JE_PERIOD','','','GL Periods LOV','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_HEADERS','PERIOD_NAME','Je Period','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','ACCRUAL_REV_JE_HEADER_ID',101,'Reversed journal entry defining column','ACCRUAL_REV_JE_HEADER_ID','','','','XXEIS_RS_ADMIN','NUMBER','GL_JE_HEADERS','ACCRUAL_REV_JE_HEADER_ID','Accrual Rev Je Header Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','REVERSED_JE_HEADER_ID',101,'Defining column of the journal entry that is reversed by this journal entry','REVERSED_JE_HEADER_ID','','','','XXEIS_RS_ADMIN','NUMBER','GL_JE_HEADERS','REVERSED_JE_HEADER_ID','Reversed Je Header Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','CODE_COMBINATION_ID',101,'Key flexfield combination defining column','CODE_COMBINATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','GL_CODE_COMBINATIONS','CODE_COMBINATION_ID','Code Combination Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','ENCUMBRANCE_TYPE_ID',101,'Encumbrance type defining column','ENCUMBRANCE_TYPE_ID~ENCUMBRANCE_TYPE_ID','','','','XXEIS_RS_ADMIN','NUMBER','GL_ENCUMBRANCE_TYPES','ENCUMBRANCE_TYPE_ID','Encumbrance Type Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JE_CATEGORY_NAME',101,'Journal entry category user defined name','JE_CATEGORY_NAME~JE_CATEGORY_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_CATEGORIES_TL','USER_JE_CATEGORY_NAME','Je Category Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JE_SOURCE_NAME',101,'Journal entry source user defined name','JE_SOURCE_NAME~JE_SOURCE_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_SOURCES_TL','USER_JE_SOURCE_NAME','Je Source Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','LEDGER_ID',101,'Ledger defining column','LEDGER_ID~LEDGER_ID','','','','XXEIS_RS_ADMIN','NUMBER','GL_LEDGERS','LEDGER_ID','Ledger Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','BUDGET_VERSION_ID',101,'Budget version defining column','BUDGET_VERSION_ID','','','','XXEIS_RS_ADMIN','NUMBER','GL_BUDGET_VERSIONS','BUDGET_VERSION_ID','Budget Version Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JE_HEADER_ID',101,'Journal entry header defining column','JE_HEADER_ID','','','','XXEIS_RS_ADMIN','NUMBER','GL_JE_HEADERS','JE_HEADER_ID','Je Header Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','ACCOUNTED_NET',101,'Accounted Net','ACCOUNTED_NET','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','CALCULATION COLUMN','CALCULATION COLUMN','Accounted Net','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','ENTERED_NET',101,'Entered Net','ENTERED_NET','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','CALCULATION COLUMN','CALCULATION COLUMN','Entered Net','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GCC#50328#PRODUCT',101,'Accounting Flexfield (KFF): Segment ''Product'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#PRODUCT','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT1','GCC#Product','50328','1014547','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GCC#50328#PRODUCT#DESCR',101,'Accounting Flexfield (KFF) : Segment ''Product'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#PRODUCT#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT1','GCC#Product Descr','50328','1014547','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GCC#50328#LOCATION',101,'Accounting Flexfield (KFF): Segment ''Location'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT2','GCC#Location','50328','1014548','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GCC#50328#LOCATION#DESCR',101,'Accounting Flexfield (KFF) : Segment ''Location'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#LOCATION#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT2','GCC#Location Descr','50328','1014548','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GCC#50328#COST_CENTER',101,'Accounting Flexfield (KFF): Segment ''Cost Center'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#COST_CENTER','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT3','GCC#Cost Center','50328','1014549','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GCC#50328#COST_CENTER#DESCR',101,'Accounting Flexfield (KFF) : Segment ''Cost Center'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#COST_CENTER#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT3','GCC#Cost Center Descr','50328','1014549','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GCC#50328#ACCOUNT',101,'Accounting Flexfield (KFF): Segment ''Account'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#ACCOUNT','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT4','GCC#Account','50328','1014550','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GCC#50328#ACCOUNT#DESCR',101,'Accounting Flexfield (KFF) : Segment ''Account'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#ACCOUNT#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT4','GCC#Account Descr','50328','1014550','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GCC#50328#PROJECT_CODE',101,'Accounting Flexfield (KFF): Segment ''Project Code'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#PROJECT_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT5','GCC#Project Code','50328','1014551','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GCC#50328#PROJECT_CODE#DESCR',101,'Accounting Flexfield (KFF) : Segment ''Project Code'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#PROJECT_CODE#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT5','GCC#Project Code Descr','50328','1014551','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GCC#50328#FURTURE_USE',101,'Accounting Flexfield : Segment ''Furture Use'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#FURTURE_USE','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT6','GCC#Furture Use','50328','1014552','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GCC#50328#FURTURE_USE#DESCR',101,'Accounting Flexfield : Segment ''Furture Use'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#FURTURE_USE#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT6','GCC#Furture Use Descr','50328','1014552','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GCC#50328#FUTURE_USE_2',101,'Accounting Flexfield (KFF): Segment ''Future Use 2'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#FUTURE_USE_2','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT7','GCC#Future Use 2','50328','1014948','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GCC#50328#FUTURE_USE_2#DESCR',101,'Accounting Flexfield (KFF) : Segment ''Future Use 2'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#FUTURE_USE_2#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT7','GCC#Future Use 2 Descr','50328','1014948','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GCC#50368#PRODUCT',101,'Accounting Flexfield (KFF): Segment ''Product'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GCC#50368#PRODUCT','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT1','GCC#Product','50368','1014596','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GCC#50368#PRODUCT#DESCR',101,'Accounting Flexfield (KFF) : Segment ''Product'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GCC#50368#PRODUCT#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT1','GCC#Product Descr','50368','1014596','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GCC#50368#DIVISION',101,'Accounting Flexfield (KFF): Segment ''Division'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GCC#50368#DIVISION','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT2','GCC#Division','50368','1014597','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GCC#50368#DIVISION#DESCR',101,'Accounting Flexfield (KFF) : Segment ''Division'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GCC#50368#DIVISION#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT2','GCC#Division Descr','50368','1014597','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GCC#50368#DEPARTMENT',101,'Accounting Flexfield (KFF): Segment ''Department'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GCC#50368#DEPARTMENT','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT3','GCC#Department','50368','1014598','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GCC#50368#DEPARTMENT#DESCR',101,'Accounting Flexfield (KFF) : Segment ''Department'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GCC#50368#DEPARTMENT#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT3','GCC#Department Descr','50368','1014598','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GCC#50368#ACCOUNT',101,'Accounting Flexfield (KFF): Segment ''Account'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GCC#50368#ACCOUNT','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT4','GCC#Account','50368','1014599','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GCC#50368#ACCOUNT#DESCR',101,'Accounting Flexfield (KFF) : Segment ''Account'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GCC#50368#ACCOUNT#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT4','GCC#Account Descr','50368','1014599','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GCC#50368#SUBACCOUNT',101,'Accounting Flexfield (KFF): Segment ''SubAccount'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GCC#50368#SUBACCOUNT','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT5','GCC#SubAccount','50368','1014600','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GCC#50368#SUBACCOUNT#DESCR',101,'Accounting Flexfield (KFF) : Segment ''SubAccount'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GCC#50368#SUBACCOUNT#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT5','GCC#SubAccount Descr','50368','1014600','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GCC#50368#FUTURE_USE',101,'Accounting Flexfield (KFF): Segment ''Future Use'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GCC#50368#FUTURE_USE','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT6','GCC#Future Use','50368','1014601','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GCC#50368#FUTURE_USE#DESCR',101,'Accounting Flexfield (KFF) : Segment ''Future Use'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GCC#50368#FUTURE_USE#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT6','GCC#Future Use Descr','50368','1014601','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','POSTED_BY',101,'Posted By','POSTED_BY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Posted By','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','ACCOUNTED_SUM',101,'Accounted Sum','ACCOUNTED_SUM','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Accounted Sum','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','ENTERED_SUM',101,'Entered Sum','ENTERED_SUM','','','','XXEIS_RS_ADMIN','NUMBER','','','Entered Sum','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GL#SEGMENT1',101,'Gl#Segment1','GL#SEGMENT1','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gl#Segment1','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GL#SEGMENT2',101,'Gl#Segment2','GL#SEGMENT2','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gl#Segment2','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GL#SEGMENT3',101,'Gl#Segment3','GL#SEGMENT3','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gl#Segment3','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GL#SEGMENT4',101,'Gl#Segment4','GL#SEGMENT4','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gl#Segment4','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GL#SEGMENT5',101,'Gl#Segment5','GL#SEGMENT5','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gl#Segment5','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GL#SEGMENT6',101,'Gl#Segment6','GL#SEGMENT6','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gl#Segment6','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GL#SEGMENT7',101,'Gl#Segment7','GL#SEGMENT7','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gl#Segment7','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GL#SEGMENT8',101,'Gl#Segment8','GL#SEGMENT8','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gl#Segment8','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GLE_ENCUMBRANCE_TYPE_ID',101,'Gle Encumbrance Type Id','GLE_ENCUMBRANCE_TYPE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Gle Encumbrance Type Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GL_ACCOUNT_TYPE',101,'Gl Account Type','GL_ACCOUNT_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gl Account Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GL_SL_LINK_ID',101,'Gl Sl Link Id','GL_SL_LINK_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Gl Sl Link Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GL_SL_LINK_TABLE',101,'Gl Sl Link Table','GL_SL_LINK_TABLE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gl Sl Link Table','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JEC_JE_CATEGORY_NAME',101,'Jec Je Category Name','JEC_JE_CATEGORY_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Jec Je Category Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JES_JE_SOURCE_NAME',101,'Jes Je Source Name','JES_JE_SOURCE_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Jes Je Source Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JL_JE_HEADER_ID',101,'Jl Je Header Id','JL_JE_HEADER_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Jl Je Header Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','PERIOD_END_DATE',101,'Period End Date','PERIOD_END_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Period End Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','PERIOD_SET_NAME',101,'Period Set Name','PERIOD_SET_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Period Set Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','PERIOD_START_DATE',101,'Period Start Date','PERIOD_START_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Period Start Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','PERIOD_TYPE',101,'Period Type','PERIOD_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Period Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','REFERENCE_1',101,'Reference 1','REFERENCE_1','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Reference 1','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','REFERENCE_10',101,'Reference 10','REFERENCE_10','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Reference 10','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','REFERENCE_2',101,'Reference 2','REFERENCE_2','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Reference 2','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','REFERENCE_3',101,'Reference 3','REFERENCE_3','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Reference 3','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','REFERENCE_4',101,'Reference 4','REFERENCE_4','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Reference 4','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','REFERENCE_5',101,'Reference 5','REFERENCE_5','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Reference 5','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','REFERENCE_6',101,'Reference 6','REFERENCE_6','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Reference 6','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','REFERENCE_7',101,'Reference 7','REFERENCE_7','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Reference 7','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','REFERENCE_8',101,'Reference 8','REFERENCE_8','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Reference 8','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','REFERENCE_9',101,'Reference 9','REFERENCE_9','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Reference 9','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','SET_OF_BOOKS',101,'Set Of Books','SET_OF_BOOKS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Set Of Books','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','SET_OF_BOOKS_ID',101,'Set Of Books Id','SET_OF_BOOKS_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Set Of Books Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','COPYRIGHT',101,'Copyright','COPYRIGHT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Copyright','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GCC#50328#FUTURE_USE',101,'Accounting Flexfield (KFF): Segment ''Future Use'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#FUTURE_USE','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT6','GCC#Future Use','50328','1014552','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GCC#50328#FUTURE_USE#DESCR',101,'Accounting Flexfield (KFF) : Segment ''Future Use'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#FUTURE_USE#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT6','GCC#Future Use Descr','50328','1014552','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GCC#Branch',101,'Descriptive flexfield (DFF): GL Accounts Column Name: Branch','GCC#Branch','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','ATTRIBUTE1','Gcc#Branch','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JL#Account',101,'Descriptive flexfield (DFF): Enter Journals: Lines Column Name: Account','JL#Account','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_LINES','ATTRIBUTE3','Jl#Account','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JL#Branch',101,'Descriptive flexfield (DFF): Enter Journals: Lines Column Name: Branch','JL#Branch','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_LINES','ATTRIBUTE1','Jl#Branch','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JL#Dept',101,'Descriptive flexfield (DFF): Enter Journals: Lines Column Name: Dept','JL#Dept','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_LINES','ATTRIBUTE2','Jl#Dept','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JL#POS_Branch',101,'Descriptive flexfield (DFF): Enter Journals: Lines Column Name: POS Branch','JL#POS_Branch','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_LINES','ATTRIBUTE5','Jl#Pos Branch','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JL#Sub_Acct',101,'Descriptive flexfield (DFF): Enter Journals: Lines Column Name: Sub Acct','JL#Sub_Acct','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_LINES','ATTRIBUTE4','Jl#Sub Acct','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JL#WC_Formula',101,'Descriptive flexfield (DFF): Enter Journals: Lines Column Name: WC Formula','JL#WC_Formula','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_LINES','ATTRIBUTE6','Jl#Wc Formula','','','','US','');
--Inserting Object Components for EIS_GL_JOURNAL_DETAILS_V
xxeis.eis_rsc_ins.vcomp( 'EIS_GL_JOURNAL_DETAILS_V','GL_JE_BATCHES',101,'GL_JE_BATCHES','JB','JB','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Journal Entry Batches','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_GL_JOURNAL_DETAILS_V','GL_CODE_COMBINATIONS_KFV',101,'GL_CODE_COMBINATIONS','GCC','GCC','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','GL Code Combinations','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_GL_JOURNAL_DETAILS_V','GL_LEDGERS',101,'GL_LEDGERS','LE','LE','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Ledgers','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_GL_JOURNAL_DETAILS_V','GL_JE_HEADERS',101,'GL_JE_HEADERS','JH','JH','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Journal Entry Headers','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_GL_JOURNAL_DETAILS_V','GL_JE_LINES',101,'GL_JE_LINES','JL','JL','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Journal Entry Lines','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_GL_JOURNAL_DETAILS_V','GL_BUDGET_VERSIONS',101,'GL_BUDGET_VERSIONS','GBV','GBV','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','GL Budget Versions','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_GL_JOURNAL_DETAILS_V','GL_ENCUMBRANCE_TYPES',101,'GL_ENCUMBRANCE_TYPES','GLE','GLE','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Encumbrance Type Definitions','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_GL_JOURNAL_DETAILS_V','GL_JE_SOURCES',101,'GL_JE_SOURCES','JES','JES','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Journal Entry Sources','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_GL_JOURNAL_DETAILS_V','GL_JE_CATEGORIES',101,'GL_JE_CATEGORIES','JEC','JEC','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Journal Entry Categories','','','','','','Y',' ',' ',' ',' ',' ');
--Inserting Object Component Joins for EIS_GL_JOURNAL_DETAILS_V
xxeis.eis_rsc_ins.vcj( 'EIS_GL_JOURNAL_DETAILS_V','GL_JE_SOURCES','JES',101,'EGJDV.JE_SOURCE_NAME','=','JES.JE_SOURCE_NAME(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_GL_JOURNAL_DETAILS_V','GL_JE_CATEGORIES','JEC',101,'EGJDV.JE_CATEGORY_NAME','=','JEC.JE_CATEGORY_NAME(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_GL_JOURNAL_DETAILS_V','GL_LEDGERS','LE',101,'EGJDV.LEDGER_NAME','=','LE.NAME(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_GL_JOURNAL_DETAILS_V','GL_JE_HEADERS','JH',101,'EGJDV.JE_HEADER_ID','=','JH.JE_HEADER_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_GL_JOURNAL_DETAILS_V','GL_JE_BATCHES','JB',101,'EGJDV.JE_BATCH_ID','=','JB.JE_BATCH_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_GL_JOURNAL_DETAILS_V','GL_JE_LINES','JL',101,'EGJDV.JE_LINE_NUM','=','JL.JE_LINE_NUM(+)','','','1','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_GL_JOURNAL_DETAILS_V','GL_JE_LINES','JL',101,'EGJDV.JL_JE_HEADER_ID','=','JL.JE_HEADER_ID(+)','','','2','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_GL_JOURNAL_DETAILS_V','GL_BUDGET_VERSIONS','GBV',101,'EGJDV.BUDGET_VERSION_ID','=','GBV.BUDGET_VERSION_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_GL_JOURNAL_DETAILS_V','GL_ENCUMBRANCE_TYPES','GLE',101,'EGJDV.ENCUMBRANCE_TYPE_ID','=','GLE.ENCUMBRANCE_TYPE_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_GL_JOURNAL_DETAILS_V','GL_CODE_COMBINATIONS_KFV','GCC',101,'EGJDV.CODE_COMBINATION_ID','=','GCC.CODE_COMBINATION_ID(+)','','','','','XXEIS_RS_ADMIN');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 101');
END IF;
END;
/
--Exporting View Source View Component -  EIS_GL_JOURNAL_DETAILS_V
--Exporting View Component Data of the View -  EIS_GL_JOURNAL_DETAILS_V
prompt Creating Object Data GL_JE_BATCHES
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(101);
IF mod_exist = 'Y' THEN 
--Inserting Object GL_JE_BATCHES
xxeis.eis_rsc_ins.v( 'GL_JE_BATCHES',101,'Journal entry batches','1.0','','','ANONYMOUS','APPS','Gl Je Batches','GJB','','','SYNONYM','US','','','');
--Delete Object Columns for GL_JE_BATCHES
xxeis.eis_rsc_utility.delete_view_rows('GL_JE_BATCHES',101,FALSE);
--Inserting Object Columns for GL_JE_BATCHES
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','ORG_ID',101,'Organization defining column','ORG_ID','','','','ANONYMOUS','NUMBER','GL_JE_BATCHES','ORG_ID','Org Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','REQUEST_ID',101,'Posting concurrent request id','REQUEST_ID','','','','ANONYMOUS','NUMBER','GL_JE_BATCHES','REQUEST_ID','Request Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','POSTING_RUN_ID',101,'Posting sequence number','POSTING_RUN_ID','','','','ANONYMOUS','NUMBER','GL_JE_BATCHES','POSTING_RUN_ID','Posting Run Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','CONTEXT2',101,'Descriptive flexfield structure defining column','CONTEXT2','','','','ANONYMOUS','VARCHAR2','GL_JE_BATCHES','CONTEXT2','Context2','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','USSGL_TRANSACTION_CODE',101,'Government transaction code','USSGL_TRANSACTION_CODE','','','','ANONYMOUS','VARCHAR2','GL_JE_BATCHES','USSGL_TRANSACTION_CODE','Ussgl Transaction Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','PACKET_ID',101,'Packet defining column for last funds check of the batch','PACKET_ID','','','','ANONYMOUS','NUMBER','GL_JE_BATCHES','PACKET_ID','Packet Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','UNRESERVATION_PACKET_ID',101,'Packet defining column for last funds unreservation of batch','UNRESERVATION_PACKET_ID','','','','ANONYMOUS','NUMBER','GL_JE_BATCHES','UNRESERVATION_PACKET_ID','Unreservation Packet Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','CONTEXT',101,'Descriptive flexfield context column','CONTEXT','','','','ANONYMOUS','VARCHAR2','GL_JE_BATCHES','CONTEXT','Context','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','ATTRIBUTE10',101,'Descriptive flexfield segment','ATTRIBUTE10','','','','ANONYMOUS','VARCHAR2','GL_JE_BATCHES','ATTRIBUTE10','Attribute10','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','ATTRIBUTE9',101,'Descriptive flexfield segment','ATTRIBUTE9','','','','ANONYMOUS','VARCHAR2','GL_JE_BATCHES','ATTRIBUTE9','Attribute9','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','ATTRIBUTE8',101,'Descriptive flexfield segment','ATTRIBUTE8','','','','ANONYMOUS','VARCHAR2','GL_JE_BATCHES','ATTRIBUTE8','Attribute8','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','ATTRIBUTE7',101,'Descriptive flexfield segment','ATTRIBUTE7','','','','ANONYMOUS','VARCHAR2','GL_JE_BATCHES','ATTRIBUTE7','Attribute7','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','ATTRIBUTE6',101,'Descriptive flexfield segment','ATTRIBUTE6','','','','ANONYMOUS','VARCHAR2','GL_JE_BATCHES','ATTRIBUTE6','Attribute6','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','ATTRIBUTE5',101,'Descriptive flexfield segment','ATTRIBUTE5','','','','ANONYMOUS','VARCHAR2','GL_JE_BATCHES','ATTRIBUTE5','Attribute5','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','ATTRIBUTE4',101,'Descriptive flexfield segment','ATTRIBUTE4','','','','ANONYMOUS','VARCHAR2','GL_JE_BATCHES','ATTRIBUTE4','Attribute4','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','ATTRIBUTE3',101,'Descriptive flexfield segment','ATTRIBUTE3','','','','ANONYMOUS','VARCHAR2','GL_JE_BATCHES','ATTRIBUTE3','Attribute3','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','ATTRIBUTE2',101,'Descriptive flexfield segment','ATTRIBUTE2','','','','ANONYMOUS','VARCHAR2','GL_JE_BATCHES','ATTRIBUTE2','Attribute2','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','ATTRIBUTE1',101,'Descriptive flexfield segment','ATTRIBUTE1','','','','ANONYMOUS','VARCHAR2','GL_JE_BATCHES','ATTRIBUTE1','Attribute1','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','PARENT_JE_BATCH_ID',101,'Defining column of the parent batch in the source reporting currency ledger','PARENT_JE_BATCH_ID','','','','ANONYMOUS','NUMBER','GL_JE_BATCHES','PARENT_JE_BATCH_ID','Parent Je Batch Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','RUNNING_TOTAL_ACCOUNTED_CR',101,'Batch running total credit, base currency','RUNNING_TOTAL_ACCOUNTED_CR','','~T~D~2','','ANONYMOUS','NUMBER','GL_JE_BATCHES','RUNNING_TOTAL_ACCOUNTED_CR','Running Total Accounted Cr','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','RUNNING_TOTAL_ACCOUNTED_DR',101,'Batch running total debit, base currency','RUNNING_TOTAL_ACCOUNTED_DR','','~T~D~2','','ANONYMOUS','NUMBER','GL_JE_BATCHES','RUNNING_TOTAL_ACCOUNTED_DR','Running Total Accounted Dr','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','RUNNING_TOTAL_CR',101,'Batch running total credit, entered currency','RUNNING_TOTAL_CR','','~T~D~2','','ANONYMOUS','NUMBER','GL_JE_BATCHES','RUNNING_TOTAL_CR','Running Total Cr','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','RUNNING_TOTAL_DR',101,'Batch running total debit, entered currency','RUNNING_TOTAL_DR','','~T~D~2','','ANONYMOUS','NUMBER','GL_JE_BATCHES','RUNNING_TOTAL_DR','Running Total Dr','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','CONTROL_TOTAL',101,'Control total column','CONTROL_TOTAL','','~T~D~2','','ANONYMOUS','NUMBER','GL_JE_BATCHES','CONTROL_TOTAL','Control Total','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','DESCRIPTION',101,'Journal entry batch description','DESCRIPTION','','','','ANONYMOUS','VARCHAR2','GL_JE_BATCHES','DESCRIPTION','Description','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','DATE_CREATED',101,'Date batch was created','DATE_CREATED','','','','ANONYMOUS','DATE','GL_JE_BATCHES','DATE_CREATED','Date Created','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','POSTED_DATE',101,'Date batch was posted','POSTED_DATE','','','','ANONYMOUS','DATE','GL_JE_BATCHES','POSTED_DATE','Posted Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','EARLIEST_POSTABLE_DATE',101,'Earliest date batch can be posted','EARLIEST_POSTABLE_DATE','','','','ANONYMOUS','DATE','GL_JE_BATCHES','EARLIEST_POSTABLE_DATE','Earliest Postable Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','UNIQUE_DATE',101,'No longer used','UNIQUE_DATE','','','','ANONYMOUS','VARCHAR2','GL_JE_BATCHES','UNIQUE_DATE','Unique Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','DEFAULT_PERIOD_NAME',101,'Accounting period for batch','DEFAULT_PERIOD_NAME','','','','ANONYMOUS','VARCHAR2','GL_JE_BATCHES','DEFAULT_PERIOD_NAME','Default Period Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','STATUS_RESET_FLAG',101,'No longer used','STATUS_RESET_FLAG','','','','ANONYMOUS','VARCHAR2','GL_JE_BATCHES','STATUS_RESET_FLAG','Status Reset Flag','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','LAST_UPDATE_LOGIN',101,'Standard Who column','LAST_UPDATE_LOGIN','','','','ANONYMOUS','NUMBER','GL_JE_BATCHES','LAST_UPDATE_LOGIN','Last Update Login','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','CREATED_BY',101,'Standard Who column','CREATED_BY','','','','ANONYMOUS','NUMBER','GL_JE_BATCHES','CREATED_BY','Created By','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','CREATION_DATE',101,'Standard Who column','CREATION_DATE','','','','ANONYMOUS','DATE','GL_JE_BATCHES','CREATION_DATE','Creation Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','APPROVAL_STATUS_CODE',101,'Journal entry batch approval status','APPROVAL_STATUS_CODE','','','','ANONYMOUS','VARCHAR2','GL_JE_BATCHES','APPROVAL_STATUS_CODE','Approval Status Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','BUDGETARY_CONTROL_STATUS',101,'Journal entry batch funds check status','BUDGETARY_CONTROL_STATUS','','','','ANONYMOUS','VARCHAR2','GL_JE_BATCHES','BUDGETARY_CONTROL_STATUS','Budgetary Control Status','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','AVERAGE_JOURNAL_FLAG',101,'Average journal flag','AVERAGE_JOURNAL_FLAG','','','','ANONYMOUS','VARCHAR2','GL_JE_BATCHES','AVERAGE_JOURNAL_FLAG','Average Journal Flag','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','DEFAULT_EFFECTIVE_DATE',101,'Date within default accounting period','DEFAULT_EFFECTIVE_DATE','','','','ANONYMOUS','DATE','GL_JE_BATCHES','DEFAULT_EFFECTIVE_DATE','Default Effective Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','ACTUAL_FLAG',101,'Balance type (Actual, Budget, or Encumbrance)','ACTUAL_FLAG','','','','ANONYMOUS','VARCHAR2','GL_JE_BATCHES','ACTUAL_FLAG','Actual Flag','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','STATUS_VERIFIED',101,'Batch status verified by posting process','STATUS_VERIFIED','','','','ANONYMOUS','VARCHAR2','GL_JE_BATCHES','STATUS_VERIFIED','Status Verified','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','STATUS',101,'Journal entry batch status','STATUS','','','','ANONYMOUS','VARCHAR2','GL_JE_BATCHES','STATUS','Status','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','NAME',101,'Name of journal entry batch','NAME','','','','ANONYMOUS','VARCHAR2','GL_JE_BATCHES','NAME','Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','SET_OF_BOOKS_ID_11I',101,'No Longer Used in R12','SET_OF_BOOKS_ID_11I','','','','ANONYMOUS','NUMBER','GL_JE_BATCHES','SET_OF_BOOKS_ID_11I','Set Of Books Id 11i','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','LAST_UPDATED_BY',101,'Standard Who column','LAST_UPDATED_BY','','','','ANONYMOUS','NUMBER','GL_JE_BATCHES','LAST_UPDATED_BY','Last Updated By','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','LAST_UPDATE_DATE',101,'Standard Who column','LAST_UPDATE_DATE','','','','ANONYMOUS','DATE','GL_JE_BATCHES','LAST_UPDATE_DATE','Last Update Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','JE_BATCH_ID',101,'Journal entry batch defining column','JE_BATCH_ID','','','','ANONYMOUS','NUMBER','GL_JE_BATCHES','JE_BATCH_ID','Je Batch Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','GLOBAL_ATTRIBUTE9',101,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE9','','','','ANONYMOUS','VARCHAR2','GL_JE_BATCHES','GLOBAL_ATTRIBUTE9','Global Attribute9','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','GLOBAL_ATTRIBUTE8',101,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE8','','','','ANONYMOUS','VARCHAR2','GL_JE_BATCHES','GLOBAL_ATTRIBUTE8','Global Attribute8','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','GLOBAL_ATTRIBUTE7',101,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE7','','','','ANONYMOUS','VARCHAR2','GL_JE_BATCHES','GLOBAL_ATTRIBUTE7','Global Attribute7','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','GLOBAL_ATTRIBUTE6',101,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE6','','','','ANONYMOUS','VARCHAR2','GL_JE_BATCHES','GLOBAL_ATTRIBUTE6','Global Attribute6','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','GLOBAL_ATTRIBUTE5',101,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE5','','','','ANONYMOUS','VARCHAR2','GL_JE_BATCHES','GLOBAL_ATTRIBUTE5','Global Attribute5','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','GLOBAL_ATTRIBUTE4',101,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE4','','','','ANONYMOUS','VARCHAR2','GL_JE_BATCHES','GLOBAL_ATTRIBUTE4','Global Attribute4','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','GLOBAL_ATTRIBUTE3',101,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE3','','','','ANONYMOUS','VARCHAR2','GL_JE_BATCHES','GLOBAL_ATTRIBUTE3','Global Attribute3','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','GLOBAL_ATTRIBUTE2',101,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE2','','','','ANONYMOUS','VARCHAR2','GL_JE_BATCHES','GLOBAL_ATTRIBUTE2','Global Attribute2','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','GLOBAL_ATTRIBUTE1',101,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE1','','','','ANONYMOUS','VARCHAR2','GL_JE_BATCHES','GLOBAL_ATTRIBUTE1','Global Attribute1','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','GLOBAL_ATTRIBUTE_CATEGORY',101,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE_CATEGORY','','','','ANONYMOUS','VARCHAR2','GL_JE_BATCHES','GLOBAL_ATTRIBUTE_CATEGORY','Global Attribute Category','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','APPROVER_EMPLOYEE_ID',101,'Defining column of the employee who submitted the journal batch for approval','APPROVER_EMPLOYEE_ID','','','','ANONYMOUS','NUMBER','GL_JE_BATCHES','APPROVER_EMPLOYEE_ID','Approver Employee Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','GROUP_ID',101,'Interface group identifying column','GROUP_ID','','','','ANONYMOUS','NUMBER','GL_JE_BATCHES','GROUP_ID','Group Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','ACCOUNTED_PERIOD_TYPE',101,'Accounting period type','ACCOUNTED_PERIOD_TYPE','','','','ANONYMOUS','VARCHAR2','GL_JE_BATCHES','ACCOUNTED_PERIOD_TYPE','Accounted Period Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','PERIOD_SET_NAME',101,'Accounting calendar name','PERIOD_SET_NAME','','','','ANONYMOUS','VARCHAR2','GL_JE_BATCHES','PERIOD_SET_NAME','Period Set Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','CHART_OF_ACCOUNTS_ID',101,'Key flexfield structure defining column','CHART_OF_ACCOUNTS_ID','','','','ANONYMOUS','NUMBER','GL_JE_BATCHES','CHART_OF_ACCOUNTS_ID','Chart Of Accounts Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','POSTED_BY',101,'User who posted the journal batch','POSTED_BY','','','','ANONYMOUS','NUMBER','GL_JE_BATCHES','POSTED_BY','Posted By','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','GLOBAL_ATTRIBUTE20',101,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE20','','','','ANONYMOUS','VARCHAR2','GL_JE_BATCHES','GLOBAL_ATTRIBUTE20','Global Attribute20','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','GLOBAL_ATTRIBUTE19',101,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE19','','','','ANONYMOUS','VARCHAR2','GL_JE_BATCHES','GLOBAL_ATTRIBUTE19','Global Attribute19','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','GLOBAL_ATTRIBUTE18',101,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE18','','','','ANONYMOUS','VARCHAR2','GL_JE_BATCHES','GLOBAL_ATTRIBUTE18','Global Attribute18','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','GLOBAL_ATTRIBUTE17',101,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE17','','','','ANONYMOUS','VARCHAR2','GL_JE_BATCHES','GLOBAL_ATTRIBUTE17','Global Attribute17','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','GLOBAL_ATTRIBUTE16',101,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE16','','','','ANONYMOUS','VARCHAR2','GL_JE_BATCHES','GLOBAL_ATTRIBUTE16','Global Attribute16','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','GLOBAL_ATTRIBUTE15',101,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE15','','','','ANONYMOUS','VARCHAR2','GL_JE_BATCHES','GLOBAL_ATTRIBUTE15','Global Attribute15','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','GLOBAL_ATTRIBUTE14',101,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE14','','','','ANONYMOUS','VARCHAR2','GL_JE_BATCHES','GLOBAL_ATTRIBUTE14','Global Attribute14','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','GLOBAL_ATTRIBUTE13',101,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE13','','','','ANONYMOUS','VARCHAR2','GL_JE_BATCHES','GLOBAL_ATTRIBUTE13','Global Attribute13','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','GLOBAL_ATTRIBUTE12',101,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE12','','','','ANONYMOUS','VARCHAR2','GL_JE_BATCHES','GLOBAL_ATTRIBUTE12','Global Attribute12','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','GLOBAL_ATTRIBUTE11',101,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE11','','','','ANONYMOUS','VARCHAR2','GL_JE_BATCHES','GLOBAL_ATTRIBUTE11','Global Attribute11','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_JE_BATCHES','GLOBAL_ATTRIBUTE10',101,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE10','','','','ANONYMOUS','VARCHAR2','GL_JE_BATCHES','GLOBAL_ATTRIBUTE10','Global Attribute10','','','','US','');
--Inserting Object Components for GL_JE_BATCHES
--Inserting Object Component Joins for GL_JE_BATCHES
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 101');
END IF;
END;
/
prompt Creating Report LOV Data for HDS-Manual Entries
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(101);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - HDS-Manual Entries
xxeis.eis_rsc_ins.lov( 101,'SELECT distinct user_je_source_name FROM gl_je_sources','','EIS_GL_JE_SOURCES_LOV','LOV of all Available Journal Sources','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 101,'SELECT DISTINCT period_name FROM gl_je_headers WHERE UPPER (je_source) = UPPER (''Payroll'')','','XXEIS_PERIOD_NAME','','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( '','SELECT ffv.flex_value, ffvtl.description, decode(ffv.summary_flag,''Y'', ''Parent'',''N'', ''Child'') Type 
				FROM 
					fnd_flex_value_sets ffvs , 
					fnd_flex_values ffv, 
					fnd_flex_values_tl ffvtl 
				WHERE   upper(ffvs.flex_value_set_name) = upper(''XXCUS_GL_PRODUCT'') 
				 and ffv.flex_value_set_id = ffvs.flex_value_set_id 
				 and ffv.FLEX_VALUE_ID = ffvtl.FLEX_VALUE_ID 
				AND ffv.enabled_flag = upper(''Y'') 
				AND ffv.summary_flag in (''Y'',''N'') 
				/* AND ffv.summary_flag <> upper(''Y'') */ 
				AND ffvtl.LANGUAGE = USERENV(''LANG'') 
				 AND xxeis.eis_gl_security_pkg.validate_segment_value( ''SEGMENT1'' , ffv.flex_value)=''TRUE'' 
				order by ffv.flex_value	','','XXCUS_GL_PRODUCT','XXCUS_GL_PRODUCT','MM050208',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( '','SELECT ffv.flex_value, ffvtl.description, decode(ffv.summary_flag,''Y'', ''Parent'',''N'', ''Child'') Type 
				FROM 
					fnd_flex_value_sets ffvs , 
					fnd_flex_values ffv, 
					fnd_flex_values_tl ffvtl 
				WHERE   upper(ffvs.flex_value_set_name) = upper(''XXCUS_GL_LOCATION'') 
				 and ffv.flex_value_set_id = ffvs.flex_value_set_id 
				 and ffv.FLEX_VALUE_ID = ffvtl.FLEX_VALUE_ID 
				AND ffv.enabled_flag = upper(''Y'') 
				AND ffv.summary_flag in (''Y'',''N'') 
				/* AND ffv.summary_flag <> upper(''Y'') */ 
				AND ffvtl.LANGUAGE = USERENV(''LANG'') 
				 AND xxeis.eis_gl_security_pkg.validate_segment_value( ''SEGMENT2'' , ffv.flex_value)=''TRUE'' 
				order by ffv.flex_value	','','XXCUS_GL_LOCATION','XXCUS_GL_LOCATION','MM050208',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( '','SELECT ffv.flex_value, ffvtl.description, decode(ffv.summary_flag,''Y'', ''Parent'',''N'', ''Child'') Type 
				FROM 
					fnd_flex_value_sets ffvs , 
					fnd_flex_values ffv, 
					fnd_flex_values_tl ffvtl 
				WHERE   upper(ffvs.flex_value_set_name) = upper(''XXCUS_GL_ACCOUNT'') 
				 and ffv.flex_value_set_id = ffvs.flex_value_set_id 
				 and ffv.FLEX_VALUE_ID = ffvtl.FLEX_VALUE_ID 
				AND ffv.enabled_flag = upper(''Y'') 
				AND ffv.summary_flag in (''Y'',''N'') 
				/* AND ffv.summary_flag <> upper(''Y'') */ 
				AND ffvtl.LANGUAGE = USERENV(''LANG'') 
				 AND xxeis.eis_gl_security_pkg.validate_segment_value( ''SEGMENT4'' , ffv.flex_value)=''TRUE'' 
				order by ffv.flex_value	','','XXCUS_GL_ACCOUNT','XXCUS_GL_ACCOUNT','MM050208',NULL,'','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 101');
END IF;
END;
/
prompt Creating Report Data for HDS-Manual Entries
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(101);
IF mod_exist = 'Y' THEN 
--Deleting Report data - HDS-Manual Entries
xxeis.eis_rsc_utility.delete_report_rows( 'HDS-Manual Entries',101 );
--Inserting Report - HDS-Manual Entries
xxeis.eis_rsc_ins.r( 101,'HDS-Manual Entries','','Detail GL report for Spreadsheet entries.  Can be used for other categories also, Spreadsheet will be the default value, but can be overridden.','','','','KP012542','EIS_GL_JOURNAL_DETAILS_V','Y','','','KP012542','','N','Audit and Setup','','CSV,Pivot Excel,EXCEL,','N','','','','','','','','US','','','','','','','','','','','','','','','','');
--Inserting Report Columns - HDS-Manual Entries
xxeis.eis_rsc_ins.rc( 'HDS-Manual Entries',101,'ACCOUNTED_NET','Amount','Accounted Net','','','','','9','N','','','','','','','','KP012542','N','N','','EIS_GL_JOURNAL_DETAILS_V','','','SUM','US','','');
xxeis.eis_rsc_ins.rc( 'HDS-Manual Entries',101,'JE_CREATION_DATE','Creation Date','Standard Who column','','','','','11','N','','','','','','','','KP012542','N','N','','EIS_GL_JOURNAL_DETAILS_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS-Manual Entries',101,'JE_LINE_DESCRIPTION','Je Line Description','Journal entry line description','','','','','8','N','','','','','','','','KP012542','N','N','','EIS_GL_JOURNAL_DETAILS_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS-Manual Entries',101,'JE_NAME','Je Name','Journal entry header name','','','','','6','N','','','','','','','','KP012542','N','N','','EIS_GL_JOURNAL_DETAILS_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS-Manual Entries',101,'JE_PERIOD','Period Name','Accounting period','','','','','4','N','','','','','','','','KP012542','N','N','','EIS_GL_JOURNAL_DETAILS_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS-Manual Entries',101,'JE_POSTED_DATE','Posted Date','Date journal entry header was posted','','','','','5','N','','','','','','','','KP012542','N','N','','EIS_GL_JOURNAL_DETAILS_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS-Manual Entries',101,'JE_SOURCE','Je Source','Journal entry source user defined name','','','','','10','N','','','','','','','','KP012542','N','N','','EIS_GL_JOURNAL_DETAILS_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS-Manual Entries',101,'NAME','Je Batch Name','Name of journal entry batch','','','','','7','N','','','','','','','','KP012542','N','N','','EIS_GL_JOURNAL_DETAILS_V','GL_JE_BATCHES','JB','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS-Manual Entries',101,'GCC#50328#ACCOUNT','Account','Accounting Flexfield (KFF): Segment ''Account'' under chart of accounts ''XXCUS Accounting Flexfield''','','','','','3','N','','','','','','','','KP012542','N','N','','EIS_GL_JOURNAL_DETAILS_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS-Manual Entries',101,'GCC#50328#LOCATION','Location','Accounting Flexfield (KFF): Segment ''Location'' under chart of accounts ''XXCUS Accounting Flexfield''','','','','','2','N','','','','','','','','KP012542','N','N','','EIS_GL_JOURNAL_DETAILS_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS-Manual Entries',101,'GCC#50328#PRODUCT','Product','Accounting Flexfield (KFF): Segment ''Product'' under chart of accounts ''XXCUS Accounting Flexfield''','','','','','1','N','','','','','','','','KP012542','N','N','','EIS_GL_JOURNAL_DETAILS_V','','','GROUP_BY','US','','');
--Inserting Report Parameters - HDS-Manual Entries
xxeis.eis_rsc_ins.rp( 'HDS-Manual Entries',101,'Period','Accounting period','JE_PERIOD','IN','XXEIS_PERIOD_NAME','','VARCHAR2','N','Y','1','Y','Y','CONSTANT','KP012542','Y','N','','','','EIS_GL_JOURNAL_DETAILS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS-Manual Entries',101,'Posted Date','Date journal entry header was posted','JE_POSTED_DATE','IN','','','DATE','N','Y','6','Y','Y','CONSTANT','KP012542','Y','N','','Start Date','','EIS_GL_JOURNAL_DETAILS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS-Manual Entries',101,'Journal Source','JE Source','JE_SOURCE','IN','EIS_GL_JE_SOURCES_LOV','''Spreadsheet''','VARCHAR2','N','Y','2','Y','Y','CONSTANT','KP012542','Y','N','','','','EIS_GL_JOURNAL_DETAILS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS-Manual Entries',101,'Account','Accounting Flexfield : Segment ''Account'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#ACCOUNT','IN','XXCUS_GL_ACCOUNT','','VARCHAR2','N','Y','5','Y','Y','CONSTANT','KP012542','Y','N','','','','EIS_GL_JOURNAL_DETAILS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS-Manual Entries',101,'Location','Accounting Flexfield : Segment ''Location'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#LOCATION','IN','XXCUS_GL_LOCATION','','VARCHAR2','N','Y','4','Y','Y','CONSTANT','KP012542','Y','N','','','','EIS_GL_JOURNAL_DETAILS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS-Manual Entries',101,'Product','Accounting Flexfield ''Product''','GCC#50328#PRODUCT','IN','XXCUS_GL_PRODUCT','','VARCHAR2','N','Y','3','Y','Y','CONSTANT','KP012542','Y','N','','','','EIS_GL_JOURNAL_DETAILS_V','','','US','');
--Inserting Dependent Parameters - HDS-Manual Entries
--Inserting Report Conditions - HDS-Manual Entries
xxeis.eis_rsc_ins.rcnh( 'HDS-Manual Entries',101,'EGJDV.JE_PERIOD IN Period','SIMPLE','','','Y','','1');
xxeis.eis_rsc_ins.rcnd( '','','JE_PERIOD','','Period','','','','','EIS_GL_JOURNAL_DETAILS_V','','','','','','IN','Y','Y','','','','','1',101,'HDS-Manual Entries','EGJDV.JE_PERIOD IN Period');
xxeis.eis_rsc_ins.rcnh( 'HDS-Manual Entries',101,'EGJDV.JE_SOURCE IN Journal Source','SIMPLE','','','Y','','2');
xxeis.eis_rsc_ins.rcnd( '','','JE_SOURCE','','Journal Source','','','','','EIS_GL_JOURNAL_DETAILS_V','','','','','','IN','Y','Y','','','','','1',101,'HDS-Manual Entries','EGJDV.JE_SOURCE IN Journal Source');
xxeis.eis_rsc_ins.rcnh( 'HDS-Manual Entries',101,'EGJDV.GCC#50328#PRODUCT IN Product','SIMPLE','','','Y','','3');
xxeis.eis_rsc_ins.rcnd( '','','GCC#50328#PRODUCT','','Product','','','','','EIS_GL_JOURNAL_DETAILS_V','','','','','','IN','Y','Y','','','','','1',101,'HDS-Manual Entries','EGJDV.GCC#50328#PRODUCT IN Product');
xxeis.eis_rsc_ins.rcnh( 'HDS-Manual Entries',101,'EGJDV.GCC#50328#LOCATION IN Location','SIMPLE','','','Y','','4');
xxeis.eis_rsc_ins.rcnd( '','','GCC#50328#LOCATION','','Location','','','','','EIS_GL_JOURNAL_DETAILS_V','','','','','','IN','Y','Y','','','','','1',101,'HDS-Manual Entries','EGJDV.GCC#50328#LOCATION IN Location');
xxeis.eis_rsc_ins.rcnh( 'HDS-Manual Entries',101,'EGJDV.GCC#50328#ACCOUNT IN Account','SIMPLE','','','Y','','5');
xxeis.eis_rsc_ins.rcnd( '','','GCC#50328#ACCOUNT','','Account','','','','','EIS_GL_JOURNAL_DETAILS_V','','','','','','IN','Y','Y','','','','','1',101,'HDS-Manual Entries','EGJDV.GCC#50328#ACCOUNT IN Account');
xxeis.eis_rsc_ins.rcnh( 'HDS-Manual Entries',101,'EGJDV.JE_POSTED_DATE IN Posted Date','SIMPLE','','','Y','','6');
xxeis.eis_rsc_ins.rcnd( '','','JE_POSTED_DATE','','Posted Date','','','','','EIS_GL_JOURNAL_DETAILS_V','','','','','','IN','Y','Y','','','','','1',101,'HDS-Manual Entries','EGJDV.JE_POSTED_DATE IN Posted Date');
--Inserting Report Sorts - HDS-Manual Entries
--Inserting Report Triggers - HDS-Manual Entries
--inserting report templates - HDS-Manual Entries
--Inserting Report Portals - HDS-Manual Entries
--inserting report dashboards - HDS-Manual Entries
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'HDS-Manual Entries','101','EIS_GL_JOURNAL_DETAILS_V','EIS_GL_JOURNAL_DETAILS_V','N','');
xxeis.eis_rsc_ins.rviews( 'HDS-Manual Entries','101','EIS_GL_JOURNAL_DETAILS_V','GL_JE_BATCHES','N','JB');
--inserting report security - HDS-Manual Entries
xxeis.eis_rsc_ins.rsec( 'HDS-Manual Entries','20005','','XXWC_VIEW_ALL_EIS_REPORTS',101,'KP012542','','','');
--Inserting Report Pivots - HDS-Manual Entries
--Inserting Report Distribution Details 
--Inserting Report Distribution Details 
--Inserting Report SubTotals Details 
--Inserting Report aggregations 
--Inserting Report   Version details- HDS-Manual Entries
xxeis.eis_rsc_ins.rv( 'HDS-Manual Entries','','HDS-Manual Entries','SA059956','29-JUN-2017');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 101');
END IF;
END;
/
