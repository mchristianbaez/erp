--Report Name            : Inventory - Onhand Quantity - WC
--Import Instructions : 1. If RTF template needs to be uploaded to report automatically then move the RTF template to physical path of APPS_DATA_FILE_DIR oracle directory. Check dba_directories view to get the physical path of oracle directory. 
--                         (If you want to use any other oracle directory other than APPS_DATA_FILE_DIR then replace APPS_DATA_FILE_DIR with your oracle directory name below.)
--                      2. Make sure XXEIS schema has read/write permissions to above used oracle directory and give full permissions to the directory at OS level.
--                      3. Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
exec xxeis.eis_rsc_ins.g_rtf_dir := 'APPS_DATA_FILE_DIR' ;
prompt Creating Object Data EIS_XXWC_INV_ONHAND_QUANTITY_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(401);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_INV_ONHAND_QUANTITY_V
xxeis.eis_rsc_ins.v( 'EIS_XXWC_INV_ONHAND_QUANTITY_V',401,'This view displays the onhand availability of Item at subinventory level. If the item is locator controled then onhand availability at locator level overrides the subinventory level. If revision enabled for the item then the onhand availability under each revision is displayed.','','','','XXEIS_RS_ADMIN','XXEIS','EIS INV Item Onhand Quantity','EXIOQV','','','VIEW','US','Y','');
--Delete Object Columns for EIS_XXWC_INV_ONHAND_QUANTITY_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_INV_ONHAND_QUANTITY_V',401,FALSE);
--Inserting Object Columns for EIS_XXWC_INV_ONHAND_QUANTITY_V
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','INV_ORG_NAME',401,'Translated name of the organization','INV_ORG_NAME','','','INV Organization Names LOV','XXEIS_RS_ADMIN','VARCHAR2','HR_ALL_ORGANIZATION_UNITS_TL','NAME','Inv Org Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','ORGANIZATION_CODE',401,'Organization code','ORGANIZATION_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_PARAMETERS','ORGANIZATION_CODE','Organization Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','SUBINVENTORY_CODE',401,'Subinventory code','SUBINVENTORY_CODE','','','INV Item Sub Inventory Names LOV','XXEIS_RS_ADMIN','VARCHAR2','MTL_ONHAND_QUANTITIES_DETAIL','SUBINVENTORY_CODE','Subinventory Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','ITEM',401,'ITEM','ITEM','','','INV Items LOV','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','CONCATENATED_SEGMENTS','Item','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','COST_GROUP',401,'Cost group name','COST_GROUP','','','INV Cost Group Names LOV','XXEIS_RS_ADMIN','VARCHAR2','CST_COST_GROUPS','COST_GROUP','Cost Group','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','UNIT_OF_MEASURE',401,'Unit of measure name','UNIT_OF_MEASURE','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_UNITS_OF_MEASURE_TL','UNIT_OF_MEASURE','Unit Of Measure','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','ON_HAND',401,'Transaction quantity in the primary unit of measure of the item','ON_HAND','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','MTL_ONHAND_QUANTITIES_DETAIL','PRIMARY_TRANSACTION_QUANTITY','On Hand','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MAX_MINMAX_QUANTITY',401,'Maximum minmax order quantity','MAX_MINMAX_QUANTITY~MAX_MINMAX_QUANTITY','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','MTL_SYSTEM_ITEMS_B','MAX_MINMAX_QUANTITY','Max Minmax Quantity','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MIN_MINMAX_QUANTITY',401,'Minimum minmax order quantity','MIN_MINMAX_QUANTITY~MIN_MINMAX_QUANTITY','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','MTL_SYSTEM_ITEMS_B','MIN_MINMAX_QUANTITY','Min Minmax Quantity','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','DESCRIPTION',401,'Description','DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Description','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','OPEN_DEMAND',401,'Open Demand','OPEN_DEMAND','','','','XXEIS_RS_ADMIN','NUMBER','','','Open Demand','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','AVERAGE_COST',401,'Average Cost','AVERAGE_COST','','','','XXEIS_RS_ADMIN','NUMBER','','','Average Cost','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','CREATION_DATE',401,'Creation Date','CREATION_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Creation Date','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','REGION',401,'Region','REGION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Region','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','STATUS_CODE',401,'Status name','STATUS_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_MATERIAL_STATUSES_TL','STATUS_CODE','Status Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','UNIT_NUMBER',401,'End item model/unit number','UNIT_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','PJM_UNIT_NUMBERS','UNIT_NUMBER','Unit Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MASTER_ORG_CODE',401,'Organization code','MASTER_ORG_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','ORG_ORGANIZATION_DEFINITIONS','ORGANIZATION_CODE','Master Org Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','LOCATOR',401,'Locator','LOCATOR','','','','XXEIS_RS_ADMIN','VARCHAR2','DERIVED COLUMN','DERIVED COLUMN','Locator','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','REVISION',401,'Item revision code','REVISION','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_ONHAND_QUANTITIES_DETAIL','REVISION','Revision','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','OWNING_ORGANIZATION',401,'The name of the organization that incurred the expenditure  If an employee is specified, this column can be NULL since PA derives its value based on the employees organization assignment as of the expenditure item date','OWNING_ORGANIZATION','','','','XXEIS_RS_ADMIN','VARCHAR2','ORG_ORGANIZATION_DEFINITIONS','ORGANIZATION_NAME','Owning Organization','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','PLANNING_ORGANIZATION',401,'The name of the organization that incurred the expenditure  If an employee is specified, this column can be NULL since PA derives its value based on the employees organization assignment as of the expenditure item date','PLANNING_ORGANIZATION','','','','XXEIS_RS_ADMIN','VARCHAR2','ORG_ORGANIZATION_DEFINITIONS','ORGANIZATION_NAME','Planning Organization','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','UNPACKED',401,'Transaction quantity in the primary unit of measure of the item','UNPACKED','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','MTL_ONHAND_QUANTITY_DETAILS','PRIMARY_TRANSACTION_QUANTITY','Unpacked','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','PACKED',401,'Transaction quantity in the primary unit of measure of the item','PACKED','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','MTL_ONHAND_QUANTITY_DETAILS','PRIMARY_TRANSACTION_QUANTITY','Packed','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','LOT_NUMBER',401,'Lot number','LOT_NUMBER','','','INV Lot Numbers LOV','XXEIS_RS_ADMIN','VARCHAR2','MTL_ONHAND_QUANTITIES_DETAIL','LOT_NUMBER','Lot Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','PROJECT_NAME',401,'User-entered project / seiban name','PROJECT_NAME','','','INV Item Project Names LOV','XXEIS_RS_ADMIN','VARCHAR2','PJM_PROJECT_PARAMETERS','PROJECT_NAME','Project Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','LOT_EXPIRY_DATE',401,'Lot expiration date','LOT_EXPIRY_DATE','','','','XXEIS_RS_ADMIN','DATE','MTL_LOT_NUMBERS','EXPIRATION_DATE','Lot Expiry Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MIL_ORGANIZATION_ID',401,'Organization identifier','MIL_ORGANIZATION_ID~MIL_ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','MTL_ITEM_LOCATIONS','ORGANIZATION_ID','Mil Organization Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MMS_STATUS_ID',401,'Status identifier','MMS_STATUS_ID~MMS_STATUS_ID','','','','XXEIS_RS_ADMIN','NUMBER','MTL_MATERIAL_STATUSES_TL','STATUS_ID','Mms Status Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MP_ORGANIZATION_ID',401,'Organization identifier','MP_ORGANIZATION_ID~MP_ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','MTL_PARAMETERS','ORGANIZATION_ID','Mp Organization Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MSIV_INVENTORY_ITEM_ID',401,'Inventory item identifier','MSIV_INVENTORY_ITEM_ID~MSIV_INVENTORY_ITEM_ID','','','','XXEIS_RS_ADMIN','NUMBER','MTL_SYSTEM_ITEMS_B','INVENTORY_ITEM_ID','Msiv Inventory Item Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MSIV_ORGANIZATION_ID',401,'Organization identifier','MSIV_ORGANIZATION_ID~MSIV_ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','MTL_SYSTEM_ITEMS_B','ORGANIZATION_ID','Msiv Organization Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MSI_ORGANIZATION_ID',401,'Organization identifier','MSI_ORGANIZATION_ID~MSI_ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','MTL_SECONDARY_INVENTORIES','ORGANIZATION_ID','Msi Organization Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','OODP_ORGANIZATION_ID',401,'Organization identifier','OODP_ORGANIZATION_ID~OODP_ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','ORG_ORGANIZATION_DEFINITIONS','ORGANIZATION_ID','Oodp Organization Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','OODW_ORGANIZATION_ID',401,'Organization identifier','OODW_ORGANIZATION_ID~OODW_ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','ORG_ORGANIZATION_DEFINITIONS','ORGANIZATION_ID','Oodw Organization Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','OOD_ORGANIZATION_ID',401,'Organization identifier','OOD_ORGANIZATION_ID~OOD_ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','ORG_ORGANIZATION_DEFINITIONS','ORGANIZATION_ID','Ood Organization Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MLN_INVENTORY_ITEM_ID',401,'Inventory item identifier','MLN_INVENTORY_ITEM_ID~MLN_INVENTORY_ITEM_ID','','','','XXEIS_RS_ADMIN','NUMBER','MTL_LOT_NUMBERS','INVENTORY_ITEM_ID','Mln Inventory Item Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MLN_LOT_NUMBER',401,'Lot number','MLN_LOT_NUMBER~MLN_LOT_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_LOT_NUMBERS','LOT_NUMBER','Mln Lot Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MLN_ORGANIZATION_ID',401,'Organization identifier','MLN_ORGANIZATION_ID~MLN_ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','MTL_LOT_NUMBERS','ORGANIZATION_ID','Mln Organization Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','ORGANIZATION_ID',401,'Organization identifier','ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','MTL_SYSTEM_ITEMS_B','ORGANIZATION_ID','Organization Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','LOCATOR_ID',401,'Locator identifier','LOCATOR_ID','','','','XXEIS_RS_ADMIN','NUMBER','MTL_ONHAND_QUANTITIES_DETAIL','LOCATOR_ID','Locator Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','INVENTORY_ITEM_ID',401,'Inventory item identifier','INVENTORY_ITEM_ID','','','','XXEIS_RS_ADMIN','NUMBER','MTL_ONHAND_QUANTITIES_DETAIL','INVENTORY_ITEM_ID','Inventory Item Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','INVENTORY_LOCATION_ID',401,'Inventory locator identifier','INVENTORY_LOCATION_ID~INVENTORY_LOCATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','MTL_ITEM_LOCATIONS','INVENTORY_LOCATION_ID','Inventory Location Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','LANGUAGE',401,'The defined Language Code of the row (From LANGUAGE_CODE column in FND_LANGUAGES table).','LANGUAGE~LANGUAGE','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_UNITS_OF_MEASURE_TL','LANGUAGE','Language','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','LIST_PRICE_PER_UNIT',401,'Unit list price - purchasing','LIST_PRICE_PER_UNIT~LIST_PRICE_PER_UNIT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','MTL_SYSTEM_ITEMS_B','LIST_PRICE_PER_UNIT','List Price Per Unit','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','LPN',401,'License Plate Number','LPN~LPN','','','','XXEIS_RS_ADMIN','VARCHAR2','WMS_LICENSE_PLATE_NUMBERS','LICENSE_PLATE_NUMBER','Lpn','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','LPN_ID',401,'LPN Identifier','LPN_ID~LPN_ID','','','','XXEIS_RS_ADMIN','NUMBER','MTL_ONHAND_QUANTITIES_DETAIL','LPN_ID','Lpn Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','ONHAND_QUANTITIES_ID',401,'Sequence Column for the rows based on MTL_ONHAND_QUANTITIES_S','ONHAND_QUANTITIES_ID~ONHAND_QUANTITIES_ID','','','','XXEIS_RS_ADMIN','NUMBER','MTL_ONHAND_QUANTITIES_DETAIL','ONHAND_QUANTITIES_ID','Onhand Quantities Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','DATE_RECEIVED',401,'Date received','DATE_RECEIVED','','','','XXEIS_RS_ADMIN','DATE','MTL_ONHAND_QUANTITIES_DETAIL','DATE_RECEIVED','Date Received','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','SECONDARY_INVENTORY_NAME',401,'Subinventory name','SECONDARY_INVENTORY_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SECONDARY_INVENTORIES','SECONDARY_INVENTORY_NAME','Secondary Inventory Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','COST_GROUP_ID',401,'Cost Group ID - Used by WMS only','COST_GROUP_ID','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','MTL_ONHAND_QUANTITIES_DETAIL','COST_GROUP_ID','Cost Group Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','PROJECT_ID',401,'System-generated number that uniquely identifies a project or seiban','PROJECT_ID','','','','XXEIS_RS_ADMIN','NUMBER','PJM_PROJECT_PARAMETERS','PROJECT_ID','Project Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','TASK_ID',401,'Task identifier','TASK_ID','','','','XXEIS_RS_ADMIN','NUMBER','MTL_ITEM_LOCATIONS','TASK_ID','Task Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','SUBINVENTORY_STATUS_ID',401,'Status Identifier  - Used by WMS only','SUBINVENTORY_STATUS_ID','','','','XXEIS_RS_ADMIN','NUMBER','MTL_SECONDARY_INVENTORIES','STATUS_ID','Subinventory Status Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','LOCATOR_STATUS_ID',401,'Status Identifier   - Used by WMS only','LOCATOR_STATUS_ID','','','','XXEIS_RS_ADMIN','NUMBER','MTL_ITEM_LOCATIONS','STATUS_ID','Locator Status Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','LOT_STATUS_ID',401,'Status identifier    - Used by WMS only','LOT_STATUS_ID','','','','XXEIS_RS_ADMIN','NUMBER','MTL_LOT_NUMBERS','STATUS_ID','Lot Status Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','PLANNING_TP_TYPE',401,'Planning Trading Partner Type. The values are based on MTL_TP_TYPES lookup.','PLANNING_TP_TYPE','','','','XXEIS_RS_ADMIN','NUMBER','MTL_ONHAND_QUANTITIES_DETAIL','PLANNING_TP_TYPE','Planning Tp Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','PLANNING_ORGANIZATION_ID',401,'Planning organization identifier','PLANNING_ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','MTL_ONHAND_QUANTITIES_DETAIL','PLANNING_ORGANIZATION_ID','Planning Organization Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','OWNING_TP_TYPE',401,'Owning Trading Partner Type. The values are based on MTL_TP_TYPES lookup.','OWNING_TP_TYPE','','','','XXEIS_RS_ADMIN','NUMBER','MTL_ONHAND_QUANTITIES_DETAIL','OWNING_TP_TYPE','Owning Tp Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','OWNING_ORGANIZATION_ID',401,'Organization identifier','OWNING_ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','MTL_ONHAND_QUANTITIES_DETAIL','ORGANIZATION_ID','Owning Organization Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','CCGA_COST_GROUP_ID',401,'Cost Group Identifier','CCGA_COST_GROUP_ID~CCGA_COST_GROUP_ID','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','CST_COST_GROUPS','COST_GROUP_ID','Ccga Cost Group Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','CCGA_ORGANIZATION_ID',401,'Organization Identifier','CCGA_ORGANIZATION_ID~CCGA_ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','CST_COST_GROUPS','ORGANIZATION_ID','Ccga Organization Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','CCG_COST_GROUP_ID',401,'Cost Group Identifier','CCG_COST_GROUP_ID~CCG_COST_GROUP_ID','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','CST_COST_GROUPS','COST_GROUP_ID','Ccg Cost Group Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','HAOU_ORGANIZATION_ID',401,'Haou Organization Id','HAOU_ORGANIZATION_ID~HAOU_ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','HR_ALL_ORGANIZATION_UNITS','ORGANIZATION_ID','Haou Organization Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MSIV#WC#HAZMAT_CONTAINER',401,'Msiv#Wc#Hazmat Container','MSIV#WC#HAZMAT_CONTAINER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Msiv#Wc#Hazmat Container','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MSIV#WC#HAZMAT_DESCRIPTION',401,'Msiv#Wc#Hazmat Description','MSIV#WC#HAZMAT_DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Msiv#Wc#Hazmat Description','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MSIV#WC#HAZMAT_PACKAGING_GRO',401,'Msiv#Wc#Hazmat Packaging Gro','MSIV#WC#HAZMAT_PACKAGING_GRO','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Msiv#Wc#Hazmat Packaging Gro','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MSIV#WC#IMPORT_DUTY_',401,'Msiv#Wc#Import Duty ','MSIV#WC#IMPORT_DUTY_','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Msiv#Wc#Import Duty ','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MSIV#WC#KEEP_ITEM_ACTIVE',401,'Msiv#Wc#Keep Item Active','MSIV#WC#KEEP_ITEM_ACTIVE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Msiv#Wc#Keep Item Active','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MSIV#WC#LAST_LEAD_TIME',401,'Msiv#Wc#Last Lead Time','MSIV#WC#LAST_LEAD_TIME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Msiv#Wc#Last Lead Time','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MSIV#WC#MSDS_#',401,'Msiv#Wc#Msds #','MSIV#WC#MSDS_#','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Msiv#Wc#Msds #','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MSIV#WC#ORM_D_FLAG',401,'Msiv#Wc#Orm D Flag','MSIV#WC#ORM_D_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Msiv#Wc#Orm D Flag','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MSIV#WC#PESTICIDE_FLAG',401,'Msiv#Wc#Pesticide Flag','MSIV#WC#PESTICIDE_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Msiv#Wc#Pesticide Flag','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MSIV#WC#PESTICIDE_FLAG_STATE',401,'Msiv#Wc#Pesticide Flag State','MSIV#WC#PESTICIDE_FLAG_STATE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Msiv#Wc#Pesticide Flag State','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MSIV#WC#PRISM_PART_NUMBER',401,'Msiv#Wc#Prism Part Number','MSIV#WC#PRISM_PART_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Msiv#Wc#Prism Part Number','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MSIV#WC#PRODUCT_CODE',401,'Msiv#Wc#Product Code','MSIV#WC#PRODUCT_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Msiv#Wc#Product Code','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MSIV#WC#RESERVE_STOCK',401,'Msiv#Wc#Reserve Stock','MSIV#WC#RESERVE_STOCK','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Msiv#Wc#Reserve Stock','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MSIV#WC#STORE_VELOCITY',401,'Msiv#Wc#Store Velocity','MSIV#WC#STORE_VELOCITY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Msiv#Wc#Store Velocity','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MSIV#WC#TAXWARE_CODE',401,'Msiv#Wc#Taxware Code','MSIV#WC#TAXWARE_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Msiv#Wc#Taxware Code','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MSIV#WC#VOC_CATEGORY',401,'Msiv#Wc#Voc Category','MSIV#WC#VOC_CATEGORY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Msiv#Wc#Voc Category','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MSIV#WC#VOC_GL',401,'Msiv#Wc#Voc Gl','MSIV#WC#VOC_GL','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Msiv#Wc#Voc Gl','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MSIV#WC#VOC_SUB_CATEGORY',401,'Msiv#Wc#Voc Sub Category','MSIV#WC#VOC_SUB_CATEGORY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Msiv#Wc#Voc Sub Category','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MSIV#WC#YEARLY_DC_VELOCITY',401,'Msiv#Wc#Yearly Dc Velocity','MSIV#WC#YEARLY_DC_VELOCITY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Msiv#Wc#Yearly Dc Velocity','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MSIV#WC#YEARLY_STORE_VELOCIT',401,'Msiv#Wc#Yearly Store Velocit','MSIV#WC#YEARLY_STORE_VELOCIT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Msiv#Wc#Yearly Store Velocit','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','SALES_VELOCITY',401,'Sales Velocity','SALES_VELOCITY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Sales Velocity','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MIL#101#STOCKLOCATIONS',401,'Mil#101#Stocklocations','MIL#101#STOCKLOCATIONS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Mil#101#Stocklocations','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MIL#101#STOCKLOCATIONS_DESC',401,'Mil#101#Stocklocations Desc','MIL#101#STOCKLOCATIONS_DESC','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Mil#101#Stocklocations Desc','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MP#BRANCH_OPERATIONS_MANAGER',401,'Mp#Branch Operations Manager','MP#BRANCH_OPERATIONS_MANAGER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Mp#Branch Operations Manager','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MP#DELIVER_CHARGE',401,'Mp#Deliver Charge','MP#DELIVER_CHARGE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Mp#Deliver Charge','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MP#DISTRICT',401,'Mp#District','MP#DISTRICT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Mp#District','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MP#FACTORY_PLANNER_DATA_DIRE',401,'Mp#Factory Planner Data Dire','MP#FACTORY_PLANNER_DATA_DIRE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Mp#Factory Planner Data Dire','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MP#FACTORY_PLANNER_EXECUTABL',401,'Mp#Factory Planner Executabl','MP#FACTORY_PLANNER_EXECUTABL','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Mp#Factory Planner Executabl','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MP#FACTORY_PLANNER_HOST',401,'Mp#Factory Planner Host','MP#FACTORY_PLANNER_HOST','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Mp#Factory Planner Host','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MP#FACTORY_PLANNER_PORT_NUMB',401,'Mp#Factory Planner Port Numb','MP#FACTORY_PLANNER_PORT_NUMB','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Mp#Factory Planner Port Numb','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MP#FACTORY_PLANNER_USER',401,'Mp#Factory Planner User','MP#FACTORY_PLANNER_USER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Mp#Factory Planner User','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MP#FRU',401,'Mp#Fru','MP#FRU','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Mp#Fru','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MP#LOCATION_NUMBER',401,'Mp#Location Number','MP#LOCATION_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Mp#Location Number','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MP#ORG_TYPE',401,'Mp#Org Type','MP#ORG_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Mp#Org Type','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MP#PRICING_ZONE',401,'Mp#Pricing Zone','MP#PRICING_ZONE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Mp#Pricing Zone','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MP#REGION',401,'Mp#Region','MP#REGION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Mp#Region','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MSIV#HDS#DROP_SHIPMENT_ELIGA',401,'Msiv#Hds#Drop Shipment Eliga','MSIV#HDS#DROP_SHIPMENT_ELIGA','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Msiv#Hds#Drop Shipment Eliga','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MSIV#HDS#INVOICE_UOM',401,'Msiv#Hds#Invoice Uom','MSIV#HDS#INVOICE_UOM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Msiv#Hds#Invoice Uom','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MSIV#HDS#LOB',401,'Msiv#Hds#Lob','MSIV#HDS#LOB','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Msiv#Hds#Lob','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MSIV#HDS#PRODUCT_ID',401,'Msiv#Hds#Product Id','MSIV#HDS#PRODUCT_ID','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Msiv#Hds#Product Id','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MSIV#HDS#SKU_DESCRIPTION',401,'Msiv#Hds#Sku Description','MSIV#HDS#SKU_DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Msiv#Hds#Sku Description','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MSIV#HDS#UNSPSC_CODE',401,'Msiv#Hds#Unspsc Code','MSIV#HDS#UNSPSC_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Msiv#Hds#Unspsc Code','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MSIV#HDS#UPC_PRIMARY',401,'Msiv#Hds#Upc Primary','MSIV#HDS#UPC_PRIMARY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Msiv#Hds#Upc Primary','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MSIV#HDS#VENDOR_PART_NUMBER',401,'Msiv#Hds#Vendor Part Number','MSIV#HDS#VENDOR_PART_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Msiv#Hds#Vendor Part Number','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MSIV#WC#AMU',401,'Msiv#Wc#Amu','MSIV#WC#AMU','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Msiv#Wc#Amu','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MSIV#WC#AVERAGE_UNITS',401,'Msiv#Wc#Average Units','MSIV#WC#AVERAGE_UNITS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Msiv#Wc#Average Units','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MSIV#WC#CALC_LEAD_TIME',401,'Msiv#Wc#Calc Lead Time','MSIV#WC#CALC_LEAD_TIME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Msiv#Wc#Calc Lead Time','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MSIV#WC#CA_PROP_65',401,'Msiv#Wc#Ca Prop 65','MSIV#WC#CA_PROP_65','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Msiv#Wc#Ca Prop 65','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MSIV#WC#COUNTRY_OF_ORIGIN',401,'Msiv#Wc#Country Of Origin','MSIV#WC#COUNTRY_OF_ORIGIN','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Msiv#Wc#Country Of Origin','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MSIV#WC#DC_VELOCITY',401,'Msiv#Wc#Dc Velocity','MSIV#WC#DC_VELOCITY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Msiv#Wc#Dc Velocity','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MSIV#WC#GTP_INDICATOR',401,'Msiv#Wc#Gtp Indicator','MSIV#WC#GTP_INDICATOR','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Msiv#Wc#Gtp Indicator','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MTV_TASK_ID',401,'Mtv Task Id','MTV_TASK_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Mtv Task Id','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','SUBINVENTORY_TYPE',401,'Subinventory Type','SUBINVENTORY_TYPE','','','','XXEIS_RS_ADMIN','NUMBER','','','Subinventory Type','','','','');
--Inserting Object Components for EIS_XXWC_INV_ONHAND_QUANTITY_V
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','PA_TASKS',401,'PA_TASKS','MTV','','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Tasks defined within Projects','N','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MTL_PARAMETERS',401,'MTL_PARAMETERS','MP','MP','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Inventory Control Options','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','CST_COST_GROUPS',401,'CST_COST_GROUPS','CCG','CCG','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Item Cost Groups','','','','','CCG','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MTL_MATERIAL_STATUSES_VL',401,'MTL_MATERIAL_STATUSES_B','MMS','MMS','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Inventory Item Status','','','','','MMSV','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MTL_ITEM_LOCATIONS_KFV',401,'MTL_ITEM_LOCATIONS','MIL','MIL','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Locator','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','PJM_UNIT_NUMBERS',401,'PJM_UNIT_NUMBERS','PUN','PUN','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','End Item Model/Unit Numbers','','','','','PUN','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MTL_SECONDARY_INVENTORIES',401,'MTL_SECONDARY_INVENTORIES','MSI','MSI','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Subinventory Definitions','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','HR_ORGANIZATION_UNITS',401,'HR_ALL_ORGANIZATION_UNITS','OOD','OOD','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Master Organization Definitions','N','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','HR_ORGANIZATION_UNITS',401,'HR_ALL_ORGANIZATION_UNITS','OODW','OODW','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Owning Organization Definitions','N','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MTL_LOT_NUMBERS',401,'MTL_LOT_NUMBERS','MLN','MLN','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Lot Number Definitions','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','HR_ORGANIZATION_UNITS',401,'HR_ALL_ORGANIZATION_UNITS','OODP','OOP','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Planning Organization Definitions','N','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MTL_SYSTEM_ITEMS_KFV',401,'MTL_SYSTEM_ITEMS_B','MSIV','MSIV','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Inventory Item Definition','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','CST_COST_GROUP_ACCOUNTS',401,'CST_COST_GROUP_ACCOUNTS','CCGA','CCGA','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Item Cost Group Accounts','','','','','CCGA','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','HR_ORGANIZATION_UNITS',401,'HR_ALL_ORGANIZATION_UNITS','HAOU','HAOU','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Organization Definitions','N','','','','','Y',' ',' ',' ',' ',' ');
--Inserting Object Component Joins for EIS_XXWC_INV_ONHAND_QUANTITY_V
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','PA_TASKS','MTV',401,'EXIOQV.MTV_TASK_ID','=','MTV.TASK_ID(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MTL_PARAMETERS','MP',401,'EXIOQV.MP_ORGANIZATION_ID','=','MP.ORGANIZATION_ID(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','CST_COST_GROUPS','CCG',401,'EXIOQV.CCG_COST_GROUP_ID','=','CCG.COST_GROUP_ID(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MTL_MATERIAL_STATUSES_VL','MMS',401,'EXIOQV.MMS_STATUS_ID','=','MMS.STATUS_ID(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MTL_ITEM_LOCATIONS_KFV','MIL',401,'EXIOQV.INVENTORY_LOCATION_ID','=','MIL.INVENTORY_LOCATION_ID(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MTL_ITEM_LOCATIONS_KFV','MIL',401,'EXIOQV.MIL_ORGANIZATION_ID','=','MIL.ORGANIZATION_ID(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','PJM_UNIT_NUMBERS','PUN',401,'EXIOQV.UNIT_NUMBER','=','PUN.UNIT_NUMBER(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MTL_SECONDARY_INVENTORIES','MSI',401,'EXIOQV.MSI_ORGANIZATION_ID','=','MSI.ORGANIZATION_ID(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MTL_SECONDARY_INVENTORIES','MSI',401,'EXIOQV.SECONDARY_INVENTORY_NAME','=','MSI.SECONDARY_INVENTORY_NAME(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','HR_ORGANIZATION_UNITS','OOD',401,'EXIOQV.OOD_ORGANIZATION_ID','=','OOD.ORGANIZATION_ID(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','HR_ORGANIZATION_UNITS','OODW',401,'EXIOQV.OODW_ORGANIZATION_ID','=','OODW.ORGANIZATION_ID(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MTL_LOT_NUMBERS','MLN',401,'EXIOQV.MLN_INVENTORY_ITEM_ID','=','MLN.INVENTORY_ITEM_ID(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MTL_LOT_NUMBERS','MLN',401,'EXIOQV.MLN_LOT_NUMBER','=','MLN.LOT_NUMBER(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MTL_LOT_NUMBERS','MLN',401,'EXIOQV.MLN_ORGANIZATION_ID','=','MLN.ORGANIZATION_ID(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','HR_ORGANIZATION_UNITS','OODP',401,'EXIOQV.OODP_ORGANIZATION_ID','=','OODP.ORGANIZATION_ID(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MTL_SYSTEM_ITEMS_KFV','MSIV',401,'EXIOQV.MSIV_INVENTORY_ITEM_ID','=','MSIV.INVENTORY_ITEM_ID(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','MTL_SYSTEM_ITEMS_KFV','MSIV',401,'EXIOQV.MSIV_ORGANIZATION_ID','=','MSIV.ORGANIZATION_ID(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','CST_COST_GROUP_ACCOUNTS','CCGA',401,'EXIOQV.CCGA_COST_GROUP_ID','=','CCGA.COST_GROUP_ID(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','CST_COST_GROUP_ACCOUNTS','CCGA',401,'EXIOQV.CCGA_ORGANIZATION_ID','=','CCGA.ORGANIZATION_ID(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_INV_ONHAND_QUANTITY_V','HR_ORGANIZATION_UNITS','HAOU',401,'EXIOQV.HAOU_ORGANIZATION_ID','=','HAOU.ORGANIZATION_ID(+)','','','','','XXEIS_RS_ADMIN');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 401');
END IF;
END;
/
prompt Creating Report LOV Data for Inventory - Onhand Quantity - WC
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(401);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - Inventory - Onhand Quantity - WC
xxeis.eis_rsc_ins.lov( 401,'select   ccg.cost_group
from cst_cost_groups ccg, cst_cost_group_accounts ccga
where ccg.cost_group_id = ccga.cost_group_id','','EIS_INV_COST_GROUP_LOV','List all the values for the cost group
','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 401,'SELECT   organization_code,organization_name
    FROM org_organization_definitions
   where operating_unit = fnd_profile.value (''ORG_ID'')
union
SELECT ''All Organizations'' organization_code, ''All'' organization_name  FROM dual','','EIS_INV_INVENTORY_ORGS_LOV','List of All Inventory Orgs under a given operating unit.','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 401,'SELECT DISTINCT concatenated_segments item, description FROM mtl_system_items_kfv msi,
                org_organization_definitions ood
          WHERE msi.organization_id = ood.organization_id
            AND ood.operating_unit = fnd_profile.VALUE (''ORG_ID'')
       ORDER BY concatenated_segments','','EIS_INV_ITEM_LOV','List of all inventory items.','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 401,'select SECONDARY_INVENTORY_NAME SUB_INVENTORY,HAOU.NAME ORGANIZATION_NAME
from MTL_SECONDARY_INVENTORIES MSI,
     HR_ALL_ORGANIZATION_UNITS HAOU
WHERE 1=1
AND MSI.ORGANIZATION_ID = HAOU.organization_id','','EIS_INV_SUBINVENTORY_LOV','List of All SubInventories.','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 401');
END IF;
END;
/
prompt Creating Report Data for Inventory - Onhand Quantity - WC
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(401);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Inventory - Onhand Quantity - WC
xxeis.eis_rsc_utility.delete_report_rows( 'Inventory - Onhand Quantity - WC' );
--Inserting Report - Inventory - Onhand Quantity - WC
xxeis.eis_rsc_ins.r( 401,'Inventory - Onhand Quantity - WC','','The Onhand Quantity Report displays the total quantity of an item in a subinventory.','','','','VP038429','EIS_XXWC_INV_ONHAND_QUANTITY_V','Y','','','VP038429','','Y','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N','','','','','','','','US','','','','');
--Inserting Report Columns - Inventory - Onhand Quantity - WC
xxeis.eis_rsc_ins.rc( 'Inventory - Onhand Quantity - WC',401,'INV_ORG_NAME','Inv Org Name','Translated name of the organization','','','default','','2','N','Y','','','','','','','VP038429','N','N','','EIS_XXWC_INV_ONHAND_QUANTITY_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory - Onhand Quantity - WC',401,'ITEM','Item','ITEM','','','default','','6','N','Y','','','','','','','VP038429','N','N','','EIS_XXWC_INV_ONHAND_QUANTITY_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory - Onhand Quantity - WC',401,'ON_HAND','On Hand','Transaction quantity in the primary unit of measure of the item','','~~~','default','','9','N','Y','','','','','','','VP038429','N','N','','EIS_XXWC_INV_ONHAND_QUANTITY_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory - Onhand Quantity - WC',401,'ORGANIZATION_CODE','Organization Code','Organization code','','','default','','3','N','Y','','','','','','','VP038429','N','N','','EIS_XXWC_INV_ONHAND_QUANTITY_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory - Onhand Quantity - WC',401,'SUBINVENTORY_CODE','Subinventory Code','Subinventory code','','','default','','4','N','Y','','','','','','','VP038429','N','N','','EIS_XXWC_INV_ONHAND_QUANTITY_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory - Onhand Quantity - WC',401,'UNIT_OF_MEASURE','Unit Of Measure','Unit of measure name','','','default','','16','N','Y','','','','','','','VP038429','N','N','','EIS_XXWC_INV_ONHAND_QUANTITY_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory - Onhand Quantity - WC',401,'MAX_MINMAX_QUANTITY','Max Qty','Maximum minmax order quantity','','~~~','default','','14','N','Y','','','','','','','VP038429','N','N','','EIS_XXWC_INV_ONHAND_QUANTITY_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory - Onhand Quantity - WC',401,'MIN_MINMAX_QUANTITY','Min Qty','Minimum minmax order quantity','','~~~','default','','13','N','Y','','','','','','','VP038429','N','N','','EIS_XXWC_INV_ONHAND_QUANTITY_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory - Onhand Quantity - WC',401,'Stock-Y/N','Stock-Y/N','Min Minmax Quantity','VARCHAR2','','default','','15','Y','Y','','','','','','(case when EXIOQV.SALES_VELOCITY IN (''N'',''Z'') Then ''N'' ELSE ''Y'' END)','VP038429','N','N','','EIS_XXWC_INV_ONHAND_QUANTITY_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory - Onhand Quantity - WC',401,'DESCRIPTION','Description','Description','','','default','','7','N','Y','','','','','','','VP038429','N','N','','EIS_XXWC_INV_ONHAND_QUANTITY_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory - Onhand Quantity - WC',401,'OPEN_DEMAND','Open Demand','Open Demand','','~~~','default','','10','N','Y','','','','','','','VP038429','N','N','','EIS_XXWC_INV_ONHAND_QUANTITY_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory - Onhand Quantity - WC',401,'AVERAGE_COST','Average Cost','Average Cost','','$~T~D~2','default','','8','','Y','','','','','','','VP038429','N','N','','EIS_XXWC_INV_ONHAND_QUANTITY_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory - Onhand Quantity - WC',401,'Qty_w/o_Demand','Qty w/o Demand','Qty w/o Demand','NUMBER','~~~','default','','11','Y','Y','','','','','','( nvl(EXIOQV.ON_HAND,0) + nvl(EXIOQV.OPEN_DEMAND,0))','VP038429','N','N','','EIS_XXWC_INV_ONHAND_QUANTITY_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory - Onhand Quantity - WC',401,'$_w/o_Demand','$ w/o Demand','$ w/o Demand','NUMBER','$~T~D~2','default','','12','Y','Y','','','','','','( EXIOQV.AVERAGE_COST * ( nvl(EXIOQV.ON_HAND,0) + nvl(EXIOQV.OPEN_DEMAND,0)))','VP038429','N','N','','EIS_XXWC_INV_ONHAND_QUANTITY_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory - Onhand Quantity - WC',401,'REGION','Region','Region','','','default','','1','','Y','','','','','','','VP038429','N','N','','EIS_XXWC_INV_ONHAND_QUANTITY_V','','','','US','');
--Inserting Report Parameters - Inventory - Onhand Quantity - WC
xxeis.eis_rsc_ins.rp( 'Inventory - Onhand Quantity - WC',401,'Organization','Inventory Organization','ORGANIZATION_CODE','IN','EIS_INV_INVENTORY_ORGS_LOV','','VARCHAR2','Y','Y','1','Y','N','CONSTANT','VP038429','Y','N','','','','EIS_XXWC_INV_ONHAND_QUANTITY_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Inventory - Onhand Quantity - WC',401,'Inventory Item','Inventory Item','ITEM','IN','EIS_INV_ITEM_LOV','','VARCHAR2','N','Y','2','Y','Y','CONSTANT','VP038429','Y','N','','','','EIS_XXWC_INV_ONHAND_QUANTITY_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Inventory - Onhand Quantity - WC',401,'Cost Group','Cost Group','COST_GROUP','IN','EIS_INV_COST_GROUP_LOV','','VARCHAR2','N','Y','4','Y','Y','CONSTANT','VP038429','Y','N','','','','EIS_XXWC_INV_ONHAND_QUANTITY_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Inventory - Onhand Quantity - WC',401,'Subinventory','Subinventory','SUBINVENTORY_CODE','IN','EIS_INV_SUBINVENTORY_LOV','','VARCHAR2','N','Y','3','Y','Y','CONSTANT','VP038429','Y','N','','','','EIS_XXWC_INV_ONHAND_QUANTITY_V','','','US','');
--Inserting Dependent Parameters - Inventory - Onhand Quantity - WC
--Inserting Report Conditions - Inventory - Onhand Quantity - WC
xxeis.eis_rsc_ins.rcnh( 'Inventory - Onhand Quantity - WC',401,'COST_GROUP IN :Cost Group ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','COST_GROUP','','Cost Group','','','','','EIS_XXWC_INV_ONHAND_QUANTITY_V','','','','','','IN','Y','Y','','','','','1',401,'Inventory - Onhand Quantity - WC','COST_GROUP IN :Cost Group ');
xxeis.eis_rsc_ins.rcnh( 'Inventory - Onhand Quantity - WC',401,'ITEM IN :Inventory Item ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','ITEM','','Inventory Item','','','','','EIS_XXWC_INV_ONHAND_QUANTITY_V','','','','','','IN','Y','Y','','','','','1',401,'Inventory - Onhand Quantity - WC','ITEM IN :Inventory Item ');
xxeis.eis_rsc_ins.rcnh( 'Inventory - Onhand Quantity - WC',401,'SUBINVENTORY_CODE IN :Subinventory ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','SUBINVENTORY_CODE','','Subinventory','','','','','EIS_XXWC_INV_ONHAND_QUANTITY_V','','','','','','IN','Y','Y','','','','','1',401,'Inventory - Onhand Quantity - WC','SUBINVENTORY_CODE IN :Subinventory ');
xxeis.eis_rsc_ins.rcnh( 'Inventory - Onhand Quantity - WC',401,'Free Text ','FREE_TEXT','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','','Y','','','','','and ( ''All Organizations'' in (:Organization) or (ORGANIZATION_CODE in (:Organization)))','1',401,'Inventory - Onhand Quantity - WC','Free Text ');
--Inserting Report Sorts - Inventory - Onhand Quantity - WC
--Inserting Report Triggers - Inventory - Onhand Quantity - WC
--inserting report templates - Inventory - Onhand Quantity - WC
xxeis.eis_rsc_ins.r_tem( 'Inventory - Onhand Quantity - WC','Inventory - Onhand Quantity_wc','Seeded template for Inventory - Onhand Quantity_wc','','','','','','','','','','','Inventory - Onhand Quantity_wc.rtf','VP038429','X','','','Y','Y','','');
--Inserting Report Portals - Inventory - Onhand Quantity - WC
--inserting report dashboards - Inventory - Onhand Quantity - WC
xxeis.eis_rsc_ins.R_dash( 'Inventory - Onhand Quantity - WC','Dynamic 702','Dynamic 702','pie','large','Unit Number','Unit Number','Subinventory Code','Subinventory Code','Count','VP038429');
xxeis.eis_rsc_ins.R_dash( 'Inventory - Onhand Quantity - WC','Dynamic 701','Dynamic 701','vertical percent bar','large','Cost Group','Cost Group','Cost Group','Cost Group','Count','VP038429');
xxeis.eis_rsc_ins.R_dash( 'Inventory - Onhand Quantity - WC','Dynamic 703','Dynamic 703','horizontal stacked bar','large','Unit Number','Unit Number','Subinventory Code','Subinventory Code','Count','VP038429');
xxeis.eis_rsc_ins.R_dash( 'Inventory - Onhand Quantity - WC','Dynamic 704','Dynamic 704','vertical stacked bar','large','Unit Number','Unit Number','Subinventory Code','Subinventory Code','Count','VP038429');
xxeis.eis_rsc_ins.R_dash( 'Inventory - Onhand Quantity - WC','Dynamic 705','Dynamic 705','vertical stacked bar','large','Subinventory Code','Subinventory Code','On Hand','On Hand','Count','VP038429');
xxeis.eis_rsc_ins.R_dash( 'Inventory - Onhand Quantity - WC','Dynamic 706','Dynamic 706','vertical stacked bar','large','Subinventory Code','Subinventory Code','On Hand','On Hand','Sum','VP038429');
xxeis.eis_rsc_ins.R_dash( 'Inventory - Onhand Quantity - WC','Dynamic 707','Dynamic 707','vertical stacked bar','large','Subinventory Code','Subinventory Code','Item','Item','Count','VP038429');
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Inventory - Onhand Quantity - WC','401','EIS_XXWC_INV_ONHAND_QUANTITY_V','EIS_XXWC_INV_ONHAND_QUANTITY_V','N','');
--inserting report security - Inventory - Onhand Quantity - WC
xxeis.eis_rsc_ins.rsec( 'Inventory - Onhand Quantity - WC','401','','XXWC_AO_BIN_MTN_REC',401,'VP038429','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory - Onhand Quantity - WC','401','','XXWC_AO_BIN_MTN_CYCLE_INV_ADJ',401,'VP038429','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory - Onhand Quantity - WC','401','','XXWC_AO_BIN_MTN_CYCLE_REC',401,'VP038429','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory - Onhand Quantity - WC','401','','XXWC_AO_INV_ADJ_REC',401,'VP038429','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory - Onhand Quantity - WC','660','','XXWC_AO_OEENTRY_REC',401,'VP038429','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory - Onhand Quantity - WC','401','','XXWC_INVENTORY_SUPER_USER',401,'VP038429','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory - Onhand Quantity - WC','401','','XXWC_INVENTORY_SPEC_SCC',401,'VP038429','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory - Onhand Quantity - WC','401','','XXWC_INV_PLANNER',401,'VP038429','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory - Onhand Quantity - WC','401','','XXWC_INVENTORY_CONTROL_INQUIRY',401,'VP038429','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory - Onhand Quantity - WC','401','','XXWC_INVENTORY_CONTROL_SR_MGR',401,'VP038429','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory - Onhand Quantity - WC','401','','INVENTORY',401,'VP038429','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory - Onhand Quantity - WC','20005','','XXWC_VIEW_ALL_EIS_REPORTS',401,'VP038429','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory - Onhand Quantity - WC','401','','XXWC_RECEIVING_ASSOCIATE',401,'VP038429','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory - Onhand Quantity - WC','401','','HDS_INVNTRY',401,'VP038429','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory - Onhand Quantity - WC','401','','XXWC_AO_REC',401,'VP038429','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory - Onhand Quantity - WC','707','','XXWC_COST_MANAGEMENT_INQ',401,'VP038429','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory - Onhand Quantity - WC','401','','XXWC_DATA_MNGT_SC',401,'VP038429','','','');
--Inserting Report Pivots - Inventory - Onhand Quantity - WC
xxeis.eis_rsc_ins.rpivot( 'Inventory - Onhand Quantity - WC',401,'Pivot','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rsc_ins.rpivot_dtls( 'Inventory - Onhand Quantity - WC',401,'Pivot','INV_ORG_NAME','ROW_FIELD','','','2','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Inventory - Onhand Quantity - WC',401,'Pivot','ITEM','ROW_FIELD','','','3','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Inventory - Onhand Quantity - WC',401,'Pivot','ON_HAND','DATA_FIELD','SUM','','1','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Inventory - Onhand Quantity - WC',401,'Pivot','ORGANIZATION_CODE','PAGE_FIELD','','','4','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Inventory - Onhand Quantity - WC',401,'Pivot','SUBINVENTORY_CODE','PAGE_FIELD','','','3','1','xlNormal');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
--Inserting Report   Version details- Inventory - Onhand Quantity - WC
xxeis.eis_rsc_ins.rv( 'Inventory - Onhand Quantity - WC','','Inventory - Onhand Quantity - WC','SA059956');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 401');
END IF;
END;
/
