--Report Name            : PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View XXEIS_ACCR_LOB_BU_V
prompt Table type Objects cannot be imported
set scan off define off
prompt Creating Object APPS.XXEIS_ACCR_LOB_BU_V
set scan off define off
DECLARE
mod_exist varchar2(1);
stmt CLOB;
v_objlength INTEGER := 0;
v_offset INTEGER := 1;
v_sql_long DBMS_SQL.varchar2s;
v_sql_long_count INTEGER := 1;
c NUMBER;
r NUMBER;
l_stmt varchar2(32000);
l_view_cur pls_integer;
l_view_rows NUMBER;
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(682);
IF mod_exist = 'Y' THEN 
dbms_lob.createtemporary(stmt,TRUE);
dbms_lob.write( lob_loc   => stmt,amount    => length('create or replace view APPS.XXEIS_ACCR_LOB_BU_V
 ("AGREEMENT_YEAR","MVID","VENDOR_NAME","LOB","BU","CAL_YEAR","CAL_MONTH","CALENDAR_PURCHASES","ACCRUAL_PURCHASES","REBATE","COOP","TOTAL_ACCRUALS"
) as '),offset    => 1, BUFFER    => 'create or replace view APPS.XXEIS_ACCR_LOB_BU_V
 ("AGREEMENT_YEAR","MVID","VENDOR_NAME","LOB","BU","CAL_YEAR","CAL_MONTH","CALENDAR_PURCHASES","ACCRUAL_PURCHASES","REBATE","COOP","TOTAL_ACCRUALS"
) as ');
l_stmt :=  'SELECT 
    AGREEMENT_YEAR,
    MVID,
    VENDOR_NAME,
    LOB,
    BU,
    CAL_YEAR ,
--    CALENDAR_MONTH_ID,
    CAL_MONTH,
    SUM(CALENDAR_PURCHASES)CALENDAR_PURCHASES,
    sum(ACCRUAL_PURCHASES) ACCRUAL_PURCHASES,
    SUM(REBATE) REBATE,
    SUM(COOP) COOP,
    SUM(TOTAL_ACCRUALS) TOTAL_ACCRUALS
FROM 
(
SELECT 
    AGREEMENT_YEAR,
    MVID,
    c.party_name VENDOR_NAME,
    LOB,
    BU,
    CALENDAR_YEAR CAL_YEAR,
--    calendar_month_id,
    DECODE(CALENDAR_MONTH_ID,1,''Jan'',2,''Feb'',3,''Mar'',4,''Apr'',''5'',''May'',6,''Jun'',7,''Jul'',8,''Aug'',9,''Sep'',10,''Oct'',11,''Nov'',12,''Dec'')||''-''|| CALENDAR_YEAR CAL_MONTH,
    0 CALENDAR_PURCHASES,
    sum(case when utilization_type=''ACCRUAL'' then PURCHASES else 0 end) ACCRUAL_PURCHASES,
    SUM(CASE WHEN REBATE_TYPE =''REBATE'' THEN ACCRUAL_AMOUNT ELSE 0 END) REBATE,
    SUM(CASE WHEN REBATE_TYPE =''COOP'' THEN ACCRUAL_AMOUNT ELSE 0 END) COOP,
    SUM(ACCRUAL_AMOUNT) TOTAL_ACCRUALS
FROM XXCUS.XXCUS_YTD_INCOME_B,
      XXCUS.XXCUS_REBATE_CUSTOMERS C
WHERE 1=1
  AND C.CUSTOMER_ID=OFU_CUST_ACCOUNT_ID
  AND C.PARTY_ATTRIBUTE1 =''HDS_MVID''

 GROUP BY
  AGREEMENT_YEAR,
    MVID,
    c.party_name,
    LOB,
    BU,
    CALENDAR_YEAR,
    CALENDAR_MONTH_ID
    
UNION 

SELECT 
       to_number(M.CALENDAR_YEAR) AGREEMENT_YEAR
      ,C.CUSTOMER_ATTRIBUTE2 MVID
      ,C.PARTY_NAME VENDOR_name    
      ,Z.PARTY_NAME LOB
      ,Z1.PARTY_NAME BU
      ,to_number(M.CALENDAR_YEAR) CAL_YEAR
      ,DECODE(SUBSTR(CALENDAR_PERIOD,6,2),1,''Jan'',2,''Feb'',3,''Mar'',4,''Apr'',''5'',''May'',6,''Jun'',7,''Jul'',8,''Aug'',9,''Sep'',10,''Oct'',11,''Nov'',12,''Dec'')||''-''||M.CALENDAR_YEAR CAL_MONTH
--      ,TO_number(SUBSTR(CALENDAR_PERIOD,6,2),99) CALENDAR_MONTH_ID
      ,M.TOTAL_PURCHASES CALENDAR_PURCHASES
      ,0 ACCRUAL_PURCHASES
      ,0 REBATE
      ,0 COOP
      ,0 TOTAL

 FROM  
        APPS.XXCUSOZF_PURCHASES_MV M
        ,XXCUS.XXCUS_REBATE_CUSTOMERS C
        ,APPS.HZ_PARTIES Z
        ,APPS.HZ_PARTIES Z1
         
WHERE 1=1
        AND M.LOB_ID=Z.PARTY_ID
        AND M.MVID=C.CUSTOMER_ID
        AND M.BU_ID=Z1.PARTY_ID
        AND GRP_MVID       = 0
        AND grp_branch     = 1
        AND GRP_QTR        = 1
        AND grp_year       = 1
        AND GRP_LOB        = 0
        AND  GRP_BU_ID       = 0
        AND grp_period     = 1
        AND GRP_CAL_YEAR   =0
        AND GRP_CAL_PERIOD = 0
        AND C.PARTY_ATTRIBUTE1=''HDS_MVID'' 
        AND Z.ATTRIBUTE1=''HDS_LOB''
        AND Z1.ATTRIBUTE1=''HDS_BU''
       
)

GROUP BY 
AGREEMENT_YEAR,
    MVID,
     VENDOR_NAME,
    LOB,
    BU,
    CAL_YEAR ,
--    CALENDAR_MONTH_ID,
    CAL_MONTH

';
dbms_lob.APPEND( dest_lob  => stmt,      src_lob   => l_stmt );
v_objlength := DBMS_LOB.getlength (stmt);
IF v_objlength <= 32000 THEN 
EXECUTE IMMEDIATE to_char(stmt);
ELSE
v_offset := 1; 
WHILE v_offset <= v_objlength
LOOP
-- each record is 256 bytes from the LOB
v_sql_long (v_sql_long_count) := DBMS_LOB.SUBSTR (stmt, 256, v_offset);
-- increment the record count
v_sql_long_count := v_sql_long_count + 1;
-- figure out the new offset
v_offset := v_offset + 256;
END LOOP;
BEGIN
  -- open cursor
 c := DBMS_SQL.open_cursor;
 -- parse VARCHAR2S table
 DBMS_SQL.parse (c, v_sql_long, 1, v_sql_long_count - 1, FALSE, 1);
 -- Execute the cursor
r := DBMS_SQL.EXECUTE (c);
-- Close the cursor
 DBMS_SQL.close_cursor (c);
 END;
END IF;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 682');
END IF;
END;
/
prompt Creating Object Data XXEIS_ACCR_LOB_BU_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(682);
IF mod_exist = 'Y' THEN 
--Inserting Object XXEIS_ACCR_LOB_BU_V
xxeis.eis_rsc_ins.v( 'XXEIS_ACCR_LOB_BU_V',682,'Paste SQL View for PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU','','','','DV003828','APPS','PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU View','X1KV1','','Y','VIEW','US','','');
--Delete Object Columns for XXEIS_ACCR_LOB_BU_V
xxeis.eis_rsc_utility.delete_view_rows('XXEIS_ACCR_LOB_BU_V',682,FALSE);
--Inserting Object Columns for XXEIS_ACCR_LOB_BU_V
xxeis.eis_rsc_ins.vc( 'XXEIS_ACCR_LOB_BU_V','MVID',682,'','MVID','','','','DV003828','VARCHAR2','','','Mvid','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_ACCR_LOB_BU_V','LOB',682,'','LOB','','','','DV003828','VARCHAR2','','','Lob','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_ACCR_LOB_BU_V','BU',682,'','BU','','','','DV003828','VARCHAR2','','','Bu','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_ACCR_LOB_BU_V','CAL_MONTH',682,'','CAL_MONTH','','','','DV003828','VARCHAR2','','','Cal Month','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_ACCR_LOB_BU_V','REBATE',682,'','REBATE','','','','DV003828','NUMBER','','','Rebate','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_ACCR_LOB_BU_V','COOP',682,'','COOP','','','','DV003828','NUMBER','','','Coop','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_ACCR_LOB_BU_V','TOTAL_ACCRUALS',682,'','TOTAL_ACCRUALS','','~T~D~2','','DV003828','NUMBER','','','Total Accruals','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_ACCR_LOB_BU_V','AGREEMENT_YEAR',682,'','','','','','DV003828','NUMBER','','','Agreement Year','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_ACCR_LOB_BU_V','CAL_YEAR',682,'','','','','','DV003828','NUMBER','','','Cal Year','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_ACCR_LOB_BU_V','VENDOR_NAME',682,'','','','','','DV003828','VARCHAR2','','','Vendor Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_ACCR_LOB_BU_V','CALENDAR_PURCHASES',682,'','','','','','DV003828','NUMBER','','','Calendar Purchases','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_ACCR_LOB_BU_V','ACCRUAL_PURCHASES',682,'','','','','','DV003828','NUMBER','','','Accrual Purchases','','','','US');
--Inserting Object Components for XXEIS_ACCR_LOB_BU_V
--Inserting Object Component Joins for XXEIS_ACCR_LOB_BU_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 682');
END IF;
END;
/
prompt Creating Report LOV Data for PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(682);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU
xxeis.eis_rsc_ins.lov( 682,'select distinct party_name
from ar.HZ_PARTIES
where ATTRIBUTE1 = ''HDS_LOB''
and party_name not in (''PLUMBING null'', ''INDUSTRIAL PVF'')','','HDS LOB_NAME','LOV party_name From ar.HZ_PARTIES is the LOB','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 682,'SELECT distinct hp.party_name LOB
 FROM apps.hz_parties hp
WHERE  hp.attribute1=''HDS_BU''
and ( hp.party_name in (''ELECTRICAL'',''UTILISERV'')
      or hp.status =''A'')','','HDS BU','BU FROM apps.hz_parties it''s party_name = LOB BUSINESS UNIT','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 682,'SELECT customer_attribute2 MVID
  FROM XXCUS.xxcus_rebate_customers
  WHERE 1=1
  AND party_attribute1=''HDS_MVID''
  order by 1','','MVID','MVID number from XXCUS.XXCUS_REBATE_CUSTOMERS','DV003828',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 682,'SELECT DISTINCT GPS.PERIOD_YEAR
FROM gl_period_statuses gps
ORDER BY GPS.PERIOD_YEAR','','XXWC OZF Calendar Year LOV','','SA059956',NULL,'N','','','N','N','','','US');
xxeis.eis_rsc_ins.lov( 682,'select per.period_name,per.period_year
from gl_periods per
order by 2 desc','','XXWC OZF Period Name LOV','','SA059956',NULL,'N','','','N','N','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 682');
END IF;
END;
/
prompt Creating Report Data for PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(682);
IF mod_exist = 'Y' THEN 
--Deleting Report data - PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU
xxeis.eis_rsc_utility.delete_report_rows( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU' );
--Inserting Report - PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU
xxeis.eis_rsc_ins.r( 682,'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU','','Spend and Accruals by Vendor, LOB, BU, Calendar Year, Calendar Month
Objects used : XXCUS.XXCUS_YTD_INCOME_B, XXCUSOZF_PURCHASES_MV"','','','','MC027824','XXEIS_ACCR_LOB_BU_V','Y','','SELECT 
    AGREEMENT_YEAR,
    MVID,
    VENDOR_NAME,
    LOB,
    BU,
    CAL_YEAR ,
--    CALENDAR_MONTH_ID,
    CAL_MONTH,
    SUM(CALENDAR_PURCHASES)CALENDAR_PURCHASES,
    sum(ACCRUAL_PURCHASES) ACCRUAL_PURCHASES,
    SUM(REBATE) REBATE,
    SUM(COOP) COOP,
    SUM(TOTAL_ACCRUALS) TOTAL_ACCRUALS
FROM 
(
SELECT 
    AGREEMENT_YEAR,
    MVID,
    c.party_name VENDOR_NAME,
    LOB,
    BU,
    CALENDAR_YEAR CAL_YEAR,
--    calendar_month_id,
    DECODE(CALENDAR_MONTH_ID,1,''Jan'',2,''Feb'',3,''Mar'',4,''Apr'',''5'',''May'',6,''Jun'',7,''Jul'',8,''Aug'',9,''Sep'',10,''Oct'',11,''Nov'',12,''Dec'')||''-''|| CALENDAR_YEAR CAL_MONTH,
    0 CALENDAR_PURCHASES,
    sum(case when utilization_type=''ACCRUAL'' then PURCHASES else 0 end) ACCRUAL_PURCHASES,
    SUM(CASE WHEN REBATE_TYPE =''REBATE'' THEN ACCRUAL_AMOUNT ELSE 0 END) REBATE,
    SUM(CASE WHEN REBATE_TYPE =''COOP'' THEN ACCRUAL_AMOUNT ELSE 0 END) COOP,
    SUM(ACCRUAL_AMOUNT) TOTAL_ACCRUALS
FROM XXCUS.XXCUS_YTD_INCOME_B,
      XXCUS.XXCUS_REBATE_CUSTOMERS C
WHERE 1=1
  AND C.CUSTOMER_ID=OFU_CUST_ACCOUNT_ID
  AND C.PARTY_ATTRIBUTE1 =''HDS_MVID''

 GROUP BY
  AGREEMENT_YEAR,
    MVID,
    c.party_name,
    LOB,
    BU,
    CALENDAR_YEAR,
    CALENDAR_MONTH_ID
    
UNION 

SELECT 
       to_number(M.CALENDAR_YEAR) AGREEMENT_YEAR
      ,C.CUSTOMER_ATTRIBUTE2 MVID
      ,C.PARTY_NAME VENDOR_name    
      ,Z.PARTY_NAME LOB
      ,Z1.PARTY_NAME BU
      ,to_number(M.CALENDAR_YEAR) CAL_YEAR
      ,DECODE(SUBSTR(CALENDAR_PERIOD,6,2),1,''Jan'',2,''Feb'',3,''Mar'',4,''Apr'',''5'',''May'',6,''Jun'',7,''Jul'',8,''Aug'',9,''Sep'',10,''Oct'',11,''Nov'',12,''Dec'')||''-''||M.CALENDAR_YEAR CAL_MONTH
--      ,TO_number(SUBSTR(CALENDAR_PERIOD,6,2),99) CALENDAR_MONTH_ID
      ,M.TOTAL_PURCHASES CALENDAR_PURCHASES
      ,0 ACCRUAL_PURCHASES
      ,0 REBATE
      ,0 COOP
      ,0 TOTAL

 FROM  
        APPS.XXCUSOZF_PURCHASES_MV M
        ,XXCUS.XXCUS_REBATE_CUSTOMERS C
        ,APPS.HZ_PARTIES Z
        ,APPS.HZ_PARTIES Z1
         
WHERE 1=1
        AND M.LOB_ID=Z.PARTY_ID
        AND M.MVID=C.CUSTOMER_ID
        AND M.BU_ID=Z1.PARTY_ID
        AND GRP_MVID       = 0
        AND grp_branch     = 1
        AND GRP_QTR        = 1
        AND grp_year       = 1
        AND GRP_LOB        = 0
        AND  GRP_BU_ID       = 0
        AND grp_period     = 1
        AND GRP_CAL_YEAR   =0
        AND GRP_CAL_PERIOD = 0
        AND C.PARTY_ATTRIBUTE1=''HDS_MVID'' 
        AND Z.ATTRIBUTE1=''HDS_LOB''
        AND Z1.ATTRIBUTE1=''HDS_BU''
       
)

GROUP BY 
AGREEMENT_YEAR,
    MVID,
     VENDOR_NAME,
    LOB,
    BU,
    CAL_YEAR ,
--    CALENDAR_MONTH_ID,
    CAL_MONTH
','MC027824','','N','ADHOC - SUMMARY ACCRUAL DETAILS','RTF,PDF,','CSV,EXCEL,Pivot Excel,','N','','','','','','N','APPS','US','','','','');
--Inserting Report Columns - PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU
xxeis.eis_rsc_ins.rc( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU',682,'CAL_MONTH','Cal Month','','','','default','','7','N','','','','','','','','MC027824','N','N','','XXEIS_ACCR_LOB_BU_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU',682,'MVID','Mvid','','','','default','','2','N','','','','','','','','MC027824','N','N','','XXEIS_ACCR_LOB_BU_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU',682,'TOTAL_ACCRUALS','Total Accruals','','','~,~.~2','default','','11','N','','','','','','','','MC027824','N','N','','XXEIS_ACCR_LOB_BU_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU',682,'COOP','Coop','','','~,~.~2','default','','10','N','','','','','','','','MC027824','N','N','','XXEIS_ACCR_LOB_BU_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU',682,'BU','Bu','','','','default','','5','N','','','','','','','','MC027824','N','N','','XXEIS_ACCR_LOB_BU_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU',682,'LOB','Lob','','','','default','','4','N','','','','','','','','MC027824','N','N','','XXEIS_ACCR_LOB_BU_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU',682,'REBATE','Rebate','','','~,~.~2','default','','9','N','','','','','','','','MC027824','N','N','','XXEIS_ACCR_LOB_BU_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU',682,'AGREEMENT_YEAR','Agreement Year','','','','default','','1','N','','','','','','','','MC027824','N','N','','XXEIS_ACCR_LOB_BU_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU',682,'CAL_YEAR','Cal Year','','','','default','','6','N','','','','','','','','MC027824','N','N','','XXEIS_ACCR_LOB_BU_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU',682,'VENDOR_NAME','Vendor Name','','','','default','','3','N','','','','','','','','MC027824','N','N','','XXEIS_ACCR_LOB_BU_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU',682,'CALENDAR_PURCHASES','Calendar Purchases','','','~.~,~2','default','','8','N','','','','','','','','MC027824','N','N','','XXEIS_ACCR_LOB_BU_V','','','SUM','US','');
--Inserting Report Parameters - PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU
xxeis.eis_rsc_ins.rp( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU',682,'Lob','','LOB','IN','HDS LOB_NAME','','VARCHAR2','N','Y','3','Y','Y','CONSTANT','MC027824','Y','N','','','','XXEIS_ACCR_LOB_BU_V','','','US','');
xxeis.eis_rsc_ins.rp( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU',682,'Mvid','','MVID','IN','MVID','','VARCHAR2','N','Y','5','Y','Y','CONSTANT','MC027824','Y','N','','','','XXEIS_ACCR_LOB_BU_V','','','US','');
xxeis.eis_rsc_ins.rp( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU',682,'Cal Month','','CAL_MONTH','IN','XXWC OZF Period Name LOV','','VARCHAR2','N','Y','2','Y','Y','CONSTANT','MC027824','Y','N','','','','XXEIS_ACCR_LOB_BU_V','','','US','');
xxeis.eis_rsc_ins.rp( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU',682,'Calendar Year','','CAL_YEAR','IN','XXWC OZF Calendar Year LOV','','VARCHAR2','Y','Y','1','Y','Y','CONSTANT','MC027824','Y','N','','','','XXEIS_ACCR_LOB_BU_V','','','US','');
xxeis.eis_rsc_ins.rp( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU',682,'Agreement Year','','AGREEMENT_YEAR','IN','XXWC OZF Calendar Year LOV','','VARCHAR2','N','Y','6','Y','Y','CONSTANT','MC027824','Y','N','','','','XXEIS_ACCR_LOB_BU_V','','','US','');
xxeis.eis_rsc_ins.rp( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU',682,'Bu','','BU','IN','HDS BU','','VARCHAR2','N','Y','4','Y','Y','CONSTANT','MC027824','Y','N','','','','XXEIS_ACCR_LOB_BU_V','','','US','');
--Inserting Dependent Parameters - PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU
xxeis.eis_rsc_ins.rdp( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU',682,'PERIOD_YEAR','Cal Month','Calendar Year','IN','N','');
--Inserting Report Conditions - PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU
xxeis.eis_rsc_ins.rcnh( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU',682,'AGREEMENT_YEAR IN :Agreement Year ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','AGREEMENT_YEAR','','Agreement Year','','','','','XXEIS_ACCR_LOB_BU_V','','','','','','IN','Y','Y','','','','','1',682,'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU','AGREEMENT_YEAR IN :Agreement Year ');
xxeis.eis_rsc_ins.rcnh( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU',682,'BU IN :Bu ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','BU','','Bu','','','','','XXEIS_ACCR_LOB_BU_V','','','','','','IN','Y','Y','','','','','1',682,'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU','BU IN :Bu ');
xxeis.eis_rsc_ins.rcnh( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU',682,'CAL_MONTH IN :Cal Month ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CAL_MONTH','','Cal Month','','','','','XXEIS_ACCR_LOB_BU_V','','','','','','IN','Y','Y','','','','','1',682,'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU','CAL_MONTH IN :Cal Month ');
xxeis.eis_rsc_ins.rcnh( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU',682,'CAL_YEAR IN :Calendar Year ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CAL_YEAR','','Calendar Year','','','','','XXEIS_ACCR_LOB_BU_V','','','','','','IN','Y','Y','','','','','1',682,'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU','CAL_YEAR IN :Calendar Year ');
xxeis.eis_rsc_ins.rcnh( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU',682,'LOB IN :Lob ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','LOB','','Lob','','','','','XXEIS_ACCR_LOB_BU_V','','','','','','IN','Y','Y','','','','','1',682,'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU','LOB IN :Lob ');
xxeis.eis_rsc_ins.rcnh( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU',682,'MVID IN :Mvid ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','MVID','','Mvid','','','','','XXEIS_ACCR_LOB_BU_V','','','','','','IN','Y','Y','','','','','1',682,'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU','MVID IN :Mvid ');
--Inserting Report Sorts - PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU
xxeis.eis_rsc_ins.rs( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU',682,'MVID','ASC','MC027824','2','');
xxeis.eis_rsc_ins.rs( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU',682,'LOB','ASC','MC027824','3','');
xxeis.eis_rsc_ins.rs( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU',682,'BU','ASC','MC027824','4','');
xxeis.eis_rsc_ins.rs( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU',682,'CAL_YEAR','ASC','MC027824','5','');
xxeis.eis_rsc_ins.rs( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU',682,'CAL_MONTH','ASC','MC027824','6','');
xxeis.eis_rsc_ins.rs( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU',682,'AGREEMENT_YEAR','ASC','MC027824','1','');
--Inserting Report Triggers - PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU
--inserting report templates - PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU
--Inserting Report Portals - PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU
--inserting report dashboards - PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU','682','XXEIS_ACCR_LOB_BU_V','XXEIS_ACCR_LOB_BU_V','N','');
--inserting report security - PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU
xxeis.eis_rsc_ins.rsec( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU','682','','XXCUS_TM_USER',682,'MC027824','','','');
xxeis.eis_rsc_ins.rsec( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU','682','','XXCUS_TM_ADMIN_USER',682,'MC027824','','','');
xxeis.eis_rsc_ins.rsec( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU','682','','XXCUS_TM_FORMS_RESP',682,'MC027824','','','');
xxeis.eis_rsc_ins.rsec( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU','682','','XXCUS_TM_ADM_FORMS_RESP',682,'MC027824','','','');
xxeis.eis_rsc_ins.rsec( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU','','TF046370','',682,'MC027824','','','');
xxeis.eis_rsc_ins.rsec( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU','','10022763','',682,'MC027824','','','');
xxeis.eis_rsc_ins.rsec( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU','','SG019472','',682,'MC027824','','','');
xxeis.eis_rsc_ins.rsec( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU','','KM026443','',682,'MC027824','','','');
xxeis.eis_rsc_ins.rsec( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU','','JG025875','',682,'MC027824','','','');
xxeis.eis_rsc_ins.rsec( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU','','LN053976','',682,'MC027824','','','');
xxeis.eis_rsc_ins.rsec( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU','','PP018915','',682,'MC027824','','','');
xxeis.eis_rsc_ins.rsec( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU','222','','XXWC_RECEIVABLES_INQUIRY_WC',682,'MC027824','','','');
xxeis.eis_rsc_ins.rsec( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU','682','','XXCUS_HDS_RB_REPORTS',682,'MC027824','','','');
xxeis.eis_rsc_ins.rsec( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU','20003','','XXCUS_HDS_RB_INQUIRY',682,'MC027824','','','');
--Inserting Report Pivots - PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU
--Inserting Report   Version details- PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU
xxeis.eis_rsc_ins.rv( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU','','PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU','SA059956');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 682');
END IF;
END;
/
