--Report Name            : Lost Quotes Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View EIS_XXWC_OM_LOST_QUOTES_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_OM_LOST_QUOTES_V
Create or replace View XXEIS.EIS_XXWC_OM_LOST_QUOTES_V
(LOCATION,CUSTOMER_NAME,CUSTOMER_NUMBER,QUOTE_DRAFT_DATE,QUOTE_REJECTED_DATE,QUOTE_NUMBER,REASON_CODE,SALES_REP,CUSTOMER_JOB_NAME,TOTAL_LINES,SUB_TOTAL,COST_TOTAL) AS 
SELECT mp.organization_code location,
    party.party_name customer_name,
    cust_acct.account_number customer_number,
    TRUNC(oh.quote_date) quote_draft_date,
    TRUNC(oer.creation_date) quote_rejected_date,
    oh.quote_number,
    oer.reason reason_code,
    REP.NAME Sales_Rep,
    hcsu.location customer_job_name,
    COUNT(ol.line_id) Total_lines,
    SUM(DECODE(ol.line_category_code,'RETURN',(ol.ordered_quantity*-1),ol.ordered_quantity)*NVL(OL.UNIT_SELLING_PRICE,0)) sub_total,
    SUM(DECODE(ol.line_category_code,'RETURN',(ol.ordered_quantity*-1),ol.ordered_quantity)* DECODE(NVL(apps.xxwc_mv_routines_pkg.get_order_line_cost (ol.line_id), 0),0,ol.unit_cost,apps.xxwc_mv_routines_pkg.get_order_line_cost (ol.line_id))) cost_total
  FROM Hz_Parties Party,
    Hz_Cust_Accounts Cust_Acct,
    Oe_Order_Headers Oh,
    Oe_Order_Lines Ol,
    Oe_Reasons_v Oer,
    Mtl_Parameters Mp,
    Ra_Salesreps Rep ,
    hz_cust_site_uses hcsu,
    hz_cust_acct_sites hcas,
    HZ_PARTY_SITES hps,
    hz_locations hl
  WHERE ol.header_id            = oh.header_id
  AND cust_acct.party_id        = party.party_id
  AND oh.sold_to_org_id         = cust_acct.cust_account_id
  AND oer.entity_id             = oh.header_id
  AND oer.entity_code           = 'HEADER'
  AND mp.organization_id        = oh.ship_from_org_id
  AND oh.ship_to_org_id         = hcsu.site_use_id
  AND hcsu.cust_acct_site_id    = hcas.cust_acct_site_id
  AND hcas.party_site_id        = hps.party_site_id
  AND hl.location_id            = hps.location_id
  AND OH.SALESREP_ID            = REP.SALESREP_ID(+)
  AND oh.org_id                 = rep.org_id(+)
  AND TRUNC(oer.creation_date) IS NOT NULL
  AND oh.quote_number          IS NOT NULL
  AND OH.TRANSACTION_PHASE_CODE ='N'
    -- AND  TRUNC(oh.quote_date) >= '28-NOV-2012' and TRUNC(oh.quote_date) <= '28-NOV-2012'
  GROUP BY mp.organization_code,
    party.party_name ,
    cust_acct.account_number ,
    TRUNC(oh.quote_date) ,
    TRUNC(oer.creation_date) ,
    oh.quote_number,
    oer.reason ,
    REP.NAME ,
    hcsu.location
/
set scan on define on
prompt Creating View Data for Lost Quotes Report
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_OM_LOST_QUOTES_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_OM_LOST_QUOTES_V',660,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Xxwc Om Lost Quotes V','EXOLQV','','');
--Delete View Columns for EIS_XXWC_OM_LOST_QUOTES_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_OM_LOST_QUOTES_V',660,FALSE);
--Inserting View Columns for EIS_XXWC_OM_LOST_QUOTES_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_LOST_QUOTES_V','COST_TOTAL',660,'Cost Total','COST_TOTAL','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Cost Total','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_LOST_QUOTES_V','SUB_TOTAL',660,'Sub Total','SUB_TOTAL','','','','XXEIS_RS_ADMIN','NUMBER','','','Sub Total','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_LOST_QUOTES_V','QUOTE_NUMBER',660,'Quote Number','QUOTE_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Quote Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_LOST_QUOTES_V','REASON_CODE',660,'Reason Code','REASON_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Reason Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_LOST_QUOTES_V','CUSTOMER_NUMBER',660,'Customer Number','CUSTOMER_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_LOST_QUOTES_V','CUSTOMER_NAME',660,'Customer Name','CUSTOMER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_LOST_QUOTES_V','LOCATION',660,'Location','LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Location','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_LOST_QUOTES_V','QUOTE_DRAFT_DATE',660,'Quote Draft Date','QUOTE_DRAFT_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Quote Draft Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_LOST_QUOTES_V','QUOTE_REJECTED_DATE',660,'Quote Rejected Date','QUOTE_REJECTED_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Quote Rejected Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_LOST_QUOTES_V','CUSTOMER_JOB_NAME',660,'Customer Job Name','CUSTOMER_JOB_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Job Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_LOST_QUOTES_V','SALES_REP',660,'Sales Rep','SALES_REP','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Sales Rep','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_LOST_QUOTES_V','TOTAL_LINES',660,'Total Lines','TOTAL_LINES','','','','XXEIS_RS_ADMIN','NUMBER','','','Total Lines','','','');
--Inserting View Components for EIS_XXWC_OM_LOST_QUOTES_V
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_LOST_QUOTES_V','HZ_PARTIES',660,'HZ_PARTIES','PARTY','PARTY','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Information About Parties Such As Organizations, P','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_LOST_QUOTES_V','HZ_CUST_ACCOUNTS',660,'HZ_CUST_ACCOUNTS','CUST_ACCT','CUST_ACCT','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Stores Information About Customer Accounts.','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_LOST_QUOTES_V','OE_ORDER_HEADERS_ALL',660,'OE_ORDER_HEADERS_ALL','OH','OH','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Oe Order Headers All Stores Header Information For','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_LOST_QUOTES_V','OE_ORDER_LINES_ALL',660,'OE_ORDER_LINES_ALL','OL','OL','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Oe Order Lines All Stores Information For All Orde','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_LOST_QUOTES_V','MTL_PARAMETERS',660,'MTL_PARAMETERS','MP','MP','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Inventory Control Options And Defaults','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_LOST_QUOTES_V','MTL_SYSTEM_ITEMS_KFV',660,'MTL_SYSTEM_ITEMS_B','MSI','MSI','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Inventory Item Definitions','','','','');
--Inserting View Component Joins for EIS_XXWC_OM_LOST_QUOTES_V
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_LOST_QUOTES_V','HZ_PARTIES','PARTY',660,'EXOLQV.PARTY_ID','=','PARTY.PARTY_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_LOST_QUOTES_V','HZ_CUST_ACCOUNTS','CUST_ACCT',660,'EXOLQV.CUST_ACCOUNT_ID','=','CUST_ACCT.CUST_ACCOUNT_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_LOST_QUOTES_V','OE_ORDER_HEADERS_ALL','OH',660,'EXOLQV.HEADER_ID','=','OH.HEADER_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_LOST_QUOTES_V','OE_ORDER_LINES_ALL','OL',660,'EXOLQV.LINE_ID','=','OL.LINE_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_LOST_QUOTES_V','MTL_PARAMETERS','MP',660,'EXOLQV.ORGANIZATION_ID','=','MP.ORGANIZATION_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_LOST_QUOTES_V','MTL_SYSTEM_ITEMS_KFV','MSI',660,'EXOLQV.INVENTORY_ITEM_ID','=','MSI.INVENTORY_ITEM_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_LOST_QUOTES_V','MTL_SYSTEM_ITEMS_KFV','MSI',660,'EXOLQV.MSI_ORGANIZATION_ID','=','MSI.ORGANIZATION_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
END;
/
set scan on define on
prompt Creating Report LOV Data for Lost Quotes Report
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Lost Quotes Report
xxeis.eis_rs_ins.lov( 660,'select nvl( party.party_name,CUST_ACCT.account_name) customer_name,CUST_ACCT.ACCOUNT_NUMBER
from  HZ_PARTIES PARTY, HZ_CUST_ACCOUNTS CUST_ACCT where CUST_ACCT.PARTY_ID = PARTY.PARTY_ID AND  PARTY.status=''A''
','','OM CUSTOMER NAME','This gives the Customer Name','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'SELECT ORGANIZATION_CODE WAREHOUSE,ORGANIZATION_NAME FROM ORG_ORGANIZATION_DEFINITIONS OOD WHERE SYSDATE < NVL(OOD.DISABLE_DATE,SYSDATE+1) AND EXISTS (SELECT 1 FROM XXEIS.EIS_ORG_ACCESS_V  WHERE organization_id = ood.organization_id )','','OM WAREHOUSE','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,' select distinct REASON_CODE from OE_REASONS','','OM QUOTE REASON CODES','','ANONYMOUS',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Lost Quotes Report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Lost Quotes Report
xxeis.eis_rs_utility.delete_report_rows( 'Lost Quotes Report' );
--Inserting Report - Lost Quotes Report
xxeis.eis_rs_ins.r( 660,'Lost Quotes Report','','The purpose of this report is to list customer rejected quotes along with the respective reason code.','','','','XXEIS_RS_ADMIN','EIS_XXWC_OM_LOST_QUOTES_V','Y','','','XXEIS_RS_ADMIN','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - Lost Quotes Report
xxeis.eis_rs_ins.rc( 'Lost Quotes Report',660,'COST_TOTAL','Cost Total','Cost Total','','~T~D~2','default','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_LOST_QUOTES_V','','');
xxeis.eis_rs_ins.rc( 'Lost Quotes Report',660,'CUSTOMER_NAME','Customer Name','Customer Name','','','default','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_LOST_QUOTES_V','','');
xxeis.eis_rs_ins.rc( 'Lost Quotes Report',660,'CUSTOMER_NUMBER','Customer Number','Customer Number','','','default','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_LOST_QUOTES_V','','');
xxeis.eis_rs_ins.rc( 'Lost Quotes Report',660,'LOCATION','Location','Location','','','default','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_LOST_QUOTES_V','','');
xxeis.eis_rs_ins.rc( 'Lost Quotes Report',660,'REASON_CODE','Reason','Reason Code','','','default','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_LOST_QUOTES_V','','');
xxeis.eis_rs_ins.rc( 'Lost Quotes Report',660,'SUB_TOTAL','Subtotal','Sub Total','','~T~D~2','default','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_LOST_QUOTES_V','','');
xxeis.eis_rs_ins.rc( 'Lost Quotes Report',660,'GM_PERCENT','Gm%','Sub Total','NUMBER','~T~D~2','default','','12','Y','','','','','','','case when EXOLQV.SUB_TOTAL  > 0 then (((EXOLQV.SUB_TOTAL - EXOLQV.COST_TOTAL) / EXOLQV.SUB_TOTAL)*100) else 0 end','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_LOST_QUOTES_V','','');
xxeis.eis_rs_ins.rc( 'Lost Quotes Report',660,'QUOTE_DRAFT_DATE','Quote Draft Date','Quote Draft Date','','','default','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_LOST_QUOTES_V','','');
xxeis.eis_rs_ins.rc( 'Lost Quotes Report',660,'QUOTE_REJECTED_DATE','Quote Rejected Date','Quote Rejected Date','','','default','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_LOST_QUOTES_V','','');
xxeis.eis_rs_ins.rc( 'Lost Quotes Report',660,'QUOTE_NUM','Quote Number','Quote Rejected Date','VARCHAR2','','default','','7','Y','','','','','','','to_char(EXOLQV.QUOTE_NUMBER)','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_LOST_QUOTES_V','','');
xxeis.eis_rs_ins.rc( 'Lost Quotes Report',660,'CUSTOMER_JOB_NAME','Job Site Name','Customer Job Name','','','default','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_LOST_QUOTES_V','','');
xxeis.eis_rs_ins.rc( 'Lost Quotes Report',660,'SALES_REP','Sales Person','Sales Rep','','','default','','13','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_LOST_QUOTES_V','','');
xxeis.eis_rs_ins.rc( 'Lost Quotes Report',660,'TOTAL_LINES','Number of Lines','Total Lines','','~~~','default','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_LOST_QUOTES_V','','');
--Inserting Report Parameters - Lost Quotes Report
xxeis.eis_rs_ins.rp( 'Lost Quotes Report',660,'Branch Number','Branch Number','LOCATION','IN','OM WAREHOUSE','','VARCHAR2','N','Y','3','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Lost Quotes Report',660,'Reason Code','Reason Code','REASON_CODE','IN','OM QUOTE REASON CODES','','VARCHAR2','N','Y','4','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Lost Quotes Report',660,'Customer Name','Customer Name','CUSTOMER_NAME','IN','OM CUSTOMER NAME','','VARCHAR2','N','Y','5','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Lost Quotes Report',660,'Date From','Date From','QUOTE_DRAFT_DATE','>=','','','DATE','Y','Y','1','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Lost Quotes Report',660,'Date To','Date To','QUOTE_DRAFT_DATE','<=','','','DATE','Y','Y','2','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
--Inserting Report Conditions - Lost Quotes Report
xxeis.eis_rs_ins.rcn( 'Lost Quotes Report',660,'LOCATION','IN',':Branch Number','','','Y','3','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Lost Quotes Report',660,'REASON_CODE','IN',':Reason Code','','','Y','4','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Lost Quotes Report',660,'CUSTOMER_NAME','IN',':Customer Name','','','Y','5','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Lost Quotes Report',660,'QUOTE_DRAFT_DATE','>=',':Date From','','','Y','1','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Lost Quotes Report',660,'QUOTE_DRAFT_DATE','<=',':Date To','','','Y','2','Y','XXEIS_RS_ADMIN');
--Inserting Report Sorts - Lost Quotes Report
xxeis.eis_rs_ins.rs( 'Lost Quotes Report',660,'CUSTOMER_NAME','ASC','XXEIS_RS_ADMIN','','');
--Inserting Report Triggers - Lost Quotes Report
--Inserting Report Templates - Lost Quotes Report
--Inserting Report Portals - Lost Quotes Report
--Inserting Report Dashboards - Lost Quotes Report
--Inserting Report Security - Lost Quotes Report
xxeis.eis_rs_ins.rsec( 'Lost Quotes Report','660','','50926',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Lost Quotes Report','660','','50927',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Lost Quotes Report','660','','50928',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Lost Quotes Report','660','','50929',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Lost Quotes Report','660','','50931',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Lost Quotes Report','660','','50930',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Lost Quotes Report','660','','21623',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Lost Quotes Report','701','','50546',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Lost Quotes Report','660','','50856',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Lost Quotes Report','660','','50857',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Lost Quotes Report','660','','50858',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Lost Quotes Report','660','','50859',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Lost Quotes Report','660','','50860',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Lost Quotes Report','660','','50861',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Lost Quotes Report','20005','','50880',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Lost Quotes Report','','LC053655','',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Lost Quotes Report','','10010432','',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Lost Quotes Report','','RB054040','',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Lost Quotes Report','','RV003897','',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Lost Quotes Report','','SS084202','',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Lost Quotes Report','','SO004816','',660,'XXEIS_RS_ADMIN','','');
--Inserting Report Pivots - Lost Quotes Report
xxeis.eis_rs_ins.rpivot( 'Lost Quotes Report',660,'Pivot','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rs_ins.rpivot_dtls( 'Lost Quotes Report',660,'Pivot','REASON_CODE','ROW_FIELD','','','1','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Lost Quotes Report',660,'Pivot','SUB_TOTAL','DATA_FIELD','SUM','Sales Amount','2','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Lost Quotes Report',660,'Pivot','COST_TOTAL','DATA_FIELD','SUM','Cost Amount','3','1','xlNormal');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
END;
/
set scan on define on
