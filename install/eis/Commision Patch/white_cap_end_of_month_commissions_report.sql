--Report Name            : White Cap End of Month Commissions Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View EIS_XXWC_AR_TBD_OUTBND_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_AR_TBD_OUTBND_V
Create or replace View XXEIS.EIS_XXWC_AR_TBD_OUTBND_V
(BUSINESSDATE,REPORT_TYPE,ORDER_NUMBER,INVOICENUMBER,LINENO,DESCRIPTION,QTY,UNITPRICE,EXTSALE,MASTERNUMBER,MASTERNAME,JOBNUMBER,CUSTOMERNAME,SALESREPNUMBER,SALESREPNAME,LOC,MP_ORGANIZATION_ID,REGIONNAME,PARTNO,CATCLASS,INVOICE_SOURCE,SHIPPED_QUANTITY,DIRECTFLAG,AVERAGECOST,TRADER,SPECIAL_COST,OPERATING_UNIT,ORG_ID,INVENTORY_ITEM_ID,ORGANIZATION_ID,CUSTOMER_TRX_ID,PARTY_ID,PERIOD_NAME,MFGADJUST,HP#101#PARTY_TYPE,HP#101#COLLECTOR,MSI#HDS#LOB,MSI#HDS#DROP_SHIPMENT_ELIGAB,MSI#HDS#INVOICE_UOM,MSI#HDS#PRODUCT_ID,MSI#HDS#VENDOR_PART_NUMBER,MSI#HDS#UNSPSC_CODE,MSI#HDS#UPC_PRIMARY,MSI#HDS#SKU_DESCRIPTION,MSI#WC#CA_PROP_65,MSI#WC#COUNTRY_OF_ORIGIN,MSI#WC#ORM_D_FLAG,MSI#WC#STORE_VELOCITY,MSI#WC#DC_VELOCITY,MSI#WC#YEARLY_STORE_VELOCITY,MSI#WC#YEARLY_DC_VELOCITY,MSI#WC#PRISM_PART_NUMBER,MSI#WC#HAZMAT_DESCRIPTION,MSI#WC#HAZMAT_CONTAINER,MSI#WC#GTP_INDICATOR,MSI#WC#LAST_LEAD_TIME,MSI#WC#AMU,MSI#WC#RESERVE_STOCK,MSI#WC#TAXWARE_CODE,MSI#WC#AVERAGE_UNITS,MSI#WC#PRODUCT_CODE,MSI#WC#IMPORT_DUTY_,MSI#WC#KEEP_ITEM_ACTIVE,MSI#WC#PESTICIDE_FLAG,MSI#WC#CALC_LEAD_TIME,MSI#WC#VOC_GL,MSI#WC#PESTICIDE_FLAG_STATE,MSI#WC#VOC_CATEGORY,MSI#WC#VOC_SUB_CATEGORY,MSI#WC#MSDS_#,MSI#WC#HAZMAT_PACKAGING_GROU,CT#101#LINE_OF_BUSINESS,CT#101#TRANSFER_TO_LINE_OF_B,CT#162#ARS_INVOICE_PRINT_DAT,CT#162#CUSTOMER_ORIGINAL_ADD,CT#162#PRISM_DATA___DO_NOT_U,CT#162#BILL_TRUST_INVOICE_PR) AS 
SELECT gd.gl_date businessdate,
    CASE
      WHEN ( mic.category_concat_segs IN ('99.99RT', '99.99RR')
      AND (oeh.name                   IN ('WC LONG TERM RENTAL', 'WC SHORT TERM RENTAL')))
        --OR upper(MSI.segment1) LIKE 'R%')
      THEN 'Rental'
      WHEN mic.segment1 IN ('BB', 'BS', 'DC', 'EC', 'EP', 'FA', 'FC', 'JS', 'ME', 'ML', ' MR', 'NS', 'PS', 'RE', 'RL', 'RP', 'SA', 'SC', 'SH', 'TH')
      THEN 'Intangibles'
      WHEN mc.segment1 IN ('GC', 'MD', 'PC', 'PD', 'PR', 'RR', 'RV', 'ST', 'XX')
      THEN 'Non Comm.'
      WHEN (mic.category_concat_segs IN ('FB.MODL', 'FB.SUBA', 'FB.OPTC', 'RF.OSPI', 'RF.SERV'))
      OR mic.category_concat_segs    IN ('60.6021', '60.6022', '60.6035')
      THEN 'FabRebar'
      WHEN ( (ol.attribute3 = 'Y'
      AND oel.name LIKE '%BILL%ONLY%')
      OR (ol.attribute15 = 'Y'
      AND oel.name LIKE '%BILL%ONLY%'))
      THEN 'Blow and Expired'
      ELSE 'Product'
    END report_type,
    oh.order_number order_number,
    ct.trx_number invoicenumber,
    ctl.line_number lineno,
    DECODE(msi.segment1 ,'Rental Charge',DECODE(ol.line_category_code,'RETURN',xxeis.eis_rs_xxwc_com_util_pkg.get_rental_item_desc(ol.reference_line_id), xxeis.eis_rs_xxwc_com_util_pkg.get_rental_item_desc(ol.link_to_line_id)) ,ctl.description) description,
    -- NVL(CTL.QUANTITY_ORDERED,0) QTY,
    -- NVL(ctl.unit_selling_price,0) unitprice,
    -- DECODE(NVL(ctl.unit_selling_price,0),0,0,(NVL(CTL.QUANTITY_ORDERED,0)*(NVL(ctl.unit_selling_price,0)+ xxeis.eis_rs_xxwc_com_util_pkg.Get_MfgAdjust(ol.header_id,ol.line_id)))) ExtSale,
    --  NVL(CTL.QUANTITY_ORDERED,0) QTY,
    NVL (ctl.quantity_invoiced, ctl.quantity_credited) qty,
    --DECODE(CTL.interface_line_attribute2,'RETURN ORDER',CTL.quantity_credited,NVL(quantity_invoiced,NVL(QUANTITY_ORDERED,0)))QTY,
    NVL (ctl.unit_selling_price, 0) unitprice,
    --  DECODE(NVL(ctl.unit_selling_price,0),0,0,(NVL(CTL.QUANTITY_ORDERED,0)*(NVL(ctl.unit_selling_price,0)+ xxeis.eis_rs_xxwc_com_util_pkg.Get_MfgAdjust(ol.header_id,ol.line_id)))) ExtSale,
    NVL ( ctl.extended_amount, DECODE ( NVL (ctl.unit_selling_price, 0), 0, 0, ( NVL (ctl.quantity_invoiced, ctl.quantity_credited) * ( NVL (ctl.unit_selling_price, 0) + xxeis.eis_rs_xxwc_com_util_pkg.get_mfgadjust ( ol.header_id, ol.line_id))))) extsale,
    hca.account_number masternumber,
    hca.account_name mastername,
    hps.party_site_number jobnumber,
    hcsu.location customername,
    jrs.salesrep_number salesrepnumber,
    nvl(jrse.source_name,jrse.resource_name) salesrepname,
    --RA.SALESREP_NUMBER SALESREPNUMBER,
    --RA.NAME SALESREPNAME,
    mp.organization_code loc,
    mp.organization_id mp_organization_id,
    mp.attribute9 regionname,
    DECODE(msi.segment1,'Rental Charge',DECODE(ol.line_category_code,'RETURN',xxeis.eis_rs_xxwc_com_util_pkg.get_rental_item(ol.reference_line_id), xxeis.eis_rs_xxwc_com_util_pkg.get_rental_item(ol.link_to_line_id)) ,msi.segment1) partno,
    mc.segment2 catclass,
    bs.name invoice_source,
    NVL (ol.shipped_quantity, 0) shipped_quantity,
    DECODE (ol.source_type_code, 'INTERNAL', 0, 'EXTERNAL', 1, ol.source_type_code) directflag,
    --NVL(APPS.CST_COST_API.GET_ITEM_COST(1,MSI.INVENTORY_ITEM_ID,MSI.ORGANIZATION_ID),0) AVERAGECOST,
    /*  CASE
    WHEN Mic.Segment1      IN ('BB', 'BS', 'DC', 'EC', 'EP', 'FA', 'FC', 'JS', 'ME',' ML',' MR', 'NS', 'PS', 'RE', 'RL', 'RP', 'SA','SC','SH','TH')
    AND Upper(Mic.Segment2) ='EC10'
    THEN to_number(Ol.Attribute8)
    WHEN NVL(ctl.unit_selling_price,0) = 0
    THEN 0
    ELSE NVL(Apps.Cst_Cost_Api.Get_Item_Cost(1,Msi.Inventory_Item_Id,Msi.Organization_Id),0)
    end averagecost,*/
    NVL(apps.xxwc_mv_routines_pkg.get_order_line_cost (ol.line_id), 0)averagecost,
    DECODE ( xxeis.eis_rs_xxwc_com_util_pkg.get_trader (ol.header_id, ol.line_id), 0, '-', 'Trader') trader,
    NVL ( ROUND ( xxeis.eis_rs_xxwc_com_util_pkg.get_special_cost ( ol.header_id, ol.line_id, ol.inventory_item_id), 2), 0) special_cost,
    hou.name operating_unit,
    hou.organization_id org_id,
    msi.inventory_item_id,
    msi.organization_id,
    ct.customer_trx_id,
    hp.party_id,
    xxeis.eis_rs_xxwc_com_util_pkg.get_period_name (gd.set_of_books_id, gd.gl_date) period_name,
    xxeis.eis_rs_xxwc_com_util_pkg.get_mfgadjust (ol.header_id, ol.line_id) mfgadjust--descr#flexfield#start
    ,
    DECODE ( hp.attribute_category, '101', xxeis.eis_rs_dff.decode_valueset ('XXCUS_PARTY_TYPE', hp.attribute1, 'I'), NULL) hp#101#party_type,
    DECODE ( hp.attribute_category, '101', xxeis.eis_rs_dff.decode_valueset ( 'XXCUSOZF_AR_COLLECTORS', hp.attribute3, 'F'), NULL) hp#101#collector,
    DECODE (msi.attribute_category, 'HDS', msi.attribute1, NULL) msi#hds#lob,
    DECODE ( msi.attribute_category, 'HDS', xxeis.eis_rs_dff.decode_valueset ('Yes_No', msi.attribute10, 'F'), NULL) msi#hds#drop_shipment_eligab,
    DECODE (msi.attribute_category, 'HDS', msi.attribute15, NULL) msi#hds#invoice_uom,
    DECODE (msi.attribute_category, 'HDS', msi.attribute2, NULL) msi#hds#product_id,
    DECODE (msi.attribute_category, 'HDS', msi.attribute3, NULL) msi#hds#vendor_part_number,
    DECODE (msi.attribute_category, 'HDS', msi.attribute4, NULL) msi#hds#unspsc_code,
    DECODE (msi.attribute_category, 'HDS', msi.attribute5, NULL) msi#hds#upc_primary,
    DECODE (msi.attribute_category, 'HDS', msi.attribute6, NULL) msi#hds#sku_description,
    DECODE (msi.attribute_category, 'WC', msi.attribute1, NULL) msi#wc#ca_prop_65,
    DECODE (msi.attribute_category, 'WC', msi.attribute10, NULL) msi#wc#country_of_origin,
    DECODE ( msi.attribute_category, 'WC', xxeis.eis_rs_dff.decode_valueset ('Yes_No', msi.attribute11, 'F'), NULL) msi#wc#orm_d_flag,
    DECODE (msi.attribute_category, 'WC', msi.attribute12, NULL) msi#wc#store_velocity,
    DECODE (msi.attribute_category, 'WC', msi.attribute13, NULL) msi#wc#dc_velocity,
    DECODE (msi.attribute_category, 'WC', msi.attribute14, NULL) msi#wc#yearly_store_velocity,
    DECODE (msi.attribute_category, 'WC', msi.attribute15, NULL) msi#wc#yearly_dc_velocity,
    DECODE (msi.attribute_category, 'WC', msi.attribute16, NULL) msi#wc#prism_part_number,
    DECODE (msi.attribute_category, 'WC', msi.attribute17, NULL) msi#wc#hazmat_description,
    DECODE ( msi.attribute_category, 'WC', xxeis.eis_rs_dff.decode_valueset ('XXWC HAZMAT CONTAINER', msi.attribute18, 'I'), NULL) msi#wc#hazmat_container,
    DECODE (msi.attribute_category, 'WC', msi.attribute19, NULL) msi#wc#gtp_indicator,
    DECODE (msi.attribute_category, 'WC', msi.attribute2, NULL) msi#wc#last_lead_time,
    DECODE (msi.attribute_category, 'WC', msi.attribute20, NULL) msi#wc#amu,
    DECODE (msi.attribute_category, 'WC', msi.attribute21, NULL) msi#wc#reserve_stock,
    DECODE ( msi.attribute_category, 'WC', xxeis.eis_rs_dff.decode_valueset ('XXWC TAXWARE CODE', msi.attribute22, 'I'), NULL) msi#wc#taxware_code,
    DECODE (msi.attribute_category, 'WC', msi.attribute25, NULL) msi#wc#average_units,
    DECODE (msi.attribute_category, 'WC', msi.attribute26, NULL) msi#wc#product_code,
    DECODE (msi.attribute_category, 'WC', msi.attribute27, NULL) msi#wc#import_duty_,
    DECODE ( msi.attribute_category, 'WC', xxeis.eis_rs_dff.decode_valueset ('Yes_No', msi.attribute29, 'F'), NULL) msi#wc#keep_item_active,
    DECODE ( msi.attribute_category, 'WC', xxeis.eis_rs_dff.decode_valueset ('Yes_No', msi.attribute3, 'F'), NULL) msi#wc#pesticide_flag,
    DECODE (msi.attribute_category, 'WC', msi.attribute30, NULL) msi#wc#calc_lead_time,
    DECODE (msi.attribute_category, 'WC', msi.attribute4, NULL) msi#wc#voc_gl,
    DECODE (msi.attribute_category, 'WC', msi.attribute5, NULL) msi#wc#pesticide_flag_state,
    DECODE (msi.attribute_category, 'WC', msi.attribute6, NULL) msi#wc#voc_category,
    DECODE (msi.attribute_category, 'WC', msi.attribute7, NULL) msi#wc#voc_sub_category,
    DECODE (msi.attribute_category, 'WC', msi.attribute8, NULL) msi#wc#msds_#,
    DECODE ( msi.attribute_category, 'WC', xxeis.eis_rs_dff.decode_valueset ( 'XXWC_HAZMAT_PACKAGE_GROUP', msi.attribute9, 'I'), NULL) msi#wc#hazmat_packaging_grou,
    DECODE ( ct.attribute_category, '101', xxeis.eis_rs_dff.decode_valueset ('XXCUSOZF_PARTY_CODES', ct.attribute1, 'F'), NULL) ct#101#line_of_business,
    DECODE (ct.attribute_category, '101', ct.attribute11, NULL) ct#101#transfer_to_line_of_b,
    DECODE (ct.attribute_category, '162', ct.attribute12, NULL) ct#162#ars_invoice_print_dat,
    DECODE (ct.attribute_category, '162', ct.attribute13, NULL) ct#162#customer_original_add,
    DECODE (ct.attribute_category, '162', ct.attribute14, NULL) ct#162#prism_data___do_not_u,
    DECODE (ct.attribute_category, '162', ct.attribute15, NULL) ct#162#bill_trust_invoice_pr
    --descr#flexfield#end
    --gl#accountff#start
    --gl#accountff#end
  FROM ra_customer_trx ct,
    ra_customer_trx_lines ctl,
    ra_cust_trx_line_gl_dist gd,
    hz_parties hp,
    hz_cust_accounts hca,
    hz_party_sites hps,
    hz_cust_acct_sites hcas,
    hz_cust_site_uses hcsu,
    jtf_rs_salesreps jrs,
    jtf_rs_resource_extns_vl jrse,
    -- RA_SALESREPS_ALL RA,
    mtl_parameters mp,
    mtl_system_items_kfv msi,
    mtl_item_categories_v mic,
    mtl_categories_v mc,
    oe_order_lines ol,
    oe_order_headers oh,
    hr_operating_units hou,
    mtl_category_sets mdcs,
    ra_batch_sources bs,
    oe_transaction_types_vl oel,
    oe_transaction_types_vl oeh
  WHERE 1                        = 1
  AND ct.customer_trx_id         = ctl.customer_trx_id
  AND ct.customer_trx_id         = gd.customer_trx_id
  AND gd.account_class           = 'REC'
  AND ct.bill_to_customer_id     = hca.cust_account_id
  AND hca.party_id               = hp.party_id(+)
  AND hp.party_id                = hps.party_id
  AND hcas.party_site_id         = hps.party_site_id
  AND hcsu.cust_acct_site_id     = hcas.cust_acct_site_id
  AND hcsu.site_use_id           = ct.bill_to_site_use_id
  AND hca.cust_account_id        = hcas.cust_account_id
  AND ol.salesrep_id             = jrs.salesrep_id
  --AND ct.primary_salesrep_id     = jrs.salesrep_id
  AND oh.org_id                  = jrs.org_id
  AND jrs.resource_id            = jrse.resource_id
  AND ctl.interface_line_context = 'ORDER ENTRY'
  AND TO_CHAR (oh.order_number)  = ct.interface_header_attribute1
  AND TO_CHAR (ol.line_id)       = ctl.interface_line_attribute6
  AND oh.header_id               = ol.header_id
  AND msi.inventory_item_id      = ol.inventory_item_id
  AND msi.organization_id        = ol.ship_from_org_id
  AND mic.category_id            = mc.category_id
  AND msi.organization_id        = mic.organization_id
  AND msi.inventory_item_id      = mic.inventory_item_id
  AND ct.org_id                  = hou.organization_id
  AND msi.organization_id        = mp.organization_id
  AND mic.category_set_id        = mdcs.category_set_id
  AND ctl.extended_amount       <> 0
  AND mdcs.category_set_name     = 'Inventory Category'
  AND ct.batch_source_id         = bs.batch_source_id
  AND ct.org_id                  = bs.org_id
  AND mc.structure_name          = 'Item Categories'
  AND oel.transaction_type_id    = ol.line_type_id
  AND oeh.transaction_type_id    = oh.order_type_id
  AND oeh.org_id                 = oh.org_id
  AND oel.org_id                 = ol.org_id
/
set scan on define on
prompt Creating View Data for White Cap End of Month Commissions Report
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_AR_TBD_OUTBND_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_AR_TBD_OUTBND_V',222,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Ar Tbd Outbound V','EXATOV','','');
--Delete View Columns for EIS_XXWC_AR_TBD_OUTBND_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_AR_TBD_OUTBND_V',222,FALSE);
--Inserting View Columns for EIS_XXWC_AR_TBD_OUTBND_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','OPERATING_UNIT',222,'Operating Unit','OPERATING_UNIT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Operating Unit','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','AVERAGECOST',222,'Averagecost','AVERAGECOST','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Averagecost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','DIRECTFLAG',222,'Directflag','DIRECTFLAG','','','','XXEIS_RS_ADMIN','NUMBER','','','Directflag','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','CATCLASS',222,'Catclass','CATCLASS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Catclass','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','PARTNO',222,'Partno','PARTNO','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Partno','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','REGIONNAME',222,'Regionname','REGIONNAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Regionname','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','LOC',222,'Loc','LOC','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Loc','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','SALESREPNAME',222,'Salesrepname','SALESREPNAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Salesrepname','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','SALESREPNUMBER',222,'Salesrepnumber','SALESREPNUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Salesrepnumber','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','CUSTOMERNAME',222,'Customername','CUSTOMERNAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customername','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','JOBNUMBER',222,'Jobnumber','JOBNUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Jobnumber','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','MASTERNAME',222,'Mastername','MASTERNAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Mastername','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','MASTERNUMBER',222,'Masternumber','MASTERNUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Masternumber','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','EXTSALE',222,'Extsale','EXTSALE','','','','XXEIS_RS_ADMIN','NUMBER','','','Extsale','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','UNITPRICE',222,'Unitprice','UNITPRICE','','','','XXEIS_RS_ADMIN','NUMBER','','','Unitprice','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','QTY',222,'Qty','QTY','','','','XXEIS_RS_ADMIN','NUMBER','','','Qty','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','DESCRIPTION',222,'Description','DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','LINENO',222,'Lineno','LINENO','','','','XXEIS_RS_ADMIN','NUMBER','','','Lineno','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','INVOICENUMBER',222,'Invoicenumber','INVOICENUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Invoicenumber','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','BUSINESSDATE',222,'Businessdate','BUSINESSDATE','','','','XXEIS_RS_ADMIN','DATE','','','Businessdate','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','TRADER',222,'Trader','TRADER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Trader','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','INVOICE_SOURCE',222,'Invoice Source','INVOICE_SOURCE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Invoice Source','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','REPORT_TYPE',222,'Report Type','REPORT_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Report Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','PERIOD_NAME',222,'Period Name','PERIOD_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Period Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','SHIPPED_QUANTITY',222,'Shipped Quantity','SHIPPED_QUANTITY','','','','XXEIS_RS_ADMIN','NUMBER','','','Shipped Quantity','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','ORDER_NUMBER',222,'Order Number','ORDER_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Order Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','MFGADJUST',222,'Mfgadjust','MFGADJUST','','','','XXEIS_RS_ADMIN','NUMBER','','','Mfgadjust','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','CUSTOMER_TRX_ID',222,'Customer Trx Id','CUSTOMER_TRX_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Customer Trx Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','ORGANIZATION_ID',222,'Organization Id','ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','INVENTORY_ITEM_ID',222,'Inventory Item Id','INVENTORY_ITEM_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Inventory Item Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','ORG_ID',222,'Org Id','ORG_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Org Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','PARTY_ID',222,'Party Id','PARTY_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Party Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','MP_ORGANIZATION_ID',222,'Mp Organization Id','MP_ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Mp Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','SPECIAL_COST',222,'Special Cost','SPECIAL_COST','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Special Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','HP#101#PARTY_TYPE',222,'Descriptive flexfield: Party Information Column Name: Party Type Context: 101','HP#101#Party_Type','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_PARTIES','ATTRIBUTE1','Hp#101#Party Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','HP#101#COLLECTOR',222,'Descriptive flexfield: Party Information Column Name: Collector Context: 101','HP#101#Collector','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_PARTIES','ATTRIBUTE3','Hp#101#Collector','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','MSI#HDS#LOB',222,'Descriptive flexfield: Items Column Name: LOB Context: HDS','MSI#HDS#LOB','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE1','Msi#Hds#Lob','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','MSI#HDS#DROP_SHIPMENT_ELIGAB',222,'Descriptive flexfield: Items Column Name: Drop Shipment Eligable Context: HDS','MSI#HDS#Drop_Shipment_Eligab','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE10','Msi#Hds#Drop Shipment Eligable','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','MSI#HDS#INVOICE_UOM',222,'Descriptive flexfield: Items Column Name: Invoice UOM Context: HDS','MSI#HDS#Invoice_UOM','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE15','Msi#Hds#Invoice Uom','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','MSI#HDS#PRODUCT_ID',222,'Descriptive flexfield: Items Column Name: Product ID Context: HDS','MSI#HDS#Product_ID','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE2','Msi#Hds#Product Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','MSI#HDS#VENDOR_PART_NUMBER',222,'Descriptive flexfield: Items Column Name: Vendor Part Number Context: HDS','MSI#HDS#Vendor_Part_Number','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE3','Msi#Hds#Vendor Part Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','MSI#HDS#UNSPSC_CODE',222,'Descriptive flexfield: Items Column Name: UNSPSC Code Context: HDS','MSI#HDS#UNSPSC_Code','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE4','Msi#Hds#Unspsc Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','MSI#HDS#UPC_PRIMARY',222,'Descriptive flexfield: Items Column Name: UPC Primary Context: HDS','MSI#HDS#UPC_Primary','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE5','Msi#Hds#Upc Primary','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','MSI#HDS#SKU_DESCRIPTION',222,'Descriptive flexfield: Items Column Name: SKU Description Context: HDS','MSI#HDS#SKU_Description','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE6','Msi#Hds#Sku Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','MSI#WC#CA_PROP_65',222,'Descriptive flexfield: Items Column Name: CA Prop 65 Context: WC','MSI#WC#CA_Prop_65','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE1','Msi#Wc#Ca Prop 65','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','MSI#WC#COUNTRY_OF_ORIGIN',222,'Descriptive flexfield: Items Column Name: Country of Origin Context: WC','MSI#WC#Country_of_Origin','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE10','Msi#Wc#Country Of Origin','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','MSI#WC#ORM_D_FLAG',222,'Descriptive flexfield: Items Column Name: ORM-D Flag Context: WC','MSI#WC#ORM_D_Flag','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE11','Msi#Wc#Orm-D Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','MSI#WC#STORE_VELOCITY',222,'Descriptive flexfield: Items Column Name: Store Velocity Context: WC','MSI#WC#Store_Velocity','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE12','Msi#Wc#Store Velocity','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','MSI#WC#DC_VELOCITY',222,'Descriptive flexfield: Items Column Name: DC Velocity Context: WC','MSI#WC#DC_Velocity','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE13','Msi#Wc#Dc Velocity','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','MSI#WC#YEARLY_STORE_VELOCITY',222,'Descriptive flexfield: Items Column Name: Yearly Store Velocity Context: WC','MSI#WC#Yearly_Store_Velocity','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE14','Msi#Wc#Yearly Store Velocity','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','MSI#WC#YEARLY_DC_VELOCITY',222,'Descriptive flexfield: Items Column Name: Yearly DC Velocity Context: WC','MSI#WC#Yearly_DC_Velocity','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE15','Msi#Wc#Yearly Dc Velocity','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','MSI#WC#PRISM_PART_NUMBER',222,'Descriptive flexfield: Items Column Name: PRISM Part Number Context: WC','MSI#WC#PRISM_Part_Number','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE16','Msi#Wc#Prism Part Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','MSI#WC#HAZMAT_DESCRIPTION',222,'Descriptive flexfield: Items Column Name: Hazmat Description Context: WC','MSI#WC#Hazmat_Description','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE17','Msi#Wc#Hazmat Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','MSI#WC#HAZMAT_CONTAINER',222,'Descriptive flexfield: Items Column Name: Hazmat Container Context: WC','MSI#WC#Hazmat_Container','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE18','Msi#Wc#Hazmat Container','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','MSI#WC#GTP_INDICATOR',222,'Descriptive flexfield: Items Column Name: GTP Indicator Context: WC','MSI#WC#GTP_Indicator','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE19','Msi#Wc#Gtp Indicator','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','MSI#WC#LAST_LEAD_TIME',222,'Descriptive flexfield: Items Column Name: Last Lead Time Context: WC','MSI#WC#Last_Lead_Time','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE2','Msi#Wc#Last Lead Time','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','MSI#WC#AMU',222,'Descriptive flexfield: Items Column Name: AMU Context: WC','MSI#WC#AMU','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE20','Msi#Wc#Amu','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','MSI#WC#RESERVE_STOCK',222,'Descriptive flexfield: Items Column Name: Reserve Stock Context: WC','MSI#WC#Reserve_Stock','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE21','Msi#Wc#Reserve Stock','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','MSI#WC#TAXWARE_CODE',222,'Descriptive flexfield: Items Column Name: Taxware Code Context: WC','MSI#WC#Taxware_Code','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE22','Msi#Wc#Taxware Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','MSI#WC#AVERAGE_UNITS',222,'Descriptive flexfield: Items Column Name: Average Units Context: WC','MSI#WC#Average_Units','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE25','Msi#Wc#Average Units','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','MSI#WC#PRODUCT_CODE',222,'Descriptive flexfield: Items Column Name: Product code Context: WC','MSI#WC#Product_code','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE26','Msi#Wc#Product Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','MSI#WC#IMPORT_DUTY_',222,'Descriptive flexfield: Items Column Name: Import Duty % Context: WC','MSI#WC#Import_Duty_','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE27','Msi#Wc#Import Duty %','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','MSI#WC#KEEP_ITEM_ACTIVE',222,'Descriptive flexfield: Items Column Name: Keep Item Active Context: WC','MSI#WC#Keep_Item_Active','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE29','Msi#Wc#Keep Item Active','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','MSI#WC#PESTICIDE_FLAG',222,'Descriptive flexfield: Items Column Name: Pesticide Flag Context: WC','MSI#WC#Pesticide_Flag','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE3','Msi#Wc#Pesticide Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','MSI#WC#CALC_LEAD_TIME',222,'Descriptive flexfield: Items Column Name: Calc Lead Time Context: WC','MSI#WC#Calc_Lead_Time','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE30','Msi#Wc#Calc Lead Time','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','MSI#WC#VOC_GL',222,'Descriptive flexfield: Items Column Name: VOC G/L Context: WC','MSI#WC#VOC_GL','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE4','Msi#Wc#Voc G/L','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','MSI#WC#PESTICIDE_FLAG_STATE',222,'Descriptive flexfield: Items Column Name: Pesticide Flag State Context: WC','MSI#WC#Pesticide_Flag_State','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE5','Msi#Wc#Pesticide Flag State','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','MSI#WC#VOC_CATEGORY',222,'Descriptive flexfield: Items Column Name: VOC Category Context: WC','MSI#WC#VOC_Category','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE6','Msi#Wc#Voc Category','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','MSI#WC#VOC_SUB_CATEGORY',222,'Descriptive flexfield: Items Column Name: VOC Sub Category Context: WC','MSI#WC#VOC_Sub_Category','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE7','Msi#Wc#Voc Sub Category','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','MSI#WC#MSDS_#',222,'Descriptive flexfield: Items Column Name: MSDS # Context: WC','MSI#WC#MSDS_#','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE8','Msi#Wc#Msds #','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','MSI#WC#HAZMAT_PACKAGING_GROU',222,'Descriptive flexfield: Items Column Name: Hazmat Packaging Group Context: WC','MSI#WC#Hazmat_Packaging_Grou','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE9','Msi#Wc#Hazmat Packaging Group','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','CT#101#LINE_OF_BUSINESS',222,'Descriptive flexfield: Transaction Information Column Name: Line of Business Context: 101','CT#101#Line_of_Business','','','','XXEIS_RS_ADMIN','VARCHAR2','RA_CUSTOMER_TRX_ALL','ATTRIBUTE1','Ct#101#Line Of Business','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','CT#101#TRANSFER_TO_LINE_OF_B',222,'Descriptive flexfield: Transaction Information Column Name: Transfer to Line Of Buisness Context: 101','CT#101#Transfer_to_Line_Of_B','','','','XXEIS_RS_ADMIN','VARCHAR2','RA_CUSTOMER_TRX_ALL','ATTRIBUTE11','Ct#101#Transfer To Line Of Buisness','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','CT#162#ARS_INVOICE_PRINT_DAT',222,'Descriptive flexfield: Transaction Information Column Name: ARS Invoice Print Date Context: 162','CT#162#ARS_Invoice_Print_Dat','','','','XXEIS_RS_ADMIN','VARCHAR2','RA_CUSTOMER_TRX_ALL','ATTRIBUTE12','Ct#162#Ars Invoice Print Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','CT#162#CUSTOMER_ORIGINAL_ADD',222,'Descriptive flexfield: Transaction Information Column Name: Customer Original Address Context: 162','CT#162#Customer_Original_Add','','','','XXEIS_RS_ADMIN','VARCHAR2','RA_CUSTOMER_TRX_ALL','ATTRIBUTE13','Ct#162#Customer Original Address','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','CT#162#PRISM_DATA___DO_NOT_U',222,'Descriptive flexfield: Transaction Information Column Name: PRISM Data - Do Not Use Context: 162','CT#162#PRISM_Data___Do_Not_U','','','','XXEIS_RS_ADMIN','VARCHAR2','RA_CUSTOMER_TRX_ALL','ATTRIBUTE14','Ct#162#Prism Data - Do Not Use','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','CT#162#BILL_TRUST_INVOICE_PR',222,'Descriptive flexfield: Transaction Information Column Name: Bill Trust Invoice Print Date Context: 162','CT#162#Bill_Trust_Invoice_Pr','','','','XXEIS_RS_ADMIN','VARCHAR2','RA_CUSTOMER_TRX_ALL','ATTRIBUTE15','Ct#162#Bill Trust Invoice Print Date','','','');
--Inserting View Components for EIS_XXWC_AR_TBD_OUTBND_V
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_AR_TBD_OUTBND_V','RA_CUSTOMER_TRX',222,'RA_CUSTOMER_TRX_ALL','CT','CT','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Header-Level Information About Invoices, Debit Mem','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_AR_TBD_OUTBND_V','MTL_SYSTEM_ITEMS_KFV',222,'MTL_SYSTEM_ITEMS_B','MSI','MSI','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Inventory Item Definitions','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_AR_TBD_OUTBND_V','HZ_PARTIES',222,'HZ_PARTIES','HP','HP','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Party Information','','','','');
--Inserting View Component Joins for EIS_XXWC_AR_TBD_OUTBND_V
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_AR_TBD_OUTBND_V','RA_CUSTOMER_TRX','CT',222,'EXATOV.CUSTOMER_TRX_ID','=','CT.CUSTOMER_TRX_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_AR_TBD_OUTBND_V','MTL_SYSTEM_ITEMS_KFV','MSI',222,'EXATOV.INVENTORY_ITEM_ID','=','MSI.INVENTORY_ITEM_ID(+)','','','','','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_AR_TBD_OUTBND_V','MTL_SYSTEM_ITEMS_KFV','MSI',222,'EXATOV.ORGANIZATION_ID','=','MSI.ORGANIZATION_ID(+)','','','','','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_AR_TBD_OUTBND_V','HZ_PARTIES','HP',222,'EXATOV.PARTY_ID','=','HP.PARTY_ID(+)','','','','','XXEIS_RS_ADMIN','','');
END;
/
set scan on define on
prompt Creating Report LOV Data for White Cap End of Month Commissions Report
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - White Cap End of Month Commissions Report
xxeis.eis_rs_ins.lov( 222,'select  per.period_name , led.name ledger_name, per.period_year, per.period_num, per.start_date, per.end_date
from    gl_periods per,gl_ledgers led
where  per.period_set_name = led.period_set_name
and     GL_SECURITY_PKG.VALIDATE_ACCESS(led.ledger_ID) = ''TRUE''
and     led.accounted_period_type = per.period_type','','AR_PERIOD_NAMES','AR_PERIOD_NAMES','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 222,'SELECT hou.NAME organization_name,
       TO_CHAR (date_from,
                NVL (fnd_profile.VALUE (''ICX_DATE_FORMAT_MASK''),
                     ''DD-MON-YYYY'')
               ) date_from,
       TO_CHAR (date_to,
                NVL (fnd_profile.VALUE (''ICX_DATE_FORMAT_MASK''),
                     ''DD-MON-YYYY'')
               ) date_to
  FROM hr_operating_units hou,
       mo_glob_org_access_tmp tmp
 WHERE hou.organization_id = tmp.organization_id','','EIS_MULTI_OPERATING_UNIT_LOV','This LOV shows all the operating units, to which user has access.','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 222,'select DISTINCT NAME from RA_BATCH_SOURCES_ALL','','AR Batch Source Name LOV','Displays Batch Sources information','XXEIS_RS_ADMIN',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for White Cap End of Month Commissions Report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - White Cap End of Month Commissions Report
xxeis.eis_rs_utility.delete_report_rows( 'White Cap End of Month Commissions Report' );
--Inserting Report - White Cap End of Month Commissions Report
xxeis.eis_rs_ins.r( 222,'White Cap End of Month Commissions Report','','The purpose of this report is to extract the necessary information on posted sales to enable the accurate calculation of commissions to White Cap Sales Representatives for a defined period of time (e.g. one month). Note:  The parameter �GL Period to Report� correlates to the range of dates that will be reported (e.g. select APR-20yy) will report all transactions within the respective GL period. 

','','','','XXEIS_RS_ADMIN','EIS_XXWC_AR_TBD_OUTBND_V','Y','','','XXEIS_RS_ADMIN','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - White Cap End of Month Commissions Report
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'AVERAGECOST','AverageCost','Averagecost','','~T~D~2','default','','18','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'BUSINESSDATE','BusinessDate','Businessdate','','','default','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'CATCLASS','CatClass','Catclass','','','default','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'CUSTOMERNAME','CustomerName','Customername','','','default','','27','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'DESCRIPTION','Description','Description','','','default','','13','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'DIRECTFLAG','DirectFlag','Directflag','','~~~','default','','26','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'INVOICENUMBER','InvoiceNumber','Invoicenumber','','','default','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'JOBNUMBER','JobNumber','Jobnumber','','','default','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'LINENO','LineNo','Lineno','','~~~','default','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'LOC','Loc','Loc','','','default','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'MASTERNAME','MasterName','Mastername','','','default','','28','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'MASTERNUMBER','MasterNumber','Masternumber','','','default','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'PARTNO','PartNo','Partno','','','default','','12','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'QTY','QTY','Qty','','~~~','default','','14','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'REGIONNAME','RegionName','Regionname','','','default','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'SALESREPNAME','SalesRepName','Salesrepname','','','default','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'SALESREPNUMBER','SalesrepNumber','Salesrepnumber','','','default','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'UNITPRICE','UnitPrice','Unitprice','','~T~D~2','default','','15','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'TRADER','Trader','Trader','','','default','','25','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'REPORT_TYPE','ReportTypeName','Report Type','','','default','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'EXT_AVG_COST','ExtAvgCost','Report Type','NUMBER','~T~D~2','default','','19','Y','','','','','','','(EXATOV.AVERAGECOST * EXATOV.QTY)','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'GM_AVG_COST','GMAvgCost','Report Type','NUMBER','~T~D~2','default','','21','Y','','','','','','','decode(EXATOV.ExtSale ,0,0,((EXATOV.ExtSale - (EXATOV.AVERAGECOST * EXATOV.QTY)) / EXATOV.ExtSale))','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'EXT_SPECIAL_COST','ExtSpecialCost','Report Type','NUMBER','~T~D~2','default','','22','Y','','','','','','','(EXATOV.SPECIAL_COST * EXATOV.QTY)','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'GPS_SPECIAL_COST','GPSpecialCost','Report Type','NUMBER','~T~D~2','default','','23','Y','','','','','','','DECODE((EXATOV.SPECIAL_COST * EXATOV.QTY),0,0,(EXATOV.ExtSale - (EXATOV.SPECIAL_COST * EXATOV.QTY)))','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'GMS_SPECIAL_COST','GMSpecialCost','Report Type','NUMBER','~T~D~2','default','','24','Y','','','','','','','DECODE((EXATOV.SPECIAL_COST * EXATOV.QTY),0,0,((EXATOV.ExtSale - (EXATOV.SPECIAL_COST * EXATOV.QTY))/(EXATOV.ExtSale)))','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'GP_AVG_COST','GPAvgCost','Report Type','NUMBER','~T~D~2','default','','20','Y','','','','','','','(EXATOV.ExtSale - (EXATOV.AVERAGECOST * EXATOV.QTY))','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'MFGADJUST','Mfgadjust','Mfgadjust','','~T~D~2','default','','16','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'EXTSALE','Extsale','Extsale','','~T~D~2','default','','17','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'ORDER_NUMBER','Order Number','Order Number','','~~~','default','','29','','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
--Inserting Report Parameters - White Cap End of Month Commissions Report
xxeis.eis_rs_ins.rp( 'White Cap End of Month Commissions Report',222,'Operating Unit','Operating Unit','OPERATING_UNIT','IN','EIS_MULTI_OPERATING_UNIT_LOV','select name from hr_operating_units where organization_id=fnd_profile.value(''ORG_ID'')','VARCHAR2','Y','Y','1','','Y','SQL','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'White Cap End of Month Commissions Report',222,'GL Period to Report','GL Period to Report','PERIOD_NAME','IN','AR_PERIOD_NAMES','select gps.period_name from gl_period_statuses gps where gps.application_id = 101  and gps.set_of_books_id = fnd_profile.value(''GL_SET_OF_BKS_ID'') and trunc(sysdate) between gps.start_date and gps.end_date','VARCHAR2','Y','Y','3','','Y','SQL','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'White Cap End of Month Commissions Report',222,'Order Line Quantity Shipped','Order Line Quantity Shipped','SHIPPED_QUANTITY','IN','','','NUMERIC','N','N','4','','Y','CONSTANT','XXEIS_RS_ADMIN','N','N','','','');
xxeis.eis_rs_ins.rp( 'White Cap End of Month Commissions Report',222,'Order Line Extended Amount','Order Line Extended Amount','EXTSALE','IN','','','NUMERIC','N','N','5','','Y','CONSTANT','XXEIS_RS_ADMIN','N','N','','','');
xxeis.eis_rs_ins.rp( 'White Cap End of Month Commissions Report',222,'Order Line Extended Cost','Order Line Extended Cost','AVERAGECOST','IN','','','NUMERIC','N','N','6','','Y','CONSTANT','XXEIS_RS_ADMIN','N','N','','','');
xxeis.eis_rs_ins.rp( 'White Cap End of Month Commissions Report',222,'Invoice Source','Invoice Source','INVOICE_SOURCE','IN','AR Batch Source Name LOV','''ORDER MANAGEMENT'',''STANDARD OM SOURCE'',''REPAIR OM SOURCE'',''WC MANUAL''','VARCHAR2','Y','Y','2','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
--Inserting Report Conditions - White Cap End of Month Commissions Report
xxeis.eis_rs_ins.rcn( 'White Cap End of Month Commissions Report',222,'SHIPPED_QUANTITY','IN',':Order Line Quantity Shipped','','','Y','4','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'White Cap End of Month Commissions Report',222,'EXTSALE','IN',':Order Line Extended Amount','','','Y','5','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'White Cap End of Month Commissions Report',222,'AVERAGECOST','IN',':Order Line Extended Cost','','','Y','6','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'White Cap End of Month Commissions Report',222,'INVOICE_SOURCE','IN',':Invoice Source','','','Y','2','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'White Cap End of Month Commissions Report',222,'OPERATING_UNIT','IN',':Operating Unit','','','Y','1','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'White Cap End of Month Commissions Report',222,'PERIOD_NAME','IN',':GL Period to Report','','','Y','3','Y','XXEIS_RS_ADMIN');
--Inserting Report Sorts - White Cap End of Month Commissions Report
xxeis.eis_rs_ins.rs( 'White Cap End of Month Commissions Report',222,'REPORT_TYPE','ASC','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rs( 'White Cap End of Month Commissions Report',222,'SALESREPNUMBER','ASC','XXEIS_RS_ADMIN','','');
--Inserting Report Triggers - White Cap End of Month Commissions Report
--Inserting Report Templates - White Cap End of Month Commissions Report
--Inserting Report Portals - White Cap End of Month Commissions Report
--Inserting Report Dashboards - White Cap End of Month Commissions Report
--Inserting Report Security - White Cap End of Month Commissions Report
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','222','','50920',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','222','','50919',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','222','','50921',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','222','','50922',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','222','','50923',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','222','','51030',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','222','','50638',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','222','','50622',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','222','','20678',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','401','','50941',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','222','','21404',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','222','','50846',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','222','','50845',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','222','','50847',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','222','','50848',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','222','','50849',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','222','','50944',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','222','','50871',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','20005','','50880',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','','LC053655','',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','','10010432','',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','','RB054040','',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','','RV003897','',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','','SS084202','',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','','SO004816','',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','20005','','50900',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','222','','50894',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','222','','50873',222,'XXEIS_RS_ADMIN','','');
--Inserting Report Pivots - White Cap End of Month Commissions Report
xxeis.eis_rs_ins.rpivot( 'White Cap End of Month Commissions Report',222,'Pivot','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rs_ins.rpivot_dtls( 'White Cap End of Month Commissions Report',222,'Pivot','LOC','COL_FIELD','','Branch Id','','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'White Cap End of Month Commissions Report',222,'Pivot','SALESREPNUMBER','ROW_FIELD','','','','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'White Cap End of Month Commissions Report',222,'Pivot','EXTSALE','DATA_FIELD','SUM','','','1','xlNormal');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
END;
/
set scan on define on
