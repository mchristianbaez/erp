--Report Name            : Purge Custom Tables Data Weekly Maintenance - WC
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating View Data for Purge Custom Tables Data Weekly Maintenance - WC
set scan off define off
DECLARE
BEGIN 
--Inserting View XXEIS_2186497_FIFWQP_V
xxeis.eis_rs_ins.v( 'XXEIS_2186497_FIFWQP_V',85000,'Paste SQL View for Purge Custom Tables Data','1.0','','','SA059956','APPS','Purge Custom Tables Data View','X2FV','','');
--Delete View Columns for XXEIS_2186497_FIFWQP_V
xxeis.eis_rs_utility.delete_view_rows('XXEIS_2186497_FIFWQP_V',85000,FALSE);
--Inserting View Columns for XXEIS_2186497_FIFWQP_V
xxeis.eis_rs_ins.vc( 'XXEIS_2186497_FIFWQP_V','DESCRIPTION',85000,'','','','','','SA059956','VARCHAR2','','','Description','','','');
--Inserting View Components for XXEIS_2186497_FIFWQP_V
--Inserting View Component Joins for XXEIS_2186497_FIFWQP_V
END;
/
set scan on define on
prompt Creating Report Data for Purge Custom Tables Data Weekly Maintenance - WC
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Purge Custom Tables Data Weekly Maintenance - WC
xxeis.eis_rs_utility.delete_report_rows( 'Purge Custom Tables Data Weekly Maintenance - WC' );
--Inserting Report - Purge Custom Tables Data Weekly Maintenance - WC
xxeis.eis_rs_ins.r( 85000,'Purge Custom Tables Data Weekly Maintenance - WC','','Purge Custom Tables Data Weekly Maintenance - WC','','','','SA059956','XXEIS_2186497_FIFWQP_V','Y','','SELECT XDFT.DESCRIPTION
FROM
XXEIS.XXWC_DEL_FAV_REP_TBL XDFT
','SA059956','','N','WC Audit Reports','','CSV,EXCEL,','');
--Inserting Report Columns - Purge Custom Tables Data Weekly Maintenance - WC
xxeis.eis_rs_ins.rc( 'Purge Custom Tables Data Weekly Maintenance - WC',85000,'DESCRIPTION','Description','','','','default','','1','N','','','','','','','','SA059956','N','N','','XXEIS_2186497_FIFWQP_V','','');
--Inserting Report Parameters - Purge Custom Tables Data Weekly Maintenance - WC
--Inserting Report Conditions - Purge Custom Tables Data Weekly Maintenance - WC
--Inserting Report Sorts - Purge Custom Tables Data Weekly Maintenance - WC
--Inserting Report Triggers - Purge Custom Tables Data Weekly Maintenance - WC
xxeis.eis_rs_ins.rt( 'Purge Custom Tables Data Weekly Maintenance - WC',85000,'BEGIN
xxeis.EIS_XXWC_PURGE_CUST_TAB_PROC;
end;','B','Y','SA059956');
--Inserting Report Templates - Purge Custom Tables Data Weekly Maintenance - WC
--Inserting Report Portals - Purge Custom Tables Data Weekly Maintenance - WC
--Inserting Report Dashboards - Purge Custom Tables Data Weekly Maintenance - WC
--Inserting Report Security - Purge Custom Tables Data Weekly Maintenance - WC
xxeis.eis_rs_ins.rsec( 'Purge Custom Tables Data Weekly Maintenance - WC','20005','','50900',85000,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Purge Custom Tables Data Weekly Maintenance - WC','1','','51669',85000,'SA059956','','');
--Inserting Report Pivots - Purge Custom Tables Data Weekly Maintenance - WC
END;
/
set scan on define on
