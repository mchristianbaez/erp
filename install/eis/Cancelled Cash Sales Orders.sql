--Report Name            : Cancelled Cash Sales Orders
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View EIS_XXWC_OM_CANC_ORDERS_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_OM_CANC_ORDERS_V
Create or replace View XXEIS.EIS_XXWC_OM_CANC_ORDERS_V
 AS 
select oh.ship_from_org_id Branch
         ,oh.ship_from_org_id ship_from_org_id
         ,ood.organization_code organization_code
         ,ood.organization_name Branch_Name
         ,mtp.attribute9 Region
         ,mtp.attribute9 attribute9
         ,mtp.attribute8 District
         ,mtp.attribute8 attribute8
         ,hzp.party_number Customer_Number
         ,hzp.party_name Customer_Name
         ,oh.order_number Order_Number
         ,otyp.name Order_Type
         ,TO_CHAR (oh.ordered_date, 'DD-MON-YYYY') Order_Date
         ,TO_CHAR (ordered_date, 'HH24:MI:SS') Created_Time
         ,fu.user_name Created_by_ID      --Added by Mahender
         ,fu.description Created_by_Name   --Added by Mahender
         /*, (SELECT user_name
              FROM fnd_user
             WHERE user_id = (SELECT created_by
                                FROM oe_order_headers_all
                               WHERE header_id = oh.header_id))
             "Created by ID1"
         ,(SELECT description
              FROM fnd_user
             WHERE user_id = (SELECT created_by
                                FROM oe_order_headers_all
                               WHERE header_id = oh.header_id))
             "Created by Name1"*/   --Commeneted by Mahender
         , ol.line_number || '.' || ol.shipment_number Line_Number
         ,ol.ordered_item SKU
         , (SELECT mtl.description
              FROM mtl_system_items_b mtl
              --, oe_order_lines_all ool   --Commeneted by Mahender
             WHERE    --mtl.inventory_item_id = ool.inventory_item_id --Added by Mahender
             mtl.inventory_item_id = ol.inventory_item_id  --Added by Mahender
                   AND mtl.organization_id = ol.ship_from_org_id
                  -- AND ool.line_id = ol.line_id  --Commeneted by Mahender
                  --AND ool.header_id = oh.header_id  --Commeneted by Mahender
                   )
             SKU_Description
         , ol.cancelled_quantity Qty_Cancelled
         ,ol.order_quantity_uom UOM
         ,ol.unit_selling_price Unit_Sell_Price
         , (ol.cancelled_quantity * ol.unit_selling_price) Extended_Sell_Price
         ,TO_CHAR (oh.last_update_date, 'DD-MON-YYYY') Cancelled_Date
         ,TO_CHAR (oh.last_update_date, 'HH24:MI:SS') Cancelled_Time
         ,oh.last_update_date Last_Update_date
         ,fu1.user_name Cancelled_by_ID   --Added by Mahender
         ,fu1.description  Cancelled_by_Name  --Added by Mahender
        /* , (SELECT user_name
              FROM fnd_user
             WHERE user_id = (SELECT last_updated_by
                                FROM oe_order_headers_all
                               WHERE header_id = oh.header_id))
             "Cancelled by ID1"
         ,(SELECT description
              FROM fnd_user
             WHERE user_id = (SELECT last_updated_by
                                FROM oe_order_headers_all
                               WHERE header_id = oh.header_id))
             "Cancelled by Name1" */  --Commeneted by Mahender
         , (SELECT NVL (SUM (araa.amount_applied), 0)
             FROM ar_receivable_applications_all araa
             ,ra_customer_trx_all ract    --Added by Mahender
            WHERE araa.CUSTOMER_TRX_ID = ract.CUSTOMER_TRX_ID   --Added by Mahender
            and ract.INTERFACE_HEADER_ATTRIBUTE1 = TO_CHAR(oh.order_number) --Added by Mahender
--            AND araa.application_ref_id = oh.header_id  --Commeneted by Mahender
            AND ol.line_number = 1 AND ol.shipment_number = 1) 
             Amt_of_Applied_Cash 
         ,oh.transactional_curr_code
         , (SELECT 1
              FROM DUAL
             WHERE ol.line_id = (SELECT MIN (line_id)
                                   FROM oe_order_lines_all
                                  WHERE header_id = oh.header_id))
             Order_Count_Indicator
         ,oep.payment_type_code Payment_Type
FROM mtl_parameters mtp
         ,oe_payments oep
         ,org_organization_definitions ood
         ,oe_order_headers_all oh
         ,oe_order_lines_all ol   
         ,hz_cust_accounts hca
         ,hz_parties hzp
         ,oe_order_types_v otyp
         ,fnd_user fu
         ,fnd_user fu1   --Added by Mahender
    WHERE     oh.header_id = ol.header_id
          AND oh.header_id = oep.header_id(+) 
          AND ol.ship_from_org_id = mtp.organization_id(+)
          AND oh.sold_to_org_id = hca.cust_account_id
          AND hca.party_id = hzp.party_id
          AND oh.created_by = fu.user_id
          AND oh.last_updated_by = fu1.user_id   --Added by Mahender
          AND otyp.order_type_id = oh.order_type_id
          AND mtp.organization_id = ood.organization_id  --Added by Mahender
          --AND oh.ship_from_org_id = ood.organization_id  --Commeneted By Mahender
          AND oh.flow_status_code = 'CANCELLED'
          AND (oep.payment_type_code IN ('CASH','CHECK') OR oep.payment_type_code IS NULL)
          AND oh.last_update_date >=
                   TO_DATE (
                      TO_CHAR (xxeis.eis_rs_xxwc_com_util_pkg.get_date_from
                              ,xxeis.eis_rs_utility.get_date_format || ' HH24:MI:SS')
                     ,xxeis.eis_rs_utility.get_date_format || ' HH24:MI:SS')
                 + 0.25                           --Added by Mahender     
          AND oh.last_update_date <=
                   TO_DATE (
                      TO_CHAR (xxeis.eis_rs_xxwc_com_util_pkg.get_date_to
                              ,xxeis.eis_rs_utility.get_date_format || ' HH24:MI:SS')
                     ,xxeis.eis_rs_utility.get_date_format || ' HH24:MI:SS')
                 + 1.25                                --Added by Mahender/
set scan on define on
prompt Creating View Data for Cancelled Cash Sales Orders
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_OM_CANC_ORDERS_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_OM_CANC_ORDERS_V',660,'','','','','MR020532','XXEIS','Eis Xxwc Om Canc Order V','EXOCSV','','');
--Delete View Columns for EIS_XXWC_OM_CANC_ORDERS_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_OM_CANC_ORDERS_V',660,FALSE);
--Inserting View Columns for EIS_XXWC_OM_CANC_ORDERS_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_CANC_ORDERS_V','UOM',660,'Uom','UOM','','','','MR020532','VARCHAR2','','','Uom','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_CANC_ORDERS_V','SKU',660,'Sku','SKU','','','','MR020532','VARCHAR2','','','Sku','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_CANC_ORDERS_V','AMT_OF_APPLIED_CASH',660,'Amt Of Applied Cash','AMT_OF_APPLIED_CASH','','','','MR020532','NUMBER','','','Amt Of Applied Cash','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_CANC_ORDERS_V','BRANCH',660,'Branch','BRANCH','','','','MR020532','NUMBER','','','Branch','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_CANC_ORDERS_V','BRANCH_NAME',660,'Branch Name','BRANCH_NAME','','','','MR020532','VARCHAR2','','','Branch Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_CANC_ORDERS_V','CANCELLED_BY_ID',660,'Cancelled By Id','CANCELLED_BY_ID','','','','MR020532','VARCHAR2','','','Cancelled By Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_CANC_ORDERS_V','CANCELLED_BY_NAME',660,'Cancelled By Name','CANCELLED_BY_NAME','','','','MR020532','VARCHAR2','','','Cancelled By Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_CANC_ORDERS_V','CANCELLED_DATE',660,'Cancelled Date','CANCELLED_DATE','','','','MR020532','VARCHAR2','','','Cancelled Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_CANC_ORDERS_V','CANCELLED_TIME',660,'Cancelled Time','CANCELLED_TIME','','','','MR020532','VARCHAR2','','','Cancelled Time','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_CANC_ORDERS_V','CREATED_BY_ID',660,'Created By Id','CREATED_BY_ID','','','','MR020532','VARCHAR2','','','Created By Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_CANC_ORDERS_V','CREATED_BY_NAME',660,'Created By Name','CREATED_BY_NAME','','','','MR020532','VARCHAR2','','','Created By Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_CANC_ORDERS_V','CREATED_TIME',660,'Created Time','CREATED_TIME','','','','MR020532','VARCHAR2','','','Created Time','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_CANC_ORDERS_V','CURRENCY',660,'Currency','CURRENCY','','','','MR020532','VARCHAR2','','','Currency','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_CANC_ORDERS_V','CUSTOMER_NAME',660,'Customer Name','CUSTOMER_NAME','','','','MR020532','VARCHAR2','','','Customer Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_CANC_ORDERS_V','CUSTOMER_NUM',660,'Customer Num','CUSTOMER_NUM','','','','MR020532','VARCHAR2','','','Customer Num','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_CANC_ORDERS_V','DISTRICT',660,'District','DISTRICT','','','','MR020532','VARCHAR2','','','District','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_CANC_ORDERS_V','EXTENDED_SELL_PRICE',660,'Extended Sell Price','EXTENDED_SELL_PRICE','','','','MR020532','NUMBER','','','Extended Sell Price','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_CANC_ORDERS_V','LINE_NUMBER',660,'Line Number','LINE_NUMBER','','','','MR020532','VARCHAR2','','','Line Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_CANC_ORDERS_V','ORDEER_NUMBER',660,'Ordeer Number','ORDEER_NUMBER','','','','MR020532','NUMBER','','','Ordeer Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_CANC_ORDERS_V','ORDER_COUNT_INDICATOR',660,'Order Count Indicator','ORDER_COUNT_INDICATOR','','','','MR020532','NUMBER','','','Order Count Indicator','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_CANC_ORDERS_V','ORDER_DATE',660,'Order Date','ORDER_DATE','','','','MR020532','VARCHAR2','','','Order Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_CANC_ORDERS_V','ORDER_TYPE',660,'Order Type','ORDER_TYPE','','','','MR020532','VARCHAR2','','','Order Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_CANC_ORDERS_V','QTY_CANCELLED',660,'Qty Cancelled','QTY_CANCELLED','','','','MR020532','NUMBER','','','Qty Cancelled','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_CANC_ORDERS_V','REGION',660,'Region','REGION','','','','MR020532','VARCHAR2','','','Region','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_CANC_ORDERS_V','SKU_DESCRIPTION',660,'Sku Description','SKU_DESCRIPTION','','','','MR020532','VARCHAR2','','','Sku Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_CANC_ORDERS_V','UNIT_SELL_PRICE',660,'Unit Sell Price','UNIT_SELL_PRICE','','','','MR020532','NUMBER','','','Unit Sell Price','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_CANC_ORDERS_V','PAYMENT_TYPE_CODE',660,'Payment Type Code','PAYMENT_TYPE_CODE','','','','MR020532','VARCHAR2','','','Payment Type Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_CANC_ORDERS_V','ORGANIZATION_CODE',660,'Organization Code','ORGANIZATION_CODE','','','','MR020532','VARCHAR2','','','Organization Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_CANC_ORDERS_V','SHIP_FROM_ORG_ID',660,'Ship From Org Id','SHIP_FROM_ORG_ID','','','','MR020532','NUMBER','','','Ship From Org Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_CANC_ORDERS_V','ATTRIBUTE8',660,'Attribute8','ATTRIBUTE8','','','','MR020532','VARCHAR2','','','Attribute8','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_CANC_ORDERS_V','ATTRIBUTE9',660,'Attribute9','ATTRIBUTE9','','','','MR020532','VARCHAR2','','','Attribute9','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_CANC_ORDERS_V','LAST_UPDATE_DATE',660,'Last Update Date','LAST_UPDATE_DATE','','','','MR020532','DATE','','','Last Update Date','','','');
--Inserting View Components for EIS_XXWC_OM_CANC_ORDERS_V
--Inserting View Component Joins for EIS_XXWC_OM_CANC_ORDERS_V
END;
/
set scan on define on
prompt Creating Report LOV Data for Cancelled Cash Sales Orders
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Cancelled Cash Sales Orders
xxeis.eis_rs_ins.lov( 660,'select distinct ATTRIBUTE9 region from mtl_parameters','','Region Lov','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'select distinct organization_code org_code,organization_name org_name from org_organization_definitions','','Branch Lov','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'select distinct ATTRIBUTE8 District from mtl_parameters','','District Lov','','ANONYMOUS',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Cancelled Cash Sales Orders
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Cancelled Cash Sales Orders
xxeis.eis_rs_utility.delete_report_rows( 'Cancelled Cash Sales Orders' );
--Inserting Report - Cancelled Cash Sales Orders
xxeis.eis_rs_ins.r( 660,'Cancelled Cash Sales Orders','','      Business Purpose/Function:  This report will be used to identify trended behavior related to cash theft.  The report should therefore pull Sales Orders that were completely cancelled (i.e. at the header level) which include only Cash transactions with Cash payment types.
','','','','MR020532','EIS_XXWC_OM_CANC_ORDERS_V','Y','','','MR020532','','N','White Cap Reports','PDF,','CSV,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - Cancelled Cash Sales Orders
xxeis.eis_rs_ins.rc( 'Cancelled Cash Sales Orders',660,'AMT_OF_APPLIED_CASH','Amt Of Applied Cash','Amt Of Applied Cash','','~~~','default','','24','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_CANC_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Cancelled Cash Sales Orders',660,'ORDER_COUNT_INDICATOR','Order Count Indicator','Order Count Indicator','','~T~D~0','default','','26','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_CANC_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Cancelled Cash Sales Orders',660,'LINE_NUMBER','Line Number','Line Number','','','default','','13','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_CANC_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Cancelled Cash Sales Orders',660,'ORDEER_NUMBER','Order Number','Ordeer Number','','~~~','default','','7','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_CANC_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Cancelled Cash Sales Orders',660,'ORDER_DATE','Order Date','Order Date','','','default','','9','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_CANC_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Cancelled Cash Sales Orders',660,'ORDER_TYPE','Order Type','Order Type','','','default','','8','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_CANC_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Cancelled Cash Sales Orders',660,'QTY_CANCELLED','Qty Cancelled','Qty Cancelled','','~~~','default','','16','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_CANC_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Cancelled Cash Sales Orders',660,'REGION','Region','Region','','','default','','3','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_CANC_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Cancelled Cash Sales Orders',660,'SKU','Sku','Sku','','','default','','14','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_CANC_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Cancelled Cash Sales Orders',660,'UNIT_SELL_PRICE','Unit Sell Price','Unit Sell Price','','~~~','default','','18','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_CANC_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Cancelled Cash Sales Orders',660,'UOM','Uom','Uom','','','default','','17','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_CANC_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Cancelled Cash Sales Orders',660,'BRANCH','Branch','Branch','','~~~','default','','1','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_CANC_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Cancelled Cash Sales Orders',660,'BRANCH_NAME','Branch Name','Branch Name','','','default','','2','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_CANC_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Cancelled Cash Sales Orders',660,'CANCELLED_DATE','Cancelled Date','Cancelled Date','','','default','','20','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_CANC_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Cancelled Cash Sales Orders',660,'CANCELLED_TIME','Cancelled Time','Cancelled Time','','','default','','21','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_CANC_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Cancelled Cash Sales Orders',660,'CREATED_TIME','Created Time','Created Time','','','default','','10','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_CANC_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Cancelled Cash Sales Orders',660,'CUSTOMER_NAME','Customer Name','Customer Name','','','default','','6','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_CANC_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Cancelled Cash Sales Orders',660,'CUSTOMER_NUM','Customer Num','Customer Num','','','default','','5','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_CANC_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Cancelled Cash Sales Orders',660,'DISTRICT','District','District','','','default','','4','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_CANC_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Cancelled Cash Sales Orders',660,'EXTENDED_SELL_PRICE','Extended Sell Price','Extended Sell Price','','~~~','default','','19','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_CANC_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Cancelled Cash Sales Orders',660,'CANCELLED_BY_ID','Cancelled By Id','Cancelled By Id','','','default','','22','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_CANC_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Cancelled Cash Sales Orders',660,'CANCELLED_BY_NAME','Cancelled By Name','Cancelled By Name','','','default','','12','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_CANC_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Cancelled Cash Sales Orders',660,'CREATED_BY_ID','Created By Id','Created By Id','','','default','','11','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_CANC_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Cancelled Cash Sales Orders',660,'CREATED_BY_NAME','Created By Name','Created By Name','','','default','','23','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_CANC_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Cancelled Cash Sales Orders',660,'SKU_DESCRIPTION','Sku Description','Sku Description','','','default','','15','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_CANC_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Cancelled Cash Sales Orders',660,'CURRENCY','Currency','Currency','','','default','','25','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_CANC_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Cancelled Cash Sales Orders',660,'PAYMENT_TYPE_CODE','Payment Type Code','Payment Type Code','','','','','27','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_CANC_ORDERS_V','','');
--Inserting Report Parameters - Cancelled Cash Sales Orders
xxeis.eis_rs_ins.rp( 'Cancelled Cash Sales Orders',660,'Region','Region','REGION','IN','Region Lov','','VARCHAR2','N','Y','4','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Cancelled Cash Sales Orders',660,'District','District','DISTRICT','IN','District Lov','','VARCHAR2','N','Y','5','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Cancelled Cash Sales Orders',660,'Branch','Branch','ORGANIZATION_CODE','IN','Branch Lov','','VARCHAR2','N','Y','3','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Cancelled Cash Sales Orders',660,'Date From','Date Range (Cancelled Date)','LAST_UPDATE_DATE','>=','','','DATE','Y','Y','1','','N','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Cancelled Cash Sales Orders',660,'Date To','Date Range (Cancelled Date)','LAST_UPDATE_DATE','<=','','','DATE','Y','Y','2','','N','CONSTANT','MR020532','Y','N','','','');
--Inserting Report Conditions - Cancelled Cash Sales Orders
xxeis.eis_rs_ins.rcn( 'Cancelled Cash Sales Orders',660,'REGION','IN',':Region','','','Y','2','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Cancelled Cash Sales Orders',660,'DISTRICT','IN',':District','','','Y','3','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Cancelled Cash Sales Orders',660,'ORGANIZATION_CODE','IN',':Branch','','','Y','1','Y','MR020532');
--Inserting Report Sorts - Cancelled Cash Sales Orders
xxeis.eis_rs_ins.rs( 'Cancelled Cash Sales Orders',660,'ORDER_DATE','DESC','MR020532','1','');
xxeis.eis_rs_ins.rs( 'Cancelled Cash Sales Orders',660,'LINE_NUMBER','ASC','MR020532','3','');
xxeis.eis_rs_ins.rs( 'Cancelled Cash Sales Orders',660,'ORDEER_NUMBER','DESC','MR020532','2','');
--Inserting Report Triggers - Cancelled Cash Sales Orders
xxeis.eis_rs_ins.rt( 'Cancelled Cash Sales Orders',660,'Begin
xxeis.EIS_RS_XXWC_COM_UTIL_PKG.set_date_from(:Date From);
xxeis.EIS_RS_XXWC_COM_UTIL_PKG.set_date_to(:Date To);
end;','B','Y','MR020532');
--Inserting Report Templates - Cancelled Cash Sales Orders
--Inserting Report Portals - Cancelled Cash Sales Orders
--Inserting Report Dashboards - Cancelled Cash Sales Orders
--Inserting Report Security - Cancelled Cash Sales Orders
xxeis.eis_rs_ins.rsec( 'Cancelled Cash Sales Orders','660','','51044',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Cancelled Cash Sales Orders','660','','51045',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Cancelled Cash Sales Orders','660','','50901',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Cancelled Cash Sales Orders','660','','51025',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Cancelled Cash Sales Orders','660','','50886',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Cancelled Cash Sales Orders','222','','51490',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Cancelled Cash Sales Orders','20005','','50900',660,'MR020532','','');
--Inserting Report Pivots - Cancelled Cash Sales Orders
xxeis.eis_rs_ins.rpivot( 'Cancelled Cash Sales Orders',660,'Pivot 1','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Pivot 1
xxeis.eis_rs_ins.rpivot_dtls( 'Cancelled Cash Sales Orders',660,'Pivot 1','REGION','PAGE_FIELD','','','1','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Cancelled Cash Sales Orders',660,'Pivot 1','DISTRICT','PAGE_FIELD','','','2','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Cancelled Cash Sales Orders',660,'Pivot 1','BRANCH','PAGE_FIELD','','','3','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Cancelled Cash Sales Orders',660,'Pivot 1','CANCELLED_BY_NAME','ROW_FIELD','','','1','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Cancelled Cash Sales Orders',660,'Pivot 1','ORDEER_NUMBER','ROW_FIELD','','','2','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Cancelled Cash Sales Orders',660,'Pivot 1','AMT_OF_APPLIED_CASH','ROW_FIELD','','','3','','');
--Inserting Report Summary Calculation Columns For Pivot- Pivot 1
xxeis.eis_rs_ins.rpivot( 'Cancelled Cash Sales Orders',660,'Pivot 2','2','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Pivot 2
xxeis.eis_rs_ins.rpivot_dtls( 'Cancelled Cash Sales Orders',660,'Pivot 2','REGION','PAGE_FIELD','','','1','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Cancelled Cash Sales Orders',660,'Pivot 2','DISTRICT','PAGE_FIELD','','','2','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Cancelled Cash Sales Orders',660,'Pivot 2','BRANCH_NAME','PAGE_FIELD','','','3','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Cancelled Cash Sales Orders',660,'Pivot 2','CANCELLED_BY_NAME','PAGE_FIELD','','','4','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Cancelled Cash Sales Orders',660,'Pivot 2','ORDEER_NUMBER','ROW_FIELD','','','1','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Cancelled Cash Sales Orders',660,'Pivot 2','AMT_OF_APPLIED_CASH','ROW_FIELD','','','2','','');
--Inserting Report Summary Calculation Columns For Pivot- Pivot 2
END;
/
set scan on define on
