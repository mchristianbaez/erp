grant all on wms_loaded_quantities_v to xxeis;

create or replace synonym XXEIS.wms_loaded_quantities_v for apps.wms_loaded_quantities_v;

grant execute on INV_Globals to xxeis;
grant execute on INV_MATERIAL_STATUS_GRP to xxeis;
grant execute on inv_decimals_pub to xxeis;