--Report Name            : On and Off Schedule Sales for GSA
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View EIS_XXWC_OM_GSA_SALES_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_OM_GSA_SALES_V
Create or replace View XXEIS.EIS_XXWC_OM_GSA_SALES_V
(CUSTOMER_NUMBER,CUSTOMER_NAME,SCHEDULE_PARTS,PRODUCT,PRODUCT_NAME,MONTH1,QUARTER_NUM,REGION,SALES,PERIOD_YEAR,AVERAGECOST,GROSS_PROFIT,ORDERED_DATE,INVOICE_NUMBER,INVOICE_DATE,QTY,UNIT_PRICE,ORDER_NUMBER,HEADER_ID,LINE_ID,CUST_ACCOUNT_ID,PARTY_ID,SITE_USE_ID,CUST_ACCT_SITE_ID,PARTY_SITE_ID,LOCATION_ID,ORGANIZATION_ID,INVENTORY_ITEM_ID,MSI_ORGANIZATION_ID) AS 
SELECT Hca_Bill.Account_Number Customer_Number,
    NVL(hca_bill.account_name,hzp.party_name) customer_name,
    xxeis.eis_rs_xxwc_com_util_pkg.get_schedule_part_flag(ol.header_id,ol.line_id) schedule_parts,
    msi.segment1 product,
    msi.description product_name,
    gps.period_name month1,
    Gps.Period_Year
    ||'Quarter'
    ||Gps.Quarter_Num quarter_num,
    MP.ATTRIBUTE9 REGION,
    NVL(ol.unit_selling_price,0)* NVL (rctl.quantity_invoiced, rctl.quantity_credited) sales,
    GPS.PERIOD_YEAR,
    (NVL (rctl.quantity_invoiced, rctl.quantity_credited)*(NVL (apps.xxwc_mv_routines_pkg.get_order_line_cost (ol.line_id) , 0))) AVERAGECOST,
    ((NVL(ol.unit_selling_price,0)                       * NVL (rctl.quantity_invoiced, rctl.quantity_credited))-(NVL (rctl.quantity_invoiced, rctl.quantity_credited)* NVL (apps.xxwc_mv_routines_pkg.get_order_line_cost (ol.line_id) , 0)) ) gross_profit,
    TRUNC(OH.ORDERED_DATE) ORDERED_DATE,
    rct.trx_number invoice_number,
    TRUNC(rct.trx_date) invoice_date,
    NVL (rctl.quantity_invoiced, rctl.quantity_credited) QTY,
    NVL(ol.unit_selling_price,0) unit_price,
    oh.order_number order_number,
    ---Primary Keys
    oh.header_id,
    ol.line_id,
    hca_bill.cust_account_id,
    hzp.party_id,
    hzcsu_ship_to.site_use_id,
    hcas_ship_to.cust_acct_site_id,
    hzps_ship_to.party_site_id,
    hzl_ship_to.location_id,
    ood.organization_id,
    MSI.INVENTORY_ITEM_ID,
    msi.organization_id msi_organization_id
    --descr#flexfield#start
    --descr#flexfield#end
    --gl#accountff#start
    --gl#accountff#end
  FROM oe_order_headers oh,
    oe_order_lines ol,
    ra_customer_trx rct,
    ra_customer_trx_lines rctl,
    hz_cust_accounts hca_bill,
    hz_cust_accounts hca_ship,
    hz_parties hzp,
    hz_cust_site_uses hzcsu_ship_to,
    hz_cust_acct_sites hcas_ship_to,
    hz_party_sites hzps_ship_to,
    hz_locations hzl_ship_to,
    org_organization_definitions ood,
    mtl_system_items_kfv msi,
    gl_period_statuses gps,
    mtl_parameters mp
  WHERE oh.header_id                     = ol.header_id
  AND rctl.interface_line_context        = 'ORDER ENTRY'
  AND rct.interface_header_context       = 'ORDER ENTRY'
  AND TO_CHAR (oh.order_number)          = rct.interface_header_attribute1 
  AND rctl.interface_line_attribute6     = TO_CHAR (ol.line_id)
  AND oh.sold_to_org_id                  = hca_bill.cust_account_id
  AND oh.ship_to_org_id                  = hca_ship.cust_account_id(+)
  AND hca_ship.cust_account_id           = hcas_ship_to.cust_account_id(+)
  AND hcas_ship_to.party_site_id         = hzps_ship_to.party_site_id(+)
  AND hcas_ship_to.cust_acct_site_id     = hzcsu_ship_to.cust_acct_site_id(+)
  AND hzps_ship_to.location_id           = hzl_ship_to.location_id(+)
  AND oh.ship_from_org_id                = ood.organization_id
  AND ol.inventory_item_id               = msi.inventory_item_id
  AND ol.ship_from_org_id                = msi.organization_id
  AND gps.application_id                 = 101
  AND ood.set_of_books_id                = gps.set_of_books_id
  AND TRUNC(oh.ordered_date) BETWEEN gps.start_date AND gps.end_date
  AND Mp.Organization_Id        = Ol.Ship_From_Org_Id
  AND Hca_Bill.Party_Id         = Hzp.Party_Id
  AND (HZP.GSA_INDICATOR_FLAG   ='Y'
  OR HZCSU_SHIP_TO.GSA_INDICATOR='Y')
/
set scan on define on
prompt Creating View Data for On and Off Schedule Sales for GSA
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_OM_GSA_SALES_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_OM_GSA_SALES_V',660,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Xxwc Om Gsa Sales V','EXOGSV','','');
--Delete View Columns for EIS_XXWC_OM_GSA_SALES_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_OM_GSA_SALES_V',660,FALSE);
--Inserting View Columns for EIS_XXWC_OM_GSA_SALES_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_GSA_SALES_V','SCHEDULE_PARTS',660,'Schedule Parts','SCHEDULE_PARTS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Schedule Parts','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_GSA_SALES_V','GROSS_PROFIT',660,'Gross Profit','GROSS_PROFIT','','','','XXEIS_RS_ADMIN','NUMBER','','','Gross Profit','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_GSA_SALES_V','AVERAGECOST',660,'Averagecost','AVERAGECOST','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Averagecost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_GSA_SALES_V','SALES',660,'Sales','SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_GSA_SALES_V','PRODUCT_NAME',660,'Product Name','PRODUCT_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Product Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_GSA_SALES_V','PRODUCT',660,'Product','PRODUCT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Product','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_GSA_SALES_V','CUSTOMER_NAME',660,'Customer Name','CUSTOMER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_GSA_SALES_V','CUSTOMER_NUMBER',660,'Customer Number','CUSTOMER_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_GSA_SALES_V','MONTH1',660,'Month1','MONTH1','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Month1','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_GSA_SALES_V','REGION',660,'Region','REGION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Region','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_GSA_SALES_V','QUARTER_NUM',660,'Quarter Num','QUARTER_NUM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Quarter Num','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_GSA_SALES_V','ORDERED_DATE',660,'Ordered Date','ORDERED_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Ordered Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_GSA_SALES_V','CUST_ACCOUNT_ID',660,'Cust Account Id','CUST_ACCOUNT_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Cust Account Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_GSA_SALES_V','CUST_ACCT_SITE_ID',660,'Cust Acct Site Id','CUST_ACCT_SITE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Cust Acct Site Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_GSA_SALES_V','HEADER_ID',660,'Header Id','HEADER_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Header Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_GSA_SALES_V','INVENTORY_ITEM_ID',660,'Inventory Item Id','INVENTORY_ITEM_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Inventory Item Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_GSA_SALES_V','LINE_ID',660,'Line Id','LINE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Line Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_GSA_SALES_V','LOCATION_ID',660,'Location Id','LOCATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Location Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_GSA_SALES_V','MSI_ORGANIZATION_ID',660,'Msi Organization Id','MSI_ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Msi Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_GSA_SALES_V','ORGANIZATION_ID',660,'Organization Id','ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_GSA_SALES_V','PARTY_ID',660,'Party Id','PARTY_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Party Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_GSA_SALES_V','PARTY_SITE_ID',660,'Party Site Id','PARTY_SITE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Party Site Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_GSA_SALES_V','SITE_USE_ID',660,'Site Use Id','SITE_USE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Site Use Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_GSA_SALES_V','PERIOD_YEAR',660,'Period Year','PERIOD_YEAR','','','','XXEIS_RS_ADMIN','NUMBER','','','Period Year','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_GSA_SALES_V','INVOICE_DATE',660,'Invoice Date','INVOICE_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Invoice Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_GSA_SALES_V','INVOICE_NUMBER',660,'Invoice Number','INVOICE_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Invoice Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_GSA_SALES_V','QTY',660,'Qty','QTY','','','','XXEIS_RS_ADMIN','NUMBER','','','Qty','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_GSA_SALES_V','UNIT_PRICE',660,'Unit Price','UNIT_PRICE','','','','XXEIS_RS_ADMIN','NUMBER','','','Unit Price','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_GSA_SALES_V','ORDER_NUMBER',660,'Order Number','ORDER_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Order Number','','','');
--Inserting View Components for EIS_XXWC_OM_GSA_SALES_V
--Inserting View Component Joins for EIS_XXWC_OM_GSA_SALES_V
END;
/
set scan on define on
prompt Creating Report Data for On and Off Schedule Sales for GSA
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - On and Off Schedule Sales for GSA
xxeis.eis_rs_utility.delete_report_rows( 'On and Off Schedule Sales for GSA' );
--Inserting Report - On and Off Schedule Sales for GSA
xxeis.eis_rs_ins.r( 660,'On and Off Schedule Sales for GSA','','Report will show on and off schedule item sales by customer and region for the given dates. Detail and Summary information provided.','','','','XXEIS_RS_ADMIN','EIS_XXWC_OM_GSA_SALES_V','Y','','','XXEIS_RS_ADMIN','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - On and Off Schedule Sales for GSA
xxeis.eis_rs_ins.rc( 'On and Off Schedule Sales for GSA',660,'AVERAGECOST','COGS','Averagecost','','~T~D~2','default','','13','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_GSA_SALES_V','','');
xxeis.eis_rs_ins.rc( 'On and Off Schedule Sales for GSA',660,'CUSTOMER_NAME','Master Customer Name','Customer Name','','','default','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_GSA_SALES_V','','');
xxeis.eis_rs_ins.rc( 'On and Off Schedule Sales for GSA',660,'CUSTOMER_NUMBER','Master Customer','Customer Number','','','default','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_GSA_SALES_V','','');
xxeis.eis_rs_ins.rc( 'On and Off Schedule Sales for GSA',660,'GROSS_PROFIT','GM','Gross Profit','','~T~D~2','default','','14','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_GSA_SALES_V','','');
xxeis.eis_rs_ins.rc( 'On and Off Schedule Sales for GSA',660,'REBATE','Rebate','Gross Profit','NUMBER','~T~D~2','default','','12','Y','','','','','','','Decode(EXOGSV.Schedule_Parts,''Yes'',(EXOGSV.Sales*0.0075),0)','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_GSA_SALES_V','','');
xxeis.eis_rs_ins.rc( 'On and Off Schedule Sales for GSA',660,'PRODUCT','Product','Product','','','default','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_GSA_SALES_V','','');
xxeis.eis_rs_ins.rc( 'On and Off Schedule Sales for GSA',660,'PRODUCT_NAME','Description','Product Name','','','default','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_GSA_SALES_V','','');
xxeis.eis_rs_ins.rc( 'On and Off Schedule Sales for GSA',660,'SALES','Sales','Sales','','~T~D~2','default','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_GSA_SALES_V','','');
xxeis.eis_rs_ins.rc( 'On and Off Schedule Sales for GSA',660,'SCHEDULE_PARTS','Sch Yes/No','Schedule Parts','','','default','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_GSA_SALES_V','','');
xxeis.eis_rs_ins.rc( 'On and Off Schedule Sales for GSA',660,'INVOICE_DATE','Invoice Date','Invoice Date','','','default','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_GSA_SALES_V','','');
xxeis.eis_rs_ins.rc( 'On and Off Schedule Sales for GSA',660,'INVOICE_NUMBER','Invoice Number','Invoice Number','','','default','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_GSA_SALES_V','','');
xxeis.eis_rs_ins.rc( 'On and Off Schedule Sales for GSA',660,'QTY','Qty','Qty','','~~~','default','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_GSA_SALES_V','','');
xxeis.eis_rs_ins.rc( 'On and Off Schedule Sales for GSA',660,'UNIT_PRICE','Unit Price','Unit Price','','~T~D~2','default','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_GSA_SALES_V','','');
xxeis.eis_rs_ins.rc( 'On and Off Schedule Sales for GSA',660,'MONTH1','Month','Month1','','','default','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_GSA_SALES_V','','');
xxeis.eis_rs_ins.rc( 'On and Off Schedule Sales for GSA',660,'GM_PER','GM%','Month1','NUMBER','~T~D~2','default','','15','Y','','','','','','','case when (NVL(EXOGSV.sales,0) !=0  AND NVL(EXOGSV.AVERAGECOST,0) !=0) then (((EXOGSV.sales-EXOGSV.AVERAGECOST)/(EXOGSV.sales))*100) WHEN NVL(EXOGSV.AVERAGECOST,0) =0  THEN 100 WHEN nvl(EXOGSV.sales,0) != 0 THEN (((EXOGSV.sales-EXOGSV.AVERAGECOST)/(EXOGSV.sales))*100) else 0 end','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_GSA_SALES_V','','');
--Inserting Report Parameters - On and Off Schedule Sales for GSA
xxeis.eis_rs_ins.rp( 'On and Off Schedule Sales for GSA',660,'Start Date','Start Date','ORDERED_DATE','>=','','','DATE','N','Y','1','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'On and Off Schedule Sales for GSA',660,'End Date','End Date','ORDERED_DATE','<=','','','DATE','N','Y','2','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
--Inserting Report Conditions - On and Off Schedule Sales for GSA
xxeis.eis_rs_ins.rcn( 'On and Off Schedule Sales for GSA',660,'ORDERED_DATE','>=',':Start Date','','','Y','2','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'On and Off Schedule Sales for GSA',660,'ORDERED_DATE','<=',':End Date','','','Y','3','Y','XXEIS_RS_ADMIN');
--Inserting Report Sorts - On and Off Schedule Sales for GSA
--Inserting Report Triggers - On and Off Schedule Sales for GSA
--Inserting Report Templates - On and Off Schedule Sales for GSA
--Inserting Report Portals - On and Off Schedule Sales for GSA
--Inserting Report Dashboards - On and Off Schedule Sales for GSA
--Inserting Report Security - On and Off Schedule Sales for GSA
xxeis.eis_rs_ins.rsec( 'On and Off Schedule Sales for GSA','660','','50926',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'On and Off Schedule Sales for GSA','660','','50927',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'On and Off Schedule Sales for GSA','660','','50928',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'On and Off Schedule Sales for GSA','660','','50929',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'On and Off Schedule Sales for GSA','660','','50931',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'On and Off Schedule Sales for GSA','660','','50930',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'On and Off Schedule Sales for GSA','660','','21623',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'On and Off Schedule Sales for GSA','701','','50546',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'On and Off Schedule Sales for GSA','660','','50856',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'On and Off Schedule Sales for GSA','660','','50857',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'On and Off Schedule Sales for GSA','660','','50858',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'On and Off Schedule Sales for GSA','660','','50859',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'On and Off Schedule Sales for GSA','660','','50860',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'On and Off Schedule Sales for GSA','660','','50861',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'On and Off Schedule Sales for GSA','20005','','50880',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'On and Off Schedule Sales for GSA','','LC053655','',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'On and Off Schedule Sales for GSA','','10010432','',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'On and Off Schedule Sales for GSA','','RB054040','',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'On and Off Schedule Sales for GSA','','RV003897','',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'On and Off Schedule Sales for GSA','','SS084202','',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'On and Off Schedule Sales for GSA','','SO004816','',660,'XXEIS_RS_ADMIN','','');
--Inserting Report Pivots - On and Off Schedule Sales for GSA
xxeis.eis_rs_ins.rpivot( 'On and Off Schedule Sales for GSA',660,'Pivot','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rs_ins.rpivot_dtls( 'On and Off Schedule Sales for GSA',660,'Pivot','CUSTOMER_NAME','ROW_FIELD','','','4','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'On and Off Schedule Sales for GSA',660,'Pivot','CUSTOMER_NUMBER','ROW_FIELD','','','3','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'On and Off Schedule Sales for GSA',660,'Pivot','REBATE','DATA_FIELD','SUM','','2','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'On and Off Schedule Sales for GSA',660,'Pivot','SALES','DATA_FIELD','SUM','','1','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'On and Off Schedule Sales for GSA',660,'Pivot','SCHEDULE_PARTS','ROW_FIELD','','','5','1','xlNormal');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
END;
/
set scan on define on
