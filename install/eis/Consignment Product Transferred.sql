--Report Name            : Consignment Product Transferred
--Import Instructions : 1. If RTF template needs to be uploaded to report automatically then move the RTF template to physical path of APPS_DATA_FILE_DIR oracle directory. Check dba_directories view to get the physical path of oracle directory. 
--                         (If you want to use any other oracle directory other than APPS_DATA_FILE_DIR then replace APPS_DATA_FILE_DIR with your oracle directory name below.)
--                      2. Make sure XXEIS schema has read/write permissions to above used oracle directory and give full permissions to the directory at OS level.
--                      3. Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
exec xxeis.eis_rs_ins.g_rtf_dir := 'APPS_DATA_FILE_DIR' ;
--Creating View EIS_XXWC_AP_CONSN_PRDTRNS_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_AP_CONSN_PRDTRNS_V
Create or replace View XXEIS.EIS_XXWC_AP_CONSN_PRDTRNS_V
(WHITECAP_PART_NUMBER,SUPPLIER_PART_NUMBER,ITEM_DESCRIPTION,DESCRIPTION,FROM_LOCATION,TO_LOCATION,QUANTITY_TRANSFERED,RECEIPT_DATE,TRANSFER_NUMBER,UNIT_COST,EXTENDED_COST,SUPPLIER_NUMBER,PURCHASE_REQ,SALES_ORDER,FROM_ORGANIZATION_ID,TO_ORGANIZATION_ID,QUANTITY_RECEIVED,HEADER_ID,LINE_ID,INVENTORY_ITEM_ID,ORGANIZATION_ID,MSI#HDS#LOB,MSI#HDS#DROP_SHIPMENT_ELIGAB,MSI#HDS#INVOICE_UOM,MSI#HDS#PRODUCT_ID,MSI#HDS#VENDOR_PART_NUMBER,MSI#HDS#UNSPSC_CODE,MSI#HDS#UPC_PRIMARY,MSI#HDS#SKU_DESCRIPTION,MSI#WC#CA_PROP_65,MSI#WC#COUNTRY_OF_ORIGIN,MSI#WC#ORM_D_FLAG,MSI#WC#STORE_VELOCITY,MSI#WC#DC_VELOCITY,MSI#WC#YEARLY_STORE_VELOCITY,MSI#WC#YEARLY_DC_VELOCITY,MSI#WC#PRISM_PART_NUMBER,MSI#WC#HAZMAT_DESCRIPTION,MSI#WC#HAZMAT_CONTAINER,MSI#WC#GTP_INDICATOR,MSI#WC#LAST_LEAD_TIME,MSI#WC#AMU,MSI#WC#RESERVE_STOCK,MSI#WC#TAXWARE_CODE,MSI#WC#AVERAGE_UNITS,MSI#WC#PRODUCT_CODE,MSI#WC#IMPORT_DUTY_,MSI#WC#KEEP_ITEM_ACTIVE,MSI#WC#PESTICIDE_FLAG,MSI#WC#CALC_LEAD_TIME,MSI#WC#VOC_GL,MSI#WC#PESTICIDE_FLAG_STATE,MSI#WC#VOC_CATEGORY,MSI#WC#VOC_SUB_CATEGORY,MSI#WC#MSDS_#,MSI#WC#HAZMAT_PACKAGING_GROU,OOH#RENTAL_TERM,OOH#ORDER_CHANNEL,OL#ESTIMATED_RETURN_DATE,OL#RERENT_PO,OL#FORCE_SHIP,OL#RENTAL_DATE,OL#ITEM_ON_BLOWOUT,OL#POF_STD_LINE,OL#RERENTAL_BILLING_TERMS,OL#PRICING_GUARDRAIL,OL#PRINT_EXPIRED_PRODUCT_DIS,OL#RENTAL_CHARGE,OL#VENDOR_QUOTE_COST,OL#PO_COST_FOR_VENDOR_QUOTE,OL#SERIAL_NUMBER,OL#ENGINEERING_COST,OL#APPLICATION_METHOD) AS 
SELECT MSI.SEGMENT1 WHITECAP_PART_NUMBER,
    --msi.organization_id,
    xxeis.eis_rs_xxwc_com_util_pkg.get_item_crossref(msi.inventory_item_id,msi.organization_id) supplier_part_number,
    msi.description item_description,
    Msi.Description,
    ood1.organization_code from_location,
    ood2.organization_code to_location,
    Ol.Ordered_Quantity Quantity_Transfered,
    TRUNC(rsh.creation_date) receipt_date,
    ooh.order_number transfer_number,
    DECODE(nvl(APPS.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id),0),0,ol.unit_cost,APPS.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id)) Unit_Cost,
    (DECODE(nvl(APPS.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id),0),0,ol.unit_cost,APPS.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id)) * ol.ordered_quantity) extended_cost,
    xxeis.eis_rs_xxwc_com_util_pkg.get_vendor_number(msi.inventory_item_id,msi.organization_id) supplier_number,
    porh.segment1 purchase_req,
    ooh.order_number sales_order,
    from_organization_id,
    to_organization_id,
    rsl.quantity_received ,
    ---Primary Keys
    ooh.header_id ,
    ol.line_id ,
    msi.inventory_item_id ,
    msi.organization_id
    --descr#flexfield#start
    ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'HDS',MSI.ATTRIBUTE1, NULL) MSI#HDS#LOB ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'HDS',xxeis.eis_rs_dff.decode_valueset( 'Yes_No',MSI.ATTRIBUTE10,'F'), NULL) MSI#HDS#Drop_Shipment_Eligab ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'HDS',MSI.ATTRIBUTE15, NULL) MSI#HDS#Invoice_UOM ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'HDS',MSI.ATTRIBUTE2, NULL) MSI#HDS#Product_ID ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'HDS',MSI.ATTRIBUTE3, NULL) MSI#HDS#Vendor_Part_Number ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'HDS',MSI.ATTRIBUTE4, NULL) MSI#HDS#UNSPSC_Code ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'HDS',MSI.ATTRIBUTE5, NULL) MSI#HDS#UPC_Primary ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'HDS',MSI.ATTRIBUTE6, NULL) MSI#HDS#SKU_Description ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE1, NULL) MSI#WC#CA_Prop_65 ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE10, NULL) MSI#WC#Country_of_Origin ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',xxeis.eis_rs_dff.decode_valueset( 'Yes_No',MSI.ATTRIBUTE11,'F'), NULL) MSI#WC#ORM_D_Flag ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE12, NULL) MSI#WC#Store_Velocity ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE13, NULL) MSI#WC#DC_Velocity ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE14, NULL) MSI#WC#Yearly_Store_Velocity ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE15, NULL) MSI#WC#Yearly_DC_Velocity ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE16, NULL) MSI#WC#PRISM_Part_Number ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE17, NULL) MSI#WC#Hazmat_Description ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',xxeis.eis_rs_dff.decode_valueset( 'XXWC HAZMAT CONTAINER',MSI.ATTRIBUTE18,'I'), NULL) MSI#WC#Hazmat_Container ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE19, NULL) MSI#WC#GTP_Indicator ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE2, NULL) MSI#WC#Last_Lead_Time ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE20, NULL) MSI#WC#AMU ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE21, NULL) MSI#WC#Reserve_Stock ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',xxeis.eis_rs_dff.decode_valueset( 'XXWC TAXWARE CODE',MSI.ATTRIBUTE22,'I'), NULL) MSI#WC#Taxware_Code ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE25, NULL) MSI#WC#Average_Units ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE26, NULL) MSI#WC#Product_code ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE27, NULL) MSI#WC#Import_Duty_ ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',xxeis.eis_rs_dff.decode_valueset( 'Yes_No',MSI.ATTRIBUTE29,'F'), NULL) MSI#WC#Keep_Item_Active ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',xxeis.eis_rs_dff.decode_valueset( 'Yes_No',MSI.ATTRIBUTE3,'F'), NULL) MSI#WC#Pesticide_Flag ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE30, NULL) MSI#WC#Calc_Lead_Time ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE4, NULL) MSI#WC#VOC_GL ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE5, NULL) MSI#WC#Pesticide_Flag_State ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE6, NULL) MSI#WC#VOC_Category ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE7, NULL) MSI#WC#VOC_Sub_Category ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE8, NULL) MSI#WC#MSDS_# ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',xxeis.eis_rs_dff.decode_valueset( 'XXWC_HAZMAT_PACKAGE_GROUP',MSI.ATTRIBUTE9,'I'), NULL) MSI#WC#Hazmat_Packaging_Grou ,
    xxeis.eis_rs_dff.decode_valueset( 'RENTAL TYPE',OOH.ATTRIBUTE1,'I') OOH#Rental_Term ,
    xxeis.eis_rs_dff.decode_valueset( 'WC_CUS_ORDER_CHANNEL',OOH.ATTRIBUTE2,'I') OOH#Order_Channel ,
    OL.ATTRIBUTE1 OL#Estimated_Return_Date ,
    OL.ATTRIBUTE10 OL#ReRent_PO ,
    OL.ATTRIBUTE11 OL#Force_Ship ,
    OL.ATTRIBUTE12 OL#Rental_Date ,
    xxeis.eis_rs_dff.decode_valueset( 'Yes_No',OL.ATTRIBUTE15,'F') OL#Item_on_Blowout ,
    OL.ATTRIBUTE19 OL#POF_Std_Line ,
    xxeis.eis_rs_dff.decode_valueset( 'ReRental Billing Type',OL.ATTRIBUTE2,'I') OL#ReRental_Billing_Terms ,
    OL.ATTRIBUTE20 OL#Pricing_Guardrail ,
    xxeis.eis_rs_dff.decode_valueset( 'Yes_No',OL.ATTRIBUTE3,'F') OL#Print_Expired_Product_Dis ,
    OL.ATTRIBUTE4 OL#Rental_Charge ,
    OL.ATTRIBUTE5 OL#Vendor_Quote_Cost ,
    OL.ATTRIBUTE6 OL#PO_Cost_for_Vendor_Quote ,
    OL.ATTRIBUTE7 OL#Serial_Number ,
    OL.ATTRIBUTE8 OL#Engineering_Cost ,
    xxeis.eis_rs_dff.decode_valueset( 'XXWC_CSP_APP_METHOD',OL.ATTRIBUTE9,'I') OL#Application_Method
    --descr#flexfield#end
    --gl#accountff#start
    --gl#accountff#end
  FROM OE_ORDER_HEADERS OOH,
    Po_Requisition_Headers Porh,
    Po_Requisition_Lines Porl,
    OE_ORDER_LINES OL,
    PO_SYSTEM_PARAMETERS POSP,
    Rcv_Shipment_Lines Rsl,
    RCV_SHIPMENT_HEADERS RSH,
    org_organization_definitions ood1,
    Org_Organization_Definitions Ood2,
    MTL_SYSTEM_ITEMS_KFV MSI,
    MTL_MFG_PART_NUMBERS MMPN,
    mtl_manufacturers mmfg
  WHERE OOH.ORDER_SOURCE_ID       = POSP.ORDER_SOURCE_ID
  AND porh.org_id                 = posp.org_id
  AND porh.requisition_header_id  = ol.source_document_id
  AND porl.requisition_line_id    = ol.source_document_line_id
  AND porh.requisition_header_id  = porl.requisition_header_id
  AND ooh.org_id                  = posp.org_id
  AND ol.header_id                = ooh.header_id
  AND msi.consigned_flag          = 1
  AND OL.SOURCE_DOCUMENT_LINE_ID IS NOT NULL
  AND porh.type_lookup_code       = 'INTERNAL'
  AND ol.cancelled_flag          <> 'Y'
  AND rsl.requisition_line_id(+)  = porl.requisition_line_id
  AND rsh.shipment_header_id(+)   = rsl.shipment_header_id
  AND RSL.SOURCE_DOCUMENT_CODE(+) = 'REQ'
  AND RSL.FROM_ORGANIZATION_ID    = OOD1.ORGANIZATION_ID(+)
  AND rsl.to_organization_id      = ood2.organization_id(+)
  AND MSI.INVENTORY_ITEM_ID       = OL.INVENTORY_ITEM_ID
  AND msi.organization_id         = ol.ship_from_org_id
  AND MSI.INVENTORY_ITEM_ID       = MMPN.INVENTORY_ITEM_ID(+)
  AND MSI.ORGANIZATION_ID         = MMPN.ORGANIZATION_ID(+)
  AND MMPN.MANUFACTURER_ID        = MMFG.MANUFACTURER_ID(+)
/
set scan on define on
prompt Creating View Data for Consignment Product Transferred
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_AP_CONSN_PRDTRNS_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_AP_CONSN_PRDTRNS_V',200,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Ap Consign Prod Trans V','EXACPV','','');
--Delete View Columns for EIS_XXWC_AP_CONSN_PRDTRNS_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_AP_CONSN_PRDTRNS_V',200,FALSE);
--Inserting View Columns for EIS_XXWC_AP_CONSN_PRDTRNS_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','WHITECAP_PART_NUMBER',200,'Whitecap Part Number','WHITECAP_PART_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Whitecap Part Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','SUPPLIER_PART_NUMBER',200,'Supplier Part Number','SUPPLIER_PART_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Supplier Part Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','DESCRIPTION',200,'Description','DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','FROM_LOCATION',200,'From Location','FROM_LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','From Location','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','TO_LOCATION',200,'To Location','TO_LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','To Location','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','QUANTITY_TRANSFERED',200,'Quantity Transfered','QUANTITY_TRANSFERED','','','','XXEIS_RS_ADMIN','NUMBER','','','Quantity Transfered','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','UNIT_COST',200,'Unit Cost','UNIT_COST','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Unit Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','EXTENDED_COST',200,'Extended Cost','EXTENDED_COST','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Extended Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','RECEIPT_DATE',200,'Receipt Date','RECEIPT_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Receipt Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','TRANSFER_NUMBER',200,'Transfer Number','TRANSFER_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Transfer Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','QUANTITY_RECEIVED',200,'Quantity Received','QUANTITY_RECEIVED','','','','XXEIS_RS_ADMIN','NUMBER','','','Quantity Received','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','SUPPLIER_NUMBER',200,'Supplier Number','SUPPLIER_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Supplier Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','PURCHASE_REQ',200,'Purchase Req','PURCHASE_REQ','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Purchase Req','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','SALES_ORDER',200,'Sales Order','SALES_ORDER','','','','XXEIS_RS_ADMIN','NUMBER','','','Sales Order','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','FROM_ORGANIZATION_ID',200,'From Organization Id','FROM_ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','From Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','ITEM_DESCRIPTION',200,'Item Description','ITEM_DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Item Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','TO_ORGANIZATION_ID',200,'To Organization Id','TO_ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','To Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','HEADER_ID',200,'Header Id','HEADER_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Header Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','INVENTORY_ITEM_ID',200,'Inventory Item Id','INVENTORY_ITEM_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Inventory Item Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','LINE_ID',200,'Line Id','LINE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Line Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','ORGANIZATION_ID',200,'Organization Id','ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','MSI#HDS#LOB',200,'Descriptive flexfield: Items Column Name: LOB Context: HDS','MSI#HDS#LOB','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE1','Msi#Hds#Lob','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','MSI#HDS#DROP_SHIPMENT_ELIGAB',200,'Descriptive flexfield: Items Column Name: Drop Shipment Eligable Context: HDS','MSI#HDS#Drop_Shipment_Eligab','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE10','Msi#Hds#Drop Shipment Eligable','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','MSI#HDS#INVOICE_UOM',200,'Descriptive flexfield: Items Column Name: Invoice UOM Context: HDS','MSI#HDS#Invoice_UOM','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE15','Msi#Hds#Invoice Uom','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','MSI#HDS#PRODUCT_ID',200,'Descriptive flexfield: Items Column Name: Product ID Context: HDS','MSI#HDS#Product_ID','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE2','Msi#Hds#Product Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','MSI#HDS#VENDOR_PART_NUMBER',200,'Descriptive flexfield: Items Column Name: Vendor Part Number Context: HDS','MSI#HDS#Vendor_Part_Number','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE3','Msi#Hds#Vendor Part Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','MSI#HDS#UNSPSC_CODE',200,'Descriptive flexfield: Items Column Name: UNSPSC Code Context: HDS','MSI#HDS#UNSPSC_Code','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE4','Msi#Hds#Unspsc Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','MSI#HDS#UPC_PRIMARY',200,'Descriptive flexfield: Items Column Name: UPC Primary Context: HDS','MSI#HDS#UPC_Primary','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE5','Msi#Hds#Upc Primary','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','MSI#HDS#SKU_DESCRIPTION',200,'Descriptive flexfield: Items Column Name: SKU Description Context: HDS','MSI#HDS#SKU_Description','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE6','Msi#Hds#Sku Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','MSI#WC#CA_PROP_65',200,'Descriptive flexfield: Items Column Name: CA Prop 65 Context: WC','MSI#WC#CA_Prop_65','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE1','Msi#Wc#Ca Prop 65','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','MSI#WC#COUNTRY_OF_ORIGIN',200,'Descriptive flexfield: Items Column Name: Country of Origin Context: WC','MSI#WC#Country_of_Origin','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE10','Msi#Wc#Country Of Origin','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','MSI#WC#ORM_D_FLAG',200,'Descriptive flexfield: Items Column Name: ORM-D Flag Context: WC','MSI#WC#ORM_D_Flag','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE11','Msi#Wc#Orm-D Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','MSI#WC#STORE_VELOCITY',200,'Descriptive flexfield: Items Column Name: Store Velocity Context: WC','MSI#WC#Store_Velocity','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE12','Msi#Wc#Store Velocity','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','MSI#WC#DC_VELOCITY',200,'Descriptive flexfield: Items Column Name: DC Velocity Context: WC','MSI#WC#DC_Velocity','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE13','Msi#Wc#Dc Velocity','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','MSI#WC#YEARLY_STORE_VELOCITY',200,'Descriptive flexfield: Items Column Name: Yearly Store Velocity Context: WC','MSI#WC#Yearly_Store_Velocity','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE14','Msi#Wc#Yearly Store Velocity','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','MSI#WC#YEARLY_DC_VELOCITY',200,'Descriptive flexfield: Items Column Name: Yearly DC Velocity Context: WC','MSI#WC#Yearly_DC_Velocity','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE15','Msi#Wc#Yearly Dc Velocity','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','MSI#WC#PRISM_PART_NUMBER',200,'Descriptive flexfield: Items Column Name: PRISM Part Number Context: WC','MSI#WC#PRISM_Part_Number','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE16','Msi#Wc#Prism Part Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','MSI#WC#HAZMAT_DESCRIPTION',200,'Descriptive flexfield: Items Column Name: Hazmat Description Context: WC','MSI#WC#Hazmat_Description','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE17','Msi#Wc#Hazmat Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','MSI#WC#HAZMAT_CONTAINER',200,'Descriptive flexfield: Items Column Name: Hazmat Container Context: WC','MSI#WC#Hazmat_Container','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE18','Msi#Wc#Hazmat Container','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','MSI#WC#GTP_INDICATOR',200,'Descriptive flexfield: Items Column Name: GTP Indicator Context: WC','MSI#WC#GTP_Indicator','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE19','Msi#Wc#Gtp Indicator','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','MSI#WC#LAST_LEAD_TIME',200,'Descriptive flexfield: Items Column Name: Last Lead Time Context: WC','MSI#WC#Last_Lead_Time','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE2','Msi#Wc#Last Lead Time','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','MSI#WC#AMU',200,'Descriptive flexfield: Items Column Name: AMU Context: WC','MSI#WC#AMU','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE20','Msi#Wc#Amu','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','MSI#WC#RESERVE_STOCK',200,'Descriptive flexfield: Items Column Name: Reserve Stock Context: WC','MSI#WC#Reserve_Stock','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE21','Msi#Wc#Reserve Stock','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','MSI#WC#TAXWARE_CODE',200,'Descriptive flexfield: Items Column Name: Taxware Code Context: WC','MSI#WC#Taxware_Code','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE22','Msi#Wc#Taxware Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','MSI#WC#AVERAGE_UNITS',200,'Descriptive flexfield: Items Column Name: Average Units Context: WC','MSI#WC#Average_Units','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE25','Msi#Wc#Average Units','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','MSI#WC#PRODUCT_CODE',200,'Descriptive flexfield: Items Column Name: Product code Context: WC','MSI#WC#Product_code','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE26','Msi#Wc#Product Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','MSI#WC#IMPORT_DUTY_',200,'Descriptive flexfield: Items Column Name: Import Duty % Context: WC','MSI#WC#Import_Duty_','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE27','Msi#Wc#Import Duty %','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','MSI#WC#KEEP_ITEM_ACTIVE',200,'Descriptive flexfield: Items Column Name: Keep Item Active Context: WC','MSI#WC#Keep_Item_Active','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE29','Msi#Wc#Keep Item Active','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','MSI#WC#PESTICIDE_FLAG',200,'Descriptive flexfield: Items Column Name: Pesticide Flag Context: WC','MSI#WC#Pesticide_Flag','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE3','Msi#Wc#Pesticide Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','MSI#WC#CALC_LEAD_TIME',200,'Descriptive flexfield: Items Column Name: Calc Lead Time Context: WC','MSI#WC#Calc_Lead_Time','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE30','Msi#Wc#Calc Lead Time','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','MSI#WC#VOC_GL',200,'Descriptive flexfield: Items Column Name: VOC G/L Context: WC','MSI#WC#VOC_GL','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE4','Msi#Wc#Voc G/L','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','MSI#WC#PESTICIDE_FLAG_STATE',200,'Descriptive flexfield: Items Column Name: Pesticide Flag State Context: WC','MSI#WC#Pesticide_Flag_State','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE5','Msi#Wc#Pesticide Flag State','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','MSI#WC#VOC_CATEGORY',200,'Descriptive flexfield: Items Column Name: VOC Category Context: WC','MSI#WC#VOC_Category','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE6','Msi#Wc#Voc Category','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','MSI#WC#VOC_SUB_CATEGORY',200,'Descriptive flexfield: Items Column Name: VOC Sub Category Context: WC','MSI#WC#VOC_Sub_Category','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE7','Msi#Wc#Voc Sub Category','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','MSI#WC#MSDS_#',200,'Descriptive flexfield: Items Column Name: MSDS # Context: WC','MSI#WC#MSDS_#','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE8','Msi#Wc#Msds #','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','MSI#WC#HAZMAT_PACKAGING_GROU',200,'Descriptive flexfield: Items Column Name: Hazmat Packaging Group Context: WC','MSI#WC#Hazmat_Packaging_Grou','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE9','Msi#Wc#Hazmat Packaging Group','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','OOH#RENTAL_TERM',200,'Descriptive flexfield: Additional Header Information Column Name: Rental Term','OOH#Rental_Term','','','','XXEIS_RS_ADMIN','VARCHAR2','OE_ORDER_HEADERS_ALL','ATTRIBUTE1','Ooh#Rental Term','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','OOH#ORDER_CHANNEL',200,'Descriptive flexfield: Additional Header Information Column Name: Order Channel','OOH#Order_Channel','','','','XXEIS_RS_ADMIN','VARCHAR2','OE_ORDER_HEADERS_ALL','ATTRIBUTE2','Ooh#Order Channel','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','OL#ESTIMATED_RETURN_DATE',200,'Descriptive flexfield: Additional Line Attribute Information Column Name: Estimated Return Date','OL#Estimated_Return_Date','','','','XXEIS_RS_ADMIN','VARCHAR2','OE_ORDER_LINES_ALL','ATTRIBUTE1','Ol#Estimated Return Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','OL#RERENT_PO',200,'Descriptive flexfield: Additional Line Attribute Information Column Name: ReRent PO','OL#ReRent_PO','','','','XXEIS_RS_ADMIN','VARCHAR2','OE_ORDER_LINES_ALL','ATTRIBUTE10','Ol#Rerent Po','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','OL#FORCE_SHIP',200,'Descriptive flexfield: Additional Line Attribute Information Column Name: Force Ship','OL#Force_Ship','','','','XXEIS_RS_ADMIN','VARCHAR2','OE_ORDER_LINES_ALL','ATTRIBUTE11','Ol#Force Ship','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','OL#RENTAL_DATE',200,'Descriptive flexfield: Additional Line Attribute Information Column Name: Rental Date','OL#Rental_Date','','','','XXEIS_RS_ADMIN','VARCHAR2','OE_ORDER_LINES_ALL','ATTRIBUTE12','Ol#Rental Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','OL#ITEM_ON_BLOWOUT',200,'Descriptive flexfield: Additional Line Attribute Information Column Name: Item on Blowout?','OL#Item_on_Blowout','','','','XXEIS_RS_ADMIN','VARCHAR2','OE_ORDER_LINES_ALL','ATTRIBUTE15','Ol#Item On Blowout?','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','OL#POF_STD_LINE',200,'Descriptive flexfield: Additional Line Attribute Information Column Name: POF Std Line','OL#POF_Std_Line','','','','XXEIS_RS_ADMIN','VARCHAR2','OE_ORDER_LINES_ALL','ATTRIBUTE19','Ol#Pof Std Line','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','OL#RERENTAL_BILLING_TERMS',200,'Descriptive flexfield: Additional Line Attribute Information Column Name: ReRental Billing Terms','OL#ReRental_Billing_Terms','','','','XXEIS_RS_ADMIN','VARCHAR2','OE_ORDER_LINES_ALL','ATTRIBUTE2','Ol#Rerental Billing Terms','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','OL#PRICING_GUARDRAIL',200,'Descriptive flexfield: Additional Line Attribute Information Column Name: Pricing Guardrail','OL#Pricing_Guardrail','','','','XXEIS_RS_ADMIN','VARCHAR2','OE_ORDER_LINES_ALL','ATTRIBUTE20','Ol#Pricing Guardrail','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','OL#PRINT_EXPIRED_PRODUCT_DIS',200,'Descriptive flexfield: Additional Line Attribute Information Column Name: Print Expired Product Disclaim','OL#Print_Expired_Product_Dis','','','','XXEIS_RS_ADMIN','VARCHAR2','OE_ORDER_LINES_ALL','ATTRIBUTE3','Ol#Print Expired Product Disclaim','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','OL#RENTAL_CHARGE',200,'Descriptive flexfield: Additional Line Attribute Information Column Name: Rental Charge','OL#Rental_Charge','','','','XXEIS_RS_ADMIN','VARCHAR2','OE_ORDER_LINES_ALL','ATTRIBUTE4','Ol#Rental Charge','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','OL#VENDOR_QUOTE_COST',200,'Descriptive flexfield: Additional Line Attribute Information Column Name: Vendor Quote Cost','OL#Vendor_Quote_Cost','','','','XXEIS_RS_ADMIN','VARCHAR2','OE_ORDER_LINES_ALL','ATTRIBUTE5','Ol#Vendor Quote Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','OL#PO_COST_FOR_VENDOR_QUOTE',200,'Descriptive flexfield: Additional Line Attribute Information Column Name: PO Cost for Vendor Quote','OL#PO_Cost_for_Vendor_Quote','','','','XXEIS_RS_ADMIN','VARCHAR2','OE_ORDER_LINES_ALL','ATTRIBUTE6','Ol#Po Cost For Vendor Quote','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','OL#SERIAL_NUMBER',200,'Descriptive flexfield: Additional Line Attribute Information Column Name: Serial Number','OL#Serial_Number','','','','XXEIS_RS_ADMIN','VARCHAR2','OE_ORDER_LINES_ALL','ATTRIBUTE7','Ol#Serial Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','OL#ENGINEERING_COST',200,'Descriptive flexfield: Additional Line Attribute Information Column Name: Engineering Cost','OL#Engineering_Cost','','','','XXEIS_RS_ADMIN','VARCHAR2','OE_ORDER_LINES_ALL','ATTRIBUTE8','Ol#Engineering Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','OL#APPLICATION_METHOD',200,'Descriptive flexfield: Additional Line Attribute Information Column Name: Application Method','OL#Application_Method','','','','XXEIS_RS_ADMIN','VARCHAR2','OE_ORDER_LINES_ALL','ATTRIBUTE9','Ol#Application Method','','','');
--Inserting View Components for EIS_XXWC_AP_CONSN_PRDTRNS_V
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','OE_ORDER_HEADERS_ALL',200,'OE_ORDER_HEADERS_ALL','OOH','OOH','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Oe Order Headers All Stores Header Information For','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','OE_ORDER_LINES_ALL',200,'OE_ORDER_LINES_ALL','OL','OL','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Oe Order Lines All Stores Information For All Orde','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','MTL_SYSTEM_ITEMS_KFV',200,'MTL_SYSTEM_ITEMS_B','MSI','MSI','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Inventory Item Definitions','','','','');
--Inserting View Component Joins for EIS_XXWC_AP_CONSN_PRDTRNS_V
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','OE_ORDER_HEADERS_ALL','OOH',200,'EXACPV.HEADER_ID','=','OOH.HEADER_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','OE_ORDER_LINES_ALL','OL',200,'EXACPV.LINE_ID','=','OL.LINE_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','MTL_SYSTEM_ITEMS_KFV','MSI',200,'EXACPV.INVENTORY_ITEM_ID','=','MSI.INVENTORY_ITEM_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_AP_CONSN_PRDTRNS_V','MTL_SYSTEM_ITEMS_KFV','MSI',200,'EXACPV.ORGANIZATION_ID','=','MSI.ORGANIZATION_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
END;
/
set scan on define on
prompt Creating Report LOV Data for Consignment Product Transferred
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Consignment Product Transferred
xxeis.eis_rs_ins.lov( 200,'SELECT distinct segment1
   FROM PO_VENDORS
   order by segment1','','SUPPLIER_NUMBER','SUPPLIER_NUMBER','XXEIS_RS_ADMIN',NULL,'','','');
END;
/
set scan on define on
prompt Creating Report Data for Consignment Product Transferred
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Consignment Product Transferred
xxeis.eis_rs_utility.delete_report_rows( 'Consignment Product Transferred' );
--Inserting Report - Consignment Product Transferred
xxeis.eis_rs_ins.r( 200,'Consignment Product Transferred','','Detailed listing of consignment product transferred for a specific time period','','','','XXEIS_RS_ADMIN','EIS_XXWC_AP_CONSN_PRDTRNS_V','Y','','','XXEIS_RS_ADMIN','','N','White Cap Reports','','EXCEL,','N');
--Inserting Report Columns - Consignment Product Transferred
xxeis.eis_rs_ins.rc( 'Consignment Product Transferred',200,'DESCRIPTION','Description','Description','','','default','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AP_CONSN_PRDTRNS_V','','');
xxeis.eis_rs_ins.rc( 'Consignment Product Transferred',200,'EXTENDED_COST','Extended Cost','Extended Cost','','~T~D~2','default','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AP_CONSN_PRDTRNS_V','','');
xxeis.eis_rs_ins.rc( 'Consignment Product Transferred',200,'FROM_LOCATION','From Location','From Location','','','default','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AP_CONSN_PRDTRNS_V','','');
xxeis.eis_rs_ins.rc( 'Consignment Product Transferred',200,'QUANTITY_TRANSFERED','Quantity','Quantity Transfered','','~~~','default','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AP_CONSN_PRDTRNS_V','','');
xxeis.eis_rs_ins.rc( 'Consignment Product Transferred',200,'SUPPLIER_PART_NUMBER','Supplier Part Number','Supplier Part Number','','','default','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AP_CONSN_PRDTRNS_V','','');
xxeis.eis_rs_ins.rc( 'Consignment Product Transferred',200,'TO_LOCATION','To Location','To Location','','','default','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AP_CONSN_PRDTRNS_V','','');
xxeis.eis_rs_ins.rc( 'Consignment Product Transferred',200,'UNIT_COST','Unit Cost','Unit Cost','','~T~D~2','default','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AP_CONSN_PRDTRNS_V','','');
xxeis.eis_rs_ins.rc( 'Consignment Product Transferred',200,'WHITECAP_PART_NUMBER','White Cap Part Number','Whitecap Part Number','','','default','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AP_CONSN_PRDTRNS_V','','');
xxeis.eis_rs_ins.rc( 'Consignment Product Transferred',200,'RECEIPT_DATE','Received Date','Receipt Date','','','default','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AP_CONSN_PRDTRNS_V','','');
xxeis.eis_rs_ins.rc( 'Consignment Product Transferred',200,'TRANSFER_NUMBER','Transfer Number','Transfer Number','','~~~','default','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AP_CONSN_PRDTRNS_V','','');
xxeis.eis_rs_ins.rc( 'Consignment Product Transferred',200,'QUANTITY_RECEIVED','Quantity Received','Quantity Received','','~~~','default','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AP_CONSN_PRDTRNS_V','','');
xxeis.eis_rs_ins.rc( 'Consignment Product Transferred',200,'SUPPLIER_NUMBER','Supplier Number','Supplier Number','','','default','','12','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AP_CONSN_PRDTRNS_V','','');
--Inserting Report Parameters - Consignment Product Transferred
xxeis.eis_rs_ins.rp( 'Consignment Product Transferred',200,'Start Date','Start Date','RECEIPT_DATE','>=','','','DATE','Y','Y','1','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','Start Date','');
xxeis.eis_rs_ins.rp( 'Consignment Product Transferred',200,'End Date','End Date','RECEIPT_DATE','<=','','','DATE','Y','Y','2','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','End Date','');
xxeis.eis_rs_ins.rp( 'Consignment Product Transferred',200,'Supplier Number','Supplier Number','SUPPLIER_NUMBER','IN','SUPPLIER_NUMBER','','VARCHAR2','N','Y','3','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
--Inserting Report Conditions - Consignment Product Transferred
xxeis.eis_rs_ins.rcn( 'Consignment Product Transferred',200,'RECEIPT_DATE','>=',':Start Date','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Consignment Product Transferred',200,'RECEIPT_DATE','<=',':End Date','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Consignment Product Transferred',200,'SUPPLIER_NUMBER','IN',':Supplier Number','','','Y','','Y','XXEIS_RS_ADMIN');
--Inserting Report Sorts - Consignment Product Transferred
xxeis.eis_rs_ins.rs( 'Consignment Product Transferred',200,'TO_LOCATION','ASC','XXEIS_RS_ADMIN','','');
--Inserting Report Triggers - Consignment Product Transferred
--Inserting Report Templates - Consignment Product Transferred
xxeis.eis_rs_ins.R_Tem( 'Consignment Product Transferred','Consignment Product Transferred','Seeded Template for Consignment Product Transferred','','','','','','','1','2','3','','','XXEIS_RS_ADMIN');
--Inserting Report Portals - Consignment Product Transferred
--Inserting Report Dashboards - Consignment Product Transferred
--Inserting Report Security - Consignment Product Transferred
xxeis.eis_rs_ins.rsec( 'Consignment Product Transferred','200','','50934',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Transferred','200','','50936',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Transferred','200','','50932',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Transferred','200','','50768',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Transferred','200','','50767',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Transferred','200','','50766',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Transferred','200','','50773',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Transferred','200','','50742',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Transferred','200','','50730',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Transferred','200','','50637',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Transferred','200','','50620',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Transferred','200','','50704',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Transferred','200','','50707',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Transferred','200','','50933',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Transferred','200','','50763',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Transferred','200','','50770',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Transferred','200','','50764',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Transferred','200','','50771',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Transferred','200','','50970',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Transferred','200','','50935',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Transferred','200','','20639',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Transferred','200','','50783',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Transferred','200','','50740',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Transferred','200','','50864',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Transferred','200','','50867',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Transferred','200','','50743',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Transferred','200','','50862',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Transferred','200','','50744',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Transferred','200','','50902',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Transferred','200','','50760',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Transferred','200','','50781',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Transferred','200','','50782',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Transferred','200','','50863',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Transferred','200','','50741',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Transferred','200','','50765',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Transferred','200','','50865',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Transferred','200','','50866',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Transferred','200','','50878',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Transferred','20005','','50880',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Transferred','','MM050208','',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Transferred','','LB048272','',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Transferred','','10012196','',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Transferred','','SO004816','',200,'XXEIS_RS_ADMIN','','');
--Inserting Report Pivots - Consignment Product Transferred
END;
/
set scan on define on
