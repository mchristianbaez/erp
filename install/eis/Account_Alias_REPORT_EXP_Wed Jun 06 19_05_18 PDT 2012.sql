--Report Name            : Account Alias Transactions - WC
--Import Instructions : 1. If RTF template needs to be uploaded to report automatically then move the RTF template to physical path of APPS_DATA_FILE_DIR oracle directory. Check dba_directories view to get the physical path of oracle directory. 
--                         (If you want to use any other oracle directory other than APPS_DATA_FILE_DIR then replace APPS_DATA_FILE_DIR with your oracle directory name below.)
--                      2. Make sure XXEIS schema has read/write permissions to above used oracle directory and give full permissions to the directory at OS level.
--                      3. Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
exec xxeis.eis_rs_ins.g_rtf_dir := 'APPS_DATA_FILE_DIR' ;
--Creating View XXWC_EIS_INV_ALIAS_TRANS_V
set scan off define off
prompt Creating View XXEIS.XXWC_EIS_INV_ALIAS_TRANS_V
Create or replace View XXEIS.XXWC_EIS_INV_ALIAS_TRANS_V
(SOURCE,DISTRIBUTION_ACCOUNT,PRODUCT,SEGMENT2,SEGMENT3,SEGMENT4,SEGMENT5,SEGMENT6,ITEM,ITEM_DESCRIPTION,INV_ORG_NAME,SUBINVENTORY,TRANSACTION_DATE,CREATION_DATE,CREATED_BY_USER,GL_PERIOD,GL_PERIOD_START_DATE,UNIT_COST,ITEM_REVISION,STOCK_LOCATORS,PROJECT_NAME,PROJECT_NUMBER,QUANTITY,VALUE,REASON,REASON_ID,TASK_NAME,TASK_NUMBER,TRANSACTION_ACTION,TRANSACTION_REFERENCE,TRANSACTION_SOURCE_TYPE_NAME,TRANSACTION_TYPE,UNIT_OF_MEASURE,VALUED_FLAG,MTT_TRANSACTION_TYPE_ID,INVENTORY_LOCATION_ID,TASK_ID,PROJECT_ID,ACCT_PERIOD_ID,TRANSACTION_ID,MMT_TRANSACTION_SOURCE_TYPE_ID,TRANSACTION_ACTION_ID,TRANSACTION_SOURCE_ID,DEPARTMENT_ID,ERROR_EXPLANATION,SUPPLIER_LOT,SOURCE_LINE_ID,PARENT_TRANSACTION_ID,SHIPMENT_NUMBER,WAYBILL_AIRBILL,FREIGHT_CODE,NUMBER_OF_CONTAINERS,RCV_TRANSACTION_ID,MOVE_TRANSACTION_ID,COMPLETION_TRANSACTION_ID,OPERTION_SEQUENCE,EXPENDITURE_TYPE,TRANSACTION_SET_ID,TRANSACTION_SOURCE_NAME,TRANSACTION_UOM,TRANSFER_SUBINVENTORY,TRANSFER_TRANSACTION_ID,MP_ORGANIZATION_ID,MIL_ORGANIZATION_ID,OAP_ORGANIZATION_ID,MSIL_INVENTORY_ITEM_ID,MSIL_ORGANIZATION_ID,LANGUAGE,INVENTORY_ITEM_ID,MSI_ORGANIZATION_ID,DISPOSITION_ID,MGP_ORGANIZATION_ID,HAOU_ORGANIZATION_ID,TRANSACTION_SOURCE_TYPE_ID) AS 
SELECT mgp.segment1 source,
        mgp.DISTRIBUTION_ACCOUNT,
        gcc.segment1 Product,gcc.segment2 ,gcc.segment3,gcc.segment4,gcc.segment5,gcc.segment6,
        msi.concatenated_segments item,
       replace(msil.description,'~','-') item_description,
       haou.name   inv_org_name,
       mmt.subinventory_code subinventory,
       trunc(mmt.transaction_date) transaction_date,
       mmt.creation_date creation_date,
       xxeis.EIS_INV_UTILITY_PKG.get_fnd_user_name(mmt.created_by)         created_by_user,
       oap.period_name gl_period,
       oap.period_start_date gl_period_start_date,
       mmt.actual_cost unit_cost,
       mmt.revision item_revision,
       mil.concatenated_segments stock_locators,
       pp.name project_name,
       pp.segment1 project_number,
       mmt.primary_quantity quantity,
       (mmt.primary_quantity*mmt.actual_cost) value,
       mtr.reason_name          reason,
       mtr.reason_id,
       pt.task_name task_name,
       pt.task_number task_number,
       xxeis.EIS_INV_UTILITY_PKG.get_lookup_meaning('MTL_TRANSACTION_ACTION',MMT.TRANSACTION_ACTION_ID) Transaction_Action,
       mmt.transaction_reference transaction_reference,
       mts.transaction_source_type_name,
       mtt.transaction_type_name transaction_type,
       msi.primary_unit_of_measure unit_of_measure,
       decode(MMT.costed_flag,'N','No',NVL(MMT.COSTED_FLAG,'Yes')) Valued_Flag,
       mtt.transaction_type_id mtt_transaction_type_id,
       mil.inventory_location_id,
       pt.task_id,
       pp.project_id,
       oap.acct_period_id,
       mmt.transaction_id,
       mmt.transaction_source_type_id mmt_transaction_source_type_id,
       mmt.transaction_action_id,
       mmt.transaction_source_id,
       mmt.department_id,
       mmt.error_explanation,
       mmt.vendor_lot_number supplier_lot,
       mmt.source_line_id,
       mmt.parent_transaction_id,
       mmt.shipment_number shipment_number,
       mmt.waybill_airbill waybill_airbill,
       mmt.freight_code freight_code,
       mmt.number_of_containers,
       mmt.rcv_transaction_id,
       mmt.move_transaction_id,
       mmt.completion_transaction_id,
       mmt.operation_seq_num opertion_sequence,
       mmt.expenditure_type ,
       mmt.transaction_set_id         ,
       mmt.transaction_source_name     ,
       mmt.transaction_uom             ,
       mmt.transfer_subinventory  ,
       mmt.transfer_transaction_id,
       mp.organization_id    mp_organization_id,
       mil.organization_id   mil_organization_id,
       oap.organization_id   oap_organization_id,
       msil.inventory_item_id msil_inventory_item_id,
       msil.organization_id  msil_organization_id,
       msil.language,
       msi.inventory_item_id,
       msi.organization_id    msi_organization_id,
       mgp.disposition_id,
       mgp.organization_id     mgp_organization_id,
       haou.organization_id    haou_organization_id,
       mts.transaction_source_type_id
       --descr#flexfield#start
       --descr#flexfield#end
  FROM mtl_transaction_types mtt,
       mtl_item_locations_kfv mil,
       pa_tasks pt,
       pa_projects_all pp,
       org_acct_periods oap,
       mtl_system_items_tl msil,
       mtl_system_items_kfv msi,
       mtl_material_transactions mmt,
       mtl_generic_dispositions mgp,
       mtl_parameters mp,
       mtl_transaction_reasons mtr,
       hr_all_organization_units haou,
       mtl_txn_source_types mts,
       gl_code_combinations gcc
 WHERE msi.organization_id                  = mp.organization_id
   and mmt.organization_id                  = mp.organization_id
   and mmt.inventory_item_id                = msi.inventory_item_id
   and mmt.transaction_source_type_id       = 6
   and mgp.disposition_id                   = mmt.transaction_source_id
   and mgp.organization_id                  = mp.organization_id
   and oap.organization_id              (+) = mmt.organization_id
   and oap.acct_period_id               (+) = mmt.acct_period_id
   and msil.inventory_item_id           (+) = msi.inventory_item_id
   and msil.organization_id             (+) = msi.organization_id
   and msil.language                    (+) = USERENV('LANG')
   and mmt.project_id                       = pp.project_id (+)
   and mmt.task_id                          = pt.task_id (+)
   and mmt.locator_id                       = mil.inventory_location_id(+)
   and mil.organization_id              (+) = mmt.organization_id
   and mmt.transaction_type_id              = mtt.transaction_type_id(+)
   and mtr.reason_id                    (+) = mmt.reason_id
   and haou.organization_id                 = msi.organization_id
   and mts.transaction_source_type_id      = mmt.transaction_source_type_id
   --AND mmt.DISTRIBUTION_ACCOUNT_ID = gcc.code_combination_id(+)   
   AND mgp.DISTRIBUTION_ACCOUNT = gcc.code_combination_id(+)
   and exists (Select 1 from XXEIS.EIS_ORG_ACCESS_V where organization_id = msil.organization_id)/
set scan on define on
prompt Creating View Data for Account Alias Transactions - WC
set scan off define off
DECLARE
BEGIN 
--Inserting View XXWC_EIS_INV_ALIAS_TRANS_V
xxeis.eis_rs_ins.v( 'XXWC_EIS_INV_ALIAS_TRANS_V',401,'Accounts alias related to miscellaneous transactions','','','','DC005370','XXEIS','Eis Inv Alias Transactions V','EIATV');
--Delete View Columns for XXWC_EIS_INV_ALIAS_TRANS_V
xxeis.eis_rs_utility.delete_view_rows('XXWC_EIS_INV_ALIAS_TRANS_V',401,FALSE);
--Inserting View Columns for XXWC_EIS_INV_ALIAS_TRANS_V
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','TRANSACTION_SET_ID',401,'Transaction Set Id','TRANSACTION_SET_ID','','','','DC005370','NUMBER','','','Transaction Set Id');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','TRANSACTION_SOURCE_ID',401,'Transaction Source Id','TRANSACTION_SOURCE_ID','','','','DC005370','NUMBER','','','Transaction Source Id');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','UNIT_OF_MEASURE',401,'Unit Of Measure','UNIT_OF_MEASURE','','','','DC005370','VARCHAR2','','','Unit Of Measure');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','VALUED_FLAG',401,'Valued Flag','VALUED_FLAG','','','','DC005370','VARCHAR2','','','Valued Flag');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','TRANSACTION_DATE',401,'Transaction Date','TRANSACTION_DATE','','','','DC005370','DATE','','','Transaction Date');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','TRANSFER_SUBINVENTORY',401,'Transfer Subinventory','TRANSFER_SUBINVENTORY','','','','DC005370','VARCHAR2','','','Transfer Subinventory');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','REASON_ID',401,'Reason Id','REASON_ID','','','','DC005370','NUMBER','','','Reason Id');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','TASK_ID',401,'Task Id','TASK_ID','','','','DC005370','NUMBER','','','Task Id');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','TRANSACTION_SOURCE_TYPE_ID',401,'Transaction Source Type Id','TRANSACTION_SOURCE_TYPE_ID','','','','DC005370','NUMBER','','','Transaction Source Type Id');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','INV_ORG_NAME',401,'Inv Org Name','INV_ORG_NAME','','','','DC005370','VARCHAR2','','','Inv Org Name');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','WAYBILL_AIRBILL',401,'Waybill Airbill','WAYBILL_AIRBILL','','','','DC005370','VARCHAR2','','','Waybill Airbill');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','FREIGHT_CODE',401,'Freight Code','FREIGHT_CODE','','','','DC005370','VARCHAR2','','','Freight Code');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','NUMBER_OF_CONTAINERS',401,'Number Of Containers','NUMBER_OF_CONTAINERS','','','','DC005370','NUMBER','','','Number Of Containers');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','RCV_TRANSACTION_ID',401,'Rcv Transaction Id','RCV_TRANSACTION_ID','','','','DC005370','NUMBER','','','Rcv Transaction Id');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MOVE_TRANSACTION_ID',401,'Move Transaction Id','MOVE_TRANSACTION_ID','','','','DC005370','NUMBER','','','Move Transaction Id');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','INVENTORY_LOCATION_ID',401,'Inventory Location Id','INVENTORY_LOCATION_ID','','','','DC005370','NUMBER','','','Inventory Location Id');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','ACCT_PERIOD_ID',401,'Acct Period Id','ACCT_PERIOD_ID','','','','DC005370','NUMBER','','','Acct Period Id');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','DEPARTMENT_ID',401,'Department Id','DEPARTMENT_ID','','','','DC005370','NUMBER','','','Department Id');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','GL_PERIOD',401,'Gl Period','GL_PERIOD','','','','DC005370','VARCHAR2','','','Gl Period');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','ITEM',401,'Item','ITEM','','','','DC005370','VARCHAR2','','','Item');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','OPERTION_SEQUENCE',401,'Opertion Sequence','OPERTION_SEQUENCE','','','','DC005370','NUMBER','','','Opertion Sequence');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','QUANTITY',401,'Quantity','QUANTITY','','','','DC005370','NUMBER','','','Quantity');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','STOCK_LOCATORS',401,'Stock Locators','STOCK_LOCATORS','','','','DC005370','VARCHAR2','','','Stock Locators');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','TASK_NUMBER',401,'Task Number','TASK_NUMBER','','','','DC005370','VARCHAR2','','','Task Number');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','TRANSACTION_ACTION_ID',401,'Transaction Action Id','TRANSACTION_ACTION_ID','','','','DC005370','NUMBER','','','Transaction Action Id');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','COMPLETION_TRANSACTION_ID',401,'Completion Transaction Id','COMPLETION_TRANSACTION_ID','','','','DC005370','NUMBER','','','Completion Transaction Id');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','EXPENDITURE_TYPE',401,'Expenditure Type','EXPENDITURE_TYPE','','','','DC005370','VARCHAR2','','','Expenditure Type');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','CREATION_DATE',401,'Creation Date','CREATION_DATE','','','','DC005370','DATE','','','Creation Date');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','CREATED_BY_USER',401,'Created By User','CREATED_BY_USER','','','','DC005370','VARCHAR2','','','Created By User');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','GL_PERIOD_START_DATE',401,'Gl Period Start Date','GL_PERIOD_START_DATE','','','','DC005370','DATE','','','Gl Period Start Date');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','ITEM_DESCRIPTION',401,'Item Description','ITEM_DESCRIPTION','','','','DC005370','VARCHAR2','','','Item Description');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','ITEM_REVISION',401,'Item Revision','ITEM_REVISION','','','','DC005370','VARCHAR2','','','Item Revision');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','PROJECT_ID',401,'Project Id','PROJECT_ID','','','','DC005370','NUMBER','','','Project Id');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','PROJECT_NUMBER',401,'Project Number','PROJECT_NUMBER','','','','DC005370','VARCHAR2','','','Project Number');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','PROJECT_NAME',401,'Project Name','PROJECT_NAME','','','','DC005370','VARCHAR2','','','Project Name');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','REASON',401,'Reason','REASON','','','','DC005370','VARCHAR2','','','Reason');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','SHIPMENT_NUMBER',401,'Shipment Number','SHIPMENT_NUMBER','','','','DC005370','VARCHAR2','','','Shipment Number');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','SUBINVENTORY',401,'Subinventory','SUBINVENTORY','','','','DC005370','VARCHAR2','','','Subinventory');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','TRANSACTION_ACTION',401,'Transaction Action','TRANSACTION_ACTION','','','','DC005370','VARCHAR2','','','Transaction Action');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','TRANSACTION_ID',401,'Transaction Id','TRANSACTION_ID','','','','DC005370','NUMBER','','','Transaction Id');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','TRANSACTION_REFERENCE',401,'Transaction Reference','TRANSACTION_REFERENCE','','','','DC005370','VARCHAR2','','','Transaction Reference');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','TRANSACTION_SOURCE_NAME',401,'Transaction Source Name','TRANSACTION_SOURCE_NAME','','','','DC005370','VARCHAR2','','','Transaction Source Name');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','TRANSACTION_TYPE',401,'Transaction Type','TRANSACTION_TYPE','','','','DC005370','VARCHAR2','','','Transaction Type');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','TRANSACTION_UOM',401,'Transaction Uom','TRANSACTION_UOM','','','','DC005370','VARCHAR2','','','Transaction Uom');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','TRANSFER_TRANSACTION_ID',401,'Transfer Transaction Id','TRANSFER_TRANSACTION_ID','','','','DC005370','NUMBER','','','Transfer Transaction Id');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','ERROR_EXPLANATION',401,'Error Explanation','ERROR_EXPLANATION','','','','DC005370','VARCHAR2','','','Error Explanation');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','SUPPLIER_LOT',401,'Supplier Lot','SUPPLIER_LOT','','','','DC005370','VARCHAR2','','','Supplier Lot');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','SOURCE_LINE_ID',401,'Source Line Id','SOURCE_LINE_ID','','','','DC005370','NUMBER','','','Source Line Id');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','PARENT_TRANSACTION_ID',401,'Parent Transaction Id','PARENT_TRANSACTION_ID','','','','DC005370','NUMBER','','','Parent Transaction Id');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','TASK_NAME',401,'Task Name','TASK_NAME','','','','DC005370','VARCHAR2','','','Task Name');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSIL_INVENTORY_ITEM_ID',401,'Msil Inventory Item Id','MSIL_INVENTORY_ITEM_ID','','','','DC005370','NUMBER','','','Msil Inventory Item Id');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSIL_ORGANIZATION_ID',401,'Msil Organization Id','MSIL_ORGANIZATION_ID','','','','DC005370','NUMBER','','','Msil Organization Id');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI_ORGANIZATION_ID',401,'Msi Organization Id','MSI_ORGANIZATION_ID','','','','DC005370','NUMBER','','','Msi Organization Id');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MTT_TRANSACTION_TYPE_ID',401,'Mtt Transaction Type Id','MTT_TRANSACTION_TYPE_ID','','','','DC005370','NUMBER','','','Mtt Transaction Type Id');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','OAP_ORGANIZATION_ID',401,'Oap Organization Id','OAP_ORGANIZATION_ID','','','','DC005370','NUMBER','','','Oap Organization Id');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','TRANSACTION_SOURCE_TYPE_NAME',401,'Transaction Source Type Name','TRANSACTION_SOURCE_TYPE_NAME','','','','DC005370','VARCHAR2','','','Transaction Source Type Name');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','DISPOSITION_ID',401,'Disposition Id','DISPOSITION_ID','','','','DC005370','NUMBER','','','Disposition Id');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','HAOU_ORGANIZATION_ID',401,'Haou Organization Id','HAOU_ORGANIZATION_ID','','','','DC005370','NUMBER','','','Haou Organization Id');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','INVENTORY_ITEM_ID',401,'Inventory Item Id','INVENTORY_ITEM_ID','','','','DC005370','NUMBER','','','Inventory Item Id');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','LANGUAGE',401,'Language','LANGUAGE','','','','DC005370','VARCHAR2','','','Language');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MGP_ORGANIZATION_ID',401,'Mgp Organization Id','MGP_ORGANIZATION_ID','','','','DC005370','NUMBER','','','Mgp Organization Id');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MIL_ORGANIZATION_ID',401,'Mil Organization Id','MIL_ORGANIZATION_ID','','','','DC005370','NUMBER','','','Mil Organization Id');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MMT_TRANSACTION_SOURCE_TYPE_ID',401,'Mmt Transaction Source Type Id','MMT_TRANSACTION_SOURCE_TYPE_ID','','','','DC005370','NUMBER','','','Mmt Transaction Source Type Id');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MP_ORGANIZATION_ID',401,'Mp Organization Id','MP_ORGANIZATION_ID','','','','DC005370','NUMBER','','','Mp Organization Id');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','UNIT_COST',401,'Unit Cost','UNIT_COST','','~~2','','DC005370','NUMBER','','','Unit Cost');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','VALUE',401,'Value','VALUE','','','','DC005370','NUMBER','','','Value');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MGP#101#ACCOUNTALIAS',401,'Key Flex Field Column - MGP#101#ACCOUNTALIAS','MGP#101#ACCOUNTALIAS','','','','DC005370','VARCHAR2','MTL_GENERIC_DISPOSITIONS','SEGMENT1','Account Alias#101');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MGP#101#ACCOUNTALIAS_DESC',401,'Key Flex Field Column - MGP#101#ACCOUNTALIAS_DESC','MGP#101#ACCOUNTALIAS_DESC','','','','DC005370','VARCHAR2','MTL_GENERIC_DISPOSITIONS','SEGMENT1','Account Alias#101#DESC');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MIL#101#STOCKLOCATIONS',401,'Key Flex Field Column - MIL#101#STOCKLOCATIONS','MIL#101#STOCKLOCATIONS','','','','DC005370','VARCHAR2','MTL_ITEM_LOCATIONS','SEGMENT1','Stock Locations#101');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MIL#101#STOCKLOCATIONS_DESC',401,'Key Flex Field Column - MIL#101#STOCKLOCATIONS_DESC','MIL#101#STOCKLOCATIONS_DESC','','','','DC005370','VARCHAR2','MTL_ITEM_LOCATIONS','SEGMENT1','Stock Locations#101#DESC');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MMT#100#SUPPLIER_NUMBER',401,'Descriptive flexfield: Transaction history Column Name: Supplier Number Context: 100','MMT#100#SUPPLIER_NUMBER','','','','DC005370','VARCHAR2','MTL_MATERIAL_TRANSACTIONS','ATTRIBUTE1','Mmt#100#Supplier Number');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MMT#100#RETURN_NUMBER',401,'Descriptive flexfield: Transaction history Column Name: Return Number Context: 100','MMT#100#RETURN_NUMBER','','','','DC005370','VARCHAR2','MTL_MATERIAL_TRANSACTIONS','ATTRIBUTE2','Mmt#100#Return Number');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MMT#100#RETURN_UNIT_PRICE',401,'Descriptive flexfield: Transaction history Column Name: Return Unit Price Context: 100','MMT#100#RETURN_UNIT_PRICE','','','','DC005370','VARCHAR2','MTL_MATERIAL_TRANSACTIONS','ATTRIBUTE3','Mmt#100#Return Unit Price');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MMT#RENTALASS#HOME_BRANCH',401,'Descriptive flexfield: Transaction history Column Name: Home Branch Context: Rental Asset Transfer','MMT#RENTALASS#HOME_BRANCH','','','','DC005370','VARCHAR2','MTL_MATERIAL_TRANSACTIONS','ATTRIBUTE1','Mmt#Rental Asset Transfer#Home Branch');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MMT#RENTALASS#VENDOR',401,'Descriptive flexfield: Transaction history Column Name: Vendor Context: Rental Asset Transfer','MMT#RENTALASS#VENDOR','','','','DC005370','VARCHAR2','MTL_MATERIAL_TRANSACTIONS','ATTRIBUTE10','Mmt#Rental Asset Transfer#Vendor');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MMT#RENTALASS#PO_NUMBER',401,'Descriptive flexfield: Transaction history Column Name: PO Number Context: Rental Asset Transfer','MMT#RENTALASS#PO_NUMBER','','','','DC005370','VARCHAR2','MTL_MATERIAL_TRANSACTIONS','ATTRIBUTE11','Mmt#Rental Asset Transfer#Po Number');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MMT#RENTALASS#ITEM_NUMBER',401,'Descriptive flexfield: Transaction history Column Name: Item Number Context: Rental Asset Transfer','MMT#RENTALASS#ITEM_NUMBER','','','','DC005370','VARCHAR2','MTL_MATERIAL_TRANSACTIONS','ATTRIBUTE12','Mmt#Rental Asset Transfer#Item Number');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MMT#RENTALASS#CER_NUMBER',401,'Descriptive flexfield: Transaction history Column Name: CER Number Context: Rental Asset Transfer','MMT#RENTALASS#CER_NUMBER','','','','DC005370','VARCHAR2','MTL_MATERIAL_TRANSACTIONS','ATTRIBUTE13','Mmt#Rental Asset Transfer#Cer Number');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MMT#RENTALASS#MISC_ISSUE_TRA',401,'Descriptive flexfield: Transaction history Column Name: Misc Issue Transaction ID Context: Rental Asset Transfer','MMT#RENTALASS#MISC_ISSUE_TRA','','','','DC005370','VARCHAR2','MTL_MATERIAL_TRANSACTIONS','ATTRIBUTE14','Mmt#Rental Asset Transfer#Misc Issue Transaction Id');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MMT#RENTALASS#CUSTODIAN_BRAN',401,'Descriptive flexfield: Transaction history Column Name: Custodian Branch Context: Rental Asset Transfer','MMT#RENTALASS#CUSTODIAN_BRAN','','','','DC005370','VARCHAR2','MTL_MATERIAL_TRANSACTIONS','ATTRIBUTE2','Mmt#Rental Asset Transfer#Custodian Branch');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MMT#RENTALASS#MAJOR_CATEGORY',401,'Descriptive flexfield: Transaction history Column Name: Major Category Context: Rental Asset Transfer','MMT#RENTALASS#MAJOR_CATEGORY','','','','DC005370','VARCHAR2','MTL_MATERIAL_TRANSACTIONS','ATTRIBUTE3','Mmt#Rental Asset Transfer#Major Category');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MMT#RENTALASS#MINOR_CATEGORY',401,'Descriptive flexfield: Transaction history Column Name: Minor Category Context: Rental Asset Transfer','MMT#RENTALASS#MINOR_CATEGORY','','','','DC005370','VARCHAR2','MTL_MATERIAL_TRANSACTIONS','ATTRIBUTE4','Mmt#Rental Asset Transfer#Minor Category');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MMT#RENTALASS#STATE',401,'Descriptive flexfield: Transaction history Column Name: State Context: Rental Asset Transfer','MMT#RENTALASS#STATE','','','','DC005370','VARCHAR2','MTL_MATERIAL_TRANSACTIONS','ATTRIBUTE5','Mmt#Rental Asset Transfer#State');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MMT#RENTALASS#CITY',401,'Descriptive flexfield: Transaction history Column Name: City Context: Rental Asset Transfer','MMT#RENTALASS#CITY','','','','DC005370','VARCHAR2','MTL_MATERIAL_TRANSACTIONS','ATTRIBUTE6','Mmt#Rental Asset Transfer#City');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MMT#RENTALASS#TRANSFER_TO_MA',401,'Descriptive flexfield: Transaction history Column Name: Transfer to MassAdd Context: Rental Asset Transfer','MMT#RENTALASS#TRANSFER_TO_MA','','','','DC005370','VARCHAR2','MTL_MATERIAL_TRANSACTIONS','ATTRIBUTE9','Mmt#Rental Asset Transfer#Transfer To Massadd');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MP#FACTORY_PLANNER_DATA_DIRE',401,'Descriptive flexfield: Organization parameters Column Name: Factory Planner Data Directory','MP#FACTORY_PLANNER_DATA_DIRE','','','','DC005370','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE1','Mp#Factory Planner Data Directory');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MP#FRU',401,'Descriptive flexfield: Organization parameters Column Name: FRU','MP#FRU','','','','DC005370','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE10','Mp#Fru');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MP#LOCATION_NUMBER',401,'Descriptive flexfield: Organization parameters Column Name: Location Number','MP#LOCATION_NUMBER','','','','DC005370','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE11','Mp#Location Number');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MP#BRANCH_OPERATIONS_MANAGER',401,'Descriptive flexfield: Organization parameters Column Name: Branch Operations Manager','MP#BRANCH_OPERATIONS_MANAGER','','','','DC005370','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE13','Mp#Branch Operations Manager');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MP#DELIVERY_CHARGE_EXEMPT',401,'Descriptive flexfield: Organization parameters Column Name: Delivery Charge Exempt','MP#DELIVERY_CHARGE_EXEMPT','','','','DC005370','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE14','Mp#Delivery Charge Exempt');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MP#FACTORY_PLANNER_EXECUTABL',401,'Descriptive flexfield: Organization parameters Column Name: Factory Planner Executable Directory','MP#FACTORY_PLANNER_EXECUTABL','','','','DC005370','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE2','Mp#Factory Planner Executable Directory');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MP#FACTORY_PLANNER_USER',401,'Descriptive flexfield: Organization parameters Column Name: Factory Planner User','MP#FACTORY_PLANNER_USER','','','','DC005370','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE3','Mp#Factory Planner User');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MP#FACTORY_PLANNER_HOST',401,'Descriptive flexfield: Organization parameters Column Name: Factory Planner Host','MP#FACTORY_PLANNER_HOST','','','','DC005370','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE4','Mp#Factory Planner Host');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MP#FACTORY_PLANNER_PORT_NUMB',401,'Descriptive flexfield: Organization parameters Column Name: Factory Planner Port Number','MP#FACTORY_PLANNER_PORT_NUMB','','','','DC005370','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE5','Mp#Factory Planner Port Number');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MP#PRICING_ZONE',401,'Descriptive flexfield: Organization parameters Column Name: Pricing Zone','MP#PRICING_ZONE','','','','DC005370','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE6','Mp#Pricing Zone');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MP#ORG_TYPE',401,'Descriptive flexfield: Organization parameters Column Name: Org Type','MP#ORG_TYPE','','','','DC005370','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE7','Mp#Org Type');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MP#DISTRICT',401,'Descriptive flexfield: Organization parameters Column Name: District','MP#DISTRICT','','','','DC005370','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE8','Mp#District');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MP#REGION',401,'Descriptive flexfield: Organization parameters Column Name: Region','MP#REGION','','','','DC005370','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE9','Mp#Region');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI#HDS#LOB',401,'Descriptive flexfield: Items Column Name: LOB Context: HDS','MSI#HDS#LOB','','','','DC005370','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE1','Msi#Hds#Lob');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI#HDS#DROP_SHIPMENT',401,'Descriptive flexfield: Items Column Name: Drop Shipment Context: HDS','MSI#HDS#DROP_SHIPMENT','','','','DC005370','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE10','Msi#Hds#Drop Shipment');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI#HDS#INVOICE_UOM',401,'Descriptive flexfield: Items Column Name: Invoice UOM Context: HDS','MSI#HDS#INVOICE_UOM','','','','DC005370','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE15','Msi#Hds#Invoice Uom');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI#HDS#PRODUCT_ID',401,'Descriptive flexfield: Items Column Name: Product ID Context: HDS','MSI#HDS#PRODUCT_ID','','','','DC005370','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE2','Msi#Hds#Product Id');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI#HDS#VENDOR_PART_NUMBER',401,'Descriptive flexfield: Items Column Name: Vendor Part Number Context: HDS','MSI#HDS#VENDOR_PART_NUMBER','','','','DC005370','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE3','Msi#Hds#Vendor Part Number');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI#HDS#UNSPSC_CODE',401,'Descriptive flexfield: Items Column Name: UNSPSC Code Context: HDS','MSI#HDS#UNSPSC_CODE','','','','DC005370','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE4','Msi#Hds#Unspsc Code');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI#HDS#UPC_PRIMARY',401,'Descriptive flexfield: Items Column Name: UPC Primary Context: HDS','MSI#HDS#UPC_PRIMARY','','','','DC005370','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE5','Msi#Hds#Upc Primary');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI#HDS#SKU_DESCRIPTION',401,'Descriptive flexfield: Items Column Name: SKU Description Context: HDS','MSI#HDS#SKU_DESCRIPTION','','','','DC005370','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE7','Msi#Hds#Sku Description');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI#WC#CA_PROP_65',401,'Descriptive flexfield: Items Column Name: CA Prop 65 Context: WC','MSI#WC#CA_PROP_65','','','','DC005370','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE1','Msi#Wc#Ca Prop 65');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI#WC#COUNTRY_OF_ORIGIN',401,'Descriptive flexfield: Items Column Name: Country of Origin Context: WC','MSI#WC#COUNTRY_OF_ORIGIN','','','','DC005370','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE10','Msi#Wc#Country Of Origin');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI#WC#ORM_D_FLAG',401,'Descriptive flexfield: Items Column Name: ORM-D Flag Context: WC','MSI#WC#ORM_D_FLAG','','','','DC005370','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE11','Msi#Wc#Orm-D Flag');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI#WC#STORE_VELOCITY',401,'Descriptive flexfield: Items Column Name: Store Velocity Context: WC','MSI#WC#STORE_VELOCITY','','','','DC005370','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE12','Msi#Wc#Store Velocity');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI#WC#DC_VELOCITY',401,'Descriptive flexfield: Items Column Name: DC Velocity Context: WC','MSI#WC#DC_VELOCITY','','','','DC005370','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE13','Msi#Wc#Dc Velocity');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI#WC#YEARLY_STORE_VELOCITY',401,'Descriptive flexfield: Items Column Name: Yearly Store Velocity Context: WC','MSI#WC#YEARLY_STORE_VELOCITY','','','','DC005370','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE14','Msi#Wc#Yearly Store Velocity');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI#WC#YEARLY_DC_VELOCITY',401,'Descriptive flexfield: Items Column Name: Yearly DC Velocity Context: WC','MSI#WC#YEARLY_DC_VELOCITY','','','','DC005370','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE15','Msi#Wc#Yearly Dc Velocity');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI#WC#PRISM_PART_NUMBER',401,'Descriptive flexfield: Items Column Name: PRISM Part Number Context: WC','MSI#WC#PRISM_PART_NUMBER','','','','DC005370','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE16','Msi#Wc#Prism Part Number');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI#WC#HAZMAT_DESCRIPTION',401,'Descriptive flexfield: Items Column Name: Hazmat Description Context: WC','MSI#WC#HAZMAT_DESCRIPTION','','','','DC005370','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE17','Msi#Wc#Hazmat Description');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI#WC#HAZMAT_CONTAINER',401,'Descriptive flexfield: Items Column Name: Hazmat Container Context: WC','MSI#WC#HAZMAT_CONTAINER','','','','DC005370','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE18','Msi#Wc#Hazmat Container');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI#WC#GTP_INDICATOR',401,'Descriptive flexfield: Items Column Name: GTP Indicator Context: WC','MSI#WC#GTP_INDICATOR','','','','DC005370','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE19','Msi#Wc#Gtp Indicator');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI#WC#LAST_LEAD_TIME',401,'Descriptive flexfield: Items Column Name: Last Lead Time Context: WC','MSI#WC#LAST_LEAD_TIME','','','','DC005370','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE2','Msi#Wc#Last Lead Time');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI#WC#AMU',401,'Descriptive flexfield: Items Column Name: AMU Context: WC','MSI#WC#AMU','','','','DC005370','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE20','Msi#Wc#Amu');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI#WC#RESERVE_STOCK',401,'Descriptive flexfield: Items Column Name: Reserve Stock Context: WC','MSI#WC#RESERVE_STOCK','','','','DC005370','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE21','Msi#Wc#Reserve Stock');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI#WC#TAXWARE_CODE',401,'Descriptive flexfield: Items Column Name: Taxware Code Context: WC','MSI#WC#TAXWARE_CODE','','','','DC005370','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE22','Msi#Wc#Taxware Code');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI#WC#AVERAGE_UNITS',401,'Descriptive flexfield: Items Column Name: Average Units Context: WC','MSI#WC#AVERAGE_UNITS','','','','DC005370','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE25','Msi#Wc#Average Units');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI#WC#PRODUCT_CODE',401,'Descriptive flexfield: Items Column Name: Product code Context: WC','MSI#WC#PRODUCT_CODE','','','','DC005370','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE26','Msi#Wc#Product Code');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI#WC#IMPORT_DUTY_',401,'Descriptive flexfield: Items Column Name: Import Duty % Context: WC','MSI#WC#IMPORT_DUTY_','','','','DC005370','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE27','Msi#Wc#Import Duty %');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI#WC#KEEP_ITEM_ACTIVE',401,'Descriptive flexfield: Items Column Name: Keep Item Active Context: WC','MSI#WC#KEEP_ITEM_ACTIVE','','','','DC005370','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE29','Msi#Wc#Keep Item Active');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI#WC#PESTICIDE_FLAG',401,'Descriptive flexfield: Items Column Name: Pesticide Flag Context: WC','MSI#WC#PESTICIDE_FLAG','','','','DC005370','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE3','Msi#Wc#Pesticide Flag');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI#WC#CALC_LEAD_TIME',401,'Descriptive flexfield: Items Column Name: Calc Lead Time Context: WC','MSI#WC#CALC_LEAD_TIME','','','','DC005370','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE30','Msi#Wc#Calc Lead Time');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI#WC#VOC_GL',401,'Descriptive flexfield: Items Column Name: VOC G/L Context: WC','MSI#WC#VOC_GL','','','','DC005370','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE4','Msi#Wc#Voc G/L');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI#WC#PESTICIDE_FLAG_STATE',401,'Descriptive flexfield: Items Column Name: Pesticide Flag State Context: WC','MSI#WC#PESTICIDE_FLAG_STATE','','','','DC005370','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE5','Msi#Wc#Pesticide Flag State');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI#WC#VOC_CATEGORY',401,'Descriptive flexfield: Items Column Name: VOC Category Context: WC','MSI#WC#VOC_CATEGORY','','','','DC005370','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE6','Msi#Wc#Voc Category');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI#WC#VOC_SUB_CATEGORY',401,'Descriptive flexfield: Items Column Name: VOC Sub Category Context: WC','MSI#WC#VOC_SUB_CATEGORY','','','','DC005370','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE7','Msi#Wc#Voc Sub Category');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI#WC#MSDS_#',401,'Descriptive flexfield: Items Column Name: MSDS # Context: WC','MSI#WC#MSDS_#','','','','DC005370','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE8','Msi#Wc#Msds #');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI#WC#HAZMAT_PACKAGING_GROU',401,'Descriptive flexfield: Items Column Name: Hazmat Packaging Group Context: WC','MSI#WC#HAZMAT_PACKAGING_GROU','','','','DC005370','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE9','Msi#Wc#Hazmat Packaging Group');
--Inserting View Components for XXWC_EIS_INV_ALIAS_TRANS_V
xxeis.eis_rs_ins.vcomp( 'XXWC_EIS_INV_ALIAS_TRANS_V','PA_TASKS',401,'PA_TASKS','PT','PT','DC005370','DC005370','1751410','User-Defined Subdivisions Of Project Work','','');
xxeis.eis_rs_ins.vcomp( 'XXWC_EIS_INV_ALIAS_TRANS_V','PA_PROJECTS',401,'PA_PROJECTS_ALL','PP','PP','DC005370','DC005370','1751410','Information About Projects','','');
xxeis.eis_rs_ins.vcomp( 'XXWC_EIS_INV_ALIAS_TRANS_V','ORG_ACCT_PERIODS',401,'ORG_ACCT_PERIODS','OAP','OAP','DC005370','DC005370','1751410','Organization Accounting Period Definition Table','','');
xxeis.eis_rs_ins.vcomp( 'XXWC_EIS_INV_ALIAS_TRANS_V','MTL_SYSTEM_ITEMS_KFV',401,'MTL_SYSTEM_ITEMS_B','MSI','MSI','DC005370','DC005370','1751410','Inventory Item Definitions','','');
xxeis.eis_rs_ins.vcomp( 'XXWC_EIS_INV_ALIAS_TRANS_V','MTL_MATERIAL_TRANSACTIONS',401,'MTL_MATERIAL_TRANSACTIONS','MMT','MMT','DC005370','DC005370','1751410','Material Transaction Table','','');
xxeis.eis_rs_ins.vcomp( 'XXWC_EIS_INV_ALIAS_TRANS_V','MTL_GENERIC_DISPOSITIONS',401,'MTL_GENERIC_DISPOSITIONS','MGP','MGP','DC005370','DC005370','1751410','Account Alias Definition','','');
xxeis.eis_rs_ins.vcomp( 'XXWC_EIS_INV_ALIAS_TRANS_V','MTL_PARAMETERS',401,'MTL_PARAMETERS','MP','MP','DC005370','DC005370','1751410','Inventory Control Options And Defaults','','');
xxeis.eis_rs_ins.vcomp( 'XXWC_EIS_INV_ALIAS_TRANS_V','MTL_TRANSACTION_REASONS',401,'MTL_TRANSACTION_REASONS','MTR','MTR','DC005370','DC005370','1751410','Inventory Transaction Reasons Table','','');
xxeis.eis_rs_ins.vcomp( 'XXWC_EIS_INV_ALIAS_TRANS_V','HR_ORGANIZATION_UNITS',401,'HR_ALL_ORGANIZATION_UNITS','HAOU','HAOU','DC005370','DC005370','1751410','HR_ALL_ORGANIZATION_UNITS','','');
xxeis.eis_rs_ins.vcomp( 'XXWC_EIS_INV_ALIAS_TRANS_V','MTL_TXN_SOURCE_TYPES',401,'MTL_TXN_SOURCE_TYPES','MTS','MTS','DC005370','DC005370','1751410','Valid Transaction Source Types','','');
xxeis.eis_rs_ins.vcomp( 'XXWC_EIS_INV_ALIAS_TRANS_V','MTL_ITEM_LOCATIONS_KFV',401,'MTL_ITEM_LOCATIONS','MIL','MIL','DC005370','DC005370','1751410','Item Locations','','');
xxeis.eis_rs_ins.vcomp( 'XXWC_EIS_INV_ALIAS_TRANS_V','MTL_TRANSACTION_TYPES',401,'MTL_TRANSACTION_TYPES','MTT','MTT','DC005370','DC005370','1751410','Inventory Transaction Types Table','','');
--Inserting View Component Joins for XXWC_EIS_INV_ALIAS_TRANS_V
xxeis.eis_rs_ins.vcj( 'XXWC_EIS_INV_ALIAS_TRANS_V','PA_TASKS','PT',401,'EIATV.TASK_ID','=','PT.TASK_ID(+)','','','','Y','DC005370');
xxeis.eis_rs_ins.vcj( 'XXWC_EIS_INV_ALIAS_TRANS_V','PA_PROJECTS','PP',401,'EIATV.PROJECT_ID','=','PP.PROJECT_ID(+)','','','','Y','DC005370');
xxeis.eis_rs_ins.vcj( 'XXWC_EIS_INV_ALIAS_TRANS_V','ORG_ACCT_PERIODS','OAP',401,'EIATV.OAP_ORGANIZATION_ID','=','OAP.ORGANIZATION_ID(+)','','','','Y','DC005370');
xxeis.eis_rs_ins.vcj( 'XXWC_EIS_INV_ALIAS_TRANS_V','ORG_ACCT_PERIODS','OAP',401,'EIATV.ACCT_PERIOD_ID','=','OAP.ACCT_PERIOD_ID(+)','','','','Y','DC005370');
xxeis.eis_rs_ins.vcj( 'XXWC_EIS_INV_ALIAS_TRANS_V','MTL_SYSTEM_ITEMS_KFV','MSI',401,'EIATV.INVENTORY_ITEM_ID','=','MSI.INVENTORY_ITEM_ID(+)','','','','Y','DC005370');
xxeis.eis_rs_ins.vcj( 'XXWC_EIS_INV_ALIAS_TRANS_V','MTL_SYSTEM_ITEMS_KFV','MSI',401,'EIATV.MSI_ORGANIZATION_ID','=','MSI.ORGANIZATION_ID(+)','','','','Y','DC005370');
xxeis.eis_rs_ins.vcj( 'XXWC_EIS_INV_ALIAS_TRANS_V','MTL_MATERIAL_TRANSACTIONS','MMT',401,'EIATV.TRANSACTION_ID','=','MMT.TRANSACTION_ID(+)','','','','Y','DC005370');
xxeis.eis_rs_ins.vcj( 'XXWC_EIS_INV_ALIAS_TRANS_V','MTL_GENERIC_DISPOSITIONS','MGP',401,'EIATV.DISPOSITION_ID','=','MGP.DISPOSITION_ID(+)','','','','Y','DC005370');
xxeis.eis_rs_ins.vcj( 'XXWC_EIS_INV_ALIAS_TRANS_V','MTL_GENERIC_DISPOSITIONS','MGP',401,'EIATV.MGP_ORGANIZATION_ID','=','MGP.ORGANIZATION_ID(+)','','','','Y','DC005370');
xxeis.eis_rs_ins.vcj( 'XXWC_EIS_INV_ALIAS_TRANS_V','MTL_PARAMETERS','MP',401,'EIATV.MP_ORGANIZATION_ID','=','MP.ORGANIZATION_ID(+)','','','','Y','DC005370');
xxeis.eis_rs_ins.vcj( 'XXWC_EIS_INV_ALIAS_TRANS_V','MTL_TRANSACTION_REASONS','MTR',401,'EIATV.REASON_ID','=','MTR.REASON_ID(+)','','','','Y','DC005370');
xxeis.eis_rs_ins.vcj( 'XXWC_EIS_INV_ALIAS_TRANS_V','HR_ORGANIZATION_UNITS','HAOU',401,'EIATV.HAOU_ORGANIZATION_ID','=','HAOU.ORGANIZATION_ID(+)','','','','Y','DC005370');
xxeis.eis_rs_ins.vcj( 'XXWC_EIS_INV_ALIAS_TRANS_V','MTL_TXN_SOURCE_TYPES','MTS',401,'EIATV.TRANSACTION_SOURCE_TYPE_ID','=','MTS.TRANSACTION_SOURCE_TYPE_ID(+)','','','','Y','DC005370');
xxeis.eis_rs_ins.vcj( 'XXWC_EIS_INV_ALIAS_TRANS_V','MTL_ITEM_LOCATIONS_KFV','MIL',401,'EIATV.INVENTORY_LOCATION_ID','=','MIL.INVENTORY_LOCATION_ID(+)','','','','','DC005370');
xxeis.eis_rs_ins.vcj( 'XXWC_EIS_INV_ALIAS_TRANS_V','MTL_ITEM_LOCATIONS_KFV','MIL',401,'EIATV.MIL_ORGANIZATION_ID','=','MIL.ORGANIZATION_ID(+)','','','','','DC005370');
xxeis.eis_rs_ins.vcj( 'XXWC_EIS_INV_ALIAS_TRANS_V','MTL_TRANSACTION_TYPES','MTT',401,'EIATV.MTT_TRANSACTION_TYPE_ID','=','MTT.TRANSACTION_TYPE_ID(+)','','','','Y','DC005370');
END;
/
set scan on define on
prompt Creating Report LOV Data for Account Alias Transactions - WC
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Account Alias Transactions - WC
xxeis.eis_rs_ins.lov( 401,'select   ORGANIZATION_NAME , ORGANIZATION_CODE
    FROM org_organization_definitions ood
   WHERE EXISTS
  (SELECT 1
  FROM XXEIS.EIS_ORG_ACCESS_V
  WHERE organization_id = ood.organization_id
  )
ORDER BY organization_name','','EIS_INV_INVENTORY_ORGS_LOV','List of All Inventory Orgs under a given operating unit.','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'SELECT DISTINCT concatenated_segments item, description FROM mtl_system_items_kfv msi,
                org_organization_definitions ood
          WHERE msi.organization_id = ood.organization_id
            AND ood.operating_unit = fnd_profile.VALUE (''ORG_ID'')
       ORDER BY concatenated_segments','','EIS_INV_ITEM_LOV','List of all inventory items.','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'select SECONDARY_INVENTORY_NAME SUB_INVENTORY,HAOU.NAME ORGANIZATION_NAME
from MTL_SECONDARY_INVENTORIES MSI,
     HR_ALL_ORGANIZATION_UNITS HAOU
WHERE 1=1
AND MSI.ORGANIZATION_ID = HAOU.organization_id','','EIS_INV_SUBINVENTORY_LOV','List of All SubInventories.','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'select user_name from fnd_user','','EIS_INV_USER','List of All FND users','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 401,'select TRANSACTION_TYPE_NAME from MTL_TRANSACTION_TYPES
where transaction_source_type_id=6
order by TRANSACTION_TYPE_NAME','','INV_TRANSACTION_TYPES_ACCOUNT_ALIAS','','XXEIS_RS_ADMIN',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Account Alias Transactions - WC
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Account Alias Transactions - WC
xxeis.eis_rs_utility.delete_report_rows( 'Account Alias Transactions - WC' );
--Inserting Report - Account Alias Transactions - WC
xxeis.eis_rs_ins.r( 401,'Account Alias Transactions - WC','','Account Alias Transactions Report to display account codes','','','','DC005370','XXWC_EIS_INV_ALIAS_TRANS_V','Y','','','DC005370','N','N','White Cap Reports','RTF,PDF,','CSV,HTML,Html Summary,XML,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - Account Alias Transactions - WC
xxeis.eis_rs_ins.rc( 'Account Alias Transactions - WC',401,'CREATED_BY_USER','Created By User','Created By User','','','','','16','N','','PAGE_FIELD','','','','','','DC005370','N','N','','XXWC_EIS_INV_ALIAS_TRANS_V','','');
xxeis.eis_rs_ins.rc( 'Account Alias Transactions - WC',401,'CREATION_DATE','Creation Date','Creation Date','','','','','17','N','','','','','','','','DC005370','N','N','','XXWC_EIS_INV_ALIAS_TRANS_V','','');
xxeis.eis_rs_ins.rc( 'Account Alias Transactions - WC',401,'ERROR_EXPLANATION','Error Explanation','Error Explanation','','','','','18','N','','','','','','','','DC005370','N','N','','XXWC_EIS_INV_ALIAS_TRANS_V','','');
xxeis.eis_rs_ins.rc( 'Account Alias Transactions - WC',401,'EXPENDITURE_TYPE','Expenditure Type','Expenditure Type','','','','','19','N','','','','','','','','DC005370','N','N','','XXWC_EIS_INV_ALIAS_TRANS_V','','');
xxeis.eis_rs_ins.rc( 'Account Alias Transactions - WC',401,'GL_PERIOD','Gl Period','Gl Period','','','','','20','N','','','','','','','','DC005370','N','N','','XXWC_EIS_INV_ALIAS_TRANS_V','','');
xxeis.eis_rs_ins.rc( 'Account Alias Transactions - WC',401,'GL_PERIOD_START_DATE','Gl Period Start Date','Gl Period Start Date','','','','','21','N','','','','','','','','DC005370','N','N','','XXWC_EIS_INV_ALIAS_TRANS_V','','');
xxeis.eis_rs_ins.rc( 'Account Alias Transactions - WC',401,'INV_ORG_NAME','Inv Org Name','Inv Org Name','','','','','1','N','','ROW_FIELD','','','','1','','DC005370','N','N','','XXWC_EIS_INV_ALIAS_TRANS_V','','');
xxeis.eis_rs_ins.rc( 'Account Alias Transactions - WC',401,'ITEM','Item','Item','','','','','2','N','','ROW_FIELD','','','','3','','DC005370','N','N','','XXWC_EIS_INV_ALIAS_TRANS_V','','');
xxeis.eis_rs_ins.rc( 'Account Alias Transactions - WC',401,'ITEM_DESCRIPTION','Item Description','Item Description','','','','','3','N','','PAGE_FIELD','','','','','','DC005370','N','N','','XXWC_EIS_INV_ALIAS_TRANS_V','','');
xxeis.eis_rs_ins.rc( 'Account Alias Transactions - WC',401,'ITEM_REVISION','Item Revision','Item Revision','','','','','27','N','','','','','','','','DC005370','N','N','','XXWC_EIS_INV_ALIAS_TRANS_V','','');
xxeis.eis_rs_ins.rc( 'Account Alias Transactions - WC',401,'QUANTITY','Quantity','Quantity','','','','','4','N','','DATA_FIELD','','SUM','','1','','DC005370','N','N','','XXWC_EIS_INV_ALIAS_TRANS_V','','');
xxeis.eis_rs_ins.rc( 'Account Alias Transactions - WC',401,'REASON','Reason','Reason','','','','','22','N','','','','','','','','DC005370','N','N','','XXWC_EIS_INV_ALIAS_TRANS_V','','');
xxeis.eis_rs_ins.rc( 'Account Alias Transactions - WC',401,'SEGMENT2','Location','Reason','','','','','7','N','','','','','','','','DC005370','N','N','','XXWC_EIS_INV_ALIAS_TRANS_V','','');
xxeis.eis_rs_ins.rc( 'Account Alias Transactions - WC',401,'SEGMENT3','Account','Reason','','','','','8','N','','','','','','','','DC005370','N','N','','XXWC_EIS_INV_ALIAS_TRANS_V','','');
xxeis.eis_rs_ins.rc( 'Account Alias Transactions - WC',401,'SEGMENT4','Cost Center','Reason','','','','','9','N','','','','','','','','DC005370','N','N','','XXWC_EIS_INV_ALIAS_TRANS_V','','');
xxeis.eis_rs_ins.rc( 'Account Alias Transactions - WC',401,'STOCK_LOCATORS','Stock Locators','Stock Locators','','','','','10','N','','','','','','','','DC005370','N','N','','XXWC_EIS_INV_ALIAS_TRANS_V','','');
xxeis.eis_rs_ins.rc( 'Account Alias Transactions - WC',401,'SUBINVENTORY','Subinventory','Subinventory','','','','','14','N','','ROW_FIELD','','','','2','','DC005370','N','N','','XXWC_EIS_INV_ALIAS_TRANS_V','','');
xxeis.eis_rs_ins.rc( 'Account Alias Transactions - WC',401,'TASK_NAME','Task Name','Task Name','','','','','23','N','','','','','','','','DC005370','N','N','','XXWC_EIS_INV_ALIAS_TRANS_V','','');
xxeis.eis_rs_ins.rc( 'Account Alias Transactions - WC',401,'TASK_NUMBER','Task Number','Task Number','','','','','24','N','','','','','','','','DC005370','N','N','','XXWC_EIS_INV_ALIAS_TRANS_V','','');
xxeis.eis_rs_ins.rc( 'Account Alias Transactions - WC',401,'TRANSACTION_DATE','Transaction Date','Transaction Date','','','','','25','N','','','','','','','','DC005370','N','N','','XXWC_EIS_INV_ALIAS_TRANS_V','','');
xxeis.eis_rs_ins.rc( 'Account Alias Transactions - WC',401,'TRANSACTION_ID','Transaction Id','Transaction Id','','','','','26','N','','','','','','','','DC005370','N','N','','XXWC_EIS_INV_ALIAS_TRANS_V','','');
xxeis.eis_rs_ins.rc( 'Account Alias Transactions - WC',401,'TRANSACTION_TYPE','Transaction Type','Transaction Type','','','','','15','N','','','','','','','','DC005370','N','N','','XXWC_EIS_INV_ALIAS_TRANS_V','','');
xxeis.eis_rs_ins.rc( 'Account Alias Transactions - WC',401,'UNIT_COST','Unit Cost','Unit Cost','','','','','11','N','','','','','','','','DC005370','N','N','','XXWC_EIS_INV_ALIAS_TRANS_V','','');
xxeis.eis_rs_ins.rc( 'Account Alias Transactions - WC',401,'UNIT_OF_MEASURE','Unit Of Measure','Unit Of Measure','','','','','12','N','','','','','','','','DC005370','N','N','','XXWC_EIS_INV_ALIAS_TRANS_V','','');
xxeis.eis_rs_ins.rc( 'Account Alias Transactions - WC',401,'VALUE','Value','Value','','','','','13','N','','DATA_FIELD','','SUM','','2','','DC005370','N','N','','XXWC_EIS_INV_ALIAS_TRANS_V','','');
xxeis.eis_rs_ins.rc( 'Account Alias Transactions - WC',401,'SOURCE','Source','Value','','','','','5','N','','','','','','','','DC005370','N','N','','XXWC_EIS_INV_ALIAS_TRANS_V','','');
xxeis.eis_rs_ins.rc( 'Account Alias Transactions - WC',401,'PRODUCT','Product','Value','','','','','6','N','','','','','','','','DC005370','N','N','','XXWC_EIS_INV_ALIAS_TRANS_V','','');
--Inserting Report Parameters - Account Alias Transactions - WC
xxeis.eis_rs_ins.rp( 'Account Alias Transactions - WC',401,'Created By User','Created By User','CREATED_BY_USER','IN','EIS_INV_USER','','VARCHAR2','N','Y','5','','N','CONSTANT','DC005370','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Account Alias Transactions - WC',401,'Organization','Organization','INV_ORG_NAME','IN','EIS_INV_INVENTORY_ORGS_LOV','','VARCHAR2','N','Y','1','','N','CONSTANT','DC005370','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Account Alias Transactions - WC',401,'Inventory Item','Inventory Item','ITEM','IN','EIS_INV_ITEM_LOV','','VARCHAR2','N','Y','3','','N','CONSTANT','DC005370','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Account Alias Transactions - WC',401,'Subinventory','Subinventory','SUBINVENTORY','IN','EIS_INV_SUBINVENTORY_LOV','','VARCHAR2','N','Y','2','','N','CONSTANT','DC005370','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Account Alias Transactions - WC',401,'Transaction Type','Transaction Type','TRANSACTION_TYPE','IN','INV_TRANSACTION_TYPES_ACCOUNT_ALIAS','','VARCHAR2','N','Y','4','','N','CONSTANT','DC005370','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Account Alias Transactions - WC',401,'Transaction Start Date','Transaction Start Date','TRANSACTION_DATE','IN','','','VARCHAR2','N','Y','6','','N','CONSTANT','DC005370','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Account Alias Transactions - WC',401,'Transaction End Date','Transaction End Date','TRANSACTION_DATE','IN','','','VARCHAR2','N','Y','7','','N','CONSTANT','DC005370','Y','N','','','');
--Inserting Report Conditions - Account Alias Transactions - WC
xxeis.eis_rs_ins.rcn( 'Account Alias Transactions - WC',401,'CREATED_BY_USER','IN',':Created By User','','','Y','5','Y','DC005370');
xxeis.eis_rs_ins.rcn( 'Account Alias Transactions - WC',401,'INV_ORG_NAME','IN',':Organization','','','Y','1','Y','DC005370');
xxeis.eis_rs_ins.rcn( 'Account Alias Transactions - WC',401,'ITEM','IN',':Item','','','Y','3','Y','DC005370');
xxeis.eis_rs_ins.rcn( 'Account Alias Transactions - WC',401,'SUBINVENTORY','IN',':Subinventory','','','Y','2','Y','DC005370');
xxeis.eis_rs_ins.rcn( 'Account Alias Transactions - WC',401,'TRANSACTION_TYPE','IN',':Transaction Type','','','Y','4','Y','DC005370');
xxeis.eis_rs_ins.rcn( 'Account Alias Transactions - WC',401,'TRANSACTION_DATE','IN',':Transaction Start Date','','','Y','6','Y','DC005370');
xxeis.eis_rs_ins.rcn( 'Account Alias Transactions - WC',401,'TRANSACTION_DATE','IN',':Transaction End Date','','','Y','7','Y','DC005370');
--Inserting Report Sorts - Account Alias Transactions - WC
xxeis.eis_rs_ins.rs( 'Account Alias Transactions - WC',401,'ITEM','ASC','DC005370');
xxeis.eis_rs_ins.rs( 'Account Alias Transactions - WC',401,'TRANSACTION_TYPE','ASC','DC005370');
--Inserting Report Triggers - Account Alias Transactions - WC
--Inserting Report Templates - Account Alias Transactions - WC
xxeis.eis_rs_ins.R_Tem( 'Account Alias Transactions - WC','Account Alias Transactions - WC','Seeded template for Account Alias Transactions - WC','','','','','','','','','','','Account Alias Transactions - WC.rtf','DC005370');
--Inserting Report Portals - Account Alias Transactions - WC
--Inserting Report Dashboards - Account Alias Transactions - WC
--Inserting Report Security - Account Alias Transactions - WC
xxeis.eis_rs_ins.rsec( 'Account Alias Transactions - WC','401','','50619',401,'DC005370','');
xxeis.eis_rs_ins.rsec( 'Account Alias Transactions - WC','401','','50879',401,'DC005370','');
xxeis.eis_rs_ins.rsec( 'Account Alias Transactions - WC','401','','50851',401,'DC005370','');
xxeis.eis_rs_ins.rsec( 'Account Alias Transactions - WC','401','','50852',401,'DC005370','');
xxeis.eis_rs_ins.rsec( 'Account Alias Transactions - WC','401','','50821',401,'DC005370','');
END;
/
set scan on define on
