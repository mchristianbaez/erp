--Report Name            : Rental Invoice Report - WC
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data EIS_XXWC_OM_INV_PRE_REG_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_OM_INV_PRE_REG_V
xxeis.eis_rsc_ins.v( 'EIS_XXWC_OM_INV_PRE_REG_V',660,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Xxwc Om Inv Pre Reg V','EXOIPRV','','','VIEW','US','','','');
--Delete Object Columns for EIS_XXWC_OM_INV_PRE_REG_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_OM_INV_PRE_REG_V',660,FALSE);
--Inserting Object Columns for EIS_XXWC_OM_INV_PRE_REG_V
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','DISCOUNT_AMT',660,'Discount Amt','DISCOUNT_AMT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Discount Amt','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','TAX_VALUE',660,'Tax Value','TAX_VALUE','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Tax Value','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','GROSS_AMOUNT',660,'Gross Amount','GROSS_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Gross Amount','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','CUSTOMER_NUMBER',660,'Customer Number','CUSTOMER_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','CUSTOMER_NAME',660,'Customer Name','CUSTOMER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','ORDER_STATUS',660,'Order Status','ORDER_STATUS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Order Status','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','ORDERED_DATE',660,'Ordered Date','ORDERED_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Ordered Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','ORDER_NUMBER',660,'Order Number','ORDER_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Order Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','WAREHOUSE',660,'Warehouse','WAREHOUSE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Warehouse','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','AVERAGE_COST',660,'Average Cost','AVERAGE_COST','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Average Cost','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','PROFIT',660,'Profit','PROFIT','','','','XXEIS_RS_ADMIN','NUMBER','','','Profit','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','ORDER_TYPE',660,'Order Type','ORDER_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Order Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','CREATED_BY',660,'Created By','CREATED_BY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Created By','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','SALESREP_NAME',660,'Salesrep Name','SALESREP_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Salesrep Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','TRX_NUMBER',660,'Trx Number','TRX_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Trx Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','PAYMENT_TERM',660,'Payment Term','PAYMENT_TERM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Payment Term','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','SALE_COST',660,'Sale Cost','SALE_COST','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Sale Cost','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','ITEM',660,'Item','ITEM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Item','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','ITEM_DESC',660,'Item Desc','ITEM_DESC','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Item Desc','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','QTY',660,'Qty','QTY','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Qty','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','UNIT_SELLING_PRICE',660,'Unit Selling Price','UNIT_SELLING_PRICE','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Unit Selling Price','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','ORDER_LINE',660,'Order Line','ORDER_LINE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Order Line','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','INVOICE_DATE',660,'Invoice Date','INVOICE_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Invoice Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','SALES',660,'Sales','SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','Sales','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','JOB_NUMBER',660,'Job Number','JOB_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Job Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','ORDER_SOURCE',660,'Order Source','ORDER_SOURCE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Order Source','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','SPECIAL_COST',660,'Special Cost','SPECIAL_COST','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Special Cost','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','MODIFIER_NAME',660,'Modifier Name','MODIFIER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Modifier Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','FREIGHT_AMT',660,'Freight Amt','FREIGHT_AMT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Freight Amt','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','INV_AMOUNT',660,'Inv Amount','INV_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Inv Amount','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','SALESREP_NUMBER',660,'Salesrep Number','SALESREP_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Salesrep Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','HEADER_ID',660,'Header Id','HEADER_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Header Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','ORGANIZATION_ID',660,'Organization Id','ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Organization Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','PAYMENT_DESC',660,'Payment Desc','PAYMENT_DESC','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Payment Desc','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','UOM',660,'Uom','UOM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Uom','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','CAT',660,'Cat','CAT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cat','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','LINE_FLOW_STATUS_CODE',660,'Line Flow Status Code','LINE_FLOW_STATUS_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Line Flow Status Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','VQN_MODIFIER_NAME',660,'Vqn Modifier Name','VQN_MODIFIER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vqn Modifier Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','LINE_TYPE',660,'Line Type','LINE_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Line Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','PARTY_ID',660,'Party Id','PARTY_ID','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Party Id','','','','US','');
--Inserting Object Components for EIS_XXWC_OM_INV_PRE_REG_V
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_OM_INV_PRE_REG_V','MTL_PARAMETERS',660,'MTL_PARAMETERS','SHIP_FROM_ORG','SHIP_FROM_ORG','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Inventory Control Options And Defaults','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_OM_INV_PRE_REG_V','HZ_PARTIES',660,'HZ_PARTIES','PARTY','PARTY','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Information About Parties Such As Organizations, P','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_OM_INV_PRE_REG_V','OE_ORDER_HEADERS',660,'OE_ORDER_HEADERS_ALL','OH','OH','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Oe Order Headers All Stores Header Information For','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_OM_INV_PRE_REG_V','OE_ORDER_LINES',660,'OE_ORDER_LINES_ALL','OL','OL','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Oe Order Lines All Stores Information For All Orde','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_OM_INV_PRE_REG_V','MTL_SYSTEM_ITEMS_KFV',660,'MTL_SYSTEM_ITEMS_B','MSI','MSI','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Inventory Item Definitions','','','','','','',' ',' ',' ',' ',' ');
--Inserting Object Component Joins for EIS_XXWC_OM_INV_PRE_REG_V
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_OM_INV_PRE_REG_V','MTL_PARAMETERS','SHIP_FROM_ORG',660,'EXOIPRV.ORGANIZATION_ID','=','SHIP_FROM_ORG.ORGANIZATION_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_OM_INV_PRE_REG_V','HZ_PARTIES','PARTY',660,'EXOIPRV.PARTY_ID','=','PARTY.PARTY_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_OM_INV_PRE_REG_V','OE_ORDER_HEADERS','OH',660,'EXOIPRV.HEADER_ID','=','OH.HEADER_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_OM_INV_PRE_REG_V','OE_ORDER_LINES','OL',660,'EXOIPRV.LINE_ID','=','OL.LINE_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_OM_INV_PRE_REG_V','MTL_SYSTEM_ITEMS_KFV','MSI',660,'EXOIPRV.INVENTORY_ITEM_ID','=','MSI.INVENTORY_ITEM_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_OM_INV_PRE_REG_V','MTL_SYSTEM_ITEMS_KFV','MSI',660,'EXOIPRV.MSI_ORGANIZATION_ID','=','MSI.ORGANIZATION_ID(+)','','','','Y','XXEIS_RS_ADMIN');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
prompt Creating Report LOV Data for Rental Invoice Report - WC
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - Rental Invoice Report - WC
xxeis.eis_rsc_ins.lov( 660,'select  RS.Name,SALESREP_ID  from  RA_SALESREPS RS
WHERE  RS.NAME is not null','','OM SALES REP','This gives the sales representative name','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'select nvl( party.party_name,CUST_ACCT.account_name) customer_name,CUST_ACCT.ACCOUNT_NUMBER
from  HZ_PARTIES PARTY, HZ_CUST_ACCOUNTS CUST_ACCT where CUST_ACCT.PARTY_ID = PARTY.PARTY_ID AND  PARTY.status=''A''
','','OM CUSTOMER NAME','This gives the Customer Name','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'Select  name, description from ra_terms_vl','','WC OM Payment Terms','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'SELECT ORGANIZATION_CODE WAREHOUSE,ORGANIZATION_NAME FROM ORG_ORGANIZATION_DEFINITIONS OOD WHERE SYSDATE < NVL(OOD.DISABLE_DATE,SYSDATE+1) AND EXISTS (SELECT 1 FROM XXEIS.EIS_ORG_ACCESS_V  WHERE organization_id = ood.organization_id )
UNION
SELECT ''All'', ''All Organizations'' from Dual','','OM Warehouse All','','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'SELECT DISTINCT attribute7 created_by
FROM apps.oe_order_headers_all','','Order Created By Lov','','ANONYMOUS',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
prompt Creating Report Data for Rental Invoice Report - WC
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Rental Invoice Report - WC
xxeis.eis_rsc_utility.delete_report_rows( 'Rental Invoice Report - WC',660 );
--Inserting Report - Rental Invoice Report - WC
xxeis.eis_rsc_ins.r( 660,'Rental Invoice Report - WC','','This Report is copied from Invoice Pre-Register report.Real-time branch sales; current day?s sales ? at a detail and summary level.','','','','SA059956','EIS_XXWC_OM_INV_PRE_REG_V','Y','','','SA059956','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N','','','','','','N','','US','','Invoice Pre-Register report','','','','','','','','','','','','','','');
--Inserting Report Columns - Rental Invoice Report - WC
xxeis.eis_rsc_ins.rc( 'Rental Invoice Report - WC',660,'CUSTOMER_NAME','Customer Name','Customer Name','','','default','','10','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Rental Invoice Report - WC',660,'ORDERED_DATE','Ordered Date','Ordered Date','','','default','','2','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Rental Invoice Report - WC',660,'ORDER_NUMBER','Order Number','Order Number','','~~~','default','','4','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Rental Invoice Report - WC',660,'ORDER_TYPE','Order Type','Order Type','','','default','','5','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Rental Invoice Report - WC',660,'CREATED_BY','Created By','Created By','','','default','','6','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Rental Invoice Report - WC',660,'TRX_NUMBER','Invoice Number','Trx Number','','','default','','9','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Rental Invoice Report - WC',660,'WAREHOUSE','Warehouse','Warehouse','','','default','','1','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Rental Invoice Report - WC',660,'SALESREP_NAME','Salesrep Name','Salesrep Name','','','default','','7','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Rental Invoice Report - WC',660,'ITEM','SKU','Item','','','default','','12','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Rental Invoice Report - WC',660,'ITEM_DESC','SKU Description','Item Desc','','','default','','13','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Rental Invoice Report - WC',660,'ORDER_LINE','Line','Order Line','','','default','','11','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Rental Invoice Report - WC',660,'UNIT_SELLING_PRICE','Unit Sell Price','Unit Selling Price','','~T~D~2','default','','15','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Rental Invoice Report - WC',660,'QTY','Order Qty','Qty','','~T~D~0','default','','14','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Rental Invoice Report - WC',660,'INVOICE_DATE','Invoice Date','Invoice Date','','','default','','8','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Rental Invoice Report - WC',660,'SALES','Extended Sell Price','Sales','','~T~D~2','default','','16','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Rental Invoice Report - WC',660,'PAYMENT_TERM','Payment Term','Payment Term','','','','','17','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Rental Invoice Report - WC',660,'Days_On_Rent','Days On Rent','Days On Rent','NUMBER','~~~','default','','3','Y','Y','','','','','','(round(to_date(:As Of Date ,XXEIS.EIS_RSC_UTILITY.GET_DATE_FORMAT)-(EXOIPRV.ORDERED_DATE)))','SA059956','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','','null','US','','');
--Inserting Report Parameters - Rental Invoice Report - WC
xxeis.eis_rsc_ins.rp( 'Rental Invoice Report - WC',660,'Warehouse','Warehouse','WAREHOUSE','IN','OM Warehouse All','','VARCHAR2','Y','Y','1','Y','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_OM_INV_PRE_REG_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Rental Invoice Report - WC',660,'Created By','Created By','CREATED_BY','IN','Order Created By Lov','','VARCHAR2','N','Y','3','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_OM_INV_PRE_REG_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Rental Invoice Report - WC',660,'Salesrep Name','Salesrep Name','SALESREP_NAME','IN','OM SALES REP','','VARCHAR2','N','Y','4','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_OM_INV_PRE_REG_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Rental Invoice Report - WC',660,'Payment Term','Payment Term','PAYMENT_TERM','IN','WC OM Payment Terms','','VARCHAR2','N','Y','5','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_OM_INV_PRE_REG_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Rental Invoice Report - WC',660,'Customer Name','Customer Name','CUSTOMER_NAME','IN','OM CUSTOMER NAME','','VARCHAR2','N','Y','6','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_OM_INV_PRE_REG_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Rental Invoice Report - WC',660,'As Of Date','As Of Date','ORDERED_DATE','>=','','','DATE','Y','Y','2','Y','N','CONSTANT','SA059956','Y','N','','Start Date','','EIS_XXWC_OM_INV_PRE_REG_V','','','US','');
--Inserting Dependent Parameters - Rental Invoice Report - WC
--Inserting Report Conditions - Rental Invoice Report - WC
xxeis.eis_rsc_ins.rcnh( 'Rental Invoice Report - WC',660,'Free Text ','FREE_TEXT','','','Y','','5');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','','Y','','','','','AND ( ''All'' IN (:Warehouse) OR (WAREHOUSE IN (:Warehouse)))
AND (EXOIPRV.ORDER_TYPE != ''STANDARD ORDER'' OR  (EXOIPRV.ORDER_TYPE = ''STANDARD ORDER''  AND EXOIPRV.ORDERED_DATE < =:As Of Date) OR (EXOIPRV.ORDER_TYPE = ''STANDARD ORDER''  AND EXOIPRV.LINE_FLOW_STATUS_CODE IN (''CLOSED'',''INVOICE_DELIVERY'',''INVOICE_HOLD'',
''INVOICE_INCOMPLETE'',''INVOICE_NOT_APPLICABLE'',''INVOICE_RFR'',''INVOICE_UNEXPECTED_ERROR'',
''PARTIAL_INVOICE_RFR'')))
','1',660,'Rental Invoice Report - WC','Free Text ');
xxeis.eis_rsc_ins.rcnh( 'Rental Invoice Report - WC',660,'EXOIPRV.CREATED_BY IN Created By','SIMPLE','','','Y','','1');
xxeis.eis_rsc_ins.rcnd( '','','CREATED_BY','','Created By','','','','','EIS_XXWC_OM_INV_PRE_REG_V','','','','','','IN','Y','Y','','','','','1',660,'Rental Invoice Report - WC','EXOIPRV.CREATED_BY IN Created By');
xxeis.eis_rsc_ins.rcnh( 'Rental Invoice Report - WC',660,'EXOIPRV.SALESREP_NAME IN Salesrep Name','SIMPLE','','','Y','','2');
xxeis.eis_rsc_ins.rcnd( '','','SALESREP_NAME','','Salesrep Name','','','','','EIS_XXWC_OM_INV_PRE_REG_V','','','','','','IN','Y','Y','','','','','1',660,'Rental Invoice Report - WC','EXOIPRV.SALESREP_NAME IN Salesrep Name');
xxeis.eis_rsc_ins.rcnh( 'Rental Invoice Report - WC',660,'EXOIPRV.PAYMENT_TERM IN Payment Term','SIMPLE','','','Y','','3');
xxeis.eis_rsc_ins.rcnd( '','','PAYMENT_TERM','','Payment Term','','','','','EIS_XXWC_OM_INV_PRE_REG_V','','','','','','IN','Y','Y','','','','','1',660,'Rental Invoice Report - WC','EXOIPRV.PAYMENT_TERM IN Payment Term');
xxeis.eis_rsc_ins.rcnh( 'Rental Invoice Report - WC',660,'EXOIPRV.CUSTOMER_NAME IN Customer Name','SIMPLE','','','Y','','4');
xxeis.eis_rsc_ins.rcnd( '','','CUSTOMER_NAME','','Customer Name','','','','','EIS_XXWC_OM_INV_PRE_REG_V','','','','','','IN','Y','Y','','','','','1',660,'Rental Invoice Report - WC','EXOIPRV.CUSTOMER_NAME IN Customer Name');
--Inserting Report Sorts - Rental Invoice Report - WC
xxeis.eis_rsc_ins.rs( 'Rental Invoice Report - WC',660,'ORDER_NUMBER','ASC','SA059956','2','');
xxeis.eis_rsc_ins.rs( 'Rental Invoice Report - WC',660,'WAREHOUSE','ASC','SA059956','1','');
xxeis.eis_rsc_ins.rs( 'Rental Invoice Report - WC',660,'TRX_NUMBER','ASC','SA059956','3','');
xxeis.eis_rsc_ins.rs( 'Rental Invoice Report - WC',660,'','ASC','SA059956','4','lpad(EXOIPRV.ORDER_LINE,10)');
--Inserting Report Triggers - Rental Invoice Report - WC
xxeis.eis_rsc_ins.rt( 'Rental Invoice Report - WC',660,'begin
XXEIS.EIS_XXWC_INV_PRE_REG_RPT_PKG.set_date_from(:As Of Date);
end;','B','Y','SA059956','AQ');
--inserting report templates - Rental Invoice Report - WC
--Inserting Report Portals - Rental Invoice Report - WC
--inserting report dashboards - Rental Invoice Report - WC
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Rental Invoice Report - WC','660','EIS_XXWC_OM_INV_PRE_REG_V','EIS_XXWC_OM_INV_PRE_REG_V','N','');
--inserting report security - Rental Invoice Report - WC
xxeis.eis_rsc_ins.rsec( 'Rental Invoice Report - WC','660','','XXWC_AO_OEENTRY',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Rental Invoice Report - WC','660','','XXWC_AO_OEENTRY_REC',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Rental Invoice Report - WC','660','','XXWC_AO_OEENTRY_PO_RPT',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Rental Invoice Report - WC','660','','XXWC_RENTAL_OM_PRICING_SUPER',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Rental Invoice Report - WC','660','','XXWC_ORDER_MGMT_READ_SHIPPING',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Rental Invoice Report - WC','660','','XXWC_ORDER_MGMT_READ_ONLY',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Rental Invoice Report - WC','660','','XXWC_ORDER_MGMT_PRICING_SUPER',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Rental Invoice Report - WC','660','','XXWC_ORDER_MGMT_PRICING_STD',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Rental Invoice Report - WC','660','','XXWC_ORDER_MGMT_PRICING_LTD',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Rental Invoice Report - WC','660','','ORDER_MGMT_SUPER_USER',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Rental Invoice Report - WC','20005','','XXWC_VIEW_ALL_EIS_REPORTS',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Rental Invoice Report - WC','20005','','XXWC_SALES_SUPPORT_ADMIN',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Rental Invoice Report - WC','660','','XXWC_ORDER_MGMT_PRICING_FULL',660,'SA059956','','','');
--Inserting Report Pivots - Rental Invoice Report - WC
xxeis.eis_rsc_ins.rpivot( 'Rental Invoice Report - WC',660,'Pivot','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rsc_ins.rpivot_dtls( 'Rental Invoice Report - WC',660,'Pivot','ORDER_NUMBER','ROW_FIELD','','','3','','','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Rental Invoice Report - WC',660,'Pivot','CREATED_BY','ROW_FIELD','','','2','','','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Rental Invoice Report - WC',660,'Pivot','WAREHOUSE','ROW_FIELD','','','1','','','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Rental Invoice Report - WC',660,'Pivot','SALES','DATA_FIELD','SUM','','1','','','');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
--Inserting Report Distribution Details 
--Inserting Report Distribution Details 
--Inserting Report SubTotals Details 
--Inserting Report aggregations 
--Inserting Report   Version details- Rental Invoice Report - WC
xxeis.eis_rsc_ins.rv( 'Rental Invoice Report - WC','','Rental Invoice Report - WC','SA059956','11-JUN-2018');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
