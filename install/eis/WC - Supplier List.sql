--Report Name            : WC - Supplier List
--Import Instructions : 1. If RTF template needs to be uploaded to report automatically then move the RTF template to physical path of APPS_DATA_FILE_DIR oracle directory. Check dba_directories view to get the physical path of oracle directory. 
--                         (If you want to use any other oracle directory other than APPS_DATA_FILE_DIR then replace APPS_DATA_FILE_DIR with your oracle directory name below.)
--                      2. Make sure XXEIS schema has read/write permissions to above used oracle directory and give full permissions to the directory at OS level.
--                      3. Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
exec xxeis.eis_rsc_ins.g_rtf_dir := 'APPS_DATA_FILE_DIR' ;
prompt Creating Object Data EIS_AP_SUPPLIERS_DET_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_AP_SUPPLIERS_DET_V
xxeis.eis_rsc_ins.v( 'EIS_AP_SUPPLIERS_DET_V',200,'This view shows details of supplier setups including credit limit information.','','','','XXEIS_RS_ADMIN','XXEIS','EIS AP Supplier Details','EAST','','','VIEW','US','Y','');
--Delete Object Columns for EIS_AP_SUPPLIERS_DET_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_AP_SUPPLIERS_DET_V',200,FALSE);
--Inserting Object Columns for EIS_AP_SUPPLIERS_DET_V
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','MINORITY_GROUP',200,'Type of minority-owned business','MINORITY_GROUP','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_VENDORS','MINORITY_GROUP_LOOKUP_CODE','Minority Group','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','WOMEN_OWNED',200,'Women Owned','WOMEN_OWNED','','','','XXEIS_RS_ADMIN','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','Women Owned','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','SMALL_BUSINESS',200,'Small Business','SMALL_BUSINESS','','','','XXEIS_RS_ADMIN','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','Small Business','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','PURCHASING_HOLD_REASON',200,'Reason for placing the supplier on purchasing hold','PURCHASING_HOLD_REASON','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_VENDORS','PURCHASING_HOLD_REASON','Purchasing Hold Reason','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','INSPECTION_REQUIRED',200,'Inspection Required','INSPECTION_REQUIRED','','','','XXEIS_RS_ADMIN','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','Inspection Required','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','RECEIPT_REQUIRED',200,'Receipt Required','RECEIPT_REQUIRED','','','','XXEIS_RS_ADMIN','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','Receipt Required','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','HOLD_PURCHASES',200,'Hold Purchases','HOLD_PURCHASES','','','','XXEIS_RS_ADMIN','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','Hold Purchases','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','PURCHASING_SITE',200,'Purchasing Site','PURCHASING_SITE','','','','XXEIS_RS_ADMIN','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','Purchasing Site','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','PAY_SITE',200,'Pay Site','PAY_SITE','','','','XXEIS_RS_ADMIN','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','Pay Site','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','TAX_REPORTING_SITE',200,'Tax Reporting Site','TAX_REPORTING_SITE','','','','XXEIS_RS_ADMIN','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','Tax Reporting Site','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','ADDRESS_LINE1',200,'First line of supplier address','ADDRESS_LINE1','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_VENDOR_SITES_ALL','ADDRESS_LINE1','Address Line1','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','ADDRESS_LINE2',200,'Second line of supplier address','ADDRESS_LINE2','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_VENDOR_SITES_ALL','ADDRESS_LINE2','Address Line2','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','ADDRESS_LINE3',200,'Third line of supplier address','ADDRESS_LINE3','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_VENDOR_SITES_ALL','ADDRESS_LINE3','Address Line3','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','ADDRESS_LINE4',200,'Fourth line of address','ADDRESS_LINE4','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_VENDOR_SITES_ALL','ADDRESS_LINE4','Address Line4','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','CITY',200,'City name','CITY','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_VENDOR_SITES_ALL','CITY','City','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','HOLD_ALL_PAYMENTS',200,'Indicates whether Oracle Payables should place all payments for this supplier on hold','HOLD_ALL_PAYMENTS~HOLD_ALL_PAYMENTS','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_SUPPLIER_SITES_ALL','HOLD_ALL_PAYMENTS_FLAG','Hold All Payments','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','INCOME_TAX_REPORTABLE',200,'Federal reportable flag','INCOME_TAX_REPORTABLE~INCOME_TAX_REPORTABLE','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_SUPPLIERS','FEDERAL_REPORTABLE_FLAG','Income Tax Reportable','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','INCOME_TAX_TYPE',200,'Type of 1099','INCOME_TAX_TYPE~INCOME_TAX_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_SUPPLIERS','TYPE_1099','Income Tax Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','PO_HOLD',200,'Indicator of whether the supplier is on purchasing hold','PO_HOLD~PO_HOLD','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_SUPPLIERS','HOLD_FLAG','Po Hold','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','SUPPLIER_SITE_NAME',200,'Site code name','SUPPLIER_SITE_NAME~SUPPLIER_SITE_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_SUPPLIER_SITES_ALL','VENDOR_SITE_CODE','Supplier Site Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','HOLD_UNVALIDATED_INVOICES',200,'Indicates whether Oracle Payables should place unapproved invoices for this supplier on hold','HOLD_UNVALIDATED_INVOICES~HOLD_UNVALIDATED_INVOICES','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_SUPPLIER_SITES_ALL','HOLD_FUTURE_PAYMENTS_FLAG','Hold Unvalidated Invoices','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','STATE_REPORTABLE',200,'State Reportable','STATE_REPORTABLE','','','','XXEIS_RS_ADMIN','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','State Reportable','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','PAY_GROUP',200,'Pay Group','PAY_GROUP','','','','XXEIS_RS_ADMIN','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','Pay Group','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','PAYMENT_TERMS',200,'Payment Terms','PAYMENT_TERMS','','','','XXEIS_RS_ADMIN','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','Payment Terms','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','HOLD_REASON',200,'Hold Reason','HOLD_REASON','','','','XXEIS_RS_ADMIN','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','Hold Reason','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','CONTACT_FIRST_NAME',200,'Contact first name','CONTACT_FIRST_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_VENDOR_CONTACTS','FIRST_NAME','Contact First Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','CONTACT_LAST_NAME',200,'Contact last name','CONTACT_LAST_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_VENDOR_CONTACTS','LAST_NAME','Contact Last Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','CONTACT_PHONE',200,'Contact phone number','CONTACT_PHONE','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_VENDOR_CONTACTS','PHONE','Contact Phone','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','SUPPLIER_NAME',200,'Supplier name','SUPPLIER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_VENDORS','VENDOR_NAME','Supplier Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','SUPPLIER_NUMBER',200,'Supplier number','SUPPLIER_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_VENDORS','SEGMENT1','Supplier Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','SUPPLIER_TYPE',200,'QuickCode code','SUPPLIER_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','FND_LOOKUP_VALUES','LOOKUP_CODE','Supplier Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','SUPPLIER_CREATED_BY',200,'Application username (what a user types in at the Oracle Applications sign-on screen)','SUPPLIER_CREATED_BY','','','','XXEIS_RS_ADMIN','VARCHAR2','FND_USER','USER_NAME','Supplier Created By','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','SUPPLIER_CREATION_DATE',200,'Standard Who column','SUPPLIER_CREATION_DATE','','','','XXEIS_RS_ADMIN','DATE','PO_VENDORS','CREATION_DATE','Supplier Creation Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','OPERATING_UNIT',200,'Translated name of the organization','OPERATING_UNIT~OPERATING_UNIT','','','','XXEIS_RS_ADMIN','VARCHAR2','HR_ALL_ORGANIZATION_UNITS_TL','NAME','Operating Unit','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','SITE_CREATED_BY',200,'Application username (what a user types in at the Oracle Applications sign-on screen)','SITE_CREATED_BY','','','','XXEIS_RS_ADMIN','VARCHAR2','FND_USER','USER_NAME','Site Created By','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','SITE_CREATION_DATE',200,'Standard Who column','SITE_CREATION_DATE','','','','XXEIS_RS_ADMIN','DATE','PO_VENDOR_SITES_ALL','CREATION_DATE','Site Creation Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','SITE_UPDATED_BY',200,'Application username (what a user types in at the Oracle Applications sign-on screen)','SITE_UPDATED_BY','','','','XXEIS_RS_ADMIN','VARCHAR2','FND_USER','USER_NAME','Site Updated By','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','SITE_UPDATE_DATE',200,'Standard Who column','SITE_UPDATE_DATE','','','','XXEIS_RS_ADMIN','DATE','PO_VENDOR_SITES_ALL','LAST_UPDATE_DATE','Site Update Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','SUPPLIER_UPDATED_BY',200,'Application username (what a user types in at the Oracle Applications sign-on screen)','SUPPLIER_UPDATED_BY','','','','XXEIS_RS_ADMIN','VARCHAR2','FND_USER','USER_NAME','Supplier Updated By','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','SUPPLIER_UPDATE_DATE',200,'Standard Who column','SUPPLIER_UPDATE_DATE','','','','XXEIS_RS_ADMIN','DATE','PO_VENDORS','LAST_UPDATE_DATE','Supplier Update Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','ALTERNATE_NAME',200,'Alternate supplier name for kana value','ALTERNATE_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_VENDORS','VENDOR_NAME_ALT','Alternate Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','TAXPAYER_ID',200,'Tax identification number','TAXPAYER_ID','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_VENDORS','NUM_1099','Taxpayer Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','TAX_REGISTRATION_NUMBER',200,'VAT registration number','TAX_REGISTRATION_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_VENDORS','VAT_REGISTRATION_NUM','Tax Registration Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','INACTIVE_ON',200,'Key flexfield end date','INACTIVE_ON','','','','XXEIS_RS_ADMIN','DATE','PO_VENDORS','END_DATE_ACTIVE','Inactive On','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','PARENT_SUPPLIER_NAME',200,'Supplier name','PARENT_SUPPLIER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_VENDORS','VENDOR_NAME','Parent Supplier Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','PARENT_SUPPLIER_NUMBER',200,'Supplier number','PARENT_SUPPLIER_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_VENDORS','SEGMENT1','Parent Supplier Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','ONE_TIME',200,'One Time','ONE_TIME','','','','XXEIS_RS_ADMIN','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','One Time','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','CREDIT_STATUS',200,'No longer used','CREDIT_STATUS','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_VENDORS','CREDIT_STATUS_LOOKUP_CODE','Credit Status','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','CREDIT_LIMIT',200,'Not used','CREDIT_LIMIT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','PO_VENDORS','CREDIT_LIMIT','Credit Limit','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','WITHHOLDING_STATUS',200,'Withholding status type','WITHHOLDING_STATUS','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_VENDORS','WITHHOLDING_STATUS_LOOKUP_CODE','Withholding Status','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','STATE',200,'State name or abbreviation','STATE','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_VENDOR_SITES_ALL','STATE','State','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','ZIP',200,'Postal code','ZIP','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_VENDOR_SITES_ALL','ZIP','Zip','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','COUNTRY',200,'Country name','COUNTRY','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_VENDOR_SITES_ALL','COUNTRY','Country','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','PHONE',200,'Phone number','PHONE','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_VENDOR_SITES_ALL','PHONE','Phone','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','FAX',200,'Customer site facsimile number','FAX','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_VENDOR_SITES_ALL','FAX','Fax','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','EMAIL_ADDRESS',200,'Email Address of the supplier Contact','EMAIL_ADDRESS','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_VENDOR_SITES_ALL','EMAIL_ADDRESS','Email Address','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','SITE_INACTIVE_ON',200,'Inactive date for record','SITE_INACTIVE_ON','','','','XXEIS_RS_ADMIN','DATE','PO_VENDOR_SITES_ALL','INACTIVE_DATE','Site Inactive On','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','VENDOR_SITE_ID',200,'Supplier site unique identifier','VENDOR_SITE_ID','','','','XXEIS_RS_ADMIN','NUMBER','PO_VENDOR_SITES_ALL','VENDOR_SITE_ID','Vendor Site Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','VENDOR_ID',200,'Supplier unique identifier','VENDOR_ID','','','','XXEIS_RS_ADMIN','NUMBER','PO_VENDORS','VENDOR_ID','Vendor Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','MIN_ORDER_AMOUNT',200,'Minimum purchase order amount','MIN_ORDER_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','PO_VENDORS','MIN_ORDER_AMOUNT','Min Order Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','SITE_CREATED_BY_ID',200,'Standard Who column','SITE_CREATED_BY_ID','','','','XXEIS_RS_ADMIN','NUMBER','PO_VENDOR_SITES_ALL','CREATED_BY','Site Created By Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','SITE_UPDATED_BY_ID',200,'Standard Who column','SITE_UPDATED_BY_ID','','','','XXEIS_RS_ADMIN','NUMBER','PO_VENDOR_SITES_ALL','LAST_UPDATED_BY','Site Updated By Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','RFQ_SITE',200,'Rfq Site','RFQ_SITE','','','','XXEIS_RS_ADMIN','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','Rfq Site','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','P_CARD_SITE',200,'P Card Site','P_CARD_SITE','','','','XXEIS_RS_ADMIN','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','P Card Site','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','ATTENTION_AR',200,'Attention Ar','ATTENTION_AR','','','','XXEIS_RS_ADMIN','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','Attention Ar','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','COUNTY',200,'Supplier site county','COUNTY','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_VENDOR_SITES_ALL','COUNTY','County','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','PROVINCE',200,'Province','PROVINCE','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_VENDOR_SITES_ALL','PROVINCE','Province','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','AREA_CODE',200,'Area code','AREA_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_VENDOR_SITES_ALL','AREA_CODE','Area Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','FAX_AREA_CODE',200,'Customer site ','FAX_AREA_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_VENDOR_SITES_ALL','FAX_AREA_CODE','Fax Area Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','SUPPLIER_NOTIF_METHOD',200,'The preferred Notification Method for the supplier','SUPPLIER_NOTIF_METHOD','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_VENDOR_SITES_ALL','SUPPLIER_NOTIF_METHOD','Supplier Notif Method','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','SHIP_TO',200,'Location name.','SHIP_TO','','','','XXEIS_RS_ADMIN','VARCHAR2','HR_LOCATIONS_ALL','LOCATION_CODE','Ship To','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','BILL_TO',200,'Location name.','BILL_TO','','','','XXEIS_RS_ADMIN','VARCHAR2','HR_LOCATIONS_ALL','LOCATION_CODE','Bill To','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','MATCH_OPTION',200,'Indicator of whether to match the invoices to the purchase order or receipt','MATCH_OPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_VENDOR_SITES_ALL','MATCH_OPTION','Match Option','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','COUNTRY_OF_ORIGIN',200,'Code for the country of manufacture','COUNTRY_OF_ORIGIN','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_VENDOR_SITES_ALL','COUNTRY_OF_ORIGIN_CODE','Country Of Origin','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','SHIP_VIA',200,'Ship Via','SHIP_VIA','','','','XXEIS_RS_ADMIN','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','Ship Via','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','FREIGHT_TERMS',200,'Freight Terms','FREIGHT_TERMS','','','','XXEIS_RS_ADMIN','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','Freight Terms','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','BILL_LOCATION_ID',200,'System-generated primary key column.','BILL_LOCATION_ID~BILL_LOCATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','HR_LOCATIONS_ALL','LOCATION_ID','Bill Location Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','SHIP_LOCATION_ID',200,'System-generated primary key column.','SHIP_LOCATION_ID~SHIP_LOCATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','HR_LOCATIONS_ALL','LOCATION_ID','Ship Location Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','QTY_RCV_TOLERANCE',200,'Quantity received tolerance percentage','QTY_RCV_TOLERANCE','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','PO_VENDORS','QTY_RCV_TOLERANCE','Qty Rcv Tolerance','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','QTY_RCV_EXCEPTION_CODE',200,'Enforces, warns, or ignores quantity receiving tolerance','QTY_RCV_EXCEPTION_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_VENDORS','QTY_RCV_EXCEPTION_CODE','Qty Rcv Exception Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','DAYS_EARLY_RECEIPT_ALLOWED',200,'Maximum acceptable number of days items can be received early','DAYS_EARLY_RECEIPT_ALLOWED','','','','XXEIS_RS_ADMIN','NUMBER','PO_VENDORS','DAYS_EARLY_RECEIPT_ALLOWED','Days Early Receipt Allowed','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','DAYS_LATE_RECEIPT_ALLOWED',200,'Maximum acceptable number of days items can be received late','DAYS_LATE_RECEIPT_ALLOWED','','','','XXEIS_RS_ADMIN','NUMBER','PO_VENDORS','DAYS_LATE_RECEIPT_ALLOWED','Days Late Receipt Allowed','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','ALLOW_SUBSTITUTES',200,'Allow Substitutes','ALLOW_SUBSTITUTES','','','','XXEIS_RS_ADMIN','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','Allow Substitutes','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','ALLOW_UNORDERED_RECEIPTS',200,'Allow Unordered Receipts','ALLOW_UNORDERED_RECEIPTS','','','','XXEIS_RS_ADMIN','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','Allow Unordered Receipts','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','AUTO_CALCULATE_INTEREST',200,'Auto Calculate Interest','AUTO_CALCULATE_INTEREST','','','','XXEIS_RS_ADMIN','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','Auto Calculate Interest','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','TERMS_DATE_BASIS',200,'Terms Date Basis','TERMS_DATE_BASIS','','','','XXEIS_RS_ADMIN','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','Terms Date Basis','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','PAY_DATE_BASIS',200,'Pay Date Basis','PAY_DATE_BASIS','','','','XXEIS_RS_ADMIN','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','Pay Date Basis','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','CREATE_DEBIT_MEMO',200,'Create Debit Memo','CREATE_DEBIT_MEMO','','','','XXEIS_RS_ADMIN','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','Create Debit Memo','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','INVOICE_AMOUNT_LIMIT',200,'Invoice Amount Limit','INVOICE_AMOUNT_LIMIT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','CALCULATION COLUMN','CALCULATION COLUMN','Invoice Amount Limit','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','ALWAYS_TAKE_DISCOUNT',200,'Always Take Discount','ALWAYS_TAKE_DISCOUNT','','','','XXEIS_RS_ADMIN','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','Always Take Discount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','INVOICE_CURRENCY',200,'Invoice Currency','INVOICE_CURRENCY','','','','XXEIS_RS_ADMIN','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','Invoice Currency','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','PAYMENT_CURRENCY',200,'Payment Currency','PAYMENT_CURRENCY','','','','XXEIS_RS_ADMIN','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','Payment Currency','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','HOLD_UNMATCHED_INVOICES',200,'Hold Unmatched Invoices','HOLD_UNMATCHED_INVOICES','','','','XXEIS_RS_ADMIN','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','Hold Unmatched Invoices','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','EXCLUSIVE_PAYMENT',200,'Exclusive Payment','EXCLUSIVE_PAYMENT','','','','XXEIS_RS_ADMIN','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','Exclusive Payment','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','EXCL_FREIGHT_FROM_DISC',200,'Excl Freight From Disc','EXCL_FREIGHT_FROM_DISC','','','','XXEIS_RS_ADMIN','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','Excl Freight From Disc','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','CONTACT_INACTIVE_ON',200,'Inactive date for record','CONTACT_INACTIVE_ON','','','','XXEIS_RS_ADMIN','DATE','PO_VENDOR_CONTACTS','INACTIVE_DATE','Contact Inactive On','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','CONTACT_MIDDLE_NAME',200,'Contact middle name','CONTACT_MIDDLE_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_VENDOR_CONTACTS','MIDDLE_NAME','Contact Middle Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','PREFIX',200,'Prefix','PREFIX','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_VENDOR_CONTACTS','PREFIX','Prefix','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','CONTACT_TITLE',200,'Title','CONTACT_TITLE','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_VENDOR_CONTACTS','TITLE','Contact Title','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','MAIL_STOP',200,'Contact mail stop','MAIL_STOP','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_VENDOR_CONTACTS','MAIL_STOP','Mail Stop','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','CONTACT_AREA_CODE',200,'Contact phone number area code','CONTACT_AREA_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_VENDOR_CONTACTS','AREA_CODE','Contact Area Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','SUPPLIER_SITE_CODE_ALT',200,'Alternate supplier site code for Kana Value','SUPPLIER_SITE_CODE_ALT','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_VENDOR_SITES_ALL','VENDOR_SITE_CODE_ALT','Supplier Site Code Alt','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','PRICE_TOLERANCE',200,'Not used','PRICE_TOLERANCE','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','PO_VENDORS','PRICE_TOLERANCE','Price Tolerance','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','FOB',200,'Fob','FOB','','','','XXEIS_RS_ADMIN','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','Fob','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','CREATED_BY',200,'Standard Who column','CREATED_BY','','','','XXEIS_RS_ADMIN','NUMBER','PO_VENDORS','CREATED_BY','Created By','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','LAST_UPDATED_BY',200,'Standard Who column','LAST_UPDATED_BY','','','','XXEIS_RS_ADMIN','NUMBER','PO_VENDORS','LAST_UPDATED_BY','Last Updated By','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','CUSTOMER_NUM',200,'Customer number with the supplier','CUSTOMER_NUM','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_VENDORS','CUSTOMER_NUM','Customer Num','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','WITHHOLDING_START_DATE',200,'Withholding start date','WITHHOLDING_START_DATE','','','','XXEIS_RS_ADMIN','DATE','PO_VENDORS','WITHHOLDING_START_DATE','Withholding Start Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','LANGUAGE',200,'Language','LANGUAGE~LANGUAGE','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_TERMS_TL','LANGUAGE','Language','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','LOCATION_ID',200,'System-generated primary key column.','LOCATION_ID~LOCATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','HR_LOCATIONS_ALL','LOCATION_ID','Location Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','ORGANIZATION_ID',200,'Organization Id','ORGANIZATION_ID~ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','HR_ALL_ORGANIZATION_UNITS','ORGANIZATION_ID','Organization Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','TERM_ID',200,'Term identifier','TERM_ID~TERM_ID','','','','XXEIS_RS_ADMIN','NUMBER','AP_TERMS_TL','TERM_ID','Term Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','USER_ID',200,'Application user identifier','USER_ID~USER_ID','','','','XXEIS_RS_ADMIN','NUMBER','FND_USER','USER_ID','User Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','POVS_VENDOR_SITE_ID',200,'Supplier site unique identifier','POVS_VENDOR_SITE_ID','','','','XXEIS_RS_ADMIN','NUMBER','AP_SUPPLIER_SITES_ALL','VENDOR_SITE_ID','Povs Vendor Site Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','ORG_ID',200,'Org Id','ORG_ID~ORG_ID','','','','XXEIS_RS_ADMIN','NUMBER','PO_VENDOR_SITES','ORG_ID','Org Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','APT1_TERM_ID',200,'Apt1 Term Id','APT1_TERM_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Apt1 Term Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','APT_TERM_ID',200,'Apt Term Id','APT_TERM_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Apt Term Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','CODE_COMBINATION_ID',200,'Code Combination Id','CODE_COMBINATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Code Combination Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','GL_SEGMENT2',200,'Gl Segment2','GL_SEGMENT2','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gl Segment2','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','LIABILITY_ACCOUNT',200,'Liability Account','LIABILITY_ACCOUNT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Liability Account','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','OFFSET_TAX',200,'Offset Tax','OFFSET_TAX','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Offset Tax','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','OPERATING_UNITS',200,'Operating Units','OPERATING_UNITS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Operating Units','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','PAYMENT_METHOD',200,'Payment Method','PAYMENT_METHOD','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Payment Method','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','PAYMENT_PRIORITY',200,'Payment Priority','PAYMENT_PRIORITY','','','','XXEIS_RS_ADMIN','NUMBER','','','Payment Priority','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','POV1_VENDOR_ID',200,'Pov1 Vendor Id','POV1_VENDOR_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Pov1 Vendor Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','REMITTANCE_EMAIL',200,'Remittance Email','REMITTANCE_EMAIL','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Remittance Email','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','USR1_USER_ID',200,'Usr1 User Id','USR1_USER_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Usr1 User Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','USR2_USER_ID',200,'Usr2 User Id','USR2_USER_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Usr2 User Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','USR3_USER_ID',200,'Usr3 User Id','USR3_USER_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Usr3 User Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','USR_USER_ID',200,'Usr User Id','USR_USER_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Usr User Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','VAT_CODE',200,'Vat Code','VAT_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vat Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','VENDOR_CONTACT_ID',200,'Vendor Contact Id','VENDOR_CONTACT_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Vendor Contact Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','COPYRIGHT',200,'Copyright','COPYRIGHT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Copyright','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','GCC#Branch',200,'Descriptive flexfield (DFF): GL Accounts Column Name: Branch','GCC#Branch','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','ATTRIBUTE1','Gcc#Branch','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','HRL#163#Property_ID',200,'Descriptive flexfield (DFF): Additional Location Details Column Name: Property ID Context: 163','HRL#163#Property_ID','','','','XXEIS_RS_ADMIN','VARCHAR2','HR_LOCATIONS_ALL','ATTRIBUTE1','Hrl#163#Property Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','HRL1#163#Property_ID',200,'Descriptive flexfield (DFF): Additional Location Details Column Name: Property ID Context: 163','HRL1#163#Property_ID','','','','XXEIS_RS_ADMIN','VARCHAR2','HR_LOCATIONS_ALL','ATTRIBUTE1','Hrl1#163#Property Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','HRL1#163#Location_ID',200,'Descriptive flexfield (DFF): Additional Location Details Column Name: Location ID Context: 163','HRL1#163#Location_ID','','','','XXEIS_RS_ADMIN','VARCHAR2','HR_LOCATIONS_ALL','ATTRIBUTE2','Hrl1#163#Location Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','HRL#163#Location_ID',200,'Descriptive flexfield (DFF): Additional Location Details Column Name: Location ID Context: 163','HRL#163#Location_ID','','','','XXEIS_RS_ADMIN','VARCHAR2','HR_LOCATIONS_ALL','ATTRIBUTE2','Hrl#163#Location Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','HRL1#163#FRU',200,'Descriptive flexfield (DFF): Additional Location Details Column Name: FRU Context: 163','HRL1#163#FRU','','','','XXEIS_RS_ADMIN','VARCHAR2','HR_LOCATIONS_ALL','ATTRIBUTE3','Hrl1#163#Fru','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','HRL#163#FRU',200,'Descriptive flexfield (DFF): Additional Location Details Column Name: FRU Context: 163','HRL#163#FRU','','','','XXEIS_RS_ADMIN','VARCHAR2','HR_LOCATIONS_ALL','ATTRIBUTE3','Hrl#163#Fru','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','HRL1#163#Location_Start',200,'Descriptive flexfield (DFF): Additional Location Details Column Name: Location Start Context: 163','HRL1#163#Location_Start','','','','XXEIS_RS_ADMIN','VARCHAR2','HR_LOCATIONS_ALL','ATTRIBUTE4','Hrl1#163#Location Start','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','HRL#163#Location_Start',200,'Descriptive flexfield (DFF): Additional Location Details Column Name: Location Start Context: 163','HRL#163#Location_Start','','','','XXEIS_RS_ADMIN','VARCHAR2','HR_LOCATIONS_ALL','ATTRIBUTE4','Hrl#163#Location Start','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','HRL#163#Location_End',200,'Descriptive flexfield (DFF): Additional Location Details Column Name: Location End Context: 163','HRL#163#Location_End','','','','XXEIS_RS_ADMIN','VARCHAR2','HR_LOCATIONS_ALL','ATTRIBUTE5','Hrl#163#Location End','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','HRL1#163#Location_End',200,'Descriptive flexfield (DFF): Additional Location Details Column Name: Location End Context: 163','HRL1#163#Location_End','','','','XXEIS_RS_ADMIN','VARCHAR2','HR_LOCATIONS_ALL','ATTRIBUTE5','Hrl1#163#Location End','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','POV#WC_PRISM_Vendor_No',200,'Descriptive flexfield (DFF): Vendors Column Name: WC PRISM Vendor No','POV#WC_PRISM_Vendor_No','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_VENDORS','ATTRIBUTE1','Pov#Wc Prism Vendor No','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','POV#GSC_R11_Vendor_ID',200,'Descriptive flexfield (DFF): Vendors Column Name: GSC R11 Vendor ID','POV#GSC_R11_Vendor_ID','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_VENDORS','ATTRIBUTE15','Pov#Gsc R11 Vendor Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','POV#GSC_Tax_Exempt_Flag',200,'Descriptive flexfield (DFF): Vendors Column Name: GSC Tax Exempt Flag','POV#GSC_Tax_Exempt_Flag','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_VENDORS','ATTRIBUTE2','Pov#Gsc Tax Exempt Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','POV#Supplier_Status',200,'Descriptive flexfield (DFF): Vendors Column Name: Supplier Status','POV#Supplier_Status','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_VENDORS','ATTRIBUTE3','Pov#Supplier Status','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','POV#Category_SBA_Owner',200,'Descriptive flexfield (DFF): Vendors Column Name: Category SBA Owner','POV#Category_SBA_Owner','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_VENDORS','ATTRIBUTE4','Pov#Category Sba Owner','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','GCC#50328#ACCOUNT',200,'Accounting Flexfield (KFF): Segment ''Account'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#ACCOUNT','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT4','GCC#Account','50328','1014550','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','GCC#50328#ACCOUNT#DESCR',200,'Accounting Flexfield (KFF) : Segment ''Account'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#ACCOUNT#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT4','GCC#Account Descr','50328','1014550','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','GCC#50328#COST_CENTER',200,'Accounting Flexfield (KFF): Segment ''Cost Center'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#COST_CENTER','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT3','GCC#Cost Center','50328','1014549','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','GCC#50328#COST_CENTER#DESCR',200,'Accounting Flexfield (KFF) : Segment ''Cost Center'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#COST_CENTER#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT3','GCC#Cost Center Descr','50328','1014549','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','GCC#50328#FUTURE_USE',200,'Accounting Flexfield (KFF): Segment ''Future Use'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#FUTURE_USE','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT6','GCC#Future Use','50328','1014552','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','GCC#50328#FUTURE_USE#DESCR',200,'Accounting Flexfield (KFF) : Segment ''Future Use'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#FUTURE_USE#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT6','GCC#Future Use Descr','50328','1014552','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','GCC#50328#FUTURE_USE_2',200,'Accounting Flexfield (KFF): Segment ''Future Use 2'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#FUTURE_USE_2','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT7','GCC#Future Use 2','50328','1014948','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','GCC#50328#FUTURE_USE_2#DESCR',200,'Accounting Flexfield (KFF) : Segment ''Future Use 2'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#FUTURE_USE_2#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT7','GCC#Future Use 2 Descr','50328','1014948','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','GCC#50328#LOCATION',200,'Accounting Flexfield (KFF): Segment ''Location'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT2','GCC#Location','50328','1014548','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','GCC#50328#LOCATION#DESCR',200,'Accounting Flexfield (KFF) : Segment ''Location'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#LOCATION#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT2','GCC#Location Descr','50328','1014548','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','GCC#50328#PRODUCT',200,'Accounting Flexfield (KFF): Segment ''Product'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#PRODUCT','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT1','GCC#Product','50328','1014547','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','GCC#50328#PRODUCT#DESCR',200,'Accounting Flexfield (KFF) : Segment ''Product'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#PRODUCT#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT1','GCC#Product Descr','50328','1014547','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','GCC#50328#PROJECT_CODE',200,'Accounting Flexfield (KFF): Segment ''Project Code'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#PROJECT_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT5','GCC#Project Code','50328','1014551','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','GCC#50328#PROJECT_CODE#DESCR',200,'Accounting Flexfield (KFF) : Segment ''Project Code'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#PROJECT_CODE#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT5','GCC#Project Code Descr','50328','1014551','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','GCC#50368#ACCOUNT',200,'Accounting Flexfield (KFF): Segment ''Account'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GCC#50368#ACCOUNT','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT4','GCC#Account','50368','1014599','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','GCC#50368#ACCOUNT#DESCR',200,'Accounting Flexfield (KFF) : Segment ''Account'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GCC#50368#ACCOUNT#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT4','GCC#Account Descr','50368','1014599','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','GCC#50368#DEPARTMENT',200,'Accounting Flexfield (KFF): Segment ''Department'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GCC#50368#DEPARTMENT','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT3','GCC#Department','50368','1014598','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','GCC#50368#DEPARTMENT#DESCR',200,'Accounting Flexfield (KFF) : Segment ''Department'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GCC#50368#DEPARTMENT#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT3','GCC#Department Descr','50368','1014598','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','GCC#50368#DIVISION',200,'Accounting Flexfield (KFF): Segment ''Division'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GCC#50368#DIVISION','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT2','GCC#Division','50368','1014597','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','GCC#50368#DIVISION#DESCR',200,'Accounting Flexfield (KFF) : Segment ''Division'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GCC#50368#DIVISION#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT2','GCC#Division Descr','50368','1014597','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','GCC#50368#FUTURE_USE',200,'Accounting Flexfield (KFF): Segment ''Future Use'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GCC#50368#FUTURE_USE','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT6','GCC#Future Use','50368','1014601','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','GCC#50368#FUTURE_USE#DESCR',200,'Accounting Flexfield (KFF) : Segment ''Future Use'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GCC#50368#FUTURE_USE#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT6','GCC#Future Use Descr','50368','1014601','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','GCC#50368#PRODUCT',200,'Accounting Flexfield (KFF): Segment ''Product'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GCC#50368#PRODUCT','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT1','GCC#Product','50368','1014596','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','GCC#50368#PRODUCT#DESCR',200,'Accounting Flexfield (KFF) : Segment ''Product'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GCC#50368#PRODUCT#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT1','GCC#Product Descr','50368','1014596','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','GCC#50368#SUBACCOUNT',200,'Accounting Flexfield (KFF): Segment ''SubAccount'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GCC#50368#SUBACCOUNT','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT5','GCC#SubAccount','50368','1014600','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_SUPPLIERS_DET_V','GCC#50368#SUBACCOUNT#DESCR',200,'Accounting Flexfield (KFF) : Segment ''SubAccount'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GCC#50368#SUBACCOUNT#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT5','GCC#SubAccount Descr','50368','1014600','','US');
--Inserting Object Components for EIS_AP_SUPPLIERS_DET_V
xxeis.eis_rsc_ins.vcomp( 'EIS_AP_SUPPLIERS_DET_V','PO_VENDOR_SITES',200,'AP_SUPPLIER_SITES_ALL','POVS','POVS','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Supplier Sites','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_AP_SUPPLIERS_DET_V','FND_USER',200,'FND_USER','USR3','USR3','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Sites Updated By','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_AP_SUPPLIERS_DET_V','AP_SUPPLIERS',200,'PO_VENDORS','POV','POV','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Vendors','Y','This view shows details of supplier setups including credit limit information.','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_AP_SUPPLIERS_DET_V','FND_USER',200,'FND_USER','USR','USR','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Suppliers Created By','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_AP_SUPPLIERS_DET_V','FND_USER',200,'FND_USER','USR1','USR1','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Suppliers Updated By','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_AP_SUPPLIERS_DET_V','AP_SUPPLIERS',200,'AP_SUPPLIERS','POV1','POV1','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Suppliers','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_AP_SUPPLIERS_DET_V','HR_LOCATIONS',200,'HR_LOCATIONS_ALL','HRL','HRL','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Ship-To Location','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_AP_SUPPLIERS_DET_V','HR_LOCATIONS',200,'HR_LOCATIONS_ALL','HRL1','HRL1','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Bill-To Location','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_AP_SUPPLIERS_DET_V','FND_USER',200,'FND_USER','USR2','USR2','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Sites Created By','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_AP_SUPPLIERS_DET_V','HR_ORGANIZATION_UNITS',200,'HR_ALL_ORGANIZATION_UNITS','HOU','HOU','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','HR_ALL_ORGANIZATION_UNITS','N','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_AP_SUPPLIERS_DET_V','GL_CODE_COMBINATIONS_KFV',200,'GL_CODE_COMBINATIONS','GCC','GCC','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Liability Account','','','','','','Y',' ',' ',' ',' ',' ');
--Inserting Object Component Joins for EIS_AP_SUPPLIERS_DET_V
xxeis.eis_rsc_ins.vcj( 'EIS_AP_SUPPLIERS_DET_V','HR_ORGANIZATION_UNITS','HOU',200,'EAST.ORGANIZATION_ID','=','HOU.ORGANIZATION_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_AP_SUPPLIERS_DET_V','PO_VENDOR_SITES','POVS',200,'EAST.povs_vendor_site_id','=','povs.vendor_site_id(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_AP_SUPPLIERS_DET_V','FND_USER','USR3',200,'EAST.USER_ID','=','USR3.USER_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_AP_SUPPLIERS_DET_V','AP_SUPPLIERS','POV',200,'EAST.VENDOR_ID','=','POV.VENDOR_ID(+)','POV.','','1','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_AP_SUPPLIERS_DET_V','FND_USER','USR',200,'EAST.USER_ID','=','USR.USER_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_AP_SUPPLIERS_DET_V','FND_USER','USR1',200,'EAST.USER_ID','=','USR1.USER_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_AP_SUPPLIERS_DET_V','AP_SUPPLIERS','POV1',200,'EAST.VENDOR_ID','=','POV1.VENDOR_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_AP_SUPPLIERS_DET_V','HR_LOCATIONS','HRL',200,'EAST.LOCATION_ID','=','HRL.LOCATION_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_AP_SUPPLIERS_DET_V','HR_LOCATIONS','HRL1',200,'EAST.LOCATION_ID','=','HRL1.LOCATION_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_AP_SUPPLIERS_DET_V','FND_USER','USR2',200,'EAST.USER_ID','=','USR2.USER_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_AP_SUPPLIERS_DET_V','GL_CODE_COMBINATIONS_KFV','GCC',200,'EAST.CODE_COMBINATION_ID','=','GCC.CODE_COMBINATION_ID(+)','','','','Y','XXEIS_RS_ADMIN');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
--Exporting View Component Data of the View -  EIS_AP_SUPPLIERS_DET_V
prompt Creating Object Data AP_SUPPLIERS
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Inserting Object AP_SUPPLIERS
xxeis.eis_rsc_ins.v( 'AP_SUPPLIERS',200,'AP_SUPPLIERS stores information about your supplier level attributes.','1.0','','','ANONYMOUS','APPS','Ap Suppliers','AS','','','SYNONYM','US','','');
--Delete Object Columns for AP_SUPPLIERS
xxeis.eis_rsc_utility.delete_view_rows('AP_SUPPLIERS',200,FALSE);
--Inserting Object Columns for AP_SUPPLIERS
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','INDIVIDUAL_1099',200,'','INDIVIDUAL_1099','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','INDIVIDUAL_1099','Individual 1099','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','PAYMENT_METHOD_LOOKUP_CODE',200,'Default payment method type','PAYMENT_METHOD_LOOKUP_CODE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','PAYMENT_METHOD_LOOKUP_CODE','Payment Method Lookup Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','ORGANIZATION_TYPE_LOOKUP_CODE',200,'IRS organization type','ORGANIZATION_TYPE_LOOKUP_CODE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','ORGANIZATION_TYPE_LOOKUP_CODE','Organization Type Lookup Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','SEGMENT1',200,'Supplier number','SEGMENT1','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','SEGMENT1','Segment1','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','VENDOR_NAME',200,'Supplier name','VENDOR_NAME','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','VENDOR_NAME','Vendor Name','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','ATTRIBUTE3',200,'Descriptive flexfield segment','ATTRIBUTE3','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','ATTRIBUTE3','Attribute3','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','ATTRIBUTE2',200,'Descriptive flexfield segment','ATTRIBUTE2','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','ATTRIBUTE2','Attribute2','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','ATTRIBUTE1',200,'Descriptive flexfield segment','ATTRIBUTE1','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','ATTRIBUTE1','Attribute1','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','CIS_ENABLED_FLAG',200,'','CIS_ENABLED_FLAG','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','CIS_ENABLED_FLAG','Cis Enabled Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','PARTNERSHIP_NAME',200,'','PARTNERSHIP_NAME','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','PARTNERSHIP_NAME','Partnership Name','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','PARTNERSHIP_UTR',200,'','PARTNERSHIP_UTR','','','','ANONYMOUS','NUMBER','AP_SUPPLIERS','PARTNERSHIP_UTR','Partnership Utr','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','UNIQUE_TAX_REFERENCE_NUM',200,'','UNIQUE_TAX_REFERENCE_NUM','','','','ANONYMOUS','NUMBER','AP_SUPPLIERS','UNIQUE_TAX_REFERENCE_NUM','Unique Tax Reference Num','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','BUS_CLASS_LAST_CERTIFIED_BY',200,'Last Certified By','BUS_CLASS_LAST_CERTIFIED_BY','','','','ANONYMOUS','NUMBER','AP_SUPPLIERS','BUS_CLASS_LAST_CERTIFIED_BY','Bus Class Last Certified By','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','CIS_VERIFICATION_DATE',200,'','CIS_VERIFICATION_DATE','','','','ANONYMOUS','DATE','AP_SUPPLIERS','CIS_VERIFICATION_DATE','Cis Verification Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','MATCH_STATUS_FLAG',200,'','MATCH_STATUS_FLAG','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','MATCH_STATUS_FLAG','Match Status Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','VERIFICATION_REQUEST_ID',200,'','VERIFICATION_REQUEST_ID','','','','ANONYMOUS','NUMBER','AP_SUPPLIERS','VERIFICATION_REQUEST_ID','Verification Request Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','VERIFICATION_NUMBER',200,'','VERIFICATION_NUMBER','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','VERIFICATION_NUMBER','Verification Number','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','NATIONAL_INSURANCE_NUMBER',200,'','NATIONAL_INSURANCE_NUMBER','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','NATIONAL_INSURANCE_NUMBER','National Insurance Number','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','BUS_CLASS_LAST_CERTIFIED_DATE',200,'Last Certified Date','BUS_CLASS_LAST_CERTIFIED_DATE','','','','ANONYMOUS','DATE','AP_SUPPLIERS','BUS_CLASS_LAST_CERTIFIED_DATE','Bus Class Last Certified Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','COMPANY_REGISTRATION_NUMBER',200,'','COMPANY_REGISTRATION_NUMBER','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','COMPANY_REGISTRATION_NUMBER','Company Registration Number','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','WORK_REFERENCE',200,'','WORK_REFERENCE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','WORK_REFERENCE','Work Reference','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','TRADING_NAME',200,'','TRADING_NAME','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','TRADING_NAME','Trading Name','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','SALUTATION',200,'','SALUTATION','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','SALUTATION','Salutation','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','LAST_NAME',200,'','LAST_NAME','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','LAST_NAME','Last Name','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','SECOND_NAME',200,'','SECOND_NAME','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','SECOND_NAME','Second Name','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','CIS_PARENT_VENDOR_ID',200,'CIS Parent Vendor ID','CIS_PARENT_VENDOR_ID','','','','ANONYMOUS','NUMBER','AP_SUPPLIERS','CIS_PARENT_VENDOR_ID','Cis Parent Vendor Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','PAY_AWT_GROUP_ID',200,'','PAY_AWT_GROUP_ID','','','','ANONYMOUS','NUMBER','AP_SUPPLIERS','PAY_AWT_GROUP_ID','Pay Awt Group Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','FIRST_NAME',200,'','FIRST_NAME','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','FIRST_NAME','First Name','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','BANK_ACCOUNT_NUM',200,'No longer used','BANK_ACCOUNT_NUM','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','BANK_ACCOUNT_NUM','Bank Account Num','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','BANK_ACCOUNT_NAME',200,'No longer used','BANK_ACCOUNT_NAME','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','BANK_ACCOUNT_NAME','Bank Account Name','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','MINORITY_GROUP_LOOKUP_CODE',200,'Type of minority-owned business','MINORITY_GROUP_LOOKUP_CODE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','MINORITY_GROUP_LOOKUP_CODE','Minority Group Lookup Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','END_DATE_ACTIVE',200,'Key flexfield end date','END_DATE_ACTIVE','','','','ANONYMOUS','DATE','AP_SUPPLIERS','END_DATE_ACTIVE','End Date Active','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','START_DATE_ACTIVE',200,'Key flexfield start date','START_DATE_ACTIVE','','','','ANONYMOUS','DATE','AP_SUPPLIERS','START_DATE_ACTIVE','Start Date Active','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','VAT_CODE',200,'VAT code','VAT_CODE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','VAT_CODE','Vat Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','WITHHOLDING_START_DATE',200,'Withholding start date','WITHHOLDING_START_DATE','','','','ANONYMOUS','DATE','AP_SUPPLIERS','WITHHOLDING_START_DATE','Withholding Start Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','WITHHOLDING_STATUS_LOOKUP_CODE',200,'Withholding status type','WITHHOLDING_STATUS_LOOKUP_CODE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','WITHHOLDING_STATUS_LOOKUP_CODE','Withholding Status Lookup Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','TYPE_1099',200,'Type of 1099','TYPE_1099','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','TYPE_1099','Type 1099','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','NUM_1099',200,'Tax identification number','NUM_1099','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','NUM_1099','Num 1099','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','PREPAY_CODE_COMBINATION_ID',200,'Unique identifier for the general ledger account for prepayment','PREPAY_CODE_COMBINATION_ID','','','','ANONYMOUS','NUMBER','AP_SUPPLIERS','PREPAY_CODE_COMBINATION_ID','Prepay Code Combination Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','EXPENSE_CODE_COMBINATION_ID',200,'Not used','EXPENSE_CODE_COMBINATION_ID','','','','ANONYMOUS','NUMBER','AP_SUPPLIERS','EXPENSE_CODE_COMBINATION_ID','Expense Code Combination Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','DISC_TAKEN_CODE_COMBINATION_ID',200,'No longer used','DISC_TAKEN_CODE_COMBINATION_ID','','','','ANONYMOUS','NUMBER','AP_SUPPLIERS','DISC_TAKEN_CODE_COMBINATION_ID','Disc Taken Code Combination Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','DISC_LOST_CODE_COMBINATION_ID',200,'No longer used','DISC_LOST_CODE_COMBINATION_ID','','','','ANONYMOUS','NUMBER','AP_SUPPLIERS','DISC_LOST_CODE_COMBINATION_ID','Disc Lost Code Combination Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','ACCTS_PAY_CODE_COMBINATION_ID',200,'Unique identifier for the supplier liability account','ACCTS_PAY_CODE_COMBINATION_ID','','~T~D~2','','ANONYMOUS','NUMBER','AP_SUPPLIERS','ACCTS_PAY_CODE_COMBINATION_ID','Accts Pay Code Combination Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','DISTRIBUTION_SET_ID',200,'Distribution set unique identifier','DISTRIBUTION_SET_ID','','','','ANONYMOUS','NUMBER','AP_SUPPLIERS','DISTRIBUTION_SET_ID','Distribution Set Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','HOLD_REASON',200,'Reason for placing the supplier on payment hold','HOLD_REASON','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','HOLD_REASON','Hold Reason','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','HOLD_FUTURE_PAYMENTS_FLAG',200,'Indicates whether Oracle Payables should place upapproved payments for this supplier on hold or not','HOLD_FUTURE_PAYMENTS_FLAG','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','HOLD_FUTURE_PAYMENTS_FLAG','Hold Future Payments Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','HOLD_ALL_PAYMENTS_FLAG',200,'Indicates whether Oracle Payables should place all payments for this supplier on hold or not','HOLD_ALL_PAYMENTS_FLAG','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','HOLD_ALL_PAYMENTS_FLAG','Hold All Payments Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','EXCHANGE_DATE_LOOKUP_CODE',200,'No longer used','EXCHANGE_DATE_LOOKUP_CODE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','EXCHANGE_DATE_LOOKUP_CODE','Exchange Date Lookup Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','INVOICE_AMOUNT_LIMIT',200,'Maximum amount per invoice','INVOICE_AMOUNT_LIMIT','','~T~D~2','','ANONYMOUS','NUMBER','AP_SUPPLIERS','INVOICE_AMOUNT_LIMIT','Invoice Amount Limit','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','PAYMENT_CURRENCY_CODE',200,'Default payment currency unique identifier','PAYMENT_CURRENCY_CODE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','PAYMENT_CURRENCY_CODE','Payment Currency Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','INVOICE_CURRENCY_CODE',200,'Default currency unique identifier','INVOICE_CURRENCY_CODE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','INVOICE_CURRENCY_CODE','Invoice Currency Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','PAYMENT_PRIORITY',200,'Payment priority','PAYMENT_PRIORITY','','','','ANONYMOUS','NUMBER','AP_SUPPLIERS','PAYMENT_PRIORITY','Payment Priority','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','PAY_GROUP_LOOKUP_CODE',200,'Payment group type','PAY_GROUP_LOOKUP_CODE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','PAY_GROUP_LOOKUP_CODE','Pay Group Lookup Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','PAY_DATE_BASIS_LOOKUP_CODE',200,'Type of payment date basis','PAY_DATE_BASIS_LOOKUP_CODE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','PAY_DATE_BASIS_LOOKUP_CODE','Pay Date Basis Lookup Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','ALWAYS_TAKE_DISC_FLAG',200,'Indicator of whether Oracle Payables should always take a discount for the supplier','ALWAYS_TAKE_DISC_FLAG','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','ALWAYS_TAKE_DISC_FLAG','Always Take Disc Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','CREDIT_LIMIT',200,'Not used','CREDIT_LIMIT','','~T~D~2','','ANONYMOUS','NUMBER','AP_SUPPLIERS','CREDIT_LIMIT','Credit Limit','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','CREDIT_STATUS_LOOKUP_CODE',200,'No longer used','CREDIT_STATUS_LOOKUP_CODE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','CREDIT_STATUS_LOOKUP_CODE','Credit Status Lookup Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','SET_OF_BOOKS_ID',200,'Set of Books unique identifier','SET_OF_BOOKS_ID','','','','ANONYMOUS','NUMBER','AP_SUPPLIERS','SET_OF_BOOKS_ID','Set Of Books Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','TERMS_ID',200,'Payment terms unique identifier','TERMS_ID','','','','ANONYMOUS','NUMBER','AP_SUPPLIERS','TERMS_ID','Terms Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','FOB_LOOKUP_CODE',200,'Default free-on-board type','FOB_LOOKUP_CODE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','FOB_LOOKUP_CODE','Fob Lookup Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','FREIGHT_TERMS_LOOKUP_CODE',200,'Default freight terms type','FREIGHT_TERMS_LOOKUP_CODE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','FREIGHT_TERMS_LOOKUP_CODE','Freight Terms Lookup Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','SHIP_VIA_LOOKUP_CODE',200,'Default carrier type','SHIP_VIA_LOOKUP_CODE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','SHIP_VIA_LOOKUP_CODE','Ship Via Lookup Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','BILL_TO_LOCATION_ID',200,'Default bill-to location unique identifier','BILL_TO_LOCATION_ID','','','','ANONYMOUS','NUMBER','AP_SUPPLIERS','BILL_TO_LOCATION_ID','Bill To Location Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','SHIP_TO_LOCATION_ID',200,'Default ship-to location unique identifier','SHIP_TO_LOCATION_ID','','','','ANONYMOUS','NUMBER','AP_SUPPLIERS','SHIP_TO_LOCATION_ID','Ship To Location Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','MIN_ORDER_AMOUNT',200,'Minimum purchase order amount','MIN_ORDER_AMOUNT','','~T~D~2','','ANONYMOUS','NUMBER','AP_SUPPLIERS','MIN_ORDER_AMOUNT','Min Order Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','PARENT_VENDOR_ID',200,'Unique identifier of the parent supplier','PARENT_VENDOR_ID','','','','ANONYMOUS','NUMBER','AP_SUPPLIERS','PARENT_VENDOR_ID','Parent Vendor Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','ONE_TIME_FLAG',200,'Indicates whether the supplier is a one-time supplier','ONE_TIME_FLAG','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','ONE_TIME_FLAG','One Time Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','CUSTOMER_NUM',200,'Customer number with the supplier','CUSTOMER_NUM','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','CUSTOMER_NUM','Customer Num','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','VENDOR_TYPE_LOOKUP_CODE',200,'Supplier type','VENDOR_TYPE_LOOKUP_CODE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','VENDOR_TYPE_LOOKUP_CODE','Vendor Type Lookup Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','EMPLOYEE_ID',200,'Employee unique identifier if the supplier is an employee','EMPLOYEE_ID','','','','ANONYMOUS','NUMBER','AP_SUPPLIERS','EMPLOYEE_ID','Employee Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','CREATED_BY',200,'Standard Who column','CREATED_BY','','','','ANONYMOUS','NUMBER','AP_SUPPLIERS','CREATED_BY','Created By','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','CREATION_DATE',200,'Standard Who column','CREATION_DATE','','','','ANONYMOUS','DATE','AP_SUPPLIERS','CREATION_DATE','Creation Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','LAST_UPDATE_LOGIN',200,'Standard Who column','LAST_UPDATE_LOGIN','','','','ANONYMOUS','NUMBER','AP_SUPPLIERS','LAST_UPDATE_LOGIN','Last Update Login','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','SEGMENT5',200,'Key flexfield summary flag','SEGMENT5','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','SEGMENT5','Segment5','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','SEGMENT4',200,'Key flexfield summary flag','SEGMENT4','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','SEGMENT4','Segment4','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','SEGMENT3',200,'Key flexfield summary flag','SEGMENT3','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','SEGMENT3','Segment3','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','SEGMENT2',200,'Key flexfield summary flag','SEGMENT2','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','SEGMENT2','Segment2','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','ENABLED_FLAG',200,'Key flexfield summary flag','ENABLED_FLAG','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','ENABLED_FLAG','Enabled Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','SUMMARY_FLAG',200,'Key flexfield summary flag','SUMMARY_FLAG','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','SUMMARY_FLAG','Summary Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','VENDOR_NAME_ALT',200,'Alternate supplier name for kana value','VENDOR_NAME_ALT','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','VENDOR_NAME_ALT','Vendor Name Alt','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','LAST_UPDATED_BY',200,'Standard Who column','LAST_UPDATED_BY','','','','ANONYMOUS','NUMBER','AP_SUPPLIERS','LAST_UPDATED_BY','Last Updated By','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','LAST_UPDATE_DATE',200,'Standard Who column','LAST_UPDATE_DATE','','','','ANONYMOUS','DATE','AP_SUPPLIERS','LAST_UPDATE_DATE','Last Update Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','VENDOR_ID',200,'Supplier unique identifier','VENDOR_ID','','','','ANONYMOUS','NUMBER','AP_SUPPLIERS','VENDOR_ID','Vendor Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','GLOBAL_ATTRIBUTE14',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE14','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','GLOBAL_ATTRIBUTE14','Global Attribute14','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','GLOBAL_ATTRIBUTE13',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE13','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','GLOBAL_ATTRIBUTE13','Global Attribute13','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','GLOBAL_ATTRIBUTE12',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE12','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','GLOBAL_ATTRIBUTE12','Global Attribute12','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','GLOBAL_ATTRIBUTE11',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE11','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','GLOBAL_ATTRIBUTE11','Global Attribute11','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','GLOBAL_ATTRIBUTE10',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE10','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','GLOBAL_ATTRIBUTE10','Global Attribute10','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','GLOBAL_ATTRIBUTE9',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE9','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','GLOBAL_ATTRIBUTE9','Global Attribute9','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','GLOBAL_ATTRIBUTE8',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE8','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','GLOBAL_ATTRIBUTE8','Global Attribute8','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','GLOBAL_ATTRIBUTE7',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE7','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','GLOBAL_ATTRIBUTE7','Global Attribute7','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','GLOBAL_ATTRIBUTE6',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE6','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','GLOBAL_ATTRIBUTE6','Global Attribute6','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','GLOBAL_ATTRIBUTE5',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE5','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','GLOBAL_ATTRIBUTE5','Global Attribute5','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','GLOBAL_ATTRIBUTE4',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE4','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','GLOBAL_ATTRIBUTE4','Global Attribute4','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','GLOBAL_ATTRIBUTE3',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE3','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','GLOBAL_ATTRIBUTE3','Global Attribute3','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','GLOBAL_ATTRIBUTE2',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE2','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','GLOBAL_ATTRIBUTE2','Global Attribute2','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','GLOBAL_ATTRIBUTE1',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE1','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','GLOBAL_ATTRIBUTE1','Global Attribute1','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','AWT_GROUP_ID',200,'Unique identifier for the withholding tax group','AWT_GROUP_ID','','','','ANONYMOUS','NUMBER','AP_SUPPLIERS','AWT_GROUP_ID','Awt Group Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','ALLOW_AWT_FLAG',200,'Indicator of whether Allow Withholding Tax is enabled','ALLOW_AWT_FLAG','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','ALLOW_AWT_FLAG','Allow Awt Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','BANK_NUMBER',200,'No longer used','BANK_NUMBER','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','BANK_NUMBER','Bank Number','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','CHECK_DIGITS',200,'Check number used by Payables','CHECK_DIGITS','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','CHECK_DIGITS','Check Digits','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','TAX_REPORTING_NAME',200,'Tax reporting method name','TAX_REPORTING_NAME','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','TAX_REPORTING_NAME','Tax Reporting Name','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','EXCLUDE_FREIGHT_FROM_DISCOUNT',200,'Exclude supplier freight from discount amount','EXCLUDE_FREIGHT_FROM_DISCOUNT','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','EXCLUDE_FREIGHT_FROM_DISCOUNT','Exclude Freight From Discount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','VALIDATION_NUMBER',200,'Validation number','VALIDATION_NUMBER','','','','ANONYMOUS','NUMBER','AP_SUPPLIERS','VALIDATION_NUMBER','Validation Number','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','AUTO_CALCULATE_INTEREST_FLAG',200,'Indicates whether interest is to be automatically calculated','AUTO_CALCULATE_INTEREST_FLAG','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','AUTO_CALCULATE_INTEREST_FLAG','Auto Calculate Interest Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','VAT_REGISTRATION_NUM',200,'VAT registration number','VAT_REGISTRATION_NUM','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','VAT_REGISTRATION_NUM','Vat Registration Num','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','OFFSET_VAT_CODE',200,'No longer used','OFFSET_VAT_CODE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','OFFSET_VAT_CODE','Offset Vat Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','PROGRAM_UPDATE_DATE',200,'Standard Who column','PROGRAM_UPDATE_DATE','','','','ANONYMOUS','DATE','AP_SUPPLIERS','PROGRAM_UPDATE_DATE','Program Update Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','PROGRAM_ID',200,'Standard Who column','PROGRAM_ID','','','','ANONYMOUS','NUMBER','AP_SUPPLIERS','PROGRAM_ID','Program Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','PROGRAM_APPLICATION_ID',200,'Standard Who column','PROGRAM_APPLICATION_ID','','','','ANONYMOUS','NUMBER','AP_SUPPLIERS','PROGRAM_APPLICATION_ID','Program Application Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','REQUEST_ID',200,'Standard Who column','REQUEST_ID','','','','ANONYMOUS','NUMBER','AP_SUPPLIERS','REQUEST_ID','Request Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','ATTRIBUTE15',200,'Descriptive flexfield segment','ATTRIBUTE15','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','ATTRIBUTE15','Attribute15','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','ATTRIBUTE14',200,'Descriptive flexfield segment','ATTRIBUTE14','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','ATTRIBUTE14','Attribute14','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','ATTRIBUTE13',200,'Descriptive flexfield segment','ATTRIBUTE13','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','ATTRIBUTE13','Attribute13','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','ATTRIBUTE12',200,'Descriptive flexfield segment','ATTRIBUTE12','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','ATTRIBUTE12','Attribute12','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','ATTRIBUTE11',200,'Descriptive flexfield segment','ATTRIBUTE11','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','ATTRIBUTE11','Attribute11','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','ATTRIBUTE10',200,'Descriptive flexfield segment','ATTRIBUTE10','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','ATTRIBUTE10','Attribute10','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','ATTRIBUTE9',200,'Descriptive flexfield segment','ATTRIBUTE9','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','ATTRIBUTE9','Attribute9','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','ATTRIBUTE8',200,'Descriptive flexfield segment','ATTRIBUTE8','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','ATTRIBUTE8','Attribute8','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','ATTRIBUTE7',200,'Descriptive flexfield segment','ATTRIBUTE7','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','ATTRIBUTE7','Attribute7','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','ATTRIBUTE6',200,'Descriptive flexfield segment','ATTRIBUTE6','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','ATTRIBUTE6','Attribute6','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','ATTRIBUTE5',200,'Descriptive flexfield segment','ATTRIBUTE5','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','ATTRIBUTE5','Attribute5','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','ATTRIBUTE4',200,'Descriptive flexfield segment','ATTRIBUTE4','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','ATTRIBUTE4','Attribute4','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','ATTRIBUTE_CATEGORY',200,'Descriptive flexfield segment','ATTRIBUTE_CATEGORY','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','ATTRIBUTE_CATEGORY','Attribute Category','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','FEDERAL_REPORTABLE_FLAG',200,'Federal reportable flag','FEDERAL_REPORTABLE_FLAG','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','FEDERAL_REPORTABLE_FLAG','Federal Reportable Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','STATE_REPORTABLE_FLAG',200,'State reportable flag','STATE_REPORTABLE_FLAG','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','STATE_REPORTABLE_FLAG','State Reportable Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','NAME_CONTROL',200,'Name control','NAME_CONTROL','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','NAME_CONTROL','Name Control','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','TAX_VERIFICATION_DATE',200,'Tax verification date','TAX_VERIFICATION_DATE','','','','ANONYMOUS','DATE','AP_SUPPLIERS','TAX_VERIFICATION_DATE','Tax Verification Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','AMOUNT_INCLUDES_TAX_FLAG',200,'Do amounts include tax from this supplier?','AMOUNT_INCLUDES_TAX_FLAG','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','AMOUNT_INCLUDES_TAX_FLAG','Amount Includes Tax Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','AUTO_TAX_CALC_OVERRIDE',200,'Allows override of tax calculation at supplier site level','AUTO_TAX_CALC_OVERRIDE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','AUTO_TAX_CALC_OVERRIDE','Auto Tax Calc Override','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','AUTO_TAX_CALC_FLAG',200,'Supplier level where the tax calculation is done','AUTO_TAX_CALC_FLAG','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','AUTO_TAX_CALC_FLAG','Auto Tax Calc Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','AP_TAX_ROUNDING_RULE',200,'No Longer Used','AP_TAX_ROUNDING_RULE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','AP_TAX_ROUNDING_RULE','Ap Tax Rounding Rule','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','EXCLUSIVE_PAYMENT_FLAG',200,'Indicates exclusive payment','EXCLUSIVE_PAYMENT_FLAG','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','EXCLUSIVE_PAYMENT_FLAG','Exclusive Payment Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','HOLD_UNMATCHED_INVOICES_FLAG',200,'Indicates whether unmatched invoices should be put on hold','HOLD_UNMATCHED_INVOICES_FLAG','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','HOLD_UNMATCHED_INVOICES_FLAG','Hold Unmatched Invoices Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','ALLOW_UNORDERED_RECEIPTS_FLAG',200,'Indicates whether unordered receipts are allowed','ALLOW_UNORDERED_RECEIPTS_FLAG','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','ALLOW_UNORDERED_RECEIPTS_FLAG','Allow Unordered Receipts Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','ALLOW_SUBSTITUTE_RECEIPTS_FLAG',200,'Indicates whether substitute items can be received in place of the ordered items','ALLOW_SUBSTITUTE_RECEIPTS_FLAG','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','ALLOW_SUBSTITUTE_RECEIPTS_FLAG','Allow Substitute Receipts Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','RECEIVING_ROUTING_ID',200,'Receipt routing unique identifier','RECEIVING_ROUTING_ID','','','','ANONYMOUS','NUMBER','AP_SUPPLIERS','RECEIVING_ROUTING_ID','Receiving Routing Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','RECEIPT_DAYS_EXCEPTION_CODE',200,'Action to be taken when items are received earlier or later than the allowed number of days specified','RECEIPT_DAYS_EXCEPTION_CODE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','RECEIPT_DAYS_EXCEPTION_CODE','Receipt Days Exception Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','DAYS_LATE_RECEIPT_ALLOWED',200,'Maximum acceptable number of days items can be received late','DAYS_LATE_RECEIPT_ALLOWED','','','','ANONYMOUS','NUMBER','AP_SUPPLIERS','DAYS_LATE_RECEIPT_ALLOWED','Days Late Receipt Allowed','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','DAYS_EARLY_RECEIPT_ALLOWED',200,'Maximum acceptable number of days items can be received early','DAYS_EARLY_RECEIPT_ALLOWED','','','','ANONYMOUS','NUMBER','AP_SUPPLIERS','DAYS_EARLY_RECEIPT_ALLOWED','Days Early Receipt Allowed','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','ENFORCE_SHIP_TO_LOCATION_CODE',200,'Action to be taken when the receiving location differs from the ship-to location','ENFORCE_SHIP_TO_LOCATION_CODE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','ENFORCE_SHIP_TO_LOCATION_CODE','Enforce Ship To Location Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','QTY_RCV_EXCEPTION_CODE',200,'Enforces, warns, or ignores quantity receiving tolerance','QTY_RCV_EXCEPTION_CODE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','QTY_RCV_EXCEPTION_CODE','Qty Rcv Exception Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','QTY_RCV_TOLERANCE',200,'Quantity received tolerance percentage','QTY_RCV_TOLERANCE','','~T~D~2','','ANONYMOUS','NUMBER','AP_SUPPLIERS','QTY_RCV_TOLERANCE','Qty Rcv Tolerance','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','RECEIPT_REQUIRED_FLAG',200,'Indicates whether shipment must be received before the invoice is paid','RECEIPT_REQUIRED_FLAG','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','RECEIPT_REQUIRED_FLAG','Receipt Required Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','INSPECTION_REQUIRED_FLAG',200,'Indicates whether inspection is required or not','INSPECTION_REQUIRED_FLAG','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','INSPECTION_REQUIRED_FLAG','Inspection Required Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','PRICE_TOLERANCE',200,'Not used','PRICE_TOLERANCE','','~T~D~2','','ANONYMOUS','NUMBER','AP_SUPPLIERS','PRICE_TOLERANCE','Price Tolerance','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','TERMS_DATE_BASIS',200,'Type of invoice payment schedule basis','TERMS_DATE_BASIS','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','TERMS_DATE_BASIS','Terms Date Basis','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','HOLD_DATE',200,'Date the supplier was placed on purchasing hold','HOLD_DATE','','','','ANONYMOUS','DATE','AP_SUPPLIERS','HOLD_DATE','Hold Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','HOLD_BY',200,'Unique identifier for the employee who placed the supplier on hold','HOLD_BY','','','','ANONYMOUS','NUMBER','AP_SUPPLIERS','HOLD_BY','Hold By','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','PURCHASING_HOLD_REASON',200,'Reason for placing the supplier on purchasing hold','PURCHASING_HOLD_REASON','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','PURCHASING_HOLD_REASON','Purchasing Hold Reason','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','HOLD_FLAG',200,'Indicator of whether the supplier is on purchasing hold','HOLD_FLAG','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','HOLD_FLAG','Hold Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','STANDARD_INDUSTRY_CLASS',200,'Standard industry classification number','STANDARD_INDUSTRY_CLASS','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','STANDARD_INDUSTRY_CLASS','Standard Industry Class','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','SMALL_BUSINESS_FLAG',200,'Indicates that the supplier is a small business','SMALL_BUSINESS_FLAG','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','SMALL_BUSINESS_FLAG','Small Business Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','WOMEN_OWNED_FLAG',200,'Indicates whether the supplier is a woman-owned business','WOMEN_OWNED_FLAG','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','WOMEN_OWNED_FLAG','Women Owned Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','BANK_ACCOUNT_TYPE',200,'No longer used','BANK_ACCOUNT_TYPE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','BANK_ACCOUNT_TYPE','Bank Account Type','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','BANK_NUM',200,'No longer used','BANK_NUM','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','BANK_NUM','Bank Num','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','TCA_SYNC_VAT_REG_NUM',200,'For internal use only','TCA_SYNC_VAT_REG_NUM','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','TCA_SYNC_VAT_REG_NUM','Tca Sync Vat Reg Num','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','TCA_SYNC_VENDOR_NAME',200,'For internal use only','TCA_SYNC_VENDOR_NAME','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','TCA_SYNC_VENDOR_NAME','Tca Sync Vendor Name','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','TCA_SYNC_NUM_1099',200,'For internal use only','TCA_SYNC_NUM_1099','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','TCA_SYNC_NUM_1099','Tca Sync Num 1099','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','NI_NUMBER',200,'National Insurance Number','NI_NUMBER','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','NI_NUMBER','Ni Number','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','PARENT_PARTY_ID',200,'Parent Party Identifier','PARENT_PARTY_ID','','','','ANONYMOUS','NUMBER','AP_SUPPLIERS','PARENT_PARTY_ID','Parent Party Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','PARTY_ID',200,'Party Identifier','PARTY_ID','','','','ANONYMOUS','NUMBER','AP_SUPPLIERS','PARTY_ID','Party Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','OFFSET_TAX_FLAG',200,'Indicator of whether the supplier uses offset taxes','OFFSET_TAX_FLAG','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','OFFSET_TAX_FLAG','Offset Tax Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','CREATE_DEBIT_MEMO_FLAG',200,'Indicator of whether a debit memo should be created','CREATE_DEBIT_MEMO_FLAG','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','CREATE_DEBIT_MEMO_FLAG','Create Debit Memo Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','FUTURE_DATED_PAYMENT_CCID',200,'Accounting Flexfield identifier for the future dated payment account','FUTURE_DATED_PAYMENT_CCID','','','','ANONYMOUS','NUMBER','AP_SUPPLIERS','FUTURE_DATED_PAYMENT_CCID','Future Dated Payment Ccid','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','MATCH_OPTION',200,'Indicator of whether to match invoices to the purchase order or the receipt for this supplier','MATCH_OPTION','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','MATCH_OPTION','Match Option','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','BANK_BRANCH_TYPE',200,'No longer used','BANK_BRANCH_TYPE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','BANK_BRANCH_TYPE','Bank Branch Type','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','BANK_CHARGE_BEARER',200,'Indicator of whether this supplier bears bank charges','BANK_CHARGE_BEARER','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','BANK_CHARGE_BEARER','Bank Charge Bearer','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','EDI_REMITTANCE_INSTRUCTION',200,'No longer used','EDI_REMITTANCE_INSTRUCTION','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','EDI_REMITTANCE_INSTRUCTION','Edi Remittance Instruction','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','EDI_REMITTANCE_METHOD',200,'No longer used','EDI_REMITTANCE_METHOD','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','EDI_REMITTANCE_METHOD','Edi Remittance Method','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','EDI_PAYMENT_FORMAT',200,'No longer used','EDI_PAYMENT_FORMAT','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','EDI_PAYMENT_FORMAT','Edi Payment Format','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','EDI_PAYMENT_METHOD',200,'No longer used','EDI_PAYMENT_METHOD','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','EDI_PAYMENT_METHOD','Edi Payment Method','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','EDI_TRANSACTION_HANDLING',200,'No longer used','EDI_TRANSACTION_HANDLING','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','EDI_TRANSACTION_HANDLING','Edi Transaction Handling','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','GLOBAL_ATTRIBUTE_CATEGORY',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE_CATEGORY','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','GLOBAL_ATTRIBUTE_CATEGORY','Global Attribute Category','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','GLOBAL_ATTRIBUTE20',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE20','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','GLOBAL_ATTRIBUTE20','Global Attribute20','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','GLOBAL_ATTRIBUTE19',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE19','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','GLOBAL_ATTRIBUTE19','Global Attribute19','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','GLOBAL_ATTRIBUTE18',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE18','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','GLOBAL_ATTRIBUTE18','Global Attribute18','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','GLOBAL_ATTRIBUTE17',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE17','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','GLOBAL_ATTRIBUTE17','Global Attribute17','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','GLOBAL_ATTRIBUTE16',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE16','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','GLOBAL_ATTRIBUTE16','Global Attribute16','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','GLOBAL_ATTRIBUTE15',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE15','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','GLOBAL_ATTRIBUTE15','Global Attribute15','','','','US');
--Inserting Object Components for AP_SUPPLIERS
--Inserting Object Component Joins for AP_SUPPLIERS
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
--Exporting View Component Data of the View -  EIS_AP_SUPPLIERS_DET_V
prompt Creating Object Data PO_VENDOR_SITES
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Inserting Object PO_VENDOR_SITES
xxeis.eis_rsc_ins.v( 'PO_VENDOR_SITES',200,'AP_SUPPLIER_SITES_ALL stores information about your supplier site level attributes.','1.0','','','ANONYMOUS','APPS','Po Vendor Sites','PVS','','','VIEW','US','','');
--Delete Object Columns for PO_VENDOR_SITES
xxeis.eis_rsc_utility.delete_view_rows('PO_VENDOR_SITES',200,FALSE);
--Inserting Object Columns for PO_VENDOR_SITES
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','SHIPPING_CONTROL',200,'Indicator of who is responsible for arranging transportation','SHIPPING_CONTROL','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','SHIPPING_CONTROL','Shipping Control','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','PRIMARY_PAY_SITE_FLAG',200,'A value of Y indicates that this is the default pay site for the supplier','PRIMARY_PAY_SITE_FLAG','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','PRIMARY_PAY_SITE_FLAG','Primary Pay Site Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','EMAIL_ADDRESS',200,'Email Address of the supplier Contact','EMAIL_ADDRESS','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','EMAIL_ADDRESS','Email Address','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','SUPPLIER_NOTIF_METHOD',200,'The preferred Notification Method for the supplier','SUPPLIER_NOTIF_METHOD','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','SUPPLIER_NOTIF_METHOD','Supplier Notif Method','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','CREATE_DEBIT_MEMO_FLAG',200,'Indicator of whether a debit memo should be created','CREATE_DEBIT_MEMO_FLAG','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','CREATE_DEBIT_MEMO_FLAG','Create Debit Memo Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','FUTURE_DATED_PAYMENT_CCID',200,'Accounting Flexfield identifier for the future dated payment account','FUTURE_DATED_PAYMENT_CCID','','','','ANONYMOUS','NUMBER','PO_VENDOR_SITES','FUTURE_DATED_PAYMENT_CCID','Future Dated Payment Ccid','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','COUNTRY_OF_ORIGIN_CODE',200,'Code for the country of manufacture','COUNTRY_OF_ORIGIN_CODE','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','COUNTRY_OF_ORIGIN_CODE','Country Of Origin Code','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','MATCH_OPTION',200,'Indicator of whether to match the invoices to the purchase order or receipt','MATCH_OPTION','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','MATCH_OPTION','Match Option','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','PCARD_SITE_FLAG',200,'Indicator of whether the site allows use of procurement cards','PCARD_SITE_FLAG','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','PCARD_SITE_FLAG','Pcard Site Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','ECE_TP_LOCATION_CODE',200,'Trading partner location code for e-Commerce Gateway','ECE_TP_LOCATION_CODE','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','ECE_TP_LOCATION_CODE','Ece Tp Location Code','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','TP_HEADER_ID',200,'EDI transaction header unique identifier','TP_HEADER_ID','','','','ANONYMOUS','NUMBER','PO_VENDOR_SITES','TP_HEADER_ID','Tp Header Id','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','GLOBAL_ATTRIBUTE20',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE20','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','GLOBAL_ATTRIBUTE20','Global Attribute20','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','GLOBAL_ATTRIBUTE19',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE19','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','GLOBAL_ATTRIBUTE19','Global Attribute19','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','GLOBAL_ATTRIBUTE18',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE18','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','GLOBAL_ATTRIBUTE18','Global Attribute18','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','GLOBAL_ATTRIBUTE17',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE17','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','GLOBAL_ATTRIBUTE17','Global Attribute17','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','GLOBAL_ATTRIBUTE16',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE16','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','GLOBAL_ATTRIBUTE16','Global Attribute16','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','GLOBAL_ATTRIBUTE15',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE15','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','GLOBAL_ATTRIBUTE15','Global Attribute15','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','GLOBAL_ATTRIBUTE14',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE14','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','GLOBAL_ATTRIBUTE14','Global Attribute14','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','GLOBAL_ATTRIBUTE13',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE13','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','GLOBAL_ATTRIBUTE13','Global Attribute13','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','GLOBAL_ATTRIBUTE12',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE12','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','GLOBAL_ATTRIBUTE12','Global Attribute12','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','GLOBAL_ATTRIBUTE11',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE11','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','GLOBAL_ATTRIBUTE11','Global Attribute11','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','GLOBAL_ATTRIBUTE10',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE10','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','GLOBAL_ATTRIBUTE10','Global Attribute10','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','GLOBAL_ATTRIBUTE9',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE9','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','GLOBAL_ATTRIBUTE9','Global Attribute9','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','GLOBAL_ATTRIBUTE8',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE8','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','GLOBAL_ATTRIBUTE8','Global Attribute8','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','GLOBAL_ATTRIBUTE7',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE7','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','GLOBAL_ATTRIBUTE7','Global Attribute7','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','GLOBAL_ATTRIBUTE6',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE6','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','GLOBAL_ATTRIBUTE6','Global Attribute6','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','GLOBAL_ATTRIBUTE5',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE5','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','GLOBAL_ATTRIBUTE5','Global Attribute5','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','GLOBAL_ATTRIBUTE4',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE4','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','GLOBAL_ATTRIBUTE4','Global Attribute4','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','GLOBAL_ATTRIBUTE3',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE3','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','GLOBAL_ATTRIBUTE3','Global Attribute3','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','GLOBAL_ATTRIBUTE2',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE2','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','GLOBAL_ATTRIBUTE2','Global Attribute2','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','GLOBAL_ATTRIBUTE1',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE1','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','GLOBAL_ATTRIBUTE1','Global Attribute1','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','GLOBAL_ATTRIBUTE_CATEGORY',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE_CATEGORY','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','GLOBAL_ATTRIBUTE_CATEGORY','Global Attribute Category','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','PAY_ON_RECEIPT_SUMMARY_CODE',200,'If the PAY_ON_CODE is ''RECEIPT'', then this column identifies how to consolidate the receipts to create invoices. Valid values are PAY_SITE, PACKING_SLIP, and RECEIPT.','PAY_ON_RECEIPT_SUMMARY_CODE','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','PAY_ON_RECEIPT_SUMMARY_CODE','Pay On Receipt Summary Code','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','PAY_ON_CODE',200,'When to create an invoice. Currently, the code ''RECEIPT'' is used. Future enhancements could include ''DELIVERY''.','PAY_ON_CODE','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','PAY_ON_CODE','Pay On Code','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','DEFAULT_PAY_SITE_ID',200,'Payment site for the site in which the receipt was entered.','DEFAULT_PAY_SITE_ID','','','','ANONYMOUS','NUMBER','PO_VENDOR_SITES','DEFAULT_PAY_SITE_ID','Default Pay Site Id','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','PAY_AWT_GROUP_ID',200,'','PAY_AWT_GROUP_ID','','','','ANONYMOUS','NUMBER','PO_VENDOR_SITES','PAY_AWT_GROUP_ID','Pay Awt Group Id','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','AWT_GROUP_ID',200,'Unique identifier for the withholding tax group','AWT_GROUP_ID','','','','ANONYMOUS','NUMBER','PO_VENDOR_SITES','AWT_GROUP_ID','Awt Group Id','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','ALLOW_AWT_FLAG',200,'Indicator of whether Allow Withholding Tax is enabled','ALLOW_AWT_FLAG','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','ALLOW_AWT_FLAG','Allow Awt Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','ADDRESS_LINE4',200,'Fourth line of address','ADDRESS_LINE4','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','ADDRESS_LINE4','Address Line4','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','CHECK_DIGITS',200,'Check digits for Accounts Payable','CHECK_DIGITS','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','CHECK_DIGITS','Check Digits','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','ORG_ID',200,'Operating unit unique identifier','ORG_ID','','','','ANONYMOUS','NUMBER','PO_VENDOR_SITES','ORG_ID','Org Id','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','BANK_CHARGE_BEARER',200,'Indicator of whether this supplier site is a bank charge bearer site','BANK_CHARGE_BEARER','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','BANK_CHARGE_BEARER','Bank Charge Bearer','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','EXCLUDE_FREIGHT_FROM_DISCOUNT',200,'Indicates whether the freight amount is to be excluded from the discount','EXCLUDE_FREIGHT_FROM_DISCOUNT','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','EXCLUDE_FREIGHT_FROM_DISCOUNT','Exclude Freight From Discount','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','VALIDATION_NUMBER',200,'Validation number','VALIDATION_NUMBER','','','','ANONYMOUS','NUMBER','PO_VENDOR_SITES','VALIDATION_NUMBER','Validation Number','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','PROGRAM_UPDATE_DATE',200,'Standard Who column','PROGRAM_UPDATE_DATE','','','','ANONYMOUS','DATE','PO_VENDOR_SITES','PROGRAM_UPDATE_DATE','Program Update Date','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','PROGRAM_ID',200,'Standard Who column','PROGRAM_ID','','','','ANONYMOUS','NUMBER','PO_VENDOR_SITES','PROGRAM_ID','Program Id','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','PROGRAM_APPLICATION_ID',200,'Standard Who column','PROGRAM_APPLICATION_ID','','','','ANONYMOUS','NUMBER','PO_VENDOR_SITES','PROGRAM_APPLICATION_ID','Program Application Id','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','REQUEST_ID',200,'Standard Who column','REQUEST_ID','','','','ANONYMOUS','NUMBER','PO_VENDOR_SITES','REQUEST_ID','Request Id','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','ATTRIBUTE15',200,'Descriptive flexfield segment','ATTRIBUTE15','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','ATTRIBUTE15','Attribute15','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','ATTRIBUTE14',200,'Descriptive flexfield segment','ATTRIBUTE14','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','ATTRIBUTE14','Attribute14','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','ATTRIBUTE13',200,'Descriptive flexfield segment','ATTRIBUTE13','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','ATTRIBUTE13','Attribute13','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','ATTRIBUTE12',200,'Descriptive flexfield segment','ATTRIBUTE12','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','ATTRIBUTE12','Attribute12','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','ATTRIBUTE11',200,'Descriptive flexfield segment','ATTRIBUTE11','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','ATTRIBUTE11','Attribute11','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','ATTRIBUTE10',200,'Descriptive flexfield segment','ATTRIBUTE10','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','ATTRIBUTE10','Attribute10','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','ATTRIBUTE9',200,'Descriptive flexfield segment','ATTRIBUTE9','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','ATTRIBUTE9','Attribute9','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','ATTRIBUTE8',200,'Descriptive flexfield segment','ATTRIBUTE8','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','ATTRIBUTE8','Attribute8','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','ATTRIBUTE7',200,'Descriptive flexfield segment','ATTRIBUTE7','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','ATTRIBUTE7','Attribute7','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','ATTRIBUTE6',200,'Descriptive flexfield segment','ATTRIBUTE6','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','ATTRIBUTE6','Attribute6','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','ATTRIBUTE5',200,'Descriptive flexfield segment','ATTRIBUTE5','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','ATTRIBUTE5','Attribute5','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','ATTRIBUTE4',200,'Descriptive flexfield segment','ATTRIBUTE4','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','ATTRIBUTE4','Attribute4','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','ATTRIBUTE3',200,'Descriptive flexfield segment','ATTRIBUTE3','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','ATTRIBUTE3','Attribute3','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','ATTRIBUTE2',200,'Descriptive flexfield segment','ATTRIBUTE2','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','ATTRIBUTE2','Attribute2','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','ATTRIBUTE1',200,'Descriptive flexfield segment','ATTRIBUTE1','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','ATTRIBUTE1','Attribute1','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','ATTRIBUTE_CATEGORY',200,'Descriptive flexfield segment','ATTRIBUTE_CATEGORY','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','ATTRIBUTE_CATEGORY','Attribute Category','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','TAX_REPORTING_SITE_FLAG',200,'Tax reporting site Flag','TAX_REPORTING_SITE_FLAG','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','TAX_REPORTING_SITE_FLAG','Tax Reporting Site Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','HOLD_UNMATCHED_INVOICES_FLAG',200,'Indicates whether unmatched invoices should be put on hold','HOLD_UNMATCHED_INVOICES_FLAG','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','HOLD_UNMATCHED_INVOICES_FLAG','Hold Unmatched Invoices Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','HOLD_REASON',200,'Reason that the Supplier Site has been placed on hold','HOLD_REASON','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','HOLD_REASON','Hold Reason','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','HOLD_FUTURE_PAYMENTS_FLAG',200,'Indicates whether Oracle Payables should place unapproved invoices for this supplier on hold','HOLD_FUTURE_PAYMENTS_FLAG','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','HOLD_FUTURE_PAYMENTS_FLAG','Hold Future Payments Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','HOLD_ALL_PAYMENTS_FLAG',200,'Indicates whether Oracle Payables should place all payments for this supplier on hold','HOLD_ALL_PAYMENTS_FLAG','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','HOLD_ALL_PAYMENTS_FLAG','Hold All Payments Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','ZIP',200,'Postal code','ZIP','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','ZIP','Zip','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','STATE',200,'State name or abbreviation','STATE','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','STATE','State','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','CITY',200,'City name','CITY','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','CITY','City','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','ADDRESS_LINES_ALT',200,'Alternate address line for Kana Value','ADDRESS_LINES_ALT','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','ADDRESS_LINES_ALT','Address Lines Alt','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','ADDRESS_LINE3',200,'Third line of supplier address','ADDRESS_LINE3','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','ADDRESS_LINE3','Address Line3','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','ADDRESS_LINE2',200,'Second line of supplier address','ADDRESS_LINE2','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','ADDRESS_LINE2','Address Line2','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','ADDRESS_LINE1',200,'First line of supplier address','ADDRESS_LINE1','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','ADDRESS_LINE1','Address Line1','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','ATTENTION_AR_FLAG',200,'Indicates whether the payments should be sent to the Accounts Receivable department','ATTENTION_AR_FLAG','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','ATTENTION_AR_FLAG','Attention Ar Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','PAY_SITE_FLAG',200,'Indicates whether you can send payments to this site','PAY_SITE_FLAG','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','PAY_SITE_FLAG','Pay Site Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','RFQ_ONLY_SITE_FLAG',200,'Indicates whether you can only send RFQs to this site','RFQ_ONLY_SITE_FLAG','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','RFQ_ONLY_SITE_FLAG','Rfq Only Site Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','PURCHASING_SITE_FLAG',200,'Indicates whether you can purchase from this site','PURCHASING_SITE_FLAG','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','PURCHASING_SITE_FLAG','Purchasing Site Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','CREATED_BY',200,'Standard Who column','CREATED_BY','','','','ANONYMOUS','NUMBER','PO_VENDOR_SITES','CREATED_BY','Created By','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','CREATION_DATE',200,'Standard Who column','CREATION_DATE','','','','ANONYMOUS','DATE','PO_VENDOR_SITES','CREATION_DATE','Creation Date','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','LAST_UPDATE_LOGIN',200,'Standard Who column','LAST_UPDATE_LOGIN','','','','ANONYMOUS','NUMBER','PO_VENDOR_SITES','LAST_UPDATE_LOGIN','Last Update Login','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','VENDOR_SITE_CODE_ALT',200,'Alternate supplier site code for Kana Value','VENDOR_SITE_CODE_ALT','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','VENDOR_SITE_CODE_ALT','Vendor Site Code Alt','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','VENDOR_SITE_CODE',200,'Site code name','VENDOR_SITE_CODE','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','VENDOR_SITE_CODE','Vendor Site Code','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','VENDOR_ID',200,'Supplier unique identifier','VENDOR_ID','','','','ANONYMOUS','NUMBER','PO_VENDOR_SITES','VENDOR_ID','Vendor Id','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','LAST_UPDATED_BY',200,'Standard Who column','LAST_UPDATED_BY','','','','ANONYMOUS','NUMBER','PO_VENDOR_SITES','LAST_UPDATED_BY','Last Updated By','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','LAST_UPDATE_DATE',200,'Standard Who column','LAST_UPDATE_DATE','','','','ANONYMOUS','DATE','PO_VENDOR_SITES','LAST_UPDATE_DATE','Last Update Date','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','VENDOR_SITE_ID',200,'Supplier site unique identifier','VENDOR_SITE_ID','','','','ANONYMOUS','NUMBER','PO_VENDOR_SITES','VENDOR_SITE_ID','Vendor Site Id','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','COUNTY',200,'Supplier site county','COUNTY','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','COUNTY','County','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','PAYMENT_CURRENCY_CODE',200,'Default payment currency unique identifier','PAYMENT_CURRENCY_CODE','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','PAYMENT_CURRENCY_CODE','Payment Currency Code','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','INVOICE_CURRENCY_CODE',200,'Default currency unique identifier','INVOICE_CURRENCY_CODE','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','INVOICE_CURRENCY_CODE','Invoice Currency Code','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','ALWAYS_TAKE_DISC_FLAG',200,'Indicator of whether Oracle Payables should always take a discount for the supplier','ALWAYS_TAKE_DISC_FLAG','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','ALWAYS_TAKE_DISC_FLAG','Always Take Disc Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','PAY_DATE_BASIS_LOOKUP_CODE',200,'Type of payment date basis','PAY_DATE_BASIS_LOOKUP_CODE','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','PAY_DATE_BASIS_LOOKUP_CODE','Pay Date Basis Lookup Code','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','INVOICE_AMOUNT_LIMIT',200,'Maximum amount per invoice','INVOICE_AMOUNT_LIMIT','','~T~D~2','','ANONYMOUS','NUMBER','PO_VENDOR_SITES','INVOICE_AMOUNT_LIMIT','Invoice Amount Limit','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','TERMS_ID',200,'Payment terms unique identifier','TERMS_ID','','','','ANONYMOUS','NUMBER','PO_VENDOR_SITES','TERMS_ID','Terms Id','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','PAYMENT_PRIORITY',200,'Payment priority','PAYMENT_PRIORITY','','','','ANONYMOUS','NUMBER','PO_VENDOR_SITES','PAYMENT_PRIORITY','Payment Priority','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','PAY_GROUP_LOOKUP_CODE',200,'Payment group type','PAY_GROUP_LOOKUP_CODE','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','PAY_GROUP_LOOKUP_CODE','Pay Group Lookup Code','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','PREPAY_CODE_COMBINATION_ID',200,'Unique identifier for the general ledger account for prepayment','PREPAY_CODE_COMBINATION_ID','','','','ANONYMOUS','NUMBER','PO_VENDOR_SITES','PREPAY_CODE_COMBINATION_ID','Prepay Code Combination Id','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','ACCTS_PAY_CODE_COMBINATION_ID',200,'Unique identifier for the supplier liability account','ACCTS_PAY_CODE_COMBINATION_ID','','~T~D~2','','ANONYMOUS','NUMBER','PO_VENDOR_SITES','ACCTS_PAY_CODE_COMBINATION_ID','Accts Pay Code Combination Id','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','DISTRIBUTION_SET_ID',200,'Distribution set unique identifier','DISTRIBUTION_SET_ID','','','','ANONYMOUS','NUMBER','PO_VENDOR_SITES','DISTRIBUTION_SET_ID','Distribution Set Id','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','TERMS_DATE_BASIS',200,'Type of invoice payment schedule basis','TERMS_DATE_BASIS','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','TERMS_DATE_BASIS','Terms Date Basis','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','TELEX',200,'Telex number','TELEX','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','TELEX','Telex','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','FAX_AREA_CODE',200,'Customer site','FAX_AREA_CODE','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','FAX_AREA_CODE','Fax Area Code','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','FAX',200,'Customer site facsimile number','FAX','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','FAX','Fax','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','INACTIVE_DATE',200,'Inactive date for record','INACTIVE_DATE','','','','ANONYMOUS','DATE','PO_VENDOR_SITES','INACTIVE_DATE','Inactive Date','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','FOB_LOOKUP_CODE',200,'Default free-on-board type','FOB_LOOKUP_CODE','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','FOB_LOOKUP_CODE','Fob Lookup Code','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','FREIGHT_TERMS_LOOKUP_CODE',200,'Default freight terms type','FREIGHT_TERMS_LOOKUP_CODE','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','FREIGHT_TERMS_LOOKUP_CODE','Freight Terms Lookup Code','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','SHIP_VIA_LOOKUP_CODE',200,'Default carrier type','SHIP_VIA_LOOKUP_CODE','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','SHIP_VIA_LOOKUP_CODE','Ship Via Lookup Code','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','BILL_TO_LOCATION_ID',200,'Ship-to location unique identifier','BILL_TO_LOCATION_ID','','','','ANONYMOUS','NUMBER','PO_VENDOR_SITES','BILL_TO_LOCATION_ID','Bill To Location Id','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','SHIP_TO_LOCATION_ID',200,'Default ship-to location unique identifier','SHIP_TO_LOCATION_ID','','','','ANONYMOUS','NUMBER','PO_VENDOR_SITES','SHIP_TO_LOCATION_ID','Ship To Location Id','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','CUSTOMER_NUM',200,'Customer number with the supplier site','CUSTOMER_NUM','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','CUSTOMER_NUM','Customer Num','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','PHONE',200,'Phone number','PHONE','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','PHONE','Phone','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','AREA_CODE',200,'Area code','AREA_CODE','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','AREA_CODE','Area Code','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','COUNTRY',200,'Country name','COUNTRY','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','COUNTRY','Country','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','PROVINCE',200,'Province','PROVINCE','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','PROVINCE','Province','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','LANGUAGE',200,'Site language','LANGUAGE','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','LANGUAGE','Language','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','ADDRESS_STYLE',200,'Style of address','ADDRESS_STYLE','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','ADDRESS_STYLE','Address Style','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','EDI_REMITTANCE_INSTRUCTION',200,'No longer used','EDI_REMITTANCE_INSTRUCTION','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','EDI_REMITTANCE_INSTRUCTION','Edi Remittance Instruction','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','EDI_REMITTANCE_METHOD',200,'No longer used','EDI_REMITTANCE_METHOD','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','EDI_REMITTANCE_METHOD','Edi Remittance Method','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','EDI_PAYMENT_FORMAT',200,'No longer used','EDI_PAYMENT_FORMAT','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','EDI_PAYMENT_FORMAT','Edi Payment Format','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','EDI_PAYMENT_METHOD',200,'No longer used','EDI_PAYMENT_METHOD','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','EDI_PAYMENT_METHOD','Edi Payment Method','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','EDI_ID_NUMBER',200,'EDI Trading Partner Number for the Supplier Site','EDI_ID_NUMBER','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','EDI_ID_NUMBER','Edi Id Number','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','EDI_TRANSACTION_HANDLING',200,'No longer used','EDI_TRANSACTION_HANDLING','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','EDI_TRANSACTION_HANDLING','Edi Transaction Handling','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','REMITTANCE_EMAIL',200,'The destination e-mail address for remittance information. Populating this field activates the E-mail Remittance Advice feature in Payables','REMITTANCE_EMAIL','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','REMITTANCE_EMAIL','Remittance Email','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','OFFSET_TAX_FLAG',200,'Indicator of whether offset tax is used','OFFSET_TAX_FLAG','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','OFFSET_TAX_FLAG','Offset Tax Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','BANK_BRANCH_TYPE',200,'No longer used','BANK_BRANCH_TYPE','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','BANK_BRANCH_TYPE','Bank Branch Type','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','BANK_NUMBER',200,'No longer used','BANK_NUMBER','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','BANK_NUMBER','Bank Number','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','OFFSET_VAT_CODE',200,'Offset VAT code','OFFSET_VAT_CODE','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','OFFSET_VAT_CODE','Offset Vat Code','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','AMOUNT_INCLUDES_TAX_FLAG',200,'Do amounts include tax from this supplier?','AMOUNT_INCLUDES_TAX_FLAG','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','AMOUNT_INCLUDES_TAX_FLAG','Amount Includes Tax Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','AUTO_TAX_CALC_OVERRIDE',200,'Allows override of tax calculation at supplier site level','AUTO_TAX_CALC_OVERRIDE','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','AUTO_TAX_CALC_OVERRIDE','Auto Tax Calc Override','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','AUTO_TAX_CALC_FLAG',200,'Level for automatic tax calculation for supplier','AUTO_TAX_CALC_FLAG','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','AUTO_TAX_CALC_FLAG','Auto Tax Calc Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','AP_TAX_ROUNDING_RULE',200,'Indicates the rounding rule for tax values','AP_TAX_ROUNDING_RULE','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','AP_TAX_ROUNDING_RULE','Ap Tax Rounding Rule','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','VAT_CODE',200,'Value-added tax code','VAT_CODE','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','VAT_CODE','Vat Code','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','CURRENT_CATALOG_NUM',200,'Not currently used','CURRENT_CATALOG_NUM','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','CURRENT_CATALOG_NUM','Current Catalog Num','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','BANK_ACCOUNT_TYPE',200,'No longer used','BANK_ACCOUNT_TYPE','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','BANK_ACCOUNT_TYPE','Bank Account Type','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','BANK_NUM',200,'No longer used','BANK_NUM','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','BANK_NUM','Bank Num','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','BANK_ACCOUNT_NUM',200,'No longer used','BANK_ACCOUNT_NUM','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','BANK_ACCOUNT_NUM','Bank Account Num','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','BANK_ACCOUNT_NAME',200,'No longer used','BANK_ACCOUNT_NAME','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','BANK_ACCOUNT_NAME','Bank Account Name','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','PAYMENT_METHOD_LOOKUP_CODE',200,'Default payment method type','PAYMENT_METHOD_LOOKUP_CODE','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','PAYMENT_METHOD_LOOKUP_CODE','Payment Method Lookup Code','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','LANGUAGE_CODE',200,'','LANGUAGE_CODE','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','LANGUAGE_CODE','Language Code','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','VAT_REGISTRATION_NUM',200,'VAT registration number','VAT_REGISTRATION_NUM','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','VAT_REGISTRATION_NUM','Vat Registration Num','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','EXCLUSIVE_PAYMENT_FLAG',200,'Exclusive payment flag','EXCLUSIVE_PAYMENT_FLAG','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','EXCLUSIVE_PAYMENT_FLAG','Exclusive Payment Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','SERVICES_TOLERANCE_ID',200,'Services Tolerance Identifier','SERVICES_TOLERANCE_ID','','','','ANONYMOUS','NUMBER','PO_VENDOR_SITES','SERVICES_TOLERANCE_ID','Services Tolerance Id','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','TOLERANCE_ID',200,'Tolerance Idetifier','TOLERANCE_ID','','','','ANONYMOUS','NUMBER','PO_VENDOR_SITES','TOLERANCE_ID','Tolerance Id','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','PARTY_SITE_ID',200,'Party Site Identifier','PARTY_SITE_ID','','','','ANONYMOUS','NUMBER','PO_VENDOR_SITES','PARTY_SITE_ID','Party Site Id','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','LOCATION_ID',200,'Location Identifier','LOCATION_ID','','','','ANONYMOUS','NUMBER','PO_VENDOR_SITES','LOCATION_ID','Location Id','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','DUNS_NUMBER',200,'DUNS Number','DUNS_NUMBER','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','DUNS_NUMBER','Duns Number','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','GAPLESS_INV_NUM_FLAG',200,'Indicates whether gapless invoice numbering is used','GAPLESS_INV_NUM_FLAG','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','GAPLESS_INV_NUM_FLAG','Gapless Inv Num Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'PO_VENDOR_SITES','SELLING_COMPANY_IDENTIFIER',200,'Selling Company Identifier used in generating invoice numbers when gapless invoice numbering is enabled','SELLING_COMPANY_IDENTIFIER','','','','ANONYMOUS','VARCHAR2','PO_VENDOR_SITES','SELLING_COMPANY_IDENTIFIER','Selling Company Identifier','','','','US');
--Inserting Object Components for PO_VENDOR_SITES
--Inserting Object Component Joins for PO_VENDOR_SITES
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
prompt Creating Report LOV Data for WC - Supplier List
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - WC - Supplier List
xxeis.eis_rsc_ins.lov( 200,'select vendor_name from po_vendors','','EIS_PO_SUPPLIER_LOV','','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 200,'SELECT DISTINCT segment1 FROM po_vendors','null','Supplier Numbers LOV','Lists the vendors number','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 200,'SELECT LOOKUP_CODE  TYPE_CODE,
    DISPLAYED_FIELD SUPPLER_TYPE
     FROM PO_LOOKUP_CODES
    WHERE NVL(inactive_date,sysdate+1)>sysdate
  AND lookup_type   = ''VENDOR TYPE''','','SUPPLIER_TYPE','SUPPLIER TYPE','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 200,'SELECT hou.NAME organization_name,
       TO_CHAR (date_from,
                NVL (fnd_profile.VALUE (''ICX_DATE_FORMAT_MASK''),
                     ''DD-MON-YYYY'')
               ) date_from,
       TO_CHAR (date_to,
                NVL (fnd_profile.VALUE (''ICX_DATE_FORMAT_MASK''),
                     ''DD-MON-YYYY'')
               ) date_to
  FROM hr_operating_units hou,
       mo_glob_org_access_tmp tmp
 WHERE hou.organization_id = tmp.organization_id','','EIS_MULTI_OPERATING_UNIT_LOV','This LOV shows all the operating units, to which user has access.','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
prompt Creating Report Data for WC - Supplier List
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Deleting Report data - WC - Supplier List
xxeis.eis_rsc_utility.delete_report_rows( 'WC - Supplier List' );
--Inserting Report - WC - Supplier List
xxeis.eis_rsc_ins.r( 200,'WC - Supplier List','','This report lists supplier information within a specified date range, supplier, supplier type or Parent Supplier. This report displays supplier name, number, address, type of supplier, who created the supplier, credit limit, etc.,

','','','','10012196','EIS_AP_SUPPLIERS_DET_V','Y','','','10012196','','N','Suppliers','','CSV,Pivot Excel,EXCEL,','N','','','','','','','','US','','','','');
--Inserting Report Columns - WC - Supplier List
xxeis.eis_rsc_ins.rc( 'WC - Supplier List',200,'OPERATING_UNIT','Operating Unit','Translated name of the organization','','','','','1','N','','','','','','','','10012196','N','N','','EIS_AP_SUPPLIERS_DET_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Supplier List',200,'HOLD_UNVALIDATED_INVOICES','Hold Unvalidated Invoices','Indicates whether Oracle Payables should place unapproved invoices for this supplier on hold','','','','','47','N','','','','','','','','10012196','N','N','','EIS_AP_SUPPLIERS_DET_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Supplier List',200,'INCOME_TAX_REPORTABLE','Income Tax Reportable','Federal reportable flag','','','','','10','N','','','','','','','','10012196','N','N','','EIS_AP_SUPPLIERS_DET_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Supplier List',200,'INCOME_TAX_TYPE','Income Tax Type','Type of 1099','','','','','12','N','','','','','','','','10012196','N','N','','EIS_AP_SUPPLIERS_DET_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Supplier List',200,'PAYMENT_TERMS','Payment Terms','Payment Terms','','','','','48','N','','','','','','','','10012196','N','N','','EIS_AP_SUPPLIERS_DET_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Supplier List',200,'PAY_GROUP','Pay Group','Pay Group','','','','','49','N','','','','','','','','10012196','N','N','','EIS_AP_SUPPLIERS_DET_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Supplier List',200,'PAY_SITE','Pay Site','Pay Site','','','','','50','N','','','','','','','','10012196','N','N','','EIS_AP_SUPPLIERS_DET_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Supplier List',200,'ADDRESS_LINE1','Address Line1','First line of supplier address','','','','','37','N','','','','','','','','10012196','N','N','','EIS_AP_SUPPLIERS_DET_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Supplier List',200,'PURCHASING_SITE','Purchasing Site','Purchasing Site','','','','','24','N','','','','','','','','10012196','N','N','','EIS_AP_SUPPLIERS_DET_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Supplier List',200,'SITE_CREATED_BY','Site Created By','Application username (what a user types in at the Oracle Applications sign-on screen)','','','','','21','N','','','','','','','','10012196','N','N','','EIS_AP_SUPPLIERS_DET_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Supplier List',200,'SITE_CREATION_DATE','Site Creation Date','Standard Who column','','','','','20','N','','','','','','','','10012196','N','N','','EIS_AP_SUPPLIERS_DET_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Supplier List',200,'SITE_UPDATED_BY','Site Updated By','Application username (what a user types in at the Oracle Applications sign-on screen)','','','','','23','N','','','','','','','','10012196','N','N','','EIS_AP_SUPPLIERS_DET_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Supplier List',200,'SITE_UPDATE_DATE','Site Update Date','Standard Who column','','','','','22','N','','','','','','','','10012196','N','N','','EIS_AP_SUPPLIERS_DET_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Supplier List',200,'SUPPLIER_SITE_NAME','Supplier Site Name','Site code name','','','','','19','N','','','','','','','','10012196','N','N','','EIS_AP_SUPPLIERS_DET_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Supplier List',200,'SUPPLIER_UPDATE_DATE','Supplier Update Date','Standard Who column','','','','','15','N','','','','','','','','10012196','N','N','','EIS_AP_SUPPLIERS_DET_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Supplier List',200,'TAX_REPORTING_SITE','Tax Reporting Site','Tax Reporting Site','','','','','25','N','','','','','','','','10012196','N','N','','EIS_AP_SUPPLIERS_DET_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Supplier List',200,'PO_HOLD','Po Hold','Indicator of whether the supplier is on purchasing hold','','','','','7','N','','','','','','','','10012196','N','N','','EIS_AP_SUPPLIERS_DET_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Supplier List',200,'SUPPLIER_NAME','Supplier Name','Supplier name','','','','','2','N','','','','','','','','10012196','N','N','','EIS_AP_SUPPLIERS_DET_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Supplier List',200,'SUPPLIER_NUMBER','Supplier Number','Supplier number','','','','','3','N','','','','','','','','10012196','N','N','','EIS_AP_SUPPLIERS_DET_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Supplier List',200,'SUPPLIER_TYPE','Supplier Type','QuickCode code','','','','','5','N','','','','','','','','10012196','N','N','','EIS_AP_SUPPLIERS_DET_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Supplier List',200,'ADDRESS_LINE2','Address Line2','Second line of supplier address','','','','','38','N','','','','','','','','10012196','N','N','','EIS_AP_SUPPLIERS_DET_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Supplier List',200,'ADDRESS_LINE3','Address Line3','Third line of supplier address','','','','','39','N','','','','','','','','10012196','N','N','','EIS_AP_SUPPLIERS_DET_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Supplier List',200,'ADDRESS_LINE4','Address Line4','Fourth line of address','','','','','40','N','','','','','','','','10012196','N','N','','EIS_AP_SUPPLIERS_DET_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Supplier List',200,'CITY','City','City name','','','','','41','N','','','','','','','','10012196','N','N','','EIS_AP_SUPPLIERS_DET_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Supplier List',200,'CONTACT_FIRST_NAME','Contact First Name','Contact first name','','','','','42','N','','','','','','','','10012196','N','N','','EIS_AP_SUPPLIERS_DET_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Supplier List',200,'CONTACT_LAST_NAME','Contact Last Name','Contact last name','','','','','43','N','','','','','','','','10012196','N','N','','EIS_AP_SUPPLIERS_DET_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Supplier List',200,'CONTACT_PHONE','Contact Phone','Contact phone number','','','','','44','N','','','','','','','','10012196','N','N','','EIS_AP_SUPPLIERS_DET_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Supplier List',200,'HOLD_ALL_PAYMENTS','Hold All Payments','Indicates whether Oracle Payables should place all payments for this supplier on hold','','','','','45','N','','','','','','','','10012196','N','N','','EIS_AP_SUPPLIERS_DET_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Supplier List',200,'HOLD_REASON','Hold Reason','Hold Reason','','','','','46','N','','','','','','','','10012196','N','N','','EIS_AP_SUPPLIERS_DET_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Supplier List',200,'ALTERNATE_NAME','Alternate Name','Alternate supplier name for kana value','','','','','4','N','','','','','','','','10012196','N','N','','EIS_AP_SUPPLIERS_DET_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Supplier List',200,'TAXPAYER_ID','Taxpayer Id','Tax identification number','','','','','8','N','','','','','','','','10012196','N','N','','EIS_AP_SUPPLIERS_DET_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Supplier List',200,'TAX_REGISTRATION_NUMBER','Tax Registration Number','VAT registration number','','','','','9','N','','','','','','','','10012196','N','N','','EIS_AP_SUPPLIERS_DET_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Supplier List',200,'PARENT_SUPPLIER_NAME','Parent Supplier Name','Supplier name','','','','','17','N','','','','','','','','10012196','N','N','','EIS_AP_SUPPLIERS_DET_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Supplier List',200,'PARENT_SUPPLIER_NUMBER','Parent Supplier Number','Supplier number','','','','','18','N','','','','','','','','10012196','N','N','','EIS_AP_SUPPLIERS_DET_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Supplier List',200,'ONE_TIME','One Time','One Time','','','','','6','N','','','','','','','','10012196','N','N','','EIS_AP_SUPPLIERS_DET_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Supplier List',200,'CREDIT_LIMIT','Credit Limit','Not used','','','','','26','N','','','','','','','','10012196','N','N','','EIS_AP_SUPPLIERS_DET_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - Supplier List',200,'CREDIT_STATUS','Credit Status','No longer used','','','','','27','N','','','','','','','','10012196','N','N','','EIS_AP_SUPPLIERS_DET_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Supplier List',200,'WITHHOLDING_STATUS','Withholding Status','Withholding status type','','','','','28','N','','','','','','','','10012196','N','N','','EIS_AP_SUPPLIERS_DET_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Supplier List',200,'WOMEN_OWNED','Women Owned','Women Owned','','','','','29','N','','','','','','','','10012196','N','N','','EIS_AP_SUPPLIERS_DET_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Supplier List',200,'MINORITY_GROUP','Minority Group','Type of minority-owned business','','','','','30','N','','','','','','','','10012196','N','N','','EIS_AP_SUPPLIERS_DET_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Supplier List',200,'SMALL_BUSINESS','Small Business','Small Business','','','','','31','N','','','','','','','','10012196','N','N','','EIS_AP_SUPPLIERS_DET_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Supplier List',200,'INSPECTION_REQUIRED','Inspection Required','Inspection Required','','','','','32','N','','','','','','','','10012196','N','N','','EIS_AP_SUPPLIERS_DET_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Supplier List',200,'RECEIPT_REQUIRED','Receipt Required','Receipt Required','','','','','33','N','','','','','','','','10012196','N','N','','EIS_AP_SUPPLIERS_DET_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Supplier List',200,'HOLD_PURCHASES','Hold Purchases','Hold Purchases','','','','','34','N','','','','','','','','10012196','N','N','','EIS_AP_SUPPLIERS_DET_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Supplier List',200,'PURCHASING_HOLD_REASON','Purchasing Hold Reason','Reason for placing the supplier on purchasing hold','','','','','35','N','','','','','','','','10012196','N','N','','EIS_AP_SUPPLIERS_DET_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Supplier List',200,'STATE_REPORTABLE','State Reportable','State Reportable','','','','','11','N','','','','','','','','10012196','N','N','','EIS_AP_SUPPLIERS_DET_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Supplier List',200,'SUPPLIER_CREATION_DATE','Supplier Creation Date','Standard Who column','','','','','13','N','','','','','','','','10012196','N','N','','EIS_AP_SUPPLIERS_DET_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Supplier List',200,'INACTIVE_ON','Inactive On','Key flexfield end date','','','','','36','N','','','','','','','','10012196','N','N','','EIS_AP_SUPPLIERS_DET_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Supplier List',200,'SUPPLIER_CREATED_BY','Supplier Created By','Application username (what a user types in at the Oracle Applications sign-on screen)','','','','','14','N','','','','','','','','10012196','N','N','','EIS_AP_SUPPLIERS_DET_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Supplier List',200,'SUPPLIER_UPDATED_BY','Supplier Updated By','Application username (what a user types in at the Oracle Applications sign-on screen)','','','','','16','N','','','','','','','','10012196','N','N','','EIS_AP_SUPPLIERS_DET_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Supplier List',200,'SITE_INACTIVE_ON','Site Inactive On','Inactive date for record','','','','','51','N','','','','','','','','10012196','N','N','','EIS_AP_SUPPLIERS_DET_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Supplier List',200,'PAYMENT_METHOD_LOOKUP_CODE','Payment Method Lookup Code','Default payment method type','','','','','52','','Y','','','','','','','10012196','N','N','','EIS_AP_SUPPLIERS_DET_V','AP_SUPPLIERS','POV','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Supplier List',200,'VENDOR_ID','Vendor Id','Supplier unique identifier','','','','','55','N','','','','','','','','10012196','N','N','','EIS_AP_SUPPLIERS_DET_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - Supplier List',200,'VENDOR_SITE_ID','Vendor Site Id','Supplier site unique identifier','','','','','56','N','','','','','','','','10012196','N','N','','EIS_AP_SUPPLIERS_DET_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - Supplier List',200,'ATTRIBUTE1','Attribute1','Descriptive flexfield segment','','','','','57','N','','','','','','','','10012196','N','N','','EIS_AP_SUPPLIERS_DET_V','AP_SUPPLIERS','POV','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Supplier List',200,'ATTRIBUTE2','Attribute2','Descriptive flexfield segment','','','','','58','N','','','','','','','','10012196','N','N','','EIS_AP_SUPPLIERS_DET_V','AP_SUPPLIERS','POV','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Supplier List',200,'ATTRIBUTE3','Attribute3','Descriptive flexfield segment','','','','','59','N','','','','','','','','10012196','N','N','','EIS_AP_SUPPLIERS_DET_V','AP_SUPPLIERS','POV','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Supplier List',200,'COUNTRY','Country','Country name','','','','','60','N','','','','','','','','10012196','N','N','','EIS_AP_SUPPLIERS_DET_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Supplier List',200,'FAX','Fax','Customer site facsimile number','','','','','61','N','','','','','','','','10012196','N','N','','EIS_AP_SUPPLIERS_DET_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Supplier List',200,'PHONE','Phone','Phone number','','','','','62','N','','','','','','','','10012196','N','N','','EIS_AP_SUPPLIERS_DET_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Supplier List',200,'ZIP','Zip','Postal code','','','','','63','N','','','','','','','','10012196','N','N','','EIS_AP_SUPPLIERS_DET_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Supplier List',200,'STATE','State','State name or abbreviation','','','','','64','N','','','','','','','','10012196','N','N','','EIS_AP_SUPPLIERS_DET_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Supplier List',200,'ORGANIZATION_TYPE_LOOKUP_CODE','Organization Type Lookup Code','IRS organization type','','','','','65','N','','','','','','','','10012196','N','N','','EIS_AP_SUPPLIERS_DET_V','AP_SUPPLIERS','POV','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Supplier List',200,'INDIVIDUAL_1099','Individual 1099','','','','','','66','N','','','','','','','','10012196','N','N','','EIS_AP_SUPPLIERS_DET_V','AP_SUPPLIERS','POV','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Supplier List',200,'VENDOR_SITE_CODE','Vendor Site Code','Site code name','','','','','67','N','','','','','','','','10012196','N','N','','EIS_AP_SUPPLIERS_DET_V','PO_VENDOR_SITES','POVS','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Supplier List',200,'PARTY_SITE_ID','Party Site Id','Party Site Identifier','','','','','68','N','','','','','','','','10012196','N','N','','EIS_AP_SUPPLIERS_DET_V','PO_VENDOR_SITES','POVS','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - Supplier List',200,'EMAIL_ADDRESS','Email Address','Email Address of the supplier Contact','','','','','70','N','','','','','','','','10012196','N','N','','EIS_AP_SUPPLIERS_DET_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Supplier List',200,'ECE_TP_LOCATION_CODE','Ece Tp Location Code','Trading partner location code for e-Commerce Gateway','','','','','71','','Y','','','','','','','10012196','N','N','','EIS_AP_SUPPLIERS_DET_V','PO_VENDOR_SITES','POVS','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Supplier List',200,'PRIMARY_PAY_SITE_FLAG','Primary Pay Site Flag','A value of Y indicates that this is the default pay site for the supplier','','','','','73','','Y','','','','','','','10012196','N','N','','EIS_AP_SUPPLIERS_DET_V','PO_VENDOR_SITES','POVS','GROUP_BY','US','');
--Inserting Report Parameters - WC - Supplier List
xxeis.eis_rsc_ins.rp( 'WC - Supplier List',200,'Operating Unit','','OPERATING_UNIT','IN','EIS_MULTI_OPERATING_UNIT_LOV','HDS White Cap - Org','VARCHAR2','Y','Y','1','Y','Y','CONSTANT','10012196','Y','N','','','','EIS_AP_SUPPLIERS_DET_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC - Supplier List',200,'Date From','','SUPPLIER_CREATION_DATE','>=','','','DATE','N','Y','2','Y','Y','CONSTANT','10012196','Y','N','','Start Date','','EIS_AP_SUPPLIERS_DET_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC - Supplier List',200,'Date To','','SUPPLIER_CREATION_DATE','<=','','','DATE','N','Y','3','Y','Y','CONSTANT','10012196','Y','N','','End Date','','EIS_AP_SUPPLIERS_DET_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC - Supplier List',200,'Supplier Name','','SUPPLIER_NAME','IN','EIS_PO_SUPPLIER_LOV','','VARCHAR2','N','Y','4','Y','Y','CONSTANT','10012196','Y','N','','','','EIS_AP_SUPPLIERS_DET_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC - Supplier List',200,'Supplier Type','','SUPPLIER_TYPE','IN','SUPPLIER_TYPE','','VARCHAR2','N','Y','5','Y','Y','CONSTANT','10012196','Y','N','','','','EIS_AP_SUPPLIERS_DET_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC - Supplier List',200,'Parent Supplier','','PARENT_SUPPLIER_NAME','IN','EIS_PO_SUPPLIER_LOV','','VARCHAR2','N','Y','6','Y','Y','CONSTANT','10012196','Y','N','','','','EIS_AP_SUPPLIERS_DET_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC - Supplier List',200,'Supplier Number','','SUPPLIER_NUMBER','IN','Supplier Numbers LOV','','NUMBER','N','Y','7','Y','Y','CONSTANT','10012196','Y','N','','','','EIS_AP_SUPPLIERS_DET_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC - Supplier List',200,'Pay Sites Only?','','PAY_SITE','IN','','','VARCHAR2','N','Y','8','Y','Y','CONSTANT','10012196','Y','N','','','','EIS_AP_SUPPLIERS_DET_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC - Supplier List',200,'Site Creation Date From','','SITE_CREATION_DATE','>=','','','DATE','N','Y','9','Y','Y','CONSTANT','10012196','Y','N','','Start Date','','EIS_AP_SUPPLIERS_DET_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC - Supplier List',200,'Site Creation Date To','','SITE_CREATION_DATE','<=','','','DATE','N','Y','10','Y','Y','CONSTANT','10012196','Y','N','','End Date','','EIS_AP_SUPPLIERS_DET_V','','','US','');
--Inserting Dependent Parameters - WC - Supplier List
--Inserting Report Conditions - WC - Supplier List
xxeis.eis_rsc_ins.rcnh( 'WC - Supplier List',200,'EAST.OPERATING_UNIT IN Operating Unit','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','OPERATING_UNIT','','Operating Unit','','','','','EIS_AP_SUPPLIERS_DET_V','','','','','','IN','Y','Y','','','','','1',200,'WC - Supplier List','EAST.OPERATING_UNIT IN Operating Unit');
xxeis.eis_rsc_ins.rcnh( 'WC - Supplier List',200,'EAST.SUPPLIER_CREATION_DATE >= Date From','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','SUPPLIER_CREATION_DATE','','Date From','','','','','EIS_AP_SUPPLIERS_DET_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',200,'WC - Supplier List','EAST.SUPPLIER_CREATION_DATE >= Date From');
xxeis.eis_rsc_ins.rcnh( 'WC - Supplier List',200,'EAST.SUPPLIER_CREATION_DATE <= Date To','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','SUPPLIER_CREATION_DATE','','Date To','','','','','EIS_AP_SUPPLIERS_DET_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',200,'WC - Supplier List','EAST.SUPPLIER_CREATION_DATE <= Date To');
xxeis.eis_rsc_ins.rcnh( 'WC - Supplier List',200,'EAST.SUPPLIER_NAME IN Supplier Name','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','SUPPLIER_NAME','','Supplier Name','','','','','EIS_AP_SUPPLIERS_DET_V','','','','','','IN','Y','Y','','','','','1',200,'WC - Supplier List','EAST.SUPPLIER_NAME IN Supplier Name');
xxeis.eis_rsc_ins.rcnh( 'WC - Supplier List',200,'EAST.SUPPLIER_TYPE IN Supplier Type','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','SUPPLIER_TYPE','','Supplier Type','','','','','EIS_AP_SUPPLIERS_DET_V','','','','','','IN','Y','Y','','','','','1',200,'WC - Supplier List','EAST.SUPPLIER_TYPE IN Supplier Type');
xxeis.eis_rsc_ins.rcnh( 'WC - Supplier List',200,'EAST.PARENT_SUPPLIER_NAME IN Parent Supplier','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','PARENT_SUPPLIER_NAME','','Parent Supplier','','','','','EIS_AP_SUPPLIERS_DET_V','','','','','','IN','Y','Y','','','','','1',200,'WC - Supplier List','EAST.PARENT_SUPPLIER_NAME IN Parent Supplier');
xxeis.eis_rsc_ins.rcnh( 'WC - Supplier List',200,'EAST.SUPPLIER_NUMBER IN Supplier Number','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','SUPPLIER_NUMBER','','Supplier Number','','','','','EIS_AP_SUPPLIERS_DET_V','','','','','','IN','Y','Y','','','','','1',200,'WC - Supplier List','EAST.SUPPLIER_NUMBER IN Supplier Number');
xxeis.eis_rsc_ins.rcnh( 'WC - Supplier List',200,'EAST.PAY_SITE IN Pay Sites Only?','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','PAY_SITE','','Pay Sites Only?','','','','','EIS_AP_SUPPLIERS_DET_V','','','','','','IN','Y','Y','','','','','1',200,'WC - Supplier List','EAST.PAY_SITE IN Pay Sites Only?');
xxeis.eis_rsc_ins.rcnh( 'WC - Supplier List',200,'EAST.SITE_CREATION_DATE >= Site Creation Date From','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','SITE_CREATION_DATE','','Site Creation Date From','','','','','EIS_AP_SUPPLIERS_DET_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',200,'WC - Supplier List','EAST.SITE_CREATION_DATE >= Site Creation Date From');
xxeis.eis_rsc_ins.rcnh( 'WC - Supplier List',200,'EAST.SITE_CREATION_DATE <= Site Creation Date To','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','SITE_CREATION_DATE','','Site Creation Date To','','','','','EIS_AP_SUPPLIERS_DET_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',200,'WC - Supplier List','EAST.SITE_CREATION_DATE <= Site Creation Date To');
--Inserting Report Sorts - WC - Supplier List
xxeis.eis_rsc_ins.rs( 'WC - Supplier List',200,'SUPPLIER_NAME','ASC','10012196','1','');
xxeis.eis_rsc_ins.rs( 'WC - Supplier List',200,'SUPPLIER_SITE_NAME','ASC','10012196','2','');
--Inserting Report Triggers - WC - Supplier List
--inserting report templates - WC - Supplier List
xxeis.eis_rsc_ins.r_tem( 'WC - Supplier List','Supplier List','Seeded template for Supplier List','','','','','','','','','','','Supplier List.rtf','10012196','X','','','Y','Y','','');
--Inserting Report Portals - WC - Supplier List
--inserting report dashboards - WC - Supplier List
xxeis.eis_rsc_ins.R_dash( 'WC - Supplier List','Suppliers Report','Suppliers Report','pie','large','Supplier Type','Supplier Type','','Supplier Number','Count','10012196');
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'WC - Supplier List','200','EIS_AP_SUPPLIERS_DET_V','EIS_AP_SUPPLIERS_DET_V','N','');
xxeis.eis_rsc_ins.rviews( 'WC - Supplier List','200','EIS_AP_SUPPLIERS_DET_V','AP_SUPPLIERS','N','POV');
xxeis.eis_rsc_ins.rviews( 'WC - Supplier List','200','EIS_AP_SUPPLIERS_DET_V','PO_VENDOR_SITES','N','POVS');
--inserting report security - WC - Supplier List
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','200','','XXWC_PAY_NO_CALENDAR',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','200','','XXWC_PAY_W_CALENDAR',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','200','','XXWC_PAY_DISBURSE',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','200','','XXWC_PAYABLES_INQUIRY',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','401','','XXWC_AO_BIN_MTN',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','401','','XXWC_AO_BIN_MTN_OEENTRY',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','401','','XXWC_AO_BIN_MTN_PO_RPT',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','401','','XXWC_AO_BIN_MTN_REC',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','401','','XXWC_AO_BIN_MTN_CYCLE_INV_ADJ',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','401','','XXWC_AO_BIN_MTN_CYCLE_OEENTRY',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','401','','XXWC_AO_BIN_MTN_CYCLE_REC',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','222','','XXWC_AO_CASHIER',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','401','','XXWC_AO_CYCLE_COUNT',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','401','','XXWC_AO_INV_ADJ_OEENTRY',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','401','','XXWC_AO_INV_ADJ_PO_RPT',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','401','','XXWC_AO_INV_ADJ_REC',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','401','','XXWC_AO_INV_ADJ',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','660','','XXWC_AO_OEENTRY_PO_RPT',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','660','','XXWC_AO_OEENTRY_REC',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','660','','XXWC_AO_OEENTRY',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','201','','XXWC_AO_PO_AUTOCREATE',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','201','','XXWC_AO_POENTRY_REPORTS',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','401','','XXWC_AO_REC',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','702','','XXWC_BILLS_OF_MATERIAL_WC',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','260','','HDS+_CMNGNT_WC',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','260','','HDS_CSH_MNGMNET_SPR_WC',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','702','','XXWC_COST_MANAGEMENT_INQUIRY',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','401','','XXWC_COUNT_SETUP',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','222','','XXWC_CRE_ASSOC_CASH_APP_MGR',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','222','','XXWC_CRE_ASSOC_CASH_APP',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','222','','XXWC_CRE_ASSOC_COLLECTIONS',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','222','','XXWC_CRE_ASSOC_CUST_MAINT',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','222','','XXWC_CRE_CUST_IFACE',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','222','','XXWC_CRE_CREDIT_COLL_MGR',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','222','','XXWC_CRE_CREDIT_COLL_MGR_NOREC',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','222','','XXWC_AR_IRECEIVABLES',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','222','','XXWC_CUSTOMER_MAINTENANCE',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','401','','XXWC_DATA_MNGT_SC',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','512','','XXWC_DEPOT_REPAIR_WC',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','175','','XXWC_EDI_SUPER_USER',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','175','','XXWC_EDI_SUPP',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','660','','XXWC_FAB_USER',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','140','','XXCUS_FA_ACCOUNTANT_WC',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','140','','FA_WC_INQR',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','140','','XXWC_ASSETS_INQUIRY_WC',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','101','','XXWC_GL_SETUP',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','401','','XXWC_INV_ACCOUNTANT',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','401','','XXWC_INVENTORY_CONTROL_INQUIRY',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','401','','XXWC_INVENTORY_CONTROL_SR_MGR',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','401','','XXWC_INV_PLANNER',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','401','','XXWC_INVENTORY_SPEC_SCC',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','401','','XXWC_INVENTORY_SUPER_USER',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','20005','','XXWC_IT_FUNC_CONFIGURATOR',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','20005','','XXWC_IT_SECURITY_ADMINISTRATOR',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','704','','XXWC_MATERIAL_PLANNER_WC',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','660','','XXWC_ORDER_MGMT_PRICING_FULL',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','660','','XXWC_ORDER_MGMT_PRICING_LTD',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','660','','XXWC_ORDER_MGMT_PRICING_STD',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','660','','XXWC_ORDER_MGMT_PRICING_SUPER',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','660','','XXWC_ORDER_MGMT_READ_ONLY',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','660','','XXWC_ORDER_MGMT_READ_ONLY_COST',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','660','','XXWC_ORDER_MGMT_READ_SHIPPING',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','200','','XXWC_PAY_MANAGER',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','200','','XXWC_PAY_VENDOR_BANK_DETAILS',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','200','','XXWC_PAY_VENDOR_MSTR',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','200','','XXWC_PAY_VENDOR_MSTR_INQUIRY',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','661','','XXWC_PRICING_MANAGER',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','20005','','XXWC_RESOURCES',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','201','','XXWC_PURCHASING_BUYER',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','201','','XXWC_PURCHASING_INQUIRY',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','201','','XXWC_PURCHASING_MGR',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','201','','XXWC_PURCHASING_SR_MRG_WC',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','201','','XXWC_PUR_SUPER_USER',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','222','','XXWC_RECEIVABLES_INQUIRY_WC',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','1','','XXWC_RECEIVABLES_TAX',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','401','','XXWC_RECEIVING_ASSOCIATE',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','660','','XXWC_RENTAL_OM_PRICING_SUPER',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','20005','','XXWC_SALES_SUPPORT_ADMIN',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','201','','XXWC_SUPP_MAINT_WC',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','20005','','XXWC_VIEW_ALL_EIS_REPORTS',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Supplier List','706','','XXWC_WORK_IN_PROCESS_WC',200,'10012196','','','');
--Inserting Report Pivots - WC - Supplier List
xxeis.eis_rsc_ins.rpivot( 'WC - Supplier List',200,'Pivot','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rsc_ins.rpivot_dtls( 'WC - Supplier List',200,'Pivot','OPERATING_UNIT','ROW_FIELD','','','1','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'WC - Supplier List',200,'Pivot','SUPPLIER_SITE_NAME','DATA_FIELD','COUNT','Count Of Supplier Sites','1','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'WC - Supplier List',200,'Pivot','SUPPLIER_NAME','ROW_FIELD','','','3','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'WC - Supplier List',200,'Pivot','SUPPLIER_TYPE','ROW_FIELD','','','2','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'WC - Supplier List',200,'Pivot','ALTERNATE_NAME','PAGE_FIELD','','','','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'WC - Supplier List',200,'Pivot','TAXPAYER_ID','PAGE_FIELD','','','','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'WC - Supplier List',200,'Pivot','TAX_REGISTRATION_NUMBER','PAGE_FIELD','','','','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'WC - Supplier List',200,'Pivot','PARENT_SUPPLIER_NAME','PAGE_FIELD','','','','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'WC - Supplier List',200,'Pivot','ONE_TIME','PAGE_FIELD','','','','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'WC - Supplier List',200,'Pivot','CREDIT_LIMIT','PAGE_FIELD','','','','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'WC - Supplier List',200,'Pivot','WITHHOLDING_STATUS','PAGE_FIELD','','','','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'WC - Supplier List',200,'Pivot','INSPECTION_REQUIRED','PAGE_FIELD','','','','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'WC - Supplier List',200,'Pivot','RECEIPT_REQUIRED','PAGE_FIELD','','','','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'WC - Supplier List',200,'Pivot','INACTIVE_ON','PAGE_FIELD','','','','1','xlNormal');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
--Inserting Report   Version details- WC - Supplier List
xxeis.eis_rsc_ins.rv( 'WC - Supplier List','','WC - Supplier List','SA059956');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
