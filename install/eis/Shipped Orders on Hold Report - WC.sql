--Report Name            : Shipped Orders on Hold Report - WC
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data EIS_XXWC_OM_SHIP_ORD_HOLD_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_OM_SHIP_ORD_HOLD_V
xxeis.eis_rsc_ins.v( 'EIS_XXWC_OM_SHIP_ORD_HOLD_V',660,'','','','','SA059956','XXEIS','EIS Xxwc Om Ship Ord Hold V','EXOSOHV','','','VIEW','US','','EIS_WC_OM_ORD_HLD_V','');
--Delete Object Columns for EIS_XXWC_OM_SHIP_ORD_HOLD_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_OM_SHIP_ORD_HOLD_V',660,FALSE);
--Inserting Object Columns for EIS_XXWC_OM_SHIP_ORD_HOLD_V
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_SHIP_ORD_HOLD_V','PART_DESCRIPTION',660,'Part Description','PART_DESCRIPTION','','','','SA059956','VARCHAR2','','','Part Description','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_SHIP_ORD_HOLD_V','PART_NUMBER',660,'Part Number','PART_NUMBER','','','','SA059956','VARCHAR2','','','Part Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_SHIP_ORD_HOLD_V','HOLD_DATE',660,'Hold Date','HOLD_DATE','','','','SA059956','DATE','','','Hold Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_SHIP_ORD_HOLD_V','ORDER_LINE_HOLD_NAME',660,'Order Line Hold Name','ORDER_LINE_HOLD_NAME','','','','SA059956','VARCHAR2','','','Order Line Hold Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_SHIP_ORD_HOLD_V','ORDER_LINE_HOLD',660,'Order Line Hold','ORDER_LINE_HOLD','','','','SA059956','VARCHAR2','','','Order Line Hold','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_SHIP_ORD_HOLD_V','ORDER_HEADER_HOLD_NAME',660,'Order Header Hold Name','ORDER_HEADER_HOLD_NAME','','','','SA059956','VARCHAR2','','','Order Header Hold Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_SHIP_ORD_HOLD_V','ORDER_HEADER_HOLD',660,'Order Header Hold','ORDER_HEADER_HOLD','','','','SA059956','VARCHAR2','','','Order Header Hold','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_SHIP_ORD_HOLD_V','ORDER_LINE_NUMBER',660,'Order Line Number','ORDER_LINE_NUMBER','','','','SA059956','VARCHAR2','','','Order Line Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_SHIP_ORD_HOLD_V','ORDER_LINE_STATUS',660,'Order Line Status','ORDER_LINE_STATUS','','','','SA059956','VARCHAR2','','','Order Line Status','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_SHIP_ORD_HOLD_V','ORDERED_DATE',660,'Ordered Date','ORDERED_DATE','','','','SA059956','DATE','','','Ordered Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_SHIP_ORD_HOLD_V','ORDER_TYPE',660,'Order Type','ORDER_TYPE','','','','SA059956','VARCHAR2','','','Order Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_SHIP_ORD_HOLD_V','ORDER_HEADER_STATUS',660,'Order Header Status','ORDER_HEADER_STATUS','','','','SA059956','VARCHAR2','','','Order Header Status','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_SHIP_ORD_HOLD_V','CREATED_BY',660,'Created By','CREATED_BY','','','','SA059956','VARCHAR2','','','Created By','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_SHIP_ORD_HOLD_V','SALES_ORDER_NUMBER',660,'Sales Order Number','SALES_ORDER_NUMBER','','','','SA059956','NUMBER','','','Sales Order Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_SHIP_ORD_HOLD_V','LOC',660,'Loc','LOC','','','','SA059956','VARCHAR2','','','Loc','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_SHIP_ORD_HOLD_V','CUSTOMER_NAME',660,'Customer Name','CUSTOMER_NAME','','','','SA059956','VARCHAR2','','','Customer Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_SHIP_ORD_HOLD_V','CUSTOMER_NUMBER',660,'Customer Number','CUSTOMER_NUMBER','','','','SA059956','VARCHAR2','','','Customer Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_SHIP_ORD_HOLD_V','HOLD_BY_ASSOCIATE_NAME',660,'Hold By Associate Name','HOLD_BY_ASSOCIATE_NAME','','','','SA059956','VARCHAR2','','','Hold By Associate Name','','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_SHIP_ORD_HOLD_V','HOLD_BY_NTID',660,'Hold By Ntid','HOLD_BY_NTID','','','','SA059956','VARCHAR2','','','Hold By Ntid','','','','','');
--Inserting Object Components for EIS_XXWC_OM_SHIP_ORD_HOLD_V
--Inserting Object Component Joins for EIS_XXWC_OM_SHIP_ORD_HOLD_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
prompt Creating Report LOV Data for Shipped Orders on Hold Report - WC
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - Shipped Orders on Hold Report - WC
xxeis.eis_rsc_ins.lov( 660,'select distinct ott.name order_type,ott.description description,ott.transaction_type_id order_type_id from oe_transaction_types_tl ott','','OM ORDER TYPE','This gives the Order Type','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'SELECT ORGANIZATION_CODE WAREHOUSE,ORGANIZATION_NAME FROM ORG_ORGANIZATION_DEFINITIONS OOD WHERE SYSDATE < NVL(OOD.DISABLE_DATE,SYSDATE+1) AND EXISTS (SELECT 1 FROM XXEIS.EIS_ORG_ACCESS_V  WHERE organization_id = ood.organization_id )
UNION
SELECT ''All'', ''All Organizations'' from Dual','','OM Warehouse All','','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( '','select ohd.name from oe_hold_definitions ohd order by 1','','XXWC OM HOLD TYPE','XXWC OM HOLD TYPE for Order on holds report','ANONYMOUS',NULL,'N','','','N','N','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
prompt Creating Report Data for Shipped Orders on Hold Report - WC
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Shipped Orders on Hold Report - WC
xxeis.eis_rsc_utility.delete_report_rows( 'Shipped Orders on Hold Report - WC',660 );
--Inserting Report - Shipped Orders on Hold Report - WC
xxeis.eis_rsc_ins.r( 660,'Shipped Orders on Hold Report - WC','','Order Holds and Not Invoiced Report','','','','SA059956','EIS_XXWC_OM_SHIP_ORD_HOLD_V','Y','','','SA059956','','N','White Cap Reports','','CSV,EXCEL,','N','','','','','','N','','US','','Orders on Hold Report - WC','','','','','','','','','','','','','','');
--Inserting Report Columns - Shipped Orders on Hold Report - WC
xxeis.eis_rsc_ins.rc( 'Shipped Orders on Hold Report - WC',660,'CREATED_BY','Created By','Created By','','','default','','6','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_SHIP_ORD_HOLD_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Shipped Orders on Hold Report - WC',660,'CUSTOMER_NAME','Customer Name','Customer Name','','','default','','5','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_SHIP_ORD_HOLD_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Shipped Orders on Hold Report - WC',660,'CUSTOMER_NUMBER','Customer Number','Customer Number','','','default','','4','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_SHIP_ORD_HOLD_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Shipped Orders on Hold Report - WC',660,'LOC','Loc','Loc','','','default','','1','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_SHIP_ORD_HOLD_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Shipped Orders on Hold Report - WC',660,'ORDERED_DATE','Ordered Date','Ordered Date','','','default','','3','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_SHIP_ORD_HOLD_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Shipped Orders on Hold Report - WC',660,'ORDER_HEADER_HOLD','Order Header Hold','Order Header Hold','','','default','','10','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_SHIP_ORD_HOLD_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Shipped Orders on Hold Report - WC',660,'ORDER_LINE_HOLD','Order Line Hold','Order Line Hold','','','default','','12','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_SHIP_ORD_HOLD_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Shipped Orders on Hold Report - WC',660,'ORDER_LINE_HOLD_NAME','Order Line Hold Name','Order Line Hold Name','','','default','','13','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_SHIP_ORD_HOLD_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Shipped Orders on Hold Report - WC',660,'ORDER_LINE_NUMBER','Order Line Number','Order Line Number','','','default','','9','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_SHIP_ORD_HOLD_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Shipped Orders on Hold Report - WC',660,'ORDER_TYPE','Order Type','Order Type','','','default','','7','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_SHIP_ORD_HOLD_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Shipped Orders on Hold Report - WC',660,'PART_DESCRIPTION','Part Description','Part Description','','','default','','16','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_SHIP_ORD_HOLD_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Shipped Orders on Hold Report - WC',660,'PART_NUMBER','Part Number','Part Number','','','default','','15','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_SHIP_ORD_HOLD_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Shipped Orders on Hold Report - WC',660,'SALES_ORDER_NUMBER','Sales Order Number','Sales Order Number','','~~~','default','','2','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_SHIP_ORD_HOLD_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Shipped Orders on Hold Report - WC',660,'HOLD_DATE','Hold Date','Hold Date','','','default','','14','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_SHIP_ORD_HOLD_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Shipped Orders on Hold Report - WC',660,'HOLD_BY_ASSOCIATE_NAME','Hold By Associate Name','Hold By Associate Name','','','default','','17','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_SHIP_ORD_HOLD_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Shipped Orders on Hold Report - WC',660,'HOLD_BY_NTID','Hold By NTID','Hold By Ntid','','','default','','18','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_SHIP_ORD_HOLD_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Shipped Orders on Hold Report - WC',660,'ORDER_HEADER_HOLD_NAME','Order Header Hold Name','Order Header Hold Name','','','default','','11','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_SHIP_ORD_HOLD_V','','','','US','','');
--Inserting Report Parameters - Shipped Orders on Hold Report - WC
xxeis.eis_rsc_ins.rp( 'Shipped Orders on Hold Report - WC',660,'Warehouse','Warehouse','LOC','IN','OM Warehouse All','','VARCHAR2','N','Y','1','Y','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_OM_SHIP_ORD_HOLD_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Shipped Orders on Hold Report - WC',660,'Ordered Date From','Ordered Date From','ORDERED_DATE','>=','','','DATE','N','Y','2','Y','N','CONSTANT','SA059956','Y','N','','Start Date','','EIS_XXWC_OM_SHIP_ORD_HOLD_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Shipped Orders on Hold Report - WC',660,'Ordered Date To','Ordered Date To','ORDERED_DATE','<=','','','DATE','N','Y','3','Y','N','CONSTANT','SA059956','Y','N','','End Date','','EIS_XXWC_OM_SHIP_ORD_HOLD_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Shipped Orders on Hold Report - WC',660,'Order Type','Order Type','ORDER_TYPE','IN','OM ORDER TYPE','','VARCHAR2','N','Y','4','Y','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_OM_SHIP_ORD_HOLD_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Shipped Orders on Hold Report - WC',660,'Hold Type','Hold Type','ORDER_HEADER_HOLD_NAME','IN','XXWC OM HOLD TYPE','','VARCHAR2','N','Y','5','Y','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_OM_SHIP_ORD_HOLD_V','','','US','');
--Inserting Dependent Parameters - Shipped Orders on Hold Report - WC
--Inserting Report Conditions - Shipped Orders on Hold Report - WC
--Inserting Report Sorts - Shipped Orders on Hold Report - WC
xxeis.eis_rsc_ins.rs( 'Shipped Orders on Hold Report - WC',660,'ORDERED_DATE','ASC','SA059956','1','');
xxeis.eis_rsc_ins.rs( 'Shipped Orders on Hold Report - WC',660,'SALES_ORDER_NUMBER','ASC','SA059956','2','');
--Inserting Report Triggers - Shipped Orders on Hold Report - WC
xxeis.eis_rsc_ins.rt( 'Shipped Orders on Hold Report - WC',660,'  BEGIN
   XXEIS.EIS_XXWC_OM_SHIP_ORD_HOLD_PKG.ORDER_HOLD_PROC (p_warehouse  	=> :Warehouse,
									p_order_date_from 	=> :Ordered Date From,
									p_order_date_to  	=> :Ordered Date To,
									P_ORDER_TYPE     	=> :Order Type,
									P_HOLD_TYPE      	=> :Hold Type);
   END;
','B','Y','SA059956','BQ');
--inserting report templates - Shipped Orders on Hold Report - WC
--Inserting Report Portals - Shipped Orders on Hold Report - WC
--inserting report dashboards - Shipped Orders on Hold Report - WC
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Shipped Orders on Hold Report - WC','660','EIS_XXWC_OM_SHIP_ORD_HOLD_V','EIS_XXWC_OM_SHIP_ORD_HOLD_V','N','');
--inserting report security - Shipped Orders on Hold Report - WC
xxeis.eis_rsc_ins.rsec( 'Shipped Orders on Hold Report - WC','660','','XXWC_AO_OEENTRY',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Shipped Orders on Hold Report - WC','660','','XXWC_AO_OEENTRY_REC',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Shipped Orders on Hold Report - WC','660','','XXWC_ORDER_MGMT_SUPER_USER',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Shipped Orders on Hold Report - WC','660','','XXWC_ORDER_MGMT_READ_SHIPPING',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Shipped Orders on Hold Report - WC','660','','XXWC_ORDER_MGMT_READ_ONLY_COST',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Shipped Orders on Hold Report - WC','660','','XXWC_ORDER_MGMT_READ_ONLY',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Shipped Orders on Hold Report - WC','660','','XXWC_ORDER_MGMT_PRICING_SUPER',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Shipped Orders on Hold Report - WC','660','','XXWC_ORDER_MGMT_PRICING_STD',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Shipped Orders on Hold Report - WC','660','','XXWC_ORDER_MGMT_PRICING_LTD',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Shipped Orders on Hold Report - WC','20005','','XXWC_VIEW_ALL_EIS_REPORTS',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Shipped Orders on Hold Report - WC','660','','XXWC_RENTAL_OM_PRICING_SUPER',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Shipped Orders on Hold Report - WC','660','','XXWC_ORDER_MGMT_PRICING_FULL',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Shipped Orders on Hold Report - WC','660','','XXWC_AO_OEENTRY_PO_RPT',660,'SA059956','','','');
--Inserting Report Pivots - Shipped Orders on Hold Report - WC
--Inserting Report Distribution Details 
--Inserting Report Distribution Details 
--Inserting Report SubTotals Details 
--Inserting Report aggregations 
--Inserting Report   Version details- Shipped Orders on Hold Report - WC
xxeis.eis_rsc_ins.rv( 'Shipped Orders on Hold Report - WC','','Shipped Orders on Hold Report - WC','SA059956','16-JAN-2018');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
