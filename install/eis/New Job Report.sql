--Report Name            : New Job Report
--Import Instructions : 1. If RTF template needs to be uploaded to report automatically then move the RTF template to physical path of APPS_DATA_FILE_DIR oracle directory. Check dba_directories view to get the physical path of oracle directory. 
--                         (If you want to use any other oracle directory other than APPS_DATA_FILE_DIR then replace APPS_DATA_FILE_DIR with your oracle directory name below.)
--                      2. Make sure XXEIS schema has read/write permissions to above used oracle directory and give full permissions to the directory at OS level.
--                      3. Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
exec xxeis.eis_rsc_ins.g_rtf_dir := 'APPS_DATA_FILE_DIR' ;
prompt Creating Object Data XXEIS_AR_CUST_LIST_SUMMARY_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(222);
IF mod_exist = 'Y' THEN 
--Inserting Object XXEIS_AR_CUST_LIST_SUMMARY_V
xxeis.eis_rsc_ins.v( 'XXEIS_AR_CUST_LIST_SUMMARY_V',222,'This view shows a summary of customer records including contact information.','','','','XXEIS_RS_ADMIN','XXEIS','XXEIS AR Customer List Summary','XEACLSV','','','VIEW','US','','');
--Delete Object Columns for XXEIS_AR_CUST_LIST_SUMMARY_V
xxeis.eis_rsc_utility.delete_view_rows('XXEIS_AR_CUST_LIST_SUMMARY_V',222,FALSE);
--Inserting Object Columns for XXEIS_AR_CUST_LIST_SUMMARY_V
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','CUSTOMER_NAME',222,'Customer Name','CUSTOMER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_PARTIES','PARTY_NAME','Customer Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','SITE_USE',222,'Site Use','SITE_USE','','','','XXEIS_RS_ADMIN','VARCHAR2','FND_LOOKUP_VALUES','MEANING','Site Use','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','CITY',222,'City','CITY','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_LOCATIONS','CITY','City','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','STATE',222,'State','STATE','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_LOCATIONS','STATE','State','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','ZIP_CODE',222,'Zip Code','ZIP_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_LOCATIONS','POSTAL_CODE','Zip Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','PARTY_SITE_NUMBER',222,'Party Site Number','PARTY_SITE_NUMBER~PARTY_SITE_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_PARTY_SITES','PARTY_SITE_NUMBER','Party Site Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','CUST#PRISM_NUMBER',222,'Descriptive flexfield: Customer Information Column Name: Prism Number','CUST#Prism_Number','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE6','Cust#Prism Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','ACCT_SITE#TAX_EXEMPTION_TYPE',222,'Descriptive flexfield: Address Information Column Name: Tax Exemption Type','ACCT_SITE#Tax_Exemption_Type','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_CUST_ACCT_SITES_ALL','ATTRIBUTE15','Acct Site#Tax Exemption Type','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','ACCT_SITE#TAX_EXEMPT1',222,'Descriptive flexfield: Address Information Column Name: Tax Exempt','ACCT_SITE#Tax_Exempt1','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_CUST_ACCT_SITES_ALL','ATTRIBUTE16','Acct Site#Tax Exempt','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','ACCT_SITE#PRISM_NUMBER',222,'Descriptive flexfield: Address Information Column Name: PRISM Number','ACCT_SITE#PRISM_Number','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_CUST_ACCT_SITES_ALL','ATTRIBUTE17','Acct Site#Prism Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','CUSTOMER_SITE_CLAS',222,'Customer Site Clas','CUSTOMER_SITE_CLAS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Site Clas','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','FUNCTIONAL_CURRENCY',222,'Functional Currency','FUNCTIONAL_CURRENCY','','','','XXEIS_RS_ADMIN','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','Functional Currency','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','FUNCTIONAL_CURRENCY_PRECISION',222,'Functional Currency Precision','FUNCTIONAL_CURRENCY_PRECISION','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','CALCULATION COLUMN','CALCULATION COLUMN','Functional Currency Precision','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','CUSTOMER_NUMBER',222,'Customer Number','CUSTOMER_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_CUST_ACCOUNTS','ACCOUNT_NUMBER','Customer Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','ADDRESS_ID',222,'Address Id','ADDRESS_ID','','','','XXEIS_RS_ADMIN','NUMBER','HZ_CUST_ACCT_SITES_ALL','CUST_ACCT_SITE_ID','Address Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','ADDRESS_LINE_1',222,'Address Line 1','ADDRESS_LINE_1','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_LOCATIONS','ADDRESS1','Address Line 1','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','CUST_STATUS',222,'Cust Status','CUST_STATUS','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_CUST_ACCOUNTS','STATUS','Cust Status','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','CUST_ACCOUNT_ID',222,'Cust Account Id','CUST_ACCOUNT_ID~CUST_ACCOUNT_ID','','','','XXEIS_RS_ADMIN','NUMBER','HZ_CUST_ACCOUNTS','CUST_ACCOUNT_ID','Cust Account Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','CUST_ACCT_SITE_ID',222,'Cust Acct Site Id','CUST_ACCT_SITE_ID~CUST_ACCT_SITE_ID','','','','XXEIS_RS_ADMIN','NUMBER','HZ_CUST_ACCT_SITES_ALL','CUST_ACCT_SITE_ID','Cust Acct Site Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','LOCATION_ID',222,'Unique identifier for this location','LOCATION_ID~LOCATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','HZ_LOCATIONS','LOCATION_ID','Location Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','PARTY_SITE_ID',222,'Party Site Id','PARTY_SITE_ID~PARTY_SITE_ID','','','','XXEIS_RS_ADMIN','NUMBER','HZ_PARTY_SITES','PARTY_SITE_ID','Party Site Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','SITE_USE_ID',222,'Site use identifier','SITE_USE_ID~SITE_USE_ID','','','','XXEIS_RS_ADMIN','NUMBER','HZ_CUST_SITE_USES_ALL','SITE_USE_ID','Site Use Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','COMPANY_NAME',222,'Company Name','COMPANY_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_SETS_OF_BOOKS','NAME','Company Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','COA_ID',222,'Coa Id','COA_ID~COA_ID','','','','XXEIS_RS_ADMIN','NUMBER','GL_SETS_OF_BOOKS','CHART_OF_ACCOUNTS_ID','Coa Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','CUSTOMER_KEY',222,'Customer Key','CUSTOMER_KEY~CUSTOMER_KEY','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_PARTIES','CUSTOMER_KEY','Customer Key','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','CUSTOMER_TYPE',222,'Customer Type','CUSTOMER_TYPE~CUSTOMER_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_CUST_ACCOUNTS','CUSTOMER_TYPE','Customer Type','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','EMPLOYEES_TOTAL',222,'Employees Total','EMPLOYEES_TOTAL~EMPLOYEES_TOTAL','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','HZ_PARTIES','EMPLOYEES_TOTAL','Employees Total','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','FISCAL_YEAREND_MONTH',222,'Fiscal Yearend Month','FISCAL_YEAREND_MONTH~FISCAL_YEAREND_MONTH','','','','XXEIS_RS_ADMIN','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','Fiscal Yearend Month','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','FREIGHT_TERM',222,'Freight Term','FREIGHT_TERM~FREIGHT_TERM','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_CUST_ACCOUNTS','FREIGHT_TERM','Freight Term','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','IDENTIFYING_ADDRESS_FLAG',222,'Identifying Address Flag','IDENTIFYING_ADDRESS_FLAG~IDENTIFYING_ADDRESS_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_PARTY_SITES','IDENTIFYING_ADDRESS_FLAG','Identifying Address Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','MISSION_STATEMENT',222,'Mission Statement','MISSION_STATEMENT~MISSION_STATEMENT','','','','XXEIS_RS_ADMIN','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','Mission Statement','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','ORIG_SYSTEM_REFERENCE',222,'Orig System Reference','ORIG_SYSTEM_REFERENCE~ORIG_SYSTEM_REFERENCE','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_CUST_ACCOUNTS','ORIG_SYSTEM_REFERENCE','Orig System Reference','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','PARTY_ID',222,'Party Id','PARTY_ID~PARTY_ID','','','','XXEIS_RS_ADMIN','NUMBER','HZ_PARTIES','PARTY_ID','Party Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','PARTY_NUMBER',222,'Party Number','PARTY_NUMBER~PARTY_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_PARTIES','PARTY_NUMBER','Party Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','PARTY_TYPE',222,'Party Type','PARTY_TYPE~PARTY_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_PARTIES','PARTY_TYPE','Party Type','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','PERSON_FIRST_NAME',222,'Person First Name','PERSON_FIRST_NAME~PERSON_FIRST_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_PARTIES','PERSON_FIRST_NAME','Person First Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','PERSON_LAST_NAME',222,'Person Last Name','PERSON_LAST_NAME~PERSON_LAST_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_PARTIES','PERSON_LAST_NAME','Person Last Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','PERSON_MIDDLE_NAME',222,'Person Middle Name','PERSON_MIDDLE_NAME~PERSON_MIDDLE_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_PARTIES','PERSON_MIDDLE_NAME','Person Middle Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','PERSON_PRE_NAME_ADJUNCT',222,'Person Pre Name Adjunct','PERSON_PRE_NAME_ADJUNCT~PERSON_PRE_NAME_ADJUNCT','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_PARTIES','PERSON_PRE_NAME_ADJUNCT','Person Pre Name Adjunct','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','PERSON_SUFFIX',222,'Person Suffix','PERSON_SUFFIX~PERSON_SUFFIX','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_PARTIES','PERSON_NAME_SUFFIX','Person Suffix','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','POTENTIAL_REVENUE_CURR_FY',222,'Potential Revenue Curr Fy','POTENTIAL_REVENUE_CURR_FY~POTENTIAL_REVENUE_CURR_FY','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','CALCULATION COLUMN','CALCULATION COLUMN','Potential Revenue Curr Fy','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','POTENTIAL_REVENUE_NEXT_FY',222,'Potential Revenue Next Fy','POTENTIAL_REVENUE_NEXT_FY~POTENTIAL_REVENUE_NEXT_FY','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','CALCULATION COLUMN','CALCULATION COLUMN','Potential Revenue Next Fy','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','REFERENCE_USE_FLAG',222,'Reference Use Flag','REFERENCE_USE_FLAG~REFERENCE_USE_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_PARTIES','REFERENCE_USE_FLAG','Reference Use Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','SIC_CODE_TYPE',222,'Sic Code Type','SIC_CODE_TYPE~SIC_CODE_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','Sic Code Type','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','TAXPAYER_ID',222,'Taxpayer Id','TAXPAYER_ID~TAXPAYER_ID','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_PARTIES','JGZZ_FISCAL_CODE','Taxpayer Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','TAX_REFERENCE',222,'Tax Reference','TAX_REFERENCE~TAX_REFERENCE','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_PARTIES','TAX_REFERENCE','Tax Reference','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','THIRD_PARTY_FLAG',222,'Third Party Flag','THIRD_PARTY_FLAG~THIRD_PARTY_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_PARTIES','THIRD_PARTY_FLAG','Third Party Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','ANALYSIS_FY',222,'Analysis Fy','ANALYSIS_FY~ANALYSIS_FY','','','','XXEIS_RS_ADMIN','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','Analysis Fy','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','COMPETITOR_FLAG',222,'Competitor Flag','COMPETITOR_FLAG~COMPETITOR_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_PARTIES','COMPETITOR_FLAG','Competitor Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','CUSTOMER_CATEGORY_CODE',222,'Customer Category Code','CUSTOMER_CATEGORY_CODE~CUSTOMER_CATEGORY_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_PARTIES','CATEGORY_CODE','Customer Category Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','CUSTOMER_CLASS_CODE',222,'Customer Class Code','CUSTOMER_CLASS_CODE~CUSTOMER_CLASS_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_CUST_ACCOUNTS','CUSTOMER_CLASS_CODE','Customer Class Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','CUSTOMER_ID',222,'Customer Id','CUSTOMER_ID~CUSTOMER_ID','','','','XXEIS_RS_ADMIN','NUMBER','HZ_CUST_ACCOUNTS','CUST_ACCOUNT_ID','Customer Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','YEAR_ESTABLISHED',222,'Year Established','YEAR_ESTABLISHED~YEAR_ESTABLISHED','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','CALCULATION COLUMN','CALCULATION COLUMN','Year Established','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','CUST#PARTY_TYPE',222,'Descriptive flexfield: Customer Information Column Name: Party Type','CUST#Party_Type','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE1','Cust#Party Type','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','CUST#VNDR_CODE_AND_FRULOC',222,'Descriptive flexfield: Customer Information Column Name: Vndr Code and FRULOC','CUST#Vndr_Code_and_FRULOC','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE2','Cust#Vndr Code And Fruloc','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','CUST#BRANCH_DESCRIPTION',222,'Descriptive flexfield: Customer Information Column Name: Branch Description','CUST#Branch_Description','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE3','Cust#Branch Description','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','LOC#PAY_TO_VENDOR_ID',222,'Descriptive flexfield: TCA Location Information Column Name: Pay To Vendor ID','LOC#Pay_To_Vendor_ID','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_LOCATIONS','ATTRIBUTE1','Loc#Pay To Vendor Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','LOC#LOB',222,'Descriptive flexfield: TCA Location Information Column Name: LOB','LOC#LOB','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_LOCATIONS','ATTRIBUTE2','Loc#Lob','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','LOC#PAY_TO_VENDOR_CODE',222,'Descriptive flexfield: TCA Location Information Column Name: Pay To Vendor Code','LOC#Pay_To_Vendor_Code','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_LOCATIONS','ATTRIBUTE3','Loc#Pay To Vendor Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','PARTY#PARTY_TYPE',222,'Descriptive flexfield: Party Information Column Name: Party Type','PARTY#Party_Type','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_PARTIES','ATTRIBUTE1','Party#Party Type','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','PARTY_SITE#REBT_PAY_TO_VNDR_',222,'Descriptive flexfield: Party Site Information Column Name: Rebt Pay to Vndr ID','PARTY_SITE#Rebt_Pay_to_Vndr_','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_PARTY_SITES','ATTRIBUTE2','Party Site#Rebt Pay To Vndr Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','PARTY_SITE#REBT_LOB',222,'Descriptive flexfield: Party Site Information Column Name: Rebt LOB','PARTY_SITE#Rebt_LOB','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_PARTY_SITES','ATTRIBUTE3','Party Site#Rebt Lob','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','PARTY_SITE#REBT_VNDR_CODE',222,'Descriptive flexfield: Party Site Information Column Name: Rebt Vndr Code','PARTY_SITE#Rebt_Vndr_Code','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_PARTY_SITES','ATTRIBUTE4','Party Site#Rebt Vndr Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','PARTY_SITE#REBT_VNDR_FLAG',222,'Descriptive flexfield: Party Site Information Column Name: Rebt Vndr Flag','PARTY_SITE#Rebt_Vndr_Flag','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_PARTY_SITES','ATTRIBUTE5','Party Site#Rebt Vndr Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','PARTY_SITE#REBT_VNDR_TAX_ID_',222,'Descriptive flexfield: Party Site Information Column Name: Rebt Vndr Tax ID Nbr','PARTY_SITE#Rebt_Vndr_Tax_ID_','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_PARTY_SITES','ATTRIBUTE6','Party Site#Rebt Vndr Tax Id Nbr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','CUST#CUSTOMER_SOURCE',222,'Descriptive flexfield: Customer Information Column Name: Customer Source','CUST#Customer_Source','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE4','Cust#Customer Source','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','CUST#LEGAL_COLLECTION_INDICA',222,'Descriptive flexfield: Customer Information Column Name: Legal Collection Indicator','CUST#Legal_Collection_Indica','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE5','Cust#Legal Collection Indicator','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','CUST#YES#NOTE_LINE_#5',222,'Descriptive flexfield: Customer Information Column Name: Note Line #5 Context: Yes','CUST#Yes#Note_Line_#5','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE16','Cust#Yes#Note Line #5','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','CUST#YES#NOTE_LINE_#1',222,'Descriptive flexfield: Customer Information Column Name: Note Line #1 Context: Yes','CUST#Yes#Note_Line_#1','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE17','Cust#Yes#Note Line #1','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','CUST#YES#NOTE_LINE_#2',222,'Descriptive flexfield: Customer Information Column Name: Note Line #2 Context: Yes','CUST#Yes#Note_Line_#2','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE18','Cust#Yes#Note Line #2','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','CUST#YES#NOTE_LINE_#3',222,'Descriptive flexfield: Customer Information Column Name: Note Line #3 Context: Yes','CUST#Yes#Note_Line_#3','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE19','Cust#Yes#Note Line #3','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','CUST#YES#NOTE_LINE_#4',222,'Descriptive flexfield: Customer Information Column Name: Note Line #4 Context: Yes','CUST#Yes#Note_Line_#4','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE20','Cust#Yes#Note Line #4','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','ACCT_SITE#PRINT_PRICES_ON_OR',222,'Descriptive flexfield: Address Information Column Name: Print Prices on Order','ACCT_SITE#Print_Prices_on_Or','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_CUST_ACCT_SITES_ALL','ATTRIBUTE1','Acct Site#Print Prices On Order','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','ACCT_SITE#JOB_INFORMATION_ON',222,'Descriptive flexfield: Address Information Column Name: Job Information on File?','ACCT_SITE#Job_Information_on','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_CUST_ACCT_SITES_ALL','ATTRIBUTE14','Acct Site#Job Information On File?','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','ACCT_SITE#MANDATORY_PO_NUMBE',222,'Descriptive flexfield: Address Information Column Name: Mandatory PO Number','ACCT_SITE#Mandatory_PO_Numbe','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_CUST_ACCT_SITES_ALL','ATTRIBUTE3','Acct Site#Mandatory Po Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','ACCT_SITE#LIEN_RELEASE_DATE',222,'Descriptive flexfield: Address Information Column Name: Lien Release Date','ACCT_SITE#Lien_Release_Date','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_CUST_ACCT_SITES_ALL','ATTRIBUTE5','Acct Site#Lien Release Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','ACCT_SITE#YES#NOTICE_TO_OWNE',222,'Descriptive flexfield: Address Information Column Name: Notice to Owner-Job Total Context: Yes','ACCT_SITE#Yes#Notice_to_Owne','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_CUST_ACCT_SITES_ALL','ATTRIBUTE18','Acct Site#Yes#Notice To Owner-Job Total','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','ACCT_SITE#YES#NOTICE_TO_OWNE1',222,'Descriptive flexfield: Address Information Column Name: Notice to Owner-Prelim Notice Context: Yes','ACCT_SITE#Yes#Notice_to_Owne1','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_CUST_ACCT_SITES_ALL','ATTRIBUTE19','Acct Site#Yes#Notice To Owner-Prelim Notice','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','ACCT_SITE#YES#NOTICE_TO_OWNE2',222,'Descriptive flexfield: Address Information Column Name: Notice to Owner-Prelim Date Context: Yes','ACCT_SITE#Yes#Notice_to_Owne2','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_CUST_ACCT_SITES_ALL','ATTRIBUTE20','Acct Site#Yes#Notice To Owner-Prelim Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','SITE_USES#CUSTOMER_SITE_CLAS',222,'Descriptive flexfield: Site Use Information Column Name: Customer Site Classification','SITE_USES#Customer_Site_Clas','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_CUST_SITE_USES_ALL','ATTRIBUTE1','Site Uses#Customer Site Classification','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','SITE_USES#GOVERNMENT_FUNDED',222,'Descriptive flexfield: Site Use Information Column Name: Government Funded?','SITE_USES#Government_Funded','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_CUST_SITE_USES_ALL','ATTRIBUTE2','Site Uses#Government Funded?','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','SITE_USES#THOMAS_GUIDE_PAGE',222,'Descriptive flexfield: Site Use Information Column Name: Thomas Guide Page','SITE_USES#Thomas_Guide_Page','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_CUST_SITE_USES_ALL','ATTRIBUTE3','Site Uses#Thomas Guide Page','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','SITE_USES#DODGE_NUMBER',222,'Descriptive flexfield: Site Use Information Column Name: Dodge Number','SITE_USES#Dodge_Number','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_CUST_SITE_USES_ALL','ATTRIBUTE4','Site Uses#Dodge Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','SITE_USES#FUTURE_USE',222,'Descriptive flexfield: Site Use Information Column Name: FUTURE USE','SITE_USES#FUTURE_USE','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_CUST_SITE_USES_ALL','ATTRIBUTE5','Site Uses#Future Use','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','SITE_USES#SALESREP_#2',222,'Descriptive flexfield: Site Use Information Column Name: Salesrep #2','SITE_USES#Salesrep_#2','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_CUST_SITE_USES_ALL','ATTRIBUTE6','Site Uses#Salesrep #2','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','SITE_USES#SALESREP_SPILT_#1',222,'Descriptive flexfield: Site Use Information Column Name: Salesrep Spilt #1','SITE_USES#Salesrep_Spilt_#1','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_CUST_SITE_USES_ALL','ATTRIBUTE7','Site Uses#Salesrep Spilt #1','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','SITE_USES#SALESREP_SPILT_#2',222,'Descriptive flexfield: Site Use Information Column Name: Salesrep Spilt #2','SITE_USES#Salesrep_Spilt_#2','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_CUST_SITE_USES_ALL','ATTRIBUTE8','Site Uses#Salesrep Spilt #2','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','SITE_USES#PERMIT_NUMBER',222,'Descriptive flexfield: Site Use Information Column Name: Permit Number','SITE_USES#Permit_Number','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_CUST_SITE_USES_ALL','ATTRIBUTE9','Site Uses#Permit Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','FOB_POINT',222,'Fob Point','FOB_POINT~FOB_POINT','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_CUST_ACCOUNTS','FOB_POINT','Fob Point','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','ORDER_TYPE_ID',222,'Order Type Id','ORDER_TYPE_ID~ORDER_TYPE_ID','','','','XXEIS_RS_ADMIN','NUMBER','HZ_CUST_ACCOUNTS','ORDER_TYPE_ID','Order Type Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','PRICE_LIST_ID',222,'Price List Id','PRICE_LIST_ID~PRICE_LIST_ID','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','HZ_CUST_ACCOUNTS','PRICE_LIST_ID','Price List Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','SALES_CHANNEL_CODE',222,'Sales Channel Code','SALES_CHANNEL_CODE~SALES_CHANNEL_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_CUST_ACCOUNTS','SALES_CHANNEL_CODE','Sales Channel Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','SHIP_VIA',222,'Ship Via','SHIP_VIA~SHIP_VIA','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_CUST_ACCOUNTS','SHIP_VIA','Ship Via','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','TAX_CODE',222,'Tax Code','TAX_CODE~TAX_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_CUST_ACCOUNTS','TAX_CODE','Tax Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','WAREHOUSE_ID',222,'Warehouse Id','WAREHOUSE_ID~WAREHOUSE_ID','','','','XXEIS_RS_ADMIN','NUMBER','HZ_CUST_ACCOUNTS','WAREHOUSE_ID','Warehouse Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','ORG_ID',222,'Org Id','ORG_ID~ORG_ID','','','','XXEIS_RS_ADMIN','NUMBER','HZ_CUST_ACCT_SITES','ORG_ID','Org Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','ACCOUNT_NAME',222,'Account Name','ACCOUNT_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Account Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','PARTY_NAME',222,'Party Name','PARTY_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Party Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','OPERATING_UNIT',222,'Operating Unit','OPERATING_UNIT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Operating Unit','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','SITE_USES_TAX_CODE',222,'Site Uses Tax Code','SITE_USES_TAX_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Site Uses Tax Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','COPYRIGHT',222,'Copyright','COPYRIGHT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Copyright','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','ACCT_SITE#JOINT_CHECK_AGREEM',222,'Acct Site#Joint Check Agreem','ACCT_SITE#JOINT_CHECK_AGREEM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Acct Site#Joint Check Agreem','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','ACCT_SITE#OWNER_FINANCED',222,'Acct Site#Owner Financed','ACCT_SITE#OWNER_FINANCED','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Acct Site#Owner Financed','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','CUST#AUTHORIZED_BUYER_NOTES',222,'Cust#Authorized Buyer Notes','CUST#AUTHORIZED_BUYER_NOTES','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cust#Authorized Buyer Notes','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','CUST#AUTHORIZED_BUYER_REQUIR',222,'Cust#Authorized Buyer Requir','CUST#AUTHORIZED_BUYER_REQUIR','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cust#Authorized Buyer Requir','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','CUST#AUTO_APPLY_CREDIT_MEMO',222,'Cust#Auto Apply Credit Memo','CUST#AUTO_APPLY_CREDIT_MEMO','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cust#Auto Apply Credit Memo','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','CUST#LEGAL_PLACEMENT',222,'Cust#Legal Placement','CUST#LEGAL_PLACEMENT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cust#Legal Placement','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','CUST#PREDOMINANT_TRADE',222,'Cust#Predominant Trade','CUST#PREDOMINANT_TRADE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cust#Predominant Trade','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','PARTY#101#COLLECTOR',222,'Party#101#Collector','PARTY#101#COLLECTOR','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Party#101#Collector','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','PARTY#101#COOP_OVERRIDE',222,'Party#101#Coop Override','PARTY#101#COOP_OVERRIDE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Party#101#Coop Override','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','PARTY#101#DEFAULT_COOP_ACCOU',222,'Party#101#Default Coop Accou','PARTY#101#DEFAULT_COOP_ACCOU','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Party#101#Default Coop Accou','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','PARTY#101#DEFAULT_COST_CENTE',222,'Party#101#Default Cost Cente','PARTY#101#DEFAULT_COST_CENTE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Party#101#Default Cost Cente','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','PARTY#101#DEFAULT_LOCATION_S',222,'Party#101#Default Location S','PARTY#101#DEFAULT_LOCATION_S','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Party#101#Default Location S','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','PARTY#101#DEFAULT_PAYMENT_AC',222,'Party#101#Default Payment Ac','PARTY#101#DEFAULT_PAYMENT_AC','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Party#101#Default Payment Ac','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','PARTY#101#DEFAULT_PAYMENT_LO',222,'Party#101#Default Payment Lo','PARTY#101#DEFAULT_PAYMENT_LO','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Party#101#Default Payment Lo','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','PARTY#101#DEFAULT_PRODUCT_SE',222,'Party#101#Default Product Se','PARTY#101#DEFAULT_PRODUCT_SE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Party#101#Default Product Se','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','PARTY#101#DEFAULT_REBATE_ACC',222,'Party#101#Default Rebate Acc','PARTY#101#DEFAULT_REBATE_ACC','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Party#101#Default Rebate Acc','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','PARTY#101#INTERCOMPANY_RECEI',222,'Party#101#Intercompany Recei','PARTY#101#INTERCOMPANY_RECEI','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Party#101#Intercompany Recei','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','PARTY#101#INTERCOMPANY__PAYA',222,'Party#101#Intercompany  Paya','PARTY#101#INTERCOMPANY__PAYA','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Party#101#Intercompany  Paya','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','PARTY#101#PARTY_TYPE',222,'Party#101#Party Type','PARTY#101#PARTY_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Party#101#Party Type','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','PARTY#101#PAYMENT_OVERRIDE',222,'Party#101#Payment Override','PARTY#101#PAYMENT_OVERRIDE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Party#101#Payment Override','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','PARTY#102#COLLECTOR',222,'Party#102#Collector','PARTY#102#COLLECTOR','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Party#102#Collector','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','PARTY#102#COOP_OVERRIDE',222,'Party#102#Coop Override','PARTY#102#COOP_OVERRIDE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Party#102#Coop Override','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','PARTY#102#DEFAULT_COOP_ACCOU',222,'Party#102#Default Coop Accou','PARTY#102#DEFAULT_COOP_ACCOU','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Party#102#Default Coop Accou','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','PARTY#102#DEFAULT_COST_CENTE',222,'Party#102#Default Cost Cente','PARTY#102#DEFAULT_COST_CENTE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Party#102#Default Cost Cente','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','PARTY#102#DEFAULT_LOCATION_S',222,'Party#102#Default Location S','PARTY#102#DEFAULT_LOCATION_S','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Party#102#Default Location S','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','PARTY#102#DEFAULT_PAYMENT_AC',222,'Party#102#Default Payment Ac','PARTY#102#DEFAULT_PAYMENT_AC','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Party#102#Default Payment Ac','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','PARTY#102#DEFAULT_PAYMENT_LO',222,'Party#102#Default Payment Lo','PARTY#102#DEFAULT_PAYMENT_LO','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Party#102#Default Payment Lo','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','PARTY#102#DEFAULT_PRODUCT_SE',222,'Party#102#Default Product Se','PARTY#102#DEFAULT_PRODUCT_SE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Party#102#Default Product Se','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','PARTY#102#DEFAULT_REBATE_ACC',222,'Party#102#Default Rebate Acc','PARTY#102#DEFAULT_REBATE_ACC','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Party#102#Default Rebate Acc','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','PARTY#102#INTERCOMPANY_PAYAB',222,'Party#102#Intercompany Payab','PARTY#102#INTERCOMPANY_PAYAB','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Party#102#Intercompany Payab','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','PARTY#102#INTERCOMPANY_RECEI',222,'Party#102#Intercompany Recei','PARTY#102#INTERCOMPANY_RECEI','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Party#102#Intercompany Recei','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','PARTY#102#PARTY_TYPE',222,'Party#102#Party Type','PARTY#102#PARTY_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Party#102#Party Type','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','PARTY#102#PAYMENT_OVERRIDE',222,'Party#102#Payment Override','PARTY#102#PAYMENT_OVERRIDE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Party#102#Payment Override','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','SITE_USES#B2B_ADDRESS',222,'Site Uses#B2b Address','SITE_USES#B2B_ADDRESS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Site Uses#B2b Address','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','SITE_USES#B2B_SHIPPING_WAREH',222,'Site Uses#B2b Shipping Wareh','SITE_USES#B2B_SHIPPING_WAREH','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Site Uses#B2b Shipping Wareh','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','CREATED_BY',222,'Created By','CREATED_BY','','','','XXEIS_RS_ADMIN','NUMBER','','','Created By','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','CREATION_DATE',222,'Creation Date','CREATION_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Creation Date','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','PRIMARY_SALESREP_ID',222,'Primary Salesrep Id','PRIMARY_SALESREP_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Primary Salesrep Id','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','AUTHORIZED_BUYER_REQUIR',222,'Authorized Buyer Requir','AUTHORIZED_BUYER_REQUIR','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Authorized Buyer Requir','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_CUST_LIST_SUMMARY_V','CUST_NOTE_LINE',222,'Cust Note Line','CUST_NOTE_LINE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cust Note Line','','','','');
--Inserting Object Components for XXEIS_AR_CUST_LIST_SUMMARY_V
xxeis.eis_rsc_ins.vcomp( 'XXEIS_AR_CUST_LIST_SUMMARY_V','HZ_CUST_SITE_USES',222,'HZ_CUST_SITE_USES_ALL','SITE_USES','SITE_USES','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Customer Site Uses','','','','','',' ',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'XXEIS_AR_CUST_LIST_SUMMARY_V','HZ_CUST_ACCT_SITES',222,'HZ_CUST_ACCT_SITES_ALL','ACCT_SITE','ACCT_SITE','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Customer Account Sites','','','','','',' ',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'XXEIS_AR_CUST_LIST_SUMMARY_V','HZ_PARTY_SITES',222,'HZ_PARTY_SITES','PARTY_SITE','PARTY_SITE','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Party Sites','','','','','',' ',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'XXEIS_AR_CUST_LIST_SUMMARY_V','HZ_LOCATIONS',222,'HZ_LOCATIONS','LOC','LOC','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Locations','','','','','',' ',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'XXEIS_AR_CUST_LIST_SUMMARY_V','HZ_CUST_ACCOUNTS',222,'HZ_CUST_ACCOUNTS','CUST','CUST','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Customer Accounts','','','','','',' ',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'XXEIS_AR_CUST_LIST_SUMMARY_V','HZ_PARTIES',222,'HZ_PARTIES','PARTY','PARTY','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Parties','','','','','',' ',' ',' ',' ',' ',' ');
--Inserting Object Component Joins for XXEIS_AR_CUST_LIST_SUMMARY_V
xxeis.eis_rsc_ins.vcj( 'XXEIS_AR_CUST_LIST_SUMMARY_V','HZ_CUST_SITE_USES','SITE_USES',222,'XEACLSV.SITE_USE_ID','=','SITE_USES.SITE_USE_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'XXEIS_AR_CUST_LIST_SUMMARY_V','HZ_CUST_ACCT_SITES','ACCT_SITE',222,'XEACLSV.CUST_ACCT_SITE_ID','=','ACCT_SITE.CUST_ACCT_SITE_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'XXEIS_AR_CUST_LIST_SUMMARY_V','HZ_PARTY_SITES','PARTY_SITE',222,'XEACLSV.PARTY_SITE_ID','=','PARTY_SITE.PARTY_SITE_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'XXEIS_AR_CUST_LIST_SUMMARY_V','HZ_LOCATIONS','LOC',222,'XEACLSV.LOCATION_ID','=','LOC.LOCATION_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'XXEIS_AR_CUST_LIST_SUMMARY_V','HZ_CUST_ACCOUNTS','CUST',222,'XEACLSV.CUST_ACCOUNT_ID','=','CUST.CUST_ACCOUNT_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'XXEIS_AR_CUST_LIST_SUMMARY_V','HZ_PARTIES','PARTY',222,'XEACLSV.PARTY_ID','=','PARTY.PARTY_ID(+)','','','','Y','XXEIS_RS_ADMIN');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 222');
END IF;
END;
/
--Exporting View Component Data of the View -  XXEIS_AR_CUST_LIST_SUMMARY_V
prompt Creating Object Data HZ_CUST_SITE_USES
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(222);
IF mod_exist = 'Y' THEN 
--Inserting Object HZ_CUST_SITE_USES
xxeis.eis_rsc_ins.v( 'HZ_CUST_SITE_USES',222,'Stores business purposes assigned to customer account sites.','1.0','','','MM027735','APPS','Hz Cust Site Uses','HCSU','','','SYNONYM','US','','');
--Delete Object Columns for HZ_CUST_SITE_USES
xxeis.eis_rsc_utility.delete_view_rows('HZ_CUST_SITE_USES',222,FALSE);
--Inserting Object Columns for HZ_CUST_SITE_USES
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','LAST_UPDATE_DATE',222,'Standard Who column - date when a user last updated this row.','LAST_UPDATE_DATE','','','','MM027735','DATE','HZ_CUST_SITE_USES','LAST_UPDATE_DATE','Last Update Date','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','LAST_UPDATED_BY',222,'Standard who column - user who last updated this row (foreign key to FND_USER.USER_ID).','LAST_UPDATED_BY','','','','MM027735','NUMBER','HZ_CUST_SITE_USES','LAST_UPDATED_BY','Last Updated By','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','SITE_USE_CODE',222,'Business purpose assigned to customer site account, such as Bill-To, Market, and Statements.','SITE_USE_CODE','','','','MM027735','VARCHAR2','HZ_CUST_SITE_USES','SITE_USE_CODE','Site Use Code','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','PRIMARY_FLAG',222,'Indicates if this site is the primary site for this customer account. Y for the primary customer account site. N for other customer account sites.','PRIMARY_FLAG','','','','MM027735','VARCHAR2','HZ_CUST_SITE_USES','PRIMARY_FLAG','Primary Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','STATUS',222,'Site use status flag, Lookup code for the CODE_STATUS column.','STATUS','','','','MM027735','VARCHAR2','HZ_CUST_SITE_USES','STATUS','Status','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','LOCATION',222,'Site use identifier','LOCATION','','','','MM027735','VARCHAR2','HZ_CUST_SITE_USES','LOCATION','Location','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','LAST_UPDATE_LOGIN',222,'Standard who column - operating system login of user who last updated this row (foreign key to FND_LOGINS.LOGIN_ID).','LAST_UPDATE_LOGIN','','','','MM027735','NUMBER','HZ_CUST_SITE_USES','LAST_UPDATE_LOGIN','Last Update Login','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','SIC_CODE',222,'Standard Industry Classification (SIC) code','SIC_CODE','','','','MM027735','VARCHAR2','HZ_CUST_SITE_USES','SIC_CODE','Sic Code','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','GSA_INDICATOR',222,'Indicates if this is a US federal agency supported by the General Services Administration (GSA). Y for organizations served by the GSA, N for organizations that are not served by the GSA.','GSA_INDICATOR','','','','MM027735','VARCHAR2','HZ_CUST_SITE_USES','GSA_INDICATOR','Gsa Indicator','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','FREIGHT_TERM',222,'Order Management lookup code for FREIGHT_TERMS','FREIGHT_TERM','','','','MM027735','VARCHAR2','HZ_CUST_SITE_USES','FREIGHT_TERM','Freight Term','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','ATTRIBUTE10',222,'Descriptive flexfield segment','ATTRIBUTE10','','','','MM027735','VARCHAR2','HZ_CUST_SITE_USES','ATTRIBUTE10','Attribute10','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','ATTRIBUTE11',222,'Descriptive flexfield segment','ATTRIBUTE11','','','','MM027735','VARCHAR2','HZ_CUST_SITE_USES','ATTRIBUTE11','Attribute11','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','PRIMARY_SALESREP_ID',222,'Identifies a salesperson associated with a business site. Also used to default salesrep in the Transactions window. The hierarchy of defaulting would follow from Bill-To site to Ship-To site to Customer, if not a multi-org setup.','PRIMARY_SALESREP_ID','','','','MM027735','NUMBER','HZ_CUST_SITE_USES','PRIMARY_SALESREP_ID','Primary Salesrep Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','TAX_CLASSIFICATION',222,'Classifies Bill-To site to tax group codes. Foreign key to the AR_LOOKUPS table (AR_TAX_CLASSIFICATION) used in AR_VAT_TAX (TAX_CLASSIFICATION) indexes','TAX_CLASSIFICATION','','','','MM027735','VARCHAR2','HZ_CUST_SITE_USES','TAX_CLASSIFICATION','Tax Classification','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','ATTRIBUTE1',222,'DFF Context Code - Global Data Elements','ATTRIBUTE1','','','','MM027735','','HZ_CUST_SITE_USES','ATTRIBUTE1','Customer Site Classification','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','TAX_HEADER_LEVEL_FLAG',222,'Used by Oracle Sales Compensation','TAX_HEADER_LEVEL_FLAG','','','','MM027735','VARCHAR2','HZ_CUST_SITE_USES','TAX_HEADER_LEVEL_FLAG','Tax Header Level Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','TAX_ROUNDING_RULE',222,'Tax rounding rule','TAX_ROUNDING_RULE','','','','MM027735','VARCHAR2','HZ_CUST_SITE_USES','TAX_ROUNDING_RULE','Tax Rounding Rule','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','SITE_USE_ID',222,'Site use identifier','SITE_USE_ID','','','','MM027735','NUMBER','HZ_CUST_SITE_USES','SITE_USE_ID','Site Use Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','CUST_ACCT_SITE_ID',222,'Identifier for the customer account site. Foreign key to the HZ_CUST_ACCT_SITES_ALL table','CUST_ACCT_SITE_ID','','','','MM027735','NUMBER','HZ_CUST_SITE_USES','CUST_ACCT_SITE_ID','Cust Acct Site Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','CREATION_DATE',222,'Standard who column - date when this row was created.','CREATION_DATE','','','','MM027735','DATE','HZ_CUST_SITE_USES','CREATION_DATE','Creation Date','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','CREATED_BY',222,'Standard who column - user who created this row (foreign key to FND_USER.USER_ID).','CREATED_BY','','','','MM027735','NUMBER','HZ_CUST_SITE_USES','CREATED_BY','Created By','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','CONTACT_ID',222,'No longer used','CONTACT_ID','','','','MM027735','NUMBER','HZ_CUST_SITE_USES','CONTACT_ID','Contact Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','BILL_TO_SITE_USE_ID',222,'Bill-To site use identifier','BILL_TO_SITE_USE_ID','','','','MM027735','NUMBER','HZ_CUST_SITE_USES','BILL_TO_SITE_USE_ID','Bill To Site Use Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','ORIG_SYSTEM_REFERENCE',222,'Site use identifier from foreign or legacy system','ORIG_SYSTEM_REFERENCE','','','','MM027735','VARCHAR2','HZ_CUST_SITE_USES','ORIG_SYSTEM_REFERENCE','Orig System Reference','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','PAYMENT_TERM_ID',222,'Payment term identifier','PAYMENT_TERM_ID','','','','MM027735','NUMBER','HZ_CUST_SITE_USES','PAYMENT_TERM_ID','Payment Term Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','SHIP_PARTIAL',222,'No longer used','SHIP_PARTIAL','','','','MM027735','VARCHAR2','HZ_CUST_SITE_USES','SHIP_PARTIAL','Ship Partial','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','SHIP_VIA',222,'The name of the preferred ship method.','SHIP_VIA','','','','MM027735','VARCHAR2','HZ_CUST_SITE_USES','SHIP_VIA','Ship Via','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','FOB_POINT',222,'Free on board point. The FOB point indicates the point at which title of goods is transfered to the buyer.','FOB_POINT','','','','MM027735','VARCHAR2','HZ_CUST_SITE_USES','FOB_POINT','Fob Point','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','ORDER_TYPE_ID',222,'Order type identifier','ORDER_TYPE_ID','','','','MM027735','NUMBER','HZ_CUST_SITE_USES','ORDER_TYPE_ID','Order Type Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','PRICE_LIST_ID',222,'Price list identifier','PRICE_LIST_ID','','~T~D~2','','MM027735','NUMBER','HZ_CUST_SITE_USES','PRICE_LIST_ID','Price List Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','WAREHOUSE_ID',222,'Warehouse identifier','WAREHOUSE_ID','','','','MM027735','NUMBER','HZ_CUST_SITE_USES','WAREHOUSE_ID','Warehouse Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','TERRITORY_ID',222,'Territory identifier','TERRITORY_ID','','','','MM027735','NUMBER','HZ_CUST_SITE_USES','TERRITORY_ID','Territory Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','ATTRIBUTE_CATEGORY',222,'Descriptive flexfield structure definition column.','ATTRIBUTE_CATEGORY','','','','MM027735','VARCHAR2','HZ_CUST_SITE_USES','ATTRIBUTE_CATEGORY','Attribute Category','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','REQUEST_ID',222,'Concurrent Program who column - concurrent request id of the program that last updated this row (foreign key to FND_CONCURRENT_REQUESTS.REQUEST_ID).','REQUEST_ID','','','','MM027735','NUMBER','HZ_CUST_SITE_USES','REQUEST_ID','Request Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','PROGRAM_APPLICATION_ID',222,'Concurrent Program who column - application id of the program that last updated this row (foreign key to FND_APPLICATION.APPLICATION_ID).','PROGRAM_APPLICATION_ID','','','','MM027735','NUMBER','HZ_CUST_SITE_USES','PROGRAM_APPLICATION_ID','Program Application Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','PROGRAM_ID',222,'Concurrent Program who column - program id of the program that last updated this row (foreign key to FND_CONCURRENT_PROGRAM.CONCURRENT_PROGRAM_ID).','PROGRAM_ID','','','','MM027735','NUMBER','HZ_CUST_SITE_USES','PROGRAM_ID','Program Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','PROGRAM_UPDATE_DATE',222,'Concurrent Program who column - date when a program last updated this row).','PROGRAM_UPDATE_DATE','','','','MM027735','DATE','HZ_CUST_SITE_USES','PROGRAM_UPDATE_DATE','Program Update Date','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','TAX_REFERENCE',222,'Taxpayer identification number','TAX_REFERENCE','','','','MM027735','VARCHAR2','HZ_CUST_SITE_USES','TAX_REFERENCE','Tax Reference','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','SORT_PRIORITY',222,'Sort priority','SORT_PRIORITY','','','','MM027735','NUMBER','HZ_CUST_SITE_USES','SORT_PRIORITY','Sort Priority','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','TAX_CODE',222,'Tax code associated with this site','TAX_CODE','','','','MM027735','VARCHAR2','HZ_CUST_SITE_USES','TAX_CODE','Tax Code','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','ATTRIBUTE12',222,'Descriptive flexfield segment','ATTRIBUTE12','','','','MM027735','VARCHAR2','HZ_CUST_SITE_USES','ATTRIBUTE12','Attribute12','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','ATTRIBUTE13',222,'Descriptive flexfield segment','ATTRIBUTE13','','','','MM027735','VARCHAR2','HZ_CUST_SITE_USES','ATTRIBUTE13','Attribute13','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','ATTRIBUTE14',222,'Descriptive flexfield segment','ATTRIBUTE14','','','','MM027735','VARCHAR2','HZ_CUST_SITE_USES','ATTRIBUTE14','Attribute14','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','ATTRIBUTE15',222,'Descriptive flexfield segment','ATTRIBUTE15','','','','MM027735','VARCHAR2','HZ_CUST_SITE_USES','ATTRIBUTE15','Attribute15','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','ATTRIBUTE16',222,'Descriptive Flexfield segment','ATTRIBUTE16','','','','MM027735','VARCHAR2','HZ_CUST_SITE_USES','ATTRIBUTE16','Attribute16','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','ATTRIBUTE17',222,'Descriptive Flexfield segment','ATTRIBUTE17','','','','MM027735','VARCHAR2','HZ_CUST_SITE_USES','ATTRIBUTE17','Attribute17','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','ATTRIBUTE18',222,'Descriptive Flexfield segment','ATTRIBUTE18','','','','MM027735','VARCHAR2','HZ_CUST_SITE_USES','ATTRIBUTE18','Attribute18','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','ATTRIBUTE19',222,'Descriptive Flexfield segment','ATTRIBUTE19','','','','MM027735','VARCHAR2','HZ_CUST_SITE_USES','ATTRIBUTE19','Attribute19','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','ATTRIBUTE20',222,'Descriptive Flexfield segment','ATTRIBUTE20','','','','MM027735','VARCHAR2','HZ_CUST_SITE_USES','ATTRIBUTE20','Attribute20','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','ATTRIBUTE21',222,'Descriptive Flexfield segment','ATTRIBUTE21','','','','MM027735','VARCHAR2','HZ_CUST_SITE_USES','ATTRIBUTE21','Attribute21','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','ATTRIBUTE22',222,'Descriptive Flexfield segment','ATTRIBUTE22','','','','MM027735','VARCHAR2','HZ_CUST_SITE_USES','ATTRIBUTE22','Attribute22','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','ATTRIBUTE23',222,'Descriptive Flexfield segment','ATTRIBUTE23','','','','MM027735','VARCHAR2','HZ_CUST_SITE_USES','ATTRIBUTE23','Attribute23','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','ATTRIBUTE24',222,'Descriptive Flexfield segment','ATTRIBUTE24','','','','MM027735','VARCHAR2','HZ_CUST_SITE_USES','ATTRIBUTE24','Attribute24','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','ATTRIBUTE25',222,'Descriptive Flexfield segment','ATTRIBUTE25','','','','MM027735','VARCHAR2','HZ_CUST_SITE_USES','ATTRIBUTE25','Attribute25','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','LAST_ACCRUE_CHARGE_DATE',222,'The last date that accrued finance charges were computed for this site.','LAST_ACCRUE_CHARGE_DATE','','','','MM027735','DATE','HZ_CUST_SITE_USES','LAST_ACCRUE_CHARGE_DATE','Last Accrue Charge Date','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','SECOND_LAST_ACCRUE_CHARGE_DATE',222,'The next-to-last date that accrued finance charges were computed for this site.','SECOND_LAST_ACCRUE_CHARGE_DATE','','','','MM027735','DATE','HZ_CUST_SITE_USES','SECOND_LAST_ACCRUE_CHARGE_DATE','Second Last Accrue Charge Date','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','LAST_UNACCRUE_CHARGE_DATE',222,'The last date that unaccrued finance charges were computed for this site.','LAST_UNACCRUE_CHARGE_DATE','','','','MM027735','DATE','HZ_CUST_SITE_USES','LAST_UNACCRUE_CHARGE_DATE','Last Unaccrue Charge Date','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','SECOND_LAST_UNACCRUE_CHRG_DATE',222,'The next-to-last date that unaccrued finance charges were computed for this site.','SECOND_LAST_UNACCRUE_CHRG_DATE','','','','MM027735','DATE','HZ_CUST_SITE_USES','SECOND_LAST_UNACCRUE_CHRG_DATE','Second Last Unaccrue Chrg Date','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','DEMAND_CLASS_CODE',222,'Demand class (user-defined Lookup used by Oracle Manufacturing)','DEMAND_CLASS_CODE','','','','MM027735','VARCHAR2','HZ_CUST_SITE_USES','DEMAND_CLASS_CODE','Demand Class Code','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','ORG_ID',222,'Organization identifier','ORG_ID','','','','MM027735','NUMBER','HZ_CUST_SITE_USES','ORG_ID','Org Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','WH_UPDATE_DATE',222,'No longer used','WH_UPDATE_DATE','','','','MM027735','DATE','HZ_CUST_SITE_USES','WH_UPDATE_DATE','Wh Update Date','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','GLOBAL_ATTRIBUTE1',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE1','','','','MM027735','VARCHAR2','HZ_CUST_SITE_USES','GLOBAL_ATTRIBUTE1','Global Attribute1','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','GLOBAL_ATTRIBUTE2',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE2','','','','MM027735','VARCHAR2','HZ_CUST_SITE_USES','GLOBAL_ATTRIBUTE2','Global Attribute2','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','GLOBAL_ATTRIBUTE3',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE3','','','','MM027735','VARCHAR2','HZ_CUST_SITE_USES','GLOBAL_ATTRIBUTE3','Global Attribute3','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','GLOBAL_ATTRIBUTE4',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE4','','','','MM027735','VARCHAR2','HZ_CUST_SITE_USES','GLOBAL_ATTRIBUTE4','Global Attribute4','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','GLOBAL_ATTRIBUTE5',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE5','','','','MM027735','VARCHAR2','HZ_CUST_SITE_USES','GLOBAL_ATTRIBUTE5','Global Attribute5','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','GLOBAL_ATTRIBUTE6',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE6','','','','MM027735','VARCHAR2','HZ_CUST_SITE_USES','GLOBAL_ATTRIBUTE6','Global Attribute6','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','GLOBAL_ATTRIBUTE7',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE7','','','','MM027735','VARCHAR2','HZ_CUST_SITE_USES','GLOBAL_ATTRIBUTE7','Global Attribute7','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','GLOBAL_ATTRIBUTE8',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE8','','','','MM027735','VARCHAR2','HZ_CUST_SITE_USES','GLOBAL_ATTRIBUTE8','Global Attribute8','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','GLOBAL_ATTRIBUTE9',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE9','','','','MM027735','VARCHAR2','HZ_CUST_SITE_USES','GLOBAL_ATTRIBUTE9','Global Attribute9','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','GLOBAL_ATTRIBUTE10',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE10','','','','MM027735','VARCHAR2','HZ_CUST_SITE_USES','GLOBAL_ATTRIBUTE10','Global Attribute10','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','GLOBAL_ATTRIBUTE11',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE11','','','','MM027735','VARCHAR2','HZ_CUST_SITE_USES','GLOBAL_ATTRIBUTE11','Global Attribute11','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','GLOBAL_ATTRIBUTE12',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE12','','','','MM027735','VARCHAR2','HZ_CUST_SITE_USES','GLOBAL_ATTRIBUTE12','Global Attribute12','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','GLOBAL_ATTRIBUTE13',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE13','','','','MM027735','VARCHAR2','HZ_CUST_SITE_USES','GLOBAL_ATTRIBUTE13','Global Attribute13','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','GLOBAL_ATTRIBUTE14',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE14','','','','MM027735','VARCHAR2','HZ_CUST_SITE_USES','GLOBAL_ATTRIBUTE14','Global Attribute14','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','GLOBAL_ATTRIBUTE15',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE15','','','','MM027735','VARCHAR2','HZ_CUST_SITE_USES','GLOBAL_ATTRIBUTE15','Global Attribute15','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','GLOBAL_ATTRIBUTE16',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE16','','','','MM027735','VARCHAR2','HZ_CUST_SITE_USES','GLOBAL_ATTRIBUTE16','Global Attribute16','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','GLOBAL_ATTRIBUTE17',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE17','','','','MM027735','VARCHAR2','HZ_CUST_SITE_USES','GLOBAL_ATTRIBUTE17','Global Attribute17','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','GLOBAL_ATTRIBUTE18',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE18','','','','MM027735','VARCHAR2','HZ_CUST_SITE_USES','GLOBAL_ATTRIBUTE18','Global Attribute18','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','GLOBAL_ATTRIBUTE19',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE19','','','','MM027735','VARCHAR2','HZ_CUST_SITE_USES','GLOBAL_ATTRIBUTE19','Global Attribute19','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','GLOBAL_ATTRIBUTE20',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE20','','','','MM027735','VARCHAR2','HZ_CUST_SITE_USES','GLOBAL_ATTRIBUTE20','Global Attribute20','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','GLOBAL_ATTRIBUTE_CATEGORY',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE_CATEGORY','','','','MM027735','VARCHAR2','HZ_CUST_SITE_USES','GLOBAL_ATTRIBUTE_CATEGORY','Global Attribute Category','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','FINCHRG_RECEIVABLES_TRX_ID',222,'Identifies receivables activity that are used for finance charges','FINCHRG_RECEIVABLES_TRX_ID','','','','MM027735','NUMBER','HZ_CUST_SITE_USES','FINCHRG_RECEIVABLES_TRX_ID','Finchrg Receivables Trx Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','DATES_NEGATIVE_TOLERANCE',222,'Acceptable range of days prior to a specified date','DATES_NEGATIVE_TOLERANCE','','','','MM027735','NUMBER','HZ_CUST_SITE_USES','DATES_NEGATIVE_TOLERANCE','Dates Negative Tolerance','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','DATES_POSITIVE_TOLERANCE',222,'Acceptable range of days after a specified date','DATES_POSITIVE_TOLERANCE','','','','MM027735','NUMBER','HZ_CUST_SITE_USES','DATES_POSITIVE_TOLERANCE','Dates Positive Tolerance','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','DATE_TYPE_PREFERENCE',222,'Preference for ship or arrival dates','DATE_TYPE_PREFERENCE','','','','MM027735','VARCHAR2','HZ_CUST_SITE_USES','DATE_TYPE_PREFERENCE','Date Type Preference','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','OVER_SHIPMENT_TOLERANCE',222,'Percentage tolerance for over shipment','OVER_SHIPMENT_TOLERANCE','','','','MM027735','NUMBER','HZ_CUST_SITE_USES','OVER_SHIPMENT_TOLERANCE','Over Shipment Tolerance','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','UNDER_SHIPMENT_TOLERANCE',222,'Percentage tolerance for under shipment','UNDER_SHIPMENT_TOLERANCE','','','','MM027735','NUMBER','HZ_CUST_SITE_USES','UNDER_SHIPMENT_TOLERANCE','Under Shipment Tolerance','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','ITEM_CROSS_REF_PREF',222,'Lookup for item cross-reference preference for placing orders','ITEM_CROSS_REF_PREF','','','','MM027735','VARCHAR2','HZ_CUST_SITE_USES','ITEM_CROSS_REF_PREF','Item Cross Ref Pref','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','OVER_RETURN_TOLERANCE',222,'Percentage tolerance for over return','OVER_RETURN_TOLERANCE','','','','MM027735','NUMBER','HZ_CUST_SITE_USES','OVER_RETURN_TOLERANCE','Over Return Tolerance','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','UNDER_RETURN_TOLERANCE',222,'Percentage tolerance for under return','UNDER_RETURN_TOLERANCE','','','','MM027735','NUMBER','HZ_CUST_SITE_USES','UNDER_RETURN_TOLERANCE','Under Return Tolerance','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','SHIP_SETS_INCLUDE_LINES_FLAG',222,'Indicates if lines should automatically be included in ship sets. Y for lines included in ship sets. N for lines not included in ship sets.','SHIP_SETS_INCLUDE_LINES_FLAG','','','','MM027735','VARCHAR2','HZ_CUST_SITE_USES','SHIP_SETS_INCLUDE_LINES_FLAG','Ship Sets Include Lines Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','ARRIVALSETS_INCLUDE_LINES_FLAG',222,'Indicates if lines should automatically be included in ship sets. Y for lines included in ship sets. N for lines not included in ship sets.','ARRIVALSETS_INCLUDE_LINES_FLAG','','','','MM027735','VARCHAR2','HZ_CUST_SITE_USES','ARRIVALSETS_INCLUDE_LINES_FLAG','Arrivalsets Include Lines Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','SCHED_DATE_PUSH_FLAG',222,'Indicates if the group schedule date should automatically be pushed. Y for pushing the schedule dates, N for not pushing the schedule date.','SCHED_DATE_PUSH_FLAG','','','','MM027735','VARCHAR2','HZ_CUST_SITE_USES','SCHED_DATE_PUSH_FLAG','Sched Date Push Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','INVOICE_QUANTITY_RULE',222,'Rule to indicate whether to invoice for ordered quantity or fulfilled quantity','INVOICE_QUANTITY_RULE','','','','MM027735','VARCHAR2','HZ_CUST_SITE_USES','INVOICE_QUANTITY_RULE','Invoice Quantity Rule','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','PRICING_EVENT',222,'No longer used','PRICING_EVENT','','','','MM027735','VARCHAR2','HZ_CUST_SITE_USES','PRICING_EVENT','Pricing Event','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','GL_ID_REC',222,'Code combination ID for the Receivable Account','GL_ID_REC','','','','MM027735','NUMBER','HZ_CUST_SITE_USES','GL_ID_REC','Gl Id Rec','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','GL_ID_REV',222,'Code combination ID for the Revenue Account','GL_ID_REV','','','','MM027735','NUMBER','HZ_CUST_SITE_USES','GL_ID_REV','Gl Id Rev','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','GL_ID_TAX',222,'Code combination ID for the Tax Account','GL_ID_TAX','','','','MM027735','NUMBER','HZ_CUST_SITE_USES','GL_ID_TAX','Gl Id Tax','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','GL_ID_FREIGHT',222,'Code combination ID for the Freight Account','GL_ID_FREIGHT','','','','MM027735','NUMBER','HZ_CUST_SITE_USES','GL_ID_FREIGHT','Gl Id Freight','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','GL_ID_CLEARING',222,'Code combination ID for the Clearing Account','GL_ID_CLEARING','','','','MM027735','NUMBER','HZ_CUST_SITE_USES','GL_ID_CLEARING','Gl Id Clearing','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','GL_ID_UNBILLED',222,'Code combination ID for the Unbilled Account','GL_ID_UNBILLED','','','','MM027735','NUMBER','HZ_CUST_SITE_USES','GL_ID_UNBILLED','Gl Id Unbilled','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','GL_ID_UNEARNED',222,'Code combination ID for the Unearned Account','GL_ID_UNEARNED','','','','MM027735','NUMBER','HZ_CUST_SITE_USES','GL_ID_UNEARNED','Gl Id Unearned','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','GL_ID_UNPAID_REC',222,'Unpaid Bills Receivable Account','GL_ID_UNPAID_REC','','','','MM027735','NUMBER','HZ_CUST_SITE_USES','GL_ID_UNPAID_REC','Gl Id Unpaid Rec','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','GL_ID_REMITTANCE',222,'Bill Receivable Remittance Account','GL_ID_REMITTANCE','','','','MM027735','NUMBER','HZ_CUST_SITE_USES','GL_ID_REMITTANCE','Gl Id Remittance','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','GL_ID_FACTOR',222,'Code combination ID for a bill-receivable factoring account. Used for Spanish requirements.','GL_ID_FACTOR','','','','MM027735','NUMBER','HZ_CUST_SITE_USES','GL_ID_FACTOR','Gl Id Factor','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','OBJECT_VERSION_NUMBER',222,'This column is used for locking purposes','OBJECT_VERSION_NUMBER','','','','MM027735','NUMBER','HZ_CUST_SITE_USES','OBJECT_VERSION_NUMBER','Object Version Number','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','CREATED_BY_MODULE',222,'TCA Who column','CREATED_BY_MODULE','','','','MM027735','VARCHAR2','HZ_CUST_SITE_USES','CREATED_BY_MODULE','Created By Module','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','APPLICATION_ID',222,'TCA Who column','APPLICATION_ID','','','','MM027735','NUMBER','HZ_CUST_SITE_USES','APPLICATION_ID','Application Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','ATTRIBUTE2',222,'DFF Context Code - Global Data Elements','ATTRIBUTE2','','','','MM027735','','HZ_CUST_SITE_USES','ATTRIBUTE2','Government Funded?','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','ATTRIBUTE3',222,'DFF Context Code - Global Data Elements','ATTRIBUTE3','','','','MM027735','','HZ_CUST_SITE_USES','ATTRIBUTE3','Thomas Guide Page','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','ATTRIBUTE4',222,'DFF Context Code - Global Data Elements','ATTRIBUTE4','','','','MM027735','','HZ_CUST_SITE_USES','ATTRIBUTE4','Dodge Number','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','ATTRIBUTE5',222,'DFF Context Code - Global Data Elements','ATTRIBUTE5','','','','MM027735','','HZ_CUST_SITE_USES','ATTRIBUTE5','FUTURE USE','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','ATTRIBUTE6',222,'DFF Context Code - Global Data Elements','ATTRIBUTE6','','','','MM027735','','HZ_CUST_SITE_USES','ATTRIBUTE6','Salesrep #2','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','ATTRIBUTE7',222,'DFF Context Code - Global Data Elements','ATTRIBUTE7','','','','MM027735','','HZ_CUST_SITE_USES','ATTRIBUTE7','Salesrep Spilt #1','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','ATTRIBUTE8',222,'DFF Context Code - Global Data Elements','ATTRIBUTE8','','','','MM027735','','HZ_CUST_SITE_USES','ATTRIBUTE8','Salesrep Spilt #2','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_SITE_USES','ATTRIBUTE9',222,'DFF Context Code - Global Data Elements','ATTRIBUTE9','','','','MM027735','','HZ_CUST_SITE_USES','ATTRIBUTE9','Permit Number','','','','US');
--Inserting Object Components for HZ_CUST_SITE_USES
--Inserting Object Component Joins for HZ_CUST_SITE_USES
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 222');
END IF;
END;
/
--Exporting View Component Data of the View -  XXEIS_AR_CUST_LIST_SUMMARY_V
prompt Creating Object Data HZ_CUST_ACCOUNTS
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(222);
IF mod_exist = 'Y' THEN 
--Inserting Object HZ_CUST_ACCOUNTS
xxeis.eis_rsc_ins.v( 'HZ_CUST_ACCOUNTS',222,'Stores information about customer accounts.','1.0','','','ANONYMOUS','APPS','Hz Cust Accounts','HCA','','','SYNONYM','US','','');
--Delete Object Columns for HZ_CUST_ACCOUNTS
xxeis.eis_rsc_utility.delete_view_rows('HZ_CUST_ACCOUNTS',222,FALSE);
--Inserting Object Columns for HZ_CUST_ACCOUNTS
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','WATCH_BALANCE_INDICATOR',222,'No longer used','WATCH_BALANCE_INDICATOR','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','WATCH_BALANCE_INDICATOR','Watch Balance Indicator','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','WATCH_ACCOUNT_FLAG',222,'No longer used','WATCH_ACCOUNT_FLAG','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','WATCH_ACCOUNT_FLAG','Watch Account Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','SINGLE_USER_FLAG',222,'No longer used','SINGLE_USER_FLAG','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','SINGLE_USER_FLAG','Single User Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','REALTIME_RATE_FLAG',222,'No longer used','REALTIME_RATE_FLAG','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','REALTIME_RATE_FLAG','Realtime Rate Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','PO_EXPIRATION_DATE',222,'No longer used','PO_EXPIRATION_DATE','','','','ANONYMOUS','DATE','HZ_CUST_ACCOUNTS','PO_EXPIRATION_DATE','Po Expiration Date','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','PO_EFFECTIVE_DATE',222,'No longer used','PO_EFFECTIVE_DATE','','','','ANONYMOUS','DATE','HZ_CUST_ACCOUNTS','PO_EFFECTIVE_DATE','Po Effective Date','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','HIGH_PRIORITY_REMARKS',222,'No longer used','HIGH_PRIORITY_REMARKS','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','HIGH_PRIORITY_REMARKS','High Priority Remarks','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','HOLD_BILL_FLAG',222,'Indicates if the bill receivable should be held or not. Y for holding the bill, N for bills receivable should not be held.','HOLD_BILL_FLAG','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','HOLD_BILL_FLAG','Hold Bill Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','HELD_BILL_EXPIRATION_DATE',222,'Held bill expiration date','HELD_BILL_EXPIRATION_DATE','','','','ANONYMOUS','DATE','HZ_CUST_ACCOUNTS','HELD_BILL_EXPIRATION_DATE','Held Bill Expiration Date','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','HOTWATCH_SVC_BAL_IND',222,'No longer used','HOTWATCH_SVC_BAL_IND','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','HOTWATCH_SVC_BAL_IND','Hotwatch Svc Bal Ind','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','HOTWATCH_SERVICE_FLAG',222,'No longer used','HOTWATCH_SERVICE_FLAG','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','HOTWATCH_SERVICE_FLAG','Hotwatch Service Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','MAJOR_ACCOUNT_NUMBER',222,'No longer used','MAJOR_ACCOUNT_NUMBER','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','MAJOR_ACCOUNT_NUMBER','Major Account Number','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','DEPARTMENT',222,'No longer used','DEPARTMENT','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','DEPARTMENT','Department','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','CREDIT_CLASSIFICATION_CODE',222,'No longer used','CREDIT_CLASSIFICATION_CODE','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','CREDIT_CLASSIFICATION_CODE','Credit Classification Code','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','ACCOUNT_ACTIVATION_DATE',222,'No longer used','ACCOUNT_ACTIVATION_DATE','','','','ANONYMOUS','DATE','HZ_CUST_ACCOUNTS','ACCOUNT_ACTIVATION_DATE','Account Activation Date','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','ACCOUNT_TERMINATION_DATE',222,'No longer used','ACCOUNT_TERMINATION_DATE','','','','ANONYMOUS','DATE','HZ_CUST_ACCOUNTS','ACCOUNT_TERMINATION_DATE','Account Termination Date','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','ACCOUNT_ESTABLISHED_DATE',222,'Date when the customer account was originally established through either an Oracle Application or a legacy system','ACCOUNT_ESTABLISHED_DATE','','','','ANONYMOUS','DATE','HZ_CUST_ACCOUNTS','ACCOUNT_ESTABLISHED_DATE','Account Established Date','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','HIGH_PRIORITY_INDICATOR',222,'No longer used','HIGH_PRIORITY_INDICATOR','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','HIGH_PRIORITY_INDICATOR','High Priority Indicator','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','PASSWORD_TEXT',222,'No longer used','PASSWORD_TEXT','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','PASSWORD_TEXT','Password Text','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','CURRENT_BALANCE',222,'No longer used','CURRENT_BALANCE','','~T~D~2','','ANONYMOUS','NUMBER','HZ_CUST_ACCOUNTS','CURRENT_BALANCE','Current Balance','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','RESTRICTION_LIMIT_AMOUNT',222,'No longer used','RESTRICTION_LIMIT_AMOUNT','','~T~D~2','','ANONYMOUS','NUMBER','HZ_CUST_ACCOUNTS','RESTRICTION_LIMIT_AMOUNT','Restriction Limit Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','SECONDARY_SPECIALIST_ID',222,'This column is for the service personnel dispatching feature in Incident Tracking and is a foreign key to the PER_PEOPLE table','SECONDARY_SPECIALIST_ID','','','','ANONYMOUS','NUMBER','HZ_CUST_ACCOUNTS','SECONDARY_SPECIALIST_ID','Secondary Specialist Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','PRIMARY_SPECIALIST_ID',222,'This column is for the service personnel dispatching feature in Incident Tracking and is a foreign key to the PER_PEOPLE table','PRIMARY_SPECIALIST_ID','','','','ANONYMOUS','NUMBER','HZ_CUST_ACCOUNTS','PRIMARY_SPECIALIST_ID','Primary Specialist Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','COTERMINATE_DAY_MONTH',222,'Identifies the date when services ordered by the customer will end. Values for this column will take the form DD-MON.','COTERMINATE_DAY_MONTH','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','COTERMINATE_DAY_MONTH','Coterminate Day Month','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','TAX_ROUNDING_RULE',222,'Tax amount rounding rule','TAX_ROUNDING_RULE','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','TAX_ROUNDING_RULE','Tax Rounding Rule','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','TAX_HEADER_LEVEL_FLAG',222,'Indicate if item is tax header or a line item. Y for records that are headers for tax purposes, N for records that are lines.','TAX_HEADER_LEVEL_FLAG','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','TAX_HEADER_LEVEL_FLAG','Tax Header Level Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','PAYMENT_TERM_ID',222,'No longer used','PAYMENT_TERM_ID','','','','ANONYMOUS','NUMBER','HZ_CUST_ACCOUNTS','PAYMENT_TERM_ID','Payment Term Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','SHIP_PARTIAL',222,'No longer used','SHIP_PARTIAL','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','SHIP_PARTIAL','Ship Partial','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','SUBCATEGORY_CODE',222,'No longer used','SUBCATEGORY_CODE','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','SUBCATEGORY_CODE','Subcategory Code','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','PRIMARY_SALESREP_ID',222,'No longer used','PRIMARY_SALESREP_ID','','','','ANONYMOUS','NUMBER','HZ_CUST_ACCOUNTS','PRIMARY_SALESREP_ID','Primary Salesrep Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','STATUS',222,'Customer status flag.  Lookup code for CODE_STATUS','STATUS','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','STATUS','Status','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','GLOBAL_ATTRIBUTE20',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE20','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','GLOBAL_ATTRIBUTE20','Global Attribute20','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','GLOBAL_ATTRIBUTE19',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE19','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','GLOBAL_ATTRIBUTE19','Global Attribute19','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','GLOBAL_ATTRIBUTE18',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE18','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','GLOBAL_ATTRIBUTE18','Global Attribute18','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','GLOBAL_ATTRIBUTE17',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE17','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','GLOBAL_ATTRIBUTE17','Global Attribute17','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','GLOBAL_ATTRIBUTE16',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE16','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','GLOBAL_ATTRIBUTE16','Global Attribute16','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','GLOBAL_ATTRIBUTE15',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE15','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','GLOBAL_ATTRIBUTE15','Global Attribute15','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','GLOBAL_ATTRIBUTE14',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE14','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','GLOBAL_ATTRIBUTE14','Global Attribute14','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','GLOBAL_ATTRIBUTE13',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE13','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','GLOBAL_ATTRIBUTE13','Global Attribute13','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','GLOBAL_ATTRIBUTE12',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE12','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','GLOBAL_ATTRIBUTE12','Global Attribute12','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','GLOBAL_ATTRIBUTE11',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE11','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','GLOBAL_ATTRIBUTE11','Global Attribute11','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','GLOBAL_ATTRIBUTE10',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE10','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','GLOBAL_ATTRIBUTE10','Global Attribute10','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','GLOBAL_ATTRIBUTE9',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE9','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','GLOBAL_ATTRIBUTE9','Global Attribute9','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','GLOBAL_ATTRIBUTE8',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE8','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','GLOBAL_ATTRIBUTE8','Global Attribute8','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','GLOBAL_ATTRIBUTE7',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE7','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','GLOBAL_ATTRIBUTE7','Global Attribute7','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','GLOBAL_ATTRIBUTE6',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE6','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','GLOBAL_ATTRIBUTE6','Global Attribute6','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','GLOBAL_ATTRIBUTE5',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE5','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','GLOBAL_ATTRIBUTE5','Global Attribute5','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','GLOBAL_ATTRIBUTE4',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE4','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','GLOBAL_ATTRIBUTE4','Global Attribute4','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','GLOBAL_ATTRIBUTE3',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE3','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','GLOBAL_ATTRIBUTE3','Global Attribute3','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','GLOBAL_ATTRIBUTE2',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE2','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','GLOBAL_ATTRIBUTE2','Global Attribute2','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','GLOBAL_ATTRIBUTE1',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE1','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','GLOBAL_ATTRIBUTE1','Global Attribute1','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','GLOBAL_ATTRIBUTE_CATEGORY',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE_CATEGORY','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','GLOBAL_ATTRIBUTE_CATEGORY','Global Attribute Category','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','ATTRIBUTE20',222,'Descriptive Flexfield segment','ATTRIBUTE20','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE20','Attribute20','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','ATTRIBUTE19',222,'Descriptive Flexfield segment','ATTRIBUTE19','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE19','Attribute19','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','ATTRIBUTE18',222,'Descriptive Flexfield segment','ATTRIBUTE18','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE18','Attribute18','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','ATTRIBUTE17',222,'Descriptive Flexfield segment','ATTRIBUTE17','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE17','Attribute17','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','ATTRIBUTE16',222,'Descriptive Flexfield segment','ATTRIBUTE16','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE16','Attribute16','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','ATTRIBUTE15',222,'Descriptive flexfield segment','ATTRIBUTE15','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE15','Attribute15','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','ATTRIBUTE14',222,'Descriptive flexfield segment','ATTRIBUTE14','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE14','Attribute14','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','ATTRIBUTE13',222,'Descriptive flexfield segment','ATTRIBUTE13','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE13','Attribute13','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','ATTRIBUTE12',222,'Descriptive flexfield segment','ATTRIBUTE12','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE12','Attribute12','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','ATTRIBUTE11',222,'Descriptive flexfield segment','ATTRIBUTE11','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE11','Attribute11','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','ATTRIBUTE10',222,'Descriptive flexfield segment','ATTRIBUTE10','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE10','Attribute10','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','ATTRIBUTE9',222,'Descriptive flexfield segment','ATTRIBUTE9','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE9','Attribute9','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','ATTRIBUTE8',222,'Descriptive flexfield segment','ATTRIBUTE8','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE8','Attribute8','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','ATTRIBUTE7',222,'Descriptive flexfield segment','ATTRIBUTE7','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE7','Attribute7','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','ATTRIBUTE6',222,'Descriptive flexfield segment','ATTRIBUTE6','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE6','Attribute6','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','ATTRIBUTE5',222,'Descriptive flexfield segment','ATTRIBUTE5','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE5','Attribute5','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','ATTRIBUTE4',222,'Descriptive flexfield segment','ATTRIBUTE4','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE4','Attribute4','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','ATTRIBUTE_CATEGORY',222,'Descriptive flexfield structure definition column.','ATTRIBUTE_CATEGORY','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE_CATEGORY','Attribute Category','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','WH_UPDATE_DATE',222,'No longer used','WH_UPDATE_DATE','','','','ANONYMOUS','DATE','HZ_CUST_ACCOUNTS','WH_UPDATE_DATE','Wh Update Date','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','PROGRAM_UPDATE_DATE',222,'Concurrent Program who column - date when a program last updated this row).','PROGRAM_UPDATE_DATE','','','','ANONYMOUS','DATE','HZ_CUST_ACCOUNTS','PROGRAM_UPDATE_DATE','Program Update Date','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','PROGRAM_ID',222,'Concurrent Program who column - program id of the program that last updated this row (foreign key to FND_CONCURRENT_PROGRAM.CONCURRENT_PROGRAM_ID).','PROGRAM_ID','','','','ANONYMOUS','NUMBER','HZ_CUST_ACCOUNTS','PROGRAM_ID','Program Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','PROGRAM_APPLICATION_ID',222,'Concurrent Program who column - application id of the program that last updated this row (foreign key to FND_APPLICATION.APPLICATION_ID).','PROGRAM_APPLICATION_ID','','','','ANONYMOUS','NUMBER','HZ_CUST_ACCOUNTS','PROGRAM_APPLICATION_ID','Program Application Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','REQUEST_ID',222,'Concurrent Program who column - concurrent request id of the program that last updated this row (foreign key to FND_CONCURRENT_REQUESTS.REQUEST_ID).','REQUEST_ID','','','','ANONYMOUS','NUMBER','HZ_CUST_ACCOUNTS','REQUEST_ID','Request Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','LAST_UPDATE_LOGIN',222,'Standard who column - operating system login of user who last updated this row (foreign key to FND_LOGINS.LOGIN_ID).','LAST_UPDATE_LOGIN','','','','ANONYMOUS','NUMBER','HZ_CUST_ACCOUNTS','LAST_UPDATE_LOGIN','Last Update Login','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','CREATED_BY',222,'Standard who column - user who created this row (foreign key to FND_USER.USER_ID).','CREATED_BY','','','','ANONYMOUS','NUMBER','HZ_CUST_ACCOUNTS','CREATED_BY','Created By','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','CREATION_DATE',222,'Standard who column - date when this row was created.','CREATION_DATE','','','','ANONYMOUS','DATE','HZ_CUST_ACCOUNTS','CREATION_DATE','Creation Date','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','LAST_UPDATED_BY',222,'Standard who column - user who last updated this row (foreign key to FND_USER.USER_ID).','LAST_UPDATED_BY','','','','ANONYMOUS','NUMBER','HZ_CUST_ACCOUNTS','LAST_UPDATED_BY','Last Updated By','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','ACCOUNT_NUMBER',222,'Account Number','ACCOUNT_NUMBER','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','ACCOUNT_NUMBER','Account Number','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','LAST_UPDATE_DATE',222,'Standard Who column - date when a user last updated this row.','LAST_UPDATE_DATE','','','','ANONYMOUS','DATE','HZ_CUST_ACCOUNTS','LAST_UPDATE_DATE','Last Update Date','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','LAST_BATCH_ID',222,'Last active batch','LAST_BATCH_ID','','','','ANONYMOUS','NUMBER','HZ_CUST_ACCOUNTS','LAST_BATCH_ID','Last Batch Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','NOTIFY_FLAG',222,'No longer used','NOTIFY_FLAG','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','NOTIFY_FLAG','Notify Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','AUTOPAY_FLAG',222,'Autopay flag','AUTOPAY_FLAG','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','AUTOPAY_FLAG','Autopay Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','STATUS_UPDATE_DATE',222,'Last status update date','STATUS_UPDATE_DATE','','','','ANONYMOUS','DATE','HZ_CUST_ACCOUNTS','STATUS_UPDATE_DATE','Status Update Date','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','ACCOUNT_REPLICATION_KEY',222,'No longer used','ACCOUNT_REPLICATION_KEY','','','','ANONYMOUS','NUMBER','HZ_CUST_ACCOUNTS','ACCOUNT_REPLICATION_KEY','Account Replication Key','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','PRICING_EVENT',222,'No longer used','PRICING_EVENT','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','PRICING_EVENT','Pricing Event','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','INVOICE_QUANTITY_RULE',222,'Rule to indicate whether to invoice for ordered quantity or fulfilled quantity','INVOICE_QUANTITY_RULE','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','INVOICE_QUANTITY_RULE','Invoice Quantity Rule','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','SCHED_DATE_PUSH_FLAG',222,'Indicates if the group schedule date should automatically be pushed. Y for pushing the schedule dates, N for not pushing the schedule date.','SCHED_DATE_PUSH_FLAG','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','SCHED_DATE_PUSH_FLAG','Sched Date Push Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','ARRIVALSETS_INCLUDE_LINES_FLAG',222,'Indicates if lines include arrival sets. Y for lines included on arrival sets, N for lines not automatically included in arrival sets','ARRIVALSETS_INCLUDE_LINES_FLAG','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','ARRIVALSETS_INCLUDE_LINES_FLAG','Arrivalsets Include Lines Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','SHIP_SETS_INCLUDE_LINES_FLAG',222,'Indicates if lines should automatically be included in ship sets. Y for lines included in ship sets, N for lines not included in ship sets.','SHIP_SETS_INCLUDE_LINES_FLAG','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','SHIP_SETS_INCLUDE_LINES_FLAG','Ship Sets Include Lines Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','ITEM_CROSS_REF_PREF',222,'Lookup for item cross reference preference for placing orders','ITEM_CROSS_REF_PREF','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','ITEM_CROSS_REF_PREF','Item Cross Ref Pref','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','UNDER_RETURN_TOLERANCE',222,'Percentage tolerance for under return','UNDER_RETURN_TOLERANCE','','','','ANONYMOUS','NUMBER','HZ_CUST_ACCOUNTS','UNDER_RETURN_TOLERANCE','Under Return Tolerance','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','OVER_RETURN_TOLERANCE',222,'Percentage tolerance for over return','OVER_RETURN_TOLERANCE','','','','ANONYMOUS','NUMBER','HZ_CUST_ACCOUNTS','OVER_RETURN_TOLERANCE','Over Return Tolerance','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','UNDER_SHIPMENT_TOLERANCE',222,'Percentage tolerance for under shipment','UNDER_SHIPMENT_TOLERANCE','','','','ANONYMOUS','NUMBER','HZ_CUST_ACCOUNTS','UNDER_SHIPMENT_TOLERANCE','Under Shipment Tolerance','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','OVER_SHIPMENT_TOLERANCE',222,'Percentage tolerance for over shipment','OVER_SHIPMENT_TOLERANCE','','','','ANONYMOUS','NUMBER','HZ_CUST_ACCOUNTS','OVER_SHIPMENT_TOLERANCE','Over Shipment Tolerance','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','DATE_TYPE_PREFERENCE',222,'Preference for ship or arrival dates','DATE_TYPE_PREFERENCE','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','DATE_TYPE_PREFERENCE','Date Type Preference','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','DATES_POSITIVE_TOLERANCE',222,'Maximum number of days after a date','DATES_POSITIVE_TOLERANCE','','','','ANONYMOUS','NUMBER','HZ_CUST_ACCOUNTS','DATES_POSITIVE_TOLERANCE','Dates Positive Tolerance','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','DATES_NEGATIVE_TOLERANCE',222,'Maximum number of days prior to a date','DATES_NEGATIVE_TOLERANCE','','','','ANONYMOUS','NUMBER','HZ_CUST_ACCOUNTS','DATES_NEGATIVE_TOLERANCE','Dates Negative Tolerance','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','COMMENTS',222,'Free format information about the customer','COMMENTS','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','COMMENTS','Comments','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','COMPETITOR_TYPE',222,'No longer used','COMPETITOR_TYPE','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','COMPETITOR_TYPE','Competitor Type','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','SOURCE_CODE',222,'Promotion that was used to make the party a customer','SOURCE_CODE','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','SOURCE_CODE','Source Code','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','WRITE_OFF_AMOUNT',222,'No longer used','WRITE_OFF_AMOUNT','','~T~D~2','','ANONYMOUS','NUMBER','HZ_CUST_ACCOUNTS','WRITE_OFF_AMOUNT','Write Off Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','WRITE_OFF_PAYMENT_AMOUNT',222,'No longer used','WRITE_OFF_PAYMENT_AMOUNT','','~T~D~2','','ANONYMOUS','NUMBER','HZ_CUST_ACCOUNTS','WRITE_OFF_PAYMENT_AMOUNT','Write Off Payment Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','WRITE_OFF_ADJUSTMENT_AMOUNT',222,'No longer used','WRITE_OFF_ADJUSTMENT_AMOUNT','','~T~D~2','','ANONYMOUS','NUMBER','HZ_CUST_ACCOUNTS','WRITE_OFF_ADJUSTMENT_AMOUNT','Write Off Adjustment Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','SUSPENSION_DATE',222,'No longer used','SUSPENSION_DATE','','','','ANONYMOUS','DATE','HZ_CUST_ACCOUNTS','SUSPENSION_DATE','Suspension Date','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','PIN_NUMBER',222,'No longer used','PIN_NUMBER','','','','ANONYMOUS','NUMBER','HZ_CUST_ACCOUNTS','PIN_NUMBER','Pin Number','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','NPA_NUMBER',222,'Identifies an account with a Number Plan Area (NPA) used in telecommunications.','NPA_NUMBER','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','NPA_NUMBER','Npa Number','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','DORMANT_ACCOUNT_FLAG',222,'No longer used','DORMANT_ACCOUNT_FLAG','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','DORMANT_ACCOUNT_FLAG','Dormant Account Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','DEPOSIT_REFUND_METHOD',222,'Refund Method','DEPOSIT_REFUND_METHOD','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','DEPOSIT_REFUND_METHOD','Deposit Refund Method','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','ACCOUNT_NAME',222,'Description chosen by external party (but can be entered internally on behalf on the customer)','ACCOUNT_NAME','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','ACCOUNT_NAME','Account Name','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','ACCT_LIFE_CYCLE_STATUS',222,'No longer used','ACCT_LIFE_CYCLE_STATUS','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','ACCT_LIFE_CYCLE_STATUS','Acct Life Cycle Status','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','GEO_CODE',222,'No longer used','GEO_CODE','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','GEO_CODE','Geo Code','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','ACCOUNT_LIABLE_FLAG',222,'No longer used','ACCOUNT_LIABLE_FLAG','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','ACCOUNT_LIABLE_FLAG','Account Liable Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','SELLING_PARTY_ID',222,'Identifier of the party entering a business relationship as a seller. A selling party can be an individual or an organization. The Selling Party represents the external facing organization with which the customer is doing business, and is n','SELLING_PARTY_ID','','','','ANONYMOUS','NUMBER','HZ_CUST_ACCOUNTS','SELLING_PARTY_ID','Selling Party Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','APPLICATION_ID',222,'TCA Who column','APPLICATION_ID','','','','ANONYMOUS','NUMBER','HZ_CUST_ACCOUNTS','APPLICATION_ID','Application Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','CREATED_BY_MODULE',222,'TCA Who column','CREATED_BY_MODULE','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','CREATED_BY_MODULE','Created By Module','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','OBJECT_VERSION_NUMBER',222,'This column is used for locking purposes','OBJECT_VERSION_NUMBER','','','','ANONYMOUS','NUMBER','HZ_CUST_ACCOUNTS','OBJECT_VERSION_NUMBER','Object Version Number','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','ATTRIBUTE2',222,'DFF Context Code - Global Data Elements','ATTRIBUTE2','','','','ANONYMOUS','','HZ_CUST_ACCOUNTS','ATTRIBUTE2','Vndr Code and FRULOC','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','ATTRIBUTE3',222,'DFF Context Code - Global Data Elements','ATTRIBUTE3','','','','ANONYMOUS','','HZ_CUST_ACCOUNTS','ATTRIBUTE3','Branch Description','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','CUST_ACCOUNT_ID',222,'Customer account identifier','CUST_ACCOUNT_ID','','','','ANONYMOUS','NUMBER','HZ_CUST_ACCOUNTS','CUST_ACCOUNT_ID','Cust Account Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','WAREHOUSE_ID',222,'Warehouse identifier','WAREHOUSE_ID','','','','ANONYMOUS','NUMBER','HZ_CUST_ACCOUNTS','WAREHOUSE_ID','Warehouse Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','SHIP_VIA',222,'The name of the preferred ship method.','SHIP_VIA','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','SHIP_VIA','Ship Via','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','FREIGHT_TERM',222,'Order Management lookup code for the FREIGHT_TERMS attribute.','FREIGHT_TERM','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','FREIGHT_TERM','Freight Term','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','FOB_POINT',222,'The point in a shipment at which title to the goods is transferred.  Examples include, FOB delivered (the title changes hands at the point of delivery).','FOB_POINT','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','FOB_POINT','Fob Point','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','TAX_CODE',222,'Tax Code for the Customer','TAX_CODE','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','TAX_CODE','Tax Code','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','PRICE_LIST_ID',222,'Price list identifier','PRICE_LIST_ID','','~T~D~2','','ANONYMOUS','NUMBER','HZ_CUST_ACCOUNTS','PRICE_LIST_ID','Price List Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','ORDER_TYPE_ID',222,'No longer used','ORDER_TYPE_ID','','','','ANONYMOUS','NUMBER','HZ_CUST_ACCOUNTS','ORDER_TYPE_ID','Order Type Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','SALES_CHANNEL_CODE',222,'Order Management lookup code for the SALES_CHANNEL attribute.','SALES_CHANNEL_CODE','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','SALES_CHANNEL_CODE','Sales Channel Code','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','CUSTOMER_CLASS_CODE',222,'Customer class indentifier','CUSTOMER_CLASS_CODE','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','CUSTOMER_CLASS_CODE','Customer Class Code','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','CUSTOMER_TYPE',222,'Receivables lookup code for the CUSTOMER_TYPE attribute. I for internal customers, R for revenue generating external customers.','CUSTOMER_TYPE','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','CUSTOMER_TYPE','Customer Type','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','ORIG_SYSTEM_REFERENCE',222,'Unique customer identifier from foreign or legacy system','ORIG_SYSTEM_REFERENCE','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCOUNTS','ORIG_SYSTEM_REFERENCE','Orig System Reference','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','PARTY_ID',222,'A foreign key to the HZ_PARTY table.','PARTY_ID','','','','ANONYMOUS','NUMBER','HZ_CUST_ACCOUNTS','PARTY_ID','Party Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','ORG_ID',222,'','ORG_ID','','','','ANONYMOUS','NUMBER','HZ_CUST_ACCOUNTS','ORG_ID','Org Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCOUNTS','ATTRIBUTE1',222,'DFF Context Code - Global Data Elements','ATTRIBUTE1','','','','ANONYMOUS','','HZ_CUST_ACCOUNTS','ATTRIBUTE1','Party Type','','','','US');
--Inserting Object Components for HZ_CUST_ACCOUNTS
--Inserting Object Component Joins for HZ_CUST_ACCOUNTS
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 222');
END IF;
END;
/
--Exporting View Component Data of the View -  XXEIS_AR_CUST_LIST_SUMMARY_V
prompt Creating Object Data HZ_PARTIES
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(222);
IF mod_exist = 'Y' THEN 
--Inserting Object HZ_PARTIES
xxeis.eis_rsc_ins.v( 'HZ_PARTIES',222,'Information about parties such as organizations, people, and groups','1.0','','','ANONYMOUS','APPS','Hz Parties','HP','','','SYNONYM','US','','');
--Delete Object Columns for HZ_PARTIES
xxeis.eis_rsc_utility.delete_view_rows('HZ_PARTIES',222,FALSE);
--Inserting Object Columns for HZ_PARTIES
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','ATTRIBUTE21',222,'Descriptive Flexfield segment','ATTRIBUTE21','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','ATTRIBUTE21','Attribute21','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','ATTRIBUTE20',222,'Descriptive Flexfield segment','ATTRIBUTE20','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','ATTRIBUTE20','Attribute20','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','ATTRIBUTE19',222,'Descriptive Flexfield segment','ATTRIBUTE19','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','ATTRIBUTE19','Attribute19','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','ATTRIBUTE18',222,'Descriptive Flexfield segment','ATTRIBUTE18','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','ATTRIBUTE18','Attribute18','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','ATTRIBUTE17',222,'Descriptive Flexfield segment','ATTRIBUTE17','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','ATTRIBUTE17','Attribute17','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','ATTRIBUTE16',222,'Descriptive Flexfield segment','ATTRIBUTE16','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','ATTRIBUTE16','Attribute16','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','VALIDATED_FLAG',222,'Indicates if the party was validated. Y for a validated party, N for a party that is not validated.','VALIDATED_FLAG','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','VALIDATED_FLAG','Validated Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','PARTY_TYPE',222,'The party type can only be Person, Organization, Group or Relationship.','PARTY_TYPE','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','PARTY_TYPE','Party Type','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','PARTY_NAME',222,'Name of this party','PARTY_NAME','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','PARTY_NAME','Party Name','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','PARTY_NUMBER',222,'Unique identification number for this party','PARTY_NUMBER','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','PARTY_NUMBER','Party Number','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','PARTY_ID',222,'Party identifier','PARTY_ID','','','','ANONYMOUS','NUMBER','HZ_PARTIES','PARTY_ID','Party Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','CREATED_BY_MODULE',222,'TCA Who column','CREATED_BY_MODULE','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','CREATED_BY_MODULE','Created By Module','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','OBJECT_VERSION_NUMBER',222,'This column is used for locking purposes','OBJECT_VERSION_NUMBER','','','','ANONYMOUS','NUMBER','HZ_PARTIES','OBJECT_VERSION_NUMBER','Object Version Number','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','DUNS_NUMBER_C',222,'DUNS_NUMBER in VARCHAR format.  Column value not restricted to nine digit DUNS Number.','DUNS_NUMBER_C','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','DUNS_NUMBER_C','Duns Number C','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','CERT_REASON_CODE',222,'Reason for organization party''s  current certification level assignment. HZ_PARTY_CERT_REASON lookup type','CERT_REASON_CODE','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','CERT_REASON_CODE','Cert Reason Code','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','ORG_CUST_BO_VERSION',222,'For TCA internal use only.','ORG_CUST_BO_VERSION','','','','ANONYMOUS','NUMBER','HZ_PARTIES','ORG_CUST_BO_VERSION','Org Cust Bo Version','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','CERTIFICATION_LEVEL',222,'Certification level an organization party. HZ_PARTY_CERT_LEVEL lookup type','CERTIFICATION_LEVEL','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','CERTIFICATION_LEVEL','Certification Level','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','PRIMARY_PHONE_EXTENSION',222,'Additional number addressed after initial connection to an internal telephone system','PRIMARY_PHONE_EXTENSION','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','PRIMARY_PHONE_EXTENSION','Primary Phone Extension','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','PRIMARY_PHONE_NUMBER',222,'A telephone number formatted in local format. The number should not include country code, area code, or extension','PRIMARY_PHONE_NUMBER','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','PRIMARY_PHONE_NUMBER','Primary Phone Number','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','PRIMARY_PHONE_AREA_CODE',222,'The area code within a country code','PRIMARY_PHONE_AREA_CODE','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','PRIMARY_PHONE_AREA_CODE','Primary Phone Area Code','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','PRIMARY_PHONE_COUNTRY_CODE',222,'International country code for a telephone number. For example, 33 for France','PRIMARY_PHONE_COUNTRY_CODE','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','PRIMARY_PHONE_COUNTRY_CODE','Primary Phone Country Code','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','PRIMARY_PHONE_LINE_TYPE',222,'Lookup code for the type of phone line. For example, general, fax, inbound or outbound','PRIMARY_PHONE_LINE_TYPE','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','PRIMARY_PHONE_LINE_TYPE','Primary Phone Line Type','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','PRIMARY_PHONE_PURPOSE',222,'Contact point type, such as business or personal','PRIMARY_PHONE_PURPOSE','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','PRIMARY_PHONE_PURPOSE','Primary Phone Purpose','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','PRIMARY_PHONE_CONTACT_PT_ID',222,'Unique identifier of this contact point','PRIMARY_PHONE_CONTACT_PT_ID','','','','ANONYMOUS','NUMBER','HZ_PARTIES','PRIMARY_PHONE_CONTACT_PT_ID','Primary Phone Contact Pt Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','PERSON_CUST_BO_VERSION',222,'For TCA internal use only.','PERSON_CUST_BO_VERSION','','','','ANONYMOUS','NUMBER','HZ_PARTIES','PERSON_CUST_BO_VERSION','Person Cust Bo Version','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','ORG_BO_VERSION',222,'For TCA internal use only.','ORG_BO_VERSION','','','','ANONYMOUS','NUMBER','HZ_PARTIES','ORG_BO_VERSION','Org Bo Version','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','PERSON_BO_VERSION',222,'For TCA internal use only.','PERSON_BO_VERSION','','','','ANONYMOUS','NUMBER','HZ_PARTIES','PERSON_BO_VERSION','Person Bo Version','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','HOME_COUNTRY',222,'Home Country of the organization if the partyis an organization','HOME_COUNTRY','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','HOME_COUNTRY','Home Country','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','PREFERRED_CONTACT_METHOD',222,'How a person or organization prefers to be contacted. Validated against lookup HZ_PREFERRED_CONTACT_METHOD.','PREFERRED_CONTACT_METHOD','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','PREFERRED_CONTACT_METHOD','Preferred Contact Method','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','SIC_CODE_TYPE',222,'Version of Standard Industry Classification (SIC) code','SIC_CODE_TYPE','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','SIC_CODE_TYPE','Sic Code Type','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','COUNTY',222,'County of the Identifying address','COUNTY','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','COUNTY','County','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','STATUS',222,'Party status flag.','STATUS','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','STATUS','Status','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','PROVINCE',222,'Province of the Identifying address','PROVINCE','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','PROVINCE','Province','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','STATE',222,'State of the Identifying address','STATE','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','STATE','State','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','POSTAL_CODE',222,'Postal Code of the Identifying address','POSTAL_CODE','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','POSTAL_CODE','Postal Code','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','CITY',222,'City of the Identifying address','CITY','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','CITY','City','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','ADDRESS4',222,'Fourth line of the Identifying address','ADDRESS4','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','ADDRESS4','Address4','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','ADDRESS3',222,'Third line of the Identifying address','ADDRESS3','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','ADDRESS3','Address3','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','ADDRESS2',222,'Second line of the Identifying address','ADDRESS2','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','ADDRESS2','Address2','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','ADDRESS1',222,'First line of the Identifying address','ADDRESS1','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','ADDRESS1','Address1','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','COUNTRY',222,'The country listed in the TERRITORY_CODE column of the FND_TERRITORY table. for the Identifying address.','COUNTRY','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','COUNTRY','Country','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','GROUP_TYPE',222,'Lookup for group type','GROUP_TYPE','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','GROUP_TYPE','Group Type','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','PERSON_IDENTIFIER',222,'Personal identifier, such as driver''s license or passport number, or country-specific identifier for person located or doing business in multiple countries','PERSON_IDENTIFIER','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','PERSON_IDENTIFIER','Person Identifier','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','PERSON_IDEN_TYPE',222,'Personal identifier type, such as driver''s license or passport. Lookup type for PERSON_IDENTIFIER column','PERSON_IDEN_TYPE','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','PERSON_IDEN_TYPE','Person Iden Type','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','KNOWN_AS',222,'An alias or other name by which a party is known','KNOWN_AS','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','KNOWN_AS','Known As','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','PERSON_PREVIOUS_LAST_NAME',222,'Previous last or surname of the person','PERSON_PREVIOUS_LAST_NAME','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','PERSON_PREVIOUS_LAST_NAME','Person Previous Last Name','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','PERSON_ACADEMIC_TITLE',222,'Academic title that is part of a person''s name, such as Dr. John Smith.','PERSON_ACADEMIC_TITLE','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','PERSON_ACADEMIC_TITLE','Person Academic Title','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','PERSON_TITLE',222,'A professional or family title. For example, Don or The Right Honorable.','PERSON_TITLE','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','PERSON_TITLE','Person Title','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','PERSON_NAME_SUFFIX',222,'Indicates the place in a family structure. For example, in Tom Jones III, the "III" is the suffix.','PERSON_NAME_SUFFIX','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','PERSON_NAME_SUFFIX','Person Name Suffix','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','PERSON_LAST_NAME',222,'Last or surname of the person','PERSON_LAST_NAME','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','PERSON_LAST_NAME','Person Last Name','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','PERSON_MIDDLE_NAME',222,'Middle name of the person','PERSON_MIDDLE_NAME','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','PERSON_MIDDLE_NAME','Person Middle Name','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','PERSON_FIRST_NAME',222,'First name of the person','PERSON_FIRST_NAME','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','PERSON_FIRST_NAME','Person First Name','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','PERSON_PRE_NAME_ADJUNCT',222,'Salutary introduction, such as Mr. or Herr','PERSON_PRE_NAME_ADJUNCT','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','PERSON_PRE_NAME_ADJUNCT','Person Pre Name Adjunct','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','TAX_NAME',222,'No longer used','TAX_NAME','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','TAX_NAME','Tax Name','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','DUNS_NUMBER',222,'The DUNS number, a unique, nine-digit identification number assigned to business entities by Dun & Bradstreet','DUNS_NUMBER','','','','ANONYMOUS','NUMBER','HZ_PARTIES','DUNS_NUMBER','Duns Number','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','JGZZ_FISCAL_CODE',222,'Taxpayer identification number, often unique identifier of person or organization. Can be SSN or income taxpayer ID in US, fiscal code or NIF in Europe','JGZZ_FISCAL_CODE','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','JGZZ_FISCAL_CODE','Jgzz Fiscal Code','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','TAX_REFERENCE',222,'Taxpayer registration number, also known as the VAT number.','TAX_REFERENCE','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','TAX_REFERENCE','Tax Reference','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','CUSTOMER_KEY',222,'Derived key used to facilitate fuzzy searches','CUSTOMER_KEY','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','CUSTOMER_KEY','Customer Key','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','HQ_BRANCH_IND',222,'Status of this site. Identifies if the location is the headquarters, a branch, or a single location.','HQ_BRANCH_IND','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','HQ_BRANCH_IND','Hq Branch Ind','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','SIC_CODE',222,'Standard Industry Classification (SIC) code','SIC_CODE','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','SIC_CODE','Sic Code','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','ORIG_SYSTEM_REFERENCE',222,'Customer identifier from foreign system. May not be unique.','ORIG_SYSTEM_REFERENCE','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','ORIG_SYSTEM_REFERENCE','Orig System Reference','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','ATTRIBUTE24',222,'Descriptive Flexfield segment','ATTRIBUTE24','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','ATTRIBUTE24','Attribute24','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','ATTRIBUTE23',222,'Descriptive Flexfield segment','ATTRIBUTE23','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','ATTRIBUTE23','Attribute23','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','ATTRIBUTE22',222,'Descriptive Flexfield segment','ATTRIBUTE22','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','ATTRIBUTE22','Attribute22','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','SALUTATION',222,'Phrase used to address a party in any correspondence.','SALUTATION','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','SALUTATION','Salutation','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','COMPETITOR_FLAG',222,'Indicates if a party is a competitor. Y for a party that is a competitor, N or null for a party that is not a competitor','COMPETITOR_FLAG','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','COMPETITOR_FLAG','Competitor Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','THIRD_PARTY_FLAG',222,'Tracks if a party has a direct relationship with the organization using Oracle Receivables or through a third party vendor. Y for a third party relationship, N for a direct relationship. If Oracle Service is installed, the Third Party field','THIRD_PARTY_FLAG','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','THIRD_PARTY_FLAG','Third Party Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','REFERENCE_USE_FLAG',222,'Indicates if customer has agreed to be a reference. Y for customer who is referencable, N or null for a nonreferencable customer','REFERENCE_USE_FLAG','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','REFERENCE_USE_FLAG','Reference Use Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','CATEGORY_CODE',222,'User-definable category. Lookup type is CUSTOMER_CATEGORY','CATEGORY_CODE','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','CATEGORY_CODE','Category Code','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','LANGUAGE_NAME',222,'Standard name for a language','LANGUAGE_NAME','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','LANGUAGE_NAME','Language Name','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','PERSON_LAST_NAME_PHONETIC',222,'Phonetic representation of the person''s last name','PERSON_LAST_NAME_PHONETIC','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','PERSON_LAST_NAME_PHONETIC','Person Last Name Phonetic','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','PERSON_FIRST_NAME_PHONETIC',222,'Phonetic representation of the person''s first name','PERSON_FIRST_NAME_PHONETIC','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','PERSON_FIRST_NAME_PHONETIC','Person First Name Phonetic','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','ORGANIZATION_NAME_PHONETIC',222,'Japanese Kana, or phonetic representation of organization name','ORGANIZATION_NAME_PHONETIC','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','ORGANIZATION_NAME_PHONETIC','Organization Name Phonetic','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','MISSION_STATEMENT',222,'Corporate charter of organization','MISSION_STATEMENT','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','MISSION_STATEMENT','Mission Statement','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','GSA_INDICATOR_FLAG',222,'Indicates if this is a US federal agency supported by the General Services Administration (GSA). Y for organizations served by the GSA, N for organizations that are not served by the GSA.','GSA_INDICATOR_FLAG','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','GSA_INDICATOR_FLAG','Gsa Indicator Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','YEAR_ESTABLISHED',222,'Year that the organization began doing business','YEAR_ESTABLISHED','','','','ANONYMOUS','NUMBER','HZ_PARTIES','YEAR_ESTABLISHED','Year Established','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','NEXT_FY_POTENTIAL_REVENUE',222,'Organization''s potential revenue for next fiscal year','NEXT_FY_POTENTIAL_REVENUE','','~T~D~2','','ANONYMOUS','NUMBER','HZ_PARTIES','NEXT_FY_POTENTIAL_REVENUE','Next Fy Potential Revenue','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','CURR_FY_POTENTIAL_REVENUE',222,'Organization''s potential revenue this fiscal year','CURR_FY_POTENTIAL_REVENUE','','~T~D~2','','ANONYMOUS','NUMBER','HZ_PARTIES','CURR_FY_POTENTIAL_REVENUE','Curr Fy Potential Revenue','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','EMPLOYEES_TOTAL',222,'Total number of employees','EMPLOYEES_TOTAL','','~T~D~2','','ANONYMOUS','NUMBER','HZ_PARTIES','EMPLOYEES_TOTAL','Employees Total','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','FISCAL_YEAREND_MONTH',222,'Month in which fiscal year ends for organization','FISCAL_YEAREND_MONTH','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','FISCAL_YEAREND_MONTH','Fiscal Yearend Month','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','ANALYSIS_FY',222,'Fiscal year that financial information is based on','ANALYSIS_FY','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','ANALYSIS_FY','Analysis Fy','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','DO_NOT_MAIL_FLAG',222,'No longer used','DO_NOT_MAIL_FLAG','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','DO_NOT_MAIL_FLAG','Do Not Mail Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','EMAIL_ADDRESS',222,'E-mail address','EMAIL_ADDRESS','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','EMAIL_ADDRESS','Email Address','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','URL',222,'Uniform resource locator','URL','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','URL','Url','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','LAST_ORDERED_DATE',222,'Date of the most recent order','LAST_ORDERED_DATE','','','','ANONYMOUS','DATE','HZ_PARTIES','LAST_ORDERED_DATE','Last Ordered Date','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','TOTAL_ORDERED_AMOUNT',222,'Total amount ordered through the last ordered date','TOTAL_ORDERED_AMOUNT','','~T~D~2','','ANONYMOUS','NUMBER','HZ_PARTIES','TOTAL_ORDERED_AMOUNT','Total Ordered Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','TOTAL_NUM_OF_ORDERS',222,'Total number of orders through the last ordered date','TOTAL_NUM_OF_ORDERS','','~T~D~2','','ANONYMOUS','NUMBER','HZ_PARTIES','TOTAL_NUM_OF_ORDERS','Total Num Of Orders','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','KNOWN_AS5',222,'An alias or other name by which a party is known','KNOWN_AS5','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','KNOWN_AS5','Known As5','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','KNOWN_AS4',222,'An alias or other name by which a party is known','KNOWN_AS4','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','KNOWN_AS4','Known As4','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','KNOWN_AS3',222,'An alias or other name by which a party is known','KNOWN_AS3','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','KNOWN_AS3','Known As3','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','KNOWN_AS2',222,'An alias or other name by which a party is known','KNOWN_AS2','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','KNOWN_AS2','Known As2','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','ATTRIBUTE15',222,'Descriptive flexfield segment','ATTRIBUTE15','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','ATTRIBUTE15','Attribute15','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','ATTRIBUTE14',222,'Descriptive flexfield segment','ATTRIBUTE14','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','ATTRIBUTE14','Attribute14','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','ATTRIBUTE13',222,'Descriptive flexfield segment','ATTRIBUTE13','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','ATTRIBUTE13','Attribute13','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','ATTRIBUTE12',222,'Descriptive flexfield segment','ATTRIBUTE12','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','ATTRIBUTE12','Attribute12','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','ATTRIBUTE11',222,'Descriptive flexfield segment','ATTRIBUTE11','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','ATTRIBUTE11','Attribute11','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','ATTRIBUTE10',222,'Descriptive flexfield segment','ATTRIBUTE10','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','ATTRIBUTE10','Attribute10','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','ATTRIBUTE9',222,'Descriptive flexfield segment','ATTRIBUTE9','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','ATTRIBUTE9','Attribute9','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','ATTRIBUTE8',222,'Descriptive flexfield segment','ATTRIBUTE8','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','ATTRIBUTE8','Attribute8','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','ATTRIBUTE7',222,'Descriptive flexfield segment','ATTRIBUTE7','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','ATTRIBUTE7','Attribute7','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','ATTRIBUTE6',222,'Descriptive flexfield segment','ATTRIBUTE6','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','ATTRIBUTE6','Attribute6','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','ATTRIBUTE5',222,'Descriptive flexfield segment','ATTRIBUTE5','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','ATTRIBUTE5','Attribute5','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','ATTRIBUTE4',222,'Descriptive flexfield segment','ATTRIBUTE4','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','ATTRIBUTE4','Attribute4','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','ATTRIBUTE3',222,'Descriptive flexfield segment','ATTRIBUTE3','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','ATTRIBUTE3','Attribute3','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','ATTRIBUTE_CATEGORY',222,'Descriptive flexfield structure definition column.','ATTRIBUTE_CATEGORY','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','ATTRIBUTE_CATEGORY','Attribute Category','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','WH_UPDATE_DATE',222,'No longer used','WH_UPDATE_DATE','','','','ANONYMOUS','DATE','HZ_PARTIES','WH_UPDATE_DATE','Wh Update Date','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','PROGRAM_UPDATE_DATE',222,'Concurrent Program who column - date when a program last updated this row).','PROGRAM_UPDATE_DATE','','','','ANONYMOUS','DATE','HZ_PARTIES','PROGRAM_UPDATE_DATE','Program Update Date','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','PROGRAM_ID',222,'Concurrent Program who column - program id of the program that last updated this row (foreign key to FND_CONCURRENT_PROGRAM.CONCURRENT_PROGRAM_ID).','PROGRAM_ID','','','','ANONYMOUS','NUMBER','HZ_PARTIES','PROGRAM_ID','Program Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','LAST_UPDATE_DATE',222,'Standard Who column - date when a user last updated this row.','LAST_UPDATE_DATE','','','','ANONYMOUS','DATE','HZ_PARTIES','LAST_UPDATE_DATE','Last Update Date','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','CREATED_BY',222,'Standard who column - user who created this row (foreign key to FND_USER.USER_ID).','CREATED_BY','','','','ANONYMOUS','NUMBER','HZ_PARTIES','CREATED_BY','Created By','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','PROGRAM_APPLICATION_ID',222,'Concurrent Program who column - application id of the program that last updated this row (foreign key to FND_APPLICATION.APPLICATION_ID).','PROGRAM_APPLICATION_ID','','','','ANONYMOUS','NUMBER','HZ_PARTIES','PROGRAM_APPLICATION_ID','Program Application Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','REQUEST_ID',222,'Concurrent Program who column - concurrent request id of the program that last updated this row (foreign key to FND_CONCURRENT_REQUESTS.REQUEST_ID).','REQUEST_ID','','','','ANONYMOUS','NUMBER','HZ_PARTIES','REQUEST_ID','Request Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','LAST_UPDATE_LOGIN',222,'Standard who column - operating system login of user who last updated this row (foreign key to FND_LOGINS.LOGIN_ID).','LAST_UPDATE_LOGIN','','','','ANONYMOUS','NUMBER','HZ_PARTIES','LAST_UPDATE_LOGIN','Last Update Login','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','CREATION_DATE',222,'Standard who column - date when this row was created.','CREATION_DATE','','','','ANONYMOUS','DATE','HZ_PARTIES','CREATION_DATE','Creation Date','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','LAST_UPDATED_BY',222,'Standard who column - user who last updated this row (foreign key to FND_USER.USER_ID).','LAST_UPDATED_BY','','','','ANONYMOUS','NUMBER','HZ_PARTIES','LAST_UPDATED_BY','Last Updated By','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','APPLICATION_ID',222,'TCA Who column','APPLICATION_ID','','','','ANONYMOUS','NUMBER','HZ_PARTIES','APPLICATION_ID','Application Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','GLOBAL_ATTRIBUTE20',222,'Not currently used','GLOBAL_ATTRIBUTE20','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','GLOBAL_ATTRIBUTE20','Global Attribute20','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','GLOBAL_ATTRIBUTE19',222,'Not currently used','GLOBAL_ATTRIBUTE19','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','GLOBAL_ATTRIBUTE19','Global Attribute19','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','GLOBAL_ATTRIBUTE18',222,'Not currently used','GLOBAL_ATTRIBUTE18','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','GLOBAL_ATTRIBUTE18','Global Attribute18','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','GLOBAL_ATTRIBUTE17',222,'Not currently used','GLOBAL_ATTRIBUTE17','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','GLOBAL_ATTRIBUTE17','Global Attribute17','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','GLOBAL_ATTRIBUTE16',222,'Not currently used','GLOBAL_ATTRIBUTE16','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','GLOBAL_ATTRIBUTE16','Global Attribute16','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','GLOBAL_ATTRIBUTE15',222,'Not currently used','GLOBAL_ATTRIBUTE15','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','GLOBAL_ATTRIBUTE15','Global Attribute15','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','GLOBAL_ATTRIBUTE14',222,'Not currently used','GLOBAL_ATTRIBUTE14','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','GLOBAL_ATTRIBUTE14','Global Attribute14','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','GLOBAL_ATTRIBUTE13',222,'Not currently used','GLOBAL_ATTRIBUTE13','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','GLOBAL_ATTRIBUTE13','Global Attribute13','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','GLOBAL_ATTRIBUTE12',222,'Not currently used','GLOBAL_ATTRIBUTE12','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','GLOBAL_ATTRIBUTE12','Global Attribute12','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','GLOBAL_ATTRIBUTE11',222,'Not currently used','GLOBAL_ATTRIBUTE11','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','GLOBAL_ATTRIBUTE11','Global Attribute11','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','GLOBAL_ATTRIBUTE10',222,'Not currently used','GLOBAL_ATTRIBUTE10','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','GLOBAL_ATTRIBUTE10','Global Attribute10','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','GLOBAL_ATTRIBUTE9',222,'Not currently used','GLOBAL_ATTRIBUTE9','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','GLOBAL_ATTRIBUTE9','Global Attribute9','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','GLOBAL_ATTRIBUTE8',222,'Not currently used','GLOBAL_ATTRIBUTE8','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','GLOBAL_ATTRIBUTE8','Global Attribute8','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','GLOBAL_ATTRIBUTE7',222,'Not currently used','GLOBAL_ATTRIBUTE7','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','GLOBAL_ATTRIBUTE7','Global Attribute7','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','GLOBAL_ATTRIBUTE6',222,'Not currently used','GLOBAL_ATTRIBUTE6','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','GLOBAL_ATTRIBUTE6','Global Attribute6','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','GLOBAL_ATTRIBUTE5',222,'Not currently used','GLOBAL_ATTRIBUTE5','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','GLOBAL_ATTRIBUTE5','Global Attribute5','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','GLOBAL_ATTRIBUTE3',222,'Not currently used','GLOBAL_ATTRIBUTE3','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','GLOBAL_ATTRIBUTE3','Global Attribute3','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','GLOBAL_ATTRIBUTE4',222,'Not currently used','GLOBAL_ATTRIBUTE4','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','GLOBAL_ATTRIBUTE4','Global Attribute4','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','GLOBAL_ATTRIBUTE2',222,'Not currently used','GLOBAL_ATTRIBUTE2','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','GLOBAL_ATTRIBUTE2','Global Attribute2','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','GLOBAL_ATTRIBUTE1',222,'Not currently used','GLOBAL_ATTRIBUTE1','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','GLOBAL_ATTRIBUTE1','Global Attribute1','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','GLOBAL_ATTRIBUTE_CATEGORY',222,'Not currently used','GLOBAL_ATTRIBUTE_CATEGORY','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','GLOBAL_ATTRIBUTE_CATEGORY','Global Attribute Category','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','ATTRIBUTE1',222,'DFF Context Code - Global Data Elements','ATTRIBUTE1','','','','ANONYMOUS','','HZ_PARTIES','ATTRIBUTE1','Party Type','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_PARTIES','ATTRIBUTE2',222,'DFF Context Code - Global Data Elements','ATTRIBUTE2','','','','ANONYMOUS','','HZ_PARTIES','ATTRIBUTE2','GVID_ID','','','','US');
--Inserting Object Components for HZ_PARTIES
--Inserting Object Component Joins for HZ_PARTIES
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 222');
END IF;
END;
/
--Exporting View Component Data of the View -  XXEIS_AR_CUST_LIST_SUMMARY_V
prompt Creating Object Data HZ_LOCATIONS
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(222);
IF mod_exist = 'Y' THEN 
--Inserting Object HZ_LOCATIONS
xxeis.eis_rsc_ins.v( 'HZ_LOCATIONS',222,'Physical addresses','1.0','','','MM027735','APPS','Hz Locations','HL','','','SYNONYM','US','','');
--Delete Object Columns for HZ_LOCATIONS
xxeis.eis_rsc_utility.delete_view_rows('HZ_LOCATIONS',222,FALSE);
--Inserting Object Columns for HZ_LOCATIONS
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','COUNTRY',222,'Country code from the TERRITORY_CODE column in the FND_TERRITORY table','COUNTRY','','','','MM027735','VARCHAR2','HZ_LOCATIONS','COUNTRY','Country','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','ADDRESS1',222,'First line for address','ADDRESS1','','','','MM027735','VARCHAR2','HZ_LOCATIONS','ADDRESS1','Address1','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','ADDRESS2',222,'Second line for address','ADDRESS2','','','','MM027735','VARCHAR2','HZ_LOCATIONS','ADDRESS2','Address2','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','ADDRESS3',222,'Third line for address','ADDRESS3','','','','MM027735','VARCHAR2','HZ_LOCATIONS','ADDRESS3','Address3','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','ADDRESS4',222,'Fourth line for address','ADDRESS4','','','','MM027735','VARCHAR2','HZ_LOCATIONS','ADDRESS4','Address4','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','CITY',222,'City','CITY','','','','MM027735','VARCHAR2','HZ_LOCATIONS','CITY','City','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','POSTAL_CODE',222,'Postal code','POSTAL_CODE','','','','MM027735','VARCHAR2','HZ_LOCATIONS','POSTAL_CODE','Postal Code','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','STATE',222,'State','STATE','','','','MM027735','VARCHAR2','HZ_LOCATIONS','STATE','State','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','COUNTY',222,'County','COUNTY','','','','MM027735','VARCHAR2','HZ_LOCATIONS','COUNTY','County','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','SALES_TAX_GEOCODE',222,'US Sales Tax Jurisdiction code. Use this field to provide a Jurisdiction Code (also called as Geocode) defined by wither Vertex or Taxware. This value is passed as a ship-to locaiton jurisdiction code to the tax partner API.','SALES_TAX_GEOCODE','','','','MM027735','VARCHAR2','HZ_LOCATIONS','SALES_TAX_GEOCODE','Sales Tax Geocode','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','SALES_TAX_INSIDE_CITY_LIMITS',222,'Indicates if the location is inside the boundary of a city. Used to calculate state and local taxes in the United States. Value ''1'' is for locations inside the city limits and ''0'' is for locations outside the city limits. Defaults to null i','SALES_TAX_INSIDE_CITY_LIMITS','','','','MM027735','VARCHAR2','HZ_LOCATIONS','SALES_TAX_INSIDE_CITY_LIMITS','Sales Tax Inside City Limits','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','VALIDATION_STATUS_CODE',222,'Standardized status code describing the results of the validation','VALIDATION_STATUS_CODE','','','','MM027735','VARCHAR2','HZ_LOCATIONS','VALIDATION_STATUS_CODE','Validation Status Code','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','DATE_VALIDATED',222,'Date the address was last validated.','DATE_VALIDATED','','','','MM027735','DATE','HZ_LOCATIONS','DATE_VALIDATED','Date Validated','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','DO_NOT_VALIDATE_FLAG',222,'A new attribute that indicates that a particular location is not eligible for address validatino regardless of the current Validation Status Code. The default is "No" meaning that by default, all address are eligible for validation.  This a','DO_NOT_VALIDATE_FLAG','','','','MM027735','VARCHAR2','HZ_LOCATIONS','DO_NOT_VALIDATE_FLAG','Do Not Validate Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','LOCATION_ID',222,'Unique identifier for this location','LOCATION_ID','','','','MM027735','NUMBER','HZ_LOCATIONS','LOCATION_ID','Location Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','ATTRIBUTE15',222,'Descriptive flexfield segment','ATTRIBUTE15','','','','MM027735','VARCHAR2','HZ_LOCATIONS','ATTRIBUTE15','Attribute15','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','GEOMETRY_SOURCE',222,'Source of Geometry Information. This column is only used by Site Hub.','GEOMETRY_SOURCE','','','','MM027735','VARCHAR2','HZ_LOCATIONS','GEOMETRY_SOURCE','Geometry Source','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','GEOMETRY_STATUS_CODE',222,'Spatial data integration status for given location.','GEOMETRY_STATUS_CODE','','','','MM027735','VARCHAR2','HZ_LOCATIONS','GEOMETRY_STATUS_CODE','Geometry Status Code','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','ACTUAL_CONTENT_SOURCE',222,'Additional Content Source Type column for backward compatibility.','ACTUAL_CONTENT_SOURCE','','','','MM027735','VARCHAR2','HZ_LOCATIONS','ACTUAL_CONTENT_SOURCE','Actual Content Source','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','GEOMETRY',222,'','GEOMETRY','','','','MM027735','SDO_GEOMETRY','HZ_LOCATIONS','GEOMETRY','Geometry','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','OBJECT_VERSION_NUMBER',222,'This column is used for locking purposes','OBJECT_VERSION_NUMBER','','','','MM027735','NUMBER','HZ_LOCATIONS','OBJECT_VERSION_NUMBER','Object Version Number','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','CREATED_BY_MODULE',222,'TCA Who column','CREATED_BY_MODULE','','','','MM027735','VARCHAR2','HZ_LOCATIONS','CREATED_BY_MODULE','Created By Module','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','APPLICATION_ID',222,'TCA Who column','APPLICATION_ID','','','','MM027735','NUMBER','HZ_LOCATIONS','APPLICATION_ID','Application Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','TIMEZONE_ID',222,'Time zone identifier. Foreign key to HZ_TIMEZONES','TIMEZONE_ID','','','','MM027735','NUMBER','HZ_LOCATIONS','TIMEZONE_ID','Timezone Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','GLOBAL_ATTRIBUTE4',222,'Not currently used','GLOBAL_ATTRIBUTE4','','','','MM027735','VARCHAR2','HZ_LOCATIONS','GLOBAL_ATTRIBUTE4','Global Attribute4','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','GLOBAL_ATTRIBUTE5',222,'Not currently used','GLOBAL_ATTRIBUTE5','','','','MM027735','VARCHAR2','HZ_LOCATIONS','GLOBAL_ATTRIBUTE5','Global Attribute5','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','GLOBAL_ATTRIBUTE6',222,'Not currently used','GLOBAL_ATTRIBUTE6','','','','MM027735','VARCHAR2','HZ_LOCATIONS','GLOBAL_ATTRIBUTE6','Global Attribute6','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','GLOBAL_ATTRIBUTE7',222,'Not currently used','GLOBAL_ATTRIBUTE7','','','','MM027735','VARCHAR2','HZ_LOCATIONS','GLOBAL_ATTRIBUTE7','Global Attribute7','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','GLOBAL_ATTRIBUTE8',222,'Not currently used','GLOBAL_ATTRIBUTE8','','','','MM027735','VARCHAR2','HZ_LOCATIONS','GLOBAL_ATTRIBUTE8','Global Attribute8','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','GLOBAL_ATTRIBUTE9',222,'Not currently used','GLOBAL_ATTRIBUTE9','','','','MM027735','VARCHAR2','HZ_LOCATIONS','GLOBAL_ATTRIBUTE9','Global Attribute9','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','GLOBAL_ATTRIBUTE10',222,'Not currently used','GLOBAL_ATTRIBUTE10','','','','MM027735','VARCHAR2','HZ_LOCATIONS','GLOBAL_ATTRIBUTE10','Global Attribute10','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','GLOBAL_ATTRIBUTE11',222,'Not currently used','GLOBAL_ATTRIBUTE11','','','','MM027735','VARCHAR2','HZ_LOCATIONS','GLOBAL_ATTRIBUTE11','Global Attribute11','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','GLOBAL_ATTRIBUTE12',222,'Not currently used','GLOBAL_ATTRIBUTE12','','','','MM027735','VARCHAR2','HZ_LOCATIONS','GLOBAL_ATTRIBUTE12','Global Attribute12','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','GLOBAL_ATTRIBUTE13',222,'Not currently used','GLOBAL_ATTRIBUTE13','','','','MM027735','VARCHAR2','HZ_LOCATIONS','GLOBAL_ATTRIBUTE13','Global Attribute13','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','GLOBAL_ATTRIBUTE14',222,'Not currently used','GLOBAL_ATTRIBUTE14','','','','MM027735','VARCHAR2','HZ_LOCATIONS','GLOBAL_ATTRIBUTE14','Global Attribute14','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','GLOBAL_ATTRIBUTE15',222,'Not currently used','GLOBAL_ATTRIBUTE15','','','','MM027735','VARCHAR2','HZ_LOCATIONS','GLOBAL_ATTRIBUTE15','Global Attribute15','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','GLOBAL_ATTRIBUTE16',222,'Not currently used','GLOBAL_ATTRIBUTE16','','','','MM027735','VARCHAR2','HZ_LOCATIONS','GLOBAL_ATTRIBUTE16','Global Attribute16','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','GLOBAL_ATTRIBUTE17',222,'Not currently used','GLOBAL_ATTRIBUTE17','','','','MM027735','VARCHAR2','HZ_LOCATIONS','GLOBAL_ATTRIBUTE17','Global Attribute17','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','GLOBAL_ATTRIBUTE18',222,'Not currently used','GLOBAL_ATTRIBUTE18','','','','MM027735','VARCHAR2','HZ_LOCATIONS','GLOBAL_ATTRIBUTE18','Global Attribute18','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','GLOBAL_ATTRIBUTE19',222,'Not currently used','GLOBAL_ATTRIBUTE19','','','','MM027735','VARCHAR2','HZ_LOCATIONS','GLOBAL_ATTRIBUTE19','Global Attribute19','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','GLOBAL_ATTRIBUTE20',222,'Not currently used','GLOBAL_ATTRIBUTE20','','','','MM027735','VARCHAR2','HZ_LOCATIONS','GLOBAL_ATTRIBUTE20','Global Attribute20','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','ORIG_SYSTEM_REFERENCE',222,'Address identifier from foreign system','ORIG_SYSTEM_REFERENCE','','','','MM027735','VARCHAR2','HZ_LOCATIONS','ORIG_SYSTEM_REFERENCE','Orig System Reference','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','PROVINCE',222,'Province','PROVINCE','','','','MM027735','VARCHAR2','HZ_LOCATIONS','PROVINCE','Province','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','ADDRESS_KEY',222,'Derived key that factilitates fuzzy searches','ADDRESS_KEY','','','','MM027735','VARCHAR2','HZ_LOCATIONS','ADDRESS_KEY','Address Key','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','ADDRESS_STYLE',222,'Used as context value for Flexible Address Format descriptive flexfield. Do not use this column, join to the FND_TERRITORIES table via COUNTRY = TERRITORY_CODE to retrieve the address style for the country.','ADDRESS_STYLE','','','','MM027735','VARCHAR2','HZ_LOCATIONS','ADDRESS_STYLE','Address Style','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','VALIDATED_FLAG',222,'Indicates if the location was validated. Y for validated, N for unvalidated.','VALIDATED_FLAG','','','','MM027735','VARCHAR2','HZ_LOCATIONS','VALIDATED_FLAG','Validated Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','ADDRESS_LINES_PHONETIC',222,'Phonetic or Kana representation of the Kanji address lines (used in Japan)','ADDRESS_LINES_PHONETIC','','','','MM027735','VARCHAR2','HZ_LOCATIONS','ADDRESS_LINES_PHONETIC','Address Lines Phonetic','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','APARTMENT_FLAG',222,'No longer used','APARTMENT_FLAG','','','','MM027735','VARCHAR2','HZ_LOCATIONS','APARTMENT_FLAG','Apartment Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','PO_BOX_NUMBER',222,'No longer used','PO_BOX_NUMBER','','','','MM027735','VARCHAR2','HZ_LOCATIONS','PO_BOX_NUMBER','Po Box Number','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','HOUSE_NUMBER',222,'No longer used','HOUSE_NUMBER','','','','MM027735','VARCHAR2','HZ_LOCATIONS','HOUSE_NUMBER','House Number','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','STREET_SUFFIX',222,'No longer used','STREET_SUFFIX','','','','MM027735','VARCHAR2','HZ_LOCATIONS','STREET_SUFFIX','Street Suffix','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','APARTMENT_NUMBER',222,'No longer used','APARTMENT_NUMBER','','','','MM027735','VARCHAR2','HZ_LOCATIONS','APARTMENT_NUMBER','Apartment Number','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','SECONDARY_SUFFIX_ELEMENT',222,'No longer used','SECONDARY_SUFFIX_ELEMENT','','','','MM027735','VARCHAR2','HZ_LOCATIONS','SECONDARY_SUFFIX_ELEMENT','Secondary Suffix Element','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','STREET',222,'No longer used','STREET','','','','MM027735','VARCHAR2','HZ_LOCATIONS','STREET','Street','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','RURAL_ROUTE_TYPE',222,'No longer used','RURAL_ROUTE_TYPE','','','','MM027735','VARCHAR2','HZ_LOCATIONS','RURAL_ROUTE_TYPE','Rural Route Type','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','RURAL_ROUTE_NUMBER',222,'No longer used','RURAL_ROUTE_NUMBER','','','','MM027735','VARCHAR2','HZ_LOCATIONS','RURAL_ROUTE_NUMBER','Rural Route Number','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','STREET_NUMBER',222,'No longer used','STREET_NUMBER','','','','MM027735','VARCHAR2','HZ_LOCATIONS','STREET_NUMBER','Street Number','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','BUILDING',222,'No longer used','BUILDING','','','','MM027735','VARCHAR2','HZ_LOCATIONS','BUILDING','Building','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','FLOOR',222,'No longer used','FLOOR','','','','MM027735','VARCHAR2','HZ_LOCATIONS','FLOOR','Floor','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','SUITE',222,'No longer used','SUITE','','','','MM027735','VARCHAR2','HZ_LOCATIONS','SUITE','Suite','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','ROOM',222,'No longer used','ROOM','','','','MM027735','VARCHAR2','HZ_LOCATIONS','ROOM','Room','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','POSTAL_PLUS4_CODE',222,'Four digit extension to the United States Postal ZIP code.','POSTAL_PLUS4_CODE','','','','MM027735','VARCHAR2','HZ_LOCATIONS','POSTAL_PLUS4_CODE','Postal Plus4 Code','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','TIME_ZONE',222,'No longer used','TIME_ZONE','','','','MM027735','VARCHAR2','HZ_LOCATIONS','TIME_ZONE','Time Zone','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','OVERSEAS_ADDRESS_FLAG',222,'No longer used','OVERSEAS_ADDRESS_FLAG','','','','MM027735','VARCHAR2','HZ_LOCATIONS','OVERSEAS_ADDRESS_FLAG','Overseas Address Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','POST_OFFICE',222,'No longer used','POST_OFFICE','','','','MM027735','VARCHAR2','HZ_LOCATIONS','POST_OFFICE','Post Office','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','POSITION',222,'The primary direction such as North or East that is used to access the location.','POSITION','','','','MM027735','VARCHAR2','HZ_LOCATIONS','POSITION','Position','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','DELIVERY_POINT_CODE',222,'No longer used','DELIVERY_POINT_CODE','','','','MM027735','VARCHAR2','HZ_LOCATIONS','DELIVERY_POINT_CODE','Delivery Point Code','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','LOCATION_DIRECTIONS',222,'Directions to the location','LOCATION_DIRECTIONS','','','','MM027735','VARCHAR2','HZ_LOCATIONS','LOCATION_DIRECTIONS','Location Directions','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','ADDRESS_EFFECTIVE_DATE',222,'Date when the location is valid.','ADDRESS_EFFECTIVE_DATE','','','','MM027735','DATE','HZ_LOCATIONS','ADDRESS_EFFECTIVE_DATE','Address Effective Date','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','ADDRESS_EXPIRATION_DATE',222,'Date when the location is no longer valid','ADDRESS_EXPIRATION_DATE','','','','MM027735','DATE','HZ_LOCATIONS','ADDRESS_EXPIRATION_DATE','Address Expiration Date','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','ADDRESS_ERROR_CODE',222,'No longer used','ADDRESS_ERROR_CODE','','','','MM027735','VARCHAR2','HZ_LOCATIONS','ADDRESS_ERROR_CODE','Address Error Code','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','CLLI_CODE',222,'Common Language Location Identifier (CLLI) code','CLLI_CODE','','','','MM027735','VARCHAR2','HZ_LOCATIONS','CLLI_CODE','Clli Code','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','DODAAC',222,'No longer used','DODAAC','','','','MM027735','VARCHAR2','HZ_LOCATIONS','DODAAC','Dodaac','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','TRAILING_DIRECTORY_CODE',222,'No longer used','TRAILING_DIRECTORY_CODE','','','','MM027735','VARCHAR2','HZ_LOCATIONS','TRAILING_DIRECTORY_CODE','Trailing Directory Code','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','LANGUAGE',222,'Operating language of the location. Foreign key to the FND_LANGUAGES table','LANGUAGE','','','','MM027735','VARCHAR2','HZ_LOCATIONS','LANGUAGE','Language','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','LIFE_CYCLE_STATUS',222,'No longer used','LIFE_CYCLE_STATUS','','','','MM027735','VARCHAR2','HZ_LOCATIONS','LIFE_CYCLE_STATUS','Life Cycle Status','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','SHORT_DESCRIPTION',222,'Short description of the location','SHORT_DESCRIPTION','','','','MM027735','VARCHAR2','HZ_LOCATIONS','SHORT_DESCRIPTION','Short Description','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','DESCRIPTION',222,'An extensive description of the location','DESCRIPTION','','','','MM027735','VARCHAR2','HZ_LOCATIONS','DESCRIPTION','Description','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','CONTENT_SOURCE_TYPE',222,'Source of data content.','CONTENT_SOURCE_TYPE','','','','MM027735','VARCHAR2','HZ_LOCATIONS','CONTENT_SOURCE_TYPE','Content Source Type','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','LOC_HIERARCHY_ID',222,'Location hierarchy identifier. Foreign key to the JTF_LOC_HIERARCHIES_B table','LOC_HIERARCHY_ID','','','','MM027735','NUMBER','HZ_LOCATIONS','LOC_HIERARCHY_ID','Loc Hierarchy Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','LAST_UPDATE_DATE',222,'Standard Who column - date when a user last updated this row.','LAST_UPDATE_DATE','','','','MM027735','DATE','HZ_LOCATIONS','LAST_UPDATE_DATE','Last Update Date','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','LAST_UPDATED_BY',222,'Standard who column - user who last updated this row (foreign key to FND_USER.USER_ID).','LAST_UPDATED_BY','','','','MM027735','NUMBER','HZ_LOCATIONS','LAST_UPDATED_BY','Last Updated By','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','CREATION_DATE',222,'Standard who column - date when this row was created.','CREATION_DATE','','','','MM027735','DATE','HZ_LOCATIONS','CREATION_DATE','Creation Date','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','CREATED_BY',222,'Standard who column - user who created this row (foreign key to FND_USER.USER_ID).','CREATED_BY','','','','MM027735','NUMBER','HZ_LOCATIONS','CREATED_BY','Created By','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','LAST_UPDATE_LOGIN',222,'Standard who column - operating system login of user who last updated this row (foreign key to FND_LOGINS.LOGIN_ID).','LAST_UPDATE_LOGIN','','','','MM027735','NUMBER','HZ_LOCATIONS','LAST_UPDATE_LOGIN','Last Update Login','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','REQUEST_ID',222,'Concurrent Program who column - concurrent request id of the program that last updated this row (foreign key to FND_CONCURRENT_REQUESTS.REQUEST_ID).','REQUEST_ID','','','','MM027735','NUMBER','HZ_LOCATIONS','REQUEST_ID','Request Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','PROGRAM_APPLICATION_ID',222,'Concurrent Program who column - application id of the program that last updated this row (foreign key to FND_APPLICATION.APPLICATION_ID).','PROGRAM_APPLICATION_ID','','','','MM027735','NUMBER','HZ_LOCATIONS','PROGRAM_APPLICATION_ID','Program Application Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','PROGRAM_ID',222,'Concurrent Program who column - program id of the program that last updated this row (foreign key to FND_CONCURRENT_PROGRAM.CONCURRENT_PROGRAM_ID).','PROGRAM_ID','','','','MM027735','NUMBER','HZ_LOCATIONS','PROGRAM_ID','Program Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','PROGRAM_UPDATE_DATE',222,'Concurrent Program who column - date when a program last updated this row).','PROGRAM_UPDATE_DATE','','','','MM027735','DATE','HZ_LOCATIONS','PROGRAM_UPDATE_DATE','Program Update Date','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','WH_UPDATE_DATE',222,'No longer used','WH_UPDATE_DATE','','','','MM027735','DATE','HZ_LOCATIONS','WH_UPDATE_DATE','Wh Update Date','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','ATTRIBUTE_CATEGORY',222,'Descriptive flexfield structure definition column.','ATTRIBUTE_CATEGORY','','','','MM027735','VARCHAR2','HZ_LOCATIONS','ATTRIBUTE_CATEGORY','Attribute Category','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','ATTRIBUTE4',222,'Descriptive flexfield segment','ATTRIBUTE4','','','','MM027735','VARCHAR2','HZ_LOCATIONS','ATTRIBUTE4','Attribute4','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','ATTRIBUTE5',222,'Descriptive flexfield segment','ATTRIBUTE5','','','','MM027735','VARCHAR2','HZ_LOCATIONS','ATTRIBUTE5','Attribute5','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','ATTRIBUTE6',222,'Descriptive flexfield segment','ATTRIBUTE6','','','','MM027735','VARCHAR2','HZ_LOCATIONS','ATTRIBUTE6','Attribute6','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','ATTRIBUTE7',222,'Descriptive flexfield segment','ATTRIBUTE7','','','','MM027735','VARCHAR2','HZ_LOCATIONS','ATTRIBUTE7','Attribute7','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','ATTRIBUTE8',222,'Descriptive flexfield segment','ATTRIBUTE8','','','','MM027735','VARCHAR2','HZ_LOCATIONS','ATTRIBUTE8','Attribute8','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','ATTRIBUTE9',222,'Descriptive flexfield segment','ATTRIBUTE9','','','','MM027735','VARCHAR2','HZ_LOCATIONS','ATTRIBUTE9','Attribute9','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','ATTRIBUTE10',222,'Descriptive flexfield segment','ATTRIBUTE10','','','','MM027735','VARCHAR2','HZ_LOCATIONS','ATTRIBUTE10','Attribute10','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','ATTRIBUTE11',222,'Descriptive flexfield segment','ATTRIBUTE11','','','','MM027735','VARCHAR2','HZ_LOCATIONS','ATTRIBUTE11','Attribute11','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','ATTRIBUTE12',222,'Descriptive flexfield segment','ATTRIBUTE12','','','','MM027735','VARCHAR2','HZ_LOCATIONS','ATTRIBUTE12','Attribute12','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','ATTRIBUTE13',222,'Descriptive flexfield segment','ATTRIBUTE13','','','','MM027735','VARCHAR2','HZ_LOCATIONS','ATTRIBUTE13','Attribute13','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','ATTRIBUTE14',222,'Descriptive flexfield segment','ATTRIBUTE14','','','','MM027735','VARCHAR2','HZ_LOCATIONS','ATTRIBUTE14','Attribute14','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','ATTRIBUTE16',222,'Descriptive Flexfield segment','ATTRIBUTE16','','','','MM027735','VARCHAR2','HZ_LOCATIONS','ATTRIBUTE16','Attribute16','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','ATTRIBUTE17',222,'Descriptive Flexfield segment','ATTRIBUTE17','','','','MM027735','VARCHAR2','HZ_LOCATIONS','ATTRIBUTE17','Attribute17','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','ATTRIBUTE18',222,'Descriptive Flexfield segment','ATTRIBUTE18','','','','MM027735','VARCHAR2','HZ_LOCATIONS','ATTRIBUTE18','Attribute18','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','ATTRIBUTE19',222,'Descriptive Flexfield segment','ATTRIBUTE19','','','','MM027735','VARCHAR2','HZ_LOCATIONS','ATTRIBUTE19','Attribute19','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','ATTRIBUTE20',222,'Descriptive Flexfield segment','ATTRIBUTE20','','','','MM027735','VARCHAR2','HZ_LOCATIONS','ATTRIBUTE20','Attribute20','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','GLOBAL_ATTRIBUTE_CATEGORY',222,'Not currently used','GLOBAL_ATTRIBUTE_CATEGORY','','','','MM027735','VARCHAR2','HZ_LOCATIONS','GLOBAL_ATTRIBUTE_CATEGORY','Global Attribute Category','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','GLOBAL_ATTRIBUTE1',222,'Not currently used','GLOBAL_ATTRIBUTE1','','','','MM027735','VARCHAR2','HZ_LOCATIONS','GLOBAL_ATTRIBUTE1','Global Attribute1','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','GLOBAL_ATTRIBUTE2',222,'Not currently used','GLOBAL_ATTRIBUTE2','','','','MM027735','VARCHAR2','HZ_LOCATIONS','GLOBAL_ATTRIBUTE2','Global Attribute2','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','GLOBAL_ATTRIBUTE3',222,'Not currently used','GLOBAL_ATTRIBUTE3','','','','MM027735','VARCHAR2','HZ_LOCATIONS','GLOBAL_ATTRIBUTE3','Global Attribute3','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','FA_LOCATION_ID',222,'Foreign key to the FA_LOCATIONS table','FA_LOCATION_ID','','','','MM027735','NUMBER','HZ_LOCATIONS','FA_LOCATION_ID','Fa Location Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','ATTRIBUTE1',222,'DFF Context Code - Global Data Elements','ATTRIBUTE1','','','','MM027735','','HZ_LOCATIONS','ATTRIBUTE1','Pay To Vendor ID','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','ATTRIBUTE2',222,'DFF Context Code - Global Data Elements','ATTRIBUTE2','','','','MM027735','','HZ_LOCATIONS','ATTRIBUTE2','LOB','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_LOCATIONS','ATTRIBUTE3',222,'DFF Context Code - Global Data Elements','ATTRIBUTE3','','','','MM027735','','HZ_LOCATIONS','ATTRIBUTE3','Pay To Vendor Code','','','','US');
--Inserting Object Components for HZ_LOCATIONS
--Inserting Object Component Joins for HZ_LOCATIONS
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 222');
END IF;
END;
/
--Exporting View Component Data of the View -  XXEIS_AR_CUST_LIST_SUMMARY_V
prompt Creating Object Data HZ_CUST_ACCT_SITES
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(222);
IF mod_exist = 'Y' THEN 
--Inserting Object HZ_CUST_ACCT_SITES
xxeis.eis_rsc_ins.v( 'HZ_CUST_ACCT_SITES',222,'Stores all customer account sites across all operating units','1.0','','','MM027735','APPS','Hz Cust Acct Sites','HCAS','','','SYNONYM','US','','');
--Delete Object Columns for HZ_CUST_ACCT_SITES
xxeis.eis_rsc_utility.delete_view_rows('HZ_CUST_ACCT_SITES',222,FALSE);
--Inserting Object Columns for HZ_CUST_ACCT_SITES
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','CREATION_DATE',222,'Standard who column - date when this row was created.','CREATION_DATE','','','','MM027735','DATE','HZ_CUST_ACCT_SITES','CREATION_DATE','Creation Date','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','CREATED_BY',222,'Standard who column - user who created this row (foreign key to FND_USER.USER_ID).','CREATED_BY','','','','MM027735','NUMBER','HZ_CUST_ACCT_SITES','CREATED_BY','Created By','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','REQUEST_ID',222,'Concurrent Program who column - concurrent request id of the program that last updated this row (foreign key to FND_CONCURRENT_REQUESTS.REQUEST_ID).','REQUEST_ID','','','','MM027735','NUMBER','HZ_CUST_ACCT_SITES','REQUEST_ID','Request Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','PROGRAM_APPLICATION_ID',222,'Concurrent Program who column - application id of the program that last updated this row (foreign key to FND_APPLICATION.APPLICATION_ID).','PROGRAM_APPLICATION_ID','','','','MM027735','NUMBER','HZ_CUST_ACCT_SITES','PROGRAM_APPLICATION_ID','Program Application Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','PROGRAM_ID',222,'Concurrent Program who column - program id of the program that last updated this row (foreign key to FND_CONCURRENT_PROGRAM.CONCURRENT_PROGRAM_ID).','PROGRAM_ID','','','','MM027735','NUMBER','HZ_CUST_ACCT_SITES','PROGRAM_ID','Program Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','PROGRAM_UPDATE_DATE',222,'Concurrent Program who column - date when a program last updated this row).','PROGRAM_UPDATE_DATE','','','','MM027735','DATE','HZ_CUST_ACCT_SITES','PROGRAM_UPDATE_DATE','Program Update Date','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','WH_UPDATE_DATE',222,'No longer used','WH_UPDATE_DATE','','','','MM027735','DATE','HZ_CUST_ACCT_SITES','WH_UPDATE_DATE','Wh Update Date','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','STATUS',222,'Customer Status flag. Receivables lookup code for CODE_STATUS','STATUS','','','','MM027735','VARCHAR2','HZ_CUST_ACCT_SITES','STATUS','Status','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','BILL_TO_FLAG',222,'Indicates if this is a Bill-To site. Y for a Bill-To site, P for the primary Bill-To site, and N for a site that is not a Bill-To site.','BILL_TO_FLAG','','','','MM027735','VARCHAR2','HZ_CUST_ACCT_SITES','BILL_TO_FLAG','Bill To Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','MARKET_FLAG',222,'Indicates if this is a Marketing site. Y for a Marketing site, P for the primary Marketing site, and N for a site that is not a Marketing site.','MARKET_FLAG','','','','MM027735','VARCHAR2','HZ_CUST_ACCT_SITES','MARKET_FLAG','Market Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','SHIP_TO_FLAG',222,'Indicates if this is a Ship-To site. Y for a Ship-To site, P for the primary Ship-To site, and N for a site that is not a Ship-To site.','SHIP_TO_FLAG','','','','MM027735','VARCHAR2','HZ_CUST_ACCT_SITES','SHIP_TO_FLAG','Ship To Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','LANGUAGE',222,'No Longer Used.','LANGUAGE','','','','MM027735','VARCHAR2','HZ_CUST_ACCT_SITES','LANGUAGE','Language','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','KEY_ACCOUNT_FLAG',222,'Key account for sales representatives','KEY_ACCOUNT_FLAG','','','','MM027735','VARCHAR2','HZ_CUST_ACCT_SITES','KEY_ACCOUNT_FLAG','Key Account Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','TP_HEADER_ID',222,'Trading partner header identifier','TP_HEADER_ID','','','','MM027735','NUMBER','HZ_CUST_ACCT_SITES','TP_HEADER_ID','Tp Header Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','ECE_TP_LOCATION_CODE',222,'Stores the EDI location code. It should be unique for a customer. The column accepts free-form text.','ECE_TP_LOCATION_CODE','','','','MM027735','VARCHAR2','HZ_CUST_ACCT_SITES','ECE_TP_LOCATION_CODE','Ece Tp Location Code','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','SERVICE_TERRITORY_ID',222,'No longer used','SERVICE_TERRITORY_ID','','','','MM027735','NUMBER','HZ_CUST_ACCT_SITES','SERVICE_TERRITORY_ID','Service Territory Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','PRIMARY_SPECIALIST_ID',222,'Used for the service personnel dispatching feature in Incident Tracking. Foreign key to the PER_PEOPLE table','PRIMARY_SPECIALIST_ID','','','','MM027735','NUMBER','HZ_CUST_ACCT_SITES','PRIMARY_SPECIALIST_ID','Primary Specialist Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','SECONDARY_SPECIALIST_ID',222,'Used for the service personnel dispatching feature in Incident Tracking. Foreign key to the PER_PEOPLE table.','SECONDARY_SPECIALIST_ID','','','','MM027735','NUMBER','HZ_CUST_ACCT_SITES','SECONDARY_SPECIALIST_ID','Secondary Specialist Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','TERRITORY_ID',222,'Identifier for the territory','TERRITORY_ID','','','','MM027735','NUMBER','HZ_CUST_ACCT_SITES','TERRITORY_ID','Territory Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','ADDRESS_TEXT',222,'Address text used for context search','ADDRESS_TEXT','','','','MM027735','CLOB','HZ_CUST_ACCT_SITES','ADDRESS_TEXT','Address Text','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','TERRITORY',222,'Not Currently Used. The Territory column should be used to store a NLS Territory as defined in Oracle database.  It will be used for future mulitple language support enhancements.','TERRITORY','','','','MM027735','VARCHAR2','HZ_CUST_ACCT_SITES','TERRITORY','Territory','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','TRANSLATED_CUSTOMER_NAME',222,'Translated customer name','TRANSLATED_CUSTOMER_NAME','','','','MM027735','VARCHAR2','HZ_CUST_ACCT_SITES','TRANSLATED_CUSTOMER_NAME','Translated Customer Name','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','CREATED_BY_MODULE',222,'TCA Who column','CREATED_BY_MODULE','','','','MM027735','VARCHAR2','HZ_CUST_ACCT_SITES','CREATED_BY_MODULE','Created By Module','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','APPLICATION_ID',222,'TCA Who column','APPLICATION_ID','','','','MM027735','NUMBER','HZ_CUST_ACCT_SITES','APPLICATION_ID','Application Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','ATTRIBUTE1',222,'DFF Context Code - Global Data Elements','ATTRIBUTE1','','','','MM027735','','HZ_CUST_ACCT_SITES','ATTRIBUTE1','Print Prices on Order','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','CUST_ACCT_SITE_ID',222,'Customer site identifier','CUST_ACCT_SITE_ID','','','','MM027735','NUMBER','HZ_CUST_ACCT_SITES','CUST_ACCT_SITE_ID','Cust Acct Site Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','CUST_ACCOUNT_ID',222,'Identifier for a customer account. Foreign key to the HZ_CUST_ACCOUNTS table','CUST_ACCOUNT_ID','','','','MM027735','NUMBER','HZ_CUST_ACCT_SITES','CUST_ACCOUNT_ID','Cust Account Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','PARTY_SITE_ID',222,'Identifier for a party site. Foreign key to the HZ_PARTY_SITES table','PARTY_SITE_ID','','','','MM027735','NUMBER','HZ_CUST_ACCT_SITES','PARTY_SITE_ID','Party Site Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','LAST_UPDATE_DATE',222,'Standard Who column - date when a user last updated this row.','LAST_UPDATE_DATE','','','','MM027735','DATE','HZ_CUST_ACCT_SITES','LAST_UPDATE_DATE','Last Update Date','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','LAST_UPDATED_BY',222,'Standard who column - user who last updated this row (foreign key to FND_USER.USER_ID).','LAST_UPDATED_BY','','','','MM027735','NUMBER','HZ_CUST_ACCT_SITES','LAST_UPDATED_BY','Last Updated By','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','LAST_UPDATE_LOGIN',222,'Standard who column - operating system login of user who last updated this row (foreign key to FND_LOGINS.LOGIN_ID).','LAST_UPDATE_LOGIN','','','','MM027735','NUMBER','HZ_CUST_ACCT_SITES','LAST_UPDATE_LOGIN','Last Update Login','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','ATTRIBUTE_CATEGORY',222,'Descriptive flexfield structure definition column.','ATTRIBUTE_CATEGORY','','','','MM027735','VARCHAR2','HZ_CUST_ACCT_SITES','ATTRIBUTE_CATEGORY','Attribute Category','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','ATTRIBUTE2',222,'Descriptive flexfield segment','ATTRIBUTE2','','','','MM027735','VARCHAR2','HZ_CUST_ACCT_SITES','ATTRIBUTE2','Attribute2','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','ATTRIBUTE4',222,'Descriptive flexfield segment','ATTRIBUTE4','','','','MM027735','VARCHAR2','HZ_CUST_ACCT_SITES','ATTRIBUTE4','Attribute4','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','ATTRIBUTE6',222,'Descriptive flexfield segment','ATTRIBUTE6','','','','MM027735','VARCHAR2','HZ_CUST_ACCT_SITES','ATTRIBUTE6','Attribute6','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','ATTRIBUTE7',222,'Descriptive flexfield segment','ATTRIBUTE7','','','','MM027735','VARCHAR2','HZ_CUST_ACCT_SITES','ATTRIBUTE7','Attribute7','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','ATTRIBUTE8',222,'Descriptive flexfield segment','ATTRIBUTE8','','','','MM027735','VARCHAR2','HZ_CUST_ACCT_SITES','ATTRIBUTE8','Attribute8','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','ATTRIBUTE9',222,'Descriptive flexfield segment','ATTRIBUTE9','','','','MM027735','VARCHAR2','HZ_CUST_ACCT_SITES','ATTRIBUTE9','Attribute9','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','ATTRIBUTE10',222,'Descriptive flexfield segment','ATTRIBUTE10','','','','MM027735','VARCHAR2','HZ_CUST_ACCT_SITES','ATTRIBUTE10','Attribute10','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','ATTRIBUTE11',222,'Descriptive flexfield segment','ATTRIBUTE11','','','','MM027735','VARCHAR2','HZ_CUST_ACCT_SITES','ATTRIBUTE11','Attribute11','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','ATTRIBUTE12',222,'Descriptive flexfield segment','ATTRIBUTE12','','','','MM027735','VARCHAR2','HZ_CUST_ACCT_SITES','ATTRIBUTE12','Attribute12','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','ATTRIBUTE13',222,'Descriptive flexfield segment','ATTRIBUTE13','','','','MM027735','VARCHAR2','HZ_CUST_ACCT_SITES','ATTRIBUTE13','Attribute13','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','GLOBAL_ATTRIBUTE_CATEGORY',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE_CATEGORY','','','','MM027735','VARCHAR2','HZ_CUST_ACCT_SITES','GLOBAL_ATTRIBUTE_CATEGORY','Global Attribute Category','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','GLOBAL_ATTRIBUTE1',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE1','','','','MM027735','VARCHAR2','HZ_CUST_ACCT_SITES','GLOBAL_ATTRIBUTE1','Global Attribute1','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','GLOBAL_ATTRIBUTE2',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE2','','','','MM027735','VARCHAR2','HZ_CUST_ACCT_SITES','GLOBAL_ATTRIBUTE2','Global Attribute2','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','GLOBAL_ATTRIBUTE3',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE3','','','','MM027735','VARCHAR2','HZ_CUST_ACCT_SITES','GLOBAL_ATTRIBUTE3','Global Attribute3','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','GLOBAL_ATTRIBUTE4',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE4','','','','MM027735','VARCHAR2','HZ_CUST_ACCT_SITES','GLOBAL_ATTRIBUTE4','Global Attribute4','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','GLOBAL_ATTRIBUTE5',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE5','','','','MM027735','VARCHAR2','HZ_CUST_ACCT_SITES','GLOBAL_ATTRIBUTE5','Global Attribute5','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','GLOBAL_ATTRIBUTE6',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE6','','','','MM027735','VARCHAR2','HZ_CUST_ACCT_SITES','GLOBAL_ATTRIBUTE6','Global Attribute6','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','GLOBAL_ATTRIBUTE7',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE7','','','','MM027735','VARCHAR2','HZ_CUST_ACCT_SITES','GLOBAL_ATTRIBUTE7','Global Attribute7','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','GLOBAL_ATTRIBUTE8',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE8','','','','MM027735','VARCHAR2','HZ_CUST_ACCT_SITES','GLOBAL_ATTRIBUTE8','Global Attribute8','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','GLOBAL_ATTRIBUTE9',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE9','','','','MM027735','VARCHAR2','HZ_CUST_ACCT_SITES','GLOBAL_ATTRIBUTE9','Global Attribute9','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','GLOBAL_ATTRIBUTE10',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE10','','','','MM027735','VARCHAR2','HZ_CUST_ACCT_SITES','GLOBAL_ATTRIBUTE10','Global Attribute10','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','GLOBAL_ATTRIBUTE11',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE11','','','','MM027735','VARCHAR2','HZ_CUST_ACCT_SITES','GLOBAL_ATTRIBUTE11','Global Attribute11','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','GLOBAL_ATTRIBUTE12',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE12','','','','MM027735','VARCHAR2','HZ_CUST_ACCT_SITES','GLOBAL_ATTRIBUTE12','Global Attribute12','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','GLOBAL_ATTRIBUTE13',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE13','','','','MM027735','VARCHAR2','HZ_CUST_ACCT_SITES','GLOBAL_ATTRIBUTE13','Global Attribute13','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','GLOBAL_ATTRIBUTE14',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE14','','','','MM027735','VARCHAR2','HZ_CUST_ACCT_SITES','GLOBAL_ATTRIBUTE14','Global Attribute14','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','GLOBAL_ATTRIBUTE15',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE15','','','','MM027735','VARCHAR2','HZ_CUST_ACCT_SITES','GLOBAL_ATTRIBUTE15','Global Attribute15','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','GLOBAL_ATTRIBUTE16',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE16','','','','MM027735','VARCHAR2','HZ_CUST_ACCT_SITES','GLOBAL_ATTRIBUTE16','Global Attribute16','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','GLOBAL_ATTRIBUTE17',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE17','','','','MM027735','VARCHAR2','HZ_CUST_ACCT_SITES','GLOBAL_ATTRIBUTE17','Global Attribute17','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','GLOBAL_ATTRIBUTE18',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE18','','','','MM027735','VARCHAR2','HZ_CUST_ACCT_SITES','GLOBAL_ATTRIBUTE18','Global Attribute18','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','GLOBAL_ATTRIBUTE19',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE19','','','','MM027735','VARCHAR2','HZ_CUST_ACCT_SITES','GLOBAL_ATTRIBUTE19','Global Attribute19','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','GLOBAL_ATTRIBUTE20',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE20','','','','MM027735','VARCHAR2','HZ_CUST_ACCT_SITES','GLOBAL_ATTRIBUTE20','Global Attribute20','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','ORIG_SYSTEM_REFERENCE',222,'Address identifier from foreign system','ORIG_SYSTEM_REFERENCE','','','','MM027735','VARCHAR2','HZ_CUST_ACCT_SITES','ORIG_SYSTEM_REFERENCE','Orig System Reference','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','ORG_ID',222,'Organization identifier','ORG_ID','','','','MM027735','NUMBER','HZ_CUST_ACCT_SITES','ORG_ID','Org Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','CUSTOMER_CATEGORY_CODE',222,'User-definable category. The lookup type is ADDRESS_CATEGORY.','CUSTOMER_CATEGORY_CODE','','','','MM027735','VARCHAR2','HZ_CUST_ACCT_SITES','CUSTOMER_CATEGORY_CODE','Customer Category Code','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','OBJECT_VERSION_NUMBER',222,'This column is used for locking purposes','OBJECT_VERSION_NUMBER','','','','MM027735','NUMBER','HZ_CUST_ACCT_SITES','OBJECT_VERSION_NUMBER','Object Version Number','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','ATTRIBUTE14',222,'DFF Context Code - Global Data Elements','ATTRIBUTE14','','','','MM027735','','HZ_CUST_ACCT_SITES','ATTRIBUTE14','Job Information on File?','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','ATTRIBUTE15',222,'DFF Context Code - Global Data Elements','ATTRIBUTE15','','','','MM027735','','HZ_CUST_ACCT_SITES','ATTRIBUTE15','Tax Exemption Type','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','ATTRIBUTE16',222,'DFF Context Code - Global Data Elements','ATTRIBUTE16','','','','MM027735','','HZ_CUST_ACCT_SITES','ATTRIBUTE16','Tax Exempt','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','ATTRIBUTE17',222,'DFF Context Code - Global Data Elements','ATTRIBUTE17','','','','MM027735','','HZ_CUST_ACCT_SITES','ATTRIBUTE17','PRISM Number','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','ATTRIBUTE18',222,'DFF Context Code - Yes','ATTRIBUTE18','','','','MM027735','','HZ_CUST_ACCT_SITES','ATTRIBUTE18','Notice to Owner-Job Total','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','ATTRIBUTE19',222,'DFF Context Code - Yes','ATTRIBUTE19','','','','MM027735','','HZ_CUST_ACCT_SITES','ATTRIBUTE19','Notice to Owner-Prelim Notice','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','ATTRIBUTE20',222,'DFF Context Code - Yes','ATTRIBUTE20','','','','MM027735','','HZ_CUST_ACCT_SITES','ATTRIBUTE20','Notice to Owner-Prelim Date','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','ATTRIBUTE3',222,'DFF Context Code - Global Data Elements','ATTRIBUTE3','','','','MM027735','','HZ_CUST_ACCT_SITES','ATTRIBUTE3','Mandatory PO Number','','','','US');
xxeis.eis_rsc_ins.vc( 'HZ_CUST_ACCT_SITES','ATTRIBUTE5',222,'DFF Context Code - Global Data Elements','ATTRIBUTE5','','','','MM027735','','HZ_CUST_ACCT_SITES','ATTRIBUTE5','Lien Release Date','','','','US');
--Inserting Object Components for HZ_CUST_ACCT_SITES
--Inserting Object Component Joins for HZ_CUST_ACCT_SITES
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 222');
END IF;
END;
/
prompt Creating Report LOV Data for New Job Report
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(222);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - New Job Report
xxeis.eis_rsc_ins.lov( 222,'select distinct state from hz_locations','','Customer State','','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 222,'select distinct postal_code from hz_locations','','Customer Zip Code','Customer Postal Code','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 222,'select distinct city from hz_locations','','Receivables_city','','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 222');
END IF;
END;
/
prompt Creating Report Data for New Job Report
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(222);
IF mod_exist = 'Y' THEN 
--Deleting Report data - New Job Report
xxeis.eis_rsc_utility.delete_report_rows( 'New Job Report' );
--Inserting Report - New Job Report
xxeis.eis_rsc_ins.r( 222,'New Job Report','','This list provides all customer accounts and sites','','','','SM063503','XXEIS_AR_CUST_LIST_SUMMARY_V','Y','','','SM063503','','N','White Cap Reports','','CSV,EXCEL,','N','','','','','','','','US','','','','');
--Inserting Report Columns - New Job Report
xxeis.eis_rsc_ins.rc( 'New Job Report',222,'LOCATION','Location','Site use identifier','','','default','','10','N','Y','','','','','','','SM063503','N','N','','XXEIS_AR_CUST_LIST_SUMMARY_V','HZ_CUST_SITE_USES','SITE_USES','','US','');
xxeis.eis_rsc_ins.rc( 'New Job Report',222,'ACCT_SITE#PRISM_NUMBER','Acct Site#Prism Number','Descriptive flexfield: Address Information Column Name: PRISM Number','','','default','','8','N','Y','','','','','','','SM063503','N','N','','XXEIS_AR_CUST_LIST_SUMMARY_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'New Job Report',222,'CUST#PRISM_NUMBER','Cust#Prism Number','Descriptive flexfield: Customer Information Column Name: Prism Number','','','default','','7','N','Y','','','','','','','SM063503','N','N','','XXEIS_AR_CUST_LIST_SUMMARY_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'New Job Report',222,'STATUS','Status','Site use status flag, Lookup code for the CODE_STATUS column.','','','default','','2','N','Y','','','','','','','SM063503','N','N','','XXEIS_AR_CUST_LIST_SUMMARY_V','HZ_CUST_SITE_USES','SITE_USES','','US','');
xxeis.eis_rsc_ins.rc( 'New Job Report',222,'CUSTOMER_NAME','Customer Name','Customer Name','','','default','','9','N','Y','','','','','','','SM063503','N','N','','XXEIS_AR_CUST_LIST_SUMMARY_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'New Job Report',222,'SITE_USE','Site Use','Site Use','','','default','','3','N','Y','','','','','','','SM063503','N','N','','XXEIS_AR_CUST_LIST_SUMMARY_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'New Job Report',222,'LAST_UPDATE_DATE','Last Update Date','Standard Who column - date when a user last updated this row.','','','default','','1','N','Y','','','','','','','SM063503','N','N','','XXEIS_AR_CUST_LIST_SUMMARY_V','HZ_CUST_SITE_USES','SITE_USES','','US','');
xxeis.eis_rsc_ins.rc( 'New Job Report',222,'ACCT_SITE#TAX_EXEMPT1','Acct Site#Tax Exempt','Descriptive flexfield: Address Information Column Name: Tax Exempt','','','default','','11','N','Y','','','','','','','SM063503','N','N','','XXEIS_AR_CUST_LIST_SUMMARY_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'New Job Report',222,'ACCT_SITE#TAX_EXEMPTION_TYPE','Acct Site#Tax Exemption Type','Descriptive flexfield: Address Information Column Name: Tax Exemption Type','','','default','','12','N','Y','','','','','','','SM063503','N','N','','XXEIS_AR_CUST_LIST_SUMMARY_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'New Job Report',222,'PARTY_SITE_NUMBER','Party Site Number','Party Site Number','','','default','','6','N','Y','','','','','','','SM063503','N','N','','XXEIS_AR_CUST_LIST_SUMMARY_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'New Job Report',222,'CUST_ACCOUNT_ID','Cust Account Id','Cust Account Id','','~T~D~0','default','','7','N','Y','','','','','','','SM063503','N','N','','XXEIS_AR_CUST_LIST_SUMMARY_V','HZ_CUST_ACCOUNTS','CUST','','US','');
xxeis.eis_rsc_ins.rc( 'New Job Report',222,'PRIMARY_PHONE_AREA_CODE','Primary Phone Area Code','The area code within a country code','','','default','','14','N','Y','','','','','','','SM063503','N','N','','XXEIS_AR_CUST_LIST_SUMMARY_V','HZ_PARTIES','PARTY','','US','');
xxeis.eis_rsc_ins.rc( 'New Job Report',222,'PRIMARY_PHONE_LINE_TYPE','Primary Phone Line Type','Lookup code for the type of phone line. For example, general, fax, inbound or outbound','','','default','','13','N','Y','','','','','','','SM063503','N','N','','XXEIS_AR_CUST_LIST_SUMMARY_V','HZ_PARTIES','PARTY','','US','');
xxeis.eis_rsc_ins.rc( 'New Job Report',222,'PRIMARY_PHONE_NUMBER','Primary Phone Number','A telephone number formatted in local format. The number should not include country code, area code, or extension','','','default','','15','N','Y','','','','','','','SM063503','N','N','','XXEIS_AR_CUST_LIST_SUMMARY_V','HZ_PARTIES','PARTY','','US','');
xxeis.eis_rsc_ins.rc( 'New Job Report',222,'ACCOUNT_NUMBER','Account Number','Account Number','','','default','','5','N','Y','','','','','','','SM063503','N','N','','XXEIS_AR_CUST_LIST_SUMMARY_V','HZ_CUST_ACCOUNTS','CUST','','US','');
xxeis.eis_rsc_ins.rc( 'New Job Report',222,'ACCOUNT_NAME','Account Name','Account Name','','','default','','9','N','Y','','','','','','','SM063503','N','N','','XXEIS_AR_CUST_LIST_SUMMARY_V','HZ_CUST_ACCOUNTS','CUST','','US','');
xxeis.eis_rsc_ins.rc( 'New Job Report',222,'COUNTRY','Country','Country code from the TERRITORY_CODE column in the FND_TERRITORY table','','','default','','21','N','Y','','','','','','','SM063503','N','N','','XXEIS_AR_CUST_LIST_SUMMARY_V','HZ_LOCATIONS','LOC','','US','');
xxeis.eis_rsc_ins.rc( 'New Job Report',222,'COUNTY','County','County','','','default','','20','N','Y','','','','','','','SM063503','N','N','','XXEIS_AR_CUST_LIST_SUMMARY_V','HZ_LOCATIONS','LOC','','US','');
xxeis.eis_rsc_ins.rc( 'New Job Report',222,'SALES_TAX_GEOCODE','Sales Tax Geocode','US Sales Tax Jurisdiction code. Use this field to provide a Jurisdiction Code (also called as Geocode) defined by wither Vertex or Taxware. This value is passed as a ship-to locaiton jurisdiction code to the tax partner API.','','','default','','22','N','Y','','','','','','','SM063503','N','N','','XXEIS_AR_CUST_LIST_SUMMARY_V','HZ_LOCATIONS','LOC','','US','');
xxeis.eis_rsc_ins.rc( 'New Job Report',222,'ADDRESS1','Address1','First line for address','','','default','','16','N','Y','','','','','','','SM063503','N','N','','XXEIS_AR_CUST_LIST_SUMMARY_V','HZ_LOCATIONS','LOC','','US','');
xxeis.eis_rsc_ins.rc( 'New Job Report',222,'ADDRESS2','Address2','Second line for address','','','default','','17','N','Y','','','','','','','SM063503','N','N','','XXEIS_AR_CUST_LIST_SUMMARY_V','HZ_LOCATIONS','LOC','','US','');
xxeis.eis_rsc_ins.rc( 'New Job Report',222,'ADDRESS3','Address3','Third line for address','','','default','','18','N','Y','','','','','','','SM063503','N','N','','XXEIS_AR_CUST_LIST_SUMMARY_V','HZ_LOCATIONS','LOC','','US','');
xxeis.eis_rsc_ins.rc( 'New Job Report',222,'ADDRESS4','Address4','Fourth line for address','','','default','','19','N','Y','','','','','','','SM063503','N','N','','XXEIS_AR_CUST_LIST_SUMMARY_V','HZ_LOCATIONS','LOC','','US','');
xxeis.eis_rsc_ins.rc( 'New Job Report',222,'CITY','City','City','','','default','','26','N','Y','','','','','','','SM063503','N','N','','XXEIS_AR_CUST_LIST_SUMMARY_V','HZ_LOCATIONS','LOC','','US','');
xxeis.eis_rsc_ins.rc( 'New Job Report',222,'STATE','State','State','','','default','','28','N','Y','','','','','','','SM063503','N','N','','XXEIS_AR_CUST_LIST_SUMMARY_V','HZ_LOCATIONS','LOC','','US','');
xxeis.eis_rsc_ins.rc( 'New Job Report',222,'POSTAL_CODE','Postal Code','Postal Code of the Identifying address','','','default','','23','N','Y','','','','','','','SM063503','N','N','','XXEIS_AR_CUST_LIST_SUMMARY_V','HZ_PARTIES','PARTY','','US','');
xxeis.eis_rsc_ins.rc( 'New Job Report',222,'CREATION_DATE','Creation Date','Standard who column - date when this row was created.','','','','','24','N','Y','','','','','','','SM063503','N','N','','XXEIS_AR_CUST_LIST_SUMMARY_V','HZ_CUST_ACCT_SITES','ACCT_SITE','','US','');
xxeis.eis_rsc_ins.rc( 'New Job Report',222,'EMAIL_ADDRESS','Email Address','E-mail address','','','','','25','N','Y','','','','','','','SM063503','N','N','','XXEIS_AR_CUST_LIST_SUMMARY_V','HZ_PARTIES','PARTY','','US','');
xxeis.eis_rsc_ins.rc( 'New Job Report',222,'CUSTOMER_SITE_CLAS','Customer Site Class','Customer Site Clas','','','default','','4','','Y','','','','','','','SM063503','N','N','','XXEIS_AR_CUST_LIST_SUMMARY_V','','','','US','');
--Inserting Report Parameters - New Job Report
xxeis.eis_rsc_ins.rp( 'New Job Report',222,'Site Use','Site Use','SITE_USE','IN','','Ship To','VARCHAR2','N','Y','6','Y','Y','CONSTANT','SM063503','Y','N','','','','XXEIS_AR_CUST_LIST_SUMMARY_V','','','US','');
xxeis.eis_rsc_ins.rp( 'New Job Report',222,'Customer City','Customer City','CITY','IN','Receivables_city','','VARCHAR2','N','Y','1','Y','Y','CONSTANT','SM063503','Y','N','','','','XXEIS_AR_CUST_LIST_SUMMARY_V','','','US','');
xxeis.eis_rsc_ins.rp( 'New Job Report',222,'Customer State','Customer State','STATE','IN','Customer State','','VARCHAR2','N','Y','2','Y','Y','CONSTANT','SM063503','Y','N','','','','XXEIS_AR_CUST_LIST_SUMMARY_V','','','US','');
xxeis.eis_rsc_ins.rp( 'New Job Report',222,'Creation Date From','Standard who column - date when this row was created.','CREATION_DATE','>=','','','DATE','N','Y','4','N','N','CONSTANT','SM063503','Y','N','','','','XXEIS_AR_CUST_LIST_SUMMARY_V','','','US','');
xxeis.eis_rsc_ins.rp( 'New Job Report',222,'Customer Zip Code','Customer Zip Code','ZIP_CODE','IN','Customer Zip Code','','VARCHAR2','N','Y','3','Y','Y','CONSTANT','SM063503','Y','N','','','','XXEIS_AR_CUST_LIST_SUMMARY_V','','','US','');
xxeis.eis_rsc_ins.rp( 'New Job Report',222,'Customer Site Clas','Customer Site Clas','CUSTOMER_SITE_CLAS','IN','','','VARCHAR2','N','Y','7','Y','Y','CONSTANT','SM063503','Y','N','','','','XXEIS_AR_CUST_LIST_SUMMARY_V','','','US','');
xxeis.eis_rsc_ins.rp( 'New Job Report',222,'Creation Date To','Creation Date To','CREATION_DATE','<=','','','DATE','N','Y','5','N','N','CONSTANT','SM063503','Y','N','','','','XXEIS_AR_CUST_LIST_SUMMARY_V','','','US','');
--Inserting Dependent Parameters - New Job Report
--Inserting Report Conditions - New Job Report
xxeis.eis_rsc_ins.rcnh( 'New Job Report',222,'ACCT_SITE.CREATION_DATE >= Creation Date From','ADVANCED','','  1#$# ','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','Creation Date From','','','','','XXEIS_AR_CUST_LIST_SUMMARY_V','','','','','','GREATER_THAN_EQUALS','Y','Y','TRUNC(ACCT_SITE.CREATION_DATE)','','','','1',222,'New Job Report','ACCT_SITE.CREATION_DATE >= Creation Date From');
xxeis.eis_rsc_ins.rcnh( 'New Job Report',222,'ACCT_SITE.CREATION_DATE <= Creation Date To','ADVANCED','','  1#$# ','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','Creation Date To','','','','','HZ_CUST_ACCT_SITES','','','','','','LESS_THAN_EQUALS','Y','Y','TRUNC(ACCT_SITE.CREATION_DATE)','','','','1',222,'New Job Report','ACCT_SITE.CREATION_DATE <= Creation Date To');
xxeis.eis_rsc_ins.rcnh( 'New Job Report',222,'XEACLSV.CITY IN Customer City','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CITY','','Customer City','','','','','XXEIS_AR_CUST_LIST_SUMMARY_V','','','','','','IN','Y','Y','','','','','1',222,'New Job Report','XEACLSV.CITY IN Customer City');
xxeis.eis_rsc_ins.rcnh( 'New Job Report',222,'XEACLSV.STATE IN Customer State','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','STATE','','Customer State','','','','','XXEIS_AR_CUST_LIST_SUMMARY_V','','','','','','IN','Y','Y','','','','','1',222,'New Job Report','XEACLSV.STATE IN Customer State');
xxeis.eis_rsc_ins.rcnh( 'New Job Report',222,'XEACLSV.ZIP_CODE IN Customer Zip Code','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','ZIP_CODE','','Customer Zip Code','','','','','XXEIS_AR_CUST_LIST_SUMMARY_V','','','','','','IN','Y','Y','','','','','1',222,'New Job Report','XEACLSV.ZIP_CODE IN Customer Zip Code');
xxeis.eis_rsc_ins.rcnh( 'New Job Report',222,'XEACLSV.SITE_USE IN Site Use','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','SITE_USE','','Site Use','','','','','XXEIS_AR_CUST_LIST_SUMMARY_V','','','','','','IN','Y','Y','','','','','1',222,'New Job Report','XEACLSV.SITE_USE IN Site Use');
xxeis.eis_rsc_ins.rcnh( 'New Job Report',222,'XEACLSV.CUSTOMER_SITE_CLAS IN Customer Site Clas','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CUSTOMER_SITE_CLAS','','Customer Site Clas','','','','','XXEIS_AR_CUST_LIST_SUMMARY_V','','','','','','IN','Y','Y','','','','','1',222,'New Job Report','XEACLSV.CUSTOMER_SITE_CLAS IN Customer Site Clas');
--Inserting Report Sorts - New Job Report
--Inserting Report Triggers - New Job Report
--inserting report templates - New Job Report
xxeis.eis_rsc_ins.r_tem( 'New Job Report','New Job Report','Seeded template for New Job Report','','','','','','','','','','','New Job Report.rtf','SM063503','X','','','Y','Y','','');
--Inserting Report Portals - New Job Report
--inserting report dashboards - New Job Report
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'New Job Report','222','XXEIS_AR_CUST_LIST_SUMMARY_V','HZ_CUST_SITE_USES','N','SITE_USES');
xxeis.eis_rsc_ins.rviews( 'New Job Report','222','XXEIS_AR_CUST_LIST_SUMMARY_V','XXEIS_AR_CUST_LIST_SUMMARY_V','N','');
xxeis.eis_rsc_ins.rviews( 'New Job Report','222','XXEIS_AR_CUST_LIST_SUMMARY_V','HZ_CUST_ACCOUNTS','N','CUST');
xxeis.eis_rsc_ins.rviews( 'New Job Report','222','XXEIS_AR_CUST_LIST_SUMMARY_V','HZ_PARTIES','N','PARTY');
xxeis.eis_rsc_ins.rviews( 'New Job Report','222','XXEIS_AR_CUST_LIST_SUMMARY_V','HZ_LOCATIONS','N','LOC');
xxeis.eis_rsc_ins.rviews( 'New Job Report','222','XXEIS_AR_CUST_LIST_SUMMARY_V','HZ_CUST_ACCT_SITES','N','ACCT_SITE');
--inserting report security - New Job Report
xxeis.eis_rsc_ins.rsec( 'New Job Report','222','','XXWC_CRE_ASSOC_CASH_APP',222,'SM063503','','','');
xxeis.eis_rsc_ins.rsec( 'New Job Report','222','','XXWC_CRE_ASSOC_COLLECTIONS',222,'SM063503','','','');
xxeis.eis_rsc_ins.rsec( 'New Job Report','222','','XXWC_CRE_ASSOC_CASH_APP_MGR',222,'SM063503','','','');
xxeis.eis_rsc_ins.rsec( 'New Job Report','222','','XXWC_CRE_ASSOC_CUST_MAINT',222,'SM063503','','','');
xxeis.eis_rsc_ins.rsec( 'New Job Report','','PP018915','',222,'SM063503','','','');
xxeis.eis_rsc_ins.rsec( 'New Job Report','','10011288','',222,'SM063503','','','');
xxeis.eis_rsc_ins.rsec( 'New Job Report','879','','XXWC_CUSTOMER_HIERARCHY_MGR',222,'SM063503','','','');
xxeis.eis_rsc_ins.rsec( 'New Job Report','879','','XXWC_CUSTOMER_HIERARCHY_MTN',222,'SM063503','','','');
xxeis.eis_rsc_ins.rsec( 'New Job Report','','SG019472','',222,'SM063503','','','');
xxeis.eis_rsc_ins.rsec( 'New Job Report','222','','RECEIVABLES INQUIRY',222,'SM063503','','','');
xxeis.eis_rsc_ins.rsec( 'New Job Report','222','','XXWC_RECEIVABLES_INQUIRY_WC',222,'SM063503','','','');
--Inserting Report Pivots - New Job Report
xxeis.eis_rsc_ins.rpivot( 'New Job Report',222,'Pivot','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rsc_ins.rpivot_dtls( 'New Job Report',222,'Pivot','ACCT_SITE#PRISM_NUMBER','DATA_FIELD','COUNT','','1','','');
xxeis.eis_rsc_ins.rpivot_dtls( 'New Job Report',222,'Pivot','CUST#PRISM_NUMBER','ROW_FIELD','','','2','','');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
--Inserting Report   Version details- New Job Report
xxeis.eis_rsc_ins.rv( 'New Job Report','','New Job Report','SA059956');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 222');
END IF;
END;
/
