--Report Name            : Inventory - WC Item Master Unassigned Taxware Code
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data XXEIS_INV_INVTRY_ATTRIBUTES_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(401);
IF mod_exist = 'Y' THEN 
--Inserting Object XXEIS_INV_INVTRY_ATTRIBUTES_V
xxeis.eis_rsc_ins.v( 'XXEIS_INV_INVTRY_ATTRIBUTES_V',401,'This view shows Item Inventory Attributes details. Information including shelf life, revision enabled, lot control, serial control, locator control, Primary UOM, and locator restrictions. Flags indicating purchasing enablement and transaction enablement also displayed.','','','','XXEIS_RS_ADMIN','XXEIS','EIS INV Item Attributes','XEIIAV1','','','VIEW','US','Y','','');
--Delete Object Columns for XXEIS_INV_INVTRY_ATTRIBUTES_V
xxeis.eis_rsc_utility.delete_view_rows('XXEIS_INV_INVTRY_ATTRIBUTES_V',401,FALSE);
--Inserting Object Columns for XXEIS_INV_INVTRY_ATTRIBUTES_V
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','CHECK_MATERIAL_SHORTAGE',401,'Flag indicating material shortages should be checked for this item','CHECK_MATERIAL_SHORTAGE','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','CHECK_SHORTAGES_FLAG','Check Material Shortage','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','SHELF_CONTROL',401,'Shelf life code','SHELF_CONTROL','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','SHELF_LIFE_CODE','Shelf Control','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','SHELF_LIFE_DAYS',401,'Length of shelf life days','SHELF_LIFE_DAYS','','','','XXEIS_RS_ADMIN','NUMBER','MTL_SYSTEM_ITEMS_B','SHELF_LIFE_DAYS','Shelf Life Days','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','LOT_STARTING_PREFIX',401,'Item-level alpha prefix for serial numbers','LOT_STARTING_PREFIX','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','AUTO_SERIAL_ALPHA_PREFIX','Lot Starting Prefix','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','LOT_STARTING_NUMBER',401,'Next auto assigned lot number','LOT_STARTING_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','START_AUTO_LOT_NUMBER','Lot Starting Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','GENERATION',401,'Serial number control code','GENERATION','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','SERIAL_NUMBER_CONTROL_CODE','Generation','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','SERIAL_STARTING_PREFIX',401,'Item-level alpha prefix for serial numbers','SERIAL_STARTING_PREFIX','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','AUTO_SERIAL_ALPHA_PREFIX','Serial Starting Prefix','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','SERIAL_STARTING_NUMBER',401,'Next auto assigned serial number','SERIAL_STARTING_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','START_AUTO_SERIAL_NUMBER','Serial Starting Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','RESTRICT_LOCATORS',401,'Locators restrictions type','RESTRICT_LOCATORS','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','RESTRICT_LOCATORS_CODE','Restrict Locators','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','ITEM_DESCRIPTION',401,'Item description is maintained in the installation base language only.   Translations table (MTL_SYSTEM_ITEMS_TL) holds item descriptions in multiple languages','ITEM_DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','DESCRIPTION','Item Description','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','INVENTORY_ITEM_FLAG',401,'Flag indicating inventory item','INVENTORY_ITEM_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','INVENTORY_ITEM_FLAG','Inventory Item Flag','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','REVISION_CONTROL',401,'Revision quantity control code','REVISION_CONTROL','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','REVISION_QTY_CONTROL_CODE','Revision Control','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','STOCKABLE',401,'Flag indicating item is stockable','STOCKABLE','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','STOCK_ENABLED_FLAG','Stockable','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','INVENTORY_ITEM',401,'INVENTORY_ITEM','INVENTORY_ITEM','','','INV Items LOV','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','SEGMENT1','Inventory Item','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','LOCATOR_CONTROL',401,'Stock locator control code','LOCATOR_CONTROL','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','LOCATION_CONTROL_CODE','Locator Control','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','INVENTORY_PLANNING_METHOD',401,'Inventory planning code','INVENTORY_PLANNING_METHOD','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','INVENTORY_PLANNING_CODE','Inventory Planning Method','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','INVOICEABLE_ITEM',401,'Flag indicating item may appear on invoices','INVOICEABLE_ITEM','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','INVOICEABLE_ITEM_FLAG','Invoiceable Item','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','PURCHASABLE',401,'Flag indicating item is purchasable','PURCHASABLE','','','INV Item Purchasable Flag LOV','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','PURCHASING_ENABLED_FLAG','Purchasable','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','PURCHASED',401,'Flag indicating purchasing item','PURCHASED','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','PURCHASING_ITEM_FLAG','Purchased','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','INV_ORG_NAME',401,'Organization Name','INV_ORG_NAME','','','INV Organization Names LOV','XXEIS_RS_ADMIN','VARCHAR2','HR_ALL_ORGANIZATION_UNITS','NAME','INV Org Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','PRICING',401,'Catch-Weight Support for 11.5.10','PRICING','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ONT_PRICING_QTY_SOURCE','Pricing','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','TRACKING',401,'Catch-weight Support for 11.5.10','TRACKING','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','TRACKING_QUANTITY_IND','Tracking','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','USER_ITEM_TYPE',401,'User-defined item type','USER_ITEM_TYPE','','','INV User Item Type Name LOV','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ITEM_TYPE','User Item Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','BULK_PICKED',401,'Material pick method','BULK_PICKED','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','BULK_PICKED_FLAG','Bulk Picked','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','DEFAULT_LOT_STATUS',401,'The current status of the concurrent request','DEFAULT_LOT_STATUS','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_MATERIAL_STATUSES_B','STATUS_CODE','Default Lot Status','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','LOT_MERGE_ENABLED',401,'Lot merge enabled','LOT_MERGE_ENABLED','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','LOT_MERGE_ENABLED','Lot Merge Enabled','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','LOT_STATUS_ENABLED',401,'Lot status enabled','LOT_STATUS_ENABLED','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','LOT_STATUS_ENABLED','Lot Status Enabled','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','LOT_SUBSTITUTION_ENABLED',401,'Lot Substitution Enabled for 11.5.9','LOT_SUBSTITUTION_ENABLED','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','LOT_SUBSTITUTION_ENABLED','Lot Substitution Enabled','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','INVENTORY_ITEM_STATUS_CODE',401,'Material status code','INVENTORY_ITEM_STATUS_CODE~INVENTORY_ITEM_STATUS_CODE','','','INV Item Status LOV','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','INVENTORY_ITEM_STATUS_CODE','Inventory Item Status Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','CYCLE_COUNT',401,'Flag indicating item may be cycle counted','CYCLE_COUNT','','~T~D~0','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','CYCLE_COUNT_ENABLED_FLAG','Cycle Count','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','CREATION_DATE',401,'Standard Who column','CREATION_DATE','','','','XXEIS_RS_ADMIN','DATE','MTL_SYSTEM_ITEMS_B','CREATION_DATE','Creation Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','INVOICE_ENABLED',401,'Indicates whether the item can be invoiced','INVOICE_ENABLED','','','INV Invoice Enabled Flag LOV','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','INVOICE_ENABLED_FLAG','Invoice Enabled','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','LOT_TRANSLATE_ENABLED',401,'Lot Translate Enabled','LOT_TRANSLATE_ENABLED','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','LOT_TRANSLATE_ENABLED','Lot Translate Enabled','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','SERIAL_STATUS_ENABLED',401,'Serial status enabled','SERIAL_STATUS_ENABLED','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','SERIAL_STATUS_ENABLED','Serial Status Enabled','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','RESERVABLE',401,'Hard reservations allowed flag','RESERVABLE','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','RESERVABLE_TYPE','Reservable','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','TRANSACTABLE',401,'Flag indicating item is transactable','TRANSACTABLE','','','INV Item Transactable Flag LOV','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','MTL_TRANSACTIONS_ENABLED_FLAG','Transactable','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','CREATED_BY_USER',401,'Standard Who column','CREATED_BY_USER~CREATED_BY_USER','','','INV User Names LOV','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','CREATED_BY','Created By User','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','DEFAULT_SERIAL_STATUS',401,'The current status of the concurrent request','DEFAULT_SERIAL_STATUS~DEFAULT_SERIAL_STATUS','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_MATERIAL_STATUSES_B','STATUS_CODE','Default Serial Status','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','LOT_CONTROL_CODE',401,'Lot control code','LOT_CONTROL_CODE~LOT_CONTROL_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','LOT_CONTROL_CODE','Lot Control Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','LOT_SPLIT_ENABLED',401,'Lot split enabled','LOT_SPLIT_ENABLED~LOT_SPLIT_ENABLED','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','LOT_SPLIT_ENABLED','Lot Split Enabled','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','NEG_MEASURE_ERROR',401,'Percent error below measured quantity','NEG_MEASURE_ERROR~NEG_MEASURE_ERROR','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','MTL_SYSTEM_ITEMS_B','NEGATIVE_MEASUREMENT_ERROR','Neg Measure Error','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','POS_MEASURE_ERROR',401,'Percent error above measured quantity','POS_MEASURE_ERROR~POS_MEASURE_ERROR','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','MTL_SYSTEM_ITEMS_B','POSITIVE_MEASUREMENT_ERROR','POs Measure Error','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','PRIMARY_UNIT_OF_MEASURE',401,'Primary stocking unit of measure for the item','PRIMARY_UNIT_OF_MEASURE~PRIMARY_UNIT_OF_MEASURE','','','INV Item Primary UOM LOV','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','PRIMARY_UNIT_OF_MEASURE','Primary Unit Of Measure','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','RESTRICT_SUBINV',401,'Subinventory restrictions type','RESTRICT_SUBINV~RESTRICT_SUBINV','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','RESTRICT_SUBINVENTORIES_CODE','Restrict Subinv','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','SECONDARY_DEFAULT_IND',401,'Catch-Weight Support for 11.5.10','SECONDARY_DEFAULT_IND~SECONDARY_DEFAULT_IND','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','SECONDARY_DEFAULT_IND','Secondary Default Ind','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','SECONDARY_UOM_CODE',401,'Statistical unit of measure name','SECONDARY_UOM_CODE~SECONDARY_UOM_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_ALL_PRIMARY_UOMS_VV','UNIT_OF_MEASURE','Secondary UOM Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','CHILD_LOT_ENABLED',401,'Child Lot Enabled','CHILD_LOT_ENABLED','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','CHILD_LOT_FLAG','Child Lot Enabled','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','CHILD_LOT_PREFIX',401,'Child Lot Prefix','CHILD_LOT_PREFIX','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','CHILD_LOT_PREFIX','Child Lot Prefix','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','CHILD_LOT_STARTING_NUMBER',401,'Child Lot Starting Number','CHILD_LOT_STARTING_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','MTL_SYSTEM_ITEMS_B','CHILD_LOT_STARTING_NUMBER','Child Lot Starting Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','COPY_LOT_ATTRIBUTE',401,'Copy Lot Attribute','COPY_LOT_ATTRIBUTE','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','COPY_LOT_ATTRIBUTE_FLAG','Copy Lot Attribute','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','DEFAULT_GRADE',401,'Default Grade','DEFAULT_GRADE','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','DEFAULT_GRADE','Default Grade','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','FORMAT_VALIDATION',401,'Format Validation','FORMAT_VALIDATION','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','CHILD_LOT_VALIDATION_FLAG','Format Validation','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','GRADE_CONTROL',401,'Grade Control','GRADE_CONTROL','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','GRADE_CONTROL_FLAG','Grade Control','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','LOT_DIVISIBLE',401,'Lot Divisible','LOT_DIVISIBLE','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','LOT_DIVISIBLE_FLAG','Lot Divisible','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','PARENT_CHILD_GENERATION',401,'Parent Child Generation','PARENT_CHILD_GENERATION','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','PARENT_CHILD_GENERATION_FLAG','Parent Child Generation','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','MSI#WC#Taxware_Code',401,'Descriptive flexfield (DFF): Items Column Name: Taxware Code Context: WC','MSI#WC#Taxware_Code','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE22','Msi#Wc#Taxware Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','INVENTORY_ITEM_ID',401,'Inventory item identifier','INVENTORY_ITEM_ID','','','','XXEIS_RS_ADMIN','NUMBER','MTL_SYSTEM_ITEMS_B','INVENTORY_ITEM_ID','Inventory Item Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','ORGANIZATION_ID',401,'Organization identifier','ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','MTL_SYSTEM_ITEMS_B','ORGANIZATION_ID','Organization Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','DUAL_UOM_DEVIATION_HIGH',401,'Upper bound of tolerance factor by which the Dual Unit of Measure may be transacted in','DUAL_UOM_DEVIATION_HIGH~DUAL_UOM_DEVIATION_HIGH','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','MTL_SYSTEM_ITEMS_B','DUAL_UOM_DEVIATION_HIGH','Dual UOM Deviation High','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','DUAL_UOM_DEVIATION_LOW',401,'Lower bound of tolerance factor by which the Dual Unit of Measure may be transacted in','DUAL_UOM_DEVIATION_LOW~DUAL_UOM_DEVIATION_LOW','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','MTL_SYSTEM_ITEMS_B','DUAL_UOM_DEVIATION_LOW','Dual UOM Deviation Low','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','HAOU_ORGANIZATION_ID',401,'Organization identifier','HAOU_ORGANIZATION_ID~HAOU_ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','HR_ALL_ORGANIZATION_UNITS','ORGANIZATION_ID','Haou Organization Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','MAPV_INVENTORY_ITEM_ID',401,'Item identifier','MAPV_INVENTORY_ITEM_ID~MAPV_INVENTORY_ITEM_ID','','','','XXEIS_RS_ADMIN','NUMBER','MTL_ALL_PRIMARY_UOMS_VV','INVENTORY_ITEM_ID','Mapv Inventory Item Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','MMSV_STATUS_ID',401,'Status identifier','MMSV_STATUS_ID~MMSV_STATUS_ID','','','','XXEIS_RS_ADMIN','NUMBER','MTL_MATERIAL_STATUSES_B','STATUS_ID','Mmsv Status Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','STATUS_ID',401,'Status identifier','STATUS_ID~STATUS_ID','','','','XXEIS_RS_ADMIN','NUMBER','MTL_MATERIAL_STATUSES_B','STATUS_ID','Status Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','COPYRIGHT',401,'Copyright','COPYRIGHT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Copyright','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','MSI#HDS#LOB',401,'Descriptive flexfield (DFF): Items Column Name: LOB Context: HDS','MSI#HDS#LOB','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE1','Msi#Hds#Lob','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','MSI#HDS#Drop_Shipment_Eligab',401,'Descriptive flexfield (DFF): Items Column Name: Drop Shipment Eligable Context: HDS','MSI#HDS#Drop_Shipment_Eligab','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE10','Msi#Hds#Drop Shipment Eligable','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','MSI#HDS#Invoice_UOM',401,'Descriptive flexfield (DFF): Items Column Name: Invoice UOM Context: HDS','MSI#HDS#Invoice_UOM','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE15','Msi#Hds#Invoice Uom','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','MSI#HDS#Product_ID',401,'Descriptive flexfield (DFF): Items Column Name: Product ID Context: HDS','MSI#HDS#Product_ID','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE2','Msi#Hds#Product Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','MSI#HDS#Vendor_Part_Number',401,'Descriptive flexfield (DFF): Items Column Name: Vendor Part Number Context: HDS','MSI#HDS#Vendor_Part_Number','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE3','Msi#Hds#Vendor Part Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','MSI#HDS#UNSPSC_Code',401,'Descriptive flexfield (DFF): Items Column Name: UNSPSC Code Context: HDS','MSI#HDS#UNSPSC_Code','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE4','Msi#Hds#Unspsc Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','MSI#HDS#UPC_Primary',401,'Descriptive flexfield (DFF): Items Column Name: UPC Primary Context: HDS','MSI#HDS#UPC_Primary','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE5','Msi#Hds#Upc Primary','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','MSI#HDS#SKU_Description',401,'Descriptive flexfield (DFF): Items Column Name: SKU Description Context: HDS','MSI#HDS#SKU_Description','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE6','Msi#Hds#Sku Description','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','MSI#WC#CA_Prop_65',401,'Descriptive flexfield (DFF): Items Column Name: CA Prop 65 Context: WC','MSI#WC#CA_Prop_65','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE1','Msi#Wc#Ca Prop 65','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','MSI#WC#Country_of_Origin',401,'Descriptive flexfield (DFF): Items Column Name: Country of Origin Context: WC','MSI#WC#Country_of_Origin','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE10','Msi#Wc#Country Of Origin','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','MSI#WC#ORM_D_Flag',401,'Descriptive flexfield (DFF): Items Column Name: ORM-D Flag Context: WC','MSI#WC#ORM_D_Flag','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE11','Msi#Wc#Orm-D Flag','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','MSI#WC#Store_Velocity',401,'Descriptive flexfield (DFF): Items Column Name: Store Velocity Context: WC','MSI#WC#Store_Velocity','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE12','Msi#Wc#Store Velocity','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','MSI#WC#DC_Velocity',401,'Descriptive flexfield (DFF): Items Column Name: DC Velocity Context: WC','MSI#WC#DC_Velocity','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE13','Msi#Wc#Dc Velocity','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','MSI#WC#Yearly_Store_Velocity',401,'Descriptive flexfield (DFF): Items Column Name: Yearly Store Velocity Context: WC','MSI#WC#Yearly_Store_Velocity','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE14','Msi#Wc#Yearly Store Velocity','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','MSI#WC#Yearly_DC_Velocity',401,'Descriptive flexfield (DFF): Items Column Name: Yearly DC Velocity Context: WC','MSI#WC#Yearly_DC_Velocity','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE15','Msi#Wc#Yearly Dc Velocity','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','MSI#WC#PRISM_Part_Number',401,'Descriptive flexfield (DFF): Items Column Name: PRISM Part Number Context: WC','MSI#WC#PRISM_Part_Number','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE16','Msi#Wc#Prism Part Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','MSI#WC#Hazmat_Description',401,'Descriptive flexfield (DFF): Items Column Name: Hazmat Description Context: WC','MSI#WC#Hazmat_Description','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE17','Msi#Wc#Hazmat Description','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','MSI#WC#Hazmat_Container',401,'Descriptive flexfield (DFF): Items Column Name: Hazmat Container Context: WC','MSI#WC#Hazmat_Container','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE18','Msi#Wc#Hazmat Container','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','MSI#WC#GTP_Indicator',401,'Descriptive flexfield (DFF): Items Column Name: GTP Indicator Context: WC','MSI#WC#GTP_Indicator','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE19','Msi#Wc#Gtp Indicator','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','MSI#WC#Last_Lead_Time',401,'Descriptive flexfield (DFF): Items Column Name: Last Lead Time Context: WC','MSI#WC#Last_Lead_Time','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE2','Msi#Wc#Last Lead Time','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','MSI#WC#AMU',401,'Descriptive flexfield (DFF): Items Column Name: AMU Context: WC','MSI#WC#AMU','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE20','Msi#Wc#Amu','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','MSI#WC#Reserve_Stock',401,'Descriptive flexfield (DFF): Items Column Name: Reserve Stock Context: WC','MSI#WC#Reserve_Stock','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE21','Msi#Wc#Reserve Stock','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','MSI#WC#Shipping_Weight_Overr',401,'Descriptive flexfield (DFF): Items Column Name: Shipping Weight Override Context: WC','MSI#WC#Shipping_Weight_Overr','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE23','Msi#Wc#Shipping Weight Override','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','MSI#WC#Rounding_Factor',401,'Descriptive flexfield (DFF): Items Column Name: Rounding Factor Context: WC','MSI#WC#Rounding_Factor','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE24','Msi#Wc#Rounding Factor','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','MSI#WC#Average_Units',401,'Descriptive flexfield (DFF): Items Column Name: Average Units Context: WC','MSI#WC#Average_Units','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE25','Msi#Wc#Average Units','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','MSI#WC#Product_code',401,'Descriptive flexfield (DFF): Items Column Name: Product code Context: WC','MSI#WC#Product_code','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE26','Msi#Wc#Product Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','MSI#WC#Import_Duty_',401,'Descriptive flexfield (DFF): Items Column Name: Import Duty % Context: WC','MSI#WC#Import_Duty_','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE27','Msi#Wc#Import Duty %','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','MSI#WC#Effective_Date',401,'Descriptive flexfield (DFF): Items Column Name: Effective Date Context: WC','MSI#WC#Effective_Date','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE28','Msi#Wc#Effective Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','MSI#WC#Keep_Item_Active',401,'Descriptive flexfield (DFF): Items Column Name: Keep Item Active Context: WC','MSI#WC#Keep_Item_Active','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE29','Msi#Wc#Keep Item Active','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','MSI#WC#Pesticide_Flag',401,'Descriptive flexfield (DFF): Items Column Name: Pesticide Flag Context: WC','MSI#WC#Pesticide_Flag','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE3','Msi#Wc#Pesticide Flag','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','MSI#WC#Calc_Lead_Time',401,'Descriptive flexfield (DFF): Items Column Name: Calc Lead Time Context: WC','MSI#WC#Calc_Lead_Time','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE30','Msi#Wc#Calc Lead Time','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','MSI#WC#VOC_GL',401,'Descriptive flexfield (DFF): Items Column Name: VOC G/L Context: WC','MSI#WC#VOC_GL','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE4','Msi#Wc#Voc G/L','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','MSI#WC#Pesticide_Flag_State',401,'Descriptive flexfield (DFF): Items Column Name: Pesticide Flag State Context: WC','MSI#WC#Pesticide_Flag_State','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE5','Msi#Wc#Pesticide Flag State','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','MSI#WC#VOC_Category',401,'Descriptive flexfield (DFF): Items Column Name: VOC Category Context: WC','MSI#WC#VOC_Category','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE6','Msi#Wc#Voc Category','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','MSI#WC#VOC_Sub_Category',401,'Descriptive flexfield (DFF): Items Column Name: VOC Sub Category Context: WC','MSI#WC#VOC_Sub_Category','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE7','Msi#Wc#Voc Sub Category','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','MSI#WC#MSDS_#',401,'Descriptive flexfield (DFF): Items Column Name: MSDS # Context: WC','MSI#WC#MSDS_#','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE8','Msi#Wc#Msds #','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','MSI#WC#Hazmat_Packaging_Grou',401,'Descriptive flexfield (DFF): Items Column Name: Hazmat Packaging Group Context: WC','MSI#WC#Hazmat_Packaging_Grou','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE9','Msi#Wc#Hazmat Packaging Group','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','MSI_WC_TAXWARE_CODE',401,'Msi Wc Taxware Code','MSI_WC_TAXWARE_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Msi Wc Taxware Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','MSI_WC_TAXWARE_NUMBER',401,'Msi Wc Taxware Number','MSI_WC_TAXWARE_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Msi Wc Taxware Number','','','','US','');
--Inserting Object Components for XXEIS_INV_INVTRY_ATTRIBUTES_V
xxeis.eis_rsc_ins.vcomp( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','MTL_SYSTEM_ITEMS_KFV',401,'MTL_SYSTEM_ITEMS_B','MSI','MSI','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Inventory Item Definition','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','HR_ORGANIZATION_UNITS',401,'HR_ALL_ORGANIZATION_UNITS','HAOU','HAOU','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','HR_ALL_ORGANIZATION_UNITS','N','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','MTL_ALL_PRIMARY_UOMS_VV',401,'MTL_UOM_CONVERSIONS','MAPV','MAPV','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Unit Of Measure Conversion Table For Both Default ','','','','','MAPUV','',' ',' ',' ',' ',' ');
--Inserting Object Component Joins for XXEIS_INV_INVTRY_ATTRIBUTES_V
xxeis.eis_rsc_ins.vcj( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','HR_ORGANIZATION_UNITS','HAOU',401,'XEIIAV1.HAOU_ORGANIZATION_ID','=','HAOU.ORGANIZATION_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','MTL_ALL_PRIMARY_UOMS_VV','MAPV',401,'XEIIAV1.MAPV_INVENTORY_ITEM_ID','=','MAPV.INVENTORY_ITEM_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','MTL_ALL_PRIMARY_UOMS_VV','MAPV',401,'XEIIAV1.SECONDARY_UOM_CODE','=','MAPV.UNIT_OF_MEASURE(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','MTL_SYSTEM_ITEMS_KFV','MSI',401,'XEIIAV1.INVENTORY_ITEM_ID','=','MSI.INVENTORY_ITEM_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'XXEIS_INV_INVTRY_ATTRIBUTES_V','MTL_SYSTEM_ITEMS_KFV','MSI',401,'XEIIAV1.ORGANIZATION_ID','=','MSI.ORGANIZATION_ID(+)','','','','Y','XXEIS_RS_ADMIN');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 401');
END IF;
END;
/
prompt Creating Report LOV Data for Inventory - WC Item Master Unassigned Taxware Code
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(401);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - Inventory - WC Item Master Unassigned Taxware Code
xxeis.eis_rsc_ins.lov( 401,'SELECT DISTINCT concatenated_segments item, description FROM mtl_system_items_kfv msi,
                org_organization_definitions ood
          WHERE msi.organization_id = ood.organization_id
            AND ood.operating_unit = fnd_profile.VALUE (''ORG_ID'')
       ORDER BY concatenated_segments','','EIS_INV_ITEM_LOV','List of all inventory items.','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 401,'select INVENTORY_ITEM_STATUS_CODE
from mtl_item_status','','EIS_INV_ITEM_STATUS_LOV','Lists  the item status','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 401,'select distinct NVL(DECODE(MSI.ATTRIBUTE_CATEGORY ,''WC'',xxeis.eis_rs_dff.decode_valueset( ''XXWC TAXWARE CODE'',MSI.ATTRIBUTE22,''I''), NULL), ''NULL'') MSI#WC#Taxware_Code
from mtl_system_items_kfv msi','','XXWC Taxware Codes','LOV to pull both taxware description','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 401,'select distinct NVL(DECODE(MSI.ATTRIBUTE_CATEGORY ,''WC'',MSI.ATTRIBUTE22, NULL), ''NULL'') MSI#WC#Taxware_Number
from mtl_system_items_kfv msi','','XXWC Taxware Number','LOV to pull both taxware number','ANONYMOUS',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 401');
END IF;
END;
/
prompt Creating Report Data for Inventory - WC Item Master Unassigned Taxware Code
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(401);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Inventory - WC Item Master Unassigned Taxware Code
xxeis.eis_rsc_utility.delete_report_rows( 'Inventory - WC Item Master Unassigned Taxware Code',401 );
--Inserting Report - Inventory - WC Item Master Unassigned Taxware Code
xxeis.eis_rsc_ins.r( 401,'Inventory - WC Item Master Unassigned Taxware Code','','Displays all items in WC Item Master that has not been assigned to a Taxware Code','','','','XXEIS_RS_ADMIN','XXEIS_INV_INVTRY_ATTRIBUTES_V','Y','','','XXEIS_RS_ADMIN','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N','','','','','','','','US','','','','','','','','','','','','','','','','');
--Inserting Report Columns - Inventory - WC Item Master Unassigned Taxware Code
xxeis.eis_rsc_ins.rc( 'Inventory - WC Item Master Unassigned Taxware Code',401,'INVENTORY_ITEM_STATUS_CODE','Inventory Item Status Code','Material status code','','','default','','6','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_INV_INVTRY_ATTRIBUTES_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Inventory - WC Item Master Unassigned Taxware Code',401,'INV_ORG_NAME','Inv Org Name','Organization Name','','','default','','1','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_INV_INVTRY_ATTRIBUTES_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Inventory - WC Item Master Unassigned Taxware Code',401,'INVENTORY_ITEM','Inventory Item','INVENTORY_ITEM','','','default','','2','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_INV_INVTRY_ATTRIBUTES_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Inventory - WC Item Master Unassigned Taxware Code',401,'ITEM_DESCRIPTION','Item Description','Item description is maintained in the installation base language only.   Translations table (MTL_SYSTEM_ITEMS_TL) holds item descriptions in multiple languages','','','default','','3','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_INV_INVTRY_ATTRIBUTES_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Inventory - WC Item Master Unassigned Taxware Code',401,'CREATION_DATE','Creation Date','Standard Who column','','','default','','7','','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_INV_INVTRY_ATTRIBUTES_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Inventory - WC Item Master Unassigned Taxware Code',401,'MSI_WC_TAXWARE_CODE','Msi Wc Taxware Code','Msi Wc Taxware Code','','','default','','5','','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_INV_INVTRY_ATTRIBUTES_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Inventory - WC Item Master Unassigned Taxware Code',401,'MSI_WC_TAXWARE_NUMBER','Msi Wc Taxware Number','Msi Wc Taxware Number','','','default','','9','','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_INV_INVTRY_ATTRIBUTES_V','','','','US','','');
--Inserting Report Parameters - Inventory - WC Item Master Unassigned Taxware Code
xxeis.eis_rsc_ins.rp( 'Inventory - WC Item Master Unassigned Taxware Code',401,'Inv Org Name','Inv Org Name','INV_ORG_NAME','IN','','WC Item Master','VARCHAR2','N','Y','1','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','XXEIS_INV_INVTRY_ATTRIBUTES_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Inventory - WC Item Master Unassigned Taxware Code',401,'Inventory Item','Inventory Item','INVENTORY_ITEM','IN','EIS_INV_ITEM_LOV','','VARCHAR2','N','Y','2','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','XXEIS_INV_INVTRY_ATTRIBUTES_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Inventory - WC Item Master Unassigned Taxware Code',401,'Inventory Item Status Code','Inventory Item Status Code','INVENTORY_ITEM_STATUS_CODE','IN','EIS_INV_ITEM_STATUS_LOV','','VARCHAR2','N','Y','3','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','XXEIS_INV_INVTRY_ATTRIBUTES_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Inventory - WC Item Master Unassigned Taxware Code',401,'Msi Wc Taxware Code','Msi Wc Taxware Code','MSI_WC_TAXWARE_CODE','IN','XXWC Taxware Codes','','VARCHAR2','N','Y','5','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','XXEIS_INV_INVTRY_ATTRIBUTES_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Inventory - WC Item Master Unassigned Taxware Code',401,'Msi Wc Taxware Number','Msi Wc Taxware Number','MSI_WC_TAXWARE_NUMBER','IN','XXWC Taxware Number','','VARCHAR2','N','Y','4','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','XXEIS_INV_INVTRY_ATTRIBUTES_V','','','US','');
--Inserting Dependent Parameters - Inventory - WC Item Master Unassigned Taxware Code
--Inserting Report Conditions - Inventory - WC Item Master Unassigned Taxware Code
xxeis.eis_rsc_ins.rcnh( 'Inventory - WC Item Master Unassigned Taxware Code',401,'INVENTORY_ITEM IN :Inventory Item ','SIMPLE','','','Y','','1');
xxeis.eis_rsc_ins.rcnd( '','','INVENTORY_ITEM','','Inventory Item','','','','','XXEIS_INV_INVTRY_ATTRIBUTES_V','','','','','','IN','Y','Y','','','','','1',401,'Inventory - WC Item Master Unassigned Taxware Code','INVENTORY_ITEM IN :Inventory Item ');
xxeis.eis_rsc_ins.rcnh( 'Inventory - WC Item Master Unassigned Taxware Code',401,'INVENTORY_ITEM_STATUS_CODE IN :Inventory Item Status Code ','SIMPLE','','','Y','','2');
xxeis.eis_rsc_ins.rcnd( '','','INVENTORY_ITEM_STATUS_CODE','','Inventory Item Status Code','','','','','XXEIS_INV_INVTRY_ATTRIBUTES_V','','','','','','IN','Y','Y','','','','','1',401,'Inventory - WC Item Master Unassigned Taxware Code','INVENTORY_ITEM_STATUS_CODE IN :Inventory Item Status Code ');
xxeis.eis_rsc_ins.rcnh( 'Inventory - WC Item Master Unassigned Taxware Code',401,'INV_ORG_NAME IN :Inv Org Name ','SIMPLE','','','Y','','3');
xxeis.eis_rsc_ins.rcnd( '','','INV_ORG_NAME','','Inv Org Name','','','','','XXEIS_INV_INVTRY_ATTRIBUTES_V','','','','','','IN','Y','Y','','','','','1',401,'Inventory - WC Item Master Unassigned Taxware Code','INV_ORG_NAME IN :Inv Org Name ');
xxeis.eis_rsc_ins.rcnh( 'Inventory - WC Item Master Unassigned Taxware Code',401,'XEIIAV1.MSI_WC_TAXWARE_CODE IN Msi Wc Taxware Code','SIMPLE','','','Y','','4');
xxeis.eis_rsc_ins.rcnd( '','','MSI_WC_TAXWARE_CODE','','Msi Wc Taxware Code','','','','','XXEIS_INV_INVTRY_ATTRIBUTES_V','','','','','','IN','Y','Y','','','','','1',401,'Inventory - WC Item Master Unassigned Taxware Code','XEIIAV1.MSI_WC_TAXWARE_CODE IN Msi Wc Taxware Code');
xxeis.eis_rsc_ins.rcnh( 'Inventory - WC Item Master Unassigned Taxware Code',401,'XEIIAV1.MSI_WC_TAXWARE_NUMBER IN Msi Wc Taxware Number','SIMPLE','','','Y','','5');
xxeis.eis_rsc_ins.rcnd( '','','MSI_WC_TAXWARE_NUMBER','','Msi Wc Taxware Number','','','','','XXEIS_INV_INVTRY_ATTRIBUTES_V','','','','','','IN','Y','Y','','','','','1',401,'Inventory - WC Item Master Unassigned Taxware Code','XEIIAV1.MSI_WC_TAXWARE_NUMBER IN Msi Wc Taxware Number');
--Inserting Report Sorts - Inventory - WC Item Master Unassigned Taxware Code
xxeis.eis_rsc_ins.rs( 'Inventory - WC Item Master Unassigned Taxware Code',401,'INVENTORY_ITEM','','XXEIS_RS_ADMIN','1','');
--Inserting Report Triggers - Inventory - WC Item Master Unassigned Taxware Code
--inserting report templates - Inventory - WC Item Master Unassigned Taxware Code
--Inserting Report Portals - Inventory - WC Item Master Unassigned Taxware Code
--inserting report dashboards - Inventory - WC Item Master Unassigned Taxware Code
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Inventory - WC Item Master Unassigned Taxware Code','401','XXEIS_INV_INVTRY_ATTRIBUTES_V','XXEIS_INV_INVTRY_ATTRIBUTES_V','N','');
--inserting report security - Inventory - WC Item Master Unassigned Taxware Code
xxeis.eis_rsc_ins.rsec( 'Inventory - WC Item Master Unassigned Taxware Code','707','','XXWC_COST_MANAGEMENT_INQ',401,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory - WC Item Master Unassigned Taxware Code','20005','','XXWC_VIEW_ALL_EIS_REPORTS',401,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory - WC Item Master Unassigned Taxware Code','401','','XXWC_AO_BIN_MTN_CYCLE_REC',401,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory - WC Item Master Unassigned Taxware Code','401','','XXWC_AO_BIN_MTN_REC',401,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory - WC Item Master Unassigned Taxware Code','401','','XXWC_AO_REC',401,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory - WC Item Master Unassigned Taxware Code','660','','XXWC_AO_OEENTRY_REC',401,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory - WC Item Master Unassigned Taxware Code','401','','XXWC_INVENTORY_SUPER_USER',401,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory - WC Item Master Unassigned Taxware Code','401','','XXWC_INVENTORY_SPEC_SCC',401,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory - WC Item Master Unassigned Taxware Code','401','','XXWC_INV_PLANNER',401,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory - WC Item Master Unassigned Taxware Code','401','','XXWC_INVENTORY_CONTROL_SR_MGR',401,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory - WC Item Master Unassigned Taxware Code','401','','XXWC_INVENTORY_CONTROL_INQUIRY',401,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory - WC Item Master Unassigned Taxware Code','401','','INVENTORY',401,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory - WC Item Master Unassigned Taxware Code','401','','XXWC_RECEIVING_ASSOCIATE',401,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory - WC Item Master Unassigned Taxware Code','401','','HDS_INVNTRY',401,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory - WC Item Master Unassigned Taxware Code','401','','XXWC_AO_INV_ADJ_REC',401,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory - WC Item Master Unassigned Taxware Code','401','','XXWC_DATA_MNGT_SC',401,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory - WC Item Master Unassigned Taxware Code','201','','XXWC_PURCHASING_INQUIRY',401,'XXEIS_RS_ADMIN','','','');
--Inserting Report Pivots - Inventory - WC Item Master Unassigned Taxware Code
--Inserting Report Distribution Details 
--Inserting Report Distribution Details 
--Inserting Report SubTotals Details 
--Inserting Report aggregations 
--Inserting Report   Version details- Inventory - WC Item Master Unassigned Taxware Code
xxeis.eis_rsc_ins.rv( 'Inventory - WC Item Master Unassigned Taxware Code','','Inventory - WC Item Master Unassigned Taxware Code','SA059956','09-AUG-2017');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 401');
END IF;
END;
/
