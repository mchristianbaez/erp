--Report Name            : Customer Mass Edit History Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View XXWC_CUSTOMER_MASS_HIST
set scan off define off
prompt Creating View XXEIS.XXWC_CUSTOMER_MASS_HIST
Create or replace View XXEIS.XXWC_CUSTOMER_MASS_HIST
(PROF_ROWID,CUST_ROWID,SITE_USES_ROWID,CUST_ACCOUNT_ID,PRIMARY_SALESREP_ID,SALESREPS_NAME,CREDIT_CLASSIFICATION_MEANING,CREDIT_CLASSIFICATION,PROFILE_CLASS_NAME,PROFILE_CLASS_ID,STANDARD_TERMS_DESCRIPTION,STANDARD_TERMS_NAME,ACCOUNT_PAYMENT_TERMS,REMMIT_TO_CODE_ATTRIBUTE2,CREDIT_HOLD,CREDIT_CHECKING,CREDIT_ANALYST_NAME,CREDIT_ANALYST_ID,COLLECTOR_NAME,P_STAGE,COLLECTOR_ID,ACCOUNT_STATUS,PROFROWID_2,CUSTROWID_2,SITEUSESROWID_2,CUSTACCOUNTID_2,PRIMARYSALESREPID_2,SALESREPSNAME_2,CREDITCLASSIFICATIONMEANING_2,CREDITCLASSIFICATION_2,PROFILECLASSNAME_2,PROFILECLASSID_2,STANDARDTERMSDESCRIPTION_2,STANDARDTERMSNAME_2,ACCOUNTPAYMENTTERMS_2,REMMITTOCODEATTRIBUTE2_2,CREDITHOLD_2,CREDITCHECKING_2,CREDITANALYSTNAME_2,CREDITANALYSTID_2,COLLECTORNAME_2,PSTAGE_2,COLLECTORID_2,ACCOUNTSTATUS_2) AS 
select "PROF_ROWID","CUST_ROWID","SITE_USES_ROWID","CUST_ACCOUNT_ID","PRIMARY_SALESREP_ID","SALESREPS_NAME","CREDIT_CLASSIFICATION_MEANING","CREDIT_CLASSIFICATION","PROFILE_CLASS_NAME","PROFILE_CLASS_ID","STANDARD_TERMS_DESCRIPTION","STANDARD_TERMS_NAME","ACCOUNT_PAYMENT_TERMS","REMMIT_TO_CODE_ATTRIBUTE2","CREDIT_HOLD","CREDIT_CHECKING","CREDIT_ANALYST_NAME","CREDIT_ANALYST_ID","COLLECTOR_NAME","P_STAGE","COLLECTOR_ID","ACCOUNT_STATUS","PROFROWID_2","CUSTROWID_2","SITEUSESROWID_2","CUSTACCOUNTID_2","PRIMARYSALESREPID_2","SALESREPSNAME_2","CREDITCLASSIFICATIONMEANING_2","CREDITCLASSIFICATION_2","PROFILECLASSNAME_2","PROFILECLASSID_2","STANDARDTERMSDESCRIPTION_2","STANDARDTERMSNAME_2","ACCOUNTPAYMENTTERMS_2","REMMITTOCODEATTRIBUTE2_2","CREDITHOLD_2","CREDITCHECKING_2","CREDITANALYSTNAME_2","CREDITANALYSTID_2","COLLECTORNAME_2","PSTAGE_2","COLLECTORID_2","ACCOUNTSTATUS_2"
    FROM (SELECT prof_rowid

              ,cust_rowid

              ,site_uses_rowid

              ,cust_account_id

              ,primary_salesrep_id

              ,salesreps_name

              ,credit_classification_meaning

              ,credit_classification

              ,profile_class_name

              ,profile_class_id

              ,standard_terms_description

              ,standard_terms_name

              ,account_payment_terms

              ,remmit_to_code_attribute2

              ,credit_hold

              ,credit_checking

              ,credit_analyst_name

              ,credit_analyst_id

              ,collector_name

               ,p_stage

              ,collector_id

              ,account_status

          FROM xxwc.xxwc_customer_maint_arch

         WHERE p_stage = 'before') before1

      ,(SELECT prof_rowid as profrowid_2

              ,cust_rowid as custrowid_2

              ,site_uses_rowid as siteusesrowid_2

              ,cust_account_id as custaccountid_2


              ,primary_salesrep_id as primarysalesrepid_2

              ,salesreps_name as salesrepsname_2

              ,credit_classification_meaning as creditclassificationmeaning_2

              ,credit_classification as creditclassification_2

              ,profile_class_name as profileclassname_2

              ,profile_class_id as profileclassid_2

              ,standard_terms_description as standardtermsdescription_2

              ,standard_terms_name as standardtermsname_2

              ,account_payment_terms as accountpaymentterms_2

              ,remmit_to_code_attribute2 as remmittocodeattribute2_2

              ,credit_hold as credithold_2

              ,credit_checking as creditchecking_2

              ,credit_analyst_name as creditanalystname_2

              ,credit_analyst_id as creditanalystid_2

              ,collector_name as collectorname_2

             ,p_stage as pstage_2

              ,collector_id as collectorid_2

              ,account_status as accountstatus_2

          FROM xxwc.xxwc_customer_maint_arch

         WHERE p_stage = 'after') after1

WHERE     after1.profrowid_2 = before1.prof_rowid

       AND after1.custrowid_2 = before1.cust_rowid

       AND after1.custaccountid_2 = before1.cust_account_id

       AND (   after1.primarysalesrepid_2 <> before1.primary_salesrep_id

            OR after1.salesrepsname_2 <> before1.salesreps_name

            OR after1.creditclassificationmeaning_2 <> before1.credit_classification_meaning

            OR after1.creditclassification_2 <> before1.credit_classification

            OR after1.profileclassname_2 <> before1.profile_class_name

            OR after1.profileclassid_2 <> before1.profile_class_id

            OR after1.standardtermsdescription_2 <> before1.standard_terms_description

            OR after1.standardtermsname_2 <> before1.standard_terms_name

            OR after1.accountpaymentterms_2 <> before1.account_payment_terms

            OR after1.remmittocodeattribute2_2 <> before1.remmit_to_code_attribute2

            OR after1.credithold_2 <> before1.credit_hold

            OR after1.creditchecking_2 <> before1.credit_checking

            OR after1.creditanalystname_2 <> before1.credit_analyst_name

            OR after1.creditanalystid_2 <> before1.credit_analyst_id

            OR after1.collectorname_2 <> before1.collector_name

            OR after1.collectorid_2 <> before1.collector_id

            OR after1.accountstatus_2 <> before1.account_status)
/
set scan on define on
prompt Creating View Data for Customer Mass Edit History Report
set scan off define off
DECLARE
BEGIN 
--Inserting View XXWC_CUSTOMER_MASS_HIST
xxeis.eis_rs_ins.v( 'XXWC_CUSTOMER_MASS_HIST',222,'','','','','XXEIS_RS_ADMIN','XXEIS','Xxwc Customer Mass Hist','XCMH','','');
--Delete View Columns for XXWC_CUSTOMER_MASS_HIST
xxeis.eis_rs_utility.delete_view_rows('XXWC_CUSTOMER_MASS_HIST',222,FALSE);
--Inserting View Columns for XXWC_CUSTOMER_MASS_HIST
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_MASS_HIST','ACCOUNTSTATUS_2',222,'Accountstatus 2','ACCOUNTSTATUS_2','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Accountstatus 2','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_MASS_HIST','COLLECTORID_2',222,'Collectorid 2','COLLECTORID_2','','','','XXEIS_RS_ADMIN','NUMBER','','','Collectorid 2','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_MASS_HIST','PSTAGE_2',222,'Pstage 2','PSTAGE_2','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Pstage 2','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_MASS_HIST','COLLECTORNAME_2',222,'Collectorname 2','COLLECTORNAME_2','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Collectorname 2','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_MASS_HIST','CREDITANALYSTID_2',222,'Creditanalystid 2','CREDITANALYSTID_2','','','','XXEIS_RS_ADMIN','NUMBER','','','Creditanalystid 2','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_MASS_HIST','CREDITANALYSTNAME_2',222,'Creditanalystname 2','CREDITANALYSTNAME_2','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Creditanalystname 2','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_MASS_HIST','CREDITCHECKING_2',222,'Creditchecking 2','CREDITCHECKING_2','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Creditchecking 2','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_MASS_HIST','CREDITHOLD_2',222,'Credithold 2','CREDITHOLD_2','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Credithold 2','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_MASS_HIST','REMMITTOCODEATTRIBUTE2_2',222,'Remmittocodeattribute2 2','REMMITTOCODEATTRIBUTE2_2','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Remmittocodeattribute2 2','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_MASS_HIST','ACCOUNTPAYMENTTERMS_2',222,'Accountpaymentterms 2','ACCOUNTPAYMENTTERMS_2','','','','XXEIS_RS_ADMIN','NUMBER','','','Accountpaymentterms 2','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_MASS_HIST','STANDARDTERMSNAME_2',222,'Standardtermsname 2','STANDARDTERMSNAME_2','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Standardtermsname 2','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_MASS_HIST','STANDARDTERMSDESCRIPTION_2',222,'Standardtermsdescription 2','STANDARDTERMSDESCRIPTION_2','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Standardtermsdescription 2','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_MASS_HIST','PROFILECLASSID_2',222,'Profileclassid 2','PROFILECLASSID_2','','','','XXEIS_RS_ADMIN','NUMBER','','','Profileclassid 2','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_MASS_HIST','PROFILECLASSNAME_2',222,'Profileclassname 2','PROFILECLASSNAME_2','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Profileclassname 2','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_MASS_HIST','CREDITCLASSIFICATION_2',222,'Creditclassification 2','CREDITCLASSIFICATION_2','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Creditclassification 2','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_MASS_HIST','CREDITCLASSIFICATIONMEANING_2',222,'Creditclassificationmeaning 2','CREDITCLASSIFICATIONMEANING_2','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Creditclassificationmeaning 2','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_MASS_HIST','SALESREPSNAME_2',222,'Salesrepsname 2','SALESREPSNAME_2','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Salesrepsname 2','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_MASS_HIST','PRIMARYSALESREPID_2',222,'Primarysalesrepid 2','PRIMARYSALESREPID_2','','','','XXEIS_RS_ADMIN','NUMBER','','','Primarysalesrepid 2','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_MASS_HIST','CUSTACCOUNTID_2',222,'Custaccountid 2','CUSTACCOUNTID_2','','','','XXEIS_RS_ADMIN','NUMBER','','','Custaccountid 2','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_MASS_HIST','SITEUSESROWID_2',222,'Siteusesrowid 2','SITEUSESROWID_2','','','','XXEIS_RS_ADMIN','ROWID','','','Siteusesrowid 2','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_MASS_HIST','CUSTROWID_2',222,'Custrowid 2','CUSTROWID_2','','','','XXEIS_RS_ADMIN','ROWID','','','Custrowid 2','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_MASS_HIST','PROFROWID_2',222,'Profrowid 2','PROFROWID_2','','','','XXEIS_RS_ADMIN','ROWID','','','Profrowid 2','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_MASS_HIST','ACCOUNT_STATUS',222,'Account Status','ACCOUNT_STATUS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Account Status','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_MASS_HIST','COLLECTOR_ID',222,'Collector Id','COLLECTOR_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Collector Id','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_MASS_HIST','P_STAGE',222,'P Stage','P_STAGE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','P Stage','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_MASS_HIST','COLLECTOR_NAME',222,'Collector Name','COLLECTOR_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Collector Name','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_MASS_HIST','CREDIT_ANALYST_ID',222,'Credit Analyst Id','CREDIT_ANALYST_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Credit Analyst Id','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_MASS_HIST','CREDIT_ANALYST_NAME',222,'Credit Analyst Name','CREDIT_ANALYST_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Credit Analyst Name','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_MASS_HIST','CREDIT_CHECKING',222,'Credit Checking','CREDIT_CHECKING','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Credit Checking','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_MASS_HIST','CREDIT_HOLD',222,'Credit Hold','CREDIT_HOLD','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Credit Hold','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_MASS_HIST','REMMIT_TO_CODE_ATTRIBUTE2',222,'Remmit To Code Attribute2','REMMIT_TO_CODE_ATTRIBUTE2','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Remmit To Code Attribute2','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_MASS_HIST','ACCOUNT_PAYMENT_TERMS',222,'Account Payment Terms','ACCOUNT_PAYMENT_TERMS','','','','XXEIS_RS_ADMIN','NUMBER','','','Account Payment Terms','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_MASS_HIST','STANDARD_TERMS_NAME',222,'Standard Terms Name','STANDARD_TERMS_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Standard Terms Name','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_MASS_HIST','STANDARD_TERMS_DESCRIPTION',222,'Standard Terms Description','STANDARD_TERMS_DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Standard Terms Description','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_MASS_HIST','PROFILE_CLASS_ID',222,'Profile Class Id','PROFILE_CLASS_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Profile Class Id','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_MASS_HIST','PROFILE_CLASS_NAME',222,'Profile Class Name','PROFILE_CLASS_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Profile Class Name','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_MASS_HIST','CREDIT_CLASSIFICATION',222,'Credit Classification','CREDIT_CLASSIFICATION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Credit Classification','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_MASS_HIST','CREDIT_CLASSIFICATION_MEANING',222,'Credit Classification Meaning','CREDIT_CLASSIFICATION_MEANING','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Credit Classification Meaning','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_MASS_HIST','SALESREPS_NAME',222,'Salesreps Name','SALESREPS_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Salesreps Name','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_MASS_HIST','PRIMARY_SALESREP_ID',222,'Primary Salesrep Id','PRIMARY_SALESREP_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Primary Salesrep Id','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_MASS_HIST','CUST_ACCOUNT_ID',222,'Cust Account Id','CUST_ACCOUNT_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Cust Account Id','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_MASS_HIST','SITE_USES_ROWID',222,'Site Uses Rowid','SITE_USES_ROWID','','','','XXEIS_RS_ADMIN','ROWID','','','Site Uses Rowid','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_MASS_HIST','CUST_ROWID',222,'Cust Rowid','CUST_ROWID','','','','XXEIS_RS_ADMIN','ROWID','','','Cust Rowid','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_MASS_HIST','PROF_ROWID',222,'Prof Rowid','PROF_ROWID','','','','XXEIS_RS_ADMIN','ROWID','','','Prof Rowid','','','');
--Inserting View Components for XXWC_CUSTOMER_MASS_HIST
--Inserting View Component Joins for XXWC_CUSTOMER_MASS_HIST
END;
/
set scan on define on
prompt Creating Report Data for Customer Mass Edit History Report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Customer Mass Edit History Report
xxeis.eis_rs_utility.delete_report_rows( 'Customer Mass Edit History Report' );
--Inserting Report - Customer Mass Edit History Report
xxeis.eis_rs_ins.r( 222,'Customer Mass Edit History Report','','This report is for audit purposes with all changes for specific customer.','','','','XXEIS_RS_ADMIN','XXWC_CUSTOMER_MASS_HIST','Y','','','XXEIS_RS_ADMIN','','N','White Cap Reports','','CSV,EXCEL,','N');
--Inserting Report Columns - Customer Mass Edit History Report
xxeis.eis_rs_ins.rc( 'Customer Mass Edit History Report',222,'ACCOUNTPAYMENTTERMS_2','Accountpaymentterms 2','Accountpaymentterms 2','','~~~','default','','36','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_MASS_HIST','','');
xxeis.eis_rs_ins.rc( 'Customer Mass Edit History Report',222,'ACCOUNTSTATUS_2','Accountstatus 2','Accountstatus 2','','','default','','44','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_MASS_HIST','','');
xxeis.eis_rs_ins.rc( 'Customer Mass Edit History Report',222,'ACCOUNT_PAYMENT_TERMS','Account Payment Terms','Account Payment Terms','','~~~','default','','14','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_MASS_HIST','','');
xxeis.eis_rs_ins.rc( 'Customer Mass Edit History Report',222,'ACCOUNT_STATUS','Account Status','Account Status','','','default','','22','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_MASS_HIST','','');
xxeis.eis_rs_ins.rc( 'Customer Mass Edit History Report',222,'COLLECTORID_2','Collectorid 2','Collectorid 2','','~~~','default','','43','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_MASS_HIST','','');
xxeis.eis_rs_ins.rc( 'Customer Mass Edit History Report',222,'COLLECTORNAME_2','Collectorname 2','Collectorname 2','','','default','','42','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_MASS_HIST','','');
xxeis.eis_rs_ins.rc( 'Customer Mass Edit History Report',222,'COLLECTOR_ID','Collector Id','Collector Id','','~~~','default','','21','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_MASS_HIST','','');
xxeis.eis_rs_ins.rc( 'Customer Mass Edit History Report',222,'COLLECTOR_NAME','Collector Name','Collector Name','','','default','','19','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_MASS_HIST','','');
xxeis.eis_rs_ins.rc( 'Customer Mass Edit History Report',222,'CREDITANALYSTID_2','Creditanalystid 2','Creditanalystid 2','','~~~','default','','41','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_MASS_HIST','','');
xxeis.eis_rs_ins.rc( 'Customer Mass Edit History Report',222,'CREDITANALYSTNAME_2','Creditanalystname 2','Creditanalystname 2','','','default','','40','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_MASS_HIST','','');
xxeis.eis_rs_ins.rc( 'Customer Mass Edit History Report',222,'CREDITCHECKING_2','Creditchecking 2','Creditchecking 2','','','default','','39','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_MASS_HIST','','');
xxeis.eis_rs_ins.rc( 'Customer Mass Edit History Report',222,'CREDITCLASSIFICATIONMEANING_2','Creditclassificationmeaning 2','Creditclassificationmeaning 2','','','default','','30','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_MASS_HIST','','');
xxeis.eis_rs_ins.rc( 'Customer Mass Edit History Report',222,'CREDITCLASSIFICATION_2','Creditclassification 2','Creditclassification 2','','','default','','31','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_MASS_HIST','','');
xxeis.eis_rs_ins.rc( 'Customer Mass Edit History Report',222,'CREDITHOLD_2','Credithold 2','Credithold 2','','','default','','38','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_MASS_HIST','','');
xxeis.eis_rs_ins.rc( 'Customer Mass Edit History Report',222,'CREDIT_ANALYST_ID','Credit Analyst Id','Credit Analyst Id','','~~~','default','','20','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_MASS_HIST','','');
xxeis.eis_rs_ins.rc( 'Customer Mass Edit History Report',222,'CREDIT_ANALYST_NAME','Credit Analyst Name','Credit Analyst Name','','','default','','18','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_MASS_HIST','','');
xxeis.eis_rs_ins.rc( 'Customer Mass Edit History Report',222,'CREDIT_CHECKING','Credit Checking','Credit Checking','','','default','','17','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_MASS_HIST','','');
xxeis.eis_rs_ins.rc( 'Customer Mass Edit History Report',222,'CREDIT_CLASSIFICATION','Credit Classification','Credit Classification','','','default','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_MASS_HIST','','');
xxeis.eis_rs_ins.rc( 'Customer Mass Edit History Report',222,'CREDIT_CLASSIFICATION_MEANING','Credit Classification Meaning','Credit Classification Meaning','','','default','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_MASS_HIST','','');
xxeis.eis_rs_ins.rc( 'Customer Mass Edit History Report',222,'CREDIT_HOLD','Credit Hold','Credit Hold','','','default','','16','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_MASS_HIST','','');
xxeis.eis_rs_ins.rc( 'Customer Mass Edit History Report',222,'CUSTACCOUNTID_2','Custaccountid 2','Custaccountid 2','','~~~','default','','27','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_MASS_HIST','','');
xxeis.eis_rs_ins.rc( 'Customer Mass Edit History Report',222,'CUSTROWID_2','Custrowid 2','Custrowid 2','','','default','','25','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_MASS_HIST','','');
xxeis.eis_rs_ins.rc( 'Customer Mass Edit History Report',222,'CUST_ACCOUNT_ID','Cust Account Id','Cust Account Id','','~~~','default','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_MASS_HIST','','');
xxeis.eis_rs_ins.rc( 'Customer Mass Edit History Report',222,'CUST_ROWID','Cust Rowid','Cust Rowid','','','default','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_MASS_HIST','','');
xxeis.eis_rs_ins.rc( 'Customer Mass Edit History Report',222,'PRIMARYSALESREPID_2','Primarysalesrepid 2','Primarysalesrepid 2','','~~~','default','','28','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_MASS_HIST','','');
xxeis.eis_rs_ins.rc( 'Customer Mass Edit History Report',222,'PRIMARY_SALESREP_ID','Primary Salesrep Id','Primary Salesrep Id','','~~~','default','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_MASS_HIST','','');
xxeis.eis_rs_ins.rc( 'Customer Mass Edit History Report',222,'PROFILECLASSID_2','Profileclassid 2','Profileclassid 2','','~~~','default','','33','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_MASS_HIST','','');
xxeis.eis_rs_ins.rc( 'Customer Mass Edit History Report',222,'PROFILECLASSNAME_2','Profileclassname 2','Profileclassname 2','','','default','','32','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_MASS_HIST','','');
xxeis.eis_rs_ins.rc( 'Customer Mass Edit History Report',222,'PROFILE_CLASS_ID','Profile Class Id','Profile Class Id','','~~~','default','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_MASS_HIST','','');
xxeis.eis_rs_ins.rc( 'Customer Mass Edit History Report',222,'PROFILE_CLASS_NAME','Profile Class Name','Profile Class Name','','','default','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_MASS_HIST','','');
xxeis.eis_rs_ins.rc( 'Customer Mass Edit History Report',222,'PROFROWID_2','Profrowid 2','Profrowid 2','','','default','','24','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_MASS_HIST','','');
xxeis.eis_rs_ins.rc( 'Customer Mass Edit History Report',222,'PROF_ROWID','Prof Rowid','Prof Rowid','','','default','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_MASS_HIST','','');
xxeis.eis_rs_ins.rc( 'Customer Mass Edit History Report',222,'PSTAGE_2','Pstage 2','Pstage 2','','','default','','23','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_MASS_HIST','','');
xxeis.eis_rs_ins.rc( 'Customer Mass Edit History Report',222,'P_STAGE','P Stage','P Stage','','','default','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_MASS_HIST','','');
xxeis.eis_rs_ins.rc( 'Customer Mass Edit History Report',222,'REMMITTOCODEATTRIBUTE2_2','Remmittocodeattribute2 2','Remmittocodeattribute2 2','','','default','','37','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_MASS_HIST','','');
xxeis.eis_rs_ins.rc( 'Customer Mass Edit History Report',222,'REMMIT_TO_CODE_ATTRIBUTE2','Remmit To Code Attribute2','Remmit To Code Attribute2','','','default','','15','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_MASS_HIST','','');
xxeis.eis_rs_ins.rc( 'Customer Mass Edit History Report',222,'SALESREPSNAME_2','Salesrepsname 2','Salesrepsname 2','','','default','','29','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_MASS_HIST','','');
xxeis.eis_rs_ins.rc( 'Customer Mass Edit History Report',222,'SALESREPS_NAME','Salesreps Name','Salesreps Name','','','default','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_MASS_HIST','','');
xxeis.eis_rs_ins.rc( 'Customer Mass Edit History Report',222,'SITEUSESROWID_2','Siteusesrowid 2','Siteusesrowid 2','','','default','','26','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_MASS_HIST','','');
xxeis.eis_rs_ins.rc( 'Customer Mass Edit History Report',222,'SITE_USES_ROWID','Site Uses Rowid','Site Uses Rowid','','','default','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_MASS_HIST','','');
xxeis.eis_rs_ins.rc( 'Customer Mass Edit History Report',222,'STANDARDTERMSDESCRIPTION_2','Standardtermsdescription 2','Standardtermsdescription 2','','','default','','34','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_MASS_HIST','','');
xxeis.eis_rs_ins.rc( 'Customer Mass Edit History Report',222,'STANDARDTERMSNAME_2','Standardtermsname 2','Standardtermsname 2','','','default','','35','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_MASS_HIST','','');
xxeis.eis_rs_ins.rc( 'Customer Mass Edit History Report',222,'STANDARD_TERMS_DESCRIPTION','Standard Terms Description','Standard Terms Description','','','default','','12','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_MASS_HIST','','');
xxeis.eis_rs_ins.rc( 'Customer Mass Edit History Report',222,'STANDARD_TERMS_NAME','Standard Terms Name','Standard Terms Name','','','default','','13','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_MASS_HIST','','');
--Inserting Report Parameters - Customer Mass Edit History Report
xxeis.eis_rs_ins.rp( 'Customer Mass Edit History Report',222,'Cust Account Id','Cust Account Id','CUST_ACCOUNT_ID','IN','','','VARCHAR2','N','Y','1','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
--Inserting Report Conditions - Customer Mass Edit History Report
xxeis.eis_rs_ins.rcn( 'Customer Mass Edit History Report',222,'CUST_ACCOUNT_ID','IN',':Cust Account Id','','','Y','1','Y','XXEIS_RS_ADMIN');
--Inserting Report Sorts - Customer Mass Edit History Report
--Inserting Report Triggers - Customer Mass Edit History Report
--Inserting Report Templates - Customer Mass Edit History Report
--Inserting Report Portals - Customer Mass Edit History Report
--Inserting Report Dashboards - Customer Mass Edit History Report
--Inserting Report Security - Customer Mass Edit History Report
xxeis.eis_rs_ins.rsec( 'Customer Mass Edit History Report','20005','','50900',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Customer Mass Edit History Report','20005','','50843',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Customer Mass Edit History Report','20005','','51185',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Customer Mass Edit History Report','20005','','50861',222,'XXEIS_RS_ADMIN','','');
--Inserting Report Pivots - Customer Mass Edit History Report
END;
/
set scan on define on
