--Report Name            : Specials Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View EIS_XXWC_INV_SPECIAL_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_INV_SPECIAL_V
Create or replace View XXEIS.EIS_XXWC_INV_SPECIAL_V
(ORGANIZATION_CODE,PART_NUMBER,DESCRIPTION,CAT,UOM,SELLING_PRICE,AVERAGECOST,GP,WEIGHT,ONHAND,EXTENDED_VALUE,VENDOR_NAME,VENDOR_NUMBER,BUYER_CODE,DATE_RECEIVED,OLDEST_BORN_DATE,OPEN_SALES_ORDERS,NO_OF_ITEM_PURCHASED,CREATION_DATE,INVENTORY_ITEM_ID,INV_ORGANIZATION_ID,ORG_ORGANIZATION_ID,AGENT_ID,MSI#HDS#LOB,MSI#HDS#DROP_SHIPMENT,MSI#HDS#INVOICE_UOM,MSI#HDS#PRODUCT_ID,MSI#HDS#VENDOR_PART_NUMBER,MSI#HDS#UNSPSC_CODE,MSI#HDS#UPC_PRIMARY,MSI#HDS#SKU_DESCRIPTION,MSI#WC#CA_PROP_65,MSI#WC#COUNTRY_OF_ORIGIN,MSI#WC#ORM_D_FLAG,MSI#WC#STORE_VELOCITY,MSI#WC#DC_VELOCITY,MSI#WC#YEARLY_STORE_VELOCITY,MSI#WC#YEARLY_DC_VELOCITY,MSI#WC#PRISM_PART_NUMBER,MSI#WC#HAZMAT_DESCRIPTION,MSI#WC#HAZMAT_CONTAINER,MSI#WC#GTP_INDICATOR,MSI#WC#LAST_LEAD_TIME,MSI#WC#AMU,MSI#WC#RESERVE_STOCK,MSI#WC#TAXWARE_CODE,MSI#WC#AVERAGE_UNITS,MSI#WC#PRODUCT_CODE,MSI#WC#IMPORT_DUTY_,MSI#WC#KEEP_ITEM_ACTIVE,MSI#WC#PESTICIDE_FLAG,MSI#WC#CALC_LEAD_TIME,MSI#WC#VOC_GL,MSI#WC#PESTICIDE_FLAG_STATE,MSI#WC#VOC_CATEGORY,MSI#WC#VOC_SUB_CATEGORY,MSI#WC#MSDS_#,MSI#WC#HAZMAT_PACKAGING_GROU) AS 
SELECT ood.organization_code ,
    msi.segment1 part_number,
    msi.description,
    xxeis.eis_rs_xxwc_com_util_pkg.Get_inv_cat_class(msi.inventory_item_id,msi.organization_id) cat,
    msi.primary_uom_code uom,
    --ol.unit_list_price selling_price,
    xxeis.eis_rs_xxwc_com_util_pkg.Get_list_Price(msi.inventory_item_id,msi.organization_id) selling_price,
    NVL(APPS.CST_COST_API.GET_ITEM_COST(1,MSI.INVENTORY_ITEM_ID,MSI.ORGANIZATION_ID),0) AVERAGECOST,
    --ROUND((((XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_LIST_PRICE(MSI.INVENTORY_ITEM_ID,MSI.ORGANIZATION_ID))-(NVL(APPS.CST_COST_API.GET_ITEM_COST(1,MSI.INVENTORY_ITEM_ID,MSI.ORGANIZATION_ID),0)))*100)/(XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_LIST_PRICE(MSI.INVENTORY_ITEM_ID,MSI.ORGANIZATION_ID)),2) GP,
    CASE
      WHEN NVL(XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_LIST_PRICE(MSI.INVENTORY_ITEM_ID,MSI.ORGANIZATION_ID),0) =0
      THEN 0
      ELSE ROUND(((NVL(XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_LIST_PRICE(MSI.INVENTORY_ITEM_ID,MSI.ORGANIZATION_ID),0)-(NVL(APPS.CST_COST_API.GET_ITEM_COST(1,MSI.INVENTORY_ITEM_ID,MSI.ORGANIZATION_ID),0)))*100)/(XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_LIST_PRICE(MSI.INVENTORY_ITEM_ID,MSI.ORGANIZATION_ID)),2)
    END GP,
    MSI.UNIT_WEIGHT WEIGHT,
    -- xxeis.eis_po_xxwc_isr_util_qa_pkg.GET_PLANNING_QUANTITY(2,1,MSI.ORGANIZATION_ID,NULL,MSI.INVENTORY_ITEM_ID)QOH,
    -- MOQD.TRANSACTION_QUANTITY ONHAND,
    xxeis.eis_po_xxwc_isr_util_qa_pkg.GET_PLANNING_QUANTITY(2,1,MSI.ORGANIZATION_ID,NULL,MSI.INVENTORY_ITEM_ID) ONHAND,
    --xxeis.eis_rs_xxwc_com_util_pkg.get_onhand_inv(msi.inventory_item_id,msi.organization_id) ONHAND,
    (ROUND(NVL(apps.cst_cost_api.get_item_cost(1,msi.inventory_item_id,msi.organization_id),0),2)*(xxeis.eis_rs_xxwc_com_util_pkg.get_onhand_inv(msi.inventory_item_id,msi.organization_id))) extended_value,
    xxeis.eis_rs_xxwc_com_util_pkg.get_po_vendor_name(msi.inventory_item_id,msi.organization_id) vendor_name,
    XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.get_po_vendor_number(MSI.INVENTORY_ITEM_ID,MSI.ORGANIZATION_ID) VENDOR_NUMBER,
    POA.AGENT_NAME BUYER_CODE,
    xxeis.eis_rs_xxwc_com_util_pkg.get_onhand_date(msi.inventory_item_id,msi.organization_id) date_received,
    xxeis.eis_rs_xxwc_com_util_pkg.get_oldest_born_date(msi.inventory_item_id,msi.organization_id) OLDEST_BORN_DATE,
    --gps.start_date,
    --gps.end_date,
    XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_OPEN_SALES_ORDERS(MSI.INVENTORY_ITEM_ID,MSI.ORGANIZATION_ID) OPEN_SALES_ORDERS,
    XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.Get_item_purchased(MSI.INVENTORY_ITEM_ID,MSI.ORGANIZATION_ID) no_of_item_purchased,
    TRUNC(msi.creation_date) creation_date,
    --primary keys
    msi.inventory_item_id,
    msi.organization_id inv_organization_id,
    OOD.ORGANIZATION_ID ORG_ORGANIZATION_ID,
    --GPS.APPLICATION_ID,
    --GPS.SET_OF_BOOKS_ID,
    --GPS.PERIOD_NAME,
    POA.AGENT_ID
    --descr#flexfield#start
    ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'HDS',MSI.ATTRIBUTE1, NULL) MSI#HDS#LOB ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'HDS',xxeis.eis_rs_dff.decode_valueset( 'Yes_No',MSI.ATTRIBUTE10,'F'), NULL) MSI#HDS#Drop_Shipment ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'HDS',MSI.ATTRIBUTE15, NULL) MSI#HDS#Invoice_UOM ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'HDS',MSI.ATTRIBUTE2, NULL) MSI#HDS#Product_ID ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'HDS',MSI.ATTRIBUTE3, NULL) MSI#HDS#Vendor_Part_Number ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'HDS',MSI.ATTRIBUTE4, NULL) MSI#HDS#UNSPSC_Code ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'HDS',MSI.ATTRIBUTE5, NULL) MSI#HDS#UPC_Primary ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'HDS',MSI.ATTRIBUTE7, NULL) MSI#HDS#SKU_Description ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE1, NULL) MSI#WC#CA_Prop_65 ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE10, NULL) MSI#WC#Country_of_Origin ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',xxeis.eis_rs_dff.decode_valueset( 'Yes_No',MSI.ATTRIBUTE11,'F'), NULL) MSI#WC#ORM_D_Flag ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE12, NULL) MSI#WC#Store_Velocity ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE13, NULL) MSI#WC#DC_Velocity ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE14, NULL) MSI#WC#Yearly_Store_Velocity ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE15, NULL) MSI#WC#Yearly_DC_Velocity ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE16, NULL) MSI#WC#PRISM_Part_Number ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE17, NULL) MSI#WC#Hazmat_Description ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',xxeis.eis_rs_dff.decode_valueset( 'XXWC HAZMAT CONTAINER',MSI.ATTRIBUTE18,'I'), NULL) MSI#WC#Hazmat_Container ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE19, NULL) MSI#WC#GTP_Indicator ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE2, NULL) MSI#WC#Last_Lead_Time ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE20, NULL) MSI#WC#AMU ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE21, NULL) MSI#WC#Reserve_Stock ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',xxeis.eis_rs_dff.decode_valueset( 'XXWC TAXWARE CODE',MSI.ATTRIBUTE22,'I'), NULL) MSI#WC#Taxware_Code ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE25, NULL) MSI#WC#Average_Units ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE26, NULL) MSI#WC#Product_code ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE27, NULL) MSI#WC#Import_Duty_ ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',xxeis.eis_rs_dff.decode_valueset( 'Yes_No',MSI.ATTRIBUTE29,'F'), NULL) MSI#WC#Keep_Item_Active ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',xxeis.eis_rs_dff.decode_valueset( 'Yes_No',MSI.ATTRIBUTE3,'F'), NULL) MSI#WC#Pesticide_Flag ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE30, NULL) MSI#WC#Calc_Lead_Time ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE4, NULL) MSI#WC#VOC_GL ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE5, NULL) MSI#WC#Pesticide_Flag_State ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE6, NULL) MSI#WC#VOC_Category ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE7, NULL) MSI#WC#VOC_Sub_Category ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE8, NULL) MSI#WC#MSDS_# ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',xxeis.eis_rs_dff.decode_valueset( 'XXWC_HAZMAT_PACKAGE_GROUP',MSI.ATTRIBUTE9,'I'), NULL) MSI#WC#Hazmat_Packaging_Grou
    --descr#flexfield#end
    --gl#accountff#start
    --gl#accountff#end
    --  ;
    --CASE WHEN  NVL(XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_LIST_PRICE(MSI.INVENTORY_ITEM_ID,MSI.ORGANIZATION_ID),0) =0 THEN 0 ELSE   ROUND(((NVL(xxeis.eis_rs_xxwc_com_util_pkg.Get_list_Price(msi.inventory_item_id,msi.organization_id),0)-(NVL(apps.cst_cost_api.get_item_cost(1,msi.inventory_item_id,msi.organization_id),0)))*100)/(xxeis.eis_rs_xxwc_com_util_pkg.Get_list_Price(msi.inventory_item_id,msi.organization_id)),2) END GP
    --    select NVL(XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_LIST_PRICE(MSI.INVENTORY_ITEM_ID,MSI.ORGANIZATION_ID),0) PRICE_LIST_PRICE,
    --   NVL(apps.cst_cost_api.get_item_cost(1,msi.inventory_item_id,msi.organization_id),0) avg_cost
  FROM mtl_system_items_kfv msi,
    -- mtl_onhand_quantities_detail moqd,
    org_organization_definitions ood,
    po_agents_v poa--,
    --gl_period_statuses gps
  WHERE 1                 =1
  AND msi.organization_id =ood.organization_id
    --AND msi.inventory_item_id =moqd.inventory_item_id
    --AND msi.organization_id   =moqd.organization_id
  AND msi.buyer_id  =poa.agent_id(+)
  AND msi.Item_type = 'SPECIAL'
    -- AND msi.segment1 LIKE 'SP%'
    --AND gps.application_id =101
    --AND gps.set_of_books_id=ood.set_of_books_id
    -- AND TRUNC(moqd.date_received) BETWEEN gps.start_date AND gps.end_date
    --AND MOQD.TRANSACTION_QUANTITY <> 0
  AND EXISTS
    (SELECT 1
    FROM XXEIS.EIS_ORG_ACCESS_V
    WHERE ORGANIZATION_ID = MSI.ORGANIZATION_ID
    )
/
set scan on define on
prompt Creating View Data for Specials Report
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_INV_SPECIAL_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_INV_SPECIAL_V',401,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Rs Xxwc Inv Special V','EXISV','','');
--Delete View Columns for EIS_XXWC_INV_SPECIAL_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_INV_SPECIAL_V',401,FALSE);
--Inserting View Columns for EIS_XXWC_INV_SPECIAL_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','BUYER_CODE',401,'Buyer Code','BUYER_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Buyer Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','ONHAND',401,'Onhand','ONHAND','','','','XXEIS_RS_ADMIN','NUMBER','','','Onhand','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','WEIGHT',401,'Weight','WEIGHT','','','','XXEIS_RS_ADMIN','NUMBER','','','Weight','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','AVERAGECOST',401,'Averagecost','AVERAGECOST','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Averagecost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','SELLING_PRICE',401,'Selling Price','SELLING_PRICE','','','','XXEIS_RS_ADMIN','NUMBER','','','Selling Price','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','UOM',401,'Uom','UOM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Uom','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','CAT',401,'Cat','CAT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cat','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','DESCRIPTION',401,'Description','DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','PART_NUMBER',401,'Part Number','PART_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Part Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','EXTENDED_VALUE',401,'Extended Value','EXTENDED_VALUE','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Extended Value','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','VENDOR_NAME',401,'Vendor Name','VENDOR_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','VENDOR_NUMBER',401,'Vendor Number','VENDOR_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','GP',401,'Gp','GP','','','','XXEIS_RS_ADMIN','NUMBER','','','Gp','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','ORGANIZATION_CODE',401,'Organization Code','ORGANIZATION_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Organization Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','OPEN_SALES_ORDERS',401,'Open Sales Orders','OPEN_SALES_ORDERS','','','','XXEIS_RS_ADMIN','NUMBER','','','Open Sales Orders','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','NO_OF_ITEM_PURCHASED',401,'No Of Item Purchased','NO_OF_ITEM_PURCHASED','','','','XXEIS_RS_ADMIN','NUMBER','','','No Of Item Purchased','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','CREATION_DATE',401,'Creation Date','CREATION_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Creation Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','AGENT_ID',401,'Agent Id','AGENT_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Agent Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','ORG_ORGANIZATION_ID',401,'Org Organization Id','ORG_ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Org Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','INV_ORGANIZATION_ID',401,'Inv Organization Id','INV_ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Inv Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','INVENTORY_ITEM_ID',401,'Inventory Item Id','INVENTORY_ITEM_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Inventory Item Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','MSI#HDS#LOB',401,'Descriptive flexfield: Items Column Name: LOB Context: HDS','MSI#HDS#LOB','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE1','Msi#Hds#Lob','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','MSI#HDS#DROP_SHIPMENT',401,'Descriptive flexfield: Items Column Name: Drop Shipment Context: HDS','MSI#HDS#Drop_Shipment','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE10','Msi#Hds#Drop Shipment','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','MSI#HDS#INVOICE_UOM',401,'Descriptive flexfield: Items Column Name: Invoice UOM Context: HDS','MSI#HDS#Invoice_UOM','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE15','Msi#Hds#Invoice Uom','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','MSI#HDS#PRODUCT_ID',401,'Descriptive flexfield: Items Column Name: Product ID Context: HDS','MSI#HDS#Product_ID','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE2','Msi#Hds#Product Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','MSI#HDS#VENDOR_PART_NUMBER',401,'Descriptive flexfield: Items Column Name: Vendor Part Number Context: HDS','MSI#HDS#Vendor_Part_Number','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE3','Msi#Hds#Vendor Part Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','MSI#HDS#UNSPSC_CODE',401,'Descriptive flexfield: Items Column Name: UNSPSC Code Context: HDS','MSI#HDS#UNSPSC_Code','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE4','Msi#Hds#Unspsc Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','MSI#HDS#UPC_PRIMARY',401,'Descriptive flexfield: Items Column Name: UPC Primary Context: HDS','MSI#HDS#UPC_Primary','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE5','Msi#Hds#Upc Primary','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','MSI#HDS#SKU_DESCRIPTION',401,'Descriptive flexfield: Items Column Name: SKU Description Context: HDS','MSI#HDS#SKU_Description','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE7','Msi#Hds#Sku Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','MSI#WC#CA_PROP_65',401,'Descriptive flexfield: Items Column Name: CA Prop 65 Context: WC','MSI#WC#CA_Prop_65','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE1','Msi#Wc#Ca Prop 65','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','MSI#WC#COUNTRY_OF_ORIGIN',401,'Descriptive flexfield: Items Column Name: Country of Origin Context: WC','MSI#WC#Country_of_Origin','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE10','Msi#Wc#Country Of Origin','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','MSI#WC#ORM_D_FLAG',401,'Descriptive flexfield: Items Column Name: ORM-D Flag Context: WC','MSI#WC#ORM_D_Flag','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE11','Msi#Wc#Orm-D Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','MSI#WC#STORE_VELOCITY',401,'Descriptive flexfield: Items Column Name: Store Velocity Context: WC','MSI#WC#Store_Velocity','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE12','Msi#Wc#Store Velocity','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','MSI#WC#DC_VELOCITY',401,'Descriptive flexfield: Items Column Name: DC Velocity Context: WC','MSI#WC#DC_Velocity','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE13','Msi#Wc#Dc Velocity','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','MSI#WC#YEARLY_STORE_VELOCITY',401,'Descriptive flexfield: Items Column Name: Yearly Store Velocity Context: WC','MSI#WC#Yearly_Store_Velocity','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE14','Msi#Wc#Yearly Store Velocity','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','MSI#WC#YEARLY_DC_VELOCITY',401,'Descriptive flexfield: Items Column Name: Yearly DC Velocity Context: WC','MSI#WC#Yearly_DC_Velocity','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE15','Msi#Wc#Yearly Dc Velocity','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','MSI#WC#PRISM_PART_NUMBER',401,'Descriptive flexfield: Items Column Name: PRISM Part Number Context: WC','MSI#WC#PRISM_Part_Number','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE16','Msi#Wc#Prism Part Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','MSI#WC#HAZMAT_DESCRIPTION',401,'Descriptive flexfield: Items Column Name: Hazmat Description Context: WC','MSI#WC#Hazmat_Description','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE17','Msi#Wc#Hazmat Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','MSI#WC#HAZMAT_CONTAINER',401,'Descriptive flexfield: Items Column Name: Hazmat Container Context: WC','MSI#WC#Hazmat_Container','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE18','Msi#Wc#Hazmat Container','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','MSI#WC#GTP_INDICATOR',401,'Descriptive flexfield: Items Column Name: GTP Indicator Context: WC','MSI#WC#GTP_Indicator','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE19','Msi#Wc#Gtp Indicator','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','MSI#WC#LAST_LEAD_TIME',401,'Descriptive flexfield: Items Column Name: Last Lead Time Context: WC','MSI#WC#Last_Lead_Time','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE2','Msi#Wc#Last Lead Time','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','MSI#WC#AMU',401,'Descriptive flexfield: Items Column Name: AMU Context: WC','MSI#WC#AMU','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE20','Msi#Wc#Amu','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','MSI#WC#RESERVE_STOCK',401,'Descriptive flexfield: Items Column Name: Reserve Stock Context: WC','MSI#WC#Reserve_Stock','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE21','Msi#Wc#Reserve Stock','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','MSI#WC#TAXWARE_CODE',401,'Descriptive flexfield: Items Column Name: Taxware Code Context: WC','MSI#WC#Taxware_Code','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE22','Msi#Wc#Taxware Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','MSI#WC#AVERAGE_UNITS',401,'Descriptive flexfield: Items Column Name: Average Units Context: WC','MSI#WC#Average_Units','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE25','Msi#Wc#Average Units','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','MSI#WC#PRODUCT_CODE',401,'Descriptive flexfield: Items Column Name: Product code Context: WC','MSI#WC#Product_code','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE26','Msi#Wc#Product Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','MSI#WC#IMPORT_DUTY_',401,'Descriptive flexfield: Items Column Name: Import Duty % Context: WC','MSI#WC#Import_Duty_','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE27','Msi#Wc#Import Duty %','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','MSI#WC#KEEP_ITEM_ACTIVE',401,'Descriptive flexfield: Items Column Name: Keep Item Active Context: WC','MSI#WC#Keep_Item_Active','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE29','Msi#Wc#Keep Item Active','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','MSI#WC#PESTICIDE_FLAG',401,'Descriptive flexfield: Items Column Name: Pesticide Flag Context: WC','MSI#WC#Pesticide_Flag','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE3','Msi#Wc#Pesticide Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','MSI#WC#CALC_LEAD_TIME',401,'Descriptive flexfield: Items Column Name: Calc Lead Time Context: WC','MSI#WC#Calc_Lead_Time','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE30','Msi#Wc#Calc Lead Time','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','MSI#WC#VOC_GL',401,'Descriptive flexfield: Items Column Name: VOC G/L Context: WC','MSI#WC#VOC_GL','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE4','Msi#Wc#Voc G/L','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','MSI#WC#PESTICIDE_FLAG_STATE',401,'Descriptive flexfield: Items Column Name: Pesticide Flag State Context: WC','MSI#WC#Pesticide_Flag_State','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE5','Msi#Wc#Pesticide Flag State','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','MSI#WC#VOC_CATEGORY',401,'Descriptive flexfield: Items Column Name: VOC Category Context: WC','MSI#WC#VOC_Category','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE6','Msi#Wc#Voc Category','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','MSI#WC#VOC_SUB_CATEGORY',401,'Descriptive flexfield: Items Column Name: VOC Sub Category Context: WC','MSI#WC#VOC_Sub_Category','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE7','Msi#Wc#Voc Sub Category','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','MSI#WC#MSDS_#',401,'Descriptive flexfield: Items Column Name: MSDS # Context: WC','MSI#WC#MSDS_#','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE8','Msi#Wc#Msds #','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','MSI#WC#HAZMAT_PACKAGING_GROU',401,'Descriptive flexfield: Items Column Name: Hazmat Packaging Group Context: WC','MSI#WC#Hazmat_Packaging_Grou','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE9','Msi#Wc#Hazmat Packaging Group','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','DATE_RECEIVED',401,'Date Received','DATE_RECEIVED','','','','XXEIS_RS_ADMIN','DATE','','','Date Received','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','OLDEST_BORN_DATE',401,'Oldest Born Date','OLDEST_BORN_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Oldest Born Date','','','');
--Inserting View Components for EIS_XXWC_INV_SPECIAL_V
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_SPECIAL_V','PO_AGENTS_V',401,'PO_AGENTS','POA','POA','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Buyers Table','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_SPECIAL_V','GL_PERIOD_STATUSES',401,'GL_PERIOD_STATUSES','GPS','GPS','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Calendar Period Statuses','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_SPECIAL_V','MTL_SYSTEM_ITEMS_KFV',401,'MTL_SYSTEM_ITEMS_B','MSI','MSI','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Inventory Item Definitions','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_SPECIAL_V','MTL_CATEGORIES',401,'MTL_CATEGORIES_B','MCV','MCV','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Categories','','','','');
--Inserting View Component Joins for EIS_XXWC_INV_SPECIAL_V
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_SPECIAL_V','PO_AGENTS_V','POA',401,'EXISV.AGENT_ID','=','POA.AGENT_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_SPECIAL_V','GL_PERIOD_STATUSES','GPS',401,'EXISV.APPLICATION_ID','=','GPS.APPLICATION_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_SPECIAL_V','GL_PERIOD_STATUSES','GPS',401,'EXISV.SET_OF_BOOKS_ID','=','GPS.SET_OF_BOOKS_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_SPECIAL_V','GL_PERIOD_STATUSES','GPS',401,'EXISV.PERIOD_NAME','=','GPS.PERIOD_NAME(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_SPECIAL_V','MTL_CATEGORIES','MCV',401,'EXISV.CATEGORY_ID','=','MCV.CATEGORY_ID(+)','','','','','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_SPECIAL_V','MTL_SYSTEM_ITEMS_KFV','MSI',401,'EXISV.INVENTORY_ITEM_ID','=','MSI.INVENTORY_ITEM_ID(+)','','','','','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_SPECIAL_V','MTL_SYSTEM_ITEMS_KFV','MSI',401,'EXISV.INV_ORGANIZATION_ID','=','MSI.ORGANIZATION_ID(+)','','','','','XXEIS_RS_ADMIN','','');
END;
/
set scan on define on
prompt Creating Report LOV Data for Specials Report
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Specials Report
xxeis.eis_rs_ins.lov( 401,'SELECT ''Include Specials with Oh Hand > 7 days'' onhand_type FROM dual
UNION 
SELECT ''Include Specials with Oh Hand < 7 days'' onhand_type FROM dual
union 
SELECT ''ALL specials'' onhand_type FROM dual','','INV SPECIALONHAND TYPE','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'SELECT organization_code code,organization_name name
FROM ORG_ORGANIZATION_DEFINITIONS OOD
WHERE EXISTS
  (SELECT 1
  FROM XXEIS.EIS_ORG_ACCESS_V
  WHERE organization_id = ood.organization_id
  )
ORDER BY organization_code','','XXWC Inventory Org List','List of All Inventory Orgs under a given operating unit.','XXEIS_RS_ADMIN',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Specials Report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Specials Report
xxeis.eis_rs_utility.delete_report_rows( 'Specials Report' );
--Inserting Report - Specials Report
xxeis.eis_rs_ins.r( 401,'Specials Report','','report on all special parts ordered; special parts on hand with or without demand; report to flag special parts to become standard parts (i.e. they are used a lot)','','','','XXEIS_RS_ADMIN','EIS_XXWC_INV_SPECIAL_V','Y','','','XXEIS_RS_ADMIN','','N','White Cap Reports','','Pivot Excel,EXCEL,','');
--Inserting Report Columns - Specials Report
xxeis.eis_rs_ins.rc( 'Specials Report',401,'AVERAGECOST','Avg Cost','Averagecost','','~T~D~2','default','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_SPECIAL_V','','');
xxeis.eis_rs_ins.rc( 'Specials Report',401,'BUYER_CODE','Buyer','Buyer Code','','','default','','15','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_SPECIAL_V','','');
xxeis.eis_rs_ins.rc( 'Specials Report',401,'CAT','Cat Class','Cat','','','default','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_SPECIAL_V','','');
xxeis.eis_rs_ins.rc( 'Specials Report',401,'DESCRIPTION','Description','Description','','','default','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_SPECIAL_V','','');
xxeis.eis_rs_ins.rc( 'Specials Report',401,'ONHAND','On Hand','Onhand','','~~~','default','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_SPECIAL_V','','');
xxeis.eis_rs_ins.rc( 'Specials Report',401,'PART_NUMBER','Part Number','Part Number','','','default','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_SPECIAL_V','','');
xxeis.eis_rs_ins.rc( 'Specials Report',401,'SELLING_PRICE','Selling Price','Selling Price','','~~~','default','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_SPECIAL_V','','');
xxeis.eis_rs_ins.rc( 'Specials Report',401,'UOM','UOM','Uom','','','default','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_SPECIAL_V','','');
xxeis.eis_rs_ins.rc( 'Specials Report',401,'WEIGHT','Weight','Weight','','~~~','default','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_SPECIAL_V','','');
xxeis.eis_rs_ins.rc( 'Specials Report',401,'EXTENDED_VALUE','Extended Value','Extended Value','','~T~D~2','default','','12','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_SPECIAL_V','','');
xxeis.eis_rs_ins.rc( 'Specials Report',401,'VENDOR_NAME','Vendor Name','Vendor Name','','','default','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_SPECIAL_V','','');
xxeis.eis_rs_ins.rc( 'Specials Report',401,'VENDOR_NUMBER','Vendor Number','Vendor Number','','','default','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_SPECIAL_V','','');
xxeis.eis_rs_ins.rc( 'Specials Report',401,'GP','GP%','Gp','','~T~D~2','default','','16','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_SPECIAL_V','','');
xxeis.eis_rs_ins.rc( 'Specials Report',401,'OPEN_SALES_ORDERS','Open Sales Orders','Open Sales Orders','','~~~','default','','14','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_SPECIAL_V','','');
xxeis.eis_rs_ins.rc( 'Specials Report',401,'ORGANIZATION_CODE','Org Number','Organization Code','','','default','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_SPECIAL_V','','');
xxeis.eis_rs_ins.rc( 'Specials Report',401,'NO_OF_ITEM_PURCHASED','No of times purchased','No Of Item Purchased','','~~~','default','','17','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_SPECIAL_V','','');
xxeis.eis_rs_ins.rc( 'Specials Report',401,'OLDEST_BORN_DATE','Oldest Born Date','Oldest Born Date','','','','','13','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_SPECIAL_V','','');
--Inserting Report Parameters - Specials Report
xxeis.eis_rs_ins.rp( 'Specials Report',401,'Organization','Organization','ORGANIZATION_CODE','IN','XXWC Inventory Org List','','VARCHAR2','Y','Y','1','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Specials Report',401,'Select Items for report','Select Items for report','','IN','INV SPECIALONHAND TYPE','''ALL specials''','VARCHAR2','N','Y','2','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
--Inserting Report Conditions - Specials Report
xxeis.eis_rs_ins.rcn( 'Specials Report',401,'ORGANIZATION_CODE','IN',':Organization','','','Y','2','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Specials Report',401,'','','','','AND ((:Select Items for report = ''Include Specials with Oh Hand > 7 days''
          AND  (sysdate -EXISV.date_received) >7 AnD  EXISV.ONHAND > 0)
     OR (:Select Items for report =''Include Specials with Oh Hand < 7 days''
          AND  (sysdate -EXISV.date_received) <= 7 AnD  EXISV.ONHAND > 0)
     OR (:Select Items for report =''ALL specials'')
     )','Y','0','','XXEIS_RS_ADMIN');
--Inserting Report Sorts - Specials Report
xxeis.eis_rs_ins.rs( 'Specials Report',401,'ORGANIZATION_CODE','ASC','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rs( 'Specials Report',401,'PART_NUMBER','ASC','XXEIS_RS_ADMIN','','');
--Inserting Report Triggers - Specials Report
--Inserting Report Templates - Specials Report
--Inserting Report Portals - Specials Report
--Inserting Report Dashboards - Specials Report
--Inserting Report Security - Specials Report
xxeis.eis_rs_ins.rsec( 'Specials Report','401','','50619',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Specials Report','401','','50924',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Specials Report','401','','51052',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Specials Report','401','','50879',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Specials Report','401','','50851',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Specials Report','401','','50852',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Specials Report','401','','50821',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Specials Report','20005','','50880',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Specials Report','401','','51029',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Specials Report','401','','50882',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Specials Report','401','','50883',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Specials Report','401','','50981',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Specials Report','401','','50855',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Specials Report','401','','50884',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Specials Report','401','','20634',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Specials Report','20005','','50900',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Specials Report','401','','50895',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Specials Report','401','','50865',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Specials Report','401','','50864',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Specials Report','401','','50849',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Specials Report','660','','50871',401,'XXEIS_RS_ADMIN','','');
--Inserting Report Pivots - Specials Report
xxeis.eis_rs_ins.rpivot( 'Specials Report',401,'Onhand By Born date and Item','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Onhand By Born date and Item
xxeis.eis_rs_ins.rpivot_dtls( 'Specials Report',401,'Onhand By Born date and Item','ORGANIZATION_CODE','PAGE_FIELD','','','1','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Specials Report',401,'Onhand By Born date and Item','ONHAND','DATA_FIELD','SUM','','1','','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Specials Report',401,'Onhand By Born date and Item','OLDEST_BORN_DATE','ROW_FIELD','','','1','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'Specials Report',401,'Onhand By Born date and Item','PART_NUMBER','ROW_FIELD','','','2','1','');
--Inserting Report Summary Calculation Columns For Pivot- Onhand By Born date and Item
END;
/
set scan on define on
