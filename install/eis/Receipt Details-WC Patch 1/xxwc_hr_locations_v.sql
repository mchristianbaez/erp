CREATE OR REPLACE VIEW XXEIS.XXWC_HR_LOCATIONS_V
AS
  SELECT LOCATION_ID,
    SUBSTR(LOCATION_CODE,1,3) LOCATION_CODE,
    INVENTORY_ORGANIZATION_ID
  FROM APPS.HR_LOCATIONS_ALL;
/