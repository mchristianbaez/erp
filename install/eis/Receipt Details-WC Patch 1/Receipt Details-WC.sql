--Report Name            : Receipt Details-WC
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View EIS_XXWC_PO_RCV_DETAILS_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_PO_RCV_DETAILS_V
Create or replace View XXEIS.EIS_XXWC_PO_RCV_DETAILS_V
 AS 
SELECT 
  rct.transaction_id tx_id,
  HRU.NAME ORG,
  OOD.ORGANIZATION_CODE ORGANIZATION_CODE,
  --PLC1.DISPLAYED_FIELD SRC_TYPE,
  flv1.meaning src_type,
  pov.vendor_name src,
  msi.concatenated_segments item,
  mca.concatenated_segments CATEGORY,
  rsl.item_revision rev,
  rsl.item_description des,
  rsh.receipt_num receipt_num,
  poh.segment1
  || DECODE (por.release_num, NULL, NULL, '-'
  || por.release_num)
  || DECODE (pol.line_num, NULL, NULL, '-'
  || pol.line_num)
  || DECODE (pll.shipment_num, NULL, NULL, '-'
  || pll.shipment_num) doc_num,
  RCT.TRANSACTION_DATE TX_DATE,
  --plc2.displayed_field tx_type,
  flv2.meaning tx_type,
  rct.unit_of_measure uom
  /* Transaction unit of measure */
  ,
  ROUND (rct.quantity, xxeis.eis_rs_po_fin_com_util_pkg.get_qty_precision ) qty
  /* Transactionn quantity */
  ,
  pll.price_override price,
  PDT.TYPE_NAME DOC_TYPE,
  --P2.FULL_NAME BUYER_PREPARER,
  xxeis.eis_rs_xxwc_com_util_pkg.get_person_name(poh.agent_id,poh.creation_date) BUYER_PREPARER,
  --p1.full_name receiver,
  xxeis.eis_rs_xxwc_com_util_pkg.get_person_name(rct.employee_id,rct.creation_date) receiver,
  rsh.packing_slip pcking_slip,
  rct.creation_date creation_date,
  rct.inspection_quality_code q_code,
  DECODE (rct.receipt_exception_flag, 'Y', 'Yes', 'N', 'No', NULL) EXCEPTION,
  --PLC3.DISPLAYED_FIELD PAR_TX_TYPE,
  FLV3.meaning PAR_TX_TYPE,
  --PLC.DISPLAYED_FIELD DESTINATION_TYPE,
  flv.meaning DESTINATION_TYPE,
  --P3.FULL_NAME DELIVER_TO_PERSON,
  xxeis.eis_rs_xxwc_com_util_pkg.get_person_name(rct.deliver_to_person_id,rct.creation_date) DELIVER_TO_PERSON,
  --  hrl.location_code deliver_to_location,
  --TRIM((SUBSTR(hrl.location_code,1,INSTR(hrl.location_code,'-',1,1)-1))) deliver_to_location,
  XHR.LOCATION_CODE  deliver_to_location,
  rct.subinventory destination_subinventory,
  msl.concatenated_segments LOCATOR,
  mtr.reason_name tx_reason,
  rsl.item_id ls_item_id,
  rct.transaction_id ls_transaction_id,
  rct.shipment_line_id ls_shipment_line_id,
  rct.organization_id ls_organization_id,
  rsh.shipment_num rcv_shipment_num,
  rsl.quantity_shipped rcv_sl_quantity_shipped,
  rsl.quantity_received rcv_sl_quantity_received,
  rsl.unit_of_measure rcv_sl_unit_of_measure,
  COMP.COMPANY COMPANY ,
  null projetc_number,
 null project_name,
  null project_id,
  msi.unit_weight,
  xxeis.eis_rs_xxwc_com_util_pkg.GET_ONHAND_INV(msi.inventory_item_id,msi.organization_id) on_hand_qty
  --descr#flexfield#start
  --descr#flexfield#end
  --gl#accountff#start
  --gl#accountff#end
FROM
  --po_lookup_codes plc,
  --po_lookup_codes plc1,
  --po_lookup_codes plc2,
  --PO_LOOKUP_CODES PLC3,
  FND_LOOKUP_VALUES FLV,
  FND_LOOKUP_VALUES FLV1,
  FND_LOOKUP_VALUES FLV2,
  FND_LOOKUP_VALUES FLV3,
  po_releases por,
  po_document_types pdt,
  PO_DISTRIBUTIONS POD,
  --per_people_f p2,
  --per_people_f p3,
  hr_locations hrl,
  mtl_transaction_reasons mtr,
  po_vendors pov,
  hr_organization_units hru,
  mtl_system_items_kfv msi,
  mtl_categories_kfv mca,
  mtl_item_locations_kfv msl,
  po_headers poh,
  po_lines pol,
  po_line_locations pll,
  rcv_transactions par,
  rcv_shipment_lines rsl,
  RCV_SHIPMENT_HEADERS RSH,
  --per_people_f p1,
  rcv_transactions rct,
  --pa_projects pp,
  (SELECT gsb.NAME company,
    fsp.inventory_organization_id organization_id,
    gsb.currency_code gl_currency
  FROM gl_sets_of_books gsb,
    financials_system_parameters fsp,
    fnd_currencies fc,
    mtl_default_sets_view mdv
  WHERE gsb.set_of_books_id  = fsp.set_of_books_id
  AND MDV.FUNCTIONAL_AREA_ID = 2
  AND FC.CURRENCY_CODE       = GSB.CURRENCY_CODE
  ) COMP,
  ORG_ORGANIZATION_DEFINITIONS OOD,
  XXEIS.XXWC_HR_LOCATIONS_V XHR
WHERE msi.inventory_item_id(+)                      = rsl.item_id
AND NVL (msi.organization_id, comp.organization_id) = comp.organization_id
AND mca.category_id                                 = rsl.category_id
AND msl.inventory_location_id(+)                    = rct.locator_id
AND NVL (msl.organization_id, rct.organization_id)  = rct.organization_id
AND rct.source_document_code                        = 'PO'
AND rsh.receipt_source_code
  || ''                      = 'VENDOR'
AND rct.shipment_line_id     = rsl.shipment_line_id
AND rct.shipment_header_id   = rsh.shipment_header_id
AND rct.po_header_id         = poh.po_header_id
AND rct.po_line_id           = pol.po_line_id
AND rct.po_line_location_id  = pll.line_location_id
AND rct.po_distribution_id   = pod.po_distribution_id(+)
AND rct.po_release_id        = por.po_release_id(+)
AND ( ( poh.type_lookup_code = 'STANDARD'
AND pdt.document_type_code   = 'PO'
AND pdt.document_subtype     = 'STANDARD' )
OR ( pdt.document_type_code  = 'RELEASE'
AND pdt.document_subtype     = NVL (por.release_type, '~') ) )
  -- AND plc1.LOOKUP_CODE = RSH.RECEIPT_SOURCE_CODE || ''
  -- AND plc1.lookup_type = 'SHIPMENT SOURCE TYPE'
AND FLV1.LOOKUP_CODE = RSH.RECEIPT_SOURCE_CODE
  || ''
AND flv1.lookup_type      = 'SHIPMENT SOURCE TYPE'
AND rsh.vendor_id         = pov.vendor_id
AND par.transaction_id(+) = rct.parent_transaction_id
AND HRL.LOCATION_ID(+)    = RCT.DELIVER_TO_LOCATION_ID
  /* AND (    p3.person_id(+) = rct.deliver_to_person_id
  AND TRUNC (rct.creation_date) BETWEEN NVL (p3.effective_start_date,
  TRUNC (rct.creation_date)
  )
  AND NVL (p3.effective_end_date,
  TRUNC (rct.creation_date)
  )
  )*/
  --AND plc.lookup_code = rct.destination_type_code
  --AND plc.lookup_type = 'RCV DESTINATION TYPE'
AND flv.LOOKUP_CODE = RCT.DESTINATION_TYPE_CODE
AND FLV.LOOKUP_TYPE = 'RCV DESTINATION TYPE'
  --AND plc2.lookup_type = 'RCV TRANSACTION TYPE'
  --AND plc2.lookup_code = rct.transaction_type
AND flv2.lookup_type = 'RCV TRANSACTION TYPE'
AND FLV2.LOOKUP_CODE = RCT.TRANSACTION_TYPE
  --AND plc3.lookup_type(+) = 'RCV TRANSACTION TYPE'
  --AND PLC3.LOOKUP_CODE(+) = PAR.TRANSACTION_TYPE
AND FLV3.lookup_type(+)   = 'RCV TRANSACTION TYPE'
AND FLV3.lookup_code(+)   = par.transaction_type
AND hru.organization_id   = rct.organization_id
AND mtr.reason_id(+)      = rct.reason_id
--AND POD.PROJECT_ID        =PP.PROJECT_ID(+)
and OOD.ORGANIZATION_ID(+)=HRU.ORGANIZATION_ID
AND XHR.LOCATION_ID=HRL.LOCATION_ID
AND FLV2.MEANING='Receive'
--AND XHR.LOCATION_CODE='710'
UNION
SELECT 
  rct.transaction_id tx_id,
  HRU1.NAME ORG,
  OOD.ORGANIZATION_CODE ORGANIZATION_CODE,
  --plc1.displayed_field src_type,
  flv1.meaning src_type,
  hru.NAME
  || prl.source_subinventory src,
  msi.concatenated_segments item,
  mca.concatenated_segments CATEGORY,
  rsl.item_revision rev,
  rsl.item_description des,
  rsh.receipt_num receipt_num,
  prh.segment1 doc_num,
  RCT.TRANSACTION_DATE TX_DATE,
  --plc2.displayed_field tx_type,
  flv2.meaning tx_type,
  rct.unit_of_measure uom
  /* Transaction unit of measure */
  ,
  ROUND (rct.quantity, xxeis.eis_rs_po_fin_com_util_pkg.get_qty_precision ) qty
  /* Transaction quantity */
  ,
  prl.unit_price price,
  PDT.TYPE_NAME DOC_TYPE,
  --P2.FULL_NAME BUYER_PREPARER,
  xxeis.eis_rs_xxwc_com_util_pkg.get_person_name(prh.preparer_id,prh.last_update_date) BUYER_PREPARER,
  --p1.full_name receiver,
  xxeis.eis_rs_xxwc_com_util_pkg.get_person_name(rct.employee_id,rct.creation_date) receiver,
  rsh.packing_slip pcking_slip,
  rct.creation_date creation_date,
  rct.inspection_quality_code q_code,
  DECODE (rct.receipt_exception_flag, 'Y', 'Yes', 'N', 'No', NULL) EXCEPTION,
  --PLC3.DISPLAYED_FIELD PAR_TX_TYPE,
  flv3.meaning PAR_TX_TYPE,
  --plc.displayed_field destination_type,
  FLV.MEANING DESTINATION_TYPE,
  --P3.FULL_NAME DELIVER_TO_PERSON,
  xxeis.eis_rs_xxwc_com_util_pkg.get_person_name(prl.to_person_id,prl.last_update_date) DELIVER_TO_PERSON,
  --  hrl.location_code deliver_to_location,
  --TRIM((SUBSTR(HRL.LOCATION_CODE,1,INSTR(HRL.LOCATION_CODE,'-',1,1)-1))) DELIVER_TO_LOCATION,
  XHR.LOCATION_CODE  deliver_to_location,
  rct.subinventory destination_subinventory,
  msl.concatenated_segments LOCATOR,
  mtr.reason_name tx_reason,
  rsl.item_id ls_item_id,
  rct.transaction_id ls_transaction_id,
  rct.shipment_line_id ls_shipment_line_id,
  rct.organization_id ls_organization_id,
  rsh.shipment_num rcv_shipment_num,
  rsl.quantity_shipped rcv_sl_quantity_shipped,
  rsl.quantity_received rcv_sl_quantity_received,
  rsl.unit_of_measure rcv_sl_unit_of_measure,
  comp.company company ,
  NULL projetc_number,
  NULL project_name,
  NULL project_id,
  msi.unit_weight,
  xxeis.eis_rs_xxwc_com_util_pkg.GET_ONHAND_INV(msi.inventory_item_id,msi.organization_id) on_hand_qty
  --descr#flexfield#start
  --descr#flexfield#end
  --gl#accountff#start
  --gl#accountff#end
FROM --po_lookup_codes plc,
  --po_lookup_codes plc1,
  --po_lookup_codes plc2,
  --po_lookup_codes plc3,
  FND_LOOKUP_VALUES FLV,
  FND_LOOKUP_VALUES FLV1,
  FND_LOOKUP_VALUES FLV2,
  FND_LOOKUP_VALUES FLV3,
  po_document_types pdt,
  --per_people_f p2,
  --per_people_f p3,
  mtl_transaction_reasons mtr,
  hr_organization_units hru,
  hr_locations hrl,
  hr_organization_units hru1,
  mtl_system_items_kfv msi,
  mtl_categories_kfv mca,
  mtl_item_locations_kfv msl,
  po_requisition_headers prh,
  po_requisition_lines prl,
  rcv_transactions par,
  rcv_shipment_lines rsl,
  RCV_SHIPMENT_HEADERS RSH,
  --per_people_f p1,
  rcv_transactions rct,
  (SELECT gsb.NAME company,
    fsp.inventory_organization_id organization_id,
    gsb.currency_code gl_currency
  FROM gl_sets_of_books gsb,
    financials_system_parameters fsp,
    fnd_currencies fc,
    mtl_default_sets_view mdv
  WHERE gsb.set_of_books_id  = fsp.set_of_books_id
  AND MDV.FUNCTIONAL_AREA_ID = 2
  AND FC.CURRENCY_CODE       = GSB.CURRENCY_CODE
  ) COMP,
  ORG_ORGANIZATION_DEFINITIONS OOD,
   XXEIS.XXWC_HR_LOCATIONS_V XHR
WHERE msi.inventory_item_id(+)                      = rsl.item_id
AND NVL (msi.organization_id, comp.organization_id) = comp.organization_id
AND mca.category_id                                 = rsl.category_id
AND msl.inventory_location_id(+)                    = rct.locator_id
AND NVL (msl.organization_id, rct.organization_id)  = rct.organization_id
AND rct.shipment_line_id                            = rsl.shipment_line_id
AND rct.shipment_header_id                          = rsh.shipment_header_id
AND rct.requisition_line_id                         = prl.requisition_line_id
AND prl.requisition_header_id                       = prh.requisition_header_id
AND rsh.organization_id                             = hru.organization_id
AND pdt.document_subtype                            = prh.type_lookup_code
AND pdt.document_type_code                          = 'REQUISITION'
AND rct.source_document_code                        = 'REQ'
AND RSH.RECEIPT_SOURCE_CODE
  || '' = 'INTERNAL ORDER'
  /*AND (    rct.employee_id = p1.person_id(+)
  /* AND TRUNC (rct.creation_date) BETWEEN NVL (p1.effective_start_date,
  TRUNC (rct.creation_date)
  )
  AND NVL (p1.effective_end_date,
  TRUNC (rct.creation_date)
  )
  )*/
  /* AND (    prh.preparer_id = p2.person_id
  AND TRUNC (prh.last_update_date)
  BETWEEN NVL (p2.effective_start_date,
  TRUNC (prh.last_update_date)
  )
  AND NVL (p2.effective_end_date,
  TRUNC (prh.last_update_date)
  )
  )*/
  --AND plc1.lookup_code = rsh.receipt_source_code || ''
  --AND plc1.lookup_type = 'SHIPMENT SOURCE TYPE'
AND FLV1.LOOKUP_CODE = RSH.RECEIPT_SOURCE_CODE
  || ''
AND FLV1.LOOKUP_TYPE      = 'SHIPMENT SOURCE TYPE'
AND PAR.TRANSACTION_ID(+) = RCT.PARENT_TRANSACTION_ID
  --AND plc.lookup_code = prl.destination_type_code
  --AND plc.lookup_type = 'RCV DESTINATION TYPE'
AND FLV.LOOKUP_CODE = PRL.DESTINATION_TYPE_CODE
AND FLV.LOOKUP_TYPE = 'RCV DESTINATION TYPE'
  /*AND (    p3.person_id = prl.to_person_id
  AND TRUNC (prl.last_update_date)
  BETWEEN NVL (p3.effective_start_date,
  TRUNC (prl.last_update_date)
  )
  AND NVL (p3.effective_end_date,
  TRUNC (prl.last_update_date)
  )
  )*/
AND hrl.location_id(+) = prl.deliver_to_location_id
  --AND plc2.lookup_type = 'RCV TRANSACTION TYPE'
  --AND plc2.lookup_code = rct.transaction_type
AND FLV2.LOOKUP_TYPE = 'RCV TRANSACTION TYPE'
AND flv2.lookup_code = rct.transaction_type
  --AND plc3.lookup_type(+) = 'RCV TRANSACTION TYPE'
  --AND plc3.lookup_code(+) = par.transaction_type
AND FLV3.LOOKUP_TYPE(+)   = 'RCV TRANSACTION TYPE'
AND flv3.lookup_code(+)   = par.transaction_type
AND hru1.organization_id  = rct.organization_id
AND MTR.REASON_ID(+)      = RCT.REASON_ID
and OOD.ORGANIZATION_ID(+)=HRU.ORGANIZATION_ID
AND XHR.LOCATION_ID=HRL.LOCATION_ID
AND FLV2.MEANING='Receive'
--AND XHR.LOCATION_CODE='710'
UNION
SELECT 
  rct.transaction_id tx_id,
  HRU1.NAME ORG,
  --plc1.displayed_field src_type,
  flv1.meaning src_type,
  HRU.NAME SRC,
  OOD.ORGANIZATION_CODE ORGANIZATION_CODE,
  msi.concatenated_segments item,
  mca.concatenated_segments CATEGORY,
  rsl.item_revision rev,
  rsl.item_description des,
  rsh.receipt_num receipt_num,
  rsh.shipment_num doc_num,
  RCT.TRANSACTION_DATE TX_DATE,
  --plc2.displayed_field /* USE DISPLAY FIELD*/ tx_type,
  flv2.meaning tx_type,
  rct.unit_of_measure uom,
  ROUND (rct.quantity, xxeis.eis_rs_po_fin_com_util_pkg.get_qty_precision ) qty,
  RSL.SHIPMENT_UNIT_PRICE PRICE,
  --plc4.displayed_field doc_type,
  flv4.meaning doc_type,
  '' BUYER_PREPARER,
  --p.full_name receiver,
  xxeis.eis_rs_xxwc_com_util_pkg.get_person_name(rct.employee_id,rct.creation_date) receiver,
  rsh.packing_slip pcking_slip,
  rct.creation_date creation_date,
  rct.inspection_quality_code q_code,
  DECODE (rct.receipt_exception_flag, 'Y', 'Yes', 'N', 'No', NULL) EXCEPTION,
  --plc3.displayed_field par_tx_type,
  flv3.meaning par_tx_type,
  --plc.displayed_field destination_type,
  FLV.meaning DESTINATION_TYPE,
  --P1.FULL_NAME DELIVER_TO_PERSON,
  xxeis.eis_rs_xxwc_com_util_pkg.get_person_name(rct.deliver_to_person_id,rct.creation_date) DELIVER_TO_PERSON,
  -- hrl.location_code deliver_to_location,
  --TRIM((SUBSTR(HRL.LOCATION_CODE,1,INSTR(HRL.LOCATION_CODE,'-',1,1)-1))) DELIVER_TO_LOCATION,
  XHR.LOCATION_CODE  deliver_to_location,
  rct.subinventory destination_subinventory,
  msl.concatenated_segments LOCATOR,
  mtr.reason_name tx_reason,
  rsl.item_id ls_item_id,
  rct.transaction_id ls_transaction_id,
  rct.shipment_line_id ls_shipment_line_id,
  rct.organization_id ls_organization_id,
  rsh.shipment_num rcv_shipment_num,
  rsl.quantity_shipped rcv_sl_quantity_shipped,
  rsl.quantity_received rcv_sl_quantity_received,
  rsl.unit_of_measure rcv_sl_unit_of_measure,
  comp.company company ,
  NULL projetc_number,
  NULL project_name,
  NULL project_id,
  msi.unit_weight,
  xxeis.eis_rs_xxwc_com_util_pkg.GET_ONHAND_INV(msi.inventory_item_id,msi.organization_id) on_hand_qty
  --descr#flexfield#start
  --descr#flexfield#end
  --gl#accountff#start
  --gl#accountff#end
FROM --po_lookup_codes plc,
  --po_lookup_codes plc1,
  --po_lookup_codes plc2,
  --po_lookup_codes plc3,
  --po_lookup_codes plc4,
  FND_LOOKUP_VALUES FLV,
  FND_LOOKUP_VALUES FLV1,
  FND_LOOKUP_VALUES FLV2,
  FND_LOOKUP_VALUES FLV3,
  FND_LOOKUP_VALUES FLV4,
  --per_people_f p1,
  hr_organization_units hru,
  hr_locations hrl,
  mtl_transaction_reasons mtr,
  hr_organization_units hru1,
  mtl_system_items_kfv msi,
  mtl_categories_kfv mca,
  mtl_item_locations_kfv msl,
  rcv_transactions par,
  rcv_shipment_lines rsl,
  RCV_SHIPMENT_HEADERS RSH,
  --per_people_f p,
  rcv_transactions rct,
  (SELECT gsb.NAME company,
    fsp.inventory_organization_id organization_id,
    gsb.currency_code gl_currency
  FROM gl_sets_of_books gsb,
    financials_system_parameters fsp,
    fnd_currencies fc,
    mtl_default_sets_view mdv
  WHERE gsb.set_of_books_id  = fsp.set_of_books_id
  AND MDV.FUNCTIONAL_AREA_ID = 2
  AND FC.CURRENCY_CODE       = GSB.CURRENCY_CODE
  ) COMP,
  ORG_ORGANIZATION_DEFINITIONS OOD,
   XXEIS.XXWC_HR_LOCATIONS_V XHR
WHERE msi.inventory_item_id(+)                      = rsl.item_id
AND NVL (msi.organization_id, comp.organization_id) = comp.organization_id
AND mca.category_id                                 = rsl.category_id
AND msl.inventory_location_id(+)                    = rct.locator_id
AND NVL (msl.organization_id, rct.organization_id)  = rct.organization_id
AND rct.shipment_line_id                            = rsl.shipment_line_id
AND rct.shipment_header_id                          = rsh.shipment_header_id
AND rsh.receipt_source_code
  || '' = 'INVENTORY'
  --AND plc4.lookup_code = rct.source_document_code
  --AND plc4.lookup_type = 'SHIPMENT SOURCE DOCUMENT TYPE'
AND FLV4.LOOKUP_CODE = RCT.SOURCE_DOCUMENT_CODE
AND FLV4.LOOKUP_TYPE = 'SHIPMENT SOURCE DOCUMENT TYPE'
  --AND plc1.lookup_code = rsh.receipt_source_code || ''
  --AND plc1.lookup_type = 'SHIPMENT SOURCE TYPE'
AND flv1.lookup_code = rsh.receipt_source_code
  || ''
AND FLV1.LOOKUP_TYPE = 'SHIPMENT SOURCE TYPE'
  /*AND (    rct.employee_id = p.person_id(+)
  /* AND TRUNC (rct.creation_date) BETWEEN NVL (p.effective_start_date,
  TRUNC (rct.creation_date)
  )
  AND NVL (p.effective_end_date,
  TRUNC (rct.creation_date)
  )
  )*/
AND rsh.organization_id   = hru.organization_id
AND par.transaction_id(+) = rct.parent_transaction_id
AND HRL.LOCATION_ID(+)    = RCT.DELIVER_TO_LOCATION_ID
  /*AND (    p1.person_id(+) = rct.deliver_to_person_id
  /*AND TRUNC (rct.creation_date) BETWEEN NVL (p1.effective_start_date,
  TRUNC (rct.creation_date)
  )
  AND NVL (p1.effective_end_date,
  TRUNC (rct.creation_date)
  )
  )*/
  --AND plc.lookup_code = rsl.destination_type_code
  --AND plc.lookup_type = 'RCV DESTINATION TYPE'
AND flv.lookup_code = rsl.destination_type_code
AND flv.lookup_type = 'RCV DESTINATION TYPE'
  --AND plc2.lookup_type = 'RCV TRANSACTION TYPE'
  --AND plc2.lookup_code = rct.transaction_type
AND flv2.lookup_type = 'RCV TRANSACTION TYPE'
AND flv2.lookup_code = rct.transaction_type
  --AND plc3.lookup_type(+) = 'RCV TRANSACTION TYPE'
  --AND plc3.lookup_code(+) = par.transaction_type
AND flv3.lookup_type(+)  = 'RCV TRANSACTION TYPE'
AND flv3.lookup_code(+)  = par.transaction_type
AND hru1.organization_id = rct.organization_id
AND MTR.REASON_ID(+)     = RCT.REASON_ID
and OOD.ORGANIZATION_ID(+)=HRU.ORGANIZATION_ID
AND XHR.LOCATION_ID=HRL.LOCATION_ID
AND FLV2.MEANING='Receive'
--AND XHR.LOCATION_CODE='710'
UNION
SELECT 
  rct.transaction_id tx_id,
  HRU.NAME ORG,
  OOD.ORGANIZATION_CODE ORGANIZATION_CODE,
  --plc1.displayed_field src_type,
  FLV1.MEANING SRC_TYPE,
  --DECODE(RCT.SOURCE_DOCUMENT_CODE, 'PO', POV.VENDOR_NAME, OEV.NAME ) SRC,
  pov.vendor_name src,
  --&p_flex_item c_flex_item,
  --&p_flex_cat c_flex_cat,
  MSI.CONCATENATED_SEGMENTS ITEM,
  MCA.CONCATENATED_SEGMENTS CATEGORY,  
  rsl.item_revision rev,
  rsl.item_description des,
  rsh.receipt_num receipt_num,
  '' DOC_NUM,
  --TO_CHAR(RCT.TRANSACTION_DATE, 'DD/MON/YYYY HH24:MI:SS') TX_DATE,
  RCT.TRANSACTION_DATE TX_DATE,
  --plc2.displayed_field tx_type,
  flv2.meaning tx_type,
  rct.unit_of_measure uom,
  /* Transaction unit of measure */
  ROUND (rct.quantity, xxeis.eis_rs_po_fin_com_util_pkg.get_qty_precision ) qty  ,
  /* Transaction quantity */
  TO_NUMBER('') price,
  '' doc_type,
  '' BUYER_PREPARER,
  --p.full_name receiver,
  xxeis.eis_rs_xxwc_com_util_pkg.get_person_name(rct.employee_id,rct.creation_date) receiver,
  rsh.packing_slip pcking_slip,
  rct.creation_date creation_date,
  RCT.INSPECTION_QUALITY_CODE Q_CODE,
  --DECODE(rct.receipt_exception_flag, 'Y', :yes, 'N', :NO, NULL ) EXCEPTION,
  DECODE (RCT.RECEIPT_EXCEPTION_FLAG, 'Y', 'Yes', 'N', 'No', NULL) EXCEPTION,
  --plc3.displayed_field par_tx_type,
  --PLC.DISPLAYED_FIELD DESTINATION_TYPE,
  FLV3.MEANING PAR_TX_TYPE,
  FLV.MEANING DESTINATION_TYPE,  
  --p2.full_name deliver_to_person,
  XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_PERSON_NAME(RCT.DELIVER_TO_PERSON_ID,RCT.CREATION_DATE) DELIVER_TO_PERSON,
  --lot.location_code deliver_to_location,
  --TRIM((SUBSTR(lot.location_code,1,INSTR(lot.location_code,'-',1,1)-1))) deliver_to_location,
  XHR.LOCATION_CODE  deliver_to_location,
  '' DESTINATION_SUBINVENTORY,
  --&p_flex_locator c_flex_locator,
  msl.concatenated_segments LOCATOR,
  mtr.reason_name tx_reason,
  rsl.item_id ls_item_id,
  rct.transaction_id ls_transaction_id,
  rct.shipment_line_id ls_shipment_line_id,
  RCT.ORGANIZATION_ID LS_ORGANIZATION_ID,
  rsh.shipment_num rcv_shipment_num,
  rsl.quantity_shipped rcv_sl_quantity_shipped,
  rsl.quantity_received rcv_sl_quantity_received,
  rsl.unit_of_measure rcv_sl_unit_of_measure,
  comp.company company ,
  NULL projetc_number,
  NULL project_name,
  NULL project_id,
  MSI.UNIT_WEIGHT,
  xxeis.eis_rs_xxwc_com_util_pkg.GET_ONHAND_INV(msi.inventory_item_id,msi.organization_id) on_hand_qty  
FROM --po_lookup_codes plc,
  --po_lookup_codes plc1,
  --po_lookup_codes plc2,
  --PO_LOOKUP_CODES PLC3,
  FND_LOOKUP_VALUES FLV,
  FND_LOOKUP_VALUES FLV1,
  FND_LOOKUP_VALUES FLV2,
  FND_LOOKUP_VALUES FLV3,  
  --per_people_f p2,
  HR_LOCATIONS lot,
  mtl_transaction_reasons mtr,
  po_vendors pov,
  hr_organization_units hru,
  mtl_system_items_kfv msi,
  mtl_categories_kfv mca,
  mtl_item_locations_kfv msl,
  rcv_transactions par,
  rcv_shipment_lines rsl,
  RCV_SHIPMENT_HEADERS RSH,
  --per_people_f p,
  RCV_TRANSACTIONS RCT,
  --oe_sold_to_orgs_v oev,
  --Added
  (SELECT gsb.NAME company,
    fsp.inventory_organization_id organization_id,
    gsb.currency_code gl_currency
  FROM gl_sets_of_books gsb,
    financials_system_parameters fsp,
    fnd_currencies fc,
    mtl_default_sets_view mdv
  WHERE gsb.set_of_books_id  = fsp.set_of_books_id
  AND MDV.FUNCTIONAL_AREA_ID = 2
  AND FC.CURRENCY_CODE       = GSB.CURRENCY_CODE
  ) COMP,
  ORG_ORGANIZATION_DEFINITIONS OOD,
  XXEIS.XXWC_HR_LOCATIONS_V XHR
WHERE MSI.INVENTORY_ITEM_ID(+)   = RSL.ITEM_ID
--AND msi.organization_id(+)       = :organization_id
AND NVL(msi.organization_id,comp.organization_id)= comp.organization_id
AND mca.category_id              = rsl.category_id
AND msl.inventory_location_id(+) = rct.locator_id
AND msl.organization_id(+)       = rct.organization_id
AND rct.source_document_code    IN('PO', 'RMA')
AND rsh.receipt_source_code     IN('VENDOR', 'CUSTOMER')
AND rct.shipment_line_id         = rsl.shipment_line_id
AND rct.shipment_header_id       = rsh.shipment_header_id
--AND ( rct.employee_id            = p.person_id(+)
--AND TRUNC(sysdate) BETWEEN NVL(p.effective_start_date, TRUNC(sysdate)) AND NVL(p.effective_end_date, TRUNC(sysdate))) --bug#6921262
--AND plc1.lookup_code      = rsh.receipt_source_code
--AND plc1.lookup_type      = 'SHIPMENT SOURCE TYPE'
AND flv1.lookup_code        = rsh.receipt_source_code
  || ''
AND flv1.lookup_type      = 'SHIPMENT SOURCE TYPE'
AND rsh.vendor_id         = pov.vendor_id(+)
AND par.transaction_id(+) = rct.parent_transaction_id
and LOT.LOCATION_ID(+)    = RCT.DELIVER_TO_LOCATION_ID
--AND lot.LANGUAGE(+)       = USERENV('LANG')
--AND ( p2.person_id(+)     = rct.deliver_to_person_id
--AND TRUNC(sysdate) BETWEEN NVL(p2.effective_start_date, TRUNC(sysdate)) AND NVL(p2.effective_end_date, TRUNC(sysdate))) --bug#6921262
--AND plc.lookup_code     = rct.destination_type_code
--AND plc.lookup_type     = 'RCV DESTINATION TYPE'
--AND plc2.lookup_type    = 'RCV TRANSACTION TYPE'
--AND plc2.lookup_code    = rct.transaction_type
--AND plc3.lookup_type(+) = 'RCV TRANSACTION TYPE'
--AND plc3.lookup_code(+) = par.transaction_type
AND FLV.LOOKUP_CODE = RCT.DESTINATION_TYPE_CODE
AND FLV.LOOKUP_TYPE  = 'RCV DESTINATION TYPE'
AND flv2.lookup_type = 'RCV TRANSACTION TYPE'
AND FLV2.LOOKUP_CODE = RCT.TRANSACTION_TYPE
AND flv3.lookup_type(+)  = 'RCV TRANSACTION TYPE'
AND FLV3.LOOKUP_CODE(+)  = PAR.TRANSACTION_TYPE
AND hru.organization_id = rct.organization_id
AND MTR.REASON_ID(+) = RCT.REASON_ID
AND RCT.TRANSACTION_TYPE = 'UNORDERED'
AND OOD.ORGANIZATION_ID(+)=HRU.ORGANIZATION_ID
/*AND NVL(p.business_group_id, 0) =
  (SELECT NVL(MAX(fsp.business_group_id), 0)
  FROM financials_system_parameters fsp
  )
AND TRUNC(SYSDATE) BETWEEN NVL(P.EFFECTIVE_START_DATE, TRUNC(SYSDATE)) AND NVL(P.EFFECTIVE_END_DATE, TRUNC(SYSDATE))
AND ( (NVL(p2.business_group_id, 0) =
  (SELECT NVL(MAX(fsp.business_group_id), 0)
  FROM financials_system_parameters fsp
  ))
OR NVL(rct.deliver_to_person_id, 0) = 0)
AND TRUNC(SYSDATE) BETWEEN NVL(P2.EFFECTIVE_START_DATE, TRUNC(SYSDATE)) AND NVL(P2.EFFECTIVE_END_DATE, TRUNC(SYSDATE))*/
and FLV2.MEANING='Receive'
AND XHR.LOCATION_ID=LOT.LOCATION_ID
--AND oev.customer_id(+)         = rsh.customer_id
UNION
SELECT 
  rct.transaction_id TX_ID ,
  HRU.NAME ORG ,
  OOD.ORGANIZATION_CODE ORGANIZATION_CODE,
  --plc1.displayed_field SRC_TYPE ,
  FLV1.MEANING SRC_TYPE,
  --OEV.NAME SRC ,
  null src,
  --&P_FLEX_ITEM C_FLEX_ITEM ,
  --&P_FLEX_CAT C_FLEX_CAT ,
  MSI.CONCATENATED_SEGMENTS ITEM,
  MCA.CONCATENATED_SEGMENTS CATEGORY,  
  rsl.item_revision REV ,
  rsl.item_description DES ,
  rsh.receipt_num receipt_num ,
  TO_CHAR(oeh.order_number)
  || '-'
  || TO_CHAR(oel.line_number) doc_num ,
--  TO_CHAR(RCT.TRANSACTION_DATE, 'DD/MON/YYYY HH24:MI:SS') TX_DATE ,
  RCT.TRANSACTION_DATE TX_DATE,
  --plc2.displayed_field tx_type ,
  flv2.meaning tx_type,
  RCT.UNIT_OF_MEASURE UOM
  /* Transaction unit of measure */
  ,
  --NULL PO_UOM ,
  RCT.QUANTITY QTY
  /* Transactionn quantity */
  ,
  rsl.shipment_unit_price PRICE ,
  'RMA' DOC_TYPE ,
  TO_CHAR(NULL) BUYER_PREPARER ,
  --P1.FULL_NAME RECEIVER ,
  xxeis.eis_rs_xxwc_com_util_pkg.get_person_name(rct.employee_id,rct.creation_date) receiver,
  RSH.PACKING_SLIP PCKING_SLIP ,
  RCT.CREATION_DATE CREATION_DATE ,
  RCT.INSPECTION_QUALITY_CODE Q_CODE ,
  --DECODE(RCT.RECEIPT_EXCEPTION_FLAG,'Y',:yes,'N',:no,NULL) EXCEPTION ,
  DECODE (RCT.RECEIPT_EXCEPTION_FLAG, 'Y', 'Yes', 'N', 'No', NULL) EXCEPTION,
  --PLC3.DISPLAYED_FIELD PAR_TX_TYPE ,
  --PLC.DISPLAYED_FIELD DESTINATION_TYPE ,
  FLV3.MEANING PAR_TX_TYPE,
  FLV.MEANING DESTINATION_TYPE,  
  --P3.FULL_NAME DELIVER_TO_PERSON ,
  --LOT.LOCATION_CODE DELIVER_TO_LOCATION ,
  XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_PERSON_NAME(RCT.DELIVER_TO_PERSON_ID,RCT.CREATION_DATE) DELIVER_TO_PERSON,
  --TRIM((SUBSTR(LOT.LOCATION_CODE,1,INSTR(LOT.LOCATION_CODE,'-',1,1)-1))) DELIVER_TO_LOCATION,  
  XHR.LOCATION_CODE  deliver_to_location,
  RCT.SUBINVENTORY DESTINATION_SUBINVENTORY ,
  --&P_FLEX_LOCATOR C_FLEX_LOCATOR ,
  msl.concatenated_segments LOCATOR,
  MTR.REASON_NAME TX_REASON ,
  RSL.ITEM_ID LS_ITEM_ID ,
  RCT.TRANSACTION_ID LS_TRANSACTION_ID ,
  RCT.SHIPMENT_LINE_ID LS_SHIPMENT_LINE_ID ,
  RCT.ORGANIZATION_ID LS_ORGANIZATION_ID,
  rsh.shipment_num rcv_shipment_num,
  rsl.quantity_shipped rcv_sl_quantity_shipped,
  rsl.quantity_received rcv_sl_quantity_received,
  RSL.UNIT_OF_MEASURE RCV_SL_UNIT_OF_MEASURE,
  null company ,
  NULL projetc_number,
  NULL project_name,
  NULL project_id,
  MSI.UNIT_WEIGHT,
  xxeis.eis_rs_xxwc_com_util_pkg.GET_ONHAND_INV(msi.inventory_item_id,msi.organization_id) on_hand_qty  
FROM RCV_transactions rct ,
  RCV_transactions par ,
  hr_organization_units hru ,
  RCV_SHIPMENT_HEADERS RSH ,
  --po_lookup_codes plc1 ,
  --po_lookup_codes plc2 ,
  FND_LOOKUP_VALUES FLV1,
  FND_LOOKUP_VALUES FLV2,  
  --oe_sold_to_orgs_v oev ,
  rcv_shipment_lines rsl ,
  mtl_system_items_kfv MSI ,
  MTL_CATEGORIES_KFV MCA ,
  --per_people_f p1 ,
  --per_people_f p3 ,
  --po_lookup_codes plc3 ,
  --PO_LOOKUP_CODES PLC  ,
  FND_LOOKUP_VALUES FLV3,
  FND_LOOKUP_VALUES FLV,  
  HR_LOCATIONS LOT ,
  MTL_TRANSACTION_REASONS MTR ,
  mtl_item_locations_kfv MSL ,
  OE_ORDER_HEADERS OEH ,
  OE_ORDER_LINES OEL,
  ORG_ORGANIZATION_DEFINITIONS OOD,
  XXEIS.XXWC_HR_LOCATIONS_V XHR
WHERE RCT.SOURCE_DOCUMENT_CODE = 'RMA'
AND RSH.RECEIPT_SOURCE_CODE
  || ''                    = 'CUSTOMER'
AND PAR.TRANSACTION_ID (+) = RCT.PARENT_TRANSACTION_ID
AND hru.organization_id    = rct.organization_id
--AND PLC1.LOOKUP_CODE = RSH.RECEIPT_SOURCE_CODE
--  || ''
--AND PLC1.LOOKUP_TYPE                             = 'SHIPMENT SOURCE TYPE'
AND FLV1.LOOKUP_CODE = RSH.RECEIPT_SOURCE_CODE
  || ''
AND FLV1.LOOKUP_TYPE                             = 'SHIPMENT SOURCE TYPE'
--AND PLC.LOOKUP_CODE                              = RCT.DESTINATION_TYPE_CODE
--AND PLC.LOOKUP_TYPE                              = 'RCV DESTINATION TYPE'
AND FLV.LOOKUP_CODE                              = RCT.DESTINATION_TYPE_CODE
and FLV.LOOKUP_TYPE                              = 'RCV DESTINATION TYPE'
--AND oev.customer_id (+)                          = rsh.customer_id
AND RCT.SHIPMENT_LINE_ID                         = RSL.SHIPMENT_LINE_ID
AND RCT.SHIPMENT_HEADER_ID                       = RSH.SHIPMENT_HEADER_ID
AND RSH.shipment_header_id                       = rsl.shipment_header_id
AND MCA.CATEGORY_ID (+)                          = RSL.CATEGORY_ID -- OUTER JOIN ???
AND MSI.INVENTORY_ITEM_ID (+)                    = RSL.ITEM_ID
AND MSL.INVENTORY_LOCATION_ID (+)                = RCT.LOCATOR_ID
AND NVL(MSL.ORGANIZATION_ID,RCT.ORGANIZATION_ID) = RCT.ORGANIZATION_ID
--AND PLC2.LOOKUP_TYPE                             = 'RCV TRANSACTION TYPE'
--AND PLC2.LOOKUP_CODE                             = RCT.TRANSACTION_TYPE
AND FLV2.LOOKUP_TYPE                             = 'RCV TRANSACTION TYPE'
AND FLV2.LOOKUP_CODE                             = RCT.TRANSACTION_TYPE
/*AND (RCT.EMPLOYEE_ID                             = P1.PERSON_ID (+)
AND TRUNC(sysdate) BETWEEN --bug#6921262
  NVL(P1.EFFECTIVE_START_DATE, TRUNC(sysdate)) AND NVL(P1.EFFECTIVE_END_DATE, TRUNC(RCT.CREATION_DATE)))*/
--AND PLC3.LOOKUP_TYPE (+) = 'RCV TRANSACTION TYPE'
--AND PLC3.LOOKUP_CODE (+) = PAR.TRANSACTION_TYPE
AND FLV3.LOOKUP_TYPE (+) = 'RCV TRANSACTION TYPE'
AND FLV3.LOOKUP_CODE (+) = PAR.TRANSACTION_TYPE
/*AND (P3.PERSON_ID(+)     = RCT.DELIVER_TO_PERSON_ID
AND TRUNC(sysdate) BETWEEN --bug#6921262
  NVL(P3.EFFECTIVE_START_DATE, TRUNC(sysdate)) AND NVL(P3.EFFECTIVE_END_DATE, TRUNC(sysdate)))*/
and LOT.LOCATION_ID(+) = RCT.DELIVER_TO_LOCATION_ID
--AND LOT.LANGUAGE(+)    = USERENV('LANG')
AND MTR.REASON_ID(+)   = RCT.REASON_ID
AND OEH.header_id      = OEL.header_id
AND OEL.LINE_ID        = RCT.OE_ORDER_LINE_ID
and OOD.ORGANIZATION_ID(+)=HRU.ORGANIZATION_ID
AND XHR.LOCATION_ID=LOT.LOCATION_ID
and FLV2.MEANING='Receive'
/
set scan on define on
prompt Creating View Data for Receipt Details-WC
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_PO_RCV_DETAILS_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_PO_RCV_DETAILS_V',201,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Xxwc Po Rcv Details V','EXPRDV','','');
--Delete View Columns for EIS_XXWC_PO_RCV_DETAILS_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_PO_RCV_DETAILS_V',201,FALSE);
--Inserting View Columns for EIS_XXWC_PO_RCV_DETAILS_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','COMPANY',201,'Company','COMPANY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Company','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','RCV_SL_UNIT_OF_MEASURE',201,'Rcv Sl Unit Of Measure','RCV_SL_UNIT_OF_MEASURE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Rcv Sl Unit Of Measure','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','RCV_SL_QUANTITY_RECEIVED',201,'Rcv Sl Quantity Received','RCV_SL_QUANTITY_RECEIVED','','','','XXEIS_RS_ADMIN','NUMBER','','','Rcv Sl Quantity Received','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','RCV_SL_QUANTITY_SHIPPED',201,'Rcv Sl Quantity Shipped','RCV_SL_QUANTITY_SHIPPED','','','','XXEIS_RS_ADMIN','NUMBER','','','Rcv Sl Quantity Shipped','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','RCV_SHIPMENT_NUM',201,'Rcv Shipment Num','RCV_SHIPMENT_NUM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Rcv Shipment Num','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','TX_REASON',201,'Tx Reason','TX_REASON','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Tx Reason','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','LOCATOR',201,'Locator','LOCATOR','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Locator','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','DESTINATION_SUBINVENTORY',201,'Destination Subinventory','DESTINATION_SUBINVENTORY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Destination Subinventory','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','DELIVER_TO_LOCATION',201,'Deliver To Location','DELIVER_TO_LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Deliver To Location','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','DELIVER_TO_PERSON',201,'Deliver To Person','DELIVER_TO_PERSON','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Deliver To Person','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','DESTINATION_TYPE',201,'Destination Type','DESTINATION_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Destination Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','PAR_TX_TYPE',201,'Par Tx Type','PAR_TX_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Par Tx Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','EXCEPTION',201,'Exception','EXCEPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Exception','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','Q_CODE',201,'Q Code','Q_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Q Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','RECEIVER',201,'Receiver','RECEIVER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Receiver','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','BUYER_PREPARER',201,'Buyer Preparer','BUYER_PREPARER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Buyer Preparer','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','DOC_TYPE',201,'Doc Type','DOC_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Doc Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','PRICE',201,'Price','PRICE','','','','XXEIS_RS_ADMIN','NUMBER','','','Price','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','QTY',201,'Qty','QTY','','','','XXEIS_RS_ADMIN','NUMBER','','','Qty','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','UOM',201,'Uom','UOM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Uom','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','TX_TYPE',201,'Tx Type','TX_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Tx Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','TX_DATE',201,'Tx Date','TX_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Tx Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','DOC_NUM',201,'Doc Num','DOC_NUM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Doc Num','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','RECEIPT_NUM',201,'Receipt Num','RECEIPT_NUM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Receipt Num','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','DES',201,'Des','DES','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Des','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','CATEGORY',201,'Category','CATEGORY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Category','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','ITEM',201,'Item','ITEM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Item','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','SRC',201,'Src','SRC','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Src','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','SRC_TYPE',201,'Src Type','SRC_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Src Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','ORG',201,'Org','ORG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Org','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','ON_HAND_QTY',201,'On Hand Qty','ON_HAND_QTY','','','','XXEIS_RS_ADMIN','NUMBER','','','On Hand Qty','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','UNIT_WEIGHT',201,'Unit Weight','UNIT_WEIGHT','','','','XXEIS_RS_ADMIN','NUMBER','','','Unit Weight','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','LS_ORGANIZATION_ID',201,'Ls Organization Id','LS_ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Ls Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','LS_SHIPMENT_LINE_ID',201,'Ls Shipment Line Id','LS_SHIPMENT_LINE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Ls Shipment Line Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','LS_TRANSACTION_ID',201,'Ls Transaction Id','LS_TRANSACTION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Ls Transaction Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','LS_ITEM_ID',201,'Ls Item Id','LS_ITEM_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Ls Item Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','CREATION_DATE',201,'Creation Date','CREATION_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Creation Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','PCKING_SLIP',201,'Pcking Slip','PCKING_SLIP','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Pcking Slip','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','REV',201,'Rev','REV','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Rev','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','ORGANIZATION_CODE',201,'Organization Code','ORGANIZATION_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Organization Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','TX_ID',201,'Tx Id','TX_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Tx Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','PROJECT_ID',201,'Project Id','PROJECT_ID','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Project Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','PROJECT_NAME',201,'Project Name','PROJECT_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Project Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','PROJETC_NUMBER',201,'Projetc Number','PROJETC_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Projetc Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','PROCESS_ID',201,'Process Id','PROCESS_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Process Id','','','');
--Inserting View Components for EIS_XXWC_PO_RCV_DETAILS_V
--Inserting View Component Joins for EIS_XXWC_PO_RCV_DETAILS_V
END;
/
set scan on define on
prompt Creating Report LOV Data for Receipt Details-WC
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Receipt Details-WC
xxeis.eis_rs_ins.lov( 201,'select agent_name buyer_name from po_agents_v','','EIS_PO_BUYER_LOV','List of Values for Buyer','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 201,'SELECT DISTINCT rsh.RECEIPT_NUM,HOU.name OPERATING_UNIT FROM RCV_SHIPMENT_HEADERS rsh,ORG_ORGANIZATION_DEFINITIONS OOD,
HR_OPERATING_UNITS HOU
WHERE OOD.OPERATING_UNIT=HOU.ORGANIZATION_ID(+)
AND OOD.ORGANIZATION_ID(+)=RSH.ORGANIZATION_ID
AND EXISTS(SELECT 1 FROM XXEIS.EIS_MO_ORG_TMP_V
            WHERE ORG_ID=HOU.ORGANIZATION_ID)
order by receipt_num','','EIS_PO_RECEIPT_NUM_LOV','List of values for PO Receipt Number(s)','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'select distinct H.FULL_NAME,HOU.name OPERATING_UNIT
from RCV_TRANSACTIONS R, HR_EMPLOYEES H,ORG_ORGANIZATION_DEFINITIONS OOD,
HR_OPERATING_UNITS HOU
where OOD.OPERATING_UNIT=HOU.ORGANIZATION_ID(+)
and OOD.ORGANIZATION_ID(+)=R.ORGANIZATION_ID
and R.EMPLOYEE_ID = H.EMPLOYEE_ID (+)
and exists(select 1 from XXEIS.EIS_MO_ORG_TMP_V
            WHERE ORG_ID=HOU.ORGANIZATION_ID)','','EIS_PO_RECEIVER_LOV','List of values for PO Receiver','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'SELECT lookup_code, meaning FROM apps.mfg_lookups
WHERE LOOKUP_TYPE = ''INV_SRS_PRECISION''
and enabled_flag = ''Y''
order by LOOKUP_CODE','','EIS_PO_NUMBER_PRECISION_LOV','','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 201,'select  distinct TRIM((SUBSTR(LOCATION_CODE,1,INSTR(LOCATION_CODE,''-'',1,1)-1))) deliver_location
 from HR_LOCATIONS','','Delivery Location','','ANONYMOUS',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Receipt Details-WC
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Receipt Details-WC
xxeis.eis_rs_utility.delete_report_rows( 'Receipt Details-WC' );
--Inserting Report - Receipt Details-WC
xxeis.eis_rs_ins.r( 201,'Receipt Details-WC','','This report lists receiving details and summary for a given receipt date Range, supplier range, purchase order range, receipt number range or by receiver. This report can be run for day to day operations by either the purchasing manager, plant managers or other authorized users for planning and execution purposes.','','','','XXEIS_RS_ADMIN','EIS_XXWC_PO_RCV_DETAILS_V','Y','','','XXEIS_RS_ADMIN','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - Receipt Details-WC
xxeis.eis_rs_ins.rc( 'Receipt Details-WC',201,'BUYER_PREPARER','Buyer Preparer','Buyer Preparer','','','default','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_RCV_DETAILS_V','','');
xxeis.eis_rs_ins.rc( 'Receipt Details-WC',201,'CATEGORY','Category','Category','','','default','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_RCV_DETAILS_V','','');
xxeis.eis_rs_ins.rc( 'Receipt Details-WC',201,'COMPANY','Company','Company','','','default','','12','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_RCV_DETAILS_V','','');
xxeis.eis_rs_ins.rc( 'Receipt Details-WC',201,'DELIVER_TO_LOCATION','Deliver To Location','Deliver To Location','','','default','','13','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_RCV_DETAILS_V','','');
xxeis.eis_rs_ins.rc( 'Receipt Details-WC',201,'DELIVER_TO_PERSON','Deliver To Person','Deliver To Person','','','default','','14','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_RCV_DETAILS_V','','');
xxeis.eis_rs_ins.rc( 'Receipt Details-WC',201,'DES','Des','Des','','','default','','15','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_RCV_DETAILS_V','','');
xxeis.eis_rs_ins.rc( 'Receipt Details-WC',201,'DESTINATION_SUBINVENTORY','Destination Subinventory','Destination Subinventory','','','default','','16','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_RCV_DETAILS_V','','');
xxeis.eis_rs_ins.rc( 'Receipt Details-WC',201,'DESTINATION_TYPE','Destination Type','Destination Type','','','default','','17','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_RCV_DETAILS_V','','');
xxeis.eis_rs_ins.rc( 'Receipt Details-WC',201,'DOC_NUM','Po Number/Line','Doc Num','','','default','','18','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_RCV_DETAILS_V','','');
xxeis.eis_rs_ins.rc( 'Receipt Details-WC',201,'DOC_TYPE','Doc Type','Doc Type','','','default','','19','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_RCV_DETAILS_V','','');
xxeis.eis_rs_ins.rc( 'Receipt Details-WC',201,'EXCEPTION','Exception','Exception','','','default','','20','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_RCV_DETAILS_V','','');
xxeis.eis_rs_ins.rc( 'Receipt Details-WC',201,'ITEM','Item','Item','','','default','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_RCV_DETAILS_V','','');
xxeis.eis_rs_ins.rc( 'Receipt Details-WC',201,'LOCATOR','Locator','Locator','','','default','','21','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_RCV_DETAILS_V','','');
xxeis.eis_rs_ins.rc( 'Receipt Details-WC',201,'ON_HAND_QTY','On Hand Qty','On Hand Qty','','~~~','default','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_RCV_DETAILS_V','','');
xxeis.eis_rs_ins.rc( 'Receipt Details-WC',201,'ORG','Org','Org','','','default','','22','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_RCV_DETAILS_V','','');
xxeis.eis_rs_ins.rc( 'Receipt Details-WC',201,'PAR_TX_TYPE','Par Tx Type','Par Tx Type','','','default','','32','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_RCV_DETAILS_V','','');
xxeis.eis_rs_ins.rc( 'Receipt Details-WC',201,'PRICE','Price','Price','','~,~.~2','default','','23','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_RCV_DETAILS_V','','');
xxeis.eis_rs_ins.rc( 'Receipt Details-WC',201,'QTY','Qty','Qty','','~~~','default','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_RCV_DETAILS_V','','');
xxeis.eis_rs_ins.rc( 'Receipt Details-WC',201,'Q_CODE','Q Code','Q Code','','','default','','24','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_RCV_DETAILS_V','','');
xxeis.eis_rs_ins.rc( 'Receipt Details-WC',201,'RCV_SHIPMENT_NUM','Rcv Shipment Num','Rcv Shipment Num','','','default','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_RCV_DETAILS_V','','');
xxeis.eis_rs_ins.rc( 'Receipt Details-WC',201,'RCV_SL_QUANTITY_RECEIVED','Rcv Sl Quantity Received','Rcv Sl Quantity Received','','~~~','default','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_RCV_DETAILS_V','','');
xxeis.eis_rs_ins.rc( 'Receipt Details-WC',201,'RCV_SL_QUANTITY_SHIPPED','Rcv Sl Quantity Shipped','Rcv Sl Quantity Shipped','','~~~','default','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_RCV_DETAILS_V','','');
xxeis.eis_rs_ins.rc( 'Receipt Details-WC',201,'RCV_SL_UNIT_OF_MEASURE','Rcv Sl Unit Of Measure','Rcv Sl Unit Of Measure','','','default','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_RCV_DETAILS_V','','');
xxeis.eis_rs_ins.rc( 'Receipt Details-WC',201,'RECEIPT_NUM','Receipt Num','Receipt Num','','','default','','25','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_RCV_DETAILS_V','','');
xxeis.eis_rs_ins.rc( 'Receipt Details-WC',201,'RECEIVER','Receiver','Receiver','','','default','','26','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_RCV_DETAILS_V','','');
xxeis.eis_rs_ins.rc( 'Receipt Details-WC',201,'SRC','Src','Src','','','default','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_RCV_DETAILS_V','','');
xxeis.eis_rs_ins.rc( 'Receipt Details-WC',201,'SRC_TYPE','Src Type','Src Type','','','default','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_RCV_DETAILS_V','','');
xxeis.eis_rs_ins.rc( 'Receipt Details-WC',201,'TX_DATE','Tx Date','Tx Date','','','default','','27','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_RCV_DETAILS_V','','');
xxeis.eis_rs_ins.rc( 'Receipt Details-WC',201,'TX_REASON','Tx Reason','Tx Reason','','','default','','28','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_RCV_DETAILS_V','','');
xxeis.eis_rs_ins.rc( 'Receipt Details-WC',201,'TX_TYPE','Tx Type','Tx Type','','','default','','29','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_RCV_DETAILS_V','','');
xxeis.eis_rs_ins.rc( 'Receipt Details-WC',201,'UNIT_WEIGHT','Unit Weight','Unit Weight','','~~~','default','','31','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_RCV_DETAILS_V','','');
xxeis.eis_rs_ins.rc( 'Receipt Details-WC',201,'UOM','Uom','Uom','','','default','','30','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_RCV_DETAILS_V','','');
--Inserting Report Parameters - Receipt Details-WC
xxeis.eis_rs_ins.rp( 'Receipt Details-WC',201,'Organization','Organization','DELIVER_TO_LOCATION','IN','Delivery Location','','VARCHAR2','N','Y','1','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Receipt Details-WC',201,'PO Receipt Date From','PO Receipt Date From','TX_DATE','>=','','','DATE','N','Y','2','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','Start Date','');
xxeis.eis_rs_ins.rp( 'Receipt Details-WC',201,'PO Receipt Date To','PO Receipt Date To','TX_DATE','<=','','','DATE','N','Y','3','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','End Date','');
xxeis.eis_rs_ins.rp( 'Receipt Details-WC',201,'Receipt Number','Receipt Number','RECEIPT_NUM','IN','EIS_PO_RECEIPT_NUM_LOV','','VARCHAR2','N','Y','4','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Receipt Details-WC',201,'Receiver','Receiver','RECEIVER','IN','EIS_PO_RECEIVER_LOV','','VARCHAR2','N','Y','5','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Receipt Details-WC',201,'Buyer','Buyer','BUYER_PREPARER','IN','EIS_PO_BUYER_LOV','','VARCHAR2','N','Y','6','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Receipt Details-WC',201,'Precision','Precision','','IN','EIS_PO_NUMBER_PRECISION_LOV','SELECT nvl(fnd_profile.VALUE(''REPORT_QUANTITY_PRECISION''),2) FROM dual','NUMERIC','Y','Y','7','','N','SQL','XXEIS_RS_ADMIN','Y','N','','','');
--Inserting Report Conditions - Receipt Details-WC
xxeis.eis_rs_ins.rcn( 'Receipt Details-WC',201,'DELIVER_TO_LOCATION','IN',':Organization','','','Y','1','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Receipt Details-WC',201,'TRUNC(TX_DATE)','>=',':PO Receipt Date From','','','Y','2','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Receipt Details-WC',201,'TRUNC(TX_DATE)','<=',':PO Receipt Date To','','','Y','3','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Receipt Details-WC',201,'RECEIPT_NUM','IN',':Receipt Number','','','Y','4','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Receipt Details-WC',201,'RECEIVER','IN',':Receiver','','','Y','5','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Receipt Details-WC',201,'BUYER_PREPARER','IN',':Buyer','','','Y','6','Y','XXEIS_RS_ADMIN');
--Inserting Report Sorts - Receipt Details-WC
xxeis.eis_rs_ins.rs( 'Receipt Details-WC',201,'SRC_TYPE','ASC','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rs( 'Receipt Details-WC',201,'RECEIPT_NUM','ASC','XXEIS_RS_ADMIN','','');
--Inserting Report Triggers - Receipt Details-WC
xxeis.eis_rs_ins.rt( 'Receipt Details-WC',201,'begin
XXEIS.EIS_RS_PO_FIN_COM_UTIL_PKG.g_qty_precision := to_number(:Precision);
end;','B','Y','XXEIS_RS_ADMIN');
--Inserting Report Templates - Receipt Details-WC
--Inserting Report Portals - Receipt Details-WC
--Inserting Report Dashboards - Receipt Details-WC
--Inserting Report Security - Receipt Details-WC
xxeis.eis_rs_ins.rsec( 'Receipt Details-WC','201','','20707',201,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Receipt Details-WC','201','','50621',201,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Receipt Details-WC','201','','50921',201,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Receipt Details-WC','201','','50892',201,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Receipt Details-WC','201','','50910',201,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Receipt Details-WC','201','','50893',201,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Receipt Details-WC','201','','50983',201,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Receipt Details-WC','200','','50904',201,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Receipt Details-WC','200','','50905',201,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Receipt Details-WC','200','','50991',201,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Receipt Details-WC','200','','50902',201,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Receipt Details-WC','200','','50890',201,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Receipt Details-WC','200','','50781',201,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Receipt Details-WC','200','','50782',201,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Receipt Details-WC','200','','50760',201,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Receipt Details-WC','200','','50637',201,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Receipt Details-WC','200','','50620',201,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Receipt Details-WC','200','','50903',201,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Receipt Details-WC','200','','50887',201,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Receipt Details-WC','200','','50888',201,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Receipt Details-WC','200','','50889',201,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Receipt Details-WC','20005','','50900',201,'XXEIS_RS_ADMIN','','');
--Inserting Report Pivots - Receipt Details-WC
xxeis.eis_rs_ins.rpivot( 'Receipt Details-WC',201,'Pivot','1','0,0|1,1,0','1,1,1,1|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rs_ins.rpivot_dtls( 'Receipt Details-WC',201,'Pivot','TX_DATE','PAGE_FIELD','','','1','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Receipt Details-WC',201,'Pivot','PAR_TX_TYPE','PAGE_FIELD','','','2','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Receipt Details-WC',201,'Pivot','DOC_TYPE','PAGE_FIELD','','','3','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Receipt Details-WC',201,'Pivot','DESTINATION_TYPE','PAGE_FIELD','','','4','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Receipt Details-WC',201,'Pivot','BUYER_PREPARER','PAGE_FIELD','','','5','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Receipt Details-WC',201,'Pivot','RECEIPT_NUM','PAGE_FIELD','','','6','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Receipt Details-WC',201,'Pivot','QTY','DATA_FIELD','SUM','','1','','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Receipt Details-WC',201,'Pivot','RECEIVER','ROW_FIELD','','','1','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'Receipt Details-WC',201,'Pivot','SRC','ROW_FIELD','','','2','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'Receipt Details-WC',201,'Pivot','DOC_NUM','ROW_FIELD','','','3','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'Receipt Details-WC',201,'Pivot','ITEM','ROW_FIELD','','','4','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'Receipt Details-WC',201,'Pivot','DES','ROW_FIELD','','','5','1','');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
END;
/
set scan on define on
