--Report Name            : GL and Bank Statement Balances
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data XXEIS_1971504_OYDRDH_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(260);
IF mod_exist = 'Y' THEN 
--Inserting Object XXEIS_1971504_OYDRDH_V
xxeis.eis_rsc_ins.v( 'XXEIS_1971504_OYDRDH_V',260,'Paste SQL View for GL and Bank Statement Balances','1.0','','','JB027320','APPS','GL and Bank Statement Balances View','X1OV','','','VIEW','US','','');
--Delete Object Columns for XXEIS_1971504_OYDRDH_V
xxeis.eis_rsc_utility.delete_view_rows('XXEIS_1971504_OYDRDH_V',260,FALSE);
--Inserting Object Columns for XXEIS_1971504_OYDRDH_V
xxeis.eis_rsc_ins.vc( 'XXEIS_1971504_OYDRDH_V','CURRENCY_CODE',260,'','','','','','JB027320','VARCHAR2','','','Currency Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1971504_OYDRDH_V','STATEMENT_DATE',260,'','','','','','JB027320','DATE','','','Statement Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1971504_OYDRDH_V','PERIOD_NAME',260,'','','','','','JB027320','VARCHAR2','','','Period Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1971504_OYDRDH_V','STATEMENT_BALANCE',260,'','','','~T~D~2','','JB027320','NUMBER','','','Statement Balance','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1971504_OYDRDH_V','GL_BALANCE',260,'','','','~T~D~2','','JB027320','NUMBER','','','Gl Balance','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1971504_OYDRDH_V','DIFFERENCE',260,'','','','','','JB027320','NUMBER','','','Difference','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1971504_OYDRDH_V','BANK_ACCOUNT_NAME',260,'','','','','','JB027320','VARCHAR2','','','Bank Account Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1971504_OYDRDH_V','BANK_ACCOUNT_NUM',260,'','','','','','JB027320','VARCHAR2','','','Bank Account Num','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1971504_OYDRDH_V','SEGMENT1',260,'','','','','','JB027320','VARCHAR2','','','Segment1','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1971504_OYDRDH_V','SEGMENT2',260,'','','','','','JB027320','VARCHAR2','','','Segment2','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1971504_OYDRDH_V','SEGMENT3',260,'','','','','','JB027320','VARCHAR2','','','Segment3','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1971504_OYDRDH_V','SEGMENT4',260,'','','','','','JB027320','VARCHAR2','','','Segment4','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1971504_OYDRDH_V','SEGMENT5',260,'','','','','','JB027320','VARCHAR2','','','Segment5','','','','US');
--Inserting Object Components for XXEIS_1971504_OYDRDH_V
--Inserting Object Component Joins for XXEIS_1971504_OYDRDH_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 260');
END IF;
END;
/
--There are no LOV/LOV TL entries for the report GL and Bank Statement Balances
prompt Creating Report Data for GL and Bank Statement Balances
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(260);
IF mod_exist = 'Y' THEN 
--Deleting Report data - GL and Bank Statement Balances
xxeis.eis_rsc_utility.delete_report_rows( 'GL and Bank Statement Balances' );
--Inserting Report - GL and Bank Statement Balances
xxeis.eis_rsc_ins.r( 260,'GL and Bank Statement Balances','','This report can be used to pull the bank statement balances as of a specific date and the gl balance for the month that the statement date is part of.  Note that the gl balance can only be retrieved by period, not by a specific date like the bank statements can.  

Created as part of project #23030.  Service # 243839.
Created by John Bozik
Created on 12/22/2015','','','','XXEIS_RS_ADMIN','XXEIS_1971504_OYDRDH_V','Y','','SELECT X5YV.BANK_ACCOUNT_NAME,
       X5YV.BANK_ACCOUNT_NUM,
       X5YV.SEGMENT1,
       X5YV.SEGMENT2,
       X5YV.SEGMENT3,
       X5YV.SEGMENT4,
       X5YV.SEGMENT5,
       X5YV.CURRENCY_CODE,
       X5YV.STATEMENT_DATE,
       GLB.PERIOD_NAME,
       X5YV.CONTROL_END_BALANCE STATEMENT_BALANCE,
       NVL(GLB.period_net_dr, 0) - NVL(GLB.period_net_cr, 0) +
       NVL(GLB.begin_balance_dr, 0) - NVL(GLB.begin_balance_cr, 0) GL_BALANCE,
       NVL(GLB.period_net_dr, 0) - NVL(GLB.period_net_cr, 0) +
       NVL(GLB.begin_balance_dr, 0) - NVL(GLB.begin_balance_cr, 0) -
       NVL(X5YV.CONTROL_END_BALANCE, 0) Difference
  FROM APPS.XXCUS_BANK_BALANCES X5YV,
       gl.gl_balances           glb,
       gl.gl_code_combinations  gcc
 WHERE 1 = 1
   and X5YV.SEGMENT1 = gcc.segment1
   and X5YV.SEGMENT2 = gcc.segment2
   and X5YV.SEGMENT3 = gcc.segment3
   and X5YV.SEGMENT4 = gcc.segment4
   and X5YV.SEGMENT5 = gcc.segment5
   and X5YV.SEGMENT6 = gcc.segment6
   and X5YV.SEGMENT7 = gcc.segment7
   and gcc.code_combination_id = glb.code_combination_id
   and glb.actual_flag = ''A''
   and X5YV.CURRENCY_CODE = glb.currency_code
   AND X5YV.CONTROL_END_BALANCE <> 0
   and gcc.segment4 not in (''101202'', ''109001'', ''981002'')
UNION ALL
SELECT X5YV.BANK_ACCOUNT_NAME,
       X5YV.BANK_ACCOUNT_NUM,
       X5YV.SEGMENT1,
       X5YV.SEGMENT2,
       X5YV.SEGMENT3,
       X5YV.SEGMENT4,
       X5YV.SEGMENT5,
       X5YV.CURRENCY_CODE,
       X5YV.STATEMENT_DATE,
       GLB.PERIOD_NAME,
       X5YV.CONTROL_END_BALANCE STATEMENT_BALANCE,
       NVL(GLB.period_net_dr, 0) - NVL(GLB.period_net_cr, 0) +
       NVL(GLB.begin_balance_dr, 0) - NVL(GLB.begin_balance_cr, 0) GL_BALANCE,
       NVL(GLB.period_net_dr, 0) - NVL(GLB.period_net_cr, 0) +
       NVL(GLB.begin_balance_dr, 0) - NVL(GLB.begin_balance_cr, 0) -
       NVL(X5YV.CONTROL_END_BALANCE, 0) Difference
  FROM APPS.XXCUS_BANK_BALANCES X5YV,
       gl.gl_balances           glb,
       gl.gl_code_combinations  gcc
 WHERE 1 = 1
   and X5YV.SEGMENT1 = gcc.segment1
   and X5YV.SEGMENT2 = gcc.segment2
   and X5YV.SEGMENT3 = gcc.segment3
   and X5YV.SEGMENT4 = gcc.segment4
   and X5YV.SEGMENT5 = gcc.segment5
   and X5YV.SEGMENT6 = gcc.segment6
   and X5YV.SEGMENT7 = gcc.segment7
   and gcc.code_combination_id = glb.code_combination_id
   and glb.actual_flag = ''A''
   and X5YV.CURRENCY_CODE = glb.currency_code
   AND X5YV.CONTROL_END_BALANCE <> 0
   and glb.translated_flag = ''R''
','XXEIS_RS_ADMIN','','N','Reconciliation','','EXCEL,','N','','','','','','','APPS','US','','','','');
--Inserting Report Columns - GL and Bank Statement Balances
xxeis.eis_rsc_ins.rc( 'GL and Bank Statement Balances',260,'SEGMENT4','Account','','','','default','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1971504_OYDRDH_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'GL and Bank Statement Balances',260,'BANK_ACCOUNT_NAME','Bank Account Name','','','','default','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1971504_OYDRDH_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'GL and Bank Statement Balances',260,'STATEMENT_BALANCE','Statement Balance','','','~,~.~2','default','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1971504_OYDRDH_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'GL and Bank Statement Balances',260,'SEGMENT3','CostCenter','','','','default','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1971504_OYDRDH_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'GL and Bank Statement Balances',260,'SEGMENT1','Product','','','','default','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1971504_OYDRDH_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'GL and Bank Statement Balances',260,'BANK_ACCOUNT_NUM','Bank Account Num','','','','default','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1971504_OYDRDH_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'GL and Bank Statement Balances',260,'SEGMENT5','Project','','','','default','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1971504_OYDRDH_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'GL and Bank Statement Balances',260,'DIFFERENCE','Difference','','','~T~D~2','default','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1971504_OYDRDH_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'GL and Bank Statement Balances',260,'CURRENCY_CODE','Currency Code','','','','default','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1971504_OYDRDH_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'GL and Bank Statement Balances',260,'SEGMENT2','Location','','','','default','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1971504_OYDRDH_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'GL and Bank Statement Balances',260,'GL_BALANCE','Gl Balance','','','~,~.~2','default','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1971504_OYDRDH_V','','','SUM','US','');
--Inserting Report Parameters - GL and Bank Statement Balances
xxeis.eis_rsc_ins.rp( 'GL and Bank Statement Balances',260,'STATEMENT_DATE','DD-MMM-YY','STATEMENT_DATE','=','','','DATE','N','Y','1','Y','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','XXEIS_1971504_OYDRDH_V','','','US','');
--Inserting Dependent Parameters - GL and Bank Statement Balances
--Inserting Report Conditions - GL and Bank Statement Balances
xxeis.eis_rsc_ins.rcnh( 'GL and Bank Statement Balances',260,'X1OV.STATEMENT_DATE = :STATEMENT_DATE ','ADVANCED','','1#$#','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','STATEMENT_DATE','','','','','','','','','','','EQUALS','Y','Y','X1OV.STATEMENT_DATE','','','','1',260,'GL and Bank Statement Balances','X1OV.STATEMENT_DATE = :STATEMENT_DATE ');
xxeis.eis_rsc_ins.rcnh( 'GL and Bank Statement Balances',260,'Free Text ','FREE_TEXT','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','','Y','','','','','and X1OV.PERIOD_NAME IN (select glp.period_name
  from gl.gl_periods glp
 where :STATEMENT_DATE between glp.start_date and glp.end_date
   and glp.period_set_name = ''4-4-QTR'')','1',260,'GL and Bank Statement Balances','Free Text ');
--Inserting Report Sorts - GL and Bank Statement Balances
--Inserting Report Triggers - GL and Bank Statement Balances
--inserting report templates - GL and Bank Statement Balances
--Inserting Report Portals - GL and Bank Statement Balances
--inserting report dashboards - GL and Bank Statement Balances
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'GL and Bank Statement Balances','260','XXEIS_1971504_OYDRDH_V','XXEIS_1971504_OYDRDH_V','N','');
--inserting report security - GL and Bank Statement Balances
xxeis.eis_rsc_ins.rsec( 'GL and Bank Statement Balances','260','','HDS_CSH_MNGMNT_SU_GLBL',260,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'GL and Bank Statement Balances','260','','HDS_CSH_MNGMNET_SPR_FSS',260,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'GL and Bank Statement Balances','260','','HDS_CSH_MNGMNET_MSTR_FSS',260,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'GL and Bank Statement Balances','','AB063501','',260,'XXEIS_RS_ADMIN','','N','');
--Inserting Report Pivots - GL and Bank Statement Balances
--Inserting Report   Version details- GL and Bank Statement Balances
xxeis.eis_rsc_ins.rv( 'GL and Bank Statement Balances','','GL and Bank Statement Balances','AB063501');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 260');
END IF;
END;
/
