--Report Name            : Extended Attributes Extract (ICC) - WC
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data EIS_XXWC_INV_EXTEND_ATTR_ICC_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(401);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_INV_EXTEND_ATTR_ICC_V
xxeis.eis_rsc_ins.v( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V',401,'','','','','SA059956','XXEIS','Eis Xxwc Inv Extend Attr Icc V','EXIEAIV','','','VIEW','US','','');
--Delete Object Columns for EIS_XXWC_INV_EXTEND_ATTR_ICC_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_INV_EXTEND_ATTR_ICC_V',401,FALSE);
--Inserting Object Columns for EIS_XXWC_INV_EXTEND_ATTR_ICC_V
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','CATEGORY_CLASS_DESC',401,'Category Class Desc','CATEGORY_CLASS_DESC','','','','SA059956','VARCHAR2','','','Category Class Desc','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','CATCLASS',401,'Catclass','CATCLASS','','','','SA059956','VARCHAR2','','','Catclass','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','CATMGT_CATEGORY_DESC',401,'Catmgt Category Desc','CATMGT_CATEGORY_DESC','','','','SA059956','VARCHAR2','','','Catmgt Category Desc','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC_COUNT',401,'Spec Count','SPEC_COUNT','','','','SA059956','NUMBER','','','Spec Count','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','ICC',401,'Icc','ICC','','','','SA059956','VARCHAR2','','','Icc','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC1',401,'Spec1','SPEC1','','','','SA059956','VARCHAR2','','','Spec1','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC10',401,'Spec10','SPEC10','','','','SA059956','VARCHAR2','','','Spec10','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC100',401,'Spec100','SPEC100','','','','SA059956','VARCHAR2','','','Spec100','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC11',401,'Spec11','SPEC11','','','','SA059956','VARCHAR2','','','Spec11','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC12',401,'Spec12','SPEC12','','','','SA059956','VARCHAR2','','','Spec12','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC13',401,'Spec13','SPEC13','','','','SA059956','VARCHAR2','','','Spec13','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC14',401,'Spec14','SPEC14','','','','SA059956','VARCHAR2','','','Spec14','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC15',401,'Spec15','SPEC15','','','','SA059956','VARCHAR2','','','Spec15','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC16',401,'Spec16','SPEC16','','','','SA059956','VARCHAR2','','','Spec16','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC17',401,'Spec17','SPEC17','','','','SA059956','VARCHAR2','','','Spec17','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC18',401,'Spec18','SPEC18','','','','SA059956','VARCHAR2','','','Spec18','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC19',401,'Spec19','SPEC19','','','','SA059956','VARCHAR2','','','Spec19','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC2',401,'Spec2','SPEC2','','','','SA059956','VARCHAR2','','','Spec2','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC20',401,'Spec20','SPEC20','','','','SA059956','VARCHAR2','','','Spec20','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC21',401,'Spec21','SPEC21','','','','SA059956','VARCHAR2','','','Spec21','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC22',401,'Spec22','SPEC22','','','','SA059956','VARCHAR2','','','Spec22','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC23',401,'Spec23','SPEC23','','','','SA059956','VARCHAR2','','','Spec23','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC24',401,'Spec24','SPEC24','','','','SA059956','VARCHAR2','','','Spec24','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC25',401,'Spec25','SPEC25','','','','SA059956','VARCHAR2','','','Spec25','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC26',401,'Spec26','SPEC26','','','','SA059956','VARCHAR2','','','Spec26','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC27',401,'Spec27','SPEC27','','','','SA059956','VARCHAR2','','','Spec27','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC28',401,'Spec28','SPEC28','','','','SA059956','VARCHAR2','','','Spec28','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC29',401,'Spec29','SPEC29','','','','SA059956','VARCHAR2','','','Spec29','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC3',401,'Spec3','SPEC3','','','','SA059956','VARCHAR2','','','Spec3','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC30',401,'Spec30','SPEC30','','','','SA059956','VARCHAR2','','','Spec30','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC31',401,'Spec31','SPEC31','','','','SA059956','VARCHAR2','','','Spec31','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC32',401,'Spec32','SPEC32','','','','SA059956','VARCHAR2','','','Spec32','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC33',401,'Spec33','SPEC33','','','','SA059956','VARCHAR2','','','Spec33','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC34',401,'Spec34','SPEC34','','','','SA059956','VARCHAR2','','','Spec34','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC35',401,'Spec35','SPEC35','','','','SA059956','VARCHAR2','','','Spec35','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC36',401,'Spec36','SPEC36','','','','SA059956','VARCHAR2','','','Spec36','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC37',401,'Spec37','SPEC37','','','','SA059956','VARCHAR2','','','Spec37','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC38',401,'Spec38','SPEC38','','','','SA059956','VARCHAR2','','','Spec38','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC39',401,'Spec39','SPEC39','','','','SA059956','VARCHAR2','','','Spec39','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC4',401,'Spec4','SPEC4','','','','SA059956','VARCHAR2','','','Spec4','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC40',401,'Spec40','SPEC40','','','','SA059956','VARCHAR2','','','Spec40','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC41',401,'Spec41','SPEC41','','','','SA059956','VARCHAR2','','','Spec41','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC42',401,'Spec42','SPEC42','','','','SA059956','VARCHAR2','','','Spec42','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC43',401,'Spec43','SPEC43','','','','SA059956','VARCHAR2','','','Spec43','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC44',401,'Spec44','SPEC44','','','','SA059956','VARCHAR2','','','Spec44','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC45',401,'Spec45','SPEC45','','','','SA059956','VARCHAR2','','','Spec45','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC46',401,'Spec46','SPEC46','','','','SA059956','VARCHAR2','','','Spec46','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC47',401,'Spec47','SPEC47','','','','SA059956','VARCHAR2','','','Spec47','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC48',401,'Spec48','SPEC48','','','','SA059956','VARCHAR2','','','Spec48','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC49',401,'Spec49','SPEC49','','','','SA059956','VARCHAR2','','','Spec49','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC5',401,'Spec5','SPEC5','','','','SA059956','VARCHAR2','','','Spec5','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC50',401,'Spec50','SPEC50','','','','SA059956','VARCHAR2','','','Spec50','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC51',401,'Spec51','SPEC51','','','','SA059956','VARCHAR2','','','Spec51','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC52',401,'Spec52','SPEC52','','','','SA059956','VARCHAR2','','','Spec52','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC53',401,'Spec53','SPEC53','','','','SA059956','VARCHAR2','','','Spec53','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC54',401,'Spec54','SPEC54','','','','SA059956','VARCHAR2','','','Spec54','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC55',401,'Spec55','SPEC55','','','','SA059956','VARCHAR2','','','Spec55','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC56',401,'Spec56','SPEC56','','','','SA059956','VARCHAR2','','','Spec56','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC57',401,'Spec57','SPEC57','','','','SA059956','VARCHAR2','','','Spec57','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC58',401,'Spec58','SPEC58','','','','SA059956','VARCHAR2','','','Spec58','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC59',401,'Spec59','SPEC59','','','','SA059956','VARCHAR2','','','Spec59','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC6',401,'Spec6','SPEC6','','','','SA059956','VARCHAR2','','','Spec6','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC60',401,'Spec60','SPEC60','','','','SA059956','VARCHAR2','','','Spec60','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC61',401,'Spec61','SPEC61','','','','SA059956','VARCHAR2','','','Spec61','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC62',401,'Spec62','SPEC62','','','','SA059956','VARCHAR2','','','Spec62','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC63',401,'Spec63','SPEC63','','','','SA059956','VARCHAR2','','','Spec63','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC64',401,'Spec64','SPEC64','','','','SA059956','VARCHAR2','','','Spec64','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC65',401,'Spec65','SPEC65','','','','SA059956','VARCHAR2','','','Spec65','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC66',401,'Spec66','SPEC66','','','','SA059956','VARCHAR2','','','Spec66','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC67',401,'Spec67','SPEC67','','','','SA059956','VARCHAR2','','','Spec67','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC68',401,'Spec68','SPEC68','','','','SA059956','VARCHAR2','','','Spec68','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC69',401,'Spec69','SPEC69','','','','SA059956','VARCHAR2','','','Spec69','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC7',401,'Spec7','SPEC7','','','','SA059956','VARCHAR2','','','Spec7','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC70',401,'Spec70','SPEC70','','','','SA059956','VARCHAR2','','','Spec70','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC71',401,'Spec71','SPEC71','','','','SA059956','VARCHAR2','','','Spec71','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC72',401,'Spec72','SPEC72','','','','SA059956','VARCHAR2','','','Spec72','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC73',401,'Spec73','SPEC73','','','','SA059956','VARCHAR2','','','Spec73','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC74',401,'Spec74','SPEC74','','','','SA059956','VARCHAR2','','','Spec74','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC75',401,'Spec75','SPEC75','','','','SA059956','VARCHAR2','','','Spec75','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC76',401,'Spec76','SPEC76','','','','SA059956','VARCHAR2','','','Spec76','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC77',401,'Spec77','SPEC77','','','','SA059956','VARCHAR2','','','Spec77','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC78',401,'Spec78','SPEC78','','','','SA059956','VARCHAR2','','','Spec78','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC79',401,'Spec79','SPEC79','','','','SA059956','VARCHAR2','','','Spec79','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC8',401,'Spec8','SPEC8','','','','SA059956','VARCHAR2','','','Spec8','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC80',401,'Spec80','SPEC80','','','','SA059956','VARCHAR2','','','Spec80','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC81',401,'Spec81','SPEC81','','','','SA059956','VARCHAR2','','','Spec81','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC82',401,'Spec82','SPEC82','','','','SA059956','VARCHAR2','','','Spec82','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC83',401,'Spec83','SPEC83','','','','SA059956','VARCHAR2','','','Spec83','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC84',401,'Spec84','SPEC84','','','','SA059956','VARCHAR2','','','Spec84','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC85',401,'Spec85','SPEC85','','','','SA059956','VARCHAR2','','','Spec85','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC86',401,'Spec86','SPEC86','','','','SA059956','VARCHAR2','','','Spec86','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC87',401,'Spec87','SPEC87','','','','SA059956','VARCHAR2','','','Spec87','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC88',401,'Spec88','SPEC88','','','','SA059956','VARCHAR2','','','Spec88','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC89',401,'Spec89','SPEC89','','','','SA059956','VARCHAR2','','','Spec89','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC9',401,'Spec9','SPEC9','','','','SA059956','VARCHAR2','','','Spec9','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC90',401,'Spec90','SPEC90','','','','SA059956','VARCHAR2','','','Spec90','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC91',401,'Spec91','SPEC91','','','','SA059956','VARCHAR2','','','Spec91','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC92',401,'Spec92','SPEC92','','','','SA059956','VARCHAR2','','','Spec92','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC93',401,'Spec93','SPEC93','','','','SA059956','VARCHAR2','','','Spec93','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC94',401,'Spec94','SPEC94','','','','SA059956','VARCHAR2','','','Spec94','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC95',401,'Spec95','SPEC95','','','','SA059956','VARCHAR2','','','Spec95','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC96',401,'Spec96','SPEC96','','','','SA059956','VARCHAR2','','','Spec96','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC97',401,'Spec97','SPEC97','','','','SA059956','VARCHAR2','','','Spec97','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC98',401,'Spec98','SPEC98','','','','SA059956','VARCHAR2','','','Spec98','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_EXTEND_ATTR_ICC_V','SPEC99',401,'Spec99','SPEC99','','','','SA059956','VARCHAR2','','','Spec99','','','','');
--Inserting Object Components for EIS_XXWC_INV_EXTEND_ATTR_ICC_V
--Inserting Object Component Joins for EIS_XXWC_INV_EXTEND_ATTR_ICC_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 401');
END IF;
END;
/
prompt Creating Report LOV Data for Extended Attributes Extract (ICC) - WC
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(401);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - Extended Attributes Extract (ICC) - WC
xxeis.eis_rsc_ins.lov( 401,'SELECT DISTINCT mcv.concatenated_segments catclass
FROM mtl_categories_b_kfv mcv
ORDER BY 1','','XXWC INV CatClass LOV','This LOV shows the category class list','SA059956',NULL,'N','','','N','','','','US');
xxeis.eis_rsc_ins.lov( 401,'select
distinct MICG.SEGMENT1 INVENTORY_ITEM_CATEGORY
from
APPS.MTL_ITEM_CATALOG_GROUPS_B MICG
order by 1','','XXWC INVENTORY ITEM CATEGORY LOV','XXWC INVENTORY ITEM CATEGORY LOV','SA059956',NULL,'N','','','N','N','','','US');
xxeis.eis_rsc_ins.lov( 401,'SELECT t_cat.description Catmgt_Category_Desc
  FROM apps.fnd_flex_values b_cat,
    APPS.FND_FLEX_VALUES_TL T_CAT,
    APPS.FND_FLEX_VALUE_SETS V_CAt
  WHERE b_cat.flex_value_id      = t_cat.flex_value_id
  AND b_cat.flex_value_set_id    = v_cat.flex_value_set_id
  and V_CAT.FLEX_VALUE_SET_NAME in (''XXWC_CATMGT_CATEGORIES'')
order by 1','','XXWC CATMGT CATEGORY DESC LOV','XXWC CATMGT CATEGORY DESC LOV','SA059956',NULL,'N','','','N','N','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 401');
END IF;
END;
/
prompt Creating Report Data for Extended Attributes Extract (ICC) - WC
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(401);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Extended Attributes Extract (ICC) - WC
xxeis.eis_rsc_utility.delete_report_rows( 'Extended Attributes Extract (ICC) - WC' );
--Inserting Report - Extended Attributes Extract (ICC) - WC
xxeis.eis_rsc_ins.r( 401,'Extended Attributes Extract (ICC) - WC','','Item Catalog Category or Attribute Group Display Name','','','','SA059956','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','Y','','','SA059956','','N','White Cap Reports','','CSV,EXCEL,','N','','','','','','N','','US','','','','');
--Inserting Report Columns - Extended Attributes Extract (ICC) - WC
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'CATCLASS','CatClass','Catclass','','','default','','2','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'CATEGORY_CLASS_DESC','Category Class Desc','Category Class Desc','','','default','','3','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'CATMGT_CATEGORY_DESC','Catmgt Category Desc','Catmgt Category Desc','','','default','','1','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'ICC','ICC','Icc','','','default','','4','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC_COUNT','Spec Count','Spec Count','','~~~','default','','5','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC1','Spec1','Spec1','','','default','','6','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC10','Spec10','Spec10','','','','','15','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC100','Spec100','Spec100','','','','','105','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC11','Spec11','Spec11','','','','','16','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC12','Spec12','Spec12','','','','','17','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC13','Spec13','Spec13','','','','','18','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC14','Spec14','Spec14','','','','','19','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC15','Spec15','Spec15','','','','','20','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC16','Spec16','Spec16','','','','','21','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC17','Spec17','Spec17','','','','','22','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC18','Spec18','Spec18','','','','','23','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC19','Spec19','Spec19','','','','','24','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC2','Spec2','Spec2','','','default','','7','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC20','Spec20','Spec20','','','','','25','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC21','Spec21','Spec21','','','','','26','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC22','Spec22','Spec22','','','','','27','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC23','Spec23','Spec23','','','','','28','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC24','Spec24','Spec24','','','','','29','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC25','Spec25','Spec25','','','','','30','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC26','Spec26','Spec26','','','','','31','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC27','Spec27','Spec27','','','','','32','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC28','Spec28','Spec28','','','','','33','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC29','Spec29','Spec29','','','','','34','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC3','Spec3','Spec3','','','default','','8','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC30','Spec30','Spec30','','','','','35','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC31','Spec31','Spec31','','','','','36','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC32','Spec32','Spec32','','','','','37','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC33','Spec33','Spec33','','','','','38','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC34','Spec34','Spec34','','','','','39','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC35','Spec35','Spec35','','','','','40','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC36','Spec36','Spec36','','','','','41','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC37','Spec37','Spec37','','','','','42','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC38','Spec38','Spec38','','','','','43','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC39','Spec39','Spec39','','','','','44','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC4','Spec4','Spec4','','','default','','9','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC40','Spec40','Spec40','','','','','45','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC41','Spec41','Spec41','','','','','46','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC42','Spec42','Spec42','','','','','47','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC43','Spec43','Spec43','','','','','48','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC44','Spec44','Spec44','','','','','49','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC45','Spec45','Spec45','','','','','50','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC46','Spec46','Spec46','','','','','51','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC47','Spec47','Spec47','','','','','52','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC48','Spec48','Spec48','','','','','53','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC49','Spec49','Spec49','','','','','54','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC5','Spec5','Spec5','','','default','','10','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC50','Spec50','Spec50','','','','','55','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC51','Spec51','Spec51','','','','','56','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC52','Spec52','Spec52','','','','','57','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC53','Spec53','Spec53','','','','','58','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC54','Spec54','Spec54','','','','','59','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC55','Spec55','Spec55','','','','','60','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC56','Spec56','Spec56','','','','','61','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC57','Spec57','Spec57','','','','','62','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC58','Spec58','Spec58','','','','','63','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC59','Spec59','Spec59','','','','','64','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC6','Spec6','Spec6','','','','','11','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC60','Spec60','Spec60','','','','','65','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC61','Spec61','Spec61','','','','','66','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC62','Spec62','Spec62','','','','','67','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC63','Spec63','Spec63','','','','','68','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC64','Spec64','Spec64','','','','','69','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC65','Spec65','Spec65','','','','','70','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC66','Spec66','Spec66','','','','','71','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC67','Spec67','Spec67','','','','','72','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC68','Spec68','Spec68','','','','','73','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC69','Spec69','Spec69','','','','','74','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC7','Spec7','Spec7','','','','','12','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC70','Spec70','Spec70','','','','','75','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC71','Spec71','Spec71','','','','','76','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC72','Spec72','Spec72','','','','','77','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC73','Spec73','Spec73','','','','','78','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC74','Spec74','Spec74','','','','','79','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC75','Spec75','Spec75','','','','','80','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC76','Spec76','Spec76','','','','','81','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC77','Spec77','Spec77','','','','','82','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC78','Spec78','Spec78','','','','','83','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC79','Spec79','Spec79','','','','','84','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC8','Spec8','Spec8','','','','','13','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC80','Spec80','Spec80','','','','','85','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC81','Spec81','Spec81','','','','','86','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC82','Spec82','Spec82','','','','','87','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC83','Spec83','Spec83','','','','','88','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC84','Spec84','Spec84','','','','','89','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC85','Spec85','Spec85','','','','','90','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC86','Spec86','Spec86','','','','','91','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC87','Spec87','Spec87','','','','','92','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC88','Spec88','Spec88','','','','','93','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC89','Spec89','Spec89','','','','','94','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC9','Spec9','Spec9','','','','','14','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC90','Spec90','Spec90','','','','','95','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC91','Spec91','Spec91','','','','','96','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC92','Spec92','Spec92','','','','','97','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC93','Spec93','Spec93','','','','','98','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC94','Spec94','Spec94','','','','','99','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC95','Spec95','Spec95','','','','','100','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC96','Spec96','Spec96','','','','','101','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC97','Spec97','Spec97','','','','','102','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC98','Spec98','Spec98','','','','','103','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Extended Attributes Extract (ICC) - WC',401,'SPEC99','Spec99','Spec99','','','','','104','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','US','');
--Inserting Report Parameters - Extended Attributes Extract (ICC) - WC
xxeis.eis_rsc_ins.rp( 'Extended Attributes Extract (ICC) - WC',401,'Cat Class','Cat Class','CATCLASS','IN','XXWC INV CatClass LOV','','VARCHAR2','N','Y','3','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Extended Attributes Extract (ICC) - WC',401,'Catmgt Category Desc','Catmgt Category Desc','CATMGT_CATEGORY_DESC','IN','XXWC CATMGT CATEGORY DESC LOV','','VARCHAR2','N','Y','2','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Extended Attributes Extract (ICC) - WC',401,'Item Catalog Category','Item Catalog Category','ICC','IN','XXWC INVENTORY ITEM CATEGORY LOV','','VARCHAR2','N','Y','1','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','US','');
--Inserting Dependent Parameters - Extended Attributes Extract (ICC) - WC
--Inserting Report Conditions - Extended Attributes Extract (ICC) - WC
xxeis.eis_rsc_ins.rcnh( 'Extended Attributes Extract (ICC) - WC',401,'EXIEAIV.CATCLASS IN Catclass','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CATCLASS','','Cat Class','','','','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','','','IN','Y','Y','','','','','1',401,'Extended Attributes Extract (ICC) - WC','EXIEAIV.CATCLASS IN Catclass');
xxeis.eis_rsc_ins.rcnh( 'Extended Attributes Extract (ICC) - WC',401,'EXIEAIV.CATMGT_CATEGORY_DESC IN Catmgt Category Desc','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CATMGT_CATEGORY_DESC','','Catmgt Category Desc','','','','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','','','IN','Y','Y','','','','','1',401,'Extended Attributes Extract (ICC) - WC','EXIEAIV.CATMGT_CATEGORY_DESC IN Catmgt Category Desc');
xxeis.eis_rsc_ins.rcnh( 'Extended Attributes Extract (ICC) - WC',401,'EXIEAIV.ICC IN Icc','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','ICC','','Item Catalog Category','','','','','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','','','','','','IN','Y','Y','','','','','1',401,'Extended Attributes Extract (ICC) - WC','EXIEAIV.ICC IN Icc');
--Inserting Report Sorts - Extended Attributes Extract (ICC) - WC
xxeis.eis_rsc_ins.rs( 'Extended Attributes Extract (ICC) - WC',401,'ICC','ASC','SA059956','1','');
--Inserting Report Triggers - Extended Attributes Extract (ICC) - WC
--inserting report templates - Extended Attributes Extract (ICC) - WC
--Inserting Report Portals - Extended Attributes Extract (ICC) - WC
--inserting report dashboards - Extended Attributes Extract (ICC) - WC
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Extended Attributes Extract (ICC) - WC','401','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','EIS_XXWC_INV_EXTEND_ATTR_ICC_V','N','');
--inserting report security - Extended Attributes Extract (ICC) - WC
xxeis.eis_rsc_ins.rsec( 'Extended Attributes Extract (ICC) - WC','401','','XXWC_DATA_MNGT_SC',401,'SA059956','','','');
--Inserting Report Pivots - Extended Attributes Extract (ICC) - WC
--Inserting Report   Version details- Extended Attributes Extract (ICC) - WC
xxeis.eis_rsc_ins.rv( 'Extended Attributes Extract (ICC) - WC','','Extended Attributes Extract (ICC) - WC','SA059956');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 401');
END IF;
END;
/
