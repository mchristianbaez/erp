--Report Name            : PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU(NEW)
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating View Data for PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU(NEW)
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_PUR_ACCRAL_LOB_BU_V
xxeis.eis_rs_ins.v( 'EIS_PUR_ACCRAL_LOB_BU_V',682,'','','','','PK059658','XXEIS','Eis Pur Accr Lob Bu V','EPALBV','','');
--Delete View Columns for EIS_PUR_ACCRAL_LOB_BU_V
xxeis.eis_rs_utility.delete_view_rows('EIS_PUR_ACCRAL_LOB_BU_V',682,FALSE);
--Inserting View Columns for EIS_PUR_ACCRAL_LOB_BU_V
xxeis.eis_rs_ins.vc( 'EIS_PUR_ACCRAL_LOB_BU_V','TOTAL_ACCRUALS',682,'Total Accruals','TOTAL_ACCRUALS','','','','PK059658','NUMBER','','','Total Accruals','','','');
xxeis.eis_rs_ins.vc( 'EIS_PUR_ACCRAL_LOB_BU_V','COOP',682,'Coop','COOP','','','','PK059658','NUMBER','','','Coop','','','');
xxeis.eis_rs_ins.vc( 'EIS_PUR_ACCRAL_LOB_BU_V','REBATE',682,'Rebate','REBATE','','','','PK059658','NUMBER','','','Rebate','','','');
xxeis.eis_rs_ins.vc( 'EIS_PUR_ACCRAL_LOB_BU_V','ACCRUAL_PURCHASES',682,'Accrual Purchases','ACCRUAL_PURCHASES','','','','PK059658','NUMBER','','','Accrual Purchases','','','');
xxeis.eis_rs_ins.vc( 'EIS_PUR_ACCRAL_LOB_BU_V','CALENDAR_PURCHASES',682,'Calendar Purchases','CALENDAR_PURCHASES','','','','PK059658','NUMBER','','','Calendar Purchases','','','');
xxeis.eis_rs_ins.vc( 'EIS_PUR_ACCRAL_LOB_BU_V','CAL_MONTH',682,'Cal Month','CAL_MONTH','','','','PK059658','VARCHAR2','','','Cal Month','','','');
xxeis.eis_rs_ins.vc( 'EIS_PUR_ACCRAL_LOB_BU_V','CAL_YEAR',682,'Cal Year','CAL_YEAR','','','','PK059658','NUMBER','','','Cal Year','','','');
xxeis.eis_rs_ins.vc( 'EIS_PUR_ACCRAL_LOB_BU_V','BU',682,'Bu','BU','','','','PK059658','VARCHAR2','','','Bu','','','');
xxeis.eis_rs_ins.vc( 'EIS_PUR_ACCRAL_LOB_BU_V','LOB',682,'Lob','LOB','','','','PK059658','VARCHAR2','','','Lob','','','');
xxeis.eis_rs_ins.vc( 'EIS_PUR_ACCRAL_LOB_BU_V','VENDOR_NAME',682,'Vendor Name','VENDOR_NAME','','','','PK059658','VARCHAR2','','','Vendor Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_PUR_ACCRAL_LOB_BU_V','MVID',682,'Mvid','MVID','','','','PK059658','VARCHAR2','','','Mvid','','','');
xxeis.eis_rs_ins.vc( 'EIS_PUR_ACCRAL_LOB_BU_V','AGREEMENT_YEAR',682,'Agreement Year','AGREEMENT_YEAR','','','','PK059658','NUMBER','','','Agreement Year','','','');
xxeis.eis_rs_ins.vc( 'EIS_PUR_ACCRAL_LOB_BU_V','PROCESS_ID',682,'Process Id','PROCESS_ID','','','','PK059658','NUMBER','','','Process Id','','','');
--Inserting View Components for EIS_PUR_ACCRAL_LOB_BU_V
--Inserting View Component Joins for EIS_PUR_ACCRAL_LOB_BU_V
END;
/
set scan on define on
prompt Creating Report LOV Data for PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU(NEW)
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU(NEW)
xxeis.eis_rs_ins.lov( 682,'select distinct nvl(attribute7, ''NULL'') agreement_year
from apps.QP_LIST_HEADERS_VL','','LOV AGREEMENT_YEAR','LOV attribute7 from apps.QP_LIST_HEADERS_VL  table is the agreement year','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 682,'select distinct nvl(attribute7, ''NULL'') agreement_year
from apps.QP_LIST_HEADERS_VL','','LOV AGREEMENT_YEAR','LOV attribute7 from apps.QP_LIST_HEADERS_VL  table is the agreement year','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 682,'select distinct party_name
from ar.HZ_PARTIES
where ATTRIBUTE1 = ''HDS_LOB''
and party_name not in (''PLUMBING null'', ''INDUSTRIAL PVF'')','','HDS LOB_NAME','LOV party_name From ar.HZ_PARTIES is the LOB','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 682,'select distinct name
from ozf.OZF_TIME_ENT_PERIOD','','LOV PERIOD','LOV NAME FROM ozf.OZF_TIME_ENT_PERIOD is the Period','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 682,'SELECT customer_attribute2 MVID
  FROM XXCUS.xxcus_rebate_customers
  WHERE 1=1
  AND party_attribute1=''HDS_MVID''
  order by 1','','MVID','MVID number from XXCUS.XXCUS_REBATE_CUSTOMERS','DV003828',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU(NEW)
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU(NEW)
xxeis.eis_rs_utility.delete_report_rows( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU(NEW)' );
--Inserting Report - PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU(NEW)
xxeis.eis_rs_ins.r( 682,'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU(NEW)','','copy report PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU','','','','PK059658','EIS_PUR_ACCRAL_LOB_BU_V','Y','','','PK059658','','N','ADHOC - SUMMARY ACCRUAL DETAILS','','CSV,EXCEL,','');
--Inserting Report Columns - PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU(NEW)
xxeis.eis_rs_ins.rc( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU(NEW)',682,'AGREEMENT_YEAR','Agreement Year','Agreement Year','','~~~','default','','1','N','','','','','','','','PK059658','N','N','','EIS_PUR_ACCRAL_LOB_BU_V','','');
xxeis.eis_rs_ins.rc( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU(NEW)',682,'BU','Bu','Bu','','','default','','5','N','','','','','','','','PK059658','N','N','','EIS_PUR_ACCRAL_LOB_BU_V','','');
xxeis.eis_rs_ins.rc( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU(NEW)',682,'CALENDAR_PURCHASES','Calendar Purchases','Calendar Purchases','','~.~,~2','default','','8','N','','','','','','','','PK059658','N','N','','EIS_PUR_ACCRAL_LOB_BU_V','','');
xxeis.eis_rs_ins.rc( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU(NEW)',682,'CAL_MONTH','Cal Month','Cal Month','','','default','','7','N','','','','','','','','PK059658','N','N','','EIS_PUR_ACCRAL_LOB_BU_V','','');
xxeis.eis_rs_ins.rc( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU(NEW)',682,'CAL_YEAR','Cal Year','Cal Year','','~~~','default','','6','N','','','','','','','','PK059658','N','N','','EIS_PUR_ACCRAL_LOB_BU_V','','');
xxeis.eis_rs_ins.rc( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU(NEW)',682,'COOP','Coop','Coop','','~.~,~2','default','','10','N','','','','','','','','PK059658','N','N','','EIS_PUR_ACCRAL_LOB_BU_V','','');
xxeis.eis_rs_ins.rc( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU(NEW)',682,'LOB','Lob','Lob','','','default','','4','N','','','','','','','','PK059658','N','N','','EIS_PUR_ACCRAL_LOB_BU_V','','');
xxeis.eis_rs_ins.rc( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU(NEW)',682,'MVID','Mvid','Mvid','','','default','','2','N','','','','','','','','PK059658','N','N','','EIS_PUR_ACCRAL_LOB_BU_V','','');
xxeis.eis_rs_ins.rc( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU(NEW)',682,'REBATE','Rebate','Rebate','','~.~,~2','default','','9','N','','','','','','','','PK059658','N','N','','EIS_PUR_ACCRAL_LOB_BU_V','','');
xxeis.eis_rs_ins.rc( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU(NEW)',682,'TOTAL_ACCRUALS','Total Accruals','Total Accruals','','~.~,~2','default','','11','N','','','','','','','','PK059658','N','N','','EIS_PUR_ACCRAL_LOB_BU_V','','');
xxeis.eis_rs_ins.rc( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU(NEW)',682,'VENDOR_NAME','Vendor Name','Vendor Name','','','default','','3','N','','','','','','','','PK059658','N','N','','EIS_PUR_ACCRAL_LOB_BU_V','','');
--Inserting Report Parameters - PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU(NEW)
xxeis.eis_rs_ins.rp( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU(NEW)',682,'Agreement Year','Agreement Year','','IN','LOV AGREEMENT_YEAR','','VARCHAR2','N','Y','5','','N','CONSTANT','PK059658','Y','N','','','');
xxeis.eis_rs_ins.rp( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU(NEW)',682,'Cal Month','Cal Month','','IN','LOV PERIOD','','VARCHAR2','N','Y','2','','N','CONSTANT','PK059658','Y','N','','','');
xxeis.eis_rs_ins.rp( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU(NEW)',682,'Calendar Year','Calendar Year','','IN','LOV AGREEMENT_YEAR','','VARCHAR2','Y','Y','1','','N','CONSTANT','PK059658','Y','N','','','');
xxeis.eis_rs_ins.rp( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU(NEW)',682,'Lob','Lob','','IN','HDS LOB_NAME','','VARCHAR2','N','Y','3','','N','CONSTANT','PK059658','Y','N','','','');
xxeis.eis_rs_ins.rp( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU(NEW)',682,'Mvid','Mvid','','IN','MVID','','VARCHAR2','N','Y','4','','N','CONSTANT','PK059658','Y','N','','','');
--Inserting Report Conditions - PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU(NEW)
xxeis.eis_rs_ins.rcn( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU(NEW)',682,'','','','','and PROCESS_ID = :SYSTEM.PROCESS_ID','Y','1','','PK059658');
--Inserting Report Sorts - PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU(NEW)
xxeis.eis_rs_ins.rs( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU(NEW)',682,'AGREEMENT_YEAR','ASC','PK059658','1','');
xxeis.eis_rs_ins.rs( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU(NEW)',682,'MVID','ASC','PK059658','2','');
xxeis.eis_rs_ins.rs( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU(NEW)',682,'LOB','ASC','PK059658','3','');
xxeis.eis_rs_ins.rs( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU(NEW)',682,'BU','ASC','PK059658','4','');
xxeis.eis_rs_ins.rs( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU(NEW)',682,'CAL_YEAR','ASC','PK059658','5','');
xxeis.eis_rs_ins.rs( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU(NEW)',682,'CAL_MONTH','ASC','PK059658','6','');
--Inserting Report Triggers - PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU(NEW)
xxeis.eis_rs_ins.rt( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU(NEW)',682,'BEGIN
XXEIS.EIS_XXWC_PUR_ACC_lob_bu_PKG.GET_ACC_lob_bu_DTLS (p_process_id  =>  :SYSTEM.PROCESS_ID,
                            P_Cal_Month  => :Cal Month,
                            p_Lob => :Lob,
                            P_Mvid =>	:Mvid,
                            P_Calendar_Year 	 =>	:Calendar Year,
                            P_Agreement_Year   =>	:Agreement Year
	);
END;','B','Y','PK059658');
xxeis.eis_rs_ins.rt( 'PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU(NEW)',682,'begin
XXEIS.eis_xxwc_pur_acc_lob_bu_pkg.clear_temp_tables(P_PROCESS_ID=> :SYSTEM.PROCESS_ID);
end;','A','Y','PK059658');
--Inserting Report Templates - PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU(NEW)
--Inserting Report Portals - PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU(NEW)
--Inserting Report Dashboards - PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU(NEW)
--Inserting Report Security - PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU(NEW)
--Inserting Report Pivots - PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU(NEW)
END;
/
set scan on define on
