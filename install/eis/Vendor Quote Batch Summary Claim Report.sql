--Report Name            : Vendor Quote Batch Summary and Claim Report
--Import Instructions : 1. If RTF template needs to be uploaded to report automatically then move the RTF template to physical path of APPS_DATA_FILE_DIR oracle directory. Check dba_directories view to get the physical path of oracle directory. 
--                         (If you want to use any other oracle directory other than APPS_DATA_FILE_DIR then replace APPS_DATA_FILE_DIR with your oracle directory name below.)
--                      2. Make sure XXEIS schema has read/write permissions to above used oracle directory and give full permissions to the directory at OS level.
--                      3. Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
exec xxeis.eis_rsc_ins.g_rtf_dir := 'APPS_DATA_FILE_DIR' ;
prompt Creating Object Data EIS_XXWC_OM_VENDOR_QUOTE_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_OM_VENDOR_QUOTE_V
xxeis.eis_rsc_ins.v( 'EIS_XXWC_OM_VENDOR_QUOTE_V',660,'','','','','ANONYMOUS','XXEIS','Eis Xxwc Om Vendor Batch V','EXOVBV','','','VIEW','US','','','');
--Delete Object Columns for EIS_XXWC_OM_VENDOR_QUOTE_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_OM_VENDOR_QUOTE_V',660,FALSE);
--Inserting Object Columns for EIS_XXWC_OM_VENDOR_QUOTE_V
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','CREATED_BY',660,'Created By','CREATED_BY','','','','ANONYMOUS','VARCHAR2','','','Created By','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','LOC',660,'Loc','LOC','','','','ANONYMOUS','VARCHAR2','','','Loc','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','GL_STRING',660,'Gl String','GL_STRING','','','','ANONYMOUS','VARCHAR2','','','Gl String','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','GL_CODING',660,'Gl Coding','GL_CODING','','','','ANONYMOUS','VARCHAR2','','','Gl Coding','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','REBATE',660,'Rebate','REBATE','','~T~D~2','','ANONYMOUS','NUMBER','','','Rebate','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','UNIT_CLAIM_VALUE',660,'Unit Claim Value','UNIT_CLAIM_VALUE','','~T~D~2','','ANONYMOUS','NUMBER','','','Unit Claim Value','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','PO_COST',660,'Po Cost','PO_COST','','~T~D~2','','ANONYMOUS','NUMBER','','','Po Cost','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','DESCRIPTION',660,'Description','DESCRIPTION','','','','ANONYMOUS','VARCHAR2','','','Description','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','UOM',660,'Uom','UOM','','','','ANONYMOUS','VARCHAR2','','','Uom','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','PART_NUMBER',660,'Part Number','PART_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Part Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','INVOICE_DATE',660,'Invoice Date','INVOICE_DATE','','','','ANONYMOUS','DATE','','','Invoice Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','INVOICE_NUMBER',660,'Invoice Number','INVOICE_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Invoice Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','VENDOR_NAME',660,'Vendor Name','VENDOR_NAME','','','','ANONYMOUS','VARCHAR2','','','Vendor Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','VENDOR_NUMBER',660,'Vendor Number','VENDOR_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Vendor Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','JOB_NUMBER',660,'Job Number','JOB_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Job Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','JOB_NAME',660,'Job Name','JOB_NAME','','','','ANONYMOUS','VARCHAR2','','','Job Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','MASTER_ACCOUNT_NUMBER',660,'Master Account Number','MASTER_ACCOUNT_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Master Account Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','MASTER_ACCOUNT_NAME',660,'Master Account Name','MASTER_ACCOUNT_NAME','','','','ANONYMOUS','VARCHAR2','','','Master Account Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','SALESPESON_NUMBER',660,'Salespeson Number','SALESPESON_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Salespeson Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','SALESPERSON_NAME',660,'Salesperson Name','SALESPERSON_NAME','','','','ANONYMOUS','VARCHAR2','','','Salesperson Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','SPECIAL_COST',660,'Special Cost','SPECIAL_COST','','~T~D~2','','ANONYMOUS','NUMBER','','','Special Cost','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','LOCATION',660,'Location','LOCATION','','','','ANONYMOUS','VARCHAR2','','','Location','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','QTY',660,'Qty','QTY','','~T~D~2','','ANONYMOUS','NUMBER','','','Qty','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','ORACLE_QUOTE_NUMBER',660,'Oracle Quote Number','ORACLE_QUOTE_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Oracle Quote Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','BPA',660,' Bpa','BPA','','','','ANONYMOUS','NUMBER','','',' Bpa','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','ORDER_NUMBER',660,'Order Number','ORDER_NUMBER','','','','ANONYMOUS','NUMBER','','','Order Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','AVERAGE_COST',660,'Average Cost','AVERAGE_COST','','~T~D~2','','ANONYMOUS','NUMBER','','','Average Cost','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','ORGANIZATION_NAME',660,'Organization Name','ORGANIZATION_NAME','','','','ANONYMOUS','VARCHAR2','','','Organization Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','PROCESS_ID',660,'Process Id','PROCESS_ID','','','','ANONYMOUS','NUMBER','','','Process Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','LIST_PRICE_PER_UNIT',660,'List Price Per Unit','LIST_PRICE_PER_UNIT','','~T~D~2','','ANONYMOUS','NUMBER','','','List Price Per Unit','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','CREATION_DATE',660,'Creation Date','CREATION_DATE','','','','ANONYMOUS','DATE','','','Creation Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','NAME',660,'Name','NAME','','','','ANONYMOUS','VARCHAR2','','','Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','CUSTOMER_TRX_ID',660,'Customer Trx Id','CUSTOMER_TRX_ID','','','','ANONYMOUS','NUMBER','','','Customer Trx Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','LINE_NUMBER',660,'Line Number','LINE_NUMBER','','','','ANONYMOUS','NUMBER','','','Line Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','SOURCE_TYPE_CODE',660,'Source Type Code','SOURCE_TYPE_CODE','','','','ANONYMOUS','VARCHAR2','','','Source Type Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','COMMON_OUTPUT_ID',660,'Common Output Id','COMMON_OUTPUT_ID','','','','ANONYMOUS','NUMBER','','','Common Output Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','PO_TRANS_COST',660,'Po Trans Cost','PO_TRANS_COST','','','','ANONYMOUS','NUMBER','','','Po Trans Cost','','','','','');
--Inserting Object Components for EIS_XXWC_OM_VENDOR_QUOTE_V
--Inserting Object Component Joins for EIS_XXWC_OM_VENDOR_QUOTE_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
prompt Creating Report LOV Data for Vendor Quote Batch Summary and Claim Report
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - Vendor Quote Batch Summary and Claim Report
xxeis.eis_rsc_ins.lov( 660,'select nvl( party.party_name,CUST_ACCT.account_name) customer_name,CUST_ACCT.ACCOUNT_NUMBER
from  HZ_PARTIES PARTY, HZ_CUST_ACCOUNTS CUST_ACCT where CUST_ACCT.PARTY_ID = PARTY.PARTY_ID AND  PARTY.status=''A''
','','OM CUSTOMER NAME','This gives the Customer Name','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'SELECT DISTINCT VENDOR_NAME
    FROM PO_VENDORS POV','','OM Vendor Name LOV','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'select distinct segment1 Vendor_Number from po_vendors','','OM Vendor Number LOV','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
prompt Creating Report Data for Vendor Quote Batch Summary and Claim Report
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Vendor Quote Batch Summary and Claim Report
xxeis.eis_rsc_utility.delete_report_rows( 'Vendor Quote Batch Summary and Claim Report',660 );
--Inserting Report - Vendor Quote Batch Summary and Claim Report
xxeis.eis_rsc_ins.r( 660,'Vendor Quote Batch Summary and Claim Report','','Provides order, credit customer and vendor details for all closed orders associated with a Vendor Quote.','','','','SA059956','EIS_XXWC_OM_VENDOR_QUOTE_V','Y','','','SA059956','','Y','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N','','','','','','N','','US','','','','','','','','','','','','','','','','');
--Inserting Report Columns - Vendor Quote Batch Summary and Claim Report
xxeis.eis_rsc_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'CREATED_BY','Created By','Created By','','','default','','27','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'DESCRIPTION','Description','Description','','','default','','15','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'INVOICE_DATE','Invoice Date','Invoice Date','','','default','','12','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'INVOICE_NUMBER','Invoice Number','Invoice Number','','','default','','10','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'JOB_NAME','Job Name','Job Name','','','default','','6','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'JOB_NUMBER','Site Number','Job Number','','','default','','5','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'LOC','Loc','Loc','','','default','','29','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'MASTER_ACCOUNT_NAME','Master Account Name','Master Account Name','','','default','','4','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'MASTER_ACCOUNT_NUMBER','Master Account Number','Master Account Number','','','default','','3','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'PART_NUMBER','Part Number','Part Number','','','default','','14','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'REBATE','Rebate','Rebate','','~T~D~5','default','','26','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'SALESPERSON_NAME','Salesperson Name','Salesperson Name','','','default','','2','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'SALESPESON_NUMBER','Salespeson Number','Salespeson Number','','','default','','1','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'UOM','Uom','Uom','','','default','','13','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'VENDOR_NAME','Vendor Name','Vendor Name','','','default','','8','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'VENDOR_NUMBER','Vendor Number','Vendor Number','','','default','','7','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'GL_CODING','GL Coding','Gl Coding','','','default','','28','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'PO_COST','Po Cost','Po Cost','','~T~D~5','default','','20','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'UNIT_CLAIM_VALUE','Unit Claim Value','Unit Claim Value','','~T~D~5','default','','24','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'GL_STRING','GL String','Gl String','','','default','','30','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'LOCATION','Location','Location','','','default','','16','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'QTY','Qty','Qty','','~~~','default','','25','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'ORACLE_QUOTE_NUMBER','Vendor Quote Number','Oracle Quote Number','','','default','','9','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'SPECIAL_COST','Special Cost','Special Cost','','~T~D~5','default','','22','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'BPA','Best Buy',' Bpa','','~T~D~5','default','','18','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'AVERAGE_COST','Average Cost','Average Cost','','~T~D~0','default','','23','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'ORDER_NUMBER','Order Number','Order Number','','~~~','default','','31','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'ORGANIZATION_NAME','Organization Name','Organization Name','','','default','','17','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'Stock/Direct','Stock/Direct','Organization Name','VARCHAR2','','default','','32','Y','Y','','','','','','(DECODE(Source_Type_code,''INTERNAL'',''Stock'',''EXTERNAL'',''Direct''))','SA059956','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'LIST_PRICE_PER_UNIT','Last Item Cost','List Price Per Unit','','~T~D~5','default','','19','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'LINE_NUMBER','Transaction Line Number','Line Number','','~~~','default','','11','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'PO_TRANS_COST','PO Trans Cost','Po Trans Cost','','~T~D~5','default','','21','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','','','US','','');
--Inserting Report Parameters - Vendor Quote Batch Summary and Claim Report
xxeis.eis_rsc_ins.rp( 'Vendor Quote Batch Summary and Claim Report',660,'Vendor Name','Vendor Name','VENDOR_NAME','IN','OM Vendor Name LOV','','VARCHAR2','N','Y','3','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_OM_VENDOR_QUOTE_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Vendor Quote Batch Summary and Claim Report',660,'Vendor Number','Vendor Number','VENDOR_NUMBER','IN','OM Vendor Number LOV','','VARCHAR2','N','Y','4','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_OM_VENDOR_QUOTE_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Vendor Quote Batch Summary and Claim Report',660,'Bill To Customer','Bill To Customer','MASTER_ACCOUNT_NAME','IN','OM CUSTOMER NAME','','VARCHAR2','N','Y','5','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_OM_VENDOR_QUOTE_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Vendor Quote Batch Summary and Claim Report',660,'Invoice Date From','Invoice Date From','','IN','','','DATE','Y','Y','1','','N','CONSTANT','SA059956','Y','N','','Start Date','','EIS_XXWC_OM_VENDOR_QUOTE_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Vendor Quote Batch Summary and Claim Report',660,'Invoice Date To','Invoice Date To','','IN','','','DATE','Y','Y','2','','N','CONSTANT','SA059956','Y','N','','End Date','','EIS_XXWC_OM_VENDOR_QUOTE_V','','','US','');
--Inserting Dependent Parameters - Vendor Quote Batch Summary and Claim Report
--Inserting Report Conditions - Vendor Quote Batch Summary and Claim Report
xxeis.eis_rsc_ins.rcnh( 'Vendor Quote Batch Summary and Claim Report',660,'MASTER_ACCOUNT_NAME IN :Bill To Customer ','SIMPLE','','','Y','','1');
xxeis.eis_rsc_ins.rcnd( '','','MASTER_ACCOUNT_NAME','','Bill To Customer','','','','','EIS_XXWC_OM_VENDOR_QUOTE_V','','','','','','IN','Y','Y','','','','','1',660,'Vendor Quote Batch Summary and Claim Report','MASTER_ACCOUNT_NAME IN :Bill To Customer ');
xxeis.eis_rsc_ins.rcnh( 'Vendor Quote Batch Summary and Claim Report',660,'PROCESS_ID IN :SYSTEM.PROCESS_ID ','ADVANCED','','1#$#','Y','','2');
xxeis.eis_rsc_ins.rcnd( '','','PROCESS_ID','','','','','','','EIS_XXWC_OM_VENDOR_QUOTE_V','','','','','','IN','Y','N','',':SYSTEM.PROCESS_ID','','','1',660,'Vendor Quote Batch Summary and Claim Report','PROCESS_ID IN :SYSTEM.PROCESS_ID ');
xxeis.eis_rsc_ins.rcnh( 'Vendor Quote Batch Summary and Claim Report',660,'VENDOR_NAME IN :Vendor Name ','SIMPLE','','','Y','','3');
xxeis.eis_rsc_ins.rcnd( '','','VENDOR_NAME','','Vendor Name','','','','','EIS_XXWC_OM_VENDOR_QUOTE_V','','','','','','IN','Y','Y','','','','','1',660,'Vendor Quote Batch Summary and Claim Report','VENDOR_NAME IN :Vendor Name ');
xxeis.eis_rsc_ins.rcnh( 'Vendor Quote Batch Summary and Claim Report',660,'VENDOR_NUMBER IN :Vendor Number ','SIMPLE','','','Y','','4');
xxeis.eis_rsc_ins.rcnd( '','','VENDOR_NUMBER','','Vendor Number','','','','','EIS_XXWC_OM_VENDOR_QUOTE_V','','','','','','IN','Y','Y','','','','','1',660,'Vendor Quote Batch Summary and Claim Report','VENDOR_NUMBER IN :Vendor Number ');
--Inserting Report Sorts - Vendor Quote Batch Summary and Claim Report
--Inserting Report Triggers - Vendor Quote Batch Summary and Claim Report
xxeis.eis_rsc_ins.rt( 'Vendor Quote Batch Summary and Claim Report',660,'begin
XXEIS.XXWC_VENDOR_QUOTE_TST_PKG.GET_HEADER_ID(P_PROCESS_ID=>:SYSTEM.PROCESS_ID,
P_INV_START_DATE=>:Invoice Date From,
P_INV_END_DATE=>:Invoice Date To);
END;
','B','Y','SA059956','AQ');
--inserting report templates - Vendor Quote Batch Summary and Claim Report
xxeis.eis_rsc_ins.r_tem( 'Vendor Quote Batch Summary and Claim Report','Vendor Quote Batch Summary and Claim Report','Seeded Template for Vendor Quote Batch Summary and Claim Report','','','','','','','','','','','Vendor Quote Batch Summary and Claim Report.rtf','SA059956','X','','','Y','Y','N','N');
xxeis.eis_rsc_ins.r_tem( 'Vendor Quote Batch Summary and Claim Report','Vendor Quote Report','Seeded template for Vendor Quote Report','','','','','','','','','','','Vendor Quote Report.rtf','SA059956','X','','','Y','Y','N','N');
--Inserting Report Portals - Vendor Quote Batch Summary and Claim Report
--inserting report dashboards - Vendor Quote Batch Summary and Claim Report
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Vendor Quote Batch Summary and Claim Report','660','EIS_XXWC_OM_VENDOR_QUOTE_V','EIS_XXWC_OM_VENDOR_QUOTE_V','N','');
--inserting report security - Vendor Quote Batch Summary and Claim Report
xxeis.eis_rsc_ins.rsec( 'Vendor Quote Batch Summary and Claim Report','660','','',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Vendor Quote Batch Summary and Claim Report','660','','XXWC_ORDER_MGMT_PRICING_FULL',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Vendor Quote Batch Summary and Claim Report','660','','XXWC_ORDER_MGMT_PRICING_LTD',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Vendor Quote Batch Summary and Claim Report','660','','XXWC_ORDER_MGMT_PRICING_STD',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Vendor Quote Batch Summary and Claim Report','660','','XXWC_ORDER_MGMT_READ_ONLY',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Vendor Quote Batch Summary and Claim Report','660','','',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Vendor Quote Batch Summary and Claim Report','20005','','',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Vendor Quote Batch Summary and Claim Report','660','','ORDER_MGMT_SUPER_USER',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Vendor Quote Batch Summary and Claim Report','660','','XXWC_ORDER_MGMT_PRICING_SUPER',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Vendor Quote Batch Summary and Claim Report','660','','XXWC_ORDER_MGMT_READ_SHIPPING',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Vendor Quote Batch Summary and Claim Report','660','','XXWC_AO_OEENTRY_PO_RPT',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Vendor Quote Batch Summary and Claim Report','660','','XXWC_AO_OEENTRY_REC',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Vendor Quote Batch Summary and Claim Report','660','','XXWC_AO_OEENTRY',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Vendor Quote Batch Summary and Claim Report','20005','','XXWC_VIEW_ALL_EIS_REPORTS',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Vendor Quote Batch Summary and Claim Report','701','','CLN_OM_3A6_ADMINISTRATOR',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Vendor Quote Batch Summary and Claim Report','660','','XXWC_RENTAL_OM_PRICING_SUPER',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Vendor Quote Batch Summary and Claim Report','661','','XXWC_PRICING_MANAGER',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Vendor Quote Batch Summary and Claim Report','','SG019472','',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Vendor Quote Batch Summary and Claim Report','660','','XXWC_ORDER_MGMT_SUPER_USER',660,'SA059956','','','');
--Inserting Report Pivots - Vendor Quote Batch Summary and Claim Report
xxeis.eis_rsc_ins.rpivot( 'Vendor Quote Batch Summary and Claim Report',660,'Pivot','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
--Inserting Report Summary Calculation Columns For Pivot- Pivot
--Inserting Report Distribution Details 
--Inserting Report Distribution Details 
--Inserting Report SubTotals Details 
--Inserting Report aggregations 
--Inserting Report   Version details- Vendor Quote Batch Summary and Claim Report
xxeis.eis_rsc_ins.rv( 'Vendor Quote Batch Summary and Claim Report','','Vendor Quote Batch Summary and Claim Report','SA059956','30-OCT-2017');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
