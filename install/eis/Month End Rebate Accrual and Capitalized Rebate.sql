--Report Name            : Month End Rebate Accrual and Capitalized Rebate
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View XXEIS_751492_ZRENHS_V
CREATE OR REPLACE VIEW APPS.XXEIS_751492_ZRENHS_V (LOB, PERIOD, POSTED_BRANCH, FISCAL_MONTH, PURCHASES, REBATE_INCOME, COOP_INCOME, TOTAL_INCOME)
AS
  SELECT BILL_TO_PARTY_NAME_LOB LOB ,
    XAEH_PERIOD_NAME PERIOD ,
    POSTED_BU_BRANCH POSTED_BRANCH,
    fiscal_month,
    SUM(
    CASE
      WHEN UTILIZATION_TYPE='ACCRUAL'
      THEN QUANTITY*SELLING_PRICE
      ELSE 0
    END) PURCHASES ,
    SUM (
    CASE
      WHEN ACTIVITY_MEDIA_NAME NOT IN ('Coop Minimum','Coop')
      THEN NVL(UTIL_ACCTD_AMOUNT,0)
      ELSE 0
    END) REBATE_INCOME ,
    SUM (
    CASE
      WHEN ACTIVITY_MEDIA_NAME IN ('Coop Minimum','Coop')
      THEN NVL(UTIL_ACCTD_AMOUNT,0)
      ELSE 0
    END) COOP_INCOME ,
    SUM(UTIL_ACCTD_AMOUNT) TOTAL_INCOME
  FROM XXCUS.XXCUS_OZF_XLA_ACCRUALS_B
  WHERE 1                     =1
  AND XAEH_GL_XFER_STATUS_CODE='Y'
  GROUP BY BILL_TO_PARTY_NAME_LOB ,
    XAEH_PERIOD_NAME ,
    POSTED_BU_BRANCH,
    fiscal_month
/
prompt Creating Object Data XXEIS_751492_ZRENHS_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(682);
IF mod_exist = 'Y' THEN 
--Inserting Object XXEIS_751492_ZRENHS_V
xxeis.eis_rsc_ins.v( 'XXEIS_751492_ZRENHS_V',682,'Paste SQL View for Month End Rebate Accrual and Capitalized Rebate v1','1.0','','','DV003828','APPS','Month End Rebate Accrual and Capitalized Rebate v1 View','X7ZV','','','VIEW','US','','');
--Delete Object Columns for XXEIS_751492_ZRENHS_V
xxeis.eis_rsc_utility.delete_view_rows('XXEIS_751492_ZRENHS_V',682,FALSE);
--Inserting Object Columns for XXEIS_751492_ZRENHS_V
xxeis.eis_rsc_ins.vc( 'XXEIS_751492_ZRENHS_V','LOB',682,'','','','','','DV003828','VARCHAR2','','','Lob','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_751492_ZRENHS_V','PERIOD',682,'','','','','','DV003828','VARCHAR2','','','Period','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_751492_ZRENHS_V','PURCHASES',682,'','','','','','DV003828','NUMBER','','','Purchases','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_751492_ZRENHS_V','REBATE_INCOME',682,'','','','','','DV003828','NUMBER','','','Rebate Income','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_751492_ZRENHS_V','COOP_INCOME',682,'','','','','','DV003828','NUMBER','','','Coop Income','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_751492_ZRENHS_V','POSTED_BRANCH',682,'','','','','','DV003828','VARCHAR2','','','Posted Branch','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_751492_ZRENHS_V','TOTAL_INCOME',682,'','','','~T~D~2','','DV003828','NUMBER','','','Total Income','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_751492_ZRENHS_V','FISCAL_MONTH',682,'','','','','','DV003828','VARCHAR2','','','Fiscal Month','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_751492_ZRENHS_V','LOB_BRANCH',682,'Lob Branch','Lob Branch','','','','DV003828','','','','Lob Branch','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_751492_ZRENHS_V','BRANCH',682,'Branch','Branch','','','','DV003828','','','','Branch','','','','US');
--Inserting Object Components for XXEIS_751492_ZRENHS_V
--Inserting Object Component Joins for XXEIS_751492_ZRENHS_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 682');
END IF;
END;
/
prompt Creating Report LOV Data for Month End Rebate Accrual and Capitalized Rebate
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(682);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - Month End Rebate Accrual and Capitalized Rebate
xxeis.eis_rsc_ins.lov( 682,'select distinct party_name
from ar.HZ_PARTIES
where ATTRIBUTE1 = ''HDS_LOB''
and party_name not in (''PLUMBING null'', ''INDUSTRIAL PVF'')','','HDS LOB_NAME','LOV party_name From ar.HZ_PARTIES is the LOB','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 682,'select distinct name
from ozf.OZF_TIME_ENT_PERIOD','','LOV PERIOD','LOV NAME FROM ozf.OZF_TIME_ENT_PERIOD is the Period','ANONYMOUS',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 682');
END IF;
END;
/
prompt Creating Report Data for Month End Rebate Accrual and Capitalized Rebate
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(682);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Month End Rebate Accrual and Capitalized Rebate
xxeis.eis_rsc_utility.delete_report_rows( 'Month End Rebate Accrual and Capitalized Rebate' );
--Inserting Report - Month End Rebate Accrual and Capitalized Rebate
xxeis.eis_rsc_ins.r( 682,'Month End Rebate Accrual and Capitalized Rebate','','Spend and Accruals by Fiscal period, lob, branch .','','','','KP059700','XXEIS_751492_ZRENHS_V','Y','','   SELECT 
        BILL_TO_PARTY_NAME_LOB LOB ,
        XAEH_PERIOD_NAME PERIOD ,
        POSTED_BU_BRANCH POSTED_BRANCH,
        fiscal_month,
        SUM(CASE WHEN UTILIZATION_TYPE=''ACCRUAL'' THEN QUANTITY*SELLING_PRICE ELSE 0 END) PURCHASES ,
   SUM (CASE WHEN ACTIVITY_MEDIA_NAME NOT IN (''Coop Minimum'',''Coop'')  THEN NVL(UTIL_ACCTD_AMOUNT,0) ELSE 0  END) REBATE_INCOME ,
        SUM (CASE WHEN ACTIVITY_MEDIA_NAME IN (''Coop Minimum'',''Coop'')    THEN NVL(UTIL_ACCTD_AMOUNT,0)   ELSE 0   END) COOP_INCOME ,
        SUM(UTIL_ACCTD_AMOUNT) TOTAL_INCOME
  FROM 
        XXCUS.XXCUS_OZF_XLA_ACCRUALS_B                
  WHERE 1=1
        AND XAEH_GL_XFER_STATUS_CODE=''Y''       
  GROUP BY 
        BILL_TO_PARTY_NAME_LOB , 
        XAEH_PERIOD_NAME ,       
        POSTED_BU_BRANCH,
        fiscal_month
','KP059700','','N','MONTH-END','','CSV,Pivot Excel,EXCEL,','N','','','','','','','APPS','US','','','','');
--Inserting Report Columns - Month End Rebate Accrual and Capitalized Rebate
xxeis.eis_rsc_ins.rc( 'Month End Rebate Accrual and Capitalized Rebate',682,'PERIOD','Period','','','','default','','1','N','','','','','','','','KP059700','N','N','','XXEIS_751492_ZRENHS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Month End Rebate Accrual and Capitalized Rebate',682,'LOB','Lob','','','','default','','2','N','','','','','','','','KP059700','N','N','','XXEIS_751492_ZRENHS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Month End Rebate Accrual and Capitalized Rebate',682,'COOP_INCOME','Coop Income','','','~,~.~2','default','','5','N','','','','','','','','KP059700','N','N','','XXEIS_751492_ZRENHS_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Month End Rebate Accrual and Capitalized Rebate',682,'REBATE_INCOME','Rebate Income','','','~,~.~2','default','','4','N','','','','','','','','KP059700','N','N','','XXEIS_751492_ZRENHS_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Month End Rebate Accrual and Capitalized Rebate',682,'TOTAL_INCOME','Total Income','','','~,~.~2','default','','6','N','','','','','','','','KP059700','N','N','','XXEIS_751492_ZRENHS_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Month End Rebate Accrual and Capitalized Rebate',682,'POSTED_BRANCH','Posted Branch','','','','','','3','N','','','','','','','','KP059700','N','N','','XXEIS_751492_ZRENHS_V','','','GROUP_BY','US','');
--Inserting Report Parameters - Month End Rebate Accrual and Capitalized Rebate
xxeis.eis_rsc_ins.rp( 'Month End Rebate Accrual and Capitalized Rebate',682,'LOB','','LOB','IN','HDS LOB_NAME','','VARCHAR2','N','Y','2','Y','Y','CONSTANT','KP059700','Y','N','','','','XXEIS_751492_ZRENHS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Month End Rebate Accrual and Capitalized Rebate',682,'Fiscal Period','','PERIOD','IN','LOV PERIOD','','VARCHAR2','Y','Y','1','Y','Y','CONSTANT','KP059700','Y','N','','','','XXEIS_751492_ZRENHS_V','','','US','');
--Inserting Dependent Parameters - Month End Rebate Accrual and Capitalized Rebate
--Inserting Report Conditions - Month End Rebate Accrual and Capitalized Rebate
xxeis.eis_rsc_ins.rcnh( 'Month End Rebate Accrual and Capitalized Rebate',682,'LOB IN :LOB ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','LOB','','LOB','','','','','XXEIS_751492_ZRENHS_V','','','','','','IN','Y','Y','','','','','1',682,'Month End Rebate Accrual and Capitalized Rebate','LOB IN :LOB ');
xxeis.eis_rsc_ins.rcnh( 'Month End Rebate Accrual and Capitalized Rebate',682,'PERIOD IN :Fiscal Period ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','PERIOD','','Fiscal Period','','','','','XXEIS_751492_ZRENHS_V','','','','','','IN','Y','Y','','','','','1',682,'Month End Rebate Accrual and Capitalized Rebate','PERIOD IN :Fiscal Period ');
xxeis.eis_rsc_ins.rcnh( 'Month End Rebate Accrual and Capitalized Rebate',682,'Free Text ','FREE_TEXT','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','','Y','','','','','and fiscal_month=UPPER(substr(:Fiscal Period,1,3))','1',682,'Month End Rebate Accrual and Capitalized Rebate','Free Text ');
--Inserting Report Sorts - Month End Rebate Accrual and Capitalized Rebate
xxeis.eis_rsc_ins.rs( 'Month End Rebate Accrual and Capitalized Rebate',682,'LOB','ASC','KP059700','1','');
xxeis.eis_rsc_ins.rs( 'Month End Rebate Accrual and Capitalized Rebate',682,'POSTED_BRANCH','ASC','KP059700','2','');
--Inserting Report Triggers - Month End Rebate Accrual and Capitalized Rebate
--inserting report templates - Month End Rebate Accrual and Capitalized Rebate
--Inserting Report Portals - Month End Rebate Accrual and Capitalized Rebate
--inserting report dashboards - Month End Rebate Accrual and Capitalized Rebate
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Month End Rebate Accrual and Capitalized Rebate','682','XXEIS_751492_ZRENHS_V','XXEIS_751492_ZRENHS_V','N','');
--inserting report security - Month End Rebate Accrual and Capitalized Rebate
xxeis.eis_rsc_ins.rsec( 'Month End Rebate Accrual and Capitalized Rebate','682','','XXCUS_TM_USER',682,'KP059700','','','');
xxeis.eis_rsc_ins.rsec( 'Month End Rebate Accrual and Capitalized Rebate','682','','XXCUS_TM_ADMIN_USER',682,'KP059700','','','');
xxeis.eis_rsc_ins.rsec( 'Month End Rebate Accrual and Capitalized Rebate','682','','XXCUS_TM_FORMS_RESP',682,'KP059700','','','');
xxeis.eis_rsc_ins.rsec( 'Month End Rebate Accrual and Capitalized Rebate','682','','XXCUS_TM_ADM_FORMS_RESP',682,'KP059700','','','');
xxeis.eis_rsc_ins.rsec( 'Month End Rebate Accrual and Capitalized Rebate','','AB063501','',682,'KP059700','','N','');
--Inserting Report Pivots - Month End Rebate Accrual and Capitalized Rebate
xxeis.eis_rsc_ins.rpivot( 'Month End Rebate Accrual and Capitalized Rebate',682,'Income,LOB, Branch','1','0,0|1,1,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Income,LOB, Branch
xxeis.eis_rsc_ins.rpivot_dtls( 'Month End Rebate Accrual and Capitalized Rebate',682,'Income,LOB, Branch','PERIOD','ROW_FIELD','','','1','1','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Month End Rebate Accrual and Capitalized Rebate',682,'Income,LOB, Branch','LOB','ROW_FIELD','','','2','1','');
--Inserting Report Summary Calculation Columns For Pivot- Income,LOB, Branch
xxeis.eis_rsc_ins.rpivot( 'Month End Rebate Accrual and Capitalized Rebate',682,'Income by LOB','2','0,0|1,1,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Income by LOB
xxeis.eis_rsc_ins.rpivot_dtls( 'Month End Rebate Accrual and Capitalized Rebate',682,'Income by LOB','PERIOD','ROW_FIELD','','','1','1','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Month End Rebate Accrual and Capitalized Rebate',682,'Income by LOB','LOB','ROW_FIELD','','','2','1','');
--Inserting Report Summary Calculation Columns For Pivot- Income by LOB
--Inserting Report   Version details- Month End Rebate Accrual and Capitalized Rebate
xxeis.eis_rsc_ins.rv( 'Month End Rebate Accrual and Capitalized Rebate','','Month End Rebate Accrual and Capitalized Rebate','AB063501');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 682');
END IF;
END;
/
