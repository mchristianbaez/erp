--Report Name            : Invoice Pre-Register report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View EIS_XXWC_OM_INV_PRE_REG_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_OM_INV_PRE_REG_V
Create or replace View XXEIS.EIS_XXWC_OM_INV_PRE_REG_V
(ORDER_NUMBER,ORDERED_DATE,CREATED_BY,ORDER_TYPE,ORDER_STATUS,CUSTOMER_NUMBER,CUSTOMER_NAME,GROSS_AMOUNT,DISCOUNT_AMT,TAX_VALUE,FREIGHT_AMT,AVERAGE_COST,PROFIT,SALESREP_NAME,INVOICE_COUNT,ORDER_COUNT,TRX_NUMBER,SALESREP_NUMBER,WAREHOUSE,INV_AMOUNT,SALES,SALE_COST,HEADER_ID,PARTY_ID,ORGANIZATION_ID) AS 
SELECT ORDER_NUMBER,
    ORDERED_DATE,
    CREATED_BY,
    ORDER_TYPE,
    ORDER_STATUS,
    CUSTOMER_NUMBER,
    CUSTOMER_NAME,
    (GROSS_AMOUNT) GROSS_AMOUNT,
    DISCOUNT_AMT DISCOUNT_AMT,
    TAX_VALUE TAX_VALUE,
    FREIGHT_AMT FREIGHT_AMT,
    AVERAGE_COST AVERAGE_COST,
    PROFIT PROFIT,
    SALESREP_NAME,
    INVOICE_COUNT,
    ORDER_COUNT,
    TRX_NUMBER,
    SALESREP_NUMBER,
    WAREHOUSE,
    INV_AMOUNT,
    SALES SALES,
    SALE_COST SALE_COST,
    HEADER_ID,
    PARTY_ID,
    ORGANIZATION_ID
  FROM
    (SELECT oh.order_number,
      TRUNC(oh.ordered_date) ordered_date,
      PPF.FULL_NAME CREATED_BY,
      oth.Name Order_Type,
      NVL(flv.meaning, initcap(REPLACE(OH.FLOW_STATUS_CODE,'_',' '))) Order_Status,
      CUST_ACCT.ACCOUNT_NUMBER CUSTOMER_NUMBER,
      NVL(CUST_ACCT.ACCOUNT_NAME,PARTY.PARTY_NAME) CUSTOMER_NAME,
      SUM(NVL(ol.unit_selling_price,0) * DECODE(otl.order_category_code,'RETURN',(ol.ordered_quantity*-1),ol.ordered_quantity)) gross_amount,
      SUM(NVL(XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_DISCOUNT_AMT(OL.HEADER_ID,OL.LINE_ID),0)) DISCOUNT_AMT,
      SUM(NVL(DECODE(otl.order_category_code,'RETURN',(ol.tax_value*-1),ol.tax_value),0)) tax_value,
      SUM(NVL(xxeis.eis_rs_xxwc_com_util_pkg.get_freight_amt(ol.header_id,ol.line_id),0)) freight_amt,
      SUM(ROUND(NVL(apps.cst_cost_api.get_item_cost(1,msi.inventory_item_id,msi.organization_id),0),2)) average_cost,
      SUM(((NVL(OL.UNIT_SELLING_PRICE,0) * OL.ORDERED_QUANTITY)- (NVL(OL.UNIT_COST,0) * OL.ORDERED_QUANTITY))) PROFIT,
      REP.NAME SALESREP_NAME,
      COUNT(DISTINCT RCT.TRX_NUMBER) INVOICE_COUNT,
      COUNT(DISTINCT OH.ORDER_NUMBER) ORDER_COUNT,
      max(RCT.TRX_NUMBER) TRX_NUMBER,
      -- null SALESREP_NAME,
      REP.SALESREP_NUMBER,
      -- null Salesrep_Number,
      SHIP_FROM_ORG.ORGANIZATION_CODE WAREHOUSE,
      NVL(SUM(RCTL.EXTENDED_AMOUNT+NVL(RCTL.TAX_RECOVERABLE,0)),0)+xxeis.EIS_RS_XXWC_COM_UTIL_PKG.get_charger_amt(oh.header_id) INV_AMOUNT,
      --      max(NVL(XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CUST_INVOICE_AMT(RCT.CUSTOMER_TRX_ID),0)) INV_AMOUNT,
      --      sum(xxeis.GET_SUM_AMOUNT(rct.customer_trx_id)) inv_amount1 ,
      SUM(NVL(ol.unit_selling_price,0) * DECODE(otl.order_category_code,'RETURN',(ol.ordered_quantity*-1),ol.ordered_quantity))SALES,
      SUM(NVL(ol.Unit_Cost,0)          * DECODE(otl.order_category_code,'RETURN',(ol.ordered_quantity*-1),ol.ordered_quantity))SALE_COST,
      ---Primary Keys
      oh.header_id ,
      party.party_id ,
      -- Rct.trx_number,
      ship_from_org.organization_id
      --    msi.inventory_item_id ,
      --    msi.organization_id msi_organization_id
      --descr#flexfield#start
      --descr#flexfield#end
      --gl#accountff#start
      --gl#accountff#end
    FROM mtl_parameters ship_from_org,
      hz_parties party,
      hz_cust_accounts cust_acct,
      oe_order_headers oh,
      oe_order_lines ol,
      HR_ALL_ORGANIZATION_UNITS HOU,
      Ra_Salesreps Rep,
      oe_transaction_types_vl oth,
      Oe_Transaction_Types_Vl otl,
      MTL_SYSTEM_ITEMS_KFV MSI,
      fnd_lookup_values_vl flv,
      PER_PEOPLE_F PPF,
      fnd_user fu ,
      -- ar_payment_schedules ps,
      RA_CUSTOMER_TRX RCT,
      RA_CUSTOMER_TRX_LINES RCTL
      ,OE_CHARGE_LINES_V OCLV
    WHERE Ol.Sold_To_Org_Id      = Cust_Acct.Cust_Account_Id(+)
    AND cust_acct.party_id       = party.party_id(+)
    AND oh.Ship_From_Org_Id      = Ship_From_Org.Organization_Id(+)
    AND Ol.Header_Id             = Oh.Header_Id
    AND MSI.ORGANIZATION_ID      = HOU.ORGANIZATION_ID
    AND OH.SALESREP_ID           = REP.SALESREP_ID(+)
    AND oh.org_id                = rep.org_id(+)
    AND Msi.Inventory_Item_Id(+) = Ol.Inventory_Item_Id
    AND Msi.Organization_Id (+)  = Ol.Ship_From_Org_Id
    AND Oh.Order_Type_Id         = oth.Transaction_Type_Id
    AND oh.org_id                = oth.org_id
    AND ol.line_Type_Id          =Otl.Transaction_Type_Id
    AND ol.org_id                = otl.org_id
    AND flv.lookup_type(+)       ='FLOW_STATUS'
    AND FLV.LOOKUP_CODE(+)       = OH.FLOW_STATUS_CODE
--and oh.order_number=10000734
--    AND TO_CHAR(OH.ORDER_NUMBER)        = RCT.INTERFACE_HEADER_ATTRIBUTE1
--    AND RCT.INTERFACE_HEADER_CONTEXT(+) = 'ORDER ENTRY'
      -- AND PS.CUSTOMER_TRX_ID       =RCT.CUSTOMER_TRX_ID
      --AND PS.CLASS                <>'PMT'
    AND TRUNC(oh.ordered_date)           < TRUNC(sysdate)
    AND ol.flow_status_code             IN ('CLOSED' )
    AND ol.invoice_interface_status_code = 'YES'
    AND fu.user_id                       =ol.created_by
    AND FU.EMPLOYEE_ID                   =PPF.PERSON_ID(+)
    AND RCT.CUSTOMER_TRX_ID(+)           = RCTL.CUSTOMER_TRX_ID
    AND TO_CHAR(OL.LINE_ID)              = RCTL.INTERFACE_LINE_ATTRIBUTE6(+)
    AND RCTL.INTERFACE_LINE_CONTEXT(+)     = 'ORDER ENTRY'
    AND RCTL.INVENTORY_ITEM_ID = OL.INVENTORY_ITEM_ID
    AND RCTL.interface_line_attribute10 = ol.ship_from_org_id
    and oh.header_id =OCLV.header_id(+)
      --    and oh.header_id=3490
    AND TRUNC (ol.creation_date) BETWEEN NVL (ppf.effective_start_date, TRUNC (ol.creation_date) ) AND NVL (ppf.effective_end_date, TRUNC (ol.creation_date) )
    GROUP BY OH.ORDER_NUMBER,
      TRUNC(oh.ordered_date),
      PPF.FULL_NAME,
      OTH.NAME,
      NVL(FLV.MEANING, INITCAP(REPLACE(OH.FLOW_STATUS_CODE,'_',' '))),
      CUST_ACCT.ACCOUNT_NUMBER,
      NVL(CUST_ACCT.ACCOUNT_NAME,PARTY.PARTY_NAME),
      rep.name,
      --  NULL ,--SALESREP_NAME,
      --  null,-- SALESREP_NUMBER,
      RCT.TRX_NUMBER,
      REP.SALESREP_NUMBER,
      SHIP_FROM_ORG.ORGANIZATION_CODE,
      OH.HEADER_ID,
      PARTY.PARTY_ID,
      SHIP_FROM_ORG.ORGANIZATION_ID
    UNION ALL
    SELECT oh.order_number,
      TRUNC(oh.ordered_date) ordered_date,
      PPF.FULL_NAME CREATED_BY,
      oth.Name Order_Type,
      NVL(flv.meaning, initcap(REPLACE(OH.FLOW_STATUS_CODE,'_',' '))) Order_Status,
      CUST_ACCT.ACCOUNT_NUMBER CUSTOMER_NUMBER,
      NVL(cust_acct.account_name,party.party_name) customer_name,
      SUM(NVL(ol.unit_selling_price,0) * DECODE(otl.order_category_code,'RETURN',(ol.ordered_quantity*-1),ol.ordered_quantity)) gross_amount,
      SUM(NVL(xxeis.eis_rs_xxwc_com_util_pkg.get_discount_amt(ol.header_id,ol.line_id),0)) discount_amt,
      SUM(NVL(DECODE(otl.order_category_code,'RETURN',(ol.tax_value*-1),ol.tax_value),0)) tax_value,
      SUM(NVL(xxeis.eis_rs_xxwc_com_util_pkg.get_freight_amt(ol.header_id,ol.line_id),0)) freight_amt,
      SUM(ROUND(NVL(apps.cst_cost_api.get_item_cost(1,msi.inventory_item_id,msi.organization_id),0),2)) average_cost,
      SUM(((NVL(OL.UNIT_SELLING_PRICE,0) * OL.ORDERED_QUANTITY)- (NVL(OL.UNIT_COST,0) * OL.ORDERED_QUANTITY))) PROFIT,
      REP.NAME ,
      COUNT(DISTINCT RCT.TRX_NUMBER) INVOICE_COUNT,
      COUNT(DISTINCT OH.ORDER_NUMBER) ORDER_COUNT,
      max(RCT.TRX_NUMBER) TRX_NUMBER,
      --  NULL SALESREP_NAME,
      Rep.Salesrep_Number,
      -- null Salesrep_Number,
      SHIP_FROM_ORG.ORGANIZATION_CODE WAREHOUSE,
      NVL(SUM(RCTL.EXTENDED_AMOUNT+NVL(RCTL.TAX_RECOVERABLE,0)),0)+xxeis.EIS_RS_XXWC_COM_UTIL_PKG.get_charger_amt(oh.header_id) INV_AMOUNT,
      --      max(NVL(XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CUST_INVOICE_AMT(RCT.CUSTOMER_TRX_ID),0)) INV_AMOUNT,
      --      sum(xxeis.GET_SUM_AMOUNT(rct.customer_trx_id)) inv_amount1,
      SUM(NVL(ol.unit_selling_price,0) * DECODE(otl.order_category_code,'RETURN',(ol.ordered_quantity*-1),ol.ordered_quantity))SALES,
      SUM(NVL(ol.Unit_Cost,0)          * DECODE(otl.order_category_code,'RETURN',(ol.ordered_quantity*-1),ol.ordered_quantity))SALE_COST,
      ---Primary Keys
      oh.header_id ,
      party.party_id ,
      -- Rct.trx_number,
      ship_from_org.organization_id
      --    msi.inventory_item_id ,
      --    msi.organization_id msi_organization_id
      --descr#flexfield#start
      --descr#flexfield#end
      --gl#accountff#start
      --gl#accountff#end
    FROM mtl_parameters ship_from_org,
      hz_parties party,
      hz_cust_accounts cust_acct,
      oe_order_headers oh,
      oe_order_lines ol,
      HR_ALL_ORGANIZATION_UNITS HOU,
      Ra_Salesreps Rep,
      oe_transaction_types_vl oth,
      Oe_Transaction_Types_Vl otl,
      mtl_system_items_kfv msi,
      fnd_lookup_values_vl flv,
      per_people_f ppf,
      FND_USER FU ,
      RA_CUSTOMER_TRX RCT,
      RA_CUSTOMER_TRX_LINES RCTL
      ,OE_CHARGE_LINES_V OCLV
    WHERE Ol.Sold_To_Org_Id      = Cust_Acct.Cust_Account_Id(+)
    AND cust_acct.party_id       = party.party_id(+)
    AND oh.Ship_From_Org_Id      = Ship_From_Org.Organization_Id(+)
    AND OL.HEADER_ID             = OH.HEADER_ID
    AND MSI.ORGANIZATION_ID      = HOU.ORGANIZATION_ID
    AND OH.SALESREP_ID           = REP.SALESREP_ID(+)
    AND oh.org_id                = rep.org_id(+)
    AND Msi.Inventory_Item_Id(+) = Ol.Inventory_Item_Id
    AND Msi.Organization_Id (+)  = Ol.Ship_From_Org_Id
    AND OH.ORDER_TYPE_ID         = OTH.TRANSACTION_TYPE_ID
    AND oh.org_id                = oth.org_id
    AND OL.LINE_TYPE_ID          =OTL.TRANSACTION_TYPE_ID
    AND ol.org_id                = otl.org_id
    AND flv.lookup_type(+)       ='FLOW_STATUS'
    AND flv.lookup_code(+)       = oh.flow_status_code
    AND TRUNC(OH.ORDERED_DATE)   = TRUNC(SYSDATE)
      -- and oh.header_id=3490
    AND ( ( OTH.NAME         ='COUNTER ORDER'
    AND OL.FLOW_STATUS_CODE IN ('FULFILLED','CLOSED' ))
    OR ( OTH.NAME           <>'COUNTER ORDER'
      --AND XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_LINE_SHIP_CONFIRM_FLAG(OL.HEADER_ID,OL.LINE_ID)                       ='Y'
      --AND DECODE(OTL.ORDER_CATEGORY_CODE,'RETURN',XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_LINE_RMA_FLAG(OL.LINE_ID),'Y')='Y'
    AND OL.FLOW_STATUS_CODE            IN ('PRE-BILLING_ACCEPTANCE','CLOSED' ) )
    OR (ol.flow_status_code            IN ('INVOICE_HOLD','CLOSED' )))
    AND fu.user_id                      =ol.created_by
    AND FU.EMPLOYEE_ID                  =PPF.PERSON_ID(+)
--    AND TO_CHAR(OH.ORDER_NUMBER)        = RCT.INTERFACE_HEADER_ATTRIBUTE1
--    AND RCT.INTERFACE_HEADER_CONTEXT(+) = 'ORDER ENTRY'
    AND RCT.CUSTOMER_TRX_ID(+)           = RCTL.CUSTOMER_TRX_ID
    AND TO_CHAR(OL.LINE_ID)              = RCTL.INTERFACE_LINE_ATTRIBUTE6(+)
    and RCTL.INTERFACE_LINE_CONTEXT(+)     = 'ORDER ENTRY'
    AND RCTL.INVENTORY_ITEM_ID = OL.INVENTORY_ITEM_ID
    AND RCTL.interface_line_attribute10 = ol.ship_from_org_id
    and oh.header_id =OCLV.header_id(+)
--and oh.order_number=10000734
    AND TRUNC (ol.creation_date) BETWEEN NVL (ppf.effective_start_date, TRUNC (ol.creation_date) ) AND NVL (ppf.effective_end_date, TRUNC (ol.creation_date) )
    GROUP BY OH.ORDER_NUMBER,
      TRUNC(oh.ordered_date),
      PPF.FULL_NAME,
      OTH.NAME,
      NVL(FLV.MEANING, INITCAP(REPLACE(OH.FLOW_STATUS_CODE,'_',' '))),
      CUST_ACCT.ACCOUNT_NUMBER,
      NVL(CUST_ACCT.ACCOUNT_NAME,PARTY.PARTY_NAME),
      REP.NAME ,
      --  null ,-- SALESREP_NAME,
      RCT.TRX_NUMBER,
      Rep.Salesrep_Number,
      -- null ,-- Salesrep_Number,
      SHIP_FROM_ORG.ORGANIZATION_CODE,
      OH.HEADER_ID,
      PARTY.PARTY_ID,
      SHIP_FROM_ORG.ORGANIZATION_ID
    )
/
set scan on define on
prompt Creating View Data for Invoice Pre-Register report
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_OM_INV_PRE_REG_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_OM_INV_PRE_REG_V',660,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Xxwc Om Inv Pre Reg V','EXOIPRV','','');
--Delete View Columns for EIS_XXWC_OM_INV_PRE_REG_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_OM_INV_PRE_REG_V',660,FALSE);
--Inserting View Columns for EIS_XXWC_OM_INV_PRE_REG_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','FREIGHT_AMT',660,'Freight Amt','FREIGHT_AMT','','','','XXEIS_RS_ADMIN','NUMBER','','','Freight Amt','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','DISCOUNT_AMT',660,'Discount Amt','DISCOUNT_AMT','','','','XXEIS_RS_ADMIN','NUMBER','','','Discount Amt','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','TAX_VALUE',660,'Tax Value','TAX_VALUE','','','','XXEIS_RS_ADMIN','NUMBER','','','Tax Value','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','GROSS_AMOUNT',660,'Gross Amount','GROSS_AMOUNT','','','','XXEIS_RS_ADMIN','NUMBER','','','Gross Amount','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','CUSTOMER_NUMBER',660,'Customer Number','CUSTOMER_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','CUSTOMER_NAME',660,'Customer Name','CUSTOMER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','ORDER_STATUS',660,'Order Status','ORDER_STATUS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Order Status','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','ORDERED_DATE',660,'Ordered Date','ORDERED_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Ordered Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','ORDER_NUMBER',660,'Order Number','ORDER_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Order Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','WAREHOUSE',660,'Warehouse','WAREHOUSE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Warehouse','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','AVERAGE_COST',660,'Average Cost','AVERAGE_COST','','','','XXEIS_RS_ADMIN','NUMBER','','','Average Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','PROFIT',660,'Profit','PROFIT','','','','XXEIS_RS_ADMIN','NUMBER','','','Profit','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','ORDER_TYPE',660,'Order Type','ORDER_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Order Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','INV_AMOUNT',660,'Inv Amount','INV_AMOUNT','','','','XXEIS_RS_ADMIN','NUMBER','','','Inv Amount','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','CREATED_BY',660,'Created By','CREATED_BY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Created By','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','SALESREP_NAME',660,'Salesrep Name','SALESREP_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Salesrep Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','INVOICE_COUNT',660,'Invoice Count','INVOICE_COUNT','','','','XXEIS_RS_ADMIN','NUMBER','','','Invoice Count','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','TRX_NUMBER',660,'Trx Number','TRX_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Trx Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','SALESREP_NUMBER',660,'Salesrep Number','SALESREP_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Salesrep Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','HEADER_ID',660,'Header Id','HEADER_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Header Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','ORGANIZATION_ID',660,'Organization Id','ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','PARTY_ID',660,'Party Id','PARTY_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Party Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','SALES',660,'Sales','SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','SALE_COST',660,'Sale Cost','SALE_COST','','','','XXEIS_RS_ADMIN','NUMBER','','','Sale Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','ORDER_COUNT',660,'Order Count','ORDER_COUNT','','','','XXEIS_RS_ADMIN','NUMBER','','','Order Count','','','');
--Inserting View Components for EIS_XXWC_OM_INV_PRE_REG_V
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_INV_PRE_REG_V','MTL_PARAMETERS',660,'MTL_PARAMETERS','SHIP_FROM_ORG','SHIP_FROM_ORG','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Inventory Control Options And Defaults','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_INV_PRE_REG_V','HZ_PARTIES',660,'HZ_PARTIES','PARTY','PARTY','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Information About Parties Such As Organizations, P','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_INV_PRE_REG_V','OE_ORDER_HEADERS',660,'OE_ORDER_HEADERS_ALL','OH','OH','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Oe Order Headers All Stores Header Information For','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_INV_PRE_REG_V','OE_ORDER_LINES',660,'OE_ORDER_LINES_ALL','OL','OL','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Oe Order Lines All Stores Information For All Orde','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_INV_PRE_REG_V','MTL_SYSTEM_ITEMS_KFV',660,'MTL_SYSTEM_ITEMS_B','MSI','MSI','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Inventory Item Definitions','','','','');
--Inserting View Component Joins for EIS_XXWC_OM_INV_PRE_REG_V
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_INV_PRE_REG_V','MTL_PARAMETERS','SHIP_FROM_ORG',660,'EXOIPRV.ORGANIZATION_ID','=','SHIP_FROM_ORG.ORGANIZATION_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_INV_PRE_REG_V','HZ_PARTIES','PARTY',660,'EXOIPRV.PARTY_ID','=','PARTY.PARTY_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_INV_PRE_REG_V','OE_ORDER_HEADERS','OH',660,'EXOIPRV.HEADER_ID','=','OH.HEADER_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_INV_PRE_REG_V','OE_ORDER_LINES','OL',660,'EXOIPRV.LINE_ID','=','OL.LINE_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_INV_PRE_REG_V','MTL_SYSTEM_ITEMS_KFV','MSI',660,'EXOIPRV.INVENTORY_ITEM_ID','=','MSI.INVENTORY_ITEM_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_INV_PRE_REG_V','MTL_SYSTEM_ITEMS_KFV','MSI',660,'EXOIPRV.MSI_ORGANIZATION_ID','=','MSI.ORGANIZATION_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
END;
/
set scan on define on
prompt Creating Report LOV Data for Invoice Pre-Register report
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Invoice Pre-Register report
xxeis.eis_rs_ins.lov( 660,'select  RS.Name,SALESREP_ID  from  RA_SALESREPS RS, HR_OPERATING_UNITS OU
WHERE RS.org_id = OU.organization_id
AND RS.NAME is not null','','OM SALES REP','This gives the sales representative name','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'SELECT ORGANIZATION_CODE WAREHOUSE,ORGANIZATION_NAME FROM ORG_ORGANIZATION_DEFINITIONS OOD WHERE SYSDATE < NVL(OOD.DISABLE_DATE,SYSDATE+1) AND EXISTS (SELECT 1 FROM XXEIS.EIS_ORG_ACCESS_V  WHERE organization_id = ood.organization_id )','','OM WAREHOUSE','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'SELECT ppf.full_name  created_by, fu.user_name user_name
  FROM FND_USER fu, per_people_f ppf
 WHERE TRUNC(SYSDATE) BETWEEN fu.START_DATE AND NVL(fu.end_date, hr_general.end_of_time)
   and TRUNC(SYSDATE) BETWEEN ppf.effective_start_date AND ppf.effective_end_date
   and fu.employee_id = ppf.person_id (+)
   and exists
          (select 1
             from fnd_user_resp_groups furg,
                  fnd_responsibility_vl fr
            where furg.responsibility_id   = fr.responsibility_id
              and fu.user_id = furg.user_id
              and fr.responsibility_name like ''XXEIS%'')','','Created By Lov','','XXEIS_RS_ADMIN',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Invoice Pre-Register report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Invoice Pre-Register report
xxeis.eis_rs_utility.delete_report_rows( 'Invoice Pre-Register report' );
--Inserting Report - Invoice Pre-Register report
xxeis.eis_rs_ins.r( 660,'Invoice Pre-Register report','','Real-time branch sales; current day�s sales � at a detail and summary level.','','','','XXEIS_RS_ADMIN','EIS_XXWC_OM_INV_PRE_REG_V','Y','','','XXEIS_RS_ADMIN','','N','White Cap Reports','RTF,PDF,','CSV,HTML,Html Summary,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - Invoice Pre-Register report
xxeis.eis_rs_ins.rc( 'Invoice Pre-Register report',660,'CUSTOMER_NAME','Customer Name','Customer Name','','','','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Pre-Register report',660,'CUSTOMER_NUMBER','Customer Number','Customer Number','','','','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Pre-Register report',660,'DISCOUNT_AMT','Discount Amt','Discount Amt','','','','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Pre-Register report',660,'FREIGHT_AMT','Freight Amt','Freight Amt','','','','','12','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Pre-Register report',660,'GROSS_AMOUNT','Gross Amount','Gross Amount','','','','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Pre-Register report',660,'ORDERED_DATE','Ordered Date','Ordered Date','','','','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Pre-Register report',660,'ORDER_NUMBER','Order Number','Order Number','','','','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Pre-Register report',660,'ORDER_STATUS','Order Status','Order Status','','','','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Pre-Register report',660,'TAX_VALUE','Sales Tax','Tax Value','','','','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Pre-Register report',660,'AVERAGE_COST','Average Cost','Average Cost','','','','','14','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Pre-Register report',660,'PROFIT','Profit','Profit','','','','','15','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Pre-Register report',660,'ORDER_TYPE','Order Type','Order Type','','','','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Pre-Register report',660,'INV_AMOUNT','Inv Amount','Inv Amount','','','','','13','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Pre-Register report',660,'GM_PER','GM%','Inv Amount','NUMBER','','','','16','Y','','','','','','','case when (NVL(EXOIPRV.sales,0) !=0  AND NVL(EXOIPRV.sale_cost,0) !=0) then (((EXOIPRV.sales-EXOIPRV.sale_cost)/(EXOIPRV.sales))*100) WHEN NVL(EXOIPRV.sale_cost,0) =0  THEN 100 WHEN nvl(EXOIPRV.sales,0) != 0 THEN (((EXOIPRV.sales-EXOIPRV.sale_cost)/(EXOIPRV.sales))*100) else 0 end','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Pre-Register report',660,'CREATED_BY','Created By','Created By','','','','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Pre-Register report',660,'INVOICE_COUNT','Invoice Count','Invoice Count','','','','','17','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Pre-Register report',660,'TRX_NUMBER','Trx Number','Trx Number','','','','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Pre-Register report',660,'WAREHOUSE','Warehouse','Warehouse','','','','','18','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','');
--Inserting Report Parameters - Invoice Pre-Register report
xxeis.eis_rs_ins.rp( 'Invoice Pre-Register report',660,'Warehouse','Warehouse','WAREHOUSE','IN','OM WAREHOUSE','','VARCHAR2','Y','Y','1','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Invoice Pre-Register report',660,'Ordered Date From','Ordered Date From','ORDERED_DATE','>=','','','DATE','Y','Y','2','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','Start Date','');
xxeis.eis_rs_ins.rp( 'Invoice Pre-Register report',660,'Ordered Date To','Ordered Date To','ORDERED_DATE','<=','','','DATE','Y','Y','3','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','End Date','');
xxeis.eis_rs_ins.rp( 'Invoice Pre-Register report',660,'Created By','Created By','CREATED_BY','IN','Created By Lov','','VARCHAR2','N','Y','4','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Invoice Pre-Register report',660,'Salesrep Name','Salesrep Name','SALESREP_NAME','IN','OM SALES REP','','VARCHAR2','N','Y','5','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
--Inserting Report Conditions - Invoice Pre-Register report
xxeis.eis_rs_ins.rcn( 'Invoice Pre-Register report',660,'WAREHOUSE','IN',':Warehouse','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Invoice Pre-Register report',660,'ORDERED_DATE','>=',':Ordered Date From','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Invoice Pre-Register report',660,'ORDERED_DATE','<=',':Ordered Date To','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Invoice Pre-Register report',660,'CREATED_BY','IN',':Created By','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Invoice Pre-Register report',660,'SALESREP_NAME','IN',':Salesrep Name','','','Y','','Y','XXEIS_RS_ADMIN');
--Inserting Report Sorts - Invoice Pre-Register report
xxeis.eis_rs_ins.rs( 'Invoice Pre-Register report',660,'CREATED_BY','ASC','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rs( 'Invoice Pre-Register report',660,'ORDER_NUMBER','ASC','XXEIS_RS_ADMIN','','');
--Inserting Report Triggers - Invoice Pre-Register report
--Inserting Report Templates - Invoice Pre-Register report
--Inserting Report Portals - Invoice Pre-Register report
--Inserting Report Dashboards - Invoice Pre-Register report
--Inserting Report Security - Invoice Pre-Register report
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','660','','50926',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','660','','50927',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','660','','50928',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','660','','50929',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','660','','50931',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','660','','50930',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','660','','50856',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','660','','50857',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','660','','50858',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','660','','50859',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','660','','50860',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','660','','50861',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','20005','','50880',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','','LC053655','',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','','10010432','',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','','RB054040','',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','','RV003897','',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','','SS084202','',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','660','','21623',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','','SO004816','',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','660','','50886',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','660','','50901',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','660','','50870',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','660','','50871',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','660','','50869',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','20005','','50900',660,'XXEIS_RS_ADMIN','','');
--Inserting Report Pivots - Invoice Pre-Register report
xxeis.eis_rs_ins.rpivot( 'Invoice Pre-Register report',660,'Pivot','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rs_ins.rpivot_dtls( 'Invoice Pre-Register report',660,'Pivot','CUSTOMER_NAME','PAGE_FIELD','','','1','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Invoice Pre-Register report',660,'Pivot','GROSS_AMOUNT','DATA_FIELD','SUM','','1','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Invoice Pre-Register report',660,'Pivot','DISCOUNT_AMT','DATA_FIELD','SUM','','2','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Invoice Pre-Register report',660,'Pivot','INV_AMOUNT','DATA_FIELD','SUM','','3','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Invoice Pre-Register report',660,'Pivot','PROFIT','DATA_FIELD','SUM','','4','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Invoice Pre-Register report',660,'Pivot','INVOICE_COUNT','DATA_FIELD','SUM','','5','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Invoice Pre-Register report',660,'Pivot','CREATED_BY','ROW_FIELD','','','1','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Invoice Pre-Register report',660,'Pivot','ORDER_NUMBER','ROW_FIELD','','','2','','');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
END;
/
set scan on define on
