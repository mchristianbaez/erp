--Report Name            : HDS AP Garnishment Open and Paid Invoices - Check Register
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data XXEIS_CHECK_REGISTER
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Inserting Object XXEIS_CHECK_REGISTER
xxeis.eis_rsc_ins.v( 'XXEIS_CHECK_REGISTER',200,'','','','','XXEIS_RS_ADMIN','XXEIS','Xxeis Check Register','XCR','','','VIEW','US','','','');
--Delete Object Columns for XXEIS_CHECK_REGISTER
xxeis.eis_rsc_utility.delete_view_rows('XXEIS_CHECK_REGISTER',200,FALSE);
--Inserting Object Columns for XXEIS_CHECK_REGISTER
xxeis.eis_rsc_ins.vc( 'XXEIS_CHECK_REGISTER','CHECK_VOID_DATE',200,'Check Void Date','CHECK_VOID_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Check Void Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_CHECK_REGISTER','CHECK_DATE',200,'Check Date','CHECK_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Check Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_CHECK_REGISTER','CHECK_NUMBER',200,'Check Number','CHECK_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Check Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_CHECK_REGISTER','VENDOR_TYPE',200,'Vendor Type','VENDOR_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_CHECK_REGISTER','ORG_ID',200,'Org Id','ORG_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Org Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_CHECK_REGISTER','INVOICE_APPROVAL_STATUS',200,'Invoice Approval Status','INVOICE_APPROVAL_STATUS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Invoice Approval Status','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_CHECK_REGISTER','INVOICE_PAYMENT_STATUS',200,'Invoice Payment Status','INVOICE_PAYMENT_STATUS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Invoice Payment Status','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_CHECK_REGISTER','PAYMENT_METHOD',200,'Payment Method','PAYMENT_METHOD','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Payment Method','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_CHECK_REGISTER','PAY_GROUP',200,'Pay Group','PAY_GROUP','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Pay Group','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_CHECK_REGISTER','INVOICE_TYPE',200,'Invoice Type','INVOICE_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Invoice Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_CHECK_REGISTER','VENDOR_SITE',200,'Vendor Site','VENDOR_SITE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Site','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_CHECK_REGISTER','VENDOR_NAME',200,'Vendor Name','VENDOR_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_CHECK_REGISTER','GL_DATE',200,'Gl Date','GL_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Gl Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_CHECK_REGISTER','DESCRIPTION',200,'Description','DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Description','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_CHECK_REGISTER','VENDOR_NUMBER',200,'Vendor Number','VENDOR_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_CHECK_REGISTER','EMPLOYEE',200,'Employee','EMPLOYEE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Employee','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_CHECK_REGISTER','DEDUCTION_CODE',200,'Deduction Code','DEDUCTION_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Deduction Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_CHECK_REGISTER','CASE_IDENTIFIER',200,'Case Identifier','CASE_IDENTIFIER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Case Identifier','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_CHECK_REGISTER','INVOICE_AMOUNT',200,'Invoice Amount','INVOICE_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Invoice Amount','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_CHECK_REGISTER','INVOICE_DATE',200,'Invoice Date','INVOICE_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Invoice Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_CHECK_REGISTER','INVOICE_NUMBER',200,'Invoice Number','INVOICE_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Invoice Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_CHECK_REGISTER','CHECK_STATUS',200,'Check Status','CHECK_STATUS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Check Status','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_CHECK_REGISTER','CHECK_AMOUNT',200,'Check Amount','CHECK_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Check Amount','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_CHECK_REGISTER','PAYMENT_AMOUNT',200,'Payment Amount','PAYMENT_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Payment Amount','','','','US','');
--Inserting Object Components for XXEIS_CHECK_REGISTER
--Inserting Object Component Joins for XXEIS_CHECK_REGISTER
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
--There are no LOV/LOV TL entries for the report HDS AP Garnishment Open and Paid Invoices - Check Register
prompt Creating Report Data for HDS AP Garnishment Open and Paid Invoices - Check Register
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Deleting Report data - HDS AP Garnishment Open and Paid Invoices - Check Register
xxeis.eis_rsc_utility.delete_report_rows( 'HDS AP Garnishment Open and Paid Invoices - Check Register',200 );
--Inserting Report - HDS AP Garnishment Open and Paid Invoices - Check Register
xxeis.eis_rsc_ins.r( 200,'HDS AP Garnishment Open and Paid Invoices - Check Register','','','','','','XXEIS_RS_ADMIN','XXEIS_CHECK_REGISTER','Y','','','XXEIS_RS_ADMIN','','N','HDS Standard Reports','','CSV,Pivot Excel,EXCEL,','Y','','','','','','','','US','','','','','','','','','','','','','','','','');
--Inserting Report Columns - HDS AP Garnishment Open and Paid Invoices - Check Register
xxeis.eis_rsc_ins.rc( 'HDS AP Garnishment Open and Paid Invoices - Check Register',200,'CASE_IDENTIFIER','Case Identifier','Case Identifier','','','default','','9','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_CHECK_REGISTER','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS AP Garnishment Open and Paid Invoices - Check Register',200,'CHECK_DATE','Check Date','Check Date','','','default','','4','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_CHECK_REGISTER','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS AP Garnishment Open and Paid Invoices - Check Register',200,'CHECK_NUMBER','Check Number','Check Number','','~~~','default','','3','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_CHECK_REGISTER','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS AP Garnishment Open and Paid Invoices - Check Register',200,'CHECK_STATUS','Check Status','Check Status','','','default','','20','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_CHECK_REGISTER','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS AP Garnishment Open and Paid Invoices - Check Register',200,'CHECK_VOID_DATE','Check Void Date','Check Void Date','','','default','','21','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_CHECK_REGISTER','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS AP Garnishment Open and Paid Invoices - Check Register',200,'DEDUCTION_CODE','Deduction Code','Deduction Code','','','default','','10','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_CHECK_REGISTER','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS AP Garnishment Open and Paid Invoices - Check Register',200,'DESCRIPTION','Description','Description','','','default','','13','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_CHECK_REGISTER','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS AP Garnishment Open and Paid Invoices - Check Register',200,'EMPLOYEE','Employee','Employee','','','default','','11','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_CHECK_REGISTER','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS AP Garnishment Open and Paid Invoices - Check Register',200,'GL_DATE','Gl Date','Gl Date','','','default','','14','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_CHECK_REGISTER','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS AP Garnishment Open and Paid Invoices - Check Register',200,'INVOICE_AMOUNT','Invoice Amount','Invoice Amount','','~T~D~2','default','','8','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_CHECK_REGISTER','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS AP Garnishment Open and Paid Invoices - Check Register',200,'INVOICE_APPROVAL_STATUS','Invoice Approval Status','Invoice Approval Status','','','default','','22','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_CHECK_REGISTER','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS AP Garnishment Open and Paid Invoices - Check Register',200,'INVOICE_DATE','Invoice Date','Invoice Date','','','default','','7','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_CHECK_REGISTER','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS AP Garnishment Open and Paid Invoices - Check Register',200,'INVOICE_NUMBER','Invoice Number','Invoice Number','','','default','','6','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_CHECK_REGISTER','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS AP Garnishment Open and Paid Invoices - Check Register',200,'INVOICE_PAYMENT_STATUS','Invoice Payment Status','Invoice Payment Status','','','default','','23','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_CHECK_REGISTER','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS AP Garnishment Open and Paid Invoices - Check Register',200,'INVOICE_TYPE','Invoice Type','Invoice Type','','','default','','17','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_CHECK_REGISTER','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS AP Garnishment Open and Paid Invoices - Check Register',200,'ORG_ID','Org Id','Org Id','','~~~','default','','1','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_CHECK_REGISTER','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS AP Garnishment Open and Paid Invoices - Check Register',200,'PAYMENT_METHOD','Payment Method','Payment Method','','','default','','19','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_CHECK_REGISTER','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS AP Garnishment Open and Paid Invoices - Check Register',200,'PAY_GROUP','Pay Group','Pay Group','','','default','','18','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_CHECK_REGISTER','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS AP Garnishment Open and Paid Invoices - Check Register',200,'VENDOR_NAME','Vendor Name','Vendor Name','','','default','','15','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_CHECK_REGISTER','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS AP Garnishment Open and Paid Invoices - Check Register',200,'VENDOR_NUMBER','Vendor Number','Vendor Number','','','default','','12','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_CHECK_REGISTER','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS AP Garnishment Open and Paid Invoices - Check Register',200,'VENDOR_SITE','Vendor Site','Vendor Site','','','default','','16','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_CHECK_REGISTER','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS AP Garnishment Open and Paid Invoices - Check Register',200,'VENDOR_TYPE','Vendor Type','Vendor Type','','','default','','2','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_CHECK_REGISTER','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS AP Garnishment Open and Paid Invoices - Check Register',200,'PAYMENT_AMOUNT','Payment Amount','Payment Amount','','~T~D~2','default','','5','','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_CHECK_REGISTER','','','SUM','US','','');
--Inserting Report Parameters - HDS AP Garnishment Open and Paid Invoices - Check Register
xxeis.eis_rsc_ins.rp( 'HDS AP Garnishment Open and Paid Invoices - Check Register',200,'Vendor Number','Vendor Number','VENDOR_NUMBER','IN','','','VARCHAR2','N','Y','1','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','XXEIS_CHECK_REGISTER','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS AP Garnishment Open and Paid Invoices - Check Register',200,'Employee Number','Employee Number','EMPLOYEE','IN','','','VARCHAR2','N','Y','2','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','XXEIS_CHECK_REGISTER','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS AP Garnishment Open and Paid Invoices - Check Register',200,'Case Identifier','Case Identifier','CASE_IDENTIFIER','IN','','','VARCHAR2','N','Y','3','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','XXEIS_CHECK_REGISTER','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS AP Garnishment Open and Paid Invoices - Check Register',200,'Check Date From','Check Date From','CHECK_DATE','>=','','','DATE','N','Y','4','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','Start Date','','XXEIS_CHECK_REGISTER','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS AP Garnishment Open and Paid Invoices - Check Register',200,'Check Date To','Check Date To','CHECK_DATE','<=','','','DATE','N','Y','5','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','End Date','','XXEIS_CHECK_REGISTER','','','US','');
--Inserting Dependent Parameters - HDS AP Garnishment Open and Paid Invoices - Check Register
--Inserting Report Conditions - HDS AP Garnishment Open and Paid Invoices - Check Register
xxeis.eis_rsc_ins.rcnh( 'HDS AP Garnishment Open and Paid Invoices - Check Register',200,'CASE_IDENTIFIER IN :Case Identifier ','SIMPLE','','','Y','','1');
xxeis.eis_rsc_ins.rcnd( '','','CASE_IDENTIFIER','','Case Identifier','','','','','XXEIS_CHECK_REGISTER','','','','','','IN','Y','Y','','','','','1',200,'HDS AP Garnishment Open and Paid Invoices - Check Register','CASE_IDENTIFIER IN :Case Identifier ');
xxeis.eis_rsc_ins.rcnh( 'HDS AP Garnishment Open and Paid Invoices - Check Register',200,'CHECK_DATE BETWEEN :Check Date From  :Check Date To','SIMPLE','','','Y','','2');
xxeis.eis_rsc_ins.rcnd( '','','CHECK_DATE','','Check Date From','','','Check Date To','','XXEIS_CHECK_REGISTER','','','','','','BETWEEN','Y','Y','','','','','1',200,'HDS AP Garnishment Open and Paid Invoices - Check Register','CHECK_DATE BETWEEN :Check Date From  :Check Date To');
xxeis.eis_rsc_ins.rcnh( 'HDS AP Garnishment Open and Paid Invoices - Check Register',200,'EMPLOYEE IN :Employee Number ','SIMPLE','','','Y','','3');
xxeis.eis_rsc_ins.rcnd( '','','EMPLOYEE','','Employee Number','','','','','XXEIS_CHECK_REGISTER','','','','','','IN','Y','Y','','','','','1',200,'HDS AP Garnishment Open and Paid Invoices - Check Register','EMPLOYEE IN :Employee Number ');
xxeis.eis_rsc_ins.rcnh( 'HDS AP Garnishment Open and Paid Invoices - Check Register',200,'ORG_ID = 166 ','ADVANCED','','1#$#','Y','','4');
xxeis.eis_rsc_ins.rcnd( '','','ORG_ID','','','','','','','XXEIS_CHECK_REGISTER','','','','','','EQUALS','Y','N','','166','','','1',200,'HDS AP Garnishment Open and Paid Invoices - Check Register','ORG_ID = 166 ');
xxeis.eis_rsc_ins.rcnh( 'HDS AP Garnishment Open and Paid Invoices - Check Register',200,'VENDOR_NUMBER IN :Vendor Number ','SIMPLE','','','Y','','5');
xxeis.eis_rsc_ins.rcnd( '','','VENDOR_NUMBER','','Vendor Number','','','','','XXEIS_CHECK_REGISTER','','','','','','IN','Y','Y','','','','','1',200,'HDS AP Garnishment Open and Paid Invoices - Check Register','VENDOR_NUMBER IN :Vendor Number ');
xxeis.eis_rsc_ins.rcnh( 'HDS AP Garnishment Open and Paid Invoices - Check Register',200,'Free Text ','FREE_TEXT','','','N','','6');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','','Y','','','','','group by  XCR.ORG_ID
,XCR.VENDOR_TYPE
,XCR.CHECK_NUMBER
,XCR.CHECK_DATE 
,XCR.INVOICE_NUMBER
,XCR.INVOICE_DATE 
,XCR.INVOICE_AMOUNT
,XCR.CASE_IDENTIFIER
,XCR.DEDUCTION_CODE
,XCR.EMPLOYEE
,XCR.VENDOR_NUMBER
,XCR.DESCRIPTION
,XCR.GL_DATE 
,XCR.VENDOR_NAME
,XCR.VENDOR_SITE
,XCR.INVOICE_TYPE
,XCR.PAY_GROUP
,XCR.PAYMENT_METHOD
,XCR.CHECK_STATUS
,XCR.CHECK_VOID_DATE 
,XCR.INVOICE_APPROVAL_STATUS
,XCR.INVOICE_PAYMENT_STATUS','1',200,'HDS AP Garnishment Open and Paid Invoices - Check Register','Free Text ');
--Inserting Report Sorts - HDS AP Garnishment Open and Paid Invoices - Check Register
--Inserting Report Triggers - HDS AP Garnishment Open and Paid Invoices - Check Register
--inserting report templates - HDS AP Garnishment Open and Paid Invoices - Check Register
--Inserting Report Portals - HDS AP Garnishment Open and Paid Invoices - Check Register
--inserting report dashboards - HDS AP Garnishment Open and Paid Invoices - Check Register
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'HDS AP Garnishment Open and Paid Invoices - Check Register','200','XXEIS_CHECK_REGISTER','XXEIS_CHECK_REGISTER','N','');
--inserting report security - HDS AP Garnishment Open and Paid Invoices - Check Register
xxeis.eis_rsc_ins.rsec( 'HDS AP Garnishment Open and Paid Invoices - Check Register','200','','HDS_AP_ADMIN_US_GSCIWO',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Garnishment Open and Paid Invoices - Check Register','200','','HDS_AP_ADMIN_US_IWO',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Garnishment Open and Paid Invoices - Check Register','200','','XXWC_PAY_NO_CALENDAR',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Garnishment Open and Paid Invoices - Check Register','200','','XXWC_PAY_W_CALENDAR',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Garnishment Open and Paid Invoices - Check Register','200','','HDS_AP_DISBUREMTS_US_IWO',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Garnishment Open and Paid Invoices - Check Register','200','','XXWC_PAY_DISBURSE',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Garnishment Open and Paid Invoices - Check Register','200','','HDS_AP_INQUIRY_US_IWO',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Garnishment Open and Paid Invoices - Check Register','200','','XXWC_PAYABLES_INQUIRY',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Garnishment Open and Paid Invoices - Check Register','200','','HDS_AP_MGR_NOSUP_US_IWO',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Garnishment Open and Paid Invoices - Check Register','200','','HDS_AP_MANAGER',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Garnishment Open and Paid Invoices - Check Register','200','','HDS_PYABLS_MNGR_CAN',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Garnishment Open and Paid Invoices - Check Register','200','','HDS_PYBLS_MNGR',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Garnishment Open and Paid Invoices - Check Register','200','','XXWC_PAY_MANAGER',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Garnishment Open and Paid Invoices - Check Register','200','','HDS_AP_SUPPLIER_MAINT_US_IWO',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Garnishment Open and Paid Invoices - Check Register','200','','HDS_AP_TRNS_ENTRY_US_IWO',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Garnishment Open and Paid Invoices - Check Register','200','','XXWC_PAY_VENDOR_BANK_DETAILS',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Garnishment Open and Paid Invoices - Check Register','200','','XXWC_PAY_VENDOR_MSTR',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Garnishment Open and Paid Invoices - Check Register','200','','XXWC_PAY_VENDOR_MSTR_INQUIRY',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Garnishment Open and Paid Invoices - Check Register','20005','','XXWC_VIEW_ALL_EIS_REPORTS',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Garnishment Open and Paid Invoices - Check Register','200','','PAYABLES_INQUIRY',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Garnishment Open and Paid Invoices - Check Register','200','','PAYABLES_MANAGER',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Garnishment Open and Paid Invoices - Check Register','200','','HDS_PAYABLES_CLOSE_GSC_GLBL',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Garnishment Open and Paid Invoices - Check Register','200','','HDS_PAYABLES_CLOSE_GLBL',200,'XXEIS_RS_ADMIN','','','');
--Inserting Report Pivots - HDS AP Garnishment Open and Paid Invoices - Check Register
--Inserting Report Distribution Details 
--Inserting Report Distribution Details 
--Inserting Report SubTotals Details 
--Inserting Report aggregations 
--Inserting Report   Version details- HDS AP Garnishment Open and Paid Invoices - Check Register
xxeis.eis_rsc_ins.rv( 'HDS AP Garnishment Open and Paid Invoices - Check Register','','HDS AP Garnishment Open and Paid Invoices - Check Register','AB065961','27-JUN-2017');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
