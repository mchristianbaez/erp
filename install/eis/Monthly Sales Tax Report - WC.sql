--Report Name            : Monthly Sales Tax Report - WC
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data EIS_XXWC_MONTHLY_SALE_TAX_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(222);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_MONTHLY_SALE_TAX_V
xxeis.eis_rsc_ins.v( 'EIS_XXWC_MONTHLY_SALE_TAX_V',222,'','','','','SA059956','XXEIS','Eis Xxwc Monthly Sale Tax V','EXMSTV','','','VIEW','US','','','');
--Delete Object Columns for EIS_XXWC_MONTHLY_SALE_TAX_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_MONTHLY_SALE_TAX_V',222,FALSE);
--Inserting Object Columns for EIS_XXWC_MONTHLY_SALE_TAX_V
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','CN_TXW_TAXLEVEL',222,'Cn Txw Taxlevel','CN_TXW_TAXLEVEL','','','','SA059956','VARCHAR2','','','Cn Txw Taxlevel','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','CN_TXW_TAXTYPE',222,'Cn Txw Taxtype','CN_TXW_TAXTYPE','','','','SA059956','VARCHAR2','','','Cn Txw Taxtype','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','CN_TXW_TAXAMT',222,'Cn Txw Taxamt','CN_TXW_TAXAMT','','','','SA059956','NUMBER','','','Cn Txw Taxamt','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','CN_TXW_TAXRATE',222,'Cn Txw Taxrate','CN_TXW_TAXRATE','','','','SA059956','NUMBER','','','Cn Txw Taxrate','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','CN_TXW_CMPLCODE',222,'Cn Txw Cmplcode','CN_TXW_CMPLCODE','','','','SA059956','VARCHAR2','','','Cn Txw Cmplcode','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','CN_TXW_EXMPTAMT',222,'Cn Txw Exmptamt','CN_TXW_EXMPTAMT','','','','SA059956','NUMBER','','','Cn Txw Exmptamt','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','CN_TXW_ADMNCODE',222,'Cn Txw Admncode','CN_TXW_ADMNCODE','','','','SA059956','VARCHAR2','','','Cn Txw Admncode','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','CN_TXW_TAXCERT',222,'Cn Txw Taxcert','CN_TXW_TAXCERT','','','','SA059956','VARCHAR2','','','Cn Txw Taxcert','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','CN_TXW_REASCODE',222,'Cn Txw Reascode','CN_TXW_REASCODE','','','','SA059956','VARCHAR2','','','Cn Txw Reascode','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','CN_TXW_BASEAMT',222,'Cn Txw Baseamt','CN_TXW_BASEAMT','','','','SA059956','NUMBER','','','Cn Txw Baseamt','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','CN_TXW_TAXCODE',222,'Cn Txw Taxcode','CN_TXW_TAXCODE','','','','SA059956','VARCHAR2','','','Cn Txw Taxcode','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','CN_TXW_EXCPCODE',222,'Cn Txw Excpcode','CN_TXW_EXCPCODE','','','','SA059956','VARCHAR2','','','Cn Txw Excpcode','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','LO_TXW_TAXLEVEL',222,'Lo Txw Taxlevel','LO_TXW_TAXLEVEL','','','','SA059956','VARCHAR2','','','Lo Txw Taxlevel','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','LO_TXW_TAXTYPE',222,'Lo Txw Taxtype','LO_TXW_TAXTYPE','','','','SA059956','VARCHAR2','','','Lo Txw Taxtype','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','LO_TXW_TAXAMT',222,'Lo Txw Taxamt','LO_TXW_TAXAMT','','','','SA059956','NUMBER','','','Lo Txw Taxamt','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','LO_TXW_TAXRATE',222,'Lo Txw Taxrate','LO_TXW_TAXRATE','','','','SA059956','NUMBER','','','Lo Txw Taxrate','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','LO_TXW_CMPLCODE',222,'Lo Txw Cmplcode','LO_TXW_CMPLCODE','','','','SA059956','VARCHAR2','','','Lo Txw Cmplcode','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','LO_TXW_EXMPTAMT',222,'Lo Txw Exmptamt','LO_TXW_EXMPTAMT','','','','SA059956','NUMBER','','','Lo Txw Exmptamt','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','LO_TXW_ADMNCODE',222,'Lo Txw Admncode','LO_TXW_ADMNCODE','','','','SA059956','VARCHAR2','','','Lo Txw Admncode','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','LO_TXW_TAXCERT',222,'Lo Txw Taxcert','LO_TXW_TAXCERT','','','','SA059956','VARCHAR2','','','Lo Txw Taxcert','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','LO_TXW_REASCODE',222,'Lo Txw Reascode','LO_TXW_REASCODE','','','','SA059956','VARCHAR2','','','Lo Txw Reascode','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','LO_TXW_BASEAMT',222,'Lo Txw Baseamt','LO_TXW_BASEAMT','','','','SA059956','NUMBER','','','Lo Txw Baseamt','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','LO_TXW_TAXCODE',222,'Lo Txw Taxcode','LO_TXW_TAXCODE','','','','SA059956','VARCHAR2','','','Lo Txw Taxcode','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','LO_TXW_EXCPCODE',222,'Lo Txw Excpcode','LO_TXW_EXCPCODE','','','','SA059956','VARCHAR2','','','Lo Txw Excpcode','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','SS_TXW_TAXLEVEL',222,'Ss Txw Taxlevel','SS_TXW_TAXLEVEL','','','','SA059956','VARCHAR2','','','Ss Txw Taxlevel','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','SS_TXW_TAXTYPE',222,'Ss Txw Taxtype','SS_TXW_TAXTYPE','','','','SA059956','VARCHAR2','','','Ss Txw Taxtype','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','SS_TXW_TAXAMT',222,'Ss Txw Taxamt','SS_TXW_TAXAMT','','','','SA059956','NUMBER','','','Ss Txw Taxamt','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','SS_TXW_TAXRATE',222,'Ss Txw Taxrate','SS_TXW_TAXRATE','','','','SA059956','NUMBER','','','Ss Txw Taxrate','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','SS_TXW_CMPLCODE',222,'Ss Txw Cmplcode','SS_TXW_CMPLCODE','','','','SA059956','VARCHAR2','','','Ss Txw Cmplcode','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','SS_TXW_EXMPTAMT',222,'Ss Txw Exmptamt','SS_TXW_EXMPTAMT','','','','SA059956','NUMBER','','','Ss Txw Exmptamt','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','SS_TXW_ADMNCODE',222,'Ss Txw Admncode','SS_TXW_ADMNCODE','','','','SA059956','VARCHAR2','','','Ss Txw Admncode','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','SS_TXW_TAXCERT',222,'Ss Txw Taxcert','SS_TXW_TAXCERT','','','','SA059956','VARCHAR2','','','Ss Txw Taxcert','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','SS_TXW_REASCODE',222,'Ss Txw Reascode','SS_TXW_REASCODE','','','','SA059956','VARCHAR2','','','Ss Txw Reascode','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','SS_TXW_BASEAMT',222,'Ss Txw Baseamt','SS_TXW_BASEAMT','','','','SA059956','NUMBER','','','Ss Txw Baseamt','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','SS_TXW_TAXCODE',222,'Ss Txw Taxcode','SS_TXW_TAXCODE','','','','SA059956','VARCHAR2','','','Ss Txw Taxcode','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','SS_TXW_EXCPCODE',222,'Ss Txw Excpcode','SS_TXW_EXCPCODE','','','','SA059956','VARCHAR2','','','Ss Txw Excpcode','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','SC_TXW_TAXLEVEL',222,'Sc Txw Taxlevel','SC_TXW_TAXLEVEL','','','','SA059956','VARCHAR2','','','Sc Txw Taxlevel','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','SC_TXW_TAXTYPE',222,'Sc Txw Taxtype','SC_TXW_TAXTYPE','','','','SA059956','VARCHAR2','','','Sc Txw Taxtype','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','SC_TXW_TAXAMT',222,'Sc Txw Taxamt','SC_TXW_TAXAMT','','','','SA059956','NUMBER','','','Sc Txw Taxamt','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','SC_TXW_TAXRATE',222,'Sc Txw Taxrate','SC_TXW_TAXRATE','','','','SA059956','NUMBER','','','Sc Txw Taxrate','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','SC_TXW_CMPLCODE',222,'Sc Txw Cmplcode','SC_TXW_CMPLCODE','','','','SA059956','VARCHAR2','','','Sc Txw Cmplcode','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','SC_TXW_EXMPTAMT',222,'Sc Txw Exmptamt','SC_TXW_EXMPTAMT','','','','SA059956','NUMBER','','','Sc Txw Exmptamt','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','SC_TXW_ADMNCODE',222,'Sc Txw Admncode','SC_TXW_ADMNCODE','','','','SA059956','VARCHAR2','','','Sc Txw Admncode','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','SC_TXW_TAXCERT',222,'Sc Txw Taxcert','SC_TXW_TAXCERT','','','','SA059956','VARCHAR2','','','Sc Txw Taxcert','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','SC_TXW_REASCODE',222,'Sc Txw Reascode','SC_TXW_REASCODE','','','','SA059956','VARCHAR2','','','Sc Txw Reascode','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','SC_TXW_BASEAMT',222,'Sc Txw Baseamt','SC_TXW_BASEAMT','','','','SA059956','NUMBER','','','Sc Txw Baseamt','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','SC_TXW_TAXCODE',222,'Sc Txw Taxcode','SC_TXW_TAXCODE','','','','SA059956','VARCHAR2','','','Sc Txw Taxcode','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','SC_TXW_EXCPCODE',222,'Sc Txw Excpcode','SC_TXW_EXCPCODE','','','','SA059956','VARCHAR2','','','Sc Txw Excpcode','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','SL_TXW_TAXLEVEL',222,'Sl Txw Taxlevel','SL_TXW_TAXLEVEL','','','','SA059956','VARCHAR2','','','Sl Txw Taxlevel','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','SL_TXW_TAXTYPE',222,'Sl Txw Taxtype','SL_TXW_TAXTYPE','','','','SA059956','VARCHAR2','','','Sl Txw Taxtype','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','SL_TXW_TAXAMT',222,'Sl Txw Taxamt','SL_TXW_TAXAMT','','','','SA059956','NUMBER','','','Sl Txw Taxamt','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','SL_TXW_TAXRATE',222,'Sl Txw Taxrate','SL_TXW_TAXRATE','','','','SA059956','NUMBER','','','Sl Txw Taxrate','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','SL_TXW_CMPLCODE',222,'Sl Txw Cmplcode','SL_TXW_CMPLCODE','','','','SA059956','VARCHAR2','','','Sl Txw Cmplcode','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','SL_TXW_EXMPTAMT',222,'Sl Txw Exmptamt','SL_TXW_EXMPTAMT','','','','SA059956','NUMBER','','','Sl Txw Exmptamt','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','SL_TXW_ADMNCODE',222,'Sl Txw Admncode','SL_TXW_ADMNCODE','','','','SA059956','VARCHAR2','','','Sl Txw Admncode','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','SL_TXW_TAXCERT',222,'Sl Txw Taxcert','SL_TXW_TAXCERT','','','','SA059956','VARCHAR2','','','Sl Txw Taxcert','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','SL_TXW_REASCODE',222,'Sl Txw Reascode','SL_TXW_REASCODE','','','','SA059956','VARCHAR2','','','Sl Txw Reascode','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','SL_TXW_BASEAMT',222,'Sl Txw Baseamt','SL_TXW_BASEAMT','','','','SA059956','NUMBER','','','Sl Txw Baseamt','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','SL_TXW_TAXCODE',222,'Sl Txw Taxcode','SL_TXW_TAXCODE','','','','SA059956','VARCHAR2','','','Sl Txw Taxcode','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','SL_TXW_EXCPCODE',222,'Sl Txw Excpcode','SL_TXW_EXCPCODE','','','','SA059956','VARCHAR2','','','Sl Txw Excpcode','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','TXW_REFERENCE',222,'Txw Reference','TXW_REFERENCE','','','','SA059956','VARCHAR2','','','Txw Reference','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','CUSTPRODCODE',222,'Custprodcode','CUSTPRODCODE','','','','SA059956','VARCHAR2','','','Custprodcode','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','JURISTYPE',222,'Juristype','JURISTYPE','','','','SA059956','VARCHAR2','','','Juristype','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','POT',222,'Pot','POT','','','','SA059956','VARCHAR2','','','Pot','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','JURISNO',222,'Jurisno','JURISNO','','','','SA059956','NUMBER','','','Jurisno','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','JURIS_STCODE',222,'Juris Stcode','JURIS_STCODE','','','','SA059956','VARCHAR2','','','Juris Stcode','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','JURIS_CNTYCODE',222,'Juris Cntycode','JURIS_CNTYCODE','','','','SA059956','VARCHAR2','','','Juris Cntycode','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','JURIS_GEOCODE',222,'Juris Geocode','JURIS_GEOCODE','','','','SA059956','VARCHAR2','','','Juris Geocode','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','JURIS_ZIPCODE',222,'Juris Zipcode','JURIS_ZIPCODE','','','','SA059956','VARCHAR2','','','Juris Zipcode','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','JURIS_ZIPEXTN',222,'Juris Zipextn','JURIS_ZIPEXTN','','','','SA059956','VARCHAR2','','','Juris Zipextn','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','JURIS_STNAME',222,'Juris Stname','JURIS_STNAME','','','','SA059956','VARCHAR2','','','Juris Stname','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','JURIS_CNTYNAME',222,'Juris Cntyname','JURIS_CNTYNAME','','','','SA059956','VARCHAR2','','','Juris Cntyname','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','JURIS_CITYNAME',222,'Juris Cityname','JURIS_CITYNAME','','','','SA059956','VARCHAR2','','','Juris Cityname','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','REQUEST_ID',222,'Request Id','REQUEST_ID','','','','SA059956','NUMBER','','','Request Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','TAXWARE_MATCH',222,'Taxware Match','TAXWARE_MATCH','','','','SA059956','VARCHAR2','','','Taxware Match','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','PERIOD',222,'Period','PERIOD','','','','SA059956','VARCHAR2','','','Period','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','INVOICE_SOURCE',222,'Invoice Source','INVOICE_SOURCE','','','','SA059956','VARCHAR2','','','Invoice Source','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','INVOICE_NUMBER',222,'Invoice Number','INVOICE_NUMBER','','','','SA059956','VARCHAR2','','','Invoice Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','INV_CREATION_DATE',222,'Inv Creation Date','INV_CREATION_DATE','','','','SA059956','DATE','','','Inv Creation Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','INVOICE_DATE',222,'Invoice Date','INVOICE_DATE','','','','SA059956','DATE','','','Invoice Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','POINT_OF_TAXATION',222,'Point Of Taxation','POINT_OF_TAXATION','','','','SA059956','VARCHAR2','','','Point Of Taxation','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','SHIP_VIA',222,'Ship Via','SHIP_VIA','','','','SA059956','VARCHAR2','','','Ship Via','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','TAXABLE_ADDRESS',222,'Taxable Address','TAXABLE_ADDRESS','','','','SA059956','VARCHAR2','','','Taxable Address','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','BRANCH',222,'Branch','BRANCH','','','','SA059956','VARCHAR2','','','Branch','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','BRANCH_ADDRESS1',222,'Branch Address1','BRANCH_ADDRESS1','','','','SA059956','VARCHAR2','','','Branch Address1','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','BRANCH_CITY',222,'Branch City','BRANCH_CITY','','','','SA059956','VARCHAR2','','','Branch City','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','BRANCH_STATE',222,'Branch State','BRANCH_STATE','','','','SA059956','VARCHAR2','','','Branch State','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','BRANCH_POSTAL_CODE',222,'Branch Postal Code','BRANCH_POSTAL_CODE','','','','SA059956','VARCHAR2','','','Branch Postal Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','BRANCH_COUNTY',222,'Branch County','BRANCH_COUNTY','','','','SA059956','VARCHAR2','','','Branch County','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','BRANCH_GEOCODE',222,'Branch Geocode','BRANCH_GEOCODE','','','','SA059956','VARCHAR2','','','Branch Geocode','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','BILL_TO_CUSTOMER_NAME',222,'Bill To Customer Name','BILL_TO_CUSTOMER_NAME','','','','SA059956','VARCHAR2','','','Bill To Customer Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','BILL_TO_ADDRESS1',222,'Bill To Address1','BILL_TO_ADDRESS1','','','','SA059956','VARCHAR2','','','Bill To Address1','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','BILL_TO_CITY',222,'Bill To City','BILL_TO_CITY','','','','SA059956','VARCHAR2','','','Bill To City','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','BILL_TO_STATE',222,'Bill To State','BILL_TO_STATE','','','','SA059956','VARCHAR2','','','Bill To State','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','BILL_TO_POSTAL_CODE',222,'Bill To Postal Code','BILL_TO_POSTAL_CODE','','','','SA059956','VARCHAR2','','','Bill To Postal Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','BILL_TO_COUNTY',222,'Bill To County','BILL_TO_COUNTY','','','','SA059956','VARCHAR2','','','Bill To County','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','SHIP_TO_CUSTOMER_NAME',222,'Ship To Customer Name','SHIP_TO_CUSTOMER_NAME','','','','SA059956','VARCHAR2','','','Ship To Customer Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','SHIP_TO_ADDRESS1',222,'Ship To Address1','SHIP_TO_ADDRESS1','','','','SA059956','VARCHAR2','','','Ship To Address1','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','SHIP_TO_CITY',222,'Ship To City','SHIP_TO_CITY','','','','SA059956','VARCHAR2','','','Ship To City','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','SHIP_TO_STATE',222,'Ship To State','SHIP_TO_STATE','','','','SA059956','VARCHAR2','','','Ship To State','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','SHIP_TO_POSTAL_CODE',222,'Ship To Postal Code','SHIP_TO_POSTAL_CODE','','','','SA059956','VARCHAR2','','','Ship To Postal Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','SHIP_TO_COUNTY',222,'Ship To County','SHIP_TO_COUNTY','','','','SA059956','VARCHAR2','','','Ship To County','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','SHIP_TO_GEOCODE',222,'Ship To Geocode','SHIP_TO_GEOCODE','','','','SA059956','VARCHAR2','','','Ship To Geocode','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','JOB_CODE',222,'Job Code','JOB_CODE','','','','SA059956','VARCHAR2','','','Job Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','PO_NUMBER',222,'Po Number','PO_NUMBER','','','','SA059956','VARCHAR2','','','Po Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','SALES_ORDER',222,'Sales Order','SALES_ORDER','','','','SA059956','VARCHAR2','','','Sales Order','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','SALES_ORDER_LINE_NUM',222,'Sales Order Line Num','SALES_ORDER_LINE_NUM','','','','SA059956','NUMBER','','','Sales Order Line Num','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','INVOICE_LINE_NUM',222,'Invoice Line Num','INVOICE_LINE_NUM','','','','SA059956','NUMBER','','','Invoice Line Num','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','ITEM_NUMBER',222,'Item Number','ITEM_NUMBER','','','','SA059956','VARCHAR2','','','Item Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','DESCRIPTION',222,'Description','DESCRIPTION','','','','SA059956','VARCHAR2','','','Description','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','INVOICE_QTY',222,'Invoice Qty','INVOICE_QTY','','','','SA059956','NUMBER','','','Invoice Qty','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','UNIT_PRICE',222,'Unit Price','UNIT_PRICE','','','','SA059956','NUMBER','','','Unit Price','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','INVOICE_TOTAL',222,'Invoice Total','INVOICE_TOTAL','','','','SA059956','NUMBER','','','Invoice Total','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','GROSS_AMOUNT',222,'Gross Amount','GROSS_AMOUNT','','','','SA059956','NUMBER','','','Gross Amount','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','TAXABLE_AMOUNT',222,'Taxable Amount','TAXABLE_AMOUNT','','','','SA059956','NUMBER','','','Taxable Amount','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','TOTAL_FREIGHT',222,'Total Freight','TOTAL_FREIGHT','','','','SA059956','NUMBER','','','Total Freight','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','TOTAL_TAX',222,'Total Tax','TOTAL_TAX','','','','SA059956','NUMBER','','','Total Tax','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','AR_STATE_TAX',222,'Ar State Tax','AR_STATE_TAX','','','','SA059956','NUMBER','','','Ar State Tax','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','AR_COUNTY_TAX',222,'Ar County Tax','AR_COUNTY_TAX','','','','SA059956','NUMBER','','','Ar County Tax','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','AR_CITY_TAX',222,'Ar City Tax','AR_CITY_TAX','','','','SA059956','NUMBER','','','Ar City Tax','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','AR_EXEMPT_AMOUNT',222,'Ar Exempt Amount','AR_EXEMPT_AMOUNT','','','','SA059956','NUMBER','','','Ar Exempt Amount','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','ST_TXW_TAXLEVEL',222,'St Txw Taxlevel','ST_TXW_TAXLEVEL','','','','SA059956','VARCHAR2','','','St Txw Taxlevel','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','ST_TXW_TAXTYPE',222,'St Txw Taxtype','ST_TXW_TAXTYPE','','','','SA059956','VARCHAR2','','','St Txw Taxtype','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','ST_TXW_TAXAMT',222,'St Txw Taxamt','ST_TXW_TAXAMT','','','','SA059956','NUMBER','','','St Txw Taxamt','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','ST_TXW_TAXRATE',222,'St Txw Taxrate','ST_TXW_TAXRATE','','','','SA059956','NUMBER','','','St Txw Taxrate','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','ST_TXW_CMPLCODE',222,'St Txw Cmplcode','ST_TXW_CMPLCODE','','','','SA059956','VARCHAR2','','','St Txw Cmplcode','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','ST_TXW_EXMPTAMT',222,'St Txw Exmptamt','ST_TXW_EXMPTAMT','','','','SA059956','NUMBER','','','St Txw Exmptamt','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','ST_TXW_ADMNCODE',222,'St Txw Admncode','ST_TXW_ADMNCODE','','','','SA059956','VARCHAR2','','','St Txw Admncode','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','ST_TXW_TAXCERT',222,'St Txw Taxcert','ST_TXW_TAXCERT','','','','SA059956','VARCHAR2','','','St Txw Taxcert','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','ST_TXW_REASCODE',222,'St Txw Reascode','ST_TXW_REASCODE','','','','SA059956','VARCHAR2','','','St Txw Reascode','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','ST_TXW_BASEAMT',222,'St Txw Baseamt','ST_TXW_BASEAMT','','','','SA059956','NUMBER','','','St Txw Baseamt','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','ST_TXW_TAXCODE',222,'St Txw Taxcode','ST_TXW_TAXCODE','','','','SA059956','VARCHAR2','','','St Txw Taxcode','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MONTHLY_SALE_TAX_V','ST_TXW_EXCPCODE',222,'St Txw Excpcode','ST_TXW_EXCPCODE','','','','SA059956','VARCHAR2','','','St Txw Excpcode','','','','US','');
--Inserting Object Components for EIS_XXWC_MONTHLY_SALE_TAX_V
--Inserting Object Component Joins for EIS_XXWC_MONTHLY_SALE_TAX_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 222');
END IF;
END;
/
prompt Creating Report LOV Data for Monthly Sales Tax Report - WC
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(222);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - Monthly Sales Tax Report - WC
xxeis.eis_rsc_ins.lov( 222,'select distinct INVOICE_SOURCE  FROM XXCUS.XXCUS_TAX_RETURN_RPT_DETAIL_B order by 1','','XXWC Sales Tax Invoice Source LOV','','SA059956',NULL,'N','','','N','N','','','US');
xxeis.eis_rsc_ins.lov( 222,'SELECT DISTINCT ITEM_NUMBER  FROM XXCUS.XXCUS_TAX_RETURN_RPT_DETAIL_B ORDER BY LPAD(ITEM_NUMBER,10)','','XXWC Sales Tax Item Number LOV','','SA059956',NULL,'N','','','N','N','','','US');
xxeis.eis_rsc_ins.lov( 222,'SELECT DISTINCT DESCRIPTION  FROM XXCUS.XXCUS_TAX_RETURN_RPT_DETAIL_B ORDER BY 1','','XXWC Sales Tax Description LOV','','SA059956',NULL,'N','','','N','N','','','US');
xxeis.eis_rsc_ins.lov( 222,'SELECT DISTINCT custprodcode avp_code
FROM xxcus.xxcus_tax_return_rpt_detail_b
ORDER BY 1','','XXWC Sales Tax AVP Code LOV','','SA059956',NULL,'N','','','N','N','','','US');
xxeis.eis_rsc_ins.lov( 222,'SELECT DISTINCT JURIS_STNAME Taxing_State
FROM XXCUS.XXCUS_TAX_RETURN_RPT_DETAIL_B
ORDER BY 1','','XXWC Sales Taxing State LOV','','SA059956',NULL,'N','','','N','N','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 222');
END IF;
END;
/
prompt Creating Report Data for Monthly Sales Tax Report - WC
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(222);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Monthly Sales Tax Report - WC
xxeis.eis_rsc_utility.delete_report_rows( 'Monthly Sales Tax Report - WC',222 );
--Inserting Report - Monthly Sales Tax Report - WC
xxeis.eis_rsc_ins.r( 222,'Monthly Sales Tax Report - WC','','Monthly Sales Tax Report - WC','','','','SA059956','EIS_XXWC_MONTHLY_SALE_TAX_V','Y','','','SA059956','','N','White Cap Tax Reporting','','CSV,EXCEL,','N','','','','','','N','','US','','','','','','','','','','','','','','','','');
--Inserting Report Columns - Monthly Sales Tax Report - WC
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'AR_CITY_TAX','AR City Tax','Ar City Tax','','~~~','default','','45','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'AR_COUNTY_TAX','AR County Tax','Ar County Tax','','~~~','default','','44','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'AR_EXEMPT_AMOUNT','AR Exempt Amount','Ar Exempt Amount','','~~~','default','','46','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'AR_STATE_TAX','AR State Tax','Ar State Tax','','~~~','default','','43','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'BILL_TO_ADDRESS1','Bill To Address1','Bill To Address1','','','default','','17','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'BILL_TO_CITY','Bill To City','Bill To City','','','default','','18','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'BILL_TO_COUNTY','Bill To County','Bill To County','','','default','','21','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'BILL_TO_CUSTOMER_NAME','Bill To Customer Name','Bill To Customer Name','','','default','','16','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'BILL_TO_POSTAL_CODE','Bill To Postal Code','Bill To Postal Code','','','default','','20','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'BILL_TO_STATE','Bill To State','Bill To State','','','default','','19','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'BRANCH','Branch','Branch','','','default','','9','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'BRANCH_ADDRESS1','Branch Address1','Branch Address1','','','default','','10','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'BRANCH_CITY','Branch City','Branch City','','','default','','11','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'BRANCH_COUNTY','Branch County','Branch County','','','default','','14','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'BRANCH_GEOCODE','Branch Geocode','Branch Geocode','','','default','','15','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'BRANCH_POSTAL_CODE','Branch Postal Code','Branch Postal Code','','','default','','13','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'BRANCH_STATE','Branch State','Branch State','','','default','','12','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'CN_TXW_ADMNCODE','Cn Txw Admncode','Cn Txw Admncode','','','','','65','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'CN_TXW_BASEAMT','Cn Txw Baseamt','Cn Txw Baseamt','','~~~','','','68','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'CN_TXW_CMPLCODE','Cn Txw Cmplcode','Cn Txw Cmplcode','','','','','63','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'CN_TXW_EXCPCODE','Cn Txw Excpcode','Cn Txw Excpcode','','','','','64','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'CN_TXW_EXMPTAMT','Cn Txw Exmptamt','Cn Txw Exmptamt','','~~~','','','70','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'CN_TXW_REASCODE','Cn Txw Reascode','Cn Txw Reascode','','','','','67','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'CN_TXW_TAXAMT','Cn Txw Taxamt','Cn Txw Taxamt','','~~~','','','61','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'CN_TXW_TAXCERT','Cn Txw Taxcert','Cn Txw Taxcert','','','','','66','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'CN_TXW_TAXCODE','Cn Txw Taxcode','Cn Txw Taxcode','','','','','69','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'CN_TXW_TAXLEVEL','Cn Txw Taxlevel','Cn Txw Taxlevel','','','','','59','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'CN_TXW_TAXRATE','Cn Txw Taxrate','Cn Txw Taxrate','','~~~','','','62','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'CN_TXW_TAXTYPE','Cn Txw Taxtype','Cn Txw Taxtype','','','','','60','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'CUSTPRODCODE','Custprodcode','Custprodcode','','','','','120','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'DESCRIPTION','Description','Description','','','default','','35','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'GROSS_AMOUNT','Gross Amount','Gross Amount','','~~~','default','','39','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'INV_CREATION_DATE','Inv Creation Date','Inv Creation Date','','','default','','4','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'INVOICE_DATE','Invoice Date','Invoice Date','','','default','','5','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'INVOICE_LINE_NUM','Invoice Line Num','Invoice Line Num','','~~~','default','','33','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'INVOICE_NUMBER','Invoice Number','Invoice Number','','','default','','3','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'INVOICE_QTY','Invoice Qty','Invoice Qty','','~~~','default','','36','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'INVOICE_SOURCE','Invoice Source','Invoice Source','','','default','','2','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'INVOICE_TOTAL','Invoice Total','Invoice Total','','~~~','default','','38','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'ITEM_NUMBER','Item Number','Item Number','','','default','','34','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'JOB_CODE','Job Code','Job Code','','','default','','29','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'JURIS_CITYNAME','Juris Cityname','Juris Cityname','','','','','131','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'JURIS_CNTYCODE','Juris Cntycode','Juris Cntycode','','','','','125','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'JURIS_CNTYNAME','Juris Cntyname','Juris Cntyname','','','','','130','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'JURIS_GEOCODE','Juris Geocode','Juris Geocode','','','','','126','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'JURIS_STCODE','Juris Stcode','Juris Stcode','','','','','124','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'JURIS_STNAME','Juris Stname','Juris Stname','','','','','129','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'JURIS_ZIPCODE','Juris Zipcode','Juris Zipcode','','','','','127','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'JURIS_ZIPEXTN','Juris Zipextn','Juris Zipextn','','','','','128','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'JURISNO','Jurisno','Jurisno','','~~~','','','123','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'JURISTYPE','Juristype','Juristype','','','','','121','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'LO_TXW_ADMNCODE','Lo Txw Admncode','Lo Txw Admncode','','','','','77','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'LO_TXW_BASEAMT','Lo Txw Baseamt','Lo Txw Baseamt','','~~~','','','80','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'LO_TXW_CMPLCODE','Lo Txw Cmplcode','Lo Txw Cmplcode','','','','','75','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'LO_TXW_EXCPCODE','Lo Txw Excpcode','Lo Txw Excpcode','','','','','82','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'LO_TXW_EXMPTAMT','Lo Txw Exmptamt','Lo Txw Exmptamt','','~~~','','','76','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'LO_TXW_REASCODE','Lo Txw Reascode','Lo Txw Reascode','','','','','79','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'LO_TXW_TAXAMT','Lo Txw Taxamt','Lo Txw Taxamt','','~~~','','','73','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'LO_TXW_TAXCERT','Lo Txw Taxcert','Lo Txw Taxcert','','','','','78','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'LO_TXW_TAXCODE','Lo Txw Taxcode','Lo Txw Taxcode','','','','','81','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'LO_TXW_TAXLEVEL','Lo Txw Taxlevel','Lo Txw Taxlevel','','','','','71','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'LO_TXW_TAXRATE','Lo Txw Taxrate','Lo Txw Taxrate','','~~~','','','74','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'LO_TXW_TAXTYPE','Lo Txw Taxtype','Lo Txw Taxtype','','','','','72','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'PERIOD','Period','Period','','','default','','1','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'PO_NUMBER','PO Number','Po Number','','','default','','30','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'POINT_OF_TAXATION','Point Of Taxation','Point Of Taxation','','','default','','6','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'POT','Pot','Pot','','','','','122','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'REQUEST_ID','Request Id','Request Id','','~~~','','','132','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'SALES_ORDER','Sales Order','Sales Order','','','default','','31','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'SALES_ORDER_LINE_NUM','Sales Order Line Num','Sales Order Line Num','','~~~','default','','32','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'SC_TXW_ADMNCODE','Sc Txw Admncode','Sc Txw Admncode','','','','','101','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'SC_TXW_BASEAMT','Sc Txw Baseamt','Sc Txw Baseamt','','~~~','','','104','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'SC_TXW_CMPLCODE','Sc Txw Cmplcode','Sc Txw Cmplcode','','','','','99','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'SC_TXW_EXCPCODE','Sc Txw Excpcode','Sc Txw Excpcode','','','','','106','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'SC_TXW_EXMPTAMT','Sc Txw Exmptamt','Sc Txw Exmptamt','','~~~','','','100','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'SC_TXW_REASCODE','Sc Txw Reascode','Sc Txw Reascode','','','','','103','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'SC_TXW_TAXAMT','Sc Txw Taxamt','Sc Txw Taxamt','','~~~','','','97','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'SC_TXW_TAXCERT','Sc Txw Taxcert','Sc Txw Taxcert','','','','','102','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'SC_TXW_TAXCODE','Sc Txw Taxcode','Sc Txw Taxcode','','','','','105','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'SC_TXW_TAXLEVEL','Sc Txw Taxlevel','Sc Txw Taxlevel','','','','','95','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'SC_TXW_TAXRATE','Sc Txw Taxrate','Sc Txw Taxrate','','~~~','','','98','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'SC_TXW_TAXTYPE','Sc Txw Taxtype','Sc Txw Taxtype','','','','','96','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'SHIP_TO_ADDRESS1','Ship To Address1','Ship To Address1','','','default','','23','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'SHIP_TO_CITY','Ship To City','Ship To City','','','default','','24','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'SHIP_TO_COUNTY','Ship To County','Ship To County','','','default','','27','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'SHIP_TO_CUSTOMER_NAME','Ship To Customer Name','Ship To Customer Name','','','default','','22','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'SHIP_TO_GEOCODE','Ship To Geocode','Ship To Geocode','','','default','','28','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'SHIP_TO_POSTAL_CODE','Ship To Postal Code','Ship To Postal Code','','','default','','26','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'SHIP_TO_STATE','Ship To State','Ship To State','','','default','','25','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'SHIP_VIA','Ship Via','Ship Via','','','default','','7','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'SL_TXW_ADMNCODE','Sl Txw Admncode','Sl Txw Admncode','','','','','113','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'SL_TXW_BASEAMT','Sl Txw Baseamt','Sl Txw Baseamt','','~~~','','','116','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'SL_TXW_CMPLCODE','Sl Txw Cmplcode','Sl Txw Cmplcode','','','','','111','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'SL_TXW_EXCPCODE','Sl Txw Excpcode','Sl Txw Excpcode','','','','','118','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'SL_TXW_EXMPTAMT','Sl Txw Exmptamt','Sl Txw Exmptamt','','~~~','','','112','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'SL_TXW_REASCODE','Sl Txw Reascode','Sl Txw Reascode','','','','','115','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'SL_TXW_TAXAMT','Sl Txw Taxamt','Sl Txw Taxamt','','~~~','','','109','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'SL_TXW_TAXCERT','Sl Txw Taxcert','Sl Txw Taxcert','','','','','114','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'SL_TXW_TAXCODE','Sl Txw Taxcode','Sl Txw Taxcode','','','','','117','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'SL_TXW_TAXLEVEL','Sl Txw Taxlevel','Sl Txw Taxlevel','','','','','107','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'SL_TXW_TAXRATE','Sl Txw Taxrate','Sl Txw Taxrate','','~~~','','','110','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'SL_TXW_TAXTYPE','Sl Txw Taxtype','Sl Txw Taxtype','','','','','108','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'SS_TXW_ADMNCODE','Ss Txw Admncode','Ss Txw Admncode','','','','','89','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'SS_TXW_BASEAMT','Ss Txw Baseamt','Ss Txw Baseamt','','~~~','','','92','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'SS_TXW_CMPLCODE','Ss Txw Cmplcode','Ss Txw Cmplcode','','','','','87','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'SS_TXW_EXCPCODE','Ss Txw Excpcode','Ss Txw Excpcode','','','','','88','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'SS_TXW_EXMPTAMT','Ss Txw Exmptamt','Ss Txw Exmptamt','','~~~','','','94','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'SS_TXW_REASCODE','Ss Txw Reascode','Ss Txw Reascode','','','','','91','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'SS_TXW_TAXAMT','Ss Txw Taxamt','Ss Txw Taxamt','','~~~','','','85','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'SS_TXW_TAXCERT','Ss Txw Taxcert','Ss Txw Taxcert','','','','','90','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'SS_TXW_TAXCODE','Ss Txw Taxcode','Ss Txw Taxcode','','','','','93','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'SS_TXW_TAXLEVEL','Ss Txw Taxlevel','Ss Txw Taxlevel','','','','','83','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'SS_TXW_TAXRATE','Ss Txw Taxrate','Ss Txw Taxrate','','~~~','','','86','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'SS_TXW_TAXTYPE','Ss Txw Taxtype','Ss Txw Taxtype','','','','','84','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'ST_TXW_ADMNCODE','St Txw Admncode','St Txw Admncode','','','','','53','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'ST_TXW_BASEAMT','St Txw Baseamt','St Txw Baseamt','','~~~','','','56','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'ST_TXW_CMPLCODE','St Txw Cmplcode','St Txw Cmplcode','','','','','51','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'ST_TXW_EXCPCODE','St Txw Excpcode','St Txw Excpcode','','','','','58','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'ST_TXW_EXMPTAMT','St Txw Exmptamt','St Txw Exmptamt','','~~~','','','52','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'ST_TXW_REASCODE','St Txw Reascode','St Txw Reascode','','','','','55','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'ST_TXW_TAXAMT','St Txw Taxamt','St Txw Taxamt','','~~~','default','','49','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'ST_TXW_TAXCERT','St Txw Taxcert','St Txw Taxcert','','','','','54','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'ST_TXW_TAXCODE','St Txw Taxcode','St Txw Taxcode','','','','','57','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'ST_TXW_TAXLEVEL','St Txw Taxlevel','St Txw Taxlevel','','','default','','47','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'ST_TXW_TAXRATE','St Txw Taxrate','St Txw Taxrate','','~~~','default','','50','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'ST_TXW_TAXTYPE','St Txw Taxtype','St Txw Taxtype','','','default','','48','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'TAXABLE_ADDRESS','Taxable Address','Taxable Address','','','default','','8','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'TAXABLE_AMOUNT','Taxable Amount','Taxable Amount','','~~~','default','','40','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'TAXWARE_MATCH','Taxware Match','Taxware Match','','','','','133','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'TOTAL_FREIGHT','Total Freight','Total Freight','','~~~','default','','41','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'TOTAL_TAX','Total Tax','Total Tax','','~~~','default','','42','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'TXW_REFERENCE','Txw Reference','Txw Reference','','','','','119','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Monthly Sales Tax Report - WC',222,'UNIT_PRICE','Unit Price','Unit Price','','~~~','default','','37','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','US','','');
--Inserting Report Parameters - Monthly Sales Tax Report - WC
xxeis.eis_rsc_ins.rp( 'Monthly Sales Tax Report - WC',222,'AVP Code','Custprodcode','CUSTPRODCODE','IN','XXWC Sales Tax AVP Code LOV','','VARCHAR2','N','Y','4','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Monthly Sales Tax Report - WC',222,'Description','Description','DESCRIPTION','IN','XXWC Sales Tax Description LOV','','VARCHAR2','N','Y','3','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Monthly Sales Tax Report - WC',222,'Invoice Source','Invoice Source','INVOICE_SOURCE','IN','XXWC Sales Tax Invoice Source LOV','','VARCHAR2','N','Y','1','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Monthly Sales Tax Report - WC',222,'Item Number','Item Number','ITEM_NUMBER','IN','XXWC Sales Tax Item Number LOV','','VARCHAR2','N','Y','2','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Monthly Sales Tax Report - WC',222,'Taxing State','Juris Stname','JURIS_STNAME','IN','XXWC Sales Taxing State LOV','','VARCHAR2','N','Y','5','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','US','');
--Inserting Dependent Parameters - Monthly Sales Tax Report - WC
--Inserting Report Conditions - Monthly Sales Tax Report - WC
xxeis.eis_rsc_ins.rcnh( 'Monthly Sales Tax Report - WC',222,'EXMSTV.INVOICE_SOURCE IN Invoice Source','SIMPLE','','','Y','','1');
xxeis.eis_rsc_ins.rcnd( '','','INVOICE_SOURCE','','Invoice Source','','','','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','','','IN','Y','Y','','','','','1',222,'Monthly Sales Tax Report - WC','EXMSTV.INVOICE_SOURCE IN Invoice Source');
xxeis.eis_rsc_ins.rcnh( 'Monthly Sales Tax Report - WC',222,'EXMSTV.ITEM_NUMBER IN Item Number','SIMPLE','','','Y','','2');
xxeis.eis_rsc_ins.rcnd( '','','ITEM_NUMBER','','Item Number','','','','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','','','IN','Y','Y','','','','','1',222,'Monthly Sales Tax Report - WC','EXMSTV.ITEM_NUMBER IN Item Number');
xxeis.eis_rsc_ins.rcnh( 'Monthly Sales Tax Report - WC',222,'EXMSTV.DESCRIPTION IN Description','SIMPLE','','','Y','','3');
xxeis.eis_rsc_ins.rcnd( '','','DESCRIPTION','','Description','','','','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','','','IN','Y','Y','','','','','1',222,'Monthly Sales Tax Report - WC','EXMSTV.DESCRIPTION IN Description');
xxeis.eis_rsc_ins.rcnh( 'Monthly Sales Tax Report - WC',222,'EXMSTV.CUSTPRODCODE IN AVP Code','SIMPLE','','','Y','','4');
xxeis.eis_rsc_ins.rcnd( '','','CUSTPRODCODE','','AVP Code','','','','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','','','IN','Y','Y','','','','','1',222,'Monthly Sales Tax Report - WC','EXMSTV.CUSTPRODCODE IN AVP Code');
xxeis.eis_rsc_ins.rcnh( 'Monthly Sales Tax Report - WC',222,'EXMSTV.JURIS_STNAME IN Taxing State','SIMPLE','','','Y','','5');
xxeis.eis_rsc_ins.rcnd( '','','JURIS_STNAME','','Taxing State','','','','','EIS_XXWC_MONTHLY_SALE_TAX_V','','','','','','IN','Y','Y','','','','','1',222,'Monthly Sales Tax Report - WC','EXMSTV.JURIS_STNAME IN Taxing State');
--Inserting Report Sorts - Monthly Sales Tax Report - WC
xxeis.eis_rsc_ins.rs( 'Monthly Sales Tax Report - WC',222,'PERIOD','ASC','SA059956','1','');
xxeis.eis_rsc_ins.rs( 'Monthly Sales Tax Report - WC',222,'INVOICE_NUMBER','ASC','SA059956','2','');
xxeis.eis_rsc_ins.rs( 'Monthly Sales Tax Report - WC',222,'INVOICE_LINE_NUM','ASC','SA059956','3','');
--Inserting Report Triggers - Monthly Sales Tax Report - WC
--inserting report templates - Monthly Sales Tax Report - WC
--Inserting Report Portals - Monthly Sales Tax Report - WC
--inserting report dashboards - Monthly Sales Tax Report - WC
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Monthly Sales Tax Report - WC','222','EIS_XXWC_MONTHLY_SALE_TAX_V','EIS_XXWC_MONTHLY_SALE_TAX_V','N','');
--inserting report security - Monthly Sales Tax Report - WC
xxeis.eis_rsc_ins.rsec( 'Monthly Sales Tax Report - WC','222','','HDS_RCVBLS_MNGR_WC',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Monthly Sales Tax Report - WC','222','','XXWC_RECEIVABLES_INQUIRY_WC',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Monthly Sales Tax Report - WC','222','','XXWC_CRE_CREDIT_COLL_MGR_NOREC',222,'SA059956','','','');
--Inserting Report Pivots - Monthly Sales Tax Report - WC
--Inserting Report Distribution Details 
--Inserting Report Distribution Details 
--Inserting Report SubTotals Details 
--Inserting Report aggregations 
--Inserting Report   Version details- Monthly Sales Tax Report - WC
xxeis.eis_rsc_ins.rv( 'Monthly Sales Tax Report - WC','','Monthly Sales Tax Report - WC','SA059956','18-SEP-2018');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 222');
END IF;
END;
/
