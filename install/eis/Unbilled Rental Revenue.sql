--Report Name            : Unbilled Rental Revenue
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating View Data for Unbilled Rental Revenue
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_INV_UNINVODR_DTL_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_INV_UNINVODR_DTL_V',401,'','','','','MR020532','XXEIS','Eis Om Uninv Order Detail V','EXIUDV','','');
--Delete View Columns for EIS_XXWC_INV_UNINVODR_DTL_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_INV_UNINVODR_DTL_V',401,FALSE);
--Inserting View Columns for EIS_XXWC_INV_UNINVODR_DTL_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_UNINVODR_DTL_V','CUSTOMER_NAME',401,'Customer Name','CUSTOMER_NAME','','','','MR020532','VARCHAR2','','','Customer Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_UNINVODR_DTL_V','LAST_ORDER_NUM',401,'Last Order Num','LAST_ORDER_NUM','','','','MR020532','NUMBER','','','Last Order Num','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_UNINVODR_DTL_V','LAST_INVOICE_NUM',401,'Last Invoice Num','LAST_INVOICE_NUM','','','','MR020532','VARCHAR2','','','Last Invoice Num','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_UNINVODR_DTL_V','LAST_BILLED_DATE',401,'Last Billed Date','LAST_BILLED_DATE','','','','MR020532','DATE','','','Last Billed Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_UNINVODR_DTL_V','ORDERED_QUANTITY',401,'Ordered Quantity','ORDERED_QUANTITY','','','','MR020532','NUMBER','','','Ordered Quantity','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_UNINVODR_DTL_V','CUSTOMER_NUMBER',401,'Customer Number','CUSTOMER_NUMBER','','','','MR020532','VARCHAR2','','','Customer Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_UNINVODR_DTL_V','DESCRIPTION',401,'Description','DESCRIPTION','','','','MR020532','VARCHAR2','','','Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_UNINVODR_DTL_V','NEW_SELLING_PRICE',401,'New Selling Price','NEW_SELLING_PRICE','','','','MR020532','NUMBER','','','New Selling Price','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_UNINVODR_DTL_V','PART_NUMBER',401,'Part Number','PART_NUMBER','','','','MR020532','VARCHAR2','','','Part Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_UNINVODR_DTL_V','SEGMENT_NUMBER',401,'Segment Number','SEGMENT_NUMBER','','','','MR020532','VARCHAR2','','','Segment Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_UNINVODR_DTL_V','LOCATION',401,'Location','LOCATION','','','','MR020532','VARCHAR2','','','Location','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_UNINVODR_DTL_V','TOTAL_DAYS',401,'Total Days','TOTAL_DAYS','','','','MR020532','NUMBER','','','Total Days','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_UNINVODR_DTL_V','UNBILLED_RENTAL_AMOUNT',401,'Unbilled Rental Amount','UNBILLED_RENTAL_AMOUNT','','~T~D~2','','MR020532','NUMBER','','','Unbilled Rental Amount','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_UNINVODR_DTL_V','UNBILLED_RENTAL_DAYS',401,'Unbilled Rental Days','UNBILLED_RENTAL_DAYS','','','','MR020532','NUMBER','','','Unbilled Rental Days','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_UNINVODR_DTL_V','SALESREP_ID',401,'Salesrep Id','SALESREP_ID','','','','MR020532','VARCHAR2','','','Salesrep Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_UNINVODR_DTL_V','PERIOD_NAME',401,'Period Name','PERIOD_NAME','','','','MR020532','VARCHAR2','','','Period Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_UNINVODR_DTL_V','ORDER_TYPE',401,'Order Type','ORDER_TYPE','','','','MR020532','VARCHAR2','','','Order Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_UNINVODR_DTL_V','CATCLASS_DESC',401,'Catclass Desc','CATCLASS_DESC','','','','MR020532','VARCHAR2','','','Catclass Desc','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_UNINVODR_DTL_V','CAT_CLASS',401,'Cat Class','CAT_CLASS','','','','MR020532','VARCHAR2','','','Cat Class','','','');
--Inserting View Components for EIS_XXWC_INV_UNINVODR_DTL_V
--Inserting View Component Joins for EIS_XXWC_INV_UNINVODR_DTL_V
END;
/
set scan on define on
prompt Creating Report LOV Data for Unbilled Rental Revenue
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Unbilled Rental Revenue
xxeis.eis_rs_ins.lov( 401,'select  distinct per.period_name , per.period_year, per.period_num, per.start_date, per.end_date
from    gl_periods per,gl_ledgers led
where  per.period_set_name = led.period_set_name
and     led.accounted_period_type = per.period_type
order by per.period_year desc,per.period_num asc','null','EIS INV PERIOD NAME','Derives GL Periods based on the corresponding setups in the sets of books','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'select nvl( party.party_name,CUST_ACCT.account_name) customer_name,CUST_ACCT.ACCOUNT_NUMBER
from  HZ_PARTIES PARTY, HZ_CUST_ACCOUNTS CUST_ACCT where CUST_ACCT.PARTY_ID = PARTY.PARTY_ID AND  PARTY.status=''A''
','','INV CUSTOMER NAME','This gives the Customer Name','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'select  cust_acct.account_number Customer_Number,cust_acct.account_name customer_name,party.party_name
from  HZ_PARTIES PARTY, HZ_CUST_ACCOUNTS CUST_ACCT
 where CUST_ACCT.PARTY_ID = PARTY.PARTY_ID','','INV CUSTOMER NUMBER','This gives the Customer Number','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'SELECT organization_code code, organization_name name
FROM ORG_ORGANIZATION_DEFINITIONS OOD
WHERE EXISTS
  (SELECT 1
  FROM XXEIS.EIS_ORG_ACCESS_V
  WHERE organization_id = ood.organization_id
  )
ORDER BY organization_code','','EIS XXWC INV ORGS','XXWC list of inventort orgs','ANONYMOUS',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Unbilled Rental Revenue
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Unbilled Rental Revenue
xxeis.eis_rs_utility.delete_report_rows( 'Unbilled Rental Revenue' );
--Inserting Report - Unbilled Rental Revenue
xxeis.eis_rs_ins.r( 401,'Unbilled Rental Revenue','','Provide a total listing of all unbilled rental revenue by branch (Customer Number, Part Number, Description, Last Invoice Number, Last Order Number, Sales Rep ID, Quantity, New Selling Price, Last Billing Date, Unbilled Days, Unbilled Revenue)','','','','MR020532','EIS_XXWC_INV_UNINVODR_DTL_V','Y','','','MR020532','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - Unbilled Rental Revenue
xxeis.eis_rs_ins.rc( 'Unbilled Rental Revenue',401,'CUSTOMER_NAME','Customer Name','Customer Name','','','default','','4','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_UNINVODR_DTL_V','','');
xxeis.eis_rs_ins.rc( 'Unbilled Rental Revenue',401,'CUSTOMER_NUMBER','Customer Number','Customer Number','','','default','','3','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_UNINVODR_DTL_V','','');
xxeis.eis_rs_ins.rc( 'Unbilled Rental Revenue',401,'LAST_BILLED_DATE','Last Billing Date','Last Billed Date','','','default','','14','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_UNINVODR_DTL_V','','');
xxeis.eis_rs_ins.rc( 'Unbilled Rental Revenue',401,'LAST_INVOICE_NUM','Last Invoice Number','Last Invoice Num','','','default','','9','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_UNINVODR_DTL_V','','');
xxeis.eis_rs_ins.rc( 'Unbilled Rental Revenue',401,'LAST_ORDER_NUM','Order Number','Last Order Num','','~~~','default','','10','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_UNINVODR_DTL_V','','');
xxeis.eis_rs_ins.rc( 'Unbilled Rental Revenue',401,'ORDERED_QUANTITY','Quantity','Ordered Quantity','','~~~','default','','12','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_UNINVODR_DTL_V','','');
xxeis.eis_rs_ins.rc( 'Unbilled Rental Revenue',401,'DESCRIPTION','Description','Description','','','default','','6','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_UNINVODR_DTL_V','','');
xxeis.eis_rs_ins.rc( 'Unbilled Rental Revenue',401,'NEW_SELLING_PRICE','New Selling Price','New Selling Price','','~T~D~2','default','','13','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_UNINVODR_DTL_V','','');
xxeis.eis_rs_ins.rc( 'Unbilled Rental Revenue',401,'PART_NUMBER','Part Number','Part Number','','','default','','5','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_UNINVODR_DTL_V','','');
xxeis.eis_rs_ins.rc( 'Unbilled Rental Revenue',401,'SEGMENT_NUMBER','Segment Number','Segment Number','','','default','','2','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_UNINVODR_DTL_V','','');
xxeis.eis_rs_ins.rc( 'Unbilled Rental Revenue',401,'LOCATION','Location','Location','','','default','','1','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_UNINVODR_DTL_V','','');
xxeis.eis_rs_ins.rc( 'Unbilled Rental Revenue',401,'UNBILLED_RENTAL_AMOUNT','Unbilled Rental Amount','Unbilled Rental Amount','','~T~D~2','default','','16','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_UNINVODR_DTL_V','','');
xxeis.eis_rs_ins.rc( 'Unbilled Rental Revenue',401,'UNBILLED_RENTAL_DAYS','Unbilled Rental Days','Unbilled Rental Days','','~~~','default','','15','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_UNINVODR_DTL_V','','');
xxeis.eis_rs_ins.rc( 'Unbilled Rental Revenue',401,'TOTAL_DAYS','Total Days','Total Days','','~~~','default','','17','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_UNINVODR_DTL_V','','');
xxeis.eis_rs_ins.rc( 'Unbilled Rental Revenue',401,'SALESREP_ID','Salesrep Name','Salesrep Id','','','default','','11','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_UNINVODR_DTL_V','','');
xxeis.eis_rs_ins.rc( 'Unbilled Rental Revenue',401,'PERIOD_NAME','Period Name','Period Name','','','default','','18','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_UNINVODR_DTL_V','','');
xxeis.eis_rs_ins.rc( 'Unbilled Rental Revenue',401,'ORDER_TYPE','Order Type','Order Type','','','default','','19','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_UNINVODR_DTL_V','','');
xxeis.eis_rs_ins.rc( 'Unbilled Rental Revenue',401,'CATCLASS_DESC','Catclass Desc','Catclass Desc','','','','','8','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_UNINVODR_DTL_V','','');
xxeis.eis_rs_ins.rc( 'Unbilled Rental Revenue',401,'CAT_CLASS','Cat Class','Cat Class','','','','','7','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_UNINVODR_DTL_V','','');
--Inserting Report Parameters - Unbilled Rental Revenue
xxeis.eis_rs_ins.rp( 'Unbilled Rental Revenue',401,'Customer Name','Customer Name','CUSTOMER_NAME','IN','INV CUSTOMER NAME','','VARCHAR2','N','Y','4','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Unbilled Rental Revenue',401,'Customer Number','Customer Number','CUSTOMER_NUMBER','IN','INV CUSTOMER NUMBER','','VARCHAR2','N','Y','3','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Unbilled Rental Revenue',401,'Fiscal Month','Fiscal Month','PERIOD_NAME','IN','EIS INV PERIOD NAME','','VARCHAR2','Y','Y','2','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Unbilled Rental Revenue',401,'Location','Location','LOCATION','IN','EIS XXWC INV ORGS','','VARCHAR2','Y','Y','1','','Y','CONSTANT','MR020532','Y','N','','','');
--Inserting Report Conditions - Unbilled Rental Revenue
xxeis.eis_rs_ins.rcn( 'Unbilled Rental Revenue',401,'CUSTOMER_NAME','IN',':Customer Name','','','Y','4','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Unbilled Rental Revenue',401,'CUSTOMER_NUMBER','IN',':Customer Number','','','Y','3','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Unbilled Rental Revenue',401,'LOCATION','IN',':Location','','','Y','1','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Unbilled Rental Revenue',401,'PERIOD_NAME','IN',':Fiscal Month','','','Y','2','Y','MR020532');
--Inserting Report Sorts - Unbilled Rental Revenue
xxeis.eis_rs_ins.rs( 'Unbilled Rental Revenue',401,'LOCATION','ASC','MR020532','','');
--Inserting Report Triggers - Unbilled Rental Revenue
xxeis.eis_rs_ins.rt( 'Unbilled Rental Revenue',401,'begin
xxeis.EIS_RS_XXWC_COM_UTIL_PKG.Set_period_name_from(:Fiscal Month);
end;','B','Y','MR020532');
--Inserting Report Templates - Unbilled Rental Revenue
--Inserting Report Portals - Unbilled Rental Revenue
--Inserting Report Dashboards - Unbilled Rental Revenue
--Inserting Report Security - Unbilled Rental Revenue
xxeis.eis_rs_ins.rsec( 'Unbilled Rental Revenue','','LB048272','',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Unbilled Rental Revenue','','10012196','',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Unbilled Rental Revenue','','MM050208','',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Unbilled Rental Revenue','','SO004816','',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Unbilled Rental Revenue','401','','50851',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Unbilled Rental Revenue','401','','50619',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Unbilled Rental Revenue','401','','51004',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Unbilled Rental Revenue','20005','','50900',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Unbilled Rental Revenue','401','','20634',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Unbilled Rental Revenue','401','','50882',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Unbilled Rental Revenue','707','','51104',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Unbilled Rental Revenue','401','','50924',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Unbilled Rental Revenue','401','','51052',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Unbilled Rental Revenue','401','','50879',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Unbilled Rental Revenue','401','','50852',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Unbilled Rental Revenue','401','','50821',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Unbilled Rental Revenue','20005','','50880',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Unbilled Rental Revenue','401','','51029',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Unbilled Rental Revenue','660','','51065',401,'MR020532','','');
--Inserting Report Pivots - Unbilled Rental Revenue
xxeis.eis_rs_ins.rpivot( 'Unbilled Rental Revenue',401,'Pivot','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rs_ins.rpivot_dtls( 'Unbilled Rental Revenue',401,'Pivot','CUSTOMER_NAME','ROW_FIELD','','','1','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Unbilled Rental Revenue',401,'Pivot','ORDERED_QUANTITY','DATA_FIELD','SUM','','1','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Unbilled Rental Revenue',401,'Pivot','NEW_SELLING_PRICE','DATA_FIELD','MAX','','2','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Unbilled Rental Revenue',401,'Pivot','PART_NUMBER','ROW_FIELD','','','2','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Unbilled Rental Revenue',401,'Pivot','UNBILLED_RENTAL_AMOUNT','DATA_FIELD','SUM','','4','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Unbilled Rental Revenue',401,'Pivot','UNBILLED_RENTAL_DAYS','DATA_FIELD','SUM','','3','1','xlNormal');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
END;
/
set scan on define on
