--Report Name            : Manual Price Override Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data EIS_XXWC_MAN_PRICE_OVERRIDE_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_MAN_PRICE_OVERRIDE_V
xxeis.eis_rsc_ins.v( 'EIS_XXWC_MAN_PRICE_OVERRIDE_V',660,'','','','','SA059956','XXEIS','Eis Xxwc Om Manual Price Override V','EXMPOV','','','VIEW','US','','EIS_XXWC_OM_MAN_PRICE_V','');
--Delete Object Columns for EIS_XXWC_MAN_PRICE_OVERRIDE_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_MAN_PRICE_OVERRIDE_V',660,FALSE);
--Inserting Object Columns for EIS_XXWC_MAN_PRICE_OVERRIDE_V
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MAN_PRICE_OVERRIDE_V','FINAL_SELLING_PRICE',660,'Final Selling Price','FINAL_SELLING_PRICE','','','','SA059956','NUMBER','','','Final Selling Price','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MAN_PRICE_OVERRIDE_V','LIST_PRICE',660,'List Price','LIST_PRICE','','','','SA059956','NUMBER','','','List Price','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MAN_PRICE_OVERRIDE_V','QUANTITY',660,'Quantity','QUANTITY','','','','SA059956','NUMBER','','','Quantity','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MAN_PRICE_OVERRIDE_V','ITEM_DESCRIPTION',660,'Item Description','ITEM_DESCRIPTION','','','','SA059956','VARCHAR2','','','Item Description','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MAN_PRICE_OVERRIDE_V','ITEM',660,'Item','ITEM','','','','SA059956','VARCHAR2','','','Item','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MAN_PRICE_OVERRIDE_V','CREATED_BY',660,'Created By','CREATED_BY','','','','SA059956','VARCHAR2','','','Created By','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MAN_PRICE_OVERRIDE_V','CUSTOMER_NUMBER',660,'Customer Number','CUSTOMER_NUMBER','','','','SA059956','VARCHAR2','','','Customer Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MAN_PRICE_OVERRIDE_V','CUSTOMER',660,'Customer','CUSTOMER','','','','SA059956','VARCHAR2','','','Customer','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MAN_PRICE_OVERRIDE_V','BRANCH',660,'Branch','BRANCH','','','','SA059956','VARCHAR2','','','Branch','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MAN_PRICE_OVERRIDE_V','PRICING_ZONE',660,'Pricing Zone','PRICING_ZONE','','','','SA059956','VARCHAR2','','','Pricing Zone','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MAN_PRICE_OVERRIDE_V','APPLICATION_METHOD',660,'Application Method','APPLICATION_METHOD','','','','SA059956','VARCHAR2','','','Application Method','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MAN_PRICE_OVERRIDE_V','ORIGINAL_UNIT_SELLING_PRICE',660,'Original Unit Selling Price','ORIGINAL_UNIT_SELLING_PRICE','','','','SA059956','NUMBER','','','Original Unit Selling Price','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MAN_PRICE_OVERRIDE_V','REASON_CODE',660,'Reason Code','REASON_CODE','','','','SA059956','VARCHAR2','','','Reason Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MAN_PRICE_OVERRIDE_V','ORDERED_DATE',660,'Ordered Date','ORDERED_DATE','','','','SA059956','DATE','','','Ordered Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MAN_PRICE_OVERRIDE_V','CAT_CLASS',660,'Cat Class','CAT_CLASS','','','','SA059956','VARCHAR2','','','Cat Class','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MAN_PRICE_OVERRIDE_V','CAT_CLASS_DESC',660,'Cat Class Desc','CAT_CLASS_DESC','','','','SA059956','VARCHAR2','','','Cat Class Desc','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MAN_PRICE_OVERRIDE_V','REGION',660,'Region','REGION','','','','SA059956','VARCHAR2','','','Region','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MAN_PRICE_OVERRIDE_V','MODIFER_NUMBER',660,'Modifer Number','MODIFER_NUMBER','','','','SA059956','VARCHAR2','','','Modifer Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MAN_PRICE_OVERRIDE_V','ORDER_NUMBER',660,'Order Number','ORDER_NUMBER','','','','SA059956','NUMBER','','','Order Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MAN_PRICE_OVERRIDE_V','ACCOUNT_MANAGER',660,'Account Manager','ACCOUNT_MANAGER','','','','SA059956','VARCHAR2','','','Account Manager','','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MAN_PRICE_OVERRIDE_V','PERCENTAGE_PRICE_CHANGE',660,'Percentage Price Change','PERCENTAGE_PRICE_CHANGE','','','','SA059956','NUMBER','','','Percentage Price Change','','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MAN_PRICE_OVERRIDE_V','PERIOD_NAME',660,'Period Name','PERIOD_NAME','','','','SA059956','VARCHAR2','','','Period Name','','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MAN_PRICE_OVERRIDE_V','DOLLARS_LOST',660,'Dollars Lost','DOLLARS_LOST','','','','SA059956','NUMBER','','','Dollars Lost','','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MAN_PRICE_OVERRIDE_V','MODIFIER_TYPE',660,'Modifier Type','MODIFIER_TYPE','','','','SA059956','VARCHAR2','','','Modifier Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MAN_PRICE_OVERRIDE_V','LIST_LINE_TYPE_CODE',660,'List Line Type Code','LIST_LINE_TYPE_CODE','','','','SA059956','VARCHAR2','','','List Line Type Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MAN_PRICE_OVERRIDE_V','AVERAGE_COST',660,'Average Cost','AVERAGE_COST','','','','SA059956','NUMBER','','','Average Cost','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MAN_PRICE_OVERRIDE_V','MSI_ORGANIZATION_ID',660,'Msi Organization Id','MSI_ORGANIZATION_ID','','','','SA059956','NUMBER','','','Msi Organization Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MAN_PRICE_OVERRIDE_V','ADJ_MAN_PRICE_ADJUSTMENT_ID',660,'Adj Man Price Adjustment Id','ADJ_MAN_PRICE_ADJUSTMENT_ID','','','','SA059956','NUMBER','','','Adj Man Price Adjustment Id','','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MAN_PRICE_OVERRIDE_V','HCA_CUST_ACCOUNT_ID',660,'Hca Cust Account Id','HCA_CUST_ACCOUNT_ID','','','','SA059956','NUMBER','','','Hca Cust Account Id','','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MAN_PRICE_OVERRIDE_V','MP_ORGANIZATION_ID',660,'Mp Organization Id','MP_ORGANIZATION_ID','','','','SA059956','NUMBER','','','Mp Organization Id','','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MAN_PRICE_OVERRIDE_V','MSI_INVENTORY_ITEM_ID',660,'Msi Inventory Item Id','MSI_INVENTORY_ITEM_ID','','','','SA059956','NUMBER','','','Msi Inventory Item Id','','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MAN_PRICE_OVERRIDE_V','OH_HEADER_ID',660,'Oh Header Id','OH_HEADER_ID','','','','SA059956','NUMBER','','','Oh Header Id','','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_MAN_PRICE_OVERRIDE_V','OL_LINE_ID',660,'Ol Line Id','OL_LINE_ID','','','','SA059956','NUMBER','','','Ol Line Id','','','','','');
--Inserting Object Components for EIS_XXWC_MAN_PRICE_OVERRIDE_V
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_MAN_PRICE_OVERRIDE_V','OE_ORDER_HEADERS',660,'OE_ORDER_HEADERS_ALL','OH','OH','SA059956','SA059956','177461846','Oe Order Headers All Stores Header Information For','Y','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_MAN_PRICE_OVERRIDE_V','OE_ORDER_LINES',660,'OE_ORDER_LINES_ALL','OL','OL','SA059956','SA059956','177461846','Oe Order Lines All Stores Information For All Orde','Y','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_MAN_PRICE_OVERRIDE_V','MTL_SYSTEM_ITEMS_B',660,'MTL_SYSTEM_ITEMS_B','MSI','MSI','SA059956','SA059956','177461846','Inventory Item Definitions','Y','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_MAN_PRICE_OVERRIDE_V','MTL_PARAMETERS',660,'MTL_PARAMETERS','MP','MP','SA059956','SA059956','177461846','Inventory Control Options And Defaults','Y','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_MAN_PRICE_OVERRIDE_V','HZ_CUST_ACCOUNTS',660,'HZ_CUST_ACCOUNTS','HCA','HCA','SA059956','SA059956','177461846','Stores Information About Customer Accounts.','Y','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_MAN_PRICE_OVERRIDE_V','OE_PRICE_ADJUSTMENTS',660,'OE_PRICE_ADJUSTMENTS','ADJ_MAN','ADJ_MAN','SA059956','SA059956','177461846','This Table Is Used To Store Price Adjustments That','Y','','','','OPA','',' ',' ',' ',' ',' ');
--Inserting Object Component Joins for EIS_XXWC_MAN_PRICE_OVERRIDE_V
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_MAN_PRICE_OVERRIDE_V','OE_ORDER_HEADERS','OH',660,'EXMPOV.OH_HEADER_ID','=','OH.HEADER_ID(+)','','','','Y','SA059956');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_MAN_PRICE_OVERRIDE_V','OE_ORDER_LINES','OL',660,'EXMPOV.OL_LINE_ID','=','OL.LINE_ID(+)','','','','Y','SA059956');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_MAN_PRICE_OVERRIDE_V','MTL_SYSTEM_ITEMS_B','MSI',660,'EXMPOV.MSI_INVENTORY_ITEM_ID','=','MSI.INVENTORY_ITEM_ID(+)','','','','Y','SA059956');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_MAN_PRICE_OVERRIDE_V','MTL_SYSTEM_ITEMS_B','MSI',660,'EXMPOV.MSI_ORGANIZATION_ID','=','MSI.ORGANIZATION_ID(+)','','','','Y','SA059956');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_MAN_PRICE_OVERRIDE_V','MTL_PARAMETERS','MP',660,'EXMPOV.MP_ORGANIZATION_ID','=','MP.ORGANIZATION_ID(+)','','','','Y','SA059956');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_MAN_PRICE_OVERRIDE_V','HZ_CUST_ACCOUNTS','HCA',660,'EXMPOV.HCA_CUST_ACCOUNT_ID','=','HCA.CUST_ACCOUNT_ID(+)','','','','Y','SA059956');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_MAN_PRICE_OVERRIDE_V','OE_PRICE_ADJUSTMENTS','ADJ_MAN',660,'EXMPOV.ADJ_MAN_PRICE_ADJUSTMENT_ID','=','ADJ_MAN.PRICE_ADJUSTMENT_ID(+)','','','','Y','SA059956');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
prompt Creating Report LOV Data for Manual Price Override Report
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - Manual Price Override Report
xxeis.eis_rsc_ins.lov( 660,'select nvl( party.party_name,CUST_ACCT.account_name) customer_name,CUST_ACCT.ACCOUNT_NUMBER
from  HZ_PARTIES PARTY, HZ_CUST_ACCOUNTS CUST_ACCT where CUST_ACCT.PARTY_ID = PARTY.PARTY_ID AND  PARTY.status=''A''
','','OM CUSTOMER NAME','This gives the Customer Name','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'select  cust_acct.account_number Customer_Number,cust_acct.account_name customer_name,party.party_name
from  HZ_PARTIES PARTY, HZ_CUST_ACCOUNTS CUST_ACCT
 where CUST_ACCT.PARTY_ID = PARTY.PARTY_ID','','OM CUSTOMER NUMBER','This gives the Customer Number','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'SELECT ppf.full_name  created_by, fu.user_name user_name
  FROM FND_USER fu, per_people_f ppf
 WHERE TRUNC(SYSDATE) BETWEEN fu.START_DATE AND NVL(fu.end_date, hr_general.end_of_time)
   and TRUNC(SYSDATE) BETWEEN ppf.effective_start_date AND ppf.effective_end_date
   and fu.employee_id = ppf.person_id (+)
   and exists
          (select 1
             from fnd_user_resp_groups furg,
                  fnd_responsibility_vl fr
            where furg.responsibility_id   = fr.responsibility_id
              and fu.user_id = furg.user_id
              and fr.responsibility_name like ''XXEIS%'')','','Created By Lov','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'select distinct attribute6 pricing_zone from mtl_parameters','','OM Pricing Zone','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'  Select Mcv.segment2 ,concatenated_segments,mcv.Description
  From Mtl_Categories_Kfv Mcv,
       Mtl_Category_Sets Mcs
  Where  Mcs.Structure_Id                 =Mcv.Structure_Id
  AND Mcs.Category_Set_Name            =''Inventory Category''','','OM CAT CLASS','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'SELECT Flex_Val.description
from  Mtl_Categories_Kfv Mcv,
    MTL_CATEGORY_SETS MCS,
    Mtl_Item_Categories Mic,
     Fnd_Flex_Values_Vl Flex_Val
    Where   Mcs.Category_Set_Name      =''Inventory Category''
  And Mcs.Structure_Id                 = Mcv.Structure_Id
  AND MIC.CATEGORY_SET_ID              = MCS.CATEGORY_SET_ID
  And Mic.Category_Id                  = Mcv.Category_Id
  And Flex_Value                       = Mcv.SEGMENT2
  and flex_value_Set_id=1015058','','OM CAT CLASS DESC','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'select distinct ATTRIBUTE9 region from mtl_parameters','','Region Lov','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'select  distinct per.period_name , per.period_year, per.period_num, per.start_date, per.end_date
from    gl_periods per,gl_ledgers led
where  per.period_set_name = led.period_set_name
and     led.accounted_period_type = per.period_type
order by per.period_num asc,
per.period_year desc','','OM PERIOD NAMES','','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'SELECT ORGANIZATION_CODE WAREHOUSE,ORGANIZATION_NAME FROM ORG_ORGANIZATION_DEFINITIONS OOD WHERE SYSDATE < NVL(OOD.DISABLE_DATE,SYSDATE+1) AND EXISTS (SELECT 1 FROM XXEIS.EIS_ORG_ACCESS_V  WHERE organization_id = ood.organization_id )
UNION
SELECT ''All'', ''All Organizations'' from Dual','','OM Warehouse All','','ANONYMOUS',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
prompt Creating Report Data for Manual Price Override Report
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Manual Price Override Report
xxeis.eis_rsc_utility.delete_report_rows( 'Manual Price Override Report',660 );
--Inserting Report - Manual Price Override Report
xxeis.eis_rsc_ins.r( 660,'Manual Price Override Report','','This Report is copied from Manual Price Changes on Sales Orders Report.Provides visibility to order lines where the unit selling price was manually overridden.','','','','SA059956','EIS_XXWC_MAN_PRICE_OVERRIDE_V','Y','','','SA059956','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N','','','','','','N','','US','','Manual Price Changes on Sales Orders Report','','','','','','','','','','','','','','');
--Inserting Report Columns - Manual Price Override Report
xxeis.eis_rsc_ins.rc( 'Manual Price Override Report',660,'MODIFER_NUMBER','Original Modifer Type','Modifer Number','','','default','','15','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MAN_PRICE_OVERRIDE_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Manual Price Override Report',660,'BRANCH','Branch','Branch','','','default','','1','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MAN_PRICE_OVERRIDE_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Manual Price Override Report',660,'CREATED_BY','Created By','Created By','','','default','','4','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MAN_PRICE_OVERRIDE_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Manual Price Override Report',660,'CUSTOMER','Customer Name','Customer','','','default','','2','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MAN_PRICE_OVERRIDE_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Manual Price Override Report',660,'CUSTOMER_NUMBER','Customer Number','Customer Number','','','default','','3','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MAN_PRICE_OVERRIDE_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Manual Price Override Report',660,'FINAL_SELLING_PRICE','Final Selling Price','Final Selling Price','','~T~D~2','default','','18','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MAN_PRICE_OVERRIDE_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Manual Price Override Report',660,'ITEM','Item','Item','','','default','','7','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MAN_PRICE_OVERRIDE_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Manual Price Override Report',660,'ITEM_DESCRIPTION','Item Description','Item Description','','','default','','8','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MAN_PRICE_OVERRIDE_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Manual Price Override Report',660,'LIST_PRICE','List Price','List Price','','~T~D~2','default','','12','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MAN_PRICE_OVERRIDE_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Manual Price Override Report',660,'QUANTITY','Quantity','Quantity','','~T~D~2','default','','11','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MAN_PRICE_OVERRIDE_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Manual Price Override Report',660,'APPLICATION_METHOD','Application Method','Application Method','','','default','','20','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MAN_PRICE_OVERRIDE_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Manual Price Override Report',660,'ORIGINAL_UNIT_SELLING_PRICE','Original Unit Selling Price','Original Unit Selling Price','','~T~D~2','default','','13','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MAN_PRICE_OVERRIDE_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Manual Price Override Report',660,'REASON_CODE','Reason Code','Reason Code','','','default','','21','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MAN_PRICE_OVERRIDE_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Manual Price Override Report',660,'Original_GM%','Original GM%','Original GM%','VARCHAR2','','right','','14','Y','Y','','','','','','(round(case when EXMPOV.ORIGINAL_UNIT_SELLING_PRICE > 0 then ((((EXMPOV.ORIGINAL_UNIT_SELLING_PRICE-EXMPOV.AVERAGE_COST))/(EXMPOV.ORIGINAL_UNIT_SELLING_PRICE))*100) else 0 end,2)||''%'')','SA059956','N','N','','EIS_XXWC_MAN_PRICE_OVERRIDE_V','','','null','US','','');
xxeis.eis_rsc_ins.rc( 'Manual Price Override Report',660,'Final_Selling_Price_GM%','Final Selling Price GM%','Final Selling Price GM%','VARCHAR2','~T~D~2','default','','19','Y','Y','','','','','','(round(case when EXMPOV.FINAL_SELLING_PRICE>0 then ((((EXMPOV.FINAL_SELLING_PRICE-EXMPOV.AVERAGE_COST))/(EXMPOV.FINAL_SELLING_PRICE))*100) else 0 end,2)||''%'')','SA059956','N','N','','EIS_XXWC_MAN_PRICE_OVERRIDE_V','','','null','US','','');
xxeis.eis_rsc_ins.rc( 'Manual Price Override Report',660,'GM%_Lost','GM% Lost','GM% Lost','NUMBER','~T~D~2','default','','23','Y','Y','','','','','','(round((CASE WHEN EXMPOV.FINAL_SELLING_PRICE>0 THEN((((EXMPOV.FINAL_SELLING_PRICE-EXMPOV.AVERAGE_COST))/(EXMPOV.FINAL_SELLING_PRICE))*100) ELSE 0 END - CASE WHEN EXMPOV.ORIGINAL_UNIT_SELLING_PRICE > 0 THEN ((((EXMPOV.ORIGINAL_UNIT_SELLING_PRICE-EXMPOV.AVERAGE_COST))/(EXMPOV.ORIGINAL_UNIT_SELLING_PRICE))*100) ELSE 0 END),2))','SA059956','N','N','','EIS_XXWC_MAN_PRICE_OVERRIDE_V','','','null','US','','');
xxeis.eis_rsc_ins.rc( 'Manual Price Override Report',660,'CAT_CLASS','Cat Class','Cat Class','','','default','','9','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MAN_PRICE_OVERRIDE_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Manual Price Override Report',660,'CAT_CLASS_DESC','Cat Class Desc','Cat Class Desc','','','default','','10','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MAN_PRICE_OVERRIDE_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Manual Price Override Report',660,'ORDER_NUMBER','Order Number','Order Number','','~~~','default','','6','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MAN_PRICE_OVERRIDE_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Manual Price Override Report',660,'ACCOUNT_MANAGER','Account Manager','Account Manager','','','default','','5','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MAN_PRICE_OVERRIDE_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Manual Price Override Report',660,'DOLLARS_LOST','Dollars Lost','Dollars Lost','','~T~D~2','default','','22','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_MAN_PRICE_OVERRIDE_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Manual Price Override Report',660,'Percentage_Price_Change','Percentage Price Change','Price Change%','VARCHAR2','','default','','17','Y','Y','','','','','','( (EXMPOV.PERCENTAGE_PRICE_CHANGE||''%''))','SA059956','N','N','','','','','null','US','','');
--Inserting Report Parameters - Manual Price Override Report
xxeis.eis_rsc_ins.rp( 'Manual Price Override Report',660,'Pricing Zone','Pricing Zone','PRICING_ZONE','IN','OM Pricing Zone','','VARCHAR2','N','Y','5','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_MAN_PRICE_OVERRIDE_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Manual Price Override Report',660,'Customer Name','Customer Name','CUSTOMER','IN','OM CUSTOMER NAME','','VARCHAR2','N','Y','8','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_MAN_PRICE_OVERRIDE_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Manual Price Override Report',660,'Customer Number','Customer Number','CUSTOMER_NUMBER','IN','OM CUSTOMER NUMBER','','VARCHAR2','N','Y','9','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_MAN_PRICE_OVERRIDE_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Manual Price Override Report',660,'From Date','From Date','ORDERED_DATE','>=','','','DATE','N','Y','3','N','Y','CONSTANT','SA059956','Y','N','','Start Date','','EIS_XXWC_MAN_PRICE_OVERRIDE_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Manual Price Override Report',660,'To Date','To Date','ORDERED_DATE','<=','','','DATE','N','Y','4','N','Y','CONSTANT','SA059956','Y','N','','End Date','','EIS_XXWC_MAN_PRICE_OVERRIDE_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Manual Price Override Report',660,'Created By','Created By','CREATED_BY','IN','Created By Lov','','VARCHAR2','N','Y','10','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_MAN_PRICE_OVERRIDE_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Manual Price Override Report',660,'Cat Class','Cat Class','CAT_CLASS','IN','OM CAT CLASS','','VARCHAR2','N','Y','6','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_MAN_PRICE_OVERRIDE_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Manual Price Override Report',660,'cat Class Desc','cat Class Desc','CAT_CLASS_DESC','IN','OM CAT CLASS DESC','','VARCHAR2','N','Y','7','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_MAN_PRICE_OVERRIDE_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Manual Price Override Report',660,'Branch','Branch','BRANCH','IN','OM Warehouse All','','VARCHAR2','Y','Y','1','Y','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_MAN_PRICE_OVERRIDE_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Manual Price Override Report',660,'Region','Region','REGION','IN','Region Lov','','VARCHAR2','N','Y','11','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_MAN_PRICE_OVERRIDE_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Manual Price Override Report',660,'Period Name','Period Name','PERIOD_NAME','IN','OM PERIOD NAMES','','VARCHAR2','N','Y','2','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_MAN_PRICE_OVERRIDE_V','','','US','');
--Inserting Dependent Parameters - Manual Price Override Report
--Inserting Report Conditions - Manual Price Override Report
xxeis.eis_rsc_ins.rcnh( 'Manual Price Override Report',660,'EXMPOV.ORDERED_DATE >= From Date','SIMPLE','','','Y','','1');
xxeis.eis_rsc_ins.rcnd( '','','ORDERED_DATE','','From Date','','','','','EIS_XXWC_MAN_PRICE_OVERRIDE_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',660,'Manual Price Override Report','EXMPOV.ORDERED_DATE >= From Date');
xxeis.eis_rsc_ins.rcnh( 'Manual Price Override Report',660,'EXMPOV.ORDERED_DATE <= To Date','SIMPLE','','','Y','','2');
xxeis.eis_rsc_ins.rcnd( '','','ORDERED_DATE','','To Date','','','','','EIS_XXWC_MAN_PRICE_OVERRIDE_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',660,'Manual Price Override Report','EXMPOV.ORDERED_DATE <= To Date');
xxeis.eis_rsc_ins.rcnh( 'Manual Price Override Report',660,'EXMPOV.PRICING_ZONE IN Pricing Zone','SIMPLE','','','Y','','3');
xxeis.eis_rsc_ins.rcnd( '','','PRICING_ZONE','','Pricing Zone','','','','','EIS_XXWC_MAN_PRICE_OVERRIDE_V','','','','','','IN','Y','Y','','','','','1',660,'Manual Price Override Report','EXMPOV.PRICING_ZONE IN Pricing Zone');
xxeis.eis_rsc_ins.rcnh( 'Manual Price Override Report',660,'EXMPOV.CAT_CLASS IN Cat Class','SIMPLE','','','Y','','4');
xxeis.eis_rsc_ins.rcnd( '','','CAT_CLASS','','Cat Class','','','','','EIS_XXWC_MAN_PRICE_OVERRIDE_V','','','','','','IN','Y','Y','','','','','1',660,'Manual Price Override Report','EXMPOV.CAT_CLASS IN Cat Class');
xxeis.eis_rsc_ins.rcnh( 'Manual Price Override Report',660,'EXMPOV.CAT_CLASS_DESC IN cat Class Desc','SIMPLE','','','Y','','5');
xxeis.eis_rsc_ins.rcnd( '','','CAT_CLASS_DESC','','cat Class Desc','','','','','EIS_XXWC_MAN_PRICE_OVERRIDE_V','','','','','','IN','Y','Y','','','','','1',660,'Manual Price Override Report','EXMPOV.CAT_CLASS_DESC IN cat Class Desc');
xxeis.eis_rsc_ins.rcnh( 'Manual Price Override Report',660,'EXMPOV.CUSTOMER IN Customer Name','SIMPLE','','','Y','','6');
xxeis.eis_rsc_ins.rcnd( '','','CUSTOMER','','Customer Name','','','','','EIS_XXWC_MAN_PRICE_OVERRIDE_V','','','','','','IN','Y','Y','','','','','1',660,'Manual Price Override Report','EXMPOV.CUSTOMER IN Customer Name');
xxeis.eis_rsc_ins.rcnh( 'Manual Price Override Report',660,'EXMPOV.CUSTOMER_NUMBER IN Customer Number','SIMPLE','','','Y','','7');
xxeis.eis_rsc_ins.rcnd( '','','CUSTOMER_NUMBER','','Customer Number','','','','','EIS_XXWC_MAN_PRICE_OVERRIDE_V','','','','','','IN','Y','Y','','','','','1',660,'Manual Price Override Report','EXMPOV.CUSTOMER_NUMBER IN Customer Number');
xxeis.eis_rsc_ins.rcnh( 'Manual Price Override Report',660,'EXMPOV.CREATED_BY IN Created By','SIMPLE','','','Y','','8');
xxeis.eis_rsc_ins.rcnd( '','','CREATED_BY','','Created By','','','','','EIS_XXWC_MAN_PRICE_OVERRIDE_V','','','','','','IN','Y','Y','','','','','1',660,'Manual Price Override Report','EXMPOV.CREATED_BY IN Created By');
xxeis.eis_rsc_ins.rcnh( 'Manual Price Override Report',660,'EXMPOV.REGION IN Region','SIMPLE','','','Y','','10');
xxeis.eis_rsc_ins.rcnd( '','','REGION','','Region','','','','','EIS_XXWC_MAN_PRICE_OVERRIDE_V','','','','','','IN','Y','Y','','','','','1',660,'Manual Price Override Report','EXMPOV.REGION IN Region');
xxeis.eis_rsc_ins.rcnh( 'Manual Price Override Report',660,'EXMPOV.PERIOD_NAME IN Period Name','SIMPLE','','','Y','','11');
xxeis.eis_rsc_ins.rcnd( '','','PERIOD_NAME','','Period Name','','','','','EIS_XXWC_MAN_PRICE_OVERRIDE_V','','','','','','IN','Y','Y','','','','','1',660,'Manual Price Override Report','EXMPOV.PERIOD_NAME IN Period Name');
xxeis.eis_rsc_ins.rcnh( 'Manual Price Override Report',660,'FreeText','FREE_TEXT','','','Y','','12');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','','Y','Y','','','','AND ( ''All'' IN (:Branch) OR (EXMPOV.BRANCH IN (:Branch)))
and EXMPOV.PERCENTAGE_PRICE_CHANGE >19.99 
and abs(round((CASE WHEN EXMPOV.FINAL_SELLING_PRICE>0 THEN((((EXMPOV.FINAL_SELLING_PRICE-EXMPOV.AVERAGE_COST))/(EXMPOV.FINAL_SELLING_PRICE))*100) ELSE 0 END - CASE WHEN EXMPOV.ORIGINAL_UNIT_SELLING_PRICE > 0 THEN ((((EXMPOV.ORIGINAL_UNIT_SELLING_PRICE-EXMPOV.AVERAGE_COST))/(EXMPOV.ORIGINAL_UNIT_SELLING_PRICE))*100) ELSE 0 END),2))>19.99','1',660,'Manual Price Override Report','FreeText');
--Inserting Report Sorts - Manual Price Override Report
xxeis.eis_rsc_ins.rs( 'Manual Price Override Report',660,'ORDER_NUMBER','ASC','SA059956','1','');
--Inserting Report Triggers - Manual Price Override Report
--inserting report templates - Manual Price Override Report
--Inserting Report Portals - Manual Price Override Report
--inserting report dashboards - Manual Price Override Report
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Manual Price Override Report','660','EIS_XXWC_MAN_PRICE_OVERRIDE_V','EIS_XXWC_MAN_PRICE_OVERRIDE_V','N','');
--inserting report security - Manual Price Override Report
xxeis.eis_rsc_ins.rsec( 'Manual Price Override Report','101','','XXCUS_GL_INQUIRY',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Manual Price Override Report','660','','XXWC_AO_OEENTRY',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Manual Price Override Report','660','','XXWC_AO_OEENTRY_REC',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Manual Price Override Report','660','','XXWC_RENTAL_OM_PRICING_SUPER',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Manual Price Override Report','660','','XXWC_ORDER_MGMT_READ_ONLY_COST',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Manual Price Override Report','660','','XXWC_ORDER_MGMT_READ_ONLY',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Manual Price Override Report','660','','XXWC_ORDER_MGMT_PRICING_SUPER',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Manual Price Override Report','660','','XXWC_ORDER_MGMT_PRICING_STD',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Manual Price Override Report','660','','ORDER_MGMT_SUPER_USER',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Manual Price Override Report','20005','','XXWC_VIEW_ALL_EIS_REPORTS',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Manual Price Override Report','701','','CLN_OM_3A6_ADMINISTRATOR',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Manual Price Override Report','660','','XXWC_ORDER_MGMT_PRICING_LTD',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Manual Price Override Report','660','','XXWC_ORDER_MGMT_PRICING_FULL',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Manual Price Override Report','660','','XXWC_AO_OEENTRY_PO_RPT',660,'SA059956','','','');
--Inserting Report Pivots - Manual Price Override Report
--Inserting Report Distribution Details 
--Inserting Report Distribution Details 
--Inserting Report SubTotals Details 
--Inserting Report aggregations 
--Inserting Report   Version details- Manual Price Override Report
xxeis.eis_rsc_ins.rv( 'Manual Price Override Report','','Manual Price Override Report','SA059956','16-JAN-2018');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
