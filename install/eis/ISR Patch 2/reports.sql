--Report Name            : Inventory Sales and Reorder Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View EIS_XXWC_PO_ISR_RPT_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_PO_ISR_RPT_V
Create or replace View XXEIS.EIS_XXWC_PO_ISR_RPT_V
(ORG,PRE,ITEM_NUMBER,VENDOR_NUM,VENDOR_NAME,SOURCE,ST,DESCRIPTION,CAT,PPLT,PLT,UOM,CL,STK_FLAG,PM,MINN,MAXN,AMU,MF_FLAG,HIT6_SALES,AVER_COST,ITEM_COST,BPA_COST,BPA,QOH,AVAILABLE,AVAILABLEDOLLAR,JAN_SALES,FEB_SALES,MAR_SALES,APR_SALES,MAY_SALES,JUNE_SALES,JUL_SALES,AUG_SALES,SEP_SALES,OCT_SALES,NOV_SALES,DEC_SALES,HIT4_SALES,ONE_SALES,SIX_SALES,TWELVE_SALES,BIN_LOC,MC,FI_FLAG,FREEZE_DATE,RES,THIRTEEN_WK_AVG_INV,THIRTEEN_WK_AN_COGS,TURNS,BUYER,TS,SO,INVENTORY_ITEM_ID,ORGANIZATION_ID,SET_OF_BOOKS_ID,ON_ORD,WT,SS,FML,OPEN_REQ,ORG_NAME,DISTRICT,REGION,SOURCING_RULE,CLT,COMMON_OUTPUT_ID,PROCESS_ID,AVAIL2) AS 
SELECT Varchar2_Col1 "ORG",
    Varchar2_Col2 "PRE",
    Varchar2_Col3 "ITEM_NUMBER",
    Varchar2_Col4 "VENDOR_NUM",
    Varchar2_Col5 "VENDOR_NAME",
    Varchar2_Col6 "SOURCE",
    Varchar2_Col7 "ST",
    Varchar2_Col8 "DESCRIPTION",
    Varchar2_Col9 "CAT",
    Varchar2_Col10 "PPLT",
    Varchar2_Col11 "PLT",
    Varchar2_Col12 "UOM",
    Varchar2_Col13 "CL",
    Varchar2_Col14 "STK_FLAG",
    Varchar2_Col15 "PM",
    Varchar2_Col16 "MINN",
    Varchar2_Col17 "MAXN",
    Varchar2_Col18 "AMU",
    Varchar2_Col19 "MF_FLAG",
    Number_Col1 Hit6_Sales,
    Number_Col2 "AVER_COST",
    Number_Col3 "ITEM_COST",
    Number_Col4 "BPA_COST",
    Varchar2_Col20 "BPA",
    Number_Col5 "QOH",
    Number_Col6 "AVAILABLE",
    Number_Col7 "AVAILABLEDOLLAR",
    Number_Col8 Jan_Sales,
    Number_Col9 Feb_Sales,
    Number_Col10 Mar_Sales,
    Number_Col11 Apr_Sales,
    Number_Col12 May_Sales,
    Number_Col13 June_Sales,
    Number_Col14 Jul_Sales,
    Number_Col15 Aug_Sales,
    Number_Col16 Sep_Sales,
    Number_Col17 Oct_Sales,
    Number_Col18 Nov_Sales,
    Number_Col19 Dec_Sales,
    Number_Col20 Hit4_Sales,
    Number_Col21 One_Sales,
    Number_Col22 Six_Sales,
    Number_Col23 Twelve_Sales,
    Varchar2_Col21 "BIN_LOC",
    Varchar2_Col22 "MC",
    Varchar2_Col23 "FI_FLAG",
    Date_Col1 "FREEZE_DATE",
    Varchar2_Col24 "RES",
    Number_Col24 "THIRTEEN_WK_AVG_INV",
    Number_Col25 "THIRTEEN_WK_AN_COGS",
    Number_Col26 "TURNS",
    Varchar2_Col25 "BUYER",
    Varchar2_Col26 "TS",
    Number_Col27 "SO",
    Number_Col28 "INVENTORY_ITEM_ID",
    Number_Col29 "ORGANIZATION_ID",
    Number_Col30 "SET_OF_BOOKS_ID",
    number_col31 "ON_ORD",
    Number_Col32 "WT",
    Number_Col33 "SS",
    Number_Col34 "FML",
    Number_Col35 "OPEN_REQ",
    Varchar2_Col27 "ORG_NAME",
    Varchar2_Col28 "DISTRICT",
    varchar2_col29 "REGION",
    Varchar2_Col30 "SOURCING_RULE",
    Number_Col36 "CLT",
    common_output_id,
    process_id,
    Number_Col37 "AVAIL2"
  FROM XXEIS.EIS_RS_COMMON_OUTPUTS/
set scan on define on
prompt Creating View Data for Inventory Sales and Reorder Report
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_PO_ISR_RPT_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_PO_ISR_RPT_V',201,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Xxwc Po Isr V','EXPIV');
--Delete View Columns for EIS_XXWC_PO_ISR_RPT_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_PO_ISR_RPT_V',201,FALSE);
--Inserting View Columns for EIS_XXWC_PO_ISR_RPT_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','BPA',201,'Bpa','BPA','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Bpa');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','ITEM_COST',201,'Item Cost','ITEM_COST','','','','XXEIS_RS_ADMIN','NUMBER','','','Item Cost');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','AVER_COST',201,'Aver Cost','AVER_COST','','','','XXEIS_RS_ADMIN','NUMBER','','','Aver Cost');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','AMU',201,'Amu','AMU','','','','XXEIS_RS_ADMIN','NUMBER','','','Amu');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','PM',201,'Pm','PM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Pm');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','STK_FLAG',201,'Stk Flag','STK_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Stk Flag');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','CL',201,'Cl','CL','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cl');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','UOM',201,'Uom','UOM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Uom');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','CAT',201,'Cat','CAT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cat');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','DESCRIPTION',201,'Description','DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Description');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','ST',201,'St','ST','','','','XXEIS_RS_ADMIN','VARCHAR2','','','St');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','SOURCE',201,'Source','SOURCE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Source');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','VENDOR_NAME',201,'Vendor Name','VENDOR_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Name');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','VENDOR_NUM',201,'Vendor Num','VENDOR_NUM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Num');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','ITEM_NUMBER',201,'Item Number','ITEM_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Item Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','PRE',201,'Pre','PRE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Pre');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','ORG',201,'Org','ORG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Org');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','BUYER',201,'Buyer','BUYER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Buyer');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','TURNS',201,'Turns','TURNS','','','','XXEIS_RS_ADMIN','NUMBER','','','Turns');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','THIRTEEN_WK_AN_COGS',201,'Thirteen Wk An Cogs','THIRTEEN_WK_AN_COGS','','','','XXEIS_RS_ADMIN','NUMBER','','','Thirteen Wk An Cogs');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','THIRTEEN_WK_AVG_INV',201,'Thirteen Wk Avg Inv','THIRTEEN_WK_AVG_INV','','','','XXEIS_RS_ADMIN','NUMBER','','','Thirteen Wk Avg Inv');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','RES',201,'Res','RES','','','','XXEIS_RS_ADMIN','NUMBER','','','Res');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','FREEZE_DATE',201,'Freeze Date','FREEZE_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Freeze Date');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','FI_FLAG',201,'Fi Flag','FI_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Fi Flag');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','MC',201,'Mc','MC','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Mc');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','BIN_LOC',201,'Bin Loc','BIN_LOC','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Bin Loc');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','AVAILABLEDOLLAR',201,'Availabledollar','AVAILABLEDOLLAR','','','','XXEIS_RS_ADMIN','NUMBER','','','Availabledollar');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','AVAILABLE',201,'Available','AVAILABLE','','','','XXEIS_RS_ADMIN','NUMBER','','','Available');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','QOH',201,'Qoh','QOH','','','','XXEIS_RS_ADMIN','NUMBER','','','Qoh');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','ORG_NAME',201,'Org Name','ORG_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Org Name');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','APR_SALES',201,'Apr Sales','APR_SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','Apr Sales');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','AUG_SALES',201,'Aug Sales','AUG_SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','Aug Sales');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','DEC_SALES',201,'Dec Sales','DEC_SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','Dec Sales');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','FEB_SALES',201,'Feb Sales','FEB_SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','Feb Sales');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','HIT4_SALES',201,'Hit4 Sales','HIT4_SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','Hit4 Sales');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','HIT6_SALES',201,'Hit6 Sales','HIT6_SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','Hit6 Sales');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','JAN_SALES',201,'Jan Sales','JAN_SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','Jan Sales');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','JUL_SALES',201,'Jul Sales','JUL_SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','Jul Sales');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','JUNE_SALES',201,'June Sales','JUNE_SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','June Sales');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','MAR_SALES',201,'Mar Sales','MAR_SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','Mar Sales');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','MAY_SALES',201,'May Sales','MAY_SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','May Sales');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','NOV_SALES',201,'Nov Sales','NOV_SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','Nov Sales');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','OCT_SALES',201,'Oct Sales','OCT_SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','Oct Sales');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','SEP_SALES',201,'Sep Sales','SEP_SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','Sep Sales');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','ONE_SALES',201,'One Sales','ONE_SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','One Sales');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','SIX_SALES',201,'Six Sales','SIX_SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','Six Sales');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','TWELVE_SALES',201,'Twelve Sales','TWELVE_SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','Twelve Sales');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','DISTRICT',201,'District','DISTRICT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','District');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','REGION',201,'Region','REGION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Region');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','MAXN',201,'Maxn','MAXN','','','','XXEIS_RS_ADMIN','NUMBER','','','Maxn');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','MINN',201,'Minn','MINN','','','','XXEIS_RS_ADMIN','NUMBER','','','Minn');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','PLT',201,'Plt','PLT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Plt');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','PPLT',201,'Pplt','PPLT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Pplt');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','TS',201,'Ts','TS','','','','XXEIS_RS_ADMIN','NUMBER','','','Ts');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','BPA_COST',201,'Bpa Cost','BPA_COST','','','','XXEIS_RS_ADMIN','NUMBER','','','Bpa Cost');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','ON_ORD',201,'On Ord','ON_ORD','','','','XXEIS_RS_ADMIN','NUMBER','','','On Ord');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','FML',201,'Fml','FML','','','','XXEIS_RS_ADMIN','NUMBER','','','Fml');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','OPEN_REQ',201,'Open Req','OPEN_REQ','','','','XXEIS_RS_ADMIN','NUMBER','','','Open Req');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','WT',201,'Wt','WT','','','','XXEIS_RS_ADMIN','NUMBER','','','Wt');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','SO',201,'So','SO','','','','XXEIS_RS_ADMIN','NUMBER','','','So');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','SOURCING_RULE',201,'Sourcing Rule','SOURCING_RULE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Sourcing Rule');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','SS',201,'Ss','SS','','','','XXEIS_RS_ADMIN','NUMBER','','','Ss');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','CLT',201,'Clt','CLT','','','','XXEIS_RS_ADMIN','NUMBER','','','Clt');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','MF_FLAG',201,'Mf Flag','MF_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Mf Flag');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','SET_OF_BOOKS_ID',201,'Set Of Books Id','SET_OF_BOOKS_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Set Of Books Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','ORGANIZATION_ID',201,'Organization Id','ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Organization Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','INVENTORY_ITEM_ID',201,'Inventory Item Id','INVENTORY_ITEM_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Inventory Item Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','COMMON_OUTPUT_ID',201,'Common Output Id','COMMON_OUTPUT_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Common Output Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','PROCESS_ID',201,'Process Id','PROCESS_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Process Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','AVAIL2',201,'Avail2','AVAIL2','','','','XXEIS_RS_ADMIN','NUMBER','','','Avail2');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','AMU',201,'Amu','AMU','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Amu');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','MAXN',201,'Maxn','MAXN','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Maxn');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','MINN',201,'Minn','MINN','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Minn');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','RES',201,'Res','RES','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Res');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','TS',201,'Ts','TS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Ts');
--Inserting View Components for EIS_XXWC_PO_ISR_RPT_V
--Inserting View Component Joins for EIS_XXWC_PO_ISR_RPT_V
END;
/
set scan on define on
prompt Creating Report LOV Data for Inventory Sales and Reorder Report
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Inventory Sales and Reorder Report
xxeis.eis_rs_ins.lov( 201,'select vendor_name from po_vendors','','EIS_PO_SUPPLIER_LOV','','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 201,'SELECT ood.organization_code organization_code,ood.organization_name organization_name  FROM ORG_ORGANIZATION_DEFINITIONS OOD WHERE EXISTS(SELECT 1 FROM XXEIS.EIS_ORG_ACCESS_V WHERE organization_id = ood.organization_id) ORDER BY organization_code','','PO Organization Lov','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'select distinct segment1 bin_loc from Mtl_Item_Locations','','Bin Location Lov','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'select distinct segment1 bin_loc from Mtl_Item_Locations','','Bin Location Lov','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'    SELECT distinct Mcv.SEGMENT2 cat_class
        from  Mtl_Categories_Kfv Mcv,
            MTL_CATEGORY_SETS MCS,
            MTL_ITEM_CATEGORIES MIC,
            mtl_system_items_kfv msi
            Where   Mcs.Category_Set_Name      = ''Inventory Category''
          AND MCS.STRUCTURE_ID                 = MCV.STRUCTURE_ID
          AND MIC.INVENTORY_ITEM_ID            = MSI.INVENTORY_ITEM_ID
          And Mic.Organization_Id              = msi.Organization_Id
          AND MIC.CATEGORY_SET_ID              = MCS.CATEGORY_SET_ID
          And Mic.Category_Id                  = Mcv.Category_Id
order by Mcv.SEGMENT2','','Catclass Lov','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'select distinct ATTRIBUTE8 District from mtl_parameters','','District Lov','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'select distinct ATTRIBUTE9 Region from mtl_parameters','','Region Lov','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'','Exclude,Include,Tool Repair Only','Tool Repair','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'','  ,Vendor Number,First 3 digits of Item,Item number,Source,2 Digit Cat,4 Digit Cat Class,Default buyer','Report Criteria','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'','Active small,All items,All items on hand, Stock items only,Non stock only,Active large, Non-stock on hand,Stock items with 0/0 min/max','EIS PO XXWC ISR REPORT COND','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'','Include,Time Sensitive Only','EIS PO XXWC Time Sensitive','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'','No,Yes','EIS PO XXWC DC Mode','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'select list_name from XXWC_PARAM_LIST where list_type = ''Org'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Org List','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'select list_name from XXWC_PARAM_LIST where list_type = ''Item'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Item List','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'select list_name from XXWC_PARAM_LIST where list_type = ''Supplier'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Supplier List','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'select list_name from XXWC_PARAM_LIST where list_type = ''Source'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Source List','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'select list_name from XXWC_PARAM_LIST where list_type = ''Cat Class''  and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Cat Class List','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'Select distinct segment1 from mtl_system_items_b','','XXWC Item','','XXEIS_RS_ADMIN',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Inventory Sales and Reorder Report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Inventory Sales and Reorder Report
xxeis.eis_rs_utility.delete_report_rows( 'Inventory Sales and Reorder Report' );
--Inserting Report - Inventory Sales and Reorder Report
xxeis.eis_rs_ins.r( 201,'Inventory Sales and Reorder Report','','This report displays info needed to make inventory replenishment decisions ( by vendor, part number) at selected locations.','','','','XXEIS_RS_ADMIN','eis_xxwc_po_isr_rpt_v','Y','','','XXEIS_RS_ADMIN','N','N','White Cap Reports','','CSV,HTML,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - Inventory Sales and Reorder Report
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'BPA_COST','Bpa Cost','Bpa Cost','','','','','29','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'ON_ORD','On Ord','On Ord','','','','','33','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'APR_SALES','Apr','Apr Sales','','','','','53','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'AUG_SALES','Aug','Aug Sales','','','','','57','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'AVAILABLE','Avail','Available','','','','','34','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'AVAILABLEDOLLAR','Ext$','Availabledollar','','','','','36','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'AVER_COST','Aver Cost','Aver Cost','','','','','27','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'BIN_LOC','Bin Loc','Bin Loc','','','','','40','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'BPA','Bpa#','Bpa','','','','','30','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'BUYER','Buyer','Buyer','','','','','48','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'CAT','Cat/Cl','Cat','','','','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'CL','Cl','Cl','','','','','19','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'DEC_SALES','Dec','Dec Sales','','','','','61','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'DESCRIPTION','Description','Description','','','','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'FEB_SALES','Feb','Feb Sales','','','','','51','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'FI_FLAG','FI','Fi Flag','','','','','42','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'FREEZE_DATE','Freeze Date','Freeze Date','','','','','43','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'HIT4_SALES','Hit4','Hit4 Sales','','','','','26','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'HIT6_SALES','Hit6','Hit6 Sales','','','','','25','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'ITEM_COST','Item Cost','Item Cost','','','','','28','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'ITEM_NUMBER','Item Number','Item Number','','','','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'JAN_SALES','Jan','Jan Sales','','','','','50','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'JUL_SALES','Jul','Jul Sales','','','','','56','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'JUNE_SALES','June','June Sales','','','','','55','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'MAR_SALES','Mar','Mar Sales','','','','','52','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'MAXN','Max','Maxn','NUMBER','','','','23','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'MAY_SALES','May','May Sales','','','','','54','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'MC','MC','Mc','','','','','41','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'MINN','Min','Minn','NUMBER','','','','22','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'NOV_SALES','Nov','Nov Sales','','','','','60','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'OCT_SALES','Oct','Oct Sales','','','','','59','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'ONE_SALES','1','One Sales','','','','','37','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'ORG','Org','Org','','','','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'PLT','PLT','Plt','','','','','12','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'PM','PM','Pm','','','','','21','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'PPLT','PPLT','Pplt','','','','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'PRE','Pre','Pre','','','','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'QOH','QOH','Qoh','','','','','31','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'RES','Res','Res','NUMBER','','','','44','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'SEP_SALES','Sep','Sep Sales','','','','','58','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'SIX_SALES','6','Six Sales','','','','','38','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'SOURCE','Source','Source','','','','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'ST','ST','St','','','','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'STK_FLAG','Stk','Stk Flag','','','','','20','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'THIRTEEN_WK_AN_COGS','13 Wk An COGS $','Thirteen Wk An Cogs','','','','','46','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'THIRTEEN_WK_AVG_INV','13 Wk Av Inv $','Thirteen Wk Avg Inv','','','','','45','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'TS','TS','Ts','NUMBER','','','','49','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'TURNS','Turns','Turns','','','','','47','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'TWELVE_SALES','12','Twelve Sales','','','','','39','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'UOM','UOM','Uom','','','','','15','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'VENDOR_NAME','Vendor','Vendor Name','','','','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'VENDOR_NUM','Vend #','Vendor Num','','','','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'AMU','AMU','Amu','NUMBER','','','','24','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'FML','FLM','Fml','','','','','17','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'OPEN_REQ','Req','Open Req','','','','','32','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'WT','Wt','Wt','','','','','16','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'SO','So','So','','','','','18','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'SOURCING_RULE','Sourcing Rule','Sourcing Rule','','','','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'SS','Ss','Ss','','','','','14','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'CLT','CLT','Clt','','','','','13','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'AVAIL2','Avail2','Avail2','','','','','35','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
--Inserting Report Parameters - Inventory Sales and Reorder Report
xxeis.eis_rs_ins.rp( 'Inventory Sales and Reorder Report',201,'Region','Region','REGION','IN','Region Lov','','VARCHAR2','N','Y','1','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Inventory Sales and Reorder Report',201,'Organization','Organization','ORG_NAME','IN','PO Organization Lov','','VARCHAR2','N','Y','3','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Inventory Sales and Reorder Report',201,'DC Mode','DC Mode','','IN','EIS PO XXWC DC Mode','''No''','VARCHAR2','Y','Y','5','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Inventory Sales and Reorder Report',201,'Starting Bin','Starting Bin','BIN_LOC','>=','Bin Location Lov','','VARCHAR2','N','Y','12','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Inventory Sales and Reorder Report',201,'Ending Bin','Ending Bin','BIN_LOC','<=','Bin Location Lov','','VARCHAR2','N','Y','13','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Inventory Sales and Reorder Report',201,'District','District','DISTRICT','IN','District Lov','','VARCHAR2','N','Y','2','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Inventory Sales and Reorder Report',201,'Tool Repair','Tool Repair','','IN','Tool Repair','''Include''','VARCHAR2','Y','Y','6','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Inventory Sales and Reorder Report',201,'Time Sensitive','Time Sensitive','','IN','EIS PO XXWC Time Sensitive','','VARCHAR2','N','Y','7','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Inventory Sales and Reorder Report',201,'Vendor','Vendor','VENDOR_NUM','IN','EIS_PO_SUPPLIER_LOV','','VARCHAR2','N','Y','14','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Inventory Sales and Reorder Report',201,'Item','Item','ITEM_NUMBER','IN','XXWC Item','','VARCHAR2','N','Y','15','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Inventory Sales and Reorder Report',201,'Cat Class','Cat Class','CAT','IN','Catclass Lov','','VARCHAR2','N','Y','16','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Inventory Sales and Reorder Report',201,'Items w/ 4 mon sales hits','Stock items w/ 4 mon sales hits > x','','IN','','','NUMERIC','N','Y','9','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Inventory Sales and Reorder Report',201,'Report Criteria','Report Criteria','','IN','Report Criteria','','VARCHAR2','N','Y','10','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Inventory Sales and Reorder Report',201,'Report Criteria Value','Report Criteria Value','','IN','','','VARCHAR2','N','Y','11','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Inventory Sales and Reorder Report',201,'Report Filter','Report Condition','','IN','EIS PO XXWC ISR REPORT COND','','VARCHAR2','N','Y','8','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Inventory Sales and Reorder Report',201,'Item List','Item List','','IN','XXWC Item List','','VARCHAR2','N','Y','17','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Inventory Sales and Reorder Report',201,'Vendor List','Vendor List','','IN','XXWC Supplier List','','VARCHAR2','N','Y','18','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Inventory Sales and Reorder Report',201,'Cat Class List','Cat Class List','','IN','XXWC Cat Class List','','VARCHAR2','N','Y','19','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Inventory Sales and Reorder Report',201,'Source List','Source List','','IN','XXWC Source List','','VARCHAR2','N','Y','20','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Inventory Sales and Reorder Report',201,'Location List','Location List','','IN','XXWC Org List','','VARCHAR2','N','Y','4','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
--Inserting Report Conditions - Inventory Sales and Reorder Report
xxeis.eis_rs_ins.rcn( 'Inventory Sales and Reorder Report',201,'PROCESS_ID','IN',':SYSTEM.PROCESS_ID','','','Y','1','N','XXEIS_RS_ADMIN');
--Inserting Report Sorts - Inventory Sales and Reorder Report
--Inserting Report Triggers - Inventory Sales and Reorder Report
xxeis.eis_rs_ins.rt( 'Inventory Sales and Reorder Report',201,'begin
xxeis.EIS_RS_XXWC_COM_UTIL_PKG.G_isr_rpt_dc_mod_sub:=:DC Mode;
xxeis.eis_po_xxwc_isr_pkg.Isr_Rpt_Proc(
P_process_id => :SYSTEM.PROCESS_ID,
P_Region =>:Region,
P_District =>:District,
P_Location =>:Organization,
P_Dc_Mode =>:DC Mode,
P_Tool_Repair =>:Tool Repair,
P_Time_Sensitive =>:Time Sensitive,                            
P_Stk_Items_With_Hit4 =>:Items w/ 4 mon sales hits,
p_Report_Condition =>:Report Filter,
P_Report_Criteria            =>:Report Criteria,
P_Report_Criteria_val            =>:Report Criteria Value,
p_start_bin_loc =>:Starting Bin,
p_end_bin_loc =>:Ending Bin,
p_vendor =>:Vendor,
p_item =>:Item,
p_cat_class =>:Cat Class,
p_org_list =>:Location List,
P_ITEM_LIST =>:Item List,
P_SUPPLIER_LIST =>:Vendor List,
P_CAT_CLASS_LIST =>:Cat Class List,
P_SOURCE_LIST =>:Source List
);
end;','B','Y','XXEIS_RS_ADMIN');
--Inserting Report Templates - Inventory Sales and Reorder Report
--Inserting Report Portals - Inventory Sales and Reorder Report
--Inserting Report Dashboards - Inventory Sales and Reorder Report
--Inserting Report Security - Inventory Sales and Reorder Report
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder Report','20005','','50861',201,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder Report','201','','50621',201,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder Report','20005','','50900',201,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder Report','401','','50846',201,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder Report','401','','50862',201,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder Report','401','','50848',201,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder Report','401','','50868',201,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder Report','401','','50849',201,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder Report','401','','50867',201,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder Report','660','','50870',201,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder Report','201','','50850',201,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder Report','201','','50872',201,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder Report','401','','50990',201,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder Report','175','','50922',201,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder Report','175','','50941',201,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder Report','401','','50882',201,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder Report','401','','50883',201,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder Report','401','','50981',201,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder Report','401','','50855',201,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder Report','401','','50884',201,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder Report','704','','50885',201,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder Report','661','','50891',201,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder Report','201','','50921',201,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder Report','201','','50892',201,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder Report','201','','50910',201,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder Report','201','','50893',201,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder Report','201','','50983',201,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder Report','201','','50989',201,'XXEIS_RS_ADMIN','');
END;
/

set scan on define on
prompt Creating Report Data for Inventory Sales and Reorder(ISR) Datamart
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Inventory Sales and Reorder(ISR) Datamart
xxeis.eis_rs_utility.delete_report_rows( 'Inventory Sales and Reorder(ISR) Datamart' );
--Inserting Report - Inventory Sales and Reorder(ISR) Datamart
xxeis.eis_rs_ins.r( 201,'Inventory Sales and Reorder(ISR) Datamart','','Datamart for Inventory Sales and Reorder Report','','','','XXEIS_RS_ADMIN','XXEIS_835_LWVGQS_V','Y','','select count(*) cnt from xxeis.eis_xxwc_po_isr_tab
','XXEIS_RS_ADMIN','N','N','White Cap Reports','RTF,PDF,','CSV,HTML,Html Summary,XML,Pivot Excel,EXCEL,','');
--Inserting Report Columns - Inventory Sales and Reorder(ISR) Datamart
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder(ISR) Datamart',201,'CNT','Cnt','','','','','','','','Y','','','','','','','XXEIS_RS_ADMIN','','','','XXEIS_835_LWVGQS_V','','');
--Inserting Report Parameters - Inventory Sales and Reorder(ISR) Datamart
--Inserting Report Conditions - Inventory Sales and Reorder(ISR) Datamart
--Inserting Report Sorts - Inventory Sales and Reorder(ISR) Datamart
--Inserting Report Triggers - Inventory Sales and Reorder(ISR) Datamart
xxeis.eis_rs_ins.rt( 'Inventory Sales and Reorder(ISR) Datamart',201,'Begin
xxeis.eis_po_xxwc_isr_pkg.main();
end;','B','Y','XXEIS_RS_ADMIN');
--Inserting Report Templates - Inventory Sales and Reorder(ISR) Datamart
--Inserting Report Portals - Inventory Sales and Reorder(ISR) Datamart
--Inserting Report Dashboards - Inventory Sales and Reorder(ISR) Datamart
--Inserting Report Security - Inventory Sales and Reorder(ISR) Datamart
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder(ISR) Datamart','20005','','50861',201,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder(ISR) Datamart','201','','50983',201,'XXEIS_RS_ADMIN','');
END;
/
set scan on define on
