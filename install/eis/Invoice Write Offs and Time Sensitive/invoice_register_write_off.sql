--Report Name            : Invoice Register - Write Offs
--Import Instructions : 1. If RTF template needs to be uploaded to report automatically then move the RTF template to physical path of APPS_DATA_FILE_DIR oracle directory. Check dba_directories view to get the physical path of oracle directory. 
--                         (If you want to use any other oracle directory other than APPS_DATA_FILE_DIR then replace APPS_DATA_FILE_DIR with your oracle directory name below.)
--                      2. Make sure XXEIS schema has read/write permissions to above used oracle directory and give full permissions to the directory at OS level.
--                      3. Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
exec xxeis.eis_rs_ins.g_rtf_dir := 'APPS_DATA_FILE_DIR' ;
--Creating View EIS_XXWC_AR_INVOICE_REGISTER_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_AR_INVOICE_REGISTER_V
Create or replace View XXEIS.EIS_XXWC_AR_INVOICE_REGISTER_V
(ADJUSTMENT_POSTABLE,TRX_TYPE,TRX_TYPE_CLASS,TRX_CURRENCY_CODE,RECEIPT_DATE,FISCAL_YEAR,FISCAL_MONTH,ORDER_NUMBER,INVOICE_NUMBER,INVOICE_DATE,CUSTOMER_NUMBER,CUSTOMER_NAME,INVOICE_AMOUNT,DISCOUNT_AMT,SHIP_TO_CITY,SHIP_TO_STATE,SHIP_TO_POSTAL_CODE,AMOUNT_APPLIED,SHIP_VIA,ACCOUNT,GL_ACCOUNT,BRANCH_DESCR,LOCATION,POSTAL_CODE,STATE,CITY,ACTIVITY_NAME,TRX_COMMENTS,TRX_DUE_DATE,TRX_GL_DATE,ADJUSTMENT_ID,ADJUSTMENT_NUM,ADJUSTMENT_DATE,ADJUSTMENT_GL_DATE,ADJ_TYPE,ADJ_TYPE_MEANING,ADJ_TYPE_CODE,WRITEOFF_AMOUNT,ADJUSTMENT_ACCTD_AMOUNT,PARTY_NAME,D_OR_I,COMPANY_NAME,COA_ID,FUNCTIONAL_CURRENCY,FUNCTIONAL_CURRENCY_PRECISION,PARTY_NUMBER,PARTY_TYPE,PARTY_ORIG_SYS_REF,CUSTOMER_KEY,DUNS_NUMBER,PERSON_FIRST_NAME,PERSON_MIDDLE_NAME,PERSON_LAST_NAME,PERSON_NAME_SUFFIX,PERSON_TITLE,COUNTRY,PROVINCE,CUST_ACCT_ORIG_SYS_REF,CUST_ACCT_STATUS,CUSTOMER_TYPE,CUST_ACCT_SITES_ORIG_SYS_REF,STATUS,CUSTOMER_CATEGORY_CODE,LANGUAGE,KEY_ACCOUNT_FLAG,TP_HEADER_ID,SITE_USE_CODE,PRIMARY_FLAG,CUST_SITE_USES_STATUS,SITE_USE_ORIG_SYS_REF,TAX_REFERENCE,TAX_CODE,OPERATING_UNIT,CUST_ACCOUNT_ID,PARTY_ID,SITE_USE_ID,CUST_ACCT_SITE_ID,CUST_TRX_TYPE_ID,PAYMENT_SCHEDULE_ID,RECEIVABLES_TRX_ID,CUSTOMER_TRX_ID,ORG_ID,CURRENCY_CODE,ORGANIZATION_ID,LEDGER_ID,HEADER_ID,CODE_COMBINATION_ID,GL#50328#ACCOUNT,GL#50328#ACCOUNT#DESCR,GL#50328#COST_CENTER,GL#50328#COST_CENTER#DESCR,GL#50328#FURTURE_USE,GL#50328#FURTURE_USE#DESCR,GL#50328#FUTURE_USE_2,GL#50328#FUTURE_USE_2#DESCR,GL#50328#LOCATION,GL#50328#LOCATION#DESCR,GL#50328#PRODUCT,GL#50328#PRODUCT#DESCR,GL#50328#PROJECT_CODE,GL#50328#PROJECT_CODE#DESCR) AS 
SELECT DECODE (adj.postable, 'Y', 'Yes', 'N', 'No') adjustment_postable,
    TYPE.NAME trx_type,
    TYPE.TYPE trx_type_class,
    trx.invoice_currency_code trx_currency_code,
    (SELECT MAX(RECEIPT_DATE)
    FROM AR_CASH_RECEIPTS CR,
      ar_payment_schedules ps
    WHERE ps.cash_receipt_id = cr.cash_receipt_id
    AND ps.trx_number        = trx.trx_number
    AND cr.cash_receipt_id  IS NOT NULL
    )RECEIPT_DATE,
    XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.XXWC_GET_FISCAL_YEAR(trx.ORG_ID,adj.gl_date) FISCAL_YEAR,
    XXEIS.eis_rs_xxwc_com_util_pkg.XXWC_GET_FISCAL_MONTH(trx.ORG_ID,adj.gl_date) FISCAL_MONTH,
    DECODE(trx.interface_header_context,'ORDER ENTRY',trx.INTERFACE_HEADER_ATTRIBUTE1,NVL(trx.CT_REFERENCE,xxeis.EIS_RS_XXWC_COM_UTIL_PKG.get_ordnum(cust.account_number, trx.trx_number))) ORDER_NUMBER,
    trx.trx_number INVOICE_NUMBER,
    trx.trx_date INVOICE_DATE,
    cust.account_number CUSTOMER_NUMBER,
    NVL(cust.account_name,party.PARTY_NAME)CUSTOMER_NAME,
    (SELECT SUM(EXTENDED_AMOUNT)
    FROM RA_CUSTOMER_TRX_LINES RCT1
    WHERE Rct1.Customer_Trx_Id=trx.Customer_Trx_Id
    )INVOICE_AMOUNT,
    NVL(PAY.DISCOUNT_original,0) DISCOUNT_AMT,
    LOCS.CITY SHIP_TO_CITY,
    Locs.State Ship_To_State,
    LOCS.POSTAL_CODE SHIP_TO_POSTAL_CODE,
    (SELECT SUM(ra.amount_applied)
    FROM ar_receivable_applications ra
    WHERE APPLICATION_TYPE         ='CASH'
    AND RA.STATUS                  ='APP'
    AND RA.APPLIED_CUSTOMER_TRX_ID = trx.CUSTOMER_TRX_ID
    ) amount_applied,
    DECODE(interface_header_context,'CONVERSION',xxeis.EIS_RS_XXWC_COM_UTIL_PKG.get_shipvia(cust.account_number, trx.trx_number),'PRISM INVOICES',interface_header_attribute12,FLV_SHIP_MTD.MEANING) SHIP_VIA,
    gl.concatenated_segments ACCOUNT,
    gl.segment4 GL_ACCOUNT,
    FV.description branch_descr,
    DECODE(trx.interface_header_context,'ORDER ENTRY',ood.organization_code,'PRISM INVOICES',trx.interface_header_attribute7,'CONVERSION',xxeis.EIS_RS_XXWC_COM_UTIL_PKG.get_warehse(cust.account_number, trx.trx_number)) Location,
    HAOU.POSTAL_CODE,
    HAOU.REGION_2 STATE,
    haou.town_or_city city,
    rec.name Activity_name,
    trx.comments trx_comments,
    pay.due_date trx_due_date,
    pay.gl_date trx_gl_date,
    adj.adjustment_id adjustment_id,
    adj.adjustment_number adjustment_num,
    adj.apply_date adjustment_date,
    adj.gl_date adjustment_gl_date,
    DECODE (adj.adjustment_type, 'C', look.meaning, DECODE (rec.TYPE, 'FINCHRG', 'Finance Charges', 'Adjustment' ) ) adj_type,
    ladjtype.meaning adj_type_meaning,
    adj.TYPE adj_type_code,
    ROUND (adj.amount, 2) WRITEOFF_AMOUNT,
    adj.acctd_amount adjustment_acctd_amount,
    party.party_name,
    DECODE (adj.adjustment_type, 'C', DECODE (TYPE.TYPE, 'GUAR', 'I', 'D'), '' ) d_or_i,
    gle.NAME company_name,
    gle.chart_of_accounts_id coa_id,
    gle.currency_code functional_currency,
    cur.PRECISION functional_currency_precision,
    party.party_number,
    party.party_type,
    party.orig_system_reference party_orig_sys_ref,
    party.customer_key,
    party.duns_number,
    party.person_first_name,
    party.person_middle_name,
    party.person_last_name,
    party.person_name_suffix,
    party.person_title,
    party.country,
    -- party.city,
    --party.postal_code,
    --party.state,
    party.province,
    cust.orig_system_reference cust_acct_orig_sys_ref,
    DECODE (cust.status, 'A', 'Active', 'I', 'Inactive', cust.status) cust_acct_status,
    cust.customer_type,
    acct_site.orig_system_reference cust_acct_sites_orig_sys_ref,
    DECODE (acct_site.status, 'A', 'Active', 'I', 'Inactive', acct_site.status ) status,
    acct_site.customer_category_code,
    acct_site.LANGUAGE,
    acct_site.key_account_flag,
    acct_site.tp_header_id,
    site_uses.site_use_code,
    site_uses.primary_flag,
    DECODE (site_uses.status, 'A', 'Active', 'I', 'Inactive', site_uses.status ) cust_site_uses_status,
    site_uses.orig_system_reference site_use_orig_sys_ref,
    site_uses.tax_reference,
    site_uses.tax_code,
    hou.NAME operating_unit
    --primary keys
    ,
    cust.cust_account_id,
    party.party_id,
    site_uses.site_use_id,
    acct_site.cust_acct_site_id,
    TYPE.cust_trx_type_id,
    pay.payment_schedule_id,
    rec.receivables_trx_id,
    trx.customer_trx_id,
    param.org_id,
    cur.currency_code,
    hou.organization_id,
    gle.ledger_id,
    oh.header_id,
    gl.code_combination_id
    --primary Keys
    --descr#flexfield#start
    --descr#flexfield#end
    --gl#accountff#start
    ,
    GL.SEGMENT4 GL#50328#ACCOUNT ,
    xxeis.eis_rs_fin_utility.decode_vset(GL.SEGMENT4,'XXCUS_GL_ACCOUNT') GL#50328#ACCOUNT#DESCR ,
    GL.SEGMENT3 GL#50328#COST_CENTER ,
    xxeis.eis_rs_fin_utility.decode_vset(GL.SEGMENT3,'XXCUS_GL_COSTCENTER') GL#50328#COST_CENTER#DESCR ,
    GL.SEGMENT6 GL#50328#FURTURE_USE ,
    xxeis.eis_rs_fin_utility.decode_vset(GL.SEGMENT6,'XXCUS_GL_FUTURE_USE1') GL#50328#FURTURE_USE#DESCR ,
    GL.SEGMENT7 GL#50328#FUTURE_USE_2 ,
    xxeis.eis_rs_fin_utility.decode_vset(GL.SEGMENT7,'XXCUS_GL_FUTURE_USE_2') GL#50328#FUTURE_USE_2#DESCR ,
    GL.SEGMENT2 GL#50328#LOCATION ,
    xxeis.eis_rs_fin_utility.decode_vset(GL.SEGMENT2,'XXCUS_GL_LOCATION') GL#50328#LOCATION#DESCR ,
    GL.SEGMENT1 GL#50328#PRODUCT ,
    xxeis.eis_rs_fin_utility.decode_vset(GL.SEGMENT1,'XXCUS_GL_PRODUCT') GL#50328#PRODUCT#DESCR ,
    GL.SEGMENT5 GL#50328#PROJECT_CODE ,
    xxeis.eis_rs_fin_utility.decode_vset(GL.SEGMENT5,'XXCUS_GL_PROJECT') GL#50328#PROJECT_CODE#DESCR
    --gl#accountff#end
  FROM hz_cust_accounts cust,
    hz_parties party,
    hz_cust_site_uses site_uses,
    hz_cust_acct_sites acct_site,
    Hz_Party_Sites Party_Sites ,
    HZ_LOCATIONS Locs,
    ar_lookups look,
    ar_lookups ladjtype,
    ra_cust_trx_types TYPE,
    gl_code_combinations_kfv gl, --gl Sometimes gl account will not be complete.. but still we need to show the adjustment
    FND_FLEX_VALUES_vL FV,
    ar_payment_schedules pay,
    ar_receivables_trx rec,
    ra_customer_trx trx,
    ar_adjustments adj,
    gl_ledgers gle,
    ar_system_parameters param,
    fnd_currencies cur,
    hr_operating_units hou,
    ORG_ORGANIZATION_DEFINITIONS OOD,
    hr_organization_units_v haou,
    oe_order_headers oh,
    FND_LOOKUP_VALUES_VL FLV_SHIP_MTD/*
    XXWC.XXWC_AREXTI_GETPAID_DUMP_TBL conv*/
  WHERE trx.complete_flag         = 'Y'
  AND cust.cust_account_id        = trx.bill_to_customer_id + 0
  AND cust.party_id               = party.party_id
  AND cust.cust_account_id        = acct_site.cust_account_id
  AND site_uses.site_use_id       = trx.Ship_To_Site_Use_Id
  AND acct_site.cust_acct_site_id = site_uses.cust_acct_site_id
  AND acct_site.Party_Site_Id     = Party_Sites.Party_Site_Id(+)
  AND Party_Sites.Location_Id     = Locs.Location_Id(+)
  AND trx.set_of_books_id         = gle.ledger_id
  AND trx.cust_trx_type_id        = TYPE.cust_trx_type_id
  AND trx.customer_trx_id         = pay.customer_trx_id + 0
  --AND trx.trx_number              = conv.invno(+)
  --AND conv.custno                 = cust.account_number(+)
    --AND decode(interface_header_context,'CONVERSION',cust.account_number,conv.custno) = conv.custno
    --AND cust.account_number(+)      = conv.invno
    --AND trx.customer_trx_id         = 489259
  AND pay.payment_schedule_id                                                                        = adj.payment_schedule_id
  AND ood.ORGANIZATION_ID                                                                            = haou.organization_id(+)
  AND DECODE(trx.interface_header_context,'ORDER ENTRY',to_number(INTERFACE_HEADER_ATTRIBUTE1),NULL) = OH.order_number(+)
  AND oh.ship_from_org_id                                                                            = ood.organization_id(+)
  AND FLV_SHIP_MTD.LOOKUP_TYPE(+)                                                                    ='SHIP_METHOD'
  AND FLV_SHIP_MTD.LOOKUP_CODE(+)                                                                    = Oh.SHIPPING_METHOD_CODE
  AND NVL (adj.status, 'A')                                                                          = 'A'
  AND TYPE.TYPE                                                                                     IN ('INV', 'DEP', 'GUAR', 'CM', 'DM', 'CB')
  AND look.lookup_type                                                                               = 'INV/CM'
  AND look.lookup_code                                                                               = TYPE.TYPE
  AND adj.adjustment_id                                                                              > 0
  AND adj.receivables_trx_id                                                                        IS NOT NULL
  AND adj.receivables_trx_id                                                                         = rec.receivables_trx_id
  AND adj.postable                                                                                   = 'Y'
  AND adj.TYPE                                                                                       = ladjtype.lookup_code
  AND ladjtype.lookup_type                                                                           = 'ADJUSTMENT_TYPE'
  AND adj.code_combination_id                                                                        = gl.code_combination_id(+)
  AND gl.segment2                                                                                    = FV.FLEX_VALUE(+)
  AND FV.FLEX_VALUE_SET_ID                                                                          IN
    (SELECT FS.FLEX_VALUE_SET_ID
    FROM FND_FLEX_VALUE_SETS FS
    WHERE FS.FLEX_VALUE_SET_NAME ='XXCUS_GL_LOCATION'
    )
  AND gle.ledger_id     = param.set_of_books_id
  AND gle.currency_code = cur.currency_code
  AND TYPE.org_id       = hou.organization_id
/
set scan on define on
prompt Creating View Data for Invoice Register - Write Offs
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_AR_INVOICE_REGISTER_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_AR_INVOICE_REGISTER_V',222,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Xxwc Ar Invoice Register V','EXAIRV','','');
--Delete View Columns for EIS_XXWC_AR_INVOICE_REGISTER_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_AR_INVOICE_REGISTER_V',222,FALSE);
--Inserting View Columns for EIS_XXWC_AR_INVOICE_REGISTER_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','SHIP_VIA',222,'Ship Via','SHIP_VIA','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Ship Via','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','SHIP_TO_POSTAL_CODE',222,'Ship To Postal Code','SHIP_TO_POSTAL_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Ship To Postal Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','SHIP_TO_STATE',222,'Ship To State','SHIP_TO_STATE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Ship To State','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','SHIP_TO_CITY',222,'Ship To City','SHIP_TO_CITY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Ship To City','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','POSTAL_CODE',222,'Postal Code','POSTAL_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Postal Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','STATE',222,'State','STATE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','State','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','CITY',222,'City','CITY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','City','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','GL_ACCOUNT',222,'Gl Account','GL_ACCOUNT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gl Account','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','DISCOUNT_AMT',222,'Discount Amt','DISCOUNT_AMT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Discount Amt','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','INVOICE_AMOUNT',222,'Invoice Amount','INVOICE_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Invoice Amount','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','CUSTOMER_NAME',222,'Customer Name','CUSTOMER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','CUSTOMER_NUMBER',222,'Customer Number','CUSTOMER_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','INVOICE_DATE',222,'Invoice Date','INVOICE_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Invoice Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','INVOICE_NUMBER',222,'Invoice Number','INVOICE_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Invoice Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','FISCAL_MONTH',222,'Fiscal Month','FISCAL_MONTH','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Fiscal Month','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','FISCAL_YEAR',222,'Fiscal Year','FISCAL_YEAR','','','','XXEIS_RS_ADMIN','NUMBER','','','Fiscal Year','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','AMOUNT_APPLIED',222,'Amount Applied','AMOUNT_APPLIED','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Amount Applied','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','RECEIPT_DATE',222,'Receipt Date','RECEIPT_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Receipt Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','WRITEOFF_AMOUNT',222,'Writeoff Amount','WRITEOFF_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Writeoff Amount','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','LOCATION',222,'Location','LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Location','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','ACTIVITY_NAME',222,'Activity Name','ACTIVITY_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Activity Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','ADJ_TYPE_CODE',222,'Adj Type Code','ADJ_TYPE_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Adj Type Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','ORDER_NUMBER',222,'Order Number','ORDER_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Order Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','ACCOUNT',222,'Account','ACCOUNT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Account','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','CUSTOMER_TRX_ID',222,'Customer Trx Id','CUSTOMER_TRX_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Customer Trx Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','ADJUSTMENT_ACCTD_AMOUNT',222,'Adjustment Acctd Amount','ADJUSTMENT_ACCTD_AMOUNT','','','','XXEIS_RS_ADMIN','NUMBER','','','Adjustment Acctd Amount','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','ADJUSTMENT_DATE',222,'Adjustment Date','ADJUSTMENT_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Adjustment Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','ADJUSTMENT_GL_DATE',222,'Adjustment Gl Date','ADJUSTMENT_GL_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Adjustment Gl Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','ADJUSTMENT_ID',222,'Adjustment Id','ADJUSTMENT_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Adjustment Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','ADJUSTMENT_NUM',222,'Adjustment Num','ADJUSTMENT_NUM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Adjustment Num','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','ADJUSTMENT_POSTABLE',222,'Adjustment Postable','ADJUSTMENT_POSTABLE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Adjustment Postable','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','ADJ_TYPE',222,'Adj Type','ADJ_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Adj Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','ADJ_TYPE_MEANING',222,'Adj Type Meaning','ADJ_TYPE_MEANING','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Adj Type Meaning','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','BRANCH_DESCR',222,'Branch Descr','BRANCH_DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Branch Descr','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','COA_ID',222,'Coa Id','COA_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Coa Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','COMPANY_NAME',222,'Company Name','COMPANY_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Company Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','COUNTRY',222,'Country','COUNTRY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Country','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','CURRENCY_CODE',222,'Currency Code','CURRENCY_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Currency Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','CUSTOMER_CATEGORY_CODE',222,'Customer Category Code','CUSTOMER_CATEGORY_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Category Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','CUSTOMER_KEY',222,'Customer Key','CUSTOMER_KEY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Key','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','CUSTOMER_TYPE',222,'Customer Type','CUSTOMER_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','CUST_ACCOUNT_ID',222,'Cust Account Id','CUST_ACCOUNT_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Cust Account Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','CUST_ACCT_ORIG_SYS_REF',222,'Cust Acct Orig Sys Ref','CUST_ACCT_ORIG_SYS_REF','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cust Acct Orig Sys Ref','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','CUST_ACCT_SITES_ORIG_SYS_REF',222,'Cust Acct Sites Orig Sys Ref','CUST_ACCT_SITES_ORIG_SYS_REF','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cust Acct Sites Orig Sys Ref','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','CUST_ACCT_SITE_ID',222,'Cust Acct Site Id','CUST_ACCT_SITE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Cust Acct Site Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','CUST_ACCT_STATUS',222,'Cust Acct Status','CUST_ACCT_STATUS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cust Acct Status','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','CUST_SITE_USES_STATUS',222,'Cust Site Uses Status','CUST_SITE_USES_STATUS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cust Site Uses Status','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','CUST_TRX_TYPE_ID',222,'Cust Trx Type Id','CUST_TRX_TYPE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Cust Trx Type Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','DUNS_NUMBER',222,'Duns Number','DUNS_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Duns Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','D_OR_I',222,'D Or I','D_OR_I','','','','XXEIS_RS_ADMIN','VARCHAR2','','','D Or I','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','FUNCTIONAL_CURRENCY',222,'Functional Currency','FUNCTIONAL_CURRENCY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Functional Currency','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','FUNCTIONAL_CURRENCY_PRECISION',222,'Functional Currency Precision','FUNCTIONAL_CURRENCY_PRECISION','','','','XXEIS_RS_ADMIN','NUMBER','','','Functional Currency Precision','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','HEADER_ID',222,'Header Id','HEADER_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Header Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','KEY_ACCOUNT_FLAG',222,'Key Account Flag','KEY_ACCOUNT_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Key Account Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','LANGUAGE',222,'Language','LANGUAGE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Language','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','LEDGER_ID',222,'Ledger Id','LEDGER_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Ledger Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','OPERATING_UNIT',222,'Operating Unit','OPERATING_UNIT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Operating Unit','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','ORGANIZATION_ID',222,'Organization Id','ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','ORG_ID',222,'Org Id','ORG_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Org Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','PARTY_ID',222,'Party Id','PARTY_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Party Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','PARTY_NAME',222,'Party Name','PARTY_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Party Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','PARTY_NUMBER',222,'Party Number','PARTY_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Party Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','PARTY_ORIG_SYS_REF',222,'Party Orig Sys Ref','PARTY_ORIG_SYS_REF','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Party Orig Sys Ref','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','PARTY_TYPE',222,'Party Type','PARTY_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Party Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','PAYMENT_SCHEDULE_ID',222,'Payment Schedule Id','PAYMENT_SCHEDULE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Payment Schedule Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','PERSON_FIRST_NAME',222,'Person First Name','PERSON_FIRST_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Person First Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','PERSON_LAST_NAME',222,'Person Last Name','PERSON_LAST_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Person Last Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','PERSON_MIDDLE_NAME',222,'Person Middle Name','PERSON_MIDDLE_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Person Middle Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','PERSON_NAME_SUFFIX',222,'Person Name Suffix','PERSON_NAME_SUFFIX','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Person Name Suffix','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','PERSON_TITLE',222,'Person Title','PERSON_TITLE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Person Title','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','PRIMARY_FLAG',222,'Primary Flag','PRIMARY_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Primary Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','PROVINCE',222,'Province','PROVINCE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Province','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','RECEIVABLES_TRX_ID',222,'Receivables Trx Id','RECEIVABLES_TRX_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Receivables Trx Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','SITE_USE_CODE',222,'Site Use Code','SITE_USE_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Site Use Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','SITE_USE_ID',222,'Site Use Id','SITE_USE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Site Use Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','SITE_USE_ORIG_SYS_REF',222,'Site Use Orig Sys Ref','SITE_USE_ORIG_SYS_REF','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Site Use Orig Sys Ref','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','STATUS',222,'Status','STATUS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Status','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','TAX_CODE',222,'Tax Code','TAX_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Tax Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','TAX_REFERENCE',222,'Tax Reference','TAX_REFERENCE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Tax Reference','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','TP_HEADER_ID',222,'Tp Header Id','TP_HEADER_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Tp Header Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','TRX_COMMENTS',222,'Trx Comments','TRX_COMMENTS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Trx Comments','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','TRX_CURRENCY_CODE',222,'Trx Currency Code','TRX_CURRENCY_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Trx Currency Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','TRX_DUE_DATE',222,'Trx Due Date','TRX_DUE_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Trx Due Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','TRX_GL_DATE',222,'Trx Gl Date','TRX_GL_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Trx Gl Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','TRX_TYPE',222,'Trx Type','TRX_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Trx Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','TRX_TYPE_CLASS',222,'Trx Type Class','TRX_TYPE_CLASS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Trx Type Class','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','GL#50328#ACCOUNT',222,'Accounting Flexfield : Segment ''Account'' under chart of accounts ''XXCUS Accounting Flexfield''','GL#50328#ACCOUNT','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT4','GL#Account','50328','1014550','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','GL#50328#ACCOUNT#DESCR',222,'Accounting Flexfield : Segment ''Account'' under chart of accounts ''XXCUS Accounting Flexfield''','GL#50328#ACCOUNT#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT4','GL#Account Descr','50328','1014550','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','GL#50328#COST_CENTER',222,'Accounting Flexfield : Segment ''Cost Center'' under chart of accounts ''XXCUS Accounting Flexfield''','GL#50328#COST_CENTER','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT3','GL#Cost Center','50328','1014549','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','GL#50328#COST_CENTER#DESCR',222,'Accounting Flexfield : Segment ''Cost Center'' under chart of accounts ''XXCUS Accounting Flexfield''','GL#50328#COST_CENTER#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT3','GL#Cost Center Descr','50328','1014549','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','GL#50328#FURTURE_USE',222,'Accounting Flexfield : Segment ''Furture Use'' under chart of accounts ''XXCUS Accounting Flexfield''','GL#50328#FURTURE_USE','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT6','GL#Furture Use','50328','1014552','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','GL#50328#FURTURE_USE#DESCR',222,'Accounting Flexfield : Segment ''Furture Use'' under chart of accounts ''XXCUS Accounting Flexfield''','GL#50328#FURTURE_USE#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT6','GL#Furture Use Descr','50328','1014552','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','GL#50328#FUTURE_USE_2',222,'Accounting Flexfield : Segment ''Future Use 2'' under chart of accounts ''XXCUS Accounting Flexfield''','GL#50328#FUTURE_USE_2','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT7','GL#Future Use 2','50328','1014948','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','GL#50328#FUTURE_USE_2#DESCR',222,'Accounting Flexfield : Segment ''Future Use 2'' under chart of accounts ''XXCUS Accounting Flexfield''','GL#50328#FUTURE_USE_2#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT7','GL#Future Use 2 Descr','50328','1014948','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','GL#50328#LOCATION',222,'Accounting Flexfield : Segment ''Location'' under chart of accounts ''XXCUS Accounting Flexfield''','GL#50328#LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT2','GL#Location','50328','1014548','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','GL#50328#LOCATION#DESCR',222,'Accounting Flexfield : Segment ''Location'' under chart of accounts ''XXCUS Accounting Flexfield''','GL#50328#LOCATION#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT2','GL#Location Descr','50328','1014548','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','GL#50328#PRODUCT',222,'Accounting Flexfield : Segment ''Product'' under chart of accounts ''XXCUS Accounting Flexfield''','GL#50328#PRODUCT','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT1','GL#Product','50328','1014547','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','GL#50328#PRODUCT#DESCR',222,'Accounting Flexfield : Segment ''Product'' under chart of accounts ''XXCUS Accounting Flexfield''','GL#50328#PRODUCT#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT1','GL#Product Descr','50328','1014547','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','GL#50328#PROJECT_CODE',222,'Accounting Flexfield : Segment ''Project Code'' under chart of accounts ''XXCUS Accounting Flexfield''','GL#50328#PROJECT_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT5','GL#Project Code','50328','1014551','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','GL#50328#PROJECT_CODE#DESCR',222,'Accounting Flexfield : Segment ''Project Code'' under chart of accounts ''XXCUS Accounting Flexfield''','GL#50328#PROJECT_CODE#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT5','GL#Project Code Descr','50328','1014551','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','CODE_COMBINATION_ID',222,'Code Combination Id','CODE_COMBINATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Code Combination Id','','','');
--Inserting View Components for EIS_XXWC_AR_INVOICE_REGISTER_V
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_AR_INVOICE_REGISTER_V','HZ_CUST_ACCOUNTS',222,'HZ_CUST_ACCOUNTS','CUST','CUST','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Stores Information About Customer Accounts.','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_AR_INVOICE_REGISTER_V','HZ_PARTIES',222,'HZ_PARTIES','PARTY','PARTY','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Information About Parties Such As Organizations, P','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_AR_INVOICE_REGISTER_V','HZ_CUST_SITE_USES',222,'HZ_CUST_SITE_USES_ALL','SITE_USES','SITE_USES','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Stores Business Purposes Assigned To Customer Acco','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_AR_INVOICE_REGISTER_V','HZ_CUST_ACCT_SITES',222,'HZ_CUST_ACCT_SITES_ALL','ACCT_SITE','ACCT_SITE','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Stores All Customer Account Sites Across All Opera','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_AR_INVOICE_REGISTER_V','GL_CODE_COMBINATIONS',222,'GL_CODE_COMBINATIONS','GL','GL','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Receivable Activity Account','','','','');
--Inserting View Component Joins for EIS_XXWC_AR_INVOICE_REGISTER_V
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_AR_INVOICE_REGISTER_V','HZ_CUST_ACCOUNTS','CUST',222,'EXAIRV.CUST_ACCOUNT_ID','=','CUST.CUST_ACCOUNT_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_AR_INVOICE_REGISTER_V','HZ_PARTIES','PARTY',222,'EXAIRV.PARTY_ID','=','PARTY.PARTY_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_AR_INVOICE_REGISTER_V','HZ_CUST_SITE_USES','SITE_USES',222,'EXAIRV.SITE_USE_ID','=','SITE_USES.SITE_USE_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_AR_INVOICE_REGISTER_V','HZ_CUST_ACCT_SITES','ACCT_SITE',222,'EXAIRV.CUST_ACCT_SITE_ID','=','ACCT_SITE.CUST_ACCT_SITE_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_AR_INVOICE_REGISTER_V','GL_CODE_COMBINATIONS','GL',222,'EXAIRV.CODE_COMBINATION_ID','=','GL.CODE_COMBINATION_ID','','','','','XXEIS_RS_ADMIN','','');
END;
/
set scan on define on
prompt Creating Report LOV Data for Invoice Register - Write Offs
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Invoice Register - Write Offs
xxeis.eis_rs_ins.lov( 222,'select distinct party_name from hz_parties p, hz_cust_accounts c
	where p.party_id = c.party_id','null','Customer Name','Displays List of Values for Customer Name ','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 222,'SELECT  CONCATENATED_SEGMENTS
  FROM   gl_code_combinations_kfv glcc
 WHERE  chart_of_accounts_id IN (
                SELECT led.chart_of_accounts_id
                  from gl_ledgers led
                 where gl_security_pkg.validate_access(led.ledger_id) = ''TRUE'')
   and nvl(summary_flag,''N'') = upper(''N'')
order by CONCATENATED_SEGMENTS','','EIS_GLACCOUNT_CONCAT_LOV','EIS_GLACCOUNT_CONCAT_LOV','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 222,'select account_number from hz_cust_accounts','null','Customer Number','Displays List of Values for Customer Number','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 222,'select distinct trx_number
from ra_customer_trx_all','null','INVOICES','List of All AR Invoices','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 222,'SELECT DISTINCT GPS.PERIOD_NAME
FROM      HR_OPERATING_UNITS          HOU,
          GL_LEDGERS                  GL,
          gl_period_statuses          gps
WHERE              
 GL.LEDGER_ID                      = HOU.SET_OF_BOOKS_ID
AND GL.LEDGER_ID                      = GPS.SET_OF_BOOKS_ID
AND GL.ACCOUNTED_PERIOD_TYPE          = GPS.PERIOD_TYPE
order by GPS.PERIOD_NAME','','Fiscal Month','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 222,'select distinct name from AR_RECEIVABLES_TRX_all','','XXWC Receivables Activities','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 222,'select  lookup_code, meaning from ar_lookups ladjtype where ladjtype.lookup_type  = ''ADJUSTMENT_TYPE''','','AR Adjustment Types','','XXEIS_RS_ADMIN',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Invoice Register - Write Offs
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Invoice Register - Write Offs
xxeis.eis_rs_utility.delete_report_rows( 'Invoice Register - Write Offs' );
--Inserting Report - Invoice Register - Write Offs
xxeis.eis_rs_ins.r( 222,'Invoice Register - Write Offs','','Provide the detail of all invoices transacted for a certain period.','','','','XXEIS_RS_ADMIN','EIS_XXWC_AR_INVOICE_REGISTER_V','Y','','','XXEIS_RS_ADMIN','','N','White Cap Reports','','CSV,HTML,Html Summary,Pivot Excel,EXCEL,','');
--Inserting Report Columns - Invoice Register - Write Offs
xxeis.eis_rs_ins.rc( 'Invoice Register - Write Offs',222,'CITY','City','City','','','','','14','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_INVOICE_REGISTER_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Register - Write Offs',222,'CUSTOMER_NAME','Customer Name','Customer Name','','','','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_INVOICE_REGISTER_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Register - Write Offs',222,'CUSTOMER_NUMBER','Customer Number','Customer Number','','','','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_INVOICE_REGISTER_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Register - Write Offs',222,'DISCOUNT_AMT','Discount Amt','Discount Amt','','','','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_INVOICE_REGISTER_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Register - Write Offs',222,'FISCAL_MONTH','Fiscal Month','Fiscal Month','','','','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_INVOICE_REGISTER_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Register - Write Offs',222,'FISCAL_YEAR','Fiscal Year','Fiscal Year','','','','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_INVOICE_REGISTER_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Register - Write Offs',222,'GL_ACCOUNT','Gl Account','Gl Account','','','','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_INVOICE_REGISTER_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Register - Write Offs',222,'INVOICE_AMOUNT','Invoice Amount','Invoice Amount','','','','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_INVOICE_REGISTER_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Register - Write Offs',222,'INVOICE_DATE','Invoice Date','Invoice Date','','','','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_INVOICE_REGISTER_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Register - Write Offs',222,'INVOICE_NUMBER','Invoice Number','Invoice Number','','','','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_INVOICE_REGISTER_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Register - Write Offs',222,'POSTAL_CODE','Postal Code','Postal Code','','','','','16','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_INVOICE_REGISTER_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Register - Write Offs',222,'SHIP_TO_CITY','Ship To City','Ship To City','','','','','17','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_INVOICE_REGISTER_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Register - Write Offs',222,'SHIP_TO_POSTAL_CODE','Ship To Postal Code','Ship To Postal Code','','','','','19','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_INVOICE_REGISTER_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Register - Write Offs',222,'SHIP_TO_STATE','Ship To State','Ship To State','','','','','18','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_INVOICE_REGISTER_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Register - Write Offs',222,'SHIP_VIA','Ship Via','Ship Via','','','','','12','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_INVOICE_REGISTER_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Register - Write Offs',222,'STATE','State','State','','','','','15','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_INVOICE_REGISTER_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Register - Write Offs',222,'AMOUNT_APPLIED','Amount Applied','Amount Applied','','','','','20','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_INVOICE_REGISTER_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Register - Write Offs',222,'RECEIPT_DATE','Receipt Date','Receipt Date','','','','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_INVOICE_REGISTER_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Register - Write Offs',222,'WRITEOFF_AMOUNT','Writeoff Amount','Writeoff Amount','','','','','21','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_INVOICE_REGISTER_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Register - Write Offs',222,'LOCATION','Location','Location','','','','','13','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_INVOICE_REGISTER_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Register - Write Offs',222,'ORDER_NUMBER','Order Number','Order Number','','','','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_INVOICE_REGISTER_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Register - Write Offs',222,'ACTIVITY_NAME','Activity Name','Activity Name','','','','','22','','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_INVOICE_REGISTER_V','','');
--Inserting Report Parameters - Invoice Register - Write Offs
xxeis.eis_rs_ins.rp( 'Invoice Register - Write Offs',222,'Customer Name','Customer Name','CUSTOMER_NAME','IN','Customer Name','','VARCHAR2','N','Y','3','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Invoice Register - Write Offs',222,'Customer Number','Customer Number','CUSTOMER_NUMBER','IN','Customer Number','','VARCHAR2','N','Y','4','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Invoice Register - Write Offs',222,'Fiscal Month','Fiscal Month','FISCAL_MONTH','IN','Fiscal Month','','VARCHAR2','Y','Y','1','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Invoice Register - Write Offs',222,'Invoice Number','Invoice Number','INVOICE_NUMBER','IN','INVOICES','','VARCHAR2','N','Y','2','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Invoice Register - Write Offs',222,'Receivable Activity','Receivable Activity','ACTIVITY_NAME','IN','XXWC Receivables Activities','','VARCHAR2','N','Y','5','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Invoice Register - Write Offs',222,'Receivables Activity Account','GL Account','ACCOUNT','IN','EIS_GLACCOUNT_CONCAT_LOV','','VARCHAR2','N','Y','6','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','Y','','','');
xxeis.eis_rs_ins.rp( 'Invoice Register - Write Offs',222,'Adjustment Type','Adjustment Type','ADJ_TYPE_CODE','IN','AR Adjustment Types','','VARCHAR2','N','Y','7','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
--Inserting Report Conditions - Invoice Register - Write Offs
xxeis.eis_rs_ins.rcn( 'Invoice Register - Write Offs',222,'CUSTOMER_NAME','IN',':Customer Name','','','Y','3','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Invoice Register - Write Offs',222,'CUSTOMER_NUMBER','IN',':Customer Number','','','Y','4','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Invoice Register - Write Offs',222,'FISCAL_MONTH','IN',':Fiscal Month','','','Y','1','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Invoice Register - Write Offs',222,'INVOICE_NUMBER','IN',':Invoice Number','','','Y','2','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Invoice Register - Write Offs',222,'ACCOUNT','IN',':Receivables Activity Account','','','Y','6','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Invoice Register - Write Offs',222,'ADJ_TYPE_CODE','IN',':Adjustment Type','','','Y','7','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Invoice Register - Write Offs',222,'ACTIVITY_NAME','IN',':Receivable Activity','','','Y','5','Y','XXEIS_RS_ADMIN');
--Inserting Report Sorts - Invoice Register - Write Offs
xxeis.eis_rs_ins.rs( 'Invoice Register - Write Offs',222,'FISCAL_MONTH','ASC','XXEIS_RS_ADMIN','','');
--Inserting Report Triggers - Invoice Register - Write Offs
--Inserting Report Templates - Invoice Register - Write Offs
xxeis.eis_rs_ins.R_Tem( 'Invoice Register - Write Offs','Invoice Register','Seeded Template for Invoice Register','','','','','','','','','','','','XXEIS_RS_ADMIN');
--Inserting Report Portals - Invoice Register - Write Offs
--Inserting Report Dashboards - Invoice Register - Write Offs
--Inserting Report Security - Invoice Register - Write Offs
xxeis.eis_rs_ins.rsec( 'Invoice Register - Write Offs','222','','21404',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Invoice Register - Write Offs','222','','20678',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Invoice Register - Write Offs','20005','','50900',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Invoice Register - Write Offs','222','','50894',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Invoice Register - Write Offs','101','','50723',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Invoice Register - Write Offs','222','','50871',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Invoice Register - Write Offs','1','','51028',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Invoice Register - Write Offs','401','','50872',222,'XXEIS_RS_ADMIN','','');
--Inserting Report Pivots - Invoice Register - Write Offs
END;
/
set scan on define on
