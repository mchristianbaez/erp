--Report Name            : Time Sensitive Mgmt - WC
--Import Instructions : 1. If RTF template needs to be uploaded to report automatically then move the RTF template to physical path of APPS_DATA_FILE_DIR oracle directory. Check dba_directories view to get the physical path of oracle directory. 
--                         (If you want to use any other oracle directory other than APPS_DATA_FILE_DIR then replace APPS_DATA_FILE_DIR with your oracle directory name below.)
--                      2. Make sure XXEIS schema has read/write permissions to above used oracle directory and give full permissions to the directory at OS level.
--                      3. Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
exec xxeis.eis_rs_ins.g_rtf_dir := 'APPS_DATA_FILE_DIR' ;
--Creating View EIS_XXWC_INV_TIME_MGMT_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_INV_TIME_MGMT_V
Create or replace View XXEIS.EIS_XXWC_INV_TIME_MGMT_V
(INV_ORG_NAME,ORGANIZATION_ID,ORGANIZATION_CODE,BUYER,MASTER_ORG_CODE,SUBINVENTORY_CODE,LOCATOR_ID,LOCATOR,INVENTORY_ITEM_ID,ITEM,ITEM_DESCRIPTION,REVISION,UNIT_OF_MEASURE,ON_HAND,PRIMARY_BIN_LOC,ALTERNATE_BIN_LOC,OLDEST_BORN_DATE,SHELF_LIFE_DAYS,VENDOR_NAME,VENDOR_NUMBER,UNPACKED,PACKED,COST_GROUP_ID,LOT_NUMBER,SUBINVENTORY_STATUS_ID,LOCATOR_STATUS_ID,LOT_STATUS_ID,PLANNING_TP_TYPE,PLANNING_ORGANIZATION_ID,
PLANNING_ORGANIZATION,OWNING_TP_TYPE,OWNING_ORGANIZATION_ID,OWNING_ORGANIZATION,LOT_EXPIRY_DATE,LPN,LPN_ID,STATUS_CODE,UNIT_NUMBER,SUBINVENTORY_TYPE,SECONDARY_INVENTORY_NAME,DATE_RECEIVED,ONHAND_QUANTITIES_ID,INVENTORY_LOCATION_ID,LANGUAGE,MIN_MINMAX_QUANTITY,MAX_MINMAX_QUANTITY,LIST_PRICE_PER_UNIT,MP_ORGANIZATION_ID,MIL_ORGANIZATION_ID,MSI_ORGANIZATION_ID,MSIV_INVENTORY_ITEM_ID,MSIV_ORGANIZATION_ID,MMS_STATUS_ID,OOD_ORGANIZATION_ID,OODW_ORGANIZATION_ID,OODP_ORGANIZATION_ID,HAOU_ORGANIZATION_ID,MLN_INVENTORY_ITEM_ID,MLN_ORGANIZATION_ID,MLN_LOT_NUMBER,REGION) AS 
SELECT haou.name inv_org_name,
    moq.organization_id organization_id,
    mp.organization_code organization_code,
    ppf.full_name buyer,
    ood.organization_code master_org_code,
    moq.subinventory_code subinventory_code,
    moq.locator_id locator_id,
    mil.concatenated_segments LOCATOR,
    moq.inventory_item_id inventory_item_id,
    msiv.concatenated_segments item,
    MSIV.description item_description,
    moq.revision revision,
    muom.unit_of_measure,
    moq.primary_transaction_quantity on_hand,
    xxeis.eis_rs_xxwc_com_util_pkg.get_primary_bin_loc(msiv.inventory_item_id,msiv.organization_id) primary_bin_loc,
    XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_ALTERNATE_BIN_LOC(MSIv.INVENTORY_ITEM_ID,MSIv.ORGANIZATION_ID) ALTERNATE_BIN_LOC,
    moq.orig_date_received oldest_born_date,
    msiv.shelf_life_days shelf_life_days ,
    xxeis.eis_po_xxwc_isr_util_qa_pkg.get_vendor_name ( msiv.inventory_item_id, msiv.organization_id) vendor_name,
    xxeis.eis_po_xxwc_isr_util_qa_pkg.get_vendor_number ( msiv.inventory_item_id, msiv.organization_id) vendor_number,
    DECODE (moq.containerized_flag, 1, 0, moq.primary_transaction_quantity ) unpacked,
    DECODE (moq.containerized_flag, 1, moq.primary_transaction_quantity, 0 ) packed,
    moq.cost_group_id cost_group_id,
    moq.lot_number lot_number,
    -- TO_CHAR (NULL) serial_number,
    --mil.project_id project_id,
    --mil.task_id task_id,
    msi.status_id subinventory_status_id,
    mil.status_id locator_status_id,
    mln.status_id lot_status_id,
    moq.planning_tp_type planning_tp_type,
    moq.planning_organization_id planning_organization_id,
    oodp.organization_name planning_organization,
    moq.owning_tp_type owning_tp_type,
    moq.owning_organization_id owning_organization_id,
    oodw.organization_name owning_organization,
    mln.expiration_date lot_expiry_date,
    mlv.lpn,
    moq.lpn_id,
    mms.status_code,
    pun.unit_number,
    msi.subinventory_type,
    msi.secondary_inventory_name,
    moq.date_received,
    moq.onhand_quantities_id,
    mil.inventory_location_id,
    muom.language ,
    msiv.MIN_MINMAX_QUANTITY ,
    msiv.MAX_MINMAX_QUANTITY ,
    MSIV.LIST_PRICE_PER_UNIT ,
    -- primary keys added for component_joins
    mp.organization_id mp_organization_id,
    mil.organization_id mil_organization_id,
    msi.organization_id msi_organization_id,
    msiv.inventory_item_id msiv_inventory_item_id,
    msiv.organization_id msiv_organization_id,
    mms.status_id mms_status_id,
    ood.organization_id ood_organization_id,
    oodw.organization_id oodw_organization_id,
    OODP.ORGANIZATION_ID OODP_ORGANIZATION_ID,
    HAOU.ORGANIZATION_ID HAOU_ORGANIZATION_ID,
    MLN.INVENTORY_ITEM_ID MLN_INVENTORY_ITEM_ID,
    MLN.ORGANIZATION_ID MLN_ORGANIZATION_ID ,
    MLN.LOT_NUMBER MLN_LOT_NUMBER,
    mp.ATTRIBUTE9 REGION
    --,msiv.SEGMENT1 item_num
    --descr#flexfield#start
    --descr#flexfield#end
    --kff#start
    --kff#end
    --gl#accountff#start
    --gl#accountff#end
  FROM mtl_parameters mp,
    mtl_item_locations_kfv mil,
    mtl_secondary_inventories msi,
    mtl_lot_numbers mln,
    mtl_system_items_kfv msiv,
    mtl_onhand_quantities_detail moq,
    mtl_units_of_measure_tl muom,
    mtl_onhand_lpn_v mlv,
    mtl_material_statuses_vl mms,
    pjm_unit_numbers pun,
    org_organization_definitions ood,
    org_organization_definitions oodw,
    org_organization_definitions oodp,
    hr_all_organization_units haou,
    per_people_f ppf
  WHERE moq.organization_id    = mp.organization_id
  AND moq.organization_id      = mil.organization_id(+)
  AND moq.locator_id           = mil.inventory_location_id(+)
  AND moq.organization_id      = msi.organization_id
  AND moq.subinventory_code    = msi.secondary_inventory_name
  AND moq.organization_id      = mln.organization_id(+)
  AND moq.inventory_item_id    = mln.inventory_item_id(+)
  AND moq.lot_number           = mln.lot_number(+)
  AND msiv.buyer_id            = ppf.person_id(+)
  AND ppf.effective_start_date =
    (SELECT MAX(effective_start_date)
    FROM per_people_f ppf1
    WHERE ppf1.person_id = ppf.person_id
    )
  AND moq.organization_id           = msiv.organization_id
  AND moq.inventory_item_id         = msiv.inventory_item_id
  AND muom.uom_code                 = msiv.primary_uom_code
  AND muom.language                 =userenv('LANG')
  AND mlv.lpn_id(+)                 = moq.lpn_id
  AND mms.status_id(+)              = mil.status_id
  AND pun.master_organization_id(+) = moq.organization_id
  AND mp.master_organization_id     = ood.organization_id
  AND moq.planning_organization_id  = oodp.organization_id
  AND moq.owning_organization_id    = oodw.organization_id
  AND haou.organization_id          = msi.organization_id
  AND ((msiv.shelf_life_days        >0
  AND msiv.shelf_life_days         <10000)
  OR moq.lot_number                IS NOT NULL)
  AND EXISTS
    (SELECT 1
    FROM XXEIS.EIS_ORG_ACCESS_V
    WHERE organization_id = msi.organization_id
    )
/
set scan on define on
prompt Creating View Data for Time Sensitive Mgmt - WC
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_INV_TIME_MGMT_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_INV_TIME_MGMT_V',401,'On-Hand quantities of Inventory Items','','','','XXEIS_RS_ADMIN','XXEIS','Eis Inv Onhand Quantity V','EIOQV','','');
--Delete View Columns for EIS_XXWC_INV_TIME_MGMT_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_INV_TIME_MGMT_V',401,FALSE);
--Inserting View Columns for EIS_XXWC_INV_TIME_MGMT_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','STATUS_CODE',401,'Status Code','STATUS_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Status Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','UNIT_NUMBER',401,'Unit Number','UNIT_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Unit Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','SUBINVENTORY_TYPE',401,'Subinventory Type','SUBINVENTORY_TYPE','','','','XXEIS_RS_ADMIN','NUMBER','','','Subinventory Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','INV_ORG_NAME',401,'Inv Org Name','INV_ORG_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Inv Org Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','ORGANIZATION_CODE',401,'Organization Code','ORGANIZATION_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Organization Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','MASTER_ORG_CODE',401,'Master Org Code','MASTER_ORG_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Master Org Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','SUBINVENTORY_CODE',401,'Subinventory Code','SUBINVENTORY_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Subinventory Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','LOCATOR',401,'Locator','LOCATOR','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Locator','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','ITEM',401,'Item','ITEM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Item','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','REVISION',401,'Revision','REVISION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Revision','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','OWNING_ORGANIZATION',401,'Owning Organization','OWNING_ORGANIZATION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Owning Organization','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','PLANNING_ORGANIZATION',401,'Planning Organization','PLANNING_ORGANIZATION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Planning Organization','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','UNIT_OF_MEASURE',401,'Unit Of Measure','UNIT_OF_MEASURE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Unit Of Measure','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','ON_HAND',401,'On Hand','ON_HAND','','','','XXEIS_RS_ADMIN','NUMBER','','','On Hand','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','UNPACKED',401,'Unpacked','UNPACKED','','','','XXEIS_RS_ADMIN','NUMBER','','','Unpacked','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','PACKED',401,'Packed','PACKED','','','','XXEIS_RS_ADMIN','NUMBER','','','Packed','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','LOT_NUMBER',401,'Lot Number','LOT_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Lot Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','LOT_EXPIRY_DATE',401,'Lot Expiry Date','LOT_EXPIRY_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Lot Expiry Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','MIL_ORGANIZATION_ID',401,'Mil Organization Id','MIL_ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Mil Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','MMS_STATUS_ID',401,'Mms Status Id','MMS_STATUS_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Mms Status Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','MP_ORGANIZATION_ID',401,'Mp Organization Id','MP_ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Mp Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','MSIV_INVENTORY_ITEM_ID',401,'Msiv Inventory Item Id','MSIV_INVENTORY_ITEM_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Msiv Inventory Item Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','MSIV_ORGANIZATION_ID',401,'Msiv Organization Id','MSIV_ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Msiv Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','MSI_ORGANIZATION_ID',401,'Msi Organization Id','MSI_ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Msi Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','OODP_ORGANIZATION_ID',401,'Oodp Organization Id','OODP_ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Oodp Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','OODW_ORGANIZATION_ID',401,'Oodw Organization Id','OODW_ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Oodw Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','OOD_ORGANIZATION_ID',401,'Ood Organization Id','OOD_ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Ood Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','MLN_INVENTORY_ITEM_ID',401,'Mln Inventory Item Id','MLN_INVENTORY_ITEM_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Mln Inventory Item Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','MLN_LOT_NUMBER',401,'Mln Lot Number','MLN_LOT_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Mln Lot Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','MLN_ORGANIZATION_ID',401,'Mln Organization Id','MLN_ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Mln Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','ORGANIZATION_ID',401,'Organization Id','ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','LOCATOR_ID',401,'Locator Id','LOCATOR_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Locator Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','INVENTORY_ITEM_ID',401,'Inventory Item Id','INVENTORY_ITEM_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Inventory Item Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','INVENTORY_LOCATION_ID',401,'Inventory Location Id','INVENTORY_LOCATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Inventory Location Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','LANGUAGE',401,'Language','LANGUAGE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Language','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','LIST_PRICE_PER_UNIT',401,'List Price Per Unit','LIST_PRICE_PER_UNIT','','','','XXEIS_RS_ADMIN','NUMBER','','','List Price Per Unit','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','LPN',401,'Lpn','LPN','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Lpn','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','LPN_ID',401,'Lpn Id','LPN_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Lpn Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','MAX_MINMAX_QUANTITY',401,'Max Minmax Quantity','MAX_MINMAX_QUANTITY','','','','XXEIS_RS_ADMIN','NUMBER','','','Max Minmax Quantity','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','MIN_MINMAX_QUANTITY',401,'Min Minmax Quantity','MIN_MINMAX_QUANTITY','','','','XXEIS_RS_ADMIN','NUMBER','','','Min Minmax Quantity','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','ONHAND_QUANTITIES_ID',401,'Onhand Quantities Id','ONHAND_QUANTITIES_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Onhand Quantities Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','DATE_RECEIVED',401,'Date Received','DATE_RECEIVED','','','','XXEIS_RS_ADMIN','DATE','','','Date Received','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','SECONDARY_INVENTORY_NAME',401,'Secondary Inventory Name','SECONDARY_INVENTORY_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Secondary Inventory Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','COST_GROUP_ID',401,'Cost Group Id','COST_GROUP_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Cost Group Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','PROJECT_ID',401,'Project Id','PROJECT_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Project Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','TASK_ID',401,'Task Id','TASK_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Task Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','SUBINVENTORY_STATUS_ID',401,'Subinventory Status Id','SUBINVENTORY_STATUS_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Subinventory Status Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','LOCATOR_STATUS_ID',401,'Locator Status Id','LOCATOR_STATUS_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Locator Status Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','LOT_STATUS_ID',401,'Lot Status Id','LOT_STATUS_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Lot Status Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','PLANNING_TP_TYPE',401,'Planning Tp Type','PLANNING_TP_TYPE','','','','XXEIS_RS_ADMIN','NUMBER','','','Planning Tp Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','PLANNING_ORGANIZATION_ID',401,'Planning Organization Id','PLANNING_ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Planning Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','OWNING_TP_TYPE',401,'Owning Tp Type','OWNING_TP_TYPE','','','','XXEIS_RS_ADMIN','NUMBER','','','Owning Tp Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','OWNING_ORGANIZATION_ID',401,'Owning Organization Id','OWNING_ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Owning Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','HAOU_ORGANIZATION_ID',401,'Haou Organization Id','HAOU_ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Haou Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','ALTERNATE_BIN_LOC',401,'Alternate Bin Loc','ALTERNATE_BIN_LOC','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Alternate Bin Loc','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','BUYER',401,'Buyer','BUYER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Buyer','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','ITEM_DESCRIPTION',401,'Item Description','ITEM_DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Item Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','OLDEST_BORN_DATE',401,'Oldest Born Date','OLDEST_BORN_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Oldest Born Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','PRIMARY_BIN_LOC',401,'Primary Bin Loc','PRIMARY_BIN_LOC','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Primary Bin Loc','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','REGION',401,'Region','REGION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Region','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','SHELF_LIFE_DAYS',401,'Shelf Life Days','SHELF_LIFE_DAYS','','','','XXEIS_RS_ADMIN','NUMBER','','','Shelf Life Days','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','VENDOR_NAME',401,'Vendor Name','VENDOR_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','VENDOR_NUMBER',401,'Vendor Number','VENDOR_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Number','','','');
--Inserting View Components for EIS_XXWC_INV_TIME_MGMT_V
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_TIME_MGMT_V','PA_TASKS',401,'PA_TASKS','MTV','','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','1894391','','N','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_TIME_MGMT_V','MTL_PARAMETERS',401,'MTL_PARAMETERS','MP','MP','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','1894391','','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_TIME_MGMT_V','CST_COST_GROUPS',401,'CST_COST_GROUPS','CCG','CCG','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','1894391','','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_TIME_MGMT_V','MTL_MATERIAL_STATUSES_VL',401,'MTL_MATERIAL_STATUSES_B','MMS','MMS','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','1894391','','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_TIME_MGMT_V','MTL_ITEM_LOCATIONS_KFV',401,'MTL_ITEM_LOCATIONS','MIL','MIL','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','1894391','','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_TIME_MGMT_V','PJM_UNIT_NUMBERS',401,'PJM_UNIT_NUMBERS','PUN','PUN','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','1894391','','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_TIME_MGMT_V','MTL_SECONDARY_INVENTORIES',401,'MTL_SECONDARY_INVENTORIES','MSI','MSI','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','1894391','','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_TIME_MGMT_V','HR_ORGANIZATION_UNITS',401,'HR_ALL_ORGANIZATION_UNITS','OOD','OOD','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','master_organization_id','N','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_TIME_MGMT_V','HR_ORGANIZATION_UNITS',401,'HR_ALL_ORGANIZATION_UNITS','OODW','OODW','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','owning_organization_id','N','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_TIME_MGMT_V','MTL_LOT_NUMBERS',401,'MTL_LOT_NUMBERS','MLN','MLN','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','1894391','','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_TIME_MGMT_V','HR_ORGANIZATION_UNITS',401,'HR_ALL_ORGANIZATION_UNITS','OODP','OOP','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','planning_organization_id','N','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_TIME_MGMT_V','MTL_SYSTEM_ITEMS_KFV',401,'MTL_SYSTEM_ITEMS_B','MSIV','MSIV','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','1894391','','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_TIME_MGMT_V','CST_COST_GROUP_ACCOUNTS',401,'CST_COST_GROUP_ACCOUNTS','CCGA','CCGA','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','1894391','','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_TIME_MGMT_V','HR_ORGANIZATION_UNITS',401,'HR_ALL_ORGANIZATION_UNITS','HAOU','HAOU','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','','N','','','');
--Inserting View Component Joins for EIS_XXWC_INV_TIME_MGMT_V
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_TIME_MGMT_V','PA_TASKS','MTV',401,'EIOQV.MTV_TASK_ID','=','MTV.TASK_ID(+)','','','','','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_TIME_MGMT_V','MTL_PARAMETERS','MP',401,'EIOQV.MP_ORGANIZATION_ID','=','MP.ORGANIZATION_ID(+)','','','','','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_TIME_MGMT_V','CST_COST_GROUPS','CCG',401,'EIOQV.CCG_COST_GROUP_ID','=','CCG.COST_GROUP_ID(+)','','','','','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_TIME_MGMT_V','MTL_MATERIAL_STATUSES_VL','MMS',401,'EIOQV.MMS_STATUS_ID','=','MMS.STATUS_ID(+)','','','','','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_TIME_MGMT_V','MTL_ITEM_LOCATIONS_KFV','MIL',401,'EIOQV.INVENTORY_LOCATION_ID','=','MIL.INVENTORY_LOCATION_ID(+)','','','','','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_TIME_MGMT_V','MTL_ITEM_LOCATIONS_KFV','MIL',401,'EIOQV.MIL_ORGANIZATION_ID','=','MIL.ORGANIZATION_ID(+)','','','','','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_TIME_MGMT_V','PJM_UNIT_NUMBERS','PUN',401,'EIOQV.UNIT_NUMBER','=','PUN.UNIT_NUMBER(+)','','','','','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_TIME_MGMT_V','MTL_SECONDARY_INVENTORIES','MSI',401,'EIOQV.MSI_ORGANIZATION_ID','=','MSI.ORGANIZATION_ID(+)','','','','','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_TIME_MGMT_V','MTL_SECONDARY_INVENTORIES','MSI',401,'EIOQV.SECONDARY_INVENTORY_NAME','=','MSI.SECONDARY_INVENTORY_NAME(+)','','','','','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_TIME_MGMT_V','HR_ORGANIZATION_UNITS','OOD',401,'EIOQV.OOD_ORGANIZATION_ID','=','OOD.ORGANIZATION_ID(+)','','','','','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_TIME_MGMT_V','HR_ORGANIZATION_UNITS','OODW',401,'EIOQV.OODW_ORGANIZATION_ID','=','OODW.ORGANIZATION_ID(+)','','','','','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_TIME_MGMT_V','MTL_LOT_NUMBERS','MLN',401,'EIOQV.MLN_INVENTORY_ITEM_ID','=','MLN.INVENTORY_ITEM_ID(+)','','','','','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_TIME_MGMT_V','MTL_LOT_NUMBERS','MLN',401,'EIOQV.MLN_LOT_NUMBER','=','MLN.LOT_NUMBER(+)','','','','','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_TIME_MGMT_V','MTL_LOT_NUMBERS','MLN',401,'EIOQV.MLN_ORGANIZATION_ID','=','MLN.ORGANIZATION_ID(+)','','','','','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_TIME_MGMT_V','HR_ORGANIZATION_UNITS','OODP',401,'EIOQV.OODP_ORGANIZATION_ID','=','OODP.ORGANIZATION_ID(+)','','','','','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_TIME_MGMT_V','MTL_SYSTEM_ITEMS_KFV','MSIV',401,'EIOQV.MSIV_INVENTORY_ITEM_ID','=','MSIV.INVENTORY_ITEM_ID(+)','','','','','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_TIME_MGMT_V','MTL_SYSTEM_ITEMS_KFV','MSIV',401,'EIOQV.MSIV_ORGANIZATION_ID','=','MSIV.ORGANIZATION_ID(+)','','','','','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_TIME_MGMT_V','CST_COST_GROUP_ACCOUNTS','CCGA',401,'EIOQV.CCGA_COST_GROUP_ID','=','CCGA.COST_GROUP_ID(+)','','','','','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_TIME_MGMT_V','CST_COST_GROUP_ACCOUNTS','CCGA',401,'EIOQV.CCGA_ORGANIZATION_ID','=','CCGA.ORGANIZATION_ID(+)','','','','','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_TIME_MGMT_V','HR_ORGANIZATION_UNITS','HAOU',401,'EIOQV.HAOU_ORGANIZATION_ID','=','HAOU.ORGANIZATION_ID(+)','','','','','XXEIS_RS_ADMIN','','');
END;
/
set scan on define on
prompt Creating Report LOV Data for Time Sensitive Mgmt - WC
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Time Sensitive Mgmt - WC
xxeis.eis_rs_ins.lov( 401,'SELECT DISTINCT concatenated_segments item, description FROM mtl_system_items_kfv msi,
                org_organization_definitions ood
          WHERE msi.organization_id = ood.organization_id
            AND ood.operating_unit = fnd_profile.VALUE (''ORG_ID'')
       ORDER BY concatenated_segments','','EIS_INV_ITEM_LOV','List of all inventory items.','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'select SECONDARY_INVENTORY_NAME SUB_INVENTORY,HAOU.NAME ORGANIZATION_NAME
from MTL_SECONDARY_INVENTORIES MSI,
     HR_ALL_ORGANIZATION_UNITS HAOU
WHERE 1=1
AND MSI.ORGANIZATION_ID = HAOU.organization_id','','EIS_INV_SUBINVENTORY_LOV','List of All SubInventories.','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'select list_name from XXWC_PARAM_LIST where list_type = ''Supplier'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Supplier List','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'select list_name from XXWC_PARAM_LIST where list_type = ''Org'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Org List','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'select distinct ATTRIBUTE9 Region from mtl_parameters','','Region Lov','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'SELECT organization_code code,organization_name name
FROM ORG_ORGANIZATION_DEFINITIONS OOD
WHERE EXISTS
  (SELECT 1
  FROM XXEIS.EIS_ORG_ACCESS_V
  WHERE organization_id = ood.organization_id
  )
ORDER BY organization_code','','XXWC Inventory Org List','List of All Inventory Orgs under a given operating unit.','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'select list_name from XXWC_PARAM_LIST where list_type = ''Item'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Item List','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'select vendor_name from po_vendors','','XXWC Vendors','','XXEIS_RS_ADMIN',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Time Sensitive Mgmt - WC
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Time Sensitive Mgmt - WC
xxeis.eis_rs_utility.delete_report_rows( 'Time Sensitive Mgmt - WC' );
--Inserting Report - Time Sensitive Mgmt - WC
xxeis.eis_rs_ins.r( 401,'Time Sensitive Mgmt - WC','','The Onhand Quantity Report displays the total quantity of an item in a subinventory.','','','','XXEIS_RS_ADMIN','EIS_XXWC_INV_TIME_MGMT_V','Y','','','XXEIS_RS_ADMIN','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - Time Sensitive Mgmt - WC
xxeis.eis_rs_ins.rc( 'Time Sensitive Mgmt - WC',401,'ITEM','Item','Item','','','default','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_TIME_MGMT_V','','');
xxeis.eis_rs_ins.rc( 'Time Sensitive Mgmt - WC',401,'LOT_EXPIRY_DATE','Lot Expiry Date','Lot Expiry Date','','','default','','12','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_TIME_MGMT_V','','');
xxeis.eis_rs_ins.rc( 'Time Sensitive Mgmt - WC',401,'LOT_NUMBER','Lot Number','Lot Number','','','default','','13','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_TIME_MGMT_V','','');
xxeis.eis_rs_ins.rc( 'Time Sensitive Mgmt - WC',401,'ON_HAND','On Hand','On Hand','','~,~.~0','default','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_TIME_MGMT_V','','');
xxeis.eis_rs_ins.rc( 'Time Sensitive Mgmt - WC',401,'ORGANIZATION_CODE','Organization Code','Organization Code','','','default','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_TIME_MGMT_V','','');
xxeis.eis_rs_ins.rc( 'Time Sensitive Mgmt - WC',401,'SUBINVENTORY_CODE','Subinventory Code','Subinventory Code','','','default','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_TIME_MGMT_V','','');
xxeis.eis_rs_ins.rc( 'Time Sensitive Mgmt - WC',401,'UNIT_OF_MEASURE','Unit Of Measure','Unit Of Measure','','','default','','14','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_TIME_MGMT_V','','');
xxeis.eis_rs_ins.rc( 'Time Sensitive Mgmt - WC',401,'ALTERNATE_BIN_LOC','Bin2','Alternate Bin Loc','','','default','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_TIME_MGMT_V','','');
xxeis.eis_rs_ins.rc( 'Time Sensitive Mgmt - WC',401,'BUYER','Buyer','Buyer','','','default','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_TIME_MGMT_V','','');
xxeis.eis_rs_ins.rc( 'Time Sensitive Mgmt - WC',401,'ITEM_DESCRIPTION','Item Description','Item Description','','','default','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_TIME_MGMT_V','','');
xxeis.eis_rs_ins.rc( 'Time Sensitive Mgmt - WC',401,'OLDEST_BORN_DATE','Oldest Born Date','Oldest Born Date','','','default','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_TIME_MGMT_V','','');
xxeis.eis_rs_ins.rc( 'Time Sensitive Mgmt - WC',401,'SHELF_LIFE_DAYS','SL Days','Shelf Life Days','','~,~.~0','default','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_TIME_MGMT_V','','');
xxeis.eis_rs_ins.rc( 'Time Sensitive Mgmt - WC',401,'VENDOR_NAME','Supplier Name','Vendor Name','','','default','','16','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_TIME_MGMT_V','','');
xxeis.eis_rs_ins.rc( 'Time Sensitive Mgmt - WC',401,'VENDOR_NUMBER','Supplier Number','Vendor Number','','','default','','15','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_TIME_MGMT_V','','');
xxeis.eis_rs_ins.rc( 'Time Sensitive Mgmt - WC',401,'PRIMARY_BIN_LOC','Bin1','Primary Bin Loc','','','default','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_TIME_MGMT_V','','');
--Inserting Report Parameters - Time Sensitive Mgmt - WC
xxeis.eis_rs_ins.rp( 'Time Sensitive Mgmt - WC',401,'Organization','Inventory Organization','ORGANIZATION_CODE','IN','XXWC Inventory Org List','','VARCHAR2','Y','Y','1','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Time Sensitive Mgmt - WC',401,'Item','Inventory Item','ITEM','IN','EIS_INV_ITEM_LOV','','VARCHAR2','N','Y','2','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Time Sensitive Mgmt - WC',401,'SubInventory','Subinventory','SUBINVENTORY_CODE','IN','EIS_INV_SUBINVENTORY_LOV','','VARCHAR2','N','Y','3','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Time Sensitive Mgmt - WC',401,'Region','Region','REGION','IN','Region Lov','','VARCHAR2','N','Y','4','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Time Sensitive Mgmt - WC',401,'Location List','Location List','','IN','XXWC Org List','','VARCHAR2','N','Y','6','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Time Sensitive Mgmt - WC',401,'Supplier List','Supplier List','','IN','XXWC Supplier List','','VARCHAR2','N','Y','8','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Time Sensitive Mgmt - WC',401,'Item List','Item List','','IN','XXWC Item List','','VARCHAR2','N','Y','7','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Time Sensitive Mgmt - WC',401,'Supplier','Supplier','VENDOR_NAME','IN','XXWC Vendors','','VARCHAR2','N','Y','5','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
--Inserting Report Conditions - Time Sensitive Mgmt - WC
xxeis.eis_rs_ins.rcn( 'Time Sensitive Mgmt - WC',401,'PROJECT_NAME','IN',':Project Name','','','N','7','N','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Time Sensitive Mgmt - WC',401,'REGION','IN',':Region','','','Y','5','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Time Sensitive Mgmt - WC',401,'ITEM','IN',':Item','','','Y','3','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Time Sensitive Mgmt - WC',401,'SUBINVENTORY_CODE','IN',':SubInventory','','','Y','4','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Time Sensitive Mgmt - WC',401,'VENDOR_NAME','IN',':Supplier','','','Y','6','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Time Sensitive Mgmt - WC',401,'ORGANIZATION_CODE','IN',':Organization','','','Y','2','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Time Sensitive Mgmt - WC',401,'','','','','AND (:Location List is null or EXISTS (SELECT 1  FROM 
                         APPS.XXWC_PARAM_LIST
                         WHERE LIST_NAME= :Location List
                            AND LIST_TYPE =''Org''
                         AND DBMS_LOB.INSTR(LIST_VALUES,
                         EIOQV.organization_code, 1, 1)<>0) 
     )
AND (:Item List is null or EXISTS (SELECT 1  FROM 
                         APPS.XXWC_PARAM_LIST
                         WHERE LIST_NAME= :Item List
                            AND LIST_TYPE =''Item''
                         AND DBMS_LOB.INSTR(LIST_VALUES,
                          EIOQV.ITEM, 1, 1)<>0) 
     )
AND (:Supplier List is null or EXISTS (SELECT 1  FROM 
                         APPS.XXWC_PARAM_LIST
                         WHERE LIST_NAME= :Supplier List
                            AND LIST_TYPE =''Supplier''
                         AND DBMS_LOB.INSTR(LIST_VALUES,
                          EIOQV.vendor_name, 1, 1)<>0) 
     )','Y','0','','XXEIS_RS_ADMIN');
--Inserting Report Sorts - Time Sensitive Mgmt - WC
--Inserting Report Triggers - Time Sensitive Mgmt - WC
--Inserting Report Templates - Time Sensitive Mgmt - WC
xxeis.eis_rs_ins.R_Tem( 'Time Sensitive Mgmt - WC','Time sensitive Mgmt � WC','Seeded template for Time sensitive Mgmt � WC','','','','','','','','','','','Time sensitive Mgmt � WC.rtf','XXEIS_RS_ADMIN');
--Inserting Report Portals - Time Sensitive Mgmt - WC
--Inserting Report Dashboards - Time Sensitive Mgmt - WC
xxeis.eis_rs_ins.r_dash( 'Time Sensitive Mgmt - WC','Dynamic 702','Dynamic 702','pie','large','Unit Number','Unit Number','Subinventory Code','Subinventory Code','Count','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.r_dash( 'Time Sensitive Mgmt - WC','Dynamic 701','Dynamic 701','vertical percent bar','large','Cost Group','Cost Group','Cost Group','Cost Group','Count','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.r_dash( 'Time Sensitive Mgmt - WC','Dynamic 703','Dynamic 703','horizontal stacked bar','large','Unit Number','Unit Number','Subinventory Code','Subinventory Code','Count','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.r_dash( 'Time Sensitive Mgmt - WC','Dynamic 704','Dynamic 704','vertical stacked bar','large','Unit Number','Unit Number','Subinventory Code','Subinventory Code','Count','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.r_dash( 'Time Sensitive Mgmt - WC','Dynamic 705','Dynamic 705','vertical stacked bar','large','Subinventory Code','Subinventory Code','On Hand','On Hand','Count','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.r_dash( 'Time Sensitive Mgmt - WC','Dynamic 706','Dynamic 706','vertical stacked bar','large','Subinventory Code','Subinventory Code','On Hand','On Hand','Sum','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.r_dash( 'Time Sensitive Mgmt - WC','Dynamic 707','Dynamic 707','vertical stacked bar','large','Subinventory Code','Subinventory Code','Item','Item','Count','XXEIS_RS_ADMIN');
--Inserting Report Security - Time Sensitive Mgmt - WC
xxeis.eis_rs_ins.rsec( 'Time Sensitive Mgmt - WC','401','','50884',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Time Sensitive Mgmt - WC','401','','50855',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Time Sensitive Mgmt - WC','401','','50981',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Time Sensitive Mgmt - WC','401','','50883',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Time Sensitive Mgmt - WC','401','','50882',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Time Sensitive Mgmt - WC','401','','51004',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Time Sensitive Mgmt - WC','401','','50867',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Time Sensitive Mgmt - WC','401','','50849',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Time Sensitive Mgmt - WC','401','','50868',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Time Sensitive Mgmt - WC','401','','50862',401,'XXEIS_RS_ADMIN','','');
--Inserting Report Pivots - Time Sensitive Mgmt - WC
xxeis.eis_rs_ins.rpivot( 'Time Sensitive Mgmt - WC',401,'Pivot','1','1,0|1,1,0','1,1,1,0|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rs_ins.rpivot_dtls( 'Time Sensitive Mgmt - WC',401,'Pivot','ITEM','ROW_FIELD','','','4','0','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Time Sensitive Mgmt - WC',401,'Pivot','ON_HAND','DATA_FIELD','SUM','','1','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Time Sensitive Mgmt - WC',401,'Pivot','SUBINVENTORY_CODE','PAGE_FIELD','','','1','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Time Sensitive Mgmt - WC',401,'Pivot','LOT_EXPIRY_DATE','ROW_FIELD','','','2','0','');
xxeis.eis_rs_ins.rpivot_dtls( 'Time Sensitive Mgmt - WC',401,'Pivot','OLDEST_BORN_DATE','ROW_FIELD','','','3','0','');
xxeis.eis_rs_ins.rpivot_dtls( 'Time Sensitive Mgmt - WC',401,'Pivot','SHELF_LIFE_DAYS','ROW_FIELD','','','6','0','');
xxeis.eis_rs_ins.rpivot_dtls( 'Time Sensitive Mgmt - WC',401,'Pivot','ORGANIZATION_CODE','ROW_FIELD','','','1','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'Time Sensitive Mgmt - WC',401,'Pivot','LOT_NUMBER','ROW_FIELD','','','5','0','');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
END;
/
set scan on define on
