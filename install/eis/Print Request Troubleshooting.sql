--Report Name            : Print Request Troubleshooting
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View XXWC_PRINTERS_V
set scan off define off
prompt Creating View XXEIS.XXWC_PRINTERS_V
Create or replace View XXEIS.XXWC_PRINTERS_V
(NTID,LAST_FIRST_NAME,PARAMETERS,START_DATE,END_DATE,CONCURRENT_PROGRAM_NAME,PRINTER,NUMBER_OF_COPIES,PRINTER_QUEUE,FILE_CREATION_DATE,FILE_NAME,FILE_SIZE,FILE_TYPE,OUTPUT_ID,REQUEST_ID) AS 
select fu.user_name NTID,
fu.description lAST_FIRST_NAME,
fcr.ARGUMENT_TEXT PARAMETERS,
fcr.ACTUAL_START_DATE START_DATE,
fcr.ACTUAL_COMPLETION_DATE END_DATE,
fcpt.user_concurrent_program_name CONCURRENT_PROGRAM_NAME,
fcr.printer,
fcr.NUMBER_OF_COPIES,
fcr.logfile_node_name PRINTER_QUEUE,
fcro.FILE_CREATION_DATE,
fcro.FILE_NAME,
fcro.FILE_SIZE,
fcro.FILE_TYPE,
fcro.OUTPUT_ID,
fcr.request_id
from apps.fnd_concurrent_requests fcr,
apps.fnd_user fu,
apps.fnd_concurrent_programs_tl fcpt,
apps.fnd_conc_req_outputs fcro
where fu.user_id = fcr.requested_by
and fcr.concurrent_program_id = fcpt.CONCURRENT_PROGRAM_ID
--and trunc(fcr.ACTUAL_START_DATE) = trunc(sysdate)
and fcro.concurrent_request_id = fcr.request_id
and fcpt.user_concurrent_program_name  LIKE '%XXWC%'


--and fcpt.USER_CONCURRENT_PROGRAM_NAME = 'XXWC OM Sales Receipt Report'
order by requested_start_date desc
/
set scan on define on
prompt Creating View Data for Print Request Troubleshooting
set scan off define off
DECLARE
BEGIN 
--Inserting View XXWC_PRINTERS_V
xxeis.eis_rs_ins.v( 'XXWC_PRINTERS_V',85000,'','','','','XXEIS_RS_ADMIN','XXEIS','Xxwc Printers V','XPV','','');
--Delete View Columns for XXWC_PRINTERS_V
xxeis.eis_rs_utility.delete_view_rows('XXWC_PRINTERS_V',85000,FALSE);
--Inserting View Columns for XXWC_PRINTERS_V
xxeis.eis_rs_ins.vc( 'XXWC_PRINTERS_V','OUTPUT_ID',85000,'Output Id','OUTPUT_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Output Id','','','');
xxeis.eis_rs_ins.vc( 'XXWC_PRINTERS_V','FILE_TYPE',85000,'File Type','FILE_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','File Type','','','');
xxeis.eis_rs_ins.vc( 'XXWC_PRINTERS_V','FILE_SIZE',85000,'File Size','FILE_SIZE','','','','XXEIS_RS_ADMIN','NUMBER','','','File Size','','','');
xxeis.eis_rs_ins.vc( 'XXWC_PRINTERS_V','FILE_NAME',85000,'File Name','FILE_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','File Name','','','');
xxeis.eis_rs_ins.vc( 'XXWC_PRINTERS_V','FILE_CREATION_DATE',85000,'File Creation Date','FILE_CREATION_DATE','','','','XXEIS_RS_ADMIN','DATE','','','File Creation Date','','','');
xxeis.eis_rs_ins.vc( 'XXWC_PRINTERS_V','PRINTER_QUEUE',85000,'Printer Queue','PRINTER_QUEUE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Printer Queue','','','');
xxeis.eis_rs_ins.vc( 'XXWC_PRINTERS_V','NUMBER_OF_COPIES',85000,'Number Of Copies','NUMBER_OF_COPIES','','','','XXEIS_RS_ADMIN','NUMBER','','','Number Of Copies','','','');
xxeis.eis_rs_ins.vc( 'XXWC_PRINTERS_V','PRINTER',85000,'Printer','PRINTER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Printer','','','');
xxeis.eis_rs_ins.vc( 'XXWC_PRINTERS_V','CONCURRENT_PROGRAM_NAME',85000,'Concurrent Program Name','CONCURRENT_PROGRAM_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Concurrent Program Name','','','');
xxeis.eis_rs_ins.vc( 'XXWC_PRINTERS_V','END_DATE',85000,'End Date','END_DATE','','','','XXEIS_RS_ADMIN','DATE','','','End Date','','','');
xxeis.eis_rs_ins.vc( 'XXWC_PRINTERS_V','START_DATE',85000,'Start Date','START_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Start Date','','','');
xxeis.eis_rs_ins.vc( 'XXWC_PRINTERS_V','PARAMETERS',85000,'Parameters','PARAMETERS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Parameters','','','');
xxeis.eis_rs_ins.vc( 'XXWC_PRINTERS_V','LAST_FIRST_NAME',85000,'Last First Name','LAST_FIRST_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Last First Name','','','');
xxeis.eis_rs_ins.vc( 'XXWC_PRINTERS_V','NTID',85000,'Ntid','NTID','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Ntid','','','');
xxeis.eis_rs_ins.vc( 'XXWC_PRINTERS_V','REQUEST_ID',85000,'Request Id','REQUEST_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Request Id','','','');
--Inserting View Components for XXWC_PRINTERS_V
--Inserting View Component Joins for XXWC_PRINTERS_V
END;
/
set scan on define on
prompt Creating Report Data for Print Request Troubleshooting
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Print Request Troubleshooting
xxeis.eis_rs_utility.delete_report_rows( 'Print Request Troubleshooting' );
--Inserting Report - Print Request Troubleshooting
xxeis.eis_rs_ins.r( 85000,'Print Request Troubleshooting','','This report gives the information on exactly where the printing went. It should be able to filter by name, program name, etc','','','','XXEIS_RS_ADMIN','XXWC_PRINTERS_V','Y','','','XXEIS_RS_ADMIN','','N','WC Audit Reports','PDF,','CSV,HTML,Html Summary,XML,EXCEL,','N');
--Inserting Report Columns - Print Request Troubleshooting
xxeis.eis_rs_ins.rc( 'Print Request Troubleshooting',85000,'CONCURRENT_PROGRAM_NAME','Concurrent Program Name','Concurrent Program Name','','','default','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_PRINTERS_V','','');
xxeis.eis_rs_ins.rc( 'Print Request Troubleshooting',85000,'END_DATE','End Date','End Date','','','default','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_PRINTERS_V','','');
xxeis.eis_rs_ins.rc( 'Print Request Troubleshooting',85000,'FILE_CREATION_DATE','File Creation Date','File Creation Date','','','default','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_PRINTERS_V','','');
xxeis.eis_rs_ins.rc( 'Print Request Troubleshooting',85000,'FILE_NAME','File Name','File Name','','','default','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_PRINTERS_V','','');
xxeis.eis_rs_ins.rc( 'Print Request Troubleshooting',85000,'FILE_SIZE','File Size','File Size','','~~~','default','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_PRINTERS_V','','');
xxeis.eis_rs_ins.rc( 'Print Request Troubleshooting',85000,'FILE_TYPE','File Type','File Type','','','default','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_PRINTERS_V','','');
xxeis.eis_rs_ins.rc( 'Print Request Troubleshooting',85000,'LAST_FIRST_NAME','Last First Name','Last First Name','','','default','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_PRINTERS_V','','');
xxeis.eis_rs_ins.rc( 'Print Request Troubleshooting',85000,'NTID','Ntid','Ntid','','','default','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_PRINTERS_V','','');
xxeis.eis_rs_ins.rc( 'Print Request Troubleshooting',85000,'NUMBER_OF_COPIES','Number Of Copies','Number Of Copies','','~~~','default','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_PRINTERS_V','','');
xxeis.eis_rs_ins.rc( 'Print Request Troubleshooting',85000,'OUTPUT_ID','Output Id','Output Id','','~~~','default','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_PRINTERS_V','','');
xxeis.eis_rs_ins.rc( 'Print Request Troubleshooting',85000,'PARAMETERS','Parameters','Parameters','','','default','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_PRINTERS_V','','');
xxeis.eis_rs_ins.rc( 'Print Request Troubleshooting',85000,'PRINTER','Printer','Printer','','','default','','12','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_PRINTERS_V','','');
xxeis.eis_rs_ins.rc( 'Print Request Troubleshooting',85000,'PRINTER_QUEUE','Printer Queue','Printer Queue','','','default','','13','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_PRINTERS_V','','');
xxeis.eis_rs_ins.rc( 'Print Request Troubleshooting',85000,'START_DATE','Start Date','Start Date','','','default','','14','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_PRINTERS_V','','');
xxeis.eis_rs_ins.rc( 'Print Request Troubleshooting',85000,'REQUEST_ID','Request Id','Request Id','','~~~','default','','15','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_PRINTERS_V','','');
--Inserting Report Parameters - Print Request Troubleshooting
xxeis.eis_rs_ins.rp( 'Print Request Troubleshooting',85000,'End Date','End Date','END_DATE','<=','','','DATE','N','Y','6','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Print Request Troubleshooting',85000,'Start Date','Start Date','START_DATE','>=','','','DATE','N','Y','5','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Print Request Troubleshooting',85000,'Concurrent Program Name','Concurrent Program Name','CONCURRENT_PROGRAM_NAME','LIKE','','','VARCHAR2','N','Y','2','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Print Request Troubleshooting',85000,'Request Id','Request Id','REQUEST_ID','IN','','','NUMERIC','N','Y','3','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Print Request Troubleshooting',85000,'Printer','Printer','PRINTER','LIKE','','','VARCHAR2','N','Y','1','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Print Request Troubleshooting',85000,'Printer Queue','Printer Queue','PRINTER_QUEUE','IN','','','VARCHAR2','N','Y','4','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','','','','');
--Inserting Report Conditions - Print Request Troubleshooting
xxeis.eis_rs_ins.rcn( 'Print Request Troubleshooting',85000,'CONCURRENT_PROGRAM_NAME','LIKE',':Concurrent Program Name','','','Y','2','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Print Request Troubleshooting',85000,'END_DATE','<=',':End Date','','','Y','6','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Print Request Troubleshooting',85000,'START_DATE','>=',':Start Date','','','Y','5','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Print Request Troubleshooting',85000,'REQUEST_ID','IN',':Request Id','','','Y','3','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Print Request Troubleshooting',85000,'PRINTER','LIKE',':Printer','','','Y','1','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Print Request Troubleshooting',85000,'PRINTER_QUEUE','IN',':Printer Queue','','','Y','4','Y','XXEIS_RS_ADMIN');
--Inserting Report Sorts - Print Request Troubleshooting
--Inserting Report Triggers - Print Request Troubleshooting
--Inserting Report Templates - Print Request Troubleshooting
--Inserting Report Portals - Print Request Troubleshooting
--Inserting Report Dashboards - Print Request Troubleshooting
--Inserting Report Security - Print Request Troubleshooting
xxeis.eis_rs_ins.rsec( 'Print Request Troubleshooting','20005','','50900',85000,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Print Request Troubleshooting','20005','','50861',85000,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Print Request Troubleshooting','20005','','50843',85000,'XXEIS_RS_ADMIN','','');
--Inserting Report Pivots - Print Request Troubleshooting
END;
/
set scan on define on
