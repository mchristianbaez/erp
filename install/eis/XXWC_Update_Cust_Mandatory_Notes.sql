--Report Name            : XXWC Update Cust Mandatory Notes
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View XXWC_AR_CUSTNOTES_ADI_VW
--Connecting to schema APPS
accept SID prompt "Please enter database ( connect string )  : "
accept apps_password prompt "Please enter APPS password : " hide
connect APPS/@
set scan off define off
prompt Creating View APPS.XXWC_AR_CUSTNOTES_ADI_VW
Create or replace View APPS.XXWC_AR_CUSTNOTES_ADI_VW
(CUST_ACCOUNT_ID,ACCOUNT_NUMBER,ACCOUNT_NAME,CUSTOMER_ACCOUNT_STATUS,TOTAL_AR,COLLECTOR_NAME,NOTE1,NOTE2,NOTE3,NOTE4,NOTE5,ACCOUNT_CUSTOMER_SOURCE,ACCOUNT_CREDIT_HOLD,ACCOUNT_PAYMENT_TERMS,ACCOUNT_PROFILE_CLASS_NAME,ACCOUNT_CREDIT_LIMIT,PO_REQUIRED) AS 
SELECT hca.cust_account_id
     , hca.account_number
     , hca.account_name
     , (SELECT flv.meaning
          FROM fnd_lookup_values        flv
     WHERE 1 = 1
       AND flv.lookup_type = 'ACCOUNT_STATUS'
       AND flv.lookup_code = hcp.account_status
       )   customer_account_status
     , (SELECT SUM(AMOUNT_DUE_REMAINING)
          FROM ar_payment_schedules_all apsa
         WHERE 1 = 1
           AND hca.cust_account_id = apsa.customer_id
           AND apsa.org_id = 162) Total_AR
     , ac.name collector_name
     , hca.ATTRIBUTE17 Note1
     , hca.ATTRIBUTE18 Note2
     , hca.ATTRIBUTE19 Note3
     , hca.ATTRIBUTE20 Note4
     , hca.ATTRIBUTE16 Note5
     , hca.ATTRIBUTE4  account_customer_source
     , hcp.credit_hold Account_Credit_Hold
     , hcp.standard_terms Account_Payment_Terms
     , hcpc.name Account_Profile_Class_Name
     , hcpa.overall_credit_limit Account_Credit_limit
     , hcas.attribute3 PO_Required
  FROM hz_cust_accounts_all     hca
     , hz_cust_acct_sites_all   hcas
     , hz_customer_profiles     hcp
     , hz_cust_profile_classes  hcpc
     , hz_cust_profile_amts     hcpa
     , ar_collectors            ac
 WHERE 1 = 1 -- and hca.account_number = '32231000'
   AND hca.cust_account_id = hcp.cust_account_id
   AND hcp.site_use_id IS NULL
   AND hca.cust_account_id = hcas.cust_account_id
   AND nvl(hcas.bill_to_flag,'N') = 'P'
   AND hcp.collector_id = ac.collector_id
   AND hcp.profile_class_id =  hcpc.profile_class_id
   AND hcp.cust_account_profile_id = hcpa.cust_account_profile_id
   AND NVL(hca.attribute_category, 'No') = 'Yes'
/
set scan on define on
prompt Creating View Data for XXWC Update Cust Mandatory Notes
set scan off define off
DECLARE
BEGIN 
--Inserting View XXWC_AR_CUSTNOTES_ADI_VW
xxeis.eis_rs_ins.v( 'XXWC_AR_CUSTNOTES_ADI_VW',222,'','','','','TD002849','APPS','Xxwc Ar Custnotes Adi Vw','XACAV','','');
--Delete View Columns for XXWC_AR_CUSTNOTES_ADI_VW
xxeis.eis_rs_utility.delete_view_rows('XXWC_AR_CUSTNOTES_ADI_VW',222,FALSE);
--Inserting View Columns for XXWC_AR_CUSTNOTES_ADI_VW
xxeis.eis_rs_ins.vc( 'XXWC_AR_CUSTNOTES_ADI_VW','PO_REQUIRED',222,'Po Required','PO_REQUIRED','','','','TD002849','VARCHAR2','','','Po Required','','','');
xxeis.eis_rs_ins.vc( 'XXWC_AR_CUSTNOTES_ADI_VW','ACCOUNT_CREDIT_LIMIT',222,'Account Credit Limit','ACCOUNT_CREDIT_LIMIT','','','','TD002849','NUMBER','','','Account Credit Limit','','','');
xxeis.eis_rs_ins.vc( 'XXWC_AR_CUSTNOTES_ADI_VW','ACCOUNT_PROFILE_CLASS_NAME',222,'Account Profile Class Name','ACCOUNT_PROFILE_CLASS_NAME','','','','TD002849','VARCHAR2','','','Account Profile Class Name','','','');
xxeis.eis_rs_ins.vc( 'XXWC_AR_CUSTNOTES_ADI_VW','ACCOUNT_PAYMENT_TERMS',222,'Account Payment Terms','ACCOUNT_PAYMENT_TERMS','','','','TD002849','NUMBER','','','Account Payment Terms','','','');
xxeis.eis_rs_ins.vc( 'XXWC_AR_CUSTNOTES_ADI_VW','ACCOUNT_CREDIT_HOLD',222,'Account Credit Hold','ACCOUNT_CREDIT_HOLD','','','','TD002849','VARCHAR2','','','Account Credit Hold','','','');
xxeis.eis_rs_ins.vc( 'XXWC_AR_CUSTNOTES_ADI_VW','ACCOUNT_CUSTOMER_SOURCE',222,'Account Customer Source','ACCOUNT_CUSTOMER_SOURCE','','','','TD002849','VARCHAR2','','','Account Customer Source','','','');
xxeis.eis_rs_ins.vc( 'XXWC_AR_CUSTNOTES_ADI_VW','NOTE5',222,'Note5','NOTE5','','','','TD002849','VARCHAR2','','','Note5','','','');
xxeis.eis_rs_ins.vc( 'XXWC_AR_CUSTNOTES_ADI_VW','NOTE4',222,'Note4','NOTE4','','','','TD002849','VARCHAR2','','','Note4','','','');
xxeis.eis_rs_ins.vc( 'XXWC_AR_CUSTNOTES_ADI_VW','NOTE3',222,'Note3','NOTE3','','','','TD002849','VARCHAR2','','','Note3','','','');
xxeis.eis_rs_ins.vc( 'XXWC_AR_CUSTNOTES_ADI_VW','NOTE2',222,'Note2','NOTE2','','','','TD002849','VARCHAR2','','','Note2','','','');
xxeis.eis_rs_ins.vc( 'XXWC_AR_CUSTNOTES_ADI_VW','NOTE1',222,'Note1','NOTE1','','','','TD002849','VARCHAR2','','','Note1','','','');
xxeis.eis_rs_ins.vc( 'XXWC_AR_CUSTNOTES_ADI_VW','COLLECTOR_NAME',222,'Collector Name','COLLECTOR_NAME','','','','TD002849','VARCHAR2','','','Collector Name','','','');
xxeis.eis_rs_ins.vc( 'XXWC_AR_CUSTNOTES_ADI_VW','TOTAL_AR',222,'Total Ar','TOTAL_AR','','','','TD002849','NUMBER','','','Total Ar','','','');
xxeis.eis_rs_ins.vc( 'XXWC_AR_CUSTNOTES_ADI_VW','CUSTOMER_ACCOUNT_STATUS',222,'Customer Account Status','CUSTOMER_ACCOUNT_STATUS','','','','TD002849','VARCHAR2','','','Customer Account Status','','','');
xxeis.eis_rs_ins.vc( 'XXWC_AR_CUSTNOTES_ADI_VW','ACCOUNT_NAME',222,'Account Name','ACCOUNT_NAME','','','','TD002849','VARCHAR2','','','Account Name','','','');
xxeis.eis_rs_ins.vc( 'XXWC_AR_CUSTNOTES_ADI_VW','ACCOUNT_NUMBER',222,'Account Number','ACCOUNT_NUMBER','','','','TD002849','VARCHAR2','','','Account Number','','','');
xxeis.eis_rs_ins.vc( 'XXWC_AR_CUSTNOTES_ADI_VW','CUST_ACCOUNT_ID',222,'Cust Account Id','CUST_ACCOUNT_ID','','','','TD002849','NUMBER','','','Cust Account Id','','','');
--Inserting View Components for XXWC_AR_CUSTNOTES_ADI_VW
--Inserting View Component Joins for XXWC_AR_CUSTNOTES_ADI_VW
END;
/
set scan on define on
prompt Creating Report LOV Data for XXWC Update Cust Mandatory Notes
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - XXWC Update Cust Mandatory Notes
xxeis.eis_rs_ins.lov( 222,'select account_number from hz_cust_accounts','null','Customer Number','Displays List of Values for Customer Number','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 222,'select name from ar_collectors','null','Collector','Displays list of values for Collector','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 222,'select distinct name from hz_cust_profile_classes','null','PROFILE CLASS','This LOV lists all the profile classes of the customers','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 222,'select account_name,account_number from hz_cust_accounts','','AR Customer Name LOV','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 222,'select  customer_status.meaning Status
from  fnd_lookup_values_vl customer_status
where customer_status.lookup_type= ''ACCOUNT_STATUS''
 and customer_status.view_application_id=222','','XXWC Customer Account Status','','ANONYMOUS',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for XXWC Update Cust Mandatory Notes
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - XXWC Update Cust Mandatory Notes
xxeis.eis_rs_utility.delete_report_rows( 'XXWC Update Cust Mandatory Notes' );
--Inserting Report - XXWC Update Cust Mandatory Notes
xxeis.eis_rs_ins.r( 222,'XXWC Update Cust Mandatory Notes','','XXWC Update Cust Notes','','','','TD002849','XXWC_AR_CUSTNOTES_ADI_VW','Y','','','TD002849','','N','Customers','','EXCEL,','N');
--Inserting Report Columns - XXWC Update Cust Mandatory Notes
xxeis.eis_rs_ins.rc( 'XXWC Update Cust Mandatory Notes',222,'ACCOUNT_CREDIT_HOLD','Account Credit Hold','Account Credit Hold','','','','','13','N','','','','','','','','TD002849','N','N','','XXWC_AR_CUSTNOTES_ADI_VW','','');
xxeis.eis_rs_ins.rc( 'XXWC Update Cust Mandatory Notes',222,'ACCOUNT_CREDIT_LIMIT','Account Credit Limit','Account Credit Limit','','','','','16','N','','','','','','','','TD002849','N','N','','XXWC_AR_CUSTNOTES_ADI_VW','','');
xxeis.eis_rs_ins.rc( 'XXWC Update Cust Mandatory Notes',222,'ACCOUNT_CUSTOMER_SOURCE','Account Customer Source','Account Customer Source','','','','','12','N','','','','','','','','TD002849','N','N','','XXWC_AR_CUSTNOTES_ADI_VW','','');
xxeis.eis_rs_ins.rc( 'XXWC Update Cust Mandatory Notes',222,'ACCOUNT_NAME','Account Name','Account Name','','','','','3','N','','','','','','','','TD002849','N','N','','XXWC_AR_CUSTNOTES_ADI_VW','','');
xxeis.eis_rs_ins.rc( 'XXWC Update Cust Mandatory Notes',222,'ACCOUNT_NUMBER','Account Number','Account Number','','','','','2','N','','','','','','','','TD002849','N','N','','XXWC_AR_CUSTNOTES_ADI_VW','','');
xxeis.eis_rs_ins.rc( 'XXWC Update Cust Mandatory Notes',222,'ACCOUNT_PAYMENT_TERMS','Account Payment Terms','Account Payment Terms','','','','','14','N','','','','','','','','TD002849','N','N','','XXWC_AR_CUSTNOTES_ADI_VW','','');
xxeis.eis_rs_ins.rc( 'XXWC Update Cust Mandatory Notes',222,'ACCOUNT_PROFILE_CLASS_NAME','Account Profile Class Name','Account Profile Class Name','','','','','15','N','','','','','','','','TD002849','N','N','','XXWC_AR_CUSTNOTES_ADI_VW','','');
xxeis.eis_rs_ins.rc( 'XXWC Update Cust Mandatory Notes',222,'COLLECTOR_NAME','Collector Name','Collector Name','','','','','6','N','','','','','','','','TD002849','N','N','','XXWC_AR_CUSTNOTES_ADI_VW','','');
xxeis.eis_rs_ins.rc( 'XXWC Update Cust Mandatory Notes',222,'CUSTOMER_ACCOUNT_STATUS','Customer Account Status','Customer Account Status','','','','','4','N','','','','','','','','TD002849','N','N','','XXWC_AR_CUSTNOTES_ADI_VW','','');
xxeis.eis_rs_ins.rc( 'XXWC Update Cust Mandatory Notes',222,'CUST_ACCOUNT_ID','Cust Account Id','Cust Account Id','','','','','1','N','','','','','','','','TD002849','N','N','','XXWC_AR_CUSTNOTES_ADI_VW','','');
xxeis.eis_rs_ins.rc( 'XXWC Update Cust Mandatory Notes',222,'NOTE1','Note1','Note1','','','','','7','N','','','','','','','','TD002849','N','N','','XXWC_AR_CUSTNOTES_ADI_VW','','');
xxeis.eis_rs_ins.rc( 'XXWC Update Cust Mandatory Notes',222,'NOTE2','Note2','Note2','','','','','8','N','','','','','','','','TD002849','N','N','','XXWC_AR_CUSTNOTES_ADI_VW','','');
xxeis.eis_rs_ins.rc( 'XXWC Update Cust Mandatory Notes',222,'NOTE3','Note3','Note3','','','','','9','N','','','','','','','','TD002849','N','N','','XXWC_AR_CUSTNOTES_ADI_VW','','');
xxeis.eis_rs_ins.rc( 'XXWC Update Cust Mandatory Notes',222,'NOTE4','Note4','Note4','','','','','10','N','','','','','','','','TD002849','N','N','','XXWC_AR_CUSTNOTES_ADI_VW','','');
xxeis.eis_rs_ins.rc( 'XXWC Update Cust Mandatory Notes',222,'NOTE5','Note5','Note5','','','','','11','N','','','','','','','','TD002849','N','N','','XXWC_AR_CUSTNOTES_ADI_VW','','');
xxeis.eis_rs_ins.rc( 'XXWC Update Cust Mandatory Notes',222,'PO_REQUIRED','Po Required','Po Required','','','','','17','N','','','','','','','','TD002849','N','N','','XXWC_AR_CUSTNOTES_ADI_VW','','');
xxeis.eis_rs_ins.rc( 'XXWC Update Cust Mandatory Notes',222,'TOTAL_AR','Total Ar','Total Ar','','','','','5','N','','','','','','','','TD002849','N','N','','XXWC_AR_CUSTNOTES_ADI_VW','','');
--Inserting Report Parameters - XXWC Update Cust Mandatory Notes
xxeis.eis_rs_ins.rp( 'XXWC Update Cust Mandatory Notes',222,'Account Name','Account Name','ACCOUNT_NAME','IN','AR Customer Name LOV','','VARCHAR2','N','Y','1','','Y','CONSTANT','TD002849','Y','N','','','');
xxeis.eis_rs_ins.rp( 'XXWC Update Cust Mandatory Notes',222,'Account Number','Account Number','ACCOUNT_NUMBER','IN','Customer Number','','VARCHAR2','N','Y','2','','Y','CONSTANT','TD002849','Y','N','','','');
xxeis.eis_rs_ins.rp( 'XXWC Update Cust Mandatory Notes',222,'Account Profile Class Name','Account Profile Class Name','ACCOUNT_PROFILE_CLASS_NAME','IN','PROFILE CLASS','','VARCHAR2','N','Y','3','','Y','CONSTANT','TD002849','Y','N','','','');
xxeis.eis_rs_ins.rp( 'XXWC Update Cust Mandatory Notes',222,'Collector Name','Collector Name','COLLECTOR_NAME','IN','Collector','','VARCHAR2','N','Y','4','','Y','CONSTANT','TD002849','Y','N','','','');
xxeis.eis_rs_ins.rp( 'XXWC Update Cust Mandatory Notes',222,'Customer Account Status','Customer Account Status','CUSTOMER_ACCOUNT_STATUS','IN','XXWC Customer Account Status','','VARCHAR2','N','Y','5','','Y','CONSTANT','TD002849','Y','N','','','');
--Inserting Report Conditions - XXWC Update Cust Mandatory Notes
xxeis.eis_rs_ins.rcn( 'XXWC Update Cust Mandatory Notes',222,'ACCOUNT_NAME','IN',':Account Name','','','Y','1','Y','TD002849');
xxeis.eis_rs_ins.rcn( 'XXWC Update Cust Mandatory Notes',222,'ACCOUNT_NUMBER','IN',':Account Number','','','Y','2','Y','TD002849');
xxeis.eis_rs_ins.rcn( 'XXWC Update Cust Mandatory Notes',222,'ACCOUNT_PROFILE_CLASS_NAME','IN',':Account Profile Class Name','','','Y','3','Y','TD002849');
xxeis.eis_rs_ins.rcn( 'XXWC Update Cust Mandatory Notes',222,'COLLECTOR_NAME','IN',':Collector Name','','','Y','4','Y','TD002849');
xxeis.eis_rs_ins.rcn( 'XXWC Update Cust Mandatory Notes',222,'CUSTOMER_ACCOUNT_STATUS','IN',':Customer Account Status','','','Y','5','Y','TD002849');
--Inserting Report Sorts - XXWC Update Cust Mandatory Notes
--Inserting Report Triggers - XXWC Update Cust Mandatory Notes
--Inserting Report Templates - XXWC Update Cust Mandatory Notes
--Inserting Report Portals - XXWC Update Cust Mandatory Notes
--Inserting Report Dashboards - XXWC Update Cust Mandatory Notes
--Inserting Report Security - XXWC Update Cust Mandatory Notes
xxeis.eis_rs_ins.rsec( 'XXWC Update Cust Mandatory Notes','222','','',222,'TD002849','','105');
xxeis.eis_rs_ins.rsec( 'XXWC Update Cust Mandatory Notes','222','','50853',222,'TD002849','','');
--Inserting Report Pivots - XXWC Update Cust Mandatory Notes
END;
/
set scan on define on
