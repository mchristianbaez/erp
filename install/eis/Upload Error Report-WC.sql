--Report Name            : Upload Error Report-WC
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating View Data for Upload Error Report-WC
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_INV_WEBADI_UPLD_ERR_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_INV_WEBADI_UPLD_ERR_V',401,'','','','','SA059956','XXEIS','Eis Xxwc Inv Webadi Upld Err V','EXIWUEV','','');
--Delete View Columns for EIS_XXWC_INV_WEBADI_UPLD_ERR_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_INV_WEBADI_UPLD_ERR_V',401,FALSE);
--Inserting View Columns for EIS_XXWC_INV_WEBADI_UPLD_ERR_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_WEBADI_UPLD_ERR_V','ERROR_MESSAGE',401,'Error Message','ERROR_MESSAGE','','','','SA059956','VARCHAR2','','','Error Message','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_WEBADI_UPLD_ERR_V','ORGANIZATION_CODE',401,'Organization Code','ORGANIZATION_CODE','','','','SA059956','VARCHAR2','','','Organization Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_WEBADI_UPLD_ERR_V','ORGANIZATION_ID',401,'Organization Id','ORGANIZATION_ID','','','','SA059956','NUMBER','','','Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_WEBADI_UPLD_ERR_V','REQUEST_ID',401,'Request Id','REQUEST_ID','','','','SA059956','NUMBER','','','Request Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_WEBADI_UPLD_ERR_V','DESCRIPTION',401,'Description','DESCRIPTION','','','','SA059956','VARCHAR2','','','Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_WEBADI_UPLD_ERR_V','ITEM_NUMBER',401,'Item Number','ITEM_NUMBER','','','','SA059956','VARCHAR2','','','Item Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_WEBADI_UPLD_ERR_V','INVENTORY_ITEM_ID',401,'Inventory Item Id','INVENTORY_ITEM_ID','','','','SA059956','NUMBER','','','Inventory Item Id','','','');
--Inserting View Components for EIS_XXWC_INV_WEBADI_UPLD_ERR_V
--Inserting View Component Joins for EIS_XXWC_INV_WEBADI_UPLD_ERR_V
END;
/
set scan on define on
prompt Creating Report Data for Upload Error Report-WC
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Upload Error Report-WC
xxeis.eis_rs_utility.delete_report_rows( 'Upload Error Report-WC' );
--Inserting Report - Upload Error Report-WC
xxeis.eis_rs_ins.r( 401,'Upload Error Report-WC','','To provide a GL audit report between MST and Child Orgs for data governance improvement.','','','','SA059956','EIS_XXWC_INV_WEBADI_UPLD_ERR_V','Y','','','SA059956','','N','White Cap Reports','','CSV,EXCEL,','N');
--Inserting Report Columns - Upload Error Report-WC
xxeis.eis_rs_ins.rc( 'Upload Error Report-WC',401,'DESCRIPTION','Description','Description','','','default','','3','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_WEBADI_UPLD_ERR_V','','');
xxeis.eis_rs_ins.rc( 'Upload Error Report-WC',401,'ERROR_MESSAGE','Error Message','Error Message','','','default','','5','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_WEBADI_UPLD_ERR_V','','');
xxeis.eis_rs_ins.rc( 'Upload Error Report-WC',401,'ITEM_NUMBER','Item Number','Item Number','','','default','','2','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_WEBADI_UPLD_ERR_V','','');
xxeis.eis_rs_ins.rc( 'Upload Error Report-WC',401,'ORGANIZATION_CODE','Organization Code','Organization Code','','','default','','4','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_WEBADI_UPLD_ERR_V','','');
xxeis.eis_rs_ins.rc( 'Upload Error Report-WC',401,'REQUEST_ID','Request Id','Request Id','','~~~','default','','1','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_WEBADI_UPLD_ERR_V','','');
--Inserting Report Parameters - Upload Error Report-WC
xxeis.eis_rs_ins.rp( 'Upload Error Report-WC',401,'Request Id','Request Id','REQUEST_ID','IN','','','NUMERIC','N','Y','1','','Y','CONSTANT','SA059956','Y','N','','','');
--Inserting Report Conditions - Upload Error Report-WC
xxeis.eis_rs_ins.rcn( 'Upload Error Report-WC',401,'REQUEST_ID','IN',':Request Id','','','Y','1','Y','SA059956');
--Inserting Report Sorts - Upload Error Report-WC
xxeis.eis_rs_ins.rs( 'Upload Error Report-WC',401,'ITEM_NUMBER','ASC','SA059956','1','');
--Inserting Report Triggers - Upload Error Report-WC
--Inserting Report Templates - Upload Error Report-WC
--Inserting Report Portals - Upload Error Report-WC
--Inserting Report Dashboards - Upload Error Report-WC
--Inserting Report Security - Upload Error Report-WC
xxeis.eis_rs_ins.rsec( 'Upload Error Report-WC','401','','50990',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Upload Error Report-WC','','PN020488','',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Upload Error Report-WC','','SS084202','',401,'SA059956','','');
--Inserting Report Pivots - Upload Error Report-WC
END;
/
set scan on define on
