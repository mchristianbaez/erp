--Report Name            : Inventory Sales and Reorder Report - WC
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data EIS_XXWC_ISR_REPORT_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(201);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_ISR_REPORT_V
xxeis.eis_rsc_ins.v( 'EIS_XXWC_ISR_REPORT_V',201,'View used in Inventory Sales and Reorder Report','','','','ANONYMOUS','XXEIS','EIS_XXWC_ISR_REPORT_V','EXPIV','','','VIEW','US','','');
--Delete Object Columns for EIS_XXWC_ISR_REPORT_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_ISR_REPORT_V',201,FALSE);
--Inserting Object Columns for EIS_XXWC_ISR_REPORT_V
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','BPA',201,'Bpa','BPA','','','','ANONYMOUS','VARCHAR2','','','Bpa','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','AVER_COST',201,'Aver Cost','AVER_COST','','~T~D~2','','ANONYMOUS','NUMBER','','','Aver Cost','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','AMU',201,'Amu','AMU','','','','ANONYMOUS','VARCHAR2','','','Amu','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','PM',201,'Pm','PM','','','','ANONYMOUS','VARCHAR2','','','Pm','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','STK_FLAG',201,'Stk Flag','STK_FLAG','','','','ANONYMOUS','VARCHAR2','','','Stk Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','CL',201,'Cl','CL','','','','ANONYMOUS','VARCHAR2','','','Cl','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','UOM',201,'Uom','UOM','','','','ANONYMOUS','VARCHAR2','','','Uom','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','CAT',201,'Cat','CAT','','','','ANONYMOUS','VARCHAR2','','','Cat','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','DESCRIPTION',201,'Description','DESCRIPTION','','','','ANONYMOUS','VARCHAR2','','','Description','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','ST',201,'St','ST','','','','ANONYMOUS','VARCHAR2','','','St','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','SOURCE',201,'Source','SOURCE','','','','ANONYMOUS','VARCHAR2','','','Source','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','VENDOR_NAME',201,'Vendor Name','VENDOR_NAME','','','','ANONYMOUS','VARCHAR2','','','Vendor Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','VENDOR_NUM',201,'Vendor Num','VENDOR_NUM','','','','ANONYMOUS','VARCHAR2','','','Vendor Num','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','ITEM_NUMBER',201,'Item Number','ITEM_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Item Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','PRE',201,'Pre','PRE','','','','ANONYMOUS','VARCHAR2','','','Pre','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','ORG',201,'Org','ORG','','','','ANONYMOUS','VARCHAR2','','','Org','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','BUYER',201,'Buyer','BUYER','','','','ANONYMOUS','VARCHAR2','','','Buyer','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','TURNS',201,'Turns','TURNS','','','','ANONYMOUS','NUMBER','','','Turns','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','THIRTEEN_WK_AN_COGS',201,'Thirteen Wk An Cogs','THIRTEEN_WK_AN_COGS','','','','ANONYMOUS','NUMBER','','','Thirteen Wk An Cogs','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','THIRTEEN_WK_AVG_INV',201,'Thirteen Wk Avg Inv','THIRTEEN_WK_AVG_INV','','','','ANONYMOUS','NUMBER','','','Thirteen Wk Avg Inv','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','RES',201,'Res','RES','','','','ANONYMOUS','VARCHAR2','','','Res','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','FREEZE_DATE',201,'Freeze Date','FREEZE_DATE','','','','ANONYMOUS','DATE','','','Freeze Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','FI_FLAG',201,'Fi Flag','FI_FLAG','','','','ANONYMOUS','VARCHAR2','','','Fi Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','BIN_LOC',201,'Bin Loc','BIN_LOC','','','','ANONYMOUS','VARCHAR2','','','Bin Loc','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','AVAILABLEDOLLAR',201,'Availabledollar','AVAILABLEDOLLAR','','','','ANONYMOUS','NUMBER','','','Availabledollar','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','AVAILABLE',201,'Available','AVAILABLE','','','','ANONYMOUS','NUMBER','','','Available','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','QOH',201,'Qoh','QOH','','','','ANONYMOUS','NUMBER','','','Qoh','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','APR_SALES',201,'Apr Sales','APR_SALES','','','','ANONYMOUS','NUMBER','','','Apr Sales','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','AUG_SALES',201,'Aug Sales','AUG_SALES','','','','ANONYMOUS','NUMBER','','','Aug Sales','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','DEC_SALES',201,'Dec Sales','DEC_SALES','','','','ANONYMOUS','NUMBER','','','Dec Sales','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','FEB_SALES',201,'Feb Sales','FEB_SALES','','','','ANONYMOUS','NUMBER','','','Feb Sales','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','HIT4_SALES',201,'Hit4 Sales','HIT4_SALES','','','','ANONYMOUS','NUMBER','','','Hit4 Sales','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','HIT6_SALES',201,'Hit6 Sales','HIT6_SALES','','','','ANONYMOUS','NUMBER','','','Hit6 Sales','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','JAN_SALES',201,'Jan Sales','JAN_SALES','','','','ANONYMOUS','NUMBER','','','Jan Sales','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','JUL_SALES',201,'Jul Sales','JUL_SALES','','','','ANONYMOUS','NUMBER','','','Jul Sales','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','JUNE_SALES',201,'June Sales','JUNE_SALES','','','','ANONYMOUS','NUMBER','','','June Sales','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','MAR_SALES',201,'Mar Sales','MAR_SALES','','','','ANONYMOUS','NUMBER','','','Mar Sales','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','MAY_SALES',201,'May Sales','MAY_SALES','','','','ANONYMOUS','NUMBER','','','May Sales','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','NOV_SALES',201,'Nov Sales','NOV_SALES','','','','ANONYMOUS','NUMBER','','','Nov Sales','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','OCT_SALES',201,'Oct Sales','OCT_SALES','','','','ANONYMOUS','NUMBER','','','Oct Sales','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','SEP_SALES',201,'Sep Sales','SEP_SALES','','','','ANONYMOUS','NUMBER','','','Sep Sales','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','ONE_SALES',201,'One Sales','ONE_SALES','','','','ANONYMOUS','NUMBER','','','One Sales','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','SIX_SALES',201,'Six Sales','SIX_SALES','','','','ANONYMOUS','NUMBER','','','Six Sales','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','TWELVE_SALES',201,'Twelve Sales','TWELVE_SALES','','','','ANONYMOUS','NUMBER','','','Twelve Sales','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','DISTRICT',201,'District','DISTRICT','','','','ANONYMOUS','VARCHAR2','','','District','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','REGION',201,'Region','REGION','','','','ANONYMOUS','VARCHAR2','','','Region','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','MAXN',201,'Maxn','MAXN','','','','ANONYMOUS','VARCHAR2','','','Maxn','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','MINN',201,'Minn','MINN','','','','ANONYMOUS','VARCHAR2','','','Minn','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','TS',201,'Ts','TS','','','','ANONYMOUS','VARCHAR2','','','Ts','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','BPA_COST',201,'Bpa Cost','BPA_COST','','~T~D~2','','ANONYMOUS','NUMBER','','','Bpa Cost','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','ON_ORD',201,'On Ord','ON_ORD','','','','ANONYMOUS','NUMBER','','','On Ord','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','FML',201,'Fml','FML','','','','ANONYMOUS','NUMBER','','','Fml','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','OPEN_REQ',201,'Open Req','OPEN_REQ','','','','ANONYMOUS','NUMBER','','','Open Req','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','WT',201,'Wt','WT','','','','ANONYMOUS','NUMBER','','','Wt','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','SO',201,'So','SO','','','','ANONYMOUS','NUMBER','','','So','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','SOURCING_RULE',201,'Sourcing Rule','SOURCING_RULE','','','','ANONYMOUS','VARCHAR2','','','Sourcing Rule','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','SS',201,'Ss','SS','','','','ANONYMOUS','NUMBER','','','Ss','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','CLT',201,'Clt','CLT','','','','ANONYMOUS','NUMBER','','','Clt','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','AVAIL2',201,'Avail2','AVAIL2','','','','ANONYMOUS','NUMBER','','','Avail2','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','INT_REQ',201,'Int Req','INT_REQ','','','','ANONYMOUS','NUMBER','','','Int Req','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','CORE',201,'Core','CORE','','','','ANONYMOUS','VARCHAR2','','','Core','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','MAKE_BUY',201,'Make Buy','MAKE_BUY','','','','ANONYMOUS','VARCHAR2','','','Make Buy','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','MFG_PART_NUMBER',201,'Mfg Part Number','MFG_PART_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Mfg Part Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','MST_VENDOR',201,'Mst Vendor','MST_VENDOR','','','','ANONYMOUS','VARCHAR2','','','Mst Vendor','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','ONHAND_GT_270',201,'Onhand Gt 270','ONHAND_GT_270','','','','ANONYMOUS','NUMBER','','','Onhand Gt 270','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','ORG_ITEM_STATUS',201,'Org Item Status','ORG_ITEM_STATUS','','','','ANONYMOUS','VARCHAR2','','','Org Item Status','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','ORG_USER_ITEM_TYPE',201,'Org User Item Type','ORG_USER_ITEM_TYPE','','','','ANONYMOUS','VARCHAR2','','','Org User Item Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','TIER',201,'Tier','TIER','','','','ANONYMOUS','VARCHAR2','','','Tier','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','CAT_SBA_OWNER',201,'Cat Sba Owner','CAT_SBA_OWNER','','','','ANONYMOUS','VARCHAR2','','','Cat Sba Owner','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','MST_ITEM_STATUS',201,'Mst Item Status','MST_ITEM_STATUS','','','','ANONYMOUS','VARCHAR2','','','Mst Item Status','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','MST_USER_ITEM_TYPE',201,'Mst User Item Type','MST_USER_ITEM_TYPE','','','','ANONYMOUS','VARCHAR2','','','Mst User Item Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','SUPERSEDE_ITEM',201,'Supersede Item','SUPERSEDE_ITEM','','','','ANONYMOUS','VARCHAR2','','','Supersede Item','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','DIR_REQ',201,'Dir Req','DIR_REQ','','','','ANONYMOUS','NUMBER','','','Dir Req','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','LAST_RECEIPT_DATE',201,'Last Receipt Date','LAST_RECEIPT_DATE','','','','ANONYMOUS','DATE','','','Last Receipt Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','FLIP_DATE',201,'Flip Date','FLIP_DATE','','','','ANONYMOUS','DATE','','','Flip Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','PLT',201,'Plt','PLT','','','','ANONYMOUS','NUMBER','','','Plt','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','PPLT',201,'Pplt','PPLT','','','','ANONYMOUS','NUMBER','','','Pplt','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','DEM',201,'Dem','DEM','','','','ANONYMOUS','NUMBER','','','Dem','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','LIST_PRICE',201,'List Price','LIST_PRICE','','~T~D~2','','ANONYMOUS','NUMBER','','','List Price','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','ITEM_COST',201,'Item Cost','ITEM_COST','','~T~D~2','','ANONYMOUS','NUMBER','','','Item Cost','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','MC',201,'Mc','MC','','','','ANONYMOUS','VARCHAR2','','','Mc','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','SITE_VENDOR_NUM',201,'Site Vendor Num','SITE_VENDOR_NUM','','','','ANONYMOUS','VARCHAR2','','','Site Vendor Num','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','VENDOR_SITE',201,'Vendor Site','VENDOR_SITE','','','','ANONYMOUS','VARCHAR2','','','Vendor Site','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','ORG_NAME',201,'Org Name','ORG_NAME','','','','ANONYMOUS','VARCHAR2','','','Org Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','MF_FLAG',201,'Mf Flag','MF_FLAG','','','','ANONYMOUS','VARCHAR2','','','Mf Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','SET_OF_BOOKS_ID',201,'Set Of Books Id','SET_OF_BOOKS_ID','','','','ANONYMOUS','NUMBER','','','Set Of Books Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','ORGANIZATION_ID',201,'Organization Id','ORGANIZATION_ID','','','','ANONYMOUS','NUMBER','','','Organization Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','INVENTORY_ITEM_ID',201,'Inventory Item Id','INVENTORY_ITEM_ID','','','','ANONYMOUS','NUMBER','','','Inventory Item Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','COMMON_OUTPUT_ID',201,'Common Output Id','COMMON_OUTPUT_ID','','','','ANONYMOUS','NUMBER','','','Common Output Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','PROCESS_ID',201,'Process Id','PROCESS_ID','','','','ANONYMOUS','NUMBER','','','Process Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','OPERATING_UNIT',201,'Operating Unit','OPERATING_UNIT','','','','ANONYMOUS','VARCHAR2','','','Operating Unit','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_ISR_REPORT_V','DEMAND',201,'Demand','DEMAND','','','','ANONYMOUS','NUMBER','','','Demand','','','','US');
--Inserting Object Components for EIS_XXWC_ISR_REPORT_V
--Inserting Object Component Joins for EIS_XXWC_ISR_REPORT_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 201');
END IF;
END;
/
prompt Creating Report LOV Data for Inventory Sales and Reorder Report - WC
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(201);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - Inventory Sales and Reorder Report - WC
xxeis.eis_rsc_ins.lov( 201,'select distinct segment1 bin_loc from Mtl_Item_Locations','','Bin Location Lov','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'    SELECT distinct Mcv.SEGMENT2 cat_class
        from  Mtl_Categories_Kfv Mcv,
            MTL_CATEGORY_SETS MCS,
            MTL_ITEM_CATEGORIES MIC,
            mtl_system_items_kfv msi
            Where   Mcs.Category_Set_Name      = ''Inventory Category''
          AND MCS.STRUCTURE_ID                 = MCV.STRUCTURE_ID
          AND MIC.INVENTORY_ITEM_ID            = MSI.INVENTORY_ITEM_ID
          And Mic.Organization_Id              = msi.Organization_Id
          AND MIC.CATEGORY_SET_ID              = MCS.CATEGORY_SET_ID
          And Mic.Category_Id                  = Mcv.Category_Id
order by Mcv.SEGMENT2','','Catclass Lov','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'select distinct ATTRIBUTE8 District from mtl_parameters','','District Lov','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'select distinct ATTRIBUTE9 Region from mtl_parameters','','Region Lov','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'','Exclude,Include,Tool Repair Only','Tool Repair','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'','  ,Vendor Number(%),3 Digit Prefix,Item number,Source,2 Digit Cat,4 Digit Cat Class,Default Buyer(%)','Report Criteria','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'','Active small,All items,All items on hand, Stock items only,Non stock only,Active large, Non-stock on hand,Stock items with 0/0 min/max','EIS PO XXWC ISR REPORT COND','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'','Include,Time Sensitive Only','EIS PO XXWC Time Sensitive','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'','Yes,No','EIS PO XXWC DC Mode','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'select list_name from XXWC_PARAM_LIST where list_type = ''Org'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Org List','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'select list_name from XXWC_PARAM_LIST where list_type = ''Item'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Item List','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'select list_name from XXWC_PARAM_LIST where list_type = ''Supplier'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Supplier List','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'select list_name from XXWC_PARAM_LIST where list_type = ''Source'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Source List','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'select list_name from XXWC_PARAM_LIST where list_type = ''Cat Class''  and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Cat Class List','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'SELECT DISTINCT segment1 Item
FROM mtl_system_items_b msi
WHERE NOT EXISTS
  (SELECT 1
  FROM mtl_parameters mp
  WHERE organization_code IN (''CAN'',''HDS'',''US1'',''CN1'')
  AND msi.organization_id  = mp.organization_id
  )
order by segment1 ASC','','XXWC Item','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'SELECT POV.SEGMENT1 VENDOR_NUMBER,
  POV.VENDOR_NAME
FROM APPS.PO_VENDORS POV
WHERE EXISTS
  (SELECT /*+ INDEX(MSA XXWC_MRP_SR_ASSIGNMENTS_N7)*/
	1
  FROM APPS.MRP_SR_SOURCE_ORG MSSO ,
    APPS.MRP_SR_RECEIPT_ORG MSRO ,
    APPS.MRP_SOURCING_RULES MSR ,
    APPS.MRP_SR_ASSIGNMENTS MSA ,
    APPS.MTL_SYSTEM_ITEMS_B MSI
  WHERE 1                   =1
  AND msro.sr_receipt_id    = msso.sr_receipt_id
  AND MSR.SOURCING_RULE_ID  = MSRO.SOURCING_RULE_ID
  AND MSSO.VENDOR_ID        = POV.VENDOR_ID
  AND MSA.SOURCING_RULE_ID  = MSRO.SOURCING_RULE_ID
  AND MSA.INVENTORY_ITEM_ID = MSI.INVENTORY_ITEM_ID
  AND MSA.ORGANIZATION_ID   = MSI.ORGANIZATION_ID
  AND MSI.SOURCE_TYPE       = 2
  )
ORDER BY LPAD(POV.SEGMENT1,10)','','XXWC Vendors','','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'SELECT ood.organization_code organization_code,
  ood.organization_name organization_name
FROM ORG_ORGANIZATION_DEFINITIONS OOD
WHERE organization_code NOT IN(''CAN'',''HDS'',''US1'',''CN1'')
ORDER BY organization_code','','XXWC Org Lov','','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'','Exclude,Include','XXWC Include Exclude','','ANONYMOUS',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 201');
END IF;
END;
/
prompt Creating Report Data for Inventory Sales and Reorder Report - WC
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(201);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Inventory Sales and Reorder Report - WC
xxeis.eis_rsc_utility.delete_report_rows( 'Inventory Sales and Reorder Report - WC' );
--Inserting Report - Inventory Sales and Reorder Report - WC
xxeis.eis_rsc_ins.r( 201,'Inventory Sales and Reorder Report - WC','','This report displays info needed to make inventory replenishment decisions ( by vendor, part number) at selected locations.','','','','VP038429','EIS_XXWC_ISR_REPORT_V','Y','','','VP038429','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N','','','','','','','','US','','','','');
--Inserting Report Columns - Inventory Sales and Reorder Report - WC
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'DIR_REQ','D Req','Dir Req','','~T~D~0','default','','35','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'PLT','PLT','Plt','','~T~D~0','default','','12','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'PPLT','PPLT','Pplt','','~T~D~0','default','','11','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'CAT_SBA_OWNER','Cat','Cat Sba Owner','','','default','','67','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'FLIP_DATE','Flip Date','Flip Date','','','default','','44','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'MAKE_BUY','Make Buy','Make Buy','','','default','','70','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'MST_USER_ITEM_TYPE','Mst User Item Type','Mst User Item Type','','','default','','74','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'LAST_RECEIPT_DATE','Last Receipt Date','Last Receipt Date','','','default','','75','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'MST_ITEM_STATUS','Mst Item Status','Mst Item Status','','','default','','73','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'SUPERSEDE_ITEM','Supersede Item','Supersede Item','','','default','','77','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'FML','FLM','Fml','','~T~D~0','default','','17','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'OPEN_REQ','E Req','Open Req','','~T~D~0','default','','33','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'WT','Wt','Wt','','~T~D~0','default','','16','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'SO','So','So','','~T~D~0','default','','18','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'SOURCING_RULE','Sourcing Rule','Sourcing Rule','','','default','','6','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'SS','Ss','Ss','','~T~D~0','default','','14','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'CLT','CLT','Clt','','~T~D~0','default','','13','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'AVAIL2','Avail2','Avail2','','~T~D~0','default','','38','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'BPA_COST','Bpa Cost','Bpa Cost','','~T~D~2','default','','29','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'ON_ORD','On Ord','On Ord','','~T~D~0','default','','36','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'APR_SALES','Apr','Apr Sales','','~T~D~0','default','','56','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'INT_REQ','I Req','Int Req','','~T~D~0','default','','34','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'CORE','Core','Core','','','default','','65','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'MST_VENDOR','Mst Vendor','Mst Vendor','','','default','','69','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'ORG_ITEM_STATUS','Org Item Status','Org Item Status','','','default','','71','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'ORG_USER_ITEM_TYPE','Org User Item Type','Org User Item Type','','','default','','72','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'TIER','Tier','Tier','','','default','','66','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'MFG_PART_NUMBER','Mfg Part Number','Mfg Part Number','','','default','','68','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'ONHAND_GT_270','Onhand Gt 270','Onhand Gt 270','','~T~D~0','default','','76','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'TURNS','Turns','Turns','','~T~D~0','default','','50','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'TWELVE_SALES','12','Twelve Sales','','~T~D~0','default','','42','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'UOM','UOM','Uom','','','default','','15','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'VENDOR_NAME','Vendor','Vendor Name','','','default','','5','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'VENDOR_NUM','Vend #','Vendor Num','','','default','','4','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'NOV_SALES','Nov','Nov Sales','','~T~D~0','default','','63','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'OCT_SALES','Oct','Oct Sales','','~T~D~0','default','','62','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'ONE_SALES','1','One Sales','','~T~D~0','default','','40','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'ORG','Org','Org','','','default','','1','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'PM','PM','Pm','','','default','','21','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'PRE','Pre','Pre','','','default','','2','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'QOH','QOH','Qoh','','~T~D~0','default','','31','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'RES','Res','Res','NUMBER','~T~D~0','default','','47','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'SEP_SALES','Sep','Sep Sales','','~T~D~0','default','','61','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'SIX_SALES','6','Six Sales','','~T~D~0','default','','41','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'SOURCE','Source','Source','','','default','','7','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'ST','ST','St','','','default','','8','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'STK_FLAG','Stk','Stk Flag','','','default','','20','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'THIRTEEN_WK_AN_COGS','13 Wk An COGS $','Thirteen Wk An Cogs','','~T~D~0','default','','49','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'AUG_SALES','Aug','Aug Sales','','~T~D~0','default','','60','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'AVAILABLE','Avail1','Available','','~T~D~0','default','','37','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'AVAILABLEDOLLAR','Ext$','Availabledollar','','~T~D~0','default','','39','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'AVER_COST','Aver Cost','Aver Cost','','~T~D~2','default','','27','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'BIN_LOC','Bin Loc','Bin Loc','','','default','','43','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'BPA','Bpa#','Bpa','','','default','','30','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'BUYER','Buyer','Buyer','','','default','','51','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'CAT','Cat/Cl','Cat','','','default','','10','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'CL','Cl','Cl','','','default','','19','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'DEC_SALES','Dec','Dec Sales','','~T~D~0','default','','64','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'DESCRIPTION','Description','Description','','','default','','9','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'FEB_SALES','Feb','Feb Sales','','~T~D~0','default','','54','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'FI_FLAG','FI','Fi Flag','','','default','','45','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'FREEZE_DATE','Res$','Freeze Date','','','default','','46','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'HIT4_SALES','Hit4','Hit4 Sales','','~T~D~0','default','','26','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'HIT6_SALES','Hit6','Hit6 Sales','','~T~D~0','default','','25','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'ITEM_NUMBER','Item Number','Item Number','','','default','','3','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'JAN_SALES','Jan','Jan Sales','','~T~D~0','default','','53','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'JUL_SALES','Jul','Jul Sales','','~T~D~0','default','','59','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'JUNE_SALES','June','June Sales','','~T~D~0','default','','58','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'MAR_SALES','Mar','Mar Sales','','~T~D~0','default','','55','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'MAXN','Max','Maxn','NUMBER','~T~D~0','default','','23','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'MAY_SALES','May','May Sales','','~T~D~0','default','','57','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'MINN','Min','Minn','NUMBER','~T~D~0','default','','22','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'THIRTEEN_WK_AVG_INV','13 Wk Av Inv $','Thirteen Wk Avg Inv','','~T~D~0','default','','48','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'TS','TS','Ts','NUMBER','~T~D~0','default','','52','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'AMU','AMU','Amu','NUMBER','~T~D~0','default','','24','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'DEM','Dem','Dem','','~T~D~0','default','','32','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Sales and Reorder Report - WC',201,'LIST_PRICE','List Price','List Price','','~T~D~2','default','','28','N','','','','','','','','VP038429','N','N','','EIS_XXWC_ISR_REPORT_V','','','SUM','US','');
--Inserting Report Parameters - Inventory Sales and Reorder Report - WC
xxeis.eis_rsc_ins.rp( 'Inventory Sales and Reorder Report - WC',201,'Item','Item','ITEM_NUMBER','IN','XXWC Item','','VARCHAR2','N','Y','16','Y','Y','CONSTANT','VP038429','Y','N','','','','EIS_XXWC_ISR_REPORT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Inventory Sales and Reorder Report - WC',201,'Cat Class','Cat Class','CAT','IN','Catclass Lov','','VARCHAR2','N','Y','17','Y','Y','CONSTANT','VP038429','Y','N','','','','EIS_XXWC_ISR_REPORT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Inventory Sales and Reorder Report - WC',201,'Items w/ 4 mon sales hits','Stock items w/ 4 mon sales hits > x','','IN','','','NUMERIC','N','Y','10','','N','CONSTANT','VP038429','Y','N','','','','EIS_XXWC_ISR_REPORT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Inventory Sales and Reorder Report - WC',201,'Report Criteria','Report Criteria','','IN','Report Criteria','','VARCHAR2','N','Y','11','','N','CONSTANT','VP038429','Y','N','','','','EIS_XXWC_ISR_REPORT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Inventory Sales and Reorder Report - WC',201,'Report Criteria Value','Report Criteria Value','','IN','','','VARCHAR2','N','Y','12','','N','CONSTANT','VP038429','Y','N','','','','EIS_XXWC_ISR_REPORT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Inventory Sales and Reorder Report - WC',201,'DC Mode','DC Mode','','IN','EIS PO XXWC DC Mode','''Yes''','VARCHAR2','N','Y','5','','N','CONSTANT','VP038429','Y','N','','','','EIS_XXWC_ISR_REPORT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Inventory Sales and Reorder Report - WC',201,'Starting Bin','Starting Bin','BIN_LOC','>=','Bin Location Lov','','VARCHAR2','N','Y','13','','Y','CONSTANT','VP038429','Y','N','','','','EIS_XXWC_ISR_REPORT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Inventory Sales and Reorder Report - WC',201,'Report Filter','Report Condition','','IN','EIS PO XXWC ISR REPORT COND','','VARCHAR2','N','Y','9','','N','CONSTANT','VP038429','Y','N','','','','EIS_XXWC_ISR_REPORT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Inventory Sales and Reorder Report - WC',201,'Item List','Item List','','IN','XXWC Item List','','VARCHAR2','N','Y','18','','N','CONSTANT','VP038429','Y','N','','','','EIS_XXWC_ISR_REPORT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Inventory Sales and Reorder Report - WC',201,'Vendor List','Vendor List','','IN','XXWC Supplier List','','VARCHAR2','N','Y','19','','N','CONSTANT','VP038429','Y','N','','','','EIS_XXWC_ISR_REPORT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Inventory Sales and Reorder Report - WC',201,'Cat Class List','Cat Class List','','IN','XXWC Cat Class List','','VARCHAR2','N','Y','20','','N','CONSTANT','VP038429','Y','N','','','','EIS_XXWC_ISR_REPORT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Inventory Sales and Reorder Report - WC',201,'Source List','Source List','','IN','XXWC Source List','','VARCHAR2','N','Y','21','','N','CONSTANT','VP038429','Y','N','','','','EIS_XXWC_ISR_REPORT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Inventory Sales and Reorder Report - WC',201,'Organization List','Organization List','','IN','XXWC Org List','','VARCHAR2','N','Y','4','','N','CONSTANT','VP038429','Y','N','','','','EIS_XXWC_ISR_REPORT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Inventory Sales and Reorder Report - WC',201,'Organization','Organization','ORG','IN','XXWC Org Lov','','VARCHAR2','N','Y','3','Y','Y','CONSTANT','VP038429','Y','N','','','','EIS_XXWC_ISR_REPORT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Inventory Sales and Reorder Report - WC',201,'Intangible Items','Intangible Items','','IN','XXWC Include Exclude','''Exclude''','VARCHAR2','N','Y','7','','N','CONSTANT','VP038429','Y','N','','','','EIS_XXWC_ISR_REPORT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Inventory Sales and Reorder Report - WC',201,'Region','Region','REGION','IN','Region Lov','','VARCHAR2','N','Y','1','Y','Y','CONSTANT','VP038429','Y','N','','','','EIS_XXWC_ISR_REPORT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Inventory Sales and Reorder Report - WC',201,'Ending Bin','Ending Bin','BIN_LOC','<=','Bin Location Lov','','VARCHAR2','N','Y','14','','Y','CONSTANT','VP038429','Y','N','','','','EIS_XXWC_ISR_REPORT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Inventory Sales and Reorder Report - WC',201,'District','District','DISTRICT','IN','District Lov','','VARCHAR2','N','Y','2','Y','Y','CONSTANT','VP038429','Y','N','','','','EIS_XXWC_ISR_REPORT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Inventory Sales and Reorder Report - WC',201,'Tool Repair','Tool Repair','','IN','Tool Repair','''Include''','VARCHAR2','N','Y','6','','N','CONSTANT','VP038429','Y','N','','','','EIS_XXWC_ISR_REPORT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Inventory Sales and Reorder Report - WC',201,'Time Sensitive','Time Sensitive','','IN','EIS PO XXWC Time Sensitive','','VARCHAR2','N','Y','8','','N','CONSTANT','VP038429','Y','N','','','','EIS_XXWC_ISR_REPORT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Inventory Sales and Reorder Report - WC',201,'Vendor','Vendor','VENDOR_NUM','IN','XXWC Vendors','','VARCHAR2','N','Y','15','Y','Y','CONSTANT','VP038429','Y','N','','','','EIS_XXWC_ISR_REPORT_V','','','US','');
--Inserting Dependent Parameters - Inventory Sales and Reorder Report - WC
--Inserting Report Conditions - Inventory Sales and Reorder Report - WC
xxeis.eis_rsc_ins.rcnh( 'Inventory Sales and Reorder Report - WC',201,'BIN_LOC <= :Ending Bin ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','BIN_LOC','','Ending Bin','','','','','EIS_XXWC_ISR_REPORT_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',201,'Inventory Sales and Reorder Report - WC','BIN_LOC <= :Ending Bin ');
xxeis.eis_rsc_ins.rcnh( 'Inventory Sales and Reorder Report - WC',201,'BIN_LOC >= :Starting Bin ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','BIN_LOC','','Starting Bin','','','','','EIS_XXWC_ISR_REPORT_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',201,'Inventory Sales and Reorder Report - WC','BIN_LOC >= :Starting Bin ');
xxeis.eis_rsc_ins.rcnh( 'Inventory Sales and Reorder Report - WC',201,'CAT IN :Cat Class ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CAT','','Cat Class','','','','','EIS_XXWC_ISR_REPORT_V','','','','','','IN','Y','Y','','','','','1',201,'Inventory Sales and Reorder Report - WC','CAT IN :Cat Class ');
xxeis.eis_rsc_ins.rcnh( 'Inventory Sales and Reorder Report - WC',201,'DISTRICT IN :District ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','DISTRICT','','District','','','','','EIS_XXWC_ISR_REPORT_V','','','','','','IN','Y','Y','','','','','1',201,'Inventory Sales and Reorder Report - WC','DISTRICT IN :District ');
xxeis.eis_rsc_ins.rcnh( 'Inventory Sales and Reorder Report - WC',201,'ITEM_NUMBER IN :Item ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','ITEM_NUMBER','','Item','','','','','EIS_XXWC_ISR_REPORT_V','','','','','','IN','Y','Y','','','','','1',201,'Inventory Sales and Reorder Report - WC','ITEM_NUMBER IN :Item ');
xxeis.eis_rsc_ins.rcnh( 'Inventory Sales and Reorder Report - WC',201,'ORG IN :Organization ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','ORG','','Organization','','','','','EIS_XXWC_ISR_REPORT_V','','','','','','IN','Y','Y','','','','','1',201,'Inventory Sales and Reorder Report - WC','ORG IN :Organization ');
xxeis.eis_rsc_ins.rcnh( 'Inventory Sales and Reorder Report - WC',201,'PROCESS_ID IN :SYSTEM.PROCESS_ID ','ADVANCED','','1#$#','Y','');
xxeis.eis_rsc_ins.rcnd( '','','PROCESS_ID','','','','','','','EIS_XXWC_ISR_REPORT_V','','','','','','IN','Y','N','',':SYSTEM.PROCESS_ID','','','1',201,'Inventory Sales and Reorder Report - WC','PROCESS_ID IN :SYSTEM.PROCESS_ID ');
xxeis.eis_rsc_ins.rcnh( 'Inventory Sales and Reorder Report - WC',201,'REGION IN :Region ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','REGION','','Region','','','','','EIS_XXWC_ISR_REPORT_V','','','','','','IN','Y','Y','','','','','1',201,'Inventory Sales and Reorder Report - WC','REGION IN :Region ');
xxeis.eis_rsc_ins.rcnh( 'Inventory Sales and Reorder Report - WC',201,'VENDOR_NUM IN :Vendor ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','VENDOR_NUM','','Vendor','','','','','EIS_XXWC_ISR_REPORT_V','','','','','','IN','Y','Y','','','','','1',201,'Inventory Sales and Reorder Report - WC','VENDOR_NUM IN :Vendor ');
--Inserting Report Sorts - Inventory Sales and Reorder Report - WC
--Inserting Report Triggers - Inventory Sales and Reorder Report - WC
xxeis.eis_rsc_ins.rt( 'Inventory Sales and Reorder Report - WC',201,'BEGIN
xxeis.eis_po_xxwc_isr_util_pkg.G_isr_rpt_dc_mod_sub:=:DC Mode;
xxwc_isr_datamart_pkg.report_isr(
p_process_id => :SYSTEM.PROCESS_ID,
p_region =>:Region,
p_district =>:District,
p_location =>:Organization,
p_dc_mode =>:DC Mode,
p_tool_repair =>:Tool Repair,
p_time_sensitive =>:Time Sensitive,
p_stk_items_with_hit4 =>:Items w/ 4 mon sales hits,
p_report_condition =>:Report Filter,
p_report_criteria            =>:Report Criteria,
p_report_criteria_val            =>:Report Criteria Value,
p_start_bin_loc =>:Starting Bin,
p_end_bin_loc =>:Ending Bin,
p_vendor =>:Vendor,
p_item =>:Item,
p_cat_class =>:Cat Class,
p_org_list =>:Organization List,
p_item_list =>:Item List,
p_supplier_list =>:Vendor List,
p_cat_class_list =>:Cat Class List,
p_source_list =>:Source List,
p_intangibles => :Intangible Items
);
END;','B','Y','VP038429','');
xxeis.eis_rsc_ins.rt( 'Inventory Sales and Reorder Report - WC',201,'BEGIN
xxeis.eis_rs_xxwc_com_util_pkg.parse_cleanup_table(:system.process_id);
END;','A','Y','VP038429','');
--inserting report templates - Inventory Sales and Reorder Report - WC
--Inserting Report Portals - Inventory Sales and Reorder Report - WC
--inserting report dashboards - Inventory Sales and Reorder Report - WC
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Inventory Sales and Reorder Report - WC','201','EIS_XXWC_ISR_REPORT_V','EIS_XXWC_ISR_REPORT_V','N','');
--inserting report security - Inventory Sales and Reorder Report - WC
xxeis.eis_rsc_ins.rsec( 'Inventory Sales and Reorder Report - WC','201','','XXWC_PUR_SUPER_USER',201,'VP038429','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory Sales and Reorder Report - WC','401','','XXWC_INV_PLANNER',201,'VP038429','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory Sales and Reorder Report - WC','201','','XXWC_PURCHASING_SR_MRG_WC',201,'VP038429','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory Sales and Reorder Report - WC','','VP038429','',201,'VP038429','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory Sales and Reorder Report - WC','','AB063501','',201,'VP038429','','N','');
--Inserting Report Pivots - Inventory Sales and Reorder Report - WC
--Inserting Report   Version details- Inventory Sales and Reorder Report - WC
xxeis.eis_rsc_ins.rv( 'Inventory Sales and Reorder Report - WC','','Inventory Sales and Reorder Report - WC','AB063501');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 201');
END IF;
END;
/
