--Report Name            : White Cap Daily Flash Cash & Charge Details.
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating View Data for White Cap Daily Flash Cash & Charge Details
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_AR_DAILY_FLASH_DL_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_AR_DAILY_FLASH_DL_V',660,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Xxwc Ar Daily Flash Dl V','EXADFDV','','');
--Delete View Columns for EIS_XXWC_AR_DAILY_FLASH_DL_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_AR_DAILY_FLASH_DL_V',660,FALSE);
--Inserting View Columns for EIS_XXWC_AR_DAILY_FLASH_DL_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_DAILY_FLASH_DL_V','LINE_ID',660,'Line Id','LINE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Line Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_DAILY_FLASH_DL_V','ORGANIZATION_ID',660,'Organization Id','ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_DAILY_FLASH_DL_V','INVENTORY_ITEM_ID',660,'Inventory Item Id','INVENTORY_ITEM_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Inventory Item Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_DAILY_FLASH_DL_V','CUSTOMER_TRX_LINE_ID',660,'Customer Trx Line Id','CUSTOMER_TRX_LINE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Customer Trx Line Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_DAILY_FLASH_DL_V','DATE1',660,'Date1','DATE1','','','','XXEIS_RS_ADMIN','DATE','','','Date1','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_DAILY_FLASH_DL_V','DELIVERY_TYPE',660,'Delivery Type','DELIVERY_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Delivery Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_DAILY_FLASH_DL_V','SALES_ORDER_CROSS_REF',660,'Sales Order Cross Ref','SALES_ORDER_CROSS_REF','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Sales Order Cross Ref','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_DAILY_FLASH_DL_V','BILL_TO_CUSTOMER_NAME',660,'Bill To Customer Name','BILL_TO_CUSTOMER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Bill To Customer Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_DAILY_FLASH_DL_V','BILL_TO_CUSTOMER',660,'Bill To Customer','BILL_TO_CUSTOMER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Bill To Customer','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_DAILY_FLASH_DL_V','SALES_ORDER_TYPE',660,'Sales Order Type','SALES_ORDER_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Sales Order Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_DAILY_FLASH_DL_V','SALES_ORDER',660,'Sales Order','SALES_ORDER','','','','XXEIS_RS_ADMIN','NUMBER','','','Sales Order','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_DAILY_FLASH_DL_V','SALES_ORDER_DATE',660,'Sales Order Date','SALES_ORDER_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Sales Order Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_DAILY_FLASH_DL_V','REGION',660,'Region','REGION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Region','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_DAILY_FLASH_DL_V','DISTRICT',660,'District','DISTRICT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','District','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_DAILY_FLASH_DL_V','BRANCH',660,'Branch','BRANCH','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Branch','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_DAILY_FLASH_DL_V','ITEM_CATCLASS',660,'Item Catclass','ITEM_CATCLASS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Item Catclass','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_DAILY_FLASH_DL_V','EXTENDED_COST',660,'Extended Cost','EXTENDED_COST','','','','XXEIS_RS_ADMIN','NUMBER','','','Extended Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_DAILY_FLASH_DL_V','UNIT_COST',660,'Unit Cost','UNIT_COST','','','','XXEIS_RS_ADMIN','NUMBER','','','Unit Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_DAILY_FLASH_DL_V','EXTENDED_SALE',660,'Extended Sale','EXTENDED_SALE','','','','XXEIS_RS_ADMIN','NUMBER','','','Extended Sale','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_DAILY_FLASH_DL_V','UNIT_SELLING_PRICE',660,'Unit Selling Price','UNIT_SELLING_PRICE','','','','XXEIS_RS_ADMIN','NUMBER','','','Unit Selling Price','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_DAILY_FLASH_DL_V','QTY',660,'Qty','QTY','','','','XXEIS_RS_ADMIN','NUMBER','','','Qty','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_DAILY_FLASH_DL_V','UOM',660,'Uom','UOM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Uom','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_DAILY_FLASH_DL_V','ITEM_DESCRIPTION',660,'Item Description','ITEM_DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Item Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_DAILY_FLASH_DL_V','ITEM',660,'Item','ITEM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Item','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_DAILY_FLASH_DL_V','INVOICE_LINE',660,'Invoice Line','INVOICE_LINE','','','','XXEIS_RS_ADMIN','NUMBER','','','Invoice Line','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_DAILY_FLASH_DL_V','INVOICE',660,'Invoice','INVOICE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Invoice','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_DAILY_FLASH_DL_V','FLASH_VERSION',660,'Flash Version','FLASH_VERSION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Flash Version','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_DAILY_FLASH_DL_V','LOCATION',660,'Location','LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Location','','','');
--Inserting View Components for EIS_XXWC_AR_DAILY_FLASH_DL_V
--Inserting View Component Joins for EIS_XXWC_AR_DAILY_FLASH_DL_V
END;
/
set scan on define on
prompt Creating Report LOV Data for White Cap Daily Flash Cash & Charge Details
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - White Cap Daily Flash Cash & Charge Details
xxeis.eis_rs_ins.lov( 660,'SELECT ORGANIZATION_CODE WAREHOUSE,ORGANIZATION_NAME FROM ORG_ORGANIZATION_DEFINITIONS OOD WHERE SYSDATE < NVL(OOD.DISABLE_DATE,SYSDATE+1) AND EXISTS (SELECT 1 FROM XXEIS.EIS_ORG_ACCESS_V  WHERE organization_id = ood.organization_id )
UNION
SELECT ''All'', ''All Organizations'' from Dual','','OM Warehouse All','','ANONYMOUS',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for White Cap Daily Flash Cash & Charge Details
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - White Cap Daily Flash Cash & Charge Details
xxeis.eis_rs_utility.delete_report_rows( 'White Cap Daily Flash Cash & Charge Details' );
--Inserting Report - White Cap Daily Flash Cash & Charge Details
xxeis.eis_rs_ins.r( 660,'White Cap Daily Flash Cash & Charge Details','','','','','','XXEIS_RS_ADMIN','EIS_XXWC_AR_DAILY_FLASH_DL_V','Y','','','XXEIS_RS_ADMIN','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','');
--Inserting Report Columns - White Cap Daily Flash Cash & Charge Details
xxeis.eis_rs_ins.rc( 'White Cap Daily Flash Cash & Charge Details',660,'BILL_TO_CUSTOMER','Bill To Customer','Bill To Customer','','','default','','19','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_DAILY_FLASH_DL_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Daily Flash Cash & Charge Details',660,'BILL_TO_CUSTOMER_NAME','Bill To Customer Name','Bill To Customer Name','','','default','','20','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_DAILY_FLASH_DL_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Daily Flash Cash & Charge Details',660,'BRANCH','Branch','Branch','','','default','','13','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_DAILY_FLASH_DL_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Daily Flash Cash & Charge Details',660,'DELIVERY_TYPE','Delivery Type','Delivery Type','','','default','','22','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_DAILY_FLASH_DL_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Daily Flash Cash & Charge Details',660,'DISTRICT','District','District','','','default','','14','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_DAILY_FLASH_DL_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Daily Flash Cash & Charge Details',660,'EXTENDED_COST','Extended Cost','Extended Cost','','~,~.~2','default','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_DAILY_FLASH_DL_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Daily Flash Cash & Charge Details',660,'EXTENDED_SALE','Extended Sales','Extended Sale','','~,~.~2','default','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_DAILY_FLASH_DL_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Daily Flash Cash & Charge Details',660,'FLASH_VERSION','Flash Version','Flash Version','','','default','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_DAILY_FLASH_DL_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Daily Flash Cash & Charge Details',660,'INVOICE','Invoice','Invoice','','','default','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_DAILY_FLASH_DL_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Daily Flash Cash & Charge Details',660,'INVOICE_LINE','Invoice Line','Invoice Line','','~~~','default','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_DAILY_FLASH_DL_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Daily Flash Cash & Charge Details',660,'ITEM','Item','Item','','','default','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_DAILY_FLASH_DL_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Daily Flash Cash & Charge Details',660,'ITEM_CATCLASS','Item Catclass','Item Catclass','','','default','','12','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_DAILY_FLASH_DL_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Daily Flash Cash & Charge Details',660,'ITEM_DESCRIPTION','Item Description','Item Description','','','default','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_DAILY_FLASH_DL_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Daily Flash Cash & Charge Details',660,'QTY','Qty','Qty','','~~~','default','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_DAILY_FLASH_DL_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Daily Flash Cash & Charge Details',660,'REGION','Region','Region','','','default','','15','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_DAILY_FLASH_DL_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Daily Flash Cash & Charge Details',660,'SALES_ORDER','Sales Order','Sales Order','','~~~','default','','17','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_DAILY_FLASH_DL_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Daily Flash Cash & Charge Details',660,'SALES_ORDER_CROSS_REF','Sales Order Cross Ref','Sales Order Cross Ref','','','default','','21','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_DAILY_FLASH_DL_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Daily Flash Cash & Charge Details',660,'SALES_ORDER_DATE','Sales Order Date','Sales Order Date','','','default','','16','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_DAILY_FLASH_DL_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Daily Flash Cash & Charge Details',660,'SALES_ORDER_TYPE','Sales Order Type','Sales Order Type','','','default','','18','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_DAILY_FLASH_DL_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Daily Flash Cash & Charge Details',660,'UNIT_COST','Unit Cost','Unit Cost','','~,~.~5','default','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_DAILY_FLASH_DL_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Daily Flash Cash & Charge Details',660,'UNIT_SELLING_PRICE','Unit Selling Price','Unit Selling Price','','~,~.~5','default','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_DAILY_FLASH_DL_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Daily Flash Cash & Charge Details',660,'UOM','Uom','Uom','','','default','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_DAILY_FLASH_DL_V','','');
--Inserting Report Parameters - White Cap Daily Flash Cash & Charge Details
xxeis.eis_rs_ins.rp( 'White Cap Daily Flash Cash & Charge Details',660,'Location','Location','LOCATION','IN','OM Warehouse All','','VARCHAR2','Y','Y','2','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'White Cap Daily Flash Cash & Charge Details',660,'Date','Prior day�s date','','IN','','select trunc(sysdate-1) from dual','DATE','N','Y','1','','N','SQL','XXEIS_RS_ADMIN','Y','N','','','');
--Inserting Report Conditions - White Cap Daily Flash Cash & Charge Details
xxeis.eis_rs_ins.rcn( 'White Cap Daily Flash Cash & Charge Details',660,'','','','','AND ( ''All'' IN (:Location) OR (LOCATION IN (:Location)))','Y','1','','XXEIS_RS_ADMIN');
--Inserting Report Sorts - White Cap Daily Flash Cash & Charge Details
--Inserting Report Triggers - White Cap Daily Flash Cash & Charge Details
xxeis.eis_rs_ins.rt( 'White Cap Daily Flash Cash & Charge Details',660,'begin
xxeis.eis_rs_xxwc_com_util_pkg.set_date_from(:Date);
end;','B','Y','XXEIS_RS_ADMIN');
--Inserting Report Templates - White Cap Daily Flash Cash & Charge Details
--Inserting Report Portals - White Cap Daily Flash Cash & Charge Details
--Inserting Report Dashboards - White Cap Daily Flash Cash & Charge Details
--Inserting Report Security - White Cap Daily Flash Cash & Charge Details
xxeis.eis_rs_ins.rsec( 'White Cap Daily Flash Cash & Charge Details','660','','51045',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap Daily Flash Cash & Charge Details','660','','50901',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap Daily Flash Cash & Charge Details','660','','51025',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap Daily Flash Cash & Charge Details','660','','50860',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap Daily Flash Cash & Charge Details','660','','50886',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap Daily Flash Cash & Charge Details','660','','50859',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap Daily Flash Cash & Charge Details','660','','50858',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap Daily Flash Cash & Charge Details','660','','50857',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap Daily Flash Cash & Charge Details','20005','','50900',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap Daily Flash Cash & Charge Details','222','','50907',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap Daily Flash Cash & Charge Details','222','','50894',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap Daily Flash Cash & Charge Details','222','','50873',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap Daily Flash Cash & Charge Details','222','','50993',660,'XXEIS_RS_ADMIN','','');
--Inserting Report Pivots - White Cap Daily Flash Cash & Charge Details
xxeis.eis_rs_ins.rpivot( 'White Cap Daily Flash Cash & Charge Details',660,'Sales Activity by Flash Version','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Sales Activity by Flash Version
xxeis.eis_rs_ins.rpivot_dtls( 'White Cap Daily Flash Cash & Charge Details',660,'Sales Activity by Flash Version','FLASH_VERSION','PAGE_FIELD','','','1','','');
xxeis.eis_rs_ins.rpivot_dtls( 'White Cap Daily Flash Cash & Charge Details',660,'Sales Activity by Flash Version','EXTENDED_COST','DATA_FIELD','SUM','','2','','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'White Cap Daily Flash Cash & Charge Details',660,'Sales Activity by Flash Version','EXTENDED_SALE','DATA_FIELD','SUM','','1','','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'White Cap Daily Flash Cash & Charge Details',660,'Sales Activity by Flash Version','BRANCH','ROW_FIELD','','','1','1','');
--Inserting Report Summary Calculation Columns For Pivot- Sales Activity by Flash Version
END;
/
set scan on define on
