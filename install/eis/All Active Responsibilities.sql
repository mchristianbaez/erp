--Report Name            : All Active Responsibilities
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data XXEIS_543_VJTAPE_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(85000);
IF mod_exist = 'Y' THEN 
--Inserting Object XXEIS_543_VJTAPE_V
xxeis.eis_rsc_ins.v( 'XXEIS_543_VJTAPE_V',85000,'Paste SQL View for All Active Responsibilities','1.0','','','10011289','APPS','All Active Responsibilities View','X5VV','','','VIEW','US','','');
--Delete Object Columns for XXEIS_543_VJTAPE_V
xxeis.eis_rsc_utility.delete_view_rows('XXEIS_543_VJTAPE_V',85000,FALSE);
--Inserting Object Columns for XXEIS_543_VJTAPE_V
xxeis.eis_rsc_ins.vc( 'XXEIS_543_VJTAPE_V','NAME',85000,'','','','','','10011289','VARCHAR2','','','Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_543_VJTAPE_V','DISPLAY_NAME',85000,'','','','','','10011289','VARCHAR2','','','Display Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_543_VJTAPE_V','DESCRIPTION',85000,'','','','','','10011289','VARCHAR2','','','Description','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_543_VJTAPE_V','APPLICATION',85000,'','','','','','10011289','VARCHAR2','','','Application','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_543_VJTAPE_V','START_DATE',85000,'','','','','','10011289','DATE','','','Start Date','','','','US');
--Inserting Object Components for XXEIS_543_VJTAPE_V
--Inserting Object Component Joins for XXEIS_543_VJTAPE_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 85000');
END IF;
END;
/
prompt Creating Report LOV Data for All Active Responsibilities
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(85000);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - All Active Responsibilities
xxeis.eis_rsc_ins.lov( 85000,'SELECT application_name FROM xxeis.eis_rs_applications','','Application Names','','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 85000');
END IF;
END;
/
prompt Creating Report Data for All Active Responsibilities
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(85000);
IF mod_exist = 'Y' THEN 
--Deleting Report data - All Active Responsibilities
xxeis.eis_rsc_utility.delete_report_rows( 'All Active Responsibilities' );
--Inserting Report - All Active Responsibilities
xxeis.eis_rsc_ins.r( 85000,'All Active Responsibilities','','This report shows all active Responsibilities.','','','','10011289','XXEIS_543_VJTAPE_V','Y','','SELECT NAME
      ,display_name
      ,description
      ,owner_tag AS application
      ,start_date
  FROM wf_local_roles lr
 WHERE lr.name LIKE ''%FND_RESP%STANDARD''
   AND lr.status = ''ACTIVE''
 ORDER BY display_name ASC
','10011289','','N','WC Audit Reports','','CSV,EXCEL,','N','','','','','','','APPS','US','','','','');
--Inserting Report Columns - All Active Responsibilities
xxeis.eis_rsc_ins.rc( 'All Active Responsibilities',85000,'NAME','Name','','','','','','1','N','','','','','','','','10011289','N','N','','XXEIS_543_VJTAPE_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'All Active Responsibilities',85000,'DISPLAY_NAME','Display Name','','','','','','2','N','','','','','','','','10011289','N','N','','XXEIS_543_VJTAPE_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'All Active Responsibilities',85000,'DESCRIPTION','Description','','','','','','5','N','','','','','','','','10011289','N','N','','XXEIS_543_VJTAPE_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'All Active Responsibilities',85000,'APPLICATION','Application','','','','','','4','N','','','','','','','','10011289','N','N','','XXEIS_543_VJTAPE_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'All Active Responsibilities',85000,'START_DATE','Start Date','','','','','','3','N','','','','','','','','10011289','N','N','','XXEIS_543_VJTAPE_V','','','GROUP_BY','US','');
--Inserting Report Parameters - All Active Responsibilities
xxeis.eis_rsc_ins.rp( 'All Active Responsibilities',85000,'Responsibility Display Name','Responsibility Display Name Like','DISPLAY_NAME','LIKE','','%','VARCHAR2','N','Y','1','','Y','CONSTANT','10011289','Y','','','','','XXEIS_543_VJTAPE_V','','','US','');
xxeis.eis_rsc_ins.rp( 'All Active Responsibilities',85000,'Application','Application','APPLICATION','=','Application Names','','VARCHAR2','N','Y','2','','Y','CONSTANT','10011289','Y','','','','','XXEIS_543_VJTAPE_V','','','US','');
--Inserting Dependent Parameters - All Active Responsibilities
--Inserting Report Conditions - All Active Responsibilities
xxeis.eis_rsc_ins.rcnh( 'All Active Responsibilities',85000,'APPLICATION = :Application ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','APPLICATION','','Application','','','','','XXEIS_543_VJTAPE_V','','','','','','EQUALS','Y','Y','','','','','1',85000,'All Active Responsibilities','APPLICATION = :Application ');
xxeis.eis_rsc_ins.rcnh( 'All Active Responsibilities',85000,'DISPLAY_NAME LIKE :Responsibility Display Name ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','DISPLAY_NAME','','Responsibility Display Name','','','','','XXEIS_543_VJTAPE_V','','','','','','LIKE','Y','Y','','','','','1',85000,'All Active Responsibilities','DISPLAY_NAME LIKE :Responsibility Display Name ');
--Inserting Report Sorts - All Active Responsibilities
xxeis.eis_rsc_ins.rs( 'All Active Responsibilities',85000,'DISPLAY_NAME','ASC','10011289','1','');
--Inserting Report Triggers - All Active Responsibilities
--inserting report templates - All Active Responsibilities
--Inserting Report Portals - All Active Responsibilities
--inserting report dashboards - All Active Responsibilities
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'All Active Responsibilities','85000','XXEIS_543_VJTAPE_V','XXEIS_543_VJTAPE_V','N','');
--inserting report security - All Active Responsibilities
xxeis.eis_rsc_ins.rsec( 'All Active Responsibilities','1','','SYSTEM_ADMINISTRATOR',85000,'10011289','','','');
xxeis.eis_rsc_ins.rsec( 'All Active Responsibilities','20024','','',85000,'10011289','','','');
xxeis.eis_rsc_ins.rsec( 'All Active Responsibilities','20005','','XXWC_VIEW_ALL_EIS_REPORTS',85000,'10011289','','','');
xxeis.eis_rsc_ins.rsec( 'All Active Responsibilities','1','','HDS_SYSTEM_MAINTENANCE',85000,'10011289','','','');
--Inserting Report Pivots - All Active Responsibilities
--Inserting Report   Version details- All Active Responsibilities
xxeis.eis_rsc_ins.rv( 'All Active Responsibilities','','All Active Responsibilities','AB063501');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 85000');
END IF;
END;
/
