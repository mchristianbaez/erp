--Report Name            : Open Purchase Orders Listing - WC
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data EIS_XXWC_PO_OPEN_ORDERS_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(201);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_PO_OPEN_ORDERS_V
xxeis.eis_rsc_ins.v( 'EIS_XXWC_PO_OPEN_ORDERS_V',201,'','','','','ANONYMOUS','XXEIS','Eis Xxwc Po Open Orders V','EXPOOV','','','VIEW','US','','');
--Delete Object Columns for EIS_XXWC_PO_OPEN_ORDERS_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_PO_OPEN_ORDERS_V',201,FALSE);
--Inserting Object Columns for EIS_XXWC_PO_OPEN_ORDERS_V
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','OPERATING_UNIT',201,'Operating Unit','OPERATING_UNIT','','','','ANONYMOUS','VARCHAR2','','','Operating Unit','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','UNIT_MEAS_LOOKUP_CODE',201,'Unit Meas Lookup Code','UNIT_MEAS_LOOKUP_CODE','','','','ANONYMOUS','VARCHAR2','','','Unit Meas Lookup Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','REQUIRED_BY',201,'Required By','REQUIRED_BY','','','','ANONYMOUS','DATE','','','Required By','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','REQ_CREATED_ON',201,'Req Created On','REQ_CREATED_ON','','','','ANONYMOUS','DATE','','','Req Created On','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','REQ_NUM',201,'Req Num','REQ_NUM','','','','ANONYMOUS','VARCHAR2','','','Req Num','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','PO_TYPE',201,'Po Type','PO_TYPE','','','','ANONYMOUS','VARCHAR2','','','Po Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','PO_STATUS',201,'Po Status','PO_STATUS','','','','ANONYMOUS','VARCHAR2','','','Po Status','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','SHIP_NEED_BY_DATE',201,'Ship Need By Date','SHIP_NEED_BY_DATE','','','','ANONYMOUS','DATE','','','Ship Need By Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','AMOUNT_DUE',201,'Amount Due','AMOUNT_DUE','','~T~D~2','','ANONYMOUS','NUMBER','','','Amount Due','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','AMOUNT_RECEIVED',201,'Amount Received','AMOUNT_RECEIVED','','~T~D~2','','ANONYMOUS','NUMBER','','','Amount Received','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','AMOUNT_ORDERED',201,'Amount Ordered','AMOUNT_ORDERED','','~T~D~2','','ANONYMOUS','NUMBER','','','Amount Ordered','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','QUANTITY_DUE',201,'Quantity Due','QUANTITY_DUE','','~T~D~2','','ANONYMOUS','NUMBER','','','Quantity Due','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','QUANTITY_RECEIVED',201,'Quantity Received','QUANTITY_RECEIVED','','~T~D~2','','ANONYMOUS','NUMBER','','','Quantity Received','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','QUANTITY_ORDERED',201,'Quantity Ordered','QUANTITY_ORDERED','','~T~D~2','','ANONYMOUS','NUMBER','','','Quantity Ordered','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','PO_UNIT_PRICE',201,'Po Unit Price','PO_UNIT_PRICE','','~T~D~2','','ANONYMOUS','NUMBER','','','Po Unit Price','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','ITEM_DESCRIPTION',201,'Item Description','ITEM_DESCRIPTION','','','','ANONYMOUS','VARCHAR2','','','Item Description','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','ITEM',201,'Item','ITEM','','','','ANONYMOUS','VARCHAR2','','','Item','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','LINE_NUM',201,'Line Num','LINE_NUM','','','','ANONYMOUS','NUMBER','','','Line Num','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','NOTE_TO_RECEIVER',201,'Note To Receiver','NOTE_TO_RECEIVER','','','','ANONYMOUS','VARCHAR2','','','Note To Receiver','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','REVISED_DATE',201,'Revised Date','REVISED_DATE','','','','ANONYMOUS','DATE','','','Revised Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','REV',201,'Rev','REV','','','','ANONYMOUS','NUMBER','','','Rev','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','SUPPLIER_SITE',201,'Supplier Site','SUPPLIER_SITE','','','','ANONYMOUS','VARCHAR2','','','Supplier Site','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','PO_SUPPLIER_NAME',201,'Po Supplier Name','PO_SUPPLIER_NAME','','','','ANONYMOUS','VARCHAR2','','','Po Supplier Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','PO_NUMBER',201,'Po Number','PO_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Po Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','BUYER_NAME',201,'Buyer Name','BUYER_NAME','','','','ANONYMOUS','VARCHAR2','','','Buyer Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','PO_CREATION_DATE',201,'Po Creation Date','PO_CREATION_DATE','','','','ANONYMOUS','DATE','','','Po Creation Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','ORG_PO_LINES',201,'Org Po Lines','ORG_PO_LINES','','','','ANONYMOUS','NUMBER','','','Org Po Lines','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','HEADER_NEED_BY_DATE',201,'Header Need By Date','HEADER_NEED_BY_DATE','','','','ANONYMOUS','DATE','','','Header Need By Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','SHIP_VIA',201,'Ship Via','SHIP_VIA','','','','ANONYMOUS','VARCHAR2','','','Ship Via','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','SUPPLIER_CONTACT_PHONE',201,'Supplier Contact Phone','SUPPLIER_CONTACT_PHONE','','','','ANONYMOUS','VARCHAR2','','','Supplier Contact Phone','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','SHIP_TO',201,'Ship To','SHIP_TO','','','','ANONYMOUS','VARCHAR2','','','Ship To','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','HEADER_PROMISE_DATE',201,'Header Promise Date','HEADER_PROMISE_DATE','','','','ANONYMOUS','DATE','','','Header Promise Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','LINE_PROMISE_DATE',201,'Line Promise Date','LINE_PROMISE_DATE','','','','ANONYMOUS','DATE','','','Line Promise Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','ORG_CODE',201,'Org Code','ORG_CODE','','','','ANONYMOUS','VARCHAR2','','','Org Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','ACCEPTANCE_DUE_DATE',201,'Acceptance Due Date','ACCEPTANCE_DUE_DATE','','','','ANONYMOUS','DATE','','','Acceptance Due Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','ACCEPTANCE_TYPE',201,'Acceptance Type','ACCEPTANCE_TYPE','','','','ANONYMOUS','VARCHAR2','','','Acceptance Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','ACCEPTED_BY',201,'Accepted By','ACCEPTED_BY','','','','ANONYMOUS','VARCHAR2','','','Accepted By','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','ACCEPTED_FLAG',201,'Accepted Flag','ACCEPTED_FLAG','','','','ANONYMOUS','VARCHAR2','','','Accepted Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','ACTION_DATE',201,'Action Date','ACTION_DATE','','','','ANONYMOUS','DATE','','','Action Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','BIN1',201,'Bin1','BIN1','','','','ANONYMOUS','VARCHAR2','','','Bin1','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','BIN2',201,'Bin2','BIN2','','','','ANONYMOUS','VARCHAR2','','','Bin2','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','BIN3',201,'Bin3','BIN3','','','','ANONYMOUS','VARCHAR2','','','Bin3','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','BUYER_GROUP',201,'Buyer Group','BUYER_GROUP','','','','ANONYMOUS','VARCHAR2','','','Buyer Group','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','ORGANIZATION_CODE',201,'Organization Code','ORGANIZATION_CODE','','','','ANONYMOUS','VARCHAR2','','','Organization Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','ATTRIBUTE2',201,'Attribute2','ATTRIBUTE2','','','','ANONYMOUS','VARCHAR2','','','Attribute2','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','TRANSFERRED_TO_OE_FLAG',201,'Transferred To Oe Flag','TRANSFERRED_TO_OE_FLAG','','','','ANONYMOUS','VARCHAR2','','','Transferred To Oe Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI#WC#AMU1',201,'Msi#Wc#Amu1','MSI#WC#AMU1','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Amu1','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI#WC#LAST_LEAD_TIME1',201,'Msi#Wc#Last Lead Time1','MSI#WC#LAST_LEAD_TIME1','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Last Lead Time1','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI#WC#GTP_INDICATOR1',201,'Msi#Wc#Gtp Indicator1','MSI#WC#GTP_INDICATOR1','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Gtp Indicator1','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI#WC#HAZMAT_CONTAINER1',201,'Msi#Wc#Hazmat Container1','MSI#WC#HAZMAT_CONTAINER1','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Hazmat Container1','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI#WC#HAZMAT_DESCRIPTION1',201,'Msi#Wc#Hazmat Description1','MSI#WC#HAZMAT_DESCRIPTION1','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Hazmat Description1','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI#WC#PRISM_PART_NUMBER1',201,'Msi#Wc#Prism Part Number1','MSI#WC#PRISM_PART_NUMBER1','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Prism Part Number1','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI#WC#YEARLY_DC_VELOCITY1',201,'Msi#Wc#Yearly Dc Velocity1','MSI#WC#YEARLY_DC_VELOCITY1','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Yearly Dc Velocity1','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI#WC#YEARLY_STORE_VELOCITY1',201,'Msi#Wc#Yearly Store Velocity1','MSI#WC#YEARLY_STORE_VELOCITY1','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Yearly Store Velocity1','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI#WC#DC_VELOCITY1',201,'Msi#Wc#Dc Velocity1','MSI#WC#DC_VELOCITY1','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Dc Velocity1','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI#WC#STORE_VELOCITY1',201,'Msi#Wc#Store Velocity1','MSI#WC#STORE_VELOCITY1','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Store Velocity1','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI#WC#ORM_D_FLAG1',201,'Msi#Wc#Orm D Flag1','MSI#WC#ORM_D_FLAG1','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Orm D Flag1','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI#WC#COUNTRY_OF_ORIGIN1',201,'Msi#Wc#Country Of Origin1','MSI#WC#COUNTRY_OF_ORIGIN1','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Country Of Origin1','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI#WC#CA_PROP_651',201,'Msi#Wc#Ca Prop 651','MSI#WC#CA_PROP_651','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Ca Prop 651','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI#HDS#SKU_DESCRIPTION1',201,'Msi#Hds#Sku Description1','MSI#HDS#SKU_DESCRIPTION1','','','','ANONYMOUS','VARCHAR2','','','Msi#Hds#Sku Description1','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI#HDS#UPC_PRIMARY1',201,'Msi#Hds#Upc Primary1','MSI#HDS#UPC_PRIMARY1','','','','ANONYMOUS','VARCHAR2','','','Msi#Hds#Upc Primary1','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI#HDS#UNSPSC_CODE1',201,'Msi#Hds#Unspsc Code1','MSI#HDS#UNSPSC_CODE1','','','','ANONYMOUS','VARCHAR2','','','Msi#Hds#Unspsc Code1','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI#HDS#VENDOR_PART_NUMBER1',201,'Msi#Hds#Vendor Part Number1','MSI#HDS#VENDOR_PART_NUMBER1','','','','ANONYMOUS','VARCHAR2','','','Msi#Hds#Vendor Part Number1','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI#HDS#PRODUCT_ID1',201,'Msi#Hds#Product Id1','MSI#HDS#PRODUCT_ID1','','','','ANONYMOUS','VARCHAR2','','','Msi#Hds#Product Id1','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI#HDS#INVOICE_UOM1',201,'Msi#Hds#Invoice Uom1','MSI#HDS#INVOICE_UOM1','','','','ANONYMOUS','VARCHAR2','','','Msi#Hds#Invoice Uom1','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI#HDS#DROP_SHIPMENT_ELIGAB1',201,'Msi#Hds#Drop Shipment Eligab1','MSI#HDS#DROP_SHIPMENT_ELIGAB1','','','','ANONYMOUS','VARCHAR2','','','Msi#Hds#Drop Shipment Eligab1','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI#HDS#DROP_SHIPMENT_ELIGAB',201,'Msi#Hds#Drop Shipment Eligab','MSI#HDS#DROP_SHIPMENT_ELIGAB','','','','ANONYMOUS','VARCHAR2','','','Msi#Hds#Drop Shipment Eligab','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI#HDS#LOB1',201,'Msi#Hds#Lob1','MSI#HDS#LOB1','','','','ANONYMOUS','VARCHAR2','','','Msi#Hds#Lob1','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MCA#SALES_ACCOUNT',201,'Mca#Sales Account','MCA#SALES_ACCOUNT','','','','ANONYMOUS','VARCHAR2','','','Mca#Sales Account','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MCA#COGS_ACCOUNT',201,'Mca#Cogs Account','MCA#COGS_ACCOUNT','','','','ANONYMOUS','VARCHAR2','','','Mca#Cogs Account','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','PVS#R11VENDOR#R11_VENDOR_ID',201,'Pvs#R11vendor#R11 Vendor Id','PVS#R11VENDOR#R11_VENDOR_ID','','','','ANONYMOUS','VARCHAR2','','','Pvs#R11vendor#R11 Vendor Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','PVS#R11VENDOR#R11_VENDOR_SIT',201,'Pvs#R11vendor#R11 Vendor Sit','PVS#R11VENDOR#R11_VENDOR_SIT','','','','ANONYMOUS','VARCHAR2','','','Pvs#R11vendor#R11 Vendor Sit','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','PVS#IWOFIPS#FIPS_CODE',201,'Pvs#Iwofips#Fips Code','PVS#IWOFIPS#FIPS_CODE','','','','ANONYMOUS','VARCHAR2','','','Pvs#Iwofips#Fips Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','PV#TAXEXEMPT#EXEMPTIONS',201,'Pv#Taxexempt#Exemptions','PV#TAXEXEMPT#EXEMPTIONS','','','','ANONYMOUS','VARCHAR2','','','Pv#Taxexempt#Exemptions','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','PV#R11VENDOR#R11_VENDOR_ID',201,'Pv#R11vendor#R11 Vendor Id','PV#R11VENDOR#R11_VENDOR_ID','','','','ANONYMOUS','VARCHAR2','','','Pv#R11vendor#R11 Vendor Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','POH#STANDARDP#FOB_TERMS_TAB',201,'Poh#Standardp#Fob Terms Tab','POH#STANDARDP#FOB_TERMS_TAB','','','','ANONYMOUS','VARCHAR2','','','Poh#Standardp#Fob Terms Tab','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','POH#STANDARDP#CARRIER_TERMS_',201,'Poh#Standardp#Carrier Terms ','POH#STANDARDP#CARRIER_TERMS_','','','','ANONYMOUS','VARCHAR2','','','Poh#Standardp#Carrier Terms ','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','POH#STANDARDP#FREIGHT_TERMS_',201,'Poh#Standardp#Freight Terms ','POH#STANDARDP#FREIGHT_TERMS_','','','','ANONYMOUS','VARCHAR2','','','Poh#Standardp#Freight Terms ','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','POH#STANDARDP#NEED_BY_DATE',201,'Poh#Standardp#Need By Date','POH#STANDARDP#NEED_BY_DATE','','','','ANONYMOUS','DATE','','','Poh#Standardp#Need By Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI#WC#HAZMAT_PACKAGING_GROU',201,'Msi#Wc#Hazmat Packaging Grou','MSI#WC#HAZMAT_PACKAGING_GROU','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Hazmat Packaging Grou','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI#WC#MSDS_#',201,'Msi#Wc#Msds #','MSI#WC#MSDS_#','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Msds #','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI#WC#VOC_SUB_CATEGORY',201,'Msi#Wc#Voc Sub Category','MSI#WC#VOC_SUB_CATEGORY','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Voc Sub Category','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI#WC#VOC_CATEGORY',201,'Msi#Wc#Voc Category','MSI#WC#VOC_CATEGORY','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Voc Category','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI#WC#PESTICIDE_FLAG_STATE',201,'Msi#Wc#Pesticide Flag State','MSI#WC#PESTICIDE_FLAG_STATE','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Pesticide Flag State','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI#WC#VOC_GL',201,'Msi#Wc#Voc Gl','MSI#WC#VOC_GL','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Voc Gl','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI#WC#CALC_LEAD_TIME',201,'Msi#Wc#Calc Lead Time','MSI#WC#CALC_LEAD_TIME','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Calc Lead Time','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI#WC#PESTICIDE_FLAG',201,'Msi#Wc#Pesticide Flag','MSI#WC#PESTICIDE_FLAG','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Pesticide Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI#WC#KEEP_ITEM_ACTIVE',201,'Msi#Wc#Keep Item Active','MSI#WC#KEEP_ITEM_ACTIVE','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Keep Item Active','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI#WC#IMPORT_DUTY_',201,'Msi#Wc#Import Duty ','MSI#WC#IMPORT_DUTY_','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Import Duty ','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI#WC#PRODUCT_CODE',201,'Msi#Wc#Product Code','MSI#WC#PRODUCT_CODE','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Product Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI#WC#AVERAGE_UNITS',201,'Msi#Wc#Average Units','MSI#WC#AVERAGE_UNITS','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Average Units','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI#WC#TAXWARE_CODE',201,'Msi#Wc#Taxware Code','MSI#WC#TAXWARE_CODE','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Taxware Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI#WC#RESERVE_STOCK',201,'Msi#Wc#Reserve Stock','MSI#WC#RESERVE_STOCK','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Reserve Stock','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI#WC#AMU',201,'Msi#Wc#Amu','MSI#WC#AMU','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Amu','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI#WC#LAST_LEAD_TIME',201,'Msi#Wc#Last Lead Time','MSI#WC#LAST_LEAD_TIME','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Last Lead Time','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI#WC#GTP_INDICATOR',201,'Msi#Wc#Gtp Indicator','MSI#WC#GTP_INDICATOR','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Gtp Indicator','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI#WC#HAZMAT_CONTAINER',201,'Msi#Wc#Hazmat Container','MSI#WC#HAZMAT_CONTAINER','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Hazmat Container','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI#WC#HAZMAT_DESCRIPTION',201,'Msi#Wc#Hazmat Description','MSI#WC#HAZMAT_DESCRIPTION','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Hazmat Description','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI#WC#PRISM_PART_NUMBER',201,'Msi#Wc#Prism Part Number','MSI#WC#PRISM_PART_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Prism Part Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI#WC#YEARLY_DC_VELOCITY',201,'Msi#Wc#Yearly Dc Velocity','MSI#WC#YEARLY_DC_VELOCITY','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Yearly Dc Velocity','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI#WC#YEARLY_STORE_VELOCITY',201,'Msi#Wc#Yearly Store Velocity','MSI#WC#YEARLY_STORE_VELOCITY','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Yearly Store Velocity','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI#WC#DC_VELOCITY',201,'Msi#Wc#Dc Velocity','MSI#WC#DC_VELOCITY','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Dc Velocity','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI#WC#STORE_VELOCITY',201,'Msi#Wc#Store Velocity','MSI#WC#STORE_VELOCITY','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Store Velocity','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI#WC#ORM_D_FLAG',201,'Msi#Wc#Orm D Flag','MSI#WC#ORM_D_FLAG','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Orm D Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI#WC#COUNTRY_OF_ORIGIN',201,'Msi#Wc#Country Of Origin','MSI#WC#COUNTRY_OF_ORIGIN','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Country Of Origin','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI#WC#CA_PROP_65',201,'Msi#Wc#Ca Prop 65','MSI#WC#CA_PROP_65','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Ca Prop 65','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI#HDS#SKU_DESCRIPTION',201,'Msi#Hds#Sku Description','MSI#HDS#SKU_DESCRIPTION','','','','ANONYMOUS','VARCHAR2','','','Msi#Hds#Sku Description','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI#HDS#UPC_PRIMARY',201,'Msi#Hds#Upc Primary','MSI#HDS#UPC_PRIMARY','','','','ANONYMOUS','VARCHAR2','','','Msi#Hds#Upc Primary','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI#HDS#UNSPSC_CODE',201,'Msi#Hds#Unspsc Code','MSI#HDS#UNSPSC_CODE','','','','ANONYMOUS','VARCHAR2','','','Msi#Hds#Unspsc Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI#HDS#VENDOR_PART_NUMBER',201,'Msi#Hds#Vendor Part Number','MSI#HDS#VENDOR_PART_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Msi#Hds#Vendor Part Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI#HDS#PRODUCT_ID',201,'Msi#Hds#Product Id','MSI#HDS#PRODUCT_ID','','','','ANONYMOUS','VARCHAR2','','','Msi#Hds#Product Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI#HDS#INVOICE_UOM',201,'Msi#Hds#Invoice Uom','MSI#HDS#INVOICE_UOM','','','','ANONYMOUS','VARCHAR2','','','Msi#Hds#Invoice Uom','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI#HDS#DROP_SHIPMENT',201,'Msi#Hds#Drop Shipment','MSI#HDS#DROP_SHIPMENT','','','','ANONYMOUS','VARCHAR2','','','Msi#Hds#Drop Shipment','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI#HDS#LOB',201,'Msi#Hds#Lob','MSI#HDS#LOB','','','','ANONYMOUS','VARCHAR2','','','Msi#Hds#Lob','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC#BRANCH',201,'Gcc#Branch','GCC#BRANCH','','','','ANONYMOUS','VARCHAR2','','','Gcc#Branch','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC1#BRANCH',201,'Gcc1#Branch','GCC1#BRANCH','','','','ANONYMOUS','VARCHAR2','','','Gcc1#Branch','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC2#BRANCH',201,'Gcc2#Branch','GCC2#BRANCH','','','','ANONYMOUS','VARCHAR2','','','Gcc2#Branch','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI_ORGANIZATION_ID',201,'Msi Organization Id','MSI_ORGANIZATION_ID','','','','ANONYMOUS','NUMBER','','','Msi Organization Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','PDT_ORG_ID',201,'Pdt Org Id','PDT_ORG_ID','','','','ANONYMOUS','NUMBER','','','Pdt Org Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','PDT_DOCUMENT_SUBTYPE',201,'Pdt Document Subtype','PDT_DOCUMENT_SUBTYPE','','','','ANONYMOUS','VARCHAR2','','','Pdt Document Subtype','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','PDT_DOCUMENT_TYPE_CODE',201,'Pdt Document Type Code','PDT_DOCUMENT_TYPE_CODE','','','','ANONYMOUS','VARCHAR2','','','Pdt Document Type Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','PSP_ORG_ID',201,'Psp Org Id','PSP_ORG_ID','','','','ANONYMOUS','NUMBER','','','Psp Org Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','PVC_VENDOR_SITE_ID',201,'Pvc Vendor Site Id','PVC_VENDOR_SITE_ID','','','','ANONYMOUS','NUMBER','','','Pvc Vendor Site Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','PVS_VENDOR_ID',201,'Pvs Vendor Id','PVS_VENDOR_ID','','','','ANONYMOUS','NUMBER','','','Pvs Vendor Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','CATEGORY_ID',201,'Category Id','CATEGORY_ID','','','','ANONYMOUS','NUMBER','','','Category Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','INVENTORY_ITEM_ID',201,'Inventory Item Id','INVENTORY_ITEM_ID','','','','ANONYMOUS','NUMBER','','','Inventory Item Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','TERM_ID',201,'Term Id','TERM_ID','','','','ANONYMOUS','NUMBER','','','Term Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','AGENT_ID',201,'Agent Id','AGENT_ID','','','','ANONYMOUS','NUMBER','','','Agent Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','EMPLOYEE_ID',201,'Employee Id','EMPLOYEE_ID','','','','ANONYMOUS','NUMBER','','','Employee Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','LINE_TYPE_ID',201,'Line Type Id','LINE_TYPE_ID','','','','ANONYMOUS','NUMBER','','','Line Type Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','VENDOR_CONTACT_ID',201,'Vendor Contact Id','VENDOR_CONTACT_ID','','','','ANONYMOUS','NUMBER','','','Vendor Contact Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','PO_RELEASE_ID',201,'Po Release Id','PO_RELEASE_ID','','','','ANONYMOUS','NUMBER','','','Po Release Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','LOCATION_ID',201,'Location Id','LOCATION_ID','','','','ANONYMOUS','NUMBER','','','Location Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','SET_OF_BOOKS_ID',201,'Set Of Books Id','SET_OF_BOOKS_ID','','','','ANONYMOUS','NUMBER','','','Set Of Books Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','SHIP_TO_LOCATION_ID',201,'Ship To Location Id','SHIP_TO_LOCATION_ID','','','','ANONYMOUS','NUMBER','','','Ship To Location Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','VENDOR_SITE_ID',201,'Vendor Site Id','VENDOR_SITE_ID','','','','ANONYMOUS','NUMBER','','','Vendor Site Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','VENDOR_ID',201,'Vendor Id','VENDOR_ID','','','','ANONYMOUS','NUMBER','','','Vendor Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','REQUISITION_LINE_ID',201,'Requisition Line Id','REQUISITION_LINE_ID','','','','ANONYMOUS','NUMBER','','','Requisition Line Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','REQUISITION_HEADER_ID',201,'Requisition Header Id','REQUISITION_HEADER_ID','','','','ANONYMOUS','NUMBER','','','Requisition Header Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','PREPARER_ID',201,'Preparer Id','PREPARER_ID','','','','ANONYMOUS','NUMBER','','','Preparer Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','ITEM_ID',201,'Item Id','ITEM_ID','','','','ANONYMOUS','NUMBER','','','Item Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','DESTINATION_ORGANIZATION_ID',201,'Destination Organization Id','DESTINATION_ORGANIZATION_ID','','','','ANONYMOUS','NUMBER','','','Destination Organization Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','SOURCE_ORGANIZATION_ID',201,'Source Organization Id','SOURCE_ORGANIZATION_ID','','','','ANONYMOUS','NUMBER','','','Source Organization Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','SOURCE_TYPE_CODE',201,'Source Type Code','SOURCE_TYPE_CODE','','','','ANONYMOUS','VARCHAR2','','','Source Type Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','CANCEL_FLAG',201,'Cancel Flag','CANCEL_FLAG','','','','ANONYMOUS','VARCHAR2','','','Cancel Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','REQ_QUANTITY_DELIVERED',201,'Req Quantity Delivered','REQ_QUANTITY_DELIVERED','','~T~D~2','','ANONYMOUS','NUMBER','','','Req Quantity Delivered','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','REQ_QUANTITY_CANCELLED',201,'Req Quantity Cancelled','REQ_QUANTITY_CANCELLED','','~T~D~2','','ANONYMOUS','NUMBER','','','Req Quantity Cancelled','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','QUANTITY',201,'Quantity','QUANTITY','','~T~D~2','','ANONYMOUS','NUMBER','','','Quantity','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','UNIT_PRICE',201,'Unit Price','UNIT_PRICE','','~T~D~2','','ANONYMOUS','NUMBER','','','Unit Price','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','REQ_LINE_CREATION_DATE',201,'Req Line Creation Date','REQ_LINE_CREATION_DATE','','','','ANONYMOUS','DATE','','','Req Line Creation Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','PO_DATA_TYPE',201,'Po Data Type','PO_DATA_TYPE','','','','ANONYMOUS','VARCHAR2','','','Po Data Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','VARIANCE_ACCOUNT',201,'Variance Account','VARIANCE_ACCOUNT','','','','ANONYMOUS','VARCHAR2','','','Variance Account','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','VARIANCE_CCID',201,'Variance Ccid','VARIANCE_CCID','','','','ANONYMOUS','NUMBER','','','Variance Ccid','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','ACCRUAL_ACCOUNT',201,'Accrual Account','ACCRUAL_ACCOUNT','','','','ANONYMOUS','VARCHAR2','','','Accrual Account','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','ACCRUAL_CCID',201,'Accrual Ccid','ACCRUAL_CCID','','','','ANONYMOUS','NUMBER','','','Accrual Ccid','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','PO_CHARGE_ACCOUNT',201,'Po Charge Account','PO_CHARGE_ACCOUNT','','','','ANONYMOUS','VARCHAR2','','','Po Charge Account','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','CHARGE_CCID',201,'Charge Ccid','CHARGE_CCID','','','','ANONYMOUS','NUMBER','','','Charge Ccid','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','COMPANY',201,'Company','COMPANY','','','','ANONYMOUS','VARCHAR2','','','Company','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','RATE',201,'Rate','RATE','','~T~D~2','','ANONYMOUS','NUMBER','','','Rate','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','SHIP_DIST',201,'Ship Dist','SHIP_DIST','','','','ANONYMOUS','VARCHAR2','','','Ship Dist','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','DISTRIBUTION_NUM',201,'Distribution Num','DISTRIBUTION_NUM','','','','ANONYMOUS','NUMBER','','','Distribution Num','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','DISTRIBUTION_ID',201,'Distribution Id','DISTRIBUTION_ID','','','','ANONYMOUS','NUMBER','','','Distribution Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','ACCRUE_ON_RECEIPT',201,'Accrue On Receipt','ACCRUE_ON_RECEIPT','','','','ANONYMOUS','VARCHAR2','','','Accrue On Receipt','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','PRICE_OVERRIDE',201,'Price Override','PRICE_OVERRIDE','','~T~D~2','','ANONYMOUS','NUMBER','','','Price Override','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','RECEIVE_CLOSE_TOLERANCE',201,'Receive Close Tolerance','RECEIVE_CLOSE_TOLERANCE','','','','ANONYMOUS','NUMBER','','','Receive Close Tolerance','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','INVOICE_CLOSE_TOLERANCE',201,'Invoice Close Tolerance','INVOICE_CLOSE_TOLERANCE','','','','ANONYMOUS','NUMBER','','','Invoice Close Tolerance','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','DAYS_LATE_RECEIPT_ALLOWED',201,'Days Late Receipt Allowed','DAYS_LATE_RECEIPT_ALLOWED','','','','ANONYMOUS','NUMBER','','','Days Late Receipt Allowed','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','DAYS_EARLY_RECEIPT_ALLOWED',201,'Days Early Receipt Allowed','DAYS_EARLY_RECEIPT_ALLOWED','','','','ANONYMOUS','NUMBER','','','Days Early Receipt Allowed','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','QTY_RCV_TOLERANCE',201,'Qty Rcv Tolerance','QTY_RCV_TOLERANCE','','~T~D~2','','ANONYMOUS','NUMBER','','','Qty Rcv Tolerance','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','PO_3_WAY_MATCH',201,'Po 3 Way Match','PO_3_WAY_MATCH','','','','ANONYMOUS','VARCHAR2','','','Po 3 Way Match','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','PO_4_WAY_MATCH',201,'Po 4 Way Match','PO_4_WAY_MATCH','','','','ANONYMOUS','VARCHAR2','','','Po 4 Way Match','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','SHIP_LAST_ACCEPT_DATE',201,'Ship Last Accept Date','SHIP_LAST_ACCEPT_DATE','','','','ANONYMOUS','DATE','','','Ship Last Accept Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','SHIP_PROMISED_DATE',201,'Ship Promised Date','SHIP_PROMISED_DATE','','','','ANONYMOUS','DATE','','','Ship Promised Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','SHIPMENT_TYPE',201,'Shipment Type','SHIPMENT_TYPE','','','','ANONYMOUS','VARCHAR2','','','Shipment Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','SHIPMENT_NUM',201,'Shipment Num','SHIPMENT_NUM','','','','ANONYMOUS','NUMBER','','','Shipment Num','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','LINE_LOCATION_ID',201,'Line Location Id','LINE_LOCATION_ID','','','','ANONYMOUS','NUMBER','','','Line Location Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','LINE_CLOSED_REASON',201,'Line Closed Reason','LINE_CLOSED_REASON','','','','ANONYMOUS','VARCHAR2','','','Line Closed Reason','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','LINE_CLOSED_DATE',201,'Line Closed Date','LINE_CLOSED_DATE','','','','ANONYMOUS','DATE','','','Line Closed Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','LINE_CLOSED_CODE',201,'Line Closed Code','LINE_CLOSED_CODE','','','','ANONYMOUS','VARCHAR2','','','Line Closed Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','TYPE_1099',201,'Type 1099','TYPE_1099','','','','ANONYMOUS','VARCHAR2','','','Type 1099','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','TAX_NAME',201,'Tax Name','TAX_NAME','','','','ANONYMOUS','VARCHAR2','','','Tax Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','TAXABLE_FLAG',201,'Taxable Flag','TAXABLE_FLAG','','','','ANONYMOUS','VARCHAR2','','','Taxable Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','LINE_CANCEL_REASON',201,'Line Cancel Reason','LINE_CANCEL_REASON','','','','ANONYMOUS','VARCHAR2','','','Line Cancel Reason','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','LINE_CANCEL_DATE',201,'Line Cancel Date','LINE_CANCEL_DATE','','','','ANONYMOUS','DATE','','','Line Cancel Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','LINE_CANCEL',201,'Line Cancel','LINE_CANCEL','','','','ANONYMOUS','VARCHAR2','','','Line Cancel','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MARKET_PRICE',201,'Market Price','MARKET_PRICE','','~T~D~2','','ANONYMOUS','NUMBER','','','Market Price','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','PO_HEADER_ID',201,'Po Header Id','PO_HEADER_ID','','','','ANONYMOUS','NUMBER','','','Po Header Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','LINE_LIST_PRICE',201,'Line List Price','LINE_LIST_PRICE','','~T~D~2','','ANONYMOUS','NUMBER','','','Line List Price','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','LINE_UNIT',201,'Line Unit','LINE_UNIT','','','','ANONYMOUS','VARCHAR2','','','Line Unit','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','ITEM_REVISION',201,'Item Revision','ITEM_REVISION','','','','ANONYMOUS','VARCHAR2','','','Item Revision','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','OPEN_FOR',201,'Open For','OPEN_FOR','','','','ANONYMOUS','VARCHAR2','','','Open For','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','OPEN_INVOICE_AMOUNT',201,'Open Invoice Amount','OPEN_INVOICE_AMOUNT','','~T~D~2','','ANONYMOUS','NUMBER','','','Open Invoice Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','AMOUNT_BILLED',201,'Amount Billed','AMOUNT_BILLED','','~T~D~2','','ANONYMOUS','NUMBER','','','Amount Billed','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','AMOUNT_CANCELLED',201,'Amount Cancelled','AMOUNT_CANCELLED','','~T~D~2','','ANONYMOUS','NUMBER','','','Amount Cancelled','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','QUANTITY_BILLED',201,'Quantity Billed','QUANTITY_BILLED','','~T~D~2','','ANONYMOUS','NUMBER','','','Quantity Billed','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','QUANTITY_CANCELLED',201,'Quantity Cancelled','QUANTITY_CANCELLED','','~T~D~2','','ANONYMOUS','NUMBER','','','Quantity Cancelled','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','CATEGORY',201,'Category','CATEGORY','','','','ANONYMOUS','VARCHAR2','','','Category','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','LINE_TYPE',201,'Line Type','LINE_TYPE','','','','ANONYMOUS','VARCHAR2','','','Line Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','LINE_CREATED_BY',201,'Line Created By','LINE_CREATED_BY','','','','ANONYMOUS','NUMBER','','','Line Created By','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','LINE_CREATION_DATE',201,'Line Creation Date','LINE_CREATION_DATE','','','','ANONYMOUS','DATE','','','Line Creation Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','LINE_LAST_UPDATED_BY',201,'Line Last Updated By','LINE_LAST_UPDATED_BY','','','','ANONYMOUS','NUMBER','','','Line Last Updated By','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','LINE_LAST_UPDATE_DATE',201,'Line Last Update Date','LINE_LAST_UPDATE_DATE','','','','ANONYMOUS','DATE','','','Line Last Update Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','LINE_ID',201,'Line Id','LINE_ID','','','','ANONYMOUS','NUMBER','','','Line Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','CLOSED_DATE',201,'Closed Date','CLOSED_DATE','','','','ANONYMOUS','DATE','','','Closed Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','CLOSED_CODE',201,'Closed Code','CLOSED_CODE','','','','ANONYMOUS','VARCHAR2','','','Closed Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','COMMENTS',201,'Comments','COMMENTS','','','','ANONYMOUS','VARCHAR2','','','Comments','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','NOTE_TO_SUPPLIER',201,'Note To Supplier','NOTE_TO_SUPPLIER','','','','ANONYMOUS','VARCHAR2','','','Note To Supplier','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','NOTE_TO_AUTHORIZER',201,'Note To Authorizer','NOTE_TO_AUTHORIZER','','','','ANONYMOUS','VARCHAR2','','','Note To Authorizer','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','ACCEPTANCE_REQUIRED',201,'Acceptance Required','ACCEPTANCE_REQUIRED','','','','ANONYMOUS','VARCHAR2','','','Acceptance Required','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','HOLD',201,'Hold','HOLD','','','','ANONYMOUS','VARCHAR2','','','Hold','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','CURRENCY_CODE',201,'Currency Code','CURRENCY_CODE','','','','ANONYMOUS','VARCHAR2','','','Currency Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','APPROVED_DATE',201,'Approved Date','APPROVED_DATE','','','','ANONYMOUS','DATE','','','Approved Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','PRINTED_DATE',201,'Printed Date','PRINTED_DATE','','','','ANONYMOUS','DATE','','','Printed Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','FREIGHT_TERMS',201,'Freight Terms','FREIGHT_TERMS','','','','ANONYMOUS','VARCHAR2','','','Freight Terms','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','FOB',201,'Fob','FOB','','','','ANONYMOUS','VARCHAR2','','','Fob','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','PO_TERMS',201,'Po Terms','PO_TERMS','','','','ANONYMOUS','VARCHAR2','','','Po Terms','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','SHIP_TO_LOCATION',201,'Ship To Location','SHIP_TO_LOCATION','','','','ANONYMOUS','VARCHAR2','','','Ship To Location','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','END_DATE',201,'End Date','END_DATE','','','','ANONYMOUS','DATE','','','End Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','START_DATE',201,'Start Date','START_DATE','','','','ANONYMOUS','DATE','','','Start Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','PO_ENABLED',201,'Po Enabled','PO_ENABLED','','','','ANONYMOUS','VARCHAR2','','','Po Enabled','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','PO_SUMMARY',201,'Po Summary','PO_SUMMARY','','','','ANONYMOUS','VARCHAR2','','','Po Summary','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','SUPPLIER_CONTACT_AREA_CODE',201,'Supplier Contact Area Code','SUPPLIER_CONTACT_AREA_CODE','','','','ANONYMOUS','VARCHAR2','','','Supplier Contact Area Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','SUPPLIER_CONTACT_LAST_NAME',201,'Supplier Contact Last Name','SUPPLIER_CONTACT_LAST_NAME','','','','ANONYMOUS','VARCHAR2','','','Supplier Contact Last Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','SUPPLIER_CONTACT_FIRST_NAME',201,'Supplier Contact First Name','SUPPLIER_CONTACT_FIRST_NAME','','','','ANONYMOUS','VARCHAR2','','','Supplier Contact First Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','RELEASE_NUM',201,'Release Num','RELEASE_NUM','','','','ANONYMOUS','NUMBER','','','Release Num','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','PO_NUMBER_RELEASE',201,'Po Number Release','PO_NUMBER_RELEASE','','','','ANONYMOUS','VARCHAR2','','','Po Number Release','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','CREATED_BY',201,'Created By','CREATED_BY','','','','ANONYMOUS','NUMBER','','','Created By','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','LAST_UPDATED_BY',201,'Last Updated By','LAST_UPDATED_BY','','','','ANONYMOUS','NUMBER','','','Last Updated By','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','LAST_UPDATE_DATE',201,'Last Update Date','LAST_UPDATE_DATE','','','','ANONYMOUS','DATE','','','Last Update Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC2#50368#SUBACCOUNT#DESCR',201,'Gcc2#50368#Subaccount#Descr','GCC2#50368#SUBACCOUNT#DESCR','','','','ANONYMOUS','VARCHAR2','','','Gcc2#50368#Subaccount#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI#WC#RESERVE_STOCK1',201,'Msi#Wc#Reserve Stock1','MSI#WC#RESERVE_STOCK1','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Reserve Stock1','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC2#50368#SUBACCOUNT',201,'Gcc2#50368#Subaccount','GCC2#50368#SUBACCOUNT','','','','ANONYMOUS','VARCHAR2','','','Gcc2#50368#Subaccount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC2#50368#PRODUCT#DESCR',201,'Gcc2#50368#Product#Descr','GCC2#50368#PRODUCT#DESCR','','','','ANONYMOUS','VARCHAR2','','','Gcc2#50368#Product#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC2#50368#PRODUCT',201,'Gcc2#50368#Product','GCC2#50368#PRODUCT','','','','ANONYMOUS','VARCHAR2','','','Gcc2#50368#Product','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC2#50368#FUTURE_USE#DESCR',201,'Gcc2#50368#Future Use#Descr','GCC2#50368#FUTURE_USE#DESCR','','','','ANONYMOUS','VARCHAR2','','','Gcc2#50368#Future Use#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC2#50368#FUTURE_USE',201,'Gcc2#50368#Future Use','GCC2#50368#FUTURE_USE','','','','ANONYMOUS','VARCHAR2','','','Gcc2#50368#Future Use','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC2#50368#DIVISION#DESCR',201,'Gcc2#50368#Division#Descr','GCC2#50368#DIVISION#DESCR','','','','ANONYMOUS','VARCHAR2','','','Gcc2#50368#Division#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC2#50368#DIVISION',201,'Gcc2#50368#Division','GCC2#50368#DIVISION','','','','ANONYMOUS','VARCHAR2','','','Gcc2#50368#Division','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC2#50368#DEPARTMENT#DESCR',201,'Gcc2#50368#Department#Descr','GCC2#50368#DEPARTMENT#DESCR','','','','ANONYMOUS','VARCHAR2','','','Gcc2#50368#Department#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC2#50368#DEPARTMENT',201,'Gcc2#50368#Department','GCC2#50368#DEPARTMENT','','','','ANONYMOUS','VARCHAR2','','','Gcc2#50368#Department','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC2#50368#ACCOUNT#DESCR',201,'Gcc2#50368#Account#Descr','GCC2#50368#ACCOUNT#DESCR','','','','ANONYMOUS','VARCHAR2','','','Gcc2#50368#Account#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC2#50368#ACCOUNT',201,'Gcc2#50368#Account','GCC2#50368#ACCOUNT','','','','ANONYMOUS','VARCHAR2','','','Gcc2#50368#Account','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC1#50368#SUBACCOUNT#DESCR',201,'Gcc1#50368#Subaccount#Descr','GCC1#50368#SUBACCOUNT#DESCR','','','','ANONYMOUS','VARCHAR2','','','Gcc1#50368#Subaccount#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC1#50368#SUBACCOUNT',201,'Gcc1#50368#Subaccount','GCC1#50368#SUBACCOUNT','','','','ANONYMOUS','VARCHAR2','','','Gcc1#50368#Subaccount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC1#50368#PRODUCT#DESCR',201,'Gcc1#50368#Product#Descr','GCC1#50368#PRODUCT#DESCR','','','','ANONYMOUS','VARCHAR2','','','Gcc1#50368#Product#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC1#50368#PRODUCT',201,'Gcc1#50368#Product','GCC1#50368#PRODUCT','','','','ANONYMOUS','VARCHAR2','','','Gcc1#50368#Product','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC1#50368#FUTURE_USE#DESCR',201,'Gcc1#50368#Future Use#Descr','GCC1#50368#FUTURE_USE#DESCR','','','','ANONYMOUS','VARCHAR2','','','Gcc1#50368#Future Use#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC1#50368#FUTURE_USE',201,'Gcc1#50368#Future Use','GCC1#50368#FUTURE_USE','','','','ANONYMOUS','VARCHAR2','','','Gcc1#50368#Future Use','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC1#50368#DIVISION#DESCR',201,'Gcc1#50368#Division#Descr','GCC1#50368#DIVISION#DESCR','','','','ANONYMOUS','VARCHAR2','','','Gcc1#50368#Division#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC1#50368#DIVISION',201,'Gcc1#50368#Division','GCC1#50368#DIVISION','','','','ANONYMOUS','VARCHAR2','','','Gcc1#50368#Division','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC1#50368#DEPARTMENT#DESCR',201,'Gcc1#50368#Department#Descr','GCC1#50368#DEPARTMENT#DESCR','','','','ANONYMOUS','VARCHAR2','','','Gcc1#50368#Department#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC1#50368#DEPARTMENT',201,'Gcc1#50368#Department','GCC1#50368#DEPARTMENT','','','','ANONYMOUS','VARCHAR2','','','Gcc1#50368#Department','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC1#50368#ACCOUNT#DESCR',201,'Gcc1#50368#Account#Descr','GCC1#50368#ACCOUNT#DESCR','','','','ANONYMOUS','VARCHAR2','','','Gcc1#50368#Account#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC1#50368#ACCOUNT',201,'Gcc1#50368#Account','GCC1#50368#ACCOUNT','','','','ANONYMOUS','VARCHAR2','','','Gcc1#50368#Account','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC#50368#SUBACCOUNT#DESCR',201,'Gcc#50368#Subaccount#Descr','GCC#50368#SUBACCOUNT#DESCR','','','','ANONYMOUS','VARCHAR2','','','Gcc#50368#Subaccount#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC#50368#SUBACCOUNT',201,'Gcc#50368#Subaccount','GCC#50368#SUBACCOUNT','','','','ANONYMOUS','VARCHAR2','','','Gcc#50368#Subaccount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC#50368#PRODUCT#DESCR',201,'Gcc#50368#Product#Descr','GCC#50368#PRODUCT#DESCR','','','','ANONYMOUS','VARCHAR2','','','Gcc#50368#Product#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC#50368#PRODUCT',201,'Gcc#50368#Product','GCC#50368#PRODUCT','','','','ANONYMOUS','VARCHAR2','','','Gcc#50368#Product','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC#50368#FUTURE_USE#DESCR',201,'Gcc#50368#Future Use#Descr','GCC#50368#FUTURE_USE#DESCR','','','','ANONYMOUS','VARCHAR2','','','Gcc#50368#Future Use#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC#50368#FUTURE_USE',201,'Gcc#50368#Future Use','GCC#50368#FUTURE_USE','','','','ANONYMOUS','VARCHAR2','','','Gcc#50368#Future Use','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC#50368#DIVISION#DESCR',201,'Gcc#50368#Division#Descr','GCC#50368#DIVISION#DESCR','','','','ANONYMOUS','VARCHAR2','','','Gcc#50368#Division#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC#50368#DIVISION',201,'Gcc#50368#Division','GCC#50368#DIVISION','','','','ANONYMOUS','VARCHAR2','','','Gcc#50368#Division','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC#50368#DEPARTMENT#DESCR',201,'Gcc#50368#Department#Descr','GCC#50368#DEPARTMENT#DESCR','','','','ANONYMOUS','VARCHAR2','','','Gcc#50368#Department#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC#50368#DEPARTMENT',201,'Gcc#50368#Department','GCC#50368#DEPARTMENT','','','','ANONYMOUS','VARCHAR2','','','Gcc#50368#Department','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC#50368#ACCOUNT#DESCR',201,'Gcc#50368#Account#Descr','GCC#50368#ACCOUNT#DESCR','','','','ANONYMOUS','VARCHAR2','','','Gcc#50368#Account#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC#50368#ACCOUNT',201,'Gcc#50368#Account','GCC#50368#ACCOUNT','','','','ANONYMOUS','VARCHAR2','','','Gcc#50368#Account','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC2#50328#PROJECT_CODE#DESCR',201,'Gcc2#50328#Project Code#Descr','GCC2#50328#PROJECT_CODE#DESCR','','','','ANONYMOUS','VARCHAR2','','','Gcc2#50328#Project Code#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC2#50328#PROJECT_CODE',201,'Gcc2#50328#Project Code','GCC2#50328#PROJECT_CODE','','','','ANONYMOUS','VARCHAR2','','','Gcc2#50328#Project Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC2#50328#PRODUCT#DESCR',201,'Gcc2#50328#Product#Descr','GCC2#50328#PRODUCT#DESCR','','','','ANONYMOUS','VARCHAR2','','','Gcc2#50328#Product#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC2#50328#PRODUCT',201,'Gcc2#50328#Product','GCC2#50328#PRODUCT','','','','ANONYMOUS','VARCHAR2','','','Gcc2#50328#Product','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC2#50328#LOCATION#DESCR',201,'Gcc2#50328#Location#Descr','GCC2#50328#LOCATION#DESCR','','','','ANONYMOUS','VARCHAR2','','','Gcc2#50328#Location#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC2#50328#LOCATION',201,'Gcc2#50328#Location','GCC2#50328#LOCATION','','','','ANONYMOUS','VARCHAR2','','','Gcc2#50328#Location','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC2#50328#FUTURE_USE_2#DESCR',201,'Gcc2#50328#Future Use 2#Descr','GCC2#50328#FUTURE_USE_2#DESCR','','','','ANONYMOUS','VARCHAR2','','','Gcc2#50328#Future Use 2#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC2#50328#FUTURE_USE_2',201,'Gcc2#50328#Future Use 2','GCC2#50328#FUTURE_USE_2','','','','ANONYMOUS','VARCHAR2','','','Gcc2#50328#Future Use 2','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC2#50328#FURTURE_USE#DESCR',201,'Gcc2#50328#Furture Use#Descr','GCC2#50328#FURTURE_USE#DESCR','','','','ANONYMOUS','VARCHAR2','','','Gcc2#50328#Furture Use#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC2#50328#FURTURE_USE',201,'Gcc2#50328#Furture Use','GCC2#50328#FURTURE_USE','','','','ANONYMOUS','VARCHAR2','','','Gcc2#50328#Furture Use','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC2#50328#COST_CENTER#DESCR',201,'Gcc2#50328#Cost Center#Descr','GCC2#50328#COST_CENTER#DESCR','','','','ANONYMOUS','VARCHAR2','','','Gcc2#50328#Cost Center#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC2#50328#COST_CENTER',201,'Gcc2#50328#Cost Center','GCC2#50328#COST_CENTER','','','','ANONYMOUS','VARCHAR2','','','Gcc2#50328#Cost Center','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC2#50328#ACCOUNT#DESCR',201,'Gcc2#50328#Account#Descr','GCC2#50328#ACCOUNT#DESCR','','','','ANONYMOUS','VARCHAR2','','','Gcc2#50328#Account#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC2#50328#ACCOUNT',201,'Gcc2#50328#Account','GCC2#50328#ACCOUNT','','','','ANONYMOUS','VARCHAR2','','','Gcc2#50328#Account','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC1#50328#PROJECT_CODE#DESCR',201,'Gcc1#50328#Project Code#Descr','GCC1#50328#PROJECT_CODE#DESCR','','','','ANONYMOUS','VARCHAR2','','','Gcc1#50328#Project Code#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC1#50328#PROJECT_CODE',201,'Gcc1#50328#Project Code','GCC1#50328#PROJECT_CODE','','','','ANONYMOUS','VARCHAR2','','','Gcc1#50328#Project Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC1#50328#PRODUCT#DESCR',201,'Gcc1#50328#Product#Descr','GCC1#50328#PRODUCT#DESCR','','','','ANONYMOUS','VARCHAR2','','','Gcc1#50328#Product#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC1#50328#PRODUCT',201,'Gcc1#50328#Product','GCC1#50328#PRODUCT','','','','ANONYMOUS','VARCHAR2','','','Gcc1#50328#Product','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC1#50328#LOCATION#DESCR',201,'Gcc1#50328#Location#Descr','GCC1#50328#LOCATION#DESCR','','','','ANONYMOUS','VARCHAR2','','','Gcc1#50328#Location#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC1#50328#LOCATION',201,'Gcc1#50328#Location','GCC1#50328#LOCATION','','','','ANONYMOUS','VARCHAR2','','','Gcc1#50328#Location','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC1#50328#FUTURE_USE_2#DESCR',201,'Gcc1#50328#Future Use 2#Descr','GCC1#50328#FUTURE_USE_2#DESCR','','','','ANONYMOUS','VARCHAR2','','','Gcc1#50328#Future Use 2#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC1#50328#FUTURE_USE_2',201,'Gcc1#50328#Future Use 2','GCC1#50328#FUTURE_USE_2','','','','ANONYMOUS','VARCHAR2','','','Gcc1#50328#Future Use 2','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC1#50328#FURTURE_USE#DESCR',201,'Gcc1#50328#Furture Use#Descr','GCC1#50328#FURTURE_USE#DESCR','','','','ANONYMOUS','VARCHAR2','','','Gcc1#50328#Furture Use#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC1#50328#FURTURE_USE',201,'Gcc1#50328#Furture Use','GCC1#50328#FURTURE_USE','','','','ANONYMOUS','VARCHAR2','','','Gcc1#50328#Furture Use','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC1#50328#COST_CENTER#DESCR',201,'Gcc1#50328#Cost Center#Descr','GCC1#50328#COST_CENTER#DESCR','','','','ANONYMOUS','VARCHAR2','','','Gcc1#50328#Cost Center#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC1#50328#COST_CENTER',201,'Gcc1#50328#Cost Center','GCC1#50328#COST_CENTER','','','','ANONYMOUS','VARCHAR2','','','Gcc1#50328#Cost Center','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC1#50328#ACCOUNT#DESCR',201,'Gcc1#50328#Account#Descr','GCC1#50328#ACCOUNT#DESCR','','','','ANONYMOUS','VARCHAR2','','','Gcc1#50328#Account#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC1#50328#ACCOUNT',201,'Gcc1#50328#Account','GCC1#50328#ACCOUNT','','','','ANONYMOUS','VARCHAR2','','','Gcc1#50328#Account','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC#50328#PROJECT_CODE#DESCR',201,'Gcc#50328#Project Code#Descr','GCC#50328#PROJECT_CODE#DESCR','','','','ANONYMOUS','VARCHAR2','','','Gcc#50328#Project Code#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC#50328#PROJECT_CODE',201,'Gcc#50328#Project Code','GCC#50328#PROJECT_CODE','','','','ANONYMOUS','VARCHAR2','','','Gcc#50328#Project Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC#50328#PRODUCT#DESCR',201,'Gcc#50328#Product#Descr','GCC#50328#PRODUCT#DESCR','','','','ANONYMOUS','VARCHAR2','','','Gcc#50328#Product#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC#50328#PRODUCT',201,'Gcc#50328#Product','GCC#50328#PRODUCT','','','','ANONYMOUS','VARCHAR2','','','Gcc#50328#Product','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC#50328#LOCATION#DESCR',201,'Gcc#50328#Location#Descr','GCC#50328#LOCATION#DESCR','','','','ANONYMOUS','VARCHAR2','','','Gcc#50328#Location#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC#50328#LOCATION',201,'Gcc#50328#Location','GCC#50328#LOCATION','','','','ANONYMOUS','VARCHAR2','','','Gcc#50328#Location','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC#50328#FUTURE_USE_2#DESCR',201,'Gcc#50328#Future Use 2#Descr','GCC#50328#FUTURE_USE_2#DESCR','','','','ANONYMOUS','VARCHAR2','','','Gcc#50328#Future Use 2#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC#50328#FUTURE_USE_2',201,'Gcc#50328#Future Use 2','GCC#50328#FUTURE_USE_2','','','','ANONYMOUS','VARCHAR2','','','Gcc#50328#Future Use 2','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC#50328#FURTURE_USE#DESCR',201,'Gcc#50328#Furture Use#Descr','GCC#50328#FURTURE_USE#DESCR','','','','ANONYMOUS','VARCHAR2','','','Gcc#50328#Furture Use#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC#50328#FURTURE_USE',201,'Gcc#50328#Furture Use','GCC#50328#FURTURE_USE','','','','ANONYMOUS','VARCHAR2','','','Gcc#50328#Furture Use','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC#50328#COST_CENTER#DESCR',201,'Gcc#50328#Cost Center#Descr','GCC#50328#COST_CENTER#DESCR','','','','ANONYMOUS','VARCHAR2','','','Gcc#50328#Cost Center#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC#50328#COST_CENTER',201,'Gcc#50328#Cost Center','GCC#50328#COST_CENTER','','','','ANONYMOUS','VARCHAR2','','','Gcc#50328#Cost Center','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC#50328#ACCOUNT#DESCR',201,'Gcc#50328#Account#Descr','GCC#50328#ACCOUNT#DESCR','','','','ANONYMOUS','VARCHAR2','','','Gcc#50328#Account#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','GCC#50328#ACCOUNT',201,'Gcc#50328#Account','GCC#50328#ACCOUNT','','','','ANONYMOUS','VARCHAR2','','','Gcc#50328#Account','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','PV#PRISM#PRISM_SUPPLIER_NUM',201,'Pv#Prism#Prism Supplier Num','PV#PRISM#PRISM_SUPPLIER_NUM','','','','ANONYMOUS','VARCHAR2','','','Pv#Prism#Prism Supplier Num','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','PV#LEGACY#LEGACY_SUPPLIER_NU',201,'Pv#Legacy#Legacy Supplier Nu','PV#LEGACY#LEGACY_SUPPLIER_NU','','','','ANONYMOUS','VARCHAR2','','','Pv#Legacy#Legacy Supplier Nu','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI#WC#HAZMAT_PACKAGING_GROU1',201,'Msi#Wc#Hazmat Packaging Grou1','MSI#WC#HAZMAT_PACKAGING_GROU1','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Hazmat Packaging Grou1','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI#WC#MSDS_#1',201,'Msi#Wc#Msds #1','MSI#WC#MSDS_#1','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Msds #1','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI#WC#VOC_SUB_CATEGORY1',201,'Msi#Wc#Voc Sub Category1','MSI#WC#VOC_SUB_CATEGORY1','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Voc Sub Category1','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI#WC#VOC_CATEGORY1',201,'Msi#Wc#Voc Category1','MSI#WC#VOC_CATEGORY1','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Voc Category1','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI#WC#PESTICIDE_FLAG_STATE1',201,'Msi#Wc#Pesticide Flag State1','MSI#WC#PESTICIDE_FLAG_STATE1','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Pesticide Flag State1','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI#WC#VOC_GL1',201,'Msi#Wc#Voc Gl1','MSI#WC#VOC_GL1','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Voc Gl1','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI#WC#CALC_LEAD_TIME1',201,'Msi#Wc#Calc Lead Time1','MSI#WC#CALC_LEAD_TIME1','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Calc Lead Time1','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI#WC#PESTICIDE_FLAG1',201,'Msi#Wc#Pesticide Flag1','MSI#WC#PESTICIDE_FLAG1','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Pesticide Flag1','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI#WC#KEEP_ITEM_ACTIVE1',201,'Msi#Wc#Keep Item Active1','MSI#WC#KEEP_ITEM_ACTIVE1','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Keep Item Active1','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI#WC#IMPORT_DUTY_1',201,'Msi#Wc#Import Duty 1','MSI#WC#IMPORT_DUTY_1','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Import Duty 1','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI#WC#PRODUCT_CODE1',201,'Msi#Wc#Product Code1','MSI#WC#PRODUCT_CODE1','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Product Code1','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI#WC#AVERAGE_UNITS1',201,'Msi#Wc#Average Units1','MSI#WC#AVERAGE_UNITS1','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Average Units1','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','MSI#WC#TAXWARE_CODE1',201,'Msi#Wc#Taxware Code1','MSI#WC#TAXWARE_CODE1','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Taxware Code1','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','SHIP_CLOSED_CODE',201,'Ship Closed Code','SHIP_CLOSED_CODE','','','','ANONYMOUS','VARCHAR2','','','Ship Closed Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','SEQ#',201,'Seq#','SEQ#','','','','ANONYMOUS','NUMBER','','','Seq#','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_V','ACC_DUE_DATE',201,'Acc Due Date','ACC_DUE_DATE','','','','ANONYMOUS','DATE','','','Acc Due Date','','','','US');
--Inserting Object Components for EIS_XXWC_PO_OPEN_ORDERS_V
--Inserting Object Component Joins for EIS_XXWC_PO_OPEN_ORDERS_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 201');
END IF;
END;
/
prompt Creating Report LOV Data for Open Purchase Orders Listing - WC
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(201);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - Open Purchase Orders Listing - WC
xxeis.eis_rsc_ins.lov( 201,'select agent_name buyer_name from po_agents_v','','EIS_PO_BUYER_LOV','List of Values for Buyer','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'select poh.segment1 PO_NUMBER,HOU.name OPERATING_UNIT FROM PO_HEADERS_ALL POH,HR_OPERATING_UNITS HOU
where POH.ORG_ID=HOU.ORGANIZATION_ID
and exists(select 1 from XXEIS.EIS_MO_ORG_TMP_V
            WHERE ORG_ID=POH.ORG_ID)','','EIS_PO_PURCHASE_ORDER_NUM_LOV','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'select vendor_name from po_vendors','','EIS_PO_SUPPLIER_LOV','','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'SELECT hou.NAME organization_name,
       TO_CHAR (date_from,
                NVL (fnd_profile.VALUE (''ICX_DATE_FORMAT_MASK''),
                     ''DD-MON-YYYY'')
               ) date_from,
       TO_CHAR (date_to,
                NVL (fnd_profile.VALUE (''ICX_DATE_FORMAT_MASK''),
                     ''DD-MON-YYYY'')
               ) date_to
  FROM hr_operating_units hou,
       mo_glob_org_access_tmp tmp
 WHERE hou.organization_id = tmp.organization_id','','EIS_MULTI_OPERATING_UNIT_LOV','This LOV shows all the operating units, to which user has access.','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( '','SELECT ORGANIZATION_CODE WAREHOUSE,ORGANIZATION_NAME
FROM ORG_ORGANIZATION_DEFINITIONS OOD
WHERE SYSDATE < NVL(OOD.DISABLE_DATE,SYSDATE+1) AND EXISTS (SELECT 1 FROM XXEIS.EIS_ORG_ACCESS_V  WHERE organization_id = ood.organization_id )
UNION
SELECT ''All'', ''All Organizations'' FROM DUAL','','XXWC All ORG LIST','XXWC All ORG LIST','ANONYMOUS',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 201');
END IF;
END;
/
prompt Creating Report Data for Open Purchase Orders Listing - WC
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(201);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Open Purchase Orders Listing - WC
xxeis.eis_rsc_utility.delete_report_rows( 'Open Purchase Orders Listing - WC' );
--Inserting Report - Open Purchase Orders Listing - WC
xxeis.eis_rsc_ins.r( 201,'Open Purchase Orders Listing - WC','','White Cap Open Purchase Orders Listing - WC Report','','','','SA059956','EIS_XXWC_PO_OPEN_ORDERS_V','Y','','','SA059956','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N','','','','','','','','US','','','','');
--Inserting Report Columns - Open Purchase Orders Listing - WC
xxeis.eis_rsc_ins.rc( 'Open Purchase Orders Listing - WC',201,'AMOUNT_DUE','Amount Due','Amount Due','','~T~D~0','default','','32','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_OPEN_ORDERS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Open Purchase Orders Listing - WC',201,'AMOUNT_ORDERED','Amount Ordered','Amount Ordered','','~T~D~0','default','','30','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_OPEN_ORDERS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Open Purchase Orders Listing - WC',201,'AMOUNT_RECEIVED','Amount Received','Amount Received','','~T~D~0','default','','31','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_OPEN_ORDERS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Open Purchase Orders Listing - WC',201,'BUYER_NAME','Buyer Name','Buyer Name','','','default','','6','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_OPEN_ORDERS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Open Purchase Orders Listing - WC',201,'ITEM','Item','Item','','','default','','21','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_OPEN_ORDERS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Open Purchase Orders Listing - WC',201,'ITEM_DESCRIPTION','Item Description','Item Description','','','default','','22','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_OPEN_ORDERS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Open Purchase Orders Listing - WC',201,'LINE_NUM','Line','Line Num','','~~~','default','','40','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_OPEN_ORDERS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Open Purchase Orders Listing - WC',201,'NOTE_TO_RECEIVER','Note To Receiver','Note To Receiver','','','default','','11','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_OPEN_ORDERS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Open Purchase Orders Listing - WC',201,'PO_CREATION_DATE','Po Creation Date','Po Creation Date','','','default','','17','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_OPEN_ORDERS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Open Purchase Orders Listing - WC',201,'PO_NUMBER','Po Number','Po Number','','','default','','10','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_OPEN_ORDERS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Open Purchase Orders Listing - WC',201,'PO_STATUS','Po Status','Po Status','','','default','','24','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_OPEN_ORDERS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Open Purchase Orders Listing - WC',201,'PO_SUPPLIER_NAME','Po Supplier Name','Po Supplier Name','','','default','','8','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_OPEN_ORDERS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Open Purchase Orders Listing - WC',201,'PO_TYPE','Po Type','Po Type','','','default','','36','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_OPEN_ORDERS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Open Purchase Orders Listing - WC',201,'PO_UNIT_PRICE','Po Price','Po Unit Price','','$~T~D~2','default','','29','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_OPEN_ORDERS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Open Purchase Orders Listing - WC',201,'QUANTITY_DUE','Qty Due','Quantity Due','','~T~D~2','default','','27','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_OPEN_ORDERS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Open Purchase Orders Listing - WC',201,'QUANTITY_ORDERED','Qty Ordered','Quantity Ordered','','~T~D~2','default','','25','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_OPEN_ORDERS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Open Purchase Orders Listing - WC',201,'QUANTITY_RECEIVED','Qty Received','Quantity Received','','~T~D~2','default','','26','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_OPEN_ORDERS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Open Purchase Orders Listing - WC',201,'REQUIRED_BY','Required By','Required By','','','default','','41','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_OPEN_ORDERS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Open Purchase Orders Listing - WC',201,'REQ_CREATED_ON','Req Created On','Req Created On','','','default','','43','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_OPEN_ORDERS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Open Purchase Orders Listing - WC',201,'REQ_NUM','Req Num','Req Num','','','default','','42','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_OPEN_ORDERS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Open Purchase Orders Listing - WC',201,'REV','Rev','Rev','','~~~','default','','39','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_OPEN_ORDERS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Open Purchase Orders Listing - WC',201,'REVISED_DATE','Po Revision Date','Revised Date','','','default','','18','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_OPEN_ORDERS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Open Purchase Orders Listing - WC',201,'SUPPLIER_SITE','Supplier Site','Supplier Site','','','default','','38','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_OPEN_ORDERS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Open Purchase Orders Listing - WC',201,'UNIT_MEAS_LOOKUP_CODE','UOM','Unit Meas Lookup Code','','','default','','37','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_OPEN_ORDERS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Open Purchase Orders Listing - WC',201,'SHIP_NEED_BY_DATE','Line Need By Date','Ship Need By Date','','','default','','19','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_OPEN_ORDERS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Open Purchase Orders Listing - WC',201,'ORG_PO_LINES','Org PO Lines','Org Po Lines','','~~~','default','','28','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_OPEN_ORDERS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Open Purchase Orders Listing - WC',201,'HEADER_NEED_BY_DATE','Header Need By Date','Header Need By Date','','','default','','20','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_OPEN_ORDERS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Open Purchase Orders Listing - WC',201,'LATE','Late','Header Need By Date','VARCHAR2','','default','','44','Y','Y','','','','','','CASE WHEN trunc(EXPOOV.LINE_PROMISE_DATE) < TRUNC(SYSDATE) THEN ''Y'' ELSE ''N'' END','SA059956','N','N','','EIS_XXWC_PO_OPEN_ORDERS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Open Purchase Orders Listing - WC',201,'SHIP_VIA','Ship Via','Ship Via','','','default','','23','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_OPEN_ORDERS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Open Purchase Orders Listing - WC',201,'SUPPLIER_CONTACT_PHONE','Supplier Contact Phone','Supplier Contact Phone','','','default','','9','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_OPEN_ORDERS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Open Purchase Orders Listing - WC',201,'SHIP_TO','Ship To','Ship To','','','default','','2','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_OPEN_ORDERS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Open Purchase Orders Listing - WC',201,'HEADER_PROMISE_DATE','Header Promise Date','Header Promise Date','','','default','','4','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_OPEN_ORDERS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Open Purchase Orders Listing - WC',201,'LINE_PROMISE_DATE','Line Promise Date','Line Promise Date','','','default','','5','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_OPEN_ORDERS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Open Purchase Orders Listing - WC',201,'DIRECT/DROP - Y/N','DIRECT/DROP - Y/N','Line Promise Date','VARCHAR2','','default','','3','Y','Y','','','','','','case when (EXPOOV.ATTRIBUTE2= ''DIRECT'' AND EXPOOV.ORG_CODE <> EXPOOV.SHIP_TO) OR EXPOOV.SHIP_TO IS NULL Then ''Y'' ELSE ''N'' END','SA059956','N','N','','EIS_XXWC_PO_OPEN_ORDERS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Open Purchase Orders Listing - WC',201,'ORG_CODE','Org Code','Org Code','','','default','','1','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_OPEN_ORDERS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Open Purchase Orders Listing - WC',201,'ACCEPTANCE_DUE_DATE','Acceptance Due Date','Acceptance Due Date','','','default','','13','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_OPEN_ORDERS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Open Purchase Orders Listing - WC',201,'ACCEPTANCE_TYPE','Latest Acceptance Type','Acceptance Type','','','default','','16','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_OPEN_ORDERS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Open Purchase Orders Listing - WC',201,'ACCEPTED_BY','Accepted By (buyer name)','Accepted By','','','default','','15','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_OPEN_ORDERS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Open Purchase Orders Listing - WC',201,'ACCEPTED_FLAG','Acceptance Applied Y/N','Accepted Flag','','','default','','12','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_OPEN_ORDERS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Open Purchase Orders Listing - WC',201,'ACTION_DATE','Date Acceptance Applied','Action Date','','','default','','14','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_OPEN_ORDERS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Open Purchase Orders Listing - WC',201,'BIN1','Bin1','Bin1','','','default','','33','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_OPEN_ORDERS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Open Purchase Orders Listing - WC',201,'BIN2','Bin2','Bin2','','','default','','34','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_OPEN_ORDERS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Open Purchase Orders Listing - WC',201,'BIN3','Bin3','Bin3','','','default','','35','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_OPEN_ORDERS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Open Purchase Orders Listing - WC',201,'BUYER_GROUP','Group','Buyer Group','','','default','','7','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_OPEN_ORDERS_V','','','','US','');
--Inserting Report Parameters - Open Purchase Orders Listing - WC
xxeis.eis_rsc_ins.rp( 'Open Purchase Orders Listing - WC',201,'Operating Unit','Operating Unit','OPERATING_UNIT','IN','EIS_MULTI_OPERATING_UNIT_LOV','HDS White Cap - Org','VARCHAR2','N','Y','2','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_PO_OPEN_ORDERS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Open Purchase Orders Listing - WC',201,'Organization','Organization','ORG_CODE','IN','XXWC All ORG LIST','','VARCHAR2','Y','Y','1','Y','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_PO_OPEN_ORDERS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Open Purchase Orders Listing - WC',201,'PO Date From','PO Date From','PO_CREATION_DATE','>=','','','DATE','N','Y','3','Y','Y','CONSTANT','SA059956','Y','N','','Start Date','','EIS_XXWC_PO_OPEN_ORDERS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Open Purchase Orders Listing - WC',201,'PO Date To','PO Date To','PO_CREATION_DATE','<=','','','DATE','N','Y','4','Y','Y','CONSTANT','SA059956','Y','N','','End Date','','EIS_XXWC_PO_OPEN_ORDERS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Open Purchase Orders Listing - WC',201,'Supplier','Supplier','PO_SUPPLIER_NAME','IN','EIS_PO_SUPPLIER_LOV','','VARCHAR2','N','Y','5','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_PO_OPEN_ORDERS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Open Purchase Orders Listing - WC',201,'PO Number From','PO Number From','PO_NUMBER','>=','EIS_PO_PURCHASE_ORDER_NUM_LOV','','VARCHAR2','N','Y','6','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_PO_OPEN_ORDERS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Open Purchase Orders Listing - WC',201,'PO Number To','PO Number To','PO_NUMBER','<=','EIS_PO_PURCHASE_ORDER_NUM_LOV','','VARCHAR2','N','Y','7','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_PO_OPEN_ORDERS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Open Purchase Orders Listing - WC',201,'Buyer','Buyer','BUYER_NAME','IN','EIS_PO_BUYER_LOV','','VARCHAR2','N','Y','8','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_PO_OPEN_ORDERS_V','','','US','');
--Inserting Dependent Parameters - Open Purchase Orders Listing - WC
--Inserting Report Conditions - Open Purchase Orders Listing - WC
xxeis.eis_rsc_ins.rcnh( 'Open Purchase Orders Listing - WC',201,'Free Text ','FREE_TEXT','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','','Y','','','','','AND EXPOOV.QUANTITY_DUE > 0
AND (EXPOOV.PO_STATUS NOT IN( ''Closed For Invoice'' ,''Closed For Receiving''))
AND EXPOOV.SHIP_CLOSED_CODE != ''CLOSED FOR RECEIVING''
AND ( ''All'' IN (:Organization) OR (ORG_CODE IN (:Organization)))','1',201,'Open Purchase Orders Listing - WC','Free Text ');
xxeis.eis_rsc_ins.rcnh( 'Open Purchase Orders Listing - WC',201,'EXPOOV.OPERATING_UNIT IN Operating Unit','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','OPERATING_UNIT','','Operating Unit','','','','','EIS_XXWC_PO_OPEN_ORDERS_V','','','','','','IN','Y','Y','','','','','1',201,'Open Purchase Orders Listing - WC','EXPOOV.OPERATING_UNIT IN Operating Unit');
xxeis.eis_rsc_ins.rcnh( 'Open Purchase Orders Listing - WC',201,'EXPOOV.PO_CREATION_DATE >= PO Date From','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','PO_CREATION_DATE','','PO Date From','','','','','EIS_XXWC_PO_OPEN_ORDERS_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',201,'Open Purchase Orders Listing - WC','EXPOOV.PO_CREATION_DATE >= PO Date From');
xxeis.eis_rsc_ins.rcnh( 'Open Purchase Orders Listing - WC',201,'EXPOOV.PO_CREATION_DATE <= PO Date To','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','PO_CREATION_DATE','','PO Date To','','','','','EIS_XXWC_PO_OPEN_ORDERS_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',201,'Open Purchase Orders Listing - WC','EXPOOV.PO_CREATION_DATE <= PO Date To');
xxeis.eis_rsc_ins.rcnh( 'Open Purchase Orders Listing - WC',201,'EXPOOV.PO_SUPPLIER_NAME IN Supplier','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','PO_SUPPLIER_NAME','','Supplier','','','','','EIS_XXWC_PO_OPEN_ORDERS_V','','','','','','IN','Y','Y','','','','','1',201,'Open Purchase Orders Listing - WC','EXPOOV.PO_SUPPLIER_NAME IN Supplier');
xxeis.eis_rsc_ins.rcnh( 'Open Purchase Orders Listing - WC',201,'EXPOOV.PO_NUMBER >= PO Number From','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','PO_NUMBER','','PO Number From','','','','','EIS_XXWC_PO_OPEN_ORDERS_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',201,'Open Purchase Orders Listing - WC','EXPOOV.PO_NUMBER >= PO Number From');
xxeis.eis_rsc_ins.rcnh( 'Open Purchase Orders Listing - WC',201,'EXPOOV.PO_NUMBER <= PO Number To','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','PO_NUMBER','','PO Number To','','','','','EIS_XXWC_PO_OPEN_ORDERS_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',201,'Open Purchase Orders Listing - WC','EXPOOV.PO_NUMBER <= PO Number To');
xxeis.eis_rsc_ins.rcnh( 'Open Purchase Orders Listing - WC',201,'EXPOOV.BUYER_NAME IN Buyer','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','BUYER_NAME','','Buyer','','','','','EIS_XXWC_PO_OPEN_ORDERS_V','','','','','','IN','Y','Y','','','','','1',201,'Open Purchase Orders Listing - WC','EXPOOV.BUYER_NAME IN Buyer');
--Inserting Report Sorts - Open Purchase Orders Listing - WC
xxeis.eis_rsc_ins.rs( 'Open Purchase Orders Listing - WC',201,'PO_NUMBER','','SA059956','1','');
--Inserting Report Triggers - Open Purchase Orders Listing - WC
--inserting report templates - Open Purchase Orders Listing - WC
--Inserting Report Portals - Open Purchase Orders Listing - WC
xxeis.eis_rsc_ins.r_port( 'Open Purchase Orders Listing - WC','XXWC_PUR_TOP_RPTS','201','Open Purchase Orders Listing - WC','Open Purchase Order report','OA.jsp?page=/eis/oracle/apps/xxeis/central/reporting/webui/EISRSCLaunchPG&EisProduct=Reporting&Portal=Yes&mod=Purchasing','','Pivot Excel,EXCEL,','CONC','N','SA059956');
--inserting report dashboards - Open Purchase Orders Listing - WC
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Open Purchase Orders Listing - WC','201','EIS_XXWC_PO_OPEN_ORDERS_V','EIS_XXWC_PO_OPEN_ORDERS_V','N','');
--inserting report security - Open Purchase Orders Listing - WC
xxeis.eis_rsc_ins.rsec( 'Open Purchase Orders Listing - WC','201','','XXWC_PUR_SUPER_USER',201,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Open Purchase Orders Listing - WC','201','','HDS_PRCHSNG_SPR_USR',201,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Open Purchase Orders Listing - WC','201','','XXWC_PURCHASING_SR_MRG_WC',201,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Open Purchase Orders Listing - WC','201','','XXWC_PURCHASING_MGR',201,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Open Purchase Orders Listing - WC','201','','XXWC_PURCHASING_INQUIRY',201,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Open Purchase Orders Listing - WC','201','','XXWC_PURCHASING_BUYER',201,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Open Purchase Orders Listing - WC','201','','XXWC_AO_PO_AUTOCREATE',201,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Open Purchase Orders Listing - WC','','FC004766','',201,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Open Purchase Orders Listing - WC','20005','','XXWC_VIEW_ALL_EIS_REPORTS',201,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Open Purchase Orders Listing - WC','','10011678','',201,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Open Purchase Orders Listing - WC','200','','XXWC_PAY_W_CALENDAR',201,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Open Purchase Orders Listing - WC','200','','XXWC_PAY_NO_CALENDAR',201,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Open Purchase Orders Listing - WC','201','','XXWC_PURCHASING_RPM',201,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Open Purchase Orders Listing - WC','','SG019472','',201,'SA059956','','','');
--Inserting Report Pivots - Open Purchase Orders Listing - WC
xxeis.eis_rsc_ins.rpivot( 'Open Purchase Orders Listing - WC',201,'Open PO''S By Line Count','1','2,0|1,1,0','1,1,1,0|None|2');
--Inserting Report Pivot Details For Pivot - Open PO'S By Line Count
xxeis.eis_rsc_ins.rpivot_dtls( 'Open Purchase Orders Listing - WC',201,'Open PO''S By Line Count','BUYER_NAME','PAGE_FIELD','','','1','','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Open Purchase Orders Listing - WC',201,'Open PO''S By Line Count','PO_SUPPLIER_NAME','ROW_FIELD','','','3','0','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Open Purchase Orders Listing - WC',201,'Open PO''S By Line Count','PO_NUMBER','ROW_FIELD','','','2','0','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Open Purchase Orders Listing - WC',201,'Open PO''S By Line Count','NOTE_TO_RECEIVER','ROW_FIELD','','','6','0','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Open Purchase Orders Listing - WC',201,'Open PO''S By Line Count','LATE','PAGE_FIELD','','','2','','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Open Purchase Orders Listing - WC',201,'Open PO''S By Line Count','ORG_PO_LINES','DATA_FIELD','COUNT','Count of PO Lines','1','','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Open Purchase Orders Listing - WC',201,'Open PO''S By Line Count','PO_STATUS','PAGE_FIELD','','','3','','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Open Purchase Orders Listing - WC',201,'Open PO''S By Line Count','SUPPLIER_CONTACT_PHONE','ROW_FIELD','','','4','0','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Open Purchase Orders Listing - WC',201,'Open PO''S By Line Count','LINE_PROMISE_DATE','ROW_FIELD','','','1','','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Open Purchase Orders Listing - WC',201,'Open PO''S By Line Count','ACCEPTED_FLAG','ROW_FIELD','','','5','','');
--Inserting Report Summary Calculation Columns For Pivot- Open PO'S By Line Count
xxeis.eis_rsc_ins.rpivot( 'Open Purchase Orders Listing - WC',201,'Open PO''s By Qty Received','2','2,0|1,1,0','1,1,1,0|None|2');
--Inserting Report Pivot Details For Pivot - Open PO's By Qty Received
xxeis.eis_rsc_ins.rpivot_dtls( 'Open Purchase Orders Listing - WC',201,'Open PO''s By Qty Received','BUYER_NAME','PAGE_FIELD','','','1','','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Open Purchase Orders Listing - WC',201,'Open PO''s By Qty Received','LATE','PAGE_FIELD','','','2','','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Open Purchase Orders Listing - WC',201,'Open PO''s By Qty Received','QUANTITY_ORDERED','DATA_FIELD','SUM','','1','','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Open Purchase Orders Listing - WC',201,'Open PO''s By Qty Received','QUANTITY_RECEIVED','DATA_FIELD','SUM','','2','','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Open Purchase Orders Listing - WC',201,'Open PO''s By Qty Received','PO_NUMBER','ROW_FIELD','','','2','0','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Open Purchase Orders Listing - WC',201,'Open PO''s By Qty Received','PO_SUPPLIER_NAME','ROW_FIELD','','','3','0','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Open Purchase Orders Listing - WC',201,'Open PO''s By Qty Received','NOTE_TO_RECEIVER','ROW_FIELD','','','6','0','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Open Purchase Orders Listing - WC',201,'Open PO''s By Qty Received','PO_STATUS','PAGE_FIELD','','','3','','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Open Purchase Orders Listing - WC',201,'Open PO''s By Qty Received','SUPPLIER_CONTACT_PHONE','ROW_FIELD','','','4','0','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Open Purchase Orders Listing - WC',201,'Open PO''s By Qty Received','SHIP_VIA','PAGE_FIELD','','','4','','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Open Purchase Orders Listing - WC',201,'Open PO''s By Qty Received','LINE_PROMISE_DATE','ROW_FIELD','','','1','','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Open Purchase Orders Listing - WC',201,'Open PO''s By Qty Received','ACCEPTED_FLAG','ROW_FIELD','','','5','','');
--Inserting Report Summary Calculation Columns For Pivot- Open PO's By Qty Received
--Inserting Report   Version details- Open Purchase Orders Listing - WC
xxeis.eis_rsc_ins.rv( 'Open Purchase Orders Listing - WC','','Open Purchase Orders Listing - WC','SA059956');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 201');
END IF;
END;
/
