--Report Name            : Product Fill Rate Detail Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View EIS_XXWC_PO_FILL_RATE_DET_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_PO_FILL_RATE_DET_V
Create or replace View XXEIS.EIS_XXWC_PO_FILL_RATE_DET_V
 AS 
SELECT
    /*+ no_parallel(isr) no_parallel(tmp) no_parallel(msi) no_parallel(ol) no_parallel(oh) */
    oh.order_number,
    DECODE (OL.OPTION_NUMBER,'',(OL.LINE_NUMBER
    ||'.'
    ||OL.SHIPMENT_NUMBER),(OL.LINE_NUMBER
    ||'.'
    ||OL.SHIPMENT_NUMBER
    ||'.'
    ||OL.OPTION_NUMBER)) Sales_Order_Line_Number,
    msi.segment1 item,
    user_item_description user_item_description,
    CASE
      WHEN (OL.SPLIT_FROM_LINE_ID IS NOT NULL
      OR msi.Item_type             = 'SPECIAL'
      OR ol.source_type_code       = 'EXTERNAL' )
      THEN NULL
      ELSE 1
    END Total_Lines,
    CASE
      WHEN ((user_item_description ='BACKORDERED'
      OR user_item_description     ='PARTIAL BACKORDERED')
      AND msi.Item_type           != 'SPECIAL'
      AND ol.source_type_code     != 'EXTERNAL')
      THEN 1
      ELSE NULL
    END BACKORDERED_LINE,
    isr.region,
    isr.district,
    isr.org,
    ffv.attribute1 DPM_RPM,
    ol.flow_status_code Status
  FROM XXEIS.EIS_XXWC_PO_ISR_TAB isr,
    XXEIS.EIS_XXWC_FILL_RATE_TMP_TBL tmp,
    apps.mtl_system_items_b_kfv msi,
    apps.oe_order_lines_all ol,
    apps.oe_order_headers_all oh,
    apps.fnd_flex_value_sets ffvs,
    apps.fnd_flex_values ffv
  WHERE 1                             =1
  AND isr.organization_id             = ol.ship_from_org_id
  AND isr.inventory_item_id           = ol.inventory_item_id
  AND msi.organization_id             = isr.organization_id
  AND msi.inventory_item_id           = isr.inventory_item_id
  AND ol.flow_status_code            != 'CANCELLED'
  AND flex_value_set_name             = 'XXWC_DISTRICT'
  AND msi.Item_type                  != 'SPECIAL'
  AND ol.source_type_code            != 'EXTERNAL'
  AND msi.inventory_item_status_code != 'Intangible'
  AND ffvs.flex_value_set_id          = ffv.flex_value_set_id
  AND isr.district                    = ffv.flex_value
  AND tmp.header_id                   = oh.header_id
  AND tmp.process_id                  = xxeis.eis_inv_xxwc_prodfill_rate_pkg.get_process_id
  AND ol.header_id                    = tmp.header_id
  AND NOT EXISTS
    (SELECT 1
    FROM OE_ORDER_LINES_all ol1
    WHERE ol1.header_id             = ol.header_id
    AND TRUNC(actual_shipment_date) < xxeis.eis_rs_xxwc_com_util_pkg.get_date_from
    )
/
set scan on define on
prompt Creating View Data for Product Fill Rate Detail Report
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_PO_FILL_RATE_DET_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_PO_FILL_RATE_DET_V',201,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Xxwc Po Fill Rate Det V','EXPFRDV','','');
--Delete View Columns for EIS_XXWC_PO_FILL_RATE_DET_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_PO_FILL_RATE_DET_V',201,FALSE);
--Inserting View Columns for EIS_XXWC_PO_FILL_RATE_DET_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_FILL_RATE_DET_V','STATUS',201,'Status','STATUS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Status','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_FILL_RATE_DET_V','DPM_RPM',201,'Dpm Rpm','DPM_RPM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Dpm Rpm','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_FILL_RATE_DET_V','ORG',201,'Org','ORG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Org','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_FILL_RATE_DET_V','DISTRICT',201,'District','DISTRICT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','District','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_FILL_RATE_DET_V','REGION',201,'Region','REGION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Region','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_FILL_RATE_DET_V','BACKORDERED_LINE',201,'Backordered Line','BACKORDERED_LINE','','','','XXEIS_RS_ADMIN','NUMBER','','','Backordered Line','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_FILL_RATE_DET_V','TOTAL_LINES',201,'Total Lines','TOTAL_LINES','','','','XXEIS_RS_ADMIN','NUMBER','','','Total Lines','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_FILL_RATE_DET_V','USER_ITEM_DESCRIPTION',201,'User Item Description','USER_ITEM_DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','User Item Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_FILL_RATE_DET_V','ITEM',201,'Item','ITEM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Item','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_FILL_RATE_DET_V','SALES_ORDER_LINE_NUMBER',201,'Sales Order Line Number','SALES_ORDER_LINE_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Sales Order Line Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_FILL_RATE_DET_V','ORDER_NUMBER',201,'Order Number','ORDER_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Order Number','','','');
--Inserting View Components for EIS_XXWC_PO_FILL_RATE_DET_V
--Inserting View Component Joins for EIS_XXWC_PO_FILL_RATE_DET_V
END;
/
set scan on define on
prompt Creating Report LOV Data for Product Fill Rate Detail Report
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Product Fill Rate Detail Report
xxeis.eis_rs_ins.lov( 201,'SELECT ood.organization_code organization_code,ood.organization_name organization_name  FROM ORG_ORGANIZATION_DEFINITIONS OOD WHERE EXISTS(SELECT 1 FROM XXEIS.EIS_ORG_ACCESS_V WHERE organization_id = ood.organization_id) ORDER BY organization_code','','PO Organization Lov','','XXEIS_RS_ADMIN',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Product Fill Rate Detail Report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Product Fill Rate Detail Report
xxeis.eis_rs_utility.delete_report_rows( 'Product Fill Rate Detail Report' );
--Inserting Report - Product Fill Rate Detail Report
xxeis.eis_rs_ins.r( 201,'Product Fill Rate Detail Report','','','','','','XXEIS_RS_ADMIN','EIS_XXWC_PO_FILL_RATE_DET_V','Y','','','XXEIS_RS_ADMIN','','N','White Cap Reports','RTF,PDF,','CSV,HTML,Html Summary,XML,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - Product Fill Rate Detail Report
xxeis.eis_rs_ins.rc( 'Product Fill Rate Detail Report',201,'BACKORDERED_LINE','Backordered Line','Backordered Line','','~~~','default','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_FILL_RATE_DET_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Detail Report',201,'DISTRICT','District','District','','','default','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_FILL_RATE_DET_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Detail Report',201,'DPM_RPM','DPM RPM','Dpm Rpm','','','default','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_FILL_RATE_DET_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Detail Report',201,'ITEM','Item','Item','','','default','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_FILL_RATE_DET_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Detail Report',201,'ORDER_NUMBER','Order Number','Order Number','','~~~','default','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_FILL_RATE_DET_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Detail Report',201,'ORG','Org','Org','','','default','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_FILL_RATE_DET_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Detail Report',201,'REGION','Region','Region','','','default','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_FILL_RATE_DET_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Detail Report',201,'SALES_ORDER_LINE_NUMBER','Line Number','Sales Order Line Number','','','default','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_FILL_RATE_DET_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Detail Report',201,'STATUS','Status','Status','','','default','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_FILL_RATE_DET_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Detail Report',201,'TOTAL_LINES','Total Lines','Total Lines','','~~~','default','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_FILL_RATE_DET_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Detail Report',201,'USER_ITEM_DESCRIPTION','User Item Description','User Item Description','','','default','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_FILL_RATE_DET_V','','');
--Inserting Report Parameters - Product Fill Rate Detail Report
xxeis.eis_rs_ins.rp( 'Product Fill Rate Detail Report',201,'Location','Location','ORG','IN','PO Organization Lov','','VARCHAR2','N','Y','1','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Product Fill Rate Detail Report',201,'Ship Date','','','IN','','select trunc(decode(to_char(sysdate,''dy''),''mon'',sysdate-3,''sun'',sysdate-2,sysdate-1)) from dual','DATE','Y','Y','2','','N','SQL','XXEIS_RS_ADMIN','Y','N','','','');
--Inserting Report Conditions - Product Fill Rate Detail Report
xxeis.eis_rs_ins.rcn( 'Product Fill Rate Detail Report',201,'ORG','IN',':Location','','','Y','1','Y','XXEIS_RS_ADMIN');
--Inserting Report Sorts - Product Fill Rate Detail Report
xxeis.eis_rs_ins.rs( 'Product Fill Rate Detail Report',201,'ORDER_NUMBER','ASC','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rs( 'Product Fill Rate Detail Report',201,'SALES_ORDER_LINE_NUMBER','ASC','XXEIS_RS_ADMIN','','');
--Inserting Report Triggers - Product Fill Rate Detail Report
xxeis.eis_rs_ins.rt( 'Product Fill Rate Detail Report',201,'begin
xxeis.eis_rs_xxwc_com_util_pkg.g_date_from := :Ship Date;
xxeis.eis_inv_xxwc_prodfill_rate_pkg.LOAD_ORDER_INFO(P_PROCESS_ID  =>:SYSTEM.PROCESS_ID);
end;','B','Y','XXEIS_RS_ADMIN');
--Inserting Report Templates - Product Fill Rate Detail Report
--Inserting Report Portals - Product Fill Rate Detail Report
--Inserting Report Dashboards - Product Fill Rate Detail Report
--Inserting Report Security - Product Fill Rate Detail Report
xxeis.eis_rs_ins.rsec( 'Product Fill Rate Detail Report','201','','50983',201,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Product Fill Rate Detail Report','201','','50621',201,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Product Fill Rate Detail Report','201','','50893',201,'XXEIS_RS_ADMIN','','');
--Inserting Report Pivots - Product Fill Rate Detail Report
END;
/
set scan on define on
