--Report Name            : Excess Receipts - WC
--Import Instructions : 1. If RTF template needs to be uploaded to report automatically then move the RTF template to physical path of APPS_DATA_FILE_DIR oracle directory. Check dba_directories view to get the physical path of oracle directory. 
--                         (If you want to use any other oracle directory other than APPS_DATA_FILE_DIR then replace APPS_DATA_FILE_DIR with your oracle directory name below.)
--                      2. Make sure XXEIS schema has read/write permissions to above used oracle directory and give full permissions to the directory at OS level.
--                      3. Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
exec xxeis.eis_rs_ins.g_rtf_dir := 'APPS_DATA_FILE_DIR' ;
--Creating View EIS_XXWC_PO_EXCESS_RECEIPTS_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_PO_EXCESS_RECEIPTS_V
Create or replace View XXEIS.EIS_XXWC_PO_EXCESS_RECEIPTS_V
(PO_HEADER_ID,PO_NUMBER,PO_LINE_NUMBER,RCV_RECEIVER,PO_LOCATION,RCV_RECEIPT_NUMBER,RCV_PACKING_SLIP,RCV_RECEIPT_DATE,SUPPLIER_NAME,SUPPLIER_NUMBER,SUPPLIER_SITE_CODE,RCV_RECEIPT_QUANTITY,PO_QUANTITY_ORDERED,PO_QUANTITY_CANCELLED,PO_QUANTITY_RECEIVED_TO_DATE,PO_QUANTITY_DUE,PO_QUANTITY_BILLED,RCV_NEED_BY_DATE,LINE_TYPE,DESCRIPTION,UNIT,RCV_PROMISED_DATE,RCV_LAST_ACCEPT_DATE,RCV_TRANSACTION_ID,RCV_CREATED_BY,RCV_CREATION_DATE,RCV_LAST_UPDATED_BY,RCV_LAST_UPDATE_DATE,RCV_TRANSACTION_TYPE,TRANSACTION_TYPE,PARENT_TRANSACTION_TYPE,RCV_UNIT_OF_MEASURE,RCV_USER_ENTERED,RCV_INTERFACE_SOURCE,RCV_SOURCE_DOCUMENT,RCV_DESTINATION_TYPE,RCV_PRIMARY_QUANTITY,RCV_PRIMARY_UNIT_OF_MEASURE,RCV_UOM_CODE,RCV_PO_UNIT_PRICE,RCV_DESTINATION_CONTEXT,RCV_SOURCE_DOC_UOM,RCV_SOURCE_DOC_QTY,QUALITY_CODE,COMMENTS,RCV_RECEIPT_SOURCE,RCV_SHIPMENT_NUM,ITEM,ORG,ORG_CODE,TRANSACTION_REASON,LOCATION_ID,INVENTORY_ITEM_ID,ORGANIZATION_ID,RCT1_TRANSACTION_ID,RSH_SHIPMENT_HEADER_ID,MTR_REASON_ID,POV_VENDOR_ID,PVS_VENDOR_SITE_ID,POL_PO_LINE_ID,PLL_LINE_LOCATION_ID,POH_PO_HEADER_ID,HRE_EMPLOYEE_ID,PLT_LINE_TYPE_ID,POH#STANDARDP#NEED_BY_DATE,POH#STANDARDP#FREIGHT_TERMS_,POH#STANDARDP#CARRIER_TERMS_,POH#STANDARDP#FOB_TERMS_TAB,POV#R11VENDOR#R11_VENDOR_ID,POV#TAXEXEMPT#EXEMPTIONS,PVS#IWOFIPS#FIPS_CODE,PVS#R11VENDOR#R11_VENDOR_SIT,PVS#R11VENDOR#R11_VENDOR_ID,RSH#PRINTED) AS 
SELECT poh.po_header_id,
    poh.segment1 po_number,
    pol.line_num po_line_number,
    hre.full_name rcv_receiver,
    hrl.location_code po_location,
    rsh.receipt_num rcv_receipt_number,
    RSH.PACKING_SLIP RCV_PACKING_SLIP,
    rt.transaction_date rcv_receipt_date,
    pov.vendor_name supplier_name,
    pov.segment1 supplier_number,
    pvs.vendor_site_code supplier_site_code,
    rt.quantity rcv_receipt_quantity,
    NVL (pll.quantity, 0) po_quantity_ordered,
    NVL (pll.quantity_cancelled, 0) po_quantity_cancelled,
    NVL (pll.quantity_received, 0) po_quantity_received_to_date,
    NVL (pll.quantity, 0) - NVL (pll.quantity_cancelled, 0) - NVL (pll.quantity_received, 0) po_quantity_due,
    pll.quantity_billed po_quantity_billed,
    pll.need_by_date rcv_need_by_date,
    plt.line_type line_type,
    pol.item_description description,
    POL.UNIT_MEAS_LOOKUP_CODE UNIT,
    pll.promised_date rcv_promised_date,
    pll.last_accept_date rcv_last_accept_date,
    rt.transaction_id rcv_transaction_id,
    rt.created_by rcv_created_by,
    rt.creation_date rcv_creation_date,
    rt.last_updated_by rcv_last_updated_by,
    rt.last_update_date rcv_last_update_date,
    rt.transaction_type rcv_transaction_type,
    plc.displayed_field transaction_type,
    NULL parent_transaction_type,
    rt.unit_of_measure rcv_unit_of_measure,
    DECODE (rt.user_entered_flag, 'Y', 'Yes', 'N', 'No' ) rcv_user_entered,
    rt.interface_source_code rcv_interface_source,
    rt.source_document_code rcv_source_document,
    rt.destination_type_code rcv_destination_type,
    rt.primary_quantity rcv_primary_quantity,
    rt.primary_unit_of_measure rcv_primary_unit_of_measure,
    rt.uom_code rcv_uom_code,
    rt.po_unit_price rcv_po_unit_price,
    rt.destination_context rcv_destination_context,
    rt.source_doc_unit_of_measure rcv_source_doc_uom,
    rt.source_doc_quantity rcv_source_doc_qty,
    rt.inspection_quality_code quality_code,
    rt.comments comments,
    rsh.receipt_source_code rcv_receipt_source,
    rsh.shipment_num rcv_shipment_num,
    msi.concatenated_segments item,
    --   mca.concatenated_segments item_category,
    ood.organization_name org,
    ood.organization_code org_code,
    mtr.reason_name transaction_reason,
    -- Added PK Columns
    hrl.location_id location_id,
    msi.inventory_item_id inventory_item_id,
    msi.organization_id organization_id,
    -- primary keys added for component joins
    rt.transaction_id rct1_transaction_id,
    rsh.shipment_header_id rsh_shipment_header_id,
    mtr.reason_id mtr_reason_id,
    pov.vendor_id pov_vendor_id,
    pvs.vendor_site_id pvs_vendor_site_id,
    pol.po_line_id pol_po_line_id,
    pll.line_location_id pll_line_location_id,
    poh.po_header_id poh_po_header_id,
    hre.employee_id hre_employee_id,
    plt.line_type_id plt_line_type_id
    --,msi.inventory_item_id MSI_INVENTORY_ITEM_ID,
    -- mca.category_id mca_category_id
    --descr#flexfield#start
    ,
    DECODE(POH.ATTRIBUTE_CATEGORY ,'Standard Purchase Order',POH.ATTRIBUTE1, NULL) POH#StandardP#Need_By_Date ,
    DECODE(POH.ATTRIBUTE_CATEGORY ,'Standard Purchase Order',POH.ATTRIBUTE2, NULL) POH#StandardP#Freight_Terms_ ,
    DECODE(POH.ATTRIBUTE_CATEGORY ,'Standard Purchase Order',POH.ATTRIBUTE3, NULL) POH#StandardP#Carrier_TERMS_ ,
    DECODE(POH.ATTRIBUTE_CATEGORY ,'Standard Purchase Order',POH.ATTRIBUTE4, NULL) POH#StandardP#FOB_TERMS_Tab ,
    DECODE(POV.ATTRIBUTE_CATEGORY ,'R11 Vendor ID',POV.ATTRIBUTE15, NULL) POV#R11Vendor#R11_Vendor_ID ,
    DECODE(POV.ATTRIBUTE_CATEGORY ,'Tax Exempt Flag',POV.ATTRIBUTE1, NULL) POV#TaxExempt#Exemptions ,
    DECODE(PVS.ATTRIBUTE_CATEGORY ,'IWO FIPS',PVS.ATTRIBUTE1, NULL) PVS#IWOFIPS#FIPS_Code ,
    DECODE(PVS.ATTRIBUTE_CATEGORY ,'R11 Vendor IDs',PVS.ATTRIBUTE14, NULL) PVS#R11Vendor#R11_Vendor_Sit ,
    DECODE(PVS.ATTRIBUTE_CATEGORY ,'R11 Vendor IDs',PVS.ATTRIBUTE15, NULL) PVS#R11Vendor#R11_Vendor_ID ,
    RSH.ATTRIBUTE1 RSH#Printed
    --descr#flexfield#end
    --gl#accountff#start
    --gl#accountff#end
  FROM po_line_locations pll, --44880
    rcv_transactions rt,
    po_headers poh,
    po_lookup_codes plc2,
    -- rcv_transactions rct1,
    -- po_lookup_codes plc1,
    po_lookup_codes plc,
    hr_employees hre,
    rcv_shipment_headers rsh,
    mtl_transaction_reasons mtr,
    org_organization_definitions ood,
    po_lines pol,
    po_line_types plt,
    MTL_SYSTEM_ITEMS_KFV MSI,
    -- MTL_CATEGORIES_KFV MCA,
    hr_locations hrl,
    po_vendors pov,
    po_vendor_sites pvs,
    hr_operating_units hou,
    gl_sets_of_books gsob,
    po_document_types pdt
  WHERE NVL (pll.quantity, 0) - NVL (pll.quantity_cancelled, 0) < NVL (pll.quantity_received, 0)
  AND pll.line_location_id                                      = rt.po_line_location_id
  AND rt.transaction_type                                      IN ('RECEIVE', 'RETURN TO VENDOR', 'CORRECT', 'RETURN TO RECEIVING')
  AND poh.po_header_id                                          = pll.po_header_id
  AND plc2.lookup_type                                          = 'RCV TRANSACTION TYPE'
  AND plc2.lookup_code                                          = rt.transaction_type
    -- AND rct1.transaction_id                                       = DECODE (rt.parent_transaction_id, NULL, rt.transaction_id, -1, rt.transaction_id, rt.parent_transaction_id )
    -- AND plc1.lookup_type                                          = 'RCV TRANSACTION TYPE'
    -- AND plc1.lookup_code                                          = rct1.transaction_type
  AND plc.lookup_type        = 'REQUISITION SOURCE TYPE'
  AND plc.lookup_code        = 'VENDOR'
  AND rt.employee_id         = hre.employee_id(+)
  AND rsh.shipment_header_id = rt.shipment_header_id
  AND rt.reason_id           = mtr.reason_id(+)
  AND ood.organization_id    = rt.organization_id
  AND rt.po_line_location_id = pll.line_location_id
  AND poh.po_header_id       = pol.po_header_id
  AND pol.po_line_id         = pll.po_line_id
  AND pol.line_type_id       = plt.line_type_id
  AND POL.ITEM_ID            = MSI.INVENTORY_ITEM_ID
  AND rt.organization_id     = MSI.ORGANIZATION_ID
    --AND POL.CATEGORY_ID                                           = MCA.CATEGORY_ID
  AND pll.ship_to_location_id = hrl.location_id
  AND poh.vendor_id           = pov.vendor_id
  AND poh.vendor_site_id      = pvs.vendor_site_id
  AND hou.organization_id     = poh.org_id
  AND gsob.set_of_books_id    = hou.set_of_books_id
  AND poh.type_lookup_code    = pdt.document_subtype
  AND PDT.DOCUMENT_TYPE_CODE IN ('PO', 'PA')
  AND pdt.org_id              =poh.org_id
/
set scan on define on
prompt Creating View Data for Excess Receipts - WC
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_PO_EXCESS_RECEIPTS_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V',201,'This view provides purchase order receipt information.','','','','XXEIS_RS_ADMIN','XXEIS','Eis Po Excess Receipts V','EPERV','','');
--Delete View Columns for EIS_XXWC_PO_EXCESS_RECEIPTS_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_PO_EXCESS_RECEIPTS_V',201,FALSE);
--Inserting View Columns for EIS_XXWC_PO_EXCESS_RECEIPTS_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','RCV_RECEIPT_DATE',201,'PO Receipt Date','RCV_RECEIPT_DATE','','','','XXEIS_RS_ADMIN','DATE','RCV_TRANSACTIONS','TRANSACTION_DATE','Rcv Receipt Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','SUPPLIER_NAME',201,'Supplier name','SUPPLIER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_VENDORS','VENDOR_NAME','Supplier Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','SUPPLIER_NUMBER',201,'Supplier number','SUPPLIER_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_VENDORS','SEGMENT1','Supplier Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','SUPPLIER_SITE_CODE',201,'Site code name','SUPPLIER_SITE_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_VENDOR_SITES_ALL','VENDOR_SITE_CODE','Supplier Site Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','RCV_RECEIPT_QUANTITY',201,'PO Receipt Quantity','RCV_RECEIPT_QUANTITY','','','','XXEIS_RS_ADMIN','NUMBER','RCV_TRANSACTIONS','QUANTITY','Rcv Receipt Quantity','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','PO_QUANTITY_ORDERED',201,'PO Quantity Ordered','PO_QUANTITY_ORDERED','','','','XXEIS_RS_ADMIN','NUMBER','PO_LINE_LOCATIONS_ALL','QUANTITY','PO Quantity Ordered','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','PO_QUANTITY_CANCELLED',201,'PO Quantity Cancelled','PO_QUANTITY_CANCELLED','','','','XXEIS_RS_ADMIN','NUMBER','PO_LINE_LOCATIONS_ALL','QUANTITY_CANCELLED','PO Quantity Cancelled','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','PO_QUANTITY_RECEIVED_TO_DATE',201,'PO Quantity Received To Date','PO_QUANTITY_RECEIVED_TO_DATE','','','','XXEIS_RS_ADMIN','NUMBER','PO_LINE_LOCATIONS_ALL','QUANTITY_RECEIVED','PO Quantity Received To Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','PO_QUANTITY_DUE',201,'PO Quantity Due','PO_QUANTITY_DUE','','','','XXEIS_RS_ADMIN','NUMBER','Calculation Column','Calculation Column','PO Quantity Due','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','PO_QUANTITY_BILLED',201,'PO Quantity Billed','PO_QUANTITY_BILLED','','','','XXEIS_RS_ADMIN','NUMBER','PO_LINE_LOCATIONS_ALL','QUANTITY_BILLED','PO Quantity Billed','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','RCV_PROMISED_DATE',201,'PO Receipt Promised Date','RCV_PROMISED_DATE','','','','XXEIS_RS_ADMIN','DATE','PO_LINE_LOCATIONS_ALL','PROMISED_DATE','Rcv Promised Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','PO_NUMBER',201,'PO Number','PO_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_HEADERS_ALL','SEGMENT1','PO Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','PO_LINE_NUMBER',201,'Line number','PO_LINE_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','PO_LINES_ALL','LINE_NUM','PO Line Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','RCV_RECEIVER',201,'PO Receipt Receiver','RCV_RECEIVER','','','','XXEIS_RS_ADMIN','VARCHAR2','PER_ALL_PEOPLE_F','FULL_NAME','Rcv Receiver','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','PO_LOCATION',201,'PO Location','PO_LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','HR_LOCATIONS_ALL','LOCATION_CODE','PO Location','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','RCV_RECEIPT_NUMBER',201,'PO Receipt Number','RCV_RECEIPT_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','RCV_SHIPMENT_HEADERS','RECEIPT_NUMBER','Rcv Receipt Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','RCV_PACKING_SLIP',201,'PO Receipt Packing Slip','RCV_PACKING_SLIP','','','','XXEIS_RS_ADMIN','VARCHAR2','RCV_SHIPMENT_HEADERS','PACKING_SLIP','Rcv Packing Slip','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','RCV_PO_UNIT_PRICE',201,'PO Receipt Po Unit Price','RCV_PO_UNIT_PRICE','','','','XXEIS_RS_ADMIN','NUMBER','RCV_TRANSACTIONS','PO_UNIT_PRICE','Rcv PO Unit Price','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','RCV_RECEIPT_SOURCE',201,'PO Receipt Source','RCV_RECEIPT_SOURCE','','','','XXEIS_RS_ADMIN','VARCHAR2','RCV_SHIPMENT_HEADERS','RECEIPT_SOURCE_CODE','Rcv Receipt Source','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','RCV_PRIMARY_UNIT_OF_MEASURE',201,'PO Receipt Primary Unit Of Measure','RCV_PRIMARY_UNIT_OF_MEASURE','','','','XXEIS_RS_ADMIN','VARCHAR2','RCV_TRANSACTIONS','PRIMARY_UNIT_OF_MEASURE','Rcv Primary Unit Of Measure','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','RCV_TRANSACTION_TYPE',201,'PO Receipt Transaction Type','RCV_TRANSACTION_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','RCV_TRANSACTIONS','TRANSACTION_TYPE','Rcv Transaction Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','RCV_PRIMARY_QUANTITY',201,'Transaction quantity in terms of the item''s primary unit of measure','RCV_PRIMARY_QUANTITY','','','','XXEIS_RS_ADMIN','NUMBER','RCV_TRANSACTIONS','PRIMARY_QUANTITY','Rcv Primary Quantity','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','RCV_SHIPMENT_NUM',201,'PO Receipt Shipment Num','RCV_SHIPMENT_NUM','','','','XXEIS_RS_ADMIN','VARCHAR2','RCV_SHIPMENT_HEADERS','SHIPMENT_NUM','Rcv Shipment Num','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','LOCATION_ID',201,'Location Id','LOCATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Location Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','COMMENTS',201,'Comments','COMMENTS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Comments','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','DESCRIPTION',201,'Description','DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','LINE_TYPE',201,'Line Type','LINE_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Line Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','ORG',201,'Org','ORG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Org','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','PARENT_TRANSACTION_TYPE',201,'Parent Transaction Type','PARENT_TRANSACTION_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Parent Transaction Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','QUALITY_CODE',201,'Quality Code','QUALITY_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Quality Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','TRANSACTION_REASON',201,'Transaction Reason','TRANSACTION_REASON','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Transaction Reason','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','TRANSACTION_TYPE',201,'Transaction Type','TRANSACTION_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Transaction Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','UNIT',201,'Unit','UNIT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Unit','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','RCV_SOURCE_DOC_UOM',201,'PO Receipt Source Document Unit of measure','RCV_SOURCE_DOC_UOM','','','','XXEIS_RS_ADMIN','VARCHAR2','RCV_TRANSACTIONS','SOURCE_DOC_UNIT_OF_MEASURE','Rcv Source Doc Uom','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','RCV_SOURCE_DOC_QTY',201,'PO Receipt Source Document Quantity','RCV_SOURCE_DOC_QTY','','','','XXEIS_RS_ADMIN','NUMBER','RCV_TRANSACTIONS','SOURCE_DOC_QUANTITY','Rcv Source Doc Qty','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','RCV_TRANSACTION_ID',201,'PO Receipt Transaction Id','RCV_TRANSACTION_ID','','','','XXEIS_RS_ADMIN','NUMBER','RCV_TRANSACTIONS','TRANSACTION_ID','Rcv Transaction Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','RCV_CREATED_BY',201,'PO Receipt Created By','RCV_CREATED_BY','','','','XXEIS_RS_ADMIN','NUMBER','RCV_TRANSACTIONS','CREATED_BY','Rcv Created By','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','RCV_CREATION_DATE',201,'PO Receipt Creation Date','RCV_CREATION_DATE','','','','XXEIS_RS_ADMIN','DATE','RCV_TRANSACTIONS','CREATION_DATE','Rcv Creation Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','RCV_LAST_UPDATED_BY',201,'PO Receipt Last Updated By','RCV_LAST_UPDATED_BY','','','','XXEIS_RS_ADMIN','NUMBER','RCV_TRANSACTIONS','LAST_UPDATED_BY','Rcv Last Updated By','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','RCV_LAST_UPDATE_DATE',201,'PO Receipt Last Update Date','RCV_LAST_UPDATE_DATE','','','','XXEIS_RS_ADMIN','DATE','RCV_TRANSACTIONS','LAST_UPDATE_DATE','Rcv Last Update Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','RCV_UNIT_OF_MEASURE',201,'PO Receipt Unit Of Measure','RCV_UNIT_OF_MEASURE','','','','XXEIS_RS_ADMIN','VARCHAR2','RCV_TRANSACTIONS','UNIT_OF_MEASURE','Rcv Unit Of Measure','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','RCV_USER_ENTERED',201,'PO Receipt User Entered','RCV_USER_ENTERED','','','','XXEIS_RS_ADMIN','VARCHAR2','Calculation Column','Calculation Column','Rcv User Entered','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','RCV_INTERFACE_SOURCE',201,'PO Receipt Interface Source','RCV_INTERFACE_SOURCE','','','','XXEIS_RS_ADMIN','VARCHAR2','RCV_TRANSACTIONS','INTERFACE_SOURCE_CODE','Rcv Interface Source','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','RCV_SOURCE_DOCUMENT',201,'PO Receipt Source Document','RCV_SOURCE_DOCUMENT','','','','XXEIS_RS_ADMIN','VARCHAR2','RCV_TRANSACTIONS','SOURCE_DOCUMENT_CODE','Rcv Source Document','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','RCV_DESTINATION_TYPE',201,'PO Receipt Destination Type','RCV_DESTINATION_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','RCV_TRANSACTIONS','DESTINATION_TYPE_CODE','Rcv Destination Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','RCV_NEED_BY_DATE',201,'PO Receipt Need By Date','RCV_NEED_BY_DATE','','','','XXEIS_RS_ADMIN','DATE','PO_LINE_LOCATIONS_ALL','NEED_BY_DATE','Rcv Need By Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','RCV_LAST_ACCEPT_DATE',201,'PO Receipt Last Accept Date','RCV_LAST_ACCEPT_DATE','','','','XXEIS_RS_ADMIN','DATE','PO_LINE_LOCATIONS_ALL','LAST_ACCEPT_DATE','Rcv Last Accept Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','RCV_UOM_CODE',201,'PO Receipt Unit of measure Code','RCV_UOM_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','RCV_TRANSACTIONS','UOM_CODE','Rcv Uom Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','RCV_DESTINATION_CONTEXT',201,'PO Receipt Destination Context','RCV_DESTINATION_CONTEXT','','','','XXEIS_RS_ADMIN','VARCHAR2','RCV_TRANSACTIONS','DESTINATION_CONTEXT','Rcv Destination Context','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','MTR_REASON_ID',201,'Mtr Reason Id','MTR_REASON_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Mtr Reason Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','PLL_LINE_LOCATION_ID',201,'Pll Line Location Id','PLL_LINE_LOCATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Pll Line Location Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','POH_PO_HEADER_ID',201,'Poh Po Header Id','POH_PO_HEADER_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Poh Po Header Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','POL_PO_LINE_ID',201,'Pol Po Line Id','POL_PO_LINE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Pol Po Line Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','POV_VENDOR_ID',201,'Pov Vendor Id','POV_VENDOR_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Pov Vendor Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','PVS_VENDOR_SITE_ID',201,'Pvs Vendor Site Id','PVS_VENDOR_SITE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Pvs Vendor Site Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','RCT1_TRANSACTION_ID',201,'Rct1 Transaction Id','RCT1_TRANSACTION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Rct1 Transaction Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','RSH_SHIPMENT_HEADER_ID',201,'Rsh Shipment Header Id','RSH_SHIPMENT_HEADER_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Rsh Shipment Header Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','HRE_EMPLOYEE_ID',201,'Hre Employee Id','HRE_EMPLOYEE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Hre Employee Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','PLT_LINE_TYPE_ID',201,'Plt Line Type Id','PLT_LINE_TYPE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Plt Line Type Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','PO_HEADER_ID',201,'Po Header Id','PO_HEADER_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Po Header Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','POH#STANDARDP#NEED_BY_DATE',201,'Descriptive flexfield: PO Headers Column Name: Need-By Date Context: Standard Purchase Order','POH#STANDARDP#NEED_BY_DATE','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_HEADERS_ALL','ATTRIBUTE1','Poh#Standard Purchase Order#Need-By Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','POH#STANDARDP#FREIGHT_TERMS_',201,'Descriptive flexfield: PO Headers Column Name: Freight Terms (TERMS Tab) Context: Standard Purchase Order','POH#STANDARDP#FREIGHT_TERMS_','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_HEADERS_ALL','ATTRIBUTE2','Poh#Standard Purchase Order#Freight Terms Terms Tab','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','POH#STANDARDP#CARRIER_TERMS_',201,'Descriptive flexfield: PO Headers Column Name: Carrier (TERMS Tab) Context: Standard Purchase Order','POH#STANDARDP#CARRIER_TERMS_','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_HEADERS_ALL','ATTRIBUTE3','Poh#Standard Purchase Order#Carrier Terms Tab','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','POH#STANDARDP#FOB_TERMS_TAB',201,'Descriptive flexfield: PO Headers Column Name: FOB (TERMS Tab) Context: Standard Purchase Order','POH#STANDARDP#FOB_TERMS_TAB','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_HEADERS_ALL','ATTRIBUTE4','Poh#Standard Purchase Order#Fob Terms Tab','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','POV#R11VENDOR#R11_VENDOR_ID',201,'Descriptive flexfield: Vendors Column Name: R11 Vendor ID Context: R11 Vendor ID','POV#R11VENDOR#R11_VENDOR_ID','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_VENDORS','ATTRIBUTE15','Pov#R11 Vendor Id#R11 Vendor Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','POV#TAXEXEMPT#EXEMPTIONS',201,'Descriptive flexfield: Vendors Column Name: Exemptions Context: Tax Exempt Flag','POV#TAXEXEMPT#EXEMPTIONS','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_VENDORS','ATTRIBUTE1','Pov#Tax Exempt Flag#Exemptions','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','PVS#IWOFIPS#FIPS_CODE',201,'Descriptive flexfield: Vendor Sites Column Name: FIPS Code Context: IWO FIPS','PVS#IWOFIPS#FIPS_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_VENDOR_SITES_ALL','ATTRIBUTE1','Pvs#Iwo Fips#Fips Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','PVS#R11VENDOR#R11_VENDOR_SIT',201,'Descriptive flexfield: Vendor Sites Column Name: R11 Vendor Site ID Context: R11 Vendor IDs','PVS#R11VENDOR#R11_VENDOR_SIT','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_VENDOR_SITES_ALL','ATTRIBUTE14','Pvs#R11 Vendor Ids#R11 Vendor Site Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','PVS#R11VENDOR#R11_VENDOR_ID',201,'Descriptive flexfield: Vendor Sites Column Name: R11 Vendor ID Context: R11 Vendor IDs','PVS#R11VENDOR#R11_VENDOR_ID','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_VENDOR_SITES_ALL','ATTRIBUTE15','Pvs#R11 Vendor Ids#R11 Vendor Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','RSH#PRINTED',201,'Descriptive flexfield: RCV_SHIPMENT_HEADERS Column Name: Printed','RSH#PRINTED','','','','XXEIS_RS_ADMIN','VARCHAR2','RCV_SHIPMENT_HEADERS','ATTRIBUTE1','Rsh#Printed','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','INVENTORY_ITEM_ID',201,'Inventory Item Id','INVENTORY_ITEM_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Inventory Item Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','ITEM',201,'Item','ITEM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Item','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','ORGANIZATION_ID',201,'Organization Id','ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','ORG_CODE',201,'Org Code','ORG_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Org Code','','','');
--Inserting View Components for EIS_XXWC_PO_EXCESS_RECEIPTS_V
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','RCV_SHIPMENT_LINES',201,'RCV_SHIPMENT_LINES','RS','RS','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Receiving Shipment Line Information','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','HR_LOCATIONS',201,'HR_LOCATIONS_ALL','HRL','HRL','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Locations','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','PO_VENDORS',201,'PO_VENDORS','POV','POV','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Suppliers','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','PO_HEADERS',201,'PO_HEADERS_ALL','POH','POH','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Purchasing Headers','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','PO_LINES',201,'PO_LINES_ALL','POL','POL','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Purchasing Lines','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','PO_LINE_LOCATIONS',201,'PO_LINE_LOCATIONS_ALL','POLL','POLL','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Document Shipment Schedules ','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','PO_DISTRIBUTIONS',201,'PO_DISTRIBUTIONS_ALL','POD','POD','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Purchase Order Distributions','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','RCV_TRANSACTIONS',201,'RCV_TRANSACTIONS','RT','RT','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Receipt Transactions','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','RCV_SHIPMENT_HEADERS',201,'RCV_SHIPMENT_HEADERS','RSH','RSH','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Receipt Headers','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','PO_AGENTS_V',201,'PO_AGENTS','POAV','POAV','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Buyers Table','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','PO_RELEASES',201,'PO_RELEASES_ALL','PR','PR','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Purchase Order Releases','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','HR_ORGANIZATION_UNITS',201,'HR_ALL_ORGANIZATION_UNITS','HOU','HOU','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','HR_ALL_ORGANIZATION_UNITS','N','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','RCV_TRANSACTIONS',201,'RCV_TRANSACTIONS','RCT1','RCT1','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Parent Receipt Transactions','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','MTL_TRANSACTION_REASONS',201,'MTL_TRANSACTION_REASONS','MTR','MTR','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Inventory Transaction Reasons','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','PO_VENDOR_SITES',201,'PO_VENDOR_SITES_ALL','PVS','PVS','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Supplier Sites','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','FINANCIALS_SYSTEM_PARAMETERS',201,'FINANCIALS_SYSTEM_PARAMS_ALL','FSP','FSP','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Financial Parameters','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','PO_LINE_TYPES',201,'PO_LINE_TYPES_B','PLT','PLT','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Line Types','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','PO_LINE_LOCATIONS',201,'PO_LINE_LOCATIONS_ALL','PLL','PLL','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Shipment Details','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','PO_DOCUMENT_TYPES',201,'PO_DOCUMENT_TYPES_ALL_B','PDT','PDT','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Document Types','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','PO_SYSTEM_PARAMETERS',201,'PO_SYSTEM_PARAMETERS_ALL','PSP','PSP','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','System Parameters','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','MTL_SYSTEM_ITEMS_B',201,'MTL_SYSTEM_ITEMS_B','MSI','MSI','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Item Details','','','','');
--Inserting View Component Joins for EIS_XXWC_PO_EXCESS_RECEIPTS_V
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','RCV_SHIPMENT_LINES','RS',201,'EPERV.SHIPMENT_LINE_ID','=','RS.SHIPMENT_LINE_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','HR_LOCATIONS','HRL',201,'EPERV.LOCATION_ID','=','HRL.LOCATION_ID(+)','','','','','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','PO_VENDORS','POV',201,'EPERV.POV_VENDOR_ID','=','POV.VENDOR_ID(+)','','','','','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','PO_HEADERS','POH',201,'EPERV.POH_PO_HEADER_ID','=','POH.PO_HEADER_ID(+)','','','','','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','PO_LINES','POL',201,'EPERV.POL_PO_LINE_ID','=','POL.PO_LINE_ID(+)','','','','','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','PO_LINE_LOCATIONS','POLL',201,'EPERV.LINE_LOCATION_ID','=','POLL.LINE_LOCATION_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','PO_DISTRIBUTIONS','POD',201,'EPERV.PO_DISTRIBUTION_ID','=','POD.PO_DISTRIBUTION_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','RCV_TRANSACTIONS','RT',201,'EPERV.RCV_TRANSACTION_ID','=','RT.TRANSACTION_ID(+)','','','','','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','RCV_SHIPMENT_HEADERS','RSH',201,'EPERV.RSH_SHIPMENT_HEADER_ID','=','RSH.SHIPMENT_HEADER_ID(+)','','','','','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','PO_AGENTS_V','POAV',201,'EPERV.AGENT_ID','=','POAV.AGENT_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','PO_RELEASES','PR',201,'EPERV.PO_RELEASE_ID','=','PR.PO_RELEASE_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','HR_ORGANIZATION_UNITS','HOU',201,'EPERV.ORG_ID','=','HOU.ORGANIZATION_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','RCV_TRANSACTIONS','RCT1',201,'EPERV.RCT1_TRANSACTION_ID','=','RCT1.TRANSACTION_ID(+)','','','','','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','MTL_TRANSACTION_REASONS','MTR',201,'EPERV.MTR_REASON_ID','=','MTR.REASON_ID(+)','','','','','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','PO_VENDOR_SITES','PVS',201,'EPERV.PVS_VENDOR_SITE_ID','=','PVS.VENDOR_SITE_ID(+)','','','','','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','FINANCIALS_SYSTEM_PARAMETERS','FSP',201,'EPERV.FSP_SET_OF_BOOKS_ID','=','FSP.SET_OF_BOOKS_ID(+)','','','','','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','FINANCIALS_SYSTEM_PARAMETERS','FSP',201,'EPERV.FSP_ORG_ID','=','FSP.ORG_ID(+)','','','','','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','PO_LINE_TYPES','PLT',201,'EPERV.PLT_LINE_TYPE_ID','=','PLT.LINE_TYPE_ID(+)','','','','','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','PO_LINE_LOCATIONS','PLL',201,'EPERV.PLL_LINE_LOCATION_ID','=','PLL.LINE_LOCATION_ID(+)','','','','','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','PO_DOCUMENT_TYPES','PDT',201,'EPERV.PDT_DOCUMENT_TYPE_CODE','=','PDT.DOCUMENT_TYPE_CODE(+)','','','','','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','PO_DOCUMENT_TYPES','PDT',201,'EPERV.PDT_DOCUMENT_SUBTYPE','=','PDT.DOCUMENT_SUBTYPE(+)','','','','','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','PO_DOCUMENT_TYPES','PDT',201,'EPERV.PDT_ORG_ID','=','PDT.ORG_ID(+)','','','','','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','PO_SYSTEM_PARAMETERS','PSP',201,'EPERV.PSP_ORG_ID','=','PSP.ORG_ID(+)','','','','','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','MTL_SYSTEM_ITEMS_B','MSI',201,'EPERV.ORGANIZATION_ID','=','MSI.ORGANIZATION_ID','','','','','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_PO_EXCESS_RECEIPTS_V','MTL_SYSTEM_ITEMS_B','MSI',201,'EPERV.INVENTORY_ITEM_ID','=','MSI.INVENTORY_ITEM_ID','','','','','XXEIS_RS_ADMIN','','');
END;
/
set scan on define on
prompt Creating Report LOV Data for Excess Receipts - WC
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Excess Receipts - WC
xxeis.eis_rs_ins.lov( 201,'select poh.segment1 PO_NUMBER,HOU.name OPERATING_UNIT FROM PO_HEADERS_ALL POH,HR_OPERATING_UNITS HOU
where POH.ORG_ID=HOU.ORGANIZATION_ID
and exists(select 1 from XXEIS.EIS_MO_ORG_TMP_V
            WHERE ORG_ID=POH.ORG_ID)','','EIS_PO_PURCHASE_ORDER_NUM_LOV','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'select vendor_name from po_vendors','','EIS_PO_SUPPLIER_LOV','','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 201,'SELECT ood.organization_code organization_code,
  ood.organization_name organization_name
FROM ORG_ORGANIZATION_DEFINITIONS OOD
WHERE organization_code NOT IN(''CAN'',''HDS'',''US1'',''CN1'')
ORDER BY organization_code','','XXWC Org Lov','','ANONYMOUS',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Excess Receipts - WC
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Excess Receipts - WC
xxeis.eis_rs_utility.delete_report_rows( 'Excess Receipts - WC' );
--Inserting Report - Excess Receipts - WC
xxeis.eis_rs_ins.r( 201,'Excess Receipts - WC','','This report lists all purchase order and receiving details for receipts in excess of quantity ordered for a given receipt date Range, supplier range or purchase order range. This report can be used to monitor excess receipts out of the defined tolerance levels. This report can also be run for audit and control purposes by either the purchasing manager, internal auditors or other authorized users.','','','','XXEIS_RS_ADMIN','EIS_XXWC_PO_EXCESS_RECEIPTS_V','Y','','','XXEIS_RS_ADMIN','','Y','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - Excess Receipts - WC
xxeis.eis_rs_ins.rc( 'Excess Receipts - WC',201,'SUPPLIER_NUMBER','Supplier Number','Supplier number','','','default','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_EXCESS_RECEIPTS_V','','');
xxeis.eis_rs_ins.rc( 'Excess Receipts - WC',201,'SUPPLIER_SITE_CODE','Supplier Site Code','Site code name','','','default','','24','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_EXCESS_RECEIPTS_V','','');
xxeis.eis_rs_ins.rc( 'Excess Receipts - WC',201,'RCV_TRANSACTION_TYPE','Rcv Transaction Type','PO Receipt Transaction Type','','','default','','25','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_EXCESS_RECEIPTS_V','','');
xxeis.eis_rs_ins.rc( 'Excess Receipts - WC',201,'PO_LINE_NUMBER','Po Line Number','Line number','','~~~','default','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_EXCESS_RECEIPTS_V','','');
xxeis.eis_rs_ins.rc( 'Excess Receipts - WC',201,'PO_LOCATION','Po Location','PO Location','','','default','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_EXCESS_RECEIPTS_V','','');
xxeis.eis_rs_ins.rc( 'Excess Receipts - WC',201,'PO_NUMBER','Po Number','PO Number','','','default','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_EXCESS_RECEIPTS_V','','');
xxeis.eis_rs_ins.rc( 'Excess Receipts - WC',201,'PO_QUANTITY_BILLED','Po Quantity Billed','PO Quantity Billed','','~~~','default','','17','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_EXCESS_RECEIPTS_V','','');
xxeis.eis_rs_ins.rc( 'Excess Receipts - WC',201,'PO_QUANTITY_CANCELLED','Po Quantity Cancelled','PO Quantity Cancelled','','~~~','default','','14','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_EXCESS_RECEIPTS_V','','');
xxeis.eis_rs_ins.rc( 'Excess Receipts - WC',201,'PO_QUANTITY_DUE','Po Quantity Due','PO Quantity Due','','~~~','default','','16','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_EXCESS_RECEIPTS_V','','');
xxeis.eis_rs_ins.rc( 'Excess Receipts - WC',201,'PO_QUANTITY_ORDERED','Po Quantity Ordered','PO Quantity Ordered','','~~~','default','','13','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_EXCESS_RECEIPTS_V','','');
xxeis.eis_rs_ins.rc( 'Excess Receipts - WC',201,'PO_QUANTITY_RECEIVED_TO_DATE','Po Quantity Received To Date','PO Quantity Received To Date','','~~~','default','','15','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_EXCESS_RECEIPTS_V','','');
xxeis.eis_rs_ins.rc( 'Excess Receipts - WC',201,'RCV_PACKING_SLIP','Rcv Packing Slip','PO Receipt Packing Slip','','','default','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_EXCESS_RECEIPTS_V','','');
xxeis.eis_rs_ins.rc( 'Excess Receipts - WC',201,'RCV_PO_UNIT_PRICE','Rcv Po Unit Price','PO Receipt Po Unit Price','','~~~','default','','19','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_EXCESS_RECEIPTS_V','','');
xxeis.eis_rs_ins.rc( 'Excess Receipts - WC',201,'RCV_PRIMARY_QUANTITY','Rcv Primary Quantity','Transaction quantity in terms of the item''s primary unit of measure','','~~~','default','','18','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_EXCESS_RECEIPTS_V','','');
xxeis.eis_rs_ins.rc( 'Excess Receipts - WC',201,'RCV_PRIMARY_UNIT_OF_MEASURE','Rcv Primary Unit Of Measure','PO Receipt Primary Unit Of Measure','','','default','','20','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_EXCESS_RECEIPTS_V','','');
xxeis.eis_rs_ins.rc( 'Excess Receipts - WC',201,'RCV_PROMISED_DATE','Rcv Promised Date','PO Receipt Promised Date','','DD-MON-RRRR','default','','21','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_EXCESS_RECEIPTS_V','','');
xxeis.eis_rs_ins.rc( 'Excess Receipts - WC',201,'RCV_RECEIPT_DATE','Rcv Receipt Date','PO Receipt Date','','DD-MON-RRRR','default','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_EXCESS_RECEIPTS_V','','');
xxeis.eis_rs_ins.rc( 'Excess Receipts - WC',201,'RCV_RECEIPT_NUMBER','Rcv Receipt Number','PO Receipt Number','','','default','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_EXCESS_RECEIPTS_V','','');
xxeis.eis_rs_ins.rc( 'Excess Receipts - WC',201,'RCV_RECEIPT_QUANTITY','Rcv Receipt Quantity','PO Receipt Quantity','','~~~','default','','12','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_EXCESS_RECEIPTS_V','','');
xxeis.eis_rs_ins.rc( 'Excess Receipts - WC',201,'RCV_RECEIPT_SOURCE','Rcv Receipt Source','PO Receipt Source','','','default','','22','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_EXCESS_RECEIPTS_V','','');
xxeis.eis_rs_ins.rc( 'Excess Receipts - WC',201,'RCV_RECEIVER','Rcv Receiver','PO Receipt Receiver','','','default','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_EXCESS_RECEIPTS_V','','');
xxeis.eis_rs_ins.rc( 'Excess Receipts - WC',201,'RCV_SHIPMENT_NUM','Rcv Shipment Num','PO Receipt Shipment Num','','','default','','23','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_EXCESS_RECEIPTS_V','','');
xxeis.eis_rs_ins.rc( 'Excess Receipts - WC',201,'SUPPLIER_NAME','Supplier Name','Supplier name','','','default','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_EXCESS_RECEIPTS_V','','');
xxeis.eis_rs_ins.rc( 'Excess Receipts - WC',201,'DESCRIPTION','Item Description','Description','','','default','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_EXCESS_RECEIPTS_V','','');
xxeis.eis_rs_ins.rc( 'Excess Receipts - WC',201,'ITEM','Item','Item','','','default','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_EXCESS_RECEIPTS_V','','');
--Inserting Report Parameters - Excess Receipts - WC
xxeis.eis_rs_ins.rp( 'Excess Receipts - WC',201,'Purchase Order','Purchase Order','PO_NUMBER','IN','EIS_PO_PURCHASE_ORDER_NUM_LOV','','VARCHAR2','N','Y','5','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Excess Receipts - WC',201,'Supplier','Supplier','SUPPLIER_NAME','IN','EIS_PO_SUPPLIER_LOV','','VARCHAR2','N','Y','4','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Excess Receipts - WC',201,'PO Receipt Date To','PO Receipt Date To','RCV_RECEIPT_DATE','IN','','','DATE','N','Y','3','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Excess Receipts - WC',201,'PO Receipt Date From','PO Receipt Date From','RCV_RECEIPT_DATE','IN','','','DATE','N','Y','2','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Excess Receipts - WC',201,'Org','Org','ORG_CODE','IN','XXWC Org Lov','','VARCHAR2','N','Y','1','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
--Inserting Report Conditions - Excess Receipts - WC
xxeis.eis_rs_ins.rcn( 'Excess Receipts - WC',201,'TRUNC(RCV_RECEIPT_DATE)','<=',':PO Receipt Date To','','','Y','4','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Excess Receipts - WC',201,'TRUNC(RCV_RECEIPT_DATE)','>=',':PO Receipt Date From','','','Y','3','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Excess Receipts - WC',201,'SUPPLIER_NAME','IN',':Supplier','','','Y','5','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Excess Receipts - WC',201,'PO_NUMBER','IN',':Purchase Order','','','Y','6','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Excess Receipts - WC',201,'OPERATING_UNIT','IN',':Operating unit','','','N','2','N','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Excess Receipts - WC',201,'ORG_CODE','IN',':Org','','','Y','1','Y','XXEIS_RS_ADMIN');
--Inserting Report Sorts - Excess Receipts - WC
xxeis.eis_rs_ins.rs( 'Excess Receipts - WC',201,'SUPPLIER_NAME','ASC','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rs( 'Excess Receipts - WC',201,'PO_NUMBER','ASC','XXEIS_RS_ADMIN','','');
--Inserting Report Triggers - Excess Receipts - WC
--Inserting Report Templates - Excess Receipts - WC
xxeis.eis_rs_ins.R_Tem( 'Excess Receipts - WC','Excess Receipts - WC','Seeded template for Excess Receipts - WC',' ','',' ','',' ','','','','','','Excess Receipts - WC.rtf','XXEIS_RS_ADMIN');
--Inserting Report Portals - Excess Receipts - WC
--Inserting Report Dashboards - Excess Receipts - WC
xxeis.eis_rs_ins.r_dash( 'Excess Receipts - WC','PO Excess Receipts','PO Excess Receipts','pie','large','Receiver','Receiver','PO Qty Received To Date','PO Qty Received To Date','Sum','XXEIS_RS_ADMIN');
--Inserting Report Security - Excess Receipts - WC
xxeis.eis_rs_ins.rsec( 'Excess Receipts - WC','401','','50884',201,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Excess Receipts - WC','401','','50855',201,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Excess Receipts - WC','401','','50981',201,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Excess Receipts - WC','401','','50882',201,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Excess Receipts - WC','401','','50883',201,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Excess Receipts - WC','401','','51004',201,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Excess Receipts - WC','401','','50619',201,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Excess Receipts - WC','401','','50867',201,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Excess Receipts - WC','401','','50849',201,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Excess Receipts - WC','401','','50868',201,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Excess Receipts - WC','201','','50983',201,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Excess Receipts - WC','201','','50621',201,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Excess Receipts - WC','201','','50893',201,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Excess Receipts - WC','201','','51409',201,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Excess Receipts - WC','201','','50910',201,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Excess Receipts - WC','201','','50892',201,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Excess Receipts - WC','201','','50921',201,'XXEIS_RS_ADMIN','','');
--Inserting Report Pivots - Excess Receipts - WC
xxeis.eis_rs_ins.rpivot( 'Excess Receipts - WC',201,'Pivot','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rs_ins.rpivot_dtls( 'Excess Receipts - WC',201,'Pivot','SUPPLIER_SITE_CODE','PAGE_FIELD','','','0','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Excess Receipts - WC',201,'Pivot','RCV_TRANSACTION_TYPE','PAGE_FIELD','','','','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Excess Receipts - WC',201,'Pivot','PO_LINE_NUMBER','PAGE_FIELD','','','0','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Excess Receipts - WC',201,'Pivot','PO_LOCATION','PAGE_FIELD','','','0','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Excess Receipts - WC',201,'Pivot','PO_NUMBER','PAGE_FIELD','','','0','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Excess Receipts - WC',201,'Pivot','PO_QUANTITY_BILLED','PAGE_FIELD','','','0','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Excess Receipts - WC',201,'Pivot','PO_QUANTITY_CANCELLED','PAGE_FIELD','','','0','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Excess Receipts - WC',201,'Pivot','PO_QUANTITY_DUE','PAGE_FIELD','','','0','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Excess Receipts - WC',201,'Pivot','PO_QUANTITY_ORDERED','PAGE_FIELD','','','0','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Excess Receipts - WC',201,'Pivot','PO_QUANTITY_RECEIVED_TO_DATE','PAGE_FIELD','','','0','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Excess Receipts - WC',201,'Pivot','RCV_RECEIPT_DATE','PAGE_FIELD','','','0','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Excess Receipts - WC',201,'Pivot','RCV_RECEIPT_NUMBER','PAGE_FIELD','','','0','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Excess Receipts - WC',201,'Pivot','RCV_RECEIPT_QUANTITY','DATA_FIELD','SUM','','1','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Excess Receipts - WC',201,'Pivot','RCV_RECEIVER','ROW_FIELD','','','2','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Excess Receipts - WC',201,'Pivot','SUPPLIER_NAME','ROW_FIELD','','','3','1','xlNormal');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
END;
/
set scan on define on
