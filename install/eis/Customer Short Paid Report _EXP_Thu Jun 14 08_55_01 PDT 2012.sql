--Report Name            : Customer Short Pay Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating View Data for Customer Short Pay Report
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_GL_CUST_SHT_PAY_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_GL_CUST_SHT_PAY_V',101,'','','','','SA059956','XXEIS','Eis Xxwc Gl Cust Sht Pay V','EXGCSPV','','');
--Delete View Columns for EIS_XXWC_GL_CUST_SHT_PAY_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_GL_CUST_SHT_PAY_V',101,FALSE);
--Inserting View Columns for EIS_XXWC_GL_CUST_SHT_PAY_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_GL_CUST_SHT_PAY_V','RECEIPT_DATE',101,'Receipt Date','RECEIPT_DATE','','','','SA059956','DATE','','','Receipt Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_GL_CUST_SHT_PAY_V','AMOUNT_APPLIED',101,'Amount Applied','AMOUNT_APPLIED','','~T~D~2','','SA059956','NUMBER','','','Amount Applied','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_GL_CUST_SHT_PAY_V','INVOICE_NUMBER',101,'Invoice Number','INVOICE_NUMBER','','','','SA059956','VARCHAR2','','','Invoice Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_GL_CUST_SHT_PAY_V','INVOICE_DATE',101,'Invoice Date','INVOICE_DATE','','','','SA059956','DATE','','','Invoice Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_GL_CUST_SHT_PAY_V','ORDER_NUMBER',101,'Order Number','ORDER_NUMBER','','','','SA059956','VARCHAR2','','','Order Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_GL_CUST_SHT_PAY_V','CUSTOMER_NAME',101,'Customer Name','CUSTOMER_NAME','','','','SA059956','VARCHAR2','','','Customer Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_GL_CUST_SHT_PAY_V','CUSTOMER_NUMBER',101,'Customer Number','CUSTOMER_NUMBER','','','','SA059956','VARCHAR2','','','Customer Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_GL_CUST_SHT_PAY_V','CREDIT_ANALYST',101,'Credit Analyst','CREDIT_ANALYST','','','','SA059956','VARCHAR2','','','Credit Analyst','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_GL_CUST_SHT_PAY_V','PERIOD_NAME',101,'Period Name','PERIOD_NAME','','','','SA059956','VARCHAR2','','','Period Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_GL_CUST_SHT_PAY_V','CUSTOMER_TRX_ID',101,'Customer Trx Id','CUSTOMER_TRX_ID','','','','SA059956','NUMBER','','','Customer Trx Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_GL_CUST_SHT_PAY_V','AMOUNT_PAID',101,'Amount Paid','AMOUNT_PAID','','~T~D~2','','SA059956','NUMBER','','','Amount Paid','','','');
--Inserting View Components for EIS_XXWC_GL_CUST_SHT_PAY_V
--Inserting View Component Joins for EIS_XXWC_GL_CUST_SHT_PAY_V
END;
/
set scan on define on
prompt Creating Report LOV Data for Customer Short Pay Report
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Customer Short Pay Report
xxeis.eis_rs_ins.lov( 101,'select  per.period_name , led.name ledger_name, per.period_year, per.period_num, per.start_date, per.end_date
from    gl_periods per,gl_ledgers led
where  per.period_set_name = led.period_set_name
and     GL_SECURITY_PKG.VALIDATE_ACCESS(led.ledger_ID) = ''TRUE''
and     led.accounted_period_type = per.period_type','null','EIS_GL_PERIOD_LOV','Derives GL Periods based on the corresponding setups in the sets of books','XXEIS_RS_ADMIN',NULL,'','','');
END;
/
set scan on define on
prompt Creating Report Data for Customer Short Pay Report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Customer Short Pay Report
xxeis.eis_rs_utility.delete_report_rows( 'Customer Short Pay Report' );
--Inserting Report - Customer Short Pay Report
xxeis.eis_rs_ins.r( 101,'Customer Short Pay Report','','This report lists all invoices that were short paid and have a balance outstanding','','','','SA059956','EIS_XXWC_GL_CUST_SHT_PAY_V','Y','','','SA059956','','N','White Cap Reports','','CSV,EXCEL,','N');
--Inserting Report Columns - Customer Short Pay Report
xxeis.eis_rs_ins.rc( 'Customer Short Pay Report',101,'AMOUNT_APPLIED','INVOICE  AMOUNT','Amount Applied','','','','','8','N','','','','','','','','SA059956','N','N','','EIS_XXWC_GL_CUST_SHT_PAY_V','','');
xxeis.eis_rs_ins.rc( 'Customer Short Pay Report',101,'CREDIT_ANALYST','Credit Analyst','Credit Analyst','','','','','1','N','','','','','','','','SA059956','N','N','','EIS_XXWC_GL_CUST_SHT_PAY_V','','');
xxeis.eis_rs_ins.rc( 'Customer Short Pay Report',101,'CUSTOMER_NAME','Customer Name','Customer Name','','','','','3','N','','','','','','','','SA059956','N','N','','EIS_XXWC_GL_CUST_SHT_PAY_V','','');
xxeis.eis_rs_ins.rc( 'Customer Short Pay Report',101,'CUSTOMER_NUMBER','CUST #','Customer Number','','','','','2','N','','','','','','','','SA059956','N','N','','EIS_XXWC_GL_CUST_SHT_PAY_V','','');
xxeis.eis_rs_ins.rc( 'Customer Short Pay Report',101,'INVOICE_DATE','INV DATE','Invoice Date','','','','','5','N','','','','','','','','SA059956','N','N','','EIS_XXWC_GL_CUST_SHT_PAY_V','','');
xxeis.eis_rs_ins.rc( 'Customer Short Pay Report',101,'INVOICE_NUMBER','INVOICE #','Invoice Number','','','','','6','N','','','','','','','','SA059956','N','N','','EIS_XXWC_GL_CUST_SHT_PAY_V','','');
xxeis.eis_rs_ins.rc( 'Customer Short Pay Report',101,'ORDER_NUMBER','ORDER #','Order Number','','','','','4','N','','','','','','','','SA059956','N','N','','EIS_XXWC_GL_CUST_SHT_PAY_V','','');
xxeis.eis_rs_ins.rc( 'Customer Short Pay Report',101,'RECEIPT_DATE','DATE PAID','Receipt Date','','','','','7','N','','','','','','','','SA059956','N','N','','EIS_XXWC_GL_CUST_SHT_PAY_V','','');
xxeis.eis_rs_ins.rc( 'Customer Short Pay Report',101,'AMOUNT_PAID','Amount Paid','Amount Paid','','','','','9','N','','','','','','','','SA059956','N','N','','EIS_XXWC_GL_CUST_SHT_PAY_V','','');
xxeis.eis_rs_ins.rc( 'Customer Short Pay Report',101,'BAL_DUE','Bal Due','Amount Paid','NUMBER','~~2','','0','10','Y','','','','','','','(EXGCSPV.AMOUNT_APPLIED-EXGCSPV.AMOUNT_PAID)','SA059956','N','N','','EIS_XXWC_GL_CUST_SHT_PAY_V','','');
--Inserting Report Parameters - Customer Short Pay Report
xxeis.eis_rs_ins.rp( 'Customer Short Pay Report',101,'Period','Period','PERIOD_NAME','IN','EIS_GL_PERIOD_LOV','','VARCHAR2','N','Y','1','','Y','CONSTANT','SA059956','Y','N','','','');
--Inserting Report Conditions - Customer Short Pay Report
xxeis.eis_rs_ins.rcn( 'Customer Short Pay Report',101,'PERIOD_NAME','IN',':Period','','','Y','','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'Customer Short Pay Report',101,'','','','','AND EXGCSPV.AMOUNT_APPLIED > EXGCSPV.AMOUNT_PAID
AND  EXGCSPV.AMOUNT_PAID<>0','Y','0','','SA059956');
--Inserting Report Sorts - Customer Short Pay Report
--Inserting Report Triggers - Customer Short Pay Report
--Inserting Report Templates - Customer Short Pay Report
--Inserting Report Portals - Customer Short Pay Report
--Inserting Report Dashboards - Customer Short Pay Report
xxeis.eis_rs_ins.r_dash( 'Customer Short Pay Report','Dynamic 760','Dynamic 760','absolute line','large','INVOICE  AMOUNT','INVOICE  AMOUNT','INVOICE  AMOUNT','INVOICE  AMOUNT','Avg','SA059956');
--Inserting Report Security - Customer Short Pay Report
xxeis.eis_rs_ins.rsec( 'Customer Short Pay Report','101','','20434',101,'SA059956','');
xxeis.eis_rs_ins.rsec( 'Customer Short Pay Report','101','','50721',101,'SA059956','');
xxeis.eis_rs_ins.rsec( 'Customer Short Pay Report','101','','50662',101,'SA059956','');
xxeis.eis_rs_ins.rsec( 'Customer Short Pay Report','101','','50720',101,'SA059956','');
xxeis.eis_rs_ins.rsec( 'Customer Short Pay Report','101','','50723',101,'SA059956','');
xxeis.eis_rs_ins.rsec( 'Customer Short Pay Report','101','','50801',101,'SA059956','');
xxeis.eis_rs_ins.rsec( 'Customer Short Pay Report','101','','50665',101,'SA059956','');
xxeis.eis_rs_ins.rsec( 'Customer Short Pay Report','101','','50722',101,'SA059956','');
xxeis.eis_rs_ins.rsec( 'Customer Short Pay Report','101','','50780',101,'SA059956','');
xxeis.eis_rs_ins.rsec( 'Customer Short Pay Report','101','','50667',101,'SA059956','');
xxeis.eis_rs_ins.rsec( 'Customer Short Pay Report','101','','50668',101,'SA059956','');
xxeis.eis_rs_ins.rsec( 'Customer Short Pay Report','101','','50617',101,'SA059956','');
xxeis.eis_rs_ins.rsec( 'Customer Short Pay Report','101','','50618',101,'SA059956','');
xxeis.eis_rs_ins.rsec( 'Customer Short Pay Report','','10012196','',101,'SA059956','');
xxeis.eis_rs_ins.rsec( 'Customer Short Pay Report','','LB048272','',101,'SA059956','');
xxeis.eis_rs_ins.rsec( 'Customer Short Pay Report','','MM050208','',101,'SA059956','');
xxeis.eis_rs_ins.rsec( 'Customer Short Pay Report','','SO004816','',101,'SA059956','');
xxeis.eis_rs_ins.rsec( 'Customer Short Pay Report','','10010432','',101,'SA059956','');
xxeis.eis_rs_ins.rsec( 'Customer Short Pay Report','','LC053655','',101,'SA059956','');
xxeis.eis_rs_ins.rsec( 'Customer Short Pay Report','','RB054040','',101,'SA059956','');
xxeis.eis_rs_ins.rsec( 'Customer Short Pay Report','','RV003897','',101,'SA059956','');
xxeis.eis_rs_ins.rsec( 'Customer Short Pay Report','','SS084202','',101,'SA059956','');
--Inserting Report Pivots - Customer Short Pay Report
END;
/
set scan on define on
