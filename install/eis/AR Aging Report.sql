--Report Name            : Accounts Receivable Aging Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View EIS_XXWC_AR_AGING_BAD_EXP_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_AR_AGING_BAD_EXP_V
Create or replace View XXEIS.EIS_XXWC_AR_AGING_BAD_EXP_V
(CUST_ACCOUNT_ID,CUSTOMER_ACCOUNT_NUMBER,PRISM_CUSTOMER_NUMBER,CUSTOMER_NAME,BILL_TO_SITE_USE_ID,BILL_TO_PARTY_SITE_NUMBER,BILL_TO_PRISM_SITE_NUMBER,BILL_TO_PARTY_SITE_NAME,BILL_TO_SITE_NAME,TRX_BILL_TO_SITE_NAME,TRX_PARTY_SITE_NUMBER,TRX_PARTY_SITE_NAME,BILL_TO_ADDRESS1,BILL_TO_ADDRESS2,BILL_TO_ADDRESS3,BILL_TO_ADDRESS4,BILL_TO_CITY,BILL_TO_CITY_PROVINCE,BILL_TO_ZIP_CODE,BILL_TO_COUNTRY,PAYMENT_SCHEDULE_ID,CUSTOMER_TRX_ID,RCTA_CCID,CASH_RECEIPT_ID,ACRA_CCID,INVOICE_NUMBER,RECEIPT_NUMBER,BRANCH_LOCATION,TRX_NUMBER,TRX_DATE,DUE_DATE,TRX_TYPE,TRANSATION_BALANCE,TRANSACTION_REMAINING_BALANCE,AGE,CURRENT_BALANCE,THIRTY_DAYS_BAL,SIXTY_DAYS_BAL,NINETY_DAYS_BAL,ONE_EIGHTY_DAYS_BAL,THREE_SIXTY_DAYS_BAL,OVER_THREE_SIXTY_DAYS_BAL,LAST_PAYMENT_DATE,CUSTOMER_ACCOUNT_STATUS,SITE_CREDIT_HOLD,CUSTOMER_PROFILE_CLASS,COLLECTOR_NAME,CREDIT_ANALYST,ACCOUNT_MANAGER,ACCOUNT_BALANCE,CUST_PAYMENT_TERM,REMIT_TO_ADDRESS_CODE,STMT_BY_JOB,SEND_STATEMENT_FLAG,SEND_CREDIT_BAL_FLAG,TRX_CUSTOMER_ID,TRX_BILL_SITE_USE_ID,CUSTOMER_PO_NUMBER,PMT_STATUS) AS 
select "CUST_ACCOUNT_ID","CUSTOMER_ACCOUNT_NUMBER","PRISM_CUSTOMER_NUMBER","CUSTOMER_NAME","BILL_TO_SITE_USE_ID","BILL_TO_PARTY_SITE_NUMBER","BILL_TO_PRISM_SITE_NUMBER","BILL_TO_PARTY_SITE_NAME","BILL_TO_SITE_NAME","TRX_BILL_TO_SITE_NAME","TRX_PARTY_SITE_NUMBER","TRX_PARTY_SITE_NAME","BILL_TO_ADDRESS1","BILL_TO_ADDRESS2","BILL_TO_ADDRESS3","BILL_TO_ADDRESS4","BILL_TO_CITY","BILL_TO_CITY_PROVINCE","BILL_TO_ZIP_CODE","BILL_TO_COUNTRY","PAYMENT_SCHEDULE_ID","CUSTOMER_TRX_ID","RCTA_CCID","CASH_RECEIPT_ID","ACRA_CCID","INVOICE_NUMBER","RECEIPT_NUMBER","BRANCH_LOCATION","TRX_NUMBER","TRX_DATE","DUE_DATE","TRX_TYPE","TRANSATION_BALANCE","TRANSACTION_REMAINING_BALANCE","AGE","CURRENT_BALANCE","THIRTY_DAYS_BAL","SIXTY_DAYS_BAL","NINETY_DAYS_BAL","ONE_EIGHTY_DAYS_BAL","THREE_SIXTY_DAYS_BAL","OVER_THREE_SIXTY_DAYS_BAL","LAST_PAYMENT_DATE","CUSTOMER_ACCOUNT_STATUS","SITE_CREDIT_HOLD","CUSTOMER_PROFILE_CLASS","COLLECTOR_NAME","CREDIT_ANALYST","ACCOUNT_MANAGER","ACCOUNT_BALANCE","CUST_PAYMENT_TERM","REMIT_TO_ADDRESS_CODE","STMT_BY_JOB","SEND_STATEMENT_FLAG","SEND_CREDIT_BAL_FLAG","TRX_CUSTOMER_ID","TRX_BILL_SITE_USE_ID","CUSTOMER_PO_NUMBER","PMT_STATUS" from XXWC_AR_CUSTOMER_BALANCE_MV/
set scan on define on
prompt Creating View Data for Accounts Receivable Aging Report
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_AR_AGING_BAD_EXP_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_AR_AGING_BAD_EXP_V',222,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Xxwc Ar Aging Bad Exp V','EXAABEV');
--Delete View Columns for EIS_XXWC_AR_AGING_BAD_EXP_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_AR_AGING_BAD_EXP_V',222,FALSE);
--Inserting View Columns for EIS_XXWC_AR_AGING_BAD_EXP_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','CUST_PAYMENT_TERM',222,'Cust Payment Term','CUST_PAYMENT_TERM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cust Payment Term');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','ACCOUNT_BALANCE',222,'Account Balance','ACCOUNT_BALANCE','','','','XXEIS_RS_ADMIN','NUMBER','','','Account Balance');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','ACCOUNT_MANAGER',222,'Account Manager','ACCOUNT_MANAGER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Account Manager');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','CREDIT_ANALYST',222,'Credit Analyst','CREDIT_ANALYST','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Credit Analyst');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','COLLECTOR_NAME',222,'Collector Name','COLLECTOR_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Collector Name');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','CUSTOMER_PROFILE_CLASS',222,'Customer Profile Class','CUSTOMER_PROFILE_CLASS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Profile Class');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','SITE_CREDIT_HOLD',222,'Site Credit Hold','SITE_CREDIT_HOLD','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Site Credit Hold');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','CUSTOMER_ACCOUNT_STATUS',222,'Customer Account Status','CUSTOMER_ACCOUNT_STATUS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Account Status');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','LAST_PAYMENT_DATE',222,'Last Payment Date','LAST_PAYMENT_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Last Payment Date');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','OVER_THREE_SIXTY_DAYS_BAL',222,'Over Three Sixty Days Bal','OVER_THREE_SIXTY_DAYS_BAL','','','','XXEIS_RS_ADMIN','NUMBER','','','Over Three Sixty Days Bal');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','THREE_SIXTY_DAYS_BAL',222,'Three Sixty Days Bal','THREE_SIXTY_DAYS_BAL','','','','XXEIS_RS_ADMIN','NUMBER','','','Three Sixty Days Bal');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','ONE_EIGHTY_DAYS_BAL',222,'One Eighty Days Bal','ONE_EIGHTY_DAYS_BAL','','','','XXEIS_RS_ADMIN','NUMBER','','','One Eighty Days Bal');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','NINETY_DAYS_BAL',222,'Ninety Days Bal','NINETY_DAYS_BAL','','','','XXEIS_RS_ADMIN','NUMBER','','','Ninety Days Bal');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','SIXTY_DAYS_BAL',222,'Sixty Days Bal','SIXTY_DAYS_BAL','','','','XXEIS_RS_ADMIN','NUMBER','','','Sixty Days Bal');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','THIRTY_DAYS_BAL',222,'Thirty Days Bal','THIRTY_DAYS_BAL','','','','XXEIS_RS_ADMIN','NUMBER','','','Thirty Days Bal');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','AGE',222,'Age','AGE','','','','XXEIS_RS_ADMIN','NUMBER','','','Age');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','TRANSACTION_REMAINING_BALANCE',222,'Transaction Remaining Balance','TRANSACTION_REMAINING_BALANCE','','','','XXEIS_RS_ADMIN','NUMBER','','','Transaction Remaining Balance');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','DUE_DATE',222,'Due Date','DUE_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Due Date');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','RECEIPT_NUMBER',222,'Receipt Number','RECEIPT_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Receipt Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','INVOICE_NUMBER',222,'Invoice Number','INVOICE_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Invoice Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','BRANCH_LOCATION',222,'Branch Location','BRANCH_LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Branch Location');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','CUSTOMER_NAME',222,'Customer Name','CUSTOMER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Name');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','ACRA_CCID',222,'Acra Ccid','ACRA_CCID','','','','XXEIS_RS_ADMIN','NUMBER','','','Acra Ccid');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','BILL_TO_ADDRESS1',222,'Bill To Address1','BILL_TO_ADDRESS1','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Bill To Address1');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','BILL_TO_ADDRESS2',222,'Bill To Address2','BILL_TO_ADDRESS2','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Bill To Address2');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','BILL_TO_ADDRESS3',222,'Bill To Address3','BILL_TO_ADDRESS3','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Bill To Address3');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','BILL_TO_ADDRESS4',222,'Bill To Address4','BILL_TO_ADDRESS4','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Bill To Address4');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','BILL_TO_CITY',222,'Bill To City','BILL_TO_CITY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Bill To City');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','BILL_TO_CITY_PROVINCE',222,'Bill To City Province','BILL_TO_CITY_PROVINCE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Bill To City Province');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','BILL_TO_COUNTRY',222,'Bill To Country','BILL_TO_COUNTRY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Bill To Country');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','BILL_TO_PARTY_SITE_NAME',222,'Bill To Party Site Name','BILL_TO_PARTY_SITE_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Bill To Party Site Name');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','BILL_TO_PARTY_SITE_NUMBER',222,'Bill To Party Site Number','BILL_TO_PARTY_SITE_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Bill To Party Site Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','BILL_TO_PRISM_SITE_NUMBER',222,'Bill To Prism Site Number','BILL_TO_PRISM_SITE_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Bill To Prism Site Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','BILL_TO_SITE_NAME',222,'Bill To Site Name','BILL_TO_SITE_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Bill To Site Name');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','BILL_TO_SITE_USE_ID',222,'Bill To Site Use Id','BILL_TO_SITE_USE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Bill To Site Use Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','BILL_TO_ZIP_CODE',222,'Bill To Zip Code','BILL_TO_ZIP_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Bill To Zip Code');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','CASH_RECEIPT_ID',222,'Cash Receipt Id','CASH_RECEIPT_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Cash Receipt Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','CURRENT_BALANCE',222,'Current Balance','CURRENT_BALANCE','','','','XXEIS_RS_ADMIN','NUMBER','','','Current Balance');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','CUSTOMER_ACCOUNT_NUMBER',222,'Customer Account Number','CUSTOMER_ACCOUNT_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Account Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','CUSTOMER_PO_NUMBER',222,'Customer Po Number','CUSTOMER_PO_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Po Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','CUSTOMER_TRX_ID',222,'Customer Trx Id','CUSTOMER_TRX_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Customer Trx Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','CUST_ACCOUNT_ID',222,'Cust Account Id','CUST_ACCOUNT_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Cust Account Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','PAYMENT_SCHEDULE_ID',222,'Payment Schedule Id','PAYMENT_SCHEDULE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Payment Schedule Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','PMT_STATUS',222,'Pmt Status','PMT_STATUS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Pmt Status');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','PRISM_CUSTOMER_NUMBER',222,'Prism Customer Number','PRISM_CUSTOMER_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Prism Customer Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','RCTA_CCID',222,'Rcta Ccid','RCTA_CCID','','','','XXEIS_RS_ADMIN','NUMBER','','','Rcta Ccid');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','REMIT_TO_ADDRESS_CODE',222,'Remit To Address Code','REMIT_TO_ADDRESS_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Remit To Address Code');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','SEND_CREDIT_BAL_FLAG',222,'Send Credit Bal Flag','SEND_CREDIT_BAL_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Send Credit Bal Flag');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','SEND_STATEMENT_FLAG',222,'Send Statement Flag','SEND_STATEMENT_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Send Statement Flag');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','STMT_BY_JOB',222,'Stmt By Job','STMT_BY_JOB','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Stmt By Job');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','TRANSATION_BALANCE',222,'Transation Balance','TRANSATION_BALANCE','','','','XXEIS_RS_ADMIN','NUMBER','','','Transation Balance');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','TRX_BILL_SITE_USE_ID',222,'Trx Bill Site Use Id','TRX_BILL_SITE_USE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Trx Bill Site Use Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','TRX_BILL_TO_SITE_NAME',222,'Trx Bill To Site Name','TRX_BILL_TO_SITE_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Trx Bill To Site Name');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','TRX_CUSTOMER_ID',222,'Trx Customer Id','TRX_CUSTOMER_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Trx Customer Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','TRX_DATE',222,'Trx Date','TRX_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Trx Date');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','TRX_NUMBER',222,'Trx Number','TRX_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Trx Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','TRX_PARTY_SITE_NAME',222,'Trx Party Site Name','TRX_PARTY_SITE_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Trx Party Site Name');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','TRX_PARTY_SITE_NUMBER',222,'Trx Party Site Number','TRX_PARTY_SITE_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Trx Party Site Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','TRX_TYPE',222,'Trx Type','TRX_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Trx Type');
--Inserting View Components for EIS_XXWC_AR_AGING_BAD_EXP_V
--Inserting View Component Joins for EIS_XXWC_AR_AGING_BAD_EXP_V
END;
/
set scan on define on
prompt Creating Report LOV Data for Accounts Receivable Aging Report
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Accounts Receivable Aging Report
xxeis.eis_rs_ins.lov( 222,'select account_number from hz_cust_accounts','null','Customer Number','Displays List of Values for Customer Number','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 222,'select name from ar_collectors','null','Collector','Displays list of values for Collector','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 222,'select distinct name from hz_cust_profile_classes','null','PROFILE CLASS','This LOV lists all the profile classes of the customers','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 222,'select  customer_status.meaning Status
from  fnd_lookup_values_vl customer_status
where customer_status.lookup_type= ''CODE_STATUS''
 and customer_status.view_application_id=222','','Customer Status','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 222,'select distinct jrse.source_name salesrepname,jrs.salesrep_number salesrepnumber
  from     jtf_rs_salesreps jrs,    jtf_rs_resource_extns jrse
  where JRS.RESOURCE_ID            =JRSE.RESOURCE_ID
','','AR Sales Person LOV','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 222,'SELECT  resource_name resource_name
 FROM jtf_rs_role_relations a,
  jtf_rs_roles_vl b,
  jtf_rs_resource_extns_vl c
WHERE a.role_resource_type  = ''RS_INDIVIDUAL''
AND a.role_resource_id      = c.resource_id
AND a.role_id               = b.role_id
AND b.role_code             = ''CREDIT_ANALYST''
AND c.category              = ''EMPLOYEE''
AND NVL(a.delete_flag,''N'') <> ''Y''
Order BY resource_name','','Credit Analyst','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 222,'select distinct credit_hold from hz_customer_profiles','','Credit Holds','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 222,'SELECT DISTINCT SEGMENT2 Location,
  FV.description
FROM GL_CODE_COMBINATIONS_KFV GCC,
  FND_FLEX_VALUES_vL FV
WHERE 1                   =1
AND FV.FLEX_VALUE(+)      =GCC.SEGMENT2
AND FV.FLEX_VALUE_SET_ID IN
  (SELECT FS.FLEX_VALUE_SET_ID
  FROM FND_FLEX_VALUE_SETS FS
  WHERE FS.FLEX_VALUE_SET_NAME =''XXCUS_GL_LOCATION''
  )
order by SEGMENT2','','AR Branch Location LOV','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 222,'select account_name,account_number from hz_cust_accounts','','AR Customer Name LOV','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 222,'SELECT  DISTINCT PARTY_SITE_NUMBER SITE_NUMBER,Site.location SITE_NAME
FROM  HZ_PARTIES HP,
      HZ_CUST_ACCOUNTS HCS,
      HZ_PARTY_SITES PARTY_SITES,
      Hz_Cust_Site_Uses Site,
      HZ_CUST_ACCT_SITES CUST_SITES
WHERE HP.PARTY_ID =HCS.PARTY_ID
AND HCS.CUST_ACCOUNT_ID =CUST_SITES.CUST_ACCOUNT_ID 
AND SITE.CUST_ACCT_SITE_ID= CUST_SITES.CUST_ACCT_SITE_ID(+)
AND CUST_SITES.PARTY_SITE_ID = PARTY_SITES.PARTY_SITE_ID(+)
order by PARTY_SITE_NUMBER','','AR Site Number LOV','','XXEIS_RS_ADMIN',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Accounts Receivable Aging Report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Accounts Receivable Aging Report
xxeis.eis_rs_utility.delete_report_rows( 'Accounts Receivable Aging Report' );
--Inserting Report - Accounts Receivable Aging Report
xxeis.eis_rs_ins.r( 222,'Accounts Receivable Aging Report','','','','','','XXEIS_RS_ADMIN','EIS_XXWC_AR_AGING_BAD_EXP_V','Y','','','XXEIS_RS_ADMIN','N','N','White Cap Reports','RTF,PDF,','CSV,HTML,Html Summary,XML,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - Accounts Receivable Aging Report
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging Report',222,'ACCOUNT_BALANCE','Account Total Balance','Account Balance','','','','','29','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_AGING_BAD_EXP_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging Report',222,'ACCOUNT_MANAGER','Salesrep Name','Account Manager','','','','','28','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_AGING_BAD_EXP_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging Report',222,'AGE','Age','Age','','','','','13','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_AGING_BAD_EXP_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging Report',222,'BRANCH_LOCATION','Branch Location','Branch Location','','','','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_AGING_BAD_EXP_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging Report',222,'COLLECTOR_NAME','Collector','Collector Name','','','','','24','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_AGING_BAD_EXP_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging Report',222,'CREDIT_ANALYST','Credit Analyst','Credit Analyst','','','','','27','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_AGING_BAD_EXP_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging Report',222,'CURRENT_BALANCE',' Current','Current Balance','','~~2','','','15','N','','DATA_FIELD','','SUM','','3','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_AGING_BAD_EXP_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging Report',222,'CUSTOMER_ACCOUNT_NUMBER','Customer Number','Customer Account Number','','','','','2','N','','ROW_FIELD','','','','2','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_AGING_BAD_EXP_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging Report',222,'CUSTOMER_ACCOUNT_STATUS','Customer Account Status','Customer Account Status','','','','','23','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_AGING_BAD_EXP_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging Report',222,'CUSTOMER_NAME','Customer Name','Customer Name','','','','','1','N','','ROW_FIELD','','','','1','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_AGING_BAD_EXP_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging Report',222,'CUSTOMER_PROFILE_CLASS','Profile Class','Customer Profile Class','','','','','25','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_AGING_BAD_EXP_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging Report',222,'DUE_DATE','Due Date','Due Date','','','','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_AGING_BAD_EXP_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging Report',222,'INVOICE_NUMBER','Invoice Number','Invoice Number','','','','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_AGING_BAD_EXP_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging Report',222,'LAST_PAYMENT_DATE','Last Payment Date','Last Payment Date','','','','','22','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_AGING_BAD_EXP_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging Report',222,'NINETY_DAYS_BAL','61 To 90 Days','Ninety Days Bal','','~~2','','','18','N','','DATA_FIELD','','SUM','','6','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_AGING_BAD_EXP_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging Report',222,'ONE_EIGHTY_DAYS_BAL','91 To 180 Days','One Eighty Days Bal','','~~2','','','19','N','','DATA_FIELD','','SUM','','7','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_AGING_BAD_EXP_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging Report',222,'OVER_THREE_SIXTY_DAYS_BAL','361+Days Past Due','Over Three Sixty Days Bal','','~~2','','','21','N','','DATA_FIELD','','SUM','','9','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_AGING_BAD_EXP_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging Report',222,'RECEIPT_NUMBER','Receipt Number','Receipt Number','','','','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_AGING_BAD_EXP_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging Report',222,'SITE_CREDIT_HOLD','Site Credit Hold Status','Site Credit Hold','','','','','26','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_AGING_BAD_EXP_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging Report',222,'THIRTY_DAYS_BAL','1 To 30 Days','Thirty Days Bal','','~~2','','','16','N','','DATA_FIELD','','SUM','','4','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_AGING_BAD_EXP_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging Report',222,'THREE_SIXTY_DAYS_BAL','181 To 360 Days','Three Sixty Days Bal','','~~2','','','20','N','','DATA_FIELD','','SUM','','8','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_AGING_BAD_EXP_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging Report',222,'TRANSACTION_REMAINING_BALANCE','Transaction Remaining Balance','Transaction Remaining Balance','','','','','12','N','','DATA_FIELD','','SUM','','1','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_AGING_BAD_EXP_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging Report',222,'TRANSATION_BALANCE','Transaction Balance','Transation Balance','','','','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_AGING_BAD_EXP_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging Report',222,'TRX_BILL_TO_SITE_NAME','Site Name','Trx Bill To Site Name','','','','','4','N','','ROW_FIELD','','','','4','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_AGING_BAD_EXP_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging Report',222,'TRX_DATE','Invoice Date','Trx Date','','','','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_AGING_BAD_EXP_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging Report',222,'SIXTY_DAYS_BAL','31 To 60 Days','Sixty Days Bal','','~~2','','','17','N','','DATA_FIELD','','SUM','','5','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_AGING_BAD_EXP_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging Report',222,'TRX_PARTY_SITE_NUMBER','Site Number','Trx Party Site Number','','','','','3','N','','ROW_FIELD','','','','3','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_AGING_BAD_EXP_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging Report',222,'TRX_TYPE','Transaction Type','Trx Type','','','','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_AGING_BAD_EXP_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging Report',222,'OUTSTANDING_AMOUNT','Outstanding Balance','Trx Type','NUMBER','~~2','','','14','Y','','DATA_FIELD','','SUM','','2','EXAABEV.transaction_remaining_balance','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_AGING_BAD_EXP_V','','');
--Inserting Report Parameters - Accounts Receivable Aging Report
xxeis.eis_rs_ins.rp( 'Accounts Receivable Aging Report',222,'Customer Name','Customer Name','CUSTOMER_NAME','IN','AR Customer Name LOV','','VARCHAR2','N','Y','1','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Accounts Receivable Aging Report',222,'Customer Number','Customer Number','CUSTOMER_ACCOUNT_NUMBER','IN','Customer Number','','VARCHAR2','N','Y','2','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Accounts Receivable Aging Report',222,'Customer Account Status','Customer Account Status','CUSTOMER_ACCOUNT_STATUS','IN','Customer Status','','VARCHAR2','N','Y','3','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Accounts Receivable Aging Report',222,'Collector','Collector','COLLECTOR_NAME','IN','Collector','','VARCHAR2','N','Y','4','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Accounts Receivable Aging Report',222,'Credit Analyst','Credit Analyst','CREDIT_ANALYST','IN','Credit Analyst','','VARCHAR2','N','Y','5','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Accounts Receivable Aging Report',222,'Credit Hold','Credit Hold','SITE_CREDIT_HOLD','IN','Credit Holds','','VARCHAR2','N','Y','6','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Accounts Receivable Aging Report',222,'Profile Class','Profile Class','CUSTOMER_PROFILE_CLASS','IN','PROFILE CLASS','','VARCHAR2','N','Y','7','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Accounts Receivable Aging Report',222,'Sales Rep','Sales Rep','ACCOUNT_MANAGER','IN','AR Sales Person LOV','','VARCHAR2','N','Y','8','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Accounts Receivable Aging Report',222,'Location','Location','BRANCH_LOCATION','IN','AR Branch Location LOV','','VARCHAR2','N','Y','9','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Accounts Receivable Aging Report',222,'Site Number','Site Number','TRX_PARTY_SITE_NUMBER','IN','AR Site Number LOV','','VARCHAR2','N','Y','10','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
--Inserting Report Conditions - Accounts Receivable Aging Report
xxeis.eis_rs_ins.rcn( 'Accounts Receivable Aging Report',222,'CUSTOMER_NAME','IN',':Customer Name','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Accounts Receivable Aging Report',222,'CUSTOMER_ACCOUNT_NUMBER','IN',':Customer Number','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Accounts Receivable Aging Report',222,'CUSTOMER_ACCOUNT_STATUS','IN',':Customer Account Status','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Accounts Receivable Aging Report',222,'COLLECTOR_NAME','IN',':Collector','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Accounts Receivable Aging Report',222,'CREDIT_ANALYST','IN',':Credit Analyst','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Accounts Receivable Aging Report',222,'SITE_CREDIT_HOLD','IN',':Credit Hold','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Accounts Receivable Aging Report',222,'CUSTOMER_PROFILE_CLASS','IN',':Profile Class','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Accounts Receivable Aging Report',222,'ACCOUNT_MANAGER','IN',':Sales Rep','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Accounts Receivable Aging Report',222,'BRANCH_LOCATION','IN',':Location','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Accounts Receivable Aging Report',222,'TRX_PARTY_SITE_NUMBER','IN',':Site Number','','','Y','','Y','XXEIS_RS_ADMIN');
--Inserting Report Sorts - Accounts Receivable Aging Report
xxeis.eis_rs_ins.rs( 'Accounts Receivable Aging Report',222,'CUSTOMER_NAME','ASC','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rs( 'Accounts Receivable Aging Report',222,'TRX_PARTY_SITE_NUMBER','ASC','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rs( 'Accounts Receivable Aging Report',222,'TRX_DATE','ASC','XXEIS_RS_ADMIN');
--Inserting Report Triggers - Accounts Receivable Aging Report
--Inserting Report Templates - Accounts Receivable Aging Report
--Inserting Report Portals - Accounts Receivable Aging Report
--Inserting Report Dashboards - Accounts Receivable Aging Report
--Inserting Report Security - Accounts Receivable Aging Report
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging Report','222','','21404',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging Report','222','','20678',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging Report','660','','50856',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging Report','660','','50857',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging Report','660','','50858',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging Report','660','','50859',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging Report','660','','50860',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging Report','660','','50861',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging Report','222','','50871',222,'XXEIS_RS_ADMIN','');
END;
/
set scan on define on
