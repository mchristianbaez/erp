--Report Name            : HDS AP PAYMENT THRU DATE RANGE
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data APFG_AP_PAYMENTS
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Inserting Object APFG_AP_PAYMENTS
xxeis.eis_rsc_ins.v( 'APFG_AP_PAYMENTS',200,'Shows payments, which are disbursements issued to a supplier.','1.0','','','XXEIS_RS_ADMIN','APPS','APFG_AP_PAYMENTS','AAP','','','VIEW','US','','');
--Delete Object Columns for APFG_AP_PAYMENTS
xxeis.eis_rsc_utility.delete_view_rows('APFG_AP_PAYMENTS',200,FALSE);
--Inserting Object Columns for APFG_AP_PAYMENTS
xxeis.eis_rsc_ins.vc( 'APFG_AP_PAYMENTS','AMOUNT',200,'Payment amount.','','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_PAYMENTS','CHECK_DATE',200,'Payment date.','','','','','XXEIS_RS_ADMIN','DATE','','','Check Date','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_PAYMENTS','CHECK_NUMBER',200,'Payment number.','','','','','XXEIS_RS_ADMIN','NUMBER','','','Check Number','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_PAYMENTS','PAYMENT_STATUS',200,'Status of payment (for example, negotiable).','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Payment Status','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_PAYMENTS','PAYMENT_METHOD',200,'Payment Method','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Payment Method','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_PAYMENTS','LAST_UPDATE_DATE',200,'Standard WHO column.','','','','','XXEIS_RS_ADMIN','DATE','','','Last Update Date','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_PAYMENTS','LAST_UPDATED_BY',200,'Standard WHO column.','','','','','XXEIS_RS_ADMIN','NUMBER','','','Last Updated By','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_PAYMENTS','CREATION_DATE',200,'Standard WHO column.','','','','','XXEIS_RS_ADMIN','DATE','','','Creation Date','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_PAYMENTS','CREATED_BY',200,'Standard WHO column.','','','','','XXEIS_RS_ADMIN','NUMBER','','','Created By','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_PAYMENTS','EXTERNAL_BANK_ACCOUNT_ID',200,'The Unique system ID of the External bank account','','','','','XXEIS_RS_ADMIN','NUMBER','','','External Bank Account ID','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_PAYMENTS','BANK_ACCOUNT_ID',200,'The Unique system ID of the bank account','','','','','XXEIS_RS_ADMIN','NUMBER','','','Bank Account ID','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_PAYMENTS','CHECK_ID',200,'Payment unique identifier.','','','','','XXEIS_RS_ADMIN','NUMBER','','','Check Id','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_PAYMENTS','BANK_ACCOUNT_NAME',200,'The name of the bank account','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Bank Account Name','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_PAYMENTS','CURRENCY_CODE',200,'Currency code for this transaction','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Currency Code','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_PAYMENTS','CHECK_VOUCHER_NUMBER',200,'Check Voucher Number','','','','','XXEIS_RS_ADMIN','NUMBER','','','Check Voucher Number','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_PAYMENTS','CHECKRUN_NAME',200,'The payment batch name. (index)','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Checkrun Name','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_PAYMENTS','PAYMENT_TYPE',200,'Flag that indicates payment type.','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Payment Type','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_PAYMENTS','PAYMENT_TYPE_DESCR',200,'Payment Type Descr','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Payment Type Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_PAYMENTS','PAYMENT_STATUS_DESCR',200,'Payment Status Descr','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Payment Status Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_PAYMENTS','PAYMENT_METHOD_DESCR',200,'Payment Method Descr','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Payment Method Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_PAYMENTS','CHECK_SEQUENCE_NUMBER',200,'Payment document sequence number.','','','','','XXEIS_RS_ADMIN','NUMBER','','','Check Sequence Number','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_PAYMENTS','CLEARED_AMOUNT',200,'The payment amount that cleared the bank.','','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Cleared Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_PAYMENTS','CLEARED_DATE',200,'Payment cleared date.','','','','','XXEIS_RS_ADMIN','DATE','','','Cleared Date','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_PAYMENTS','VENDOR_ID',200,'Vendor Id','','','','','XXEIS_RS_ADMIN','NUMBER','','','Vendor Id','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_PAYMENTS','VENDOR_SITE_ID',200,'Vendor Site Id','','','','','XXEIS_RS_ADMIN','NUMBER','','','Vendor Site Id','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_PAYMENTS','VENDOR_NAME',200,'The supplier or payer name.','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Name','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_PAYMENTS','ADDRESS_LINE_1',200,'The first address line of the payment.','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Address Line 1','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_PAYMENTS','ADDRESS_LINE_2',200,'The second address line of the payment.','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Address Line 2','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_PAYMENTS','ADDRESS_LINE_3',200,'The third address line of the payment.','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Address Line 3','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_PAYMENTS','VENDOR_CITY',200,'The city name of the payee.','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor City','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_PAYMENTS','VENDOR_STATE',200,'The state name of the payee.','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor State','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_PAYMENTS','VENDOR_POSTAL_CODE',200,'The state or province postal code of the payee.','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Postal Code','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_PAYMENTS','VENDOR_COUNTRY',200,'The country name of the payee.','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Country','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_PAYMENTS','VENDOR_SITE_CODE',200,'The supplier or payer site code, in the case a supplier has multiple pay sites.','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Site Code','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_PAYMENTS','VENDOR_BANK_ACCOUNT_NUMBER',200,'The suppliers bank account number for electronic fund transfer payment purposes.','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Bank Account Number','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_PAYMENTS','VENDOR_BANK_ACCOUNT_TYPE',200,'The suppliers bank account type code for electronic fund transfer payment purposes.','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Bank Account Type','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_PAYMENTS','TREASURY_PAYMENT_DATE',200,'The date the payment processed through the internal clearing organization.','','','','','XXEIS_RS_ADMIN','DATE','','','Treasury Payment Date','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_PAYMENTS','TREASURY_PAYMENT_NUMBER',200,'The number assigned to a pryment processed through the internal clearing organization.','','','','','XXEIS_RS_ADMIN','NUMBER','','','Treasury Payment Number','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_PAYMENTS','STOP_PAYMENT_RELEASED_DATE',200,'Date and time user released stop payment.','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Stop Payment Released Date','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_PAYMENTS','STOP_PAYMENT_RELEASED_USER_ID',200,'User who released stop payment.','','','','','XXEIS_RS_ADMIN','NUMBER','','','Stop Payment Released User Id','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_PAYMENTS','STOP_PAYMENT_RECORDED_DATE',200,'Date and time user recorded stop payment.','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Stop Payment Recorded Date','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_PAYMENTS','STOP_PAYMENT_RECORDED_USER_ID',200,'User who recorded stop payment.','','','','','XXEIS_RS_ADMIN','NUMBER','','','Stop Payment Recorded User Id','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_PAYMENTS','VOID_DATE',200,'Payment void date.','','','','','XXEIS_RS_ADMIN','DATE','','','Void Date','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_PAYMENTS','FUTURE_PAY_DUE_DATE',200,'Negotiable date for future dated payment.','','','','','XXEIS_RS_ADMIN','DATE','','','Future Pay Due Date','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_PAYMENTS','GLBL_CONTEXT_VALUE',200,'DESC CONTEXT,7003,JG_AP_CHECKS,JA.TH.APXPAWKB.CHECKS','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Glbl Context Value','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_PAYMENTS','GLBL_PAYMENT_DELIVERY_DATE_2',200,'DESC SEGMENT,7003,JG_AP_CHECKS,JA.TH.APXPAWKB.CHECKS,GLOBAL_ATTRIBUTE5','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Glbl Payment Delivery Date 2','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_PAYMENTS','GLBL_TAX_INVOICE_NUMBER_2',200,'DESC SEGMENT,7003,JG_AP_CHECKS,JA.TH.APXPAWKB.CHECKS,GLOBAL_ATTRIBUTE1','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Glbl Tax Invoice Number 2','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_PAYMENTS','GLBL_TAX_INVOICE_DATE_2',200,'DESC SEGMENT,7003,JG_AP_CHECKS,JA.TH.APXPAWKB.CHECKS,GLOBAL_ATTRIBUTE2','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Glbl Tax Invoice Date 2','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_PAYMENTS','GLBL_SUPPLIER_TAX_INVOICE_NU_2',200,'DESC SEGMENT,7003,JG_AP_CHECKS,JA.TH.APXPAWKB.CHECKS,GLOBAL_ATTRIBUTE3','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Glbl Supplier Tax Invoice Nu 2','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_PAYMENTS','GLBL_TAX_ACCOUNTING_PERIOD_2',200,'DESC SEGMENT,7003,JG_AP_CHECKS,JA.TH.APXPAWKB.CHECKS,GLOBAL_ATTRIBUTE4','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Glbl Tax Accounting Period 2','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_PAYMENTS','USSGL_TRANSACTION_CODE',200,'Transaction code for creating US standard general ledger journal entries. (Oracle Public Sector Payables)','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','US Standard GL Transaction Code','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_PAYMENTS','CLEARED_FUNCTIONAL_AMOUNT',200,'Payment cleared amount in functional currency.','','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Cleared Functional Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_PAYMENTS','CLEARED_CURRENCY_EXCH_RATE',200,'Exchange rate at which the payment cleared.  For foreign currency payments only.','','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Cleared Currency Exchange Rate','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_PAYMENTS','CLEARED_CURRENCY_EXCH_DATE',200,'Date the clearing exchange rate is effective.  Usually accounting date of a transaction.','','','','','XXEIS_RS_ADMIN','DATE','','','Cleared Currency Exchange Date','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_PAYMENTS','CLEARED_CRNCY_EXCH_RATE_TYPE',200,'Exchange rate type at payment clearing time.  For foreign currency payments only.','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cleared Currency Exchange Rate Type','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_PAYMENTS','CURRENCY_EXCHANGE_RATE',200,'Exchange rate for foreign currency payments.','','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Currency Exchange Rate','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_PAYMENTS','CURRENCY_EXCHANGE_DATE',200,'Date exchange rate is effective.  Usually the accounting date of the transaction.','','','','','XXEIS_RS_ADMIN','DATE','','','Currency Exchange Date','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_PAYMENTS','CURRENCY_EXCHANGE_RATE_TYPE',200,'Exchange rate type for foreign currency payments.','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Currency Exchange Rate Type','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_PAYMENTS','FUNCTIONAL_AMOUNT',200,'Payment amount in functional currency.','','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Functional Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_PAYMENTS','CLEARED_ERROR_AMOUNT',200,'Cleared payment error amount.','','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Cleared Error Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_PAYMENTS','CLEARED_CHARGES_AMOUNT',200,'Cleared payment charges amount.','','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Cleared Charges Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_PAYMENTS','CLEARED_ERROR_FUNCTIONAL_AMT',200,'Cleared payment error in functional amount.','','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Cleared Error Functional Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_PAYMENTS','CLEARED_CHARGES_FUNCTIONAL_AMT',200,'Cleared payment charges base amount.','','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Cleared Charges Functional Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_PAYMENTS','TRANSFER_PRIORITY',200,'Transfer priority for bank charges (Japanese feature).','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Transfer Priority','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_PAYMENTS','STAMP_DUTY_AMOUNT',200,'Stamp duty tax amount.','','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Stamp Duty Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_PAYMENTS','STAMP_DUTY_FUNCTIONAL_AMOUNT',200,'Stamp duty tax amount in functional currency.','','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Stamp Duty Functional Amount','','','','US');
--Inserting Object Components for APFG_AP_PAYMENTS
xxeis.eis_rsc_ins.vcomp( 'APFG_AP_PAYMENTS','APFG_AP_INVOICE_PAYMENTS',200,'','AAIP','','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','AP Invoice Payments','N','Shows invoice payments made to suppliers','','','AAIP','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'APFG_AP_PAYMENTS','POFG_SUPPLIERS',200,'','PS','','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Suppliers','N','This shows external organizations that provide goods and services to an internal organization.','','','PS','',' ',' ',' ',' ',' ');
--Inserting Object Component Joins for APFG_AP_PAYMENTS
xxeis.eis_rsc_ins.vcj( 'APFG_AP_PAYMENTS','APFG_AP_INVOICE_PAYMENTS','AAIP',200,'AAIP.CHECK_ID','=','AAP.CHECK_ID(+)','','','3','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'APFG_AP_PAYMENTS','POFG_SUPPLIERS','PS',200,'PS.SUPPLIER_ID','=','AAP.VENDOR_ID','','','6','Y','XXEIS_RS_ADMIN');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
--Exporting View Component Data of the View -  APFG_AP_PAYMENTS
prompt Creating Object Data POFG_SUPPLIERS
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Inserting Object POFG_SUPPLIERS
xxeis.eis_rsc_ins.v( 'POFG_SUPPLIERS',200,'','','','','XXEIS_RS_ADMIN','APPS','Pofg Suppliers','PS','','','VIEW','US','','');
--Delete Object Columns for POFG_SUPPLIERS
xxeis.eis_rsc_utility.delete_view_rows('POFG_SUPPLIERS',200,FALSE);
--Inserting Object Columns for POFG_SUPPLIERS
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','BANK_BRANCH_TYPE',200,'Bank Branch Type','BANK_BRANCH_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Bank Branch Type','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','BANK_CHARGE_BEARER',200,'Bank Charge Bearer','BANK_CHARGE_BEARER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Bank Charge Bearer','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','LAST_UPDATED_BY',200,'Last Updated By','LAST_UPDATED_BY','','','','XXEIS_RS_ADMIN','NUMBER','','','Last Updated By','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','LAST_UPDATED_DATE',200,'Last Updated Date','LAST_UPDATED_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Last Updated Date','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','CREATED_BY',200,'Created By','CREATED_BY','','','','XXEIS_RS_ADMIN','NUMBER','','','Created By','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','CREATED_DATE',200,'Created Date','CREATED_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Created Date','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','RECEIVING_ROUTING_NAME',200,'Receiving Routing Name','RECEIVING_ROUTING_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Receiving Routing Name','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','RECEIVING_ROUTING_ID',200,'Receiving Routing Id','RECEIVING_ROUTING_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Receiving Routing Id','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','PAYMENT_TERMS_NAME',200,'Payment Terms Name','PAYMENT_TERMS_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Payment Terms Name','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','PAYMENT_TERMS_ID',200,'Payment Terms Id','PAYMENT_TERMS_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Payment Terms Id','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','HOLD_BY_EMPLOYEE_NAME',200,'Hold By Employee Name','HOLD_BY_EMPLOYEE_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Hold By Employee Name','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','HOLD_BY_EMPLOYEE_NUMBER',200,'Hold By Employee Number','HOLD_BY_EMPLOYEE_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Hold By Employee Number','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','HOLD_BY',200,'Hold By','HOLD_BY','','','','XXEIS_RS_ADMIN','NUMBER','','','Hold By','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','PAYMENT_CURRENCY_NAME',200,'Payment Currency Name','PAYMENT_CURRENCY_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Payment Currency Name','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','PAYMENT_CURRENCY_CODE',200,'Payment Currency Code','PAYMENT_CURRENCY_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Payment Currency Code','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','INVOICE_CURRENCY_NAME',200,'Invoice Currency Name','INVOICE_CURRENCY_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Invoice Currency Name','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','INVOICE_CURRENCY_CODE',200,'Invoice Currency Code','INVOICE_CURRENCY_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Invoice Currency Code','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','PARENT_SUPPLIER_NAME',200,'Parent Supplier Name','PARENT_SUPPLIER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Parent Supplier Name','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','PARENT_SUPPLIER_ID',200,'Parent Supplier Id','PARENT_SUPPLIER_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Parent Supplier Id','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','SUPPLIER_EMPLOYEE_NAME',200,'Supplier Employee Name','SUPPLIER_EMPLOYEE_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Supplier Employee Name','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','SUPPLIER_EMPLOYEE_NUMBER',200,'Supplier Employee Number','SUPPLIER_EMPLOYEE_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Supplier Employee Number','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','SUPPLIER_EMPLOYEE_ID',200,'Supplier Employee Id','SUPPLIER_EMPLOYEE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Supplier Employee Id','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','EDI_TRANSACTION_HANDLING',200,'Edi Transaction Handling','EDI_TRANSACTION_HANDLING','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Edi Transaction Handling','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','EDI_REMITTANCE_INSTRUCTION',200,'Edi Remittance Instruction','EDI_REMITTANCE_INSTRUCTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Edi Remittance Instruction','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','EDI_REMITTANCE_METHOD',200,'Edi Remittance Method','EDI_REMITTANCE_METHOD','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Edi Remittance Method','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','EDI_PAYMENT_FORMAT',200,'Edi Payment Format','EDI_PAYMENT_FORMAT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Edi Payment Format','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','EDI_PAYMENT_METHOD',200,'Edi Payment Method','EDI_PAYMENT_METHOD','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Edi Payment Method','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','WITHHOLDING_START_DATE',200,'Withholding Start Date','WITHHOLDING_START_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Withholding Start Date','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','WITHHOLDING_STATUS_TYPE',200,'Withholding Status Type','WITHHOLDING_STATUS_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Withholding Status Type','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','ALLOW_WITHHOLDING_FLAG',200,'Allow Withholding Flag','ALLOW_WITHHOLDING_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Allow Withholding Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','RECEIPT_DAYS_EXCEPTION',200,'Receipt Days Exception','RECEIPT_DAYS_EXCEPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Receipt Days Exception','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','ALLOW_UNORDERED_RECEIPT',200,'Allow Unordered Receipt','ALLOW_UNORDERED_RECEIPT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Allow Unordered Receipt','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','ALLOW_SUBSTITUTE_RECEIPT',200,'Allow Substitute Receipt','ALLOW_SUBSTITUTE_RECEIPT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Allow Substitute Receipt','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','LATE_RECEIPT_ALLOWED_DAYS',200,'Late Receipt Allowed Days','LATE_RECEIPT_ALLOWED_DAYS','','','','XXEIS_RS_ADMIN','NUMBER','','','Late Receipt Allowed Days','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','EARLY_RECEIPT_ALLOWED_DAYS',200,'Early Receipt Allowed Days','EARLY_RECEIPT_ALLOWED_DAYS','','','','XXEIS_RS_ADMIN','NUMBER','','','Early Receipt Allowed Days','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','QTY_RECEIVED_EXCEPTION',200,'Qty Received Exception','QTY_RECEIVED_EXCEPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Qty Received Exception','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','QTY_RECEIVED_TOLERANCE',200,'Qty Received Tolerance','QTY_RECEIVED_TOLERANCE','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Qty Received Tolerance','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','INVOICE_MATCHING',200,'Invoice Matching','INVOICE_MATCHING','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Invoice Matching','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','ENFORCE_SHIP_TO_LOCATION',200,'Enforce Ship To Location','ENFORCE_SHIP_TO_LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Enforce Ship To Location','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','PURCHASING_HOLD_REASON',200,'Purchasing Hold Reason','PURCHASING_HOLD_REASON','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Purchasing Hold Reason','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','PURCHASING_HOLD_DATE',200,'Purchasing Hold Date','PURCHASING_HOLD_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Purchasing Hold Date','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','FREE_ON_BOARD_POINT',200,'Free On Board Point','FREE_ON_BOARD_POINT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Free On Board Point','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','FREIGHT_TERMS',200,'Freight Terms','FREIGHT_TERMS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Freight Terms','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','PAY_ALONE_FLAG',200,'Pay Alone Flag','PAY_ALONE_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Pay Alone Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','ALLOW_INTEREST_INVOICES',200,'Allow Interest Invoices','ALLOW_INTEREST_INVOICES','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Allow Interest Invoices','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','EXCL_FREIGHT_FROM_DISC',200,'Excl Freight From Disc','EXCL_FREIGHT_FROM_DISC','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Excl Freight From Disc','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','ALWAYS_TAKE_DISCOUNT',200,'Always Take Discount','ALWAYS_TAKE_DISCOUNT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Always Take Discount','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','PAYMENT_METHOD',200,'Payment Method','PAYMENT_METHOD','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Payment Method','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','PAY_DATE_BASIS',200,'Pay Date Basis','PAY_DATE_BASIS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Pay Date Basis','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','TERMS_DATE_BASIS',200,'Terms Date Basis','TERMS_DATE_BASIS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Terms Date Basis','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','PAYMENT_PRIORITY',200,'Payment Priority','PAYMENT_PRIORITY','','','','XXEIS_RS_ADMIN','NUMBER','','','Payment Priority','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','PAY_GROUP',200,'Pay Group','PAY_GROUP','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Pay Group','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','HOLD_REASON',200,'Hold Reason','HOLD_REASON','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Hold Reason','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','HOLD_UNMATCHED_INVOICES',200,'Hold Unmatched Invoices','HOLD_UNMATCHED_INVOICES','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Hold Unmatched Invoices','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','HOLD_FUTURE_PAYMENTS',200,'Hold Future Payments','HOLD_FUTURE_PAYMENTS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Hold Future Payments','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','HOLD_ALL_PAYMENTS',200,'Hold All Payments','HOLD_ALL_PAYMENTS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Hold All Payments','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','INVOICE_AMOUNT_LIMIT',200,'Invoice Amount Limit','INVOICE_AMOUNT_LIMIT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Invoice Amount Limit','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','ORGANIZATION_TYPE',200,'Organization Type','ORGANIZATION_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Organization Type','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','VALIDATION_NUMBER',200,'Validation Number','VALIDATION_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Validation Number','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','TAX_VERIFICATION_DATE',200,'Tax Verification Date','TAX_VERIFICATION_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Tax Verification Date','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','NAME_CONTROL',200,'Name Control','NAME_CONTROL','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Name Control','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','TAX_REPORTING_METHOD',200,'Tax Reporting Method','TAX_REPORTING_METHOD','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Tax Reporting Method','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','STATE_REPORTABLE_FLAG',200,'State Reportable Flag','STATE_REPORTABLE_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','State Reportable Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','TYPE_OF_1099',200,'Type Of 1099','TYPE_OF_1099','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Type Of 1099','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','FEDERAL_REPORTABLE_FLAG',200,'Federal Reportable Flag','FEDERAL_REPORTABLE_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Federal Reportable Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','AMOUNT_INCLUDES_TAX_FLAG',200,'Amount Includes Tax Flag','AMOUNT_INCLUDES_TAX_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Amount Includes Tax Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','TAX_ROUNDING_RULE',200,'Tax Rounding Rule','TAX_ROUNDING_RULE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Tax Rounding Rule','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','ALLOW_TAX_CALC_OVERRIDE',200,'Allow Tax Calc Override','ALLOW_TAX_CALC_OVERRIDE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Allow Tax Calc Override','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','TAX_CALCULATION_LEVEL',200,'Tax Calculation Level','TAX_CALCULATION_LEVEL','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Tax Calculation Level','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','CUSTOMER_NUMBER',200,'Customer Number','CUSTOMER_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Number','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','WOMEN_OWNED_FLAG',200,'Women Owned Flag','WOMEN_OWNED_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Women Owned Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','SMALL_BUSINESS_FLAG',200,'Small Business Flag','SMALL_BUSINESS_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Small Business Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','MINORITY_GROUP',200,'Minority Group','MINORITY_GROUP','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Minority Group','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','ONE_TIME_FLAG',200,'One Time Flag','ONE_TIME_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','One Time Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','STANDARD_INDUSTRY_CLASS',200,'Standard Industry Class','STANDARD_INDUSTRY_CLASS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Standard Industry Class','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','SUPPLIER_TYPE',200,'Supplier Type','SUPPLIER_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Supplier Type','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','TAX_IDENTIFICATION_NUMBER',200,'Tax Identification Number','TAX_IDENTIFICATION_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Tax Identification Number','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','VAT_REGISTRATION_NUMBER',200,'Vat Registration Number','VAT_REGISTRATION_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vat Registration Number','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','END_ACTIVE_DATE',200,'End Active Date','END_ACTIVE_DATE','','','','XXEIS_RS_ADMIN','DATE','','','End Active Date','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','START_ACTIVE_DATE',200,'Start Active Date','START_ACTIVE_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Start Active Date','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','SUPPLIER_NUMBER',200,'Supplier Number','SUPPLIER_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Supplier Number','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','ALTERNATE_SUPPLIER_NAME',200,'Alternate Supplier Name','ALTERNATE_SUPPLIER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Alternate Supplier Name','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','SUPPLIER_NAME',200,'Supplier Name','SUPPLIER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Supplier Name','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','SUPPLIER_ID',200,'Supplier Id','SUPPLIER_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Supplier Id','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','OFFSET_TAX_FLAG',200,'Offset Tax Flag','OFFSET_TAX_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Offset Tax Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','CREATE_DEBIT_MEMO_FLAG',200,'Create Debit Memo Flag','CREATE_DEBIT_MEMO_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Create Debit Memo Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','FUTURE_DATED_PAYMENT_CCID',200,'Future Dated Payment Ccid','FUTURE_DATED_PAYMENT_CCID','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Future Dated Payment Ccid','','','','US');
xxeis.eis_rsc_ins.vc( 'POFG_SUPPLIERS','MATCH_OPTION',200,'Match Option','MATCH_OPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Match Option','','','','US');
--Inserting Object Components for POFG_SUPPLIERS
--Inserting Object Component Joins for POFG_SUPPLIERS
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
--Exporting View Component Data of the View -  APFG_AP_PAYMENTS
prompt Creating Object Data APFG_AP_INVOICE_PAYMENTS
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Inserting Object APFG_AP_INVOICE_PAYMENTS
xxeis.eis_rsc_ins.v( 'APFG_AP_INVOICE_PAYMENTS',200,'','','','','XXEIS_RS_ADMIN','APPS','Apfg Ap Invoice Payments','AAIP','','','VIEW','US','','');
--Delete Object Columns for APFG_AP_INVOICE_PAYMENTS
xxeis.eis_rsc_utility.delete_view_rows('APFG_AP_INVOICE_PAYMENTS',200,FALSE);
--Inserting Object Columns for APFG_AP_INVOICE_PAYMENTS
xxeis.eis_rsc_ins.vc( 'APFG_AP_INVOICE_PAYMENTS','LAST_UPDATE_LOGIN',200,'Last Update Login','LAST_UPDATE_LOGIN','','','','XXEIS_RS_ADMIN','NUMBER','','','Last Update Login','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_INVOICE_PAYMENTS','LAST_UPDATE_DATE',200,'Last Update Date','LAST_UPDATE_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Last Update Date','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_INVOICE_PAYMENTS','LAST_UPDATED_BY',200,'Last Updated By','LAST_UPDATED_BY','','','','XXEIS_RS_ADMIN','NUMBER','','','Last Updated By','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_INVOICE_PAYMENTS','CREATION_DATE',200,'Creation Date','CREATION_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Creation Date','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_INVOICE_PAYMENTS','CREATED_BY',200,'Created By','CREATED_BY','','','','XXEIS_RS_ADMIN','NUMBER','','','Created By','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_INVOICE_PAYMENTS','SET_OF_BOOKS_ID',200,'Set Of Books Id','SET_OF_BOOKS_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Set Of Books Id','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_INVOICE_PAYMENTS','LOSS_CODE_COMBINATION_ID',200,'Loss Code Combination Id','LOSS_CODE_COMBINATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Loss Code Combination Id','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_INVOICE_PAYMENTS','INVOICE_ID',200,'Invoice Id','INVOICE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Invoice Id','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_INVOICE_PAYMENTS','GAIN_CODE_COMBINATION_ID',200,'Gain Code Combination Id','GAIN_CODE_COMBINATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Gain Code Combination Id','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_INVOICE_PAYMENTS','CHECK_ID',200,'Check Id','CHECK_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Check Id','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_INVOICE_PAYMENTS','ASSET_CODE_COMBINATION_ID',200,'Asset Code Combination Id','ASSET_CODE_COMBINATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Asset Code Combination Id','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_INVOICE_PAYMENTS','ACCTS_PAY_CODE_COMBINATION_ID',200,'Accts Pay Code Combination Id','ACCTS_PAY_CODE_COMBINATION_ID','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Accts Pay Code Combination Id','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_INVOICE_PAYMENTS','APS_EXTERNAL_BANK_ACCOUNT_ID',200,'Aps External Bank Account Id','APS_EXTERNAL_BANK_ACCOUNT_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Aps External Bank Account Id','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_INVOICE_PAYMENTS','EXTERNAL_BANK_ACCOUNT_ID',200,'External Bank Account Id','EXTERNAL_BANK_ACCOUNT_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','External Bank Account Id','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_INVOICE_PAYMENTS','SET_OF_BOOKS_NAME',200,'Set Of Books Name','SET_OF_BOOKS_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Set Of Books Name','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_INVOICE_PAYMENTS','DESCRIPTION',200,'Description','DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Description','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_INVOICE_PAYMENTS','AMOUNT_PAID',200,'Amount Paid','AMOUNT_PAID','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Amount Paid','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_INVOICE_PAYMENTS','INVOICE_AMOUNT',200,'Invoice Amount','INVOICE_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Invoice Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_INVOICE_PAYMENTS','INVOICE_DATE',200,'Invoice Date','INVOICE_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Invoice Date','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_INVOICE_PAYMENTS','INVOICE_NUMBER',200,'Invoice Number','INVOICE_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Invoice Number','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_INVOICE_PAYMENTS','CHECK_STATUS',200,'Check Status','CHECK_STATUS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Check Status','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_INVOICE_PAYMENTS','CHECK_TYPE',200,'Check Type','CHECK_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Check Type','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_INVOICE_PAYMENTS','BANK_ACCOUNT_NAME',200,'Bank Account Name','BANK_ACCOUNT_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Bank Account Name','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_INVOICE_PAYMENTS','CHECK_CURRENCY_CODE',200,'Check Currency Code','CHECK_CURRENCY_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Check Currency Code','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_INVOICE_PAYMENTS','CHECK_AMOUNT',200,'Check Amount','CHECK_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Check Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_INVOICE_PAYMENTS','CHECK_DATE',200,'Check Date','CHECK_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Check Date','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_INVOICE_PAYMENTS','CHECK_NUMBER',200,'Check Number','CHECK_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Check Number','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_INVOICE_PAYMENTS','PAYMENT_TYPE',200,'Payment Type','PAYMENT_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Payment Type','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_INVOICE_PAYMENTS','INVOICE_PAYMENT_TYPE',200,'Invoice Payment Type','INVOICE_PAYMENT_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Invoice Payment Type','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_INVOICE_PAYMENTS','POSTED_FLAG',200,'Posted Flag','POSTED_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Posted Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_INVOICE_PAYMENTS','PERIOD_NAME',200,'Period Name','PERIOD_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Period Name','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_INVOICE_PAYMENTS','PAYMENT_NUMBER',200,'Payment Number','PAYMENT_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Payment Number','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_INVOICE_PAYMENTS','PAYMENT_BASE_AMOUNT',200,'Payment Base Amount','PAYMENT_BASE_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Payment Base Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_INVOICE_PAYMENTS','INVOICE_BASE_AMOUNT',200,'Invoice Base Amount','INVOICE_BASE_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Invoice Base Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_INVOICE_PAYMENTS','EXCHANGE_RATE_TYPE',200,'Exchange Rate Type','EXCHANGE_RATE_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Exchange Rate Type','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_INVOICE_PAYMENTS','EXCHANGE_RATE',200,'Exchange Rate','EXCHANGE_RATE','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Exchange Rate','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_INVOICE_PAYMENTS','EXCHANGE_DATE',200,'Exchange Date','EXCHANGE_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Exchange Date','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_INVOICE_PAYMENTS','DISCOUNT_TAKEN',200,'Discount Taken','DISCOUNT_TAKEN','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Discount Taken','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_INVOICE_PAYMENTS','DISCOUNT_LOST',200,'Discount Lost','DISCOUNT_LOST','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Discount Lost','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_INVOICE_PAYMENTS','CASH_POSTED_FLAG',200,'Cash Posted Flag','CASH_POSTED_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cash Posted Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_INVOICE_PAYMENTS','BANK_NUMBER',200,'Bank Number','BANK_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Bank Number','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_INVOICE_PAYMENTS','BANK_ACCOUNT_TYPE',200,'Bank Account Type','BANK_ACCOUNT_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Bank Account Type','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_INVOICE_PAYMENTS','BANK_ACCOUNT_NUMBER',200,'Bank Account Number','BANK_ACCOUNT_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Bank Account Number','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_INVOICE_PAYMENTS','ASSETS_ADDITION_FLAG',200,'Assets Addition Flag','ASSETS_ADDITION_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Assets Addition Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_INVOICE_PAYMENTS','AMOUNT',200,'Amount','AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_INVOICE_PAYMENTS','ACCRUAL_POSTED_FLAG',200,'Accrual Posted Flag','ACCRUAL_POSTED_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Accrual Posted Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_INVOICE_PAYMENTS','ACCOUNTING_DATE',200,'Accounting Date','ACCOUNTING_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Accounting Date','','','','US');
xxeis.eis_rsc_ins.vc( 'APFG_AP_INVOICE_PAYMENTS','INVOICE_PAYMENT_ID',200,'Invoice Payment Id','INVOICE_PAYMENT_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Invoice Payment Id','','','','US');
--Inserting Object Components for APFG_AP_INVOICE_PAYMENTS
--Inserting Object Component Joins for APFG_AP_INVOICE_PAYMENTS
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
prompt Creating Report LOV Data for HDS AP PAYMENT THRU DATE RANGE
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - HDS AP PAYMENT THRU DATE RANGE
xxeis.eis_rsc_ins.lov( 200,'SELECT DISTINCT segment1 FROM po_vendors','null','Supplier Numbers LOV','Lists the vendors number','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
prompt Creating Report Data for HDS AP PAYMENT THRU DATE RANGE
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Deleting Report data - HDS AP PAYMENT THRU DATE RANGE
xxeis.eis_rsc_utility.delete_report_rows( 'HDS AP PAYMENT THRU DATE RANGE' );
--Inserting Report - HDS AP PAYMENT THRU DATE RANGE
xxeis.eis_rsc_ins.r( 200,'HDS AP PAYMENT THRU DATE RANGE','',' Imported from Discoverer. Workbook Name: AP PAYMENT THRU DATE RANGE Sheet Name = Sheet 1','','','','XXEIS_RS_ADMIN','APFG_AP_PAYMENTS','Y','','','XXEIS_RS_ADMIN','','N','HDS Standard Reports','','CSV,Pivot Excel,EXCEL,','Y','','','','','','','','US','','','','');
--Inserting Report Columns - HDS AP PAYMENT THRU DATE RANGE
xxeis.eis_rsc_ins.rc( 'HDS AP PAYMENT THRU DATE RANGE',200,'CHECK_DATE','CHECK_DATE','Payment date.','DATE','','default','','1','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','APFG_AP_PAYMENTS','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP PAYMENT THRU DATE RANGE',200,'AMOUNT','AMOUNT','Payment amount.','NUMBER','~T~D~2','default','','2','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','APFG_AP_PAYMENTS','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP PAYMENT THRU DATE RANGE',200,'PAYMENT_METHOD','Payment Method','Payment Method','VARCHAR2','','default','','3','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','APFG_AP_PAYMENTS','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP PAYMENT THRU DATE RANGE',200,'PAYMENT_STATUS','PAYMENT_STATUS','Status of payment (for example, negotiable).','VARCHAR2','','default','','4','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','APFG_AP_PAYMENTS','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP PAYMENT THRU DATE RANGE',200,'CHECK_NUMBER','Check Number','Payment number.','NUMBER','~~~','default','','6','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','APFG_AP_PAYMENTS','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP PAYMENT THRU DATE RANGE',200,'PS.SUPPLIER_NUMBER','Supplier Number','Supplier Number','','','default','','5','','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','APFG_AP_PAYMENTS','POFG_SUPPLIERS','PS','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP PAYMENT THRU DATE RANGE',200,'AAIP.INVOICE_NUMBER','Invoice Number','Invoice Number','','','default','','7','','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','APFG_AP_PAYMENTS','APFG_AP_INVOICE_PAYMENTS','AAIP','GROUP_BY','US','');
--Inserting Report Parameters - HDS AP PAYMENT THRU DATE RANGE
xxeis.eis_rsc_ins.rp( 'HDS AP PAYMENT THRU DATE RANGE',200,'Check Date From','Check Date From','CHECK_DATE','>=','','','DATE','N','Y','1','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','Start Date','','APFG_AP_PAYMENTS','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS AP PAYMENT THRU DATE RANGE',200,'Check Date To','Check Date To','CHECK_DATE','<=','','','DATE','N','Y','2','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','End Date','','APFG_AP_PAYMENTS','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS AP PAYMENT THRU DATE RANGE',200,'Supplier Number','Supplier Number','SUPPLIER_NUMBER','IN','Supplier Numbers LOV','','VARCHAR2','Y','Y','4','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','APFG_AP_PAYMENTS','POFG_SUPPLIERS','PS','US','');
--Inserting Dependent Parameters - HDS AP PAYMENT THRU DATE RANGE
--Inserting Report Conditions - HDS AP PAYMENT THRU DATE RANGE
xxeis.eis_rsc_ins.rcnh( 'HDS AP PAYMENT THRU DATE RANGE',200,'PS.SUPPLIER_NUMBER IN Supplier Number','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','SUPPLIER_NUMBER','','Supplier Number','','','','','APFG_AP_PAYMENTS','PS.POFG_SUPPLIERS','','','','','IN','Y','Y','','','','','1',200,'HDS AP PAYMENT THRU DATE RANGE','PS.SUPPLIER_NUMBER IN Supplier Number');
xxeis.eis_rsc_ins.rcnh( 'HDS AP PAYMENT THRU DATE RANGE',200,'AAP.CHECK_DATE >= Check Date From','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CHECK_DATE','','Check Date From','','','','','APFG_AP_PAYMENTS','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',200,'HDS AP PAYMENT THRU DATE RANGE','AAP.CHECK_DATE >= Check Date From');
xxeis.eis_rsc_ins.rcnh( 'HDS AP PAYMENT THRU DATE RANGE',200,'AAP.CHECK_DATE <= Check Date To','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CHECK_DATE','','Check Date To','','','','','APFG_AP_PAYMENTS','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',200,'HDS AP PAYMENT THRU DATE RANGE','AAP.CHECK_DATE <= Check Date To');
--Inserting Report Sorts - HDS AP PAYMENT THRU DATE RANGE
--Inserting Report Triggers - HDS AP PAYMENT THRU DATE RANGE
--inserting report templates - HDS AP PAYMENT THRU DATE RANGE
--Inserting Report Portals - HDS AP PAYMENT THRU DATE RANGE
--inserting report dashboards - HDS AP PAYMENT THRU DATE RANGE
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'HDS AP PAYMENT THRU DATE RANGE','200','APFG_AP_PAYMENTS','POFG_SUPPLIERS','N','PS');
xxeis.eis_rsc_ins.rviews( 'HDS AP PAYMENT THRU DATE RANGE','200','APFG_AP_PAYMENTS','APFG_AP_INVOICE_PAYMENTS','N','AAIP');
xxeis.eis_rsc_ins.rviews( 'HDS AP PAYMENT THRU DATE RANGE','200','APFG_AP_PAYMENTS','APFG_AP_PAYMENTS','N','');
--inserting report security - HDS AP PAYMENT THRU DATE RANGE
xxeis.eis_rsc_ins.rsec( 'HDS AP PAYMENT THRU DATE RANGE','200','','PAYABLES_MANAGER',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP PAYMENT THRU DATE RANGE','200','','',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP PAYMENT THRU DATE RANGE','200','','',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP PAYMENT THRU DATE RANGE','200','','',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP PAYMENT THRU DATE RANGE','200','','PAYABLES_INQUIRY',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP PAYMENT THRU DATE RANGE','200','','',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP PAYMENT THRU DATE RANGE','200','','',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP PAYMENT THRU DATE RANGE','200','','HDS_AP_INQUIRY_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP PAYMENT THRU DATE RANGE','200','','HDS_AP_DISBURSEMTS_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP PAYMENT THRU DATE RANGE','200','','',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP PAYMENT THRU DATE RANGE','200','','',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP PAYMENT THRU DATE RANGE','200','','',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP PAYMENT THRU DATE RANGE','200','','HDS_PYABLS_MNGR_CAN',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP PAYMENT THRU DATE RANGE','200','','',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP PAYMENT THRU DATE RANGE','200','','HDS_AP_SUPPLIER_MAINT_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP PAYMENT THRU DATE RANGE','200','','',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP PAYMENT THRU DATE RANGE','200','','HDS_OIE_USER',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP PAYMENT THRU DATE RANGE','200','','HDS_AP_ADMIN_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP PAYMENT THRU DATE RANGE','200','','HDS_AP_MGR_NOSUP_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP PAYMENT THRU DATE RANGE','200','','HDS_AP_TRNS_ENTRY_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP PAYMENT THRU DATE RANGE','200','','HDS_AP_ADMIN_US_GSCIWO',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP PAYMENT THRU DATE RANGE','200','','HDS_AP_ADMIN_US_IWO',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP PAYMENT THRU DATE RANGE','200','','HDS_AP_DISBUREMTS_US_IWO',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP PAYMENT THRU DATE RANGE','200','','HDS_AP_INQUIRY_US_IWO',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP PAYMENT THRU DATE RANGE','200','','HDS_AP_TRNS_ENTRY_US_IWO',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP PAYMENT THRU DATE RANGE','200','','XXWC_PAY_NO_CALENDAR',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP PAYMENT THRU DATE RANGE','200','','XXWC_PAY_W_CALENDAR',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP PAYMENT THRU DATE RANGE','200','','XXWC_PAY_DISBURSE',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP PAYMENT THRU DATE RANGE','200','','XXWC_PAYABLES_INQUIRY',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP PAYMENT THRU DATE RANGE','200','','XXWC_PAY_MANAGER',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP PAYMENT THRU DATE RANGE','200','','XXWC_PAY_VENDOR_BANK_DETAILS',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP PAYMENT THRU DATE RANGE','200','','XXWC_PAY_VENDOR_MSTR',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP PAYMENT THRU DATE RANGE','200','','XXWC_PAY_VENDOR_MSTR_INQUIRY',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP PAYMENT THRU DATE RANGE','200','','HDS_AP_MGR_NOSUP_US_IWO',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP PAYMENT THRU DATE RANGE','200','','HDS_AP_MANAGER',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP PAYMENT THRU DATE RANGE','200','','HDS_PYBLS_MNGR',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP PAYMENT THRU DATE RANGE','20005','','XXWC_VIEW_ALL_EIS_REPORTS',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP PAYMENT THRU DATE RANGE','200','','HDS_AP_TRNS_ENTRY_CAD_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP PAYMENT THRU DATE RANGE','200','','HDS_AP_SUPPLIER_MAINT_CAD_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP PAYMENT THRU DATE RANGE','200','','HDS_AP_MGR_NOSUP_CAD_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP PAYMENT THRU DATE RANGE','200','','HDS_AP_DISBURSEMTS_CAD_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP PAYMENT THRU DATE RANGE','200','','HDS_AP_ADMIN_CAD_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP PAYMENT THRU DATE RANGE','200','','HDS_PAYABLES_CLOSE_GSC_GLBL',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP PAYMENT THRU DATE RANGE','200','','HDS_PAYABLES_CLOSE_GLBL',200,'XXEIS_RS_ADMIN','','','');
--Inserting Report Pivots - HDS AP PAYMENT THRU DATE RANGE
xxeis.eis_rsc_ins.rpivot( 'HDS AP PAYMENT THRU DATE RANGE',200,'Pivot','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS AP PAYMENT THRU DATE RANGE',200,'Pivot','AMOUNT','DATA_FIELD','SUM','','','1','xlNormal');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
--Inserting Report   Version details- HDS AP PAYMENT THRU DATE RANGE
xxeis.eis_rsc_ins.rv( 'HDS AP PAYMENT THRU DATE RANGE','','HDS AP PAYMENT THRU DATE RANGE','AB063501');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
