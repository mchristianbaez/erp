--Report Name            : Accounts Receivable Aging Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View EIS_XXWC_AR_AGING_BAD_DEBT_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_AR_AGING_BAD_DEBT_V
Create or replace View XXEIS.EIS_XXWC_AR_AGING_BAD_DEBT_V
(BILL_TO_CUSTOMER_NAME,PARENT_CUSTOMER_NAME,BILL_TO_CUSTOMER_NUMBER,PARENT_CUSTOMER_NUMBER,LOCATION,SALESREP_NAME,TRX_TYPE_NAME,TRX_NUM,INVOICE_DATE,RECEIPT_NUMBER,RECEIPT_DATE,DUE_DATE,INVOICE_AMOUNT,DAYS_PAST_DUE,AMOUNT_DUE_REMAINING,CUSTOMER_ACCOUNT_STATUS,PROFILE_CLASS,COLLECTOR,CREDIT_ANALYST_NAME,CREDIT_MANAGER,CREDIT_HOLD_STATUS,PAYMENT_TERMS,BUCKET_CURRENT,APPLIED_DATE,BUCKET_1_TO_30,BUCKET_31_TO_60,BUCKET_61_TO_90,BUCKET_91_TO_180,BUCKET_181_TO_360,BUCKET_361_DAYS_AND_ABOVE,ACCT_BAL,SITE_NUMBER,SITE_NAME,OUTSTANDING_AMOUNT) AS 
SELECT main.bill_to_customer_name,
    main.parent_Customer_Name,
    main.bill_to_customer_number,
    main.PARENT_CUSTOMER_NUMBER,
    main.LOCATION,
    MAIN.SALESREP_NAME,
    main.TRX_TYPE_NAME,
    Main.Trx_Num,
    Main.Invoice_Date,
    Main.Receipt_Number,
    Main.Receipt_Date,
    Main.Due_Date,
    Main.Invoice_Amount,
    Main.Days_Past_Due,
    Main.Amount_Due_Remaining,
    Main.Customer_Account_Status,
    Main.Profile_Class,
    Main.Collector,
    Main.Credit_Analyst_Name,
    Main.Credit_Manager,
    Main.Credit_Hold_Status,
    Main.Payment_Terms,
    Main.Bucket_Current,
    Main.Applied_Date,
    Main.Bucket_1_To_30,
    Main.Bucket_31_To_60,
    Main.Bucket_61_To_90,
    Main.Bucket_91_To_180,
    Main.Bucket_181_To_360,
    MAIN.BUCKET_361_DAYS_AND_ABOVE,
    main.ACCT_BAL,
    main.site_number,
    main.site_name,
    main.Outstanding_Amount
    --(main.bucket_current+ main.bucket_1_to_30+ main.bucket_31_to_60+ main.bucket_61_to_90+ main.bucket_181_to_360+ main.bucket_361_days_and_above)total,
  FROM
    (SELECT Gcc.Concatenated_Segments Concatenated_Segments,
      NVL (cust.account_name, party.party_name) bill_to_customer_name,
      NVL (parent_cust.Account_Name, parent_Party.Party_Name) parent_Customer_Name,
      cust.account_number bill_to_customer_number,
      PARENT_CUST.ACCOUNT_NUMBER PARENT_CUSTOMER_NUMBER,
      gcc.segment2 location,
      jsr.name salesrep_name ,
      Ctt.Name Trx_Type_Name,
      Ctt.Cust_Trx_Type_Id Trx_Type_Id,
      TO_CHAR (NULL) Receipt_Method_Name,
      To_Number (NULL) Receipt_Method_Id,
      Siteb.Site_Use_Id Contact_Site_Id,
      Siteb.Site_Use_Code Bill_Site_Code,
      Locb.Address1
      || Locb.Address2
      || Locb.Address3
      || Locb.Address4 Bill_To_Address,
      Locb.Province Bill_To_Province,
      Locb.County Bill_To_County,
      Locb.City Bill_To_City,
      Locb.State Bill_To_State,
      Locb.Country Bill_To_Country,
      Locb.Postal_Code Bill_To_Postal_Code,
      Locb.Address_Key Bill_To_Address_Key,
      Cust.Cust_Account_Id Bill_To_Customer_Id,
      Sites.Site_Use_Id Ship_Contact_Site_Id,
      Sites.Site_Use_Code Ship_Site_Code,
      Locs.Address1
      || Locs.Address2
      || Locs.Address3
      || Locs.Address4 Ship_To_Address,
      Locs.Province Ship_To_Province,
      Locs.County Ship_To_County,
      Locs.City Ship_To_City,
      Locs.State Ship_To_State,
      Locs.Country Ship_To_Country,
      Locs.Postal_Code Ship_To_Postal_Code,
      Locs.Address_Key Ship_To_Address_Key,
      Ps.Payment_Schedule_Id Payment_Sched_Id,
      Ps.Class Trx_Class,
      Ps.Due_Date Due_Date,
      Ps.Acctd_Amount_Due_Remaining Acct_Amonut_Due_Remaining,
      Ps.Amount_Due_Remaining Amount_Due_Remaining,
      Ps.Trx_Number Trx_Num,
      Trx.Trx_Date Invoice_Date,
      (SELECT SUM(Extended_Amount)
      FROM Ra_Customer_Trx_Lines Rctl
      WHERE Rctl.Customer_Trx_Id=Trx.Customer_Trx_Id
      )Invoice_Amount,
      REPLACE (Trx.Purchase_Order, '~', ' ') Po_Number,
      Trx.Interface_Header_Attribute1 Sale_Order_Number,
      TO_CHAR (NULL) Receipt_Number,
      NULL Receipt_Date,
      Ceil (Xxeis.Eis_Rs_Ar_Fin_Com_Util_Pkg.Get_Asof_Date - Ps.Due_Date) Days_Past_Due,
      Ps.Amount_Adjusted Amount_Adjusted,
      Ps.Amount_Applied Amount_Applied,
      Ps.Amount_Credited Amount_Credited,
      Ps.Gl_Date Gl_Date,
      DECODE ( Ps.Invoice_Currency_Code, Gle.Currency_Code, NULL, DECODE (Ps.Exchange_Rate, NULL, '*', NULL) ) Data_Converted,
      NVL (Ps.Exchange_Rate, 1) Ps_Exchange_Rate,
      Xxeis.Eis_Rs_Ar_Fin_Com_Util_Pkg.Amount_Remaining ( Ps.Payment_Schedule_Id, Ps.Due_Date, NULL, NULL, (Ps.Amount_Due_Original)  *NVL(Ps.Exchange_Rate,1), Ps.Amount_Applied, Ps.Amount_Credited, Ps.Amount_Adjusted, Ps.Class ) Outstanding_Amount,
      Xxeis.Eis_Rs_Ar_Fin_Com_Util_Pkg.Amount_Remaining (Ps.Payment_Schedule_Id, Ps.Due_Date, NULL, 0, (Ps.Amount_Due_Original)      *NVL(Ps.Exchange_Rate,1), Ps.Amount_Applied, Ps.Amount_Credited, Ps.Amount_Adjusted, Ps.Class ) Bucket_Current,
      Xxeis.Eis_Rs_Ar_Fin_Com_Util_Pkg.Amount_Remaining (Ps.Payment_Schedule_Id, Ps.Due_Date, 1, 30, (Ps.Amount_Due_Original)        *NVL(Ps.Exchange_Rate,1), Ps.Amount_Applied, Ps.Amount_Credited, Ps.Amount_Adjusted, Ps.Class ) Bucket_1_To_30,
      Xxeis.Eis_Rs_Ar_Fin_Com_Util_Pkg.Amount_Remaining ( Ps.Payment_Schedule_Id, Ps.Due_Date, 31, 60, (Ps.Amount_Due_Original)      *NVL(Ps.Exchange_Rate,1), Ps.Amount_Applied, Ps.Amount_Credited, Ps.Amount_Adjusted, Ps.Class )Bucket_31_To_60,
      Xxeis.Eis_Rs_Ar_Fin_Com_Util_Pkg.Amount_Remaining ( Ps.Payment_Schedule_Id, Ps.Due_Date, 61, 90, (Ps.Amount_Due_Original)      *NVL(Ps.Exchange_Rate,1), Ps.Amount_Applied, Ps.Amount_Credited, Ps.Amount_Adjusted, Ps.Class )Bucket_61_To_90,
      Xxeis.Eis_Rs_Ar_Fin_Com_Util_Pkg.Amount_Remaining ( Ps.Payment_Schedule_Id, Ps.Due_Date, 91, 180, (Ps.Amount_Due_Original)     *NVL(Ps.Exchange_Rate,1), Ps.Amount_Applied, Ps.Amount_Credited, Ps.Amount_Adjusted, Ps.Class ) Bucket_91_To_180,
      Xxeis.Eis_Rs_Ar_Fin_Com_Util_Pkg.Amount_Remaining ( Ps.Payment_Schedule_Id, Ps.Due_Date, 181, 360, (Ps.Amount_Due_Original)    *NVL(Ps.Exchange_Rate,1), Ps.Amount_Applied, Ps.Amount_Credited, Ps.Amount_Adjusted, Ps.Class )Bucket_181_To_360,
      Xxeis.Eis_Rs_Ar_Fin_Com_Util_Pkg.Amount_Remaining ( Ps.Payment_Schedule_Id, Ps.Due_Date, 361, 9999999, (Ps.Amount_Due_Original)*NVL(Ps.Exchange_Rate,1), Ps.Amount_Applied, Ps.Amount_Credited, Ps.Amount_Adjusted, Ps.Class )Bucket_361_Days_And_Above,
      (xxeis.EIS_RS_XXWC_COM_UTIL_PKG.Get_Customer_Total(cust.cust_account_id)                                                       + xxeis.EIS_RS_XXWC_COM_UTIL_PKG.get_Customer_receipt_Total(cust.cust_account_id)) Acct_bal,
      Hou.Name Operating_Unit,
      Hzpc.Name Profile_Class,
      customer_status.meaning Customer_Account_Status,
      Ac.Name Collector,
      Jrdv.Resource_Name Credit_Analyst_Name,
      Jrdv.Source_Mgr_Name Credit_Manager,
      Hzp.Credit_Hold Credit_Hold_Status,
      rt.name payment_terms,
      (SELECT MAX(NVL(ra.apply_date,ra.gl_date))
      FROM ar_receivable_applications ra,
        ar_payment_schedules aps
      WHERE application_type     ='CASH'
      AND ra.status              ='APP'
      AND aps.payment_schedule_id=ra.payment_schedule_id
      AND customer_id            = Ps.Customer_Id
      )APPLIED_DATE,
      party_siteb.party_site_number site_number,
      --Party_Siteb.PARTY_SITE_NAME site_name,
      --SUBSTR(Siteb.LOCATION,1,INSTR(Siteb.LOCATION,'-',1)-1) site_name,
      Siteb.LOCATION site_name,
      --Primary Keys
      Ctt.Cust_Trx_Type_Id,
      Ctt.Org_Id,
      Cust.Cust_Account_Id,
      Party.Party_Id,
      Gcc.Code_Combination_Id,
      Siteb.Site_Use_Id,
      Locb.Location_Id,
      Cust_Siteb.Cust_Acct_Site_Id,
      Party_Sites.Party_Site_Id,
      Hou.Organization_Id,
      Ps.Payment_Schedule_Id
      --descr#flexfield#start
      --descr#flexfield#end
      --gl#accountff#start
      --gl#accountff#end
    FROM Ra_Cust_Trx_Types Ctt,
      Ra_Customer_Trx Trx,
      Hz_Cust_Accounts Cust,
      Hz_Parties Party,
      Ar_Payment_Schedules Ps,
      Ra_Cust_Trx_Line_Gl_Dist Gld,
      Gl_Code_Combinations_Kfv Gcc,
      Gl_Ledgers Gle,
      Fnd_User Fu1,
      Fnd_User Fu2,
      Fnd_Territories_Vl Ter,
      Hz_Cust_Site_Uses Siteb,
      Hz_Locations Locb,
      Hz_Cust_Acct_Sites Cust_Siteb,
      Hz_Party_Sites Party_Siteb ,
      Hz_Cust_Site_Uses Sites,
      Hz_Locations Locs,
      Hz_Cust_Acct_Sites Cust_Sites,
      Hz_Party_Sites Party_Sites ,
      Hr_Organization_Units Hou ,
      Hz_Customer_Profiles Hzp ,
      Hz_Cust_Profile_Classes Hzpc,
      Ar_Collectors Ac,
      jtf_rs_defresources_v jrdv,
      hz_cust_acct_relate cust_rel,
      hz_cust_accounts parent_cust ,
      hz_parties parent_party,
      AR_LOOKUPS customer_status ,
      ra_salesreps jsr,
      Ra_Terms Rt
    WHERE TRUNC (Ps.Gl_Date)                             <= Xxeis.Eis_Rs_Ar_Fin_Com_Util_Pkg.Get_Asof_Date
    AND Trx.Cust_Trx_Type_Id                              = Ctt.Cust_Trx_Type_Id
    AND Trx.Bill_To_Customer_Id                           = Cust.Cust_Account_Id
    AND Cust.Party_Id                                     = Party.Party_Id
    AND Ps.Customer_Trx_Id                                = Trx.Customer_Trx_Id
    AND Trx.Customer_Trx_Id                               = Gld.Customer_Trx_Id
    AND Gld.Account_Class                                 = 'REC'
    AND Gld.Latest_Rec_Flag                               = 'Y'
    AND Gld.Code_Combination_Id                           = Gcc.Code_Combination_Id
    AND Gle.Ledger_Id                                     = Trx.Set_Of_Books_Id
    AND Fu1.User_Id                                       = Trx.Last_Updated_By
    AND Fu2.User_Id                                       = Trx.Created_By
    AND Trx.Bill_To_Site_Use_Id                           = Siteb.Site_Use_Id
    AND Cust_Siteb.Cust_Acct_Site_Id                      = Siteb.Cust_Acct_Site_Id
    AND Cust_Siteb.Party_Site_Id                          = Party_Siteb.Party_Site_Id
    AND Party_Siteb.Location_Id                           = Locb.Location_Id
    AND Trx.Ship_To_Site_Use_Id                           = Sites.Site_Use_Id(+)
    AND Cust_Sites.Cust_Acct_Site_Id(+)                   = Sites.Cust_Acct_Site_Id
    AND Cust_Sites.Party_Site_Id                          = Party_Sites.Party_Site_Id(+)
    AND Party_Sites.Location_Id                           = Locs.Location_Id(+)
    AND Locb.Country                                      = Ter.Territory_Code
    AND Ctt.Org_Id                                        = Hou.Organization_Id
    AND Hzp.Cust_Account_Id                               = Cust.Cust_Account_Id
    AND Hzp.Site_Use_Id                                   = Siteb.Site_Use_Id
    AND hzp.profile_class_id                              = hzpc.profile_class_id
    AND hzp.collector_id                                  = ac.collector_id(+)
    AND Hzp.Credit_Analyst_Id                             = Jrdv.Resource_Id(+)
    AND ps.term_id                                        = rt.term_id(+)
    AND cust_rel.related_cust_account_id(+)               =cust.cust_account_id
    AND cust_rel.cust_account_id                          =parent_cust.cust_account_id(+)
    AND parent_cust.party_id                              = parent_party.party_id(+)
    AND NVL(customer_status.lookup_type,'ACCOUNT_STATUS') = 'ACCOUNT_STATUS'
    AND customer_status.lookup_code(+)                    = HZP.ACCOUNT_STATUS
    AND NVL(cust_rel.status,'A')                          ='A'
    AND jsr.salesrep_id (+)                               = trx.primary_salesrep_id
    AND jsr.org_id (+)                                    = trx.org_id
    --and Trx.trx_number='232047669-00'
    --   AND PARTY.PARTY_ID=200813
    UNION
    SELECT gcc.concatenated_segments concatenated_segments,
      NVL (cust.account_name, party.party_name) customer_name,
      NVL (parent_cust.Account_Name, parent_Party.Party_Name) parent_Customer_Name,
      cust.account_number customer_number,
      PARENT_CUST.ACCOUNT_NUMBER PARENT_CUSTOMER_NUMBER,
      gcc.segment2 location,
      NULL salesrep_name ,
      TO_CHAR (NULL) Trx_Type_Name,
      To_Number (NULL) Trx_Type_Id,
      Arm.Name Receipt_Method_Name,
      Arm.Receipt_Method_Id Receipt_Method_Id,
      Site.Site_Use_Id Contact_Site_Id,
      Site.Site_Use_Code Site_Code,
      Loc.Address1
      || Loc.Address2
      || Loc.Address3
      || Loc.Address4 Address,
      Loc.Province Province,
      Loc.County County,
      Loc.City City,
      Loc.State State,
      Loc.Country Country,
      Loc.Postal_Code,
      Loc.Address_Key,
      Cust.Cust_Account_Id Customer_Id,
      To_Number (NULL) Ship_Contact_Site_Id,
      TO_CHAR (NULL) Ship_Site_Code,
      TO_CHAR (NULL) Ship_To_Address,
      TO_CHAR (NULL) Ship_To_Province,
      TO_CHAR (NULL) Ship_To_County,
      TO_CHAR (NULL) Ship_To_City,
      TO_CHAR (NULL) Ship_To_State,
      TO_CHAR (NULL) Ship_To_Country,
      TO_CHAR (NULL) Ship_To_Postal_Code,
      TO_CHAR (NULL) Ship_To_Address_Key,
      Ps.Payment_Schedule_Id Payment_Sched_Id,
      Ps.Class Trx_Class,
      Ps.Due_Date Due_Date,
      Ps.Acctd_Amount_Due_Remaining,
      Ps.Amount_Due_Remaining,
      TO_CHAR (NULL) Trx_Num,
      NULL Invoice_Date,
      0 Invoice_Amount,
      TO_CHAR (NULL) Po_Number,
      TO_CHAR (NULL) Sale_Order_Number,
      Ps.Trx_Number Receipt_Number,
      Cr.Receipt_Date Receipt_Date,
      Ceil (Xxeis.Eis_Rs_Ar_Fin_Com_Util_Pkg.Get_Asof_Date - Ps.Due_Date) Days_Past_Due,
      Ps.Amount_Adjusted Amount_Adjusted,
      Ps.Amount_Applied Amount_Applied,
      Ps.Amount_Credited Amount_Credited,
      Ps.Gl_Date Gl_Date,
      DECODE (Ps.Invoice_Currency_Code, Gle.Currency_Code, NULL, DECODE (Ps.Exchange_Rate, NULL, '*', NULL) ) Data_Converted,
      NVL (Ps.Exchange_Rate, 1) Ps_Exchange_Rate,
      Xxeis.Eis_Rs_Ar_Fin_Com_Util_Pkg.Amount_Remaining ( Ps.Payment_Schedule_Id, Ps.Due_Date, NULL, NULL, Ps.Amount_Due_Original  *NVL(Ps.Exchange_Rate,1), Ps.Amount_Applied, Ps.Amount_Credited, Ps.Amount_Adjusted, Ps.Class ) Outstanding_Amount,
      Xxeis.Eis_Rs_Ar_Fin_Com_Util_Pkg.Amount_Remaining ( Ps.Payment_Schedule_Id, Ps.Due_Date, NULL, 0, Ps.Amount_Due_Original     *NVL(Ps.Exchange_Rate,1), Ps.Amount_Applied, Ps.Amount_Credited, Ps.Amount_Adjusted, Ps.Class ) Bucket_Current,
      Xxeis.Eis_Rs_Ar_Fin_Com_Util_Pkg.Amount_Remaining ( Ps.Payment_Schedule_Id, Ps.Due_Date, 1, 30, Ps.Amount_Due_Original       *NVL(Ps.Exchange_Rate,1), Ps.Amount_Applied, Ps.Amount_Credited, Ps.Amount_Adjusted, Ps.Class ) Bucket_1_To_30,
      Xxeis.Eis_Rs_Ar_Fin_Com_Util_Pkg.Amount_Remaining ( Ps.Payment_Schedule_Id, Ps.Due_Date, 31, 60, Ps.Amount_Due_Original      *NVL(Ps.Exchange_Rate,1), Ps.Amount_Applied, Ps.Amount_Credited, Ps.Amount_Adjusted, Ps.Class ) Bucket_31_To_60,
      Xxeis.Eis_Rs_Ar_Fin_Com_Util_Pkg.Amount_Remaining ( Ps.Payment_Schedule_Id, Ps.Due_Date, 61, 90, Ps.Amount_Due_Original      *NVL(Ps.Exchange_Rate,1), Ps.Amount_Applied, Ps.Amount_Credited, Ps.Amount_Adjusted, Ps.Class ) Bucket_61_To_90,
      Xxeis.Eis_Rs_Ar_Fin_Com_Util_Pkg.Amount_Remaining ( Ps.Payment_Schedule_Id, Ps.Due_Date, 91, 180, Ps.Amount_Due_Original     *NVL(Ps.Exchange_Rate,1), Ps.Amount_Applied, Ps.Amount_Credited, Ps.Amount_Adjusted, Ps.Class ) Bucket_91_To_180,
      Xxeis.Eis_Rs_Ar_Fin_Com_Util_Pkg.Amount_Remaining ( Ps.Payment_Schedule_Id, Ps.Due_Date, 181, 360, Ps.Amount_Due_Original    *NVL(Ps.Exchange_Rate,1), Ps.Amount_Applied, Ps.Amount_Credited, Ps.Amount_Adjusted, Ps.Class ) Bucket_181_To_360,
      Xxeis.Eis_Rs_Ar_Fin_Com_Util_Pkg.Amount_Remaining ( Ps.Payment_Schedule_Id, Ps.Due_Date, 361, 9999999, Ps.Amount_Due_Original*NVL(Ps.Exchange_Rate,1), Ps.Amount_Applied, Ps.Amount_Credited, Ps.Amount_Adjusted, Ps.Class ) Bucket_361_Days_And_Above,
      (xxeis.EIS_RS_XXWC_COM_UTIL_PKG.Get_Customer_Total(cust.cust_account_id)                                                     + xxeis.EIS_RS_XXWC_COM_UTIL_PKG.get_Customer_receipt_Total(cust.cust_account_id)) Acct_bal,
      Hou.Name Operating_Unit,
      Hzpc.Name Profile_Class,
      customer_status.meaning Customer_Account_Status,
      Ac.Name Collector,
      Jrdv.Resource_Name Credit_Analyst_Name,
      Jrdv.Source_Mgr_Name Credit_Manager,
      Hzp.Credit_Hold Credit_Hold_Status,
      Rt.Name Payment_Terms,
      (SELECT MAX(NVL(ra.apply_date,ra.gl_date))
      FROM ar_receivable_applications ra,
        ar_payment_schedules aps
      WHERE Application_Type     ='CASH'
      AND ra.status              ='APP'
      AND aps.payment_schedule_id=ra.payment_schedule_id
      AND customer_id            = Ps.Customer_Id
      )APPLIED_DATE,
      party_sites.party_site_number site_number,
      --  party_sites.PARTY_SITE_NAME site_name,
      --SUBSTR(Site.LOCATION,1,INSTR(Site.LOCATION,'-',1)-1) site_name,
      Site.LOCATION site_name,
      --Primary Keys
      NULL Cust_Trx_Type_Id,
      NULL Org_Id,
      Cust.Cust_Account_Id,
      Party.Party_Id,
      Gcc.Code_Combination_Id,
      Site.Site_Use_Id,
      Loc.Location_Id,
      Cust_Sites.Cust_Acct_Site_Id,
      Party_Sites.Party_Site_Id,
      Hou.Organization_Id,
      Ps.Payment_Schedule_Id
      --descr#flexfield#start
      --descr#flexfield#end
      --gl#accountff#start
      --gl#accountff#end
    FROM Ar_Payment_Schedules Ps,
      Ar_Cash_Receipts Cr,
      Ar_Receipt_Methods Arm,
      Ar_Receipt_Method_Accounts Arma,
      Gl_Code_Combinations_Kfv Gcc,
      Gl_Ledgers Gle,
      Hz_Cust_Accounts Cust,
      Hz_Parties Party,
      Hz_Cust_Site_Uses Site,
      Hz_Cust_Acct_Sites Cust_Sites,
      Hz_Party_Sites Party_Sites,
      Hz_Locations Loc,
      Hr_Operating_Units Hou,
      Hz_Customer_Profiles Hzp,
      Hz_Cust_Profile_Classes Hzpc,
      Ar_Collectors Ac,
      jtf_rs_defresources_v jrdv,
      hz_cust_acct_relate cust_rel,
      hz_cust_accounts parent_cust ,
      hz_parties parent_party,
      AR_LOOKUPS CUSTOMER_STATUS ,
      Ra_Terms Rt
    WHERE TRUNC (Ps.Gl_Date)                             <= Xxeis.Eis_Rs_Ar_Fin_Com_Util_Pkg.Get_Asof_Date
    AND Ps.Cash_Receipt_Id                                = Cr.Cash_Receipt_Id
    AND Arm.Receipt_Method_Id                             = Cr.Receipt_Method_Id
    AND Arm.Receipt_Method_Id                             = Arma.Receipt_Method_Id
    AND Arma.Receipt_Method_Id                            = Cr.Receipt_Method_Id
    AND Arma.Unapplied_Ccid                               = Gcc.Code_Combination_Id
    AND Gle.Ledger_Id                                     = Cr.Set_Of_Books_Id
    AND Ps.Customer_Id                                    = Cust.Cust_Account_Id(+)
    AND Cust.Party_Id                                     = Party.Party_Id(+)
    AND Ps.Customer_Site_Use_Id                           = Site.Site_Use_Id(+)
    AND Site.Cust_Acct_Site_Id                            = Cust_Sites.Cust_Acct_Site_Id(+)
    AND Cust_Sites.Party_Site_Id                          = Party_Sites.Party_Site_Id(+)
    AND Party_Sites.Location_Id                           = Loc.Location_Id(+)
    AND Ps.Org_Id                                         = Hou.Organization_Id
    AND Hzp.Cust_Account_Id                               = Cust.Cust_Account_Id
    AND Hzp.Site_Use_Id                                   = Site.Site_Use_Id
    AND hzp.profile_class_id                              = hzpc.profile_class_id
    AND hzp.collector_id                                  = ac.collector_id(+)
    AND Hzp.Credit_Analyst_Id                             = Jrdv.Resource_Id(+)
    AND ps.term_id                                        = rt.term_id(+)
    AND cust_rel.related_cust_account_id(+)               =cust.cust_account_id
    AND cust_rel.cust_account_id                          =parent_cust.cust_account_id(+)
    AND parent_cust.party_id                              = parent_party.party_id(+)
    AND NVL(customer_status.lookup_type,'ACCOUNT_STATUS') = 'ACCOUNT_STATUS'
    AND customer_status.lookup_code(+)                    = HZP.ACCOUNT_STATUS
    AND NVL(CUST_REL.STATUS,'A')                          ='A'
      --  AND PARTY.PARTY_ID=200813
    AND NOT EXISTS
      (SELECT 1
      FROM Ar_Cash_Receipt_History Crh1
      WHERE Crh1.Cash_Receipt_Id = Cr.Cash_Receipt_Id
      AND TRUNC (Crh1.Gl_Date)  <= Xxeis.Eis_Rs_Ar_Fin_Com_Util_Pkg.Get_Asof_Date
      AND Status                 = 'REVERSED'
      )
    )MAIN
/
set scan on define on
prompt Creating View Data for Accounts Receivable Aging Report
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_AR_AGING_BAD_DEBT_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_AR_AGING_BAD_DEBT_V',222,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Xxwc Ar Aging Bad Debt V','EXAABDV');
--Delete View Columns for EIS_XXWC_AR_AGING_BAD_DEBT_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_AR_AGING_BAD_DEBT_V',222,FALSE);
--Inserting View Columns for EIS_XXWC_AR_AGING_BAD_DEBT_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_DEBT_V','CREDIT_HOLD_STATUS',222,'Credit Hold Status','CREDIT_HOLD_STATUS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Credit Hold Status');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_DEBT_V','CREDIT_MANAGER',222,'Credit Manager','CREDIT_MANAGER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Credit Manager');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_DEBT_V','CREDIT_ANALYST_NAME',222,'Credit Analyst Name','CREDIT_ANALYST_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Credit Analyst Name');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_DEBT_V','COLLECTOR',222,'Collector','COLLECTOR','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Collector');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_DEBT_V','CUSTOMER_ACCOUNT_STATUS',222,'Customer Account Status','CUSTOMER_ACCOUNT_STATUS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Account Status');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_DEBT_V','PROFILE_CLASS',222,'Profile Class','PROFILE_CLASS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Profile Class');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_DEBT_V','BUCKET_361_DAYS_AND_ABOVE',222,'Bucket 361 Days And Above','BUCKET_361_DAYS_AND_ABOVE','','','','XXEIS_RS_ADMIN','NUMBER','','','Bucket 361 Days And Above');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_DEBT_V','BUCKET_181_TO_360',222,'Bucket 181 To 360','BUCKET_181_TO_360','','','','XXEIS_RS_ADMIN','NUMBER','','','Bucket 181 To 360');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_DEBT_V','BUCKET_61_TO_90',222,'Bucket 61 To 90','BUCKET_61_TO_90','','','','XXEIS_RS_ADMIN','NUMBER','','','Bucket 61 To 90');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_DEBT_V','BUCKET_31_TO_60',222,'Bucket 31 To 60','BUCKET_31_TO_60','','','','XXEIS_RS_ADMIN','NUMBER','','','Bucket 31 To 60');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_DEBT_V','BUCKET_1_TO_30',222,'Bucket 1 To 30','BUCKET_1_TO_30','','','','XXEIS_RS_ADMIN','NUMBER','','','Bucket 1 To 30');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_DEBT_V','BUCKET_CURRENT',222,'Bucket Current','BUCKET_CURRENT','','','','XXEIS_RS_ADMIN','NUMBER','','','Bucket Current');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_DEBT_V','DAYS_PAST_DUE',222,'Days Past Due','DAYS_PAST_DUE','','','','XXEIS_RS_ADMIN','NUMBER','','','Days Past Due');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_DEBT_V','RECEIPT_NUMBER',222,'Receipt Number','RECEIPT_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Receipt Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_DEBT_V','TRX_NUM',222,'Trx Num','TRX_NUM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Trx Num');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_DEBT_V','AMOUNT_DUE_REMAINING',222,'Amount Due Remaining','AMOUNT_DUE_REMAINING','','','','XXEIS_RS_ADMIN','NUMBER','','','Amount Due Remaining');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_DEBT_V','DUE_DATE',222,'Due Date','DUE_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Due Date');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_DEBT_V','BILL_TO_CUSTOMER_NUMBER',222,'Bill To Customer Number','BILL_TO_CUSTOMER_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Bill To Customer Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_DEBT_V','BILL_TO_CUSTOMER_NAME',222,'Bill To Customer Name','BILL_TO_CUSTOMER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Bill To Customer Name');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_DEBT_V','INVOICE_AMOUNT',222,'Invoice Amount','INVOICE_AMOUNT','','','','XXEIS_RS_ADMIN','NUMBER','','','Invoice Amount');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_DEBT_V','INVOICE_DATE',222,'Invoice Date','INVOICE_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Invoice Date');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_DEBT_V','RECEIPT_DATE',222,'Receipt Date','RECEIPT_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Receipt Date');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_DEBT_V','PAYMENT_TERMS',222,'Payment Terms','PAYMENT_TERMS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Payment Terms');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_DEBT_V','BUCKET_91_TO_180',222,'Bucket 91 To 180','BUCKET_91_TO_180','','','','XXEIS_RS_ADMIN','NUMBER','','','Bucket 91 To 180');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_DEBT_V','APPLIED_DATE',222,'Applied Date','APPLIED_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Applied Date');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_DEBT_V','PARENT_CUSTOMER_NAME',222,'Parent Customer Name','PARENT_CUSTOMER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Parent Customer Name');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_DEBT_V','PARENT_CUSTOMER_NUMBER',222,'Parent Customer Number','PARENT_CUSTOMER_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Parent Customer Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_DEBT_V','SALESREP_NAME',222,'Salesrep Name','SALESREP_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Salesrep Name');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_DEBT_V','ACCT_BAL',222,'Acct Bal','ACCT_BAL','','','','XXEIS_RS_ADMIN','NUMBER','','','Acct Bal');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_DEBT_V','LOCATION',222,'Location','LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Location');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_DEBT_V','TRX_TYPE_NAME',222,'Trx Type Name','TRX_TYPE_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Trx Type Name');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_DEBT_V','SITE_NUMBER',222,'Site Number','SITE_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Site Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_DEBT_V','OUTSTANDING_AMOUNT',222,'Outstanding Amount','OUTSTANDING_AMOUNT','','','','XXEIS_RS_ADMIN','NUMBER','','','Outstanding Amount');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_DEBT_V','SITE_NAME',222,'Site Name','SITE_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Site Name');
--Inserting View Components for EIS_XXWC_AR_AGING_BAD_DEBT_V
--Inserting View Component Joins for EIS_XXWC_AR_AGING_BAD_DEBT_V
END;
/
set scan on define on
prompt Creating Report LOV Data for Accounts Receivable Aging Report
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Accounts Receivable Aging Report
xxeis.eis_rs_ins.lov( 222,'select account_number from hz_cust_accounts','null','Customer Number','Displays List of Values for Customer Number','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 222,'select name from ar_collectors','null','Collector','Displays list of values for Collector','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 222,'select distinct name from hz_cust_profile_classes','null','PROFILE CLASS','This LOV lists all the profile classes of the customers','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 222,'select  customer_status.meaning Status
from  fnd_lookup_values_vl customer_status
where customer_status.lookup_type= ''CODE_STATUS''
 and customer_status.view_application_id=222','','Customer Status','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 222,'select distinct jrse.source_name salesrepname,jrs.salesrep_number salesrepnumber
  from     jtf_rs_salesreps jrs,    jtf_rs_resource_extns jrse
  where JRS.RESOURCE_ID            =JRSE.RESOURCE_ID
','','AR Sales Person LOV','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 222,'SELECT  resource_name resource_name
 FROM jtf_rs_role_relations a,
  jtf_rs_roles_vl b,
  jtf_rs_resource_extns_vl c
WHERE a.role_resource_type  = ''RS_INDIVIDUAL''
AND a.role_resource_id      = c.resource_id
AND a.role_id               = b.role_id
AND b.role_code             = ''CREDIT_ANALYST''
AND c.category              = ''EMPLOYEE''
AND NVL(a.delete_flag,''N'') <> ''Y''
Order BY resource_name','','Credit Analyst','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 222,'select distinct credit_hold from hz_customer_profiles','','Credit Holds','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 222,'SELECT DISTINCT SEGMENT2 Location,
  FV.description
FROM GL_CODE_COMBINATIONS_KFV GCC,
  FND_FLEX_VALUES_vL FV
WHERE 1                   =1
AND FV.FLEX_VALUE(+)      =GCC.SEGMENT2
AND FV.FLEX_VALUE_SET_ID IN
  (SELECT FS.FLEX_VALUE_SET_ID
  FROM FND_FLEX_VALUE_SETS FS
  WHERE FS.FLEX_VALUE_SET_NAME =''XXCUS_GL_LOCATION''
  )
order by SEGMENT2','','AR Branch Location LOV','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 222,'select account_name,account_number from hz_cust_accounts','','AR Customer Name LOV','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 222,'SELECT  DISTINCT PARTY_SITE_NUMBER SITE_NUMBER,Site.location SITE_NAME
FROM  HZ_PARTIES HP,
      HZ_CUST_ACCOUNTS HCS,
      HZ_PARTY_SITES PARTY_SITES,
      Hz_Cust_Site_Uses Site,
      HZ_CUST_ACCT_SITES CUST_SITES
WHERE HP.PARTY_ID =HCS.PARTY_ID
AND HCS.CUST_ACCOUNT_ID =CUST_SITES.CUST_ACCOUNT_ID 
AND SITE.CUST_ACCT_SITE_ID= CUST_SITES.CUST_ACCT_SITE_ID(+)
AND CUST_SITES.PARTY_SITE_ID = PARTY_SITES.PARTY_SITE_ID(+)
order by PARTY_SITE_NUMBER','','AR Site Number LOV','','XXEIS_RS_ADMIN',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Accounts Receivable Aging Report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Accounts Receivable Aging Report
xxeis.eis_rs_utility.delete_report_rows( 'Accounts Receivable Aging Report' );
--Inserting Report - Accounts Receivable Aging Report
xxeis.eis_rs_ins.r( 222,'Accounts Receivable Aging Report','','','','','','XXEIS_RS_ADMIN','EIS_XXWC_AR_AGING_BAD_DEBT_V','Y','','','XXEIS_RS_ADMIN','N','N','White Cap Reports','RTF,PDF,','CSV,HTML,Html Summary,XML,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - Accounts Receivable Aging Report
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging Report',222,'BILL_TO_CUSTOMER_NAME','Customer Name','Bill To Customer Name','','','','','1','N','','ROW_FIELD','','','','1','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_AGING_BAD_DEBT_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging Report',222,'BILL_TO_CUSTOMER_NUMBER','Customer Number','Bill To Customer Number','','','','','2','N','','ROW_FIELD','','','','2','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_AGING_BAD_DEBT_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging Report',222,'BUCKET_181_TO_360','181 To 360 Days','Bucket 181 To 360','','','','','20','N','','DATA_FIELD','','SUM','','8','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_AGING_BAD_DEBT_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging Report',222,'BUCKET_1_TO_30','1 To 30 Days','Bucket 1 To 30','','','','','16','N','','DATA_FIELD','','SUM','','4','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_AGING_BAD_DEBT_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging Report',222,'BUCKET_31_TO_60','31 To 60 Days','Bucket 31 To 60','','','','','17','N','','DATA_FIELD','','SUM','','5','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_AGING_BAD_DEBT_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging Report',222,'BUCKET_361_DAYS_AND_ABOVE','361+Days Past Due','Bucket 361 Days And Above','','','','','21','N','','DATA_FIELD','','SUM','','9','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_AGING_BAD_DEBT_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging Report',222,'BUCKET_61_TO_90','61 To 90 Days','Bucket 61 To 90','','','','','18','N','','DATA_FIELD','','SUM','','6','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_AGING_BAD_DEBT_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging Report',222,'BUCKET_CURRENT',' Current','Bucket Current','','','','','15','N','','DATA_FIELD','','SUM','','3','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_AGING_BAD_DEBT_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging Report',222,'CREDIT_HOLD_STATUS','Site Credit Hold Status','Credit Hold Status','','','','','26','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_AGING_BAD_DEBT_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging Report',222,'CUSTOMER_ACCOUNT_STATUS','Customer Account Status','Customer Account Status','','','','','23','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_AGING_BAD_DEBT_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging Report',222,'DAYS_PAST_DUE','Age','Days Past Due','','','','','13','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_AGING_BAD_DEBT_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging Report',222,'INVOICE_AMOUNT','Transaction Balance','Invoice Amount','','','','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_AGING_BAD_DEBT_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging Report',222,'INVOICE_DATE','Invoice Date','Invoice Date','','','','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_AGING_BAD_DEBT_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging Report',222,'PROFILE_CLASS','Profile Class','Profile Class','','','','','25','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_AGING_BAD_DEBT_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging Report',222,'RECEIPT_NUMBER','Receipt Number','Receipt Number','','','','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_AGING_BAD_DEBT_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging Report',222,'TRX_NUM','Invoice Number','Trx Num','','','','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_AGING_BAD_DEBT_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging Report',222,'DUE_DATE','Due Date','Due Date','','','','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_AGING_BAD_DEBT_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging Report',222,'AMOUNT_DUE_REMAINING','Transaction Remaining Balance','Amount Due Remaining','','','','','12','N','','DATA_FIELD','','SUM','','1','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_AGING_BAD_DEBT_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging Report',222,'COLLECTOR','Collector','Collector','','','','','24','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_AGING_BAD_DEBT_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging Report',222,'CREDIT_ANALYST_NAME','Credit Analyst','Credit Analyst Name','','','','','27','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_AGING_BAD_DEBT_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging Report',222,'BUCKET_91_TO_180','91 To 180 Days','Bucket 91 To 180','','','','','19','N','','DATA_FIELD','','SUM','','7','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_AGING_BAD_DEBT_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging Report',222,'APPLIED_DATE','Last Payment Date','Applied Date','','','','','22','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_AGING_BAD_DEBT_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging Report',222,'SALESREP_NAME','Salesrep Name','Salesrep Name','','','','','28','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_AGING_BAD_DEBT_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging Report',222,'ACCT_BAL','Account Total Balance','Acct Bal','','','','','29','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_AGING_BAD_DEBT_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging Report',222,'LOCATION','Branch Location','Location','','','','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_AGING_BAD_DEBT_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging Report',222,'TRX_TYPE_NAME','Transaction Type','Trx Type Name','','','','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_AGING_BAD_DEBT_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging Report',222,'SITE_NUMBER','Site Number','Site Number','','','','','3','N','','ROW_FIELD','','','','3','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_AGING_BAD_DEBT_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging Report',222,'OUTSTANDING_AMOUNT','Outstanding Balance','Outstanding Amount','','','','','14','N','','DATA_FIELD','','SUM','','2','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_AGING_BAD_DEBT_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging Report',222,'SITE_NAME','Site Name','Site Name','','','','','4','N','','ROW_FIELD','','','','4','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_AGING_BAD_DEBT_V','','');
--Inserting Report Parameters - Accounts Receivable Aging Report
xxeis.eis_rs_ins.rp( 'Accounts Receivable Aging Report',222,'Customer Name','Customer Name','BILL_TO_CUSTOMER_NAME','IN','AR Customer Name LOV','','VARCHAR2','N','Y','1','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Accounts Receivable Aging Report',222,'Customer Account Status','Customer Account Status','CUSTOMER_ACCOUNT_STATUS','IN','Customer Status','','VARCHAR2','N','Y','3','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Accounts Receivable Aging Report',222,'Collector','Collector','COLLECTOR','IN','Collector','','VARCHAR2','N','Y','4','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Accounts Receivable Aging Report',222,'Credit Analyst','Credit Analyst','CREDIT_ANALYST_NAME','IN','Credit Analyst','','VARCHAR2','N','Y','5','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Accounts Receivable Aging Report',222,'Credit Hold','Credit Hold','CREDIT_HOLD_STATUS','IN','Credit Holds','','VARCHAR2','N','Y','6','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Accounts Receivable Aging Report',222,'Profile Class','Profile Class','PROFILE_CLASS','IN','PROFILE CLASS','','VARCHAR2','N','Y','7','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Accounts Receivable Aging Report',222,'As Of Date','As Of Date','','IN','','select trunc(sysdate) from dual','DATE','N','Y','8','','N','SQL','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Accounts Receivable Aging Report',222,'Sales Rep','Sales Rep','SALESREP_NAME','IN','AR Sales Person LOV','','VARCHAR2','N','Y','9','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Accounts Receivable Aging Report',222,'Location','Location','LOCATION','IN','AR Branch Location LOV','','VARCHAR2','N','Y','10','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Accounts Receivable Aging Report',222,'Customer Number','Customer Number','BILL_TO_CUSTOMER_NUMBER','IN','Customer Number','','VARCHAR2','N','Y','2','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Accounts Receivable Aging Report',222,'Site Number','Site Number','SITE_NUMBER','IN','AR Site Number LOV','','VARCHAR2','N','Y','11','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
--Inserting Report Conditions - Accounts Receivable Aging Report
xxeis.eis_rs_ins.rcn( 'Accounts Receivable Aging Report',222,'BILL_TO_CUSTOMER_NAME','IN',':Customer Name','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Accounts Receivable Aging Report',222,'CUSTOMER_ACCOUNT_STATUS','IN',':Customer Account Status','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Accounts Receivable Aging Report',222,'COLLECTOR','IN',':Collector','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Accounts Receivable Aging Report',222,'CREDIT_ANALYST_NAME','IN',':Credit Analyst','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Accounts Receivable Aging Report',222,'CREDIT_HOLD_STATUS','IN',':Credit Hold','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Accounts Receivable Aging Report',222,'PROFILE_CLASS','IN',':Profile Class','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Accounts Receivable Aging Report',222,'SALESREP_NAME','IN',':Account manager','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Accounts Receivable Aging Report',222,'SALESREP_NAME','IN',':Sales Rep','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Accounts Receivable Aging Report',222,'LOCATION','IN',':Location','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Accounts Receivable Aging Report',222,'SITE_NUMBER','IN',':Site Number','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Accounts Receivable Aging Report',222,'BILL_TO_CUSTOMER_NUMBER','IN',':Customer Number','','','Y','','Y','XXEIS_RS_ADMIN');
--Inserting Report Sorts - Accounts Receivable Aging Report
xxeis.eis_rs_ins.rs( 'Accounts Receivable Aging Report',222,'BILL_TO_CUSTOMER_NAME','ASC','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rs( 'Accounts Receivable Aging Report',222,'SITE_NUMBER','ASC','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rs( 'Accounts Receivable Aging Report',222,'INVOICE_DATE','ASC','XXEIS_RS_ADMIN');
--Inserting Report Triggers - Accounts Receivable Aging Report
xxeis.eis_rs_ins.rt( 'Accounts Receivable Aging Report',222,'Begin

 xxeis.eis_rs_ar_fin_com_util_pkg.set_asof_date(:As Of Date);

End;','B','Y','XXEIS_RS_ADMIN');
--Inserting Report Templates - Accounts Receivable Aging Report
--Inserting Report Portals - Accounts Receivable Aging Report
--Inserting Report Dashboards - Accounts Receivable Aging Report
--Inserting Report Security - Accounts Receivable Aging Report
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging Report','222','','50871',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging Report','222','','50638',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging Report','222','','50622',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging Report','401','','50872',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging Report','222','','20778',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging Report','222','','21404',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging Report','222','','20678',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging Report','','10010432','',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging Report','','LC053655','',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging Report','','RB054040','',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging Report','','RV003897','',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging Report','','SO004816','',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging Report','','SS084202','',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging Report','401','','50835',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging Report','401','','50840',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging Report','222','','50846',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging Report','222','','50845',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging Report','222','','50847',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging Report','222','','50848',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging Report','222','','50849',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging Report','222','','50944',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging Report','101','','50720',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging Report','101','','50723',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging Report','101','','50722',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging Report','20005','','50880',222,'XXEIS_RS_ADMIN','');
END;
/
set scan on define on
