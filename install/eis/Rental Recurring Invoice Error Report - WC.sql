--Report Name            : Rental Recurring Invoice Error Report - WC
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data EIS_XXWC_OM_RENTAL_INV_ERROR_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_OM_RENTAL_INV_ERROR_V
xxeis.eis_rsc_ins.v( 'EIS_XXWC_OM_RENTAL_INV_ERROR_V',660,'','','','','SA059956','XXEIS','Eis Xxwc Om Rental Inv Error V','EXORIEV','','','VIEW','US','','');
--Delete Object Columns for EIS_XXWC_OM_RENTAL_INV_ERROR_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_OM_RENTAL_INV_ERROR_V',660,FALSE);
--Inserting Object Columns for EIS_XXWC_OM_RENTAL_INV_ERROR_V
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_RENTAL_INV_ERROR_V','ORDER_CREATION_DATE',660,'Order Creation Date','ORDER_CREATION_DATE','','','','SA059956','DATE','','','Order Creation Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_RENTAL_INV_ERROR_V','BRANCH',660,'Branch','BRANCH','','','','SA059956','VARCHAR2','','','Branch','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_RENTAL_INV_ERROR_V','ORDER_TYPE',660,'Order Type','ORDER_TYPE','','','','SA059956','VARCHAR2','','','Order Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_RENTAL_INV_ERROR_V','ERROR_MESSAGE',660,'Error Message','ERROR_MESSAGE','','','','SA059956','VARCHAR2','','','Error Message','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_RENTAL_INV_ERROR_V','ORDER_NUMBER',660,'Order Number','ORDER_NUMBER','','','','SA059956','NUMBER','','','Order Number','','','','US');
--Inserting Object Components for EIS_XXWC_OM_RENTAL_INV_ERROR_V
--Inserting Object Component Joins for EIS_XXWC_OM_RENTAL_INV_ERROR_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
prompt Creating Report LOV Data for Rental Recurring Invoice Error Report - WC
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - Rental Recurring Invoice Error Report - WC
xxeis.eis_rsc_ins.lov( 660,'SELECT organization_code branch,
  organization_name
FROM org_organization_definitions ood
WHERE SYSDATE < NVL (ood.disable_date, SYSDATE + 1)
AND EXISTS
  (SELECT 1
  FROM xxeis.eis_org_access_v
  WHERE organization_id = ood.organization_id
  )','','XXWC OM Organization Code','','SA059956',NULL,'Y','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
prompt Creating Report Data for Rental Recurring Invoice Error Report - WC
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Rental Recurring Invoice Error Report - WC
xxeis.eis_rsc_utility.delete_report_rows( 'Rental Recurring Invoice Error Report - WC' );
--Inserting Report - Rental Recurring Invoice Error Report - WC
xxeis.eis_rsc_ins.r( 660,'Rental Recurring Invoice Error Report - WC','','Rental Invoice Error report','','','','SA059956','EIS_XXWC_OM_RENTAL_INV_ERROR_V','Y','','','SA059956','','N','White Cap Reports','','CSV,EXCEL,','N','','','','','','','','US','','','','');
--Inserting Report Columns - Rental Recurring Invoice Error Report - WC
xxeis.eis_rsc_ins.rc( 'Rental Recurring Invoice Error Report - WC',660,'BRANCH','Branch','Branch','','','default','','5','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_RENTAL_INV_ERROR_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Rental Recurring Invoice Error Report - WC',660,'ORDER_CREATION_DATE','Order Creation Date','Order Creation Date','','','default','','6','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_RENTAL_INV_ERROR_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Rental Recurring Invoice Error Report - WC',660,'ORDER_NUMBER','Order Number','Order Number','','~~~','default','','1','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_RENTAL_INV_ERROR_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Rental Recurring Invoice Error Report - WC',660,'ORDER_TYPE','Order Type','Order Type','','','default','','4','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_RENTAL_INV_ERROR_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Rental Recurring Invoice Error Report - WC',660,'Error_Msg','Error Msg','','VARCHAR2','','default','','3','Y','','','','','','','(replace(replace(EXORIEV.ERROR_MESSAGE,chr(13),''''),chr(10),''''))','SA059956','N','N','','','','','','US','');
--Inserting Report Parameters - Rental Recurring Invoice Error Report - WC
xxeis.eis_rsc_ins.rp( 'Rental Recurring Invoice Error Report - WC',660,'Branch','Branch','BRANCH','IN','XXWC OM Organization Code','','VARCHAR2','N','Y','1','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_OM_RENTAL_INV_ERROR_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Rental Recurring Invoice Error Report - WC',660,'Creation Date From','Creation Date From','ORDER_CREATION_DATE','>=','','','DATE','N','Y','2','Y','Y','CONSTANT','SA059956','Y','N','','Start Date','','EIS_XXWC_OM_RENTAL_INV_ERROR_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Rental Recurring Invoice Error Report - WC',660,'Creation Date To','Creation Date To','ORDER_CREATION_DATE','<=','','','DATE','N','Y','3','Y','Y','CONSTANT','SA059956','Y','N','','End Date','','EIS_XXWC_OM_RENTAL_INV_ERROR_V','','','US','');
--Inserting Dependent Parameters - Rental Recurring Invoice Error Report - WC
--Inserting Report Conditions - Rental Recurring Invoice Error Report - WC
xxeis.eis_rsc_ins.rcnh( 'Rental Recurring Invoice Error Report - WC',660,'BRANCH IN :Branch ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','BRANCH','','Branch','','','','','EIS_XXWC_OM_RENTAL_INV_ERROR_V','','','','','','IN','Y','Y','','','','','1',660,'Rental Recurring Invoice Error Report - WC','BRANCH IN :Branch ');
xxeis.eis_rsc_ins.rcnh( 'Rental Recurring Invoice Error Report - WC',660,'TRUNC(ORDER_CREATION_DATE) >= :Creation Date From ','ADVANCED','','1#$#','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','Creation Date From','','','','','','','','','','','GREATER_THAN_EQUALS','Y','Y','TRUNC(ORDER_CREATION_DATE)','','','','1',660,'Rental Recurring Invoice Error Report - WC','TRUNC(ORDER_CREATION_DATE) >= :Creation Date From ');
xxeis.eis_rsc_ins.rcnh( 'Rental Recurring Invoice Error Report - WC',660,'TRUNC(ORDER_CREATION_DATE) <= :Creation Date To ','ADVANCED','','1#$#','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','Creation Date To','','','','','','','','','','','LESS_THAN_EQUALS','Y','Y','TRUNC(ORDER_CREATION_DATE)','','','','1',660,'Rental Recurring Invoice Error Report - WC','TRUNC(ORDER_CREATION_DATE) <= :Creation Date To ');
--Inserting Report Sorts - Rental Recurring Invoice Error Report - WC
xxeis.eis_rsc_ins.rs( 'Rental Recurring Invoice Error Report - WC',660,'BRANCH','ASC','SA059956','1','');
xxeis.eis_rsc_ins.rs( 'Rental Recurring Invoice Error Report - WC',660,'ORDER_NUMBER','ASC','SA059956','2','');
--Inserting Report Triggers - Rental Recurring Invoice Error Report - WC
--inserting report templates - Rental Recurring Invoice Error Report - WC
--Inserting Report Portals - Rental Recurring Invoice Error Report - WC
--inserting report dashboards - Rental Recurring Invoice Error Report - WC
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Rental Recurring Invoice Error Report - WC','660','EIS_XXWC_OM_RENTAL_INV_ERROR_V','EIS_XXWC_OM_RENTAL_INV_ERROR_V','N','');
--inserting report security - Rental Recurring Invoice Error Report - WC
xxeis.eis_rsc_ins.rsec( 'Rental Recurring Invoice Error Report - WC','660','','XXWC_RENTAL_OM_PRICING_SUPER',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Rental Recurring Invoice Error Report - WC','20005','','XXWC_VIEW_ALL_EIS_REPORTS',660,'SA059956','','','');
--Inserting Report Pivots - Rental Recurring Invoice Error Report - WC
--Inserting Report   Version details- Rental Recurring Invoice Error Report - WC
xxeis.eis_rsc_ins.rv( 'Rental Recurring Invoice Error Report - WC','','Rental Recurring Invoice Error Report - WC','AB063501');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
