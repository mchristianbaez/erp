--Report Name            : EIS Delete Favorite Report List
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating View Data for EIS Delete Favorite Report List
set scan off define off
DECLARE
BEGIN 
--Inserting View XXEIS_2156499_TIVHBO_V
xxeis.eis_rs_ins.v( 'XXEIS_2156499_TIVHBO_V',85000,'Paste SQL View for EIS Delete Favorite Report List','1.0','','','SA059956','APPS','EIS Delete Favorite Report List View','X2TV','','');
--Delete View Columns for XXEIS_2156499_TIVHBO_V
xxeis.eis_rs_utility.delete_view_rows('XXEIS_2156499_TIVHBO_V',85000,FALSE);
--Inserting View Columns for XXEIS_2156499_TIVHBO_V
xxeis.eis_rs_ins.vc( 'XXEIS_2156499_TIVHBO_V','DESCRIPTION',85000,'','','','','','SA059956','VARCHAR2','','','Description','','','');
--Inserting View Components for XXEIS_2156499_TIVHBO_V
--Inserting View Component Joins for XXEIS_2156499_TIVHBO_V
END;
/
set scan on define on
prompt Creating Report LOV Data for EIS Delete Favorite Report List
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - EIS Delete Favorite Report List
xxeis.eis_rs_ins.lov( '','SELECT USER_NAME FROM FND_USER WHERE TRUNC(SYSDATE) BETWEEN START_DATE AND NVL(end_date,sysdate)','','ALL_USER_NAMES','','ANONYMOUS',NULL,'','','');
END;
/
set scan on define on
prompt Creating Report Data for EIS Delete Favorite Report List
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - EIS Delete Favorite Report List
xxeis.eis_rs_utility.delete_report_rows( 'EIS Delete Favorite Report List' );
--Inserting Report - EIS Delete Favorite Report List
xxeis.eis_rs_ins.r( 85000,'EIS Delete Favorite Report List','','This report deletes the Favorite Report List from users.','','','','SA059956','XXEIS_2156499_TIVHBO_V','Y','','SELECT XDFT.DESCRIPTION
FROM
APPS.XXWC_DEL_FAV_REP_TBL XDFT
','SA059956','','N','WC Audit Reports','','CSV,EXCEL,','');
--Inserting Report Columns - EIS Delete Favorite Report List
xxeis.eis_rs_ins.rc( 'EIS Delete Favorite Report List',85000,'DESCRIPTION','Description','','','','','','1','','Y','','','','','','','SA059956','N','N','','XXEIS_2156499_TIVHBO_V','','');
--Inserting Report Parameters - EIS Delete Favorite Report List
xxeis.eis_rs_ins.rp( 'EIS Delete Favorite Report List',85000,'Report Name','Report Name','','=','','','VARCHAR2','Y','Y','1','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'EIS Delete Favorite Report List',85000,'User Name','User Name','','IN','ALL_USER_NAMES','','VARCHAR2','N','Y','2','','N','CONSTANT','SA059956','Y','N','','','');
--Inserting Report Conditions - EIS Delete Favorite Report List
--Inserting Report Sorts - EIS Delete Favorite Report List
--Inserting Report Triggers - EIS Delete Favorite Report List
xxeis.eis_rs_ins.rt( 'EIS Delete Favorite Report List',85000,'BEGIN
XXEIS.EIS_XXWC_DEL_FAV_REP_PROC(
p_report_name => :Report Name,
p_user_name => :User Name);
END;','B','Y','SA059956');
--Inserting Report Templates - EIS Delete Favorite Report List
--Inserting Report Portals - EIS Delete Favorite Report List
--Inserting Report Dashboards - EIS Delete Favorite Report List
--Inserting Report Security - EIS Delete Favorite Report List
--Inserting Report Pivots - EIS Delete Favorite Report List
END;
/
set scan on define on
