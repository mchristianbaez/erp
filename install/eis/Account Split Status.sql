--Report Name            : Account Split Status
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View EIS_XXWC_AR_ACCOUNT_SPLIT_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_AR_ACCOUNT_SPLIT_V
Create or replace View XXEIS.EIS_XXWC_AR_ACCOUNT_SPLIT_V
(CUSTOMER_NAME,CUSTOMER_NUMBER,SITE_USE,ADDRESS_ID,ADDRESS_LINE_1,CITY,STATE,ZIP_CODE,CUST_STATUS,CUSTOMER_ID,ORIG_SYSTEM_REFERENCE,CUSTOMER_CLASS_CODE,CUSTOMER_TYPE,TAX_CODE,FOB_POINT,SHIP_VIA,PRICE_LIST_ID,FREIGHT_TERM,ORDER_TYPE_ID,SALES_CHANNEL_CODE,WAREHOUSE_ID,PARTY_ID,PARTY_NUMBER,PARTY_TYPE,CUSTOMER_KEY,CUSTOMER_CATEGORY_CODE,TAX_REFERENCE,TAXPAYER_ID,EMPLOYEES_TOTAL,POTENTIAL_REVENUE_CURR_FY,POTENTIAL_REVENUE_NEXT_FY,FISCAL_YEAREND_MONTH,YEAR_ESTABLISHED,ANALYSIS_FY,MISSION_STATEMENT,SIC_CODE_TYPE,COMPETITOR_FLAG,REFERENCE_USE_FLAG,THIRD_PARTY_FLAG,PERSON_PRE_NAME_ADJUNCT,PERSON_FIRST_NAME,PERSON_MIDDLE_NAME,PERSON_LAST_NAME,PERSON_SUFFIX,PARTY_SITE_NUMBER,SITE_CREATION_DATE,IDENTIFYING_ADDRESS_FLAG,SALESREP_NAME,SALESREP_NUMBER,COMPANY_NAME,FUNCTIONAL_CURRENCY,FUNCTIONAL_CURRENCY_PRECISION,COA_ID,LOCATION,PURPOSE,SALESREP_NUMBER2,CUST_ACCOUNT_ID,SITE_USE_ID,CUST_ACCT_SITE_ID,PARTY_SITE_ID,LOCATION_ID,CUST#PARTY_TYPE,CUST#VNDR_CODE_AND_FRULOC,CUST#BRANCH_DESCRIPTION,LOC#PAY_TO_VENDOR_ID,LOC#LOB,LOC#PAY_TO_VENDOR_CODE,PARTY#PARTY_TYPE,PARTY#GVID_ID,PARTY_SITE#REBT_PAY_TO_VNDR_,PARTY_SITE#REBT_LOB,PARTY_SITE#REBT_VNDR_CODE,PARTY_SITE#REBT_VNDR_FLAG,PARTY_SITE#REBT_VNDR_TAX_ID_,CUST#CUSTOMER_SOURCE,CUST#LEGAL_COLLECTION_INDICA,CUST#PRISM_NUMBER,CUST#YES#NOTE_LINE_#5,CUST#YES#NOTE_LINE_#1,CUST#YES#NOTE_LINE_#2,CUST#YES#NOTE_LINE_#3,CUST#YES#NOTE_LINE_#4,ACCT_SITE#PRINT_PRICES_ON_OR,ACCT_SITE#JOB_INFORMATION_ON,ACCT_SITE#TAX_EXEMPTION_TYPE,ACCT_SITE#TAX_EXEMPT1,ACCT_SITE#PRISM_NUMBER,ACCT_SITE#MANDATORY_PO_NUMBE,ACCT_SITE#LIEN_RELEASE_DATE,ACCT_SITE#YES#NOTICE_TO_OWNE,ACCT_SITE#YES#NOTICE_TO_OWNE1,ACCT_SITE#YES#NOTICE_TO_OWNE2,SITE_USES#CUSTOMER_SITE_CLAS,SITE_USES#GOVERNMENT_FUNDED,SITE_USES#THOMAS_GUIDE_PAGE,SITE_USES#DODGE_NUMBER,SITE_USES#FUTURE_USE,SITE_USES#SALESREP_#2,SITE_USES#SALESREP_SPILT_#1,SITE_USES#SALESREP_SPILT_#2,SITE_USES#PERMIT_NUMBER) AS 
SELECT SUBSTRB (party.party_name, 1, 50) customer_name,
    cust.account_number customer_number,
    SUBSTRB (look.meaning, 1, 8) site_use,
    acct_site.cust_acct_site_id address_id,
    SUBSTRB (loc.address1, 1, 30) address_line_1,
    SUBSTRB (loc.city, 1, 15) city,
    SUBSTRB (loc.state, 1, 2) state,
    SUBSTRB (loc.postal_code, 1, 10) zip_code,
    cust.status cust_status,
    cust.cust_account_id customer_id,
    cust.orig_system_reference orig_system_reference,
    cust.customer_class_code customer_class_code,
    cust.customer_type customer_type,
    cust.tax_code tax_code,
    cust.fob_point fob_point,
    cust.ship_via ship_via,
    cust.price_list_id price_list_id,
    cust.freight_term freight_term,
    cust.order_type_id order_type_id,
    cust.sales_channel_code sales_channel_code,
    cust.warehouse_id warehouse_id,
    party.party_id,
    party.party_number,
    party.party_type,
    party.customer_key customer_key,
    party.category_code customer_category_code,
    party.tax_reference tax_reference,
    party.jgzz_fiscal_code taxpayer_id,
    party.employees_total,
    DECODE (party.party_type, 'ORGANIZATION', party.curr_fy_potential_revenue, TO_NUMBER (NULL) ) potential_revenue_curr_fy,
    DECODE (party.party_type, 'ORGANIZATION', party.next_fy_potential_revenue, TO_NUMBER (NULL) ) potential_revenue_next_fy,
    DECODE (party.party_type, 'ORGANIZATION', party.fiscal_yearend_month, NULL ) fiscal_yearend_month,
    DECODE (party.party_type, 'ORGANIZATION', party.year_established, TO_NUMBER (NULL) ) year_established,
    DECODE (party.party_type, 'ORGANIZATION', party.analysis_fy, NULL ) analysis_fy,
    DECODE (party.party_type, 'ORGANIZATION', party.mission_statement, NULL ) mission_statement,
    DECODE (party.party_type, 'ORGANIZATION', party.sic_code_type, NULL ) sic_code_type,
    party.competitor_flag competitor_flag,
    party.reference_use_flag reference_use_flag,
    party.third_party_flag third_party_flag,
    party.person_pre_name_adjunct,
    party.person_first_name,
    party.person_middle_name,
    party.person_last_name,
    party.person_name_suffix person_suffix,
    party_site.party_site_number,
    party_site.creation_date site_creation_date,
    party_site.identifying_address_flag,
    jrs.salesrep_number salesrep_number,
    NVL(jrse.source_name,jrse.resource_name) salesrep_name,
    (SELECT sob.NAME company_name
    FROM gl_sets_of_books sob,
      ar_system_parameters param,
      fnd_currencies cur
    WHERE sob.set_of_books_id = param.set_of_books_id
    AND sob.currency_code     = cur.currency_code
    AND sob.set_of_books_id   = fnd_profile.VALUE ('GL_SET_OF_BKS_ID')
    ) company_name,
    (SELECT sob.currency_code
    FROM gl_sets_of_books sob,
      ar_system_parameters param,
      fnd_currencies cur
    WHERE sob.set_of_books_id = param.set_of_books_id
    AND sob.currency_code     = cur.currency_code
    AND sob.set_of_books_id   = fnd_profile.VALUE ('GL_SET_OF_BKS_ID')
    ) functional_currency,
    (SELECT cur.PRECISION
    FROM gl_sets_of_books sob,
      ar_system_parameters param,
      fnd_currencies cur
    WHERE sob.set_of_books_id = param.set_of_books_id
    AND sob.currency_code     = cur.currency_code
    AND sob.set_of_books_id   = fnd_profile.VALUE ('GL_SET_OF_BKS_ID')
    ) functional_currency_precision,
    (SELECT sob.chart_of_accounts_id coa_id
    FROM gl_sets_of_books sob,
      ar_system_parameters param,
      fnd_currencies cur
    WHERE sob.set_of_books_id = param.set_of_books_id
    AND sob.currency_code     = cur.currency_code
    AND sob.set_of_books_id   = fnd_profile.VALUE ('GL_SET_OF_BKS_ID')
    ) coa_id,
    SITE_USES.location,
    case when SITE_USE_CODE ='BILL_TO' and PRIMARY_FLAG ='Y' THEN
                 'PBillTo'
              WHEN  SITE_USE_CODE ='BILL_TO' THEN
                 'BillTo'
              Else
                 'ShipTo'
              End Purpose,
   jrs1.salesrep_number salesrep_number2,                  
    --Primary Keys
    cust.cust_account_id,
    site_uses.site_use_id,
    acct_site.cust_acct_site_id,
    party_site.party_site_id,
    loc.location_id
    --descr#flexfield#start
    ,
    xxeis.eis_rs_dff.decode_valueset( 'XXCUS_PARTY_TYPE',CUST.ATTRIBUTE1,'I') CUST#Party_Type ,
    CUST.ATTRIBUTE2 CUST#Vndr_Code_and_FRULOC ,
    CUST.ATTRIBUTE3 CUST#Branch_Description ,
    LOC.ATTRIBUTE1 LOC#Pay_To_Vendor_ID ,
    LOC.ATTRIBUTE2 LOC#LOB ,
    LOC.ATTRIBUTE3 LOC#Pay_To_Vendor_Code ,
    xxeis.eis_rs_dff.decode_valueset( 'XXCUS_PARTY_TYPE',PARTY.ATTRIBUTE1,'I') PARTY#Party_Type ,
    PARTY.ATTRIBUTE2 PARTY#GVID_ID ,
    PARTY_SITE.ATTRIBUTE2 PARTY_SITE#Rebt_Pay_to_Vndr_ ,
    PARTY_SITE.ATTRIBUTE3 PARTY_SITE#Rebt_LOB ,
    PARTY_SITE.ATTRIBUTE4 PARTY_SITE#Rebt_Vndr_Code ,
    PARTY_SITE.ATTRIBUTE5 PARTY_SITE#Rebt_Vndr_Flag ,
    PARTY_SITE.ATTRIBUTE6 PARTY_SITE#Rebt_Vndr_Tax_ID_ ,
    CUST.ATTRIBUTE4 CUST#Customer_Source ,
    xxeis.eis_rs_dff.decode_valueset( 'XXWC_LEGAL_COLLECTION',CUST.ATTRIBUTE5,'F') CUST#Legal_Collection_Indica ,
    CUST.ATTRIBUTE6 CUST#Prism_Number ,
    DECODE(CUST.ATTRIBUTE_CATEGORY ,'Yes',CUST.ATTRIBUTE16, NULL) CUST#Yes#Note_Line_#5 ,
    DECODE(CUST.ATTRIBUTE_CATEGORY ,'Yes',CUST.ATTRIBUTE17, NULL) CUST#Yes#Note_Line_#1 ,
    DECODE(CUST.ATTRIBUTE_CATEGORY ,'Yes',CUST.ATTRIBUTE18, NULL) CUST#Yes#Note_Line_#2 ,
    DECODE(CUST.ATTRIBUTE_CATEGORY ,'Yes',CUST.ATTRIBUTE19, NULL) CUST#Yes#Note_Line_#3 ,
    DECODE(CUST.ATTRIBUTE_CATEGORY ,'Yes',CUST.ATTRIBUTE20, NULL) CUST#Yes#Note_Line_#4 ,
    xxeis.eis_rs_dff.decode_valueset( 'XXWC_PRINT_PRICES',ACCT_SITE.ATTRIBUTE1,'F') ACCT_SITE#Print_Prices_on_Or ,
    xxeis.eis_rs_dff.decode_valueset( 'Yes_No',ACCT_SITE.ATTRIBUTE14,'F') ACCT_SITE#Job_Information_on ,
    xxeis.eis_rs_dff.decode_valueset( 'XXWC_TAX_EXEMPTION_TYPE',ACCT_SITE.ATTRIBUTE15,'I') ACCT_SITE#Tax_Exemption_Type ,
    xxeis.eis_rs_dff.decode_valueset( 'Yes_No',ACCT_SITE.ATTRIBUTE16,'F') ACCT_SITE#Tax_Exempt1 ,
    ACCT_SITE.ATTRIBUTE17 ACCT_SITE#PRISM_Number ,
    xxeis.eis_rs_dff.decode_valueset( 'Yes_No',ACCT_SITE.ATTRIBUTE3,'F') ACCT_SITE#Mandatory_PO_Numbe ,
    ACCT_SITE.ATTRIBUTE5 ACCT_SITE#Lien_Release_Date ,
    DECODE(ACCT_SITE.ATTRIBUTE_CATEGORY ,'Yes',ACCT_SITE.ATTRIBUTE18, NULL) ACCT_SITE#Yes#Notice_to_Owne ,
    DECODE(ACCT_SITE.ATTRIBUTE_CATEGORY ,'Yes',ACCT_SITE.ATTRIBUTE19, NULL) ACCT_SITE#Yes#Notice_to_Owne1 ,
    DECODE(ACCT_SITE.ATTRIBUTE_CATEGORY ,'Yes',ACCT_SITE.ATTRIBUTE20, NULL) ACCT_SITE#Yes#Notice_to_Owne2 ,
    xxeis.eis_rs_dff.decode_valueset( 'XXWC_CUSTOMER_SITE_CLASS',SITE_USES.ATTRIBUTE1,'F') SITE_USES#Customer_Site_Clas ,
    xxeis.eis_rs_dff.decode_valueset( 'Yes_No',SITE_USES.ATTRIBUTE2,'F') SITE_USES#Government_Funded ,
    SITE_USES.ATTRIBUTE3 SITE_USES#Thomas_Guide_Page ,
    SITE_USES.ATTRIBUTE4 SITE_USES#Dodge_Number ,
    SITE_USES.ATTRIBUTE5 SITE_USES#FUTURE_USE ,
    xxeis.eis_rs_dff.decode_valueset( 'OM: Salesreps',SITE_USES.ATTRIBUTE6,'F') SITE_USES#Salesrep_#2 ,
    SITE_USES.ATTRIBUTE7 SITE_USES#Salesrep_Spilt_#1 ,
    SITE_USES.ATTRIBUTE8 SITE_USES#Salesrep_Spilt_#2 ,
    SITE_USES.ATTRIBUTE9 SITE_USES#Permit_Number
    --descr#flexfield#end
    --gl#accountff#start
    --gl#accountff#end
  FROM ar_lookups look,
    ar_lookups look_status,
    --ar_lookups look_site_status, -- Bug fix 1033465
    hz_cust_accounts cust,
    hz_parties party,
    hz_cust_site_uses site_uses,
    hz_cust_acct_sites acct_site,
    hz_party_sites party_site,
    jtf_rs_salesreps jrs,
    jtf_rs_resource_extns_vl jrse,
    jtf_rs_salesreps jrs1,
    jtf_rs_resource_extns_vl jrse1,
    hz_locations loc
  WHERE cust.cust_account_id        = acct_site.cust_account_id(+)
  AND cust.party_id                 = party.party_id
  AND acct_site.party_site_id       = party_site.party_site_id(+)
  AND site_uses.primary_salesrep_id = jrs.salesrep_id(+)
  AND site_uses.org_id              = jrs.org_id(+)
  AND jrs.resource_id               = jrse.resource_id(+)
  AND jrs1.salesrep_id              = to_number(SITE_USES.ATTRIBUTE6)
  AND jrs1.resource_id              = jrse1.resource_id(+)
  AND loc.location_id(+)            = party_site.location_id
  AND acct_site.cust_acct_site_id   = site_uses.cust_acct_site_id(+)
  AND look.lookup_type(+)           = 'SITE_USE_CODE'
  AND look.lookup_code(+)           = site_uses.site_use_code
  AND look_status.lookup_type(+)    = 'CODE_STATUS'
  AND look_status.lookup_code(+)    = NVL (cust.status, 'A')
/
set scan on define on
prompt Creating View Data for Account Split Status
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_AR_ACCOUNT_SPLIT_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V',222,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Xxwc Ar Account Split V','EXAASV','','');
--Delete View Columns for EIS_XXWC_AR_ACCOUNT_SPLIT_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_AR_ACCOUNT_SPLIT_V',222,FALSE);
--Inserting View Columns for EIS_XXWC_AR_ACCOUNT_SPLIT_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','SITE_USES#PERMIT_NUMBER',222,'Site Uses#Permit Number','SITE_USES#PERMIT_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Site Uses#Permit Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','SITE_USES#SALESREP_SPILT_#2',222,'Site Uses#Salesrep Spilt #2','SITE_USES#SALESREP_SPILT_#2','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Site Uses#Salesrep Spilt #2','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','SITE_USES#SALESREP_SPILT_#1',222,'Site Uses#Salesrep Spilt #1','SITE_USES#SALESREP_SPILT_#1','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Site Uses#Salesrep Spilt #1','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','SITE_USES#SALESREP_#2',222,'Site Uses#Salesrep #2','SITE_USES#SALESREP_#2','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Site Uses#Salesrep #2','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','SITE_USES#FUTURE_USE',222,'Site Uses#Future Use','SITE_USES#FUTURE_USE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Site Uses#Future Use','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','SITE_USES#DODGE_NUMBER',222,'Site Uses#Dodge Number','SITE_USES#DODGE_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Site Uses#Dodge Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','SITE_USES#THOMAS_GUIDE_PAGE',222,'Site Uses#Thomas Guide Page','SITE_USES#THOMAS_GUIDE_PAGE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Site Uses#Thomas Guide Page','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','SITE_USES#GOVERNMENT_FUNDED',222,'Site Uses#Government Funded','SITE_USES#GOVERNMENT_FUNDED','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Site Uses#Government Funded','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','SITE_USES#CUSTOMER_SITE_CLAS',222,'Site Uses#Customer Site Clas','SITE_USES#CUSTOMER_SITE_CLAS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Site Uses#Customer Site Clas','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','ACCT_SITE#YES#NOTICE_TO_OWNE2',222,'Acct Site#Yes#Notice To Owne2','ACCT_SITE#YES#NOTICE_TO_OWNE2','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Acct Site#Yes#Notice To Owne2','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','ACCT_SITE#YES#NOTICE_TO_OWNE1',222,'Acct Site#Yes#Notice To Owne1','ACCT_SITE#YES#NOTICE_TO_OWNE1','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Acct Site#Yes#Notice To Owne1','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','ACCT_SITE#YES#NOTICE_TO_OWNE',222,'Acct Site#Yes#Notice To Owne','ACCT_SITE#YES#NOTICE_TO_OWNE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Acct Site#Yes#Notice To Owne','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','ACCT_SITE#LIEN_RELEASE_DATE',222,'Acct Site#Lien Release Date','ACCT_SITE#LIEN_RELEASE_DATE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Acct Site#Lien Release Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','ACCT_SITE#MANDATORY_PO_NUMBE',222,'Acct Site#Mandatory Po Numbe','ACCT_SITE#MANDATORY_PO_NUMBE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Acct Site#Mandatory Po Numbe','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','ACCT_SITE#PRISM_NUMBER',222,'Acct Site#Prism Number','ACCT_SITE#PRISM_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Acct Site#Prism Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','ACCT_SITE#TAX_EXEMPT1',222,'Acct Site#Tax Exempt1','ACCT_SITE#TAX_EXEMPT1','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Acct Site#Tax Exempt1','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','ACCT_SITE#TAX_EXEMPTION_TYPE',222,'Acct Site#Tax Exemption Type','ACCT_SITE#TAX_EXEMPTION_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Acct Site#Tax Exemption Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','ACCT_SITE#JOB_INFORMATION_ON',222,'Acct Site#Job Information On','ACCT_SITE#JOB_INFORMATION_ON','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Acct Site#Job Information On','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','ACCT_SITE#PRINT_PRICES_ON_OR',222,'Acct Site#Print Prices On Or','ACCT_SITE#PRINT_PRICES_ON_OR','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Acct Site#Print Prices On Or','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','CUST#YES#NOTE_LINE_#4',222,'Cust#Yes#Note Line #4','CUST#YES#NOTE_LINE_#4','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cust#Yes#Note Line #4','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','CUST#YES#NOTE_LINE_#3',222,'Cust#Yes#Note Line #3','CUST#YES#NOTE_LINE_#3','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cust#Yes#Note Line #3','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','CUST#YES#NOTE_LINE_#2',222,'Cust#Yes#Note Line #2','CUST#YES#NOTE_LINE_#2','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cust#Yes#Note Line #2','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','CUST#YES#NOTE_LINE_#1',222,'Cust#Yes#Note Line #1','CUST#YES#NOTE_LINE_#1','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cust#Yes#Note Line #1','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','CUST#YES#NOTE_LINE_#5',222,'Cust#Yes#Note Line #5','CUST#YES#NOTE_LINE_#5','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cust#Yes#Note Line #5','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','CUST#PRISM_NUMBER',222,'Cust#Prism Number','CUST#PRISM_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cust#Prism Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','CUST#LEGAL_COLLECTION_INDICA',222,'Cust#Legal Collection Indica','CUST#LEGAL_COLLECTION_INDICA','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cust#Legal Collection Indica','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','CUST#CUSTOMER_SOURCE',222,'Cust#Customer Source','CUST#CUSTOMER_SOURCE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cust#Customer Source','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','PARTY_SITE#REBT_VNDR_TAX_ID_',222,'Party Site#Rebt Vndr Tax Id ','PARTY_SITE#REBT_VNDR_TAX_ID_','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Party Site#Rebt Vndr Tax Id ','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','PARTY_SITE#REBT_VNDR_FLAG',222,'Party Site#Rebt Vndr Flag','PARTY_SITE#REBT_VNDR_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Party Site#Rebt Vndr Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','PARTY_SITE#REBT_VNDR_CODE',222,'Party Site#Rebt Vndr Code','PARTY_SITE#REBT_VNDR_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Party Site#Rebt Vndr Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','PARTY_SITE#REBT_LOB',222,'Party Site#Rebt Lob','PARTY_SITE#REBT_LOB','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Party Site#Rebt Lob','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','PARTY_SITE#REBT_PAY_TO_VNDR_',222,'Party Site#Rebt Pay To Vndr ','PARTY_SITE#REBT_PAY_TO_VNDR_','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Party Site#Rebt Pay To Vndr ','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','PARTY#GVID_ID',222,'Party#Gvid Id','PARTY#GVID_ID','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Party#Gvid Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','PARTY#PARTY_TYPE',222,'Party#Party Type','PARTY#PARTY_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Party#Party Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','LOC#PAY_TO_VENDOR_CODE',222,'Loc#Pay To Vendor Code','LOC#PAY_TO_VENDOR_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Loc#Pay To Vendor Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','LOC#LOB',222,'Loc#Lob','LOC#LOB','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Loc#Lob','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','LOC#PAY_TO_VENDOR_ID',222,'Loc#Pay To Vendor Id','LOC#PAY_TO_VENDOR_ID','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Loc#Pay To Vendor Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','CUST#BRANCH_DESCRIPTION',222,'Cust#Branch Description','CUST#BRANCH_DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cust#Branch Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','CUST#VNDR_CODE_AND_FRULOC',222,'Cust#Vndr Code And Fruloc','CUST#VNDR_CODE_AND_FRULOC','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cust#Vndr Code And Fruloc','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','CUST#PARTY_TYPE',222,'Cust#Party Type','CUST#PARTY_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cust#Party Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','LOCATION_ID',222,'Location Id','LOCATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Location Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','PARTY_SITE_ID',222,'Party Site Id','PARTY_SITE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Party Site Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','CUST_ACCT_SITE_ID',222,'Cust Acct Site Id','CUST_ACCT_SITE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Cust Acct Site Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','SITE_USE_ID',222,'Site Use Id','SITE_USE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Site Use Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','CUST_ACCOUNT_ID',222,'Cust Account Id','CUST_ACCOUNT_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Cust Account Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','PURPOSE',222,'Purpose','PURPOSE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Purpose','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','LOCATION',222,'Location','LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Location','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','COA_ID',222,'Coa Id','COA_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Coa Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','FUNCTIONAL_CURRENCY_PRECISION',222,'Functional Currency Precision','FUNCTIONAL_CURRENCY_PRECISION','','','','XXEIS_RS_ADMIN','NUMBER','','','Functional Currency Precision','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','FUNCTIONAL_CURRENCY',222,'Functional Currency','FUNCTIONAL_CURRENCY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Functional Currency','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','COMPANY_NAME',222,'Company Name','COMPANY_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Company Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','SALESREP_NUMBER',222,'Salesrep Number','SALESREP_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Salesrep Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','SALESREP_NAME',222,'Salesrep Name','SALESREP_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Salesrep Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','IDENTIFYING_ADDRESS_FLAG',222,'Identifying Address Flag','IDENTIFYING_ADDRESS_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Identifying Address Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','PARTY_SITE_NUMBER',222,'Party Site Number','PARTY_SITE_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Party Site Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','PERSON_SUFFIX',222,'Person Suffix','PERSON_SUFFIX','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Person Suffix','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','PERSON_LAST_NAME',222,'Person Last Name','PERSON_LAST_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Person Last Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','PERSON_MIDDLE_NAME',222,'Person Middle Name','PERSON_MIDDLE_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Person Middle Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','PERSON_FIRST_NAME',222,'Person First Name','PERSON_FIRST_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Person First Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','PERSON_PRE_NAME_ADJUNCT',222,'Person Pre Name Adjunct','PERSON_PRE_NAME_ADJUNCT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Person Pre Name Adjunct','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','THIRD_PARTY_FLAG',222,'Third Party Flag','THIRD_PARTY_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Third Party Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','REFERENCE_USE_FLAG',222,'Reference Use Flag','REFERENCE_USE_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Reference Use Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','COMPETITOR_FLAG',222,'Competitor Flag','COMPETITOR_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Competitor Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','SIC_CODE_TYPE',222,'Sic Code Type','SIC_CODE_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Sic Code Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','MISSION_STATEMENT',222,'Mission Statement','MISSION_STATEMENT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Mission Statement','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','ANALYSIS_FY',222,'Analysis Fy','ANALYSIS_FY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Analysis Fy','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','YEAR_ESTABLISHED',222,'Year Established','YEAR_ESTABLISHED','','','','XXEIS_RS_ADMIN','NUMBER','','','Year Established','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','FISCAL_YEAREND_MONTH',222,'Fiscal Yearend Month','FISCAL_YEAREND_MONTH','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Fiscal Yearend Month','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','POTENTIAL_REVENUE_NEXT_FY',222,'Potential Revenue Next Fy','POTENTIAL_REVENUE_NEXT_FY','','','','XXEIS_RS_ADMIN','NUMBER','','','Potential Revenue Next Fy','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','POTENTIAL_REVENUE_CURR_FY',222,'Potential Revenue Curr Fy','POTENTIAL_REVENUE_CURR_FY','','','','XXEIS_RS_ADMIN','NUMBER','','','Potential Revenue Curr Fy','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','EMPLOYEES_TOTAL',222,'Employees Total','EMPLOYEES_TOTAL','','','','XXEIS_RS_ADMIN','NUMBER','','','Employees Total','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','TAXPAYER_ID',222,'Taxpayer Id','TAXPAYER_ID','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Taxpayer Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','TAX_REFERENCE',222,'Tax Reference','TAX_REFERENCE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Tax Reference','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','CUSTOMER_CATEGORY_CODE',222,'Customer Category Code','CUSTOMER_CATEGORY_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Category Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','CUSTOMER_KEY',222,'Customer Key','CUSTOMER_KEY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Key','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','PARTY_TYPE',222,'Party Type','PARTY_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Party Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','PARTY_NUMBER',222,'Party Number','PARTY_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Party Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','PARTY_ID',222,'Party Id','PARTY_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Party Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','WAREHOUSE_ID',222,'Warehouse Id','WAREHOUSE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Warehouse Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','SALES_CHANNEL_CODE',222,'Sales Channel Code','SALES_CHANNEL_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Sales Channel Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','ORDER_TYPE_ID',222,'Order Type Id','ORDER_TYPE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Order Type Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','FREIGHT_TERM',222,'Freight Term','FREIGHT_TERM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Freight Term','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','PRICE_LIST_ID',222,'Price List Id','PRICE_LIST_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Price List Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','SHIP_VIA',222,'Ship Via','SHIP_VIA','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Ship Via','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','FOB_POINT',222,'Fob Point','FOB_POINT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Fob Point','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','TAX_CODE',222,'Tax Code','TAX_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Tax Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','CUSTOMER_TYPE',222,'Customer Type','CUSTOMER_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','CUSTOMER_CLASS_CODE',222,'Customer Class Code','CUSTOMER_CLASS_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Class Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','ORIG_SYSTEM_REFERENCE',222,'Orig System Reference','ORIG_SYSTEM_REFERENCE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Orig System Reference','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','CUSTOMER_ID',222,'Customer Id','CUSTOMER_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Customer Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','CUST_STATUS',222,'Cust Status','CUST_STATUS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cust Status','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','ZIP_CODE',222,'Zip Code','ZIP_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Zip Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','STATE',222,'State','STATE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','State','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','CITY',222,'City','CITY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','City','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','ADDRESS_LINE_1',222,'Address Line 1','ADDRESS_LINE_1','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Address Line 1','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','ADDRESS_ID',222,'Address Id','ADDRESS_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Address Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','SITE_USE',222,'Site Use','SITE_USE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Site Use','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','CUSTOMER_NUMBER',222,'Customer Number','CUSTOMER_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','CUSTOMER_NAME',222,'Customer Name','CUSTOMER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','SALESREP_NUMBER2',222,'Salesrep Number2','SALESREP_NUMBER2','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Salesrep Number2','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ACCOUNT_SPLIT_V','SITE_CREATION_DATE',222,'Site Creation Date','SITE_CREATION_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Site Creation Date','','','');
--Inserting View Components for EIS_XXWC_AR_ACCOUNT_SPLIT_V
--Inserting View Component Joins for EIS_XXWC_AR_ACCOUNT_SPLIT_V
END;
/
set scan on define on
prompt Creating Report Data for Account Split Status
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Account Split Status
xxeis.eis_rs_utility.delete_report_rows( 'Account Split Status' );
--Inserting Report - Account Split Status
xxeis.eis_rs_ins.r( 222,'Account Split Status','','Account Split Status','','','','XXEIS_RS_ADMIN','EIS_XXWC_AR_ACCOUNT_SPLIT_V','Y','','','XXEIS_RS_ADMIN','','N','White Cap Reports','RTF,PDF,','CSV,HTML,Html Summary,XML,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - Account Split Status
xxeis.eis_rs_ins.rc( 'Account Split Status',222,'CUSTOMER_NAME','Customer Name','Customer Name','','','default','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_ACCOUNT_SPLIT_V','','');
xxeis.eis_rs_ins.rc( 'Account Split Status',222,'CUSTOMER_NUMBER','Customer Number','Customer Number','','','default','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_ACCOUNT_SPLIT_V','','');
xxeis.eis_rs_ins.rc( 'Account Split Status',222,'LOCATION','Location','Location','','','default','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_ACCOUNT_SPLIT_V','','');
xxeis.eis_rs_ins.rc( 'Account Split Status',222,'PARTY_SITE_NUMBER','Party Site Number','Party Site Number','','','default','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_ACCOUNT_SPLIT_V','','');
xxeis.eis_rs_ins.rc( 'Account Split Status',222,'SALESREP_NAME','Salesrep Number','Salesrep Name','','','default','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_ACCOUNT_SPLIT_V','','');
xxeis.eis_rs_ins.rc( 'Account Split Status',222,'SALESREP_NUMBER','Salesrep Name','Salesrep Number','','','default','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_ACCOUNT_SPLIT_V','','');
xxeis.eis_rs_ins.rc( 'Account Split Status',222,'SITE_USES#SALESREP_#2','Salesrep Name2','Site Uses#Salesrep #2','','','default','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_ACCOUNT_SPLIT_V','','');
xxeis.eis_rs_ins.rc( 'Account Split Status',222,'SITE_USES#SALESREP_SPILT_#1','Spilt #1','Site Uses#Salesrep Spilt #1','','','default','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_ACCOUNT_SPLIT_V','','');
xxeis.eis_rs_ins.rc( 'Account Split Status',222,'SITE_USES#SALESREP_SPILT_#2','Spilt #2','Site Uses#Salesrep Spilt #2','','','default','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_ACCOUNT_SPLIT_V','','');
xxeis.eis_rs_ins.rc( 'Account Split Status',222,'PURPOSE','Purpose','Purpose','','','default','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_ACCOUNT_SPLIT_V','','');
xxeis.eis_rs_ins.rc( 'Account Split Status',222,'SALESREP_NUMBER2','Salesrep Number2','Salesrep Number2','','','default','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_ACCOUNT_SPLIT_V','','');
xxeis.eis_rs_ins.rc( 'Account Split Status',222,'SITE_CREATION_DATE','Site Creation Date','Site Creation Date','','','default','','12','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_ACCOUNT_SPLIT_V','','');
--Inserting Report Parameters - Account Split Status
--Inserting Report Conditions - Account Split Status
xxeis.eis_rs_ins.rcn( 'Account Split Status',222,'','','','','AND (SITE_USES#SALESREP_SPILT_#1 is not null or SITE_USES#SALESREP_SPILT_#2 is not null)','Y','1','','XXEIS_RS_ADMIN');
--Inserting Report Sorts - Account Split Status
--Inserting Report Triggers - Account Split Status
--Inserting Report Templates - Account Split Status
--Inserting Report Portals - Account Split Status
--Inserting Report Dashboards - Account Split Status
--Inserting Report Security - Account Split Status
xxeis.eis_rs_ins.rsec( 'Account Split Status','222','','50894',222,'XXEIS_RS_ADMIN','','');
--Inserting Report Pivots - Account Split Status
END;
/
set scan on define on

