---------------------------------------------------------------------------------------------------------------
/**************************************************************************************************************
  PURPOSE	  : Issue with TAXWARE - Sales Tax Exempton Certificate Report 
  REVISIONS   :
  VERSION 		DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     	   1-Dec-2016        	Siva   		 TMS#20161213-00033  
**************************************************************************************************************/
DELETE
FROM xxeis.eis_rs_view_columns
WHERE VIEW_ID IN
 (SELECT view_id
 FROM XXEIS.EIS_RS_VIEWS
 WHERE view_name LIKE 'XXEIS_36460_WZVDQT_V');
Commit;  
/