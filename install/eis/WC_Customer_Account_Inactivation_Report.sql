--Report Name            : WC-Customer Account Inactivation Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View EIS_XXWC_AR_INACT_ACCT_DTLS_C
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_AR_INACT_ACCT_DTLS_C
Create or replace View XXEIS.EIS_XXWC_AR_INACT_ACCT_DTLS_C
 AS 
SELECT ACCOUNT_NUMBER,
   ACCOUNT_NAME	,
   COLLECTOR,
   PROFILE_CLASS,
   LAST_SALE_DATE,
   LAST_PAYMENT_DATE,
   ORDER_DATE,
   AMOUNT_DUE_REMAINING
FROM XXEIS.EIS_XXWC_AR_INACTIVE_ACC_STG/
set scan on define on
prompt Creating View Data for WC-Customer Account Inactivation Report
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_AR_INACT_ACCT_DTLS_C
xxeis.eis_rs_ins.v( 'EIS_XXWC_AR_INACT_ACCT_DTLS_C',222,'','','','','DM027741','XXEIS','Eis Xxwc Ar Inact Acct Dtls C','EXAIADC','','');
--Delete View Columns for EIS_XXWC_AR_INACT_ACCT_DTLS_C
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_AR_INACT_ACCT_DTLS_C',222,FALSE);
--Inserting View Columns for EIS_XXWC_AR_INACT_ACCT_DTLS_C
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INACT_ACCT_DTLS_C','AMOUNT_DUE_REMAINING',222,'Amount Due Remaining','AMOUNT_DUE_REMAINING','','','','DM027741','NUMBER','','','Amount Due Remaining','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INACT_ACCT_DTLS_C','ORDER_DATE',222,'Order Date','ORDER_DATE','','','','DM027741','DATE','','','Order Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INACT_ACCT_DTLS_C','LAST_PAYMENT_DATE',222,'Last Payment Date','LAST_PAYMENT_DATE','','','','DM027741','DATE','','','Last Payment Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INACT_ACCT_DTLS_C','LAST_SALE_DATE',222,'Last Sale Date','LAST_SALE_DATE','','','','DM027741','DATE','','','Last Sale Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INACT_ACCT_DTLS_C','PROFILE_CLASS',222,'Profile Class','PROFILE_CLASS','','','','DM027741','VARCHAR2','','','Profile Class','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INACT_ACCT_DTLS_C','COLLECTOR',222,'Collector','COLLECTOR','','','','DM027741','VARCHAR2','','','Collector','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INACT_ACCT_DTLS_C','ACCOUNT_NAME',222,'Account Name','ACCOUNT_NAME','','','','DM027741','VARCHAR2','','','Account Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_INACT_ACCT_DTLS_C','ACCOUNT_NUMBER',222,'Account Number','ACCOUNT_NUMBER','','','','DM027741','VARCHAR2','','','Account Number','','','');
--Inserting View Components for EIS_XXWC_AR_INACT_ACCT_DTLS_C
--Inserting View Component Joins for EIS_XXWC_AR_INACT_ACCT_DTLS_C
END;
/
set scan on define on
prompt Creating Report Data for WC-Customer Account Inactivation Report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - WC-Customer Account Inactivation Report
xxeis.eis_rs_utility.delete_report_rows( 'WC-Customer Account Inactivation Report' );
--Inserting Report - WC-Customer Account Inactivation Report
xxeis.eis_rs_ins.r( 222,'WC-Customer Account Inactivation Report','','','','','','DM027741','EIS_XXWC_AR_INACT_ACCT_DTLS_C','Y','','','DM027741','','N','White Cap Reports','','CSV,EXCEL,','N');
--Inserting Report Columns - WC-Customer Account Inactivation Report
xxeis.eis_rs_ins.rc( 'WC-Customer Account Inactivation Report',222,'ACCOUNT_NAME','Account Name','Account Name','','','default','','1','N','','','','','','','','DM027741','N','N','','EIS_XXWC_AR_INACT_ACCT_DTLS_C','','');
xxeis.eis_rs_ins.rc( 'WC-Customer Account Inactivation Report',222,'ACCOUNT_NUMBER','Account Number','Account Number','','','default','','2','N','','','','','','','','DM027741','N','N','','EIS_XXWC_AR_INACT_ACCT_DTLS_C','','');
xxeis.eis_rs_ins.rc( 'WC-Customer Account Inactivation Report',222,'AMOUNT_DUE_REMAINING','Amount Due Remaining','Amount Due Remaining','','~~~','default','','8','N','','','','','','','','DM027741','N','N','','EIS_XXWC_AR_INACT_ACCT_DTLS_C','','');
xxeis.eis_rs_ins.rc( 'WC-Customer Account Inactivation Report',222,'COLLECTOR','Collector','Collector','','','default','','3','N','','','','','','','','DM027741','N','N','','EIS_XXWC_AR_INACT_ACCT_DTLS_C','','');
xxeis.eis_rs_ins.rc( 'WC-Customer Account Inactivation Report',222,'LAST_PAYMENT_DATE','Last Payment Date','Last Payment Date','','','default','','5','N','','','','','','','','DM027741','N','N','','EIS_XXWC_AR_INACT_ACCT_DTLS_C','','');
xxeis.eis_rs_ins.rc( 'WC-Customer Account Inactivation Report',222,'LAST_SALE_DATE','Last Sale Date','Last Sale Date','','','default','','6','N','','','','','','','','DM027741','N','N','','EIS_XXWC_AR_INACT_ACCT_DTLS_C','','');
xxeis.eis_rs_ins.rc( 'WC-Customer Account Inactivation Report',222,'ORDER_DATE','Order Date','Order Date','','','default','','7','N','','','','','','','','DM027741','N','N','','EIS_XXWC_AR_INACT_ACCT_DTLS_C','','');
xxeis.eis_rs_ins.rc( 'WC-Customer Account Inactivation Report',222,'PROFILE_CLASS','Profile Class','Profile Class','','','default','','4','N','','','','','','','','DM027741','N','N','','EIS_XXWC_AR_INACT_ACCT_DTLS_C','','');
--Inserting Report Parameters - WC-Customer Account Inactivation Report
xxeis.eis_rs_ins.rp( 'WC-Customer Account Inactivation Report',222,'Month','Month','','IN','','24','NUMERIC','Y','Y','1','','N','CONSTANT','DM027741','Y','N','','','');
xxeis.eis_rs_ins.rp( 'WC-Customer Account Inactivation Report',222,'AsofDate','As of Date','','IN','','','DATE','Y','Y','2','','N','CURRENT_DATE','DM027741','Y','N','','','');
--Inserting Report Conditions - WC-Customer Account Inactivation Report
--Inserting Report Sorts - WC-Customer Account Inactivation Report
--Inserting Report Triggers - WC-Customer Account Inactivation Report
xxeis.eis_rs_ins.rt( 'WC-Customer Account Inactivation Report',222,'Begin
XXEIS.EIS_RS_XXWC_AR_UTIL_PKG.inactive_acc_pre_trig (:Month, :AsofDate);
End;','B','Y','DM027741');
--Inserting Report Templates - WC-Customer Account Inactivation Report
--Inserting Report Portals - WC-Customer Account Inactivation Report
--Inserting Report Dashboards - WC-Customer Account Inactivation Report
--Inserting Report Security - WC-Customer Account Inactivation Report
xxeis.eis_rs_ins.rsec( 'WC-Customer Account Inactivation Report','222','','51929',222,'DM027741','','');
xxeis.eis_rs_ins.rsec( 'WC-Customer Account Inactivation Report','222','','51289',222,'DM027741','','');
xxeis.eis_rs_ins.rsec( 'WC-Customer Account Inactivation Report','222','','50993',222,'DM027741','','');
xxeis.eis_rs_ins.rsec( 'WC-Customer Account Inactivation Report','222','','50854',222,'DM027741','','');
xxeis.eis_rs_ins.rsec( 'WC-Customer Account Inactivation Report','222','','50853',222,'DM027741','','');
xxeis.eis_rs_ins.rsec( 'WC-Customer Account Inactivation Report','222','','50879',222,'DM027741','','');
xxeis.eis_rs_ins.rsec( 'WC-Customer Account Inactivation Report','222','','50877',222,'DM027741','','');
xxeis.eis_rs_ins.rsec( 'WC-Customer Account Inactivation Report','222','','51939',222,'DM027741','','');
xxeis.eis_rs_ins.rsec( 'WC-Customer Account Inactivation Report','222','','50878',222,'DM027741','','');
xxeis.eis_rs_ins.rsec( 'WC-Customer Account Inactivation Report','222','','50880',222,'DM027741','','');
xxeis.eis_rs_ins.rsec( 'WC-Customer Account Inactivation Report','222','','50992',222,'DM027741','','');
xxeis.eis_rs_ins.rsec( 'WC-Customer Account Inactivation Report','20005','','50897',222,'DM027741','','');
xxeis.eis_rs_ins.rsec( 'WC-Customer Account Inactivation Report','222','','50894',222,'DM027741','','');
xxeis.eis_rs_ins.rsec( 'WC-Customer Account Inactivation Report','222','','51208',222,'DM027741','','');
--Inserting Report Pivots - WC-Customer Account Inactivation Report
END;
/
set scan on define on
