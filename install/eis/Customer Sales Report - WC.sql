--Report Name            : Customer Sales Report - WC
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data EIS_XXWC_CUSTOMER_SALES_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(222);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_CUSTOMER_SALES_V
xxeis.eis_rsc_ins.v( 'EIS_XXWC_CUSTOMER_SALES_V',222,'','','','','SA059956','XXEIS','Eis Xxwc Customer Sales V','EXCSV','','','VIEW','US','','','');
--Delete Object Columns for EIS_XXWC_CUSTOMER_SALES_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_CUSTOMER_SALES_V',222,FALSE);
--Inserting Object Columns for EIS_XXWC_CUSTOMER_SALES_V
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CUSTOMER_SALES_V','CUSTOMER_NAME',222,'Customer Name','CUSTOMER_NAME','','','','SA059956','VARCHAR2','','','Customer Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CUSTOMER_SALES_V','CUSTOMER_NUMBER',222,'Customer Number','CUSTOMER_NUMBER','','','','SA059956','VARCHAR2','','','Customer Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CUSTOMER_SALES_V','CUSTOMER_CLASSIFICATION',222,'Customer Classification','CUSTOMER_CLASSIFICATION','','','','SA059956','VARCHAR2','','','Customer Classification','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CUSTOMER_SALES_V','SALE_AMOUNT',222,'Sale Amount','SALE_AMOUNT','','','','SA059956','NUMBER','','','Sale Amount','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CUSTOMER_SALES_V','CREATION_DATE',222,'Creation Date','CREATION_DATE','','','','SA059956','DATE','','','Creation Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CUSTOMER_SALES_V','PERIOD_NAME',222,'Period Name','PERIOD_NAME','','','','SA059956','VARCHAR2','','','Period Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CUSTOMER_SALES_V','PERIOD_YEAR',222,'Period Year','PERIOD_YEAR','','','','SA059956','NUMBER','','','Period Year','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CUSTOMER_SALES_V','PROMOTION_LINES',222,'Promotion Lines','PROMOTION_LINES','','','','SA059956','VARCHAR2','','','Promotion Lines','','','','','');
--Inserting Object Components for EIS_XXWC_CUSTOMER_SALES_V
--Inserting Object Component Joins for EIS_XXWC_CUSTOMER_SALES_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 222');
END IF;
END;
/
prompt Creating Report LOV Data for Customer Sales Report - WC
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(222);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - Customer Sales Report - WC
xxeis.eis_rsc_ins.lov( 222,'select period_name
from gl.GL_PERIODS
where period_set_name  = ''4-4-QTR''','','HDS_Fiscal_Month','','ID020048',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 222,'SELECT NVL(hca.account_name, hp.party_name) customer_name,hca.account_number customer_number
FROM hz_cust_accounts hca,
  hz_parties hp
where hca.party_id=hp.party_id
ORDER BY 1','','XXWC Customer Name LOV','Display List of Values for Customer Name','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 222,'SELECT hca.account_number customer_number,NVL(hca.account_name, hp.party_name) customer_name
FROM hz_cust_accounts hca,
  hz_parties hp
where hca.party_id=hp.party_id
ORDER BY lpad(hca.account_number,10)','','XXWC Customer Number LOV','Displays List of Values for Customer Number','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( '','','''Add'',''Remove''','XXWC Add Remove LOV','','SA059956',NULL,'Y','','','N','N','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 222');
END IF;
END;
/
prompt Creating Report Data for Customer Sales Report - WC
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(222);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Customer Sales Report - WC
xxeis.eis_rsc_utility.delete_report_rows( 'Customer Sales Report - WC',222 );
--Inserting Report - Customer Sales Report - WC
xxeis.eis_rsc_ins.r( 222,'Customer Sales Report - WC','','Customer Sales Report - WC','','','','SA059956','EIS_XXWC_CUSTOMER_SALES_V','Y','','','SA059956','','N','White Cap Reports','','CSV,EXCEL,','N','','','','','','N','','US','','','','','','','','','','','','','','','','');
--Inserting Report Columns - Customer Sales Report - WC
xxeis.eis_rsc_ins.rc( 'Customer Sales Report - WC',222,'CREATION_DATE','Invoice Creation Date','Creation Date','','','default','','1','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_CUSTOMER_SALES_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Customer Sales Report - WC',222,'CUSTOMER_CLASSIFICATION','Customer Classification','Customer Classification','','','default','','2','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_CUSTOMER_SALES_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Customer Sales Report - WC',222,'CUSTOMER_NAME','Customer Name','Customer Name','','','default','','4','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_CUSTOMER_SALES_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Customer Sales Report - WC',222,'CUSTOMER_NUMBER','Customer Number','Customer Number','','','default','','3','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_CUSTOMER_SALES_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Customer Sales Report - WC',222,'SALE_AMOUNT','Sale Amount','Sale Amount','','~T~D~2','default','','5','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_CUSTOMER_SALES_V','','','','US','','');
--Inserting Report Parameters - Customer Sales Report - WC
xxeis.eis_rsc_ins.rp( 'Customer Sales Report - WC',222,'Customer Name','Customer Name','CUSTOMER_NAME','IN','XXWC Customer Name LOV','','VARCHAR2','N','Y','1','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_CUSTOMER_SALES_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Customer Sales Report - WC',222,'Customer Number','Customer Number','CUSTOMER_NUMBER','IN','XXWC Customer Number LOV','','VARCHAR2','N','Y','2','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_CUSTOMER_SALES_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Customer Sales Report - WC',222,'Period Name','Period Name','PERIOD_NAME','IN','HDS_Fiscal_Month','','VARCHAR2','Y','Y','3','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_CUSTOMER_SALES_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Customer Sales Report - WC',222,'Promotion Lines','Promotion Lines','PROMOTION_LINES','IN','XXWC Add Remove LOV','''Add''','VARCHAR2','Y','Y','4','N','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_CUSTOMER_SALES_V','','','US','');
--Inserting Dependent Parameters - Customer Sales Report - WC
xxeis.eis_rsc_ins.rdp( 'Customer Sales Report - WC',222,'CUSTOMER_NUMBER','Customer Name','Customer Number','IN','N','');
xxeis.eis_rsc_ins.rdp( 'Customer Sales Report - WC',222,'CUSTOMER_NAME','Customer Number','Customer Name','IN','N','');
--Inserting Report Conditions - Customer Sales Report - WC
xxeis.eis_rsc_ins.rcnh( 'Customer Sales Report - WC',222,'EXCSV.CUSTOMER_NAME IN Customer Name','SIMPLE','','','Y','','1');
xxeis.eis_rsc_ins.rcnd( '','','CUSTOMER_NAME','','Customer Name','','','','','EIS_XXWC_CUSTOMER_SALES_V','','','','','','IN','Y','Y','','','','','1',222,'Customer Sales Report - WC','EXCSV.CUSTOMER_NAME IN Customer Name');
xxeis.eis_rsc_ins.rcnh( 'Customer Sales Report - WC',222,'EXCSV.CUSTOMER_NUMBER IN Customer Number','SIMPLE','','','Y','','2');
xxeis.eis_rsc_ins.rcnd( '','','CUSTOMER_NUMBER','','Customer Number','','','','','EIS_XXWC_CUSTOMER_SALES_V','','','','','','IN','Y','Y','','','','','1',222,'Customer Sales Report - WC','EXCSV.CUSTOMER_NUMBER IN Customer Number');
xxeis.eis_rsc_ins.rcnh( 'Customer Sales Report - WC',222,'EXCSV.PERIOD_NAME IN Period Name','SIMPLE','','','Y','','3');
xxeis.eis_rsc_ins.rcnd( '','','PERIOD_NAME','','Period Name','','','','','EIS_XXWC_CUSTOMER_SALES_V','','','','','','IN','Y','Y','','','','','1',222,'Customer Sales Report - WC','EXCSV.PERIOD_NAME IN Period Name');
xxeis.eis_rsc_ins.rcnh( 'Customer Sales Report - WC',222,'EXCSV.PROMOTION_LINES IN Promotion Lines','SIMPLE','','','Y','','4');
xxeis.eis_rsc_ins.rcnd( '','','PROMOTION_LINES','','Promotion Lines','','','','','EIS_XXWC_CUSTOMER_SALES_V','','','','','','IN','Y','Y','','','','','1',222,'Customer Sales Report - WC','EXCSV.PROMOTION_LINES IN Promotion Lines');
--Inserting Report Sorts - Customer Sales Report - WC
xxeis.eis_rsc_ins.rs( 'Customer Sales Report - WC',222,'CREATION_DATE','ASC','SA059956','1','');
--Inserting Report Triggers - Customer Sales Report - WC
--inserting report templates - Customer Sales Report - WC
--Inserting Report Portals - Customer Sales Report - WC
--inserting report dashboards - Customer Sales Report - WC
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Customer Sales Report - WC','222','EIS_XXWC_CUSTOMER_SALES_V','EIS_XXWC_CUSTOMER_SALES_V','N','');
--inserting report security - Customer Sales Report - WC
xxeis.eis_rsc_ins.rsec( 'Customer Sales Report - WC','','PP018915','',222,'SA059956','','N','');
xxeis.eis_rsc_ins.rsec( 'Customer Sales Report - WC','','LB048272','',222,'SA059956','','N','');
--Inserting Report Pivots - Customer Sales Report - WC
--Inserting Report Distribution Details 
--Inserting Report Distribution Details 
--Inserting Report SubTotals Details 
--Inserting Report aggregations 
--Inserting Report   Version details- Customer Sales Report - WC
xxeis.eis_rsc_ins.rv( 'Customer Sales Report - WC','','Customer Sales Report - WC','SA059956','19-JUN-2018');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 222');
END IF;
END;
/
