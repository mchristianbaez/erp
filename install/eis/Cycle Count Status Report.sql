--Report Name            : Cycle Count Status Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data EIS_XXWC_CYCLE_STATUS_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(401);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_CYCLE_STATUS_V
xxeis.eis_rsc_ins.v( 'EIS_XXWC_CYCLE_STATUS_V',401,'','','','','ANONYMOUS','XXEIS','Eis Xxwc Cycle Status V','EXCSV','','','VIEW','US','','');
--Delete Object Columns for EIS_XXWC_CYCLE_STATUS_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_CYCLE_STATUS_V',401,FALSE);
--Inserting Object Columns for EIS_XXWC_CYCLE_STATUS_V
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CYCLE_STATUS_V','BEGINING_COUNT_DATE',401,'Begining Count Date','BEGINING_COUNT_DATE','','','','ANONYMOUS','DATE','','','Begining Count Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CYCLE_STATUS_V','ORG_CODE',401,'Org Code','ORG_CODE','','','','ANONYMOUS','VARCHAR2','','','Org Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CYCLE_STATUS_V','REGION',401,'Region','REGION','','','','ANONYMOUS','VARCHAR2','','','Region','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CYCLE_STATUS_V','DISTRICT',401,'District','DISTRICT','','','','ANONYMOUS','VARCHAR2','','','District','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CYCLE_STATUS_V','COUNT_ITEMS',401,'Count Items','COUNT_ITEMS','','','','ANONYMOUS','NUMBER','','','Count Items','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CYCLE_STATUS_V','COUNT_NOT_APPROVED',401,'Count Not Approved','COUNT_NOT_APPROVED','','','','ANONYMOUS','NUMBER','','','Count Not Approved','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CYCLE_STATUS_V','CREATION_DATE',401,'Creation Date','CREATION_DATE','','','','ANONYMOUS','DATE','','','Creation Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CYCLE_STATUS_V','ORG',401,'Org','ORG','','','','ANONYMOUS','VARCHAR2','','','Org','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CYCLE_STATUS_V','CYCLE_COUNT_ENABLED_FLAG',401,'Cycle Count Enabled Flag','CYCLE_COUNT_ENABLED_FLAG','','','','ANONYMOUS','VARCHAR2','','','Cycle Count Enabled Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CYCLE_STATUS_V','MTL_TRANSACTIONS_ENABLED_FLAG',401,'Mtl Transactions Enabled Flag','MTL_TRANSACTIONS_ENABLED_FLAG','','','','ANONYMOUS','VARCHAR2','','','Mtl Transactions Enabled Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CYCLE_STATUS_V','STOCK_ENABLED_FLAG',401,'Stock Enabled Flag','STOCK_ENABLED_FLAG','','','','ANONYMOUS','VARCHAR2','','','Stock Enabled Flag','','','','US');
--Inserting Object Components for EIS_XXWC_CYCLE_STATUS_V
--Inserting Object Component Joins for EIS_XXWC_CYCLE_STATUS_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 401');
END IF;
END;
/
prompt Creating Report LOV Data for Cycle Count Status Report
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(401);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - Cycle Count Status Report
xxeis.eis_rsc_ins.lov( 401,'select list_name from XXWC_PARAM_LIST where list_type = ''Org'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Org List','','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 401,'select distinct ATTRIBUTE9 Region from mtl_parameters','','Region Lov','','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 401,'SELECT organization_code code,organization_name name
FROM ORG_ORGANIZATION_DEFINITIONS OOD
WHERE EXISTS
  (SELECT 1
  FROM XXEIS.EIS_ORG_ACCESS_V
  WHERE organization_id = ood.organization_id
  )
ORDER BY organization_code','','XXWC INV ORGANIZATIONS LOV','List of All Inventory Orgs under a given operating unit.','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 401,'select distinct ATTRIBUTE8 District from mtl_parameters','','District Lov','District Lov','ANONYMOUS',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 401');
END IF;
END;
/
prompt Creating Report Data for Cycle Count Status Report
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(401);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Cycle Count Status Report
xxeis.eis_rsc_utility.delete_report_rows( 'Cycle Count Status Report' );
--Inserting Report - Cycle Count Status Report
xxeis.eis_rsc_ins.r( 401,'Cycle Count Status Report','','Details cycle count status of counts created but not approved.','','','','DM027741','EIS_XXWC_CYCLE_STATUS_V','Y','','','DM027741','','N','White Cap Reports','','Pivot Excel,EXCEL,','N','','','','','','','','US','','','','');
--Inserting Report Columns - Cycle Count Status Report
xxeis.eis_rsc_ins.rc( 'Cycle Count Status Report',401,'BEGINING_COUNT_DATE','Count Date','Begining Count Date','','','default','','5','N','','','','','','','','DM027741','N','N','','EIS_XXWC_CYCLE_STATUS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Cycle Count Status Report',401,'DISTRICT','District','District','','','default','','2','N','','','','','','','','DM027741','N','N','','EIS_XXWC_CYCLE_STATUS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Cycle Count Status Report',401,'REGION','Region','Region','','','default','','1','N','','','','','','','','DM027741','N','N','','EIS_XXWC_CYCLE_STATUS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Cycle Count Status Report',401,'COUNT_ITEMS','Count of Items','Count Items','','~T~D~0','default','','6','N','','','','','','','','DM027741','N','N','','EIS_XXWC_CYCLE_STATUS_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Cycle Count Status Report',401,'COUNT_NOT_APPROVED','Count of items not approved','Count Not Approved','','~T~D~0','default','','7','N','','','','','','','','DM027741','N','N','','EIS_XXWC_CYCLE_STATUS_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Cycle Count Status Report',401,'ORG_CODE','Organization','Org Code','','','default','','3','N','','','','','','','','DM027741','N','N','','EIS_XXWC_CYCLE_STATUS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Cycle Count Status Report',401,'CREATION_DATE','Count Created','Creation Date','','','default','','4','N','','','','','','','','DM027741','N','N','','EIS_XXWC_CYCLE_STATUS_V','','','GROUP_BY','US','');
--Inserting Report Parameters - Cycle Count Status Report
xxeis.eis_rsc_ins.rp( 'Cycle Count Status Report',401,'Region','Region','REGION','IN','Region Lov','','VARCHAR2','N','Y','1','Y','Y','CONSTANT','DM027741','Y','N','','','','EIS_XXWC_CYCLE_STATUS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Cycle Count Status Report',401,'District','District','DISTRICT','IN','District Lov','','VARCHAR2','N','Y','2','Y','Y','CONSTANT','DM027741','Y','N','','','','EIS_XXWC_CYCLE_STATUS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Cycle Count Status Report',401,'Organization','Org','ORG_CODE','IN','XXWC INV ORGANIZATIONS LOV','','VARCHAR2','Y','Y','3','Y','Y','CONSTANT','DM027741','Y','N','','','','EIS_XXWC_CYCLE_STATUS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Cycle Count Status Report',401,'Org List','Org List','','IN','XXWC Org List','','VARCHAR2','N','Y','4','N','N','CONSTANT','DM027741','Y','N','','','','EIS_XXWC_CYCLE_STATUS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Cycle Count Status Report',401,'Count Date Beginning','Count Date Beginning','CREATION_DATE','>=','','','DATE','N','Y','5','Y','Y','CONSTANT','DM027741','Y','N','','','','EIS_XXWC_CYCLE_STATUS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Cycle Count Status Report',401,'Count date ending','Count date ending','CREATION_DATE','<=','','','DATE','N','Y','6','Y','Y','CONSTANT','DM027741','Y','N','','','','EIS_XXWC_CYCLE_STATUS_V','','','US','');
--Inserting Dependent Parameters - Cycle Count Status Report
--Inserting Report Conditions - Cycle Count Status Report
xxeis.eis_rsc_ins.rcnh( 'Cycle Count Status Report',401,'CREATION_DATE >= :Count Date Beginning ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CREATION_DATE','','Count Date Beginning','','','','','EIS_XXWC_CYCLE_STATUS_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',401,'Cycle Count Status Report','CREATION_DATE >= :Count Date Beginning ');
xxeis.eis_rsc_ins.rcnh( 'Cycle Count Status Report',401,'CREATION_DATE <= :Count date ending ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CREATION_DATE','','Count date ending','','','','','EIS_XXWC_CYCLE_STATUS_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',401,'Cycle Count Status Report','CREATION_DATE <= :Count date ending ');
xxeis.eis_rsc_ins.rcnh( 'Cycle Count Status Report',401,'DISTRICT IN :District ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','DISTRICT','','District','','','','','EIS_XXWC_CYCLE_STATUS_V','','','','','','IN','Y','Y','','','','','1',401,'Cycle Count Status Report','DISTRICT IN :District ');
xxeis.eis_rsc_ins.rcnh( 'Cycle Count Status Report',401,'EXCSV.ORG_CODE IN :Organization ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','ORG_CODE','','Organization','','','','','EIS_XXWC_CYCLE_STATUS_V','','','','','','IN','Y','Y','','','','','1',401,'Cycle Count Status Report','EXCSV.ORG_CODE IN :Organization ');
xxeis.eis_rsc_ins.rcnh( 'Cycle Count Status Report',401,'REGION IN :Region ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','REGION','','Region','','','','','EIS_XXWC_CYCLE_STATUS_V','','','','','','IN','Y','Y','','','','','1',401,'Cycle Count Status Report','REGION IN :Region ');
xxeis.eis_rsc_ins.rcnh( 'Cycle Count Status Report',401,'Free Text ','FREE_TEXT','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','','Y','','','','','AND ( :Org List is NULL  OR
          EXISTS  (SELECT 1
  from xxeis.eis_xxwc_param_parse_list x
  where x.list_name     =:Org List
  and x.list_type     =''Org''
 and x.process_id    = :system.process_id
and  EXCSV.ORG_CODE =regexp_replace(x.list_value, ''[^[a-z,A-Z,0-9]]*''  )))','1',401,'Cycle Count Status Report','Free Text ');
--Inserting Report Sorts - Cycle Count Status Report
xxeis.eis_rsc_ins.rs( 'Cycle Count Status Report',401,'REGION','ASC','DM027741','1','');
xxeis.eis_rsc_ins.rs( 'Cycle Count Status Report',401,'DISTRICT','ASC','DM027741','2','');
--Inserting Report Triggers - Cycle Count Status Report
xxeis.eis_rsc_ins.rt( 'Cycle Count Status Report',401,'Declare
L_TEMP         varchar2(240);
begin
L_TEMP         :=:Org List ;
if l_temp is not null then
XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.PARSE_PARAM_LIST(P_PROCESS_ID => :SYSTEM.PROCESS_ID,
                        P_LIST_NAME => L_TEMP ,
                        P_LIST_TYPE => ''Org''
                        );
xxeis.EIS_RS_XXWC_COM_UTIL_PKG.g_flag_value:=''Y'';
else
xxeis.EIS_RS_XXWC_COM_UTIL_PKG.g_flag_value:=''N'';
end if;
end;
','B','Y','DM027741','AQ');
xxeis.eis_rsc_ins.rt( 'Cycle Count Status Report',401,'BEGIN
xxeis.EIS_RS_XXWC_COM_UTIL_PKG.parse_cleanup_table(P_PROCESS_ID    => :SYSTEM.PROCESS_ID);
END;','A','Y','DM027741','AQ');
--inserting report templates - Cycle Count Status Report
--Inserting Report Portals - Cycle Count Status Report
--inserting report dashboards - Cycle Count Status Report
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Cycle Count Status Report','401','EIS_XXWC_CYCLE_STATUS_V','EIS_XXWC_CYCLE_STATUS_V','N','');
--inserting report security - Cycle Count Status Report
xxeis.eis_rsc_ins.rsec( 'Cycle Count Status Report','401','','XXWC_INVENTORY_SUPER_USER',401,'DM027741','','','');
xxeis.eis_rsc_ins.rsec( 'Cycle Count Status Report','401','','XXWC_INVENTORY_SPEC_SCC',401,'DM027741','','','');
xxeis.eis_rsc_ins.rsec( 'Cycle Count Status Report','401','','XXWC_INV_PLANNER',401,'DM027741','','','');
xxeis.eis_rsc_ins.rsec( 'Cycle Count Status Report','401','','XXWC_INVENTORY_CONTROL_INQUIRY',401,'DM027741','','','');
xxeis.eis_rsc_ins.rsec( 'Cycle Count Status Report','401','','XXWC_INVENTORY_CONTROL_SR_MGR',401,'DM027741','','','');
xxeis.eis_rsc_ins.rsec( 'Cycle Count Status Report','401','','XXWC_INV_ACCOUNTANT',401,'DM027741','','','');
xxeis.eis_rsc_ins.rsec( 'Cycle Count Status Report','401','','HDS_INVNTRY',401,'DM027741','','','');
xxeis.eis_rsc_ins.rsec( 'Cycle Count Status Report','401','','XXWC_AO_INV_ADJ',401,'DM027741','','','');
xxeis.eis_rsc_ins.rsec( 'Cycle Count Status Report','401','','XXWC_AO_INV_ADJ_REC',401,'DM027741','','','');
xxeis.eis_rsc_ins.rsec( 'Cycle Count Status Report','401','','XXWC_AO_INV_ADJ_PO_RPT',401,'DM027741','','','');
xxeis.eis_rsc_ins.rsec( 'Cycle Count Status Report','201','','XXWC_PURCHASING_INQUIRY',401,'DM027741','','','');
--Inserting Report Pivots - Cycle Count Status Report
xxeis.eis_rsc_ins.rpivot( 'Cycle Count Status Report',401,'Count Status','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Count Status
xxeis.eis_rsc_ins.rpivot_dtls( 'Cycle Count Status Report',401,'Count Status','COUNT_ITEMS','DATA_FIELD','SUM','','1','','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Cycle Count Status Report',401,'Count Status','COUNT_NOT_APPROVED','DATA_FIELD','SUM','','2','','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Cycle Count Status Report',401,'Count Status','REGION','ROW_FIELD','','','1','','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Cycle Count Status Report',401,'Count Status','DISTRICT','ROW_FIELD','','','2','','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Cycle Count Status Report',401,'Count Status','BEGINING_COUNT_DATE','PAGE_FIELD','','','1','','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Cycle Count Status Report',401,'Count Status','ORG_CODE','ROW_FIELD','','','3','','');
--Inserting Report Summary Calculation Columns For Pivot- Count Status
--Inserting Report   Version details- Cycle Count Status Report
xxeis.eis_rsc_ins.rv( 'Cycle Count Status Report','','Cycle Count Status Report','MT063505');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 401');
END IF;
END;
/
