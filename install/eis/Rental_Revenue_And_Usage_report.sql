--Report Name            : Rental Revenue and Usage Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View EIS_XXWC_OM_RENTAL_ITEMS_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_OM_RENTAL_ITEMS_V
Create or replace View XXEIS.EIS_XXWC_OM_RENTAL_ITEMS_V
 AS 
SELECT date_from
         ,date_to
         ,a.location
         ,a.region                                                 --Added By Mahender for TMS# 20141006-00045
         ,a.item_number
         ,a.item_description
         ,NVL (
             xxeis.eis_rs_xxwc_com_util_pkg.get_unit_cost (a.inventory_item_id
                                                          ,a.organization_id
                                                          ,a.item_number)
            ,0)
             average_cost
         ,qty_on_hand
         , -- and DATE_RECEIVED between xxeis.eis_rs_xxwc_com_util_pkg.get_date_from and xxeis.eis_rs_xxwc_com_util_pkg.get_date_to)
          -- - NVL(ISR.QOH,0)  - NVL(ISR.demand,0)+NVL(ISR.ON_ORD,0)) QUANTITY_ONHAND,
          qty_on_rent actual_rent_days
         --         ,a.revenue_generated   --Commented By Mahender for TMS# 20141006-00045
         ,a.qty_on_rent
         /*,                                                                                         -- LINE_ID,
          NVL (xxeis.eis_rs_xxwc_com_util_pkg.get_item_onhand_qty (a.inventory_item_id, a.organization_id)
              ,0)
             on_hand*/
         ,on_hand
         ,demand_qty                                               --Added By Mahender for TMS# 20141006-00045
         , (on_hand - demand_qty) varianc                          --Added By Mahender for TMS# 20141006-00045
         , (qty_on_rent + on_hand) owned                           --Added By Mahender for TMS# 20141006-00045
         , (qty_on_rent * 100) / (DECODE ( (qty_on_rent + on_hand), 0, 1, (qty_on_rent + on_hand)))
             utilization_rate                                      --Added By Mahender for TMS# 20141006-00045
     FROM (  SELECT                           -- OL.ORDERED_ITEM,OLRR.ATTRIBUTE12,OL.LINE_ID,RCTL.SALES_ORDER,
                   xxeis.eis_rs_xxwc_com_util_pkg.get_date_from date_from
                   ,xxeis.eis_rs_xxwc_com_util_pkg.get_date_to date_to
                   ,mtl.organization_code location
                   ,mtl.attribute9 region                          --Added By Mahender for TMS# 20141006-00045
                   ,msi.concatenated_segments item_number
                   ,msi.description item_description
                   --,SUM (olrr.ordered_quantity) qty_on_rent  --Commented By Mahender for TMS# 20141006-00045
                   , (SELECT NVL (SUM (ordered_quantity), 0)
                        FROM oe_order_lines_all
                       WHERE     inventory_item_id = msi.inventory_item_id
                             AND flow_status_code = 'AWAITING_RETURN'
                             AND ship_from_org_id = msi.organization_id)
                       qty_on_rent                                 --Added By Mahender for TMS# 20141006-00045
                   --                   ,NVL (SUM (NVL (rctl.extended_amount, 0) + NVL (ol.tax_value, 0)), 0) revenue_generated  --Commented By Mahender for TMS# 20141006-00045
                   ,msi.inventory_item_id
                   ,msi.organization_id
                   ,MAX (
                       xxeis.eis_po_xxwc_isr_util_pkg.get_onhand_inv (msi.inventory_item_id
                                                                     ,msi.organization_id))
                       qty_on_hand
                   ,NVL (
                       xxeis.eis_rs_xxwc_com_util_pkg.get_item_onhand_qty (msi.inventory_item_id
                                                                          ,msi.organization_id)
                      ,0)
                       on_hand
                   , (SELECT NVL (open_sales_orders, 0)
                        FROM apps.xxwc_inv_item_search_v
                       WHERE     inventory_item_id = msi.inventory_item_id
                             AND organization_id = msi.organization_id)
                       demand_qty                                  --Added By Mahender for TMS# 20141006-00045
               FROM apps.oe_order_lines_all ol
                   ,apps.oe_order_lines_all olrr
                   ,apps.ra_customer_trx_lines_all rctl
                   ,apps.mtl_system_items_b_kfv msi
                   ,apps.mtl_parameters mtl
              WHERE     rctl.interface_line_context = 'ORDER ENTRY'
                    AND rctl.interface_line_attribute6 = TO_CHAR (ol.line_id)
                    AND ol.link_to_line_id = olrr.line_id
--                    AND msi.inventory_item_id = olrr.inventory_item_id  -- --Commented By Mahender for TMS# 20150127-00162
                    AND msi.organization_id = olrr.ship_from_org_id
                    AND mtl.organization_id = msi.organization_id
                    AND TRUNC (TO_DATE (olrr.attribute12, 'YYYY/MM/DD HH24:MI:SS')) >=
                           xxeis.eis_rs_xxwc_com_util_pkg.get_date_from
                    AND TRUNC (TO_DATE (olrr.attribute12, 'YYYY/MM/DD HH24:MI:SS')) <=
                           xxeis.eis_rs_xxwc_com_util_pkg.get_date_to
                    --  and TRUNC(RCTL.CREATION_DATE)     >= TO_dATE('01-MAY-2014','DD-MON-YYYY')+0.25
                    --  AND TRUNC(RCTL.CREATION_DATE)     <= TO_dATE('31-MAY-2014','DD-MON-YYYY')+1.25
                    --  and OLRR.INVENTORY_ITEM_ID         =3067601
                    --  and OLRR.SHIP_FROM_ORG_ID          = 251
                    --    and OLRR.ORDERED_ITEM    = 'R176B46H90QP'
                    --      and MTL.organization_code= '025'
                    AND EXISTS
                           (SELECT 1
                              FROM apps.oe_order_lines_all ol2
                             WHERE ol2.ordered_item = 'Rental Charge' AND ol2.link_to_line_id = olrr.line_id)
           GROUP BY xxeis.eis_rs_xxwc_com_util_pkg.get_date_from
                   ,xxeis.eis_rs_xxwc_com_util_pkg.get_date_to
                   ,mtl.attribute9
                   ,mtl.organization_code
                   ,msi.concatenated_segments
                   ,msi.description
                   ,msi.inventory_item_id
                   ,msi.organization_id      -- ,olrr.line_id  --Commented By Mahender for TMS# 20141006-00045
                                       ) a/
set scan on define on
prompt Creating View Data for Rental Revenue and Usage Report
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_OM_RENTAL_ITEMS_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_OM_RENTAL_ITEMS_V',660,'','','','','MR020532','XXEIS','Eis Xxwc Om Rental Items V','EXORIV','','');
--Delete View Columns for EIS_XXWC_OM_RENTAL_ITEMS_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_OM_RENTAL_ITEMS_V',660,FALSE);
--Inserting View Columns for EIS_XXWC_OM_RENTAL_ITEMS_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_RENTAL_ITEMS_V','ITEM_DESCRIPTION',660,'Item Description','ITEM_DESCRIPTION','','','','MR020532','VARCHAR2','','','Item Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_RENTAL_ITEMS_V','ITEM_NUMBER',660,'Item Number','ITEM_NUMBER','','','','MR020532','VARCHAR2','','','Item Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_RENTAL_ITEMS_V','LOCATION',660,'Location','LOCATION','','','','MR020532','VARCHAR2','','','Location','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_RENTAL_ITEMS_V','QTY_ON_HAND',660,'Qty On Hand','QTY_ON_HAND','','','','MR020532','NUMBER','','','Qty On Hand','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_RENTAL_ITEMS_V','UTILIZATION_RATE',660,'Utilization Rate','UTILIZATION_RATE','','','','MR020532','NUMBER','','','Utilization Rate','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_RENTAL_ITEMS_V','QTY_ON_RENT',660,'Qty On Rent','QTY_ON_RENT','','','','MR020532','NUMBER','','','Qty On Rent','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_RENTAL_ITEMS_V','REGION',660,'Region','REGION','','','','MR020532','VARCHAR2','','','Region','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_RENTAL_ITEMS_V','DEMAND_QTY',660,'Demand Qty','DEMAND_QTY','','','','MR020532','NUMBER','','','Demand Qty','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_RENTAL_ITEMS_V','VARIANC',660,'Varianc','VARIANC','','','','MR020532','NUMBER','','','Varianc','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_RENTAL_ITEMS_V','OWNED',660,'Owned','OWNED','','','','MR020532','NUMBER','','','Owned','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_RENTAL_ITEMS_V','AVERAGE_COST',660,'Average Cost','AVERAGE_COST','','~T~D~2','','MR020532','NUMBER','','','Average Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_RENTAL_ITEMS_V','REVENUE_GENERATED',660,'Revenue Generated','REVENUE_GENERATED','','','','MR020532','NUMBER','','','Revenue Generated','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_RENTAL_ITEMS_V','DATE_FROM',660,'Date From','DATE_FROM','','','','MR020532','DATE','','','Date From','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_RENTAL_ITEMS_V','DATE_TO',660,'Date To','DATE_TO','','','','MR020532','DATE','','','Date To','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_RENTAL_ITEMS_V','ACTUAL_RENT_DAYS',660,'Actual Rent Days','ACTUAL_RENT_DAYS','','','','MR020532','NUMBER','','','Actual Rent Days','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_RENTAL_ITEMS_V','ON_HAND',660,'On Hand','ON_HAND','','','','MR020532','NUMBER','','','On Hand','','','');
--Inserting View Components for EIS_XXWC_OM_RENTAL_ITEMS_V
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_RENTAL_ITEMS_V','OE_ORDER_HEADERS',660,'OE_ORDER_HEADERS_ALL','OH','OH','MR020532','MR020532','-1','Oe Order Headers All Stores Header Information For','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_RENTAL_ITEMS_V','OE_ORDER_LINES',660,'OE_ORDER_LINES_ALL','OL','OL','MR020532','MR020532','-1','Oe Order Lines All Stores Information For All Orde','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_RENTAL_ITEMS_V','HZ_PARTIES',660,'HZ_PARTIES','HZP','HZP','MR020532','MR020532','-1','Information About Parties Such As Organizations, P','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_RENTAL_ITEMS_V','MTL_PARAMETERS',660,'MTL_PARAMETERS','MTP','MTP','MR020532','MR020532','-1','Inventory Control Options And Defaults','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_RENTAL_ITEMS_V','MTL_SYSTEM_ITEMS_KFV',660,'MTL_SYSTEM_ITEMS_B','MSI','MSI','MR020532','MR020532','-1','Inventory Item Definitions','','','','');
--Inserting View Component Joins for EIS_XXWC_OM_RENTAL_ITEMS_V
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_RENTAL_ITEMS_V','OE_ORDER_HEADERS','OH',660,'EXORIV.HEADER_ID','=','OH.HEADER_ID(+)','','','','Y','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_RENTAL_ITEMS_V','OE_ORDER_LINES','OL',660,'EXORIV.LINE_ID','=','OL.LINE_ID(+)','','','','Y','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_RENTAL_ITEMS_V','HZ_PARTIES','HZP',660,'EXORIV.PARTY_ID','=','HZP.PARTY_ID(+)','','','','Y','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_RENTAL_ITEMS_V','MTL_PARAMETERS','MTP',660,'EXORIV.ORGANIZATION_ID','=','MTP.ORGANIZATION_ID(+)','','','','Y','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_RENTAL_ITEMS_V','MTL_SYSTEM_ITEMS_KFV','MSI',660,'EXORIV.INVENTORY_ITEM_ID','=','MSI.INVENTORY_ITEM_ID(+)','','','','Y','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_RENTAL_ITEMS_V','MTL_SYSTEM_ITEMS_KFV','MSI',660,'EXORIV.MSI_ORGANIZATION_ID','=','MSI.ORGANIZATION_ID(+)','','','','Y','MR020532','','');
END;
/
set scan on define on
prompt Creating Report LOV Data for Rental Revenue and Usage Report
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Rental Revenue and Usage Report
xxeis.eis_rs_ins.lov( 660,'SELECT ORGANIZATION_CODE WAREHOUSE,ORGANIZATION_NAME FROM ORG_ORGANIZATION_DEFINITIONS OOD WHERE SYSDATE < NVL(OOD.DISABLE_DATE,SYSDATE+1) AND EXISTS (SELECT 1 FROM XXEIS.EIS_ORG_ACCESS_V  WHERE organization_id = ood.organization_id )','','OM WAREHOUSE','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( '','select msi.segment1 item_number, msi.description item_description from apps.mtl_system_items msi where organization_id=222','','XX EIS WC ITEM MASTER LOV','Display the list of WC Item Master','MR020532',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Rental Revenue and Usage Report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Rental Revenue and Usage Report
xxeis.eis_rs_utility.delete_report_rows( 'Rental Revenue and Usage Report' );
--Inserting Report - Rental Revenue and Usage Report
xxeis.eis_rs_ins.r( 660,'Rental Revenue and Usage Report','','Provide revenue figures from the White Cap Rental Business on a summary or detailed basis for specific warehouses.','','','','MR020532','EIS_XXWC_OM_RENTAL_ITEMS_V','Y','','','MR020532','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - Rental Revenue and Usage Report
xxeis.eis_rs_ins.rc( 'Rental Revenue and Usage Report',660,'ITEM_DESCRIPTION','Item Description','Item Description','','','default','','4','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_RENTAL_ITEMS_V','','');
xxeis.eis_rs_ins.rc( 'Rental Revenue and Usage Report',660,'ITEM_NUMBER','Item Number','Item Number','','','default','','3','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_RENTAL_ITEMS_V','','');
xxeis.eis_rs_ins.rc( 'Rental Revenue and Usage Report',660,'LOCATION','Location','Location','','','default','','2','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_RENTAL_ITEMS_V','','');
xxeis.eis_rs_ins.rc( 'Rental Revenue and Usage Report',660,'QTY_ON_HAND','Quantity Onhand','Qty On Hand','','~~~','default','','6','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_RENTAL_ITEMS_V','','');
xxeis.eis_rs_ins.rc( 'Rental Revenue and Usage Report',660,'REGION','Region','Region','','','default','','1','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_RENTAL_ITEMS_V','','');
xxeis.eis_rs_ins.rc( 'Rental Revenue and Usage Report',660,'DEMAND_QTY','Quantity Demand','Demand Qty','','~~~','default','','7','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_RENTAL_ITEMS_V','','');
xxeis.eis_rs_ins.rc( 'Rental Revenue and Usage Report',660,'QTY_ON_RENT','Quantity On Rent','Qty On Rent','','~~~','default','','8','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_RENTAL_ITEMS_V','','');
xxeis.eis_rs_ins.rc( 'Rental Revenue and Usage Report',660,'VARIANC','Variance','Varianc','','~~~','default','','9','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_RENTAL_ITEMS_V','','');
xxeis.eis_rs_ins.rc( 'Rental Revenue and Usage Report',660,'UTILIZATION_RATE','Utilization Rate%','Utilization Rate','','~,~.~2','default','','10','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_RENTAL_ITEMS_V','','');
xxeis.eis_rs_ins.rc( 'Rental Revenue and Usage Report',660,'OWNED','Owned','Owned','','','','','5','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_RENTAL_ITEMS_V','','');
--Inserting Report Parameters - Rental Revenue and Usage Report
xxeis.eis_rs_ins.rp( 'Rental Revenue and Usage Report',660,'Item Number','Item Number','ITEM_NUMBER','IN','XX EIS WC ITEM MASTER LOV','','VARCHAR2','Y','Y','4','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Rental Revenue and Usage Report',660,'Warehouse','Warehouse','LOCATION','IN','OM WAREHOUSE','','VARCHAR2','N','Y','3','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Rental Revenue and Usage Report',660,'Date From','Date From','','IN','','SELECT TRUNC (ADD_MONTHS (SYSDATE, -1)) FROM DUAL','DATE','Y','Y','1','','N','SQL','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Rental Revenue and Usage Report',660,'Date To','Date To','','IN','','','DATE','Y','Y','2','','N','CURRENT_DATE','MR020532','Y','N','','','');
--Inserting Report Conditions - Rental Revenue and Usage Report
xxeis.eis_rs_ins.rcn( 'Rental Revenue and Usage Report',660,'ITEM_NUMBER','IN',':Item Number','','','Y','4','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Rental Revenue and Usage Report',660,'LOCATION','IN',':Warehouse','','','Y','3','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Rental Revenue and Usage Report',660,'','','','','AND EXORIV.OWNED >0','Y','1','','MR020532');
--Inserting Report Sorts - Rental Revenue and Usage Report
--Inserting Report Triggers - Rental Revenue and Usage Report
xxeis.eis_rs_ins.rt( 'Rental Revenue and Usage Report',660,'begin
Xxeis.Eis_Rs_Xxwc_Com_Util_Pkg.set_Date_From(:Date From);
Xxeis.Eis_Rs_Xxwc_Com_Util_Pkg.set_Date_to(:Date To);
end;
','B','Y','MR020532');
--Inserting Report Templates - Rental Revenue and Usage Report
--Inserting Report Portals - Rental Revenue and Usage Report
--Inserting Report Dashboards - Rental Revenue and Usage Report
--Inserting Report Security - Rental Revenue and Usage Report
xxeis.eis_rs_ins.rsec( 'Rental Revenue and Usage Report','','SS084202','',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Rental Revenue and Usage Report','','10010432','',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Rental Revenue and Usage Report','660','','50886',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Rental Revenue and Usage Report','20005','','50900',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Rental Revenue and Usage Report','660','','51044',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Rental Revenue and Usage Report','660','','50860',660,'MR020532','','');
--Inserting Report Pivots - Rental Revenue and Usage Report
xxeis.eis_rs_ins.rpivot( 'Rental Revenue and Usage Report',660,'Pivot','1','1,0|1,2,1','1,1,1,1|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rs_ins.rpivot_dtls( 'Rental Revenue and Usage Report',660,'Pivot','QTY_ON_HAND','DATA_FIELD','SUM','','2','','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Rental Revenue and Usage Report',660,'Pivot','ITEM_DESCRIPTION','ROW_FIELD','','','2','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'Rental Revenue and Usage Report',660,'Pivot','REGION','ROW_FIELD','','','1','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Rental Revenue and Usage Report',660,'Pivot','DEMAND_QTY','DATA_FIELD','SUM','','3','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Rental Revenue and Usage Report',660,'Pivot','QTY_ON_RENT','DATA_FIELD','SUM','','4','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Rental Revenue and Usage Report',660,'Pivot','VARIANC','DATA_FIELD','SUM','','5','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Rental Revenue and Usage Report',660,'Pivot','UTILIZATION_RATE','ROW_FIELD','','','3','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Rental Revenue and Usage Report',660,'Pivot','OWNED','DATA_FIELD','SUM','','1','','');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
END;
/
set scan on define on
