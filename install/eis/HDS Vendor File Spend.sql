--Report Name            : HDS Vendor File Spend
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View XXEIS_501485_OQULTQ_V
prompt Table type Objects cannot be imported
set scan off define off
prompt Creating Object APPS.XXEIS_501485_OQULTQ_V
set scan off define off
DECLARE
mod_exist varchar2(1);
stmt CLOB;
v_objlength INTEGER := 0;
v_offset INTEGER := 1;
v_sql_long DBMS_SQL.varchar2s;
v_sql_long_count INTEGER := 1;
c NUMBER;
r NUMBER;
l_stmt varchar2(32000);
l_view_cur pls_integer;
l_view_rows NUMBER;
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(91000);
IF mod_exist = 'Y' THEN 
dbms_lob.createtemporary(stmt,TRUE);
dbms_lob.write( lob_loc   => stmt,amount    => length('create or replace view APPS.XXEIS_501485_OQULTQ_V
 ("VENDOR_NAME","ADDRESS_LINE1","ADDRESS_LINE2","CITY","STATE","ZIP","INVOICE_ID","AMOUNT_PAID","VENDOR_NUMBER","NUM_1099","PHONE","PAYMENT_METHOD_LOOKUP_CODE","GL_DATE"
) as '),offset    => 1, BUFFER    => 'create or replace view APPS.XXEIS_501485_OQULTQ_V
 ("VENDOR_NAME","ADDRESS_LINE1","ADDRESS_LINE2","CITY","STATE","ZIP","INVOICE_ID","AMOUNT_PAID","VENDOR_NUMBER","NUM_1099","PHONE","PAYMENT_METHOD_LOOKUP_CODE","GL_DATE"
) as ');
l_stmt :=  'select pv.vendor_name
      ,pvs.address_line1
      ,pvs.address_line2
      ,pvs.city
      ,pvs.state
      ,pvs.zip
      ,count(i.invoice_id) invoice_id
      ,sum(i.amount_paid) amount_paid
      ,pv.segment1 vendor_number
      ,pv.num_1099
      ,pvs.phone
      ,pvs.payment_method_lookup_code
      ,i.gl_date
from  ap.ap_suppliers pv
     ,ap.ap_supplier_sites_all pvs
     ,ap.ap_invoices_all i
where pv.vendor_id = pvs.vendor_id
and   i.vendor_site_id = pvs.vendor_site_id
and   pvs.org_id = 163
and   pvs.pay_group_lookup_code NOT IN (''EMPLOYEE'',''TAX'')
and   pvs.inactive_date IS NULL
and   i.org_id = 163
group by
pv.vendor_name
      ,pvs.address_line1
      ,pvs.address_line2
      ,pvs.city
      ,pvs.state
      ,pvs.zip
      ,pv.segment1 
      ,pv.num_1099
      ,pvs.phone
      ,pvs.payment_method_lookup_code
      ,i.gl_date

';
dbms_lob.APPEND( dest_lob  => stmt,      src_lob   => l_stmt );
v_objlength := DBMS_LOB.getlength (stmt);
IF v_objlength <= 32000 THEN 
EXECUTE IMMEDIATE to_char(stmt);
ELSE
v_offset := 1; 
WHILE v_offset <= v_objlength
LOOP
-- each record is 256 bytes from the LOB
v_sql_long (v_sql_long_count) := DBMS_LOB.SUBSTR (stmt, 256, v_offset);
-- increment the record count
v_sql_long_count := v_sql_long_count + 1;
-- figure out the new offset
v_offset := v_offset + 256;
END LOOP;
BEGIN
  -- open cursor
 c := DBMS_SQL.open_cursor;
 -- parse VARCHAR2S table
 DBMS_SQL.parse (c, v_sql_long, 1, v_sql_long_count - 1, FALSE, 1);
 -- Execute the cursor
r := DBMS_SQL.EXECUTE (c);
-- Close the cursor
 DBMS_SQL.close_cursor (c);
 END;
END IF;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 91000');
END IF;
END;
/
prompt Creating Object Data XXEIS_501485_OQULTQ_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(91000);
IF mod_exist = 'Y' THEN 
--Inserting Object XXEIS_501485_OQULTQ_V
xxeis.eis_rsc_ins.v( 'XXEIS_501485_OQULTQ_V',91000,'Paste SQL View for HDS Vendor File Spend','1.0','','','ID020048','APPS','HDS Vendor File Spend View','X5OV','','Y','VIEW','US','','');
--Delete Object Columns for XXEIS_501485_OQULTQ_V
xxeis.eis_rsc_utility.delete_view_rows('XXEIS_501485_OQULTQ_V',91000,FALSE);
--Inserting Object Columns for XXEIS_501485_OQULTQ_V
xxeis.eis_rsc_ins.vc( 'XXEIS_501485_OQULTQ_V','VENDOR_NAME',91000,'','','','','','ID020048','VARCHAR2','','','Vendor Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_501485_OQULTQ_V','ADDRESS_LINE1',91000,'','','','','','ID020048','VARCHAR2','','','Address Line1','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_501485_OQULTQ_V','ADDRESS_LINE2',91000,'','','','','','ID020048','VARCHAR2','','','Address Line2','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_501485_OQULTQ_V','CITY',91000,'','','','','','ID020048','VARCHAR2','','','City','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_501485_OQULTQ_V','STATE',91000,'','','','','','ID020048','VARCHAR2','','','State','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_501485_OQULTQ_V','ZIP',91000,'','','','','','ID020048','VARCHAR2','','','Zip','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_501485_OQULTQ_V','VENDOR_NUMBER',91000,'','','','','','ID020048','VARCHAR2','','','Vendor Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_501485_OQULTQ_V','NUM_1099',91000,'','','','','','ID020048','VARCHAR2','','','Num 1099','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_501485_OQULTQ_V','PHONE',91000,'','','','','','ID020048','VARCHAR2','','','Phone','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_501485_OQULTQ_V','PAYMENT_METHOD_LOOKUP_CODE',91000,'','','','','','ID020048','VARCHAR2','','','Payment Method Lookup Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_501485_OQULTQ_V','AMOUNT_PAID',91000,'','','','~T~D~2','','ID020048','NUMBER','','','Amount Paid','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_501485_OQULTQ_V','INVOICE_ID',91000,'','','','','','ID020048','NUMBER','','','Invoice Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_501485_OQULTQ_V','GL_DATE',91000,'','','','','','ID020048','DATE','','','Gl Date','','','','US');
--Inserting Object Components for XXEIS_501485_OQULTQ_V
--Inserting Object Component Joins for XXEIS_501485_OQULTQ_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 91000');
END IF;
END;
/
--There are no LOV/LOV TL entries for the report HDS Vendor File Spend
prompt Creating Report Data for HDS Vendor File Spend
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(91000);
IF mod_exist = 'Y' THEN 
--Deleting Report data - HDS Vendor File Spend
xxeis.eis_rsc_utility.delete_report_rows( 'HDS Vendor File Spend' );
--Inserting Report - HDS Vendor File Spend
xxeis.eis_rsc_ins.r( 91000,'HDS Vendor File Spend','','HDS Vendor File Spend','','','','XXEIS_RS_ADMIN','XXEIS_501485_OQULTQ_V','Y','','select pv.vendor_name
      ,pvs.address_line1
      ,pvs.address_line2
      ,pvs.city
      ,pvs.state
      ,pvs.zip
      ,count(i.invoice_id) invoice_id
      ,sum(i.amount_paid) amount_paid
      ,pv.segment1 vendor_number
      ,pv.num_1099
      ,pvs.phone
      ,pvs.payment_method_lookup_code
      ,i.gl_date
from  ap.ap_suppliers pv
     ,ap.ap_supplier_sites_all pvs
     ,ap.ap_invoices_all i
where pv.vendor_id = pvs.vendor_id
and   i.vendor_site_id = pvs.vendor_site_id
and   pvs.org_id = 163
and   pvs.pay_group_lookup_code NOT IN (''EMPLOYEE'',''TAX'')
and   pvs.inactive_date IS NULL
and   i.org_id = 163
group by
pv.vendor_name
      ,pvs.address_line1
      ,pvs.address_line2
      ,pvs.city
      ,pvs.state
      ,pvs.zip
      ,pv.segment1 
      ,pv.num_1099
      ,pvs.phone
      ,pvs.payment_method_lookup_code
      ,i.gl_date
','XXEIS_RS_ADMIN','','N','HDS Standard Reports','','CSV,EXCEL,Pivot Excel,','N','','','','','','N','APPS','US','','','','');
--Inserting Report Columns - HDS Vendor File Spend
xxeis.eis_rsc_ins.rc( 'HDS Vendor File Spend',91000,'ADDRESS_LINE2','Address Line2','','','','default','','3','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_501485_OQULTQ_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Vendor File Spend',91000,'PAYMENT_METHOD_LOOKUP_CODE','Payment Method Lookup Code','','','','','','12','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_501485_OQULTQ_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Vendor File Spend',91000,'ZIP','Zip','','','','default','','6','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_501485_OQULTQ_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Vendor File Spend',91000,'VENDOR_NUMBER','Vendor Number','','','','default','','9','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_501485_OQULTQ_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Vendor File Spend',91000,'ADDRESS_LINE1','Address Line1','','','','default','','2','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_501485_OQULTQ_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Vendor File Spend',91000,'CITY','City','','','','default','','4','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_501485_OQULTQ_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Vendor File Spend',91000,'PHONE','Phone','','','','','','11','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_501485_OQULTQ_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Vendor File Spend',91000,'STATE','State','','','','default','','5','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_501485_OQULTQ_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Vendor File Spend',91000,'VENDOR_NAME','Vendor Name','','','','default','','1','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_501485_OQULTQ_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Vendor File Spend',91000,'NUM_1099','Num 1099','','','','default','','10','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_501485_OQULTQ_V','','','','US','');
--Inserting Report Parameters - HDS Vendor File Spend
xxeis.eis_rsc_ins.rp( 'HDS Vendor File Spend',91000,'GL DATE FROM','GL DATE FROM','GL_DATE','>=','','','DATE','N','Y','1','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','XXEIS_501485_OQULTQ_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS Vendor File Spend',91000,'GL DATE TO','GL DATE TO','GL_DATE','<=','','','DATE','N','Y','2','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','XXEIS_501485_OQULTQ_V','','','US','');
--Inserting Dependent Parameters - HDS Vendor File Spend
--Inserting Report Conditions - HDS Vendor File Spend
xxeis.eis_rsc_ins.rcnh( 'HDS Vendor File Spend',91000,'GL_DATE BETWEEN :GL DATE FROM  :GL DATE TO','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','GL_DATE','','GL DATE FROM','','','GL DATE TO','','XXEIS_501485_OQULTQ_V','','','','','','BETWEEN','Y','Y','','','','','1',91000,'HDS Vendor File Spend','GL_DATE BETWEEN :GL DATE FROM  :GL DATE TO');
--Inserting Report Sorts - HDS Vendor File Spend
--Inserting Report Triggers - HDS Vendor File Spend
--inserting report templates - HDS Vendor File Spend
--Inserting Report Portals - HDS Vendor File Spend
--inserting report dashboards - HDS Vendor File Spend
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'HDS Vendor File Spend','91000','XXEIS_501485_OQULTQ_V','XXEIS_501485_OQULTQ_V','N','');
--inserting report security - HDS Vendor File Spend
xxeis.eis_rsc_ins.rsec( 'HDS Vendor File Spend','200','','HDS_AP_TRNS_ENTRY_US_IWO',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Vendor File Spend','200','','HDS_AP_TRNS_ENTRY_US_GSC',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Vendor File Spend','200','','HDS_AP_SUPPLIER_MAINT_US_IWO',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Vendor File Spend','200','','HDS_AP_SUPPLIER_MAINT_US_GSC',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Vendor File Spend','200','','HDS_AP_SUPPLIER_MAINT_CAD_GSC',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Vendor File Spend','200','','XXWC_PAY_MANAGER',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Vendor File Spend','200','','HDS_AP_MANAGER',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Vendor File Spend','200','','HDS_AP_MGR_NOSUP_US_IWO',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Vendor File Spend','200','','HDS_AP_MGR_NOSUP_US_GSC',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Vendor File Spend','200','','XXWC_PAYABLES_INQUIRY',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Vendor File Spend','200','','HDS_AP_INQUIRY_US_IWO',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Vendor File Spend','200','','HDS_AP_INQUIRY_US_GSC',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Vendor File Spend','200','','XXWC_PAY_DISBURSE',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Vendor File Spend','200','','HDS_AP_DISBUREMTS_US_IWO',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Vendor File Spend','200','','HDS_AP_DISBURSEMTS_US_GSC',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Vendor File Spend','200','','HDS_PAYABLES_CLOSE_GLBL',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Vendor File Spend','200','','XXWC_PAY_W_CALENDAR',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Vendor File Spend','200','','XXWC_PAY_NO_CALENDAR',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Vendor File Spend','200','','HDS_AP_ADMIN_US_IWO',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Vendor File Spend','200','','HDS_AP_ADMIN_US_GSCIWO',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Vendor File Spend','200','','HDS_AP_ADMIN_US_GSC',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Vendor File Spend','200','','HDS_AP_CAN_GSC',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Vendor File Spend','200','','HDS_OIE_USER',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Vendor File Spend','101','','XXCUS_GL_MANAGER_GLOBAL',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Vendor File Spend','101','','XXCUS_GL_ACCOUNTANT_GLOBAL',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Vendor File Spend','200','','HDS_AP_TRNS_ENTRY_CAD_GSC',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Vendor File Spend','200','','HDS_AP_MGR_NOSUP_CAD_GSC',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Vendor File Spend','200','','HDS_AP_INQ_CANADA',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Vendor File Spend','200','','HDS_AP_DISBURSEMTS_CAD_GSC',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Vendor File Spend','200','','HDS_AP_ADMIN_CAD_GSC',91000,'XXEIS_RS_ADMIN','','','');
--Inserting Report Pivots - HDS Vendor File Spend
--Inserting Report   Version details- HDS Vendor File Spend
xxeis.eis_rsc_ins.rv( 'HDS Vendor File Spend','','HDS Vendor File Spend','AB063501');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 91000');
END IF;
END;
/
