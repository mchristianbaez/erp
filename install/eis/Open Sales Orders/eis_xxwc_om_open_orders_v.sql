Create or replace View XXEIS.EIS_XXWC_OM_OPEN_ORDERS_V
(WAREHOUSE,SALES_PERSON_NAME,CREATED_BY,ORDER_NUMBER,ORDERED_DATE,LINE_CREATION_DATE,ORDER_TYPE,QUOTE_NUMBER,ORDER_LINE_STATUS,ORDER_HEADER_STATUS,CUSTOMER_NUMBER,CUSTOMER_NAME,CUSTOMER_JOB_NAME,ORDER_AMOUNT,SHIPPING_METHOD,SHIP_TO_CITY,ZIP_CODE,SCHEDULE_SHIP_DATE,HEADER_STATUS,ITEM_NUMBER,ITEM_DESCRIPTION,QTY,PAYMENT_TERMS,ORDER_HEADER_ID,ORDER_LINE_ID,CUST_ACCOUNT_ID,CUST_ACCT_SITE_ID,
 SALESREP_ID,PARTY_ID,TRANSACTION_TYPE_ID,MTP_ORGANIZATION_ID,HZCS_SHIP_TO_STE_ID,HZPS_SHP_TO_PRT_ID) AS 
SELECT mtp.organization_code warehouse,
    rep.name sales_person_name,
    ppf.full_name created_by,
    OH.ORDER_NUMBER ORDER_NUMBER,
    /* (OL.LINE_NUMBER
    ||'.'
    ||OL.SHIPMENT_NUMBER) LINE_NUMBER,*/
    TRUNC(OH.ORDERED_DATE) ORDERED_DATE,
    TRUNC(ol.creation_date) line_creation_date,
    OTT.NAME ORDER_TYPE,
    OH.QUOTE_NUMBER,
    XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_ORDER_LINE_STATUS(OL.LINE_ID, ol.flow_status_code) ORDER_LINE_STATUS,
    --apps.OE_LINE_STATUS_PUB.get_line_status(ol.line_id,ol.flow_status_code) ORDER_LINE_STATUS,
    flv_order_status.meaning order_header_status,
    --OL.FLOW_STATUS_CODE order_line_status,
    HCA.ACCOUNT_NUMBER CUSTOMER_NUMBER,
    NVL(hca.account_name,hzp.party_name) customer_name,
    hzcs_ship_to.location customer_job_name,
    ROUND((OL.UNIT_SELLING_PRICE * DECODE(ott.order_category_code,'RETURN',OL.ORDERED_QUANTITY*-1,OL.ORDERED_QUANTITY)),2) ORDER_AMOUNT,
    flv_ship_mtd.meaning shipping_method,
    hzl_ship_to.city ship_to_city,
    hzl_ship_to.postal_code zip_code,
    OL.SCHEDULE_SHIP_DATE SCHEDULE_SHIP_DATE,
    OH.FLOW_STATUS_CODE HEADER_STATUS,
    msi.segment1 item_number ,
    MSI.DESCRIPTION ITEM_DESCRIPTION,
    DECODE(ott.order_category_code,'RETURN',(NVL(Ol.ordered_quantity,0)*-1),NVL(Ol.ordered_quantity,0)) QTY,
    rt.name Payment_terms,
    --Primary Keys Added
    Oh.Header_Id Order_Header_Id,
    Ol.Line_Id Order_Line_Id,
    hca.cust_account_id cust_account_id,
    Hcas_Ship_To.Cust_Acct_Site_Id Cust_Acct_Site_Id,
    Rep.Salesrep_Id Salesrep_Id,
    Hzp.Party_Id Party_Id,
    ott.transaction_type_id transaction_type_id,
    Mtp.Organization_Id Mtp_Organization_Id,
    Hzcs_Ship_To.Site_Use_Id Hzcs_Ship_To_Ste_Id,
    hzps_ship_to.party_site_id hzps_shp_to_prt_id
    --descr#flexfield#start
    --descr#flexfield#end
    --gl#accountff#start
    --gl#accountff#end
  FROM oe_order_headers oh,
    oe_order_lines ol,
    hz_cust_accounts hca,
    hz_parties hzp,
    oe_transaction_types_vl ott,
    ra_salesreps rep,
    mtl_parameters mtp,
    hz_cust_site_uses hzcs_ship_to,
    hz_cust_acct_sites hcas_ship_to,
    HZ_PARTY_SITES HZPS_SHIP_TO,
    hz_locations hzl_ship_to,
    hr_all_organization_units haou,
    PER_PEOPLE_F PPF,
    FND_USER FU,
    FND_LOOKUP_VALUES_VL FLV_SHIP_MTD,
    oe_lookups flv_order_status,
    RA_TERMS_VL RT,
    MTL_SYSTEM_ITEMS_KFV MSI,
    FND_LOOKUP_VALUES_VL FLV_LINE_STATUS
  WHERE ol.header_id                 = oh.header_id
  AND oh.sold_to_org_id              = hca.cust_account_id(+)
  AND hca.party_id                   = hzp.party_id(+)
  AND OH.ORDER_TYPE_ID               = OTT.TRANSACTION_TYPE_ID
  AND ol.ship_from_org_id            = mtp.organization_id(+)
  AND oh.ship_to_org_id              = hzcs_ship_to.site_use_id(+)
  AND hzcs_ship_to.cust_acct_site_id = hcas_ship_to.cust_acct_site_id(+)
  AND hcas_ship_to.party_site_id     = hzps_ship_to.party_site_id(+)
  AND hzl_ship_to.location_id(+)     = hzps_ship_to.location_id
  AND OH.SALESREP_ID                 = REP.SALESREP_ID(+)
  AND oh.org_id                      = rep.org_id(+)
  AND OL.FLOW_STATUS_CODE NOT       IN ('CLOSED','CANCELLED')
  AND MTP.ORGANIZATION_ID            = HAOU.ORGANIZATION_ID
    --AND OH.CREATED_BY                  = PPF.PERSON_ID
  AND FU.USER_ID                   =OH.CREATED_BY
  AND FU.EMPLOYEE_ID               =PPF.PERSON_ID(+)
  AND FLV_SHIP_MTD.LOOKUP_TYPE(+)  ='SHIP_METHOD'
  AND FLV_SHIP_MTD.LOOKUP_CODE(+)  = OL.SHIPPING_METHOD_CODE
  AND flv_order_status.lookup_type ='FLOW_STATUS'
  AND flv_order_status.lookup_code = Oh.flow_status_code
  AND oh.payment_term_id(+)        = rt.term_id
  AND ott.name                    IN('STANDARD ORDER','COUNTER ORDER')
  AND ((ol.FLOW_STATUS_CODE        = 'ENTERED'
  AND TRUNC (oh.creation_date)     < TRUNC(sysdate))
  OR ol.FLOW_STATUS_CODE          <> 'ENTERED' )
  AND TRUNC (oh.creation_date) BETWEEN NVL (ppf.effective_start_date, TRUNC (oh.creation_date) ) AND NVL (ppf.effective_end_date, TRUNC (oh.creation_date) )
  AND OH.TRANSACTION_PHASE_CODE     <>'N'
  AND MSI.ORGANIZATION_ID            = OL.SHIP_FROM_ORG_ID
  AND MSI.INVENTORY_ITEM_ID          = OL.INVENTORY_ITEM_ID
  AND FLV_LINE_STATUS.LOOKUP_TYPE(+) ='LINE_FLOW_STATUS'
  AND FLV_LINE_STATUS.LOOKUP_CODE(+) = OL.FLOW_STATUS_CODE
/