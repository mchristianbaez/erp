--Report Name            : Open Sales Orders Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View EIS_XXWC_OM_OPEN_ORDERS_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_OM_OPEN_ORDERS_V
Create or replace View XXEIS.EIS_XXWC_OM_OPEN_ORDERS_V
 AS 
select /*+ INDEX(oh XXWC_OE_ORDER_HEADERS_ALL_N2) index(ol XXWC_OE_ORDER_LINES_ALL_N3) index(hcas_ship_to HZ_CUST_ACCT_SITES_N1) */
 mtp.organization_code warehouse,
    REP.name SALES_PERSON_NAME,
  --  ppf.full_name created_by,
   XXEIS.eis_rs_xxwc_com_util_pkg.GET_EMPOLYEE_NAME(FU.USER_ID,oh.creation_date)created_by,
    OH.ORDER_NUMBER ORDER_NUMBER,
    /* (OL.LINE_NUMBER
    ||'.'
    ||OL.SHIPMENT_NUMBER) LINE_NUMBER,*/
    TRUNC(OH.ORDERED_DATE) ORDERED_DATE,
    TRUNC(ol.creation_date) line_creation_date,
    OTT.NAME ORDER_TYPE,
    OH.QUOTE_NUMBER,
    XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_ORDER_LINE_STATUS(OL.LINE_ID, ol.flow_status_code) ORDER_LINE_STATUS,
    --apps.OE_LINE_STATUS_PUB.get_line_status(ol.line_id,ol.flow_status_code) ORDER_LINE_STATUS,
    flv_order_status.meaning order_header_status,
    --OL.FLOW_STATUS_CODE order_line_status,
    HCA.ACCOUNT_NUMBER CUSTOMER_NUMBER,
    NVL(hca.account_name,hzp.party_name) customer_name,
    HZCS_SHIP_TO.LOCATION CUSTOMER_JOB_NAME,
    ROUND((OL.UNIT_SELLING_PRICE * DECODE(OL.LINE_CATEGORY_CODE,'RETURN',OL.ORDERED_QUANTITY *-1,OL.ORDERED_QUANTITY)),2) ORDER_AMOUNT,
    flv_ship_mtd.meaning shipping_method,
    hzl_ship_to.city ship_to_city,
    hzl_ship_to.postal_code zip_code,
    OL.SCHEDULE_SHIP_DATE SCHEDULE_SHIP_DATE,
    OH.FLOW_STATUS_CODE HEADER_STATUS,
    msi.segment1 item_number ,
    MSI.DESCRIPTION ITEM_DESCRIPTION,
    DECODE(ol.line_category_code,'RETURN',(NVL(Ol.ordered_quantity,0)*-1),NVL(Ol.ordered_quantity,0)) QTY,
    rt.name Payment_terms,
    oh.request_date,
    --Primary Keys Added
    Oh.Header_Id Order_Header_Id,
    Ol.Line_Id Order_Line_Id,
    hca.cust_account_id cust_account_id,
    Hcas_Ship_To.Cust_Acct_Site_Id Cust_Acct_Site_Id,
    Rep.Salesrep_Id Salesrep_Id,
    Hzp.Party_Id Party_Id,
    ott.transaction_type_id transaction_type_id,
    Mtp.Organization_Id Mtp_Organization_Id,
    Hzcs_Ship_To.Site_Use_Id Hzcs_Ship_To_Ste_Id,
    HZPS_SHIP_TO.PARTY_SITE_ID HZPS_SHP_TO_PRT_ID,
    OL.USER_ITEM_DESCRIPTION USER_ITEM_DESCRIPTION,
    xxeis.EIS_RS_XXWC_COM_UTIL_PKG.GET_DOC_DATE(oh.header_id, ol.line_id,ol.INVENTORY_ITEM_ID,ol.ship_from_org_id,oh.ship_from_org_id) delivery_doc_date
    --xxeis.EIS_RS_XXWC_COM_UTIL_PKG.GET_DOC_DATE(oh.header_id, oh.ship_from_org_id) delivery_doc_date
    --descr#flexfield#start
    --descr#flexfield#end
    --gl#accountff#start
    --gl#accountff#end
  from OE_ORDER_HEADERS           OH,
    OE_ORDER_LINES                OL,
    HZ_CUST_ACCOUNTS              HCA,
    HZ_PARTIES                    HZP,
    OE_TRANSACTION_TYPES_VL       OTT,
    RA_SALESREPS                  REP,
    MTL_PARAMETERS                MTP,
    HZ_CUST_SITE_USES             HZCS_SHIP_TO,
    HZ_CUST_ACCT_SITES            HCAS_SHIP_TO,
    HZ_PARTY_SITES                HZPS_SHIP_TO,
    HZ_LOCATIONS                  HZL_SHIP_TO,
  --  PER_PEOPLE_F                  PPF,
    FND_USER                      FU,
    FND_LOOKUP_VALUES_VL          FLV_SHIP_MTD,
    OE_LOOKUPS                    FLV_ORDER_STATUS,
    RA_TERMS_VL                   RT,
    MTL_SYSTEM_ITEMS_KFV          MSI
  --  FND_LOOKUP_VALUES_VL          FLV_LINE_STATUS
  WHERE ol.header_id                 = oh.header_id
  AND oh.sold_to_org_id              = hca.cust_account_id(+)
  AND hca.party_id                   = hzp.party_id(+)
  AND OH.ORDER_TYPE_ID               = OTT.TRANSACTION_TYPE_ID
  AND ol.ship_from_org_id            = mtp.organization_id(+)
  AND oh.ship_to_org_id              = hzcs_ship_to.site_use_id(+)
  AND hzcs_ship_to.cust_acct_site_id = hcas_ship_to.cust_acct_site_id(+)
  AND hcas_ship_to.party_site_id     = hzps_ship_to.party_site_id(+)
  AND hzl_ship_to.location_id(+)     = hzps_ship_to.location_id
  AND OH.SALESREP_ID                 = REP.SALESREP_ID(+)
  AND OH.ORG_ID                      = REP.ORG_ID(+)
  and OL.FLOW_STATUS_CODE not       in ('CLOSED','CANCELLED')
  --AND OH.CREATED_BY                  = PPF.PERSON_ID
  and FU.USER_ID                   =OH.CREATED_BY
 -- AND FU.EMPLOYEE_ID               =PPF.PERSON_ID(+)
  AND FLV_SHIP_MTD.LOOKUP_TYPE(+)  ='SHIP_METHOD'
  AND FLV_SHIP_MTD.LOOKUP_CODE(+)  = OL.SHIPPING_METHOD_CODE
  AND flv_order_status.lookup_type ='FLOW_STATUS'
  AND flv_order_status.lookup_code = Oh.flow_status_code
  AND oh.payment_term_id(+)        = rt.term_id
  AND ott.name                    IN('STANDARD ORDER','COUNTER ORDER','RETURN ORDER','REPAIR ORDER')
  AND ((ol.FLOW_STATUS_CODE        = 'ENTERED'
  AND TRUNC (oh.creation_date)     < TRUNC(sysdate))
  or OL.FLOW_STATUS_CODE          <> 'ENTERED' )
  --AND TRUNC (oh.creation_date) BETWEEN NVL (ppf.effective_start_date, TRUNC (oh.creation_date) ) AND NVL (ppf.effective_end_date, TRUNC (oh.creation_date) )
  AND OH.TRANSACTION_PHASE_CODE     <>'N'
  and MSI.ORGANIZATION_ID            = OL.SHIP_FROM_ORG_ID
  and MSI.INVENTORY_ITEM_ID          = OL.INVENTORY_ITEM_ID/
set scan on define on
prompt Creating View Data for Open Sales Orders Report
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_OM_OPEN_ORDERS_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_OM_OPEN_ORDERS_V',660,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Xxwc Om Open Orders V','EXOOOV','','');
--Delete View Columns for EIS_XXWC_OM_OPEN_ORDERS_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_OM_OPEN_ORDERS_V',660,FALSE);
--Inserting View Columns for EIS_XXWC_OM_OPEN_ORDERS_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','ORDER_LINE_STATUS',660,'Order Line Status','ORDER_LINE_STATUS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Order Line Status','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','ORDER_AMOUNT',660,'Order Amount','ORDER_AMOUNT','','','','XXEIS_RS_ADMIN','NUMBER','','','Order Amount','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','SCHEDULE_SHIP_DATE',660,'Schedule Ship Date','SCHEDULE_SHIP_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Schedule Ship Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','SALES_PERSON_NAME',660,'Sales Person Name','SALES_PERSON_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Sales Person Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','ZIP_CODE',660,'Zip Code','ZIP_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Zip Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','SHIP_TO_CITY',660,'Ship To City','SHIP_TO_CITY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Ship To City','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','CUSTOMER_JOB_NAME',660,'Customer Job Name','CUSTOMER_JOB_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Job Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','WAREHOUSE',660,'Warehouse','WAREHOUSE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Warehouse','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','ORDER_TYPE',660,'Order Type','ORDER_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Order Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','CUSTOMER_NAME',660,'Customer Name','CUSTOMER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','CUSTOMER_NUMBER',660,'Customer Number','CUSTOMER_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','SHIPPING_METHOD',660,'Shipping Method','SHIPPING_METHOD','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Shipping Method','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','ORDERED_DATE',660,'Ordered Date','ORDERED_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Ordered Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','ORDER_NUMBER',660,'Order Number','ORDER_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Order Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','CREATED_BY',660,'Created By','CREATED_BY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Created By','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','QUOTE_NUMBER',660,'Quote Number','QUOTE_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Quote Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','ORDER_HEADER_STATUS',660,'Order Header Status','ORDER_HEADER_STATUS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Order Header Status','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','PAYMENT_TERMS',660,'Payment Terms','PAYMENT_TERMS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Payment Terms','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','ITEM_DESCRIPTION',660,'Item Description','ITEM_DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Item Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','ITEM_NUMBER',660,'Item Number','ITEM_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Item Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','QTY',660,'Qty','QTY','','','','XXEIS_RS_ADMIN','NUMBER','','','Qty','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','REQUEST_DATE',660,'Request Date','REQUEST_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Request Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','HZPS_SHP_TO_PRT_ID',660,'Hzps Shp To Prt Id','HZPS_SHP_TO_PRT_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Hzps Shp To Prt Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','HZCS_SHIP_TO_STE_ID',660,'Hzcs Ship To Ste Id','HZCS_SHIP_TO_STE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Hzcs Ship To Ste Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','MTP_ORGANIZATION_ID',660,'Mtp Organization Id','MTP_ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Mtp Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','TRANSACTION_TYPE_ID',660,'Transaction Type Id','TRANSACTION_TYPE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Transaction Type Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','PARTY_ID',660,'Party Id','PARTY_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Party Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','SALESREP_ID',660,'Salesrep Id','SALESREP_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Salesrep Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','CUST_ACCT_SITE_ID',660,'Cust Acct Site Id','CUST_ACCT_SITE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Cust Acct Site Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','CUST_ACCOUNT_ID',660,'Cust Account Id','CUST_ACCOUNT_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Cust Account Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','ORDER_LINE_ID',660,'Order Line Id','ORDER_LINE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Order Line Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','ORDER_HEADER_ID',660,'Order Header Id','ORDER_HEADER_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Order Header Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','LINE_CREATION_DATE',660,'Line Creation Date','LINE_CREATION_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Line Creation Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','HEADER_STATUS',660,'Header Status','HEADER_STATUS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Header Status','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','USER_ITEM_DESCRIPTION',660,'User Item Description','USER_ITEM_DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','User Item Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','DELIVERY_DOC_DATE',660,'Delivery Doc Date','DELIVERY_DOC_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Delivery Doc Date','','','');
--Inserting View Components for EIS_XXWC_OM_OPEN_ORDERS_V
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_OPEN_ORDERS_V','HZ_CUST_ACCOUNTS',660,'HZ_CUST_ACCOUNTS','HCA','HCA','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Stores Information About Customer Accounts.','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_OPEN_ORDERS_V','HZ_PARTIES',660,'HZ_PARTIES','HZP','HZP','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Information About Parties Such As Organizations, P','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_OPEN_ORDERS_V','MTL_PARAMETERS',660,'MTL_PARAMETERS','MTP','MTP','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Inventory Control Options And Defaults','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_OPEN_ORDERS_V','HZ_CUST_ACCT_SITES',660,'HZ_CUST_ACCT_SITES_ALL','HCAS_SHIP_TO','HCAS_SHIP_TO','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Stores All Customer Account Sites Across All Opera','','','','');
--Inserting View Component Joins for EIS_XXWC_OM_OPEN_ORDERS_V
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_OPEN_ORDERS_V','HZ_CUST_ACCOUNTS','HCA',660,'EXOOOV.CUST_ACCOUNT_ID','=','HCA.CUST_ACCOUNT_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_OPEN_ORDERS_V','HZ_PARTIES','HZP',660,'EXOOOV.PARTY_ID','=','HZP.PARTY_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_OPEN_ORDERS_V','MTL_PARAMETERS','MTP',660,'EXOOOV.MTP_ORGANIZATION_ID','=','MTP.ORGANIZATION_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_OPEN_ORDERS_V','HZ_CUST_ACCT_SITES','HCAS_SHIP_TO',660,'EXOOOV.CUST_ACCT_SITE_ID','=','HCAS_SHIP_TO.CUST_ACCT_SITE_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
END;
/
set scan on define on
prompt Creating Report LOV Data for Open Sales Orders Report
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Open Sales Orders Report
xxeis.eis_rs_ins.lov( 660,'select  RS.Name,SALESREP_ID  from  RA_SALESREPS RS, HR_OPERATING_UNITS OU
WHERE RS.org_id = OU.organization_id
AND RS.NAME is not null','','OM SALES REP','This gives the sales representative name','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'select ott.name order_type,ott.description description,ott.transaction_type_id order_type_id from oe_transaction_types_vl ott','','OM ORDER TYPE','This gives the Order Type','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'select nvl( party.party_name,CUST_ACCT.account_name) customer_name,CUST_ACCT.ACCOUNT_NUMBER
from  HZ_PARTIES PARTY, HZ_CUST_ACCOUNTS CUST_ACCT where CUST_ACCT.PARTY_ID = PARTY.PARTY_ID AND  PARTY.status=''A''
','','OM CUSTOMER NAME','This gives the Customer Name','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'select  cust_acct.account_number Customer_Number,cust_acct.account_name customer_name,party.party_name
from  HZ_PARTIES PARTY, HZ_CUST_ACCOUNTS CUST_ACCT
 where CUST_ACCT.PARTY_ID = PARTY.PARTY_ID','','OM CUSTOMER NUMBER','This gives the Customer Number','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'select meaning ship_method,description from FND_LOOKUP_VALUES_vl where lookup_type=''SHIP_METHOD''','','OM SHIP METHOD','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'SELECT ppf.full_name  created_by, fu.user_name user_name
  FROM FND_USER fu, per_people_f ppf
 WHERE TRUNC(SYSDATE) BETWEEN fu.START_DATE AND NVL(fu.end_date, hr_general.end_of_time)
   and TRUNC(SYSDATE) BETWEEN ppf.effective_start_date AND ppf.effective_end_date
   and fu.employee_id = ppf.person_id (+)
   and exists
          (select 1
             from fnd_user_resp_groups furg,
                  fnd_responsibility_vl fr
            where furg.responsibility_id   = fr.responsibility_id
              and fu.user_id = furg.user_id
              and fr.responsibility_name like ''XXEIS%'')','','Created By Lov','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'Select  name, description from ra_terms_vl','','WC OM Payment Terms','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'SELECT DISTINCT LOCATION FROM hz_cust_site_uses','','OM Customer Job Name LOV','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'SELECT ORGANIZATION_CODE WAREHOUSE,ORGANIZATION_NAME FROM ORG_ORGANIZATION_DEFINITIONS OOD WHERE SYSDATE < NVL(OOD.DISABLE_DATE,SYSDATE+1) AND EXISTS (SELECT 1 FROM XXEIS.EIS_ORG_ACCESS_V  WHERE organization_id = ood.organization_id )
UNION
SELECT ''All'', ''All Organizations'' from Dual','','OM Warehouse All','','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'SELECT meaning Status 
  FROM OE_LOOKUPS lv
 WHERE lookup_type = ''LINE_FLOW_STATUS''
  AND lookup_code not in(''CLOSED'', ''CANCELLED'')','','WC Open Order Line Status','','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'SELECT meaning Status 
  FROM OE_LOOKUPS lv
 WHERE lookup_type = ''FLOW_STATUS''
   AND lookup_code not in(''CANCELLED'', ''CLOSED'')','','WC Open Order Header Status','','ANONYMOUS',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Open Sales Orders Report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Open Sales Orders Report
xxeis.eis_rs_utility.delete_report_rows( 'Open Sales Orders Report' );
--Inserting Report - Open Sales Orders Report
xxeis.eis_rs_ins.r( 660,'Open Sales Orders Report','','Open orders report by customer, by job, by salesperson, by created by, by shipping method, by promise date.','','','','XXEIS_RS_ADMIN','EIS_XXWC_OM_OPEN_ORDERS_V','Y','','','XXEIS_RS_ADMIN','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - Open Sales Orders Report
xxeis.eis_rs_ins.rc( 'Open Sales Orders Report',660,'CREATED_BY','Created By','Created By','','','default','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Open Sales Orders Report',660,'CUSTOMER_NUMBER','Customer Number','Customer Number','','','default','','17','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Open Sales Orders Report',660,'ORDERED_DATE','Ordered Date','Ordered Date','','','default','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Open Sales Orders Report',660,'ORDER_AMOUNT','Order Amount','Order Amount','','~T~D~2','default','','20','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Open Sales Orders Report',660,'ORDER_LINE_STATUS','Order Line Status','Order Line Status','','','default','','15','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Open Sales Orders Report',660,'ORDER_NUMBER','Order Number','Order Number','','~~~','default','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Open Sales Orders Report',660,'ORDER_TYPE','Order Type','Order Type','','','default','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Open Sales Orders Report',660,'SCHEDULE_SHIP_DATE','Schedule Ship Date','Schedule Ship Date','','','default','','24','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Open Sales Orders Report',660,'SHIPPING_METHOD','Shipping Method','Shipping Method','','','default','','21','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Open Sales Orders Report',660,'ZIP_CODE','Zip Code','Zip Code','','','default','','23','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Open Sales Orders Report',660,'WAREHOUSE','Warehouse','Warehouse','','','default','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Open Sales Orders Report',660,'SHIP_TO_CITY','Ship To City','Ship To City','','','default','','22','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Open Sales Orders Report',660,'CUSTOMER_JOB_NAME','Customer Job Name','Customer Job Name','','','default','','19','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Open Sales Orders Report',660,'CUSTOMER_NAME','Customer Name','Customer Name','','','default','','18','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Open Sales Orders Report',660,'SALES_PERSON_NAME','Sales Person Name','Sales Person Name','','','default','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Open Sales Orders Report',660,'QUOTE_NUMBER','Quote Number','Quote Number','','~~~','default','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Open Sales Orders Report',660,'ORDER_HEADER_STATUS','Order Header Status','Order Header Status','','','default','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Open Sales Orders Report',660,'PAYMENT_TERMS','Payment Terms','Payment Terms','','','default','','16','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Open Sales Orders Report',660,'ITEM_DESCRIPTION','Item Description','Item Description','','','default','','12','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Open Sales Orders Report',660,'ITEM_NUMBER','Item Number','Item Number','','','default','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Open Sales Orders Report',660,'QTY','Qty','Qty','','~,~.~0','default','','14','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Open Sales Orders Report',660,'REQUEST_DATE','Requested Date','Request Date','','','default','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Open Sales Orders Report',660,'USER_ITEM_DESCRIPTION','User Item Description','User Item Description','','','default','','13','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Open Sales Orders Report',660,'DELIVERY_DOC_DATE','Delivery Doc Date','Delivery Doc Date','','','','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','');
--Inserting Report Parameters - Open Sales Orders Report
xxeis.eis_rs_ins.rp( 'Open Sales Orders Report',660,'Created By','Created By','CREATED_BY','IN','Created By Lov','','VARCHAR2','N','Y','7','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Open Sales Orders Report',660,'Job Name','Job Name','CUSTOMER_JOB_NAME','IN','OM Customer Job Name LOV','','VARCHAR2','N','Y','5','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Open Sales Orders Report',660,'Customer Name','Customer Name','CUSTOMER_NAME','IN','OM CUSTOMER NAME','','VARCHAR2','N','Y','3','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Open Sales Orders Report',660,'Customer Number','Customer Number','CUSTOMER_NUMBER','IN','OM CUSTOMER NUMBER','','VARCHAR2','N','Y','4','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Open Sales Orders Report',660,'Ordered Date From','Ordered Date From','ORDERED_DATE','>=','','','DATE','N','Y','11','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','Start Date','');
xxeis.eis_rs_ins.rp( 'Open Sales Orders Report',660,'Order Type','Order Type','ORDER_TYPE','IN','OM ORDER TYPE','','VARCHAR2','N','Y','2','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Open Sales Orders Report',660,'Sales Person','Sales Person','SALES_PERSON_NAME','IN','OM SALES REP','','VARCHAR2','N','Y','6','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Open Sales Orders Report',660,'Schedule Ship Date From','Schedule Ship Date From','SCHEDULE_SHIP_DATE','>=','','','DATE','N','Y','9','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','Start Date','');
xxeis.eis_rs_ins.rp( 'Open Sales Orders Report',660,'Shipping Method','Shipping Method','SHIPPING_METHOD','IN','OM SHIP METHOD','','VARCHAR2','N','Y','8','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Open Sales Orders Report',660,'Warehouse','Warehouse','WAREHOUSE','IN','OM Warehouse All','','VARCHAR2','Y','Y','1','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Open Sales Orders Report',660,'Ordered Date To','Ordered Date To','ORDERED_DATE','<=','','','DATE','N','Y','12','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','End Date','');
xxeis.eis_rs_ins.rp( 'Open Sales Orders Report',660,'Schedule Ship Date To','Schedule Ship Date To','SCHEDULE_SHIP_DATE','<=','','','DATE','N','Y','10','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','End Date','');
xxeis.eis_rs_ins.rp( 'Open Sales Orders Report',660,'Order Line Status','Order Line Status','ORDER_LINE_STATUS','IN','WC Open Order Line Status','','VARCHAR2','N','Y','13','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Open Sales Orders Report',660,'Order Header Status','Order Header Status','ORDER_HEADER_STATUS','IN','WC Open Order Header Status','','VARCHAR2','N','Y','14','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Open Sales Orders Report',660,'Payment Terms','Payment Terms','PAYMENT_TERMS','IN','WC OM Payment Terms','','VARCHAR2','N','Y','15','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
--Inserting Report Conditions - Open Sales Orders Report
xxeis.eis_rs_ins.rcn( 'Open Sales Orders Report',660,'CREATED_BY','IN',':Created By','','','Y','7','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Open Sales Orders Report',660,'CUSTOMER_NAME','IN',':Customer Name','','','Y','3','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Open Sales Orders Report',660,'CUSTOMER_NUMBER','IN',':Customer Number','','','Y','4','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Open Sales Orders Report',660,'ORDER_TYPE','IN',':Order Type','','','Y','2','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Open Sales Orders Report',660,'SHIPPING_METHOD','IN',':Shipping Method','','','Y','8','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Open Sales Orders Report',660,'WAREHOUSE','IN',':Warehouse','','','N','6','N','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Open Sales Orders Report',660,'TRUNC(ORDERED_DATE)','>=',':Ordered Date From','','','Y','8','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Open Sales Orders Report',660,'TRUNC(SCHEDULE_SHIP_DATE)','>=',':Schedule Ship Date From','','','Y','10','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Open Sales Orders Report',660,'TRUNC(ORDERED_DATE)','<=',':Ordered Date To','','','Y','11','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Open Sales Orders Report',660,'TRUNC(SCHEDULE_SHIP_DATE)','<=',':Schedule Ship Date To','','','Y','13','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Open Sales Orders Report',660,'CUSTOMER_JOB_NAME','IN',':Job Name','','','Y','5','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Open Sales Orders Report',660,'SALES_PERSON_NAME','IN',':Sales Person','','','Y','6','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Open Sales Orders Report',660,'ORDER_LINE_STATUS','IN',':Order Line Status','','','Y','13','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Open Sales Orders Report',660,'ORDER_HEADER_STATUS','IN',':Order Header Status','','','Y','14','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Open Sales Orders Report',660,'PAYMENT_TERMS','IN',':Payment Terms','','','Y','15','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Open Sales Orders Report',660,'','','','','AND ( ''All'' IN (:Warehouse) OR (WAREHOUSE IN (:Warehouse)))','Y','0','','XXEIS_RS_ADMIN');
--Inserting Report Sorts - Open Sales Orders Report
xxeis.eis_rs_ins.rs( 'Open Sales Orders Report',660,'WAREHOUSE','ASC','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rs( 'Open Sales Orders Report',660,'ORDER_NUMBER','ASC','XXEIS_RS_ADMIN','','');
--Inserting Report Triggers - Open Sales Orders Report
--Inserting Report Templates - Open Sales Orders Report
--Inserting Report Portals - Open Sales Orders Report
--Inserting Report Dashboards - Open Sales Orders Report
--Inserting Report Security - Open Sales Orders Report
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','660','','50926',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','660','','50927',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','660','','50928',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','660','','50929',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','660','','50931',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','660','','50930',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','660','','21623',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','701','','50546',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','660','','50856',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','660','','50857',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','660','','50858',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','660','','50859',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','660','','50860',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','660','','50861',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','20005','','50880',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','','LC053655','',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','','10010432','',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','','RB054040','',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','','RV003897','',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','','SS084202','',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','','SO004816','',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','660','','50886',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','660','','50901',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','660','','50870',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','660','','50871',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','660','','50869',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','20005','','50900',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','660','','51044',660,'XXEIS_RS_ADMIN','','');
--Inserting Report Pivots - Open Sales Orders Report
xxeis.eis_rs_ins.rpivot( 'Open Sales Orders Report',660,'Pivot','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rs_ins.rpivot_dtls( 'Open Sales Orders Report',660,'Pivot','ORDER_AMOUNT','DATA_FIELD','SUM','','1','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Open Sales Orders Report',660,'Pivot','CREATED_BY','ROW_FIELD','','','1','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Open Sales Orders Report',660,'Pivot','CUSTOMER_NAME','ROW_FIELD','','','2','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Open Sales Orders Report',660,'Pivot','ORDER_NUMBER','ROW_FIELD','','','3','','');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
END;
/
set scan on define on
