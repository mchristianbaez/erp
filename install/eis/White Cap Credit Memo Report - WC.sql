--Report Name            : White Cap Credit Memo Report - WC
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating View Data for White Cap Credit Memo Report - WC
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_AR_CRDT_MEMO_DTLS_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_AR_CRDT_MEMO_DTLS_V',222,'','','','','MR020532','XXEIS','Eis Xxwc Ar Credit Memo Dtls V','EXACMDV','','');
--Delete View Columns for EIS_XXWC_AR_CRDT_MEMO_DTLS_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_AR_CRDT_MEMO_DTLS_V',222,FALSE);
--Inserting View Columns for EIS_XXWC_AR_CRDT_MEMO_DTLS_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CRDT_MEMO_DTLS_V','MASTER_NAME',222,'Master Name','MASTER_NAME','','','','MR020532','VARCHAR2','','','Master Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CRDT_MEMO_DTLS_V','INVOICE_LINE_AMOUNT',222,'Invoice Line Amount','INVOICE_LINE_AMOUNT','','','','MR020532','NUMBER','','','Invoice Line Amount','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CRDT_MEMO_DTLS_V','CREDIT_REASON_SALE',222,'Credit Reason Sale','CREDIT_REASON_SALE','','','','MR020532','VARCHAR2','','','Credit Reason Sale','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CRDT_MEMO_DTLS_V','ORIGINAL_INVOICE_DATE',222,'Original Invoice Date','ORIGINAL_INVOICE_DATE','','','','MR020532','DATE','','','Original Invoice Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CRDT_MEMO_DTLS_V','ORIGINAL_INVOICE_NUMBER',222,'Original Invoice Number','ORIGINAL_INVOICE_NUMBER','','','','MR020532','VARCHAR2','','','Original Invoice Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CRDT_MEMO_DTLS_V','ORDER_NUMBER',222,'Order Number','ORDER_NUMBER','','','','MR020532','NUMBER','','','Order Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CRDT_MEMO_DTLS_V','INVOICE_NUMBER',222,'Invoice Number','INVOICE_NUMBER','','','','MR020532','VARCHAR2','','','Invoice Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CRDT_MEMO_DTLS_V','LOCATION',222,'Location','LOCATION','','','','MR020532','VARCHAR2','','','Location','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CRDT_MEMO_DTLS_V','CUSTOMER_NUMBER',222,'Customer Number','CUSTOMER_NUMBER','','','','MR020532','VARCHAR2','','','Customer Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CRDT_MEMO_DTLS_V','FISCAL_MONTH',222,'Fiscal Month','FISCAL_MONTH','','','','MR020532','VARCHAR2','','','Fiscal Month','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CRDT_MEMO_DTLS_V','FISCAL_YEAR',222,'Fiscal Year','FISCAL_YEAR','','','','MR020532','NUMBER','','','Fiscal Year','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CRDT_MEMO_DTLS_V','ORIGIN_DATE',222,'Origin Date','ORIGIN_DATE','','','','MR020532','DATE','','','Origin Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CRDT_MEMO_DTLS_V','INVOICE_TYPE',222,'Invoice Type','INVOICE_TYPE','','','','MR020532','VARCHAR2','','','Invoice Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CRDT_MEMO_DTLS_V','LINE_TYPE',222,'Line Type','LINE_TYPE','','','','MR020532','VARCHAR2','','','Line Type','','','');
--Inserting View Components for EIS_XXWC_AR_CRDT_MEMO_DTLS_V
--Inserting View Component Joins for EIS_XXWC_AR_CRDT_MEMO_DTLS_V
END;
/
set scan on define on
prompt Creating Report LOV Data for White Cap Credit Memo Report - WC
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - White Cap Credit Memo Report - WC
xxeis.eis_rs_ins.lov( 222,'SELECT organization_code,organization_id,organization_name FROM org_organization_definitions','','AR Organizaion Code LOV','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 222,'SELECT DISTINCT GPS.PERIOD_NAME
FROM      HR_OPERATING_UNITS          HOU,
          GL_LEDGERS                  GL,
          gl_period_statuses          gps
WHERE
 GL.LEDGER_ID                      = HOU.SET_OF_BOOKS_ID
AND GL.LEDGER_ID                      = GPS.SET_OF_BOOKS_ID
AND GL.ACCOUNTED_PERIOD_TYPE          = GPS.PERIOD_TYPE
order by GPS.PERIOD_NAME','','Fiscal Month','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 222,'SELECT DISTINCT GPS.PERIOD_YEAR
FROM      HR_OPERATING_UNITS          HOU,
          GL_LEDGERS                  GL,
          gl_period_statuses          gps
WHERE
 GL.LEDGER_ID                      = HOU.SET_OF_BOOKS_ID
AND GL.LEDGER_ID                      = GPS.SET_OF_BOOKS_ID
AND GL.ACCOUNTED_PERIOD_TYPE          = GPS.PERIOD_TYPE
order by GPS.PERIOD_YEAR
','','Fiscal Year','','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 222,'select distinct apply_date
    from ar_receivable_applications
    where application_type=''CM''','','Origin Date','','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 222,'select distinct apply_date
    from ar_receivable_applications
    where application_type=''CM''','','Origin Date','','ANONYMOUS',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for White Cap Credit Memo Report - WC
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - White Cap Credit Memo Report - WC
xxeis.eis_rs_utility.delete_report_rows( 'White Cap Credit Memo Report - WC' );
--Inserting Report - White Cap Credit Memo Report - WC
xxeis.eis_rs_ins.r( 222,'White Cap Credit Memo Report - WC','','The purpose of this report is to provide White Cap District Managers with data regarding customer credit activities for their approvals on Credit Memos.  The report serves to provide SOX control/compliance.','','','','MR020532','EIS_XXWC_AR_CRDT_MEMO_DTLS_V','Y','','','MR020532','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - White Cap Credit Memo Report - WC
xxeis.eis_rs_ins.rc( 'White Cap Credit Memo Report - WC',222,'CREDIT_REASON_SALE','Credit Reason Sale','Credit Reason Sale','','','default','','27','N','','','','','','','','MR020532','N','N','','EIS_XXWC_AR_CRDT_MEMO_DTLS_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Credit Memo Report - WC',222,'CUSTOMER_NUMBER','Customer Number','Customer Number','','','default','','3','N','','','','','','','','MR020532','N','N','','EIS_XXWC_AR_CRDT_MEMO_DTLS_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Credit Memo Report - WC',222,'FISCAL_MONTH','Fiscal Month','Fiscal Month','','','default','','2','N','','','','','','','','MR020532','N','N','','EIS_XXWC_AR_CRDT_MEMO_DTLS_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Credit Memo Report - WC',222,'FISCAL_YEAR','Fiscal Year','Fiscal Year','','~~~','default','','1','N','','','','','','','','MR020532','N','N','','EIS_XXWC_AR_CRDT_MEMO_DTLS_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Credit Memo Report - WC',222,'INVOICE_LINE_AMOUNT','Invoice Line Amount','Invoice Line Amount','','~T~D~2','default','','28','N','','','','','','','','MR020532','N','N','','EIS_XXWC_AR_CRDT_MEMO_DTLS_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Credit Memo Report - WC',222,'INVOICE_NUMBER','Invoice Number','Invoice Number','','','default','','18','N','','','','','','','','MR020532','N','N','','EIS_XXWC_AR_CRDT_MEMO_DTLS_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Credit Memo Report - WC',222,'MASTER_NAME','Master Name','Master Name','','','default','','33','N','','','','','','','','MR020532','N','N','','EIS_XXWC_AR_CRDT_MEMO_DTLS_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Credit Memo Report - WC',222,'ORDER_NUMBER','Order Number','Order Number','','~~~','default','','19','N','','','','','','','','MR020532','N','N','','EIS_XXWC_AR_CRDT_MEMO_DTLS_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Credit Memo Report - WC',222,'ORIGINAL_INVOICE_DATE','Original Invoice Date','Original Invoice Date','','','default','','25','N','','','','','','','','MR020532','N','N','','EIS_XXWC_AR_CRDT_MEMO_DTLS_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Credit Memo Report - WC',222,'ORIGINAL_INVOICE_NUMBER','Original Invoice Number','Original Invoice Number','','','default','','24','N','','','','','','','','MR020532','N','N','','EIS_XXWC_AR_CRDT_MEMO_DTLS_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Credit Memo Report - WC',222,'ORIGIN_DATE','Origin Date','Origin Date','','','default','','4','N','','','','','','','','MR020532','N','N','','EIS_XXWC_AR_CRDT_MEMO_DTLS_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Credit Memo Report - WC',222,'INVOICE_TYPE','Invoice Type','Invoice Type','','','default','','6','N','','','','','','','','MR020532','N','N','','EIS_XXWC_AR_CRDT_MEMO_DTLS_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Credit Memo Report - WC',222,'LINE_TYPE','Line Type','Line Type','','','default','','23','N','','','','','','','','MR020532','N','N','','EIS_XXWC_AR_CRDT_MEMO_DTLS_V','','');
--Inserting Report Parameters - White Cap Credit Memo Report - WC
xxeis.eis_rs_ins.rp( 'White Cap Credit Memo Report - WC',222,'Fiscal Year','Fiscal Year','FISCAL_YEAR','IN','Fiscal Year','SELECT distinct period_year FROM GL_PERIOD_STATUSES WHERE  APPLICATION_ID=101 AND CLOSING_STATUS=''O'' and sysdate between start_date and end_date','VARCHAR2','Y','Y','1','','Y','SQL','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'White Cap Credit Memo Report - WC',222,'Fiscal Month','Fiscal Month','FISCAL_MONTH','IN','Fiscal Month','SELECT distinct period_name FROM GL_PERIOD_STATUSES WHERE  APPLICATION_ID=101 AND CLOSING_STATUS=''O'' and sysdate between start_date and end_date','VARCHAR2','N','Y','2','','Y','SQL','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'White Cap Credit Memo Report - WC',222,'Location','Location','LOCATION','IN','AR Organizaion Code LOV','','VARCHAR2','N','Y','3','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'White Cap Credit Memo Report - WC',222,'Origin Date From','Origin Date From','ORIGIN_DATE','>=','Origin Date','','VARCHAR2','N','Y','4','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'White Cap Credit Memo Report - WC',222,'Origin Date To','Origin Date To','ORIGIN_DATE','<=','Origin Date','','VARCHAR2','N','Y','5','','Y','CONSTANT','MR020532','Y','N','','','');
--Inserting Report Conditions - White Cap Credit Memo Report - WC
xxeis.eis_rs_ins.rcn( 'White Cap Credit Memo Report - WC',222,'FISCAL_YEAR','IN',':Fiscal Year','','','Y','1','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'White Cap Credit Memo Report - WC',222,'FISCAL_MONTH','IN',':Fiscal Month','','','Y','2','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'White Cap Credit Memo Report - WC',222,'LOCATION','IN',':Location','','','Y','3','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'White Cap Credit Memo Report - WC',222,'ORIGIN_DATE','>=',':Origin Date From','','','Y','4','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'White Cap Credit Memo Report - WC',222,'ORIGIN_DATE','<=',':Origin Date To','','','Y','5','Y','MR020532');
--Inserting Report Sorts - White Cap Credit Memo Report - WC
--Inserting Report Triggers - White Cap Credit Memo Report - WC
--Inserting Report Templates - White Cap Credit Memo Report - WC
--Inserting Report Portals - White Cap Credit Memo Report - WC
--Inserting Report Dashboards - White Cap Credit Memo Report - WC
--Inserting Report Security - White Cap Credit Memo Report - WC
--Inserting Report Pivots - White Cap Credit Memo Report - WC
END;
/
set scan on define on
