--Report Name            : Product Recall Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data EIS_XXWC_OM_PRODUCT_RECALL_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_OM_PRODUCT_RECALL_V
xxeis.eis_rsc_ins.v( 'EIS_XXWC_OM_PRODUCT_RECALL_V',660,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Xxwc Om Product Recall V','EXOPRV1','','','VIEW','US','','');
--Delete Object Columns for EIS_XXWC_OM_PRODUCT_RECALL_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_OM_PRODUCT_RECALL_V',660,FALSE);
--Inserting Object Columns for EIS_XXWC_OM_PRODUCT_RECALL_V
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PRODUCT_RECALL_V','VENDOR_NUMBER',660,'Vendor Number','VENDOR_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PRODUCT_RECALL_V','VENDOR_NAME',660,'Vendor Name','VENDOR_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PRODUCT_RECALL_V','SALES_ORDER_NUMBER',660,'Sales Order Number','SALES_ORDER_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Sales Order Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PRODUCT_RECALL_V','CUSTOMER_SOLD_TO',660,'Customer Sold To','CUSTOMER_SOLD_TO','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Sold To','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PRODUCT_RECALL_V','ONHAND_LOCATION',660,'Onhand Location','ONHAND_LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Onhand Location','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PRODUCT_RECALL_V','SOLD_FROM_WAREHOUSE',660,'Sold From Warehouse','SOLD_FROM_WAREHOUSE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Sold From Warehouse','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PRODUCT_RECALL_V','ORDERED_DATE',660,'Ordered Date','ORDERED_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Ordered Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PRODUCT_RECALL_V','ITEM_DESCRIPTION',660,'Item Description','ITEM_DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Item Description','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PRODUCT_RECALL_V','ITEM_NUMBER',660,'Item Number','ITEM_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Item Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PRODUCT_RECALL_V','LOT_NUMBER',660,'Lot Number','LOT_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Lot Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PRODUCT_RECALL_V','SERIAL_NUMBER',660,'Serial Number','SERIAL_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Serial Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PRODUCT_RECALL_V','LINE_NUMBER',660,'Line Number','LINE_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Line Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PRODUCT_RECALL_V','ONHAND_QTY',660,'Onhand Qty','ONHAND_QTY','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Onhand Qty','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PRODUCT_RECALL_V','ORDERED_QTY',660,'Ordered Qty','ORDERED_QTY','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Ordered Qty','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PRODUCT_RECALL_V','DELIVERY_DETAIL_ID',660,'Delivery Detail Id','DELIVERY_DETAIL_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Delivery Detail Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PRODUCT_RECALL_V','HEADER_ID',660,'Header Id','HEADER_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Header Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PRODUCT_RECALL_V','INVENTORY_ITEM_ID',660,'Inventory Item Id','INVENTORY_ITEM_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Inventory Item Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PRODUCT_RECALL_V','LINE_ID',660,'Line Id','LINE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Line Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PRODUCT_RECALL_V','MSI_ORGANIZATION_ID',660,'Msi Organization Id','MSI_ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Msi Organization Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PRODUCT_RECALL_V','ORGANIZATION_ID',660,'Organization Id','ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Organization Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PRODUCT_RECALL_V','PARTY_ID',660,'Party Id','PARTY_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Party Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PRODUCT_RECALL_V','LINE_SHIPMENT',660,'Line Shipment','LINE_SHIPMENT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Line Shipment','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PRODUCT_RECALL_V','SOLD_ONHAND',660,'Sold Onhand','SOLD_ONHAND','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Sold Onhand','','','','');
--Inserting Object Components for EIS_XXWC_OM_PRODUCT_RECALL_V
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_OM_PRODUCT_RECALL_V','OE_ORDER_HEADERS',660,'OE_ORDER_HEADERS_ALL','OH','OH','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Oe Order Headers All Stores Header Information For','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_OM_PRODUCT_RECALL_V','OE_ORDER_LINES',660,'OE_ORDER_LINES_ALL','OL','OL','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Oe Order Lines All Stores Information For All Orde','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_OM_PRODUCT_RECALL_V','HZ_PARTIES',660,'HZ_PARTIES','HZP','HZP','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Information About Parties Such As Organizations, P','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_OM_PRODUCT_RECALL_V','MTL_PARAMETERS',660,'MTL_PARAMETERS','MTP','MTP','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Inventory Control Options And Defaults','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_OM_PRODUCT_RECALL_V','MTL_SYSTEM_ITEMS_KFV',660,'MTL_SYSTEM_ITEMS_B','MSI','MSI','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Inventory Item Definitions','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_OM_PRODUCT_RECALL_V','WSH_DELIVERY_DETAILS',660,'WSH_DELIVERY_DETAILS','WDD','WDD','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Delivery Details','','','','','WDD','',' ',' ',' ',' ',' ');
--Inserting Object Component Joins for EIS_XXWC_OM_PRODUCT_RECALL_V
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_OM_PRODUCT_RECALL_V','OE_ORDER_HEADERS','OH',660,'EXOPRV1.HEADER_ID','=','OH.HEADER_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_OM_PRODUCT_RECALL_V','OE_ORDER_LINES','OL',660,'EXOPRV1.LINE_ID','=','OL.LINE_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_OM_PRODUCT_RECALL_V','HZ_PARTIES','HZP',660,'EXOPRV1.PARTY_ID','=','HZP.PARTY_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_OM_PRODUCT_RECALL_V','MTL_PARAMETERS','MTP',660,'EXOPRV1.ORGANIZATION_ID','=','MTP.ORGANIZATION_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_OM_PRODUCT_RECALL_V','MTL_SYSTEM_ITEMS_KFV','MSI',660,'EXOPRV1.INVENTORY_ITEM_ID','=','MSI.INVENTORY_ITEM_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_OM_PRODUCT_RECALL_V','MTL_SYSTEM_ITEMS_KFV','MSI',660,'EXOPRV1.MSI_ORGANIZATION_ID','=','MSI.ORGANIZATION_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_OM_PRODUCT_RECALL_V','WSH_DELIVERY_DETAILS','WDD',660,'EXOPRV1.DELIVERY_DETAIL_ID','=','WDD.DELIVERY_DETAIL_ID(+)','','','','Y','XXEIS_RS_ADMIN');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
prompt Creating Report LOV Data for Product Recall Report
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - Product Recall Report
xxeis.eis_rsc_ins.lov( 660,'SELECT DISTINCT LOT_NUMBER,HOU.NAME ORGANIZATION_NAME from mtl_lot_numbers ml ,hr_all_organization_units hou WHERE ML.ORGANIZATION_ID=HOU.ORGANIZATION_ID AND exists (Select 1 from XXEIS.EIS_ORG_ACCESS_V where organization_id = ml.organization_id)
','','OM LOT NUMBER','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'SELECT serial_number,hou.organization_name current_organization FROM mtl_serial_numbers msn,org_organization_definitions hou WHERE HOU.ORGANIZATION_ID=MSN.CURRENT_ORGANIZATION_ID  and exists (Select 1 from XXEIS.EIS_ORG_ACCESS_V where organization_id = hou.organization_id)
','','OM SERIAL NUMBER','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'SELECT ORGANIZATION_CODE WAREHOUSE,ORGANIZATION_NAME FROM ORG_ORGANIZATION_DEFINITIONS OOD WHERE SYSDATE < NVL(OOD.DISABLE_DATE,SYSDATE+1) AND EXISTS (SELECT 1 FROM XXEIS.EIS_ORG_ACCESS_V  WHERE organization_id = ood.organization_id )','','OM WAREHOUSE','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'SELECT DISTINCT VENDOR_NAME
    FROM PO_VENDORS POV','','OM Vendor Name LOV','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'SELECT DISTINCT MSI.SEGMENT1   ITEM_NUMBER,
MSI.DESCRIPTION  ITEM_DESCRIPTION from mtl_system_items_b msi','','OM Item Number LOV','Order Item numbers','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
prompt Creating Report Data for Product Recall Report
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Product Recall Report
xxeis.eis_rsc_utility.delete_report_rows( 'Product Recall Report' );
--Inserting Report - Product Recall Report
xxeis.eis_rsc_ins.r( 660,'Product Recall Report','','Provide visibility to products (generally tools) on-hand and sold to customers that have been Recalled by the Vendor.','','','','MR020532','EIS_XXWC_OM_PRODUCT_RECALL_V','Y','','','MR020532','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N','','','','','','','','US','','','','');
--Inserting Report Columns - Product Recall Report
xxeis.eis_rsc_ins.rc( 'Product Recall Report',660,'CUSTOMER_SOLD_TO','Customer Sold To','Customer Sold To','','','','','8','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_PRODUCT_RECALL_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Product Recall Report',660,'ITEM_DESCRIPTION','Item Description','Item Description','','','','','4','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_PRODUCT_RECALL_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Product Recall Report',660,'ITEM_NUMBER','Item Number','Item Number','','','','','3','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_PRODUCT_RECALL_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Product Recall Report',660,'ONHAND_LOCATION','Onhand Location','Onhand Location','','','','','7','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_PRODUCT_RECALL_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Product Recall Report',660,'SALES_ORDER_NUMBER','Sales Order Number','Sales Order Number','','','','','9','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_PRODUCT_RECALL_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Product Recall Report',660,'SOLD_FROM_WAREHOUSE','Sold From Warehouse','Sold From Warehouse','','','','','12','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_PRODUCT_RECALL_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Product Recall Report',660,'VENDOR_NAME','Vendor Name','Vendor Name','','','','','2','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_PRODUCT_RECALL_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Product Recall Report',660,'VENDOR_NUMBER','Vendor Number','Vendor Number','','','','','1','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_PRODUCT_RECALL_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Product Recall Report',660,'LOT_NUMBER','Lot Number','Lot Number','','','','','6','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_PRODUCT_RECALL_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Product Recall Report',660,'ORDERED_DATE','Sale Date','Ordered Date','','','','','11','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_PRODUCT_RECALL_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Product Recall Report',660,'LINE_NUMBER','Line Number','Line Number','','','','','10','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_PRODUCT_RECALL_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Product Recall Report',660,'ONHAND_QTY','Onhand Qty','Onhand Qty','','','','','13','','Y','','','','','','','MR020532','N','N','','EIS_XXWC_OM_PRODUCT_RECALL_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Product Recall Report',660,'ORDERED_QTY','Ordered Qty','Ordered Qty','','','','','14','','Y','','','','','','','MR020532','N','N','','EIS_XXWC_OM_PRODUCT_RECALL_V','','','SUM','US','');
--Inserting Report Parameters - Product Recall Report
xxeis.eis_rsc_ins.rp( 'Product Recall Report',660,'Item Number','Item Number','ITEM_NUMBER','IN','OM Item Number LOV','','VARCHAR2','N','Y','2','Y','Y','CONSTANT','MR020532','Y','N','','','','EIS_XXWC_OM_PRODUCT_RECALL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Product Recall Report',660,'Vendor Name','Vendor Name','VENDOR_NAME','IN','OM Vendor Name LOV','','VARCHAR2','N','Y','1','Y','Y','CONSTANT','MR020532','Y','N','','','','EIS_XXWC_OM_PRODUCT_RECALL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Product Recall Report',660,'Lot Number From','Lot Number From','LOT_NUMBER','>=','OM LOT NUMBER','','VARCHAR2','N','Y','7','Y','Y','CONSTANT','MR020532','Y','N','','','','EIS_XXWC_OM_PRODUCT_RECALL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Product Recall Report',660,'Serial Number From','Serial Number From','SERIAL_NUMBER','>=','OM SERIAL NUMBER','','VARCHAR2','N','Y','5','Y','Y','CONSTANT','MR020532','Y','N','','','','EIS_XXWC_OM_PRODUCT_RECALL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Product Recall Report',660,'Sold Date From','Sold Date From','ORDERED_DATE','>=','','','DATE','N','Y','3','N','N','CONSTANT','MR020532','Y','N','','Start Date','','EIS_XXWC_OM_PRODUCT_RECALL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Product Recall Report',660,'Sold Date To','Sold Date To','ORDERED_DATE','<=','','','DATE','N','Y','4','N','N','CONSTANT','MR020532','Y','N','','End Date','','EIS_XXWC_OM_PRODUCT_RECALL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Product Recall Report',660,'Serial Number To','Serial Number To','SERIAL_NUMBER','<=','OM SERIAL NUMBER','','VARCHAR2','N','Y','6','Y','Y','CONSTANT','MR020532','Y','N','','','','EIS_XXWC_OM_PRODUCT_RECALL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Product Recall Report',660,'Lot Number To','Lot Number To','LOT_NUMBER','<=','OM LOT NUMBER','','VARCHAR2','N','Y','8','Y','Y','CONSTANT','MR020532','Y','N','','','','EIS_XXWC_OM_PRODUCT_RECALL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Product Recall Report',660,'Branch From','Branch From','SOLD_FROM_WAREHOUSE','>=','OM WAREHOUSE','','VARCHAR2','N','Y','9','Y','Y','CONSTANT','MR020532','Y','N','','','','EIS_XXWC_OM_PRODUCT_RECALL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Product Recall Report',660,'Branch To','Branch To','SOLD_FROM_WAREHOUSE','<=','OM WAREHOUSE','','VARCHAR2','N','Y','10','Y','Y','CONSTANT','MR020532','Y','N','','','','EIS_XXWC_OM_PRODUCT_RECALL_V','','','US','');
--Inserting Dependent Parameters - Product Recall Report
--Inserting Report Conditions - Product Recall Report
xxeis.eis_rsc_ins.rcnh( 'Product Recall Report',660,'EXOPRV1.VENDOR_NAME IN Vendor Name','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','VENDOR_NAME','','Vendor Name','','','','','EIS_XXWC_OM_PRODUCT_RECALL_V','','','','','','IN','Y','Y','','','','','1',660,'Product Recall Report','EXOPRV1.VENDOR_NAME IN Vendor Name');
xxeis.eis_rsc_ins.rcnh( 'Product Recall Report',660,'EXOPRV1.ITEM_NUMBER IN Item Number','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','ITEM_NUMBER','','Item Number','','','','','EIS_XXWC_OM_PRODUCT_RECALL_V','','','','','','IN','Y','Y','','','','','1',660,'Product Recall Report','EXOPRV1.ITEM_NUMBER IN Item Number');
xxeis.eis_rsc_ins.rcnh( 'Product Recall Report',660,'EXOPRV1.SERIAL_NUMBER >= Serial Number From','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','SERIAL_NUMBER','','Serial Number From','','','','','EIS_XXWC_OM_PRODUCT_RECALL_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',660,'Product Recall Report','EXOPRV1.SERIAL_NUMBER >= Serial Number From');
xxeis.eis_rsc_ins.rcnh( 'Product Recall Report',660,'EXOPRV1.SERIAL_NUMBER <= Serial Number To','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','SERIAL_NUMBER','','Serial Number To','','','','','EIS_XXWC_OM_PRODUCT_RECALL_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',660,'Product Recall Report','EXOPRV1.SERIAL_NUMBER <= Serial Number To');
xxeis.eis_rsc_ins.rcnh( 'Product Recall Report',660,'EXOPRV1.LOT_NUMBER >= Lot Number From','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','LOT_NUMBER','','Lot Number From','','','','','EIS_XXWC_OM_PRODUCT_RECALL_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',660,'Product Recall Report','EXOPRV1.LOT_NUMBER >= Lot Number From');
xxeis.eis_rsc_ins.rcnh( 'Product Recall Report',660,'EXOPRV1.LOT_NUMBER <= Lot Number To','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','LOT_NUMBER','','Lot Number To','','','','','EIS_XXWC_OM_PRODUCT_RECALL_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',660,'Product Recall Report','EXOPRV1.LOT_NUMBER <= Lot Number To');
xxeis.eis_rsc_ins.rcnh( 'Product Recall Report',660,'EXOPRV1.SOLD_FROM_WAREHOUSE >= Branch From','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','SOLD_FROM_WAREHOUSE','','Branch From','','','','','EIS_XXWC_OM_PRODUCT_RECALL_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',660,'Product Recall Report','EXOPRV1.SOLD_FROM_WAREHOUSE >= Branch From');
xxeis.eis_rsc_ins.rcnh( 'Product Recall Report',660,'EXOPRV1.SOLD_FROM_WAREHOUSE <= Branch To','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','SOLD_FROM_WAREHOUSE','','Branch To','','','','','EIS_XXWC_OM_PRODUCT_RECALL_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',660,'Product Recall Report','EXOPRV1.SOLD_FROM_WAREHOUSE <= Branch To');
--Inserting Report Sorts - Product Recall Report
xxeis.eis_rsc_ins.rs( 'Product Recall Report',660,'VENDOR_NAME','ASC','MR020532','1','');
xxeis.eis_rsc_ins.rs( 'Product Recall Report',660,'ITEM_NUMBER','ASC','MR020532','2','');
xxeis.eis_rsc_ins.rs( 'Product Recall Report',660,'ONHAND_LOCATION','ASC','MR020532','3','');
--Inserting Report Triggers - Product Recall Report
xxeis.eis_rsc_ins.rt( 'Product Recall Report',660,'Begin
xxeis.eis_rs_xxwc_com_util_pkg.g_date_from := :Sold Date From ;
xxeis.eis_rs_xxwc_com_util_pkg.g_date_to := :Sold Date To ;
END;','B','Y','MR020532','AQ');
--inserting report templates - Product Recall Report
--Inserting Report Portals - Product Recall Report
--inserting report dashboards - Product Recall Report
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Product Recall Report','660','EIS_XXWC_OM_PRODUCT_RECALL_V','EIS_XXWC_OM_PRODUCT_RECALL_V','N','');
--inserting report security - Product Recall Report
xxeis.eis_rsc_ins.rsec( 'Product Recall Report','660','','',660,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Product Recall Report','660','','',660,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Product Recall Report','660','','',660,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Product Recall Report','660','','',660,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Product Recall Report','660','','',660,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Product Recall Report','660','','',660,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Product Recall Report','660','','ORDER_MGMT_SUPER_USER',660,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Product Recall Report','701','','CLN_OM_3A6_ADMINISTRATOR',660,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Product Recall Report','660','','',660,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Product Recall Report','660','','XXWC_ORDER_MGMT_PRICING_FULL',660,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Product Recall Report','660','','XXWC_ORDER_MGMT_PRICING_LTD',660,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Product Recall Report','660','','XXWC_ORDER_MGMT_PRICING_STD',660,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Product Recall Report','660','','XXWC_ORDER_MGMT_READ_ONLY',660,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Product Recall Report','660','','',660,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Product Recall Report','20005','','',660,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Product Recall Report','','LC053655','',660,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Product Recall Report','','10010432','',660,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Product Recall Report','','RB054040','',660,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Product Recall Report','','RV003897','',660,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Product Recall Report','','SS084202','',660,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Product Recall Report','','SO004816','',660,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Product Recall Report','660','','XXWC_ORDER_MGMT_PRICING_SUPER',660,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Product Recall Report','660','','XXWC_ORDER_MGMT_READ_SHIPPING',660,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Product Recall Report','660','','XXWC_AO_OEENTRY_PO_RPT',660,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Product Recall Report','660','','XXWC_AO_OEENTRY_REC',660,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Product Recall Report','660','','XXWC_AO_OEENTRY',660,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Product Recall Report','20005','','XXWC_VIEW_ALL_EIS_REPORTS',660,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Product Recall Report','660','','XXWC_RENTAL_OM_PRICING_SUPER',660,'MR020532','','','');
--Inserting Report Pivots - Product Recall Report
--Inserting Report   Version details- Product Recall Report
xxeis.eis_rsc_ins.rv( 'Product Recall Report','','Product Recall Report','SA059956');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
