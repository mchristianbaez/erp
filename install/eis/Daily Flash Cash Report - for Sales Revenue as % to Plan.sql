--Report Name            : Daily Flash Cash Report - for Sales Revenue as % to Plan
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View EIS_XXWC_AR_DAILY_FLASH_SALE_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_AR_DAILY_FLASH_SALE_V
Create or replace View XXEIS.EIS_XXWC_AR_DAILY_FLASH_SALE_V
(DATE1,BRANCH_LOCATION,SALE_AMT,SALE_COST,PAYMENT_TYPE,INVOICE_COUNT) AS 
SELECT TRUNC (xxeis.eis_rs_xxwc_com_util_pkg.get_date_from) date1,
    ood.organization_code branch_location,
    SUM ( NVL (rctl.quantity_invoiced, rctl.quantity_credited) * NVL (ol.unit_selling_price, 0)) sale_amt,
    SUM ( NVL (rctl.quantity_invoiced, rctl.quantity_credited) * NVL (apps.xxwc_mv_routines_pkg.get_order_line_cost (ol.line_id) , 0)) sale_cost,
    --NVL(SUM(DECODE(otl.order_category_code,'RETURN',(NVL(Ol.ordered_quantity,0)*-1),NVL(Ol.ordered_quantity,0)) * NVL(Ol.Unit_Selling_Price,0)),0) SALE_AMT,
    --SUM (DECODE(otl.order_category_code,'RETURN',(NVL(Ol.ordered_quantity,0)   *-1),NVL(Ol.ordered_quantity,0)) *NVL(ol.unit_cost,0))sale_cost,
    -- SUM(oep.payment_amount) cash_sales,
    xxeis.eis_rs_xxwc_com_util_pkg.get_payment_type payment_type,
    COUNT ( DISTINCT xxeis.eis_rs_xxwc_com_util_pkg.get_dist_invoice_num ( rct.customer_trx_id, rct.trx_number, otlh.name)) invoice_count
    ---Primary Keys
  FROM ra_customer_trx rct,
    ra_customer_trx_lines rctl,
    oe_order_headers oh,
    oe_order_lines ol,
    org_organization_definitions ood,
    oe_transaction_types_vl otl,
    oe_transaction_types_vl otlh
  WHERE 1 = 1
    --AND trunc(Rct.creation_date) = trunc(xxeis.eis_rs_xxwc_com_util_pkg.get_date_from)
  AND rct.creation_date >= TO_DATE ( TO_CHAR ( xxeis.eis_rs_xxwc_com_util_pkg.get_date_from, xxeis.eis_rs_utility.get_date_format
    || ' HH24:MI:SS'), xxeis.eis_rs_utility.get_date_format
    || ' HH24:MI:SS') + 0.25
  AND rct.creation_date <= TO_DATE ( TO_CHAR ( xxeis.eis_rs_xxwc_com_util_pkg.get_date_from, xxeis.eis_rs_utility.get_date_format
    || ' HH24:MI:SS'), xxeis.eis_rs_utility.get_date_format
    || ' HH24:MI:SS') + 1.25
    --and TO_CHAR(RCT.CREATION_DATE,'DD-MON-YY HH:MM:SS')    >= TO_CHAR(XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_DATE_FROM ,'DD-MON-YY HH:MM:SS') +0.25
    -- AND TO_CHAR(Rct.creation_date ,'DD-MON-YY HH:MM:SS')   <= to_char(xxeis.eis_rs_xxwc_com_util_pkg.get_date_from ,'DD-MON-YY HH:MM:SS') +1.25
  AND TO_CHAR (oh.order_number)                = TO_CHAR (rct.interface_header_attribute1)
  AND ood.organization_id(+)                   = ol.ship_from_org_id
  AND TO_CHAR (rctl.interface_line_attribute6) = TO_CHAR (ol.line_id)
  AND rctl.inventory_item_id                   = ol.inventory_item_id
  AND otl.transaction_type_id                  = ol.line_type_id
  AND otlh.transaction_type_id                 = oh.order_type_id
  AND rctl.customer_trx_id                     = rct.customer_trx_id
  AND ol.ordered_item  NOT IN('CONTOFFSET','CONTBILL')
 /* AND NOT EXISTS(SELECT 1 
                   from dual 
                   where ol.ordered_item  in('CONTOFFSET','CONTBILL')
                )*/
  AND ol.header_id                             = oh.header_id
  AND rctl.interface_line_context              = 'ORDER ENTRY'
  AND rct.interface_header_context             = 'ORDER ENTRY'
  AND NOT EXISTS
    (SELECT 1
    FROM hz_customer_profiles hcp,
      hz_cust_profile_classes hcpc,
      hz_cust_accounts hca
    WHERE hca.party_id       = hcp.party_id
    AND oh.sold_to_org_id    = hcp.cust_account_id
    AND hcp.site_use_id     IS NULL
    AND hcp.profile_class_id = hcpc.profile_class_id(+)
    AND hcpc.name LIKE 'WC%Branches%'
    )
    /* AND NOT EXISTS
    (SELECT 1
    FROM OE_PRICE_ADJUSTMENTS_V
    WHERE HEADER_ID     = OL.HEADER_ID
    AND LINE_ID         = OL.LINE_ID
    AND adjustment_name ='BRANCH_COST_MODIFIER'
    )*/
  AND ( ( xxeis.eis_rs_xxwc_com_util_pkg.get_payment_type = 'CASH'
  AND ( EXISTS
    (SELECT 1
    FROM oe_payments oep
    WHERE ( ( oep.payment_type_code IN ('CASH', 'CHECK', 'CREDIT_CARD')
    AND oep.header_id                = ol.header_id))
    )
  OR ( otl.order_category_code = 'RETURN'
  AND EXISTS
    (SELECT 1
    FROM xxwc_om_cash_refund_tbl xoc
    WHERE xoc.return_header_id = oh.header_id
    ))))
  OR ( xxeis.eis_rs_xxwc_com_util_pkg.get_payment_type = 'CHARGE'
  AND ( ( NOT EXISTS
    (SELECT 1 FROM oe_payments oep WHERE oep.header_id = ol.header_id
    )
  AND otl.order_category_code <> 'RETURN')
  OR ( otl.order_category_code = 'RETURN'
  AND NOT EXISTS
    (SELECT 1
    FROM xxwc_om_cash_refund_tbl xoc1
    WHERE xoc1.return_header_id = oh.header_id
    )))))
  GROUP BY TRUNC (xxeis.eis_rs_xxwc_com_util_pkg.get_date_from),
    ood.organization_code,
    xxeis.eis_rs_xxwc_com_util_pkg.get_payment_type
/
set scan on define on
prompt Creating View Data for Daily Flash Cash Report - for Sales Revenue as % to Plan
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_AR_DAILY_FLASH_SALE_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_AR_DAILY_FLASH_SALE_V',660,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Xxwc Ar Daily Flash Sale V','EXADFSV','','');
--Delete View Columns for EIS_XXWC_AR_DAILY_FLASH_SALE_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_AR_DAILY_FLASH_SALE_V',660,FALSE);
--Inserting View Columns for EIS_XXWC_AR_DAILY_FLASH_SALE_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_DAILY_FLASH_SALE_V','DATE1',660,'Date1','DATE1','','','','XXEIS_RS_ADMIN','DATE','','','Date1','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_DAILY_FLASH_SALE_V','BRANCH_LOCATION',660,'Branch Location','BRANCH_LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Branch Location','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_DAILY_FLASH_SALE_V','INVOICE_COUNT',660,'Invoice Count','INVOICE_COUNT','','','','XXEIS_RS_ADMIN','NUMBER','','','Invoice Count','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_DAILY_FLASH_SALE_V','SALE_AMT',660,'Sale Amt','SALE_AMT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Sale Amt','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_DAILY_FLASH_SALE_V','SALE_COST',660,'Sale Cost','SALE_COST','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Sale Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_DAILY_FLASH_SALE_V','PAYMENT_TYPE',660,'Payment Type','PAYMENT_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Payment Type','','','');
--Inserting View Components for EIS_XXWC_AR_DAILY_FLASH_SALE_V
--Inserting View Component Joins for EIS_XXWC_AR_DAILY_FLASH_SALE_V
END;
/
set scan on define on
prompt Creating Report LOV Data for Daily Flash Cash Report - for Sales Revenue as % to Plan
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Daily Flash Cash Report - for Sales Revenue as % to Plan
xxeis.eis_rs_ins.lov( 660,'SELECT ORGANIZATION_CODE WAREHOUSE,ORGANIZATION_NAME FROM ORG_ORGANIZATION_DEFINITIONS OOD WHERE SYSDATE < NVL(OOD.DISABLE_DATE,SYSDATE+1) AND EXISTS (SELECT 1 FROM XXEIS.EIS_ORG_ACCESS_V  WHERE organization_id = ood.organization_id )
UNION
SELECT ''All'', ''All Organizations'' from Dual','','OM Warehouse All','','ANONYMOUS',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Daily Flash Cash Report - for Sales Revenue as % to Plan
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Daily Flash Cash Report - for Sales Revenue as % to Plan
xxeis.eis_rs_utility.delete_report_rows( 'Daily Flash Cash Report - for Sales Revenue as % to Plan' );
--Inserting Report - Daily Flash Cash Report - for Sales Revenue as % to Plan
xxeis.eis_rs_ins.r( 660,'Daily Flash Cash Report - for Sales Revenue as % to Plan','','The purpose of this extract is to provide Finance with a daily report of all cash sales by branch.  The selected date parameter represents the desired date of sales (e.g. For sales reported on Aug 1st, set the Date parameter = Aug. 1st.  This report is to be processed daily and intended to accompany the daily extracts, flash_charge and flash_cash.
','','','','XXEIS_RS_ADMIN','EIS_XXWC_AR_DAILY_FLASH_SALE_V','Y','','','XXEIS_RS_ADMIN','','N','White Cap Reports','','CSV,HTML,EXCEL,','N');
--Inserting Report Columns - Daily Flash Cash Report - for Sales Revenue as % to Plan
xxeis.eis_rs_ins.rc( 'Daily Flash Cash Report - for Sales Revenue as % to Plan',660,'BRANCH_LOCATION','Location','Branch Location','','','default','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_DAILY_FLASH_SALE_V','','');
xxeis.eis_rs_ins.rc( 'Daily Flash Cash Report - for Sales Revenue as % to Plan',660,'DATE1','Date','Date1','','','default','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_DAILY_FLASH_SALE_V','','');
xxeis.eis_rs_ins.rc( 'Daily Flash Cash Report - for Sales Revenue as % to Plan',660,'INVOICE_COUNT','Invoice Count','Invoice Count','','~~~','default','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_DAILY_FLASH_SALE_V','','');
xxeis.eis_rs_ins.rc( 'Daily Flash Cash Report - for Sales Revenue as % to Plan',660,'GROSS_PROFIT','GP$','Invoice Count','NUMBER','~T~D~2','default','','5','Y','','','','','','','(NVL(EXADFSV.SALE_AMT,0)-NVL(EXADFSV.sale_cost,0))','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_DAILY_FLASH_SALE_V','','');
xxeis.eis_rs_ins.rc( 'Daily Flash Cash Report - for Sales Revenue as % to Plan',660,'SALE_AMT','Net Sales Dollars','Sale Amt','','~T~D~2','default','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_DAILY_FLASH_SALE_V','','');
--Inserting Report Parameters - Daily Flash Cash Report - for Sales Revenue as % to Plan
xxeis.eis_rs_ins.rp( 'Daily Flash Cash Report - for Sales Revenue as % to Plan',660,'Location','WC Branch Number','BRANCH_LOCATION','IN','OM Warehouse All','','VARCHAR2','Y','Y','2','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Daily Flash Cash Report - for Sales Revenue as % to Plan',660,'Date','Prior day�s date','','IN','','select trunc(sysdate-1) from dual','DATE','Y','Y','1','','N','SQL','XXEIS_RS_ADMIN','Y','N','','','');
--Inserting Report Conditions - Daily Flash Cash Report - for Sales Revenue as % to Plan
xxeis.eis_rs_ins.rcn( 'Daily Flash Cash Report - for Sales Revenue as % to Plan',660,'','','','','AND ( ''All'' IN (:Location) OR (BRANCH_LOCATION IN (:Location)))','Y','1','','XXEIS_RS_ADMIN');
--Inserting Report Sorts - Daily Flash Cash Report - for Sales Revenue as % to Plan
xxeis.eis_rs_ins.rs( 'Daily Flash Cash Report - for Sales Revenue as % to Plan',660,'DATE1','ASC','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rs( 'Daily Flash Cash Report - for Sales Revenue as % to Plan',660,'BRANCH_LOCATION','ASC','XXEIS_RS_ADMIN','','');
--Inserting Report Triggers - Daily Flash Cash Report - for Sales Revenue as % to Plan
xxeis.eis_rs_ins.rt( 'Daily Flash Cash Report - for Sales Revenue as % to Plan',660,'begin
xxeis.eis_rs_xxwc_com_util_pkg.set_payment_type(''CASH'');
xxeis.eis_rs_xxwc_com_util_pkg.set_date_from(:Date);
end;','B','Y','XXEIS_RS_ADMIN');
--Inserting Report Templates - Daily Flash Cash Report - for Sales Revenue as % to Plan
--Inserting Report Portals - Daily Flash Cash Report - for Sales Revenue as % to Plan
--Inserting Report Dashboards - Daily Flash Cash Report - for Sales Revenue as % to Plan
--Inserting Report Security - Daily Flash Cash Report - for Sales Revenue as % to Plan
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','660','','51030',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','660','','50638',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','660','','50622',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','401','','50941',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','660','','21404',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','660','','20678',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','660','','50846',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','660','','50845',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','660','','50847',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','660','','50848',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','660','','50849',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','660','','50944',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','660','','50871',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','20005','','50880',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','','LC053655','',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','','10010432','',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','','RB054040','',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','','RV003897','',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','','SS084202','',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','','SO004816','',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','660','','50856',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','660','','50857',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','660','','50858',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','660','','50859',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','660','','50860',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','660','','50861',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','660','','50886',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','660','','50901',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','660','','50870',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','660','','50869',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','20005','','50900',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','660','','51044',660,'XXEIS_RS_ADMIN','','');
--Inserting Report Pivots - Daily Flash Cash Report - for Sales Revenue as % to Plan
xxeis.eis_rs_ins.rpivot( 'Daily Flash Cash Report - for Sales Revenue as % to Plan',660,'Pivot','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rs_ins.rpivot_dtls( 'Daily Flash Cash Report - for Sales Revenue as % to Plan',660,'Pivot','BRANCH_LOCATION','ROW_FIELD','','Location','2','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Daily Flash Cash Report - for Sales Revenue as % to Plan',660,'Pivot','DATE1','ROW_FIELD','','Date','1','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Daily Flash Cash Report - for Sales Revenue as % to Plan',660,'Pivot','GROSS_PROFIT','ROW_FIELD','','','3','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Daily Flash Cash Report - for Sales Revenue as % to Plan',660,'Pivot','INVOICE_COUNT','ROW_FIELD','','','4','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Daily Flash Cash Report - for Sales Revenue as % to Plan',660,'Pivot','SALE_AMT','ROW_FIELD','','','5','','');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
END;
/
set scan on define on
