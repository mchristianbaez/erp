--Report Name            : Rental Utilization and Usage by Category - WC
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating View Data for Rental Utilization and Usage by Category - WC
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_OM_RENTAL_ITEMS_CAT_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_OM_RENTAL_ITEMS_CAT_V',660,'','','','','SA059956','XXEIS','Eis Xxwc Om Rental Items V','EXORIV','','');
--Delete View Columns for EIS_XXWC_OM_RENTAL_ITEMS_CAT_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_OM_RENTAL_ITEMS_CAT_V',660,FALSE);
--Inserting View Columns for EIS_XXWC_OM_RENTAL_ITEMS_CAT_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_RENTAL_ITEMS_CAT_V','ITEM_DESCRIPTION',660,'Item Description','ITEM_DESCRIPTION','','','','SA059956','VARCHAR2','','','Item Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_RENTAL_ITEMS_CAT_V','ITEM_NUMBER',660,'Item Number','ITEM_NUMBER','','','','SA059956','VARCHAR2','','','Item Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_RENTAL_ITEMS_CAT_V','LOCATION',660,'Location','LOCATION','','','','SA059956','VARCHAR2','','','Location','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_RENTAL_ITEMS_CAT_V','QTY_ON_RENT',660,'Qty On Rent','QTY_ON_RENT','','','','SA059956','NUMBER','','','Qty On Rent','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_RENTAL_ITEMS_CAT_V','REGION',660,'Region','REGION','','','','SA059956','VARCHAR2','','','Region','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_RENTAL_ITEMS_CAT_V','DEMAND_QTY',660,'Demand Qty','DEMAND_QTY','','','','SA059956','NUMBER','','','Demand Qty','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_RENTAL_ITEMS_CAT_V','VARIANC',660,'Varianc','VARIANC','','','','SA059956','NUMBER','','','Varianc','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_RENTAL_ITEMS_CAT_V','UTILIZATION_RATE',660,'Utilization Rate','UTILIZATION_RATE','','','','SA059956','NUMBER','','','Utilization Rate','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_RENTAL_ITEMS_CAT_V','OWNED',660,'Owned','OWNED','','','','SA059956','NUMBER','','','Owned','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_RENTAL_ITEMS_CAT_V','ON_HAND',660,'On Hand','ON_HAND','','','','SA059956','NUMBER','','','On Hand','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_RENTAL_ITEMS_CAT_V','CATEGORY',660,'Category','CATEGORY','','','','SA059956','VARCHAR2','','','Category','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_RENTAL_ITEMS_CAT_V','CATEGORY_CLASS',660,'Category Class','CATEGORY_CLASS','','','','SA059956','VARCHAR2','','','Category Class','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_RENTAL_ITEMS_CAT_V','SUB_CATEGORY',660,'Sub Category','SUB_CATEGORY','','','','SA059956','VARCHAR2','','','Sub Category','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_RENTAL_ITEMS_CAT_V','CAT_CLASS_DESCRIPTION',660,'Cat Class Description','CAT_CLASS_DESCRIPTION','','','','SA059956','VARCHAR2','','','Cat Class Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_RENTAL_ITEMS_CAT_V','RENTAL_TYPE',660,'Rental Type','RENTAL_TYPE','','','','SA059956','CHAR','','','Rental Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_RENTAL_ITEMS_CAT_V','AVERAGE_COST',660,'Average Cost','AVERAGE_COST','','','','SA059956','NUMBER','','','Average Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_RENTAL_ITEMS_CAT_V','ACTUAL_RENT_DAYS',660,'Actual Rent Days','ACTUAL_RENT_DAYS','','','','SA059956','NUMBER','','','Actual Rent Days','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_RENTAL_ITEMS_CAT_V','DISTRICT',660,'District','DISTRICT','','','','SA059956','VARCHAR2','','','District','','','');
--Inserting View Components for EIS_XXWC_OM_RENTAL_ITEMS_CAT_V
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_RENTAL_ITEMS_CAT_V','OE_ORDER_LINES',660,'OE_ORDER_LINES_ALL','OL','OL','SA059956','SA059956','-1','Oe Order Lines All Stores Information For All Orde','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_RENTAL_ITEMS_CAT_V','OE_ORDER_HEADERS',660,'OE_ORDER_HEADERS_ALL','OH','OH','SA059956','SA059956','-1','Oe Order Headers All Stores Header Information For','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_RENTAL_ITEMS_CAT_V','HZ_PARTIES',660,'HZ_PARTIES','HZP','HZP','SA059956','SA059956','-1','Information About Parties Such As Organizations, P','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_RENTAL_ITEMS_CAT_V','MTL_PARAMETERS',660,'MTL_PARAMETERS','MTP','MTP','SA059956','SA059956','-1','Inventory Control Options And Defaults','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_RENTAL_ITEMS_CAT_V','MTL_SYSTEM_ITEMS_KFV',660,'MTL_SYSTEM_ITEMS_B','MSI','MSI','SA059956','SA059956','-1','Inventory Item Definitions','','','','');
--Inserting View Component Joins for EIS_XXWC_OM_RENTAL_ITEMS_CAT_V
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_RENTAL_ITEMS_CAT_V','OE_ORDER_HEADERS','OH',660,'EXORIV.HEADER_ID','=','OH.HEADER_ID(+)','','','','Y','SA059956','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_RENTAL_ITEMS_CAT_V','OE_ORDER_LINES','OL',660,'EXORIV.LINE_ID','=','OL.LINE_ID(+)','','','','Y','SA059956','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_RENTAL_ITEMS_CAT_V','HZ_PARTIES','HZP',660,'EXORIV.PARTY_ID','=','HZP.PARTY_ID(+)','','','','Y','SA059956','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_RENTAL_ITEMS_CAT_V','MTL_PARAMETERS','MTP',660,'EXORIV.ORGANIZATION_ID','=','MTP.ORGANIZATION_ID(+)','','','','Y','SA059956','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_RENTAL_ITEMS_CAT_V','MTL_SYSTEM_ITEMS_KFV','MSI',660,'EXORIV.INVENTORY_ITEM_ID','=','MSI.INVENTORY_ITEM_ID(+)','','','','Y','SA059956','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_RENTAL_ITEMS_CAT_V','MTL_SYSTEM_ITEMS_KFV','MSI',660,'EXORIV.MSI_ORGANIZATION_ID','=','MSI.ORGANIZATION_ID(+)','','','','Y','SA059956','','');
END;
/
set scan on define on
prompt Creating Report LOV Data for Rental Utilization and Usage by Category - WC
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Rental Utilization and Usage by Category - WC
xxeis.eis_rs_ins.lov( 660,'SELECT ORGANIZATION_CODE WAREHOUSE,ORGANIZATION_NAME FROM ORG_ORGANIZATION_DEFINITIONS OOD WHERE SYSDATE < NVL(OOD.DISABLE_DATE,SYSDATE+1) AND EXISTS (SELECT 1 FROM XXEIS.EIS_ORG_ACCESS_V  WHERE organization_id = ood.organization_id )','','OM WAREHOUSE','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( '','select msi.segment1 item_number, msi.description item_description from apps.mtl_system_items msi where organization_id=222
and msi.segment1 like ''R%''
UNION
select ''All Rental Items'' item_number,''All Rental Items'' item_description from dual','','XX EIS WC RENTAL ITEM LOV','To display Rental item list','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'SELECT ''Rental'' rental_type FROM dual
UNION ALL
SELECT ''ReRent'' rental_type FROM dual','','EIS OM Rental Type LOV','','SA059956',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Rental Utilization and Usage by Category - WC
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Rental Utilization and Usage by Category - WC
xxeis.eis_rs_utility.delete_report_rows( 'Rental Utilization and Usage by Category - WC' );
--Inserting Report - Rental Utilization and Usage by Category - WC
xxeis.eis_rs_ins.r( 660,'Rental Utilization and Usage by Category - WC','','Rental Utilization and Usage - WC','','','','SA059956','EIS_XXWC_OM_RENTAL_ITEMS_CAT_V','Y','','','SA059956','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','');
--Inserting Report Columns - Rental Utilization and Usage by Category - WC
xxeis.eis_rs_ins.rc( 'Rental Utilization and Usage by Category - WC',660,'DEMAND_QTY','Quantity Demand','Demand Qty','','~~~','default','','13','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_RENTAL_ITEMS_CAT_V','','');
xxeis.eis_rs_ins.rc( 'Rental Utilization and Usage by Category - WC',660,'ITEM_DESCRIPTION','Item Description','Item Description','','','default','','10','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_RENTAL_ITEMS_CAT_V','','');
xxeis.eis_rs_ins.rc( 'Rental Utilization and Usage by Category - WC',660,'ITEM_NUMBER','Item Number','Item Number','','','default','','9','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_RENTAL_ITEMS_CAT_V','','');
xxeis.eis_rs_ins.rc( 'Rental Utilization and Usage by Category - WC',660,'LOCATION','Location','Location','','','default','','3','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_RENTAL_ITEMS_CAT_V','','');
xxeis.eis_rs_ins.rc( 'Rental Utilization and Usage by Category - WC',660,'OWNED','Owned','Owned','','~~~','default','','11','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_RENTAL_ITEMS_CAT_V','','');
xxeis.eis_rs_ins.rc( 'Rental Utilization and Usage by Category - WC',660,'QTY_ON_RENT','Quantity On Rent','Qty On Rent','','~~~','default','','14','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_RENTAL_ITEMS_CAT_V','','');
xxeis.eis_rs_ins.rc( 'Rental Utilization and Usage by Category - WC',660,'REGION','Region','Region','','','default','','1','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_RENTAL_ITEMS_CAT_V','','');
xxeis.eis_rs_ins.rc( 'Rental Utilization and Usage by Category - WC',660,'UTILIZATION_RATE','Utilization Rate%','Utilization Rate','','~,~.~2','default','','16','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_RENTAL_ITEMS_CAT_V','','');
xxeis.eis_rs_ins.rc( 'Rental Utilization and Usage by Category - WC',660,'VARIANC','Varianc','Varianc','','~~~','default','','15','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_RENTAL_ITEMS_CAT_V','','');
xxeis.eis_rs_ins.rc( 'Rental Utilization and Usage by Category - WC',660,'ON_HAND','Quantity On Hand','On Hand','','~~~','default','','12','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_RENTAL_ITEMS_CAT_V','','');
xxeis.eis_rs_ins.rc( 'Rental Utilization and Usage by Category - WC',660,'CATEGORY','Category','Category','','','default','','7','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_RENTAL_ITEMS_CAT_V','','');
xxeis.eis_rs_ins.rc( 'Rental Utilization and Usage by Category - WC',660,'CATEGORY_CLASS','Category Class','Category Class','','','default','','4','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_RENTAL_ITEMS_CAT_V','','');
xxeis.eis_rs_ins.rc( 'Rental Utilization and Usage by Category - WC',660,'SUB_CATEGORY','Sub Category','Sub Category','','','default','','8','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_RENTAL_ITEMS_CAT_V','','');
xxeis.eis_rs_ins.rc( 'Rental Utilization and Usage by Category - WC',660,'CAT_CLASS_DESCRIPTION','Cat Class Description','Cat Class Description','','','default','','6','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_RENTAL_ITEMS_CAT_V','','');
xxeis.eis_rs_ins.rc( 'Rental Utilization and Usage by Category - WC',660,'RENTAL_TYPE','Rental Type','Rental Type','','','','','5','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_RENTAL_ITEMS_CAT_V','','');
xxeis.eis_rs_ins.rc( 'Rental Utilization and Usage by Category - WC',660,'DISTRICT','District','District','','','','','2','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_RENTAL_ITEMS_CAT_V','','');
--Inserting Report Parameters - Rental Utilization and Usage by Category - WC
xxeis.eis_rs_ins.rp( 'Rental Utilization and Usage by Category - WC',660,'Warehouse','Warehouse','LOCATION','IN','OM WAREHOUSE','','VARCHAR2','N','Y','1','','Y','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Rental Utilization and Usage by Category - WC',660,'Rental Item Number','Item Number','ITEM_NUMBER','IN','XX EIS WC RENTAL ITEM LOV','''All Rental Items''','VARCHAR2','Y','Y','3','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Rental Utilization and Usage by Category - WC',660,'Rental Type','Rental Type','RENTAL_TYPE','IN','EIS OM Rental Type LOV','','VARCHAR2','N','Y','2','','Y','CONSTANT','SA059956','Y','N','','','');
--Inserting Report Conditions - Rental Utilization and Usage by Category - WC
xxeis.eis_rs_ins.rcn( 'Rental Utilization and Usage by Category - WC',660,'LOCATION','IN',':Warehouse','','','Y','1','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'Rental Utilization and Usage by Category - WC',660,'','','','','and (''All Rental Items'' IN (:Rental Item Number) or (ITEM_NUMBER in (:Rental Item Number)))
AND (QTY_ON_RENT<>0 or ON_HAND<>0 or DEMAND_QTY<>0)','Y','1','','SA059956');
xxeis.eis_rs_ins.rcn( 'Rental Utilization and Usage by Category - WC',660,'RENTAL_TYPE','IN',':Rental Type','','','Y','2','Y','SA059956');
--Inserting Report Sorts - Rental Utilization and Usage by Category - WC
--Inserting Report Triggers - Rental Utilization and Usage by Category - WC
--Inserting Report Templates - Rental Utilization and Usage by Category - WC
--Inserting Report Portals - Rental Utilization and Usage by Category - WC
--Inserting Report Dashboards - Rental Utilization and Usage by Category - WC
--Inserting Report Security - Rental Utilization and Usage by Category - WC
xxeis.eis_rs_ins.rsec( 'Rental Utilization and Usage by Category - WC','660','','50886',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Rental Utilization and Usage by Category - WC','660','','50860',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Rental Utilization and Usage by Category - WC','660','','51044',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Rental Utilization and Usage by Category - WC','20005','','50900',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Rental Utilization and Usage by Category - WC','','10010432','',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Rental Utilization and Usage by Category - WC','','SS084202','',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Rental Utilization and Usage by Category - WC','660','','51025',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Rental Utilization and Usage by Category - WC','660','','50859',660,'SA059956','','');
--Inserting Report Pivots - Rental Utilization and Usage by Category - WC
xxeis.eis_rs_ins.rpivot( 'Rental Utilization and Usage by Category - WC',660,'Pivot','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rs_ins.rpivot_dtls( 'Rental Utilization and Usage by Category - WC',660,'Pivot','DEMAND_QTY','DATA_FIELD','SUM','','2','','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Rental Utilization and Usage by Category - WC',660,'Pivot','QTY_ON_RENT','DATA_FIELD','SUM','','3','','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Rental Utilization and Usage by Category - WC',660,'Pivot','VARIANC','DATA_FIELD','SUM','','4','','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Rental Utilization and Usage by Category - WC',660,'Pivot','REGION','ROW_FIELD','','','1','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'Rental Utilization and Usage by Category - WC',660,'Pivot','ITEM_DESCRIPTION','ROW_FIELD','','','2','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'Rental Utilization and Usage by Category - WC',660,'Pivot','UTILIZATION_RATE','DATA_FIELD','AVE','','5','','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Rental Utilization and Usage by Category - WC',660,'Pivot','OWNED','DATA_FIELD','SUM','','1','','xlNormal');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
END;
/
set scan on define on
