--Report Name            : WC Long Running Concurrent Programs Rpt
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data EIS_XXWC_LONG_RUNNING_RPT_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(85000);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_LONG_RUNNING_RPT_V
xxeis.eis_rsc_ins.v( 'EIS_XXWC_LONG_RUNNING_RPT_V',85000,'','','','','ANONYMOUS','XXEIS','Eis Xxwc Long Running Rpt V','EXLRRV','','','VIEW','US','','');
--Delete Object Columns for EIS_XXWC_LONG_RUNNING_RPT_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_LONG_RUNNING_RPT_V',85000,FALSE);
--Inserting Object Columns for EIS_XXWC_LONG_RUNNING_RPT_V
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_LONG_RUNNING_RPT_V','COMPLETION_TEXT',85000,'Completion Text','COMPLETION_TEXT','','','','ANONYMOUS','VARCHAR2','','','Completion Text','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_LONG_RUNNING_RPT_V','PARAMETERS',85000,'Parameters','PARAMETERS','','','','ANONYMOUS','VARCHAR2','','','Parameters','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_LONG_RUNNING_RPT_V','DATE_COMPLETED',85000,'Date Completed','DATE_COMPLETED','','','','ANONYMOUS','DATE','','','Date Completed','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_LONG_RUNNING_RPT_V','DATE_STARTED',85000,'Date Started','DATE_STARTED','','','','ANONYMOUS','DATE','','','Date Started','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_LONG_RUNNING_RPT_V','DURATION_MINS',85000,'Duration Mins','DURATION_MINS','','','','ANONYMOUS','NUMBER','','','Duration Mins','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_LONG_RUNNING_RPT_V','REQUESTOR',85000,'Requestor','REQUESTOR','','','','ANONYMOUS','VARCHAR2','','','Requestor','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_LONG_RUNNING_RPT_V','REQUEST_NAME',85000,'Request Name','REQUEST_NAME','','','','ANONYMOUS','VARCHAR2','','','Request Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_LONG_RUNNING_RPT_V','REQUEST_ID',85000,'Request Id','REQUEST_ID','','','','ANONYMOUS','NUMBER','','','Request Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_LONG_RUNNING_RPT_V','ACTUAL_COMPLETION_DATE',85000,'Actual Completion Date','ACTUAL_COMPLETION_DATE','','','','ANONYMOUS','DATE','','','Actual Completion Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_LONG_RUNNING_RPT_V','ACTUAL_START_DATE',85000,'Actual Start Date','ACTUAL_START_DATE','','','','ANONYMOUS','DATE','','','Actual Start Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_LONG_RUNNING_RPT_V','REQUEST_DATE',85000,'Request Date','REQUEST_DATE','','','','ANONYMOUS','DATE','','','Request Date','','','','US');
--Inserting Object Components for EIS_XXWC_LONG_RUNNING_RPT_V
--Inserting Object Component Joins for EIS_XXWC_LONG_RUNNING_RPT_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 85000');
END IF;
END;
/
--There are no LOV/LOV TL entries for the report WC Long Running Concurrent Programs Rpt
prompt Creating Report Data for WC Long Running Concurrent Programs Rpt
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(85000);
IF mod_exist = 'Y' THEN 
--Deleting Report data - WC Long Running Concurrent Programs Rpt
xxeis.eis_rsc_utility.delete_report_rows( 'WC Long Running Concurrent Programs Rpt' );
--Inserting Report - WC Long Running Concurrent Programs Rpt
xxeis.eis_rsc_ins.r( 85000,'WC Long Running Concurrent Programs Rpt','','once every morning to publish or distribute an Excel report of all concurrent programs running over a certain run time threshold say "X" minutes. The "X" can be a parameter on the report defaulted to 120 mins (2 hours). Users should be able to run the report ad-hoc by providing the Date range, and "Run time threshold".','','','','GG050582','EIS_XXWC_LONG_RUNNING_RPT_V','Y','','','GG050582','','N','WC Knowledge Management Reports','PDF,','CSV,EXCEL,Pivot Excel,','N','','','','','','N','','US','','','','');
--Inserting Report Columns - WC Long Running Concurrent Programs Rpt
xxeis.eis_rsc_ins.rc( 'WC Long Running Concurrent Programs Rpt',85000,'COMPLETION_TEXT','Completion Text','Completion Text','','','default','','8','N','Y','','','','','','','GG050582','N','N','','EIS_XXWC_LONG_RUNNING_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'WC Long Running Concurrent Programs Rpt',85000,'DATE_COMPLETED','Date Completed','Date Completed','','MM/DD/RR HH24:MI','default','','6','N','Y','','','','','','','GG050582','N','N','','EIS_XXWC_LONG_RUNNING_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'WC Long Running Concurrent Programs Rpt',85000,'DATE_STARTED','Date Started','Date Started','','MM/DD/RR HH24:MI','default','','5','N','Y','','','','','','','GG050582','N','N','','EIS_XXWC_LONG_RUNNING_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'WC Long Running Concurrent Programs Rpt',85000,'DURATION_MINS','Duration Mins','Duration Mins','','~~~','default','','4','N','Y','','','','','','','GG050582','N','N','','EIS_XXWC_LONG_RUNNING_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'WC Long Running Concurrent Programs Rpt',85000,'PARAMETERS','Parameters','Parameters','','','default','','7','N','Y','','','','','','','GG050582','N','N','','EIS_XXWC_LONG_RUNNING_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'WC Long Running Concurrent Programs Rpt',85000,'REQUESTOR','Requestor','Requestor','','','default','','3','N','Y','','','','','','','GG050582','N','N','','EIS_XXWC_LONG_RUNNING_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'WC Long Running Concurrent Programs Rpt',85000,'REQUEST_ID','Request Id','Request Id','','~~~','default','','1','N','Y','','','','','','','GG050582','N','N','','EIS_XXWC_LONG_RUNNING_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'WC Long Running Concurrent Programs Rpt',85000,'REQUEST_NAME','Request Name','Request Name','','','default','','2','N','Y','','','','','','','GG050582','N','N','','EIS_XXWC_LONG_RUNNING_RPT_V','','','','US','');
--Inserting Report Parameters - WC Long Running Concurrent Programs Rpt
xxeis.eis_rsc_ins.rp( 'WC Long Running Concurrent Programs Rpt',85000,'Time Duration','Time Duration','DURATION_MINS','>=','','120','NUMBER','N','Y','3','Y','Y','CONSTANT','GG050582','Y','','','','','EIS_XXWC_LONG_RUNNING_RPT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC Long Running Concurrent Programs Rpt',85000,'Date From','Date From','DATE_STARTED','>=','','select sysdate -1 from dual','DATE','N','Y','1','Y','Y','SQL','GG050582','Y','','','Start Date','','EIS_XXWC_LONG_RUNNING_RPT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC Long Running Concurrent Programs Rpt',85000,'Date To','Date To','DATE_STARTED','<=','','','DATE','N','Y','2','Y','N','CURRENT_DATE','GG050582','Y','','','End Date','','EIS_XXWC_LONG_RUNNING_RPT_V','','','US','');
--Inserting Dependent Parameters - WC Long Running Concurrent Programs Rpt
--Inserting Report Conditions - WC Long Running Concurrent Programs Rpt
xxeis.eis_rsc_ins.rcnh( 'WC Long Running Concurrent Programs Rpt',85000,'DATE_STARTED >= :Date From ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','DATE_STARTED','','Date From','','','','','EIS_XXWC_LONG_RUNNING_RPT_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',85000,'WC Long Running Concurrent Programs Rpt','DATE_STARTED >= :Date From ');
xxeis.eis_rsc_ins.rcnh( 'WC Long Running Concurrent Programs Rpt',85000,'Free Text ','FREE_TEXT','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','','Y','','','','','AND ((to_date(to_char(request_date,''DD-MM-YY HH24:MI:SS''),''DD-MM-YY HH24:MI:SS'')
     between to_date(:Date From||'' 06:00:00'',''DD-MM-YY HH24:MI:SS'') AND to_date(:Date To|| '' 07:00:00'',''DD-MM-YY HH24:MI:SS''))
     or (TO_DATE(TO_CHAR(ACTUAL_START_DATE,''DD-MM-YY HH24:MI:SS''),''DD-MM-YY HH24:MI:SS'')
     between TO_DATE(:Date From||'' 06:00:00'',''DD-MM-YY HH24:MI:SS'') and TO_DATE(:Date To||'' 07:00:00'',''DD-MM-YY HH24:MI:SS'')))','1',85000,'WC Long Running Concurrent Programs Rpt','Free Text ');
xxeis.eis_rsc_ins.rcnh( 'WC Long Running Concurrent Programs Rpt',85000,'EXLRRV.DURATION_MINS >= Time Duration','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','DURATION_MINS','','Time Duration','','','','','EIS_XXWC_LONG_RUNNING_RPT_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',85000,'WC Long Running Concurrent Programs Rpt','EXLRRV.DURATION_MINS >= Time Duration');
--Inserting Report Sorts - WC Long Running Concurrent Programs Rpt
--Inserting Report Triggers - WC Long Running Concurrent Programs Rpt
--inserting report templates - WC Long Running Concurrent Programs Rpt
--Inserting Report Portals - WC Long Running Concurrent Programs Rpt
--inserting report dashboards - WC Long Running Concurrent Programs Rpt
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'WC Long Running Concurrent Programs Rpt','85000','EIS_XXWC_LONG_RUNNING_RPT_V','EIS_XXWC_LONG_RUNNING_RPT_V','N','');
--inserting report security - WC Long Running Concurrent Programs Rpt
xxeis.eis_rsc_ins.rsec( 'WC Long Running Concurrent Programs Rpt','20005','','XXWC_VIEW_ALL_EIS_REPORTS',85000,'GG050582','','','');
xxeis.eis_rsc_ins.rsec( 'WC Long Running Concurrent Programs Rpt','20005','','XXWC_IT_OPERATIONS_ANALYST',85000,'GG050582','','','');
xxeis.eis_rsc_ins.rsec( 'WC Long Running Concurrent Programs Rpt','20005','','XXWC_IT_SECURITY_ADMINISTRATOR',85000,'GG050582','','','');
xxeis.eis_rsc_ins.rsec( 'WC Long Running Concurrent Programs Rpt','20005','','XXWC_IT_FUNC_CONFIGURATOR',85000,'GG050582','','','');
xxeis.eis_rsc_ins.rsec( 'WC Long Running Concurrent Programs Rpt','170','','XXWC_KB_ADMIN',85000,'GG050582','','','');
--Inserting Report Pivots - WC Long Running Concurrent Programs Rpt
--Inserting Report   Version details- WC Long Running Concurrent Programs Rpt
xxeis.eis_rsc_ins.rv( 'WC Long Running Concurrent Programs Rpt','','WC Long Running Concurrent Programs Rpt','SR056655');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 85000');
END IF;
END;
/
