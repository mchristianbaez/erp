--Report Name            : Customer Listing with Created By
--Import Instructions : 1. If RTF template needs to be uploaded to report automatically then move the RTF template to physical path of APPS_DATA_FILE_DIR oracle directory. Check dba_directories view to get the physical path of oracle directory. 
--                         (If you want to use any other oracle directory other than APPS_DATA_FILE_DIR then replace APPS_DATA_FILE_DIR with your oracle directory name below.)
--                      2. Make sure XXEIS schema has read/write permissions to above used oracle directory and give full permissions to the directory at OS level.
--                      3. Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
exec xxeis.eis_rs_ins.g_rtf_dir := 'APPS_DATA_FILE_DIR' ;
prompt Creating View Data for Customer Listing with Created By
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_AR_CUST_LIST_SUMMARY_V
xxeis.eis_rs_ins.v( 'EIS_AR_CUST_LIST_SUMMARY_V',222,'This view shows a summary of customer records including contact information.  ','','','','SA059956','XXEIS','Eis Ar Cust List Summary V','EACLSV','','');
--Delete View Columns for EIS_AR_CUST_LIST_SUMMARY_V
xxeis.eis_rs_utility.delete_view_rows('EIS_AR_CUST_LIST_SUMMARY_V',222,FALSE);
--Inserting View Columns for EIS_AR_CUST_LIST_SUMMARY_V
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','FUNCTIONAL_CURRENCY',222,'Functional Currency','FUNCTIONAL_CURRENCY','','','','SA059956','VARCHAR2','Calculation Column','Calculation Column','Functional Currency','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','FUNCTIONAL_CURRENCY_PRECISION',222,'Functional Currency Precision','FUNCTIONAL_CURRENCY_PRECISION','','','','SA059956','NUMBER','Calculation Column','Calculation Column','Functional Currency Precision','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','CUSTOMER_NAME',222,'Customer Name','CUSTOMER_NAME','','','','SA059956','VARCHAR2','HZ_PARTIES','PARTY_NAME','Customer Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','CUSTOMER_NUMBER',222,'Customer Number','CUSTOMER_NUMBER','','','','SA059956','VARCHAR2','HZ_CUST_ACCOUNTS','ACCOUNT_NUMBER','Customer Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','SITE_USE',222,'Site Use','SITE_USE','','','','SA059956','VARCHAR2','FND_LOOKUP_VALUES','MEANING','Site Use','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','ADDRESS_ID',222,'Address Id','ADDRESS_ID','','','','SA059956','NUMBER','HZ_CUST_ACCT_SITES_ALL','CUST_ACCT_SITE_ID','Address Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','ADDRESS_LINE_1',222,'Address Line 1','ADDRESS_LINE_1','','','','SA059956','VARCHAR2','HZ_LOCATIONS','ADDRESS1','Address Line 1','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','CITY',222,'City','CITY','','','','SA059956','VARCHAR2','HZ_LOCATIONS','CITY','City','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','STATE',222,'State','STATE','','','','SA059956','VARCHAR2','HZ_LOCATIONS','STATE','State','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','ZIP_CODE',222,'Zip Code','ZIP_CODE','','','','SA059956','VARCHAR2','HZ_LOCATIONS','POSTAL_CODE','Zip Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','CUST_STATUS',222,'Cust Status','CUST_STATUS','','','','SA059956','VARCHAR2','HZ_CUST_ACCOUNTS','STATUS','Cust Status','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','CUST_ACCOUNT_ID',222,'Cust Account Id','CUST_ACCOUNT_ID~CUST_ACCOUNT_ID','','','','SA059956','NUMBER','','','Cust Account Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','CUST_ACCT_SITE_ID',222,'Cust Acct Site Id','CUST_ACCT_SITE_ID~CUST_ACCT_SITE_ID','','','','SA059956','NUMBER','','','Cust Acct Site Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','LOCATION_ID',222,'Location Id','LOCATION_ID~LOCATION_ID','','','','SA059956','NUMBER','','','Location Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','PARTY_SITE_ID',222,'Party Site Id','PARTY_SITE_ID~PARTY_SITE_ID','','','','SA059956','NUMBER','','','Party Site Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','SITE_USE_ID',222,'Site Use Id','SITE_USE_ID~SITE_USE_ID','','','','SA059956','NUMBER','','','Site Use Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','COMPANY_NAME',222,'Company Name','COMPANY_NAME','','','','SA059956','VARCHAR2','GL_SETS_OF_BOOKS','NAME','Company Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','COA_ID',222,'Coa Id','COA_ID~COA_ID','','','','SA059956','NUMBER','GL_SETS_OF_BOOKS','CHART_OF_ACCOUNTS_ID','Coa Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','CUSTOMER_KEY',222,'Customer Key','CUSTOMER_KEY~CUSTOMER_KEY','','','','SA059956','VARCHAR2','HZ_PARTIES','CUSTOMER_KEY','Customer Key','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','CUSTOMER_TYPE',222,'Customer Type','CUSTOMER_TYPE~CUSTOMER_TYPE','','','','SA059956','VARCHAR2','HZ_CUST_ACCOUNTS','CUSTOMER_TYPE','Customer Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','EMPLOYEES_TOTAL',222,'Employees Total','EMPLOYEES_TOTAL~EMPLOYEES_TOTAL','','','','SA059956','NUMBER','HZ_PARTIES','EMPLOYEES_TOTAL','Employees Total','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','FISCAL_YEAREND_MONTH',222,'Fiscal Yearend Month','FISCAL_YEAREND_MONTH~FISCAL_YEAREND_MONTH','','','','SA059956','VARCHAR2','Calculation Column','Calculation Column','Fiscal Yearend Month','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','FOB_POINT',222,'Fob Point','FOB_POINT~FOB_POINT','','','','SA059956','VARCHAR2','HZ_CUST_ACCOUNTS','FOB_POINT','Fob Point','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','FREIGHT_TERM',222,'Freight Term','FREIGHT_TERM~FREIGHT_TERM','','','','SA059956','VARCHAR2','HZ_CUST_ACCOUNTS','FREIGHT_TERM','Freight Term','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','IDENTIFYING_ADDRESS_FLAG',222,'Identifying Address Flag','IDENTIFYING_ADDRESS_FLAG~IDENTIFYING_ADDRESS_FLAG','','','','SA059956','VARCHAR2','HZ_PARTY_SITES','IDENTIFYING_ADDRESS_FLAG','Identifying Address Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','MISSION_STATEMENT',222,'Mission Statement','MISSION_STATEMENT~MISSION_STATEMENT','','','','SA059956','VARCHAR2','Calculation Column','Calculation Column','Mission Statement','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','ORDER_TYPE_ID',222,'Order Type Id','ORDER_TYPE_ID~ORDER_TYPE_ID','','','','SA059956','NUMBER','HZ_CUST_ACCOUNTS','ORDER_TYPE_ID','Order Type Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','ORIG_SYSTEM_REFERENCE',222,'Orig System Reference','ORIG_SYSTEM_REFERENCE~ORIG_SYSTEM_REFERENCE','','','','SA059956','VARCHAR2','HZ_CUST_ACCOUNTS','ORIG_SYSTEM_REFERENCE','Orig System Reference','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','PARTY_ID',222,'Party Id','PARTY_ID~PARTY_ID','','','','SA059956','NUMBER','HZ_PARTIES','PARTY_ID','Party Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','PARTY_NUMBER',222,'Party Number','PARTY_NUMBER~PARTY_NUMBER','','','','SA059956','VARCHAR2','HZ_PARTIES','PARTY_NUMBER','Party Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','PARTY_SITE_NUMBER',222,'Party Site Number','PARTY_SITE_NUMBER~PARTY_SITE_NUMBER','','','','SA059956','VARCHAR2','HZ_PARTY_SITES','PARTY_SITE_NUMBER','Party Site Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','PARTY_TYPE',222,'Party Type','PARTY_TYPE~PARTY_TYPE','','','','SA059956','VARCHAR2','HZ_PARTIES','PARTY_TYPE','Party Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','PERSON_FIRST_NAME',222,'Person First Name','PERSON_FIRST_NAME~PERSON_FIRST_NAME','','','','SA059956','VARCHAR2','HZ_PARTIES','PERSON_FIRST_NAME','Person First Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','PERSON_LAST_NAME',222,'Person Last Name','PERSON_LAST_NAME~PERSON_LAST_NAME','','','','SA059956','VARCHAR2','HZ_PARTIES','PERSON_LAST_NAME','Person Last Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','PERSON_MIDDLE_NAME',222,'Person Middle Name','PERSON_MIDDLE_NAME~PERSON_MIDDLE_NAME','','','','SA059956','VARCHAR2','HZ_PARTIES','PERSON_MIDDLE_NAME','Person Middle Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','PERSON_PRE_NAME_ADJUNCT',222,'Person Pre Name Adjunct','PERSON_PRE_NAME_ADJUNCT~PERSON_PRE_NAME_ADJUNCT','','','','SA059956','VARCHAR2','HZ_PARTIES','PERSON_PRE_NAME_ADJUNCT','Person Pre Name Adjunct','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','PERSON_SUFFIX',222,'Person Suffix','PERSON_SUFFIX~PERSON_SUFFIX','','','','SA059956','VARCHAR2','HZ_PARTIES','PERSON_NAME_SUFFIX','Person Suffix','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','POTENTIAL_REVENUE_CURR_FY',222,'Potential Revenue Curr Fy','POTENTIAL_REVENUE_CURR_FY~POTENTIAL_REVENUE_CURR_FY','','','','SA059956','NUMBER','Calculation Column','Calculation Column','Potential Revenue Curr Fy','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','POTENTIAL_REVENUE_NEXT_FY',222,'Potential Revenue Next Fy','POTENTIAL_REVENUE_NEXT_FY~POTENTIAL_REVENUE_NEXT_FY','','','','SA059956','NUMBER','Calculation Column','Calculation Column','Potential Revenue Next Fy','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','PRICE_LIST_ID',222,'Price List Id','PRICE_LIST_ID~PRICE_LIST_ID','','','','SA059956','NUMBER','HZ_CUST_ACCOUNTS','PRICE_LIST_ID','Price List Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','REFERENCE_USE_FLAG',222,'Reference Use Flag','REFERENCE_USE_FLAG~REFERENCE_USE_FLAG','','','','SA059956','VARCHAR2','HZ_PARTIES','REFERENCE_USE_FLAG','Reference Use Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','SALES_CHANNEL_CODE',222,'Sales Channel Code','SALES_CHANNEL_CODE~SALES_CHANNEL_CODE','','','','SA059956','VARCHAR2','HZ_CUST_ACCOUNTS','SALES_CHANNEL_CODE','Sales Channel Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','SHIP_VIA',222,'Ship Via','SHIP_VIA~SHIP_VIA','','','','SA059956','VARCHAR2','HZ_CUST_ACCOUNTS','SHIP_VIA','Ship Via','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','SIC_CODE_TYPE',222,'Sic Code Type','SIC_CODE_TYPE~SIC_CODE_TYPE','','','','SA059956','VARCHAR2','Calculation Column','Calculation Column','Sic Code Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','TAXPAYER_ID',222,'Taxpayer Id','TAXPAYER_ID~TAXPAYER_ID','','','','SA059956','VARCHAR2','HZ_PARTIES','JGZZ_FISCAL_CODE','Taxpayer Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','TAX_CODE',222,'Tax Code','TAX_CODE~TAX_CODE','','','','SA059956','VARCHAR2','HZ_CUST_ACCOUNTS','TAX_CODE','Tax Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','TAX_REFERENCE',222,'Tax Reference','TAX_REFERENCE~TAX_REFERENCE','','','','SA059956','VARCHAR2','HZ_PARTIES','TAX_REFERENCE','Tax Reference','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','THIRD_PARTY_FLAG',222,'Third Party Flag','THIRD_PARTY_FLAG~THIRD_PARTY_FLAG','','','','SA059956','VARCHAR2','HZ_PARTIES','THIRD_PARTY_FLAG','Third Party Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','ANALYSIS_FY',222,'Analysis Fy','ANALYSIS_FY~ANALYSIS_FY','','','','SA059956','VARCHAR2','Calculation Column','Calculation Column','Analysis Fy','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','COMPETITOR_FLAG',222,'Competitor Flag','COMPETITOR_FLAG~COMPETITOR_FLAG','','','','SA059956','VARCHAR2','HZ_PARTIES','COMPETITOR_FLAG','Competitor Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','CUSTOMER_CATEGORY_CODE',222,'Customer Category Code','CUSTOMER_CATEGORY_CODE~CUSTOMER_CATEGORY_CODE','','','','SA059956','VARCHAR2','HZ_PARTIES','CATEGORY_CODE','Customer Category Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','CUSTOMER_CLASS_CODE',222,'Customer Class Code','CUSTOMER_CLASS_CODE~CUSTOMER_CLASS_CODE','','','','SA059956','VARCHAR2','HZ_CUST_ACCOUNTS','CUSTOMER_CLASS_CODE','Customer Class Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','CUSTOMER_ID',222,'Customer Id','CUSTOMER_ID~CUSTOMER_ID','','','','SA059956','NUMBER','HZ_CUST_ACCOUNTS','CUST_ACCOUNT_ID','Customer Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','WAREHOUSE_ID',222,'Warehouse Id','WAREHOUSE_ID~WAREHOUSE_ID','','','','SA059956','NUMBER','HZ_CUST_ACCOUNTS','WAREHOUSE_ID','Warehouse Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','YEAR_ESTABLISHED',222,'Year Established','YEAR_ESTABLISHED~YEAR_ESTABLISHED','','','','SA059956','NUMBER','Calculation Column','Calculation Column','Year Established','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','CUST#PARTY_TYPE',222,'Descriptive flexfield: Customer Information Column Name: Party Type','CUST#Party_Type','','','','SA059956','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE1','Cust#Party Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','CUST#VNDR_CODE_AND_FRULOC',222,'Descriptive flexfield: Customer Information Column Name: Vndr Code and FRULOC','CUST#Vndr_Code_and_FRULOC','','','','SA059956','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE2','Cust#Vndr Code And Fruloc','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','CUST#BRANCH_DESCRIPTION',222,'Descriptive flexfield: Customer Information Column Name: Branch Description','CUST#Branch_Description','','','','SA059956','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE3','Cust#Branch Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','LOC#PAY_TO_VENDOR_ID',222,'Descriptive flexfield: TCA Location Information Column Name: Pay To Vendor ID','LOC#Pay_To_Vendor_ID','','','','SA059956','VARCHAR2','HZ_LOCATIONS','ATTRIBUTE1','Loc#Pay To Vendor Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','LOC#LOB',222,'Descriptive flexfield: TCA Location Information Column Name: LOB','LOC#LOB','','','','SA059956','VARCHAR2','HZ_LOCATIONS','ATTRIBUTE2','Loc#Lob','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','LOC#PAY_TO_VENDOR_CODE',222,'Descriptive flexfield: TCA Location Information Column Name: Pay To Vendor Code','LOC#Pay_To_Vendor_Code','','','','SA059956','VARCHAR2','HZ_LOCATIONS','ATTRIBUTE3','Loc#Pay To Vendor Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','PARTY#PARTY_TYPE',222,'Descriptive flexfield: Party Information Column Name: Party Type','PARTY#Party_Type','','','','SA059956','VARCHAR2','HZ_PARTIES','ATTRIBUTE1','Party#Party Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','PARTY#GVID_ID',222,'Descriptive flexfield: Party Information Column Name: GVID_ID','PARTY#GVID_ID','','','','SA059956','VARCHAR2','HZ_PARTIES','ATTRIBUTE2','Party#Gvid Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','PARTY_SITE#REBT_PAY_TO_VNDR_',222,'Descriptive flexfield: Party Site Information Column Name: Rebt Pay to Vndr ID','PARTY_SITE#Rebt_Pay_to_Vndr_','','','','SA059956','VARCHAR2','HZ_PARTY_SITES','ATTRIBUTE2','Party Site#Rebt Pay To Vndr Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','PARTY_SITE#REBT_LOB',222,'Descriptive flexfield: Party Site Information Column Name: Rebt LOB','PARTY_SITE#Rebt_LOB','','','','SA059956','VARCHAR2','HZ_PARTY_SITES','ATTRIBUTE3','Party Site#Rebt Lob','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','PARTY_SITE#REBT_VNDR_CODE',222,'Descriptive flexfield: Party Site Information Column Name: Rebt Vndr Code','PARTY_SITE#Rebt_Vndr_Code','','','','SA059956','VARCHAR2','HZ_PARTY_SITES','ATTRIBUTE4','Party Site#Rebt Vndr Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','PARTY_SITE#REBT_VNDR_FLAG',222,'Descriptive flexfield: Party Site Information Column Name: Rebt Vndr Flag','PARTY_SITE#Rebt_Vndr_Flag','','','','SA059956','VARCHAR2','HZ_PARTY_SITES','ATTRIBUTE5','Party Site#Rebt Vndr Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','PARTY_SITE#REBT_VNDR_TAX_ID_',222,'Descriptive flexfield: Party Site Information Column Name: Rebt Vndr Tax ID Nbr','PARTY_SITE#Rebt_Vndr_Tax_ID_','','','','SA059956','VARCHAR2','HZ_PARTY_SITES','ATTRIBUTE6','Party Site#Rebt Vndr Tax Id Nbr','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','CUST#CUSTOMER_SOURCE',222,'Descriptive flexfield: Customer Information Column Name: Customer Source','CUST#Customer_Source','','','','SA059956','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE4','Cust#Customer Source','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','CUST#LEGAL_COLLECTION_INDICA',222,'Descriptive flexfield: Customer Information Column Name: Legal Collection Indicator','CUST#Legal_Collection_Indica','','','','SA059956','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE5','Cust#Legal Collection Indicator','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','CUST#PRISM_NUMBER',222,'Descriptive flexfield: Customer Information Column Name: Prism Number','CUST#Prism_Number','','','','SA059956','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE6','Cust#Prism Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','CUST#YES#NOTE_LINE_#5',222,'Descriptive flexfield: Customer Information Column Name: Note Line #5 Context: Yes','CUST#Yes#Note_Line_#5','','','','SA059956','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE16','Cust#Yes#Note Line #5','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','CUST#YES#NOTE_LINE_#1',222,'Descriptive flexfield: Customer Information Column Name: Note Line #1 Context: Yes','CUST#Yes#Note_Line_#1','','','','SA059956','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE17','Cust#Yes#Note Line #1','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','CUST#YES#NOTE_LINE_#2',222,'Descriptive flexfield: Customer Information Column Name: Note Line #2 Context: Yes','CUST#Yes#Note_Line_#2','','','','SA059956','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE18','Cust#Yes#Note Line #2','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','CUST#YES#NOTE_LINE_#3',222,'Descriptive flexfield: Customer Information Column Name: Note Line #3 Context: Yes','CUST#Yes#Note_Line_#3','','','','SA059956','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE19','Cust#Yes#Note Line #3','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','CUST#YES#NOTE_LINE_#4',222,'Descriptive flexfield: Customer Information Column Name: Note Line #4 Context: Yes','CUST#Yes#Note_Line_#4','','','','SA059956','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE20','Cust#Yes#Note Line #4','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','ACCT_SITE#PRINT_PRICES_ON_OR',222,'Descriptive flexfield: Address Information Column Name: Print Prices on Order','ACCT_SITE#Print_Prices_on_Or','','','','SA059956','VARCHAR2','HZ_CUST_ACCT_SITES_ALL','ATTRIBUTE1','Acct Site#Print Prices On Order','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','ACCT_SITE#JOB_INFORMATION_ON',222,'Descriptive flexfield: Address Information Column Name: Job Information on File?','ACCT_SITE#Job_Information_on','','','','SA059956','VARCHAR2','HZ_CUST_ACCT_SITES_ALL','ATTRIBUTE14','Acct Site#Job Information On File?','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','ACCT_SITE#TAX_EXEMPTION_TYPE',222,'Descriptive flexfield: Address Information Column Name: Tax Exemption Type','ACCT_SITE#Tax_Exemption_Type','','','','SA059956','VARCHAR2','HZ_CUST_ACCT_SITES_ALL','ATTRIBUTE15','Acct Site#Tax Exemption Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','ACCT_SITE#TAX_EXEMPT1',222,'Descriptive flexfield: Address Information Column Name: Tax Exempt','ACCT_SITE#Tax_Exempt1','','','','SA059956','VARCHAR2','HZ_CUST_ACCT_SITES_ALL','ATTRIBUTE16','Acct Site#Tax Exempt','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','ACCT_SITE#PRISM_NUMBER',222,'Descriptive flexfield: Address Information Column Name: PRISM Number','ACCT_SITE#PRISM_Number','','','','SA059956','VARCHAR2','HZ_CUST_ACCT_SITES_ALL','ATTRIBUTE17','Acct Site#Prism Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','ACCT_SITE#MANDATORY_PO_NUMBE',222,'Descriptive flexfield: Address Information Column Name: Mandatory PO Number','ACCT_SITE#Mandatory_PO_Numbe','','','','SA059956','VARCHAR2','HZ_CUST_ACCT_SITES_ALL','ATTRIBUTE3','Acct Site#Mandatory Po Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','ACCT_SITE#LIEN_RELEASE_DATE',222,'Descriptive flexfield: Address Information Column Name: Lien Release Date','ACCT_SITE#Lien_Release_Date','','','','SA059956','VARCHAR2','HZ_CUST_ACCT_SITES_ALL','ATTRIBUTE5','Acct Site#Lien Release Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','ACCT_SITE#YES#NOTICE_TO_OWNE',222,'Descriptive flexfield: Address Information Column Name: Notice to Owner-Job Total Context: Yes','ACCT_SITE#Yes#Notice_to_Owne','','','','SA059956','VARCHAR2','HZ_CUST_ACCT_SITES_ALL','ATTRIBUTE18','Acct Site#Yes#Notice To Owner-Job Total','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','ACCT_SITE#YES#NOTICE_TO_OWNE1',222,'Descriptive flexfield: Address Information Column Name: Notice to Owner-Prelim Notice Context: Yes','ACCT_SITE#Yes#Notice_to_Owne1','','','','SA059956','VARCHAR2','HZ_CUST_ACCT_SITES_ALL','ATTRIBUTE19','Acct Site#Yes#Notice To Owner-Prelim Notice','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','ACCT_SITE#YES#NOTICE_TO_OWNE2',222,'Descriptive flexfield: Address Information Column Name: Notice to Owner-Prelim Date Context: Yes','ACCT_SITE#Yes#Notice_to_Owne2','','','','SA059956','VARCHAR2','HZ_CUST_ACCT_SITES_ALL','ATTRIBUTE20','Acct Site#Yes#Notice To Owner-Prelim Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','SITE_USES#CUSTOMER_SITE_CLAS',222,'Descriptive flexfield: Site Use Information Column Name: Customer Site Classification','SITE_USES#Customer_Site_Clas','','','','SA059956','VARCHAR2','HZ_CUST_SITE_USES_ALL','ATTRIBUTE1','Site Uses#Customer Site Classification','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','SITE_USES#GOVERNMENT_FUNDED',222,'Descriptive flexfield: Site Use Information Column Name: Government Funded?','SITE_USES#Government_Funded','','','','SA059956','VARCHAR2','HZ_CUST_SITE_USES_ALL','ATTRIBUTE2','Site Uses#Government Funded?','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','SITE_USES#THOMAS_GUIDE_PAGE',222,'Descriptive flexfield: Site Use Information Column Name: Thomas Guide Page','SITE_USES#Thomas_Guide_Page','','','','SA059956','VARCHAR2','HZ_CUST_SITE_USES_ALL','ATTRIBUTE3','Site Uses#Thomas Guide Page','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','SITE_USES#DODGE_NUMBER',222,'Descriptive flexfield: Site Use Information Column Name: Dodge Number','SITE_USES#Dodge_Number','','','','SA059956','VARCHAR2','HZ_CUST_SITE_USES_ALL','ATTRIBUTE4','Site Uses#Dodge Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','SITE_USES#FUTURE_USE',222,'Descriptive flexfield: Site Use Information Column Name: FUTURE USE','SITE_USES#FUTURE_USE','','','','SA059956','VARCHAR2','HZ_CUST_SITE_USES_ALL','ATTRIBUTE5','Site Uses#Future Use','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','SITE_USES#SALESREP_#2',222,'Descriptive flexfield: Site Use Information Column Name: Salesrep #2','SITE_USES#Salesrep_#2','','','','SA059956','VARCHAR2','HZ_CUST_SITE_USES_ALL','ATTRIBUTE6','Site Uses#Salesrep #2','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','SITE_USES#SALESREP_SPILT_#1',222,'Descriptive flexfield: Site Use Information Column Name: Salesrep Spilt #1','SITE_USES#Salesrep_Spilt_#1','','','','SA059956','VARCHAR2','HZ_CUST_SITE_USES_ALL','ATTRIBUTE7','Site Uses#Salesrep Spilt #1','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','SITE_USES#SALESREP_SPILT_#2',222,'Descriptive flexfield: Site Use Information Column Name: Salesrep Spilt #2','SITE_USES#Salesrep_Spilt_#2','','','','SA059956','VARCHAR2','HZ_CUST_SITE_USES_ALL','ATTRIBUTE8','Site Uses#Salesrep Spilt #2','','','');
xxeis.eis_rs_ins.vc( 'EIS_AR_CUST_LIST_SUMMARY_V','SITE_USES#PERMIT_NUMBER',222,'Descriptive flexfield: Site Use Information Column Name: Permit Number','SITE_USES#Permit_Number','','','','SA059956','VARCHAR2','HZ_CUST_SITE_USES_ALL','ATTRIBUTE9','Site Uses#Permit Number','','','');
--Inserting View Components for EIS_AR_CUST_LIST_SUMMARY_V
xxeis.eis_rs_ins.vcomp( 'EIS_AR_CUST_LIST_SUMMARY_V','HZ_CUST_SITE_USES',222,'HZ_CUST_SITE_USES_ALL','SITE_USES','SITE_USES','SA059956','SA059956','-1','Stores Business Purposes Assigned To Customer','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_AR_CUST_LIST_SUMMARY_V','HZ_CUST_ACCT_SITES',222,'HZ_CUST_ACCT_SITES_ALL','ACCT_SITE','ACCT_SITE','SA059956','SA059956','-1','Stores All Customer Account Sites','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_AR_CUST_LIST_SUMMARY_V','HZ_PARTY_SITES',222,'HZ_PARTY_SITES','PARTY_SITE','PARTY_SITE','SA059956','SA059956','-1','Links Party To Physical Locations','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_AR_CUST_LIST_SUMMARY_V','HZ_LOCATIONS',222,'HZ_LOCATIONS','LOC','LOC','SA059956','SA059956','-1','Physical Addresses','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_AR_CUST_LIST_SUMMARY_V','HZ_CUST_ACCOUNTS',222,'HZ_CUST_ACCOUNTS','CUST','CUST','SA059956','SA059956','-1','Stores Information About Customer Accounts.','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_AR_CUST_LIST_SUMMARY_V','HZ_PARTIES',222,'HZ_PARTIES','PARTY','PARTY','SA059956','SA059956','-1','Information About Parties Such As Organizations','','','','');
--Inserting View Component Joins for EIS_AR_CUST_LIST_SUMMARY_V
xxeis.eis_rs_ins.vcj( 'EIS_AR_CUST_LIST_SUMMARY_V','HZ_CUST_SITE_USES','SITE_USES',222,'EACLSV.SITE_USE_ID','=','SITE_USES.SITE_USE_ID(+)','','','','Y','SA059956','','');
xxeis.eis_rs_ins.vcj( 'EIS_AR_CUST_LIST_SUMMARY_V','HZ_CUST_ACCT_SITES','ACCT_SITE',222,'EACLSV.CUST_ACCT_SITE_ID','=','ACCT_SITE.CUST_ACCT_SITE_ID(+)','','','','Y','SA059956','','');
xxeis.eis_rs_ins.vcj( 'EIS_AR_CUST_LIST_SUMMARY_V','HZ_PARTY_SITES','PARTY_SITE',222,'EACLSV.PARTY_SITE_ID','=','PARTY_SITE.PARTY_SITE_ID(+)','','','','Y','SA059956','','');
xxeis.eis_rs_ins.vcj( 'EIS_AR_CUST_LIST_SUMMARY_V','HZ_LOCATIONS','LOC',222,'EACLSV.LOCATION_ID','=','LOC.LOCATION_ID(+)','','','','Y','SA059956','','');
xxeis.eis_rs_ins.vcj( 'EIS_AR_CUST_LIST_SUMMARY_V','HZ_CUST_ACCOUNTS','CUST',222,'EACLSV.CUST_ACCOUNT_ID','=','CUST.CUST_ACCOUNT_ID(+)','','','','Y','SA059956','','');
xxeis.eis_rs_ins.vcj( 'EIS_AR_CUST_LIST_SUMMARY_V','HZ_PARTIES','PARTY',222,'EACLSV.PARTY_ID','=','PARTY.PARTY_ID(+)','','','','Y','SA059956','','');
END;
/
set scan on define on
prompt Creating Report LOV Data for Customer Listing with Created By
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Customer Listing with Created By
xxeis.eis_rs_ins.lov( '','SELECT USER_NAME FROM FND_USER WHERE TRUNC(SYSDATE) BETWEEN START_DATE AND NVL(end_date,sysdate)','','ALL_USER_NAMES','','ANONYMOUS',NULL,'','','');
xxeis.eis_rs_ins.lov( 222,'select distinct party_name from hz_parties p, hz_cust_accounts c
    where p.party_id = c.party_id','null','Customer Name','Displays List of Values for Customer Name ','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 222,'select account_number from hz_cust_accounts','null','Customer Number','Displays List of Values for Customer Number','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 222,'select distinct state from hz_locations','','Customer State','','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 222,'select distinct postal_code from hz_locations','','Customer Zip Code','Customer Postal Code','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 222,'select distinct status from hz_cust_accounts','','Customer Status','','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 222,'select distinct city from hz_locations','','Receivables_city','','XXEIS_RS_ADMIN',NULL,'','','');
END;
/
set scan on define on
prompt Creating Report Data for Customer Listing with Created By
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Customer Listing with Created By
xxeis.eis_rs_utility.delete_report_rows( 'Customer Listing with Created By' );
--Inserting Report - Customer Listing with Created By
xxeis.eis_rs_ins.r( 222,'Customer Listing with Created By','','Report is for Pricing Group during the Conversion onto Oracle.  

This list provides all customer accounts and sites.','','','','SA059956','EIS_AR_CUST_LIST_SUMMARY_V','Y','','','SA059956','','N','White Cap Reports','','CSV,EXCEL,','N');
--Inserting Report Columns - Customer Listing with Created By
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'LOCATION','Location','Site use identifier','','','default','','22','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','HZ_CUST_SITE_USES','SITE_USES');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'PRIMARY_FLAG','Primary Flag','Indicates if this site is the primary site for this customer account. Y for the primary customer account site. N for other customer account sites.','','','default','','23','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','HZ_CUST_SITE_USES','SITE_USES');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'CREATION_DATE','Creation Date','Standard who column - date when this row was created.','','','default','','20','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','HZ_CUST_ACCOUNTS','CUST');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'STATUS','Status','Site use status flag, Lookup code for the CODE_STATUS column.','','','default','','24','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','HZ_CUST_SITE_USES','SITE_USES');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'ATTRIBUTE1','Customer Site Classification','DFF Context Code - Global Data Elements','','','default','','21','N','Y','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','HZ_CUST_SITE_USES','SITE_USES');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'LAST_UPDATED_BY','Last Updated By','Standard who column - user who last updated this row (foreign key to FND_USER.USER_ID).','','~~~','default','','26','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','HZ_CUST_SITE_USES','SITE_USES');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'LAST_UPDATE_LOGIN','Last Update Login','Standard who column - operating system login of user who last updated this row (foreign key to FND_LOGINS.LOGIN_ID).','','~~~','default','','27','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','HZ_CUST_SITE_USES','SITE_USES');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'LAST_UPDATE_DATE','Last Update Date','Standard Who column - date when a user last updated this row.','','','default','','25','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','HZ_CUST_SITE_USES','SITE_USES');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'CREATED_BY_MODULE','Created By Module','TCA Who column','','','default','','28','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','HZ_CUST_ACCT_SITES','ACCT_SITE');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'ACCOUNT_NAME','Account Name','Description chosen by external party (but can be entered internally on behalf on the customer)','','','','','62','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','HZ_CUST_ACCOUNTS','CUST');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'ACCOUNT_NUMBER','Account Number','Account Number','','','','','63','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','HZ_CUST_ACCOUNTS','CUST');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'ATTRIBUTE1','Party Type','DFF Context Code - Global Data Elements','','','','','67','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','HZ_CUST_ACCOUNTS','CUST');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'ATTRIBUTE10','Attribute10','Descriptive flexfield segment','','','','','43','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','HZ_CUST_ACCOUNTS','CUST');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'ATTRIBUTE11','Attribute11','Descriptive flexfield segment','','','','','44','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','HZ_CUST_ACCOUNTS','CUST');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'ATTRIBUTE12','Attribute12','Descriptive flexfield segment','','','','','45','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','HZ_CUST_ACCOUNTS','CUST');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'ATTRIBUTE13','Attribute13','Descriptive flexfield segment','','','','','46','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','HZ_CUST_ACCOUNTS','CUST');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'ATTRIBUTE14','Attribute14','Descriptive flexfield segment','','','','','47','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','HZ_CUST_ACCOUNTS','CUST');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'ATTRIBUTE15','Attribute15','Descriptive flexfield segment','','','','','48','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','HZ_CUST_ACCOUNTS','CUST');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'ATTRIBUTE16','Attribute16','Descriptive Flexfield segment','','','','','49','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','HZ_CUST_ACCOUNTS','CUST');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'ATTRIBUTE17','Attribute17','Descriptive Flexfield segment','','','','','50','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','HZ_CUST_ACCOUNTS','CUST');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'ATTRIBUTE18','Attribute18','Descriptive Flexfield segment','','','','','51','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','HZ_CUST_ACCOUNTS','CUST');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'ATTRIBUTE19','Attribute19','Descriptive Flexfield segment','','','','','52','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','HZ_CUST_ACCOUNTS','CUST');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'ATTRIBUTE20','Attribute20','Descriptive Flexfield segment','','','','','53','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','HZ_CUST_ACCOUNTS','CUST');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'ATTRIBUTE4','Attribute4','Descriptive flexfield segment','','','','','54','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','HZ_CUST_ACCOUNTS','CUST');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'ATTRIBUTE5','Attribute5','Descriptive flexfield segment','','','','','55','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','HZ_CUST_ACCOUNTS','CUST');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'ATTRIBUTE6','Attribute6','Descriptive flexfield segment','','','','','56','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','HZ_CUST_ACCOUNTS','CUST');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'ATTRIBUTE7','Attribute7','Descriptive flexfield segment','','','','','57','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','HZ_CUST_ACCOUNTS','CUST');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'ATTRIBUTE8','Attribute8','Descriptive flexfield segment','','','','','58','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','HZ_CUST_ACCOUNTS','CUST');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'ATTRIBUTE9','Attribute9','Descriptive flexfield segment','','','','','59','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','HZ_CUST_ACCOUNTS','CUST');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'ATTRIBUTE_CATEGORY','Attribute Category','Descriptive flexfield structure definition column.','','','','','60','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','HZ_CUST_ACCOUNTS','CUST');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'CREATED_BY','Created By','Standard who column - user who created this row (foreign key to FND_USER.USER_ID).','','','','','15','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','HZ_CUST_ACCOUNTS','CUST');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'CREATED_BY_MODULE','Created By Module','TCA Who column','','','','','16','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','HZ_CUST_ACCOUNTS','CUST');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'CREDIT_CLASSIFICATION_CODE','Credit Classification Code','No longer used','','','','','17','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','HZ_CUST_ACCOUNTS','CUST');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'CUSTOMER_TYPE','Customer Type','Receivables lookup code for the CUSTOMER_TYPE attribute. I for internal customers, R for revenue generating external customers.','','','','','18','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','HZ_CUST_ACCOUNTS','CUST');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'CUST_ACCOUNT_ID','Cust Account Id','Customer account identifier','','','','','61','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','HZ_CUST_ACCOUNTS','CUST');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'ORG_ID','Org Id','','','','','','66','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','HZ_CUST_ACCOUNTS','CUST');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'PARTY_ID','Party Id','A foreign key to the HZ_PARTY table.','','','','','6','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','HZ_CUST_ACCOUNTS','CUST');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'STATUS','Status','Customer status flag.  Lookup code for CODE_STATUS','','','','','19','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','HZ_CUST_ACCOUNTS','CUST');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'TAX_CODE','Tax Code','Tax Code for the Customer','','','','','69','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','HZ_CUST_ACCOUNTS','CUST');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'TAX_HEADER_LEVEL_FLAG','Tax Header Level Flag','Indicate if item is tax header or a line item. Y for records that are headers for tax purposes, N for records that are lines.','','','','','70','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','HZ_CUST_ACCOUNTS','CUST');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'TAX_ROUNDING_RULE','Tax Rounding Rule','Tax amount rounding rule','','','','','71','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','HZ_CUST_ACCOUNTS','CUST');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'ACCT_SITE#PRISM_NUMBER','Acct Site#Prism Number','Descriptive flexfield: Address Information Column Name: PRISM Number','','','','','29','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','','');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'ACCT_SITE#TAX_EXEMPT1','Acct Site#Tax Exempt','Descriptive flexfield: Address Information Column Name: Tax Exempt','','','','','30','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','','');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'ACCT_SITE#TAX_EXEMPTION_TYPE','Acct Site#Tax Exemption Type','Descriptive flexfield: Address Information Column Name: Tax Exemption Type','','','','','31','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','','');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'ADDRESS_LINE_1','Address Line 1','Address Line 1','','','','','9','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','','');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'CITY','City','City','','','','','10','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','','');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'CUSTOMER_ID','Customer Id','Customer Id','','','','','1','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','','');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'CUSTOMER_NAME','Customer Name','Customer Name','','','','','5','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','','');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'CUSTOMER_NUMBER','Customer Number','Customer Number','','','','','4','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','','');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'CUST_ACCOUNT_ID','Cust Account Id','Cust Account Id','','','','','2','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','','');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'CUST_ACCT_SITE_ID','Cust Acct Site Id','Cust Acct Site Id','','','','','3','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','','');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'CUST_STATUS','Cust Status','Cust Status','','','','','32','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','','');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'LOCATION_ID','Location Id','Location Id','','','','','33','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','','');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'ORIG_SYSTEM_REFERENCE','Orig System Reference','Orig System Reference','','','','','34','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','','');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'PARTY_ID','Party Id','Party Id','','','','','35','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','','');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'PARTY_NUMBER','Party Number','Party Number','','','','','36','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','','');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'PARTY_SITE_ID','Party Site Id','Party Site Id','','','','','37','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','','');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'PARTY_SITE_NUMBER','Party Site Number','Party Site Number','','','','','7','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','','');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'REFERENCE_USE_FLAG','Reference Use Flag','Reference Use Flag','','','','','38','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','','');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'SITE_USE','Site Use','Site Use','','','','','39','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','','');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'SITE_USE_ID','Site Use Id','Site Use Id','','','','','8','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','','');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'STATE','State','State','','','','','11','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','','');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'TAX_CODE','Tax Code','Tax Code','','','','','40','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','','');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'TAX_REFERENCE','Tax Reference','Tax Reference','','','','','41','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','','');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'ZIP_CODE','Zip Code','Zip Code','','','','','12','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','','');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'PRIMARY_PHONE_AREA_CODE','Primary Phone Area Code','The area code within a country code','','','','','64','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','HZ_PARTIES','PARTY');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'PRIMARY_PHONE_NUMBER','Primary Phone Number','A telephone number formatted in local format. The number should not include country code, area code, or extension','','','','','65','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','HZ_PARTIES','PARTY');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'PARTY_TYPE','Party Type','The party type can only be Person, Organization, Group or Relationship.','','','','','68','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','HZ_PARTIES','PARTY');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'SALES_TAX_GEOCODE','Sales Tax Geocode','US Sales Tax Jurisdiction code. Use this field to provide a Jurisdiction Code (also called as Geocode) defined by wither Vertex or Taxware. This value is passed as a ship-to locaiton jurisdiction code to the tax partner API.','','','','','13','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','HZ_LOCATIONS','LOC');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'TAX_CLASSIFICATION','Tax Classification','Classifies Bill-To site to tax group codes. Foreign key to the AR_LOOKUPS table (AR_TAX_CLASSIFICATION) used in AR_VAT_TAX (TAX_CLASSIFICATION) indexes','','','','','42','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','HZ_CUST_SITE_USES','SITE_USES');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'TAX_CODE','Tax Code','Tax code associated with this site','','','','','72','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','HZ_CUST_SITE_USES','SITE_USES');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'TAX_HEADER_LEVEL_FLAG','Tax Header Level Flag','Used by Oracle Sales Compensation','','','','','73','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','HZ_CUST_SITE_USES','SITE_USES');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'TAX_REFERENCE','Tax Reference','Taxpayer identification number','','','','','74','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','HZ_CUST_SITE_USES','SITE_USES');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'TAX_ROUNDING_RULE','Tax Rounding Rule','Tax rounding rule','','','','','75','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','HZ_CUST_SITE_USES','SITE_USES');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'SALES_TAX_INSIDE_CITY_LIMITS','Sales Tax Inside City Limits','Indicates if the location is inside the boundary of a city. Used to calculate state and local taxes in the United States. Value ''1'' is for locations inside the city limits and ''0'' is for locations outside the city limits. Defaults to null i','','','','','14','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','HZ_LOCATIONS','LOC');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'ADDRESS1','Address1','First line for address','','','','','76','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','HZ_LOCATIONS','LOC');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'ADDRESS2','Address2','Second line for address','','','','','77','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','HZ_LOCATIONS','LOC');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'ADDRESS3','Address3','Third line for address','','','','','78','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','HZ_LOCATIONS','LOC');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'ADDRESS4','Address4','Fourth line for address','','','','','79','N','','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','HZ_LOCATIONS','LOC');
xxeis.eis_rs_ins.rc( 'Customer Listing with Created By',222,'PRIMARY_SALESREP_ID','Primary Salesrep Id','Identifies a salesperson associated with a business site. Also used to default salesrep in the Transactions window. The hierarchy of defaulting would follow from Bill-To site to Ship-To site to Customer, if not a multi-org setup.','','','','','80','','Y','','','','','','','SA059956','N','N','','EIS_AR_CUST_LIST_SUMMARY_V','HZ_CUST_SITE_USES','SITE_USES');
--Inserting Report Parameters - Customer Listing with Created By
xxeis.eis_rs_ins.rp( 'Customer Listing with Created By',222,'Customer State','Customer State','STATE','IN','Customer State','','VARCHAR2','N','Y','4','','Y','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Customer Listing with Created By',222,'Customer Zip Code','Customer Zip Code','ZIP_CODE','IN','Customer Zip Code','','VARCHAR2','N','Y','5','','Y','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Customer Listing with Created By',222,'Customer Status','Customer Status','CUST_STATUS','IN','Customer Status','A','VARCHAR2','N','Y','6','','Y','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Customer Listing with Created By',222,'Customer City','Customer City','CITY','IN','Receivables_city','','VARCHAR2','N','Y','3','','Y','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Customer Listing with Created By',222,'Customer Name','Customer Name','CUSTOMER_NAME','IN','Customer Name','','VARCHAR2','N','Y','1','','Y','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Customer Listing with Created By',222,'Customer Number','Customer Number','CUSTOMER_NUMBER','IN','Customer Number','','VARCHAR2','N','Y','2','','Y','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Customer Listing with Created By',222,'Creation Date From','Standard who column - date when this row was created.','CREATION_DATE','>=','','','DATE','N','Y','7','','Y','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Customer Listing with Created By',222,'Customer Site Classification','DFF Context Code - Global Data Elements','ATTRIBUTE1','IN','','','VARCHAR2','N','Y','8','','Y','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Customer Listing with Created By',222,'Cust#Yes#Note Line #2','Descriptive flexfield: Customer Information Column Name: Note Line #2 Context: Yes','CUST#YES#NOTE_LINE_#2','IN','','','VARCHAR2','N','Y','9','','Y','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Customer Listing with Created By',222,'Creation Date To','Standard who column - date when this row was created.','','<=','','','DATE','N','Y','10','','Y','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Customer Listing with Created By',222,'Customer Type','Customer Type','CUSTOMER_TYPE','IN','','','VARCHAR2','N','Y','11','','Y','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Customer Listing with Created By',222,'Party Type','The party type can only be Person, Organization, Group or Relationship.','PARTY_TYPE','IN','','','VARCHAR2','N','Y','12','','Y','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Customer Listing with Created By',222,'Attribute7','Descriptive flexfield segment','ATTRIBUTE7','IN','','','VARCHAR2','N','Y','13','','Y','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Customer Listing with Created By',222,'Site Use','Site Use','SITE_USE','IN','','','VARCHAR2','N','Y','14','','Y','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Customer Listing with Created By',222,'Created By','Standard who column - user who created this row (foreign key to FND_USER.USER_ID).','CREATED_BY','IN','','','VARCHAR2','N','Y','15','','Y','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Customer Listing with Created By',222,'Primary Salesrep Id','Identifies a salesperson associated with a business site. Also used to default salesrep in the Transactions window. The hierarchy of defaulting would follow from Bill-To site to Ship-To site to Customer, if not a multi-org setup.','PRIMARY_SALESREP_ID','IN','ALL_USER_NAMES','','VARCHAR2','N','Y','16','','Y','CONSTANT','SA059956','Y','N','','','');
--Inserting Report Conditions - Customer Listing with Created By
xxeis.eis_rs_ins.rcn( 'Customer Listing with Created By',222,'CITY','IN',':Customer City','','','Y','3','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'Customer Listing with Created By',222,'CUSTOMER_NAME','IN',':Customer Name','','','Y','1','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'Customer Listing with Created By',222,'CUSTOMER_NUMBER','IN',':Customer Number','','','Y','2','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'Customer Listing with Created By',222,'STATE','IN',':Customer State','','','Y','4','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'Customer Listing with Created By',222,'ZIP_CODE','IN',':Customer Zip Code','','','Y','5','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'Customer Listing with Created By',222,'CUST_STATUS','IN',':Customer Status','','','Y','6','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'Customer Listing with Created By',222,'ACCT_SITE#PRINT_PRICES_ON_OR','IN',':Acct Site#Print Prices On Order','','','Y','10','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'Customer Listing with Created By',222,'SITE_USES#CUSTOMER_SITE_CLAS','IN',':Site Uses#Customer Site Classification','','','Y','18','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'Customer Listing with Created By',222,'CREATION_DATE','IN',':Creation Date','','','Y','11','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'Customer Listing with Created By',222,'ATTRIBUTE1','IN',':Customer Site Classification','','','Y','8','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'Customer Listing with Created By',222,'ATTRIBUTE1','IN',':Print Prices on Order','','','Y','15','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'Customer Listing with Created By',222,'CUST#YES#NOTE_LINE_#2','IN',':Cust#Yes#Note Line #2','','','Y','9','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'Customer Listing with Created By',222,'CREATION_DATE','>=',':Creation Date From','','','Y','7','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'Customer Listing with Created By',222,'CUSTOMER_TYPE','IN',':Customer Type','','','Y','11','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'Customer Listing with Created By',222,'PARTY_TYPE','IN',':Party Type','','','Y','12','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'Customer Listing with Created By',222,'ATTRIBUTE7','IN',':Attribute7','','','Y','13','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'Customer Listing with Created By',222,'SITE_USE','IN',':Site Use','','','Y','14','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'Customer Listing with Created By',222,'CREATED_BY','IN',':Created By','','','Y','15','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'Customer Listing with Created By',222,'PRIMARY_SALESREP_ID','IN',':Primary Salesrep Id','','','Y','16','Y','SA059956');
--Inserting Report Sorts - Customer Listing with Created By
--Inserting Report Triggers - Customer Listing with Created By
--Inserting Report Templates - Customer Listing with Created By
xxeis.eis_rs_ins.R_Tem( 'Customer Listing with Created By','Customer Listing with Created By','Seeded template for Customer Listing with Created By','','','','','','','','','','','Customer Listing with Created By.rtf','SA059956');
--Inserting Report Portals - Customer Listing with Created By
--Inserting Report Dashboards - Customer Listing with Created By
--Inserting Report Security - Customer Listing with Created By
--Inserting Report Pivots - Customer Listing with Created By
xxeis.eis_rs_ins.rpivot( 'Customer Listing with Created By',222,'Pivot','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
--Inserting Report Summary Calculation Columns For Pivot- Pivot
END;
/
set scan on define on
