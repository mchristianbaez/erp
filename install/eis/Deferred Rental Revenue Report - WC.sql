--Report Name            : Deferred Rental Revenue Report - WC
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data EIS_XXWC_OM_DEFERRED_RENTAL_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_OM_DEFERRED_RENTAL_V
xxeis.eis_rsc_ins.v( 'EIS_XXWC_OM_DEFERRED_RENTAL_V',660,'','','','','SA059956','XXEIS','Eis Xxwc Om Deferred Rental V','EXODRV','','','VIEW','US','','');
--Delete Object Columns for EIS_XXWC_OM_DEFERRED_RENTAL_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_OM_DEFERRED_RENTAL_V',660,FALSE);
--Inserting Object Columns for EIS_XXWC_OM_DEFERRED_RENTAL_V
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_DEFERRED_RENTAL_V','ORGANIZATION_ID',660,'Organization Id','ORGANIZATION_ID','','','','SA059956','NUMBER','','','Organization Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_DEFERRED_RENTAL_V','MSI_ORGANIZATION_ID',660,'Msi Organization Id','MSI_ORGANIZATION_ID','','','','SA059956','NUMBER','','','Msi Organization Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_DEFERRED_RENTAL_V','MSI_INVENTORY_ITEM_ID',660,'Msi Inventory Item Id','MSI_INVENTORY_ITEM_ID','','','','SA059956','NUMBER','','','Msi Inventory Item Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_DEFERRED_RENTAL_V','HZP_PARTY_ID',660,'Hzp Party Id','HZP_PARTY_ID','','','','SA059956','NUMBER','','','Hzp Party Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_DEFERRED_RENTAL_V','OH_HEADER_ID',660,'Oh Header Id','OH_HEADER_ID','','','','SA059956','NUMBER','','','Oh Header Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_DEFERRED_RENTAL_V','OLL_LINE_ID',660,'Oll Line Id','OLL_LINE_ID','','','','SA059956','NUMBER','','','Oll Line Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_DEFERRED_RENTAL_V','OL_LINE_ID',660,'Ol Line Id','OL_LINE_ID','','','','SA059956','NUMBER','','','Ol Line Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_DEFERRED_RENTAL_V','DEFERRAL_DAYS',660,'Deferral Days','DEFERRAL_DAYS','','','','SA059956','NUMBER','','','Deferral Days','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_DEFERRED_RENTAL_V','INVOICE_DATE',660,'Invoice Date','INVOICE_DATE','','','','SA059956','DATE','','','Invoice Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_DEFERRED_RENTAL_V','INVOICE_NUMBER',660,'Invoice Number','INVOICE_NUMBER','','','','SA059956','VARCHAR2','','','Invoice Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_DEFERRED_RENTAL_V','RENTAL_RATE',660,'Rental Rate','RENTAL_RATE','','','','SA059956','VARCHAR2','','','Rental Rate','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_DEFERRED_RENTAL_V','RENTAL_BILLING_TERMS',660,'Rental Billing Terms','RENTAL_BILLING_TERMS','','','','SA059956','VARCHAR2','','','Rental Billing Terms','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_DEFERRED_RENTAL_V','BILL_TO_CUST_PH_NO',660,'Bill To Cust Ph No','BILL_TO_CUST_PH_NO','','','','SA059956','VARCHAR2','','','Bill To Cust Ph No','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_DEFERRED_RENTAL_V','BILL_TO_CUST_NAME',660,'Bill To Cust Name','BILL_TO_CUST_NAME','','','','SA059956','VARCHAR2','','','Bill To Cust Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_DEFERRED_RENTAL_V','PRI_BILL_TO_PH',660,'Pri Bill To Ph','PRI_BILL_TO_PH','','','','SA059956','VARCHAR2','','','Pri Bill To Ph','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_DEFERRED_RENTAL_V','RENTAL_DATE',660,'Rental Date','RENTAL_DATE','','','','SA059956','DATE','','','Rental Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_DEFERRED_RENTAL_V','ATTRIBUTE12',660,'Attribute12','ATTRIBUTE12','','','','SA059956','VARCHAR2','','','Attribute12','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_DEFERRED_RENTAL_V','DAYS_ON_RENT',660,'Days On Rent','DAYS_ON_RENT','','','','SA059956','NUMBER','','','Days On Rent','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_DEFERRED_RENTAL_V','RENTAL_LOCATION',660,'Rental Location','RENTAL_LOCATION','','','','SA059956','VARCHAR2','','','Rental Location','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_DEFERRED_RENTAL_V','RENTAL_START_DATE',660,'Rental Start Date','RENTAL_START_DATE','','','','SA059956','DATE','','','Rental Start Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_DEFERRED_RENTAL_V','QUANTITY_ON_RENT',660,'Quantity On Rent','QUANTITY_ON_RENT','','','','SA059956','NUMBER','','','Quantity On Rent','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_DEFERRED_RENTAL_V','ITEM_DESCRIPTION',660,'Item Description','ITEM_DESCRIPTION','','','','SA059956','VARCHAR2','','','Item Description','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_DEFERRED_RENTAL_V','ITEM_NUMBER',660,'Item Number','ITEM_NUMBER','','','','SA059956','VARCHAR2','','','Item Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_DEFERRED_RENTAL_V','SALES_PERSON_NAME',660,'Sales Person Name','SALES_PERSON_NAME','','','','SA059956','VARCHAR2','','','Sales Person Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_DEFERRED_RENTAL_V','CUSTOMER_JOB_NAME',660,'Customer Job Name','CUSTOMER_JOB_NAME','','','','SA059956','VARCHAR2','','','Customer Job Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_DEFERRED_RENTAL_V','CUSTOMER_NAME',660,'Customer Name','CUSTOMER_NAME','','','','SA059956','VARCHAR2','','','Customer Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_DEFERRED_RENTAL_V','ORDER_TYPE',660,'Order Type','ORDER_TYPE','','','','SA059956','VARCHAR2','','','Order Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_DEFERRED_RENTAL_V','LINE_NUMBER',660,'Line Number','LINE_NUMBER','','','','SA059956','VARCHAR2','','','Line Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_DEFERRED_RENTAL_V','CREATED_BY',660,'Created By','CREATED_BY','','','','SA059956','VARCHAR2','','','Created By','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_DEFERRED_RENTAL_V','EST_RENTAL_RETURN_DATE',660,'Est Rental Return Date','EST_RENTAL_RETURN_DATE','','','','SA059956','VARCHAR2','','','Est Rental Return Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_DEFERRED_RENTAL_V','ORDER_NUMBER',660,'Order Number','ORDER_NUMBER','','','','SA059956','NUMBER','','','Order Number','','','','US');
--Inserting Object Components for EIS_XXWC_OM_DEFERRED_RENTAL_V
--Inserting Object Component Joins for EIS_XXWC_OM_DEFERRED_RENTAL_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
--There are no LOV/LOV TL entries for the report Deferred Rental Revenue Report - WC
prompt Creating Report Data for Deferred Rental Revenue Report - WC
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Deferred Rental Revenue Report - WC
xxeis.eis_rsc_utility.delete_report_rows( 'Deferred Rental Revenue Report - WC' );
--Inserting Report - Deferred Rental Revenue Report - WC
xxeis.eis_rsc_ins.r( 660,'Deferred Rental Revenue Report - WC','','Deferred Rental Revenue Report - WC','','','','SA059956','EIS_XXWC_OM_DEFERRED_RENTAL_V','Y','','','SA059956','','N','White Cap Reports','','CSV,EXCEL,','N','','','','','','N','','US','','','','');
--Inserting Report Columns - Deferred Rental Revenue Report - WC
xxeis.eis_rsc_ins.rc( 'Deferred Rental Revenue Report - WC',660,'BILL_TO_CUST_NAME','Bill To Customer Name','Bill To Cust Name','','','default','','23','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_DEFERRED_RENTAL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Deferred Rental Revenue Report - WC',660,'BILL_TO_CUST_PH_NO','Bill To Customer Phone No','Bill To Cust Ph No','','','default','','24','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_DEFERRED_RENTAL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Deferred Rental Revenue Report - WC',660,'CREATED_BY','Created By','Created By','','','default','','16','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_DEFERRED_RENTAL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Deferred Rental Revenue Report - WC',660,'CUSTOMER_JOB_NAME','Customer Job Name','Customer Job Name','','','default','','22','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_DEFERRED_RENTAL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Deferred Rental Revenue Report - WC',660,'CUSTOMER_NAME','Customer Name','Customer Name','','','default','','21','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_DEFERRED_RENTAL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Deferred Rental Revenue Report - WC',660,'DAYS_ON_RENT','Days On Rent','Days On Rent','','~~~','default','','17','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_DEFERRED_RENTAL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Deferred Rental Revenue Report - WC',660,'DEFERRAL_DAYS','Deferral Days','Deferral Days','','~~~','default','','18','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_DEFERRED_RENTAL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Deferred Rental Revenue Report - WC',660,'EST_RENTAL_RETURN_DATE','Est Rental Return Date','Est Rental Return Date','','','default','','12','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_DEFERRED_RENTAL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Deferred Rental Revenue Report - WC',660,'INVOICE_DATE','Invoice Date','Invoice Date','','','default','','7','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_DEFERRED_RENTAL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Deferred Rental Revenue Report - WC',660,'INVOICE_NUMBER','Invoice Number','Invoice Number','','','default','','6','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_DEFERRED_RENTAL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Deferred Rental Revenue Report - WC',660,'ITEM_DESCRIPTION','Item Description','Item Description','','','default','','5','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_DEFERRED_RENTAL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Deferred Rental Revenue Report - WC',660,'ITEM_NUMBER','Item Number','Item Number','','','default','','4','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_DEFERRED_RENTAL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Deferred Rental Revenue Report - WC',660,'LINE_NUMBER','Line Number','Line Number','','','default','','2','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_DEFERRED_RENTAL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Deferred Rental Revenue Report - WC',660,'ORDER_NUMBER','Order Number','Order Number','','~~~','default','','1','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_DEFERRED_RENTAL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Deferred Rental Revenue Report - WC',660,'ORDER_TYPE','Order Type','Order Type','','','default','','3','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_DEFERRED_RENTAL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Deferred Rental Revenue Report - WC',660,'PRI_BILL_TO_PH','Primary Bill To Phone','Pri Bill To Ph','','','default','','20','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_DEFERRED_RENTAL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Deferred Rental Revenue Report - WC',660,'QUANTITY_ON_RENT','Quantity On Rent','Quantity On Rent','','~~~','default','','8','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_DEFERRED_RENTAL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Deferred Rental Revenue Report - WC',660,'RENTAL_BILLING_TERMS','Rental Billing Terms','Rental Billing Terms','','','default','','14','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_DEFERRED_RENTAL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Deferred Rental Revenue Report - WC',660,'RENTAL_DATE','Rental Date','Rental Date','','','default','','11','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_DEFERRED_RENTAL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Deferred Rental Revenue Report - WC',660,'RENTAL_LOCATION','Rental Location','Rental Location','','','default','','13','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_DEFERRED_RENTAL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Deferred Rental Revenue Report - WC',660,'RENTAL_RATE','Rental Rate','Rental Rate','','','default','','9','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_DEFERRED_RENTAL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Deferred Rental Revenue Report - WC',660,'RENTAL_START_DATE','Rental Start Date','Rental Start Date','','','default','','10','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_DEFERRED_RENTAL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Deferred Rental Revenue Report - WC',660,'SALES_PERSON_NAME','Sales Person Name','Sales Person Name','','','default','','15','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_DEFERRED_RENTAL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Deferred Rental Revenue Report - WC',660,'Amount_to_Defer','Amount to Defer','Amount to Defer','NUMBER','~T~D~2','default','','19','Y','Y','','','','','','( (EXODRV.QUANTITY_ON_RENT * EXODRV.DEFERRAL_DAYS)*( EXODRV.RENTAL_RATE/28))','SA059956','N','N','','EIS_XXWC_OM_DEFERRED_RENTAL_V','','','','US','');
--Inserting Report Parameters - Deferred Rental Revenue Report - WC
xxeis.eis_rsc_ins.rp( 'Deferred Rental Revenue Report - WC',660,'Month End Close Date','Month End Close Date','','','','','DATE','N','Y','1','Y','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_OM_DEFERRED_RENTAL_V','','','US','');
--Inserting Dependent Parameters - Deferred Rental Revenue Report - WC
--Inserting Report Conditions - Deferred Rental Revenue Report - WC
xxeis.eis_rsc_ins.rcnh( 'Deferred Rental Revenue Report - WC',660,'FreeText','FREE_TEXT','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','','Y','Y','','','','and EXODRV.RENTAL_RATE >0
and  EXODRV.ATTRIBUTE12 != ''28 Day ReRental Billing''','1',660,'Deferred Rental Revenue Report - WC','FreeText');
--Inserting Report Sorts - Deferred Rental Revenue Report - WC
xxeis.eis_rsc_ins.rs( 'Deferred Rental Revenue Report - WC',660,'ORDER_NUMBER','ASC','SA059956','1','');
xxeis.eis_rsc_ins.rs( 'Deferred Rental Revenue Report - WC',660,'LINE_NUMBER','ASC','SA059956','2','');
--Inserting Report Triggers - Deferred Rental Revenue Report - WC
xxeis.eis_rsc_ins.rt( 'Deferred Rental Revenue Report - WC',660,'BEGIN
  XXEIS.eis_rs_xxwc_com_util_pkg.g_date_from:= :Month End Close Date;
END;','B','Y','SA059956','AQ');
--inserting report templates - Deferred Rental Revenue Report - WC
--Inserting Report Portals - Deferred Rental Revenue Report - WC
--inserting report dashboards - Deferred Rental Revenue Report - WC
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Deferred Rental Revenue Report - WC','660','EIS_XXWC_OM_DEFERRED_RENTAL_V','EIS_XXWC_OM_DEFERRED_RENTAL_V','N','');
--inserting report security - Deferred Rental Revenue Report - WC
xxeis.eis_rsc_ins.rsec( 'Deferred Rental Revenue Report - WC','401','','XXWC_AO_REC',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Deferred Rental Revenue Report - WC','707','','XXWC_COST_MANAGEMENT_INQ',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Deferred Rental Revenue Report - WC','401','','XXWC_INVENTORY_CONTROL_INQUIRY',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Deferred Rental Revenue Report - WC','401','','XXWC_INV_ACCOUNTANT',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Deferred Rental Revenue Report - WC','401','','HDS_INVNTRY',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Deferred Rental Revenue Report - WC','20005','','XXWC_VIEW_ALL_EIS_REPORTS',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Deferred Rental Revenue Report - WC','401','','INVENTORY',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Deferred Rental Revenue Report - WC','','10012196','',660,'SA059956','','N','');
xxeis.eis_rsc_ins.rsec( 'Deferred Rental Revenue Report - WC','','LB048272','',660,'SA059956','','N','');
--Inserting Report Pivots - Deferred Rental Revenue Report - WC
--Inserting Report   Version details- Deferred Rental Revenue Report - WC
xxeis.eis_rsc_ins.rv( 'Deferred Rental Revenue Report - WC','','Deferred Rental Revenue Report - WC','SA059956');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
