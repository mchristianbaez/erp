--Report Name            : Account Alias Transactions - WC
--Import Instructions : 1. If RTF template needs to be uploaded to report automatically then move the RTF template to physical path of APPS_DATA_FILE_DIR oracle directory. Check dba_directories view to get the physical path of oracle directory. 
--                         (If you want to use any other oracle directory other than APPS_DATA_FILE_DIR then replace APPS_DATA_FILE_DIR with your oracle directory name below.)
--                      2. Make sure XXEIS schema has read/write permissions to above used oracle directory and give full permissions to the directory at OS level.
--                      3. Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
exec xxeis.eis_rs_ins.g_rtf_dir := 'APPS_DATA_FILE_DIR' ;
prompt Creating View Data for Account Alias Transactions - WC
set scan off define off
DECLARE
BEGIN 
--Inserting View XXWC_EIS_INV_ALIAS_TRANS_V
xxeis.eis_rs_ins.v( 'XXWC_EIS_INV_ALIAS_TRANS_V',401,'','','','','MR020532','XXEIS','Xxwc Eis Inv Alias Trans V','XEIATV','','');
--Delete View Columns for XXWC_EIS_INV_ALIAS_TRANS_V
xxeis.eis_rs_utility.delete_view_rows('XXWC_EIS_INV_ALIAS_TRANS_V',401,FALSE);
--Inserting View Columns for XXWC_EIS_INV_ALIAS_TRANS_V
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','UNIT_OF_MEASURE',401,'Unit Of Measure','UNIT_OF_MEASURE','','','','MR020532','VARCHAR2','','','Unit Of Measure','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','TRANSACTION_DATE',401,'Transaction Date','TRANSACTION_DATE','','','','MR020532','DATE','','','Transaction Date','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','INV_ORG_NAME',401,'Inv Org Name','INV_ORG_NAME','','','','MR020532','VARCHAR2','','','Inv Org Name','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','GL_PERIOD',401,'Gl Period','GL_PERIOD','','','','MR020532','VARCHAR2','','','Gl Period','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','ITEM',401,'Item','ITEM','','','','MR020532','VARCHAR2','','','Item','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','QUANTITY',401,'Quantity','QUANTITY','','','','MR020532','NUMBER','','','Quantity','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','STOCK_LOCATORS',401,'Stock Locators','STOCK_LOCATORS','','','','MR020532','VARCHAR2','','','Stock Locators','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','TASK_NUMBER',401,'Task Number','TASK_NUMBER','','','','MR020532','VARCHAR2','','','Task Number','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','EXPENDITURE_TYPE',401,'Expenditure Type','EXPENDITURE_TYPE','','','','MR020532','VARCHAR2','','','Expenditure Type','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','CREATION_DATE',401,'Creation Date','CREATION_DATE','','','','MR020532','DATE','','','Creation Date','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','CREATED_BY_USER',401,'Created By User','CREATED_BY_USER','','','','MR020532','VARCHAR2','','','Created By User','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','GL_PERIOD_START_DATE',401,'Gl Period Start Date','GL_PERIOD_START_DATE','','','','MR020532','DATE','','','Gl Period Start Date','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','ITEM_DESCRIPTION',401,'Item Description','ITEM_DESCRIPTION','','','','MR020532','VARCHAR2','','','Item Description','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','ITEM_REVISION',401,'Item Revision','ITEM_REVISION','','','','MR020532','VARCHAR2','','','Item Revision','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','REASON',401,'Reason','REASON','','','','MR020532','VARCHAR2','','','Reason','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','SUBINVENTORY',401,'Subinventory','SUBINVENTORY','','','','MR020532','VARCHAR2','','','Subinventory','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','TRANSACTION_ID',401,'Transaction Id','TRANSACTION_ID','','','','MR020532','NUMBER','','','Transaction Id','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','TRANSACTION_TYPE',401,'Transaction Type','TRANSACTION_TYPE','','','','MR020532','VARCHAR2','','','Transaction Type','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','ERROR_EXPLANATION',401,'Error Explanation','ERROR_EXPLANATION','','','','MR020532','VARCHAR2','','','Error Explanation','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','TASK_NAME',401,'Task Name','TASK_NAME','','','','MR020532','VARCHAR2','','','Task Name','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','UNIT_COST',401,'Unit Cost','UNIT_COST','','~T~D~2','','MR020532','NUMBER','','','Unit Cost','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','VALUE',401,'Value','VALUE','','','','MR020532','NUMBER','','','Value','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','SEGMENT4',401,'Segment4','SEGMENT4','','','','MR020532','VARCHAR2','','','Segment4','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','SEGMENT3',401,'Segment3','SEGMENT3','','','','MR020532','VARCHAR2','','','Segment3','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','SEGMENT2',401,'Segment2','SEGMENT2','','','','MR020532','VARCHAR2','','','Segment2','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','PRODUCT',401,'Product','PRODUCT','','','','MR020532','VARCHAR2','','','Product','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','SOURCE',401,'Source','SOURCE','','','','MR020532','VARCHAR2','','','Source','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','TRANSACTION_REFERENCE',401,'Transaction Reference','TRANSACTION_REFERENCE','','','','MR020532','VARCHAR2','','','Transaction Reference','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','CAT_CLASS',401,'Cat Class','CAT_CLASS','','','','MR020532','VARCHAR2','','','Cat Class','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','ORG_CODE',401,'Org Code','ORG_CODE','','','','MR020532','VARCHAR2','','','Org Code','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','TRANSFER_TRANSACTION_ID',401,'Transfer Transaction Id','TRANSFER_TRANSACTION_ID','','','','MR020532','NUMBER','','','Transfer Transaction Id','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','TRANSFER_SUBINVENTORY',401,'Transfer Subinventory','TRANSFER_SUBINVENTORY','','','','MR020532','VARCHAR2','','','Transfer Subinventory','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','TRANSACTION_UOM',401,'Transaction Uom','TRANSACTION_UOM','','','','MR020532','VARCHAR2','','','Transaction Uom','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','TRANSACTION_SOURCE_NAME',401,'Transaction Source Name','TRANSACTION_SOURCE_NAME','','','','MR020532','VARCHAR2','','','Transaction Source Name','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','TRANSACTION_SET_ID',401,'Transaction Set Id','TRANSACTION_SET_ID','','','','MR020532','NUMBER','','','Transaction Set Id','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','OPERTION_SEQUENCE',401,'Opertion Sequence','OPERTION_SEQUENCE','','','','MR020532','NUMBER','','','Opertion Sequence','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','COMPLETION_TRANSACTION_ID',401,'Completion Transaction Id','COMPLETION_TRANSACTION_ID','','','','MR020532','NUMBER','','','Completion Transaction Id','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MOVE_TRANSACTION_ID',401,'Move Transaction Id','MOVE_TRANSACTION_ID','','','','MR020532','NUMBER','','','Move Transaction Id','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','RCV_TRANSACTION_ID',401,'Rcv Transaction Id','RCV_TRANSACTION_ID','','','','MR020532','NUMBER','','','Rcv Transaction Id','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','NUMBER_OF_CONTAINERS',401,'Number Of Containers','NUMBER_OF_CONTAINERS','','','','MR020532','NUMBER','','','Number Of Containers','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','FREIGHT_CODE',401,'Freight Code','FREIGHT_CODE','','','','MR020532','VARCHAR2','','','Freight Code','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','WAYBILL_AIRBILL',401,'Waybill Airbill','WAYBILL_AIRBILL','','','','MR020532','VARCHAR2','','','Waybill Airbill','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','SHIPMENT_NUMBER',401,'Shipment Number','SHIPMENT_NUMBER','','','','MR020532','VARCHAR2','','','Shipment Number','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','PARENT_TRANSACTION_ID',401,'Parent Transaction Id','PARENT_TRANSACTION_ID','','','','MR020532','NUMBER','','','Parent Transaction Id','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','SOURCE_LINE_ID',401,'Source Line Id','SOURCE_LINE_ID','','','','MR020532','NUMBER','','','Source Line Id','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','SUPPLIER_LOT',401,'Supplier Lot','SUPPLIER_LOT','','','','MR020532','VARCHAR2','','','Supplier Lot','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','DEPARTMENT_ID',401,'Department Id','DEPARTMENT_ID','','','','MR020532','NUMBER','','','Department Id','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','TRANSACTION_SOURCE_ID',401,'Transaction Source Id','TRANSACTION_SOURCE_ID','','','','MR020532','NUMBER','','','Transaction Source Id','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','TRANSACTION_ACTION_ID',401,'Transaction Action Id','TRANSACTION_ACTION_ID','','','','MR020532','NUMBER','','','Transaction Action Id','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MMT_TRANSACTION_SOURCE_TYPE_ID',401,'Mmt Transaction Source Type Id','MMT_TRANSACTION_SOURCE_TYPE_ID','','','','MR020532','NUMBER','','','Mmt Transaction Source Type Id','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','ACCT_PERIOD_ID',401,'Acct Period Id','ACCT_PERIOD_ID','','~T~D~2','','MR020532','NUMBER','','','Acct Period Id','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','PROJECT_ID',401,'Project Id','PROJECT_ID','','','','MR020532','NUMBER','','','Project Id','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','TASK_ID',401,'Task Id','TASK_ID','','','','MR020532','NUMBER','','','Task Id','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','INVENTORY_LOCATION_ID',401,'Inventory Location Id','INVENTORY_LOCATION_ID','','','','MR020532','NUMBER','','','Inventory Location Id','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MTT_TRANSACTION_TYPE_ID',401,'Mtt Transaction Type Id','MTT_TRANSACTION_TYPE_ID','','','','MR020532','NUMBER','','','Mtt Transaction Type Id','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','VALUED_FLAG',401,'Valued Flag','VALUED_FLAG','','','','MR020532','VARCHAR2','','','Valued Flag','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','TRANSACTION_SOURCE_TYPE_NAME',401,'Transaction Source Type Name','TRANSACTION_SOURCE_TYPE_NAME','','','','MR020532','VARCHAR2','','','Transaction Source Type Name','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','TRANSACTION_ACTION',401,'Transaction Action','TRANSACTION_ACTION','','','','MR020532','VARCHAR2','','','Transaction Action','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','REASON_ID',401,'Reason Id','REASON_ID','','','','MR020532','NUMBER','','','Reason Id','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','PROJECT_NUMBER',401,'Project Number','PROJECT_NUMBER','','','','MR020532','VARCHAR2','','','Project Number','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','PROJECT_NAME',401,'Project Name','PROJECT_NAME','','','','MR020532','VARCHAR2','','','Project Name','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','SEGMENT6',401,'Segment6','SEGMENT6','','','','MR020532','VARCHAR2','','','Segment6','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','SEGMENT5',401,'Segment5','SEGMENT5','','','','MR020532','VARCHAR2','','','Segment5','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','DISTRIBUTION_ACCOUNT',401,'Distribution Account','DISTRIBUTION_ACCOUNT','','','','MR020532','NUMBER','','','Distribution Account','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','TRANSACTION_SOURCE_TYPE_ID',401,'Transaction Source Type Id','TRANSACTION_SOURCE_TYPE_ID','','','','MR020532','NUMBER','','','Transaction Source Type Id','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','HAOU_ORGANIZATION_ID',401,'Haou Organization Id','HAOU_ORGANIZATION_ID','','','','MR020532','NUMBER','','','Haou Organization Id','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MGP_ORGANIZATION_ID',401,'Mgp Organization Id','MGP_ORGANIZATION_ID','','','','MR020532','NUMBER','','','Mgp Organization Id','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','DISPOSITION_ID',401,'Disposition Id','DISPOSITION_ID','','','','MR020532','NUMBER','','','Disposition Id','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI_ORGANIZATION_ID',401,'Msi Organization Id','MSI_ORGANIZATION_ID','','','','MR020532','NUMBER','','','Msi Organization Id','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','INVENTORY_ITEM_ID',401,'Inventory Item Id','INVENTORY_ITEM_ID','','','','MR020532','NUMBER','','','Inventory Item Id','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','LANGUAGE',401,'Language','LANGUAGE','','','','MR020532','VARCHAR2','','','Language','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSIL_ORGANIZATION_ID',401,'Msil Organization Id','MSIL_ORGANIZATION_ID','','','','MR020532','NUMBER','','','Msil Organization Id','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSIL_INVENTORY_ITEM_ID',401,'Msil Inventory Item Id','MSIL_INVENTORY_ITEM_ID','','','','MR020532','NUMBER','','','Msil Inventory Item Id','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','OAP_ORGANIZATION_ID',401,'Oap Organization Id','OAP_ORGANIZATION_ID','','','','MR020532','NUMBER','','','Oap Organization Id','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MIL_ORGANIZATION_ID',401,'Mil Organization Id','MIL_ORGANIZATION_ID','','','','MR020532','NUMBER','','','Mil Organization Id','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MP_ORGANIZATION_ID',401,'Mp Organization Id','MP_ORGANIZATION_ID','','','','MR020532','NUMBER','','','Mp Organization Id','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MGP#101#ACCOUNTALIAS',401,'Key Flex Field Column - MGP#101#ACCOUNTALIAS','MGP#101#ACCOUNTALIAS','','','','MR020532','VARCHAR2','MTL_GENERIC_DISPOSITIONS','SEGMENT1','Account Alias#101','','1015064','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MGP#101#ACCOUNTALIAS_DESC',401,'Key Flex Field Column - MGP#101#ACCOUNTALIAS_DESC','MGP#101#ACCOUNTALIAS_DESC','','','','MR020532','VARCHAR2','MTL_GENERIC_DISPOSITIONS','SEGMENT1','Account Alias#101#DESC','','1015064','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MIL#101#STOCKLOCATIONS',401,'Key Flex Field Column - MIL#101#STOCKLOCATIONS','MIL#101#STOCKLOCATIONS','','','','MR020532','VARCHAR2','MTL_ITEM_LOCATIONS','SEGMENT1','Stock Locations#101','','1015063','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MIL#101#STOCKLOCATIONS_DESC',401,'Key Flex Field Column - MIL#101#STOCKLOCATIONS_DESC','MIL#101#STOCKLOCATIONS_DESC','','','','MR020532','VARCHAR2','MTL_ITEM_LOCATIONS','SEGMENT1','Stock Locations#101#DESC','','1015063','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MMT#WCRENTALA#HOME_BRANCH',401,'Descriptive flexfield: Transaction history Column Name: Home Branch Context: WC Rental Asset Transfer','MMT#WCRentalA#Home_Branch','','','','MR020532','VARCHAR2','MTL_MATERIAL_TRANSACTIONS','ATTRIBUTE1','Mmt#Wc Rental Asset Transfer#Home Branch','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MMT#WCRENTALA#VENDOR',401,'Descriptive flexfield: Transaction history Column Name: Vendor Context: WC Rental Asset Transfer','MMT#WCRentalA#Vendor','','','','MR020532','VARCHAR2','MTL_MATERIAL_TRANSACTIONS','ATTRIBUTE10','Mmt#Wc Rental Asset Transfer#Vendor','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MMT#WCRENTALA#PO_NUMBER',401,'Descriptive flexfield: Transaction history Column Name: PO Number Context: WC Rental Asset Transfer','MMT#WCRentalA#PO_Number','','','','MR020532','VARCHAR2','MTL_MATERIAL_TRANSACTIONS','ATTRIBUTE11','Mmt#Wc Rental Asset Transfer#Po Number','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MMT#WCRENTALA#ITEM_NUMBER',401,'Descriptive flexfield: Transaction history Column Name: Item Number Context: WC Rental Asset Transfer','MMT#WCRentalA#Item_Number','','','','MR020532','VARCHAR2','MTL_MATERIAL_TRANSACTIONS','ATTRIBUTE12','Mmt#Wc Rental Asset Transfer#Item Number','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MMT#WCRENTALA#CER_NUMBER',401,'Descriptive flexfield: Transaction history Column Name: CER Number Context: WC Rental Asset Transfer','MMT#WCRentalA#CER_Number','','','','MR020532','VARCHAR2','MTL_MATERIAL_TRANSACTIONS','ATTRIBUTE13','Mmt#Wc Rental Asset Transfer#Cer Number','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MMT#WCRENTALA#MISC_ISSUE_TRA',401,'Descriptive flexfield: Transaction history Column Name: Misc Issue Transaction ID Context: WC Rental Asset Transfer','MMT#WCRentalA#Misc_Issue_Tra','','','','MR020532','VARCHAR2','MTL_MATERIAL_TRANSACTIONS','ATTRIBUTE14','Mmt#Wc Rental Asset Transfer#Misc Issue Transaction Id','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MMT#WCRENTALA#CUSTODIAN_BRAN',401,'Descriptive flexfield: Transaction history Column Name: Custodian Branch Context: WC Rental Asset Transfer','MMT#WCRentalA#Custodian_Bran','','','','MR020532','VARCHAR2','MTL_MATERIAL_TRANSACTIONS','ATTRIBUTE2','Mmt#Wc Rental Asset Transfer#Custodian Branch','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MMT#WCRENTALA#MAJOR_CATEGORY',401,'Descriptive flexfield: Transaction history Column Name: Major Category Context: WC Rental Asset Transfer','MMT#WCRentalA#Major_Category','','','','MR020532','VARCHAR2','MTL_MATERIAL_TRANSACTIONS','ATTRIBUTE3','Mmt#Wc Rental Asset Transfer#Major Category','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MMT#WCRENTALA#MINOR_CATEGORY',401,'Descriptive flexfield: Transaction history Column Name: Minor Category Context: WC Rental Asset Transfer','MMT#WCRentalA#Minor_Category','','','','MR020532','VARCHAR2','MTL_MATERIAL_TRANSACTIONS','ATTRIBUTE4','Mmt#Wc Rental Asset Transfer#Minor Category','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MMT#WCRENTALA#STATE',401,'Descriptive flexfield: Transaction history Column Name: State Context: WC Rental Asset Transfer','MMT#WCRentalA#State','','','','MR020532','VARCHAR2','MTL_MATERIAL_TRANSACTIONS','ATTRIBUTE5','Mmt#Wc Rental Asset Transfer#State','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MMT#WCRENTALA#CITY',401,'Descriptive flexfield: Transaction history Column Name: City Context: WC Rental Asset Transfer','MMT#WCRentalA#City','','','','MR020532','VARCHAR2','MTL_MATERIAL_TRANSACTIONS','ATTRIBUTE6','Mmt#Wc Rental Asset Transfer#City','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MMT#WCRENTALA#TRANSFER_TO_MA',401,'Descriptive flexfield: Transaction history Column Name: Transfer to MassAdd Context: WC Rental Asset Transfer','MMT#WCRentalA#Transfer_to_Ma','','','','MR020532','VARCHAR2','MTL_MATERIAL_TRANSACTIONS','ATTRIBUTE9','Mmt#Wc Rental Asset Transfer#Transfer To Massadd','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MMT#WCRETURNT#SUPPLIER_NUMBE',401,'Descriptive flexfield: Transaction history Column Name: Supplier Number Context: WC Return to Vendor','MMT#WCReturnt#Supplier_Numbe','','','','MR020532','VARCHAR2','MTL_MATERIAL_TRANSACTIONS','ATTRIBUTE1','Mmt#Wc Return To Vendor#Supplier Number','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MMT#WCRETURNT#RETURN_NUMBER',401,'Descriptive flexfield: Transaction history Column Name: Return Number Context: WC Return to Vendor','MMT#WCReturnt#Return_Number','','','','MR020532','VARCHAR2','MTL_MATERIAL_TRANSACTIONS','ATTRIBUTE2','Mmt#Wc Return To Vendor#Return Number','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MMT#WCRETURNT#RETURN_UNIT_PR',401,'Descriptive flexfield: Transaction history Column Name: Return Unit Price Context: WC Return to Vendor','MMT#WCReturnt#Return_Unit_Pr','','','','MR020532','VARCHAR2','MTL_MATERIAL_TRANSACTIONS','ATTRIBUTE3','Mmt#Wc Return To Vendor#Return Unit Price','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MP#FACTORY_PLANNER_DATA_DIRE',401,'Descriptive flexfield: Organization parameters Column Name: Factory Planner Data Directory','MP#Factory_Planner_Data_Dire','','','','MR020532','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE1','Mp#Factory Planner Data Directory','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MP#FRU',401,'Descriptive flexfield: Organization parameters Column Name: FRU','MP#FRU','','','','MR020532','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE10','Mp#Fru','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MP#LOCATION_NUMBER',401,'Descriptive flexfield: Organization parameters Column Name: Location Number','MP#Location_Number','','','','MR020532','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE11','Mp#Location Number','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MP#BRANCH_OPERATIONS_MANAGER',401,'Descriptive flexfield: Organization parameters Column Name: Branch Operations Manager','MP#Branch_Operations_Manager','','','','MR020532','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE13','Mp#Branch Operations Manager','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MP#DELIVER_CHARGE',401,'Descriptive flexfield: Organization parameters Column Name: Deliver Charge','MP#Deliver_Charge','','','','MR020532','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE14','Mp#Deliver Charge','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MP#FACTORY_PLANNER_EXECUTABL',401,'Descriptive flexfield: Organization parameters Column Name: Factory Planner Executable Directory','MP#Factory_Planner_Executabl','','','','MR020532','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE2','Mp#Factory Planner Executable Directory','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MP#FACTORY_PLANNER_USER',401,'Descriptive flexfield: Organization parameters Column Name: Factory Planner User','MP#Factory_Planner_User','','','','MR020532','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE3','Mp#Factory Planner User','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MP#FACTORY_PLANNER_HOST',401,'Descriptive flexfield: Organization parameters Column Name: Factory Planner Host','MP#Factory_Planner_Host','','','','MR020532','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE4','Mp#Factory Planner Host','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MP#FACTORY_PLANNER_PORT_NUMB',401,'Descriptive flexfield: Organization parameters Column Name: Factory Planner Port Number','MP#Factory_Planner_Port_Numb','','','','MR020532','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE5','Mp#Factory Planner Port Number','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MP#PRICING_ZONE',401,'Descriptive flexfield: Organization parameters Column Name: Pricing Zone','MP#Pricing_Zone','','','','MR020532','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE6','Mp#Pricing Zone','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MP#ORG_TYPE',401,'Descriptive flexfield: Organization parameters Column Name: Org Type','MP#Org_Type','','','','MR020532','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE7','Mp#Org Type','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MP#DISTRICT',401,'Descriptive flexfield: Organization parameters Column Name: District','MP#District','','','','MR020532','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE8','Mp#District','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MP#REGION',401,'Descriptive flexfield: Organization parameters Column Name: Region','MP#Region','','','','MR020532','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE9','Mp#Region','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI#HDS#LOB',401,'Descriptive flexfield: Items Column Name: LOB Context: HDS','MSI#HDS#LOB','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE1','Msi#Hds#Lob','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI#HDS#DROP_SHIPMENT_ELIGAB',401,'Descriptive flexfield: Items Column Name: Drop Shipment Eligable Context: HDS','MSI#HDS#Drop_Shipment_Eligab','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE10','Msi#Hds#Drop Shipment Eligable','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI#HDS#INVOICE_UOM',401,'Descriptive flexfield: Items Column Name: Invoice UOM Context: HDS','MSI#HDS#Invoice_UOM','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE15','Msi#Hds#Invoice Uom','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI#HDS#PRODUCT_ID',401,'Descriptive flexfield: Items Column Name: Product ID Context: HDS','MSI#HDS#Product_ID','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE2','Msi#Hds#Product Id','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI#HDS#VENDOR_PART_NUMBER',401,'Descriptive flexfield: Items Column Name: Vendor Part Number Context: HDS','MSI#HDS#Vendor_Part_Number','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE3','Msi#Hds#Vendor Part Number','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI#HDS#UNSPSC_CODE',401,'Descriptive flexfield: Items Column Name: UNSPSC Code Context: HDS','MSI#HDS#UNSPSC_Code','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE4','Msi#Hds#Unspsc Code','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI#HDS#UPC_PRIMARY',401,'Descriptive flexfield: Items Column Name: UPC Primary Context: HDS','MSI#HDS#UPC_Primary','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE5','Msi#Hds#Upc Primary','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI#HDS#SKU_DESCRIPTION',401,'Descriptive flexfield: Items Column Name: SKU Description Context: HDS','MSI#HDS#SKU_Description','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE6','Msi#Hds#Sku Description','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI#WC#CA_PROP_65',401,'Descriptive flexfield: Items Column Name: CA Prop 65 Context: WC','MSI#WC#CA_Prop_65','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE1','Msi#Wc#Ca Prop 65','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI#WC#COUNTRY_OF_ORIGIN',401,'Descriptive flexfield: Items Column Name: Country of Origin Context: WC','MSI#WC#Country_of_Origin','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE10','Msi#Wc#Country Of Origin','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI#WC#ORM_D_FLAG',401,'Descriptive flexfield: Items Column Name: ORM-D Flag Context: WC','MSI#WC#ORM_D_Flag','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE11','Msi#Wc#Orm-D Flag','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI#WC#STORE_VELOCITY',401,'Descriptive flexfield: Items Column Name: Store Velocity Context: WC','MSI#WC#Store_Velocity','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE12','Msi#Wc#Store Velocity','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI#WC#DC_VELOCITY',401,'Descriptive flexfield: Items Column Name: DC Velocity Context: WC','MSI#WC#DC_Velocity','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE13','Msi#Wc#Dc Velocity','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI#WC#YEARLY_STORE_VELOCITY',401,'Descriptive flexfield: Items Column Name: Yearly Store Velocity Context: WC','MSI#WC#Yearly_Store_Velocity','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE14','Msi#Wc#Yearly Store Velocity','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI#WC#YEARLY_DC_VELOCITY',401,'Descriptive flexfield: Items Column Name: Yearly DC Velocity Context: WC','MSI#WC#Yearly_DC_Velocity','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE15','Msi#Wc#Yearly Dc Velocity','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI#WC#PRISM_PART_NUMBER',401,'Descriptive flexfield: Items Column Name: PRISM Part Number Context: WC','MSI#WC#PRISM_Part_Number','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE16','Msi#Wc#Prism Part Number','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI#WC#HAZMAT_DESCRIPTION',401,'Descriptive flexfield: Items Column Name: Hazmat Description Context: WC','MSI#WC#Hazmat_Description','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE17','Msi#Wc#Hazmat Description','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI#WC#HAZMAT_CONTAINER',401,'Descriptive flexfield: Items Column Name: Hazmat Container Context: WC','MSI#WC#Hazmat_Container','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE18','Msi#Wc#Hazmat Container','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI#WC#GTP_INDICATOR',401,'Descriptive flexfield: Items Column Name: GTP Indicator Context: WC','MSI#WC#GTP_Indicator','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE19','Msi#Wc#Gtp Indicator','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI#WC#LAST_LEAD_TIME',401,'Descriptive flexfield: Items Column Name: Last Lead Time Context: WC','MSI#WC#Last_Lead_Time','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE2','Msi#Wc#Last Lead Time','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI#WC#AMU',401,'Descriptive flexfield: Items Column Name: AMU Context: WC','MSI#WC#AMU','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE20','Msi#Wc#Amu','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI#WC#RESERVE_STOCK',401,'Descriptive flexfield: Items Column Name: Reserve Stock Context: WC','MSI#WC#Reserve_Stock','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE21','Msi#Wc#Reserve Stock','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI#WC#TAXWARE_CODE',401,'Descriptive flexfield: Items Column Name: Taxware Code Context: WC','MSI#WC#Taxware_Code','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE22','Msi#Wc#Taxware Code','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI#WC#AVERAGE_UNITS',401,'Descriptive flexfield: Items Column Name: Average Units Context: WC','MSI#WC#Average_Units','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE25','Msi#Wc#Average Units','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI#WC#PRODUCT_CODE',401,'Descriptive flexfield: Items Column Name: Product code Context: WC','MSI#WC#Product_code','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE26','Msi#Wc#Product Code','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI#WC#IMPORT_DUTY_',401,'Descriptive flexfield: Items Column Name: Import Duty % Context: WC','MSI#WC#Import_Duty_','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE27','Msi#Wc#Import Duty %','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI#WC#KEEP_ITEM_ACTIVE',401,'Descriptive flexfield: Items Column Name: Keep Item Active Context: WC','MSI#WC#Keep_Item_Active','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE29','Msi#Wc#Keep Item Active','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI#WC#PESTICIDE_FLAG',401,'Descriptive flexfield: Items Column Name: Pesticide Flag Context: WC','MSI#WC#Pesticide_Flag','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE3','Msi#Wc#Pesticide Flag','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI#WC#CALC_LEAD_TIME',401,'Descriptive flexfield: Items Column Name: Calc Lead Time Context: WC','MSI#WC#Calc_Lead_Time','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE30','Msi#Wc#Calc Lead Time','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI#WC#VOC_GL',401,'Descriptive flexfield: Items Column Name: VOC G/L Context: WC','MSI#WC#VOC_GL','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE4','Msi#Wc#Voc G/L','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI#WC#PESTICIDE_FLAG_STATE',401,'Descriptive flexfield: Items Column Name: Pesticide Flag State Context: WC','MSI#WC#Pesticide_Flag_State','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE5','Msi#Wc#Pesticide Flag State','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI#WC#VOC_CATEGORY',401,'Descriptive flexfield: Items Column Name: VOC Category Context: WC','MSI#WC#VOC_Category','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE6','Msi#Wc#Voc Category','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI#WC#VOC_SUB_CATEGORY',401,'Descriptive flexfield: Items Column Name: VOC Sub Category Context: WC','MSI#WC#VOC_Sub_Category','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE7','Msi#Wc#Voc Sub Category','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI#WC#MSDS_#',401,'Descriptive flexfield: Items Column Name: MSDS # Context: WC','MSI#WC#MSDS_#','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE8','Msi#Wc#Msds #','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_INV_ALIAS_TRANS_V','MSI#WC#HAZMAT_PACKAGING_GROU',401,'Descriptive flexfield: Items Column Name: Hazmat Packaging Group Context: WC','MSI#WC#Hazmat_Packaging_Grou','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE9','Msi#Wc#Hazmat Packaging Group','','','');
--Inserting View Components for XXWC_EIS_INV_ALIAS_TRANS_V
xxeis.eis_rs_ins.vcomp( 'XXWC_EIS_INV_ALIAS_TRANS_V','PA_TASKS',401,'PA_TASKS','PT','PT','MR020532','MR020532','-1','User-Defined Subdivisions Of Project Work','','','','');
xxeis.eis_rs_ins.vcomp( 'XXWC_EIS_INV_ALIAS_TRANS_V','PA_PROJECTS',401,'PA_PROJECTS_ALL','PP','PP','MR020532','MR020532','-1','Information About Projects','','','','');
xxeis.eis_rs_ins.vcomp( 'XXWC_EIS_INV_ALIAS_TRANS_V','ORG_ACCT_PERIODS',401,'ORG_ACCT_PERIODS','OAP','OAP','MR020532','MR020532','-1','Organization Accounting Period Definition Table','','','','');
xxeis.eis_rs_ins.vcomp( 'XXWC_EIS_INV_ALIAS_TRANS_V','MTL_SYSTEM_ITEMS_KFV',401,'MTL_SYSTEM_ITEMS_B','MSI','MSI','MR020532','MR020532','-1','Inventory Item Definitions','','','','');
xxeis.eis_rs_ins.vcomp( 'XXWC_EIS_INV_ALIAS_TRANS_V','MTL_MATERIAL_TRANSACTIONS',401,'MTL_MATERIAL_TRANSACTIONS','MMT','MMT','MR020532','MR020532','-1','Material Transaction Table','','','','');
xxeis.eis_rs_ins.vcomp( 'XXWC_EIS_INV_ALIAS_TRANS_V','MTL_GENERIC_DISPOSITIONS',401,'MTL_GENERIC_DISPOSITIONS','MGP','MGP','MR020532','MR020532','-1','Account Alias Definition','','','','');
xxeis.eis_rs_ins.vcomp( 'XXWC_EIS_INV_ALIAS_TRANS_V','MTL_PARAMETERS',401,'MTL_PARAMETERS','MP','MP','MR020532','MR020532','-1','Inventory Control Options And Defaults','','','','');
xxeis.eis_rs_ins.vcomp( 'XXWC_EIS_INV_ALIAS_TRANS_V','MTL_TRANSACTION_REASONS',401,'MTL_TRANSACTION_REASONS','MTR','MTR','MR020532','MR020532','-1','Inventory Transaction Reasons Table','','','','');
xxeis.eis_rs_ins.vcomp( 'XXWC_EIS_INV_ALIAS_TRANS_V','HR_ORGANIZATION_UNITS',401,'HR_ALL_ORGANIZATION_UNITS','HAOU','HAOU','MR020532','MR020532','-1','HR_ALL_ORGANIZATION_UNITS','N','','','');
xxeis.eis_rs_ins.vcomp( 'XXWC_EIS_INV_ALIAS_TRANS_V','MTL_TXN_SOURCE_TYPES',401,'MTL_TXN_SOURCE_TYPES','MTS','MTS','MR020532','MR020532','-1','Valid Transaction Source Types','','','','');
xxeis.eis_rs_ins.vcomp( 'XXWC_EIS_INV_ALIAS_TRANS_V','MTL_ITEM_LOCATIONS_KFV',401,'MTL_ITEM_LOCATIONS','MIL','MIL','MR020532','MR020532','-1','Item Locations','','','','');
xxeis.eis_rs_ins.vcomp( 'XXWC_EIS_INV_ALIAS_TRANS_V','MTL_TRANSACTION_TYPES',401,'MTL_TRANSACTION_TYPES','MTT','MTT','MR020532','MR020532','-1','Inventory Transaction Types Table','','','','');
--Inserting View Component Joins for XXWC_EIS_INV_ALIAS_TRANS_V
xxeis.eis_rs_ins.vcj( 'XXWC_EIS_INV_ALIAS_TRANS_V','PA_TASKS','PT',401,'EIATV.TASK_ID','=','PT.TASK_ID(+)','','','','Y','MR020532','','');
xxeis.eis_rs_ins.vcj( 'XXWC_EIS_INV_ALIAS_TRANS_V','PA_PROJECTS','PP',401,'EIATV.PROJECT_ID','=','PP.PROJECT_ID(+)','','','','Y','MR020532','','');
xxeis.eis_rs_ins.vcj( 'XXWC_EIS_INV_ALIAS_TRANS_V','ORG_ACCT_PERIODS','OAP',401,'EIATV.OAP_ORGANIZATION_ID','=','OAP.ORGANIZATION_ID(+)','','','','Y','MR020532','','');
xxeis.eis_rs_ins.vcj( 'XXWC_EIS_INV_ALIAS_TRANS_V','ORG_ACCT_PERIODS','OAP',401,'EIATV.ACCT_PERIOD_ID','=','OAP.ACCT_PERIOD_ID(+)','','','','Y','MR020532','','');
xxeis.eis_rs_ins.vcj( 'XXWC_EIS_INV_ALIAS_TRANS_V','MTL_SYSTEM_ITEMS_KFV','MSI',401,'EIATV.INVENTORY_ITEM_ID','=','MSI.INVENTORY_ITEM_ID(+)','','','','Y','MR020532','','');
xxeis.eis_rs_ins.vcj( 'XXWC_EIS_INV_ALIAS_TRANS_V','MTL_SYSTEM_ITEMS_KFV','MSI',401,'EIATV.MSI_ORGANIZATION_ID','=','MSI.ORGANIZATION_ID(+)','','','','Y','MR020532','','');
xxeis.eis_rs_ins.vcj( 'XXWC_EIS_INV_ALIAS_TRANS_V','MTL_MATERIAL_TRANSACTIONS','MMT',401,'EIATV.TRANSACTION_ID','=','MMT.TRANSACTION_ID(+)','','','','Y','MR020532','','');
xxeis.eis_rs_ins.vcj( 'XXWC_EIS_INV_ALIAS_TRANS_V','MTL_GENERIC_DISPOSITIONS','MGP',401,'EIATV.DISPOSITION_ID','=','MGP.DISPOSITION_ID(+)','','','','Y','MR020532','','');
xxeis.eis_rs_ins.vcj( 'XXWC_EIS_INV_ALIAS_TRANS_V','MTL_GENERIC_DISPOSITIONS','MGP',401,'EIATV.MGP_ORGANIZATION_ID','=','MGP.ORGANIZATION_ID(+)','','','','Y','MR020532','','');
xxeis.eis_rs_ins.vcj( 'XXWC_EIS_INV_ALIAS_TRANS_V','MTL_PARAMETERS','MP',401,'EIATV.MP_ORGANIZATION_ID','=','MP.ORGANIZATION_ID(+)','','','','Y','MR020532','','');
xxeis.eis_rs_ins.vcj( 'XXWC_EIS_INV_ALIAS_TRANS_V','MTL_TRANSACTION_REASONS','MTR',401,'EIATV.REASON_ID','=','MTR.REASON_ID(+)','','','','Y','MR020532','','');
xxeis.eis_rs_ins.vcj( 'XXWC_EIS_INV_ALIAS_TRANS_V','HR_ORGANIZATION_UNITS','HAOU',401,'EIATV.HAOU_ORGANIZATION_ID','=','HAOU.ORGANIZATION_ID(+)','','','','Y','MR020532','','');
xxeis.eis_rs_ins.vcj( 'XXWC_EIS_INV_ALIAS_TRANS_V','MTL_TXN_SOURCE_TYPES','MTS',401,'EIATV.TRANSACTION_SOURCE_TYPE_ID','=','MTS.TRANSACTION_SOURCE_TYPE_ID(+)','','','','Y','MR020532','','');
xxeis.eis_rs_ins.vcj( 'XXWC_EIS_INV_ALIAS_TRANS_V','MTL_ITEM_LOCATIONS_KFV','MIL',401,'EIATV.INVENTORY_LOCATION_ID','=','MIL.INVENTORY_LOCATION_ID(+)','','','','','MR020532','','');
xxeis.eis_rs_ins.vcj( 'XXWC_EIS_INV_ALIAS_TRANS_V','MTL_ITEM_LOCATIONS_KFV','MIL',401,'EIATV.MIL_ORGANIZATION_ID','=','MIL.ORGANIZATION_ID(+)','','','','','MR020532','','');
xxeis.eis_rs_ins.vcj( 'XXWC_EIS_INV_ALIAS_TRANS_V','MTL_TRANSACTION_TYPES','MTT',401,'EIATV.MTT_TRANSACTION_TYPE_ID','=','MTT.TRANSACTION_TYPE_ID(+)','','','','Y','MR020532','','');
END;
/
set scan on define on
prompt Creating Report LOV Data for Account Alias Transactions - WC
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Account Alias Transactions - WC
xxeis.eis_rs_ins.lov( 401,'SELECT DISTINCT concatenated_segments Item_Category,
                description
           FROM mtl_categories_kfv
           order by concatenated_segments','','EIS_INV_ITEM_CATEGORIES_LOV','Item Categories','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 401,'SELECT DISTINCT concatenated_segments item, description FROM mtl_system_items_kfv msi,
                org_organization_definitions ood
          WHERE msi.organization_id = ood.organization_id
            AND ood.operating_unit = fnd_profile.VALUE (''ORG_ID'')
       ORDER BY concatenated_segments','','EIS_INV_ITEM_LOV','List of all inventory items.','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'select SECONDARY_INVENTORY_NAME SUB_INVENTORY,HAOU.NAME ORGANIZATION_NAME
from MTL_SECONDARY_INVENTORIES MSI,
     HR_ALL_ORGANIZATION_UNITS HAOU
WHERE 1=1
AND MSI.ORGANIZATION_ID = HAOU.organization_id','','EIS_INV_SUBINVENTORY_LOV','List of All SubInventories.','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'select user_name from fnd_user','','EIS_INV_USER','List of All FND users','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 401,'select TRANSACTION_TYPE_NAME from MTL_TRANSACTION_TYPES
where transaction_source_type_id=6
order by TRANSACTION_TYPE_NAME','','INV_TRANSACTION_TYPES_ACCOUNT_ALIAS','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( '','SELECT DISTINCT SEGMENT1 SOURCE FROM mtl_generic_dispositions','','XX EIS INV SOURCE TYPE','LOV To Display Source Type','ANONYMOUS',NULL,'Y','','');
xxeis.eis_rs_ins.lov( '','SELECT ORGANIZATION_CODE WAREHOUSE,ORGANIZATION_NAME 
FROM ORG_ORGANIZATION_DEFINITIONS OOD 
WHERE SYSDATE < NVL(OOD.DISABLE_DATE,SYSDATE+1) AND EXISTS (SELECT 1 FROM XXEIS.EIS_ORG_ACCESS_V  WHERE organization_id = ood.organization_id )
UNION
SELECT ''All'', ''All Organizations'' FROM DUAL','','XXWC All ORG LIST','XXWC All ORG LIST','ANONYMOUS',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Account Alias Transactions - WC
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Account Alias Transactions - WC
xxeis.eis_rs_utility.delete_report_rows( 'Account Alias Transactions - WC' );
--Inserting Report - Account Alias Transactions - WC
xxeis.eis_rs_ins.r( 401,'Account Alias Transactions - WC','','Account Alias Transactions Report to display account codes','','','','MR020532','XXWC_EIS_INV_ALIAS_TRANS_V','Y','','','MR020532','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - Account Alias Transactions - WC
xxeis.eis_rs_ins.rc( 'Account Alias Transactions - WC',401,'CREATED_BY_USER','Created By User','Created By User','','','','','18','N','','','','','','','','MR020532','N','N','','XXWC_EIS_INV_ALIAS_TRANS_V','','');
xxeis.eis_rs_ins.rc( 'Account Alias Transactions - WC',401,'CREATION_DATE','Creation Date','Creation Date','','','','','19','N','','','','','','','','MR020532','N','N','','XXWC_EIS_INV_ALIAS_TRANS_V','','');
xxeis.eis_rs_ins.rc( 'Account Alias Transactions - WC',401,'ERROR_EXPLANATION','Error Explanation','Error Explanation','','','','','20','N','','','','','','','','MR020532','N','N','','XXWC_EIS_INV_ALIAS_TRANS_V','','');
xxeis.eis_rs_ins.rc( 'Account Alias Transactions - WC',401,'EXPENDITURE_TYPE','Expenditure Type','Expenditure Type','','','','','21','N','','','','','','','','MR020532','N','N','','XXWC_EIS_INV_ALIAS_TRANS_V','','');
xxeis.eis_rs_ins.rc( 'Account Alias Transactions - WC',401,'GL_PERIOD','Gl Period','Gl Period','','','','','22','N','','','','','','','','MR020532','N','N','','XXWC_EIS_INV_ALIAS_TRANS_V','','');
xxeis.eis_rs_ins.rc( 'Account Alias Transactions - WC',401,'GL_PERIOD_START_DATE','Gl Period Start Date','Gl Period Start Date','','','','','23','N','','','','','','','','MR020532','N','N','','XXWC_EIS_INV_ALIAS_TRANS_V','','');
xxeis.eis_rs_ins.rc( 'Account Alias Transactions - WC',401,'INV_ORG_NAME','Inv Org Name','Inv Org Name','','','','','1','N','','','','','','','','MR020532','N','N','','XXWC_EIS_INV_ALIAS_TRANS_V','','');
xxeis.eis_rs_ins.rc( 'Account Alias Transactions - WC',401,'ITEM','Item','Item','','','','','2','N','','','','','','','','MR020532','N','N','','XXWC_EIS_INV_ALIAS_TRANS_V','','');
xxeis.eis_rs_ins.rc( 'Account Alias Transactions - WC',401,'ITEM_DESCRIPTION','Item Description','Item Description','','','','','3','N','','','','','','','','MR020532','N','N','','XXWC_EIS_INV_ALIAS_TRANS_V','','');
xxeis.eis_rs_ins.rc( 'Account Alias Transactions - WC',401,'ITEM_REVISION','Item Revision','Item Revision','','','','','29','N','','','','','','','','MR020532','N','N','','XXWC_EIS_INV_ALIAS_TRANS_V','','');
xxeis.eis_rs_ins.rc( 'Account Alias Transactions - WC',401,'QUANTITY','Quantity','Quantity','','~T~D~2','','','5','N','','','','','','','','MR020532','N','N','','XXWC_EIS_INV_ALIAS_TRANS_V','','');
xxeis.eis_rs_ins.rc( 'Account Alias Transactions - WC',401,'REASON','Reason','Reason','','','','','24','N','','','','','','','','MR020532','N','N','','XXWC_EIS_INV_ALIAS_TRANS_V','','');
xxeis.eis_rs_ins.rc( 'Account Alias Transactions - WC',401,'SEGMENT2','Location','Segment2','','','','','9','N','','','','','','','','MR020532','N','N','','XXWC_EIS_INV_ALIAS_TRANS_V','','');
xxeis.eis_rs_ins.rc( 'Account Alias Transactions - WC',401,'SEGMENT3','Account','Segment3','','','','','10','N','','','','','','','','MR020532','N','N','','XXWC_EIS_INV_ALIAS_TRANS_V','','');
xxeis.eis_rs_ins.rc( 'Account Alias Transactions - WC',401,'SEGMENT4','Cost Center','Segment4','','','','','11','N','','','','','','','','MR020532','N','N','','XXWC_EIS_INV_ALIAS_TRANS_V','','');
xxeis.eis_rs_ins.rc( 'Account Alias Transactions - WC',401,'STOCK_LOCATORS','Stock Locators','Stock Locators','','','','','12','N','','','','','','','','MR020532','N','N','','XXWC_EIS_INV_ALIAS_TRANS_V','','');
xxeis.eis_rs_ins.rc( 'Account Alias Transactions - WC',401,'SUBINVENTORY','Subinventory','Subinventory','','','','','16','N','','','','','','','','MR020532','N','N','','XXWC_EIS_INV_ALIAS_TRANS_V','','');
xxeis.eis_rs_ins.rc( 'Account Alias Transactions - WC',401,'TASK_NAME','Task Name','Task Name','','','','','25','N','','','','','','','','MR020532','N','N','','XXWC_EIS_INV_ALIAS_TRANS_V','','');
xxeis.eis_rs_ins.rc( 'Account Alias Transactions - WC',401,'TASK_NUMBER','Task Number','Task Number','','','','','26','N','','','','','','','','MR020532','N','N','','XXWC_EIS_INV_ALIAS_TRANS_V','','');
xxeis.eis_rs_ins.rc( 'Account Alias Transactions - WC',401,'TRANSACTION_DATE','Transaction Date','Transaction Date','','','','','27','N','','','','','','','','MR020532','N','N','','XXWC_EIS_INV_ALIAS_TRANS_V','','');
xxeis.eis_rs_ins.rc( 'Account Alias Transactions - WC',401,'TRANSACTION_ID','Transaction Id','Transaction Id','','','','','28','N','','','','','','','','MR020532','N','N','','XXWC_EIS_INV_ALIAS_TRANS_V','','');
xxeis.eis_rs_ins.rc( 'Account Alias Transactions - WC',401,'TRANSACTION_TYPE','Transaction Type','Transaction Type','','','','','17','N','','','','','','','','MR020532','N','N','','XXWC_EIS_INV_ALIAS_TRANS_V','','');
xxeis.eis_rs_ins.rc( 'Account Alias Transactions - WC',401,'UNIT_COST','Unit Cost','Unit Cost','','','','','13','N','','','','','','','','MR020532','N','N','','XXWC_EIS_INV_ALIAS_TRANS_V','','');
xxeis.eis_rs_ins.rc( 'Account Alias Transactions - WC',401,'UNIT_OF_MEASURE','Unit Of Measure','Unit Of Measure','','','','','14','N','','','','','','','','MR020532','N','N','','XXWC_EIS_INV_ALIAS_TRANS_V','','');
xxeis.eis_rs_ins.rc( 'Account Alias Transactions - WC',401,'VALUE','Value','Value','','','','','15','N','','','','','','','','MR020532','N','N','','XXWC_EIS_INV_ALIAS_TRANS_V','','');
xxeis.eis_rs_ins.rc( 'Account Alias Transactions - WC',401,'SOURCE','Source','Source','','','','','7','N','','','','','','','','MR020532','N','N','','XXWC_EIS_INV_ALIAS_TRANS_V','','');
xxeis.eis_rs_ins.rc( 'Account Alias Transactions - WC',401,'PRODUCT','Product','Product','','','','','8','N','','','','','','','','MR020532','N','N','','XXWC_EIS_INV_ALIAS_TRANS_V','','');
xxeis.eis_rs_ins.rc( 'Account Alias Transactions - WC',401,'CAT_CLASS','Cat Class','Cat Class','','','','','4','N','','','','','','','','MR020532','N','N','','XXWC_EIS_INV_ALIAS_TRANS_V','','');
xxeis.eis_rs_ins.rc( 'Account Alias Transactions - WC',401,'TRANSACTION_REFERENCE','Transaction Reference','Transaction Reference','','','','','6','N','','','','','','','','MR020532','N','N','','XXWC_EIS_INV_ALIAS_TRANS_V','','');
--Inserting Report Parameters - Account Alias Transactions - WC
xxeis.eis_rs_ins.rp( 'Account Alias Transactions - WC',401,'Created By User','Created By User','CREATED_BY_USER','IN','EIS_INV_USER','','VARCHAR2','N','Y','5','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Account Alias Transactions - WC',401,'Organization','Organization','ORG_CODE','IN','XXWC All ORG LIST','','VARCHAR2','Y','Y','1','','N','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Account Alias Transactions - WC',401,'Inventory Item','Inventory Item','ITEM','IN','EIS_INV_ITEM_LOV','','VARCHAR2','N','Y','3','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Account Alias Transactions - WC',401,'Subinventory','Subinventory','SUBINVENTORY','IN','EIS_INV_SUBINVENTORY_LOV','','VARCHAR2','N','Y','2','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Account Alias Transactions - WC',401,'Transaction Type','Transaction Type','TRANSACTION_TYPE','IN','INV_TRANSACTION_TYPES_ACCOUNT_ALIAS','','VARCHAR2','N','Y','4','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Account Alias Transactions - WC',401,'Transaction Start Date','Transaction Start Date','TRANSACTION_DATE','>=','','','DATE','N','Y','6','','Y','CONSTANT','MR020532','Y','N','','Start Date','');
xxeis.eis_rs_ins.rp( 'Account Alias Transactions - WC',401,'Transaction End Date','Transaction End Date','TRANSACTION_DATE','<=','','','DATE','N','Y','7','','Y','CONSTANT','MR020532','Y','N','','End Date','');
xxeis.eis_rs_ins.rp( 'Account Alias Transactions - WC',401,'Cat Class','Cat Class','CAT_CLASS','IN','EIS_INV_ITEM_CATEGORIES_LOV','','VARCHAR2','N','Y','8','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Account Alias Transactions - WC',401,'Source','Source','SOURCE','IN','XX EIS INV SOURCE TYPE','','VARCHAR2','N','Y','9','','Y','CONSTANT','MR020532','Y','N','','','');
--Inserting Report Conditions - Account Alias Transactions - WC
xxeis.eis_rs_ins.rcn( 'Account Alias Transactions - WC',401,'CREATED_BY_USER','IN',':Created By User','','','Y','6','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Account Alias Transactions - WC',401,'SUBINVENTORY','IN',':Subinventory','','','Y','2','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Account Alias Transactions - WC',401,'TRANSACTION_TYPE','IN',':Transaction Type','','','Y','5','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Account Alias Transactions - WC',401,'TRANSACTION_DATE','>=',':Transaction Start Date','','','Y','7','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Account Alias Transactions - WC',401,'TRANSACTION_DATE','<=',':Transaction End Date','','','Y','8','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Account Alias Transactions - WC',401,'ITEM','IN',':Inventory Item','','','Y','3','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Account Alias Transactions - WC',401,'ORG_CODE','IN',':Organization','','','N','4','N','MR020532');
xxeis.eis_rs_ins.rcn( 'Account Alias Transactions - WC',401,'CAT_CLASS','IN',':Cat Class','','','Y','9','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Account Alias Transactions - WC',401,'SOURCE','IN',':Source','','','Y','10','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Account Alias Transactions - WC',401,'','','','','AND ( ''All'' IN (:Organization) OR (ORG_CODE IN (:Organization)))','Y','0','','MR020532');
--Inserting Report Sorts - Account Alias Transactions - WC
xxeis.eis_rs_ins.rs( 'Account Alias Transactions - WC',401,'ITEM','ASC','MR020532','','');
xxeis.eis_rs_ins.rs( 'Account Alias Transactions - WC',401,'TRANSACTION_TYPE','ASC','MR020532','','');
--Inserting Report Triggers - Account Alias Transactions - WC
--Inserting Report Templates - Account Alias Transactions - WC
xxeis.eis_rs_ins.R_Tem( 'Account Alias Transactions - WC','Account Alias Transactions - WC','Seeded template for Account Alias Transactions - WC','','','','','','','','','','','Account Alias Transactions - WC.rtf','MR020532');
--Inserting Report Portals - Account Alias Transactions - WC
--Inserting Report Dashboards - Account Alias Transactions - WC
--Inserting Report Security - Account Alias Transactions - WC
xxeis.eis_rs_ins.rsec( 'Account Alias Transactions - WC','401','','50865',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Account Alias Transactions - WC','401','','50862',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Account Alias Transactions - WC','401','','50864',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Account Alias Transactions - WC','401','','50866',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Account Alias Transactions - WC','401','','50848',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Account Alias Transactions - WC','401','','50868',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Account Alias Transactions - WC','401','','50849',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Account Alias Transactions - WC','401','','50867',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Account Alias Transactions - WC','660','','50871',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Account Alias Transactions - WC','401','','50851',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Account Alias Transactions - WC','401','','50619',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Account Alias Transactions - WC','20005','','50900',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Account Alias Transactions - WC','401','','51004',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Account Alias Transactions - WC','401','','50882',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Account Alias Transactions - WC','401','','50883',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Account Alias Transactions - WC','401','','50981',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Account Alias Transactions - WC','401','','50855',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Account Alias Transactions - WC','401','','50884',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Account Alias Transactions - WC','401','','50990',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Account Alias Transactions - WC','660','','50886',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Account Alias Transactions - WC','201','','50892',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Account Alias Transactions - WC','','SG019472','',401,'MR020532','','');
--Inserting Report Pivots - Account Alias Transactions - WC
xxeis.eis_rs_ins.rpivot( 'Account Alias Transactions - WC',401,'Pivot','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rs_ins.rpivot_dtls( 'Account Alias Transactions - WC',401,'Pivot','ITEM_DESCRIPTION','PAGE_FIELD','','','','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Account Alias Transactions - WC',401,'Pivot','QUANTITY','DATA_FIELD','SUM','','1','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Account Alias Transactions - WC',401,'Pivot','CREATED_BY_USER','PAGE_FIELD','','','','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Account Alias Transactions - WC',401,'Pivot','INV_ORG_NAME','ROW_FIELD','','','1','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Account Alias Transactions - WC',401,'Pivot','ITEM','ROW_FIELD','','','3','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Account Alias Transactions - WC',401,'Pivot','SUBINVENTORY','ROW_FIELD','','','2','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Account Alias Transactions - WC',401,'Pivot','VALUE','DATA_FIELD','SUM','','2','1','xlNormal');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
END;
/
set scan on define on
