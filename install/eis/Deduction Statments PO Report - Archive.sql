--Report Name            : Deduction Statments PO Report - Archive
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View XXEIS_2106497_BSGOXA_V
prompt Table type Objects cannot be imported
set scan off define off
prompt Creating Object APPS.XXEIS_2106497_BSGOXA_V
set scan off define off
DECLARE
mod_exist varchar2(1);
stmt CLOB;
v_objlength INTEGER := 0;
v_offset INTEGER := 1;
v_sql_long DBMS_SQL.varchar2s;
v_sql_long_count INTEGER := 1;
c NUMBER;
r NUMBER;
l_stmt varchar2(32000);
l_view_cur pls_integer;
l_view_rows NUMBER;
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(682);
IF mod_exist = 'Y' THEN 
dbms_lob.createtemporary(stmt,TRUE);
dbms_lob.write( lob_loc   => stmt,amount    => length('create or replace view APPS.XXEIS_2106497_BSGOXA_V
 ("MVID","VENDOR","LOB","BU","OFFER_CODE","OFFER_NAME","STATUS_CODE","REBATE_TYPE","PO_NUMBER","PAY_TO_VENDOR_CODE","BU_BRANCH_CODE","LOCATION_SEGMENT","PRODUCT","DATE_ORDERED","RESALE_DATE_CREATED","OFU_GL_DATE","GL_CALENDAR_MONTH","AGREEMENT_YEAR","TOTAL_RECEIPTS","REBATES") as '),offset    => 1, BUFFER    => 'create or replace view APPS.XXEIS_2106497_BSGOXA_V
 ("MVID","VENDOR","LOB","BU","OFFER_CODE","OFFER_NAME","STATUS_CODE","REBATE_TYPE","PO_NUMBER","PAY_TO_VENDOR_CODE","BU_BRANCH_CODE","LOCATION_SEGMENT","PRODUCT","DATE_ORDERED","RESALE_DATE_CREATED","OFU_GL_DATE","GL_CALENDAR_MONTH","AGREEMENT_YEAR","TOTAL_RECEIPTS","REBATES") as ');
l_stmt :=  'SELECT 
  B.OFU_MVID MVID,
  c.party_name VENDOR,
  B.BILL_TO_PARTY_NAME_LOB LOB,
  b.resale_line_attribute2 BU,
  B.OFFER_CODE,
  QLHV.DESCRIPTION OFFER_NAME,
  OO.STATUS_CODE,
  decode(b.activity_media_name,''Coop'',''COOP'',''Coop Minimum'',''COOP'',''REBATE'') Rebate_Type,
  po_number PO_NUMBER,
  B.RESALE_LINE_ATTRIBUTE1 PAY_TO_VENDOR_CODE,
  B.RESALE_LINE_ATTRIBUTE6 BU_BRANCH_CODE,
  B.RESALE_LINE_ATTRIBUTE12 LOCATION_SEGMENT,
  B.RESALE_LINE_ATTRIBUTE11 PRODUCT,
  B.DATE_ORDERED,
  B.RESALE_DATE_CREATED,
  B.OFU_GL_DATE OFU_GL_DATE,
  TO_CHAR(B.OFU_GL_DATE,''Mon-YYYY'') GL_Calendar_MONTH,
  b.calendar_year agreement_year,
  SUM(CASE WHEN UTILIZATION_TYPE = ''ACCRUAL'' THEN SELLING_PRICE*QUANTITY ELSE 0 end) TOTAL_RECEIPTS,
  SUM(UTIL_ACCTD_AMOUNT) REBATES

FROM  
  xxcus.xxcus_ozf_xla_accruals_archive B,
  XXCUS.XXCUS_REBATE_CUSTOMERS C,
  ozf.ozf_offers oo,
  APPS.QP_LIST_HEADERS_VL qlhv
WHERE 1=1
AND C.CUSTOMER_ID=B.OFU_CUST_ACCOUNT_ID
AND QLHV.LIST_HEADER_ID=B.QP_LIST_HEADER_ID
AND OO.OFFER_CODE =  B.OFFER_CODE
GROUP BY 
    B.OFU_MVID,
  c.party_name ,
  B.BILL_TO_PARTY_NAME_LOB ,
  b.resale_line_attribute2,
  B.OFFER_CODE,
  QLHV.DESCRIPTION ,
  OO.STATUS_CODE,
  decode(b.activity_media_name,''Coop'',''COOP'',''Coop Minimum'',''COOP'',''REBATE'') ,
  PO_NUMBER ,
  B.RESALE_LINE_ATTRIBUTE1,
  B.RESALE_LINE_ATTRIBUTE6 ,
  B.RESALE_LINE_ATTRIBUTE12 ,
  B.RESALE_LINE_ATTRIBUTE11 ,
  B.DATE_ORDERED,
  B.RESALE_DATE_CREATED,
  B.OFU_GL_DATE ,
  TO_CHAR(B.OFU_GL_DATE,''Mon-YYYY''),
  B.CALENDAR_YEAR

';
dbms_lob.APPEND( dest_lob  => stmt,      src_lob   => l_stmt );
v_objlength := DBMS_LOB.getlength (stmt);
IF v_objlength <= 32000 THEN 
EXECUTE IMMEDIATE to_char(stmt);
ELSE
v_offset := 1; 
WHILE v_offset <= v_objlength
LOOP
-- each record is 256 bytes from the LOB
v_sql_long (v_sql_long_count) := DBMS_LOB.SUBSTR (stmt, 256, v_offset);
-- increment the record count
v_sql_long_count := v_sql_long_count + 1;
-- figure out the new offset
v_offset := v_offset + 256;
END LOOP;
BEGIN
  -- open cursor
 c := DBMS_SQL.open_cursor;
 -- parse VARCHAR2S table
 DBMS_SQL.parse (c, v_sql_long, 1, v_sql_long_count - 1, FALSE, 1);
 -- Execute the cursor
r := DBMS_SQL.EXECUTE (c);
-- Close the cursor
 DBMS_SQL.close_cursor (c);
 END;
END IF;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 682');
END IF;
END;
/
prompt Creating Object Data XXEIS_2106497_BSGOXA_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(682);
IF mod_exist = 'Y' THEN 
--Inserting Object XXEIS_2106497_BSGOXA_V
xxeis.eis_rsc_ins.v( 'XXEIS_2106497_BSGOXA_V',682,'Paste SQL View for PO Deduction Summary Report - Archive','1.0','','','KP059700','APPS','PO Deduction Summary Report - Archive View','X2BV','','','VIEW','US','','','');
--Delete Object Columns for XXEIS_2106497_BSGOXA_V
xxeis.eis_rsc_utility.delete_view_rows('XXEIS_2106497_BSGOXA_V',682,FALSE);
--Inserting Object Columns for XXEIS_2106497_BSGOXA_V
xxeis.eis_rsc_ins.vc( 'XXEIS_2106497_BSGOXA_V','MVID',682,'','','','','','KP059700','VARCHAR2','','','Mvid','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_2106497_BSGOXA_V','VENDOR',682,'','','','','','KP059700','VARCHAR2','','','Vendor','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_2106497_BSGOXA_V','LOB',682,'','','','','','KP059700','VARCHAR2','','','Lob','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_2106497_BSGOXA_V','BU',682,'','','','','','KP059700','VARCHAR2','','','Bu','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_2106497_BSGOXA_V','OFFER_CODE',682,'','','','','','KP059700','VARCHAR2','','','Offer Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_2106497_BSGOXA_V','OFFER_NAME',682,'','','','','','KP059700','VARCHAR2','','','Offer Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_2106497_BSGOXA_V','STATUS_CODE',682,'','','','','','KP059700','VARCHAR2','','','Status Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_2106497_BSGOXA_V','REBATE_TYPE',682,'','','','','','KP059700','VARCHAR2','','','Rebate Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_2106497_BSGOXA_V','PO_NUMBER',682,'','','','','','KP059700','VARCHAR2','','','Po Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_2106497_BSGOXA_V','PAY_TO_VENDOR_CODE',682,'','','','','','KP059700','VARCHAR2','','','Pay To Vendor Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_2106497_BSGOXA_V','BU_BRANCH_CODE',682,'','','','','','KP059700','VARCHAR2','','','Bu Branch Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_2106497_BSGOXA_V','LOCATION_SEGMENT',682,'','','','','','KP059700','VARCHAR2','','','Location Segment','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_2106497_BSGOXA_V','PRODUCT',682,'','','','','','KP059700','VARCHAR2','','','Product','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_2106497_BSGOXA_V','DATE_ORDERED',682,'','','','','','KP059700','DATE','','','Date Ordered','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_2106497_BSGOXA_V','RESALE_DATE_CREATED',682,'','','','','','KP059700','DATE','','','Resale Date Created','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_2106497_BSGOXA_V','OFU_GL_DATE',682,'','','','','','KP059700','DATE','','','Ofu Gl Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_2106497_BSGOXA_V','GL_CALENDAR_MONTH',682,'','','','','','KP059700','VARCHAR2','','','Gl Calendar Month','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_2106497_BSGOXA_V','AGREEMENT_YEAR',682,'','','','','','KP059700','VARCHAR2','','','Agreement Year','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_2106497_BSGOXA_V','TOTAL_RECEIPTS',682,'','','','~T~D~2','','KP059700','NUMBER','','','Total Receipts','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_2106497_BSGOXA_V','REBATES',682,'','','','','','KP059700','NUMBER','','','Rebates','','','','US','');
--Inserting Object Components for XXEIS_2106497_BSGOXA_V
--Inserting Object Component Joins for XXEIS_2106497_BSGOXA_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 682');
END IF;
END;
/
prompt Creating Report LOV Data for Deduction Statments PO Report - Archive
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(682);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - Deduction Statments PO Report - Archive
xxeis.eis_rsc_ins.lov( 682,'select distinct nvl(attribute7, ''NULL'') agreement_year
from apps.QP_LIST_HEADERS_VL','','LOV AGREEMENT_YEAR','LOV attribute7 from apps.QP_LIST_HEADERS_VL  table is the agreement year','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 682,'select distinct party_name
from ar.HZ_PARTIES
where ATTRIBUTE1 = ''HDS_MVID''','','LOV VENDOR_NAME','VENDOR NAME FROM ar.HZ_PARTIES
','ID020048',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 682,'select distinct name
from ozf.OZF_TIME_ENT_PERIOD','','LOV PERIOD','LOV NAME FROM ozf.OZF_TIME_ENT_PERIOD is the Period','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 682,'select distinct description from apps.qp_list_headers_vl','','HDS AGREEMENT_NAME','select distinct description from apps.qp_list_headers_vl','DH052844',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 682');
END IF;
END;
/
prompt Creating Report Data for Deduction Statments PO Report - Archive
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(682);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Deduction Statments PO Report - Archive
xxeis.eis_rsc_utility.delete_report_rows( 'Deduction Statments PO Report - Archive',682 );
--Inserting Report - Deduction Statments PO Report - Archive
xxeis.eis_rsc_ins.r( 682,'Deduction Statments PO Report - Archive','','YTD deduction statements that show deductions taken and the purchase bases of the deductions. This report is being pulled from xla archival table for period which is older than 2 caldenar year.','','','','KP059700','XXEIS_2106497_BSGOXA_V','Y','','SELECT 
  B.OFU_MVID MVID,
  c.party_name VENDOR,
  B.BILL_TO_PARTY_NAME_LOB LOB,
  b.resale_line_attribute2 BU,
  B.OFFER_CODE,
  QLHV.DESCRIPTION OFFER_NAME,
  OO.STATUS_CODE,
  decode(b.activity_media_name,''Coop'',''COOP'',''Coop Minimum'',''COOP'',''REBATE'') Rebate_Type,
  po_number PO_NUMBER,
  B.RESALE_LINE_ATTRIBUTE1 PAY_TO_VENDOR_CODE,
  B.RESALE_LINE_ATTRIBUTE6 BU_BRANCH_CODE,
  B.RESALE_LINE_ATTRIBUTE12 LOCATION_SEGMENT,
  B.RESALE_LINE_ATTRIBUTE11 PRODUCT,
  B.DATE_ORDERED,
  B.RESALE_DATE_CREATED,
  B.OFU_GL_DATE OFU_GL_DATE,
  TO_CHAR(B.OFU_GL_DATE,''Mon-YYYY'') GL_Calendar_MONTH,
  b.calendar_year agreement_year,
  SUM(CASE WHEN UTILIZATION_TYPE = ''ACCRUAL'' THEN SELLING_PRICE*QUANTITY ELSE 0 end) TOTAL_RECEIPTS,
  SUM(UTIL_ACCTD_AMOUNT) REBATES

FROM  
  xxcus.xxcus_ozf_xla_accruals_archive B,
  XXCUS.XXCUS_REBATE_CUSTOMERS C,
  ozf.ozf_offers oo,
  APPS.QP_LIST_HEADERS_VL qlhv
WHERE 1=1
AND C.CUSTOMER_ID=B.OFU_CUST_ACCOUNT_ID
AND QLHV.LIST_HEADER_ID=B.QP_LIST_HEADER_ID
AND OO.OFFER_CODE =  B.OFFER_CODE
GROUP BY 
    B.OFU_MVID,
  c.party_name ,
  B.BILL_TO_PARTY_NAME_LOB ,
  b.resale_line_attribute2,
  B.OFFER_CODE,
  QLHV.DESCRIPTION ,
  OO.STATUS_CODE,
  decode(b.activity_media_name,''Coop'',''COOP'',''Coop Minimum'',''COOP'',''REBATE'') ,
  PO_NUMBER ,
  B.RESALE_LINE_ATTRIBUTE1,
  B.RESALE_LINE_ATTRIBUTE6 ,
  B.RESALE_LINE_ATTRIBUTE12 ,
  B.RESALE_LINE_ATTRIBUTE11 ,
  B.DATE_ORDERED,
  B.RESALE_DATE_CREATED,
  B.OFU_GL_DATE ,
  TO_CHAR(B.OFU_GL_DATE,''Mon-YYYY''),
  B.CALENDAR_YEAR
','KP059700','','N','PAYMENTS ','','EXCEL,','','','','','','','','APPS','US','','','','','','','','','','','','','','','','');
--Inserting Report Columns - Deduction Statments PO Report - Archive
xxeis.eis_rsc_ins.rc( 'Deduction Statments PO Report - Archive',682,'DATE_ORDERED','Date Ordered','','','','default','','9','N','','','','','','','','KP059700','N','N','','XXEIS_2106497_BSGOXA_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Deduction Statments PO Report - Archive',682,'GL_CALENDAR_MONTH','GL Calendar Month','','','','default','','12','N','','','','','','','','KP059700','N','N','','XXEIS_2106497_BSGOXA_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Deduction Statments PO Report - Archive',682,'OFFER_NAME','Offer Name','','','','default','','5','N','','','','','','','','KP059700','N','N','','XXEIS_2106497_BSGOXA_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Deduction Statments PO Report - Archive',682,'MVID','MVID','','','','default','','1','N','','','','','','','','KP059700','N','N','','XXEIS_2106497_BSGOXA_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Deduction Statments PO Report - Archive',682,'RESALE_DATE_CREATED','Resale Date Created','','','','default','','11','N','','','','','','','','KP059700','N','N','','XXEIS_2106497_BSGOXA_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Deduction Statments PO Report - Archive',682,'VENDOR','Vendor','','','','default','','2','N','','','','','','','','KP059700','N','N','','XXEIS_2106497_BSGOXA_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Deduction Statments PO Report - Archive',682,'LOB','LOB','','','','default','','3','N','','','','','','','','KP059700','N','N','','XXEIS_2106497_BSGOXA_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Deduction Statments PO Report - Archive',682,'STATUS_CODE','Status Code','','','','default','','6','N','','','','','','','','KP059700','N','N','','XXEIS_2106497_BSGOXA_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Deduction Statments PO Report - Archive',682,'PO_NUMBER','PO Number','','','','default','','8','N','','','','','','','','KP059700','N','N','','XXEIS_2106497_BSGOXA_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Deduction Statments PO Report - Archive',682,'TOTAL_RECEIPTS','Total Receipts','','','~,~.~2','default','','13','N','','','','','','','','KP059700','N','N','','XXEIS_2106497_BSGOXA_V','','','SUM','US','','');
xxeis.eis_rsc_ins.rc( 'Deduction Statments PO Report - Archive',682,'REBATE_TYPE','Rebate Type','','','','default','','7','N','','','','','','','','KP059700','N','N','','XXEIS_2106497_BSGOXA_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Deduction Statments PO Report - Archive',682,'REBATES','Rebates','','','~,~.~2','default','','14','N','','','','','','','','KP059700','N','N','','XXEIS_2106497_BSGOXA_V','','','SUM','US','','');
xxeis.eis_rsc_ins.rc( 'Deduction Statments PO Report - Archive',682,'OFU_GL_DATE','GL Date','','','','default','','10','N','','','','','','','','KP059700','N','N','','XXEIS_2106497_BSGOXA_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Deduction Statments PO Report - Archive',682,'BU','BU','','','','default','','4','N','','','','','','','','KP059700','N','N','','XXEIS_2106497_BSGOXA_V','','','GROUP_BY','US','','');
--Inserting Report Parameters - Deduction Statments PO Report - Archive
xxeis.eis_rsc_ins.rp( 'Deduction Statments PO Report - Archive',682,'Agreement Year','','AGREEMENT_YEAR','IN','LOV AGREEMENT_YEAR','','VARCHAR2','Y','Y','4','Y','Y','CONSTANT','KP059700','Y','N','','','','XXEIS_2106497_BSGOXA_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Deduction Statments PO Report - Archive',682,'Gl Calendar Month','','GL_CALENDAR_MONTH','IN','LOV PERIOD','','VARCHAR2','Y','Y','5','Y','Y','CONSTANT','KP059700','Y','N','','','','XXEIS_2106497_BSGOXA_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Deduction Statments PO Report - Archive',682,'Offer Name','','OFFER_NAME','IN','HDS AGREEMENT_NAME','','VARCHAR2','N','Y','6','Y','Y','CONSTANT','KP059700','Y','N','','','','XXEIS_2106497_BSGOXA_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Deduction Statments PO Report - Archive',682,'GL Date To','','OFU_GL_DATE','<=','','','DATE','N','Y','2','Y','Y','CONSTANT','KP059700','Y','N','','End Date','','XXEIS_2106497_BSGOXA_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Deduction Statments PO Report - Archive',682,'Vendor','','VENDOR','IN','LOV VENDOR_NAME','','VARCHAR2','Y','Y','3','Y','Y','CONSTANT','KP059700','Y','N','','','','XXEIS_2106497_BSGOXA_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Deduction Statments PO Report - Archive',682,'GL Date From','','OFU_GL_DATE','>=','','','DATE','N','Y','1','Y','Y','CONSTANT','KP059700','Y','N','','Start Date','','XXEIS_2106497_BSGOXA_V','','','US','');
--Inserting Dependent Parameters - Deduction Statments PO Report - Archive
--Inserting Report Conditions - Deduction Statments PO Report - Archive
xxeis.eis_rsc_ins.rcnh( 'Deduction Statments PO Report - Archive',682,'AGREEMENT_YEAR IN :Agreement Year ','SIMPLE','','','Y','','1');
xxeis.eis_rsc_ins.rcnd( '','','AGREEMENT_YEAR','','Agreement Year','','','','','XXEIS_2106497_BSGOXA_V','','','','','','IN','Y','Y','','','','','1',682,'Deduction Statments PO Report - Archive','AGREEMENT_YEAR IN :Agreement Year ');
xxeis.eis_rsc_ins.rcnh( 'Deduction Statments PO Report - Archive',682,'GL_CALENDAR_MONTH IN :Gl Calendar Month ','SIMPLE','','','Y','','2');
xxeis.eis_rsc_ins.rcnd( '','','GL_CALENDAR_MONTH','','Gl Calendar Month','','','','','XXEIS_2106497_BSGOXA_V','','','','','','IN','Y','Y','','','','','1',682,'Deduction Statments PO Report - Archive','GL_CALENDAR_MONTH IN :Gl Calendar Month ');
xxeis.eis_rsc_ins.rcnh( 'Deduction Statments PO Report - Archive',682,'OFFER_NAME IN :Offer Name ','SIMPLE','','','Y','','3');
xxeis.eis_rsc_ins.rcnd( '','','OFFER_NAME','','Offer Name','','','','','XXEIS_2106497_BSGOXA_V','','','','','','IN','Y','Y','','','','','1',682,'Deduction Statments PO Report - Archive','OFFER_NAME IN :Offer Name ');
xxeis.eis_rsc_ins.rcnh( 'Deduction Statments PO Report - Archive',682,'OFU_GL_DATE >= :GL Date From ','SIMPLE','','','Y','','4');
xxeis.eis_rsc_ins.rcnd( '','','OFU_GL_DATE','','GL Date From','','','','','XXEIS_2106497_BSGOXA_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',682,'Deduction Statments PO Report - Archive','OFU_GL_DATE >= :GL Date From ');
xxeis.eis_rsc_ins.rcnh( 'Deduction Statments PO Report - Archive',682,'OFU_GL_DATE <= :GL Date To ','SIMPLE','','','Y','','5');
xxeis.eis_rsc_ins.rcnd( '','','OFU_GL_DATE','','GL Date To','','','','','XXEIS_2106497_BSGOXA_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',682,'Deduction Statments PO Report - Archive','OFU_GL_DATE <= :GL Date To ');
xxeis.eis_rsc_ins.rcnh( 'Deduction Statments PO Report - Archive',682,'VENDOR IN :Vendor ','SIMPLE','','','Y','','6');
xxeis.eis_rsc_ins.rcnd( '','','VENDOR','','Vendor','','','','','XXEIS_2106497_BSGOXA_V','','','','','','IN','Y','Y','','','','','1',682,'Deduction Statments PO Report - Archive','VENDOR IN :Vendor ');
xxeis.eis_rsc_ins.rcnh( 'Deduction Statments PO Report - Archive',682,'Free Text ','FREE_TEXT','','','Y','','7');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','','Y','','','','','and mvid in (select customer_attribute2 from xxcus.xxcus_rebate_customers
where 1=1
and party_name in (:Vendor))','1',682,'Deduction Statments PO Report - Archive','Free Text ');
--Inserting Report Sorts - Deduction Statments PO Report - Archive
xxeis.eis_rsc_ins.rs( 'Deduction Statments PO Report - Archive',682,'MVID','ASC','KP059700','1','');
xxeis.eis_rsc_ins.rs( 'Deduction Statments PO Report - Archive',682,'LOB','ASC','KP059700','2','');
xxeis.eis_rsc_ins.rs( 'Deduction Statments PO Report - Archive',682,'OFFER_NAME','ASC','KP059700','3','');
xxeis.eis_rsc_ins.rs( 'Deduction Statments PO Report - Archive',682,'OFU_GL_DATE','ASC','KP059700','4','');
--Inserting Report Triggers - Deduction Statments PO Report - Archive
--inserting report templates - Deduction Statments PO Report - Archive
--Inserting Report Portals - Deduction Statments PO Report - Archive
--inserting report dashboards - Deduction Statments PO Report - Archive
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Deduction Statments PO Report - Archive','682','XXEIS_2106497_BSGOXA_V','XXEIS_2106497_BSGOXA_V','N','');
--inserting report security - Deduction Statments PO Report - Archive
xxeis.eis_rsc_ins.rsec( 'Deduction Statments PO Report - Archive','682','','XXCUS_TM_ADMIN_USER',682,'KP059700','','','');
xxeis.eis_rsc_ins.rsec( 'Deduction Statments PO Report - Archive','','AB065961','',682,'KP059700','','N','');
--Inserting Report Pivots - Deduction Statments PO Report - Archive
--Inserting Report Distribution Details 
--Inserting Report Distribution Details 
--Inserting Report SubTotals Details 
--Inserting Report aggregations 
--Inserting Report   Version details- Deduction Statments PO Report - Archive
xxeis.eis_rsc_ins.rv( 'Deduction Statments PO Report - Archive','','Deduction Statments PO Report - Archive','AB065961','27-JUN-2017');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 682');
END IF;
END;
/
