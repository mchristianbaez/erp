--Report Name            : Legal Indicator Query - General
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating View Data for Legal Indicator Query - General
set scan off define off
DECLARE
BEGIN 
--Inserting View XXWC_AR_LEGAL_GEN
xxeis.eis_rs_ins.v( 'XXWC_AR_LEGAL_GEN',222,'Paste SQL View for Legal Indicator Query - General','1.0','','','DM027741','APPS','Legal Indicator Query - General View','X1MV1','','');
--Delete View Columns for XXWC_AR_LEGAL_GEN
xxeis.eis_rs_utility.delete_view_rows('XXWC_AR_LEGAL_GEN',222,FALSE);
--Inserting View Columns for XXWC_AR_LEGAL_GEN
xxeis.eis_rs_ins.vc( 'XXWC_AR_LEGAL_GEN','ACCOUNT_NUMBER',222,'','','','','','DM027741','VARCHAR2','','','Account Number','','','');
xxeis.eis_rs_ins.vc( 'XXWC_AR_LEGAL_GEN','ACCOUNT_NAME',222,'','','','','','DM027741','VARCHAR2','','','Account Name','','','');
xxeis.eis_rs_ins.vc( 'XXWC_AR_LEGAL_GEN','ACCOUNT_STATUS',222,'','','','','','DM027741','VARCHAR2','','','Account Status','','','');
xxeis.eis_rs_ins.vc( 'XXWC_AR_LEGAL_GEN','CREATION_DATE',222,'','','','','','DM027741','DATE','','','Creation Date','','','');
xxeis.eis_rs_ins.vc( 'XXWC_AR_LEGAL_GEN','PARTY_NUMBER',222,'','','','','','DM027741','VARCHAR2','','','Party Number','','','');
xxeis.eis_rs_ins.vc( 'XXWC_AR_LEGAL_GEN','PRIMARY_PHONE_AREA_CODE',222,'','','','','','DM027741','VARCHAR2','','','Primary Phone Area Code','','','');
xxeis.eis_rs_ins.vc( 'XXWC_AR_LEGAL_GEN','ATTRIBUTE5',222,'','','','','','DM027741','VARCHAR2','','','Attribute5','','','');
xxeis.eis_rs_ins.vc( 'XXWC_AR_LEGAL_GEN','SEND_STATEMENTS',222,'','','','','','DM027741','VARCHAR2','','','Send Statements','','','');
xxeis.eis_rs_ins.vc( 'XXWC_AR_LEGAL_GEN','ATTRIBUTE10',222,'','','','','','DM027741','VARCHAR2','','','Attribute10','','','');
xxeis.eis_rs_ins.vc( 'XXWC_AR_LEGAL_GEN','NAME',222,'Name','NAME','','','','DM027741','VARCHAR2','','','Name','','','');
xxeis.eis_rs_ins.vc( 'XXWC_AR_LEGAL_GEN','PARTY_ID',222,'','','','','','DM027741','NUMBER','','','Party Id','','','');
--Inserting View Components for XXWC_AR_LEGAL_GEN
--Inserting View Component Joins for XXWC_AR_LEGAL_GEN
END;
/
set scan on define on
prompt Creating Report Data for Legal Indicator Query - General
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Legal Indicator Query - General
xxeis.eis_rs_utility.delete_report_rows( 'Legal Indicator Query - General' );
--Inserting Report - Legal Indicator Query - General
xxeis.eis_rs_ins.r( 222,'Legal Indicator Query - General','','','','','','DM027741','XXWC_AR_LEGAL_GEN','Y','','SELECT DISTINCT
A.PARTY_ID ,
A.ACCOUNT_NUMBER ,
a.account_name,
c.account_status,
c.send_statements,
a.attribute5,
a.creation_date,
B.PARTY_NUMBER,
B.PRIMARY_PHONE_AREA_CODE,
a.attribute10
from ar.hz_cust_accounts a,
ar.hz_parties b,
ar.hz_customer_profiles c
where a.party_id = b.party_id
AND A.CUST_ACCOUNT_ID = C.CUST_ACCOUNT_ID
--and a.attribute5 is null
','DM027741','','N','Conversion Reports','PDF,','CSV,EXCEL,','N');
--Inserting Report Columns - Legal Indicator Query - General
xxeis.eis_rs_ins.rc( 'Legal Indicator Query - General',222,'ACCOUNT_NAME','Account Name','','','','default','','2','N','','','','','','','','DM027741','N','N','','XXWC_AR_LEGAL_GEN','','');
xxeis.eis_rs_ins.rc( 'Legal Indicator Query - General',222,'ACCOUNT_STATUS','Account Status','','','','default','','3','N','','','','','','','','DM027741','N','N','','XXWC_AR_LEGAL_GEN','','');
xxeis.eis_rs_ins.rc( 'Legal Indicator Query - General',222,'ACCOUNT_NUMBER','Account Number','','','','default','','1','N','','','','','','','','DM027741','N','N','','XXWC_AR_LEGAL_GEN','','');
xxeis.eis_rs_ins.rc( 'Legal Indicator Query - General',222,'CREATION_DATE','Creation Date','','','','default','','7','N','','','','','','','','DM027741','N','N','','XXWC_AR_LEGAL_GEN','','');
xxeis.eis_rs_ins.rc( 'Legal Indicator Query - General',222,'PRIMARY_PHONE_AREA_CODE','Primary Phone Area Code','','','','default','','9','N','','','','','','','','DM027741','N','N','','XXWC_AR_LEGAL_GEN','','');
xxeis.eis_rs_ins.rc( 'Legal Indicator Query - General',222,'PARTY_NUMBER','Party Number','','','','default','','8','N','','','','','','','','DM027741','N','N','','XXWC_AR_LEGAL_GEN','','');
xxeis.eis_rs_ins.rc( 'Legal Indicator Query - General',222,'ATTRIBUTE5','Legal Collection Indicator','','','','default','','6','N','','','','','','','','DM027741','N','N','','XXWC_AR_LEGAL_GEN','','');
xxeis.eis_rs_ins.rc( 'Legal Indicator Query - General',222,'SEND_STATEMENTS','Send Statements','','','','default','','5','N','','','','','','','','DM027741','N','N','','XXWC_AR_LEGAL_GEN','','');
xxeis.eis_rs_ins.rc( 'Legal Indicator Query - General',222,'ATTRIBUTE10','Legal Placement','','','','default','','10','N','','','','','','','','DM027741','N','N','','XXWC_AR_LEGAL_GEN','','');
xxeis.eis_rs_ins.rc( 'Legal Indicator Query - General',222,'NAME','Name','Name','','','','','4','N','','','','','','','','DM027741','N','N','','XXWC_AR_LEGAL_GEN','','');
--Inserting Report Parameters - Legal Indicator Query - General
xxeis.eis_rs_ins.rp( 'Legal Indicator Query - General',222,'Account Status','','ACCOUNT_STATUS','IN','','''LEGAL'', ''BANKRUPTCY''','VARCHAR2','N','Y','2','','Y','CONSTANT','DM027741','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Legal Indicator Query - General',222,'Creation Date','','CREATION_DATE','IN','','01-MAY-12','DATE','Y','Y','1','','Y','CONSTANT','DM027741','Y','N','','','');
--Inserting Report Conditions - Legal Indicator Query - General
xxeis.eis_rs_ins.rcn( 'Legal Indicator Query - General',222,'ACCOUNT_STATUS','IN',':Account Status','','','Y','2','Y','DM027741');
xxeis.eis_rs_ins.rcn( 'Legal Indicator Query - General',222,'CREATION_DATE','>=',':Creation Date','','','Y','1','Y','DM027741');
--Inserting Report Sorts - Legal Indicator Query - General
xxeis.eis_rs_ins.rs( 'Legal Indicator Query - General',222,'ACCOUNT_NUMBER','','DM027741','','');
--Inserting Report Triggers - Legal Indicator Query - General
--Inserting Report Templates - Legal Indicator Query - General
--Inserting Report Portals - Legal Indicator Query - General
--Inserting Report Dashboards - Legal Indicator Query - General
--Inserting Report Security - Legal Indicator Query - General
xxeis.eis_rs_ins.rsec( 'Legal Indicator Query - General','222','','50993',222,'DM027741','','');
xxeis.eis_rs_ins.rsec( 'Legal Indicator Query - General','222','','50854',222,'DM027741','','');
xxeis.eis_rs_ins.rsec( 'Legal Indicator Query - General','222','','50879',222,'DM027741','','');
xxeis.eis_rs_ins.rsec( 'Legal Indicator Query - General','','DM027741','',222,'DM027741','','');
--Inserting Report Pivots - Legal Indicator Query - General
END;
/
set scan on define on
