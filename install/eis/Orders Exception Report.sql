--Report Name            : Orders Exception Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data EIS_XXWC_OM_ORDER_EXP_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_OM_ORDER_EXP_V
xxeis.eis_rsc_ins.v( 'EIS_XXWC_OM_ORDER_EXP_V',660,'','','','','ANONYMOUS','XXEIS','Eis Xxwc Om Order Exp V','EXOOEV','','','VIEW','US','','');
--Delete Object Columns for EIS_XXWC_OM_ORDER_EXP_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_OM_ORDER_EXP_V',660,FALSE);
--Inserting Object Columns for EIS_XXWC_OM_ORDER_EXP_V
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_EXP_V','LOC',660,'Loc','LOC','','','','ANONYMOUS','VARCHAR2','','','Loc','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_EXP_V','EXCEPTION_DESCRIPTION',660,'Exception Description','EXCEPTION_DESCRIPTION','','','','ANONYMOUS','VARCHAR2','','','Exception Description','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_EXP_V','EXT_ORDER_TOTAL',660,'Ext Order Total','EXT_ORDER_TOTAL','','~T~D~2','','ANONYMOUS','NUMBER','','','Ext Order Total','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_EXP_V','ORDER_TYPE',660,'Order Type','ORDER_TYPE','','','','ANONYMOUS','VARCHAR2','','','Order Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_EXP_V','CUSTOMER_NAME',660,'Customer Name','CUSTOMER_NAME','','','','ANONYMOUS','VARCHAR2','','','Customer Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_EXP_V','EXCEPTION_DATE',660,'Exception Date','EXCEPTION_DATE','','','','ANONYMOUS','DATE','','','Exception Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_EXP_V','ORDER_DATE',660,'Order Date','ORDER_DATE','','','','ANONYMOUS','DATE','','','Order Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_EXP_V','ORDER_NUMBER',660,'Order Number','ORDER_NUMBER','','','','ANONYMOUS','NUMBER','','','Order Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_EXP_V','EXCEPTION_RESAON',660,'Exception Resaon','EXCEPTION_RESAON','','','','ANONYMOUS','VARCHAR2','','','Exception Resaon','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_EXP_V','SALES_PERSON_NAME',660,'Sales Person Name','SALES_PERSON_NAME','','','','ANONYMOUS','VARCHAR2','','','Sales Person Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_EXP_V','CREATED_BY',660,'Created By','CREATED_BY','','','','ANONYMOUS','VARCHAR2','','','Created By','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_EXP_V','QTY',660,'Qty','QTY','','~T~D~2','','ANONYMOUS','NUMBER','','','Qty','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_EXP_V','SHIP_METHOD',660,'Ship Method','SHIP_METHOD','','','','ANONYMOUS','VARCHAR2','','','Ship Method','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_EXP_V','REQUEST_DATE',660,'Request Date','REQUEST_DATE','','','','ANONYMOUS','DATE','','','Request Date','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_EXP_V','PROCESS_ID',660,'Process Id','PROCESS_ID','','','','ANONYMOUS','NUMBER','','','Process Id','','','','US');
--Inserting Object Components for EIS_XXWC_OM_ORDER_EXP_V
--Inserting Object Component Joins for EIS_XXWC_OM_ORDER_EXP_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
prompt Creating Report LOV Data for Orders Exception Report
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - Orders Exception Report
xxeis.eis_rsc_ins.lov( 660,'select  RS.Name,SALESREP_ID  from  RA_SALESREPS RS
WHERE  RS.NAME is not null','','OM SALES REP','This gives the sales representative name','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'SELECT ORGANIZATION_CODE WAREHOUSE,ORGANIZATION_NAME FROM ORG_ORGANIZATION_DEFINITIONS OOD WHERE SYSDATE < NVL(OOD.DISABLE_DATE,SYSDATE+1) AND EXISTS (SELECT 1 FROM XXEIS.EIS_ORG_ACCESS_V  WHERE organization_id = ood.organization_id )
UNION
SELECT ''All'', ''All Organizations'' from Dual','','OM Warehouse All','','ANONYMOUS',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
prompt Creating Report Data for Orders Exception Report
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Orders Exception Report
xxeis.eis_rsc_utility.delete_report_rows( 'Orders Exception Report' );
--Inserting Report - Orders Exception Report
xxeis.eis_rsc_ins.r( 660,'Orders Exception Report','','White Cap Exception Report','','','','SA059956','EIS_XXWC_OM_ORDER_EXP_V','Y','','','SA059956','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N','','','','','','','','US','','','','');
--Inserting Report Columns - Orders Exception Report
xxeis.eis_rsc_ins.rc( 'Orders Exception Report',660,'EXCEPTION_DESCRIPTION','Exception Description','Exception Description','','','default','','13','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_ORDER_EXP_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Orders Exception Report',660,'EXCEPTION_RESAON','Exception Reason','Exception Resaon','','','default','','2','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_ORDER_EXP_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Orders Exception Report',660,'EXT_ORDER_TOTAL','Line Total','Ext Order Total','','~T~D~2','default','','12','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_ORDER_EXP_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Orders Exception Report',660,'ORDER_DATE','Order Date','Order Date','','','default','','5','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_ORDER_EXP_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Orders Exception Report',660,'ORDER_NUMBER','Order Number','Order Number','','~~~','default','','4','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_ORDER_EXP_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Orders Exception Report',660,'ORDER_TYPE','Order Type','Order Type','','','default','','9','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_ORDER_EXP_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Orders Exception Report',660,'CREATED_BY','Created By','Created By','','','default','','3','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_ORDER_EXP_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Orders Exception Report',660,'QTY','Qty','Qty','','~~~','default','','11','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_ORDER_EXP_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Orders Exception Report',660,'LOC','Loc','Loc','','','default','','1','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_ORDER_EXP_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Orders Exception Report',660,'SALES_PERSON_NAME','Sales Person Name','Sales Person Name','','','default','','14','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_ORDER_EXP_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Orders Exception Report',660,'SHIP_METHOD','Ship Method','Ship Method','','','default','','10','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_ORDER_EXP_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Orders Exception Report',660,'CUSTOMER_NAME','Customer Name','Customer Name','','','default','','8','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_ORDER_EXP_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Orders Exception Report',660,'EXCEPTION_DATE','Exception Date','Exception Date','','','default','','7','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_ORDER_EXP_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Orders Exception Report',660,'REQUEST_DATE','Request Date','Request Date','','','default','','6','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_ORDER_EXP_V','','','','US','');
--Inserting Report Parameters - Orders Exception Report
xxeis.eis_rsc_ins.rp( 'Orders Exception Report',660,'Sales Person Name','Sales Person Name','SALES_PERSON_NAME','IN','OM SALES REP','','VARCHAR2','N','Y','2','','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_OM_ORDER_EXP_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Orders Exception Report',660,'Organization','Organization','LOC','IN','OM Warehouse All','','VARCHAR2','Y','Y','1','','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_OM_ORDER_EXP_V','','','US','');
--Inserting Dependent Parameters - Orders Exception Report
--Inserting Report Conditions - Orders Exception Report
xxeis.eis_rsc_ins.rcnh( 'Orders Exception Report',660,'Free Text ','FREE_TEXT','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','','Y','','','','','AND PROCESS_ID=:SYSTEM.PROCESS_ID','1',660,'Orders Exception Report','Free Text ');
--Inserting Report Sorts - Orders Exception Report
xxeis.eis_rsc_ins.rs( 'Orders Exception Report',660,'','ASC','SA059956','1','1');
--Inserting Report Triggers - Orders Exception Report
xxeis.eis_rsc_ins.rt( 'Orders Exception Report',660,'begin
xxeis.eis_xxwc_om_order_exp_pkg.process_order_exception(
p_process_id 		=> :SYSTEM.PROCESS_ID,
p_organization		=> :Organization,
p_sales_person_name => :Sales Person Name);
end;','B','Y','SA059956','AQ');
--inserting report templates - Orders Exception Report
--Inserting Report Portals - Orders Exception Report
--inserting report dashboards - Orders Exception Report
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Orders Exception Report','660','EIS_XXWC_OM_ORDER_EXP_V','EIS_XXWC_OM_ORDER_EXP_V','N','');
--inserting report security - Orders Exception Report
xxeis.eis_rsc_ins.rsec( 'Orders Exception Report','660','','XXWC_RENTAL_OM_PRICING_SUPER',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Orders Exception Report','660','','',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Orders Exception Report','660','','XXWC_ORDER_MGMT_SUPER_USER',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Orders Exception Report','660','','XXWC_ORDER_MGMT_READ_ONLY',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Orders Exception Report','660','','XXWC_ORDER_MGMT_PRICING_SUPER',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Orders Exception Report','660','','XXWC_ORDER_MGMT_PRICING_STD',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Orders Exception Report','660','','XXWC_ORDER_MGMT_PRICING_LTD',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Orders Exception Report','660','','XXWC_ORDER_MGMT_READ_SHIPPING',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Orders Exception Report','660','','XXWC_ORDER_MGMT_READ_ONLY_COST',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Orders Exception Report','660','','XXWC_ORDER_MGMT_PRICING_FULL',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Orders Exception Report','20005','','XXWC_VIEW_ALL_EIS_REPORTS',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Orders Exception Report','','HY016540','',660,'SA059956','','','');
--Inserting Report Pivots - Orders Exception Report
xxeis.eis_rsc_ins.rpivot( 'Orders Exception Report',660,'Summary by reason','1','1,0|1,2,0','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Summary by reason
xxeis.eis_rsc_ins.rpivot_dtls( 'Orders Exception Report',660,'Summary by reason','EXT_ORDER_TOTAL','DATA_FIELD','SUM','Ext Total','1','','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Orders Exception Report',660,'Summary by reason','EXCEPTION_RESAON','ROW_FIELD','','','1','1','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Orders Exception Report',660,'Summary by reason','ORDER_NUMBER','ROW_FIELD','','','3','1','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Orders Exception Report',660,'Summary by reason','CREATED_BY','ROW_FIELD','','','2','','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Orders Exception Report',660,'Summary by reason','QTY','ROW_FIELD','','','4','','');
--Inserting Report Summary Calculation Columns For Pivot- Summary by reason
--Inserting Report   Version details- Orders Exception Report
xxeis.eis_rsc_ins.rv( 'Orders Exception Report','','Orders Exception Report','SA059956');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
