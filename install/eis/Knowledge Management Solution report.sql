--Report Name            : Knowledge Management Solution report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View XXWC_KNOWLEDGE_MGR_SOL_V
set scan off define off
prompt Creating View XXEIS.XXWC_KNOWLEDGE_MGR_SOL_V
Create or replace View XXEIS.XXWC_KNOWLEDGE_MGR_SOL_V
 AS 
SELECT DISTINCT set_number,
                     a.name,
                     a.creation_date "Created",
                     a.last_update_date "Modified",
                     b.user_name "Created by",
                     b.description "Created by name",
                     c.user_name "Modified by",
                     c.description "Modified by name"
       FROM apps.cs_kb_sets_vl a, apps.fnd_user b, apps.fnd_user c
      WHERE     NVL (a.latest_version_flag, 'N') = 'Y'
            AND a.status = 'PUB'
            AND a.original_author = b.user_id
            AND a.last_updated_by = c.user_id
            AND b.user_id <> 1
   ORDER BY 2 DESC/
set scan on define on
prompt Creating View Data for Knowledge Management Solution report
set scan off define off
DECLARE
BEGIN 
--Inserting View XXWC_KNOWLEDGE_MGR_SOL_V
xxeis.eis_rs_ins.v( 'XXWC_KNOWLEDGE_MGR_SOL_V',85000,'','','','','HT038687','XXEIS','Xxwc Knowledge Mgr Sol V','XKMSV','','');
--Delete View Columns for XXWC_KNOWLEDGE_MGR_SOL_V
xxeis.eis_rs_utility.delete_view_rows('XXWC_KNOWLEDGE_MGR_SOL_V',85000,FALSE);
--Inserting View Columns for XXWC_KNOWLEDGE_MGR_SOL_V
xxeis.eis_rs_ins.vc( 'XXWC_KNOWLEDGE_MGR_SOL_V','MODIFIED_BY_NAME',85000,'Modified By Name','MODIFIED_BY_NAME','','','','HT038687','VARCHAR2','','','Modified By Name','','','');
xxeis.eis_rs_ins.vc( 'XXWC_KNOWLEDGE_MGR_SOL_V','MODIFIED_BY',85000,'Modified By','MODIFIED_BY','','','','HT038687','VARCHAR2','','','Modified By','','','');
xxeis.eis_rs_ins.vc( 'XXWC_KNOWLEDGE_MGR_SOL_V','CREATED_BY_NAME',85000,'Created By Name','CREATED_BY_NAME','','','','HT038687','VARCHAR2','','','Created By Name','','','');
xxeis.eis_rs_ins.vc( 'XXWC_KNOWLEDGE_MGR_SOL_V','CREATED_BY',85000,'Created By','CREATED_BY','','','','HT038687','VARCHAR2','','','Created By','','','');
xxeis.eis_rs_ins.vc( 'XXWC_KNOWLEDGE_MGR_SOL_V','MODIFIED',85000,'Modified','MODIFIED','','','','HT038687','DATE','','','Modified','','','');
xxeis.eis_rs_ins.vc( 'XXWC_KNOWLEDGE_MGR_SOL_V','CREATED',85000,'Created','CREATED','','','','HT038687','DATE','','','Created','','','');
xxeis.eis_rs_ins.vc( 'XXWC_KNOWLEDGE_MGR_SOL_V','NAME',85000,'Name','NAME','','','','HT038687','VARCHAR2','','','Name','','','');
xxeis.eis_rs_ins.vc( 'XXWC_KNOWLEDGE_MGR_SOL_V','SET_NUMBER',85000,'Set Number','SET_NUMBER','','','','HT038687','VARCHAR2','','','Set Number','','','');
--Inserting View Components for XXWC_KNOWLEDGE_MGR_SOL_V
--Inserting View Component Joins for XXWC_KNOWLEDGE_MGR_SOL_V
END;
/
set scan on define on
prompt Creating Report Data for Knowledge Management Solution report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Knowledge Management Solution report
xxeis.eis_rs_utility.delete_report_rows( 'Knowledge Management Solution report' );
--Inserting Report - Knowledge Management Solution report
xxeis.eis_rs_ins.r( 85000,'Knowledge Management Solution report','','This is a Knowledge Management Solution Authoring report to show solution name, who created them, last updated date and who approved','','','','HT038687','XXWC_KNOWLEDGE_MGR_SOL_V','Y','','','HT038687','','N','WC Knowledge Management Reports','','CSV,EXCEL,','N');
--Inserting Report Columns - Knowledge Management Solution report
xxeis.eis_rs_ins.rc( 'Knowledge Management Solution report',85000,'CREATED','Created','Created','','','default','','3','N','','','','','','','','HT038687','N','N','','XXWC_KNOWLEDGE_MGR_SOL_V','','');
xxeis.eis_rs_ins.rc( 'Knowledge Management Solution report',85000,'CREATED_BY','Created By','Created By','','','default','','5','N','','','','','','','','HT038687','N','N','','XXWC_KNOWLEDGE_MGR_SOL_V','','');
xxeis.eis_rs_ins.rc( 'Knowledge Management Solution report',85000,'CREATED_BY_NAME','Created By Name','Created By Name','','','default','','6','N','','','','','','','','HT038687','N','N','','XXWC_KNOWLEDGE_MGR_SOL_V','','');
xxeis.eis_rs_ins.rc( 'Knowledge Management Solution report',85000,'MODIFIED','Modified','Modified','','','default','','4','N','','','','','','','','HT038687','N','N','','XXWC_KNOWLEDGE_MGR_SOL_V','','');
xxeis.eis_rs_ins.rc( 'Knowledge Management Solution report',85000,'MODIFIED_BY','Modified By','Modified By','','','default','','7','N','','','','','','','','HT038687','N','N','','XXWC_KNOWLEDGE_MGR_SOL_V','','');
xxeis.eis_rs_ins.rc( 'Knowledge Management Solution report',85000,'MODIFIED_BY_NAME','Modified By Name','Modified By Name','','','default','','8','N','','','','','','','','HT038687','N','N','','XXWC_KNOWLEDGE_MGR_SOL_V','','');
xxeis.eis_rs_ins.rc( 'Knowledge Management Solution report',85000,'NAME','Name','Name','','','default','','2','N','','','','','','','','HT038687','N','N','','XXWC_KNOWLEDGE_MGR_SOL_V','','');
xxeis.eis_rs_ins.rc( 'Knowledge Management Solution report',85000,'SET_NUMBER','Set Number','Set Number','','','default','','1','N','','','','','','','','HT038687','N','N','','XXWC_KNOWLEDGE_MGR_SOL_V','','');
--Inserting Report Parameters - Knowledge Management Solution report
--Inserting Report Conditions - Knowledge Management Solution report
--Inserting Report Sorts - Knowledge Management Solution report
--Inserting Report Triggers - Knowledge Management Solution report
--Inserting Report Templates - Knowledge Management Solution report
--Inserting Report Portals - Knowledge Management Solution report
--Inserting Report Dashboards - Knowledge Management Solution report
--Inserting Report Security - Knowledge Management Solution report
xxeis.eis_rs_ins.rsec( 'Knowledge Management Solution report','20005','','50843',85000,'HT038687','','');
xxeis.eis_rs_ins.rsec( 'Knowledge Management Solution report','20005','','51207',85000,'HT038687','','');
xxeis.eis_rs_ins.rsec( 'Knowledge Management Solution report','20005','','50861',85000,'HT038687','','');
xxeis.eis_rs_ins.rsec( 'Knowledge Management Solution report','170','','51549',85000,'HT038687','','');
xxeis.eis_rs_ins.rsec( 'Knowledge Management Solution report','170','','51509',85000,'HT038687','','');
--Inserting Report Pivots - Knowledge Management Solution report
END;
/
set scan on define on
