--Report Name            : Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data EIS_XXWC_PO_AUTO_REAPPRVD_PO_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(201);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_PO_AUTO_REAPPRVD_PO_V
xxeis.eis_rsc_ins.v( 'EIS_XXWC_PO_AUTO_REAPPRVD_PO_V',201,'','','','','SA059956','APPS','Eis Xxwc Po Auto Reapprvd Po V','EXPARPV','','','VIEW','US','','');
--Delete Object Columns for EIS_XXWC_PO_AUTO_REAPPRVD_PO_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_PO_AUTO_REAPPRVD_PO_V',201,FALSE);
--Inserting Object Columns for EIS_XXWC_PO_AUTO_REAPPRVD_PO_V
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_AUTO_REAPPRVD_PO_V','REGION',201,'Region','REGION','','','','SA059956','VARCHAR2','','','Region','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_AUTO_REAPPRVD_PO_V','DISTRICT',201,'District','DISTRICT','','','','SA059956','VARCHAR2','','','District','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_AUTO_REAPPRVD_PO_V','BRANCH',201,'Branch','BRANCH','','','','SA059956','VARCHAR2','','','Branch','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_AUTO_REAPPRVD_PO_V','SHIP_TO_LOCATION',201,'Ship To Location','SHIP_TO_LOCATION','','','','SA059956','VARCHAR2','','','Ship To Location','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_AUTO_REAPPRVD_PO_V','PO_PREVIOUS_AMOUNT',201,'Po Previous Amount','PO_PREVIOUS_AMOUNT','','','','SA059956','NUMBER','','','Po Previous Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_AUTO_REAPPRVD_PO_V','PO_AMOUNT',201,'Po Amount','PO_AMOUNT','','','','SA059956','NUMBER','','','Po Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_AUTO_REAPPRVD_PO_V','BUYER_APPROVAL_LIMIT',201,'Buyer Approval Limit','BUYER_APPROVAL_LIMIT','','','','SA059956','NUMBER','','','Buyer Approval Limit','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_AUTO_REAPPRVD_PO_V','VENDOR_CONTACT',201,'Vendor Contact','VENDOR_CONTACT','','','','SA059956','VARCHAR2','','','Vendor Contact','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_AUTO_REAPPRVD_PO_V','VENDOR_SITE_CODE',201,'Vendor Site Code','VENDOR_SITE_CODE','','','','SA059956','VARCHAR2','','','Vendor Site Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_AUTO_REAPPRVD_PO_V','VENDOR_NUMBER',201,'Vendor Number','VENDOR_NUMBER','','','','SA059956','VARCHAR2','','','Vendor Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_AUTO_REAPPRVD_PO_V','VENDOR_NAME',201,'Vendor Name','VENDOR_NAME','','','','SA059956','VARCHAR2','','','Vendor Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_AUTO_REAPPRVD_PO_V','BUYER',201,'Buyer','BUYER','','','','SA059956','VARCHAR2','','','Buyer','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_AUTO_REAPPRVD_PO_V','APPROVED_DATE',201,'Approved Date','APPROVED_DATE','','','','SA059956','DATE','','','Approved Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_AUTO_REAPPRVD_PO_V','CREATION_DATE',201,'Creation Date','CREATION_DATE','','','','SA059956','DATE','','','Creation Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_AUTO_REAPPRVD_PO_V','REVISION_NUM',201,'Revision Num','REVISION_NUM','','','','SA059956','NUMBER','','','Revision Num','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_AUTO_REAPPRVD_PO_V','STATUS',201,'Status','STATUS','','','','SA059956','VARCHAR2','','','Status','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_AUTO_REAPPRVD_PO_V','PO_NUMBER',201,'Po Number','PO_NUMBER','','','','SA059956','VARCHAR2','','','Po Number','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_AUTO_REAPPRVD_PO_V','ORIGINAL_PO_AMOUNT',201,'Original Po Amount','ORIGINAL_PO_AMOUNT','','','','SA059956','NUMBER','','','Original Po Amount','','','','');
--Inserting Object Components for EIS_XXWC_PO_AUTO_REAPPRVD_PO_V
--Inserting Object Component Joins for EIS_XXWC_PO_AUTO_REAPPRVD_PO_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 201');
END IF;
END;
/
prompt Creating Report LOV Data for Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(201);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC
xxeis.eis_rsc_ins.lov( 201,'select distinct ATTRIBUTE8 District from mtl_parameters','','District Lov','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'select distinct ATTRIBUTE9 Region from mtl_parameters','','Region Lov','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'select agent_name buyer_name from po_agents_v','','PO Buyer Name LOV','This LOV lists all Buyer Names','XXEIS_RS_ADMIN',NULL,'','','','Y','','','','US');
xxeis.eis_rsc_ins.lov( '','select distinct vendor_name,segment1 vendor_number from po_vendors order by 1','','EIS XXWC Vendor Name LOV','','SA059956',NULL,'N','','','N','N','','','US');
xxeis.eis_rsc_ins.lov( '','select distinct segment1 vendor_number,vendor_name from po_vendors order by lpad(segment1,10)','','EIS XXWC Vendor Num LOV','','SA059956',NULL,'N','','','N','N','','','US');
xxeis.eis_rsc_ins.lov( 201,'SELECT mp.organization_code FROM mtl_parameters mp ORDER BY 1','','XXWC PO Organization Code LOV','','SA059956',NULL,'N','','','N','N','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 201');
END IF;
END;
/
prompt Creating Report Data for Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(201);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC
xxeis.eis_rsc_utility.delete_report_rows( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC' );
--Inserting Report - Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC
xxeis.eis_rsc_ins.r( 201,'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC','','PO Re-Approval - POs >= $500k can be modified with a threshold of 2% without evoking the full approval workflow.','','','','SA059956','EIS_XXWC_PO_AUTO_REAPPRVD_PO_V','Y','','','SA059956','','N','White Cap Reports','','CSV,EXCEL,','N','','','','','','N','','US','','','','');
--Inserting Report Columns - Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC
xxeis.eis_rsc_ins.rc( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC',201,'APPROVED_DATE','Approved Date','Approved Date','','','default','','9','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_AUTO_REAPPRVD_PO_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC',201,'BRANCH','Branch','Branch','','','default','','3','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_AUTO_REAPPRVD_PO_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC',201,'BUYER','Buyer','Buyer','','','default','','10','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_AUTO_REAPPRVD_PO_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC',201,'BUYER_APPROVAL_LIMIT','Buyer Approval Limit','Buyer Approval Limit','','~T~D~2','default','','11','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_AUTO_REAPPRVD_PO_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC',201,'CREATION_DATE','Creation Date','Creation Date','','','default','','8','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_AUTO_REAPPRVD_PO_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC',201,'DISTRICT','District','District','','','default','','2','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_AUTO_REAPPRVD_PO_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC',201,'PO_AMOUNT','New PO Amount','Po Amount','','~T~D~2','default','','14','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_AUTO_REAPPRVD_PO_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC',201,'PO_PREVIOUS_AMOUNT','Previous PO Amount','Po Previous Amount','','~T~D~2','default','','13','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_AUTO_REAPPRVD_PO_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC',201,'REGION','Region','Region','','','default','','1','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_AUTO_REAPPRVD_PO_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC',201,'REVISION_NUM','Revision Num','Revision Num','','~~~','default','','6','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_AUTO_REAPPRVD_PO_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC',201,'SHIP_TO_LOCATION','Ship To Location','Ship To Location','','','default','','4','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_AUTO_REAPPRVD_PO_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC',201,'STATUS','Status','Status','','','default','','7','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_AUTO_REAPPRVD_PO_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC',201,'VENDOR_CONTACT','Vendor Contact','Vendor Contact','','','default','','19','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_AUTO_REAPPRVD_PO_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC',201,'VENDOR_NAME','Vendor Name','Vendor Name','','','default','','18','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_AUTO_REAPPRVD_PO_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC',201,'VENDOR_NUMBER','Vendor Number','Vendor Number','','','default','','17','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_AUTO_REAPPRVD_PO_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC',201,'VENDOR_SITE_CODE','Vendor Site Code','Vendor Site Code','','','default','','20','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_AUTO_REAPPRVD_PO_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC',201,'PO_NUMBER','PO','Po Number','','','default','','5','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_AUTO_REAPPRVD_PO_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC',201,'Change_Percent','Change Percent','PERC_CHANGE_FROM_PREV_PO_AMOUNT','NUMBER','~T~D~2','default','','15','Y','Y','','','','','','(ROUND (((NVL(exparpv.po_amount,0)- NVL(exparpv.po_previous_amount,0)) / NVL(exparpv.po_previous_amount,0)) * 100, 2))','SA059956','N','N','','EIS_XXWC_PO_AUTO_REAPPRVD_PO_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC',201,'ORIGINAL_PO_AMOUNT','Original PO Amount','Original Po Amount','','~T~D~2','default','','12','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_AUTO_REAPPRVD_PO_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC',201,'Change_Amount','Change Amount','CHANGE_AMOUNT','NUMBER','~T~D~2','default','','16','Y','Y','','','','','','(NVL(EXPARPV.PO_AMOUNT,0) - NVL(EXPARPV.PO_PREVIOUS_AMOUNT,0))','SA059956','N','N','','','','','','US','');
--Inserting Report Parameters - Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC
xxeis.eis_rsc_ins.rp( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC',201,'Approved Date From','Approved Date From','APPROVED_DATE','>=','','','DATE','N','Y','3','N','Y','CONSTANT','SA059956','Y','N','','Start Date','','EIS_XXWC_PO_AUTO_REAPPRVD_PO_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC',201,'Branch','Branch','BRANCH','IN','XXWC PO Organization Code LOV','','VARCHAR2','N','Y','7','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_PO_AUTO_REAPPRVD_PO_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC',201,'Buyer','Buyer','BUYER','IN','PO Buyer Name LOV','','VARCHAR2','N','Y','8','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_PO_AUTO_REAPPRVD_PO_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC',201,'Creation Date From','Creation Date From','CREATION_DATE','>=','','select sysdate-7 from dual','DATE','Y','Y','1','N','Y','SQL','SA059956','Y','N','','Start Date','','EIS_XXWC_PO_AUTO_REAPPRVD_PO_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC',201,'District','District','DISTRICT','IN','District Lov','','VARCHAR2','N','Y','9','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_PO_AUTO_REAPPRVD_PO_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC',201,'Region','Region','REGION','IN','Region Lov','','VARCHAR2','N','Y','10','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_PO_AUTO_REAPPRVD_PO_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC',201,'Vendor Name','Vendor Name','VENDOR_NAME','IN','EIS XXWC Vendor Name LOV','','VARCHAR2','N','Y','5','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_PO_AUTO_REAPPRVD_PO_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC',201,'Vendor Number','Vendor Number','VENDOR_NUMBER','IN','EIS XXWC Vendor Num LOV','','VARCHAR2','N','Y','6','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_PO_AUTO_REAPPRVD_PO_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC',201,'Creation Date To','Creation Date To','CREATION_DATE','<=','','','DATE','Y','Y','2','N','Y','CURRENT_DATE','SA059956','Y','N','','End Date','','EIS_XXWC_PO_AUTO_REAPPRVD_PO_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC',201,'Approved Date To','Approved Date To','APPROVED_DATE','<=','','','DATE','N','Y','4','N','Y','CONSTANT','SA059956','Y','N','','End Date','','EIS_XXWC_PO_AUTO_REAPPRVD_PO_V','','','US','');
--Inserting Dependent Parameters - Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC
xxeis.eis_rsc_ins.rdp( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC',201,'VENDOR_NUMBER','Vendor Name','Vendor Number','IN','N','');
xxeis.eis_rsc_ins.rdp( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC',201,'VENDOR_NAME','Vendor Number','Vendor Name','IN','N','');
--Inserting Report Conditions - Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC
xxeis.eis_rsc_ins.rcnh( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC',201,'EXPARPV.CREATION_DATE >= Creation Date From','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CREATION_DATE','','Creation Date From','','','','','EIS_XXWC_PO_AUTO_REAPPRVD_PO_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',201,'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC','EXPARPV.CREATION_DATE >= Creation Date From');
xxeis.eis_rsc_ins.rcnh( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC',201,'EXPARPV.CREATION_DATE <= Creation Date To','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CREATION_DATE','','Creation Date To','','','','','EIS_XXWC_PO_AUTO_REAPPRVD_PO_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',201,'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC','EXPARPV.CREATION_DATE <= Creation Date To');
xxeis.eis_rsc_ins.rcnh( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC',201,'EXPARPV.APPROVED_DATE >= Approved Date From','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','APPROVED_DATE','','Approved Date From','','','','','EIS_XXWC_PO_AUTO_REAPPRVD_PO_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',201,'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC','EXPARPV.APPROVED_DATE >= Approved Date From');
xxeis.eis_rsc_ins.rcnh( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC',201,'EXPARPV.APPROVED_DATE <= Approved Date To','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','APPROVED_DATE','','Approved Date To','','','','','EIS_XXWC_PO_AUTO_REAPPRVD_PO_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',201,'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC','EXPARPV.APPROVED_DATE <= Approved Date To');
xxeis.eis_rsc_ins.rcnh( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC',201,'EXPARPV.VENDOR_NAME IN Vendor Name','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','VENDOR_NAME','','Vendor Name','','','','','EIS_XXWC_PO_AUTO_REAPPRVD_PO_V','','','','','','IN','Y','Y','','','','','1',201,'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC','EXPARPV.VENDOR_NAME IN Vendor Name');
xxeis.eis_rsc_ins.rcnh( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC',201,'EXPARPV.VENDOR_NUMBER IN Vendor Number','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','VENDOR_NUMBER','','Vendor Number','','','','','EIS_XXWC_PO_AUTO_REAPPRVD_PO_V','','','','','','IN','Y','Y','','','','','1',201,'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC','EXPARPV.VENDOR_NUMBER IN Vendor Number');
xxeis.eis_rsc_ins.rcnh( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC',201,'EXPARPV.BRANCH IN Branch','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','BRANCH','','Branch','','','','','EIS_XXWC_PO_AUTO_REAPPRVD_PO_V','','','','','','IN','Y','Y','','','','','1',201,'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC','EXPARPV.BRANCH IN Branch');
xxeis.eis_rsc_ins.rcnh( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC',201,'EXPARPV.BUYER IN Buyer','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','BUYER','','Buyer','','','','','EIS_XXWC_PO_AUTO_REAPPRVD_PO_V','','','','','','IN','Y','Y','','','','','1',201,'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC','EXPARPV.BUYER IN Buyer');
xxeis.eis_rsc_ins.rcnh( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC',201,'EXPARPV.DISTRICT IN District','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','DISTRICT','','District','','','','','EIS_XXWC_PO_AUTO_REAPPRVD_PO_V','','','','','','IN','Y','Y','','','','','1',201,'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC','EXPARPV.DISTRICT IN District');
xxeis.eis_rsc_ins.rcnh( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC',201,'EXPARPV.REGION IN Region','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','REGION','','Region','','','','','EIS_XXWC_PO_AUTO_REAPPRVD_PO_V','','','','','','IN','Y','Y','','','','','1',201,'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC','EXPARPV.REGION IN Region');
xxeis.eis_rsc_ins.rcnh( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC',201,'FreeText','FREE_TEXT','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','','Y','Y','','','','AND NVL (EXPARPV.PO_PREVIOUS_AMOUNT, 0) > 0
AND(ROUND (((NVL(EXPARPV.PO_AMOUNT,0)- NVL(EXPARPV.PO_PREVIOUS_AMOUNT,0)) / NVL(EXPARPV.PO_PREVIOUS_AMOUNT,0)) * 100, 2))>0
AND EXPARPV.PO_AMOUNT  >  EXPARPV.BUYER_APPROVAL_LIMIT
AND EXPARPV.PO_AMOUNT  >  EXPARPV.PO_PREVIOUS_AMOUNT','1',201,'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC','FreeText');
--Inserting Report Sorts - Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC
xxeis.eis_rsc_ins.rs( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC',201,'REGION','ASC','SA059956','1','');
xxeis.eis_rsc_ins.rs( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC',201,'BUYER','ASC','SA059956','2','');
--Inserting Report Triggers - Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC
--inserting report templates - Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC
--Inserting Report Portals - Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC
--inserting report dashboards - Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC','201','EIS_XXWC_PO_AUTO_REAPPRVD_PO_V','EIS_XXWC_PO_AUTO_REAPPRVD_PO_V','N','');
--inserting report security - Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC
xxeis.eis_rsc_ins.rsec( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC','201','','PO_SOUTH',201,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC','201','','SLA_PO_DEV',201,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC','201','','PURCHASING,_VISION_SERVICES_(U',201,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC','201','','PURCHASING_PROJECT_MFG',201,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC','201','','PURCHASING_OPERATIONS',201,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC','201','','PURCHASING,_VISION_DISTRIBUTIO',201,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC','201','','PURCHASING,_VISION_ADB_(USA)',201,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC','201','','PURCHASING_FOR_AX',201,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC','201','','PURCHASING_SUPER_USER',201,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC','201','','PURCHASING_REQUESTOR',201,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC','201','','PURCHASING_RECEIVER',201,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC','201','','OPM APPS PURCHASING',201,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC','201','','PO_INTELLIGENCE_USER',201,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC','201','','PURCHASING_BUYER',201,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC','201','','OPM_PO',201,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC','201','','PO_OPM_RIO',201,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC','201','','OPM_PO_FR',201,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC','201','','PURCHASING_SUPER_USER_G',201,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC','201','','PURCHASING_REQUESTOR_G',201,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC','201','','PURCHASING_RECEIVER_G',201,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC','201','','PO_INTELLIGENCE_USER_G',201,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC','201','','PURCHASING_BUYER_G',201,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC','201','','P2P_PO_USER',201,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC','201','','S',201,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC','201','','SS',201,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC','201','','MRC PURCHASING MANAGER',201,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC','201','','XXWC_SALES_PURCHASING_MGR',201,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC','201','','XXWC_PUR_SUPER_USER',201,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC','201','','HDS_PRCHSNG_SPR_USR',201,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC','201','','XXWC_PURCHASING_SR_MRG_WC',201,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC','201','','XXWC_PURCHASING_RPM',201,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC','201','','XXWC_PURCHASING_MGR',201,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC','201','','XXWC_PURCHASING_INQUIRY',201,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC','201','','XXWC_PURCHASING_BUYER',201,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC','201','','XXWC_EOM_PURCHASING',201,'SA059956','','','');
--Inserting Report Pivots - Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC
--Inserting Report   Version details- Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC
xxeis.eis_rsc_ins.rv( 'Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC','','Purchasing PO Tolerance Auto Re-approval within DOA Limits - WC','SA059956');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 201');
END IF;
END;
/
