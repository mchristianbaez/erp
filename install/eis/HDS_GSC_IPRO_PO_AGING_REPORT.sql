--Report Name            : HDS GSC iPro PO Aging Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data XXCUS_GSC_PO_DETAIL_WITH_REQ_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(201);
IF mod_exist = 'Y' THEN 
--Inserting Object XXCUS_GSC_PO_DETAIL_WITH_REQ_V
xxeis.eis_rsc_ins.v( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V',201,'','','','','ANONYMOUS','XXEIS','Xxcus Gsc Po Detail With Req V','XGPDWRV','','','VIEW','US','','');
--Delete Object Columns for XXCUS_GSC_PO_DETAIL_WITH_REQ_V
xxeis.eis_rsc_utility.delete_view_rows('XXCUS_GSC_PO_DETAIL_WITH_REQ_V',201,FALSE);
--Inserting Object Columns for XXCUS_GSC_PO_DETAIL_WITH_REQ_V
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','POL_CREATED_BY',201,'Pol Created By','POL_CREATED_BY','','','','ANONYMOUS','NUMBER','','','Pol Created By','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_CLOSED_CODE',201,'Po Closed Code','PO_CLOSED_CODE','','','','ANONYMOUS','VARCHAR2','','','Po Closed Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_SHIP_TO_LOCATION',201,'Po Ship To Location','PO_SHIP_TO_LOCATION','','','','ANONYMOUS','VARCHAR2','','','Po Ship To Location','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_SUPPLIER_SITE_CODE',201,'Po Supplier Site Code','PO_SUPPLIER_SITE_CODE','','','','ANONYMOUS','VARCHAR2','','','Po Supplier Site Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_SUPPLIER_NAME',201,'Po Supplier Name','PO_SUPPLIER_NAME','','','','ANONYMOUS','VARCHAR2','','','Po Supplier Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_NUMBER',201,'Po Number','PO_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Po Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_BUYER_NAME',201,'Po Buyer Name','PO_BUYER_NAME','','','','ANONYMOUS','VARCHAR2','','','Po Buyer Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','REQ_PREPARER_NAME',201,'Req Preparer Name','REQ_PREPARER_NAME','','','','ANONYMOUS','VARCHAR2','','','Req Preparer Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_CREATED_BY',201,'Po Created By','PO_CREATED_BY','','','','ANONYMOUS','NUMBER','','','Po Created By','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_LAST_UPDATED_BY',201,'Po Last Updated By','PO_LAST_UPDATED_BY','','','','ANONYMOUS','NUMBER','','','Po Last Updated By','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','OPERATING_UNIT',201,'Operating Unit','OPERATING_UNIT','','','','ANONYMOUS','VARCHAR2','','','Operating Unit','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','REQ_NUMBER',201,'Req Number','REQ_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Req Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_LAST_ACCEPT_DATE',201,'Po Last Accept Date','PO_LAST_ACCEPT_DATE','','','','ANONYMOUS','DATE','','','Po Last Accept Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_PROMISED_DATE',201,'Po Promised Date','PO_PROMISED_DATE','','','','ANONYMOUS','DATE','','','Po Promised Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_NEED_BY_DATE',201,'Po Need By Date','PO_NEED_BY_DATE','','','','ANONYMOUS','DATE','','','Po Need By Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','POL_CLOSED_CODE',201,'Pol Closed Code','POL_CLOSED_CODE','','','','ANONYMOUS','VARCHAR2','','','Pol Closed Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','REQUESTOR_EMAIL',201,'Requestor Email','REQUESTOR_EMAIL','','','','ANONYMOUS','VARCHAR2','','','Requestor Email','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','GCC#50328#PROJECT_CODE#DESCR',201,'Gcc#50328#Project Code#Descr','GCC#50328#PROJECT_CODE#DESCR','','','','ANONYMOUS','VARCHAR2','','','Gcc#50328#Project Code#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','GCC#50328#PROJECT_CODE',201,'Gcc#50328#Project Code','GCC#50328#PROJECT_CODE','','','','ANONYMOUS','VARCHAR2','','','Gcc#50328#Project Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_AMOUNT_BILLED',201,'Po Amount Billed','PO_AMOUNT_BILLED','','~T~D~2','','ANONYMOUS','NUMBER','','','Po Amount Billed','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_AMOUNT_DUE',201,'Po Amount Due','PO_AMOUNT_DUE','','~T~D~2','','ANONYMOUS','NUMBER','','','Po Amount Due','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_AMOUNT_RECEIVED',201,'Po Amount Received','PO_AMOUNT_RECEIVED','','~T~D~2','','ANONYMOUS','NUMBER','','','Po Amount Received','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_AMOUNT_CANCELLED',201,'Po Amount Cancelled','PO_AMOUNT_CANCELLED','','~T~D~2','','ANONYMOUS','NUMBER','','','Po Amount Cancelled','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_AMOUNT_ORDERED',201,'Po Amount Ordered','PO_AMOUNT_ORDERED','','~T~D~2','','ANONYMOUS','NUMBER','','','Po Amount Ordered','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_QUANTITY_BILLED',201,'Po Quantity Billed','PO_QUANTITY_BILLED','','~T~D~2','','ANONYMOUS','NUMBER','','','Po Quantity Billed','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_QUANTITY_DUE',201,'Po Quantity Due','PO_QUANTITY_DUE','','~T~D~2','','ANONYMOUS','NUMBER','','','Po Quantity Due','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_QUANTITY_RECEIVED',201,'Po Quantity Received','PO_QUANTITY_RECEIVED','','~T~D~2','','ANONYMOUS','NUMBER','','','Po Quantity Received','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_QUANTITY_CANCELLED',201,'Po Quantity Cancelled','PO_QUANTITY_CANCELLED','','~T~D~2','','ANONYMOUS','NUMBER','','','Po Quantity Cancelled','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_QUANTITY_ORDERED',201,'Po Quantity Ordered','PO_QUANTITY_ORDERED','','~T~D~2','','ANONYMOUS','NUMBER','','','Po Quantity Ordered','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_UNIT_PRICE',201,'Po Unit Price','PO_UNIT_PRICE','','~T~D~2','','ANONYMOUS','NUMBER','','','Po Unit Price','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_ITEM_DESCRIPTION',201,'Po Item Description','PO_ITEM_DESCRIPTION','','','','ANONYMOUS','VARCHAR2','','','Po Item Description','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_ITEM_CATEGORY',201,'Po Item Category','PO_ITEM_CATEGORY','','','','ANONYMOUS','VARCHAR2','','','Po Item Category','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_ITEM_NUMBER',201,'Po Item Number','PO_ITEM_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Po Item Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_LINE_NUM',201,'Po Line Num','PO_LINE_NUM','','','','ANONYMOUS','NUMBER','','','Po Line Num','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_LINE_TYPE',201,'Po Line Type','PO_LINE_TYPE','','','','ANONYMOUS','VARCHAR2','','','Po Line Type','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','POL_CREATED_BY_NAME',201,'Pol Created By Name','POL_CREATED_BY_NAME','','','','ANONYMOUS','VARCHAR2','','','Pol Created By Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','POL_CREATION_DATE',201,'Pol Creation Date','POL_CREATION_DATE','','','','ANONYMOUS','DATE','','','Pol Creation Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','POL_LAST_UPDATED_BY',201,'Pol Last Updated By','POL_LAST_UPDATED_BY','','','','ANONYMOUS','NUMBER','','','Pol Last Updated By','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','POL_LAST_UPDATE_DATE',201,'Pol Last Update Date','POL_LAST_UPDATE_DATE','','','','ANONYMOUS','DATE','','','Pol Last Update Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_LINE_ID',201,'Po Line Id','PO_LINE_ID','','','','ANONYMOUS','NUMBER','','','Po Line Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_CLOSED_DATE',201,'Po Closed Date','PO_CLOSED_DATE','','','','ANONYMOUS','DATE','','','Po Closed Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_COMMENTS',201,'Po Comments','PO_COMMENTS','','','','ANONYMOUS','VARCHAR2','','','Po Comments','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_NOTE_TO_RECEIVER',201,'Po Note To Receiver','PO_NOTE_TO_RECEIVER','','','','ANONYMOUS','VARCHAR2','','','Po Note To Receiver','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_NOTE_TO_SUPPLIER',201,'Po Note To Supplier','PO_NOTE_TO_SUPPLIER','','','','ANONYMOUS','VARCHAR2','','','Po Note To Supplier','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_NOTE_TO_AUTHORIZER',201,'Po Note To Authorizer','PO_NOTE_TO_AUTHORIZER','','','','ANONYMOUS','VARCHAR2','','','Po Note To Authorizer','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_ACCEPTANCE_REQUIRED',201,'Po Acceptance Required','PO_ACCEPTANCE_REQUIRED','','','','ANONYMOUS','VARCHAR2','','','Po Acceptance Required','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_ON_HOLD',201,'Po On Hold','PO_ON_HOLD','','','','ANONYMOUS','VARCHAR2','','','Po On Hold','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_ACCEPTANCE_DUE_DATE',201,'Po Acceptance Due Date','PO_ACCEPTANCE_DUE_DATE','','','','ANONYMOUS','DATE','','','Po Acceptance Due Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_CURRENCY_CODE',201,'Po Currency Code','PO_CURRENCY_CODE','','','','ANONYMOUS','VARCHAR2','','','Po Currency Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_APPROVED_DATE',201,'Po Approved Date','PO_APPROVED_DATE','','','','ANONYMOUS','DATE','','','Po Approved Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_PRINTED_DATE',201,'Po Printed Date','PO_PRINTED_DATE','','','','ANONYMOUS','DATE','','','Po Printed Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_REVISED_DATE',201,'Po Revised Date','PO_REVISED_DATE','','','','ANONYMOUS','DATE','','','Po Revised Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_REVISION_NUM',201,'Po Revision Num','PO_REVISION_NUM','','','','ANONYMOUS','NUMBER','','','Po Revision Num','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_STATUS',201,'Po Status','PO_STATUS','','','','ANONYMOUS','VARCHAR2','','','Po Status','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_FREIGHT_TERMS',201,'Po Freight Terms','PO_FREIGHT_TERMS','','','','ANONYMOUS','VARCHAR2','','','Po Freight Terms','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_FOB',201,'Po Fob','PO_FOB','','','','ANONYMOUS','VARCHAR2','','','Po Fob','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_SHIP_VIA',201,'Po Ship Via','PO_SHIP_VIA','','','','ANONYMOUS','VARCHAR2','','','Po Ship Via','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_TERMS',201,'Po Terms','PO_TERMS','','','','ANONYMOUS','VARCHAR2','','','Po Terms','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_END_DATE',201,'Po End Date','PO_END_DATE','','','','ANONYMOUS','DATE','','','Po End Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_START_DATE',201,'Po Start Date','PO_START_DATE','','','','ANONYMOUS','DATE','','','Po Start Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_ENABLED',201,'Po Enabled','PO_ENABLED','','','','ANONYMOUS','VARCHAR2','','','Po Enabled','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_SUMMARY',201,'Po Summary','PO_SUMMARY','','','','ANONYMOUS','VARCHAR2','','','Po Summary','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','SUPPLIER_CONTACT_PHONE',201,'Supplier Contact Phone','SUPPLIER_CONTACT_PHONE','','','','ANONYMOUS','VARCHAR2','','','Supplier Contact Phone','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','SUPPLIER_CONTACT_AREA_CODE',201,'Supplier Contact Area Code','SUPPLIER_CONTACT_AREA_CODE','','','','ANONYMOUS','VARCHAR2','','','Supplier Contact Area Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','SUPPLIER_CONTACT_LAST_NAME',201,'Supplier Contact Last Name','SUPPLIER_CONTACT_LAST_NAME','','','','ANONYMOUS','VARCHAR2','','','Supplier Contact Last Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','SUPPLIER_CONTACT_FIRST_NAME',201,'Supplier Contact First Name','SUPPLIER_CONTACT_FIRST_NAME','','','','ANONYMOUS','VARCHAR2','','','Supplier Contact First Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_TYPE',201,'Po Type','PO_TYPE','','','','ANONYMOUS','VARCHAR2','','','Po Type','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','REQ_PREPARER_ID',201,'Req Preparer Id','REQ_PREPARER_ID','','','','ANONYMOUS','NUMBER','','','Req Preparer Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','REQ_CREATED_BY_NAME',201,'Req Created By Name','REQ_CREATED_BY_NAME','','','','ANONYMOUS','VARCHAR2','','','Req Created By Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','REQ_CREATED_BY',201,'Req Created By','REQ_CREATED_BY','','','','ANONYMOUS','NUMBER','','','Req Created By','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_CREATED_BY_NAME',201,'Po Created By Name','PO_CREATED_BY_NAME','','','','ANONYMOUS','VARCHAR2','','','Po Created By Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_CREATION_DATE',201,'Po Creation Date','PO_CREATION_DATE','','','','ANONYMOUS','DATE','','','Po Creation Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_LAST_UPDATED_BY_NAME',201,'Po Last Updated By Name','PO_LAST_UPDATED_BY_NAME','','','','ANONYMOUS','VARCHAR2','','','Po Last Updated By Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_LAST_UPDATE_DATE',201,'Po Last Update Date','PO_LAST_UPDATE_DATE','','','','ANONYMOUS','DATE','','','Po Last Update Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_HEADER_ID',201,'Po Header Id','PO_HEADER_ID','','','','ANONYMOUS','NUMBER','','','Po Header Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','MSI#WC#AVERAGE_UNITS',201,'Msi#Wc#Average Units','MSI#WC#AVERAGE_UNITS','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Average Units','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','MSI#WC#TAXWARE_CODE',201,'Msi#Wc#Taxware Code','MSI#WC#TAXWARE_CODE','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Taxware Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','MSI#WC#RESERVE_STOCK',201,'Msi#Wc#Reserve Stock','MSI#WC#RESERVE_STOCK','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Reserve Stock','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','MSI#WC#AMU',201,'Msi#Wc#Amu','MSI#WC#AMU','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Amu','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','MSI#WC#LAST_LEAD_TIME',201,'Msi#Wc#Last Lead Time','MSI#WC#LAST_LEAD_TIME','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Last Lead Time','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','MSI#WC#GTP_INDICATOR',201,'Msi#Wc#Gtp Indicator','MSI#WC#GTP_INDICATOR','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Gtp Indicator','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','MSI#WC#HAZMAT_CONTAINER',201,'Msi#Wc#Hazmat Container','MSI#WC#HAZMAT_CONTAINER','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Hazmat Container','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','MSI#WC#HAZMAT_DESCRIPTION',201,'Msi#Wc#Hazmat Description','MSI#WC#HAZMAT_DESCRIPTION','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Hazmat Description','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','MSI#WC#PRISM_PART_NUMBER',201,'Msi#Wc#Prism Part Number','MSI#WC#PRISM_PART_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Prism Part Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','MSI#WC#YEARLY_DC_VELOCITY',201,'Msi#Wc#Yearly Dc Velocity','MSI#WC#YEARLY_DC_VELOCITY','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Yearly Dc Velocity','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','MSI#WC#YEARLY_STORE_VELOCITY',201,'Msi#Wc#Yearly Store Velocity','MSI#WC#YEARLY_STORE_VELOCITY','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Yearly Store Velocity','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','MSI#WC#DC_VELOCITY',201,'Msi#Wc#Dc Velocity','MSI#WC#DC_VELOCITY','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Dc Velocity','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','MSI#WC#STORE_VELOCITY',201,'Msi#Wc#Store Velocity','MSI#WC#STORE_VELOCITY','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Store Velocity','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','MSI#WC#ORM_D_FLAG',201,'Msi#Wc#Orm D Flag','MSI#WC#ORM_D_FLAG','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Orm D Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','MSI#WC#COUNTRY_OF_ORIGIN',201,'Msi#Wc#Country Of Origin','MSI#WC#COUNTRY_OF_ORIGIN','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Country Of Origin','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','MSI#WC#CA_PROP_65',201,'Msi#Wc#Ca Prop 65','MSI#WC#CA_PROP_65','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Ca Prop 65','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','MSI#HDS#SKU_DESCRIPTION',201,'Msi#Hds#Sku Description','MSI#HDS#SKU_DESCRIPTION','','','','ANONYMOUS','VARCHAR2','','','Msi#Hds#Sku Description','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','MSI#HDS#UPC_PRIMARY',201,'Msi#Hds#Upc Primary','MSI#HDS#UPC_PRIMARY','','','','ANONYMOUS','VARCHAR2','','','Msi#Hds#Upc Primary','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','MSI#HDS#UNSPSC_CODE',201,'Msi#Hds#Unspsc Code','MSI#HDS#UNSPSC_CODE','','','','ANONYMOUS','VARCHAR2','','','Msi#Hds#Unspsc Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','MSI#HDS#VENDOR_PART_NUMBER',201,'Msi#Hds#Vendor Part Number','MSI#HDS#VENDOR_PART_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Msi#Hds#Vendor Part Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','MSI#HDS#PRODUCT_ID',201,'Msi#Hds#Product Id','MSI#HDS#PRODUCT_ID','','','','ANONYMOUS','VARCHAR2','','','Msi#Hds#Product Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','MSI#HDS#INVOICE_UOM',201,'Msi#Hds#Invoice Uom','MSI#HDS#INVOICE_UOM','','','','ANONYMOUS','VARCHAR2','','','Msi#Hds#Invoice Uom','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','MSI#HDS#DROP_SHIPMENT_ELIGAB',201,'Msi#Hds#Drop Shipment Eligab','MSI#HDS#DROP_SHIPMENT_ELIGAB','','','','ANONYMOUS','VARCHAR2','','','Msi#Hds#Drop Shipment Eligab','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','MSI#HDS#LOB',201,'Msi#Hds#Lob','MSI#HDS#LOB','','','','ANONYMOUS','VARCHAR2','','','Msi#Hds#Lob','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','GCC#BRANCH',201,'Gcc#Branch','GCC#BRANCH','','','','ANONYMOUS','VARCHAR2','','','Gcc#Branch','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PROJECT_ID',201,'Project Id','PROJECT_ID','','','','ANONYMOUS','NUMBER','','','Project Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PROJECT_NAME',201,'Project Name','PROJECT_NAME','','','','ANONYMOUS','VARCHAR2','','','Project Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PROJECT_NUMBER',201,'Project Number','PROJECT_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Project Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','CODE_COMBINATION_ID',201,'Code Combination Id','CODE_COMBINATION_ID','','','','ANONYMOUS','NUMBER','','','Code Combination Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','AGENT_ID',201,'Agent Id','AGENT_ID','','','','ANONYMOUS','NUMBER','','','Agent Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','LOCATION_ID',201,'Location Id','LOCATION_ID','','','','ANONYMOUS','NUMBER','','','Location Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','INVENTORY_ITEM_ID',201,'Inventory Item Id','INVENTORY_ITEM_ID','','','','ANONYMOUS','NUMBER','','','Inventory Item Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','VENDOR_ID',201,'Vendor Id','VENDOR_ID','','','','ANONYMOUS','NUMBER','','','Vendor Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','ORG_ID',201,'Org Id','ORG_ID','','','','ANONYMOUS','NUMBER','','','Org Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','REQ_LINE_QTY',201,'Req Line Qty','REQ_LINE_QTY','','~T~D~2','','ANONYMOUS','NUMBER','','','Req Line Qty','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','REQ_LINE_UNIT_PRICE',201,'Req Line Unit Price','REQ_LINE_UNIT_PRICE','','~T~D~2','','ANONYMOUS','NUMBER','','','Req Line Unit Price','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','REQ_LINE_ITEM_DESCRIPTION',201,'Req Line Item Description','REQ_LINE_ITEM_DESCRIPTION','','','','ANONYMOUS','VARCHAR2','','','Req Line Item Description','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','REQ_LINE_NUM',201,'Req Line Num','REQ_LINE_NUM','','','','ANONYMOUS','NUMBER','','','Req Line Num','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','REQ_TYPE',201,'Req Type','REQ_TYPE','','','','ANONYMOUS','VARCHAR2','','','Req Type','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','REQ_STATUS',201,'Req Status','REQ_STATUS','','','','ANONYMOUS','VARCHAR2','','','Req Status','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','REQ_DESCRIPTION',201,'Req Description','REQ_DESCRIPTION','','','','ANONYMOUS','VARCHAR2','','','Req Description','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','REQUISITION_HEADER_ID',201,'Requisition Header Id','REQUISITION_HEADER_ID','','','','ANONYMOUS','NUMBER','','','Requisition Header Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','REQUISITION_LINE_ID',201,'Requisition Line Id','REQUISITION_LINE_ID','','','','ANONYMOUS','NUMBER','','','Requisition Line Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','DISTRIBUTION_ID',201,'Distribution Id','DISTRIBUTION_ID','','','','ANONYMOUS','NUMBER','','','Distribution Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_VARIANCE_ACCOUNT',201,'Po Variance Account','PO_VARIANCE_ACCOUNT','','','','ANONYMOUS','VARCHAR2','','','Po Variance Account','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_VARIANCE_CCID',201,'Po Variance Ccid','PO_VARIANCE_CCID','','','','ANONYMOUS','NUMBER','','','Po Variance Ccid','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_ACCRUAL_ACCOUNT',201,'Po Accrual Account','PO_ACCRUAL_ACCOUNT','','','','ANONYMOUS','VARCHAR2','','','Po Accrual Account','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_ACCRUAL_CCID',201,'Po Accrual Ccid','PO_ACCRUAL_CCID','','','','ANONYMOUS','NUMBER','','','Po Accrual Ccid','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_CHARGE_ACCOUNT',201,'Po Charge Account','PO_CHARGE_ACCOUNT','','','','ANONYMOUS','VARCHAR2','','','Po Charge Account','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_CHARGE_CCID',201,'Po Charge Ccid','PO_CHARGE_CCID','','','','ANONYMOUS','NUMBER','','','Po Charge Ccid','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_SET_OF_BOOKS',201,'Po Set Of Books','PO_SET_OF_BOOKS','','','','ANONYMOUS','VARCHAR2','','','Po Set Of Books','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','SHIPMENT_RECEIPT_DATE',201,'Shipment Receipt Date','SHIPMENT_RECEIPT_DATE','','','','ANONYMOUS','DATE','','','Shipment Receipt Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_DISTRIBUTION_NUM',201,'Po Distribution Num','PO_DISTRIBUTION_NUM','','','','ANONYMOUS','NUMBER','','','Po Distribution Num','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_DISTRIBUTION_ID',201,'Po Distribution Id','PO_DISTRIBUTION_ID','','','','ANONYMOUS','NUMBER','','','Po Distribution Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_ACCRUE_ON_RECEIPT',201,'Po Accrue On Receipt','PO_ACCRUE_ON_RECEIPT','','','','ANONYMOUS','VARCHAR2','','','Po Accrue On Receipt','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_RECEIVE_CLOSE_TOLERANCE',201,'Po Receive Close Tolerance','PO_RECEIVE_CLOSE_TOLERANCE','','','','ANONYMOUS','NUMBER','','','Po Receive Close Tolerance','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_INVOICE_CLOSE_TOLERANCE',201,'Po Invoice Close Tolerance','PO_INVOICE_CLOSE_TOLERANCE','','','','ANONYMOUS','NUMBER','','','Po Invoice Close Tolerance','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_DAYS_LATE_RECEIPT_ALLOWED',201,'Po Days Late Receipt Allowed','PO_DAYS_LATE_RECEIPT_ALLOWED','','','','ANONYMOUS','NUMBER','','','Po Days Late Receipt Allowed','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_DAYS_EARLY_RECEIPT_ALLOWED',201,'Po Days Early Receipt Allowed','PO_DAYS_EARLY_RECEIPT_ALLOWED','','','','ANONYMOUS','NUMBER','','','Po Days Early Receipt Allowed','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_QTY_RCV_TOLERANCE',201,'Po Qty Rcv Tolerance','PO_QTY_RCV_TOLERANCE','','~T~D~2','','ANONYMOUS','NUMBER','','','Po Qty Rcv Tolerance','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_3_WAY_MATCH',201,'Po 3 Way Match','PO_3_WAY_MATCH','','','','ANONYMOUS','VARCHAR2','','','Po 3 Way Match','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_4_WAY_MATCH',201,'Po 4 Way Match','PO_4_WAY_MATCH','','','','ANONYMOUS','VARCHAR2','','','Po 4 Way Match','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_SHIPMENT_TYPE',201,'Po Shipment Type','PO_SHIPMENT_TYPE','','','','ANONYMOUS','VARCHAR2','','','Po Shipment Type','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_SHIPMENT_NUM',201,'Po Shipment Num','PO_SHIPMENT_NUM','','','','ANONYMOUS','NUMBER','','','Po Shipment Num','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_LINE_LOCATION_ID',201,'Po Line Location Id','PO_LINE_LOCATION_ID','','','','ANONYMOUS','NUMBER','','','Po Line Location Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','POL_CLOSED_REASON',201,'Pol Closed Reason','POL_CLOSED_REASON','','','','ANONYMOUS','VARCHAR2','','','Pol Closed Reason','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','POL_CLOSED_DATE',201,'Pol Closed Date','POL_CLOSED_DATE','','','','ANONYMOUS','DATE','','','Pol Closed Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_TYPE_1099',201,'Po Type 1099','PO_TYPE_1099','','','','ANONYMOUS','VARCHAR2','','','Po Type 1099','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_TAX_NAME',201,'Po Tax Name','PO_TAX_NAME','','','','ANONYMOUS','VARCHAR2','','','Po Tax Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_TAXABLE_FLAG',201,'Po Taxable Flag','PO_TAXABLE_FLAG','','','','ANONYMOUS','VARCHAR2','','','Po Taxable Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','POL_CANCEL_REASON',201,'Pol Cancel Reason','POL_CANCEL_REASON','','','','ANONYMOUS','VARCHAR2','','','Pol Cancel Reason','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','POL_CANCEL_DATE',201,'Pol Cancel Date','POL_CANCEL_DATE','','','','ANONYMOUS','DATE','','','Pol Cancel Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','POL_CANCEL',201,'Pol Cancel','POL_CANCEL','','','','ANONYMOUS','VARCHAR2','','','Pol Cancel','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_MARKET_PRICE',201,'Po Market Price','PO_MARKET_PRICE','','~T~D~2','','ANONYMOUS','NUMBER','','','Po Market Price','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_LIST_PRICE',201,'Po List Price','PO_LIST_PRICE','','~T~D~2','','ANONYMOUS','NUMBER','','','Po List Price','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_UOM',201,'Po Uom','PO_UOM','','','','ANONYMOUS','VARCHAR2','','','Po Uom','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','PO_ITEM_REVISION',201,'Po Item Revision','PO_ITEM_REVISION','','','','ANONYMOUS','VARCHAR2','','','Po Item Revision','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','GCC#50368#SUBACCOUNT#DESCR',201,'Gcc#50368#Subaccount#Descr','GCC#50368#SUBACCOUNT#DESCR','','','','ANONYMOUS','VARCHAR2','','','Gcc#50368#Subaccount#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','GCC#50368#SUBACCOUNT',201,'Gcc#50368#Subaccount','GCC#50368#SUBACCOUNT','','','','ANONYMOUS','VARCHAR2','','','Gcc#50368#Subaccount','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','GCC#50368#PRODUCT#DESCR',201,'Gcc#50368#Product#Descr','GCC#50368#PRODUCT#DESCR','','','','ANONYMOUS','VARCHAR2','','','Gcc#50368#Product#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','GCC#50368#PRODUCT',201,'Gcc#50368#Product','GCC#50368#PRODUCT','','','','ANONYMOUS','VARCHAR2','','','Gcc#50368#Product','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','GCC#50368#FUTURE_USE#DESCR',201,'Gcc#50368#Future Use#Descr','GCC#50368#FUTURE_USE#DESCR','','','','ANONYMOUS','VARCHAR2','','','Gcc#50368#Future Use#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','GCC#50368#FUTURE_USE',201,'Gcc#50368#Future Use','GCC#50368#FUTURE_USE','','','','ANONYMOUS','VARCHAR2','','','Gcc#50368#Future Use','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','GCC#50368#DIVISION#DESCR',201,'Gcc#50368#Division#Descr','GCC#50368#DIVISION#DESCR','','','','ANONYMOUS','VARCHAR2','','','Gcc#50368#Division#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','GCC#50368#DIVISION',201,'Gcc#50368#Division','GCC#50368#DIVISION','','','','ANONYMOUS','VARCHAR2','','','Gcc#50368#Division','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','GCC#50368#DEPARTMENT#DESCR',201,'Gcc#50368#Department#Descr','GCC#50368#DEPARTMENT#DESCR','','','','ANONYMOUS','VARCHAR2','','','Gcc#50368#Department#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','GCC#50368#DEPARTMENT',201,'Gcc#50368#Department','GCC#50368#DEPARTMENT','','','','ANONYMOUS','VARCHAR2','','','Gcc#50368#Department','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','GCC#50368#ACCOUNT#DESCR',201,'Gcc#50368#Account#Descr','GCC#50368#ACCOUNT#DESCR','','','','ANONYMOUS','VARCHAR2','','','Gcc#50368#Account#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','GCC#50368#ACCOUNT',201,'Gcc#50368#Account','GCC#50368#ACCOUNT','','','','ANONYMOUS','VARCHAR2','','','Gcc#50368#Account','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','GCC#50328#FUTURE_USE_2#DESCR',201,'Gcc#50328#Future Use 2#Descr','GCC#50328#FUTURE_USE_2#DESCR','','','','ANONYMOUS','VARCHAR2','','','Gcc#50328#Future Use 2#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','GCC#50328#FUTURE_USE_2',201,'Gcc#50328#Future Use 2','GCC#50328#FUTURE_USE_2','','','','ANONYMOUS','VARCHAR2','','','Gcc#50328#Future Use 2','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','GCC#50328#FURTURE_USE#DESCR',201,'Gcc#50328#Furture Use#Descr','GCC#50328#FURTURE_USE#DESCR','','','','ANONYMOUS','VARCHAR2','','','Gcc#50328#Furture Use#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','GCC#50328#FURTURE_USE',201,'Gcc#50328#Furture Use','GCC#50328#FURTURE_USE','','','','ANONYMOUS','VARCHAR2','','','Gcc#50328#Furture Use','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','GCC#50328#ACCOUNT#DESCR',201,'Gcc#50328#Account#Descr','GCC#50328#ACCOUNT#DESCR','','','','ANONYMOUS','VARCHAR2','','','Gcc#50328#Account#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','GCC#50328#ACCOUNT',201,'Gcc#50328#Account','GCC#50328#ACCOUNT','','','','ANONYMOUS','VARCHAR2','','','Gcc#50328#Account','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','GCC#50328#COST_CENTER#DESCR',201,'Gcc#50328#Cost Center#Descr','GCC#50328#COST_CENTER#DESCR','','','','ANONYMOUS','VARCHAR2','','','Gcc#50328#Cost Center#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','GCC#50328#COST_CENTER',201,'Gcc#50328#Cost Center','GCC#50328#COST_CENTER','','','','ANONYMOUS','VARCHAR2','','','Gcc#50328#Cost Center','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','GCC#50328#LOCATION#DESCR',201,'Gcc#50328#Location#Descr','GCC#50328#LOCATION#DESCR','','','','ANONYMOUS','VARCHAR2','','','Gcc#50328#Location#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','GCC#50328#LOCATION',201,'Gcc#50328#Location','GCC#50328#LOCATION','','','','ANONYMOUS','VARCHAR2','','','Gcc#50328#Location','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','GCC#50328#PRODUCT#DESCR',201,'Gcc#50328#Product#Descr','GCC#50328#PRODUCT#DESCR','','','','ANONYMOUS','VARCHAR2','','','Gcc#50328#Product#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','GCC#50328#PRODUCT',201,'Gcc#50328#Product','GCC#50328#PRODUCT','','','','ANONYMOUS','VARCHAR2','','','Gcc#50328#Product','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','POH#STANDARDP#FOB_TERMS_TAB',201,'Poh#Standardp#Fob Terms Tab','POH#STANDARDP#FOB_TERMS_TAB','','','','ANONYMOUS','VARCHAR2','','','Poh#Standardp#Fob Terms Tab','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','POH#STANDARDP#CARRIER_TERMS_',201,'Poh#Standardp#Carrier Terms ','POH#STANDARDP#CARRIER_TERMS_','','','','ANONYMOUS','VARCHAR2','','','Poh#Standardp#Carrier Terms ','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','POH#STANDARDP#FREIGHT_TERMS_',201,'Poh#Standardp#Freight Terms ','POH#STANDARDP#FREIGHT_TERMS_','','','','ANONYMOUS','VARCHAR2','','','Poh#Standardp#Freight Terms ','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','POH#STANDARDP#NEED_BY_DATE',201,'Poh#Standardp#Need By Date','POH#STANDARDP#NEED_BY_DATE','','','','ANONYMOUS','VARCHAR2','','','Poh#Standardp#Need By Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','MSI#WC#HAZMAT_PACKAGING_GROU',201,'Msi#Wc#Hazmat Packaging Grou','MSI#WC#HAZMAT_PACKAGING_GROU','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Hazmat Packaging Grou','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','MSI#WC#MSDS_#',201,'Msi#Wc#Msds #','MSI#WC#MSDS_#','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Msds #','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','MSI#WC#VOC_SUB_CATEGORY',201,'Msi#Wc#Voc Sub Category','MSI#WC#VOC_SUB_CATEGORY','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Voc Sub Category','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','MSI#WC#VOC_CATEGORY',201,'Msi#Wc#Voc Category','MSI#WC#VOC_CATEGORY','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Voc Category','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','MSI#WC#PESTICIDE_FLAG_STATE',201,'Msi#Wc#Pesticide Flag State','MSI#WC#PESTICIDE_FLAG_STATE','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Pesticide Flag State','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','MSI#WC#VOC_GL',201,'Msi#Wc#Voc Gl','MSI#WC#VOC_GL','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Voc Gl','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','MSI#WC#CALC_LEAD_TIME',201,'Msi#Wc#Calc Lead Time','MSI#WC#CALC_LEAD_TIME','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Calc Lead Time','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','MSI#WC#PESTICIDE_FLAG',201,'Msi#Wc#Pesticide Flag','MSI#WC#PESTICIDE_FLAG','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Pesticide Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','MSI#WC#KEEP_ITEM_ACTIVE',201,'Msi#Wc#Keep Item Active','MSI#WC#KEEP_ITEM_ACTIVE','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Keep Item Active','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','MSI#WC#IMPORT_DUTY_',201,'Msi#Wc#Import Duty ','MSI#WC#IMPORT_DUTY_','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Import Duty ','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','MSI#WC#PRODUCT_CODE',201,'Msi#Wc#Product Code','MSI#WC#PRODUCT_CODE','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Product Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','LINE_LOCATION_ID',201,'Line Location Id','LINE_LOCATION_ID','','','','ANONYMOUS','NUMBER','','','Line Location Id','','','','');
xxeis.eis_rsc_ins.vc( 'XXCUS_GSC_PO_DETAIL_WITH_REQ_V','RECEIPT_DATE',201,'Receipt Date','RECEIPT_DATE','','','','ANONYMOUS','DATE','','','Receipt Date','','','','');
--Inserting Object Components for XXCUS_GSC_PO_DETAIL_WITH_REQ_V
--Inserting Object Component Joins for XXCUS_GSC_PO_DETAIL_WITH_REQ_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 201');
END IF;
END;
/
prompt Creating Report LOV Data for HDS GSC iPro PO Aging Report
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(201);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - HDS GSC iPro PO Aging Report
xxeis.eis_rsc_ins.lov( 201,'select agent_name buyer_name from po_agents_v','','EIS_PO_BUYER_LOV','List of Values for Buyer','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'select poh.segment1 PO_NUMBER,HOU.name OPERATING_UNIT FROM PO_HEADERS_ALL POH,HR_OPERATING_UNITS HOU
where POH.ORG_ID=HOU.ORGANIZATION_ID
and exists(select 1 from XXEIS.EIS_MO_ORG_TMP_V
            WHERE ORG_ID=POH.ORG_ID)','','EIS_PO_PURCHASE_ORDER_NUM_LOV','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'select PRH.SEGMENT1 Requisition_number,HOU.name OPERATING_UNIT from PO_REQUISITION_HEADERS_ALL PRH,HR_OPERATING_UNITS HOU
where PRH.ORG_ID=HOU.ORGANIZATION_ID
and exists(select 1 from XXEIS.EIS_MO_ORG_TMP_V
            WHERE ORG_ID=PRH.ORG_ID)
order by PRH.SEGMENT1','','EIS_PO_REQ_LOV','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'select vendor_name from po_vendors','','EIS_PO_SUPPLIER_LOV','','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'SELECT lookup_code, meaning FROM apps.mfg_lookups
WHERE LOOKUP_TYPE = ''INV_SRS_PRECISION''
and enabled_flag = ''Y''
order by LOOKUP_CODE','','EIS_PO_NUMBER_PRECISION_LOV','','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'SELECT hou.NAME organization_name,
       TO_CHAR (date_from,
                NVL (fnd_profile.VALUE (''ICX_DATE_FORMAT_MASK''),
                     ''DD-MON-YYYY'')
               ) date_from,
       TO_CHAR (date_to,
                NVL (fnd_profile.VALUE (''ICX_DATE_FORMAT_MASK''),
                     ''DD-MON-YYYY'')
               ) date_to
  FROM hr_operating_units hou,
       mo_glob_org_access_tmp tmp
 WHERE hou.organization_id = tmp.organization_id','','EIS_MULTI_OPERATING_UNIT_LOV','This LOV shows all the operating units, to which user has access.','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 201');
END IF;
END;
/
prompt Creating Report Data for HDS GSC iPro PO Aging Report
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(201);
IF mod_exist = 'Y' THEN 
--Deleting Report data - HDS GSC iPro PO Aging Report
xxeis.eis_rsc_utility.delete_report_rows( 'HDS GSC iPro PO Aging Report' );
--Inserting Report - HDS GSC iPro PO Aging Report
xxeis.eis_rsc_ins.r( 201,'HDS GSC iPro PO Aging Report','','This report lists purchase order, line, shipment and distribution details along with purchase order requisition, line and distribution details created within a specified date range, supplier range, buyer(s), purchase requisition or purchase order range. This report can be used to track the purchase orders back to the requisitions thereby monitoring that all purchase orders have been created against proper requisitions. This also includes the Receipt Date and Requestor Created By.

Per Project: 24066

','','','','ID020048','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','Y','','','ID020048','','N','iProcurement','','CSV,Pivot Excel,EXCEL,','N','','','','','','','','US','','','','');
--Inserting Report Columns - HDS GSC iPro PO Aging Report
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro PO Aging Report',201,'GCC#50328#PROJECT_CODE','Gcc#50328#Project Code','Gcc#50328#Project Code','','','','','33','N','Y','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro PO Aging Report',201,'GCC#50328#PROJECT_CODE#DESCR','Gcc#50328#Project Code#Descr','Gcc#50328#Project Code#Descr','','','','','34','N','Y','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro PO Aging Report',201,'POL_CREATED_BY','Pol Created By','Pol Created By','','~~~','','','37','N','Y','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro PO Aging Report',201,'PO_BUYER_NAME','Po Buyer Name','Po Buyer Name','','','','','38','N','Y','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro PO Aging Report',201,'PO_CLOSED_CODE','Po Closed Code','Po Closed Code','','','','','27','N','Y','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro PO Aging Report',201,'PO_CREATED_BY','Po Created By','Po Created By','','~~~','','','39','N','Y','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro PO Aging Report',201,'PO_LAST_ACCEPT_DATE','Po Last Accept Date','Po Last Accept Date','','','','','31','N','Y','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro PO Aging Report',201,'PO_LAST_UPDATED_BY','Po Last Updated By','Po Last Updated By','','~~~','','','40','N','Y','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro PO Aging Report',201,'PO_NEED_BY_DATE','Po Need By Date','Po Need By Date','','','','','29','N','Y','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro PO Aging Report',201,'PO_PROMISED_DATE','Po Promised Date','Po Promised Date','','','','','30','N','Y','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro PO Aging Report',201,'PO_SHIP_TO_LOCATION','Po Ship To Location','Po Ship To Location','','','','','36','N','Y','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro PO Aging Report',201,'PO_SUPPLIER_SITE_CODE','Po Supplier Site Code','Po Supplier Site Code','','','','','32','N','Y','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro PO Aging Report',201,'REQUESTOR_EMAIL','Requestor Email','Requestor Email','','','','','35','N','Y','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro PO Aging Report',201,'REQ_PREPARER_NAME','Req Preparer Name','Req Preparer Name','','','default','','41','N','Y','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro PO Aging Report',201,'POL_CLOSED_CODE','Pol Closed Code','Pol Closed Code','','','','','28','N','Y','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro PO Aging Report',201,'OPERATING_UNIT','Operating Unit','Operating Unit','','','default','','1','N','Y','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro PO Aging Report',201,'PO_DISTRIBUTION_NUM','Po Distribution Num','Po Distribution Num','','~~~','default','','7','N','Y','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro PO Aging Report',201,'PO_ITEM_CATEGORY','Po Item Category','Po Item Category','','','default','','10','N','Y','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro PO Aging Report',201,'PO_LINE_NUM','Po Line Num','Po Line Num','','~~~','default','','6','N','Y','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro PO Aging Report',201,'PO_NUMBER','Po Number','Po Number','','','default','','2','N','Y','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro PO Aging Report',201,'PO_SUPPLIER_NAME','Po Supplier Name','Po Supplier Name','','','default','','5','N','Y','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro PO Aging Report',201,'REQ_DESCRIPTION','Req Description','Req Description','','','default','','4','N','Y','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro PO Aging Report',201,'REQ_LINE_NUM','Req Line Num','Req Line Num','','~~~','default','','8','N','Y','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro PO Aging Report',201,'REQ_LINE_QTY','Req Line Qty','Req Line Qty','','~T~D~2','default','','9','N','Y','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro PO Aging Report',201,'REQ_NUMBER','Req Number','Req Number','','','default','','3','N','Y','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro PO Aging Report',201,'PO_CREATION_DATE','Po Creation Date','Po Creation Date','','','','','13','N','Y','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro PO Aging Report',201,'PO_ITEM_DESCRIPTION','Po Item Description','Po Item Description','','','','','16','N','Y','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro PO Aging Report',201,'PO_SHIPMENT_NUM','Po Shipment Num','Po Shipment Num','','~~~','','','12','N','Y','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro PO Aging Report',201,'PO_STATUS','Po Status','Po Status','','','','','14','N','Y','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro PO Aging Report',201,'PO_TYPE','Po Type','Po Type','','','','','15','N','Y','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro PO Aging Report',201,'PO_UNIT_PRICE','Po Unit Price','Po Unit Price','','~T~D~2','','','18','N','Y','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro PO Aging Report',201,'PO_UOM','Po Uom','Po Uom','','','','','17','N','Y','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro PO Aging Report',201,'REQ_LINE_ITEM_DESCRIPTION','Req Line Item Description','Req Line Item Description','','','','','11','N','Y','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro PO Aging Report',201,'PO_ACCRUAL_ACCOUNT','Po Accrual Account','Po Accrual Account','','','','','25','N','Y','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro PO Aging Report',201,'PO_AMOUNT_BILLED','Po Amount Billed','Po Amount Billed','','~T~D~2','','','23','N','Y','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro PO Aging Report',201,'PO_AMOUNT_CANCELLED','Po Amount Cancelled','Po Amount Cancelled','','~T~D~2','','','20','N','Y','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro PO Aging Report',201,'PO_AMOUNT_DUE','Po Amount Due','Po Amount Due','','~T~D~2','','','22','N','Y','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro PO Aging Report',201,'PO_AMOUNT_ORDERED','Po Amount Ordered','Po Amount Ordered','','~T~D~2','','','19','N','Y','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro PO Aging Report',201,'PO_AMOUNT_RECEIVED','Po Amount Received','Po Amount Received','','~T~D~2','','','21','N','Y','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro PO Aging Report',201,'PO_CHARGE_ACCOUNT','Po Charge Account','Po Charge Account','','','','','24','N','Y','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro PO Aging Report',201,'PO_VARIANCE_ACCOUNT','Po Variance Account','Po Variance Account','','','','','26','N','Y','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro PO Aging Report',201,'REQ_CREATED_BY_NAME','Req Created By Name','Req Created By Name','','','default','','42','','Y','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro PO Aging Report',201,'RECEIPT_DATE','Shipment Receipt Date','Receipt Date','','','default','','43','','Y','','','','','','','ID020048','N','N','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','','','US','');
--Inserting Report Parameters - HDS GSC iPro PO Aging Report
xxeis.eis_rsc_ins.rp( 'HDS GSC iPro PO Aging Report',201,'OPerating unit','OPerating unit','OPERATING_UNIT','IN','EIS_MULTI_OPERATING_UNIT_LOV','''HD Supply Corp USD - Org''','VARCHAR2','N','Y','1','Y','Y','CONSTANT','ID020048','Y','N','','','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS GSC iPro PO Aging Report',201,'PO Date From','From Date (Purchase Order Date)','','IN','','','DATE','N','Y','2','Y','N','CONSTANT','ID020048','Y','N','','Start Date','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS GSC iPro PO Aging Report',201,'PO Date To','To Date (Purchase Order Date)','','IN','','','DATE','N','Y','3','Y','N','CONSTANT','ID020048','Y','N','','End Date','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS GSC iPro PO Aging Report',201,'Supplier From','Supplier From','PO_SUPPLIER_NAME','>=','EIS_PO_SUPPLIER_LOV','','VARCHAR2','N','Y','4','Y','Y','CONSTANT','ID020048','Y','N','','','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS GSC iPro PO Aging Report',201,'Supplier To','Supplier To','PO_SUPPLIER_NAME','<=','EIS_PO_SUPPLIER_LOV','','VARCHAR2','N','Y','5','Y','Y','CONSTANT','ID020048','Y','N','','','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS GSC iPro PO Aging Report',201,'Buyer','Buyer','PO_BUYER_NAME','IN','EIS_PO_BUYER_LOV','','VARCHAR2','N','Y','6','Y','Y','CONSTANT','ID020048','Y','N','','','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS GSC iPro PO Aging Report',201,'PO Req From','PO Requisition From','REQ_NUMBER','>=','EIS_PO_REQ_LOV','','VARCHAR2','N','Y','7','Y','Y','CONSTANT','ID020048','Y','N','','','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS GSC iPro PO Aging Report',201,'PO Req To','PO Requisition To','REQ_NUMBER','<=','EIS_PO_REQ_LOV','','VARCHAR2','N','Y','8','Y','Y','CONSTANT','ID020048','Y','N','','','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS GSC iPro PO Aging Report',201,'PO From','Purchase Order From','PO_NUMBER','>=','EIS_PO_PURCHASE_ORDER_NUM_LOV','','VARCHAR2','N','Y','9','Y','Y','CONSTANT','ID020048','Y','N','','','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS GSC iPro PO Aging Report',201,'PO To','Purchase Order To','PO_NUMBER','<=','EIS_PO_PURCHASE_ORDER_NUM_LOV','','VARCHAR2','N','Y','10','Y','Y','CONSTANT','ID020048','Y','N','','','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS GSC iPro PO Aging Report',201,'Dynamic Precision Option','','','IN','EIS_PO_NUMBER_PRECISION_LOV','SELECT nvl(fnd_profile.VALUE(''REPORT_QUANTITY_PRECISION''),2) FROM dual','NUMBER','N','Y','11','Y','N','SQL','ID020048','Y','N','','','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS GSC iPro PO Aging Report',201,'PaTT Project Code','Accounting Flexfield : Segment ''Project Code'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#PROJECT_CODE','IN','','','VARCHAR2','N','Y','12','Y','Y','CONSTANT','ID020048','Y','N','','','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','','US','');
--Inserting Dependent Parameters - HDS GSC iPro PO Aging Report
--Inserting Report Conditions - HDS GSC iPro PO Aging Report
xxeis.eis_rsc_ins.rcnh( 'HDS GSC iPro PO Aging Report',201,'TRUNC(XGPDWRV.PO_CREATION_DATE) >= :PO Date From ','ADVANCED','','  1#$# ','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','PO Date From','','','','','','','','','','','GREATER_THAN_EQUALS','Y','Y','TRUNC(XGPDWRV.PO_CREATION_DATE)','','','','1',201,'HDS GSC iPro PO Aging Report','TRUNC(XGPDWRV.PO_CREATION_DATE) >= :PO Date From ');
xxeis.eis_rsc_ins.rcnh( 'HDS GSC iPro PO Aging Report',201,'TRUNC(XGPDWRV.PO_CREATION_DATE) <= :PO Date To ','ADVANCED','','  1#$# ','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','PO Date To','','','','','','','','','','','LESS_THAN_EQUALS','Y','Y','TRUNC(XGPDWRV.PO_CREATION_DATE)','','','','1',201,'HDS GSC iPro PO Aging Report','TRUNC(XGPDWRV.PO_CREATION_DATE) <= :PO Date To ');
xxeis.eis_rsc_ins.rcnh( 'HDS GSC iPro PO Aging Report',201,'XGPDWRV.OPERATING_UNIT IN OPerating unit','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','OPERATING_UNIT','','OPerating unit','','','','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','','','','','IN','Y','Y','','','','','1',201,'HDS GSC iPro PO Aging Report','XGPDWRV.OPERATING_UNIT IN OPerating unit');
xxeis.eis_rsc_ins.rcnh( 'HDS GSC iPro PO Aging Report',201,'XGPDWRV.PO_SUPPLIER_NAME >= Supplier From','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','PO_SUPPLIER_NAME','','Supplier From','','','','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',201,'HDS GSC iPro PO Aging Report','XGPDWRV.PO_SUPPLIER_NAME >= Supplier From');
xxeis.eis_rsc_ins.rcnh( 'HDS GSC iPro PO Aging Report',201,'XGPDWRV.PO_SUPPLIER_NAME <= Supplier To','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','PO_SUPPLIER_NAME','','Supplier To','','','','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',201,'HDS GSC iPro PO Aging Report','XGPDWRV.PO_SUPPLIER_NAME <= Supplier To');
xxeis.eis_rsc_ins.rcnh( 'HDS GSC iPro PO Aging Report',201,'XGPDWRV.PO_BUYER_NAME IN Buyer','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','PO_BUYER_NAME','','Buyer','','','','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','','','','','IN','Y','Y','','','','','1',201,'HDS GSC iPro PO Aging Report','XGPDWRV.PO_BUYER_NAME IN Buyer');
xxeis.eis_rsc_ins.rcnh( 'HDS GSC iPro PO Aging Report',201,'XGPDWRV.REQ_NUMBER >= PO Req From','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','REQ_NUMBER','','PO Req From','','','','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',201,'HDS GSC iPro PO Aging Report','XGPDWRV.REQ_NUMBER >= PO Req From');
xxeis.eis_rsc_ins.rcnh( 'HDS GSC iPro PO Aging Report',201,'XGPDWRV.REQ_NUMBER <= PO Req To','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','REQ_NUMBER','','PO Req To','','','','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',201,'HDS GSC iPro PO Aging Report','XGPDWRV.REQ_NUMBER <= PO Req To');
xxeis.eis_rsc_ins.rcnh( 'HDS GSC iPro PO Aging Report',201,'XGPDWRV.PO_NUMBER >= PO From','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','PO_NUMBER','','PO From','','','','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',201,'HDS GSC iPro PO Aging Report','XGPDWRV.PO_NUMBER >= PO From');
xxeis.eis_rsc_ins.rcnh( 'HDS GSC iPro PO Aging Report',201,'XGPDWRV.PO_NUMBER <= PO To','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','PO_NUMBER','','PO To','','','','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',201,'HDS GSC iPro PO Aging Report','XGPDWRV.PO_NUMBER <= PO To');
xxeis.eis_rsc_ins.rcnh( 'HDS GSC iPro PO Aging Report',201,'XGPDWRV.GCC#50328#PROJECT_CODE IN PaTT Project Code','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','GCC#50328#PROJECT_CODE','','PaTT Project Code','','','','','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','','','','','','IN','Y','Y','','','','','1',201,'HDS GSC iPro PO Aging Report','XGPDWRV.GCC#50328#PROJECT_CODE IN PaTT Project Code');
--Inserting Report Sorts - HDS GSC iPro PO Aging Report
--Inserting Report Triggers - HDS GSC iPro PO Aging Report
--inserting report templates - HDS GSC iPro PO Aging Report
--Inserting Report Portals - HDS GSC iPro PO Aging Report
--inserting report dashboards - HDS GSC iPro PO Aging Report
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'HDS GSC iPro PO Aging Report','201','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','XXCUS_GSC_PO_DETAIL_WITH_REQ_V','N','');
--inserting report security - HDS GSC iPro PO Aging Report
xxeis.eis_rsc_ins.rsec( 'HDS GSC iPro PO Aging Report','178','','SELF_SERVICE_PURCHASING_5',201,'ID020048','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GSC iPro PO Aging Report','178','','XXCUS_IPRO_NO_RECEIPTS',201,'ID020048','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GSC iPro PO Aging Report','178','','SELF_SERVICES_CATALOG_GSC',201,'ID020048','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GSC iPro PO Aging Report','178','','SLF_SRVC_PRCHSNG_5',201,'ID020048','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GSC iPro PO Aging Report','101','','XXCUS_GL_INQUIRY',201,'ID020048','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GSC iPro PO Aging Report','101','','XXCUS_GL_ACCOUNTANT_USD',201,'ID020048','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GSC iPro PO Aging Report','','JS016768','',201,'ID020048','','','');
--Inserting Report Pivots - HDS GSC iPro PO Aging Report
--Inserting Report   Version details- HDS GSC iPro PO Aging Report
xxeis.eis_rsc_ins.rv( 'HDS GSC iPro PO Aging Report','','HDS GSC iPro PO Aging Report','SA059956');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 201');
END IF;
END;
/
