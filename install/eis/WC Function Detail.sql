--Report Name            : WC Function Detail
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data XXWC_FUNC_DETAIL_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(85000);
IF mod_exist = 'Y' THEN 
--Inserting Object XXWC_FUNC_DETAIL_V
xxeis.eis_rsc_ins.v( 'XXWC_FUNC_DETAIL_V',85000,'','','','','10011289','XXEIS','Xxwc Func Detail V','XFDV','','','VIEW','US','','');
--Delete Object Columns for XXWC_FUNC_DETAIL_V
xxeis.eis_rsc_utility.delete_view_rows('XXWC_FUNC_DETAIL_V',85000,FALSE);
--Inserting Object Columns for XXWC_FUNC_DETAIL_V
xxeis.eis_rsc_ins.vc( 'XXWC_FUNC_DETAIL_V','CODE_PATH',85000,'Code Path','CODE_PATH','','','','10011289','VARCHAR2','','','Code Path','','','','US');
xxeis.eis_rsc_ins.vc( 'XXWC_FUNC_DETAIL_V','PATH',85000,'Path','PATH','','','','10011289','VARCHAR2','','','Path','','','','US');
xxeis.eis_rsc_ins.vc( 'XXWC_FUNC_DETAIL_V','LEVEL_NUM',85000,'Level Num','LEVEL_NUM','','','','10011289','NUMBER','','','Level Num','','','','US');
xxeis.eis_rsc_ins.vc( 'XXWC_FUNC_DETAIL_V','PROMPT',85000,'Prompt','PROMPT','','','','10011289','VARCHAR2','','','Prompt','','','','US');
xxeis.eis_rsc_ins.vc( 'XXWC_FUNC_DETAIL_V','MENU_NAME',85000,'Menu Name','MENU_NAME','','','','10011289','VARCHAR2','','','Menu Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXWC_FUNC_DETAIL_V','USER_MENU_NAME',85000,'User Menu Name','USER_MENU_NAME','','','','10011289','VARCHAR2','','','User Menu Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXWC_FUNC_DETAIL_V','FUNCTION_ID',85000,'Function Id','FUNCTION_ID','','','','10011289','NUMBER','','','Function Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXWC_FUNC_DETAIL_V','FUNCTION_NAME',85000,'Function Name','FUNCTION_NAME','','','','10011289','VARCHAR2','','','Function Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXWC_FUNC_DETAIL_V','USER_FUNCTION_NAME',85000,'User Function Name','USER_FUNCTION_NAME','','','','10011289','VARCHAR2','','','User Function Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXWC_FUNC_DETAIL_V','RESPONSIBILITY_KEY',85000,'Responsibility Key','RESPONSIBILITY_KEY','','','','10011289','VARCHAR2','','','Responsibility Key','','','','US');
xxeis.eis_rsc_ins.vc( 'XXWC_FUNC_DETAIL_V','RESPONSIBILITY_NAME',85000,'Responsibility Name','RESPONSIBILITY_NAME','','','','10011289','VARCHAR2','','','Responsibility Name','','','','US');
--Inserting Object Components for XXWC_FUNC_DETAIL_V
--Inserting Object Component Joins for XXWC_FUNC_DETAIL_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 85000');
END IF;
END;
/
prompt Creating Report LOV Data for WC Function Detail
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(85000);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - WC Function Detail
xxeis.eis_rsc_ins.lov( '','SELECT responsibility_name FROM fnd_responsibility_tl','','RESP_LOV','','ANONYMOUS',NULL,'','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 85000');
END IF;
END;
/
prompt Creating Report Data for WC Function Detail
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(85000);
IF mod_exist = 'Y' THEN 
--Deleting Report data - WC Function Detail
xxeis.eis_rsc_utility.delete_report_rows( 'WC Function Detail' );
--Inserting Report - WC Function Detail
xxeis.eis_rsc_ins.r( 85000,'WC Function Detail','','Lists ALL functions for WC Responsibilities. This report follows the complete menu structure and respects menu/function exclusions.','','','','10011985','XXWC_FUNC_DETAIL_V','Y','','','10011985','','N','WC Audit Reports','','CSV,EXCEL,','','','','','','','','','US','','','','');
--Inserting Report Columns - WC Function Detail
xxeis.eis_rsc_ins.rc( 'WC Function Detail',85000,'CODE_PATH','Code Path','Code Path','','','','','11','N','','','','','','','','10011985','N','N','','XXWC_FUNC_DETAIL_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Function Detail',85000,'FUNCTION_ID','Function Id','Function Id','','','','','5','N','','','','','','','','10011985','N','N','','XXWC_FUNC_DETAIL_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC Function Detail',85000,'FUNCTION_NAME','Function Name','Function Name','','','','','4','N','','','','','','','','10011985','N','N','','XXWC_FUNC_DETAIL_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Function Detail',85000,'LEVEL_NUM','Level Num','Level Num','','','','','9','N','','','','','','','','10011985','N','N','','XXWC_FUNC_DETAIL_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC Function Detail',85000,'MENU_NAME','Menu Name','Menu Name','','','','','7','N','','','','','','','','10011985','N','N','','XXWC_FUNC_DETAIL_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Function Detail',85000,'PATH','Path','Path','','','','','10','N','','','','','','','','10011985','N','N','','XXWC_FUNC_DETAIL_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Function Detail',85000,'PROMPT','Prompt','Prompt','','','','','6','N','','','','','','','','10011985','N','N','','XXWC_FUNC_DETAIL_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Function Detail',85000,'RESPONSIBILITY_KEY','Responsibility Key','Responsibility Key','','','','','2','N','','','','','','','','10011985','N','N','','XXWC_FUNC_DETAIL_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Function Detail',85000,'RESPONSIBILITY_NAME','Responsibility Name','Responsibility Name','','','','','1','N','','','','','','','','10011985','N','N','','XXWC_FUNC_DETAIL_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Function Detail',85000,'USER_FUNCTION_NAME','User Function Name','User Function Name','','','','','3','N','','','','','','','','10011985','N','N','','XXWC_FUNC_DETAIL_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Function Detail',85000,'USER_MENU_NAME','User Menu Name','User Menu Name','','','','','8','N','','','','','','','','10011985','N','N','','XXWC_FUNC_DETAIL_V','','','GROUP_BY','US','');
--Inserting Report Parameters - WC Function Detail
xxeis.eis_rsc_ins.rp( 'WC Function Detail',85000,'Responsibility','Choose a WC Responsibility','RESPONSIBILITY_NAME','IN','RESP_LOV','','VARCHAR2','Y','Y','1','Y','Y','CONSTANT','10011985','Y','','','','','XXWC_FUNC_DETAIL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC Function Detail',85000,'Function Name','The "user function name"','USER_FUNCTION_NAME','LIKE','','','VARCHAR2','N','Y','2','','Y','CONSTANT','10011985','Y','','','','','XXWC_FUNC_DETAIL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC Function Detail',85000,'Function Code','The function code','FUNCTION_NAME','LIKE','','','VARCHAR2','N','Y','3','','Y','CONSTANT','10011985','Y','','','','','XXWC_FUNC_DETAIL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC Function Detail',85000,'Menu Name','The menu name in the menu editing screen','USER_MENU_NAME','LIKE','','','VARCHAR2','N','Y','4','','Y','CONSTANT','10011985','Y','','','','','XXWC_FUNC_DETAIL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC Function Detail',85000,'Menu Code','The Menu Code','MENU_NAME','LIKE','','','VARCHAR2','N','Y','5','','Y','CONSTANT','10011985','Y','','','','','XXWC_FUNC_DETAIL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC Function Detail',85000,'Menu Prompt','This is the prompt displayed to the user in the menu tree.','PROMPT','LIKE','','','VARCHAR2','N','Y','6','','Y','CONSTANT','10011985','Y','','','','','XXWC_FUNC_DETAIL_V','','','US','');
--Inserting Dependent Parameters - WC Function Detail
--Inserting Report Conditions - WC Function Detail
xxeis.eis_rsc_ins.rcnh( 'WC Function Detail',85000,'FUNCTION_NAME LIKE :Function Code ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','FUNCTION_NAME','','Function Code','','','','','XXWC_FUNC_DETAIL_V','','','','','','LIKE','Y','Y','','','','','1',85000,'WC Function Detail','FUNCTION_NAME LIKE :Function Code ');
xxeis.eis_rsc_ins.rcnh( 'WC Function Detail',85000,'MENU_NAME LIKE :Menu Code ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','MENU_NAME','','Menu Code','','','','','XXWC_FUNC_DETAIL_V','','','','','','LIKE','Y','Y','','','','','1',85000,'WC Function Detail','MENU_NAME LIKE :Menu Code ');
xxeis.eis_rsc_ins.rcnh( 'WC Function Detail',85000,'PROMPT LIKE :Menu Prompt ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','PROMPT','','Menu Prompt','','','','','XXWC_FUNC_DETAIL_V','','','','','','LIKE','Y','Y','','','','','1',85000,'WC Function Detail','PROMPT LIKE :Menu Prompt ');
xxeis.eis_rsc_ins.rcnh( 'WC Function Detail',85000,'RESPONSIBILITY_NAME IN :Responsibility ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','RESPONSIBILITY_NAME','','Responsibility','','','','','XXWC_FUNC_DETAIL_V','','','','','','IN','Y','Y','','','','','1',85000,'WC Function Detail','RESPONSIBILITY_NAME IN :Responsibility ');
xxeis.eis_rsc_ins.rcnh( 'WC Function Detail',85000,'USER_FUNCTION_NAME LIKE :Function Name ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','USER_FUNCTION_NAME','','Function Name','','','','','XXWC_FUNC_DETAIL_V','','','','','','LIKE','Y','Y','','','','','1',85000,'WC Function Detail','USER_FUNCTION_NAME LIKE :Function Name ');
xxeis.eis_rsc_ins.rcnh( 'WC Function Detail',85000,'USER_MENU_NAME LIKE :Menu Name ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','USER_MENU_NAME','','Menu Name','','','','','XXWC_FUNC_DETAIL_V','','','','','','LIKE','Y','Y','','','','','1',85000,'WC Function Detail','USER_MENU_NAME LIKE :Menu Name ');
--Inserting Report Sorts - WC Function Detail
--Inserting Report Triggers - WC Function Detail
--inserting report templates - WC Function Detail
--Inserting Report Portals - WC Function Detail
--inserting report dashboards - WC Function Detail
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'WC Function Detail','85000','XXWC_FUNC_DETAIL_V','XXWC_FUNC_DETAIL_V','N','');
--inserting report security - WC Function Detail
xxeis.eis_rsc_ins.rsec( 'WC Function Detail','20005','','XXWC_VIEW_ALL_EIS_REPORTS',85000,'10011985','','','');
xxeis.eis_rsc_ins.rsec( 'WC Function Detail','20005','','XXWC_IT_SECURITY_ADMINISTRATOR',85000,'10011985','','','');
xxeis.eis_rsc_ins.rsec( 'WC Function Detail','20005','','XXWC_IT_OPERATIONS_ANALYST',85000,'10011985','','','');
xxeis.eis_rsc_ins.rsec( 'WC Function Detail','20005','','XXWC_IT_FUNC_CONFIGURATOR',85000,'10011985','','','');
xxeis.eis_rsc_ins.rsec( 'WC Function Detail','1','','SYSTEM_ADMINISTRATOR',85000,'10011985','','','');
--Inserting Report Pivots - WC Function Detail
--Inserting Report   Version details- WC Function Detail
xxeis.eis_rsc_ins.rv( 'WC Function Detail','','WC Function Detail','AB063501');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 85000');
END IF;
END;
/
