--Report Name            : WC Buyer Positions List
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View XXEIS_296498_TDHZMQ_V
--Connecting to schema APPS
accept SID prompt "Please enter database ( connect string )  : "
accept apps_password prompt "Please enter APPS password : " hide
connect APPS/@
set scan off define off
prompt Creating View APPS.XXEIS_296498_TDHZMQ_V
Create or replace View APPS.XXEIS_296498_TDHZMQ_V
(JOB,POSITION,START_DATE,END_DATE) AS 
select
pos.job_name Job
,pos.name Position
,pos.date_effective Start_Date
,pos.date_End End_date
from
apps.per_positions_v pos
where
current_date between date_effective and nvl(date_end, CURRENT_DATE + 1)
order by
pos.JOB_NAME
,pos.name

/
set scan on define on
prompt Creating View Data for WC Buyer Positions List
set scan off define off
DECLARE
BEGIN 
--Inserting View XXEIS_296498_TDHZMQ_V
xxeis.eis_rs_ins.v( 'XXEIS_296498_TDHZMQ_V',85000,'Paste SQL View for WC Buyer Positions List','1.0','','','XXEIS_RS_ADMIN','APPS','WC Buyer Positions List View','X2TV1','','');
--Delete View Columns for XXEIS_296498_TDHZMQ_V
xxeis.eis_rs_utility.delete_view_rows('XXEIS_296498_TDHZMQ_V',85000,FALSE);
--Inserting View Columns for XXEIS_296498_TDHZMQ_V
xxeis.eis_rs_ins.vc( 'XXEIS_296498_TDHZMQ_V','JOB',85000,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Job','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_296498_TDHZMQ_V','POSITION',85000,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Position','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_296498_TDHZMQ_V','START_DATE',85000,'','','','','','XXEIS_RS_ADMIN','DATE','','','Start Date','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_296498_TDHZMQ_V','END_DATE',85000,'','','','','','XXEIS_RS_ADMIN','DATE','','','End Date','','','');
--Inserting View Components for XXEIS_296498_TDHZMQ_V
--Inserting View Component Joins for XXEIS_296498_TDHZMQ_V
END;
/
set scan on define on
prompt Creating Report Data for WC Buyer Positions List
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - WC Buyer Positions List
xxeis.eis_rs_utility.delete_report_rows( 'WC Buyer Positions List' );
--Inserting Report - WC Buyer Positions List
xxeis.eis_rs_ins.r( 85000,'WC Buyer Positions List','','This will assist with quickly getting a list of all Buyer Positions.

','','','','XXEIS_RS_ADMIN','XXEIS_296498_TDHZMQ_V','Y','','select
pos.job_name Job
,pos.name Position
,pos.date_effective Start_Date
,pos.date_End End_date
from
apps.per_positions_v pos
where
current_date between date_effective and nvl(date_end, CURRENT_DATE + 1)
order by
pos.JOB_NAME
,pos.name
','XXEIS_RS_ADMIN','','N','WC Audit Reports','','CSV,EXCEL,','N');
--Inserting Report Columns - WC Buyer Positions List
xxeis.eis_rs_ins.rc( 'WC Buyer Positions List',85000,'JOB','Job','','','','default','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_296498_TDHZMQ_V','','');
xxeis.eis_rs_ins.rc( 'WC Buyer Positions List',85000,'POSITION','Position','','','','default','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_296498_TDHZMQ_V','','');
xxeis.eis_rs_ins.rc( 'WC Buyer Positions List',85000,'START_DATE','Start Date','','','','default','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_296498_TDHZMQ_V','','');
xxeis.eis_rs_ins.rc( 'WC Buyer Positions List',85000,'END_DATE','End Date','','','','default','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_296498_TDHZMQ_V','','');
--Inserting Report Parameters - WC Buyer Positions List
--Inserting Report Conditions - WC Buyer Positions List
--Inserting Report Sorts - WC Buyer Positions List
--Inserting Report Triggers - WC Buyer Positions List
--Inserting Report Templates - WC Buyer Positions List
--Inserting Report Portals - WC Buyer Positions List
--Inserting Report Dashboards - WC Buyer Positions List
--Inserting Report Security - WC Buyer Positions List
xxeis.eis_rs_ins.rsec( 'WC Buyer Positions List','20005','','50843',85000,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'WC Buyer Positions List','20005','','51207',85000,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'WC Buyer Positions List','20005','','50861',85000,'XXEIS_RS_ADMIN','','');
--Inserting Report Pivots - WC Buyer Positions List
END;
/
set scan on define on
