--Report Name            : AR Aging with Source Code
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View EIS_XXWC_AR_AGING_SRCE_COD_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_AR_AGING_SRCE_COD_V
Create or replace View XXEIS.EIS_XXWC_AR_AGING_SRCE_COD_V
 AS 
SELECT xacb.customer_name bill_to_customer_name
           ,xacb.customer_account_number bill_to_customer_number
           ,SUM (xacb.transaction_remaining_balance) outstanding_amount
           ,SUM (xacb.current_balance) bucket_current
           ,SUM (xacb.thirty_days_bal) bucket_1_to_30
           ,SUM (xacb.sixty_days_bal) bucket_31_to_60
           ,SUM (xacb.ninety_days_bal) bucket_61_to_90
           ,SUM (xacb.one_eighty_days_bal) bucket_91_to_180
           ,SUM (xacb.three_sixty_days_bal) bucket_181_to_360
           ,SUM (xacb.over_three_sixty_days_bal) bucket_361_days_and_above
           ,xacb.customer_profile_class profile_class
           ,xacb.account_credit_hold credit_hold
           ,xacb.customer_account_status
           ,xacb.collector_name collector
           ,xacb.credit_analyst credit_analyst_name
           ,xacb.salesrep_number salesrep_number
           ,xacb.account_manager account_manager
           ,hca.attribute4 source_code                
       FROM  
           hz_cust_accounts hca,                    
                                xxwc_ar_customer_balance_mv xacb 
      WHERE hca.cust_account_id(+) = xacb.cust_account_id 
   GROUP BY customer_name
           ,customer_account_number
           ,customer_profile_class
           ,account_credit_hold
           ,customer_account_status
           ,collector_name
           ,credit_analyst
           ,salesrep_number
           ,account_manager
           ,attribute4/
set scan on define on
prompt Creating View Data for AR Aging with Source Code
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_AR_AGING_SRCE_COD_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_AR_AGING_SRCE_COD_V',222,'','','','','MR020532','XXEIS','Eix Xxwc Ar Summary V','EXASV1','','');
--Delete View Columns for EIS_XXWC_AR_AGING_SRCE_COD_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_AR_AGING_SRCE_COD_V',222,FALSE);
--Inserting View Columns for EIS_XXWC_AR_AGING_SRCE_COD_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_SRCE_COD_V','BUCKET_361_DAYS_AND_ABOVE',222,'Bucket 361 Days And Above','BUCKET_361_DAYS_AND_ABOVE','','','','MR020532','NUMBER','','','Bucket 361 Days And Above','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_SRCE_COD_V','BUCKET_181_TO_360',222,'Bucket 181 To 360','BUCKET_181_TO_360','','','','MR020532','NUMBER','','','Bucket 181 To 360','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_SRCE_COD_V','BUCKET_91_TO_180',222,'Bucket 91 To 180','BUCKET_91_TO_180','','','','MR020532','NUMBER','','','Bucket 91 To 180','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_SRCE_COD_V','BUCKET_61_TO_90',222,'Bucket 61 To 90','BUCKET_61_TO_90','','','','MR020532','NUMBER','','','Bucket 61 To 90','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_SRCE_COD_V','BUCKET_31_TO_60',222,'Bucket 31 To 60','BUCKET_31_TO_60','','','','MR020532','NUMBER','','','Bucket 31 To 60','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_SRCE_COD_V','BUCKET_1_TO_30',222,'Bucket 1 To 30','BUCKET_1_TO_30','','','','MR020532','NUMBER','','','Bucket 1 To 30','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_SRCE_COD_V','BUCKET_CURRENT',222,'Bucket Current','BUCKET_CURRENT','','','','MR020532','NUMBER','','','Bucket Current','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_SRCE_COD_V','OUTSTANDING_AMOUNT',222,'Outstanding Amount','OUTSTANDING_AMOUNT','','','','MR020532','NUMBER','','','Outstanding Amount','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_SRCE_COD_V','BILL_TO_CUSTOMER_NUMBER',222,'Bill To Customer Number','BILL_TO_CUSTOMER_NUMBER','','','','MR020532','VARCHAR2','','','Bill To Customer Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_SRCE_COD_V','BILL_TO_CUSTOMER_NAME',222,'Bill To Customer Name','BILL_TO_CUSTOMER_NAME','','','','MR020532','VARCHAR2','','','Bill To Customer Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_SRCE_COD_V','CREDIT_HOLD',222,'Credit Hold','CREDIT_HOLD','','','','MR020532','VARCHAR2','','','Credit Hold','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_SRCE_COD_V','CUSTOMER_ACCOUNT_STATUS',222,'Customer Account Status','CUSTOMER_ACCOUNT_STATUS','','','','MR020532','VARCHAR2','','','Customer Account Status','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_SRCE_COD_V','PROFILE_CLASS',222,'Profile Class','PROFILE_CLASS','','','','MR020532','VARCHAR2','','','Profile Class','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_SRCE_COD_V','COLLECTOR',222,'Collector','COLLECTOR','','','','MR020532','VARCHAR2','','','Collector','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_SRCE_COD_V','CREDIT_ANALYST_NAME',222,'Credit Analyst Name','CREDIT_ANALYST_NAME','','','','MR020532','VARCHAR2','','','Credit Analyst Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_SRCE_COD_V','SALESREP_NUMBER',222,'Salesrep Number','SALESREP_NUMBER','','','','MR020532','VARCHAR2','','','Salesrep Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_SRCE_COD_V','ACCOUNT_MANAGER',222,'Account Manager','ACCOUNT_MANAGER','','','','MR020532','VARCHAR2','','','Account Manager','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_SRCE_COD_V','SOURCE_CODE',222,'Source Code','SOURCE_CODE','','','','MR020532','VARCHAR2','','','Source Code','','','');
--Inserting View Components for EIS_XXWC_AR_AGING_SRCE_COD_V
--Inserting View Component Joins for EIS_XXWC_AR_AGING_SRCE_COD_V
END;
/
set scan on define on
prompt Creating Report LOV Data for AR Aging with Source Code
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - AR Aging with Source Code
xxeis.eis_rs_ins.lov( 222,'select distinct party_name from hz_parties p, hz_cust_accounts c
	where p.party_id = c.party_id','null','Customer Name','Displays List of Values for Customer Name ','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 222,'select name from ar_collectors','null','Collector','Displays list of values for Collector','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 222,'select distinct name from hz_cust_profile_classes','null','PROFILE CLASS','This LOV lists all the profile classes of the customers','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 222,'SELECT  resource_name resource_name
 FROM jtf_rs_role_relations a,
  jtf_rs_roles_vl b,
  jtf_rs_resource_extns_vl c
WHERE a.role_resource_type  = ''RS_INDIVIDUAL''
AND a.role_resource_id      = c.resource_id
AND a.role_id               = b.role_id
AND b.role_code             = ''CREDIT_ANALYST''
AND c.category              = ''EMPLOYEE''
AND NVL(a.delete_flag,''N'') <> ''Y''
Order BY resource_name','','Credit Analyst','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 222,'select distinct credit_hold from hz_customer_profiles','','Credit Holds','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 222,'select  customer_status.meaning Status
from  fnd_lookup_values_vl customer_status
where customer_status.lookup_type= ''ACCOUNT_STATUS''
 and customer_status.view_application_id=222','','XXWC Customer Account Status','','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 222,'select distinct account_manager
from XXEIS.EIS_XXWC_AR_AGING_BAD_EXP_V
order by account_manager','','XXWC AR Aging SalesRep Name','Sales Rep Name from AR Aging View','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 222,'select distinct salesrep_number
from XXEIS.EIS_XXWC_AR_AGING_BAD_EXP_V
order by salesrep_number','','XXWC AR Aging SalesRep Num','Sales Rep number from Aging View','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( '','select distinct attribute4 Source_Code from hz_cust_accounts where attribute4 not in ''-''','','XXWC AR Source Code','To display Source code','MR020532',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for AR Aging with Source Code
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - AR Aging with Source Code
xxeis.eis_rs_utility.delete_report_rows( 'AR Aging with Source Code' );
--Inserting Report - AR Aging with Source Code
xxeis.eis_rs_ins.r( 222,'AR Aging with Source Code','','','','','','MR020532','EIS_XXWC_AR_AGING_SRCE_COD_V','Y','','','MR020532','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - AR Aging with Source Code
xxeis.eis_rs_ins.rc( 'AR Aging with Source Code',222,'BILL_TO_CUSTOMER_NUMBER','Customer Number','Bill To Customer Number','','','default','','2','N','','','','','','','','MR020532','N','N','','EIS_XXWC_AR_AGING_SRCE_COD_V','','');
xxeis.eis_rs_ins.rc( 'AR Aging with Source Code',222,'BUCKET_181_TO_360','Bucket 181 To 360','Bucket 181 To 360','','~~~','default','','13','N','','','','','','','','MR020532','N','N','','EIS_XXWC_AR_AGING_SRCE_COD_V','','');
xxeis.eis_rs_ins.rc( 'AR Aging with Source Code',222,'BUCKET_1_TO_30','Bucket 1 To 30','Bucket 1 To 30','','~~~','default','','9','N','','','','','','','','MR020532','N','N','','EIS_XXWC_AR_AGING_SRCE_COD_V','','');
xxeis.eis_rs_ins.rc( 'AR Aging with Source Code',222,'BUCKET_31_TO_60','Bucket 31 To 60','Bucket 31 To 60','','~~~','default','','10','N','','','','','','','','MR020532','N','N','','EIS_XXWC_AR_AGING_SRCE_COD_V','','');
xxeis.eis_rs_ins.rc( 'AR Aging with Source Code',222,'BUCKET_361_DAYS_AND_ABOVE','Bucket 361 Days And Above','Bucket 361 Days And Above','','~~~','default','','14','N','','','','','','','','MR020532','N','N','','EIS_XXWC_AR_AGING_SRCE_COD_V','','');
xxeis.eis_rs_ins.rc( 'AR Aging with Source Code',222,'BUCKET_61_TO_90','Bucket 61 To 90','Bucket 61 To 90','','~~~','default','','11','N','','','','','','','','MR020532','N','N','','EIS_XXWC_AR_AGING_SRCE_COD_V','','');
xxeis.eis_rs_ins.rc( 'AR Aging with Source Code',222,'BUCKET_91_TO_180','Bucket 91 To 180','Bucket 91 To 180','','~~~','default','','12','N','','','','','','','','MR020532','N','N','','EIS_XXWC_AR_AGING_SRCE_COD_V','','');
xxeis.eis_rs_ins.rc( 'AR Aging with Source Code',222,'BUCKET_CURRENT','Bucket Current','Bucket Current','','~~~','default','','8','N','','','','','','','','MR020532','N','N','','EIS_XXWC_AR_AGING_SRCE_COD_V','','');
xxeis.eis_rs_ins.rc( 'AR Aging with Source Code',222,'OUTSTANDING_AMOUNT','Customer Account Balance','Outstanding Amount','','~~~','default','','7','N','','','','','','','','MR020532','N','N','','EIS_XXWC_AR_AGING_SRCE_COD_V','','');
xxeis.eis_rs_ins.rc( 'AR Aging with Source Code',222,'COLLECTOR','Collector','Collector','','','default','','3','N','','','','','','','','MR020532','N','N','','EIS_XXWC_AR_AGING_SRCE_COD_V','','');
xxeis.eis_rs_ins.rc( 'AR Aging with Source Code',222,'CREDIT_HOLD','Credit Hold','Credit Hold','','','default','','4','N','','','','','','','','MR020532','N','N','','EIS_XXWC_AR_AGING_SRCE_COD_V','','');
xxeis.eis_rs_ins.rc( 'AR Aging with Source Code',222,'CUSTOMER_ACCOUNT_STATUS','Customer Account Status','Customer Account Status','','','default','','5','N','','','','','','','','MR020532','N','N','','EIS_XXWC_AR_AGING_SRCE_COD_V','','');
xxeis.eis_rs_ins.rc( 'AR Aging with Source Code',222,'PROFILE_CLASS','Profile Class','Profile Class','','','default','','6','N','','','','','','','','MR020532','N','N','','EIS_XXWC_AR_AGING_SRCE_COD_V','','');
xxeis.eis_rs_ins.rc( 'AR Aging with Source Code',222,'ACCOUNT_MANAGER','Salesrep Name','Account Manager','','','default','','16','N','','','','','','','','MR020532','N','N','','EIS_XXWC_AR_AGING_SRCE_COD_V','','');
xxeis.eis_rs_ins.rc( 'AR Aging with Source Code',222,'SALESREP_NUMBER','Salesrep Number','Salesrep Number','','','default','','15','N','','','','','','','','MR020532','N','N','','EIS_XXWC_AR_AGING_SRCE_COD_V','','');
xxeis.eis_rs_ins.rc( 'AR Aging with Source Code',222,'BILL_TO_CUSTOMER_NAME','Customer Name','Bill To Customer Name','','','default','','1','N','','','','','','','','MR020532','N','N','','EIS_XXWC_AR_AGING_SRCE_COD_V','','');
xxeis.eis_rs_ins.rc( 'AR Aging with Source Code',222,'SOURCE_CODE','Source Code','Source Code','','','','','17','','Y','','','','','','','MR020532','N','N','','EIS_XXWC_AR_AGING_SRCE_COD_V','','');
--Inserting Report Parameters - AR Aging with Source Code
xxeis.eis_rs_ins.rp( 'AR Aging with Source Code',222,'Customer Account Status','Customer Account Status','CUSTOMER_ACCOUNT_STATUS','IN','XXWC Customer Account Status','','VARCHAR2','N','Y','2','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'AR Aging with Source Code',222,'Collector','Collector','COLLECTOR','IN','Collector','','VARCHAR2','N','Y','3','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'AR Aging with Source Code',222,'Customer Name','Customer Name','BILL_TO_CUSTOMER_NAME','IN','Customer Name','','VARCHAR2','N','Y','1','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'AR Aging with Source Code',222,'Credit Analyst','Credit Analyst','CREDIT_ANALYST_NAME','IN','Credit Analyst','','VARCHAR2','N','Y','4','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'AR Aging with Source Code',222,'Credit Hold','Credit Hold','CREDIT_HOLD','IN','Credit Holds','','VARCHAR2','N','Y','5','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'AR Aging with Source Code',222,'Profile Class','Profile Class','PROFILE_CLASS','IN','PROFILE CLASS','','VARCHAR2','N','Y','6','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'AR Aging with Source Code',222,'Sales Rep Name','Sales Rep Name','ACCOUNT_MANAGER','IN','XXWC AR Aging SalesRep Name','','VARCHAR2','N','Y','7','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'AR Aging with Source Code',222,'Sales Rep Number','Sales Rep Number','SALESREP_NUMBER','IN','XXWC AR Aging SalesRep Num','','VARCHAR2','N','Y','8','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'AR Aging with Source Code',222,'Source Code','Source Code','SOURCE_CODE','IN','XXWC AR Source Code','','VARCHAR2','N','Y','9','','Y','CONSTANT','MR020532','Y','N','','','');
--Inserting Report Conditions - AR Aging with Source Code
xxeis.eis_rs_ins.rcn( 'AR Aging with Source Code',222,'BILL_TO_CUSTOMER_NAME','IN',':Customer Name','','','Y','1','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'AR Aging with Source Code',222,'SOURCE_CODE','IN',':Source Code','','','Y','9','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'AR Aging with Source Code',222,'CUSTOMER_ACCOUNT_STATUS','IN',':Customer Account Status','','','Y','2','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'AR Aging with Source Code',222,'COLLECTOR','IN',':Collector','','','Y','3','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'AR Aging with Source Code',222,'CREDIT_ANALYST_NAME','IN',':Credit Analyst','','','Y','4','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'AR Aging with Source Code',222,'CREDIT_HOLD','IN',':Credit Hold','','','Y','5','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'AR Aging with Source Code',222,'PROFILE_CLASS','IN',':Profile Class','','','Y','6','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'AR Aging with Source Code',222,'ACCOUNT_MANAGER','IN',':Sales Rep','','','Y','7','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'AR Aging with Source Code',222,'ACCOUNT_MANAGER','IN',':Sales Rep Name','','','Y','7','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'AR Aging with Source Code',222,'SALESREP_NUMBER','IN',':Sales Rep Number','','','Y','8','Y','MR020532');
--Inserting Report Sorts - AR Aging with Source Code
--Inserting Report Triggers - AR Aging with Source Code
--Inserting Report Templates - AR Aging with Source Code
--Inserting Report Portals - AR Aging with Source Code
--Inserting Report Dashboards - AR Aging with Source Code
--Inserting Report Security - AR Aging with Source Code
xxeis.eis_rs_ins.rsec( 'AR Aging with Source Code','222','','50878',222,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'AR Aging with Source Code','222','','50877',222,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'AR Aging with Source Code','222','','50879',222,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'AR Aging with Source Code','222','','50853',222,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'AR Aging with Source Code','222','','50854',222,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'AR Aging with Source Code','222','','50993',222,'MR020532','','');
--Inserting Report Pivots - AR Aging with Source Code
xxeis.eis_rs_ins.rpivot( 'AR Aging with Source Code',222,'Pivot','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
--Inserting Report Summary Calculation Columns For Pivot- Pivot
END;
/
set scan on define on
