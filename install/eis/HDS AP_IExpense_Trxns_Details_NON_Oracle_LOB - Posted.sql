--Report Name            : HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data XXCUS_BULLET_IEXP_TBL
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(91000);
IF mod_exist = 'Y' THEN 
--Inserting Object XXCUS_BULLET_IEXP_TBL
xxeis.eis_rsc_ins.v( 'XXCUS_BULLET_IEXP_TBL',91000,'Paste SQL View for iExpense Bullet Train Table','','','','XXEIS_RS_ADMIN','XXEIS','XXCUS_BULLET_IEXP_TBL','XBIT','','','VIEW','US','','');
--Delete Object Columns for XXCUS_BULLET_IEXP_TBL
xxeis.eis_rsc_utility.delete_view_rows('XXCUS_BULLET_IEXP_TBL',91000,FALSE);
--Inserting Object Columns for XXCUS_BULLET_IEXP_TBL
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','PERIOD_NAME',91000,'','PERIOD_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Period Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','CC_FISCAL_PERIOD',91000,'','CC_FISCAL_PERIOD','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cc Fiscal Period','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','ORACLE_PRODUCT',91000,'','ORACLE_PRODUCT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Oracle Product','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','ORACLE_LOCATION',91000,'','ORACLE_LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Oracle Location','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','ORACLE_COST_CENTER',91000,'','ORACLE_COST_CENTER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Oracle Cost Center','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','ORACLE_ACCOUNT',91000,'','ORACLE_ACCOUNT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Oracle Account','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','ITEM_DESCRIPTION',91000,'','ITEM_DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Item Description','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','LINE_AMOUNT',91000,'','LINE_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Line Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','DISTRIBUTION_LINE_NUMBER',91000,'','DISTRIBUTION_LINE_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Distribution Line Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','FULL_NAME',91000,'','FULL_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Full Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','EMPLOYEE_NUMBER',91000,'','EMPLOYEE_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Employee Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','EMP_DEFAULT_PROD',91000,'','EMP_DEFAULT_PROD','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Emp Default Prod','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','EMP_DEFAULT_LOC',91000,'','EMP_DEFAULT_LOC','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Emp Default Loc','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','EMP_DEFAULT_COSTCTR',91000,'','EMP_DEFAULT_COSTCTR','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Emp Default Costctr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','CREATION_DATE',91000,'','CREATION_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Creation Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','REPORT_SUBMITTED_DATE',91000,'','REPORT_SUBMITTED_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Report Submitted Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','MERCHANT_NAME',91000,'','MERCHANT_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Merchant Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','BUSINESS_PURPOSE',91000,'','BUSINESS_PURPOSE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Business Purpose','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','JUSTIFICATION',91000,'','JUSTIFICATION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Justification','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','START_EXPENSE_DATE',91000,'','START_EXPENSE_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Start Expense Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','END_EXPENSE_DATE',91000,'','END_EXPENSE_DATE','','','','XXEIS_RS_ADMIN','DATE','','','End Expense Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','REMARKS',91000,'','REMARKS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Remarks','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','ATTENDEES',91000,'','ATTENDEES','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Attendees','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','CREDIT_CARD_TRX_ID',91000,'','CREDIT_CARD_TRX_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Credit Card Trx Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','TRANSACTION_DATE',91000,'','TRANSACTION_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Transaction Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','MERCHANT_LOCATION',91000,'','MERCHANT_LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Merchant Location','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','DESTINATION_FROM',91000,'','DESTINATION_FROM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Destination From','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','DESTINATION_TO',91000,'','DESTINATION_TO','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Destination To','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','DAILY_DISTANCE',91000,'','DAILY_DISTANCE','','','','XXEIS_RS_ADMIN','NUMBER','','','Daily Distance','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','TRIP_DISTANCE',91000,'','TRIP_DISTANCE','','','','XXEIS_RS_ADMIN','NUMBER','','','Trip Distance','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','DT_SENTTO_GL',91000,'','DT_SENTTO_GL','','','','XXEIS_RS_ADMIN','DATE','','','Dt Sentto Gl','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','EXPENSE_REPORT_NUMBER',91000,'','EXPENSE_REPORT_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Expense Report Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','CARD_PROGRAM_NAME',91000,'','CARD_PROGRAM_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Card Program Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','REPORT_TOTAL',91000,'','REPORT_TOTAL','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Report Total','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','TRANSACTION_AMOUNT',91000,'','TRANSACTION_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Transaction Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','APPROVER_ID',91000,'','APPROVER_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Approver Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','APPROVER_NAME',91000,'','APPROVER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Approver Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','QUERY_DESCR',91000,'','QUERY_DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Query Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','POSTED_DATE',91000,'','POSTED_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Posted Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','MCC_CODE_NO',91000,'','MCC_CODE_NO','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Mcc Code No','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','COST_CENTER_DESCR',91000,'','COST_CENTER_DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cost Center Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','FRU',91000,'','FRU','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Fru','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','CARDMEMBER_NAME',91000,'','CARDMEMBER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cardmember Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','MCC_CODE',91000,'','MCC_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Mcc Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','IMPORTED_TO_GL',91000,'','IMPORTED_TO_GL','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Imported To Gl','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','QUERY_NUM',91000,'','QUERY_NUM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Query Num','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','EXPENSE_REPORT_STATUS',91000,'','EXPENSE_REPORT_STATUS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Expense Report Status','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','REFERENCE_NUMBER',91000,'','REFERENCE_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Reference Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','PRODUCT_DESCR',91000,'','PRODUCT_DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Product Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','LOCATION_DESCR',91000,'','LOCATION_DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Location Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','ACCOUNT_DESCR',91000,'','ACCOUNT_DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Account Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','ORACLE_ACCOUNTS',91000,'','ORACLE_ACCOUNTS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Oracle Accounts','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','CATEGORY',91000,'','CATEGORY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Category','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','SUB_CATEGORY',91000,'','SUB_CATEGORY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Sub Category','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','INVOICE_NUMBER',91000,'','INVOICE_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Invoice Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','DESCRIPTION',91000,'','DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Description','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','AVG_MILEAGE_RATE',91000,'','AVG_MILEAGE_RATE','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Avg Mileage Rate','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','ACCOUNTING_DATE',91000,'','ACCOUNTING_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Accounting Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','AMT_DUE_CCARD_COMPANY',91000,'','AMT_DUE_CCARD_COMPANY','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Amt Due Ccard Company','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','CC_TRNS_CATEGORY',91000,'','CC_TRNS_CATEGORY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cc Trns Category','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','WORKFLOW_APPROVED_FLAG',91000,'','WORKFLOW_APPROVED_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Workflow Approved Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','EXPENSE_CURRENT_APPROVER_ID',91000,'','EXPENSE_CURRENT_APPROVER_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Expense Current Approver Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','REPORT_TYPE',91000,'','REPORT_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Report Type','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','ADVANCE_INVOICE_TO_APPLY',91000,'','ADVANCE_INVOICE_TO_APPLY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Advance Invoice To Apply','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','ADVANCE_DISTRIBUTION_NUMBER',91000,'','ADVANCE_DISTRIBUTION_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Advance Distribution Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','ADVANCE_FLAG',91000,'','ADVANCE_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Advance Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','ADVANCE_GL_DATE',91000,'','ADVANCE_GL_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Advance Gl Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','ADVANCE_NUMBER',91000,'','ADVANCE_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Advance Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','REPORT_REJECT_CODE',91000,'','REPORT_REJECT_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Report Reject Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','APP_POST_FLAG',91000,'','APP_POST_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','App Post Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','APPROVED_IN_GL',91000,'','APPROVED_IN_GL','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Approved In Gl','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','VOUCHNO',91000,'','VOUCHNO','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vouchno','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','PROJECT_NUMBER',91000,'','PROJECT_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Project Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','PROJECT_NAME',91000,'','PROJECT_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Project Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','CURRENCY_CODE',91000,'','CURRENCY_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Currency Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','VENDOR_NAME',91000,'','VENDOR_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','ATTENDEES_EMP',91000,'','ATTENDEES_EMP','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Attendees Emp','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','ATTENDEES_TE',91000,'','ATTENDEES_TE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Attendees Te','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','ATTENDEES_CTI1',91000,'','ATTENDEES_CTI1','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Attendees Cti1','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','ATTENDEES_CTI2',91000,'','ATTENDEES_CTI2','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Attendees Cti2','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','DISTANCE_UNIT_CODE',91000,'','DISTANCE_UNIT_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Distance Unit Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','CARD_PROGRAM_ID',91000,'','CARD_PROGRAM_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Card Program Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','MILES',91000,'','MILES','','','','XXEIS_RS_ADMIN','NUMBER','','','Miles','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','RATE_PER_MILE',91000,'','RATE_PER_MILE','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Rate Per Mile','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','ATTRIBUTE_CATEGORY',91000,'','ATTRIBUTE_CATEGORY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Attribute Category','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','CATEGORY_CODE',91000,'','CATEGORY_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Category Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','RECEIPT_CURRENCY_AMOUNT',91000,'','RECEIPT_CURRENCY_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Receipt Currency Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','AMT_DUE_EMPLOYEE',91000,'','AMT_DUE_EMPLOYEE','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Amt Due Employee','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','IMPORTED_TO_AP',91000,'','IMPORTED_TO_AP','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Imported To Ap','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','GST_AMOUNT',91000,'Gst Amount','GST_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Gst Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','HST_AMOUNT',91000,'Hst Amount','HST_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Hst Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','ORACLE_SEGMENT6',91000,'Oracle Segment6','ORACLE_SEGMENT6','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Oracle Segment6','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','ORACLE_SEGMENT7',91000,'Oracle Segment7','ORACLE_SEGMENT7','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Oracle Segment7','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','ORG_ID',91000,'Org Id','ORG_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Org Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','PST_AMOUNT',91000,'Pst Amount','PST_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Pst Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','QST_AMOUNT',91000,'Qst Amount','QST_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Qst Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','TAX_PROVINCE',91000,'Tax Province','TAX_PROVINCE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Tax Province','','','','US');
--Inserting Object Components for XXCUS_BULLET_IEXP_TBL
xxeis.eis_rsc_ins.vcomp( 'XXCUS_BULLET_IEXP_TBL','XXCUS_NATURAL_ACCT_TBL',91000,'','XNAT','','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','XXCUS_NATURAL_ACCT_TBL','','','','','XNAT','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'XXCUS_BULLET_IEXP_TBL','XXCUS_LOCATION_CODE_TBL',91000,'','XLC','','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','XXCUS_LOCATION_CODE_TBL','','','','','XLCT','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'XXCUS_BULLET_IEXP_TBL','AP_CREDIT_CARD_TRXNS_ALL',91000,'','ACCTA','','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','AP_CREDIT_CARD_TRXNS_ALL','','','','','ACCTA','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'XXCUS_BULLET_IEXP_TBL','XXHDS_APPR_ACT_TERM_DATE',91000,'','XAATD','','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','XXHDS_APPR_ACT_TERM_DATE','','','','','XAATD','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'XXCUS_BULLET_IEXP_TBL','XXHDS_MCC_CODE_ACCT_INFO',91000,'','XMCAI','','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','XXHDS_MCC_CODE_ACCT_INFO','','','','','XMCAI','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'XXCUS_BULLET_IEXP_TBL','XXHDS_CC_HOLDER_INFO',91000,'','XCHI','','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','XXHDS_CC_HOLDER_INFO','','','','','XCHI','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'XXCUS_BULLET_IEXP_TBL','XXEIS_LOB_NAMES_V',91000,'','XLNV','','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','XXEIS_LOB_NAMES_V','','','','','X1HV','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'XXCUS_BULLET_IEXP_TBL','XXCUSHR_PS_EMP_ALL_TBL',91000,'','XPEAT','','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','XXCUSHR_PS_EMP_ALL_TBL','','','','','','',' ',' ',' ',' ',' ');
--Inserting Object Component Joins for XXCUS_BULLET_IEXP_TBL
xxeis.eis_rsc_ins.vcj( 'XXCUS_BULLET_IEXP_TBL','XXEIS_LOB_NAMES_V','XLNV',91000,'XBIT.ORACLE_PRODUCT','=','XLNV.ORACLE_PRODUCT(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'XXCUS_BULLET_IEXP_TBL','AP_CREDIT_CARD_TRXNS_ALL','ACCTA',91000,'XBIT.CREDIT_CARD_TRX_ID','=','ACCTA.TRX_ID(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'XXCUS_BULLET_IEXP_TBL','XXCUS_LOCATION_CODE_TBL','XLC',91000,'XBIT.ORACLE_LOCATION','=','XLC.ENTRP_LOC(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'XXCUS_BULLET_IEXP_TBL','XXCUS_NATURAL_ACCT_TBL','XNAT',91000,'XBIT.ORACLE_ACCOUNT','=','XNAT.Z_OLD_VALUE(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'XXCUS_BULLET_IEXP_TBL','XXHDS_APPR_ACT_TERM_DATE','XAATD',91000,'XBIT.APPROVER_ID','=','XAATD.APPROVER_ID(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'XXCUS_BULLET_IEXP_TBL','XXHDS_CC_HOLDER_INFO','XCHI',91000,'XBIT.EMPLOYEE_NUMBER','=','XCHI.EMPLOYEE_NUMBER','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'XXCUS_BULLET_IEXP_TBL','XXHDS_MCC_CODE_ACCT_INFO','XMCAI',91000,'XBIT.MCC_CODE_NO','=','XMCAI.MCC_CODE_N0(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'XXCUS_BULLET_IEXP_TBL','XXCUSHR_PS_EMP_ALL_TBL','XPEAT',91000,'XBIT.EMPLOYEE_NUMBER','=','XPEAT.EMPLOYEE_NUMBER','','','','','XXEIS_RS_ADMIN');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 91000');
END IF;
END;
/
--Exporting View Component Data of the View -  XXCUS_BULLET_IEXP_TBL
prompt Creating Object Data XXCUS_NATURAL_ACCT_TBL
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(91000);
IF mod_exist = 'Y' THEN 
--Inserting Object XXCUS_NATURAL_ACCT_TBL
xxeis.eis_rsc_ins.v( 'XXCUS_NATURAL_ACCT_TBL',91000,'','1.0','','','ANONYMOUS','XXCUS','Xxcus Natural Acct Tbl','XNAT','','','TABLE','US','','');
--Delete Object Columns for XXCUS_NATURAL_ACCT_TBL
xxeis.eis_rsc_utility.delete_view_rows('XXCUS_NATURAL_ACCT_TBL',91000,FALSE);
--Inserting Object Columns for XXCUS_NATURAL_ACCT_TBL
xxeis.eis_rsc_ins.vc( 'XXCUS_NATURAL_ACCT_TBL','PROJECTDESCR',91000,'','PROJECTDESCR','','','','ANONYMOUS','VARCHAR2','XXCUS_NATURAL_ACCT_TBL','PROJECTDESCR','Projectdescr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_NATURAL_ACCT_TBL','LOB',91000,'','LOB','','','','ANONYMOUS','VARCHAR2','XXCUS_NATURAL_ACCT_TBL','LOB','Lob','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_NATURAL_ACCT_TBL','LOB_SUBCLASS',91000,'','LOB_SUBCLASS','','','','ANONYMOUS','VARCHAR2','XXCUS_NATURAL_ACCT_TBL','LOB_SUBCLASS','Lob Subclass','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_NATURAL_ACCT_TBL','SYSTEM_CD',91000,'','SYSTEM_CD','','','','ANONYMOUS','VARCHAR2','XXCUS_NATURAL_ACCT_TBL','SYSTEM_CD','System Cd','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_NATURAL_ACCT_TBL','TARGET_SYS',91000,'','TARGET_SYS','','','','ANONYMOUS','VARCHAR2','XXCUS_NATURAL_ACCT_TBL','TARGET_SYS','Target Sys','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_NATURAL_ACCT_TBL','MAP_LEVEL',91000,'','MAP_LEVEL','','','','ANONYMOUS','VARCHAR2','XXCUS_NATURAL_ACCT_TBL','MAP_LEVEL','Map Level','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_NATURAL_ACCT_TBL','MAP_SUBLEVEL',91000,'','MAP_SUBLEVEL','','','','ANONYMOUS','VARCHAR2','XXCUS_NATURAL_ACCT_TBL','MAP_SUBLEVEL','Map Sublevel','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_NATURAL_ACCT_TBL','Z_OLD_VALUE',91000,'','Z_OLD_VALUE','','','','ANONYMOUS','VARCHAR2','XXCUS_NATURAL_ACCT_TBL','Z_OLD_VALUE','Z Old Value','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_NATURAL_ACCT_TBL','Z_OLD_VALUE2',91000,'','Z_OLD_VALUE2','','','','ANONYMOUS','VARCHAR2','XXCUS_NATURAL_ACCT_TBL','Z_OLD_VALUE2','Z Old Value2','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_NATURAL_ACCT_TBL','Z_OLD_VALUE3',91000,'','Z_OLD_VALUE3','','','','ANONYMOUS','VARCHAR2','XXCUS_NATURAL_ACCT_TBL','Z_OLD_VALUE3','Z Old Value3','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_NATURAL_ACCT_TBL','Z_OLD_VALUE4',91000,'','Z_OLD_VALUE4','','','','ANONYMOUS','VARCHAR2','XXCUS_NATURAL_ACCT_TBL','Z_OLD_VALUE4','Z Old Value4','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_NATURAL_ACCT_TBL','Z_NEW_VALUE',91000,'','Z_NEW_VALUE','','','','ANONYMOUS','VARCHAR2','XXCUS_NATURAL_ACCT_TBL','Z_NEW_VALUE','Z New Value','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_NATURAL_ACCT_TBL','Z_NEW_VALUE2',91000,'','Z_NEW_VALUE2','','','','ANONYMOUS','VARCHAR2','XXCUS_NATURAL_ACCT_TBL','Z_NEW_VALUE2','Z New Value2','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_NATURAL_ACCT_TBL','Z_NEW_VALUE3',91000,'','Z_NEW_VALUE3','','','','ANONYMOUS','VARCHAR2','XXCUS_NATURAL_ACCT_TBL','Z_NEW_VALUE3','Z New Value3','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_NATURAL_ACCT_TBL','Z_NEW_VALUE4',91000,'','Z_NEW_VALUE4','','','','ANONYMOUS','VARCHAR2','XXCUS_NATURAL_ACCT_TBL','Z_NEW_VALUE4','Z New Value4','','','','US');
--Inserting Object Components for XXCUS_NATURAL_ACCT_TBL
--Inserting Object Component Joins for XXCUS_NATURAL_ACCT_TBL
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 91000');
END IF;
END;
/
--Exporting View Component Data of the View -  XXCUS_BULLET_IEXP_TBL
prompt Creating Object Data XXCUS_LOCATION_CODE_TBL
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(91000);
IF mod_exist = 'Y' THEN 
--Inserting Object XXCUS_LOCATION_CODE_TBL
xxeis.eis_rsc_ins.v( 'XXCUS_LOCATION_CODE_TBL',91000,'','1.0','','','ANONYMOUS','XXCUS','Xxcus Location Code Tbl','XLCT','','','TABLE','US','','');
--Delete Object Columns for XXCUS_LOCATION_CODE_TBL
xxeis.eis_rsc_utility.delete_view_rows('XXCUS_LOCATION_CODE_TBL',91000,FALSE);
--Inserting Object Columns for XXCUS_LOCATION_CODE_TBL
xxeis.eis_rsc_ins.vc( 'XXCUS_LOCATION_CODE_TBL','FRU',91000,'','FRU','','','','ANONYMOUS','VARCHAR2','XXCUS_LOCATION_CODE_TBL','FRU','Fru','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_LOCATION_CODE_TBL','EFFDT',91000,'','EFFDT','','','','ANONYMOUS','DATE','XXCUS_LOCATION_CODE_TBL','EFFDT','Effdt','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_LOCATION_CODE_TBL','FRU_DESCR',91000,'','FRU_DESCR','','','','ANONYMOUS','VARCHAR2','XXCUS_LOCATION_CODE_TBL','FRU_DESCR','Fru Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_LOCATION_CODE_TBL','INACTIVE',91000,'','INACTIVE','','','','ANONYMOUS','VARCHAR2','XXCUS_LOCATION_CODE_TBL','INACTIVE','Inactive','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_LOCATION_CODE_TBL','LOB_BRANCH',91000,'','LOB_BRANCH','','','','ANONYMOUS','VARCHAR2','XXCUS_LOCATION_CODE_TBL','LOB_BRANCH','Lob Branch','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_LOCATION_CODE_TBL','LOB_DEPT',91000,'','LOB_DEPT','','','','ANONYMOUS','VARCHAR2','XXCUS_LOCATION_CODE_TBL','LOB_DEPT','Lob Dept','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_LOCATION_CODE_TBL','LOB_COMPANY',91000,'','LOB_COMPANY','','','','ANONYMOUS','VARCHAR2','XXCUS_LOCATION_CODE_TBL','LOB_COMPANY','Lob Company','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_LOCATION_CODE_TBL','SYSTEM_CD',91000,'','SYSTEM_CD','','','','ANONYMOUS','VARCHAR2','XXCUS_LOCATION_CODE_TBL','SYSTEM_CD','System Cd','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_LOCATION_CODE_TBL','SYSTEM_CODE',91000,'','SYSTEM_CODE','','','','ANONYMOUS','VARCHAR2','XXCUS_LOCATION_CODE_TBL','SYSTEM_CODE','System Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_LOCATION_CODE_TBL','ENTRP_ENTITY',91000,'','ENTRP_ENTITY','','','','ANONYMOUS','VARCHAR2','XXCUS_LOCATION_CODE_TBL','ENTRP_ENTITY','Entrp Entity','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_LOCATION_CODE_TBL','ENTRP_LOC',91000,'','ENTRP_LOC','','','','ANONYMOUS','VARCHAR2','XXCUS_LOCATION_CODE_TBL','ENTRP_LOC','Entrp Loc','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_LOCATION_CODE_TBL','ENTRP_CC',91000,'','ENTRP_CC','','','','ANONYMOUS','VARCHAR2','XXCUS_LOCATION_CODE_TBL','ENTRP_CC','Entrp Cc','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_LOCATION_CODE_TBL','AP_DOCUM_FLAG',91000,'','AP_DOCUM_FLAG','','','','ANONYMOUS','VARCHAR2','XXCUS_LOCATION_CODE_TBL','AP_DOCUM_FLAG','Ap Docum Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_LOCATION_CODE_TBL','HC_FLAG',91000,'','HC_FLAG','','','','ANONYMOUS','VARCHAR2','XXCUS_LOCATION_CODE_TBL','HC_FLAG','Hc Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_LOCATION_CODE_TBL','BUSINESS_UNIT',91000,'','BUSINESS_UNIT','','','','ANONYMOUS','VARCHAR2','XXCUS_LOCATION_CODE_TBL','BUSINESS_UNIT','Business Unit','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_LOCATION_CODE_TBL','COMPANY',91000,'','COMPANY','','','','ANONYMOUS','VARCHAR2','XXCUS_LOCATION_CODE_TBL','COMPANY','Company','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_LOCATION_CODE_TBL','IEXP_FRU_OVERRIDE',91000,'','IEXP_FRU_OVERRIDE','','','','ANONYMOUS','VARCHAR2','XXCUS_LOCATION_CODE_TBL','IEXP_FRU_OVERRIDE','Iexp Fru Override','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_LOCATION_CODE_TBL','FRU_OVERRIDE',91000,'','FRU_OVERRIDE','','','','ANONYMOUS','VARCHAR2','XXCUS_LOCATION_CODE_TBL','FRU_OVERRIDE','Fru Override','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_LOCATION_CODE_TBL','CREATION_DT',91000,'','CREATION_DT','','','','ANONYMOUS','DATE','XXCUS_LOCATION_CODE_TBL','CREATION_DT','Creation Dt','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_LOCATION_CODE_TBL','CREATEOPRID',91000,'','CREATEOPRID','','','','ANONYMOUS','VARCHAR2','XXCUS_LOCATION_CODE_TBL','CREATEOPRID','Createoprid','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_LOCATION_CODE_TBL','UPDATE_DT',91000,'','UPDATE_DT','','','','ANONYMOUS','DATE','XXCUS_LOCATION_CODE_TBL','UPDATE_DT','Update Dt','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_LOCATION_CODE_TBL','UPDATED_USERID',91000,'','UPDATED_USERID','','','','ANONYMOUS','VARCHAR2','XXCUS_LOCATION_CODE_TBL','UPDATED_USERID','Updated Userid','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_LOCATION_CODE_TBL','COUNTRY',91000,'','COUNTRY','','','','ANONYMOUS','VARCHAR2','XXCUS_LOCATION_CODE_TBL','COUNTRY','Country','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_LOCATION_CODE_TBL','STATE',91000,'','STATE','','','','ANONYMOUS','VARCHAR2','XXCUS_LOCATION_CODE_TBL','STATE','State','','','','US');
--Inserting Object Components for XXCUS_LOCATION_CODE_TBL
--Inserting Object Component Joins for XXCUS_LOCATION_CODE_TBL
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 91000');
END IF;
END;
/
--Exporting View Component Data of the View -  XXCUS_BULLET_IEXP_TBL
prompt Creating Object Data AP_CREDIT_CARD_TRXNS_ALL
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(91000);
IF mod_exist = 'Y' THEN 
--Inserting Object AP_CREDIT_CARD_TRXNS_ALL
xxeis.eis_rsc_ins.v( 'AP_CREDIT_CARD_TRXNS_ALL',91000,'Detailed information about the credit card transactions of your employees','1.0','','','XXEIS_RS_ADMIN','AP','Ap Credit Card Trxns All','ACCTA','','','TABLE','US','','');
--Delete Object Columns for AP_CREDIT_CARD_TRXNS_ALL
xxeis.eis_rsc_utility.delete_view_rows('AP_CREDIT_CARD_TRXNS_ALL',91000,FALSE);
--Inserting Object Columns for AP_CREDIT_CARD_TRXNS_ALL
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','INACTIVE_EMP_WF_ITEM_KEY',91000,'','INACTIVE_EMP_WF_ITEM_KEY','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','INACTIVE_EMP_WF_ITEM_KEY','Inactive Emp Wf Item Key','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','LOCATION_ID',91000,'','LOCATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','LOCATION_ID','Location Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','REQUEST_ID',91000,'','REQUEST_ID','','','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','REQUEST_ID','Request Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','MERCHANT_COUNTRY_CODE',91000,'','MERCHANT_COUNTRY_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','MERCHANT_COUNTRY_CODE','Merchant Country Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','DISPUTE_DATE',91000,'','DISPUTE_DATE','','','','XXEIS_RS_ADMIN','DATE','AP_CREDIT_CARD_TRXNS_ALL','DISPUTE_DATE','Dispute Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','PAYMENT_DUE_FROM_CODE',91000,'','PAYMENT_DUE_FROM_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','PAYMENT_DUE_FROM_CODE','Payment Due From Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','TRX_AVAILABLE_DATE',91000,'','TRX_AVAILABLE_DATE','','','','XXEIS_RS_ADMIN','DATE','AP_CREDIT_CARD_TRXNS_ALL','TRX_AVAILABLE_DATE','Trx Available Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','CARD_ACCEPTOR_ID',91000,'','CARD_ACCEPTOR_ID','','','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','CARD_ACCEPTOR_ID','Card Acceptor Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','TRXN_DETAIL_FLAG',91000,'','TRXN_DETAIL_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','TRXN_DETAIL_FLAG','Trxn Detail Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','CARD_ID',91000,'','CARD_ID','','','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','CARD_ID','Card Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','DESCRIPTION',91000,'','DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','DESCRIPTION','Description','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','COMPANY_NUMBER',91000,'','COMPANY_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','COMPANY_NUMBER','Company Number','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','MARKET_CODE',91000,'','MARKET_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','MARKET_CODE','Market Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','VALIDATE_REQUEST_ID',91000,'','VALIDATE_REQUEST_ID','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','VALIDATE_REQUEST_ID','Validate Request Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','TRX_ID',91000,'','TRX_ID','','','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','TRX_ID','Trx Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','VALIDATE_CODE',91000,'','VALIDATE_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','VALIDATE_CODE','Validate Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','CARD_PROGRAM_ID',91000,'','CARD_PROGRAM_ID','','','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','CARD_PROGRAM_ID','Card Program Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','EXPENSED_AMOUNT',91000,'','EXPENSED_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','EXPENSED_AMOUNT','Expensed Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','REFERENCE_NUMBER',91000,'','REFERENCE_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','REFERENCE_NUMBER','Reference Number','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','TRANSACTION_TYPE',91000,'','TRANSACTION_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','TRANSACTION_TYPE','Transaction Type','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','TRANSACTION_DATE',91000,'','TRANSACTION_DATE','','','','XXEIS_RS_ADMIN','DATE','AP_CREDIT_CARD_TRXNS_ALL','TRANSACTION_DATE','Transaction Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','TRANSACTION_AMOUNT',91000,'','TRANSACTION_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','TRANSACTION_AMOUNT','Transaction Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','DEBIT_FLAG',91000,'','DEBIT_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','DEBIT_FLAG','Debit Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','BILLED_DATE',91000,'','BILLED_DATE','','','','XXEIS_RS_ADMIN','DATE','AP_CREDIT_CARD_TRXNS_ALL','BILLED_DATE','Billed Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','BILLED_AMOUNT',91000,'','BILLED_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','BILLED_AMOUNT','Billed Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','BILLED_DECIMAL',91000,'','BILLED_DECIMAL','','','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','BILLED_DECIMAL','Billed Decimal','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','BILLED_CURRENCY_CODE',91000,'','BILLED_CURRENCY_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','BILLED_CURRENCY_CODE','Billed Currency Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','POSTED_DATE',91000,'','POSTED_DATE','','','','XXEIS_RS_ADMIN','DATE','AP_CREDIT_CARD_TRXNS_ALL','POSTED_DATE','Posted Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','POSTED_AMOUNT',91000,'','POSTED_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','POSTED_AMOUNT','Posted Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','POSTED_DECIMAL',91000,'','POSTED_DECIMAL','','','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','POSTED_DECIMAL','Posted Decimal','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','POSTED_CURRENCY_CODE',91000,'','POSTED_CURRENCY_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','POSTED_CURRENCY_CODE','Posted Currency Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','CURRENCY_CONVERSION_EXPONENT',91000,'','CURRENCY_CONVERSION_EXPONENT','','','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','CURRENCY_CONVERSION_EXPONENT','Currency Conversion Exponent','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','CURRENCY_CONVERSION_RATE',91000,'','CURRENCY_CONVERSION_RATE','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','CURRENCY_CONVERSION_RATE','Currency Conversion Rate','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','MIS_INDUSTRY_CODE',91000,'','MIS_INDUSTRY_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','MIS_INDUSTRY_CODE','Mis Industry Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','SIC_CODE',91000,'','SIC_CODE','','','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','SIC_CODE','Sic Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','MERCHANT_TAX_ID',91000,'','MERCHANT_TAX_ID','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','MERCHANT_TAX_ID','Merchant Tax Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','MERCHANT_REFERENCE',91000,'','MERCHANT_REFERENCE','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','MERCHANT_REFERENCE','Merchant Reference','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','MERCHANT_NAME1',91000,'','MERCHANT_NAME1','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','MERCHANT_NAME1','Merchant Name1','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','MERCHANT_NAME2',91000,'','MERCHANT_NAME2','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','MERCHANT_NAME2','Merchant Name2','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','MERCHANT_ADDRESS1',91000,'','MERCHANT_ADDRESS1','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','MERCHANT_ADDRESS1','Merchant Address1','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','MERCHANT_ADDRESS2',91000,'','MERCHANT_ADDRESS2','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','MERCHANT_ADDRESS2','Merchant Address2','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','MERCHANT_ADDRESS3',91000,'','MERCHANT_ADDRESS3','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','MERCHANT_ADDRESS3','Merchant Address3','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','MERCHANT_ADDRESS4',91000,'','MERCHANT_ADDRESS4','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','MERCHANT_ADDRESS4','Merchant Address4','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','MERCHANT_CITY',91000,'','MERCHANT_CITY','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','MERCHANT_CITY','Merchant City','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','MERCHANT_PROVINCE_STATE',91000,'','MERCHANT_PROVINCE_STATE','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','MERCHANT_PROVINCE_STATE','Merchant Province State','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','CATEGORY',91000,'','CATEGORY','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','CATEGORY','Category','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','REPORT_HEADER_ID',91000,'','REPORT_HEADER_ID','','','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','REPORT_HEADER_ID','Report Header Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','EXPENSE_STATUS',91000,'','EXPENSE_STATUS','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','EXPENSE_STATUS','Expense Status','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','COMPANY_PREPAID_INVOICE_ID',91000,'','COMPANY_PREPAID_INVOICE_ID','','','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','COMPANY_PREPAID_INVOICE_ID','Company Prepaid Invoice Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','CARD_NUMBER',91000,'','CARD_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','CARD_NUMBER','Card Number','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','MERCHANT_POSTAL_CODE',91000,'','MERCHANT_POSTAL_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','MERCHANT_POSTAL_CODE','Merchant Postal Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','MERCHANT_COUNTRY',91000,'','MERCHANT_COUNTRY','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','MERCHANT_COUNTRY','Merchant Country','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','TOTAL_TAX',91000,'','TOTAL_TAX','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','TOTAL_TAX','Total Tax','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','LOCAL_TAX',91000,'','LOCAL_TAX','','','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','LOCAL_TAX','Local Tax','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','NATIONAL_TAX',91000,'','NATIONAL_TAX','','','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','NATIONAL_TAX','National Tax','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','OTHER_TAX',91000,'','OTHER_TAX','','','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','OTHER_TAX','Other Tax','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','ORG_ID',91000,'','ORG_ID','','','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','ORG_ID','Org Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','LAST_UPDATE_DATE',91000,'','LAST_UPDATE_DATE','','','','XXEIS_RS_ADMIN','DATE','AP_CREDIT_CARD_TRXNS_ALL','LAST_UPDATE_DATE','Last Update Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','LAST_UPDATED_BY',91000,'','LAST_UPDATED_BY','','','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','LAST_UPDATED_BY','Last Updated By','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','LAST_UPDATE_LOGIN',91000,'','LAST_UPDATE_LOGIN','','','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','LAST_UPDATE_LOGIN','Last Update Login','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','CREATION_DATE',91000,'','CREATION_DATE','','','','XXEIS_RS_ADMIN','DATE','AP_CREDIT_CARD_TRXNS_ALL','CREATION_DATE','Creation Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','CREATED_BY',91000,'','CREATED_BY','','','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','CREATED_BY','Created By','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','FOLIO_TYPE',91000,'','FOLIO_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','FOLIO_TYPE','Folio Type','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','ATM_CASH_ADVANCE',91000,'','ATM_CASH_ADVANCE','','','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','ATM_CASH_ADVANCE','Atm Cash Advance','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','ATM_TRANSACTION_DATE',91000,'','ATM_TRANSACTION_DATE','','','','XXEIS_RS_ADMIN','DATE','AP_CREDIT_CARD_TRXNS_ALL','ATM_TRANSACTION_DATE','Atm Transaction Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','ATM_FEE_AMOUNT',91000,'','ATM_FEE_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','ATM_FEE_AMOUNT','Atm Fee Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','ATM_TYPE',91000,'','ATM_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','ATM_TYPE','Atm Type','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','ATM_ID',91000,'','ATM_ID','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','ATM_ID','Atm Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','ATM_NETWORK_ID',91000,'','ATM_NETWORK_ID','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','ATM_NETWORK_ID','Atm Network Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','RESTAURANT_FOOD_AMOUNT',91000,'','RESTAURANT_FOOD_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','RESTAURANT_FOOD_AMOUNT','Restaurant Food Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','RESTAURANT_BEVERAGE_AMOUNT',91000,'','RESTAURANT_BEVERAGE_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','RESTAURANT_BEVERAGE_AMOUNT','Restaurant Beverage Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','RESTAURANT_TIP_AMOUNT',91000,'','RESTAURANT_TIP_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','RESTAURANT_TIP_AMOUNT','Restaurant Tip Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','CAR_RENTAL_DATE',91000,'','CAR_RENTAL_DATE','','','','XXEIS_RS_ADMIN','DATE','AP_CREDIT_CARD_TRXNS_ALL','CAR_RENTAL_DATE','Car Rental Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','CAR_RETURN_DATE',91000,'','CAR_RETURN_DATE','','','','XXEIS_RS_ADMIN','DATE','AP_CREDIT_CARD_TRXNS_ALL','CAR_RETURN_DATE','Car Return Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','CAR_RENTAL_LOCATION',91000,'','CAR_RENTAL_LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','CAR_RENTAL_LOCATION','Car Rental Location','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','CAR_RENTAL_STATE',91000,'','CAR_RENTAL_STATE','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','CAR_RENTAL_STATE','Car Rental State','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','CAR_RETURN_LOCATION',91000,'','CAR_RETURN_LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','CAR_RETURN_LOCATION','Car Return Location','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','CAR_RETURN_STATE',91000,'','CAR_RETURN_STATE','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','CAR_RETURN_STATE','Car Return State','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','CAR_RENTER_NAME',91000,'','CAR_RENTER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','CAR_RENTER_NAME','Car Renter Name','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','CAR_RENTAL_DAYS',91000,'','CAR_RENTAL_DAYS','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','CAR_RENTAL_DAYS','Car Rental Days','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','CAR_RENTAL_AGREEMENT_NUMBER',91000,'','CAR_RENTAL_AGREEMENT_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','CAR_RENTAL_AGREEMENT_NUMBER','Car Rental Agreement Number','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','CAR_CLASS',91000,'','CAR_CLASS','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','CAR_CLASS','Car Class','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','CAR_TOTAL_MILEAGE',91000,'','CAR_TOTAL_MILEAGE','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','CAR_TOTAL_MILEAGE','Car Total Mileage','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','CAR_GAS_AMOUNT',91000,'','CAR_GAS_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','CAR_GAS_AMOUNT','Car Gas Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','CAR_INSURANCE_AMOUNT',91000,'','CAR_INSURANCE_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','CAR_INSURANCE_AMOUNT','Car Insurance Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','CAR_MILEAGE_AMOUNT',91000,'','CAR_MILEAGE_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','CAR_MILEAGE_AMOUNT','Car Mileage Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','CAR_DAILY_RATE',91000,'','CAR_DAILY_RATE','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','CAR_DAILY_RATE','Car Daily Rate','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','HOTEL_ARRIVAL_DATE',91000,'','HOTEL_ARRIVAL_DATE','','','','XXEIS_RS_ADMIN','DATE','AP_CREDIT_CARD_TRXNS_ALL','HOTEL_ARRIVAL_DATE','Hotel Arrival Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','HOTEL_DEPART_DATE',91000,'','HOTEL_DEPART_DATE','','','','XXEIS_RS_ADMIN','DATE','AP_CREDIT_CARD_TRXNS_ALL','HOTEL_DEPART_DATE','Hotel Depart Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','HOTEL_CHARGE_DESC',91000,'','HOTEL_CHARGE_DESC','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','HOTEL_CHARGE_DESC','Hotel Charge Desc','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','HOTEL_GUEST_NAME',91000,'','HOTEL_GUEST_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','HOTEL_GUEST_NAME','Hotel Guest Name','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','HOTEL_STAY_DURATION',91000,'','HOTEL_STAY_DURATION','','','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','HOTEL_STAY_DURATION','Hotel Stay Duration','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','HOTEL_ROOM_RATE',91000,'','HOTEL_ROOM_RATE','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','HOTEL_ROOM_RATE','Hotel Room Rate','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','HOTEL_NO_SHOW_FLAG',91000,'','HOTEL_NO_SHOW_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','HOTEL_NO_SHOW_FLAG','Hotel No Show Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','HOTEL_ROOM_AMOUNT',91000,'','HOTEL_ROOM_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','HOTEL_ROOM_AMOUNT','Hotel Room Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','HOTEL_TELEPHONE_AMOUNT',91000,'','HOTEL_TELEPHONE_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','HOTEL_TELEPHONE_AMOUNT','Hotel Telephone Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','HOTEL_ROOM_TAX',91000,'','HOTEL_ROOM_TAX','','','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','HOTEL_ROOM_TAX','Hotel Room Tax','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','HOTEL_BAR_AMOUNT',91000,'','HOTEL_BAR_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','HOTEL_BAR_AMOUNT','Hotel Bar Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','HOTEL_MOVIE_AMOUNT',91000,'','HOTEL_MOVIE_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','HOTEL_MOVIE_AMOUNT','Hotel Movie Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','HOTEL_GIFT_SHOP_AMOUNT',91000,'','HOTEL_GIFT_SHOP_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','HOTEL_GIFT_SHOP_AMOUNT','Hotel Gift Shop Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','HOTEL_LAUNDRY_AMOUNT',91000,'','HOTEL_LAUNDRY_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','HOTEL_LAUNDRY_AMOUNT','Hotel Laundry Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','HOTEL_HEALTH_AMOUNT',91000,'','HOTEL_HEALTH_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','HOTEL_HEALTH_AMOUNT','Hotel Health Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','HOTEL_RESTAURANT_AMOUNT',91000,'','HOTEL_RESTAURANT_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','HOTEL_RESTAURANT_AMOUNT','Hotel Restaurant Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','HOTEL_BUSINESS_AMOUNT',91000,'','HOTEL_BUSINESS_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','HOTEL_BUSINESS_AMOUNT','Hotel Business Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','HOTEL_PARKING_AMOUNT',91000,'','HOTEL_PARKING_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','HOTEL_PARKING_AMOUNT','Hotel Parking Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','HOTEL_ROOM_SERVICE_AMOUNT',91000,'','HOTEL_ROOM_SERVICE_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','HOTEL_ROOM_SERVICE_AMOUNT','Hotel Room Service Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','HOTEL_TIP_AMOUNT',91000,'','HOTEL_TIP_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','HOTEL_TIP_AMOUNT','Hotel Tip Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','HOTEL_MISC_AMOUNT',91000,'','HOTEL_MISC_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','HOTEL_MISC_AMOUNT','Hotel Misc Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','HOTEL_CITY',91000,'','HOTEL_CITY','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','HOTEL_CITY','Hotel City','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','HOTEL_STATE',91000,'','HOTEL_STATE','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','HOTEL_STATE','Hotel State','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','HOTEL_FOLIO_NUMBER',91000,'','HOTEL_FOLIO_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','HOTEL_FOLIO_NUMBER','Hotel Folio Number','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','HOTEL_ROOM_TYPE',91000,'','HOTEL_ROOM_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','HOTEL_ROOM_TYPE','Hotel Room Type','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','AIR_DEPARTURE_DATE',91000,'','AIR_DEPARTURE_DATE','','','','XXEIS_RS_ADMIN','DATE','AP_CREDIT_CARD_TRXNS_ALL','AIR_DEPARTURE_DATE','Air Departure Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','AIR_DEPARTURE_CITY',91000,'','AIR_DEPARTURE_CITY','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','AIR_DEPARTURE_CITY','Air Departure City','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','AIR_ROUTING',91000,'','AIR_ROUTING','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','AIR_ROUTING','Air Routing','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','AIR_ARRIVAL_CITY',91000,'','AIR_ARRIVAL_CITY','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','AIR_ARRIVAL_CITY','Air Arrival City','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','AIR_STOPOVER_FLAG',91000,'','AIR_STOPOVER_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','AIR_STOPOVER_FLAG','Air Stopover Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','AIR_BASE_FARE_AMOUNT',91000,'','AIR_BASE_FARE_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','AIR_BASE_FARE_AMOUNT','Air Base Fare Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','AIR_FARE_BASIS_CODE',91000,'','AIR_FARE_BASIS_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','AIR_FARE_BASIS_CODE','Air Fare Basis Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','AIR_SERVICE_CLASS',91000,'','AIR_SERVICE_CLASS','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','AIR_SERVICE_CLASS','Air Service Class','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','AIR_CARRIER_ABBREVIATION',91000,'','AIR_CARRIER_ABBREVIATION','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','AIR_CARRIER_ABBREVIATION','Air Carrier Abbreviation','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','AIR_CARRIER_CODE',91000,'','AIR_CARRIER_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','AIR_CARRIER_CODE','Air Carrier Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','AIR_TICKET_ISSUER',91000,'','AIR_TICKET_ISSUER','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','AIR_TICKET_ISSUER','Air Ticket Issuer','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','AIR_ISSUER_CITY',91000,'','AIR_ISSUER_CITY','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','AIR_ISSUER_CITY','Air Issuer City','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','AIR_PASSENGER_NAME',91000,'','AIR_PASSENGER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','AIR_PASSENGER_NAME','Air Passenger Name','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','AIR_REFUND_TICKET_NUMBER',91000,'','AIR_REFUND_TICKET_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','AIR_REFUND_TICKET_NUMBER','Air Refund Ticket Number','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','AIR_EXCHANGED_TICKET_NUMBER',91000,'','AIR_EXCHANGED_TICKET_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','AIR_EXCHANGED_TICKET_NUMBER','Air Exchanged Ticket Number','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','AIR_AGENCY_NUMBER',91000,'','AIR_AGENCY_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','AIR_AGENCY_NUMBER','Air Agency Number','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','AIR_TICKET_NUMBER',91000,'','AIR_TICKET_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','AIR_TICKET_NUMBER','Air Ticket Number','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','FINANCIAL_CATEGORY',91000,'','FINANCIAL_CATEGORY','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','FINANCIAL_CATEGORY','Financial Category','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','PAYMENT_FLAG',91000,'','PAYMENT_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','PAYMENT_FLAG','Payment Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','RECORD_TYPE',91000,'','RECORD_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','RECORD_TYPE','Record Type','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','MERCHANT_ACTIVITY',91000,'','MERCHANT_ACTIVITY','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','MERCHANT_ACTIVITY','Merchant Activity','','','','US');
--Inserting Object Components for AP_CREDIT_CARD_TRXNS_ALL
--Inserting Object Component Joins for AP_CREDIT_CARD_TRXNS_ALL
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 91000');
END IF;
END;
/
prompt Creating Report LOV Data for HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(91000);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted
xxeis.eis_rsc_ins.lov( 91000,'select DISTINCT PERIOD_NAME from XXCUS.XXCUS_BULLET_IEXP_TBL','','XXCUS_PERIOD_NAME T&E','Oracle Period Date from the bullet train table','ID020048',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 91000,'select DISTINCT EMPLOYEE_NUMBER from XXCUS.XXCUS_BULLET_IEXP_TBL','','XXCUS_EMPLOYEE_NUMBER T&E','Oracle Employee number from the bullet train table','ID020048',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 91000,'select DISTINCT FULL_NAME from XXCUS.XXCUS_BULLET_IEXP_TBL','','XXCUS_EMPLOYEE_NAME T&E','Oracle Employee name from the bullet train table','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( '','SELECT SYSTEM_CD  FROM (select distinct xnat.system_cd
   from XXCUS.XXCUS_NATURAL_ACCT_TBL  xnat
 where xnat.system_cd in (''ORCL-CB'',''ORCL-CTI'',''ORCL-FM'',''ORCL-WW'',''ORCL-CAN'')
) CUO1212777','','SYSTEM_CD LOV','LOV Item Class "LOB Sources from XXCUS.XXCUS_NATURAL_ACCT_TBL".','ID020048',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 91000,'select DISTINCT ORACLE_LOCATION from XXCUS.XXCUS_BULLET_IEXP_TBL','','XXCUS_ORACLE_LOCATION T&E','Oracle Location number','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 91000,'select DISTINCT ORACLE_PRODUCT from XXCUS.XXCUS_BULLET_IEXP_TBL','','XXCUS_ORACLE_PRODUCT T&E','Oracle product number','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( '','select distinct fru from xxcus.xxcus_bullet_iexp_tbl','','XXCUS_T&E_FRU','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 91000');
END IF;
END;
/
prompt Creating Report Data for HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(91000);
IF mod_exist = 'Y' THEN 
--Deleting Report data - HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted
xxeis.eis_rsc_utility.delete_report_rows( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted' );
--Inserting Report - HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted
xxeis.eis_rsc_ins.r( 91000,'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted','','HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted','','','','XXEIS_RS_ADMIN','XXCUS_BULLET_IEXP_TBL','Y','','','XXEIS_RS_ADMIN','','Y','HDS Standard Reports','','CSV,EXCEL,Pivot Excel,','N','','','','','','N','','US','','','','');
--Inserting Report Columns - HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted
xxeis.eis_rsc_ins.rc( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted',91000,'APPROVER_ID','Approver Id','','','~T~D~0','default','','34','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXCUS_BULLET_IEXP_TBL','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted',91000,'APPROVER_NAME','Approver Name','','','','default','','33','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXCUS_BULLET_IEXP_TBL','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted',91000,'ATTENDEES','Attendees','','','','default','','32','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXCUS_BULLET_IEXP_TBL','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted',91000,'BUSINESS_PURPOSE','Business Purpose','','','','default','','8','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXCUS_BULLET_IEXP_TBL','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted',91000,'CC_FISCAL_PERIOD','Cc Fiscal Period','','','','default','','19','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXCUS_BULLET_IEXP_TBL','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted',91000,'CREATION_DATE','Creation Date','','','','default','','21','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXCUS_BULLET_IEXP_TBL','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted',91000,'CREDIT_CARD_TRX_ID','Credit Card Trx Id','','','~T~D~0','default','','17','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXCUS_BULLET_IEXP_TBL','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted',91000,'DAILY_DISTANCE','Daily Distance','','','~T~D~0','default','','30','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXCUS_BULLET_IEXP_TBL','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted',91000,'DESTINATION_FROM','Destination From','','','','default','','28','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXCUS_BULLET_IEXP_TBL','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted',91000,'DESTINATION_TO','Destination To','','','','default','','29','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXCUS_BULLET_IEXP_TBL','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted',91000,'DISTRIBUTION_LINE_NUMBER','Distribution Line Number','','','~T~D~0','default','','6','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXCUS_BULLET_IEXP_TBL','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted',91000,'DT_SENTTO_GL','Dt Sentto Gl','','','','default','','23','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXCUS_BULLET_IEXP_TBL','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted',91000,'EMPLOYEE_NUMBER','Employee Number','','','','default','','11','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXCUS_BULLET_IEXP_TBL','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted',91000,'EMP_DEFAULT_COSTCTR','Emp Default Costctr','','','','default','','14','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXCUS_BULLET_IEXP_TBL','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted',91000,'EMP_DEFAULT_LOC','Emp Default Loc','','','','default','','13','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXCUS_BULLET_IEXP_TBL','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted',91000,'EMP_DEFAULT_PROD','Emp Default Prod','','','','default','','12','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXCUS_BULLET_IEXP_TBL','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted',91000,'END_EXPENSE_DATE','End Expense Date','','','','default','','25','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXCUS_BULLET_IEXP_TBL','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted',91000,'EXPENSE_REPORT_NUMBER','Expense Report Number','','','~T~D~0','default','','5','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXCUS_BULLET_IEXP_TBL','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted',91000,'FULL_NAME','Full Name','','','','default','','10','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXCUS_BULLET_IEXP_TBL','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted',91000,'ITEM_DESCRIPTION','Item Description','','','','default','','7','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXCUS_BULLET_IEXP_TBL','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted',91000,'JUSTIFICATION','Justification','','','','default','','26','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXCUS_BULLET_IEXP_TBL','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted',91000,'LINE_AMOUNT','Line Amount','','','~T~D~2','default','','4','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXCUS_BULLET_IEXP_TBL','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted',91000,'MERCHANT_LOCATION','Merchant Location','','','','default','','16','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXCUS_BULLET_IEXP_TBL','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted',91000,'MERCHANT_NAME','Merchant Name','','','','default','','15','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXCUS_BULLET_IEXP_TBL','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted',91000,'ORACLE_ACCOUNT','Oracle Account','','','','default','','37','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXCUS_BULLET_IEXP_TBL','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted',91000,'ORACLE_COST_CENTER','Oracle Cost Center','','','','default','','36','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXCUS_BULLET_IEXP_TBL','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted',91000,'ORACLE_LOCATION','Oracle Location','','','','default','','35','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXCUS_BULLET_IEXP_TBL','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted',91000,'PERIOD_NAME','Period Name','','','','default','','1','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXCUS_BULLET_IEXP_TBL','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted',91000,'QUERY_DESCR','Query Descr','','','','default','','38','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXCUS_BULLET_IEXP_TBL','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted',91000,'REMARKS','Remarks','','','','default','','27','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXCUS_BULLET_IEXP_TBL','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted',91000,'REPORT_SUBMITTED_DATE','Report Submitted Date','','','','default','','22','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXCUS_BULLET_IEXP_TBL','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted',91000,'REPORT_TOTAL','Report Total','','','~T~D~2','default','','9','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXCUS_BULLET_IEXP_TBL','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted',91000,'START_EXPENSE_DATE','Start Expense Date','','','','default','','24','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXCUS_BULLET_IEXP_TBL','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted',91000,'TRANSACTION_AMOUNT','Transaction Amount','','','~T~D~2','default','','20','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXCUS_BULLET_IEXP_TBL','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted',91000,'TRANSACTION_DATE','Transaction Date','','','','default','','18','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXCUS_BULLET_IEXP_TBL','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted',91000,'TRIP_DISTANCE','Trip Distance','','','~T~D~0','default','','31','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXCUS_BULLET_IEXP_TBL','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted',91000,'Z_NEW_VALUE','Z New Value','','','','default','','2','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXCUS_BULLET_IEXP_TBL','XXCUS_NATURAL_ACCT_TBL','XNAT','','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted',91000,'Z_NEW_VALUE2','Z New Value2','','','','default','','3','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXCUS_BULLET_IEXP_TBL','XXCUS_NATURAL_ACCT_TBL','XNAT','','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted',91000,'FRU','Fru','','','','default','','4','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXCUS_BULLET_IEXP_TBL','XXCUS_LOCATION_CODE_TBL','XLC','','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted',91000,'CATEGORY','Category','','','','default','','40','','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXCUS_BULLET_IEXP_TBL','AP_CREDIT_CARD_TRXNS_ALL','ACCTA','','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted',91000,'PROJECT_NAME','Project Name','','','','default','','40','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXCUS_BULLET_IEXP_TBL','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted',91000,'PROJECT_NUMBER','Project Number','','','','default','','39','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXCUS_BULLET_IEXP_TBL','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted',91000,'MCC_CODE','Mcc Code','','','','default','','42','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXCUS_BULLET_IEXP_TBL','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted',91000,'MCC_CODE_NO','Mcc Code No','','','','default','','41','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXCUS_BULLET_IEXP_TBL','','','','US','');
--Inserting Report Parameters - HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted
xxeis.eis_rsc_ins.rp( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted',91000,'Fiscal Period','','','IN','XXCUS_PERIOD_NAME T&E','','VARCHAR2','Y','Y','1','Y','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','XXCUS_BULLET_IEXP_TBL','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted',91000,'Oracle Product','','','IN','XXCUS_ORACLE_PRODUCT T&E','','VARCHAR2','Y','Y','3','Y','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','XXCUS_BULLET_IEXP_TBL','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted',91000,'Employee Name','','','IN','XXCUS_EMPLOYEE_NAME T&E','','VARCHAR2','N','Y','4','Y','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','XXCUS_BULLET_IEXP_TBL','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted',91000,'BranchFRU','','','IN','XXCUS_T&E_FRU','','VARCHAR2','N','Y','5','Y','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','XXCUS_BULLET_IEXP_TBL','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted',91000,'Oracle Location','','','IN','XXCUS_ORACLE_LOCATION T&E','','VARCHAR2','N','Y','6','Y','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','XXCUS_BULLET_IEXP_TBL','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted',91000,'Employee Number','','','IN','XXCUS_EMPLOYEE_NUMBER T&E','','VARCHAR2','N','Y','7','Y','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','XXCUS_BULLET_IEXP_TBL','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted',91000,'LegacySource','Legacy Source System','','=','SYSTEM_CD LOV','','VARCHAR2','Y','Y','2','N','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','XXCUS_BULLET_IEXP_TBL','','','US','');
--Inserting Dependent Parameters - HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted
--Inserting Report Conditions - HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted
xxeis.eis_rsc_ins.rcnh( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted',91000,'XBIT.EMPLOYEE_NUMBER IN :Employee Number ','ADVANCED','','1#$#','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','Employee Number','','','','','','','','','','','IN','Y','Y','XBIT.EMPLOYEE_NUMBER','','','','1',91000,'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted','XBIT.EMPLOYEE_NUMBER IN :Employee Number ');
xxeis.eis_rsc_ins.rcnh( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted',91000,'XBIT.FRU IN :BranchFRU ','ADVANCED','','1#$#','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','BranchFRU','','','','','','','','','','','IN','Y','Y','XBIT.FRU','','','','1',91000,'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted','XBIT.FRU IN :BranchFRU ');
xxeis.eis_rsc_ins.rcnh( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted',91000,'XBIT.FULL_NAME IN :Employee Name ','ADVANCED','','1#$#','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','Employee Name','','','','','','','','','','','IN','Y','Y','XBIT.FULL_NAME','','','','1',91000,'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted','XBIT.FULL_NAME IN :Employee Name ');
xxeis.eis_rsc_ins.rcnh( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted',91000,'XBIT.ORACLE_LOCATION IN :Oracle Location ','ADVANCED','','1#$#','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','Oracle Location','','','','','','','','','','','IN','Y','Y','XBIT.ORACLE_LOCATION','','','','1',91000,'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted','XBIT.ORACLE_LOCATION IN :Oracle Location ');
xxeis.eis_rsc_ins.rcnh( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted',91000,'XBIT.ORACLE_PRODUCT IN :Oracle Product ','ADVANCED','','1#$#','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','Oracle Product','','','','','','','','','','','IN','Y','Y','XBIT.ORACLE_PRODUCT','','','','1',91000,'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted','XBIT.ORACLE_PRODUCT IN :Oracle Product ');
xxeis.eis_rsc_ins.rcnh( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted',91000,'XBIT.PERIOD_NAME IN :Fiscal Period ','ADVANCED','','1#$#','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','Fiscal Period','','','','','','','','','','','IN','Y','Y','XBIT.PERIOD_NAME','','','','1',91000,'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted','XBIT.PERIOD_NAME IN :Fiscal Period ');
xxeis.eis_rsc_ins.rcnh( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted',91000,'XNAT.SYSTEM_CD(+) = :LegacySource ','ADVANCED','','1#$#','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','LegacySource','','','','','','','','','','','EQUALS','Y','Y','XNAT.SYSTEM_CD(+)','','','','1',91000,'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted','XNAT.SYSTEM_CD(+) = :LegacySource ');
--Inserting Report Sorts - HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted
--Inserting Report Triggers - HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted
--inserting report templates - HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted
--Inserting Report Portals - HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted
--inserting report dashboards - HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted','91000','XXCUS_BULLET_IEXP_TBL','XXCUS_BULLET_IEXP_TBL','N','');
xxeis.eis_rsc_ins.rviews( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted','91000','XXCUS_BULLET_IEXP_TBL','XXCUS_NATURAL_ACCT_TBL','N','XNAT');
xxeis.eis_rsc_ins.rviews( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted','91000','XXCUS_BULLET_IEXP_TBL','XXCUS_LOCATION_CODE_TBL','N','XLC');
xxeis.eis_rsc_ins.rviews( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted','91000','XXCUS_BULLET_IEXP_TBL','AP_CREDIT_CARD_TRXNS_ALL','N','ACCTA');
--inserting report security - HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted
xxeis.eis_rsc_ins.rsec( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted','200','','',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted','200','','',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted','200','','',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted','200','','',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted','200','','HDS_AP_TRNS_ENTRY_US_GSC',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted','200','','',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted','200','','',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted','200','','HDS_AP_INQUIRY_US_GSC',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted','200','','HDS_AP_DISBURSEMTS_US_GSC',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted','200','','',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted','200','','',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted','200','','',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted','200','','',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted','200','','HDS_AP_ADMIN_US_GSC',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted','200','','',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted','200','','HDS_AP_SUPPLIER_MAINT_US_GSC',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted','200','','',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted','200','','PAYABLES_INQUIRY',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted','200','','PAYABLES_MANAGER',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted','200','','',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted','200','','',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted','200','','HDS_OIE_USER',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted','200','','HDS_AP_MGR_NOSUP_US_GSC',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted','20005','','XXWC_VIEW_ALL_EIS_REPORTS',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted','101','','XXCUS_GL_MANAGER_GLOBAL',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted','101','','XXCUS_GL_ACCOUNTANT_GLOBAL',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted','200','','HDS_AP_TRNS_ENTRY_CAD_GSC',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted','200','','HDS_AP_SUPPLIER_MAINT_CAD_GSC',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted','200','','HDS_AP_MGR_NOSUP_CAD_GSC',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted','200','','HDS_AP_INQ_CANADA',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted','200','','HDS_AP_DISBURSEMTS_CAD_GSC',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted','200','','HDS_AP_ADMIN_CAD_GSC',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted','200','','HDS_OIE_USER_CAD',91000,'XXEIS_RS_ADMIN','','','');
--Inserting Report Pivots - HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted
--Inserting Report   Version details- HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted
xxeis.eis_rsc_ins.rv( 'HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted','','HDS AP_IExpense_Trxns_Details_NON_Oracle_LOB - Posted','AB063501');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 91000');
END IF;
END;
/
